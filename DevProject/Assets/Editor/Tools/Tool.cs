﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEditor;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;
public class PSD2UGUI : EditorWindow
{

    [MenuItem("GameObject/复制Shader名字", false, 0)]
    static void CopyShader()
    {
        Transform transform = Selection.activeGameObject.transform;
        Image a = transform.gameObject.GetComponent<Image>();
        Debug.Log(a.material.shader.name);
    }

    [MenuItem("GameObject/复制GameObject路径", false, 0)]
    static void CopyGameObjectPath()
    {
        Transform transform = Selection.activeGameObject.transform;
        GameObject go = transform.gameObject;
        List<Transform> transformList = new List<Transform>();
        while (transform != null)
        {
            transformList.Add(transform);
            transform = transform.parent;
        }
        string path = "";
        for (int i = 0; i < transformList.Count-1; i++)
        {
            if(i==0)
            {
                path = transformList[i].name;
            }
            else
            {
                path = transformList[i].name + "/" + path;
            }
        }
        bool isPointerClick = false;
        MonoBehaviour[] components = go.GetComponents<MonoBehaviour>();
        bool isButton = false;
        bool isToggle = false;
        for (int i = 0; i < components.Length; i++)
        {
            if(components[i].enabled)
            {
                Type[] types = components[i].GetType().GetInterfaces();
                for (int j = 0; j < types.Length; j++)
                {
                    if (types[j].Name == "IPointerClickHandler")
                    {
                        isPointerClick = true;
                    }
                }
                if(components[i] is Button)
                {
                    isButton = true;
                }
                else if(components[i] is Toggle)
                {
                    isToggle = true;
                }
            }
        }
        if (isPointerClick)
        {
            if(isButton)
            {
                EditorGUIUtility.systemCopyBuffer = path+",Button";
            }
           else if(isToggle)
           {
                EditorGUIUtility.systemCopyBuffer = path+",Toggle";
           }
           else
           {
                EditorGUIUtility.systemCopyBuffer = path+",Container";
           }
        }
        else
        {
            EditorGUIUtility.systemCopyBuffer = "无效路径:"+path;
        }
        Debug.Log("GameObject Path: " + path);
    }

    [MenuItem("GameObject/复制GameObject位置成list", false, 0)]
    static void CopyGameObjectPosition()
    {
        Transform transform = Selection.activeGameObject.transform;
        GameObject go = transform.gameObject;
        EditorGUIUtility.systemCopyBuffer = string.Concat(transform.localPosition.x, ",",
            transform.localPosition.y, ",", transform.localPosition.z);
    }

    [MenuItem("GameObject/复制GameObject角度成list", false, 0)]
    static void CopyGameObjectLocalEulerAngles()
    {
        Transform transform = Selection.activeGameObject.transform;
        GameObject go = transform.gameObject;
        EditorGUIUtility.systemCopyBuffer = string.Concat(transform.localEulerAngles.x, ",",
            transform.localEulerAngles.y, ",", transform.localEulerAngles.z);
    }

    [MenuItem("Tools/打开persistentDataPath")]
    public static void OpenPersistentDataPath()
    {
        System.Diagnostics.Process.Start("Explorer.exe", Application.persistentDataPath.Replace('/', '\\'));
    }
}