﻿#define ENABLE_BLOOM_GUI
#define ENABLE_RADIALBLUR_GUI
#define ENABLE_SCREENDARK_GUI


using UnityEngine;
using UnityEditor;
using System.Collections;

namespace ArtTech.ImageEffects {
    [CustomEditor(typeof(PostEffect))]
    public class YSPostEffectEditor : Editor {
        SerializedObject _serializedObj;

#if ENABLE_DOF_GUI
        SerializedProperty _DOFEnabled;
        SerializedProperty _DOFForegroundBlur;    
        SerializedProperty _DOFFocalDistance;
        SerializedProperty _DOFSmoothness;
        SerializedProperty _DOFBlurWidth;
        SerializedProperty _DOFFocalObject;
#endif

#if ENABLE_SS_GUI
        SerializedProperty _sunShaftsEnabled;
        SerializedProperty _sunShaftsScreenBlendMode;
        SerializedProperty _sunTransform;
        SerializedProperty _sunColor;
        SerializedProperty _sunShaftsBlurRadius;
        SerializedProperty _sunShaftsIntensity;
        SerializedProperty _sunShaftsMaxRadius;
        private static bool _showCrosshair = false;
#endif

#if ENABLE_BLOOM_GUI
        SerializedProperty _bloomEnabled;
        SerializedProperty _bloomThreshhold;
        SerializedProperty _bloomMaxBrightness;
        SerializedProperty _bloomSoftKnee;
        SerializedProperty _bloomRadius;
        SerializedProperty _bloomIntensity; 
#endif

#if ENABLE_RADIALBLUR_GUI
        SerializedProperty _radialBlurEnabled;
        SerializedProperty _radialBlurStrength;
        //SerializedProperty _radialBlurSamples;
        SerializedProperty _radialBlurCenter;
        SerializedProperty _radialBlurQuality;
        SerializedProperty _radialBlurSharpness;
        SerializedProperty _radialBlurDarkness;
        SerializedProperty _radialBlurEnableVignette;
        SerializedProperty _radialBlurVignetteSize;
#endif

#if ENABLE_SCREENDARK_GUI
        SerializedProperty _screenDarkEnabled;
        SerializedProperty _screenDarkAlpha;
        SerializedProperty _screendDarkLayers;
#endif

#if ENABLE_CC_GUI
        SerializedProperty _colorCorrectionEnabled;
       // SerializedProperty _colorCorrectionMode;
        SerializedProperty _CCRedChannel;
        SerializedProperty _CCGreenChannel;
        SerializedProperty _CCBlueChannel;
        SerializedProperty _CCLutTexture;
#endif

#if ENABLE_VIGN_GUI
        SerializedProperty _vignettingEnabled;
        SerializedProperty _vignettingIntensity;
#endif

#if ENABLE_SAT_GUI
        SerializedProperty _saturationEnable;
        SerializedProperty _saturation;
#endif

#if ENABLE_BRI_GUI
        SerializedProperty _brightnessEnable;
        SerializedProperty _brightness;
#endif


        void OnEnable() {
            _serializedObj = new SerializedObject(target);

#if ENABLE_SS_GUI
            _sunShaftsEnabled = _serializedObj.FindProperty("_sunShaftsEnabled");
            _sunShaftsScreenBlendMode = _serializedObj.FindProperty("_sunShaftsScreenBlendMode");
            _sunTransform = _serializedObj.FindProperty("_sunTransform");
            _sunColor = _serializedObj.FindProperty("_sunColor");
            _sunShaftsBlurRadius = _serializedObj.FindProperty("_sunShaftsBlurRadius");
            _sunShaftsIntensity = _serializedObj.FindProperty("_sunShaftsIntensity");
            _sunShaftsMaxRadius = _serializedObj.FindProperty("_sunShaftsMaxRadius");
#endif

#if ENABLE_DOF_GUI
            _DOFEnabled = _serializedObj.FindProperty("_DOFEnabled");
            _DOFForegroundBlur = _serializedObj.FindProperty("_DOFForegroundBlur");
            _DOFFocalDistance = _serializedObj.FindProperty("_DOFFocalDistance");
            _DOFSmoothness = _serializedObj.FindProperty("_DOFSmoothness");
            _DOFBlurWidth = _serializedObj.FindProperty("_DOFBlurWidth");
            _DOFFocalObject = _serializedObj.FindProperty("_DOFFocalObject");
#endif

#if ENABLE_BLOOM_GUI
            _bloomEnabled = _serializedObj.FindProperty("_bloomEnabled");
            _bloomThreshhold = _serializedObj.FindProperty("_bloomParameters._threshold");
            _bloomMaxBrightness = _serializedObj.FindProperty("_bloomParameters._maxBrightness");
            _bloomSoftKnee = _serializedObj.FindProperty("_bloomParameters._softKnee");
            _bloomRadius = _serializedObj.FindProperty("_bloomParameters._radius");
            _bloomIntensity = _serializedObj.FindProperty("_bloomParameters._intensity");
#endif

#if ENABLE_RADIALBLUR_GUI
            _radialBlurEnabled = _serializedObj.FindProperty("_radialBlurEnabled");
            _radialBlurStrength = _serializedObj.FindProperty("_radialBlurParameters._strength");
            //_radialBlurSamples = _serializedObj.FindProperty("_radialBlurParameters._samples");
            _radialBlurCenter = _serializedObj.FindProperty("_radialBlurParameters._center");
            _radialBlurQuality = _serializedObj.FindProperty("_radialBlurParameters._quality");
            _radialBlurSharpness = _serializedObj.FindProperty("_radialBlurParameters._sharpness");
            _radialBlurDarkness = _serializedObj.FindProperty("_radialBlurParameters._darkness");
            _radialBlurEnableVignette = _serializedObj.FindProperty("_radialBlurParameters._enableVignette");
            _radialBlurVignetteSize = _serializedObj.FindProperty("_radialBlurParameters._vignetteSize");
#endif

#if ENABLE_SCREENDARK_GUI
            _screenDarkEnabled = _serializedObj.FindProperty("_screenDarkEnabled");
            _screenDarkAlpha = _serializedObj.FindProperty("_screenDarkParameters._alpha");
            _screendDarkLayers = _serializedObj.FindProperty("_screenDarkParameters._layers");
#endif

#if ENABLE_CC_GUI
            _colorCorrectionEnabled = _serializedObj.FindProperty("_colorCorrectionEnabled");
            //_colorCorrectionMode = _serializedObj.FindProperty("_colorCorrectionMode");
            _CCRedChannel = _serializedObj.FindProperty("_CCRedChannel");
            _CCGreenChannel = _serializedObj.FindProperty("_CCGreenChannel");
            _CCBlueChannel = _serializedObj.FindProperty("_CCBlueChannel");
            _CCLutTexture = _serializedObj.FindProperty("_CCLutTexture");
           // if (_colorCorrectionMode.enumValueIndex == (int)PostEffect.ColorCorrectionMode.Simple) {
                if (_CCRedChannel.animationCurveValue.length == 0)
                    _CCRedChannel.animationCurveValue = new AnimationCurve(new Keyframe(0, 0, 1, 1), new Keyframe(1, 1, 1, 1));
                if (_CCGreenChannel.animationCurveValue.length == 0)
                    _CCGreenChannel.animationCurveValue = new AnimationCurve(new Keyframe(0, 0, 1, 1), new Keyframe(1, 1, 1, 1));
                if (_CCBlueChannel.animationCurveValue.length == 0)
                    _CCBlueChannel.animationCurveValue = new AnimationCurve(new Keyframe(0, 0, 1, 1), new Keyframe(1, 1, 1, 1));

                _serializedObj.ApplyModifiedProperties();
          //  }
#endif

#if ENABLE_VIGN_GUI
            _vignettingEnabled = _serializedObj.FindProperty("_vignettingEnabled");
            _vignettingIntensity = _serializedObj.FindProperty("_vignettingIntensity");
#endif

#if ENABLE_SAT_GUI
            _saturationEnable = _serializedObj.FindProperty("_saturationEnable");
            _saturation = _serializedObj.FindProperty("_saturation");
#endif

#if ENABLE_BRI_GUI
            _brightnessEnable = _serializedObj.FindProperty("_brightnessEnable");
            _brightness = _serializedObj.FindProperty("_brightness");
#endif
        }

        public override void OnInspectorGUI() {
            _serializedObj.Update();
            PostEffect pe = target as PostEffect;

            bool isModuleChanged = false;


            //Color bgColor = GUI.backgroundColor = new Color(.74f, .74f, 1f, 1f);
            //Color contentColor = GUI.contentColor = new Color(.8f, .8f, 1f, 1f);

#if ENABLE_DOF_GUI
            EditorGUILayout.Space();

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(_DOFEnabled, new GUIContent("Depth Of Field"));
            if (EditorGUI.EndChangeCheck())
                isModuleChanged = true;
            if (_DOFEnabled.boolValue) {
                EditorGUILayout.BeginVertical("box");
                EditorGUILayout.Space();
                EditorGUILayout.PropertyField(_DOFForegroundBlur, new GUIContent("Foreground Blur ?"));
                EditorGUILayout.PropertyField(_DOFFocalDistance, new GUIContent("Focal Distance"));
                EditorGUILayout.PropertyField(_DOFSmoothness, new GUIContent("Smoothness"));
                EditorGUILayout.PropertyField(_DOFBlurWidth, new GUIContent("Blur Width"));
                EditorGUILayout.PropertyField(_DOFFocalObject, new GUIContent("Focal Object"));
                EditorGUILayout.Space();
                EditorGUILayout.EndVertical();
            }
#endif

#if ENABLE_SS_GUI
            EditorGUILayout.Space();

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(_sunShaftsEnabled, new GUIContent("Sun Shafts"));
            if (EditorGUI.EndChangeCheck())
                isModuleChanged = true;
            if (_sunShaftsEnabled.boolValue) {
                EditorGUILayout.BeginVertical("box");
                EditorGUILayout.Space();
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(_sunShaftsScreenBlendMode, new GUIContent("Blend Mode"));
                if (EditorGUI.EndChangeCheck())
                    isModuleChanged = true;
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PropertyField(_sunTransform, new GUIContent("Shafts Caster", "Chose a transform that acts as a root point for the produced sun shafts"));
                if (pe._sunTransform) {
                    if (GUILayout.Button("Move to screen center")) {
                        if (EditorUtility.DisplayDialog("", "" + pe._sunTransform.name + "将被移动到当前摄像机的前方。 确定进行吗? ", "确定", "取消")) {
                            Camera editorCamera = SceneView.currentDrawingSceneView.camera;
                            Ray ray = editorCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
                            pe._sunTransform.position = ray.origin + ray.direction * 500.0f;
                            pe._sunTransform.LookAt(editorCamera.transform);
                        }
                    }
                }
                EditorGUILayout.EndHorizontal();
                if (pe._sunTransform) {
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    _showCrosshair = GUILayout.Toggle(_showCrosshair, "Show Crosshair");
                    EditorGUILayout.EndHorizontal();
                }
                EditorGUILayout.PropertyField(_sunColor, new GUIContent("Shafts Color"));
                _sunShaftsMaxRadius.floatValue = 1.0f - EditorGUILayout.Slider("Distance Falloff", 1.0f - _sunShaftsMaxRadius.floatValue, 0, 1);
                EditorGUILayout.PropertyField(_sunShaftsBlurRadius, new GUIContent("Blur Size"));
                EditorGUILayout.PropertyField(_sunShaftsIntensity, new GUIContent("Intensity"));
                EditorGUILayout.Space();
                EditorGUILayout.EndVertical();
            }     
#endif

#if ENABLE_BLOOM_GUI
            EditorGUILayout.Space();

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(_bloomEnabled, new GUIContent("Bloom"));
            if (EditorGUI.EndChangeCheck())
                isModuleChanged = true;
            if (_bloomEnabled.boolValue) {
                EditorGUILayout.BeginVertical("box");
                EditorGUILayout.Space();
                EditorGUILayout.PropertyField(_bloomThreshhold, new GUIContent("Threshhold", "Filters out pixels under this level of brightness."));
                EditorGUILayout.PropertyField(_bloomMaxBrightness, new GUIContent("Max Brightness"));
                EditorGUILayout.PropertyField(_bloomSoftKnee, new GUIContent("Soft Knee", "Makes transition between under/over-threshold gradual."));
                EditorGUILayout.PropertyField(_bloomRadius, new GUIContent("Radius", "Changes extent of veiling effects in A screen resolution-independent fashion."));
                EditorGUILayout.PropertyField(_bloomIntensity, new GUIContent("Intensity", "Blend factor of the result image."));
                EditorGUILayout.Space();
                EditorGUILayout.EndVertical();
            }
#endif

#if ENABLE_RADIALBLUR_GUI
            if(Application.isPlaying) {
                EditorGUILayout.Space();

                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(_radialBlurEnabled, new GUIContent("Radial Blur"));
                if (EditorGUI.EndChangeCheck())
                    isModuleChanged = true;
                if (_radialBlurEnabled.boolValue) {
                    EditorGUILayout.BeginVertical("box");
                    EditorGUILayout.Space();
                    EditorGUILayout.PropertyField(_radialBlurStrength, new GUIContent("Strength", "Blur strength."));
                    //EditorGUILayout.PropertyField(_radialBlurSamples, new GUIContent("Samples", "Sample count. Higher means better quality but slower processing."));
                    EditorGUILayout.PropertyField(_radialBlurCenter, new GUIContent("Center", "Focus point."));
                    EditorGUILayout.PropertyField(_radialBlurQuality, new GUIContent("Quality", "Quality preset. Higher means better quality but slower processing."));
                    EditorGUILayout.PropertyField(_radialBlurSharpness, new GUIContent("Sharpness", "Smoothness of the vignette effect."));
                    EditorGUILayout.PropertyField(_radialBlurDarkness, new GUIContent("Darkness", "Amount of vignetting on screen."));
                    EditorGUI.BeginChangeCheck();
                    EditorGUILayout.PropertyField(_radialBlurEnableVignette, new GUIContent("Enable Vignette", "Should the effect be applied like a vignette ?"));
                    if (EditorGUI.EndChangeCheck())
                        isModuleChanged = true;
                    EditorGUILayout.PropertyField(_radialBlurVignetteSize, new GUIContent("Vignette Size"));
                    EditorGUILayout.Space();
                    EditorGUILayout.EndVertical();
                }
            }
#endif


#if ENABLE_SCREENDARK_GUI
            if (Application.isPlaying) {
                EditorGUILayout.Space();

                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(_screenDarkEnabled, new GUIContent("Screen Dark"));
                if (EditorGUI.EndChangeCheck())
                    isModuleChanged = true;
                if (_screenDarkEnabled.boolValue) {
                    EditorGUILayout.BeginVertical("box");
                    EditorGUILayout.Space();
                    EditorGUILayout.PropertyField(_screenDarkAlpha, new GUIContent("Alpha"));
                    EditorGUILayout.PropertyField(_screendDarkLayers, new GUIContent("Layers"));
                    EditorGUILayout.Space();
                    EditorGUILayout.EndVertical();
                }
            }
#endif

#if ENABLE_CC_GUI
            EditorGUILayout.Space();

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(_colorCorrectionEnabled, new GUIContent("Color Correction"));
            if (EditorGUI.EndChangeCheck())
                isModuleChanged = true;
            if (_colorCorrectionEnabled.boolValue) {
                EditorGUILayout.BeginVertical("box");
                EditorGUILayout.Space();
                //EditorGUI.BeginChangeCheck();
                //EditorGUILayout.PropertyField(_colorCorrectionMode, new GUIContent("Type"));
                //if (EditorGUI.EndChangeCheck())
                //    isModuleChanged = true;
               // if (_colorCorrectionMode.enumValueIndex == (int)PostEffect.ColorCorrectionMode.Simple) {
                    GUILayout.Label("Curves", EditorStyles.boldLabel);
                    EditorGUI.BeginChangeCheck();
                    EditorGUILayout.PropertyField(_CCRedChannel, new GUIContent("  Red"));
                    EditorGUILayout.PropertyField(_CCGreenChannel, new GUIContent("  Green"));
                    EditorGUILayout.PropertyField(_CCBlueChannel, new GUIContent("  Blue"));
                    if (EditorGUI.EndChangeCheck()) {
                        _serializedObj.ApplyModifiedProperties();
                        pe.UpdateTextures();
                    }
               // } else if (_colorCorrectionMode.enumValueIndex == (int)PostEffect.ColorCorrectionMode.Amplify) {
               //     EditorGUILayout.PropertyField(_CCLutTexture, new GUIContent("Lut Texture"));
              //  }
                EditorGUILayout.Space();
                EditorGUILayout.EndVertical();
            }
#endif

#if ENABLE_VIGN_GUI
            EditorGUILayout.Space();

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(_vignettingEnabled, new GUIContent("Vignetting"));
            if (EditorGUI.EndChangeCheck())
                isModuleChanged = true;
            if (_vignettingEnabled.boolValue) {
                EditorGUILayout.BeginVertical("box");
                EditorGUILayout.Space();
                EditorGUILayout.PropertyField(_vignettingIntensity, new GUIContent("Intensity"));
                EditorGUILayout.Space();
                EditorGUILayout.EndVertical();
            }
#endif

#if ENABLE_SAT_GUI
            EditorGUILayout.Space();

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(_saturationEnable, new GUIContent("Saturation"));
            if (EditorGUI.EndChangeCheck())
                isModuleChanged = true;
            if (_saturationEnable.boolValue) {
                EditorGUILayout.BeginVertical("box");
                EditorGUILayout.Space();
                EditorGUILayout.PropertyField(_saturation, new GUIContent("Saturation"));
                EditorGUILayout.Space();
                EditorGUILayout.EndVertical();
            }
#endif


#if ENABLE_BRI_GUI
            if (Application.isPlaying) {
                EditorGUILayout.Space();

                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(_brightnessEnable, new GUIContent("Brightness"));
                if (EditorGUI.EndChangeCheck())
                    isModuleChanged = true;
                if (_brightnessEnable.boolValue) {
                    EditorGUILayout.BeginVertical("box");
                    EditorGUILayout.Space();
                    EditorGUILayout.PropertyField(_brightness, new GUIContent("Brightness"));
                    EditorGUILayout.Space();
                    EditorGUILayout.EndVertical();
                }
            }
#endif

            _serializedObj.ApplyModifiedProperties();

            if (isModuleChanged) {
                pe.EnableShaderKeywords();
            }
        }

#if ENABLE_SS_GUI
        void OnSceneGUI() {
            if ((target as PostEffect).SunShaftsEnabled) {               
                if (_showCrosshair) {
                    Camera editorCamera = SceneView.currentDrawingSceneView.camera;

                    float halfViewWidth = editorCamera.pixelWidth / 2;
                    float halfViewHeight = editorCamera.pixelHeight / 2;

                    Handles.color = Color.green;

                    Vector3 p1 = editorCamera.ScreenToWorldPoint(new Vector3(halfViewWidth - 15, halfViewHeight, editorCamera.nearClipPlane + 0.2f));
                    Vector3 p2 = editorCamera.ScreenToWorldPoint(new Vector3(halfViewWidth + 15, halfViewHeight, editorCamera.nearClipPlane + 0.2f));
                    Handles.DrawLine(p1, p2);

                    p1 = editorCamera.ScreenToWorldPoint(new Vector3(halfViewWidth, halfViewHeight - 15, editorCamera.nearClipPlane + 0.2f));
                    p2 = editorCamera.ScreenToWorldPoint(new Vector3(halfViewWidth, halfViewHeight + 15, editorCamera.nearClipPlane + 0.2f));
                    Handles.DrawLine(p1, p2);
                }
            }

        }
#endif

    }
}

