﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using UnityEditor;

namespace InspectorSetting
{
    [CustomEditor(typeof(BeInspectedBehaviour))]
    public class GameLogicInspector : UnityEditor.Editor
    {
        private static string SPACE = "    ";

        public override void OnInspectorGUI()
        {
            if (serializedObject == null)return;

            var target = serializedObject.targetObject as BeInspectedBehaviour;
            var beInspectedObject = target.beInspectedObject;
            var insData = beInspectedObject.GetInspectingData();
            for (int i = 0; i < insData.Count; i++)
            {
                DrawInspectingData(insData[i]);
            }
        }

        private void DrawInspectingData(InspectingData inspectinData, string space = "")
        {
            var oldColor = GUI.contentColor;
            GUI.contentColor = inspectinData.color;
            if (inspectinData.type.IsGenericType)
            {
                EditorGUILayout.LabelField(space + inspectinData.title + ":");
                if (inspectinData.type == typeof(Dictionary<string, string>))
                {
                    var dValue = inspectinData.value as Dictionary<string, string>;
                    var den = dValue.GetEnumerator();
                    while (den.MoveNext())
                    {
                        EditorGUILayout.LabelField(space + SPACE + den.Current.Key + ":" + den.Current.Value);
                    }
                }
                else if (inspectinData.type == typeof(List<string>))
                {
                    var lValue = inspectinData.value as List<string>;
                    var len = lValue.GetEnumerator();
                    while (len.MoveNext())
                    {
                        EditorGUILayout.LabelField(space + SPACE + len.Current);
                    }
                }
            }
            else
            {
                EditorGUILayout.LabelField(space + SPACE + inspectinData.title + ":" + inspectinData.value.ToString());
            }
            GUI.contentColor = oldColor;
            if (inspectinData.children != null) 
            {
                for (int i = 0; i < inspectinData.children.Count; i++)
                {
                    DrawInspectingData(inspectinData.children[i], SPACE);
                }
            }
        }
        
    }
}
