Shader "XGame/MOGO2/Environment/VirtualGloss_PerVertex_Additive_Lightmap" 
{
	Properties 
	{
		_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
		_GlossTex ("Gloss (RGB)", 2D) = "white" {}
		
		_SpecLightDirX ("Specular Light Dir X", Range(-1, 1)) = 1
		_SpecLightDirY ("Specular Light Dir Y", Range(-1, 1)) = 1
		_SpecLightDirZ ("Specular Light Dir Z", Range(-1, 1)) = 1
		
		_SpecularColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_SpecIntensity ("Specular Intensity", Range (0.0, 10.0)) = 1.5
		_Shininess ("Shininess", Range (0.01, 1)) = 0.078125
		_ScrollingSpeed("Scrolling speed", Vector) = (0,0,0,0)

		_VestColor ("Vest Color", Color) = (1.0, 1.0, 1.0, 1.0)
	}
        
	SubShader 
	{
	    Tags { "RenderType" = "Opaque" "NormalDepth" = "VVGD_NormalDepth" }
	    LOD 300

		// ---- forward rendering base pass: -------//
		Pass 
		{
			Name "FORWARD"
			Tags { "LightMode" = "ForwardBase" }

			CGPROGRAM
			// compile directives
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
			#pragma multi_compile_fog
			#include "HLSLSupport.cginc"
			#include "UnityShaderVariables.cginc"
			#define UNITY_PASS_FORWARDBASE
			#include "../Mogo2Include.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			//#pragma multi_compile LIGHTNING_OFF LIGHTNING_ON
			//#if defined (LIGHTNING_ON)
				//#define ENABLE_LIGHTNING 1
			//#endif

			#define INTERNAL_DATA
			#define WorldReflectionVector(data,normal) data.worldRefl
			#define WorldNormalVector(data,normal) normal

			// Original surface shader snippet:
			#line 12 ""
			#ifdef DUMMY_PREPROCESSOR_TO_WORK_AROUND_HLSL_COMPILER_LINE_HANDLING
			#endif


            sampler2D _MainTex;
			float4 _MainTex_ST;
			
			sampler2D _GlossTex;

			//samplerCUBE _ReflTex;
		
			#ifndef LIGHTMAP_OFF
				// float4 unity_LightmapST;
				// sampler2D unity_Lightmap;
			#endif

			float _SpecLightDirX;
			float _SpecLightDirY;
			float _SpecLightDirZ;
			fixed3 _SpecularColor;
			float _SpecIntensity;
			float _Shininess;
			float4 _ScrollingSpeed;
			fixed3 _VestColor;
			
			struct v2f 
			{
				float4 pos 	: SV_POSITION;
				half2 uv 	: TEXCOORD0;
				
			#ifndef LIGHTMAP_OFF
				half2 lmap : TEXCOORD1;
			#endif
				
				fixed4 spec : TEXCOORD2;		// w is the depth [0-1]
				
				LIGHTING_COORDS(3,4)
				UNITY_FOG_COORDS(5)
				
				//fixed4 fogParam : TEXCOORD4;

			//#ifdef ENABLE_LIGHTNING
			//	fixed3 lightning : TEXCOORD5;
			//#endif
			};
			
			// vertex shader
			v2f vert (appdata_full v) 
			{
			  	v2f o;

			#if 1
				o.pos = UnityObjectToClipPos(v.vertex);
			#else
				float4x4 t = transpose( UNITY_MATRIX_MVP );
				o.pos  = t[2] * v.vertex.z + t[3];
				o.pos += t[0] * v.vertex.x;
				o.pos += t[1] * v.vertex.y;
			#endif
				
				o.uv = v.texcoord * _MainTex_ST.xy + _MainTex_ST.zw + frac(_ScrollingSpeed * _Time.y);
		
				float3 worldN = UnityObjectToWorldNormal(v.normal);
				float4 worldP = mul( unity_ObjectToWorld, v.vertex);
				float3 worldV = normalize( _WorldSpaceCameraPos - worldP );
				float3 worldL = ( float3( _SpecLightDirX,_SpecLightDirY,_SpecLightDirZ ) );
				float3 h = ( worldV + worldL ) * 0.5;

				o.spec.rgb = _SpecularColor * pow( saturate( dot( worldN, normalize(h) ) ), _Shininess * 128 ) * _SpecIntensity;//saturate( dot( worldL, worldN ) );
								
				// the depth 
				o.spec.w = abs( (1 - clamp(-mul(UNITY_MATRIX_MV, v.vertex).z / _DepthFar,0,2)) * _DOFApature);//saturate( -mul(UNITY_MATRIX_MV, v.vertex).z / 20.0 );
				
			#ifndef LIGHTMAP_OFF
				o.lmap = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
			#endif

			  	// pass lighting information to pixel shader
			  	TRANSFER_VERTEX_TO_FRAGMENT(o);
			  	
				//o.fogParam = ComputeFogParam(o.pos.w, worldP.xyz);

			#if ENABLE_LIGHTNING
				o.lightning = Lightning(worldN);
			#endif

				UNITY_TRANSFER_FOG(o, o.pos);
			  	
			  	return o;
			}
		
			
			// fragment shader
			fixed4 frag (v2f IN) : SV_Target 
			{
			  	fixed4 c = tex2D (_MainTex, IN.uv);
			  	
			  	fixed3 gloss = tex2D( _GlossTex, IN.uv );
			  	
			  	// lightmaps:
			  	#ifndef LIGHTMAP_OFF
					fixed3 lightInfo = DecodeLightmap (UNITY_SAMPLE_TEX2D(unity_Lightmap, IN.lmap));
				#else
					fixed3 lightInfo = 1;
				#endif
				    
		    	// combine lightmaps with realtime shadows
			    #ifdef SHADOWS_SCREEN
			      	lightInfo = min(lightInfo, LIGHT_ATTENUATION(IN) * 2);
			    #endif // SHADOWS_SCREEN
			  	
				#if ENABLE_LIGHTNING
					c.rgb *= ( 1 + IN.spec.rgb * gloss + IN.lightning.rgb ) * lightInfo;
				#else
					c.rgb *= ( 1 + IN.spec.rgb * gloss ) * lightInfo;
				#endif

				c.rgb = c.rgb * _VestColor;

				UNITY_APPLY_FOG(IN.fogCoord, c);

			  	//c.rgb = MixFogColor(c.rgb, IN.fogParam);
			  	c.a = IN.spec.w;
			  	
			  	return saturate(c);
			}

			ENDCG

		}
    }

    SubShader 
	{
	    Tags { "RenderType" = "Opaque" "NormalDepth" = "VVGD_NormalDepth"  }
	    LOD 250

		// ---- forward rendering base pass: -------//
		Pass 
		{
			Name "FORWARD"
			Tags { "LightMode" = "ForwardBase" }

			CGPROGRAM
			// compile directives
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
			#pragma multi_compile_fog
			#include "HLSLSupport.cginc"
			#include "UnityShaderVariables.cginc"
			#define UNITY_PASS_FORWARDBASE
			
			#include "../Mogo2Include.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			#define INTERNAL_DATA
			#define WorldReflectionVector(data,normal) data.worldRefl
			#define WorldNormalVector(data,normal) normal

			// Original surface shader snippet:
			#line 12 ""
			#ifdef DUMMY_PREPROCESSOR_TO_WORK_AROUND_HLSL_COMPILER_LINE_HANDLING
			#endif


            sampler2D _MainTex;
			float4 _MainTex_ST;

			//samplerCUBE _ReflTex;
		
			#ifndef LIGHTMAP_OFF
				// float4 unity_LightmapST;
				// sampler2D unity_Lightmap;
			#endif

			float _SpecLightDirX;
			float _SpecLightDirY;
			float _SpecLightDirZ;
			fixed3 _SpecularColor;
			float _SpecIntensity;
			float _Shininess;
			float4 _ScrollingSpeed;
			
			struct v2f 
			{
				float4 pos 	: SV_POSITION;
				half2 uv 	: TEXCOORD0;
				
			#ifndef LIGHTMAP_OFF
				half2 lmap : TEXCOORD1;
			#endif
				
				fixed3 spec : TEXCOORD2;
				
				LIGHTING_COORDS(3,4)
				UNITY_FOG_COORDS(5)
			};
			
			// vertex shader
			v2f vert (appdata_full v) 
			{
			  	v2f o;

			#if 1
				o.pos = UnityObjectToClipPos(v.vertex);
			#else
				float4x4 t = transpose( UNITY_MATRIX_MVP );
				o.pos  = t[2] * v.vertex.z + t[3];
				o.pos += t[0] * v.vertex.x;
				o.pos += t[1] * v.vertex.y;
			#endif
				
				o.uv = v.texcoord * _MainTex_ST.xy + _MainTex_ST.zw + frac(_ScrollingSpeed * _Time.y);
		
			#if (defined(SHADER_API_GLES) || defined(SHADER_API_GLES3)) && defined(SHADER_API_MOBILE)
				float3 worldN = v.normal * 1.0;
				float4 worldP = v.vertex;
			#else
				float3 worldN = UnityObjectToWorldNormal(v.normal);
				float4 worldP = mul( unity_ObjectToWorld, v.vertex);
			#endif
			
				float3 worldV = normalize( _WorldSpaceCameraPos - worldP );

				float3 worldL = ( float3( _SpecLightDirX,_SpecLightDirY,_SpecLightDirZ ) );
				float3 h = ( worldV + worldL ) * 0.5;

				o.spec = _SpecularColor * pow( saturate( dot( worldN, normalize(h) ) ), _Shininess * 128 ) * _SpecIntensity;//saturate( dot( worldL, worldN ) );
		
			#ifndef LIGHTMAP_OFF
				o.lmap = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
			#endif

			  	// pass lighting information to pixel shader
			  	TRANSFER_VERTEX_TO_FRAGMENT(o);
				UNITY_TRANSFER_FOG(o, o.pos);
			  	
			  	return o;
			}
			
			// fragment shader
			fixed4 frag (v2f IN) : SV_Target 
			{
			  	fixed4 c = tex2D (_MainTex, IN.uv);
			  	
			  	// lightmaps:
			  	#ifndef LIGHTMAP_OFF
			  
					fixed3 lightInfo = DecodeLightmap (UNITY_SAMPLE_TEX2D(unity_Lightmap, IN.lmap));
				
				#else
				
					fixed3 lightInfo = 1;
					
				#endif
				    
		    	// combine lightmaps with realtime shadows
			    #ifdef SHADOWS_SCREEN
			    	
			      #if (defined(SHADER_API_GLES) || defined(SHADER_API_GLES3)) && defined(SHADER_API_MOBILE)
			      	lightInfo = min(lightInfo, LIGHT_ATTENUATION(IN) * 2);
			      #else
			      	lightInfo = min(lightInfo, LIGHT_ATTENUATION(IN) * 2);
			      #endif
			    
			    #endif // SHADOWS_SCREEN
			  	
			  	c.rgb *= ( 1 + IN.spec.rgb ) * lightInfo;
				UNITY_APPLY_FOG(IN.fogCoord, c);
			  	
			  	return c;
			}

			ENDCG
		}
    }
    
    
    Fallback "Mobile/Diffuse"
}