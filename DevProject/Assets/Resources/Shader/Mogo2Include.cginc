// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: unity_Scale shader variable was removed; replaced 'unity_Scale.w' with '1.0'


#ifndef MOGO2_CG_INCLUDED
#define MOGO2_CG_INCLUDED

//////////////////////////////////////////////////////////////////
// global headers
//////////////////////////////////////////////////////////////////

#include "UnityCG.cginc"
#include "TerrainEngine.cginc"

//////////////////////////////////////////////////////////////////
// global macros
//////////////////////////////////////////////////////////////////

#define COLORGRADING_ALPHA_THRESHOLD 181.0/255.0
#define DOF_FACTOR_LIMIT 180.0/255.0

//////////////////////////////////////////////////////////////////
// global variables
//////////////////////////////////////////////////////////////////

//float3 g_fogSectionNormal;
//float  g_fogSectionDistance;
//float  g_fogTransition;
//float3 g_fogColor1;
//float3 g_fogColor2;
//float  g_fogStart;
//float  g_fogRange;

half3  g_lightningDir;
half3  g_lightningColor;

half4	_DepthFar;
half	_DOFApature;

samplerCUBE _NormalCube;

//////////////////////////////////////////////////////////////////
// util functions
//////////////////////////////////////////////////////////////////

void WriteTangentSpaceData (appdata_full v, out half3 ts0, out half3 ts1, out half3 ts2) {
	TANGENT_SPACE_ROTATION;
	ts0 = mul(rotation, unity_ObjectToWorld[0].xyz * 1.0);
	ts1 = mul(rotation, unity_ObjectToWorld[1].xyz * 1.0);
	ts2 = mul(rotation, unity_ObjectToWorld[2].xyz * 1.0);				
}

half2 EthansFakeReflection (half4 vtx) {
	half3 worldSpace = mul(unity_ObjectToWorld, vtx).xyz;
	worldSpace = (-_WorldSpaceCameraPos * 0.6 + worldSpace) * 0.07;
	return worldSpace.xz;
}

/*
float4 ComputeFogParam(float viewZ, float3 worldXYZ)
{
	float4 fogParam;
	fogParam.w = saturate( (viewZ - g_fogStart) * g_fogRange );
	float dist = dot(worldXYZ, g_fogSectionNormal) - g_fogSectionDistance;
	float factor = clamp(dist * g_fogTransition, -1, 1) * 0.5 + 0.5;
	fogParam.rgb = lerp(g_fogColor1, g_fogColor2, factor) * fogParam.w;
	return fogParam;
}

float3 MixFogColor(float3 srcRGB, float4 fogParam)
{
	return srcRGB * (1 - fogParam.w) + fogParam.rgb;
}
*/

half3 CubeNormal(half3 n)
{
	return texCUBE(_NormalCube, n).xyz * 2 - 1;
}

fixed3 Lightning( float3 normalWS )
{
	return saturate( dot( g_lightningDir.xyz, normalWS ) ) * g_lightningColor.rgb;
}

fixed3 Multiply(in fixed3 cB, in fixed3 cS) { return cB * cS; }
fixed3 Screen(in fixed3 cB, in fixed3 cS) { return 1.0 - (1.0 - cB) * (1.0 - cS); }
fixed3 HardLight(in fixed3 cB, in fixed3 cS) { return lerp( Multiply( cB, 2.0 * cS ), Screen( cB, 2.0 * cS - 1 ), step(0.5, cS)); }
fixed3 Overlay(in fixed3 cB, in fixed3 cS) { return HardLight(cS, cB); }

//////////////////////////////////////////////////////////////////
// DOF FACTOR
//////////////////////////////////////////////////////////////////
fixed ComputeDofFactor( in half2 depthParams )
{
	//return DOF_FACTOR_LIMIT - abs( 1 - clamp( depth / _DepthFar, 0, 2 ) ) * _DOFApature;
	
	half2 depthPattern = depthParams * _DepthFar.xy;
	half  depthFactor = max( depthPattern.x,depthPattern.y );
	//return DOF_FACTOR_LIMIT - abs( 1 - clamp( depthFactor, 0, 2 ) ) * _DOFApature;
	return DOF_FACTOR_LIMIT - saturate( depthFactor - 1 ) * _DOFApature;
}

void ComputeAlphaForAdditive( inout fixed4 c )
{
	c.a *= max( c.r, max( c.g, c.b ) ); 
}

#endif