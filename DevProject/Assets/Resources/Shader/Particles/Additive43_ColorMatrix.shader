﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/MOGO2/Particles/Additive43_ColorMatrix" {
Properties {
	//_TintColor ("Tint Color", Color) = (1,1,1,1)
	_MainTex ("Particle Texture", 2D) = "white" {}
	_ZOffset ("Z Offset",float) = 0

	_ColorMatrix0 ("Color Matrix 0", Vector) = (1,0,0,0)
    _ColorMatrix1 ("Color Matrix 1", Vector) = (0,1,0,0)
    _ColorMatrix2 ("Color Matrix 2", Vector) = (0,0,1,0)
}

Category {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	Blend SrcAlpha One
	Cull Off Lighting Off ZWrite Off Fog { Color (0,0,0,0) }
	BindChannels {
		Bind "Color", color
		Bind "Vertex", vertex
		Bind "TexCoord", texcoord
	}
	
	SubShader {
		Pass {
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma multi_compile_particles

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			//fixed4 _TintColor;
			half _ZOffset;

			half4 _ColorMatrix0;
			half4 _ColorMatrix1;
			half4 _ColorMatrix2;
			
			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};
			
			float4 _MainTex_ST;

			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.vertex.z -= _ZOffset;
				
				o.color = v.color;
				o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
				
				return o;
			}

			half4 frag (v2f i) : COLOR
			{
				//return _TintColor * tex2D(_MainTex, i.texcoord) * i.color * 2.0f;

				half4 tex = tex2D(_MainTex, i.texcoord) * i.color;
				
				half4 c = tex;
				c.a = 1;

				c.rgb = half3(	dot( c, _ColorMatrix0 ),
		        				dot( c, _ColorMatrix1 ),
		        				dot( c, _ColorMatrix2 ) );

				c.a = tex.a;

				return saturate(c * 2.0);
			}
			ENDCG 
		}
	} 	
	
	SubShader {
		Pass {
			SetTexture [_MainTex] {
				constantColor [_TintColor]
				combine constant * primary
			}
			SetTexture [_MainTex] {
				combine texture * previous DOUBLE
			}
		}
	}
	
	SubShader {
		Pass {
			SetTexture [_MainTex] {
				combine texture * primary
			}
		}
	}
}
}
