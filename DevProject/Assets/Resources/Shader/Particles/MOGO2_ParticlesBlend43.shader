﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/MOGO2/Particles/MOGO2_ParticlesBlend43" {
Properties {
	_TintColor ("Tint Color", Color) = (0.5,0.5,0.5,0.5)
	_MainTex ("Particle Texture", 2D) = "white" {}
	_ZOffset ("Z Offset",float) = 0
}

Category {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	Cull Off Lighting Off ZWrite Off Fog { Color (0,0,0,0) }
		
	SubShader {
	
		LOD 300
		
		Blend SrcAlpha OneMinusSrcAlpha, One One
		BlendOp Add,Max
	
		Pass {
		
			CGPROGRAM
			//#pragma exclude_renderers gles
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
						
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			fixed4 _TintColor;
			half _ZOffset;
			
			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				
			};
			
			float4 _MainTex_ST;

			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.vertex.z -= _ZOffset;
				
				o.color = v.color;
				o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : COLOR
			{
				return tex2D(_MainTex, i.texcoord) * i.color * _TintColor * 2;
			}
			ENDCG
		}
	} 	
	
	// LOD 250
	
	
	Fallback "Particles/Alpha Blended"
}
}
