﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'


Shader "XGame/MOGO2/Particles/ShellEffectAdd"
{
	Properties
	{
		_MainTex ("Particle Texture", 2D) = "white" {}
		_NormalCube ("_NormalCube", CUBE) = "black" {}
		_TintColor ("Tint Color", Color) = (1,1,1,1)
		_ZOffset ("Z Offset",float) = 0
	}

	SubShader
	{
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Pass
		{
			Blend SrcAlpha One
			Cull Back
			Lighting Off
			ZWrite Off

			BindChannels
			{
				Bind "Color", color
				Bind "Vertex", vertex
				Bind "TexCoord", texcoord
			}
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma multi_compile_particles
			
			#include "../Mogo2Include.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _TintColor;
			half _ZOffset;
			
			struct v2f
			{
				float4 vertex : POSITION;
				half4 color : COLOR0;
				float2 texcoord : TEXCOORD0;
				half3 normal	: TEXCOORD1;
				half3 view	: TEXCOORD2;
			};
			
			v2f vert(appdata_full v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.vertex.z -= _ZOffset;
				o.color = v.color;
				o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
				o.normal = mul((half3x3)unity_ObjectToWorld, v.normal.xyz);
				o.view = _WorldSpaceCameraPos.xyz - mul(unity_ObjectToWorld, v.vertex).xyz;
				return o;
			}

			half4 frag (v2f i) : COLOR
			{
				half4 c = tex2D(_MainTex, i.texcoord);
				half3 normal = CubeNormal(i.normal);
				half3 view = CubeNormal(i.view);
				half ndv = dot(normal, view);
				ndv = 1 - ndv;
				ndv *= ndv;
				c *= _TintColor * i.color * 2.0f;
				c.a *= ndv;
				return c;
			}
			ENDCG 
		}
	}

	FallBack "Particles/Additive" 
}
