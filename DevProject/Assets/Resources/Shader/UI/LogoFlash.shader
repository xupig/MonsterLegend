﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/MOGO2/UI/LogoFlash"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "grey" {}
		_AlphaTex("AlphaTex",2D) = "grey"{}
		_LightTex("LightTex",2D) = "grey"{}
		_AlphaFactor("AlphaFactor",Float) = 1
		_LightFactor("LightFactor",Float) = 1
	}

	SubShader
	{
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			Lighting Off
			ZWrite Off
			ZTest Off
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest

			#include "../Mogo2Include.cginc"
		
			sampler2D _MainTex;
			sampler2D _LightTex;
			sampler2D _AlphaTex;
			float4 _MainTex_ST;
			half _LightFactor;
			half _AlphaFactor;
			half _Offset;

			struct v2f
			{
				float4 pos : SV_POSITION;
				half2 uv : TEXCOORD0;
				half2 lightUV : TEXCOORD1;
			};

			v2f vert(appdata_base v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.lightUV = v.texcoord;
				o.lightUV.x += _Offset;
				return o;
			}

			half4 frag (v2f i) : COLOR
			{
				half4 c = tex2D(_MainTex, i.uv);
				half4 a = tex2D(_AlphaTex, i.uv);
				half4 l = tex2D(_LightTex, i.lightUV);
				c.rgb += l.rgb * _LightFactor;
				c.a = a.r * _AlphaFactor;
				return c;
			}

			ENDCG
		}
	}

	FallBack "Mobile/Diffuse"
}
