
Shader "XGame/Aion/RainFlow" {
    Properties {
      _MainTex ("Main Texture", 2D) = "white" {}
      _BumpMap ("Normal Map", 2D) = "bump" {}
      _HeightMap ("Height Map", 2D) = "black" {}

      HeightScale ("TerrianHeightScale", Range(0.001,0.08)) = 0.02 
	  WaterHeight ("WaterHeight", Range(0,2)) = 0.5
      RainScale ("Rain scale", Range(0,1)) = 0.5           
      RainSpeed ("Rain speed", Range(0,40)) = 15
      RainIntensity ("Rain Intensity", Range(0,1)) = 1
      LightPow("LightPow",Range(0,2))=1

      _FlowMap ("FlowMap", 2D) = "grey" {}    
      FlowingMap ("FlowMap Bumps", 2D) = "gray" {}
      RippleMap ("Rain Dropplet", 2D) = "gray" {}
    }
    SubShader {
	Tags { "RenderType" = "Opaque" }
	CGPROGRAM
	#pragma surface surf PPL vertex:vert
	#pragma target 3.0	
	
	struct Input {
		float2 uv_MainTex;
		
		float3 viewDir;
		float4 _auxDir;
		
		float4 color:COLOR;
	};

	float HeightScale;

	sampler2D _MainTex;
	sampler2D _BumpMap;
	sampler2D RippleMap;
	sampler2D FlowingMap;
	
	sampler2D _HeightMap;
	sampler2D _FlowMap;

	
	float RainSpeed;
	float RainIntensity;

	float RainScale;

	float WaterHeight;
	float LightPow;
    half4 LightingPPL(SurfaceOutput s,half3 viewDir,UnityGI gi)
    {
				half3 lightDir = gi.light.dir;
				half3 h = normalize(lightDir+viewDir);
				half diff = max(0,dot(s.Normal,lightDir));
				float nh = max(0,dot(s.Normal,h));
				float spec = pow(nh,s.Specular*128.0)*s.Gloss;
				half4 c;
				//c.rgb = (s.Albedo*gi.light.color.rgb*diff + gi.light.color.rgb*spec)*(0.5*2);
				c.rgb = s.Albedo*LightPow;
				//c.a = s.Alpha;
				return c;
			//return LightingBlinnPhong(s, viewDir, gi);
	} 

	inline void LightingPPL_GI(
                SurfaceOutput s,
                UnityGIInput data,
                inout UnityGI gi)
    {
                gi = UnityGI_Base(data, 0.5, s.Normal);
     //            	UnityGI o_gi;
					// ResetUnityGI(o_gi);
					// o_gi.light = data.light;
					// o_gi.light.color *= data.atten;
					// gi = o_gi;
    }

	inline float2 GetRipple(float2 UV, float Intensity)
	{
	    float4 Ripple = tex2D(RippleMap, UV);
	    Ripple.xy = Ripple.xy * 2 - 1;
	
	    float DropFrac = frac(Ripple.w + _Time.x*RainSpeed);
	    float TimeFrac = DropFrac - 1.0f + Ripple.z;
	    float DropFactor = saturate(0.2f + Intensity * 0.8f - DropFrac);
	    float FinalFactor = DropFactor * Ripple.z * sin( clamp(TimeFrac * 9.0f, 0.0f, 3.0f) * 3.1415);
	    
	    return Ripple.xy * FinalFactor * 0.35f;
	}
	
	void vert (inout appdata_full v, out Input o) {
		UNITY_INITIALIZE_OUTPUT(Input, o);

		float3 binormal = cross( v.normal, v.tangent.xyz ) * v.tangent.w;
		float3x3 rotation = float3x3( v.tangent.xyz, binormal, v.normal.xyz );		
		
		float3 viewDir = -ObjSpaceViewDir(v.vertex);
		float3 viewRefl = reflect (viewDir, v.normal);
		float2 refl_vec = normalize(mul((float3x3)unity_ObjectToWorld, viewRefl)).xz;
		float3 refl_rot;
		refl_rot.x=sin(_Time.x);
		refl_rot.y=cos(_Time.x);
		refl_rot.z=-refl_rot.x;
		o._auxDir.x=dot(refl_vec, refl_rot.yz);
		o._auxDir.y=dot(refl_vec, refl_rot.xy);
		o._auxDir.xy=o._auxDir.xy*0.5+0.5;

		o._auxDir.zw = ( mul (rotation, mul(unity_WorldToObject, float4(0,1,0,0)).xyz) ).xy;		
	}
	
	void surf (Input IN, inout SurfaceOutput o) {
      	float3 rayPos;
      	rayPos.z=tex2D(_HeightMap, IN.uv_MainTex).a;
      	rayPos.xy=IN.uv_MainTex + ParallaxOffset(rayPos.z, HeightScale, IN.viewDir.xyz);    	
		
		float p = saturate(WaterHeight-rayPos.z);

		float2 flowUV=lerp(IN.uv_MainTex, rayPos.xy, 1-p*0.5);
		float _Tim=frac(_Time.x*4)*2;
		float ft=abs(frac(_Tim)*2 - 1);
		float2 flowSpeed=clamp((IN._auxDir.zw)*4,-1,1);
		flowSpeed+=(tex2D(_FlowMap, flowUV).rg*2-1)/10;
		float2 flowOffset=tex2D(FlowingMap, flowUV+frac(_Tim.xx)*flowSpeed).ag*2-1;
		flowOffset=lerp(flowOffset, tex2D(FlowingMap, flowUV+frac(_Tim.xx+0.5)*flowSpeed*1.1).ag*2-1, ft);
		flowOffset*=max(p, 0);

		float2 rippleUV = IN.uv_MainTex*(1-RainScale) + flowOffset*0.1*flowSpeed;
		float2 roff = GetRipple( rippleUV, RainIntensity);
	    roff += GetRipple( rippleUV+float2(0.25,0.25), RainIntensity);
		roff*=4;
		roff+=flowOffset;
		rayPos.xy+=0.02*roff;
		
      	fixed4 col = tex2D (_MainTex, rayPos.xy);       
        o.Normal = UnpackNormal (tex2D (_BumpMap, rayPos.xy));
        o.Normal.xy+=roff;
        o.Normal=normalize(o.Normal); 		
                
        o.Albedo = col.rgb;
	}
	ENDCG
      
    } 
    Fallback "Diffuse"
}
