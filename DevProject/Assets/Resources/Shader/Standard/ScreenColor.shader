// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/Aion/ScreenColor"{
	Properties{
		_Color("Color", Color) = (1,1,1,0)
	}

	SubShader {
		ZTest Always Zwrite Off Cull Off 

		Pass {
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert 
			#pragma fragment frag 
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"

			fixed4 _Color;
			struct v2f {
				float4 pos : POSITION;
			};

			v2f vert(appdata_base i) {
				v2f o;
				o.pos = UnityObjectToClipPos(i.vertex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target {
				return _Color;
			}
			ENDCG
		} 
	}
}