// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: commented out 'half4 unity_LightmapST', a built-in variable
// Upgrade NOTE: commented out 'sampler2D unity_Lightmap', a built-in variable
// Upgrade NOTE: replaced tex2D unity_Lightmap with UNITY_SAMPLE_TEX2D


Shader "XGame/MOGO2/Environment/ModelTerrain"
{
	Properties
	{
		_Splat0 ("Layer 1", 2D) = "white" {}
		_Splat1 ("Layer 2", 2D) = "white" {}
		_Splat2 ("Layer 3", 2D) = "white" {}
		_Control ("Control (RGB)", 2D) = "grey" {}
	}
                
	SubShader
	{
		Tags { "Queue"="Geometry+3" "SplatCount" = "3" "RenderType" = "Opaque" }
		Fog { Mode Off }

		Pass
		{
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog 
			#pragma fragmentoption ARB_precision_hint_fastest
			
			#include "../Mogo2Include.cginc"

			sampler2D _Control;
			half4 _Control_ST;
			sampler2D _Splat0;
			half4 _Splat0_ST;
			sampler2D _Splat1;
			half4 _Splat1_ST;
			sampler2D _Splat2;
			half4 _Splat2_ST;
			// sampler2D unity_Lightmap;
			// half4 unity_LightmapST;

			struct v2f 
			{
				float4 pos : SV_POSITION;
				half2 control : TEXCOORD0;
				half2 splatUV[3]: TEXCOORD1;
				half2 lmap : TEXCOORD4;
				UNITY_FOG_COORDS(5)
			};

			v2f vert (appdata_full v) 
			{
				v2f o;

				o.pos = UnityObjectToClipPos(v.vertex);

				o.control = TRANSFORM_TEX(v.texcoord, _Control);
				o.splatUV[0] = TRANSFORM_TEX(v.texcoord, _Splat0);
				o.splatUV[1] = TRANSFORM_TEX(v.texcoord, _Splat1);
				o.splatUV[2] = TRANSFORM_TEX(v.texcoord, _Splat2);
				o.lmap = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
		
				UNITY_TRANSFER_FOG(o, o.pos);
			  	
				return o;
			}
		
			fixed4 frag (v2f i) : COLOR 
			{
				fixed4 control = tex2D (_Control, i.control);
				fixed4 c0 = tex2D (_Splat0, i.splatUV[0]);
				fixed4 c1 = tex2D (_Splat1, i.splatUV[1]);
				fixed4 c2 = tex2D (_Splat2, i.splatUV[2]);

				fixed4 c = c0 * control.r + c1 * control.g + c2 * control.b;
				c.rgb *= DecodeLightmap(UNITY_SAMPLE_TEX2D(unity_Lightmap, i.lmap));
				c = saturate(c);
				UNITY_APPLY_FOG(i.fogCoord, c);
			  	
				return c;
			}
			ENDCG
		}
	}

	Fallback "Mobile/Diffuse"
}
