Shader "XGame/MOGO2/Environment/FlowFakeReflect"
{
	
	Properties
	{
		_MainTex ("MainTex", 2D) = "black" {}
		_Reflect("Reflect", 2D) = "White" {}
		_ReflectHeight("ReflectHeight", float) = 50
		_ReflectScale("ReflectScale", float) = 0.01
		_FlowSpeedX("FlowSpeedX", float) = -10
		_FlowSpeedZ("FlowSpeedZ", float) = 0
	}

	SubShader
	{
		Tags { "RenderType"="Opaque" }

		LOD 300 

		Pass
		{
			CGPROGRAM
		
			#include "../Mogo2Include.cginc"

			sampler2D _MainTex;
			sampler2D _Reflect;
			// sampler2D unity_Lightmap;
			// float4 unity_LightmapST;
			half _ReflectHeight;
			half _ReflectScale;
			half _FlowSpeedX;
			half _FlowSpeedZ;

			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog 
			#pragma fragmentoption ARB_precision_hint_fastest
			//#pragma multi_compile LIGHTNING_OFF LIGHTNING_ON

			#if defined (LIGHTNING_ON)
				#define ENABLE_LIGHTNING 1
			#endif

			struct v2f
			{
				half4 pos : POSITION ;
				half2 uv : TEXCOORD0;
				half2 reflectUv : TEXCOORD1;
				//fixed4 fogParam : TEXCOORD2;
				UNITY_FOG_COORDS(2)
				#ifdef LIGHTMAP_ON
					half2 uvLM : TEXCOORD3;
				#endif
				#ifdef ENABLE_LIGHTNING
					fixed3 lightning : TEXCOORD4;
				#endif	
			};

			fixed _t = 0.0;
			v2f vert(appdata_full v) 
			{
				v2f o;
				o.pos = UnityObjectToClipPos (v.vertex);
				o.uv = v.texcoord;

				float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
				float3 cp = worldPos.xyz - _WorldSpaceCameraPos.xyz;
				_t = modf(_Time.y, _t);
				o.reflectUv = (worldPos.xz - _ReflectHeight * cp.xz / cp.y + half2(_FlowSpeedX, _FlowSpeedZ) * _t) * _ReflectScale;

				#ifdef LIGHTMAP_ON
					o.uvLM = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
				#endif

				#if ENABLE_LIGHTNING
					float3 worldN = mul( (float3x3)unity_ObjectToWorld, v.normal * 1.0 );
					o.lightning = Lightning(worldN);
				#endif

				//o.fogParam = ComputeFogParam(o.pos.w, worldPos.xyz);
				UNITY_TRANSFER_FOG(o, o.pos);
				
				return o; 
			}
				
			fixed4 frag(v2f i) : COLOR0 
			{
				fixed4 tex = tex2D(_MainTex, i.uv);
				fixed4 rfl = tex2D(_Reflect, i.reflectUv);

				#ifdef LIGHTMAP_ON
					fixed3 lt = ( DecodeLightmap (UNITY_SAMPLE_TEX2D(unity_Lightmap, i.uvLM)));
				#else
					fixed3 lt = 1;
				#endif

				#if ENABLE_LIGHTNING
					lt = lt + i.lightning.rgb * lt;
				#endif

				tex.rgb  = lt * tex.rgb + rfl.rgb;

				tex = saturate(tex);

				//tex.rgb = MixFogColor(tex.rgb, i.fogParam);
				UNITY_APPLY_FOG(i.fogCoord, tex);
			
				return tex;	
			}
	
			ENDCG
		}
	}

	FallBack "Mobile/Diffuse"
}
