// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: commented out 'float4 unity_LightmapST', a built-in variable
// Upgrade NOTE: commented out 'sampler2D unity_Lightmap', a built-in variable
// Upgrade NOTE: replaced tex2D unity_Lightmap with UNITY_SAMPLE_TEX2D


Shader "XGame/MOGO2/Environment/FakeReflect"
{
	Properties
	{
		_MainTex ("MainTex", 2D) = "black" {}
		_Reflect("Reflect", 2D) = "grey" {}
		_ReflectHeight("ReflectHeight", float) = 50
		_ReflectScale("ReflectScale", float) = 0.01
		_ReflectStreng("ReflectStreng", float) = 1
		_MaxReflect("MaxReflect", Range(0, 1)) = 0.3
	}

	SubShader
	{
		Tags { "RenderType"="Opaque" }

		LOD 300 

		Pass
		{
			CGPROGRAM
		
			#include "../Mogo2Include.cginc"

			sampler2D _MainTex;
			sampler2D _Reflect;
			half _ReflectHeight;
			half _ReflectScale;
			half _ReflectStreng;
			half _MaxReflect;
			#ifndef LIGHTMAP_OFF
				// sampler2D unity_Lightmap;
				// float4 unity_LightmapST;
			#endif

			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog 
			#pragma fragmentoption ARB_precision_hint_fastest

			struct v2f
			{
				half4 pos : POSITION ;
				half2 uv : TEXCOORD0;
				half2 reflectUv : TEXCOORD1;
				half view : TEXCOORD2;
				//fixed4 fogParam : TEXCOORD3;
				UNITY_FOG_COORDS(3)
				#ifndef LIGHTMAP_OFF
					half2 uvLM : TEXCOORD4;
				#endif
			};
		
			v2f vert(appdata_full v) 
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord;

				float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
				half3 cp = worldPos.xyz - _WorldSpaceCameraPos.xyz;
				o.reflectUv = (worldPos.xz - _ReflectHeight * cp.xz / cp.y) * _ReflectScale;
				cp = normalize(cp);
				o.view = 1 + cp.y;

				#ifndef LIGHTMAP_OFF
					o.uvLM = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
				#endif

				//o.fogParam = ComputeFogParam(o.pos.w, worldPos.xyz);
				UNITY_TRANSFER_FOG(o, o.pos);
				
				return o; 
			}
				
			fixed4 frag(v2f i) : COLOR0 
			{
				fixed4 tex = tex2D(_MainTex, i.uv);
				fixed4 rfl = tex2D(_Reflect, i.reflectUv);

				#ifndef LIGHTMAP_OFF
					fixed3 lt = DecodeLightmap(UNITY_SAMPLE_TEX2D(unity_Lightmap, i.uvLM));
				#else
					fixed3 lt = 1;
				#endif

				half fnl = i.view * i.view;
				fnl *= fnl;
				fnl = min(_MaxReflect, fnl);
				tex.rgb = lt * tex.rgb * (1 - fnl) + _ReflectStreng * rfl.rgb * fnl;
				tex = saturate(tex);
				UNITY_APPLY_FOG(i.fogCoord, tex);
			
				return tex;	
			}
	
			ENDCG
		}
	}

	FallBack "Mobile/Diffuse"
}
