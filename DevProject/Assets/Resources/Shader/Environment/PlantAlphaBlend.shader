﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: commented out 'float4 unity_LightmapST', a built-in variable
// Upgrade NOTE: commented out 'sampler2D unity_Lightmap', a built-in variable
// Upgrade NOTE: replaced tex2D unity_Lightmap with UNITY_SAMPLE_TEX2D

Shader "XGame/MOGO2/Environment/PlantAlphaBlend"
{
	Properties
	{
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_Color ("Main Color", Color) = (1,1,1,1)
	}

	SubShader
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		LOD 200
		
		Pass 
		{
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog

			#include "../Mogo2Include.cginc"
		
			sampler2D _MainTex;
			float4 _MainTex_ST;
			half4 _Color;
		
			#ifndef LIGHTMAP_OFF
				// sampler2D unity_Lightmap;
				// float4 unity_LightmapST;
			#endif

			struct v2f 
			{
				float4 pos : SV_POSITION;
				half2 uv : TEXCOORD0;
			#ifndef LIGHTMAP_OFF
				half2 litMap : TEXCOORD1;
			#endif
				UNITY_FOG_COORDS(2)
			};

			v2f vert (appdata_full v) 
			{
				v2f o;
			
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
			
				#ifndef LIGHTMAP_OFF
					o.litMap = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
				#endif

				//float4 worldP = mul(unity_ObjectToWorld, v.vertex);
				
				//o.fogParam = ComputeFogParam(o.pos.w, worldP.xyz);
				UNITY_TRANSFER_FOG(o, o.pos);

				return o;
			}

			half4 frag(v2f i) : COLOR
			{
				half4 c = tex2D (_MainTex, i.uv);
				c *= _Color;
			  	
				#ifndef LIGHTMAP_OFF
					c.rgb *= DecodeLightmap(UNITY_SAMPLE_TEX2D(unity_Lightmap, i.litMap)).rgb;
				#endif
				    
				//c.rgb = MixFogColor(c.rgb, i.fogParam);
				UNITY_APPLY_FOG(i.fogCoord, c);
			  	
				return c;
			}

			ENDCG
		}
	}

	FallBack "Transparent/Diffuse"
}
