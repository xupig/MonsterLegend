// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "XGame/MOGO2/Environment/Sky2CloudColorBack"
{
	Properties
	{
		_MainTex("_MainTex", 2D) = "blue" {}
		_Repeat1("_Repeat1", float) = 1
		_SpeedU1("_SpeedU1", float) = 0.5
		_SpeedV1("_SpeedV1", float) = 0
		_Cloud2("_Cloud2", 2D) = "grey" {}
		_Repeat2("_Repeat2", float) = 1
		_SpeedU2("_SpeedU2", float) = -0.5
		_SpeedV2("_SpeedV2", float) = 0
		_BackColor("_BackColor", Color) = (0, 0.5, 1, 1)
		_MMultiplier("_MMultiplier", float) = 6
		_Alpha("Alpha",Range(0,1))=1
	}

	SubShader
	{
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		//ZWrite Off
		Lighting Off		
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha
		Pass
		{
			ZTest LEqual Cull Off ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			
			#include "../Mogo2Include.cginc"

			struct appdata_t
			{
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};
	
			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv1: TEXCOORD0;
				float2 uv2: TEXCOORD1;
			};

			sampler2D _MainTex;
			sampler2D _Cloud2;
			float _Repeat1;
			float _Repeat2;
			float _SpeedU1;
			float _SpeedV1;
			float _SpeedU2;
			float _SpeedV2;
			half3 _BackColor;
			half _MMultiplier;
			float _Alpha;
			v2f vert (appdata_t v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv1 = v.texcoord * _Repeat1 + float2(_SpeedU1, _SpeedV1) * _Time.y;
				o.uv2 = v.texcoord * _Repeat2 + float2(_SpeedU2, _SpeedV2) * _Time.y;
				return o;
			}

			fixed4 frag (v2f i) : COLOR
			{
				fixed4 tex1 = tex2D(_MainTex, i.uv1);
				fixed4 tex2 = tex2D(_Cloud2, i.uv2);
				fixed4 c;
				c.rgb = _BackColor + _BackColor * tex1.rgb * tex2.rgb * _MMultiplier;
				c.a = _Alpha;
				return saturate(c);
			}

			ENDCG
		}
	}
	
	FallBack "Mobile/Diffuse"
}