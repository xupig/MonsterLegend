﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "XGame/MOGO2/PostProcess/Downsample4x"
{
	Properties
	{
		_MainTex ("MainTex", 2D) = "white" {}
	}

	SubShader
	{
		Pass
		{
			Cull Off
			ZTest Always
			ZWrite Off

			CGPROGRAM
			
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma vertex vert
			#pragma fragment frag
			
			#include "../Mogo2Include.cginc"

			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			sampler2D _MainTex;
			float2 _Offset;

			v2f vert(appdata_full v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord;
				#ifdef UNITY_HALF_TEXEL_OFFSET
				o.uv.y += _Offset.y * 2;
				#endif
				return o;
			}

			half4 frag(v2f i) : COLOR
			{
				half4 c;
				c.rgb = tex2D(_MainTex, i.uv + _Offset).rgb;
				c.rgb += tex2D(_MainTex, i.uv - _Offset).rgb;
				c.rgb += tex2D(_MainTex, i.uv + float2(-_Offset.x, _Offset.y)).rgb;
				c.rgb += tex2D(_MainTex, i.uv + float2(_Offset.x, -_Offset.y)).rgb;
				c.rgb *= 0.25;
				c.a = 1;
				return c;
			}

			ENDCG
		}
	}

	FallBack off
}
