﻿Shader "Hidden/XGame/ShadowOnly" {

	CGINCLUDE
	#include "UnityCG.cginc"
	uniform float4		_MainTex_ST;
	uniform fixed		_Cutoff;
	uniform sampler2D	_MainTex;

	struct a2v {
		float4	vertex	: POSITION;
		float2	uv		: TEXCOORD0;
	};
	
	struct v2f {
		float4	pos	: SV_POSITION;
		half2	uv	: TEXCOORD0;
	};

	v2f vert(a2v v) {
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		o.uv = v.uv * _MainTex_ST.xy + _MainTex_ST.zw;
		return o;
	}
	
	fixed4 frag(v2f i) : SV_Target {
		return fixed4(0, 0, 0, 1);
	}
	
	fixed4 fragT(v2f i) : SV_Target{
		fixed a = tex2D(_MainTex, i.uv).a;
		clip(a - 0.5);
		return fixed4(0, 0, 0, a);
	}
	ENDCG


	SubShader {
		Tags { "RenderType" = "Opaque" }
		//Offset 1, 0
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			ENDCG
		}
	}

	SubShader {
		Tags { "RenderType" = "TransparentCutout" }
		//Offset 1, 0
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment fragT
			ENDCG
		}
	}

	SubShader {
		Tags { "RenderType" = "Transparent" }
		//Offset 1, 0
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment fragT
			ENDCG
		}
	}
}
