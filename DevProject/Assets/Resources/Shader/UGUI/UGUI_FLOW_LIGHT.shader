Shader "UGUI/UGUI_FLOW_LIGHT"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _AlphaTex ("Alpha Texture", 2D) = "white" {}

        _Color ("Tint", Color) = (1, 1, 1, 1)
        [MaterialToggle] PixelSnap ("Pixel snap", float) = 0
        
        _FlowlightTex ("Flowlight Texture", 2D) = "white" {}
        _Power ("Power", float) = 1
        _SpeedX ("SpeedX", float) = 1
        _SpeedY ("SpeedY", float) = 0

        _StencilComp ("Stencil Comparison", Float) = 8
        _Stencil ("Stencil ID", Float) = 0
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilReadMask ("Stencil Read Mask", Float) = 255

        _ColorMask ("Color Mask", Float) = 15

        //_UvRect ("UV Rect", Vector) = (0,0,0,0)
     }
 
    SubShader
    {
        Tags
        { 
            "Queue"="Transparent" 
            "IgnoreProjector"="True" 
            "RenderType"="Transparent" 
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }
 
        Stencil
        {
            Ref [_Stencil]
            Comp [_StencilComp]
            Pass [_StencilOp] 
            ReadMask [_StencilReadMask]
            WriteMask [_StencilWriteMask]
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha
        ColorMask [_ColorMask]
 
        Pass
        {
        CGPROGRAM        
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            
            struct appdata_t
            {
                float4 vertex : POSITION;
                float4 color : COLOR;
                float2 texcoord : TEXCOORD0;   
                float2 texcoord2 : TEXCOORD1;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
                half2 texcoord : TEXCOORD0;
                float2 texflow : TEXCOORD1;
            };
            
            fixed4 _Color;
            sampler2D _FlowlightTex;
            fixed4 _FlowlightTex_ST;
            fixed _SpeedX;
            fixed _SpeedY;
            //half4 _UvRect;  

            fixed _t = 0.0;
            v2f vert(appdata_t IN)
            {
                v2f OUT;
                _t = modf(_Time.x, _t);
                OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
                OUT.texcoord = IN.texcoord;
                //OUT.texflow = TRANSFORM_TEX(IN.texcoord, _FlowlightTex);				
                //OUT.texflow.x = (OUT.texflow.x - _UvRect.x) / (_UvRect.z - _UvRect.x) + _t * _SpeedX;  
                //OUT.texflow.y = (OUT.texflow.y - _UvRect.y) / (_UvRect.w - _UvRect.y) + _t * _SpeedY;
				//OUT.texflow = TRANSFORM_TEX(half2((IN.vertex.x - _UvRect.x) / _UvRect.z, (IN.vertex.y - _UvRect.y) / _UvRect.w), _FlowlightTex);
				OUT.texflow = TRANSFORM_TEX(IN.texcoord2, _FlowlightTex);
				OUT.texflow.x += _t.x * _SpeedX;
				OUT.texflow.y += _t.x * _SpeedY;

                OUT.color = IN.color * _Color;
                #ifdef PIXELSNAP_ON
                OUT.vertex = UnityPixelSnap (OUT.vertex);
                #endif

                return OUT;
            }

            sampler2D _MainTex;
            sampler2D _AlphaTex;
            float _Power;

            fixed4 frag(v2f IN) : SV_Target
            {
                fixed4 c = tex2D(_MainTex, IN.texcoord);
                half4 acolor = tex2D(_AlphaTex,IN.texcoord);
                c.a *= acolor.r;
                fixed4 cadd = tex2D(_FlowlightTex, IN.texflow) * _Power;
                cadd.rgb *= c.rgb;
                c.rgb += cadd.rgb;
                c.rgb *= c.a;
                return c;
            }
        ENDCG
        }
    }
}