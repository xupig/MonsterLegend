Shader "XGame/Shader Forge/sf_uv_3_chanel_blend" {
    Properties {
        _TintColor ("Color", Color) = (0.9852941,0.8259083,0.8680986,1)
        _tex_R ("tex_R", 2D) = "white" {}
        _R_Uspeed ("R_Uspeed", Float ) = 0
        _R_Vspeed ("R_Vspeed", Float ) = 0
        _tex_G ("tex_G", 2D) = "white" {}
        _G_Uspeed ("G_Uspeed", Float ) = 0
        _G_Vspeed ("G_Vspeed", Float ) = 0
        _tex_B ("tex_B", 2D) = "white" {}
        _B_Uspeed ("B_Uspeed", Float ) = 0
        _B_Vspeed ("B_Vspeed", Float ) = 0
        _Strengh ("Strengh", Float ) = 10
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform sampler2D _tex_R; uniform float4 _tex_R_ST;
            uniform float4 _TintColor;
            uniform sampler2D _tex_G; uniform float4 _tex_G_ST;
            uniform sampler2D _tex_B; uniform float4 _tex_B_ST;
            uniform float _Strengh;
            uniform float _R_Uspeed;
            uniform float _R_Vspeed;
            uniform float _G_Uspeed;
            uniform float _G_Vspeed;
            uniform float _B_Uspeed;
            uniform float _B_Vspeed;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 node_412 = _Time;
                float2 node_8263 = (i.uv0+(float2(_R_Uspeed,_R_Vspeed)*node_412.g));
                float4 _tex_R_var = tex2D(_tex_R,TRANSFORM_TEX(node_8263, _tex_R));
                float4 node_9759 = _Time;
                float2 node_3788 = ((i.uv0+(node_9759.g*_G_Uspeed)*float2(1,0))+(node_9759.g*_G_Vspeed)*float2(0,1));
                float4 _tex_G_var = tex2D(_tex_G,TRANSFORM_TEX(node_3788, _tex_G));
                float4 node_5989 = _Time;
                float2 node_680 = (i.uv0+(float2(_B_Uspeed,_B_Vspeed)*node_5989.g));
                float4 _tex_B_var = tex2D(_tex_B,TRANSFORM_TEX(node_680, _tex_B));
                float node_2589 = max(0, (_tex_R_var.r*_tex_G_var.g*_tex_B_var.b*_Strengh)*0.7+-0.2);
                float3 emissive = (_TintColor.rgb*i.vertexColor.rgb*node_2589);
                float3 finalColor = emissive;
                return fixed4(finalColor,saturate((i.vertexColor.a*_TintColor.a*node_2589)));
            }
            ENDCG
        }
    }
}
