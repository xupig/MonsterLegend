Shader "XGame/Shader Forge/sf_aion_RGBA_blend_dissolve" {
    Properties {
        _Main_Map ("Main_Map", 2D) = "white" {}
        _Main_Color ("Main_Color", Color) = (1,1,1,1)
        _Strengh ("Strengh", Float ) = 1
        [MaterialToggle] _RGB_Channel ("RGB_Channel", Float ) = 0
        [MaterialToggle] _R_Channel ("R_Channel", Float ) = 0
        [MaterialToggle] _G_Channel ("G_Channel", Float ) = 0
        [MaterialToggle] _B_Channel ("B_Channel", Float ) = 0
        [MaterialToggle] _A_Channel ("A_Channel", Float ) = 0
        _Noise_Map ("Noise_Map", 2D) = "white" {}
        [MaterialToggle] _noise_R ("noise_R", Float ) = 0
        [MaterialToggle] _noise_G ("noise_G", Float ) = 0
        [MaterialToggle] _noise_B ("noise_B", Float ) = 0
        [MaterialToggle] _noise_A ("noise_A", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform sampler2D _Main_Map; uniform float4 _Main_Map_ST;
            uniform float4 _Main_Color;
            uniform float _Strengh;
            uniform fixed _R_Channel;
            uniform fixed _G_Channel;
            uniform fixed _B_Channel;
            uniform fixed _A_Channel;
            uniform fixed _RGB_Channel;
            uniform sampler2D _Noise_Map; uniform float4 _Noise_Map_ST;
            uniform fixed _noise_R;
            uniform fixed _noise_G;
            uniform fixed _noise_B;
            uniform fixed _noise_A;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float node_9947 = 0.0;
                float4 _Noise_Map_var = tex2D(_Noise_Map,TRANSFORM_TEX(i.uv0, _Noise_Map));
                clip((saturate((_Main_Color.a*i.vertexColor.a*(lerp( node_9947, _Noise_Map_var.r, _noise_R )+lerp( node_9947, _Noise_Map_var.g, _noise_G )+lerp( node_9947, _Noise_Map_var.b, _noise_B )+lerp( node_9947, _Noise_Map_var.a, _noise_A ))*8.0))*3.333333+-1.0) - 0.5);
////// Lighting:
////// Emissive:
                float4 _Main_Map_var = tex2D(_Main_Map,TRANSFORM_TEX(i.uv0, _Main_Map));
                float node_766 = (lerp( node_9947, _Main_Map_var.r, _R_Channel )+lerp( node_9947, _Main_Map_var.g, _G_Channel )+lerp( node_9947, _Main_Map_var.b, _B_Channel )+lerp( node_9947, _Main_Map_var.a, _A_Channel ));
                float3 emissive = (_Main_Color.rgb*i.vertexColor.rgb*_Strengh*lerp( node_766, _Main_Map_var.rgb, _RGB_Channel )*_Noise_Map_var.r);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,saturate((_Main_Color.a*i.vertexColor.a*(node_766*2.222222+-1.222222))));
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
   // CustomEditor "ShaderForgeMaterialInspector"
}
