// Shader created with Shader Forge v1.35 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.35;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:True,fnfb:True;n:type:ShaderForge.SFN_Final,id:9361,x:33209,y:32712,varname:node_9361,prsc:2|emission-2830-OUT,olwid-7626-OUT,olcol-5706-OUT;n:type:ShaderForge.SFN_NormalVector,id:9684,x:31841,y:32752,prsc:2,pt:True;n:type:ShaderForge.SFN_Dot,id:7782,x:32115,y:32713,cmnt:Lambert,varname:node_7782,prsc:2,dt:1|A-1860-XYZ,B-9684-OUT;n:type:ShaderForge.SFN_Tex2d,id:851,x:32038,y:32239,ptovrint:False,ptlb:Main_Map,ptin:_Main_Map,varname:node_851,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Color,id:5927,x:32048,y:32425,ptovrint:False,ptlb:Main_Color,ptin:_Main_Color,varname:node_5927,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:544,x:32283,y:32462,cmnt:Diffuse Color,varname:node_544,prsc:2|A-851-RGB,B-5927-RGB;n:type:ShaderForge.SFN_Add,id:2830,x:32644,y:32382,varname:node_2830,prsc:2|A-544-OUT,B-5879-RGB,C-3392-OUT,D-8060-OUT;n:type:ShaderForge.SFN_Tex2d,id:6607,x:32457,y:32635,ptovrint:False,ptlb:Falloff_Map,ptin:_Falloff_Map,varname:node_6607,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:a07463f9e8fdf5142ad1644e6f4689c6,ntxv:0,isnm:False|UVIN-7675-OUT;n:type:ShaderForge.SFN_Append,id:7675,x:32283,y:32701,varname:node_7675,prsc:2|A-7782-OUT,B-7782-OUT;n:type:ShaderForge.SFN_Color,id:5879,x:32235,y:32239,ptovrint:False,ptlb:Emission_Color,ptin:_Emission_Color,varname:node_5879,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Negate,id:7396,x:32281,y:32908,varname:node_7396,prsc:2|IN-5029-OUT;n:type:ShaderForge.SFN_ViewVector,id:9449,x:32269,y:33038,varname:node_9449,prsc:2;n:type:ShaderForge.SFN_Dot,id:5823,x:32551,y:32907,varname:node_5823,prsc:2,dt:1|A-7396-OUT,B-9449-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3496,x:32551,y:33169,ptovrint:False,ptlb:Side_Power,ptin:_Side_Power,varname:node_3496,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:20;n:type:ShaderForge.SFN_Add,id:5029,x:32058,y:32953,varname:node_5029,prsc:2|A-1860-XYZ,B-9684-OUT;n:type:ShaderForge.SFN_Multiply,id:8060,x:32890,y:32896,varname:node_8060,prsc:2|A-5823-OUT,B-5692-OUT,C-1425-OUT,D-4599-RGB,E-6548-OUT;n:type:ShaderForge.SFN_Power,id:1425,x:32767,y:33085,varname:node_1425,prsc:2|VAL-5823-OUT,EXP-3496-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5692,x:32551,y:33068,ptovrint:False,ptlb:Side_Falloff,ptin:_Side_Falloff,varname:node_5692,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Color,id:4599,x:32639,y:32752,ptovrint:False,ptlb:Side_Color,ptin:_Side_Color,varname:node_4599,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:3392,x:32719,y:32573,varname:node_3392,prsc:2|A-6607-RGB,B-4599-RGB;n:type:ShaderForge.SFN_Vector4Property,id:1860,x:31841,y:32582,ptovrint:False,ptlb:Light_Position,ptin:_Light_Position,varname:node_1860,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1,v2:0,v3:-1,v4:0;n:type:ShaderForge.SFN_ToggleProperty,id:6548,x:32326,y:33203,ptovrint:False,ptlb:Use_Side_Light,ptin:_Use_Side_Light,varname:node_6548,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True;n:type:ShaderForge.SFN_Multiply,id:5706,x:32863,y:33294,varname:node_5706,prsc:2|A-509-OUT,B-851-RGB;n:type:ShaderForge.SFN_Vector4,id:509,x:32597,y:33294,varname:node_509,prsc:2,v1:0.345098,v2:0.345098,v3:0.345098,v4:0.7;n:type:ShaderForge.SFN_Vector1,id:5237,x:32744,y:33448,varname:node_5237,prsc:2,v1:0.005;n:type:ShaderForge.SFN_ValueProperty,id:4092,x:32719,y:33565,ptovrint:False,ptlb:Outline_Width(Default 1),ptin:_Outline_WidthDefault1,varname:node_4092,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:7626,x:32963,y:33483,varname:node_7626,prsc:2|A-5237-OUT,B-4092-OUT;proporder:851-5927-5879-6607-6548-3496-5692-4599-1860-4092;pass:END;sub:END;*/

Shader "XGame/Shader Forge/sf_toon_character_unlit_ui" {
    Properties {
        _Main_Map ("Main_Map", 2D) = "white" {}
        _Main_Color ("Main_Color", Color) = (1,1,1,1)
        _Emission_Color ("Emission_Color", Color) = (0,0,0,1)
        _Falloff_Map ("Falloff_Map", 2D) = "white" {}
        [MaterialToggle] _Use_Side_Light ("Use_Side_Light", Float ) = 1
        _Side_Power ("Side_Power", Float ) = 20
        _Side_Falloff ("Side_Falloff", Float ) = 1
        _Side_Color ("Side_Color", Color) = (0.5,0.5,0.5,1)
        _Light_Position ("Light_Position", Vector) = (1,0,-1,0)
        _Outline_WidthDefault1 ("Outline_Width(Default 1)", Float ) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            uniform sampler2D _Main_Map; uniform float4 _Main_Map_ST;
            uniform float _Outline_WidthDefault1;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, float4(v.vertex.xyz + v.normal*(0.005*_Outline_WidthDefault1),1) );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 _Main_Map_var = tex2D(_Main_Map,TRANSFORM_TEX(i.uv0, _Main_Map));
                return fixed4((float4(0.345098,0.345098,0.345098,0.7)*_Main_Map_var.rgb).rgb,0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            uniform sampler2D _Main_Map; uniform float4 _Main_Map_ST;
            uniform float4 _Main_Color;
            uniform sampler2D _Falloff_Map; uniform float4 _Falloff_Map_ST;
            uniform float4 _Emission_Color;
            uniform float _Side_Power;
            uniform float _Side_Falloff;
            uniform float4 _Side_Color;
            uniform float4 _Light_Position;
            uniform fixed _Use_Side_Light;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _Main_Map_var = tex2D(_Main_Map,TRANSFORM_TEX(i.uv0, _Main_Map));
                float node_7782 = max(0,dot(_Light_Position.rgb,normalDirection)); // Lambert
                float2 node_7675 = float2(node_7782,node_7782);
                float4 _Falloff_Map_var = tex2D(_Falloff_Map,TRANSFORM_TEX(node_7675, _Falloff_Map));
                float node_5823 = max(0,dot((-1*(_Light_Position.rgb+normalDirection)),viewDirection));
                float3 emissive = ((_Main_Map_var.rgb*_Main_Color.rgb)+_Emission_Color.rgb+(_Falloff_Map_var.rgb*_Side_Color.rgb)+(node_5823*_Side_Falloff*pow(node_5823,_Side_Power)*_Side_Color.rgb*_Use_Side_Light));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
