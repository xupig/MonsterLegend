Shader "XGame/Shader Forge/sf_aion_earthquake" {
    Properties {
        _Main_Map ("Main_Map", 2D) = "white" {}
        _Main_Color ("Main_Color", Color) = (1,1,1,1)
        _Strengh ("Strengh", Float ) = 1
        [MaterialToggle] _RGB_Channel ("RGB_Channel", Float ) = 0
        [MaterialToggle] _R_Channel ("R_Channel", Float ) = 0
        [MaterialToggle] _G_Channel ("G_Channel", Float ) = 0
        [MaterialToggle] _B_Channel ("B_Channel", Float ) = 0
        [MaterialToggle] _A_Channel ("A_Channel", Float ) = 0
        _Speed ("Speed", Float ) = 1
        _Noise_Map ("Noise_Map", 2D) = "white" {}
        _Power ("Power", Range(0, 1)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent+1"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _Main_Map; uniform float4 _Main_Map_ST;
            uniform float4 _Main_Color;
            uniform float _Strengh;
            uniform fixed _R_Channel;
            uniform fixed _G_Channel;
            uniform fixed _B_Channel;
            uniform fixed _A_Channel;
            uniform fixed _RGB_Channel;
            uniform float _Speed;
            uniform sampler2D _Noise_Map; uniform float4 _Noise_Map_ST;
            uniform float _Power;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 node_9989 = _Time + _TimeEditor;
                float node_3706 = 1.0;
                float node_5324 = (-1.0);
                float2 node_8038 = (i.uv0+(node_9989.r*float2(node_3706,node_5324)*_Speed));
                float4 node_6895 = tex2D(_Noise_Map,TRANSFORM_TEX(node_8038, _Noise_Map));
                float2 node_9386 = (i.uv0+(node_9989.r*float2(node_5324,node_3706)*_Speed));
                float4 node_3066 = tex2D(_Noise_Map,TRANSFORM_TEX(node_9386, _Noise_Map));
                float2 node_18 = (i.uv0+(node_9989.r*float2(0.2,0.7)*_Speed));
                float4 node_3269 = tex2D(_Noise_Map,TRANSFORM_TEX(node_18, _Noise_Map));
                float node_9947 = 0.0;
                float4 _Main_Map_var = tex2D(_Main_Map,TRANSFORM_TEX(i.uv0, _Main_Map));
                float node_766 = (lerp( node_9947, _Main_Map_var.r, _R_Channel )+lerp( node_9947, _Main_Map_var.g, _G_Channel )+lerp( node_9947, _Main_Map_var.b, _B_Channel )+lerp( node_9947, _Main_Map_var.a, _A_Channel ));
                float3 emissive = clamp((((node_6895.r*node_3066.g*node_3269.rgb*lerp((-80.0),2.0,_Power))*2.0+-1.0)+(_Main_Color.rgb*i.vertexColor.rgb*_Strengh*lerp( node_766, _Main_Map_var.rgb, _RGB_Channel ))),0,8);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,saturate((_Main_Color.a*node_766*i.vertexColor.a*4.0)));
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    //CustomEditor "ShaderForgeMaterialInspector"
}
