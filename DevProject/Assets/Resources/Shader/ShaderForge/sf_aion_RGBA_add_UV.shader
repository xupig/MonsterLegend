Shader "XGame/Shader Forge/sf_aion_RGBA_add_UV" {
    Properties {
        _Main_Map ("Main_Map", 2D) = "white" {}
        _TintColor ("Color", Color) = (1,1,1,1)
        _Strengh ("Strengh", Float ) = 1
        [MaterialToggle] _RGBA ("RGBA", Float ) = 0
        [MaterialToggle] _R_Channel ("R_Channel", Float ) = 0
        _R_speed_uv ("R_speed_uv", Vector) = (0,0,1,1)
        [MaterialToggle] _G_Channel ("G_Channel", Float ) = 0
        _G_speed_uv ("G_speed_uv", Vector) = (0,0,1,1)
        [MaterialToggle] _B_Channel ("B_Channel", Float ) = 0
        _B_speed_uv ("B_speed_uv", Vector) = (0,0,1,1)
        [MaterialToggle] _A_Channel ("A_Channel", Float ) = 0
        _A_speed_uv ("A_speed_uv", Vector) = (0,0,1,1)
        [MaterialToggle] _A_map_Channel ("A_map_Channel", Float ) = 0
        _A_Map ("A_Map", 2D) = "white" {}
        _Vertex_Offset ("Vertex_Offset", Float ) = 1
        _Offset_Speed ("Offset_Speed", Float ) = 1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _TintColor;
            uniform float _Strengh;
            uniform float4 _R_speed_uv;
            uniform sampler2D _Main_Map; uniform float4 _Main_Map_ST;
            uniform float4 _G_speed_uv;
            uniform float4 _B_speed_uv;
            uniform float4 _A_speed_uv;
            uniform fixed _R_Channel;
            uniform fixed _G_Channel;
            uniform fixed _B_Channel;
            uniform fixed _A_Channel;
            uniform fixed _RGBA;
            uniform sampler2D _A_Map; uniform float4 _A_Map_ST;
            uniform fixed _A_map_Channel;
            uniform float _Vertex_Offset;
            uniform float _Offset_Speed;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
#if !defined(SHADER_API_GLES)
                float4 node_3969 = _Time + _TimeEditor;
                float4 node_4593 = tex2Dlod(_Main_Map,float4(TRANSFORM_TEX(o.uv0, _Main_Map),0.0,0));
                float node_5486 = (sin(mul(unity_ObjectToWorld, v.vertex).a)*(o.uv0.r*cos((node_3969.g*_Offset_Speed))*node_4593.r)*_Vertex_Offset);
                v.vertex.xyz += float3(node_5486,node_5486,node_5486);
#endif
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float node_1246 = 0.0;
                float4 node_8155 = _Time + _TimeEditor;
                float2 node_9565 = ((float2(_R_speed_uv.b,_R_speed_uv.a)*i.uv0)+(float2(_R_speed_uv.r,_R_speed_uv.g)*node_8155.g));
                float4 _MainTex = tex2D(_Main_Map,TRANSFORM_TEX(node_9565, _Main_Map));
                float _R_Channel_var = lerp( node_1246, _MainTex.r, _R_Channel );
                float2 node_1812 = ((float2(_G_speed_uv.b,_G_speed_uv.a)*i.uv0)+(float2(_G_speed_uv.r,_G_speed_uv.g)*node_8155.g));
                float4 node_9812 = tex2D(_Main_Map,TRANSFORM_TEX(node_1812, _Main_Map));
                float _G_Channel_var = lerp( node_1246, node_9812.g, _G_Channel );
                float2 node_5040 = ((float2(_B_speed_uv.b,_B_speed_uv.a)*i.uv0)+(float2(_B_speed_uv.r,_B_speed_uv.g)*node_8155.g));
                float4 node_5684 = tex2D(_Main_Map,TRANSFORM_TEX(node_5040, _Main_Map));
                float _B_Channel_var = lerp( node_1246, node_5684.b, _B_Channel );
                float2 node_8627 = ((float2(_A_speed_uv.b,_A_speed_uv.a)*i.uv0)+(float2(_A_speed_uv.r,_A_speed_uv.g)*node_8155.g));
                float4 node_5806 = tex2D(_Main_Map,TRANSFORM_TEX(node_8627, _Main_Map));
                float2 node_758 = (i.uv0+float2(i.vertexColor.a,node_1246));
                float4 _A_Map_var = tex2D(_A_Map,TRANSFORM_TEX(node_758, _A_Map));
                float3 _A_map_Channel_var = lerp( lerp( node_1246, node_5806.a, _A_Channel ), _A_Map_var.rgb, _A_map_Channel );
                float3 emissive = saturate(((lerp( (_R_Channel_var*_G_Channel_var*_B_Channel_var), float4(_MainTex.r,node_9812.g,node_5684.b,node_5806.a), _RGBA )*_A_map_Channel_var)*i.vertexColor.rgb*_TintColor.rgb*_Strengh*_A_map_Channel_var)).rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    //CustomEditor "ShaderForgeMaterialInspector"
}
