Shader "XGame/Shader Forge/sf_toon_RGB_blend_dissolve" {
    Properties {
        _Main_Map ("Main_Map", 2D) = "white" {}
        _Main_Color ("Main_Color", Color) = (1,1,1,0.384)
        _Strengh ("Strengh", Float ) = 1
        [MaterialToggle] _RGB_Channel ("RGB_Channel", Float ) = 0
        [MaterialToggle] _R_Channel ("R_Channel", Float ) = 0
        [MaterialToggle] _G_Channel ("G_Channel", Float ) = 0
        [MaterialToggle] _B_Channel ("B_Channel", Float ) = 0
        [MaterialToggle] _A_Channel ("A_Channel", Float ) = 0
        _Dissolve_Map ("Dissolve_Map", 2D) = "white" {}
        //[MaterialToggle] _Soft_OnOffDX11orDefferedMode ("Soft_On/Off(DX11 or Deffered Mode)", Float ) = 1
        //_Soft ("Soft", Range(0, 3)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            //#pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            //#pragma target 2.0
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _Main_Color;
            uniform float _Strengh;
            uniform float _Soft;
            uniform fixed _Soft_OnOffDX11orDefferedMode;
            uniform sampler2D _Dissolve_Map; uniform float4 _Dissolve_Map_ST;
            uniform sampler2D _Main_Map; uniform float4 _Main_Map_ST;
            uniform fixed _R_Channel;
            uniform fixed _G_Channel;
            uniform fixed _B_Channel;
            uniform fixed _A_Channel;
            uniform fixed _RGB_Channel;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
                float4 projPos : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                //float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                //float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float4 _Dissolve_Map_var = tex2D(_Dissolve_Map,TRANSFORM_TEX(i.uv0, _Dissolve_Map));
                float4 _Main_Map_var = tex2D(_Main_Map,TRANSFORM_TEX(i.uv0, _Main_Map));
				//float node_3625 = 0.0;
                //float node_3651 = (lerp( node_3625, _Main_Map_var.r, _R_Channel )+lerp( node_3625, _Main_Map_var.g, _G_Channel )+lerp( node_3625, _Main_Map_var.b, _B_Channel )+lerp( node_3625, _Main_Map_var.a, _A_Channel ));
				float node_3651 = _Main_Map_var.r*_R_Channel + _Main_Map_var.g*_G_Channel + _Main_Map_var.b*_B_Channel + _Main_Map_var.a*_A_Channel;
                clip((_Dissolve_Map_var.r+((_Main_Color.a*i.vertexColor.a*node_3651)*2.0+-1.0)) - 0.5);
////// Lighting:
////// Emissive:
                float3 emissive = ((_Main_Color.rgb*i.vertexColor.rgb*_Strengh*i.vertexColor.a*lerp( node_3651, _Main_Map_var.rgb, _RGB_Channel ))/*lerp( 1.0, saturate((sceneZ-partZ)/_Soft), _Soft_OnOffDX11orDefferedMode )*/);
                float3 finalColor = emissive;
                return fixed4(finalColor,(i.vertexColor.a*_Main_Color.a*_Dissolve_Map_var.r));
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
   // CustomEditor "ShaderForgeMaterialInspector"
}
