Shader "XGame/Shader Forge/sf_character_p4_stone" {
    Properties {
        _MainTex ("Main_Map", 2D) = "white" {}
        _Dissolve_Map ("Dissolve_Map", 2D) = "white" {}
        _Dissolve ("Dissolve", Range(0, 1)) = 0
        [MaterialToggle] _Dissolve_UV2 ("Dissolve_UV2", Float ) = 0
        _Emission_Color ("Emission_Color", Color) = (0.5147059,0.5147059,0.5147059,1)
        _Stone_Map ("Stone_Map", 2D) = "white" {}
		[HideInInspector]_AmbientColor("AmbientColor", Color) = (0.212, 0.227, 0.259, 1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float _Dissolve;
            uniform sampler2D _Dissolve_Map; uniform float4 _Dissolve_Map_ST;
            uniform fixed _Dissolve_UV2;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Emission_Color;
            uniform sampler2D _Stone_Map; uniform float4 _Stone_Map_ST;
			uniform fixed4 _AmbientColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                LIGHTING_COORDS(4,5)
                UNITY_FOG_COORDS(6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
#if !defined(SHADER_API_GLES)
                float2 _Dissolve_UV2_var = lerp( o.uv0, o.uv1, _Dissolve_UV2 );
                float4 _Stone_Map_var = tex2Dlod(_Stone_Map,float4(TRANSFORM_TEX(_Dissolve_UV2_var, _Stone_Map),0.0,0));
                float4 _Dissolve_Map_var = tex2Dlod(_Dissolve_Map,float4(TRANSFORM_TEX(_Dissolve_UV2_var, _Dissolve_Map),0.0,0));
                float node_8987 = step(_Dissolve,_Dissolve_Map_var.r);
                v.vertex.xyz += (0.05*_Stone_Map_var.rgb*v.normal*node_8987);
#endif
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float2 _Dissolve_UV2_var = lerp( i.uv0, i.uv1, _Dissolve_UV2 );
                float4 _Stone_Map_var = tex2D(_Stone_Map,TRANSFORM_TEX(_Dissolve_UV2_var, _Stone_Map));
                float4 _Dissolve_Map_var = tex2D(_Dissolve_Map,TRANSFORM_TEX(_Dissolve_UV2_var, _Dissolve_Map));
                float node_8987 = step(_Dissolve,_Dissolve_Map_var.r);
                float3 emissive = lerp(_MainTex_var.rgb,((((_MainTex_var.r*_MainTex_var.g*_MainTex_var.b)+_Stone_Map_var.rgb+(dot(lightDirection,dot(viewDirection,i.normalDir))*0.2))*_Emission_Color.rgb)+ _AmbientColor.rgb),node_8987);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float _Dissolve;
            uniform sampler2D _Dissolve_Map; uniform float4 _Dissolve_Map_ST;
            uniform fixed _Dissolve_UV2;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Emission_Color;
            uniform sampler2D _Stone_Map; uniform float4 _Stone_Map_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                LIGHTING_COORDS(4,5)
                UNITY_FOG_COORDS(6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
#if !defined(SHADER_API_GLES)
                float2 _Dissolve_UV2_var = lerp( o.uv0, o.uv1, _Dissolve_UV2 );
                float4 _Stone_Map_var = tex2Dlod(_Stone_Map,float4(TRANSFORM_TEX(_Dissolve_UV2_var, _Stone_Map),0.0,0));
                float4 _Dissolve_Map_var = tex2Dlod(_Dissolve_Map,float4(TRANSFORM_TEX(_Dissolve_UV2_var, _Dissolve_Map),0.0,0));
                float node_8987 = step(_Dissolve,_Dissolve_Map_var.r);
                v.vertex.xyz += (0.05*_Stone_Map_var.rgb*v.normal*node_8987);
#endif
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
////// Lighting:
                float3 finalColor = 0;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_fog
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float _Dissolve;
            uniform sampler2D _Dissolve_Map; uniform float4 _Dissolve_Map_ST;
            uniform fixed _Dissolve_UV2;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Emission_Color;
            uniform sampler2D _Stone_Map; uniform float4 _Stone_Map_ST;
			uniform fixed4 _AmbientColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
#if !defined(SHADER_API_GLES)
                float2 _Dissolve_UV2_var = lerp( o.uv0, o.uv1, _Dissolve_UV2 );
                float4 _Stone_Map_var = tex2Dlod(_Stone_Map,float4(TRANSFORM_TEX(_Dissolve_UV2_var, _Stone_Map),0.0,0));
                float4 _Dissolve_Map_var = tex2Dlod(_Dissolve_Map,float4(TRANSFORM_TEX(_Dissolve_UV2_var, _Dissolve_Map),0.0,0));
                float node_8987 = step(_Dissolve,_Dissolve_Map_var.r);
                v.vertex.xyz += (0.05*_Stone_Map_var.rgb*v.normal*node_8987);
#endif
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : SV_Target {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float2 _Dissolve_UV2_var = lerp( i.uv0, i.uv1, _Dissolve_UV2 );
                float4 _Stone_Map_var = tex2D(_Stone_Map,TRANSFORM_TEX(_Dissolve_UV2_var, _Stone_Map));
                float4 _Dissolve_Map_var = tex2D(_Dissolve_Map,TRANSFORM_TEX(_Dissolve_UV2_var, _Dissolve_Map));
                float node_8987 = step(_Dissolve,_Dissolve_Map_var.r);
                o.Emission = lerp(_MainTex_var.rgb,((((_MainTex_var.r*_MainTex_var.g*_MainTex_var.b)+_Stone_Map_var.rgb+(dot(lightDirection,dot(viewDirection,i.normalDir))*0.2))*_Emission_Color.rgb)+ _AmbientColor.rgb),node_8987);
                
                float3 diffColor = float3(0,0,0);
                o.Albedo = diffColor;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
   // CustomEditor "ShaderForgeMaterialInspector"
}
