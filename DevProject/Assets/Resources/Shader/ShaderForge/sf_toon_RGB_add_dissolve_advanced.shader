Shader "XGame/Shader Forge/sf_toon_RGB_add_dissolve_advanced" {
    Properties {
        _Main_Map ("Main_Map", 2D) = "white" {}
        _Main_Color ("Main_Color", Color) = (1,1,1,1)
        _Strengh ("Strengh", Float ) = 1
        [MaterialToggle] _RGB_Channel ("RGB_Channel", Float ) = 0
        [MaterialToggle] _R_Channel ("R_Channel", Float ) = 0
        [MaterialToggle] _G_Channel ("G_Channel", Float ) = 0
        [MaterialToggle] _B_Channel ("B_Channel", Float ) = 0
        [MaterialToggle] _A_Channel ("A_Channel", Float ) = 0
        _Dissolve_Map ("Dissolve_Map", 2D) = "white" {}
        //[MaterialToggle] _Soft_OnOffDX11orDefferedMode ("Soft_On/Off(DX11 or Deffered Mode)", Float ) = 1
        //_Soft ("Soft", Range(0, 3)) = 0
        _UV2_Map ("UV2_Map", 2D) = "white" {}
        _R_Uspeed ("R_Uspeed", Float ) = 0
        _R_Vspeed ("R_Vspeed", Float ) = 0
        _G_Uspeed ("G_Uspeed", Float ) = 0
        _G_Vspeed ("G_Vspeed", Float ) = 0
        _Fresnel ("Fresnel", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal n3ds wiiu 
            //#pragma target 2.0
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform float4 _Main_Color;
            uniform float _Strengh;
            uniform float _Soft;
            uniform fixed _Soft_OnOffDX11orDefferedMode;
            uniform sampler2D _Dissolve_Map; uniform float4 _Dissolve_Map_ST;
            uniform sampler2D _Main_Map; uniform float4 _Main_Map_ST;
            uniform fixed _R_Channel;
            uniform fixed _G_Channel;
            uniform fixed _B_Channel;
            uniform fixed _A_Channel;
            uniform fixed _RGB_Channel;
            uniform float _R_Uspeed;
            uniform float _R_Vspeed;
            uniform sampler2D _UV2_Map; uniform float4 _UV2_Map_ST;
            uniform float _G_Uspeed;
            uniform float _G_Vspeed;
            uniform float _Fresnel;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                float4 vertexColor : COLOR;
                float4 projPos : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                //float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                //float partZ = max(0,i.projPos.z - _ProjectionParams.g);
                float4 _Dissolve_Map_var = tex2D(_Dissolve_Map,TRANSFORM_TEX(i.uv0, _Dissolve_Map));
                float node_9419 = (_Dissolve_Map_var.r+((_Main_Color.a*i.vertexColor.a)*1.3+-0.6));
                clip(node_9419 - 0.5);
////// Lighting:
////// Emissive:
                float4 node_9952 = _Time + _TimeEditor;
                float2 node_411 = (i.uv1+(float2(_R_Uspeed,_R_Vspeed)*node_9952.g));
                float4 node_8308 = tex2D(_UV2_Map,TRANSFORM_TEX(node_411, _UV2_Map));
                float2 node_4799 = (i.uv1+(float2(_G_Uspeed,_G_Vspeed)*node_9952.g));
                float4 node_1229 = tex2D(_UV2_Map,TRANSFORM_TEX(node_4799, _UV2_Map));
                //float node_3625 = 0.0;
                float4 _Main_Map_var = tex2D(_Main_Map,TRANSFORM_TEX(i.uv0, _Main_Map));
                //float3 _RGB_Channel_var = lerp( (lerp( node_3625, _Main_Map_var.r, _R_Channel )+lerp( node_3625, _Main_Map_var.g, _G_Channel )+lerp( node_3625, _Main_Map_var.b, _B_Channel )+lerp( node_3625, _Main_Map_var.a, _A_Channel )), _Main_Map_var.rgb, _RGB_Channel );
				float3 _RGB_Channel_var = lerp(_Main_Map_var.r*_R_Channel + _Main_Map_var.g*_G_Channel + _Main_Map_var.b*_B_Channel + _Main_Map_var.a*_A_Channel, _Main_Map_var.rgb, _RGB_Channel);
                float3 emissive = (saturate((_RGB_Channel_var/(node_8308.r*node_1229.g)))*((_Main_Color.rgb*i.vertexColor.rgb*_Strengh*i.vertexColor.a*_RGB_Channel_var)/*lerp( 1.0, saturate((sceneZ-partZ)/_Soft), _Soft_OnOffDX11orDefferedMode )*/*_RGB_Channel_var*pow(1.0-max(0,dot(normalDirection, viewDirection)),_Fresnel)));
                float3 finalColor = emissive;
                return fixed4(finalColor,node_9419);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
   // CustomEditor "ShaderForgeMaterialInspector"
}
