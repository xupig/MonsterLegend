﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/Mogo/Particles/MogoParticlesBlend43_Flow" {
Properties {
	_TintColor ("Tint Color", Color) = (0.5,0.5,0.5,0.5)
	_MainTex ("Particle Texture", 2D) = "white" {}
	_FlowMap ("Flow Map (RGB)", 2D) = "black" {}
	_ZOffset ("Z Offset",float) = 0
	_FluxColor ("Flux color Layer(RGB)", Color) 																			= (1.0, 1.0, 1.0, 1.0)
	_FluxParams("Flux Params Layer", Vector)																				= (1.0, 1.0, 1.0, 1.0)
}

Category {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	Blend SrcAlpha OneMinusSrcAlpha
	
	Cull Off Lighting Off ZWrite Off
	BindChannels {
		Bind "Color", color
		Bind "Vertex", vertex
		Bind "TexCoord", texcoord
	}
	
	SubShader {
		Pass {
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma multi_compile_particles
			
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			fixed4 _TintColor;
			half _ZOffset;

			uniform sampler2D 	_FlowMap;
			uniform fixed4		_FluxColor;
			uniform half4 		_FluxParams;
			
			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex		: POSITION;
				fixed4 color		: COLOR;
				float2 texcoord		: TEXCOORD0;
				half2  uvFlux		: TEXCOORD1;
			};
			
			float4 _MainTex_ST;

			v2f vert (appdata_t v)
			{
				v2f o = (v2f)0;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.vertex.z -= _ZOffset;
				
				o.color		= v.color;
				o.texcoord	= TRANSFORM_TEX(v.texcoord,_MainTex);
			
				o.uvFlux.xy = v.vertex.xy * _FluxParams.xy * 0.1 + _Time.x * _FluxParams.zw;

				return o;
			}

			
			fixed4 frag (v2f i) : COLOR
			{
				fixed4 c = i.color * _TintColor * tex2D(_MainTex, i.texcoord);

				//c.rgb +=  tex2D( _FlowMap,  i.uvFlux.xy ).rgb * _FluxColor.rgb * i.uvFlux.z;
				c.rgb  = lerp( c.rgb, _FluxColor.rgb * _FluxColor.a, tex2D( _FlowMap,  i.uvFlux.xy ).rgb );

				return 2.0f * c;
			}
			ENDCG
		}
	} 	
	
	SubShader {
		Pass {
			SetTexture [_MainTex] {
				constantColor [_TintColor]
				combine constant * primary
			}
			SetTexture [_MainTex] {
				combine texture * previous DOUBLE
			}
		}
	}
	
	SubShader {
		Pass {
			SetTexture [_MainTex] {
				combine texture * primary
			}
		}
	}
}
}
