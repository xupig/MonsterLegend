// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/Mogo/Particle/MogoSpotGradient" {
Properties {
	[HideInInspector]_MainTex ("Base (RGB)", 2D) = "white" {}
	_BaseColor ("Base Color(RGBA)", Color) = (1,1,1,1)
	//_StartWidth( "Start Width", float ) = 1.0
	//_EndWidth( "End Width", float ) = 1.0
	//_MaxWidth( "Max Width", float ) = 2.0
	//_RadiusThreshold(" Radius Threshold", float) = 1.0
	_InternalAlpha( "Internal Alpha", Range(0,1) ) = 0.4
	[HideInInspector]_threshold("Threshold", float) = 0.0
}

SubShader {
	Tags { "RenderType"="Transparent" "Queue"="Transparent" }
	//LOD 300

	Cull Off

	Blend SrcAlpha OneMinusSrcAlpha
	ZWrite Off
	
	Pass {  
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest

			#include "UnityCG.cginc"

			fixed4 _BaseColor;
			//half _MinWidth;
			//half _StartWidth;
			//half _EndWidth;
			//half _MaxWidth;
			//half _RadiusThreshold;
			half4 _threshold;
			half _InternalAlpha;

			struct appdata_t {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				half4 texcoord : TEXCOORD0;
				half3 attenPattern : TEXCOORD1;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata_t v)
			{
				v2f o = (v2f)0;

				float4 vert = v.vertex;
				
				vert.x = v.vertex.x * lerp( _threshold.z, _threshold.w, v.texcoord.y );
				  
				o.attenPattern.xyz = vert.xyz;

				o.vertex = UnityObjectToClipPos(vert);
				o.texcoord.xy = v.texcoord;//TRANSFORM_TEX(v.texcoord, _MainTex);

				o.texcoord.zw = _threshold.xy;

				return o;
			}
			
			fixed4 frag (v2f i) : COLOR
			{
				half gradientpattern	= abs( i.texcoord.x * 2 - 1 );

				half gradient			= saturate( gradientpattern - i.texcoord.z ) * i.texcoord.w;

				fixed4 col				= _BaseColor;
				col.rgb					*= 2;

				col.a					= col.a * gradient + _InternalAlpha;

				half2  dir				= i.texcoord.xy - half2( 0.5, 0 );
				col.a					*= 1 - dot( dir, dir);

				return col;
			}
		ENDCG
	}
}

}
