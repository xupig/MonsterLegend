// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/Mogo/Particle/MogoEdgeGradient" {
Properties {
	[HideInInspector]_MainTex ("Base (RGB)", 2D) = "white" {}
	_BaseColor ("Base Color(RGBA)", Color) = (1,1,1,1)
	//_ScaleX( "Scale X", float ) = 1.0
	//_MaxWidth( "Max Width", float ) = 2.0
	_InternalAlpha( "Internal Alpha", Range(0,1) ) = 0.4
	//[HideInInspector]_threshold("Threshold", float) = 0.0
	[HideInInspector]_thickness("Thickness", float) = 0.01
	[HideInInspector]_width("Width", float) = 0.5
}

SubShader {
	Tags { "RenderType"="Transparent" "Queue"="Transparent" }
	//LOD 300

	Cull Off
	Blend SrcAlpha OneMinusSrcAlpha
	ZWrite Off
	
	Pass {  
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest

			#include "UnityCG.cginc"

			fixed4 _BaseColor;
			//half _MinWidth;
			half _ScaleX;
			half _MaxWidth;
			//half _threshold;
			half _InternalAlpha;
			half _thickness;
			half _width;

			struct appdata_t {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				half4 texcoord : TEXCOORD0;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;


			
			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.texcoord = v.vertex;
				return o;
			}
			
			fixed4 frag (v2f i) : COLOR
			{
				fixed4 col				= _BaseColor;
				
				half d = abs(i.texcoord.x);
				half y = abs(i.texcoord.y);
				
				//draw thickness or fill color
				col.a = step(_width - d, _thickness) + step(y, _thickness/4) + step(1 - _thickness/4, y)
						+ max(0.1, col.a * d / _width - (1-_InternalAlpha));

				return col;
			}
		ENDCG
	}
}

}
