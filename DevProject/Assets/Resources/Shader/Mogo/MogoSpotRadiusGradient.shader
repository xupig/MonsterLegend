// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/Mogo/Particle/MogoSpotRadiusGradient" {
Properties {
	[HideInInspector]_MainTex ("Base (RGB)", 2D) = "white" {}
	_BaseColor ("Base Color(RGBA)", Color) = (1,1,1,1)
	_InternalAlpha( "Internal Alpha", Range(0,1) ) = 0.5
	[HideInInspector]_thickness("Thickness", float) = 0.02
	[HideInInspector]_angle("Angle", float) = 0.0
	[HideInInspector]_radius("Radius", float) = 1.0
	[HideInInspector]_center("Center", Vector) = (0,0,0)
}

SubShader {
	Tags { "RenderType"="Transparent" "Queue"="Transparent" }
	//LOD 300

	Cull Off

	Blend SrcAlpha OneMinusSrcAlpha
	ZWrite Off
	
	Pass {  
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest

			#include "UnityCG.cginc"

			fixed4 _BaseColor;
			half _InternalAlpha;
			half3 _center;
			half _thickness;
			half _radius;
			half _angle;

			struct appdata_t {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				fixed4 texcoord : TEXCOORD0;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata_t v)
			{
				v2f o = (v2f)0;

				float4 vert = v.vertex;

				o.texcoord = v.vertex;

				o.vertex = UnityObjectToClipPos(vert);

				return o;
			}
			
			fixed4 frag (v2f i) : COLOR
			{
			
				fixed4 col				= _BaseColor;
				
				half angle = atan(abs(i.texcoord.x / i.texcoord.z));
				half d = distance(i.texcoord.xyz, _center);

				// draw outline
				//col.a = step(angle, _angle) * step(d, _radius);
				col.a = max(0, step(angle, _angle) + step(d, _radius) - 1);

				// draw thickness or fill color 
				col.a *= (step(d, _thickness / sin(_angle - angle)) + step(_radius - d, _thickness))
						+ max(0.1, col.a * d / _radius - (1-_InternalAlpha));

				return col;
			}
		ENDCG
	}
}

}
