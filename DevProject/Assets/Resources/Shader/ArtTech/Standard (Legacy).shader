﻿Shader "XGame/Standard (Legacy)" {
	Properties {
		[MaterialEnum(On, 0, Off, 2)] _Cull("双面渲染", Float) = 2

		_Color("Main Color", Color) = (1, 1, 1, 1)
		_MainTex("Base (RGB) Gloss (A)", 2D) = "white" {}

		_Cutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5

		_BumpScale("Scale", Float) = 1.0
		_BumpMap("Normal Map", 2D) = "bump" {}

		_SpecMap("Specular Map", 2D) = "white" {}
		_SpecColor("Specular Color", Color) = (0, 0, 0, 1)
		_Shininess("Shininess", Range(0.01, 1)) = 0.078125
		//_ShininessScale("Shininess Scale", Range(0.01, 1)) = 1

		_EmissionColor("Emission Color", Color) = (0, 0, 0)
		_EmissionMap("Emission Map", 2D) = "white" {}

		//_ReflectionColor("Reflection Color", Color) = (1, 1, 1, 1)
		//_ReflectionMap("Reflection Map", 2D) = "white" {}
		//_ReflectionCube("Reflection Cubemap", Cube) = "_Skybox" {}

		// Blending state
		[HideInInspector] _Mode("__mode", Int) = 0.0
		[HideInInspector] _SrcBlend("__src", Int) = 1.0
		[HideInInspector] _DstBlend("__dst", Int) = 0.0
		[HideInInspector] _ZWrite("__zw", Int) = 1.0
	}

	SubShader {
		Tags{ "RenderType" = "Opaque" }
		LOD 300
		Cull[_Cull]

		Pass {
			Name "FORWARD"
			Tags{ "LightMode" = "ForwardBase" }

			Blend[_SrcBlend][_DstBlend]
			ZWrite[_ZWrite]

			CGPROGRAM
			#pragma target 3.0
			#pragma multi_compile __ _NORMALMAP
			#pragma multi_compile __ _SPECULAR
			#pragma multi_compile __ _ALPHATEST_ON _ALPHABLEND_ON
			#pragma multi_compile __ _EMISSION
			//#pragma shader_feature _SPECULARMAP 
			//#pragma shader_feature _CUBE_REFLECT

			#pragma skip_variants VERTEXLIGHT_ON

			#pragma multi_compile_fog
			#pragma multi_compile_fwdbase

			#pragma vertex vertBase
			#pragma fragment fragBase

			#define UNITY_PASS_FORWARDBASE
			#include "Include/StandardForward.cginc"
			ENDCG
		}
	
		Pass {
			Name "FORWARD"
			Tags { "LightMode" = "ForwardAdd" }

			Blend[_SrcBlend] One
			Fog{ Color(0, 0, 0, 0) }
			ZWrite Off 
			ZTest LEqual
			

			CGPROGRAM
			#pragma target 3.0
			//这里不用multi_compile可以减少最终AB包中Shader变体的数量，因为最终只需要第1个Pass（下同）
			#pragma shader_feature  _NORMALMAP
			#pragma shader_feature  _SPECULAR
			#pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON
			//#pragma shader_feature _SPECULARMAP 

			#pragma multi_compile_fwdadd_fullshadows
			#pragma multi_compile_fog
			#pragma multi_compile_fwdadd

			#pragma vertex vertAdd
			#pragma fragment fragAdd
			
			#define UNITY_PASS_FORWARDADD
			#include "Include/StandardForward.cginc"
			ENDCG
		}

		
		Pass {
			Name "ShadowCaster"
			Tags { "LightMode" = "ShadowCaster" }

			ZWrite On ZTest LEqual

			CGPROGRAM
			#pragma target 3.0
			#pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON

			#pragma multi_compile_shadowcaster

			#pragma vertex vertShadowCaster
			#pragma fragment fragShadowCaster
			
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "Include/StandardShadow.cginc"
			ENDCG
		}
		
		

		Pass {
			Name "Meta"
			Tags { "LightMode" = "Meta" }
		
			Cull Off

			CGPROGRAM
			#pragma shader_feature _EMISSION
			#pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON

			#pragma vertex vertMeta
			#pragma fragment fragMeta
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "Include/StandardMeta.cginc"
			ENDCG
		}
		

	}


	CustomEditor "ArtTech.LegacyStandardShaderGUI"
}
