﻿#ifndef STANDARD_SHADOW_INCLUDED
#define STANDARD_SHADOW_INCLUDED

#include "StandardCommon.cginc"


struct VertexOutputShadowCaster {
	V2F_SHADOW_CASTER;
#if defined(_ALPHATEST_ON)
	float2 tex : TEXCOORD1;
#endif
};


VertexOutputShadowCaster vertShadowCaster(appdata_full v) {
	VertexOutputShadowCaster o;
	UNITY_INITIALIZE_OUTPUT(VertexOutputShadowCaster, o);

	TRANSFER_SHADOW_CASTER_NORMALOFFSET(o)
#if defined(_ALPHATEST_ON)
	o.tex = TRANSFORM_TEX(v.texcoord, _MainTex);
#endif
	return o;
}

fixed4 fragShadowCaster(VertexOutputShadowCaster i) : SV_Target{
#if defined(_ALPHATEST_ON)
	half alpha = tex2D(_MainTex, i.tex).a * _Color.a;
	clip(alpha - _Cutoff);
#endif

	SHADOW_CASTER_FRAGMENT(i)
}


#endif