﻿#ifndef STANDARD_META_INCLUDED
#define STANDARD_META_INCLUDED

#include "StandardCommon.cginc"

#include "UnityMetaPass.cginc"


struct VertexOutputMeta {
	float4 pos : SV_POSITION;
	float4 pack0  : TEXCOORD0;
};


VertexOutputMeta vertMeta(appdata_full v) {
	VertexOutputMeta o;
	UNITY_INITIALIZE_OUTPUT(VertexOutputMeta, o);

	o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST);
	o.pack0.xy = TRANSFORM_TEX(v.texcoord, _MainTex);
	/*
#if defined (_EMISSION)
	o.pack0.zw = TRANSFORM_TEX(v.texcoord, _EmissionMap);
#endif
	*/

	return o;
}

fixed4 fragMeta(VertexOutputMeta i) : SV_Target{
	Input surfIN;
	UNITY_INITIALIZE_OUTPUT(Input, surfIN);
	surfIN.uv_MainTex = i.pack0.xy;
	/*
#if defined (_EMISSION)
	surfIN.uv_Illum = i.pack0.zw;
#endif
	*/

#ifdef UNITY_COMPILER_HLSL
	VSSurfaceOutput o = (VSSurfaceOutput)0;
#else
	VSSurfaceOutput o;
#endif
	o.Albedo = 0.0;
	o.Emission = 0.0;
#if defined (_SPECULAR)
	o.Specular = 0.0;
#endif
	o.Alpha = 0.0;
	//o.Gloss = 0.0;

	surf(surfIN, o);

#if defined(_ALPHATEST_ON)
	clip(o.Alpha - _Cutoff);
#endif

	UnityMetaInput metaIN;
	UNITY_INITIALIZE_OUTPUT(UnityMetaInput, metaIN);
	metaIN.Albedo = o.Albedo;
#if defined (_EMISSION)
	metaIN.Emission = o.Emission;
#endif
	return UnityMetaFragment(metaIN);
}


#endif