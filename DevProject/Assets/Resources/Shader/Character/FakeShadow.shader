﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/MOGO2/Character/FakeShadow" 
{
	Properties
	{
		_Color ("Color", Color) = (0,0,0,1)
	}

	SubShader
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		LOD 200
		
		Pass 
		{
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"	
		
			fixed4 _Color;

			struct v2f 
			{
				float4 pos : SV_POSITION;
				half2 uv : TEXCOORD0;
			};

			v2f vert (appdata_full v) 
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord;
				return o;
			}

			fixed4 frag(v2f i) : COLOR
			{
				fixed4 o = _Color;
				fixed2 c = i.uv * 2 - 1;
				fixed a = 1 - dot(c, c);
				o.a *= a;
				o.a = saturate(o.a);
				return o;
			}

			ENDCG
		}
	}

	FallBack "Transparent/Diffuse"
}
