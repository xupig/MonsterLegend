#ifndef __PLAYER_COMMON__
#define __PLAYER_COMMON__
		
#include "../Mogo2Include.cginc"
			
uniform sampler2D 	_MainTex;
uniform half4 		_MainTex_ST;
#if defined(__ALPHATEST_ON)
uniform half		_Cutoff;
#endif
uniform sampler2D 	_GlossTex;
uniform sampler2D 	_BRDFTex;
uniform half 		_SelfShadingLightDirX;
uniform half 		_SelfShadingLightDirY;
uniform half 		_SelfShadingLightDirZ;
uniform half		_Shininess;
uniform fixed		_SpecIntensity;
uniform fixed3 		_RimLightPower;
uniform fixed3 		_RimLightColorStrength;
uniform fixed       _RimLightStrength;
uniform fixed 		_RimLightWarp;
uniform fixed		_HighLight;
#if defined(_FLOW_FLASH_ON) || defined(_FLOW_FLASH2_ON)
uniform fixed3      _EmissiveColor1;
uniform sampler2D 	_FlowTex;
uniform fixed3		_FlowColor;
uniform half		_FlowSpeedX;
uniform half		_FlowSpeedY;
uniform samplerCUBE _FlashCubeTex;
uniform fixed3      _FlashColor;
uniform half        _FlashStrength;
uniform float4x4	_FlashNormalRotM;
#if defined(_FLOW_FLASH2_ON)
uniform fixed3      _FlashColor2;
uniform half        _FlashStrength2;			
uniform float4x4	_FlashNormalRotM2;
#endif
#endif
uniform fixed4		_AmbientColor;


struct v2f {	
	half4 pos : POSITION;
	half2 uv : TEXCOORD0;
	half2 brdf : TEXCOORD1;
	half3 view : TEXCOORD2;
	half4 normal : TEXCOORD3;
	UNITY_FOG_COORDS(4)
#if defined(_FLOW_FLASH_ON) || defined(_FLOW_FLASH2_ON)
	half2 uvFlow : TEXCOORD5;
	half3 uvFlash : TEXCOORD6;
#if defined(_FLOW_FLASH2_ON)
	half3 uvFlash2 : TEXCOORD7;
#endif
#endif
};

v2f vert(appdata_full v) {
	v2f o = (v2f)0;

	o.pos = UnityObjectToClipPos(v.vertex);
	o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);

	half3 wp = mul(unity_ObjectToWorld, v.vertex).xyz;
	o.normal.xyz = UnityObjectToWorldNormal(v.normal);			
	o.view = normalize(_WorldSpaceCameraPos.xyz - wp);
	half3 ld = normalize(half3(_SelfShadingLightDirX, _SelfShadingLightDirY, _SelfShadingLightDirZ));
	half3 h = normalize(ld + o.view);
	half nh = saturate(dot(o.normal.xyz, h));
	o.brdf = half2(saturate(dot(o.normal.xyz, ld)) * 0.5 + 0.5, nh);
	o.normal.w = pow(nh, _Shininess * 128) * _SpecIntensity;
				
#if defined(_FLOW_FLASH_ON) || defined(_FLOW_FLASH2_ON)
	o.uvFlow = o.normal.xy + _Time.xx * half2(_FlowSpeedX, _FlowSpeedY);
	o.uvFlash = mul(_FlashNormalRotM, half4(o.normal.xyz,0)).xyz;
#if defined(_FLOW_FLASH2_ON)
	o.uvFlash2 = mul(_FlashNormalRotM2, half4(o.normal.xyz, 0)).xyz;
#endif
#endif

	UNITY_TRANSFER_FOG(o, o.pos);
	return o;
}

			
			
fixed4 frag(v2f i) : SV_Target {
	fixed4 clr = tex2D(_MainTex, i.uv);
#if defined(__ALPHATEST_ON)
	clip(clr.a - _Cutoff);
#endif
	fixed4 gls = tex2D(_GlossTex, i.uv);
	fixed3 brdf = tex2D(_BRDFTex, i.brdf).rgb;
#if defined(_FLOW_FLASH_ON) || defined(_FLOW_FLASH2_ON)
	fixed4 fl1 = tex2D(_FlowTex, i.uvFlow);
#endif

	fixed vdn = saturate(dot(i.view, i.normal.xyz));
	fixed3 facing = (1.0 - vdn);
	facing.gb *= facing.gb;
	facing.b *= facing.b;
	fixed rim = dot(facing, _RimLightPower) * saturate(i.brdf.x - _RimLightWarp);

	clr.rgb *= i.normal.w * gls.r + brdf * gls.g + _AmbientColor;

#if defined(_FLOW_FLASH_ON) || defined(_FLOW_FLASH2_ON)
	clr.rgb += gls.b * (fl1.rgb * _FlowColor 

			+ texCUBE(_FlashCubeTex, i.uvFlash) * _FlashColor * _FlashStrength
#if defined(_FLOW_FLASH2_ON)
			+ texCUBE(_FlashCubeTex, i.uvFlash2) * _FlashColor2 * _FlashStrength2
#endif
		);
#endif
		
	clr.rgb = saturate(clr.rgb);
	fixed3 rc = Overlay(clr.rgb, saturate(_RimLightColorStrength * _RimLightStrength));
	clr.rgb *= _HighLight;
	clr.rgb	= lerp(clr.rgb, rc, rim);
	UNITY_APPLY_FOG(i.fogCoord, clr);
#if !defined(__ALPHABLEND_ON) && !defined(__ALPHATEST_ON)
	clr.a = 1;
#endif
			
	return clr;
}


#endif