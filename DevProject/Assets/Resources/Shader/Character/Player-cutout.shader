// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'


Shader "XGame/MOGO2/Character/Player-cutout" {
	Properties {
		[MaterialEnum(On,0, Off,2)] _Cull("2-Sided", Float) = 2
		_MainTex("Base(RGB)", 2D) = "grey" {}
		_Cutoff("Alpha cutoff", Range(0,1)) = 0.5
		_GlossTex("Gloss(R) BRDF(G) Flow(B)", 2D) = "white" {}
		_BRDFTex("BRDF", 2D) = "grey" {}
		_SelfShadingLightDirX("SelfShadingLight Dir X", Range(-1.0, 1.0)) = 1.0
		_SelfShadingLightDirY("SelfShadingLight Dir Y", Range(-1.0, 1.0)) = 0.5
		_SelfShadingLightDirZ("SelfShadingLight Dir Z", Range(-1.0, 1.0)) = 0.5
		_Shininess("Shininess", Range(0.01, 1.0)) = 0.5
		_SpecIntensity("Specular Intensity", Range(0, 5.0)) = 1.0
		_RimLightPower("RimPower", Color) = (0.4, 0.0, 1.0, 0.0)
		_RimLightColorStrength("RimColor", Color) = (1.0, 1.0, 0.0, 1.0)
		_RimLightStrength("RimStrength", Range(0.0, 4.0)) = 1
		_RimLightWarp("RimWarp", Range(-1.0, 1.0)) = -1.0
		[HideInInspector]_HighLight("HighLight", float) = 1.0
		[HideInInspector]_FlowTex("FlowTex(RGB)", 2D) = "white" {}
		[HideInInspector]_FlowColor("FlowColor", Color) = (1, 1, 1, 1)
		[HideInInspector]_FlowSpeedX("Flow Speed X", float) = 0.5
		[HideInInspector]_FlowSpeedY("Flow Speed Y", float) = 0.5
		[HideInInspector]_FlashCubeTex("FlashCubeTex", Cube) = "_SkyBox" {}
		[HideInInspector]_FlashColor("FlashColor", Color) = (1.0, 1.0, 1.0, 1.0)
		[HideInInspector]_FlashStrength("FlashStrength", float) = 1
		[HideInInspector]_FlashColor2("FlashColor2", Color) = (1.0, 1.0, 1.0, 1.0)
		[HideInInspector]_FlashStrength2("FlashStrength2", float) = 1
		_AmbientColor("AmbientColor", Color) = (0.212, 0.227, 0.259, 1)
	}

	SubShader {
		LOD 300
		Tags { "RenderType" = "TransparentCutout" "Queue" = "AlphaTest" "LightMode" = "ForwardBase"}

		Pass {
			Cull[_Cull]

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			//#pragma multi_compile_fog
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma multi_compile __ _FLOW_FLASH_ON _FLOW_FLASH2_ON

			#define __ALPHATEST_ON
			#include "PlayerCommon.cginc"			
			ENDCG
		}

	}
				
	Fallback "Diffuse"
}
