// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'


Shader "XGame/MOGO2/Character/Boss_2UV" 
{
	Properties 
	{
		[MaterialEnum(On,0, Off,2)] _Cull("2-Sided", Float) = 2
		_MainTex ("Base(RGB)", 2D) = "grey" {}
		_BRDFTex ("BRDF", 2D) = "grey" {}
		_SelfShadingLightDirX("SelfShadingLight Dir X", Range(-1.0, 1.0)) = 1.0
		_SelfShadingLightDirY("SelfShadingLight Dir Y", Range(-1.0, 1.0)) = 0.5
		_SelfShadingLightDirZ("SelfShadingLight Dir Z", Range(-1.0, 1.0)) = 0.5
		_Region("Region Specular(R) Emissive(B)", 2D) = "white" {}
		_SpecIntensity("Specular Intensity", Range(0, 5.0)) = 1.0
		_RimPower ("RimPower", Color) = (0.4, 0.0, 1.0, 0.0)
		_RimColor ("RimColor", Color) = (1.0, 1.0, 0.0, 1.0)
		_RimStrength("RimStrength", Range(0.5, 4.0)) = 1
		[HideInInspector]_HighLight("HighLight", float) = 1.0
		[MaterialToggle] Emissive_Flow("EmissiveFlow?", float) = 0
		_EmissiveColor("EmissiveColor", Color) = (1.0, 1.0, 1.0, 1)
		_FlowTex("FlowTex(RGB)", 2D) = "white" {}
		_FlowDirX("Flow Dir X", Range(-1.0, 1.0)) = 0.5
		_FlowDirY("Flow Dir Y", Range(-1.0, 1.0)) = 0.5
		_AmbientColor("AmbientColor", Color) = (0.212, 0.227, 0.259, 1)

		_VestColor ("Vest Color", Color) = (1.0, 1.0, 1.0, 1.0)
	}

	SubShader 
	{
		LOD 300
		Tags { "RenderType"="Opaque" "Queue"="Geometry+20" "LightMode"="ForwardBase"}	
				
		Pass 
		{
			Cull[_Cull]
	
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma multi_compile __ EMISSIVE_FLOW_ON
			#pragma multi_compile LIGHTPROBE_OFF LIGHTPROBE_ON
			#include "../Mogo2Include.cginc"
			
			uniform sampler2D 	_MainTex;
			uniform half4 		_MainTex_ST;
			uniform sampler2D 	_BRDFTex;
			uniform half 		_SelfShadingLightDirX;
			uniform half 		_SelfShadingLightDirY;
			uniform half 		_SelfShadingLightDirZ;
			uniform sampler2D   _Region;
			uniform fixed		_SpecIntensity;
			uniform fixed3 		_RimColor;
			uniform fixed       _RimStrength;
			uniform fixed3 		_RimPower;
			uniform fixed		_HighLight;
			#ifdef EMISSIVE_FLOW_ON
			uniform fixed3      _EmissiveColor;
			uniform sampler2D 	_FlowTex;
			uniform float		_FlowDirX;
			uniform float		_FlowDirY;
			#endif
			uniform fixed4		_AmbientColor;

			struct v2f
			{
				half4 pos : POSITION;
				half2 uv : TEXCOORD0;
				half3 normal : TEXCOORD1;
				half3 light : TEXCOORD2;
				half3 view : TEXCOORD3;
				//fixed4 fogParam : TEXCOORD4;
				float2 uvFlow : TEXCOORD5;
				half3 SHLight : TEXCOORD6;
			};
			
			v2f vert(appdata_full v)
			{
				v2f o = (v2f)0;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				half3 wp = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.normal = normalize(mul((half3x3)unity_ObjectToWorld, SCALED_NORMAL));
				o.light = normalize(half3(_SelfShadingLightDirX, _SelfShadingLightDirY, _SelfShadingLightDirZ));
				o.view = normalize(_WorldSpaceCameraPos.xyz - wp);
				//o.fogParam = ComputeFogParam(o.pos.w, wp);
				#ifdef EMISSIVE_FLOW_ON
				o.uvFlow = v.texcoord1 + _Time.y * float2(_FlowDirX, _FlowDirY);
				#endif

				#ifdef LIGHTPROBE_ON
				o.SHLight = ShadeSH9(half4(o.normal.xyz, 1.0));
				#endif

				return o;
			}
			
			fixed4 frag(v2f i) : COLOR 
			{
				fixed4 clr = tex2D(_MainTex, i.uv);
				fixed4 rg = tex2D(_Region, i.uv);
				half nl = saturate(dot(i.normal, i.light)) * 0.5 + 0.5;
				half nh = saturate(dot(i.normal, normalize(i.light + i.view)));
				fixed4 brdf = tex2D(_BRDFTex, half2(nl, nh));
				#ifdef EMISSIVE_FLOW_ON
				fixed4 fl = tex2D(_FlowTex, i.uvFlow);
				#endif

				fixed vdn = saturate(dot(i.view, i.normal.xyz));
				fixed3 facing = (1.0 - vdn);
				facing.gb *= facing.gb;
				facing.b *= facing.b;
				fixed rim = dot(facing, _RimPower) * nl;
				fixed3 rc = Overlay(clr.rgb, _RimColor * _RimStrength);

				#ifdef LIGHTPROBE_ON
				clr.rgb *= (brdf.rgb + brdf.a * rg.r * _SpecIntensity + i.SHLight);
				#else
				clr.rgb *= (brdf.rgb + _AmbientColor.rgb + brdf.a * rg.r * _SpecIntensity);
				#endif
				
				#ifdef EMISSIVE_FLOW_ON
				clr.rgb += rg.b * fl * _EmissiveColor;
				#endif
				clr.rgb *= _HighLight;
				clr.rgb	= lerp(clr.rgb, rc, rim);
				//clr.rgb = MixFogColor(clr.rgb, i.fogParam);
			
				return clr;
			}
			
			ENDCG
		}

	}
	
	Fallback "Diffuse"

	CustomEditor "CustomTrilightMaterialInspector"
}
