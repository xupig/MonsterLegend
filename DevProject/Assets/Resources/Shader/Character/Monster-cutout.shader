Shader "XGame/MOGO2/Character/Monster-cutout" {
	Properties {
		[MaterialEnum(On,0, Off,2)] _Cull("2-Sided", Float) = 2
		_MainTex ("Base(RGB)", 2D) = "grey" {}
		_Cutoff("Alpha cutoff", Range(0,1)) = 0.5
		_BRDFTex ("BRDF", 2D) = "grey" {}
		_SelfShadingLightDirX("SelfShadingLight Dir X", Range(-1.0, 1.0)) = 1.0
		_SelfShadingLightDirY("SelfShadingLight Dir Y", Range(-1.0, 1.0)) = 0.5
		_SelfShadingLightDirZ("SelfShadingLight Dir Z", Range(-1.0, 1.0)) = 0.5
		_Shininess("Shininess", Range(0.01, 1.0)) = 0.5
		_SpecIntensity("Specular Intensity", Range(0, 5.0)) = 1.0
		_RimPower ("RimPower", Color) = (0.4, 0.0, 1.0, 0.0)
		_RimColor ("RimColor", Color) = (1.0, 1.0, 0.0, 1.0)
		_RimStrength("RimStrength", Range(0.0, 4.0)) = 1
		[HideInInspector]_HighLight("HighLight", float) = 1.0
		_Region("Region Emissive(B)", 2D) = "white" {}
		[MaterialToggle] Emissive_Flow("EmissiveFlow?", float) = 0
		_EmissiveColor("EmissiveColor", Color) = (1.0, 1.0, 1.0, 1)
		_FlowTex("FlowTex(RGB)", 2D) = "white" {}
		_FlowDirX("Flow Dir X", Range(-1.0, 1.0)) = 0.5
		_FlowDirY("Flow Dir Y", Range(-1.0, 1.0)) = 0.5
		_AmbientColor("AmbientColor", Color) = (0.212, 0.227, 0.259, 1)
		
		_VestColor ("Vest Color", Color) = (1.0, 1.0, 1.0, 1.0)
	}

	SubShader {
		LOD 300
		Tags{ "RenderType" = "TransparentCutout" "Queue" = "AlphaTest" "LightMode" = "ForwardBase" }
				
		Pass {
			Cull[_Cull]
	
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma multi_compile __ EMISSIVE_FLOW_ON
			#pragma multi_compile_fog

			#define __ALPHATEST_ON
			#include "MonsterCommon.cginc"		
			ENDCG
		}
	}

	Fallback "Diffuse"
}
