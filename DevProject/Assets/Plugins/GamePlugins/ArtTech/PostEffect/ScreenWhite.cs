﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ShaderUtils;
using UnityEngine;

namespace ArtTech.ImageEffects {
    [ExecuteInEditMode]
    public class ScreenWhite : PlayableBase {
        public Color _color = Color.white;
        [Range(0, 1)]
        public float _alpha;

        private Material _material;
        private Material Mat {
            get {
                if (_material == null) {
                    _material = new Material(ShaderRuntime.loader.Find("XGame/Aion/ScreenColor"));
                    _material.hideFlags = HideFlags.HideAndDontSave;
                }
                return _material;
            }
        }

        private void Start() {
            
        }

        private void OnDestroy() {
            if (_material != null) {
                DestroyImmediate(_material);
                _material = null;
            }
        }


        private void OnPostRender() {
            if (_alpha == 0)
                return;

            GL.PushMatrix();
            Mat.SetPass(0);
            Mat.color = new Color(_color.r, _color.g, _color.b, _alpha);
            GL.LoadOrtho();
            GL.Begin(GL.QUADS);
            GL.Vertex3(0, 0, 0);
            GL.Vertex3(0, 1, 0);
            GL.Vertex3(1, 1, 0);
            GL.Vertex3(1, 0, 0);
            GL.End();
            GL.PopMatrix();
        }

        public void Play(Color startColor, float startAlpha, Color endColor, float endAlpha, float duration, Action finish, bool disableOnPlayFinish) {
            PlayBase((ft) => {
                _color = Color.Lerp(startColor, endColor, ft);
                _alpha = Mathf.Lerp(startAlpha, endAlpha, ft);
            }, duration, finish, disableOnPlayFinish);
        }

        public void Play(float start, float end, float duration, Action finish = null, bool disableOnPlayFinish = false)
        {
            PlayBase((ft) =>
            {
                _alpha = Mathf.Lerp(start, end, ft);
            }, duration, finish, disableOnPlayFinish);
        }

    }
}


