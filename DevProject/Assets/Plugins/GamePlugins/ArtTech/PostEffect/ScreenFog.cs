﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ArtTech.ImageEffects {
    public class ScreenFog : PlayableBase {
        private void Start() {

        }

        public void Play(Color fogColorStart, float fogStartStart, float fogEndStart,
                    Color fogColorEnd, float fogStartEnd, float fogEndEnd, float duration, Action finish, bool disableOnPlayFinish) {
            PlayBase((ft) => {
                RenderSettings.fogColor = Color.Lerp(fogColorStart, fogColorEnd, ft);
                RenderSettings.fogStartDistance = Mathf.Lerp(fogStartStart, fogStartEnd, ft);
                RenderSettings.fogEndDistance = Mathf.Lerp(fogEndStart, fogEndEnd, ft);
            }, duration, finish, disableOnPlayFinish);
        }
    }

}

