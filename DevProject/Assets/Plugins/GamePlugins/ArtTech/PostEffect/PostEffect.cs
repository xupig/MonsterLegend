﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ShaderUtils;


namespace ArtTech.ImageEffects {
    [AddComponentMenu("ArtTech/Image Effects/PostEffect")]

    [RequireComponent(typeof(Camera))]
    //[ExecuteInEditMode]
    [DisallowMultipleComponent]

    public partial class PostEffect : MonoBehaviour {
        public bool _bloomEnabled;
        public bool BloomEnabled {
            get { return _bloomEnabled; }
            set { if (_bloomEnabled != value) { _bloomEnabled = value; EnableBloomKeywords(); } }
        }

        [System.Serializable]
        public class BloomParameters {
            public float _threshold = 2f;
            public float _maxBrightness = 2.01f;
            [Range(0, 1)]
            public float _softKnee = 0f;
            [Range(1, 7)]
            public float _radius = 7;
            public float _intensity = 1;
        }


        public bool _radialBlurEnabled;
        public bool RadialBlurEnabled {
            get { return _radialBlurEnabled; }
            set { if (_radialBlurEnabled != value) { _radialBlurEnabled = value; EnableRadialBlurKeywords(); } }
        }

        [System.Serializable]
        public class RadiaBlurParameters {
            public enum QualityPreset {
                Low = 4,
                Medium = 8,
                High = 12,
                //Custom
            }

            [Range(0f, 1f)]
            public float _strength = 0.1f;
            //[Range(2, 32)]
            //public int _samples = 10;
            public Vector2 _center = new Vector2(0.5f, 0.5f);
            public QualityPreset _quality = QualityPreset.Medium;
            [Range(-100f, 100f)]
            public float _sharpness = 40f;
            [Range(0f, 100f)]
            public float _darkness = 35f;
            public bool _enableVignette = true;
            [Range(0.01f, 1)]
            public float _vignetteSize = 1;
        }


        public bool _screenDarkEnabled;
        public bool ScreenDarkEnabled {
            get { return _screenDarkEnabled; }
            set { if (_screenDarkEnabled != value) { _screenDarkEnabled = value; EnableScreenDarkKeywords(); } }
        }

        [System.Serializable]
        public class ScreenDarkParameters {
            [Range(0, 1)]
            public float _alpha;
            public LayerMask _layers;
        }


        public class BloomImpl {
            public const int _kMaxIterations = 16;
            public static readonly RenderTexture[] _blurBuffer1 = new RenderTexture[_kMaxIterations];
            public static readonly RenderTexture[] _blurBuffer2 = new RenderTexture[_kMaxIterations];
        }

       
        private Camera _focusCamera;
        public Camera FocusCamera {
            get {
                if (_focusCamera == null) {
                    GameObject go = new GameObject("__FocusCamera__");
                    go.hideFlags = HideFlags.DontSave;
                    go.transform.SetParent(_camera.transform);
                    go.transform.localPosition = Vector3.zero;
                    go.transform.localRotation = Quaternion.identity;
                    _focusCamera = go.AddComponent<Camera>();
                    _focusCamera.clearFlags = CameraClearFlags.SolidColor;
                    //_focusCamera.cullingMask = _screenDarkParameters._layers;
                    _focusCamera.backgroundColor = Color.black;
                    _focusCamera.enabled = false;
                }

                return _focusCamera;
            }
        }


        public BloomParameters _bloomParameters = new BloomParameters();
        private BloomImpl _bloomImplement = new BloomImpl();

        public RadiaBlurParameters _radialBlurParameters = new RadiaBlurParameters();

        public ScreenDarkParameters _screenDarkParameters = new ScreenDarkParameters();




        static int _supportHDR = -1;
        const string _shaderName = "Hidden/PostEffect";
        Shader _shader;
        Material _material;

        bool _supported;
        Camera _camera;

        void Awake() {
            _camera = this.GetComponent<Camera>();

            if (_supportHDR == -1) {
                _supportHDR = SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGBHalf) ? 1 : 0;
                if (_supportHDR == 0) {
                    Debug.Log("[PostEffect.Awake] no HDR texture support!");
                }
            }
        }

        void OnDestroy() {
            if (_material != null) {
                DestroyImmediate(_material);
                _material = null;
            }

            if (_focusCamera != null) {
                DestroyImmediate(_focusCamera.gameObject);
                _focusCamera = null;
            }
        }

        bool Supported() {
            if (_supported)
                return true;

            _supported = (SystemInfo.supportsImageEffects && _shader.isSupported);
            return _supported;
        }

        bool CreateMaterial() {
            if (_shader == null)
                _shader = ShaderRuntime.loader.Find(_shaderName);

            if (_shader == null)
                return false;

            if (!_shader.isSupported)
                return false;

            if (_material == null) {
                _material = new Material(_shader);
                _material.hideFlags = HideFlags.HideAndDontSave;
                EnableShaderKeywords();
            }

            return true;
        }

        bool CreateResources() {
            if (!CreateMaterial())
                return false;

            if (!Supported())
                return false;

            return true;
        }

        public void EnableShaderKeywords() {
            EnableBloomKeywords();
            EnableRadialBlurKeywords();
            EnableScreenDarkKeywords();
        }

        void EnableBloomKeywords() {
            if (!_material) return;

            if (_bloomEnabled && _supportHDR == 1) {
                _material.EnableKeyword("BLOOM_ON");
                _material.DisableKeyword("BLOOM_OFF");
            } else {
                _material.EnableKeyword("BLOOM_OFF");
                _material.DisableKeyword("BLOOM_ON");
            }
        }

        void EnableRadialBlurKeywords() {
            if (!_material) return;

            if (_radialBlurEnabled) {
                if (_radialBlurParameters._enableVignette) {
                    _material.EnableKeyword("RB_VIGN");
                    _material.DisableKeyword("RB_NO_VIGN");
                    _material.DisableKeyword("RB_OFF");
                } else {
                    _material.EnableKeyword("RB_NO_VIGN");
                    _material.DisableKeyword("RB_VIGN");
                    _material.DisableKeyword("RB_OFF");
                }

            } else {
                _material.EnableKeyword("RB_OFF");
                _material.DisableKeyword("RB_VIGN");
                _material.DisableKeyword("RB_NO_VIGN");
            }
        }

        void EnableScreenDarkKeywords() {
            if (!_material) return;

            if (_screenDarkEnabled) {
                _material.EnableKeyword("SCREEN_DARK_ON");
                _material.DisableKeyword("SCREEN_DARK_OFF");
            } else {
                _material.EnableKeyword("SCREEN_DARK_OFF");
                _material.DisableKeyword("SCREEN_DARK_ON");
            }
        }




        private void OnPreRender() {
            if (_bloomEnabled && _supportHDR == 1) {
                if (!_camera.hdr)
                    _camera.hdr = true;
                if (QualitySettings.antiAliasing != 0)
                    QualitySettings.antiAliasing = 0;
            } else {
                if (_camera.hdr)
                    _camera.hdr = false;
            }
        }



        List<RenderTexture> _tempRTList = new List<RenderTexture>();


        void DoBloom(RenderTexture source) {
            bool HighQuality = false;
            bool AntiFlicker = false;

            var useRGBM = Application.isMobilePlatform;

            // source texture size
            var tw = source.width;
            var th = source.height;

            // halve the texture size for the low quality mode
            if (HighQuality) {
                tw /= 2;
                th /= 2;
            } else {
                tw /= 4;
                th /= 4;
            }

            RenderTexture destination = RenderTexture.GetTemporary(tw, th, 0, RenderTextureFormat.ARGB32);
            _tempRTList.Add(destination);

            // blur buffer format
            var rtFormat = useRGBM ? RenderTextureFormat.Default : RenderTextureFormat.DefaultHDR;

            // determine the iteration count
            var logh = Mathf.Log(th, 2) + _bloomParameters._radius - 8;
            var logh_i = (int)logh;
            var iterations = Mathf.Clamp(logh_i, 1, BloomImpl._kMaxIterations);

            // update the shader properties
            var threshold = Mathf.GammaToLinearSpace(_bloomParameters._threshold);
            var maxBrightness = Mathf.GammaToLinearSpace(_bloomParameters._maxBrightness);

            _material.SetVector("_Threshold", new Vector2(threshold, maxBrightness));

            var knee = threshold * _bloomParameters._softKnee + 1e-5f;
            var curve = new Vector3(threshold - knee, knee * 2, 0.25f / knee);
            _material.SetVector("_Curve", curve);

            var pfo = !HighQuality && AntiFlicker;
            _material.SetFloat("_PrefilterOffs", pfo ? -0.5f : 0.0f);

            _material.SetFloat("_SampleScale", 0.5f + logh - logh_i);
            _material.SetFloat("_Intensity", Mathf.Max(0.0f, _bloomParameters._intensity));

            var prefiltered = RenderTexture.GetTemporary(tw, th, 0, rtFormat);

            Graphics.Blit(source, prefiltered, _material, AntiFlicker ? 2 : 1);

            // construct A mip pyramid
            var last = prefiltered;
            for (var level = 0; level < iterations; level++) {
                BloomImpl._blurBuffer1[level] = RenderTexture.GetTemporary(last.width / 2, last.height / 2, 0, rtFormat);
                Graphics.Blit(last, BloomImpl._blurBuffer1[level], _material, level == 0 ? (AntiFlicker ? 4 : 3) : 5);
                last = BloomImpl._blurBuffer1[level];
            }

            // upsample and combine loop
            for (var level = iterations - 2; level >= 0; level--) {
                var basetex = BloomImpl._blurBuffer1[level];
                _material.SetTexture("_BaseTex", basetex);
                BloomImpl._blurBuffer2[level] = RenderTexture.GetTemporary(basetex.width, basetex.height, 0, rtFormat);
                Graphics.Blit(last, BloomImpl._blurBuffer2[level], _material, HighQuality ? 7 : 6);
                last = BloomImpl._blurBuffer2[level];
            }

            destination.DiscardContents();
            Graphics.Blit(last, destination, _material, HighQuality ? 9 : 8);

            _material.SetTexture("_BloomTex", destination);


            for (var i = 0; i < BloomImpl._kMaxIterations; i++) {
                if (BloomImpl._blurBuffer1[i] != null) RenderTexture.ReleaseTemporary(BloomImpl._blurBuffer1[i]);
                if (BloomImpl._blurBuffer2[i] != null) RenderTexture.ReleaseTemporary(BloomImpl._blurBuffer2[i]);
                BloomImpl._blurBuffer1[i] = null;
                BloomImpl._blurBuffer2[i] = null;
            }
            RenderTexture.ReleaseTemporary(prefiltered);
        }



        void DoRadialBlur() {
            //int samples = _radialBlurParameters._quality == RadiaBlurParameters.QualityPreset.Custom ? _radialBlurParameters._samples : (int)_radialBlurParameters._quality;
            int samples = (int)_radialBlurParameters._quality; //此参数无效，最终在shader为8

            _material.SetVector("_Center", _radialBlurParameters._center);
            _material.SetVector("_Params", new Vector4(_radialBlurParameters._strength, samples, _radialBlurParameters._sharpness * 0.01f, _radialBlurParameters._darkness * 0.02f));
            _material.SetFloat("_VignetteSize", _radialBlurParameters._vignetteSize);
        }

        void DoScreenDark(RenderTexture source) {
            RenderTexture layerTargetTexture = RenderTexture.GetTemporary(source.width, source.height, 16, RenderTextureFormat.Default);
            _tempRTList.Add(layerTargetTexture);

            FocusCamera.cullingMask = _screenDarkParameters._layers;
            FocusCamera.targetTexture = layerTargetTexture;
            FocusCamera.projectionMatrix = _camera.projectionMatrix;
            FocusCamera.Render();

            _material.SetFloat("_DarkAlpha", _screenDarkParameters._alpha);
            _material.SetTexture("_DarkLayerTex", layerTargetTexture);
        }

        void OnRenderImage(RenderTexture source, RenderTexture destination) {
            if (!CreateResources()) {
                Graphics.Blit(source, destination);
                return;
            }


            if (_bloomEnabled && _supportHDR == 1)
                DoBloom(source);

            if (_radialBlurEnabled)
                DoRadialBlur();

            if (_screenDarkEnabled)
                DoScreenDark(source);

            Graphics.Blit(source, destination, _material, 0);


            for (int i = 0; i < _tempRTList.Count; i++)
                RenderTexture.ReleaseTemporary(_tempRTList[i]);
            _tempRTList.Clear();

        }

    }
}