﻿using System;
using UnityEngine;


namespace ArtTech.ImageEffects {
    [AddComponentMenu("")]
    public class PostEffectProxy : MonoBehaviour {
        PostEffect _currentPostEffect;
        ScreenWhite _currentScreenWhite;
        ScreenFog _currentScreenFog;


        bool _optnBloomEnabled = true;
        bool _optnRadialEnabled = true;

        bool OptnBloomEnabled { get { return _optnBloomEnabled; } set { _optnBloomEnabled = value; } }
        bool OptnRadialEnabled { get { return _optnRadialEnabled; } set { _optnRadialEnabled = value; } }


        bool _rawBloomEnabled = false;




        private void Awake() {
            _currentPostEffect = GetComponent<PostEffect>();
            if (_currentPostEffect == null)
                Debug.LogError("[PostEffectProxy.Awake] PostEffect is null!");

            _currentScreenWhite = GetComponent<ScreenWhite>();
            if (_currentScreenWhite == null)
                Debug.LogError("[PostEffectProxy.Awake] ScreenWhite is null!");

            _currentScreenFog = GetComponent<ScreenFog>();
            if (_currentScreenFog == null)
                Debug.LogError("[PostEffectProxy.Awake] ScreenFog is null!");
        }

        private void Start() {
            _rawBloomEnabled = _currentPostEffect.BloomEnabled;

            OnChangeQualityLevel();
        }

        public bool Enabled {
            get { return _currentPostEffect.enabled; }
            set { _currentPostEffect.enabled = value; }
        }

        public bool BloomEnabled {
            set { _currentPostEffect.BloomEnabled = value && OptnBloomEnabled; }
        }
        public float BloomThreshold {
            set { _currentPostEffect._bloomParameters._threshold = value; }
        }
        public float BloomMaxBrightness {
            set { _currentPostEffect._bloomParameters._maxBrightness = value; }
        }
        public float BloomSoftKnee {
            set { _currentPostEffect._bloomParameters._softKnee = value; }
        }
        public float BloomRadius {
            set { _currentPostEffect._bloomParameters._radius = value; }
        }
        public float BloomIntensity {
            set { _currentPostEffect._bloomParameters._intensity = value; }
        }

        public int ScreenDarkLayers {
            set { _currentPostEffect._screenDarkParameters._layers = value; }
        }


        public bool ScreenWhiteEnabled {
            set { _currentScreenWhite.enabled = value; }
        }
        public Color ScreenWhiteColor {
            set { _currentScreenWhite._color = value; }
        }
        public float ScreenWhiteAlpha {
            set { _currentScreenWhite._alpha = value; }
        }

        public bool RadialBlurEnabled {
            get { return _currentPostEffect.RadialBlurEnabled; }
            set { _currentPostEffect.RadialBlurEnabled = value; }
        }

        public float RadialBlurDarkness {
            set { _currentPostEffect._radialBlurParameters._darkness = value; }
        }

        public float RadialBlurStrength {
            set { _currentPostEffect._radialBlurParameters._strength = value; }
        }

        public float RadialBlurSharpness {
            set { _currentPostEffect._radialBlurParameters._sharpness = value; }
        }

        public float RadialBlurVignetteSize {
            set { _currentPostEffect._radialBlurParameters._vignetteSize = value; }
        }


        public void PlayDOF(DofParameter StartParameter, DofParameter endParameter, float duration) {
        }

        public void PlayGodRay(GodRayParameter startParameter, GodRayParameter endParameter, float duration) {
        }

        public void PlayHdrBloom(HdrBloomParameter startParameter, HdrBloomParameter endParameter, float duration) {
        }

        public void PlayScreenWhite(ScreenWhiteParameter startParameter, ScreenWhiteParameter endParameter, float duration, Action finish, bool disableOnPlayFinish) {
            if (!_currentPostEffect.enabled)
                _currentPostEffect.enabled = true;

            Color startColor = startParameter != null ? startParameter.Color : _currentScreenWhite._color;
            float startAlpha = startParameter != null ? startParameter.Alpha : _currentScreenWhite._alpha;
            Color endColor = endParameter != null ? endParameter.Color : _currentScreenWhite._color;
            float endAlpha = endParameter != null ? endParameter.Alpha : _currentScreenWhite._alpha;
            _currentScreenWhite.Play(startColor, startAlpha, endColor, endAlpha, duration, finish, disableOnPlayFinish);
        }

        public void PlayRadialBlur(RadiaBlurParameter startParameter, RadiaBlurParameter endParameter, float duration) {
        }


        public void PlayRadialBlur(float startDarkness, float startStrength, float startSharpness, float startVignetteSize,
                    float endDarkness, float endStrength, float endSharpness, float endVignetteSize,
                    float duration, Action finish, bool disableOnPlayFinish) {
            if (!OptnRadialEnabled)
                return;

            if (!_currentPostEffect.enabled)
                _currentPostEffect.enabled = true;
            _currentPostEffect.PlayRadialBlur(startDarkness, startStrength, startSharpness, startVignetteSize,
                        endDarkness, endStrength, endSharpness, endVignetteSize,
                        duration, finish, disableOnPlayFinish);
        }

        public void PlayScreenDark(float start, float end, float duration, Action finish, bool disableOnPlayFinish) {
            if (!_currentPostEffect.enabled)
                _currentPostEffect.enabled = true;
            _currentPostEffect.PlayScreenDark(start, end, duration, finish, disableOnPlayFinish);
        }

        public void PlayScreenWhite(Color startColor, float startAlpha, Color endColor, float endAlpha, float duration, Action finish, bool disableOnPlayFinish) {
            if (!_currentScreenWhite.enabled)
                _currentScreenWhite.enabled = true;
            _currentScreenWhite.Play(startColor, startAlpha, endColor, endAlpha, duration, finish, disableOnPlayFinish);
        }

        public void PlayScreenWhite(float start, float end, float duration, Action finish, bool disableOnPlayFinish)
        {
            if (!_currentScreenWhite.enabled)
                _currentScreenWhite.enabled = true;
            _currentScreenWhite.Play(start, end, duration, finish, disableOnPlayFinish);
        }

        public void PlayScreenFog(Color fogColorStart, float fogStartStart, float fogEndStart,
                    Color fogColorEnd, float fogStartEnd, float fogEndEnd,
                    float duration, Action finish, bool disableOnPlayFinish) {
            if (!_currentScreenFog.enabled)
                _currentScreenFog.enabled = true;
            _currentScreenFog.Play(fogColorStart, fogStartStart, fogEndStart,
                fogColorEnd, fogStartEnd, fogEndEnd, duration, finish, disableOnPlayFinish);
        }


        public void OnChangeQualityLevel() {
            if (_currentPostEffect == null)
                return;

            switch (PostEffectQuality.QualityLevel) {
                case PostEffectQuality.Quality.High:
                    OptnBloomEnabled = true;
                    OptnRadialEnabled = true;
                    _currentPostEffect.BloomEnabled = _rawBloomEnabled;
                    break;

                case PostEffectQuality.Quality.Medium:
                    OptnBloomEnabled = false;
                    OptnRadialEnabled = true;
                    _currentPostEffect.BloomEnabled = false;
                    break;

                case PostEffectQuality.Quality.Low:
                    OptnBloomEnabled = false;
                    OptnRadialEnabled = false;
                    _currentPostEffect.BloomEnabled = false;
                    break;
            }
        }



        private void Update() {
            if (_currentPostEffect.BloomEnabled || _currentPostEffect.RadialBlurEnabled || _currentPostEffect.ScreenDarkEnabled) {
                if (!_currentPostEffect.enabled)
                    _currentPostEffect.enabled = true;
            } else {
                if (_currentPostEffect.enabled)
                    _currentPostEffect.enabled = false;
            }
        }
    }
}

