﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace ArtTech.ImageEffects {

    public partial class PostEffect {


        public enum PostEffectType {
            ScreenDark = 0,
            RadialBlur
        }

        public struct PlayData {
            public PostEffectType _type;
            public Action<float> _moveAction;
            public float _duration;
            public Action _callback;
            public Action _disableOnPlayFinishCallback;

            public float _startTime;
            public bool _isRunning;
        }

        List<PlayData> _playDatas = new List<PlayData>();


        public void PlayRadialBlur(float startDarkness, float startStrength, float startSharpness, float startVignetteSize,
                            float endDarkness, float endStrength, float endSharpness, float endVignetteSize,
                            float duration, Action finish, bool disableOnPlayFinish) {

            RadialBlurEnabled = true;
            Action disableOnPlayFinishCallback = null;
            if (disableOnPlayFinish)
                disableOnPlayFinishCallback = () => { RadialBlurEnabled = false; };

            PlayBase(PostEffectType.RadialBlur, (ft) => {
                _radialBlurParameters._sharpness = Mathf.Lerp(startSharpness, endSharpness, ft);
                _radialBlurParameters._strength = Mathf.Lerp(startStrength, endStrength, ft);
                _radialBlurParameters._darkness = Mathf.Lerp(startDarkness, endDarkness, ft);
                _radialBlurParameters._vignetteSize = Mathf.Lerp(startVignetteSize, endVignetteSize, ft);
            }, duration, finish, disableOnPlayFinishCallback);
        }


        public void PlayScreenDark(float start, float end, float duration, Action finish, bool disableOnPlayFinish) {
            ScreenDarkEnabled = true;
            Action disableOnPlayFinishCallback = null;
            if (disableOnPlayFinish)
                disableOnPlayFinishCallback = () => { ScreenDarkEnabled = false; };

            PlayBase(PostEffectType.ScreenDark, (ft) => {
                _screenDarkParameters._alpha = Mathf.Lerp(start, end, ft);
            }, duration, finish, disableOnPlayFinishCallback);
        }



        void PlayBase(PostEffectType type, Action<float> moveAction, float duration, Action callback = null, Action disableOnPlayFinishCallback = null) {
            int index = -1;
            for (int i = 0; i < _playDatas.Count; i++) {
                if (_playDatas[i]._type == type) {
                    index = i;
                    break;
                }
            }

            if (index == -1) {
                _playDatas.Add(new PlayData());
                index = _playDatas.Count - 1;
            }


            PlayData data = new PlayData {
                _type = type,
                _moveAction = moveAction,
                _duration = duration,
                _callback = callback,
                _disableOnPlayFinishCallback = disableOnPlayFinishCallback,
                _startTime = Time.time,
                _isRunning = true,
            };
            _playDatas[index] = data;
        }


        private void Update() {
            for (int i = 0; i < _playDatas.Count; i++) {
                PlayData data = _playDatas[i];
                if (data._isRunning) {
                    float step = (Time.time - data._startTime) / data._duration;
                    if (step <= 1)
                        data._moveAction(step);
                    else {
                        if (data._callback != null)
                            data._callback();

                        if (data._disableOnPlayFinishCallback != null) {
                            data._disableOnPlayFinishCallback();
                        }

                        data._isRunning = false;
                        _playDatas[i] = data;
                    }
                }
            }
        }

    }
}