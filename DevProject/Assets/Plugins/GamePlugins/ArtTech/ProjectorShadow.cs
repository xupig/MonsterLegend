﻿using UnityEngine;
using ShaderUtils;

namespace ArtTech {
    [AddComponentMenu("")]
    public class ProjectorShadow : MonoBehaviour {
        public Transform target;
        private float _distance = 10;

        public float shadowSize = 1;
        public int blurTimes = 3;
        public float blurOffset;
        public Material blurMat;
        public bool useBlur;
        Camera _camera;
        private Projector _projector;
        RenderTexture _shadowTex;
        private float _orthographicSize = 5;
        public float orthographicSize {
            set {
                _orthographicSize = value;
                if (_projector != null) _projector.orthographicSize = _orthographicSize;
                if (_camera != null) _camera.orthographicSize = _orthographicSize;
            }
            get { return _orthographicSize; }
        }

        int _shadowTexSize = -1;
        public int ShadowTexSize {
            set {
                if (_shadowTexSize != value) {
                    _shadowTexSize = value;
                    CreateShadowTex(_shadowTexSize);
                }
            }
        }
        static bool isRendering;

        int _ignoreLayers;

        void Awake() {
            _ignoreLayers = -1 ^ LayerMask.GetMask("Terrain");
            if (this.gameObject.GetComponent<Projector>() == null) {
                this.gameObject.AddComponent<Projector>();
            }
            CreateLightSpaceCam();
            //LoadFade();
        }

        void OnDestory() {
            Clear();
        }

        void Clear() {
            if (_shadowTex) {
                Destroy(_shadowTex);
                _shadowTex = null;
            }
        }

        public void IgnoreDefaultLayer(bool state) {
            _projector.ignoreLayers = state ? _ignoreLayers : _ignoreLayers ^ LayerMask.GetMask("Default");
        }

        public void CreateLightSpaceCam() {
            _projector = gameObject.GetComponent<Projector>();
            if (_projector == null)
                return;
            _projector.orthographic = true;
            _projector.ignoreLayers = _ignoreLayers;
            Material mat = new Material(ShaderRuntime.loader.Find("XGame/Shadow/ProjectorShadow"));
            //Material mat = pj.material;
            _projector.material = mat;
            _projector.nearClipPlane = 5;
            _projector.farClipPlane = 60;
            _projector.orthographicSize = orthographicSize;//5;
            GameObject go = new GameObject("ProjectorCam", typeof(Camera));
            go.transform.SetParent(this.transform, false);
            go.hideFlags = HideFlags.DontSave;// HideFlags.HideAndDontSave;
            _camera = go.GetComponent<Camera>();
            _camera.orthographic = true;
            _camera.enabled = false;
            _camera.transform.localPosition = Vector3.zero;
            _camera.transform.localRotation = Quaternion.Euler(Vector3.zero);
            _camera.gameObject.AddComponent<FlareLayer>();
            _camera.SetReplacementShader(ShaderRuntime.loader.Find("Hidden/XGame/ShadowOnly"), "RenderType");

            InitCamera(_camera, _projector);
            //UseBlur(curCam.GetComponent<BlurForShadow>()); 

            //if (mat.HasProperty("_ShadowTex")) mat.SetTexture("_ShadowTex", _shadowTex);
        }

        void CreateShadowTex(int size) {
            if (_camera == null)
                return;

            if (_shadowTex != null) {
                Destroy(_shadowTex);
                _shadowTex = null;
            }

            if (size > 0) {
                _shadowTex = new RenderTexture(size, size, 0);
                _shadowTex.hideFlags = HideFlags.DontSave;
                _camera.targetTexture = _shadowTex;

                _projector.material.SetTexture("_ShadowTex", _shadowTex);
            }
        }


        void LoadFade() {
            /*
            GameResource.ObjectPool.Instance.GetObject("Images/shadowfade.png", (obj) => {
                Texture2D text = obj as Texture2D;
                Projector pj = gameObject.GetComponent<Projector>();
                Material mat = pj.material;
                if (mat.HasProperty("_FadeTex")) mat.SetTexture("_FadeTex", text);
            });
            */
        }

        void InitCamera(Camera cam, Projector projector) {
            cam.clearFlags = CameraClearFlags.SolidColor;
            cam.backgroundColor = new Color(1, 1, 1, 0);
            cam.farClipPlane = projector.farClipPlane;
            cam.nearClipPlane = projector.nearClipPlane;
            cam.orthographic = projector.orthographic;
            cam.fieldOfView = projector.fieldOfView;
            cam.aspect = projector.aspectRatio;
            cam.orthographicSize = projector.orthographicSize;
            cam.depthTextureMode = DepthTextureMode.None;
            cam.renderingPath = RenderingPath.Forward;
            cam.cullingMask = LayerMask.GetMask("Actor") | LayerMask.GetMask("Monster") | LayerMask.GetMask("Dummy")
                | LayerMask.GetMask("Avatar");
            //Debug.Log(cam.transform.rotation);
            cam.transform.position = transform.position;
            cam.transform.rotation = transform.rotation;
        }

        void Update() {
            FollowingTarget();
            RenderObjects();
        }

        void FollowingTarget() {
            if (target == null) return;
            Vector3 focusPoint = target.transform.position;
            this.transform.position = target.transform.position - Vector3.forward * _distance;
            this.transform.RotateAround(focusPoint, new Vector3(1, 0, 0), 140);
            //this.transform.RotateAround(focusPoint, new Vector3(0, 1, 0), 30);
            this.transform.LookAt(focusPoint);
        }

        void RenderObjects() {
            if (isRendering)
                return;
            isRendering = true;
            _camera.Render();
            isRendering = false;
        }
    }
}
