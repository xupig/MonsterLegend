﻿//#define USE_SIMPLE_MESH_SHADOW

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ArtTech {
    public class ShadowManager {
        GameObject _shadowGameObject;
        ProjectorShadow _projectorShadow;
#if USE_SIMPLE_MESH_SHADOW
        List<SimpleMeshShadow> _meshShadows = new List<SimpleMeshShadow>();
#endif
        bool _inited = false;

        private static ShadowManager s_instance = null;
        public static ShadowManager GetInstance() {
            if (s_instance == null) {
                s_instance = new ShadowManager();
            }
            return s_instance;
        }

        public enum Quality {
            High = 0,
            Medium,
            Low
        }

        private static Quality _quality = Quality.High;
        public static Quality QualityLevel {
            get { return _quality; }
            set { if (_quality != value) { _quality = value; OnChangeQualityLevel(); } }
        }

        static void OnChangeQualityLevel() {
            switch (_quality) {
                case Quality.High:
                    ShadowManager.GetInstance().SetProjectorShadowActive(true);
                    ShadowManager.GetInstance().SetProjectorShadowTextureSize(2048);
                    break;

                case Quality.Medium:
                    ShadowManager.GetInstance().SetProjectorShadowActive(true);
                    ShadowManager.GetInstance().SetProjectorShadowTextureSize(1024);
                    break;

                case Quality.Low:
                    ShadowManager.GetInstance().SetProjectorShadowActive(false);
                    ShadowManager.GetInstance().SetProjectorShadowTextureSize(0);  //0表示删除ShadowTex;
                    break;
            }
        }

        void Init() {
            if (_inited) return;
            _inited = true;
            _shadowGameObject = new GameObject("ShadowSystem");
            GameObject.DontDestroyOnLoad(_shadowGameObject);
            _projectorShadow = _shadowGameObject.AddComponent<ProjectorShadow>();
            if (QualityLevel <= Quality.Medium)
                _projectorShadow.ShadowTexSize = QualityLevel == Quality.High ? 2048 : 1024;
            _shadowGameObject.SetActive(false);
        }

        public void SetOrthographicSize(float orthographicSize) {
            Init();
            _projectorShadow.orthographicSize = orthographicSize;
        }

        public void IgnoreDefaultLayer(bool state) {
            Init();
            _projectorShadow.IgnoreDefaultLayer(state);
        }

        public void SetFollowingTarget(Transform target) {
            Init();
            _projectorShadow.target = target;
        }

        public void OpenShadow() {
            Init();
            if (QualityLevel <= Quality.Medium)
                _shadowGameObject.SetActive(true);
        }

        public void HideShadow() {
            Init();
            _shadowGameObject.SetActive(false);
        }

        void SetProjectorShadowActive(bool active) {
            if (_shadowGameObject != null)
                _shadowGameObject.SetActive(active);

#if USE_SIMPLE_MESH_SHADOW
            for (int i = 0; i < _meshShadows.Count; i++) {
                if (_meshShadows[i] != null) {
                    _meshShadows[i]._renderer.enabled = !active;
                }
            }
#endif
        }

        void SetProjectorShadowTextureSize(int size) {
            _projectorShadow.ShadowTexSize = size;
        }



#if USE_SIMPLE_MESH_SHADOW
        public GameObject CreateSimpleMeshShadow() {
            GameObject meshGo = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("ArtTech/MeshShadow/SimpleMeshShadow"));

            SimpleMeshShadow comp = meshGo.GetComponent<SimpleMeshShadow>();
            AddSimpleMeshShadow(comp);
            if (QualityLevel == Quality.High)
                comp._renderer.enabled = false;

            return meshGo;
        }


        void AddSimpleMeshShadow(SimpleMeshShadow comp) {
            for (int i = 0; i < _meshShadows.Count; i++) {
                if (_meshShadows[i] == null) {
                    _meshShadows[i] = comp;
                    return;
                }
            }

            _meshShadows.Add(comp);
        }
#endif
    }
}
