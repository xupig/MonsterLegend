﻿using UnityEngine;
using System.Collections;


namespace ArtTech {
    public class Benchmark : MonoBehaviour {
        public delegate void EndDelegate();
        public static EndDelegate OnEnd;

        public Camera _camera;
        public MeshRenderer _renderer;
        public Shader _shader;

        int _pixelLightCount;
        private void Awake() {
            _pixelLightCount = QualitySettings.pixelLightCount;
            QualitySettings.pixelLightCount = 1000;

            _renderer.sharedMaterial = new Material(_shader);

            for (int j = -11; j <= 10; j++) {
                for (int i = -11; i <= 10; i++) {
                    GameObject go = new GameObject("Point light");
                    go.transform.position = new Vector3(i + 0.5f, 0.2f, j + 0.5f);
                    go.transform.SetParent(transform);
        
                    Light light = go.AddComponent<Light>();
                    light.type = LightType.Point;
                    light.range = 1;
                }
            }

            _camera.targetTexture = new RenderTexture(1920, 1080, 0);
        }

        private void OnDisable() {
            QualitySettings.pixelLightCount = _pixelLightCount;
        }

        /*
        private void OnRenderImage(RenderTexture source, RenderTexture destination) {
            Graphics.Blit(source, destination);
        }
        */

        
        IEnumerator Start() {
            int oldFrameRate = Application.targetFrameRate;
            Application.targetFrameRate = 60;
            yield return new WaitForSeconds(1);

            int startFrame = Time.frameCount;
            float startTime = Time.realtimeSinceStartup;
            yield return new WaitForSeconds(1);
            int endFrame = Time.frameCount;
            float endTime = Time.realtimeSinceStartup;

            float fpsAvg = (endFrame - startFrame) / (endTime - startTime);

            Debug.Log("[Benchmark].Start fps avg " + fpsAvg);

            GraphicsQuality.SetAutoQualityLevelByFps(fpsAvg);
            Application.targetFrameRate = oldFrameRate;

            OnEnd();
        }
    }
}
