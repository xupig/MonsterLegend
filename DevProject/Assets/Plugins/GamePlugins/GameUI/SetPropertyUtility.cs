using UnityEngine;
using UnityEngine.Events;

namespace UIExtension
{
    internal static class SetPropertyUtility
    {
        public static bool SetColor(ref Color currentValue, Color newValue)
        {
            if (currentValue.r == newValue.r && currentValue.g == newValue.g && currentValue.b == newValue.b && currentValue.a == newValue.a)
                return false;
			
            currentValue = newValue;
            return true;
        }

        public static bool SetFloat(ref float currentValue, float newValue)
        {
            if(currentValue == newValue)
                return false;

            currentValue = newValue;
            return true;
        }

        public static bool SetInt(ref int currentValue, int newValue)
        {
            if(currentValue == newValue)
                return false;

            currentValue = newValue;
            return true;
        }

        public static bool SetBool(ref bool currentValue, bool newValue)
        {
            if(currentValue == newValue)
                return false;

            currentValue = newValue;
            return true;
        }

        public static bool SetStruct<T>(ref T currentValue, T newValue) where T : struct
        {
            if (currentValue.Equals(newValue))
                return false;

            currentValue = newValue;
            return true;
        }

        public static bool SetClass<T>(ref T currentValue, T newValue) where T : class
        {
            if ((currentValue == null && newValue == null) || (currentValue != null && currentValue.Equals(newValue)))
                return false;

            currentValue = newValue;
            return true;
        }
    }
}
