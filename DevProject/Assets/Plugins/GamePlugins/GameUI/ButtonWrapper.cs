﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;
using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace UIExtension
{
    [AddComponentMenu("XGameUI/ButtonWrapper", 30)]
    public class ButtonWrapper : Button, IGameStyleComponent
    {
        public bool custom = false;
        public bool hasClickAnim = true;
        public string gameStyle = string.Empty;

        public bool IsCustom() { return custom; }
        public string GetGameStyle() { return gameStyle; }

        public string guideName = string.Empty;

        public int clickSoundId = 1;
        public static Action<int> ClickSound;

        TextMeshWrapper _text = null;
        bool _gotText = false;
        public void SetInteractable(bool state)
        {
            this.interactable = state;
            if (!_gotText && _text == null)
            {
                _gotText = true;
                _text = this.GetComponentInChildren<TextMeshWrapper>(true);
            }
            if (_text != null)
            {
                //_text.SetInteractable(state);
            }
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
            if (ClickSound != null) ClickSound(clickSoundId);
        }

        public void SetGameStyle(Object styleObject)
        {
            //Debug.LogError("SetGameStyle");
            ButtonWrapper styleButton = styleObject as ButtonWrapper;
            this.gameStyle = styleButton.gameStyle;
            CopyRectTransform(styleButton);
            CopyImage(styleButton);
            CopyTransition(styleButton);
            CopyText(styleButton);
        }

        void CopyRectTransform(ButtonWrapper styleButton)
        {
            RectTransform myRect = this.GetComponent<RectTransform>();
            RectTransform styleRect = styleButton.GetComponent<RectTransform>();
            //Debug.LogError("myRect" + myRect.sizeDelta.x);
            //Debug.LogError("styleRect" + styleRect.sizeDelta.x);
            myRect.sizeDelta = styleRect.sizeDelta;
        }

        void CopyImage(ButtonWrapper styleButton)
        {
            ImageWrapper srcImage = styleButton.GetComponent<ImageWrapper>();
            ImageWrapper tarImage = this.GetComponent<ImageWrapper>();
            tarImage.SetGameStyle(srcImage);
        }

        void CopyTransition(ButtonWrapper styleButton)
        {
            this.transition = styleButton.transition;
            this.spriteState = styleButton.spriteState;
            this.colors = styleButton.colors;
            if (transition == Transition.Animation)
            {
                AnimationTriggers triggers = new AnimationTriggers();
                triggers.disabledTrigger = styleButton.animationTriggers.disabledTrigger;
                triggers.highlightedTrigger = styleButton.animationTriggers.highlightedTrigger;
                triggers.normalTrigger = styleButton.animationTriggers.normalTrigger;
                triggers.pressedTrigger = styleButton.animationTriggers.pressedTrigger;
            }
        }

        void CopyText(ButtonWrapper styleButton)
        {
            var scrText = styleButton.GetComponentInChildren<TextMeshWrapper>(true);
            var tarText = this.GetComponentInChildren<TextMeshWrapper>(true);
            if (tarText != null && scrText != null)
            {
                tarText.gameObject.SetActive(scrText.gameObject.activeSelf);
                tarText.SetGameStyle(scrText);
            }
        }
    }
}
