﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UIExtension
{
    [AddComponentMenu("XGameUI/StateButtonWrapper", 32)]
    public class StateButtonWrapper : ButtonWrapper
    {
        public Transform upTransform = null;
        public Transform downTransform = null;
        
        public override void OnPointerEnter(PointerEventData eventData)
        {
            base.OnPointerEnter(eventData);
            if (downTransform != null && upTransform != null)
            {
                upTransform.gameObject.SetActive(false);
                downTransform.gameObject.SetActive(true);
            }
        }
        public override void OnPointerExit(PointerEventData eventData)
        {
            base.OnPointerExit(eventData);
            if (downTransform != null && upTransform != null)
            {
                upTransform.gameObject.SetActive(true);
                downTransform.gameObject.SetActive(false);
            }
        }    
    }
}
