﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Sprites;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// 五边形，雷达图
/// </summary>
namespace UIExtension
{
    public class PentagonImage : ImageWrapper
    {
        #region static members
        private const int VERTEXES_COUNT = 5;
        //0/180, 72/180, 144/180, 216/180, 288/180
        private static readonly float[] _cosAngles = new float[VERTEXES_COUNT] { Mathf.Cos(0), Mathf.Cos(0.4f * Mathf.PI), Mathf.Cos(0.8f * Mathf.PI), Mathf.Cos(1.2f * Mathf.PI), Mathf.Cos(1.6f * Mathf.PI) };
        private static readonly float[] _sinAngles = new float[VERTEXES_COUNT] { Mathf.Sin(0), Mathf.Sin(0.4f * Mathf.PI), Mathf.Sin(0.8f * Mathf.PI), Mathf.Sin(1.2f * Mathf.PI), Mathf.Sin(1.6f * Mathf.PI) };
        private static readonly float[] _defaultValues = new float[VERTEXES_COUNT] { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f };
        #endregion

        private float[] _values = _defaultValues;
        //顶点坐标数组
        private Vector2[] _positions;
        private Vector2 _centerPositon;
        //UV坐标数组
        private Vector2[] _uvs;
        private Vector2 _centerUV;

        public void SetValues(float v0, float v1, float v2, float v3, float v4)
        {
            SetValues(new float[VERTEXES_COUNT] { v0, v1, v2, v3, v4 });
        }

        public void SetValues(float[] values)
        {
            if(values.Length != VERTEXES_COUNT)
            {
                throw new Exception("The vertexes count of Pentagon must be 5");
            }
            _values = new float[VERTEXES_COUNT] { values[0], values[1], values[2], values[3], values[4] };
            SetAllDirty();
        }

        /// <summary>
        /// 获取顶点在图片中的相对坐标，用于做其他效果
        /// </summary>
        /// <returns></returns>
        public Vector2[] GetVertexPostions()
        {
            return _positions;
        }

        //计算顶点坐标和UV坐标
        private void CalculatePostionsAndUVs()
        {
			Vector4 v = Vector4.zero; //Ari GetDrawingDimensions(preserveAspect);
            var uv = (overrideSprite != null) ? DataUtility.GetOuterUV(overrideSprite) : Vector4.zero;
            float vw = v.z - v.x;
            float vh = v.w - v.y;
            float half_vw = vw * 0.5f;
            float half_vh = vh * 0.5f;
            _centerPositon = new Vector2(v.x + half_vw, v.y + half_vh);

            float uvw = uv.z - uv.x;
            float uvh = uv.w - uv.y;
            float half_uvw = uvw * 0.5f;
            float half_uvh = uvh * 0.5f;
            _centerUV = new Vector2(uv.x + half_uvw, uv.y + half_uvh);

            _positions = new Vector2[VERTEXES_COUNT];
            _uvs = new Vector2[VERTEXES_COUNT];

            for(int i = 0; i < VERTEXES_COUNT; i++)
            {
                float delta_vw = half_vw * _values[i] * _sinAngles[i];
                float delta_vh = half_vh * _values[i] * _cosAngles[i];

                float delta_uvw = half_uvw * _sinAngles[i];
                float delta_uvh = half_uvh * _cosAngles[i];

                _positions[i] = new Vector2(_centerPositon.x + delta_vw, _centerPositon.y + delta_vh);
                _uvs[i] = new Vector2(_centerUV.x + delta_uvw, _centerUV.y + delta_uvh);
            }
        }

        public override void SetAllDirty()
        {
            CalculatePostionsAndUVs();
            base.SetAllDirty();
        }

        protected override void OnFillVBO(List<UIVertex> vbo)
        {
            var vert = UIVertex.simpleVert;
            vert.color = color;
            for(int i = 0; i < VERTEXES_COUNT; i++)
            {
                vert.position = _centerPositon;
                vert.uv0 = _centerUV;
                vbo.Add(vert);

                vert.position = _positions[i];
                vert.uv0 = _uvs[i];
                vbo.Add(vert);

                vert.position = _positions[(i + 1) % VERTEXES_COUNT];
                vert.uv0 = _uvs[(i + 1) % VERTEXES_COUNT];
                vbo.Add(vert);

                vert.position = _centerPositon;
                vert.uv0 = _centerUV;
                vbo.Add(vert);
            }
        }
    }
}
	