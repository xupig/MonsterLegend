﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;
using System.Collections.Generic;

namespace UIExtension
{
    [AddComponentMenu("XGameUI/ImageWrapper", 10)]
    public class ImageWrapper : Image
    {
        public static Func<string, Object> GetSprite;
        public static Func<string, Object> GetMaterial;
        public string spriteKey;
        public string materialKey;
        /// <summary>
        /// 专用作于AtlasPrefab中记录Sprite资源模板
        /// </summary>
        public string spriteKeyTemplate;
        /// <summary>
        /// 当图片或文本的Size改变了，才需要调用OnRectTransformDimensionsChange
        /// </summary>
        private bool _isDimensionsChanged = true;
        private bool _canSetAllDirty = true;
        private bool _continuousDimensionDirty = false;

        public Material _defaultMaterial = null;
        private float _gray = 1;
        private Material _grayMaterial = null;
        private float _blur = 0;
        private Material _blurMaterial = null;

        static public bool showAllArea = false;

        protected override void Awake()
        {
            base.Awake();
        }

        protected override void OnEnable()
        {
            _canSetAllDirty = false;
            if (this.sprite == null && string.IsNullOrEmpty(spriteKey) == false)
            {
                if (GetSprite != null)
                {
                    sprite = GetSprite(spriteKey) as Sprite;
                    if (sprite != null) spriteKey = string.Empty;
                }
            }

            if (_defaultMaterial == null && string.IsNullOrEmpty(materialKey) == false)
            {
                if (GetMaterial != null)
                {
                    _defaultMaterial = GetMaterial(materialKey) as Material;
                    if (_defaultMaterial != null) materialKey = string.Empty;
                    if (showAllArea && Application.isEditor && this.raycastTarget && _defaultMaterial != null)
                    {
                        this._defaultMaterial = new Material(this._defaultMaterial);
                        this._defaultMaterial.shader = ShaderUtils.ShaderRuntime.loader.Find("UGUI/UGUI_OUTLINE");
                        this.material = _defaultMaterial;
                        this._defaultMaterial.SetFloat("_ShowArea", 0.5f);
                    }
                }
                ResetMaterialsTexture();
            }
            if (this.material == this.defaultMaterial && _defaultMaterial != null)
            {
                this.material = _defaultMaterial;
            }
            _canSetAllDirty = true;
            //优化性能：在OnEnable函数中，只有上一次m_ShouldRecalculate为true的时候，才将其设为true，重新计算遮罩值
            //Ari bool lastShouldRecalculate = m_ShouldRecalculate;
            base.OnEnable();
            /*Ari
            if(lastShouldRecalculate == false)
            {
                m_ShouldRecalculate = false;
            }*/
        }

        void ResetMaterialsTexture()
        {
            if (_defaultMaterial != null)
            {
                if (this._grayMaterial != null)
                {
                    this._grayMaterial.SetTexture("_AlphaTex", _defaultMaterial.GetTexture("_AlphaTex"));
                }
                if (this._blurMaterial != null)
                {
                    this._blurMaterial.SetTexture("_AlphaTex", _defaultMaterial.GetTexture("_AlphaTex"));
                }
            }
        }

        public override void SetAllDirty()
        {
            if (_canSetAllDirty == true)
            {
                base.SetAllDirty();
            }
        }

        public override void SetLayoutDirty()
        {

        }

        public void ForceRecalculateMaskable()
        {
            //Ari m_ShouldRecalculate = true;
        }

        protected override void OnBeforeTransformParentChanged()
        {
            GraphicRegistry.UnregisterGraphicForCanvas(canvas, this);
        }

        protected override void OnRectTransformDimensionsChange()
        {
            if (_isDimensionsChanged == true || _continuousDimensionDirty == true)
            {
                base.OnRectTransformDimensionsChange();
                _isDimensionsChanged = false;
            }
        }

        public void SetDimensionsDirty()
        {
            _isDimensionsChanged = true;
        }

        public void SetContinuousDimensionDirty(bool value)
        {
            _continuousDimensionDirty = value;
        }

        /// <summary>
        /// 设置灰度，值为0时图片灰化，值为1时图片正常显示
        /// </summary>
        /// <param name="value"></param>
        public void SetGray(float value)
        {
            if (this._gray == value) return;
            this._gray = value;
            if (_grayMaterial == null)
            {
                Shader shader = ShaderUtils.ShaderRuntime.loader.Find("UGUI/UGUI_IMAGEGREY");
                if (shader != null)
                {
                    this._grayMaterial = new Material(shader);
                    ResetMaterialsTexture();
                }
            }
            if (_grayMaterial != null)
            {
                if (value == 1)
                {
                    this.material = _defaultMaterial != null ? _defaultMaterial : this.defaultMaterial;
                }
                else
                {
                    this.material = this._grayMaterial;
                    this._grayMaterial.SetFloat("_GreyAlpha", value);
                }
            }
        }

        /// <summary>
        /// 设置模糊，值为0时图片灰化，值为1时图片正常显示
        /// </summary>
        /// <param name="value"></param>
        public void SetBlur(float value, Color color)
        {
            if (this._blur == value) return;
            this._blur = value;
            if (_blurMaterial == null)
            {
                Shader shader = ShaderUtils.ShaderRuntime.loader.Find("UGUI/UGUI_BLUR");
                if (shader != null)
                {
                    this._blurMaterial = new Material(shader);
                    ResetMaterialsTexture();
                }
            }
            if (_blurMaterial != null)
            {
                if (value == 0)
                {
                    this.material = _defaultMaterial != null ? _defaultMaterial : this.defaultMaterial;
                }
                else
                {
                    this.material = this._blurMaterial;
                    this._blurMaterial.SetFloat("_Distance", value);
                    this._blurMaterial.SetColor("_Color", color);
                }
            }
        }

        public void SetMaterial(Material material)
        {
            if (material != null)
            {
                if (_defaultMaterial != null)
                {
                    material.SetTexture("_AlphaTex", _defaultMaterial.GetTexture("_AlphaTex"));
                }
                this.material = material;
            }
            else
            {
                this.material = _defaultMaterial != null ? _defaultMaterial : this.defaultMaterial;
            }
        }

        public void SetGameStyle(Object styleObject)
        {
            ImageWrapper styleImage = styleObject as ImageWrapper;
            this.sprite = styleImage.sprite;
            this.color = styleImage.color;
            this.raycastTarget = styleImage.raycastTarget;
            this.type = styleImage.type;
            this.preserveAspect = styleImage.preserveAspect;
            base.SetAllDirty();
        }

    }
}
