﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Object = UnityEngine.Object;

namespace UIExtension
{
    [AddComponentMenu("XGameUI/TextMeshWrapper", 11)]
    public class TextMeshWrapper : TextMeshProUGUI, IGameStyleComponent
    {
        public static Func<string, Object> GetFont;
        public static Func<int, string> GetLanguage;

        static Dictionary<string, TMPAssets> _fontAssetMap = new Dictionary<string, TMPAssets>();
        static TMPAssets GetFontAssets(string fontKey)
        {
            if (_fontAssetMap.ContainsKey(fontKey)) return _fontAssetMap[fontKey];
            TMPAssets result = null;
            while (GetFont != null)
            {
                var fontGO = GetFont(fontKey) as GameObject;
                if (fontGO == null) break;
                result = fontGO.GetComponent<TMPAssets>();
                break;
            }
            _fontAssetMap[fontKey] = result;
            return result;
        }

        //语言配置ID
        [SerializeField]
        public int langId = -1;
        [SerializeField]
        public string fontKey;
        [SerializeField]
        public string materialKey;

        [SerializeField]
        public bool custom = false;

        [SerializeField]
        public string gameStyle = string.Empty;

        public bool IsCustom() { return custom; }
        public string GetGameStyle() { return gameStyle; }
        public void SetGameStyle(Object styleObject)
        {
            TextMeshWrapper styleText = styleObject as TextMeshWrapper;
            this.custom = false;
            this.gameStyle = styleText.gameStyle;
            this.fontStyle = styleText.fontStyle;
            this.fontSize = styleText.fontSize;
            this.lineSpacing = styleText.lineSpacing;
            this.alignment = styleText.alignment;
            this.color = styleText.color;
            this.raycastTarget = styleText.raycastTarget;
        }

        protected override void Awake()
        {
            base.Awake();
            if (this.langId != -1)
            {
                if (GetLanguage != null)
                {
                    this.text = GetLanguage(this.langId);
                }
            }
        }

        protected override void OnEnable()
        {
            if ((font == null || font.name.IndexOf("Temp") >= 0) && string.IsNullOrEmpty(fontKey) == false)
            {
                TMPAssets assets = GetFontAssets(fontKey);
                if (assets != null)
                {
                    font = assets.fontAsset;
                    var material = assets.GetMaterial(materialKey);
                    if (material) this.SetSharedMaterial(material);
                    if (assets.spriteAsset)
                    {
                        spriteAsset = assets.spriteAsset;
                    }
                }
            }
            base.OnEnable();
        }
    }
}
