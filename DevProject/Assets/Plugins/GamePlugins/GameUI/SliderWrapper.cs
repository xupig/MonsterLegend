﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;
using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace UIExtension
{
    public class SliderWrapper : Slider
    {
        ImageWrapper[] _fillImage;
        protected override void Awake()
        {
            base.Awake();
            _fillImage = this.GetComponentsInChildren<ImageWrapper>();
        }

        void RefreshDimensions()
        {
            for (int i = 0; i < _fillImage.Length; i++)
            {
                _fillImage[i].SetAllDirty();
            }
        }

        protected override void Set(float input, bool sendCallback)
        {
            base.Set(input, sendCallback);
            RefreshDimensions();
        }
    }
}
