﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;
using System.Collections.Generic;

namespace UIExtension
{
    public interface IGameStyleComponent
    {
        bool IsCustom();
        string GetGameStyle();
        void SetGameStyle(Object styleObject);
    }

}
