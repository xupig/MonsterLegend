﻿using UnityEngine;
using UnityEngine.Sprites;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

namespace UIExtension
{
    /// <summary>
    /// 通过调整图片Texture的UV坐标值，实现进度条图片的效果
    /// </summary>
    public class UVScaleImage : ImageWrapper
    {
        private Rect _originalRect;
        private UVScaleDirection _direction = UVScaleDirection.Backward;

        public UVScaleDirection Direction
        {
            get
            {
                return _direction;
            }
            set
            {
                _direction = value;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            _originalRect = rectTransform.rect;
        }

		/*Ari
        protected override void OnFillVBO(List<UIVertex> vbo)
        {
            if(overrideSprite == null)
            {
                //Ari base.OnFillVBO(vbo);
                return;
            }
            if(type != Type.Simple)
            {
                if(Application.isEditor == true)
                {
                    Debug.LogWarning("UVScaleImage only support the [Image.Type.Simple] option");
					//Ari  base.OnFillVBO(vbo);
                    return;
                }
                else
                {
                    type = Type.Simple;
                }
            }
            GenerateSprite(vbo, preserveAspect);
            ApplyGreyAndAlpha(vbo);
        }*/

        private Vector4 CalculateUV()
        {
            Vector4 uv = (overrideSprite != null) ? DataUtility.GetOuterUV(overrideSprite) : Vector4.zero;
            Vector2 scale = GetScale();
            float fixedUVX = (1.0f - scale.x) * (uv.z - uv.x);
            float fixedUVY = (1.0f - scale.y) * (uv.w - uv.y);
            if(_direction == UVScaleDirection.Backward)
            {
                uv = new Vector4(uv.x + fixedUVX, uv.y + fixedUVY, uv.z, uv.w);
            }
            else if(_direction == UVScaleDirection.Forward)
            {
                uv = new Vector4(uv.x, uv.y, uv.z - fixedUVX, uv.w - fixedUVY);
            }
            return uv;
        }

        private void GenerateSprite(List<UIVertex> vbo, bool preserveAspect)
        {
            var vert = UIVertex.simpleVert;
            vert.color = color;

			Vector4 v = Vector4.zero; //Ari GetDrawingDimensions(preserveAspect);
            Vector4 uv = CalculateUV();
            
            vert.position = new Vector3(v.x, v.y);
            vert.uv0 = new Vector2(uv.x, uv.y);
            vbo.Add(vert);

            vert.position = new Vector3(v.x, v.w);
            vert.uv0 = new Vector2(uv.x, uv.w);
            vbo.Add(vert);

            vert.position = new Vector3(v.z, v.w);
            vert.uv0 = new Vector2(uv.z, uv.w);
            vbo.Add(vert);

            vert.position = new Vector3(v.z, v.y);
            vert.uv0 = new Vector2(uv.z, uv.y);
            vbo.Add(vert);
        }

        private Vector2 GetScale()
        {
            return new Vector2(rectTransform.sizeDelta.x / _originalRect.width, rectTransform.sizeDelta.y / _originalRect.height);
        }

        public enum UVScaleDirection
        {
            Backward,
            Forward
        }
    }
}
