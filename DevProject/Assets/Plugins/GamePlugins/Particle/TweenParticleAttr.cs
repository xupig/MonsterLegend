﻿using System;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(ParticleSystem))]
public class TweenParticleAttr : MonoBehaviour
{
    private ParticleSystem particleSys;
    private ParticleSystemRenderer particleSysRenderer;
    //private Material targetMat;
    private bool needUpdate;
    private bool curNeedUpdate;
    private float startDelay;
    private float duration;

    private float fadeInBeginTime;
    private float fadeInEndTime;
    private float fadeOutBeginTime;
    private float fadeOutEndTime;

    //private float startTime;
    //private float endTime;
    private float curDeltaTime;

    private bool hasSetFadeInEnd;
    private bool hasSetFadeOutEnd;

    private bool isLoop;
    private bool hasFinishStartDelay;

    public float FadeInBegin = 0;
    public float FadeInEnd = 0.3f;
    public float FadeOutBegin = 0.7f;
    public float FadeOutEnd = 1;

    public float FadeInBeginValue = 0;
    public float FadeInEndValue = 1;
    public float FadeOutBeginValue = 1;
    public float FadeOutEndValue = 0;

    public string TargetProp = "_dissolve";
    MaterialPropertyBlock prop = null;

    private void Start()
    {
#if UNITY_EDITOR
        lastUpdateTime = Time.realtimeSinceStartup;
#endif
        //Debug.Log();
        Init();
    }

    public void Init()
    {
        if (prop == null)
            prop = new MaterialPropertyBlock();
        if (!particleSysRenderer)
            particleSysRenderer = GetComponent<ParticleSystemRenderer>();
        if (particleSysRenderer.sharedMaterial.HasProperty(TargetProp))
        {
            if (!particleSys)
                particleSys = GetComponent<ParticleSystem>();
            //if (!targetMat)
            //{
            //    Shader.PropertyToID("_Color");
            //    if (Application.isEditor)
            //    {
            //        targetMat = particleSysRenderer.sharedMaterial;
            //    }
            //    else
            //    {
            //        targetMat = particleSysRenderer.material;
            //    }
            //}
            if (FadeInBegin == FadeInEnd && FadeInEnd == FadeOutBegin && FadeOutBegin == FadeOutEnd)
            {
                needUpdate = false;
            }
            else
            {
                needUpdate = true;
                isLoop = particleSys.loop;
                startDelay = particleSys.startDelay;
                duration = particleSys.startLifetime;
                fadeInBeginTime = FadeInBegin * duration;
                fadeInEndTime = FadeInEnd * duration;
                fadeOutBeginTime = FadeOutBegin * duration;
                fadeOutEndTime = FadeOutEnd * duration;
            }
            //startTime = Time.realtimeSinceStartup;
            //endTime = Time.realtimeSinceStartup + duration;
            ResetPara();
        }
    }

#if UNITY_EDITOR
    float lastUpdateTime;
#endif
    private void GetCurDeltaTime()
    {
#if UNITY_EDITOR
        curDeltaTime += Time.realtimeSinceStartup - lastUpdateTime;
        lastUpdateTime = Time.realtimeSinceStartup;
#else
        curDeltaTime += Time.deltaTime;
#endif
    }

    private void Update()
    {
        if (!curNeedUpdate)
            return;
        //Debug.Log("Update" + curDeltaTime);
        GetCurDeltaTime();

        if (!hasFinishStartDelay)
        {
            if (curDeltaTime < startDelay)
            {
                return;
            }
            else
            {
                hasFinishStartDelay = true;
                curDeltaTime = 0;
            }
        }

        if (curDeltaTime > fadeOutEndTime)
        {
            if (!hasSetFadeOutEnd)
            {
                //if (targetMat)
                particleSysRenderer.GetPropertyBlock(prop);
                prop.SetFloat(TargetProp, FadeOutEndValue);
                //Debug.Log("SetFloat FadeOutEndValue: " + FadeOutEndValue);
                particleSysRenderer.SetPropertyBlock(prop);
                //targetMat.SetFloat(TargetProp, FadeOutEndValue);
                hasSetFadeOutEnd = true;
            }
        }
        else if (curDeltaTime > fadeOutBeginTime)
        {
            var timePercentage = (curDeltaTime - fadeOutBeginTime) / (fadeOutEndTime - fadeOutBeginTime);
            //if (targetMat)
            particleSysRenderer.GetPropertyBlock(prop);
            prop.SetFloat(TargetProp, (FadeOutEndValue - FadeOutBeginValue) * timePercentage + FadeOutBeginValue);
            //Debug.Log("SetFloat  (FadeOutEndValue - FadeOutBeginValue) * timePercentage + FadeOutBeginValue: " + (FadeOutEndValue - FadeOutBeginValue) * timePercentage + FadeOutBeginValue);
            particleSysRenderer.SetPropertyBlock(prop);
            //targetMat.SetFloat(TargetProp, (FadeOutEndValue - FadeOutBeginValue) * timePercentage + FadeOutBeginValue);
        }
        else if (curDeltaTime > fadeInEndTime)
        {
            var timePercentage = (curDeltaTime - fadeInEndTime) / (fadeOutBeginTime - fadeInEndTime);
            //if (targetMat)
            particleSysRenderer.GetPropertyBlock(prop);
            prop.SetFloat(TargetProp, (FadeOutBeginValue - FadeInEndValue) * timePercentage + FadeInEndValue);
            //Debug.Log("SetFloat  (FadeOutBeginValue - FadeInEndValue) * timePercentage + FadeInEndValue: " + (FadeOutBeginValue - FadeInEndValue) * timePercentage + FadeInEndValue);
            particleSysRenderer.SetPropertyBlock(prop);
            //if (!hasSetFadeInEnd)
            //{
            //    //if (targetMat)
            //    particleSysRenderer.GetPropertyBlock(prop);
            //    prop.SetFloat(TargetProp, FadeInEndValue);
            //    particleSysRenderer.SetPropertyBlock(prop);
            //    //targetMat.SetFloat(TargetProp, FadeInEndValue);
            //    hasSetFadeInEnd = true;
            //}
        }
        else if (curDeltaTime > fadeInBeginTime)
        {
            var timePercentage = (curDeltaTime - fadeInBeginTime) / (fadeInEndTime - fadeInBeginTime);
            //if (targetMat)
            particleSysRenderer.GetPropertyBlock(prop);
            prop.SetFloat(TargetProp, (FadeInEndValue - FadeInBeginValue) * timePercentage + FadeInBeginValue);
            //Debug.Log("SetFloat (FadeInEndValue - FadeInBeginValue) * timePercentage + FadeInBeginValue: " + (FadeInEndValue - FadeInBeginValue) * timePercentage + FadeInBeginValue);
            particleSysRenderer.SetPropertyBlock(prop);
            //targetMat.SetFloat(TargetProp, (FadeInEndValue - FadeInBeginValue) * timePercentage + FadeInBeginValue);
        }

        if (curDeltaTime > duration)
        {
            if (isLoop)
            {
                ResetPara(false);
            }
            else
            {
                curNeedUpdate = false;
            }
        }
    }

    private void OnEnable()
    {
        ResetPara();
    }

    private void ResetPara(bool forceResetProp = true)
    {
        hasSetFadeInEnd = false;
        hasSetFadeOutEnd = false;
        curNeedUpdate = needUpdate;
        curDeltaTime = 0;
#if UNITY_EDITOR
        lastUpdateTime = Time.realtimeSinceStartup;
#endif
        if (particleSysRenderer && (forceResetProp || !isLoop))
        {
            if (prop == null)
                prop = new MaterialPropertyBlock();
            particleSysRenderer.GetPropertyBlock(prop);
            prop.SetFloat(TargetProp, FadeInBeginValue);
            //Debug.Log("ResetPara FadeInBeginValue: " + FadeInBeginValue);
            particleSysRenderer.SetPropertyBlock(prop);
        }
        //if (targetMat)
        //    targetMat.SetFloat(TargetProp, FadeInBeginValue);
    }
}