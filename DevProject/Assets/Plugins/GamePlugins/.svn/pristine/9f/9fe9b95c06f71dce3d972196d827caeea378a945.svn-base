﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;
using System.Collections.Generic;

namespace UIExtension
{
    [AddComponentMenu("XGameUI/ToggleWrapper", 11)]
    public class ToggleWrapper : Toggle, IGameStyleComponent
    {
        public const string CHILD_CHECKMARK = "Checkmark";
        public const string CHILD_BACK = "Background";

        private string _currentState;
        private GameObject _checkmarkGo;
        private GameObject _backGo;

        public int clickSoundId = 1;
        public static Action<int> ClickSound;

        protected override void Awake()
        {
            base.Awake();
            CreateChildren();
            Initialize();
            AddEventListener();
        }

        private void CreateChildren()
        {
            _checkmarkGo = WrapperHelper.GetChild(this.gameObject, CHILD_CHECKMARK);
            _backGo = WrapperHelper.GetChild(this.gameObject, CHILD_BACK);
        }

        private void Initialize()
        {
            this.transition = Transition.None;
            this.toggleTransition = ToggleTransition.None;
            ToggleCheckmark();
        }

        private void AddEventListener()
        {
            base.onValueChanged.AddListener(OnValueChanged);
        }

        private void OnValueChanged(bool isOn)
        {
            ToggleCheckmark();
            if (ClickSound != null) ClickSound(clickSoundId);
        }

        public void ToggleCheckmark()
        {
            if (_checkmarkGo != null)
            {
                _checkmarkGo.SetActive(this.isOn);
            }
            if (_backGo != null)
            {
                _backGo.SetActive(!this.isOn);
            }
        }

        public bool Visible
        {
            get
            {
                return gameObject.activeSelf;
            }
            set
            {
                gameObject.SetActive(value);
            }
        }

        Text _text = null;
        bool _gotText = false;
        public void SetInteractable(bool state)
        {
            this.interactable = state;
            if (!_gotText && _text == null)
            {
                _gotText = true;
                _text = this.GetComponentInChildren<Text>(true);
            }
            if (_text != null)
            {
                //_text.SetInteractable(state);
            }
        }

/*************** style 设置相关 ****************************/

        public bool custom = false;

        public string gameStyle = string.Empty;

        public bool IsCustom() { return custom; }
        public string GetGameStyle() { return gameStyle; }
        public void SetGameStyle(Object styleObject)
        {
            ToggleWrapper styleToggle = styleObject as ToggleWrapper;
            this.gameStyle = styleToggle.gameStyle;
            //CopyRectTransform(styleToggle);
            CopyTransition(styleToggle);

            CopyToggleChild(styleToggle);
        }


        void CopyRectTransform(ToggleWrapper styleToggle)
        {
            RectTransform myRect = this.GetComponent<RectTransform>();
            RectTransform styleRect = styleToggle.GetComponent<RectTransform>();
            myRect.sizeDelta = styleRect.sizeDelta;
        }

        void CopyTransition(ToggleWrapper styleToggle)
        {
            this.transition = styleToggle.transition;
            this.spriteState = styleToggle.spriteState;
            this.colors = styleToggle.colors;
            if (transition == Transition.Animation)
            {
                AnimationTriggers triggers = new AnimationTriggers();
                triggers.disabledTrigger = styleToggle.animationTriggers.disabledTrigger;
                triggers.highlightedTrigger = styleToggle.animationTriggers.highlightedTrigger;
                triggers.normalTrigger = styleToggle.animationTriggers.normalTrigger;
                triggers.pressedTrigger = styleToggle.animationTriggers.pressedTrigger;
            }
        }

        void CopyText(ToggleWrapper styleToggle)
        {/*
            var scrText = styleToggle.GetComponentInChildren<TextMeshWrapper>(true);
            var tarText = this.GetComponentInChildren<TextMeshWrapper>(true);
            tarText.gameObject.SetActive(scrText.gameObject.activeSelf);
            tarText.SetGameStyle(scrText);*/
        }

        void CopyToggleChild(ToggleWrapper styleToggle)
        {
            CopyText(styleToggle);
            CopyBackground(styleToggle);
            CopyCheckmark(styleToggle);
        }

        void CopyBackground(ToggleWrapper styleToggle)
        {
            CopyRectTransform(styleToggle, "Background");
            CopyImage(styleToggle, "Background");
        }

        void CopyCheckmark(ToggleWrapper styleToggle)
        {
            CopyRectTransform(styleToggle, "Checkmark");
            CopyImage(styleToggle, "Checkmark");
        }

        void CopyRectTransform(ToggleWrapper styleToggle, string name)
        {
            GameObject srcgo = styleToggle.transform.FindChild(name).gameObject;
            GameObject targo = this.transform.FindChild(name).gameObject;
            RectTransform myRect = targo.GetComponent<RectTransform>();
            RectTransform styleRect = srcgo.GetComponent<RectTransform>();
            myRect.sizeDelta = styleRect.sizeDelta;
        }

        void CopyImage(ToggleWrapper styleToggle,string name)
        {
            GameObject srcgo = styleToggle.transform.FindChild(name).gameObject;
            GameObject targo = this.transform.FindChild(name).gameObject;
            ImageWrapper srcImage = srcgo.GetComponent<ImageWrapper>();
            ImageWrapper tarImage = targo.GetComponent<ImageWrapper>();
            tarImage.SetGameStyle(srcImage);
        }
    }
}
