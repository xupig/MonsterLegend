﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace GameResource
{
    public class FakeFBX : MonoBehaviour
    {
        [SerializeField]
        public string[] keys;

        [SerializeField]
        public Mesh[] meshes;

        Dictionary<string, Mesh> _keyMeshMap;

        public Mesh GetMesh(string key)
        {
            if (_keyMeshMap == null)
            {
                _keyMeshMap = new Dictionary<string, Mesh>();
                for (int i = 0; i < keys.Length; i++)
                {
                    _keyMeshMap[keys[i]] = meshes[i];
                }
            }
            Mesh mesh = null;
            _keyMeshMap.TryGetValue(key, out mesh);
            return mesh;
        }
    }
}

