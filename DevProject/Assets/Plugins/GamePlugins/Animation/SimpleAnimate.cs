﻿//-----------------------------------------------------------------
// Light probe fake point light
// Used for Game Runtime
// wangjian
//-----------------------------------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Game.Shading.Runtime
{
    [ExecuteInEditMode]
    public class SimpleAnimate : MonoBehaviour
    {
        public enum SimpleAnimateType
        {
            TRANSLATION,
            SCALE,
            ROTATION,
        };
        public SimpleAnimateType animType = SimpleAnimateType.ROTATION;

        public int loopCount = 0;

        public Vector3 speed;

        public AnimationCurve animation;
        
        private Transform tm = null;


        // Use this for initialization
        void Start()
        {
            if (tm == null)
                tm = this.gameObject.transform;

        }

        void Awake()
        {
            
        }

        void OnBecameVisible()
        {
            enabled = true;
        }
        void OnBecameInvisible()
        {
            enabled = false;
        }

        // Update is called once per frame
        void Update()
        {
            if( animType == SimpleAnimateType.TRANSLATION )
            {
                //tm.position += 
            }
            else if( animType == SimpleAnimateType.SCALE )
            {
               //tm.localScale = animation.Evaluate(Time.time);
            }
            else if( animType == SimpleAnimateType.ROTATION )
            {
                tm.Rotate(speed * Time.deltaTime, Space.Self);
            }
        }
        
    }
}
