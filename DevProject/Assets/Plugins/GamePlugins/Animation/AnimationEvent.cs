﻿using UnityEngine;
using System.Collections;

public class AnimationEvent : MonoBehaviour
{
    public delegate void EventDelegate(string action);
    public EventDelegate onEvent;
    public delegate void RiderEventDelegate(int cmd);
    public RiderEventDelegate onRiderEvent;
    public delegate void FootEventDelegate(int cmd);
    public FootEventDelegate onFootEvent;

    private void OnEvent(string action)
    {
        if (onEvent != null)
        {
            onEvent(action);
        }
    }

    private void OnRider(int cmd)
    {
        onRiderEvent(cmd);
    }

    private void OnFoot(int cmd)
    {
        onFootEvent(cmd);
    }
}
