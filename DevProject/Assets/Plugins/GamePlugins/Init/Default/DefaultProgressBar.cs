﻿using System.IO;
using GameLoader;
using GameLoader.Utils;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace Default
{
    /// <summary>
    /// 默认加载进度条
    /// </summary>
    class DefaultProgressBar
    {
        private Text labTip;              //当前加载文件提示
        private Text labFloatTip;         //动态提示
        private Text labProgress;         //进度标签 
        private Image imageBg;            //背景图组件
        private Texture2D newestBg;       //最新背景图
        private RectTransform maskRect;   //进度蒙版
        private RectTransform fxRext;     //进度条粒子特效
        private RectTransform anchor;     //进度条的描点(做自适应)
        private Vector2 _canvasSize;

        private int index = -1;
        private int beginTime = 0;
        private bool isUpdate = true;
        private string[] floatTipList;
        private const string newestBgName = "/progressbg.png.u";
        public const int PANEL_WIDTH = 1280;
        public const int PANEL_HEIGHT = 720;

        public DefaultProgressBar()
        {
            LoadDefaultSkin();
            LoadProgressBarBg();
        }

        public GameObject prefab { get; private set; }
        public float maskMaxWidth { get; private set; }
        public float fxInitX { get; private set; }

        //加载默认皮肤[资源在DevProject|RuntimeProject]工程的Resources/Dialog/目录下
        private void LoadDefaultSkin()
        {
            //加载MessageBox.prefab资源
            UnityEngine.Object res = Resources.Load("Dialog/ProgressBar", typeof(GameObject));
            prefab = (GameObject)GameObject.Instantiate(res);
            GameObject.DontDestroyOnLoad(prefab);
            res = null;
            SetSkin();
        }

        private void SetSkin()
        {
            Canvas canvas = prefab.GetComponentInChildren<Canvas>();
            _canvasSize = canvas.GetComponent<RectTransform>().sizeDelta;

            anchor = prefab.transform.Find("Canvas/Pivot").GetComponent<RectTransform>();
            labTip = anchor.Find("labTip").GetComponent<Text>();
            labProgress = anchor.Find("Label").GetComponent<Text>();
            labFloatTip = anchor.Find("labFloatTip").GetComponent<Text>();

            imageBg = canvas.transform.Find("Image_bg").GetComponent<Image>();
            maskRect = anchor.Find("LoadBar_mask").GetComponent<RectTransform>();
            fxRext = anchor.Find("LoadBar_mask/LoadBar_fx").GetComponent<RectTransform>();
            maskMaxWidth = maskRect.sizeDelta.x;
            fxInitX = fxRext.localPosition.x;
            AdjustPosition(anchor);  //自适应调整
            InitLabProperty(labFloatTip, 20);
            InitLabProperty(labTip, 25);
            InitLabProgress();
            UpdateProgress(0);
        }

        ///加载背景图
        private void LoadProgressBarBg()
        {
#if !UNITY_WEBPLAYER
            string url = string.Concat(Application.persistentDataPath, "/RuntimeResource/ProgressBar", newestBgName);
            if (File.Exists(url))
            {//加载最新背景图
                DriverLogger.Info(string.Format("[LoadNewestBg] Load newest bg url:{0} [Start]", url));
                HttpWrappr.instance.LoadWwwImage(url, OnLoadNewestBg);
            }
            else
#endif
            {//加载默认背景图
                var path = Application.streamingAssetsPath + newestBgName;
                DriverLogger.Info(string.Format("[LoadNewestBg] Load default bg:{0}", path));
                HttpWrappr.instance.LoadWwwImage(path, OnLoadNewestBg);
            }

        }

        //下载最新背景图完成
        private void OnLoadNewestBg(string url, Texture2D bg)
        {
            DriverLogger.Info(string.Format("[LoadNewestBg] url:{0} [OK] bg Image:{1},imageBg:{2}", url, bg, imageBg));
            if (bg != null)
            {
                newestBg = bg;
                if (imageBg != null && newestBg != null) imageBg.sprite = Sprite.Create(newestBg, new Rect(0, 0, newestBg.width, newestBg.height), new Vector2(0, 0));
            }
        }

        /// <summary>
        /// 更新皮肤
        /// </summary>
        /// <param name="skin">皮肤资源</param>
        public void UpdateSkin(GameObject skin)
        {
            if (skin == null)
            {
                DriverLogger.Error("[DefaultProgressBar]皮肤为空，无法更换！");
                return;
            }
            //销毁前一个皮肤资源
            Dispose();
            //设置新皮肤资源
            this.prefab = skin;
            SetSkin();
        }

        /// <summary>
        /// 更改进度
        /// </summary>
        /// <param name="progress">进度值[0-1]之间</param>
        public void UpdateProgress(float progress)
        {
            if (progress > 1) progress = 1;
            if (progress < 0) progress = 0;
            labProgress.text = string.Format("{0:#}%", progress * 100);

            //更改蒙版宽度
            Vector2 v2 = maskRect.sizeDelta;
            v2.x = progress * maskMaxWidth;
            maskRect.sizeDelta = v2;
            //更改特效位置
            Vector2 v3 = fxRext.localPosition;
            v3.x = fxInitX + v2.x;
            fxRext.localPosition = v3;
        }

        /// <summary>
        /// 更新加载说明提示
        /// </summary>
        /// <param name="tip">加载说明提示</param>
        public void UpdateTip(string tip)
        {
            labTip.text = string.Format("{0}", string.IsNullOrEmpty(tip) ? "" : tip);
        }

        /// <summary>
        /// 显示进度条
        /// </summary>
        public void Show()
        {
            UpdateProgress(0);
            labTip.text = string.Empty;
            if (!prefab.activeSelf) prefab.SetActive(true);
        }

        /// <summary>
        /// 关闭进度条
        /// </summary>
        public void Close()
        {
            UpdateProgress(0);
            prefab.SetActive(false);
        }

        /// <summary>
        /// 销毁
        /// </summary>
        public void Dispose()
        {
            isUpdate = false;
            labTip = null;
            fxRext = null;
            maskRect = null;
            anchor = null;
            labProgress = null;
        }

        //进度条自适应调整
        public void AdjustPosition(RectTransform transform)
        {
            if (transform == null) return;
            transform.anchoredPosition = new Vector2(_canvasSize.x / 2 - transform.sizeDelta.x / 2, 116 - _canvasSize.y);
        }

        public void UpdateFloatTip(string[] list)
        {
            this.index = -1;
            this.floatTipList = list;
            this.isUpdate = true;
        }

        public void OnUpdate()
        {
            if (!isUpdate) return;
            if (floatTipList == null || floatTipList.Length < 1) return;
            if (prefab == null || prefab.activeSelf == false) return;
            if (Environment.TickCount - beginTime >= 2000)
            {//每隔2秒显示一次动态公告
                beginTime = Environment.TickCount;
                if (++index >= floatTipList.Length) index = 0;
                if (index < 0) index = 0;
                labFloatTip.text = floatTipList[index];
            }
        }

        private void InitLabProperty(Text lab, int fontSize)
        {
            if (lab == null) return;
            lab.fontSize = fontSize;
            lab.color = new Color(255, 255, 255, 255) / 255f;
            Outline outline = lab.gameObject.GetComponent<Outline>();
            if (outline == null)
            {
                outline = lab.gameObject.AddComponent<Outline>();
                outline.effectColor = new Color(0, 0, 0, 204) / 255f;
                outline.effectDistance = new Vector2(1, -1);
            }
        }

        private void InitLabProgress()
        {
            if (labProgress != null)
            {
                Outline outline = labProgress.gameObject.GetComponent<Outline>();
                if (outline == null)
                {
                    outline = labProgress.gameObject.AddComponent<Outline>();
                    outline.effectColor = new Color(0, 0, 0, 204) / 255f;
                    outline.effectDistance = new Vector2(1, -1);
                }
            }
        }
    }
}
