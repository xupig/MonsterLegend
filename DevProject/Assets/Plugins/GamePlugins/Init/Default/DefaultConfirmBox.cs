﻿using GameLoader;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Default
{
    /// <summary>
    /// 应用启动到进入游戏--使用的确认提示框
    /// 1、确认框
    /// 2、强制框
    /// </summary>
    class DefaultConfirmBox
    {
        private Text txtTip;
        private Text txtCancel;
        private Text txtConfirm;
        private Button btnCancel;
        private Button btnConfirm;

        private GameObject goCancel;
        private GameObject goConfirm;
        private Action<bool> callback;

        private const string DEFAULT_OK = "OK";
        private const string DEFAULT_CANCEL = "Cancel";

        public DefaultConfirmBox()
        {
            LoadDefaultSkin();
        }

        /// <summary>
        /// 资源预设
        /// </summary>
        public GameObject prefab { get; private set; }

        //加载默认皮肤[资源在DevProject|RuntimeProject]工程的Resources/Dialog/目录下
        private void LoadDefaultSkin() 
        {
            //加载MessageBox.prefab资源
            UnityEngine.Object res = Resources.Load("Dialog/ConfirmBox", typeof(GameObject));
            prefab = (GameObject)GameObject.Instantiate(res);
            GameObject.DontDestroyOnLoad(prefab);
            res = null;
            SetSkin();
        }

        //提取控件
        private void SetSkin()
        {
            txtTip = prefab.transform.Find("Canvas/labTip").GetComponent<Text>();
            goConfirm = prefab.transform.Find("Canvas/btnConfirm").gameObject;
            txtConfirm = goConfirm.transform.Find("Text").GetComponent<Text>();
            btnConfirm = goConfirm.GetComponent<Button>();

            goCancel = prefab.transform.Find("Canvas/btnCancel").gameObject;
            txtCancel = goCancel.transform.Find("Text").GetComponent<Text>();
            btnCancel = goCancel.GetComponent<Button>();
            prefab.SetActive(false);
            AddEvents();
        }

        /// <summary>
        /// 更换皮肤
        /// </summary>
        /// <param name="skin">皮肤资源</param>
        public void UpdateSkin(GameObject skin)
        {
            if (skin == null) 
            {
                DriverLogger.Error("[DefaultConfirmBox]皮肤为空，无法更换！");
                return;
            }
            //销毁前一个皮肤资源
            Dispose();
            //设置新皮肤资源
            this.prefab = skin;
            SetSkin();
        }

        /// <summary>
        /// 显示对话框
        /// </summary>
        /// <param name="okText">提示</param>
        /// <param name="callback">点击按钮后的回调</param>
        /// <param name="okText">确认按钮文本</param>
        /// <param name="cancelText">取消按钮文本</param>
        /// <param name="isCancel">是否显示取消按钮[true:显示,false:隐藏]</param>
        public void Show(string msg, Action<bool> callback = null, string okText = null, string cancelText = null, bool isCancel = true)
        {
            this.callback = callback;
            txtConfirm.text = string.IsNullOrEmpty(okText) ? DEFAULT_OK : okText;
            txtCancel.text = string.IsNullOrEmpty(cancelText) ? DEFAULT_CANCEL : cancelText;
            txtTip.text = string.IsNullOrEmpty(msg) ? "" : msg;
            
            Vector3 v3 = goConfirm.transform.localPosition;
            v3.x = isCancel ? -100 : 0;
            goConfirm.transform.localPosition = v3;
            goCancel.SetActive(isCancel);
            prefab.SetActive(true);
        }

        /// <summary>
        /// 关闭对话框
        /// </summary>
        public void Close()
        {
            if (prefab.activeSelf) prefab.SetActive(false);
        }

        /// <summary>
        /// 释放
        /// </summary>
        public void Dispose()
        {
            //Close();
            RemoveEvents();
            txtTip = null;
            txtConfirm = null;
            txtCancel = null;
            btnConfirm = null;
            btnCancel = null;
            goCancel = null;
            goConfirm = null;
            //prefab = null;
            callback = null;
        }

        private void AddEvents()
        {
            btnConfirm.onClick.AddListener(OnConfirm);
            btnCancel.onClick.AddListener(OnCancel);
        }

        private void RemoveEvents()
        {
            btnConfirm.onClick.RemoveListener(OnConfirm);
            btnCancel.onClick.RemoveListener(OnCancel);
        }

        private void OnConfirm()
        {
            Close();
            if (callback != null) callback(true);
        }

        private void OnCancel()
        {
            Close();
            if (callback != null) callback(false);
        }
    }
}
