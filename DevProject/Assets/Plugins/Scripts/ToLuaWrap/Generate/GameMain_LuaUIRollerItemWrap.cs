﻿//this source code was auto-generated by tolua#, do not modify it
using System;
using LuaInterface;

public class GameMain_LuaUIRollerItemWrap
{
	public static void Register(LuaState L)
	{
		L.BeginClass(typeof(GameMain.LuaUIRollerItem), typeof(GameMain.LuaUIComponent), null);
		L.RegFunction("SetDataDirty", SetDataDirty);
		L.RegFunction("DoRefreshData", DoRefreshData);
		L.RegFunction("SetBelongList", SetBelongList);
		L.RegFunction("Reset", Reset);
		L.RegFunction("Stop", Stop);
		L.RegFunction("Show", Show);
		L.RegFunction("Hide", Hide);
		L.RegFunction("Dispose", Dispose);
		L.RegFunction("__eq", op_Equality);
		L.RegFunction("__tostring", Lua_ToString);
		L.RegVar("_startPos", get__startPos, set__startPos);
		L.RegVar("_curPos", get__curPos, set__curPos);
		L.RegVar("onEnd", get_onEnd, set_onEnd);
		L.RegVar("use", get_use, set_use);
		L.RegVar("IsSelected", get_IsSelected, set_IsSelected);
		L.RegVar("Index", get_Index, set_Index);
		L.RegVar("ID", get_ID, set_ID);
		L.EndClass();
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SetDataDirty(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)ToLua.CheckObject(L, 1, typeof(GameMain.LuaUIRollerItem));
			obj.SetDataDirty();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int DoRefreshData(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)ToLua.CheckObject(L, 1, typeof(GameMain.LuaUIRollerItem));
			obj.DoRefreshData();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SetBelongList(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)ToLua.CheckObject(L, 1, typeof(GameMain.LuaUIRollerItem));
			LuaTable arg0 = ToLua.CheckLuaTable(L, 2);
			obj.SetBelongList(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Reset(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)ToLua.CheckObject(L, 1, typeof(GameMain.LuaUIRollerItem));
			obj.Reset();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Stop(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)ToLua.CheckObject(L, 1, typeof(GameMain.LuaUIRollerItem));
			obj.Stop();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Show(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)ToLua.CheckObject(L, 1, typeof(GameMain.LuaUIRollerItem));
			obj.Show();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Hide(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)ToLua.CheckObject(L, 1, typeof(GameMain.LuaUIRollerItem));
			obj.Hide();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Dispose(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)ToLua.CheckObject(L, 1, typeof(GameMain.LuaUIRollerItem));
			obj.Dispose();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int op_Equality(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			UnityEngine.Object arg0 = (UnityEngine.Object)ToLua.ToObject(L, 1);
			UnityEngine.Object arg1 = (UnityEngine.Object)ToLua.ToObject(L, 2);
			bool o = arg0 == arg1;
			LuaDLL.lua_pushboolean(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Lua_ToString(IntPtr L)
	{
		object obj = ToLua.ToObject(L, 1);

		if (obj != null)
		{
			LuaDLL.lua_pushstring(L, obj.ToString());
		}
		else
		{
			LuaDLL.lua_pushnil(L);
		}

		return 1;
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get__startPos(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)o;
			UnityEngine.Vector3 ret = obj._startPos;
			ToLua.Push(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index _startPos on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get__curPos(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)o;
			UnityEngine.Vector3 ret = obj._curPos;
			ToLua.Push(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index _curPos on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_onEnd(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)o;
			bool ret = obj.onEnd;
			LuaDLL.lua_pushboolean(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index onEnd on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_use(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)o;
			bool ret = obj.use;
			LuaDLL.lua_pushboolean(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index use on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_IsSelected(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)o;
			bool ret = obj.IsSelected;
			LuaDLL.lua_pushboolean(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index IsSelected on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_Index(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)o;
			int ret = obj.Index;
			LuaDLL.lua_pushinteger(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index Index on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_ID(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)o;
			int ret = obj.ID;
			LuaDLL.lua_pushinteger(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index ID on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set__startPos(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)o;
			UnityEngine.Vector3 arg0 = ToLua.ToVector3(L, 2);
			obj._startPos = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index _startPos on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set__curPos(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)o;
			UnityEngine.Vector3 arg0 = ToLua.ToVector3(L, 2);
			obj._curPos = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index _curPos on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_onEnd(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)o;
			bool arg0 = LuaDLL.luaL_checkboolean(L, 2);
			obj.onEnd = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index onEnd on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_use(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)o;
			bool arg0 = LuaDLL.luaL_checkboolean(L, 2);
			obj.use = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index use on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_IsSelected(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)o;
			bool arg0 = LuaDLL.luaL_checkboolean(L, 2);
			obj.IsSelected = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index IsSelected on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_Index(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)o;
			int arg0 = (int)LuaDLL.luaL_checknumber(L, 2);
			obj.Index = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index Index on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_ID(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			GameMain.LuaUIRollerItem obj = (GameMain.LuaUIRollerItem)o;
			int arg0 = (int)LuaDLL.luaL_checknumber(L, 2);
			obj.ID = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index ID on a nil value" : e.Message);
		}
	}
}

