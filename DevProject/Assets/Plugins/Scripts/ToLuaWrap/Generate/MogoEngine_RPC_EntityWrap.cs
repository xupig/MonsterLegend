﻿//this source code was auto-generated by tolua#, do not modify it
using System;
using LuaInterface;

public class MogoEngine_RPC_EntityWrap
{
	public static void Register(LuaState L)
	{
		L.BeginClass(typeof(MogoEngine.RPC.Entity), typeof(System.Object), null);
		L.RegFunction("OnInit", OnInit);
		L.RegFunction("OnEnterWorld", OnEnterWorld);
		L.RegFunction("OnLeaveWorld", OnLeaveWorld);
		L.RegFunction("OnReuse", OnReuse);
		L.RegFunction("OnRelease", OnRelease);
		L.RegFunction("OnEnterSpace", OnEnterSpace);
		L.RegFunction("OnLeaveSpace", OnLeaveSpace);
		L.RegFunction("OnCellAttached", OnCellAttached);
		L.RegFunction("SetAttr", SetAttr);
		L.RegFunction("SetAttrs", SetAttrs);
		L.RegFunction("SetAttrsByLuaTable", SetAttrsByLuaTable);
		L.RegFunction("RpcCall", RpcCall);
		L.RegFunction("RpcCallResp", RpcCallResp);
		L.RegFunction("ClearWait", ClearWait);
		L.RegFunction("RegisterWait", RegisterWait);
		L.RegFunction("ActionRpcCall", ActionRpcCall);
		L.RegFunction("ListRpcCall", ListRpcCall);
		L.RegFunction("SetPosition", SetPosition);
		L.RegFunction("SetRotation", SetRotation);
		L.RegFunction("SetEuler", SetEuler);
		L.RegFunction("Teleport", Teleport);
		L.RegFunction("GetTransform", GetTransform);
		L.RegFunction("RegisterPropertySetDelegate", RegisterPropertySetDelegate);
		L.RegFunction("GetPropertySetDelegate", GetPropertySetDelegate);
		L.RegFunction("RegisterMethodDelegate", RegisterMethodDelegate);
		L.RegFunction("GetMethodDelegate", GetMethodDelegate);
		L.RegFunction("New", _CreateMogoEngine_RPC_Entity);
		L.RegFunction("__tostring", Lua_ToString);
		L.RegVar("id", get_id, set_id);
		L.RegVar("dbid", get_dbid, set_dbid);
		L.RegVar("entityType", get_entityType, set_entityType);
		L.RegVar("typeId", get_typeId, set_typeId);
		L.RegVar("position", get_position, set_position);
		L.RegVar("rotation", get_rotation, set_rotation);
		L.RegVar("entityDef", get_entityDef, set_entityDef);
		L.RegVar("isInWorld", get_isInWorld, set_isInWorld);
		L.RegVar("canBeAttached", get_canBeAttached, set_canBeAttached);
		L.RegVar("isOpenWait", get_isOpenWait, set_isOpenWait);
		L.RegVar("cacheTag", get_cacheTag, set_cacheTag);
		L.RegVar("OnEnterWorldCB", get_OnEnterWorldCB, set_OnEnterWorldCB);
		L.RegVar("OnLeaveWorldCB", get_OnLeaveWorldCB, set_OnLeaveWorldCB);
		L.RegVar("timeStamp", get_timeStamp, set_timeStamp);
		L.RegVar("target", get_target, set_target);
		L.RegVar("pet_eid", get_pet_eid, set_pet_eid);
		L.RegVar("owner_id", get_owner_id, set_owner_id);
		L.RegVar("localToWorldMatrix", get_localToWorldMatrix, null);
		L.EndClass();
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int _CreateMogoEngine_RPC_Entity(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 0)
			{
				MogoEngine.RPC.Entity obj = new MogoEngine.RPC.Entity();
				ToLua.PushObject(L, obj);
				return 1;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to ctor method: MogoEngine.RPC.Entity.New");
			}
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int OnInit(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			obj.OnInit();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int OnEnterWorld(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			obj.OnEnterWorld();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int OnLeaveWorld(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			obj.OnLeaveWorld();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int OnReuse(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			uint arg0 = (uint)LuaDLL.luaL_checknumber(L, 2);
			obj.OnReuse(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int OnRelease(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			obj.OnRelease();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int OnEnterSpace(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			obj.OnEnterSpace();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int OnLeaveSpace(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			obj.OnLeaveSpace();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int OnCellAttached(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			obj.OnCellAttached();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SetAttr(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			string arg0 = ToLua.CheckString(L, 2);
			object arg1 = ToLua.ToVarObject(L, 3);
			obj.SetAttr(arg0, arg1);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SetAttrs(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			System.Collections.Generic.List<MogoEngine.RPC.EntityPropertyValue> arg0 = (System.Collections.Generic.List<MogoEngine.RPC.EntityPropertyValue>)ToLua.CheckObject(L, 2, typeof(System.Collections.Generic.List<MogoEngine.RPC.EntityPropertyValue>));
			obj.SetAttrs(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SetAttrsByLuaTable(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			string arg0 = ToLua.CheckString(L, 2);
			obj.SetAttrsByLuaTable(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int RpcCall(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			string arg0 = ToLua.CheckString(L, 2);
			object[] arg1 = ToLua.ToParamsObject(L, 3, count - 2);
			obj.RpcCall(arg0, arg1);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int RpcCallResp(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			string arg0 = ToLua.CheckString(L, 2);
			object[] arg1 = ToLua.CheckObjectArray(L, 3);
			obj.RpcCallResp(arg0, arg1);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int ClearWait(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			int arg0 = (int)LuaDLL.luaL_checknumber(L, 2);
			obj.ClearWait(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int RegisterWait(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			int arg0 = (int)LuaDLL.luaL_checknumber(L, 2);
			obj.RegisterWait(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int ActionRpcCall(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			string arg0 = ToLua.CheckString(L, 2);
			int arg1 = (int)LuaDLL.luaL_checknumber(L, 3);
			object[] arg2 = ToLua.ToParamsObject(L, 4, count - 3);
			obj.ActionRpcCall(arg0, arg1, arg2);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int ListRpcCall(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 4);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			string arg0 = ToLua.CheckString(L, 2);
			int arg1 = (int)LuaDLL.luaL_checknumber(L, 3);
			int[] arg2 = ToLua.CheckNumberArray<int>(L, 4);
			obj.ListRpcCall(arg0, arg1, arg2);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SetPosition(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			UnityEngine.Vector3 arg0 = ToLua.ToVector3(L, 2);
			obj.SetPosition(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SetRotation(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			UnityEngine.Quaternion arg0 = ToLua.ToQuaternion(L, 2);
			obj.SetRotation(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SetEuler(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			UnityEngine.Vector3 arg0 = ToLua.ToVector3(L, 2);
			obj.SetEuler(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Teleport(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			UnityEngine.Vector3 arg0 = ToLua.ToVector3(L, 2);
			obj.Teleport(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int GetTransform(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			UnityEngine.Transform o = obj.GetTransform();
			ToLua.Push(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int RegisterPropertySetDelegate(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			System.Type arg0 = (System.Type)ToLua.CheckObject(L, 2, typeof(System.Type));
			string arg1 = ToLua.CheckString(L, 3);
			obj.RegisterPropertySetDelegate(arg0, arg1);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int GetPropertySetDelegate(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			string arg0 = ToLua.CheckString(L, 2);
			System.Delegate o = obj.GetPropertySetDelegate(arg0);
			ToLua.Push(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int RegisterMethodDelegate(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			string arg0 = ToLua.CheckString(L, 2);
			obj.RegisterMethodDelegate(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int GetMethodDelegate(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)ToLua.CheckObject(L, 1, typeof(MogoEngine.RPC.Entity));
			string arg0 = ToLua.CheckString(L, 2);
			System.Reflection.MethodInfo o = obj.GetMethodDelegate(arg0);
			ToLua.PushObject(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, null);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Lua_ToString(IntPtr L)
	{
		object obj = ToLua.ToObject(L, 1);

		if (obj != null)
		{
			LuaDLL.lua_pushstring(L, obj.ToString());
		}
		else
		{
			LuaDLL.lua_pushnil(L);
		}

		return 1;
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_id(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			uint ret = obj.id;
			LuaDLL.lua_pushnumber(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index id on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_dbid(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			ulong ret = obj.dbid;
			LuaDLL.lua_pushnumber(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index dbid on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_entityType(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			string ret = obj.entityType;
			LuaDLL.lua_pushstring(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index entityType on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_typeId(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			ushort ret = obj.typeId;
			LuaDLL.lua_pushnumber(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index typeId on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_position(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			UnityEngine.Vector3 ret = obj.position;
			ToLua.Push(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index position on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_rotation(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			UnityEngine.Quaternion ret = obj.rotation;
			ToLua.Push(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index rotation on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_entityDef(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			MogoEngine.RPC.EntityDef ret = obj.entityDef;
			ToLua.PushObject(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index entityDef on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_isInWorld(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			bool ret = obj.isInWorld;
			LuaDLL.lua_pushboolean(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index isInWorld on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_canBeAttached(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			bool ret = obj.canBeAttached;
			LuaDLL.lua_pushboolean(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index canBeAttached on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_isOpenWait(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			bool ret = obj.isOpenWait;
			LuaDLL.lua_pushboolean(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index isOpenWait on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_cacheTag(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			uint ret = obj.cacheTag;
			LuaDLL.lua_pushnumber(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index cacheTag on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_OnEnterWorldCB(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			System.Action ret = obj.OnEnterWorldCB;
			ToLua.Push(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index OnEnterWorldCB on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_OnLeaveWorldCB(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			System.Action ret = obj.OnLeaveWorldCB;
			ToLua.Push(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index OnLeaveWorldCB on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_timeStamp(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			System.Reflection.MethodInfo ret = obj.timeStamp;
			ToLua.PushObject(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index timeStamp on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_target(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			object ret = obj.target;
			ToLua.Push(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index target on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_pet_eid(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			uint ret = obj.pet_eid;
			LuaDLL.lua_pushnumber(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index pet_eid on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_owner_id(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			uint ret = obj.owner_id;
			LuaDLL.lua_pushnumber(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index owner_id on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_localToWorldMatrix(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			UnityEngine.Matrix4x4 ret = obj.localToWorldMatrix;
			ToLua.PushValue(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index localToWorldMatrix on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_id(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			uint arg0 = (uint)LuaDLL.luaL_checknumber(L, 2);
			obj.id = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index id on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_dbid(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			ulong arg0 = (ulong)LuaDLL.luaL_checknumber(L, 2);
			obj.dbid = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index dbid on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_entityType(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			string arg0 = ToLua.CheckString(L, 2);
			obj.entityType = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index entityType on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_typeId(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			ushort arg0 = (ushort)LuaDLL.luaL_checknumber(L, 2);
			obj.typeId = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index typeId on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_position(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			UnityEngine.Vector3 arg0 = ToLua.ToVector3(L, 2);
			obj.position = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index position on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_rotation(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			UnityEngine.Quaternion arg0 = ToLua.ToQuaternion(L, 2);
			obj.rotation = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index rotation on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_entityDef(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			MogoEngine.RPC.EntityDef arg0 = (MogoEngine.RPC.EntityDef)ToLua.CheckObject(L, 2, typeof(MogoEngine.RPC.EntityDef));
			obj.entityDef = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index entityDef on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_isInWorld(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			bool arg0 = LuaDLL.luaL_checkboolean(L, 2);
			obj.isInWorld = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index isInWorld on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_canBeAttached(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			bool arg0 = LuaDLL.luaL_checkboolean(L, 2);
			obj.canBeAttached = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index canBeAttached on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_isOpenWait(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			bool arg0 = LuaDLL.luaL_checkboolean(L, 2);
			obj.isOpenWait = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index isOpenWait on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_cacheTag(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			uint arg0 = (uint)LuaDLL.luaL_checknumber(L, 2);
			obj.cacheTag = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index cacheTag on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_OnEnterWorldCB(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			System.Action arg0 = null;
			LuaTypes funcType2 = LuaDLL.lua_type(L, 2);

			if (funcType2 != LuaTypes.LUA_TFUNCTION)
			{
				 arg0 = (System.Action)ToLua.CheckObject(L, 2, typeof(System.Action));
			}
			else
			{
				LuaFunction func = ToLua.ToLuaFunction(L, 2);
				arg0 = DelegateFactory.CreateDelegate(typeof(System.Action), func) as System.Action;
			}

			obj.OnEnterWorldCB = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index OnEnterWorldCB on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_OnLeaveWorldCB(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			System.Action arg0 = null;
			LuaTypes funcType2 = LuaDLL.lua_type(L, 2);

			if (funcType2 != LuaTypes.LUA_TFUNCTION)
			{
				 arg0 = (System.Action)ToLua.CheckObject(L, 2, typeof(System.Action));
			}
			else
			{
				LuaFunction func = ToLua.ToLuaFunction(L, 2);
				arg0 = DelegateFactory.CreateDelegate(typeof(System.Action), func) as System.Action;
			}

			obj.OnLeaveWorldCB = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index OnLeaveWorldCB on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_timeStamp(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			System.Reflection.MethodInfo arg0 = (System.Reflection.MethodInfo)ToLua.CheckObject(L, 2, typeof(System.Reflection.MethodInfo));
			obj.timeStamp = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index timeStamp on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_target(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			object arg0 = ToLua.ToVarObject(L, 2);
			obj.target = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index target on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_pet_eid(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			uint arg0 = (uint)LuaDLL.luaL_checknumber(L, 2);
			obj.pet_eid = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index pet_eid on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_owner_id(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			MogoEngine.RPC.Entity obj = (MogoEngine.RPC.Entity)o;
			uint arg0 = (uint)LuaDLL.luaL_checknumber(L, 2);
			obj.owner_id = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index owner_id on a nil value" : e.Message);
		}
	}
}

