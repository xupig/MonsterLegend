﻿using GameLoader;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameLoader.Utils.XML;
using GameResource;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace GameMain
{
    internal class ResourceDownloadManager
    {
        public readonly List<int> LevelThreshold = new List<int>() { 0, 30, 60, 70, 110, 100000 };
        private string m_downloadedWebSourceInfoPath;
        private string m_resourcePath;
        //private string m_runtimeResourcePath;
        private string m_webResourcePath;
        private bool m_downloadIsAllDone;
        private int m_downloadFailCounter;
        //private Action<string, string, string> m_actionBeginDecompressPackage;
        private AddtiveResourceData m_currentHandlingPackage;

        public Action<AddtiveResourceData> HandlePackageBegin;
        public Action<AddtiveResourceData> HandlePackageFinish;
        public Action<AddtiveResourceData, float> HandlePackageProgress;
        private Action<string, byte[], int> CheckPackageMd5Action;

        //Stack<AddtiveResourceData> m_allAddtiveResourceDownloadStack = new Stack<AddtiveResourceData>();
        /// <summary>
        /// 普通下载队列
        /// </summary>
        Queue<AddtiveResourceData> m_allAddtiveResourceNormalQueue = new Queue<AddtiveResourceData>();
        /// <summary>
        /// 优先下载队列
        /// </summary>
        Queue<AddtiveResourceData> m_allAddtiveResourceHighQueue = new Queue<AddtiveResourceData>();
        /// <summary>
        /// 按等级排序必要资源包状态记录对象
        /// </summary>
        SortedList<int, AddtiveResourceData> m_allNecessaryResource = new SortedList<int, AddtiveResourceData>(new DuplicateKeyComparer<int>());
        /// <summary>
        /// 按等级排序非必要资源包状态记录对象
        /// </summary>
        SortedList<int, AddtiveResourceData> m_allNotNecessaryResource = new SortedList<int, AddtiveResourceData>(new DuplicateKeyComparer<int>());
        /// <summary>
        /// 下载资源包状态记录对象
        /// </summary>
        Dictionary<string, AddtiveResourceData> m_allAddtiveResourceDic = new Dictionary<string, AddtiveResourceData>();
        /// <summary>
        /// 所有已下载资源包
        /// </summary>
        public Dictionary<string, string> m_downloadedPackages = new Dictionary<string, string>();

        private object m_downloadStackLocker = new object();
        private object m_downloadedPackagesLocker = new object();
        private bool m_isStopDownload = false;
        private bool m_forceDownloadNotNecessary = false;
        private int m_currentLevelThrehold;
        private int m_nextLevelThrehold;
        private uint m_safeCheckDownloadTimer;
        private bool m_testNotDownloadSP = false;
        private bool m_checkSPMd5 = false;

        public bool DownloadIsAllDone
        {
            get
            {
                return m_downloadIsAllDone;
            }
        }

        public void Init()
        {
            m_currentLevelThrehold = LevelThreshold[0];
            m_nextLevelThrehold = LevelThreshold[1];
            m_forceDownloadNotNecessary = SystemConfig.GetValueInCfg("ForceNecessaryRes") == "1";
            CheckPackageMd5Action = CheckPackageMd5;
            //m_actionBeginDecompressPackage = BeginDecompressPackage;
            m_downloadedWebSourceInfoPath = String.Concat(Application.persistentDataPath, ConstString.RutimeResourceConfig, "/DownloadedInfo.txt");
            m_resourcePath = ObjectPool.ASSET_SP_PATH;
            //LoggerHelper.Error("m_resourcePath: " + m_resourcePath);
            //m_runtimeResourcePath = SystemConfig.RuntimeResourcePath;
            //LoggerHelper.Error("m_runtimeResourcePath: " + m_runtimeResourcePath);
            m_webResourcePath = SystemConfig.GetValueInCfg("WebResource");
            LoadDownloadedFiles();
            m_testNotDownloadSP = SystemConfig.GetValueInCfg("TestNotDownloadSP") == "1";
            m_checkSPMd5 = SystemConfig.GetValueInCfg("CheckSPMd5") == "1";
            if (SystemConfig.GetValueInCfg("SafeCheckDownload") == "1")
            {
                SafeCheckDownload();
            }
        }

        public void SafeCheckDownload()
        {
            m_safeCheckDownloadTimer = TimerHeap.AddTimer(60000, 30000, DownloadPackage);
        }

        public void SetAllAddtiveResourceList(List<AddtiveResourceData> allAddtiveResourceList)
        {
            //LoggerHelper.Debug("SetAllAddtiveResourceList: " + allAddtiveResourceList.Count);
            //for (int i = allAddtiveResourceList.Count - 1; i >= 0; i--)
            for (int i = 0; i < allAddtiveResourceList.Count; i++)
            {
                var item = allAddtiveResourceList[i];
                if (item.IsNecessary)
                    m_allNecessaryResource.Add(item.Level, item);
                else
                {
                    if (m_forceDownloadNotNecessary)
                        m_allNotNecessaryResource.Add(item.Level, item);
                }
                lock (m_downloadStackLocker)
                    m_allAddtiveResourceNormalQueue.Enqueue(item);
                var packageName = item.PackageName;
                m_allAddtiveResourceDic.Add(packageName, item);
                if (m_checkSPMd5)
                {
                    if (m_downloadedPackages.ContainsKey(packageName) && m_downloadedPackages[packageName] != item.Md5)//把下载过但md5不一致的下载记录去掉
                        m_downloadedPackages.Remove(packageName);
                }

                if (m_downloadedPackages.ContainsKey(packageName))//下载记录里还有就标记为已下载
                    item.IsDownloaded = true;
            }
            //Debug.LogWarning("m_allAddtiveResourceNormalQueue: " + m_allAddtiveResourceNormalQueue.Count);
            //Debug.LogWarning("m_downloadedPackages: " + m_downloadedPackages.Count);
        }

        public void StopDownloadPackage()
        {
            //Debug.LogWarning("StopDownloadPackage");
            m_isStopDownload = true;
        }

        public void DownloadPackage()
        {
            if (m_testNotDownloadSP)
                return;
            AddtiveResourceData ard = null;
            lock (m_downloadStackLocker)
                ard = m_currentHandlingPackage;
            if (ard != null)
                return;
            //Driver.Instance.StartCoroutine(DoDownloadPackage());
            DoDownloadPackage();
        }

        public void DoDownloadPackage()
        {
            if (m_isStopDownload)
                return;
            //yield break;
            var isDownloadFinished = false;
            lock (m_downloadStackLocker)
                isDownloadFinished = PeekAddtiveResourceData() == null;

            if (!isDownloadFinished)
            {
                AddtiveResourceData ar;
                lock (m_downloadStackLocker)
                    ar = PeekAddtiveResourceData();
                while (m_downloadedPackages.ContainsKey(ar.PackageName))
                {
                    //Debug.LogWarning("DoDownloadPackage m_downloadedPackages ContainsKey: " + ar.PackageName);
                    HandleNextPackage();
                    lock (m_downloadStackLocker)
                    {
                        var data = PeekAddtiveResourceData();
                        //PeekAddtiveResourceData();
                        isDownloadFinished = data == null;
                        //Debug.LogWarning("isDownloadFinished: " + isDownloadFinished + " " + (data != null ? data.PackageName + " " + data.IsDownloaded : "null"));
                        if (isDownloadFinished)
                        {
                            AllDownloadFinished();
                            return;
                            //yield break;
                        }
                    }
                    lock (m_downloadStackLocker)
                        ar = PeekAddtiveResourceData();
                    //yield return new WaitForFixedUpdate();
                }
                if (ar.Level > m_nextLevelThrehold)
                {
                    //LoggerHelper.Error("ar.Level > m_nextLevelThrehold: " + ar.PackageName + " " + ar.Level + " " + m_nextLevelThrehold);
                    return;
                    //yield break;
                }
                lock (m_downloadStackLocker)
                    m_currentHandlingPackage = ar;
                LoaderDriver.Invoke(HandlePackageBegin, ar);
                var fileName = ar.PackageName.Replace("/", "$") + "_" + ar.Md5 + ".pkg";
                var url = m_webResourcePath + fileName;
                //Debug.LogWarning("DownloadPackage: " + Path.Combine(m_runtimeResourcePath, fileName));
                //Debug.LogWarning("DownloadPackage: " + ar.PackageName);
                DownloadPackageProgress(1, ar.FileSize, ar.FileSize);
                HttpWrappr.instance.LoadWwwData(url, DownloadPackageFinish, DownloadPackageError, true);
            }
            else
            {
                AllDownloadFinished();
            }
        }

        public bool IsPackageDownloaded(string packageName)
        {
            if (m_downloadIsAllDone)
            {
                //LoggerHelper.Error("m_downloadIsAllDone: " + m_downloadIsAllDone);
                return true;
            }
            var result = false;
            lock (m_downloadedPackagesLocker)
            {
                if (m_allAddtiveResourceDic.ContainsKey(packageName))
                {
                    //if (!m_downloadedPackages.ContainsKey(packageName))
                    //LoggerHelper.Error("m_downloadedPackages.ContainsKey: " + packageName + ": " + m_downloadedPackages.ContainsKey(packageName));
                    result = m_downloadedPackages.ContainsKey(packageName);
                }
                else
                {
                    LoggerHelper.Error("packageName is not in m_allAddtiveResourceDic: " + packageName);
                    result = false;
                }
            }
            return result;
        }

        public bool IsLevelDownloaded(int level)
        {
            //LoggerHelper.Warning("m_downloadIsAllDone: " + m_downloadIsAllDone);
            if (m_downloadIsAllDone)
                return true;
            if (m_testNotDownloadSP)
                return true;

            for (int i = 0; i < m_allNecessaryResource.Count; i++)
            {
                if (m_allNecessaryResource.Keys[i] <= level)
                {
                    if (!m_downloadedPackages.ContainsKey(m_allNecessaryResource.Values[i].PackageName))
                        return false;
                }
                else
                {
                    if (m_forceDownloadNotNecessary)//强制下载非必要资源判断
                    {
                        for (int j = 0; j < m_allNotNecessaryResource.Count; j++)
                        {
                            if (m_allNotNecessaryResource.Keys[j] <= level)
                            {
                                if (!m_downloadedPackages.ContainsKey(m_allNotNecessaryResource.Values[j].PackageName))
                                    return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }
                    return true;
                }
            }
            return true;
        }

        public void CheckPreDownload(int level)
        {
            var newThresholdIndex = GetLevelBelongThresholdIndex(level);
            var newCurLevelThrehold = LevelThreshold[newThresholdIndex];
            m_nextLevelThrehold = LevelThreshold[newThresholdIndex + 1];
            if (newCurLevelThrehold != m_currentLevelThrehold)
            {
                //LoggerHelper.Error("CheckPreDownload: newCurLevelThrehold: " + newCurLevelThrehold + " m_currentLevelThrehold: " + m_currentLevelThrehold + " m_nextLevelThrehold: " + m_nextLevelThrehold);
                DownloadPackage();
            }
            m_currentLevelThrehold = newCurLevelThrehold;
        }

        private int GetLevelBelongThresholdIndex(int level)
        {
            for (int i = 0; i < LevelThreshold.Count; i++)
            {
                if (i + 1 < LevelThreshold.Count)
                {
                    if (LevelThreshold[i] <= level && level < LevelThreshold[i + 1])
                    {
                        return i;
                    }
                }
                else
                {
                    return i;
                }
            }
            LoggerHelper.Error("level not match LevelThreshold: " + LevelThreshold.PackList() + " level: " + level);
            return 0;
        }

        public List<AddtiveResourceData> GetAllNecessaryResources(int level)
        {
            if (m_downloadIsAllDone)
                return new List<AddtiveResourceData>();
            var result = new List<AddtiveResourceData>();
            for (int i = 0; i < m_allNecessaryResource.Count; i++)
            {
                if (m_allNecessaryResource.Keys[i] <= level)
                {
                    if (!m_downloadedPackages.ContainsKey(m_allNecessaryResource.Values[i].PackageName))
                        result.Add(m_allNecessaryResource.Values[i]);
                }
            }
            if (m_forceDownloadNotNecessary)//强制下载非必要资源判断
            {
                for (int i = 0; i < m_allNotNecessaryResource.Count; i++)
                {
                    if (m_allNotNecessaryResource.Keys[i] <= level)
                    {
                        if (!m_downloadedPackages.ContainsKey(m_allNotNecessaryResource.Values[i].PackageName))
                            result.Add(m_allNotNecessaryResource.Values[i]);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 插队下载
        /// </summary>
        /// <param name="res"></param>
        public void JumpDownloadQueue(List<AddtiveResourceData> res)
        {
            lock (m_downloadStackLocker)
            {
                for (int i = 0; i < res.Count; i++)
                {
                    m_allAddtiveResourceHighQueue.Enqueue(res[i]);
                }
            }
        }

        private void LoadDownloadedFiles()
        {
            if (!File.Exists(m_downloadedWebSourceInfoPath))
                return;

            var downloadedPackagesText = FileUtils.LoadFile(m_downloadedWebSourceInfoPath);
            var downloadedPackages = downloadedPackagesText.Split('\n');
            //LoggerHelper.Warning("downloadedFiles Length: " + downloadedPackages.Length);
            for (int i = 0; i < downloadedPackages.Length; i++)
            {
                if (!string.IsNullOrEmpty(downloadedPackages[i]))
                {
                    var index = downloadedPackages[i].LastIndexOf("_", StringComparison.Ordinal);
                    if (downloadedPackages[i].Length < index + 33)//33 = 1 + 32，1位下划线，32位md5长度
                        LoggerHelper.Error(downloadedPackages[i] + downloadedPackages[i].Length + " " + (index + 33), false);
                    var packageName = downloadedPackages[i].Substring(0, index);
                    var md5 = downloadedPackages[i].Substring(index + 1, 32);

                    m_downloadedPackages[packageName] = md5;
                    //LoggerHelper.Warning("m_downloadedFiles: " + downloadedPackages[i]);
                }
            }
        }

        private void DownloadPackageProgress(float progress, long bytesReceived, long totalBytesToReceive)
        {
            LoaderDriver.Invoke(HandlePackageProgress, m_currentHandlingPackage, progress);
        }

        private void DownloadPackageFinish(string url, byte[] bytes1, byte[] bytes2)
        {
            var fileName = Path.GetFileName(url);
            var md5Index = fileName.LastIndexOf("_", StringComparison.Ordinal);
            if (md5Index > 0)
            {
                CheckPackageMd5Action.BeginInvoke(fileName, bytes1, md5Index, null, null);
            }
            else
            {
                lock (m_downloadStackLocker)
                    m_currentHandlingPackage = null;
                LoggerHelper.Error("DownloadPackage Error: file name is bad " + fileName);
                HandleNextPackage();
                TimerHeap.AddTimer(1000, 0, DownloadPackage);
            }

            //TimerHeap.AddTimer(1000, 0, DownloadPackage);
        }

        private void CheckPackageMd5(string fileName, byte[] bytes, int md5Index)
        {
            var targetMd5 = fileName.Substring(md5Index + 1, 32);
            var curMd5 = MD5Utils.FormatMD5(MD5Utils.CreateMD5(bytes));
            if (targetMd5 == curMd5)
            {
                BeginDecompressPackage(bytes, fileName, targetMd5);
                //m_actionBeginDecompressPackage.BeginInvoke(filePath, url, targetMd5, null, null);
                return;
            }
            else
            {
                lock (m_downloadStackLocker)
                    m_currentHandlingPackage = null;
                LoggerHelper.Error("DownloadPackage Error: md5 not match " + fileName + " targetMd5: " + targetMd5 + " curMd5: " + curMd5);
                TimerHeap.AddTimer(1000, 0, DownloadPackage);
                //File.Delete(filePath);
            }
        }

        private void BeginDecompressPackage(byte[] data, string fileName, string targetMd5)
        {
            try
            {
                //Debug.LogWarning("BeginDecompressPackage: " + m_resourcePath + " " + filePath);
                MemoryStream ms = new MemoryStream(data);
                ms.DecompressToDirectory(m_resourcePath, DecompressProgress, true);
                //File.Delete(filePath);
                //var fileName = Path.GetFileName(url);
                var packageName = fileName.Substring(0, fileName.Length - 37).Replace("$", "/");//去掉长度为：_ + 32 + .pkg
                                                                                                //LoggerHelper.Error("packageName: " + packageName + " fileName: " + fileName);
                lock (m_downloadedPackagesLocker)
                {
                    m_downloadedPackages[packageName] = targetMd5;
                    m_allAddtiveResourceDic[packageName].IsDownloaded = true;
                    //Debug.LogWarning("m_downloadedPackages: " + packageName);
                    SaveDownloadedFile();
                }
                lock (m_downloadStackLocker)
                    m_currentHandlingPackage = null;
                HandleNextPackage();
                LoaderDriver.Invoke(DownloadPackage);
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        private void DownloadPackageError(string url, string error)
        {
            m_downloadFailCounter++;
            lock (m_downloadStackLocker)
                m_currentHandlingPackage = null;
            if (m_downloadFailCounter <= 5)
            {
                LoggerHelper.Info("DownloadPackageError: " + url + " error: " + error + " retry: " + m_downloadFailCounter);
            }
            else
            {
                HandleNextPackage();
            }
            TimerHeap.AddTimer(1000, 0, DownloadPackage);
        }

        private void DecompressProgress(long progress)
        {
            //LoaderDriver.Invoke(HandlePackageProgress.SafeInvoke, m_currentHandlingPackage, 0.5f + progress * 0.5f);//下载和解压各占一半进度
        }

        private void HandleNextPackage()
        {
            m_downloadFailCounter = 0;
            AddtiveResourceData ar;
            lock (m_downloadStackLocker)
                ar = DequeueAddtiveResourceData();
            if (ar != null)
            {
                //Debug.LogWarning("HandleNextPackage: " + ar.PackageName);
                LoaderDriver.Invoke(HandlePackageFinish, ar);
                //LoaderDriver.Invoke(HandlePackageFinish.SafeInvoke, ar);
            }
        }

        private void SaveDownloadedFile()
        {
            //return;
            var sb = new StringBuilder();
            foreach (var fileAndMd5 in m_downloadedPackages)
            {
                if (!string.IsNullOrEmpty(fileAndMd5.Key) && !string.IsNullOrEmpty(fileAndMd5.Value))
                    sb.AppendFormat("{0}_{1}\n", fileAndMd5.Key, fileAndMd5.Value);
            }
            //LoggerHelper.Info("SaveDownloadedFile: " + sb);
            //LoggerHelper.Info("m_downloadedWebSourceInfoPath: " + m_downloadedWebSourceInfoPath);
            FileUtils.SaveText(m_downloadedWebSourceInfoPath, sb.ToString());
        }

        private void AllDownloadFinished()
        {
            m_downloadIsAllDone = true;
            LoggerHelper.Info("DownloadPackage all done: " + m_downloadedPackages.Count);
            TimerHeap.DelTimer(m_safeCheckDownloadTimer);
        }

        private AddtiveResourceData PeekAddtiveResourceData()
        {
            if (m_allAddtiveResourceHighQueue.Count != 0)
                return m_allAddtiveResourceHighQueue.Peek();
            if (m_allAddtiveResourceNormalQueue.Count != 0)
                return m_allAddtiveResourceNormalQueue.Peek();
            return null;
        }

        private AddtiveResourceData DequeueAddtiveResourceData()
        {
            var ar = PeekAddtiveResourceData();
            if (ar != null && ar.IsDownloaded)
            {
                if (m_allAddtiveResourceHighQueue.Count != 0)
                    return m_allAddtiveResourceHighQueue.Dequeue();
                if (m_allAddtiveResourceNormalQueue.Count != 0)
                    return m_allAddtiveResourceNormalQueue.Dequeue();
            }
            return null;
        }
    }
}