﻿using GameLoader;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameLoader.Utils.XML;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace GameMain
{
    public class ResourceMappingManager
    {
        private static ResourceMappingManager m_instance;
        public static ResourceMappingManager Instance
        {
            get
            {
                if (m_instance == null)
                    m_instance = new ResourceMappingManager();
                return m_instance;
            }
        }

        public bool InitSuccess
        {
            get
            {
                return m_initSuccess;
            }
        }

        public long DownloadTotalCounter
        {
            get
            {
                return m_downloadTotalCounter;
            }
        }

        public long DownloadCurrentCounter
        {
            get
            {
                return m_downloadCurrentCounter;
            }
        }

        public Action<string> ActionPackageBegin;
        public Action<string, float> ActionPackageProgress;
        public Action<bool> ActionAllPackageFinish;

        private ResourceDownloadManager m_downloadManager = new ResourceDownloadManager();
        private WaitingLoadResourceUIManager m_uiManager = new WaitingLoadResourceUIManager();

        private string m_runtileResourceConfigPath;
        private Dictionary<string, ResourceMappingData> m_allResourceMappingDic = new Dictionary<string, ResourceMappingData>();
        private int m_retryCounter;
        private bool m_initSuccess;
        private bool m_isSmallPackage;

        private AddtiveResourceData m_ard = new AddtiveResourceData();//为解决ios stripping问题
        private ResourceMappingData m_rmd = new ResourceMappingData();//为解决ios stripping问题
        private List<AddtiveResourceData> m_downloadList;
        private List<AddtiveResourceData> m_finishedList = new List<AddtiveResourceData>();
        private long m_downloadTotalCounter;
        private long m_downloadCurrentCounter;
        private AddtiveResourceData m_currentDownloadPackage;
        private int m_currentPlayerLevel = 1;

        public ResourceMappingManager()
        {
            //为解决ios stripping问题
            m_ard.FileSize = 0;
            m_ard.id = 0;
            m_ard.IsDownloaded = false;
            m_ard.IsNecessary = false;
            m_ard.Level = 0;
            m_ard.Md5 = "";
            m_ard.PackageName = "";
            m_rmd.id = 0;
            m_rmd.PackageName = "";
            m_rmd.SourcePath = "";
            m_rmd.TargetPath = "";
        }

        public void Init()
        {
            if (SystemConfig.GetValueInCfg("IsSmallPackage") != "1")
                return;

            m_isSmallPackage = true;
            LoggerHelper.Info("ResourceMappingManager.Init");
            m_downloadManager.HandlePackageBegin = HandlePackageBegin;
            m_downloadManager.HandlePackageProgress = HandlePackageProgress;
            m_downloadManager.HandlePackageFinish = HandlePackageFinish;
            InitAsync(AfterInit);
            //Action async = InitAsync;
            //async();
            //async.BeginInvoke(null, null);
        }

        public void LoadUI()
        {
            if (SystemConfig.GetValueInCfg("IsSmallPackage") != "1")
                return;
            m_uiManager.LoadUI();
        }

        public void ShowLoading()
        {
            m_uiManager.ShowUI();
        }

        public void HideLoading()
        {
            m_uiManager.HideUI();
        }

        public string GetReplaceRes(string resourceName)
        {
            if (!m_isSmallPackage)
                return resourceName;

            //LoggerHelper.Info("GetFileMapping: " + resourceName);
            if (m_allResourceMappingDic.ContainsKey(resourceName))
            {
                return m_allResourceMappingDic[resourceName].TargetPath;
            }
            else
            {
                return resourceName;
            }
        }

        public string GetResMapping(string resourceName)
        {
            if (!m_isSmallPackage)
                return resourceName;

            //LoggerHelper.Error("GetFileMapping: " + resourceName);
            if (m_allResourceMappingDic.ContainsKey(resourceName))
            {
                var rmd = m_allResourceMappingDic[resourceName];
                if (!m_downloadManager.IsPackageDownloaded(rmd.PackageName))
                    return rmd.TargetPath;
                else
                    return resourceName;
            }
            else
            {
                return resourceName;
            }
        }

        public string[] GetResMappings(string[] resourcesName)
        {
            if (!m_isSmallPackage)
                return resourcesName;

            var replacedPath = new List<string>(resourcesName.Length);
            bool containNotDownloadedRes = false;
            for (int i = 0; i < resourcesName.Length; i++)
            {
                var resourceName = resourcesName[i];
                //if (resourceName == "Assets/Resources/Characters/Avatar/101/101_weapon_301.prefab")
                //{
                //    LoggerHelper.Error("m_allResourceMappingDicContainsKey: " + m_allResourceMappingDic.ContainsKey(resourceName));
                //}
                if (m_allResourceMappingDic.ContainsKey(resourceName))
                {
                    var rmd = m_allResourceMappingDic[resourceName];
                    if (!m_downloadManager.IsPackageDownloaded(rmd.PackageName))
                        containNotDownloadedRes = true;
                    //LoggerHelper.Error("GetResMappings not loaded: " + resourcesName.PackArray());
                    replacedPath.Add(rmd.TargetPath);
                    //LoggerHelper.Info(resourceName);
                }
                else
                {
                    //LoggerHelper.Error("GetResMappings loaded: " + resourcesName.PackArray());
                    replacedPath.Add(resourceName);
                }
            }
            if (containNotDownloadedRes)
            {
                //LoggerHelper.Error("GetResMappings not loaded: " + resourcesName.PackArray());
                return replacedPath.ToArray();
            }
            else
            {
                return resourcesName;
            }
        }

        public bool IsLevelReady(int level)
        {
            if (!m_isSmallPackage)
                return true;
            CheckPreDownload(level);
            return m_downloadManager.IsLevelDownloaded(level);
        }

        public void CheckPreDownload(int level)
        {
            m_downloadManager.CheckPreDownload(level);
        }

        public long GetNecessaryResources(int level)
        {
            m_currentPlayerLevel = level;
            m_finishedList = new List<AddtiveResourceData>();
            m_downloadList = m_downloadManager.GetAllNecessaryResources(level);
            //LoggerHelper.Error("DownloadNecessaryResources: " + m_downloadList.Select(t => t.PackageName).ToList().PackList());
            m_downloadTotalCounter = 0;
            m_downloadCurrentCounter = 0;
            for (int i = 0; i < m_downloadList.Count; i++)
            {
                m_downloadTotalCounter += m_downloadList[i].FileSize;
            }
            m_downloadManager.JumpDownloadQueue(m_downloadList);
            return m_downloadTotalCounter / 1024 / 1024;
        }

        public void DownloadNecessaryResources()
        {
            if (m_downloadManager.DownloadIsAllDone)
                m_downloadManager.DownloadPackage();
        }

        public string GetCurrentDownloadPackageName()
        {
            if (m_currentDownloadPackage != null)
                return m_currentDownloadPackage.PackageName;
            else
                return "";
        }

        public void StopDownloadPackage()
        {
            m_downloadManager.StopDownloadPackage();
        }

        private void InitAsync(Action<bool> callback)
        {
            m_runtileResourceConfigPath = String.Concat(Application.persistentDataPath, ConstString.RutimeResourceConfig);
            m_downloadManager.Init();
            LoadFile("AddtiveResourceList", (text) =>
            {
                if (string.IsNullOrEmpty(text))
                {
                    callback(false);
                    return;
                }
                var addResList = XMLUtils.LoadXMLText<AddtiveResourceData>(text);
                addResList.Sort((Comparison<AddtiveResourceData>)delegate (AddtiveResourceData a, AddtiveResourceData b)
                {
                    return a.Level > b.Level ? 1 : a.Level == b.Level ? 0 : -1;
                });
                m_downloadManager.SetAllAddtiveResourceList(addResList);
                //LoggerHelper.Info("AddtiveResourceData: " + text);
                LoadFile("ResourceMapping", (text1) =>
                {
                    //LoggerHelper.Info("ResourceMapping: " + text1);
                    if (string.IsNullOrEmpty(text1))
                    {
                        callback(false);
                        return;
                    }
                    var list = XMLUtils.LoadXMLText<ResourceMappingData>(text1);
                    for (int i = 0; i < list.Count; i++)
                    {
                        var path = list[i].SourcePath;
                        if (!m_allResourceMappingDic.ContainsKey(path))
                            m_allResourceMappingDic.Add(path, list[i]);
                        else
                            LoggerHelper.Error("Same key already in m_allResourceMappingDic: " + list[i].SourcePath);
                    }
                    //LoggerHelper.Error(m_allResourceMappingDic.PackMap());
                    m_downloadManager.DownloadPackage();
                    callback(true);
                    //if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
                    //{
                    //    LoggerHelper.Info("Current is wifi, begin DownloadPackage.");
                    //}
                    //else
                    //    LoggerHelper.Info("Current is not wifi, not begin DownloadPackage.");
                });
            });
        }

        private void AfterInit(bool isSuccess)
        {
            if (!isSuccess)
            {
                if (m_retryCounter < 3)
                {
                    m_retryCounter++;
                    TimerHeap.AddTimer<Action<bool>>(1000, 0, InitAsync, AfterInit);
                }
            }
            else
            {
                m_initSuccess = true;
            }
        }

        private void LoadFile(string key, Action<string> callback)
        {
            var url = SystemConfig.GetValueInCfg(key);
            var fileName = Path.Combine(m_runtileResourceConfigPath, Path.GetFileName(url));
            //LoggerHelper.Error("LoadFile fileName: " + fileName);
            if (File.Exists(fileName))
            {
                var targetMd5 = SystemConfig.GetValueInCfg(key + "Md5");
                var fileMd5 = MD5Utils.BuildFileMd5(fileName);
                if (targetMd5 == fileMd5)
                {
                    callback(FileUtils.LoadFile(fileName));
                    return;
                }
            }
            HttpWrappr.instance.LoadWwwText(url, (u, text) =>
            {
                FileUtils.SaveText(fileName, text);
                callback(text);
            });
        }

        private void HandlePackageBegin(AddtiveResourceData package)
        {
            m_currentDownloadPackage = package;
            ActionPackageBegin.SafeInvoke(package.PackageName);
        }

        private void HandlePackageProgress(AddtiveResourceData package, float progress)
        {
            //Debug.LogWarning(package.PackageName + " " + progress);
            ActionPackageProgress.SafeInvoke(package.PackageName, (m_downloadCurrentCounter + package.FileSize * progress) / m_downloadTotalCounter);
        }

        private void HandlePackageFinish(AddtiveResourceData package)
        {
            if (m_downloadList == null)
                return;
            //Debug.LogWarning("HandlePackageFinish: " + package.PackageName);
            if (!m_finishedList.Contains(package) && m_downloadList.Contains(package))
            {
                m_finishedList.Add(package);
                m_downloadCurrentCounter += package.FileSize;
                if (m_finishedList.Count == m_downloadList.Count)
                {
                    //Debug.LogWarning("HandlePackageFinish last:\n" + m_finishedList.Select(t => t.PackageName).ToList().PackList() + "\n" + m_downloadList.Select(t => t.PackageName).ToList().PackList());
                    CheckPackageFinish();
                }
            }
        }

        private void CheckPackageFinish()
        {
            var list = m_downloadManager.GetAllNecessaryResources(m_currentPlayerLevel);

            //Debug.LogWarning("CheckPackageFinish m_downloadedPackages: " + m_downloadManager.m_downloadedPackages.Keys.ToList().PackList());
            //Debug.LogWarning("CheckPackageFinish AllNecessaryResources: " + list.Select(t => t.PackageName).ToList().PackList());
            ActionAllPackageFinish.SafeInvoke(list.Count == 0);
            EventDispatcher.TriggerEvent("OnActionAllPackageFinish", list.Count == 0);
        }
    }
}