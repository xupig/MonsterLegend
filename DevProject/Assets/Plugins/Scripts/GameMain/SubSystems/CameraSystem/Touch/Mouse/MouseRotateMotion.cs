﻿using GameData;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameMain.GlobalManager
{
    public class MouseRotateMotion : TouchRotateMotion
    {
        private Vector3 _mousePosition;
        private Vector3 _lastMousePosition;
        private Vector3 _firstMousePosition;
        private int _inputButton = 0;
        public MouseRotateMotion(float touchDistanceToRotation,
            float touchLeftRange, float touchRightRange, float touchTopRange, float touchBottomRange)
            : base(touchDistanceToRotation, 
                touchLeftRange, touchRightRange, touchTopRange, touchBottomRange)
        {
        }

        public override void OnUpdate()
        {
            _inputButton = (int)KeyboardManager.GetInstance().MouseClickMode;
            if (Input.GetMouseButton(_inputButton))
            {
                _mousePosition = Input.mousePosition;
                if (_status == TouchStatus.IDLE)
                {
                    if (CheckTouchRange() && !InControlStick() && CheckFullUI() && CheckMouseOverUI() && CheckRotateRange())
                    {
                        Begin();
                    }
                }
                else
                {
                    Touching();
                }
            }
            else
            {
                if (_status == TouchStatus.TOUCHING)
                {
                    End();
                }
            }
        }

        private void Begin()
        {
            _lastMousePosition = _mousePosition;
            if (_rotationY == 0)
            {
                _rotationY = _originalRotationY;
            }
            if (_rotationX == 0)
            {
                _rotationX = _originalRotationX;
            }
            _status = TouchStatus.TOUCHING;
        }

        private float _lastRotationXOffset = 0;
        private void Touching()
        {
            float deltaX = _mousePosition.x - _lastMousePosition.x;
            float deltaY = _mousePosition.y - _lastMousePosition.y;
            float deltaRotationY = deltaX * _touchDistanceToRotation;
            float deltaRotationX = deltaY * _touchDistanceToRotation;
            _rotationYOffset += deltaRotationY;
            _rotationXOffset += deltaRotationX;
            if (_rotationYOffset >= 360) _rotationYOffset -= 360;
            if (_rotationYOffset <= -360) _rotationYOffset += 360;
            if (_rotationXOffset >= 360) _rotationXOffset -= 360;
            if (_rotationXOffset <= -360) _rotationXOffset += 360;
            _rotationY = _originalRotationY + _rotationYOffset;
            _rotationX = _originalRotationX + _rotationXOffset;
            if (_rotationY >= 360) _rotationY -= 360;
            if (_rotationY <= -360) _rotationY += 360;
            if (_rotationX >= 360) _rotationX -= 360;
            if (_rotationX <= -360) _rotationX += 360;
            if (_rotationX < _minRotationX || _rotationX > _maxRotationX)
            {
                _rotationX = Mathf.Clamp(_rotationX, _minRotationX, _maxRotationX);
                _rotationXOffset = _lastRotationXOffset;
            }
            _lastMousePosition = _mousePosition;
            _lastRotationXOffset = _rotationXOffset;
        }

        private void End()
        {
            _status = TouchStatus.IDLE;
            _firstMousePosition.Set(0, 0, 0);
        }

        private bool CheckTouchRange()
        {
            return CheckRange(_mousePosition.x, _mousePosition.y);
        }

        private bool CheckMouseOverUI()
        {
            /*
            if(PCFingerGuidePanel.isShow || (FingerGuidePanel.isShow && PlatformHelper.InEditorPlatform()))
            {
                return true;
            }*/
            return !EventSystem.current.IsPointerOverGameObject();
        }

        private bool CheckRotateRange()
        {
            if (_firstMousePosition == Vector3.zero)
            {
                _firstMousePosition = _mousePosition;
                return false;
            }
            
            float distance = Vector3.Distance(_mousePosition, _firstMousePosition);
            /*float xDistance = ACTSystem.ACTMathUtils.Abs(_mousePosition.x - _firstMousePosition.x);
            float sinAngle = xDistance / distance;*/
            return distance > 1f;
        }
    }
}
