﻿using GameData;

namespace GameMain.GlobalManager
{
    public class MouseTouchesMotion : BaseTouchesMotion
    {
        protected override void OnInit(float distanceDefaultScale, float touchLeftRange, float touchRightRange, float touchTopRange, float touchBottomRange)
        {
            string[] args = global_params_helper.GetGlobalParam(GlobalParamId.mouse_camera_params).Split(',');
            float scrollWheelAxis2Distance = float.Parse(args[0]);
            float scrollDuration = float.Parse(args[1]);
            float minDistance = float.Parse(args[2]);
            float minRotationX = float.Parse(args[3]);
            float touchDistance2Rotation = float.Parse(args[4]);

            _touchScaleMotion = new MouseScaleMotion(distanceDefaultScale, scrollWheelAxis2Distance, scrollDuration, minDistance, minRotationX,
                touchLeftRange, touchRightRange, touchTopRange, touchBottomRange);
            _touchRotateMotion = new MouseRotateMotion(touchDistance2Rotation,
                touchLeftRange, touchRightRange, touchTopRange, touchBottomRange);
        }
    }
}
