﻿using GameLoader.Utils;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class FingerRotateMotion : TouchRotateMotion
    {
        private Touch _touch;
        private float _startRotateMinDistance;
        private Vector3 _firstMousePosition;

        public FingerRotateMotion(float touchDistanceToRotation,
           float touchLeftRange, float touchRightRange, float touchTopRange, float touchBottomRange,
           float startRotateMinDistance)
            : base(touchDistanceToRotation,
                touchLeftRange, touchRightRange, touchTopRange, touchBottomRange)
        {
            _startRotateMinDistance = startRotateMinDistance;
        }

        public override void OnUpdate()
        {
            if (CheckTouchCount())
            {
                if (_touch.phase == TouchPhase.Moved)
                {
                    if (_status == TouchStatus.IDLE)
                    {
                        if (CheckTouchRange() && CheckFullUI() && CheckDeltaPosition() && CheckRotateRange())
                        {
                            Begin();
                        }
                    }
                    else
                    {
                        Touching();
                    }
                }
            }
            else
            {
                if (_status == TouchStatus.TOUCHING)
                {
                    End();
                }
            }
        }

        private bool CheckTouchCount()
        {
            int count = Input.touchCount;
            if (count > 2 || count == 0)
            {
                return false;
            }

            int fingerId = GetStickFingerId();
            Touch[] touches = Input.touches;
            bool result = false;
            if (count == 1)
            {
                if (fingerId != touches[0].fingerId)
                {
                    _touch = touches[0];
                    result = true;
                }
            }
            else
            {
                if (fingerId == touches[0].fingerId)
                {
                    _touch = touches[1];
                    result = true;
                }
                else if (fingerId == touches[1].fingerId)
                {
                    _touch = touches[0];
                    result = true;
                }
            }
            return result;
        }

        private void Begin()
        {
            if (_rotationY == 0)
            {
                _rotationY = _originalRotationY;
            }
            if (_rotationX == 0)
            {
                _rotationX = _originalRotationX;
            }
            _status = TouchStatus.TOUCHING;
        }

        private float _lastRotationXOffset = 0;
        private void Touching()
        {
            float deltaX = _touch.deltaPosition.x;
            float deltaY = _touch.deltaPosition.y;
            float deltaRotationY = deltaX * _touchDistanceToRotation;
            float deltaRotationX = deltaY * _touchDistanceToRotation;
            _rotationYOffset += deltaRotationY;
            _rotationXOffset += deltaRotationX;
            if (_rotationYOffset >= 360) _rotationYOffset -= 360;
            if (_rotationYOffset <= -360) _rotationYOffset += 360;
            if (_rotationXOffset >= 360) _rotationXOffset -= 360;
            if (_rotationXOffset <= -360) _rotationXOffset += 360;
            _rotationY = _originalRotationY + _rotationYOffset;
            _rotationX = _originalRotationX + _rotationXOffset;
            if (_rotationY >= 360) _rotationY -= 360;
            if (_rotationY <= -360) _rotationY += 360;
            if (_rotationX >= 360) _rotationX -= 360;
            if (_rotationX <= -360) _rotationX += 360;
            if (_rotationX < _minRotationX || _rotationX > _maxRotationX)
            {
                _rotationX = Mathf.Clamp(_rotationX, _minRotationX, _maxRotationX);
                _rotationXOffset = _lastRotationXOffset;
            }
            _lastRotationXOffset = _rotationXOffset;
        }

        private void End()
        {
            _status = TouchStatus.IDLE;
            _firstMousePosition.Set(0, 0, 0);
        }

        private bool CheckTouchRange()
        {
            return CheckRange(_touch.position.x, _touch.position.y);
        }

        private bool CheckDeltaPosition()
        {
            return ACTSystem.ACTMathUtils.Abs(_touch.deltaPosition.x) >= _startRotateMinDistance;
        }

        private bool CheckRotateRange()
        {
            if (_firstMousePosition == Vector3.zero)
            {
                _firstMousePosition = _touch.position;
                return false;
            }
            
            float distance = Vector3.Distance(_touch.position, _firstMousePosition);
            /*float xDistance = ACTSystem.ACTMathUtils.Abs(_touch.position.x - _firstMousePosition.x);
            float sinAngle = xDistance / distance;*/
            return distance > 1f;
        }

        public override void UpdateArgs(float touchDistanceToRotation, float startRotateMinDistance)
        {
            _startRotateMinDistance = startRotateMinDistance;
            base.UpdateArgs(touchDistanceToRotation, startRotateMinDistance);
        }
    }
}
