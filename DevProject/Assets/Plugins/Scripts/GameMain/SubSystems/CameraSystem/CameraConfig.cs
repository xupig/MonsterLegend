﻿
/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/18 20:32:30 
 * function: 摄像机常量
 * *******************************************************/

using UnityEngine;
namespace GameMain.GlobalManager
{
    public enum CameraMotionType : byte
    {
        ROTATION_TARGET = 1,        //锁定角度，目标，距离
        ROTATION_POSITION = 2,          //锁定角度，位置
        TARGET_POSITION = 3,            //锁定目标，位置
        TWO_TARGET = 4,              //双目标锁定
        LOOK_AT_FORWARD = 5, //锁定目标前方
        ROTATION_PK_TARGET = 6, //战斗锁定角度，目标，距离
        TWO_TARGET_PLUS = 7,              //双目标锁定加强版
        DRAG_SKILL = 8,              //拖动技能模式
        FACE_TARGRT = 9,
        FIXED_ANGLE = 10,        //越肩视角
        CLOSE_UP = 11,        //锁定角度，目标，距离缓动
    }

    public enum CameraAccordingMode : byte
    {
        AccordingMotion = 0,        //缓动模式
        AccordingAnimation          //动画模式
    }

    public class CameraConfig
    {
        public const int CAMERA_TYPE_ROTATE = 0;
        public const int CAMERA_TYPE_ROTATE_DIRECTLY = 1;
        public const int CAMERA_TYPE_SHAKE_1 = 2;
        public const int CAMERA_TYPE_SHAKE_2 = 3;
        public const int CAMERA_TYPE_MOVE = 4;
    }

    public struct CameraInfoBackup
    {
        public CameraAccordingMode accordingMode;
        public CameraMotionType motionType;
        public Vector3 position;
        public Vector3 localEulerAngles;
        public float distance;
        public float distanceScale;
        public Transform target;
        public float rotationMinSpeed;
    }
}
