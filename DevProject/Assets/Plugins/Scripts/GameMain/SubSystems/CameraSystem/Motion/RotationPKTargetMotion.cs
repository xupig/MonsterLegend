﻿using Common.ClientConfig;
using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class RotationPKTargetMotion : BaseCameraMotion
    {
        public bool keepTouchesScale = false;
        private float _originalDistance = 0f;
        public float originalDistance { get { return _originalDistance; } }
        private Vector3 _originalRotation;
        public Vector3 originalRotation { get { return _originalRotation; } }

        private bool _isAdjustingRotation = false;
        private bool _isAdjustingDistance = false;
        private Vector3 _rotationSpeed;
        private Vector3 _rotationAcceleration;
        private float _rotationAccelerateDuration;
        private Vector3 _startRotation;
        private float _distanceSpeed;

        private Vector3 _rotation;
        private float _distance;

        private float _focusPointAdjustTime;
        private float _focusPointMinSpeed;
        private float _focusPointOffset;

        private BaseTouchesMotion _touchesMotion;

        public bool IsScaled()
        {
            return _touchesMotion != null && _touchesMotion.distanceScale < 1;
        }

        public override bool IsAdjusting()
        {
            return _isAdjustingDistance || _isAdjustingRotation;
        }

        public override CameraMotionType GetCameraType()
        {
            return CameraMotionType.ROTATION_PK_TARGET;
        }

        public RotationPKTargetMotion(CameraData data)
            : base(data)
        {
            InitParams();
        }

        public override void Start()
        {
            if (_touchesMotion == null)
            {
                if (Common.Global.PlatformHelper.InTouchPlatform())
                {
                    _touchesMotion = new FingerTouchesMotion();
                }
                else
                {
                    _touchesMotion = new MouseTouchesMotion();
                }
            }
            bool canTouch = CanTouch();
            if (canTouch)
            {
                _touchesMotion.OnStart(data.targetDistance, data.targetRotation.x, data.targetRotation.y, keepTouchesScale);
            }
            _originalDistance = data.targetDistance;
            _originalRotation = data.targetRotation;
            if (keepTouchesScale && canTouch)
            {
                data.targetDistance = _touchesMotion.curDistance;
                data.targetRotation.x = _touchesMotion.GetRotationXByDistance(data.targetDistance);
            }
            _updateImmediately = false;
            _isAdjustingDistance = true;
            _isAdjustingRotation = true;
            _duration = 0;
            _rotation = _startRotation = camera.localEulerAngles;
            _distance = Vector3.Distance(data.target.position, camera.position);
            if (data.rotationDuration > 0)
            {
                if (data.rotationAccelerateRate == 0)
                {
                    _rotationSpeed = (data.targetRotation - _rotation) / data.rotationDuration;
                    _rotationAcceleration = Vector3.zero;
                    _rotationAccelerateDuration = 0;
                }
                else
                {
                    if (data.rotationAccelerateRate > 0.5f) data.rotationAccelerateRate = 0.5f;
                    _rotationAccelerateDuration = data.rotationDuration * data.rotationAccelerateRate;
                    _rotationAcceleration = (data.targetRotation - _rotation) / (_rotationAccelerateDuration * (data.rotationDuration - _rotationAccelerateDuration));
                    _rotationSpeed = _rotationAcceleration * _rotationAccelerateDuration;
                }
            }
            else
            {
                _updateImmediately = true;
            }
            if (data.distanceDuration > 0)
            {
                _distanceSpeed = (data.targetDistance - _distance) / data.distanceDuration;
            }
            else
            {
                _updateImmediately = true;
            }
            if (_updateImmediately)
            {
                OnUpdate();
            }
        }

        public override void OnUpdate()
        {
            if (IsAdjusting())
            {
                _duration += deltaTime;
                if (_isAdjustingRotation)
                {
                    AdjustRotation();
                }
                if (_isAdjustingDistance)
                {
                    AdjustDistance();
                }
            }
            else if (CanTouch())
            {
                _touchesMotion.OnUpdate();
                if (_touchesMotion.scaleStatus == TouchStatus.TOUCHING)
                {
                    _distance = _touchesMotion.distance;
                }
                if (_touchesMotion.rotateStatus == TouchStatus.TOUCHING)
                {
                    _rotation.x = _touchesMotion.rotationRotationX;
                    _rotation.y = _touchesMotion.rotationRotationY;
                    data.targetRotation.y = _rotation.y;
                    data.targetRotation.x = _rotation.x;
                }
            }
            UpdateFocusPoint();
            UpdateCamera();
            UpdateCameraByWall();
        }

        static Ray _downRay = new Ray(Vector3.zero, Vector3.down);
        static RaycastHit _raycastHit = new RaycastHit();
        static int cameraWallLayerValue = (1 << PhysicsLayerDefine.LAYER_CAMERA_WALL);
        protected void UpdateCameraByWall()
        {
            Vector3 focusPoint = data.focusPoint;
            Vector3 dir = camera.position - focusPoint;
            _downRay.direction = dir.normalized;
            _downRay.origin = focusPoint;
            _raycastHit.point = Vector3.zero;
            Physics.Raycast(_downRay, out _raycastHit, 50f, cameraWallLayerValue);
            float newDis = (_raycastHit.point - focusPoint).magnitude;
            if (newDis < dir.magnitude)
            {
                camera.position = _raycastHit.point;
            }
        }

        private float deltaY = 1.5f;
        private float maxDiveH = 1;
        private float maxElevation = 70;
        private float maxPitchDownAngle = 30;
        private int cameraWall = 1 << ACTSystem.ACTSystemTools.NameToLayer("CameraWall");
        protected override void UpdateCamera()
        {
            Vector3 p = data.focusPoint;
            float rx = _rotation.x;
            if (rx > maxElevation)
            {
                rx = maxElevation;
            }
            if (rx < -maxPitchDownAngle)
            {
                rx = -maxPitchDownAngle;
            }
            Matrix4x4 ltow = new Matrix4x4();
            ltow.SetTRS(p, Quaternion.Euler(new Vector3(rx, 0, 0)), Vector3.one);
            Vector3 p1 = ltow.MultiplyPoint(new Vector3(0, 0, -_distance));
            float dy = p1.y - p.y;
            float realdistance = 0;
            float h = -maxDiveH;
            if (dy < h)
            {
                realdistance = (h / dy) * _distance;
            }
            else
            {
                realdistance = _distance;
            }
            camera.position = p - Vector3.forward * realdistance;
            camera.RotateAround(p, new Vector3(1, 0, 0), rx);
            camera.RotateAround(p, new Vector3(0, 1, 0), _rotation.y);
            camera.LookAt(p);
        }

        private void UpdateFocusPoint()
        {
            Vector3 targetFocusPoint = data.target.position;
            if ((targetFocusPoint - data.focusPoint).magnitude >= _focusPointOffset)
            {
                data.focusPoint = targetFocusPoint;
                return;
            }
            Vector3 lerp = Vector3.Lerp(data.focusPoint, targetFocusPoint, deltaTime / _focusPointAdjustTime);

            float minOffset = _focusPointMinSpeed * deltaTime;
            Vector3 lerpOffset = lerp - data.focusPoint;
            if ((targetFocusPoint - data.focusPoint).magnitude < minOffset)
            {
                data.focusPoint = targetFocusPoint;
            }
            else
            {
                if (lerpOffset.magnitude < minOffset)
                {
                    data.focusPoint = data.focusPoint + lerpOffset.normalized * minOffset;
                }
                else
                {
                    data.focusPoint = lerp;
                }
            }
        }

        private void AdjustRotation()
        {
            if (data.rotationDuration == 0 || _duration >= data.rotationDuration || _rotation == data.targetRotation)
            {
                _rotation = data.targetRotation;
                _isAdjustingRotation = false;
                return;
            }
            UpdateRotation();
        }

        private void UpdateRotation()
        {
            if (_rotationAccelerateDuration == 0)
            {
                _rotation += _rotationSpeed * deltaTime;
                return;
            }

            if (data.rotationDuration - _duration <= _rotationAccelerateDuration)
            {
                float t = (data.rotationDuration - _duration);
                Vector3 offset = 0.5f * _rotationAcceleration * t * t;
                _rotation = data.targetRotation - offset;
            }
            else if (_duration <= _rotationAccelerateDuration)
            {
                Vector3 offset = 0.5f * _rotationAcceleration * _duration * _duration;
                _rotation = _startRotation + offset;
            }
            else
            {
                _rotation += _rotationSpeed * deltaTime;
            }
        }

        private void AdjustDistance()
        {
            if (data.distanceDuration == 0 || _duration >= data.distanceDuration || _distance == data.targetDistance)
            {
                _distance = data.targetDistance;
                _isAdjustingDistance = false;
                return;
            }
            _distance += _distanceSpeed * deltaTime;
        }

        public override void Stop()
        {
            _isAdjustingDistance = _isAdjustingRotation = false;
        }

        public void UpdateRange(float top, float right, float bottom, float left)
        {
            _touchesMotion.UpdateRange(top, right, bottom, left);
        }

        private bool CanTouch()
        {
            return true;
        }

        public override void Stretch(float offset, float speed, float duration)
        { }

        private void InitParams()
        {
            List<float> argList = data_parse_helper.ParseListFloat(global_params_helper.GetGlobalParam(GlobalParamId.rotate_camera_params).Split(','));
            _focusPointAdjustTime = argList[0];
            _focusPointMinSpeed = argList[1];
            _focusPointOffset = argList[2];
            List<float> defaultRotateArgs = data_parse_helper.ParseListFloat(global_params_helper.GetGlobalParam(GlobalParamId.default_camera_h_rotate_params).Split(','));
            deltaY = defaultRotateArgs[3];
            maxDiveH = defaultRotateArgs[2];
            maxElevation = defaultRotateArgs[0];
            maxPitchDownAngle = defaultRotateArgs[1];
        }
    }
}

