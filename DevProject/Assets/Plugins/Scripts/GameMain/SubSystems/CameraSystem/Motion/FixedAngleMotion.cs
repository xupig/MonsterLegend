﻿using Common.ClientConfig;
using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class FixedAngleMotion : BaseCameraMotion
    {
        private bool _isAdjustingRotation = false;
        private bool _isAdjustingDistance = false;
        private float _xLength = 1f;
        private float _yLength = 1f;

        public FixedAngleMotion(CameraData data)
            : base(data)
        {
            //InitParams();
        }

        public override bool IsAdjusting()
        {
            return _isAdjustingDistance || _isAdjustingRotation;
        }

        public override CameraMotionType GetCameraType()
        {
            return CameraMotionType.FIXED_ANGLE;
        }

        public override void Start()
        {
            _xLength = Mathf.Cos(Mathf.Deg2Rad * data.targetRotation.x);
            _yLength = Mathf.Sin(Mathf.Deg2Rad * data.targetRotation.x);
        }

        public override void OnUpdate()
        {
            UpdateCamera();
        }

        Vector3 targetPos = Vector3.zero;
        Vector3 lockPoint = Vector3.zero;
        protected override void UpdateCamera()
        {
            targetPos = Vector3.forward * _xLength * data.targetDistance + data.target.position;
            targetPos.y += data.targetDistance * _yLength;
            camera.position = targetPos;
            camera.LookAt(data.target);
        }

        public override void Stop()
        {
            _isAdjustingDistance = _isAdjustingRotation = false;
        }

        public override void Stretch(float offset, float speed, float duration)
        { 
        }
    }
}

