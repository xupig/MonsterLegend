﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using LitJson;
using UnityEngine;
using GameLoader.Utils;

namespace GameMain
{
    public class CommonLocalCacheWriter
    {
#if !UNITY_WEBPLAYER
        private string _logFileName = "common.so";
        private string _logFilePath = "";
        private FileStream _fs;

        public CommonLocalCacheWriter()
        {
            string logPath = string.Concat(UnityEngine.Application.persistentDataPath, ConstString.RutimeResourceConfig, "/cache");
            if (!Directory.Exists(logPath))
                Directory.CreateDirectory(logPath);
            _logFilePath = string.Concat(logPath, "/", _logFileName);
        }

        /// <summary>
        /// 写本地缓存
        /// </summary>
        /// <param name="content"></param>
        public void WriteLocalCache(string content)
        {
            try
            {
                _fs = new FileStream(_logFilePath, FileMode.OpenOrCreate, FileAccess.Write);
                byte[] bytes = ASCIIEncoding.UTF8.GetBytes(content);
                _fs.SetLength(bytes.Length);
                _fs.Write(bytes, 0, bytes.Length);
                _fs.Flush();
                _fs.Close();
                _fs.Dispose();
                _fs = null;
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message);
            }
        }

        /// <summary>
        /// 读取本地缓存
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> ReadLocalCache()
        {
            try
            {
                _fs = new FileStream(_logFilePath, FileMode.OpenOrCreate, FileAccess.Read);
                if (_fs.Length == 0)
                {
                    _fs.Close();
                    _fs.Dispose();
                    _fs = null;
                    return new Dictionary<string, string>();
                }
                byte[] data = new byte[_fs.Length];
                _fs.Read(data, 0, data.Length);
                _fs.Close();
                _fs.Dispose();
                _fs = null;
                string content = ASCIIEncoding.UTF8.GetString(data);
                return JsonMapper.ToObject<Dictionary<string, string>>(content);
            }
            catch (Exception ex)
            {
                CleanLocalCache();
                LoggerHelper.Error(ex.Message);
            }
            return new Dictionary<string, string>();
        }

        public void CleanLocalCache()
        {
            WriteLocalCache(JsonMapper.ToJson(new Dictionary<string, string>()));
        }
#else

        const string COMMON_LOCAL_CACHE = "CommonLocalCache";
        public CommonLocalCacheWriter()
        {
            
        }

        /// <summary>
        /// 写本地缓存
        /// </summary>
        /// <param name="content"></param>
        public void WriteLocalCache(string content)
        {
            try
            {
                content = WWW.EscapeURL(content);
                PlayerPrefs.SetString(COMMON_LOCAL_CACHE, content);
                //Debug.LogError("WriteCommonLocalCache Save:" + content.Length);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message);
            }
        }

        /// <summary>
        /// 读取本地缓存
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> ReadLocalCache()
        {
            try
            {
                string content = PlayerPrefs.GetString(COMMON_LOCAL_CACHE);
                content = WWW.UnEscapeURL(content);
                return JsonMapper.ToObject<Dictionary<string, string>>(content);
            }
            catch (Exception ex)
            {
                CleanLocalCache();
                LoggerHelper.Error(ex.Message);
            }
            return new Dictionary<string, string>();
        }

        public void CleanLocalCache()
        {
            WriteLocalCache(JsonMapper.ToJson(new Dictionary<string, string>()));
        }

#endif
    }
}
