﻿#region 模块信息
/*==========================================
// 文件名：LocalCache
// 命名空间: GameLogic.GameLogic.Common.Global
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/14 11:12:23
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameMain
{
    public class LocalCache
    {
        public static void Write(string localId, object data, CacheLevel level)
        {
            LocalCacheHelper.Write(localId, data, level, (int)(Common.Global.Global.serverTimeStamp / 1000));
        }

        public static T Read<T>(string localId)
        {
            return LocalCacheHelper.Read<T>(localId);
        }
    }
}
