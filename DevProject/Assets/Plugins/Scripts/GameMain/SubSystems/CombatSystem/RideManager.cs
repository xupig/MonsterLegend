﻿using ACTSystem;
using Common.ClientConfig;
using Common.ServerConfig;
using GameData;
using GameLoader.Utils;
using GameMain.ClientConfig;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Mgrs;
using UnityEngine;

namespace GameMain
{
    public class RideManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class RideManager
    {
        public static readonly int RIDE_BUFF = 9989;
        public static bool CanShowRide = true;
        private EntityCreature _owner;
        private int _actorID = 0;
        public int actorID { get { return _actorID; } }
        private bool _isRiding = false;
        public bool isRiding { get { return _isRiding; } }
        private float _speed = 0;
        public float speed { get { return _speed; } }
        private Vector3 _modelScale = Vector3.one;
        public Vector3 modelScale { get { return _modelScale; } }
        public ACTActor ride
        {
            get
            {
                if (_owner == null || _owner.actor == null)
                    return null;
                return _owner.actor.ride;
            }
        }

        public RideManager() { }
        public void SetOwner(EntityCreature owner)
        {
            _owner = owner;
        }

        public void Release()
        {
            DestroyActor();
            ClearData();
        }

        public void ClearData()
        {
            _owner = null;
            CanShowRide = true;
            _owner = null;
            _actorID = 0;
            _isRiding = false;
            _speed = 0;
            _modelScale = Vector3.one;
            animationEvent = null;
        }

        public void SetMount(int actorID, float modelScale)
        {
            _actorID = actorID;
            _modelScale = new Vector3(modelScale, modelScale, modelScale);
            RefreshRide();
        }

        public void RefreshRide()
        {
            //if (_owner.isHideEntity) return; 
            if (_owner.actor == null) return;
            if (_actorID == 0 || !CanShowRide)
            {
                Dismount();
            }
            else
            {
                Mount();
            }
        }

        bool NeedHideOtherRide()
        {
            if (!_owner.IsPlayer() && (PlayerSettingManager.hideOtherPlayer || _owner.isHideEntity)) return true;
            return false;
        }


        //上坐骑
        bool Mount()
        {
            _isRiding = true;
            var ride = _owner.actor.ride;
            if (ride != null && ride.ActorID == _actorID && ride.localScale == _modelScale)
            {
                this._owner.moveManager.UpdateActorSpeedRate();
                return false;
            }
            CreateActor();
            return true;
        }

        //下坐骑
        bool Dismount()
        {
            //_owner.bufferManager.RemoveBuffer(RIDE_BUFF);
            DestroyActor();
            if (_owner.IsPlayer())
            {
                GameObject.DontDestroyOnLoad(_owner.actor.gameObject);
                var slotCamera = _owner.actor.boneController.GetBoneByName(ACTBoneController.SLOT_CAMERA_NAME);
                CameraTargetProxy.instance.SetTargetTransform(slotCamera);
                CameraManager.GetInstance().SetFollowingTarget(CameraTargetProxy.instance.transform);
            }
            _isRiding = false;
            this._owner.moveManager.UpdateActorSpeedRate();
            return true;
        }

        AnimationEvent animationEvent;
        void CreateActor()
        {
            var closeActorID = _actorID;
            GameObjectPoolManager.GetInstance().CreateActorGameObject(closeActorID, (actor) =>
            {
                if (actor == null)
                {
                    LoggerHelper.Error(string.Format("创建坐骑模型出错，坐骑模型id = {0}找不到对应的模型。", closeActorID));
                    return;
                }
                if (_owner == null || !_owner.isInWorld || _actorID != closeActorID)
                {
                    GameObjectPoolManager.GetInstance().ReleaseActorGameObject(actor.actorData.ID, actor.gameObject);
                    return;
                }
                ACTActor rideActor = ride;
                if (_modelScale.x == -1)
                {
                    //-1表示使用模型本身大小
                    _modelScale = actor.localScale;
                }
                actor.localScale = _modelScale;
                _owner.actor.rideController.Mount(actor);
                CameraManager.GetInstance().mainCamera.enabled = false;
                CameraManager.GetInstance().mainCamera.enabled = true;
                ride.name = string.Format("Ride:{0}-{1}", _owner.id, closeActorID);
                this._owner.moveManager.UpdateActorSpeedRate();
                if (rideActor != null)
                {
                    GameObjectPoolManager.GetInstance().ReleaseActorGameObject(rideActor.actorData.ID, rideActor.gameObject);
                }
                //诡异的特效显示问题
                actor.gameObject.SetActive(false);
                actor.gameObject.SetActive(true);
                animationEvent = actor.gameObject.GetComponent<AnimationEvent>();
                if (animationEvent == null)
                {
                    animationEvent = actor.gameObject.AddComponent<AnimationEvent>();
                    animationEvent.onFootEvent = OnFootEvent;
                }
                ACTSystemTools.SetFxShow(actor.transform, !PlayerSettingManager.disableFx);
                if (_owner.IsPlayer())
                {
                    var slotCamera = actor.boneController.GetBoneByName(ACTBoneController.SLOT_CAMERA_NAME);
                    CameraTargetProxy.instance.SetTargetTransform(slotCamera);
                    CameraManager.GetInstance().SetFollowingTarget(CameraTargetProxy.instance.transform);
                    GameObject.DontDestroyOnLoad(actor.gameObject);
                    if (!_owner.bufferManager.ContainsBuffer(RIDE_BUFF))
                    {
                        //_owner.bufferManager.AddBuffer(RIDE_BUFF);
                    }
                    ride.footStep.canUse = true;
                }
                if (NeedHideOtherRide())
                {
                    actor.visible = false;
                }
            });
        }

        protected void OnFootEvent(int param)
        {
            if (ride == null) return;
            ride.footStep.OnFootEvent(ride, param);
        }

        void DestroyActor()
        {
            if (ride == null)
            {
                return;
            }
            ACTActor rideActor = ride;
            _owner.actor.rideController.Dismount();
            if (rideActor != null)
            {
                GameObjectPoolManager.GetInstance().ReleaseActorGameObject(rideActor.actorData.ID, rideActor.gameObject);
            }
        }
    }
}
