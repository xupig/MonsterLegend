﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameMain;

using ACTSystem;

namespace GameMain.CombatSystem
{

    public class BufferSubject
    {
        public int bufferID;

        public bool isClientBuff = false;
        public bool canReplaceByServer = false;

        private EntityCreature _owner;
        protected ACTHandler _actHandler;

        private BufferData _bufferData;
        public BufferData bufferData
        {
            get { return _bufferData; }
        }

        private float _updateTime;

        private bool _isPlaying = false;

        private float _duration;

        private float _endTime;

        public float remainTime
        {
            get { return _endTime - RealTime.time; }
        }

        protected float _pastTime = 0;
        protected List<SkillEventData> _skillEventList = new List<SkillEventData>();
        protected List<BufferTimeEffectData> _skillTimeEffectList = new List<BufferTimeEffectData>();

        List<uint> _visualFXGUIDList = new List<uint>();

        public BufferSubject()
        {
            _actHandler = new ACTHandler();
            _actHandler.priority = VisualFXPriority.H;
        }

        public void Reset(BufferData data)
        {
            _bufferData = data;
            bufferID = data.buffID;
            _visualFXGUIDList.Clear();
            _pastTime = 0;
        }

        public void SetOwner(EntityCreature owner)
        {
            _owner = owner;
            _actHandler.qualitySettingValue = _owner.qualitySettingValue;
        }

        protected void ResetSkillEventList()
        {
            _skillEventList.Clear();
            var skillEvents = _bufferData.skillEvents;
            for (int i = 0; i < skillEvents.Count; i++)
            {
                _skillEventList.Add(skillEvents[i]);
            }
        }

        protected void ResetTimeEffectList()
        {
            _skillTimeEffectList.Clear();
            var timeEffects = _bufferData.timeEffect;
            if (timeEffects == null) return;
            for (int i = 0; i < timeEffects.Count; i++)
            {
                _skillTimeEffectList.Add(timeEffects[i]);
            }
        }

        public bool Update()
        {
            if (!_isPlaying) { return _isPlaying; }
            _isPlaying = !IsOver();
            if (_isPlaying)
            {
                _pastTime += UnityPropUtils.deltaTime;
                HandleUpdataEffect();
                DoingEvents();
                DoingTimeEffects();
            }
            return _isPlaying;
        }

        public void Start(float duration = -1f)
        {
            if (duration > 0) //大于零按输入参数作为duration
            {
                _duration = duration * 0.001f;
            }
            else if (duration == 0) //等于零按表格输入数据作为duration 
            {
                _duration = _bufferData.totalTime * 0.001f;
            }
            else //小于零作为永久buff
            {
                _duration = duration;
            }
            _endTime = RealTime.time + _duration;
            _isPlaying = true;
            //修改对应状态
            ResetSkillEventList();
            ResetTimeEffectList();
            HandleStartEffect();
        }

        public void Stop()
        {
            _endTime = 0;
            HandleEndEffect();
        }

        public void End()
        {
            _endTime = 0;
            HandleEndEffect();
        }

        public bool IsOver()
        {
            return _duration > 0 && _endTime < RealTime.time;
        }

        #region 具体效果

        void DoingEvents()
        {
            for (int i = _skillEventList.Count - 1; i >= 0; i--)
            {
                var eventData = _skillEventList[i];
                if (eventData.delay < 0)
                {
                    eventData.delay = ACTSkillEventHandler.GetEventDelay(eventData.mainID, eventData.eventIdx);
                }
                if (eventData.delay < _pastTime)
                {
                    _actHandler.OnEvent(_owner.actor, eventData);
                    _skillEventList.RemoveAt(i);
                }
            }
        }

        void DoingTimeEffects()
        {
            for (int i = _skillTimeEffectList.Count - 1; i >= 0; i--)
            {
                var timeEffectData = _skillTimeEffectList[i];
                if (timeEffectData.exeTime < _pastTime)
                {
                    _skillTimeEffectList.RemoveAt(i);
                    BufferEffectHandler.GetInstance().ExecEffect(_owner, this.isClientBuff, timeEffectData.effectName, timeEffectData.effectValue);
                }
            }
        }

        private void HandleStartEffect()
        {
            BufferEffectHandler.GetInstance().ExecEffects(_owner, this.isClientBuff, _bufferData.startEffect);
            PlayBuffFX();
        }

        private void HandleEndEffect()
        {
            BufferEffectHandler.GetInstance().ExecEffects(_owner, this.isClientBuff, _bufferData.endEffect);
            StopBuffFX();
        }

        public void HandleUpdataEffect()
        {
            if (_bufferData.updateEffect != null)
            {
                if (_updateTime < UnityPropUtils.realtimeSinceStartup)
                {
                    BufferEffectHandler.GetInstance().ExecEffects(_owner, this.isClientBuff, _bufferData.updateEffect);
                    float delayTime = _bufferData.delayTime == 0 ? 1 : _bufferData.delayTime * 0.001f;
                    _updateTime = UnityPropUtils.realtimeSinceStartup + delayTime;
                }
            }
        }

        
        public void PlayBuffFX()
        {
            if (_owner.actor == null) { 
                return; 
            }
            var fxIDList = _bufferData.buffVisualFXs;
            for(int i = 0;i < fxIDList.Count;i++)
            {
                var fxID = fxIDList[i];
                var visualFXID = ACTVisualFXManager.GetInstance().PlayACTVisualFX(fxID, _owner.actor, -1, _owner.qualitySettingValue);
				if (visualFXID != 0)
				{
					_visualFXGUIDList.Add(visualFXID);
				}
            }
        }

        public void PlaySpecialFX()
        {
            if (_owner.actor == null)
            {
                return;
            }
            
            var fxID = _bufferData.special_hitfx_id;
            //GameLoader.Utils.LoggerHelper.Error("1_______buffID:   " + _bufferData.buffID + ",special_hitfx_id: " + _bufferData.special_hitfx_id);
            if (fxID == 0) return;
            //var fxID = 2501;
            var visualFXID = ACTVisualFXManager.GetInstance().PlayACTVisualFX(fxID, _owner.actor, -1, _owner.qualitySettingValue);
            if (visualFXID != 0)
            {
                _visualFXGUIDList.Add(visualFXID);
            }

        }


        public void StopBuffFX()
        {
            for (int i = 0; i < _visualFXGUIDList.Count; i++)
            {
                ACTVisualFXManager.GetInstance().StopVisualFX(_visualFXGUIDList[i]);
            }
            _visualFXGUIDList.Clear();
        }

        #endregion
    }
}
