﻿using System;
using System.Collections.Generic;
using System.Text;

using MogoEngine.RPC;
using GameMain;

using MogoEngine;
using Common.ClientConfig;
using Common.ServerConfig;
using UnityEngine;

using GameLoader.Utils;
using GameData;
using MogoEngine.Events;

using ACTSystem;
using GameMain.GlobalManager;
using GameLoader.Utils.Timer;

namespace GameMain.CombatSystem
{
    public class BufferEffectHandler
    {
        private static BufferEffectHandler s_instance = null;
        public static BufferEffectHandler GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new BufferEffectHandler();
            }
            return s_instance;
        }
        private delegate bool EffectHandleFun(EntityCreature entity, string[] args);
        private Dictionary<string, EffectHandleFun> _clientEffectFunMap = new Dictionary<string, EffectHandleFun>(46);
        private Dictionary<string, EffectHandleFun> _serverEffectFunMap = new Dictionary<string, EffectHandleFun>(34);
        private BufferEffectHandler()
        {
            InitClientEffectFunMap();
            InitServerEffectFunMap();
        }

        void InitClientEffectFunMap()
        {
            _clientEffectFunMap.Add("add_hp", AddHP);
            _clientEffectFunMap.Add("add_hp_per", AddHPPer);
            _clientEffectFunMap.Add("add_attri", AddAttri);
            _clientEffectFunMap.Add("set_state", SetState);
            _clientEffectFunMap.Add("unset_state", UnsetState);
            _clientEffectFunMap.Add("cast_spell", CastSpell);
            _clientEffectFunMap.Add("add_adjust_spell", AddAdjustSpell);
            _clientEffectFunMap.Add("del_adjust_spell", DelAdjustSpell);
            _clientEffectFunMap.Add("add_spell_action", AddSpellAction);
            _clientEffectFunMap.Add("del_spell_action", DelSpellAction);
            _clientEffectFunMap.Add("play_action", PlayAction);
            _clientEffectFunMap.Add("stop_action", StopAction);
            _clientEffectFunMap.Add("add_speed_rate", AddSpeedRate);
            _clientEffectFunMap.Add("del_speed_rate", DelSpeedRate);
            _clientEffectFunMap.Add("shake", CameraShake);
            _clientEffectFunMap.Add("stretch", CameraStretch);
            _clientEffectFunMap.Add("add_shader", AddShader);
            _clientEffectFunMap.Add("del_shader", DelShader);
            _clientEffectFunMap.Add("add_buff", AddBuff);
            _clientEffectFunMap.Add("add_notice", AddNotice);
            _clientEffectFunMap.Add("del_notice", DelNotice);
            _clientEffectFunMap.Add("add_timescale", AddTimeScale);
            _clientEffectFunMap.Add("del_timescale", DelTimeScale);
            _clientEffectFunMap.Add("add_scale_rate", AddScaleRate);
            _clientEffectFunMap.Add("del_scale_rate", DelScaleRate);
            _clientEffectFunMap.Add("break_spells", BreakSpells);
            _clientEffectFunMap.Add("change_gravity", ChangeGravity);
            _clientEffectFunMap.Add("stop_horizontal", StopHorizontal);
            _clientEffectFunMap.Add("stop_vertical", StopVertical);
            _clientEffectFunMap.Add("del_movement", StopMovement);
            _clientEffectFunMap.Add("show_weapon", ShowWeapon);
            _clientEffectFunMap.Add("hide_weapon", HideWeapon);
            _clientEffectFunMap.Add("pack_up_weapon", PackUpWeapon);
            _clientEffectFunMap.Add("add_shield_against", AddShieldAgainst);
            _clientEffectFunMap.Add("del_shield_against", DelShieldAgainst);
            _clientEffectFunMap.Add("set_jump_speed", SetJumpSpeed);
            _clientEffectFunMap.Add("control_ui", ControlUI);
            _clientEffectFunMap.Add("start_buff_event", StartBuffEvent);
            _clientEffectFunMap.Add("end_buff_event", EndBuffEvent);
            _clientEffectFunMap.Add("control_camera", ControlCamera);
            _clientEffectFunMap.Add("set_lock_target", SetLockTarget);
            _clientEffectFunMap.Add("set_transform", SetTransformt);
            _clientEffectFunMap.Add("pause_action", PauseAction);
            _clientEffectFunMap.Add("change_material", ChangeMaterial);
            _clientEffectFunMap.Add("set_super_armor_level", SetSuperArmorLevel);
            _clientEffectFunMap.Add("add_super_armor_level", AddSuperArmorLevel);
        }

        void InitServerEffectFunMap()
        {
            _serverEffectFunMap.Add("set_state", SetState);
            _serverEffectFunMap.Add("unset_state", UnsetState);
            _serverEffectFunMap.Add("cast_spell", CastSpell);
            _serverEffectFunMap.Add("add_adjust_spell", AddAdjustSpell);
            _serverEffectFunMap.Add("del_adjust_spell", DelAdjustSpell);
            _serverEffectFunMap.Add("add_spell_action", AddSpellAction);
            _serverEffectFunMap.Add("del_spell_action", DelSpellAction);
            _serverEffectFunMap.Add("play_action", PlayAction);
            _serverEffectFunMap.Add("stop_action", StopAction);
            _serverEffectFunMap.Add("add_speed_rate", AddSpeedRate);
            _serverEffectFunMap.Add("del_speed_rate", DelSpeedRate);
            _serverEffectFunMap.Add("shake", CameraShake);
            _serverEffectFunMap.Add("stretch", CameraStretch);
            _serverEffectFunMap.Add("add_shader", AddShader);
            _serverEffectFunMap.Add("del_shader", DelShader);
            _serverEffectFunMap.Add("add_notice", AddNotice);
            _serverEffectFunMap.Add("del_notice", DelNotice);
            _serverEffectFunMap.Add("add_timescale", AddTimeScale);
            _serverEffectFunMap.Add("del_timescale", DelTimeScale);
            _serverEffectFunMap.Add("add_scale_rate", AddScaleRate);
            _serverEffectFunMap.Add("del_scale_rate", DelScaleRate);
            _serverEffectFunMap.Add("break_spells", BreakSpells);
            _serverEffectFunMap.Add("change_gravity", ChangeGravity);
            _serverEffectFunMap.Add("stop_horizontal", StopHorizontal);
            _serverEffectFunMap.Add("stop_vertical", StopVertical);
            _serverEffectFunMap.Add("add_shield_against", AddShieldAgainst);
            _serverEffectFunMap.Add("del_shield_against", DelShieldAgainst);
            _serverEffectFunMap.Add("set_jump_speed", SetJumpSpeed);
            _serverEffectFunMap.Add("control_ui", ControlUI);
            _serverEffectFunMap.Add("start_buff_event", StartBuffEvent);
            _serverEffectFunMap.Add("end_buff_event", EndBuffEvent);
            _serverEffectFunMap.Add("set_transform", SetTransformt);
            _serverEffectFunMap.Add("pause_action", PauseAction);
            _serverEffectFunMap.Add("change_material", ChangeMaterial);
        }

        public void ExecEffects(EntityCreature entity, bool isClientBuff, List<BufferEffectData> effects)
        {           
            if (effects == null) return;
            for (int i = 0; i < effects.Count;i++ )
            {
                var effect = effects[i];
                ExecEffect(entity, isClientBuff, effect.effectName, effect.effectValue);
            }
        }

        public void ExecEffect(EntityCreature entity, bool isClientBuff, string effectName, string[] args)
        {
            if (isClientBuff)
            {
                ExecEffectClient(entity, effectName, args);
            }
            else
            {
                ExecEffectServer(entity, effectName, args);
            }
        }

        public bool ExecEffectClient(EntityCreature entity, string effectName, string[] args)
        {
            if (_clientEffectFunMap.ContainsKey(effectName))
            {
                return _clientEffectFunMap[effectName](entity, args);
            }
            return false;
        }

        public bool ExecEffectServer(EntityCreature entity, string effectName, string[] args)
        {
            if (_serverEffectFunMap.ContainsKey(effectName))
            {
                return _serverEffectFunMap[effectName](entity, args);
            }
            return false;
        }

        #region 修改属性
        private bool AddHP(EntityCreature entity, string[] args)
        {
            int hpChange = int.Parse(args[0]);
            int newHP = entity.cur_hp + hpChange;
            newHP = Mathf.Max(0, Mathf.Min(newHP, entity.max_hp));
            MogoWorld.SynEntityAttr(entity, EntityPropertyDefine.cur_hp, newHP);
            if (entity.cur_hp == 0)
            {
                entity.stateManager.SetState(state_config.AVATAR_STATE_DEATH, true);
            }
            if (hpChange != 0)
            {
                entity.bufferManager.OnBuffHpChange(hpChange);
            }
            return true;
        }

        private bool AddHPPer(EntityCreature entity, string[] args)
        {
            int hpChange = (int)(float.Parse(args[0]) * entity.max_hp);
            int newHP = entity.cur_hp + hpChange;
            newHP = Mathf.Max(0, Mathf.Min(newHP, entity.max_hp));
            MogoWorld.SynEntityAttr(entity, EntityPropertyDefine.cur_hp, newHP);
            if (entity.cur_hp == 0)
            {
                entity.stateManager.SetState(state_config.AVATAR_STATE_DEATH, true);
            }
            if (hpChange != 0)
            {
                entity.bufferManager.OnBuffHpChange(hpChange);
            }
            return true;
        }

        private bool AddAttri(EntityCreature entity, string[] args)
        {
            /*
            var attriID = int.Parse(args[0]);
            var attriAddValue = float.Parse(args[1]);
            var oldValue = entity.GetAttribute((fight_attri_config)attriID);
            if (attri_config_helper.IsFloat(attriID))
            {
                attriAddValue *= 10000;
                oldValue *= 10000;
            }
            int newValue = (int)(oldValue + attriAddValue);
            entity.OnSyncOneBa((byte)attriID, newValue);*/
            return true;
        }

        private bool AddSpeedRate(EntityCreature entity, string[] args)
        {
            entity.moveManager.AddSpeedMlp(float.Parse(args[0]));
            return true;
        }

        private bool DelSpeedRate(EntityCreature entity, string[] args)
        {
            entity.moveManager.RemoveSpeedMlp(float.Parse(args[0]));
            return true;
        }
        #endregion

        #region 设置状态
        private bool SetState(EntityCreature entity, string[] args)
        {
            entity.stateManager.AccumulateState(int.Parse(args[0]), 1);
            return true;
        }

        private bool UnsetState(EntityCreature entity, string[] args)
        {
            entity.stateManager.AccumulateState(int.Parse(args[0]), -1);
            return true;
        }
        #endregion

        #region 技能相关
        private bool CastSpell(EntityCreature entity, string[] args)
        {
            entity.skillManager.PlaySkill(int.Parse(args[0]));
            return true;
        }

        private bool AddAdjustSpell(EntityCreature entity, string[] args)
        {
            entity.skillManager.AddAdjustSkillID(int.Parse(args[0]));
            return true;
        }

        private bool DelAdjustSpell(EntityCreature entity, string[] args)
        {
            entity.skillManager.RemoveAdjustSkillID(int.Parse(args[0]));
            return true;
        }

        private bool AddSpellAction(EntityCreature entity, string[] args)
        {
            entity.skillManager.AddAdjustSkillActionID(int.Parse(args[0]));
            return true;
        }

        private bool DelSpellAction(EntityCreature entity, string[] args)
        {
            entity.skillManager.RemoveAdjustSkillActionID(int.Parse(args[0]));
            return true;
        }

        private bool BreakSpells(EntityCreature entity, string[] args)
        {
            entity.skillManager.BreakCurSkill();
            /*
            if (entity is EntityCreature)
            {
                (entity as EntityCreature).rideManager.Dismount();
            }*/
            return true;
        }

        public bool ChangeGravity(EntityCreature entity, string[] args)
        {
            entity.SetGravity(float.Parse(args[0]));
            return true;
        }

        public bool StopHorizontal(EntityCreature entity, string[] args)
        {
            entity.moveManager.Stop();
            return true;
        }

        public bool StopVertical(EntityCreature entity, string[] args)
        {
            entity.moveManager.StopVertical();
            return true;
        }

        public bool StopMovement(EntityCreature entity, string[] args)
        {
            entity.moveManager.Stop();
            entity.moveManager.StopVertical();
            return true;
        }

        public bool AddShieldAgainst(EntityCreature entity, string[] args)
        {
            ShieldAgainstData shieldAgainstData = entity.skillManager.shieldAgainstData;
            shieldAgainstData.isShieldAgainst = true;
            shieldAgainstData.curHitCount = 0;
            shieldAgainstData.angle = int.Parse(args[0]);
            shieldAgainstData.maxHitCount = int.Parse(args[2]);
            for (int i = 3; i < 6; i++)
            {
                shieldAgainstData.buffIdList.Add(int.Parse(args[i]));
            }
            return true;
        }

        public bool DelShieldAgainst(EntityCreature entity, string[] args)
        {
            entity.skillManager.shieldAgainstData.isShieldAgainst = false;
            entity.skillManager.shieldAgainstData.curHitCount = 0;
            return true;
        }

        public bool SetJumpSpeed(EntityCreature entity, string[] args)
        {
            entity.moveManager.Jump(int.Parse(args[0]));
            return true;
        }

        public bool ChangeMaterial(EntityCreature entity, string[] args)
        {
            int skillID = int.Parse(args[0]);
            int eventID = int.Parse(args[1]);
            ACTEventBase subFX = ACTRunTimeData.GetEventBySkillIDAndEventIdx(skillID, eventID);
            if (entity.actor != null && subFX is ACTEventChangeMaterial)
            {
                entity.actor.materialAnimationController.PlayMaterialAnimation(subFX as ACTEventChangeMaterial);
            }
            return true;
        }

        #endregion

        #region 播放动作
        private bool PlayAction(EntityCreature entity, string[] args)
        {
            if (entity.actor != null)
            {
                entity.actor.animationController.PlayAction(int.Parse(args[0]));
            }
            return true;
        }

        private bool StopAction(EntityCreature entity, string[] args)
        {
            string stateName = args[0];
            if (entity.actor != null && entity.actor.animationController.IsInState(stateName))
            {
                entity.actor.animationController.ReturnReady();
            }
            return true;
        }

        private bool PauseAction(EntityCreature entity, string[] args)
        {
            int frameCount = int.Parse(args[0]);
            if (entity.actor != null)
            {
                entity.actor.animationController.PauseTo(frameCount);
            }
            return true;
        }
        #endregion

        #region 武器

        private bool ShowWeapon(EntityCreature entity, string[] args)
        {
            if (entity.actor != null)
            {
                entity.actor.boneController.SetWeaponSlotVisible(true);
            }
            return true;
        }

        private bool HideWeapon(EntityCreature entity, string[] args)
        {
            if (entity.actor != null)
            {
                entity.actor.boneController.SetWeaponSlotVisible(false);
            }
            return true;
        }

        private bool PackUpWeapon(EntityCreature entity, string[] args)
        {
            if (entity.actor != null)
            {
                int id = int.Parse(args[0]);
                if (id == 0)
                {
                    entity.actor.equipController.PackUpWeapon(false);
                }
                else
                {
                    entity.actor.equipController.PackUpWeapon(true);
                }
            }
            return true;
        }

        #endregion

        #region 怪物shader
        private bool AddShader(EntityCreature entity, string[] args)
        {
            if (entity == null || !(entity is EntityCreature))
            {
                return false;
            }
            EntityCreature entityMonsterBase = entity as EntityCreature;
            //entityMonsterBase.SetModelShader(int.Parse(args[0]));
            return true;
        }

        private bool DelShader(EntityCreature entity, string[] args)
        {
            if (entity == null || !(entity is EntityCreature))
            {
                return false;
            }
            EntityCreature entityMonsterBase = entity as EntityCreature;
            //entityMonsterBase.ResetModelShader();
            return true;
        }
        #endregion

        #region buff相关
        private bool AddBuff(EntityCreature entity, string[] args)
        {
            int buffId = int.Parse(args[0]);
            float duration = float.Parse(args[1]);
            entity.bufferManager.AddBuffer(buffId, duration);
            return true;
        }

        private bool StartBuffEvent(EntityCreature entity, string[] args)
        {
            entity.conditionEventManager.SetState(args, true);
            return true;
        }

        private bool EndBuffEvent(EntityCreature entity, string[] args)
        {
            entity.conditionEventManager.SetState(args);
            return true;
        }
        #endregion

        #region 相机相关
        private bool CameraShake(EntityCreature entity, string[] args)
        {
            var type = int.Parse(args[0]);
            var shakeID = int.Parse(args[1]);
            var duration = float.Parse(args[2]) * 0.001f;
            var count = args.Length >= 4 ? int.Parse(args[3]) : -1;
            if (type == 0)//自己
            {
                if (entity == EntityPlayer.Player)
                {
                    CameraManager.GetInstance().ShakeCamera(shakeID, duration, count);
                }
            }
            else if (type == 1)//所有人
            {
                CameraManager.GetInstance().ShakeCamera(shakeID, duration, count);
            }
            return true;
        }

        private bool CameraStretch(EntityCreature entity, string[] args)
        {
            var offset = float.Parse(args[0]);
            var speed = float.Parse(args[1]);
            var duration = float.Parse(args[2]);
            CameraManager.GetInstance().StretchCamera(offset, speed, duration);
            return true;
        }

        private bool ControlCamera(EntityCreature entity, string[] args)
        {
            if (!entity.IsPlayer()) return true;
            var type = int.Parse(args[0]);
            switch (type)
            {
                case 0:
                    CameraManager.GetInstance().ChangeToLastMotion();
                    break;
                case 1:
                    CameraManager.GetInstance().ChangeToDragSkillMotion(entity.actor.boneController.GetBoneByName("slot_camera"), Vector3.one, 10);
                    break;
                default:
                    break;
            }
            return true;
        }

        static List<uint> targetList = new List<uint>();
        private bool SetLockTarget(EntityCreature entity, string[] args)
        {
            if (!entity.IsPlayer()) return true;
            var type = int.Parse(args[0]);
            switch (type)
            {
                case 1:
                    var radius = float.Parse(args[1]);
                    var height = float.Parse(args[2]);
                    Matrix4x4 srcWorldMatrix = Matrix4x4.identity;
                    srcWorldMatrix.SetTRS(EntityPlayer.Player.pickUpSkillPosition, Quaternion.identity, Vector3.one);
                    EntityFilter.GetEntitiesInCylinderRange(targetList, srcWorldMatrix, radius, height);
                    if (targetList.Contains(entity.target_id)) break;
                    EntityCreature target;
                    uint newTargrtId = 0;
                    float distance = 0;
                    float minDistance = 10000;
                    for (int i = 0; i < targetList.Count; i++)
                    {
                        uint entityID = targetList[i];
                        if (entityID == entity.id) continue;
                        target = MogoWorld.GetEntity(entityID) as EntityCreature;
                        distance = Vector3.Distance(entity.position, target.position);
                        if (distance < minDistance)
                        {
                            minDistance = distance;
                            newTargrtId = entityID;
                        }
                    }
                    if (newTargrtId != 0)
                    {
                        EntityPlayer.Player.target_id = newTargrtId;
                    }
                    break;
                default:
                    break;
            }
            return true;
        }        
        #endregion

        #region 系统提示
        private bool AddNotice(EntityCreature entity, string[] args)
        {
            /*
            if (entity == null || !(entity is PlayerAvatar))
            {
                return false;
            }
            SystemNoticeDisplayData notice = new SystemNoticeDisplayData();
            notice.type = int.Parse(args[0]);
            notice.content = int.Parse(args[1]);
            notice.time = uint.Parse(args[2]);
            notice.priority = int.Parse(args[3]);
            SpaceNoticeActionManager.Instance.AddSystemNotice(notice);*/
            return true;
        }

        private bool DelNotice(EntityCreature entity, string[] args)
        {
            /*
            if (entity == null || !(entity is PlayerAvatar))
            {
                return false;
            }
            int type = int.Parse(args[0]);
            int contentId = int.Parse(args[1]);
            EventDispatcher.TriggerEvent<int, int>(BattleUIEvents.HIDE_SYSTEM_NOTICE_DISPLAY, type, contentId);
            */
            return true;
        }
        #endregion

        #region 慢放效果
        private bool AddTimeScale(EntityCreature entity, string[] args)
        {
            float scale = float.Parse(args[0]);
            int duration = int.Parse(args[1]);
            scale = (scale < 0) ? 0 : scale;
            ACTTimeScaleManager.GetInstance().KeepTimeScale(scale, duration * 0.001f);
            return true;
        }

        private bool DelTimeScale(EntityCreature entity, string[] args)
        {
            ACTTimeScaleManager.GetInstance().CancelTimeScale();
            return true;
        }
        #endregion

        #region 缩放实体
        private bool AddScaleRate(EntityCreature entity, string[] args)
        {
            entity.AddScaleMlp(float.Parse(args[0]));
            return true;
        }

        private bool DelScaleRate(EntityCreature entity, string[] args)
        {
            entity.RemoveScaleMlp(float.Parse(args[0]));
            return true;
        }
        #endregion

        #region UI相关
        private bool ControlUI(EntityCreature entity, string[] args)
        {
            if (entity != EntityPlayer.Player)
            {
                return false;
            }
            bool isOpen = int.Parse(args[0]) == 0;
            int id = int.Parse(args[1]);
            LuaDriver.instance.CallFunction("CSCALL_CONTROL_UI", isOpen, id);
            return true;
        }
        #endregion

        #region 变身
        private bool SetTransformt(EntityCreature entity, string[] args)
        {
            int id = int.Parse(args[0]);
            if (id == 0)
            {
                //结束变身
                int model = role_data_helper.GetModel(entity.vocation);
                entity.SetActor(model);
            }
            else
            {
                //开始变身
                int model = transform_helper.GetModelId(id);
                entity.SetActor(model);
            }
            return true;
        }
        #endregion

        #region 霸体
        private bool SetSuperArmorLevel(EntityCreature entity, string[] args)
        {
            entity.superArmorLevel = int.Parse(args[0]);
            return true;
        }

        private bool AddSuperArmorLevel(EntityCreature entity, string[] args)
        {
            entity.superArmorLevel += int.Parse(args[0]);
            return true;
        }
        #endregion
    }
}
