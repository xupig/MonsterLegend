﻿using System.Collections.Generic;



namespace GameMain.CombatSystem
{
    static public class SkillActionAdjusterServer
    {
        static public int GetSelfMovType(this SkillActionServer skillAction)
        {
            int result = skillAction.skillActionData.selfMovType;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.selfMovType, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<float> GetSelfMovArg(this SkillActionServer skillAction)
        {
            List<float> result = skillAction.skillActionData.selfMovArg;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.selfMovArg, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public Dictionary<int, List<float>> GetTargetMovPreBuff(this SkillActionServer skillAction)
        {
            Dictionary<int, List<float>> result = skillAction.skillActionData.targetMovPreBuff;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.targetMovPreBuff, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetTargetMovType(this SkillActionServer skillAction)
        {
            int result = skillAction.skillActionData.targetMovType;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.targetMovType, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<float> GetTargetMovArg(this SkillActionServer skillAction)
        {
            List<float> result = skillAction.skillActionData.targetMovArg;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.targetMovArg, (AdjustType)adjustData.adjustType);
            }
            return result;
        }


        static public int GetAttackRegionType(this SkillActionServer skillAction)
        {
            int result = skillAction.skillActionData.attackRegionType;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.attackRegionType, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<int> GetAttackRegionArg(this SkillActionServer skillAction)
        {
            List<int> result = skillAction.skillActionData.attackRegionArg;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.attackRegionArg, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetRegionJudgeTime(this SkillActionServer skillAction)
        {
            int result = skillAction.skillActionData.regionJudgeTime;
            for (int i = 0; i < skillAction.adjustSkillActionDataList.Count; i++)
            {
                var adjustData = skillAction.adjustSkillActionDataList[i];
                result = AdjustTools.Adjust(result, adjustData.regionJudgeTime, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetSearchType(this SkillSubjectServer skillSubject)
        {
            int result = skillSubject.skillData.searchType;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.searchType, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<int> GetSearchTargetTypes(this SkillSubjectServer skillSubject)
        {
            List<int> result = skillSubject.skillData.searchTargetType;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.searchTargetType, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetSearchRegionType(this SkillSubjectServer skillSubject)
        {
            int result = skillSubject.skillData.searchRegionType;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.searchRegionType, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<int> GetSearchRegionArg(this SkillSubjectServer skillSubject)
        {
            List<int> result = skillSubject.skillData.searchRegionArg;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.searchRegionArg, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<int> GetOriginAdjust(this SkillSubjectServer skillSubject)
        {
            List<int> result = skillSubject.skillData.originAdjust;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.originAdjust, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public List<int> GetTargetFilterOrders(this SkillSubjectServer skillSubject)
        {
            List<int> result = skillSubject.skillData.targetFilterOrders;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.targetFilterOrders, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public Dictionary<int, List<int>> GetTargetFilterArgs(this SkillSubjectServer skillSubject)
        {
            Dictionary<int, List<int>> result = skillSubject.skillData.targetFilterArgs;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.targetFilterArgs, (AdjustType)adjustData.adjustType);
            }
            return result;
        }

        static public int GetFaceTgt(this SkillSubjectServer skillSubject)
        {
            int result = skillSubject.skillData.faceTgt;
            for (int i = 0; i < skillSubject.adjustSkillDataList.Count; i++)
            {
                var adjustData = skillSubject.adjustSkillDataList[i];
                result = AdjustTools.Adjust(result, adjustData.faceTgt, (AdjustType)adjustData.adjustType);
            }
            return result;
        }
    }
}
