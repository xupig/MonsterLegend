﻿using Common.Structs.ProtoBuf;
using GameLoader.Utils;
using GameMain;
using System;
using System.Collections.Generic;

namespace GameMain.CombatSystem
{
    public class SkillSubjectManagerServerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class SkillSubjectManagerServer
    {
        private EntityCreature _owner;

        private List<SkillSubjectServer> _skillSubjects = new List<SkillSubjectServer>();

        private SkillActionManagerServer _skillActionManager;
        private bool isLooping = false;
        private bool hasInitUpdate = false;

        public SkillSubjectManagerServer() { }
        public void SetOwner(EntityCreature ower)
        {
            _owner = ower;
            if (!hasInitUpdate)
            {
                hasInitUpdate = true;
                MogoEngine.MogoWorld.RegisterUpdate<SkillSubjectManagerServerUpdateDelegate>("SkillSubjectManagerServer.Update", Update);
            }
            if (_skillActionManager == null)
            {
                _skillActionManager = CreatureManagerObjectPool.CreateSkillActionManagerServer(this._owner);
            }
            else
            {
                _skillActionManager.SetOwner(this._owner);
            }
            isLooping = true;
        }

        public void Release()
        {
            isLooping = false;
            //MogoEngine.MogoWorld.UnregisterUpdate("SkillSubjectManagerServer.Update", Update);
            _skillSubjects.Clear();
            _skillActionManager.Release();
        }

        public void AddSkill(int skillID, uint mainTargetID = 0, int slot = 0)
        {
            RemoveSkill(skillID);
            var skillSubject = CombatLogicObjectPool.CreateSkillSubjectServer(skillID, slot);
            skillSubject.mainTargetID = mainTargetID;
            AddSkillSubject(skillSubject);
        }

        public void AddSkillSubject(SkillSubjectServer skillSubject)
        {
            if (skillSubject.GetActionCutTime() > 0)
            {
                /*
                if (_owner is EntityAvatar)
                {
                    (_owner as EntityAvatar).rideManager.Dismount();
                }*/
                BreakCurSkill();
            }
            AdjustTools.AdjustSkill(skillSubject, _owner.skillManager.GetCurAdjustSkillIDList());
            skillSubject.SetOwner(_owner);
            _skillSubjects.Add(skillSubject);
            skillSubject.Update();
        }

        private void RemoveSkill(int skillID)
        {
            for (int i = _skillSubjects.Count - 1; i >= 0; i--)
            {
                var skillSubject = _skillSubjects[i];
                if (skillSubject.skillID == skillID)
                {
                    skillSubject.Stop();
                    _skillSubjects.RemoveAt(i);
                    CombatLogicObjectPool.ReleaseSkillSubjectServer(skillSubject);
                }
            }
        }

        public void BreakCurSkill()
        {
            if (_skillSubjects == null) return;
            for (int i = 0; i < _skillSubjects.Count; i++)
            {
                var skillSubject = _skillSubjects[i];
                skillSubject.Break();
            }
        }

        public void ReleaseSkill(int skillID)
        {
            if (_skillSubjects == null) return;
            for (int i = 0; i < _skillSubjects.Count; i++)
            {
                var skillSubject = _skillSubjects[i];
                skillSubject.Release();
            }
        }

        private void Update()
        {
            if (!isLooping) return;
            UpdateSkillSubjects();
        }

        void UpdateSkillSubjects()
        {
            for (int i = _skillSubjects.Count - 1; i >= 0; i--)
            {
                var skillSubject = _skillSubjects[i];
                skillSubject.Update();
            }
        }

        public List<SkillSubjectServer> GetCurSkillSubjects()
        {
            return _skillSubjects;
        }

        public SkillSubjectServer GetCurSkillSubjectByID(int skillID)
        {
            for (int i = 0; i < _skillSubjects.Count; i++)
            {
                if (_skillSubjects[i].skillID == skillID)
                {
                    return _skillSubjects[i];
                }
            }
            return null;
        }

        //***********服务器回调相关*******************


        public void ActiveSkillActionByServer(PbSpellAction spellActionData)
        {
            int skillID = (int)spellActionData.spell_id;
            int skillActionID = (int)spellActionData.action_id;
            uint targetID = spellActionData.target_id;

            var actionData = CombatLogicObjectPool.GetSkillActionData(skillActionID);
            if (actionData.noJudgeSpellId == 0)
            {
                var skillSubject = GetCurSkillSubjectByID(skillID);
                if (skillSubject == null)
                {
                    LoggerHelper.Debug("Air:skill:ActiveSkillActionByServer skillSubject == null " + this._owner.id + ":" + skillID + ":" + _skillSubjects.Count);
                    return;
                }
                if (skillSubject.skillData == null)
                {
                    LoggerHelper.Debug("Air:skill:ActiveSkillActionByServer skillSubject.skillData == null " + this._owner.id + ":" + skillID);
                    return;
                }
                var faceLockMode = (FaceLockMode)skillSubject.GetFaceLockMode();
                if (faceLockMode == FaceLockMode.LockDuringSkill)
                {
                    var entity = CombatSystemTools.GetCreatureByID(targetID);
                    if (entity != null && _owner.actor != null)
                    {
                        _owner.actor.FaceTo(entity.actor);
                    }
                }
            }

            var skillAction = CombatLogicObjectPool.CreateSkillActionServer(skillActionID);
            skillAction.SetOwner(_owner);
            skillAction.skillID = skillID;
            skillAction.mainTargetID = targetID;
            skillAction.pbSpellAction = spellActionData;
            _skillActionManager.AddSkillAction(skillAction);
        }
        
        public void JudgeSkillActionByServer(UInt16 skillID, UInt16 skillActionID, PbSpellDamageInfo skillDamageInfo)
        {
            _skillActionManager.JudgeSkillActionByServer(skillID, skillActionID, skillDamageInfo);
        }

        public void OnSetActionOriginByServer(PbSpellActionOrigin spellActionOrigin)
        {
            _skillActionManager.OnSetActionOriginByServer(spellActionOrigin);
        }   
    }
}
