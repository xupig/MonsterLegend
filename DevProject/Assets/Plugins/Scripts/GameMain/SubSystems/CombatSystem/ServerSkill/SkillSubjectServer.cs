﻿using Common.Global;
using Common.States;
using GameData;
using GameLoader.Utils;
using GameMain;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.CombatSystem
{
    public class SkillSubjectServer
    {
        public int skillID;

        EntityCreature _owner;
        ACTHandler _actHandler;

        uint _mainTargetID;
        public uint mainTargetID
        { 
            get { return _mainTargetID; }
            set { _mainTargetID = value; }
        }

        uint _lockTargetID;

        float _alwaysFaceTargetEndTime = 0;

        SkillState _curState = SkillState.STANDBY;
        public SkillState curState
        {
            get { return _curState; }
        }

        bool _isBreaked = false;

        SkillData _skillData;
        public SkillData skillData { get { return _skillData; } }

        List<SkillData> _adjustSkillDataList = new List<SkillData>();
        public List<SkillData> adjustSkillDataList { get { return _adjustSkillDataList; } }

        List<SkillEventData> _skillEventList = new List<SkillEventData>();
        List<SkillBuffer> _skillBufferList = new List<SkillBuffer>();

        float _duration = 1;
        public float duration { get { return _duration; } }
        float _startTimeStamp = 0;
        float _pastTime = 0;
        bool _checkCut = false;
        bool _isStorageRequire = false;
        bool _isSingingRequire = false;
        bool _isStickRequire = false;
        //技能槽位
        public int slot = 0;

        List<uint> _oldTargetIDList = new List<uint>();

        public SkillSubjectServer()
        {
            _actHandler = new ACTHandler();
        }

        public void Reset(SkillData data)
        {
            _curState = SkillState.STANDBY;
            _pastTime = 0;
            _isBreaked = false;
            _checkCut = false;
            _skillData = data;
            _mainTargetID = 0;
            _alwaysFaceTargetEndTime = this.GetFaceAlways() * 0.001f + RealTime.time;
            _duration = this.GetActionCutTime() * 0.001f;
            _oldTargetIDList.Clear();

            skillID = _skillData.skillID;
            ClearAdjustSkillData();
            _isStorageRequire = false;
            _isSingingRequire = false;
            _isStickRequire = false;
            if (slot == 0)
            {
                slot = data.pos;
            }
        }

        public void ClearAdjustSkillData()
        {
            _adjustSkillDataList.Clear();
        }

        public void AddAdjustSkillData(SkillData skillData)
        {
            if (_adjustSkillDataList.Contains(skillData)) return;
            _adjustSkillDataList.Add(skillData);
        }

        public List<SkillEventData> GetSkillEventList()
        {
            return _skillEventList;
        }

        void ResetSkillEventList()
        {
            _skillEventList.Clear();
            var skillEvents = this.GetSkillEvents();
            for (int i = 0; i < skillEvents.Count; i++)
            {
                _skillEventList.Add(skillEvents[i]);
            }
        }

        void ResetSkillBufferList()
        {
            _skillBufferList.Clear();
            var addBuffsOnCast = this.GetAddBuffsOnCast();
            if (addBuffsOnCast == null) return;
            foreach (var kv in addBuffsOnCast)
            {
                var skillBuffer = new SkillBuffer();
                skillBuffer.bufferID = kv.Key;
                skillBuffer.delay = kv.Value[0] * 0.001f;
                skillBuffer.duration = kv.Value[1];
                _skillBufferList.Add(skillBuffer);
            }
        }

        public void SetOwner(EntityCreature owner)
        {
            _owner = owner;
            _actHandler.qualitySettingValue = _owner.qualitySettingValue;
            _actHandler.priority = _owner.visualFXPriority;
        }

        public SkillState Update()
        {
            if (_curState == SkillState.STANDBY)
            {
                Begin();
            }
            else if (_curState == SkillState.ACTIVE)
            {
                _pastTime = RealTime.time - _startTimeStamp;
                if (_isBreaked || _pastTime >= _duration)
                {
                    End();
                }
                else
                {
                    Doing();
                }
            }
            CheckCut();
            return _curState;
        }

        public void Break()
        {
            if (!_isBreaked)
            {
                if (_curState != SkillState.ENDED)
                {
                    OnBreak();
                }
                _actHandler.BreakAllVisualFX();
            }
            _isBreaked = true;
        }

        //停止蓄力
        public void Release()
        {
            if (_isBreaked) return;
            if (_isStorageRequire || _isStickRequire)
            {
                Break();
            }
            if (_isStorageRequire && _owner.IsPlayer())
            {
                BreakSpellReq();
            }
        }

        public void Stop()
        {
			End();
			_owner = null;
            _skillData = null;
        }

        void Begin()
        {
            _curState = SkillState.ACTIVE;
            _startTimeStamp = RealTime.time;
            ChangeMoveMode();
            ResetSkillEventList();
            FindSkillForSkillBegin();
            if (_owner.IsPlayer())
            {
                OnBeginByPlayer();
                LuaDriver.instance.CallFunction("CSCALL_REFRESH_LAST_CAST_SPELL", skillID);
            }
            else if (_owner.isClientControlServerEntity)
            {
                OnBeginByFollower();
            }
            ShowPickUpFx();
            BeginSinging();
            Doing();
        }

        void BeginSinging()
        {
            int isStorage = this.GetIsStorageRequire();
            if (isStorage == 1)
            {
                this._isStorageRequire = true;
            }
            if (this._isStorageRequire || this._isSingingRequire)
            {
                //开始吟唱
                if (this._owner == EntityPlayer.Player)
                {
                    //LuaDriver.instance.CallFunction("CSCALL_PLAYER_SINGING", 0, 1, this._duration);
                }
            }
        }

        void Doing()
        {
            DoingBuffers();
            DoingEvents();
            FacingTarget();
        }

        void End()
        {
            if (_curState == SkillState.ENDED) return;
            _curState = SkillState.ENDED;
            EndSinging();
            if (!_isBreaked)
            {
                OnBreak();
                CheckCut();
            }
            _actHandler.Clear();
            //蓄力结束后自动触发同一个位置上的技能。
            if ((_isStickRequire||_isStorageRequire) && _owner.IsPlayer())
            {
                _owner.skillManager.PlaySkillByPos(this.slot);
            }
        }

        void EndSinging()
        {
            if (this._isStorageRequire || this._isSingingRequire)
            {
                //结束吟唱
                if (this._owner == EntityPlayer.Player)
                {
                    //LuaDriver.instance.CallFunction("CSCALL_PLAYER_SINGING", 0, 1, 0);
                }
            }
            if (this._isStickRequire && _owner.IsPlayer())
            {
                LuaDriver.instance.CallFunction("CSCALL_CONTROL_UI", false, 1);
                PickUpSkillControl.ClosePickUpRange();
            }
        }

        void OnBreak()
        {
            DelBuffsOnBreakOrEnd();
            _actHandler.ResetDefaultState(this._owner.actor);
            _skillBufferList.Clear();
            _skillEventList.Clear();
        }

        void DoingEvents()
        {
            for (int i = _skillEventList.Count - 1; i >= 0; i--)
            {
                var eventData = _skillEventList[i];
                if (eventData.delay < 0)
                {
                    eventData.delay = ACTSkillEventHandler.GetEventDelay(eventData.mainID, eventData.eventIdx);
                }
                if (eventData.delay <= _pastTime)
                {
                    _actHandler.OnEvent(_owner.actor, eventData);
                    _skillEventList.RemoveAt(i);
                }
            }
        }

        void DoingBuffers()
        {
            for (int i = _skillBufferList.Count - 1; i >= 0; i--)
            {
                var bufferData = _skillBufferList[i];
                if (bufferData.delay <= _pastTime)
                {
                    _owner.bufferManager.AddBuffer(bufferData.bufferID, bufferData.duration, true, false, false);
                    _skillBufferList.RemoveAt(i);
                }
            }
        }

        void FacingTarget()
        {
            if (_alwaysFaceTargetEndTime > RealTime.time)
            {
                if (this.IsFaceTargetWithVertical())
                {
                    LockDirection(true);
                }
                else
                {
                    LockDirection(false);
                }
            }
        }

        bool IsFaceTargetWithVertical()
        {
            return this.GetFaceTgt() == 1;
        }

        void CheckCut()
        {
            if (_isBreaked) return;
            if (_checkCut) return;
            if (_owner.actor == null) return;
            if (_pastTime >= _duration && InMovingState())
            {
                _checkCut = true;
                if (!_owner.IsOnAir())
                {
                    _owner.actor.animationController.ReturnReady();
                }                
            }
        }

        bool InMovingState()
        {
            if (_owner.IsPlayer())
            {
                //return ControlStickState.strength > 0 || _owner.actor.actorState.forwardMovingSpeedState != 0 || _owner.actor.actorState.rightMovingSpeedState != 0;
                return ControlStickState.strength > 0 || _owner.actor.animationController.GetSpeed() != 0;
            }
            return _owner.actor.actorState.forwardMovingSpeedState != 0 || _owner.actor.actorState.rightMovingSpeedState != 0;
        }

        void FindSkillForSkillBegin()
        {
            FindSkillTarget();

            _lockTargetID = this._mainTargetID;
            LockDirection(false);
            return;

            var faceLockMode = (FaceLockMode)this.GetFaceLockMode();
            if (faceLockMode == FaceLockMode.LockOnSkillActive
                || faceLockMode == FaceLockMode.LockDuringSkill)
            {
                _lockTargetID = this._mainTargetID;
                LockDirection(this.IsFaceTargetWithVertical());
            }
        }

        void OnBeginByPlayer()
        {
            if (PlatformHelper.InPCPlatform())
            {
                FaceAccordingMouse();
            }
            else
            {
                FaceAccordingStick();
            }
            SetCommonCD();
            ResetSkillBufferList();
            CastSpellReq();
            RemoveConditionBuff();
        }

        void OnBeginByFollower()
        {
            SetCommonCD();
            ResetSkillBufferList();
            FollowerCastSpellReq();
            RemoveConditionBuff();
        }

        void ChangeMoveMode()
        {
            if (this._owner.IsPlayer())
            {
                this._owner.moveManager.MoveBySkill(_duration);
            }
        }

        void FaceAccordingStick()
        {
            if (this.GetAccordingStick() == 0) return;
            if (ControlStickState.strength <= 0) return;
            Vector3 moveDirection = ControlStickState.GetMoveDirectionByControlStick();
            _owner.actor.forward = moveDirection;
        }

        void FaceAccordingMouse()
        {
            var mousePos = ControlStickState.GetMoveDirectionByMouse();
            (EntityPlayer.Player as EntityCreature).actor.FaceTo(mousePos);    
        }

        void SetCommonCD()
        {
            var cd = this.GetCD();
            _owner.skillManager.SetCommonCD(skillID, cd[1]);
        }

        static List<uint> _filterList = new List<uint>();
        static List<uint> _targetFilterList = new List<uint>();
        static List<uint> _candidateList = new List<uint>();
        void FindSkillTarget(bool isUnique = false)
        {
            SearchMainTargetType searchMainType = (SearchMainTargetType)this.GetSearchType();
            if (searchMainType == SearchMainTargetType.CurLockTarget)
            {
                mainTargetID = this._owner.target_id;
                return;
            }
            var targetType = this.GetSearchTargetTypes();
            TargetRangeType targetRangeType = (TargetRangeType)this.GetSearchRegionType();
            List<int> targetRangeParam = this.GetSearchRegionArg();
            List<int> originAdjust = this.GetOriginAdjust();
            var matrix = _owner.GetTransform().localToWorldMatrix;
            EntityFilter.GetEntitiesByRangeType(_filterList, matrix, targetRangeType, targetRangeParam, originAdjust, EntityFilterType.SKILL_SUBJECT_FILTER);
            if (_filterList.Contains(this._owner.id)) _filterList.Remove(this._owner.id);
            if (isUnique)
            {
                mainTargetID = 0;
                for (int i = _filterList.Count - 1; i >= 0; i--)
                {
                    if (_oldTargetIDList.Contains(_filterList[i]))
                    {
                        _filterList.RemoveAt(i);
                    }
                }
            }
            List<int> targetFilterOrders = this.GetTargetFilterOrders();
            Dictionary<int, List<int>> targetFilterArgs = this.GetTargetFilterArgs();
            TargetFilter.GetCanSearchCreaturesByTargetType(_targetFilterList, _owner, _filterList, targetType);
            TargetFilter.FilterTarget(_candidateList, _owner, _targetFilterList, targetFilterOrders, targetFilterArgs, 1, 1);
            if (_candidateList.Count > 0)
            {
                mainTargetID = _candidateList[0];
                if (isUnique)
                {
                    _oldTargetIDList.Add(mainTargetID);
                }
            }
        }

        void RemoveConditionBuff()
        {
            if (this._skillData.showType > 0)
            {
                _owner.bufferManager.RemoveBuffer(this._skillData.showType);
            }
        }

        void DelBuffsOnBreakOrEnd()
        {
            var delBuffsOnBreak = this.GetDelBuffsOnBreak();
            if (delBuffsOnBreak != null)
            {
                for (int i = 0; i < delBuffsOnBreak.Count; i++)
                {
                    _owner.bufferManager.RemoveBuffer(delBuffsOnBreak[i]);
                }
            }
        }

        void ShowPickUpFx()
        {
            if (!(this._owner is EntityPlayer))
            {
                return;
            }
            int type = this._skillData.specialSearchType;
            bool isShow = type != 0 && type != 99;
            this._isStickRequire = isShow;
            if (!isShow) return;
            List<int> specialSearchArg = this.GetSpecialSearchArg();
            Matrix4x4 judgeMatrix = GetRegionJudgeMatrix();
            PickUpSkillControl.DrawPickUpByRangeType(judgeMatrix, type, specialSearchArg);
        }

        Matrix4x4 GetRegionJudgeMatrix()
        {
            return _owner.GetTransform().localToWorldMatrix;
        }

        void LockDirection(bool includeVertical)
        {
            var entity = CombatSystemTools.GetCreatureByID(_lockTargetID);
            if (entity != null && _owner.actor != null)
            {
                _owner.actor.FaceTo(entity.actor,includeVertical);
            }
        }

        #region player相关操作
        void CastSpellReq()
        {
            var position = this._owner.position;
            int playerPosX = (int)(position.x * 100);
            int playerPosY = (int)(position.y * 100);
            int playerPosZ = (int)(position.z * 100);
            var face = this._owner.face;
            byte faceX = (byte)(face.x);
            byte faceY = (byte)(face.y);
            byte faceZ = (byte)(face.z);
            if (wings_value_helper.IsWingsValueId(skillID))
            {
                EntityPlayer.Player.RpcCall("fly_action_req", skillID, playerPosX, playerPosY, playerPosZ, faceX, faceY, faceZ, (UInt32)1);
                return;
            }
            //检测守护灵技能特殊处理
            if (EntityPlayer.Player.eudemonSkillId == skillID)
            {
                EntityPlayer.Player.RpcCall("eudemon_spell_req", skillID, playerPosX, playerPosY, playerPosZ, faceX, faceY, faceZ, (UInt32)1);
                return;
            }
            //EntityPlayer.Player.RpcCall("cast_spell_req", skillID, playerPosX, playerPosY, playerPosZ, faceX, faceY, faceZ, (UInt32)1);//Common.Global.Global.localTimeStamp);
            EntityPlayer.Player.RpcCall("cast_spell_to_req", skillID, playerPosX, playerPosY, playerPosZ, faceX, faceY, faceZ, (UInt32)1, EntityPlayer.Player.target_id);
        }

        public void CastSpellResp(uint mainTargetID)
        {
            _mainTargetID = mainTargetID;
            FindSkillForSkillBegin();
        }
        #endregion

        #region follower相关操作
        void FollowerCastSpellReq()
        {
            var position = this._owner.position;
            int playerPosX = (int)(position.x * 100);
            int playerPosY = (int)(position.y * 100);
            int playerPosZ = (int)(position.z * 100);
            var face = this._owner.face;
            byte faceX = (byte)(face.x);
            byte faceY = (byte)(face.y);
            byte faceZ = (byte)(face.z);
            //EntityPlayer.Player.RpcCall("other_cast_spell_req", _owner.id, skillID, playerPosX, playerPosY, playerPosZ, faceX, faceY, faceZ, Global.localTimeStamp);
            EntityPlayer.Player.RpcCall("other_cast_spell_to_req", _owner.id, skillID, playerPosX, playerPosY, playerPosZ, faceX, faceY, faceZ, Global.localTimeStamp, _owner.target_id);
        }

        public void FollowerCastSpellResp(uint mainTargetID)
        {
            _mainTargetID = mainTargetID;
            FindSkillForSkillBegin();
        }
        #endregion

        void BreakSpellReq()
        {
            EntityPlayer.Player.RpcCall("break_spell_req", this.skillID);
        }
    }


}
