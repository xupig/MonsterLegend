﻿using Common.ClientConfig;
using Common.ServerConfig;
using GameData;
using GameMain;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using ShaderUtils;
using UnityEngine;

namespace GameMain.CombatSystem
{
    public static class CharacterAction
    {
        public const int ACTIVE_MOVE_ABLE = 0;
        public const int MOVE_ABLE = 1;
        public const int ATTACK_ABLE = 2;
        public const int HIT_ABLE = 3;
        public const int TURN_ABLE = 4;
        public const int VISIBLE = 5;
        public const int SHOW = 6;
        public const int THINK_ABLE = 7;
        public const int COLLIDE_ABLE = 8;
        public const int TRANSMIT_ABLE = 9;
        public const int AUTO_MOUNT_ABLE = 10;
        public const int FACE_TO_DEFAULT_ABLE = 11;
        public const int SEARCH_ABLE = 12;
        public const int RECOVER_ABLE = 13;
        public const int ACTION_IDX_LEN = 14;
    }

    public class StateManager
    {
        private static readonly UInt32 ONE = 1;

        private EntityCreature _owner;
        private delegate void StateHandleFun(EntityCreature entity);
        private static StateHandleFun[][] _stateFuncMap;
        private static StateHandleFun[][] stateFuncMap
        {
            get 
            {
                if (_stateFuncMap == null)
                {
                    _stateFuncMap = new StateHandleFun[state_config.AVATAR_STATE_SPELL_MAX][];
                    _stateFuncMap[state_config.AVATAR_STATE_DEATH] = new StateHandleFun[] { OnEnterDeath, OnLeaveDeath };
                    _stateFuncMap[state_config.AVATAR_STATE_DEAD_GHOST] = new StateHandleFun[] { OnEnterGhost, OnLeaveGhost };
                    _stateFuncMap[state_config.AVATAR_STATE_NOT_FACE_TO_DEFAULT] = new StateHandleFun[] { OnEnterNotFaceDefault, OnLeaveNotFaceDefault };
                    _stateFuncMap[state_config.AVATAR_STATE_TRANSFORM] = new StateHandleFun[] { OnEnterTransform, OnLeaveTransform };
                    _stateFuncMap[state_config.AVATAR_STATE_RIDE] = new StateHandleFun[] { OnEnterRide, OnLeaveRide };
                }
                return _stateFuncMap; 
            }
        }

        private int[] _characterActionStateMap;
        public int[] characterActionStateMap
        {
            get { return _characterActionStateMap; }
        }
        private UInt32 _currentState = 0;
        public UInt32 currentState
        {
            get { return _currentState; }
        }

        Dictionary<int, int> _stateAccumulationMap = new Dictionary<int, int>();

        public StateManager() { }
        public void SetOwner(EntityCreature owner)
        {
            RemoveListeners();
            _owner = owner;
            InitCharacterActionState();
            EntityPropertyManager.Instance.AddEventListener(_owner, EntityPropertyDefine.state_cell, this.OnStateChange);
        }

        public void Release()
        {
            RemoveListeners();
            ClearData();
        }

        private void RemoveListeners()
        {
            if (_owner == null) return;
            EntityPropertyManager.Instance.RemoveEventListener(_owner, EntityPropertyDefine.state_cell, this.OnStateChange);
        }

        private void ClearData()
        {
            for (int i = 0; i < _characterActionStateMap.Length; i++)
            {
                _characterActionStateMap[i] = 0;
            }
            _currentState = 0;
            _stateAccumulationMap.Clear();
        }

        public void InitCharacterActionState()
        {
            _characterActionStateMap = new int[CharacterAction.ACTION_IDX_LEN];
            _characterActionStateMap[CharacterAction.ACTIVE_MOVE_ABLE] = 0;
            _characterActionStateMap[CharacterAction.MOVE_ABLE] = 0;
            _characterActionStateMap[CharacterAction.ATTACK_ABLE] = 0;
            _characterActionStateMap[CharacterAction.HIT_ABLE] = 0;
            _characterActionStateMap[CharacterAction.TURN_ABLE] = 0;
            _characterActionStateMap[CharacterAction.VISIBLE] = 0;
            _characterActionStateMap[CharacterAction.SHOW] = 0;
            _characterActionStateMap[CharacterAction.THINK_ABLE] = 0;
            _characterActionStateMap[CharacterAction.COLLIDE_ABLE] = 0;
            _characterActionStateMap[CharacterAction.TRANSMIT_ABLE] = 0;
            _characterActionStateMap[CharacterAction.AUTO_MOUNT_ABLE] = 0;
            _characterActionStateMap[CharacterAction.FACE_TO_DEFAULT_ABLE] = 0;
            _characterActionStateMap[CharacterAction.SEARCH_ABLE] = 0;
            _characterActionStateMap[CharacterAction.RECOVER_ABLE] = 0;
        }

        public void OnStateChange()
        {
            if (MogoWorld.IsClientEntity(_owner)) return;
            UInt32 newState = _owner.state_cell;
            for (int pos = 0; pos < state_config.AVATAR_STATE_SPELL_MAX - 1; pos++)
            {
                UInt32 cursor = ONE << pos;
                bool newSubState = (newState & cursor) != 0;
                SetState(pos, newSubState, false);
            }
            _currentState = newState;
            RefreshCharacterActionState();
        }

        public void SetState(int pos, bool newSubState, bool refresh = true)
        {
            UInt32 cursor = ONE << pos;
            bool oldSubState = (currentState & cursor) != 0;
            if (newSubState)
            {
                _currentState |= cursor;
            }
            else
            {
                _currentState &= ~cursor;
            }
            if (oldSubState != newSubState)
            {
                CallStateChangeHandler(pos, newSubState);
            }
            if (refresh) RefreshCharacterActionState();
        }

        public void AccumulateState(int pos, int value)
        {
            if (!_stateAccumulationMap.ContainsKey(pos))
            {
                _stateAccumulationMap[pos] = 0;
            }
            _stateAccumulationMap[pos] += value;
            if (_stateAccumulationMap[pos] < 0)//这种情况不应该发生的，做容错处理，以免影响后续状态
            {
                _stateAccumulationMap[pos] = 0;
            }
            if (_stateAccumulationMap[pos] > 0)
            {
                SetState(pos, true);
            }
            else
            {
                SetState(pos, false);
            }
        }

        public void RefreshCharacterActionState()
        {
            int __active_move_able = 0;
            int __move_able = 0;
            int __attack_able = 0;
            int __hit_able = 0;
            int __turn_able = 0;
            int __visible = 0;
            int __show = 0;
            int __think_able = 0;
            int __collide_able = 0;
            int __transmit_able = 0;
            int __auto_mount_able = 0;
            int __face_to_default_able = 0;
            int __search_able = 0;
            int __recover_able = 0;
            for (int pos = 0; pos < state_config.AVATAR_STATE_SPELL_MAX; pos++)
            {
                var data = spell_state_helper.GetSpellStateData(pos);
                if (data == null) continue;
                UInt32 cursor = ONE << pos;
                int coefficient = (currentState & cursor) != 0 ? 1 : 0;
                __active_move_able |= coefficient * data.__active_move_able;
                __move_able |= coefficient * data.__move_able;
                __attack_able |= coefficient * data.__attack_able;
                __hit_able |= coefficient * data.__hit_able;
                __turn_able |= coefficient * data.__turn_able;
                __visible |= coefficient * data.__visible;
                __show |= coefficient * data.__show;
                __think_able |= coefficient * data.__think_able;
                __collide_able |= coefficient * data.__collide_able;
                __transmit_able |= coefficient * data.__transmit_able;
                __auto_mount_able |= coefficient * data.__auto_mount_able;
                __face_to_default_able |= coefficient * data.__face_default_able;
                __search_able |= coefficient * data.__search_able;
                __recover_able |= coefficient * data.__recover_able;
            }
            _characterActionStateMap[CharacterAction.ACTIVE_MOVE_ABLE] = __active_move_able;
            _characterActionStateMap[CharacterAction.MOVE_ABLE] = __move_able;
            _characterActionStateMap[CharacterAction.ATTACK_ABLE] = __attack_able;
            _characterActionStateMap[CharacterAction.HIT_ABLE] = __hit_able;
            _characterActionStateMap[CharacterAction.TURN_ABLE] = __turn_able;
            _characterActionStateMap[CharacterAction.VISIBLE] = __visible;
            _characterActionStateMap[CharacterAction.SHOW] = __show;
            _characterActionStateMap[CharacterAction.THINK_ABLE] = __think_able;
            _characterActionStateMap[CharacterAction.COLLIDE_ABLE] = __collide_able;
            _characterActionStateMap[CharacterAction.TRANSMIT_ABLE] = __transmit_able;
            _characterActionStateMap[CharacterAction.AUTO_MOUNT_ABLE] = __auto_mount_able;
            _characterActionStateMap[CharacterAction.FACE_TO_DEFAULT_ABLE] = __face_to_default_able;
            _characterActionStateMap[CharacterAction.SEARCH_ABLE] = __search_able;
            _characterActionStateMap[CharacterAction.RECOVER_ABLE] = __recover_able;
            OnCharacterActionStateChange();
        }

        public void CallStateChangeHandler(int pos, bool on)
        {
            if (stateFuncMap[pos] != null)
            {
                if (on)
                {
                    stateFuncMap[pos][0](_owner);
                }
                else
                {
                    stateFuncMap[pos][1](_owner);
                }
            }
        }

        public bool InState(int state)
        {
            return (currentState & (ONE << state)) > 0;
        }

        public bool CanDO(int action)
        {
            return _characterActionStateMap[action] == 0;
        }

        #region 子状态处理

        void OnCharacterActionStateChange()
        {
            this._owner.collidable = CanDO(CharacterAction.COLLIDE_ABLE);
            this._owner.visible = CanDO(CharacterAction.VISIBLE);
            this._owner.show = CanDO(CharacterAction.SHOW);
        }

        #endregion

        #region 状态处理

        static void OnEnterNotFaceDefault(EntityCreature owner)
        {
            if (owner.actor == null) return;
            owner.actor.actorController.SetUpdateFaceEnabled(false);
        }

        static void OnLeaveNotFaceDefault(EntityCreature owner)
        {
            if (owner.actor == null) return;
            owner.actor.actorController.SetUpdateFaceEnabled(true);
        }

        static void OnEnterDeath(EntityCreature owner)
        {
            owner.moveManager.Stop();
            owner.OnDeath();
            owner.skillManager.ClearAllCD();
        }

        static void OnLeaveDeath(EntityCreature owner)
        {
            if (owner.actor == null) return;
            owner.actor.animationController.Relive();
            owner.OnRelive();
        }

        static void OnEnterGhost(EntityCreature owner)
        {
            owner.OnEnterGhost();
        }

        static void OnLeaveGhost(EntityCreature owner)
        {
            owner.OnLeaveGhost();
        }

        static void OnEnterTransform(EntityCreature owner)
        {
            owner.SetRide(0);
            owner.OnEnterTransform();
        }

        static void OnLeaveTransform(EntityCreature owner)
        {
            owner.OnLeaveTransform();
            //结束变身
            //int model = role_data_helper.GetModel(_owner.vocation);
            //_owner.SetActor(model);
        }

        static void OnEnterRide(EntityCreature owner)
        {

        }

        static void OnLeaveRide(EntityCreature owner)
        {

        }

        #endregion 状态处理


    }
}
