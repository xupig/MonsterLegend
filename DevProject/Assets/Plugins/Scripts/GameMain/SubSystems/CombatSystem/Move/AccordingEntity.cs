﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameMain;

using ACTSystem;
using MogoEngine;

namespace GameMain.CombatSystem
{
    public class AccordingEntity : AccordingStateBase
    {
        private float _maxAccordingDistance = 10f;

        private Vector3 _targetPosition;
        private Vector3 _moveDirection;
        private float _targetDistance;
        private float _curMoveSpeed;
        private bool _right = false;

        public AccordingEntity(EntityCreature owner)
            :base(owner)
        {
            modeType = AccordingMode.AccordingEntity;
        }

        public override void OnEnter()
        {
            
        }

        public override bool Update()
        {
            var actor = movingActor;
            if (actor != null)
            {
                CalculateValues();
                if (!CheckMoveAble()) return false;
                if (!CheckDistance()) return false;
                FaceTo();
                Move();
            }
            return true;
        }

        bool CheckMoveAble()
        {
            if (!_owner.CanActiveMove())
            {
                movingActor.actorController.Stop();
                return false;
            }
            return true;
        }

        void CalculateValues()
        {
            _targetPosition = _owner.position;
            _moveDirection = _targetPosition - movingActor.position;
            if (!movingActor.isFly)
            {
                _moveDirection.y = 0;
            }
            _targetDistance = _moveDirection.magnitude;
            _curMoveSpeed = _owner.moveManager.curMoveSpeed * 0.96f; //故意让客户端比服务器略慢，减少卡顿感.
        }

        bool CheckDistance()
        {
            if (_targetDistance > _maxAccordingDistance)
            {
                movingActor.position = _targetPosition;
                return false;
            }
            if (_targetDistance < 0.5f || _targetDistance <= (_curMoveSpeed * UnityPropUtils.deltaTime * 2))
            {
                movingActor.actorController.Stop();
                if (movingActor.isFly)
                {
                    movingActor.actorController.StopVertical();
                }
                return false;
            }
            return true;
        }

        void FaceTo()
        {
            bool alreadyFaceTarget = false;
            _right = false;
            if (_owner.target_id > 0)
            {
                var actor = movingActor;
                var target = MogoWorld.GetEntity(_owner.target_id);
                if (target != null)
                {
                    var forward = target.position - actor.position;
                    _owner.actor.forward = forward;
                    _right = true;
                    var angle = ACTSystem.ACTMathUtils.Abs(Vector3.Angle(forward, _moveDirection.normalized));
                    _right = (angle > 45 && angle < 135) ? true : false;
                    alreadyFaceTarget = true;
                }
            }
            if (!alreadyFaceTarget)
            {
                if (_owner.CanTurn())
                {
                    movingActor.FaceTo(_targetPosition);
                }
            }
        }

        void Move()
        {
            Vector3 movement = _moveDirection.normalized * _curMoveSpeed;
            movingActor.actorController.Move(movement, _right);
            if (movingActor.isFly)
            {
                movingActor.actorController.verticalSpeed = movement.y;
            }
        }


        public override void OnLeave()
        {
            
        }
    }
}
