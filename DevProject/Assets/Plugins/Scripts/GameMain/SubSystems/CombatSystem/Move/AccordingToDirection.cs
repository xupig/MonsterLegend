﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameMain;

using ACTSystem;
using MogoEngine;

namespace GameMain.CombatSystem
{
    public class AccordingToDirection : AccordingStateBase
    {
        private float _moveBaseSpeed = 0f;

        private float _moveTime = 0;

        private Vector3 _direction = Vector3.zero;

        public AccordingToDirection(EntityCreature owner)
            : base(owner)
        {
            modeType = AccordingMode.AccordingToDirection;
        }

        public bool Reset(Vector3 targetPosition, float speed, float moveTime)
        {
            if (movingActor == null) return false;
            if (speed == 0) return false;
            var actorPos = movingActor.position;
            if (speed > 0)
            {
                _direction = targetPosition - actorPos;
            }
            else {
                _direction = actorPos - targetPosition;
            }
            _owner.FaceToPosition(_direction * 100);
            _moveBaseSpeed = ACTSystem.ACTMathUtils.Abs(speed);
            _moveTime = moveTime;
            return true;
        }

        public override bool Update()
        {
            //服务器应该遵循的规则：
            //    当技能行为带位移：同步行为的同时同步“最终”目标地点信息，服务器首先将玩家位置移动到该位置
            //    行为位移结束，自动恢复默认同步方式。
            //问题：坐标有玩家自身同步到服务器，技能相关位移又该如何选择？
            var actor = movingActor;
            if (actor == null) return false;
            if (!_owner.CanActiveMove())
            {
                actor.actorController.Stop();
                return false;
            }
            if (_moveTime <= 0)
            {
                actor.actorController.Stop();
                _owner.position = actor.position;
                return false;
            }
            _moveTime = _moveTime - UnityPropUtils.deltaTime;
            Vector3 movement = _direction.normalized * _moveBaseSpeed;
            movement.y = 0;
            actor.forward = _direction;
            actor.actorController.Move(movement);
            _owner.position = actor.position;
            return true;
        }

        public override void OnLeave()
        {
             
        }
    }
}
