﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameMain;

using ACTSystem;
using Common.States;
using MogoEngine.Events;

using GameData;

using GameLoader.Utils;

namespace GameMain.CombatSystem
{
    //player专用，常规状态下根据摇杆状态移动
    public class AccordingGlide : AccordingStateBase
    {
        public AccordingGlide(EntityCreature owner)
            : base(owner)
        {
            modeType = AccordingMode.AccordingGlide;
        }

        public override void OnEnter()
        {

        }

        private GameObject fly = null;
        public override bool Update()
        {
            //return UpdatePlanA();
            return UpdatePlanC();
        }

        private bool UpdatePlanC()
        {
            var actor = movingActor;
            if (actor == null) return false;
            _owner.position = actor.position;
            if (ControlStickState.strength < 0) return false;
            CheckingStick();
            while (true)
            {
                actor.actorController.verticalSpeed = global_params_helper.glideSpeedV;
                Vector3 moveDirection = ControlStickState.GetMoveDirectionByControlStick();
                moveDirection.y = 0;
                if (_owner.CanTurn())
                {
                    float step = global_params_helper.glideAngleSpeed * 3.14159f / 180 * UnityPropUtils.deltaTime;
                    _owner.actor.forward = Vector3.RotateTowards(_owner.actor.forward, moveDirection.normalized, step, 0).normalized;
                }
                if (_owner.CanActiveMove())
                {
                    var curMoveSpeed = global_params_helper.glideSpeedH;
                    Vector3 movement = _owner.actor.forward * curMoveSpeed;
                    actor.animationController.SetApplyRootMotion(false);                    
                    actor.actorController.Move(movement);
                    TryClearPlayerAction();
                }
                else
                {
                    actor.actorController.Stop();
                }
                break;
            }
            return true;
        }

        private bool UpdatePlanB()
        {
            var actor = movingActor;
            if (actor == null) return false;
            _owner.position = actor.position;
            if (ControlStickState.strength < 0) return false;
            CheckingStick();
            bool sprint = false;
            while (true)
            {
                Vector3 moveDirection = Vector3.one * (ControlStickState.direction.x / ControlStickState.direction.magnitude);
                if (ControlStickState.strength == 0)
                {
                    moveDirection = Vector3.forward;
                }
                if (_owner.CanTurn())
                {//要在ACTActorController里改才能生效，不然会被重置
                    //_owner.actor.forward = moveDirection;
                }
                if (_owner.CanActiveMove())
                {
                    _owner.deltaHv = _owner.deltaHv + _owner.freeHdeceleration * UnityPropUtils.deltaTime;
                    float curMoveSpeed = _owner.freeFlyHspeed + _owner.deltaHv;
                    Vector3 movement = moveDirection;
                    actor.animationController.SetApplyRootMotion(false);
                    actor.actorController.Move(movement, true);
                    actor.actorController.SetFreeFly(_owner.stateManager.InState(29));
                    sprint = true;
                    TryClearPlayerAction();
                }
                else
                {
                    actor.actorController.Stop();
                    sprint = false;
                }
                break;
            }
            TrySprint(sprint);
            return true;
        }

        private Animator flyactor = null;
        int glideHitLayer = 1 << ACTSystem.ACTSystemTools.NameToLayer("Terrain");
        private Transform flycamera = null;
        private float grange = 150;
        private float gs = 3;
        private float gtrans = 0.008f;
        private bool UpdatePlanA()
        {
            var actor = movingActor;
            if (actor == null) return false;
            _owner.position = actor.position;
            if (ControlStickState.strength < 0) return false;
            if (fly == null)
            {
                fly = GameObject.Find("fly(Clone)");
                if (fly == null)
                {
                    return false;
                }
                flyactor = fly.GetComponent<Animator>();
                flycamera = fly.transform.Find("fly_body/fly_body_new_rotation");
                List<float> globalParams = data_parse_helper.ParseListFloat(global_params_helper.GetGlobalParam(304).Split(','));
                grange = globalParams[0];
                gs = globalParams[1];
                gtrans = globalParams[2];
            }
            CheckingStick();
            bool sprint = false;
            while (true)
            {
                Vector3 moveDirection = Vector3.one * (ControlStickState.direction.x / ControlStickState.direction.magnitude);
                if (ControlStickState.strength == 0)
                {
                    moveDirection = Vector3.forward;
                }
                if (Physics.Raycast(actor.position, actor.forward, 2, glideHitLayer))
                {
                    GameMain.EntityPlayer.Player.GlideDeath();
                    return false;
                }
                if (flyactor.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.99f)
                {
                    fly.SetActive(false);
                    _owner.bufferManager.RemoveBuffer(65001);
                    _owner.bufferManager.RemoveBuffer(4050);
                    return false;
                }
                if (_owner.CanActiveMove())
                {
                    var curMoveSpeed = _owner.moveManager.curMoveSpeed;
                    Vector3 movement = moveDirection * curMoveSpeed;
                    actor.animationController.SetApplyRootMotion(false);
                    actor.actorController.Move(moveDirection, true);
                    float range = grange;
                    float speed = gs;
                    actor.actorController.SetLockToFly(flycamera, true, range, speed, gtrans);
                    sprint = true;
                    TryClearPlayerAction();
                }
                else
                {
                    actor.actorController.Stop();
                    sprint = false;
                }
                break;
            }
            TrySprint(sprint);
            return true;
        }

        public override void OnLeave()
        {
            TrySprint(false);
        }

        public override void Stop()
        {
            base.Stop();
        }

        private void TrySprint(bool state)
        {
            if (state)
            {
                //ari PlayerSprintManager.Instance.TryStartSprint();
            }
            else
            {
                //ari PlayerSprintManager.Instance.TryStopSprint();
            }
        }

        float _nextClearTime = 0;
        private void TryClearPlayerAction()
        {
            if (_nextClearTime < UnityPropUtils.realtimeSinceStartup)
            {
                //ari PlayerCommandManager.GetInstance().MoveByStick();
                _nextClearTime = UnityPropUtils.realtimeSinceStartup + 0.0f;
            }
        }

        private float _lastStickStrength = 0;
        private void CheckingStick()
        {
            var actor = movingActor;
            if (actor == null) return;
            _owner.position = actor.position;
            if (ControlStickState.strength < 0) return;

            if (_lastStickStrength == 0 && _lastStickStrength != ControlStickState.strength)
            {
                //ari EventDispatcher.TriggerEvent(MainUIEvents.CLOSE_PLAYERINFO);
            }
            _lastStickStrength = ControlStickState.strength;
        }
    }
}
