﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameMain;

using ACTSystem;

using Common.Utils;
using GameData;

namespace GameMain.CombatSystem
{
    public class AccordingPathFly : AccordingStateBase
    {
        private Vector3 _targetPosition = Vector3.zero;
        private float _moveSpeed = 0;
        //private List<float> _path;
        private List<Vector3> _path;
        private Vector3 _startPoint = Vector3.zero;     //每段寻路的起始点
        private Vector3 _nextPoint = Vector3.zero;      //每段寻路的终点
        private float _currentPartDistance = 0f;        //每段寻路的距离
        private Vector3 moveByPathTargetPosition = Vector3.zero;
        private float _moveTime = 0;
        private float _endDistance = 0;
        private float _turnAngle = 0;
        private float _turnSpeed = 0;
        private Vector3 _lastDirec = Vector3.zero;
        private float _turnTime = 0.5f;
        public bool isPause = false;

        public AccordingPathFly(EntityCreature owner)
            : base(owner)
        {
            modeType = AccordingMode.AccordingPathFly;
        }

        public bool Reset(List<Vector3> path, float speed, float endDistance = 0)
        {
            var actor = movingActor;
            if (actor == null) return false;
            if (_path != null) _path.Clear();
            moveByPathTargetPosition = Vector3.zero;
            isPause = false;
            _startPoint = Vector3.zero;
            _nextPoint = Vector3.zero;
            _endDistance = endDistance;
            _moveSpeed = speed;
            SetPath(path);
            _targetPosition = _path[_path.Count - 1];
            if ((moveByPathTargetPosition - _targetPosition).magnitude < 0.5f) return false;
            moveByPathTargetPosition = _targetPosition;
            
            var sPosition = actor.position;
            _currentPartDistance = 0;
            if (_path.Count > 1)
            {
                SetNextPoint();//路径中的第一个点为当前玩家所在位置，可以不要
            }
            if (sPosition.x == _targetPosition.x && sPosition.z == _targetPosition.z)
            {
                _targetPosition = Vector3.zero;
                moveByPathTargetPosition = Vector3.zero;
                return false;
            }
            if (!SetNextPoint())
            {
                _targetPosition = Vector3.zero;
                moveByPathTargetPosition = Vector3.zero;
                return false;
            }
            Vector3 dire = _nextPoint - actor.position;
            dire.y = 0;
            _owner.actor.forward = dire.normalized;
            return true;
        }

        public override void OnEnter()
        {
            //_owner.stateManager.OnPathFly();
            //_owner.stateManager.OnPathGlide();
        }

        public override void OnLeave()
        {
            if (_path != null) _path.Clear();
            moveByPathTargetPosition = Vector3.zero;
            //_owner.stateManager.UnPathFly();
            //_owner.stateManager.UnPathGlide();
        }

        float step = 1;
        public override bool Update()
        {
            var actor = movingActor;
            if (actor == null) return false;
            bool result = true;
            _owner.position = actor.position;
            while (true)
            {
                if (!_owner.CanActiveMove())
                {
                    actor.actorController.Stop();
                    if (actor.isFly)
                    {
                        actor.actorController.StopVertical();
                    }
                    return false;
                }
                if (isPause)
                {
                    actor.actorController.Stop();
                    if (actor.isFly)
                    {
                        actor.actorController.StopVertical();
                    }
                    break; 
                }
                actor.animationController.SetApplyRootMotion(false);
                var moveDirection = _nextPoint - actor.position;
                var distance = moveDirection.magnitude;
                bool setNextPointResult = true;
                var distanceToStartPoint = Vector3.Distance(_startPoint, actor.position);
                step = _turnSpeed * 3.14159f / 180 * UnityPropUtils.deltaTime;
                _owner.actor.forward = Vector3.RotateTowards(_owner.actor.forward, _lastDirec, step, 0).normalized;

                var toEndDistance = (actor.position - _targetPosition).magnitude;
                if (toEndDistance < this._endDistance)
                {
                    actor.actorController.Stop();
                    if (actor.isFly)
                    {
                        actor.actorController.StopVertical();
                    }
                    _owner.position = actor.position;
                    return false;
                }
                if (distance < _moveSpeed * UnityPropUtils.deltaTime && !HasNextPoint())
                {
                    actor.position = _nextPoint;
                    _owner.position = actor.position;
                    return false;
                }
                if (ACTSystem.ACTMathUtils.Abs(_currentPartDistance - distanceToStartPoint) <= 0.5f && HasNextPoint())
                {
                    setNextPointResult = SetNextPoint();
                }
                if (!setNextPointResult)
                {
                    result = false;
                    break;
                }
                _moveTime = _moveTime - UnityPropUtils.deltaTime;

                Vector3 movement = moveDirection.normalized * _moveSpeed;
                actor.actorController.verticalSpeed = movement.y;
                actor.actorController.Move(movement);
                break;
            }
            return result;
        }

        private void SetPath(List<Vector3> path)
        {
            List<Vector3> pathCopy = new List<Vector3>(path);
            List<Vector3> listV = new List<Vector3>() { new Vector3(432, 75, 506), new Vector3(432, 75, 477), new Vector3(422, 75, 467), new Vector3(432, 75, 457) };
            if (path == null)
            {
                _path = listV;
            }
            else
            {
                _path = pathCopy;
            }
            _path.Insert(0, movingActor.position);
        }

        private bool HasNextPoint()
        {
            return _path.Count > 0;
        }

        private bool SetNextPoint()
        {
            if (_path == null || _path.Count == 0)
            {
                return false;
            }
            if (_nextPoint != Vector3.zero)
            {
                _startPoint = _nextPoint;
            }
            _nextPoint = _path[0];
            _path.RemoveAt(0);
            if (_startPoint != Vector3.zero)
            {
                _currentPartDistance = Vector3.Distance(_startPoint, _nextPoint);

                Vector3 curDirec = _nextPoint - _startPoint;
                curDirec.y = 0;
                _turnAngle = Vector3.Angle(curDirec, _lastDirec);
                _lastDirec = curDirec.normalized;
                _turnSpeed = _turnAngle / _turnTime;
            } 
            else
            {
                _lastDirec = _owner.actor.forward;
                _lastDirec.y = 0;
                _turnSpeed = 0;
            }
            return true;
        }
    }
}

