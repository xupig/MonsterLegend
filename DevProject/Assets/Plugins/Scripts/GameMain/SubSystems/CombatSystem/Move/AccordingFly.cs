﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameMain;

using ACTSystem;
using Common.States;
using MogoEngine.Events;

using GameData;

using GameLoader.Utils;

namespace GameMain.CombatSystem
{
    //player专用，常规状态下根据摇杆状态移动
    public class AccordingFly : AccordingStateBase
    {
        public AccordingFly(EntityCreature owner)
            : base(owner)
        {
            modeType = AccordingMode.AccordingFly;
        }

        public override void OnEnter()
        {

        }

        private bool setfly = false;
        public override bool Update()
        {
            var actor = movingActor;
            if (actor == null) return false;
            _owner.position = actor.position;
            if (ControlStickState.strength < 0) return false;
            //Debug.LogError("Update:" + ControlStickState.strength);

            CheckingStick();
            bool sprint = false;
            while (true)
            {
                if (ControlStickState.strength == 0)
                {
                    actor.actorController.Stop();
                    if (actor.isFly)
                    {
                        actor.actorController.StopVertical();
                    }
                    sprint = false;
                    setfly = false;
                    break;
                }
                if (!setfly)
                {
                    setfly = true;
                    //_owner.PlayAction(63);
                }
                Vector3 moveDirection = ControlStickState.GetMoveDirectionByControlStick();
                if (_owner.CanTurn())
                {
                    _owner.actor.forward = moveDirection;
                }
                if (_owner.CanActiveMove())
                {
                    var curMoveSpeed = _owner.moveManager.curMoveSpeed;
                    Vector3 movement = moveDirection * curMoveSpeed;
                    actor.animationController.SetApplyRootMotion(false);
                    if (actor.isFly)
                    {
                        //actor.actorController.verticalSpeed = movement.y;
                        if (IsFlyHeightLimit(movement.y))
                        {
                            actor.actorController.StopVertical();
                        }
                        else
                        {
                            actor.actorController.verticalSpeed = movement.y;
                        }
                    }
                    actor.actorController.Move(movement);
                    sprint = true;
                    TryClearPlayerAction();
                }
                else
                {
                    actor.actorController.Stop();
                    sprint = false;
                }
                break;
            }
            TrySprint(sprint);
            return true;
        }

        public override void OnLeave()
        {
            TrySprint(false);
        }

        public override void Stop()
        {
            base.Stop();
        }

        private void TrySprint(bool state)
        {
            if (state)
            {
                //ari PlayerSprintManager.Instance.TryStartSprint();
            }
            else
            {
                //ari PlayerSprintManager.Instance.TryStopSprint();
            }
        }

        float _nextClearTime = 0;
        private void TryClearPlayerAction()
        {
            if (_nextClearTime < UnityPropUtils.realtimeSinceStartup)
            {
                //ari PlayerCommandManager.GetInstance().MoveByStick();
                _nextClearTime = UnityPropUtils.realtimeSinceStartup + 0.0f;
            }
        }

        private float _lastStickStrength = 0;
        private void CheckingStick()
        {
            var actor = movingActor;
            if (actor == null) return;
            _owner.position = actor.position;
            if (ControlStickState.strength < 0) return;

            if (_lastStickStrength == 0 && _lastStickStrength != ControlStickState.strength)
            {
                //ari EventDispatcher.TriggerEvent(MainUIEvents.CLOSE_PLAYERINFO);
            }
            _lastStickStrength = ControlStickState.strength;
        }

        private bool IsFlyHeightLimit(float y)
        {
            if ((y > 0 && movingActor.position.y - movingActor.actorController.groundHeight > global_params_helper.maxFlyH))
            {
                return true;
            }
            if ((y < 0 && movingActor.position.y - movingActor.actorController.groundHeight < global_params_helper.minFlyH))
            {
                return true;
            }
            return false;
        }
    }
}
