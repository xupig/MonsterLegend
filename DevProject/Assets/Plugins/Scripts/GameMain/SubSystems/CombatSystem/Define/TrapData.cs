﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using GameData;
using GameLoader.Utils;
namespace GameMain.CombatSystem
{
    public class TrapData
    {
        public int id;
        public int modelId;
        public int range;
        public int height;
        public int startTime;
        public int duration;
        public Dictionary<int, int> triggerSelfDict;
        public Dictionary<int, List<int>> triggerTargetDict;
        public List<int> targetTypeList;
        public int delType;
        public List<int> spells;
        public int delayTime;
        public int delEntityDelayTime;
        public Dictionary<int, List<int>> addBuffDict;
        public Dictionary<int, int> delBuffDict;

        public TrapData(int id)
        {
            try
            {
                if (!GameData.XMLManager.trap.ContainsKey(id))
                {
                    LoggerHelper.Error("TrapData Key Error:" + id);
                    return;
                }
            }
            catch (Exception ex) { LoggerHelper.Error("TrapData:" + GameData.XMLManager.trap + ":" + ex.Message + "\n\n" + ex.StackTrace); }
            trap data = null;
            data = GameData.XMLManager.trap[id];

            this.id = data.__id;
            modelId = data.__model_id;
            range = data.__range;
            height = data.__height;
            startTime = data.__start_time;
            duration = data.__duration;
            spells = data_parse_helper.ParseListInt(data.__spells);
            targetTypeList = data_parse_helper.ParseListInt(data.__target_type);
            delType = data.__del_type;
            delayTime = data.__del_delay_time;
            addBuffDict = data_parse_helper.ParseDictionaryIntListInt(data.__add_buff);
            delBuffDict = data_parse_helper.ParseDictionaryIntInt(data.__del_buff);
            triggerSelfDict = data_parse_helper.ParseDictionaryIntInt(data.__trigger_self);
            delEntityDelayTime = data.__del_entity_delay_time;
            triggerTargetDict = data_parse_helper.ParseDictionaryIntListInt(data.__trigger_target);
        }

    }
}
