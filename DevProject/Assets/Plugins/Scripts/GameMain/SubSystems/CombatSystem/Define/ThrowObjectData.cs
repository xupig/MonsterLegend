﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using GameData;
using GameLoader.Utils;
namespace GameMain.CombatSystem
{
    public class ThrowObjectData
    {
        public int id;
        public int speed;
        public int modelId;
        public int totalTime;
        public Dictionary<int, int> timeEffect;
        public int touchCondition;
        public int touchEffect;
        public int delType;
        public List<int> spells;
        public int delay_time;

        public ThrowObjectData(int id)
        {
            try
            {
                if (!GameData.XMLManager.throw_object.ContainsKey(id))
                {
                    LoggerHelper.Error("ThrowObjectData Key Error:" + id);
                    return;
                }
            }
            catch (Exception ex) { LoggerHelper.Error("ThrowObjectData:" + GameData.XMLManager.throw_object + ":" + ex.Message + "\n\n" + ex.StackTrace); }
            throw_object data = null;
            data = GameData.XMLManager.throw_object[id];

            this.id = data.__id;
            modelId = data.__model_id;
            speed = data.__speed;
            totalTime = data.__total_time;
            timeEffect = data_parse_helper.ParseDictionaryIntInt(data.__time_effect);
            touchCondition = data.__touch_condition;
            touchEffect = data.__touch_effect;
            delType = data.__del_type;
            spells = data_parse_helper.ParseListInt(data.__spells);
            delay_time = data.__delay_time;
        }

    }
}
