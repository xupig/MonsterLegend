﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using GameData;
using GameLoader.Utils;
namespace GameMain.CombatSystem
{

    public class SkillData
    {
        public static int SKILL_POS_ONE = 1;
        public static int SKILL_POS_TWO = 2;
        public static int SKILL_POS_THREE = 3;
        public static int SKILL_POS_FOUR = 4;

        public int skillID;

        public int group;
        public int level;

        public int pos;
        public int dir;
        public int showPirority;
        public int showType;
        public int is_stick_change;//新增
        public List<int> stick_change_list;//新增
        public List<int> adjustSPGroup;
        public List<int> adjustSpell;
        public int adjustPriority;  //新增

        public int adjustType;
        public int spellType;//新增
        public List<int> skillActions;
        public List<float> skillActionProbs;
        public List<float> skillActionActiveTimes;

        //public int spActiveWay; //不用
        public List<int> cd;
        public int chargeTimes; //新增
        public int energyCostType; //新增
        public int energyCost; //新增
        //public Dictionary<int, int> useCosts; //不用

        public Dictionary<int, List<int>> addBuffsOnCast;
        public List<int> delBuffsOnBreak;

        public int specialSearchType;//新增
        public List<int> specialSearchArg;//新增
        public int searchType = 1;//新增
        public int spellZone = 0;//新增
        public List<int> useTimes;//新增

        //public float attackActionSpeed;
        public float actionCutTime;
        //public int actionMovSwitch;
        public List<int> searchTargetType;
        public List<int> originAdjust;
        public int searchRegionType;
        public List<int> searchRegionArg;
        public List<int> targetFilterOrders;
        public Dictionary<int, List<int>> targetFilterArgs;
        public int accordingStick;
        public int searchTargetRepeat;
        public int faceLockMode;
        public int faceAlways;
        public int faceTgt;
        public bool clearBuffWhenCast;
        public List<SkillEventData> skillEvents;

        public int isSingingRequire;
        public int singingTime;

        public int isStorageRequire;
        public int followingSpell;

        public int aiRange;
        public int iconId;
        public int remainingTime;
        public bool needRecord;

        public SkillData(int skillID)
        {
            try
            {
                if (!GameData.XMLManager.spell.ContainsKey(skillID)) 
                {
                    LoggerHelper.Error("SkillData Key Error:" + skillID);
                    return;
                }
            }
            catch (Exception ex) { GameLoader.Utils.LoggerHelper.Error("SkillData22:" + GameData.XMLManager.spell + ":" + ex.Message + "\n\n" + ex.StackTrace); }
            spell data = null;
            data = GameData.XMLManager.spell[skillID];
            
            this.skillID = data.__id;
            skillActionActiveTimes = data_parse_helper.ParseListFloat(data.__action_active_time);
            skillEvents = CombatDataTools.ParseSkillEvents(data.__events);
            var skillSoundEvents = CombatDataTools.ParseSkillEvents(data.__sound_events);
            skillEvents.AddRange(skillSoundEvents);
            group = data.__group;
            level = data.__sp_level;
            pos = data.__pos;
            showPirority = data.__show_priority;
            showType = data.__show_type;
            is_stick_change = data.__is_stick_change;
            stick_change_list = data_parse_helper.ParseListInt(data.__stick_change_list);
            adjustSPGroup = data_parse_helper.ParseListInt(data.__adjust_sp_group);
            adjustSpell = data_parse_helper.ParseListInt(data.__adjust_spell);
            adjustType = data.__adjust_type;
            skillActions = data_parse_helper.ParseListInt(data.__actions);
            skillActionProbs = data_parse_helper.ParseListFloat(data.__action_probs);
            skillActionActiveTimes = data_parse_helper.ParseListFloat(data.__action_active_time);
            //spActiveWay = data.__sp_active_way;
            cd = data_parse_helper.ParseListInt(data.__cd);
            energyCostType = data.__energy_cost_type;
            energyCost = data.__cost;
            //useCosts = data_parse_helper.ParseDictionaryIntInt(data.__use_costs);
            addBuffsOnCast = data_parse_helper.ParseDictionaryIntListInt(data.__add_buffs_on_cast);
            delBuffsOnBreak = data_parse_helper.ParseListInt(data.__del_buffs_on_break);
            specialSearchType = data.__special_search_type;
            specialSearchArg = data_parse_helper.ParseListInt(data.__special_search_arg);
            searchType = data.__search_type;
            isStorageRequire = data.__is_storage_require;
            //attackActionSpeed = data.__attack_action_speed;
            actionCutTime = data.__action_cut_time;
            //actionMovSwitch = data.__action_mov_switch;
            searchTargetType = data_parse_helper.ParseListInt(data.__search_tgt_type2);
            originAdjust = data_parse_helper.ParseListInt(data.__origin_adjust);
            searchRegionType = data.__search_region_type;
            searchRegionArg = data_parse_helper.ParseListInt(data.__search_region_arg);
            targetFilterOrders = data_parse_helper.ParseListInt(data.__target_filter_orders);
            targetFilterArgs = data_parse_helper.ParseDictionaryIntListInt(data.__target_filter_args);
            accordingStick = data.__according_stick;
            searchTargetRepeat = data.__search_tgt_repeat;
            faceLockMode = data.__face_lock_mode;
            faceAlways = data.__face_always;
            faceTgt = data.__face_tgt;
            clearBuffWhenCast = data.__show_type >= 0;
            aiRange = data.__ai_range;
            iconId = data.__icon;
            spellZone = data.__spell_zone;
            useTimes = data_parse_helper.ParseListInt(data.__use_times);
            remainingTime = data.__remaining_time;
            needRecord = data.__need_record == 1;
        }
        
    }
}
