﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using GameMain;

using MogoEngine;
using MogoEngine.RPC;

namespace GameMain.CombatSystem
{

    public class DrawRangeTools
    {
        static private Dictionary<EntityFilterType, Color> _filterTypeColorMap
            = new Dictionary<EntityFilterType, Color>() 
            {
                {EntityFilterType.SKILL_SUBJECT_FILTER, Color.green},
                {EntityFilterType.SKILL_ACTION_FILTER, Color.red}
            };

        #region 范围显示



        static private float DEBUG_SHOW_TIME = 1;
        static public bool ShowDebugRange = false;
        static public void DrawCircleRange(Vector3 origin, float radius, EntityFilterType filterType)
        {
            if (!UnityPropUtils.IsEditor)
            {
                return;
            }
            if (!ShowDebugRange) return;
            SphereRange.Draw(origin, radius, _filterTypeColorMap[filterType], DEBUG_SHOW_TIME);
        }

        static public void DrawSectorRange(Vector3 origin, Vector3 forward, float radius, float angle, float height, EntityFilterType filterType)
        {
            if (!UnityPropUtils.IsEditor)
            {
                return;
            }
            if (!ShowDebugRange) return;
            SectorRange.Draw(origin, forward, radius, angle, height, _filterTypeColorMap[filterType], DEBUG_SHOW_TIME);
        }

        static public void DrawRectangleRange(Vector3 origin, Vector3 forward, Vector3 right, float length, float width, float height, EntityFilterType filterType)
        {
            if (!UnityPropUtils.IsEditor)
            {
                return;
            }
            if (!ShowDebugRange) return;
            RectangleRange.Draw(origin, forward, right, length, width, height, _filterTypeColorMap[filterType], DEBUG_SHOW_TIME);
        }

        static public void DrawCylinderRange(Vector3 origin, float radius, float height, EntityFilterType filterType)
        {
            if (!UnityPropUtils.IsEditor)
            {
                return;
            }
            if (!ShowDebugRange) return;
            CylinderRange.Draw(origin, radius, height, _filterTypeColorMap[filterType], DEBUG_SHOW_TIME);
        }

        #endregion
    }
}
