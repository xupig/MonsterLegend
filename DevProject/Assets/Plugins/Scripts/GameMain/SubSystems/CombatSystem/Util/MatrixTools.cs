﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using GameMain;

using MogoEngine;
using MogoEngine.RPC;

namespace GameMain.CombatSystem
{
    public static class MatrixTools
    {
        public static Vector3 forward(this Matrix4x4 matrix)
        {
            Vector4 vz = matrix.GetColumn(2);
            return new Vector3(vz.x, vz.y, vz.z);
        }

        public static Vector3 upwards(this Matrix4x4 matrix)
        {
            Vector4 vy = matrix.GetColumn(1);
            return new Vector3(vy.x, vy.y, vy.z);
        }

        public static Vector3 right(this Matrix4x4 matrix)
        {
            Vector4 vx = matrix.GetColumn(0);
            return new Vector3(vx.x, vx.y, vx.z);
        }

        public static Quaternion rotation(this Matrix4x4 matrix)
        {
            return Quaternion.LookRotation(forward(matrix), upwards(matrix));
        }

        public static Vector3 position(this Matrix4x4 matrix)
        {
            return new Vector3(matrix.m03, matrix.m13, matrix.m23); ;
        }
    }
}
