﻿using GameData;
using GameLoader.Utils;
using GameMain.CombatSystem;
using System;
using UnityEngine;

namespace GameMain.CombatSystem
{

    public class BaseRange : MonoBehaviour
    {
        protected Transform _trans;
        protected float _endTime;
        protected GameObject _mesh;
        void Awake()
        {
            _trans = this.transform;
            _mesh = _trans.Find("Mesh").gameObject;
        }

        protected void ResetPosition(Vector3 pos)
        {
            _trans.position = pos;
        }

        protected void ResetDuration(float time)
        {
            _endTime = UnityPropUtils.realtimeSinceStartup + time;
        }

        protected void ResetColor(Color color)
        {
            var renderer = this._mesh.GetComponent<MeshRenderer>();
            renderer.material.SetColor("MainColor", color);
        }

        protected void Play()
        {
            this.gameObject.SetActive(true);
            OnPlay();
        }

        void Update()
        {
            if (UnityPropUtils.realtimeSinceStartup > _endTime)
            {
                Stop();
            }
        }

        protected void Stop()
        {
            this.gameObject.SetActive(false);
            OnStop();
        }

        virtual protected void OnPlay()
        {

        }

        virtual protected void OnStop()
        {
        }

        static protected void LoadRange<T>(string modelPath, Action<T> callback) where T : BaseRange
        {
            //no ResourceMappingManager
            GameResource.ObjectPool.Instance.GetGameObjects(new string[] { modelPath }, (objs) =>
            {
                if (objs[0] == null)
                {
                    LoggerHelper.Error("CreateRange modelName:" + modelPath + " is null");
                    return;
                }
                GameObject go = objs[0] as GameObject;
                T range = go.AddComponent<T>();
                callback(range);
            });
        }

        static Transform _rangePoolObj;
        static protected Transform GetRangePoolObj()
        {
            if (_rangePoolObj == null)
            {
                GameObject go = new GameObject("_RangePool");
                GameObject.DontDestroyOnLoad(go);
                _rangePoolObj = go.transform;
            }
            return _rangePoolObj;
        }

    }
}