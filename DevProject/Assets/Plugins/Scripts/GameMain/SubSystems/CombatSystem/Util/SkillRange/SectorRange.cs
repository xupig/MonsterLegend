﻿using GameData;
using GameLoader.Utils;
using GameMain.CombatSystem;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.CombatSystem
{
    public class SectorRange : BaseRange
    {

        override protected void OnPlay()
        {

        }

        override protected void OnStop()
        {
            this.transform.SetParent(GetRangePoolObj(), false);
            _rangePool.Enqueue(this);
        }

        protected void ResetForward(Vector3 forward)
        {
            this.transform.forward = forward;
            //this.transform.Rotate(new Vector3(0, 1, 0), -90);
        }

        protected void ResetMesh(float radius, float angle, float high)
        {
            //_trans.localScale = new Vector3(radius, radius, radius);
            MeshFilter mf = this._mesh.GetComponent<MeshFilter>();
            mf.mesh = CreateMesh(radius, angle, high, 3, 0, 0);
            
        }

        static void Draw(SectorRange range, Vector3 origin, Vector3 forward, float radius, float angle, float high, Color color, float time)
        {
            range.ResetPosition(origin);
            range.ResetDuration(time);
            range.ResetColor(color);
            range.ResetForward(forward);
            range.ResetMesh(radius, angle, high);
            range.Play();
        }

        static Queue<SectorRange> _rangePool = new Queue<SectorRange>();
        static public void Draw(Vector3 origin, Vector3 forward, float radius, float angle, float high, Color color, float time)
        {
            if (_rangePool.Count == 0)
            {
                LoadRange<SectorRange>(Utils.PathUtils.SectorRangePath, (newRange) =>
                {
                    GameObject.DontDestroyOnLoad(newRange);
                    newRange.gameObject.name = "SectorRange";
                    newRange.transform.SetParent(null);
                    Draw(newRange, origin, forward, radius, angle, high, color, time);
                });
            }
            else
            {
                var range = _rangePool.Dequeue();
                range.transform.SetParent(null);
                Draw(range, origin, forward, radius, angle, high, color, time);
            }
        }

        private float radius;
        private float angleDegree;
        private int segments;

        private Mesh cacheMesh ;

        /// <summary>
        /// 创建一个扇形Mesh
        /// </summary>
        /// <param name="radius">扇形半价</param>
        /// <param name="angleDegree">扇形角度</param>
        /// <param name="segments">扇形弧线分段数</param>
        /// <param name="angleDegreePrecision">扇形角度精度（在满足精度范围内，认为是同个角度）</param>
        /// <param name="radiusPrecision">
        /// <pre>
        /// 扇形半价精度（在满足半价精度范围内，被认为是同个半价）。
        /// 比如：半价精度为1000，则：1.001和1.002不被认为是同个半径。因为放大1000倍之后不相等。
        /// 如果半价精度设置为100，则1.001和1.002可认为是相等的。
        /// </pre>
        /// </param>
        /// <returns></returns>
        public Mesh CreateMesh(float radius, float angleDegree, float high, int segments, int angleDegreePrecision, int radiusPrecision)
        {
            if (checkDiff(radius, angleDegree, high, segments, angleDegreePrecision, radiusPrecision))
            {
                Mesh newMesh = Create(radius, angleDegree, high, segments);
                if (newMesh != null)
                {
                    cacheMesh = newMesh;
                    this.radius = radius;
                    this.angleDegree = angleDegree;
                    this.segments = segments;
                }
            }
            return cacheMesh;
        }

        private Mesh Create(float radius, float angleDegree, float high, int segments)
        {

            if (segments == 0)
            {
                segments = 1;
#if UNITY_EDITOR
                LoggerHelper.Info("segments must be larger than zero.");
#endif
            }

            Mesh mesh = new Mesh();
            Vector3[] vertices = new Vector3[(3 + segments - 1) * 2];
            int topIdx = vertices.Length / 2;
            vertices[0] = new Vector3(0, 0, 0);
            vertices[topIdx] = new Vector3(0, high, 0);

            float angle = Mathf.Deg2Rad * angleDegree;
            float currAngle = angle / 2;
            float deltaAngle = angle / segments;

            for (int i = 1; i < topIdx; i++)
            {
                vertices[i] = new Vector3(Mathf.Cos(currAngle) * radius, 0, Mathf.Sin(currAngle) * radius);
                vertices[i + topIdx] = new Vector3(vertices[i].x, high, vertices[i].z);
                currAngle -= deltaAngle;
            }

            int bottomSum = segments * 3;
            int sideIdx = bottomSum * 2;
            int triangleVSum = bottomSum * 2 + 4 * 3;
            int[] triangles = new int[sideIdx + 4 * 3];
            for (int i = 0, vi = 1; i < bottomSum; i += 3, vi++)
            {
                triangles[i] = 0;
                triangles[i + 1] = vi;
                triangles[i + 2] = vi + 1;
                triangles[bottomSum + i] = topIdx;
                triangles[bottomSum + i + 1] = topIdx + vi;
                triangles[bottomSum + i + 2] = topIdx + vi + 1;
            }

            triangles[sideIdx] = 1;
            triangles[sideIdx + 1] = topIdx;
            triangles[sideIdx + 2] = 0;

            triangles[sideIdx + 3] = topIdx + 1;
            triangles[sideIdx + 4] = topIdx;
            triangles[sideIdx + 5] = 1;

            triangles[sideIdx + 6] = 0;
            triangles[sideIdx + 7] = topIdx;
            triangles[sideIdx + 8] = topIdx - 1;

            triangles[sideIdx + 9] = topIdx  - 1;
            triangles[sideIdx + 10] = topIdx;
            triangles[sideIdx + 11] = topIdx * 2 - 1;

            mesh.vertices = vertices;
            mesh.triangles = triangles;

            Vector2[] uvs = new Vector2[vertices.Length];
            for (int i = 0; i < uvs.Length; i++)
            {
                uvs[i] = new Vector2(vertices[i].x, vertices[i].z);
            }
            mesh.uv = uvs;

            return mesh;
        }

        private bool checkDiff(float radius, float angleDegree, float high, int segments, int angleDegreePrecision, int radiusPrecision)
        {
            return segments != this.segments || (int)((angleDegree - this.angleDegree) * angleDegreePrecision) != 0 ||
                   (int)((radius - this.radius) * radiusPrecision) != 0;
        }
    }

}
