﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using GameMain;
using GameMain.GlobalManager;

namespace GameMain.CombatSystem
{
    public class CreatureManagerObjectPool
    {
        private static Queue<SkillManager> _skillManagerPool = new Queue<SkillManager>();
        private static Queue<BufferManager> _bufferManagerPool = new Queue<BufferManager>();
        private static Queue<TrapManager> _trapManagerPool = new Queue<TrapManager>();
        private static Queue<ThrowObjectManager> _throwObjectManagerPool = new Queue<ThrowObjectManager>();
        private static Queue<RideManager> _rideManagerPool = new Queue<RideManager>(); // 没有Update方法
        private static Queue<MoveManager> _moveManagerPool = new Queue<MoveManager>();
        private static Queue<EquipManager> _equipManagerPool = new Queue<EquipManager>(); //
        private static Queue<ConditionEventManager> _conditionEventManagerPool = new Queue<ConditionEventManager>(); //
        private static Queue<StateManager> _stateManagerPool = new Queue<StateManager>(); //

        private static Queue<SkillSubjectManagerServer> _skillSubjectManagerServerPool = new Queue<SkillSubjectManagerServer>();
        private static Queue<SkillSubjectManager> _skillSubjectManagerPool = new Queue<SkillSubjectManager>();
        private static Queue<SkillActionManager> _skillActionManagerPool = new Queue<SkillActionManager>();
        private static Queue<SkillActionManagerServer> _skillActionManagerPoolServer = new Queue<SkillActionManagerServer>();

        #region SkillManager对象池
        // SkillManager 包含 SkillActionManager  SkillSubjectManager  SkillActionManagerServer  SkillSubjectManagerServer
        public static SkillManager CreateSkillManager(EntityCreature owner)
        {
            SkillManager result;
            if (_skillManagerPool.Count > 0)
                result = _skillManagerPool.Dequeue();
            else
                result = new SkillManager();
            result.SetOwner(owner);
            return result;
        }

        public static void ReleaseSkillManager(SkillManager skillManager)
        {
            skillManager.Release();
            _skillManagerPool.Enqueue(skillManager);
        }

        // Subject
        public static SkillSubjectManagerServer CreateSkillSubjectManagerServer(EntityCreature owner)
        {
            SkillSubjectManagerServer result;
            if (_skillSubjectManagerServerPool.Count > 0)
                result = _skillSubjectManagerServerPool.Dequeue();
            else
                result = new SkillSubjectManagerServer();
            result.SetOwner(owner);
            return result;
        }

        public static void ReleaseSkillSubjectManagerServer(SkillSubjectManagerServer skillSubjectManagerServer)
        {
            skillSubjectManagerServer.Release();
            _skillSubjectManagerServerPool.Enqueue(skillSubjectManagerServer);
        }
        
        // 
        public static SkillSubjectManager CreateSkillSubjectManager(EntityCreature owner)
        {
            SkillSubjectManager result;
            if (_skillSubjectManagerPool.Count > 0)
                result = _skillSubjectManagerPool.Dequeue();
            else
                result = new SkillSubjectManager();
            result.SetOwner(owner);
            return result;
        }

        public static void ReleaseSkillSubjectManager(SkillSubjectManager skillSubjectManager)
        {
            skillSubjectManager.Release();
            _skillSubjectManagerPool.Enqueue(skillSubjectManager);
        }

        // Action
        public static SkillActionManager CreateSkillActionManager(EntityCreature owner)
        {
            SkillActionManager result;
            if (_skillActionManagerPool.Count > 0)
                result = _skillActionManagerPool.Dequeue();
            else
                result = new SkillActionManager();
            result.SetOwner(owner);
            return result;
        }

        public static void ReleaseSkillActionManager(SkillActionManager skillActionManager)
        {
            skillActionManager.Release();
            _skillActionManagerPool.Enqueue(skillActionManager);
        }

        public static SkillActionManagerServer CreateSkillActionManagerServer(EntityCreature owner)
        {
            SkillActionManagerServer result;
            if (_skillActionManagerPoolServer.Count > 0)
                result = _skillActionManagerPoolServer.Dequeue();
            else
                result = new SkillActionManagerServer();
            result.SetOwner(owner);
            return result;
        }

        public static void ReleaseSkillActionManagerServer(SkillActionManagerServer skillActionManagerServer)
        {
            skillActionManagerServer.Release();
            _skillActionManagerPoolServer.Enqueue(skillActionManagerServer);
        }
        #endregion


        #region BufferManager对象池
        public static BufferManager CreateBufferManager(EntityCreature owner)
        {
            BufferManager result;
            if (_bufferManagerPool.Count > 0)
                result = _bufferManagerPool.Dequeue();
            else
                result = new BufferManager();
            result.SetOwner(owner);
            return result;
        }

        public static void ReleaseBufferManager(BufferManager bufferManager)
        {
            bufferManager.Release();
            _bufferManagerPool.Enqueue(bufferManager);
        }
        #endregion

        #region TrapManager对象池
        public static TrapManager CreateTrapManager(EntityCreature owner)
        {
            TrapManager result;
            if (_trapManagerPool.Count > 0)
                result = _trapManagerPool.Dequeue();
            else
                result = new TrapManager();
            result.SetOwner(owner);
            return result;
        }

        public static void ReleaseTrapManager(TrapManager trapManager)
        {
            trapManager.Release();
            _trapManagerPool.Enqueue(trapManager);
        }
        #endregion

        #region ThrowObjectManager对象池
        public static ThrowObjectManager CreateThrowObjectManager(EntityCreature owner)
        {
            ThrowObjectManager result;
            if (_throwObjectManagerPool.Count > 0)
                result = _throwObjectManagerPool.Dequeue();
            else
                result = new ThrowObjectManager();
            result.SetOwner(owner);
            return result;
        }

        public static void ReleaseThrowObjectManager(ThrowObjectManager throwObjectManager)
        {
            throwObjectManager.Release();
            _throwObjectManagerPool.Enqueue(throwObjectManager);
        }
        #endregion

        #region RideManager对象池
        public static RideManager CreateRideManager(EntityCreature owner)
        {
            RideManager result;
            if (_rideManagerPool.Count > 0)
                result = _rideManagerPool.Dequeue();
            else
                result = new RideManager();
            result.SetOwner(owner);
            return result;
        }

        public static void ReleaseRideManager(RideManager rideManager)
        {
            rideManager.Release();
            _rideManagerPool.Enqueue(rideManager);
        }
        #endregion

        #region MoveManager对象池
        public static MoveManager CreateMoveManager(EntityCreature owner)
        {
            MoveManager result;
            if (_moveManagerPool.Count > 0)
                result = _moveManagerPool.Dequeue();
            else
                result = new MoveManager();
            result.SetOwner(owner);
            return result;
        }

        public static void ReleaseMoveManager(MoveManager moveManager)
        {
            moveManager.Release();
            _moveManagerPool.Enqueue(moveManager);
        }
        #endregion

        #region EquipManager对象池
        public static EquipManager CreateEquipManager(EntityCreature owner)
        {
            EquipManager result;
            if (_equipManagerPool.Count > 0)
                result = _equipManagerPool.Dequeue();
            else
                result = new EquipManager();
            result.SetOwner(owner);
            return result;
        }

        public static void ReleaseEquipManager(EquipManager equipManager)
        {
            equipManager.Release();
            _equipManagerPool.Enqueue(equipManager);
        }
        #endregion

        #region ConditionEventManager对象池
        public static ConditionEventManager CreateConditionEventManager(EntityCreature owner)
        {
            ConditionEventManager result;
            if (_conditionEventManagerPool.Count > 0)
                result = _conditionEventManagerPool.Dequeue();
            else
                result = new ConditionEventManager();
            result.SetOwner(owner);
            return result;
        }

        public static void ReleaseConditionEventManager(ConditionEventManager conditionEventManager)
        {
            conditionEventManager.Release();
            _conditionEventManagerPool.Enqueue(conditionEventManager);
        }
        #endregion

        #region StateManager对象池
        public static StateManager CreateStateManager(EntityCreature owner)
        {
            StateManager result;
            if (_stateManagerPool.Count > 0)
                result = _stateManagerPool.Dequeue();
            else
                result = new StateManager();
            result.SetOwner(owner);
            return result;
        }

        public static void ReleaseStateManager(StateManager stateManager)
        {
            stateManager.Release();
            _stateManagerPool.Enqueue(stateManager);
        }
        #endregion


        public static void ClearAllData()
        {
            _skillManagerPool.Clear();
            _skillSubjectManagerServerPool.Clear();
            _skillSubjectManagerPool.Clear();
            _skillActionManagerPool.Clear();
            _skillActionManagerPoolServer.Clear();
            _bufferManagerPool.Clear();
            _trapManagerPool.Clear();
            _throwObjectManagerPool.Clear();
            _rideManagerPool.Clear();
            _moveManagerPool.Clear();
            _equipManagerPool.Clear();
            _conditionEventManagerPool.Clear();
            _stateManagerPool.Clear();
        }
    }
}
