﻿using GameLoader.Utils.Timer;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;
using Common.States;

namespace GameMain.CombatSystem
{
    public class PickUpSkillControl
    {
        static private BasePickUpRange _curPickUpRange = null;
        static public BasePickUpRange curPickUpRange
        {
            get { return _curPickUpRange; }
            set { _curPickUpRange = value; }
        }

        static public void ClosePickUpRange()
        {
            if (curPickUpRange == null)
            {
                return;
            }
            curPickUpRange.Stop();
        }

        public static void SetPickUpPosData(Vector3 selectPos)
        {
            EntityPlayer.Player.pickUpSkillPosition = selectPos;
            int xPos=(int)(selectPos.x*100);
            int yPos=(int)(selectPos.y*100);
            int zPos=(int)(selectPos.z*100);
            var face = EntityPlayer.Player.face;
            byte faceX = (byte)(face.x);
            byte faceY = (byte)(face.y);
            byte faceZ = (byte)(face.z);
            int skillId = EntityPlayer.Player.skillManager.curSkillId;
            EntityPlayer.Player.RpcCall("mark_origin_req", skillId, xPos, yPos, zPos, faceX, faceY, faceZ);
        }
     
        static public void DrawPickUpByRangeType(Matrix4x4 srcWorldMatrix, int type, List<int> targetRangeParam)
        {
            switch (type)
            {
                case 1:
                    Matrix4x4 m = srcWorldMatrix;
                    Matrix4x4 matrixPosotionOffset = Matrix4x4.identity;
                    matrixPosotionOffset.SetColumn(3, new Vector4(0, 0, 0, 1));
                    m = m * matrixPosotionOffset;
                    Vector3 pos = new Vector3(m.m03, m.m13, m.m23);
                    CirclePickUpRange.Draw(pos, targetRangeParam[0] * 0.01f, targetRangeParam[1] * 0.01f, targetRangeParam[2] * 0.01f, targetRangeParam[3] * 0.01f);
                    break;
                default:
                    break;
            }
        }       
    }
}
