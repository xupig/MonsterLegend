﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using GameMain;

using MogoEngine;
using MogoEngine.RPC;
using GameLoader.Utils.Timer;
using GameData;

namespace GameMain.CombatSystem
{

    public class EarlyWarning
    {
        // 预警参数
        static private Color _staticColor = new Color(1, 41 / 255f, 41 / 255f, 1);
        static private Color _dynamicColor = new Color(211 / 255f, 11 / 255f, 11 / 255f, 1);
        static private float _thickness = 0.03f;
        static private float _internalAlpha = 0.7f;
        static private float _internalAlphaStatic = 0.4f;

        #region 范围判断

        static public void DrawEarlyWarningByRangeType(Matrix4x4 srcWorldMatrix, TargetRangeType targetRangeType, List<int> targetRangeParam, List<int> originAdjust, float duration = 1)
        {
            float offsetX = 0;
            float offsetY = 0;
            float angleOffset = 0;
            if (originAdjust != null && originAdjust.Count>0)
            {
                offsetX = originAdjust[0] * 0.01f;
                offsetY = originAdjust[1] * 0.01f;
                if (originAdjust.Count>=3) 
                { 
                    angleOffset = originAdjust[2]; 
                }
            }
            switch (targetRangeType)
            {
                case TargetRangeType.SphereRange:
                    if (targetRangeParam.Count >= 1)
                    {
                        float radius = targetRangeParam[0] * 0.01f;
                        DrawEarlyWarningInCircleRange(srcWorldMatrix, radius, offsetX, offsetY, 0, duration);
                    }
                    break;
                case TargetRangeType.SectorRange:
                    if (targetRangeParam.Count >= 2)
                    {
                        float radius = targetRangeParam[0] * 0.01f;
                        float angle = targetRangeParam[1];
                        DrawEarlyWarningInSectorRange(srcWorldMatrix, radius, angle, offsetX, offsetY, angleOffset, duration);
                    }
                    break;
                case TargetRangeType.ForwardRectangleRange:
                    if (targetRangeParam.Count >= 1)
                    {
                        float length = targetRangeParam[0] * 0.01f;
                        float width = targetRangeParam[1] * 0.01f;
                        DrawEarlyWarningInRectangleRange(srcWorldMatrix, length, width, offsetX, offsetY, angleOffset, duration);
                    }
                    break;
            }
        }

        static public void DrawEarlyWarningByRangeType(Matrix4x4 srcWorldMatrix, TargetRangeType targetRangeType, List<int> targetRangeParam, float duration = 1)
        {
            float offsetX = 0;
            float offsetY = 0;
            float angleOffset = 0;
            switch (targetRangeType)
            {
                case TargetRangeType.SphereRange:
                    if (targetRangeParam.Count >= 1)
                    {
                        float radius = targetRangeParam[0] * 0.01f;
                        DrawEarlyWarningInCircleRange(srcWorldMatrix, radius, offsetX, offsetY, 0, duration);
                    }
                    break;
                case TargetRangeType.SectorRange:
                    if (targetRangeParam.Count >= 2)
                    {
                        float radius = targetRangeParam[0] * 0.01f;
                        float angle = targetRangeParam[1];
                        DrawEarlyWarningInSectorRange(srcWorldMatrix, radius, angle, offsetX, offsetY, angleOffset, duration);
                    }
                    break;
                case TargetRangeType.ForwardRectangleRange:
                    if (targetRangeParam.Count >= 1)
                    {
                        float length = targetRangeParam[0] * 0.01f;
                        float width = targetRangeParam[1] * 0.01f;
                        DrawEarlyWarningInRectangleRange(srcWorldMatrix, length, width, offsetX, offsetY, angleOffset, duration);
                    }
                    break;
            }
        }

        static public void DrawEarlyWarningInCircleRange(Matrix4x4 srcWorldMatrix, float radius, float offsetX = 0, float offsetY = 0, float angleOffset = 0, float duration = 1, EntityFilterType filterType = EntityFilterType.SKILL_SUBJECT_FILTER)
        {
            float offsetZ = global_params_helper.early_warning_adjust_y;
            Matrix4x4 m = srcWorldMatrix;
            Matrix4x4 matrixPosotionOffset = Matrix4x4.identity;
            matrixPosotionOffset.SetColumn(3, new Vector4(offsetY, offsetZ, offsetX, 1));
            m = m * matrixPosotionOffset;
            Vector3 posi = new Vector3(m.m03, m.m13, m.m23);
            DrawCircleStatic(posi, radius, duration);
            DrawCircleDynamic(posi, radius, duration);
        }

        static void DrawCircleStatic(Vector3 position, float radius, float duration)
        {
            //no ResourceMappingManager
            GameResource.ObjectPool.Instance.GetGameObject(Utils.PathUtils.EarlyWarningCirclePath, (go) =>
            {
                GameObject alertObject = go as GameObject;
                if (alertObject)
                {
                    alertObject.transform.position = position;
                    //alertObject.transform.rotation = Quaternion.Euler(270, 0, 0);
                    float scale = radius * 2;
                    alertObject.transform.localScale = new Vector3(scale, scale, scale);
                    CircleGradientViaScale gradientViaScaleScript = alertObject.GetComponent<CircleGradientViaScale>();
                    if (gradientViaScaleScript)
                    {
                        gradientViaScaleScript.hasAnimation = false;
                        gradientViaScaleScript.thickness = _thickness;
                        gradientViaScaleScript.internalAlpha = _internalAlphaStatic;
                        gradientViaScaleScript.baseColor = _staticColor;
                    }
                    TimerHeap.AddTimer(Convert.ToUInt32(duration * 1000), 0, () => { GameObject.Destroy(alertObject); });
                }
            });
        }

        static void DrawCircleDynamic(Vector3 position, float radius, float duration)
        {
            //no ResourceMappingManager
            GameResource.ObjectPool.Instance.GetGameObject(Utils.PathUtils.EarlyWarningCirclePath, (go) =>
            {
                GameObject alertObject = go as GameObject;
                if (alertObject)
                {
                    alertObject.transform.position = position;
                    alertObject.transform.rotation = Quaternion.Euler(270, 0, 0);
                    alertObject.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    CircleGradientViaScale gradientViaScaleScript = alertObject.GetComponent<CircleGradientViaScale>();
                    if (gradientViaScaleScript)
                    {
                        gradientViaScaleScript.hasAnimation = true;
                        gradientViaScaleScript.animCurv.postWrapMode = WrapMode.Default;
                        gradientViaScaleScript.duration = duration;
                        gradientViaScaleScript.maxScale = radius * 2;
                        gradientViaScaleScript.thickness = _thickness;
                        gradientViaScaleScript.internalAlpha = _internalAlpha;
                        gradientViaScaleScript.baseColor = _dynamicColor;
                    }
                    TimerHeap.AddTimer(Convert.ToUInt32(duration * 1000), 0, () => { GameObject.Destroy(alertObject); });
                }
            });
        }

        //static string _earlyWarningsSpotPath = "Fx/EarlyWarning/fx_yujing_Spot.prefab";
        static public void DrawEarlyWarningInSectorRange(Matrix4x4 srcWorldMatrix, float radius, float angle = 180f, float offsetX = 0, float offsetY = 0, float angleOffset = 0, float duration = 1, EntityFilterType filterType = EntityFilterType.SKILL_SUBJECT_FILTER)
        {
            float offsetZ = global_params_helper.early_warning_adjust_y;
            Matrix4x4 m = srcWorldMatrix;
            Matrix4x4 matrixPosotionOffset = Matrix4x4.identity;
            matrixPosotionOffset.SetColumn(3, new Vector4(offsetY, offsetZ, offsetX, 1));
            m = m * matrixPosotionOffset;
            Vector3 posi = new Vector3(m.m03, m.m13, m.m23);
            Matrix4x4 matrixAngleOffset = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, angleOffset, 0), Vector3.one);
            Vector3 forward = srcWorldMatrix.forward();
            forward = matrixAngleOffset.MultiplyVector(forward).normalized;
            DrawSectorStatic(posi, forward, radius, angle, duration);
            DrawSectorDynamic(posi, forward, radius, angle, duration);
        }

        static void DrawSectorStatic(Vector3 position, Vector3 forward, float radius, float angle, float duration)
        {
            //no ResourceMappingManager
            GameResource.ObjectPool.Instance.GetGameObject(Utils.PathUtils.EarlyWarningSpotRadiusPath, (go) =>
            {
                GameObject alertObject = go as GameObject;
                if (alertObject)
                {
                    alertObject.transform.position = position;
                    alertObject.transform.forward = forward;
                    float scaleZ = radius;
                    alertObject.transform.localScale = new Vector3(1.0f, 1.0f, scaleZ);
                    SpotGradientViaRadius gradientViaScaleScript = alertObject.GetComponent<SpotGradientViaRadius>();
                    if (gradientViaScaleScript)
                    {
                        gradientViaScaleScript.hasAnimation = false;
                        gradientViaScaleScript.maxAngle = angle;
                        gradientViaScaleScript.thickness = _thickness;
                        gradientViaScaleScript.internalAlpha = _internalAlphaStatic;
                        gradientViaScaleScript.baseColor = _staticColor;
                    }
                    TimerHeap.AddTimer(Convert.ToUInt32(duration * 1000), 0, () => { GameObject.Destroy(alertObject); });
                }
            });
        }

        static void DrawSectorDynamic(Vector3 position, Vector3 forward, float radius, float angle, float duration)
        {
            //no ResourceMappingManager
            GameResource.ObjectPool.Instance.GetGameObject(Utils.PathUtils.EarlyWarningSpotRadiusPath, (go) =>
            {
                GameObject alertObject = go as GameObject;
                if (alertObject)
                {
                    alertObject.transform.position = position;
                    alertObject.transform.forward = forward;
                    float scaleZ = radius;
                    alertObject.transform.localScale = new Vector3(1.0f, 1.0f, scaleZ);
                    SpotGradientViaRadius gradientViaScaleScript = alertObject.GetComponent<SpotGradientViaRadius>();
                    if (gradientViaScaleScript)
                    {
                        gradientViaScaleScript.hasAnimation = true;
                        gradientViaScaleScript.animCurv.postWrapMode = WrapMode.Default;
                        gradientViaScaleScript.duration = duration;
                        gradientViaScaleScript.maxAngle = angle;
                        gradientViaScaleScript.maxRadius = radius;
                        gradientViaScaleScript.thickness = _thickness;
                        gradientViaScaleScript.internalAlpha = _internalAlpha;
                        gradientViaScaleScript.baseColor = _dynamicColor;
                    }
                    TimerHeap.AddTimer(Convert.ToUInt32(duration * 1000), 0, () => { GameObject.Destroy(alertObject); });
                }
            });
        }


        static public void DrawEarlyWarningInRectangleRange(Matrix4x4 srcWorldMatrix, float length, float width, float offsetX = 0, float offsetY = 0, float angleOffset = 0, float duration = 1, EntityFilterType filterType = EntityFilterType.SKILL_SUBJECT_FILTER)
        {
            float offsetZ = global_params_helper.early_warning_adjust_y;
            Matrix4x4 m = srcWorldMatrix;
            Matrix4x4 matrixPosotionOffset = Matrix4x4.identity;
            matrixPosotionOffset.SetColumn(3, new Vector4(offsetY, offsetZ, offsetX, 1));
            m = m * matrixPosotionOffset;
            Vector3 posi = new Vector3(m.m03, m.m13, m.m23);
            
            var right = srcWorldMatrix.right().normalized;
            var forward = srcWorldMatrix.forward().normalized;
            var rightBottomPoint = posi + right * (width * 0.5f);
            rightBottomPoint.y = posi.y;
            Matrix4x4 matrixAngleOffset = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, angleOffset, 0), Vector3.one);
            forward = matrixAngleOffset.MultiplyVector(forward).normalized;
            right = matrixAngleOffset.MultiplyVector(right).normalized;
            DrawRectangleStatic(posi, forward, width, length, duration);
            DrawRectangleDynamic(posi, forward, width, length, duration);
        }

        static void DrawRectangleStatic(Vector3 position, Vector3 forward, float width, float length, float duration)
        {
            //no ResourceMappingManager
            GameResource.ObjectPool.Instance.GetGameObject(Utils.PathUtils.EarlyWarningStraightPath, (go) =>
            {
                GameObject alertObject = go as GameObject;
                if (alertObject)
                {
                    alertObject.transform.position = position;
                    var targetRotate = Quaternion.FromToRotation(Vector3.forward, forward);
                    alertObject.transform.rotation = targetRotate;
                    alertObject.transform.Rotate(270, 0, 0);
                    float scaleX = width;
                    float scaleY = length;
                    alertObject.transform.localScale = new Vector3(scaleX, scaleY, 1.0f);
                    EdgeGradientViaScaleX gradientViaScaleScript = alertObject.GetComponent<EdgeGradientViaScaleX>();
                    if (gradientViaScaleScript)
                    {
                        gradientViaScaleScript.hasAnimation = false;
                        gradientViaScaleScript.thickness = _thickness;
                        gradientViaScaleScript.internalAlpha = _internalAlphaStatic;
                        gradientViaScaleScript.baseColor = _staticColor;
                    }
                    TimerHeap.AddTimer(Convert.ToUInt32(duration * 1000), 0, () => { GameObject.Destroy(alertObject); });
                }
            });
        }

        static void DrawRectangleDynamic(Vector3 position, Vector3 forward, float width, float length, float duration)
        {
            //no ResourceMappingManager
            GameResource.ObjectPool.Instance.GetGameObject(Utils.PathUtils.EarlyWarningStraightPath, (go) =>
            {
                GameObject alertObject = go as GameObject;
                if (alertObject)
                {
                    alertObject.transform.position = position;
                    var targetRotate = Quaternion.FromToRotation(Vector3.forward, forward);
                    alertObject.transform.rotation = targetRotate;
                    alertObject.transform.Rotate(270, 0, 0);
                    float scaleX = width;
                    float scaleY = length;
                    alertObject.transform.localScale = new Vector3(scaleX, scaleY, 1.0f);
                    EdgeGradientViaScaleX gradientViaScaleScript = alertObject.GetComponent<EdgeGradientViaScaleX>();
                    if (gradientViaScaleScript)
                    {
                        gradientViaScaleScript.hasAnimation = true;
                        gradientViaScaleScript.animCurv.postWrapMode = WrapMode.Default;
                        gradientViaScaleScript.maxScaleX = width;
                        gradientViaScaleScript.duration = duration;
                        gradientViaScaleScript.thickness = _thickness;
                        gradientViaScaleScript.internalAlpha = _internalAlpha;
                        gradientViaScaleScript.baseColor = _dynamicColor;
                    }
                    TimerHeap.AddTimer(Convert.ToUInt32(duration * 1000), 0, () => { GameObject.Destroy(alertObject); });
                }
            });
        }

        #endregion
    }
}
