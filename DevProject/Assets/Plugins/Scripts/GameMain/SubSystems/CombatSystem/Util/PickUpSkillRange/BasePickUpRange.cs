﻿using GameData;
using GameLoader.Utils;
using GameMain.CombatSystem;
using System;
using UnityEngine;

namespace GameMain.CombatSystem
{

    public class BasePickUpRange : MonoBehaviour
    {
        protected Transform _trans;
        protected Transform _skillTrans;
        void Awake()
        {
            _trans = this.transform;
            _skillTrans = this.transform.Find("skill_trans");
        }

        protected void ResetPosition(Vector3 pos)
        {
            _trans.position = pos;
        }

        protected void Play()
        {
            this.gameObject.SetActive(true);
            OnPlay();
        }

        virtual protected void Update()
        {
        }

        public void Stop()
        {
            this.gameObject.SetActive(false);
            OnStop();
        }

        virtual protected void OnPlay()
        {
        }

        virtual protected void OnStop()
        {            
        }

        static protected void LoadRange<T>(string modelPath, Action<T> callback) where T : BasePickUpRange
        {
            //no ResourceMappingManager
            GameResource.ObjectPool.Instance.GetGameObjects(new string[] { modelPath }, (objs) =>
            {
                if (objs[0] == null)
                {
                    LoggerHelper.Error("CreateRange modelName:" + modelPath + " is null");
                    return;
                }
                GameObject go = objs[0] as GameObject;
                T range = go.AddComponent<T>();
                callback(range);
            });
        }

        static Transform _rangePoolObj;
        static protected Transform GetRangePoolObj()
        {
            if (_rangePoolObj == null)
            {
                GameObject go = new GameObject("_PickUpRangePool");
                GameObject.DontDestroyOnLoad(go);
                _rangePoolObj = go.transform;
            }
            return _rangePoolObj;
        }

    }
}