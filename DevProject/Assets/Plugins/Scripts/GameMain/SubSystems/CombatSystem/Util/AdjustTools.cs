﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

using GameMain;

using GameLoader.Utils;

namespace GameMain.CombatSystem
{
    public static class AdjustTools
    {

        public static bool CheckSkillCanAdjust(int mainSkillID, int adjustSkillID)
        {
            var mainSkillData = CombatLogicObjectPool.GetSkillData(mainSkillID);
            var adjustSkillData = CombatLogicObjectPool.GetSkillData(adjustSkillID);
            if (adjustSkillData.adjustSPGroup != null
                && adjustSkillData.adjustSPGroup.Contains(mainSkillData.group))
            {
                return true;
            }
            if (adjustSkillData.adjustSpell != null
                && adjustSkillData.adjustSpell.Contains(mainSkillID))
            {
                return true;
            }
            return false;
        }

        public static void AdjustSkill(SkillSubject skillSubject, List<int> adjustSkillIDList)
        {
            var mainSkillData = CombatLogicObjectPool.GetSkillData(skillSubject.skillID);
            for (int i = 0; i < adjustSkillIDList.Count; i++)
            {
                var adjustSkillID = adjustSkillIDList[i];
                if (!CheckSkillCanAdjust(mainSkillData.skillID, adjustSkillID)) continue;
                var adjustSkillData = CombatLogicObjectPool.GetSkillData(adjustSkillID);
                skillSubject.AddAdjustSkillData(adjustSkillData);
            }
        }

        public static void AdjustSkill(SkillSubjectServer skillSubject, List<int> adjustSkillIDList)
        {
            var mainSkillData = CombatLogicObjectPool.GetSkillData(skillSubject.skillID);
            for (int i = 0; i < adjustSkillIDList.Count; i++)
            {
                var adjustSkillID = adjustSkillIDList[i];
                if (!CheckSkillCanAdjust(mainSkillData.skillID, adjustSkillID)) continue;
                var adjustSkillData = CombatLogicObjectPool.GetSkillData(adjustSkillID);
                skillSubject.AddAdjustSkillData(adjustSkillData);
            }
        }

        public static bool CheckSkillActionCanAdjust(int mainSkillActionID, int adjustSkillActionID)
        {
            var adjustSkillActionData = CombatLogicObjectPool.GetSkillActionData(adjustSkillActionID);
            if (adjustSkillActionData.adjustActionSet.Contains(mainSkillActionID))
            {
                return true;
            }
            return false;
        }

        public static void AdjustSkillAction(SkillAction skillAction, List<int> adjustSkillActionIDList)
        {
            var mainSkillActionData = CombatLogicObjectPool.GetSkillActionData(skillAction.actionID);
            for (int i = 0; i < adjustSkillActionIDList.Count; i++)
            {
                var adjustSkillActionID = adjustSkillActionIDList[i];
                if (!CheckSkillActionCanAdjust(mainSkillActionData.actionID, adjustSkillActionID)) continue;
                var adjustSkillActionData = CombatLogicObjectPool.GetSkillActionData(adjustSkillActionID);
                skillAction.AddAdjustSkillActionData(adjustSkillActionData);
            }
        }

        public static void AdjustSkillAction(SkillActionServer skillAction, List<int> adjustSkillActionIDList)
        {
            var mainSkillActionData = CombatLogicObjectPool.GetSkillActionData(skillAction.actionID);
            for (int i = 0; i < adjustSkillActionIDList.Count; i++)
            {
                var adjustSkillActionID = adjustSkillActionIDList[i];
                if (!CheckSkillActionCanAdjust(mainSkillActionData.actionID, adjustSkillActionID)) continue;
                var adjustSkillActionData = CombatLogicObjectPool.GetSkillActionData(adjustSkillActionID);
                skillAction.AddAdjustSkillActionData(adjustSkillActionData);
            }
        }

        static int CLEAR_FLAG = -99;
        public static int Adjust(int src, int adjustData, AdjustType adjustType)
        {
            int result = src;
            if (adjustData == 0) return src;
            if (adjustData == CLEAR_FLAG) return 0;
            switch (adjustType)
            {
                case AdjustType.ONE:
                    result = adjustData;
                    break;
                case AdjustType.TWO:
                    result += adjustData;
                    break;
                case AdjustType.THREE:
                    result *= adjustData;
                    break;
                case AdjustType.NINE:
                    if (adjustData > 0) result = 0;
                    break;
            }
            return result;
        }

        public static float Adjust(float src, float adjustData, AdjustType adjustType)
        {
            float result = src;
            if (adjustData == 0) return src;
            if (adjustData == CLEAR_FLAG) return 0;
            switch (adjustType)
            {
                case AdjustType.ONE:
                    result = adjustData;
                    break;
                case AdjustType.TWO:
                    result += adjustData;
                    break;
                case AdjustType.THREE:
                    result *= adjustData;
                    break;
                case AdjustType.NINE:
                    if (adjustData > 0) result = 0;
                    break;
            }
            return result;
        }

        public static List<int> Adjust(List<int> src, List<int> adjustData, AdjustType adjustType)
        {
            List<int> result;
            if (adjustData.Count == 0) return src;
            switch (adjustType)
            {
                case AdjustType.ONE:
                    result = adjustData;
                    break;
                case AdjustType.TWO:
                    result = new List<int>();
                    result.InsertRange(0, src);
                    var maxIdx = Mathf.Min(result.Count, adjustData.Count);
                    for (int i = 0; i < maxIdx; i++)
                    {
                        result[i] += adjustData[i];
                    }
                    break;
                case AdjustType.THREE:
                    result = new List<int>();
                    result.InsertRange(0, src);
                    maxIdx = Mathf.Min(result.Count, adjustData.Count);
                    for (int i = 0; i < maxIdx; i++)
                    {
                        result[i] *= adjustData[i];
                    }
                    break;
                case AdjustType.FOUR:
                    result = new List<int>();
                    result.InsertRange(0, src);
                    for (int i = 0; i < result.Count; i++)
                    {
                        result[i] += adjustData[0];
                    }
                    break;
                case AdjustType.FIVE:
                    result = new List<int>();
                    result.InsertRange(0, src);
                    for (int i = 0; i < result.Count; i++)
                    {
                        result[i] *= adjustData[0];
                    }
                    break;
                case AdjustType.SIX:
                    result = new List<int>();
                    result.InsertRange(0, src);
                    for (int i = 0; i < adjustData.Count; i++)
                    {
                        result.Add(adjustData[i]);
                    }
                    break;
                default:
                    result = src;
                    break;
            }
            return result;
        }

        public static List<float> Adjust(List<float> src, List<float> adjustData, AdjustType adjustType)
        {
            List<float> result = null;
            if (adjustData.Count == 0) return src;
            switch (adjustType)
            {
                case AdjustType.ONE:
                    //result.InsertRange(0, adjustData);
                    result = adjustData;
                    break;
                case AdjustType.TWO:
                    result = new List<float>();
                    result.InsertRange(0, src);
                    var maxIdx = Mathf.Min(result.Count, adjustData.Count);
                    for (int i = 0; i < maxIdx; i++)
                    {
                        result[i] += adjustData[i]; 
                    }
                    break;
                case AdjustType.THREE:
                    result = new List<float>();
                    result.InsertRange(0, src);
                    maxIdx = Mathf.Min(result.Count, adjustData.Count);
                    for (int i = 0; i < maxIdx; i++)
                    {
                        result[i] *= adjustData[i]; 
                    }
                    break;
                case AdjustType.FOUR:
                    result = new List<float>();
                    result.InsertRange(0, src);
                    for (int i = 0; i < result.Count; i++)
                    {
                        result[i] += adjustData[0];
                    }
                    break;
                case AdjustType.FIVE:
                    result = new List<float>();
                    result.InsertRange(0, src);
                    for (int i = 0; i < result.Count; i++)
                    {
                        result[i] *= adjustData[0];
                    }
                    break;
                case AdjustType.SIX:
                    result = new List<float>();
                    result.InsertRange(0, src);
                    for (int i = 0; i < adjustData.Count; i++)
                    {
                        result.Add(adjustData[i]);
                    }
                    break;
                default:
                    result = src;
                    break;
            }
            return result;
        }
        
        public static Dictionary<int, int> Adjust(Dictionary<int, int> src, Dictionary<int, int> adjustData, AdjustType adjustType)
        {
            Dictionary<int, int> result = null;
            if (adjustData.Count == 0) return src;
            switch (adjustType)
            {
                case AdjustType.ONE:
                    result = adjustData;
                    break;
                case AdjustType.TWO:
                    result = new Dictionary<int, int>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                        if (adjustData.ContainsKey(keyValue.Key))
                        {
                            result[keyValue.Key] += adjustData[keyValue.Key];
                        }
                    }
                    break;
                case AdjustType.THREE:
                    result = new Dictionary<int, int>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                        if (adjustData.ContainsKey(keyValue.Key))
                        {
                            result[keyValue.Key] *= adjustData[keyValue.Key];
                        }
                    }
                    break;
                case AdjustType.FOUR:
                    result = new Dictionary<int, int>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value + adjustData[1]);
                    }
                    break;
                case AdjustType.FIVE:
                    result = new Dictionary<int, int>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value * adjustData[1]);
                    }
                    break;
                case AdjustType.SIX:
                    result = new Dictionary<int, int>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                    }
                    foreach (var keyValue in adjustData)
                    {
                        if (result.ContainsKey(keyValue.Key))
                        {
                            LoggerHelper.Info(string.Format("GetFireCosts调整技能key重复。key = {0}, src = {1}, new = {2}.", keyValue.Key, result[keyValue.Key], keyValue.Value));
                            result[keyValue.Key] = keyValue.Value;
                        }
                        else
                        {
                            result.Add(keyValue.Key, keyValue.Value);
                        }
                    }
                    break;
                case AdjustType.SEVEN:
                    result = new Dictionary<int, int>();
                    foreach (var keyValue in src)
                    {
                        if (adjustData.ContainsKey(keyValue.Key)) { continue; }
                        result.Add(keyValue.Key, keyValue.Value);
                    }
                    break;
                case AdjustType.EIGHT:
                    result = new Dictionary<int, int>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                    }
                    foreach (var keyValue in adjustData)
                    {
                        result[keyValue.Key] = keyValue.Value;
                    }
                    break;
            }
            return result;
        }

        public static Dictionary<int, float> Adjust(Dictionary<int, float> src, Dictionary<int, float> adjustData, AdjustType adjustType)
        {
            Dictionary<int, float> result = null;
            if (adjustData.Count == 0) return src;
            switch (adjustType)
            {
                case AdjustType.ONE:
                    result = adjustData;
                    break;
                case AdjustType.TWO:
                    result = new Dictionary<int, float>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                        if (adjustData.ContainsKey(keyValue.Key))
                        {
                            result[keyValue.Key] += adjustData[keyValue.Key];
                        }
                    }
                    break;
                case AdjustType.THREE:
                    result = new Dictionary<int, float>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                        if (adjustData.ContainsKey(keyValue.Key))
                        {
                            result[keyValue.Key] *= adjustData[keyValue.Key];
                        }
                    }
                    break;
                case AdjustType.FOUR:
                    result = new Dictionary<int, float>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value + adjustData[1]);
                    }
                    break;
                case AdjustType.FIVE:
                    result = new Dictionary<int, float>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value * adjustData[1]);
                    }
                    break;
                case AdjustType.SIX:
                    result = new Dictionary<int, float>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                    }
                    foreach (var keyValue in adjustData)
                    {
                        if (result.ContainsKey(keyValue.Key))
                        {
                            result[keyValue.Key] = keyValue.Value;
                        }
                        else
                        {
                            result.Add(keyValue.Key, keyValue.Value);
                        }
                    }
                    break;
                case AdjustType.SEVEN:
                    result = new Dictionary<int, float>();
                    foreach (var keyValue in src)
                    {
                        if (adjustData.ContainsKey(keyValue.Key)) { continue; }
                        result.Add(keyValue.Key, keyValue.Value);
                    }
                    break;
                case AdjustType.EIGHT:
                    result = new Dictionary<int, float>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                    }
                    foreach (var keyValue in adjustData)
                    {
                        result[keyValue.Key] = keyValue.Value;
                    }
                    break;
            }
            return result;
        }

        public static Dictionary<int, List<int>> Adjust(Dictionary<int, List<int>> src, Dictionary<int, List<int>> adjustData, AdjustType adjustType)
        {
            Dictionary<int, List<int>> result = null;
            if (adjustData.Count == 0) return src;
            switch (adjustType)
            {
                case AdjustType.ONE:
                    result = adjustData;
                    break;
                case AdjustType.TWO:
                    result = new Dictionary<int, List<int>>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                        if (adjustData.ContainsKey(keyValue.Key))
                        {
                            var newList = result[keyValue.Key];
                            var oldList = adjustData[keyValue.Key];
                            for (int i = 0; newList.Count < 0; i++)
                            {
                                newList[i] += oldList[i];
                            }
                        }
                    }
                    break;
                case AdjustType.THREE:
                    result = new Dictionary<int, List<int>>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                        if (adjustData.ContainsKey(keyValue.Key))
                        {
                            var newList = result[keyValue.Key];
                            var adjustList = adjustData[keyValue.Key];
                            for (int i = 0; newList.Count < 0; i++)
                            {
                                newList[i] *= adjustList[i];
                            }
                        }
                    }
                    break;
                case AdjustType.FOUR:
                    result = new Dictionary<int, List<int>>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                        var newList = result[keyValue.Key];
                        for (int i = 0; newList.Count < 0; i++)
                        {
                            newList[i] += adjustData[1][i];
                        }
                    }
                    break;
                case AdjustType.FIVE:
                    result = new Dictionary<int, List<int>>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                        var newList = result[keyValue.Key];
                        for (int i = 0; newList.Count < 0; i++)
                        {
                            newList[i] *= adjustData[1][i];
                        }
                    }
                    break;
                case AdjustType.SIX:
                    result = new Dictionary<int, List<int>>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                    }
                    foreach (var keyValue in adjustData)
                    {
                        if (result.ContainsKey(keyValue.Key))
                        {
                            result[keyValue.Key] = keyValue.Value;
                        }
                        else
                        {
                            result.Add(keyValue.Key, keyValue.Value);
                        }
                    }
                    break;
                case AdjustType.SEVEN:
                    result = new Dictionary<int, List<int>>();
                    foreach (var keyValue in src)
                    {
                        if (adjustData.ContainsKey(keyValue.Key)) { continue; }
                        result.Add(keyValue.Key, keyValue.Value);
                    }
                    break;
                case AdjustType.EIGHT:
                    result = new Dictionary<int, List<int>>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                    }
                    foreach (var keyValue in adjustData)
                    {
                        result[keyValue.Key] = keyValue.Value;
                    }
                    break;
            }
            return result;
        }

        public static Dictionary<int, List<float>> Adjust(Dictionary<int, List<float>> src, Dictionary<int, List<float>> adjustData, AdjustType adjustType)
        {
            Dictionary<int, List<float>> result = null;
            if (adjustData.Count == 0) return src;
            switch (adjustType)
            {
                case AdjustType.ONE:
                    result = adjustData;
                    break;
                case AdjustType.TWO:
                    result = new Dictionary<int, List<float>>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                        if (adjustData.ContainsKey(keyValue.Key))
                        {
                            var newList = result[keyValue.Key];
                            var oldList = adjustData[keyValue.Key];
                            for (int i = 0; newList.Count < 0; i++)
                            {
                                newList[i] += oldList[i];
                            }
                        }
                    }
                    break;
                case AdjustType.THREE:
                    result = new Dictionary<int, List<float>>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                        if (adjustData.ContainsKey(keyValue.Key))
                        {
                            var newList = result[keyValue.Key];
                            var adjustList = adjustData[keyValue.Key];
                            for (int i = 0; newList.Count < 0; i++)
                            {
                                newList[i] *= adjustList[i];
                            }
                        }
                    }
                    break;
                case AdjustType.FOUR:
                    result = new Dictionary<int, List<float>>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                        var newList = result[keyValue.Key];
                        for (int i = 0; newList.Count < 0; i++)
                        {
                            newList[i] += adjustData[1][i];
                        }
                    }
                    break;
                case AdjustType.FIVE:
                    result = new Dictionary<int, List<float>>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                        var newList = result[keyValue.Key];
                        for (int i = 0; newList.Count < 0; i++)
                        {
                            newList[i] *= adjustData[1][i];
                        }
                    }
                    break;
                case AdjustType.SIX:
                    result = new Dictionary<int, List<float>>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                    }
                    foreach (var keyValue in adjustData)
                    {
                        if (result.ContainsKey(keyValue.Key))
                        {
                            result[keyValue.Key] = keyValue.Value;
                        }
                        else
                        {
                            result.Add(keyValue.Key, keyValue.Value);
                        }
                    }
                    break;
                case AdjustType.SEVEN:
                    result = new Dictionary<int, List<float>>();
                    foreach (var keyValue in src)
                    {
                        if (adjustData.ContainsKey(keyValue.Key)) { continue; }
                        result.Add(keyValue.Key, keyValue.Value);
                    }
                    break;
                case AdjustType.EIGHT:
                    result = new Dictionary<int, List<float>>();
                    foreach (var keyValue in src)
                    {
                        result.Add(keyValue.Key, keyValue.Value);
                    }
                    foreach (var keyValue in adjustData)
                    {
                        result[keyValue.Key] = keyValue.Value;
                    }
                    break;
            }
            return result;
        }  
        
    }
}
