﻿using GameData;
using GameLoader.Utils;
using GameMain.CombatSystem;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.CombatSystem
{
    public class CylinderRange : BaseRange
    {

        override protected void OnPlay()
        {

        }

        override protected void OnStop()
        {
            this.transform.SetParent(GetRangePoolObj(), false);
            _rangePool.Enqueue(this);
        }

        protected void ResetScale(float radius, float height)
        {
            _trans.localScale = new Vector3(radius * 2, height, radius * 2);
        }

        static void Draw(CylinderRange range, Vector3 origin, float radius, float height, Color color, float time)
        {
            range.ResetPosition(origin);
            range.ResetDuration(time);
            range.ResetColor(color);
            range.ResetScale(radius, height);
            range.Play();
        }

        static Queue<CylinderRange> _rangePool = new Queue<CylinderRange>();
        static public void Draw(Vector3 origin, float radius, float height, Color color, float time)
        {
            if (_rangePool.Count == 0)
            {
                LoadRange<CylinderRange>(Utils.PathUtils.CylinderRangePath, (newRange) =>
                {
                    GameObject.DontDestroyOnLoad(newRange);
                    newRange.transform.SetParent(null);
                    Draw(newRange, origin, radius, height, color, time);
                });
            }
            else
            {
                var range = _rangePool.Dequeue();
                range.transform.SetParent(null);
                Draw(range, origin, radius, height, color, time);
            }

        }


    }
}
