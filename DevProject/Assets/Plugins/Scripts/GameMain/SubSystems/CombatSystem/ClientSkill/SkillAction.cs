﻿using Common.ClientConfig;
using Common.ServerConfig;
using Common.States;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameMain;
using GameMain.ClientConfig;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.RPC;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace GameMain.CombatSystem
{
    public enum SkillActionStage
    {
        STANDBY,
        ACTIVE,
        ENDED
    }

    public class SkillActionMovement
    {
        public float startTime;

        public float baseSpeed;
        public MovementType movementType;
        public uint moveEntityID;
        public Vector3 targetPosition;
        public float offsetX;
        public float offsetY;
        public float offsetH;
        public float accelerate;
        public bool percent;
        public float upSpeed;
        public float upAccelerate;
        public float maxMoveDistance;
    }

    public class SkillDropItem
    {
        public int id;
        public float rate;
        public uint minCount;
        public uint maxCount;
        public float minRadius;
        public float maxRadius;
        public int pickType;
        public float duration;
        public SkillDropItemOriginalPointType originalPointType;

        private Vector3 originalPosition;

        private const int DROP_ITEM_MAX_COUNT = 12;
        private const float DROP_ITEM_PER_ANGLE = 30;

        public SkillDropItem(int id, List<float> dataList)
        {
            this.id = id;
            rate = dataList[0];
            minCount = (uint)dataList[1];
            maxCount = (uint)dataList[2];
            minRadius = dataList[3] * 0.01f;
            maxRadius = dataList[4] * 0.01f;
            pickType = (int)dataList[5];
            duration = dataList[6];
            originalPointType = (SkillDropItemOriginalPointType)dataList[7];
        }

        public void SetOriginalPosition(Vector3 originalPosition)
        {
            this.originalPosition = originalPosition;
        }

        public bool CanDrop()
        {
            return RandomUtils.GetRandomFloat(0f, 1f) <= rate;
        }

        public List<Vector2> GetRandomPositionList(bool bFly)
        {
            float startAngle = RandomUtils.GetRandomFloat(0, 360f);
            uint count = GetRandomCount();
            List<Vector2> randomPositionList = new List<Vector2>();
            int[] randomSequence = RandomUtils.RandomSequence(0, DROP_ITEM_MAX_COUNT - 1);

            float angle;
            float distance;
            Vector2 position;
            int index = 0;
            for (int i = 0; i < count; ++i)
            {
                if (index >= randomSequence.Length)
                {
                    index = 0;
                }
                angle = startAngle + randomSequence[index] * DROP_ITEM_PER_ANGLE;
                distance = GetRandomRadius();
                position = GetPositionByAngleAndDistance(angle, distance);
                index++;
                if (!IsCanMove(position, bFly))
                {
                    randomPositionList.Add(GetAroundCanMovePoint(position, bFly));
                }
                else
                {
                    randomPositionList.Add(position);
                }
            }
            return randomPositionList;
        }

        private Vector2 GetAroundCanMovePoint(Vector2 position, bool bFly)
        {
            Vector3 posVec3 = FindPathManager.GetInstance().CheckPointAroundCanMove(position.x, position.y, bFly);
            return new Vector2(posVec3.x, posVec3.z);
        }

        private bool IsCanMove(Vector2 position, bool bFly)
        {
            return FindPathManager.GetInstance().CheckCurrPointIsCanMove(position.x, position.y, bFly);
        }

        private Vector2 GetPositionByAngleAndDistance(float angle, float distance)
        {
            float x = originalPosition.x + distance * Mathf.Cos(angle);
            float y = originalPosition.z + distance * Mathf.Sin(angle);
            return new Vector2(x, y);
        }

        private uint GetRandomCount()
        {
            return (uint)RandomUtils.GetRandomInt((int)minCount, (int)maxCount + 1);
        }

        private float GetRandomRadius()
        {
            return RandomUtils.GetRandomFloat(minRadius, maxRadius);
        }
    }

    public class ShieldAgainstData
    {
        public bool isShieldAgainst = false;
        public int maxHitCount = 0;
        public int angle = 0;
        public int curHitCount = 0;
        public List<int> buffIdList = new List<int>();

        public void ClearData()
        { 
            isShieldAgainst = false;
            maxHitCount = 0;
            angle = 0;
            curHitCount = 0;
            buffIdList.Clear();
        }
    }

    public class SkillAction
    {
        static public float DEFAULT_DURATION = 1;
        public bool isServer = false; 
        public int skillID;
        public int actionID;
        public float delay = 0f;

        public Matrix4x4 ownerWorldMatrixOnSkillActive;
        protected Matrix4x4 _ownerWorldMatrixOnActive;
        protected Matrix4x4 _targetWorldMatrixOnActive;
        protected Matrix4x4 _ownerWorldMatrixOnFixTime;
        protected Matrix4x4 _targetWorldMatrixOnFixTime;
        protected Matrix4x4 _ownerWorldMatrixOnJudge;
        protected Matrix4x4 _targetWorldMatrixOnJudge;
        protected Matrix4x4 _activeOriginMatrix;    //Action激活时的判定矩阵
        protected Matrix4x4 _judgePointMatrix;

        protected bool _haveTarget;
        protected List<int> _fixOriginAdjust;

        protected uint _mainTargetID;
        public uint mainTargetID
        {
            set 
            { 
                _mainTargetID = value;
                if (_actHandler != null) _actHandler.mainTargetID = value;
            }
        }
        //用来陷阱技能指定目标
        public uint assignTargetID;

        protected EntityCreature _owner;
        protected ACTHandler _actHandler;

        protected SkillActionStage _curStage = SkillActionStage.STANDBY;
        public SkillActionStage curState
        {
            get { return this._curStage; }
        }

        protected SkillActionData _skillActionData;
        public SkillActionData skillActionData { get { return _skillActionData; } }
        protected List<SkillActionData> _adjustSkillActionDataList = new List<SkillActionData>();
        public List<SkillActionData> adjustSkillActionDataList { get { return _adjustSkillActionDataList; } }

        protected List<SkillEventData> _skillEventList = new List<SkillEventData>();

        protected List<SkillActionMovement> _skillActionMovements = new List<SkillActionMovement>();

        protected List<uint> _targetIDList = new List<uint>();
        protected Dictionary<uint, hit_fly_params> _hitFlyTargetDict = new Dictionary<uint, hit_fly_params>();
        protected float _duration = 0;
        protected float _pastTime = 0;
        private float _startTimeStamp = 0;
        protected float _fireTime = 0;
        public float fireTime
        {
            get { return this._fireTime; }
        }

        protected RegionJudgeTime _regionJudgeTime;
        protected float _regionJudgeDelayTime = 0;
        public float regionJudgeDelayTime
        {
            get { return this._regionJudgeDelayTime; }
        }

        public SkillAction()
        {
            _actHandler = new ACTHandler();
        }

        public void Reset(SkillActionData data)
        {
            //Debug.LogError("Reset:" + data);
            _duration = DEFAULT_DURATION;
            _pastTime = 0;
            actionID = data.actionID;
            mainTargetID = 0;
            assignTargetID = 0;
            _curStage = SkillActionStage.STANDBY;
            _skillActionData = data;
            _adjustSkillActionDataList.Clear();

            _ownerWorldMatrixOnActive = Matrix4x4.identity;
            _targetWorldMatrixOnActive = Matrix4x4.identity;
            _ownerWorldMatrixOnFixTime = Matrix4x4.identity;
            _targetWorldMatrixOnFixTime = Matrix4x4.identity;
            _ownerWorldMatrixOnJudge = Matrix4x4.identity;
            _targetWorldMatrixOnJudge = Matrix4x4.identity;
            _activeOriginMatrix = Matrix4x4.identity;
            _judgePointMatrix = Matrix4x4.identity;
        }

        private void CalculateDuration()
        {
            for (int i = 0; i < _skillActionMovements.Count; i++)
            {
                _duration = Mathf.Max(_skillActionMovements[i].startTime - RealTime.time, _duration);
            }
            for (int i = 0; i < _skillEventList.Count; i++)
            {
                _duration = Mathf.Max(_skillEventList[i].delay, _duration);
            }
        }

        private void ShowEarlyWarning()
        {
            float duration = _fireTime - RealTime.time;
            if (duration <= 0) return;
            if ((EarlyWarningType)skillActionData.earlyWarningType == EarlyWarningType.None) return;
            if ((EarlyWarningType)skillActionData.earlyWarningType == EarlyWarningType.Self 
                && !this._owner.IsPlayer()) 
            { 
                return; 
            }
            OriginType originType = (OriginType)this.GetOriginType();
            Matrix4x4 judgeMatrix = GetRegionJudgeMatrix(_regionJudgeTime, originType);
            TargetRangeType targetRangeType = (TargetRangeType)this.GetAttackRegionType();
            List<int> targetRangeParam = this.GetAttackRegionArg();
            EarlyWarning.DrawEarlyWarningByRangeType(judgeMatrix, targetRangeType, targetRangeParam, _fixOriginAdjust,duration);
        }

        private void AttackRangePreview()
        {
            //LoggerHelper.Error("[SkillAction:AttackRangePreview]=>1_________________________fireTime:   " + _fireTime);
            float duration = 0.5f;// _fireTime - RealTime.time;
            if (duration <= 0) return;
            //
            OriginType originType = (OriginType)this.GetOriginType();
            Matrix4x4 judgeMatrix = GetRegionJudgeMatrix(_regionJudgeTime, originType);
            TargetRangeType targetRangeType = TargetRangeType.SphereRange;
            List<int> targetRangeParam = new List<int>() { 500};// this.GetAttackRegionArg();
            //LoggerHelper.Error("[SkillAction:AttackRangePreview]=>2_________________________fireTime:   " + _fireTime);
            EarlyWarning.DrawEarlyWarningByRangeType(judgeMatrix, targetRangeType, targetRangeParam, _fixOriginAdjust,duration);
        }


        public void AddAdjustSkillActionData(SkillActionData skillActionData)
        {
            if (_adjustSkillActionDataList.Contains(skillActionData)) return;
            int idx = 0;
            for (int i = 0; i < _adjustSkillActionDataList.Count; i++)
            {
                if (skillActionData.adjustPriority > _adjustSkillActionDataList[i].adjustPriority)
                {
                    break;
                }
                idx++;
            }
            _adjustSkillActionDataList.Insert(idx, skillActionData);
        }

        public List<SkillEventData> GetSkillEventList()
        {
            return _skillEventList;
        }

        
        private void ResetActiveOriginMatrix()
        {          
            var originType = (OriginType)this.GetOriginType();
            _activeOriginMatrix = GetRegionJudgeMatrix(_regionJudgeTime, originType);
            if (!_haveTarget && originType != OriginType.Self && originType != OriginType.Custom)
            {
                _activeOriginMatrix = _ownerWorldMatrixOnActive;
            }
            float offsetX = 0;
            float offsetY = 0;
            float offsetH = 0;
            if (_fixOriginAdjust == null)
            {
                LoggerHelper.Error(string.Format("ResetActiveOriginMatrix, fixOriginAdjust is null, ActionID = {0}.", actionID));
            }
            if (_fixOriginAdjust.Count > 0)
            {
                offsetX = _fixOriginAdjust[0] * 0.01f;
                offsetY = _fixOriginAdjust[1] * 0.01f;
                offsetH = _fixOriginAdjust[2] * 0.01f;
            }
            Matrix4x4 matrixPositionOffset = Matrix4x4.identity;
            matrixPositionOffset.SetColumn(3, new Vector4(offsetY, offsetH, offsetX, 1));
            _activeOriginMatrix = _activeOriginMatrix * matrixPositionOffset;
            _actHandler.actionActiveOrigin = _activeOriginMatrix.position();
        }

        private void ResetFireTime()
        {
            var fireDelayTime = this.GetFireDelayTime();
            if (fireDelayTime >= 0)
            {
                _fireTime = RealTime.time + fireDelayTime * 0.001f;
            }
            else if ((OriginType)this.GetOriginType() != OriginType.Custom)
            {
                var dis = (_activeOriginMatrix.position() - _owner.position).magnitude;
                float speed = ACTSystem.ACTMathUtils.Abs(fireDelayTime * 0.01f);
                _fireTime = RealTime.time + dis / speed;   
            }
                /*ari
            else if ((OriginType)this.GetOriginType() == OriginType.Custom)
            {
				if (_owner == null || !(_owner is EntityAirVehicle) || (_owner as EntityAirVehicle).airVehiclePath.riderSlot == null) return;     //考虑到空中载具销毁了,它的技能应该无效
                //计算到目标点距离
                var dis = (_activeOriginMatrix.position() - _owner.position).magnitude;
                float flyTime = ACTSystem.ACTMathUtils.Abs(fireDelayTime * 0.01f);
                _fireTime = RealTime.time + dis / flyTime;
            }*/
        }

        private void ResetRegionJudgeTime()
        {
            var data = this.GetRegionJudgeTime();
            if (data < 0)
            {
                _regionJudgeDelayTime = RealTime.time + ACTSystem.ACTMathUtils.Abs(data) * 0.001f;
                _regionJudgeTime = RegionJudgeTime.OnFixTime;
            }
            else
            {
                _regionJudgeTime = (RegionJudgeTime)data;
            }
        }

        private void ResetTargetWorldMatrixOnActive()
        {
            _targetWorldMatrixOnActive = Matrix4x4.identity;
            var targetEntity = CombatSystemTools.GetCreatureByID(_mainTargetID);
            if (targetEntity != null && targetEntity.GetTransform() != null)
            {
                _targetWorldMatrixOnActive = targetEntity.GetTransform().localToWorldMatrix;
                _haveTarget = true;
            }
            else
            {
                _haveTarget = false;
            }
            //根据
            if ((OriginType)this.GetOriginType() == OriginType.Custom)
            {
                _actHandler.actionActiveOrigin = _owner.pickUpSkillPosition;
                _targetWorldMatrixOnActive = Matrix4x4.TRS(_actHandler.actionActiveOrigin, Quaternion.identity, Vector3.one);
            }
            /*ari
            if ((OriginType)this.GetOriginType() == OriginType.Custom)
            {
                _actHandler.actionActiveOrigin = (_owner as EntityAirVehicle).attackTargetPosition;
                _targetWorldMatrixOnActive = Matrix4x4.TRS(_actHandler.actionActiveOrigin, Quaternion.identity, Vector3.one);
            }*/
        }

        private void ResetSkillEventList()
        {
            _skillEventList.Clear();
            var skillEvents = this.GetSkillEvents();
            for (int i = 0; i < skillEvents.Count; i++)
            {
                var eventData = skillEvents[i];
                if (eventData.delay < 0)
                {
                    eventData.delay = ACTSkillEventHandler.GetEventDelay(eventData.mainID, eventData.eventIdx);
                }
                _skillEventList.Add(eventData);
            }
        }

        public void SetOwner(EntityCreature owner)
        {
            _owner = owner;
            _actHandler.Clear();
            _actHandler.qualitySettingValue = _owner.qualitySettingValue;
            _actHandler.priority = _owner.visualFXPriority;
        }

        public void Clear()
        {
            _owner = null;
            _skillActionData = null;
            _skillActionMovements.Clear();
        }

        bool IsAllEnd()
        {
            if (_skillActionMovements.Count > 0) return false;
            if (_skillEventList.Count > 0) return false;
            if (_fireTime > 0) return false;
            return true;
        }

        public SkillActionStage Update()
        {
            if (_curStage == SkillActionStage.STANDBY)
            {
                Begin();
            }
            else if (_curStage == SkillActionStage.ACTIVE)
            {
                //_pastTime += UnityPropUtils.deltaTime;
                _pastTime = RealTime.time - _startTimeStamp;
                if (_pastTime >= _duration && IsAllEnd())
                {
                    End();
                }
                else
                {
                    Doing();
                }
            }
            return _curStage;
        }

        void Begin()
        {
            _curStage = SkillActionStage.ACTIVE;
            OnBegin();
            //ari EventDispatcher.TriggerEvent<int, string>(SpaceActionEvents.SpaceActionEvents_Cond, InstanceDefine.COND_TYPE_SKILL_ACTION, this.actionID.ToString());
            if (this._mainTargetID == 0 && ((OriginType)this.GetOriginType()) == OriginType.Target)
            {
                End();
            }
        }

        void Doing()
        {
            DoingEvents();
            Moving();
            CheckRegionJudgeDelayTime();
            Judge();
        }
        
        virtual protected void OnBegin()
        {
            _startTimeStamp = RealTime.time;
            if (_owner.GetTransform() == null) return;//这里不合理，临时加
            TurnToOnBegin();
            _ownerWorldMatrixOnActive = _owner.GetTransform().localToWorldMatrix;
            ResetRegionJudgeTime();
            ResetTargetWorldMatrixOnActive();
            ResetActiveOriginMatrix();
            ResetFireTime();
            ResetSkillEventList();
            ChangeCD();
            SetSelfMovement();
            AddSelfBuffer();
            DelSelfBuffer();
            CalculateDuration();
            //AttackRangePreview();
            if (_regionJudgeTime == RegionJudgeTime.OnSkillActive
                ||  _regionJudgeTime == RegionJudgeTime.OnActionActive)
                ShowEarlyWarning();
            Doing();
        }

        public void End()
        {
            _curStage = SkillActionStage.ENDED;
        }


        #region 持续效果

        void DoingEvents()
        {
            for (int i = _skillEventList.Count - 1; i >= 0; i--)
            {
                var eventData = _skillEventList[i];
                if (eventData.delay <= _pastTime)
                {
                    _actHandler.OnEvent(_owner.actor, eventData);
                    _skillEventList.RemoveAt(i);
                }
            }
        }

        void Moving()
        {
            for (int i = _skillActionMovements.Count - 1; i >= 0; i--)
            {
                var movement = _skillActionMovements[i];
                if (movement.startTime < RealTime.time)
                {
                    _skillActionMovements.RemoveAt(i);
                    _owner.checkCrashWall = false;
                    ExecuteMovement(movement);
                }
            }
        }

        void ExecuteMovement(SkillActionMovement movement)
        {
            var moveEntity = CombatSystemTools.GetCreatureByID(movement.moveEntityID);
            if (moveEntity == null) return;
            var srcPosition = moveEntity.position;

            Vector3 targetPosition = movement.targetPosition;
            var dir = targetPosition - srcPosition;
			float maxMoveDistance = ACTSystem.ACTMathUtils.Abs(movement.maxMoveDistance);
            var dis = Mathf.Min(dir.magnitude, maxMoveDistance);
            targetPosition = srcPosition + dir.normalized * dis;
            moveEntity.moveManager.MoveByMovement(targetPosition, movement.baseSpeed, movement.accelerate, movement.percent);
            if (movement.upSpeed > 0)
            {
                if (moveEntity.actor != null) moveEntity.actor.actorController.Jump(movement.upSpeed, 0);
			}
			if (_hitFlyTargetDict.ContainsKey(moveEntity.id))
			{
                moveEntity.actorDeathController.MoveByMovement(targetPosition, movement.baseSpeed, movement.accelerate, false, false);
			}
		}
		
		void CheckRegionJudgeDelayTime()
		{
            if (_regionJudgeDelayTime == 0 || _regionJudgeDelayTime > RealTime.time) return;
            _regionJudgeDelayTime = 0;
            if (_owner.GetTransform() != null)
            {
                _ownerWorldMatrixOnFixTime = _owner.GetTransform().localToWorldMatrix;
            }
            var targetEntity = CombatSystemTools.GetCreatureByID(_mainTargetID);
            if (targetEntity != null && targetEntity.GetTransform() != null)
            {
                _targetWorldMatrixOnFixTime = targetEntity.GetTransform().localToWorldMatrix;
            }
            ResetActiveOriginMatrix();
            //AttackRangePreview();
            ShowEarlyWarning();
            _actHandler.OnFixTime(this._owner.actor);
        }

        void Judge()
        {
            if (_fireTime == 0 || _fireTime > RealTime.time) return;
            _fireTime = 0;
            ResetWorldMatrixOnJudge();
            DetectTargets();
            CalculateDamage();
            TriggerAction();
            AddBufferTarget();
            DelBuffTarget();
            CalcSuperArmor();
            SetEP();
            HandleDropItems();
            SpawnObject();
        }

        void ResetWorldMatrixOnJudge()
        {
            if (_owner.GetTransform() != null)
            {
                _ownerWorldMatrixOnJudge = _owner.GetTransform().localToWorldMatrix;
            }
            var targetEntity = CombatSystemTools.GetCreatureByID(_mainTargetID);
            if (targetEntity != null && targetEntity.GetTransform() != null)
            {
                _targetWorldMatrixOnJudge = targetEntity.GetTransform().localToWorldMatrix;
            } 
        }

        Matrix4x4 FixSrcMatrix(Matrix4x4 srcMatrix, Matrix4x4 fixMatrix)
        {
            var targetPosition = fixMatrix.position();
			var sourcePosition = srcMatrix.position();
			var direction = targetPosition - sourcePosition;
            var quaternion = Quaternion.identity;
            if (direction != Vector3.zero)
            {
                quaternion = Quaternion.LookRotation(direction.normalized);
            }
			var result = Matrix4x4.identity;
			result.SetTRS (sourcePosition + direction, quaternion, Vector3.one);
			return result;
        }

        void ResetFixOriginAdjust(bool fix = false)
        {
            var originAdjust = this.GetOriginAdjust();
            _fixOriginAdjust = originAdjust;
            if (fix && originAdjust.Count >= 8)
            {
                _fixOriginAdjust = new List<int>();
                for (int i = 4; i < 8; i++)
                { //截取后六个参数设置
                    _fixOriginAdjust.Add(originAdjust[i]);
                }
            }
            //if (_fixOriginAdjust.Count == 1)
        }

        Matrix4x4 GetRegionJudgeMatrix(RegionJudgeTime regionJudgeTime, OriginType originType)
        {
            //LoggerHelper.Error("[SkillAction:GetRegionJudgeMatrix]=>11_________regionJudgeTime: " + regionJudgeTime + ",originType: " + originType + ",skillID:  " + skillID);
            if (this._owner.GetTransform() == null)
            {
                return ownerWorldMatrixOnSkillActive;
            }
            Matrix4x4 curOwnerMatrix = this._owner.GetTransform().localToWorldMatrix;
            Matrix4x4 srcMatrix = curOwnerMatrix;
            Matrix4x4 tarMatrix;
            ResetFixOriginAdjust();
            switch (regionJudgeTime)
            {
                case RegionJudgeTime.OnSkillActive:
                    srcMatrix = ownerWorldMatrixOnSkillActive;
                    break;
                case RegionJudgeTime.OnActionActive:
                    srcMatrix = _ownerWorldMatrixOnActive;
                    if (originType == OriginType.Target)
                    {
                        tarMatrix = _targetWorldMatrixOnActive;
                        srcMatrix = FixSrcMatrix(srcMatrix, tarMatrix);
                    }
                    else if (originType == OriginType.Auto)
                    {
                        if (!_haveTarget)
                        {
                            ResetFixOriginAdjust(true);
                        }
                        else 
                        {
                            tarMatrix = _targetWorldMatrixOnActive;
                            srcMatrix = FixSrcMatrix(srcMatrix, tarMatrix);
                        } 
                    }
                    else if (originType == OriginType.Custom)
                    {
                        tarMatrix = _targetWorldMatrixOnActive;
                        srcMatrix = FixSrcMatrix(srcMatrix, tarMatrix);
                    }
                    
                    break;
                case RegionJudgeTime.OnActionJudge:
                    srcMatrix = this._ownerWorldMatrixOnJudge;
                    if (originType == OriginType.Target)
                    {
                        tarMatrix = this._targetWorldMatrixOnJudge;
                        srcMatrix = FixSrcMatrix(srcMatrix, tarMatrix);
                    }
                    else if (originType == OriginType.Auto)
                    {
                        if (!_haveTarget)
                        {
                            ResetFixOriginAdjust(true);
                        }
                        else
                        {
                            tarMatrix = _targetWorldMatrixOnJudge;
                            srcMatrix = FixSrcMatrix(srcMatrix, tarMatrix);
                        } 
                    }
                    break;
                case RegionJudgeTime.OnFixTime:
                    srcMatrix = this._ownerWorldMatrixOnFixTime;
                    if (originType == OriginType.Target)
                    { 
                        tarMatrix = this._targetWorldMatrixOnFixTime;
                        srcMatrix = FixSrcMatrix(srcMatrix, tarMatrix);
                    }
                    else if (originType == OriginType.Auto)
                    {
                        if (!_haveTarget)
                        {
                            ResetFixOriginAdjust(true);
                        }
                        else
                        {
                            tarMatrix = _targetWorldMatrixOnFixTime;
                            srcMatrix = FixSrcMatrix(srcMatrix, tarMatrix);
                        } 
                    }
                    else if (originType == OriginType.Custom)
                    {
                        //LoggerHelper.Error("[SkillAction:GetRegionJudgeMatrix]=>//////////////////////////////////////////");
                    }
                    break;
            }
            return srcMatrix;
        }

        #endregion 持续效果

        #region 瞬间效果

        void ChangeCD()
        {
            var changeData = this.GetChgSpellCD();
            if (changeData == null) return;
            foreach (var kv in changeData)
            {
                var skillGroup = kv.Key;
                var time = kv.Value;
                this._owner.skillManager.ChangeSkillGroupCD(skillGroup, time);
            }
        }

        List<float> FixMoveArgs(MovementType movementType, List<float> srcMoveArgs, bool forTargetMove = false)
        {
            int argNum = 8;
            List<float> curMoveArgs = srcMoveArgs;
            if (MovementType.AccordingAuto == movementType)
            {
                var target = CombatSystemTools.GetCreatureByID(this._mainTargetID);
                if (target == null && srcMoveArgs.Count >= argNum * 2)
                {
                    curMoveArgs = new List<float>();
                    for (int i = argNum; i < argNum * 2; i++)
                    { //截取后六个参数设置
                        curMoveArgs.Add(srcMoveArgs[i]);
                    }
                }
            }
            else if (forTargetMove)
            {
                var target = CombatSystemTools.GetCreatureByID(this._mainTargetID);
                if (target != null && target.IsOnAir() && srcMoveArgs.Count >= argNum * 2)
                {
                    curMoveArgs = new List<float>();
                    for (int i = argNum; i < argNum * 2; i++)
                    { //截取后六个参数设置
                        curMoveArgs.Add(srcMoveArgs[i]);
                    }
                }
            }
            return curMoveArgs;
        }

        void SetSelfMovement()
        {
            MovementType moveType = (MovementType)this.GetSelfMovType();
            if (moveType == MovementType.None) return;
            var moveArgs = this.GetSelfMovArg();
            List<float> curMoveArgs = FixMoveArgs(moveType, moveArgs);
            Matrix4x4 moveTarget = Matrix4x4.identity;
            Matrix4x4 moveSource = _ownerWorldMatrixOnActive;
            var mainTargetEntity = CombatSystemTools.GetCreatureByID(this._mainTargetID);
            switch(moveType){
                case MovementType.AccordingSelf:
                    moveTarget = moveSource;
                    break;
                case MovementType.AccordingTarget:
                    if (mainTargetEntity != null && mainTargetEntity.GetTransform() != null) moveTarget = mainTargetEntity.GetTransform().localToWorldMatrix;
                    break;
                case MovementType.AccordingAuto:
                    if (mainTargetEntity == null)
                    {
                        moveTarget = moveSource;
                    }
                    else {
                        if (mainTargetEntity != null && mainTargetEntity.GetTransform() != null) moveTarget = mainTargetEntity.GetTransform().localToWorldMatrix;
                    }
                    break;
                case MovementType.AccordingTargetProjection:
                    
                    break;
                case MovementType.AccordingBackInTime:
                    break;
                default:
                    return;
            }

            if (moveTarget == Matrix4x4.identity) return;
            var movement = GetActionMovement(_owner, moveSource, moveTarget, moveType, curMoveArgs, true);
            _skillActionMovements.Add(movement);
        }

        Vector3 FixTargetPosition(int axisType, Vector3 oldPosition)
        { 
            Vector3 result = oldPosition;
            switch (axisType)
            {
                case AxisType.StandardAxis:
                    result = oldPosition;
                    break;
                case AxisType.StandardAxisAtSameHeight:
                    result.y = _owner.position.y;
                    break;
                case AxisType.StandardAxisAtProjection:
                    result.y = CombatSystemTools.GetGroundPosition(_owner.position).y;
                    break;
                case AxisType.RelativeAxis:
                    result = oldPosition;
                    break;
            }
            return result;
        }

        SkillActionMovement GetActionMovement(EntityCreature moveEntity, Matrix4x4 moveSource, Matrix4x4 moveTarget, MovementType moveType, List<float> curMoveArgs, bool notifyForwardDirection = false)
        {
            SkillActionMovement movement = new SkillActionMovement();
            try
            {
                int idx = 0;
                int axisType = (int)curMoveArgs[idx++];
                movement.offsetX = curMoveArgs[idx++] * 0.01f;
                movement.offsetY = curMoveArgs[idx++] * 0.01f;
                movement.offsetH = curMoveArgs[idx++] * 0.01f;
                var delay = curMoveArgs[idx++] * 0.001f;
                movement.startTime = RealTime.time + delay;
                movement.maxMoveDistance = curMoveArgs[idx++] * 0.01f;
                movement.baseSpeed = curMoveArgs[idx++] * 0.01f;
                float accelerate = curMoveArgs[idx++];
                if (ACTSystem.ACTMathUtils.Abs(accelerate) < 100000)
                {
                    movement.accelerate = accelerate * 0.01f;
                    movement.percent = false;
                }
                else {
                    movement.accelerate = accelerate%100000 * 0.01f;
                    movement.percent = true;
                }
                movement.movementType = moveType;
                movement.moveEntityID = moveEntity.id;
                Vector3 targetPos = moveTarget.position();
                targetPos = FixTargetPosition(axisType, targetPos);
                Vector3 sourcePos = moveSource.position();
                if (moveSource == moveTarget)
                {
                    Matrix4x4 m1 = Matrix4x4.identity;
                    m1.SetColumn(3, new Vector4(movement.offsetY, movement.offsetH, movement.offsetX, 1));
                    moveSource = moveSource * m1;
                    movement.targetPosition = moveSource.position();
                }
                else
                {
                    var direction = (targetPos - sourcePos).normalized;
                    if (axisType != AxisType.RelativeAxis)
                    {
                        Vector3 tempTargetPos = targetPos;
                        tempTargetPos.y = sourcePos.y;
                        direction = (tempTargetPos - sourcePos).normalized;
                    }
                    Matrix4x4 mm = Matrix4x4.identity;
                    mm.SetTRS(targetPos, Quaternion.LookRotation(direction), Vector3.one);
                    var fontDriection = mm.forward().normalized * movement.offsetX;
                    var rightDirection = mm.right().normalized * movement.offsetY;
                    var upDirection = mm.upwards().normalized * movement.offsetH;
                    var offset = fontDriection + rightDirection + upDirection;
                    movement.targetPosition = new Vector3(targetPos.x + offset.x, targetPos.y + offset.y, targetPos.z + offset.z);
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("Movement Error: in skillAction:" + this.actionID + ":" + ex.Message);
            }
            return movement;
        }

        void AddSelfBuffer()
        {
            var buffData = this.GetAddBuffSelf();
            if (buffData == null) return;
            var en = buffData.GetEnumerator();
            while (en.MoveNext())
            {
                int buffID = en.Current.Key;
                var rate = en.Current.Value[0] * 0.0001f;
                var duration = en.Current.Value[1];
                if (RandomUtils.GetRandomFloat(0f, 1f) < rate)
                {
                    _owner.bufferManager.AddBuffer(buffID, duration, true);
                }
            }
        }

        void DelSelfBuffer()
        {
            var buffData = this.GetDelBuffSelf();
            if (buffData == null) return;
            var en = buffData.GetEnumerator();
            while (en.MoveNext())
            {
                int buffID = en.Current.Key;
                var rate = en.Current.Value * 0.0001f;
                if (RandomUtils.GetRandomFloat(0f, 1f) < rate)
                {
                    _owner.bufferManager.RemoveBuffer(buffID);
                }
            }
        }

        static List<uint> _filterList = new List<uint>();
        static List<uint> _candidateList = new List<uint>();
        void DetectTargets()
        {           
            //找目标
            _targetIDList.Clear();
            var targetType = this.GetTargetTypes();
            var originType = (OriginType)this.GetOriginType();
            TargetRangeType targetRangeType = (TargetRangeType)this.GetAttackRegionType();
            List<int> targetRangeParam = this.GetAttackRegionArg();
            if (targetRangeParam.Count > 0) targetRangeParam.Add(200);//临时
            List<int> targetFilterOrders = this.GetTargetFilterOrders();
            Dictionary<int, List<int>> targetFilterArgs = this.GetTargetFilterArgs();
            var targetMinCount = this.GetTargetMinCount();
            var targetMaxCount = this.GetTargetMaxCount();
            _judgePointMatrix = GetRegionJudgeMatrix(_regionJudgeTime, originType);
            EntityFilter.GetEntitiesByRangeType(_filterList, _judgePointMatrix, targetRangeType, targetRangeParam, _fixOriginAdjust, EntityFilterType.SKILL_ACTION_FILTER);
            TargetFilter.GetCreaturesByTargetType(_candidateList, _owner, _filterList, targetType, assignTargetID);
            TargetFilter.FilterTarget(_targetIDList, _owner, _candidateList, targetFilterOrders, targetFilterArgs, targetMinCount, targetMaxCount);
            TargetFilter.FilterTargetWithShieldAgainst(_targetIDList, _owner);
            _actHandler.actionJudgeOrigin = _judgePointMatrix.position();
            _actHandler.OnJudge(this._owner.actor);
        }

        void CalculateDamage()
        {
            _hitFlyTargetDict.Clear();
            for (int i = 0; i < _targetIDList.Count; i++)
            {
                uint entityID = _targetIDList[i];
                EntityCreature target = MogoWorld.GetEntity(entityID) as EntityCreature;
                if (target != null)
                {
                    SkillDamage result;
                    if (CheckTargetHasSelectedBossPart(target))
                    {
                        result = CalculateDamageByBossPart(target, _targetIDList.Count);
                    }
                    else
                    {
                        result = SkillCalculate.CalculateDamage(this._owner, target, this.skillID, this, _targetIDList.Count);
                    }
                    if (result.mode == 0)   //不计算伤害
                    {
                        return;
                    }

                    _actHandler.OnTargetBeHit(_owner.actor, target, delegate()
					{
						if (result.value > 0)
						{
							TargetBeHit(target);
                            if (_owner.IsPlayer())
                            {
                                PlayerEventManager.GetInstance().OnHitAndDamageOther();
                            }
						}
                        bool isPlayer = target == EntityPlayer.Player;

                        AttackType attackType = CombatSystemTools.DamageMode2AttackType(result.mode);
                        if (CombatSystemTools.CanShowDamage(this._owner, target) && !(attackType == AttackType.ATTACK_HIT && result.value == 0))
                        {
                            if (target.actor != null)
                            {
                                PerformaceManager.GetInstance().CreateDamageNumber(target.actor.position, -(int)result.value, (int)attackType, isPlayer);
                                if (isPlayer)
                                {
                                    PlayerEventManager.GetInstance().BeHit();
                                }
                            }
                        }

                        var oldHp = target.cur_hp;
                        Int32 cur_hp = target.cur_hp < result.value ? 0 : target.cur_hp - result.value;
                        target.OnChangeHp(_owner.id, oldHp, cur_hp, (int)attackType);
                        MogoWorld.SynEntityAttr(target, EntityPropertyDefine.cur_hp, cur_hp);

                        if (cur_hp == 0)
                        {
                            HandleTargetBeHitFlyOnDeath(target);
                            target.stateManager.SetState(state_config.AVATAR_STATE_DEATH, true);

                            //临时加的删除怪物逻辑，实际怪物应由刷怪行为回收
                            if (target.entityType == EntityConfig.ENTITY_TYPE_NAME_MONSTER)
                            {
                                GameLoader.Utils.Timer.TimerHeap.AddTimer(0, 1000, () =>
                                {
                                    MogoWorld.DestroyEntity(target.id);
                                });
                            }
                            
                            /*
                            if (target is EntityDummy)
                            {
                                (target as EntityDummy).BeKilledBy(this._owner);
                            }*/
                        }
                    });
                }
            }
        }

        private void HandleTargetBeHitFlyOnDeath(EntityCreature target)
        {
            bool needToBeHitFly = CheckTargetNeedToBeHitFly(target);
            if (needToBeHitFly)
            {
                hit_fly_params hitFlyParams = hit_fly_params_helper.GetRandomHitFlyParams();
                target.actorDeathController.flyingTime = hitFlyParams.__duration * 0.001f;
                hitFlyParams.__target_mov_type = 2;
                hitFlyParams.__target_mov_arg = "3,1000,0,0,0,1000,3000,-10030";
                _hitFlyTargetDict.Add(target.id, hitFlyParams);
            }
            else {
            }
        }

        private bool CheckTargetNeedToBeHitFly(EntityCreature target)
        {
            if (_owner.entityType != EntityConfig.ENTITY_TYPE_NAME_PLAYER_AVATAR) return false;
            return false;
            if (!target.bufferManager.ContainsBuffer(hit_fly_params_helper.GetHitFlyHpBuffID()))
            {
                return false;
            }
            return target.bufferManager.CanAddBuffer(hit_fly_params_helper.GetHitFlyBuffID());
        }

        private bool CheckTargetHasSelectedBossPart(EntityCreature target)
        {
            /*ari
            if (!(target.entityType == EntityConfig.ENTITY_TYPE_MONSTER))
            {
                return false;
            }
            EntityMonsterBase targetMonster = target as EntityMonsterBase;
            return targetMonster.partManager.CurrentSelectedSubject != null;
             */
            return false;
        }

        private SkillDamage CalculateDamageByBossPart(EntityCreature target, int targetCnt)
        {
            /*ari
            EntityMonsterBase targetMonster = target as EntityMonsterBase;
            SkillDamage result = targetMonster.partManager.CalculateDamage(this._owner, this.skillID, this, targetCnt);
            return result;
             */
            return new SkillDamage();
        }

        void TargetBeHit(EntityCreature target)
        {
            if (target.actor != null)
            {
                //Debug.LogError("TargetBeHit:" + target.id);
                if (CombatSystemTools.CanShowDamage(this._owner, target))
                {
                    var hlSetting = global_params_helper.GetHighLightSetting();
                    target.actor.equipController.HighLight(hlSetting[0], hlSetting[1]);
                }
                target.OnHit();
            }
        }

        void TriggerAction()
        {
			if (_targetIDList.Count > 0 && this.GetFireAction() != null) //受击目标数大于0就代表可以触发新行为
            {
                foreach (var kv in this.GetFireAction())
                {
                    if (kv.Value >= RandomUtils.GetRandomFloat(0f, 1f))
                    {
                        this._owner.skillManager.AddSkillActionToSkillSubject(this.skillID, kv.Key, ownerWorldMatrixOnSkillActive, _mainTargetID);
                    }
                }
            }
        }

        void AddBufferTarget()
        {
            if (_targetIDList.Count == 0 || this.GetAddBuffTarget().Count == 0) return;
            var data = this.GetAddBuffTarget();
            var en = data.GetEnumerator();
            while (en.MoveNext())
            {
                var buffid = en.Current.Key;
                var rate = en.Current.Value[0] * 0.0001f;
                var duration = en.Current.Value[1];
                if (RandomUtils.GetRandomFloat(0f, 1f) > rate) continue;
                for (int i = 0; i < _targetIDList.Count; i++)
                {
                    var target = CombatSystemTools.GetCreatureByID(_targetIDList[i]);
                    if (target != null)
                    {
                        if (target.stateManager.InState(state_config.AVATAR_STATE_DEATH)) continue;
                        target.bufferManager.AddBuffer(buffid, duration, true);
                    }
                }
            }       
        }

        void DelBuffTarget()
        {
            if (_targetIDList.Count == 0 || this.GetDelBuffTarget().Count == 0) return;
            var data = this.GetDelBuffTarget();
            var en = data.GetEnumerator();
            while (en.MoveNext())
            {
                var buffid = en.Current.Key;
                var rate = en.Current.Value * 0.0001f;
                for (int i = 0; i < _targetIDList.Count; i++)
                {
                    var target = CombatSystemTools.GetCreatureByID(_targetIDList[i]);
                    if (target.stateManager.InState(state_config.AVATAR_STATE_DEATH)) continue;
                    if (RandomUtils.GetRandomFloat(0f, 1f) > rate) continue;
                    if (target != null)
                    {
                        target.bufferManager.RemoveBuffer(buffid);
                    }
                }
            }       
        }
        
        void AddBufferSelf()
        {
            if (this.GetAddBuffSelf().Count <= 0) return;
            var data = this.GetAddBuffSelf();
            var en = data.GetEnumerator();
            while (en.MoveNext())
            {
                var buffid = en.Current.Key;
                var rate = en.Current.Value[0] * 0.0001f;
                var duration = en.Current.Value[1];
                if (RandomUtils.GetRandomFloat(0f, 1f) <= rate)
                {
                    _owner.bufferManager.AddBuffer(buffid, duration, true);
                }
            } 
        }

        void DelBufferSelf()
        {
            if (this.GetDelBuffSelf() == null) return;
            var data = this.GetDelBuffSelf();
            var en = data.GetEnumerator();
            while (en.MoveNext())
            {
                var buffid = en.Current.Key;
                var rate = en.Current.Value * 0.0001f;
                if (RandomUtils.GetRandomFloat(0f, 1f) <= rate)
                {
                    _owner.bufferManager.RemoveBuffer(buffid);
                }
            } 
        }

        void SetTargetMovement(EntityCreature moveEntity)
        {
            MovementType moveType = (MovementType)GetTargetMovType(moveEntity.id);
            if (moveType == MovementType.None) return;
            var moveArgs = GetTargetMovArg(moveEntity.id);
            List<float> curMoveArgs = FixMoveArgs(moveType, moveArgs, true);
            Matrix4x4 moveTarget = Matrix4x4.identity;
            Matrix4x4 moveSource = _owner.GetTransform().localToWorldMatrix;
            //LoggerHelper.Error("[SkillAction:SetTargetMovement]=>1_______________________moveType:  " + moveType + ",_owner:    " + _owner);
            switch (moveType)
            {
                case MovementType.None:
                    return;
                case MovementType.AccordingSelf:
                    moveTarget = moveSource;
                    break;
                case MovementType.AccordingTarget:
                    moveTarget = moveEntity.GetTransform().localToWorldMatrix;
                    moveEntity.checkCrashWall = true;
                    break;
                case MovementType.AccordingJudgePoint:
                    moveSource = _judgePointMatrix;
                    moveTarget = _judgePointMatrix;
                    break;
                case MovementType.AccordingJudgeTarget:
                    moveSource = _judgePointMatrix;
                    moveTarget = moveEntity.GetTransform().localToWorldMatrix;
                    break;
                default:
                    return;
            }
            if (moveTarget == Matrix4x4.identity) return;
            var movement = GetActionMovement(moveEntity, moveSource, moveTarget, moveType, curMoveArgs);
            var dir = movement.targetPosition - moveEntity.position;
            moveEntity.actor.FaceTo(moveEntity.position - dir);
            this.ExecuteMovement(movement);
        }

        int GetTargetMovType(uint id)
        {
            if (_hitFlyTargetDict.ContainsKey(id))
            {
                return _hitFlyTargetDict[id].__target_mov_type;
            }
            return this.GetTargetMovType();
        }

        List<float> GetTargetMovArg(uint id)
        {
            if (_hitFlyTargetDict.ContainsKey(id))
            {
                return CombatDataTools.ParseListFloat(_hitFlyTargetDict[id].__target_mov_arg);
            }
            return this.GetTargetMovArg();
        }

        void AddBuffersToCreature(EntityCreature creature, Dictionary<int, List<float>> buffs)
        {
            if (buffs == null || buffs.Count == 0) return;
            var en = buffs.GetEnumerator();
            while (en.MoveNext())
            {
                var buffid = en.Current.Key;
                var rate = en.Current.Value[0] * 0.0001f;
                var duration = en.Current.Value[1];
                if (RandomUtils.GetRandomFloat(0f, 1f) <= rate)
                {
                    creature.bufferManager.AddBuffer(buffid, duration, true);
                }
            }
        }

        void CalcSuperArmor() //霸体
        {
            int attackerLevel = this.GetSuperArmorDestroyLevel();
            var targetSuperArmorBehitBuff = this.GetTargetSuperArmorBehitBuff();
            var targetBehitBuff = this.GetTargetBehitBuff();
            if (attackerLevel < 1)
            {
                attackerLevel = 1;
            }
            for (int i = 0; i < _targetIDList.Count; i++)
            {
                var entID = _targetIDList[i];
                //if (!_dmgTargetIDList.Contains(entID)) continue;
                var target = CombatSystemTools.GetCreatureByID(entID);
                int hiterLevel = target.superArmorLevel;
                if (attackerLevel > hiterLevel)
                {
                    AddBuffersToCreature(target, targetBehitBuff);
                    SetTargetMovPreBuff(target);
                }
                else
                {
                    AddBuffersToCreature(target, targetSuperArmorBehitBuff);
                }
            }
        }

        void SetTargetMovPreBuff(EntityCreature target)
        {
            var targetMovPreBuff = this.GetTargetMovPreBuff();

            bool isHitFlyTarget = false;
            if (target != null && target.actor != null)
            {
                var movePreBuff = targetMovPreBuff;
                if (_hitFlyTargetDict.ContainsKey(target.id))
                {
                    //死亡击飞时，用击飞的BUFF代替当前的受击BUFF
                    movePreBuff = hit_fly_params_helper.GetHitFlyTargetMovPreBuff();
                    isHitFlyTarget = true;
                }
                if (!isHitFlyTarget && target.stateManager.InState(state_config.AVATAR_STATE_DEATH))
                    return;
                if (isHitFlyTarget) { //1004 临时加
                    target.actor.FaceTo(_owner.position);
                    SetTargetMovement(target);
                    return;
                }
                float rateMax = 1f;
                foreach (var pair in movePreBuff)
                {
                    var buffid = pair.Key;
                    var rate = pair.Value[0] * 0.0001f;
                    var duration = pair.Value[1];
                    if (RandomUtils.GetRandomFloat(0f, rateMax) <= rate)
                    {
                        if (target.bufferManager.AddBuffer(buffid, duration, true))
                        {
                            target.actor.FaceTo(_owner.position);
                            if (pair.Value.Count > 2) //
                            {
                                SetTargetMovement(target);
                            }
                        }
                        break;
                    }
                    rateMax -= rate;
                }
            }
        }

        protected void SetEP()
        {
            /*
            if (_targetIDList.Count > 0) //受击目标数大于0就代表可以触发新行为
            {
                var usecosts = this.GetFireCosts();
                if (_owner is EntityAvatar && usecosts.ContainsKey(public_config.ITEM_SPECIAL_SPELL_ENERGY))
                {
                    int oldEp = _owner.cur_ep;
                    Int16 cur_ep = (Int16)(_owner.cur_ep - usecosts[public_config.ITEM_SPECIAL_SPELL_ENERGY]);
                    cur_ep = (Int16)Mathf.Min(_owner.max_ep, Mathf.Max(0, cur_ep));
                    MogoWorld.SynEntityAttr(_owner, EntityPropertyDefine.cur_ep, cur_ep);
                    if (_owner == EntityPlayerLuaBase.Player)
                    {
                        EventDispatcher.TriggerEvent<int, int>(BattleUIEvents.ADD_EP, oldEp, cur_ep);
                    }
                }
            }*/
        }

        private void HandleDropItems()
        {
            /*
            if (_targetIDList.Count == 0) return;
            var fireDropItem = this.GetFireDropItem();
            if (fireDropItem.Count == 0)
            {
                return;
            }
            var en = fireDropItem.GetEnumerator();
            while (en.MoveNext())
            {
                HandleSingleDropItem(en.Current.Key, en.Current.Value);
            }*/
        }

        private void HandleSingleDropItem(int id, List<float> args)
        {
            var skillDropItem = new SkillDropItem(id, args);
            if (!skillDropItem.CanDrop())
            {
                //LoggerHelper.Info(string.Format("处理SkillAction为{0}的掉落id为{1}的掉落物，概率计算不掉落", this.actionID, id));
                return;
            }

            Vector3 originalPosition = GetOriginalPosition(skillDropItem.originalPointType);
            skillDropItem.SetOriginalPosition(originalPosition);
            List<Vector2> randomPositionList = skillDropItem.GetRandomPositionList(_owner.actor.modifyLayer);
            //LoggerHelper.Info(string.Format("处理SkillAction为{0}的掉落id为{1}的掉落物，掉落{2}个", this.actionID, id, randomPositionList.Count));
            // Vector2 position;//为排除警告，注释掉
            /*
            for (int i = 0; i < randomPositionList.Count; ++i)
            {
                position = randomPositionList[i];
                MogoEngine.MogoWorld.CreateEntity(GameMain.EntityConfig.ENTITY_TYPE_NAME_DROP_ITEM, (entity) =>
                {
                    var dropItem = entity as GameMain.EntityDropItem;
                    dropItem.type_id = (UInt32)id;
                    dropItem.owner_id = (UInt32)_owner.id;
                    if (_owner is EntityAvatar)
                    {
                        dropItem.owner_type = public_config.ENTITY_TYPE_AVATAR;
                    }
                    else if (_owner is EntityMonsterBase)
                    {
                        dropItem.owner_type = public_config.ENTITY_TYPE_MONSTER;
                    }
                    dropItem.owner_pve_camp = (byte)_owner.GetCampPveType();
                    dropItem.owner_pvp_camp = (byte)_owner.GetCampPvpType();
                    dropItem.pickup_type = (byte)skillDropItem.pickType;
                    dropItem.isServerItem = false;
                    dropItem.duration = skillDropItem.duration;
                    dropItem.SetPosition(new Vector3(position.x, originalPosition.y, position.y));
                });
            }*/
        }

        private void SpawnObject()
        {
            if (skillActionData.spawnType.Count == 0)
            {
                return;
            }
            if (skillActionData.spawnType.ContainsKey((int)SpawnType.ThrowObject))
            {
                int id = skillActionData.spawnType[(int)SpawnType.ThrowObject];
                ThrowObjectData throwObjectData = CombatLogicObjectPool.GetThrowObjectData(id);
                EntityCreature objectEntity = MogoEngine.Mgrs.EntityMgr.Instance.CreateClientEntity("ThrowObject", (entity) => {
                    EntityCreature entityCreature = (entity as EntityCreature);
                    entityCreature.spawnObjectId = id;
                }) as EntityCreature;
                objectEntity.position = _activeOriginMatrix.position();
                objectEntity.skillManager.SetLearnedSkillList(throwObjectData.spells);
                objectEntity.spawnEntityFace = _owner.actor.forward;
                objectEntity.clientCamp = _owner.clientCamp;
                objectEntity.castEntityId = _owner.id;
            }
            if (skillActionData.spawnType.ContainsKey((int)SpawnType.Trap))
            {
                int id = skillActionData.spawnType[(int)SpawnType.Trap];
                TrapData trapData = CombatLogicObjectPool.GetTrapObjectData(id);
                EntityCreature objectEntity = MogoEngine.Mgrs.EntityMgr.Instance.CreateClientEntity("Trap", (entity) =>
                {
                    EntityCreature entityCreature = (entity as EntityCreature);
                    entityCreature.spawnObjectId = id;
                    //用来陷阱召唤新的陷阱情况
                    if (_owner.entityType == EntityConfig.ENTITY_TYPE_NAME_TRAP)
                    {
                        entityCreature.castEntityId = _owner.castEntityId;
                    }
                    else
                    {
                        entityCreature.castEntityId = _owner.id;
                    }
                    entityCreature.clientCamp = _owner.clientCamp;
                }) as EntityCreature;
                objectEntity.position = _activeOriginMatrix.position();
                objectEntity.skillManager.SetLearnedSkillList(trapData.spells);
                objectEntity.spawnEntityFace = _owner.actor.forward;
            }
        }

        Vector3 turnToFace = Vector3.zero;
        private void TurnToOnBegin()
        {
            turnToFace = _owner.face;
            turnToFace.y += this.GetFaceAdjust() * 0.5f;
            _owner.face = turnToFace;
        }

        private Vector3 GetOriginalPosition(SkillDropItemOriginalPointType originalPointType)
        {
            if (originalPointType == SkillDropItemOriginalPointType.ATTACKER)
            {
                return _owner.position;
            }
            else
            {
                if (_targetIDList.Count == 0)
                {
                    return _owner.position;
                }
                int randomTargetIndex = RandomUtils.GetRandomInt(0, _targetIDList.Count);
                if (MogoWorld.GetEntity(_targetIDList[randomTargetIndex]) == null)
                {
                    return _owner.position;
                }
                else
                {
                    Vector3 direction = Vector3.zero;
                    EntityCreature creature = MogoWorld.GetEntity(_targetIDList[randomTargetIndex]) as EntityCreature;
                    if (originalPointType == SkillDropItemOriginalPointType.RANDOM_TARGET_EDGE)
                    {
                        direction = (_owner.position - creature.position).normalized * creature.hitRadius;
                    }
                    return creature.position + direction;
                }
            }
        }
        #endregion
    }
}
