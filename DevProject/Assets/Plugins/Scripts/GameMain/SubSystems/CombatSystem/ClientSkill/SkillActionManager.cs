﻿using GameMain;
using System.Collections.Generic;

namespace GameMain.CombatSystem
{
    public class SkillActionManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class SkillActionManager
    {
        private EntityCreature _owner;

        private List<SkillAction> _skillActions = new List<SkillAction>();
        private bool isLooping = false;
        private bool hasInitUpdate = false;

        public SkillActionManager() { }
        public void SetOwner(EntityCreature ower)
        {
            _owner = ower;
            if (!hasInitUpdate)
            {
                hasInitUpdate = true;
                MogoEngine.MogoWorld.RegisterUpdate<SkillActionManagerUpdateDelegate>("SkillActionManager.Update", Update);
            }
            isLooping = true;
        }

        public void Release()
        {
            isLooping = false;
            //MogoEngine.MogoWorld.UnregisterUpdate("SkillActionManager.Update", Update);
            _skillActions.Clear();
        }

        public void AddSkillAction(SkillAction skillAction)
        {
            AdjustTools.AdjustSkillAction(skillAction, _owner.skillManager.GetCurAdjustSkillActionIDList());
            _skillActions.Insert(0, skillAction);
            UpdateSkillActions();
        }

        private void Update()
        {
            if (!isLooping) return;
            UpdateSkillActions();
        }

        void UpdateSkillActions()
        {
            for (int i = _skillActions.Count - 1; i >= 0; i--)
            {
                var skillAction = _skillActions[i];
                var result = skillAction.Update();
                if (result == SkillActionStage.ENDED)
                {
                    _skillActions.RemoveAt(i);
                    CombatLogicObjectPool.ReleaseSkillAction(skillAction);
                }
            }
        }
    }
}
