﻿using Common.States;
using GameMain;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.CombatSystem
{
    public class SkillSubjectManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class SkillSubjectManager
    {
        private EntityCreature _owner;

        private List<SkillSubject> _skillSubjects = new List<SkillSubject>();

        private SkillActionManager _skillActionManager;
        private bool isLooping = false;
        private bool hasInitUpdate = false;

        public SkillSubjectManager() { }
        public void SetOwner(EntityCreature ower)
        {
            _owner = ower;
            if (!hasInitUpdate)
            {
                hasInitUpdate = true;
                MogoEngine.MogoWorld.RegisterUpdate<SkillSubjectManagerUpdateDelegate>("SkillSubjectManager,Update", Update);
            }
            if (_skillActionManager == null)
            {
                _skillActionManager = CreatureManagerObjectPool.CreateSkillActionManager(this._owner);
            }
            else
            {
                _skillActionManager.SetOwner(this._owner);
            }
            isLooping = true;
        }

        public void Release()
        {
            isLooping = false;
            //MogoEngine.MogoWorld.UnregisterUpdate("SkillSubjectManager,Update", Update);
            _skillSubjects.Clear();
            _skillActionManager.Release();
        }

        public void AddSkill(int skillID, uint assignId = 0, int slot = 0)
        {
            var skillSubject = CombatLogicObjectPool.CreateSkillSubject(skillID, slot);
            AddSkillSubject(skillSubject, assignId);
        }

        private void AddSkillSubject(SkillSubject skillSubject, uint assignId = 0)
        {
            if (skillSubject.GetActionCutTime() > 0)
            {
                /*
                if (_owner is EntityAvatar)
                {
                    (_owner as EntityAvatar).rideManager.Dismount();
                }*/
                BreakCurSkill();
            }
            AdjustTools.AdjustSkill(skillSubject, _owner.skillManager.GetCurAdjustSkillIDList());
            skillSubject.SetOwner(_owner);
            skillSubject.SetSkillActionManager(_skillActionManager);
            skillSubject.assignTargetID = assignId;
            _skillSubjects.Add(skillSubject);
            skillSubject.Update();
        }

        public void ReleaseSkill(int skillID)
        {
            if (_skillSubjects == null) return;
            for (int i = 0; i < _skillSubjects.Count; i++)
            {
                var perSkillSubject = _skillSubjects[i];
                if (perSkillSubject.skillID == skillID)
                {
                    perSkillSubject.Release();
                }
            }
        }

        public void BreakCurSkill()
        {
            if (_skillSubjects == null) return;
            for (int i = 0; i < _skillSubjects.Count; i++)
            {
                var perSkillSubject = _skillSubjects[i];
                perSkillSubject.Break();
            }
        }

        private void Update()
        {
            if (!isLooping) return;
            UpdateSkillSubjects();
        }

        void UpdateSkillSubjects()
        {
            for (int i = _skillSubjects.Count - 1; i >= 0; i--)
            {
                var skillSubject = _skillSubjects[i];
                if (skillSubject.Update() == SkillState.ENDED)
                {
                    _skillSubjects.RemoveAt(i);
                    CombatLogicObjectPool.ReleaseSkillSubject(skillSubject);
                }
            }
        }

        public List<SkillSubject> GetCurSkillSubjects()
        {
            return _skillSubjects;
        }

        public void CreateSkillAction(int skillID, int skillActionID, Matrix4x4 ownerWorldMatrixOnSkillActive, uint mainTargetID)
        {
            var skillAction = CombatLogicObjectPool.CreateSkillAction(skillActionID);
            skillAction.SetOwner(_owner);
            skillAction.skillID = skillID;
            skillAction.ownerWorldMatrixOnSkillActive = ownerWorldMatrixOnSkillActive;
            skillAction.delay = 0;
            if (mainTargetID != 0)
            {
                skillAction.mainTargetID = mainTargetID;
            }
            _skillActionManager.AddSkillAction(skillAction);
        }


    }
}
