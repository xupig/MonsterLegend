﻿using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using MogoEngine.Events;
using GameLoader.Utils;
using Common.Events;
using Common.States;
using GameData;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using GameLoader.Utils.Timer;
using MogoEngine.RPC;

namespace GameMain
{
    public class BossSyncPosManagerUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class BossSyncPosManager
    {
        EntityCreature _owner;

        private int _syncPosSpaceFrame = 30; //属性同步间隔时间（单位：帧）。
        private float _syncPosNextTime = 0;
        private HashSet<byte> _destroyPartList;

        public BossSyncPosManager(EntityCreature owner)
        {
            _owner = owner;
        }

        public void OnBossEnterWorld()
        {
            _destroyPartList = new HashSet<byte>();
            MogoWorld.RegisterUpdate<BossSyncPosManagerUpdateDelegate>("BossSyncPosManager.Update", Update);
        }

        public void OnBossLeaveWorld()
        {
            MogoWorld.UnregisterUpdate("BossSyncPosManager.Update", Update);
            _destroyPartList.Clear();
        }

        public void OnDestroyPart(byte partID)
        {
            if (_destroyPartList.Contains(partID)) return;
            _destroyPartList.Add(partID);
        }

        public void Update()
        {
            _syncPosNextTime--;
            if (_syncPosNextTime <= 0)
            {
                _syncPosNextTime = _syncPosSpaceFrame;
                SyncPos();
            }
        }

        Vector3 position = Vector3.zero;
        byte partID = 0;
        public void SyncPos()
        {         
            if (_owner == null || _owner.actor == null)
            {
                return;
            }
            foreach (var item in _owner.actor.boneController.GetBeHitSlotDict())
            {
                partID = (byte)item.Key;
                if (_destroyPartList.Contains(partID)) continue;
                position = item.Value.position;
                System.Int32 x = (System.Int32)(position.x * RPCConfig.POSITION_SCALE);
                System.Int32 y = (System.Int32)(position.y * RPCConfig.POSITION_SCALE);
                System.Int32 z = (System.Int32)(position.z * RPCConfig.POSITION_SCALE);
                MogoEngine.RPC.ServerProxy.Instance.BossPartMove(_owner.id, partID, x, y, z);
            }
        }
    }
}
