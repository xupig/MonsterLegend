/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：PlayerActionManager
// 创建者：Matako
// 修改者列表：
// 创建日期：
// 模块描述：行为管理器
//==========================================*/

using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using MogoEngine.Events;
using GameLoader.Utils;
using Common.Events;
using Common.States;
using GameData;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using GameLoader.Utils.Timer;
using MogoEngine.RPC;

namespace GameMain.GlobalManager
{
    public class SyncPosManager
    {
        EntityCreature _owner;

        private uint _syncPosTimerID = 0;

        public SyncPosManager(EntityCreature owner) 
        {
            _owner = owner;
        }

        public void Release()
        {
            if (_syncPosTimerID > 0)
            {
                TimerHeap.DelTimer(_syncPosTimerID);
                _syncPosTimerID = 0;
            }
        }

        public void OnEnterFollower()
        {
            if (_syncPosTimerID == 0)
            {
                _syncPosTimerID = TimerHeap.AddTimer(1000, 300, SyncPos);
            }
        }

        public void OnLeaveFollower()
        {
            if (_syncPosTimerID > 0)
            {
                TimerHeap.DelTimer(_syncPosTimerID);
                _syncPosTimerID = 0;
            }
        }

        private Vector3 _prePos = new Vector3();
        public void SyncPos()
        {
            if (EntityPlayer.pauseSyncPos)//用于停所有坐标同步
                return;
            if (_owner.actor == null)
            {
                return;
            }
            Vector3 position = _owner.actor.position;
            if (ACTSystem.ACTMathUtils.Abs(position.x - _prePos.x) < 0.2f && ACTSystem.ACTMathUtils.Abs(position.z - _prePos.z) < 0.2f)
            {
                return;
            }
            _prePos.x = position.x;
            _prePos.y = position.y;
            _prePos.z = position.z;
            System.Int32 x = (System.Int32)(position.x * RPCConfig.POSITION_SCALE);
            System.Int32 y = (System.Int32)(position.y * RPCConfig.POSITION_SCALE);
            System.Int32 z = (System.Int32)(position.z * RPCConfig.POSITION_SCALE);
            Vector3 localEulerAngles = _owner.actor.localEulerAngles;
            byte eulerAnglesX = (byte)(localEulerAngles.x * 0.5f);
            byte eulerAnglesY = (byte)(localEulerAngles.y * 0.5f);
            byte eulerAnglesZ = (byte)(localEulerAngles.z * 0.5f);
            MogoEngine.RPC.ServerProxy.Instance.OthersMove(_owner.id, eulerAnglesX, eulerAnglesY, eulerAnglesZ, x, y, z);
        }
    }
}