﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using MogoEngine.RPC;
using GameMain;
using Common.States;
using GameLoader.Utils;
using Common.ClientConfig;
using MogoEngine;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Utils;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.ClientConfig;
using GameMain.CombatSystem;
using ACTSystem;

namespace GameMain.CombatSystem
{
    public class ThrowObjectManagerUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class ThrowObjectManager
    {
        private EntityCreature _owner;
        private ThrowObjectData _throwObjectData;
        float _endTime = 0;
        float _speed = 0;
        Dictionary<int, int> timeEffect;
        int touchEffect;
        float _startTime = 0;
        float _totalTime = 0;
        int _delayTime = 0;
        Vector3 _direction;
        Vector3 _startPos;
        HashSet<int> hasTriggerTime = new HashSet<int>();
        bool isCollider = false;
        RaycastHit raycastInfo;
        float _length = 0;
        int _terrainLayerValue = 1 << ACTLayerUtils.Terrain;
        int _wallLayerValue = 1 << ACTLayerUtils.Wall;
        Transform modelTran;
        private bool isEnd = false;
        public bool isLooping = false;
        private bool hasInitUpdate = false;

        public ThrowObjectManager() { }
        public void SetOwner(EntityCreature owner)
        {
            _owner = owner;
            if (!hasInitUpdate)
            {
                hasInitUpdate = true;
                MogoEngine.MogoWorld.RegisterUpdate<TrapManagerUpdateDelegate>("ThrowObjectManager.Update", Update);
            }
            isLooping = true;
            _throwObjectData = CombatLogicObjectPool.GetThrowObjectData(owner.spawnObjectId);
            InitData();
        }

        public void Release()
        {
            ClearData();
            //MogoEngine.MogoWorld.UnregisterUpdate("ThrowObjectManager.Update", Update);
        }

        public void ClearData()
        {
            isLooping = false;
            _owner = null;
            _throwObjectData = null;
            _endTime = 0;
            _speed = 0;
            timeEffect = null;
            touchEffect = 0;
            _startTime = 0;
            _totalTime = 0;
            _delayTime = 0;
            hasTriggerTime.Clear();
            isCollider = false;
            _length = 0;
            modelTran = null;
            isEnd = false;
            isLooping = false;
            hasInitUpdate = false;
        }

        private void InitData()
        {
            _startTime = RealTime.time;
            _totalTime = _throwObjectData.totalTime * 0.001f;
            _endTime = RealTime.time + _totalTime;
            _delayTime = _throwObjectData.delay_time;
            _speed = _throwObjectData.speed * 0.01f;
            _length = _speed * _totalTime;
            _direction = _owner.spawnEntityFace;
            modelTran = _owner.actor.transform.GetChild(0);
            _owner.actor.gameObject.GetComponent<CharacterController>().enabled = false;
            this.timeEffect = _throwObjectData.timeEffect;
            this.touchEffect = _throwObjectData.touchEffect;
            _startPos = _owner.actor.position;
            _startPos.y += 1;
            _owner.actor.position = _startPos;
            _owner.actor.onTriggerEnterCall = (collider) => { isCollider = true; };
            Vector3 newPos = _startPos + _direction * 1.5f;
            bool isRaycast = Physics.Raycast(newPos, _direction, out raycastInfo, _length, _wallLayerValue | _terrainLayerValue);
            if (isRaycast)
            {
                float time = raycastInfo.distance / _speed;
                _endTime = UnityPropUtils.realtimeSinceStartup + time;
            }
        }

        private void Update()
        {
            if (!isLooping) return;
            if (isEnd) return;
            var actor = _owner.actor;
            if (actor == null) return;
            actor.position = actor.position + UnityPropUtils.deltaTime * _speed * _direction;
            foreach (var item in timeEffect)
            {
                if (hasTriggerTime.Contains(item.Key))
                {
                    continue;
                }
                if (_startTime + item.Key * 0.001f < UnityPropUtils.realtimeSinceStartup)
                {
                    this._owner.skillManager.PlaySkillByPos(item.Value);
                    hasTriggerTime.Add(item.Key);
                }
            }
            if (isCollider || _endTime < UnityPropUtils.realtimeSinceStartup)
            {
                End();
                isEnd = true;
            }           
        }

        private void End()
        {
            _owner.skillManager.PlaySkillByPos(touchEffect);
            hasTriggerTime.Clear();
            isCollider = false;
            _owner.actor.onTriggerEnterCall = null;
            modelTran.gameObject.SetActive(false);
            TimerHeap.AddTimer((uint)_delayTime, 0, () =>
            {
                EntityMgr.Instance.DestroyEntity(_owner.id);
            });
        }
    }
}
