﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using MogoEngine.RPC;
using GameMain;
using Common.States;
using GameLoader.Utils;
using Common.ClientConfig;
using MogoEngine;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Utils;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.ClientConfig;
using GameMain.CombatSystem;

namespace GameMain.CombatSystem
{
    public class TriggerTargetData
    {
        public float startTime = 0;
        public HashSet<int> triggerSet = new HashSet<int>();
        public Dictionary<int, float> triggerDict = new Dictionary<int, float>();
    }

    public class TrapManagerUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class TrapManager
    {
        private EntityCreature _owner;
        private TrapData _trapData;
        private float _preTime = 0;
        private float _startTime = 0;
        private float _effectTime = 0;
        private float _endTime = 0;
        private float _updateTime = 0.1f;
        private int _enterTrapSkillPos;
        private int _endTrapSkillPos;
        private bool _hasTrigger = false;
        private float _radius;
        private float _height;
        private Transform modelTran;
        private Dictionary<uint, Entity> entities;
        private List<uint> entityIDs;
        private Dictionary<uint, TriggerTargetData> inTrapTimeDict = new Dictionary<uint, TriggerTargetData>();
        private HashSet<uint> inTrapSet = new HashSet<uint>();
        private HashSet<int> triggerSelfTimeSet = new HashSet<int>();
        private bool isLooping = false;
        private bool hasInitUpdate = false;

        public TrapManager() { }
        public void SetOwner(EntityCreature owner)
        {
            _owner = owner;
            if (!hasInitUpdate)
            {
                hasInitUpdate = true;
                MogoEngine.MogoWorld.RegisterUpdate<TrapManagerUpdateDelegate>("TrapManager.Update", Update);
            }
            isLooping = true;
            _trapData = CombatLogicObjectPool.GetTrapObjectData(owner.spawnObjectId);
            InitData();
        }

        public void InitData()
        {
            _startTime = RealTime.time;
            _effectTime = RealTime.time + _trapData.startTime * 0.001f;
            _endTime = _effectTime + (_trapData.duration + _trapData.delayTime) * 0.001f;
            _radius = _trapData.range * 0.01f;
            _height = _trapData.height * 0.01f;
            modelTran = _owner.actor.transform.GetChild(0);
            if (_trapData.triggerSelfDict.ContainsKey(-1))
            {
                _enterTrapSkillPos = _trapData.triggerSelfDict[-1];
            }
            else
            {
                _enterTrapSkillPos = -1;
            }
            if (_trapData.triggerSelfDict.ContainsKey(-2))
            {
                _endTrapSkillPos = _trapData.triggerSelfDict[-2];
            }
            else
            {
                _endTrapSkillPos = -1;
            }
            triggerSelfTimeSet.Add(-1);
            triggerSelfTimeSet.Add(-2);
            _preTime = RealTime.time;
            entities = MogoWorld.Entities;
            entityIDs = MogoWorld.EntityIDs;
        }

        public void Release()
        {
            ClearData();
            //MogoEngine.MogoWorld.UnregisterUpdate("TrapManager.Update", Update);
        }

        public void ClearData()
        {
            isLooping = false;
            _owner = null;
            _trapData = null;
            _preTime = 0;
            _startTime = 0;
            _effectTime = 0;
            _endTime = 0;
            _updateTime = 0.1f;
            _enterTrapSkillPos = 0;
            _endTrapSkillPos = 0;
            _hasTrigger = false;
            _radius = 0;
            _height = 0;
            modelTran = null;
            entities = null;
            entityIDs = null;
            inTrapTimeDict.Clear();
            inTrapSet.Clear();
            triggerSelfTimeSet.Clear();
            isBottom = false;
            isTop = false;
            selfV2 = Vector2.zero;
            targetV2 = Vector2.zero;
            selfPos = Vector3.zero;
            targetPos = Vector3.zero;
        }

        public void Update()
        {
            if (!isLooping) return;
            if (RealTime.time - _preTime < _updateTime)
            {
                return;
            }
            _preTime = RealTime.time;
            if (_effectTime > RealTime.time)
            {
                return;
            }
            TriggerSelf();
            if (_endTime < RealTime.time)
            {
                End();
            }
            for (int i = 0; i < entityIDs.Count; i++)
            {
                EntityCreature entity = entities[entityIDs[i]] as EntityCreature;
                if (entity == null) continue;
                if (entity.entityType == EntityConfig.ENTITY_TYPE_NAME_TRAP || entity.entityType == EntityConfig.ENTITY_TYPE_NAME_THROWOBJECT) 
                    continue;
                if (IsTarget(entity))
                {
                    if (IsInTrap(entity))
                    {
                        if (inTrapSet.Contains(entity.id))
                        {
                            InTrap(entity);
                        }
                        else
                        {
                            EnterTrap(entity);
                        }
                    }
                    else
                    {
                        if (inTrapSet.Contains(entity.id))
                        {
                            ExitTrap(entity);
                        }
                    }
                }
            }
        }

        bool IsTarget(EntityCreature entity)
        {
            if (entity.realCamp == _owner.realCamp && _trapData.targetTypeList.Contains((int)TriggerTrapType.Friend))
            {                
                return true;
            }
            if (_trapData.targetTypeList.Contains((int)TriggerTrapType.Caller) && entity.id == _owner.castEntityId)
            {
                return true;
            }
            if (entity.realCamp != _owner.realCamp && _trapData.targetTypeList.Contains((int)TriggerTrapType.Enemy))
            {
                return true;
            }
            return false;
        }

        private void TriggerSelf()
        {
            foreach (var item in _trapData.triggerSelfDict)
            {
                if (triggerSelfTimeSet.Contains(item.Key))
                {
                    continue;
                }
                if (_startTime + item.Key * 0.001f < RealTime.time)
                {
                    triggerSelfTimeSet.Add(item.Key);
                    _owner.skillManager.PlaySkill(item.Value);
                }
            }
        }

        private void TriggerTarget(EntityCreature entity)
        {
            TriggerTargetData data = inTrapTimeDict[entity.id];
            foreach (var item in _trapData.triggerTargetDict)
            {
                if (data.triggerSet.Contains(item.Key))
                {
                    continue;
                }
                if (data.triggerDict.ContainsKey(item.Key))
                {
                    if (data.triggerDict[item.Key] < RealTime.time)
                    {
                        data.triggerDict[item.Key] = RealTime.time + item.Value[1] * 0.001f;
                        _owner.skillManager.PlaySkill(item.Value[0], entity.id);
                    }
                }
                else
                {
                    if (data.startTime + item.Key * 0.001f < RealTime.time)
                    {
                        if (item.Value[1] == 0)
                        {
                            _owner.skillManager.PlaySkill(item.Value[0], entity.id);
                            data.triggerSet.Add(item.Key);
                        }
                        else
                        {
                            data.triggerDict.Add(item.Key, RealTime.time + item.Value[1] * 0.001f);
                            _owner.skillManager.PlaySkill(item.Value[0], entity.id);
                        }
                    }
                }
            }
        }

        bool isBottom = false;
        bool isTop = false;
        Vector2 selfV2 = Vector2.zero;
        Vector2 targetV2 = Vector2.zero;
        Vector3 selfPos = Vector3.zero;
        Vector3 targetPos = Vector3.zero;
        private bool IsInTrap(EntityCreature entity)
        {
            selfPos = _owner.position;
            targetPos = entity.position;
            isBottom = (selfPos.y - targetPos.y) >= 0 && (selfPos.y - targetPos.y) <= entity.hitHeight;
            isTop = (targetPos.y - selfPos.y) >= 0 && (targetPos.y - selfPos.y) <= _height;
            if (!isBottom && !isTop)
            {
                return false;
            }
            selfV2.x = selfPos.x;
            selfV2.y = selfPos.z;
            targetV2.x = targetPos.x;
            targetV2.y = targetPos.z;
            if (Vector2.Distance(selfV2, targetV2) < _radius + entity.hitRadius)
            {
                return true;
            }
            return false;
        }

        private void EnterTrap(EntityCreature entity)
        {
            if (!_hasTrigger)
            {
                _hasTrigger = true;
                if (_enterTrapSkillPos != -1)
                {
                    _owner.skillManager.PlaySkill(_enterTrapSkillPos);
                }
            }
            inTrapSet.Add(entity.id);
            TriggerTargetData data = new TriggerTargetData();
            data.startTime = RealTime.time;
            inTrapTimeDict.Add(entity.id, data);
            var en = _trapData.addBuffDict.GetEnumerator();
            while (en.MoveNext())
            {
                var buffid = en.Current.Key;
                var rate = en.Current.Value[0] * 0.0001f;
                var duration = en.Current.Value[1];
                if (RandomUtils.GetRandomFloat(0f, 1f) <= rate)
                {
                    entity.bufferManager.AddBuffer(buffid, duration, true);
                }
            }
            if (_trapData.delType == (int)TrapEndType.EnterEnd)
            {
                _endTime = RealTime.time + _trapData.delayTime * 0.001f;
            }
        }

        private void ExitTrap(EntityCreature entity)
        {
            inTrapSet.Remove(entity.id);
            inTrapTimeDict.Remove(entity.id);
            var en = _trapData.delBuffDict.GetEnumerator();
            while (en.MoveNext())
            {
                var buffid = en.Current.Key;
                var rate = en.Current.Value * 0.0001f;
                if (RandomUtils.GetRandomFloat(0f, 1f) <= rate)
                {
                    entity.bufferManager.RemoveBuffer(buffid);
                }
            }            
        }

        private void InTrap(EntityCreature entity)
        {
            if (inTrapTimeDict.ContainsKey(entity.id))
            {
                TriggerTarget(entity);
            }
        }

        public void End()
        {
            if (_endTrapSkillPos != -1)
            {
                _owner.skillManager.PlaySkill(_endTrapSkillPos);
            }
            modelTran.gameObject.SetActive(false);
            TimerHeap.AddTimer((uint)_trapData.delEntityDelayTime, 0, () =>
            {
                EntityMgr.Instance.DestroyEntity(_owner.id);
            });
        }
    }
}
