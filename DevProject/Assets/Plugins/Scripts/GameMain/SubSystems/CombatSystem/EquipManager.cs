﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;
using Common.Structs;
using GameData;
using GameLoader.Utils.Timer;
using Common.ClientConfig;
using MogoEngine.Events;
using Common.Events;
using Common.Data;
using MogoEngine;
using Common.ServerConfig;
using GameMain.ClientConfig;
using Common.ExtendTools;

namespace GameMain.CombatSystem
{
    public class EquipManager
    {
        private EntityCreature  _owner;
        private string _curEquipInfo = string.Empty;
        private AvatarModelData _curAvatarModelData = new AvatarModelData();
        private AvatarModelData _setAvatarModelData = new AvatarModelData();//存储当前需要的设置数据
        private bool _dirty = false;

        private int _fashionId = 0; //当前时装id(只对玩家, 若=0则穿装备，否则穿时装)

        public EquipManager() { }
        public void SetOwner(EntityCreature ower)
        {
            RemoveListeners();
            _owner = ower;
            _curAvatarModelData.vocation = this._owner.vocation;

            if (string.IsNullOrEmpty(_owner.facade))
            {
                //npc不换装的情况避免刷新一次
                if (_owner.entityType != EntityConfig.ENTITY_TYPE_NAME_NPC)
                {
                    ResetCurEquipDictByDefault();
                }
            }
            else 
            {
                ResetCurEquipDictByString(_owner.facade);
                RefreshAvatarInfo();
            }
            RefreshEquip();
            EntityPropertyManager.Instance.AddEventListener(this._owner, EntityPropertyDefine.facade, OnFacadeChange);
            /*ari EventDispatcher.AddEventListener<int>(SettingEvents.UPDATE_RENDER_QUALITY_SETTING, OnQualityChange); */
        }

        public void Release()
        {
            RemoveListeners(); 
        }

        private void RemoveListeners()
        {
            ClearData(); 
            if (_owner == null) return;
            //EventDispatcher.RemoveEventListener<int>(SettingEvents.UPDATE_RENDER_QUALITY_SETTING, OnQualityChange);
            EntityPropertyManager.Instance.RemoveEventListener(this._owner, EntityPropertyDefine.facade, OnFacadeChange);
        }
        

        private void ClearData()
        {
            _curEquipInfo = string.Empty;
            _curAvatarModelData.ClearData();
            _setAvatarModelData.ClearData();
            _dirty = false;
            _fashionId = 0; //当前时装id(只对玩家, 若=0则穿装备，否则穿时装)
            if (synTimerID != 0) 
            {
                TimerHeap.DelTimer(synTimerID);
                synTimerID = 0;
            }
        }


        private void OnQualityChange(int quality)
        {
            UpdateSettingType();
        }

        private void UpdateSettingType()
        {
            if (_owner.actor == null) return;
            _owner.actor.equipController.equipWing.UpdateSettingType();
        }

        void SetDefaultEquip()
        {
            if (_owner.actor == null) return;
            _owner.actor.equipController.equipCloth.defaultEquipID = role_data_helper.GetDefaultCloth((int)this._owner.vocation);
            _owner.actor.equipController.equipWeapon.defaultEquipID = role_data_helper.GetDefaultWeapon((int)this._owner.vocation);
        }

        void OnFacadeChange()
        {
            if (_owner.actor == null) return;
            if (_owner != EntityPlayer.Player)
            {
                RefreshAvatarEquip();
            }
        }
        
        public void RefreshEquip()
        {
            if (_owner.actor == null) return;
            if (_owner == EntityPlayer.Player || _owner == EntityPlayer.ShowPlayer || _owner.entityType == EntityConfig.ENTITY_TYPE_NAME_CREATE_ROLE_AVATAR)
            {
                RefreshPlayerEquip();
            }
            else
            {
                RefreshAvatarEquip();
            }
        }

        public void OnActorLoaded()
        {
            if (_owner.InState(state_config.AVATAR_STATE_TRANSFORM))
            {
                if (_owner.IsPlayer())
                {
                    RefreshEquip();
                }
            }
            else
            {
                ReloadEquip();
            }
        }

        public void ReloadCloth()
        {
            var clothInfo = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth);
            if (clothInfo.equipID > 0)
            {
                _owner.actor.equipController.equipCloth.PutOn(clothInfo.equipID, clothInfo.particle, clothInfo.flow, true, clothInfo.colorR, clothInfo.colorG, clothInfo.colorB);
            }
            else
            {
                _owner.actor.equipController.equipCloth.PutOn(0);
            }
        }

        public void ReloadHead()
        {
            var headInfo = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Head);
            if (headInfo == null) return;
            if (headInfo.equipID > 0)
            {
                _owner.actor.equipController.equipHead.PutOn(headInfo.equipID,headInfo.particle,headInfo.flow,true);
            }
            else
            {
                _owner.actor.equipController.equipHead.PutOn(0);
            }
        }

        public void ReloadWeapon()
        {
            var weaponInfo = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon);
            if (weaponInfo.equipID > 0)
            {
                _owner.actor.equipController.equipWeapon.PutOn(weaponInfo.equipID, weaponInfo.particle, weaponInfo.flow, true, weaponInfo.colorR, weaponInfo.colorG, weaponInfo.colorB);
            }
            else
            {
                _owner.actor.equipController.equipWeapon.PutOn(0);
            }
        }

        public void ReloadWing()
        {
            var wingInfo = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing);
            if (wingInfo.equipID > 0)
            {
                _owner.actor.equipController.equipWing.PutOn(wingInfo.equipID, wingInfo.particle, wingInfo.flow, false);
            }
            else
            {
                _owner.actor.equipController.equipWing.PutOn(0);
            }
        }

        public void ReloadManteau()
        {
            var info = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Manteau);
            if (info.equipID > 0)
            {
                _owner.actor.equipController.equipManteau.PutOn(info.equipID, info.particle, info.flow, false);
            }
            else
            {
                _owner.actor.equipController.equipManteau.PutOn(0);
            }
        }

        public void ReloadEffect()
        {
            var info = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect);
            if (info.equipID > 0)
            {
                _owner.actor.equipController.equipEffect.PutOn(info.equipID, info.particle, info.flow, false);
            }
            else
            {
                _owner.actor.equipController.equipEffect.PutOn(0);
            }
        }

        void CheckDirty()
        {
            if (_dirty)
            {
                if (_owner.actor != null)
                {
                    ReloadEquip();
                }
                _dirty = false;
            }
        }

        public void ReloadEquip()
        {
            //if (_owner.isHideEntity)
            //{
            //    return;
            //}
            //npc处理
            if (_owner.entityType == EntityConfig.ENTITY_TYPE_NAME_NPC && String.IsNullOrEmpty(_owner.facade))
            {
                return;
            }
            //处理npc延迟加载真正模型，重新刷一次
            if (_owner.entityType == EntityConfig.ENTITY_TYPE_NAME_NPC )
            {
                _curAvatarModelData.vocation = this._owner.vocation;
                ResetCurEquipDictByString(_owner.facade);
            }
            try
            {
                ReloadHead();
                ReloadCloth();
                ReloadWeapon();
                ReloadWing();
                ReloadEffect();
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        public string GetEquipInfoStr()
        {
            return ModelEquipTools.CalculateEquipInfoStr(this._curAvatarModelData);
        }

        void ResetCurEquipDictByDefault()
        {
            ModelEquipTools.ResetAvatarModelDataByDefault(_curAvatarModelData);
            _dirty = true;
        }

        void ResetCurEquipDictByString(string equipInfo)
        {
            ModelEquipTools.ResetAvatarModelDataByString(_curAvatarModelData, equipInfo);
            _dirty = true;
        }

        void RefreshAvatarInfo()
        {
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).equipID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).equipID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).particle = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).particle;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).flow = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).flow;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorRID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorRID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorGID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorGID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorBID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorBID;

            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).equipID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).equipID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).particle = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).particle;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).flow = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).flow;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).colorRID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).colorRID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).colorGID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).colorGID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).colorBID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).colorBID;

            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).equipID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).equipID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).particle = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).particle;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).flow = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).flow;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorRID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorRID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorGID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorGID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorBID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorBID;

            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).equipID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).equipID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).particle = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).particle;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).flow = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).flow;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).colorRID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).colorRID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).colorGID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).colorGID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).colorBID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).colorBID;

            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).equipID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).equipID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).particle = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).particle;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).flow = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).flow;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).colorRID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).colorRID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).colorGID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).colorGID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).colorBID = _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).colorBID;
        }

        #region avatar

        public void RefreshAvatarEquip()
        {
            if (string.IsNullOrEmpty(_owner.facade)) return;
            if (!string.IsNullOrEmpty(_curEquipInfo) && _curEquipInfo.Equals(_owner.facade)) return;
            _curEquipInfo = _owner.facade;
            _dirty = true;
            ResetCurEquipDictByString(_curEquipInfo);
            RefreshAvatarInfo();
            CheckDirty();
        }

        #endregion

        #region player

        void RefreshFashion(int fashionId)
        {
            if (_fashionId == fashionId)
            {
                return;
            }
            _fashionId = fashionId;
            RefreshEquip();
        }

        public void RefreshPlayerEquip()
        {
            ResetCurHead();
            ResetCurCloth();
            ResetCurWeapon();
            ResetCurWing();
            ResetCurEffect();
            CheckSynInfoToServer();
            CheckDirty();
        }

        public void SyncShowPlayerFacade()
        {
            //预览情况不直接同步
            if (_owner.isPreviewState)
                return;
            EntityMgr.Instance.SetAttr(EntityPlayer.ShowPlayer, "facade", EntityPlayer.Player.equipManager.GetEquipInfoStr());
        }

        void SynInfoToServer()
        {
            var curInfo = GetEquipInfoStr();
            if (!curInfo.Equals(this._owner.facade))
            {
                this._owner.facade = curInfo;
                EntityPlayer.Player.RpcCall("set_facade_req", curInfo);
                //MogoWorld.SynEntityAttr(this._owner, EntityPropertyDefine.facade, this._owner.facade);
            }
            _dirty = false;
        }

        uint synTimerID = 0;
        void CheckSynInfoToServer()
        {
            if (!this._owner.IsPlayer())
            {
                var curInfo = GetEquipInfoStr();
                if (!curInfo.Equals(this._owner.facade))
                {
                    this._owner.facade = curInfo;
                    _curEquipInfo = this._owner.facade;
                }
                return;
            }
            if (_dirty && !_owner.InState(state_config.AVATAR_STATE_TRANSFORM))
            {
                if (synTimerID != 0) TimerHeap.DelTimer(synTimerID);
                synTimerID = TimerHeap.AddTimer(3000, 0, SynInfoToServer);
                SyncShowPlayerFacade();
            }
        }

        void ResetCurCloth()
        {
            int curClothID = _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).equipID;
            if (curClothID == 0) curClothID = role_data_helper.GetDefaultCloth((int)this._owner.vocation);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).equipID != curClothID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).equipID = curClothID;
            }
            if (curClothID == 0)
            {
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).particle = 0;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).flow = 0;
                return;
            }
            int curClothParticeID = GetCurParticleID(ACTEquipmentType.Cloth);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).particle != curClothParticeID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).particle = curClothParticeID;
            }
            int curClothFlowID = GetCurFlowID(ACTEquipmentType.Cloth);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).flow != curClothFlowID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).flow = curClothFlowID;
            }
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorRID != _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorRID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorRID = _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorRID;
            }
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorGID != _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorGID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorGID = _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorGID;
            }
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorBID != _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorBID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorBID = _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorBID;
            }
        }

        void ResetCurWeapon()
        {
            int curWeapon = _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).equipID;
            if (curWeapon == 0) curWeapon = role_data_helper.GetDefaultWeapon((int)this._owner.vocation);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).equipID != curWeapon)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).equipID = curWeapon;
            }
            if (curWeapon == 0)
            {
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).particle = 0;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).flow = 0;
                return;
            }
            int curWeaponParticeID = GetCurParticleID(ACTEquipmentType.Weapon);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).particle != curWeaponParticeID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).particle = curWeaponParticeID;
            }
            int curWeaponFlowID = GetCurFlowID(ACTEquipmentType.Weapon);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).flow != curWeaponFlowID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).flow = curWeaponFlowID;
            }

            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorRID != _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorRID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorRID = _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorRID;
            }
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorGID != _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorGID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorGID = _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorGID;
            }
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorBID != _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorBID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorBID = _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorBID;
            }
        }

        void ResetCurHead()
        {
            int curHeadID = _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).equipID;
            if (curHeadID == 0) curHeadID = role_data_helper.GetDefaultHead((int)_owner.vocation);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).equipID != curHeadID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).equipID = curHeadID;
            }
            if (curHeadID == 0)
            {
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).particle = 0;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).flow = 0;
                return;
            }
            int curHeadParticleID = GetCurParticleID(ACTEquipmentType.Head);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).particle != curHeadParticleID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).particle = curHeadParticleID;
            }
            int curHeadFlowID = GetCurFlowID(ACTEquipmentType.Head);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).flow != curHeadFlowID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).flow = curHeadFlowID;
            }
        }

        void ResetCurWing()
        {
            int curWing = _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).equipID;
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).equipID != curWing)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).equipID = curWing;
            }
            if (curWing == 0)
            {
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).particle = 0;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).flow = 0;
                return;
            }
            int curWingParticeID = GetCurParticleID(ACTEquipmentType.Wing);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).particle != curWingParticeID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).particle = curWingParticeID;
            }
            int curWingFlowID = GetCurFlowID(ACTEquipmentType.Wing);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).flow != curWingFlowID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).flow = curWingFlowID;
            }
        }

        void ResetCurEffect()
        {
            int curEffect = _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).equipID;
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).equipID != curEffect)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).equipID = curEffect;
            }
            if (curEffect == 0)
            {
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).particle = 0;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).flow = 0;
                return;
            }
            int curEffectParticeID = GetCurParticleID(ACTEquipmentType.Effect);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).particle != curEffectParticeID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).particle = curEffectParticeID;
            }
            int curEffectFlowID = GetCurFlowID(ACTEquipmentType.Effect);
            if (_curAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).flow != curEffectFlowID)
            {
                _dirty = true;
                _curAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).flow = curEffectFlowID;
            }
        }

        public void SetCurCloth(int clothID, int particleID = 0, int flowID = 0, int r = 0, int g = 0, int b = 0)
        {
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).equipID = clothID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).particle = particleID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).flow = flowID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorRID = r;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorGID = g;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).colorBID = b;
            RefreshEquip();
        }

        public void SetCurWeapon(int weaponID, int particleID = 0, int flowID = 0, int r = 0, int g = 0, int b = 0)
        {
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).equipID = weaponID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).particle = particleID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).flow = flowID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorRID = r;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorGID = g;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).colorBID = b;
            RefreshEquip();
        }

        public void SetCurHead(int headID, int particleID = 0, int flowID = 0, int r = 0, int g = 0, int b = 0)
        {
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).equipID = headID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).particle = particleID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Head).flow = flowID;
            RefreshEquip();
        }

        public void SetCurWing(int wingId, int particleID = 0, int flowID = 0, int r = 0, int g = 0, int b = 0)
        {
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).equipID = wingId;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).particle = particleID;
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Wing).flow = flowID;
            RefreshEquip();
        }

        public void SetCurEffect(int effectId, int particleID = 0, int flowID = 0, int r = 0, int g = 0, int b = 0)
        {
            _setAvatarModelData.GetEquipInfo(ACTEquipmentType.Effect).equipID = effectId;
            RefreshEquip();
        }

        public void ResetDefaultEquip(ACTEquipmentType equipType)
        {
            int equipID = 0;
            int particleID = 0;
            int flowID = 0;
            int r = 0;
            int g = 0;
            int b = 0;
            switch (equipType)
            {
                case ACTEquipmentType.Cloth:
                    equipID = role_data_helper.GetDefaultCloth((int)this._owner.vocation);
                    SetCurCloth(equipID, particleID, flowID, r, g, b);
                    break;
                case ACTEquipmentType.Wing:
                    SetCurWing(equipID, particleID, flowID, r, g, b);
                    break;
                case ACTEquipmentType.Head:
                    equipID = role_data_helper.GetDefaultHead((int)this._owner.vocation);
                    SetCurHead(equipID, particleID, flowID, r, g, b);
                    break;
                case ACTEquipmentType.Weapon:
                    equipID = role_data_helper.GetDefaultWeapon((int)this._owner.vocation);
                    SetCurWeapon(equipID, particleID, flowID, r, g, b);
                    break;
                case ACTEquipmentType.Effect:
                    SetCurEffect(equipID, particleID, flowID, r, g, b);
                    break;
                default:
                    break;
            }
        }

        public void AddEquip(int equipID, int particleID = 0, int flowID = 0, int r = 0, int g = 0, int b = 0)
        {
            if (equipID <= 0) return;
            var data = ACTRunTimeData.GetEquipmentData(equipID);
            switch (data.EquipType)
            {
                case ACTEquipmentType.Cloth:
                    SetCurCloth(equipID, particleID, flowID, r, g, b);
                    break;
                case ACTEquipmentType.Weapon:
                    SetCurWeapon(equipID, particleID, flowID, r, g, b);
                    break;
                case ACTEquipmentType.Wing:
                    SetCurWing(equipID, particleID, flowID, r, g, b);
                    break;
                case ACTEquipmentType.Head:
                    SetCurHead(equipID, particleID, flowID, r, g, b);
                    break;
                case ACTEquipmentType.Effect:
                    SetCurEffect(equipID, particleID, flowID, r, g, b);
                    break;
                default:
                    break;
            }
        }

        private int GetCurParticleID(ACTEquipmentType type)
        {
            return _setAvatarModelData.GetEquipInfo(type).particle;
        }

        private int GetCurFlowID(ACTEquipmentType type)
        {
            return _setAvatarModelData.GetEquipInfo(type).flow;
        }

        private bool UseFashionEquipment()
        {
            return _fashionId > 0;
        }

        #endregion
    }
}
