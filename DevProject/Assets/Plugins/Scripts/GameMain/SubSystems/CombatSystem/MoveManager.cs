﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using MogoEngine.RPC;
using GameMain;
using Common.States;
using GameLoader.Utils;
using Common.ClientConfig;
using MogoEngine.Mgrs;
using MogoEngine.Events;


using Common.Utils;
using GameData;
using Common.ServerConfig;

namespace GameMain.CombatSystem
{
    public class MoveManagerUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class MoveManager
    {
        private EntityCreature _owner;
        private Action<EntityCreature> _moveEndCallBack = null;

        public Action<EntityCreature> MoveEndCallBack
        {
            get
            {
                return _moveEndCallBack;
            }

            set
            {
                _moveEndCallBack = value;
            }
        }

        private List<float> _speedMlps = new List<float>();
        public List<float> speedMlps
        {
            get { return _speedMlps; }
        }

        private float _curMoveSpeed;
        private float _curClientMoveSpeed;
        public float speedRate = 1.0f;
        public float curMoveSpeed
        {
            get
            {
                if (!EntityPlayer.battleServerMode)
                {
                    if (_owner.IsRiding())
                    {
                        return _owner.rideManager.speed;
                    }
                    return _curClientMoveSpeed;
                }
                return _curMoveSpeed;
            }
        }

        private AccordingStateBase _curAccordingState;

        private Dictionary<AccordingMode, AccordingStateBase> _accordingModeActionMap = new Dictionary<AccordingMode, AccordingStateBase>(new AccordingModeComparer());

        private bool isLooping = false;
        private bool hasInitUpdate = false;

        public MoveManager() { }
        public void SetOwner(EntityCreature owner)
        {
            RemoveListeners();
            _owner = owner;
            if (!hasInitUpdate)
            {
                hasInitUpdate = true;
                MogoEngine.MogoWorld.RegisterUpdate<MoveManagerUpdateDelegate>("MoveManager.Update", Update);
            }
            isLooping = true;
            
            reCalculateMoveSpeed();
            EntityPropertyManager.Instance.AddEventListener(owner, EntityPropertyDefine.speed, reCalculateMoveSpeed);
            if (_owner.IsPlayer())
            {
                EntityPropertyManager.Instance.AddEventListener(owner, EntityPropertyDefine.base_speed, reCalculateMoveSpeed);
            }
        }

        public void Release()
        {
            //MogoEngine.MogoWorld.UnregisterUpdate("MoveManager.Update", Update);
            RemoveListeners();
            ClearData();
        }

        private void RemoveListeners()
        {
            if (_owner == null) return;
            EntityPropertyManager.Instance.RemoveEventListener(_owner, EntityPropertyDefine.speed, reCalculateMoveSpeed);
            if (_owner.IsPlayer())
            {
                EntityPropertyManager.Instance.RemoveEventListener(_owner, EntityPropertyDefine.base_speed, reCalculateMoveSpeed);
            }
        }

        private void ClearData()
        {
            isLooping = false;
            _moveEndCallBack = null;
            _speedMlps.Clear();
            _curMoveSpeed = 0f;
            _curClientMoveSpeed = 0f;
            speedRate = 1.0f;
            _curAccordingState = null;
            _accordingModeActionMap.Clear();
            defaultAccordingMode = AccordingMode.AccordingEntity;
            _accordingMode = AccordingMode.AccordingEntity;
            lastY = 0f;
        }

        private bool isAutoMoving = false;//自动寻路状态标记
        public AccordingMode _accordingMode = AccordingMode.AccordingEntity;
        public AccordingMode accordingMode
        {
            set
            {
                _accordingMode = value;
                var newAccordingState = GetAccordingState(_accordingMode);
                if (_curAccordingState != newAccordingState)
                {
                    if (_curAccordingState != null)
                    {
                        _curAccordingState.OnLeave();
                        if (_curAccordingState.modeType == AccordingMode.AccordingLine || _curAccordingState.modeType == AccordingMode.AccordingPath)
                        {
                            isAutoMoving = false;
                        }
                    }
                    _curAccordingState = newAccordingState;
                    newAccordingState.OnEnter();
                    if (_accordingMode == AccordingMode.AccordingLine || _accordingMode == AccordingMode.AccordingPath)
                    {
                        isAutoMoving = true;
                    }
                }
            }
            get { return _accordingMode; }
        }

        public AccordingMode defaultAccordingMode = AccordingMode.AccordingEntity;

        private AccordingStateBase CreateAccordingState(AccordingMode mode)
        {
            AccordingStateBase result = null;
            switch (mode)
            {
                case AccordingMode.AccordingActor:
                    result = new AccordingActor(_owner);
                    break;
                case AccordingMode.AccordingEntity:
                    result = new AccordingEntity(_owner);
                    break;
                case AccordingMode.AccordingMovement:
                    result = new AccordingMovement(_owner);
                    break;
                case AccordingMode.AccordingPath:
                    result = new AccordingPath(_owner);
                    break;
                case AccordingMode.AccordingStick:
                    result = new AccordingStick(_owner);
                    break;
                case AccordingMode.AccordingStickInSkill:
                    result = new AccordingStickInSkill(_owner);
                    break;
                case AccordingMode.AccordingLine:
                    result = new AccordingLine(_owner);
                    break;
                case AccordingMode.AccordingDirection:
                    result = new AccordingDirection(_owner);
                    break;
                case AccordingMode.AccordingAnimationClip:
                    result = new AccordingAnimationClip(_owner);
                    break;
                case AccordingMode.AccordingElevator:
                    result = new AccordingElevator(_owner);
                    break;
                case AccordingMode.AccordingSteeringWheel:
                    result = new AccordingSteeringWheel(_owner);
                    break;
                case AccordingMode.AccordingTouch:
                    result = new AccordingTouch(_owner);
                    break;
                case AccordingMode.AccordingTouchInSkill:
                    result = new AccordingTouchInSkill(_owner);
                    break;
                case AccordingMode.AccordingFly:
                    result = new AccordingFly(_owner);
                    break;
                case AccordingMode.AccordingGlide:
                    result = new AccordingGlide(_owner);
                    break;
                case AccordingMode.AccordingSkill:
                    result = new AccordingSkill(_owner);
                    break;
                case AccordingMode.AccordingPathFly:
                    result = new AccordingPathFly(_owner);
                    break;
                case AccordingMode.AccordingToDirection:
                    result = new AccordingToDirection(_owner);
                    break;
            }
            return result;
        }

        private AccordingStateBase GetAccordingState(AccordingMode mode)
        {
            if (!_accordingModeActionMap.ContainsKey(mode))
            {
                _accordingModeActionMap.Add(mode, CreateAccordingState(mode));
            }
            return _accordingModeActionMap[mode];
        }

        private void reCalculateMoveSpeed()
        {
            _curMoveSpeed = _owner.speed * 0.01f;

            _curClientMoveSpeed = _owner.speed * 0.01f;
            for (int i = 0; i < _speedMlps.Count; i++)
            {
                _curClientMoveSpeed *= _speedMlps[i];
            }
        }

        public void UpdateSpeedRate()
        {
            float addRate = _owner.GetBattleAttribute(battle_attri_config.ATTRI_ID_MOVE_SPEED_RATE);
            float reduceRate = _owner.GetBattleAttribute(battle_attri_config.ATTRI_ID_MOVE_SPEED_REDUCE_RATE);
            float speed = (1 + addRate) / (1 + reduceRate);
            speed = Mathf.Round(speed * 10) / 10; // 保留一位，四舍五入
            if (speed <= 0) LoggerHelper.Error("@策划 移动速度比例<= 0,检查buff效果add_attri(65或66", true);
            if (speed < 0.1) speed = 0.1f;
            if (speed > 2f) speed = 2f;
            speedRate = speed;
        }

        //行走动画速度
        public void UpdateActorSpeedRate()
        {
            if (_owner.actor == null) return;
            float speed = 1;
            if (!_owner.IsRiding())
            {
                float addRate = _owner.GetBattleAttribute(battle_attri_config.ATTRI_ID_MOVE_SPEED_RATE);
                float reduceRate = _owner.GetBattleAttribute(battle_attri_config.ATTRI_ID_MOVE_SPEED_REDUCE_RATE);
                speed = (1 + addRate) / (1 + reduceRate);
            }
            else
            {
                //speed = _owner.moveManager.curMoveSpeed / global_params_helper.map_mount_speed;
                speed = _owner.moveManager.curMoveSpeed / 1;
            }
            speed = Mathf.Round(speed * 10) / 10; // 保留一位，四舍五入
            if (speed < 0.3f) speed = 0.3f;
            if (speed > 1.6f) speed = 1.6f;
            _owner.actor.animationController.SetReadySpeed(speed);
            var ride = _owner.actor.ride;
            if (ride != null)
            {
                ride.animationController.SetReadySpeed(speed);
            }
        }

        public void AddSpeedMlp(float value)
        {
            _speedMlps.Add(value);
            reCalculateMoveSpeed();
        }

        public void RemoveSpeedMlp(float value)
        {
            if (_speedMlps.Contains(value))
            {
                _speedMlps.Remove(value);
            }
            reCalculateMoveSpeed();
        }

        public void ClearSpeedMlp()
        {
            _speedMlps.Clear();
        }

        float lastY = 0f;
        private void Update()
        {
            if (!isLooping) return;
            if (_owner.banBeControlled) return;


            if (_curAccordingState != null)
            {
                if (!_curAccordingState.Update()
                    && _curAccordingState.modeType != defaultAccordingMode)
                {
                    Stop();
                }
                //if (isAutoMoving && _owner.IsPlayer())
                //{
                //    bool touching = Input.GetMouseButton(0) || Input.touchCount > 0;
                //    lastY = GameMain.GlobalManager.CameraManager.GetInstance().CameraTransform.localEulerAngles.y;
                //    if (ACTSystem.ACTMathUtils.Abs(_owner.actor.localEulerAngles.y - lastY) > 0.1f && !touching)
                //    {
                //        lastY = _owner.actor.localEulerAngles.y;
                //        GameMain.GlobalManager.CameraManager.GetInstance().AutoMovingCamera(20, lastY);
                //    }
                //}
            }
        }

        public void Stop(bool runCallback = true)
        {
            if (_curAccordingState != null)
            {
                _curAccordingState.Stop();
                var cb = MoveEndCallBack;
                MoveEndCallBack = null;
                if (runCallback && cb != null && (accordingMode == AccordingMode.AccordingLine || accordingMode == AccordingMode.AccordingPath))
                {
                    cb.Invoke(_owner);
                }
            }
            accordingMode = defaultAccordingMode;
        }

        public void StopVertical()
        {
            if (_curAccordingState != null)
            {
                _curAccordingState.StopVertical();
            }
        }

        public void Move(Vector3 targetPosition, float speed, Action<EntityCreature> moveEndCallBack = null, float endDistance = 0)
        {
            bool refresh = false;
            if (speed == -1) speed = this.curMoveSpeed;
            if (speed == -2)
            {
                //使用玩家实时速度
                speed = this.curMoveSpeed;
                refresh = true;
            }
            if (!FindPathManager.GetInstance().CanLine(_owner.position, targetPosition, _owner.actor.modifyLayer))
            {
                MoveByPath(targetPosition, speed, moveEndCallBack, endDistance, refresh);
            }
            else
            {
                MoveByLine(targetPosition, speed, moveEndCallBack, endDistance, refresh);
            }
        }

        public void MoveByLine(Vector3 targetPosition, float baseSpeed, Action<EntityCreature> moveEndCallBack = null, float endDistance = 0, bool refresh = false)
        {
            if (baseSpeed == -1) baseSpeed = this.curMoveSpeed;
            var accordingLine = GetAccordingState(AccordingMode.AccordingLine) as AccordingLine;
            if (!accordingLine.Reset(targetPosition, baseSpeed, endDistance, refresh)) return;
            accordingMode = AccordingMode.AccordingLine;
            MoveEndCallBack = moveEndCallBack;
        }

        public void MoveByMovement(Vector3 targetPosition, float baseSpeed, float accelerate = 0, bool percent = false, bool needFix = true)
        {
            var accordingMovement = GetAccordingState(AccordingMode.AccordingMovement) as AccordingMovement;
            if (!accordingMovement.Reset(targetPosition, baseSpeed, accelerate, percent, needFix)) return;
            accordingMode = AccordingMode.AccordingMovement;
            Update();
        }

        public void MoveByPath(Vector3 targetPosition, float speed, Action<EntityCreature> moveEndCallBack = null, float endDistance = 0, bool refresh = false)
        {
            var accordingPath = GetAccordingState(AccordingMode.AccordingPath) as AccordingPath;
            reCalculateMoveSpeed();//临时这里处理一下
            if (!accordingPath.Reset(targetPosition, speed, endDistance, refresh)) return;

            accordingMode = AccordingMode.AccordingPath;
            MoveEndCallBack = moveEndCallBack;

        }

        public List<float> GetPath()
        {
            return GetAccordingState(accordingMode).GetPath();
        }

        public void MoveBySkill(float duration)
        {
            AccordingSkill accordingSkill;
            /*ari
            if (!PlayerTouchBattleModeManager.GetInstance().CurrentCanTouch())
            {
                accordingSkill = GetAccordingState(AccordingMode.AccordingStickInSkill) as AccordingSkill;
            }
            else
            {
                accordingSkill = GetAccordingState(AccordingMode.AccordingTouchInSkill) as AccordingSkill;
            }*/
            accordingSkill = GetAccordingState(AccordingMode.AccordingTouchInSkill) as AccordingSkill;
            if (!accordingSkill.Reset(duration)) return;
            accordingMode = accordingSkill.modeType;
        }

        public void MoveByDirection(uint targetID, float speed, float moveTime = 1, bool right = false)
        {
            var accordingDirection = GetAccordingState(AccordingMode.AccordingDirection) as AccordingDirection;
            if (!accordingDirection.Reset(targetID, speed, moveTime, right)) return;
            accordingMode = AccordingMode.AccordingDirection;
        }

        public void MoveToDirection(Vector3 targetPosition, float speed, float moveTime = 1)
        {
            var accordingToDirection = GetAccordingState(AccordingMode.AccordingToDirection) as AccordingToDirection;
            if (!accordingToDirection.Reset(targetPosition, speed, moveTime)) return;
            accordingMode = AccordingMode.AccordingToDirection;
        }

        public void Jump(int speed)
        {
            _owner.actor.actorController.Jump(speed * 0.01f);
        }

        public void MoveByPathFly(float speed = 5, List<Vector3> path = null, Action<EntityCreature> moveEndCallBack = null, float endDistance = 0)
        {
            var accordingPath = GetAccordingState(AccordingMode.AccordingPathFly) as AccordingPathFly;
            if (!accordingPath.Reset(path, speed, endDistance)) return;
            accordingMode = AccordingMode.AccordingPathFly;
            MoveEndCallBack = moveEndCallBack;
        }

        public void PauseFly(bool state)
        {
            if (_curAccordingState is AccordingPathFly)
            {
                (_curAccordingState as AccordingPathFly).isPause = state;
            }
        }
    }
}
