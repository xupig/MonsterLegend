﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameData;
using MogoEngine.RPC;
using GameMain;

using Common.ServerConfig;
using GameLoader.Utils;
using Common.States;
using MogoEngine;

using Common.Structs.ProtoBuf;
using MogoEngine.Events;


namespace GameMain.CombatSystem
{
    public class LimitedSkillData
    {
        private int maxNum = 0;
        private float roundCD = 0;
        private float maxTime = 0;
        private float _lastRecordTime = 0;
        private float _curTime = 0;

        public float curTime
        {
            get
            {
                Refresh();
                return _curTime;
            }
        }

        private void Refresh()
        {
            _curTime += RealTime.time - _lastRecordTime;
            _lastRecordTime = RealTime.time;
            if (_curTime > maxTime)
            {
                _curTime = maxTime;
            }
        }

        public void Init(int maxNum, float roundCD, float maxTime)
        {
            _lastRecordTime = RealTime.time;
            _curTime = maxTime;
            this.maxNum = maxNum;
            this.roundCD = roundCD;
            this.maxTime = maxTime;
        }

        public bool IsCanUse()
        {
            Refresh();
            if (_curTime < roundCD) return false;
            return true;
        }

        public void UseOne()
        {
            Refresh();
            _curTime -= roundCD;
        }
    }

    public class SkillManagerUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class SkillManager
    {
        private static readonly int BUFF_SKILL_DEFALUT = 0;
        private static readonly int SKILL_POS_DIR_MASK = 8;

        public delegate void VoidDelegate();
        public VoidDelegate onPosChange;

        private EntityCreature _owner;

        private Dictionary<int, float> _skillCD = new Dictionary<int, float>();
        private Dictionary<int, LimitedSkillData> _limitedSkillTimeDict = new Dictionary<int, LimitedSkillData>();

        private HashSet<int> _slottedSkillList = new HashSet<int>();
        public HashSet<int> slottedSkillList { get { return _slottedSkillList; } }

        private List<int> _ownedSkillList = new List<int>();
        private Dictionary<int, int> _ownedSkillDict = new Dictionary<int, int>();

        private Dictionary<int, int> _learnedSkillDict = new Dictionary<int, int>();
        private Dictionary<int, int> _transformSkillDict = new Dictionary<int, int>(); //变身技能

        private Dictionary<int, Dictionary<int, int>> _posToSkillIDListMap = new Dictionary<int, Dictionary<int, int>>(6);
        public Dictionary<int, Dictionary<int, int>> posToSkillIDListMap { get { return _posToSkillIDListMap; } }

        private HashSet<int> _skillBuffers = new HashSet<int>();

        private List<int> _curAllPosList = new List<int>();

        private Dictionary<int, int> _curPosSkillIDMap = new Dictionary<int, int>();

        //调整技能ID列表
        //private List<int> _adjustSkillIDList = new List<int>();
        //公共CD
        private float _commonCD = 0f;
        //造成公共CD的技能ID
        private int _curCommonCdSkillId = 0;
        private int _useSkillNum = 0;
        private bool _inServerMode = false;
        public bool inServerMode { get { return _inServerMode; } }

        public ShieldAgainstData shieldAgainstData = new ShieldAgainstData();

        private SkillSubjectManager _skillSubjectManager;
        public SkillSubjectManager skillSubjectManager { get { return _skillSubjectManager; } }

        private SkillSubjectManagerServer _skillSubjectManagerServer;
        public SkillSubjectManagerServer skillSubjectManagerServer { get { return _skillSubjectManagerServer; } }

        private bool _isSkillPreview = false;
        private bool _isDramaSkill = false;
        public int curSkillId = 0;
        private bool isLooping = false;
        private bool hasInitUpdate = false;

        public SkillManager() { }
        public void SetOwner(EntityCreature owner)
        {
            _owner = owner;
            //_isSkillPreview = owner is EntityPreview;
            //_isDramaSkill = owner.isDramaEntity && !(owner.IsPlayer());
            //SetInServerMode(GameSceneManager.GetInstance().IsServerMonster());
            if (!hasInitUpdate)
            {
                hasInitUpdate = true;
                MogoEngine.MogoWorld.RegisterUpdate<SkillManagerUpdateDelegate>("SkillManager.Update", Update);
            }
            isLooping = true;
            //SetLearnedSkillList(_learnedSkillList);
            RefreshOwnedSkills();

            if (_skillSubjectManagerServer != null)
            {
                _skillSubjectManagerServer.SetOwner(this._owner);
            }
            if (_skillSubjectManager != null)
            {
                _skillSubjectManager.SetOwner(this._owner);
            }
        }

        public void Release()
        {
            ClearData();
            //MogoEngine.MogoWorld.UnregisterUpdate("SkillManager.Update", Update);
            if (_skillSubjectManager != null) _skillSubjectManager.Release();
            if (_skillSubjectManagerServer != null) _skillSubjectManagerServer.Release();
        }

        public void ClearData()
        {
            isLooping = false;
            _owner = null;
            _skillCD.Clear();
            _limitedSkillTimeDict.Clear();
            _slottedSkillList.Clear();
            _ownedSkillList.Clear();
            _ownedSkillDict.Clear();
            _learnedSkillDict.Clear();
            _transformSkillDict.Clear();
            //_posToSkillIDListMap.Clear();
            foreach (var posDic in _posToSkillIDListMap) { posDic.Value.Clear(); }
            _skillBuffers.Clear();
            _curAllPosList.Clear();
            _curPosSkillIDMap.Clear();
            _commonCD = 0f;
            _curCommonCdSkillId = 0;
            _useSkillNum = 0;
            _inServerMode = false;
            shieldAgainstData.ClearData();
            _isSkillPreview = false;
            _isDramaSkill = false;
            curSkillId = 0;
            _spaceTime = 0;
            _lastPlaySkill = 0;
            _adjustSkillIDList.Clear();
            _adjustSkillActionIDList.Clear();
            _minFixDis = 0.5f;
        }

        public void SetSlottedSkillList(List<int> slottedList)
        {
            for (int i = 0; i < slottedList.Count; i++)
            {
                var skillID = slottedList[i];
                _slottedSkillList.Add(skillID);
            }
        }

        public HashSet<int> GetSlottedSkillList()
        {
            return _slottedSkillList;
        }


        public void SetLearnedSkillList(List<int> learnedSkillList)
        {
            _learnedSkillDict.Clear();
            for (int i = 0; i < learnedSkillList.Count; i++)
            {
                _learnedSkillDict.Add(learnedSkillList[i], 0);
            }
            SetLearnedSkillDict(_learnedSkillDict);
        }

        public void SetLearnedSkillDict(Dictionary<int, int> learnedDict)
        {
            _learnedSkillDict = learnedDict;
            RefreshOwnedSkills();
        }

        //刷新现在拥有的技能
        public void RefreshOwnedSkills()
        {
            _ownedSkillDict.Clear();
            _ownedSkillList.Clear();
            foreach (var item in _learnedSkillDict)
            {
                _ownedSkillDict.Add(item.Key, item.Value);
                _ownedSkillList.Add(item.Key);
            }
            foreach (var item in _transformSkillDict)
            {
                _ownedSkillDict.Add(item.Key, item.Value);
                _ownedSkillList.Add(item.Key);
            }
            try
            {
                ResetPosIDMap(_ownedSkillDict);
            }
            catch (Exception ex) { LoggerHelper.Error(ex.Message); }
        }

        //public void AddLearnedSkillList(List<int> learnedList)
        //{
        //    if (_learnedSkillList == null)
        //    {
        //        _learnedSkillList = learnedList;
        //    }
        //    else
        //    {
        //        for (int i = 0; i < learnedList.Count; i++)
        //        {
        //            int newID = learnedList[i];
        //            if (!_learnedSkillList.Contains(newID))
        //            {
        //                _learnedSkillList.Add(newID);
        //            }
        //        }
        //    }
        //    try
        //    {
        //        ResetPosIDMap(_learnedSkillList);
        //    }
        //    catch (Exception ex) { LoggerHelper.Error(ex.Message + ":\n" + ex.StackTrace); }
        //}

        private void ResetPosIDMap(Dictionary<int, int> skillIDDict)
        {
            //_posToSkillIDListMap.Clear();
            foreach (var posDic in _posToSkillIDListMap) { posDic.Value.Clear(); }
            _skillBuffers.Clear();
            _curAllPosList.Clear();
            _curPosSkillIDMap.Clear();
            _limitedSkillTimeDict.Clear();
            foreach (var item in skillIDDict)
            {
                var skillID = item.Key;
                int slot = item.Value;
                var skillData = CombatLogicObjectPool.GetSkillData(skillID);
                int pos = skillData.pos;
                pos = (skillData.dir << SKILL_POS_DIR_MASK) + pos;
                if (slot != 0)
                {
                    pos = slot;
                }
                if (!_posToSkillIDListMap.ContainsKey(pos))
                {
                    _posToSkillIDListMap.Add(pos, new Dictionary<int, int>());
                }
                var bufferID = ACTSystem.ACTMathUtils.Abs(skillData.showType);
                if (!_posToSkillIDListMap[pos].ContainsKey(bufferID))
                {
                    _posToSkillIDListMap[pos].Add(bufferID, skillID);
                }
                if (!_skillBuffers.Contains(bufferID))
                {
                    _skillBuffers.Add(bufferID);
                }
                if (bufferID == 0)
                {
                    _curPosSkillIDMap[pos] = skillID;
                }
                if (!_curAllPosList.Contains(pos))
                {
                    _curAllPosList.Add(pos);
                }
                if (IsLimitedSkill(skillID) && !_limitedSkillTimeDict.ContainsKey(skillID))
                {
                    LimitedSkillData lsd = new LimitedSkillData();
                    float maxTime = GetCanUseTimeTotal(skillID);
                    int maxNum = GetCanUseTime(skillID)[0];
                    float roundCD = GetCanUseTime(skillID)[1] * 0.001f;
                    lsd.Init(maxNum, roundCD, maxTime);
                    _limitedSkillTimeDict.Add(skillID, lsd);
                }
            }
            OnPosChange();
        }

        //private void ResetPosIDMap(List<int> skillIDList)
        //{
        //    //_posToSkillIDListMap.Clear();
        //    foreach (var posDic in _posToSkillIDListMap){ posDic.Value.Clear(); }
        //    _skillBuffers.Clear();
        //    _curAllPosList.Clear();
        //    _curPosSkillIDMap.Clear();
        //    _limitedSkillTimeDict.Clear();
        //    for (int i = 0; i < skillIDList.Count; i++)
        //    {
        //        var skillID = skillIDList[i];
        //        var skillData = CombatLogicObjectPool.GetSkillData(skillID);
        //        int pos = skillData.pos;
        //        pos = (skillData.dir << SKILL_POS_DIR_MASK) + pos;
        //        if (!_posToSkillIDListMap.ContainsKey(pos))
        //        {
        //            _posToSkillIDListMap.Add(pos, new Dictionary<int, int>());
        //        }
        //        var bufferID = ACTSystem.ACTMathUtils.Abs(skillData.showType);
        //        if (!_posToSkillIDListMap[pos].ContainsKey(bufferID))
        //        {
        //            _posToSkillIDListMap[pos].Add(bufferID, skillID);
        //        }
        //        if (!_skillBuffers.Contains(bufferID))
        //        { 
        //            _skillBuffers.Add(bufferID); 
        //        }
        //        if (bufferID == 0)
        //        {
        //            _curPosSkillIDMap[pos] = skillID;
        //        }
        //        if (!_curAllPosList.Contains(pos))
        //        {
        //            _curAllPosList.Add(pos);
        //        }
        //        if (IsLimitedSkill(skillID) && !_limitedSkillTimeDict.ContainsKey(skillID))
        //        {
        //            LimitedSkillData lsd = new LimitedSkillData();                    
        //            float maxTime = GetCanUseTimeTotal(skillID);
        //            int maxNum = GetCanUseTime(skillID)[0];
        //            float roundCD = GetCanUseTime(skillID)[1] * 0.001f;
        //            lsd.Init(maxNum, roundCD, maxTime);
        //            _limitedSkillTimeDict.Add(skillID, lsd);
        //        }
        //    }
        //    OnPosChange();
        //}

        public void AddSkillActionToSkillSubject(int skillID, int skillActionID, Matrix4x4 ownerWorldMatrixOnSkillActive, uint mainTargetID)
        {
            if (!_inServerMode)
            {
                _skillSubjectManager.CreateSkillAction(skillID, skillActionID, ownerWorldMatrixOnSkillActive, mainTargetID);
            }
        }

        public void BreakCurSkill()
        {
            if (!_inServerMode)
            {
                _skillSubjectManager.BreakCurSkill();
            }
            else
            {
                _skillSubjectManagerServer.BreakCurSkill();
            }
        }

        public bool PlaySkill(int skillID, uint assignId = 0, int slot = 0)
        {
            if (_owner == null) return false;
            int errorID = CanDo(skillID);
            if (errorID > 0)
            {
                return false;
            }
            if (_owner.IsPlayer())
            {
                PlayerEventManager.GetInstance().CastSpell();
            }
            if (_ownedSkillList.Contains(skillID))
            {
                this._useSkillNum++;
            }
            if (_inServerMode)
            {
                _skillSubjectManagerServer.AddSkill(skillID, assignId, slot);
            }
            else
            {
                if (_limitedSkillTimeDict.ContainsKey(skillID))
                {
                    _limitedSkillTimeDict[skillID].UseOne();
                }
                _skillSubjectManager.AddSkill(skillID, assignId, slot);
            }
            curSkillId = skillID;
            return true;
        }

        public bool PlaySkillBySkillPlayer(int skillID, uint assignId = 0)
        {
            if (!_isSkillPreview)
            {
                _isSkillPreview = true;
                SetInServerMode(false);
            }
            _skillSubjectManager.AddSkill(skillID, assignId);
            return true;
        }

        public void SetDramaSkill(bool state)
        {
            _isDramaSkill = state;
        }

        public void OnBufferChange(int bufferID, bool isAdded = false)
        {
            if (_owner.IsPlayer())
            {
                //刷新ui界面上显示
                var bufferData = CombatLogicObjectPool.GetBufferData(bufferID);
                if (bufferData.clientUIShow)
                {
                    LuaDriver.instance.CallFunction("CSCALL_ON_BUFFER_CHANGE");
                }
            }
            if (_skillBuffers.Contains(bufferID))
            {
                for (int i = 0; i < _curAllPosList.Count; i++)
                {
                    var pos = _curAllPosList[i];
                    if (_posToSkillIDListMap.ContainsKey(pos) && _posToSkillIDListMap[pos].ContainsKey(bufferID))
                    {
                        _curPosSkillIDMap[pos] = GetSkillIDByPos(pos);
                    }
                }
                OnPosChange();
            }
        }

        public Dictionary<int, int> GetCurPosSkillIDMap()
        {
            return _curPosSkillIDMap;
        }

        public int GetSkillIDByPos(int pos)
        {
            int curDir = ControlStickState.GetDirectionZone();
            int curSkillDir = ControlStickState.GetSkillDirectionZone();
            int dirPos = (curDir << SKILL_POS_DIR_MASK) + pos;
            pos = _posToSkillIDListMap.ContainsKey(dirPos) ? dirPos : pos;
            int canPlaySkill = 0;
            while (_posToSkillIDListMap.ContainsKey(pos))
            {
                var buffList = _owner.bufferManager.GetCurBufferIDList();
                var buffSkillMap = _posToSkillIDListMap[pos];
                if (!buffSkillMap.ContainsKey(BUFF_SKILL_DEFALUT))
                {
                    LoggerHelper.Error(string.Format("{0}在Pos{1}上没有对应可用的默认技能!", this._owner.id, pos));
                    break;
                }
                canPlaySkill = buffSkillMap[BUFF_SKILL_DEFALUT];
                var canPlaySkillData = CombatLogicObjectPool.GetSkillData(canPlaySkill);
                if (canPlaySkillData.is_stick_change == 1 && curSkillDir != 0)
                {
                    canPlaySkill = canPlaySkillData.stick_change_list[curSkillDir - 1];
                    canPlaySkillData = CombatLogicObjectPool.GetSkillData(canPlaySkill);
                }
                for (int i = 0; i < buffList.Count; i++)
                {
                    var bufferID = buffList[i];
                    if (buffSkillMap.ContainsKey(bufferID))
                    {
                        var skillID = buffSkillMap[bufferID];
                        var skillData = CombatLogicObjectPool.GetSkillData(skillID);
                        if (skillData.showPirority > canPlaySkillData.showPirority)
                        {
                            canPlaySkill = skillID;
                            canPlaySkillData = CombatLogicObjectPool.GetSkillData(canPlaySkill);
                        }
                    }
                }
                break;
            }
            return canPlaySkill;
        }

        private int _lastPlaySkill = 0;
        public int PlaySkillByPos(int pos, uint assignId = 0)
        {
            _lastPlaySkill = 0;
            int canPlaySkill = GetSkillIDByPos(pos);
            if (canPlaySkill > 0)
            {
                _lastPlaySkill = canPlaySkill;
                return PlaySkill(canPlaySkill, assignId, pos) ? canPlaySkill : 0;
            }
            return 0;
        }

        public void ReleaseSkillByPos(int pos)
        {
            if (_lastPlaySkill == 0) return;
            if (!_inServerMode)
            {
                _skillSubjectManager.ReleaseSkill(_lastPlaySkill);
            }
            else
            {
                _skillSubjectManagerServer.ReleaseSkill(_lastPlaySkill);
            }
        }

        private void SetInServerMode(bool state)
        {
            _inServerMode = (_isSkillPreview || _isDramaSkill) ? false : state;
            if (_inServerMode)
            {
                if (_skillSubjectManagerServer == null)
                {
                    _skillSubjectManagerServer = CreatureManagerObjectPool.CreateSkillSubjectManagerServer(this._owner);
                }
            }
            else
            {
                if (_skillSubjectManager == null)
                {
                    _skillSubjectManager = CreatureManagerObjectPool.CreateSkillSubjectManager(this._owner);
                }
            }
        }

        private void CheckServerMode()
        {
            if (_isSkillPreview || _isDramaSkill)
            {
                return;
            }
            //SetInServerMode(ServerProxy.Instance.Connected);
            SetInServerMode(EntityPlayer.battleServerMode);
        }

        int _spaceTime = 0;
        private void Update()
        {
            if (!isLooping) return;
            _spaceTime--;
            if (_spaceTime > 0) { return; }
            _spaceTime = 30; //这里要修改设置方式，不能每帧都调。
            CheckServerMode();
        }



        public int CanDo(int skillID)
        {
            int result = 0;
            while (true)
            {
                if (IsInCD(skillID) && !_isSkillPreview && !_isDramaSkill) { result = (int)error_code.ERR_SPELL_IN_CD; break; }
                if (IsInCommonCD(skillID)) { result = (int)error_code.ERR_SPELL_IN_COMMON_CD; break; }
                if (!IsInTheZone(skillID)) { result = (int)error_code.ERR_SPELL_NOT_IN_ZONE; break; }
                if (!EPEnough(skillID)) { result = (int)error_code.ERR_NOT_ENOUGH_SPELL_ENERGY; break; }
                if (!IsCanAttack(skillID)) { result = (int)error_code.ERR_SPELL_PREVIA_NOT_CAST; break; }
                if (!IsHasCanUseTime(skillID)) { result = (int)error_code.ERR_SPELL_IN_CD; break; }
                break;
            }
            return result;
        }

        public void SetCommonCD(int skillID, int cd)
        {
            this._commonCD = RealTime.time + cd * 0.001f;
            _curCommonCdSkillId = skillID;
            OnPosChange();
        }

        public int GetCurCommonCdSkillId()
        {
            return _curCommonCdSkillId;
        }

        public float GetCommonCD()
        {
            return this._commonCD;
        }

        public Dictionary<int, float> GetSkillCDs()
        {
            return _skillCD;
        }

        public int GetCD(int skillID)
        {
            int result = 0;
            if (_skillCD.ContainsKey(skillID))
            {
                result = (int)(Mathf.Max(_skillCD[skillID] - RealTime.time, 0) * 1000);
            }
            return result;
        }

        public float GetCDEndTime(int skillID)
        {
            float result = 0;
            if (_commonCD - RealTime.time > 0)
            {
                result = -_commonCD;
            }
            if (_skillCD.ContainsKey(skillID) && _skillCD[skillID] > RealTime.time)
            {
                result = _skillCD[skillID];
            }
            return result;
        }

        public void SetCD(int skillID, int cd)
        {
            _skillCD[skillID] = RealTime.time + cd * 0.001f;
            OnPosChange();
        }

        public bool IsLimitedSkill(int skillID)
        {
            var skillData = CombatLogicObjectPool.GetSkillData(skillID);
            if (skillData.useTimes.Count > 0 && skillData.useTimes[0] > 0)
            {
                return true;
            }
            return false;
        }

        public List<int> GetCanUseTime(int skillID)
        {
            var skillData = CombatLogicObjectPool.GetSkillData(skillID);
            if (skillData.useTimes.Count == 0) return null;
            return skillData.useTimes;
        }

        public float GetCurLimitedSkillTime(int skillID)
        {
            if (IsLimitedSkill(skillID))
            {
                return _limitedSkillTimeDict[skillID].curTime;
            }
            return 0;
        }

        public float GetCanUseRoundTime(int skillID)
        {
            var skillData = CombatLogicObjectPool.GetSkillData(skillID);
            if (skillData.useTimes.Count == 0) return 0;
            return skillData.useTimes[1] * 0.001f;
        }

        public float GetCanUseTimeTotal(int skillID)
        {
            var skillData = CombatLogicObjectPool.GetSkillData(skillID);
            if (skillData.useTimes.Count == 0) return 0;
            return skillData.useTimes[0] * skillData.useTimes[1] * 0.001f;
        }

        public void ChangeCD(int skillID, int cd)
        {
            SetCD(skillID, GetCD(skillID) + cd);
        }

        public void ChangeSkillGroupCD(int skillGroup, int cd)
        {
            List<int> skillGroupList = skill_helper.GetSpellGroupList(skillGroup);
            if (skillGroupList == null)
            {
                return;
            }
            for (int i = 0; i < skillGroupList.Count; ++i)
            {
                if (_ownedSkillList.Contains(skillGroupList[i]))
                {
                    ChangeCD(skillGroupList[i], cd);
                }
            }
        }

        public bool IsInCommonCD(int skillID)
        {
            var skillData = CombatLogicObjectPool.GetSkillData(skillID);
            if (skillData.cd.Count >= 3 && skillData.cd[2] > 0) return false;
            return (this._commonCD - RealTime.time) > 0;
        }

        public bool IsInTheZone(int skillID)
        {
            return true;
        }

        public bool IsCanAttack(int skillID)
        {
            bool result = false;
            var skillData = CombatLogicObjectPool.GetSkillData(skillID);
            SkillCastZoneType type = (SkillCastZoneType)skillData.spellZone;
            if (_owner.stateManager.CanDO(CharacterAction.ATTACK_ABLE))
            {
                result = true;
            }
            else if (_owner.stateManager.CanDO(CharacterAction.RECOVER_ABLE) && type == SkillCastZoneType.Recover)
            {
                result = true;
            }
            return result;
        }

        public bool IsInCD(int skillID)
        {
            bool result = false;
            if (_skillCD.ContainsKey(skillID))
            {
                result = (_skillCD[skillID] - RealTime.time) > 0;
            }
            return result;
        }

        public bool IsHasCanUseTime(int skillID)
        {
            bool result = true;
            if (_limitedSkillTimeDict.ContainsKey(skillID))
            {
                result = _limitedSkillTimeDict[skillID].IsCanUse();
            }
            return result;
        }

        public void ClearAllCD()
        {
            _skillCD.Clear();
            _commonCD = 0;
            OnPosChange();
        }

        public int GetSkillAIRange(int skillID)
        {
            var skillData = CombatLogicObjectPool.GetSkillData(skillID);
            return skillData.aiRange;
        }

        public float GetActionCutTime(int skillID)
        {
            var skillData = CombatLogicObjectPool.GetSkillData(skillID);
            return skillData.actionCutTime;
        }

        public bool IsStickSkill(int skillID)
        {
            var skillData = CombatLogicObjectPool.GetSkillData(skillID);
            return skillData.specialSearchType != 0 && CanDo(skillID) == 0;
        }

        public bool EPEnough(int skillID)
        {
            bool result = true;
            var skillData = CombatLogicObjectPool.GetSkillData(skillID);
            if (_owner.IsPlayer() && skillData.energyCostType == public_config.ITEM_SPECIAL_SPELL_MP)
            {
                return _owner.cur_mp >= skillData.energyCost;
            }
            return result;
        }

        //调整技能ID列表
        private List<int> _adjustSkillIDList = new List<int>();
        public void AddAdjustSkillID(int skillID)
        {
            _adjustSkillIDList.Add(skillID);
        }

        public void RemoveAdjustSkillID(int skillID)
        {
            if (_adjustSkillIDList.Contains(skillID))
            {
                _adjustSkillIDList.Remove(skillID);
            }
        }

        public List<int> GetCurAdjustSkillIDList()
        {
            return _adjustSkillIDList;
        }

        //调整技能行为ID列表
        private List<int> _adjustSkillActionIDList = new List<int>();
        public void AddAdjustSkillActionID(int skillActionID)
        {
            _adjustSkillActionIDList.Add(skillActionID);
        }

        public void RemoveAdjustSkillActionID(int skillActionID)
        {
            if (_adjustSkillActionIDList.Contains(skillActionID))
            {
                _adjustSkillActionIDList.Remove(skillActionID);
            }
        }

        public List<int> GetCurAdjustSkillActionIDList()
        {
            return _adjustSkillActionIDList;
        }

        public void ClearUseSkillNum()
        {
            this._useSkillNum = 0;
        }

        public int GetUseSkillNum()
        {
            return _useSkillNum;
        }

        public void OnPosChange()
        {
            if (onPosChange != null) onPosChange();
        }

        #region 服务器同步技能

        private float _minFixDis = 0.5f;
        public void FixPosition(float x, float y, float z)
        {
            var targetPosition = new Vector3();
            targetPosition.x = x;
            targetPosition.y = this._owner.position.y;
            targetPosition.z = z;
            var dis = (targetPosition - this._owner.position).magnitude;
            if (dis > _minFixDis)
            {
                this._owner.Teleport(targetPosition);
            }
        }

        public void PlaySkillByServer(PbSpellOtherCastSpellResp respData)
        {
            if (_skillSubjectManagerServer != null)
            {
                FixPosition(respData.pos_x * 0.01f, respData.pos_y * 0.01f, respData.pos_z * 0.01f);
                this._owner.face = new Vector3(respData.face_x, respData.face_y, respData.face_z);
                _skillSubjectManagerServer.AddSkill((int)respData.spell_id, (uint)respData.target_id);
            }
        }

        public void ActiveSkillActionByServer(PbSpellAction skillActionData)
        {
            if (_skillSubjectManagerServer != null)
            {
                _skillSubjectManagerServer.ActiveSkillActionByServer(skillActionData);
            }
        }

        public void JudgeSkillActionByServer(UInt16 skillID, UInt16 skillActionID, PbSpellDamageInfo skillDamageInfo)
        {
            if (_skillSubjectManagerServer != null)
            {
                _skillSubjectManagerServer.JudgeSkillActionByServer(skillID, skillActionID, skillDamageInfo);
            }
        }

        public void OnSkillPlayByServer(PbSpellSelfCastSpellResp respData)
        {
            if (respData.err_id > 0)
            {
                if (UnityPropUtils.IsEditor)
                    LoggerHelper.Error("SelfCastSpellResp:" + respData.err_id);
                return;
            }
            int skillID = (int)respData.spell_id;
            if (_skillSubjectManagerServer != null)
            {
                var skillSubject = _skillSubjectManagerServer.GetCurSkillSubjectByID(skillID);
                if (skillSubject == null)
                {
                    if (UnityPropUtils.IsEditor) LoggerHelper.Warning("skillSubject is null. " + respData.spell_id);
                    return;
                }
                skillSubject.CastSpellResp(respData.target_id);
                AdjustTools.AdjustSkill(skillSubject, GetCurAdjustSkillIDList());
                var cd = skillSubject.GetCD();
                SetCD(skillID, cd[0]);
            }
        }

        public void OnFollowerSkillPlayByServer(UInt16 skillID, Int16 x, Int16 z, byte face, UInt32 targetID)
        {
            if (_skillSubjectManagerServer != null)
            {
                var skillSubject = _skillSubjectManagerServer.GetCurSkillSubjectByID(skillID);
                if (skillSubject == null)
                {
                    if (UnityPropUtils.IsEditor) LoggerHelper.Warning("OnFollowerSkillPlayByServer skillSubject is null." + skillID);
                    return;
                }
                FixPosition(x * 0.01f, this._owner.position.y, z * 0.01f);
                //Ari: this._owner.face = face;
                skillSubject.FollowerCastSpellResp(targetID);
                AdjustTools.AdjustSkill(skillSubject, GetCurAdjustSkillIDList());
                var cd = skillSubject.GetCD();
                SetCD(skillID, cd[0]);
            }
        }

        public void OnSetActionOriginByServer(PbSpellActionOrigin spellActionOrigin)
        {
            if (_skillSubjectManagerServer != null)
            {
                _skillSubjectManagerServer.OnSetActionOriginByServer(spellActionOrigin);
            }
        }

        #endregion
    }
}
