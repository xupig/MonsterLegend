﻿
using UnityEngine;
using ACTSystem;
using GameLoader.Utils;

namespace GameMain.CombatSystem
{
    public static class ACTSkillEventHandler
    {
        public static float GetEventDelay(int mainID, int eventIdx)
        {
            ACTEventBase subFX = ACTRunTimeData.GetEventBySkillIDAndEventIdx(mainID, eventIdx);
            if (subFX == null) {
                LoggerHelper.Error("GetEventDelay Error:" + mainID + ":" + eventIdx);
                return 0 ;
            }
            return subFX.Delay;
        }

        public static void OnEvent(ACTActor actor, int mainID, int eventIdx)
        {
            ACTEventBase subFX = ACTRunTimeData.GetEventBySkillIDAndEventIdx(mainID, eventIdx);
            if (subFX == null)
            {
                LoggerHelper.Error(string.Format("@XXX 找不到对应的技能事件 ACTSkillEventHandler OnEvent Error:{0}:{1}", mainID, eventIdx));
                return;
            }
            if (subFX.EventFXType == ACTSystem.EventType.AnimationFX)
            {
                OnAnimationFX(actor, subFX as ACTEventAnimationFX);
            }
            else if (subFX.EventFXType == ACTSystem.EventType.VisualFX)
            {
                OnVisualFX(actor, subFX as ACTEventVisualFX);
            }
            else if (subFX.EventFXType == ACTSystem.EventType.SoundFX)
            {
                OnSoundFX(actor, subFX as ACTEventSoundFX);
            }
        }

        public static void OnAnimationFX(ACTActor actor, ACTEventAnimationFX setting)
        {
            actor.animationController.PlayAnimationFX(setting);
            actor.actorController.Jump(setting.UpVelocity, setting.KeepFloatTime);
            actor.actorController.SetSlowRate(setting.SlowRate);
            actor.actorController.Move(actor.forward.normalized * setting.PushForce);
        }

        public static void OnVisualFX(ACTActor actor, ACTEventVisualFX setting)
        {
            ACTVisualFXManager.GetInstance().PlayACTEventVisualFX(setting, actor, Vector3.zero);
        }

        public static void OnSoundFX(ACTActor actor, ACTEventSoundFX setting)
        {
            actor.soundController.PlayHitSoundFX(setting);
        }
    }
}
