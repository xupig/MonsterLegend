﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using UnityEngine.Events;
using MogoEngine.RPC;
using ACTSystem;
using MogoEngine;
using EventType = ACTSystem.EventType ;
using GameLoader.Utils;
namespace GameMain.CombatSystem
{
    public class EventTypeComparer : IEqualityComparer<ACTSystem.EventType>
    {
        public bool Equals(EventType x, EventType y)
        {
            return x == y;
        }

        public int GetHashCode(EventType obj)
        {
            return (int)obj;
        }
    }

    public class ACTHandler
    {
        public const int EXTEND_TYPE_ACTION_ACTIVE_VISUAL_FX = 1;
        public const int EXTEND_TYPE_ACTION_JUDGE_VISUAL_FX = 2;
        public const int EXTEND_TYPE_ACTION_FIX_TIME_VISUAL_FX = 3;

        public Vector3 actionActiveOrigin = Vector3.zero;
        public Vector3 actionJudgeOrigin = Vector3.zero;

        public uint mainTargetID = 0;

        public VisualFXPriority priority = VisualFXPriority.L;

        protected ACTEventBeHitVisualFX _eventBeHitVisualFX;
        protected ACTEventStateChange _curStateChange;
        protected ACTEventBeHitSetting _curBeHitSetting;
        protected ACTEventBeHitSoundFX _curBeHitSound;
        protected ACTEventTracerVisualFX _curTracerVisualFX;

        protected ACTEventVisualFX _curJudgeVisualFX;
        protected ACTEventVisualFX _curFixTimeVisualFX;

        protected ACTEventSoundFX _curJudgeSound;

        protected ACTEventBulletVisualFX _curJudgeBulletVisualFX;
        protected ACTEventBulletVisualFX _curFixTimeBulletVisualFX;
        protected ACTEventTriggerVisualFX _eventTriggerVisualFX;
        protected ACTEventBeHitChangeMaterial _eventBeHitChangeMaterial;

        protected Dictionary<ACTSystem.EventType, Action<ACTActor, ACTEventBase, SkillEventData>> _eventTypeFuncMap;
        static readonly private Dictionary<ACTSystem.EventType, bool> _needCheckEventMap = new Dictionary<ACTSystem.EventType, bool> { 
            {EventType.VisualFX, true},
            {EventType.BeHitVisualFX, true},
            {EventType.WeaponState, true},
            {EventType.TracerVisualFX, true},
            {EventType.BulletVisualFX, true},
            {EventType.LaserVisualFX, true},
            {EventType.TriggerVisualFX, true},
            {EventType.GhostShadow, true},
            {EventType.BeHitChangeMaterial, true},
        };

        protected List<uint> _visualFXGUIDList = new List<uint>();
        public int qualitySettingValue = 0;

        public ACTHandler()
        {
            mainTargetID = 0;
            InitEnvetTypeFuncMap();
        }

        private void InitEnvetTypeFuncMap()
        {
            _eventTypeFuncMap = new Dictionary<ACTSystem.EventType, Action<ACTActor, ACTEventBase, SkillEventData>>(15, new EventTypeComparer());
            _eventTypeFuncMap.Add(EventType.AnimationFX, OnAnimationFX);
            _eventTypeFuncMap.Add(EventType.VisualFX, OnVisualFX);
            _eventTypeFuncMap.Add(EventType.SoundFX, OnSoundFX);
            _eventTypeFuncMap.Add(EventType.BeHitVisualFX, SetBeHitVisualFX);
            _eventTypeFuncMap.Add(EventType.BeHitSetting, SetBeHitSetting);
            _eventTypeFuncMap.Add(EventType.BeHitSoundFX, SetBeHitSound);
            _eventTypeFuncMap.Add(EventType.StateChange, OnStateChange);
            _eventTypeFuncMap.Add(EventType.WeaponState, OnWeaponState);
            _eventTypeFuncMap.Add(EventType.TracerVisualFX, SetTracerVisualFX);
            _eventTypeFuncMap.Add(EventType.BulletVisualFX, OnBulletVisualFX);
            _eventTypeFuncMap.Add(EventType.LaserVisualFX, OnLaserVisualFX);
            _eventTypeFuncMap.Add(EventType.TriggerVisualFX, SetTriggerVisualFX);
            _eventTypeFuncMap.Add(EventType.ChangeMaterial, OnChangeMaterial);
            _eventTypeFuncMap.Add(EventType.GhostShadow, OnGhostShadow);
            _eventTypeFuncMap.Add(EventType.BeHitChangeMaterial, SetBeHitChangeMaterial);
        }

        public void Clear()
        {
            _eventBeHitVisualFX = null;
            _curStateChange = null;
            _curBeHitSetting = null;
            _curBeHitSound = null;
            _curTracerVisualFX = null;
            _curJudgeVisualFX = null;
            _curJudgeSound = null;
            _curFixTimeVisualFX = null;
            _curJudgeBulletVisualFX = null;
            _curFixTimeBulletVisualFX = null;
            _eventTriggerVisualFX = null;

            actionActiveOrigin = Vector3.zero;
            actionJudgeOrigin = Vector3.zero;
            _visualFXGUIDList.Clear();
        }

        public void BreakAllVisualFX()
        {
            for (int i = 0; i < _visualFXGUIDList.Count; i++)
            {
                ACTVisualFXManager.GetInstance().StopVisualFX(_visualFXGUIDList[i]);
            }
            _visualFXGUIDList.Clear();
        }

        private void AddToVisualFXGUIDList(uint guid)
        {
            if (guid > 0) _visualFXGUIDList.Add(guid);
        }

        static private ACTEventStateChange _defualtStateChange = new ACTEventStateChange();
        public void ResetDefaultState(ACTActor actor)
        {
            if (actor == null) return;
            OnStateChange(actor, _defualtStateChange);
            ResetWeaponsState(actor);
        }

        public void ResetWeaponsState(ACTActor actor)
        {
            var weapons = actor.weaponController.GetWeapons();
            if (weapons == null) return;
            foreach (var keyValuePair in weapons)
            {
                actor.weaponController.SetWeaponTrailEmit(keyValuePair.Value.WeaponID, false);
            }
        }

        public float GetEventDelay(int mainID, int eventIdx)
        {
            ACTEventBase subFX = ACTRunTimeData.GetEventBySkillIDAndEventIdx(mainID, eventIdx);
            if (subFX == null) {
                if (UnityPropUtils.IsEditor) LoggerHelper.Error(string.Format("@策划 找不到对应的技能事件，ACTHandler GetEventDelay Error:{0}:{1}", mainID, eventIdx));
                return 0;
            }
            return subFX.Delay;
        }

        public void OnEvent(ACTActor actor, SkillEventData eventData)
        {
            if (actor == null) return;
            ACTEventBase subFX = ACTRunTimeData.GetEventBySkillIDAndEventIdx(eventData.mainID, eventData.eventIdx);
            //Debug.LogError("eventData.mainID = " + eventData.mainID + ", eventData.eventIdx=" + eventData.eventIdx);
            if (subFX == null)
            {
                if (UnityPropUtils.IsEditor) LoggerHelper.Error(string.Format("@策划 找不到对应的技能事件，ACTHandler OnEvent Error:{0}:{1}", eventData.mainID, eventData.eventIdx));
                return;
            }
            if (!CheckCanShow(actor, subFX.EventFXType)) return;
            if (_eventTypeFuncMap.ContainsKey(subFX.EventFXType))
            {
                _eventTypeFuncMap[subFX.EventFXType](actor, subFX, eventData);
            }
            else {
                LoggerHelper.Error("_eventTypeFuncMap Key error :" + subFX.EventFXType);
            }
        }

        #region 技能事件处理

        private void OnAnimationFX(ACTActor actor, ACTEventBase baseSetting, SkillEventData eventData = null)
        {
            var setting = baseSetting as ACTEventAnimationFX;
            actor.animationController.PlayAnimationFX(setting);
        }

        private void OnGhostShadow(ACTActor actor, ACTEventBase baseSetting, SkillEventData eventData = null)
        {
            if (PlayerSettingManager.curQualityLevel == ClientQualityLevel.LOW) return;
            var setting = baseSetting as ACTEventGhostShadow;
            var color = new Color(setting.XrayColorReal.x, setting.XrayColorReal.y, setting.XrayColorReal.z);
            int uid = setting.SkillID * 100 + setting.EventIndex;
            actor.equipController.GhostShadow(setting.Duration, setting.Interval, setting.FadeTime, setting.MinDistance, color, setting.Rim, setting.Inside, uid);
        }

        private void OnVisualFX(ACTActor actor, ACTEventBase baseSetting, SkillEventData eventData = null)
        {
            float locationRealY;
            var setting = baseSetting as ACTEventVisualFX;
            if (eventData.duration > 0)
            {
                setting.Duration = eventData.duration * 0.001f;
            }
            uint guid = 0;
            switch (eventData.extend)
            { 
                case EXTEND_TYPE_ACTION_ACTIVE_VISUAL_FX:
                    var newACTEventVisualFX = setting.Clone() as ACTEventVisualFX;
                    newACTEventVisualFX.LocationType = ACTVFXLocationType.CUSTOM_WORLD;
                    //
                    locationRealY = newACTEventVisualFX.LocationReal.y;      //先备份美术y数值
                    newACTEventVisualFX.LocationReal = new ACTVector3();     //使用技能编辑器里面的位置参数
                    newACTEventVisualFX.LocationReal.y = locationRealY;
                    guid = ACTVisualFXManager.GetInstance().PlayACTEventVisualFX(newACTEventVisualFX, actor, actionActiveOrigin, qualitySettingValue, priority);
                    break;
                case EXTEND_TYPE_ACTION_JUDGE_VISUAL_FX:
                    _curJudgeVisualFX = setting.Clone() as ACTEventVisualFX;
                    _curJudgeVisualFX.LocationType = ACTVFXLocationType.CUSTOM_WORLD;
                    //
                    locationRealY = _curJudgeVisualFX.LocationReal.y;      //先备份美术y数值
                    _curJudgeVisualFX.LocationReal = new ACTVector3();         
                    _curJudgeVisualFX.LocationReal.y = locationRealY;
                    break;
                case EXTEND_TYPE_ACTION_FIX_TIME_VISUAL_FX:
                    _curFixTimeVisualFX = setting.Clone() as ACTEventVisualFX;
                    _curFixTimeVisualFX.LocationType = ACTVFXLocationType.CUSTOM_WORLD;
                    //
                    locationRealY = _curFixTimeVisualFX.LocationReal.y;      //先备份美术y数值
                    _curFixTimeVisualFX.LocationReal = new ACTVector3();      //使用技能编辑器里面的位置参数
                    _curFixTimeVisualFX.LocationReal.y = locationRealY;
                    break;
                default:
                    guid = ACTVisualFXManager.GetInstance().PlayACTEventVisualFX(setting, actor, Vector3.zero, qualitySettingValue, priority);
                    break;
            }
            AddToVisualFXGUIDList(guid);
        }

        private void OnSoundFX(ACTActor actor, ACTEventBase baseSetting, SkillEventData eventData = null)
        {
            var setting = baseSetting as ACTEventSoundFX;

            if (eventData.extend != EXTEND_TYPE_ACTION_JUDGE_VISUAL_FX)
            {
                actor.soundController.PlayHitSoundFX(setting);
            }
            else
            {
                _curJudgeSound = setting;      //记录音效信息,判定时候使用
            }
        }

        private void SetBeHitVisualFX(ACTActor actor, ACTEventBase baseSetting, SkillEventData eventData = null)
        {
            var setting = baseSetting as ACTEventBeHitVisualFX;
            _eventBeHitVisualFX = setting;
        }

        private void SetBeHitSetting(ACTActor actor, ACTEventBase baseSetting, SkillEventData eventData = null)
        {
            var setting = baseSetting as ACTEventBeHitSetting;
            _curBeHitSetting = setting;
        }

        private void SetBeHitSound(ACTActor actor, ACTEventBase baseSetting, SkillEventData eventData = null)
        {
            var setting = baseSetting as ACTEventBeHitSoundFX;
            _curBeHitSound = setting;
        }

        private void SetTracerVisualFX(ACTActor actor, ACTEventBase baseSetting, SkillEventData eventData = null)
        {
            var setting = baseSetting as ACTEventTracerVisualFX;
            _curTracerVisualFX = setting;
        }

        protected void OnLaserVisualFX(ACTActor actor, ACTEventBase baseSetting, SkillEventData eventData = null)
        {
            var setting = baseSetting as ACTEventLaserVisualFX;
            Entity targetEntity = MogoWorld.GetEntity(mainTargetID);
            if (targetEntity == null || !(targetEntity is EntityCreature) || (targetEntity as EntityCreature).actor == null)
            {
                //LoggerHelper.Error("[OnLaserVisualFX]=>1_______________targetEntity:   " + targetEntity + ",mainTargetID:   " + mainTargetID);
                return;
            }
            //当技能行为表中的特效持续时间大于-1时就使用配表中的时间改写掉原来技能编辑器中的特效时间
            //LoggerHelper.Error("[OnLaserVisualFX]=>eventData.mainID:   " + eventData.mainID + "eventData.duration:    " + eventData.duration + ",setting.Duration:   " + setting.Duration);
            if (eventData.duration > 0)
            {
                setting.Duration = eventData.duration * 0.001f;
            }
            

            uint guid = ACTVisualFXManager.GetInstance().PlayACTLaser(setting,
                    actor, (targetEntity as EntityCreature).actor, null, qualitySettingValue);
            AddToVisualFXGUIDList(guid);
        }

        private void SetTriggerVisualFX(ACTActor actor, ACTEventBase baseSetting, SkillEventData eventData = null)
        {
            var setting = baseSetting as ACTEventTriggerVisualFX;
            _eventTriggerVisualFX = setting;
            actor.fxController.SetTriggerVisualFX(setting);
        }

        private void SetBeHitChangeMaterial(ACTActor actor, ACTEventBase baseSetting, SkillEventData eventData = null)
        {
            var setting = baseSetting as ACTEventBeHitChangeMaterial;
            _eventBeHitChangeMaterial = setting;
        }

        private void OnStateChange(ACTActor actor, ACTEventBase baseSetting, SkillEventData eventData = null)
        {
            var setting = baseSetting as ACTEventStateChange;
            _curStateChange = setting;
            var animator = actor.GetComponent<Animator>();
            if (animator != null)
            {
                animator.applyRootMotion = setting.ApplyRootMotion;
            }

            if (!string.IsNullOrEmpty(setting.SkinnedSlot))
            {
                string[] splitSlots = setting.SkinnedSlot.Split(',');
                for (int i = 0; i < splitSlots.Length; i++)
                {
                    Transform slot = actor.boneController.GetBoneByName(splitSlots[i]);
                    SkinnedMeshRenderer smr = slot.GetComponent<SkinnedMeshRenderer>();
                    if (smr)
                    {
                        smr.enabled = setting.ShowSkinned;
                    }
                }
            }
        }

        private void OnWeaponState(ACTActor actor, ACTEventBase baseSetting, SkillEventData eventData = null)
        {
            var setting = baseSetting as ACTEventWeaponState;
            for (int j = 0; j < setting.WeaponStateList.Count; j++)
            {
                var settingData = setting.WeaponStateList[j];
                actor.weaponController.SetWeaponTrailEmit(settingData.WeaponID, settingData.TrailEmit);
            }
        }

        protected void OnBulletVisualFX(ACTActor actor, ACTEventBase baseSetting, SkillEventData eventData = null)
        {
            var setting = baseSetting as ACTEventBulletVisualFX;
            switch (eventData.extend)
            {
                case EXTEND_TYPE_ACTION_ACTIVE_VISUAL_FX:
                    OnBulletVisualFXForActive(actor, setting);
                    break;
                case EXTEND_TYPE_ACTION_JUDGE_VISUAL_FX:
                    _curJudgeBulletVisualFX = setting.Clone() as ACTEventBulletVisualFX;
                    break;
                case EXTEND_TYPE_ACTION_FIX_TIME_VISUAL_FX:
                    _curFixTimeBulletVisualFX = setting.Clone() as ACTEventBulletVisualFX;
                    break;
                default:
                    break;
            }
        }

        protected void OnChangeMaterial(ACTActor actor, ACTEventBase baseSetting, SkillEventData eventData = null)
        {
            actor.materialAnimationController.PlayMaterialAnimation(baseSetting as ACTEventChangeMaterial);
        }

        #endregion 技能事件处理

        protected void OnBulletVisualFXForActive(ACTActor actor, ACTEventBulletVisualFX setting)
        {
            var newACTEventBulletVisualFX = setting.Clone() as ACTEventBulletVisualFX;
            Vector3 targetLocation = Vector3.zero;
            //设置攻击目标                
            targetLocation = newACTEventBulletVisualFX.targetLocation == ACTCalcTargetLocation.Custom ? actionActiveOrigin :
                           new Vector3(actionActiveOrigin.x, newACTEventBulletVisualFX.TargetPositionReal.y + actor.position.y, actionActiveOrigin.z);
            ACTVisualFXManager.GetInstance().PlayACTBullet(setting, actor, targetLocation, () =>
            {
                if (setting.HitVisualEvent > 0)
                {
                    ACTEventBase subFX = ACTRunTimeData.GetEventBySkillIDAndEventIdx(setting.SkillID, setting.HitVisualEvent);
                    if (subFX is ACTEventVisualFX)
                    {
                        ACTVisualFXManager.GetInstance().PlayACTEventVisualFX(subFX as ACTEventVisualFX, actor, targetLocation, qualitySettingValue);
                    }
                }
            }, qualitySettingValue);
        }

        void PlayVisualFXForFixPosition(ACTActor actor, ACTEventVisualFX fxSetting, Vector3 position)
        {
            if (position.x == 0 && position.z == 0) position = actor.position;
            ACTVisualFXManager.GetInstance().PlayACTEventVisualFX(fxSetting, actor, position, qualitySettingValue, priority);
        }

        void PlayBulletVisualFXForFixPosition(ACTActor actor, ACTEventBulletVisualFX fxSetting, Vector3 position)
        {
            Vector3 targetLocation;
            targetLocation = new Vector3(position.x, fxSetting.TargetPositionReal.y + actor.position.y, position.z);
            if (targetLocation.x == 0 && targetLocation.z == 0) targetLocation = actor.position;
            ACTVisualFXManager.GetInstance().PlayACTBullet(fxSetting, actor, targetLocation, () =>
            {
                ACTEventBase subFX = ACTRunTimeData.GetEventBySkillIDAndEventIdx(fxSetting.SkillID, fxSetting.HitVisualEvent);
                if (subFX is ACTEventVisualFX)
                {
                    ACTVisualFXManager.GetInstance().PlayACTEventVisualFX(subFX as ACTEventVisualFX, actor, targetLocation, qualitySettingValue, VisualFXPriority.L);
                }
            }, qualitySettingValue);
        }

        public void OnJudge(ACTActor actor)      //效果激活的时候
        {
            if (_curJudgeVisualFX != null)
            {
                PlayVisualFXForFixPosition(actor, _curJudgeVisualFX, actionJudgeOrigin);
            }
            if (_curJudgeBulletVisualFX != null)
            {
                PlayBulletVisualFXForFixPosition(actor, _curJudgeBulletVisualFX, actionJudgeOrigin);
            }
            //
            PlaySoundAtJudge(actor);
            
            
        }

        private void PlaySoundAtJudge(ACTActor actor)
        {
            if (_curJudgeSound != null)
            {
                actor.soundController.PlayHitSoundFX(_curJudgeSound);
                _curJudgeSound = null;
            }
        }

        public void OnFixTime(ACTActor actor)
        {
            if (_curFixTimeVisualFX != null)
            {
                PlayVisualFXForFixPosition(actor, _curFixTimeVisualFX, actionActiveOrigin);
            }
            if (_curFixTimeBulletVisualFX != null)
            {
                PlayBulletVisualFXForFixPosition(actor, _curFixTimeBulletVisualFX, actionActiveOrigin);
            }
        }

        public void OnTargetBeHit(ACTActor caster, EntityCreature target, UnityAction beHitActionCB = null)
        {
            if (target == null || target.actor == null) return;
            if (_curTracerVisualFX != null)
            {
                ACTVisualFXManager.GetInstance().PlayACTTracer(_curTracerVisualFX,
                    caster, target.actor, () => {
                        if (beHitActionCB != null) beHitActionCB();
                        OnTargetBeHitTwo(caster, target);
                    }, qualitySettingValue);
            }
            else
            {
                if (beHitActionCB != null) beHitActionCB();
                OnTargetBeHitTwo(caster, target);
            }
        }

        void OnTargetBeHitTwo(ACTActor caster, EntityCreature target)
        {
            if (target == null || target.actor == null) return;
            var targetEntity = CombatSystemTools.GetCreatureByID(target.actor.owner.GetActorOwnerID());
            if (targetEntity == null) return;
            if (!targetEntity.stateManager.CanDO(CharacterAction.HIT_ABLE)) return;
            HandleBeHitSetting(caster, target.actor);
            HandleBeHitVisualFX(caster, target.actor);
            HandleBeHitChangeMaterial(caster, target.actor);
            //播放特殊特效
            PlaySpecialFX(target);

            //播放受击音效
            if (_curBeHitSound != null) caster.soundController.PlayBeHitSoundFX(_curBeHitSound);
            //播放目标自身受击音效
            target.actor.soundController.PlaySelfBeHitSound();
        }

        private void PlaySpecialFX(EntityCreature target)
        {
            target.bufferManager.PlaySpecialFX();
        }

        void HandleBeHitVisualFX(ACTActor caster, ACTActor target)
        {
            if (_eventBeHitVisualFX != null)
            {
                ACTVisualFXManager.GetInstance().PlayACTEventVisualFX(_eventBeHitVisualFX, target, Vector3.zero, qualitySettingValue, VisualFXPriority.L, caster);
            }
        }

        void HandleBeHitChangeMaterial(ACTActor caster, ACTActor target)
        {
            if (_eventBeHitChangeMaterial != null)
            {
                target.materialAnimationController.PlayMaterialAnimation(_eventBeHitChangeMaterial as ACTEventBeHitChangeMaterial);
            }
        }

        void HandleBeHitSetting(ACTActor caster, ACTActor target)
        {
            if (_curBeHitSetting != null)
            {
                //var animationController = target.animationController;
                //var actorController = target.actorController;
                //actorController.Stop();
                //int beHitAction = target.actorState.OnGround ? _curBeHitSetting.BeHitAction : _curBeHitSetting.BeHitAirAction;
                //if (beHitAction > 0)
                //{
                //    animationController.PlayAction(beHitAction);
                //}
                if (_curBeHitSetting.HitTimeScaleKeep > 0)
                {
                    target.animationController.PauseTo((int)_curBeHitSetting.HitTimeScaleKeep);
                }
                if (_curBeHitSetting.HitTimeScale > 0)
                {
                    caster.animationController.PauseTo((int)_curBeHitSetting.HitTimeScale);
                }
                if (_curBeHitSetting.HitVisualEvent > 0)
                {
                    ACTEventBase subFX = ACTRunTimeData.GetEventBySkillIDAndEventIdx(_curBeHitSetting.SkillID, _curBeHitSetting.HitVisualEvent);
                    if (subFX is ACTEventChangeMaterial)
                    {
                        target.materialAnimationController.PlayMaterialAnimation(subFX as ACTEventChangeMaterial);
                    }
                }
            }
        }

        private bool CheckCanShow(ACTActor actor, EventType eventType)
        {
            if(actor.owner.IsPlayer()){
                if (PlayerSettingManager.hidePlayer && _needCheckEventMap.ContainsKey(eventType))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
