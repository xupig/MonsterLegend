﻿using System;
using System.Collections.Generic;


namespace GameMain
{
    public class SoundRecoverManager
    {
        public static string BGMUSIC = "BgMusic";
        public static string VOICE = "voice";
        //public static string SOUND = "sound";


        private static SoundRecoverManager _instance;
        public static SoundRecoverManager instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SoundRecoverManager();
                }
                return _instance;
            }
        }

        private Dictionary<string, SoundRecoverBase> _soundDict = null;

        public SoundRecoverManager()
        {
            _soundDict = new Dictionary<string, SoundRecoverBase>();
            _soundDict.Add(BGMUSIC, new BgMusicRecover());
            _soundDict.Add(VOICE, new SoundRecoverBase());
        }

        public SoundRecoverBase GetSoundRecover(string soundType)
        {
            if (!_soundDict.ContainsKey(soundType)) return null;
            return _soundDict[soundType];
        }

        public void ReleaseAll()
        {
            if (_soundDict == null) return;
            foreach (var item in _soundDict)
            {
                item.Value.Release();
            }
        }
        

    }
}
