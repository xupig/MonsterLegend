﻿using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;
using Common.ExtendTools;
using Common.Utils;
using GameResource;
using ACTSystem;

namespace GameMain
{
    public class SoundManager
    {
        private static SoundManager s_instance = null;
        public static SoundManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new SoundManager();
            }
            return s_instance;
        }

        GameObject _audioSourceSlot;

        public void SetRootGameObject(GameObject root)
        {
            if (_audioSourceSlot != null) return;
                _audioSourceSlot = new GameObject();
                _audioSourceSlot.name = "SND_SLOT";
                _audioSourceSlot.transform.SetParent(root.transform, false);
        }

        private SoundBase GetSound(int soundID)
        {
            return SoundPool.GetInstance().GetSound(_audioSourceSlot, soundID);
        }

        public uint PlaySound(int soundID,bool loop = false)
        {
            if (_audioSourceSlot == null) return 0;
            float settingSoundVolume = ACTSystem.ACTSystemDriver.GetInstance().soundVolume;
            if (Mathf.Approximately(settingSoundVolume, 0))
            {
                return 0;
            }
            SoundBase sound = GetSound(soundID);
            //添加优先级判定
            if (sound.JudgePriority(sound_helper.Priority(soundID)) == false) return 0;
            sound.Play(soundID, loop);
            return sound.id;
        }

        public bool StopSound(uint id)
        {
            SoundBase sound = SoundPool.GetInstance().GetSound(id);
            if (sound == null)
            {
                return false;
            }
            sound.Stop();
            return true;
        }

        public void ResetAllSound()
        {
            SoundPool.GetInstance().Reset();
        }

        /// <summary>
        /// 播放点击语音
        /// 语音播放后通过定时器设置一个CD时间
        /// </summary>
        /// <param name="soundID"></param>
        /// <param name="loop"></param>
        public void PlayClickVoice(int soundID, bool loop = false)
        {
            PlaySound(soundID, loop);
        }

        public void PreloadClips(int[] idList)
        {
            string[] clipPaths = new string[idList.Length];
            for (int i = 0; i < clipPaths.Length; i++)
            {
                clipPaths[i] = sound_helper.Path(idList[i]);
            }
            clipPaths = ObjectPool.Instance.GetResMappings(clipPaths);
            ObjectPool.Instance.PreloadAsset(clipPaths, () => { });
        }
    }
}
