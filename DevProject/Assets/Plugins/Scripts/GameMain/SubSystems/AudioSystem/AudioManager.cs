﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using GameMain;
using MogoEngine;

namespace GameMain
{
    public class AudioManager
    {
        private static AudioManager s_instance = null;
        public static AudioManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new AudioManager();
            }
            return s_instance;
        }

        private bool _isTempLoseVolume = false;
        public bool isTempLoseVolume
        {
            get { return _isTempLoseVolume; }

        }

        private AudioListener _audioListener;

        AudioManager()
        {
            
        }

        public void SetRootGameObject(GameObject root)
        {
            BgMusicManager.GetInstance().SetRootGameObject(root);
            SoundManager.GetInstance().SetRootGameObject(root);

            _audioListener = root.GetComponent<AudioListener>();
            if (_audioListener == null)
            {
                _audioListener = root.AddComponent<AudioListener>();
            }
        }

        public void SetAudioListener(bool state)
        {
            if (EntityPlayer.Player != null && EntityPlayer.Player.actor != null)
            {
                state = false;
            }
            if (_audioListener != null)
            {
                _audioListener.enabled = state;
            }
        }

        public void OnLoadGameSceneStart()
        {
            BgMusicManager.GetInstance().StopAllBgMusic();
            SoundRecoverManager.instance.ReleaseAll();
        }

        public void TempCloseVolume(bool includeBGMusic = true)
        {
            if (includeBGMusic) BgMusicAudioSource.GetInstance().TempCloseVolume();
            SoundPool.GetInstance().TempCloseVolume();
            TempCloseEntityVolume();
            _isTempLoseVolume = true;
        }
        public void ResetTempVolume(bool includeBGMusic = true)
        {
            if (includeBGMusic) BgMusicAudioSource.GetInstance().ResetTempVolume();
            SoundPool.GetInstance().ResetTempVolume();
            ResetTempEntityVolume();
            _isTempLoseVolume = false;
        }

        private void TempCloseEntityVolume()
        {
            EntityCreature entity;
            foreach (var item in MogoWorld.Entities)
            {
                if (!(item.Value is EntityCreature)) continue;
                entity = (item.Value as EntityCreature);
                if (entity.actor == null) continue;
                if (entity.actor.soundController == null) continue;
                entity.actor.soundController.TempCloseVolume();
            }
        }

        private void ResetTempEntityVolume()
        {
            EntityCreature entity;
            foreach (var item in MogoWorld.Entities)
            {
                if (!(item.Value is EntityCreature)) continue;
                entity = (item.Value as EntityCreature);
                if (entity.actor == null) continue;
                if (entity.actor.soundController == null) continue;
                entity.actor.soundController.ResetTempVolume();
            }
        }
    }
}

