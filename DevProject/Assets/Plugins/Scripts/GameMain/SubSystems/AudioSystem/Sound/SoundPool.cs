﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain
{
    public class SoundPool
    {
        private static SoundPool _instance = null;
        public static SoundPool GetInstance()
        {
            if (_instance == null)
             {
                 _instance = new SoundPool();
             }
            return _instance;
        }

        private Dictionary<uint, SoundBase> _soundDict;
        private List<SoundBase> _soundList;
        private SoundBase _appointSound = null;

        public Action BeginPlaySoundEvent;
        public Action StopPlaySoundEvent;

        public SoundPool()
        {
            _soundDict = new Dictionary<uint, SoundBase>();
            _soundList = new List<SoundBase>();
        }

        public SoundBase GetSound(GameObject soundSlot, int SoundID)
        {
           return SoundID < 0 ? GetAppointSound(soundSlot) : GetComSound(soundSlot);
        }

        public SoundBase GetSound(uint id)
        {
            return _soundDict.ContainsKey(id) ? _soundDict[id] : null;
        }

        private SoundBase GetAppointSound(GameObject soundSlot)
        {
            if (_appointSound != null) return _appointSound;

            _appointSound = new SoundBase(soundSlot);
            _appointSound.BeginPlaySoundEvent = BeginPlaySoundEvent;
            _appointSound.StopPlaySoundEvent = StopPlaySoundEvent;
            return _appointSound;
        }

        private SoundBase GetComSound(GameObject soundSlot)
        {
            SoundBase retSound = null;
            if (_soundDict.Count > 0)
            {
                SoundBase sound;
                for (int i = 0; i < _soundList.Count; i++)
                {
                    sound = _soundList[i];
                    if (sound.CanUse())
                    {
                        retSound = sound;
                        break;
                    }
                }
            }
            if (retSound == null)
            {
                retSound = new SoundBase(soundSlot);
                retSound.BeginPlaySoundEvent = BeginPlaySoundEvent;
                retSound.StopPlaySoundEvent = StopPlaySoundEvent;
                _soundDict.Add(retSound.id, retSound);
                _soundList.Add(retSound);
            }
            return retSound;
        }


        public void Reset()
        {
            SoundBase sound;
            foreach (var node in _soundDict)
            {
                sound = node.Value; 
                sound.Reset();
            }
        }

        public void TempCloseVolume()
        {
            if (_soundDict != null)
            {
                if (_soundDict.Count > 0)
                {
                    SoundBase sound;
                    for (int i = 0; i < _soundList.Count; i++)
                    {
                        sound = _soundList[i];
                        sound.ResetTempVolume();
                    }
                }
            }
            if (_appointSound != null)
            {
                _appointSound.TempCloseVolume();
            }
        }

        public void ResetTempVolume()
        {
            if (_soundDict != null)
            {
                if (_soundDict.Count > 0)
                {
                    SoundBase sound;
                    for (int i = 0; i < _soundList.Count; i++)
                    {
                        sound = _soundList[i];
                        sound.ResetTempVolume();
                    }
                }
            }
            if (_appointSound != null)
            {
                _appointSound.ResetTempVolume();
            }
        }

    }
}
