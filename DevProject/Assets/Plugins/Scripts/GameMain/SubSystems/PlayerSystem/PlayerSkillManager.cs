using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.CustomType;
using GameMain.CombatSystem;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;
using MogoEngine.RPC;

namespace GameMain.GlobalManager
{
    public class PlayerSkillData
    {
        public int skillID = 0;
        public float endTime = 0;
        public bool isChange = false;
        public float curTime = 0;
        public int lifeTime = 0;
    }

    public class PlayerSkillManager
    {
        private static Dictionary<int, int> _playerGroundSkillPosDict;
        private static Dictionary<int, int> _playerAirSkillPosDict;
        private static Dictionary<int, int> _playerFlySkillPosDict;
        private static Dictionary<int, int> _playerGroundTransformSkillPosDict = new Dictionary<int,int>();
        private static Dictionary<int, int> _playerAirTransformSkillPosDict = new Dictionary<int,int>();
        //总的技能按钮槽列表
        private static List<int> _playerSkillButtonPosList = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        private static int _maxSkillPosCount = 8;
        private static PlayerSkillManager s_instance = null;
        public static PlayerSkillManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PlayerSkillManager();
            }
            return s_instance;
        }

        private Dictionary<int, PlayerSkillData> _posCDEndTimeMap = new Dictionary<int, PlayerSkillData>();
        private Dictionary<int, int> _posToIconIdDict = new Dictionary<int, int>();
        private Dictionary<int, bool> _posToShowMPMaskDict = new Dictionary<int, bool>(); 

        private PlayerSkillManager()
        {
            InitPlayerTransformSkillPosDict();
        }

        public void OnPlayerEnterWorld()
        {
            EntityPlayer.Player.skillManager.onPosChange = OnPlayerSkillInfoChange;
        }

        public void OnPlayerLeaveWorld()
        {
            EntityPlayer.Player.skillManager.onPosChange = null;
            this.ClearData();
        }

        public void ClearData()
        {
            _posCDEndTimeMap.Clear();
            _posToIconIdDict.Clear();
            _posToShowMPMaskDict.Clear();
        }

        public void SetPlayerFlySkillPosDict(Dictionary<int, int> flySkillPosDict)
        {
            _playerFlySkillPosDict = flySkillPosDict;
        }

        public void SetPlayerGroundSkillPosDict(Dictionary<int,int> groundSkillPosDict)
        {
            _playerGroundSkillPosDict = groundSkillPosDict;
        }

        public void SetPlayerAirSkillPosDict(Dictionary<int, int> airSkillPosDict)
        {
            _playerAirSkillPosDict = airSkillPosDict;
        }

        public void InitPlayerTransformSkillPosDict()
        {
            for (int i = 0; i < _maxSkillPosCount; i++)
            {
                _playerGroundTransformSkillPosDict.Add(i + 1, 41 + i);
                _playerAirTransformSkillPosDict.Add(i + 1, 51 + i);
            }
        }

        Dictionary<int, int> _skillPosDict;
        public Dictionary<int, int> GetSkillPosDict(EntityCreature creature)
        {
            if (creature.InState(state_config.AVATAR_STATE_FLY) || creature.InState(state_config.AVATAR_STATE_GLIDE))
            {
                _skillPosDict = _playerFlySkillPosDict;
            }
            else if (creature.InState(state_config.AVATAR_STATE_TRANSFORM))
            {
                if (creature.IsOnAir())
                {
                    _skillPosDict = _playerAirTransformSkillPosDict;
                }
                else
                {
                    _skillPosDict = _playerGroundTransformSkillPosDict;
                }
            }
            else
            {
                if (creature.IsOnAir())
                {
                    //_skillPosDict = _playerAirSkillPosDict;
                    //暂时策划要求处理
                    _skillPosDict = _playerGroundSkillPosDict;
                }
                else
                {
                    _skillPosDict = _playerGroundSkillPosDict;
                }
            }
            return _skillPosDict;
        }

        public void OnPlayerSkillInfoChange()
        {
            OnSkillInfoChange(EntityPlayer.Player);
        }

        private float _curTime = 0;
        public bool isInit = false;
        private void OnSkillInfoChange(EntityCreature creature)
        {
            if (!isInit) return;
            var posSkillIDMap = creature.skillManager.GetCurPosSkillIDMap();
            Dictionary<int, int> skillPosDict = GetSkillPosDict(creature);
            for (int i = 0; i < _playerSkillButtonPosList.Count; i++)
            {
                int pos = _playerSkillButtonPosList[i];
                if (!skillPosDict.ContainsKey(pos)) continue;
                int slot = skillPosDict[pos];
                if (slot == 0)
                {
                    continue;
                }
                if (!_posToIconIdDict.ContainsKey(pos))
                {
                    _posToIconIdDict.Add(pos, 0);
                }
                if (!_posCDEndTimeMap.ContainsKey(pos))
                {
                    _posCDEndTimeMap[pos] = new PlayerSkillData();
                }
                if (posSkillIDMap.ContainsKey(slot))
                {
                    var curSkillID = posSkillIDMap[slot];
                    var endTime = creature.skillManager.GetCDEndTime(curSkillID);
                    _posCDEndTimeMap[pos].isChange = false;
                    if (_posCDEndTimeMap[pos].skillID != curSkillID)
                    {
                        _posCDEndTimeMap[pos].isChange = true;
                        _posCDEndTimeMap[pos].skillID = curSkillID;
                        int iconId = CombatLogicObjectPool.GetSkillData(curSkillID).iconId;
                        if (_posToIconIdDict[pos] != iconId)
                        {
                            _posToIconIdDict[pos] = iconId;
                            LuaDriver.instance.CallFunction("CSCALL_CHANGE_SKILL_ICON", pos, iconId);
                        }
                    }
                    if (_posCDEndTimeMap[pos].isChange && RealTime.time < endTime || _posCDEndTimeMap[pos].endTime != endTime)
                    {
                        _posCDEndTimeMap[pos].endTime = endTime;
                        int cd = CombatLogicObjectPool.GetSkillData(curSkillID).cd[0];
                        if (endTime >= 0)
                        {
                            var cdTime = endTime - RealTime.time;
                            if (cdTime > 0)
                            {
                                LuaDriver.instance.CallFunction("CSCALL_CHANGE_SKILL_CD", pos, cdTime * 1000, cd, 0);
                            }
                            else if (_posCDEndTimeMap[pos].isChange)
                            {
                                LuaDriver.instance.CallFunction("CSCALL_CHANGE_SKILL_CD", pos, 0, cd, 0);
                            }
                        }
                        else
                        {
                            //公共CD     
                            float cdTime = -endTime - RealTime.time;
                            int curCommonCdSkillId = creature.skillManager.GetCurCommonCdSkillId();
                            var data = CombatLogicObjectPool.GetSkillData(curCommonCdSkillId);
                            if (pos > 1)
                            {
                                LuaDriver.instance.CallFunction("CSCALL_CHANGE_SKILL_CD", pos, -cdTime * 1000, cd, 0);
                            }
                        }
                    }
                    if (creature.skillManager.IsLimitedSkill(curSkillID))
                    {                        
                        _curTime = creature.skillManager.GetCurLimitedSkillTime(curSkillID);
                        if (_posCDEndTimeMap[pos].isChange || _posCDEndTimeMap[pos].curTime != _curTime)
                        {
                            _posCDEndTimeMap[pos].curTime = _curTime;                            
                            int curTime = (int)(_curTime * 1000);
                            int roundTime = (int)(creature.skillManager.GetCanUseRoundTime(curSkillID) * 1000);
                            int totalTime = (int)(creature.skillManager.GetCanUseTimeTotal(curSkillID) * 1000);
                            int time = totalTime - curTime;
                            LuaDriver.instance.CallFunction("CSCALL_CHANGE_SKILL_CD", pos, time, totalTime, roundTime);
                        }
                    }
                }
                else
                {
                    if (_posToIconIdDict[pos] != 0)
                    {
                        _posCDEndTimeMap[pos].skillID = 0;
                        _posToIconIdDict[pos] = 0;
                        LuaDriver.instance.CallFunction("CSCALL_CHANGE_SKILL_ICON", pos, 0);
                        if (_posCDEndTimeMap[pos].endTime - RealTime.time > 0)
                        {
                            LuaDriver.instance.CallFunction("CSCALL_CHANGE_SKILL_CD", pos, 0, 0, 0);
                        }
                    }
                }
            }
        }

        #region  响应服务器释放技能操作
        private bool CanDiscard(EntityCreature creature)
        {
            if (creature == null || creature.actor == null || creature.isHideMonster) //|| creature.isHideEntity
            {
                return true;
            }
            if (!creature.IsPlayer() && PlayerSettingManager.hideOtherPlayer) return true;
            return false;
        }

        public void SelfCastSpellResp(byte[] pbByte)
        {
            PbSpellSelfCastSpellResp respData = GameProtoUtils.ParseProto<PbSpellSelfCastSpellResp>(pbByte);
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.MSGID_CLIENT_RPC_RESP, "SelfCastSpellResp spell_id=" + respData.spell_id + ",target_id=" + respData.target_id);
            }
            EntityPlayer.Player.skillManager.OnSkillPlayByServer(respData);
        }

        public void OtherCastSpellResp(byte[] pbByte)
        {
            PbSpellOtherCastSpellResp respData = GameProtoUtils.ParseProto<PbSpellOtherCastSpellResp>(pbByte);
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(respData.caster_id);
            if (CanDiscard(creature) || creature.isClientControlServerEntity) return;
            if (Vector3.Distance(creature.position, EntityPlayer.Player.position) > PlayerSettingManager.GetInstance().battleDistanceLimit &&
                !CombatLogicObjectPool.GetSkillData((int)respData.spell_id).needRecord) return; //距离远的情况下不处理对应的包
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.MSGID_CLIENT_RPC_RESP, "OtherCastSpellResp spell_id=" + respData.spell_id + ",target_id=" + respData.target_id);
            }
            creature.skillManager.PlaySkillByServer(respData); //不是跟随者走创建新技能流程
        }

        public void SpellActionResp(byte[] pbSpellActionByte)
        {

            PbSpellAction spellAction = GameProtoUtils.ParseProto<PbSpellAction>(pbSpellActionByte);
            if (spellAction == null) return;
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(spellAction.caster_id);
            if (CanDiscard(creature)) return;
            if (Vector3.Distance(creature.position, EntityPlayer.Player.position) > PlayerSettingManager.GetInstance().battleDistanceLimit &&
                !CombatLogicObjectPool.GetSkillActionData((int)spellAction.action_id).needRecord) return; //距离远的情况下不处理对应的包
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.MSGID_CLIENT_RPC_RESP, "SpellActionResp spell_id=" + spellAction.spell_id + ",target_id=" + spellAction.target_id + ",action_id" + spellAction.action_id);
            }
            creature.skillManager.ActiveSkillActionByServer(spellAction);
        }

        public void SpellDamageResp(byte[] pbByte)//(UInt32 attackerID, UInt16 skillID, UInt16 skillActionID, byte[] skillDamageByte)
        {
            PbSpellDamageInfo skillDamageInfo = GameProtoUtils.ParseProto<PbSpellDamageInfo>(pbByte);
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(skillDamageInfo.caster_id);
            if (CanDiscard(creature)) return;
            if (Vector3.Distance(creature.position, EntityPlayer.Player.position) > PlayerSettingManager.GetInstance().battleDistanceLimit &&
                !CombatLogicObjectPool.GetSkillActionData((int)skillDamageInfo.action_id).needRecord) return; //距离远的情况下不处理对应的包
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.MSGID_CLIENT_RPC_RESP, "SpellDamageResp spell_id=" + skillDamageInfo.spell_id + ",caster_id=" + skillDamageInfo.caster_id + ",action_id" + skillDamageInfo.action_id);
            }
            creature.skillManager.JudgeSkillActionByServer((UInt16)skillDamageInfo.spell_id, (UInt16)skillDamageInfo.action_id, skillDamageInfo);
        }

        public void OtherSpellActionResp(byte[] pbOtherSpellAction)
        {
            //PbOtherSpellAction otherSpellAction = MogoProtoUtils.ParseProto<PbOtherSpellAction>(pbOtherSpellAction);
        }

        public void SpellActionOriginResp(byte[] pbSpellActionOrigin)
        {
            PbSpellActionOrigin spellActionOrigin = GameProtoUtils.ParseProto<PbSpellActionOrigin>(pbSpellActionOrigin);
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(spellActionOrigin.caster_id);
            if (CanDiscard(creature)) return;
            if (Vector3.Distance(creature.position, EntityPlayer.Player.position) > PlayerSettingManager.GetInstance().battleDistanceLimit &&
                !CombatLogicObjectPool.GetSkillActionData((int)spellActionOrigin.action_id).needRecord) return; //距离远的情况下不处理对应的包
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.MSGID_CLIENT_RPC_RESP, "SpellActionOriginResp spell_id=" + spellActionOrigin.spell_id + ",caster_id=" + spellActionOrigin.caster_id + ",action_id" + spellActionOrigin.action_id);
            }
            creature.skillManager.OnSetActionOriginByServer(spellActionOrigin);
        }

        public void FollowerCastSpellResp(UInt16 errorCode, UInt32 attackerID, UInt16 skillID, UInt32 targetID)
        {
            /*
            if (errorCode > 0)
            {
                PlayerAvatar.Player.OnActionResp(0, errorCode, new LuaTable());
                if (errorCode == (ushort)error_code.ERR_CLIENT_TIME_TICK)
                {
                    LoggerHelper.Info(string.Format("RefreshLocalTimeStamp, localTimeStamp = {0}, serverTimeStamp = {1}",
                        Global.localTimeStamp, Global.serverTimeStamp));
                    RefreshLocalTimeStamp();
                }
                return;
            }
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(attackerID);
            if (creature == null) return;
            var pos = creature.position;
            creature.skillManager.OnFollowerSkillPlayByServer(skillID, (short)(pos.x * 100), (short)(pos.z * 100), creature.face, targetID);*/
        }

        public void SpellCdChangeResp(byte[] data)
        {
            PbCdChangeSpells pbCdChangeSpells = GameProtoUtils.ParseProto<PbCdChangeSpells>(data);
            var cdDataList = pbCdChangeSpells.cd_change_spells;
            for (int i = 0; i < cdDataList.Count; i++)
            {
                var unit = cdDataList[i];
                switch (unit.type)
                {
                    case 0://技能ID
                        EntityPlayer.Player.skillManager.ChangeSkillGroupCD((int)unit.spell_id, unit.change_cd);
                        break;
                    case 1://组ID
                        EntityPlayer.Player.skillManager.ChangeCD((int)unit.spell_id, unit.change_cd);
                        break;
                    default:
                        break;
                }
            }
        }
        //Boss部位破坏
        public void MonsterPartDestroyResp(byte[] data)
        {

        }

        #endregion

    }
}
