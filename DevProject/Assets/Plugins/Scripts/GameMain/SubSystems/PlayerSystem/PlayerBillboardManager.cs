﻿using System;
using System.Collections.Generic;
using UnityEngine;
using MogoEngine;
using MogoEngine.Events;
using GameLoader.Utils;
using Common.Events;
using Common.States;
using GameData;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using GameLoader.Utils.Timer;
using MogoEngine.RPC;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using GameMain.ClientConfig;

namespace GameMain.CombatSystem
{
    public class PlayerBillboardManagerUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public enum MonsterType
    {
        Normal = 1,
        Elite = 2,
        NormalBoss = 3,
        WorldBoss = 4,
        SleipmonBoss = 5,
    }

    public class PlayerBillboardManager
    {
        EntityCreature _owner;
        public int bloodMissTime = 0;
        int _bossBloodDistance;
        int _updateFrame = 10;
        int _curFrame = 0;
        private Dictionary<uint, Entity> entities;
        private List<uint> entityIDs;
        Entity entity;
        //目前只处理只有一个BOSS
        uint _bossEID = 0;
        bool _refreshBoss = true;

        public PlayerBillboardManager(EntityCreature owner)
        {
            _owner = owner;
            bloodMissTime = int.Parse(global_params_helper.GetGlobalParam(51));
            _bossBloodDistance = int.Parse(global_params_helper.GetGlobalParam(50));
            MogoEngine.MogoWorld.RegisterUpdate<PlayerBillboardManagerUpdateDelegate>("PlayerBillboardManager.Update", Update);
            entities = MogoWorld.Entities;
            entityIDs = MogoWorld.EntityIDs;
        }

        private uint _entityId = 0;
        private float _endTime = 0;
        private void Update()
        {
            _curFrame++;
            if (_curFrame < _updateFrame)
            {
                return;
            }
            _curFrame = 0;

            for (int index = 0; index < _owner.showEntityList.Count; index++)
            {
                _entityId = _owner.showEntityList[index];
                _endTime = _owner.showEntityDict[_entityId];
                if (_endTime > 0 && RealTime.time > _endTime)
                {
                    _owner.showEntityDict.Remove(_entityId);
                    _owner.showEntityList.Remove(_entityId);
                }
            }

            _refreshBoss = true;
            if (_bossEID != 0 && entityIDs.Contains(_bossEID))
            {
                entity = entities[_bossEID] as Entity;
                if (Vector3.Distance(_owner.position, entity.position) < _bossBloodDistance)
                {
                    _refreshBoss = false;
                }
            }

            if (_refreshBoss)
            {
                for (int i = 0; i < entityIDs.Count; i++)
                {
                    entity = entities[entityIDs[i]] as Entity;
                    if ((entity.entityType == EntityConfig.ENTITY_TYPE_NAME_MONSTER || entity.entityType == EntityConfig.ENTITY_TYPE_NAME_DUMMY)
                        && ((entity as EntityCreature).monsterType == (int)MonsterType.NormalBoss || (entity as EntityCreature).monsterType == (int)MonsterType.WorldBoss ||
                            (entity as EntityCreature).monsterType == (int)MonsterType.SleipmonBoss))
                    {
                        if (Vector3.Distance(_owner.position, entity.position) < _bossBloodDistance)
                        {
                            if (entity.id != _bossEID)
                            {
                                _bossEID = entity.id;
                                LuaDriver.instance.CallFunction("CSCALL_CONTROL_BOSSSTATE", true, _bossEID);
                            }
                        }
                        else
                        {
                            if (_bossEID == entity.id)
                            {
                                LuaDriver.instance.CallFunction("CSCALL_CONTROL_BOSSSTATE", false);
                                _bossEID = 0;
                            }
                        }
                    }
                }
            }

            if (_bossEID != 0 && !entityIDs.Contains(_bossEID))
            {
                _bossEID = 0;
                LuaDriver.instance.CallFunction("CSCALL_CONTROL_BOSSSTATE", false);
            }
        }
    }
}
