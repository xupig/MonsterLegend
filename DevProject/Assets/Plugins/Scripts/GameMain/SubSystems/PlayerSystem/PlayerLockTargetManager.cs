using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using MogoEngine.Events;
using GameLoader.Utils;
using Common.Events;
using Common.States;
using GameData;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using GameLoader.Utils.Timer;
using MogoEngine.RPC;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using GameMain.ClientConfig;
using Common.ServerConfig;

namespace GameMain.CombatSystem
{
    public class EnemyAvatar 
    {
        private uint _eid = 0;
        public uint eid
        {
            get { return _eid; }
            set 
            {
                if (value != _eid)
                {
                    _eid = value;
                    if (trigger)
                    {
                        LuaDriver.instance.CallFunction("CSCALL_ON_ENEMY_AVATAR_ID_CHANGE", index, _eid);
                    }
                }
            }
        }

        private bool _attackState = false;
        public bool attackState
        {
            get { return _attackState; }
            set
            {
                if (value != _attackState)
                {
                    _attackState = value;
                    if (_attackState)
                    {
                        if (trigger)
                        {
                            LuaDriver.instance.CallFunction("CSCALL_ON_ENEMY_AVATAR_STATE_CHANGE", index, 1);
                        }
                    }
                    else 
                    {
                        if (trigger)
                        {
                            LuaDriver.instance.CallFunction("CSCALL_ON_ENEMY_AVATAR_STATE_CHANGE", index, 0);
                        }
                    }
                }
            }
        }

        public int index = 1;
        public bool trigger = false;

        public EnemyAvatar(int idx, bool trigger = false)
        {
            index = idx;
            this.trigger = trigger;
        }
    }

    public class PlayerLockTargetManagerUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class PlayerLockTargetManager
    {
        EntityCreature _owner;
        float _lockDis = 15f;
        float _releaseDis = 18f;
        float _lockAngle = 90f;
        float _lockLength = 0.5f;
        float _lockEndTime = 0;
        float _lockDuration = 5;

        public List<EnemyAvatar> enemyAvatarList = new List<EnemyAvatar>();
        public List<EnemyAvatar> enemyAvatarTriggerList = new List<EnemyAvatar>();  

        public PlayerLockTargetManager(EntityCreature owner)
        {
            _owner = owner;
            var listFloat = global_params_helper.GetListFloat(GlobalParamId.lockTargetDis);
            _lockDis = listFloat[0];
            _releaseDis = listFloat[1];
            _lockLength = listFloat[2];
            _lockAngle = listFloat[3];

            _lockDuration = int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.camera_lock_target_time)) * 0.001f;

            _searchRange = global_params_helper.player_ai_search_range;

            InitArrow();
            MogoEngine.MogoWorld.RegisterUpdate<PlayerLockTargetManagerUpdateDelegate>("LockTargetManager.Update", Update);

            enemyAvatarList.Add(new EnemyAvatar(1));
            enemyAvatarList.Add(new EnemyAvatar(2));
            enemyAvatarList.Add(new EnemyAvatar(3));

            enemyAvatarTriggerList.Add(new EnemyAvatar(1, true));
            enemyAvatarTriggerList.Add(new EnemyAvatar(2, true));
            enemyAvatarTriggerList.Add(new EnemyAvatar(3, true));
        }

        public void Release()
        {
            MogoEngine.MogoWorld.UnregisterUpdate("LockTargetManager.Update", Update);
            _owner = null;
            if (arrow)
            {
                GameObject.Destroy(arrow.gameObject);
            }
        }

        Vector3 defaultPos = new Vector3(-1000, -1000, -1000);
        Transform arrow = null;
        EntityCreature newTarget = null;
        EntityCreature lockTarget = null;
        public void InitArrow()
        {
            //no ResourceMappingManager
            GameResource.ObjectPool.Instance.GetGameObject(Utils.PathUtils.GuidArrowPath, (go) =>
            {
                UnityEngine.Object.DontDestroyOnLoad(go);
                go.transform.position = defaultPos;
                go.transform.localScale = go.transform.localScale * 0.5f;
                arrow = go.transform;
            });
        }

        bool _inSafeArea = false;
        public bool inSafeArea
        {
            get { return _inSafeArea; }
            set
            {
                if (value != _inSafeArea)
                {
                    if (value)
                    {
                        _owner.SetActorMode(0);
                        EntityPlayer.Player.RpcCall("set_fight_idle_state_req", 0);
                        ClearTarget();
                    }
                    else
                    {
                        _owner.SetActorMode(1);
                        EntityPlayer.Player.RpcCall("set_fight_idle_state_req", 1);
                    }
                    _owner.SetAttr("inSafeArea", value);
                    LuaDriver.instance.CallFunction("CSCALL_CONTROL_UI", !value, 2);
                    _inSafeArea = value;
                    _owner.actor.inSafeArea = value;
                }
            }
        }

        //强制刷新一次状态
        public void ForceRefreshInSafeArea()
        {
            if (_inSafeArea)
            {
                _owner.SetActorMode(0);
                EntityPlayer.Player.RpcCall("set_fight_idle_state_req", 0);
            }
            else
            {
                _owner.SetActorMode(1);
                EntityPlayer.Player.RpcCall("set_fight_idle_state_req", 1);
            }
            _owner.SetAttr("inSafeArea", _inSafeArea);
            LuaDriver.instance.CallFunction("CSCALL_CONTROL_UI", !_inSafeArea, 2);
            _owner.actor.inSafeArea = _inSafeArea;
        }

        public void LockTarget(float duration = 5f)
        {
            FindNewTarget();
            if (_owner.target_id == 0) return;
            _lockEndTime = duration + RealTime.time;
        }

        public void BattleAutoLockTarget()
        {
            if (_owner.target_id == 0) LockTarget(_lockDuration);
        }

        private float lastTime = 0;
        private void Update()
        {
            if (_owner == null || _owner.actor == null) return;

            if (UnityPropUtils.realtimeSinceStartup - lastTime > 0.2f)
            {
                CheckEnemyAvatar();
                lastTime = UnityPropUtils.realtimeSinceStartup;
            }

            //安全区检测
            inSafeArea = SafeAreaManager.GetInstance().IsSafeArea(_owner.position.x, _owner.position.z);

            if (_owner.target_id == 0) return;
            UpdateArrow();
            if (_lockEndTime > 0 && RealTime.time > _lockEndTime)
            {
                _lockEndTime = -1;
                ClearTarget();
                return;
            }
            CheckOldTarget();
        }

        public void SetTarget(uint targetId)
        {
            if (targetId == 0)
            {
                lockTarget = null;
            }
            else
            {
                lockTarget = MogoWorld.Entities[targetId] as EntityCreature;
                if (lockTarget == null || lockTarget.actor == null) return;
            }
            UpdateArrow();
        }

        private void UpdateArrow()
        {
            if (arrow != null)
            {
                if (lockTarget != null && lockTarget.actor != null)
                {
                    arrow.position = lockTarget.actor.boneController.GetBoneByName("slot_billboard").position;
                }
                else
                {
                    arrow.position = defaultPos;
                }
            }
        }

        public void ClearTarget()
        {
            if (_owner.target_id != 0)
            {
                _owner.target_id = 0;
                lockTarget = null;
                UpdateArrow();
            }
        }

        private bool CheckOldTarget()
        {
            if (_owner.target_id == 0) return false;
            EntityCreature target = MogoWorld.GetEntity(_owner.target_id) as EntityCreature;
            if (target == null || target.clearTarget || !target.CanSearch())
            {
                ClearTarget();
                return false;
            }
            else
            {
                if ((target.position - EntityPlayer.Player.position).magnitude > _releaseDis)
                {
                    ClearTarget();
                    return false;
                }
                return true;
            }
        }

        #region 敌方玩家目标
        public void OnMapIdChange()
        {
            ResetEnemyAvatarList();
        }

        private bool deleteState = false;
        public void CheckEnemyAvatar()
        {
            if (!map_helper.GetPvpShow(_owner.map_id))
            {
                return;
            }
            int count = 0;
            deleteState = false;
            for (int i = 0; i < enemyAvatarList.Count; i++)
            {
                if (IsEnemyAvatarTarget(enemyAvatarList[i].eid))
                {
                    count++;
                }
                else if(enemyAvatarList[i].eid != 0)
                {
                    enemyAvatarList[i].eid = 0;
                    enemyAvatarList[i].attackState = false;
                    deleteState = true;
                }
            }
            if (deleteState)
            {
                SortEnemyAvatar();
            }
            if (count == enemyAvatarList.Count) return;

            List<uint> entityIDs = MogoWorld.EntityIDs;
            Dictionary<uint, Entity> entities = MogoWorld.Entities;
            for (int i = 0; i < entityIDs.Count; i++)
            {
                EntityCreature creature = entities[entityIDs[i]] as EntityCreature;
                if (!CanBeTarget(creature)) continue;
                if (creature.entityType != EntityConfig.ENTITY_TYPE_NAME_AVATAR) continue;
                var newDis = (creature.position - _owner.position).magnitude;
                if (newDis > _lockDis) continue;
                if (IsExistInEnemyAvatarList(creature.id)) continue;
                if (GetEnemyAvatarCount() == enemyAvatarList.Count) return;
               
                SetEnemyAvatar(creature.id);
            }
        }

        private void SetEnemyAvatar(uint eid)
        {
            for (int k = 0; k < enemyAvatarList.Count; k++)
            {
                if (enemyAvatarList[k].eid == 0)
                {
                    enemyAvatarList[k].eid = eid;
                    enemyAvatarList[k].attackState = false;
                    break;
                }
            }
            SortEnemyAvatar();
        }

        private void SortEnemyAvatar()
        {
            ResetEnemyAvatarTriggerList();
            int index = 0;
            for (int k = 0; k < enemyAvatarList.Count; k++)
            {
                if (enemyAvatarList[k].attackState && enemyAvatarList[k].eid > 0)
                {
                    index = GetCanSetEnemyAvatarTriggerIndex();
                    enemyAvatarTriggerList[index].eid = enemyAvatarList[k].eid;
                    enemyAvatarTriggerList[index].attackState = enemyAvatarList[k].attackState;
                }
            }

            for (int k = 0; k < enemyAvatarList.Count; k++)
            {
                if (!enemyAvatarList[k].attackState && enemyAvatarList[k].eid > 0)
                {
                    index = GetCanSetEnemyAvatarTriggerIndex();
                    enemyAvatarTriggerList[index].eid = enemyAvatarList[k].eid;
                    enemyAvatarTriggerList[index].attackState = enemyAvatarList[k].attackState;
                }
            }
        }

        private int GetCanSetEnemyAvatarTriggerIndex()
        {
            for (int k = 0; k < enemyAvatarTriggerList.Count; k++)
            {
                if (enemyAvatarTriggerList[k].eid == 0)
                {
                    return k;
                }
            }
            return 0;
        }

        private void ResetEnemyAvatarTriggerList()
        {
            for (int i = 0; i < enemyAvatarTriggerList.Count; i++)
            {
                enemyAvatarTriggerList[i].eid = 0;
                enemyAvatarTriggerList[i].attackState = false;
            }
        }

        private bool IsEnemyAvatarTarget(uint eid)
        {
            Entity creature;
            if (MogoWorld.Entities.TryGetValue(eid, out creature))
            {
                var newDis = (creature.position - _owner.position).magnitude;
                if ((creature as EntityCreature).cur_hp > 0 && CanBeTarget((creature as EntityCreature), false) && newDis <= _lockDis) return true;
            }
            return false;
        }

        private bool IsExistInEnemyAvatarList(uint eid)
        {
            for (int i = 0; i < enemyAvatarList.Count; i++)
            {
                if (enemyAvatarList[i].eid == eid)
                {
                    return true;
                }
            }
            return false;
        }

        private int GetEnemyAvatarCount()
        {
            int count = 0;
            for (int i = 0; i < enemyAvatarList.Count; i++)
            {
                if (IsEnemyAvatarTarget(enemyAvatarList[i].eid))
                {
                    count++;
                }
            }
            return count;
        }

        public void RefreshEnemyAvatarAttackState(uint eid)
        {
            for (int i = 0; i < enemyAvatarList.Count; i++)
            {
                if (enemyAvatarList[i].eid == eid)
                {
                    enemyAvatarList[i].attackState = true;
                }
            }
            SortEnemyAvatar();
        }

        private void ResetEnemyAvatarList()
        {
            for (int i = 0; i < enemyAvatarList.Count; i++)
            {
                enemyAvatarList[i].eid = 0;
                enemyAvatarList[i].attackState = false;
            }
            SortEnemyAvatar();
        }
        #endregion

        private void FindNewTarget()
        {
            float minDis = 0;
            uint minDisCreature = 0;
            newTarget = null;
            List<uint> entityIDs = MogoWorld.EntityIDs;
            Dictionary<uint, Entity> entities = MogoWorld.Entities;

            for (int i = 0; i < entityIDs.Count; i++)
            {
                EntityCreature creature = entities[entityIDs[i]] as EntityCreature;

                if (!CanBeTarget(creature)) continue;

                var newDis = (creature.position - _owner.position).magnitude;
                if (newDis > _lockDis) continue;
                if (minDisCreature == 0)
                {
                    minDis = newDis;
                    minDisCreature = creature.id;
                    newTarget = creature;
                }
                else if (newDis < minDis)
                {
                    minDis = newDis;
                    minDisCreature = creature.id;
                    newTarget = creature;
                }
            }
            if (newTarget != null)
            {
                _owner.target_id = minDisCreature;
                //_owner.actor.FaceTo(newTarget.position);
            }
        }

        public void FindTargetBySwitchButton()
        {
            float playerToTargetDis = 0;
            if (_owner.target_id != 0)
            {
                EntityCreature targetEntity = MogoWorld.Entities[_owner.target_id] as EntityCreature;
                playerToTargetDis = (targetEntity.position - _owner.position).magnitude;
            }

            uint minDisCreature = 0;
            uint largerDisCreature = 0;
            float minDis = -1;
            float largerDis = -1;

            List<uint> entityIDs = MogoWorld.EntityIDs;
            Dictionary<uint, Entity> entities = MogoWorld.Entities;
            for (int i = 0; i < entityIDs.Count; i++)
            {
                EntityCreature creature = entities[entityIDs[i]] as EntityCreature;

                if (!CanBeTarget(creature)) continue;

                var newDis = (creature.position - _owner.position).magnitude;
                if (newDis > _lockDis) continue;

                if (minDis == -1)
                {
                    minDis = newDis;
                    minDisCreature = creature.id;
                }

                if (minDis > newDis)
                {
                    minDis = newDis;
                    minDisCreature = creature.id;
                }

                if (largerDis == -1 && playerToTargetDis < newDis)
                {
                    largerDis = newDis;
                    largerDisCreature = creature.id;
                }

                if (playerToTargetDis < newDis && newDis < largerDis)
                {
                    largerDis = newDis;
                    largerDisCreature = creature.id;
                }

            }

            if (largerDisCreature != 0)
            {
                _owner.target_id = largerDisCreature;
            }
            else
            {
                _owner.target_id = minDisCreature;
            }
        }

        private bool CanBeTarget(EntityCreature creature, bool exceptSame = true)
        {
            //在安全区：不选择任何目标
            if (inSafeArea) return false;
            if (creature == null) return false;
            if (creature.GetTransform() == null) return false;
            if (creature.IsPlayer()) return false;
            if (exceptSame && creature.id == _owner.target_id) return false;
            if (creature.CanSearch() == false) return false;
            if (creature.cur_hp <= 0) return false;
            if (SafeAreaManager.GetInstance().IsSafeArea(creature.position.x, creature.position.z)) return false;
            if (creature.entityType == EntityConfig.ENTITY_TYPE_NAME_NPC) return false;
            if (creature.entityType == EntityConfig.ENTITY_TYPE_NAME_FABAO) return false;
            if (creature.entityType == EntityConfig.ENTITY_TYPE_NAME_PET) return false;
            if (creature.entityType == EntityConfig.ENTITY_TYPE_NAME_PERI) return false;
            if ((creature.entityType == EntityConfig.ENTITY_TYPE_NAME_MONSTER || creature.entityType == EntityConfig.ENTITY_TYPE_NAME_DUMMY)
                && creature.faction_id != _owner.faction_id)
            {
                //非自己阵营怪物所有情况都可选
                return true;
            }

            //不启用pk模式，只能选怪物
            if (!map_helper.GetUsePkMode(_owner.map_id))
            {
                return false;
            }

            //在安全区：玩家只能选择怪物，不选择玩家
            if (creature.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR && !inSafeArea)
            {
                //开启红名模式时，在3种PK模式中，都能选择红名玩家，与怪物同样处理
                if (map_helper.GetUseRedName(_owner.map_id) && creature.pk_value > 0)
                {
                    return true;
                }

                //如果两个玩家等级相差100级以上，且不开启等级限制时，两人互相不能选择
                if (ACTSystem.ACTMathUtils.Abs(creature.level - _owner.level) > 100 && map_helper.GetHasKillLevelLimit(_owner.map_id))
                {
                    return false;
                }

                if (_owner.pk_mode == 1)
                {
                    //玩家可以选择怪物和非同盟、非同队玩家，不选择同盟或同队玩家
                    if (creature.faction_id != _owner.faction_id)
                    {
                        return true;
                    }
                    if ((creature.team_id != _owner.team_id || (creature.team_id == 0 && _owner.team_id == 0)) &&
                        (creature.guild_name != _owner.guild_name || (String.IsNullOrEmpty(creature.guild_name) && String.IsNullOrEmpty(_owner.guild_name))))
                    {
                        return true;
                    }
                }

                if (_owner.pk_mode == 2)
                {
                    //攻击全体
                    return true;
                }
            }

            return false;
        }

        public void OnTouchEntity(uint id)
        {
            EntityCreature target = MogoWorld.GetEntity(id) as EntityCreature;
            if (CanBeTarget(target))
            {
                _owner.target_id = id;
            }
        }

        private float _searchRange = 10f;
        public void FindTargetByAI()
        {
            if (inSafeArea)
            {
                ClearTarget();
            }
            if (_owner.target_id != 0)
            {
                if (!MogoWorld.Entities.ContainsKey(_owner.target_id))
                {
                    return;
                }
                EntityCreature targetEntity = MogoWorld.Entities[_owner.target_id] as EntityCreature;
                if (CanBeTarget(targetEntity, false))
                {
                    return;
                }
            }

            uint readNameId = 0;
            uint monsterId = 0;
            uint friendId = 0;
            uint enemyId = 0;

            List<uint> entityIDs = MogoWorld.EntityIDs;
            Dictionary<uint, Entity> entities = MogoWorld.Entities;

            //选中最近的怪物
            float minDis = _searchRange + 1;
            for (int i = 0; i < entityIDs.Count; i++)
            {
                EntityCreature creature = entities[entityIDs[i]] as EntityCreature;
                if (!CanBeTarget(creature)) continue;
                var newDis = (creature.position - _owner.position).magnitude;
                if (newDis > _searchRange) continue;
                if ((creature.entityType == EntityConfig.ENTITY_TYPE_NAME_MONSTER || creature.entityType == EntityConfig.ENTITY_TYPE_NAME_DUMMY)
                    && creature.faction_id != _owner.faction_id)
                {
                    if (newDis < minDis)
                    {
                        minDis = newDis;
                        monsterId = creature.id;
                    }
                }
            }

            //优先非阵营的红名玩家
            for (int i = 0; i < entityIDs.Count; i++)
            {
                EntityCreature creature = entities[entityIDs[i]] as EntityCreature;
                if (!CanBeTarget(creature)) continue;
                var newDis = (creature.position - _owner.position).magnitude;
                if (newDis > _searchRange) continue;

                if (friendId == 0 && creature.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR && creature.faction_id == _owner.faction_id)
                {
                    friendId = creature.id;
                }
                if (enemyId == 0 && creature.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR && creature.faction_id != _owner.faction_id)
                {
                    enemyId = creature.id;
                }
                if (map_helper.GetUseRedName(_owner.map_id) && creature.pk_value > 0 && creature.faction_id != _owner.faction_id)
                {
                    readNameId = creature.id;
                }
            }

            if (readNameId != 0)
            {
                _owner.target_id = readNameId;
                return;
            }
            if (enemyId != 0)
            {
                _owner.target_id = enemyId;
                return;
            }
            if (friendId != 0)
            {
                _owner.target_id = friendId;
                return;
            }
            if (monsterId != 0)
            {
                _owner.target_id = monsterId;
                return;
            }

        }
    }
}