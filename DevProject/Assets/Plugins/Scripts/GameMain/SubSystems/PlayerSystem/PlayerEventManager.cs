﻿using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using MogoEngine.RPC;
using MogoEngine.Events;
using GameLoader.Utils.Timer;
using Common.Events;
using Common.States;
using GameData;
using Common.ServerConfig;
using Common.ClientConfig;
using GameLoader.Utils;
using MogoEngine.Mgrs;
using GameMain.ClientConfig;


namespace GameMain
{
    public class PlayerEventManagerUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class PlayerEventManager
    {
        EntityPlayer _owner;

        private static PlayerEventManager s_instance = null;
        public static PlayerEventManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PlayerEventManager();
            }
            return s_instance;
        }

        private int _syncPosSpaceFrame = 3; //更新间隔（单位：帧）。
        private float _syncPosNextTime = 0;
        private float _attackStateTimeStamp = 0;
        private float _attackStateTimeInterval = 5f; //脱离攻击状态时间间隔
        private float _notMoveStateTimeInterval = 10f; //检测待机状态时间间隔
        private float _lastTime = 0;

        private PlayerEventManager()
        {
            _owner = EntityPlayer.Player;
        }

        public void OnPlayerEnterWorld()
        {
            _owner = EntityPlayer.Player;
            MogoWorld.RegisterUpdate<PlayerSyncPosManagerUpdateDelegate>("PlayerEventManager.Update", Update);
            _attackStateTimeInterval = global_params_helper.no_fight_state_time;
            _notMoveStateTimeInterval = global_params_helper.no_move_state_time;
            moveStateTimeStamp = UnityPropUtils.realtimeSinceStartup;
        }

        public void OnPlayerLeaveWorld()
        {
            MogoWorld.UnregisterUpdate("PlayerEventManager.Update", Update);
        }

        private bool InMovingState()
        {
            if (_owner.actor == null) return false;
            return ControlStickState.strength > 0 || _owner.actor.animationController.GetSpeed() != 0;
        }

        public void Update()
        {
            _syncPosNextTime--;
            if (_syncPosNextTime <= 0)
            {
                _syncPosNextTime = _syncPosSpaceFrame;
                if (InMovingState())
                {
                    //触发移动
                    BreakMapTeleportSing();
                    BreakCollectSing();
                }
            }
            if (UnityPropUtils.realtimeSinceStartup - _attackStateTimeStamp > _attackStateTimeInterval)
            {
                _owner.inAttackState = false;
            }
            if (UnityPropUtils.realtimeSinceStartup - _lastTime > 1)
            {
                _lastTime = UnityPropUtils.realtimeSinceStartup;
                OneSecondTick();
            }
        }

        private Vector3 lastPos = Vector3.zero;
        private float moveStateTimeStamp = 0;

        void OneSecondTick()
        {
            if (Vector3.Distance(_owner.position, lastPos) > 0.1f || ControlStickState.strength > 0)
            {
                moveStateTimeStamp = UnityPropUtils.realtimeSinceStartup;
            }
            lastPos = _owner.position;
            if (_owner.isOpenAutoFightCheck && UnityPropUtils.realtimeSinceStartup - moveStateTimeStamp > _notMoveStateTimeInterval)
            {
                //待机到时启动自动战斗，不用受击触发
                LuaDriver.instance.CallFunction("CSCALL_AUTO_FIGHT");
            }
        }

        //检测传送吟唱打断
        public void BreakMapTeleportSing()
        {
            if (!_owner.isTeleportSing)
            {
                return;
            }
            LuaDriver.instance.CallFunction("CSCALL_BREAK_TELEPORT_SING");
        }

        //检测采集吟唱打断
        public void BreakCollectSing()
        {
            if (!_owner.isCollectSing)
            {
                return;
            }
            LuaDriver.instance.CallFunction("CSCALL_BREAK_COLLECT_SING");
        }

        //被攻击调用
        public void BeHit(uint eid = 0)
        {
            BreakMapTeleportSing();
            BreakCollectSing();

            EntityCreature enemy = MogoWorld.GetEntity(eid) as EntityCreature;
            bool isAvatar = enemy.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR;
            if(isAvatar)
            {
                _owner.lockTargetManager.RefreshEnemyAvatarAttackState(eid);
                _owner.hitOrBeHitTime = UnityPropUtils.realtimeSinceStartup;
            }
            //策划先要注释
            ////玩家和怪物受击区分
            //if (isAvatar)
            //{
            //    if (UnityPropUtils.realtimeSinceStartup - moveStateTimeStamp > 2f) //定死被其他玩家攻击2秒响应
            //    {
            //        LuaDriver.instance.CallFunction("CSCALL_AUTO_FIGHT");
            //    }
            //}
            //else
            //{
            //    //长时间待机被攻击启动自动战斗
            //    if (UnityPropUtils.realtimeSinceStartup - moveStateTimeStamp > _notMoveStateTimeInterval)
            //    {
            //        LuaDriver.instance.CallFunction("CSCALL_AUTO_FIGHT");
            //    }
            //}

            //长时间待机被攻击启动自动战斗
            if (UnityPropUtils.realtimeSinceStartup - moveStateTimeStamp > _notMoveStateTimeInterval)
            {
                LuaDriver.instance.CallFunction("CSCALL_AUTO_FIGHT");
            }
        }

        //死亡调用
        public void OnDeath()
        {
            BreakMapTeleportSing();
            BreakCollectSing();
        }

        //击中目标造成伤害调用
        public void OnHitAndDamageOther(uint eid = 0)
        {
            _owner.lockTargetManager.BattleAutoLockTarget();
            Entity target = MogoWorld.GetEntity(eid);
            if (target != null && target.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR)
            {
                //攻击其他玩家
                _owner.hitOrBeHitTime = UnityPropUtils.realtimeSinceStartup;
            }
        }

        //释放技能调用
        public void CastSpell()
        {
            _attackStateTimeStamp = UnityPropUtils.realtimeSinceStartup;
            _owner.inAttackState = true;

            if (_owner.rideManager != null && _owner.rideManager.ride != null)
            {
                //骑行时释放技能下马
                LuaDriver.instance.CallFunction("CSCALL_RIDE_HIDE");
            }
        }
    }
}