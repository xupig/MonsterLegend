﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;
using Common.ServerConfig;
using MogoEngine;
using MogoEngine.Events;
using Common.Events;
using GameData;
using Common.ClientConfig;
using GameResource;

namespace GameMain
{
    public class PreloadManagerUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }


    //游戏预加载管理器
    //针对某些特定场合对资源进行预加载
    public class PreloadManager
    {
        private static PreloadManager s_instance = null;
        public static PreloadManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PreloadManager();
            }
            return s_instance;
        }

        List<PreloadResource> _preloadStrategys = new List<PreloadResource>();
        PreloadManager()
        {
            InspectorSetting.InspectingTool.Inspect(new InspectingObjectPool(ObjectPool.Instance), GameObject.Find("_ObjectPoolProxy"));
        }

        public void OnPlayerEnterWorld()
        {
            MogoWorld.RegisterUpdate<PreloadManagerUpdateDelegate>("PreloadManager.Update", Update);
        }

        public void OnPlayerLeaveWorld()
        {
            MogoWorld.UnregisterUpdate("PreloadManager.Update", Update);
        }

        public void ClearPreloadInBackground()
        {
            _preloadStrategys.Clear();
        }

        public void AddBackgroundPreloadPaths(uint priority, string[] paths)
        {
            var re = new PreloadResource(priority, paths);
            int idx = 0;
            for (int i = 0; i < _preloadStrategys.Count; i++)
            {
                if (re.priority > _preloadStrategys[i].priority)
                {
                    break;
                }
                idx++;
            }
            _preloadStrategys.Insert(idx, re);
        }

        int _spaceTime = 0;
        int DEFAULT_SPACE_TIME = 5; //加载间隔时间
        void Update()
        {
            if (_spaceTime > 0) { _spaceTime--; return; }
            _spaceTime = DEFAULT_SPACE_TIME;
            if (_preloadStrategys.Count == 0) return;
            if (!_preloadStrategys[0].Update())
            {
                _preloadStrategys.RemoveAt(0);
            }
        }

        
    }

}
