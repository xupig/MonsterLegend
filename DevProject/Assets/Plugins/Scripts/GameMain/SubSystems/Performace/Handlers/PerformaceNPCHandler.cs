using System.Collections.Generic;
using UnityEngine;

using GameMain.CombatSystem;
namespace GameMain
{
    public class PerformaceNPCHandler : PerformaceBaseHandler
    {
        List<EntityCreature> _npcList;
        int _handleFrameNum = 30;
        int _frameNum = 0;
        public static bool isRunning = true;
        private static readonly float _showDistance = 15f;

        public PerformaceNPCHandler()
        {
            _npcList = new List<EntityCreature>();
        }

        override public void Update()
        {
            if (!isRunning) return;
            _frameNum++;
            if (_frameNum < _handleFrameNum)
            {
                return;
            }
            _frameNum = 0;
            Vector3 playerPos = EntityPlayer.Player.position;
            for (int i = _npcList.Count - 1; i >= 0; i--)
            {
                EntityCreature npc = _npcList[i];
                if (npc != null)
                {
                    if (Vector3.Distance(playerPos, npc.position) < _showDistance)
                    {
                        if (npc.IsDefaultModel())
                        {
                            npc.OnEnterDistance();
                        }
                        if(npc.actor!=null)
                            npc.actor.enabled = true;
                    }
                    else 
                    {
                        // �������npc��������
                        if (npc.actor != null)
                            npc.actor.enabled = false;
                    }
                }
                else
                {
                    _npcList.RemoveAt(i);
                }
            }
        }

        public void OnNPCActorLoaded(EntityCreature npc)
        {
            if (!isRunning) return;
            _npcList.Add(npc);
        }

        public void OnNPCLeaveWorld(EntityCreature npc)
        {
            _npcList.Remove(npc);
        }
        
    }
}