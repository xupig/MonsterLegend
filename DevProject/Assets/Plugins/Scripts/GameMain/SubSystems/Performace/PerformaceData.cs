using UnityEngine;

using GameMain.CombatSystem;
namespace GameMain
{
    public struct PerformaceDamageData
    { 
        public Vector3 position;
        public int damage;
        public int type;
        public bool isPlayer;
    }
}