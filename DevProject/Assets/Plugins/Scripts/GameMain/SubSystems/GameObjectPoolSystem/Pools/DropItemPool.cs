using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using GameData;
using GameLoader.Utils;
namespace GameMain
{
    public class DropItemPoolUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class DropItemPool
    {
        GameObject _poolObj;
        Dictionary<int, Queue<GameObject>> _typeIdGameObjectMap = new Dictionary<int, Queue<GameObject>>();
        Queue<Action> _delayCreateCallbacks = new Queue<Action>();
        public DropItemPool(GameObject mainPoolObj)
        {
            _poolObj = new GameObject("DropItemPool");
            _poolObj.SetActive(false);
            _poolObj.transform.SetParent(mainPoolObj.transform, false);
            MogoWorld.RegisterUpdate<DropItemPoolUpdateDelegate>("DropItemPool.Update", Update);
        }

        public void Release()
        {
            MogoWorld.UnregisterUpdate("DropItemPool.Update", Update);
        }

        void Update()
        {
            if (_delayCreateCallbacks.Count > 0)
            {
                Action action = _delayCreateCallbacks.Dequeue();
                action();
            }
        }

        Queue<GameObject> GetGameObjectQueue(int typeId)
        {
            if (!_typeIdGameObjectMap.ContainsKey(typeId))
            {
                _typeIdGameObjectMap[typeId] = new Queue<GameObject>();
            }
            return _typeIdGameObjectMap[typeId];
        }

        string[] GetResourcePaths(int typeId, bool canBePicked)
        {
            /*
            var modelPath = GameData.item_helper.GetDropModelPath((int)typeId, canBePicked);
            var effectPath = GameData.item_helper.GetDropEffectPath((int)typeId);
            if (!string.IsNullOrEmpty(modelPath))
            {
                if (!string.IsNullOrEmpty(effectPath))
                {
                    return new string[] { modelPath, effectPath };
                }
                else
                {
                    return new string[] { modelPath };
                }
            }*/
            return null;
        }

        public void CreateDropItemGameObject(int typeId, bool canBePicked, Action<GameObject> callback)
        {
            _delayCreateCallbacks.Enqueue(() =>
            {
                CreateDropItemGameObjectImmediately(typeId, canBePicked, callback);
            });
        }

        public void CreateDropItemGameObjectImmediately(int typeId, bool canBePicked, Action<GameObject> callback)
        {
            var queue = GetGameObjectQueue(typeId);
            if (queue.Count > 0)
            {
                var obj = queue.Dequeue();
                obj.transform.SetParent(null);
                //obj.SetActive(true);
                callback(obj);
                return;
            }
            var resourcePaths = GetResourcePaths(typeId, canBePicked);
            if (resourcePaths != null)
            {
                resourcePaths = GameResource.ObjectPool.Instance.GetResMappings(resourcePaths);
                GameResource.ObjectPool.Instance.GetGameObjects(resourcePaths, (objs) =>
                {
                    var model = objs[0];
                    if (model == null) LoggerHelper.Error(resourcePaths[0] + " load error! ");
                    if (objs.Length >= 2 && objs[1] != null)
                    {
                        var effect = objs[1];
                        if (model == null)
                        {
                            GameObject.Destroy(effect);
                            return;
                        }
                        effect.transform.SetParent(model.transform, false);
                    }
                    callback(model);
                });
            }
            else
            {
                LoggerHelper.Error("itemId " + typeId + " does not have a match modelPath");
            }
        }

        public void ReleaseDropItemGameObject(int typeId, GameObject gameObject)
        {
            var queue = GetGameObjectQueue(typeId);
            gameObject.transform.SetParent(_poolObj.transform);
            gameObject.transform.position = Vector3.zero;
            gameObject.name = typeId.ToString();
            //gameObject.SetActive(false);
            queue.Enqueue(gameObject);
        }

        public void Clear()
        {
            var childCount = _poolObj.transform.childCount;
            for (int i = childCount - 1; i >= 0; i--)
            {
                var go = _poolObj.transform.GetChild(i);
                GameObject.Destroy(go.gameObject);
            }
            _typeIdGameObjectMap.Clear();
        }
    }
}