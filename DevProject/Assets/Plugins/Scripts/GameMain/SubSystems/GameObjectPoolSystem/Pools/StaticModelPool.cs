using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using GameData;
using GameLoader.Utils;
namespace GameMain
{
    public class StaticModelPoolUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class StaticModelPool
    {
        GameObject _poolObj;
        Dictionary<int, Queue<GameObject>> _modelIdGameObjectMap = new Dictionary<int, Queue<GameObject>>();
        Queue<Action> _delayCreateCallbacks = new Queue<Action>();
        public StaticModelPool(GameObject mainPoolObj)
        {
            _poolObj = new GameObject("StaticModelPool");
            _poolObj.SetActive(false);
            _poolObj.transform.SetParent(mainPoolObj.transform, false);
            MogoWorld.RegisterUpdate<StaticModelPoolUpdateDelegate>("StaticModelPool.Update", Update);
        }

        public void Release()
        {
            MogoWorld.UnregisterUpdate("StaticModelPool.Update", Update);
        }

        void Update()
        {
            if (_delayCreateCallbacks.Count > 0)
            {
                Action action = _delayCreateCallbacks.Dequeue();
                action();
            }
        }

        Queue<GameObject> GetGameObjectQueue(int modelId)
        {
            if (!_modelIdGameObjectMap.ContainsKey(modelId))
            {
                _modelIdGameObjectMap[modelId] = new Queue<GameObject>();
            }
            return _modelIdGameObjectMap[modelId];
        }

        string GetResourcePath(int modelId)
        {
            return statics_model_helper.GetModelPath(modelId);
        }

        public void CreateStaticModelGameObject(int modelId, Action<GameObject> callback)
        {
            _delayCreateCallbacks.Enqueue(() =>
            {
                CreateStaticModelGameObjectImmediately(modelId, callback);
            });
        }

        public void CreateStaticModelGameObjectImmediately(int modelId, Action<GameObject> callback)
        {
            var queue = GetGameObjectQueue(modelId);
            if (queue.Count > 0)
            {
                var obj = queue.Dequeue();
                obj.transform.SetParent(null);
                callback(obj);
                return;
            }
            var resourcePath = GetResourcePath(modelId);
            if (resourcePath != null)
            {
                resourcePath = GameResource.ObjectPool.Instance.GetResMapping(resourcePath);
                GameResource.ObjectPool.Instance.GetGameObject(resourcePath, (obj) =>
                {
                    var model = obj;
                    if (model == null) LoggerHelper.Error(resourcePath + " load error! ");
                    StaticModel modelComp = model.AddComponent<StaticModel>();
                    modelComp.modelID = modelId;
                    callback(model);
                });
            }
            else
            {
                LoggerHelper.Error("modelId " + modelId + " does not have a match modelPath");
            } 
        }

        public void ReleaseStaticModelGameObject(int modelId, GameObject gameObject)
        {
            var queue = GetGameObjectQueue(modelId);
            gameObject.transform.SetParent(_poolObj.transform);
            gameObject.transform.position = Vector3.zero;
            gameObject.name = modelId.ToString();
            queue.Enqueue(gameObject);
        }

        public void Clear()
        {
            var childCount = _poolObj.transform.childCount;
            for (int i = childCount - 1; i >= 0; i--)
            {
                var go = _poolObj.transform.GetChild(i);
                GameObject.Destroy(go.gameObject);
            }
            _modelIdGameObjectMap.Clear();
        }
    }
}