using System;
using UnityEngine;
using ACTSystem;

namespace GameMain
{
    public class GameObjectPoolManager
    {
        private static GameObjectPoolManager s_instance = null;
        public static GameObjectPoolManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new GameObjectPoolManager();
            }
            return s_instance;
        }

        GameObject _poolObj;
        StaticModelPool _staticModelPool;
        ActorPool _actorPool;
        private GameObjectPoolManager()
        {
            _poolObj = new GameObject("GameObjectPool");
            _poolObj.AddComponent<GameObjectPoolDriver>();
            GameObject.DontDestroyOnLoad(_poolObj);
            _staticModelPool = new StaticModelPool(_poolObj);
            _actorPool = new ActorPool(_poolObj);
        }

        public void CreateStaticModelGameObject(int typeId, Action<GameObject> callback)
        {
            _staticModelPool.CreateStaticModelGameObject(typeId, callback);
        }

        public void ReleaseStaticModelGameObject(int typeId, GameObject gameObject)
        {
            _staticModelPool.ReleaseStaticModelGameObject(typeId, gameObject);
        }

        public void CreateActorGameObject(int actorId, Action<ACTActor> callback, bool immediately = false)
        {
            _actorPool.CreateActorGameObject(actorId, callback, immediately);
        }

        public void ReleaseActorGameObject(int actorId, GameObject gameObject)
        {
            _actorPool.ReleaseActorGameObject(actorId, gameObject);
        }

        public void Clear()
        {
            _staticModelPool.Clear();
            _actorPool.Clear();
        }

    }
}