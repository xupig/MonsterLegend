using System;
using UnityEngine;
using ACTSystem;
using GameLoader.IO;
using System.Collections.Generic;

namespace GameMain
{
    public class FindPathManager
    {
        private const string DATA_BLOCK_MAPS = "data/block_maps/s";
      
        private static FindPathManager s_instance = null;
        public static FindPathManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new FindPathManager();
            }
            return s_instance;
        }

        private BlockMap _blockMap;
        public BlockMap blockMap
        {
            get { return _blockMap; }
        }

        public void LoadBlockMap(string sceneName)
        {
            string path = string.Concat(DATA_BLOCK_MAPS, sceneName, ConstString.BM_SUFFIX);
            byte[] byteContent = FileAccessManager.LoadBytes(path);
            if (byteContent == null)
            {
                if(sceneName != "1000")
                {
                    string info = string.Format("[{0}] is not Configure block_map(skip or forget?)", sceneName);
                    GameLoader.Utils.LoggerHelper.Error(info);
                }
                return;
            }
            _blockMap = new BlockMap(byteContent);
        }

        public List<float> pathList = new List<float>();
        //A*Ѱ·
        public List<float> GetPath(float x1, float z1, float x2, float z2, bool bFly = false)
        {
            if (_blockMap != null)
            {
                pathList.Clear();
                var retrunPathList = _blockMap.FindPath((int)x1, (int)z1, (int)x2, (int)z2, bFly);
                if (retrunPathList != null)
                {
                    for (int i = 0; i < retrunPathList.Count; i++)
                    {
                        pathList.Add(retrunPathList[i]);
                    }
                }
                //DrawPath(pathList, 8);
                return pathList;
            }
            return null;
        }

        public bool CheckCurrPointIsCanMove(float posx, float posz, bool bFly = false)
        {
            if (_blockMap == null) return true;
            return _blockMap.IsWay(posx, posz, bFly);
        }

        public Vector3 CheckPointAroundCanMove(float posx, float posz, bool bFly = false, bool largeRangeSearch = false)
        {
            if (_blockMap == null) return Vector3.zero;
            return _blockMap.CheckPointAroundCanMove(posx, posz, bFly, largeRangeSearch);
        }

        public byte[,] GetBlockMapData()
        {
            if (_blockMap != null)
            {
                return _blockMap.GetBlockMapData();
            }
            return null;
        }

        public List<float> result = new List<float>();
        public List<float> FixPath(List<float> path)
        {
            result.Clear();
            Vector3 lastDirection = Vector3.zero;
            Vector3 lastPoint = Vector3.zero;
            Vector3 nextPoint = Vector3.zero;
            int pointCount = path.Count / 2;
            for (int i = 0; i < pointCount; i++)
            {
                nextPoint.x = path[i * 2];
                nextPoint.y = 0;
                nextPoint.z = path[i * 2 + 1];
                if (lastPoint == Vector3.zero)
                {
                    result.Add(nextPoint.x);
                    result.Add(nextPoint.z);
                }
                else
                {
                    var newDirection = nextPoint - lastPoint;
                    if (newDirection != lastDirection)
                    {
                        result.Add(nextPoint.x);
                        result.Add(nextPoint.z);
                        lastDirection = newDirection;
                    }
                    else if (pointCount - 1 == i)
                    {
                        result.Add(nextPoint.x);
                        result.Add(nextPoint.z);
                    }
                }
                lastPoint = nextPoint;
            }
            return result;
        }

        public void DrawPath(List<float> path, float y = 0)
        {
            List<float> result = new List<float>();
            Vector3 lastDirection = Vector3.zero;
            Vector3 lastPoint = Vector3.zero;
            Vector3 nextPoint = Vector3.zero;
            //Debug.LogError("DrawPath: =====" + path.Count);
            for (int i = 0; i < path.Count / 2; i++)
            {
                nextPoint.x = path[i * 2];
                nextPoint.y = y;
                nextPoint.z = path[i * 2 + 1];
                if (lastPoint != Vector3.zero)
                {
                    Debug.DrawLine(lastPoint, nextPoint, Color.blue, 10);
                }
                //Debug.LogError(nextPoint);
                lastPoint = nextPoint;
            }
        }

        public bool CanLine(Vector3 posA, Vector3 posB, bool bFly = false)
        {
            //return true;//没有正式资源，设置碰撞也很麻烦，先临时全都支线走  Ash
            bool result = true;
            posA.y = 0;
            posB.y = 0;
            var vector = posB - posA;
            var dis = vector.magnitude;
            var dir = vector.normalized;
            float step = 0.3f;
            int count = (int)(dis / step);
            for (int i = 0; i < count; i++)
            {
                float newPosX = posA.x + (dir.x * i * step);
                float newPosZ = posA.z + (dir.z * i * step);
                if (!CheckCurrPointIsCanMove(newPosX, newPosZ, bFly))
                {
                    result = false;
                    break;
                }
            }
            return result;
        }
    }
}