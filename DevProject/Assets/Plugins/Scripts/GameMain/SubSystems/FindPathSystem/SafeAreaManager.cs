﻿using System;
using UnityEngine;
using ACTSystem;
using GameLoader.IO;
using System.Collections.Generic;
using System.IO;

namespace GameMain
{
    public class SafeAreaMap
    {
        public const byte BLOCK_TYPE_NORMAL = 0;    //安全区域

        byte[,] _mapData;
        int _xMax;
        int _zMax;
        int _origin_x;
        int _origin_z;

        public SafeAreaMap(byte[] data)
        {
            Init(data);
        }

        public void Init(byte[] data)
        {
            uint cursor = 0;
            int xl = data[cursor++];
            int xh = data[cursor++];
            int zl = data[cursor++];
            int zh = data[cursor++];

            _xMax = xl + (xh << 8);
            _zMax = zl + (zh << 8);
            _mapData = new byte[_xMax, _zMax];

            xl = data[cursor++];
            xh = data[cursor++];
            zl = data[cursor++];
            zh = data[cursor++];
            _origin_x = xl + (xh << 8);
            _origin_z = zl + (zh << 8);

            for (int i = 0; i < _xMax; ++i)
            {
                for (int j = 0; j < _zMax; ++j)
                {
                    _mapData[i, j] = data[cursor++];
                }
            }
        }

        public byte[,] GetSafeAreaMapData()
        {
            return _mapData;
        }

        public bool IsSafeArea(float posx, float posz)
        {
            int xpos = Mathf.RoundToInt(posx);
            int zpos = Mathf.RoundToInt(posz);
            //转换相对坐标
            xpos -= _origin_x;
            zpos -= _origin_z;
            return IsSafeArea(xpos, zpos);
        }

        private bool IsSafeArea(int x, int z)
        {
            if (OutOfRange(x, z)) return false;
            return _mapData[x, z] == BLOCK_TYPE_NORMAL;
        }

        private bool OutOfRange(int x, int z)
        {
            return x < 0 || x >= _xMax || z < 0 || z >= _zMax;
        }
    }

    public class SafeAreaManager
    {
        private const string DATA_BLOCK_MAPS = "data/block_maps/s";
        private const string SAFE_AREA_MAP_SUFFIX = ".sam";
      
        private static SafeAreaManager s_instance = null;
        public static SafeAreaManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new SafeAreaManager();
            }
            return s_instance;
        }

        private SafeAreaMap _safeAreaMap;
        public SafeAreaMap safeAreaMap
        {
            get { return _safeAreaMap; }
        }

        public void LoadSafeAreaMap(string sceneName)
        {
            string path = string.Concat(DATA_BLOCK_MAPS, sceneName, SAFE_AREA_MAP_SUFFIX);
            byte[] byteContent = FileAccessManager.LoadBytes(path);
            if (byteContent == null)
            {
                _safeAreaMap = null;
                return;
            }
            _safeAreaMap = new SafeAreaMap(byteContent);
        }

        public bool IsSafeArea(float posx, float posz)
        {
            if (_safeAreaMap == null)
            {
                return false;
            }
            return _safeAreaMap.IsSafeArea(posx, posz);
        }
    }
}