﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using GameMain;
using Common.ClientConfig;
using GameResource;
using System.IO;
using GameLoader;
using GameLoader.Utils;
using MogoEngine;
using GameData;
using System.Collections;
using GameLoader.IO;
using LitJson;
using GameLoader.Mgrs;

namespace GameMain
{
    public class UploadLogManager
    {
        private static UploadLogManager s_instance = null;
        public static UploadLogManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new UploadLogManager();
            }
            return s_instance;
        }

        UploadLogManager()
        {

        }

        public void UploadInfo()
        {
            string log = JsonMapper.ToJson(UploadLogMgr.GetInstance().logHead);
            //LoggerHelper.Info(string.Format("<color=#00ff00>UploadInfo: {0} </color>", log));
            MogoEngine.MogoWorld.RpcCall("upload_client_info", log);
        }

        //log_type:1
        public void UploadLog(string[] data)
        {
            UploadLogMgr.GetInstance().UploadLog(data);
            //if (EntityPlayer.Player == null)
            //{
            //    UploadLogMgr.GetInstance().UploadLog(data);
            //    return;
            //}
            //if (data.Length % 2 != 0)
            //{
            //    LoggerHelper.Error("UploadLog:data.Length % 2 != 0");
            //    return;
            //}
            //Dictionary<string, string> logDict = new Dictionary<string, string>(UploadLogMgr.GetInstance().logHead);
            //for (int i = 0; i < data.Length / 2; i++)
            //{
            //    logDict[data[i * 2]] = data[i * 2 + 1];
            //}
            //string log = JsonMapper.ToJson(logDict);
            ////LoggerHelper.Info(string.Format("<color=#00ffff>UploadLog: {0} </color>", log));
            //EntityPlayer.Player.RpcCall("operate_log", log);
        }
    }

}