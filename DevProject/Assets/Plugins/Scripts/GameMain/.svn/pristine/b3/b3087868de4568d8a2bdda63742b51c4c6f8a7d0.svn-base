﻿using GameMain.GlobalManager;
using UnityEngine;
using System.Collections.Generic;
using GameData;

public class CameraTargetProxy : MonoBehaviour
{
    static CameraTargetProxy _instance;

    public static CameraTargetProxy instance
    {
        get {
            if (_instance == null)
            {
                GameObject go = new GameObject("CameraTargetProxy");
                GameObject.DontDestroyOnLoad(go);
                _instance = go.AddComponent<CameraTargetProxy>();
            }
            return _instance;
        }
    }

    Transform _transform;
    Transform _targetTransform;
    Transform _lockTargetTranform;
    float _lockDistance;
    float _lockSpeedRate;
    float _lockSpeedRatePlus;
    //处理锁定目标位置缓动
    Vector3 _lockTargetProxyPos = Vector3.zero;
    void Awake()
    {
        _transform = this.transform;
        List<float> rotateList = data_parse_helper.ParseListFloat(global_params_helper.GetGlobalParam(GlobalParamId.camera_target).Split(','));
        _lockDistance = rotateList[0];
        _lockSpeedRate = rotateList[1];
        _lockSpeedRatePlus = rotateList[2];
        _lockTargetProxyPos = _transform.position;
    }

    void Update()
    {
        if (CameraManager.GetInstance().mainCamera.currentMotionType != CameraMotionType.TWO_TARGET)
        {
            UpdateTwoTargetPlusMode();
            return;
        }
        UpdateTwoTargetMode();
    }

    public void UpdateTwoTargetMode()
    {
        if (_targetTransform == null) return;
        Vector3 pos = _transform.position;
        Vector3 targetPos = _targetTransform.position;

        if (_lockTargetTranform != null)
        {
            var lockDir = (_lockTargetTranform.position - targetPos).normalized;
            float curLockDis = Vector3.Distance(_targetTransform.position, _lockTargetTranform.position);
            float oldld = _lockDistance;
            if (curLockDis < (2 * oldld))
            {//当主角和目标小于_lockDistance时，跟随的距离变化
                oldld = 0.5f * curLockDis;
            }
            targetPos = targetPos + lockDir * oldld;
            _transform.LookAt(_lockTargetTranform);
        }

        Vector3 dir = targetPos - pos;
        float dis = dir.magnitude;
        if (dis > 10)
        {
            pos = targetPos;
        }
        else
        {
            dir = dir.normalized;
            float speed = dis * _lockSpeedRate;
            float moveDis = Time.deltaTime * speed;
            if (moveDis <= dis)
            {
                pos = pos + (dir * moveDis);
            }
        }
        _transform.position = pos;
    }

    Vector3 posLock = Vector3.zero;
    Vector3 posLockTrans = Vector3.zero;
    public void UpdateTwoTargetPlusMode()
    {
        if (_targetTransform == null) return;
        Vector3 pos = _transform.position;
        Vector3 targetPos = _targetTransform.position;
        Vector3 dir = targetPos - pos;
        float dis = dir.magnitude;
        if (dis > 10)
        {
            pos = targetPos;
        }
        else
        {
            dir = dir.normalized;
            float speed = dis * _lockSpeedRatePlus;
            float moveDis = Time.deltaTime * speed;
            if (moveDis <= dis)
            {
                pos = pos + (dir * moveDis);
            }
        }
        _transform.position = pos;

        if (_lockTargetTranform != null)
        {
            posLock = _lockTargetProxyPos;
            posLockTrans = _lockTargetTranform.position;
            Vector3 dirLock = posLockTrans - posLock;
            float disLock = dirLock.magnitude;
            if (disLock > 10)
            {
                posLock = posLockTrans;
            }
            else
            {
                dirLock = dirLock.normalized;
                float speed = disLock * _lockSpeedRatePlus;
                float moveDis = Time.deltaTime * speed;
                if (moveDis <= disLock)
                {
                    posLock = posLock + (dirLock * moveDis);
                }
            }
            _lockTargetProxyPos = posLock;
        }
    }

    public void SetTargetTransform(Transform transform)
    {
        _targetTransform = transform;
    }

    public void SetLockTargetTranform(Transform transform)
    {
        _lockTargetTranform = transform;
    }

    public Transform GetTransform()
    {
        return _transform;
    }

    public Transform GetTargetTransform()
    {
        return _targetTransform;
    }

    public Transform GetLockTargetTranform()
    {
        return _lockTargetTranform;
    }

    public Vector3 GetLockTargetProxyPos()
    {
        return _lockTargetProxyPos;
    }
}
