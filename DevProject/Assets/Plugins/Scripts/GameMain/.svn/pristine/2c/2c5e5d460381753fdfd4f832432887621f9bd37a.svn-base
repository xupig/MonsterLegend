﻿using LuaInterface;
using UnityEngine;

namespace GameMain
{
    public class XGameObjectTweenRotation : MonoBehaviour
    {

        Transform _trans = null;
        Vector3 _fromRotation;              //起始值
        Vector3 _curRotation;              //当前值
        Vector3 _toRotation;                //目标值    
        float _duration = 0f;
        Vector3 _add;
        Vector3 _tweenDirection;
        WrapMode _wrapMode;
        LuaFunction _luafunc;

        private bool _isTweening = false;

        public bool IsTweening
        {
            get { return _isTweening; }
            set { _isTweening = value; }
        }

        void Awake()
        {
            _trans = transform;
        }

        public void SetFromToRotationOnce(Vector3 formRotation, Vector3 toRotation, float duration, LuaFunction luafunc)
        {
            SetFromToRotation(formRotation, toRotation, duration, (int)WrapMode.Once, luafunc);
        }

        public void SetToRotationOnce(Vector3 toRotation, float duration, LuaFunction luafunc)
        {
            SetFromToRotation(_trans.eulerAngles, toRotation, duration, (int)WrapMode.Once, luafunc);
        }

        public void SetToRotation(Vector3 toRotation, float duration, int wrapMode, LuaFunction luafunc)
        {
            SetFromToRotation(_trans.eulerAngles, toRotation, duration, wrapMode, luafunc);
        }

        public void SetFromToRotation(Vector3 formRotation, Vector3 toRotation, float duration, int wrapMode, LuaFunction luafunc)
        {
            _luafunc = luafunc;
            _curRotation = formRotation;
            _fromRotation = formRotation;
            _toRotation = toRotation;
            _duration = duration;
            _wrapMode = (WrapMode)wrapMode;

            _add = new Vector3((_toRotation.x - _fromRotation.x) / _duration, (_toRotation.y - _fromRotation.y) / _duration, (_toRotation.z - _fromRotation.z) / _duration);
            _tweenDirection = Vector3.Normalize(_toRotation - _curRotation);
            SetRotation();
            _isTweening = true;
        }

        void Update()
        {
            if (!_isTweening) return;
            _curRotation += _add * UnityPropUtils.deltaTime;
            if (Vector3.Normalize(_toRotation - _curRotation) != _tweenDirection)
            {
                if(_luafunc != null)
                {
                    LuaFacade.CallLuaFunction(_luafunc);

                }
                if (_wrapMode == WrapMode.Once)
                {
                    _isTweening = false;
                    _curRotation = _toRotation;
                }
                else if (_wrapMode == WrapMode.PingPong)
                {
                    _isTweening = false;
                    _curRotation = _toRotation;
                    SetFromToRotation(_toRotation, _fromRotation, _duration, (int)(_wrapMode), _luafunc);
                }
                else if (_wrapMode == WrapMode.Loop)
                {
                    _curRotation = _fromRotation;
                }
            }
            //else
            //{
            //    _curRotation += _add * UnityPropUtils.deltaTime;
            //    if (Vector3.Normalize(_toRotation - _curRotation) != _tweenDirection)
            //    {
            //        _curRotation = _toRotation;
            //    }
            //}
            SetRotation();
        }

        private void SetRotation()
        {
            _trans.eulerAngles = _curRotation;
        }

    }
}