﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;

namespace GameMain
{
    public class GameObjectSoundManager
    {
        private static GameObjectSoundManager s_instance = null;
        public static GameObjectSoundManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new GameObjectSoundManager();
            }
            return s_instance;
        }

        private Dictionary<string, AudioClip> _audioClipPool = new Dictionary<string, AudioClip>();

        public void Play(GameObject gameObject, string name, float volume, bool loop = false)
        {
            var soundHander = gameObject.GetComponent<SoundHander>();
            if (soundHander == null) soundHander = gameObject.AddComponent<SoundHander>();
            name = GameResource.ObjectPool.Instance.GetResMapping(name);
            if (_audioClipPool.ContainsKey(name))
            {
                soundHander.Play(_audioClipPool[name], volume, loop);
            }
            else{
                _audioClipPool[name] = null;
                GameResource.ObjectPool.Instance.GetAudioClips(new string[] { name }, (objs) =>
                {
                    if (objs[0] != null)
                    {
                        _audioClipPool[name] = objs[0];
                        soundHander.Play(objs[0], volume, loop);
                    }
                });  
            }
        }

        public void Play(AudioSource source,GameObject gameObject, string name, float volume, bool loop = false)
        {
            name = GameResource.ObjectPool.Instance.GetResMapping(name);
            if (_audioClipPool.ContainsKey(name))
            {
                source.clip = _audioClipPool[name];
                source.volume = AudioManager.GetInstance().isTempLoseVolume == true ? 0f : volume;
                source.loop = loop;
                if (source.enabled) source.Play();
            }
            else
            {
                _audioClipPool[name] = null;
                GameResource.ObjectPool.Instance.GetAudioClips(new string[] { name }, (objs) =>
                {
                    if (gameObject == null)
                    {
                        GameResource.ObjectPool.Instance.Release(name);
                        return;
                    }
                    if (objs[0] != null)
                    {
                        _audioClipPool[name] = objs[0];
                        source.clip = objs[0];
                        source.volume = AudioManager.GetInstance().isTempLoseVolume == true ? 0f : volume;
                        source.loop = loop;
                        if (source.enabled) source.Play();
                    }
                });
            }
        }

        public void ClearPool()
        {
            foreach (var pair in _audioClipPool)
            {
                GameResource.ObjectPool.Instance.Release(pair.Key);
            }
            _audioClipPool.Clear();
        }
    }

    [RequireComponent(typeof(AudioSource))]
    public class SoundHander : MonoBehaviour
    {
        private AudioSource m_source;
        void Awake()
        {
            m_source = this.GetComponent<AudioSource>();
        }

        public void Play(AudioClip audioClip, float volume, bool loop = false)
        {
            m_source.clip = audioClip;
            m_source.volume = volume;
            m_source.loop = loop;
            m_source.Play();
        }
    }
}
