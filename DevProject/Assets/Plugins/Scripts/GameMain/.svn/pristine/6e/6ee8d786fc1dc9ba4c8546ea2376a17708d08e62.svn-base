﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using GameMain;

using ACTSystem;
using GameLoader.Utils;
using GameData;

namespace GameMain.CombatSystem
{
    public class AccordingMovement : AccordingStateBase
    {

        private bool _isCrashWall = false;
        public bool isCrashWall
        {
            get { return _isCrashWall; }
        }

        private Vector3 _moveTargetPosition = Vector3.zero;
        public Vector3 moveTargetPosition
        {
            get { return _moveTargetPosition; }
        }
        private float _moveBaseSpeed = 0f;
        private float _accelerate = 0;
        private bool _percent = false;

        public AccordingMovement(EntityCreature owner)
            : base(owner)
        {
            modeType = AccordingMode.AccordingMovement;
        }
        Vector3 oldTargetPosition;
        public bool Reset(Vector3 targetPosition, float baseSpeed, float accelerate = 0, bool percent = false, bool needFix = true)
        {
            var actor = movingActor;
            if (actor == null) return false;
            if (baseSpeed == 0) return false;
            //备份原来计算的目标点 
            oldTargetPosition = targetPosition;
            if (needFix)
            {
                //计算修正后的坐标
                targetPosition = FixTargetPosition(actor.position, targetPosition);
                //判断是否撞墙
                _isCrashWall = false;
                if (_owner.checkCrashWall == true)
                {
                    _isCrashWall = oldTargetPosition == targetPosition ? false : true;
                }
                
            }
            _accelerate = accelerate;
            _percent = percent;
            _moveTargetPosition = targetPosition;
            _moveBaseSpeed = baseSpeed;
            DrawMoveLine(actor.position, targetPosition, Color.yellow);
            return true;
        }

        public bool ResetTarget(Vector3 targetPosition)
        {
            //备份原来计算的目标点
            //if (_needFix)
            //{
            //    //计算修正后的坐标
            //    targetPosition = FixTargetPosition(actor.position, targetPosition);
            //    //判断是否撞墙
            //    _isCrashWall = oldTargetPosition == targetPosition ? false : true;
            //}
            //
            _moveTargetPosition = targetPosition;
            //_moveTime = dis * 2 / _moveBaseSpeed;
            //_accelerate = 0;
            //DrawMoveLine(actor.position, targetPosition, Color.yellow);
            return true;
        }

        public override void OnEnter()
        {
            
        }

        public override bool Update()
        {
            //服务器应该遵循的规则：
            //    当技能行为带位移：同步行为的同时同步“最终”目标地点信息，服务器首先将玩家位置移动到该位置
            //    行为位移结束，自动恢复默认同步方式。
            //问题：坐标有玩家自身同步到服务器，技能相关位移又该如何选择？
            var actor = movingActor;
            if (actor == null) return false;
            if (_moveTargetPosition == Vector3.zero) return false;
            var moveDirection = _moveTargetPosition - actor.position;
            //moveDirection.y = 0;
            var distance = moveDirection.magnitude;
            if (_moveBaseSpeed <= 0)
            {
                actor.actorController.Stop();
                actor.actorController.StopVertical();
                _owner.position = actor.position;
                return false;
            }
            if (!_isCrashWall && actor.actorController.crashedWall)
            {
                actor.actorController.Stop();
                actor.actorController.StopVertical();
                _owner.position = actor.position;
                return false;
            }
            if (distance < (_moveBaseSpeed * UnityPropUtils.deltaTime))
            {
                actor.actorController.Stop();
                actor.actorController.StopVertical();
                actor.position = _moveTargetPosition;
                _owner.position = actor.position;
                //如果是撞到墙,需要重新计算目标点
                if (_isCrashWall)
                {
                    _isCrashWall = false;
                    //去掉撞墙反弹
                    //ResetForCrashWall();
                }
                else 
                {
                    return false;
                }
            }
            //_moveTime = _moveTime - UnityPropUtils.deltaTime;
            Vector3 movement = moveDirection.normalized * _moveBaseSpeed;
            actor.actorController.Jump(movement.y);
            
            movement.y = 0;
            actor.actorController.Move(movement);
            _owner.position = actor.position;
            //根据加速度计算速度
            //if (_accelerate != 0 && actor.actorState.OnGround)
            //{
            //    _moveBaseSpeed = Mathf.Max(0, _moveBaseSpeed + _accelerate * UnityPropUtils.deltaTime);
            //}
            if (_percent) {
                _moveBaseSpeed = Mathf.Max(0, _moveBaseSpeed + _moveBaseSpeed * _accelerate * UnityPropUtils.deltaTime);
            }
            else
            {
                _moveBaseSpeed = Mathf.Max(0, _moveBaseSpeed + _accelerate * UnityPropUtils.deltaTime);
            }
            if (_moveBaseSpeed < 0.5f) {
                actor.actorController.Stop();
                actor.actorController.StopVertical();
                _owner.position = actor.position;
                return false;
            }
            return true;
        }


        private void ResetForCrashWall()
        {
            Vector3 newTargePosition =Vector3.zero;// = EntityPlayerLuaBase.Player.position;
            newTargePosition = CalcRefectPosition();
            ResetTarget(newTargePosition);
            //设置目标模型角度
            if (_RayRefectionInfo.IsChangeAvatarModel())
            {
                movingActor.transform.forward = _RayRefectionInfo.avatarRotation;
            }
            
        }

        /// <summary>
        /// 反射点计算
        /// </summary>
        private Vector3 CalcRefectPosition()
        {
            _RayRefectionInfo.allDistance = Vector3.Distance(oldTargetPosition,_RayRefectionInfo.castRay.origin);
            return _RayRefectionInfo.CalcReflectPosition();

        }

        public override void OnLeave()
        {
            _moveTargetPosition = Vector3.zero;
        }

        static Ray _castRay = new Ray(Vector3.zero, Vector3.zero);
        static RaycastHit _raycastHit = new RaycastHit();
        static RayRefectionInfo _RayRefectionInfo = new RayRefectionInfo();
        static float _far = 20;
        static int _terrainLayerValue = (1 << ACTSystem.ACTSystemTools.NameToLayer("Wall")) | (1 << ACTSystem.ACTSystemTools.NameToLayer("DynamicWall"));
        private Vector3 FixTargetPosition(Vector3 sourcePosition, Vector3 targetPosition)
        {                                  
            var transform = movingActor.transform;
            CharacterController controller = movingActor.GetComponent<CharacterController>();
            var dir = targetPosition - sourcePosition;
            var far = dir.magnitude;
            _castRay.origin = sourcePosition;
            _castRay.direction = dir.normalized;
            _raycastHit.point = Vector3.zero;
            Physics.SphereCast(_castRay, 1, out _raycastHit, far, _terrainLayerValue);
            _RayRefectionInfo.SetInfo(_castRay, _raycastHit, oldTargetPosition);
            var dism = (_raycastHit.point - _castRay.origin).magnitude;
            //DrawMoveLine(_castRay.origin, _raycastHit.point, Color.yellow);

            
            _castRay.origin = sourcePosition + transform.right.normalized * controller.radius * 0.9f;
            _raycastHit.point = Vector3.zero;
            Physics.Raycast(_castRay, out _raycastHit, _far, _terrainLayerValue);
            var disr = (_raycastHit.point - _castRay.origin).magnitude;
            DrawMoveLine(_castRay.origin, _raycastHit.point, Color.red);

            _castRay.origin = sourcePosition - transform.right.normalized * controller.radius * 0.9f;
            _raycastHit.point = Vector3.zero;
            Physics.Raycast(_castRay, out _raycastHit, _far, _terrainLayerValue);
            var disl = (_raycastHit.point - _castRay.origin).magnitude;
            DrawMoveLine(_castRay.origin, _raycastHit.point, Color.red);
            //临时定义个的误差(避免以为目标点和重点重合不能产生)
            var newdis = Mathf.Min(dism, disr, disl) - 0.01f;
            //var newdis = dism + 0.02f;
            if (newdis < dir.magnitude)
            {
                targetPosition = sourcePosition + dir.normalized * Mathf.Max(0, newdis - controller.radius * 2);
            }
            return targetPosition;
        }

        private void DrawMoveLine(Vector3 srcPosition, Vector3 tarPosition, Color color, float time = 15)
        {
            if (!UnityPropUtils.IsEditor)
            {
                return;
            }
            srcPosition.y += 1;
            tarPosition.y += 1;
            Debug.DrawLine(srcPosition, tarPosition, color, 15);
        }
    }
}
