﻿using Common.Global;
using Common.Structs;
using GameLoader.Utils;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace GameData
{
    public class transform_helper
    {
        public static transform GetTransformItem(int id)
        {
            if (XMLManager.transform.ContainsKey(id))
            {
                return XMLManager.transform[id];
            }
            LoggerHelper.Error("配置表transform.xml 未找到配置项： " + id);
            return null;
        }

        public static int GetModelId(int id)
        {
            transform item = GetTransformItem(id);
            if (item == null)
            {
                return 1;
            }
            return item.__model;
        }

        public static List<int> GetSpellList(int id)
        {
            transform item = GetTransformItem(id);
            return data_parse_helper.ParseListInt(item.__spell);
        }
    }
}

