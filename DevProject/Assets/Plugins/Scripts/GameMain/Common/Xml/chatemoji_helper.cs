﻿using Common.Utils;
using GameLoader.Utils;
using System.Collections.Generic;

namespace GameData
{
    public class chatemoji_helper
    {

        private static List<string> _emojiFirstSpriteNameList = null;
        private static Dictionary<string, List<int>> _emojiSpriteIndexListDict = null;

        public static List<string> GetEmojiFirstSpriteNameList()
        {
            if (_emojiFirstSpriteNameList == null)
            {
                _emojiFirstSpriteNameList = new List<string>();
                foreach( KeyValuePair<int, chat_emoji_animation> item in XMLManager.chat_emoji_animation)
                {
                    _emojiFirstSpriteNameList.Add(item.Value.__name);
                }
            }
            return _emojiFirstSpriteNameList;
        }

        public static Dictionary<string, List<int>> GetSpriteIndexListDict()
        {
            if (_emojiSpriteIndexListDict == null)
            {
                _emojiSpriteIndexListDict = new Dictionary<string, List<int>>();
                foreach (KeyValuePair<int, chat_emoji_animation> item in XMLManager.chat_emoji_animation)
                {
                    _emojiSpriteIndexListDict.Add(item.Value.__name , data_parse_helper.ParseListInt(item.Value.__sequence));
                }
            }
            return _emojiSpriteIndexListDict;
        }
    }
}
