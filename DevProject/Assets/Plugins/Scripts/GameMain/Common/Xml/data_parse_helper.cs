﻿using System;
using System.Collections.Generic;
using GameLoader.Utils;
namespace GameData
{

    public static class data_parse_helper
    {
        
        static List<string> _emptyListString = new List<string>();
        public static List<string> ParseListString(string source)
        {
            List<string> result = _emptyListString;
            if (source != null)
            {
                result = new List<string>();
                string[] sourceArray = source.Split(',');
                for (int i = 0; i < sourceArray.Length; i++)
                {
                    result.Add(sourceArray[i]);
                }
            }
            return result;
        }

        public static List<string> ParseListString(string[] source)
        {
            List<string> result = _emptyListString;
            if (source != null)
            {
                result = new List<string>();
                for (int i = 0; i < source.Length; i++)
                {
                    result.Add(source[i]);
                }
            }
            return result;
        }

        static List<int> _emptyListInt = new List<int>();
        public static List<int> ParseListInt(string source)
        {
            List<int> result = _emptyListInt;
            if (source != null)
            {
                result = new List<int>();
                string[] sourceArray =  source.Split(',');
                for (int i = 0; i < sourceArray.Length; i++)
                {
                    result.Add(int.Parse(sourceArray[i]));
                }
            }
            return result;
        }

        public static List<int> ParseListInt(List<string> source)
        {
            List<int> result = _emptyListInt;
            if (source != null)
            {
                result = new List<int>();
                for (int i = 0; i < source.Count; i++)
                {
                    result.Add(int.Parse(source[i]));
                }
            }
            return result;
        }

        public static List<int> ParseListInt(string[] source)
        {
            List<int> result = _emptyListInt;
            if (source != null)
            {
                result = new List<int>();
                for (int i = 0; i < source.Length; i++)
                {
                    result.Add(int.Parse(source[i]));
                }
            }
            return result;
        }

        static List<float> _emptyListFloat = new List<float>();
        public static List<float> ParseListFloat(string source)
        {
            List<float> result = _emptyListFloat;
            if (source != null)
            {
                result = new List<float>();
                string[] sourceArray = source.Split(',');
                for (int i = 0; i < sourceArray.Length; i++)
                {
                    result.Add(float.Parse(sourceArray[i]));
                }
            }
            return result;
        }

        public static List<float> ParseListFloat(List<string> source)
        {
            List<float> result = _emptyListFloat;
            if (source != null)
            {
                result = new List<float>();
                for (int i = 0; i < source.Count; i++)
                {
                    result.Add(float.Parse(source[i]));
                }
            }
            return result;
        }

        public static List<float> ParseListFloat(string[] source)
        {
            List<float> result = _emptyListFloat;
            if (source != null)
            {
                result = new List<float>();
                for (int i = 0; i < source.Length; i++)
                {
                    result.Add(float.Parse(source[i]));
                }
            }
            return result;
        }

        public static Dictionary<int, int> ParseDictionaryIntInt(string source)
        {
            return ParseDictionaryIntInt(ParseMap(source));
        }

        public static Dictionary<int, UInt64> ParseDictionaryIntUInt64(string source)
        {
            return ParseDictionaryIntUInt64(ParseMap(source));
        }


        static Dictionary<int, int> _emptyDictionaryIntInt = new Dictionary<int, int>();
        public static Dictionary<int, int> ParseDictionaryIntInt(Dictionary<string, string> source)
        {
            Dictionary<int, int> result = _emptyDictionaryIntInt;
            if (source != null)
            {
                result = new Dictionary<int, int>();
                var en = source.GetEnumerator();
                while (en.MoveNext())
                {
                    result.Add(int.Parse(en.Current.Key), int.Parse(en.Current.Value));
                }
            }
            return result;
        }

        static Dictionary<int, UInt64> _emptyDictionaryIntUInt64 = new Dictionary<int, UInt64>();
        public static Dictionary<int, UInt64> ParseDictionaryIntUInt64(Dictionary<string, string> source)
        {
            Dictionary<int, UInt64> result = _emptyDictionaryIntUInt64;
            if (source != null)
            {
                result = new Dictionary<int, UInt64>();
                var en = source.GetEnumerator();
                while (en.MoveNext())
                {
                    result.Add(int.Parse(en.Current.Key), UInt64.Parse(en.Current.Value));
                }
            }
            return result;
        }

        public static Dictionary<int, float> ParseDictionaryIntFloat(string source)
        {
            return ParseDictionaryIntFloat(ParseMap(source));
        }

        static Dictionary<int, float> _emptyDictionaryIntFloat = new Dictionary<int, float>();
        public static Dictionary<int, float> ParseDictionaryIntFloat(Dictionary<string, string> source)
        {
            Dictionary<int, float> result = _emptyDictionaryIntFloat;
            if (source != null)
            {
                result = new Dictionary<int, float>();
                var en = source.GetEnumerator();
                while (en.MoveNext())
                {
                    result.Add(int.Parse(en.Current.Key), float.Parse(en.Current.Value));
                }
            }
            return result;
        }

        static Dictionary<int, List<int>> _emptyDictionaryIntListInt = new Dictionary<int, List<int>>();
        public static Dictionary<int, List<int>> ParseDictionaryIntListInt(Dictionary<string, string[]> source)
        {
            Dictionary<int, List<int>> result = _emptyDictionaryIntListInt;
            if (source != null)
            {
                result = new Dictionary<int, List<int>>();
                var en = source.GetEnumerator();
                while (en.MoveNext())
                {
                    result.Add(int.Parse(en.Current.Key), ParseListInt(en.Current.Value));
                }
            }
            return result;
        }

        public static Dictionary<int, List<int>> ParseDictionaryIntListInt(string source)
        {
            return ParseDictionaryIntListInt(ParseMapList(source));
        }

        static Dictionary<int, List<float>> _emptyDictionaryIntListFloat = new Dictionary<int, List<float>>();
        public static Dictionary<int, List<float>> ParseDictionaryIntListFloat(Dictionary<string, string[]> source)
        {
            Dictionary<int, List<float>> result = _emptyDictionaryIntListFloat;
            if (source != null)
            {
                result = new Dictionary<int, List<float>>();
                var en = source.GetEnumerator();
                while (en.MoveNext())
                {
                    result.Add(int.Parse(en.Current.Key), ParseListFloat(en.Current.Value));
                }
            }
            return result;
        }

        public static Dictionary<int, List<float>> ParseDictionaryIntListFloat(string source)
        {
            return ParseDictionaryIntListFloat(ParseMapList(source));
        }

        static Dictionary<int, List<string>> _emptyDictionaryIntListString = new Dictionary<int, List<string>>();
        public static Dictionary<int, List<string>> ParseDictionaryIntListString(Dictionary<string, string[]> source)
        {
            Dictionary<int, List<string>> result = _emptyDictionaryIntListString;
            if (source != null)
            {
                result = new Dictionary<int, List<string>>();
                var en = source.GetEnumerator();
                while (en.MoveNext())
                {
                    result.Add(int.Parse(en.Current.Key), ParseListString(en.Current.Value));
                }
            }
            return result;
        }

        public static Dictionary<int, List<string>> ParseDictionaryIntListString(string source)
        {
            return ParseDictionaryIntListString(ParseMapList(source));
        }

        public static Dictionary<string, string> ParseMap(string strMap)
        {
            Dictionary<String, String> result = new Dictionary<String, String>();
            if (string.IsNullOrEmpty(strMap))
            {
                return result;
            }

            string[] map = strMap.Split(';');//根据字典项分隔符分割字符串，获取键值对字符串
            for (int i = 0; i < map.Length; i++)
            {
                if (String.IsNullOrEmpty(map[i]))
                {
                    continue;
                }

                string[] keyValuePair = map[i].Split(':');//根据键值分隔符分割键值对字符串
                if (keyValuePair.Length == 2)
                {
                    if (!result.ContainsKey(keyValuePair[0]))
                        result.Add(keyValuePair[0], keyValuePair[1]);
                    else
                        LoggerHelper.Error(String.Format(" Key {0} already exist index {1} of {2}", keyValuePair[0], i, strMap));
                }
                else
                {
                    LoggerHelper.Error(String.Format(" KeyValuePair are not match: {0} index {1} of {2}", map[i], i, strMap));
                }
            }
            return result;
        }

        public static Dictionary<string, string[]> ParseMapList(string strMap)
        {
            Dictionary<String, string[]> result = new Dictionary<String, string[]>();
            if (string.IsNullOrEmpty(strMap))
            {
                return result;
            }

            string[] map = strMap.Split(';');//根据字典项分隔符分割字符串，获取键值对字符串
            for (int i = 0; i < map.Length; i++)
            {
                if (String.IsNullOrEmpty(map[i]))
                {
                    continue;
                }

                string[] keyValuePair = map[i].Split(':');//根据键值分隔符分割键值对字符串
                if (keyValuePair.Length == 2)
                {
                    if (!result.ContainsKey(keyValuePair[0]))
                        result.Add(keyValuePair[0], keyValuePair[1].Split(','));
                    else
                        LoggerHelper.Error(String.Format(" Key {0} already exist, index {1} of {2}.", keyValuePair[0], i, strMap));
                }
                else
                {
                    LoggerHelper.Error(String.Format(" KeyValuePair are not match: {0}, index {1} of {2}.", map[i], i, strMap));
                }
            }
            return result;
        }
    }
}
