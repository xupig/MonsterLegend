﻿using Common.Global;
using Common.Structs;
using GameLoader.Utils;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace GameData
{
    public class wings_value_helper
    {
        public static wings_value GetWingsValueItem(int id)
        {
            if (XMLManager.wings_value.ContainsKey(id))
            {
                return XMLManager.wings_value[id];
            }
            LoggerHelper.Error("配置表wings_value.xml 未找到配置项： " + id);
            return null;
        }

        public static bool IsWingsValueId(int id)
        {
            if (XMLManager.wings_value.Keys.Contains(id))
            {
                return true;
            }
            return false;
        }
    }
}
