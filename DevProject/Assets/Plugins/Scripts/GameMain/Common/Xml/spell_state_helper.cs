﻿#region 模块信息
/*==========================================
// 文件名：skill_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/22 11:23:05
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class spell_state_helper
    {
        private static spell_state[] spellDict;

        public static spell_state GetSpellStateData(int id)
        {
            InitData();
            if (id < spellDict.Length) return spellDict[id];
            return null; ;
        }

        private static void InitData()
        {
            if (spellDict == null)
            {
                spellDict = new spell_state[state_config.AVATAR_STATE_SPELL_MAX];
                for (int i = 0; i < state_config.AVATAR_STATE_SPELL_MAX; i++)
                {
                    if (XMLManager.spell_state.ContainsKey(i))
                    {
                        spellDict[i] = XMLManager.spell_state[i];
                    }
                    else
                    {
                        spellDict[i] = null;
                    }
                }
            }
        }
    }
}
