﻿using Common.Global;
using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Data;
using Common.ServerConfig;
using Common.Structs;


namespace GameData
{
    /// <summary>
    /// 对应配置表role_data.xml
    /// </summary>
    public class role_data_helper
    {
        private static role_data GetData(int vocation)
        {
            if (!XMLManager.role_data.ContainsKey(vocation))
            {
                return null;
            }
            return XMLManager.role_data[vocation];
        }

        public static int GetModel(int vocation)
        {
            role_data data = GetData(vocation);
            if (data != null)
            {
                return data.__model;
            }
            return 0;
        }

        public static int GetSpeed(int vocation)
        {
            role_data data = GetData(vocation);
            if (data != null)
            {
                return data.__speed;
            }
            return 0;
        }

        public static int GetDefaultCloth(int vocation)
        {
            role_data data = GetData(vocation);
            if (data != null)
            {
                return data.__default_cloth;
            }
            return 0;
        }

        public static int GetDefaultHead(int vocation)
        {
            role_data data = GetData(vocation);
            if (data != null)
            {
                return data.__default_head;
            }
            return 0;
        }

        //public static int GetDefaultHair(int vocation)
        //{
        //    role_data data = GetData(vocation);
        //    if (data != null)
        //    {
        //        return data.__default_hair;
        //    }
        //    return 0;
        //}

        public static int GetDefaultWeapon(int vocation)
        {
            role_data data = GetData(vocation);
            if (data != null)
            {
                return data.__default_weapon;
            }
            return 0;
        }

        //public static int GetDefaultDeputyWeapon(int vocation)
        //{
        //    role_data data = GetData(vocation);
        //    if (data != null)
        //    {
        //        return data.__default_deputy_weapon;
        //    }
        //    return 0;
        //}

        public static string GetCityControllerPath(int vocation)
        {
            role_data data = GetData(vocation);
            return data.__city_controller;
        }

        public static string GetCombatControllerPath(int vocation)
        {
            role_data data = GetData(vocation);
            return data.__combat_controller;
        }

        public static string GetUIControllerPath(int vocation)
        {
            role_data data = GetData(vocation);
            return data.__ui_controller;
        }

        public static string GetCreateControllerPath(int vocation)
        {
            role_data data = GetData(vocation);
            return data.__create_controller;
        } 
    }
}
