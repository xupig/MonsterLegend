﻿#region 模块信息
/*==========================================
// 文件名：style_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/2/10 19:39:26
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameData
{
    public class style_helper
    {
        public static font_style GetStyle(int styleId)
        {
            if(XMLManager.font_style.ContainsKey(styleId))
            {
                return XMLManager.font_style[styleId];
            }
            return null;
        }

    }
}
