﻿using Common.Global;
using Common.Structs;
using GameLoader.Utils;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace GameData
{
    public class map_helper
    {
        public static map GetMapData(int id)
        {
            if (XMLManager.map.ContainsKey(id))
            {
                return XMLManager.map[id];
            }
            LoggerHelper.Error("配置表map.xml 未找到配置项： " + id);
            return null;
        }

        public static bool GetHasKillLevelLimit(int id)
        {
            map item = GetMapData(id);
            return item.__kill_100 == 0;
        }

        public static bool GetUsePkMode(int id)
        {
            map item = GetMapData(id);
            return item.__use_pk_mode == 1;
        }

        public static bool GetUseRedName(int id)
        {
            map item = GetMapData(id);
            return item.__use_red_name == 1;
        }

        public static bool GetPvpShow(int id)
        {
            map item = GetMapData(id);
            return item.__pvp_show_mode == 1;
        }
    }
}


