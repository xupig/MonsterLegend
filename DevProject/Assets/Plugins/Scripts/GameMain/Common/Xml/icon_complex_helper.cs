﻿#region 模块信息
/*==========================================
// 文件名：icon_helper
// 命名空间: GameLogic.GameLogic.Common.ClientConfig.Xml
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/27 10:20:03
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Global;
using Common.Structs;
using GameLoader.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace GameData
{
    public class icon_complex_helper
    {
        private const string DEFAULT_VALUE = "0";
        private const string ICON_404 = "item404";

        public static icon_complex GetIconComplex(int id)
        {
            if (XMLManager.icon_complex.ContainsKey(id))
            {
                return XMLManager.icon_complex[id];
            }
            LoggerHelper.Error("配置表icon_complex.xml 未找到配置项： " + id);
            return null;
        }

        public static Dictionary<int, List<int>> GetTypes(int id)
        {
            var data = icon_complex_helper.GetIconComplex(id);
            return data_parse_helper.ParseDictionaryIntListInt(data.__type);
        }

        public static List<float> GetParams(int id, int type, int paramsId)
        {
            var data = icon_complex_helper.GetIconComplex(id);
            if (type == 1)
            {
                var animData = data_parse_helper.ParseDictionaryIntListFloat(data.__anim_params);
                return animData[paramsId];
            }
            else if (type == 2)
            {
                var rotationData = data_parse_helper.ParseDictionaryIntListFloat(data.__rotation_params);
                return rotationData[paramsId];
            }
            else if (type == 3)
            {
                var scaleData = data_parse_helper.ParseDictionaryIntListFloat(data.__scale_params);
                return scaleData[paramsId];
            }
            return null;
        }

    }
}
