﻿using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameData
{
    public class sound_helper
    {

        public static sound GetSoundData(int id)
        {
            if (!XMLManager.sound.ContainsKey(id))
            {
                if (UnityPropUtils.IsEditor) LoggerHelper.Error("[音效语音配置表]未包含项: " + id.ToString());
                return null;
            }
            return XMLManager.sound[id];
        }

        public static int Priority(int id)
        {
            sound snd = GetSoundData(id);
            if (snd != null)
            {
                return snd.__priority;
            }
            return 0;
        }

        public static string Path(int id)
        {
            sound snd = GetSoundData(id);
            if (snd != null)
            {
                return snd.__path;
            }
            return string.Empty;
        }

        public static int Loop(int id)
        {
            sound snd = GetSoundData(id);
            if (snd != null)
            {
                return snd.__loop;
            }
            return 0;
        }

        public static int Pitch(int id)
        {
            sound snd = GetSoundData(id);
            if (snd != null)
            {
                return snd.__pitch;
            }
            return 0;
        }

        public static float Volume(int id)
        {
            sound snd = GetSoundData(id);
            if (snd != null)
            {
                return snd.__volume == 0 ? 1 : snd.__volume;
            }
            return 0;
        }
    }
}
