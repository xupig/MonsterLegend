﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMain.ClientConfig
{
    
    public class EntityConfig
    {
        public const string ENTITY_TYPE_NAME_AVATAR = "Avatar";
        public const string ENTITY_TYPE_NAME_ACCOUNT = "Account";
        public const string ENTITY_TYPE_NAME_CREATE_ROLE_AVATAR = "CreateRoleAvatar";
        public const string ENTITY_TYPE_NAME_PLAYER_AVATAR = "PlayerAvatar";
        public const string ENTITY_TYPE_NAME_DUMMY = "Dummy";
        public const string ENTITY_TYPE_NAME_MONSTER = "Monster";
        public const string ENTITY_TYPE_NAME_NPC = "NPC";
        public const string ENTITY_TYPE_NAME_CLIENT_DROP_ITEM = "ClientDropItem";
        public const string ENTITY_TYPE_NAME_DROP_ITEM = "DropItem";
        public const string ENTITY_TYPE_NAME_PET = "Pet";
        public const string ENTITY_TYPE_NAME_PUPPET = "Puppet";
        public const string ENTITY_TYPE_NAME_GROUND_VEHICLE = "GroundVehicle";
        public const string ENTITY_TYPE_NAME_AIR_VEHICLE = "AirVehicle";
        public const string ENTITY_TYPE_NAME_PREVIEW = "PreviewRole";
        public const string ENTITY_TYPE_NAME_CHEST = "Chest";
        public const string ENTITY_TYPE_NAME_DUEL_CHEST = "DuelChest";
        public const string ENTITY_TYPE_NAME_PORTAL = "Portal";
        public const string ENTITY_TYPE_NAME_COMBAT_PORTAL = "CombatPortal";
        public const string ENTITY_TYPE_NAME_AGENT = "Agent";
        public const string ENTITY_TYPE_NAME_TRAP = "Trap";
        public const string ENTITY_TYPE_NAME_THROWOBJECT = "ThrowObject";
        public const string ENTITY_TYPE_NAME_SKILL_SHOW_AVATAR = "SkillShowAvatar";
        public const string ENTITY_TYPE_NAME_FABAO = "Fabao";
        public const string ENTITY_TYPE_NAME_PERI = "Peri";
        public const string ENTITY_TYPE_NAME_UISHOWENTITY = "UIShowEntity";
    }
}
