﻿#region 模块信息
/*==========================================
// 文件名：LangEnum
// 命名空间: GameLogic.GameLogic.Common.Global
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/7 17:39:32
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.ClientConfig
{
    public enum LangEnum
    {
        //道具相关
        COMMON_ITEM = 2000,
        EQUIP = 2001,
        GEM = 2002,
        RUNE = 2003,
        MATERIAL = 2004,
        LUCKY_STONE = 2005,
        SCROLL = 2006,

        //道具品质颜色
        ITEM_QUALITY_WHITE = 25,
        ITEM_QUALITY_GREEN = 26,
        ITEM_QUALITY_BLUE = 27,
        ITEM_QUALITY_PURPLE = 5036,
        ITEM_QUALITY_ORANGE = 5037,
        ITEM_QUALITY_GOLDEN = 5038,
        ITEM_QUALITY_RED = 5039,

        //道具类型
        ITEM_COMMON = 5101,
        ITEM_EQUIP = 5102,
        ITEM_GEM = 5103,
        ITEM_RUNE = 5104,
        ITEM_SPECIAL = 5113,
        ITEM_MATERIAL = 5111,
        ITEM_LUCKY_STONE = 5112,
        ITEM_LEARN_DRAWING = 5106,
        ITEM_ENCHANT_SCROLL = 5105,
        ITEM_SUIT_SCROLL = 5114,
        ITEM_BUILD_DRAWING = 5115,
        ITEM_EQUIP_BOX = 5119,
        ITEM_TOKEN = 5116,
        ITEM_FRIEND = 5117,

        ENSURE_CANCEL_FOLLOW_FIGHT = 10116,

        //职业
        VOC_WARRIOR = 1,
        VOC_ASSASSIN = 2,
        VOC_WIZARD = 3,
        VOC_ARCHER = 4,

        //可合成宝石
        GEM_COMPOSABLE = 3112,

        Energy = 18,

        #region 属性
        ATTR_Naili = 300,     //+{0}耐力
        ATTR_Liliang = 301,       //+{0}力量
        #endregion

        #region 组队10001-12000
        TEAM_HAS_INVITE = 10003,//已邀请...
        TEAM_DISSMISS = 10004, //队伍已解散
        TEAM_WAIT_CAPTAIN = 10005,//等待队长处理
        TEAM_INVITE_CD = 10006,//3s内只能对同一个玩家发出一次邀请
        TEAM_LEAVE_TEAM = 10007,//确定离开队伍？
        TEAM_NEW_APPLY = 10011,//      您有新的组队申请信息
        TEAM_NEW_INVITE = 10012,//您有新的组队邀请信息 
        TEAM_CALL_TEAMMATE = 10013,//确定召唤全体队员至您身边？
        TEAM_KICK = 10014,//确定将{0}请离队伍？
        TEAM_CHANGE_CAPTAIN = 10015,//	确定将队长转移给{0}？
        TEAM_TRANSMIT = 10016,//	确定传送至{0}身边？
        TEAM_WORLR_SPEAK_TITLE = 10017, //写下你想发布的组队信息，这些信息将被推送到世界频道
        TEAM_WORLD_SPEAK_INPUT = 10018, //输入发布信息
        TEAM_TEAMMATE_CALL_YOU = 10019,// {0}召唤你至{1}，是否同意？
        TEAM_CALL_ONE = 10020,//确定召唤{0}至您身边？
        TEAM_APPLY_INFO = 10021,//$(1,{0,PlayerId}{1,Level}级) 申请加入队伍
        TEAM_OTHER_INVITE_INFO = 10022,//$(1,{0,PlayerId}) 想邀请 $(1,{1,PlayerId})加入队伍
        TEAM_CAPTAIN_INVITE_INFO = 10023,//$(1,{0,PlayerId}{1,Level}级) 邀请您加入 $(1,{2,PlayerId}) 的队伍
        #endregion

        #region 符文 12001-13000
        RUNE_COMBINE_MESSAGE = 4005,//{0,ItemId}Lv{1,Num}将吞噬其他低等级符文升到Lv{2,Num}
        RUNE_COMBINE_TIPS = 4006, //合成符文中含有紫色或者更高品质的符文
        RUNE_SWALLOW = 4007,//{}将被吞噬，你确定？
        RUNE_HAS_SENIOR_RUNE = 4010,//	有高级符文将被吞噬，你确定？
        RUNE_USE = 4131,    //确定使用该符文吗？
        RUNE_CURRENT_LEVEL = 4132,  //当前等级: {0}
        RUNE_CURRENT_EXP = 4133,    //当前经验: {0}
        RUNE_LEVEL_ACTIVE = 4134,   //(符文等级{0}激活)
        RUNE_NEED_LEVEL = 4135,    //需求等级: {0}
        RUNE_NEED_VACATION = 4136,   //需求职业: {0}
        RUNE_SWALLOW_GET_EXP = 4137,  //吞噬可获得{0}点符文经验
        RUNE_NEXT_LEVEL = 4138,      //下一等级: {0}
        RUNE_MAX_LEVEL = 4139,       //当前已是最高等级
        RUNE_HAS_NOTHING_TO_SWALLOW = 4140, //没有可吞噬的符文
        RUNE_SWALLOW_CONFIRM = 4141,//确定吞噬这些符文吗？
        RUNE_NO_VACATION_LIMIT = 4142,   //需求职业: 任何职业
        RUNE_NORMAL = 4143, //普通许愿
        RUNE_ADVANCED = 4144, //高级许愿
        RUNE_PERFECT = 4145,  //完美许愿
        RUNE_NO_FREE_TIMES = 4146,//今日免费次数已用完
        RUNE_NO_AUTO_ADD_RUNE = 4147,//没有可自动添加的蓝绿品质符文
        RUNE_BIND_MONEY_ONCE_ALERT = 4148,
        RUNE_BIND_MONEY_TEN_ALERT = 4149,
        RUNE_NO_RUNE_CAN_BE_COMBINED = 4331,//当前没有满足一键合成条件的符文
        #endregion

        #region 通用confirm
        CONFIRM_TEXT = 10,   //确定
        CANCEL_TEXT = 11,   //取消
        ALL_ITEM_TXT = 56122, // "全部"中文
        COMMON_GEM_NOT_ENOUGH = 35000,
        OPERATION_CD = 10006,//您的操作太快了
        PROMPT = 24,//提示
        #endregion

        #region 好友
        FRIEND_SEARCH = 57036,
        FRIEND_LIST_FULL = 57000,//{玩家名字}$(6,好友已满)	
        FRIEND_HAS_APPLY = 57001,//	$(6,你已向该玩家发送过好友申请)	
        FRIEND_ALREADY_FRIEND = 57002,//	$(6,你们已是好友关系)	
        FRIEND_APPLY_SCUESS = 57003,//	成功发送好友申请	
        FRIEND_OTHER_SEND_ITEM = 57035,//{0}向你赠送了{1}，你们获得了 $(6,{2})点亲密值。{3}
        FRIEND_SEND_ITEM = 57034,//你向{0}赠送了{1}，你们获得了 $(6,{2})点亲密值。{3}
        FRIEND_FINISH_SOMETHING = 57010,//你和{0}共同完成{1}，你们获得了 $(6,{2})点亲密值。{3}
        FRIEND_SNED_SOMETHING = 57034,//	你向{玩家名字}赠送了{道具名字}，你们获得了{1}点亲密值。{年}{月}{日}	
        FRIEND_RECV_SOMETHING = 57035,//	{玩家名字}向你赠送了{道具名字}，你们获得了{1}点亲密值。{年}{月}{日}	
        FRIEND_ADD_BLACKLIST = 57016,//	屏蔽成功，将不再收到任何{玩家名字}发送的消息
        FRIEND_BLESSING_NUM_LIMIT = 57018,//	$(6,今日祝福次数已达上限)	
        FRIEND_HAS_BLESSING = 57019,//	$(6,你已祝福过此好友)	
        FRIEND_REJECT_FRIEND = 57005,//	{玩家名字}拒绝了你的好友请求
        FRIEND_ADD_FRIEND = 57004,//	与{玩家名字}成为好友	
        FRIEND_ONLINE = 57013,//	你的好友{玩家名字}进入游戏	
        FRIEND_OFFLINE = 57014,//	你的好友{玩家名字}离开游戏
        FRIEND_NOTHING_TO_RECEIVE = 57021,//	$(6,没有可领取的体力祝福，快邀请好友祝福你吧)	
        FRIEND_RECEIVE_BLESS_REWARD = 57023,// 领取{0}所赠送的{1}点体力
        FRIEND_CANT_SEARCH_MYSELF = 57011,//	$(6,不能搜索自己)	
        FRIEND_OTHER_DEL_FRIEND = 57039,// {0}与你断绝了好友关系
        FRIEND_MY_DEL_FRIEND = 57040,// 你与{0}断绝了好友关系
        FRIEND_DEL_CONFIRM = 57009,	//确定要将{玩家名字}踢出好友圈永不相见吗？	

        FRIEND_SELECT_SEND_ITEM = 57024,//请选择赠送物品
        FRIEND_SELECT_SEND_PLAYER = 57025,//请选择赠送玩家
        FRIEND_SEND_LIMIT = 57026,//此物品今日赠送已达上限
        FRIEND_SEND_DIAMOND = 57027,//钻石不足
        FRIEND_SEND_COST_BIND_DIAMOND = 57041,//是否花费{0}钻石赠送{1}给{2}?
        FRIEND_SEND_COST_DIAMOND = 57042,//是否花费{0}钻石赠送{1}给{2}?
        FRIEND_SEND_COST_ALL_DIAMOND = 57044,//是否花费{0}钻石赠送{1}给{2}?
        FRIEND_INTIMATE_LIMIT = 57116, //亲密度已达上限
//57006	你与{玩家名字}是结拜关系，请先断袖再解除好友关系	
//57007	你与{玩家名字}是夫妻关系，请先离婚再解除好友关系	
//57008	你与{玩家名字}是师徒关系，请先解除师徒关系再解除好友关系	
//57009	确定要将{玩家名字}踢出好友圈永不相见吗？	

//57011	$(6,不能搜索自己)	

//57015	$(6,屏蔽人数已达上限，可在屏蔽界面中整理)	
//57016	屏蔽成功，将不再收到任何{玩家名字}发送的消息	
//57017	成功取消屏蔽	

//57020	祝福成功	

//57022	$(6,今日领取祝福已达上限，明天再来吧)	
//57023	获得{玩家名字}送出的{2}点体力祝福	
//57024	请选择赠送物品	
//57025	请选择要赠送的好友	
//57026	$(6,此物品今日赠送已达上限)	
//57027	$(6,钻石不足)	
//57028	赠送成功，你与{玩家名字}亲密值增加{1}	
//57029	$(6,好友不在线，无法祝福)	
//57030	暂无好友	
//57031	当前没有申请信息	
//57032	当前没有记录	
//57033	加为好友	

    
//57036	输入昵称查找	

        #endregion

        #region 背包5000-6500
        BAG_SELL = 5000,                                //出售
        BAG_USE = 5001,                                 //使用
        BAG_EQUIP = 5002,                             //装备
        BAG_FENJIE = 5003,                            //分解
        BAG_SHOW = 5004,                            //展示
        BAG_XIANGQIAN = 5006,                   //镶嵌
        BAG_FUMO = 5007,                            //镶嵌
        BAG_HECHENG = 5008,                      //附魔
        BAG_CHARGE = 5009,                         //合成
        BAG_DUIBI_LEFT = 5010,                     //对比左手
        BAG_DUIBI_RIGHT = 5011,                  //对比右手
        BAG_GENGHUAN = 5012,                   //更换

        /**提示语（5031~5060）**/
        BAG_TIP_SELL_ZHENGUI = 5031,                         //此装备为[0]珍贵装备，确定出售吗？
        BAG_TIP_SELL_GET_GOLD = 5032,                       //出售装备获得{0}金币
        BAG_TIP_FULL_TO_MAIL = 5033,                         //背包已满，物品已发送至邮件，请及时领取
        BAG_TIP_UNLOCK_GRID = 5034,                         //是否花费{0} 钻石， 开启{1}个格子
        BAG_TIP_GEM_NOT_ENOUGH = 5035,                //没有足够的钻石

        /**描述文字（5061~5100）**/
        BAG_TIP_LEIXING = 5061,                            //类型：
        BAG_TIP_ZUOYONG = 5062,                       //作用：
        BAG_TIP_LAIYUAN = 5063,                         //来源：
        BAG_TIP_ChuShouJiaGe = 5064,                 //出售价格：
        BAG_TIP_ZhuangbeiShuxing = 5065,           //装备属性
        BAG_TIP_XiyouShuxing = 5066,                  //稀有属性
        BAG_TIP_BaoshiShuxing = 5067,                //宝石属性
        BAG_TIP_FumoXiaoguo = 5068,                 //附魔效果
        BAG_TIP_DangqianZhuangbei = 5069,       //当前装备
        BAG_TIP_Duidie = 5070,                            //堆叠：
        BAG_TIP_Dengji = 5071,                            //等级：
        BAG_TIP_Miaoshu = 5072,                        //描述：
        BAG_TIP_Weikaiqi = 5073,                        //未开启
        BAG_TIP_XuyaoDengji = 5074,                 //需要等级：
        BAG_TIP_XuyaoZhiye = 5075,                  //需要职业：
        BAG_TIP_Jiashuju = 5076,                        //假数据

        /**道具类型（5101~5600）**/
        BAG_ITEM_TYPE_Zhuangbei = 5101,                  //装备
        BAG_ITEM_TYPE_Baoshi = 5102,                        //宝石
        BAG_ITEM_TYPE_Fuwen= 5103,                         //符文
        BAG_ITEM_TYPE_Cailiao = 5104,                        //材料
        BAG_ITEM_TYPE_Xingyunshi = 5105,                 //幸运石
        BAG_ITEM_TYPE_PeifangTuzhi = 5106,              //配方图纸
        BAG_ITEM_TYPE_PutongDaoju = 5107,             //普通道具
        BAG_NOT_ENOUGH = 56009,                        //背包空间不足

        TASK_ALL_MAIN_TASK_FINISH = 51416,// 全部主线任务已完成！
        #endregion

        #region 邮箱37001~37799
        
        MAIL_TIP_DELETE_MAIL = 37702,               //删除所有邮件提示
        MAIL_TIP_DELETE_SUCCESS = 37703,            //刪除郵件成功
        MAIL_TIP_NONE_DRAWATTACH = 37701,           //提示玩家未领取附件
        MAIL_TIP_DELAY_COMPANSION = 37002,
        MAIL_TIP_NONE_MAIL = 37713,                 //没有邮件提示标签
        MAIL_TIP_BAG_FULL = 5041,                   //背包空间不足提示标签
        MAIL_TIP_NONE_ATTACHMENT = 37714,           //暂无附件可领取
        MAIL_TIP_SEND_DAY_MAIL_LIMIT = 37712,       //邮件发送达到上限
        MAIL_TIP_EMPTY_MAIL = 37709,                //空邮件提示
        MAIL_TIP_ADD_RECEIVER = 37710,              //请添加收件好友
        MAIL_TIP_COMEFROM = 37027,
        MAIL_TIP_CONTENT_LIMIT = 37708,             //邮件内容已达到上线
        MAIL_TIP_CLICK_INPUT_TEXT = 37705,          //点击输入邮件内容
        MAIL_TIP_SEND_MAIL_SUCCESS = 37711,         //邮件发送成功

        #endregion

        #region 制造
        MAKE_TRAIN_LEVEL_LIMIT = 32000,//人物等级达到{0}级才可继续培养
        MAKE_MATERIAL_LIMIT = 32001,//{0}不足，无法制造
        MAKE_ADD_EXP = 32002,//	获得制造经验+{0}
        MAKE_DOUBLE_EXP = 32003,//	天神保佑，获得制造经验双倍加成+{0}
        MAKE_SKILL_LEVEL_UP_AND_UNLOCK_FORMULA = 32004,//	恭喜，你的制造等级提升至{0}级，解锁了新的配方！
        MAKE_SKILL_LEVEL_UP = 32005,//恭喜，制造等级提升至{0}级！
        MAKE_CANT_TRAIN = 32006,//	
        MAKE_LEARN_LEVEL_LIMIT = 32008,//	制造等级达到{0}级才可以学习
        MAKE_LEVEL_LIMIT = 32010,//	人物等级达到{0}级才可以制造
        MAKE_LEARN_MONEY_LIMIT = 32011,//	{0}不足，无法学习
        MAKE_BAG_FULL = 32012,//背包已满，清理后再来制造吧

        MAKE_LEVEL_REGION = 32017,
        MAKE_TIME_LIMIT = 32018,
        MAKE_MONEY_ENOUGH = 32019,
        MAKE_MATERIAL_ENOUGH = 32020,//
        MAKE_MATERIAL_NOT_ENOUGH = 32021,
        MAKE_MONEY_NOT_ENOUGH = 32022,
        MAKE_DAY = 32023,
        #endregion

        #region 奖励系统
        REWARD_STILL_HAVE_VIP_REWARD = 33112,
        REWARD_NOT_HAVE_REWARD = 33113,
        REWARD_PROGRESS = 33114,
        REWARD_ONLINE_MINUTE = 33115,
        REWARD_ONLINE_HOUR = 33116,
        REWARD_LOGIN_DAY = 33117,
        REWARD_VIP_NUM_DOUBLE_REWARD = 33118,
        REWARD_VIP_NUM_GIFT = 33119,
        REWARD_LOGIN_MONTH_TITLE = 33120,
        REWARD_UPGRADE_NEXT_VIP_NEED_CHARGE = 33121,
        REWARD_REACH_MAX_VIP = 33122,
        REWARD_CATEGORY_DAILY_ACTIVE = 33123,
        REWARD_CATEGORY_DAILY_TASK = 33124,
        REWARD_CATEGORY_MONTH_LOGIN = 33125,
        REWARD_CATEGORY_VIP_GIFT = 33126,
        REWARD_CARD_LEFT_DAY = 33127,
        REWARD_CARD_LEFT_HOUR = 33128,
        REWARD_CARD_LEFT_MINUTE = 33129,
        REWARD_DAILY_TASK_NOT_REACH_TIME = 33130,
        REWARD_DAILY_TASK_TIME_END = 33131,
        REWARD_PLAYER_RETURN = 124000,
        REWARD_CATEGORY_REWARD_COMPENSATION = 124022,
        #endregion

        #region 宝石 34000~34999
        GEM_COMMON_BLESS_NUMBER = 34011,    //普通祝福*{0}
        GEM_LITTLE_BLESS_NUMBER = 34012,    //祝福小暴击*{0}
        GEM_BIG_BLESS_NUMBER = 34013,       //祝福大暴击*{0}
        GEM_EXTEND_GET_LITTLE_BLESS = 34014,//额外赠送祝福小暴击*{0}
        GEM_TOTAL_GET_BLESS = 34015,        //祝福值提升：{0}+{0}

        #endregion

        #region 翅膀 35000~35999
        WING_TIP_ACTIVE_SPIRIT = 35001,  //您激活了新的精灵翅膀套装属性!
        WING_TIP_ACTIVE_PHANTOM = 35002,  //您激活了新的幻影翅膀套装属性!
        WING_TIP_TRAIN_SMALL_EXP = 35003,  //普通培养，获得{0}经验值！
        WING_TIP_TRAIN_MIDDLE_EXP = 35004,  //培养小暴击，获得{0}+{1}经验值！
        WING_TIP_TRAIN_LARGE_EXP = 35005,  //培养大暴击，获得{0}+{1}经验值！
        WING_TIP_TRAIN_MIDDLE = 35006,  //培养小暴击
        WING_TIP_TRAIN_LARGE = 35007,  //培养大暴击
        WING_TIP_TRAIN_SMALL = 35008,  //普通培养
        WING_TIP_TRAIN_EXP = 35760,  //获得{0}+{1}点经验值！
        #endregion

        #region 强化系统11001~11006

        STRENGTHEN_MATERAIL_NONE = 11003,
        STRENGTHEN_CURRENCY_NONE = 11002,
        STRENGTHEN_SUCCESS = 11005,
        STRENGTHEN_FAIL = 11004,
        STRENGTHEN_UPLIMIT = 11006,

        #endregion

        #region 附魔卷轴
        ENCHANT_No_Equip_Can_Enchant = 30700, // 无装备可附魔
        ENCHANT_Unenchant = 30701, //附魔条件
        #endregion

        #region 装备分解
        COMPOSITION_PLEASE_SELECTED_EQUIP = 71000, 
        COMPOSITION_ARE_YOU_OK = 71001,
        COMPOSITION_BAG_NOT_ENOUGH = 71002,

        #endregion

        #region 代币
        TOKEN_LEVEL_ACTIVE = 60001,// {0}级可激活
        TOKEN_VIP_ACTIVE = 60002,//vip{0}可激活
        TOKEN_CONTRIBUTE_ACTIVE = 60003,//{0}公会贡献可激活
        TOKEN_VOCATION_ACTIVE = 992,//{0}公会贡献可激活
        TOKEN_FB_ACTIVE = 993,
        #endregion

        #region 副本
        COPY_FIGHT_LIMIT = 1101,
        COPY_LEVEL_LIMIT = 1102,
        COPY_COST_ENERGY = 1131,
        COPY_COST_GOLD = 1132,
        COPY_SCORE_RULE_TIME = 1301,
        COPY_SCORE_RULE_KILL_COUNT = 1302,
        COPY_SCORE_RULE_LIFE = 1303,
        COPY_SCORE_RULE_DEAD_COUNT = 1305,
        COPY_SCORE_RULE_NO_PET = 1304,
        ACTIVITY_NOT_OPEN = 44317,
        COPY_MATCHING_CAN_NOT_CALL = 97044,
        COPY_MATCHING_CAN_NOT_ENTRY = 97045,
        #endregion

        #region 星魂 63000~64000
        STARSOUL_TIP_ISACTIVATED_LABEL = 63000,
        STARSOUL_TIP_CANACTIVATE_LABEL = 63001,
        STARSOUL_TIP_NOTACTIVATE_LABEL = 63002,
        STARSOUL_TIP_ACTIVATED_CONDITION = 63003,
        STARSOUL_TIP_ACTIVATED_COST = 63004,
        STARSOUL_TIP_ACTIVATED_PROPERTY = 63005,
        STARSOUL_TIP_ACTIVATED_ITEM = 63006,
        STARSOUL_TIP_INTRORDUCE_TITLE = 63007,
        STARSOUL_TIP_INTRORDUCE_CONTENT = 63008,
        #endregion

        #region 时装
        FASHION_REAMIN_DAY_LABEL = 36506,
        FASHION_REAMIN_HOUR_LABEL = 36507,
        FASHION_REAMIN_FOREVER_LABEL = 36508,
        #endregion

        #region 交互
        PLAYERINFO_ADD_FRIEND = 63500,	//加为好友
        PLAYERINFO_DEL_FRIEND = 63501,  //删除好友
        PLAYERINFO_ADD_BLACKLIST = 63502,	//屏蔽
        PLAYERINFO_DEL_BLACKLIST = 63503,	//取消屏蔽
        PLAYERINFO_ADD_TEAM = 63504,	//邀请入队
        PLAYERINFO_APPLY_TEAM = 63505,	//申请入队
        PLAYERINFO_ADD_GUILD = 63506,	//邀请入会
        PLAYERINFO_APPLY_GUILD = 63507,	//申请入会

        #endregion

        #region 交易系统 56000-56299
        TRADE_CONSIGNMENT_TIME_24 = 56213,
        TRADE_CONSIGNMENT_TIME_48 = 56214,
        TRADE_PRICE_HIGHEST_TIP = 56005,
        TRADE_PRICE_CHEAPEST_TIP = 56006,
        TRADE_SELECT_PUTAWAY_ITEM = 56003,
        TRADE_GOLD_NOT_ENOUGHT = 56007,
        TRADE_NONE_ITEM_TO_BUY = 56110,
        TRADE_SELL_UPLIMIT = 56004,
        TRADE_BUY_UPLIMIT = 56000,
        TRADE_REFRESH_TIME_TXT = 56107,
        TRADE_NONE_ITEM_SELL = 56218,
        TRADE_BUY_ITEM_ONCONFIRM = 56002,
        TRADE_BAG_NOT_ENOUGHT = 56001,
        TRADE_OTHER_CHINESE = 56103,
        #endregion

        #region 快速通知
        FAST_NOTICE_MAIL = 55001,
        FAST_NOTICE_TEAM = 55002,
        FAST_NOTICE_FRIEND = 55003,
        FAST_NOTICE_BAG = 55004,
        #endregion

        #region 商城
        MALL_NEW_THING = 56700,
        MALL_HOT_SELL = 56701,
        MALL_FLASH_SALE = 56702,
        MALL_DISCOUNT = 56703,
        MALL_LIMIT_TIME = 56704,
        MALL_VIP = 56705,
        MALL_OLD_PRICE = 56706,
        MALL_DISCOUNT_PRICE = 56707,
        MALL_VIP_PRICE = 56708,
        MALL_LEFT_NUM = 56709,
        MALL_LEFT_TIME = 56710,
        MALL_SELL_OUT = 56711,
        MALL_HAD_BUY = 56712,
        MALL_NO_DISCOUNT_COUPON = 56713,
        MALL_VIP_LIMIT = 56721,
        MALL_CANCEL = 56722,
        MALL_BECOME_VIP = 56723,
        MALL_DAY_BUY_LIMIT = 56724,
        MALL_HAD_BUY_ITEM = 56725,
        MALL_ITEM_HAD_SELL_OUT = 56726,
        MALL_ITEM_EXPIRED = 56727,
        MALL_NOT_ENOUGH_DIAMOND = 56728,
        MALL_PAY = 56730,
        MALL_BAG_FULL = 56731,
        MALL_SEND_SUCCESSFUL_TIP = 56732,
        MALL_COUPON_COUNT = 56734,
        MALL_CLEAR_COUPON = 56735,
        MALL_ITEM_HAD_EXPIRED = 56736,
        MALL_TOTAL_BUY_LIMIT = 56738,
        MALL_BUY = 56740,
        MALL_BUY_SUCCESS = 56742,
        #endregion

        #region 试练系统 42000-42001
        WANDER_LAND_EXPLAIN1 = 42000,
        WANDER_LAND_EXPLAIN2 = 42001,
        #endregion

        #region 任务 
        TASK_FINISHED = 50201,//(已完成)
        #endregion

        #region 宠物
        PET_QUALITY_GREEN = 43202,
        PET_QUALITY_BLUE = 43203,
        PET_QUALITY_PURPLE = 43204,
        PET_QUALITY_ORANGE = 43205,
        PET_QUALITY_GOLDEN = 43206,

        PET_UNLOCK_LEVEL = 43400,
        PET_NO_FIGHTING_PET = 43401,
        PET_UNLOCK_QUALITY = 43402,
        PET_STATE_FIGHT = 43403,
        PET_STATE = 43404,
        PET_REACH_UNLOCK_LEVEL = 43405,
        PET_QUALITY = 43406,
        PET_LEVEL = 43407,
        PET_QUALITY1 = 43408,
        PET_ADD_EXP = 43410,
        PET_CURRENT_LEVEL = 43411,
        PET_LEVEL_LIMIT = 43412,
        PET_UNLOCK_SKILL = 43413,
        PET_PHYSICS_ATTACK = 43414,
        PET_MAGIC_ATTACK = 43415,
        PET_DEFENCE = 43416,
        PET_ASSIST = 43417,
        PET_ALL = 43418,
        PET_ORDER = 43419,
        #endregion

        #region 排行榜
        RANKINGLIST_OUT_OF_RANK = 65201,
        RANKINGLIST_BASIC_INFO = 65315,
        RANKINGLIST_FIGHT_INFO = 65327,
        RANKINGLIST_GEM = 5103,
        RANKINGLIST_RUNE = 5104,
        RANKINGLIST_NOT_JOIN_ANY_GUILD = 65311,
        RANKINGLIST_NOT_GEM_INLAYED = 65312,
        RANKINGLIST_NOT_RUNE_EQUIPED = 65313,
        RANKINGLIST_FIGHT_FORCE_COMPARE = 124206,
        #endregion

        #region 宝藏
        TREASURE_BOX_ACTIVITY_END = 99101,
        SHARE_TREASURE_CHEST_POSITION = 99103,
        TREASURE_USE_GET = 99109,
        SHARE_TREASURE_POSITION_COST_TIP = 99113,
        TREASURE_USE_WILD_LIMIT = 99120,
        TREASURE_USE_TIMES_LIMIT = 99121,
        TREASURE_BOX_VIP_ADD_TIMES = 99127,
        SHARE_TREASURE_BOSS_POSITION_TO_WORLD = 99204,
        SHARE_TREASURE_BOSS_POSITION = 99205,
        SHARE_TREASURE_POSITION_SUCCEED = 99206,
        SHARE_TREASURE_BOSS_POSITION_TO_GUILD = 99294,
        SHARE_SUMMON_STONE_BOSS_POSITION_TO_WORLD = 105530,
        SHARE_SUMMON_STONE_BOSS_POSITION = 105531,
        #endregion
    }
}
