using System;
using System.Collections.Generic;

namespace Common.ServerConfig {

    ///<summary>
    ///-系统消息id命名规则:
    ///</summary>


    ///<summary>
    ///
    ///</summary>


    ///<summary>
    ///    错误码: 1~29999
    ///</summary>


    ///<summary>
    ///    服务器系统公告: 30000~49999
    ///</summary>


    ///<summary>
    ///    客户端系统公告: 50000~59999
    ///</summary>


    ///<summary>
    ///    操作码: 60000+action_id
    ///</summary>


    ///<summary>
    ///
    ///</summary>


    public enum error_code {


    ///<summary>
    ///----------------------------------------------------------------------------------------
    ///</summary>
    

    ///<summary>
    ///错误码
    ///</summary>
    
    

    ///<summary>
    ///角色创建账号相关的错误码  --系统相关错误码
    ///</summary>
    
    ERR_SUCCESSFUL                = 0,

    ///<summary>
    ///正在创建角色中
    ///</summary>
    ERR_CREATING_CHARACTER        = 1, 

    ///<summary>
    ///该角色名已存在
    ///</summary>
    ERR_AVATAR_NAME_EXIST         = 2, 

    ///<summary>
    ///角色名太短
    ///</summary>
    ERR_AVATAR_NAME_TOO_SHORT     = 3, 

    ///<summary>
    ///角色名太长
    ///</summary>
    ERR_AVATAR_NAME_TOO_LONG      = 4, 

    ///<summary>
    ///创建角色数量已达上限
    ///</summary>
    ERR_CREATE_AVATAR_TO_LIMIT    = 5, 

    ///<summary>
    ///角色性别不正确
    ///</summary>
    ERR_AVATAR_GENDER             = 6, 

    ///<summary>
    ///角色职业不正确
    ///</summary>
    ERR_AVATAR_VOCATION           = 7, 

    ///<summary>
    ///角色数据不正常
    ///</summary>
    ERR_LOGIN_AVATAR_BAD          = 8, 

    ///<summary>
    ///该角色不是你的
    ///</summary>
    ERR_LOGIN_NOT_MY_CHARACTER    = 9, 

    ///<summary>
    ///账号正在选择角色进入游戏
    ///</summary>
    ERR_ACCOUNT_ENTERING_GAME     = 11, 

    ///<summary>
    ///账号已选择角色进入游戏
    ///</summary>
    ERR_ACCOUNT_ENTERED_GAME      = 12, 

    ///<summary>
    ///角色的dbid错误
    ///</summary>
    ERR_AVATAR_DBID               = 13, 

    ///<summary>
    ///角色vip等级过低
    ///</summary>
    ERR_AVATAR_VIP                = 14, 

    ///<summary>
    ///角色等级过低
    ///</summary>
    ERR_AVATAR_LEVEL_LOW          = 15, 

    ///<summary>
    ///角色体力不足
    ///</summary>
    ERR_AVATAR_NOT_ENOUGH_ENERGY  = 16, 

    ///<summary>
    ///没有cell
    ///</summary>
    ERR_AVATAR_NO_CELL            = 17, 

    ///<summary>
    ///协议格式错误
    ///</summary>
    ERR_MSG_FORMAT                = 18, 

    ///<summary>
    ///VIP等级参数错误
    ///</summary>
    ERR_AVATAR_VIP_PARAMETER      = 19, 

    ///<summary>
    ///玩家等级过高
    ///</summary>
    ERR_AVATAR_LEVEL_HIGH         = 20, 

    ///<summary>
    ///没有该玩家 玩家不存在
    ///</summary>
    ERR_NO_THIS_PLAYER            = 21, 

    ///<summary>
    ///usermgr没有创建成功
    ///</summary>
    ERR_USER_MGR_NOT_EXIST        = 22, 

    ///<summary>
    ///金币不足
    ///</summary>
    ERR_NOT_ENOUGH_GOLD           = 23, 

    ///<summary>
    ///钻石，元宝不足
    ///</summary>
    ERR_NOT_ENOUGH_DIAMOND        = 24, 

    ///<summary>
    ///绑定钻石不足
    ///</summary>
    ERR_NOT_ENOUGH_BIND_DIAMOND   = 25, 

    ///<summary>
    ///没有足够体力
    ///</summary>
    ERR_NOT_ENOUGH_ENERGY         = 26, 

    ///<summary>
    ///没有足够释放技能的能量值
    ///</summary>
    ERR_NOT_ENOUGH_SPELL_ENERGY   = 27, 

    ///<summary>
    ///没有足够精力
    ///</summary>
    ERR_NOT_ENOUGH_POWER          = 28, 

    ///<summary>
    ///名称中含有非法词汇
    ///</summary>
    ERR_AVATAR_NAME_HAS_BANNED_WORD = 29, 

    ///<summary>
    ///账号状态错误
    ///</summary>
    ERR_ACCOUNT_STATE             = 30, 

    ///<summary>
    ///玩家战斗力过低
    ///</summary>
    ERR_AVATAR_FIFHT_FORCE_LOW    = 31, 

    ///<summary>
    ///玩家战斗力过高
    ///</summary>
    ERR_AVATAR_FIFHT_FORCE_HIGH   = 32, 

    ///<summary>
    ///功能没有开启
    ///</summary>
    ERR_FUNCTION_NOT_OPEN         = 33, 

    ///<summary>
    ///任务未完成
    ///</summary>
    ERR_AVATAR_TASK               = 34, 

    ///<summary>
    ///任务未接受
    ///</summary>
    ERR_AVATAR_TASK_ACCEPTED      = 35, 

    ///<summary>
    ///没有足够的符文石
    ///</summary>
    ERR_NOT_ENOUGH_RUNE_STONE     = 36, 

    ///<summary>
    ///星魂不足
    ///</summary>
    ERR_NOT_ENOUGH_STAR_SPIRIT    = 37, 

    ///<summary>
    ///任务未领奖
    ///</summary>
    ERR_AVATAR_TASK_NOT_REWARDED  = 39, 

    ///<summary>
    ///ip禁止登陆
    ///</summary>
    ERR_LOGIN_IP_FORBIDDEN        = 40, 

    ///<summary>
    ///账号禁止登陆
    ///</summary>
    ERR_LOGIN_ACCOUNT_FORBIDDEN   = 41, 

    ///<summary>
    ///要塞石材不足
    ///</summary>
    ERR_NOT_ENOUGH_FORTRESS_STONE = 42, 

    ///<summary>
    ///要塞木材不足
    ///</summary>
    ERR_NOT_ENOUGH_FORTRESS_WOOD  = 43, 

    ///<summary>
    ///个人公会贡献值不足
    ///</summary>
    ERR_NOT_ENOUGH_CONTRIBUTION   = 44, 

    ///<summary>
    ///活跃值不足
    ///</summary>
    ERR_NOT_ENOUGH_DAILY_ACTIVE_POINT = 45, 

    ///<summary>
    ///活跃值不足
    ///</summary>
    ERR_NOT_ENOUGH_ACTIVE_POINT   = 46, 

    ///<summary>
    ///成就点不足
    ///</summary>
    ERR_NOT_ENOUGH_ACHIEVEMENT_POINT   = 47, 


    ///<summary>
    ///该格子未解锁
    ///</summary>
    ERR_GRID_LOCKED               = 48, 

    ///<summary>
    ///背包编号错误
    ///</summary>
    ERR_PKG_INDEX                 = 49, 


    ///<summary>
    ///-与传送相关的错误码---
    ///</summary>
    

    ///<summary>
    ///角色正在传送中
    ///</summary>
    ERR_AVATAR_IN_TELEPORT        = 50, 

    ///<summary>
    ///玩家已在该关卡中
    ///</summary>
    ERR_AVATAR_HAS_IN_MAP         = 51, 

    ///<summary>
    ///关卡人数已满
    ///</summary>
    ERR_MISSION_PLAYER_FULL       = 52, 

    ///<summary>
    ///关卡已被重置
    ///</summary>
    ERR_MISSION_RESET             = 53, 

    ///<summary>
    ///需要有公会
    ///</summary>
    ERR_MISSION_NEED_GUILD        = 54, 

    ///<summary>
    ///关卡还在准备中
    ///</summary>
    ERR_MISSION_IS_PREPARING      = 55, 

    ///<summary>
    ///现在正处于跨服地图中
    ///</summary>
    ERR_MISSION_IS_IN_CROSS_MAP   = 56, 


    ///<summary>
    ///未定义的消费类别配置
    ///</summary>
    ERR_COST_CLASS_NOT_SUPPORT    = 57, 

    ///<summary>
    ///消费类型配置不支持
    ///</summary>
    ERR_COST_TYPE_NOT_SUPPORT     = 58, 
    

    ///<summary>
    ///导师值不足
    ///</summary>
    ERR_NOT_ENOUGH_TUTOR_VALUE    = 60, 



    ///<summary>
    ///---技能相关的错误码 101-150---
    ///</summary>
    

    ///<summary>
    ///客户端时间戳出问题啦
    ///</summary>
    ERR_CLIENT_TIME_TICK         = 101,  

    ///<summary>
    ///技能释放者已死亡
    ///</summary>
    ERR_ENTITY_DEATH             = 102,  

    ///<summary>
    ///技能未装备
    ///</summary>
    ERR_SPELL_NOT_EQUIPED        = 103,

    ///<summary>
    ///技能不在正确释放区域
    ///</summary>
    ERR_SPELL_NOT_IN_ZONE        = 109,

    ///<summary>
    ///技能释放时间间隔太短
    ///</summary>
    ERR_SPELL_INTV_TOO_SHORT     = 110, 

    ///<summary>
    ///技能在公共cd中
    ///</summary>
    ERR_SPELL_IN_COMMON_CD       = 111, 

    ///<summary>
    ///技能在自身的cd中
    ///</summary>
    ERR_SPELL_IN_CD              = 112, 

    ///<summary>
    ///该连续技能处于cd中
    ///</summary>
    ERR_SPELL_CONTINUE_IN_CD     = 113, 

    ///<summary>
    ///该连续技已超时
    ///</summary>
    ERR_SPELL_CONTINUE_TIMEOUT   = 114, 

    ///<summary>
    ///前置技能未释放
    ///</summary>
    ERR_SPELL_PREVIA_NOT_CAST    = 115,  

    ///<summary>
    ///前置技能的次数少于规定次数
    ///</summary>
    ERR_SPELL_PREVIA_CNT_LESS    = 116,  

    ///<summary>
    ///该技能还未学会
    ///</summary>
    ERR_SPELL_NOT_LEARNED        = 117,  

    ///<summary>
    ///该技能的激活buff不存在
    ///</summary>
    ERR_SPELL_NOT_ACTIVE_BUFF    = 118,  

    ///<summary>
    ///目标跟自己不在同一场景中
    ///</summary>
    ERR_TGT_NOT_IN_SAME_SPACE    = 119, 

    ///<summary>
    ///目标不存在
    ///</summary>
    ERR_TGT_NOT_EXIST            = 120, 

    ///<summary>
    ///目标距离自己太远
    ///</summary>
    ERR_TGT_DIS_TOO_FAR          = 121, 

    ///<summary>
    ///该状态下不能发起攻击
    ///</summary>
    ERR_NOT_ATTACK_IN_STATE      = 122, 

    ///<summary>
    ///技能在组cd中
    ///</summary>
    ERR_SPELL_IN_GROUP_CD        = 123, 

    ///<summary>
    ///该物品不能被拾取
    ///</summary>
    ERR_ITEM_CANOT_BE_PICKUP     = 140, 

    ///<summary>
    ///该物品拾取次数已达上限
    ///</summary>
    ERR_ITEM_PICKUP_TIMES_OUT    = 141, 


    ///<summary>
    ///--------------------------GM 181~199-------------------------
    ///</summary>
    

    ///<summary>
    ///授予GM权限成功
    ///</summary>
    ERR_GM_GIVE_PRIVILEGE_SUCCESS = 181,    
    ERR_GM_CMD_TOO_SHORT          = 182,

    ///<summary>
    ///非GM账号
    ///</summary>
    ERR_GM_NO_ACCOUNT_PRIVILEGE   = 183,    

    ///<summary>
    ///没有执行这条GM指令的权限
    ///</summary>
    ERR_GM_NO_EXC_CMD_PRIVILEGE   = 184,    

    ///<summary>
    ///xml没有配置这个GM指令
    ///</summary>
    ERR_GM_NO_CMD_IN_XML          = 185,    

    ///<summary>
    ///代码里没有这个GM指令
    ///</summary>
    ERR_GM_NO_CMD_IN_CODE         = 186,    

    ///<summary>
    ///GM指令格式错误
    ///</summary>
    ERR_GM_CMD_FORMAT_WRONG       = 187,    

    ///<summary>
    ///GM指令执行成功
    ///</summary>
    ERR_GM_EXC_CMD_SUCCESS        = 188,    


    ///<summary>
    ///--------------------------mgr_action 201-250 -------------------------
    ///</summary>
    

    ///<summary>
    ///操作id和玩家现有状态冲突
    ///</summary>
    ERR_ACTION_FORBID_IN_STATE    = 201,   

    ///<summary>
    ///角色未处于允许该操作的状态
    ///</summary>
    ERR_ACTION_HASNOT_STATE       = 202,   



    ///<summary>
    ///--------------------------聊天mgr_avatar_chat 210~250 -------------------------
    ///</summary>
    

    ///<summary>
    ///没有这个聊天频道
    ///</summary>
    ERR_CHAT_NO_THIS_CHANNEL             = 210,      

    ///<summary>
    ///聊天消息为空
    ///</summary>
    ERR_CHAT_MSG_EMPTY                   = 211,      

    ///<summary>
    ///聊天消息太长，超过60
    ///</summary>
    ERR_CHAT_MSG_TOO_LONG                = 212,      

    ///<summary>
    ///自己和自己聊天
    ///</summary>
    ERR_CHAT_SPEAK_TO_YORSELF            = 213,      

    ///<summary>
    ///聊天对象不在线
    ///</summary>
    ERR_CHAT_PERSON_NOT_ONLINE           = 214,      

    ///<summary>
    ///发言过于频繁，还需要等待{0}秒
    ///</summary>
    ERR_CHAT_CHANNEL_CD                  = 215,      

    ///<summary>
    ///被对方屏蔽了
    ///</summary>
    ERR_CHAT_SHIELD                      = 216,      

    ///<summary>
    ///自己不能屏蔽/取消屏蔽自己
    ///</summary>
    ERR_CHAT_SHIELD_TO_YORSELF           = 217,      

    ///<summary>
    ///活力值不足，不能在此频道发言
    ///</summary>
    ERR_CHAT_ENERGY_NOT_ENOUGH           = 218,      

    ///<summary>
    ///等级不够,等级达到{0}级后才可以在此频道发言
    ///</summary>
    ERR_CHAT_LEVEL_NOT_ENOUGH            = 219,      

    ///<summary>
    ///加入{0}方可在此频道发言
    ///</summary>
    ERR_CHAT_NOT_INTO_CHANNEL            = 220,      

    ///<summary>
    ///被永久禁言了
    ///</summary>
    ERR_CHAT_BAN_CHAT_FOREVER            = 221,      

    ///<summary>
    ///被禁言了，剩余时间
    ///</summary>
    ERR_CHAT_BAN_CHAT                    = 222,      


    ///<summary>
    ///--------------------------邮件MailMgr 251~299 -------------------------
    ///</summary>
    

    ///<summary>
    ///ERR_MAIL_INFO_REQUESTING             = 251,      --已加入到队列正在请求邮件信息
    ///</summary>
    

    ///<summary>
    ///邮件已被阅读
    ///</summary>
    ERR_MAIL_ALREADY_READ                = 252,      

    ///<summary>
    ///该邮件不存在
    ///</summary>
    ERR_MAIL_NOT_EXIST                   = 253,      

    ///<summary>
    ///avatar没有任何邮件
    ///</summary>
    ERR_MAIL_AVATAR_HAS_NO_MAILS         = 254,      

    ///<summary>
    ///该邮件没有附件
    ///</summary>
    ERR_MAIL_NO_ATTACHMENT               = 255,      

    ///<summary>
    ///邮件已领取附件
    ///</summary>
    ERR_MAIL_ALREADY_GET                 = 256,      

    ///<summary>
    ///邮件操作非法
    ///</summary>
    ERR_MAIL_ILLEGALITY                  = 257,      

    ///<summary>
    ///已达每日邮件上限
    ///</summary>
    ERR_MAIL_DAY_LIMIT                   = 258,      

    ///<summary>
    ///没有邮件
    ///</summary>
    ERR_MAIL_NO_MAIL                     = 259,      

    ///<summary>
    ///邮件附件中的道具不足
    ///</summary>
    ERR_MAIL_ATTACHMENT_NOT_ENOUGH       = 260,      

    ///<summary>
    ///该邮件有附件,不能删除
    ///</summary>
    ERR_MAIL_DEL_ATTACHMENT              = 261,      

    ///<summary>
    ///发送邮件失败
    ///</summary>
    ERR_MAIL_SEND_FAILED                 = 262,      


    ///<summary>
    ///--------------------------物品mgr_avatar_item 300~350 -------------------------
    ///</summary>
    

    ///<summary>
    ///背包已满
    ///</summary>
    ERR_ITEM_BAG_FULL                    = 300,      

    ///<summary>
    ///没有这个物品
    ///</summary>
    ERR_ITEM_NO_THIS_ITEM                = 301,      

    ///<summary>
    ///没有这个使用效果
    ///</summary>
    ERR_ITEM_NO_THIS_USE_EFFECT          = 302,      

    ///<summary>
    ///avatar不够等级使用道具
    ///</summary>
    ERR_ITEM_USE_AVATAR_LEVEL_TOO_LOW    = 303,      

    ///<summary>
    ///背包里没有这个物品
    ///</summary>
    ERR_ITEM_NO_THIS_ITEM_IN_BAG         = 304,      

    ///<summary>
    ///背包里没有足够的物品
    ///</summary>
    ERR_NOT_ENOUGH_ITEM                  = 305,      

    ///<summary>
    ///没有配置奖励数据
    ///</summary>
    ERR_ITEM_NO_REWARD_DATA              = 306,      

    ///<summary>
    ///该物品不能使用
    ///</summary>
    ERR_ITEM_CAN_NOT_USE                 = 307,      

    ///<summary>
    ///没有这种背包类型
    ///</summary>
    ERR_ITEM_NO_THIS_PKG_TYPE            = 308,      

    ///<summary>
    ///物品位置不对
    ///</summary>
    ERR_ITEM_POSITION_WRONG              = 309,      

    ///<summary>
    ///移动空物品
    ///</summary>
    ERR_MOVE_EMPTY_ITEM                  = 310,      

    ///<summary>
    ///该物品不能出售
    ///</summary>
    ERR_ITEM_CAN_NOT_SELL                = 311,      

    ///<summary>
    ///物品移动位置没有变化
    ///</summary>
    ERR_ITEM_NO_MOVE                     = 313,      

    ///<summary>
    ///背包没有物品
    ///</summary>
    ERR_PKG_HAS_NO_ITEMS                 = 314,      

    ///<summary>
    ///该物品没有那么多数量
    ///</summary>
    ERR_ITEM_REQ_TOO_MANY_COUT           = 315,      

    ///<summary>
    ///该物品不能购买
    ///</summary>
    ERR_ITEM_CAN_NOT_BUY                 = 316,      

    ///<summary>
    ///赎回仓库已满
    ///</summary>
    ERR_ITEM_PKG_REDEEM_FULL             = 317,      

    ///<summary>
    ///物品已过期失效
    ///</summary>
    ERR_ITEM_OUT_OF_DATE                 = 318,      

    ///<summary>
    ///背包数量已达最大
    ///</summary>
    ERR_BAG_COUNT_BE_MAX                 = 322,      

    ///<summary>
    ///这件装备不能穿到指定的位置
    ///</summary>
    ERR_EQUIP_CANT_WARE                  = 323,      

    ///<summary>
    ///玩家职业不匹配
    ///</summary>
    ERR_ITEM_AVATAR_VACATION_NOT_MATCH   = 324,      

    ///<summary>
    ///该格子上已有物品
    ///</summary>
    ERR_HAS_ITEM_IN_GRID                 = 325,      

    ///<summary>
    ///该格子上没有物品
    ///</summary>
    ERR_NOT_ITEM_IN_GRID                 = 326,      

    ///<summary>
    ///没有足够的空闲格子
    ///</summary>
    ERR_ITEM_NOT_ENOUGH_GRID             = 327,      

    ///<summary>
    ///需要预留格子
    ///</summary>
    ERR_ITEM_NEED_EMPTY_GRID             = 328,      

    ///<summary>
    ///不能添加奖励
    ///</summary>
    ERR_REWARD_CANT_ADD                  = 340,      

    ///<summary>
    ///达到上限
    ///</summary>
    ERR_ITEM_OBTAIN_LIMIT                = 341,      

    ///<summary>
    ///不能转让给这个玩家
    ///</summary>
    ERR_ITEM_TRANSFER_PLAYER             = 342,      

    ///<summary>
    ///超出了转让时间
    ///</summary>
    ERR_ITEM_TRANSFER_TIME               = 343,      

    ///<summary>
    ///沒有使用效果
    ///</summary>
    ERR_ITEM_NOT_EFFECT                  = 344,      


    ///<summary>
    ///--------------------------任务mgr_avatar_task 351~400 -------------------------
    ///</summary>
    

    ///<summary>
    ///任务配置不存在
    ///</summary>
    ERR_TASK_CFG_NOT_EXIST               = 351,      

    ///<summary>
    ///已经接受这个任务了
    ///</summary>
    ERR_TASK_ALREADY_ACCEPT              = 352,      

    ///<summary>
    ///身上没有这个任务
    ///</summary>
    ERR_TASK_NO_THIS_TASK_IN_BODY        = 353,      

    ///<summary>
    ///任务未完成
    ///</summary>
    ERR_TASK_NOT_FINISH                  = 354,      

    ///<summary>
    ///前置任务未完成
    ///</summary>
    ERR_TASK_LAST_NOT_FINISH             = 355,      

    ///<summary>
    ///没有这个任务类型
    ///</summary>
    ERR_TASK_NO_THIS_TYPE                = 356,      

    ///<summary>
    ///主线任务不能放弃
    ///</summary>
    ERR_MAIN_TASK_CAN_NOT_DISCARD        = 357,      

    ///<summary>
    ///任务已经完成了
    ///</summary>
    ERR_TASK_ALREADY_FINISH              = 358,      

    ///<summary>
    ///任务还没有接受
    ///</summary>
    ERR_TASK_NOT_ACCEPT                  = 359,      



    ///<summary>
    ///--------------------------关卡副本mgr_avatar_mission 380~450 --------------------
    ///</summary>
    

    ///<summary>
    ///该章节关卡还未全部通关
    ///</summary>
    ERR_MISSION_NOT_PASS_CHAPTER                      = 394,      

    ///<summary>
    ///该关卡没有该分线
    ///</summary>
    ERR_MISSION_NOT_THE_LINE                          = 395,      

    ///<summary>
    ///该关卡没有开分线
    ///</summary>
    ERR_MISSION_NOT_LINES                             = 396,      

    ///<summary>
    ///关卡人数已达上限
    ///</summary>
    ERR_MISSION_PLAYER_CNT_TO_LIMIT                   = 397,      

    ///<summary>
    ///等待匹配中
    ///</summary>
    ERR_MISSION_WAITING_MATCH                         = 398,      

    ///<summary>
    ///副本禁止匹配
    ///</summary>
    ERR_MISSION_FORBID_MATCH                          = 399,      

    ///<summary>
    ///玩家人数不够
    ///</summary>
    ERR_MISSION_NOT_ENOUGH_PLAYER                     = 400,      

    ///<summary>
    ///没有这个关卡配置
    ///</summary>
    ERR_NO_THIS_MISSION                               = 401,      

    ///<summary>
    ///超过每日挑战关卡次数
    ///</summary>
    ERR_MISSION_NO_MORE_DAILY_ENTER_COUNT             = 402,      

    ///<summary>
    ///未完成前置关卡
    ///</summary>
    ERR_MISSION_NOT_FINISH_PRE_MISSION                = 403,      

    ///<summary>
    ///处于非单机副本状态
    ///</summary>
    ERR_MISSION_AVATAR_STATE_NOT_CONSOLE              = 404,      

    ///<summary>
    ///没有三星评分
    ///</summary>
    ERR_MISSION_NOT_THREE_STARS                       = 405,      

    ///<summary>
    ///没有足够的扫荡券
    ///</summary>
    ERR_MISSION_NOT_ENOUGH_MOP_UP_TICKET              = 406,      

    ///<summary>
    ///体力不足
    ///</summary>
    ERR_MISSION_NOT_ENOUGH_ENERGY                     = 407,      

    ///<summary>
    ///挑战次数不足
    ///</summary>
    ERR_MISSION_NOT_ENOUGH_DAILY_TIMES                = 408,      

    ///<summary>
    ///材料不足
    ///</summary>
    ERR_MISSION_NOT_ENOUGH_MATERIAL                   = 409,      

    ///<summary>
    ///金币不足
    ///</summary>
    ERR_MISSION_NOT_ENOUGH_GOLD                       = 410,      

    ///<summary>
    ///钻石不足
    ///</summary>
    ERR_MISSION_NOT_ENOUGH_DIAMOND                    = 411,      

    ///<summary>
    ///绑定钻石不足
    ///</summary>
    ERR_MISSION_NOT_ENOUGH_BIND_DIAMOND               = 412,      

    ///<summary>
    ///没有挑战过副本
    ///</summary>
    ERR_MISSION_NOT_CHALLENGE                         = 415,      

    ///<summary>
    ///副本记录数据错误
    ///</summary>
    ERR_MISSION_RECORD_DATA_ERROR                     = 416,      

    ///<summary>
    ///奖励已经领取
    ///</summary>
    ERR_MISSION_REWARD_HAS_GET                        = 417,      

    ///<summary>
    ///前置关卡没有解锁
    ///</summary>
    ERR_MISSION_PRE_IS_LOCK                           = 418,      

    ///<summary>
    ///关卡id不对
    ///</summary>
    ERR_MISSION_ID_IS_ERR                             = 419,      


    ///<summary>
    ///没有这个章节
    ///</summary>
    ERR_NO_THIS_CHAPTER                               = 420,      

    ///<summary>
    ///这个章节没有星魂奖励
    ///</summary>
    ERR_CHAPTER_NO_STAR_SPIRIT_REWAED                 = 421,      

    ///<summary>
    ///星魂奖等级别错误
    ///</summary>
    ERR_CHAPTER_STAR_SPIRIT_REWARD_LEVEL              = 422,      

    ///<summary>
    ///章节星魂奖励已经领取过
    ///</summary>
    ERR_CHAPTER_REWARD_HAS_GET                        = 423,      


    ///<summary>
    ///掉落物不存在
    ///</summary>
    ERR_MISSION_NO_DROP_ITEM                           = 425,      

    ///<summary>
    ///掉落物不属于你
    ///</summary>
    ERR_MISSION_DROP_ITEM_NOT_YOU                      = 426,      

    ///<summary>
    ///野外掉落，捡取物品已达上限
    ///</summary>
    ERR_MISSION_PICK_DROP_LIMIT                        = 427,      

    ///<summary>
    ///没有复活次数了
    ///</summary>
    ERR_MISSION_NO_REVIVE_CNT                          = 428,      

    ///<summary>
    ///复活冷却cd中
    ///</summary>
    ERR_MISSION_REVIVE_CD                              = 429,      


    ///<summary>
    ///根据副本类型购买次数配置错误,无法购买
    ///</summary>
    ERR_MISSION_TYPE_BUY_CFG_ERROR                     = 430,      

    ///<summary>
    ///超过vip购买限制，无法购买
    ///</summary>
    ERR_MISSION_TYPE_BUY_LIMIT                         = 431,      

    ///<summary>
    ///有组队，进入组队副本，需要先退队，弹出提示框
    ///</summary>
    ERR_MISSION_TEAM_TYPE_SINGLE                       = 432,      


    ///<summary>
    ///重置次数不足
    ///</summary>
    ERR_MISSION_NO_ENOUGH_RESET_TIMES                  = 435,      

    ///<summary>
    ///重置次数充足，不用购买
    ///</summary>
    ERR_MISSION_RESET_TIMES_IS_ENOUGH                  = 436,      

    ///<summary>
    ///vip等级不足，重置次数已经用完
    ///</summary>
    ERR_MISSION_NO_ENOUGH_VIP_LEVEL                    = 437,      

    ///<summary>
    ///组队副本不允许单人中途退出
    ///</summary>
    ERR_MISSION_NO_QUIT_IN_TEAM                        = 438,      
    

    ///<summary>
    ///某个玩家拒绝再来一局
    ///</summary>
    ERR_MISSION_PLAY_AGAIN_REJECT                      = 440,      

    ///<summary>
    ///某个玩家次数已达上限
    ///</summary>
    ERR_MISSION_ONE_PLAYER_CNT_TO_LIMIT                = 441,      

    ///<summary>
    ///该关卡不能再来一次
    ///</summary>
    ERR_MISSION_NOT_PLAY_AGAIN                         = 442,      

    ///<summary>
    ///该关卡还未结算
    ///</summary>
    ERR_MISSION_NOT_FINISHED                           = 443,      

    ///<summary>
    ///公会试炼副本不在活动时间内
    ///</summary>
    ERR_MISSION_NOT_IN_GUILD_TRY_MISSION               = 444,      

    ///<summary>
    ///前置低级副本未通关
    ///</summary>
    ERR_PRE_MISSION_NOT_WIN                            = 445,      



    ///<summary>
    ///--------------------buff相关的错误码 451-500 ------------------------
    ///</summary>
    

    ///<summary>
    ///buff不存在
    ///</summary>
    ERR_BUFF_NOT_EXIST                                = 451,  


    ///<summary>
    ///--------------------------装备mgr_avatar_equip 501~550 --------------------
    ///</summary>
    

    ///<summary>
    ///不是装备
    ///</summary>
    ERR_NOT_EQUIPMENT                    = 501,      

    ///<summary>
    ///不能重铸
    ///</summary>
    ERR_EQUIP_CANT_RECAST                      = 502,      


    ///<summary>
    ///-------------------------宝石mgr_avatar_gem  551~600-------------------
    ///</summary>
    

    ///<summary>
    ///宝石系统错误代码
    ///</summary>
    

    ///<summary>
    ///参数错误
    ///</summary>
    ERR_GEM_PARA                        = 551,  

    ///<summary>
    ///配置错误
    ///</summary>
    ERR_GEM_CONFIG                      = 552,  

    ///<summary>
    ///材料不足
    ///</summary>
    ERR_GEM_NOT_ENOUGH_MATERIAL         = 554,  

    ///<summary>
    ///删除宝石失败
    ///</summary>
    ERR_GEM_DEL_FAILED                  = 555,  

    ///<summary>
    ///宝石和装备不匹配
    ///</summary>
    ERR_GEM_CAN_NOT_INLAY               = 556,  

    ///<summary>
    ///等级已最高
    ///</summary>
    ERR_GEM_LEVEL_ALREADY_MAX           = 557,  

    ///<summary>
    ///装备不存在
    ///</summary>
    ERR_GEM_EQUI_NOT_EXISTS             = 558,  

    ///<summary>
    ///宝石不存在
    ///</summary>
    ERR_GEM_NOT_EXISTS                  = 559,  

    ///<summary>
    ///背包没有足够的空格
    ///</summary>
    ERR_GEM_NO_EMPTY_GRID               = 560,  

    ///<summary>
    ///装备插槽满了或者与镶嵌宝石不匹配
    ///</summary>
    ERR_GEM_SLOT_FULL_OR_NOT_MATCH      = 561,  

    ///<summary>
    ///装备的插槽不存在
    ///</summary>
    ERR_GEM_EQUI_SLOT_NOT_EXISTS        = 562,  

    ///<summary>
    ///宝石不能取下
    ///</summary>
    ERR_GEM_CAN_NOT_OUTLAY              = 563,  

    ///<summary>
    ///装备的插槽上没有宝石
    ///</summary>
    ERR_GEM_SLOT_NO_GEM                 = 564,  

    ///<summary>
    ///玩家等级不够
    ///</summary>
    ERR_GEM_SLOT_LEVEL_NOT_ENOUGH       = 565,  

    ///<summary>
    ///该扩展槽已经激活
    ///</summary>
    ERR_GEM_SLOT_ACTIVE_HAS_ACTIVE      = 566,  

    ///<summary>
    ///前一个扩展槽还未激活
    ///</summary>
    ERR_GEM_SLOT_ACTIVE_PRE_NOT_ACTIVE  = 567,  

    ///<summary>
    ///激活扩展槽花费不够
    ///</summary>
    ERR_GEM_SLOT_ACTIVE_COST_NOT_ENOUGH = 568,  

    ///<summary>
    ///激活扩展槽失败
    ///</summary>
    ERR_GEM_SLOT_ACTIVE_COST_FAILURE    = 569,  

    ///<summary>
    ///卖出宝石大于当前格子的数量
    ///</summary>
    ERR_GEM_NUM_TOO_MUCH                = 570,  

    ///<summary>
    ///该宝石不能出售
    ///</summary>
    ERR_GEM_CAN_NOT_SELL                = 571,  

    ///<summary>
    ///玩家等级不够
    ///</summary>
    ERR_GEM_AVATAR_LEVEL_NOT_ENOUGH     = 572,  

    ///<summary>
    ///职业不匹配
    ///</summary>
    ERR_GEM_AVATAR_VOCATION_NOT_MATCH   = 573,  

    ///<summary>
    ///扩展槽未激活
    ///</summary>
    ERR_GEM_SLOT_NOT_ACTIVE             = 574,  

    ///<summary>
    ///不是装备
    ///</summary>
    ERR_GEM_NOT_EQUIP                   = 575,  

    ///<summary>
    ///宝石战斗属性效果参数错误
    ///</summary>
    ERR_GEM_EFFECT_PARAMETER_ERROR      = 576,  

    ///<summary>
    ///不可以打孔
    ///</summary>
    ERR_GEM_CAN_NOT_PUNCHING            = 577,  

    ///<summary>
    ///宝石封灵数据不存在
    ///</summary>
    ERR_GEM_SPIRIT_CFG_NOT_EXISTS       = 578,  

    ///<summary>
    ///宝石封灵已达最大等级
    ///</summary>
    ERR_GEM_SPIRIT_HAS_MAX_LEVEL        = 579,  

    ///<summary>
    ///原有槽宝石类型相同，不能镶嵌
    ///</summary>
    ERR_GEM_INLAY_IS_SAME               = 580,  


    ///<summary>
    ///--------------------------公会GuildMgr 601~650 --------------------
    ///</summary>
    

    ///<summary>
    ///没有这个公会
    ///</summary>
    ERR_GUILD_NO_THIS_GUILD              = 601,      

    ///<summary>
    ///公会成员已满
    ///</summary>
    ERR_GUILD_MEMBER_FULL                = 602,      

    ///<summary>
    ///不是这个公会的成员
    ///</summary>
    ERR_GUILD_NOT_THIS_GUILD_MEMBER      = 603,      

    ///<summary>
    ///公会名字已被占用
    ///</summary>
    ERR_GUILD_NAME_BE_USED               = 604,      

    ///<summary>
    ///没有权限
    ///</summary>
    ERR_GUILD_HAVE_NO_RIGHT              = 605,      

    ///<summary>
    ///已经在公会里
    ///</summary>
    ERR_GUILD_ALREADY_IN_GUILD           = 606,      

    ///<summary>
    ///未加入公会
    ///</summary>
    ERR_GUILD_NOT_IN_GUILD               = 607,      

    ///<summary>
    ///正在解散公会
    ///</summary>
    ERR_GUILD_DELETING                   = 608,      

    ///<summary>
    ///公会非正在解散状态
    ///</summary>
    ERR_GUILD_STATE_NOT_DEL              = 609,      

    ///<summary>
    ///公会没有成员
    ///</summary>
    ERR_GUILD_NO_MEMBERS                 = 610,      

    ///<summary>
    ///玩家等级不足
    ///</summary>
    ERR_GUILD_PLAYER_LEVEL_LOW           = 611,      

    ///<summary>
    ///工会技能不存在
    ///</summary>
    ERR_GUILD_SKILL_NOT_EXIST            = 612,      

    ///<summary>
    ///前一个等级的工会技能不存在
    ///</summary>
    ERR_GUILD_SKILL_LAST_NOT_EXIST       = 613,      

    ///<summary>
    ///已经申请加入该公会，等待审批
    ///</summary>
    ERR_GUILD_ALREADY_APPLY_JOIN         = 614,      

    ///<summary>
    ///已经领取了
    ///</summary>
    ERR_GUILD_REWARDED                   = 615,      

    ///<summary>
    ///已经领取了
    ///</summary>
    ERR_GUILD_NOT_COMPLETED              = 616,      

    ///<summary>
    ///工会物资已满
    ///</summary>
    ERR_GUILD_BAG_FULL                   = 617,      

    ///<summary>
    ///对方已经取消申请加入该公会
    ///</summary>
    ERR_GUILD_CANCEL_APPLY               = 618,      

    ///<summary>
    ///物品不够
    ///</summary>
    ERR_GUILD_ITEM_NOT_ENOUGH            = 619,      

    ///<summary>
    ///配置不存在
    ///</summary>
    ERR_GUILD_CONFIG_ISNT_EXIST          = 620,      

    ///<summary>
    ///配置与工会等级不匹配
    ///</summary>
    ERR_GUILD_LEVEL_ISNT_MATCH           = 621,      

    ///<summary>
    ///达到等级上限
    ///</summary>
    ERR_GUILD_ACHIEVE_MAX_LEVEL          = 622,      

    ///<summary>
    ///公会等级不足
    ///</summary>
    ERR_GUILD_LEVEL_NOT_ENOUGH           = 623,      

    ///<summary>
    ///公会人数不足
    ///</summary>
    ERR_GUILD_MEMBER_COUNT_NOT_ENOUGH    = 624,      

    ///<summary>
    ///公会繁荣度不足
    ///</summary>
    ERR_GUILD_BOOMING_NOT_ENOUGH         = 625,      

    ///<summary>
    ///公会资金不足
    ///</summary>
    ERR_GUILD_FUND_NOT_ENOUGH            = 626,      

    ///<summary>
    ///公会资金达到上限
    ///</summary>
    ERR_GUILD_FUND_MAX                   = 627,      

    ///<summary>
    ///公会玩家贡献达到上限
    ///</summary>
    ERR_GUILD_PLAYER_CONTRIBUTION_MAX    = 628,      

    ///<summary>
    ///已经取消加入公会
    ///</summary>
    ERR_GUILD_ALREADY_CANCEL_JOIN        = 629,      

    ///<summary>
    ///会长不能离开公会
    ///</summary>
    ERR_GUILD_LEADER_NOt_LEAVE           = 630,      

    ///<summary>
    ///职位不变
    ///</summary>
    ERR_GUILD_POSITION_INVARIANT         = 631,      

    ///<summary>
    ///这个职位已经达到人数上限
    ///</summary>
    ERR_GUILD_POSITION_LIMIT             = 632,      

    ///<summary>
    ///技能已经学习了
    ///</summary>
    ERR_GUILD_SKILL_ALREADY_STUDY        = 633,      

    ///<summary>
    ///公会名字过长
    ///</summary>
    ERR_GUILD_NAME_TOO_LOOG              = 634,      

    ///<summary>
    ///未输入公会名字
    ///</summary>
    ERR_GUILD_NAME_IS_NULL               = 635,      

    ///<summary>
    ///没有选择公会徽章
    ///</summary>
    ERR_GUILD_BADGE_TYPE_IS_NULL         = 636,      

    ///<summary>
    ///刚退出上一个公会,需要等待｛｝时｛｝分后才能申请新的公会
    ///</summary>
    ERR_GUILD_APPLY_CD                   = 637,      

    ///<summary>
    ///公会名字中含有非法词汇
    ///</summary>
    ERR_GUILD_NAME_HAS_BANNED_WORD       = 638,      

    ///<summary>
    ///你刚发布了公告，休息下吧
    ///</summary>
    ERR_GUILD_NOTICE_CD                  = 639,      

    ///<summary>
    ///你今天发布了太多公告，休息一下吧
    ///</summary>
    ERR_GUILD_NOTICE_LIMIT               = 640,      

    ///<summary>
    ///对方已经在公会
    ///</summary>
    ERR_GUILD_OTHER_ALREADY_IN_GUILD     = 641,      

    ///<summary>
    ///你没有公会，无法邀请对方
    ///</summary>
    ERR_GUILD_CAN_NOT_INVITE             = 642,      

    ///<summary>
    ///对方已离线，无法发送邀请
    ///</summary>
    ERR_GUILD_OTHER_OFFLINE              = 643,      

    ///<summary>
    ///对方未加入公会
    ///</summary>
    ERR_GUILD_OTHER_NOT_IN_GUILD         = 644,      

    ///<summary>
    ///不存在的军衔
    ///</summary>
    ERR_GUILD_NOT_EXIST_MILITARY         = 645,      

    ///<summary>
    ///该军衔已达最高数量限制
    ///</summary>
    ERR_GUILD_MILITARY_LIMIT             = 646,      

    ///<summary>
    ///在小组赛期间，军衔无法修改
    ///</summary>
    ERR_GUILD_MILITARY_BATTLE            = 647,      

    ///<summary>
    ///公会人数已达上限，请升级
    ///</summary>
    ERR_GUILD_MEMBER_FULL_LEVEL_UP       = 648,      

    ///<summary>
    ///公会宣言含有非法词汇
    ///</summary>
    ERR_GUILD_MANIFESTO_HAS_BANNED_WORD  = 649,      

    ///<summary>
    ///不再公会战时间，不能召集
    ///</summary>
    ERR_GUILD_BATTLE_CALL_NOT_IN_STATE   = 650,      


    ///<summary>
    ///--------------------拍卖系统的错误码 651-700------------------------
    ///</summary>
    

    ///<summary>
    ///拍卖价格过低
    ///</summary>
    ERR_AUCTION_MIN_PRICE               = 651,    

    ///<summary>
    ///拍卖价格过高
    ///</summary>
    ERR_AUCTION_MAX_PRICE               = 652,    

    ///<summary>
    ///禁止拍卖
    ///</summary>
    ERR_AUCTION_FORBID_SELL             = 653,    

    ///<summary>
    ///管理器创建失败
    ///</summary>
    ERR_AUCTION_MGR_NOT_EXIST           = 654,    

    ///<summary>
    ///该物品已卖出
    ///</summary>
    ERR_AUCTION_ITEM_SELLED             = 655,    

    ///<summary>
    ///价格不匹配
    ///</summary>
    ERR_AUCTION_PRICE_NOT_MATCH         = 656,    

    ///<summary>
    ///数据库删除错误
    ///</summary>
    ERR_AUCTION_DB_DEL_ERROR            = 657,    

    ///<summary>
    ///没有货币可提取
    ///</summary>
    ERR_AUCTION_NOT_MONEY               = 658,    

    ///<summary>
    ///物品未过期
    ///</summary>
    ERR_AUCTION_ITEM_NOT_TIMEOUT        = 659,    

    ///<summary>
    ///扩展格子数量已达上限
    ///</summary>
    ERR_AUCITON_EX_GRID_TO_LIMIT        = 660,    

    ///<summary>
    ///购买数量已达上限
    ///</summary>
    ERR_AUCTION_BUY_CNT_TO_LIMIT        = 661,    

    ///<summary>
    ///出售数量已达上限
    ///</summary>
    ERR_AUCTION_SELL_CNT_TO_LIMIT       = 662,    

    ///<summary>
    ///物品还在销售队列中
    ///</summary>
    ERR_AUCTION_ITEM_IN_SELL            = 663,    

    ///<summary>
    ///交易市场没有足够该物品
    ///</summary>
    ERR_AUCTION_NOT_ENOUGH_ITEM         = 664,    

    ///<summary>
    ///今日提取货币已达上限
    ///</summary>
    ERR_AUCTION_GET_MONEY_TO_LIMIT      = 665,    

    ///<summary>
    ///提示有物品售出
    ///</summary>
    ERR_AUCTION_TIPMSG_SELLED           = 680,    

    ///<summary>
    ///提示物品即将过期
    ///</summary>
    ERR_AUCTION_TIPMSG_TOBE_TIMEOUT     = 681,    

    ///<summary>
    ///提示物品没有卖出，已下架
    ///</summary>
    ERR_AUCTION_TIPMSG_NOT_SELLED       = 682,    

    ///<summary>
    ///求购物品数量已达上限
    ///</summary>
    ERR_AUCTION_WANT_BUY_TO_LIMIT       = 683,    

    ///<summary>
    ///今日求购次数已达上限
    ///</summary>
    ERR_AUCTION_TODAY_WANT_BUY_TO_LIMIT = 684,    

    ///<summary>
    ///新物品上架公告
    ///</summary>
    ERR_AUCTION_TIPMSG_NEW_ITEM_PUTAWAY = 685,    

    ///<summary>
    ///发送求购公告
    ///</summary>
    ERR_AUCTION_TIPMSG_WANT_BUY         = 686,    

    ///<summary>
    ///通知求购物品上架公告
    ///</summary>
    ERR_AUCTION_TIPMSG_HAS_WANT_BUY     = 687,    
    


    ///<summary>
    ///--------------------附魔系统错误码701~750------------------------
    ///</summary>
    

    ///<summary>
    ///该装备不能附魔
    ///</summary>
    ERR_ENCHANT_CAN_NOT_FUMO                  = 701,    

    ///<summary>
    ///没有附魔信息
    ///</summary>
    ERR_ENCHANT_NO_INFO                       = 702,    

    ///<summary>
    ///保存附魔信息出错
    ///</summary>
    ERR_ENCHANT_SAVE_ERROR                    = 703,    

    ///<summary>
    ///获取装备信息出错
    ///</summary>
    ERR_ENCHANT_EQUIP_ERROR                   = 704,    

    ///<summary>
    ///询问客户端是否替换附魔属性
    ///</summary>
    ERR_ENCHANT_REPLACE                       = 705,    

    ///<summary>
    ///相同属性id下，生成的属性值比原来的低
    ///</summary>
    ERR_ENCHANT_LOWER_ATTRI_VALUE             = 706,    


    ///<summary>
    ///--------------------符文系统错误码750~800------------------------
    ///</summary>
    

    ///<summary>
    ///符文背包已满。没有足够的格子
    ///</summary>
    ERR_RUNE_BAG_FULL                        = 750,     

    ///<summary>
    ///无法放进符文孔位(类型不匹配)
    ///</summary>
    ERR_RUNE_CAN_NOT_PUT_ON                  = 751,     

    ///<summary>
    ///没有符文
    ///</summary>
    ERR_RUNE_NO_RUNE                         = 752,     

    ///<summary>
    ///没有身体符文
    ///</summary>
    ERR_RUNE_NO_BODY_RUNE                    = 753,     

    ///<summary>
    ///免费次数已经用完
    ///</summary>
    ERR_RUNE_NOT_ENOUGH_COUNT                = 754,     

    ///<summary>
    ///还在倒计时中
    ///</summary>
    ERR_RUNE_WISH_CD                         = 755,     

    ///<summary>
    ///该符文被锁定了
    ///</summary>
    ERR_RUNE_LOCK                            = 756,     

    ///<summary>
    ///出售失败
    ///</summary>
    ERR_RUNE_SALE_FAIL                       = 757,     

    ///<summary>
    ///等级不足，无法穿戴
    ///</summary>
    ERR_RUNE_LEVEL_NOT_ENOUGH                = 758,     

    ///<summary>
    ///职业不符合，无法穿戴
    ///</summary>
    ERR_RUNE_VACATION_NOT_ACCORD             = 759,     

    ///<summary>
    ///合成失败，经验符文不能相互吞噬
    ///</summary>
    ERR_RUNE_COMPOSE_FAIL                    = 760,     

    ///<summary>
    ///经验符文不能穿到身上
    ///</summary>
    ERR_RUNE_EXP_NOT_PUT_ON                  = 761,     

    ///<summary>
    ///同一类型符文只能穿一个
    ///</summary>
    ERR_RUNE_IDENTICAL_TYPE                  = 762,     

    ///<summary>
    ///操作失败，符文类型不符合孔位要求
    ///</summary>
    ERR_RUNE_POS_NOT_ACCORD                  = 763,     

    ///<summary>
    ///操作失败，符文类型不符合分页要求
    ///</summary>
    ERR_RUNE_PAGE_NOT_ACCORD                 = 764,     

    ///<summary>
    ///低品质不能吞噬高品质
    ///</summary>
    ERR_RUNE_QUALITY_LOW                     = 765,     

    ///<summary>
    ///符文等级已经达到上限
    ///</summary>
    ERR_RUNE_LEVEL_LIMIT                     = 766,     



    ///<summary>
    ///--------------------组队系统错误码 801~850------------------------
    ///</summary>
    

    ///<summary>
    ///队伍不存在
    ///</summary>
    ERR_TEAM_TEAM_NOT_EXIST                  = 801,     

    ///<summary>
    ///队员不存在
    ///</summary>
    ERR_TEAM_MEMBER_NOT_EXIST                = 802,     

    ///<summary>
    ///被邀请者不存在
    ///</summary>
    ERR_TEAM_INVITEE_NOT_EXIST               = 803,     

    ///<summary>
    ///邀请者不存在
    ///</summary>
    ERR_TEAM_INVITER_NOT_EXIST               = 804,     

    ///<summary>
    ///申请者不存在
    ///</summary>
    ERR_TEAM_APPLYER_NOT_EXIST               = 805,     

    ///<summary>
    ///你不在队伍中
    ///</summary>
    ERR_TEAM_NOT_IN_TEAM                     = 806,     

    ///<summary>
    ///邀请者不在队伍中
    ///</summary>
    ERR_TEAM_INVITER_NOT_IN_TEAM             = 807,     

    ///<summary>
    ///对方不在队伍中
    ///</summary>
    ERR_TEAM_OTHER_NOT_IN_TEAM               = 808,     

    ///<summary>
    ///你已经在队伍中
    ///</summary>
    ERR_TEAM_IN_TEAM                         = 809,     

    ///<summary>
    ///邀请者已经在队伍中
    ///</summary>
    ERR_TEAM_INVITER_IN_TEAM                 = 810,     

    ///<summary>
    ///被邀请者已经在队伍中
    ///</summary>
    ERR_TEAM_INVITEE_IN_TEAM                 = 811,     

    ///<summary>
    ///申请者已经在队伍中
    ///</summary>
    ERR_TEAM_APPLYER_IN_TEAM                 = 812,     

    ///<summary>
    ///你不满足最小组队等级
    ///</summary>
    ERR_TEAM_LIMIT_LEVEL                     = 813,     

    ///<summary>
    ///被邀请者不满足最小组队等级
    ///</summary>
    ERR_TEAM_INVITEE_LIMIT_LEVEL             = 814,     

    ///<summary>
    ///对方不在线
    ///</summary>
    ERR_TEAM_OTHER_NOT_ONLINE                = 815,     

    ///<summary>
    ///邀请者不在线
    ///</summary>
    ERR_TEAM_INVITER_NOT_ONLINE              = 816,     

    ///<summary>
    ///被邀请者不在线
    ///</summary>
    ERR_TEAM_INVITEE_NOT_ONLINE              = 817,     

    ///<summary>
    ///申请者不在线
    ///</summary>
    ERR_TEAM_APPLYER_NOT_ONLINE              = 818,     

    ///<summary>
    ///队伍在不允许加入队伍的状态(在等待其他玩家同意开始玩法状态下)
    ///</summary>
    ERR_TEAM_JOIN_STATE_ERR                  = 819,     

    ///<summary>
    ///队伍在不允许开始玩法的状态(在等待其他玩家同意开始玩法状态下)
    ///</summary>
    ERR_TEAM_START_STATE_ERR                 = 820,     

    ///<summary>
    ///队伍已满(队伍已达到最大队员数)
    ///</summary>
    ERR_TEAM_MAX_MEMBER                      = 821,     

    ///<summary>
    ///你不是队长(队长权限,如换队长)
    ///</summary>
    ERR_TEAM_NOT_CPTAIN                      = 822,     

    ///<summary>
    ///你不在该队伍
    ///</summary>
    ERR_TEAM_NOT_IN_SAME_TEAM                = 823,     

    ///<summary>
    ///邀请者已经换队伍
    ///</summary>
    ERR_TEAM_INVITER_TEAM_CHANGE             = 824,     

    ///<summary>
    ///被邀请者拒绝了你的邀请
    ///</summary>
    ERR_TEAM_INVITEE_REJECT                  = 825,     

    ///<summary>
    ///队长拒绝了你的邀请
    ///</summary>
    ERR_TEAM_LEADER_REJECT                   = 826,     

    ///<summary>
    ///队长拒绝了你的申请
    ///</summary>
    ERR_TEAM_LEADER_APPLY_REJECT             = 827,     

    ///<summary>
    ///权限参数错误
    ///</summary>
    ERR_TEAM_RIGHT_ERR                       = 828,     

    ///<summary>
    ///不能向自己发邀请
    ///</summary>
    ERR_TEAM_INVITE_SELF                     = 829,     

    ///<summary>
    ///不能向自己发申请协议
    ///</summary>
    ERR_TEAM_APPLY_SELF                      = 830,     

    ///<summary>
    ///不能向自己发同意申请协议
    ///</summary>
    ERR_TEAM_APPLY_ACCEPT_SELF               = 831,     

    ///<summary>
    ///不能向自己发剔除协议
    ///</summary>
    ERR_TEAM_KICK_SELF                       = 832,     

    ///<summary>
    ///不能向自己提权协议
    ///</summary>
    ERR_TEAM_GIVE_RIGHT_SELF                 = 833,     

    ///<summary>
    ///不能向自己发换队长协议
    ///</summary>
    ERR_TEAM_CHANGE_CAPTAIN_SELF             = 834,     

    ///<summary>
    ///你不在可组队区域
    ///</summary>
    ERR_TEAM_NOT_IN_MAP                      = 835,     

    ///<summary>
    ///被邀请者不在可组队区域
    ///</summary>
    ERR_TEAM_INVITEE_NOT_IN_MAP              = 836,     

    ///<summary>
    ///邀请者不在可组队区域
    ///</summary>
    ERR_TEAM_INVITER_NOT_IN_MAP              = 837,     

    ///<summary>
    ///申请者不在可组队区域
    ///</summary>
    ERR_TEAM_APPLYER_NOT_IN_MAP              = 838,     

    ///<summary>
    ///对方不在可组队区域
    ///</summary>
    ERR_TEAM_OTHER_NOT_IN_MAP                = 839,     

    ///<summary>
    ///对方不是队长
    ///</summary>
    ERR_TEAM_OTHER_NOT_CPTAIN                = 840,     

    ///<summary>
    ///对方拒绝了你的召唤
    ///</summary>
    ERR_TEAM_CALL_REJECT                     = 841,     

    ///<summary>
    ///你不在组队可召唤传送区域
    ///</summary>
    ERR_TEAM_NOT_IN_CALL_MAP                 = 842,     

    ///<summary>
    ///对方不在组队可召唤传送区域
    ///</summary>
    ERR_TEAM_OTHER_NOT_IN_CALL_MAP           = 843,     

    ///<summary>
    ///队伍人数不够
    ///</summary>
    ERR_TEAM_COUNT_NOT_ENOUGH                = 844,     

    ///<summary>
    ///队伍人数超过上限
    ///</summary>
    ERR_TEAM_COUNT_OVER_LIMIT                = 845,     

    ///<summary>
    ///xx和队长不在同一公会中
    ///</summary>
    ERR_TEAM_LIMIT_SAME_GUILD                = 846,     

    ///<summary>
    ///队伍需要都是女性
    ///</summary>
    ERR_TEAM_LIMIT_FEMALE                    = 847,     

    ///<summary>
    ///队伍需要都是男性
    ///</summary>
    ERR_TEAM_LIMIT_MALE                      = 848,     

    ///<summary>
    ///队伍需要是一男一女的情侣
    ///</summary>
    ERR_TEAM_LIMIT_VALENTINE                 = 849,     

    ///<summary>
    ///队伍等级差过大
    ///</summary>
    ERR_TEAM_LIMIT_LEVEL_DIFF                = 850,     



    ///<summary>
    ///--------------------强化系统错误码 851~900------------------------
    ///</summary>
    

    ///<summary>
    ///装备强化上限
    ///</summary>
    ERR_STRENGTHEN_LIMIT                     = 851,     

    ///<summary>
    ///幸运石插槽不足
    ///</summary>
    ERR_STRENGTHEN_SLOT                      = 852,     

    ///<summary>
    ///缺少幸运石
    ///</summary>
    ERR_STRENGTHEN_LACK_OF_STONES            = 853,     

    ///<summary>
    ///缺少材料
    ///</summary>
    ERR_STRENGTHEN_LACK_OF_MATERIAL          = 854,     

    ///<summary>
    ///缺少材料，但可以用高级材料替换
    ///</summary>
    ERR_STRENGTHEN_CAN_REPLACE               = 855,     

    ///<summary>
    ///强化失败了
    ///</summary>
    ERR_STRENGTHEN_FAIL                      = 856,     

    ///<summary>
    ///没有下级的强化
    ///</summary>
    ERR_STRENGTHEN_NEXT_LEVEL_NOT_EXIST      = 857,     

    ///<summary>
    ///不能使用套装卷轴
    ///</summary>
    ERR_SUIT_SCROLL_CANT_USE                 = 861,     


    ///<summary>
    ///--------------------翅膀系统错误码 901~950------------------------
    ///</summary>
    

    ///<summary>
    ///翅膀不存在
    ///</summary>
    ERR_WING_NOT_EXIST                           = 901,      

    ///<summary>
    ///翅膀不能培养
    ///</summary>
    ERR_WING_NOT_TRAIN                           = 902,      

    ///<summary>
    ///vip等级不足
    ///</summary>
    ERR_WING_VIP_LEVEL_NOT_ENOUGH                = 903,      

    ///<summary>
    ///角色等级不足
    ///</summary>
    ERR_WING_LEVEL_NOT_ENOUGH                    = 904,      

    ///<summary>
    ///培养的等级达到上限
    ///</summary>
    ERR_WING_TRAIN_LEVEL_LIMIT                   = 905,      

    ///<summary>
    ///已经有该翅膀了
    ///</summary>
    ERR_WING_ALREADY_WING                        = 906,      



    ///<summary>
    ///--------------------制造系统 951~1000------------------------
    ///</summary>
    

    ///<summary>
    ///等级不足
    ///</summary>
    ERR_MFG_NOT_ENOUGH_LEVEL                      = 951,      

    ///<summary>
    ///没有足够的消耗道具
    ///</summary>
    ERR_MFG_NOT_ENOUGH_ITEMS                      = 952,      

    ///<summary>
    ///已经满级
    ///</summary>
    ERR_MFG_MAX_MFG_SKILL_LEVEL                   = 953,      

    ///<summary>
    ///配置错误
    ///</summary>
    ERR_MFG_CONFIG_ERR                            = 954,      

    ///<summary>
    ///还没有学习获得该稀有配方
    ///</summary>
    ERR_MFG_NOT_HAVE_FORMULA                      = 955,      

    ///<summary>
    ///已经学习获得该稀有配方
    ///</summary>
    ERR_MFG_HAVE_FORMULA                          = 956,      

    ///<summary>
    ///不是稀有配方
    ///</summary>
    ERR_MFG_NOT_RARE_FORMULA                      = 957,      

    ///<summary>
    ///没有足够的制造等级
    ///</summary>
    ERR_MFG_NOT_ENOUGH_MFG_SKILL_LEVEL            = 958,      

    ///<summary>
    ///制造cd时间
    ///</summary>
    ERR_MFG_PRODUCT_CD                            = 959,      

    ///<summary>
    ///天赋id错误
    ///</summary>
    ERR_MFG_TALENT_ID_ERROR                       = 960,      

    ///<summary>
    ///已经学会了这个天赋
    ///</summary>
    ERR_MFG_HAD_TALENT                            = 961,      

    ///<summary>
    ///需要学习x天赋
    ///</summary>
    ERR_MFG_NEED_TALENT                           = 962,      

    ///<summary>
    ///玩家需要达到x等级后
    ///</summary>
    ERR_MFG_NOT_ENOUGH_AVATAR_LEVEL               = 963,      


    ///<summary>
    ///-----执行数据库操作相关的错误码1050-1099------------
    ///</summary>
    

    ///<summary>
    ///sql执行失败
    ///</summary>
    ERR_EXCUTE_SQL_FAIL         = 1050,      

    ///<summary>
    ///非正数
    ///</summary>
    ERR_NOT_POSITIVE_NUM        = 1051,      

    ///<summary>
    ///写数据库失败
    ///</summary>
    ERR_WRITE_TO_DB             = 1052,      


    ///<summary>
    ///-----------------------------通用错误码1100-1150-------------------------------------
    ///</summary>
    

    ///<summary>
    ///没有这个函数名
    ///</summary>
    ERR_NO_THIS_FUNC_NAME       = 1100,      

    ///<summary>
    ///参数类型错误
    ///</summary>
    ERR_PARAM                   = 1101,      

    ///<summary>
    ///变量为空
    ///</summary>
    ERR_VAR_IS_NIL              = 1102,      

    ///<summary>
    ///表格配置错误
    ///</summary>
    ERR_TABLE_CFG               = 1103,      

    ///<summary>
    ///玩家vip等级不够
    ///</summary>
    ERR_VIP_LEVEL_NOT_ENOUGH    = 1104,      

    ///<summary>
    ///屏蔽显示
    ///</summary>
    ERR_SHILED_SHOW_VIP_ID      = 1105,      


    ///<summary>
    ///-----------------------------好友系统相当1151-1200-------------------------------------
    ///</summary>
    

    ///<summary>
    ///对象不存在
    ///</summary>
    ERR_FRIEND_NO_THIS_PLAYER                     = 1151,     

    ///<summary>
    ///好友里没有这个玩家
    ///</summary>
    ERR_FRIEND_NO_THIS_FRIEND                     = 1152,     

    ///<summary>
    ///已经是你的朋友了
    ///</summary>
    ERR_FRIEND_ALREADY                            = 1153,     

    ///<summary>
    ///好友满了
    ///</summary>
    ERR_FRIEND_FULL                               = 1154,     

    ///<summary>
    ///玩家不在线
    ///</summary>
    ERR_FRIEND_ONLINE                             = 1155,     

    ///<summary>
    ///已经发送过体力祝福了
    ///</summary>
    ERR_FRIEND_ALREADY_SEND                       = 1156,     

    ///<summary>
    ///领取体力祝福超过上限
    ///</summary>
    ERR_FRIEND_RECV_UPPER_LIMIT                   = 1157,     

    ///<summary>
    ///亲密值道具赠送次数达到上限
    ///</summary>
    ERR_FRIEND_ITEM_GIVE_LIMIT                    = 1158,     

    ///<summary>
    ///已经领取体力
    ///</summary>
    ERR_FRIEND_BLESSED                            = 1159,     

    ///<summary>
    ///对应的道具赠送次数达到上限
    ///</summary>
    ERR_FRIEND_GIVE_GOODS_LIMIT                   = 1160,     

    ///<summary>
    ///对方好友已达上限
    ///</summary>
    ERR_FRIEND_OTHER_FULL                         = 1161,     

    ///<summary>
    ///今日申请好友已达上限
    ///</summary>
    ERR_FRIEND_ADD_COUNT_LIMIT                    = 1162,     

    ///<summary>
    ///今天发送次数已达上限
    ///</summary>
    ERR_FRIEND_SEND_UPPER_LIMIT                   = 1163,     

    ///<summary>
    ///----------------------------星魂系统 1201~1250--------------------------------------------
    ///</summary>
    

    ///<summary>
    ///前一个星没有点亮
    ///</summary>
    ERR_STAR_SPIRIT_PRE_NOT_LIGHT                 = 1201,     

    ///<summary>
    ///星魂不够
    ///</summary>
    ERR_STAR_SPIRIT_NOT_ENOUGH_COST               = 1202,     

    ///<summary>
    ///已经点亮
    ///</summary>
    ERR_STAR_SPIRIT_ALREADY_LIGHT                 = 1203,     


    ///<summary>
    ///----------------------------代币商店1251~1300---------------------------------------------
    ///</summary>
    

    ///<summary>
    ///每日次数已经用完
    ///</summary>
    ERR_TOKEN_SHOP_DAILY_LIMIT                    = 1251,     

    ///<summary>
    ///代币消耗不足
    ///</summary>
    ERR_TOKEN_SHOP_TOKEN_IS_NOT_ENOUGH            = 1252,     

    ///<summary>
    ///总量限购，总数已经被卖光了
    ///</summary>
    ERR_TOKEN_SHOP_TOTAL_CNT_LIMIT                = 1253,     


    ///<summary>
    ///------------------------------幻境探索系统1301~1350 ------------------------------------
    ///</summary>
    

    ///<summary>
    ///护送已经完成
    ///</summary>
    ERR_DREAMLAND_CONVOY_ALREADY_FINISH           = 1301,     

    ///<summary>
    ///正在护送中
    ///</summary>
    ERR_DREAMLAND_CONVOYING                       = 1302,     

    ///<summary>
    ///已经是最高等级了
    ///</summary>
    ERR_DREAMLAND_ALREADY_HIGHEST_QUALITY         = 1303,     

    ///<summary>
    ///袭击CD不需要清除
    ///</summary>
    ERR_DREAMLAND_NO_NEED_CLEAN                   = 1304,     

    ///<summary>
    ///没有护送
    ///</summary>
    ERR_DREAMLAND_NO_CONVOY                       = 1305,     

    ///<summary>
    ///抢劫次数达到上限
    ///</summary>
    ERR_DREAMLAND_ATK_COUNT_LIMIT                 = 1306,     

    ///<summary>
    ///抢劫cd中
    ///</summary>
    ERR_DREAMLAND_ATK_CDING                       = 1307,     

    ///<summary>
    ///对方不在我可见范围内
    ///</summary>
    ERR_DREAMLAND_ATK_NO_IN_VISIBLE               = 1308,     

    ///<summary>
    ///对方护送已经完成
    ///</summary>
    ERR_DREAMLAND_OTHER_CONVOY_ALREADY_FINISH     = 1309,     

    ///<summary>
    ///对方没有护送
    ///</summary>
    ERR_DREAMLAND_OTHER_NO_CONVOY                 = 1310,     

    ///<summary>
    ///复仇次数达到上限
    ///</summary>
    ERR_DREAMLAND_REVENGE_COUNT_LIMIT             = 1311,     

    ///<summary>
    ///对方被抢次数达到上限
    ///</summary>
    ERR_DREAMLAND_OTHER_CONVOY_ATK_COUNT_LIMIT    = 1312,     

    ///<summary>
    ///对方没抢劫过我
    ///</summary>
    ERR_DREAMLAND_OTHER_NO_LOOT                   = 1313,     

    ///<summary>
    ///购买抢劫次数达到上限
    ///</summary>
    ERR_DREAMLAND_BUY_COUNT_LIMIT                 = 1314,     



    ///<summary>
    ///------------------------------时装系统1351~1400 ------------------------------------
    ///</summary>
    

    ///<summary>
    ///没有这个时装
    ///</summary>
    ERR_FASHION_NOFASHION                         = 1351,     

    ///<summary>
    ///职业不匹配(这个职业穿不了)
    ///</summary>
    ERR_FASHION_VOCATION_MISMATCH                 = 1352,     

    ///<summary>
    ///已经有了
    ///</summary>
    ERR_FASHION_ALREADY_OWN                       = 1353,     

    ///<summary>
    ///时间错误
    ///</summary>
    ERR_FASHION_INVALID_TIME                      = 1354,     



    ///<summary>
    ///------------------------------商城系统1401~1450 ------------------------------------
    ///</summary>
    

    ///<summary>
    ///商城版本数据
    ///</summary>
    ERR_MARKET_VERSION_ERR                       = 1401,     

    ///<summary>
    ///没有这个商城数据
    ///</summary>
    ERR_MARKET_NO_DATA                           = 1402,     

    ///<summary>
    ///商城数据配置错误
    ///</summary>
    ERR_MARKET_DATA_ERR                          = 1403,     

    ///<summary>
    ///客户端发送与商城数据不匹配
    ///</summary>
    ERR_MARKET_DATA_NOT_MATCH                    = 1404,     

    ///<summary>
    ///该商品仅用于购买，不可赠送
    ///</summary>
    ERR_MARKET_ONLY_BUY                          = 1405,     

    ///<summary>
    ///该商品仅用于赠送，不可购买给自己
    ///</summary>
    ERR_MARKET_ONLY_GIVE                         = 1406,     

    ///<summary>
    ///已达到个人每日限购
    ///</summary>
    ERR_MARKET_BUY_DAILY_CNT_LIMIT               = 1407,     

    ///<summary>
    ///已达到个人总限购
    ///</summary>
    ERR_MARKET_BUY_TOTAL_CNT_LIMIT               = 1408,     

    ///<summary>
    ///个人每日限购,不够剩余数量
    ///</summary>
    ERR_MARKET_BUY_DAILY_CNT_LIMIT_1             = 1409,     

    ///<summary>
    ///个人总限购,不够剩余数量
    ///</summary>
    ERR_MARKET_BUY_TOTAL_CNT_LIMIT_1             = 1410,     

    ///<summary>
    ///对方不是你的好友
    ///</summary>
    ERR_MARKET_GIVE_NOT_FRIEND                   = 1411,     

    ///<summary>
    ///你没有该优惠劵
    ///</summary>
    ERR_MARKET_HAS_NOT_COUPON                    = 1412,     

    ///<summary>
    ///无法使用该优惠劵,与购买物品不匹配
    ///</summary>
    ERR_MARKET_COUPON_NOT_USE_PAGE               = 1413,     

    ///<summary>
    ///达不到优惠劵价格限制
    ///</summary>
    ERR_MARKET_COUPON_PRICE_LIMIT                = 1414,     

    ///<summary>
    ///该优惠劵已达每日使用限制次数
    ///</summary>
    ERR_MARKET_COUPON_DAILY_CNT_LIMIT            = 1415,     

    ///<summary>
    ///限时购买，不在购买时间内
    ///</summary>
    ERR_MARKET_TIME_LIMIT                        = 1416,     

    ///<summary>
    ///vip专属购买，且需要大于配置的vip等级
    ///</summary>
    ERR_MARKET_VIP_LIMIT                         = 1417,     

    ///<summary>
    ///总量限购，只允许购买一个
    ///</summary>
    ERR_MARKET_ONE_CNT_LIMIT                     = 1418,     

    ///<summary>
    ///总量限购，总数已经被卖光了
    ///</summary>
    ERR_MARKET_TOTAL_CNT_LIMIT                   = 1419,     

    ///<summary>
    ///该商品已经下架
    ///</summary>
    ERR_MARKET_IS_OFF                            = 1420,     

    ///<summary>
    ///赠送,好友度限制
    ///</summary>
    ERR_MARKET_FRIEND_DEGREE_LIMIT               = 1421,     

    ///<summary>
    ///赠送,每日最高价格限制
    ///</summary>
    ERR_MARKET_DAILY_GIVE_PRICE_LIMIT            = 1422,     

    ///<summary>
    ///赠送,赠送前，自己总消耗金额限制
    ///</summary>
    ERR_MARKET_TOTAL_CONSUME_PRICE_LIMIT         = 1423,     


    ///<summary>
    ///-----------------------------VIP系统1451~1500-----------------------------------------------
    ///</summary>
    

    ///<summary>
    ///vip奖励已经领取
    ///</summary>
    ERR_VIP_REWARDS_HAS_GET                      = 1451,     

    ///<summary>
    ///没有这个vip奖励
    ///</summary>
    ERR_VIP_NO_REWARDS                           = 1452,     

    ///<summary>
    ///vip等级达到上限
    ///</summary>
    ERR_VIP_MAX_LEVEL                            = 1453,     

    ///<summary>
    ///购买的等级比当前vip等级低
    ///</summary>
    ERR_VIP_LOW_LEVEL                            = 1454,     

    ///<summary>
    ///错误的等级参数
    ///</summary>
    ERR_VIP_WRONG_LEVEL                          = 1455,     

    ///<summary>
    ///购买的等级和当前vip等级
    ///</summary>
    ERR_VIP_SAME_LEVEL                           = 1456,     


    ///<summary>
    ///-----------------------------排行榜系统1501~1550-----------------------------------------------
    ///</summary>
    

    ///<summary>
    ///没有足够的崇拜次数
    ///</summary>
    ERR_RANK_NOT_ENOUGH_WORSHIP_TIME             = 1501,     

    ///<summary>
    ///已经领取奖励了
    ///</summary>
    ERR_RANK_ALREADY_GET_REWARD                  = 1502,     

    ///<summary>
    ///今天已经崇拜过他
    ///</summary>
    ERR_RANK_TODAY_ALREADY_WIRSHIP               = 1503,     


    ///<summary>
    ///-----------------------------宠物系统1551~1600-----------------------------------------------
    ///</summary>
    

    ///<summary>
    ///宠物数据还没有准备好
    ///</summary>
    ERR_PET_NOT_LOAD_ALL_PET                     = 1551,     

    ///<summary>
    ///已经有这个宠物了
    ///</summary>
    ERR_PET_HAS_PET                              = 1552,     

    ///<summary>
    ///还没有获得这个宠物了
    ///</summary>
    ERR_PET_NOT_HAS_PET                          = 1553,     

    ///<summary>
    ///没有足够的宠物召唤道具
    ///</summary>
    ERR_PET_NOT_ENOUGH_CALL_ITEMS                = 1554,     

    ///<summary>
    ///没有足够的宠物经验丹道具
    ///</summary>
    ERR_PET_NOT_ENOUGH_EXP_ITEMS                 = 1555,     

    ///<summary>
    ///宠物经验丹道具经验配置错误
    ///</summary>
    ERR_PET_ITEM_EXP_ERROR                       = 1556,     

    ///<summary>
    ///该宠物已经满级
    ///</summary>
    ERR_PET_MAX_LEVEL                            = 1557,     

    ///<summary>
    ///宠物品质对等级的上限限制(升级的时候)
    ///</summary>
    ERR_PET_QUALITY_LEVEL_LIMIT                  = 1558,     

    ///<summary>
    ///没有足够的宠物升星道具
    ///</summary>
    ERR_PET_NOT_ENOUGH_STAR_ITEMS                = 1559,     

    ///<summary>
    ///没有足够的宠物升品质道具
    ///</summary>
    ERR_PET_NOT_ENOUGH_QUALITY_ITEMS             = 1560,     

    ///<summary>
    ///该宠物已经满星
    ///</summary>
    ERR_PET_MAX_STAR                             = 1561,     

    ///<summary>
    ///该宠物已经满品质
    ///</summary>
    ERR_PET_MAX_QUALITY                          = 1562,     

    ///<summary>
    ///不在待命状态
    ///</summary>
    ERR_PET_NOT_IN_IDLE                          = 1563,     

    ///<summary>
    ///不在战斗状态
    ///</summary>
    ERR_PET_NOT_IN_COMBAT                        = 1564,     

    ///<summary>
    ///不在助战状态
    ///</summary>
    ERR_PET_NOT_IN_ASSIST                        = 1565,     

    ///<summary>
    ///在待命状态
    ///</summary>
    ERR_PET_IN_IDLE                              = 1566,     

    ///<summary>
    ///在战斗状态
    ///</summary>
    ERR_PET_IN_COMBAT                            = 1567,     

    ///<summary>
    ///在助战状态
    ///</summary>
    ERR_PET_IN_ASSIST                            = 1568,     

    ///<summary>
    ///该位置已经有在战斗状态的宠物
    ///</summary>
    ERR_PET_HAS_IN_COMBAT                        = 1569,     

    ///<summary>
    ///该位置已经有在助战状态的宠物
    ///</summary>
    ERR_PET_HAS_IN_ASSIST                        = 1570,     

    ///<summary>
    ///该助战位置还没有开启
    ///</summary>
    ERR_PET_HOLE_NOT_OPEN                        = 1571,     

    ///<summary>
    ///已经激活出战该宠物
    ///</summary>
    ERR_PET_HAS_IN_ACTIVE                        = 1572,     

    ///<summary>
    ///不在激活出战状态
    ///</summary>
    ERR_PET_HAS_NOT_IN_ACTIVE                    = 1573,     

    ///<summary>
    ///不在待激活状态
    ///</summary>
    ERR_PET_NOT_IN_IDLE_ACTIVE                   = 1574,     

    ///<summary>
    ///已达到最大出战宠物数
    ///</summary>
    ERR_PET_MAX_COMBAT_PET                       = 1575,     

    ///<summary>
    ///该出战位置还没有开启
    ///</summary>
    ERR_PET_COMBAT_HOLE_NOT_OPEN                 = 1576,     

    ///<summary>
    ///该位置没有在战斗状态的宠物,故无法替换
    ///</summary>
    ERR_PET_HAS_NOT_IN_COMBAT                    = 1577,     

    ///<summary>
    ///宠物需要满星,才能洗练
    ///</summary>
    ERR_PET_NEED_MAX_STAR                        = 1578,     

    ///<summary>
    ///宠物品质对等级的上限限制(突破品质的时候)
    ///</summary>
    ERR_PET_QUALITY_LEVEL_LIMIT_1                = 1579,     

    ///<summary>
    ///宠物配置有误
    ///</summary>
    ERR_PET_CFG_ERROR                            = 1580,     

    ///<summary>
    ///宠物等级不能超过玩家等级。
    ///</summary>
    ERR_PET_AVATAR_LEVEL_LIMIT                   = 1581,     

    ///<summary>
    ///没有死亡的宠物
    ///</summary>
    ERR_PET_NO_DEAD_PETS                         = 1582,     

    ///<summary>
    ///不在死亡状态
    ///</summary>
    ERR_PET_NOT_IN_DEAD                          = 1583,     

    ///<summary>
    ///该地图宠物不可复活
    ///</summary>
    ERR_PET_NOT_REVIVE_MAP                       = 1584,     

    ///<summary>
    ///该宠物的洗练等级已经满级
    ///</summary>
    ERR_PET_MAX_REFRESH_LEVEL                    = 1585,     

    ///<summary>
    ///该宠物的洗练配置错误
    ///</summary>
    ERR_PET_REFRESH_CFG_ERROR                    = 1586,     

    ///<summary>
    ///宠物还没有洗练过
    ///</summary>
    ERR_PET_NOT_REFRESH                          = 1587,     

    ///<summary>
    ///宠物品质不符合洗练开启要求
    ///</summary>
    ERR_PET_NEED_QUALITY                         = 1588,     

    ///<summary>
    ///玩家vip不符合洗练开启要求
    ///</summary>
    ERR_PET_NEED_VIP                             = 1589,     

    ///<summary>
    ///宠物star不符合洗练开启要求
    ///</summary>
    ERR_PET_NEED_STAR                            = 1590,     


    ///<summary>
    ///-----------------------------活动相关的错误码1601-1650-------------------------------------
    ///</summary>
    

    ///<summary>
    ///活动当前时间未开放
    ///</summary>
    ERR_ACTIVITY_NOT_OPEN   = 1601,    


    ///<summary>
    ///-----------------------------技能子系统1651-1700-----------------------------------------
    ///</summary>
    

    ///<summary>
    ///技能id错误
    ///</summary>
    ERR_SPELL_ID                                 = 1651,     

    ///<summary>
    ///该技能已经学习了
    ///</summary>
    ERR_SPELL_HAS_LEARNED                        = 1652,     

    ///<summary>
    ///技能学习消耗不足
    ///</summary>
    ERR_SPELL_COST_NOT_ENOUGH                    = 1653,     

    ///<summary>
    ///技能还未学习
    ///</summary>
    ERR_SPELL_HAS_NOT_LEARNED                    = 1654,     

    ///<summary>
    ///技能已达最高等级
    ///</summary>
    ERR_SPELL_LEVEL_IS_HIGHEST                   = 1655,     

    ///<summary>
    ///技能专精等级不够
    ///</summary>
    ERR_SPELL_PROFICIENT_LEVEL_NOT_ENOUGH        = 1656,     

    ///<summary>
    ///已有该专精
    ///</summary>
    ERR_SPELL_PROFICIENT_IS_SAME                 = 1657,     

    ///<summary>
    ///技能不能装备
    ///</summary>
    ERR_SPELL_CAN_EQUIP                          = 1658,     

    ///<summary>
    ///技能槽不匹配
    ///</summary>
    ERR_SPELL_SLOT_NOT_MATCH                     = 1659,     

    ///<summary>
    ///已经装备该技能
    ///</summary>
    ERR_SPELL_HAS_EQUIP                          = 1660,     

    ///<summary>
    ///不能学习
    ///</summary>
    ERR_SPELL_CAN_LEARN                          = 1661,     

    ///<summary>
    ///对方是友方，请选择敌方作为目标
    ///</summary>
    ERR_SPELL_TARGET_IS_FRIEND                   = 1662,     

    ///<summary>
    ///对方已死亡，请选择其他目标
    ///</summary>
    ERR_SPELL_TARGET_IS_DEATH                    = 1663,     

    ///<summary>
    ///对方已不在可视范围，请选择其他目标
    ///</summary>
    ERR_SPELL_TARGET_IS_OUT                      = 1664,     

    ///<summary>
    ///等级不足，不能选专精
    ///</summary>
    ERR_SPELL_LEVEL_NOT_ENOUGH                   = 1665,     

    ///<summary>
    ///    ERR_SPELL_PROFICIENT_CAN_NOT_CHANGE          = 1666,     --该地图不能切换专精
    ///</summary>


    ///<summary>
    ///    ERR_SPELL_PROFICIENT_NOT_ENOUGH              = 1667,     --切换专精的消耗不足
    ///</summary>



    ///<summary>
    ///-----------------------------1701-1750 副本玩法:恶魔之门-----------------------------------------
    ///</summary>
    

    ///<summary>
    ///不在开放时间内
    ///</summary>
    ERR_EVIL_NOT_OPEN                            = 1701,     

    ///<summary>
    ///你在队伍中,你不是队长,队长才可以点击挑战   --不用 --因为修改成单人模式
    ///</summary>
    ERR_EVIL_NOT_TEAM_LEADER                     = 1702,     

    ///<summary>
    ///已经参加过了                                                                         --不用 --因为修改成单人模式
    ///</summary>
    ERR_EVIL_HAD_JOIN                            = 1703,     

    ///<summary>
    ///世界等级限制
    ///</summary>
    ERR_EVIL_WORLD_LEVEL_LIMIT                   = 1704,     

    ///<summary>
    ///副本id配置错误。恶魔之门世界等级匹配的副本id配置错误
    ///</summary>
    ERR_EVIL_INST_ID_ERROR                       = 1705,     

    ///<summary>
    ///恶魔之门配置错误
    ///</summary>
    ERR_EVIL_CFG_ERROR                           = 1706,     

    ///<summary>
    ///自己所在场景不能传送
    ///</summary>
    ERR_EVIL_SHARE_SEND_MAP_ERR                  = 1707,     


    ///<summary>
    ///-----------------------------要塞 1751~1800-----------------------------------
    ///</summary>
    

    ///<summary>
    ///找不到base或building
    ///</summary>
    ERR_FORTRESS_CANT_FIND                       = 1751,     

    ///<summary>
    ///这个建筑不能升级了
    ///</summary>
    ERR_FORTRESS_BUILDING_CANT_UPGRADE           = 1752,     

    ///<summary>
    ///已经有相同或更高等级的建筑了。
    ///</summary>
    ERR_FORTRESS_BUILDING_CONFLICT               = 1753,     

    ///<summary>
    ///等级不足
    ///</summary>
    ERR_FORTRESS_INSUFFICIENT_LEVEL              = 1754,     

    ///<summary>
    ///材料不足
    ///</summary>
    ERR_FORTRESS_INSUFFICIENT_MAT                = 1755,     

    ///<summary>
    ///找不到旧建筑
    ///</summary>
    ERR_FORTRESS_CANT_FIND_OLD_BUILDING          = 1756,     

    ///<summary>
    ///找不到配置
    ///</summary>
    ERR_FORTRESS_CANT_FIND_CONFIG                = 1757,     


    ///<summary>
    ///找不到配置
    ///</summary>
    ERR_TIME_ALTAR_CANT_FIND_CONFIG              = 1761,     

    ///<summary>
    ///次数不足
    ///</summary>
    ERR_TIME_ALTAR_COUNT_NOT_ENOUGH              = 1762,     


    ///<summary>
    ///-----------------------------1801~1850奖励系统------------------------------------------------
    ///</summary>
    

    ///<summary>
    ///宝箱vip奖励已经领取
    ///</summary>
    ERR_AWARD_BOX_VIP_REWARD_HAS_GET              = 1801,     

    ///<summary>
    ///没有该奖励，奖励id错误
    ///</summary>
    ERR_AWARD_ID_ERROR                            = 1802,     

    ///<summary>
    ///每日任务奖励领取时间不对
    ///</summary>
    ERR_AWARD_DAILY_TASK_REWARD_TIME_ERR          = 1803,     

    ///<summary>
    ///本日已经签到
    ///</summary>
    ERR_AWARD_HAS_SIGN_IN                         = 1804,     

    ///<summary>
    ///本日签到奖励已经领取
    ///</summary>
    ERR_AWARD_HAS_GET_SIGN_IN_REWARD              = 1805,     

    ///<summary>
    ///本日签到VIP奖励已经领取
    ///</summary>
    ERR_AWARD_HAS_GET_SIGN_IN_VIP_REWARD          = 1806,     

    ///<summary>
    ///没有VIP奖励
    ///</summary>
    ERR_AWARD_NO_SIGN_IN_VIP_REWARD               = 1807,     

    ///<summary>
    ///签到天数不足
    ///</summary>
    ERR_AWARD_SIGN_IN_NOT_ENOUGH                  = 1808,     

    ///<summary>
    ///宝箱奖励已经领取
    ///</summary>
    ERR_AWARD_HAS_GET_BOX_REWARD                  = 1809,     

    ///<summary>
    ///活跃值不足
    ///</summary>
    ERR_AWARD_ACTIVE_POINT_NOT_ENOUGH             = 1810,     

    ///<summary>
    ///任务未完成
    ///</summary>
    ERR_AWARD_ACTIVE_TASK_NOT_COMPLETE            = 1811,     

    ///<summary>
    ///每日奖励已经领取
    ///</summary>
    ERR_AWARD_HAS_GET_DAILY_REWARD                = 1812,     

    ///<summary>
    ///道具消耗不足
    ///</summary>
    ERR_AWARD_ITEM_NOT_ENOUGH                     = 1813,     

    ///<summary>
    ///-----------------------------1851~1900怪物身体部位系统------------------------------------------------
    ///</summary>
    

    ///<summary>
    ///没找到改部位
    ///</summary>
    ERR_BODY_PART_NO_PART              = 1851,     

    ///<summary>
    ///部位锁着
    ///</summary>
    ERR_BODY_PART_NO_LOCK              = 1852,     

    ///<summary>
    ///部位已经被破坏
    ///</summary>
    ERR_BODY_PART_NO_DESTROY           = 1853,     


    ///<summary>
    ///-----------------------------1901~1950好友邀请系统-------------------------------------------------
    ///</summary>
    

    ///<summary>
    ///邀请码不存在
    ///</summary>
    ERR_FRIEND_INVITE_NUM_NOT_EXIST              = 1901,    

    ///<summary>
    ///邀请码已经存在
    ///</summary>
    ERR_FRIEND_INVITE_NUM_EXIST                  = 1902,    

    ///<summary>
    ///邀请码错误
    ///</summary>
    ERR_FRIEND_INVITE_CODE_ERR                   = 1903,    

    ///<summary>
    ///邀请码使用次数过多，该邀请码无法使用
    ///</summary>
    ERR_FRIEND_INVITE_CODE_USE_TIMES_TOO_MUCH    = 1904,    

    ///<summary>
    ///没有生成邀请码
    ///</summary>
    ERR_FRIEND_INVITE_NO_INVITE_NUM              = 1905,    

    ///<summary>
    ///奖励id错误
    ///</summary>
    ERR_FRIEND_INVITE_ID_ERROR                   = 1906,    

    ///<summary>
    ///任务未完成
    ///</summary>
    ERR_FRIEND_INVITE_TASK_NOT_COMPLETE          = 1907,    

    ///<summary>
    ///奖励达到领取上限
    ///</summary>
    ERR_FRIEND_INVITE_REWARD_LIMIT               = 1908,    

    ///<summary>
    ///已有邀请码
    ///</summary>
    ERR_FRIEND_INVITE_HAS_INVITE_NUM             = 1909,    

    ///<summary>
    ///已经使用过邀请码注册该角色
    ///</summary>
    ERR_FRIEND_INVITE_HAS_USED_INVITE_NUM        = 1910,    

    ///<summary>
    ///生成邀请码错误
    ///</summary>
    ERR_FRIEND_INVITE_GENERATE_INVITE_NUM        = 1911,    

    ///<summary>
    ///邀请码未生成
    ///</summary>
    ERR_FRIEND_INVITE_NUM_NOT_GENERATE           = 1912,    

    ///<summary>
    ///使用的邀请码有误
    ///</summary>
    ERR_FRIEND_INVITE_HAS_ERROR_INVITE_NUM       = 1913,    


    ///<summary>
    ///-----------------------------1951-2000 副本玩法:组队副本-----------------------------------------
    ///</summary>
    

    ///<summary>
    ///不是队长，无法开始玩法
    ///</summary>
    ERR_TEAM_MISSION_NOT_TEAM_LEADER            = 1951,  



    ///<summary>
    ///-----------------------------2001-2050 副本玩法:喊人组队-----------------------------------------
    ///</summary>
    

    ///<summary>
    ///喊人失效
    ///</summary>
    ERR_TEAM_CALL_LOSE                            = 2001,    


    ///<summary>
    ///-----------------------------2051-2100 体力管理----------------------------------------
    ///</summary>
    

    ///<summary>
    ///体力购买次数已达上限
    ///</summary>
    ERR_ENERGY_BUY_CNT_TO_LIMIT                   = 2051,    

    ///<summary>
    ///体力数量达到上限
    ///</summary>
    ERR_ENERGY_NUM_TO_LIMIT                       = 2052,    


    ///<summary>
    ///-----------------------------2101-2150 单人副本玩法----------------------------------------
    ///</summary>
    

    ///<summary>
    ///首次挑战未通关
    ///</summary>
    ERR_SINGLE_MISSION_FIRST_NOT_WIN              = 2101,    

    ///<summary>
    ///购买次数达到上限
    ///</summary>
    ERR_SINGLE_MISSION_BUY_CNT_TO_LIMIT           = 2102,    


    ///<summary>
    ///-----------------------------2151-2200 试炼秘境----------------------------------------
    ///</summary>
    

    ///<summary>
    ///该副本未通关
    ///</summary>
    ERR_TRY_MISSION_NOT_WIN                       = 2151,    

    ///<summary>
    ///没有三星，不能领取三星奖励
    ///</summary>
    ERR_TRY_MISSION_NOT_THREE_STARS               = 2152,    

    ///<summary>
    ///重置次数不足
    ///</summary>
    ERR_TRY_MISSION_NOT_ENOUGH_REPLACE            = 2153,    

    ///<summary>
    ///重置消耗不足
    ///</summary>
    ERR_TRY_MISSION_NOT_REPLACE_TIMES             = 2154,    

    ///<summary>
    ///没有足够的星数
    ///</summary>
    ERR_TRY_MISSION_NOT_ENOUGH_STARS              = 2155,    

    ///<summary>
    ///奖励已经领取
    ///</summary>
    ERR_TRY_MISSION_HAS_GAIN_BOX                  = 2156,    

    ///<summary>
    ///该章节今日未选取
    ///</summary>
    ERR_TRY_MISSION_CHAPTER_NOT_CHOOSE            = 2157,    

    ///<summary>
    ///星数异常
    ///</summary>
    ERR_TRY_MISSION_STAR_EXCEPTION                = 2158,    

    ///<summary>
    ///章节异常
    ///</summary>
    ERR_TRY_MISSION_CHAPTER_EXCEPTION             = 2159,    

    ///<summary>
    ///没有奖励可领取
    ///</summary>
    ERR_TRY_MISSION_REWARD_EMPTY                  = 2160,    

    ///<summary>
    ///关卡禁止进入
    ///</summary>
    ERR_TRY_MISSION_FORBID_ENTER                  = 2161,    

    ///<summary>
    ///章节不匹配
    ///</summary>
    ERR_TRY_MISSION_CHAPTER_UNMATCH               = 2162,    

    ///<summary>
    ///副本数据不匹配
    ///</summary>
    ERR_TRY_MISSION_DATA_UNMATCH                  = 2163,    

    ///<summary>
    ///扫荡超过历史最高层数
    ///</summary>
    ERR_TRY_MISSION_OVER_MAX_LEVEL                = 2164,    
    

    ///<summary>
    ///-----------------------------2201-2250 组队系统扩展----------------------------------------
    ///</summary>
    

    ///<summary>
    ///已经在跟随状态，不能重复设置
    ///</summary>
    ERR_TEAM_ALERADY_FOLLOW                  = 2201,     

    ///<summary>
    ///不再组队状态，不能跟随
    ///</summary>
    ERR_TEAM_NOT_IN_TEAM_CAN_NOT_FOLLOW      = 2202,     

    ///<summary>
    ///不在跟随状态，不能设置
    ///</summary>
    ERR_TEAM_NOT_FOLLOW                      = 2203,     

    ///<summary>
    ///队长不能跟随
    ///</summary>
    ERR_TEAM_LEADER_CAN_NOT_FOLLOW           = 2204,     

    ///<summary>
    ///队长没有在公会中
    ///</summary>
    ERR_TEAM_LIMIT_LEADER_NOT_GUILD          = 2205,     

    ///<summary>
    ///xx没有在公会中
    ///</summary>
    ERR_TEAM_LIMIT_MEMBER_NOT_GUILD          = 2206,     

    ///<summary>
    ///对方不在可传送状态
    ///</summary>
    ERR_TEAM_OTHER_NOT_IN_SEND_STATE         = 2207,     

    ///<summary>
    ///自己不在可传送状态
    ///</summary>
    ERR_TEAM_NOT_IN_SEND_STATE               = 2208,     

    ///<summary>
    ///邀请cd中
    ///</summary>
    ERR_TEAM_INVITE_CD                       = 2209,     

    ///<summary>
    ///game_id错误
    ///</summary>
    ERR_TEAM_GAME_ID_ERR                     = 2210,     


    ///<summary>
    ///-----------------------------2301-2350 拾取宝箱----------------------------------------
    ///</summary>
    

    ///<summary>
    ///cd不足，不能拾取
    ///</summary>
    ERR_PICKUP_CHEST_CD                  = 2301,     

    ///<summary>
    ///次数用完，不能拾取
    ///</summary>
    ERR_PICKUP_TIMES                     = 2302,     

    ///<summary>
    ///背包空间不足，不能拾取
    ///</summary>
    ERR_PICKUP_BAG_SPACE                 = 2303,     

    ///<summary>
    ///客户端参数错误
    ///</summary>
    ERR_PICKUP_ARG                       = 2304,     

    ///<summary>
    ///玩家已经拾取过该宝箱
    ///</summary>
    ERR_PICKUP_ALERADY                   = 2305,     

    ///<summary>
    ///宝箱拾取次数已到上限
    ///</summary>
    ERR_PICKUP_CHEST_TIMES               = 2306,     

    ///<summary>
    ///宝箱拾取的消耗不足
    ///</summary>
    ERR_PICKUP_COST                      = 2307,     


    ///<summary>
    ///-----------------------------2351~2400 藏宝图---------------------------------------
    ///</summary>
    

    ///<summary>
    ///达到今天的拾取上限
    ///</summary>
    ERR_TREASURE_MAP_MAX_COUNT           = 2351,     

    ///<summary>
    ///不在指定的地图
    ///</summary>
    ERR_TREASURE_MAP_ERR_MAP             = 2352,     

    ///<summary>
    ///不是传送么所有者，不能传送
    ///</summary>
    ERR_TREASURE_PORTAL_NOT_OWNER        = 2353,     
    

    ///<summary>
    ///----------------------绑定手机2401-2410----------------------------------------------
    ///</summary>
    

    ///<summary>
    ///重复绑定
    ///</summary>
    ERR_BIND_MOBILE_REPEAT_BIND          = 2401,     

    ///<summary>
    ///奖励已经领取
    ///</summary>
    ERR_BIND_MOBILE_HAS_GET_REWARD       = 2402,     

    ///<summary>
    ///每日验证码超过上限
    ///</summary>
    ERR_BIND_MOBILE_DAILY_CODE_LIMIT     = 2403,     

    ///<summary>
    ///生成验证码太快
    ///</summary>
    ERR_BIND_MOBILE_CODE_INTERVAL        = 2404,     

    ///<summary>
    ///验证码不匹配
    ///</summary>
    ERR_BIND_MOBILE_CODE_NOT_MATCH       = 2405,     

    ///<summary>
    ///验证码过期
    ///</summary>
    ERR_BIND_MOBILE_CODE_PAST_DUE        = 2406,     



    ///<summary>
    ///----------------------2451-2500 单人副本玩法---------------------------------------------
    ///</summary>
    

    ///<summary>
    ///你不能向自己邀请
    ///</summary>
    ERR_PK_INVITE_SELF                     = 2451,  

    ///<summary>
    ///你不在可pk场景
    ///</summary>
    ERR_PK_NOT_IN_PK_MAP                   = 2452,  

    ///<summary>
    ///你在队伍中
    ///</summary>
    ERR_PK_IN_TEAM                         = 2453,  

    ///<summary>
    ///被邀请者不存在 被邀请者dbid错误
    ///</summary>
    ERR_PK_INVITEE_NOT_EXIST               = 2454,  

    ///<summary>
    ///被邀请者不在线
    ///</summary>
    ERR_PK_INVITEE_NOT_ONLINE              = 2455,  

    ///<summary>
    ///被邀请者在队伍中
    ///</summary>
    ERR_PK_INVITEE_IN_TEAM                 = 2456,  

    ///<summary>
    ///被邀请者不在可pk场景
    ///</summary>
    ERR_PK_INVITEE_NOT_IN_PK_MAP           = 2457,  

    ///<summary>
    ///你不能向自己接受
    ///</summary>
    ERR_PK_ACCEPT_SELF                     = 2458,  

    ///<summary>
    ///邀请者不存在 邀请者dbid错误
    ///</summary>
    ERR_PK_INVITER_NOT_EXIST               = 2459,  

    ///<summary>
    ///邀请者不在线
    ///</summary>
    ERR_PK_INVITER_NOT_ONLINE              = 2460,  

    ///<summary>
    ///邀请者在队伍中
    ///</summary>
    ERR_PK_INVITER_IN_TEAM                 = 2461,  

    ///<summary>
    ///邀请者不在可pk场景
    ///</summary>
    ERR_PK_INVITER_NOT_IN_PK_MAP           = 2462,  

    ///<summary>
    ///被邀请者拒绝了你的pk邀请
    ///</summary>
    ERR_PK_INVITEE_REJECT                  = 2463,  
    
    

    ///<summary>
    ///---------------------------2501-2550 称号系统---------------------------------------------
    ///</summary>
    

    ///<summary>
    ///称号背包为空
    ///</summary>
    ERR_TITLE_BAG_IS_EMPTY                 = 2501,  

    ///<summary>
    ///称号已装备
    ///</summary>
    ERR_TITLE_HAS_EQUIPED                  = 2502,  

    ///<summary>
    ///未获得该称号
    ///</summary>
    ERR_TITLE_HAS_NOT_GET_TITLE            = 2503,  

    ///<summary>
    ///该称号已经过期
    ///</summary>
    ERR_TITLE_HAS_PAST_DUE                 = 2504,  


    ///<summary>
    ///---------------------------2551-2600 成就系统---------------------------------------------
    ///</summary>
    

    ///<summary>
    ///成就不存在
    ///</summary>
    ERR_ACHIEVEMENT_CFG_NOT_FOUND          = 2551,  

    ///<summary>
    ///成就的奖励字段为空
    ///</summary>
    ERR_ACHIEVEMENT_NULL_FIELD_REWARD      = 2552,  
    
    

    ///<summary>
    ///----------------------充值系统2600-2700--------------------------
    ///</summary>
    

    ///<summary>
    ///所有充值数据
    ///</summary>
    ERR_CHARGE_DATAS                       = 2600,  

    ///<summary>
    ///充值到账通知
    ///</summary>
    ERR_CHARGE_NOTIFY                      = 2601,  

    ///<summary>
    ///获取资源超时
    ///</summary>
    ERR_CHARGE_TIMEOUT                     = 2602,  

    ///<summary>
    ///获取资源失败
    ///</summary>
    ERR_CHARGE_FAILURE                     = 2603,  

    ///<summary>
    ///资源数据异常
    ///</summary>
    ERR_CHARGE_SYS_LOSE                    = 2604,  

    ///<summary>
    ///充值数据不一致
    ///</summary>
    ERR_CHARGE_UNMATCH                     = 2605,  

    ///<summary>
    ///购买次数超限
    ///</summary>
    ERR_CHARGE_TIMES_OVER                  = 2606,  

    ///<summary>
    ///购买时间限制
    ///</summary>
    ERR_CHARGE_TIME_LIMIT                  = 2607,  

    ///<summary>
    ///领取成功通知
    ///</summary>
    ERR_CHARGE_GET_SUCCESS                 = 2608,  

    ///<summary>
    ///不是好友
    ///</summary>
    ERR_CHARGE_NOT_FRIEND                  = 2609,  

    ///<summary>
    ///充值金额不足
    ///</summary>
    ERR_CHARGE_NUM_NOT_ENOUGH              = 2610,  

    

    ///<summary>
    ///---------------------------2701-2800 炼金系统---------------------------------------------
    ///</summary>
    

    ///<summary>
    ///炼金次数已满
    ///</summary>
    ERR_ALCHEMY_NO_ALCHEMY_CNT            = 2701,  

    ///<summary>
    ///被邀请者的协助次数已满
    ///</summary>
    ERR_ALCHEMY_NO_ASSIST_CNT             = 2702,  

    ///<summary>
    ///炼金在cd中
    ///</summary>
    ERR_ALCHEMY_IN_CD                     = 2703,  

    ///<summary>
    ///炼金已经完成，请先领取
    ///</summary>
    ERR_ALCHEMY_HAD_FINISH                = 2704,  

    ///<summary>
    ///没有足够的消耗道具
    ///</summary>
    ERR_ALCHEMY_NOT_ENOUGH_ITEMS          = 2705,  

    ///<summary>
    ///请先炼金，再领取
    ///</summary>
    ERR_ALCHEMY_NO_ALCHEMY                = 2706,  

    ///<summary>
    ///不是好友或者不是公会成员
    ///</summary>
    ERR_ALCHEMY_NO_FRIEND                 = 2707,  

    ///<summary>
    ///只能邀请x级以上的好友或工会成员
    ///</summary>
    ERR_ALCHEMY_ASSIST_LEVEL_LIMIT        = 2708,  

    ///<summary>
    ///邀请的玩家不在线
    ///</summary>
    ERR_ALCHEMY_NOT_ONLINE                = 2709,  
    

    ///<summary>
    ///---------------------------2801-2850 队长分配系统---------------------------------------------
    ///</summary>
    

    ///<summary>
    ///新一轮分配
    ///</summary>
    CAPTAIN_NOTIRY_TYPE_NEW           = 2801,     

    ///<summary>
    ///当前分配结果
    ///</summary>
    CAPTAIN_NOTIRY_TYPE_RESULT        = 2802,     

    ///<summary>
    ///队友申请/放弃
    ///</summary>
    CAPTAIN_NOTIRY_TYPE_UPDATE        = 2803,     

    ///<summary>
    ///该玩家不在队伍不可分配
    ///</summary>
    CAPTAIN_NOTIRY_TYPE_OUT_GROUP     = 2804,     

    ///<summary>
    ///您不是队长不可分配
    ///</summary>
    CAPTAIN_NOTIRY_TYPE_NOT_CAPTAIN   = 2805,     

    ///<summary>
    ///队伍已经解散
    ///</summary>
    CAPTAIN_NOTIRY_TYPE_GROUP_BREAK   = 2806,     

    ///<summary>
    ///---------------------------2851-2900 装备熔炼系统---------------------------------------------
    ///</summary>
    

    ///<summary>
    ///找不到配置
    ///</summary>
    ERR_EQUIP_SMELTING_NO_CONFIG      = 2851,     

    ///<summary>
    ///等级错误
    ///</summary>
    ERR_EQUIP_SMELTING_WRONG_LEVEL    = 2852,     


    ///<summary>
    ///---------------------------2901~2920 坐骑系统------------------------------------
    ///</summary>
    

    ///<summary>
    ///没有这个坐骑
    ///</summary>
    ERR_RIDE_NORIDE                            = 2901,     

    ///<summary>
    ///职业不匹配(这个职业传不了)
    ///</summary>
    ERR_RIDE_VOCATION_MISMATCH                 = 2902,     

    ///<summary>
    ///已经有了
    ///</summary>
    ERR_RIDE_ALREADY_OWN                       = 2903,     

    ///<summary>
    ///时间错误
    ///</summary>
    ERR_RIDE_INVALID_TIME                      = 2904,     

    ///<summary>
    ///仅飞行坐骑可以飞行
    ///</summary>
    ERR_RIDE_NEED_RIDE_FLY                     = 2905,     

    ///<summary>
    ///您需要骑乘飞行坐骑
    ///</summary>
    ERR_RIDE_NEED_FLYING_RIDE                  = 2906,     


    ///<summary>
    ///---------------------------2921~2950 公会商城系统------------------------------------
    ///</summary>
    

    ///<summary>
    ///没有公会
    ///</summary>
    ERR_GUILD_SHOP_NO_GUILD                    = 2921,     

    ///<summary>
    ///公会商店请求参数校验失败（商城数据版本错误）
    ///</summary>
    ERR_GUILD_SHOP_PARAMS_VERIFY_FAILED        = 2922,     

    ///<summary>
    ///公会商店数据不存在
    ///</summary>
    ERR_GUILD_SHOP_DATA_NOT_EXIST              = 2923,     

    ///<summary>
    ///公会商店剩余数量不足
    ///</summary>
    ERR_GUILD_SHOP_REMAIN_COUNT_NOT_ENOUGH     = 2924,     

    ///<summary>
    ///公会职位不符合要求
    ///</summary>
    ERR_GUILD_SHOP_POSITION_NOT_SUPPORT        = 2925,     

    ///<summary>
    ///公会商店点赞无效（重复操作）
    ///</summary>
    ERR_GUILD_SHOP_ALREADY_ADD_PRAISE          = 2926,     

    ///<summary>
    ///公会商店取消赞无效（重复操作）
    ///</summary>
    ERR_GUILD_SHOP_ALREADY_DEL_PRAISE          = 2927,     

    ///<summary>
    ///公会商店操作太快了（购买或兑换）
    ///</summary>
    ERR_GUILD_SHOP_OP_TOO_FAST                 = 2928,     

    ///<summary>
    ///公会商店点赞数已到上限
    ///</summary>
    ERR_GUILD_SHOP_ADD_PRAISE_LIMITED          = 2929,     

    ///<summary>
    ///公会商店商品未解锁（购买或兑换）
    ///</summary>
    ERR_GUILD_SHOP_ITEM_LOCKED                 = 2930,     

    ///<summary>
    ///公会商店商品购买总次数已达上限（总量限购）
    ///</summary>
    ERR_GUILD_SHOP_TOTAL_BUY_LIMIT             = 2931,     

    ///<summary>
    ///公会商店商品每日购买次数已达上限（每日限购）
    ///</summary>
    ERR_GUILD_SHOP_DAILY_BUY_LIMIT             = 2932,     

    ///<summary>
    ///公会商店商品无需购买（无需进货）
    ///</summary>
    ERR_GUILD_SHOP_DONT_NEED_BUY               = 2933,     

    ///<summary>
    ///请求会长（在线，有权限的人）补货（带一个道具ID参数）
    ///</summary>
    ERR_GUILD_SHOP_REQUEST                     = 2934,     



    ///<summary>
    ///---------------------------2951-3000 创角活动(七天)---------------------------------------------
    ///</summary>
    

    ///<summary>
    ///配置不存在
    ///</summary>
    ERR_CREATE_ACTIVITY_CFG_NOT_FOUND          = 2951,  

    ///<summary>
    ///奖励字段为空
    ///</summary>
    ERR_CREATE_ACTIVITY_NULL_FIELD_REWARD      = 2952,  

    ///<summary>
    ///未完成
    ///</summary>
    ERR_CREATE_ACTIVITY_NOT_COMPLETED          = 2953,  

    ///<summary>
    ///活动关闭了
    ///</summary>
    ERR_CREATE_ACTIVITY_CLOSE                  = 2954,  

    ///<summary>
    ///已领取终极大奖
    ///</summary>
    ERR_CREATE_ACTIVITY_FINAL_FLAG             = 2955,  

    ///<summary>
    ///已经领取普通奖励
    ///</summary>
    ERR_CREATE_ACTIVITY_HAD_REWARD             = 2956,  

    ///<summary>
    ///终极大奖,进度还不达要求
    ///</summary>
    ERR_CREATE_ACTIVITY_FINAL_PROCESS          = 2957,  

    ///<summary>
    ///领取普通奖励，天数不对,当前天需要大于等于配置day_sort才可以领取
    ///</summary>
    ERR_CREATE_ACTIVITY_REWARD_DAY             = 2958,  


    ///<summary>
    ///---------------------------3001~3020 装备重铸---------------------------------------------
    ///</summary>
    

    ///<summary>
    ///找不到装备
    ///</summary>
    ERR_EQUIP_RECAST_CANT_FIND_EQUIP           = 3001,  

    ///<summary>
    ///找不到重铸信息
    ///</summary>
    ERR_EQUIP_RECAST_CANT_FIND_RECAST_INFO     = 3002,  

    ///<summary>
    ///参数错误
    ///</summary>
    ERR_EQUIP_RECAST_WRONG_ARG                 = 3003,  

    ///<summary>
    ///重铸出带buff的装备
    ///</summary>
    SYS_ID_EQUIP_RECAST_BUFF_NOTICE            = 3004,  
    

    ///<summary>
    ///---------------------------3021~3040 装备升级---------------------------------------------
    ///</summary>
    

    ///<summary>
    ///找不到配置
    ///</summary>
    ERR_EQUIP_UPGRADE_CANT_FIND_CFG            = 3021,  

    ///<summary>
    ///装备不能升级
    ///</summary>
    ERR_EQUIP_UPGRADE_CANT_UPGRADE             = 3021,  

    ///<summary>
    ///装备升级消耗不足
    ///</summary>
    ERR_EQUIP_UPGRADE_COST_INSUFFICIENT        = 3022,  

    ///<summary>
    ///职业错误
    ///</summary>
    ERR_EQUIP_UPGRADE_WRONG_VOCATION           = 3023,  
    

    ///<summary>
    ///---------------------------3051~3100 导师系统---------------------------------------------
    ///</summary>
    

    ///<summary>
    ///奖励配置错误
    ///</summary>
    ERR_TUTOR_CFG_ERROR                        = 3051,  

    ///<summary>
    ///领奖等级不符合要求
    ///</summary>
    ERR_TUTOR_REWARD_LEVEL_LIMIT               = 3052,  

    ///<summary>
    ///已经领奖
    ///</summary>
    ERR_TUTOR_HAD_REWARD                       = 3053,  

    ///<summary>
    ///exp每天限制
    ///</summary>
    ERR_TUTOR_EXP_LIMIT                        = 3054,  

    ///<summary>
    ///value每天限制
    ///</summary>
    ERR_TUTOR_VALUE_LIMIT                      = 3055,  

    ///<summary>
    ///满级
    ///</summary>
    ERR_TUTOR_FULL_LEVEL                       = 3056,  

    ///<summary>
    ///导师等级过低
    ///</summary>
    ERR_AVATAR_TUTOR_LEVEL_LOW                 = 3057,  

    ///<summary>
    ///获得导师经验
    ///</summary>
    ERR_AVATAR_TUTOR_GET_EXP                   = 3058,  

    ///<summary>
    ///获得导师值
    ///</summary>
    ERR_AVATAR_TUTOR_GET_VALUE                 = 3059,  

    ///<summary>
    ///获得导师经验和导师值
    ///</summary>
    ERR_AVATAR_TUTOR_GET_EXP_VALUE             = 3060,  


    ///<summary>
    ///--------------------------3200~3250 红包系统--------------------------
    ///</summary>
    

    ///<summary>
    ///操作过快，请稍等
    ///</summary>
    RED_ENVELOPE_GRABING     = 3200,      

    ///<summary>
    ///次数超过总上限
    ///</summary>
    RED_ENVELOPE_SUM_TIMES   = 3201,      

    ///<summary>
    ///抢不到红包
    ///</summary>
    RED_ENVELOPE_FAIL        = 3202,      


    ///<summary>
    ///--------------------------3251~3300 公会任务系统--------------------------
    ///</summary>
    

    ///<summary>
    ///公会中找不到这个任务
    ///</summary>
    ERR_CANT_FIND_TASK_IN_GUILD                = 3251,  

    ///<summary>
    ///公会中这个任务已经接完了
    ///</summary>
    ERR_REAMAIN_TASK_COUNT_IS_ZERO             = 3252,  

    ///<summary>
    ///时间不对
    ///</summary>
    ERR_GUILD_TASK_WRONG_TIME                  = 3253,  

    ///<summary>
    ///已经有一个公会任务了
    ///</summary>
    ERR_ALREADY_HAS_A_GUILD_TASK               = 3254,  

    ///<summary>
    ///没有公会任务
    ///</summary>
    ERR_DONT_HAVE_A_GUILD_TASK                 = 3255,  

    ///<summary>
    ///错误的公会任务
    ///</summary>
    ERR_WRONG_GUILD_TASK                       = 3256,  

    ///<summary>
    ///找不到这个公会任务
    ///</summary>
    ERR_CANT_FIND_GUILD_TASK                   = 3257,  

    ///<summary>
    ///任务已完成
    ///</summary>
    ERR_TASK_ACCOMPLISH                        = 3258,  

    ///<summary>
    ///找不到任务分数奖励配置
    ///</summary>
    ERR_CANT_FIND_SCORE_CFG                    = 3259,  

    ///<summary>
    ///公会分数不足
    ///</summary>
    ERR_NOT_ENOUGH_GUILD_SCORE                 = 3260,  

    ///<summary>
    ///兑换奖励的时候今日做过的公会任务数量不足
    ///</summary>
    ERR_NOT_ENOUGH_SCORE_TASK_NUM              = 3261,  

    ///<summary>
    ///这个任务没有次数了
    ///</summary>
    ERR_NO_TASK_COUNT                          = 3262,  

    ///<summary>
    ///这个组的奖励已经领了
    ///</summary>
    ERR_GROUP_HAS_REWARDED                     = 3263,  


    ///<summary>
    ///--------------------------公会GuildMgr 3301~3400 --------------------
    ///</summary>
    

    ///<summary>
    ///公会战召集在cd
    ///</summary>
    ERR_GUILD_BATTLE_CALL_CD                   = 3301,  


    ///<summary>
    ///----------------------决斗系统错误码122000~122100----------------
    ///</summary>
    


    ///<summary>
    ///赛季未开启
    ///</summary>
    ERR_DUEL_ENTER_MAP_SEASON_UNOPENED          = 122003,   

    ///<summary>
    ///您不在决斗场内，无法查找（代表玩家自身不在决斗候选地图）
    ///</summary>
    ERR_DUEL_GET_INVITE_LIST_SELF_NOT_IN_MAP    = 122004,   

    ///<summary>
    ///您已经在一场决斗中了，无法查找（代表玩家自身已开启一场决斗赛）
    ///</summary>
    ERR_DUEL_GET_INVITE_LIST_SELF_PLAYING       = 122005,   

    ///<summary>
    ///您不在决斗场内，无法发起决斗（代表玩家自身不在决斗候选地图）
    ///</summary>
    ERR_DUEL_INVITE_SELF_NOT_IN_MAP             = 122006,   

    ///<summary>
    ///目标不在决斗场内，无法发起决斗（代表对方不在决斗候选地图）
    ///</summary>
    ERR_DUEL_INVITE_OTHER_NOT_IN_MAP            = 122007,   

    ///<summary>
    ///您已经在一场决斗中了，无法发起决斗（代表玩家自身已开启一场决斗赛）
    ///</summary>
    ERR_DUEL_INVITE_SELF_PLAYING                = 122008,   

    ///<summary>
    ///目标已经在一场决斗中了，无法发起决斗（代表对方已开启一场决斗赛）
    ///</summary>
    ERR_DUEL_INVITE_OTHER_PLAYING               = 122009,   

    ///<summary>
    ///角色没有参加过决斗（代表角色不存在）
    ///</summary>
    ERR_DUEL_QUERY_PLAYER_INFO_NOT_FOUND        = 122010,   

    ///<summary>
    ///没有奖励可以领取（没有奖励可以领取）
    ///</summary>
    ERR_DUEL_GET_STAGE_REWARD_NOT_EXIST         = 122011,   

    ///<summary>
    ///获取要下注的比赛信息或状态时，发现比赛不存在（可能已经结束）
    ///</summary>
    ERR_DUEL_GET_GAMBLE_INFO_NOT_EXIST          = 122012,   

    ///<summary>
    ///获取要下注的比赛信息或状态时，发现比赛不能下注（可能是无效局或者下注已超时）
    ///</summary>
    ERR_DUEL_GET_GAMBLE_INFO_CANT_BET           = 122013,   

    ///<summary>
    ///获取要下注的比赛信息时，发现自己已有比赛不能获取到信息
    ///</summary>
    ERR_DUEL_GET_GAMBLE_INFO_IN_OTHER_MATCH     = 122014,   

    ///<summary>
    ///对普通比赛下注时，发现下注太快（受CD拦截，客户端尝试多次下注被驳回，避免重复下注）
    ///</summary>
    ERR_DUEL_BET1_TOO_FAST                      = 122020,   

    ///<summary>
    ///对普通比赛下注时，发现比赛不存在（可能已经结束）
    ///</summary>
    ERR_DUEL_BET1_NOT_EXIST                     = 122021,   

    ///<summary>
    ///对普通比赛下注时，发现比赛是眷顾之战（下注方式错误）
    ///</summary>
    ERR_DUEL_BET1_TYPE_ERROR                    = 122022,   

    ///<summary>
    ///对普通比赛下注时，发现比赛不能下注（可能是无效局或者下注已超时）
    ///</summary>
    ERR_DUEL_BET1_CANT_BET                      = 122023,   

    ///<summary>
    ///对普通比赛下注时，发现比赛已曾下注过
    ///</summary>
    ERR_DUEL_BET1_HAS_DONE                      = 122024,   

    ///<summary>
    ///对普通比赛下注时，发现自己已有比赛不能下注
    ///</summary>
    ERR_DUEL_BET1_IN_OTHER_MATCH                = 122025,   

    ///<summary>
    ///对普通比赛下注时，发现下注的角色错误
    ///</summary>
    ERR_DUEL_BET1_WRONG_PLAYER                  = 122026,   

    ///<summary>
    ///对普通比赛下注时，发现下注货币不足
    ///</summary>
    ERR_DUEL_BET1_OUT_OF_MONEY                  = 122027,   

    ///<summary>
    ///对普通比赛下注时，发现下注获奖数量已达上限，禁止下注
    ///</summary>
    ERR_DUEL_BET1_REWARD_LIMIT                  = 122028,   

    ///<summary>
    ///普通比赛结束时，给予下注失败者的提示（带名字参数，依次为：比赛胜利者、比赛失败者）
    ///</summary>
    ERR_DUEL_BET1_FAILED                        = 122029,   

    ///<summary>
    ///对眷顾之战比赛下注时，发现下注太快（受CD拦截，客户端尝试多次下注被驳回，避免重复下注）
    ///</summary>
    ERR_DUEL_BET2_TOO_FAST                      = 122030,   

    ///<summary>
    ///对眷顾之战比赛下注时，发现比赛不存在（可能已经结束）
    ///</summary>
    ERR_DUEL_BET2_NOT_EXIST                     = 122031,   

    ///<summary>
    ///对眷顾之战比赛下注时，发现比赛是眷顾之战（下注方式错误）
    ///</summary>
    ERR_DUEL_BET2_TYPE_ERROR                    = 122032,   

    ///<summary>
    ///对眷顾之战比赛下注时，发现比赛不能下注（可能是无效局或者下注已超时）
    ///</summary>
    ERR_DUEL_BET2_CANT_BET                      = 122033,   

    ///<summary>
    ///对眷顾之战比赛下注时，发现比赛已曾下注过
    ///</summary>
    ERR_DUEL_BET2_HAS_DONE                      = 122034,   

    ///<summary>
    ///对眷顾之战比赛下注时，发现自己已有比赛不能下注
    ///</summary>
    ERR_DUEL_BET2_IN_OTHER_MATCH                = 122035,   

    ///<summary>
    ///对眷顾之战比赛下注时，发现下注的角色错误
    ///</summary>
    ERR_DUEL_BET2_WRONG_PLAYER                  = 122036,   

    ///<summary>
    ///对眷顾之战比赛下注时，发现下注货币不足
    ///</summary>
    ERR_DUEL_BET2_OUT_OF_MONEY                  = 122037,   

    ///<summary>
    ///眷顾之战比赛开始公告（带比赛双方名字和比赛id参数）
    ///</summary>
    ERR_DUEL_BET2_BEGIN_NOTICE                  = 122040,   

    ///<summary>
    ///眷顾之战比赛结束公告（带名字参数，依次为：胜利者、失败者，没有幸运者）
    ///</summary>
    ERR_DUEL_BET2_END_NOTICE0                   = 122041,   

    ///<summary>
    ///眷顾之战比赛结束公告（带名字参数，依次为：胜利者、失败者，以及1个幸运者）
    ///</summary>
    ERR_DUEL_BET2_END_NOTICE1                   = 122042,   

    ///<summary>
    ///眷顾之战比赛结束公告（带名字参数，依次为：胜利者、失败者，以及2个幸运者）
    ///</summary>
    ERR_DUEL_BET2_END_NOTICE2                   = 122043,   

    ///<summary>
    ///眷顾之战比赛结束公告（带名字参数，依次为：胜利者、失败者，以及3个幸运者）
    ///</summary>
    ERR_DUEL_BET2_END_NOTICE3                   = 122044,   

    ///<summary>
    ///眷顾之战比赛结束公告（带名字参数，依次为：胜利者、失败者，以及4个幸运者）
    ///</summary>
    ERR_DUEL_BET2_END_NOTICE4                   = 122045,   

    ///<summary>
    ///眷顾之战比赛结束公告（带名字参数，依次为：胜利者、失败者，以及5个幸运者）
    ///</summary>
    ERR_DUEL_BET2_END_NOTICE5                   = 122046,   

    ///<summary>
    ///眷顾之战比赛平局结束公告（带比赛双方名字参数）
    ///</summary>
    ERR_DUEL_BET2_DRAWGAME_END                  = 122047,   



    ///<summary>
    ///----------------------------------------------------------------------------------------
    ///</summary>
    

    ///<summary>
    ///流程控制
    ///</summary>
    

    ///<summary>
    ///表示接收到这个错误码后继续
    ///</summary>
    ERR_CONTINUE              = 10001,     

    ///<summary>
    ///表示接收到这个错误码后跳出
    ///</summary>
    ERR_BREAK                 = 10002,     

    ///<summary>
    ///表示接收到这个错误码后返回
    ///</summary>
    ERR_RETURN                = 10003,     



    ///<summary>
    ///----------------------F码系统错误码130000~130100----------------
    ///</summary>
    

    ///<summary>
    ///F码cd时间没到
    ///</summary>
    ERR_F_CODE_CD             = 130000,    

    ///<summary>
    ///F码已拿过奖励
    ///</summary>
    ERR_F_CODE_ALREADY        = 130001,    






    ///<summary>
    ///----------------------------------------------------------------------------------------
    ///</summary>
    

    ///<summary>
    ///服务器系统公告
    ///</summary>
    
    

    ///<summary>
    ///----------------------组队系统30000-30050-------------------------
    ///</summary>
    

    ///<summary>
    ///(个人)被剔除队伍了
    ///</summary>
    SYS_ID_TEAM_BE_KICK              = 30000,     

    ///<summary>
    ///(队伍内广播)xx被剔除队伍了
    ///</summary>
    SYS_ID_TEAM_BE_KICK_TEAM         = 30001,     

    ///<summary>
    ///(队伍内广播)xx队员不同意进入xx副本
    ///</summary>
    SYS_ID_TEAM_NOT_AGREE            = 30002,     

    ///<summary>
    ///(队伍内广播)xx队员不满足进入xx副本的条件    --TODO:具体是什么原因呢。把原因发给客户端提示。很多。分开不同的系统消息协议。
    ///</summary>
    SYS_ID_TEAM_NOT_CONDITION        = 30003,     

    ///<summary>
    ///(队伍内广播)xx队员退出，取消进入xx副本
    ///</summary>
    SYS_ID_TEAM_EXIT                 = 30004,     

    ///<summary>
    ///(队伍内广播)xx队员掉线，取消进入xx副本
    ///</summary>
    SYS_ID_TEAM_CLIENT_DEATH         = 30005,     

    ///<summary>
    ///(队伍内广播)xx进入队伍
    ///</summary>
    SYS_ID_TEAM_ENTER                = 30006,     

    ///<summary>
    ///(队伍内广播)xx成为队长
    ///</summary>
    SYS_ID_TEAM_CAPTAIN              = 30007,     

    ///<summary>
    ///(队伍内广播)xx离开队伍
    ///</summary>
    SYS_ID_TEAM_LEAVE                = 30008,     

    ///<summary>
    ///?? 杀死怪物获得xx经验(组队+xx) --不需要,已经通过额外的buff处理额外奖励加成
    ///</summary>
    SYS_ID_TEAM_KILL_MONSTER         = 30009,     

    ///<summary>
    ///?? 捡到xx金币(组队+xx)         --不需要,已经通过额外的buff处理额外奖励加成
    ///</summary>
    SYS_ID_TEAM_PICK_GOLD            = 30010,     

    ///<summary>
    /// xx被召唤至yy附近
    ///</summary>
    SYS_ID_TEAM_CALL                 = 30011,     

    ///<summary>
    /// xx传送至yy附近
    ///</summary>
    SYS_ID_TEAM_SEND                 = 30012,     

    ///<summary>
    ///(队伍内广播)xx队员不满足进入xx副本的条件 。 ERR_AVATAR_LEVEL_LOW      15             xxx角色等级过低。
    ///</summary>
    SYS_ID_TEAM_NOT_COND_LEVEL_LOW   = 30013,     

    ///<summary>
    ///(队伍内广播)xx队员不满足进入xx副本的条件 。 ERR_AVATAR_HAS_IN_MAP      51            xxx玩家已经在副本中
    ///</summary>
    SYS_ID_TEAM_NOT_COND_IN_COPY     = 30014,     

    ///<summary>
    ///(队伍内广播)xx队员不满足进入xx副本的条件 。 ERR_MISSION_NO_MORE_DAILY_ENTER_COUNT 402  xxx玩家超过每日挑战关卡次数
    ///</summary>
    SYS_ID_TEAM_NOT_COND_ENTER_CNT   = 30015,     

    ///<summary>
    ///(队伍内广播)xx队员不满足进入xx副本的条件 。 ERR_AVATAR_LEVEL_HIGH           20        xxx角色等级过高。
    ///</summary>
    SYS_ID_TEAM_NOT_COND_LEVEL_HIGH  = 30016,     

    ///<summary>
    ///(队伍内广播)xx队员不满足进入xx副本的条件 。 ERR_AVATAR_FIFHT_FORCE_LOW           31        xxx玩家战斗力过低。
    ///</summary>
    SYS_ID_TEAM_NOT_COND_FIGHT_LOW   = 30017,     

    ///<summary>
    ///(队伍内广播)xx队员不满足进入xx副本的条件 。 ERR_AVATAR_FIFHT_FORCE_HIGH           32        xxx玩家战斗力过高
    ///</summary>
    SYS_ID_TEAM_NOT_COND_FIGHT_HIGH  = 30018,     
                                                                    
   

    ///<summary>
    ///----------------------公会系统30051-30100-------------------------
    ///</summary>
    

    ///<summary>
    ///整个公会成员都能接到的提示30051-30079
    ///</summary>
    

    ///<summary>
    ///{0}加入公会，欢迎新人
    ///</summary>
    SYS_ID_GUILD_JOIN                = 30051,      

    ///<summary>
    ///{0}离开了公会
    ///</summary>
    SYS_ID_GUILD_LEAVE               = 30052,      

    ///<summary>
    ///{0}已开启，各位兄弟姐妹快准备好参加吧！
    ///</summary>
    SYS_ID_GUILD_ACT_OPEN            = 30053,      

    ///<summary>
    ///{0}已结束，下次再见！
    ///</summary>
    SYS_ID_GUILD_ACT_CLOSE           = 30054,      

    ///<summary>
    ///在公会兄弟姐妹的努力下，达成{0}成就，喝彩吧！
    ///</summary>
    SYS_ID_GUILD_ACHIEVE_FINISH      = 30055,      

    ///<summary>
    ///经过不懈努力，新学会了{0}公会技能！
    ///</summary>
    SYS_ID_GUILD_LEARN_SKILL         = 30056,      

    ///<summary>
    ///公会技能{0}升至{0}级
    ///</summary>
    SYS_ID_GUILD_PROMOTE_SKILL       = 30057,      

    ///<summary>
    ///{0}捐献了{1}{2}，公会资金增加了{3}
    ///</summary>
    SYS_ID_GUILD_CONTRIBUTION        = 30058,      

    ///<summary>
    ///{0}由{1}升职为{2}
    ///</summary>
    SYS_ID_GUILD_POSITION_PROMOTION  = 30059,      

    ///<summary>
    ///{0}由{1}降职为{2}
    ///</summary>
    SYS_ID_GUILD_POSITION_DEMOTION   = 30060,      

    ///<summary>
    ///公会福利已更新，快去看看吧！
    ///</summary>
    SYS_ID_GUILD_WEAL_UPDATE         = 30061,      

    ///<summary>
    ///公会登录活跃人数过少，繁荣度降低{0,Num}点
    ///</summary>
    SYS_ID_GUILD_BOOMING_MEMBER_COST = 30063,      

    ///<summary>
    ///公会日常维护资金不足，繁荣度降低{0,Num}点
    ///</summary>
    SYS_ID_GUILD_BOOMING_FUND_COST   = 30064,      

    ///<summary>
    ///公会登录活跃人数正常，繁荣度增加{0,Num}点
    ///</summary>
    SYS_ID_GUILD_BOOMING_MEMBER_ADD  = 30065,      

    ///<summary>
    ///公会日常维护消耗正常，繁荣度增加{0,Num}点
    ///</summary>
    SYS_ID_GUILD_BOOMING_FUND_ADD    = 30066,      

    ///<summary>
    ///公会繁荣度降至为0，公会进入冻结期，将于{0,Num}天后自动解散
    ///</summary>
    SYS_ID_GUILD_BOOMING_FREEZE      = 30067,      

    ///<summary>
    ///报名公会战，没权限
    ///</summary>
    SYS_ID_GUILD_BATTLE_SIGNUP_NO_RIGHT= 30068,    

    ///<summary>
    ///不是报名阶段，不能报名
    ///</summary>
    SYS_ID_GUILD_BATTLE_SIGNUP_NO_STATE= 30069,    

    ///<summary>
    ///抽签公会战，没有权限
    ///</summary>
    SYS_ID_GUILD_BATTLE_DRAW_NO_RIGHT= 30070,      

    ///<summary>
    ///不是抽签阶段，不能抽签
    ///</summary>
    SYS_ID_GUILD_BATTLE_DRAW_NO_STATE= 30071,      

    ///<summary>
    ///没有空位，不能抽签
    ///</summary>
    SYS_ID_GUILD_BATTLE_DRAW_NO_POS  = 30072,      

    ///<summary>
    ///不是前16名，不能抽签
    ///</summary>
    SYS_ID_GUILD_BATTLE_DRAW_NO_RANK = 30073,      

    ///<summary>
    ///不能重复报名
    ///</summary>
    SYS_ID_GUILD_BATTLE_SIGNUP_NO_REPEAT=30074,    

    ///<summary>
    ///没有报名，不能抽签
    ///</summary>
    SYS_ID_GUILD_BATTLE_DRAW_NO_SIGNUP=30075,      

    ///<summary>
    ///不够资金，不能抽签
    ///</summary>
    SYS_ID_GUILD_BATTLE_DRAW_NOT_FUND= 30076,      

    ///<summary>
    ///轮空提示
    ///</summary>
    SYS_ID_GUILD_BATTLE_BYE          = 30077,      

    ///<summary>
    ///每轮公会战开始提示
    ///</summary>
    SYS_ID_GUILD_BATTLE_START        = 30078,      

    ///<summary>
    ///每轮公会战胜利提示
    ///</summary>
    SYS_ID_GUILD_BATTLE_WIN          = 30079,      


    ///<summary>
    ///某个成员接到的提示
    ///</summary>
    

    ///<summary>
    ///{0}公会{1}{2}拒绝了你的加入申请
    ///</summary>
    SYS_ID_GUILD_APPLY_REFUSED       = 30080,      

    ///<summary>
    ///你已加入{0}，和大家一起愉快玩耍
    ///</summary>
    SYS_ID_GUILD_JOIN_SUCCESS        = 30081,      

    ///<summary>
    ///有新的公会消息
    ///</summary>
    SYS_ID_GUILD_NEW_NOTICE          = 30082,      

    ///<summary>
    ///公会升级了
    ///</summary>
    SYS_ID_GUILD_LEVEL_UP            = 30083,      

    ///<summary>
    ///公会成就领奖
    ///</summary>
    SYS_ID_GUILD_ACHIEVEMENT_REWARD  = 30084,      


    ///<summary>
    ///每轮公会战失败提示
    ///</summary>
    SYS_ID_GUILD_BATTLE_LOSE         = 30085,      

    ///<summary>
    ///公会战友谊赛的轮空提示
    ///</summary>
    SYS_ID_GUILD_BATTLE_FRIENDLY_BYE = 30086,      

    ///<summary>
    ///{职位}{玩家名字}在公会商店补充了{道具名字}X{数量}，兄弟姐妹们可以去兑换啦~
    ///</summary>
    SYS_ID_GUILD_SHOP_BUY            = 30087,      


    ///<summary>
    ///公会战系统通知
    ///</summary>
    

    ///<summary>
    ///公会战试炼通知
    ///</summary>
    SYS_ID_GUILD_BATTLE_TEST_NOTICE  =30088,       

    ///<summary>
    ///公会战召集通知
    ///</summary>
    SYS_ID_GUILD_BATTLE_CALL         =30089,       

    ///<summary>
    ///公会战报名通知
    ///</summary>
    SYS_ID_GUILD_BATTLE_SIGNUP_NOTICE= 30090,      

    ///<summary>
    ///公会战抽签通知
    ///</summary>
    SYS_ID_GUILD_BATTLE_DRAW_NOTICE  = 30091,      

    ///<summary>
    ///公会战小组赛开始通知
    ///</summary>
    SYS_ID_GUILD_BATTLE_GROUP_MATCH_NOTICE=30092,  

    ///<summary>
    ///友谊赛结束通知
    ///</summary>
    SYS_ID_GUILD_BATTLE_FRIENDLY_MATCH_END_NOTICE=30093,

    ///<summary>
    ///小组赛友谊赛开始通知
    ///</summary>
    SYS_ID_GUILD_BATTLE_GROUP_FRIENDLY_MATCH_START_NOTICE=30094,

    ///<summary>
    ///淘汰赛友谊赛开始通知
    ///</summary>
    SYS_ID_GUILD_BATTLE_FINAL_FRIENDLY_MATCH_START_NOTICE=30095,

    ///<summary>
    ///公会战淘汰赛开始
    ///</summary>
    SYS_ID_GUILD_BATTLE_FINAL_MATCH_NOTICE= 30096, 

    ///<summary>
    ///淘汰赛结束通知
    ///</summary>
    SYS_ID_GUILD_BATTLE_FINAL_MATCH_END_NOTICE3=30097,

    ///<summary>
    ///淘汰赛结束通知
    ///</summary>
    SYS_ID_GUILD_BATTLE_FINAL_MATCH_END_NOTICE2=30098,

    ///<summary>
    ///淘汰赛结束通知
    ///</summary>
    SYS_ID_GUILD_BATTLE_FINAL_MATCH_END_NOTICE1=30099,


    ///<summary>
    ///公会语音聊天加资金提示
    ///</summary>
    SYS_ID_GUILD_CHAT_NOTICE         = 30100,       


    ///<summary>
    ///----------------------好友系统30101-30150-------------------------
    ///</summary>
    

    ///<summary>
    ///你和$(1,{0})共同完成$(4,{1})，你们获得了$(23,{2})点亲密值。{3}{4}{5}
    ///</summary>
    SYS_ID_FREIDN_DEGREE_MISSION     = 30101,      

    ///<summary>
    ///你向$(1,{0})赠送了$(1,{1})，你们获得了$(23,{2})点亲密值。{3}/{4}/{5}
    ///</summary>
    SYS_ID_FREIDN_DEGREE_GIVE_GOODS  = 30102,      

    ///<summary>
    ///$(1,{0})向你赠送了$(1,{1})，你们获得了$(23,{2})点亲密值。{3}/{4}/{5}
    ///</summary>
    SYS_ID_FREIDN_DEGREE_GIVE_GOODSED= 30103,      

    ///<summary>
    ///你与$(1,{0})于野外共同作战$(23,{1})分钟，亲密值增加$(23,{2})
    ///</summary>
    SYS_ID_FREIDN_DEGREE_TIME        = 30104,      

    ///<summary>
    ///你与$(1,{0})于野外共同击杀$(1,{1})，亲密值增加$(23,{2})
    ///</summary>
    SYS_ID_FREIDN_DEGREE_KILL        = 30105,      

    ///<summary>
    ///{0}帮你充值{1}钻石，你们的亲密值增加了{2}点
    ///</summary>
    SYS_ID_FRIEND_CHARGE_PRESENT     = 30106,      

    ///<summary>
    ///你帮{0}充值{1}钻石，你们的亲密值增加了{2}点
    ///</summary>
    SYS_ID_FRIEND_FOR_CHARGE         = 30107,      

    ///<summary>
    ///$(1,{0})与$(1,{1})共同炼金，亲密值增加$(23,{2}) --第一个是邀请者
    ///</summary>
    SYS_ID_FREIDN_DEGREE_ALCHEMY     = 30108,      
    

    ///<summary>
    ///----------------------幻境探索30151-30200-------------------------
    ///</summary>
    

    ///<summary>
    ///玩家$(1,{0,PlayerName})福星高照，将飞龙品质提升至金色，正在护送$(4,金色飞龙)！
    ///</summary>
    SYS_ID_DREAMLAND_QUALITY_GOLDEN  = 30151,      

    ///<summary>
    ///玩家$(1,{0,PlayerName})在飞龙大赛中被宝藏砸中脑门，获得了{1,ItemId}！
    ///</summary>
    SYS_ID_DREAMLAND_FINISH_REWARD   = 30152,      

    ///<summary>
    ///$(1,{0,PlayerName})打劫了$(2,{1,PlayerName})的金色飞龙，获得了{2,MoneyType}金币、{3,ItemId}经验的奖励！
    ///</summary>
    SYS_ID_DREAMLAND_ATK_GOLDEN      = 30153,      


    ///<summary>
    ///----------------------藏宝图30201-30210-------------------------
    ///</summary>
    

    ///<summary>
    ///玩家$(1,{0,PlayerName})福星高照，将飞龙品质提升至金色，正在护送$(4,金色飞龙)！
    ///</summary>
    SYS_ID_TREASURE_MAP_DROP         = 30201,      

    ///<summary>
    ///玩家$(1,{0,PlayerName})福星高照，将飞龙品质提升至金色，正在护送$(4,金色飞龙)！
    ///</summary>
    SYS_ID_TREASURE_MAP_BOSS         = 30202,      

    ///<summary>
    ///玩家$(1,{0,PlayerName})福星高照，将飞龙品质提升至金色，正在护送$(4,金色飞龙)！
    ///</summary>
    SYS_ID_TREASURE_MAP_TELEPORT_PORTAL = 30203,   

    ///<summary>
    ///玩家$(1,{0,PlayerName})福星高照，将飞龙品质提升至金色，正在护送$(4,金色飞龙)！
    ///</summary>
    SYS_ID_TREASURE_MAP_DROP_SELF    = 30204,      

    ///<summary>
    ///----------------------VIP30211-30220-----------------------------
    ///</summary>
    

    ///<summary>
    ///vip升级
    ///</summary>
    SYS_ID_VIP_LEVEL_UP              = 30211,      

    ///<summary>
    ///----------------------物品系统30221-30250-------------------------
    ///</summary>
    

    ///<summary>
    ///物品合成
    ///</summary>
    SYS_ID_ITEM_COMPOSE              = 30221,      

    ///<summary>
    ///物品品质公告
    ///</summary>
    SYS_ID_ITEM_QUALITY_NOTICE       = 30222,      
    

    ///<summary>
    ///----------------------宝石系统30251-30260--------------------------
    ///</summary>
    

    ///<summary>
    ///合成了5级以上宝石
    ///</summary>
    SYS_ID_GEM_COMBINE               = 30251,      

    ///<summary>
    ///有宝石返回到了背包
    ///</summary>
    SYS_ID_GEM_RETURN                = 30252,      

    ///<summary>
    ///有打孔器返回到了背包
    ///</summary>
    SYS_ID_GEM_PUNCHING_RETURN       = 30253,      
    

    ///<summary>
    ///----------------------pk系统30261-30270--------------------------
    ///</summary>
    

    ///<summary>
    ///xxx与yyyPK，将yyy打了个落花流水
    ///</summary>
    SYS_ID_INVITER_WIN               = 30261,      

    ///<summary>
    ///xxx与yyyPK，被yyy狠狠教训了一顿
    ///</summary>
    SYS_ID_INVITER_LOSE              = 30262,      


    ///<summary>
    ///----------------------队长分配30271-30280--------------------------
    ///</summary>
    

    ///<summary>
    ///队长分配通知ID
    ///</summary>
    CAPTAIN_DIST_NOTIFY_ID           = 30271,      

    ///<summary>
    ///----------------------队长分配30281-30290--------------------------
    ///</summary>
    

    ///<summary>
    ///争夺传送门返回信息
    ///</summary>
    FIGHT_FOR_TRANS_PORTAL           = 30281,      

    ///<summary>
    ///----------------------时空裂隙30291-30310--------------------------
    ///</summary>
    

    ///<summary>
    ///没有队伍
    ///</summary>
    ERR_SPACETIME_RIFT_NOT_TEAM      = 30291,      

    ///<summary>
    ///不是队长
    ///</summary>
    ERR_SPACETIME_RIFT_NOT_LEADER    = 30292,      

    ///<summary>
    ///超过每轮最大次数
    ///</summary>
    ERR_SPACETIME_RIFT_OVER_TIMES    = 30293,      

    ///<summary>
    ///随机的新任务
    ///</summary>
    ERR_SPACETIME_RIFT_RANDOM_TASK   = 30294,      

    ///<summary>
    ///没有申请任务
    ///</summary>
    ERR_SPACETIME_RIFT_NOT_APPLY_TASK= 30295,      

    ///<summary>
    ///玩家没有在队长身边
    ///</summary>
    ERR_SPACETIME_RIFT_FAR_CAPTAIN   = 30296,      

    ///<summary>
    ///同步任务完毕
    ///</summary>
    ERR_SPACETIME_RIFT_SYNC_OK       = 30297,      

    ///<summary>
    ///任务中断
    ///</summary>
    ERR_SPACETIME_RIFT_TASK_INTERUPT = 30298,      

    ///<summary>
    ///没有离线任务记录
    ///</summary>
    ERR_SPACETIME_RIFT_NO_OFF_RECORD = 30299,      

    ///<summary>
    ///任务没有完成不可领取奖励
    ///</summary>
    ERR_SPACETIME_RIFT_TASK_NOT_END  = 30299,      

    ///<summary>
    ///本周任务奖励已经领取
    ///</summary>
    ERR_SPACETIME_RIFT_HAS_REWARD    = 30300,      

    ///<summary>
    ///本周任务次数不足不可领取
    ///</summary>
    ERR_SPACETIME_RIFT_FORBID_GAIN   = 30301,      

    ///<summary>
    ///队伍中角色等级过低
    ///</summary>
    ERR_SPACETIME_RIFT_LOW_LEVEL     = 30302,      

    ///<summary>
    ///----------------------幸运转盘30311=30315--------------------------
    ///</summary>
    

    ///<summary>
    ///获得幸运币
    ///</summary>
    SYS_ID_LUCKY_TURNTABLE_LUCKY_COIN= 30311,      


    ///<summary>
    ///----------------------装备熔炼30316=30330--------------------------
    ///</summary>
    

    ///<summary>
    ///获得稀有装备
    ///</summary>
    SYS_ID_EQUIP_SMELTING_BROADCAST  = 30316,      


    ///<summary>
    ///----------------------晚上活动30401~30405--------------------------
    ///</summary>
    

    ///<summary>
    ///晚上活动优胜者通告
    ///</summary>
    NIGHT_ACTIVITY_WINNER_NOTICE     = 30401,      


    ///<summary>
    ///----------------------首次红包奖励30501~30505--------------------------
    ///</summary>
    

    ///<summary>
    ///首次红包奖励通告
    ///</summary>
    FIRST_RED_ENVELOPE_REWARD_NOTICE     = 30501,      


    ///<summary>
    ///----------------------公会提示30506~30509--------------------------
    ///</summary>
    

    ///<summary>
    ///公会新人私聊提示
    ///</summary>
    SYS_ID_GUILD_NOOB_CHAT_NOTICE         = 30506,        

    ///<summary>
    ///公会任务完成提示
    ///</summary>
    SYS_ID_GUILD_TASK_ACCOMPLISH          = 30507,        

    ///<summary>
    ///公会任务额外奖励通知提示
    ///</summary>
    SYS_ID_GUILD_TASK_REWARD_NOTICE       = 30508,        


    ///<summary>
    ///----------------------排行榜公告30510~30520--------------------------
    ///</summary>
    

    ///<summary>
    ///恶魔日榜发奖的时候的前三名
    ///</summary>
    SYS_ID_RANK_EVIL_DAY      = 30510,     

    ///<summary>
    ///恶魔周榜发奖的时候的前三名
    ///</summary>
    SYS_ID_RANK_EVIL_WEEK     = 30511,     

    ///<summary>
    ///晚上活动公会排行榜前三名
    ///</summary>
    SYS_ID_RANK_ACTIVITY      = 30512,     

    ///<summary>
    ///----------------------组队奖励赠送30521~30530--------------------------
    ///</summary>
    

    ///<summary>
    ///队长奖励已过期
    ///</summary>
    CAPTAIN_REWARD_RECORD_EXPIRE  = 30521, 

    ///<summary>
    ///对方已离线
    ///</summary>
    CAPTAIN_REWARD_PLAYER_OFFLINE = 30522, 

    ///<summary>
    ///可申请赠送的记录为空
    ///</summary>
    CAPTAIN_REWARD_RECORD_EMPTY   = 30523, 

    ///<summary>
    ///申请成功
    ///</summary>
    CAPTAIN_REWEAD_APPLY_SUCCESS  = 30524, 

    ///<summary>
    ///有人申请赠送
    ///</summary>
    CAPTAIN_REWEAD_SOMEONE_APPLY  = 30525, 

    ///<summary>
    ///奖励已转赠或已绑定
    ///</summary>
    CAPTAIN_REWEAD_CANNOT_APPLY   = 30526, 


    ///<summary>
    ///----------------------野外随机事件31000~31100----------------
    ///</summary>
    

    ///<summary>
    ///怪物出现
    ///</summary>
    WIDE_RANDOM_MONSTER_START_1     = 31000, 

    ///<summary>
    ///怪物出现
    ///</summary>
    WIDE_RANDOM_MONSTER_START_2     = 31001, 


    ///<summary>
    ///----------------------防沉迷系统错误码--------------------------
    ///</summary>
    

    ///<summary>
    ///只提示tips
    ///</summary>
    ERR_ANTI_ADDICTION_TIPS          = 0,     

    ///<summary>
    ///提示tips和退出按钮
    ///</summary>
    ERR_ANTI_ADDICTION_TIP_AND_QUIT  = 1,     

    ///<summary>
    ///提示tips可以不退出
    ///</summary>
    ERR_ANTI_ADDICTION_TIP_NOT_QUIT  = 2,     


    ///<summary>
    ///----------------------野外提示 31101~31150----------------------
    ///</summary>
    

    ///<summary>
    ///转交
    ///</summary>
    SYS_ID_WILD_TASK_REWARD_TRANSFER = 31101, 

    ///<summary>
    ///野外经验上限提示
    ///</summary>
    SYS_ID_WILD_DROP_EXP_LIMIT       = 31102, 


    ///<summary>
    ///----------------------道具转交提示 31151~31200----------------------
    ///</summary>
    
    SYS_ID_ITEM_TRANSFER            = 31151,


    ///<summary>
    ///----------------------问答系统错误码 31201~31250-------------------------
    ///</summary>
    
    SYS_ID_Q_AND_A_ALREADY          = 31201,


    ///<summary>
    ///----------------------老玩家回归错误码 31251~31280-------------------------
    ///</summary>
    

    ///<summary>
    ///没有回归奖励
    ///</summary>
    OLD_RETURN_REWARD_EMPTY         = 32251,  

    ///<summary>
    ///当前非回归状态
    ///</summary>
    OLD_RETURN_NOT_VALID_STATUS     = 32252,  

    ///<summary>
    ///已经领取奖励
    ///</summary>
    OLD_RETURN_HAS_GAIN_REWARD      = 32253,  

    ///<summary>
    ///不可领取奖励
    ///</summary>
    OLD_RETURN_NOT_GAIN_REWARD      = 32254,  


    ///<summary>
    ///----------------------公会系统31280-31380-------------------------
    ///</summary>
    

    ///<summary>
    ///公会聊天加资金的公会记事
    ///</summary>
    SYS_ID_GUILD_CHAT_RECORD        = 31280,  

    ///<summary>
    ///公会没资格参加公会战
    ///</summary>
    SYS_ID_GUILD_NO_BOSS            = 31281,  

}



    ///<summary>
    ///-----------------------------------------------------
    ///</summary>

}
