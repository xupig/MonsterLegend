using System;
using System.Collections.Generic;

namespace Common.ServerConfig {
    public static class battle_attri_config
    {
        public static int ATTRI_ID_STR = 1;
        public static int ATTRI_ID_STA = 2;
	    public static int ATTRI_ID_INT = 3;
	    public static int ATTRI_ID_SPI = 4;
	    public static int ATTRI_ID_DEX = 5;
	    public static int ATTRI_ID_LUC = 6;

        public static int ATTRI_ID_HP_BASE = 11 ;
        public static int ATTRI_ID_HP_RATE = 12 ;
        public static int ATTRI_ID_HP_ADDED = 13 ;
        public static int ATTRI_ID_EXTRA_HP_RATE = 14 ;
        public static int ATTRI_ID_EXTRA_HP_ADDED = 15 ;
        public static int ATTRI_ID_PHY_ATK_BASE = 16 ;
        public static int ATTRI_ID_PHY_ATK_RATE = 17 ;
        public static int ATTRI_ID_PHY_ATK_ADDED = 18 ;
        public static int ATTRI_ID_EXTRA_PHY_ATK_RATE = 19 ;
        public static int ATTRI_ID_EXTRA_PHY_ATK_ADDED = 20 ;
        public static int ATTRI_ID_MAG_ATK_BASE = 21 ;
        public static int ATTRI_ID_MAG_ATK_RATE = 22 ;
        public static int ATTRI_ID_MAG_ATK_ADDED = 23 ;
        public static int ATTRI_ID_EXTRA_MAG_ATK_RATE = 24 ;
        public static int ATTRI_ID_EXTRA_MAG_ATK_ADDED = 25 ;
        public static int ATTRI_ID_PHY_DEF_BASE = 26 ;
        public static int ATTRI_ID_PHY_DEF_RATE = 27 ;
        public static int ATTRI_ID_PHY_DEF_ADDED = 28 ;
        public static int ATTRI_ID_EXTRA_PHY_DEF_RATE = 29 ;
        public static int ATTRI_ID_EXTRA_PHY_DEF_ADDED = 30 ;
        public static int ATTRI_ID_MAG_DEF_BASE = 31 ;
        public static int ATTRI_ID_MAG_DEF_RATE = 32 ;
        public static int ATTRI_ID_MAG_DEF_ADDED = 33 ;
        public static int ATTRI_ID_EXTRA_MAG_DEF_RATE = 34 ;
        public static int ATTRI_ID_EXTRA_MAG_DEF_ADDED = 35 ;
        public static int ATTRI_ID_HIT_RATE = 36 ;
        public static int ATTRI_ID_MISS_RATE = 37 ;
        public static int ATTRI_ID_BROKEN_ATK_RATE = 38 ;
        public static int ATTRI_ID_CRIT = 39 ;
        public static int ATTRI_ID_ANTI_CRIT = 40 ;
        public static int ATTRI_ID_EXTRA_CRIT_RATE = 41 ;
        public static int ATTRI_ID_EXTRA_ANTI_CRIT_RATE = 42 ;
        public static int ATTRI_ID_CRIT_ATK_RATE = 43 ;
        public static int ATTRI_ID_CRIT_EXTRA_ATK_RATE = 44 ;
        public static int ATTRI_ID_ANTI_CRIT_EXTRA_ATK_RATE = 45 ;
        public static int ATTRI_ID_PHY_DEF_PENETRATION = 46 ;
        public static int ATTRI_ID_EXTRA_PHY_DEF_REDUCE_RATE = 47 ;
        public static int ATTRI_ID_EXTRA_PHY_DEF_PENETRATION_RATE = 48 ;
        public static int ATTRI_ID_MAG_DEF_PENETRATION = 49 ;
        public static int ATTRI_ID_EXTRA_MAG_DEF_REDUCE_RATE = 50 ;
        public static int ATTRI_ID_EXTRA_MAG_DEF_PENETRATION_RATE = 51 ;
        public static int ATTRI_ID_PVP_ADDITION = 52 ;
        public static int ATTRI_ID_PVP_ANTI = 53 ;
        public static int ATTRI_ID_EXTRA_PVP_ADDITION_RATE = 54 ;
        public static int ATTRI_ID_EXTRA_PVP_ANTI_RATE = 55 ;
        public static int ATTRI_ID_SHIELD_DEFENSE  = 56 ;
        public static int ATTRI_ID_MAG_RESISTANCE = 57 ;
        public static int ATTRI_ID_SLASHING_PROPERTY = 58 ;
        public static int ATTRI_ID_SLASHING_PROPERTY_DEFENSE = 59 ;
        public static int ATTRI_ID_PIERCING_PROPERTY = 60 ;
        public static int ATTRI_ID_PIERCING_PROPERTY_DEFENSE = 61 ;
        public static int ATTRI_ID_BLOW_PROPERTY = 62 ;
        public static int ATTRI_ID_BLOW_PROPERTY_DEFENSE = 63 ;
        public static int ATTRI_ID_ALL_PHY_PROPERTY = 64 ;
        public static int ATTRI_ID_ALL_PHY_PROPERTY_DEFENSE = 65 ;
        public static int ATTRI_ID_FIRE_PROPERTY = 66 ;
        public static int ATTRI_ID_FIRE_PROPERTY_DEFENSE = 67 ;
        public static int ATTRI_ID_ICE_PROPERTY = 68 ;
        public static int ATTRI_ID_ICE_PROPERTY_DEFENSE = 69 ;
        public static int ATTRI_ID_LIGHT_PROPERTY = 70 ;
        public static int ATTRI_ID_LIGHT_PROPERTY_DEFENSE = 71 ;
        public static int ATTRI_ID_DARK_PROPERTY = 72 ;
        public static int ATTRI_ID_DARK_PROPERTY_DEFENSE = 73 ;
        public static int ATTRI_ID_ALL_MAG_PROPERTY = 74 ;
        public static int ATTRI_ID_ALL_MAG_PROPERTY_DEFENSE = 75 ;
        public static int ATTRI_ID_FLOAT_ATK = 76 ;
        public static int ATTRI_ID_SKILL_RATE = 77 ;
        public static int ATTRI_ID_SKILL_ADDED = 78 ;
        public static int ATTRI_ID_DAMAGE_REDUCE = 79 ;
        public static int ATTRI_ID_MOVE_SPEED_RATE = 80 ;
        public static int ATTRI_ID_MOVE_SPEED_REDUCE_RATE = 81 ;
        public static int ATTRI_ID_SKILL_CD_EXTEND = 82 ;
        public static int ATTRI_ID_SKILL_CD_REDUCE = 83 ;
        public static int ATTRI_ID_ATTACK_SPEED_RATE = 84 ;
        public static int ATTRI_ID_RELEASE_SPEED_RATE = 85 ;
        public static int ATTRI_ID_WEAPONS_HARD = 86 ;
        public static int ATTRI_ID_HARD = 87 ;
        public static int ATTRI_ID_HARD_RES = 88 ;
        public static int ATTRI_ID_HP_RECOVER_BASE = 89 ;
        public static int ATTRI_ID_HP_RECOVER_RATE = 90 ;
        public static int ATTRI_ID_BE_HP_RECOVER_RATE = 91 ;
        public static int ATTRI_ID_OINTMENT_HP_RECOVER = 92 ;
        public static int ATTRI_ID_TREATMENT_HP_RECOVER_RATE = 93 ;
		public static int ATTRI_ID_MP_BASE = 94;
		public static int ATTRI_ID_MP_RATE = 95;
		public static int ATTRI_ID_MP_ADDED = 96;
		public static int ATTRI_ID_MP_RECOVER_BASE = 97;
		public static int ATTRI_ID_MP_RECOVER_RATE = 98;

        public static int MAX_ATTRI2_ID = 220;    //最大的一级二级属性id; <=这个值会保存到int[MAX_ATTRI2_ID]数组里;为了兼容填表;这个值实际比其他属性值还大

        //以下属性不会保存到int[]数组里
        public static int ATTRI_ID_MAX_HP = 200 ;
        public static int ATTRI_ID_CUR_HP = 201 ;
		public static int ATTRI_ID_MAX_MP = 202;
		public static int ATTRI_ID_CUR_MP = 203;
		public static int ATTRI_ID_CUR_EUDEMON_ENERGY = 205; //守护灵当前能量值			
		public static int ATTRI_ID_VOCATION = 301;
		public static int ATTRI_ID_LEVEL = 302;
		public static int ATTRI_ID_FACTION = 303;	//阵营
		public static int ATTRI_ID_ETYPE = 305;	//战斗实体类型
		public static int ATTRI_ID_SPACE_ID = 309;
		public static int ATTRI_ID_HIT_RADIUS = 311;     //受击半径
		public static int ATTRI_ID_EUDEMON_DURABILITY_RATIO = 318; //守护灵耐久度系数 * 10000  --(耐久度*0.06) / (1 + 0.06*耐久度)

        public static int ATTRI_ID_TOTAL_PHY_ATK    = 1001;
        public static int ATTRI_ID_TOTAL_MAG_ATK    = 1002;
        public static int ATTRI_ID_TOTAL_PHY_DEF    = 1003;
        public static int ATTRI_ID_TOTAL_MAG_DEF    = 1004;
        public static int ATTRI_ID_TOTAL_HP_RECOVER = 1005;
        public static int ATTRI_ID_COMBAT_VALUE     = 1006;
        public static int ATTRI_ID_TOTAL_HP         = 1007;
    }
}


