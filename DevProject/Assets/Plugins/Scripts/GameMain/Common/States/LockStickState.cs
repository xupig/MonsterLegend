﻿using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Common.States
{
    public static class LockStickState
    {
        //摇杆方向
        public static Vector2 direction = Vector2.zero;
        //摇杆力度
        public static float strength = 0;
        private static bool _isDragging = false;
        public static bool isDragging
        {
            set
            {
                _isDragging = value;
            }
            get { return _isDragging; }
        }
        public static int fingerId = -1;

        public static Vector3 GetMoveDirectionByLockStick()
        {
            if (CameraManager.GetInstance().CameraTransform == null) return Vector3.zero;
            Vector3 moveRight = CameraManager.GetInstance().CameraTransform.right;
            if (!GameMain.EntityPlayer.Player.actor.isFly)
            {
                moveRight.y = 0;
            }
            moveRight.Normalize();

            Vector3 moveForward = CameraManager.GetInstance().CameraTransform.forward;
            if (!GameMain.EntityPlayer.Player.actor.isFly)
            {
                moveForward.y = 0;
            }
            moveForward.Normalize();

            Vector3 moveDirection = moveRight * LockStickState.direction.x +
                                    moveForward * LockStickState.direction.y;
            moveDirection.Normalize();
            return moveDirection;
        }
    }
}
