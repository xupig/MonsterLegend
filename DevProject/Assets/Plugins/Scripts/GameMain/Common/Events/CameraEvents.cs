﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Events
{
    public static class CameraEvents
    {
        public static string ON_MAIN_CAMERA_LOADED = "ON_MAIN_CAMERA_LOADED";  
        public static string HIDE_MAIN_CAMERA = "HIDE_MAIN_CAMERA";
        public static string SHOW_MAIN_CAMERA = "SHOW_MAIN_CAMERA";  
    }
}
