﻿

namespace Common.Events
{
    public class TouchEvents
    {
        public const string ON_TOUCH_ENTITY = "TouchEvents_ON_TOUCH_ENTITY";
        public const string ON_DOUBLE_TOUCH_ENTITY = "TouchEvents_ON_DOUBLE_TOUCH_ENTITY";

        public const string ON_TOUCH_SCREEN = "TouchEvents_ON_TOUCH_SCREEN";

        public const string ON_TOUCH_TERRAIN = "TouchEvents_ON_TOUCH_TERRAIN";

        public const string ON_TOUCH_BLANK_SPACE = "TouchEvents_ON_TOUCH_BLANK_SPACE";
        public const string ON_DOUBLE_TOUCH_BLANK_SPACE = "TouchEvents_ON_DOUBLE_TOUCH_BLANK_SPACE";

        public const string ON_DRAW_UP = "TouchEvents_ON_DRAW_UP";
        public const string ON_DRAW_DOWN = "TouchEvents_ON_DRAW_DOWN";
    }
}
