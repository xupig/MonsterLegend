﻿#region 模块信息
/*==========================================
// 文件名：MogoHtmlUtils
// 命名空间: GameLogic.GameLogic.Common.Utils
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/7 14:58:42
// 描述说明：
// 其他：
//==========================================*/
#endregion

using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Common.Utils
{
    public static class GameStringUtils
    {

        private static string[] playerNameColorList = new string[]{"#39FE00"};

        private static string[] itemNameColorList = new string[]{"#39FE00"};

        private static Regex styleReg = new Regex(@"\$\(\d+,[^$]*?\)");
        public static string ConvertStyle(string str)
        {
            MatchCollection _matchCollection = styleReg.Matches(str);
            for (int i = 0; i < _matchCollection.Count; i++)
            {
                string _matchContent = _matchCollection[i].ToString();

                int _index = _matchContent.IndexOf(",");
                if (_index == -1) { LoggerHelper.Error("CalculatorKey Error !" + _matchContent); }
                string content = _matchContent.Substring(_index + 1, _matchContent.Length - _index - 2);
                string style = _matchContent.Substring(2, _index - 2);
                font_style fontStyle = style_helper.GetStyle(int.Parse(style));
                if (fontStyle == null)
                {
                    LoggerHelper.Error("字体配置表中不存在的字体样式:" + style);
                    fontStyle = style_helper.GetStyle(1);
                }
                str = str.Replace(_matchContent, AppendStyle(content, fontStyle));
            }
            return str;
        }
        
        public static string AppendStyle(string content,font_style style)
        {
            if(style.__color!=null)
            {
                content = AppendColor(style.__color, content);
            }
            if(style.__size!=0)
            {
                content = AppendSize(style.__size, content);
            }
            if(style.__bold!=0)
            {
                content = AppendBold(true, content);
            }
            return content;
        }
        

        public static string PlayerAppendColor(string name,int level)
        {
            return AppendColor(playerNameColorList[0], name);
        }

        public static string ItemAppendColor(string name, int level)
        {
            //MatchEve
            return AppendColor(itemNameColorList[0], name);
        }

        private static void match()
        {

        }

        public static string AppendColor(string color, string str)
        {
            return string.Concat("<color=", color, ">", str, "</color>");
        }

        public static string AppendSize(int size,string str)
        {
            return string.Concat("<size=", size, ">", str, "</size>");
        }

        public static string AppendBold(bool bold,string str)
        {
            if(bold)
            {
                return string.Concat("<b>", str, "</b>");
            }
            return str;
        }

        public static List<int> Convert2List(string str)
        {
            string[] itemList = str.Split(',');
            List<int> result = new List<int>();
            for (int i = 0; i < itemList.Length; i++)
            {
                result.Add(int.Parse(itemList[i]));
            }
            return result;
        }

        public static List<float> Convert2ListFloat(string str)
        {
            string[] itemList = str.Split(',');
            List<float> result = new List<float>();
            for (int i = 0; i < itemList.Length; i++)
            {
                result.Add(float.Parse(itemList[i]));
            }
            return result;
        }

        public static Dictionary<int, int> Convert2Dic(string str)
        {
            string[] itemList = str.Split(',');
            Dictionary<int, int> result = new Dictionary<int, int>();
            for (int i = 0; i < itemList.Length; i++)
            {
                string[] valueList = itemList[i].Split(':');
                result.Add(int.Parse(valueList[0]), int.Parse(valueList[1]));
            }
            return result;
        }

        public static Dictionary<int, float> Convert2DicFloat(string str)
        {
            string[] itemList = str.Split(',');
            Dictionary<int, float> result = new Dictionary<int, float>();
            for (int i = 0; i < itemList.Length; i++)
            {
                string[] valueList = itemList[i].Split(':');
                result.Add(int.Parse(valueList[0]), float.Parse(valueList[1]));
            }
            return result;
        }

        public static Dictionary<int, List<int>> Convert2OneToMany(string str)
        {
            string[] itemList = str.Split(';');
            Dictionary<int, List<int>> result = new Dictionary<int, List<int>>();
            for (int i = 0; i < itemList.Length; i++)
            {
                string[] valueList = itemList[i].Split(':');
                result.Add(int.Parse(valueList[0]), Convert2List(valueList[1]));
            }

            return result;
        }

        public static Dictionary<int, List<float>> Convert2OneToManyFloat(string str)
        {
            string[] itemList = str.Split(';');
            Dictionary<int, List<float>> result = new Dictionary<int, List<float>>();
            for (int i = 0; i < itemList.Length; i++)
            {
                string[] valueList = itemList[i].Split(':');
                result.Add(int.Parse(valueList[0]), Convert2ListFloat(valueList[1]));
            }

            return result;
        }

        public static string ReplaceChangeLine(string content)
        {
            return content.Replace("\\n", "\n");
        }

    }
}
