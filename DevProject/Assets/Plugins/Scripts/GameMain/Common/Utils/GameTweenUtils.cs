﻿using ACTSystem;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace MogoEngine.Utils
{
    public class GameTweenUtilsUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public enum GameTweenType
    {
        SUB = 0,
        ADD = 1
        
    }

    public class GameTween
    {

        private GameTweenType _tweenType = GameTweenType.SUB;
        private GameObject _resourceObj;
        private float _resourceValue;
        private float _targetValue;
        private Dictionary<GameTweenType, Action> _updateDict = null;
        private float _changeValue = 0f;
        private bool _isStart = false;
        private Material mat;
        //
        private float _changeValue2 = 0f;
        private float _resourceValue2;
        private Color _color;

        public bool isEnd { get; set; }
        public GameTween(GameObject resourceValue, float targetValue, float _startTime, float time,Color color, GameTweenType tweenType = GameTweenType.SUB)
        {
            _isStart = false;
            isEnd = false;
            this._resourceObj = resourceValue;
            mat = _resourceObj.GetComponent<MeshRenderer>().materials[0];
            
            this._targetValue = targetValue;
            this._tweenType = tweenType;
            this._color = color;
            _updateDict = new Dictionary<GameTweenType, Action>();
            //_updateDict.Add(MogoTweenType.ADD, AddUpdate);
           // _updateDict.Add(MogoTweenType.SUB, SubUpdate);
            //计算变化量
            _resourceValue = mat.GetColor("_BaseColor").a;
            _changeValue = ACTMathUtils.Abs(this._resourceValue - targetValue) / time;
            _changeValue /= 30f;
            //
            _resourceValue2 = mat.GetFloat("_InternalAlpha");
            _changeValue2 = ACTMathUtils.Abs(this._resourceValue2 - targetValue) / time;
            _changeValue2 /= 30f;

            GameLoader.Utils.Timer.TimerHeap.AddTimer(Convert.ToUInt32(_startTime * 1000), 0, () =>
                {
                    _isStart = true;
                    
                });
        }

        public void Update()
        {
            if (_isStart == false) return;
            //_updateDict[_tweenType]();
            switch (_tweenType)
            {
                case GameTweenType.SUB:
                    SubUpdate();
                    break;
                case GameTweenType.ADD:
                    AddUpdate();
                    break;
               
            }


        }    

        private void AddUpdate()
        {
            _resourceValue = mat.GetColor("_BaseColor").a;
            _resourceValue = _resourceValue + _changeValue;
            isEnd = _resourceValue >= _targetValue ? true : false;
            if (isEnd == true) _resourceValue = _targetValue;
            mat.SetColor("_BaseColor", new Color(this._color.r, this._color.g, this._color.b, _resourceValue));
           
            //
            _resourceValue2 = mat.GetFloat("_InternalAlpha");
            _resourceValue2 = _resourceValue2 + _changeValue2;
            if (isEnd == true) _resourceValue2 = _targetValue;
            mat.SetFloat("_InternalAlpha", _resourceValue2);

        }

        private void SubUpdate()
        {
            _resourceValue = mat.GetColor("_BaseColor").a;
            _resourceValue = _resourceValue - _changeValue; 
            isEnd = _resourceValue <= _targetValue ? true : false;
            if (isEnd == true) _resourceValue = _targetValue;
            mat.SetColor("_BaseColor", new Color(this._color.r, this._color.g, this._color.b, _resourceValue));

            //
            _resourceValue2 = mat.GetFloat("_InternalAlpha");
            _resourceValue2 = _resourceValue2 - _changeValue2;
            if (isEnd == true) _resourceValue2 = _targetValue;
            mat.SetFloat("_InternalAlpha", _resourceValue2);
            //Debug.LogError("1_______resourceValue: " + _resourceValue + ",_resourceValue2: " + _resourceValue2);
        }

        public void Dispose()
        {
            _updateDict = null;
        }

    }

    public class GameTweenUtils
    {
        private static GameTweenUtils _instance;
        public static GameTweenUtils instance
        {
            get 
            {
                if(_instance == null)
                {
                    _instance = new GameTweenUtils();
                }
                return _instance;
            }
        }

        private List<GameTween> _tweenList = null;


        public GameTweenUtils()
        {
            MogoWorld.RegisterUpdate<GameTweenUtilsUpdateDelegate>("MogoTweenUtils.Update", Update);
            _tweenList = new List<GameTween>();
        }

        private void Update()
        {
            if (_tweenList.Count <= 0) return;
            //
            for (int i = 0; i < _tweenList.Count; i++)
            {
                _tweenList[i].Update();
                if (_tweenList[i].isEnd == true)
                {
                    _tweenList[i].Dispose();
                    _tweenList.Remove(_tweenList[i]);
                }
            }

        }

        public void AddTween(GameObject resourceValue, float targetValue, float _startTime, float time,Color useCustomColor, GameTweenType tweenType = GameTweenType.SUB)
        {
            _tweenList.Add(new GameTween(resourceValue, targetValue, _startTime,time,useCustomColor, tweenType));
        }



    }
}
