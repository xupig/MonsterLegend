﻿

/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/5/20 9:53:53 
 * function: 
 * *******************************************************/

using GameLoader.Utils;
using UnityEngine;
namespace Common.Utils
{
    public class GameMathUtils
    {
        //弧度转角度
        public static float Radian2Angle(float radian)
        {
            return radian * 180f / Mathf.PI;
        }

        //角度转弧度
        public static float Angle2Radian(float angle)
        {
            return angle * Mathf.PI / 180f;
        }
    }
}
