﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using GameData;
using GameLoader.Utils;
using ACTSystem;
using Common.Structs;
namespace Common.Data
{
    public class ModelEquipTools
    {
        public static AvatarModelData CreateAvatarModelData(int vocation)
        {
            var modelData = new AvatarModelData();
            modelData.vocation = vocation;
            ResetAvatarModelDataByDefault(modelData);
            return modelData;
        }

        public static AvatarModelData CreateAvatarModelData(int vocation, string equipInfoStr)
        { 
            var modelData = new AvatarModelData();
            modelData.vocation = vocation;
            ResetAvatarModelDataByDefault(modelData);
            ResetAvatarModelDataByString(modelData, equipInfoStr);
            return modelData;
        }

        public static AvatarModelData CreateNpcModelData(string equipInfoStr)
        {
            var modelData = new AvatarModelData();
            modelData.vocation = 0;
            ResetAvatarModelDataByString(modelData, equipInfoStr);
            return modelData;
        }
        
        public static string CalculateSubEquipInfoStr(ACTEquipmentType type, AvatarEquipInfo avatarEquipInfo)
        {
            string result = string.Concat(((int)type).ToString(), ":",
                                    avatarEquipInfo.equipID.ToString(), ",",
                                    avatarEquipInfo.particle.ToString(),",",
                                     avatarEquipInfo.flow.ToString(), ",",
                                    avatarEquipInfo.colorRID.ToString(), ",",
                                    avatarEquipInfo.colorGID.ToString(), ",",
                                    avatarEquipInfo.colorBID.ToString());
            return result;
        }

        public static string CalculateEquipInfoStr(AvatarModelData modelData)
        {
            string result =
                string.Concat(
                        CalculateSubEquipInfoStr(ACTEquipmentType.Cloth, modelData.equips[ACTEquipmentType.Cloth]), ";",
                        CalculateSubEquipInfoStr(ACTEquipmentType.Weapon, modelData.equips[ACTEquipmentType.Weapon]), ";",
                        CalculateSubEquipInfoStr(ACTEquipmentType.Wing, modelData.equips[ACTEquipmentType.Wing]), ";",
                        CalculateSubEquipInfoStr(ACTEquipmentType.Effect, modelData.equips[ACTEquipmentType.Effect]), ";",
                        CalculateSubEquipInfoStr(ACTEquipmentType.Head, modelData.equips[ACTEquipmentType.Head])
                            );
            return result;
        }

        public static void ResetAvatarModelDataByString(AvatarModelData modelData, string equipInfoStr)
        {//equipInfoStr格式  装备类型：装备ID，粒子ID，流光ID，颜色设置RID，颜色设置GID，颜色设置BID；装务类型：装备ID，粒子ID，流光ID，颜色设置RID，颜色设置GID，颜色设置BID；
            //例子   1:1010401,0,0,0,0,0;11:1010101,0,0,0,0,0;21:0,0,0,0,0,0
            try
            {
                if (string.IsNullOrEmpty(equipInfoStr)) return;
                var equipInfoStrings = equipInfoStr.Split(';');
                for (int i = 0; i < equipInfoStrings.Length; i++)
                {
                    var subStr = equipInfoStrings[i];
                    if (string.IsNullOrEmpty(subStr)) continue;
                    var keyValueStr = subStr.Split(':');
                    var valueStr = keyValueStr[1].Split(',');
                    var key = (ACTEquipmentType)int.Parse(keyValueStr[0]);
                    modelData.equips[key].equipID = int.Parse(valueStr[0]);
                    modelData.equips[key].particle = int.Parse(valueStr[1]);
                    modelData.equips[key].flow = int.Parse(valueStr[2]);
                    if (valueStr.Length > 3)
                    {
                        modelData.equips[key].colorRID = int.Parse(valueStr[3]);
                        modelData.equips[key].colorGID = int.Parse(valueStr[4]);
                        modelData.equips[key].colorBID = int.Parse(valueStr[5]);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("equipInfoStr:" + equipInfoStr + "\n" + ex.Message);
            }
        }

        public static void ResetAvatarModelDataByDefault(AvatarModelData modelData)
        {
            var curCloth = role_data_helper.GetDefaultCloth((int)modelData.vocation);
            modelData.equips[ACTEquipmentType.Cloth].equipID = curCloth;
            var curWeapon = role_data_helper.GetDefaultWeapon((int)modelData.vocation);
            modelData.equips[ACTEquipmentType.Weapon].equipID = curWeapon;
            var curHead = role_data_helper.GetDefaultHead((int)modelData.vocation);
            modelData.equips[ACTEquipmentType.Head].equipID = curHead;
            //var curHair = role_data_helper.GetDefaultHair((int)modelData.vocation);
            //modelData.equips[ACTEquipmentType.Hair].equipID = curHair;
            //var curDeputyWeapon = role_data_helper.GetDefaultDeputyWeapon((int)modelData.vocation);
            //modelData.equips[ACTEquipmentType.DeputyWeapon].equipID = curDeputyWeapon;
            var curWing = 0;
            modelData.equips[ACTEquipmentType.Wing].equipID = curWing;
        }

        public static void ResetActorByAvatarModelData(ACTActor actor, AvatarModelData avatarModelData)
        {
            var clothInfo = avatarModelData.GetEquipInfo(ACTEquipmentType.Cloth);
            actor.equipController.equipCloth.PutOn(clothInfo.equipID, clothInfo.particle, clothInfo.flow);
            actor.equipController.equipWeapon.PutOn(avatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).equipID);
            //actor.equipController.equipDeputyWeapon.PutOn(avatarModelData.GetEquipInfo(ACTEquipmentType.DeputyWeapon).equipID);
            actor.equipController.equipWing.PutOn(avatarModelData.GetEquipInfo(ACTEquipmentType.Wing).equipID);
            //Debug.Log(string.Format("<color=#ffff00>clothInfo.equipID : {0},clothInfo.particle: {1},</color>", clothInfo.equipID, clothInfo.particle));
        }
    }


}
