﻿namespace Common.Data
{
    public class ChatConst
    {
        public static readonly string STR_DAY = "天";
        public static readonly string STR_HOUR = "小时";
        public static readonly string STR_MINUTE = "分钟";
        public static readonly string STR_SECOND = "秒";
        public static readonly string SEND_LINK_TIME_LENGTH_TEMPLATE = "第{0}天";
        public static readonly string CHAT_QUESTION_TITLE = "【答题】";
        public static readonly string CHAT_QUESTION_END_TIPS = "(本轮答题已结束，敬请期待下轮)";

        public static readonly string IN_TEAM_TIPS_TEMPLATE = "处于队伍中，您现在可以$(1,【{0}】)";
        public static readonly string FOLLOW_CAPTAIN = "跟随战斗";
        public static readonly string CANCEL_FOLLOW_CAPTAIN = "取消跟随";
        public static readonly string CAPTAIN_CALL_ALL = "召唤全队";
        public static readonly string GO_TO_AUTO_MATCH_TEAM_TIPS = "自动匹配组队玩法$(1,【立刻前往】)";

        public static readonly string NOT_SEND_PRIVATE_MSG_TO_SELF = "$(6,不能和自己聊天)";

        public static readonly string ACCEPT_PLAYER_JOIN_TEAM = "中文ID10135";// MogoLanguageUtil.GetContent(10135);

        public static readonly string CHAT_SYSTEM_TITLE_WORLD = "中文ID32535";// MogoLanguageUtil.GetContent(32535);
        public static readonly string CHAT_SYSTEM_TITLE_VICINITY = "中文ID32536";// MogoLanguageUtil.GetContent(32536);
        public static readonly string CHAT_SYSTEM_TITLE_TEAM = "中文ID32538";// MogoLanguageUtil.GetContent(32538);
        public static readonly string CHAT_SYSTEM_TITLE_GUILD = "中文ID32537";// MogoLanguageUtil.GetContent(32537);
        public static readonly string CHAT_SYSTEM_TITLE_PRIVATE = "中文ID32539";// MogoLanguageUtil.GetContent(32539);
        public static readonly string CHAT_SYSTEM_TITLE_SYSTEM = "中文ID32540";// MogoLanguageUtil.GetContent(32540);
        public static readonly string CHAT_SYSTEM_TITLE_TIPS = "中文ID32541";// MogoLanguageUtil.GetContent(32541);
        public static readonly string CHAT_SYSTEM_TITLE_QUESTION = "中文ID123008";// MogoLanguageUtil.GetContent(123008);


        //by lhs
        public static readonly int MAX_CHAT_ITEM_COUNT = 30;
    }
}
