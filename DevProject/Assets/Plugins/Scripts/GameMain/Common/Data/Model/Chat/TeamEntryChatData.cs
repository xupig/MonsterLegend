﻿using System;
using System.Collections.Generic;
using Common.ServerConfig;

namespace Common.Data
{
    public class TeamEntryChatData
    {
        private List<PbChatInfoResp> _teamChatInfoList;

        public List<PbChatInfoResp> TeamChatInfoList
        {
            get { return _teamChatInfoList; }
        }

        public TeamEntryChatData()
        {
            _teamChatInfoList = new List<PbChatInfoResp>();
        }

        public void AddTeamChatInfo(PbChatInfoResp chatInfo)
        {
            if (chatInfo.channel_id != public_config.CHANNEL_ID_TEAM)
                return;
            _teamChatInfoList.Add(chatInfo);
            if (_teamChatInfoList.Count > ChatConst.MAX_CHAT_ITEM_COUNT)
            {
                _teamChatInfoList.RemoveAt(0);
            }
        }
    }
}
