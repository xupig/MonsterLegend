﻿using System;
using System.Collections.Generic;
using UnityEngine;
using GameLoader.IO;
using GameLoader.Utils;
using GameLoader.Config;
using System.Text;
using System.Text.RegularExpressions;

namespace Common.Data
{
    public class FilterWordsData
    {
        private const string DATA_FILTER_WORDS = "data/filter_words/";
        private const string FILTER_STAR = "*";
        private const string FILTER_WITH_COLOR_START_TAG = "<color=#f22424>";
        private const string FILTER_WITH_COLOR_END_TAG = "</color>";
        private const string FILTER_SPECIAL_WORD_START_TAG = "ScolorF";
        private const string FILTER_SPECIAL_WORD_END_TAG = "EcolorF";

        private Dictionary<string, List<string>> _filterWordsDict = new Dictionary<string, List<string>>();
        private List<Regex> _regexWordList = new List<Regex>();
        private static readonly string SPECIAL_WORDS = "~!@#$%^&*()_+-=[]{};'\\:\"|,./<>?";

        private static FilterWordsData _instance;
        public static FilterWordsData Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new FilterWordsData();
                }
                return _instance;
            }
        }

        protected FilterWordsData()
        {
            Init();
        }

        private void Init()
        {
            LoadData("stopword_sub.txt", InitFilterDict);
            LoadData("stopword_re.txt", InitFilterRegexList);
        }

        private void LoadData(string filtersName, Action<string> initDataAction)
        {
            string strContent = string.Empty;
            switch (UnityPropUtils.Platform)
            {
                case RuntimePlatform.Android:
                case RuntimePlatform.IPhonePlayer:
                //case RuntimePlatform.WindowsWebPlayer://为排除警告
                //    strContent = LoadFilterWordsDataAtAndroid("发布模式", filtersName);
                //    break;
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.WindowsPlayer:
                    if (GameLoader.SystemConfig.ReleaseMode)
                    {
                        strContent = LoadFilterWordsDataAtAndroid("开发模式下--发布模式", filtersName);
                    }
                    else
                    {
                        string path = string.Concat(GameLoader.SystemConfig.ResPath, DATA_FILTER_WORDS, filtersName);
                        LoggerHelper.Info(string.Format("[开发模式下][LoadFilterWordsData] 加载文件:{0},{1}", path, filtersName));
                        //strContent = path.LoadFile();
                        strContent = FileUtils.LoadFile(path);
                    }
                    break;
            }
            if (initDataAction != null)
            {
                initDataAction(strContent);
            }
        }

        private string LoadFilterWordsDataAtAndroid(string logTitle, string filtersName)
        {
            string fileName = string.Concat(DATA_FILTER_WORDS, filtersName);
            bool isExistFile = FileAccessManager.IsFileExist(fileName);
            LoggerHelper.Info(string.Format("[{0}][LoadFilterWordsData] 文件:{1},{2}", logTitle, fileName, isExistFile ? "存在" : "找不到"));
            return isExistFile ? FileAccessManager.LoadText(fileName) : null;
        }

        private void InitFilterDict(string strContent)
        {
            if (string.IsNullOrEmpty(strContent))
            {
                LoggerHelper.Error("InitFilterDict strContent is empty!!!!!");
                return;
            }
            string[] strSplit = strContent.Split(',');
            List<string> wordList = null;
            for (int i = 0; i < strSplit.Length; i++)
            {
                string word = strSplit[i];
                if (!string.IsNullOrEmpty(word))
                {
                    string strKey = word.Substring(0, 1);
                    if (_filterWordsDict.ContainsKey(strKey))
                    {
                        wordList = _filterWordsDict[strKey];
                        if (word.Length > 1)
                        {
                            string strValue = word.Substring(1, word.Length - 1);
                            wordList.Add(strValue);
                        }
                    }
                    else
                    {
                        wordList = new List<string>();
                        _filterWordsDict.Add(strKey, wordList);
                        if (word.Length > 1)
                        {
                            string strValue = word.Substring(1, word.Length - 1);
                            wordList.Add(strValue);
                        }
                    }
                }
            }
        }

        private void InitFilterRegexList(string strContent)
        {
            string[] strSplits = strContent.Split(new char[] { '\n' });
            string needReplaceOldStr = "+";
            string replacedStr = "*?";
            //最后一个是空字符串
            for (int i = 0; i < strSplits.Length - 1; i++)
            {
                string strRegex = strSplits[i].TrimEnd();
                strRegex = strRegex.Replace(needReplaceOldStr, replacedStr);
                Regex pattern = new Regex(strRegex);
                _regexWordList.Add(pattern);
            }
        }

        public string FilterContent(string msg)
        {
            return FilterContent(msg, FilterWordsType.ReplacedWithStar, (uint)FilterWordsRangeType.ShieldWords);
        }

        public string FilterContent(string msg, FilterWordsType filterType, uint filterWordsRange)
        {
            //要先将特殊符号过滤了
            if (((uint)FilterWordsRangeType.SpecialWords & filterWordsRange) != 0)
            {
                msg = StopSpecialWords(msg, filterType);
            }
            if (((uint)FilterWordsRangeType.ShieldWords & filterWordsRange) != 0)
            {
                msg = StopShieldWords(msg, filterType);
            }
            if (((uint)FilterWordsRangeType.RegexWords & filterWordsRange) != 0)
            {
                msg = StopRegexWords(msg, filterType);
            }
            return msg;
        }

        private string StopShieldWords(string msg, FilterWordsType filterType)
        {
            foreach (var item in _filterWordsDict)
            {
                string key = item.Key;
                if (msg.Contains(key))
                {
                    List<string> wordList = item.Value;
                    for (int i = 0; i < wordList.Count; i++)
                    {
                        string filterWord = wordList[i];
                        filterWord = filterWord.Insert(0, key);
                        if (msg.Contains(filterWord))
                        {
                            switch (filterType)
                            {
                                case FilterWordsType.ReplacedWithStar:
                                    msg = msg.Replace(filterWord, FILTER_STAR);
                                    break;
                                case FilterWordsType.ChangedWithRedColor:
                                    msg = FilterContentWithRedColor(msg, filterWord);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            return msg;
        }

        private string FilterContentWithRedColor(string msg, string filterWord)
        {
            string filterColorWord = AddTagsInFilterWord(filterWord, FILTER_WITH_COLOR_START_TAG, FILTER_WITH_COLOR_END_TAG);
            msg = msg.Replace(filterWord, filterColorWord);
            return msg;
        }

        private string AddTagsInFilterWord(string filterWord, string startTag, string endTag)
        {
            string destWord = filterWord;
            destWord = destWord.Insert(0, startTag);
            destWord = destWord.Insert(destWord.Length, endTag);
            return destWord;
        }

        private string StopRegexWords(string msg, FilterWordsType filterType)
        {
            List<string> hasReplacedWords = null;
            for (int i = 0; i < _regexWordList.Count; i++)
            {
                MatchCollection matchWordCollection = _regexWordList[i].Matches(msg);
                int count = matchWordCollection.Count;
                for (int j = 0; j < count; j++)
                {
                    string matchWord = matchWordCollection[j].Value;
                    if (!string.IsNullOrEmpty(matchWord))
                    {
                        switch (filterType)
                        {
                            case FilterWordsType.ReplacedWithStar:
                                msg = msg.Replace(matchWord, FILTER_STAR);
                                break;
                            case FilterWordsType.ChangedWithRedColor:
                                {
                                    if (hasReplacedWords == null)
                                    {
                                        hasReplacedWords = new List<string>();
                                    }
                                    if (!hasReplacedWords.Contains(matchWord))
                                    {
                                        msg = FilterContentWithRedColor(msg, matchWord);
                                        hasReplacedWords.Add(matchWord);
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            return msg;
        }

        private string StopSpecialWords(string msg, FilterWordsType filterType)
        {
            for (int i = 0; i < SPECIAL_WORDS.Length; i++)
            {
                string word = SPECIAL_WORDS[i].ToString();
                if (word == FILTER_STAR && filterType == FilterWordsType.ReplacedWithStar)
                    continue;
                if (msg.Contains(word))
                {
                    switch (filterType)
                    {
                        case FilterWordsType.ReplacedWithStar:
                            msg = msg.Replace(word, FILTER_STAR);
                            break;
                        case FilterWordsType.ChangedWithRedColor:
                            string destFilterWord = AddTagsInFilterWord(word, FILTER_SPECIAL_WORD_START_TAG, FILTER_SPECIAL_WORD_END_TAG);
                            msg = msg.Replace(word, destFilterWord);
                            break;
                        default:
                            break;
                    }
                }
            }
            if (filterType == FilterWordsType.ChangedWithRedColor)
            {
                msg = msg.Replace(FILTER_SPECIAL_WORD_START_TAG, FILTER_WITH_COLOR_START_TAG);
                msg = msg.Replace(FILTER_SPECIAL_WORD_END_TAG, FILTER_WITH_COLOR_END_TAG);
            }
            return msg;
        }
    }

    public enum FilterWordsType
    {
        ReplacedWithStar = 0,
        ChangedWithRedColor
    }

    public enum FilterWordsRangeType
    {
        ShieldWords = 2,
        RegexWords = 4,
        SpecialWords = 8
    }
}
