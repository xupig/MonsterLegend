﻿using System;
using System.Collections.Generic;
using GameData;

namespace Common.Data
{
    public class ChatShowData
    {
        private List<ShowLinkItemData> _showItemList = new List<ShowLinkItemData>();
        private Dictionary<int, bool> _functionOpenDict = new Dictionary<int, bool>();
        private List<int> _notOpenShowIdList = new List<int>();

        public ChatShowData()
        {

            /*foreach (KeyValuePair<int, chat_show> item in XMLManager.chat_show)
            {
                int id = item.Value.__id;
                if (chat_show_helper.IsShowFunctionOpen(id))
                {
                    ShowLinkItemData itemData = CreateShowItemData(id);
                    _showItemList.Add(itemData);
                }
                else
                {
                    _notOpenShowIdList.Add(id);
                }
            }*/
            _showItemList.Sort(SortShowItemList);
        }

        private ShowLinkItemData CreateShowItemData(int showId)
        {
            ShowLinkItemData itemData = new ShowLinkItemData(showId);
            return itemData;
        }

        private int SortShowItemList(ShowLinkItemData a, ShowLinkItemData b)
        {
            return a.Order - b.Order;
        }

        public List<ShowLinkItemData> ShowItemList
        {
            get
            {
                if (_notOpenShowIdList.Count > 0)
                {
                    List<int> openedIdList = new List<int>();
                    for (int i = 0; i < _notOpenShowIdList.Count; i++)
                    {
                        /*int id = _notOpenShowIdList[i];
                        if (chat_show_helper.IsShowFunctionOpen(id))
                        {
                            ShowLinkItemData itemData = CreateShowItemData(id);
                            _showItemList.Add(itemData);
                            openedIdList.Add(id);
                        }*/
                    }
                    for (int i = 0; i < openedIdList.Count; i++)
                    {
                        _notOpenShowIdList.Remove(openedIdList[i]);
                    }
                    if (openedIdList.Count > 0)
                    {
                        _showItemList.Sort(SortShowItemList);
                    }
                }
                return _showItemList;
            }
        }
    }

    public class ShowLinkItemData
    {
        private int _iconId = 0;
        private int _order = 0;
        private int _functionId = 0;

        public ShowLinkItemData(int id)
        {
            GameLoader.Utils.LoggerHelper.Error("ShowLinkItemData");
            Id = id;
            /*chat_show showInfo = chat_show_helper.GetConfig(id);
            if (showInfo != null)
            {
                _iconId = function_helper.GetIconId(showInfo.__function_id);
                _functionId = showInfo.__function_id;
                _order = showInfo.__order;
            }*/
        }

        public int Id
        {
            get;
            private set;
        }

        public int IconId
        {
            get { return _iconId; }
        }

        public int Order
        {
            get { return _order; }
        }

        public int FunctionId
        {
            get { return _functionId; }
        }

    }
}