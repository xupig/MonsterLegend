﻿using Common.ClientConfig;
using Common.Events;
using Common.Utils;
using GameMain;
using GameMain.GlobalManager;
using MogoEngine.Events;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseResponse : MonoBehaviour
{
    private const float DOUBLE_CLICK_TIME_SPAN = 0.4f;
    private float _touchTimeStamp = 0;
    private Vector3 _touchPosition;
    private float _doubleTouchDistance = 100f;
    private bool _isTouchingUI = false;
    private Vector3 _firstMousePosition = Vector3.zero;
    private int _inputButton = 0;
    private int _inverseInputButton = 1;

    void Update()
    {
        _inputButton = (int)KeyboardManager.GetInstance().MouseClickMode;
        _inverseInputButton = 1 - _inputButton;

        ProcessTouchingUI();
        RaycastHit hit;
        if (Input.GetMouseButtonUp(_inputButton))
        {
            EventDispatcher.TriggerEvent<Vector3>(TouchEvents.ON_TOUCH_SCREEN, Input.mousePosition);
            if (CameraManager.GetInstance().Camera == null || _isTouchingUI)
            {
                ReleaseTouchingUI();
                return;
            }

            if (CheckRotateRange())
            {
                if (Input.mousePosition.y > _firstMousePosition.y)
                {
                    //PlayerRideManager.Instance.RequestFly();
                }
                else
                {
                    //PlayerRideManager.Instance.RequestLand();
                }
            }
            //hit = GetRaycastHit(UnityEngine.LayerMask.NameToLayer("Trigger"));
            hit = GetRaycastHitFix();
            if (hit.transform != null)
            {
                var actorSelectTrigger = hit.transform.GetComponent<ActorSelectTrigger>();
                if (actorSelectTrigger != null)
                {
                    actorSelectTrigger.OnMouseButtonUp();
                }
            }
            else
            {
                if (_inputButton == (int)MouseClickMode.LoLMode)
                {
                    OnTouchBlankSpace();
                }
            }
        }
        else if (Input.GetMouseButtonDown(_inputButton))
        {
            _firstMousePosition = Input.mousePosition;
        }
        else if (Input.GetMouseButton(_inputButton))
        {
            if (!CheckRotateRange())
            {
                _firstMousePosition.Set(0, 0, 0);
            }
        }
        else if (Input.GetMouseButton(_inverseInputButton))
		{
            if (_isTouchingUI)
            {
                ReleaseTouchingUI();
                return;
            }
            hit = GetRaycastHit(ACTSystem.ACTSystemTools.NameToLayer("Terrain"));
            if (hit.transform != null)
            {
                EventDispatcher.TriggerEvent<Vector3>(TouchEvents.ON_TOUCH_TERRAIN, hit.point);
            }
        }
        ReleaseTouchingUI();
    }

    private bool CheckRotateRange()
    {
        if (_firstMousePosition == Vector3.zero)
        {
            return false;
        }
        float distance = Vector3.Distance(Input.mousePosition, _firstMousePosition);
        float xDistance = ACTSystem.ACTMathUtils.Abs(Input.mousePosition.x - _firstMousePosition.x);
        float sinAngle = xDistance / distance;
        return sinAngle <= 0.5f;
    }

    private RaycastHit GetRaycastHit(int layer)
    {
        return GameRayUtils.MouseRaycastFromCamera(100, 1 << layer);
    }

    private RaycastHit GetRaycastHitFix()
    {
        int layerMask = (1 << ACTSystem.ACTSystemTools.NameToLayer(PhysicsLayerDefine.Trigger)) + (1 << ACTSystem.ACTSystemTools.NameToLayer(PhysicsLayerDefine.Monster))
                + (1 << ACTSystem.ACTSystemTools.NameToLayer(PhysicsLayerDefine.Avatar)) + (1 << ACTSystem.ACTSystemTools.NameToLayer(PhysicsLayerDefine.TransparentFX));
        return GameRayUtils.MouseRaycastFromCamera(100, layerMask);
    }

    private bool CheckMouseLeftTouchScreen()
    {
        return Input.GetMouseButtonUp(_inputButton);
    }

    private void OnTouchBlankSpace()
    {
        EventDispatcher.TriggerEvent(TouchEvents.ON_TOUCH_BLANK_SPACE);
        var now = UnityPropUtils.realtimeSinceStartup;
        Vector3 touchPosition;
        touchPosition = Input.mousePosition;
        if (_touchTimeStamp > 0)
        {
            if (now - _touchTimeStamp <= DOUBLE_CLICK_TIME_SPAN)
            {
                if (CheckTouchPosition(touchPosition))
                {
                    OnDoubleTouchBlankSpace();
                    _touchTimeStamp = 0;
                    _touchPosition = Vector3.zero;
                    return;
                }
            }
        }
        _touchTimeStamp = now;
        _touchPosition = touchPosition;
    }

    private bool CheckTouchPosition(Vector3 touchPosition)
    {
        return Vector3.Distance(touchPosition, _touchPosition) < _doubleTouchDistance;
    }

    private void OnDoubleTouchBlankSpace()
    {
        EventDispatcher.TriggerEvent(TouchEvents.ON_DOUBLE_TOUCH_BLANK_SPACE);
        CameraManager.GetInstance().OnDoubleTouchBlankSpace();
    }

    private void ProcessTouchingUI()
    {
        if (Input.GetMouseButtonDown(0))
        {
			_isTouchingUI = IsMouseOverUI();
        }
    }

    private void ReleaseTouchingUI()
    {
        if (Input.GetMouseButtonUp(0))
        {
			_isTouchingUI = false;
        }
    }

    private bool IsMouseOverUI()
    {
        bool isPointerOverGo = EventSystem.current.IsPointerOverGameObject();
        return isPointerOverGo;
    }
}