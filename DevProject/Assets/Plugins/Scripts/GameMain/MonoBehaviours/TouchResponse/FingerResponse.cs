﻿using Common.ClientConfig;
using Common.Events;
using Common.States;
using Common.Utils;
using GameMain.GlobalManager;
using MogoEngine.Events;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameMain
{
    public class FingerResponse : MonoBehaviour
    {
        private const float DOUBLE_CLICK_TIME_SPAN = 0.4f;
        private float _touchTimeStamp = 0;
        private Touch _touch;
        private float _doubleTouchDistance = 100f;
        private bool _isTouchingUI = false;
        private Vector3 _firstTouchPosition = Vector3.zero;
        private Vector3 _touchPosition = Vector3.zero;

        void Update()
        {
            ProcessTouchingUI();
            if (CheckTouchCount())
            {
                if (_touch.phase == TouchPhase.Ended)
                {
                    _touchPosition = new Vector3(_touch.position.x, _touch.position.y, 0);
                    EventDispatcher.TriggerEvent<Vector3>(TouchEvents.ON_TOUCH_SCREEN, _touchPosition);

                    if (CameraManager.GetInstance().Camera == null || _isTouchingUI)
                    {
                        ReleaseTouchingUI();
                        return;
                    }

                    if (CheckRotateRange())
                    {
                        if (_touch.position.y > _firstTouchPosition.y)
                        {
                            //PlayerRideManager.Instance.RequestFly();
                        }
                        else
                        {
                            //PlayerRideManager.Instance.RequestLand();
                        }
                    }

                    //RaycastHit hit = GetRaycastHit(UnityEngine.LayerMask.NameToLayer("Trigger"));
                    RaycastHit hit = GetRaycastHitFix();
                    if (hit.transform != null)
                    {
                        var actorSelectTrigger = hit.transform.GetComponent<ActorSelectTrigger>();
                        if (actorSelectTrigger != null)
                        {
                            actorSelectTrigger.OnMouseButtonUp();
                        }
                    }
                    else
                    {
                        hit = GetRaycastHit(ACTSystem.ACTSystemTools.NameToLayer("Terrain"));
                        if (hit.transform != null)
                        {
                            EventDispatcher.TriggerEvent<Vector3>(TouchEvents.ON_TOUCH_TERRAIN, hit.point);
                        }
                        OnTouchBlankSpace();
                    }
                }

                else if (_touch.phase == TouchPhase.Began)
                {
                    _firstTouchPosition.Set(_touch.position.x, _touch.position.y, 0);
                }
                else if (Input.GetMouseButton(0))
                {
                    if (!CheckRotateRange())
                    {
                        _firstTouchPosition.Set(0, 0, 0);
                    }
                }
            }
            ReleaseTouchingUI();
        }

        private bool CheckRotateRange()
        {
            if (_firstTouchPosition == Vector3.zero)
            {
                return false;
            }
            float distance = Vector3.Distance(new Vector3(_touch.position.x, _touch.position.y, 0), _firstTouchPosition);
            float xDistance = ACTSystem.ACTMathUtils.Abs(_touch.position.x - _firstTouchPosition.x);
            float sinAngle = xDistance / distance;
            return sinAngle <= 0.5f;
        }

        private RaycastHit GetRaycastHit(int layer)
        {
            return GameRayUtils.TouchRaycastFromCamera(new Vector3(_touch.position.x, _touch.position.y, 0), 100, 1 << layer);
        }

        private RaycastHit GetRaycastHitFix()
        {
            int layerMask = (1 << ACTSystem.ACTSystemTools.NameToLayer(PhysicsLayerDefine.Trigger)) + (1 << ACTSystem.ACTSystemTools.NameToLayer(PhysicsLayerDefine.Monster))
                + (1 << ACTSystem.ACTSystemTools.NameToLayer(PhysicsLayerDefine.Avatar)) + (1 << ACTSystem.ACTSystemTools.NameToLayer(PhysicsLayerDefine.TransparentFX));
            return GameRayUtils.MouseRaycastFromCamera(100, layerMask);
        }

        private bool CheckTouchCount()
        {
            int count = Input.touchCount;
            if (count > 2 || count == 0)
            {
                return false;
            }
            int fingerId = ControlStickState.fingerId;
            Touch[] touches = Input.touches;
            bool result = false;
            if (count == 1)
            {
                if (touches[0].fingerId != fingerId)
                {
                    _touch = touches[0];
                    result = true;
                }
            }
            else
            {
                if (touches[0].fingerId == fingerId)
                {
                    _touch = touches[1];
                    result = true;
                }
                else if (touches[1].fingerId == fingerId)
                {
                    _touch = touches[0];
                    result = true;
                }
            }
            return result;
        }

        private void OnTouchBlankSpace()
        {
            EventDispatcher.TriggerEvent(TouchEvents.ON_TOUCH_BLANK_SPACE);
            var now = UnityPropUtils.realtimeSinceStartup;
            Vector3 touchPosition;
            touchPosition = _touchPosition;
            if (_touchTimeStamp > 0)
            {
                if (now - _touchTimeStamp <= DOUBLE_CLICK_TIME_SPAN)
                {
                    if (CheckTouchPosition(touchPosition))
                    {
                        OnDoubleTouchBlankSpace();
                        _touchTimeStamp = 0;
                        _touchPosition = Vector3.zero;
                        return;
                    }
                }
            }
            _touchTimeStamp = now;
            _touchPosition = touchPosition;
        }

        private bool CheckTouchPosition(Vector3 touchPosition)
        {
            return Vector3.Distance(touchPosition, _touchPosition) < _doubleTouchDistance;
        }

        private void OnDoubleTouchBlankSpace()
        {
            EventDispatcher.TriggerEvent(TouchEvents.ON_DOUBLE_TOUCH_BLANK_SPACE);
            CameraManager.GetInstance().OnDoubleTouchBlankSpace();
        }

        private void ProcessTouchingUI()
        {
            if (Input.touchCount > 0)
            {
                int fingerId = ControlStickState.fingerId;
                Touch touch;
                if (Input.touchCount > 1)
                {
                    for (int i = 0; i < Input.touchCount; i++)
                    {
                        touch = Input.GetTouch(i);
                        if (touch.fingerId == fingerId)
                            continue;
                        if (touch.phase == TouchPhase.Began)
                        {
                            if (_isTouchingUI)
                                break;
                            _isTouchingUI = IsTouchOverUI(i);
                            break;
                        }
                    }
                }
                else
                {
                    touch = Input.GetTouch(0);
                    if (touch.fingerId != fingerId && touch.phase == TouchPhase.Began)
                    {
                        _isTouchingUI = IsTouchOverUI(0);
                    }
                }
            }
        }

        private void ReleaseTouchingUI()
        {
            if (Input.touchCount > 0)
            {
                if (_isTouchingUI)
                {
                    int endedCount = 0;
                    int touchCount = 0;
                    int fingerId = ControlStickState.fingerId;
                    for (int i = 0; i < Input.touchCount; i++)
                    {
                        if (Input.GetTouch(i).fingerId == fingerId)
                            continue;
                        touchCount++;
                        if (Input.GetTouch(i).phase == TouchPhase.Ended)
                        {
                            endedCount++;
                        }
                    }
                    if (endedCount == touchCount)
                    {
                        _isTouchingUI = false;
                    }
                }
            }
        }

        private bool IsTouchOverUI(int touchIndex)
        {
            bool isTouch = false;
            if (Input.touchCount > 0 && EventSystem.current.IsPointerOverGameObject(Input.GetTouch(touchIndex).fingerId))
            {
                isTouch = true;
            }
            return isTouch;
        }
    }
}