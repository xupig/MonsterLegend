﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace GameMain
{
    public class LuaSpaceBox : LuaBehaviour
    {
        void OnTriggerEnter(Collider boxCollider)
        {
            var currEntity = ACTSystemAdapter.GetOwner(boxCollider.gameObject.GetComponent<ACTSystem.ACTActor>());
            if (currEntity != null) CallFunction("OnTriggerEnter", currEntity.id);
        }

        void OnTriggerExit(Collider boxCollider)
        {
            var currEntity = ACTSystemAdapter.GetOwner(boxCollider.gameObject.GetComponent<ACTSystem.ACTActor>());
            if (currEntity != null) CallFunction("OnTriggerExit", currEntity.id);
        }

        public void CreateBoxCollider(Vector3 size, Vector3 rotation)
        {
            gameObject.transform.localEulerAngles = rotation;
            var box = gameObject.AddComponent<BoxCollider>();
            box.size = size;
            box.isTrigger = true;
        }
    }
}