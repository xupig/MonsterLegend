﻿using GameMain.GlobalManager;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/10 10:38:22 
 * function: 
 * *******************************************************/
public class CameraLookAt : CameraAction
{
    public Transform target;
    public float rotationDuration = 0;
    public float rotationMinSpeed = 0;
    public Vector3 targetPosition = -1 * Vector3.one;
    public float positionDuration = 0;

    protected override void Run()
    {
        CameraManager.GetInstance().ChangeToTargetPositionMotion(target, rotationDuration, rotationMinSpeed, targetPosition, positionDuration);
    }

    // int id = 0;//为排除警告，注释掉
    bool showGM = false;
    string cmd = "";
  /*  public void OnGUI()
    {
        if (UnityPropUtils.Platform == RuntimePlatform.WindowsPlayer)
        {
            return;
        }
        //if (GUI.Button(new Rect(150, 0, 50, 30), "跳转"))
        //{
        //    UnityEngine.Transform target = CameraTargetProxy.instance.transform;
        //    System.Collections.Generic.List<float> globalParams = GameData.data_parse_helper.ParseListFloat(GameData.global_params_helper.GetGlobalParam(76).Split(','));
        //    float distance = globalParams[0];
        //    GameMain.GlobalManager.CameraManager.GetInstance().ChangeToRotationTargetMotion(target, new UnityEngine.Vector3(globalParams[1], -80, 0), rotationDuration, rotationMinSpeed, distance, 0);
        //    GameMain.GlobalManager.CameraManager.GetInstance().ChangeCameraFov((int)globalParams[3]);
        //    GameMain.EntityPlayer.Player.Teleport(new Vector3(3452.0f, 114.4f, 1706.1f));
        //    //GameMain.GlobalManager.CameraManager.GetInstance().mainCamera.screenBloom.enabled = true;
        //}
        //if (GUI.Button(new Rect(200, 50, 50, 30), "ddd"))
        //{
        //    GameMain.EntityPlayer.Player.GlideDeath();
        //}
        //if (GUI.Button(new Rect(110, 0, 50, 30), "cross"))
        //{
        //    GameMain.EntityPlayer.Player.RpcCall("arena_point_action_req", 501, "");
        //}
        //if (GUI.Button(new Rect(170, 0, 50, 30), "quit"))
        //{
        //    GameMain.EntityPlayer.Player.RpcCall("arena_point_action_req", 503, "");
        //}
        if (GUI.Button(new Rect(100, 0, 50, 30), "GM"))
        {
            showGM = true;
        }       
        if (showGM)
        {
            cmd = GUI.TextArea(new Rect(160, 0, 150, 30), cmd);
            if (GUI.Button(new Rect(160, 35, 50, 30), "RUN"))
            {
                if (cmd.Trim() == "c")
                {
                    CameraManager.GetInstance().isOpenLock = false;
                    return;
                }
                if (cmd.Trim() == "s")
                {
                    GameMain.CombatSystem.DrawRangeTools.ShowDebugRange = !GameMain.CombatSystem.DrawRangeTools.ShowDebugRange;
                    return;
                }
                string c = "1," + cmd.Trim();
                GameMain.EntityPlayer.Player.RpcCall("chat_action_req", 351, c);
            }
            if (GUI.Button(new Rect(240, 35, 50, 30), "CLOSE"))
            {
                showGM = false;
            }
        }
    }
    */
    public void Callback()
    {

    }
}
