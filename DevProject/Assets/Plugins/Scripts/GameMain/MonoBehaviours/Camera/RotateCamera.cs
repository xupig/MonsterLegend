﻿using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.RPC;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/10 10:38:10 
 * function: 
 * *******************************************************/
public class RotateCamera : CameraAction
{
    public Transform target;
    public Vector3 rotation = Vector3.zero;
    public float distance = -1;
    public float rotationDuration = 0;
    public float rotationAccelerateRate = 0;
    public float distanceDuration = 0;

    protected override void Run()
    {
        CameraManager.GetInstance().ChangeToRotationTargetMotion(target, rotation, rotationDuration, rotationAccelerateRate, distance, distanceDuration);
    }
}
