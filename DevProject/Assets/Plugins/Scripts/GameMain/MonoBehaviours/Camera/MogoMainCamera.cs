﻿using GameData;
using GameLoader.Utils;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using UnityEngine;

public class MogoMainCamera : MonoBehaviour
{
    public Vector3 CURRENT_ROTATION_NEW
    {
        get 
        {
            return cameraCom.transform.localEulerAngles;
        }
    }

    public float CURRENT_DISTANCE_NEW
    {
        get
        {
            return cameraMotion.data.targetDistance;
        }
    }

    public static Vector3 LAST_ROTATION_TARGET_ROTATION = -1 * Vector3.one;
    public static Vector3 CURRENT_ROTATION = -1 * Vector3.one;

    //跟踪目标
    public Transform target;
    private Camera _camera;
    
    public Camera cameraCom
    {
        get { return _camera; }
    }

#if !UNITY_IPHONE
    [SerializeField, SetProperty("height")]
#endif
    private float _height = 0f; //相机偏移焦点高度设置
    public float height
    {
        get { return _height; }
        set 
        {
            _height = value;
            _cameraMotion.data.height = _height;
        }
    }
    //距离目标
#if !UNITY_IPHONE
    [SerializeField, SetProperty("distance")]
#endif
    private float _distance = 11f;
    public float distance
    {
        get { return _distance; }
        set 
        {
            _distance = value;
            if (editorModification)
            {
                _cameraMotion.data.targetDistance = _distance;
                _cameraMotion.Start();
            }
        }
    }

    //角度
#if !UNITY_IPHONE
    [SerializeField, SetProperty("rotationX")]
#endif
    private float _rotationX = 45f;
    public float rotationX
    {
        get { return _rotationX; }
        set
        {
            _rotationX = value;
            if (editorModification)
            {
                _cameraMotion.data.targetRotation.x = _rotationX;
                _cameraMotion.Start();
            }
        }
    }

    //角度
#if !UNITY_IPHONE
    [SerializeField, SetProperty("rotationY")]
#endif
    private float _rotationY = 0f;
    public float rotationY
    {
        get { return _rotationY; }
        set
        {
            _rotationY = value;
            if (editorModification)
            {
                _cameraMotion.data.targetRotation.y = _rotationY;
                _cameraMotion.Start();
            }
        }
    }

    //角度
#if !UNITY_IPHONE
    [SerializeField, SetProperty("rotationZ")]
#endif
    private float _rotationZ = 0f;
    public float rotationZ
    {
        get { return _rotationZ; }
        set
        {
            _rotationZ = value;
            _cameraMotion.data.targetRotation.z = _rotationZ;
            if (editorModification)
            {
                _cameraMotion.data.targetRotation.z = _rotationZ;
                _cameraMotion.Start();
            }
        }
    }

    //编辑器调整开关
#if !UNITY_IPHONE
    [SerializeField, SetProperty("EditorModification")]
#endif
    private bool _editorModification = false;
    public bool editorModification
    {
        get { return _editorModification; }
        set
        {
            _editorModification = value;
        }
    }

    //角度（0为水平,90俯视）
    //public float rotationX = 40.0f;
    //public float rotationY = 135.0f;
    //public float rotationZ = 0f;
    public float rotationMinSpeed = 0f;

    private CameraAnimData _shakeData;
    private float _shakeStartTime;
    private float _shakeDuration;
    private int _shakeCount;
    public bool isShaking = false;

    private float _xSpeed;
    private float _xDelta;//跟轴同向
    private float _ySpeed;
    private float _yDelta;
    private float _zSpeed;
    private float _zDelta;
    private float dTime;
    private int _currPlayShakeId = 0;
    private List<CameraInfo> test = new List<CameraInfo>();
    private BaseCameraMotion _cameraMotion;
    public BaseCameraMotion cameraMotion { get { return _cameraMotion; } }

    public CameraAccordingMode accordingMode { get; set; }

    private HashSet<string> _withoutTestObjectTags = new HashSet<string>();
    public int defaultCullingMask = 0;
    void Awake()
    {
        _camera = gameObject.GetComponent<Camera>();
        defaultCullingMask = _camera.cullingMask;
        //_camera.depthTextureMode |= DepthTextureMode.Depth;
    }

    void Start()
    {
        //在此添加需要排除检测的物体,以后可能用layer处理更好
        _withoutTestObjectTags.Add("MainCamera");
        _withoutTestObjectTags.Add("terrain");
        _withoutTestObjectTags.Add("Player");

        //gameObject.AddComponent<PostEffectProxy>();
        AddTestScripts();
        //if (UnityPropUtils.IsEditor)InspectorSetting.InspectingTool.Inspect(new GameMain.Inspectors.InspectingCamera(this), gameObject);
    }

    void AddTestScripts()
    {
        gameObject.AddComponent<RotateCamera>();
        gameObject.AddComponent<MoveCamera>();
        gameObject.AddComponent<CameraLookAt>();

        PostEffectHandlerBase.Instance.InitAll(_camera);
        PostEffectHandlerBase.Instance.SetBloomEnabled(_camera, true);
        PostEffectHandlerBase.Instance.SetBloomThreshold(_camera, 2);
        PostEffectHandlerBase.Instance.SetBloomMaxBrightness(_camera, 2f);
        PostEffectHandlerBase.Instance.SetBloomSoftKnee(_camera, 0.05f);
        PostEffectHandlerBase.Instance.SetBloomRadius(_camera, 5.5f);
        PostEffectHandlerBase.Instance.SetBloomIntensity(_camera, 1f);
        //PostEffectHandlerBase.Instance.SetScreenDarkLayers(_camera, 1 << PhysicsLayerDefine.LAYER_ACTOR | 1 << PhysicsLayerDefine.LAYER_BLOOM);
    }

    public void ShowScreenDying(bool isShow)
    {
        //screenDying.enabled = isShow;
    }

    void OnDestroy()
    {
        _cameraMotion = null;
    }

    void LateUpdate()
    {
        if (!target)
        {
            return;
        }

        if (_cameraMotion != null && accordingMode == CameraAccordingMode.AccordingMotion)
        {
            _cameraMotion.OnUpdate();
        }
        if (accordingMode != CameraAccordingMode.AccordingMotion && !isShaking)
        {
            UpdateCamera();
        }
        if (isShaking)
        {
            DoShaking();
        }
        //if (_skyTransform != null)
        //{
        //    _skyTransform.position = transform.position;
        //}
        //if (isUpdateClipPlane)
        //{
        //    UpdateClipPlane();
        //}
        //if (isUpdateFog)
        //{
        //    UpdateFog();
        //}
    }

    void UpdateCamera()
    {
        gameObject.transform.position = new Vector3(0, 0, 0);
        gameObject.transform.localPosition = new Vector3(0, 0, 0);
    }

    #region 摄像机远近切渐变控制

    /// <summary>
    /// 是否进行ClipPlane渐变
    /// </summary>
    private bool isUpdateClipPlane;
    /// <summary>
    /// farClipPlane每秒渐变量
    /// </summary>
    private float farClipPlaneDeltaValue;
    /// <summary>
    /// nearClipPlane每秒渐变量
    /// </summary>
    private float nearClipPlaneDeltaValue;
    /// <summary>
    /// 目标farClipPlane
    /// </summary>
    private float targetFarClipPlane;
    /// <summary>
    /// 目标nearClipPlane
    /// </summary>
    private float targetNearClipPlane;
    /// <summary>
    /// farClipPlane值变化方向，用于判断渐变是否完成
    /// </summary>
    private int farClipPlaneDirection;
    /// <summary>
    /// nearClipPlane值变化方向，用于判断渐变是否完成
    /// </summary>
    private int nearClipPlaneDirection;

    private bool isFinishUpdateClipPlane;

    private void UpdateClipPlane()
    {
        if (_camera != null)
        {
            isFinishUpdateClipPlane = true;
            if (Math.Sign(targetFarClipPlane - _camera.farClipPlane) == farClipPlaneDirection)
            {
                _camera.farClipPlane += farClipPlaneDeltaValue * UnityPropUtils.deltaTime;
                isFinishUpdateClipPlane = false;
            }
            else
            {
                _camera.farClipPlane = targetFarClipPlane;
            }
            if (Math.Sign(targetNearClipPlane - _camera.nearClipPlane) == nearClipPlaneDirection)
            {
                _camera.nearClipPlane += nearClipPlaneDeltaValue * UnityPropUtils.deltaTime;
                isFinishUpdateClipPlane = false;
            }
            else
            {
                _camera.nearClipPlane = targetNearClipPlane;
            }
            if (isFinishUpdateClipPlane)
                isUpdateClipPlane = false;
        }
    }

    public void ChangeCameraFarClipPlane(float farClipPlane, float duration = 0)
    {
        targetFarClipPlane = farClipPlane;
        if (duration > 0)
        {
            farClipPlaneDeltaValue = (targetFarClipPlane - _camera.farClipPlane) / duration;
            farClipPlaneDirection = Math.Sign(farClipPlaneDeltaValue);
            isUpdateClipPlane = true;
        }
        else
        {
            if (_camera != null) _camera.farClipPlane = farClipPlane;
        }
    }

    public void ChangeCameraNearClipPlane(float nearClipPlane, float duration = 0)
    {
        targetNearClipPlane = nearClipPlane;
        if (duration > 0)
        {
            nearClipPlaneDeltaValue = (targetNearClipPlane - _camera.nearClipPlane) / duration;
            nearClipPlaneDirection = Math.Sign(nearClipPlaneDeltaValue);
            isUpdateClipPlane = true;
        }
        else
        {
            if (_camera != null) _camera.nearClipPlane = nearClipPlane;
        }
    }

    #endregion

    #region 雾效渐变控制

    /// <summary>
    /// 是否进行雾效渐变
    /// </summary>
    private bool isUpdateFog;
    /// <summary>
    /// fogStartDistance每秒渐变量
    /// </summary>
    private float fogStartDistanceDeltaValue;
    /// <summary>
    /// fogEndDistance每秒渐变量
    /// </summary>
    private float fogEndDistanceDeltaValue;
    /// <summary>
    /// 目标fogStartDistance
    /// </summary>
    private float targetFogStartDistance;
    /// <summary>
    /// 目标fogEndDistance
    /// </summary>
    private float targetFogEndDistance;
    /// <summary>
    /// fogStartDistance值变化方向，用于判断渐变是否完成
    /// </summary>
    private int fogStartDistanceDirection;
    /// <summary>
    /// fogEndDistance值变化方向，用于判断渐变是否完成
    /// </summary>
    private int fogEndDistanceDirection;

    private bool isFinishUpdateFog;

    private void UpdateFog()
    {
        if (_camera != null)
        {
            isFinishUpdateFog = true;
            if (Math.Sign(targetFogStartDistance - RenderSettings.fogStartDistance) == fogStartDistanceDirection)
            {
                RenderSettings.fogStartDistance += fogStartDistanceDeltaValue * UnityPropUtils.deltaTime;
                isFinishUpdateFog = false;
            }
            else
            {
                RenderSettings.fogStartDistance = targetFogStartDistance;
            }
            if (Math.Sign(targetFogEndDistance - RenderSettings.fogEndDistance) == fogEndDistanceDirection)
            {
                RenderSettings.fogEndDistance += fogEndDistanceDeltaValue * UnityPropUtils.deltaTime;
                isFinishUpdateFog = false;
            }
            else
            {
                RenderSettings.fogEndDistance = targetFogEndDistance;
            }
            if (isFinishUpdateFog)
                isUpdateFog = false;
        }
    }

    public void ChangeFogStartDistance(float fogStartDistance, float duration = 0)
    {
        targetFogStartDistance = fogStartDistance;
        if (duration > 0)
        {
            fogStartDistanceDeltaValue = (targetFogStartDistance - RenderSettings.fogStartDistance) / duration;
            fogStartDistanceDirection = Math.Sign(fogStartDistanceDeltaValue);
            isUpdateFog = true;
        }
        else
        {
            RenderSettings.fogStartDistance = fogStartDistance;
        }
    }

    public void ChangeFogEndDistance(float fogEndDistance, float duration = 0)
    {
        targetFogEndDistance = fogEndDistance;
        if (duration > 0)
        {
            fogEndDistanceDeltaValue = (targetFogEndDistance - RenderSettings.fogEndDistance) / duration;
            fogEndDistanceDirection = Math.Sign(fogEndDistanceDeltaValue);
            isUpdateFog = true;
        }
        else
        {
            RenderSettings.fogEndDistance = fogEndDistance;
        }
    }

    #endregion

    public void FlashCamera(float alpha)
    {
        PostEffectHandlerBase.Instance.SetScreenWhiteAlpha(_camera, alpha);
        PostEffectHandlerBase.Instance.SetScreenWhiteColor(_camera, Color.white);
        PostEffectHandlerBase.Instance.SetScreenWhiteEnabled(_camera, alpha > 0);
    }

    public void PlayScreenWhite(float start, float end, float duration)
    {
        PostEffectHandlerBase.Instance.PlayScreenWhite(_camera, start, end, duration, null, true);
    }

    public void BlackCamera(float alpha, float duration)
    {
        PostEffectHandlerBase.Instance.SetScreenWhiteColor(_camera, Color.black);
        PostEffectHandlerBase.Instance.SetScreenWhiteEnabled(_camera, true);
        ScreenWhiteParameter swp = new ScreenWhiteParameter();
        swp.Alpha = alpha;
        swp.Color = Color.black;
        PostEffectHandlerBase.Instance.PlayScreenWhite(_camera, null, swp, duration, ScreenWhiteFinish);
    }

    private void ScreenWhiteFinish()
    {
        PostEffectHandlerBase.Instance.SetScreenWhiteEnabled(_camera, false);
    }

    public void SetRadialBlurColorFul(bool state, float darkness = 0, float strength = 0.1f, float sharpness = 40f, float vignetteSize = 1)
    {
        PostEffectHandlerBase.Instance.SetRadialBlurEnabled(_camera, state);
        PostEffectHandlerBase.Instance.SetRadialBlurDarkness(_camera, darkness);
        PostEffectHandlerBase.Instance.SetRadialBlurStrength(_camera, strength);
        PostEffectHandlerBase.Instance.SetRadialBlurSharpness(_camera, sharpness);
        PostEffectHandlerBase.Instance.SetRadialBlurVignetteSize(_camera, vignetteSize);
    }

    void DoShaking()
    {
        dTime = UnityPropUtils.realtimeSinceStartup - _shakeStartTime;
        if (dTime > _shakeDuration || _shakeCount == 0)
        {
            ResetShakeInfo();
            return;
        }
        _shakeData.ySwing = _shakeData.ySwing * _shakeData.attenuateOddsY;
        _shakeData.xSwing = _shakeData.xSwing * _shakeData.attenuateOddsX;
        _shakeData.zSwing = _shakeData.zSwing * _shakeData.attenuateOddsZ;
        if (_shakeData.ySwing != 0 && _shakeData.ySwing <= 0.001 || _shakeData.xSwing != 0 && _shakeData.xSwing <= 0.001 || _shakeData.zSwing != 0 && _shakeData.zSwing <= 0.001)
        {
            ResetShakeInfo();
            return;
        }
        _shakeData.UpdateSpeed(ref _xSpeed, ref _ySpeed, ref _zSpeed, dTime);

        _xDelta = _xSpeed;
        _yDelta = _ySpeed;
        _zDelta = _zSpeed;
        _shakeCount--;
        transform.Translate(new Vector3(_xDelta, _yDelta, _zDelta), Space.Self);
    }

    //Transform _skyTransform = null;
    public void ResetSky(Transform transform)
    {
        /*
        _skyTransform = transform;
        if (_skyTransform == null) return;
        _skyTransform.gameObject.GetComponent<Renderer>().material.renderQueue = 900;
        Camera myCamera = gameObject.GetComponent<Camera>();
        myCamera.clearFlags = CameraClearFlags.Depth;
        GameObject go = GameObject.Find("Fog_FarPlane");
        if (go)
        {
            FarFogPlane ffp = go.GetComponent<FarFogPlane>();
            ffp.ResetFog();
            go.SetActive(false);
        }
        */
    }

    public void SetEnabled(bool enabled)
    {
        this.enabled = enabled;
    }

    public void SetCurrShakeInfo(int priority, int id, float len, float odds = 1.0f)
    {
        //test = new List<CameraInfo>();
        var cameraInfo = new CameraInfo();
        cameraInfo.priority = priority;
        cameraInfo.key = id;
        cameraInfo.len = len;
        cameraInfo.endTime = UnityPropUtils.realtimeSinceStartup + len;
        cameraInfo.shakeCountOdds = odds;
        test.Add(cameraInfo);
        test.Sort((Comparison<CameraInfo>)delegate (CameraInfo a, CameraInfo b)
        {
            return a.priority > b.priority ? 1 : a.priority == b.priority ? 0 : -1;
        });
    }

    public bool IsCanShake(int id)
    {
        for (int i = 0; i < test.Count; i++)
        {
            if (test[i].key == id)
            {
                if (test[i].endTime <= UnityPropUtils.realtimeSinceStartup)
                {
                    test.RemoveAt(i);
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void ResetCurrShakeInfo()
    {
        if (test != null)
        {
            test.Clear();
        }
    }

    public void ShakeCamera(int id, float xSw, float ySw, float zSw, float duration, int count,
                float attenOddsX, float attenOddsY, float attenOddsZ, int xDir = 1, int yDir = 1, int zDir = 1)
    {

        if (isShaking && _currPlayShakeId > 0)
        {
            for (int i = 0; i < test.Count; i++)
            {
                if (test[i].key == _currPlayShakeId)
                {
                    test.RemoveAt(i);
                }
            }
            _xDelta = 0;
            _yDelta = 0;
            _zDelta = 0;
            float shakeCountOdds = dTime / _shakeDuration;
            SetCurrShakeInfo(XMLManager.camera_anim[id].__priority, _currPlayShakeId, dTime, shakeCountOdds);
            isShaking = false;
        }
        CameraAnimData data = new CameraAnimData()
        {
            xSwing = xSw,
            ySwing = ySw,
            zSwing = zSw,
            xDir = xDir,
            yDir = yDir,
            zDir = zDir,
            attenuateOddsX = attenOddsX,
            attenuateOddsY = attenOddsY,
            attenuateOddsZ = attenOddsZ
        };
        if (data == null) return;
        _shakeData = data;
        _shakeStartTime = UnityPropUtils.realtimeSinceStartup;
        _shakeDuration = duration;
        _shakeCount = count;
        _xDelta = 0;
        _yDelta = 0;
        _zDelta = 0;
        _xSpeed = data.xSwing * data.xDir;
        _ySpeed = data.ySwing * data.yDir;
        _zSpeed = data.zSwing * data.zDir;
        _currPlayShakeId = id;
        isShaking = true;
    }

    public int CurrPlayShakeId
    {
        get
        {
            return _currPlayShakeId;
        }
    }

    public void ResetShakeInfo()
    {
        isShaking = false;
        _currPlayShakeId = 0;
        _xDelta = 0;
        _yDelta = 0;
        _zDelta = 0;
    }

    public void SetCameraMotion(BaseCameraMotion motion)
    {
        //Debug.LogError("SetCameraMotion: " + motion.GetType());
        _cameraMotion = motion;
    }

    public void StopCurrentCameraMotion()
    {
        if (_cameraMotion != null)
        {
            _cameraMotion.Stop();
        }
    }

    public bool IsScaled()
    {
        return _cameraMotion != null && _cameraMotion is RotationTargetMotion && (_cameraMotion as RotationTargetMotion).IsScaled();
    }

    public void Stretch(float offset, float speed, float duration)
    {
        if (_cameraMotion != null && accordingMode == CameraAccordingMode.AccordingMotion)
        {
            _cameraMotion.Stretch(offset, speed, duration);
        }
    }

    public void ChangeToFixedAngleMotion(Transform target, float angle, float distance)
    {
        FixedAngleMotion motion = _cameraMotion as FixedAngleMotion;
        SetTarget(target);
        motion.data.target = this.target;
        if (target != null)
        {
            Vector3 targetPosition = target.position;
            if (motion.data.focusPoint == Vector3.zero)
            {
                motion.data.focusPoint = targetPosition;
            }
        }
        motion.data.targetRotation = new Vector3(angle, 0, 0);
        motion.data.targetDistance = SetTargetDistance(distance);
        motion.Start();
    }

    public void ChangeToFaceTargetMotion(Transform target, Vector3 localEulerAngles, float distance, float height, bool ignoreTimeScale, bool keepTouchesScale)
    {
        FaceToTargetMotion motion = _cameraMotion as FaceToTargetMotion;
        SetTarget(target);
        motion.data.target = this.target;
        if (target != null)
        {
            Vector3 targetPosition = target.position;
            if (motion.data.focusPoint == Vector3.zero)
            {
                motion.data.focusPoint = targetPosition;
            }
        }
        Vector3 targetLocalEulerAngles = SetTargetRotation(localEulerAngles);
        motion.data.targetRotation = targetLocalEulerAngles;
        motion.data.targetDistance = SetTargetDistance(distance);
        motion.data.height = SetTargetHeight(height);
        motion.ignoreTimeScale = ignoreTimeScale;
        motion.Start();
    }

    public void ChangeToDragSkillMotion(Transform target, Vector3 localEulerAngles, float distance, bool ignoreTimeScale, bool keepTouchesScale)
    {
        DragSkillMotion motion = _cameraMotion as DragSkillMotion;
        SetTarget(target);
        motion.data.target = this.target;
        if (target != null)
        {
            Vector3 targetPosition = target.position;
            if (motion.data.focusPoint == Vector3.zero)
            {
                motion.data.focusPoint = targetPosition;
            }
        }
        Vector3 targetLocalEulerAngles = SetTargetRotation(localEulerAngles);
        motion.data.targetRotation = targetLocalEulerAngles;
        motion.data.targetDistance = SetTargetDistance(distance);
        motion.ignoreTimeScale = ignoreTimeScale;
        motion.keepTouchesScale = keepTouchesScale;
        motion.Start();
    }

    public void ChangeToLookAtTargetForwardMotion(Transform target, Vector3 localEulerAngles, float rotationDuration, float rotationAccelerateRate, float distance, float distanceDuration, bool ignoreTimeScale, bool keepTouchesScale)
    {
        LookAtTargetForwardMotion motion = _cameraMotion as LookAtTargetForwardMotion;
        SetTarget(target);
        motion.data.target = this.target;
        if (target != null)
        {
            Vector3 targetPosition = target.position;
            if (motion.data.focusPoint == Vector3.zero)
            {
                motion.data.focusPoint = targetPosition;
            }
        }
        Vector3 targetLocalEulerAngles = SetTargetRotation(localEulerAngles);
        motion.data.targetRotation = targetLocalEulerAngles;
        motion.data.rotationDuration = rotationDuration;
        motion.data.rotationAccelerateRate = rotationAccelerateRate;
        motion.data.targetDistance = SetTargetDistance(distance);
        motion.data.distanceDuration = distanceDuration;
        motion.ignoreTimeScale = ignoreTimeScale;
        motion.keepTouchesScale = keepTouchesScale;
        motion.Start();
    }

    public void ChangeToRotationPKTargetMotion(Transform target, Vector3 localEulerAngles, float rotationDuration, float rotationAccelerateRate, float distance, float distanceDuration, bool ignoreTimeScale, bool keepTouchesScale)
    {
        RotationPKTargetMotion motion = _cameraMotion as RotationPKTargetMotion;
        SetTarget(target);
        motion.data.target = this.target;
        if (target != null)
        {
            Vector3 targetPosition = target.position;
            if (motion.data.focusPoint == Vector3.zero)
            {
                motion.data.focusPoint = targetPosition;
            }
        }
        Vector3 targetLocalEulerAngles = SetTargetRotation(localEulerAngles);
        motion.data.targetRotation = targetLocalEulerAngles;
        motion.data.rotationDuration = rotationDuration;
        motion.data.rotationAccelerateRate = rotationAccelerateRate;
        motion.data.targetDistance = SetTargetDistance(distance);
        motion.data.distanceDuration = distanceDuration;
        motion.ignoreTimeScale = ignoreTimeScale;
        motion.keepTouchesScale = keepTouchesScale;
        motion.Start();
    }

    public void ChangeToRotationTargetMotion(Transform target, Vector3 localEulerAngles, float rotationDuration, float rotationAccelerateRate, float distance, float distanceDuration, bool ignoreTimeScale, bool keepTouchesScale)
    {
        RotationTargetMotion motion = _cameraMotion as RotationTargetMotion;
        SetTarget(target);
        motion.data.target = this.target;
        if (target != null)
        {
            Vector3 targetPosition = target.position;
            if (motion.data.focusPoint == Vector3.zero)
            {
                motion.data.focusPoint = targetPosition;
            }
        }
        Vector3 targetLocalEulerAngles = SetTargetRotation(localEulerAngles);
        motion.data.targetRotation = targetLocalEulerAngles;
        motion.data.rotationDuration = rotationDuration;
        motion.data.rotationAccelerateRate = rotationAccelerateRate;
        motion.data.targetDistance = SetTargetDistance(distance);
        motion.data.distanceDuration = distanceDuration;
        motion.ignoreTimeScale = ignoreTimeScale;
        motion.keepTouchesScale = keepTouchesScale;
        motion.Start();
    }

    public void ChangeToTwoTargetPlusMotion(Transform mainTarget, Transform lockTarget, Vector3 localEulerAngles, float rotationDuration, float rotationAccelerateRate, float distance, float distanceDuration, bool ignoreTimeScale, bool keepTouchesScale)
    {
        TwoTargetPlusMotion motion = _cameraMotion as TwoTargetPlusMotion;
        SetTarget(mainTarget);
        motion.data.target = this.target;
        if (mainTarget != null)
        {
            Vector3 targetPosition = mainTarget.position;
            if (motion.data.focusPoint == Vector3.zero)
            {
                motion.data.focusPoint = targetPosition;
            }
        }
        motion.data.lockTarget = lockTarget;
        Vector3 targetLocalEulerAngles = SetTargetRotation(localEulerAngles);
        motion.data.targetRotation = targetLocalEulerAngles;
        motion.data.rotationDuration = rotationDuration;
        motion.data.rotationAccelerateRate = rotationAccelerateRate;
        motion.data.targetDistance = SetTargetDistance(distance);
        motion.data.distanceDuration = distanceDuration;
        motion.ignoreTimeScale = ignoreTimeScale;
        motion.keepTouchesScale = keepTouchesScale;
        motion.Start();
    }

    public void ChangeToTwoTargetMotion(Transform mainTarget, Transform lockTarget, Vector3 localEulerAngles, float rotationDuration, float rotationAccelerateRate, float distance, float distanceDuration, bool ignoreTimeScale, bool keepTouchesScale)
    {
        TwoTargetMotion motion = _cameraMotion as TwoTargetMotion;
        SetTarget(mainTarget);
        motion.data.target = this.target;
        if (mainTarget != null)
        {
            Vector3 targetPosition = mainTarget.position;
            if (motion.data.focusPoint == Vector3.zero)
            {
                motion.data.focusPoint = targetPosition;
            }
        }
        motion.data.lockTarget = lockTarget;
        Vector3 targetLocalEulerAngles = SetTargetRotation(localEulerAngles);
        motion.data.targetRotation = targetLocalEulerAngles;
        motion.data.rotationDuration = rotationDuration;
        motion.data.rotationAccelerateRate = rotationAccelerateRate;
        motion.data.targetDistance = SetTargetDistance(distance);
        motion.data.distanceDuration = distanceDuration;
        motion.ignoreTimeScale = ignoreTimeScale;
        motion.keepTouchesScale = keepTouchesScale;
        motion.Start();
    }

    public void ChangeToRotationPositionMotion(Vector3 localEulerAngles, float rotationDuration, float rotationAccelerateRate, Vector3 position, float positionDuration, bool ignoreTimeScale)
    {
        RotationPositionMotion motion = _cameraMotion as RotationPositionMotion;
        Vector3 targetLocalEulerAngles = SetTargetRotation(localEulerAngles);
        motion.data.focusPoint.Set(0, 0, 0);
        motion.data.targetRotation = targetLocalEulerAngles;
        motion.data.rotationDuration = rotationDuration;
        motion.data.rotationAccelerateRate = rotationAccelerateRate;
        Vector3 targetPosition = SetTargetPosition(position);
        motion.data.targetPosition = targetPosition;
        motion.data.positionDuration = positionDuration;
        motion.ignoreTimeScale = ignoreTimeScale;
        motion.Start();
    }

    public void ChangeToTargetPositionMotion(Transform target, float rotationDuration, float rotationMinSpeed, Vector3 position, float positionDuration, bool ignoreTimeScale)
    {
        TargetPositionMotion motion = _cameraMotion as TargetPositionMotion;
        SetTarget(target);
        this.rotationMinSpeed = rotationMinSpeed;
        motion.data.target = this.target;
        motion.data.focusPoint.Set(0, 0, 0);
        motion.data.rotationDuration = rotationDuration;
        motion.data.rotationMinSpeed = rotationMinSpeed;
        Vector3 targetPosition = SetTargetPosition(position);
        motion.data.targetPosition = targetPosition;
        motion.data.positionDuration = positionDuration;
        motion.ignoreTimeScale = ignoreTimeScale;
        motion.Start();
    }

    public void ChangeToCloseUpMotion(Transform target, Vector3 localEulerAngles, float rotationDuration, float rotationAccelerateRate, float distance, float distanceDuration,
        bool ignoreTimeScale, bool keepTouchesScale, bool increment)
    {
        CloseUpMotion motion = _cameraMotion as CloseUpMotion;
        SetTarget(target);
        motion.data.target = this.target;
        if (target != null)
        {
            Vector3 targetPosition = target.position;
            if (motion.data.focusPoint == Vector3.zero)
            {
                motion.data.focusPoint = targetPosition;
            }
        }
        Vector3 targetLocalEulerAngles = SetTargetRotation(localEulerAngles, increment);
        motion.data.targetRotation = targetLocalEulerAngles;
        motion.data.rotationDuration = rotationDuration;
        motion.data.rotationAccelerateRate = rotationAccelerateRate;
        motion.data.targetDistance = SetTargetDistance(distance);
        motion.data.distanceDuration = distanceDuration;
        motion.ignoreTimeScale = ignoreTimeScale;
        motion.keepTouchesScale = keepTouchesScale;
        motion.Start();
    }

    private void SetTarget(Transform target)
    {
        if (target != null)
        {
            this.target = target;
        }
    }

    private Vector3 SetTargetRotation(Vector3 localEulerAngles, bool increment = false)
    {
        //LoggerHelper.Error("SetTargetRotation: " + localEulerAngles);
        if (localEulerAngles == CURRENT_ROTATION)
        {
            return this._camera.transform.localEulerAngles;
        }
        if (increment)
        {
            var curAngles = this._camera.transform.localEulerAngles;
            rotationX = curAngles.x + localEulerAngles.x;
            rotationY = curAngles.y + localEulerAngles.y;
            rotationZ = curAngles.z + localEulerAngles.z;
        }
        else
        {
            rotationX = localEulerAngles.x;
            rotationY = localEulerAngles.y;
            rotationZ = localEulerAngles.z;
        }
        return localEulerAngles;
    }

    private Vector3 SetTargetPosition(Vector3 position)
    {
        return position;
    }

    public float SetTargetDistance(float distance)
    {
        this.distance = distance;
        return distance;
    }

    private float SetTargetHeight(float height)
    {
        this.height = height;
        return height;
    }

    public CameraMotionType currentMotionType
    {
        get
        {
            if (_cameraMotion != null)
            {
                return _cameraMotion.GetCameraType();
            }
            return CameraMotionType.ROTATION_TARGET;
        }
    }

    public BaseCameraMotion currentCameraMotion
    {
        get
        {
            return _cameraMotion;
        }
    }

    public void UpdateTouchesRange(float top, float right, float bottom, float left)
    {
        if (_cameraMotion != null && _cameraMotion is RotationTargetMotion)
        {
            (_cameraMotion as RotationTargetMotion).UpdateRange(top, right, bottom, left);
        }
    }
}

public class CameraInfo
{
    public int priority;
    public int key;
    public float len;
    public float shakeCountOdds = 1.0f;
    public float endTime = 0;
}
