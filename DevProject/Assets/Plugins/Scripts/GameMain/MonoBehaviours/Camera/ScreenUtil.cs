﻿using Common.Events;
using MogoEngine.Events;
using UnityEngine;


public class ScreenUtil : MonoBehaviour
{
    public const int PANEL_WIDTH = 1280;
    public const int PANEL_HEIGHT = 720;
    private static float _screenScale = 1;

    public static int ScreenWidth
    {
        get
        {
            return _lastScreenWidth;
        }
    }

    public static int ScreenHeight
    {
        get
        {
            return _lastScreenHeight;
        }
    }

    public static float screenScale
    {
        get
        {
            return _screenScale;
        }

        private set
        {
            _screenScale = value;
        }
    }

    private static int _lastScreenWidth = 1280;
    private static int _lastScreenHeight = 720;
    private int _space = 0;

    void Update()
    {
        if (_space > 0)
        {
            _space--;
            return;
        }
        _space = 30;
        if (_lastScreenHeight != Screen.height || _lastScreenWidth != Screen.width)
        {
            _lastScreenHeight = Screen.height;
            _lastScreenWidth = Screen.width;
            ScreenUtils.ScreenHeight = _lastScreenHeight;
            ScreenUtils.ScreenWidth = _lastScreenWidth;
            CalculateScreenScaleFactor();
            EventDispatcher.TriggerEvent(ScreenEvents.ON_SCREEN_RESIZE, screenScale);
        }
    }

    void CalculateScreenScaleFactor()
    {
        float scaleWidth = (float)ScreenWidth / PANEL_WIDTH;
        float scaleHeight = (float)ScreenHeight / PANEL_HEIGHT;
        screenScale = scaleWidth < scaleHeight ? scaleWidth : scaleHeight;
    }
}



