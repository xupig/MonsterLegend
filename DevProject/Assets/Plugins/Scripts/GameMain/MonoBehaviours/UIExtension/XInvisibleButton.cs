using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace GameMain
{

    /// <summary>
    /// 按钮透明热区，阻挡按钮中Selectable子组件对鼠标的响应
    /// </summary>
    public class XButtonHitArea : Graphic
    {
        public override void SetAllDirty()
        {
            //blank
        }

        public override void SetLayoutDirty()
        {
            //blank
        }

        public override void SetVerticesDirty()
        {
            //blank
        }

        public override void SetMaterialDirty()
        {
            //blank
        }

    }

    /// <summary>
    /// 热区为透明的按钮
    /// </summary>
    public class XInvisibleButton : Button
    {
        protected override void Awake()
        {
            base.Awake();
            if(gameObject.GetComponent<Graphic>() == null)
            {
                gameObject.AddComponent<XButtonHitArea>();
            }
        }
    }
}
