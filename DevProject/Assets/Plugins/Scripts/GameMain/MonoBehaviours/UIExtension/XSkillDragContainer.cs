﻿using ACTSystem;
using Common.Data;
using Common.Structs;
using GameData;
using GameMain.GlobalManager;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using Common.ExtendTools;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using System.Collections.Generic;
using Common.Utils;

namespace GameMain
{
    public class XSkillDragContainer : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        private RectTransform _rectTransform;
        private Vector2 _currentPosition;

        private Transform _rotateTrans;
        private Transform _rotatePosTrans;
        private Transform _dragTrans;

        private List<Transform> _skillTransList = new List<Transform>();

        private const int Max_Skill_Pos_Count = 12;
        private const int Max_Skill_Count = 8;
        private const int Start_Skill_Index = 2;
        private const int End_Skill_Index = 9;
        private const float Unit_Angle = 30f;
        private const float ROTE_SPEED = 1f; //0.73f
        private const float Change_Page_Angle = 30f; //30f

        private List<Transform> _skillPosTransList = new List<Transform>();

        private int _curPage = 1;
        private Vector3 _hidePosition = new Vector3(-1000, -1000, 0);

        private Transform _pageOne;
        private Transform _pageTwo;

        /// <summary>
        /// 是否可拖动
        /// </summary>
        public bool dragable { get; set; }

        protected void Awake()
        {
            _dragTrans = this.transform.FindChild("Container_Skills");

            _rotateTrans = this.transform.FindChild("Container_Center");
            _rotatePosTrans = this.transform.FindChild("Container_Center/Container_Pos");

            for (int i = Start_Skill_Index; i <= End_Skill_Index; i++)
            {
                _skillTransList.Add(this.transform.FindChild("Container_Skills/Container_Skill" + i));
            }

            for (int i = 1; i <= Max_Skill_Pos_Count; i++)
            {
                _skillPosTransList.Add(this.transform.FindChild("Container_Center/Container_Pos" + i));
            }

            _pageOne = this.transform.FindChild("Container_Page/Image_1");
            _pageTwo = this.transform.FindChild("Container_Page/Image_2");
            _pageOne.gameObject.SetActive(true);
            _pageTwo.gameObject.SetActive(false);

            _rectTransform = _dragTrans.GetComponent<RectTransform>();
            dragable = true;
            SetPage(1);
        }

        Vector3 rotateAngle = Vector3.zero;
        public void OnDrag(PointerEventData eventData)
        {
            if (dragable == false) return;

            Vector2 localPosition;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform,
            eventData.position, eventData.pressEventCamera, out localPosition))
            {
                float lengthX = (localPosition - _currentPosition).x;
                float lengthY = (localPosition - _currentPosition).y;
                float length = lengthX;
                if (ACTSystem.ACTMathUtils.Abs(lengthX) < ACTSystem.ACTMathUtils.Abs(lengthY))
                {
                    length = lengthY;
                }
                _currentPosition = localPosition;

                if (_rotateTrans != null)
                {
                    float rotateZ = -length * ROTE_SPEED;
                    float endRotateZ = _rotateTrans.localEulerAngles.z + rotateZ;
                    if (endRotateZ <= 120 || endRotateZ >= 240)
                    {
                        rotateAngle.z = endRotateZ;
                        _rotateTrans.localEulerAngles = rotateAngle;
                        Rotate();
                    }
                }

            }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            LuaGameState.skillDragState = 1;
            if (dragable == false) return;
            _onUpdate = false;
            _rotateTrans.localEulerAngles = Vector3.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform,
            eventData.position, eventData.pressEventCamera, out _currentPosition);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (dragable == false) return;
            markZ = _rotateTrans.localEulerAngles.z;
            if ((markZ > Change_Page_Angle && markZ < 120) || (markZ > 240 && markZ < (360 - Change_Page_Angle)))
            {
                if (_rotateTrans.localEulerAngles.z >= 240)
                {
                    endZ = 240;
                    delta = (endZ - _rotateTrans.localEulerAngles.z) / 0.5f;
                }
                else
                {
                    endZ = 120;
                    delta = (endZ - _rotateTrans.localEulerAngles.z) / 0.5f;
                }
            }
            else
            {
                endZ = markZ;
                if (_rotateTrans.localEulerAngles.z >= 240)
                {
                    delta = (_rotateTrans.localEulerAngles.z - 360) / -0.5f;
                }
                else
                {
                    delta = _rotateTrans.localEulerAngles.z / -0.5f;
                }
            }
            _onUpdate = true;
            Rotate();
            LuaGameState.skillDragState = 0;
        }

        bool _onUpdate = false;
        float delta = 0;
        float markZ = 0;
        float endZ = 0;
        Vector3 updateRotateAngle = Vector3.zero;
        void Update()
        {
            if(_onUpdate)
            {
                if (markZ > 120 && (endZ - _rotateTrans.localEulerAngles.z > 0f))
                {
                    ChangePage(markZ);
                    _rotateTrans.localEulerAngles = Vector3.zero;
                    _onUpdate = false;
                    Rotate();
                    return;
                }
                if (markZ <= 120 && (endZ - _rotateTrans.localEulerAngles.z < 0f))
                {
                    ChangePage(markZ);
                    _rotateTrans.localEulerAngles = Vector3.zero;
                    _onUpdate = false;
                    Rotate();
                    return;
                }
                updateRotateAngle.z = _rotateTrans.localEulerAngles.z + UnityPropUtils.deltaTime * delta;
                _rotateTrans.localEulerAngles = updateRotateAngle;
                Rotate();
            }
        }

        private void ChangePage(float z)
        {
            if ((z > Change_Page_Angle && z < 120) || (z > 240 && z < (360 - Change_Page_Angle)))
            {
                if (_curPage == 1)
                {
                    _curPage = 2;
                    _pageOne.gameObject.SetActive(false);
                    _pageTwo.gameObject.SetActive(true);
                }
                else
                {
                    _curPage = 1;
                    _pageOne.gameObject.SetActive(true);
                    _pageTwo.gameObject.SetActive(false);
                }
            }
        }

        public void SetPage(int page)
        {
            _curPage = page;
            _rotateTrans.localEulerAngles = Vector3.zero;
            Rotate();
            if (_curPage == 2)
            {
                _pageOne.gameObject.SetActive(false);
                _pageTwo.gameObject.SetActive(true);
            }
            else
            {
                _pageOne.gameObject.SetActive(true);
                _pageTwo.gameObject.SetActive(false);
            }
        }


        private List<int> hideList_1 = new List<int>() { 5, 6, 7 };
        private List<int> hideList_2 = new List<int>() { 6, 7, 0 };
        private List<int> hideList_3 = new List<int>() { 7, 0, 1 };
        private List<int> hideList_4 = new List<int>() { 0, 1, 2 };
        private List<int> hideList_5 = new List<int>() { 4, 5, 6 };
        private List<int> hideList_6 = new List<int>() { 3, 4, 5 };
        private List<int> hideList_7 = new List<int>() { 2, 3, 4 };
        private List<int> hideList_8 = new List<int>() { 1, 2, 3 };
        private List<int> hideList_9 = new List<int>() { 1, 2, 3 };
        private List<int> hideList_10 = new List<int>() { 2, 3, 4 };
        private List<int> hideList_11 = new List<int>() { 3, 4, 5 };
        private List<int> hideList_12 = new List<int>() { 4, 5, 6 };
        private List<int> hideList_13 = new List<int>() { 0, 1, 2 };
        private List<int> hideList_14 = new List<int>() { 7, 0, 1 };
        private List<int> hideList_15 = new List<int>() { 6, 7, 0 };
        private List<int> hideList_16 = new List<int>() { 5, 6, 7 };

        private List<int> posList_1 = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7 };
        private List<int> posList_2 = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7 };
        private List<int> posList_3 = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7 };
        private List<int> posList_4 = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7 };
        private List<int> posList_5 = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 11 };
        private List<int> posList_6 = new List<int>() { 0, 1, 2, 3, 4, 5, 10, 11 };
        private List<int> posList_7 = new List<int>() { 0, 1, 2, 3, 4, 9, 10, 11 };
        private List<int> posList_8 = new List<int>() { 0, 1, 2, 3, 8, 9, 10, 11 };
        private List<int> posList_9 = new List<int>() { 4, 5, 6, 7, 0, 1, 2, 3 };
        private List<int> posList_10 = new List<int>() { 4, 5, 6, 7, 0, 1, 2, 3 };
        private List<int> posList_11 = new List<int>() { 4, 5, 6, 7, 0, 1, 2, 3 };
        private List<int> posList_12 = new List<int>() { 4, 5, 6, 7, 0, 1, 2, 3 };
        private List<int> posList_13 = new List<int>() { 8, 9, 10, 11, 0, 1, 2, 3 };
        private List<int> posList_14 = new List<int>() { 8, 9, 10, 11, 0, 1, 2, 3 };
        private List<int> posList_15 = new List<int>() { 8, 9, 10, 11, 0, 1, 2, 3 };
        private List<int> posList_16 = new List<int>() { 8, 9, 10, 11, 0, 1, 2, 3 };
        //一页显示4个，每个相隔30度，最大滑动120度
        private void Rotate()
        {
            if (dragable == false) return;
            float z = _rotateTrans.localEulerAngles.z;
            if (_curPage == 1)
            {
                if (z >= 0 && z <= 30)
                {
                    //显示技能2,3,4,5,6
                    for (int i = 0; i < 8; i++)
                    {
                        int index = posList_1[i];
                        _skillTransList[i].position = _skillPosTransList[index].position;
                        if (!_skillTransList[i].gameObject.activeSelf)
                        {
                            _skillTransList[i].gameObject.SetActive(true);
                        }
                    }

                    for (int j = 0; j < 3; j++)
                    {
                        int index = hideList_1[j];
                        if (_skillTransList[index].gameObject.activeSelf)
                        {
                            _skillTransList[index].gameObject.SetActive(false);
                        }
                    }
                }
                else if (z > 30 && z <= 60)
                {
                    //显示技能3,4,5,6,7
                    for (int i = 0; i < 8; i++)
                    {
                        int index = posList_2[i];
                        _skillTransList[i].position = _skillPosTransList[index].position;
                        if (!_skillTransList[i].gameObject.activeSelf)
                        {
                            _skillTransList[i].gameObject.SetActive(true);
                        }
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        int index = hideList_2[j];
                        if (_skillTransList[index].gameObject.activeSelf)
                        {
                            _skillTransList[index].gameObject.SetActive(false);
                        }
                    }
                }
                else if (z > 60 && z <= 90)
                {
                    //显示技能4,5,6,7,8
                    for (int i = 0; i < 8; i++)
                    {
                        int index = posList_3[i];
                        _skillTransList[i].position = _skillPosTransList[index].position;
                        if (!_skillTransList[i].gameObject.activeSelf)
                        {
                            _skillTransList[i].gameObject.SetActive(true);
                        }
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        int index = hideList_3[j];
                        if (_skillTransList[index].gameObject.activeSelf)
                        {
                            _skillTransList[index].gameObject.SetActive(false);
                        }
                    }
                }
                else if (z > 90 && z <= 120)
                {
                    //显示5,6,7,8,9
                    for (int i = 0; i < 8; i++)
                    {
                        int index = posList_4[i];
                        _skillTransList[i].position = _skillPosTransList[index].position;
                        if (!_skillTransList[i].gameObject.activeSelf)
                        {
                            _skillTransList[i].gameObject.SetActive(true);
                        }
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        int index = hideList_4[j];
                        if (_skillTransList[index].gameObject.activeSelf)
                        {
                            _skillTransList[index].gameObject.SetActive(false);
                        }
                    }
                }

                else if (z >= 330 && z < 360)
                {
                    //显示9,2,3,4,5
                    for (int i = 0; i < 8; i++)
                    {
                        int index = posList_5[i];
                        _skillTransList[i].position = _skillPosTransList[index].position;
                        if (!_skillTransList[i].gameObject.activeSelf)
                        {
                            _skillTransList[i].gameObject.SetActive(true);
                        }
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        int index = hideList_5[j];
                        if (_skillTransList[index].gameObject.activeSelf)
                        {
                            _skillTransList[index].gameObject.SetActive(false);
                        }
                    }
                }
                else if (z >= 300 && z < 330)
                {
                    //显示8,9,2,3,4
                    for (int i = 0; i < 8; i++)
                    {
                        int index = posList_6[i];
                        _skillTransList[i].position = _skillPosTransList[index].position;
                        if (!_skillTransList[i].gameObject.activeSelf)
                        {
                            _skillTransList[i].gameObject.SetActive(true);
                        }
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        int index = hideList_6[j];
                        if (_skillTransList[index].gameObject.activeSelf)
                        {
                            _skillTransList[index].gameObject.SetActive(false);
                        }
                    }
                }
                else if (z >= 270 && z < 300)
                {
                    //显示7,8,9,2,3
                    for (int i = 0; i < 8; i++)
                    {
                        int index = posList_7[i];
                        _skillTransList[i].position = _skillPosTransList[index].position;
                        if (!_skillTransList[i].gameObject.activeSelf)
                        {
                            _skillTransList[i].gameObject.SetActive(true);
                        }
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        int index = hideList_7[j];
                        if (_skillTransList[index].gameObject.activeSelf)
                        {
                            _skillTransList[index].gameObject.SetActive(false);
                        }
                    }
                }
                else if (z >= 240 && z < 270)
                {
                    //显示6,7,8,9,2
                    for (int i = 0; i < 8; i++)
                    {
                        int index = posList_8[i];
                        _skillTransList[i].position = _skillPosTransList[index].position;
                        if (!_skillTransList[i].gameObject.activeSelf)
                        {
                            _skillTransList[i].gameObject.SetActive(true);
                        }
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        int index = hideList_8[j];
                        if (_skillTransList[index].gameObject.activeSelf)
                        {
                            _skillTransList[index].gameObject.SetActive(false);
                        }
                    }
                }
            }
            else
            {
                if (z >= 0 && z <= 30)
                {
                    //显示技能6,7,8,9,2
                    for (int i = 0; i < 8; i++)
                    {
                        int index = posList_9[i];
                        _skillTransList[i].position = _skillPosTransList[index].position;
                        if (!_skillTransList[i].gameObject.activeSelf)
                        {
                            _skillTransList[i].gameObject.SetActive(true);
                        }
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        int index = hideList_9[j];
                        if (_skillTransList[index].gameObject.activeSelf)
                        {
                            _skillTransList[index].gameObject.SetActive(false);
                        }
                    }
                }
                else if (z > 30 && z <= 60)
                {
                    //显示技能7,8,9,2,3
                    for (int i = 0; i < 8; i++)
                    {
                        int index = posList_10[i];
                        _skillTransList[i].position = _skillPosTransList[index].position;
                        if (!_skillTransList[i].gameObject.activeSelf)
                        {
                            _skillTransList[i].gameObject.SetActive(true);
                        }
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        int index = hideList_10[j];
                        if (_skillTransList[index].gameObject.activeSelf)
                        {
                            _skillTransList[index].gameObject.SetActive(false);
                        }
                    }
                }
                else if (z > 60 && z <= 90)
                {
                    //显示技能8,9,2,3,4
                    for (int i = 0; i < 8; i++)
                    {
                        int index = posList_11[i];
                        _skillTransList[i].position = _skillPosTransList[index].position;
                        if (!_skillTransList[i].gameObject.activeSelf)
                        {
                            _skillTransList[i].gameObject.SetActive(true);
                        }
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        int index = hideList_11[j];
                        if (_skillTransList[index].gameObject.activeSelf)
                        {
                            _skillTransList[index].gameObject.SetActive(false);
                        }
                    }
                }
                else if (z > 90 && z <= 120)
                {
                    //显示9,2,3,4,5
                    for (int i = 0; i < 8; i++)
                    {
                        int index = posList_12[i];
                        _skillTransList[i].position = _skillPosTransList[index].position;
                        if (!_skillTransList[i].gameObject.activeSelf)
                        {
                            _skillTransList[i].gameObject.SetActive(true);
                        }
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        int index = hideList_12[j];
                        if (_skillTransList[index].gameObject.activeSelf)
                        {
                            _skillTransList[index].gameObject.SetActive(false);
                        }
                    }
                }

                else if (z >= 330 && z < 360)
                {
                    //显示5,6,7,8,9
                    for (int i = 0; i < 8; i++)
                    {
                        int index = posList_13[i];
                        _skillTransList[i].position = _skillPosTransList[index].position;
                        if (!_skillTransList[i].gameObject.activeSelf)
                        {
                            _skillTransList[i].gameObject.SetActive(true);
                        }
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        int index = hideList_13[j];
                        if (_skillTransList[index].gameObject.activeSelf)
                        {
                            _skillTransList[index].gameObject.SetActive(false);
                        }
                    }
                }
                else if (z >= 300 && z < 330)
                {
                    //显示4,5,6,7,8
                    for (int i = 0; i < 8; i++)
                    {
                        int index = posList_14[i];
                        _skillTransList[i].position = _skillPosTransList[index].position;
                        if (!_skillTransList[i].gameObject.activeSelf)
                        {
                            _skillTransList[i].gameObject.SetActive(true);
                        }
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        int index = hideList_14[j];
                        if (_skillTransList[index].gameObject.activeSelf)
                        {
                            _skillTransList[index].gameObject.SetActive(false);
                        }
                    }
                }
                else if (z >= 270 && z < 300)
                {
                    //显示3,4,5,6,7
                    for (int i = 0; i < 8; i++)
                    {
                        int index = posList_15[i];
                        _skillTransList[i].position = _skillPosTransList[index].position;
                        if (!_skillTransList[i].gameObject.activeSelf)
                        {
                            _skillTransList[i].gameObject.SetActive(true);
                        }
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        int index = hideList_15[j];
                        if (_skillTransList[index].gameObject.activeSelf)
                        {
                            _skillTransList[index].gameObject.SetActive(false);
                        }
                    }
                }
                else if (z >= 240 && z < 270)
                {
                    //显示2,3,4,5,6
                    for (int i = 0; i < 8; i++)
                    {
                        int index = posList_16[i];
                        _skillTransList[i].position = _skillPosTransList[index].position;
                        if (!_skillTransList[i].gameObject.activeSelf)
                        {
                            _skillTransList[i].gameObject.SetActive(true);
                        }
                    }
                    for (int j = 0; j < 3; j++)
                    {
                        int index = hideList_16[j];
                        if (_skillTransList[index].gameObject.activeSelf)
                        {
                            _skillTransList[index].gameObject.SetActive(false);
                        }
                    }
                }
            }
        }

        //一页显示4个，每个相隔30度，最大滑动120度
        private void RotateOld()
        {
            if (dragable == false) return;
            float z = _rotateTrans.localEulerAngles.z;
            if (_curPage == 1)
            {
                if (z >= 0 && z <= 30)
                {
                    //显示技能2,3,4,5,6
                    _skillTransList[0].position = _skillPosTransList[0].position;
                    _skillTransList[1].position = _skillPosTransList[1].position;
                    _skillTransList[2].position = _skillPosTransList[2].position;
                    _skillTransList[3].position = _skillPosTransList[3].position;
                    _skillTransList[4].position = _skillPosTransList[4].position;
                    _skillTransList[5].position = _hidePosition;
                    _skillTransList[6].position = _hidePosition;
                    _skillTransList[7].position = _hidePosition;

                    for (int i = 0; i < 8; i++)
                    {
                        if (!_skillTransList[i].gameObject.activeSelf)
                        {
                            _skillTransList[i].gameObject.SetActive(true);
                        }
                    }

                    if (!_skillTransList[4].gameObject.activeSelf)
                    {
                        _skillTransList[4].gameObject.SetActive(true);
                    }
                    if (!_skillTransList[5].gameObject.activeSelf)
                    {
                        _skillTransList[5].gameObject.SetActive(true);
                    }
                    if (!_skillTransList[6].gameObject.activeSelf)
                    {
                        _skillTransList[6].gameObject.SetActive(true);
                    }

                }
                else if (z > 30 && z <= 60)
                {
                    //显示技能3,4,5,6,7
                    _skillTransList[0].position = _hidePosition;
                    _skillTransList[1].position = _skillPosTransList[1].position;
                    _skillTransList[2].position = _skillPosTransList[2].position;
                    _skillTransList[3].position = _skillPosTransList[3].position;
                    _skillTransList[4].position = _skillPosTransList[4].position;
                    _skillTransList[5].position = _skillPosTransList[5].position;
                    _skillTransList[6].position = _hidePosition;
                    _skillTransList[7].position = _hidePosition;
                }
                else if (z > 60 && z <= 90)
                {
                    //显示技能4,5,6,7,8
                    _skillTransList[0].position = _hidePosition;
                    _skillTransList[1].position = _hidePosition;
                    _skillTransList[2].position = _skillPosTransList[2].position;
                    _skillTransList[3].position = _skillPosTransList[3].position;
                    _skillTransList[4].position = _skillPosTransList[4].position;
                    _skillTransList[5].position = _skillPosTransList[5].position;
                    _skillTransList[6].position = _skillPosTransList[6].position;
                    _skillTransList[7].position = _hidePosition;
                }
                else if (z > 90 && z <= 120)
                {
                    //显示5,6,7,8,9
                    _skillTransList[0].position = _hidePosition;
                    _skillTransList[1].position = _hidePosition;
                    _skillTransList[2].position = _hidePosition;
                    _skillTransList[3].position = _skillPosTransList[3].position;
                    _skillTransList[4].position = _skillPosTransList[4].position;
                    _skillTransList[5].position = _skillPosTransList[5].position;
                    _skillTransList[6].position = _skillPosTransList[6].position;
                    _skillTransList[7].position = _skillPosTransList[7].position;
                }

                else if (z >= 330 && z < 360)
                {
                    //显示9,2,3,4,5
                    _skillTransList[0].position = _skillPosTransList[0].position;
                    _skillTransList[1].position = _skillPosTransList[1].position;
                    _skillTransList[2].position = _skillPosTransList[2].position;
                    _skillTransList[3].position = _skillPosTransList[3].position;
                    _skillTransList[4].position = _hidePosition;
                    _skillTransList[5].position = _hidePosition;
                    _skillTransList[6].position = _hidePosition;
                    _skillTransList[7].position = _skillPosTransList[11].position;
                }
                else if (z >= 300 && z < 330)
                {
                    //显示8,9,2,3,4
                    _skillTransList[0].position = _skillPosTransList[0].position;
                    _skillTransList[1].position = _skillPosTransList[1].position;
                    _skillTransList[2].position = _skillPosTransList[2].position;
                    _skillTransList[3].position = _hidePosition;
                    _skillTransList[4].position = _hidePosition;
                    _skillTransList[5].position = _hidePosition;
                    _skillTransList[6].position = _skillPosTransList[10].position;
                    _skillTransList[7].position = _skillPosTransList[11].position;
                }
                else if (z >= 270 && z < 300)
                {
                    //显示7,8,9,2,3
                    _skillTransList[0].position = _skillPosTransList[0].position;
                    _skillTransList[1].position = _skillPosTransList[1].position;
                    _skillTransList[2].position = _hidePosition;
                    _skillTransList[3].position = _hidePosition;
                    _skillTransList[4].position = _hidePosition;
                    _skillTransList[5].position = _skillPosTransList[9].position;
                    _skillTransList[6].position = _skillPosTransList[10].position;
                    _skillTransList[7].position = _skillPosTransList[11].position;
                }
                else if (z >= 240 && z < 270)
                {
                    //显示6,7,8,9,2
                    _skillTransList[0].position = _skillPosTransList[0].position;
                    _skillTransList[1].position = _hidePosition;
                    _skillTransList[2].position = _hidePosition;
                    _skillTransList[3].position = _hidePosition;
                    _skillTransList[4].position = _skillPosTransList[8].position;
                    _skillTransList[5].position = _skillPosTransList[9].position;
                    _skillTransList[6].position = _skillPosTransList[10].position;
                    _skillTransList[7].position = _skillPosTransList[11].position;
                }
            }
            else
            {
                if (z >= 0 && z <= 30)
                {
                    //显示技能6,7,8,9,2
                    _skillTransList[0].position = _skillPosTransList[4].position;
                    _skillTransList[1].position = _hidePosition;
                    _skillTransList[2].position = _hidePosition;
                    _skillTransList[3].position = _hidePosition;
                    _skillTransList[4].position = _skillPosTransList[0].position;
                    _skillTransList[5].position = _skillPosTransList[1].position;
                    _skillTransList[6].position = _skillPosTransList[2].position;
                    _skillTransList[7].position = _skillPosTransList[3].position;
                }
                else if (z > 30 && z <= 60)
                {
                    //显示技能7,8,9,2,3
                    _skillTransList[0].position = _skillPosTransList[4].position;
                    _skillTransList[1].position = _skillPosTransList[5].position;
                    _skillTransList[2].position = _hidePosition;
                    _skillTransList[3].position = _hidePosition;
                    _skillTransList[4].position = _hidePosition;
                    _skillTransList[5].position = _skillPosTransList[1].position;
                    _skillTransList[6].position = _skillPosTransList[2].position;
                    _skillTransList[7].position = _skillPosTransList[3].position;
                }
                else if (z > 60 && z <= 90)
                {
                    //显示技能8,9,2,3,4
                    _skillTransList[0].position = _skillPosTransList[4].position;
                    _skillTransList[1].position = _skillPosTransList[5].position;
                    _skillTransList[2].position = _skillPosTransList[6].position;
                    _skillTransList[3].position = _hidePosition;
                    _skillTransList[4].position = _hidePosition;
                    _skillTransList[5].position = _hidePosition;
                    _skillTransList[6].position = _skillPosTransList[2].position;
                    _skillTransList[7].position = _skillPosTransList[3].position;
                }
                else if (z > 90 && z <= 120)
                {
                    //显示9,2,3,4,5
                    _skillTransList[0].position = _skillPosTransList[4].position;
                    _skillTransList[1].position = _skillPosTransList[5].position;
                    _skillTransList[2].position = _skillPosTransList[6].position;
                    _skillTransList[3].position = _skillPosTransList[7].position;
                    _skillTransList[4].position = _hidePosition;
                    _skillTransList[5].position = _hidePosition;
                    _skillTransList[6].position = _hidePosition;
                    _skillTransList[7].position = _skillPosTransList[3].position;
                }

                else if (z >= 330 && z < 360)
                {
                    //显示5,6,7,8,9
                    _skillTransList[0].position = _hidePosition;
                    _skillTransList[1].position = _hidePosition;
                    _skillTransList[2].position = _hidePosition;
                    _skillTransList[3].position = _skillPosTransList[11].position;
                    _skillTransList[4].position = _skillPosTransList[0].position;
                    _skillTransList[5].position = _skillPosTransList[1].position;
                    _skillTransList[6].position = _skillPosTransList[2].position;
                    _skillTransList[7].position = _skillPosTransList[3].position;
                }
                else if (z >= 300 && z < 330)
                {
                    //显示4,5,6,7,8
                    _skillTransList[0].position = _hidePosition;
                    _skillTransList[1].position = _hidePosition;
                    _skillTransList[2].position = _skillPosTransList[10].position;
                    _skillTransList[3].position = _skillPosTransList[11].position;
                    _skillTransList[4].position = _skillPosTransList[0].position;
                    _skillTransList[5].position = _skillPosTransList[1].position;
                    _skillTransList[6].position = _skillPosTransList[2].position;
                    _skillTransList[7].position = _hidePosition;
                }
                else if (z >= 270 && z < 300)
                {
                    //显示3,4,5,6,7
                    _skillTransList[0].position = _hidePosition;
                    _skillTransList[1].position = _skillPosTransList[9].position;
                    _skillTransList[2].position = _skillPosTransList[10].position;
                    _skillTransList[3].position = _skillPosTransList[11].position;
                    _skillTransList[4].position = _skillPosTransList[0].position;
                    _skillTransList[5].position = _skillPosTransList[1].position;
                    _skillTransList[6].position = _hidePosition;
                    _skillTransList[7].position = _hidePosition;
                }
                else if (z >= 240 && z < 270)
                {
                    //显示2,3,4,5,6
                    _skillTransList[0].position = _skillPosTransList[8].position;
                    _skillTransList[1].position = _skillPosTransList[9].position;
                    _skillTransList[2].position = _skillPosTransList[10].position;
                    _skillTransList[3].position = _skillPosTransList[11].position;
                    _skillTransList[4].position = _skillPosTransList[0].position;
                    _skillTransList[5].position = _hidePosition;
                    _skillTransList[6].position = _hidePosition;
                    _skillTransList[7].position = _hidePosition;
                }
            }
        }
    }
}

