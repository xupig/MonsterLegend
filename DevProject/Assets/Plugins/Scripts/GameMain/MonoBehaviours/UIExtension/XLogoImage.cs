﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Common.Utils;
using GameResource;
using UIExtension;
using MogoEngine.Events;
using GameLoader.Utils;
using GameLoader.Mgrs;
using System.IO;

namespace GameMain
{
    public class XLogoImage : MonoBehaviour
    {
        ImageWrapper _image = null;
        ImageWrapper _loginBG = null;
        string logoPath;
        string localLogoPath;
        string localLoginBGPath;

        void Awake()
        {
            _image = gameObject.AddComponent<ImageWrapper>();
            _image.enabled = false;
            _image.raycastTarget = false;
            logoPath = String.Concat(Application.persistentDataPath, ConstString.LogoPath);
            localLogoPath = String.Concat(Application.streamingAssetsPath, ConstString.LocalLogo);
            localLoginBGPath = String.Concat(Application.streamingAssetsPath, ConstString.LocalLoginBG);
            SetLocalImage();
            Transform loginBGTrans = transform.parent.FindChild("Image_LoginBG");
            if (loginBGTrans != null)
            {
                _loginBG = loginBGTrans.gameObject.GetComponent<ImageWrapper>();
                if (_loginBG == null)
                {
                    _loginBG = loginBGTrans.gameObject.AddComponent<ImageWrapper>();
                    _loginBG.raycastTarget = false;
                }
                _loginBG.enabled = false;
                var bgRect = loginBGTrans.gameObject.GetComponent<RectTransform>();
                if (bgRect)
                {
                    float scrHeight = Screen.height;
                    float scrWidth = Screen.width;
                    float imgHeight = 720;
                    float imgWidth = 1280;
                    var imgScale = imgWidth / imgHeight;
                    if (scrWidth / scrHeight > imgScale)
                    {
                        var newWidth = imgHeight * scrWidth / scrHeight;
                        var newHeight = newWidth / imgScale;
                        bgRect.sizeDelta = new Vector2(newWidth, newHeight);
                    }
                    else
                    {
                        var newHeight = imgWidth * scrHeight / scrWidth;
                        var newWidth = newHeight * imgScale;
                        bgRect.sizeDelta = new Vector2(newWidth, newHeight);
                    }
                }
            }
            SetLoginBGImage();
        }

        void OnDisable()
        {
            EventDispatcher.RemoveEventListener("OnLogoLoaded", OnLogoLoaded);
        }

        void OnEnable()
        {
            EventDispatcher.AddEventListener("OnLogoLoaded", OnLogoLoaded);
        }

        private void SetLoginBGImage()
        {
            if (_loginBG == null || _loginBG.sprite != null) return;
            FileDownloadMgr.Instance.LoadWwwImage(localLoginBGPath, OnLoginBGTextureLoaded);
        }

        private void SetLocalImage()
        {
            if (_image == null || _image.sprite != null) return;
            FileDownloadMgr.Instance.LoadWwwImage(localLogoPath, OnLogoTextureLoaded);
        }

        public void SetImage()
        {
            if (_image == null || _image.sprite != null) return;
            if (File.Exists(logoPath))
                FileDownloadMgr.Instance.LoadWwwImage(logoPath, OnLogoTextureLoaded);
        }

        private void OnLogoLoaded()
        {
            if (_image == null) return;
            if (File.Exists(logoPath))
                FileDownloadMgr.Instance.LoadWwwImage(logoPath, OnLogoTextureLoaded);
        }

        private void OnLogoTextureLoaded(string path, Texture2D logo)
        {
            if (_image == null) return;
            _image.enabled = true;
            _image.sprite = Sprite.Create(logo, new Rect(0, 0, logo.width, logo.height), new Vector2(0, 0));
        }

        private void OnLoginBGTextureLoaded(string path, Texture2D bg)
        {
            if (_loginBG == null) return;
            _loginBG.sprite = Sprite.Create(bg, new Rect(0, 0, bg.width, bg.height), new Vector2(0, 0));
            _loginBG.enabled = true;
        }
    }
}
