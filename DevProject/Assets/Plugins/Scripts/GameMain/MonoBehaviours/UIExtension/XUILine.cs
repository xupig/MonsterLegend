﻿using UIExtension;
using UnityEngine;
using System;
//UI连接线，用一张UI图片作旋转拉伸连接两个点
namespace GameMain
{
    public class XUILine : XContainer
    {
        private RectTransform lineRect;
        private RectTransform rect;
        protected override void Awake()
        {
            Transform line = transform.Find("Image_Line");
            lineRect = line.GetComponent<RectTransform>();
            rect = transform.GetComponent<RectTransform>();
        }

        public void ConnectByTwoVertice(float p1x, float p1y, float p2x, float p2y)
        {
            float angle = CalulateXYAnagle(p1x, p1y, p2x, p2y);//(float)(Math.Atan((p1x - p2y) / (p1y - p2y)) * 180 / Math.PI) - 180;
            //Debug.LogError("ConnectByTwoVertice" + angle + "/" + p1x + "/" + p1y+"/" +p2x +"/"+p2y);
            SetLineWidth(Vector2.Distance( new Vector2(p1x,p1y),new Vector2(p2x,p2y)));
            SetRotation(angle);
            rect.anchoredPosition = new Vector3(p1x, p1y, 0);
        }

        private float CalulateXYAnagle(float startx, float starty, float endx, float endy)
        {
            if (endx - startx == 0)
            {
                if (starty > endy)
                {
                    return -90;
                }else
                {
                    return 90;
                }

            }
            //除数不能为0
            float tan = (float)(Math.Atan(ACTSystem.ACTMathUtils.Abs((endy - starty) / (endx - startx))) * 180 / Math.PI);
           
            if (endx > startx && endy > starty)//第一象限
            {
                return tan;
            }
            else if (endx > startx && endy < starty)//第二象限
            {
                return -tan;
            }
            else if (endx < startx && endy > starty)//第三象限
            {
                return 180 - tan;
            }
            else
            {
                return tan - 180;
            }

        }

        public void SetLineWidth(float width)
        {
            lineRect.sizeDelta = new Vector2(width, lineRect.sizeDelta.y);
        }

        public void SetRotation(float angle)
        {
            rect.rotation = Quaternion.Euler(0, 0, angle);
        }
    }
}
