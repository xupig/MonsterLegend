﻿using ACTSystem;
using UnityEngine;
using UnityEngine.EventSystems;
using Common.ExtendTools;

namespace GameMain
{
    public class PlayerModelComponent : XContainer, IPointerClickHandler
    {
        protected RectTransform _rectTransform;
        protected GameObject _modelGo;
        protected GameObject _mountModelGo;
        protected ACTActor _playerActor;
        protected int _id = 0;

        SortOrderedRenderAgent sortOrderedRenderAgent;

        protected float _scale = 1f;
        protected Vector3 _startPosition = new Vector3(0, 0, 0);
        protected Vector3 _rotation = new Vector3(0, 180, 0);
        protected Vector3 _initRectPosition = new Vector3(0, 0, 0);

        protected float _curScale = 1f;

        protected ModelDragComponent _dragComponent;

        public ModelDragComponent dragComponent
        {
            get
            {
                return _dragComponent;
            }
        }

        protected override void Awake()
        {
            _rectTransform = this.GetComponent<RectTransform>();
            _dragComponent = this.gameObject.AddComponent<ModelDragComponent>();
            if (_rectTransform != null)
            {
                _initRectPosition = _rectTransform.localPosition;
            }
        }

        public void SetDragable(bool state)
        {
            this._dragComponent.dragable = state;
        }

        private void SetModelSetting()
        {
            var characterController = _modelGo.GetComponent<CharacterController>();
            characterController.enabled = false;
            var actor = _modelGo.GetComponent<ACTActor>();
            if (actor != null) actor.actorController.isStatic = true;
            float Scale = 1;
            if (_rectTransform != null)
            {
                Scale = _scale * _rectTransform.sizeDelta.y / characterController.height;
                //_modelGo.transform.position = new Vector3(0.5f * _rectTransform.sizeDelta.x, 0, 0) + _startPosition + _rectTransform.position;
                _modelGo.transform.position = new Vector3(0.5f * _rectTransform.sizeDelta.x, 0, 0) + _startPosition;
            }
            else
            {
                _modelGo.transform.position = _startPosition;
            }
            _modelGo.transform.localScale = new Vector3(Scale, Scale, Scale);
            _modelGo.transform.localRotation = Quaternion.Euler(_rotation);

            _modelGo.transform.SetParent(transform, false);

            sortOrderedRenderAgent = _modelGo.AddComponentOnlyOne<SortOrderedRenderAgent>();
            SortOrderedRenderAgent.ReorderAll();
            ModelComponentUtil.ChangeUIModelParam(_modelGo);
        }

        public void SetStartSetting(Vector3 position, Vector3 euler, float scale)
        {
            if (EntityPlayer.ShowPlayer == null || EntityPlayer.ShowPlayer.GetTransform() == null)
            {
                return;
            }
            EntityPlayer.ShowPlayer.ResetSkillShowAvater();
            _modelGo = EntityPlayer.ShowPlayer.GetTransform().gameObject;
            _dragComponent.SetModel(_modelGo);
            _startPosition = position;
            _rotation = euler;
            _scale = scale;
            SetModelSetting();
        }

        public void PlayFx(string path, Vector3 relativePos, float duration)
        {
            if (_modelGo != null)
            {
                Vector3 position = _modelGo.transform.position + relativePos;
                ACTVisualFXManager.GetInstance().PlayACTVisualFX(path, position, Vector3.zero, duration, 0, VisualFXPriority.H, sortOrderedRenderAgent.order);
            }
        }

        private Vector3 _hidePos = new Vector3(-1000, -1000, 0);
        public void Hide()
        {
            if (_rectTransform != null)
            {
                _rectTransform.localPosition = _hidePos;
            }
        }

        public void Show()
        {
            if (_rectTransform != null)
            {
                _rectTransform.localPosition = _initRectPosition;
            }
        }

        public void HideFx()
        {
            if (_modelGo != null)
            {
                ACTSystemTools.SetUIFxLayerShow(_modelGo.transform, false);
            }
        }

        public void ShowFx()
        {
            if (_modelGo != null)
            {
                ACTSystemTools.SetUIFxLayerShow(_modelGo.transform, true);
            }
        }
    }


}
