﻿using ACTSystem;
using GameLoader.Utils;
using GameResource;
using System;
using UnityEngine;

namespace GameMain
{
    public class EquipModelComponent : PathModelComponent
    {
        private int _id;
        private bool _isSelfRotate;//展示模型是否自转
        protected Vector3 _initRectPosition = new Vector3(0, 0, 0);
        private XGameObjectTweenRotation rotator;
        private Vector3 start = new Vector3(0,0,0);
        private Vector3 stop = new Vector3(0, 0, 360);

        protected override void Awake()
        {
            _rectTransform = this.GetComponent<RectTransform>();
            if (_rectTransform != null)
            {
                _initRectPosition = _rectTransform.localPosition;
            }
        }

        public void LoadEquipModel(int equipID, string controllerPath = "")
        {
            if (_id != equipID)
            {
                ClearModelGameObject();
                _id = equipID;
                var actorData = ACTRunTimeData.GetEquipmentData(equipID);
                if (actorData == null)
                {
                    LoggerHelper.Error("LoadEquip数据配置错误 actorID = " + equipID);
                    return;
                }
                string modelPath = actorData.GetModelAssetPath();
                if (string.IsNullOrEmpty(controllerPath))
                {
                    LoadModel(modelPath, (obj) =>
                        {
                            if (this._isSelfRotate)
                            {
                                StartSelfRotate(obj);
                            }
                        });
                }
                else
                {
                    LoadModel(modelPath, (obj) =>
                    {
                        var animatorProxy = obj.GetComponent<AnimatorProxy>();
                        if (animatorProxy == null)
                        {
                            animatorProxy = obj.AddComponent<AnimatorProxy>();
                        }
                        animatorProxy.SetController(controllerPath, () =>
                        {
                        }
                        );

                        if (this._isSelfRotate)
                        {
                            StartSelfRotate(obj);
                        }
                    });
                }
            }
        }

        private Vector3 _hidePos = new Vector3(-4000, -4000, 0);
        public void Hide()
        {
            if (_rectTransform != null)
            {
                _rectTransform.localPosition = _hidePos;
            }
        }

        public void Show()
        {
            if (_rectTransform != null)
            {
                _rectTransform.localPosition = _initRectPosition;
            }
        }

        private void StartSelfRotate(GameObject obj)
        {
            Transform root = obj.transform.FindChild("root");
            if(root == null){
                LoggerHelper.Error("模型" + obj.name + "缺少root节点");
                return;
            }
            rotator = root.gameObject.GetComponent<XGameObjectTweenRotation>();
            if (rotator == null)
            {
                rotator = root.gameObject.AddComponent<XGameObjectTweenRotation>();
            }
            
            start.x = root.eulerAngles.x;
            start.y = root.eulerAngles.y;
            stop.x = root.eulerAngles.x;
            stop.y = root.eulerAngles.y;
            rotator.IsTweening = true;
            rotator.SetFromToRotation(start, stop, 3.6f, 2, null);
        }

        public void SetSelfRotate(bool value)
        {
            this._isSelfRotate = value;
        }
    }
}
