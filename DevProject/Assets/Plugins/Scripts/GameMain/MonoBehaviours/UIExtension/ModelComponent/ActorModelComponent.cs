﻿using ACTSystem;
using Common.Data;
using GameData;
using System;
using UnityEngine;

namespace GameMain
{
    public class ActorModelComponent : ActModelComponent
    {
        public void LoadModel(int actorID)
        {
            DoLoadActor(actorID, (actor) => 
            {
                //诡异的特效显示问题
                actor.gameObject.SetActive(false);
                actor.gameObject.SetActive(true); 
            }, true);
        }

        public void LoadAvatarModel(int vocation, string equipInfo = "", bool packUpWeapon = false)
        {
            if (vocation == 0)
            {
                GameLoader.Utils.LoggerHelper.Error("LoadAvatarModel Error vocation == Vocation.None");
                return;
            }
            int actorID = role_data_helper.GetModel((int)vocation);
            DoLoadActor(actorID, (actor) =>
            {
                actor.equipController.equipCloth.isMaterialHQ = true;
                actor.equipController.equipCloth.defaultEquipID = role_data_helper.GetDefaultCloth((int)vocation);
                actor.equipController.equipWeapon.defaultEquipID = role_data_helper.GetDefaultWeapon((int)vocation);
                actor.equipController.equipHead.defaultEquipID = role_data_helper.GetDefaultHead((int)vocation);

                actor.animationController.SetCullingMode(AnimatorCullingMode.AlwaysAnimate);
                var avatarModelData = ModelEquipTools.CreateAvatarModelData(vocation, equipInfo);
                var weaponInfo = avatarModelData.GetEquipInfo(ACTEquipmentType.Weapon);
                var clothInfo = avatarModelData.GetEquipInfo(ACTEquipmentType.Cloth);
                var headInfo = avatarModelData.GetEquipInfo(ACTEquipmentType.Head);
                var wingInfo = avatarModelData.GetEquipInfo(ACTEquipmentType.Wing);
                var effectInfo = avatarModelData.GetEquipInfo(ACTEquipmentType.Effect);
                // 加载完成之后，需要改动特效或者流光的效果
                actor.equipController.equipWeapon.onLoadParticleFinished = OnLoadFinished;
                actor.equipController.equipCloth.onLoadParticleFinished = OnLoadFinished;
                actor.equipController.equipWeapon.onLoadFlowFinished = OnLoadFinished;
                actor.equipController.equipCloth.onLoadFlowFinished = OnLoadFinished;
                actor.equipController.equipWeapon.onLoadEquipFinished = OnLoadFinished;
                actor.equipController.equipCloth.onLoadEquipFinished = OnLoadFinished;

                actor.equipController.equipCloth.PutOn(clothInfo.equipID, clothInfo.particle, clothInfo.flow);
                actor.equipController.equipWeapon.PutOn(weaponInfo.equipID, weaponInfo.particle, weaponInfo.flow);
                actor.equipController.equipHead.PutOn(headInfo.equipID, headInfo.particle, headInfo.flow);
                actor.equipController.equipWing.PutOn(wingInfo.equipID, wingInfo.particle, wingInfo.flow);
                actor.equipController.equipEffect.PutOn(effectInfo.equipID, effectInfo.particle, effectInfo.flow);

                ACTSystemAdapter.SetUIActorController(actor, packUpWeapon, (int)vocation, () =>
                {
                    OnModelPrefabLoaded(actor);
                });
            }, true);
        }

        public void LoadNpcModel(int actorID, string equipInfo = "", bool inCity = true, int vocation = 0)
        {
            DoLoadActor(actorID, (actor) =>
            {
                actor.equipController.equipCloth.isMaterialHQ = true;
                actor.animationController.SetCullingMode(AnimatorCullingMode.AlwaysAnimate);
                var avatarModelData = ModelEquipTools.CreateNpcModelData(equipInfo);
                var weaponInfo = avatarModelData.GetEquipInfo(ACTEquipmentType.Weapon);
                var clothInfo = avatarModelData.GetEquipInfo(ACTEquipmentType.Cloth);
                var headInfo = avatarModelData.GetEquipInfo(ACTEquipmentType.Head);
                var wingInfo = avatarModelData.GetEquipInfo(ACTEquipmentType.Wing);

                // 加载完成之后，需要改动特效或者流光的效果
                actor.equipController.equipWeapon.onLoadParticleFinished = OnLoadFinished;
                actor.equipController.equipCloth.onLoadParticleFinished = OnLoadFinished;
                actor.equipController.equipWeapon.onLoadFlowFinished = OnLoadFinished;
                actor.equipController.equipCloth.onLoadFlowFinished = OnLoadFinished;
                actor.equipController.equipWeapon.onLoadEquipFinished = OnLoadFinished;
                actor.equipController.equipCloth.onLoadEquipFinished = OnLoadFinished;

                actor.equipController.equipCloth.PutOn(clothInfo.equipID, clothInfo.particle, clothInfo.flow);
                actor.equipController.equipWeapon.PutOn(weaponInfo.equipID, weaponInfo.particle, weaponInfo.flow);
                actor.equipController.equipWing.PutOn(wingInfo.equipID, wingInfo.particle, wingInfo.flow);
                actor.equipController.equipHead.PutOn(headInfo.equipID);

                if (vocation > 0)
                {
                    ACTSystemAdapter.SetUIActorController(actor, !inCity, (int)vocation, () =>
                    {
                        OnModelPrefabLoaded(actor);
                    });
                }
            }, true);
        }

        private void DoLoadActor(int actorID, Action<ACTActor> callback = null, bool layoutImmediately = true)
        {
            if (_id != actorID)
            {
                ClearModelGameObject();
                _id = actorID;
                ACTSystemAdapter.CreateActor(actorID, (actor) =>
                {
                    if (actor == null)
                    {
                        GameLoader.Utils.LoggerHelper.Error("CreateActor error in ModelComponent " + actorID);
                        return;
                    }
                    if (actorID != _id)
                    {
                        GameObject.Destroy(actor.gameObject);
                        return;
                    }
                    if (layoutImmediately == true)
                    {
                        OnModelPrefabLoaded(actor);
                    }
                    if (callback != null)
                    {
                        callback(actor);
                    }
                }, true);
            }
            else
            {
                if (callback != null && _modelGo != null)
                {
                    callback(_modelGo.GetComponent<ACTActor>());
                }
            }
        }

        private Vector3 _hidePos = new Vector3(-1000, -1000, 0);
        public void Hide()
        {
            if (_rectTransform != null)
            {
                _rectTransform.localPosition = _hidePos;
            }
        }

        public void Show()
        {
            if (_rectTransform != null)
            {
                _rectTransform.localPosition = _initRectPosition;
            }
        }

        public void HideFx()
        {
            if (_modelGo != null)
            {
                ACTSystemTools.SetUIFxLayerShow(_modelGo.transform, false);
            }
        }

        public void ShowFx()
        {
            if (_modelGo != null)
            {
                ACTSystemTools.SetUIFxLayerShow(_modelGo.transform, true);
            }
        }
    }
}
