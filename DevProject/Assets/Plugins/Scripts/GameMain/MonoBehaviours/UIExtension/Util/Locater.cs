﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 用于GameObject位置调整
/// </summary>
namespace GameMain
{
    public class Locater : UIBehaviour
    {
        private RectTransform _rect;

        protected override void Awake()
        {
            base.Awake();
            //_rect = GetComponent<RectTransform>();
        }

        private RectTransform Rect
        {
            get
            {
                if (_rect == null)
                {
                    _rect = GetComponent<RectTransform>();
                }
                return _rect;
            }
        }

        public float X
        {
            get
            {
                return Rect.anchoredPosition.x;
            }
            set
            {
                Rect.anchoredPosition = new Vector2(value, this.Y);
            }
        }

        public float Y
        {
            get
            {
                return Rect.anchoredPosition.y;
            }
            set
            {
                Rect.anchoredPosition = new Vector2(this.X, value);
            }
        }

        public Vector2 Position
        {
            get
            {
                return Rect.anchoredPosition;
            }
            set
            {
                Rect.anchoredPosition = value;
            }
        }

        public virtual float Width
        {
            get
            {
                return Rect.sizeDelta.x;
            }
        }

        public virtual float Height
        {
            get
            {
                return Rect.sizeDelta.y;
            }
        }
    }
}
