﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
//using UIExtension;

namespace GameMain
{
    /// <summary>
    /// 添加到ListItem上
    /// 在某一范围内时渲染
    /// 在范围外时不渲染
    /// </summary>
    public class ListItemRenderingAgent : MonoBehaviour
    {
        private static ChildrenComponentsPool<List<MaskableGraphic>> _graphicListPool = new ChildrenComponentsPool<List<MaskableGraphic>>(null, l => l.Clear());
        private static Vector3 HIDE_SCALE = new Vector3(0, 1, 1);
        private RectTransform _boundary;
        private RectTransform _parentRectTransform;
        private RectTransform _selfRectTransform;

        private RectTransform _contentContainer;

        private bool _visible = true;

        public bool Visible
        {
            get { return _visible; }
            set { _visible = value; }
        }

        public LuaUIListItem Item { get; set; }

        protected void Awake()
        {
            _selfRectTransform = GetComponent<RectTransform>();
            _parentRectTransform = transform.parent.GetComponent<RectTransform>();
            var contRect = this.transform.FindChild("Container_content");
            if (contRect)
            {
                _contentContainer = contRect.GetComponent<RectTransform>();
            }
        }

        public void SetBoundary(RectTransform boundary)
        {
            _boundary = boundary;
        }

        /// <summary>
        /// 打印这里的具体值来测试
        /// </summary>
        protected void LateUpdate()
        {
            Vector2 selfTopLeft = _parentRectTransform.anchoredPosition + _selfRectTransform.anchoredPosition;
            Vector2 selfBottomRight = selfTopLeft + new Vector2(_selfRectTransform.sizeDelta.x, - _selfRectTransform.sizeDelta.y);
            Vector2 boundaryTopLeft = new Vector2(0, 0);
            Vector2 boundaryBottomRight = boundaryTopLeft + new Vector2(_boundary.sizeDelta.x, - _boundary.sizeDelta.y);
            bool visible = true;
            if(selfBottomRight.x <= boundaryTopLeft.x || selfBottomRight.y >= boundaryTopLeft.y || selfTopLeft.y <= boundaryBottomRight.y || selfTopLeft.x >= boundaryBottomRight.x)
            {
                visible = false;
            }
            if(visible != _visible)
            {
                _visible = visible;
                IncludeInRendering(_visible);
            }

            if(this.Item != null && _visible == true)
            {
                if(this.Item.IsDataDirty == true)
                {
                    this.Item.DoRefreshData();
                    this.Item.IsDataDirty = false;
                }
            }
        }

        /// <summary>
        /// 当isOverlaped为True的时候渲染，否则不渲染
        /// </summary>
        /// <param name="visible"></param>
        private void IncludeInRendering(bool visible)
        {
            if (_contentContainer == null) return;
            if (visible == false)
            {
                _contentContainer.localScale = HIDE_SCALE;
            }
            else
            {
                _contentContainer.localScale = Vector3.one;
            }
        }

    }
}
