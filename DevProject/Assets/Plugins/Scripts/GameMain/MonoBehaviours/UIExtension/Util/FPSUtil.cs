﻿using UnityEngine;
using System.Collections;
using GameMain.GlobalManager;
using System.Collections.Generic;

namespace GameMain
{
    public class FPSUtil : DriverMonoBehaviour
    {
        static public float fps = 0;
        static public float averageFps = 30;
        static public bool lowFps = false;
        static private bool _pause = false;
        static private Dictionary<int, bool> _pauseState = new Dictionary<int, bool>(2);
        float _updateInterval = 0.5f;

        float _lastInterval = 0; // Last interval end time
        float _frames = 0; // Frames over current interval

        float _statisticalTimes = 60;  //统计时长
        float _statisticalAllFPS = 0;
        float _statisticalAllCount = 0;

        void Awake()
        {
        }

        // Use this for initialization
        void Start()
        {
            _lastInterval = UnityPropUtils.realtimeSinceStartup;
            _frames = 0;
        }

        // Update is called once per frame
        void Update()
        {
            if (_pause)
                return;
            ++_frames;
            var timeNow = UnityPropUtils.realtimeSinceStartup;
            if (timeNow > _lastInterval + _updateInterval)
            {
                fps = _frames / (timeNow - _lastInterval);
                _frames = 0;
                _lastInterval = timeNow;
                _statisticalAllFPS = _statisticalAllFPS + fps;
                _statisticalAllCount++;
                if (_statisticalAllCount >= _statisticalTimes)
                {
                    averageFps = _statisticalAllFPS / _statisticalAllCount;
                    if (averageFps < 20)
                        lowFps = true;  //平均帧率少于20帧做个标记
                    else
                        lowFps = false;
                    _statisticalAllFPS = 0;
                    _statisticalAllCount = 0;
                }
            }
        }


        #region 设置检测开关
        static public void InitPauseState(int[] keys, bool isPause)
        {
            _pauseState.Clear();
            for (int i = 0; i < keys.Length; i++)
            {
                _pauseState.Add(keys[i], false);
            }
            UpdatePauseState();
        }

        static public void SetPauseState(int key, bool isPause)
        {
            _pauseState[key] = isPause;
            UpdatePauseState();
        }

        static private void UpdatePauseState()
        {
            bool state = false;
            foreach (KeyValuePair<int, bool> pair in _pauseState)
            {
                state |= pair.Value; // 全为false才开启检测
            }
            _pause = state;
        }
        #endregion
    }
}