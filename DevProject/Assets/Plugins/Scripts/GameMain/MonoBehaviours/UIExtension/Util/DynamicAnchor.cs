﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 用于GameObject位置调整
/// </summary>
namespace GameMain
{
    public class DynamicAnchor : UIBehaviour
    {
        private RectTransform _rect;
        private RectTransform Rect
        {
            get
            {
                if (_rect == null)
                {
                    _rect = GetComponent<RectTransform>();
                }
                return _rect;
            }
        }

        public Vector2 Pivot
        {
            get
            {
                return Rect.pivot;
            }
            set
            {
                Rect.pivot = value;
            }
        }

        private Vector2 _screenAnchoredPosition;
        public Vector2 ScreenAnchoredPosition
        {
            set
            {
                _screenAnchoredPosition = value;
                Refresh();
            }
        }

        void Refresh()
        {
            Vector2 localPoint;
            var parentRect = this.transform.parent.gameObject.GetComponent<RectTransform>();
            Vector2 screenPoint = new Vector2(_screenAnchoredPosition.x * ScreenUtil.ScreenWidth, _screenAnchoredPosition.y * ScreenUtil.ScreenHeight);
            bool result = RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRect, screenPoint, XUICamera.Camera, out localPoint);
            Rect.anchoredPosition = localPoint;
        }

        protected override void OnEnable()
        {
            Refresh();
        }
    }
}
