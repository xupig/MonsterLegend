﻿using GameData;
using System.Collections.Generic;
using UIExtension;
using UnityEngine;

namespace GameMain
{

    public class XImageChanger : MonoBehaviour
    {
        ImageWrapper _image = null;
        ImagesProviderProxy _provider = null;
        RectTransform _rectTransform = null;
        void Awake()
        {
            _image = gameObject.GetComponent<ImageWrapper>();
            var provider = gameObject.GetComponent<ImagesProvider>();
            _provider = ImagesProviderProxy.GetProxy(provider);
            _rectTransform = GetComponent<RectTransform>();
        }

        public void ChangeImage(string name)
        {
            if (_image == null || _provider == null) return;
            var dic = _provider.imageWrappersDict;
            if (dic.ContainsKey(name))
            {
                var newImage = dic[name];
                if (newImage != null)
                {
                    _image.sprite = newImage.sprite;
                    _image.material = newImage.material;
                }
            }
        }

        public void ChangeImageByGO(GameObject go)
        {
            var imageWrapper = go.GetComponent<ImageWrapper>();
            if (imageWrapper != null) {
                _image.sprite = imageWrapper.sprite;
                _rectTransform.sizeDelta = go.GetComponent<RectTransform>().sizeDelta;
            }
        }
    }
}
