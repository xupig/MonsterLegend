﻿using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.Events;
//using UnityEngine.EventSystems;
using System;
//using System.Text;
using System.Text.RegularExpressions;
//using System.Collections;
using System.Collections.Generic;

namespace GameMain
{
    /// <summary>
    /// Usage Example:
    /// GameObject canvas = GameObject.Find("Canvas");

    /// FontData fontData = new FontData();
    /// fontData.alignment = TextAnchor.LowerLeft;
    /// fontData.font = font; 
    /// fontData.fontSize = 20;
    /// fontData.fontStyle = FontStyle.Normal;
    /// fontData.lineSpacing = 0.5f;
    /// fontData.richText = true;

    /// string content = "每年生日都很让人期待，因为[face01]生日当天可以收到很多很多[face02]祝福，还可以收到<color=#ff0000>礼物</color>，还可以当天我最大，还可以";

    /// TextBlock block = new TextBlock(content, new Regex(@"\[face\d{1,2}\]"), 500.0f, fontData, Color.blue);
    /// block.GameObject.transform.parent = canvas.transform;
    /// block.GameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
    /// </summary>

    public class TextBlock
    {
        private static int _index = 0;

        public static TextGenerationSettings textGenerationSettings = new TextGenerationSettings();
        public static TextGenerator textGenerator = new TextGenerator();

        public string Content { get; private set; }
        public Regex GraphicPattern { get; private set; }
        public float Width { get; private set; }
        public float Height { get; private set; }
        public FontData FontData { get; private set; }
        public Color FontColor { get; private set; }
        public HtmlHelper HtmlHelper { get; private set; }
        public GameObject GameObject { get; private set; }

        protected Queue<ContentElement> _elementQueue;
        protected ContentElement _currentElement;
        protected List<TextLine> _lineList;
        protected float _maxWidth;

        protected static void UpdateTextGenerationSettings(FontData fontData, Color fontColor)
        {
            textGenerationSettings.textAnchor = fontData.alignment;
            textGenerationSettings.color = fontColor;
            textGenerationSettings.pivot = new Vector2(0.0f, 1.0f);
            textGenerationSettings.richText = fontData.richText;
            textGenerationSettings.font = fontData.font;
            //textGenerationSettings.fontSize = fontData.fontSize;
            //textGenerationSettings.fontStyle = fontData.fontStyle;
            textGenerationSettings.horizontalOverflow = HorizontalWrapMode.Overflow;
            textGenerationSettings.verticalOverflow = VerticalWrapMode.Overflow;
            textGenerationSettings.generateOutOfBounds = true;
            textGenerationSettings.lineSpacing = fontData.lineSpacing;
            textGenerationSettings.generationExtents = new Vector2(int.MaxValue, fontData.fontSize * (1.0f + textGenerationSettings.lineSpacing));
        }

        public static Vector2 GetTextContentSize(string content)
        {
            return new Vector2(textGenerator.GetPreferredWidth(content, textGenerationSettings), textGenerator.GetPreferredHeight(content, textGenerationSettings));
        }

        public TextBlock(string content, Regex graphicPattern, float maxWidth, FontData fontData, Color fontColor)
        {
            this.Content = content;
            this.GraphicPattern = graphicPattern;
            this.FontData = fontData;
            this.FontColor = fontColor;
            UpdateTextGenerationSettings(fontData, fontColor);
            this.HtmlHelper = new HtmlHelper();
            _lineList = new List<TextLine>();
            _maxWidth = maxWidth;
        }

        public void Build()
        {
            if (this.FontData.richText == true && this.HtmlHelper.Validate(this.Content) == false)
            {
                throw new Exception("Html validation failed!!");
            }
            CreateGameObject();
            DoBeforeParseContent();
            ParseContent();
            ShowContent();
            UpdateBlockSize();
        }

        private void CreateGameObject()
        {
            this.GameObject = new GameObject(GetGameObjectName());
            GameObjectHelper.AddBottomLeftRectTransform(this.GameObject, Vector2.zero, Vector2.zero);
        }

        protected virtual string GetGameObjectName()
        {
            return string.Format("textBlock_{0}", (_index++).ToString());
        }

        protected virtual void DoBeforeParseContent()
        {

        }

        private void ParseContent()
        {
            _elementQueue = new Queue<ContentElement>();
            int index = 0;
            string graphicElementContent;
            string textElementContent;
            while (this.GraphicPattern.IsMatch(this.Content, index) == true)
            {
                //GameLoader.Utils.LoggerHelper.Error(this.GraphicPattern.ToString());
                graphicElementContent = this.GraphicPattern.Match(this.Content, index).Value;
                int graphicIndex = this.Content.IndexOf(graphicElementContent, index);
                textElementContent = this.Content.Substring(index, (graphicIndex - index));
                //GameLoader.Utils.LoggerHelper.Error(graphicElementContent + " graphicElementContent textElementContent: " + textElementContent);
                if (textElementContent.Length > 0)
                {
                    _elementQueue.Enqueue(CreateTextElement(textElementContent));
                }
                _elementQueue.Enqueue(CreateOtherElement(graphicElementContent));
                index = graphicIndex + graphicElementContent.Length;
            }
            textElementContent = this.Content.Substring(index);
            _elementQueue.Enqueue(CreateTextElement(textElementContent));
        }

        private string GetTextElementContent(string elementContent)
        {
            string leftTag = this.HtmlHelper.GetLeftTag();
            this.HtmlHelper.UpdateTagMatchStack(elementContent);
            string rightTag = this.HtmlHelper.GetRightTag();
            elementContent = leftTag + elementContent + rightTag;
            return elementContent;
        }

        protected virtual ContentElement CreateTextElement(string elementContent)
        {
            return new TextElement(elementContent, this);
        }

        protected virtual ContentElement CreateOtherElement(string elementContent)
        {
            return new GraphicElement(elementContent, this);
        }

        protected virtual void ShowContent()
        {
            TextLine line = CreateTextLine(null, _maxWidth);
            while (line != null)
            {
                _lineList.Add(line);
                line = CreateTextLine(line, _maxWidth);
            }
            DoLineListLayout();
        }

        protected virtual TextLine CreateTextLine(TextLine previousLine, float width)
        {
            if (GetCurrentElement() == null)
            {
                return null;
            }
            TextLine line = new TextLine(this, _maxWidth);
            GameObject fragment = line.CreateFragment(GetCurrentElement());
            while (fragment != null)
            {
                fragment = line.CreateFragment(GetCurrentElement());
            }
            GameObjectHelper.AddToParent(line.GameObject, this.GameObject);
            if (line.Width > this.Width)
            {
                this.Width = line.Width;
            }
            return line;
        }

        private ContentElement GetCurrentElement()
        {
            if (_currentElement == null || (_currentElement.IsFinished == true && _elementQueue.Count > 0))
            {
                _currentElement = _elementQueue.Dequeue();
                _currentElement.PopulateContent();
                return _currentElement;
            }
            if (_currentElement.IsFinished == false)
            {
                return _currentElement;
            }
            return null;
        }

        private void UpdateBlockSize()
        {
            GameObjectHelper.ResizeRectTransform(GameObject, new Vector2(Width, Height));
        }

        protected virtual void DoLineListLayout()
        {
            this.Height = 0;
            for (int i = 0; i < _lineList.Count; i++)
            {
                TextLine line = _lineList[i];
                GameObjectHelper.MoveRectTransform(line.GameObject, new Vector2(0, -this.Height));
                this.Height += line.Height;
            }
        }

        public float MaxWidth
        {
            get
            {
                return _maxWidth;
            }
        }

        public int LineCount
        {
            get
            {
                return _lineList.Count;
            }
        }
    }
}

