﻿#region 模块信息
/*==========================================
// 文件名：ChatWantDonateLinkWrapper
// 命名空间: GameLogic.GameLogic.Common.Chat
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/3/11 9:51:56
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameMain
{
    public class ChatWantDonateLinkWrapper
    {
        public string desc;
        public uint time;
        public uint id;

        public ChatWantDonateLinkWrapper(string desc, uint time, uint id)
        {
            this.desc = desc;
            this.time = time;
            this.id = id;
        }
    }
}
