﻿using UnityEngine;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using MogoEngine.Events;
using UIExtension;

namespace GameMain
{
    public class ChatPrivateRespElement : GraphicElement
    {
        private static Regex PLAYER_NAME_PATTERN = new Regex(@"(?<=(\#resp,)).*?(?=\,)");
        private static Regex PLAYER_ID_PATTERN = new Regex(@"(?<=\,)\d{1,20}(?=\#)");

        private ulong _playerDbid = 0;
        private string _playerName = string.Empty;
        private static string _labelText = "_labelText";// MogoLanguageUtil.GetContent(32544);

        public ChatPrivateRespElement(string content, ChatTextBlock textBlock)
            : base(content, textBlock)
        {

        }

        protected override void AddFragmentComponent(GameObject go)
        {
            TextWrapper text = ChatTextBlock.CreateChatLinkTextComponent(go);
            text.text = _labelText;
            text.SetDimensionsDirty();
            Button btn = go.AddComponent<Button>();
            btn.onClick.AddListener(OnRespClick);
        }

        private ulong GetPlayerDbid()
        {
            if (_playerDbid == 0)
            {
                ulong.TryParse(PLAYER_ID_PATTERN.Match(this.Content).Value, out _playerDbid);
            }
            return _playerDbid;
        }

        private string GetPlayerName()
        {
            if (string.IsNullOrEmpty(_playerName))
            {
                _playerName = PLAYER_NAME_PATTERN.Match(this.Content).Value;
            }
            return _playerName;
        }

        private void OnRespClick()
        {
            ulong playerDbid = GetPlayerDbid();
            string playerName = GetPlayerName();
            if (playerDbid != 0 && playerDbid != EntityPlayer.Player.id && !string.IsNullOrEmpty(playerName))
            {
                //EventDispatcher.TriggerEvent<ulong, string>(ChatEvents.ON_CLICK_RESPONSE_PRIVATE_PLAYER, playerDbid, playerName);
                GameLoader.Utils.LoggerHelper.Debug(" ChatPrivateRespElement OnRespClick");
            }
        }

        protected override Vector2 GetFragmentSize()
        {
            return TextBlock.GetTextContentSize(_labelText);
        }

        protected override Vector2 GetFragmentPostion()
        {
            ChatTextBlock chatBlock = this.TextBlock as ChatTextBlock;
            Vector2 size = GetFragmentSize();
            Vector2 pos = new Vector2(chatBlock.MaxWidth - size.x, 0f);
            return Vector2.zero;
        }
    }
}
