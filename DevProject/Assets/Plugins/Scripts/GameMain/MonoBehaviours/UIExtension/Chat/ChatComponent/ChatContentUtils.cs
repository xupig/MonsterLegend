﻿using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;

namespace GameMain
{
    public class ChatContentUtils
    {
        public static string NAME_IN_RRONT_OF_DESC_FORMAT = "{0} :";
        public static string LINK_SEND_TIME_FORMAT = "<time={0}>";  //参数以秒为单位
        public static Regex LINK_SEND_TIME_PATTERN = new Regex(@"<time=\d{1,20}>");
        public static Regex LINK_SEND_TIME_VALUE_PATTERN = new Regex(@"(?<=\<time\=)\d{1,20}(?=\>)");

        public static string CreateLinkContent(string linkDesc, string linkData, string playerName, int linkType, float maxWidth)
        {
            return string.Format("[{0},{1},{2}]", linkDesc, (int)linkType, linkData);
        }

        public static List<string> ProcessChatLinkDesc(string srcDesc, float maxWidth, TextGenerationSettings textGenerationSettings)
        {
            //TextGenerator的实例如果在传入参数TextGenerationSettings的设置有变化时，在计算characters的字符宽度时可能会有问题，
            //因此在用到时，就单独创建实例
            TextGenerator contentGenerator = new TextGenerator();
            TextFitWidthHelper textFitWidthHelper = new TextFitWidthHelper(contentGenerator, textGenerationSettings);
            List<string> descList = textFitWidthHelper.GetFitSplitContentList(srcDesc, maxWidth);
            return descList;
        }

        public static string GetOnePartFitContent(string srcContent, float maxWidth, TextGenerationSettings textGenerationSettings)
        {
            TextGenerator contentGenerator = new TextGenerator();
            TextFitWidthHelper textFitWidthHelper = new TextFitWidthHelper(contentGenerator, textGenerationSettings);
            string result = textFitWidthHelper.GetFitContent(srcContent, maxWidth);
            return result;
        }

        public static ulong GetSendLinkTimeStamp(string linkContent)
        {
            ulong timeStamp = 0;
            string strTime = LINK_SEND_TIME_PATTERN.Match(linkContent).Value;
            if (!string.IsNullOrEmpty(strTime))
            {
                strTime = LINK_SEND_TIME_VALUE_PATTERN.Match(strTime).Value;
                ulong.TryParse(strTime, out timeStamp);
            }
            return timeStamp;
        }

        public static string GetLinkContentWithoutSendTime(string linkContent)
        {
            string destContent = linkContent;
            string strTime = LINK_SEND_TIME_PATTERN.Match(linkContent).Value;
            if (!string.IsNullOrEmpty(strTime))
            {
                destContent = linkContent.Replace(strTime, "");
            }
            return destContent;
        }

        public static Vector2 GetContentSize(string content, TextGenerationSettings textGenerationSettings)
        {
            TextGenerator textGenerator = new TextGenerator();
            return new Vector2(textGenerator.GetPreferredWidth(content, textGenerationSettings), textGenerator.GetPreferredHeight(content, textGenerationSettings));
        }

    }
}
