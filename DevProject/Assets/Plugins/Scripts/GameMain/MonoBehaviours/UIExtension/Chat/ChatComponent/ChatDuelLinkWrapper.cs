﻿using Common.Data;

namespace GameMain
{
    public class ChatDuelLinkWrapper : ChatLinkBaseWrapper
    {
        public int MatchId;
        public string Name;

        public ChatDuelLinkWrapper(int matchId, string name)
            : base(ChatLinkType.Duel)
        {
            this.MatchId = matchId;
            this.Name = name;
        }

        protected override void ParseDataString(string[] dataStringSplits)
        {

        }

        public override string ToDataString()
        {
            return string.Empty;
        }
    }
}
