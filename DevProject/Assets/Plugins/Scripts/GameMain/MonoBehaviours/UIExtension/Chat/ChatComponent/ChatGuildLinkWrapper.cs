﻿using Common.Data;

namespace GameMain
{
    public class ChatGuildLinkWrapper : ChatLinkBaseWrapper
    {
        public string name;
        public ulong guildId;
        public string data = "";

        private const string GUILD_LINK_TEMPLATE = "[{0};{1}]";

        public static ChatGuildLinkWrapper CreateLinkWrapper(string[] dataStringSplits)
        {
            return new ChatGuildLinkWrapper(dataStringSplits);
        }

        public ChatGuildLinkWrapper(string[] dataStringSplits)
            : base(ChatLinkType.Guild, dataStringSplits)
        {

        }

        public ChatGuildLinkWrapper(ulong guildId, string name)
            : base(ChatLinkType.Guild)
        {
            this.guildId = guildId;
            this.name = name;
            _linkDesc = name;
        }

        protected override void ParseDataString(string[] dataStringSplits)
        {
            data = string.Join(",", dataStringSplits);
            this.guildId = ulong.Parse(dataStringSplits[1]);
        }

        public override string ToDataString()
        {
            return data;// string.Format(GUILD_LINK_TEMPLATE, ((int)LinkType).ToString(), guildId.ToString());
        }
    }
}
