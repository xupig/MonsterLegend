﻿using UnityEngine;
using UnityEngine.UI;
using Common.Data;
using GameData;


namespace GameMain
{
    public class ChatGuildLinkElement : ChatLinkElement
    {

        private ChatGuildLinkWrapper _linkWrapper = null;

        public ChatGuildLinkElement(string content, ChatTextBlock textBlock, ChatLinkBaseWrapper linkWrapper)
            : base(content, textBlock)
        {
            string desc = GetLinkDesc(linkWrapper);
            _linkWrapper = linkWrapper as ChatGuildLinkWrapper;
            this.Content = desc;
        }

        protected override void AddFragmentComponent(GameObject go)
        {
            base.AddFragmentComponent(go);
            Button btn = go.AddComponent<Button>();
            btn.onClick.AddListener(OnNameClick);

            Text text = go.GetComponent<Text>();
            if(text != null)
            {
                text.raycastTarget = true;
            }
        }

        private ulong GetGuildId()
        {
            ulong guildId = _linkWrapper.guildId;
            return guildId;
        }

        private void OnNameClick()
        {
            /*int needLevel = int.Parse(global_params_helper.GetGlobalParam(82));
            if (EntityPlayer.Player.level < needLevel)// lhs if (PlayerAvatar.Player.level < needLevel)
            {
                //ari//ari ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74612, needLevel));
                return;
            }
            ulong guildId = GetGuildId();
            if (guildId != 0)
            {
                // lhs GuildManager.Instance.ApplyJoinGuild(guildId);
            }*/

            LuaDriver.instance.CallFunction("CSCALL_CHAT_TEXT_CLICK", _linkWrapper.ToDataString());
        }

        protected override Vector2 GetFragmentPostion()
        {
            return Vector2.zero;
        }
    }
}
