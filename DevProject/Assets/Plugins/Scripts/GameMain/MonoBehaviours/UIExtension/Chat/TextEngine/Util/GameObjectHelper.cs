﻿using UnityEngine;
//using UnityEngine.UI;
//using System;
//using System.Collections.Generic;
//using System.Text;

namespace GameMain
{
    public class GameObjectHelper
    {
        public static RectTransform AddBottomLeftRectTransform(GameObject go, Vector2 position, Vector2 size)
        {
            RectTransform rectTransform = go.AddComponent<RectTransform>();
            rectTransform.pivot = new Vector2(0, 0);
            rectTransform.anchorMin = new Vector2(0, 0);
            rectTransform.anchorMax = new Vector2(0, 0);
            rectTransform.sizeDelta = size;
            rectTransform.anchoredPosition = position;
            return rectTransform;
        }

        public static void ResizeRectTransform(GameObject go, Vector2 size)
        {
            RectTransform rectTransform = go.GetComponent<RectTransform>();
            rectTransform.sizeDelta = size;
        }

        public static void MoveRectTransform(GameObject go, Vector2 position)
        {
            RectTransform rectTransform = go.GetComponent<RectTransform>();
            rectTransform.anchoredPosition = position;
        }

        public static void AddToParent(GameObject child, GameObject parent)
        {
            child.transform.SetParent(parent.transform);
        }
    }
}
