﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

namespace GameMain
{
    public class TextLine
    {
        private static int _index = 0;

        private float _lineWidth;

        public GameObject GameObject { get; private set; }
        public TextBlock TextBlock { get; private set; }

        public TextLine(TextBlock textBlock, float lineWidth)
        {
            this.TextBlock = textBlock;
            _lineWidth = lineWidth;
            CreateGameObject();
        }

        private void CreateGameObject()
        {
            this.GameObject = new GameObject(GetGameObjectName());
            GameObjectHelper.AddBottomLeftRectTransform(this.GameObject, Vector2.zero, Vector2.zero);
        }

        private string GetGameObjectName()
        {
            return string.Format("textLine_{0}", _index++);
        }

        public GameObject CreateFragment(ContentElement element)
        {
            if (element == null)
            {
                return null;
            }
            if (element is GraphicElement)
            {
                GraphicElement graphicElement = element as GraphicElement;
                if (graphicElement.CheckIsOutOfLineWidth(_lineWidth))
                {
                    graphicElement.Abort();
                    Debug.LogWarning("Graphic Element width should smaller than line max width !!!");
                    return null;
                }
            }
            GameObject fragment = element.GetFragment(this.GameObject, GetAvailableWidth());
            if (fragment != null)
            {
                RectTransform rect = fragment.GetComponent<RectTransform>();
                rect.anchoredPosition = new Vector2(this.Width, rect.anchoredPosition.y);
                this.Width += rect.sizeDelta.x;
                if (rect.sizeDelta.y > this.Height)
                {
                    this.Height = rect.sizeDelta.y;
                }
                UpdateSize(this.Width, this.Height);
            }
            return fragment;
        }

        private float GetAvailableWidth()
        {
            return _lineWidth - this.Width;
        }

        private void UpdateSize(float w, float h)
        {
            GameObjectHelper.ResizeRectTransform(GameObject, new Vector2(w, h));
        }

        public virtual float Width { get; private set; }
        public virtual float Height { get; private set; }

        public Vector2 Position
        {
            get
            {
                RectTransform rect = this.GameObject.GetComponent<RectTransform>();
                return rect.anchoredPosition;
            }
            set
            {
                RectTransform rect = this.GameObject.GetComponent<RectTransform>();
                rect.anchoredPosition = value;
            }
        }

    }
}

