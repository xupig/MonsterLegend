﻿using Common.Data;

namespace GameMain
{
    public class ChatTeamLinkWrapper : ChatLinkBaseWrapper
    {
        public string name;
        public ulong playerDbid;
        public uint copyId;
        public ulong guildId;

        public int minLevel;
        public int maxLevel;

        private const string TEAM_LINK_TEMPLATE = "[{0};{1};{2};{3};{4}]";

        public static ChatTeamLinkWrapper CreateLinkWrapper(string[] dataStringSplits)
        {
            return new ChatTeamLinkWrapper(dataStringSplits);
        }

        public ChatTeamLinkWrapper(string[] dataStringSplits)
            : base(ChatLinkType.Team, dataStringSplits)
        {

        }

        public ChatTeamLinkWrapper(string name, ulong playerDbid, uint copyId, int minLevel, int maxLevel)
            : base(ChatLinkType.Team)
        {
            this.name = name;
            this.playerDbid = playerDbid;
            this.copyId = copyId;
            this.minLevel = minLevel;
            this.maxLevel = maxLevel;
            LinkDesc = name;
        }

        protected override void ParseDataString(string[] dataStringSplits)
        {
            playerDbid = ulong.Parse(dataStringSplits[1]);
            copyId = uint.Parse(dataStringSplits[2]);
            minLevel = int.Parse(dataStringSplits[3]);
            maxLevel = int.Parse(dataStringSplits[4]);
        }

        public override string ToDataString()
        {
            return string.Format(TEAM_LINK_TEMPLATE, ((int)LinkType).ToString(), playerDbid.ToString(), copyId.ToString(), minLevel.ToString(), maxLevel.ToString());
        }
    }
}
