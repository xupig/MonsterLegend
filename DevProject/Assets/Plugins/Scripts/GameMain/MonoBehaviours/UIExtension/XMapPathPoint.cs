﻿using GameLoader.Utils;
using GameMain.GlobalManager;
using MogoEngine;
using System;
using System.Collections.Generic;
using System.IO;
using UIExtension;
using UnityEngine;

namespace GameMain
{
    public class XMapPathPoint : XContainer
    {
        Transform m_myTransform;
        Transform m_pathPoint;
        float m_upX;
        float m_upY;
        float m_downX;
        float m_downY;
        int m_canvasWidth;
        int m_canvasHeight;
        float _xScale;
        float _yScale;
        List<Transform> _pathPointItems = new List<Transform>();
        List<Transform> _pathPointItemPool = new List<Transform>();
        float m_dotDistance = 10;

        public static readonly Vector3 HIDE_POSITION = new Vector3(-1000, -1400, 0);

        protected override void Awake()
        {
            base.Awake();
            m_myTransform = transform;
            m_pathPoint = this.transform.Find("Container_PathPoint");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }

        public void SetBasicPara(float upX, float upY, float downX, float downY, int canvasWidth, int canvasHeight)
        {
            m_upX = upX;
            m_upY = upY;
            m_downX = downX;
            m_downY = downY;
            m_canvasWidth = canvasWidth;
            m_canvasHeight = canvasHeight;
            _xScale = canvasWidth / (upX - downX);
            _yScale = canvasHeight / (upY - downY);
            //LoggerHelper.Error("upX: " + upX + " upY: " + upY + "downX: " + downX + " downY: " + downY + "canvasWidth: " + canvasWidth + " canvasHeight: " + canvasHeight);

        }

        public void FindPath(int screenX, int screenY)
        {
            RemoveAllPathPoint();
            //LoggerHelper.Error("screenX: " + screenX + " screenY: " + screenY);
            var targetX = screenX / _xScale + m_downX;
            var targetZ = screenY / _yScale + m_downY;
            var player = MogoWorld.Player as EntityCreature;

            //LoggerHelper.Error("targetX: " + targetX + " targetZ: " + targetZ);
            player.moveManager.Move(new Vector3(targetX, 0, targetZ), -1, PlayerMoveCallback, 0);
            var paths = player.moveManager.GetPath();
            //LoggerHelper.Error("paths: " + paths.PackList());
            ShowPaths(paths);
        }

        public void ShowPath()
        {
            RemoveAllPathPoint();
            var player = MogoWorld.Player as EntityCreature;
            var paths = player.moveManager.GetPath();
            player.moveManager.MoveEndCallBack += PlayerMoveCallback;
            ShowPaths(paths);
        }

        private void PlayerMoveCallback(EntityCreature entity)
        {
            RemoveAllPathPoint();
        }

        public void ShowPaths(List<float> paths)
        {
            if (paths.Count <= 2)
                return;
            Vector3 lastEnd = Vector3.zero;//用于记录上一段路线的终点，使得路线转折点不过于密集
            float lastMargin = 0;//用于记录上一段路线剩余描点距离，使得路线转折点不过于密集
            for (int i = 0; i + 3 < paths.Count; i += 2)
            {
                var realOneX = paths[i];
                var realOneZ = paths[i + 1];
                var mapPosOne = GetMapPos(realOneX, realOneZ);
                var realTwoX = paths[i + 2];
                var realTwoZ = paths[i + 3];
                var mapPosTwo = GetMapPos(realTwoX, realTwoZ);
                var dic = Vector3.Distance(mapPosTwo, lastEnd);
                //LoggerHelper.Error("ShowPaths: " + mapPosOne + mapPosTwo);
                if (dic + lastMargin >= m_dotDistance)
                {
                    CreateDots(mapPosOne, mapPosTwo, ref lastMargin);
                    lastEnd = mapPosTwo;
                }
                else
                {
                    lastMargin += dic;
                }
            }
        }

        private void CreateDots(Vector3 start, Vector3 end, ref float margin)
        {
            //LoggerHelper.Error("start: " + start + " end: " + end);
            var vector = end - start;
            var dis = vector.magnitude - margin;
            var dir = vector.normalized;
            var angle = 360 - VectorAngle(Vector3.right, vector);
            //LoggerHelper.Error("angle before: " + angle);
            var angleVec = new Vector3(0, 0, angle);
            AddPathPoint(start + dir * margin, angleVec);

            var count = (int)(dis / m_dotDistance);
            var newMargin = dis + margin - m_dotDistance * count;
            if (newMargin > m_dotDistance)
                count++;
            //LoggerHelper.Error("CreateDots: " + "start: " + start + " end: " + end + " " + angle + " " + count);

            for (int i = 0; i < count; i++)
            {
                var curMargin = (m_dotDistance * (i + 1) + margin);
                if (curMargin <= dis)//超出距离的不画
                    AddPathPoint(start + dir * curMargin, angleVec);
            }
            if (newMargin > m_dotDistance)
                newMargin -= m_dotDistance;
            margin = newMargin;
        }

        private Vector3 GetMapPos(float x, float z)
        {
            return new Vector3((x - m_downX) * _xScale, (z - m_downY) * _yScale, 0);
        }

        private Transform CreatePathPointItem()
        {
            if (_pathPointItemPool.Count > 0)
            {
                var obj = _pathPointItemPool[0];
                _pathPointItemPool.RemoveAt(0);
                return obj;
            }
            else
            {
                var item = Instantiate<Transform>(m_pathPoint);
                item.SetParent(m_myTransform, false);
                return item;
            }
        }

        private void ReleasePathPointItem(Transform item)
        {
            _pathPointItemPool.Add(item);
        }

        private void AddPathPoint(Vector3 position, Vector3 dir)
        {
            var item = CreatePathPointItem();
            _pathPointItems.Add(item);
            item.localPosition = position;
            item.localEulerAngles = dir;
        }

        float VectorAngle(Vector2 from, Vector2 to)
        {
            float angle;
            Vector3 cross = Vector3.Cross(from, to);
            angle = Vector2.Angle(from, to);
            return cross.z > 0 ? -angle : angle;
        }

        public void RemoveAllPathPoint()
        {
            for (int i = 0; i < _pathPointItems.Count; i++)
            {
                var item = _pathPointItems[i];
                item.localPosition = HIDE_POSITION;
                ReleasePathPointItem(item);
            }
            _pathPointItems.Clear();
        }
    }
}