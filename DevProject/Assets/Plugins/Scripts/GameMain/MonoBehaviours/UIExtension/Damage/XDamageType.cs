﻿using GameLoader.Utils.Timer;
using System.Collections.Generic;
using System.Text;
using UIExtension;
using UnityEngine;

namespace GameMain
{
    static public class XDamageType
    {
        static public string NUMBER_HIT = "lianji";
        static public string NUMBER_ATTACK = "baojihong";
        static public int OffY = -60;
    }

    static public class XDamageAttackType
    {
        static public int HIT = 1;
        static public int CRITICAL = 2;
        static public int STRIKE = 3;
        static public int MISS = 4;
        static public int TREAT = 5;
        static public int PARRY = 6;  //格挡
        static public int DEADLY = 9;  //会心
        static public int ABSORB = 10;  //伤害吸收
        static public int INVINCIBLE = 11;  //无敌
        static public int GET_HIT = 12;  //受击
        static public int PET_HIT = 13;  //宠物攻击
        static public int TREASURE_HIT = 14;  //法宝攻击
        static public int UNYIELDING = 15;  //不屈

        static public int LEN_MAX = 16;
    }

    static public class ArtNumberType
    {
        static public string NOTSHOW = "notshow";
        static public string HIT = "direndiaoxue";
        static public string CRITICAL = "baoji";
        static public string TREAT = "huixue";
        static public string GET_HIT = "diaoxue";
        static public string PET_HIT = "chongwu";
        static public string TREASURE_HIT = "fabao";
        static public string DEADLY = "huixin";
    }

    static public class ArtWord
    {
        static public string ADD_YELLOW = "jiahaohuang";
        static public string ADD_BLUE = "jiahaolan";
        static public string TREAT = "huixue";
        static public string MISS = "shanbi";
        static public string CRITICAL = "baoji";
        static public string ABSORB = "shanghaixishou";
        static public string INVINCIBLE = "wudi";
        static public string DEADLY = "huixin";
        static public string PARRY = "gedang";
        static public string UNYIELDING = "wudi"; //用无敌的资源
    }

    //static public class PlayTime
    //{
    //    static public float SHOW = 0.1f; //渐显 时间 秒
    //    static public float MOVE = 0.1f; //移动 时间 秒
    //    static public float SHOWTime = 0.1f; // 显示 时间 秒
    //    static public float HIDE = 0.2f; //渐隐 时间 秒
    //    static public float SHOWTimeEx = 0.2f; //额外显示时间 秒
    //}

    //static public class MoveDistance
    //{
    //    static public int GET_HIT = -120; //普通攻击 向下 移动
    //    static public int GET_CRITICAL = -120; //暴击攻击 向下 移动
    //    static public int GET_TREAT = -120; //治疗 向下 移动
    //    static public int ATTACK_HIT = 180; //普通攻击 向上 移动
    //    static public int ATTACK_CRITICAL = 80; //暴击 向上 移动
    //    static public int MISS = 180; //MISS 向上 移动
    //    static public int GET_MISS = 180; //MISS 向上 移动
    //}

    //static public class ScaleType
    //{
    //    static public float Larger = 1.8f;
    //    static public float Narrow = 0.4f;
    //    static public float Original = 1.0f;
    //}

    //static public class FiveElement
    //{
    //    static public int GOLD = 1; //金
    //    static public int WOOD = 2; //木
    //    static public int WATER = 3; //水
    //    static public int FIRE = 4; //火
    //    static public int EARTH = 5; //土
    //    static public int LEN_MAX = 6;
    //}

    //static public class FiveEWord
    //{
    //    static public string GOLD = "jin";
    //    static public string WOOD = "mu";
    //    static public string WATER = "shui";
    //    static public string FIRE = "huo";
    //    static public string EARTH = "tu";
    //}

    //static public class FiveEColor
    //{
    //    static public Color ORIGINAL = new Color(1f,1f,1f,1f);
    //    static public Color GOLD = new Color(197f / 255f, 157f / 255f, 28f / 255f, 1f);
    //    static public Color WOOD = new Color(62f / 255f, 174f / 255f, 50f / 255f, 1f);
    //    static public Color WATER = new Color(32f / 255f, 118f / 255f, 219f / 255f, 1f);
    //    static public Color FIRE = new Color(202f / 255f, 39f / 255f, 0f, 1f);
    //    static public Color EARTH = new Color(176f / 255f, 96f / 255f, 24f / 255f, 1f);
    //}

    //初始偏移位置
    static public class StartOffY
    {
        static public int ATTACK_CRITICAL = 100;
    }
}
