﻿using Common.Global;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using System.Collections.Generic;
using System.Text;
using UIExtension;
using UnityEngine;

namespace GameMain
{
    public class XDamageHandler : XContainer
    {
        public static Vector3 HIDE_POSITION = new Vector3(-1000, -1400, 0);
        const int MAX_GRID_COUNT = 10;
        public static XDamageHandler instance;
        GameObject _numberGo;
        GameObject _damageGo;
        GameObject _wordGo;
        GameObject _noUseGo;
        Transform _underTransform;
        Transform _topTransform;

        int _damageGridNumber = 0;
        int _currentCount = 0;

        Dictionary<string, Sprite> _itemDict = new Dictionary<string, Sprite>();
        string[] _hitTypeArtNumberTypeMap = new string[XDamageAttackType.LEN_MAX];
        string[] _hitTypeArtWordTypeMap = new string[XDamageAttackType.LEN_MAX];

        Stack<XDamageGrid> _damageGridStack = new Stack<XDamageGrid>();
        Stack<XDamageGrid> _damageGridPlayerStack = new Stack<XDamageGrid>();
        XDamageGrid[] _showGridList = new XDamageGrid[MAX_GRID_COUNT];

        protected override void Awake()
        {
            instance = this;
            _numberGo = this.transform.FindChild("Container_number").gameObject;
            _numberGo.transform.localScale = Vector3.zero;
            _wordGo = this.transform.FindChild("Container_word").gameObject;
            _wordGo.transform.localScale = Vector3.zero;
            _damageGo = this.transform.FindChild("Container_damage").gameObject;
            _noUseGo = this.transform.FindChild("Container_noUse").gameObject;
            _underTransform = this.transform.FindChild("Container_layer1");
            _topTransform = this.transform.FindChild("Container_layer2");
            this._numberGo.transform.localPosition = HIDE_POSITION;
            this._wordGo.transform.localPosition = HIDE_POSITION;
            this._damageGo.transform.localPosition = HIDE_POSITION;
            this.InitItemDict();
            this.InitHitType2ArtNumberType();
            this.InitHitType2ArtWordType();
            PreCreateDamageItem();
            if (_noUseGo.GetComponent<Canvas>() == null)
            {
                _noUseGo.AddComponent<Canvas>();
            }
        }

        protected override void OnEnable()
        {
            
        }

        protected override void OnDisable()
        {
            
        }

        void Update()
        {

        }

        void InitItemDict()
        {
            List<ImageWrapper> imageList = new List<ImageWrapper>();
            this._numberGo.GetComponentsInChildren<ImageWrapper>(imageList);
            for (int i = 0; i < imageList.Count; i++)
            {
                var image = imageList[i];
                var name = image.gameObject.name;
                this._itemDict[name.Substring(7, name.Length - 7)] = image.sprite;
            }
            imageList.Clear();
            this._wordGo.GetComponentsInChildren<ImageWrapper>(imageList);
            for (int i = 0; i < imageList.Count; i++)
            {
                var image = imageList[i];
                var name = image.gameObject.name;
                this._itemDict[name.Substring(8, name.Length - 8)] = image.sprite;
            }
            imageList.Clear();
        }

        void InitHitType2ArtNumberType()
        {
            this._hitTypeArtNumberTypeMap[XDamageAttackType.HIT] = ArtNumberType.HIT;
            this._hitTypeArtNumberTypeMap[XDamageAttackType.CRITICAL] = ArtNumberType.CRITICAL;
            this._hitTypeArtNumberTypeMap[XDamageAttackType.GET_HIT] = ArtNumberType.GET_HIT;
            this._hitTypeArtNumberTypeMap[XDamageAttackType.TREAT] = ArtNumberType.TREAT;
            this._hitTypeArtNumberTypeMap[XDamageAttackType.PET_HIT] = ArtNumberType.PET_HIT;
            this._hitTypeArtNumberTypeMap[XDamageAttackType.TREASURE_HIT] = ArtNumberType.TREASURE_HIT;
            this._hitTypeArtNumberTypeMap[XDamageAttackType.DEADLY] = ArtNumberType.DEADLY;
            this._hitTypeArtNumberTypeMap[XDamageAttackType.TREAT] = ArtNumberType.TREAT;
        }


        void InitHitType2ArtWordType()
        {
            this._hitTypeArtWordTypeMap[XDamageAttackType.MISS] = ArtWord.MISS;
            this._hitTypeArtWordTypeMap[XDamageAttackType.CRITICAL] = ArtWord.CRITICAL;
            this._hitTypeArtWordTypeMap[XDamageAttackType.DEADLY] = ArtWord.DEADLY;
            this._hitTypeArtWordTypeMap[XDamageAttackType.ABSORB] = ArtWord.ABSORB;
            this._hitTypeArtWordTypeMap[XDamageAttackType.INVINCIBLE] = ArtWord.INVINCIBLE;
            this._hitTypeArtWordTypeMap[XDamageAttackType.TREAT] = ArtWord.TREAT;
            this._hitTypeArtWordTypeMap[XDamageAttackType.PARRY] = ArtWord.PARRY;
            this._hitTypeArtWordTypeMap[XDamageAttackType.UNYIELDING] = ArtWord.UNYIELDING;
        }


        string HitType2ArtNumberType(int type, bool isPlayer)
        {
            if (isPlayer && type == XDamageAttackType.HIT)
            {
                return ArtNumberType.GET_HIT;
            }
            if (!string.IsNullOrEmpty(this._hitTypeArtNumberTypeMap[type]))
            {
                return this._hitTypeArtNumberTypeMap[type];
            }

            return ArtNumberType.NOTSHOW;
        }

        string HitType2ArtWordType(int type, bool isPlayer)
        {
            if (!string.IsNullOrEmpty(this._hitTypeArtWordTypeMap[type]))
            {
                return this._hitTypeArtWordTypeMap[type];
            }
            return "";
        }

        int index = 0;
        List<float> posY = new List<float> { 0, 0.5f, 0.5f, 1, 1 };
        List<float> posX = new List<float> { 0, 0.4f, -0.4f, 0.8f, -0.8f };

        public void CreateDamageNumber(Vector3 position, int damage, int type, bool isPlayer)
        {
            if (Camera.main == null)
            {
                return;
            }
            if (damage == 0 && (type == XDamageAttackType.HIT || type == XDamageAttackType.CRITICAL || type == XDamageAttackType.GET_HIT))
            {
                return;
            }
            if (this._currentCount > MAX_GRID_COUNT) return;
            var screenPosition = Camera.main.WorldToScreenPoint(position);
            if (this.CheckPositionIsInScreen(screenPosition))
            {
                var damageGrid = this.GetDamageGrid(isPlayer);
                var colorType = this.HitType2ArtNumberType(type, isPlayer);
                var word = this.HitType2ArtWordType(type, isPlayer);
                if (damage < 0)
                {
                    damage = -damage;
                }
                if (word != "") 
                {
                    this.CreateDamageWord(damageGrid, word, isPlayer);
                }
                if (colorType != ArtNumberType.NOTSHOW)
                {
                    damageGrid = this.CreateNumber(damageGrid, colorType, damage, isPlayer);
                }
                if (isPlayer)
                {
                    index = index % 5;
                    position.x = position.x + posX[index];
                    position.y = position.y + posY[index];
                }
                damageGrid.Show(position,type);
                this._currentCount = this._currentCount + 1;
            }
        }

        // 改变 显示的grid
        void ChangeGridListPosition(bool isPlayer)
        {
            for (int i = 0; i < _showGridList.Length; i++)
            {
                var item = _showGridList[i];
                if (item == null) continue;
                if (item.GetIsPlayer() == true)
                {
                    this.ChangeGridPosition(item);
                }
            }
        }

        void ChangeGridPosition(XDamageGrid item)
        {
            var trans = item.GetTransform();
            var curPos = trans.localPosition;
            var isPlayer = item.GetIsPlayer();
            var offY = XDamageType.OffY;
            trans.localPosition = curPos + new Vector3(0, offY, 0);
        }

        // 保存显示中的grid
        void SaveShowGridList(XDamageGrid damageGrid)
        {
            int idx = -1;
            for (int i = 0; i < _showGridList.Length; i++)
            {
                var item = _showGridList[i];
                if (item == null)
                {
                    idx = i;
                }
            }
            if (idx >= 0)
            {
                //this._showIndex = this._showIndex + 1;
                damageGrid.SetGridIndex(idx);
                this._showGridList[idx] = damageGrid;
            }
        }

        bool CheckPositionIsInScreen(Vector3 position)
        {
            var x = position.x;
            var y = position.y;
            return x >= 0 && y >= 0 && x <= ScreenUtil.ScreenWidth && y <= ScreenUtil.ScreenHeight;
        }

        XDamageGrid GetDamageGrid(bool isPlayer)
        {
            XDamageGrid damageGrid = null;
            if (isPlayer)
            {
                if (this._damageGridPlayerStack.Count > 0)
                {
                    damageGrid = this._damageGridPlayerStack.Pop();
                }
            }
            else { 
                if (this._damageGridStack.Count > 0)
                {
                    damageGrid = this._damageGridStack.Pop();
                }
            }
            if (damageGrid == null)
            {
                damageGrid = this.AddDamageGrid();
                damageGrid.SetIsPlayer(isPlayer);
                this.SetGridParent(damageGrid, isPlayer);
            }
            return damageGrid;
        }

        void SetGridParent(XDamageGrid damageGrid, bool isPlayer)
        {
            var trans = damageGrid.GetTransform();
            if (isPlayer) 
            {
                trans.SetParent(this._topTransform, false);
            }
            else
            {
                trans.SetParent(this._underTransform, false);
            }
            trans.localPosition = Vector3.zero;
            damageGrid.SetIsPlayer(isPlayer);
        }

        XDamageGrid AddDamageGrid()
        {
            var go = GameObject.Instantiate(this._damageGo);
            this._damageGridNumber = this._damageGridNumber + 1;
            go.name = "DamageGrid" + this._damageGridNumber;
            var damageGrid = go.AddComponent<XDamageGrid>();
            var trans = go.GetComponent<RectTransform>();
            if (trans == null){
                trans = go.AddComponent<RectTransform>();
            }
            damageGrid.SetTransform(trans);
            return damageGrid;
        }

        public void RemoveDamageGrid(XDamageGrid damageGrid)
        {
            damageGrid.Hide();
            if (damageGrid.GetIsPlayer())
            {
                this._damageGridPlayerStack.Push(damageGrid);
            }
            else {
                this._damageGridStack.Push(damageGrid);
            }
            this.CancelShowGridList(damageGrid);
            this._currentCount = this._currentCount - 1;
        }

        //移除显示中的grid
        public void CancelShowGridList(XDamageGrid damageGrid)
        {
            var index = damageGrid.GetGridInde();
            this._showGridList[index] = null;
        }

        public void RemoveItemFormList(List<XDamageItem> damageItemList)
        {
            for (int i = 0; i < damageItemList.Count; i++)
            {
                var item = damageItemList[i];
                var itemName = item.Name;
                item.Hide();
                _damageItemPool.Enqueue(item);
            }
        }

        XDamageGrid CreateNumber(XDamageGrid damageGrid, string colorType, int value, bool isPlayer)
        {
            var str = value.ToString();
            for (int i =0;i<str.Length;i++)
            {
                var name = colorType + str.Substring(i, 1);
                var damageItem = this.GetDamageItem(name, isPlayer);
                damageGrid.AddNumberItem(damageItem);
            }
            return damageGrid;
        }

        XDamageGrid CreateDamageWord(XDamageGrid damageGrid, string word, bool isPlayer)
        {
            var damageItem = this.GetDamageItem(word, isPlayer);
            damageGrid.AddNumberItem(damageItem);
            return damageGrid;
        }

        void PreCreateDamageItem()
        {
            for (int i = 0; i < 5; i++)
            {
                bool isPlayer = true;
                var damageGrid = this.AddDamageGrid();
                damageGrid.SetIsPlayer(isPlayer);
                SetGridParent(damageGrid, isPlayer);
                _damageGridPlayerStack.Push(damageGrid);
                isPlayer = false;
                damageGrid = this.AddDamageGrid();
                damageGrid.SetIsPlayer(isPlayer);
                SetGridParent(damageGrid, isPlayer);
                _damageGridStack.Push(damageGrid);
            }
            XDamageItem damageItem = null;
            for (int i = 0; i < 10; i++)
            {
                var go = GameObject.Instantiate(_numberGo.transform.GetChild(0).gameObject);
                go.name = "DamageItem";
                damageItem = go.AddComponent<XDamageItem>();
                damageItem.GetTransform().SetParent(this._noUseGo.transform);
                _damageItemPool.Enqueue(damageItem);
            }
        }

        Queue<XDamageItem> _damageItemPool = new Queue<XDamageItem>();
        XDamageItem GetDamageItem(string nameKey, bool isTop)
        {
            XDamageItem damageItem = null;
            if (_damageItemPool.Count == 0)
            {
                var go = GameObject.Instantiate(_numberGo.transform.GetChild(0).gameObject);
                go.name = "DamageItem";
                damageItem = go.AddComponent<XDamageItem>();
                damageItem.GetTransform().SetParent(this._noUseGo.transform);
            }
            else {
                damageItem = _damageItemPool.Dequeue();
            }
            ResetDamageItem(damageItem, nameKey, isTop);
            return damageItem;
        }

        void ResetDamageItem(XDamageItem damageItem, string name, bool isTop)
        {
            if (!this._itemDict.ContainsKey(name))
            {
                if (Application.isEditor) LoggerHelper.Error(name + " is not exist in panel");
                return;
            }
            if (damageItem.Name != name)
            {
                var sprite = this._itemDict[name];
                damageItem.SetSprite(sprite);
                damageItem.Name = name;
            }
        }
    }
}
