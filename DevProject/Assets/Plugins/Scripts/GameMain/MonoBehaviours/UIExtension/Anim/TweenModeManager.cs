﻿using System;
using UnityEngine;
using ACTSystem;

namespace GameMain
{
    public enum TweenMode
    {
        //匀速变化
        Linear = 1,
        //先慢后快
        InQuad = 2,
        //先快后慢（Quadratic二次方缓动，EaseOut公式）
        OutQuad = 3
    }

    public static class TweenModeManager
    {
        //获取当前进度 (timestamp,动画执行到当前帧所进过的时间; begin,开始位置; change,需要变化的量; duration,动画的总时间)
        public static float GetProgress(TweenMode tweenMode, float timestamp, float begin, float change, float duration)
        {
            switch (tweenMode)
            {
                case TweenMode.Linear:
                    return (timestamp / duration) * change + begin;
                case TweenMode.OutQuad:
                    return -(timestamp /= duration) * (timestamp - 2) * change + begin;
                case TweenMode.InQuad:
                    return (timestamp /= duration) * timestamp * change + begin;
                default:
                    return (timestamp / duration) * change + begin;
            }
        }

        //Vector3版本
        public static Vector3 GetProgressVector3(TweenMode tweenMode, float timestamp, Vector3 begin, Vector3 change, float duration)
        {
            switch (tweenMode)
            {
                case TweenMode.Linear:
                    return (timestamp / duration) * change + begin;
                case TweenMode.OutQuad:
                    return -(timestamp /= duration) * (timestamp - 2) * change + begin;
                case TweenMode.InQuad:
                    return (timestamp /= duration) * timestamp * change + begin;
                default:
                    return (timestamp / duration) * change + begin;
            }
        }

    }
}
