﻿using UnityEngine;
using LuaInterface;

namespace GameMain
{
    public class XGameObjectTweenPosition : MonoBehaviour
    {

        Transform _trans = null;
        Vector3 _fromPos;              //起始值
        Vector3 _curPos;               //当前值
        Vector3 _toPos;                //目标值
        Vector3 _change;               //变化值
        float _duration = 0f;
        float _timestamp = 0f;
        WrapMode _wrapMode;
        TweenMode _tweenMode = TweenMode.Linear;
        LuaFunction _luafunc;

        private bool _isTweening = false;

        public bool IsTweening
        {
            get { return _isTweening; }
            set 
            {
                _isTweening = value; 
                if (!_isTweening)
                {
                    LuaDriver.instance.CallFunction("CSCALL_ON_GUIDE_TWEEN_POSITION_END", gameObject.name);
                } 
            }
        }

        void Awake()
        {
            _trans = transform;
        }

        //传世界坐标
        public void OnceOutQuadWordPos(Vector3 toPosWorld, float duration, LuaFunction luafunc)
        {
            _tweenMode = TweenMode.OutQuad;
            Vector3 toPosLocal = _trans.parent.transform.InverseTransformPoint(toPosWorld);
            SetFromToPos(_trans.localPosition, toPosLocal, duration, (int)WrapMode.Once, luafunc);
        }

        //传local坐标
        public void OnceOutQuad(Vector3 toPos, float duration, LuaFunction luafunc)
        {
            _tweenMode = TweenMode.OutQuad;
            SetFromToPos(_trans.localPosition, toPos, duration, (int)WrapMode.Once, luafunc);
        }

        public void SetToPosTween(Vector3 toPos, float duration, int wrapMode, int tweenMode, LuaFunction luafunc)
        {
            _tweenMode = (TweenMode)tweenMode;
            SetFromToPos(_trans.localPosition, toPos, duration, wrapMode, luafunc);
        }

        public void SetFromToPosTween(Vector3 formPos, Vector3 toPos, float duration, int wrapMode, int tweenMode, LuaFunction luafunc)
        {
            _tweenMode = (TweenMode)tweenMode;
            SetFromToPos(formPos, toPos, duration, wrapMode, luafunc);
        }

        public void SetFromToPosOnce(Vector3 formPos, Vector3 toPos, float duration, LuaFunction luafunc)
        {
            SetFromToPos(formPos, toPos, duration, (int)WrapMode.Once, luafunc);
        }

        public void SetToPosOnce(Vector3 toPos, float duration, LuaFunction luafunc)
        {
            SetFromToPos(_trans.localPosition, toPos, duration, (int)WrapMode.Once, luafunc);
        }

        public void SetToPos(Vector3 toPos, float duration, int wrapMode, LuaFunction luafunc)
        {
            SetFromToPos(_trans.localPosition, toPos, duration, wrapMode, luafunc);
        }

        public void SetFromToPos(Vector3 formPos, Vector3 toPos, float duration, int wrapMode, LuaFunction luafunc =null)
        {
            _curPos = formPos;
            _fromPos = formPos;
            _toPos = toPos;
            _change = toPos - formPos;
            _duration = duration;
            _timestamp = 0;
            _wrapMode = (WrapMode)wrapMode;
            SetPosition();
            _luafunc = luafunc;
            _isTweening = true;
        }

        void Update()
        {
            if (!_isTweening)
            {
                _timestamp = 0;
                return;
            } 

            _timestamp += UnityPropUtils.deltaTime;
            _curPos = TweenModeManager.GetProgressVector3(_tweenMode, _timestamp, _fromPos, _change, _duration);

            if (_timestamp > _duration)
            {
                _timestamp = 0;
                if (_wrapMode == WrapMode.Once)
                {
                    _isTweening = false;
                    _curPos = _toPos;
                }
                else if (_wrapMode == WrapMode.PingPong)
                {
                    _isTweening = false;
                    _curPos = _toPos;
                    SetFromToPos(_toPos, _fromPos, _duration, (int)(_wrapMode));
                }
                else if (_wrapMode == WrapMode.Loop)
                {
                    _curPos = _fromPos;
                }
                if (_luafunc != null)
                {
                    SetPosition();
                    LuaFacade.CallLuaFunction(_luafunc);
                    return;
                }
            }         
            SetPosition();
        }

        private void SetPosition()
        {
            _trans.localPosition = _curPos;
        }

    }
}