﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace GameMain
{
    public class XImageFollow : MonoBehaviour
    {
        Image _currentImage = null;
        float _curAmount = 1f;
        float _followAmount = 1f;
        bool _isFollowing = false;
        float _followDirection = 0;
        float _damping = 0.5f;
        float _ref = 0f;

        float deltaAlpha = 0.1f;
        bool isAlphaChange = false;
        float _length = 0;
        Vector3 startPos = Vector3.zero;
        Vector3 curPos = Vector3.zero;
        Color curColor = new Color();
        Color startColor = new Color();
        void Awake()
        {
            _currentImage = this.GetComponent<Image>();
            if (_currentImage == null)
            {
                Debug.LogError("XImageFollow can not find a Image in " + this.gameObject.name);
                this.enabled = false;
                return;
            }
            curPos = _currentImage.rectTransform.localPosition;
            startPos = _currentImage.rectTransform.localPosition;
            startColor = _currentImage.color;
            curColor = startColor;
        }

        public void Init(float length)
        {
            _length = length;
            startPos.x = startPos.x - _length;
            curColor.a = 0;
            _currentImage.color = curColor;
        }

        public void SetValue(float value)
        {
            _currentImage.color = startColor;
            _followAmount = _curAmount;
            _curAmount = value;
            if (_curAmount != _followAmount)
            {
                _isFollowing = true;
                _followDirection = Mathf.Sign(_curAmount - _followAmount);
            }
        }

        void Update()
        {
            if (isAlphaChange)
            {
                curColor.a -= deltaAlpha;
                _currentImage.color = curColor;
                if (curColor.a < 0)
                {
                    isAlphaChange = false;
                }
            }
            if (!_isFollowing) return;
            _followAmount = Mathf.SmoothDamp(_followAmount, _curAmount, ref _ref, _damping);
            if (ACTSystem.ACTMathUtils.Abs(_followAmount - _curAmount) < 0.001f
                || Mathf.Sign(_curAmount - _followAmount) != _followDirection)
            {
                _isFollowing = false;
                curPos.x = startPos.x + _curAmount * _length;
                _currentImage.rectTransform.localPosition = curPos;
                _followAmount = _curAmount;
                isAlphaChange = true;
                curColor = startColor;
            }
            else
            {
                curPos.x = startPos.x + _followAmount * _length;
                _currentImage.rectTransform.localPosition = curPos;
            }
        }
    }
}

