﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using GameLoader.Utils;

namespace GameMain
{
    public class XImageMultipleDelayFilling : MonoBehaviour
    {
        Image showImage;

        float _amount = 1f;
        float _fillingAmount = 1f;

        public float FillingAmount
        {
            get { return _fillingAmount; }
        }
        bool _isFilling = false;

        public bool IsFilling
        {
            get { return _isFilling; }
        }

        float _fillingDirection = 0;
        float _damping = 0.5f;

        public float Damping
        {
            set {_damping = value; }
        }

        public bool isRight
        {
            get
            {
                return showImage.fillOrigin == 1; 
            }
        }

        void Awake()
        {
            showImage = transform.Find("Image_Delay").GetComponent<Image>();
            if (showImage == null)
            {
                LoggerHelper.Error("XImageMultipleDelayFilling can not find a Image in " + transform.Find("Image_Delay").name);
                this.enabled = false;
                return;
            }

            _amount = showImage.fillAmount;
            _fillingAmount = showImage.fillAmount;
        }

        private bool _isResetToZero = false;
        public void SetIsResetToZero(bool value)
        {
            _isResetToZero = value;
        }

        //直接设定_fillingAmount，用于不进行动画播放的时候同步设定进度百分比
        public void SetValueDirectly(float value)
        {
            _fillingAmount = value;
            _amount = value;
            _isFilling = false;
        }

        private float _changeValue = 0;
        public void SetValue(float value)
        {
            _fillingAmount = _amount;
            _amount = value;
            _changeValue = _amount - _fillingAmount;
            if (_amount != _fillingAmount)
            {
                _isFilling = true;
                _fillingDirection = Mathf.Sign(_amount - _fillingAmount);
            }
        }

        void Update()
        {
            if (!_isFilling) return;
            _fillingAmount += (_changeValue / _damping) * UnityPropUtils.deltaTime;
            if (ACTSystem.ACTMathUtils.Abs(_fillingAmount - _amount) < 0.001f
                || Mathf.Sign(_amount - _fillingAmount) != _fillingDirection)
            {
                _isFilling = false;
                _fillingAmount = _amount;
                float residualValue = _amount % 1;
                if ((_amount >= 1 && !_isResetToZero && residualValue == 0))
                {
                    GetShowImage(_fillingAmount).fillAmount = 1;
                }
                else
                {
                    GetShowImage(_fillingAmount).fillAmount = residualValue;
                }
            }
            else
            {
                GetShowImage(_fillingAmount).fillAmount = _fillingAmount % 1;
            }
        }

        Image GetShowImage(float value)
        {
            return showImage;
        }
    }
}


