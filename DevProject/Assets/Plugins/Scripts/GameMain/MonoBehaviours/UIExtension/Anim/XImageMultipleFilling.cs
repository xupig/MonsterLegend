﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using GameLoader.Utils;

namespace GameMain
{
    public class XImageMultipleFilling : MonoBehaviour
    {
        List<Image> _imageList = new List<Image>();

        float _amount = 1f;
        float _fillingAmount = 1f;

        public float FillingAmount
        {
            get { return _fillingAmount; }
        }
        bool _isFilling = false;

        public bool IsFilling
        {
            get { return _isFilling; }
        }

        float _fillingDirection = 0;
        float _damping = 0.5f;

        public float Damping
        {
            set {_damping = value; }
        }

        public bool isRight
        {
            get
            {
                return _imageList[0].fillOrigin == 1; 
            }
        }

        private int lastShow = -1;
        private int lastBgShow = -1;
        private Dictionary<int, Sprite> _spriteDict;

        Image bgImage;
        Image showImage;

        Transform bgTransform;
        Transform showTransform;
        XImageChanger showChanger;
        XImageChanger bgChanger;

        void Awake()
        {
            showTransform = this.transform.Find("Image_Show");
            bgTransform = this.transform.Find("Image_Bg");

            showChanger = showTransform.gameObject.AddComponent<XImageChanger>();
            bgChanger = bgTransform.gameObject.AddComponent<XImageChanger>();

            showImage = showTransform.GetComponent<Image>();
            bgImage = bgTransform.GetComponent<Image>();

            _amount = showImage.fillAmount;
            _fillingAmount = showImage.fillAmount;
        }

        private bool _isResetToZero = false;
        public void SetIsResetToZero(bool value)
        {
            _isResetToZero = value;
        }

        //直接设定_fillingAmount，用于不进行动画播放的时候同步设定进度百分比
        public void SetValueDirectly(float value)
        {
            _fillingAmount = value;
            _amount = value;
            _isFilling = false;
        }

        private float _changeValue = 0;
        public void SetValue(float value)
        {
            _fillingAmount = _amount;
            _amount = value;
            _changeValue = _amount - _fillingAmount;
            if (_amount != _fillingAmount)
            {
                _isFilling = true;
                _fillingDirection = Mathf.Sign(_amount - _fillingAmount);
            }
        }

        void Update()
        {
            if (!_isFilling) return;
            _fillingAmount += (_changeValue / _damping) * UnityPropUtils.deltaTime;
            if (ACTSystem.ACTMathUtils.Abs(_fillingAmount - _amount) < 0.001f
                || Mathf.Sign(_amount - _fillingAmount) != _fillingDirection)
            {
                _fillingAmount = _amount;
                _isFilling = false;
                float residualValue = _amount % 1;
                if ((_amount >= 1 && !_isResetToZero && residualValue == 0))
                {
                    GetShowImageNew(_fillingAmount).fillAmount = 1;
                }
                else
                {
                    GetShowImageNew(_fillingAmount).fillAmount = residualValue;
                }
            }
            else
            {
                GetShowImageNew(_fillingAmount).fillAmount = _fillingAmount % 1;
            }
        }

        public void SetSpriteDict(Dictionary<int, Sprite> spriteDict)
        {
            _spriteDict = spriteDict;
        }

        Dictionary<int, string> _spriteNameDict;
        public void SetSpriteNameDict(Dictionary<int, string> spriteNameDict)
        {
            _spriteNameDict = spriteNameDict;
        }

        Image GetShowImageNew(float value)
        {
            int index = 0;
            index = (int)Math.Max(Mathf.Floor(value), 0);
            index = index % _spriteNameDict.Count;
            int bgIndex = (index + 4) % 5;

            if (value > 1)
            {
                if (lastBgShow != bgIndex)
                {
                    bgChanger.ChangeImage(_spriteNameDict[bgIndex]);
                    bgImage.fillAmount = 1;
                }
                lastBgShow = bgIndex;
            }
            else
            {
                bgImage.fillAmount = 0;
                lastBgShow = -1;
            }

            if (lastShow != index)
            {
                showChanger.ChangeImage(_spriteNameDict[index]);
            }
            lastShow = index;

            return showImage;
        }
    }
}

