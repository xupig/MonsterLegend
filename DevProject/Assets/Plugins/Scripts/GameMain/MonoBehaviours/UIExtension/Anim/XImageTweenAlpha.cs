﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace GameMain
{
    public class XImageTweenAlpha : MonoBehaviour
    {

        Image _currentImage = null;
        Color _color;
        float _fromAlpha = 1f;              //起始值
        float _curAlpha = 1f;              //当前值
        float _toAlpha = 1f;                //目标值    
        float _duration = 0f;
        float _add = 0f;
        float _tweenDirection;
        WrapMode _wrapMode;

        private bool _isTweening = false;

        public bool IsTweening
        {
            get { return _isTweening; }
            set { _isTweening = value; }
        }

        void Awake()
        {
            _currentImage = this.GetComponent<Image>();
            if (_currentImage)
            {
                _color = _currentImage.color;
            }            
        }

        public void ResetAlpha(float toAlpha)
        {
            _curAlpha = toAlpha;
            SetColor(_curAlpha);
        }

        public void SetFromToAlphaOnce(float formAlpha, float toAlpha, float duration)
        {
            SetFromToAlpha(formAlpha, toAlpha, duration, (int)WrapMode.Once);
        }

        public void SetToAlphaOnce(float toAlpha, float duration)
        {
            SetFromToAlpha(_currentImage.color.a, toAlpha, duration, (int)WrapMode.Once);
        }

        public void SetToAlpha(float toAlpha, float duration, int wrapMode)
        {
            SetFromToAlpha(_currentImage.color.a, toAlpha, duration, wrapMode);
        }

        public void SetFromToAlpha(float formAlpha, float toAlpha, float duration, int wrapMode)
        {
            _curAlpha = formAlpha;
            _fromAlpha = formAlpha;
            _toAlpha = toAlpha;
            _duration = duration;
            _wrapMode = (WrapMode)wrapMode;

            _add = (_toAlpha - _fromAlpha) / _duration;
            _tweenDirection = Mathf.Sign(_toAlpha - _curAlpha);
            SetColor(_curAlpha);
            _isTweening = true;
        }

        void Update()
        {
            if (!_isTweening) return;

            _curAlpha += _add * UnityPropUtils.deltaTime;
            if (Mathf.Sign(_toAlpha - _curAlpha) != _tweenDirection)
            {
                //GameLoader.Utils.LoggerHelper.Error("_toAlpha: " + _toAlpha + " _curAlpha: " + _curAlpha + " sign: " + Mathf.Sign(_toAlpha - _curAlpha) + " _tweenDirection: " + _tweenDirection);
                if (_wrapMode == WrapMode.Once)
                {
                    _isTweening = false;
                    _curAlpha = _toAlpha;
                }
                else if (_wrapMode == WrapMode.PingPong)
                {
                    _curAlpha = _toAlpha;
                    _toAlpha = _fromAlpha;
                    _fromAlpha = _curAlpha;
                    _tweenDirection = -_tweenDirection;
                    _add = -_add;
                }
                else if (_wrapMode == WrapMode.Loop)
                {
                    _curAlpha = _fromAlpha;
                }
            }
            //else
            //{
            //    if (Mathf.Sign(_toAlpha - _curAlpha) != _tweenDirection)
            //    {
            //        _curAlpha = _toAlpha;
            //    }
            //}            
            SetColor(_curAlpha);
        }

        public void SetColor(float nowAlpha)
        {
            _color = new Color(_color.r, _color.g, _color.b, nowAlpha);
            _currentImage.color = _color;
        }

    }
}