﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace GameMain
{
    public class XImageTweenColor : MonoBehaviour
    {

        Image _currentImage = null;
        //Color _color;
        Color _fromColor;              //起始值
        Color _curColor;              //当前值
        Color _toColor;                //目标值    
        float _duration = 0f;
        Color _add;
        float _tweenDirection;
        WrapMode _wrapMode;

        private bool _isTweening = false;

        public bool IsTweening
        {
            get { return _isTweening; }
        }

        void Awake()
        {
            _currentImage = this.GetComponent<Image>();
            _curColor = _currentImage.color;
        }

        public void ResetColor(float toR, float toG, float toB, float toA)
        {
            _curColor = new Color(toR, toG, toB, toA);
            SetColor();
        }

        public void SetFromToColorOnce(float fromR, float fromG, float fromB, float fromA, float toR, float toG, float toB, float toA, float duration)
        {
            SetFromToColor(fromR, fromG, fromB, fromA, toR, toG, toB, toA, duration, (int)WrapMode.Once);
        }

        public void SetToColorOnce(float toR, float toG, float toB, float toA, float duration)
        {
            var color = _currentImage.color;
            SetFromToColor(color.r, color.g, color.b, color.a, toR, toG, toB, toA, duration, (int)WrapMode.Once);
        }

        public void SetToColor(float toR, float toG, float toB, float toA, float duration, int wrapMode)
        {
            var color = _currentImage.color;
            SetFromToColor(color.r, color.g, color.b, color.a, toR, toG, toB, toA, duration, wrapMode);
        }

        public void SetFromToColor(float fromR, float fromG, float fromB, float fromA, float toR, float toG, float toB, float toA, float duration, int wrapMode)
        {
            _fromColor = new Color(fromR, fromG, fromB, fromA);
            _toColor = new Color(toR, toG, toB, toA);
            _curColor = _fromColor;
            _duration = duration;
            _wrapMode = (WrapMode)wrapMode;

            _add = (_toColor - _fromColor) / _duration;
            _tweenDirection = Mathf.Sign(_toColor.r - _curColor.r);
            SetColor();
            _isTweening = true;
        }

        void Update()
        {
            if (!_isTweening) return;

            _curColor += _add * UnityPropUtils.deltaTime;
            if (Mathf.Sign(_toColor.r - _curColor.r) != _tweenDirection)
            {
                //GameLoader.Utils.LoggerHelper.Error("_toAlpha: " + _toAlpha + " _curAlpha: " + _curAlpha + " sign: " + Mathf.Sign(_toAlpha - _curAlpha) + " _tweenDirection: " + _tweenDirection);
                if (_wrapMode == WrapMode.Once)
                {
                    _isTweening = false;
                    _curColor = _toColor;
                }
                else if (_wrapMode == WrapMode.PingPong)
                {
                    _curColor = _toColor;
                    _toColor = _fromColor;
                    _fromColor = _curColor;
                    _tweenDirection = -_tweenDirection;
                    _add = new Color(-_add.r, -_add.g, -_add.b, -_add.a);
                }
                else if (_wrapMode == WrapMode.Loop)
                {
                    _curColor = _fromColor;
                }
            }
            //else
            //{
            //    if (Mathf.Sign(_toAlpha - _curAlpha) != _tweenDirection)
            //    {
            //        _curAlpha = _toAlpha;
            //    }
            //}            
            SetColor();
        }

        private void SetColor()
        {
            _currentImage.color = _curColor;
        }

    }
}