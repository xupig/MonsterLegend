﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UIExtension;
using LuaInterface;
using GameLoader.Utils;

namespace GameMain
{
    public class XImageFloat : MonoBehaviour
    {
        Vector3 HIDE_POSITION = new Vector3(-1000, -1400, 0);
        static float[] positionSet = new float[50] {0, 0.25f, 0.5f, 0.75f, 1, 1, 0.75f, 0.5f, 0.25f, 0, 0, 0.25f, 0.5f, 0.75f, 0.75f, 0.5f, 0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        static float[] positionCritSet = new float[50] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.25f, 0.5f, 0.75f, 0.75f, 0.5f, 0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        static float[] ScaleSet = new float[10] { 1, 3, 5, 4, 3, 2, 1, 1, 1, 1 };
        static float[] FlashSet = new float[5] { 0, 0.8f, 0.5f, 0.2f, 0};

        static float[] positionSetOne = new float[25] { 0, 0, 0, 0, 0, 0.2f, 0.4f, 0.6f, 0.8f, 0.9f, 1f, 1.1f, 1.11f, 1.12f, 1.13f, 1.14f, 1.15f, 1.15f, 1.15f, 1.15f, 1.15f, 1.1f, 1.1f, 1.1f, 1.1f };
        static float[] ScaleSetOne = new float[25] { 3, 1, 0.9f, 1.5f, 1, 1, 1, 1, 1, 1, 1, 0.9f, 0.9f, 0.8f, 0.7f, 0.6f, 0.5f, 0.4f, 0.3f, 0.1f, 0, 0, 0, 0, 0 };
        static float[] AlphaSetOne = new float[25] { 0.5f, 0.7f, 0.8f, 1, 1, 1, 1, 1, 1, 1, 0.9f, 0.8f, 0.7f, 0.7f, 0.6f, 0.5f, 0.4f, 0.3f, 0.2f, 0.1f, 0, 0, 0, 0, 0 };

        static float[] positionSetTwo = new float[25] { 0, 0, 0, 0, 0, 0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.75f, 1f, 1.11f, 1.12f, 1.13f, 1.14f, 1.15f, 1.15f, 1.16f, 1.16f, 1.16f, 1.16f, 1.16f };
        static float[] ScaleSetTwo = new float[25] { 0.5f, 1, 1.5f, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
        static float[] AlphaSetTwo = new float[25] { 0.5f, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0.9f, 0.9f, 0.9f, 0.7f, 0.6f, 0.5f, 0.4f, 0.3f, 0.2f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f };

        static int[] typeToMode = new int[20] { 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
        int curFrameNum = 0;
        bool isFloat = false;
        int maxFrameNumber = 0;
        Vector3 startPos = Vector3.zero;
        ImageWrapper image;
        Action callBack;
        RectTransform rect;
        Vector3 delta = new Vector3(0, 5, 0);
        Color curColor = new Color();
        int curType = 0;
        float height = 10;
        float amendmentHeight = 50f;
        Vector3 position
        {
            get { return rect.localPosition; }
            set { rect.localPosition = value; }
        }
        Vector3 scale
        {
            get { return rect.localScale; }
            set { rect.localScale = value; }
        }
        float alpha
        {
            get { return image.color.a; }
            set 
            {
                curColor.a = value;
                image.color = curColor;
            }
        }
        void Awake()
        {
            image = gameObject.GetComponent<ImageWrapper>();
            height = image.preferredHeight;
            if (image == null)
            {
                LoggerHelper.Error("image is null in XImageFloat awake in " + this.gameObject.name);
                this.enabled = false;
                return;
            }
            rect = gameObject.transform as RectTransform;
            curColor = image.color;
        }

        private float lastTime = 0;
        void Update()
        {
            if(!isFloat)
            {
                return;
            }
            if (UnityPropUtils.realtimeSinceStartup - lastTime > 0.033f)
            {
                curFrameNum++;
                lastTime = UnityPropUtils.realtimeSinceStartup;
            }
            if (curFrameNum > maxFrameNumber)
            {
                isFloat = false;
                callBack();
                ProcessEnd();
                return;
            }
            Process(curFrameNum);
        }              

        public void StartFloat(Vector3 position, int type, Action callBack)
        {
            isFloat = true;
            lastTime = UnityPropUtils.realtimeSinceStartup;
            curFrameNum = 0;
            startPos = position;
            startPos.y += amendmentHeight;
            this.callBack = callBack;
            rect.localPosition = startPos;
            curType = typeToMode[type];
            if (curType == 1)
            {
                maxFrameNumber = 20;
            }
            else if (curType == 2)
            {
                maxFrameNumber = 25;
            }
        }

        public void SetAlpha(float alpha)
        {
            curColor.a = alpha;
            image.color = curColor;
        }

        void ProcessEnd()
        {
            position = HIDE_POSITION;
        }

        #region 模式1
        void ProcessPositionModeOne(int frame)
        {
            if (frame <= 0 || frame >= XImageFloat.positionSetOne.Length)
            {
                return;
            }
            delta.y = XImageFloat.positionSetOne[frame] * height;
            position = startPos + delta;
        }

        void ProcessScaleModeOne(int frame)
        {
            if (frame <= 0 || frame >= XImageFloat.ScaleSetOne.Length)
            {
                return;
            }
            scale = Vector3.one * XImageFloat.ScaleSetOne[frame];
        }

        void ProcessAlphaModeOne(int frame)
        {
            if (frame >= 0 && frame < AlphaSetOne.Length)
            {
                alpha = AlphaSetOne[frame];
            }
        }

        void ProcessModeOne(int frame)
        {
            ProcessPositionModeOne(frame);
            ProcessAlphaModeOne(frame);
            ProcessScaleModeOne(frame);
        }
        #endregion

        #region 模式2
        void ProcessPositionModeTwo(int frame)
        {
            if (frame <= 0 || frame >= XImageFloat.positionSetTwo.Length)
            {
                return;
            }
            delta.y = XImageFloat.positionSetTwo[frame] * height;
            position = startPos + delta;
        }

        void ProcessScaleModeTwo(int frame)
        {
            if (frame <= 0 || frame >= XImageFloat.ScaleSetTwo.Length)
            {
                return;
            }
            scale = Vector3.one * XImageFloat.ScaleSetTwo[frame];
        }

        void ProcessAlphaModeTwo(int frame)
        {
            if (frame >= 0 && frame < AlphaSetTwo.Length)
            {
                alpha = AlphaSetTwo[frame];
            }
        }

        void ProcessModeTwo(int frame)
        {
            ProcessPositionModeTwo(frame);
            ProcessAlphaModeTwo(frame);
            ProcessScaleModeTwo(frame);
        }
        #endregion

        void ProcessFlash(int frame)
        {
            if (frame <= 0 || frame >= XImageFloat.FlashSet.Length)
            {
                return;
            }
            GameMain.GlobalManager.CameraManager.GetInstance().FlashCamera(XImageFloat.FlashSet[frame]);            
        }

        void Process(int frame)
        {
            if (curType == 1)
            {
                ProcessModeOne(frame);
            }
            else if (curType == 2)
            {
                ProcessModeTwo(frame);
            }
        }
    }
}
