﻿using GameLoader.Utils;
using UIExtension;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameMain
{
    public class TweenButtonEnlarge : UIBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
    {
        private RectTransform _rect;
        private const float EXTAND_RATE = 1.1f;
        private const float ORIGN_RATE = 1.0f;
        private const float TARGET_SIDE_MAX = 400; //任意一个边长如果超过这个值，将不启用动画
        private static Vector2 PIVOT_CENTER = new Vector2(0.5f, 0.5f);
        private Vector3 _orgScale = Vector3.one;
        private bool _canPlay = true;
        public ButtonWrapper button;
        private bool _listening = false;
        protected override void Awake()
        {
            _rect = gameObject.GetComponent<RectTransform>();
            Vector2 oldPivot = _rect.pivot;
            _orgScale = _rect.localScale;
            if (_rect.sizeDelta.x * _orgScale.x > TARGET_SIDE_MAX || _rect.sizeDelta.y * _orgScale.y > TARGET_SIDE_MAX)
            {
                _canPlay = false;
            }
            else
            {
                _rect.pivot = PIVOT_CENTER;
                Vector3 localposition = _rect.localPosition;
                localposition.x += _rect.sizeDelta.x * (PIVOT_CENTER.x - oldPivot.x) * _orgScale.x;
                localposition.y += _rect.sizeDelta.y * (PIVOT_CENTER.y - oldPivot.y) * _orgScale.y;
                _rect.localPosition = localposition;
                _canPlay = true;
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (!_canPlay || (button && button.interactable == false)) return;
            _listening = true;
            TweenScale.Begin(gameObject, 0.1f, _orgScale * EXTAND_RATE, UITweener.Method.EaseOut);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (!_canPlay || (button && button.interactable == false)) return;
            _listening = false;
            TweenScale.Begin(gameObject, 0.5f, _orgScale * ORIGN_RATE, UITweener.Method.BounceIn);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!_listening) return;
            TweenScale.Begin(gameObject, 0.1f, _orgScale * EXTAND_RATE, UITweener.Method.EaseOut);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (!_listening) return;
            TweenScale.Begin(gameObject, 0.5f, _orgScale * ORIGN_RATE, UITweener.Method.BounceIn);
        }

        protected override void OnDisable()
        {
            TweenScale ts = gameObject.GetComponent<TweenScale>();
            if (ts != null && ts.enabled)
            {
                ts.enabled = false;
                _rect.localScale = _orgScale;
            }
        }
    }
}
