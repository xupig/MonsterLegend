using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using GameLoader.Utils;

namespace GameMain
{
    public class XImageUpgradeFilling : MonoBehaviour
    {
        Image _currentImage = null;
        float _amount = 1f;             //目标值
        float _fillingAmount = 1f;      //当前值

        public float FillingAmount
        {
            get { return _fillingAmount; }
        }
        bool _isFilling = false;

        public bool IsFilling
        {
            get { return _isFilling; }
        }

        float _fillingDirection = 0;
        float _damping = 0.5f;

        public float Damping
        {
            set {_damping = value; }
        }

        float _ref = 0f;

        int times = 0;                 //需要升顶的次数
        float remain = 0f;             //升顶后剩下的值

        void Awake()
        {
            _currentImage = this.GetComponent<Image>();
            if (_currentImage == null)
            {
                LoggerHelper.Error("XImageFilling can not find a Image in " + this.gameObject.name);
                this.enabled = false;
                return;
            }
            _amount = _currentImage.fillAmount;
            _fillingAmount = _currentImage.fillAmount;
        }

        //直接设定_fillingAmount，用于不进行动画播放的时候同步设定进度百分比
        public void SetValueDirectly(float value)
        {
            _fillingAmount = value;
            _amount = value;
            _isFilling = false;
            _currentImage.fillAmount = _fillingAmount;
        }
        
        public void SetValue(float value)
        {
            times = (int)value;
            remain = value % 1;

            _fillingAmount = _amount;
            _amount = value;
            if (_amount != _fillingAmount)
            {
                _isFilling = true;
                _fillingDirection = Mathf.Sign(_amount - _fillingAmount);
            }
        }

        void Update()
        {
            if (!_isFilling) return;

            if (times == 0)
            {
                _fillingAmount = Mathf.SmoothDamp(_fillingAmount, _amount, ref _ref, _damping);
                if (ACTSystem.ACTMathUtils.Abs(_fillingAmount - _amount) < 0.001f
                    || Mathf.Sign(_amount - _fillingAmount) != _fillingDirection)
                {
                    _isFilling = false;
                    _currentImage.fillAmount = _amount;
                    _fillingAmount = _amount;
                }
                else
                {
                    _currentImage.fillAmount = _fillingAmount;
                }
            }
            else
            {
                _amount = 1;

                //_ref控制速度，需要修改速度时在这里修改
                _ref = 0.2f*times;

                _fillingAmount = Mathf.SmoothDamp(_fillingAmount, _amount, ref _ref, _damping);
                if (ACTSystem.ACTMathUtils.Abs(_fillingAmount - _amount) < 0.001f
                    || Mathf.Sign(_amount - _fillingAmount) != _fillingDirection)
                {
                    _isFilling = true;
                    times--;

                    if (times == 0)
                    {
                        _amount = remain;
                        remain = 0;
                    }
  
                    _currentImage.fillAmount = 0;
                    _fillingAmount = 0;
                }
                else
                {
                    _currentImage.fillAmount = _fillingAmount;
                }

            }
           
        }
    }
}
