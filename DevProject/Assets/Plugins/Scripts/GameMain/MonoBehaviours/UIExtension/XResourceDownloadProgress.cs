﻿using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using System.IO;
using UIExtension;
using UnityEngine;

namespace GameMain
{
    public class XResourceDownloadProgress : XContainer
    {
        private TextWrapper m_textTips;
        private TextWrapper m_textProgress;
        private LuaUIProgressBar m_progressBar;
        private float m_fileTotalSize;
        private float CurrentLoadValue = 0;
        private uint LoadingTimer = uint.MaxValue;

        protected override void Awake()
        {
            base.Awake();
            var txtTips = this.transform.Find("Text_Tips");
            if (txtTips) m_textTips = txtTips.GetComponent<TextWrapper>();
            var txtProgress = this.transform.Find("Text_Progress");
            if (txtProgress) m_textProgress = txtProgress.GetComponent<TextWrapper>();
            var pg = this.transform.Find("ProgressBar_Download");
            if (pg) m_progressBar = LuaBehaviour.AddLuaBehaviour(pg.gameObject, typeof(LuaUIProgressBar), "UIComponent.Base.UIProgressBar") as LuaUIProgressBar;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            StartUpdateProgress();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            StopUpdateProgress();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            StopUpdateProgress();
        }

        public void StartUpdateProgress()
        {
            m_textTips.text = ResourceMappingManager.Instance.GetCurrentDownloadPackageName();
            m_textProgress.text = "";
            m_fileTotalSize = ResourceMappingManager.Instance.DownloadTotalCounter / 1024f / 1024f;
            ResourceMappingManager.Instance.ActionPackageBegin = OnActionPackageBegin;
            ResourceMappingManager.Instance.ActionPackageProgress = OnActionPackageProgress;
            ResourceMappingManager.Instance.ActionAllPackageFinish = OnActionAllPackageFinish;
        }

        public void StopUpdateProgress()
        {
            TimerHeap.DelTimer(LoadingTimer);
            ResourceMappingManager.Instance.ActionPackageBegin = null;
            ResourceMappingManager.Instance.ActionPackageProgress = null;
            ResourceMappingManager.Instance.ActionAllPackageFinish = null;
        }

        private void OnActionPackageBegin(string packageName)
        {
            m_textTips.text = Path.GetFileName(packageName);
        }

        private void OnActionPackageProgress(string packageName, float progress)
        {
            SmothLoading(progress);
            //m_textProgress.text = string.Format("{0:#}%\n{1:0.00}M/{2:0.00}M", progress * 100, m_fileTotalSize * progress, m_fileTotalSize);
            ////LoggerHelper.Warning(packageName + " " + progress);
            //if (m_progressBar)
            //    m_progressBar.SetProgress(progress);
        }

        private void OnActionAllPackageFinish(bool isSuccess)
        {
            m_textTips.text = "";
            //m_textProgress.text = string.Format("{0::0.00}%\n{1:0.00}M/{2:0.00}M", 100, m_fileTotalSize, m_fileTotalSize);
            m_textProgress.text = string.Format("{0:0.00}%", 100, m_fileTotalSize, m_fileTotalSize);
        }


        private void SmothLoading(float end)
        {
            //LoggerHelper.Error("SmothLoading");
            TimerHeap.DelTimer(LoadingTimer);
            SetLoading(end);
        }

        private void SetLoading(float end)
        {
            LoadingTimer = TimerHeap.AddTimer(20, 0, () =>
            {
                float seed = (end - CurrentLoadValue) * 0.03f;
                CurrentLoadValue = CurrentLoadValue + seed;
                UpdateProgress(CurrentLoadValue);
                SetLoading(end);
            });
        }

        private void UpdateProgress(float progress)
        {
            //m_textProgress.text = string.Format("{0:0.00}%\n{1:0.00}M/{2:0.00}M", progress * 100, m_fileTotalSize * progress, m_fileTotalSize);
            m_textProgress.text = string.Format("{0:0.00}%", progress * 100, m_fileTotalSize * progress, m_fileTotalSize);
            //LoggerHelper.Warning(packageName + " " + progress);
            if (m_progressBar)
                m_progressBar.SetProgress(progress);
        }
    }
}