﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Common.Utils;
using GameResource;
using UIExtension;
using GameLoader.Utils;

namespace GameMain
{
    public class XIconAnimProxyBase : MonoBehaviour
    {
        public Sprite[] sprites;
        public ImageWrapper image;
    }

    public class XIconAnimProxy : XIconAnimProxyBase
    {


        public int speed = 1;   //速度代表每多少帧换下一张图片
        public bool isAnimation = true;

        private int _spriteIndex = 0;

        int _space = 0;
        protected void Update()
        {
            if (!isAnimation)
            {
                return;
            }
            if (image == null) return;
            if (_space > 0)
            {
                _space--;
                return;
            }
            _space = speed - 1;

            if (sprites == null || sprites.Length <= _spriteIndex)
            {
                return;
            }
            image.sprite = sprites[_spriteIndex];
            _spriteIndex++;
            if (_spriteIndex >= sprites.Length)
            {
                _spriteIndex = 0;
            }
        }
    }

    public class XIconAnim : XIcon
    {
        private string[] _spriteNameList;
        private bool _animation = true;
        public bool playAnimation { set { _animation = value; } }
        private int _speed = 1;   //速度代表每多少帧换下一张图片
        public int speed { set { _speed = value; } }

        private XIconAnimProxy _spritesProxy;

        public void SetSprites(string[] spriteNameList, Color color , bool raycastTarget = false, bool frezz = false)
        {            
            _spriteNameList = spriteNameList;
            if (spriteNameList.Length > 0)
            {
                SetSprite(_spriteNameList[0], color, raycastTarget);
            }
        }

        //设置序列帧是否播放
        public void SetIsAnimation(bool flag)
        {
            if (_spritesProxy)
            {
                _spritesProxy.isAnimation = flag;
            }
        }

        protected override void DoResetSprite()
        {
            base.DoResetSprite();
            if (_atlasPrefab != null)
            {
                _spritesProxy = GameSpriteAnimationUtils.GetSpritesProxy<XIconAnimProxy>(_atlasPrefab, _spriteNameList);
                _spritesProxy.speed = _speed;
                _spritesProxy.isAnimation = _animation;
            }
        }
    }
}
