﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Common.Utils;
using GameResource;
using UIExtension;
using UnityEngine.Sprites;

namespace GameMain
{
    public class XImageFlowLight : MonoBehaviour, IMeshModifier {
        ImageWrapper _imageWrapper;
        Material _lightMaterial = null;
        bool _reseted = false;

        private int _speed = -7;

        private bool _isDestroy = false;
        public bool IsDestroy
        {
            get { return _isDestroy; }
        }

        void Awake()
        {
            _isDestroy = false;
            _imageWrapper = GetComponent<ImageWrapper>();
        }

        void OnDestroy()
        {
            _isDestroy = true;
        }

        void OnEnable()
        {
            if (_imageWrapper == null) return;
            if (_lightMaterial != null)
            {
                _imageWrapper.SetMaterial(_lightMaterial);
                return;
            }
            if (_lightMaterial == null)
            {
                Shader shader = ShaderUtils.ShaderRuntime.loader.Find("UGUI/UGUI_FLOW_LIGHT");
                if (shader != null)
                {
                    _lightMaterial = new Material(shader);
                    _lightMaterial.SetFloat("_Power", 2);
                    _lightMaterial.SetFloat("_SpeedX", _speed);
                    _lightMaterial.SetFloat("_SpeedY", 0);
                    //Vector4 uvRect = DataUtility.GetOuterUV(_imageWrapper.overrideSprite);
                    //_lightMaterial.SetVector("_UvRect", uvRect);
                    _lightMaterial.SetTextureScale("_FlowlightTex", new Vector2(0.37f, 1f));
                }
            }
            if (_lightMaterial != null)
            {
                _imageWrapper.SetMaterial(_lightMaterial);
                //no ResourceMappingManager
                GameResource.ObjectPool.Instance.GetObject(Utils.PathUtils.UIFlowLightPath, (obj) =>
                {
                    Texture2D text = obj as Texture2D;
                    _lightMaterial.SetTexture("_FlowlightTex", text);


                  
                });
            }
        }

        public void SetSpeed(int i)
        {
            _speed = i;
            if (_lightMaterial != null)
            {
                _lightMaterial.SetFloat("_SpeedX", _speed);    
            }            
        }

        //已废弃的接口函数，但必须要实现
        public void ModifyMesh(Mesh mesh) {
        }

        //修改UI的Mesh代码，强制生成第二层UV
        public void ModifyMesh(VertexHelper vh) {
            UIVertex vert = new UIVertex();

            float minX = float.MaxValue;
            float minY = float.MaxValue;
            float maxX = float.MinValue;
            float maxY = float.MinValue;
            for (int i = 0; i < vh.currentVertCount; i++) {
                vh.PopulateUIVertex(ref vert, i);
                if (vert.position.x < minX)
                    minX = vert.position.x;

                if (vert.position.y < minY)
                    minY = vert.position.y;

                if (vert.position.x > maxX)
                    maxX = vert.position.x;

                if (vert.position.y > maxY)
                    maxY = vert.position.y;
            }

            for (int i = 0; i < vh.currentVertCount; i++) {
                vh.PopulateUIVertex(ref vert, i);
                vert.uv1 = new Vector2((vert.position.x - minX) / (maxX - minX), (vert.position.y - minY) / (maxY - minY));
                vh.SetUIVertex(vert, i);
            }
        }

        void Update()
        {
            if (_reseted == false)
            {
                _reseted = true;
                _imageWrapper.enabled = false;
                _imageWrapper.enabled = true;
            }

          

        }

        public void Refresh()
        {

        }

        void OnDisable()
        {
            if (_imageWrapper == null) return;
            _imageWrapper.SetMaterial(null);
        }
    }
}
