using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Common.Utils;
using GameResource;
using UIExtension;

namespace GameMain
{
    public class XIcon : MonoBehaviour
    {
        /// <summary>
        /// 大小包替换资源
        /// </summary>
        private readonly static string m_spResPath = "Atlas/empty/a_empty.prefab";
        /// <summary>
        /// 大小包替换icon名字
        /// </summary>
        private readonly static string m_spSpriteName = "sp_empty";
        string _spriteName;
        bool _raycastTarget = false;
        List<string> _spriteParams;
        Color _color;
        float _gray;
        int _width;
        int _height;
        string _atlasPrefabPath = null;
        string atlasPrefabPath { get { return _atlasPrefabPath; } }
        protected GameObject _atlasPrefab = null;
        bool _inited = false;
        bool _autoSize = false;
        float _scale = 1f;
        float _blur = 0f;
        Color _blurColor;

        public void Init()
        {
            if (_inited) return;
            _inited = true;
            Reset();
        }

        public void Reset()
        {
            RectTransform rect = this.GetComponent<RectTransform>();
            _width = (int)rect.sizeDelta.x;
            _height = (int)rect.sizeDelta.y;
        }

        public void SetBlur(float value, Color color)
        {
            _blur = value;
            _blurColor = color;
        }

        public void AutoSize(bool value)
        {
            _autoSize = value;
        }

        public void SetSprite(string spriteName, Color color, bool raycastTarget = false, float gray = 1, float scale = 1)
        {
            Init();
            _spriteParams = GameAtlasUtils.GetSpriteParams(spriteName);
            if (_spriteParams == null) return;
            if ((_spriteName == spriteName) && (color == _color) && (gray == _gray)) return;
            string atlasPrefabPath = _spriteParams[0];
            atlasPrefabPath = ObjectPool.Instance.GetResMapping(atlasPrefabPath);
            if (atlasPrefabPath == m_spResPath)
                _spriteName = m_spSpriteName;
            else
                _spriteName = spriteName;
            _color = color;
            _gray = gray;
            _raycastTarget = raycastTarget;
            _scale = scale;
            if (atlasPrefabPath == _atlasPrefabPath)
            {
                if (_atlasPrefab != null)
                {
                    DoResetSprite();
                }
            }
            else
            {
                _atlasPrefabPath = atlasPrefabPath;
                if (_atlasPrefab != null) GameObject.Destroy(_atlasPrefab);
                LoadSprite(_atlasPrefabPath, this.gameObject);
            }
        }

        public void OnSpriteGoLoaded(GameObject go)
        {
            _atlasPrefab = go;
            DoResetSprite();
        }

        protected virtual void DoResetSprite()
        {
            ImageWrapper image = _atlasPrefab.GetComponent<ImageWrapper>();
            image.raycastTarget = _raycastTarget;
            string spriteKey = string.Format(image.spriteKeyTemplate, _spriteName);
            image.sprite = ObjectPool.Instance.GetAssemblyObject(spriteKey) as Sprite;
            image.color = _color;
            image.SetGray(_gray);
            image.SetBlur(_blur, _blurColor);
            RectTransform rect = _atlasPrefab.GetComponent<RectTransform>();
            float scaleX = _width / float.Parse(_spriteParams[5]);
            float scaleY = _height / float.Parse(_spriteParams[6]);

            if (_autoSize)
            {
                rect.pivot = new Vector2(0.5f, 0.5f);
                rect.anchoredPosition3D = new Vector3(0, 0, 0);
                rect.sizeDelta = new Vector2(int.Parse(_spriteParams[3]), int.Parse(_spriteParams[4]));
            }
            else
            {
                rect.anchoredPosition3D = new Vector3(int.Parse(_spriteParams[1]) * scaleX, -int.Parse(_spriteParams[2]) * scaleY, 0);//锚点位置等于原图裁剪位置*缩放比
                rect.sizeDelta = new Vector2(int.Parse(_spriteParams[3]) * scaleX, int.Parse(_spriteParams[4]) * scaleY);//sizeDelta等于裁剪后的图片的高宽*缩放比
            }
            rect.localScale = Vector3.one * _scale;
            rect.localRotation = new Quaternion();
            image.SetAllDirty();
        }

        private static void LoadSprite(string atlasPrefabPath, GameObject holder)
        {
            ObjectPool.Instance.GetGameObject(atlasPrefabPath, delegate (GameObject go) { OnSpriteGoLoaded(holder, go, atlasPrefabPath); });
        }

        private static void OnSpriteGoLoaded(GameObject holder, GameObject go, string atlasPrefabPath)
        {
            if (holder != null)
            {
                XIcon xIcon = holder.GetComponent<XIcon>();
                if (xIcon.atlasPrefabPath == atlasPrefabPath)
                {
                    go.transform.SetParent(holder.transform);
                    go.SetActive(true);
                    xIcon.OnSpriteGoLoaded(go);
                    return;
                }
            }
            UnityEngine.Object.Destroy(go);
        }

        /*
        void OnEnable()
        {
            if (!string.IsNullOrEmpty(_atlasPrefabPath) && _atlasPrefab == null)
            {
                LoadSprite(_atlasPrefabPath, this.gameObject);
            }
        }

        void OnDisable()
        {
            if (_atlasPrefab != null) GameObject.Destroy(_atlasPrefab);
            _atlasPrefab = null;
        }*/
    }
}
