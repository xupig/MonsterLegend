﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;


namespace GameMain
{
    /// <summary>
    /// 应用启动入口
    /// </summary>
    public class LuaBehaviour : MonoBehaviour
    {
        //public static string luaClassType;
        public string luaClassName;
        protected bool hasCalledAwake = false;
        protected bool hasCalledOnEnable = false;
        private LuaTable _luaTable;
        public LuaTable luaTable {
            private set
            {
                if (_luaTable != null)
                {
                    LoggerHelper.Error("LuaBehaviour:_luaTable 被赋值多次！");
                }
                _luaTable = value;
                CallFunction("__init__", this);
            }
            get { return _luaTable; } 
        }
        private Dictionary<string, LuaFunction> _functionsCache = new Dictionary<string, LuaFunction>();

        public static LuaBehaviour AddLuaBehaviour(GameObject go, Type type, string luaClassType)
        {
            object[] result = LuaDriver.instance.CallFunction("CSCALL_CREATE_LUA_BEHAVIOUR", luaClassType);
            var luaTable = result[0] as LuaTable;
            var luaBehaviour = go.AddComponent(type) as LuaBehaviour;
            luaBehaviour.luaTable = luaTable;
            luaBehaviour.luaClassName = luaClassType;
            if (luaBehaviour.hasCalledAwake)
            {
                luaBehaviour.CallFunction("Awake");
            }
            if (luaBehaviour.hasCalledOnEnable)
            {
                luaBehaviour.CallFunction("OnEnable");
            }
            return luaBehaviour;
        }

        protected virtual void Awake()
        {
            hasCalledAwake = true;
            if (_luaTable != null)
            {
                CallFunction("Awake");
            }
        }

        protected virtual void Start()
        {
            CallFunction("Start");
        }

        protected virtual void OnDestroy()
        {
            CallFunction("__destroy__");
            foreach (var pair in _functionsCache)
            {
                if (pair.Value != null) pair.Value.Dispose();
            }
            if (_luaTable != null)
            {
                _luaTable.Dispose();
                _luaTable = null;
            }
        }

        protected virtual void OnEnable()
        {
            hasCalledOnEnable = true;
            if (_luaTable != null)
            {
                CallFunction("OnEnable");
            }
        }

        protected virtual void OnDisable()
        {
            CallFunction("OnDisable");
        }

        private LuaFunction GetLuaFunction(string funcName)
        {
            LuaFunction func = null;
            if (!_functionsCache.ContainsKey(funcName))
            {
                if (_luaTable != null)
                {
                    func = _luaTable.GetLuaFunction(funcName);
                }
                _functionsCache.Add(funcName, func);
            }
            else
            {
                func = _functionsCache[funcName];
            }
            return func;
        }

        protected void CallFunction(string funcName)
        {
            LuaFunction func = GetLuaFunction(funcName);
            if (func != null)
            {
                func.BeginPCall();
                func.Push(_luaTable);
                func.PCall();
                func.EndPCall();
            }
            else
            {
                if (this._luaTable.IsAlive())
                {
                    LoggerHelper.Error(funcName + " function not found in " + this.luaClassName);
                }
            }
        }

        protected void CallFunction(string funcName, object arg1)
        {
            LuaFunction func = GetLuaFunction(funcName);
            if (func != null)
            {
                func.BeginPCall();
                func.Push(_luaTable);
                func.Push(arg1);
                func.PCall();
                func.EndPCall();
            }
            else
            {
                if (this._luaTable.IsAlive())
                {
                    LoggerHelper.Error(funcName + " function not found in " + this.luaClassName );
                }
            }
        }

        protected void CallFunction(string funcName, object arg1,object arg2)
        {
            LuaFunction func = GetLuaFunction(funcName);
            if (func != null)
            {
                func.BeginPCall();
                func.Push(_luaTable);
                func.Push(arg1);
                func.Push(arg2);
                func.PCall();
                func.EndPCall();
            }
            else
            {
                if (this._luaTable.IsAlive())
                {
                    LoggerHelper.Error(funcName + " function not found in " + this.luaClassName);
                }
            }
        }

        public void SetLayer(string layerName)
        {
            gameObject.layer = ACTSystem.ACTSystemTools.NameToLayer(layerName);
        }
    }
}