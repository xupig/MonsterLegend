﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameMain;
using GameLoader.Utils;
using MogoEngine.Events;
using Common.Events;
using System.Collections;

namespace GameMain
{
    /// <summary>
    /// 应用启动入口
    /// </summary>
    public class LuaDriver : MonoBehaviour
    {
        public static LuaDriver instance;

        LuaState _luaState = null;
        protected LuaLooper _loop = null;

        void Awake()
        {
            float rss = UnityPropUtils.realtimeSinceStartup;
            instance = this;
            if (GameLoader.SystemConfig.isShowGMGUI) 
                LoaderDriver.Instance.gameObject.AddComponent<LuaConsole>();
            LuaFileUtils.Instance.luaFileLoader = new GameLuaFileLoader();
            _luaState = new LuaState();
            _luaState.OpenLibs(LuaDLL.luaopen_pb);
            _luaState.OpenLibs(LuaDLL.luaopen_struct);
            OpenCJson();
            //一下效果等同于LuaBinder.Bind(_luaState);
            //之所以要用反射的方式调用，主要是不想直接引用ToLuaWrap,否则会造成互相引用，
            //ToLuaWrap需要通过GameMain生成接口绑定。
#if UNITY_IPHONE
            LuaBinder.Bind(_luaState);
#else
            Type luaBinderType = Main.assemblies["ToLuaWrap.dll"].GetType("LuaBinder");
            MethodInfo bindMethod = luaBinderType.GetMethod("Bind", BindingFlags.Static | BindingFlags.Public);
            bindMethod.Invoke(null, new object[] { _luaState });
#endif
            _luaState.LuaSetTop(0);
            _luaState.Start();
            StartLooper();
            EventDispatcher.AddEventListener<float>(ScreenEvents.ON_SCREEN_RESIZE, OnScreenResize);
            EventDispatcher.AddEventListener<Vector3>(TouchEvents.ON_TOUCH_SCREEN, OnTouchScreen);
            EventDispatcher.AddEventListener<string>("TriggerSceneWaitingArea", OnTriggerSceneWaitingArea);
            EventDispatcher.AddEventListener<bool>("OnActionAllPackageFinish", OnActionAllPackageFinish);
            EventDispatcher.AddEventListener<MogoEngine.RPC.DefCheckResult>(RPCEvents.CheckDef, OnCheckDefMD5);
            _luaState.DoFile("Main.lua");
        }

        public void SwitchConsole()
        {
            LuaConsole lc = gameObject.GetComponent<LuaConsole>();
            if (lc == null)
            {
                gameObject.AddComponent<LuaConsole>();
            }
            else
            {
                GameObject.Destroy(lc);
            }
        }

        protected void StartLooper()
        {
            _loop = gameObject.AddComponent<LuaLooper>();
            _loop.luaState = _luaState;
            //LuaLooper.SPACE_TIME = 2;
        }

        void OnApplicationQuit()
        {
            CallFunction("CSCALL_OnApplicationQuit");
            ResourceMappingManager.Instance.StopDownloadPackage();
        }

        void OnApplicationPause(bool paused)
        {
            if (paused)
            {
                GameObjectPoolManager.GetInstance().Clear();
                ACTSystem.ACTVisualFXManager.GetInstance().ReleaseACTVisualFx();
                ACTSystemAdapter.curACTSystemDriver.ObjectPool.ClearPool();
                GameObjectSoundManager.GetInstance().ClearPool();
                GameResource.ObjectPool.Instance.OnBeforeEnterScene();
                Resources.UnloadUnusedAssets();
                GC.Collect();
            }
            CallFunction("CSCALL_OnApplicationPause");
        }

        void OnScreenResize(float screenScale)
        {
            CallFunction("CSCALL_OnScreenResize", screenScale);
        }

        void OnTouchScreen(Vector3 position)
        {
            CallFunction("CSCALL_OnTouchScreen", position.x, position.y, position.z);
        }

        void OnTriggerSceneWaitingArea(string sceneName)
        {
            CallFunction("CSCALL_OnTriggerSceneWaitingArea", sceneName);
        }

        void OnActionAllPackageFinish(bool isSuccess)
        {
            CallFunction("CSCALL_OnActionAllPackageFinish", isSuccess);
        }

        void OnDestroy()
        {
            Destroy();
        }

        void CallFunction(string funcName)
        {
            var func = _luaState.GetFunction(funcName);
            if (func != null)
            {
                func.BeginPCall();
                func.PCall();
                func.EndPCall();
            }
            else
            {
                LoggerHelper.Error(funcName + " function not found in luaState!");
            }
        }

        void CallFunction(string funcName, object arg1)
        {
            var func = _luaState.GetFunction(funcName);
            if (func != null)
            {
                func.BeginPCall();
                func.Push(arg1);
                func.PCall();
                func.EndPCall();
            }
            else
            {
                LoggerHelper.Error(funcName + " function not found!");
            }
        }

        public object[] CallFunction(string funcName, params object[] args)
        {
            LuaFunction func = _luaState.GetFunction(funcName);
            if (func != null)
            {
                return func.Call(args);
            }
            else
            {
                Debug.LogError(string.Format("function {0} is not exsit", funcName));
            }
            return null;
        }

        public void LuaGC()
        {
            _luaState.LuaGC(LuaGCOptions.LUA_GCCOLLECT);
        }

        public object[] DoString(string str)
        {
            return _luaState.DoString(str, "LuaDriver.cs");
        }

        public object[] DoFile(string filename)
        {
            return _luaState.DoFile(filename);
        }

        protected void Destroy()
        {
            if (_luaState != null)
            {
                LuaState state = _luaState;
                _luaState = null;

                if (_loop != null)
                {
                    _loop.Destroy();
                    _loop = null;
                }

                state.Dispose();
                instance = null;
            }
        }

        protected void OpenCJson()
        {
            _luaState.LuaGetField(LuaIndexes.LUA_REGISTRYINDEX, "_LOADED");
            _luaState.OpenLibs(LuaDLL.luaopen_cjson);
            _luaState.LuaSetField(-2, "cjson");

            _luaState.OpenLibs(LuaDLL.luaopen_cjson_safe);
            _luaState.LuaSetField(-2, "cjson.safe");
        }

        public void TestLuaCall()
        {
            float t = UnityPropUtils.realtimeSinceStartup;
            Action a = LuaFacade.TestLuaCall;
            var func = _luaState.GetFunction("TestLuaCall");
            for (int i = 0; i < 100000; i++)
            {
                func.BeginPCall();
                func.PCall();
                func.EndPCall();
            }
            Debug.LogError("cs call lua2:" + (UnityPropUtils.realtimeSinceStartup - t));
        }

        private void OnCheckDefMD5(MogoEngine.RPC.DefCheckResult result)
        {
            switch (result)
            {
                case MogoEngine.RPC.DefCheckResult.ENUM_LOGIN_CHECK_NO_SERVICE:
                    LoggerHelper.Error("检查DEF时服务器链接不成功！");
                    CallFunction("CSCALL_ON_DEF_LINK_ERROR");
                    break;

                case MogoEngine.RPC.DefCheckResult.ENUM_LOGIN_CHECK_ENTITY_DEF_NOMATCH:
                    LoggerHelper.Error("检查DEF时MD5码不正确！");
                    CallFunction("CSCALL_ON_DEF_MD5_ERROR");
                    break;
            }
            LoggerHelper.Info("OnCheckDefMD5: " + result);
        }
    }
}