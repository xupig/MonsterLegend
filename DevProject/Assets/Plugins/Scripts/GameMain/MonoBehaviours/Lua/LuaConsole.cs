﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using MogoEngine.RPC;

namespace GameMain
{
    /// <summary>
    /// 应用启动入口
    /// </summary>
    public class LuaConsole : MonoBehaviour
    {
        List<string> _lastLogs = new List<string>();
        List<string> _lastRunStrings = new List<string>();
        int _lastIndex = -1;
        public static LuaConsole Instance { get { return _instance; } }
        static LuaConsole _instance;
        void Awake()
        {
            GameLoader.Utils.LoggerHelper.logDelegate = OnLog;
            LuaInterface.ToLua.printDelegate = OnLuaPrint;
            _instance = this;
        }

        bool isShowLog = false;
        bool isShowNetControl = false;

        void OnGUI()
        {
            if (GUI.Button(new Rect(0, 0, 40, 40), "log"))
            {
                isShowLog = !isShowLog;
            }
            if (isShowLog)
            {
                GUILayout.BeginVertical();
                ShowLog();
                ShowInput();
                GUILayout.EndHorizontal();
            }
            if (isShowNetControl)
            {
                GUILayout.BeginVertical();
                ShowNetControl();
                GUILayout.EndHorizontal();
            }
            /*
            if (GUI.Button(new Rect(20, 0, 30, 30), "C"))
            {
                Vector3 pos = EntityPlayer.Player.position;
                int x = (int)(pos.x * 100f);
                int y = (int)(pos.y * 100f);
                int z = (int)(pos.z * 100f);
                Vector3 face = EntityPlayer.Player.face;
                byte faceX = (byte)(face.x);
                byte faceY = (byte)(face.y);
                byte faceZ = (byte)(face.z);
                EntityPlayer.Player.RpcCall("cast_spell_req", 1, x, y, z, faceX, faceY, faceZ, (UInt32)Common.Global.Global.localTimeStamp);
            }
            if (GUI.Button(new Rect(50, 0, 30, 30), "S"))
            {
                GameMain.CombatSystem.DrawRangeTools.ShowDebugRange = !GameMain.CombatSystem.DrawRangeTools.ShowDebugRange;
            }
            if (GUI.Button(new Rect(90, 0, 30, 30), "L"))
            {
                //var ltm = EntityPlayer.Player.lockTargetManager;
                //if (ltm.IsPause()) { ltm.ResumeFindLock(); }
                //else{ltm.PauseFindLock();}
                EntityPlayer.Player.lockTargetManager.LockByDuration(5f);
            }*/

        }


        //float _scrollPos = 100;
        void ShowLog()
        {
            System.Text.StringBuilder _sb = new System.Text.StringBuilder(5000);
            //sb.Append("==============================================================\n");
            Queue<string> queue = new Queue<string>();
            for (int i = 0; i < _lastLogs.Count; i++)
            {
                string subStr = _lastLogs[i];
                var subArray = subStr.Split('\n');
                for (int j = 0; j < subArray.Length; j++)
                {
                    queue.Enqueue(subArray[j]);
                }
            }
            while (queue.Count > 0)
            {
                var subSt = queue.Dequeue();
                if (queue.Count > 18) continue;
                _sb.Append(subSt);
                _sb.Append("\n");
            }
            string outputString = _sb.ToString();
            //_scrollPos = GUILayout.BeginScrollView(_scrollPos, false, false, GUILayout.Width(1000), GUILayout.Height(200));
            //GUI.TextArea(new Rect(0, 0, 1000, 300), inputString.ToString());
            //GUILayout.EndScrollView();
            GUILayout.BeginHorizontal();
            //_scrollPos = GUILayout.VerticalScrollbar(_scrollPos, 10, 100, 0, GUILayout.Height(300)); 
            GUILayout.TextArea(outputString, GUILayout.Height(300), GUILayout.ExpandHeight(true));
            GUILayout.EndHorizontal();
        }

        string _inputString = string.Empty;
        void ShowInput()
        {
            GUILayout.BeginHorizontal();
            //GUILayout.Label(new Rect(0, 300, 30, 20), ">>>", GUILayout.Width(30), GUILayout.Height(20));
            GUILayout.Label(">>>", GUILayout.Width(30), GUILayout.Height(20));
            GUI.SetNextControlName("input");
            _inputString = GUILayout.TextField(_inputString, GUILayout.Height(20));
            //_inputString = GUI.TextField(new Rect(30, 300, 970, 20), _inputString);
            if (Event.current != null
                && GUI.GetNameOfFocusedControl() == "input")
            {
                if (Event.current.keyCode == KeyCode.Return)
                {
                    RunCurrentString();
                }
                else if (Event.current.keyCode == KeyCode.UpArrow)
                {
                    FindLastByUp();
                }
                else if (Event.current.keyCode == KeyCode.DownArrow)
                {
                    FindLastByDown();
                }
            }
            if (GUILayout.Button("->", GUILayout.Width(40)))
            {
                RunCurrentString();
            }
            GUILayout.EndHorizontal();
        }

        void ShowNetControl()
        {
            if (GUILayout.Button("StopRec", GUILayout.Width(200)))
            {
                ((RemoteProxy)ServerProxy.Instance).SetRecieveState(false);
            }
            if (GUILayout.Button("StartRec", GUILayout.Width(200)))
            {
                ((RemoteProxy)ServerProxy.Instance).SetRecieveState(true);
            }
            if (GUILayout.Button("GetRecCount", GUILayout.Width(200)))
            {
                OnLog(((RemoteProxy)ServerProxy.Instance).GetRecieveQueueCount().ToString());
            }
            if (GUILayout.Button("NextRec", GUILayout.Width(200)))
            {
                ((RemoteProxy)ServerProxy.Instance).RevieveOne();
                OnLog(((RemoteProxy)ServerProxy.Instance).GetRecieveQueueCount().ToString());
            }

        }

        void RunCurrentString()
        {
            string runString = _inputString;
            _inputString = string.Empty;
            if (!string.IsNullOrEmpty(runString))
            {
                if (runString == "@shownetcontrol")
                {
                    isShowNetControl = !isShowNetControl;
                    return;
                }
                OnLuaPrint(runString);
                AddLastString(runString);
                LuaDriver.instance.DoString(runString);
            }
            if (ExCommand(runString)) return;
        }

        float _upCD = 0;
        void FindLastByUp()
        {
            if (_lastRunStrings.Count == 0) return;
            if (UnityPropUtils.realtimeSinceStartup - _upCD < 0.3f) return;
            _upCD = UnityPropUtils.realtimeSinceStartup;
            _lastIndex = _lastIndex - 1;
            if (_lastIndex < 0)
            {
                _lastIndex = _lastRunStrings.Count - 1;
            }
            _inputString = _lastRunStrings[_lastIndex];
        }

        float _downCD = 0;
        void FindLastByDown()
        {
            if (_lastRunStrings.Count == 0) return;
            if (UnityPropUtils.realtimeSinceStartup - _downCD < 0.3f) return;
            _downCD = UnityPropUtils.realtimeSinceStartup;
            _lastIndex = _lastIndex + 1;
            if (_lastIndex >= _lastRunStrings.Count)
            {
                _lastIndex = 0;
            }
            _inputString = _lastRunStrings[_lastIndex];
        }

        void AddLastString(string str)
        {
            int oldIdx = _lastRunStrings.IndexOf(str);
            if (oldIdx >= 0) _lastLogs.RemoveAt(oldIdx);
            _lastRunStrings.Add(str);
            if (_lastRunStrings.Count > 10) _lastLogs.RemoveAt(0);
            _lastIndex = _lastRunStrings.Count;
        }

        void OnLog(string message)
        {
            _lastLogs.Add(message);
            if (_lastLogs.Count > 20) _lastLogs.RemoveAt(0);
        }

        void OnLuaPrint(string message)
        {
            OnLog(">>> " + message);
        }

        //*******************扩展命令****************************
        public bool ExCommand(string runString)
        {
            if (runString.StartsWith("@"))
            {
                //GMManager.GetInstance().ProcessCommand(runString);

                if (runString.Trim() == "c")
                {
                    GameMain.GlobalManager.CameraManager.GetInstance().isOpenLock = false;
                    return true;
                }
                if (runString.Trim() == "s")
                {
                    GameMain.CombatSystem.DrawRangeTools.ShowDebugRange = !GameMain.CombatSystem.DrawRangeTools.ShowDebugRange;
                    return true;
                }
                string c = "1," + runString.Trim();
                GameMain.EntityPlayer.Player.RpcCall("chat_action_req", 351, c);


                return true;
            }
            return false;
        }
    }
}
