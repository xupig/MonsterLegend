﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using MogoEngine;
using GameMain;
using MogoEngine.RPC;
using Common.Utils;
using GameMain.GlobalManager;
using GameMain.ClientConfig;

namespace GameMain
{
    public class EntityBillboard : MonoBehaviour
        {
        static readonly int BBT_Normal = 1;
        static readonly int BBT_Elite = 2;
        static readonly int BBT_Npc = 3;
        static readonly int BBT_OtherPlayer = 4;
        static readonly int BBT_Boss = 5;

        [NoToLua]
        public static readonly Vector3 HIDE_POSITION = new Vector3(-1000, -1400, 0);

        private Vector3 _lastEntityPos = Vector3.zero;
        private RectTransform _rectTransform;

        [NoToLua]
        public bool isHide { get; private set; }
        private bool _isShow = false;
        private int _showType = 0;

        private Entity m_entity = null;
        [NoToLua]
        public Entity entity
        {
            get 
            {
                if (m_entity == null && _entityId != 0) 
                { 
                    m_entity = MogoWorld.GetEntity(_entityId);
                }
                return m_entity;
            }
        }
        private bool _isAvatar = false;
        private bool _isAvatarRiding = false;
        private bool _isRidingValid = false;

        Transform _slotBillboard = null;
        private uint m_entityId = 0;
        [NoToLua]
        public uint _entityId
        {
            get { return m_entityId; }
            set {
                if (m_entityId != value)
                {
                    m_entity = null;
                    _slotBillboard = null;
                }
                m_entityId = value;
            }
        }


        void Awake()
        {
            //Canvas canvas = this.GetComponent<Canvas>();
            //if (canvas == null) this.gameObject.AddComponent<Canvas>();
            if (_rectTransform == null) _rectTransform = this.gameObject.GetComponent<RectTransform>();
        }

        public void SetEntityID(uint entityID, int type)
        {
            _entityId = entityID;
            _isAvatar = (entity != null && (entity.entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR || entity.entityType == EntityConfig.ENTITY_TYPE_NAME_PLAYER_AVATAR));
            _showType = type;
            _isShow = (type == BBT_Normal || type == BBT_Elite || type == BBT_Boss);
            if (_entityId  == 0)
            {
                this.Hide();
                EntityBillboardManager.instance.RemoveBillboard(this);
            }
            else
            {
                EntityBillboardManager.instance.AddBillboard(this);
            }
        }

        [NoToLua]
        public bool IsLooping()
        {
            if (_entityId == 0) return false;
            if (entity == null) return false;
            if (_isShow && !EntityPlayer.Player.showEntityList.Contains(_entityId)) return false;
            return true;
        }
        
        private Transform GetSlotBillboard(Entity entity)
        {
            if (_slotBillboard == null)
            {
                EntityCreature creature = entity as EntityCreature;
                if (creature.controlActor != null)
                {
                    _slotBillboard = creature.controlActor.boneController.GetBoneByName("slot_billboard");
                }
            }
            return _slotBillboard;
        }

        private Transform GetAvatarSlotBillboard(Entity entity)
        {
            EntityCreature creature = entity as EntityCreature;
            bool isRiding = creature.IsRiding();
            bool isRidingValid = creature.IsRidingValid();
            if (_slotBillboard == null || isRidingValid != _isRidingValid || _isAvatarRiding != isRiding)
            {
                _isRidingValid = isRidingValid;    
                if (creature.controlActor != null)
                {
                    //Debug.LogError("entId = " + creature.id + ", GetSlot isRiding= " + isRiding + ", value=" + _isRidingValid + ", old =" + _isAvatarRiding + ", actorName=" + creature.controlActor.name);
                    _slotBillboard = creature.controlActor.boneController.GetBoneByName("slot_billboard");
                    _isAvatarRiding = isRiding;
                }
            }
            return _slotBillboard;
        }

        private Transform GetStaticSlotBillboard(Entity entity)
        {
            if (_slotBillboard == null)
            {
                EntityStatic creature = entity as EntityStatic;
                 _slotBillboard = creature.GetBillboardSlot();
            }
            return _slotBillboard;
        }

        [NoToLua]
        public Vector3 GetEntityPos()
        {
            Vector3 pos = Vector3.zero;
            if (entity == null) return pos;
            if (_isAvatar)
            {
                Transform slotBillboard = GetAvatarSlotBillboard(entity);
                if (slotBillboard != null)
                {
                    pos = slotBillboard.position;
                }
                else
                {
                    pos = entity.position;
                }
            }
            else if (entity is EntityCreature)
            {
                Transform slotBillboard = GetSlotBillboard(entity);
                if (slotBillboard != null)
                {
                    pos = slotBillboard.position;
                }
                else 
                { 
                    pos = entity.position; 
                }
            }
            else if (entity is EntityStatic)
            {
                Transform billboard = GetStaticSlotBillboard(entity);
                if (billboard != null)
                {
                    pos = billboard.position;
                }
                else {
                    pos = entity.position;
                }
            }
            return pos;
        }

        [NoToLua]
        public bool IsEntityPosChange(Vector3 pos)
        {
            if (pos == Vector3.zero) return false;
            if (_lastEntityPos != pos)
            {
                _lastEntityPos = pos;
                return true;
            }
            return false;
        }

        [NoToLua]
        public void CalculatePosition(Vector3 screenPosition)
        {
            Vector3 localPosition = screenPosition / ScreenUtil.screenScale;
            _rectTransform.localPosition = localPosition;
        }

        [NoToLua]
        public void Show()
        {
            if (!isHide) return;
            _rectTransform.localScale = Vector3.one;
            isHide = false;
        }

        [NoToLua]
        public void Hide()
        {
            if (isHide) return;
            isHide = true;
            _rectTransform.localPosition = HIDE_POSITION;
            _rectTransform.localScale = Vector3.zero;
        }

    }
}