﻿using ACTSystem;
using Common.Data;
using GameMain.CombatSystem;
using System.Collections.Generic;

namespace GameMain
{

    public class ActorDeath : ActorDeathBase
    {
        public List<SkillEventData> effectEvents = null;

        public override void Start(ACTActor actor, float disappearTime, int disappearType)
        {
            base.Start(actor, disappearTime, disappearType);
            if (_actor != null)
            {
                _actor.animationController.Die();
            }
        }
        protected override void OnEnterDisappear()
        {
            base.OnEnterDisappear();
            var actorACTHandler = _actor.gameObject.GetComponent<ActorACTHandler>();
            if (actorACTHandler != null && effectEvents != null)
            {
                actorACTHandler.PlayEvents(this.effectEvents);
            }
        }

        protected override float GetSinkDuration()
        {
            return _sinkTime;
        }

        protected override float GetDestroyDuration()
        {
            return GetSinkDuration() + _destroyTime;
        }
    }
}