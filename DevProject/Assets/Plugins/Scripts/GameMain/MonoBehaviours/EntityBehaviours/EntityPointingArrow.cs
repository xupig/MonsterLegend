﻿using GameMain.GlobalManager;
using MogoEngine;
using UnityEngine;

namespace GameMain
{
    public class EntityPointingArrow : PosPointingArrow
    {
        private uint _entityId;
        private Vector3 _curPos;
        private Vector3 _targetPos;

        public void Show(uint id, float addtiveHeight)
        {
            _entityId = id;
            _addtiveHeight = addtiveHeight;
            enabled = true;
        }

        public override void Hide()
        {
            _entityId = 0;
            _addtiveHeight = 0;
            base.Hide();
        }

        void Update()
        {
            if (CameraManager.GetInstance().Camera == null) { return; }
            EntityCreature entity = MogoWorld.GetEntity(_entityId) as EntityCreature;
            if (entity == null || entity.controlActor == null) { return; }
            _curPos = entity.controlActor.position;
            _targetPos = new Vector3(_curPos.x, _curPos.y + _addtiveHeight, _curPos.z);
            UpdatePos(_targetPos);
        }
    }
}