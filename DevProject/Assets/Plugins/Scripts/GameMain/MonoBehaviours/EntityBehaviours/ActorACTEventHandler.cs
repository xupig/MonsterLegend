﻿using ACTSystem;
using Common.Data;
using GameData;
using GameLoader.Utils;
using GameMain.CombatSystem;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain
{

    public class ActorACTHandler : MonoBehaviour
    {
        protected ACTActor _actor;
        protected ACTHandler _actHandler;
        protected List<SkillEventData> _skillEventList = new List<SkillEventData>();
        public int qualitySetting
        {
            set { _actHandler.qualitySettingValue = value; }
        }
        void Awake()
        {
            _actor = gameObject.GetComponent<ACTActor>();
            _actHandler = new ACTHandler();
            _actHandler.priority = VisualFXPriority.L;
        }

        void Update()
        {
            DoingEvents();
        }

        public void PlayEvents(List<SkillEventData> events)
        {
            for (int i = 0; i < events.Count; i++)
            {
                var eventData = events[i];
                if (eventData.delay < 0)
                {
                    eventData.delay = ACTSkillEventHandler.GetEventDelay(eventData.mainID, eventData.eventIdx);
                }
                eventData.playTime = UnityPropUtils.realtimeSinceStartup + eventData.delay;
                _skillEventList.Add(eventData);
            }
        }

        void DoingEvents()
        {
            for (int i = _skillEventList.Count - 1; i >= 0; i--)
            {
                var eventData = _skillEventList[i];
                if (eventData.playTime < UnityPropUtils.realtimeSinceStartup)
                {
                    _actHandler.OnEvent(_actor, eventData);
                    _skillEventList.RemoveAt(i);
                }
            }
        }
    }
}