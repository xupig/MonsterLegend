﻿using UnityEngine;
using System.Collections.Generic;
using LuaInterface;
using UnityEngine.UI;
namespace GameMain
{
    public class LuaUIComponent : LuaBehaviour
    {
        protected Dictionary<string, LuaFunction> _buttonFuncs;
        private Dictionary<string, LuaFunction> buttonFuncs
        {
            get
            {
                if (_buttonFuncs == null)
                {
                    _buttonFuncs = new Dictionary<string, LuaFunction>();
                }
                return _buttonFuncs;
            }
        }

        /// <summary>
        /// 添加单击事件
        /// </summary>
        public void AddClick(GameObject go, LuaFunction luafunc)
        {
            //if (go == null || luafunc == null) return;
            //buttonFuncs.Add(go.name, luafunc);
            //go.GetComponent<Button>().onClick.RemoveAllListeners();
            UIExtension.ButtonWrapper button = go.GetComponent<UIExtension.ButtonWrapper>();
            if (button.hasClickAnim)
            {
                TweenButtonEnlarge tween = go.GetComponent<TweenButtonEnlarge>();
                if (tween == null) tween = go.AddComponent<TweenButtonEnlarge>();
                tween.button = button;
            }
            button.onClick.AddListener(
                delegate()
                {
                    //SoundManager.GetInstance().PlaySound(1);
                    luafunc.Call(go);
                }
            );
        }

        /// <summary>
        /// 删除单击事件
        /// </summary>
        /// <param name="go"></param>
        public void RemoveClick(GameObject go)
        {
            //if (go == null) return;
            //LuaFunction luafunc = null;
            //if (buttonFuncs.TryGetValue(go.name, out luafunc))
            //{
            //    go.GetComponent<Button>().onClick.RemoveAllListeners();
            //    Debug.LogError("RemoveClick2" + luafunc);
            //    luafunc.Dispose();
            //    luafunc = null;
            //    buttonFuncs.Remove(go.name);
            //    Debug.LogError("RemoveClick3 ");
            //}
            go.GetComponent<Button>().onClick.RemoveAllListeners();
        }

        /// <summary>
        /// 清除单击事件
        /// </summary>
        public void ClearClick()
        {
            if (_buttonFuncs == null) return;
            foreach (var de in _buttonFuncs)
            {
                if (de.Value != null)
                {
                    de.Value.Dispose();
                }
            }
            _buttonFuncs.Clear();
            _buttonFuncs = null;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            ClearClick();
        }

        public bool Visible
        {
            get
            {
                return gameObject.activeSelf;
            }
            set
            {
                gameObject.SetActive(value);
            }
        }
    }
}