﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GameMain
{
    public class LuaUIRawImage : RawImage,IPointerDownHandler, IPointerUpHandler, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        public Transform RoleTransform;
        private Vector2 pointerDown;
        private Transform touchArea;
        private RectTransform rectTransform;

        public const int MAX_WIDTH = 1280;
        public const int MAX_HEIGHT = 720;

        protected override void Awake()
        {
            base.Awake();
            //BloomOptimized bo = GameObject.Find("RttForCreateRole").AddComponent<BloomOptimized>();
            //bo.threshold = 0.25f;
            //bo.intensity = 0.13f;
            //bo.blurSize = 0.52f;
            //bo.blurIterations = 1;

            //Bloom bloom = GameObject.Find("RttForCreateRole").AddComponent<Bloom>();
            //bloom.bloomIntensity = 0.5f;
            //bloom.sepBlurSpread = 1.0f;
            //bloom.bloomThreshold = 0.4f;
            //bloom.bloomBlurIterations = 5;

            //Antialiasing antialiasing = GameObject.Find("RttForCreateRole").AddComponent<Antialiasing>();
            touchArea = transform.Find("TouchArea");
            if (touchArea != null)
            {
                touchArea.gameObject.AddComponent<Empty4Raycast>();
                this.raycastTarget = false;
            }

            rectTransform = gameObject.GetComponent<RectTransform>();
        }

        public void AdjustSize()
        {
            float x = (MAX_WIDTH - rectTransform.sizeDelta.x) / (MAX_WIDTH * 2);
            float y = (MAX_HEIGHT - rectTransform.sizeDelta.y) / (MAX_HEIGHT * 2);
            float width = rectTransform.sizeDelta.x / MAX_WIDTH;
            float height = rectTransform.sizeDelta.y / MAX_HEIGHT;
            this.uvRect = new Rect(x, y, width, height);
        }

        public void SetBloom(GameObject go)
        {
            /*
            if (go != null)
            {
                Bloom bloom = go.AddComponent<Bloom>();
                bloom.bloomIntensity = 0.5f;
                bloom.sepBlurSpread = 1.0f;
                bloom.bloomThreshold = 0.4f;
                bloom.bloomBlurIterations = 5;
            }
            */
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (RoleTransform != null)
            {
                pointerDown = eventData.position;
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (RoleTransform != null)
            {

            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (RoleTransform != null)
            {

            }
        }

        private Vector2 lastVector2;
        public void OnDrag(PointerEventData eventData)
        {
            if (RoleTransform != null)
            {               
                Vector2 offset = eventData.position - lastVector2;
                float deg = offset.x/500*(-720);
                RoleTransform.Rotate(Vector3.up, deg);
                lastVector2 = eventData.position;

            }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (RoleTransform != null)
            {
               lastVector2 = eventData.position;
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (RoleTransform != null)
            {

            }
        }
    }
}
