﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UIExtension;

namespace GameMain
{
    public class LuaUIInputFieldMesh : LuaUIComponent
    {
        InputFieldMeshWrapper _inuptFieldMesh;
        protected override void Awake()
        {
            base.Awake();
            _inuptFieldMesh = GetComponent<InputFieldMeshWrapper>();
            if (_inuptFieldMesh == null) _inuptFieldMesh = this.gameObject.AddComponent<InputFieldMeshWrapper>();
            _inuptFieldMesh.onEndEdit.AddListener(OnTextEndEdit);
        }

        protected override void OnDestroy()
        {
            _inuptFieldMesh.onEndEdit.RemoveListener(OnTextEndEdit);
        }

        private void OnTextEndEdit(string text)
        {
            CallFunction("OnTextMeshEndEdit", text);
        }

    }
}