﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;
//using UIExtension;
using System.Collections;
using Common.ClientConfig;

namespace GameMain
{
    /// <summary>
    /// NavigationMen是二级菜单布局容器
    /// NavigationMenu中必须含有名为item的子元素
    /// </summary>
    public class LuaUINavigationMenu : LuaUIComponent
    {
        //类内含义
        //FItem一级菜单元素
        //SItem二级菜单元素
        public const string FIRST_LEVEL_ITEM_PREFIX = "FItem_";
        public const string SECOND_LEVEL_ITEM_PREFIX = "SItem_";
        public const int DEFAULT_GAP = 0;
        /// <summary>
        /// 在KList上层有Mask的时候可以添加RenderingAgent以优化性能
        /// </summary>
        public bool isItemAddRenderingAgent = true;

        //列表项模板
        private GameObject _templateFL;//一级菜单模版
        private GameObject _templateSL;//二级菜单模版
        //一级菜单列表
        protected List<LuaUINavigationMenuItem> _fItemList = new List<LuaUINavigationMenuItem>();
        //二级菜单列表
        protected List<List<LuaUINavigationMenuItem>> _sItemList = new List<List<LuaUINavigationMenuItem>>();
        //一级菜单展开状态
        protected List<bool> _expandState = new List<bool>();
        //是否始终只有一个一级菜单展开
        private bool _onlyOneExpand;
        private bool _autoExpand = true;
        //二级菜单背景
        private GameObject _secondLevelBg;
        private RectTransform _rectSecondLevelBg;

        private int _topToDownGap = DEFAULT_GAP;        //item列间距
        private int _leftToRightGap = DEFAULT_GAP;      //item行间距
        private int _topToDownCount = 1;                //item列数量
        private int _leftToRightCount = int.MaxValue;   //item行

        private LuaUIListPadding _padding = new LuaUIListPadding();
        private LuaUIListDirection _direction = LuaUIListDirection.TopToDown;
        private Vector2 _fitemSize = Vector2.zero;
        private Vector2 _sitemSize = Vector2.zero;
        public string luaFItemType = string.Empty;
        public string luaSItemType = string.Empty;


        private LuaUINavigationMenuItem _selectedSItem;

        private XComponentEvent<LuaUINavigationMenu, int[]> _onSelectedFIndexChanged;
        private XComponentEvent<LuaUINavigationMenu, int[]> _onSelectedSIndexChanged;
        private XComponentEvent<LuaUINavigationMenu, LuaUINavigationMenuItem> _onFItemClicked;
        private XComponentEvent<LuaUINavigationMenu, LuaUINavigationMenuItem> _onSItemClicked;

        private bool _useObjectPool = false;
        private Dictionary<LuaUINavigationMenuItemType, Queue<GameObject>> _objectPool;

        private bool _isExecutingCoroutine = false;

        public XComponentEvent<LuaUINavigationMenu, int[]> onSelectedFIndexChanged
        {
            get
            {
                if (_onSelectedFIndexChanged == null)
                {
                    _onSelectedFIndexChanged = new XComponentEvent<LuaUINavigationMenu, int[]>();
                }
                return _onSelectedFIndexChanged;
            }
        }

        public XComponentEvent<LuaUINavigationMenu, int []> onSelectedSIndexChanged
        {
            get
            {
                if (_onSelectedSIndexChanged == null)
                {
                    _onSelectedSIndexChanged = new XComponentEvent<LuaUINavigationMenu, int []>();
                }
                return _onSelectedSIndexChanged;
            }
        }

        //一级菜单点击事件
        public XComponentEvent<LuaUINavigationMenu, LuaUINavigationMenuItem> onFItemClicked
        {
            get
            {
                if (_onFItemClicked == null)
                {
                    _onFItemClicked = new XComponentEvent<LuaUINavigationMenu, LuaUINavigationMenuItem>();
                }
                return _onFItemClicked;
            }
        }

        //二级菜单点击事件
        public XComponentEvent<LuaUINavigationMenu, LuaUINavigationMenuItem> onSItemClicked
        {
            get
            {
                if (_onSItemClicked == null)
                {
                    _onSItemClicked = new XComponentEvent<LuaUINavigationMenu, LuaUINavigationMenuItem>();
                }
                return _onSItemClicked;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            var rect = this.transform.GetComponent<RectTransform>();
            rect.anchorMin = new Vector2(0, 1);
            rect.anchorMax = new Vector2(0, 1);
            rect.pivot = new Vector2(0, 1);
            rect.anchoredPosition = Vector2.zero;
            InitFItemTemplate();
            InitFItemSize();
            InitSItemTemplate();
            InitSItemSize();
            InitSBg();
            if (_useObjectPool) InitObjectPool();
        }

        public LuaUIListPadding Padding
        {
            get { return _padding; }
        }

        public int TopToDownGap
        {
            get { return _topToDownGap; }
        }

        public int LeftToRightGap
        {
            get { return _leftToRightGap; }
        }

        protected void InitFItemTemplate()
        {
            _templateFL = this.transform.FindChild("FirstLevelItem").gameObject;
            var rect = _templateFL.GetComponent<RectTransform>();
            rect.anchorMin = new Vector2(0, 1);
            rect.anchorMax = new Vector2(0, 1);
            rect.pivot = new Vector2(0, 1);
            _templateFL.SetActive(false);
        }

        protected void InitSItemTemplate()
        {
            _templateSL = this.transform.FindChild("SecondLevelItem").gameObject;
            var rect = _templateSL.GetComponent<RectTransform>();
            rect.anchorMin = new Vector2(0, 1);
            rect.anchorMax = new Vector2(0, 1);
            rect.pivot = new Vector2(0, 1);
            _templateSL.SetActive(false);
        }

        protected void InitFItemSize()
        {
            RectTransform rect = _templateFL.GetComponent<RectTransform>();
            _fitemSize.x = rect.sizeDelta.x;
            _fitemSize.y = rect.sizeDelta.y;
        }

        protected void InitSItemSize()
        {
            RectTransform rect = _templateSL.GetComponent<RectTransform>();
            _sitemSize.x = rect.sizeDelta.x;
            _sitemSize.y = rect.sizeDelta.y;
        }

        //初始化二级菜单背景图(不一定有，根据UI需求)
        protected void InitSBg()
        {
            var slb = this.transform.FindChild("Image_SecondLevelBg");
            if (slb)
            {
                _secondLevelBg = slb.gameObject;
                _rectSecondLevelBg = _secondLevelBg.GetComponent<RectTransform>();
                _rectSecondLevelBg.anchorMin = new Vector2(0, 1);
                _rectSecondLevelBg.anchorMax = new Vector2(0, 1);
                _rectSecondLevelBg.pivot = new Vector2(0, 1);
            }
        }

        public virtual void SetPadding(int top, int right, int bottom, int left)
        {
            _padding.top = top;
            _padding.right = right;
            _padding.bottom = bottom;
            _padding.left = left;
        }

        public virtual void SetGap(int topToDownGap, int leftToRightGap)
        {
            _topToDownGap = topToDownGap;
            _leftToRightGap = leftToRightGap;
        }

        //设定是否始终只有一个一级菜单展开，展开另外的一级菜单会缩起其他一级菜单
        public virtual void SetOnlyOneFItemExpand(bool onlyOneExpand)
        {
            _onlyOneExpand = onlyOneExpand;
        }

        public List<LuaUINavigationMenuItem> FItemList
        {
            get
            {
                return _fItemList;
            }
        }

        /// <summary>
        /// 用于对性能比较敏感的场合，只做一次布局操作
        /// coroutineCreateCount，当通过协程的方式创建Item时每帧创建Item的数量设置
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataList"></param>
        
        public virtual void SetDataList(int[] lengthArray, int coroutineCreateCount = 0)
        {
            StopCreateItem();
            if (coroutineCreateCount == 0)
            {
                for (int i = 0; i < lengthArray.Length; i++)
                {
                    int subLength = lengthArray[i];
                    AddFItemAt(i);
                    _expandState.Insert(i,false);

                    //给二级菜单列表新增二级List
                    if (_sItemList.Count <= i)
                    {
                         List<LuaUINavigationMenuItem> subList  = new List<LuaUINavigationMenuItem>();
                        _sItemList.Insert(i, subList);
                    }
                    if (subLength > 1)
                    {
                        //数据第一项是一级菜单数据，二级菜单数据索引从2开始
                        for (int j = 1; j < subLength; j++)
                        {
                            AddSItemAt(i, j - 1);
                        }
                    }
                }
                UpdateItemListLayout();
                InvokeAllItemCreated();
            }
            else
            {
                StartCoroutine(CreateItem(lengthArray, coroutineCreateCount));
            }
        }

        protected virtual IEnumerator CreateItem(int[] lengthArray, int coroutineCreateCount)
        {
            _isExecutingCoroutine = true;
            coroutineCreateCount = coroutineCreateCount <= 0 ? 1 : coroutineCreateCount;

            for (int i = 0; i < lengthArray.Length; i++)
            {
                int subLength = lengthArray[i];
                AddFItemAt(i);
                _expandState.Insert(i, false);

                //给二级菜单列表新增二级List
                if (_sItemList.Count <= i)
                {
                    List<LuaUINavigationMenuItem> subList = new List<LuaUINavigationMenuItem>();
                    _sItemList.Insert(i, subList);
                }
                if (subLength > 1)
                {
                    int atTimes = 0;
                    //数据第一项是一级菜单数据，二级菜单数据索引从2开始
                    for (int j = 1; j < subLength; j++)
                    {
                        if (!AddSItemAt(i, j - 1)) { atTimes++; }
                        if (atTimes >= coroutineCreateCount)
                        {
                            atTimes = 0;
                            UpdateItemListLayout();
                            yield return null;
                        }
                    }
                }
                yield return null;
            }

            InvokeAllItemCreated();
            _isExecutingCoroutine = false;
        }

        protected virtual void StopCreateItem()
        {
            if (_isExecutingCoroutine == true)
            {
                StopAllCoroutines();
                UpdateItemListLayout();
                _isExecutingCoroutine = false;
            }
        }

        protected void InvokeAllItemCreated()
        {
            CallFunction("OnAllItemCreated");
        }

        //增加一级菜单元素
        private void AddFItemAt(int index, bool layoutImmediately = false)
        {
            LuaUINavigationMenuItem item = CreateItemEx(LuaUINavigationMenuItemType.FirstLevel);
            var itemGo = item.gameObject;
            AddToHierarchy(itemGo);
            item.FIndex = index;
            item.SIndex = -1;//设成-1为了Lua层判断是否为一级菜单
            item.name = GenerateFItemName(index);
            item.SetBelongList(this.luaTable);
            item.SetDataDirty();
            _fItemList.Insert(index, item);
            AddItemEventListener(item, LuaUINavigationMenuItemType.FirstLevel);
            
            if (isItemAddRenderingAgent == true)
            {
                ListItemRenderingAgent agent = itemGo.GetComponent<ListItemRenderingAgent>();
                if (agent == null)
                {
                    agent = itemGo.AddComponent<ListItemRenderingAgent>();
                    agent.SetBoundary(this.transform.parent.GetComponent<RectTransform>());
                    agent.Item = item;
                    item.renderingAgent = agent;
                }
            }
            if (layoutImmediately == true)
            {
                UpdateItemListLayout();
            }
        }

        //增加二级菜单元素
        private bool AddSItemAt(int fIndex, int sIndex, bool layoutImmediately = false)
        {
            LuaUINavigationMenuItem item = CreateItemEx(LuaUINavigationMenuItemType.SecondLevel);
            var itemGo = item.gameObject;
            AddToHierarchy(itemGo);
            item.FIndex = fIndex;
            item.SIndex = sIndex;
            item.name = GenerateSItemName(sIndex);
            item.SetBelongList(this.luaTable);
            item.SetDataDirty();

            _sItemList[fIndex].Insert(sIndex, item);
            AddItemEventListener(item, LuaUINavigationMenuItemType.SecondLevel);

            if (isItemAddRenderingAgent == true)
            {
                ListItemRenderingAgent agent = itemGo.GetComponent<ListItemRenderingAgent>();
                if (agent == null)
                {
                    agent = itemGo.AddComponent<ListItemRenderingAgent>();
                    agent.SetBoundary(this.transform.parent.GetComponent<RectTransform>());
                    agent.Item = item;
                    item.renderingAgent = agent;
                }
            }
            if (layoutImmediately == true)
            {
                UpdateItemListLayout();
            }
            return item.fromPool;
        }

        //创建一级菜单元素
        protected GameObject CreateFItemGo()
        {
            return Instantiate(_templateFL) as GameObject;
        }

        //创建二级菜单元素
        protected GameObject CreateSItemGo()
        {
            return Instantiate(_templateSL) as GameObject;
        }

        //一级菜单名字
        private string GenerateFItemName(int index)
        {
            return FIRST_LEVEL_ITEM_PREFIX + index;
        }

        //二级菜单名字
        private string GenerateSItemName(int index)
        {
            return SECOND_LEVEL_ITEM_PREFIX + index;
        }

        private void AddToHierarchy(GameObject itemGo)
        {
            itemGo.transform.SetParent(this.transform);
            itemGo.transform.localScale = Vector3.one;
            itemGo.SetActive(true);
        }

        protected void UpdateItemListLayout()
        {
            //元素个数根据展开状态动态计算
            _topToDownCount = CalculateItemCount();
            if (_secondLevelBg)
            {
                _secondLevelBg.SetActive(false);
            }
            RectTransform listRect = GetComponent<RectTransform>();
            float listRight = 0;
            float listBottom = 0;
            Vector2 totalSizeDelta = Vector2.zero;
            Vector2 position = Vector2.zero;
            position.x = _padding.left + _leftToRightGap;
            position.y = _padding.top + _topToDownGap;
            for (int i = 0; i < _fItemList.Count; i++)
            {
                LuaUINavigationMenuItem fitem = _fItemList[i];
                if (fitem.Visible)
                {
                    RectTransform itemRect = fitem.GetComponent<RectTransform>();
                    itemRect.anchoredPosition = new Vector2(position.x, -position.y);
                    itemRect.localPosition = new Vector3(itemRect.localPosition.x, itemRect.localPosition.y, 0);
                    //暂时只计算竖排版
                    position.x = _padding.left + _leftToRightGap;
                    position.y += _padding.top + _fitemSize.y + _topToDownGap;
                    float itemRight = position.x + itemRect.sizeDelta.x + _padding.right;
                    listRight = itemRight > listRight ? itemRight : listRight;
                    float itemBottom = position.y + itemRect.sizeDelta.y + _padding.bottom;
                    listBottom = itemBottom > listBottom ? itemBottom : listBottom;
                    
                }

                int sListCount = _sItemList[i].Count;
                for (int j = 0; j < sListCount; j++)
                {
                    LuaUINavigationMenuItem sitem = _sItemList[i][j];
                    //该一级菜单是展开状态
                    if(_expandState[i]){
                        RectTransform itemRect = sitem.rect;
                        itemRect.localScale = Vector3.one;
                        itemRect.anchoredPosition = new Vector2(position.x, -position.y);
                        itemRect.localPosition = new Vector3(itemRect.localPosition.x, itemRect.localPosition.y, 0);

                        //二级菜单背景处理
                        if (_secondLevelBg && j == 0)
                        {
                            _secondLevelBg.SetActive(true);
                            float slbHeight = sListCount * itemRect.sizeDelta.y + (sListCount - 1) * _topToDownGap;
                            _rectSecondLevelBg.sizeDelta = new Vector2(_rectSecondLevelBg.sizeDelta.x, slbHeight);
                            _rectSecondLevelBg.localPosition = new Vector2(_rectSecondLevelBg.localPosition.x,itemRect.localPosition.y);
                        }
                        position.x = _padding.left + _leftToRightGap;
                        position.y += _padding.top + _sitemSize.y + _topToDownGap;
                        float itemRight = position.x + itemRect.sizeDelta.x + _padding.right;
                        listRight = itemRight > listRight ? itemRight : listRight;
                        float itemBottom = position.y + itemRect.sizeDelta.y + _padding.bottom;
                        listBottom = itemBottom > listBottom ? itemBottom : listBottom;
                    }
                    //隐藏状态
                    else{
                        sitem.transform.localPosition = _hidePosition;
                        sitem.rect.localScale = Vector3.zero;
                    }
                }
                
            }
            listRect.sizeDelta = new Vector2(listRight, listBottom);
            MogoEngine.Events.EventDispatcher.TriggerEvent("UpdateScrollViewBar");
        }

        //计算列表长度
        private int CalculateItemCount()
        {
            int count = _fItemList.Count;
            for (int i = 0; i < _expandState.Count;i++ )
            {
                if (_expandState[i])
                {
                    count += _sItemList[i].Count;
                }
            }
            return count;
        }

        private void AddItemEventListener(LuaUINavigationMenuItem item, LuaUINavigationMenuItemType nmit)
        {

            if (nmit == LuaUINavigationMenuItemType.FirstLevel)
            {
                item.onClick.AddListener(OnFItemClicked);
            }
            else if (nmit == LuaUINavigationMenuItemType.SecondLevel)
            {
                item.onClick.AddListener(OnSItemClicked);
            }
        }

        private void RemoveItemEventListener(LuaUINavigationMenuItem item)
        {
            item.onClick.RemoveAllListeners();
        }

        public void SelectFItem(int findex)
        {
            LuaUINavigationMenuItem fitem =  _fItemList[findex];
            if (fitem)
            {
                int[] temp = new int[] { findex };
                OnFItemClicked(fitem, temp);
            }
           
        }

        protected virtual void OnFItemClicked(LuaUINavigationMenuItem item, int[] index)
        {
            int findex = index[0];
            if (_onFItemClicked != null)
            {
                _onFItemClicked.Invoke(this, item);
            }
            CallFunction("OnFirstLevelMenuItemClicked", item.luaTable);
            //修改展开状态
            if (_autoExpand == true)
            {
                ExpandFItems(findex);
            }
            if (_onSelectedFIndexChanged != null)
            {
                _onSelectedFIndexChanged.Invoke(this, index);
            }
        }

        public bool isExpandFItem(int findex)
        {
            return _expandState[findex];            
        }

        public void ExpandFItems(int findex)
        {
            _expandState[findex] = !_expandState[findex];
            //缩起其他一级菜单
            if (_onlyOneExpand)
            {
                for (int i = 0; i < _expandState.Count; i++)
                {
                    if (i != findex)
                    {
                        _expandState[i] = false;
                    }
                }
            }
            UpdateItemListLayout();
            
        }

        public void SetAutoExpand(bool value)
        {
            this._autoExpand = value;
        }

        protected virtual void OnSItemClicked(LuaUINavigationMenuItem item, int [] index)
        {
            int findex = index[0];
            int sindex = index[1];
            int[] oldSelectedIndex = this.SelectedIndex;
            if (_onSItemClicked != null)
            {
                _onSItemClicked.Invoke(this, item);
            }
            CallFunction("OnSecondLevelMenuItemClicked", item.luaTable);
            if (item == _selectedSItem && _selectedSItem != null && _selectedSItem.FIndex == findex && _selectedSItem.SIndex == sindex)
            {
                return;
            }
            bool isSelectedIndexChanged = (this.SelectedIndex != oldSelectedIndex);
            if (isSelectedIndexChanged == true)//若在_onItemClicked事件中修改了KList.SelectedIndex，则不再调用后续事件
            {
                return;
            }
            if (_selectedSItem != null)
            {
                _selectedSItem.IsSelected = false;
            }
            _selectedSItem = item;
            _selectedSItem.IsSelected = true;
            if (_onSelectedSIndexChanged != null)
            {
                _onSelectedSIndexChanged.Invoke(this, index);
            }
        }

        public int[] SelectedIndex
        {
            get
            {
                if (_selectedSItem == null)
                {
                    return null;
                }
                return new int[] { _selectedSItem.FIndex, _selectedSItem.SIndex };
            }
        }

        //移除所有元素
        public void RemoveAll()
        {
            for (int i = _fItemList.Count-1; i >=0 ; i--)
            {
                LuaUINavigationMenuItem fitem = _fItemList[i];
                RemoveItemEventListener(fitem);
                _fItemList.RemoveAt(i);
                fitem.Dispose();
                ReleaseItemEx(fitem);
                _expandState.RemoveAt(i);
                if(_sItemList[i] != null){
                    for (int j = _sItemList[i].Count-1; j >= 0;j-- )
                    {
                        LuaUINavigationMenuItem sitem = _sItemList[i][j];
                        RemoveItemEventListener(sitem);
                        _sItemList[i].RemoveAt(j);
                        sitem.Dispose();
                        ReleaseItemEx(sitem);
                    }
                }
            }
            UpdateItemListLayout();
        }

        //获取一级菜单元素luatable
        public LuaTable GetFItem(int index)
        {
            return _fItemList[index].luaTable; 
        }

        //获取二级菜单元素luatable
        public LuaTable GetSItem(int fIndex, int sIndex)
        {
            return _sItemList[fIndex][sIndex].luaTable;
        }


        private void InitObjectPool()
        {
            if (_objectPool == null)
            {
                _objectPool = new Dictionary<LuaUINavigationMenuItemType, Queue<GameObject>>(2);
                _objectPool[LuaUINavigationMenuItemType.FirstLevel] = new Queue<GameObject>();
                _objectPool[LuaUINavigationMenuItemType.SecondLevel] = new Queue<GameObject>();
            }
        }

        public void UseObjectPool(bool value)
        {
            this._useObjectPool = value;
            if (this._useObjectPool)
            {
                InitObjectPool();
            }
        }

        public void ClearObjectPool()
        {
            if (_objectPool != null)
            {
                foreach (var pair in _objectPool)
                {
                    var subPool = pair.Value;
                    while (subPool.Count > 0)
                    {
                        GameObject.Destroy(subPool.Dequeue());
                    }
                }
            }
        }

        protected LuaUINavigationMenuItem CreateItemEx(LuaUINavigationMenuItemType itemType)
        {
            GameObject itemGo = null;
            LuaUINavigationMenuItem item = null;
            if (this._useObjectPool)
            {
                var subPool = _objectPool[itemType];
                if (subPool.Count > 0)
                {
                    itemGo = subPool.Dequeue();
                    item = itemGo.GetComponent<LuaUINavigationMenuItem>();
                    item.fromPool = true;
                    item.rect.localScale = Vector3.one;
                }
            }
            if (itemGo == null)
            {
                GameObject _template = itemType == LuaUINavigationMenuItemType.FirstLevel ? _templateFL : _templateSL;
                itemGo = Instantiate(_template) as GameObject;
                string luaItemType = itemType == LuaUINavigationMenuItemType.FirstLevel ? this.luaFItemType : this.luaSItemType;
                item = LuaBehaviour.AddLuaBehaviour(itemGo, typeof(LuaUINavigationMenuItem), luaItemType) as LuaUINavigationMenuItem;
                item.ItemType = itemType;
            }
            return item;
        }

        Vector3 _hidePosition = new Vector3(1001, 1001, 0);
        protected void ReleaseItemEx(LuaUINavigationMenuItem item)
        {
            if (this._useObjectPool)
            {
                var subPool = _objectPool[item.ItemType];
                subPool.Enqueue(item.gameObject);
                item.IsSelected = false;
                item.rect.localPosition = _hidePosition;
                item.rect.localScale = Vector3.zero;
                item.gameObject.name = item.gameObject.GetInstanceID().ToString();
            }
            else
            {
                GameObject.Destroy(item.gameObject);
            }
        }
    }
}
