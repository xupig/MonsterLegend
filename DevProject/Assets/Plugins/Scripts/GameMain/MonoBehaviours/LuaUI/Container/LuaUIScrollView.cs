﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace GameMain
{
    public class LuaUIScrollView : LuaUIComponent
    {
        XScrollView _scrollView;
        protected override void Awake()
        {
            base.Awake();
            _scrollView = GetComponent<XScrollView>();
            if (_scrollView == null) _scrollView = this.gameObject.AddComponent<XScrollView>();
            _scrollView.ScrollRect.onRequestNext.AddListener(OnRequestNext);
            _scrollView.ScrollRect.onReRequest.AddListener(OnReRequest);
            _scrollView.ScrollRect.onMoveEnd.AddListener(OnMoveEnd);
            _scrollView.ScrollRect.onClick.AddListener(OnClick);
        }

        protected override void OnDestroy()
        {
            _scrollView.ScrollRect.onRequestNext.RemoveListener(OnRequestNext);
            _scrollView.ScrollRect.onReRequest.RemoveListener(OnReRequest);
            _scrollView.ScrollRect.onMoveEnd.RemoveListener(OnMoveEnd);
            _scrollView.ScrollRect.onClick.RemoveListener(OnClick);
        }

        private void OnRequestNext(XScrollRect scrollPage)
        {
            CallFunction("OnRequestNext");
        }

        private void OnReRequest(XScrollRect scrollPage)
        {
            CallFunction("OnRequestPre");
        }

        private void OnMoveEnd(XScrollRect scrollPage)
        {
            CallFunction("OnMoveEnd");
        }

        private void OnClick(XScrollRect scrollPage)
        {
            CallFunction("OnClick");
        }

        public void SetScrollRectState(bool active)
        {
            _scrollView.SetScrollRectState(active);
        }

        public void SetHorizontalMove(bool state)
        {
            _scrollView.ScrollRect.horizontal = state;
        }

        public void SetVerticalMove(bool state)
        {
            _scrollView.ScrollRect.vertical = state;
        }

        public void SetScrollRectEnable(bool state)
        {
            _scrollView.ScrollRect.enabled = state;
        }

        public void SetCheckEnded(bool state)
        {
            _scrollView.ScrollRect.checkEnded = state;
        }

        public void RefreshContent()
        {
            _scrollView.RefreshContent();
        }   
        
        public void ResetContentPosition()
        {
            _scrollView.ResetContentPosition();
        }

        public void ResetMaskSize(float x, float y)
        {
            _scrollView.ResetMaskSize(x, y);
        }

        public void UpdateArrow()
        {
            _scrollView.UpdateArrow();
        }
    }
}