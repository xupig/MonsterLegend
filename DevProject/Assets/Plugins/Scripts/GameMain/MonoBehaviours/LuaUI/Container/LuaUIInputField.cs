﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UIExtension;
 
namespace GameMain
{
    public class LuaUIInputField : LuaUIComponent
    {
        InputFieldWrapper _inuptField;
        protected override void Awake()
        {
            base.Awake();
            _inuptField = GetComponent<InputFieldWrapper>();
            if (_inuptField == null) _inuptField = this.gameObject.AddComponent<InputFieldWrapper>();
            _inuptField.onEndEdit.AddListener(OnTextEndEdit);
        }

        protected override void OnDestroy()
        {
            _inuptField.onEndEdit.RemoveListener(OnTextEndEdit);
        }

        private void OnTextEndEdit(string text)
        {
            CallFunction("OnTextEndEdit", text);
        }

    }
}