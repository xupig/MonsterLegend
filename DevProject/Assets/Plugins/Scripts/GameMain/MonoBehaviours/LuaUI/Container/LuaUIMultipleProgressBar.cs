﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UIExtension;
using Common.Utils;
using GameResource;
using GameData;

namespace GameMain
{
    public class LuaUIMultipleProgressBar:LuaUIComponent
    {
        private GameObject _bar;
        private GameObject _containerPointEnd;
        private RectTransform _rectBar;
        private RectTransform _rectPE;
        private TextMeshWrapper _textAttach;
        private XImageMultipleFilling _imageBarFilling;
        private XImageMultipleDelayFilling _imageBarDelayFilling;
        private bool _showTweenAnimate;
        private uint _barDelayTimeID = 0;
        private float _pointEndX;
        private float _pointEndY;
        private bool _isTweening;               //是否进入缓动动画状态

        private uint _barDelayTime;
        private uint _pointEndFadeInTime;
        private uint _pointEndFadeOutTime;
        private bool _isMultipleTweenByProgress;//是否会根据progress大与1进行多次缓动
        private bool _isResetToZero = false;     //设定progress值大于1时进行多次缓动如果progress值为整数最后一次动画之后是否归零
                                                //一般需求是true
        private int  _tweenTimes;              //缓动动画次数

        private int maxCount = 5;

        protected override void Awake()
        {
            base.Awake();
            _bar = this.transform.Find("Container_ImageBar").gameObject;
            _rectBar = _bar.GetComponent<RectTransform>();

            _barDelayTime = 300;

            var cpe = this.transform.Find("Container_PointEnd");
            if(cpe)
            {
                _containerPointEnd = cpe.gameObject;
                _rectPE = _containerPointEnd.GetComponent<RectTransform>();
                _pointEndY = (float)(_rectBar.anchoredPosition.y + _rectBar.sizeDelta.y * 0.5 - _rectPE.sizeDelta.y * 0.5);
            }

            _pointEndFadeInTime = 0;
            _pointEndFadeOutTime = 0;
            
            var txt = this.transform.Find("Text_Attach");
            if (txt) _textAttach = txt.GetComponent<TextMeshWrapper>();

            InitFilling();
            InitSprite();
        }

        Dictionary<int, string> _spriteNameDict =new Dictionary<int,string>();
        private void InitSprite()
        {
            for (int i = 1; i <= maxCount; i++)
            {
                _spriteNameDict.Add(i - 1, "Image_" + i);
            }
            _imageBarFilling.SetSpriteNameDict(_spriteNameDict);
        }

        private void OnValueChanged()
        {
           // CallFunction("OnValueChanged");
        }

        //设置barDelay延迟播放时间（ms）
        public void SetBarDelayTime(uint value)
        {
            _barDelayTime = value;
        }

        //设置是否显示缓动
        public void ShowTweenAnimate(bool shouldShow)
        {
            _showTweenAnimate = shouldShow;
            if (shouldShow && _imageBarFilling == null)
            {
                InitFilling();
            }
        }

        //设定动画时间
        public void SetTweenTime(float time)
        {
            if (_imageBarFilling == null)
            {
                InitFilling();
            }
            _imageBarFilling.Damping = time;
            if(_imageBarDelayFilling)
            {
                _imageBarDelayFilling.Damping = time;
            }
        }

        //设定progress值大于1时是否进行多次缓动
        public void SetMutipleTween(bool isMultipleTweenByProgress)
        {
            _isMultipleTweenByProgress = isMultipleTweenByProgress;
        }

        //设定progress值大于1时进行多次缓动如果progress值为整数最后一次动画之后是否归零
        public void SetIsResetToZero(bool isResetToZero)
        {
            _isResetToZero = isResetToZero;
            _imageBarFilling.SetIsResetToZero(isResetToZero);
            if (_imageBarDelayFilling)
            {
                _imageBarDelayFilling.SetIsResetToZero(isResetToZero);
            }
        }

        //初始化Bar填充组件的引用
        private void InitFilling()
        {
            if (_bar && _imageBarFilling == null)
            {
                _imageBarFilling = _bar.AddComponent<XImageMultipleFilling>();
            }
            Transform tran = this.transform.Find("Container_ImageBar/Image_Delay");
            if (tran != null && _imageBarDelayFilling == null)
            {
                _imageBarDelayFilling = _bar.AddComponent<XImageMultipleDelayFilling>();
            }
        }

        //设置进度条末端动画渐出时间
        public void SetPointEndFadeOutTime(uint value)
        {
            if (_containerPointEnd)
            {
                _pointEndFadeOutTime = value;
            }
        }

        //设置进度条末端动画渐入时间
        public void SetPointEndFadeInTime(uint value)
        {
            if (_containerPointEnd)
            {
                _pointEndFadeInTime = value;
            }
        }

        //设置附属文本
        public void SetProgressText(string txt)
        {
            if (_textAttach) _textAttach.text = txt;
        }

        public void SetProgress(float progress)
        {
            if (progress < 0) progress = 0;

            if (_showTweenAnimate)
            {
                StartTween(progress);
            }
            else
            {
                DirectSetProgress(progress);
            }
        }

        //开始播放进度动画
        private void StartTween(float currendProgressValue)
        {
            if (_imageBarFilling)
            {
                _imageBarFilling.SetValue(currendProgressValue);
            }
            
            if (_imageBarDelayFilling)
            {
                _barDelayTimeID = GameLoader.Utils.Timer.TimerHeap.AddTimer(_barDelayTime, 0, () =>
                {
                    _imageBarDelayFilling.SetValue(currendProgressValue);
                });
            }
            if (_containerPointEnd)
            {
                _containerPointEnd.SetActive(true);
            }
            _isTweening = true;
            SetPointEndPosition(currendProgressValue);
        }

        //直接设定进度，重置Filling里面的各项值
        private void DirectSetProgress(float currendProgressValue)
        {
            if (_isResetToZero && currendProgressValue == 1)
            {
                currendProgressValue = 0;
            }
                
            if (_imageBarFilling)
            {
                _imageBarFilling.SetValueDirectly(currendProgressValue);
            }
            if (_imageBarDelayFilling)
            {
                _imageBarDelayFilling.SetValueDirectly(currendProgressValue);
            }
            SetPointEndPosition(currendProgressValue);
        }

        private void SetPointEndPosition(float progress)
        {
            if (_containerPointEnd)
            {
                if (progress > 1)
                {
                    progress = progress % 1;
                }
                if (_imageBarFilling != null && _imageBarFilling.isRight)
                {
                    _pointEndX = (float)(_rectBar.anchoredPosition.x + _rectBar.sizeDelta.x * (1 - progress) - _rectPE.sizeDelta.x * 0.5);
                }
                else
                {
                    _pointEndX = (float)(_rectBar.anchoredPosition.x + _rectBar.sizeDelta.x * progress - _rectPE.sizeDelta.x * 0.5);
                }
                _rectPE.anchoredPosition = new Vector2(_pointEndX, _pointEndY);
            }
        }


        void Update()
        {
            if (_showTweenAnimate)
            {
                if (_isTweening)
                {
                    //正在播放
                    if (_imageBarFilling.IsFilling)
                    {
                        if (_containerPointEnd)
                        {
                            SetPointEndPosition(_imageBarFilling.FillingAmount);
                        }
                    }
                    //结束播放
                    else
                    {
                        TweenEnd();      
                    }
                }
            }
        }

        private void TweenEnd()
        {
            //有末端渐出状态
            if (_pointEndFadeOutTime > 0)
            {
            }
            if (_containerPointEnd)
            {
                _containerPointEnd.SetActive(false);
            }
            //进度动画播放完成回调
            _isTweening = false;
            CallFunction("OnProgressTweenCompleted");
        }
    }
}

