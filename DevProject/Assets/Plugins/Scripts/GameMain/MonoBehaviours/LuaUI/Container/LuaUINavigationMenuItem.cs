﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;
//using UIExtension;
using System.Collections;

namespace GameMain
{
    //=========================================================
    [DisallowMultipleComponent]
    public class LuaUINavigationMenuItem : LuaUIListItem, IPointerClickHandler, IPointerDownHandler
    {
        public virtual LuaUINavigationMenuItemType ItemType { get; set; }           //菜单元素类型
        //public virtual bool IsSelected { get; set; }
        public virtual int FIndex { get; set; }             //一级菜单元素索引
        public virtual int SIndex { get; set; }             //二级菜单元素索引，如果是二级菜单元素有这项值有效
        new protected XComponentEvent<LuaUINavigationMenuItem, int[]> _onClick;
        new internal bool IsDataDirty { get; set; }

        new public XComponentEvent<LuaUINavigationMenuItem, int[]> onClick
        {
            get
            {
                if (_onClick == null)
                {
                    _onClick = new XComponentEvent<LuaUINavigationMenuItem, int[]>();
                }
                return _onClick;
            }
        }

        new public void OnPointerClick(PointerEventData evtData)
        {
            if (evtData.dragging == true)
            {
                return;
            }
            if (_onClick != null)
            {
                int[] index = new int[] { this.FIndex, this.SIndex };
                _onClick.Invoke(this, index);
            }
        }
    }

    //导航菜单元素类型
    public enum LuaUINavigationMenuItemType
    {
        FirstLevel,
        SecondLevel
    }
}
