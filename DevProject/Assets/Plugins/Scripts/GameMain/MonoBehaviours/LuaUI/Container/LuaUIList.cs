﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;
//using UIExtension;
using System.Collections;

namespace GameMain
{
    /// <summary>
    /// List可以作为单行单列或多行多列布局容器
    /// List中必须含有名为item的子元素
    /// </summary>
    public class LuaUIList : LuaUIComponent
    {
        public const string ITEM_PREFIX = "item_";
        public const int DEFAULT_GAP = 0;
        public string luaItemType = string.Empty;
        public int clickSoundId = 1;
        /// <summary>
        /// 在KList上层有Mask的时候可以添加RenderingAgent以优化性能
        /// </summary>
        public bool isItemAddRenderingAgent = true;
        //列表项模板
        protected GameObject _template;
        protected List<LuaUIListItem> _itemList = new List<LuaUIListItem>();
        protected int length = 0;

        protected int _topToDownGap = DEFAULT_GAP;        //item列间距
        protected int _leftToRightGap = DEFAULT_GAP;      //item行间距
        protected int _topToDownCount = 1;                //item列数量
        protected int _leftToRightCount = int.MaxValue;   //item行

        protected LuaUIListPadding _padding = new LuaUIListPadding();
        protected LuaUIListDirection _direction;
        protected Vector2 _itemSize = Vector2.zero;

        private LuaUIListItem _selectedItem;

        private XComponentEvent<LuaUIList, int> _onSelectedIndexChanged;
        private XComponentEvent<LuaUIList, LuaUIListItem> _onItemClicked;

        private bool _isExecutingCoroutine = false;

        protected RectTransform _contentRect;
        protected RectTransform _maskRect;

        private bool _useObjectPool = false;
        private Queue<GameObject> _objectPool;
        private ListItemAnimType _itemAnimType = ListItemAnimType.None;

        public XComponentEvent<LuaUIList, int> onSelectedIndexChanged
        {
            get
            {
                if (_onSelectedIndexChanged == null)
                {
                    _onSelectedIndexChanged = new XComponentEvent<LuaUIList, int>();
                }
                return _onSelectedIndexChanged;
            }
        }

        public XComponentEvent<LuaUIList, LuaUIListItem> onItemClicked
        {
            get
            {
                if (_onItemClicked == null)
                {
                    _onItemClicked = new XComponentEvent<LuaUIList, LuaUIListItem>();
                }
                return _onItemClicked;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            _contentRect = this.transform.GetComponent<RectTransform>();
            _maskRect = this.transform.parent.GetComponent<RectTransform>();
            _contentRect.anchorMin = new Vector2(0, 1);
            _contentRect.anchorMax = new Vector2(0, 1);
            _contentRect.pivot = new Vector2(0, 1);
            _contentRect.anchoredPosition = Vector2.zero;
            InitItemTemplate();
            InitItemSize();
            if (_useObjectPool) InitObjectPool();
        }

        public LuaUIListPadding Padding
        {
            get { return _padding; }
        }

        public int TopToDownGap
        {
            get { return _topToDownGap; }
        }

        public int LeftToRightGap
        {
            get { return _leftToRightGap; }
        }

        public int TopToDownCount
        {
            get { return _topToDownCount; }
        }

        public int LeftToRightCount
        {
            get { return _leftToRightCount; }
        }

        public Vector2 ItemSize
        {
            get { return _itemSize; }
        }

        protected void InitItemTemplate()
        {
            _template = this.transform.FindChild("item").gameObject;
            var rect = _template.GetComponent<RectTransform>();
            rect.anchorMin = new Vector2(0, 1);
            rect.anchorMax = new Vector2(0, 1);
            rect.pivot = new Vector2(0, 1);
            _template.SetActive(false);
        }

        protected void InitItemSize()
        {
            RectTransform rect = _template.GetComponent<RectTransform>();
            _itemSize.x = rect.sizeDelta.x;
            _itemSize.y = rect.sizeDelta.y;
        }

        public virtual void SetPadding(int top, int right, int bottom, int left)
        {
            _padding.top = top;
            _padding.right = right;
            _padding.bottom = bottom;
            _padding.left = left;
        }

        public virtual void SetGap(int topToDownGap, int leftToRightGap)
        {
            _topToDownGap = topToDownGap;
            _leftToRightGap = leftToRightGap;
        }

        /// <summary>
        /// List分成多个columnCount * rowCount的部分（子页），当columnCount或rowCount == int.MaxValue的时候是无限长单页
        /// row 横向，排
        /// column 纵向，列
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="topToDownCount"></param>
        /// <param name="leftToRightCount"></param>
        public virtual void SetDirection(LuaUIListDirection direction, int leftToRightCount, int topToDownCount)
        {
            leftToRightCount = leftToRightCount < 0 ? int.MaxValue : leftToRightCount;
            topToDownCount = topToDownCount < 0 ? int.MaxValue : topToDownCount;
            _direction = direction;
            if (_direction == LuaUIListDirection.LeftToRight)
            {
                _leftToRightCount = leftToRightCount;
                _topToDownCount = Mathf.Min(topToDownCount, (int)(int.MaxValue / leftToRightCount));
            }
            else
            {
                _leftToRightCount = Mathf.Min(leftToRightCount, (int)(int.MaxValue / topToDownCount));
                _topToDownCount = topToDownCount;
            }
        }

        public List<LuaUIListItem> ItemList
        {
            get
            {
                return _itemList;
            }
        }

        /// <summary>
        /// 用于对性能比较敏感的场合，只做一次布局操作
        /// coroutineCreateCount，当通过协程的方式创建Item时每帧创建Item的数量设置
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataList"></param>
        public virtual void SetLength(int len, int coroutineCreateCount = 0)
        {
            StopCreateItem();
            this.length = len;
            int curListCount = _itemList.Count;
            if (len == curListCount)
            {
                SetAllItemDirty();
                InvokeAllItemCreated();
            }
            else if (len > curListCount)
            {
                if (coroutineCreateCount == 0 || !gameObject.activeInHierarchy)
                {
                    for (int i = curListCount; i < len; i++)
                    {
                        AddItem(false);
                    }
                    UpdateItemListLayout();
                    SetAllItemDirty();
                    InvokeAllItemCreated();
                }
                else
                {
                    StartCoroutine(CreateItem(coroutineCreateCount));
                }
            }
            else if (len < curListCount)
            {
                RemoveItemRange(len, curListCount - len);
                SetAllItemDirty();
                InvokeAllItemCreated();
            }
        }

        public virtual void SetItemAnimType(int itemAnimType)
        {
            this._itemAnimType = (ListItemAnimType)itemAnimType;
        }

        protected virtual IEnumerator CreateItem(int coroutineCreateCount)
        {
            _isExecutingCoroutine = true;
            coroutineCreateCount = coroutineCreateCount == 0 ? this.length : coroutineCreateCount;
            int curListCount = _itemList.Count;
            int startIndex = curListCount;
            int endIndex = startIndex;
            int atTimes = 0;
            while (endIndex < this.length)
            {
                endIndex++;
                if (!AddItem(false)) { atTimes++; }
                if (atTimes >= coroutineCreateCount)
                {
                    atTimes = 0;
                    UpdateItemListLayout();
                    yield return null;
                }
            }
            SetAllItemDirty();
            UpdateItemListLayout();
            InvokeAllItemCreated();
            _isExecutingCoroutine = false;
        }

        protected virtual void StopCreateItem()
        {
            if (_isExecutingCoroutine == true)
            {
                StopAllCoroutines();
                UpdateItemListLayout();
                SetAllItemDirty();
                _isExecutingCoroutine = false;
            }
        }

        protected void InvokeAllItemCreated()
        {
            CallFunction("OnAllItemCreated");
        }

        protected bool IsExecutingCoroutine
        {
            get
            {
                return _isExecutingCoroutine;
            }
        }

        public bool AddItem(bool layoutImmediately = true)
        {
            return AddItemAt(_itemList.Count, layoutImmediately);
        }

        public virtual bool AddItemAt(int index, bool layoutImmediately = true)
        {
            LuaUIListItem item = CreateItemEx();
            GameObject itemGo = item.gameObject;
            AddToHierarchy(itemGo);
            item.Index = index;
            item.name = GenerateItemName(index);
            item.SetBelongList(this.luaTable);
            item.SetDataDirty();
            _itemList.Insert(index, item);
            AddItemEventListener(item);

            if (isItemAddRenderingAgent == true)
            {
                ListItemRenderingAgent agent = itemGo.GetComponent<ListItemRenderingAgent>();
                if (agent == null)
                {
                    agent = itemGo.AddComponent<ListItemRenderingAgent>();
                    agent.SetBoundary(this.transform.parent.GetComponent<RectTransform>());
                    agent.Item = item;
                    item.renderingAgent = agent;
                }
            }
            if (layoutImmediately == true)
            {
                UpdateItemListLayout();
            }
            return item.fromPool;
        }

        public void SetListItemRenderingAgent(bool enable)
        {
            try
            {
                for (int i = 0; i < _itemList.Count; i++)
                {
                    if (_itemList[i].renderingAgent != null)
                        _itemList[i].renderingAgent.enabled = enable;
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        protected string GenerateItemName(int index)
        {
            return ITEM_PREFIX + index;
        }

        public GameObject TemplateGo()
        {
            return _template;
        }

        private void AddToHierarchy(GameObject itemGo)
        {
            itemGo.transform.SetParent(this.transform);
            itemGo.transform.localScale = Vector3.one;
            itemGo.SetActive(true);
        }

        public void DoLayout()
        {
            UpdateItemListLayout();
        }

        protected virtual void UpdateItemListLayout()
        {
            RectTransform listRect = GetComponent<RectTransform>();
            float listRight = 0;
            float listBottom = 0;
            int index = OffsetItemCount;
            Vector2 totalSizeDelta = Vector2.zero;
            for (int i = 0; i < _itemList.Count; i++)
            {
                LuaUIListItem item = _itemList[i];
                if (item.Visible)
                {
                    RectTransform itemRect = item.GetComponent<RectTransform>();
                    Vector2 position = CalculateItemPosition(index, itemRect);
                    float itemRight = position.x + itemRect.sizeDelta.x + _padding.right;
                    listRight = itemRight > listRight ? itemRight : listRight;
                    float itemBottom = position.y + itemRect.sizeDelta.y + _padding.bottom;
                    listBottom = itemBottom > listBottom ? itemBottom : listBottom;
                    itemRect.anchoredPosition = new Vector2(position.x, -position.y);
                    itemRect.localPosition = new Vector3(itemRect.localPosition.x, itemRect.localPosition.y, 0);
                    if (item.Index != index)
                    {
                        item.Index = index;
                        item.name = GenerateItemName(index);
                    }
                    index++;
                }
            }
            listRect.sizeDelta = new Vector2(listRight, listBottom);
            MogoEngine.Events.EventDispatcher.TriggerEvent("UpdateScrollViewBar");
        }

        protected virtual Vector2 CalculateItemPosition(int index, RectTransform itemRect)
        {
            Vector2 result = Vector2.zero;
            int pageCount = _leftToRightCount * _topToDownCount;
            if (_direction == LuaUIListDirection.LeftToRight)
            {
                result.x = _padding.left + ((int)(index / pageCount) * _leftToRightCount + index % _leftToRightCount) * (_itemSize.x + _leftToRightGap);
                result.y = _padding.top + (int)((index % pageCount) / _leftToRightCount) * (_itemSize.y + _topToDownGap);
            }
            else
            {
                result.x = _padding.left + (int)((index % pageCount) / _topToDownCount) * (_itemSize.x + _leftToRightGap);
                result.y = _padding.top + ((int)(index / pageCount) * _topToDownCount + index % _topToDownCount) * (_itemSize.y + _topToDownGap);
            }
            return result;
        }

        protected Vector2 CalculateBottomPosition()
        {
            if (_contentRect.sizeDelta.y > _maskRect.sizeDelta.y)
            {
                LuaUIListItem bottomItem = _itemList[_itemList.Count - 1];
                RectTransform itemRect = bottomItem.gameObject.GetComponent<RectTransform>();
                float y = Mathf.Abs(itemRect.anchoredPosition.y) + itemRect.sizeDelta.y - _maskRect.sizeDelta.y;
                return new Vector2(_contentRect.anchoredPosition.x, y);
            }
            else
            {
                return new Vector2(_contentRect.anchoredPosition.x, 0f);
            }
        }

        private Vector2 CalculateTopPosition()
        {
            return new Vector2(_contentRect.anchoredPosition.x, 0f);
        }

        public void MoveToBottom()
        {
            if (_itemList.Count == 0)
                return;
            _contentRect.anchoredPosition = CalculateBottomPosition();
        }

        public void MoveToTop()
        {
            if (_itemList.Count == 0)
                return;
            _contentRect.anchoredPosition = CalculateTopPosition();
        }

        public bool InBottom()
        {
            return (_contentRect.anchoredPosition - CalculateBottomPosition()).magnitude < 5;
        }

        protected virtual int OffsetItemCount
        {
            get
            {
                return 0;
            }
        }

        protected void AddItemEventListener(LuaUIListItem item)
        {
            item.onClick.AddListener(OnItemClicked);
        }

        protected void RemoveItemEventListener(LuaUIListItem item)
        {
            item.onClick.RemoveAllListeners();
        }

        protected virtual void OnItemClicked(LuaUIListItem item, int index)
        {
            int oldSelectedIndex = this.SelectedIndex;
            if (_onItemClicked != null)
            {
                _onItemClicked.Invoke(this, item);
            }
            if (clickSoundId > 0) SoundManager.GetInstance().PlaySound(clickSoundId);
            CallFunction("OnItemClicked", item.luaTable);
            if (item == _selectedItem && _selectedItem != null && _selectedItem.Index == index)
            {
                return;
            }
            bool isSelectedIndexChanged = (this.SelectedIndex != oldSelectedIndex);
            if (isSelectedIndexChanged == true)//若在_onItemClicked事件中修改了KList.SelectedIndex，则不再调用后续事件
            {
                return;
            }
            if (_selectedItem != null)
            {
                _selectedItem.IsSelected = false;
                _selectedItem.SetItemSelected(false);
            }
            item.SetItemSelected(true);
            _selectedItem = item;
            _selectedItem.IsSelected = true;
            if (_onSelectedIndexChanged != null)
            {
                _onSelectedIndexChanged.Invoke(this, index);
            }
        }

        public int SelectedIndex
        {
            get
            {
                if (_selectedItem == null)
                {
                    return -1;
                }
                return _selectedItem.Index;
            }
            set
            {
                if (_selectedItem != null)
                {
                    _selectedItem.IsSelected = false;
                    _selectedItem.SetItemSelected(false);
                    _selectedItem = null;
                }
                if (value >= 0 && value <= _itemList.Count - 1)
                {
                    _selectedItem = _itemList[value];
                    _selectedItem.SetItemSelected(true);
                    _selectedItem.IsSelected = true;
                }
            }
        }

        public void RemoveItemAt(int index)
        {
            LuaUIListItem item = _itemList[index];
            RemoveItemEventListener(item);
            //item.gameObject.SetActive(false);
            _itemList.RemoveAt(index);
            item.Dispose();
            ReleaseItemEx(item);
            UpdateItemListLayout();
        }

        public virtual void RemoveAll()
        {
            RemoveItemRange(0, _itemList.Count);
        }

        public virtual void RemoveItemRange(int start, int count)
        {
            for (int i = start + count - 1; i >= start; i--)
            {
                LuaUIListItem item = _itemList[i];
                RemoveItemEventListener(item);
                _itemList.RemoveAt(i);
                item.Dispose();
                ReleaseItemEx(item);
            }
            UpdateItemListLayout();
        }

        public virtual LuaTable GetItem(int index)
        {
            return _itemList[index].luaTable; 
        }

        public virtual void SetAllItemDirty()
        {
            for (int i = 0; i < _itemList.Count; i++)
            {
                _itemList[i].SetDataDirty();
            }
        }

        public virtual void RefreshAllItemData()
        {
            for (int i = 0; i < _itemList.Count; i++)
            {
                _itemList[i].DoRefreshData();
            }
        }

        private void InitObjectPool()
        {
            if (_objectPool == null)
            {
                _objectPool = new Queue<GameObject>();
            }
        }

        public void UseObjectPool(bool value)
        {
            this._useObjectPool = value;
            if (this._useObjectPool)
            {
                InitObjectPool();
            }
        }

        public void ClearObjectPool()
        {
            if (_objectPool == null) return;
            while (_objectPool.Count > 0)
            {
                GameObject.Destroy(_objectPool.Dequeue());
            }
        }

        protected LuaUIListItem CreateItemEx()
        {
            GameObject itemGo = null;
            LuaUIListItem item = null;
            if (this._useObjectPool)
            {
                if (_objectPool.Count > 0)
                {
                    itemGo = _objectPool.Dequeue();
                    item = itemGo.GetComponent<LuaUIListItem>();
                    item.fromPool = true;
                    item.rect.localScale = Vector3.one;
                }
            }
            if (itemGo == null)
            {
                itemGo = Instantiate(_template) as GameObject;
                item = LuaBehaviour.AddLuaBehaviour(itemGo, typeof(LuaUIListItem), this.luaItemType) as LuaUIListItem;
                item.animType = this._itemAnimType;
            }
            return item;
        }

        Vector3 _hidePosition = new Vector3(1001, 1001, 0);
        protected void ReleaseItemEx(LuaUIListItem item)
        {
            if (this._useObjectPool)
            {
                _objectPool.Enqueue(item.gameObject);
                item.IsSelected = false;
                item.rect.localPosition = _hidePosition;
                item.rect.localScale = Vector3.zero;
                item.gameObject.name = item.gameObject.GetInstanceID().ToString();
            }
            else
            {
                GameObject.Destroy(item.gameObject);
            }
        }
    }

    public struct LuaUIListPadding
    {
        public int top;
        public int right;
        public int bottom;
        public int left;
    }

    public enum LuaUIListDirection
    {
        //
        /// <summary>
        /// List Item布局时，纵向布局优先（TopToDown）横向布局优先（LeftToRight）
        /// 如单列列表应该是（LeftToRight = 1, TopToDown = int.Max）;
        /// 如单行列表应该是（LeftToRight = int.Max, TopToDown = 1）;
        /// </summary>
        TopToDown,
        LeftToRight
    }
}
