﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UIExtension;
using GameMain;
using Common.Data;

namespace GameMain
{
    public class LuaUIChatTextBlock : LuaUIComponent
    {
        GameObject _chatContainer = null;
        ChatTextBlock _block = null;
        RectTransform _chatBGRec = null;
        ImageWrapper _imageWrapperBG = null;
        protected override void Awake()
        {
            base.Awake();
            GameObject chatBG = this.transform.Find("Container_BG/Image_MessageBG").gameObject;
            _imageWrapperBG = chatBG.GetComponent<ImageWrapper>();
            _chatBGRec = chatBG.GetComponent<RectTransform>();
            _chatContainer = this.transform.Find("Container_Message").gameObject;
        }

        protected override void OnDestroy()
        {
        }

        public int AddTextBlock(int channel, string paramstr, string liststr, string content)
        {
            if(_block != null)
            {
                _block.GameObject.transform.SetParent(null);
                GameObject.Destroy(_block.GameObject);
            }
            float maxWidth = 270.0f;
            PbChatInfoResp data = new PbChatInfoResp();
            data.SetTestData();
            if (channel == -1)
            {
                data.channel_id = 4;
            }
            else
            {
                data.channel_id = (uint)channel;
            }
            string[] paramsArr = paramstr.Split(',');
            data.send_dbid = ulong.Parse(paramsArr[1]);
            data.send_level = uint.Parse(paramsArr[4]);
            data.send_name = paramsArr[2];
            data.msg = content;// UIChatManager.Instance.ChatData.FilterWordsInfo.FilterContent(content);
            data.seq_id = UIChatManager.Instance.ChatId;

            UIChatManager.Instance.ChatData.ChatLinkData.AddChatLinkInfo(data);
            if (channel == -1)
            {
                _block = ChatTextBlock.CreateBlock(data, maxWidth, ChatTextBlock.COLOR_SYSTEM_TEAM_CANNEL, false);
            }
            else
            {
                _block = ChatTextBlock.CreateBlock(data, maxWidth, ChatTextBlock.COLOR_SYSTEM_WORLD_CANNEL, false);
            }
            _block.GameObject.transform.SetParent(_chatContainer.transform);
            RectTransform rect = _block.GameObject.GetComponent<RectTransform>();
            rect.localScale = Vector3.one;
            rect.anchoredPosition3D = Vector3.zero;
            
            //if(EntityPlayer.Player.cross_uuid == paramsArr[0])
            //{
            //    rect.anchoredPosition = new Vector2(-_block.Width, -52.0f);
            //}
            //else
            //{
                rect.anchoredPosition = new Vector2(78, -56.0f);
            //}
            UpdateBg(_block.Width, _block.Height);

            return (int)_block.Height;
        }

        private void UpdateBg(float width, float height)
        {
            _chatBGRec.sizeDelta = new Vector2(width+30, height + 10);
            _imageWrapperBG.SetAllDirty();
        }
    }
}