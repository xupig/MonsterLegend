﻿using UnityEngine.EventSystems;
using UIExtension;
using TMPro;
using UnityEngine;

namespace GameMain
{
    public class LuaUILinkTextMesh : LuaUIComponent, IPointerClickHandler, IPointerDownHandler
    {
        TextMeshWrapper _textMeshWrapper;
        protected override void Awake()
        {
            base.Awake();
            _textMeshWrapper = GetComponent<TextMeshWrapper>();
            if (_textMeshWrapper == null) _textMeshWrapper = this.gameObject.AddComponent<TextMeshWrapper>();
        }

        protected override void OnDestroy()
        {
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            //GameLoader.Utils.LoggerHelper.Debug("OnPointerClick");
            int linkIndex = TMP_TextUtilities.FindIntersectingLink(_textMeshWrapper, eventData.position, GetUICamera());
            //GameLoader.Utils.LoggerHelper.Debug("linkIndex:" + linkIndex);
            string linkId = "";
            string linkText = "";
            if (linkIndex != -1 )
            {
                TMP_LinkInfo linkInfo = _textMeshWrapper.textInfo.linkInfo[linkIndex];
                linkId = linkInfo.GetLinkID();
                linkText = linkInfo.GetLinkText();
            }

            CallFunction("OnClickLink",linkId,eventData.position);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
        }

        private Camera GetUICamera()
        {
            var go = GameObject.Find("UICamera");
            if (go == null)
            {
                return null;
            }
            Camera camera = go.GetComponent<Camera>();
            return camera;
        }
    }
}