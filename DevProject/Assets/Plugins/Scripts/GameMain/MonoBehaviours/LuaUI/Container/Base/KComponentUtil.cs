﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

namespace GameMain
{
    public class KComponentUtil
    {
        private static bool OPEN_LOG_WARNING = false;
        private const string ITEM_NAME_PREFIX = "item_";
        private const string ITEM_NAME = "item";

        public static GameObject GetChild(GameObject host, string childName)
        {
            Transform childTransform = host.transform.Find(childName);
            if(childTransform == null)
            {
                if(OPEN_LOG_WARNING == true)
                {
                    Debug.LogWarning(string.Format("未找到GameObject {0} 的名为 {1} 的次级GameObject！", host.name, childName));
                }
                return null;
            }
            return childTransform.gameObject;
        }

        public static T GetChildComponent<T>(GameObject host, string childName) where T : Component
        {
            GameObject child = GetChild(host, childName);
            if(child == null)
            {
                if(OPEN_LOG_WARNING == true)
                {
                    Debug.LogWarning(string.Format("未找到GameObject {0} 的名为 {1} 的次级GameObject！", host.name, childName));
                }
                return null;
            }
            T component = child.GetComponent<T>();
            if(component == null)
            {
                if(OPEN_LOG_WARNING == true)
                {
                    Debug.LogWarning(string.Format("未找到GameObject {0} 上类型为 {1} 的Component", host.name, typeof(T)));
                }
            }
            return component;
        }

        public static T AddChildComponent<T>(GameObject host, string childName) where T : Component
        {
            GameObject child = GetChild(host, childName);
            if(child == null)
            {
                if(OPEN_LOG_WARNING == true)
                {
                    Debug.LogWarning(string.Format("未找到GameObject {0} 的名为 {1} 的次级GameObject！", host.name, childName));
                }
                return null;
            }
            T component = child.AddComponent<T>();
            if(component == null)
            {
                if(OPEN_LOG_WARNING == true)
                {
                    Debug.LogWarning(string.Format("未找到GameObject {0} 上类型为 {1} 的Component", host.name, typeof(T)));
                }
            }
            return component;
        }


        /// <summary>
        /// 获取直到面板Canvas的路径
        /// </summary>
        /// <param name="go"></param>
        /// <returns></returns>
        public static string GetAudioSettingPath(GameObject go)
        {
            Transform trans = go.transform;
            string path = GetAudioSettingName(trans.name);
            do
            {
                trans = trans.parent;
                path = GetAudioSettingName(trans.name) + "/" + path;
            }
            while(trans.GetComponent<Canvas>() == null);
            return path;
        }

        /// <summary>
        /// 列表中动态创建Item在音频设置中的名字需要去除序号数字
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static string GetAudioSettingName(string name)
        {
            if(name.Contains(ITEM_NAME_PREFIX) == true)
            {
                return ITEM_NAME;
            }
            return name;
        }
    }

}
