﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace GameMain
{
    public class LuaUIScrollPage : LuaUIComponent
    {
        XScrollPage _scrollPage;
        public int TotalPage
        {
            get
            {
                return _scrollPage.TotalPage;
            }
            set
            {
                _scrollPage.TotalPage = value;
            }
        }

        public int CurrentPage
        {
            get
            {
                return _scrollPage.CurrentPage;
            }
            set
            {
                _scrollPage.CurrentPage = value;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            _scrollPage = GetComponent<XScrollPage>();
            if (_scrollPage == null) _scrollPage = this.gameObject.AddComponent<XScrollPage>();
            _scrollPage.onCurrentPageChanged.AddListener(OnCurrentPageChanged);
            _scrollPage.onRequestPrePage.AddListener(OnRequestPrePage);
            _scrollPage.onRequestNextPage.AddListener(OnRequestNextPage);
        }

        protected override void OnDestroy()
        {
            _scrollPage.onCurrentPageChanged.RemoveListener(OnCurrentPageChanged);
            _scrollPage.onRequestPrePage.RemoveListener(OnRequestPrePage);
            _scrollPage.onRequestNextPage.RemoveListener(OnRequestNextPage);
        }

        private void OnCurrentPageChanged(XScrollPage scrollPage, int page)
        {
            CallFunction("OnCurrentPageChanged", page);
        }

        private void OnRequestPrePage(XScrollPage scrollPage)
        {
            CallFunction("OnRequestPrePage");
        }

        private void OnRequestNextPage(XScrollPage scrollPage)
        {
            CallFunction("OnRequestNextPage");
        }
    }
}