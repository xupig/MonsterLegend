﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;
//using UIExtension;
using System.Collections;

namespace GameMain
{
    public enum ListItemAnimType
    { 
        None = 0,
        DownToUp = 1,
    }

    //=========================================================
    [DisallowMultipleComponent]
    public class LuaUIListItem : LuaUIComponent, IPointerClickHandler, IPointerDownHandler
    {
        public virtual bool IsSelected { get; set; }
        public virtual int Index { get; set; }
        //public virtual System.Object Data { get; set; }
        protected XComponentEvent<LuaUIListItem, int> _onClick;
        internal bool IsDataDirty { get; set; }
        public virtual ListItemAnimType animType { get; set; }
        public ListItemRenderingAgent renderingAgent = null;

        public bool fromPool = false;

        private GameObject _itemSelected = null;
        private GameObject _itemUnSelected = null;

        public virtual int ID
        {
            get;
            set;
        }

        public XComponentEvent<LuaUIListItem, int> onClick
        {
            get
            {
                if (_onClick == null)
                {
                    _onClick = new XComponentEvent<LuaUIListItem, int>();
                }
                return _onClick;
            }
        }

        RectTransform _rect = null;
        public RectTransform rect
        {
            get
            {
                if (_rect == null)
                {
                    _rect = this.GetComponent<RectTransform>();
                }
                return _rect;
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            EventSystem.current.SetSelectedGameObject(gameObject, eventData);
        }

        public void OnPointerClick(PointerEventData evtData)
        {
            if (evtData.dragging == true)
            {
                return;
            }
            if (_onClick != null)
            {
                _onClick.Invoke(this, this.Index);
            }
        }

        public void SetDataDirty()
        {
            IsDataDirty = true;
        }

        public void DoRefreshData() 
        {
            CallFunction("__refresh__");
        }

        public void SetBelongList(LuaTable belongList)
        {
            CallFunction("__belonglist__", belongList);
        }

        public virtual void Show() { }
        public virtual void Hide() { }
        public virtual void Dispose() { }

        protected override void OnEnable()
        {
            base.OnEnable();
            Show();
            OnPlayAnim();
        }

        protected void OnAnimFinished()
        {
            CallFunction("__animfinished__");
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            Hide();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            Dispose();
        }

        Transform _root = null;
        protected void OnPlayAnim()
        {
            if (this.animType == ListItemAnimType.None) return;
            if (_root == null)
            {
                _root = this.transform.FindChild("item_root");
                if (_root == null)
                {
                    LoggerHelper.Error("can't find 'item_root' in " + this.gameObject.name + " for play anim!");
                    return;
                }
            }
            switch (this.animType)
            { 
                case ListItemAnimType.DownToUp:
                    AnimDownToUp();
                    break;
                default:
                    break;
            }
        }

        protected void AnimDownToUp()
        {
            var rect = _root.GetComponent<RectTransform>();
            rect.anchoredPosition = new Vector3(0, -500 + -100 * Index, 0);
            TweenPosition.Begin(_root.gameObject, 0.3f, Vector3.zero, 0.02f * Index, UITweener.Method.EaseIn, (ut) => { OnAnimFinished(); });
        }

        protected override void Awake()
        {
            base.Awake();
            InitItemTemplate();
        }

        protected void InitItemTemplate()
        {
            var selected = this.transform.FindChild("Selected");
            if (selected != null)
            {
                _itemSelected = selected.gameObject;
            }
            var unselected = this.transform.FindChild("UnSelected");
            if (unselected != null)
            {
                _itemUnSelected = unselected.gameObject;
            }
            SetItemSelected(false);
        }

        public void SetItemSelected(bool value)
        {
            //LoggerHelper.Error("============================SetItemSelected");
            if (_itemSelected != null)
            {
                //LoggerHelper.Error("===========111=================SetItemSelected");
                _itemSelected.SetActive(value);
            }
            if (_itemUnSelected != null)
            {
                //LoggerHelper.Error("===========222=================SetItemSelected");
                _itemUnSelected.SetActive(!value);
            }
        }
    }
}
