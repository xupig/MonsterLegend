﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;


namespace GameMain
{
    public class ScrollPageArrow : XContainer
    {
        private Image _nextImage;
        private Image _prevImage;

        protected override void Awake()
        {
            _nextImage = GetChildComponent<Image>("Image_arrowNext");
            _prevImage = GetChildComponent<Image>("Image_arrowPrev");
        }

        public void UpdateArrow(int current, int total)
        {
            HideArrow();
            if(current > 0)
            {
                Color nPColor = _prevImage.color;
                nPColor.a = 1.0f;
                _prevImage.color = nPColor;
            }
            if(current < total - 1)
            {
                Color nNColor = _prevImage.color;
                nNColor.a = 1.0f;
                _nextImage.color = nNColor;
            }
        }

        private void HideArrow()
        {
            Color nColor = _nextImage.color;
            nColor.a = 1.0f;
            _nextImage.color = nColor;
            nColor  = _prevImage.color;
            nColor.a = 1.0f;
            _prevImage.color = nColor;
        }
    }

}
