﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UIExtension;

namespace GameMain
{
    public class LuaUIToggle : LuaUIComponent
    {
        ToggleWrapper _toggle;
        protected override void Awake()
        {
            base.Awake();
            _toggle = GetComponent<ToggleWrapper>();
            if (_toggle == null) _toggle = this.gameObject.AddComponent<ToggleWrapper>();
            _toggle.onValueChanged.AddListener(OnValueChanged);
        }

        protected override void OnDestroy()
        {
            _toggle.onValueChanged.RemoveListener(OnValueChanged);
        }

        private void OnValueChanged(bool state)
        {
            CallFunction("OnValueChanged", state);
        }

        public void SetIsOn(bool state)
        {
            _toggle.isOn = state;
        }
    }
}