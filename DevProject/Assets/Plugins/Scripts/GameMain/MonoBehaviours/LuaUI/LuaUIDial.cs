﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameMain
{
    public class LuaUIDial : LuaBehaviour
    {
        public static readonly Vector3 HIDE_POSITION = new Vector3(-1000, -1400, 0);

        private Transform itemsTran;
        private Transform selectTran;
        private bool isTurning = false;
        private bool isSelecting = false;
        private float curTime = 0;
        private float totalLength = 40;
        private float curLength = 0;
        private float speed = 1;
        private float accelerate = 4;
        private int totalCount = 8; //item的个数
        private int curIndex = 0;
        private int lastIndex = 0;
        public float stateTime = 1f; //单个阶段时间
        public int totalTurns = 6; //总的转动圈数
        public float minSpeed = 3f; //最后停止修正速度
        private Dictionary<int, Transform> posT = new Dictionary<int, Transform>();
        private Queue<int> queue = new Queue<int>();

        private float curSelectTime = 0;
        private float totalSelectTime = 1;

        protected override void Awake()
        {
            itemsTran = transform.Find("Container_Items");
            selectTran = transform.Find("Image_Select");
            for (int i = 0; i < itemsTran.childCount; i++)
            {
                posT.Add(i, itemsTran.GetChild(i));
            }
            totalCount = itemsTran.childCount;
            selectTran.position = HIDE_POSITION;
        }

        void Update()
        {
            if (queue.Count > 0 && !isTurning && !isSelecting)
            {
                int index = queue.Dequeue();
                Turn(index);
            }
            if (isTurning && !isSelecting)
            {
                curTime += UnityPropUtils.deltaTime;
                if (curTime < stateTime)
                {
                    speed += accelerate * UnityPropUtils.deltaTime;
                }
                else if (curTime > stateTime + stateTime)
                {
                    speed = Math.Max(speed - accelerate * UnityPropUtils.deltaTime, minSpeed);
                }
                curLength += speed * UnityPropUtils.deltaTime;
                curIndex = (int)Math.Floor(curLength) % totalCount;
                if(curIndex != lastIndex)
                {
                    selectTran.position = posT[curIndex].position;
                    lastIndex = curIndex;
                }
                if(curLength >= totalLength)
                {
                    SelectBegin(curIndex);
                    EndTurn();
                }
            }
            if (isSelecting)
            {
                curSelectTime += UnityPropUtils.deltaTime;
                if (curSelectTime > totalSelectTime)
                {
                    SelectEnd(curIndex);
                }
            }
        }
        //最后选中表现效果
        private void SelectBegin(int index)
        {
            isSelecting = true;
            curSelectTime = 0;
        }

        private void SelectEnd(int index)
        {
            isSelecting = false;
            selectTran.position = HIDE_POSITION;
            if (queue.Count <= 0)
            {
                EndRoundTurn();
            }
        }

        private void Turn(int index)
        {
            curLength = lastIndex;
            speed = 0;
            curTime = 0;
            totalLength = totalCount * totalTurns + index;
            accelerate = totalLength / (stateTime * stateTime * 2);
            isTurning = true;
        }

        private void EndTurn()
        {
            speed = 0;
            curTime = 0;
            isTurning = false;
        }

        public void StartTurn(int[] index)
        {
            int length = index.Length;
            for (int i = 0; i < index.Length; i++)
            {
                queue.Enqueue(index[i] - 1);
            }
        }

        private void EndRoundTurn()
        {
            CallFunction("__endroundturn__");
        }
    }
}

