﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace GameMain
{
    public class LuaUIPointableDragable : LuaUIComponent, IPointerDownHandler, IPointerUpHandler, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        public Action<PointerEventData> onDragEvent;
        public Action<PointerEventData> onBeginDragEvent;
        public Action<PointerEventData> onEndDragEvent;

        public void OnPointerDown(PointerEventData eventData)
        {
            CallFunction("OnPointerDown", eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            CallFunction("OnPointerUp", eventData);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            CallFunction("OnPointerClick", eventData);
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (onDragEvent != null)
            {
                onDragEvent(eventData);
            }
            CallFunction("OnDrag", eventData);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (onBeginDragEvent != null)
            {
                onBeginDragEvent(eventData);
            }
            CallFunction("OnBeginDrag", eventData);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (onEndDragEvent != null)
            {
                onEndDragEvent(eventData);
            }
            CallFunction("OnEndDrag", eventData);
        }
    }
}