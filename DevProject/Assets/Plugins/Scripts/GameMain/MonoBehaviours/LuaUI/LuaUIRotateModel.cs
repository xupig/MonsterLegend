﻿using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GameMain
{
    public class LuaUIRotateModel : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        public Transform targetTransform;
        private Transform touchArea;
        private RectTransform rectTransform;

        public const int MAX_WIDTH = 1280;
        public const int MAX_HEIGHT = 720;

        protected void Awake()
        {
            touchArea = transform.Find("TouchArea");
            if (touchArea != null)
            {
                touchArea.gameObject.AddComponent<Empty4Raycast>();
            }
            else 
            {
                LoggerHelper.Error("未找到TouchArea");
            }

            rectTransform = gameObject.GetComponent<RectTransform>();
        }

        private Vector2 lastVector2;
        public void OnDrag(PointerEventData eventData)
        {
            if (targetTransform != null)
            {               
                Vector2 offset = eventData.position - lastVector2;
                float deg = offset.x/500*(-720);
                targetTransform.Rotate(Vector3.up, deg);
                lastVector2 = eventData.position;

            }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (targetTransform != null)
            {
               lastVector2 = eventData.position;
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (targetTransform != null)
            {

            }
        }

        public void SetTragetTransform(Transform trans)
        {
            targetTransform = trans;
        }
    }
}

