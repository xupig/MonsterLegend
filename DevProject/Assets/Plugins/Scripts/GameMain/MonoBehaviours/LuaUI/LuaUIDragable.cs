﻿using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using LuaInterface;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace GameMain
{
    public class LuaUIDragable : LuaUIComponent, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        public void OnDrag(PointerEventData eventData)
        {
            CallFunction("OnDrag", eventData);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            CallFunction("OnBeginDrag", eventData);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            CallFunction("OnEndDrag", eventData);
        }
    }
}