﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameMain
{
    public class LuaUIRoller : LuaBehaviour
    {
        public static readonly Vector3 HIDE_POSITION = new Vector3(-1000, -1400, 0);

        public string luaItemType = string.Empty;
        protected int length = 0;

        private Transform itemsTran;
        private Transform maskTran;
        private int totalCount = 0; //item的个数


        Vector3 _fromPos;              //起始值
        Vector3 _curPos;              //当前值
        Vector3 _toPos;                //目标值    
        int _startItemCount = 0;     //初始显示的item数
        float _duration = 0f;
        float _itemSize = 0f;
        Vector3 _add;
        float _xSign;
        float _ySign;
        float _zSign;
        bool _isTweening = false;

        float _width = 10;

        private Dictionary<int, Transform> posT = new Dictionary<int, Transform>();
        private Dictionary<int, LuaUIRollerItem> items = new Dictionary<int, LuaUIRollerItem>();
        private Queue<int> queue = new Queue<int>();

        protected override void Awake()
        {
            itemsTran = transform.Find("Mask/Container_Items");
            maskTran = transform.Find("Mask");
            _width = maskTran.GetComponent<RectTransform>().sizeDelta.x;

            totalCount = itemsTran.childCount;
            for (int i = 0; i < itemsTran.childCount; i++)
            {
                posT.Add(i, itemsTran.GetChild(i));
            }
        }

        public void SetItemType(string type)
        {
            luaItemType = type;
            for (int i = 0; i < totalCount; i++)
            {
                GameObject go = posT[i].gameObject;
                LuaUIRollerItem item = LuaBehaviour.AddLuaBehaviour(go, typeof(LuaUIRollerItem), this.luaItemType) as LuaUIRollerItem;
                items.Add(i, item);
                item.SetBelongList(this.luaTable);
            }
        }

        public void SetLength(int len)
        {
            Reset();
            this.length = len;
        }

        private void Reset()
        {
            for (int i = 0; i < totalCount; i++)
            {
                LuaUIRollerItem item = items[i];
                item.transform.localPosition = HIDE_POSITION;
                item.Reset();
            }
            _lastShowItem = null;
        }

        int curIndex = 1;
        int startShowCount = 0;
        Vector3 startPos;
        public void OnStart()
        {
            _isTweening = true;
            curIndex = 1;
            for (int i = 0; i < totalCount; i++)
            {
                LuaUIRollerItem item = items[i];
                item.transform.localPosition = _fromPos;
            }

            startShowCount = Math.Min(_startItemCount, totalCount);
            for (int i = 1; i <= startShowCount; i++)
            {
                LuaUIRollerItem item = GetItem();
                if (_xSign > 0)
                {
                    startPos = _fromPos + new Vector3(_itemSize * i, 0, 0);
                }
                if (_xSign < 0)
                {
                    startPos = _fromPos - new Vector3(_itemSize * i, 0, 0);
                }
                item.transform.localPosition = startPos;
                _lastShowItem = item;
            }
        }

        public void OnContinue()
        {
            _isTweening = true;
        }

        public void OnStop()
        {
            _isTweening = false;
        }

        public LuaUIRollerItem GetItem()
        {
            for (int i = 0; i < totalCount; i++)
            {
                LuaUIRollerItem item = items[i];
                if (!item.use)
                {
                    curIndex = (curIndex - 1) % length + 1;
                    item.Reset();
                    item.use = true;
                    item.onEnd = false;
                    item.transform.localPosition = _fromPos;
                    item.Index = curIndex;
                    curIndex++;
                    item.DoRefreshData();
                    return item;
                }
            }
            return null;
        }

        LuaUIRollerItem _lastShowItem;
        void Update()
        {
            if (!_isTweening || length == 0) return;

            if (_lastShowItem == null)
            {
                _lastShowItem = GetItem();
            }
            if (CanGetNext())
            {
                _lastShowItem = GetItem();
            }

            for (int i = 0; i < totalCount; i++)
            {
                LuaUIRollerItem item = items[i];
                if (!item.use)
                {
                    continue;
                }
                _curPos = item.transform.localPosition;
                _curPos += _add * UnityPropUtils.deltaTime;
                if (!CheckDirection(_curPos))
                {
                    _curPos = _toPos;
                    item.onEnd = true;
                }

                if (item.onEnd)
                {
                    //一次结束
                    _curPos = _fromPos;
                    OnRoundEnd(item);
                }
                SetPosition(item.transform);
            }
        }

        private bool CanGetNext()
        {
            if (_lastShowItem != null)
            {
                if (_xSign > 0 && _lastShowItem.transform.localPosition.x > (_fromPos.x + _itemSize))
                {
                    return true;
                }
                if (_xSign < 0 && _lastShowItem.transform.localPosition.x < (_fromPos.x - _itemSize))
                {
                    return true;
                }
            }
            return false;
        }


        //一圈结束
        private void OnRoundEnd(LuaUIRollerItem item)
        {
            item.Stop();
        }

        public void SetFromToPos(Vector3 formPos, Vector3 toPos, float duration, float itemSize, int startItemCount)
        {
            _curPos = formPos;
            _fromPos = formPos;
            _toPos = toPos;
            _duration = duration;
            _itemSize = itemSize;
            _startItemCount = startItemCount;

            _xSign = Mathf.Sign(_toPos.x - _curPos.x);
            _ySign = Mathf.Sign(_toPos.y - _curPos.y);
            _zSign = Mathf.Sign(_toPos.z - _curPos.z);

            _add = new Vector3((_toPos.x - _fromPos.x) / _duration, (_toPos.y - _fromPos.y) / _duration, (_toPos.z - _fromPos.z) / _duration);
        }

        private bool CheckDirection(Vector3 curPos)
        {
            if (Mathf.Sign(_toPos.x - curPos.x) == _xSign && Mathf.Sign(_toPos.y - curPos.y) == _ySign && Mathf.Sign(_toPos.z - curPos.z) == _zSign)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void SetPosition(Transform trans)
        {
            trans.localPosition = _curPos;
        }
    }
}

