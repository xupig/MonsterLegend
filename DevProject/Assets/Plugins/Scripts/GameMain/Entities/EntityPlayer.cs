﻿using ACTSystem;
using GameData;
using GameLoader.Utils;
using LuaInterface;
using MogoEngine.Events;
using MogoEngine.RPC;
using ShaderUtils;
using System;
using System.Collections.Generic;
using UnityEngine;
using GameMain.CombatSystem;
using Common.ServerConfig;
using GameMain.GlobalManager;
using GameLoader.Utils.Timer;

namespace GameMain
{

    public class EntityPlayer : EntityCreature
    {

        public static EntityPlayer Player = null;
        public static EntityCreature ShowPlayer = null;
        public PlayerLockTargetManager lockTargetManager;
        public PlayerBillboardManager billboardManager;

        #region def属性

        /// <summary>
        /// 客户端用来表示选中的操作模式
        /// 1: 页游操作方式 左键移动、右键转镜头
        /// 2: LOL操作方式 
        /// </summary>
        public Byte control_setting_chosen { get; set; }
        /// <summary>
        /// 客户端用来表示LOL模式选中的控制技能方式
        /// </summary>
        public Byte control_setting_lol_move { get; set; }
        /// <summary>
        /// 客户端Web Player模式选中的控制技能方式
        /// </summary>
        public Byte control_setting_web_player { get; set; }
        /// <summary>
        /// 自己才看得到的buff
        /// 格式是 1001:10;1002:20
        /// </summary>
        //public string buff_client { get; set; }

        public override UInt16 level
        {
            get { return m_level; }
            set
            {
                m_level = value;
                SetBattleAttribute(battle_attri_config.ATTRI_ID_LEVEL, m_level);
            }
        }

        #endregion def属性

        public static bool battleServerMode = true;
        public static bool isLibraMode = false;
        public static bool pauseSyncPos = false;

        public EntityPlayer()
        {
            RegisterPropertySetDelegate(typeof(Action<float>), "base_speed");
            RegisterMethodDelegate("SelfCastSpellResp");
            RegisterMethodDelegate("OtherCastSpellResp");
            RegisterMethodDelegate("SpellDamageResp");
            RegisterMethodDelegate("SpellActionResp");
            RegisterMethodDelegate("SpellActionOriginResp");
            RegisterMethodDelegate("SpellTargetResp");
            RegisterMethodDelegate("OtherSpellActionResp");
            RegisterMethodDelegate("SpellCdChangeResp");
            RegisterMethodDelegate("AddBuffResp");
            RegisterMethodDelegate("sync_time_resp");
            RegisterMethodDelegate("BuffHpChangeResp");
            RegisterMethodDelegate("MonsterPartDestroyResp");
        }

        public override void OnEnterWorld()
        {
            Player = this;
            LocalCacheHelper.Init(Player.dbid.ToString());
            base.OnEnterWorld();
            visualFXPriority = VisualFXPriority.H;
            var accordingMode = CombatSystem.AccordingMode.AccordingStick;
            this.moveManager.accordingMode = accordingMode;
            this.moveManager.defaultAccordingMode = accordingMode;
            PerformaceManager.GetInstance().OnPlayerEnterWorld();
            PlayerSyncPosManager.GetInstance().OnPlayerEnterWorld();
            PlayerEventManager.GetInstance().OnPlayerEnterWorld();
            this.lockTargetManager = new PlayerLockTargetManager(this);
            this.billboardManager = new PlayerBillboardManager(this);
            PlayerTimerManager.GetInstance().OnPlayerEnterWorld();
            PreloadManager.GetInstance().OnPlayerEnterWorld();
            PlayerSkillManager.GetInstance().OnPlayerEnterWorld();
            PlayerSkillPreviewManager.GetInstance().OnPlayerEnterWorld();
            ProjectionManager.GetInstance().OnPlayerEnterWorld();
        }

        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
            PerformaceManager.GetInstance().OnPlayerLeaveWorld();
            ProjectionManager.GetInstance().OnPlayerLeaveWorld();
            PlayerSyncPosManager.GetInstance().OnPlayerLeaveWorld();
            PlayerEventManager.GetInstance().OnPlayerLeaveWorld();
            this.lockTargetManager.Release();
            PlayerTimerManager.GetInstance().OnPlayerLeaveWorld();
            PreloadManager.GetInstance().OnPlayerLeaveWorld();
            PlayerSkillManager.GetInstance().OnPlayerLeaveWorld();
            PlayerSkillPreviewManager.GetInstance().OnPlayerLeaveWorld();
            this.showEntityList.Clear();
            this.showEntityDict.Clear();
        }

        public override void OnEnterSpace()
        {
            base.OnEnterSpace();
        }

        public override void OnLeaveSpace()
        {
            base.OnLeaveSpace();
        }



        public override void OnActorLoaded()
        {
            base.OnActorLoaded();
            if (isInWorld)
            {
                SetHideSkin(PlayerSettingManager.hidePlayer);
            }
            if (this.actor != null)
            {
                UnityEngine.Object.DontDestroyOnLoad(this.actor.gameObject);
                CameraTargetProxy.instance.SetTargetTransform(this.actor.boneController.GetBoneByName("slot_camera"));
                CameraManager.GetInstance().SetFollowingTarget(CameraTargetProxy.instance.transform);
                this.BecomeShadowTarget(20, true);
                this.actor.actorController.alwaysCheckGround = true;
                this.actor.actorController.usePlayerController = true;
                //Ari 为Demo屏蔽 this.actor.equipController.equipCloth.isShelter = true;
                InspectorSetting.InspectingTool.Inspect(new InspectingPlayer(this), this.actor.gameObject);
                this.actor.IsUseFootSmoke = true;
                this.actor.IsUseFootSound = true;
                _audioListener = this.actor.gameObject.GetComponent<AudioListener>();
                if (_audioListener == null)
                {
                    _audioListener = this.actor.gameObject.AddComponent<AudioListener>();
                    AudioManager.GetInstance().SetAudioListener(false);
                }
                lockTargetManager.ForceRefreshInSafeArea();
            }
        }

        protected override bool CheckVisibleChanged(bool newVisible, bool newShow)
        {
            return _show != newShow;
        }

        public override float PlaySkillByPos(int pos)
        {
            try
            {
                int skillId = this.skillManager.PlaySkillByPos(pos);
                if (skillId == 0) return 0;
                var skillData = CombatLogicObjectPool.GetSkillData(skillId);
                float commonCD = this.skillManager.GetCommonCD() - RealTime.time;
                return commonCD;
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("PlaySkillByPos:" + ex.Message + "\n" + ex.StackTrace);
                return -1;
            }
        }

        public void SetPlayerFlySkillPosList(int[] flySkillPosArray)
        {
            //字典以数组形式传入，自己组装
            Dictionary<int, int> flySkillPosDict = new Dictionary<int, int>();
            for (int i = 0; i < flySkillPosArray.Length; i += 2)
            {
                flySkillPosDict.Add(flySkillPosArray[i], flySkillPosArray[i + 1]);
            }
            PlayerSkillManager.GetInstance().SetPlayerFlySkillPosDict(flySkillPosDict);
        }

        public void SetPlayerGroundSkillPosList(int[] groundSkillPosArray)
        {
            //字典以数组形式传入，自己组装
            Dictionary<int,int> groundSkillPosDict = new Dictionary<int,int>();
            for (int i = 0; i < groundSkillPosArray.Length; i += 2)
            {
                groundSkillPosDict.Add(groundSkillPosArray[i], groundSkillPosArray[i + 1]);
            }
            PlayerSkillManager.GetInstance().SetPlayerGroundSkillPosDict(groundSkillPosDict);
        }

        public void SetPlayerAirSkillPosList(int[] airSkillPosArray)
        {
            //字典以数组形式传入，自己组装
            Dictionary<int, int> airSkillPosDict = new Dictionary<int, int>();
            for (int i = 0; i < airSkillPosArray.Length; i += 2)
            {
                airSkillPosDict.Add(airSkillPosArray[i], airSkillPosArray[i + 1]);
            }
            PlayerSkillManager.GetInstance().SetPlayerAirSkillPosDict(airSkillPosDict);
        }

        public void InitSkillButtons()
        {
            PlayerSkillManager.GetInstance().isInit = true;
            this.skillManager.OnPosChange();
        }

        public void GlideDeath()
        {
            bufferManager.RemoveBuffer(65001);
            bufferManager.RemoveBuffer(4050);
            LuaFacade.Destroy(GameObject.Find("fly(Clone)"));
            Teleport(new Vector3(850.1f, 1696.29f, 217));
            UnityEngine.Transform target = CameraTargetProxy.instance.transform;
            List<float> globalParams = global_params_helper.CameraInitial;
            float distance = globalParams[0];
            GameMain.GlobalManager.CameraManager.GetInstance().ChangeToRotationTargetMotion(target, new UnityEngine.Vector3(5, 0, 0), 0, 0, distance, 0);
            GameObject ui = GameObject.Find("UICamera");
            ui.SetActive(false);
            GameLoader.Utils.Timer.TimerHeap.AddTimer<GameObject>(2100, 0, (u) => { u.SetActive(true); }, ui);
        }

        public override void OnEnterTransform()
        {
            this.CallLuaSelfFunction("OnEnterTransform");
        }

        public override void OnLeaveTransform()
        {
            this.CallLuaSelfFunction("OnLeaveTransform");
        }

        public void FaceToCamera()
        {
            var camera = CameraManager.GetInstance().Camera;
            if (camera != null)
            {
                float newY = camera.transform.eulerAngles.y + 180;
                if (newY > 360) newY -= 360;
                Vector3 newFace = new Vector3(0, newY * 0.5f, 0);
                this.face = newFace;
            }
        }

        public void ClearTarget()
        {
            this.lockTargetManager.ClearTarget();
        }

        #region 技能RPC
        public void SelfCastSpellResp(byte[] pbByte)
        {
            PlayerSkillManager.GetInstance().SelfCastSpellResp(pbByte);
        }

        public void OtherCastSpellResp(byte[] pbByte)
        {
            PlayerSkillManager.GetInstance().OtherCastSpellResp(pbByte);
        }

        public void SpellDamageResp(byte[] pbByte)//(UInt32 attackerID, UInt16 skillID, UInt16 skillActionID, byte[] skillDamageByte)
        {
            PlayerSkillManager.GetInstance().SpellDamageResp(pbByte);
        }

        //<!-- 通知玩家自己的技能的某个行为激活 -->
        public void SpellActionResp(byte[] pbSpellActionByte)
        {
            //LoggerHelper.Debug("Ari:skill:SelfSpellActionResp:" + skillID + "::" + skillActionID + "::" + targetID);
            PlayerSkillManager.GetInstance().SpellActionResp(pbSpellActionByte);
        }

        public void SpellActionOriginResp(byte[] pbSpellActionOrigin)
        {
            PlayerSkillManager.GetInstance().SpellActionOriginResp(pbSpellActionOrigin);
        }

        public void SpellTargetResp(byte[] pbByte)
        {

        }

        public void OtherSpellActionResp(byte[] pbOtherSpellAction)
        {
            PlayerSkillManager.GetInstance().OtherSpellActionResp(pbOtherSpellAction);
        }

        public void SpellCdChangeResp(byte[] pbCdChangeSpells)
        {
            PlayerSkillManager.GetInstance().SpellCdChangeResp(pbCdChangeSpells);
        }
        #endregion

        #region Buff

        public void AddBuffResp(byte[] pbAddBuff)
        {
            PlayerBuffManager.GetInstance().AddBuffResp(pbAddBuff);
        }

        public void BuffHpChangeResp(byte[] pbBuffHpChange)
        {
            PlayerBuffManager.GetInstance().BuffHpChangeResp(pbBuffHpChange);
        }


        #endregion

        #region ServerTime

        public void sync_time_resp(UInt64 serverTime)
        {
            //LoggerHelper.Error("C# sync_time_resp: " + serverTime);
            var serverTimeStr = serverTime.ToString();
            serverTimeStr = serverTimeStr.Substring(0, serverTimeStr.Length - 3);
            UInt32 serverTimeI = UInt32.Parse(serverTimeStr);
            CallLuaSelfFunction("sync_time_resp", serverTimeI);
            PlayerTimerManager.GetInstance().SyncTimeResp(serverTime);
        }

        #endregion

        public void MonsterPartDestroyResp(byte[]  pbMonsterPartDestroy)
        {
            PlayerSkillManager.GetInstance().MonsterPartDestroyResp(pbMonsterPartDestroy);
        }
    }
}
