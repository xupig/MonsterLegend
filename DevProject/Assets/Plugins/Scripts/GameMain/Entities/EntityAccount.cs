﻿using System;
using GameMain.GlobalManager;

namespace GameMain
{
    public class EntityAccount : EntityLuaBase
    {
        public EntityAccount()
        {
            RegisterMethodDelegate("sync_time_resp");
        }

        public void sync_time_resp(UInt64 serverTime)
        {
            //LoggerHelper.Error("C# sync_time_resp: " + serverTime);
            var serverTimeStr = serverTime.ToString();
            serverTimeStr = serverTimeStr.Substring(0, serverTimeStr.Length - 3);
            UInt32 serverTimeI = UInt32.Parse(serverTimeStr);
            CallLuaSelfFunction("sync_time_resp", serverTimeI);
            PlayerTimerManager.GetInstance().SyncTimeResp(serverTime);
        }
    }
}
