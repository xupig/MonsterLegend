﻿using ACTSystem;
using GameData;
using GameLoader.Utils;
using LuaInterface;
using MogoEngine.Events;
using MogoEngine.RPC;
using ShaderUtils;
using System.Collections.Generic;
using UnityEngine;
using GameMain.CombatSystem;
using Common.ServerConfig;
using GameMain.GlobalManager;
using Common.ClientConfig;
using MogoEngine.Mgrs;
namespace GameMain
{

    public class EntityStatic : EntityLuaBase
    {
        static public string SLOT_BILLBOARD_NAME = "slot_billboard";
        static public float CameraSlotHight = 1;
        private Transform billboardSlot;
        public int modelID;
        public int effectModelID;
        private StaticModel _model;      //物品模型
        private StaticModel _effectModel;//特效模型
        private bool _destroyEffectAfterLoaded = false;
        private bool _destroyModelAfterLoaded = false;
        public override void OnEnterWorld()
        {
            base.OnEnterWorld();
        }

        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
            DestroyModel();
            DestroyEffectModel();
        }

        public void SetModel(int id)
        {
            if (this.modelID == id) return;
            this.modelID = id;
            DestroyModel();
            CreateModel();
        }

        protected bool _modelLoaded = false;
        private void CreateModel()
        {
            if (this._modelLoaded) return;
            this._modelLoaded = true;
            int closeModelID = this.modelID;
            GameObjectPoolManager.GetInstance().CreateStaticModelGameObject(closeModelID, (model) =>
            {
                if (model == null)
                {
                    return;
                }
                if (isInWorld && closeModelID == this.modelID)
                {
                    _model = model.GetComponent<StaticModel>();
                    _model.position = this.position;
                    OnModelLoaded();
                }
                else
                {
                    GameObjectPoolManager.GetInstance().ReleaseStaticModelGameObject(closeModelID, model);
                }
            });
        }

        public void DestroyModel()
        {
            if (_model != null)
            {
                GameObjectPoolManager.GetInstance().ReleaseStaticModelGameObject(_model.modelID, _model.gameObject);
                _model = null;
                this.modelID = 0;
            }
        }

        protected void OnModelLoaded()
        {
            if (_destroyModelAfterLoaded)
            {
                FlyToEntity();
            }
            else
            {
                InitSlotCamera();
                FixPosition();
            }
            this.CallLuaSelfFunction("OnModelLoaded");
        }

        protected void InitSlotCamera()
        {
            var bil = _model.transform.FindChild(SLOT_BILLBOARD_NAME);
            if (bil == null)
            {
                bil = new GameObject(SLOT_BILLBOARD_NAME).transform;
                bil.SetParent(_model.transform, false);
                Vector3 oldPos = bil.localPosition;
                oldPos.y = CameraSlotHight;
                bil.localPosition = oldPos;
            }
            billboardSlot = bil;
        }

        //设置模型初始化scale
        public void InitModelScale(float scale)
        {
            _model.scale = new Vector3(scale, scale, scale);
        }

        //////////////////////////////////掉落道具特效动画加载//////////////////////////////////////////
        /// <summary>
        /// 
        /// </summary>
        public void SetEffectModel(int id)
        {
            if (this.effectModelID == id) return;
            this.effectModelID = id;
            DestroyEffectModel();
            CreateEffectModel();
        }

        protected bool _effectModelLoaded = false;
        private void CreateEffectModel()
        {
            if (this._effectModelLoaded) return;
            this._effectModelLoaded = true;
            int closeModelID = this.effectModelID;
            GameObjectPoolManager.GetInstance().CreateStaticModelGameObject(closeModelID, (model) =>
            {
                if (model == null)
                {
                    return;
                }
                if (isInWorld && closeModelID == this.effectModelID)
                {
                    _effectModel = model.GetComponent<StaticModel>();
                    _effectModel.position = this.position;
                    OnEffectModelLoaded();
                }
                else
                {
                    GameObjectPoolManager.GetInstance().ReleaseStaticModelGameObject(closeModelID, model);
                }
            });
        }

        public void DestroyEffectModel()
        {
            if (_effectModel != null)
            {
                GameObjectPoolManager.GetInstance().ReleaseStaticModelGameObject(_effectModel.modelID, _effectModel.gameObject);
                _effectModel = null;
                this.effectModelID = 0;
            }
        }

        protected void OnEffectModelLoaded()
        {
            this.CallLuaSelfFunction("OnEffectModelLoaded");
            if (_destroyEffectAfterLoaded)
            {
                DestroyEffectModel();
            }else
            {
                FixPosition();
            }
        }

        public override void SetPosition(Vector3 position)
        {
            base.SetPosition(position);
            if (_model)
            {
                _model.position = position;
                FixPosition();
            }
            if (_effectModel)
            {
                _effectModel.position = position;
            }
        }

        //设置随机位置
        public void SetRandomPosition(Vector3 position, float randRate)
        {
            Vector3 newPos = Vector3.zero;
            //暂时先不用
            for (int i = 0; i < 10; i++)
            {
                newPos.x = position.x + Random.Range(-1.0f, 1.0f) * randRate;
                newPos.y = position.y;
                newPos.z = position.z + Random.Range(-1.0f, 1.0f) * randRate;
                if (FindPathManager.GetInstance().CheckCurrPointIsCanMove(newPos.x, newPos.z))
                {
                    break;
                }
                //十次随机失败直接使用原位置
                if (i == 9)
                {
                    newPos = position;
                }
            }

            SetPosition(newPos);
        }



        public override Transform GetTransform()
        {
            if (this._model != null)
            {
                return this._model.transform;
            }
            else
            {
                return base.GetTransform();
            }
        }

        //设置飞向某个实体（暂时飞向主角）
        public void FlyToEntity()
        {
            if (this._model)
            {
                this._model.FlyToEntity(this.id);
            }
            else
            {
                _destroyModelAfterLoaded = true;
            }
            if (this._effectModel)
            {
                this.DestroyEffectModel();
            }
            else
            {
                _destroyEffectAfterLoaded = true;
            }
        }

        public void FlyToEntity(uint entityId)
        {
        }

        static RaycastHit _raycastHit = new RaycastHit();
        static int _terrainLayerValue = 1 << PhysicsLayerDefine.LAYER_TERRAIN;
        protected virtual void FixPosition()
        {
            if (GetTransform() == null) return;
            var actorPos = this.position;
            actorPos.y = actorPos.y + 100;
            Physics.Raycast(actorPos, Vector3.down, out _raycastHit, 120f, _terrainLayerValue);
            GetTransform().position = _raycastHit.point;
            this.position = _raycastHit.point;
        }

        public Transform GetBillboardSlot()
        {
            return billboardSlot;
        }
    }
}
