﻿using ACTSystem;
using GameData;
using GameLoader.Utils;
using LuaInterface;
using MogoEngine.Events;
using MogoEngine.RPC;
using ShaderUtils;
using System;
using System.Collections.Generic;
using UnityEngine;
using GameMain.CombatSystem;
using GameMain.ClientConfig;

namespace GameMain
{
    public class EntityLuaBaseUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class EntityLuaBase : Entity
    {
        protected LuaTable _luaEntity;
        public LuaTable luaEntity { get { return _luaEntity; } }
        protected Dictionary<string, LuaFunction> _setters = new Dictionary<string, LuaFunction>();

        protected void CallLuaSelfFunction(string funcName)
        {
            var func = _luaEntity.GetLuaFunction(funcName);
            if (func != null)
            {
                func.BeginPCall();
                func.Push(_luaEntity);
                func.PCall();
                func.EndPCall();
            }
            else
            {
                LoggerHelper.Error(funcName + " function not found in luaState!");
            }
        }

        protected void CallLuaSelfFunction(string funcName, object arg)
        {
            var func = _luaEntity.GetLuaFunction(funcName);
            if (func != null)
            {
                func.BeginPCall();
                func.Push(_luaEntity);
                func.Push(arg);
                func.PCall();
                func.EndPCall();
            }
            else
            {
                LoggerHelper.Error(funcName + " function not found in luaState!");
            }
        }

        protected void CallLuaSelfFunction(string funcName, object arg1, object arg2, object arg3, object arg4)
        {
            var func = _luaEntity.GetLuaFunction(funcName);
            if (func != null)
            {
                func.BeginPCall();
                func.Push(_luaEntity);
                func.Push(arg1);
                func.Push(arg2);
                func.Push(arg3);
                func.Push(arg4);
                func.PCall();
                func.EndPCall();
            }
            else
            {
                LoggerHelper.Error(funcName + " function not found in luaState!");
            }
        }

        public override void OnInit() 
        {
            object[] result = LuaDriver.instance.CallFunction("CSCALL_CREATE_ENTITY", this.entityType, this);
            _luaEntity = result[0] as LuaTable;
        }

        public override void OnEnterWorld()
        {
            base.OnEnterWorld();
            CallLuaSelfFunction("OnEnterWorld");
        }

        public override void OnLeaveWorld()
        {
            base.OnLeaveWorld();
            CallLuaSelfFunction("__desctor__");
            CallLuaSelfFunction("OnLeaveWorld");
            _luaEntity.Dispose();
            _luaEntity = null;
        }

        public override void OnReuse(uint id) 
        {
            base.OnReuse(id);
            CallLuaSelfFunction("__reuse__", id);
        }

        public override void OnRelease() 
        {
            base.OnRelease();
            CallLuaSelfFunction("__release__");
        }

        public override void OnEnterSpace() 
        {
            base.OnEnterSpace();
            CallLuaSelfFunction("OnEnterSpace");
        }

        public override void OnLeaveSpace() 
        {
            base.OnLeaveSpace();
            CallLuaSelfFunction("OnLeaveSpace");
        }

        public override void OnCellAttached()
        {
            base.OnCellAttached();
            CallLuaSelfFunction("OnCellAttached");
        }

        Type _luaTableType = typeof(GameLoader.Utils.CustomType.LuaTable);
        public override void SetAttr(string attrName, object value)
        {
            LuaTable tableValue = null;
            if (value.GetType() == _luaTableType)
            {
                string luaString = "return " + (value as GameLoader.Utils.CustomType.LuaTable).GetLuaString();
                //Debug.LogError("sourceLuaString:" + (value as GameLoader.Utils.CustomType.LuaTable).sourceLuaString);
                //Debug.LogError("SetAttr:" + attrName + ":" + luaString);
                //LoggerHelper.Debug(string.Format("<color=#00ff00>SetAttr {0} ： {1} </color>", attrName, luaString));
                var objs = _luaEntity.GetLuaState().DoString(luaString);
                if(objs.Length > 0){
                    tableValue = objs[0] as LuaTable;
                    value = tableValue;
                }
            }
            LuaFunction luaFunc = _luaEntity.GetLuaFunction("__setattr__");
            if (luaFunc != null)
            {
                luaFunc.BeginPCall();
                luaFunc.Push(_luaEntity);
                luaFunc.Push(attrName);
                luaFunc.Push(value);
                luaFunc.PCall();
                luaFunc.EndPCall();
            }
            if (tableValue != null) tableValue.Dispose();
        }

        static List<LuaTable> _tableList = new List<LuaTable>();
        public override void SetAttrs(List<EntityPropertyValue> props)
        {
            if (props.Count == 0) return;
            string[] attrnames = new string[props.Count];
            object[] attrvalues = new object[props.Count];
            LuaTable tableValue = null;
            for (int i = 0; i < props.Count; i++)
            {
                var prop = props[i];
                attrnames[i] = prop.Property.Name;
                object value = prop.Value;
                
                if (value.GetType() == _luaTableType)
                {
                    string luaString = "return " + (value as GameLoader.Utils.CustomType.LuaTable).GetLuaString();
                    var objs = _luaEntity.GetLuaState().DoString(luaString);
                    if (objs.Length > 0)
                    {
                        tableValue = objs[0] as LuaTable;
                        value = tableValue;
                        _tableList.Add(tableValue);
                    }
                }
                attrvalues[i] = value;
            }
            LuaFunction luaFunc = _luaEntity.GetLuaFunction("__setattrs__");
            if (luaFunc != null)
            {
                luaFunc.BeginPCall();
                luaFunc.Push(_luaEntity);
                luaFunc.Push(attrnames);
                luaFunc.Push(attrvalues);
                luaFunc.Push(props.Count);
                luaFunc.PCall();
                luaFunc.EndPCall();
            }
            for (int i = 0; i < _tableList.Count; i++)
            {
                tableValue = _tableList[i];
                if (tableValue != null) tableValue.Dispose();
            }
            _tableList.Clear();
        }

        public override void SetAttrsByLuaTable(string luaTableStr)
        {
            //Debug.LogError("SetAttrsByLuaTable:" + luaTableStr);
            LuaTable tableValue = null;
            var objs = _luaEntity.GetLuaState().DoString(luaTableStr);
            if (objs.Length > 0)
            {
                tableValue = objs[0] as LuaTable;
            }
            LuaFunction luaFunc = _luaEntity.GetLuaFunction("__setattrstable__");
            if (luaFunc != null && tableValue != null)
            {
                luaFunc.BeginPCall();
                luaFunc.Push(_luaEntity);
                luaFunc.Push(tableValue);
                luaFunc.PCall();
                luaFunc.EndPCall();
                tableValue.Dispose();
            }
        }

        List<LuaTable> _tableValues = new List<LuaTable>();
        public override void RpcCallResp(string functionName, object[] args)
        {
            if(functionName == null)
            {
                LoggerHelper.Error("RpcCallResp Error: empty function name");
                return;
            }
            if(args == null)
            {
                LoggerHelper.Error("RpcCallResp Error: args in function: " + functionName);
                return;
            }
            object[] nargs = new object[args.Length + 1];
            nargs[0] = _luaEntity;
            args.CopyTo(nargs, 1);
            LuaTable tableValue = null;
            for (int i = 1; i < nargs.Length; i++)
            {
                if (nargs[i].GetType() == _luaTableType)
                {
                    string luaString = "return " + (nargs[i] as GameLoader.Utils.CustomType.LuaTable).GetLuaString();
                    //LoggerHelper.Debug(string.Format("<color=#00ff00>functionName {0} ： {1} </color>", functionName, luaString));
                    var objs = _luaEntity.GetLuaState().DoString(luaString);
                    if (objs.Length > 0)
                    {
                        tableValue = objs[0] as LuaTable;
                        nargs[i] = tableValue;
                        _tableValues.Add(tableValue);
                    }
                }
            }
            LuaFunction luaFunc = _luaEntity.GetLuaFunction(functionName);
            if (luaFunc != null)
            {
                luaFunc.BeginPCall();
                int count = nargs == null ? 0 : nargs.Length;
                if (!luaFunc.GetLuaState().LuaCheckStack(count + 6))
                {
                    luaFunc.EndPCall();
                    throw new LuaException("RpcCallResp stack overflow");
                }
                luaFunc.PushArgs(nargs);
                luaFunc.PCall();
                luaFunc.EndPCall();
            }
            else {
                LoggerHelper.Error(string.Format("RpcCallResp: function {0} is not exist!", functionName));
            }
            if (_tableValues.Count > 0)
            {
                for (int i = 0; i < _tableValues.Count; i++)
                {
                    tableValue = _tableValues[i];
                    if (tableValue != null) tableValue.Dispose();
                }
                _tableValues.Clear();
            }
        }

        public bool IsPlayer()
        {
            return this == EntityPlayer.Player;
        }

        public bool IsAvatar()
        {
            return (entityType == EntityConfig.ENTITY_TYPE_NAME_AVATAR || entityType == EntityConfig.ENTITY_TYPE_NAME_PLAYER_AVATAR);
        }

        public bool IsShowPlayer()
        {
            return this == EntityPlayer.ShowPlayer;
        }

        public void CreateTempCube(Vector3 pos, GameObject parent)
        {
            GameObject g = GameObject.CreatePrimitive(PrimitiveType.Cube);
            g.GetComponent<BoxCollider>().enabled = false;
            g.transform.position = pos;
            g.transform.SetParent(parent.transform);
        }
    }
}
