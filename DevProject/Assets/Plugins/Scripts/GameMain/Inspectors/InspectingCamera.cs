﻿using Common.States;
using GameData;
using GameMain.GlobalManager;
using InspectorSetting;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain
{
    //[System.Serializable]
    public class InspectingCamera : BeInspectedObject
    {
        private CameraManager _target;
        public InspectingCamera(CameraManager target)
        {
            _target = target;
        }

        public override List<InspectingData> GetInspectingData()
        {
            var result = GetInspectingDataList();
            result.Clear();
            UpdateCameraState(result);
            UpdateCameraMotion(result);
            return result;
        }

       //static int i = 1;//为排除警告，注释掉
        private void UpdateCameraState(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "CameraState:";
            var cm = CameraManager.GetInstance();
            List<string> value = new List<string>();
            {
                value.Add(" currentMotionType:" + cm.mainCamera.currentMotionType);  
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        }

        private void UpdateCameraMotion(List<InspectingData> dataList)
        {
            InspectingData data = new InspectingData();
            data.title = "CameraMotion:";
            var cm = CameraManager.GetInstance();
            var cameraMotion = cm.mainCamera.cameraMotion;
            if (cameraMotion == null) return;
            var motionData = cameraMotion.data;
            List<string> value = new List<string>();
            {
                value.Add(" targetDistance:" + motionData.targetDistance);
                value.Add(" targetPosition:" + motionData.targetPosition);
                value.Add(" targetRotation:" + motionData.targetRotation);

                value.Add(" distanceDuration:" + motionData.distanceDuration);
                value.Add(" focusPoint:" + motionData.focusPoint);
                value.Add(" lockTarget:" + motionData.lockTarget);
                value.Add(" positionDuration:" + motionData.positionDuration);
                value.Add(" rotationDuration:" + motionData.rotationDuration);
                if (cameraMotion is TwoTargetMotion)
                {
                    var ttm = cameraMotion as TwoTargetMotion;
                    value.Add(" lockPointSP:" + ttm.lockPointSP);
                    value.Add(" playerViewSP:" + ttm.playerViewSP);
                }
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        }
    }
}
