﻿using Common.States;
using GameData;
using GameMain.GlobalManager;
using InspectorSetting;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain
{
    //[System.Serializable]
    public class InspectingPlayer : BeInspectedObject
    {
        private EntityPlayer _target;
        public InspectingPlayer(EntityPlayer target)
        {
            _target = target;
        }

        public override List<InspectingData> GetInspectingData()
        {
            var result = GetInspectingDataList();
            result.Clear();
            UpdateControlStickState(result);
            return result;
        }

        static int i = 1;
        private void UpdateControlStickState(List<InspectingData> dataList)
        {
            //Debug.LogError("UpdateControlStickState:" + ControlStickState.strength);
            InspectingData data = new InspectingData();
            data.title = "ControlStickState:";
            List<string> value = new List<string>();
            {
                value.Add(" direction:" + ControlStickState.direction + ":" + ControlStickState.GetDirectionZone());
                value.Add(" strength:" + ControlStickState.strength);
                value.Add(" isDragging:" + ControlStickState.isDragging);
                value.Add(" fingerId:" + ControlStickState.fingerId);
                value.Add(" position:" + _target.actor.position);
                value.Add(" battleServerMode:" + EntityPlayer.battleServerMode);
                value.Add(" i:" + i++);
                value.Add(" inSafeArea:" + SafeAreaManager.GetInstance().IsSafeArea(EntityPlayer.Player.GetTransform().position.x, EntityPlayer.Player.GetTransform().position.z));
                value.Add(" inAttackState:" + _target.inAttackState);
                //value.Add(" skill Direction:" + ControlStickState.GetSkillDirectionZone());
            }
            data.type = value.GetType();
            data.value = value;
            data.color = Color.cyan;
            dataList.Add(data);
        }
    }
}
