﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    public class ACTSoundController
    {
        public AudioClip[] audioClipList;

        public ACTActor m_actor;

        private ACTAudioSourceManager _audioSourceManager;

        public ACTSoundController(ACTActor actor)
        {
            m_actor = actor;
            //目前声道暂定为5个
            _audioSourceManager = new ACTAudioSourceManager(actor,5);


        }

        public void PlaySound(string sname)
        { 
            for(int i = 0;i <audioClipList.Length;i++)
            {
                if (audioClipList[i].name == sname)
                {
                    var aslist = m_actor.gameObject.GetComponents<AudioSource>();
                    AudioSource canas =null;
                    foreach(var audios in aslist)
                    {
                        if (!audios.isPlaying)
                        {
                            canas = audios;
                            break;
                        }
                    }
                    if (canas == null)
                    {
                        canas = m_actor.gameObject.AddComponent<AudioSource>();
                    }
                    canas.clip = audioClipList[i];
                    canas.volume = 1;
                    canas.loop = false;
                    canas.Play();
                    break;
                }
            }
        }

        public void PlayClip(AudioClip clip, float volume)
        {
            AudioSource canas = GetSpareAudioSource();
            
            canas.clip = clip;
            canas.volume = volume;
            canas.loop = false;
            canas.Play();
        }

        public void PlayHitSoundFX(ACTEventSoundFX data)
        {
            if (data.HitSoundList.Count <= 0) return;

            //现在改成按照概率播放音效，倒地播放哪一个音效，随机选择
            var cr = UnityEngine.Random.Range(0f, 1f);
            cr = cr - data.PlayRate;
            if (cr >= 0)
            {
                return;
            }
            //根据列表随机音效播放
            var list = data.HitSoundList;
            //ACTSystemLog.LogError("[ACTSoundController:PlayHitSoundFX]=>list.Count: " + list.Count + ",data.PlayRate:   " + data.PlayRate);
            if (list.Count >0)
            {
                int rndNum = UnityEngine.Random.Range(0, 1000) % list.Count;
                //Debug.LogError("[ACTSoundController:PlayHitSoundFX]=>data.HitSoundList[" + rndNum + "].Clip_Name: " + data.HitSoundList[rndNum].Clip_Name + ",data.PlayRate:   " + data.PlayRate);
                Play(data.HitSoundList[rndNum].Clip_Name, data.HitSoundList[rndNum].Volume, false,
                    data.actsoundType == ACTSoundType.AttackSound ? ACTAudioSourceType.AttackSound : ACTAudioSourceType.AttackVoice);
            }

        }

        /// <summary>
        /// 播放受击音效
        /// </summary>
        /// <param name="data"></param>
        public void PlayBeHitSoundFX(ACTEventBeHitSoundFX data)
        {
            if (data.BeHitSoundList.Count <= 0) return;

            var cr = UnityEngine.Random.Range(0f, 1f);
            cr = cr - data.PlayRate;
            if (cr >= 0)
            {
                return;
            }

            var list = data.BeHitSoundList;
            if (list.Count > 0)
            {
                int rndNum = UnityEngine.Random.Range(0, 1000) % list.Count;
                //ACTSystemLog.LogError("[ACTSoundController:PlayHitSoundFX]=>data.HitSoundList[" + rndNum+"].Clip_Name: " + data.HitSoundList[rndNum].Clip_Name + ",data.PlayRate:   " + data.PlayRate);
                Play(data.BeHitSoundList[rndNum].Clip_Name, data.BeHitSoundList[rndNum].Volume, false, ACTAudioSourceType.BehitSound);
            }
        }
        /// <summary>
        /// 播放自身受击音效
        /// </summary>
        public void PlaySelfBeHitSound()
        {
            PlaySelfSound(ACTSelfSoundType.BehitSelfSound);
        }
        /// <summary>
        /// 播放出生音效
        /// </summary>
        public void PlayBornSound()
        {
            PlaySelfSound(ACTSelfSoundType.BornSound);
        }
        /// <summary>
        /// 播放死亡音效
        /// </summary>
        public void PlayDeadSound()
        {
            PlaySelfSound(ACTSelfSoundType.DeadSound);
        }
        /// <summary>
        /// 播放脚步音效
        /// </summary>
        public void PlayFootSound(ACTFootSoundType footSoundType)
        {
            ACTDataActor actDataActor = m_actor.actorData;
            if (actDataActor.FootSoundList.Count <= 0) return;
            int random = UnityEngine.Random.Range(0, 100);
            if (random > actDataActor.FootPlayRate) return;
            var list = actDataActor.FootSoundList;
            switch (footSoundType)
            {
                case ACTFootSoundType.Default:
                    if (actDataActor.FootSoundList.Count > 0)
                    {
                        Play(actDataActor.FootSoundList[0].Clip_Name, actDataActor.FootSoundList[0].Volume, false, ACTAudioSourceType.FootSound);
                    }
                    break;
                case ACTFootSoundType.Water:
                    if (actDataActor.FootSoundList.Count > 1)
                    {
                        Play(actDataActor.FootSoundList[1].Clip_Name, actDataActor.FootSoundList[1].Volume, false, ACTAudioSourceType.FootSound);
                    }
                    break;
                case ACTFootSoundType.Sand:
                    if (actDataActor.FootSoundList.Count > 2)
                    {
                        Play(actDataActor.FootSoundList[2].Clip_Name, actDataActor.FootSoundList[2].Volume, false, ACTAudioSourceType.FootSound);
                    }
                    break;
                default:
                break;
            }                 
        }

        public void PlaySpeakSound(string strPath)
        {
            //Debug.LogError("PlaySpeakSound:"+strPath);
            if (!String.IsNullOrEmpty(strPath))
                Play(strPath,0.5f,false,ACTAudioSourceType.SpeakSound);
        }

        private void PlaySelfSound(ACTSelfSoundType beHitSoundType)
        {
            ACTDataActor actDataActor = m_actor.actorData;
            switch (beHitSoundType)
            {
                case ACTSelfSoundType.BornSound:
                    if (actDataActor.bornSound != null)
                    {
                        Play(actDataActor.bornSound.Clip_Name, actDataActor.bornSound.Volume, false, ACTAudioSourceType.BehitSelfSound);
                    }
                    break;
                case ACTSelfSoundType.DeadSound:
                    if (actDataActor.deadSound != null)
                    {
                        Play(actDataActor.deadSound.Clip_Name, actDataActor.deadSound.Volume, false, ACTAudioSourceType.BehitSelfSound);
                    }
                    break;
                case ACTSelfSoundType.BehitSelfSound:
                    if (actDataActor.BeHitSoundList.Count <= 0) return;
                    int random = UnityEngine.Random.Range(0, 100);
                    if(random>actDataActor.PlayRate) return;
                    var list = actDataActor.BeHitSoundList;
                    if (actDataActor.PlaySoundType == ACTPlaySoundType.Random)
                    {
                        int rndNum = UnityEngine.Random.Range(0, 1000) % list.Count;
                        Play(actDataActor.BeHitSoundList[rndNum].Clip_Name, actDataActor.BeHitSoundList[rndNum].Volume, false, ACTAudioSourceType.BehitSelfSound);
                    }
                    else
                    {
                        int rndNum = m_actor.curBehitSelfCount;
                        Play(actDataActor.BeHitSoundList[rndNum].Clip_Name, actDataActor.BeHitSoundList[rndNum].Volume, false, ACTAudioSourceType.BehitSelfSound);
                        m_actor.curBehitSelfCount++;
                        m_actor.curBehitSelfCount = m_actor.curBehitSelfCount % list.Count;
                    }
                    break;
                default:
                    break;
            }
        }

        public AudioSource GetSpareAudioSource(ACTAudioSourceType _audioSourceType = ACTAudioSourceType.AttackSound)
        {
            AudioSource canas = _audioSourceManager.GetAudioSource(_audioSourceType);
            return canas;
        }

        public void Play(string strPath, float volume, bool loop = false,ACTAudioSourceType _audioSourceType = ACTAudioSourceType.AttackSound)
        {
            if (string.IsNullOrEmpty(strPath)) return;
            float settingSoundVolume = ACTSystem.ACTSystemDriver.GetInstance().soundVolume;
            if (Mathf.Approximately(settingSoundVolume, 0))
            {
                return;
            }
            AudioSource canas = GetSpareAudioSource(_audioSourceType);
            ACTSystemDriver.GetInstance().SoundPlayer.Play(canas, strPath, ACTSystem.ACTSystemDriver.GetInstance().soundVolume, loop);
        }

        #region 临时音量控制
        public void TempCloseVolume()
        {
            if (_audioSourceManager == null) return;
            _audioSourceManager.TempCloseVolume();
        }

        public void ResetTempVolume()
        {
            if (_audioSourceManager == null) return;
            _audioSourceManager.ResetTempVolume();
        }
        #endregion

    }
}                                                                             
