﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    public class ACTActionController : MonoBehaviour
    {
        private Animator _animator;
        void Awake()
        {
            _animator = gameObject.GetComponent<Animator>();
        }

        private Queue<int> m_actionQueue = new Queue<int>();
        public void PlayAction(int action)
        {
            m_actionQueue.Enqueue(action);
        }

        public void PlayActionByName(string name)
        {
            var animator = gameObject.GetComponent<Animator>();
            if (animator != null)
            {
                animator.Play(name);
            }
        }

        public void PlayAnimationFX(ACTEventAnimationFX data)
        {
            PlayAction(data.ActionID);
        }


        void LateUpdate()
        {
            if (_animator != null)
            {
                if (_animator.IsInTransition(0))
                {
                    return;
                }
                if (m_actionQueue.Count > 0)
                {
                    var action = m_actionQueue.Dequeue();
                    _animator.SetInteger("Action", action);
                }
                else
                {
                    _animator.SetInteger("Action", 0);
                }

            }
        }
    }
}
