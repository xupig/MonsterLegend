﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using ShaderUtils;

namespace ACTSystem
{
    public class ACTEquipController
    {
        public bool isRendering = false;
        private bool _isPackUp = false;
        private bool _isFirst;
        public ACTActor _actor;
        private ACTEquipCloth _equipCloth;
        public ACTEquipCloth equipCloth
        {
            get{return _equipCloth;}
        }

        private ACTEquipHead _equipHead;

        public ACTEquipHead equipHead
        {
            get { return _equipHead; }
        }

        private ACTEquipHair _equipHair;

        public ACTEquipHair equipHair
        {
            get { return _equipHair; }
        }
        private ACTEquipWeapon _equipWeapon;
        public ACTEquipWeapon equipWeapon
        {
            get{return _equipWeapon;}
        }

        private ACTEquipWeapon _equipDeputyWeapon;
        public ACTEquipWeapon equipDeputyWeapon
        {
            get { return _equipDeputyWeapon; }
        }

        private ACTEquipWing _equipWing;
        public ACTEquipWing equipWing
        {
            get{return _equipWing;}
        }

        private ACTEquipManteau _equipManteau;
        public ACTEquipManteau equipManteau
        {
            get { return _equipManteau; }
        }

        private ACTEquipEffect _equipEffect;
        public ACTEquipEffect equipEffect
        {
            get { return _equipEffect; }
        }

        public ACTEquipController(ACTActor actor)
        {
            _isFirst = true;
            _actor = actor;
            _equipCloth = new ACTEquipCloth(actor);
            _equipHead=new ACTEquipHead(actor);
            //镇魂街没有一下装备
            _equipHair = new ACTEquipHair(actor);
            _equipWeapon = new ACTEquipWeapon(actor);
            _equipDeputyWeapon = new ACTEquipWeapon(actor);
            _equipWing = new ACTEquipWing(actor);
            _equipManteau = new ACTEquipManteau(actor);
            _equipEffect = new ACTEquipEffect(actor);
            InitRenderEventListener();
        }

        void InitRenderEventListener()
        {
            SkinnedMeshRenderer[] smrList = GetSkinnedMeshRenderers();
            for (int i = 0; i < smrList.Length; ++i)
            {
                SkinnedMeshRenderer smr = smrList[i];
                smr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                smr.receiveShadows = false;
                smr.motionVectors = false;
                ACTEventListener.Get(smr.gameObject).onBecameVisible = OnBecameVisible;
                ACTEventListener.Get(smr.gameObject).onBecameInvisible = OnBecameInvisible;
            }
        }

        public void OnActorDestroy()
        {
            _equipCloth.OnActorDestroy();
            _equipHead.OnActorDestroy();
            _equipHair.OnActorDestroy();
            _equipWeapon.OnActorDestroy();
            _equipDeputyWeapon.OnActorDestroy();
            _equipWing.OnActorDestroy();
            _equipManteau.OnActorDestroy();
            _equipEffect.OnActorDestroy();
        }

        public void OnBecameVisible(GameObject gameObject)
        {
            //Debug.LogError("OnBecameVisible " + gameObject.name);
            isRendering = true;
        }

        public void OnBecameInvisible(GameObject gameObject)
        {
            //Debug.LogError("OnBecameInvisible " + gameObject.name);
            isRendering = false;
        }
        //编辑器使用
        public void AddEquip(int equipID, int particleID = 0, int flowID = 0)
        {
            var data = ACTRunTimeData.GetEquipmentData(equipID);
            switch (data.EquipType)
            {
                case ACTEquipmentType.Cloth:
                    _equipCloth.PutOn(equipID, particleID, flowID);
                    break;
                case ACTEquipmentType.Weapon:
                    _equipWeapon.InCity(false);
                    _equipWeapon.PutOn(equipID, particleID, flowID);
                    break;
                case ACTEquipmentType.Wing:
                    _equipWing.PutOn(equipID, particleID, flowID);
                    break;
                case ACTEquipmentType.Head:
                    _equipHead.PutOn(equipID, particleID, flowID);
                    break;
                case ACTEquipmentType.Manteau:
                    _equipManteau.PutOn(equipID, particleID, flowID);
                    break;
                case ACTEquipmentType.Effect:
                    _equipEffect.PutOn(equipID, particleID, flowID);
                    break;
                default:
                    break;
            }
        }

        public void RemoveEquip(int equipID)
        {
            var data = ACTRunTimeData.GetEquipmentData(equipID);
            switch (data.EquipType)
            {
                case ACTEquipmentType.Cloth:
                    _equipCloth.TakeOff(equipID);
                    break;
                case ACTEquipmentType.Weapon:
                    _equipWeapon.TakeOff(equipID);
                    break;
                case ACTEquipmentType.Wing:
                    _equipWing.TakeOff(equipID);
                    break;
                case ACTEquipmentType.Head:
                    _equipHead.TakeOff(equipID);
                    break;
                case ACTEquipmentType.Manteau:
                    _equipManteau.TakeOff(equipID);
                    break;
                case ACTEquipmentType.Effect:
                    _equipEffect.PutOn(equipID);
                    break;
                default:
                    break;
            }   
        }

        //*****************************************效果*****************************************************
        static float DEFAULT_HIGH_LIGHT_VALUE = 1;
        static float DEFAULT_HIGH_LIGHT_DURATION = 0.2f;
        float highLightValue = DEFAULT_HIGH_LIGHT_VALUE;
        float highLightEndTime = 0;
        public void Update()
        {
            CheckHighLight();
        }

        public void CheckHighLight()
        {
            if (highLightValue == DEFAULT_HIGH_LIGHT_VALUE) return;
            if (highLightEndTime <= UnityPropUtils.realtimeSinceStartup)
            {
                HighLight(DEFAULT_HIGH_LIGHT_DURATION, DEFAULT_HIGH_LIGHT_VALUE);
            }
        }

        SkinnedMeshRenderer[] _smrList = null;
        public SkinnedMeshRenderer[] GetSkinnedMeshRenderers()
        { 
            if (_smrList == null)
            {
                 _smrList = _actor.gameObject.GetComponentsInChildren<SkinnedMeshRenderer>(false);
            }
            return _smrList;
        }

        //闪白效果
        public void HighLight(float duration = 0.1f, float value = 3f)
        {
            if (_actor.gameObject == null) return;
            SkinnedMeshRenderer[] smrList = GetSkinnedMeshRenderers();
            if (smrList == null)return;

            highLightValue = value;
            for (int i = 0; i < smrList.Length; ++i)
            {
                for (int j = 0; j < smrList[i].materials.Length; j++)
                {
                    smrList[i].materials[j].SetFloat("_HighLight", value);
                }
            }
            highLightEndTime = UnityPropUtils.realtimeSinceStartup + duration;
        }

        public void SkinVisible(bool visible)
        {
            if (_actor.gameObject == null) return;
            SkinnedMeshRenderer[] smrList = GetSkinnedMeshRenderers();
            if (smrList == null) return;
            for (int i = 0; i < smrList.Length; ++i)
            {
                smrList[i].enabled = visible;
            }
        }

        public void PackUpWeapon(bool isPackUp)
        {
            if (_actor.actorState.OnRiding) return;
            if (_isFirst)
            {
                _isFirst = false;
                this._isPackUp = isPackUp;
                _equipWeapon.InCity(isPackUp);
                _equipDeputyWeapon.InCity(isPackUp);
            }
            if (this._isPackUp == isPackUp)
            {
                return;
            }
            this._isPackUp = isPackUp;
            _equipWeapon.InCity(isPackUp);
            _equipDeputyWeapon.InCity(isPackUp);
        }

        public void GhostShadow(float duration, float interval, float fadeout, float distance, Color xrayColor, float rim = 1.2f, float inside = 0f, int uid = 0)
        {
            if (_actor.gameObject == null) return;
            Transform body = _actor.boneController.GetHumanBody();
            if (uid == 0)
            {
                var animState = _actor.animationController.GetCurrentState();
                uid = animState.shortNameHash;
            }
            if (body)
            {
                var ghost = body.gameObject.GetComponent<ACTGhostShadow>();
                if (ghost == null)
                {
                    ghost = body.gameObject.AddComponent<ACTGhostShadow>();
                }
                ghost._xrayColor = xrayColor;
                ghost._rim = rim;
                ghost._Inside = inside;
                ghost.Play(duration, interval, fadeout, distance, uid);
            }
            Transform head = _actor.boneController.GetHumanHead();
            if (head)
            {
                var ghost = head.gameObject.GetComponent<ACTGhostShadow>();
                if (ghost == null)
                {
                    ghost = head.gameObject.AddComponent<ACTGhostShadow>();
                }
                ghost._xrayColor = xrayColor;
                ghost._rim = rim;
                ghost._Inside = inside;
                ghost.Play(duration, interval, fadeout, distance, uid);
            }
        }

        #region 翅膀隐藏显示
        private bool _canWingShow = true;
        public bool canWingShow
        {
            get { return _canWingShow; }
        }

        public void SetWingShow(bool isShow)
        {
            _canWingShow = isShow;
            if (equipWing == null) return;
            equipWing.ShowWingGameObject(isShow);
        }
        #endregion

        #region 装备特效显示开关
        private bool _canEquipFxShow = true;
        public bool canEquipFxShow
        {
            get { return _canEquipFxShow; }
            set { _canEquipFxShow = value; }
        }
        #endregion

        #region 装备流光显示开关
        private bool _canEquipFlowShow = true;
        public bool canEquipFlowShow
        {
            get { return _canEquipFlowShow; }
        }

        public void SetEquipFlowShow(bool isShow)
        {
            _canEquipFlowShow = isShow;
            if (equipCloth != null && equipCloth.flowFlashEffect != null)
            {
                equipCloth.flowFlashEffect.EnableEffect = isShow && equipCloth.isFlowShow;
            }
            if (equipWeapon != null && equipWeapon.flowFlashEffect != null)
            {
                equipWeapon.flowFlashEffect.enabled = isShow && equipWeapon.isFlowShow;
            }
        }
        #endregion
    }
}
