using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace ACTSystem
{
    public class ACTActorController 
    {
        static List<int> _speedRateList = new List<int>() { 0, 2, 5, 10 };

        public float gravity = 20.0f;
        public bool isStatic = false;
        public bool alwaysCheckGround = true;
        public bool usePlayerController = false;
        public CollisionFlags collisionFlags;
        public bool crashedWall
        {
            get {
                return (collisionFlags & CollisionFlags.Sides) != 0;
            }
        }

        public float verticalSpeed = 0.0f;
        Vector3 _horizontalSpeed = new Vector3();
        public Vector3 horizontalSpeed
        {
            get { return _horizontalSpeed; }
        }

        private Transform _actorTransform;

        private CharacterController _controller;
        public CharacterController controller
        {
            get { return _controller; }
        }

        private float _groundHigh = 0;
        public float groundHeight
        {
            get { return _groundHigh; }
        }

        private Vector3 _movement;
        public Vector3 movement
        {
            get { return _movement; }
        }

        private ACTActor _actor;

        public float _slowRate;

        private float _keepFloatRemainTime;

        //下沉相关
        private bool _isSinking = false;
        public bool isSinking
        {
            get { return _isSinking; }
        }

        private float _sinkSpeed = 0.3f;
        public float sinkSpeed
        {
            get { return _sinkSpeed; }
        }

        private float _sinkDistance = 0f;
        private float _airHeight = 0.5f; //设定大于这个高度为空中
        public bool needFixHeight = false;

        public ACTActorController(ACTActor actor)
        {
            _actor = actor;
            _actorTransform = _actor.transform;
            _controller = _actor.GetComponent<CharacterController>();
            if (_controller == null)
            {
                _controller = _actor.gameObject.AddComponent<CharacterController>();
            }
        }

        public void Reset()
        {
            _keepFloatRemainTime = 0;
            verticalSpeed = 0;
            alwaysCheckGround = false;
            _slowRate = 0;
            isStatic = false;
            _isSinking = false;
            _sinkDistance = 0;
            _horizontalSpeed = Vector3.zero;
            _updateFaceEnabled = true;
        }

        static Ray _downRay = new Ray(Vector3.zero, Vector3.down);
        static RaycastHit _raycastHit = new RaycastHit();
        static Vector3 _hitPoint = Vector3.zero;
        static int _terrainLayerValue = 1 << ACTLayerUtils.Terrain;
        Vector3 _lastCalculatePosition = Vector3.zero;
        bool CheckOnGround()
        {
            bool result = false;
            var actorPos = _actorTransform.position;
            if (_lastCalculatePosition != actorPos || alwaysCheckGround)
            {
                _lastCalculatePosition = actorPos;
                actorPos.y = 2000;
                _downRay.origin = actorPos;
                _raycastHit.point = Vector3.zero;
                Physics.Raycast(_downRay, out _raycastHit, 2000f, _terrainLayerValue);
                _hitPoint.x = actorPos.x;
                _hitPoint.y = _raycastHit.point.y;
                _hitPoint.z = actorPos.z;
                _groundHigh = _hitPoint.y;
                if (UnityPropUtils.IsEditor)
                {
                    Debug.DrawLine(actorPos, _hitPoint, Color.yellow);
                }
            }
            
            var dis = _actorTransform.position.y - _hitPoint.y;
            if (dis < 0 || (dis <= 0.1 && verticalSpeed <= 0)) result = true;
            _actor.actorState.OnAir = dis > _airHeight;
            _actor.actorState.OnGround = result;
            return result;
        }

        public void Update()
        {
            if (isStatic) return;
            if (_must)
            {
                UpdateLockPosition();
                return;
            }
            var deltaTime = UnityPropUtils.deltaTime;
            if (_freeFly)
            {
                UpdateFreeFly(deltaTime);
                return;
            }
            CheckOnGround();
            UpdateVerticalSpeed(deltaTime);
            Moving(deltaTime);
            UpdateHorizontalSpeed(deltaTime);
            //UpdateBone();//为实现上下半身独立运动，现在没用
            UpdateFace();
            CheckSinking(deltaTime);
        }

        Vector3 _tempPosition = new Vector3();
        private void UpdateVerticalSpeed(float deltaTime)
        {
            if (!_actor.isFly && (_actor.isFlyingOnVehicle || _actor.isRiding))
            {
                //关闭掉之前的重力影响
                _movement.y = 0;
                return;
            }

            if (_actor.actorState.OnGround && verticalSpeed < 0) { verticalSpeed = 0; }
            _keepFloatRemainTime -= deltaTime;
            if (_actorTransform.position.y > _groundHigh)
            {
                verticalSpeed -= gravity * deltaTime;
                if (!_actor.isFly && _keepFloatRemainTime > 0 && verticalSpeed < 0)
                {
                    verticalSpeed = 0;
                }
            }
            else if (_actorTransform.position.y < _groundHigh)
            {
                if (verticalSpeed <= 0 || _groundHigh - _actorTransform.position.y > 0.5)
                {
                    _tempPosition.x = _actorTransform.position.x;
                    _tempPosition.y = _groundHigh;
                    _tempPosition.z = _actorTransform.position.z;
                    _actorTransform.position = _tempPosition;
                    if (!_actor.isFly)
                    {
                        verticalSpeed = 0;
                    }
                }
            }

            _movement.y = verticalSpeed;
            _actor.actorState.OnHitAir = verticalSpeed * 0.2f;
        }

        public bool IsMoving()
        {
            //return _movement.magnitude != 0;
            return Mathf.Sqrt(_movement.x * _movement.x + _movement.z * _movement.z) != 0; // 忽略y方向
        }

        static Vector3 _actorOldPos = Vector3.zero;
        private void Moving(float deltaTime)
        {
            if (_actor.isRiding)
            {
                return;
            }
            _movement.x = _horizontalSpeed.x;
            _movement.z = _horizontalSpeed.z;
            _actorOldPos = _actorTransform.position;
            if (_actorOldPos.y <= _groundHigh && _movement.y < 0)
            {
                _movement.y = 0;
            }
            if (usePlayerController && _controller != null && _controller.enabled)// &&) (_movement.x != 0 || _movement.z != 0))
            {
                if (usePlayerController)
                {
                    //Debug.Log("usePlayerController：" + _movement);
                    collisionFlags = _controller.Move(_movement * deltaTime);
                    bool grounded = (collisionFlags & CollisionFlags.CollidedBelow) != 0;
                    if (grounded)
                    {
                        _actor.actorState.OnGround = true;
                        verticalSpeed = 0;
                    }
                }
            }
            else
            {
                _actorTransform.position = _actorOldPos + _movement * deltaTime;
            }
            HorizontalSpeedToAnimationSpeedNew();
            RightSpeedTransition();
            ForwardSpeedTransition();
        }

        private void HorizontalSpeedToAnimationSpeedNew()
        {
            Vector3 project = new Vector3(0, 0, 0);
            project = Vector3.Project(_horizontalSpeed, _actor.transform.forward);
            var dot = Vector3.Dot(_horizontalSpeed.normalized, _actor.transform.forward);
            if (ACTMathUtils.Abs(dot) < 0.1)
            {
                dot = 0;
            }

            var sign = Mathf.Sign(dot);
            var curValue = project.magnitude * sign;
            var absValue = ACTMathUtils.Abs(curValue);
            for (int i = 0; i < _speedRateList.Count; i++)
            {
                if (absValue <= _speedRateList[i] + 0.1f)
                {
                    curValue = _speedRateList[i] * sign;
                    break;
                }
                //限定在最大设置范围内
                if (i == _speedRateList.Count - 1)
                {
                    curValue = _speedRateList[i] * sign;
                }
            }
            _actor.actorState.forwardMovingSpeedState = curValue;

            project = Vector3.Project(_horizontalSpeed, _actor.transform.right);
            dot = Vector3.Dot(_horizontalSpeed.normalized, _actor.transform.right);
            if (ACTMathUtils.Abs(dot) < 0.1)
            {
                dot = 0;
            }

            sign = Mathf.Sign(dot);
            curValue = project.magnitude * sign;
            absValue = ACTMathUtils.Abs(curValue);
            for (int i = 0; i < _speedRateList.Count; i++)
            {
                if (absValue <= _speedRateList[i] + 0.1f)
                {
                    curValue = _speedRateList[i] * sign;
                    break;
                }
                //限定在最大设置范围内
                if (i == _speedRateList.Count - 1)
                {
                    curValue = _speedRateList[i] * sign;
                }
            }
            _actor.actorState.rightMovingSpeedState = curValue;
        }

        private void RightSpeedTransition()
        {
            var targetValue = _actor.actorState.rightMovingSpeedState;
            var curValue = _actor.actorState.rightMovingSpeedTransition;
            if (Mathf.Sign(targetValue) != Mathf.Sign(curValue))
            {
                _actor.actorState.rightMovingSpeedTransition = targetValue;
                return;
            }
            float t = curValue > targetValue ? 0.3f : 0.4f;
            if (ACTMathUtils.Abs(targetValue - curValue) < t)
            {
                _actor.actorState.rightMovingSpeedTransition = targetValue;
            }
            else
            {
                _actor.actorState.rightMovingSpeedTransition = Mathf.Lerp(curValue, targetValue, t);
            }
            if (_actor.rideOwner != null)
            {
                _actor.rideOwner.actorState.rightMovingSpeedTransition = _actor.actorState.rightMovingSpeedTransition;
            }
        }

        private void ForwardSpeedTransition()
        {
            var targetValue = _actor.actorState.forwardMovingSpeedState;
            var curValue = _actor.actorState.forwardMovingSpeedTransition;
            if (Mathf.Sign(targetValue) != Mathf.Sign(curValue))
            {
                _actor.actorState.forwardMovingSpeedTransition = targetValue;
                return;
            }
            float t = curValue > targetValue ? 0.3f : 0.4f;
            if (ACTMathUtils.Abs(targetValue - curValue) < t)
            {
                _actor.actorState.forwardMovingSpeedTransition = targetValue;
            }
            else
            {
                _actor.actorState.forwardMovingSpeedTransition = Mathf.Lerp(curValue, targetValue, t);
            }
            if (_actor.rideOwner != null)
            {
                _actor.rideOwner.actorState.forwardMovingSpeedTransition = _actor.actorState.forwardMovingSpeedTransition;
            }
        }

        private float SlowSpeed(float speed, float slowRate, float min)
        {
            if (ACTMathUtils.Abs(speed) < min)
            {
                speed = 0;
            }
            else
            {
                speed = Mathf.Sign(speed) * Mathf.Max(ACTMathUtils.Abs(speed) - slowRate, 0);
            }
            return speed;
        }

        private void UpdateHorizontalSpeed(float deltaTime)
        {
            if (_slowRate == 0) return;
            _horizontalSpeed.x = SlowSpeed(_horizontalSpeed.x, _slowRate * deltaTime, 0.5f);
            _horizontalSpeed.z = SlowSpeed(_horizontalSpeed.z, _slowRate * deltaTime, 0.5f);
        }

        private void CheckSinking(float deltaTime)
        {
            if (_isSinking && _actor.actorState.OnGround)
            {
                _sinkDistance += _sinkSpeed * deltaTime;
                var curPos = _actorTransform.position;
                curPos.y = curPos.y - _sinkDistance;
                _actorTransform.position = curPos;
            }
        }

        private bool m_hasGetBone;
        private Transform m_bone;
        private bool m_containsBone;

        private void UpdateBone()
        {
            if(!m_hasGetBone)
            {
                var bone = _actor.boneController.GetBoneByName("Bip01 Pelvis");
                m_containsBone = bone != null;
                m_bone = bone;
                m_hasGetBone = true;
            }
            if (!m_containsBone) return;
            if (_horizontalSpeed.magnitude == 0) return;
            var forward = _actorTransform.forward;
            var moveDir = _horizontalSpeed.normalized;
            var angle = Vector3.Angle(forward, moveDir);
            //if (angle != 0 && ACTMathUtils.Abs(angle) < 100)
            //{
            //    var percent = Mathf.Min(60, ACTMathUtils.Abs(angle)) / ACTMathUtils.Abs(angle);
            //    var dir = Vector3.Slerp(forward, moveDir, percent);
            //    bone.rotation = Quaternion.FromToRotation(forward, dir) * bone.rotation;
            //}
            if (angle != 0 && angle != 180)
            {
                if (ACTMathUtils.Abs(angle) > 90)
                {
                    forward = -forward;
                    angle = 180 - ACTMathUtils.Abs(angle);
                }
                var percent = Mathf.Min(60, ACTMathUtils.Abs(angle)) / ACTMathUtils.Abs(angle);
                var dir = Vector3.Slerp(forward, moveDir, percent);
                m_bone.rotation = Quaternion.FromToRotation(forward, dir) * m_bone.rotation;
            }
        }

        public void UpdateFace()
        {
            if (!_updateFaceEnabled) return;
            Transform trans = _actor.GetTransform();
            Quaternion rotation = trans.rotation;
            float x = rotation.x;
            if (x == 0) return;
            rotation.x = 0;
            rotation.z = 0;
            trans.rotation = Quaternion.Slerp(trans.rotation, rotation, 0.2f);
        }

        public void Jump(float vSpeed, float keepFloatTime = 0)
        {
            verticalSpeed = vSpeed;
            _actor.actorState.OnGround = false;
            _keepFloatRemainTime = keepFloatTime;
        }

        public void Sink()
        {
            _isSinking = true;
        }

        public void SetSlowRate(float slowRate)
        {
            _slowRate = slowRate;
        }

        private float deltaH = 0;
        public void ToGround()
        {
            _lastCalculatePosition = Vector3.zero;
            CheckOnGround();
            deltaH = 0;
            if (_controller != null && needFixHeight)
            {
                deltaH = _controller.height * 0.5f - _controller.center.y;
            }
            _actorTransform.position = new Vector3(_actorTransform.position.x, _groundHigh + deltaH, _actorTransform.position.z);
            verticalSpeed = 0;
        }

        public void Move(Vector3 movement, bool right = false)
        {
            _actor.actorState.RightMove = right;
            _horizontalSpeed.x = movement.x;
            _horizontalSpeed.z = movement.z;
        }

        private bool _must = false;
        private Transform _mustTarget = null;
        public void SetLockToFly(Transform target, bool must, float range, float speed, float trans)
        {
            div = range;
            speeddiv = speed;
            _mustTarget = target;
            _must = must;
            speed_tran = trans;
            if (!must)
            {
                curdiv = 0;
                curTran = 0.5f;
            }
        }

        private float div = 0;
        private float curdiv = 0;
        private float speeddiv = 3;
        private float curTran = 0.5f;
        private float speed_tran = 0.008f;
        private Transform body = null;
        private Transform camer = null;
        private void UpdateLockPosition()
        {
            if (body == null)
            {
                body = _mustTarget.parent;//.FindChild("fly_body");
            }
            curdiv = curdiv + speeddiv * _horizontalSpeed.x;
            float delta = 0;
            if (_horizontalSpeed.x != 0)
            {
                delta = speed_tran * _horizontalSpeed.x;
            }
            else
            {
                if (curTran < 0.5f)
                {
                    delta = speed_tran;
                    if (0.5f - curTran < speed_tran)
                    {
                        delta = 0.5f - curTran;
                    }
                }
                if (curTran > 0.5f)
                {
                    delta = -speed_tran;
                    if (curTran - 0.5f < speed_tran)
                    {
                        delta = 0.5f - curTran;
                    }
                }
            }
            curTran = curTran + delta;
            if (curTran > 1)
            {
                curTran = 1;
            }
            if (curTran < 0)
            {
                curTran = 0;
            }
            
            //_actor.actorState.MovingSpeedTransition = curTran;
            if (body != null)
            {
                float x = body.localEulerAngles.x;
                if ( x > 180)
                {
                    x = x - 360;
                }
                float t = x * 0.2f;
                _actor.actorState.MovingSpeedTransition = t;
            }
            else
            {
                _actor.actorState.MovingSpeedTransition = 0;
            }
            if (curdiv > div)
            {
                curdiv = div;
            }
            if (curdiv < -div)
            {
                curdiv = -div;
            }
            Matrix4x4 ltow = _mustTarget.localToWorldMatrix;
            Vector3 p = ltow.MultiplyPoint(new Vector3(curdiv, -2, 6));
            p = _mustTarget.position;
            _actorTransform.position = p;
            _actorTransform.rotation = _mustTarget.rotation;
        }

        private bool _freeFly = false;
        public void SetFreeFly(bool free)
        {
            _freeFly = free;
        }

        public void UpdateFreeFlyArgs(float rrate, float hspeed, float hdeceleration, float vspeed, float g, float minHeight)
        {
            _hspeed = hspeed;
            _hdeceleration = hdeceleration;
            _vspeed = vspeed;
            _freeFlyG = g;
            _minHeight = minHeight;
            _rrate = rrate;
            _deltaHv = 0;
            _deltaVv = 0;
        }

        private float _hspeed = 0;
        private float _hdeceleration = 0;
        private float _vspeed = 0;
        private float _freeFlyG = 0;
        private float _minHeight = 0;
        private float _rrate = 30;
        private float _deltaHv = 0;
        private float _deltaVv = 0;
        private void UpdateFreeFly(float deltaTime)
        {
            _actor.actorState.MovingSpeedTransition = _horizontalSpeed.x * 0.5f + 0.5f;
            _movement = _actorTransform.forward;
            _deltaHv = _deltaHv + _hdeceleration * deltaTime;
            _deltaVv = _deltaVv - _freeFlyG * deltaTime;
            float hs = _hspeed + _deltaHv;
            _movement = _movement * hs;
            verticalSpeed = _vspeed + _deltaVv;
            _movement.y = verticalSpeed;
            if (_actorTransform.position.y <= _minHeight)
            {
                _movement.y = 0;
                verticalSpeed = 0;
                _deltaVv = 0;
            }
            _actorTransform.position = _actorTransform.position + _movement * deltaTime;
            UpdateFaceFly(deltaTime);
        }

        private void UpdateFaceFly(float deltaTime)
        {
            float rotationspeed = _rrate;
            float r = _horizontalSpeed.x / horizontalSpeed.magnitude;
            float y = rotationspeed * deltaTime * r;
            y = _actorTransform.localEulerAngles.y + y;
            Quaternion q = Quaternion.Euler(new Vector3(0, y, 0));
            _actorTransform.rotation = q;
        }

        public void Stop()
        {
            _horizontalSpeed.x = 0;
            _horizontalSpeed.z = 0;
        }

        public void StopVertical()
        {
            verticalSpeed = 0;
        }

        public float GetRadius()
        {
            return _controller.radius;
        }

        public float GetHeight()
        {
            return _controller.height;
        }

        public void SetRadius(float radius)
        {
            _controller.radius = radius;
        }

        public void SetBoxHeight(float height)
        {
            float oldHeight = height;
            Vector3 center = _controller.center;
            center.y += (height - oldHeight) * 0.5f;
            _controller.center = center;
            _controller.height = height;
        }

        public void SetControllerEnabled(bool enabled)
        {
            _controller.enabled = enabled;
        }

        private bool _updateFaceEnabled = true;
        public void SetUpdateFaceEnabled(bool enabled)
        {
            _updateFaceEnabled = enabled;
        }
    }
}