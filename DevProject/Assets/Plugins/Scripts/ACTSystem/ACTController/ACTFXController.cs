﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ACTSystem
{
    public class ACTFXController
    {
        public ACTActor _actor;
        private ACTEventTriggerVisualFX _onGroundTriggerVisualFX;
        private ACTEventTriggerVisualFX _onAirTriggerVisualFX;

        private List<float> _onGroundTriggerTime = new List<float>();
        private List<float> _onAirTriggerTime = new List<float>();

        public ACTFXController(ACTActor actor)
        {
            _actor = actor;
            _actor.actorState.OnGroundCB = OnGround;
            _actor.actorState.OnAirCB = OnAir;
        }

        public void Update()
        {
            CheckOnGround();
            CheckOnAir();
        }

        private void CheckOnGround()
        {
            for (int i = 0; i < _onGroundTriggerTime.Count; i++)
            {
                float endTime = _onGroundTriggerTime[i];
                if (endTime > 0 && UnityPropUtils.realtimeSinceStartup > endTime)
                {
                    TriggerOnGround();
                    _onGroundTriggerTime[i] = -1;
                }
            }           
        }

        private void CheckOnAir()
        {
            for (int i = 0; i < _onAirTriggerTime.Count; i++)
            {
                float endTime = _onAirTriggerTime[i];
                if (endTime > 0 && UnityPropUtils.realtimeSinceStartup > endTime)
                {
                    TriggerOnAir();
                    _onAirTriggerTime[i] = -1;
                }
            }
        }

        public void SetTriggerVisualFX(ACTEventTriggerVisualFX triggerVisualFX)
        {
            switch (triggerVisualFX.TriggerFXType)
            {
                case ACTTriggerFXType.OnGround:
                    _onGroundTriggerVisualFX = triggerVisualFX;
                    break;
                case ACTTriggerFXType.OnAir:
                    _onAirTriggerVisualFX = triggerVisualFX;
                    break;
            }
        }

        public void RemoveTriggerVisualFX(ACTEventTriggerVisualFX triggerVisualFX)
        {
            switch (triggerVisualFX.TriggerFXType)
            {
                case ACTTriggerFXType.OnGround:
                    if (_onGroundTriggerVisualFX == triggerVisualFX)
                        _onGroundTriggerVisualFX = null;
                    break;
                case ACTTriggerFXType.OnAir:
                    if (_onAirTriggerVisualFX == triggerVisualFX)
                        _onAirTriggerVisualFX = null;
                    break;
            }
        }

        private void GetOnGroundTriggerTime()
        {
            bool isFull = false;
            float curTime = UnityPropUtils.realtimeSinceStartup + _onGroundTriggerVisualFX.PlayDelay;
            for (int i = 0; i < _onGroundTriggerTime.Count; i++)
            {
                if (_onGroundTriggerTime[i] < 0)
                {
                    _onGroundTriggerTime[i] = curTime;
                    break;
                }
                isFull = true;
            }
            if (isFull || _onGroundTriggerTime.Count == 0)
            {
                _onGroundTriggerTime.Add(curTime);
            }
        }

        private void GetOnAirTriggerTime()
        {
            bool isFull = false;
            float curTime = UnityPropUtils.realtimeSinceStartup + _onAirTriggerVisualFX.PlayDelay;
            for (int i = 0; i < _onAirTriggerTime.Count; i++)
            {
                if (_onAirTriggerTime[i] < 0)
                {
                    _onAirTriggerTime[i] = curTime;
                    break;
                }
                isFull = true;
            }
            if (isFull)
            {
                _onAirTriggerTime.Add(curTime);
            }
        }

        public void OnGround()
        {
            if (_onGroundTriggerVisualFX != null)
            {             
                if (_onGroundTriggerVisualFX.PlayDelay > 0)
                {
                    GetOnGroundTriggerTime();
                }
                else
                {
                    TriggerOnGround();
                }
            }
        }

        private void TriggerOnGround()
        {
            if (_onGroundTriggerVisualFX != null)
            {
                ACTVisualFXManager.GetInstance().PlayACTEventVisualFX(_onGroundTriggerVisualFX, this._actor, Vector3.zero);
                if (_onGroundTriggerVisualFX.IsLoop == false)
                {
                    _onGroundTriggerVisualFX = null;
                }
            }
        }

        public void OnAir()
        {
            if (_onAirTriggerVisualFX != null)
            {
                if (_onAirTriggerVisualFX.PlayDelay > 0)
                {
                    GetOnAirTriggerTime();
                }
                else
                {
                    TriggerOnAir();
                }
            }
        }

        private void TriggerOnAir()
        {
            if (_onAirTriggerVisualFX != null)
            {
                ACTVisualFXManager.GetInstance().PlayACTEventVisualFX(_onAirTriggerVisualFX, this._actor, Vector3.zero);
                if (_onAirTriggerVisualFX.IsLoop == false)
                {
                    _onAirTriggerVisualFX = null;
                }
            }
        }
    }
}
