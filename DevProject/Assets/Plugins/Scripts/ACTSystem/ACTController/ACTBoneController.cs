﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ACTSystem
{
    public class ACTBoneController
    {
        static public string ROOT_NAME = "bip_master";
        static public string BODY_NAME = "body";
        static public string FACE_NAME = "face";
        static public string HEAD_NAME = "head";
        static public string SLOT_BILLBOARD_NAME = "slot_billboard";
        static public string SLOT_MAGIC_GEM_NAME = "slot_magic_gem";
        static public string SLOT_CAMERA_NAME = "slot_camera";
        static public string SLOT_MIDDLE_NAME = "slot_middle";
        static public string SLOT_BONE_WEAPON_RIGHT = "bone_weapon_right";
        static public string SLOT_BONE_WEAPON_LEFT = "bone_weapon_left";
        static public string SLOT_BONE_WEAPON_RIGHT_BACK = "bone_weapon_RB";
        static public string SLOT_BONE_WEAPON_LEFT_BACK = "bone_weapon_LB";
        static public string SLOT_BONE_CAMERA = "bone_camera";
        static public string SLOT_CAMERA_POS = "camera_pos";
        static public List<string> WEAPON_SLOTS = new List<string>() {
            SLOT_BONE_WEAPON_RIGHT, SLOT_BONE_WEAPON_LEFT, SLOT_BONE_WEAPON_RIGHT_BACK, SLOT_BONE_WEAPON_LEFT_BACK, };
        static public float CameraSlotHight = 2;
        public ACTActor _actor;

        private Dictionary<string, Transform> _bonesCache = new Dictionary<string, Transform>();
        //for bigBoss
        static public string PART_NAME = "BeHitSlot";
        private Dictionary<int, Transform> _beHitSlotDict = new Dictionary<int, Transform>();

        public ACTBoneController(ACTActor actor)
        {
            _actor = actor;
            ResetSlots();
        }

        private void ResetSlots()
        {
            var bone = GetBoneByName(SLOT_BILLBOARD_NAME);
            if (bone != null)
            {
                bone.rotation = Quaternion.identity;
            }
            bone = GetBoneByName(SLOT_CAMERA_NAME);
            if (bone != null)
            {
                bone.rotation = Quaternion.identity;
            }
            bone = GetBoneByName(SLOT_MIDDLE_NAME);
            if (bone != null)
            {
                bone.rotation = Quaternion.identity;
            }
        }

        public void Reset()
        {
            var bone = GetBoneByName(SLOT_BONE_CAMERA);
            if (bone != null)
            {
                bone.localRotation = Quaternion.identity;
            }
        }

        private bool _initedBones = false;
        private void InitBones()
        {
            if (_initedBones) return;
            _initedBones = true;
            Transform rootBone = _actor.transform;
            if (rootBone != null)
            {
                CacheBoneChildren(rootBone);
            }
            InitSlotBillboard(rootBone);
            InitSlotCamera(rootBone);
            InitSlotMiddle(rootBone);
            InitSlotMagicGem(rootBone);
            if (UnityPropUtils.IsEditor) InitBoneCamera();
        }

        Vector3 oldPos = Vector3.zero;
        private void InitSlotBillboard(Transform rootBone)
        {
            var bone = GetBoneByName(SLOT_BILLBOARD_NAME);
            if (bone) GameObject.DestroyImmediate(bone.gameObject);
            GameObject newBillboardSlot = new GameObject(SLOT_BILLBOARD_NAME);
            Transform newTransform = newBillboardSlot.transform;
            newTransform.SetParent(rootBone, false);
            var controller = this._actor.actorController.controller;
            if (controller != null)
            {
                oldPos = newTransform.localPosition;
                oldPos.y = controller.height + 0.2f;
                newTransform.localPosition = oldPos;
            }
            _bonesCache[SLOT_BILLBOARD_NAME] = newTransform;

        }

        Vector3 OffsetPos = new Vector3(0, 0f, 0f);
        private void InitSlotMagicGem(Transform rootBone)
        {
            var bone = GetBoneByName(SLOT_MAGIC_GEM_NAME);
            if (bone) GameObject.DestroyImmediate(bone.gameObject);
            GameObject newBillboardSlot = new GameObject(SLOT_MAGIC_GEM_NAME);
            Transform newTransform = newBillboardSlot.transform;
            newTransform.SetParent(rootBone, false);
            var controller = this._actor.actorController.controller;
            if (controller != null)
            {
                oldPos = newTransform.localPosition;
                oldPos.y = controller.height;
                oldPos += OffsetPos;
                newTransform.localPosition = oldPos;
            }
            _bonesCache[SLOT_MAGIC_GEM_NAME] = newTransform;

        }

        private void InitSlotCamera(Transform rootBone)
        {
            Transform bone = GetBoneByName(SLOT_CAMERA_NAME);
            if (bone) GameObject.DestroyImmediate(bone.gameObject);
            GameObject newCameraSlot = new GameObject(SLOT_CAMERA_NAME);
            Transform newTransform = newCameraSlot.transform;
            newTransform.SetParent(rootBone, false);
            Vector3 oldPos = newTransform.localPosition;
            oldPos.y = CameraSlotHight;
            newTransform.localPosition = oldPos;
            _bonesCache[SLOT_CAMERA_NAME] = newTransform;
        }

        private void InitSlotMiddle(Transform rootBone)
        {
            var bone = GetBoneByName(SLOT_MIDDLE_NAME);
            if (bone) GameObject.DestroyImmediate(bone.gameObject);
            GameObject newBillboardSlot = new GameObject(SLOT_MIDDLE_NAME);
            Transform newTransform = newBillboardSlot.transform;
            newTransform.SetParent(rootBone, false);
            var controller = this._actor.actorController.controller;
            if (controller != null)
            {
                oldPos = newTransform.localPosition;
                oldPos.y = controller.height * 0.5f;
                newTransform.localPosition = oldPos;
            }
            _bonesCache[SLOT_MIDDLE_NAME] = newTransform;
        }

        private void InitBoneCamera()
        {
            var bone = GetBoneByName(SLOT_BONE_CAMERA);
            if (bone) return;
            var rootBone = GetBoneByName(ROOT_NAME);
            if (rootBone)
            {
                GameObject newBone = new GameObject(SLOT_BONE_CAMERA);
                Transform newTransform = newBone.transform;
                newTransform.SetParent(rootBone, false);
                _bonesCache[SLOT_BONE_CAMERA] = newTransform;
                GetAnimCameraPos();
            }
        }

        private void CacheBoneChildren(Transform transform)
        {
            _bonesCache[transform.name] = transform;
            if (transform.name.Contains(PART_NAME))
            {
                int index = int.Parse(transform.name.Split('_')[1]);
                _beHitSlotDict.Add(index, transform);
            }
            for (int i = 0; i < transform.childCount; i++)
            {
                CacheBoneChildren(transform.GetChild(i));
            }
        }

        public Dictionary<int, Transform> GetBeHitSlotDict()
        {
            if (_beHitSlotDict.Count > 0)
            {
                return _beHitSlotDict;
            }
            return null;
        }

        public Transform GetBoneByName(string name)
        {
            InitBones();
            if (_bonesCache.ContainsKey(name))
            {
                if (SLOT_BILLBOARD_NAME == name)
                {
                    _bonesCache[name].rotation = Quaternion.identity;
                }
                return _bonesCache[name];
            }
            //if (UnityPropUtils.IsEditor)
            //    Debug.LogWarning("GetBoneByName Error slot not find: " + name);
            return null;
        }

        public Transform FindBoneChild(Transform transform, string boneName)
        {
            Transform bone = transform.Find(boneName);
            if (bone == null)
            {
                foreach (Transform child in transform)
                {
                    bone = FindBoneChild(child, boneName);
                    if (bone != null) return bone;
                }
            }
            return bone;
        }

        Transform _humanBody = null;
        public Transform GetHumanBody()
        {
            if (_humanBody != null) return _humanBody;
            _humanBody = _actor.transform.Find("man");
            if (_humanBody == null) _humanBody = _actor.transform.Find("woman");
            if (_humanBody == null) _humanBody = _actor.transform.Find("warrior_body");
            if (_humanBody == null) _humanBody = _actor.transform.Find("mage_body_01");
            if (_humanBody == null) _humanBody = _actor.transform.Find("warrior_body_cloth");
            if (_humanBody == null) _humanBody = GetBoneByName(BODY_NAME);
            //_humanBody = GetBoneByName(BODY_NAME);
            return _humanBody;
        }

        Transform _humanHead = null;
        public Transform GetHumanHead()
        {
            if (_humanHead != null) return _humanHead;
            _humanHead = GetBoneByName(HEAD_NAME);
            return _humanHead;
        }

        Transform _humanFace = null;
        public Transform GetHumanFace()
        {
            if (_humanFace != null) return _humanFace;
            _humanFace = GetBoneByName(FACE_NAME);
            return _humanFace;
        }

        Transform _cameraPos = null;
        public Transform GetAnimCameraPos()
        {
            if (_cameraPos != null) return _cameraPos;
            _cameraPos = GetBoneByName(SLOT_CAMERA_POS);
            if (_cameraPos == null)
            {
                var boneCamera = GetBoneByName(SLOT_BONE_CAMERA);
                if (boneCamera)
                {
                    _cameraPos = (new GameObject(SLOT_CAMERA_POS)).transform;
                    _cameraPos.SetParent(boneCamera, false);
                    _cameraPos.localPosition = Vector3.zero;
                    _cameraPos.Rotate(270f, 180f, 0);
                    _bonesCache[SLOT_CAMERA_POS] = _cameraPos;
                }
            }
            return _cameraPos;
        }

        public void SetWeaponSlotVisible(bool state)
        {
            for (int i = 0; i < WEAPON_SLOTS.Count; i++)
            {
                Transform bone = GetBoneByName(WEAPON_SLOTS[i]);
                if (bone != null) bone.gameObject.SetActive(state);
            }
        }

        public void EnableMeshRender(bool isHide)
        {
            for (int k = 0; k < WEAPON_SLOTS.Count; k++)
            {
                Transform bone = GetBoneByName(WEAPON_SLOTS[k]);
                if (bone != null) {
                    var mrList = bone.GetComponentsInChildren<MeshRenderer>();
                    if (mrList != null)
                    {
                        for (int i = 0; i < mrList.Length; ++i)
                        {
                            mrList[i].enabled = !isHide;
                        }
                    }
                }
            }
        }

        public void ResetBillboard(float height)
        {
            var bone = GetBoneByName(SLOT_BILLBOARD_NAME);
            oldPos = bone.localPosition;
            oldPos.y = height;
            bone.localPosition = oldPos;
        }
    }
}
