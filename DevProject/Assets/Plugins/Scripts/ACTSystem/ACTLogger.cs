﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACTSystem
{
    public class ACTLogger
    {
        public static void Info(string msg) {
            ACTSystemDriver.GetInstance().Logger.Info(msg);
        }

        public static void Error(string msg) {
            ACTSystemDriver.GetInstance().Logger.Error(msg);
        }

        public static void Warning(string msg) {
            ACTSystemDriver.GetInstance().Logger.Warning(msg);
        }
    }
}
