﻿#region 模块信息
/*==========================================
// 模块名：ACTExtensions
// 命名空间: ACTSystem
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/05/21
// 描述说明：用来扩展技能相关类的方法
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ACTSystem
{
    public static class ACTExtensions
    {
        public static Vector3 ToVector3(this ACTVector3 vector3)
        {
            return new Vector3(vector3.x, vector3.y, vector3.z);
        }
    }
}
