﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

using UnityEngine;

namespace ACTSystem
{

    public class ACTSystemTools
    {
        static string FxTag = "Fx";
        public static string EmptyPrefab = "EmptyPrefab";

        static Dictionary<string, int> nameToLayerDict = new Dictionary<string, int>();
        static Dictionary<int, string> layerToNameDict = new Dictionary<int, string>();

        static public int NameToLayer(string name)
        {
            int layer;
            if (nameToLayerDict.TryGetValue(name, out layer))
            {
                return layer;
            }
            else 
            {
                layer = LayerMask.NameToLayer(name);
                nameToLayerDict.Add(name, layer);
                return layer;
            }
        }

        static public string LayerToName(int layer)
        {
            string name;
            if (layerToNameDict.TryGetValue(layer, out name))
            {
                return name;
            }
            else
            {
                name = LayerMask.LayerToName(layer);
                layerToNameDict.Add(layer, name);
                return name;
            }
        }

        static public void SetLayer(Transform trans, int layer, bool includeChildren = false)
        {
            if (layer == ACTLayerUtils.Actor && trans.gameObject.CompareTag(FxTag))
            {
                trans.gameObject.layer = ACTLayerUtils.Bloom;
            }
            else
            {
                trans.gameObject.layer = layer;
            }

            if (includeChildren)
            {
                for (int i = 0; i < trans.childCount; i++)
                {
                    SetLayer(trans.GetChild(i), layer, includeChildren);
                }
            }
        }

        static public void SetFxTag(Transform trans)
        {
            if (trans.gameObject.GetComponent<ParticleSystem>() != null)
            {
                trans.gameObject.tag = FxTag;
            }
            for (int i = 0; i < trans.childCount; i++)
            {
                SetFxTag(trans.GetChild(i));
            }
        }

        static public void SetFxShow(Transform trans, bool isShow)
        {
            if (trans.gameObject.CompareTag(FxTag))
            {
                trans.gameObject.SetActive(isShow);
            }
            for (int i = 0; i < trans.childCount; i++)
            {
                SetFxShow(trans.GetChild(i), isShow);
            }
        }

        static public void SetUIFxLayerShow(Transform trans, bool show)
        {
            if (trans.gameObject.CompareTag(FxTag))
            {
                if (show)
                {
                    trans.gameObject.layer = ACTLayerUtils.UI;
                }
                else 
                {
                    trans.gameObject.layer = ACTLayerUtils.Bloom;
                }
            }

            for (int i = 0; i < trans.childCount; i++)
            {
                SetUIFxLayerShow(trans.GetChild(i), show);
            }
        }

        static public Transform GetBone(Transform transform, string boneName)
        {
            Transform bone = transform.Find(boneName);
            if (bone == null)
            {
                foreach (Transform child in transform)
                {
                    bone = GetBone(child, boneName);
                    if (bone != null) return bone;
                }
            }
            return bone;
        }

        static public string ROOT_NAME = "bip_master";
        static public string GetBonePath(Transform transform, string name = "")
        {
            if (transform == null)
            {
                return name;
            }
            if (transform.name == ROOT_NAME)
            {
                return name == ""?transform.name:transform.name + "/" + name;
            }
            return GetBonePath(transform.parent, name == ""?transform.name:transform.name + "/" + name);
        }

        /// 替换字符串中的子字符串。
        /// </summary>
        /// <param name="input">原字符串</param>
        /// <param name="oldValue">旧子字符串</param>
        /// <param name="newValue">新子字符串</param>
        /// <param name="count">替换数量</param>
        /// <param name="startAt">从第几个字符开始</param>
        /// <returns>替换后的字符串</returns>
        public static String ReplaceFirst(string input, string oldValue, string newValue, int startAt = 0)
        {
            int pos = input.IndexOf(oldValue, startAt);
            if (pos < 0)
            {
                return input;
            }
            return string.Concat(input.Substring(0, pos), newValue, input.Substring(pos + oldValue.Length));
        }

        static private float DEBUG_SHOW_TIME = 4;

        static public void DrawCircleRange(Vector3 origin, float radius)
        {
            if (!UnityPropUtils.IsEditor)
            {
                return;
            }
            
            //Debug.DrawRay(origin, direction.normalized * distance, Color.blue, 6);
            Vector3 direction = new Vector3(0, 0, 1);
            Matrix4x4 m1 = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, 1, 0), Vector3.one);
            for (int i = 0; i < 360; i++)
            {
                direction = m1.MultiplyVector(direction).normalized * radius;
                Debug.DrawLine(origin, origin + direction, Color.red, DEBUG_SHOW_TIME);
            }
        }

        static public void DrawSphereCast(Vector3 origin, Vector3 direction, float radius, float distance)
        {
            if (!UnityPropUtils.IsEditor)
            {
                return;
            }
            var angle = 5;
            Matrix4x4 m1 = Matrix4x4.TRS(Vector3.zero, Quaternion.AngleAxis(angle, direction), Vector3.one);
            var direction2 = Vector3.Cross(direction, Vector3.up);
            for (int i = 0; i < 360 / angle; i++)
            {
                direction2 = m1.MultiplyVector(direction2).normalized * radius;
                Debug.DrawLine(origin, origin + direction2, Color.red, DEBUG_SHOW_TIME);
                Debug.DrawLine(origin + direction2, origin + direction2 + direction * distance, Color.red, DEBUG_SHOW_TIME);
            }
        }
    }

}
