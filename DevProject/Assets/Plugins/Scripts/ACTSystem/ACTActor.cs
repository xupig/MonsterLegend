﻿using UnityEngine;
using System.Collections;
using System;

namespace ACTSystem
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(ACTEventListener))]
    public class ACTActor : MonoBehaviour
    {
        public int ID = 0;
        public int ActorID = 0;
        public string modelName = string.Empty;
        public IACTActorOwner owner;
        public ACTDoSkill action = null;
        public ACTDataActor actorData;
        public ACTDataState actorState;
        public ACTActorController actorController;
        public ACTAnimationController animationController;
        public ACTSoundController soundController;
        public ACTFXController fxController;
        public ACTBoneController boneController;
        public ACTWeaponController weaponController;
        public ACTEquipController equipController;
        public ACTMaterialAnimationController materialAnimationController;
        public ACTFootStep footStep;
        public int fxSortingOrder = -1;
        public ACTRideController rideController;
        public int rideId = 0;
        public ACTActor ride 
        {
            get
            {
                if (rideId == 0) return null;
                return ACTSystemDriver.GetInstance().GetActor(rideId);
            }
        }
        public int rideOwnerId = 0;
        public ACTActor rideOwner
        {
            get
            {
                if (rideOwnerId == 0) return null;
                return ACTSystemDriver.GetInstance().GetActor(rideOwnerId);
            }
        }

        public bool isRiding
        {
            get
            {
                return rideId > 0;
            }
        }

        public Vector3 position
        {
            set
            {
                GetTransform().position = value;
            }
            get
            {
                return GetTransform().position;
            }
        }

        public Vector3 localPosition
        {
            set
            {
                GetTransform().localPosition = value;
            }
            get
            {
                return GetTransform().localPosition;
            }
        }

        public Vector3 localScale
        {
            set
            {
                GetTransform().localScale = value;
            }
            get
            {
                return GetTransform().localScale;
            }
        }

        public Vector3 forward
        {
            set
            {
                GetTransform().forward = value;
            }
            get
            {
                return GetTransform().forward;
            }
        }

        public Vector3 up
        {
            get
            {
                return GetTransform().up;
            }
        }

        public Vector3 right
        {
            get
            {
                return GetTransform().right;
            }
        }

        public Quaternion rotation
        {
            set
            {
                GetTransform().rotation = value;
            }
            get
            {
                return GetTransform().rotation;
            }
        }

        public Vector3 localEulerAngles
        {
            set
            {
                GetTransform().localEulerAngles = value;
            }
            get
            {
                return GetTransform().localEulerAngles;
            }
        }

        bool _isFly = false;
        public bool isFly
        {
            set
            {
                _isFly = value;
            }
            get
            {
                return _isFly;
            }
        }

        bool _visible = true;
        public bool visible
        {
            set
            {
                if (_visible == value) return;
                _visible = value;
                OnVisibleChange();
                if (isRiding)
                    ride.visible = _visible;
            }
            get
            {
                return _visible;
            } 
        }

        private bool _isFlyingOnVehicle = false;
        public bool isFlyingOnVehicle
        {
            get { return _isFlyingOnVehicle; }
            set 
            { 
                _isFlyingOnVehicle = value;
                actorController.Stop();
            }
        }

        //是否是坐骑飞行状态
        private bool _isFlyingOnRide = false;
        public bool isFlyingOnRide
        {
            get { return _isFlyingOnRide; }
            set { _isFlyingOnRide = value; }
        }

        //是否因为飞行切换了层级
        private bool _modifyLayer = false;
        public bool modifyLayer
        {
            get { return _modifyLayer; }
        }
        //是否有脚印粒子
        private bool _isUseFootSmoke = true;
        public bool IsUseFootSmoke
        {
            get { return _isUseFootSmoke; }
            set { _isUseFootSmoke = value; }
        }
        //是否有脚步声
        private bool isUseFootSound = true;

        public bool IsUseFootSound
        {
            get { return isUseFootSound; }
            set { isUseFootSound = value; }
        }
        //脚步的速度限制
        private float footStepSpeed = 10.0f;

        public float FootStepSpeed
        {
            get { return footStepSpeed; }
            set { footStepSpeed = value; }
        }
        //脚于地面距离
        public float FootMinDistance = 0.25f;
        //左脚、右脚
        public Transform LeftFootTransform;
        public Transform RightFootTransform;
        //记录顺序播放自身受击音效时当前音效序号
        public int curBehitSelfCount = 0;

        //是否在安全区
        public bool inSafeArea = false;

        //是否屏蔽特效
        public bool isHideFX = false;

        public void SetIsFlyingOnRide(bool state, bool modifyLayer = false)
        {
            if (_isFlyingOnRide != state)
            {
                _isFlyingOnRide = state;
                _modifyLayer = modifyLayer;
                int actionID = state ? ACTAnimationActionID.FLY_UP : ACTAnimationActionID.FLY_DOWN;
                ride.animationController.PlayAction(actionID);
                actorController.Stop();
			}
			int rideLayer = (state && modifyLayer) ? ACTLayerUtils.ActorFly : ACTLayerUtils.Actor;
			ride.SetLayer(rideLayer, false);
        }

        void Awake()
        {
            actorState = new ACTDataState();
            actorController = new ACTActorController(this);
            animationController = new ACTAnimationController(this);
            soundController = new ACTSoundController(this);
            fxController = new ACTFXController(this);
            boneController = new ACTBoneController(this);
            weaponController = new ACTWeaponController(this);
            equipController = new ACTEquipController(this);
            rideController = new ACTRideController(this);
            materialAnimationController = new ACTMaterialAnimationController(this);
            footStep = new ACTFootStep(this);
            var animator = GetComponent<Animator>();
            if (animator != null)
            {
                animator.applyRootMotion = false;
            }
            if (this.gameObject.GetComponent<ACTEventListener>() == null)
            {
                this.gameObject.AddComponent<ACTEventListener>();
            }
            modelName = this.gameObject.name;
        }

        public void Reset()
        {
            this.visible = true;
			this._isFlyingOnVehicle = false;
			this._isFlyingOnRide = false;
            this.actorState.Reset();
            this.actorController.Reset();
            this.animationController.Reset();
            this.materialAnimationController.Reset();
        }

        void Start()
        {

        }
        
        void Update()
        {
            if (animationController != null)
            {
                animationController.Update();
            }
            if (equipController != null)
            {
                equipController.Update();
            }
            if (rideController != null)
            {
                rideController.Update();
            }
            if (fxController != null)
            {
                fxController.Update();
            }
            CheckingSkinVisible();
        }

        void LateUpdate()
        {
            if (actorController == null) return;
            actorController.Update();
        }

        void OnDestroy()
        {
            if (equipController != null)
            {
                equipController.OnActorDestroy();
            }
            ACTSystemDriver dirver = ACTSystemDriver.GetInstance();
            if (dirver != null)
            {
                dirver.OnActorDestroy(ID);
            }
        }

        void OnVisibleChange()
        {
            int layer;
            if (!_visible)
            {
                layer = ACTLayerUtils.TransparentFX;
            }
            else
            {
                layer = ACTLayerUtils.Actor;
            }
            int currentLayer;
            for (int i = 0; i < transform.childCount; i++)
            {
                currentLayer = transform.GetChild(i).gameObject.layer;
                if (currentLayer != ACTLayerUtils.TransparentFX && currentLayer != ACTLayerUtils.Actor)
                {
                    continue;
                }
                ACTSystemTools.SetLayer(transform.GetChild(i), layer, true);
            }
            //this.animationController.animator.enabled = _visible;
            if (isRiding && _visible)
            {
                this.animationController.SetRidingState(true);
                this.animationController.SetRidingType(this.actorState.RideType);
            }
        }

        public string GetLayer()
        {
            //return LayerMask.LayerToName(gameObject.layer);
            return ACTSystemTools.LayerToName(gameObject.layer);
        }

        public void SetLayer(int layer, bool includeChildren = false)
        {
            ACTSystemTools.SetLayer(transform, layer, includeChildren);
        }

        public void SetLayer(string layer, bool includeChildren = false)
        {
            ACTSystemTools.SetLayer(transform, UnityEngine.LayerMask.NameToLayer(layer), includeChildren);
        }

        //做显示延时
        private bool _skinVisible = false;
        private float _skinVisibleBeginTime = 0;
        void CheckingSkinVisible()
        {
            if (_skinVisible) return;
            if (_skinVisibleBeginTime > UnityPropUtils.realtimeSinceStartup) return;
            _skinVisibleBeginTime = 0;
            _skinVisible = true;
            this.equipController.SkinVisible(_skinVisible);
        }

        public void SetSkinVisibleDelay(float duration)
        {
            _skinVisible = false;
            this.equipController.SkinVisible(_skinVisible);
            _skinVisibleBeginTime = UnityPropUtils.realtimeSinceStartup + duration;
        }

        public void ToGround()
        {
            if (isRiding)
                ride.actorController.ToGround();
            else
                actorController.ToGround();
        }

        public void TurnTo(Vector3 direction)
        {
            localEulerAngles = direction;
        }

        public void FaceTo(ACTActor target, bool includeVertical = false)
        {
            if (target == null)
            {
                return;
            }
            Vector3 targetPos = target.transform.position;
            FaceTo(targetPos, includeVertical);
        }

        public void FaceTo(Vector3 targetPos, bool includeVertical = false)
        {
            if (includeVertical)
            {
                GetTransform().LookAt(targetPos);
            }
            else {
                GetTransform().LookAt(new Vector3(targetPos.x, transform.position.y, targetPos.z));
            }
        }

        public Transform GetTransform()
        {
            if (isRiding)
                return ride.GetTransform();
            else
                return transform;
        }
        public Action<Collider> onTriggerEnterCall = null;
        void OnTriggerEnter(Collider collider)
        {
            if (onTriggerEnterCall != null)
            {
                onTriggerEnterCall(collider);
            }
        }
    }
}
