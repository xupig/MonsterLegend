﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    public class ACTSimpleLogger : IACTLogger
    {
        static public bool isDebugMode = false;

        public void Info(string msg)
        {
            if (UnityPropUtils.IsEditor || isDebugMode)
            {
                Debug.Log(msg);
            }
        }

        public void Error(string msg)
        {
            if (UnityPropUtils.IsEditor || isDebugMode)
            {
                Debug.LogError(msg);
            }
        }

        public void Warning(string msg)
        {
            if (UnityPropUtils.IsEditor || isDebugMode)
            {
                Debug.LogWarning(msg);
            }
        }
    }
}
