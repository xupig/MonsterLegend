﻿#region 模块信息
/*==========================================
// 模块名：ACTAudioSourceManager
// 命名空间: ACTSystem
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/05/23
// 描述说明：技能音效声源管理器
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ACTSystem
{
    public enum ACTAudioSourceType
    {
        /// <summary>
        /// 攻击音效
        /// </summary>
        AttackSound = 0,

        /// <summary>
        /// 攻击语音
        /// </summary>
        AttackVoice = 1,

        /// <summary>
        /// 受击音效
        /// </summary>
        BehitSound = 2,

        /// <summary>
        /// 受击自身音效
        /// </summary>
        BehitSelfSound = 3,
        /// <summary>
        /// 脚步音效
        /// </summary>
        FootSound = 4,
        /// <summary>
        /// 说话音效
        /// </summary>
        SpeakSound = 5
    }

    public enum ACTSelfSoundType
    {
        /// <summary>
        /// 出生音效
        /// </summary>
        BornSound = 0,

        /// <summary>
        /// 死亡语音
        /// </summary>
        DeadSound = 1,

        /// <summary>
        /// 受击自身音效
        /// </summary>
        BehitSelfSound = 2
    }

    public enum ACTFootSoundType
    {
        /// <summary>
        /// 默认音效
        /// </summary>
        Default = 0,

        /// <summary>
        /// 水中音效
        /// </summary>
        Water = 1,

        /// <summary>
        /// 沙地音效
        /// </summary>
        Sand = 2
    }

    public class ACTAudioSourceManager
    {

        public ACTActor m_actor;

        /// <summary>
        /// 最大声道数量
        /// </summary>
        private int m_maxAudioSourceCount = 0;

        /// <summary>
        /// 声道列表
        /// </summary>
        private List<AudioSource> m_audioSourceList;

        /// <summary>
        /// 声道列表下标
        /// </summary>
        private int m_asIndex = 0;
        public int asIndex
        {
            get
            {
                return m_asIndex;
            }
            set
            {
                value %= m_maxAudioSourceCount;
                m_asIndex = value;
            }
        }

        /// <summary>
        /// 吼叫声源
        /// </summary>
        private AudioSource m_RoarAudioSource = null;

        /// <summary>
        /// 被打声源
        /// </summary>
        private AudioSource m_BeHitAudioSource = null;

        /// <summary>
        /// 被打自身声源
        /// </summary>
        private AudioSource m_BeHitSelfAudioSource = null;

        /// <summary>
        /// 脚步声源
        /// </summary>
        private AudioSource m_FootAudioSource = null;

        private AudioSource m_SpeakAudioSource = null;

        public ACTAudioSourceManager(ACTActor _actor, int _maxAuduoSourceCount)
        {
            this.m_actor = _actor;
            this.m_maxAudioSourceCount = _maxAuduoSourceCount;
            m_audioSourceList = new List<AudioSource>();
        }

        #region 获取声源
        /// <summary>
        /// 获取攻击音效声道
        /// </summary>
        /// <returns></returns>
        private AudioSource GetAttackSoundAudioSource()
        {
            if (m_audioSourceList.Count <= asIndex)
            {
                var ad = AddComponent();
                m_audioSourceList.Add(ad);
            }
            return m_audioSourceList[asIndex++];
        }

        /// <summary>
        /// 获取吼叫音效声道
        /// </summary>
        /// <returns></returns>
        private AudioSource GetRoarAudioSource()
        {
            if (m_RoarAudioSource == null)
            {
                m_RoarAudioSource = AddComponent();
                //ACTSystemLog.LogError("[ACTAudioSourceManager:GetRoarAudioSource]=>2___name: " + m_actor.gameObject.name);
            }
            return m_RoarAudioSource;
        }

        /// <summary>
        /// 获取被击打音效声道
        /// </summary>
        /// <returns></returns>
        private AudioSource GetBeHitAudioSource()
        {
            if (m_BeHitAudioSource == null)
            {
                m_BeHitAudioSource = AddComponent();
            }
            return m_BeHitAudioSource;
        }

        /// <summary>
        /// 获取被击打自身音效声道
        /// </summary>
        /// <returns></returns>
        private AudioSource GetBeHitSelfAudioSource()
        {
            if (m_BeHitSelfAudioSource == null)
            {
                m_BeHitSelfAudioSource = AddComponent();
            }
            return m_BeHitSelfAudioSource;
        }

        /// <summary>
        /// 获取脚步音效声道
        /// </summary>
        /// <returns></returns>
        private AudioSource GetFootAudioSource()
        {
            if (m_FootAudioSource == null)
            {
                m_FootAudioSource = AddComponent();
            }
            return m_FootAudioSource;
        }

        private AudioSource GetSpeakAudioSource()
        {
            if (m_SpeakAudioSource == null)
            {
                m_SpeakAudioSource = AddComponent();
            }
            return m_SpeakAudioSource;
        }

        public AudioSource GetAudioSource(ACTAudioSourceType _audioSourceType = ACTAudioSourceType.AttackSound)
        {
            switch (_audioSourceType)
            {
                case ACTAudioSourceType.AttackSound:
                    return GetAttackSoundAudioSource();
                case ACTAudioSourceType.AttackVoice:
                    return GetRoarAudioSource();
                case ACTAudioSourceType.BehitSound:
                    return GetBeHitAudioSource();
                case ACTAudioSourceType.BehitSelfSound:
                    return GetBeHitSelfAudioSource();
                case ACTAudioSourceType.FootSound:
                    return GetFootAudioSource();
                case ACTAudioSourceType.SpeakSound:
                    return GetSpeakAudioSource();
                default:
                    break;
            }
            return null;
        }

        //static AnimationCurve _animationCurve = null;
        //static AnimationCurve GetAnimationCurve()
        //{
        //    if (_animationCurve == null)
        //    {
        //        _animationCurve = new AnimationCurve(new Keyframe[]{
        //            new Keyframe(0, 1),
        //            new Keyframe(0.8f, 1),
        //            new Keyframe(1, 0f),
        //        });

        //    }
        //    return _animationCurve;
        //}

        private AudioSource AddComponent()
        {
            AudioSource audioSource = m_actor.gameObject.AddComponent<AudioSource>();
            audioSource.spatialBlend = 1;
            audioSource.rolloffMode = AudioRolloffMode.Custom;
            audioSource.maxDistance = 15f;
            audioSource.playOnAwake = false;
            //audioSource.SetCustomCurve(AudioSourceCurveType.CustomRolloff, GetAnimationCurve());
            return audioSource;
        }
        #endregion

        #region 临时音量控制
        public void TempCloseVolume()
        {
            TempSetAttackAudioVolume();
            TempSetRoarAudioVolume();
            TempSetBeHitAudioVolume();

        }

        public void ResetTempVolume()
        {
            TempSetAttackAudioVolume(ACTSystem.ACTSystemDriver.GetInstance().soundVolume);
            TempSetRoarAudioVolume(ACTSystem.ACTSystemDriver.GetInstance().soundVolume);
            TempSetBeHitAudioVolume(ACTSystem.ACTSystemDriver.GetInstance().soundVolume);

        }

        private void TempSetAttackAudioVolume(float svolume = 0f)
        {
            if (m_audioSourceList != null)
            {
                for (int i = 0; i < m_audioSourceList.Count; i++)
                {
                    m_audioSourceList[i].volume = svolume;
                    //Debug.LogError("[TempSetAttackAudioVolume]=>1_____svolume:  " + svolume);
                }
            }
        }
        private void TempSetRoarAudioVolume(float svolume = 0f)
        {
            if (m_RoarAudioSource == null) return;
            m_RoarAudioSource.volume = svolume;
        }

        private void TempSetBeHitAudioVolume(float svolume = 0f)
        {
            if (m_BeHitAudioSource == null) return;
            m_BeHitAudioSource.volume = svolume;
        }
        #endregion


    }
}
