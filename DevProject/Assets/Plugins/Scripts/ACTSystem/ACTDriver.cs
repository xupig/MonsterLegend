﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{


    public class ACTDriver:MonoBehaviour
    {


        public ACTTimeScaleManager actTimeScaleManager;
        public ACTVisualFXManager actVisualFXManager;
        void Awake()
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 30;
            actTimeScaleManager = ACTTimeScaleManager.GetInstance();
            actVisualFXManager = ACTVisualFXManager.GetInstance();
        }

        void Update()
        {
            if (actTimeScaleManager!=null)
            {
                actTimeScaleManager.OnUpdate();
                actVisualFXManager.OnUpdate();
            }
        }

    }
}
