﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{


    /// <summary>
    /// actor数据
    /// </summary>
    //[System.Serializable]
    public class ACTDataState
    {
        public delegate void BoolDelegate(bool value);
        public BoolDelegate OnGroundChange;
        public BoolDelegate OnAirChange;
        public Action OnGroundCB;
        public Action OnAirCB;

        public AnimatorStateInfo CurAnimationState = new AnimatorStateInfo();

        public float BaseSpeed = 4f;

        public float SpeedMlp = 1;

        public bool CanTurn = true;

        public bool CanMove = false;

        public bool RightMove = false;

        public float MovingSpeedState = 0f;

        public float rightMovingSpeedState = 0f;
        public float forwardMovingSpeedState = 0f;

        public float MovingSpeedTransition = 0f;

        public float rightMovingSpeedTransition = 0f;
        public float forwardMovingSpeedTransition = 0f;

        bool _inAir = false;
        public bool InAir
        {
            get { return _inAir; }
            set { _inAir = value; }
        }

        bool _onGround = true;
        public bool OnGround {
            get { return _onGround; }
            set {
                if (_onGround == value) return;
                _onGround = value;
                if (OnGroundChange != null) OnGroundChange(_onGround);
                if (_onGround && OnGroundCB != null) OnGroundCB();
            }
        }

        bool _onAir = true;
        public bool OnAir
        {
            get { return _onAir; }
            set
            {
                if (_onAir == value) return;
                _onAir = value;
                if (OnAirChange != null) OnAirChange(_onAir);
                if (_onAir && OnAirCB != null) OnAirCB();
            }
        }

        public float OnHitAir = 0;

        public bool OnFlying = false;

        public bool IsReady = false;

        public bool Damaged = false;

        public bool OnRiding = false;

        public int RideType = 0;

        public void Reset()
        {
            BaseSpeed = 4f;
            SpeedMlp = 1;
            CanTurn = true;
            CanMove = false;
            RightMove = false;
            MovingSpeedState = 0f;
            rightMovingSpeedState = 0f;
            forwardMovingSpeedState = 0f;
            MovingSpeedTransition = 0f;
            rightMovingSpeedTransition = 0f;
            forwardMovingSpeedTransition = 0f;
            OnGround = true;
            OnHitAir = 0;
            OnFlying = false;
            Damaged = false;
            OnRiding = false;
            RideType = 0;
        }
    }
}
