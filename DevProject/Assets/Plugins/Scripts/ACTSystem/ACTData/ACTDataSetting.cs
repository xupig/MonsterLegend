﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{

    [System.Serializable]
    public class ACTDataSettingKeyAction
    {
        public KeyCode KeyCode;
        public int ActionID;
    }


    /// <summary>
    /// 
    /// </summary>
    [System.Serializable]
    public class ACTDataSetting
    {
        /// <summary>
        /// 被控制对象
        /// </summary>
        public ACTActor Actor;

        /// <summary>
        /// 键对应行为对照表
        /// </summary>
        public ACTDataSettingKeyAction[] KeyActionList;

    }

    [System.Serializable]
    public class ACTDataSettingAction
    {
        public bool Enable;

        public int ActionID;

        public int CutTime;
    }

    [System.Serializable]
    public class ACTDataSettingContiAttack
    {
        /// <summary>
        /// 被控制对象
        /// </summary>
        public ACTActor Actor;

        public ACTDataSettingAction[] ActionArr;

        

    }


    [System.Serializable]
    public class ACTDataSettingStandBy
    {
        /// <summary>
        /// 被控制对象
        /// </summary>
        public ACTActor Actor;

        public List<int> ActionIdList;

        public float MinDuration;

        public float MaxDuration;
    }

}
