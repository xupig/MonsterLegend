﻿using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    [System.Serializable]
    public class ACTDataVisualFXList : ScriptableObject
    {
        public List<ACTDataVisualFX> m_dataList = new List<ACTDataVisualFX>();
        private Dictionary<int, ACTDataVisualFX> m_dataDict = new Dictionary<int, ACTDataVisualFX>();
        public ACTDataVisualFXList()
        {

        }

        public void ReInitDict()
        {
            m_dataDict.Clear();
            for (int i = 0; i < m_dataList.Count; i++)
            {
                var data = m_dataList[i];
                //data.SetDirty();
                m_dataDict.Add(data.ID, data);
            }
        }

        public void CreateACTDataVisualFX(int id)
        {
            if (m_dataDict.ContainsKey(id)) return;
            var data = new ACTDataVisualFX();
            data.ID = id;
            m_dataDict.Add(data.ID, data);
            m_dataList.Add(data);
        }

        public void RemoveACTDataVisualFX(int id)
        {
            if (!m_dataDict.ContainsKey(id)) return;
            var data = m_dataDict[id];
            m_dataDict.Remove(data.ID);
            m_dataList.Remove(data);
        }

        public ACTDataVisualFX LoadOrCreate(int id)
        {
            if (!m_dataDict.ContainsKey(id))
            {
                CreateACTDataVisualFX(id);
            }
            return m_dataDict[id];
        }

        public ACTDataVisualFX GetVisualFXData(int id)
        {
            if (!m_dataDict.ContainsKey(id)) return null;
            return m_dataDict[id];
        }

        public Dictionary<int, ACTDataVisualFX> GetVisualFXDict()
        {
            return m_dataDict;
        }

        public void SetVisualFXDict(Dictionary<int, ACTDataVisualFX> dict)
        {
            m_dataDict = dict;
        }

        public bool IsEmpty()
        {
            return m_dataList.Count == 0;
        }
    }



}
