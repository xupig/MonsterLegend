﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

using UnityEngine;

namespace ACTSystem
{

    [System.Serializable]
    public class ACTDataSkillList : ScriptableObject
    {
        public List<ACTDataSkill> m_skillList = new List<ACTDataSkill>();
        private Dictionary<int, ACTDataSkill> m_skillDict = new Dictionary<int, ACTDataSkill>();
        public ACTDataSkillList()
        { 
            
        }

        public void ReInitDict()
        {
            m_skillDict.Clear();
            for (int i = 0; i < m_skillList.Count; i++)
            {
                var data = m_skillList[i];
                data.SetDirty();
                m_skillDict.Add(data.ID, data);
            }
        }

        public void CreateACTDataSkill(int id)
        {
            if (m_skillDict.ContainsKey(id)) return;
            var data = new ACTDataSkill();
            data.ID = id;
            m_skillDict.Add(data.ID, data);
            m_skillList.Add(data);
        }

        public void RemoveACTDataSkill(int id)
        {
            if (!m_skillDict.ContainsKey(id)) return;
            var data = m_skillDict[id];
            m_skillDict.Remove(data.ID);
            m_skillList.Remove(data);
        }

        public ACTDataSkill LoadOrCreate(int id)
        {
            if (!m_skillDict.ContainsKey(id))
            {
                CreateACTDataSkill(id);
            }
            m_skillDict[id].ResetID();
            return m_skillDict[id];
        }

        public ACTDataSkill Clone(int sid, int tid)
        {
            if (!m_skillDict.ContainsKey(sid)) return null;
            if (m_skillDict.ContainsKey(tid))
            {
                RemoveACTDataSkill(tid);
            };
            var data = m_skillDict[sid].Clone();
            data.ID = tid;
            m_skillDict.Add(data.ID, data);
            m_skillList.Add(data);
            return data;
        }

        public ACTDataSkill GetSkillData(int id)
        {
            if (!m_skillDict.ContainsKey(id)) return null;
            return m_skillDict[id];
        }

        public Dictionary<int, ACTDataSkill> GetSkillDict()
        {
            return m_skillDict;
        }

        public void SetSkillDict(Dictionary<int, ACTDataSkill> dict)
        {
            m_skillDict = dict;
        }

        public bool IsEmpty()
        {
            return m_skillList.Count == 0;
        }

        
    }

}
