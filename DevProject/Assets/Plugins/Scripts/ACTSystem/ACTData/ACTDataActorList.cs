﻿using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{


    [System.Serializable]
    public class ACTDataActorList : ScriptableObject
    {
        public List<ACTDataActor> m_dataList = new List<ACTDataActor>();
        private Dictionary<int, ACTDataActor> m_dataDict = new Dictionary<int, ACTDataActor>();
        public ACTDataActorList()
        {

        }


        public void ReInitDict()
        {
            m_dataDict.Clear();
            for (int i = 0; i < m_dataList.Count; i++)
            {
                var data = m_dataList[i];
                //data.SetDirty();
                m_dataDict.Add(data.ID, data);
            }
        }

        public void CreateACTDataActor(int id)
        {
            if (m_dataDict.ContainsKey(id)) return;
            var data = new ACTDataActor();
            data.ID = id;
            m_dataDict.Add(data.ID, data);
            m_dataList.Add(data);
        }

        public void RemoveACTDataActor(int id)
        {
            if (!m_dataDict.ContainsKey(id)) return;
            var data = m_dataDict[id];
            m_dataDict.Remove(data.ID);
            m_dataList.Remove(data);
        }

        public ACTDataActor LoadOrCreate(int id)
        {
            if (!m_dataDict.ContainsKey(id))
            {
                CreateACTDataActor(id);
            }
            return m_dataDict[id];
        }

        public ACTDataActor GetActorData(int id)
        {
            if (!m_dataDict.ContainsKey(id)) return null;
            return m_dataDict[id];
        }

        public Dictionary<int, ACTDataActor> GetActorDict()
        {
            return m_dataDict;
        }

        public void SetActorDict(Dictionary<int, ACTDataActor> dict)
        {
            m_dataDict = dict;
        }

        public bool IsEmpty()
        {
            return m_dataList.Count == 0;
        }
    }



}
