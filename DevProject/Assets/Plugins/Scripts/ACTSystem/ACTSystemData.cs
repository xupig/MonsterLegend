﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    [System.Serializable]
    public class ACTSystemData :ScriptableObject
    {
        public ACTDataActorList ActorList;//= new ACTDataActorList();

        public ACTDataSkillList SkillList;//= new ACTDataSkillList();

        public ACTDataVisualFXList VisualFXList;//= new ACTDataActorList();

        public ACTDataEquipmentList EquipmentList;//= new ACTDataActorList();

        public ACTDataEquipmentFXList EquipmentFXList;//= new ACTDataActorList();

        public bool isInitedBySerializableData = false;

        public void ReInit()
        {
            if (isInitedBySerializableData) return;
            if (SkillList != null) SkillList.ReInitDict();
            if (ActorList != null) ActorList.ReInitDict();
            if (VisualFXList != null) VisualFXList.ReInitDict();
            if (EquipmentList != null) EquipmentList.ReInitDict();
            if (EquipmentFXList != null) EquipmentFXList.ReInitDict();
        }

        public void InitBySerializableData(ACTSerializableData data)
        {
            //ACTSystemLog.LogError("InitBySerializableData::::");
            isInitedBySerializableData = true;
            this.ActorList = UnityEngine.ScriptableObject.CreateInstance<ACTDataActorList>();//new ACTDataActorList();
            this.ActorList.SetActorDict(data.ActorsDict);
            this.SkillList = UnityEngine.ScriptableObject.CreateInstance<ACTDataSkillList>();// new ACTDataSkillList();
            this.SkillList.SetSkillDict(data.SkillsDict);
            this.VisualFXList = UnityEngine.ScriptableObject.CreateInstance<ACTDataVisualFXList>();// new ACTDataSkillList();
            this.VisualFXList.SetVisualFXDict(data.VisualFXsDict);
            this.EquipmentList = UnityEngine.ScriptableObject.CreateInstance<ACTDataEquipmentList>();// new ACTDataSkillList();
            this.EquipmentList.SetEquipmentDict(data.EquipmentDict);
            this.EquipmentFXList = UnityEngine.ScriptableObject.CreateInstance<ACTDataEquipmentFXList>();// new ACTDataSkillList();
            this.EquipmentFXList.SetEquipmentFXDict(data.EquipmentFXDict);
        }
    }
}
