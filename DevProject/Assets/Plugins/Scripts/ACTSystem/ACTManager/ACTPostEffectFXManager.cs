﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    public class ACTPostEffectFXManager
    {
        //public static Type TypeofProxy { private get; set; }

        static ACTPostEffectFXManager s_instance;
        static public ACTPostEffectFXManager GetInstance()
        { 
            if (s_instance == null)
            {
                s_instance = new ACTPostEffectFXManager();
            }
            return s_instance;
        }

        //PostEffectProxyBase _postEffectProxy;
        //PostEffectProxyBase GetPostEffectProxy()
        //{
          //  if (_postEffectProxy == null)
            //{
              //  GameObject go = Camera.main.gameObject;
                //_postEffectProxy = go.GetComponent(TypeofProxy) as PostEffectProxyBase;
                //if (_postEffectProxy == null) {
                  //  _postEffectProxy = go.AddComponent(TypeofProxy) as PostEffectProxyBase;
                //}
            //}
            //return _postEffectProxy;
        //}

        public void PlayACTEventVisualFX(ACTEventPostEffectFX data)
        {
            switch(data.EffectType)
            {   
                case ACTPostEffectType.DOF:
                    PlayDOF(data);
                    break;
                case ACTPostEffectType.GodRay:
                    PlayGodRay(data);
                    break;
                case ACTPostEffectType.HdrBloom:
                    PlayHdrBloom(data);
                    break;
                case ACTPostEffectType.RadialBlur:
                    PlayRadialBlur(data);
                    break;
                case ACTPostEffectType.ScreenWhite:
                    PlayScreenWhite(data);
                    break;
            }
        }

        public void PlayDOF(ACTEventPostEffectFX data)
        {
            DofParameter StartParameter = new DofParameter();
            StartParameter.FocalDistance = data.StartFloatArray[0];
            StartParameter.OffsetDistance = data.StartFloatArray[1];
            StartParameter.DepthLevel = (int)data.StartFloatArray[2];
            DofParameter EndParameter = new DofParameter();
            EndParameter.FocalDistance = data.EndFloatArray[0];
            EndParameter.OffsetDistance = data.EndFloatArray[1];
            EndParameter.DepthLevel = (int)data.EndFloatArray[2];
           // GetPostEffectProxy().PlayDOF(StartParameter, EndParameter, data.Duration);
            PostEffectHandlerBase.Instance.PlayDOF(Camera.main, StartParameter, EndParameter, data.Duration);
        }

        public void PlayGodRay(ACTEventPostEffectFX data)
        {
            GodRayParameter StartParameter = new GodRayParameter();
            float[] floatArray = data.StartFloatArray;
            StartParameter.SunColor = new Color(floatArray[0], floatArray[1], floatArray[2], floatArray[3]);
            StartParameter.Density = floatArray[4];
            StartParameter.Decay = data.StartFloatArray[5];
            StartParameter.Exposure = floatArray[6];
            StartParameter.Alpha = data.StartFloatArray[7];
            GodRayParameter EndParameter = new GodRayParameter();
            floatArray = data.EndFloatArray;
            EndParameter.SunColor = new Color(floatArray[0], floatArray[1], floatArray[2], floatArray[3]);
            EndParameter.Density = floatArray[4];
            EndParameter.Decay = data.StartFloatArray[5];
            EndParameter.Exposure = floatArray[6];
            EndParameter.Alpha = data.StartFloatArray[7];
            //GetPostEffectProxy().PlayGodRay(StartParameter, EndParameter, data.Duration);
            PostEffectHandlerBase.Instance.PlayGodRay(Camera.main, StartParameter, EndParameter, data.Duration);
        }

        public void PlayHdrBloom(ACTEventPostEffectFX data)
        {
            HdrBloomParameter StartParameter = new HdrBloomParameter();
            float[] floatArray = data.StartFloatArray;
            StartParameter.Drak = floatArray[0];
            StartParameter.Alpha = floatArray[1];
            StartParameter.Brightness = floatArray[2];
            StartParameter.BlurColor = new Color(floatArray[3], floatArray[4], floatArray[5], floatArray[6]);
            HdrBloomParameter EndParameter = new HdrBloomParameter();
            floatArray = data.EndFloatArray;
            EndParameter.Drak = floatArray[0];
            EndParameter.Alpha = floatArray[1];
            EndParameter.Brightness = floatArray[2];
            EndParameter.BlurColor = new Color(floatArray[3], floatArray[4], floatArray[5], floatArray[6]);
            //GetPostEffectProxy().PlayHdrBloom(StartParameter, EndParameter, data.Duration);
            PostEffectHandlerBase.Instance.PlayHdrBloom(Camera.main, StartParameter, EndParameter, data.Duration);
        }

        public void PlayRadialBlur(ACTEventPostEffectFX data)
        {
            RadiaBlurParameter StartParameter = new RadiaBlurParameter();
            StartParameter.SamplerDist = data.StartFloatArray[0];
            StartParameter.SamplerStrength = data.StartFloatArray[1];
            RadiaBlurParameter EndParameter = new RadiaBlurParameter();
            EndParameter.SamplerDist = data.EndFloatArray[0];
            EndParameter.SamplerStrength = data.EndFloatArray[1];
            //GetPostEffectProxy().PlayRadialBlur(StartParameter, EndParameter, data.Duration);
            PostEffectHandlerBase.Instance.PlayRadialBlur(Camera.main, StartParameter, EndParameter, data.Duration);
        }

        public void PlayScreenWhite(ACTEventPostEffectFX data)
        {
            ScreenWhiteParameter StartParameter = new ScreenWhiteParameter();
            StartParameter.Alpha = data.StartFloatArray[0];
            ScreenWhiteParameter EndParameter = new ScreenWhiteParameter();
            EndParameter.Alpha = data.EndFloatArray[0];
            //GetPostEffectProxy().PlayScreenWhite(StartParameter, EndParameter, data.Duration);
            PostEffectHandlerBase.Instance.PlayScreenWhite(Camera.main, StartParameter, EndParameter, data.Duration);
        }
    }
}
