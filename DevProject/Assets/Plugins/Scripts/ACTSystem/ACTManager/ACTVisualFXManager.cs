﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    public class ACTVisualFXManager 
    {
        static int ACTIVE_LIMIT_PER_FRAME = 2;//每贞激活特效数量限制

        static ACTVisualFXManager s_instance;
        static public ACTVisualFXManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new ACTVisualFXManager();
            }
            return s_instance;
        }

        static public Dictionary<VisualFXQuality, int> qualityVisualFXLimits
            = new Dictionary<VisualFXQuality, int>() { 
                                                { VisualFXQuality.H, 30 }, 
                                                { VisualFXQuality.M, 20 }, 
                                                { VisualFXQuality.L, 10 }
            };

        private static uint _guidPoint = 0;

        private static T Create<T>() where T : ACTVisualFx, new()
        {
            T result = new T();
            result.Reset();
            result.guid = ++_guidPoint;
            return result;
        }

        private static ACTVisualFx CreateACTVisualFx()
        {
            return Create<ACTVisualFx>();
        }

        private static ACTVisualFxTracer CreateACTVisualFxTracer()
        {
            return Create<ACTVisualFxTracer>();
        }

        private static ACTVisualFxLaser CreateACTVisualFxLaser()
        {
            return Create<ACTVisualFxLaser>();
        }

        private static ACTVisualFxBullet CreateACTVisualFxBullet()
        {
            return Create<ACTVisualFxBullet>();
        }

        private Dictionary<uint, ACTVisualFx> _visualFXDic = new Dictionary<uint, ACTVisualFx>();
        private List<ACTVisualFx> _visualFXList = new List<ACTVisualFx>();
        private ACTVisualFXManager()
        {
        }

        public void OnUpdate()
        {
            int activeLimit = ACTIVE_LIMIT_PER_FRAME;
            for (int i = _visualFXList.Count - 1; i >= 0; i--)
            {
                var visualFX = _visualFXList[i];
                if (visualFX.IsReady() && visualFX.priority != VisualFXPriority.H )
                {
                    activeLimit--;
                    if (activeLimit < 0)
                    {
                        continue;
                    }
                }
                var result = visualFX.Update();
                if (result == ACTVisualFXPlayState.ENDED)
                {
                    _visualFXList.RemoveAt(i);
                    _visualFXDic.Remove(visualFX.guid);
                }
            }
        }

        public void PlayVisualFX(ACTVisualFx visualFx)
        {
            _visualFXList.Add(visualFx);
            _visualFXDic[visualFx.guid] = visualFx;
            visualFx.Update();
        }

        public void StopVisualFX(uint guid)
        {
            if (_visualFXDic.ContainsKey(guid))
            {
                _visualFXDic[guid].Stop();
            }
        }

        public string TakePerfabName(ACTEventVisualFX data, int settingQuality = 0)
        {
            string result = string.Empty;
            if (data == null) return result;
            int qualityInt = (int)ACTSystemDriver.GetInstance().CurVisualQuality + settingQuality;
            if (qualityInt < 0) qualityInt = 0;
            if (qualityInt > (int)VisualFXQuality.H) qualityInt = (int)VisualFXQuality.H;
            VisualFXQuality quality = (VisualFXQuality)qualityInt;
            switch (quality)
            {
                case VisualFXQuality.H: result = data.Perfabs_H_Name; break;
                case VisualFXQuality.M: result = data.Perfabs_M_Name; break;
                case VisualFXQuality.L: result = data.Perfabs_L_Name; break;
                default: result = data.Perfabs_L_Name; break;
            }
            return result;
        }

        public string TakePerfabName(ACTDataVisualFX data, int settingQuality = 0)
        {
            string result = string.Empty;
            if (data == null) return result;
            int qualityInt = (int)ACTSystemDriver.GetInstance().CurVisualQuality + settingQuality;
            if (qualityInt < 0) qualityInt = 0;
            if (qualityInt > (int)VisualFXQuality.H) qualityInt = (int)VisualFXQuality.H;
            VisualFXQuality quality = (VisualFXQuality)qualityInt;
            switch (quality)
            {
                case VisualFXQuality.H: result = data.Perfabs_H_Name; break;
                case VisualFXQuality.M: result = data.Perfabs_M_Name; break;
                case VisualFXQuality.L: result = data.Perfabs_L_Name; break;
                default: result = data.Perfabs_L_Name; break;
            }
            //if (result == string.Empty) Debug.LogWarning("[TakePerfabName]=>@美术缺少品质[" + ACTSystemDriver.GetInstance().CurVisualQuality + "]的特效资源");
            return result;
        }

        bool GrantAuthorization(VisualFXPriority priority)
        {
            if (priority == VisualFXPriority.H)return true;
            var limit = qualityVisualFXLimits[ACTSystemDriver.GetInstance().CurVisualQuality];
            if (limit <= _visualFXList.Count) return false;
            return true;
        }

        #region 普通特效
        public uint PlayACTEventVisualFX(ACTEventVisualFX data, ACTActor actor, Vector3 location, int settingQuality = 0, VisualFXPriority priority = VisualFXPriority.H, ACTActor locationActor = null)
        {
            if (actor.isHideFX) return 0;
            if (!GrantAuthorization(priority)) return 0;
            uint id = 0;
            if (!string.IsNullOrEmpty(data.Slot))
            {
                string[] splitSlots =data.Slot.Split(',');
                for (int i = 0; i < splitSlots.Length; i++)
                {
                    id = PlayACTEventVisualFXSingle(data, actor, location, splitSlots[i], settingQuality, priority, locationActor);
                }
                return id;
            }
            return PlayACTEventVisualFXSingle(data, actor, location, data.Slot, settingQuality, priority, locationActor);
        }

        public uint PlayACTEventVisualFXSingle(ACTEventVisualFX data, ACTActor actor, Vector3 location, string slot, int settingQuality = 0, VisualFXPriority priority = VisualFXPriority.H, ACTActor locationActor = null)
        {
            if (!GrantAuthorization(priority)) return 0;
            var visualFXObj = CreateACTVisualFx();
            if (visualFXObj == null) return 0;
            visualFXObj.priority = priority;
            visualFXObj.duration = data.Duration;
            visualFXObj.sortOrder = actor.fxSortingOrder;
            visualFXObj.perfabName = TakePerfabName(data, settingQuality);
            visualFXObj.disappearDelay = data.DisappearDelay;
            ACTVisualFxLocator.InitVisualFXLocation(visualFXObj, actor, location, data.LocationType, slot, data.LocationReal, data.RocationReal, locationActor, data.SkinSlot);
            PlayVisualFX(visualFXObj);
            return visualFXObj.guid;
        }

        public uint PlayACTVisualFX(int visualFXID, ACTActor caster,
            float duration = -1, int settingQuality = 0, VisualFXPriority priority = VisualFXPriority.H)
        {
            if (!GrantAuthorization(priority)) return 0;
            var data = ACTRunTimeData.GetVisualFXData(visualFXID);
			if (data == null)
			{
				return 0;
			}
            var visualFXObj = CreateACTVisualFx();
            if (visualFXObj == null) return 0;
            visualFXObj.priority = priority;
            visualFXObj.duration = duration > 0 ? duration : data.Duration;
            visualFXObj.perfabName = TakePerfabName(data, settingQuality);
            visualFXObj.sortOrder = caster.fxSortingOrder;
            ACTVisualFxLocator.InitVisualFXLocation(visualFXObj, caster, Vector3.zero, data.LocationType, data.Slot, data.LocationReal,null,null,data.SkinSlot);
            PlayVisualFX(visualFXObj);
            return visualFXObj.guid;
        }

        public uint PlayACTVisualFX(int visualFXID, Vector3 position,
            float duration = -1, int settingQuality = 0, VisualFXPriority priority = VisualFXPriority.H)
        {
            if (!GrantAuthorization(priority)) return 0;
            var data = ACTRunTimeData.GetVisualFXData(visualFXID);
            var visualFXObj = CreateACTVisualFx();
            if (visualFXObj == null) return 0;
            visualFXObj.priority = priority;
            visualFXObj.duration = duration > 0 ? duration : data.Duration;
            visualFXObj.perfabName = TakePerfabName(data, settingQuality);
            ACTVisualFxLocator.InitVisualFXLocation(visualFXObj, null, position, ACTVFXLocationType.CUSTOM_WORLD, data.Slot, data.LocationReal,null,null,data.SkinSlot);
            PlayVisualFX(visualFXObj);
            return visualFXObj.guid;
        }

        public uint PlayACTVisualFX(string perfabName, Vector3 position, Vector3 localEulerAngles,
            float duration = -1, int settingQuality = 0, VisualFXPriority priority = VisualFXPriority.H, int sortOrder = -1)
        {
            if (!GrantAuthorization(priority)) return 0;
            var visualFXObj = CreateACTVisualFx();
            if (visualFXObj == null) return 0;
            visualFXObj.priority = priority;
            visualFXObj.duration = duration;
            visualFXObj.perfabName = perfabName;
            visualFXObj.sortOrder = sortOrder;
            visualFXObj.localRotation = Quaternion.Euler(localEulerAngles);
            ACTVisualFxLocator.InitVisualFXLocation(visualFXObj, null, position, ACTVFXLocationType.CUSTOM_WORLD, string.Empty,null,null,null,null);
            PlayVisualFX(visualFXObj);
            return visualFXObj.guid;
        }


        #endregion

        #region 追踪特效
        public void PlayACTTracer(int visualFXID, float speed, 
            ACTActor caster, ACTVector3 offset, ACTActor target,
            Action action = null, int settingQuality = 0, VisualFXPriority priority = VisualFXPriority.H)
        {
            if (!GrantAuthorization(priority)) return;
            var data = ACTRunTimeData.GetVisualFXData(visualFXID);
            var visualFXObj = CreateACTVisualFxTracer();
            if (visualFXObj == null) return;
            visualFXObj.priority = priority;
            visualFXObj.duration = data.Duration;
            visualFXObj.speed = speed;
            visualFXObj.target = target.transform;
            visualFXObj.callback = action;
            visualFXObj.targetOffset.y = offset.y;
            visualFXObj.disappearDelay = data.DisappearDelay == 0 ? 2 : data.DisappearDelay;
            visualFXObj.perfabName = TakePerfabName(data, settingQuality);
            ACTVisualFxLocator.InitVisualFXLocation(visualFXObj, caster, Vector3.zero, data.LocationType, data.Slot, offset,null,null,data.SkinSlot);
            PlayVisualFX(visualFXObj);
        }

        public void PlayACTTracer(ACTEventTracerVisualFX data, 
            ACTActor caster, ACTActor target,
            Action action = null, int settingQuality = 0, VisualFXPriority priority = VisualFXPriority.H)
        {
            if (!GrantAuthorization(priority)) return;
            for (int i = 0; i < data.TracerList.Count; i++)
            {
                var tracerData = data.TracerList[i];
                PlayACTTracer(tracerData.VisualFXID, tracerData.Speed, caster, 
                    tracerData.LocationReal, target, i == 0 ? action : null, settingQuality, priority);
            }
        }

        public void PlayACTTracer(int visualFXID, float speed, Vector3 srcPosition, 
            ACTActor target, Vector3 targetOffset,
            Action action = null, int settingQuality = 0, VisualFXPriority priority = VisualFXPriority.H)
        {
            if (!GrantAuthorization(priority)) return;
            var data = ACTRunTimeData.GetVisualFXData(visualFXID);
            var visualFXObj = CreateACTVisualFxTracer();
            if (visualFXObj == null) return;
            visualFXObj.priority = priority;
            visualFXObj.duration = data.Duration;
            visualFXObj.speed = speed;
            visualFXObj.target = target.transform;
            visualFXObj.callback = action;
            visualFXObj.targetOffset = targetOffset;
            visualFXObj.disappearDelay = data.DisappearDelay == 0 ? 2 : data.DisappearDelay;
            visualFXObj.perfabName = TakePerfabName(data, settingQuality);
            ACTVisualFxLocator.InitVisualFXLocation(visualFXObj, null, srcPosition, ACTVFXLocationType.CUSTOM_WORLD, data.Slot,null,null,null,data.SkinSlot);
            PlayVisualFX(visualFXObj);
        }
        #endregion

        #region 激光特效
        public uint PlayACTLaser(ACTLaser actLaser, ACTEventLaserVisualFX laserData, 
            ACTActor caster, ACTActor target,
            Action action = null, int settingQuality = 0, VisualFXPriority priority = VisualFXPriority.H)
        {
            if (!GrantAuthorization(priority)) return 0;
            ACTDataVisualFX data = ACTRunTimeData.GetVisualFXData(actLaser.VisualFXID);
            ACTVisualFxLaser visualFXObj = CreateACTVisualFxLaser();
            if (visualFXObj == null) return 0;
            if (data == null)
            {
                ACTLogger.Error("[ACTVisualFXManager:PlayACTLaser]=>4____data == null ,visualFXID:  " + actLaser.VisualFXID);
            }
            visualFXObj.priority = priority;
            visualFXObj.duration = laserData.Duration;//激光特效持续时间
            //ACTSystemDebug.LogError("[ACTVisualFXManager:PlayACTLaser]=>4____data == null ,visualFXID:  " + _actLaser.VisualFXID + "visualFXObj.duration:   " + visualFXObj.duration);
            //
            visualFXObj.SetCaster(caster);
            visualFXObj.SetTarget(target) ;
            visualFXObj.startSlotName = laserData.startSlotName;
            visualFXObj.endSlotName = laserData.endSlotName;
            visualFXObj.startOffset = laserData.startPositionReal;
            visualFXObj.endOffset = laserData.endPositionReal;
            visualFXObj.LocationType = laserData.LocationType;
            visualFXObj.callback = action;
            visualFXObj.perfabName = TakePerfabName(data, settingQuality);
            data.LocationType = ACTVFXLocationType.CUSTOM_WORLD;

            ACTVisualFxLocator.InitVisualFXLocation(visualFXObj, caster, caster.transform.position, data.LocationType, data.Slot,null,null,null,data.SkinSlot);
            PlayVisualFX(visualFXObj);
            //
            PlayAssistLaser(ref visualFXObj.startVisualFx, actLaser.startFXID, target.gameObject.transform, 
                visualFXObj.duration, settingQuality);
            PlayAssistLaser(ref visualFXObj.endVisualFx, actLaser.endFXID, caster.gameObject.transform,
                visualFXObj.duration, settingQuality);
            return visualFXObj.guid;
        }

        public uint PlayACTLaser(ACTEventLaserVisualFX data, 
            ACTActor caster, ACTActor target,
            Action action = null, int settingQuality = 0, VisualFXPriority priority = VisualFXPriority.H)
        {
            if (!GrantAuthorization(priority)) return 0;
            uint guid = 0;
            for (int i = 0; i < data.LaserList.Count; i++)
            {
                var laserData = data.LaserList[i];
                guid = PlayACTLaser(laserData, data, caster, target, i == 0 ? action : null, settingQuality, priority);    
            }
            //目前LaserList中只能有一个数据，所以直接记录guid用来返回
            return guid;
        }

        private uint PlayAssistLaser(ref ACTVisualFxAssist assistFXObj,
            int visualFXID, Transform target, float duration, int settingQuality = 0, VisualFXPriority priority = VisualFXPriority.H)
        {
            if (!GrantAuthorization(priority)) return 0;
            if (visualFXID == 0) return 0;
            var data = ACTRunTimeData.GetVisualFXData(visualFXID);
            //assistFXObj = CreateACTVisualFx();
            assistFXObj = new ACTVisualFxAssist();
            assistFXObj.targetPos = target;
            assistFXObj.priority = priority;
            assistFXObj.duration = duration;
            assistFXObj.perfabName = TakePerfabName(data, settingQuality);
            PlayVisualFX(assistFXObj);
            return assistFXObj.guid;
        }

        #endregion

        #region 子弹特效
        public void PlayACTBullet(int visualFXID, float speed, 
            ACTActor caster, ACTVector3 offset, Vector3 targetLocation, float acceleration,
            ACTCalcTargetLocation calcTargetLocation, Action action = null, int settingQuality = 0, VisualFXPriority priority = VisualFXPriority.H)
        {
            if (!GrantAuthorization(priority)) return;
            var data = ACTRunTimeData.GetVisualFXData(visualFXID);
            var visualFXObj = CreateACTVisualFxBullet();
            if (visualFXObj == null) return;
            visualFXObj.priority = priority;
            visualFXObj.duration = data.Duration;
            visualFXObj.speed = speed;
            visualFXObj.targetPosition = targetLocation;
            visualFXObj.acceleration = -acceleration;
            visualFXObj.callback = action;
            visualFXObj.calcTargetLocation = calcTargetLocation;
            visualFXObj.perfabName = TakePerfabName(data, settingQuality);
            visualFXObj.disappearDelay = data.DisappearDelay == 0 ? 2 : data.DisappearDelay;
            ACTVisualFxLocator.InitVisualFXLocation(visualFXObj, caster, Vector3.zero, data.LocationType, data.Slot, offset,null,null,data.SkinSlot);
            PlayVisualFX(visualFXObj);
        }

        public void PlayACTBullet(ACTEventBulletVisualFX data, ACTActor caster,
            Vector3 targetLocation, Action action = null, int settingQuality = 0, VisualFXPriority priority = VisualFXPriority.H)
        {
            if (!GrantAuthorization(priority)) return;
            for (int i = 0; i < data.BulletList.Count; i++)
            {
                var tracerData = data.BulletList[i];
                PlayACTBullet(tracerData.VisualFXID, tracerData.Speed, 
                    caster, tracerData.LocationReal, targetLocation, tracerData.Acceleration, 
                    data.targetLocation, i == 0 ? action : null, settingQuality, priority);
            }
        }

        public void PlayACTBullet(int visualFXID, float speed, 
            Vector3 startPosition, Vector3 targetPosition, float acceleration,
            Action action = null, int settingQuality = 0, VisualFXPriority priority = VisualFXPriority.H)
        {
            if (!GrantAuthorization(priority)) return;
            var data = ACTRunTimeData.GetVisualFXData(visualFXID);
            var visualFXObj = CreateACTVisualFxBullet();
            if (visualFXObj == null) return;
            visualFXObj.priority = priority;
            visualFXObj.duration = data.Duration;
            visualFXObj.speed = speed;
            visualFXObj.targetPosition = targetPosition;
            visualFXObj.acceleration = -acceleration;
            visualFXObj.callback = action;
            visualFXObj.perfabName = TakePerfabName(data, settingQuality);
            visualFXObj.localPosition = startPosition;
            visualFXObj.disappearDelay = data.DisappearDelay == 0 ? 2 : data.DisappearDelay;
            PlayVisualFX(visualFXObj);
        }
        #endregion
        #region 脚步特效
        public uint PlayFootVisualFX(int visualFXID, ACTActor caster, int footIndex,
    float duration = -1, int settingQuality = 0, VisualFXPriority priority = VisualFXPriority.H)
        {
            if (!GrantAuthorization(priority)) return 0;
            var data = ACTRunTimeData.GetVisualFXData(visualFXID);
            if (data == null)
            {
                return 0;
            }
            var visualFXObj = CreateACTVisualFx();
            if (visualFXObj == null) return 0;
            visualFXObj.priority = priority;
            visualFXObj.duration = duration > 0 ? duration : data.Duration;
            visualFXObj.perfabName = TakePerfabName(data, settingQuality);
            ACTVisualFxLocator.InitVisualFXLocation(visualFXObj, caster, Vector3.zero, data.LocationType, caster.actorData.FootTransList[footIndex], data.LocationReal, null, null, data.SkinSlot);
            PlayVisualFX(visualFXObj);
            return visualFXObj.guid;
        }
        #endregion

        public void ReleaseACTVisualFx()
        {
            for (int i = _visualFXList.Count - 1; i >= 0; i--)
            {
                var visualFX = _visualFXList[i];
                var result = visualFX.Update();
                if (result == ACTVisualFXPlayState.PLAYING)
                {
                    visualFX.Stop(true);
                    _visualFXList.RemoveAt(i);
                    _visualFXDic.Remove(visualFX.guid);
                }
            }
        }
    }
}
