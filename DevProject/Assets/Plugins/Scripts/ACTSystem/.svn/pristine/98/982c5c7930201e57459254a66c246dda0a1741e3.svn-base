﻿using UnityEngine;

/// <summary>
/// Event Hook class lets you easily add remote event listener functions to an object.
/// Example usage: ACTEventListener.Get(gameObject).onClick += MyClickFunction;
/// </summary>

namespace ACTSystem
{

    [AddComponentMenu("ACT/Internal/Event Listener")]
    public class ACTEventListener : MonoBehaviour
    {
        public delegate void VoidDelegate(GameObject go);
        public delegate void BoolDelegate(GameObject go, bool value);
        public delegate void FloatDelegate(GameObject go, float value);
        public delegate void VectorDelegate(GameObject go, Vector2 value);
        public delegate void StringDelegate(GameObject go, string value);
        public delegate void ObjectDelegate(GameObject go, GameObject value);

        public delegate void CollisionDelegate(GameObject go, Collision value);
        public delegate void ColliderDelegate(GameObject go, Collider value);
        public delegate void ControllerColliderHitDelegate(GameObject go, ControllerColliderHit value);


        public object parameter;

        public VoidDelegate onUpdate;
        public VoidDelegate onDestroy;
        public VoidDelegate onBecameVisible;
        public VoidDelegate onBecameInvisible;

        void Update() { if (onUpdate != null) onUpdate(gameObject); }
        void OnDestroy() { if (onDestroy != null) onDestroy(gameObject); }
        void OnBecameVisible() { if (onBecameVisible != null) onBecameVisible(gameObject); }
        void OnBecameInvisible() { if (onBecameInvisible != null) onBecameInvisible(gameObject); }

        static public ACTEventListener Get(GameObject go)
        {
            ACTEventListener listener = go.GetComponent<ACTEventListener>();
            if (listener == null) listener = go.AddComponent<ACTEventListener>();
            return listener;
        }
    }
}