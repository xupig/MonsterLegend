﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using ShaderUtils;
using ACTSystem;

// 残影效果  
public class ACTGhostShadow : MonoBehaviour
{
    class AfterImage
    {
        public Mesh mesh;
        public Material material;
        public Matrix4x4 matrix;
        public float showStartTime;
        public float duration;  // 残影镜像存在时间  
        public float alpha;
        public Vector3 positon;
        public bool needRemove = false;
    }

    private float _duration; // 残影特效持续时间  
    private float _interval; // 间隔  
    private float _fadeTime; // 淡出时间  
    private float _minDistance;//最小距离
    private List<AfterImage> _imageList = new List<AfterImage>();
    private Shader _shaderAfterImage;

    public ACTActor actor = null;
    public Color _xrayColor = new Color(1,1,1,1);
    public float _rim = 1.2f;
    public float _Inside = 0f;

    SkinnedMeshRenderer[] _renderers;
    Material[] _materials;
    Mesh[] _bakeMesh;
    Dictionary<int, Mesh[]> _bakeMeshs;

    private Transform _transform;

    void Awake()
    {
        _shaderAfterImage = ShaderRuntime.loader.Find("XGame/Mobile-XrayEffect");
        _renderers = GetComponentsInChildren<SkinnedMeshRenderer>();
        _materials = new Material[_renderers.Length];
        _bakeMeshs = new Dictionary<int, Mesh[]>();
        for (int i = 0; i < _renderers.Length; ++i)
        {
            var item = _renderers[i];
            var mat = new Material(item.sharedMaterial);
            mat.shader = _shaderAfterImage;
            mat.SetColor("_Color", _xrayColor);
            mat.SetFloat("_Inside", _Inside);
            mat.SetFloat("_Rim", _rim);
            _materials[i] = mat;
        }
        _transform = transform;
        return;
        var actor = _transform.parent.GetComponent<ACTActor>();
        if (actor)
        {
            var bm = _transform;
            var bl = actor.boneController.GetBoneByName("bip_localmotion");
            if (bm != null && bl != null)
            {
                var gm = new GameObject("ghostshadow_location").transform;
                gm.SetParent(bm, false);
                gm.SetParent(bl, true);
                _transform = gm;
            }
        }
    }
    //总时间、间隔时间，单个存在时间，最小间隔距离
    public void Play(float duration, float interval, float fadeout, float distance, int uid)
    {
        _duration = duration;
        _interval = interval;
        _fadeTime = fadeout;
        _minDistance = distance;
        if (!_bakeMeshs.ContainsKey(uid))
        {
            _bakeMesh = new Mesh[_renderers.Length];
            for (int i = 0; i < _renderers.Length; ++i)
            {
                var item = _renderers[i];
                var mesh = new Mesh();
                mesh.name = "ghostshadow_" + uid;
                item.BakeMesh(mesh);
                _bakeMesh[i] = mesh;
            }
            _bakeMeshs[uid] = _bakeMesh;
        }
        else {
            _bakeMesh = _bakeMeshs[uid];
        }
        StartCoroutine(DoAddImage());
    }

    IEnumerator DoAddImage()
    {
        float startTime = UnityPropUtils.realtimeSinceStartup;
        while (true)
        {
            if (UnityPropUtils.realtimeSinceStartup - startTime > _duration)
            {
                break;
            }

            CreateImage();

            yield return new WaitForSeconds(_interval);
        }
    }

    private void CreateImage()
    {
        if (_imageList.Count > 0 && Vector3.Distance(_transform.position, _imageList[_imageList.Count - 1].positon) < _minDistance)
            return;


        Transform t = _transform;
        Matrix4x4 matrix = t.localToWorldMatrix;
        Vector4 vz = matrix.GetColumn(2);
        Vector3 forward = new Vector3(vz.x, vz.y, vz.z);
        Vector4 vy = matrix.GetColumn(1);
        Vector3 upwards = new Vector3(vy.x, vy.y, vy.z);
        var r = Quaternion.LookRotation(forward, upwards);
        var p = new Vector3(matrix.m03, matrix.m13, matrix.m23);
        matrix = Matrix4x4.TRS(p, r, Vector3.one);
        Material mat = null;
        for (int i = 0; i < _renderers.Length; ++i)
        {
            var item = _renderers[i];
            t = item.transform;
            mat = _materials[i];
            _imageList.Add(new AfterImage
            {
                mesh = _bakeMesh[i],
                material = mat,
                matrix = matrix,
                showStartTime = UnityPropUtils.realtimeSinceStartup,
                duration = _fadeTime,
                positon = t.position
            });
        }
    }

    private void DrawMesh(Mesh trailMesh, Material trailMaterial)
    {
        Graphics.DrawMesh(trailMesh, Matrix4x4.identity, trailMaterial, gameObject.layer);
    }

    void Update()
    {

    }

    void LateUpdate()
    {
        bool hasRemove = false;
        foreach (var item in _imageList)
        {
            float time = UnityPropUtils.realtimeSinceStartup - item.showStartTime;
            if (time > item.duration)
            {
                item.needRemove = true;
                hasRemove = true;
                continue;
            }

            if (item.material.HasProperty("_Color"))
            {
                item.alpha = Mathf.Max(0, 1 - time / item.duration);
                Color color = _xrayColor;
                //color.a = item.alpha;
                item.material.SetColor("_Color", color);
                item.material.SetFloat("_Inside", _Inside * item.alpha);
                item.material.SetFloat("_Rim", _rim * item.alpha);
            }
            Graphics.DrawMesh(item.mesh, item.matrix, item.material, gameObject.layer);
        }

        if (hasRemove)
        {
            _imageList.RemoveAll(x => x.needRemove);
        }
    }
}  


