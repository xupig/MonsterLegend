﻿using System.Collections.Generic;
using System.Text;
using System;
using UnityEngine;

namespace ACTSystem
{
    public class ACTVisualFxBullet : ACTVisualFx
    {
        public Vector3 targetPosition;

        public float speed;

        public Action callback;

        public ACTCalcTargetLocation calcTargetLocation = ACTCalcTargetLocation.Auto;

        public bool arrived = false;

        public float acceleration;
        public float sideAngle = 0;
        private float _time = 0;
        private float _ySpeedInit = 0;
        private float _yPositionInit = 0;
        private float _horizontalSpeed = 0;
        private Matrix4x4 _matrixInit;

        public ACTVisualFxBullet()
        {
            fxType = ACTVisualFXType.TYPE_BULLET;
        }

        override protected void OnPlay()
        {
            switch (calcTargetLocation)
            {
                case ACTCalcTargetLocation.Custom:
                    InitInCustom();
                    break;
                case ACTCalcTargetLocation.Auto:
                default:
                    InitInAuto();
                    break;
            }
        }

        private void InitInAuto()
        {
            Vector3 horizontalDirection = GetHorizontalDirection();
            float sx = horizontalDirection.magnitude;
            _horizontalSpeed = speed;
            float duration = sx / _horizontalSpeed;
            float yDistance = targetPosition.y - localPosition.y;
            _ySpeedInit = yDistance / duration - 0.5f * acceleration * duration;
            _yPositionInit = localPosition.y;
            _matrixInit = Matrix4x4.TRS(Vector3.zero, Quaternion.LookRotation(horizontalDirection), Vector3.one);
            _matrixInit = _matrixInit * Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, 0, sideAngle), Vector3.one);
        }

        private void InitInCustom()
        {
            Vector3 direction = targetPosition - localPosition;
            float distance = direction.magnitude;
            float duration = distance / speed;
            float yDistance = targetPosition.y - localPosition.y;
            _ySpeedInit = yDistance / duration - 0.5f * acceleration * duration;
            _yPositionInit = localPosition.y;
            float radian = Mathf.Asin(ACTMathUtils.Abs(yDistance) / distance);
            _horizontalSpeed = speed * Mathf.Cos(radian);
            _matrixInit = Matrix4x4.TRS(localPosition, Quaternion.LookRotation(direction), Vector3.one);
            _matrixInit = _matrixInit * Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, 0, sideAngle), Vector3.one);
        }

        private Vector3 GetHorizontalDirection()
        {
            return new Vector3(targetPosition.x, localPosition.y, targetPosition.z) - localPosition;
        }

        override protected void OnReset()
        {
            arrived = false;
            _time = 0;
        }

        override protected void OnUpdate()
        {
            Approaching();
        }

        void Approaching()
        {
            if (arrived) return;
            Vector3 horizontalDirection = GetHorizontalDirection();
            float distance = horizontalDirection.magnitude;
            float frameSpeed = _horizontalSpeed * UnityPropUtils.deltaTime;

            if (distance <= frameSpeed)
            {
                Arrive();
            }
            else
            {
                _time += UnityPropUtils.deltaTime;
                Vector3 newLocalPosition = Vector3.zero;
                newLocalPosition = localPosition + frameSpeed * horizontalDirection.normalized;
                newLocalPosition.y = _yPositionInit; 
                var sideOffset =_ySpeedInit * _time + 0.5f * acceleration * _time * _time;
                if (_transform != null)
                {
                    Matrix4x4 m = _matrixInit;
                    m = m * Matrix4x4.TRS(new Vector3(0, sideOffset, 0), Quaternion.identity, Vector3.one);
                    newLocalPosition = newLocalPosition + new Vector3(m.m03, m.m13, m.m23);
                    _transform.LookAt(newLocalPosition);
                }
                localPosition = newLocalPosition;
            }
        }

        private void Arrive()
        {
            localPosition = targetPosition;
            arrived = true;
            if (callback != null)
            {
                callback();
                callback = null;
            }
            Stop();
        }

        override protected void OnStop()
        {

        }
    }
}
