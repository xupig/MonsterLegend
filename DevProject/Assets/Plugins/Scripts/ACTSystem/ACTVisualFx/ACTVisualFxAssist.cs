﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ACTSystem
{
    public class ACTVisualFxAssist : ACTVisualFx
    {
        public Transform targetPos;
        protected override void OnUpdate()
        {
            if (_assetObject == null || _assetObject.gameObject == null || targetPos == null) return;

            //直接忽略高度
            _assetObject.gameObject.transform.LookAt(new Vector3(targetPos.position.x, _assetObject.gameObject.transform.position.y, targetPos.position.z));
        }
    }
}
