﻿using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    public enum ACTVisualFXPlayState
    {
        READY,
        PLAYING,
        DISAPPEARING,
        ENDED
    }

    public enum ACTVisualFXType
    { 
        TYPE_NORMAL,
        TYPE_TRACER,
        TYPE_BULLET,
        TYPE_LASER
    }
}
