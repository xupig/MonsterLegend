﻿using UnityEngine;
using System.Collections;
using System;

namespace ACTSystem
{
    public class ACTMaterialAnim : MonoBehaviour
    {
        public delegate void VoidDelegate();

        string _lastMaterialPath;
        string _curMaterialPath;
        bool _isLoading = false;
        bool _isPlaying = false;
        VoidDelegate _loadedCallback;
        Material _originMaterial;
        Material _currentMaterial;

        ACTEventChangeMaterial _changeMaterialData;
        Renderer _render;
        float _startTime = 0;

        void Awake()
        {
            _render = GetComponent<Renderer>();
            if (_render == null)
            {
                _render = GetComponentInChildren<Renderer>();
            }
        }

        public void Reset()
        {
            if (_originMaterial != null)
            {
                _render.material = _originMaterial;
                _originMaterial = null;
            }
            _changeMaterialData = null;
            _curMaterialPath = null;
            _lastMaterialPath = null;
            _changeMaterialData = null;
        }

        public void Play(string path, ACTEventChangeMaterial changeMaterialData)
        {
            if (changeMaterialData.MaterialID == 0)
            {
                Reset();
            }
            else
            {
                if (_originMaterial == null)
                {
                    _originMaterial = _render.material;
                }
                path = ACTSystemDriver.GetInstance().ObjectPool.assetLoader.GetResMapping(path);
                SetMaterial(path);
                _startTime = UnityPropUtils.realtimeSinceStartup;
                _changeMaterialData = changeMaterialData;
                ResetColors();
                _isPlaying = true;
            }
        }

        void SetMaterial(string path, VoidDelegate cb = null)
        {
            _lastMaterialPath = path;
            if (cb !=null)_loadedCallback += cb;
            LoadMaterial();
        }

        void LoadMaterial()
        {
            if (_isLoading) return;
            if (_lastMaterialPath == _curMaterialPath)
            {
                LoadedCallback();
                return;
            }
            if (string.IsNullOrEmpty(_lastMaterialPath)) return;
            var path = _lastMaterialPath;
            _isLoading = true;
            var gameObjectProxy = gameObject;
            ACTSystemDriver.GetInstance().ObjectPool.assetLoader.GetObject(path, (obj) =>
            {
                _isLoading = false;
                if (gameObjectProxy == null)
                {
                    ReleaseMaterial(path);
                    return;
                }
                if (path != _lastMaterialPath)
                {
                    ReleaseMaterial(path);
                    LoadMaterial();
                    return;
                }
                if (obj == null)
                {
                    Debug.LogError(path + " not found!!");
                    return;
                }
                ReleaseMaterial(_curMaterialPath);
                _curMaterialPath = _lastMaterialPath;
                var render = GetComponent<Renderer>();
                //武器换材质
                if (render == null)
                {
                    render = GetComponentInChildren<Renderer>();
                }
                render.material = obj as Material;
                _currentMaterial = render.material;
                LoadedCallback();
            });
        }

        void LoadedCallback()
        {
            if (_loadedCallback != null)
            {
                _loadedCallback();
                _loadedCallback = null;
            }
        }

        void ReleaseMaterial(string path)
        {
            if (string.IsNullOrEmpty(path)) return;
            ACTSystemDriver.GetInstance().ObjectPool.assetLoader.ReleaseAsset(path);
        }

        void OnDestroy()
        {
            ReleaseMaterial(_curMaterialPath);
        }

        void ResetColors()
        {
            var aniList = _changeMaterialData.MaterialAnimationList;
            for (int i = 0; i < aniList.Count; i++)
            {
                var data = aniList[i];
                if (data.ValueType == ACTMaterialAnimationType.Color)
                {
                    data.StartColorRuntime = new Color(data.StartColorReal.x, data.StartColorReal.y, data.StartColorReal.z, data.StartColorReal.w);
                    data.EndColorRuntime = new Color(data.EndColorReal.x, data.EndColorReal.y, data.EndColorReal.z, data.EndColorReal.w);
                }
            }
        }

        void Update()
        {
            if (_changeMaterialData == null) return;
            if (!_isPlaying) return;
            if (this._isLoading) return;
            var aniList = _changeMaterialData.MaterialAnimationList;
            var deltaTime = UnityPropUtils.realtimeSinceStartup - _startTime;
            bool flag = false;
            bool result = false;
            for (int i = 0; i < aniList.Count; i++)
            {
                var data = aniList[i];
                switch (data.ValueType)
                {
                    case ACTMaterialAnimationType.Float:
                        result = UpdateFloatValue(data, deltaTime);
                        break;
                    case ACTMaterialAnimationType.Color:
                        result = UpdateColorValue(data, deltaTime);
                        break;
                }
                if (result) flag = true;
            }
            if (!flag) _isPlaying = false;
        }

        bool UpdateFloatValue(ACTMaterialAnimation data, float deltaTime)
        {
            var duration = data.Duration;
            if (deltaTime >= duration)
            {
                _currentMaterial.SetFloat(data.ValueName, data.EndValue);
                return false;
            }
            else
            {
                //var startValue = data.StartValue;
                //var endValue = data.EndValue;
                //var speed = (endValue - startValue) / data.Duration;
                //var newValue = startValue + speed * deltaTime;
                var newValue = Mathf.Lerp(data.StartValue, data.EndValue, deltaTime / data.Duration);
                _currentMaterial.SetFloat(data.ValueName, newValue);
                return true;
            }
        }

        bool UpdateColorValue(ACTMaterialAnimation data, float deltaTime)
        {
            var duration = data.Duration;
            if (deltaTime >= duration)
            {
                _currentMaterial.SetColor(data.ValueName, data.EndColorRuntime);
                return false;
            }
            else
            {
                var newValue = Color.Lerp(data.StartColorRuntime, data.EndColorRuntime, deltaTime / data.Duration);
                _currentMaterial.SetColor(data.ValueName, newValue);
                return true;
            }
        }
    }
}