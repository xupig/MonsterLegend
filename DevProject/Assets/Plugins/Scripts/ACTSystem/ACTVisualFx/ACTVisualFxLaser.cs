﻿#region 模块信息
/*==========================================
// 模块名：ACTVisualFxLaser
// 命名空间: ACTSystem
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/05/19
// 描述说明：技能激光特效
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using System.Text;
using System;
using UnityEngine;

namespace ACTSystem
{
    public class ACTVisualFxLaser : ACTVisualFx
    {
        /// <summary>
        /// 技能投射者
        /// </summary>
        public ACTActor caster;

        /// <summary>
        /// 技能作用目标
        /// </summary>
        public ACTActor target;

        /// <summary>
        /// 定位类型
        /// </summary>
        public ACTVFXLocationType LocationType;

        /// <summary>
        /// 起点名称
        /// </summary>
        public string startSlotName;

        /// <summary>
        /// 起点偏移
        /// </summary>
        public ACTVector3 startOffset;

        /// <summary>
        /// 终点名称
        /// </summary>
        public string endSlotName;

        /// <summary>
        /// 终点偏移
        /// </summary>
        public ACTVector3 endOffset;


        /// <summary>
        /// 起点
        /// </summary>
        private Transform _startSlot;

        /// <summary>
        /// 起点挂接标志
        /// </summary>
        private bool _startSlotFlag = false;

        /// <summary>
        /// 起点
        /// </summary>
        private Transform _endSlot;

        private Vector3 _startPosition;
        private Vector3 _endPosition;


        public Action callback;

        private LineRenderer _lineRenderer;
        private static GameObject _startOffsetGo = null;
        #region 辅助特效
        public ACTVisualFxAssist startVisualFx;
        public ACTVisualFxAssist endVisualFx;

        #endregion

        public ACTVisualFxLaser()
        {
            fxType = ACTVisualFXType.TYPE_LASER;
        }

        public void SetCaster(ACTActor curCaster)
        {
            caster = curCaster;
            var listener = caster.GetComponent<ACTEventListener>();
            listener.onDestroy += OnCasterOrTargetDestroy;
        }

        public void SetTarget(ACTActor curTarget)
        {
            target = curTarget;
            var listener = target.GetComponent<ACTEventListener>();
            listener.onDestroy += OnCasterOrTargetDestroy;
        }

        public void OnCasterOrTargetDestroy(GameObject go)
        {
            Stop();
        }

        protected override void OnPlay()
        {
            _startSlotFlag = false;

        }

        protected override void OnReset()
        {
            _startSlot = null;
            _startSlotFlag = false;
            endSlotName = null;
            startSlotName = string.Empty;
            endSlotName = string.Empty;
        }

        protected override void OnUpdate()
        {
            UpdateLaser();
        }

        private void UpdateLaser()
        {
            //添加LineRenderer
            if (_assetObject == null) return;
            if (_lineRenderer == null)
            {
                _lineRenderer = _assetObject.gameObject.GetComponent<LineRenderer>();
                if (_lineRenderer == null)
                {
                    _lineRenderer = _assetObject.gameObject.AddComponent<LineRenderer>();
                    _lineRenderer.SetColors(new Color(1, 0, 0, 0.5f), new Color(0, 0, 1, 0.6f));
                }
            }
            SetSlotInfo();
            UpdateAssistFxPosition();                        
            //设置计算激光位置信息
            SetLaserPosition(_startPosition, _endPosition);
            
        }

        /// <summary>
        /// 更新辅助特效坐标
        /// </summary>
        private void UpdateAssistFxPosition()
        {
            //var startDir = (endSlot.position - startSlot.position).normalized;
            if (startVisualFx != null)
            {
                startVisualFx.localPosition = _startPosition;
            }
            if (endVisualFx != null) endVisualFx.localPosition = _endPosition;

        }

        private void SetLaserPosition(Vector3 startPosition, Vector3 endPosition)
        {
            //设置目标点
            //_lineRenderer.SetWidth(0.1f, 0.1f);
            _lineRenderer.SetPosition(0, startPosition);
            _lineRenderer.SetPosition(1, endPosition);
        }

        /// <summary>
        /// 设置挂节点信息
        /// </summary>
        private void SetSlotInfo()
        {
            if (caster == null) return;
            //挂接类型只是针对施法者
            switch (LocationType)
            {
                case ACTVFXLocationType.SELF_LOCAL:
                    GetStartOffsetGameObject();
                    _startOffsetGo.transform.SetParent(caster.transform, false);
                    _startOffsetGo.transform.localPosition = startOffset.ToVector3();
                    //ACTSystemLog.LogError("[ACTVisualFxAssist:SetSlotInfo]=>1__localPosition: " + _startOffsetGo.transform.localPosition + ",position:  " + _startOffsetGo.transform.position);
                    //获取施法者根节点
                    _startSlot = _startOffsetGo.transform;
                    _startOffsetGo.SetActive(false);
                    _startPosition = _startSlot.position;
                    break;
                case ACTVFXLocationType.SELF_SLOT:
                    GetStartOffsetGameObject();
                    SetSlotInfo(ref _startSlot, startSlotName, caster);
                    _startOffsetGo.transform.SetParent(_startSlot, false);
                    _startOffsetGo.transform.localPosition = startOffset.ToVector3();

                    _startPosition = _startOffsetGo.transform.position;
                    break;
                case ACTVFXLocationType.SELF_WORLD:
                    if (_startSlotFlag == false)
                    {
                        _startSlotFlag = true;
                        GetStartOffsetGameObject();
                        _startOffsetGo.transform.SetParent(caster.transform, false);
                        _startOffsetGo.transform.localPosition = startOffset.ToVector3();

                        _startPosition = _startOffsetGo.transform.position;
                    }
                    break;
                case ACTVFXLocationType.SLOT_WORLD:
                    SetSlotPositionRotation(startSlotName, caster, (slotPosition,slot) =>
                        {
                            GetStartOffsetGameObject();
                            _startOffsetGo.transform.SetParent(slot, false);
                            _startOffsetGo.transform.localPosition = startOffset.ToVector3();

                            _startPosition = _startOffsetGo.transform.position;
                            //ACTSystemLog.LogError("[ACTVisualFxLaser:SetSlotInfo]=>slotPosition:   " + slotPosition);
                        });
                    break;
                case ACTVFXLocationType.CUSTOM_WORLD:
                    if (_startSlotFlag == false)
                    {
                        _startPosition = startOffset.ToVector3();
                    }
                    break;
                case ACTVFXLocationType.SELF_LOCAL_SLOT:
                    GetStartOffsetGameObject();
                    _startOffsetGo.transform.SetParent(caster.transform, false);
                    SetSlotInfo(ref _startSlot, startSlotName, caster);

                    _startOffsetGo.transform.localPosition = _startSlot.localPosition + startOffset.ToVector3();
                    //ACTSystemLog.LogError("[ACTVisualFxAssist:SetSlotInfo]=>1__localPosition: " + _startOffsetGo.transform.localPosition + ",position:  " + _startOffsetGo.transform.position);
                    //获取施法者根节点
                    _startSlot = _startOffsetGo.transform;
                    _startOffsetGo.SetActive(false);
                    _startPosition = _startSlot.position;

                    break;
                default:
                    break;
            }
            

            //设置目标挂节点信息
            SetSlotInfo(ref _endSlot, endSlotName, target);
            //
            //计算起点和终点
            
            _endPosition = _endSlot.position + endOffset.ToVector3();
            //ACTSystemLog.LogError("[ACTVisualFxLaser:SetSlotInfo]=>_startPosition: " + _startPosition + startOffset.ToVector3()
            //    + ",_endSlot.position:  " + _endSlot.position + ",endOffset.ToVector3():    " + endOffset.ToVector3());
        }

        private void GetStartOffsetGameObject()
        {
            if (_startOffsetGo == null)
            {
                _startOffsetGo = new GameObject();
                _startOffsetGo.name = "startOffsetGo";
            }
            _startOffsetGo.SetActive(true);
        }

        private void SetSlotInfo(ref Transform _slot, string _slotName, ACTActor _actor)
        {
            if (_slot == null)
            {
                if (string.IsNullOrEmpty(_slotName))
                {
                    _slot = _actor.transform;
                }
                else
                {
                    _slot = _actor.boneController.GetBoneByName(_slotName);
                    if (_slot == null)
                    {
                        _slot = _actor.transform;
                    }
                }
            }
        }

        /// <summary>
        /// 只获取坐标和角度，不设置Transform的引用关系，目的是只想获取一次信息
        /// </summary>
        private void SetSlotPositionRotation(string _slotName, ACTActor _actor,Action<Vector3,Transform> callBack = null)
        {
            if (_startSlotFlag) return;
            Vector3 slotPosition = Vector3.zero;
            _startSlotFlag = true;
            if (string.IsNullOrEmpty(_slotName))
            {
                slotPosition = _actor.transform.position;
                if (callBack != null)
                {
                    callBack(slotPosition, _actor.transform);
                }
            }
            else
            {
                Transform boneSlot = _actor.boneController.GetBoneByName(_slotName);
                if (boneSlot == null)
                {
                    slotPosition = _actor.transform.position;
                    if (callBack != null)
                    {
                        callBack(slotPosition, _actor.transform);
                    }
                }
                else
                {
                    slotPosition = boneSlot.position;
                    if (callBack != null)
                    {
                        callBack(slotPosition, boneSlot);
                    }
                }
            }
        }

        override protected void OnStop()
        {
            if (startVisualFx != null)
            {
                startVisualFx.Stop();
            }
            if (endVisualFx != null)
            {
                endVisualFx.Stop();
            }
        }

    }
}
