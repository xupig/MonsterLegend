﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ACTSystem
{

    public class ACTVisualFxLocatorData
    {
        public ACTActor actor;
        public Vector3 location;
        public ACTVFXLocationType locationType;
        public string slotName;
        public string skinSlotName;
        public ACTVector3 offset = null;
        public ACTVector3 rotate = null;
        public ACTActor locationActor = null;

        public void Reset(ACTActor actor, Vector3 location, ACTVFXLocationType locationType,
                string slotName, ACTVector3 offset = null, ACTVector3 rotate = null, ACTActor locationActor = null, string skinSlotName = null)
        {
            this.actor = actor;
            this.location = location;
            this.locationType = locationType;
            this.slotName = slotName;
            this.offset = offset;
            this.rotate = rotate;
            this.locationActor = locationActor;
            this.skinSlotName = skinSlotName;
        }

        public void Clear()
        {
            actor = null;
        }

        static Queue<ACTVisualFxLocatorData> _objectPool = new Queue<ACTVisualFxLocatorData>();
        public static ACTVisualFxLocatorData CreateData()
        {
            if (_objectPool.Count == 0)
            {
                _objectPool.Enqueue(new ACTVisualFxLocatorData());
            }
            return _objectPool.Dequeue();
        }

        public static void ReleaseData(ACTVisualFxLocatorData data)
        {
            data.Clear();
            _objectPool.Enqueue(data);
        }
    }

    public static class ACTVisualFxLocator
    {
        public static void InitVisualFXLocation(ACTVisualFx visualFXObj, ACTActor actor, Vector3 location,
                ACTVFXLocationType locationType, string slotName, ACTVector3 offset = null, ACTVector3 rotate = null, ACTActor locationActor = null, string skinSlotName = null)
        {
            ACTVisualFxLocatorData data = ACTVisualFxLocatorData.CreateData();
            data.Reset(actor, location, locationType, slotName, offset, rotate, locationActor, skinSlotName);

            var locatorFunctions = GetLocatorFunctions();
            if (locatorFunctions.ContainsKey((int)locationType))
            {
                locatorFunctions[(int)locationType](visualFXObj, data);
            }
            GetSkinSlotAndActor(visualFXObj, data);
            ACTVisualFxLocatorData.ReleaseData(data);
        }

        static Dictionary<int, Action<ACTVisualFx, ACTVisualFxLocatorData>> _locatorFunctions;
        static Dictionary<int, Action<ACTVisualFx, ACTVisualFxLocatorData>> GetLocatorFunctions()
        {
            if (_locatorFunctions == null)
            {
                _locatorFunctions = new Dictionary<int, Action<ACTVisualFx, ACTVisualFxLocatorData>>();
                _locatorFunctions.Add((int)ACTVFXLocationType.SELF_LOCAL, LocationBySelfLocal);
                _locatorFunctions.Add((int)ACTVFXLocationType.SELF_SLOT, LocationBySelfSlot);
                _locatorFunctions.Add((int)ACTVFXLocationType.SELF_WORLD, LocationBySelfWorld);
                _locatorFunctions.Add((int)ACTVFXLocationType.SLOT_WORLD, LocationBySlotWorld);
                _locatorFunctions.Add((int)ACTVFXLocationType.CUSTOM_WORLD, LocationByCustomWorld);
                _locatorFunctions.Add((int)ACTVFXLocationType.SELF_LOCAL_ONLY_POS, LocationBySelfLocalOnlyPos);
                _locatorFunctions.Add((int)ACTVFXLocationType.SELF_SLOT_ONLY_POS, LocationBySelfSlotOnlyPos);
                _locatorFunctions.Add((int)ACTVFXLocationType.SELF_LOCAL_SLOT, LocationBySelfLocalSlot);
                _locatorFunctions.Add((int)ACTVFXLocationType.BEHIT_WORLD, LocationByBeHitWorld);
                _locatorFunctions.Add((int)ACTVFXLocationType.SELF_LOCAL_ONLY_POS_WORLD_FACE, LocationBySelfLocalOnlyPosWorldFace);
            }
            return _locatorFunctions;
        }

        static BoxCollider _locationBox;
        static BoxCollider GetLocationBox()
        {
            if (_locationBox == null)
            {
                var go = new GameObject("_locationBox");
                GameObject.DontDestroyOnLoad(go);
                go.hideFlags = HideFlags.DontSaveInEditor;
                go.layer = ACTLayerUtils.Trigger;
                _locationBox = go.AddComponent<BoxCollider>();
                _locationBox.isTrigger = true;
            }
            return _locationBox;
        }

        static void SetOffsetInLocal(ACTVisualFx visualFXObj, ACTVector3 offset)
        {
            if (offset != null)
            {
                visualFXObj.localPosition = new Vector3(offset.x, offset.y, offset.z);
            }
        }

        static void SetOffsetInWorld(ACTVisualFx visualFXObj, ACTVector3 offset)
        {
            if (offset != null)
            {
                var offsetMat = Matrix4x4.TRS(new Vector3(visualFXObj.localPosition.x, visualFXObj.localPosition.y, visualFXObj.localPosition.z), Quaternion.LookRotation(visualFXObj.forward), Vector3.one);
                var offsetReal = offsetMat.MultiplyPoint3x4(new Vector3(offset.x, offset.y, offset.z));
                visualFXObj.localPosition = offsetReal;
            }
        }

        static void SetRotate(ACTVisualFx visualFXObj, ACTVector3 rotate)
        {
            if (visualFXObj == null) return;
            Quaternion rotation = Quaternion.identity;
            if (visualFXObj.localRotation != null)
            {
                rotation = visualFXObj.localRotation;
            }
            if (rotate != null)
            {
                var angle = rotation.eulerAngles;
                rotation = Quaternion.Euler(new Vector3(angle.x + rotate.x, angle.y + rotate.y, angle.z + rotate.z));
            }
            if (rotation != Quaternion.identity)
            {
                visualFXObj.localRotation = rotation;
            }
        }

        static void LocationBySelfLocal(ACTVisualFx visualFXObj, ACTVisualFxLocatorData data)
        {
            visualFXObj.parent = data.actor.transform;
            SetOffsetInLocal(visualFXObj, data.offset);
            SetRotate(visualFXObj, data.rotate);
        }

        static void GetSkinSlotAndActor(ACTVisualFx visualFXObj, ACTVisualFxLocatorData data)
        {
            if (data.skinSlotName != null)
            {
                var skinSlot = data.actor.boneController.GetBoneByName(data.skinSlotName);
                visualFXObj.SkinSlot = skinSlot;
                visualFXObj.actor = data.actor;
            }
        }

        static void LocationBySelfSlot(ACTVisualFx visualFXObj, ACTVisualFxLocatorData data)
        {
            var slot = data.actor.boneController.GetBoneByName(data.slotName);
            visualFXObj.parent = slot;
            SetOffsetInLocal(visualFXObj, data.offset);
            SetRotate(visualFXObj, data.rotate);
        }

        static void LocationBySelfWorld(ACTVisualFx visualFXObj, ACTVisualFxLocatorData data)
        {
            visualFXObj.localPosition = data.actor.transform.position;
            visualFXObj.forward = data.actor.transform.forward;
            SetOffsetInWorld(visualFXObj, data.offset);
            if (data.actor == null) return;
            Transform locationTransform = data.actor.transform;
            var rotate = data.rotate;
            var forward = locationTransform.forward;
            var locationMatrix = locationTransform.localToWorldMatrix;
            Vector3 rotation = Vector3.zero;
            if (rotate != null)
            {
                rotation = new Vector3(rotate.x, rotate.y, rotate.z);
            }
            if (rotation != Vector3.zero)
            {
                var newM = locationMatrix * Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(rotation), Vector3.one);
                Vector4 vz = newM.GetColumn(2);
                forward = new Vector3(vz.x, vz.y, vz.z);
            }
            visualFXObj.forward = forward;
            SetRotate(visualFXObj, data.rotate);
        }

        static void LocationBySlotWorld(ACTVisualFx visualFXObj, ACTVisualFxLocatorData data)
        {
            var slot = data.actor.boneController.GetBoneByName(data.slotName);
            if (slot == null)
            {
                ACTLogger.Error("slotName:" + data.slotName + " not exist in " + data.actor.name);
                return;
            }
            visualFXObj.localPosition = slot.transform.position;
            visualFXObj.localRotation = slot.transform.rotation;
            visualFXObj.forward = data.actor.transform.forward;
            SetOffsetInWorld(visualFXObj, data.offset);
            SetRotate(visualFXObj, data.rotate);
        }

        static void LocationByCustomWorld(ACTVisualFx visualFXObj, ACTVisualFxLocatorData data)
        {
            if (data.location != Vector3.zero)
            {
                visualFXObj.localPosition = data.location;
                if (data.actor != null)
                {
                    visualFXObj.forward = data.actor.transform.forward;
                }
            }
            SetOffsetInWorld(visualFXObj, data.offset);
            SetRotate(visualFXObj, data.rotate);
        }

        static void LocationBySelfLocalOnlyPos(ACTVisualFx visualFXObj, ACTVisualFxLocatorData data)
        {
            visualFXObj.localPosition = data.actor.transform.position;
            visualFXObj.forward = data.actor.transform.forward;
            visualFXObj.followTransform = data.actor.transform;
            visualFXObj.isFollow = true;
            SetOffsetInWorld(visualFXObj, data.offset);
            SetRotate(visualFXObj, data.rotate);
        }

        static void LocationBySelfLocalOnlyPosWorldFace(ACTVisualFx visualFXObj, ACTVisualFxLocatorData data)
        {
            visualFXObj.localPosition = data.actor.transform.position;
            visualFXObj.followTransform = data.actor.transform;
            visualFXObj.isFollow = true;
            SetOffsetInWorld(visualFXObj, data.offset);
            SetRotate(visualFXObj, data.rotate);
        }

        static void LocationBySelfSlotOnlyPos(ACTVisualFx visualFXObj, ACTVisualFxLocatorData data)
        {
            var slot = data.actor.boneController.GetBoneByName(data.slotName);
            if (slot == null)
            {
                ACTLogger.Error("slotName:" + data.slotName + " not exist in " + data.actor.name);
                return;
            }
            visualFXObj.localPosition = slot.transform.position;
            visualFXObj.localRotation = slot.transform.rotation;
            visualFXObj.forward = data.actor.transform.forward;
            visualFXObj.followTransform = slot;
            visualFXObj.isFollow = true;
            SetOffsetInWorld(visualFXObj, data.offset);
            SetRotate(visualFXObj, data.rotate);
        }

        static void LocationBySelfLocalSlot(ACTVisualFx visualFXObj, ACTVisualFxLocatorData data)
        {
            visualFXObj.parent = data.actor.transform;  //挂接到根骨骼上
            var slot = data.actor.boneController.GetBoneByName(data.slotName);
            visualFXObj.localPosition = slot.transform.localPosition;      //根据挂接骨骼设置模型偏移
            visualFXObj.localRotation = slot.transform.localRotation;      //根据挂接骨骼设置模型角度
            SetOffsetInLocal(visualFXObj, data.offset);
            SetRotate(visualFXObj, data.rotate);
        }

        static Ray _downRay = new Ray(Vector3.zero, Vector3.down);
        static RaycastHit _raycastHit = new RaycastHit();
        static Vector3 _hitPoint = Vector3.zero;
        static int _triggerLayerValue = 1 << ACTLayerUtils.Trigger;
        static void LocationByBeHitWorld(ACTVisualFx visualFXObj, ACTVisualFxLocatorData data)
        {
            if (data.locationActor == null) return;
            Transform locationTransform = data.locationActor.transform;
            var locationActorPos = locationTransform.position;
            var locationPos = locationActorPos;
            var locationMatrix = locationTransform.localToWorldMatrix;
            var offset = data.offset;
            if (offset != null)
            {
                var newM = locationMatrix * Matrix4x4.TRS(new Vector3(offset.x, offset.y, offset.z), Quaternion.identity, Vector3.one);
                locationPos = new Vector3(newM.m03, newM.m13, newM.m23);
            }

            var beHitBoxList = data.actor.actorData.BeHitBoxList;
            Transform slot = null;
            Vector3 size = Vector3.zero;
            if (beHitBoxList.Count > 0)
            {
                slot = data.actor.boneController.GetBoneByName(beHitBoxList[0].Slot);
                var boxSize = beHitBoxList[0].BoxSizeReal;
                size = new Vector3(boxSize.x, boxSize.y, boxSize.z);
            }
            if (slot == null)
                slot = data.actor.transform;
            var targetPos = slot.position;
            if (size != Vector3.zero)
            {
                var box = GetLocationBox();
                box.size = size;
                box.transform.position = targetPos;
                box.transform.forward = slot.forward;
                Vector3 dir = locationTransform.forward;
                if (locationPos.x != locationActorPos.x || locationPos.z != locationActorPos.z)
                {
                    dir = locationPos - locationActorPos;
                    dir = Vector3.ProjectOnPlane(dir, locationTransform.up);
                }
                _downRay.origin = locationPos;
                _downRay.direction = dir;
                _raycastHit.point = Vector3.zero;
                Physics.Raycast(_downRay, out _raycastHit, 30, _triggerLayerValue);
                if (UnityPropUtils.IsEditor) Debug.DrawRay(locationPos, dir, Color.blue);
                if (_raycastHit.point != Vector3.zero)
                {
                    targetPos = _raycastHit.point;
                }
            }
            visualFXObj.localPosition = targetPos;
            var rotate = data.rotate;
            var forward = locationTransform.forward;
            Vector3 rotation = Vector3.zero;
            if (rotate != null)
            {
                rotation = new Vector3(rotate.x, rotate.y, rotate.z);
            }
            if (rotation != Vector3.zero)
            {
                var newM = locationMatrix * Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(rotation), Vector3.one);
                Vector4 vz = newM.GetColumn(2);
                forward = new Vector3(vz.x, vz.y, vz.z);
            }
            visualFXObj.forward = forward;
        }
    }
}
