﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{

    public class ACTAssetObject
    {
        public string path;
        public GameObject gameObject;
        public bool isUsed = false;
    }

    public class ACTObjectPool
    {
        const float USE_TIMEOUT = 300; //特效保留的超时时间

        public bool useTimeout = true;

        IACTAssetLoader _assetLoader;
        public IACTAssetLoader assetLoader
        {
            get { return _assetLoader; }
        }

        GameObject _poolObj;

        public Dictionary<string, List<ACTAssetObject>> _content
            = new Dictionary<string, List<ACTAssetObject>>();
        public Dictionary<string, bool> _preloadStates
            = new Dictionary<string, bool>();
        public Dictionary<string, float> _lastUseTimeRecord
            = new Dictionary<string, float>();

        public ACTObjectPool(ACTSystemDriver driver)
        {
            _assetLoader = new ACTSimpleLoader();
            Transform _pool = driver.transform.Find("ACTObjectPool");
            if (_pool != null)
            {
                _poolObj = _pool.gameObject;
            }
            else
            {
                _poolObj = new GameObject("ACTObjectPool");
                _poolObj.transform.SetParent(driver.transform);
                _poolObj.hideFlags = HideFlags.DontSaveInEditor;
            }
        }

        /*
        const float UPDATE_SPACE_TIME = 60; //update时间间隔 
        float _lastUpdateTime = 0;
        public void Update()
        {
            if (UnityPropUtils.realtimeSinceStartup - _lastUpdateTime < UPDATE_SPACE_TIME) return;
            _lastUpdateTime = UnityPropUtils.realtimeSinceStartup;
            UnloadUnusedAssets();
        }*/

        public void ClearPool()
        {
            _preloadStates.Clear();
            _content.Clear();
            _lastUseTimeRecord.Clear();
            var childCount = _poolObj.transform.childCount;
            for (int i = childCount - 1; i >= 0; i--)
            {
                var go = _poolObj.transform.GetChild(i);
                GameObject.Destroy(go.gameObject);
            }
        }

        void ClearObject(string path)
        {
            if (_content.ContainsKey(path))
            {
                var list = _content[path];
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    var aobj = list[i];
                    if (aobj.gameObject != null)
                    {
                        GameObject.Destroy(aobj.gameObject);
                    }
                    list.RemoveAt(i);
                }
                _content.Remove(path);
            }
            if (_preloadStates.ContainsKey(path))
            {
                _preloadStates.Remove(path);
            }
        }

        public void UnloadUnusedAssets()
        {
            CheckTimeoutAssets();
        }

        List<string> _cacheList = new List<string>();
        void CheckTimeoutAssets()
        {
            if (!useTimeout) return;
            _cacheList.Clear();
            foreach (var pair in _lastUseTimeRecord)
            {
                if (UnityPropUtils.realtimeSinceStartup - pair.Value > USE_TIMEOUT)
                {
                    _cacheList.Add(pair.Key);
                }
            }
            for (int i = 0; i < _cacheList.Count; i++)
            {
                var path = _cacheList[i];
                _lastUseTimeRecord.Remove(path);
                ClearObject(path);
            }
        }

        public void SetAssetLoader(IACTAssetLoader assetLoader)
        {
            _assetLoader = assetLoader;
        }

        private ACTAssetObject CreateNewAssetObject(GameObject gameObject)
        {
            var assetObejct = new ACTAssetObject();
            assetObejct.gameObject = gameObject;
            return assetObejct;
        }

        private bool IsPreloaded(string strPath)
        {
            if (!_preloadStates.ContainsKey(strPath))
            {
                _preloadStates[strPath] = false;
                _assetLoader.PreloadAsset(new string[] { strPath }, () =>
                {
                    _preloadStates[strPath] = true;
                });
            }
            return _preloadStates[strPath];
        }

        private List<ACTAssetObject> GetAssetObjectList(string strPath)
        {
            if (!_content.ContainsKey(strPath))
            {
                _content.Add(strPath, new List<ACTAssetObject>());
            }
            return _content[strPath];
        }

        private ACTAssetObject GetUnuseAssetObject(string strPath)
        {
            ACTAssetObject result = null;
            var list = GetAssetObjectList(strPath);
            for (int i = list.Count - 1; i >= 0; i--)
            {
                var aobj = list[i];
                if (aobj.gameObject == null)
                {
                    list.RemoveAt(i);
                    continue;
                }
                if (!aobj.isUsed)
                {
                    result = aobj;
                    result.isUsed = true;
                    result.gameObject.transform.SetParent(null, false);
                    result.gameObject.transform.localPosition = Vector3.zero;
                    break;
                }
            }
            return result;
        }

        private void RecordUseTime(string strPath)
        {
            if (!_lastUseTimeRecord.ContainsKey(strPath))
            {
                _lastUseTimeRecord[strPath] = 0;
            }
            _lastUseTimeRecord[strPath] = UnityPropUtils.realtimeSinceStartup;
        }

        //采用同步方式获取
        public ACTAssetObject CreateGameObject(string strPath)
        {
            ACTAssetObject result = null;
            if (string.IsNullOrEmpty(strPath))
            {
                return result;
            }
            strPath = _assetLoader.GetResMapping(strPath);
            RecordUseTime(strPath);
            if (!IsPreloaded(strPath))
            {
                return result;
            }
            result = GetUnuseAssetObject(strPath);
            if (result == null)
            {
                ACTAssetObject newACTAssetObject = null;
                LoadGameObject(strPath, (assetObject) =>
                {
                    newACTAssetObject = assetObject;
                });
                if (newACTAssetObject != null)
                {
                    result = GetUnuseAssetObject(strPath);
                }
            }
            if (result != null) result.isUsed = true;
            return result;
        }

        private void LoadGameObject(string strPath, Action<ACTAssetObject> action)
        {
            _assetLoader.GetGameObject(strPath, (obj) =>
            {
                ACTAssetObject result = null;
                if (obj != null)
                {
                    result = CreateNewAssetObject(obj as GameObject);
                    result.path = strPath;
                    result.gameObject.SetActive(false);
                    GetAssetObjectList(strPath).Add(result);
                    result.gameObject.transform.SetParent(_poolObj.transform, false);
                    //ChangeLayersRecursively(result.gameObject.transform, "Bloom");
                }
                action(result);
            });
        }

        public void ChangeLayersRecursively(Transform trans, string name)
        {
            ChangeLayersRecursively(trans, ACTSystemTools.NameToLayer(name));
        }

        public void ChangeLayersRecursively(Transform trans, int layer)
        {
            trans.gameObject.layer = layer;
            foreach (Transform child in trans)
            {
                ChangeLayersRecursively(child, layer);
            }
        }

        public void ReleaseGameObject(ACTAssetObject assetObject)
        {
            if (assetObject.gameObject == null)
            {
                if (assetObject.path == null) { ACTLogger.Error("assetObject.path == null"); }
                GetAssetObjectList(assetObject.path).Remove(assetObject);
                return;
            }
            if (_poolObj == null) return;
            assetObject.isUsed = false;
            assetObject.gameObject.SetActive(false);
            assetObject.gameObject.transform.SetParent(_poolObj.transform, false);
        }
    }
}
