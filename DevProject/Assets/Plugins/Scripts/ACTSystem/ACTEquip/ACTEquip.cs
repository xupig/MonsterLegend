﻿
using ArtTech;
using System;
using System.Collections.Generic;
using UnityEngine;
namespace ACTSystem
{
    public class ACTEquip
    {
        public delegate void OnLoadFinishedDelegate();

        public int defaultEquipID = 0;
        protected ACTActor _actor;
        public string layer = "Default";
        protected bool _actorIsDestory = false;
        public ACTDataEquipment curEquipData;
        protected bool _isNormalQuality = true;
        public int curEquipParticleID = 0;
        public ACTDataEquipParticle curEquipParticleData;
        public int curEquipFlowID = 0;
        public ACTDataEquipFlow curEquipFlowData;
        protected bool _onEquipLoading = false;
        protected bool _onParticleLoading = false;
        protected bool _onFlowLoading = false;
        //染色修改参数
        public string colorChannelR = "_ChangeColorR";
        public string colorChannelG = "_ChangeColorG";
        public string colorChannelB = "_ChangeColorB";
        //储存要染颜色，保持模型还未加载完成就已经再变色情形不出问题
        protected string colorR;
        protected string colorG;
        protected string colorB;

        public FlowFlashEffect flowFlashEffect;
        protected MeshRenderer ownerMRenderer;

        public bool isFlowShow = false;

        private string _flowMaterialPath = string.Empty;
        protected string flowMaterialPath
        {
            set
            {
                if (!string.IsNullOrEmpty(_flowMaterialPath))
                {
                    var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
                    assetLoader.ReleaseAsset(_flowMaterialPath);
                }
                _flowMaterialPath = value;
            }
        }

        public OnLoadFinishedDelegate onLoadEquipFinished;
        public OnLoadFinishedDelegate onLoadParticleFinished;
        public OnLoadFinishedDelegate onLoadFlowFinished;

        public ACTEquip(ACTActor actor)
        {
            _actor = actor;
            _actorIsDestory = false;
        }

        //实体被销毁时调用
        virtual public void OnActorDestroy()
        {
            _actorIsDestory = true;
            flowMaterialPath = string.Empty;
        }

        /// <summary>
        /// 穿戴装备
        /// </summary>
        /// <param name="equipID">装备id</param>
        /// <param name="particleID">装备特效id</param>
        /// <param name="flowID">装备流光id</param>
        /// <param name="isNormalQuality">是否正常质量(若否则是低模质量)</param>
        /// <param name="onLoadFinished">穿戴成功后回调</param>
        public void PutOn(int equipID, int particleID = 0, int flowID = 0, bool isNormalQuality = true, string colorR = "", string colorG = "", string colorB = "")
        {
            //if (_actor.name.IndexOf("101(") > -1)
            //{
            //    Debug.LogError(string.Format("PutOn, equipID = {0}, particleID = {1}, flowID = {2}", equipID, particleID, flowID));
            //}
            if (equipID == 0)
            {
                TakeOff();
                return;
            }

            var data = ACTRunTimeData.GetEquipmentData(equipID);
            if (data == null)
            {
                ACTLogger.Error("equipID Error (装备模型ID不存在)：" + equipID);
                data = ACTRunTimeData.GetEquipmentData(defaultEquipID);
                if (data == null)
                {
                    ACTLogger.Error("default equipID Error (装备模型ID不存在)：" + defaultEquipID);
                    return;
                }
            }

            this.colorR = colorR;
            this.colorG = colorG;
            this.colorB = colorB;

            if (!NeedToPutOnEquip(data, flowID, isNormalQuality))
            {
                OnPutOnOthers(particleID, flowID, colorR, colorG, colorB);
                return;
            }
            OnPutOn(data, particleID, flowID, isNormalQuality, colorR, colorG, colorB);
        }

        /// <summary>
        /// 更换装备粒子效果
        /// </summary>
        /// <param name="particleID"></param>
        public void ChangeParticle(int particleID)
        {
            if (curEquipData == null)
            {
                return;
            }
            if (curEquipParticleID == particleID)
            {
                return;
            }
            PutOn(curEquipData.ID, particleID, curEquipFlowID);
        }

        /// <summary>
        /// 更换装备流光效果
        /// </summary>
        /// <param name="flowID"></param>
        public void ChangeFlow(int flowID)
        {
            if (curEquipData == null)
            {
                return;
            }
            if (curEquipFlowID == flowID)
            {
                return;
            }
            PutOn(curEquipData.ID, curEquipParticleID, flowID);
        }

        //脱下equipID的装备
        public void TakeOff(int equipID)
        {
            if (curEquipData != null && curEquipData.ID == equipID)
            {
                TakeOff();
            }
        }

        //改变装备质量
        public void ChangeQuality(bool isNormalQuality)
        {
            if (curEquipData == null) return;
            if (_isNormalQuality == isNormalQuality) return;
            int particleID = (curEquipParticleData == null) ? 0 : curEquipParticleData.ID;
            int flowID = (curEquipFlowData == null) ? 0 : curEquipFlowData.ID;
            OnPutOn(curEquipData, particleID, flowID, isNormalQuality);
        }

        //染色
        public Material SetColor(Material material, string colorR, string colorG, string colorB)
        {
            if (material == null) return material;
            if (material.HasProperty(colorChannelR))
            {
                Color r, g, b;
                if (!string.IsNullOrEmpty(colorR))
                {
                    ColorUtility.TryParseHtmlString("#" + colorR, out r);
                    material.SetColor(colorChannelR, r);
                }
                if (!string.IsNullOrEmpty(colorG))
                {
                    ColorUtility.TryParseHtmlString("#" + colorG, out g);
                    material.SetColor(colorChannelG, g);
                }
                if (!string.IsNullOrEmpty(colorB))
                {
                    ColorUtility.TryParseHtmlString("#" + colorB, out b);
                    material.SetColor(colorChannelB, b);
                }
            }
            return material;
        }

        protected void OnSetColor(string colorR, string colorG, string colorB)
        {
            var slot = _actor.boneController.GetBoneByName(curEquipData.Slot);
            if (slot == null)
                return;
            var slotSkinnedRender = slot.GetComponent<SkinnedMeshRenderer>();
            if (slotSkinnedRender == null || slotSkinnedRender.sharedMaterial == null)
            {
                return;
            }
            slotSkinnedRender.sharedMaterial = SetColor(slotSkinnedRender.sharedMaterial, colorR, colorG, colorB);
        }

        protected void PutOnParticle(int particleID)
        {
            if (curEquipData == null)
            {
                return;
            }
            //if (curEquipParticleID == particleID)
            //{
            //    return;
            //}
            if (particleID == 0)
            {
                TakeOffParticle();
                return;
            }
            var particleData = curEquipData.GetEquipParticleData(particleID);
            if (particleData == null)
            {
                ACTLogger.Error(string.Format("equipID Error (装备ID{0}的粒子ID{1}不存在)：", curEquipData.ID, particleID));
                return;
            }
            OnPutOnParticle(particleData);
        }

        protected void PutOnFlow(int flowID)
        {
            if (curEquipData == null)
            {
                return;
            }
            //if (curEquipFlowID == flowID)
            //{
            //    return;
            //}
            if (flowID == 0)
            {
                TakeOffFlow();
                return;
            }
            var flowData = curEquipData.GetEquipFlowData(flowID);
            if (flowData == null)
            {
                ACTLogger.Error(string.Format("equipID Error (装备ID{0}的流光ID{1}不存在)：", curEquipData.ID, flowID));
                return;
            }
            OnPutOnFlow(flowData);
        }

        protected void TakeOff()
        {
            TakeOffParticle();
            TakeOffFlow();
            OnTakeOff();
        }

        protected void TakeOffParticle()
        {
            //if (curEquipParticleID == 0)
            //{
            //    return;
            //}
            OnTakeOffParticle();
            curEquipParticleID = 0;
            curEquipParticleData = null;
        }

        protected void TakeOffFlow()
        {
            //if (curEquipFlowID == 0)
            //{
            //    return;
            //}
            if (curEquipData == null)
            {
                return;
            }
            OnTakeOffFlow();
            curEquipFlowID = 0;
            curEquipFlowData = null;
        }

        /// <summary>
        /// 改变GameObject的层级
        /// </summary>
        /// <param name="go"></param>
        protected void ChangeGameObjectLayer(GameObject go)
        {
            if (go == null)
            {
                return;
            }
            List<Transform> transformList = new List<Transform>();
            go.GetComponentsInChildren<Transform>(true, transformList);
            for (int i = 0; i < transformList.Count; i++)
            {
                transformList[i].gameObject.layer = ACTSystemTools.NameToLayer(layer);
            }
        }

        protected bool NeedToPutOnEquip(ACTDataEquipment data, int flowID, bool isNormalQuality)
        {
            return curEquipData != data || _isNormalQuality != isNormalQuality || curEquipFlowID != flowID;
        }

        protected void FlowEnableKeyword(Material material, bool enabled)
        {
            if (enabled)
            {
                material.EnableKeyword("EMISSIVE_FLOW_ON");
            }
            else
            {
                material.DisableKeyword("EMISSIVE_FLOW_ON");
            }
        }

        protected void FlowEnable(FlowFlashEffect flowFlashEffect, Material[] materials, bool enabled, ACTDataEquipFlow flowData = null)
        {
            if (flowFlashEffect == null || materials == null || flowData == null) return;
            isFlowShow = enabled;
            enabled = _actor.equipController.canEquipFlowShow && enabled;
            if (enabled)
            {
                flowFlashEffect.SetMaterials(materials);//必须先设置材质，后设置属性

                flowData.flowColor.r = flowData.flowColorReal.x;
                flowData.flowColor.g = flowData.flowColorReal.y;
                flowData.flowColor.b = flowData.flowColorReal.z;
                flowData.flowColor.a = flowData.flowColorReal.w;

                flowData.flashColor.r = flowData.flashColorReal.x;
                flowData.flashColor.g = flowData.flashColorReal.y;
                flowData.flashColor.b = flowData.flashColorReal.z;
                flowData.flashColor.a = flowData.flashColorReal.w;

                flowData.flashColor2.r = flowData.flashColor2Real.x;
                flowData.flashColor2.g = flowData.flashColor2Real.y;
                flowData.flashColor2.b = flowData.flashColor2Real.z;
                flowData.flashColor2.a = flowData.flashColor2Real.w;

                flowData.flowSpeed.x = flowData.flowSpeedReal.x;
                flowData.flowSpeed.y = flowData.flowSpeedReal.y;

                flowFlashEffect.FlowColor = flowData.flowColor;
                flowFlashEffect.FlowSpeed = flowData.flowSpeed;

                flowFlashEffect.FlashColor = flowData.flashColor;
                flowFlashEffect.FlashStrength = flowData.flashStrength;
                flowFlashEffect.RoundTime = flowData.roundTime;

                flowFlashEffect.UseLayer2 = flowData.useLayer2;
                flowFlashEffect.FlashColor2 = flowData.flashColor2;
                flowFlashEffect.FlashStrength2 = flowData.flashStrength2;
                flowFlashEffect.CycleTime = flowData.cycleTime;
                flowFlashEffect.StartTime2 = flowData.startTime2;
                flowFlashEffect.RoundTime2 = flowData.roundTime2;

                var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
                if (!string.IsNullOrEmpty(flowData.flowTexPath))
                {
                    //no ResourceMappingManager
                    assetLoader.GetObject(flowData.flowTexPath, delegate(UnityEngine.Object obj)
                    {
                        flowFlashEffect.FlowTex = obj as Texture2D;
                    });
                }
                if (!string.IsNullOrEmpty(flowData.flashCubeTexPath))
                {
                    //no ResourceMappingManager
                    assetLoader.GetObject(flowData.flashCubeTexPath, delegate(UnityEngine.Object obj)
                    {
                        flowFlashEffect.FlashCubeTex = obj as Cubemap;
                    });
                }
            }

            flowFlashEffect.EnableEffect = enabled;
        }

        virtual protected void OnPutOn(ACTDataEquipment data, int particleID, int flowID, bool isNormalQuality, string colorR = "", string colorG = "", string colorB = "")
        {
        }

        virtual protected void OnPutOnOthers(int particleID, int flowID, string colorR = "", string colorG = "", string colorB = "")
        {
        }

        virtual protected void OnTakeOff()
        {
        }

        virtual protected void OnPutOnParticle(ACTDataEquipParticle data)
        {
        }

        virtual protected void OnTakeOffParticle()
        {
        }

        virtual protected void OnPutOnFlow(ACTDataEquipFlow data)
        {
        }

        virtual protected void OnTakeOffFlow()
        {
        }

        List<ParticleSystemRenderer> rendererList = new List<ParticleSystemRenderer>();
        protected void RefreshSortOrder(GameObject go)
        {
            var slot = _actor.boneController.GetBoneByName("body");
            if (slot == null)
                return;
            var slotSkinnedRender = slot.GetComponent<SkinnedMeshRenderer>();
            rendererList.Clear();
            go.GetComponentsInChildren(false, rendererList);
            for (int i = 0; i < rendererList.Count; i++)
            {
                rendererList[i].sortingOrder = slotSkinnedRender.sortingOrder;
            }
        }
    }
}
