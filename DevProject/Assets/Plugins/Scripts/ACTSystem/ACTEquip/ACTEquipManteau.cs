﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using ShaderUtils;

namespace ACTSystem
{
    public class ACTEquipManteau : ACTEquipComponent
    {
        private Animator _manteauAnimator = null;

        public ACTEquipManteau(ACTActor actor)
            : base(actor)
        {
        }

        protected override void OnPutOn(ACTDataEquipment data, int particleID, int flowID, bool isNormalQuality, string colorR, string colorG, string colorB)
        {
            if (data.EquipType != ACTEquipmentType.Manteau) { ACTLogger.Error("equipID is not a Manteau:" + data.ID); }
            _isNormalQuality = isNormalQuality;
            if (curEquipData != null) TakeOff();
            _manteauAnimator = null;
            if (string.IsNullOrEmpty(data.PrefabPath))
            {
                ACTLogger.Error("ACTDataEquipment Data error:" + data.ID);
                return;
            }
            curEquipData = data;
            curEquipParticleID = particleID;
            curEquipFlowID = flowID;
            if (_onEquipLoading)
            {
                return;
            }
            _onEquipLoading = true;
            var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
            string prefabPath = (_isNormalQuality || string.IsNullOrEmpty(data.PrefabPath_L)) ? data.PrefabPath : data.PrefabPath_L;
            prefabPath = assetLoader.GetResMapping(prefabPath);
            assetLoader.GetGameObject(prefabPath, delegate(UnityEngine.Object obj)
            {
                _onEquipLoading = false;
                if (_actorIsDestory || curEquipData != data)
                {
                    if (obj) GameObject.Destroy(obj);
                    if (curEquipData != data || _isNormalQuality != isNormalQuality)
                    {
                        if (curEquipData != null)
                        {
                            OnPutOn(curEquipData, curEquipParticleID, curEquipFlowID, _isNormalQuality, colorR, colorG, colorB);
                        }
                    }
                    return;
                }
                var _manteauGameObject = obj as GameObject;
                _manteauAnimator = _manteauGameObject.GetComponent<Animator>();
                OnPutOn(curEquipData.Slot, _manteauGameObject);   
                PutOnParticle(curEquipParticleID);
                PutOnFlow(curEquipFlowID);
                if (onLoadEquipFinished != null)
                {
                    onLoadEquipFinished();
                }
            });
        }

        private GameObject _manteauGameObject;
        void OnPutOn(string slotName, GameObject manteauGameObject)
        {
            if (string.IsNullOrEmpty(slotName) || manteauGameObject != null)
            {
                var slot = _actor.boneController.GetBoneByName(slotName);
                if (slot == null) return;
                manteauGameObject.transform.SetParent(slot, false);
                _manteauGameObject = manteauGameObject;
                ACTSystemTools.SetFxTag(_manteauGameObject.transform);
                ACTSystemTools.SetLayer(_manteauGameObject.transform, slot.gameObject.layer, true);
                ACTSystemTools.SetFxShow(_manteauGameObject.transform, _actor.equipController.canEquipFxShow);
            }
        }

        protected override void OnTakeOff()
        {
            if (_manteauGameObject != null)
            {
                GameObject.Destroy(_manteauGameObject);
            }
            _manteauGameObject = null;
            curEquipData = null;
            _manteauAnimator = null;
        }

        protected override void OnPutOnParticle(ACTDataEquipParticle data)
        {
            if (curEquipParticleData != null) TakeOffParticle();
            curEquipParticleID = data.ID;
            curEquipParticleData = data;
            //LoggerHelper.Info(string.Format("PutOnWingParticle, equipID = {0}, particleID = {1}", _curWingData.ID, data.ID));
            if (curEquipParticleData.PrefabList.Count < 1)
            {
                return;
            }
            if (_onEquipLoading || _onParticleLoading)
            {
                return;
            }
            _onParticleLoading = true;
            var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
            var prefabPath = curEquipParticleData.PrefabList[0].PrefabPath;
            prefabPath = assetLoader.GetResMapping(prefabPath);
            assetLoader.GetGameObject(prefabPath, delegate(UnityEngine.Object obj)
            {
                _onParticleLoading = false;
                if (_actorIsDestory)
                {
                    GameObject.Destroy(obj);
                    return;
                }
                if (curEquipParticleData != data)
                {
                    GameObject.Destroy(obj);
                    if (curEquipParticleData != null)
                    {
                        OnPutOnParticle(curEquipParticleData);
                    }
                    return;
                }
                if (_actorIsDestory) return;
                if (curEquipParticleData != data) return;
                var particleGameObject = obj as GameObject;
                OnPutOnParticle(particleGameObject);
                if (onLoadParticleFinished != null)
                {
                    onLoadParticleFinished();
                }
            });
        }

        GameObject _particleGameObject;
        void OnPutOnParticle(GameObject particleGameObject)
        {
            if (particleGameObject != null && _manteauGameObject)
            {
                particleGameObject.transform.SetParent(_manteauGameObject.transform, false);
                _particleGameObject = particleGameObject;
            }
        }

        protected override void OnTakeOffParticle()
        {
            if (_particleGameObject != null)
            {
                //LoggerHelper.Info(string.Format("TakeOffWingParticle, equipID = {0}, particleID = {1}", _curWingData.ID, _curWingParticleData.ID));
                GameObject.Destroy(_particleGameObject);
            }
            _particleGameObject = null;
            curEquipParticleData = null;
        }

        protected override void OnPutOnFlow(Material material)
        {
            if (_particleGameObject == null)
            {
                return;
            }
            var ownerSMRenderer = _particleGameObject.transform.GetComponent<MeshRenderer>();
            if (ownerSMRenderer != null)
            {
                ownerSMRenderer.sharedMaterial = null;
                ownerSMRenderer.sharedMaterial = material;
            }
        }

        protected override void OnTakeOffFlow()
        {
            if (_particleGameObject == null)
            {
                return;
            }
            var ownerSMRenderer = _particleGameObject.transform.GetComponent<MeshRenderer>();
            if (ownerSMRenderer != null && ownerSMRenderer.sharedMaterial != null)
            {
                FlowEnableKeyword(ownerSMRenderer.sharedMaterial, false);
            }
        }
    }
}

