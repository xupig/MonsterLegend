﻿using UnityEngine;

namespace ACTSystem
{
    public class ACTEquipComponent : ACTEquip
    {
        public ACTEquipComponent(ACTActor actor)
            : base(actor)
        {
        }

        protected override void OnPutOnOthers(int particleID, int flowID, string colorR = "", string colorG = "", string colorB = "")
        {
            if (curEquipParticleID != particleID)
            {
                curEquipParticleID = particleID;
                PutOnParticle(particleID);
            }
            if (curEquipFlowID != flowID)
            {
                curEquipFlowID = flowID;
                PutOnFlow(flowID);
            }
            OnSetColor(colorR, colorG, colorB);
        }

        protected override void OnPutOnFlow(ACTDataEquipFlow data)
        {
            string flowMaterialPath = data.MaterialPath;
            if (string.IsNullOrEmpty(flowMaterialPath))
            {
                ACTLogger.Error("ACTDataEquipFlow Data error:" + data.ID);
                return;
            }
            curEquipFlowID = data.ID;
            curEquipFlowData = data;
            if (_onFlowLoading)
            {
                return;
            }
            _onFlowLoading = true;
            var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
            string[] paths = new string[] { flowMaterialPath };
            paths = assetLoader.GetResMappings(paths);
            assetLoader.GetObjects(paths, delegate(UnityEngine.Object[] objs)
            {
                _onFlowLoading = false;
                if (_actorIsDestory || curEquipFlowData != data)
                {
                    assetLoader.ReleaseAssets(paths);
                    if (curEquipFlowData != data)
                    {
                        if (curEquipFlowData != null)
                        {
                            OnPutOnFlow(curEquipFlowData);
                        }
                    }
                    return;
                }
                this.flowMaterialPath = paths[0];
                var material = objs[0] as Material;
                if (material == null)
                {
                    ACTLogger.Error(string.Format("装备流光加载出错，装备{0} 流光{1} MaterialPath:{2}", curEquipData.ID, curEquipFlowData.ID, paths[0]));
                    return;
                }
                //FlowEnableKeyword(material, true);
                OnPutOnFlow(material);
                if (ownerMRenderer != null)
                {
                    FlowEnable(flowFlashEffect, new[] { ownerMRenderer.material }, true, data);
                }
                if (onLoadFlowFinished != null)
                {
                    onLoadFlowFinished();
                }
            });
        }

        virtual protected void OnPutOnFlow(Material material)
        {
        }
    }
}
