﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ACTSystem
{
    public class ACTEquipHead : ACTEquip
    {
        private string _meshPath = string.Empty;
        public string meshPath
        {
            set
            {
                if (!string.IsNullOrEmpty(_meshPath))
                {
                    var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
                    assetLoader.ReleaseAsset(_meshPath);
                }
                _meshPath = value;
            }
        }

        private string _materialPath = string.Empty;
        public string materialPath
        {
            set
            {
                if (!string.IsNullOrEmpty(_materialPath))
                {
                    var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
                    assetLoader.ReleaseAsset(_materialPath);
                }
                _materialPath = value;
            }
        }

        private SkinnedMeshRenderer headSkinnedRender;

        public ACTEquipHead(ACTActor actor) : base(actor)
        {
        }
        //是否使用高品质
        private bool _isMaterialHQ = false;

        public bool isMaterialHQ
        {
            set { _isMaterialHQ = value; }
        }
        protected override void OnPutOn(ACTDataEquipment data, int particleID, int flowID, bool isNormalQuality, string colorR = "", string colorG = "", string colorB = "")
        {
            //Debug.LogError("Head onPutOn");
            if (data.EquipType != ACTEquipmentType.Head) { ACTLogger.Error("equipID is not a Head:" + data.ID); }
            _isNormalQuality = isNormalQuality;
            if(curEquipData!=null)TakeOff();
            string newMeshPath = (_isNormalQuality || string.IsNullOrEmpty(data.MeshPath_L))
                ? data.MeshPath
                : data.MeshPath_L;
            string newMaterialPath = (_isNormalQuality || string.IsNullOrEmpty(data.MaterialPath_L))
                ? data.MaterialPath
                : data.MaterialPath_L;
            if (_isMaterialHQ && string.IsNullOrEmpty(data.MaterialHQPath))
            {
                newMaterialPath = data.MaterialHQPath;
            }
            var flowData = data.GetEquipFlowData(flowID);
            if (flowID > 0 && flowData != null)
            {
                newMaterialPath = flowData.MaterialPath;
            }
            if (string.IsNullOrEmpty(newMeshPath) || string.IsNullOrEmpty(newMaterialPath))
            {
                ACTLogger.Error("ACTDataEquipment Data error:" + data.ID);
                return;
            }
            curEquipData = data;
            _onEquipLoading = true;
            if (flowID > 0 && flowData != null)
            {
                curEquipFlowID = flowData.ID;
                curEquipFlowData = flowData;
            }
            var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
            String[] pathes = new[] {newMeshPath, newMaterialPath};
            pathes = assetLoader.GetResMappings(pathes);
            assetLoader.GetObjects(pathes, delegate(UnityEngine.Object[] objs)
            {
                //Debug.LogError("Head onPutOn 222");
                _onEquipLoading = false;
                if (_actorIsDestory)
                {
                    assetLoader.ReleaseAssets(pathes);
                    return;
                }
                if (curEquipData != data || _isNormalQuality != isNormalQuality)
                {
                    assetLoader.ReleaseAssets(pathes);
                    if (curEquipData != null)
                    {
                        OnPutOn(curEquipData, curEquipParticleID, curEquipFlowID, _isNormalQuality, colorR, colorG, colorB);
                    }
                    return;
                }
                if (objs[0] == null)
                {
                    ACTLogger.Error(String.Format("装备加载出错 {0} MeshPath:{1}", curEquipData.ID, pathes[0]));
                    return;
                }
                var fbx = objs[0] as GameObject;
                //Debug.LogError("Head onPutOn 333：" + (fbx==null));
                this.meshPath = pathes[0];
                var meshObjName = Path.GetFileNameWithoutExtension(pathes[0]);
                if (meshObjName == ACTSystemTools.EmptyPrefab)
                    return;
                //Debug.LogError("Head onPutOn 444：" + meshObjName);
                Transform meshObj = fbx.transform.Find(meshObjName);
                if (meshObj == null)
                {
                    ACTLogger.Error(string.Format("{0} 中没有找到：{1} 联系美术修复;", pathes[0], meshObjName));
                    return;
                }
                if (objs[1] == null)
                {
                    ACTLogger.Error(string.Format("头部加载出错 {0} MaterialPath:{1}", curEquipData.ID, pathes[1]));
                    return;
                }
                Material material = objs[1] as Material;
                this.materialPath = pathes[1];
                SkinnedMeshRenderer skinnedMeshRender;
                try
                {
                    skinnedMeshRender = meshObj.GetComponent<SkinnedMeshRenderer>();
                }
                catch (Exception)
                {
                    skinnedMeshRender = null;
                }
                if (skinnedMeshRender != null)
                {
                    OnPutOn(curEquipData.Slot, skinnedMeshRender, material, colorR, colorG, colorB);
                }
                else
                {
                    var meshRender = meshObj.GetComponent<MeshRenderer>();
                    var meshFilter = meshObj.GetComponent<MeshFilter>();
                    OnPutOn(curEquipData.Slot, meshFilter, meshRender, material, colorR, colorG, colorB);
                }
                if (flowID == 0)
                {
                    TakeOffFlow();
                }
                else
                {
                    FlowEnable(flowFlashEffect, new[] { headSkinnedRender.material }, true, flowData);
                    if (onLoadFlowFinished != null)
                    {
                        onLoadFlowFinished();
                    }
                }
                if (onLoadEquipFinished != null)
                    onLoadEquipFinished();
            } );
        }
        //先做个没动画的puton
        void OnPutOn(string slotName, MeshFilter newMeshFilter, MeshRenderer newMeshRender, Material material, string colorR, string colorG, string colorB)
        {
            if (!string.IsNullOrEmpty(slotName))
            {
                var headSlot = _actor.boneController.GetBoneByName(slotName);
                if (headSlot == null)
                    return;
                var headRender = headSlot.GetComponent<MeshRenderer>();
                var headMeshFilter = headSlot.GetComponent<MeshFilter>();
                headMeshFilter.sharedMesh = newMeshFilter.sharedMesh;
                headRender.material = material;
            }
        }
        //private GameObject _headGameObject;
        void OnPutOn(string slotName, SkinnedMeshRenderer newSMRender, Material material, string colorR, string colorG, string colorB)
        {
            if (!string.IsNullOrEmpty(slotName))
            {
                var headSlot = _actor.boneController.GetBoneByName(slotName);
                if (headSlot == null)
                    return;
                if (flowFlashEffect == null)
                {
                    flowFlashEffect = headSlot.gameObject.AddComponent<ArtTech.FlowFlashEffect>();
                    flowFlashEffect.EnableEffect = false;
                }
                if (headSkinnedRender == null) headSkinnedRender = headSlot.GetComponent<SkinnedMeshRenderer>();
                headSkinnedRender.sharedMesh = newSMRender.sharedMesh;
                List<Transform> bones = new List<Transform>();
                for (int i = 0; i < newSMRender.bones.Length; i++)
                {
                    var bone = _actor.boneController.GetBoneByName(newSMRender.bones[i].name);
                    if (bone == null)
                    {
                        ACTLogger.Error("bone is none " + newSMRender.bones[i].name);
                        continue;
                    }
                    bones.Add(bone);
                }
                headSkinnedRender.bones = bones.ToArray();
                headSkinnedRender.sharedMaterial = null;
                headSkinnedRender.sharedMaterial = material;

                headSkinnedRender.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                headSkinnedRender.receiveShadows = false;
            }
        }   
        protected override void OnTakeOff()
        {
            if (curEquipData == null) return;
            var headSlot = _actor.boneController.GetBoneByName(curEquipData.Slot);
            if (headSlot == null)
            {
                //ACTSystemDebug.LogError("headSlot is not found:" + curEquipData.Slot);
                return;
            }
            var headSkinnedMeshRenderer = headSlot.transform.GetComponent<SkinnedMeshRenderer>();
            headSkinnedMeshRenderer.sharedMesh = null;
            headSkinnedMeshRenderer.sharedMaterial = null;
            curEquipData = null;
            headSkinnedRender = null;

        }

        protected override void OnPutOnFlow(ACTDataEquipFlow data)
        {
            curEquipFlowID = data.ID;
            curEquipFlowData = data;
            OnPutOn(curEquipData, curEquipParticleID, data.ID, _isNormalQuality);
        }

        protected override void OnTakeOffFlow()
        {
            var headSlot = _actor.boneController.GetHumanHead();
            if (headSlot == null)
            {
                return;
            }
            if (headSkinnedRender != null && headSkinnedRender.sharedMaterial == null)
            {
                return;
            }
            FlowEnable(flowFlashEffect, new Material[] { }, false);
        }
    }
}
