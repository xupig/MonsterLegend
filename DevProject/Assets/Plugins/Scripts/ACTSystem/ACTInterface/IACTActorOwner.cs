﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    public interface IACTActorOwner
    {
        uint GetActorOwnerID();
        bool IsPlayer();
    }
}
