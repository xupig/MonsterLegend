﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    public interface IACTSoundPlayer
    {
        void Play(AudioSource source, string strPath, float volume, bool loop = false);
    }
}
