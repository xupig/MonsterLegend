﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACTSystem
{
    public interface IACTLogger
    {
        void Info(string msg);
        void Error(string msg);
        void Warning(string msg);

    }
}
