﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace ACTSystem
{
    public interface IACTAssetLoader
    {
        void GetObject(string path, ACT_VOID_OBJ callback);

        void GetObjects(string[] strPaths, ACT_VOID_OBJS callback);

        void GetGameObject(string strPath, ACT_VOID_OBJ callback);

        void GetGameObjects(string[] strPaths, ACT_VOID_OBJS callback);

        void PreloadAsset(string[] strPaths, ACT_VOID callback);

        void ReleaseAsset(string strPath);

        void ReleaseAssets(string[] strPaths);

        string GetResMapping(string resourceName);

        string[] GetResMappings(string[] resourcesName);
    }
}
