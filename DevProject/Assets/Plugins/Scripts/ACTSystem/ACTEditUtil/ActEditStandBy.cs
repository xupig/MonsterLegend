﻿
using UnityEngine;
namespace ACTSystem
{
    [RequireComponent(typeof(ACTEditDriver))]
    public class ActEditStandBy : MonoBehaviour
    {
        public ACTDataSettingStandBy CustomSettingStandBy;

        private float _nextPlayActionTimeStamp = 0;
        private bool _standByState = false;
        private bool _playFlag = false;
        private bool _startNextAction = false;
        private float _nextJudgeTimeStamp = 0;

        public void Play()
        {
            _playFlag = true;
        }

        void Update()
        {
            if (!_playFlag)
            {
                return;
            }
            if (CustomSettingStandBy == null || CustomSettingStandBy.Actor == null)
            {
                return;
            }
            if (CustomSettingStandBy.ActionIdList == null || CustomSettingStandBy.ActionIdList.Count == 0)
            {
                return;
            }
            if (_nextPlayActionTimeStamp > 0 && UnityPropUtils.realtimeSinceStartup >= _nextPlayActionTimeStamp)
            {
                _nextPlayActionTimeStamp = 0;
                _standByState = true;
                _startNextAction = false;
                if (CustomSettingStandBy.Actor.animationController.IsReady() &&
                    CustomSettingStandBy.Actor.animationController.GetSpeed() == 0 &&
                    CustomSettingStandBy.Actor.animationController.GetSpeedX() == 0)
                {
                    PlayStandByAction();
                }
                return;
            }

            if (_nextJudgeTimeStamp == 0)
            {
                _nextJudgeTimeStamp = UnityPropUtils.realtimeSinceStartup + 1f;
            }
            else if (UnityPropUtils.realtimeSinceStartup >= _nextJudgeTimeStamp)
            {
                _nextJudgeTimeStamp = 0;
                if (CustomSettingStandBy.Actor.animationController.IsReady())
                {
                    _standByState = false;
                    if (CustomSettingStandBy.Actor.animationController.GetSpeed() == 0 &&
                        CustomSettingStandBy.Actor.animationController.GetSpeedX() == 0)
                    {
                        if (!_startNextAction)
                        {
                            _nextPlayActionTimeStamp = UnityPropUtils.realtimeSinceStartup + Random.Range(CustomSettingStandBy.MinDuration, CustomSettingStandBy.MaxDuration);
                            _startNextAction = true;
                        }
                    }
                    else
                    {
                        //StopStandByAction();
                        _startNextAction = false;
                    }
                }
            }
        }

        private void PlayStandByAction()
        {
            int randomAction = CustomSettingStandBy.ActionIdList[Random.Range(0, CustomSettingStandBy.ActionIdList.Count)];
            CustomSettingStandBy.Actor.animationController.PlayAction(randomAction);
        }

        private void StopStandByAction()
        {
            _standByState = false;
            CustomSettingStandBy.Actor.animationController.ReturnReady();
        }
    }
}
