﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    public enum SkillState
    {
        NONE,
        PLAYING,
        ENDED
    }

    public class ACTEventBeHitChangeMaterialConfig
    {
        public ACTEventBeHitChangeMaterial eventBeHitChangeMaterial;
        public float triggerTime = 0;
        public ACTActor target;
    }

    public class ACTDoSkill
    {

        protected SkillState m_curState = SkillState.NONE;
        protected ACTActor m_caster;
        //protected ACTWeapon[] m_weapons;
        protected Rigidbody m_weaponRigidbody;

        protected float m_duration = 1;
        protected float m_pastTime = 0;

        protected ACTDataSkill m_curData;
        protected bool[] m_dataFlags;

        protected ACTEventBeHitVisualFX m_eventBeHitVisualFX;
        protected ACTEventStateChange m_curStateChange;
        protected ACTEventBeHitSetting m_curBeHitSetting;
        protected ACTEventBeHitSoundFX m_curBeHitSound;
        protected ACTEventTracerVisualFX m_curTracerVisualFX;
        protected ACTEventLaserVisualFX m_curLaserVisualFX;
        protected List<ACTEventBeHitChangeMaterialConfig> m_eventBeHitChangeMaterialList = new List<ACTEventBeHitChangeMaterialConfig>();
        //protected ACTEventBulletVisualFX m_curBulletVisualFX;
        //protected ACTEventTargetDetector m_targetDetector;

        public int skillID;

        public ACTDoSkill(ACTActor actor, ACTDataSkill data)
        {
            if (data == null)
            {
                ACTLogger.Error("ACTDoSkill data == null");
                return;
            }
            //ACTSystemLog.LogError(data.ID + "--" + data.Duration);
            m_caster = actor;
            if (actor.action != null) actor.action.End();
            actor.action = this;

            skillID = data.ID;
            //ACTSystemLog.Log("data.GetEvents().Count:" + data.GetEvents().Count);
            m_duration = data.Duration;

            m_dataFlags = new bool[data.GetEvents().Count];
            m_curData = data;
            ACTEventListener.Get(m_caster.gameObject).onUpdate += _OnUpdate;
            m_caster.actorState.SpeedMlp = 0;
        }

        ~ACTDoSkill()
        {
            m_caster = null;
        }

        public bool CanLink()
        {
            return m_curData.LinkTime <= this.m_pastTime;
        }

        void Play()
        { 
            m_curState = SkillState.PLAYING;
        }

        void ReturnDefault()
        {
            m_caster.weaponController.ClearAllWeaponTrailEmit();
            OnStateChange(new ACTEventStateChange());
        }

        public void End() 
        {          
            ACTEventListener.Get(m_caster.gameObject).onUpdate -= _OnUpdate;
            m_curState = SkillState.ENDED;
            ReturnDefault();
            m_caster.action = null;
            m_caster.actorController.SetSlowRate(0);
            m_caster.actorController.Stop();
        }

        protected Vector3 m_lastPosition1 = Vector3.zero;
        protected Vector3 m_lastPosition2 = Vector3.zero;
        protected Vector3 m_lastPosition3 = Vector3.zero;

        protected int[] m_gameObjectIDList = new int[20];
        protected Dictionary<int, int> m_gameObjectIDMap = new Dictionary<int, int>();
        virtual protected void _OnUpdate(GameObject go)
        {
            if (m_curState == SkillState.NONE)
            {
                Play();
            }
            else if (m_curState == SkillState.PLAYING)
            {
                m_pastTime += UnityPropUtils.deltaTime;
                if (m_pastTime >= m_duration)
                {
                    End();
                }
                else
                {
                    var events = m_curData.GetEvents();
                    
                    for (int i = 0; i < events.Count; i++)
                    {
                        if (!events[i].IsUsed)
                        {
                            m_dataFlags[i] = true;
                            continue;
                        }
                        if (!m_dataFlags[i] && events[i].Delay < m_pastTime)
                        {
                            DoSubFX(events[i]);
                            m_dataFlags[i] = true;
                        }
                    }
                }
            }
            CheckChangeMaterialTrigger();
        }

        private void CheckChangeMaterialTrigger()
        {
            for (int i = 0; i < m_eventBeHitChangeMaterialList.Count; i++)
            {
                if (UnityPropUtils.realtimeSinceStartup >= m_eventBeHitChangeMaterialList[i].triggerTime && m_eventBeHitChangeMaterialList[i].target != null)
                {
                    m_eventBeHitChangeMaterialList[i].target.materialAnimationController.PlayMaterialAnimation(m_eventBeHitChangeMaterialList[i].eventBeHitChangeMaterial);
                    m_eventBeHitChangeMaterialList.RemoveAt(i);
                }
            }
        }

        protected void DoSubFX(ACTEventBase subFX)
        {
            //ACTSystemLog.LogError(subFX.EventFXType + "---" + m_pastTime + "---" + subFX.Delay);
            if (subFX.EventFXType == EventType.AnimationFX)
            {
                OnAnimationFX(subFX as ACTEventAnimationFX);
            }
            else if (subFX.EventFXType == EventType.VisualFX)
            {
                ACTVisualFXManager.GetInstance().PlayACTEventVisualFX(subFX as ACTEventVisualFX, m_caster, Vector3.zero);
            }
            else if (subFX.EventFXType == EventType.SoundFX)
            {
                m_caster.soundController.PlayHitSoundFX(subFX as ACTEventSoundFX);
            }
            else if (subFX.EventFXType == EventType.BeHitVisualFX)
            {
                m_eventBeHitVisualFX = subFX as ACTEventBeHitVisualFX;
            }
            else if (subFX.EventFXType == EventType.StateChange)
            {
                OnStateChange(subFX as ACTEventStateChange);
            }
            else if (subFX.EventFXType == EventType.BeHitSetting)
            {
                OnBeHitSetting(subFX as ACTEventBeHitSetting);
            }
            else if (subFX.EventFXType == EventType.BeHitSoundFX)
            {
                m_curBeHitSound = subFX as ACTEventBeHitSoundFX;
            }
            else if (subFX.EventFXType == EventType.TargetDetector)
            {
                OnTargetDetector(subFX as ACTEventTargetDetector);
            }
            else if (subFX.EventFXType == EventType.WeaponState)
            {
                OnWeaponState(subFX as ACTEventWeaponState);
            }
            else if (subFX.EventFXType == EventType.TracerVisualFX)
            {
                m_curTracerVisualFX = subFX as ACTEventTracerVisualFX;
            }
            else if (subFX.EventFXType == EventType.BulletVisualFX)
            {
                OnBulletVisualFX(subFX as ACTEventBulletVisualFX);
            }
            else if (subFX.EventFXType == EventType.LaserVisualFX)
            {
                m_curLaserVisualFX = subFX as ACTEventLaserVisualFX;
            }
            else if (subFX.EventFXType == EventType.PostEffectFX)
            {
                OnPostEffectFX(subFX as ACTEventPostEffectFX);
            }
            else if (subFX.EventFXType == EventType.TriggerVisualFX)
            {
                m_caster.fxController.SetTriggerVisualFX(subFX as ACTEventTriggerVisualFX);
            }
            else if (subFX.EventFXType == EventType.ChangeMaterial)
            {
                m_caster.materialAnimationController.PlayMaterialAnimation(subFX as ACTEventChangeMaterial);
            }
            else if (subFX.EventFXType == EventType.CameraAnimation)
            {
                OnCameraAnimation(subFX as ACTEventCameraAnimation);
            }
            else if (subFX.EventFXType == EventType.GhostShadow)
            {
                OnGhostShadow(subFX as ACTEventGhostShadow);
            }
            else if (subFX.EventFXType == EventType.BeHitChangeMaterial)
            {
                ACTEventBeHitChangeMaterialConfig config = new ACTEventBeHitChangeMaterialConfig();
                config.eventBeHitChangeMaterial = subFX as ACTEventBeHitChangeMaterial;
                m_eventBeHitChangeMaterialList.Add(config);
            }
        }

        protected void OnStateChange(ACTEventStateChange stateChange)
        {
            m_curStateChange = stateChange;
            var animator = m_caster.GetComponent<Animator>();
            if (animator != null)
            {
                animator.applyRootMotion = stateChange.ApplyRootMotion;
            }
            m_caster.actorState.SpeedMlp = stateChange.SpeedMlp;
            m_caster.actorState.CanTurn = stateChange.CanTurn;
            m_caster.actorState.CanMove = stateChange.CanMove;
            m_caster.actorState.IsReady = stateChange.IsReady;

            if(!string.IsNullOrEmpty(stateChange.SkinnedSlot))
            {
                string[] splitSlots = stateChange.SkinnedSlot.Split(',');
                for (int i = 0; i < splitSlots.Length; i++)
                {
                    Transform slot = m_caster.boneController.GetBoneByName(splitSlots[i]);
                    SkinnedMeshRenderer smr = slot.GetComponent<SkinnedMeshRenderer>();
                    if (smr)
                    {
                        smr.enabled = stateChange.ShowSkinned;
                    }
                }
            }
        }

        protected void OnAnimationFX(ACTEventAnimationFX setting)
        {
            m_caster.animationController.PlayAnimationFX(setting);
            m_caster.actorController.Jump(setting.UpVelocity, setting.KeepFloatTime);
            m_caster.actorController.SetSlowRate(setting.SlowRate);
            m_caster.actorController.Move(m_caster.transform.forward.normalized * setting.PushForce); 
        }

        protected void OnBeHitSetting(ACTEventBeHitSetting setting)
        {
            m_curBeHitSetting = setting;
        }

        protected void OnTargetDetector(ACTEventTargetDetector setting)
        {
            //Debug.LogError("1_____________________________________________");
            //ACTSystemLog.LogError("setting.Offset.y:" + setting.Offset.y + "aa" + this.skillID);
            var origin = m_caster.transform.position 
                + m_caster.transform.right.normalized * setting.OffsetReal.x
                + m_caster.transform.up.normalized * setting.OffsetReal.y
                + m_caster.transform.forward.normalized * setting.OffsetReal.z
                ;
            var radius = setting.Radius;

            Quaternion rot = Quaternion.Euler(new Vector3(setting.DiretionReal.x, setting.DiretionReal.y, setting.DiretionReal.z));
            Matrix4x4 m = Matrix4x4.TRS(Vector3.zero, rot, Vector3.one);
            var direction = m.MultiplyPoint3x4( m_caster.transform.forward );
            
            var distance = setting.Distance;
            
            if (setting.FilterType == ACTTargetDetectorType.SPHERE_CAST_ALL)
            {
                RaycastHit[] hitList = Physics.SphereCastAll(origin, radius, direction.normalized, distance, 1 << ACTLayerUtils.Actor);
                for (int k = 0; k < hitList.Length; k++)
                {
                    var obj = hitList[k].collider.gameObject;
                    var act = obj.GetComponent<ACTActor>();
                    if (act == null) continue;
                    BeforeTrigerEnter(null, act, Vector3.zero);
                }
                //Debug.DrawRay(origin, direction.normalized * distance, Color.blue, 6);
                ACTSystemTools.DrawSphereCast(origin, direction, radius, distance);
                
            }
            else if (setting.FilterType == ACTTargetDetectorType.OVERLAP_SPHERE)
            {
                Collider[] colliderList = Physics.OverlapSphere(origin, radius, 1 << ACTLayerUtils.Actor);
                for (int k = 0; k < colliderList.Length; k++)
                {
                    var obj = colliderList[k].gameObject;
                    var act = obj.GetComponent<ACTActor>();
                    if (act == null) continue;
                    BeforeTrigerEnter(null, act, Vector3.zero);
                }
                ACTSystemTools.DrawCircleRange(origin, radius);
            }
        }

        protected void BeforeTrigerEnter(GameObject go, ACTActor target, Vector3 hitPoint)
        {
            if (m_curTracerVisualFX != null)
            {
                ACTVisualFXManager.GetInstance().PlayACTTracer(m_curTracerVisualFX,
                    this.m_caster, target, ()=>{_OnTriggerEnter(go, target, hitPoint);});
            }
            else if (m_curLaserVisualFX != null)
            {
                ACTVisualFXManager.GetInstance().PlayACTLaser(m_curLaserVisualFX,
                    this.m_caster, target, () => { _OnTriggerEnter(go, target, hitPoint); });
                _OnTriggerEnter(go, target, hitPoint);
            }
            else
            {
                _OnTriggerEnter(go, target, hitPoint);
            }
        }

        protected void OnWeaponState(ACTEventWeaponState setting)
        {
            for (int j = 0; j < setting.WeaponStateList.Count; j++)
            {
                var settingData = setting.WeaponStateList[j];
                m_caster.weaponController.SetWeaponTrailEmit(settingData.WeaponID, settingData.TrailEmit);
            }
        }
 
        protected void OnBulletVisualFX(ACTEventBulletVisualFX setting)
        {
            var offsetMat = Matrix4x4.TRS(new Vector3(m_caster.position.x, m_caster.position.y, m_caster.position.z),
                                          Quaternion.LookRotation(m_caster.transform.forward), Vector3.one);
            var targetLocation = offsetMat.MultiplyPoint3x4(setting.TargetPosition);
            ACTVisualFXManager.GetInstance().PlayACTBullet(setting, this.m_caster, targetLocation, () =>
            {
                if (setting.HitVisualEvent > 0)
                {
                    ACTEventBase subFX = ACTRunTimeData.GetEventBySkillIDAndEventIdx(setting.SkillID, setting.HitVisualEvent);
                    if (subFX is ACTEventVisualFX)
                    {
                        ACTVisualFXManager.GetInstance().PlayACTEventVisualFX(subFX as ACTEventVisualFX, this.m_caster, targetLocation);
                    }
                }
            });
        }

        protected void OnPostEffectFX(ACTEventPostEffectFX setting)
        {
            ACTPostEffectFXManager.GetInstance().PlayACTEventVisualFX(setting);
        }

        protected void OnCameraAnimation(ACTEventCameraAnimation setting)
        {
            var camera = Camera.main;
            if (camera == null) return;
            var ani = camera.gameObject.GetComponent<ACTEditCameraAnimation>();
            if (ani == null)
            {
                ani = camera.gameObject.AddComponent<ACTEditCameraAnimation>();
            }
            ani.Play(setting.Perfab_Name, setting.Duration, this.m_caster, setting.Slot);
        }

        protected void OnGhostShadow(ACTEventGhostShadow setting)
        {
            var color = new Color(setting.XrayColorReal.x, setting.XrayColorReal.y, setting.XrayColorReal.z);
            int uid = setting.SkillID * 100 + setting.EventIndex;
            m_caster.equipController.GhostShadow(setting.Duration, setting.Interval, setting.FadeTime, setting.MinDistance, color, setting.Rim, setting.Inside, uid);
        }

        virtual protected void _OnTriggerEnter(GameObject go, ACTActor target, Vector3 hitPoint)
        {
            if (target == m_caster) return;
            //ACTSystemLog.LogError("_OnTriggerEnter::"+target);
            if (m_curStateChange != null)
            {
                if (m_curStateChange.AttackLockTarget)
                {
                    m_caster.FaceTo(target);
                }
            }
            target.FaceTo(m_caster);
            //ACTSystemLog.LogError("_OnTriggerEnter2::" + target);
            if (m_curBeHitSetting != null)
            {
                var animationController = target.animationController;//target.gameObject.GetComponent<ACTAnimationController>();
                var actorController = target.actorController;//target.gameObject.GetComponent<ACTActorController>();
                actorController.Stop();

                int beHitAction = 0;
                float raiseForce = 0;
                if (target.actorState.OnGround)
                {
                    beHitAction = m_curBeHitSetting.BeHitAction;
                    raiseForce = m_curBeHitSetting.RaiseForce;
                }
                else
                {
                    beHitAction = m_curBeHitSetting.BeHitAirAction;
                    raiseForce = m_curBeHitSetting.AirRaiseForce;
                }
                var pf = m_curBeHitSetting.PushForce - target.actorData.DePushForce;
                var rf = raiseForce - target.actorData.DeRaiseForce;
                var fn = (target.transform.position - m_caster.transform.position).normalized;//-target.transform.forward.normalized;
                
                if (rf != 0)
                {
                    actorController.Jump(rf, m_curBeHitSetting.KeepFloatTime);
                    actorController.SetSlowRate(m_curBeHitSetting.SlowRate);
                    actorController.Move(new Vector3(fn.x * pf, 0, fn.z * pf));
                }
                else if (pf > 0)
                {
                    actorController.SetSlowRate(m_curBeHitSetting.SlowRate);
                    actorController.Move(fn * pf);
                }

                animationController.PlayAction(beHitAction);

                if (m_curBeHitSetting.HitTimeScaleKeep>0)
                {
                    target.animationController.PauseTo((int)m_curBeHitSetting.HitTimeScaleKeep);
                }
                if (m_curBeHitSetting.HitTimeScale > 0)
                {
                    m_caster.animationController.PauseTo((int)m_curBeHitSetting.HitTimeScale);
                }
                if (m_curBeHitSetting.HitVisualEvent > 0)
                {
                    ACTEventBase subFX = ACTRunTimeData.GetEventBySkillIDAndEventIdx(m_curBeHitSetting.SkillID, m_curBeHitSetting.HitVisualEvent);
                    if (subFX is ACTEventChangeMaterial)
                    {
                        target.materialAnimationController.PlayMaterialAnimation(subFX as ACTEventChangeMaterial);
                    }
                }
            }
            //ACTSystemLog.LogError("_OnTriggerEnter3::" + m_eventBeHitVisualFX);
            if (m_eventBeHitVisualFX != null)
            {
                ACTVisualFXManager.GetInstance().PlayACTEventVisualFX(m_eventBeHitVisualFX, target, hitPoint, 0, VisualFXPriority.H, this.m_caster);
            }

            if (m_eventBeHitChangeMaterialList.Count > 0)
            {
                for (int i = 0; i < m_eventBeHitChangeMaterialList.Count; i++)
                {
                    m_eventBeHitChangeMaterialList[i].target = target;
                    m_eventBeHitChangeMaterialList[i].triggerTime = UnityPropUtils.realtimeSinceStartup + m_eventBeHitChangeMaterialList[i].eventBeHitChangeMaterial.DelayTriggerTime;
                }
            }
            //ACTSystemLog.LogError("_OnTriggerEnter4::" + target);
            //播放受击音效
            if (m_curBeHitSound != null)
            {
                m_caster.soundController.PlayBeHitSoundFX(m_curBeHitSound);
            }
            //播放目标自身受击音效
            target.soundController.PlaySelfBeHitSound();
            
            //if (m_curBeHitSound != null)
            //{
            //    var cr = UnityEngine.Random.Range(0f, 1f);
            //    var list = m_curBeHitSound.BeHitSoundList;
            //    ACTSystemLog.LogError("[ACTDoSkill:_OnTriggerEnter]=>1_______________________cr");
            //    for (int i = 0; i < list.Count; i++)
            //    {
            //        var da = list[i];
            //        cr = cr - da.PlayRate;
            //        if (cr < 0)
            //        {
            //            target.soundController.Play(da.Clip_Name, da.Volume,false,ACTAudioSourceType.BehitSound);
            //            break;
            //        }
            //    }
            //}     
   

        }

        virtual protected void _OnTriggerExit(GameObject go, ACTActor target, Vector3 hitPoint)
        {
            if (target == m_caster) return;
        }

    }
}
