﻿
using UnityEngine;
namespace ACTSystem
{
    public class ACTEditCameraAnimation : MonoBehaviour
    {
        bool _isPlaying = false;
        string _animationPath = string.Empty;
        GameObject _animationGO = null;
        Transform _animatioTrans = null;
        ACTActor _target;
        string _slotName = null;
        Transform _targetTrans = null;
        Camera _camera;

        float _duration = 0;

        public void Play(string animationPath, float duration, ACTActor target, string slotName)
        {
            _camera = Camera.main;
            _isPlaying = true;
            _animationPath = animationPath;
            _target = target;
            _slotName = slotName;
            _duration = duration;
            Load();
        }

        public void Load()
        {
            string cachePath = _animationPath;
            var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
            //no ResourceMappingManager
            assetLoader.GetGameObject(cachePath, (result) =>
            {
                GameObject obj = result as GameObject;
                if (obj == null)
                {
                    Debug.LogError(string.Format("切换为相机动画模式失败，因为路径为{0}找不到资源", _animationPath));
                    return;
                }
                if (cachePath != _animationPath)
                {
                    GameObject.Destroy(obj);
                    return;
                }
                //GameObject.DontDestroyOnLoad(obj);
                _animationGO = obj;
                _animatioTrans = obj.transform;
                Transform cameraSlot = _animationGO.transform.Find("camera_slot");
                _camera.transform.SetParent(cameraSlot, false);
                _camera.transform.localPosition = Vector3.zero;
                _camera.transform.localEulerAngles = Vector3.zero;
                var mmc = _camera.gameObject.GetComponent("MogoMainCamera") as MonoBehaviour;
                if (mmc != null) mmc.enabled = false;
                ResetAnimationRoot();
            });
        }

        private void ResetAnimationRoot()
        {
            if (_target != null)
            {
                _targetTrans = _target.transform;
                if (!string.IsNullOrEmpty(_slotName))
                {
                    var slot = _target.boneController.GetBoneByName(_slotName);
                    if (slot != null)
                    {
                        _targetTrans = slot;
                    }
                }
            }
        }

        public void Update() 
        {
            if (!_isPlaying) return;
            _duration = _duration - UnityPropUtils.deltaTime;
            if (_duration < 0)
            {
                Stop();
                return;
            }
            if (_targetTrans == null) return;
            _animatioTrans.position = _targetTrans.position;
        }

        public void Stop() {
            _isPlaying = false;
            _animationPath = string.Empty;
            _camera.transform.SetParent(null, false);
            //GameObject.DontDestroyOnLoad(camera.gameObject);
            if (_animationGO != null) GameObject.Destroy(_animationGO);
            _animationGO = null;
            _animatioTrans = null;
            _targetTrans = null;
            var mmc = _camera.gameObject.GetComponent("MogoMainCamera") as MonoBehaviour;
            if (mmc != null) mmc.enabled = true;
            _camera = null;
        }

    }
}
