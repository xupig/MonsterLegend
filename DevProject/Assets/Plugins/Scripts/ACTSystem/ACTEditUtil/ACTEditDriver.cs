﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{

    public class ACTEditDriver:MonoBehaviour
    {

        public ACTDataSetting CustomSetting;

        public int CurrentSelectSkillID;

        void Awake()
        { 
            
        }

        void Start()
        {
            RenderTextureMaterialHelper.curRTLoader = new ACTSimpleRTLoader();
        }

        void Update()
        {
            UnityPropUtils.time = Time.time;
            UnityPropUtils.deltaTime = Time.deltaTime;
            UnityPropUtils.realtimeSinceStartup = Time.realtimeSinceStartup;
            //根据键盘按键播放技能
            for (int i = 0; i < CustomSetting.KeyActionList.Length; i++)
            {
                if (Input.GetKeyDown(CustomSetting.KeyActionList[i].KeyCode))
                {
                    PlayAction(CustomSetting.KeyActionList[i].ActionID);
                    break;
                }
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                PlayAction(CurrentSelectSkillID);
            }
        }

        void PlayAction(int actionID)
        {
            //Debug.Log("PlayAction::" + actionID);
            var actData = ACTRunTimeData.GetSkillData(actionID);
            if (CustomSetting.Actor != null && actData != null)
            {
                new ACTDoSkill(CustomSetting.Actor, actData);
            }
            //var actData = ACTRunTimeData.GetActionData(actionID);
            //if (CustomSetting.Actor != null && actData != null)
            //{
            //    new ACTDoAction(CustomSetting.Actor, actData);
            //}
        }
    }
}
