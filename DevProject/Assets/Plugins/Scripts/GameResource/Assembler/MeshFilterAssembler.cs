﻿using UnityEngine;

namespace GameResource
{
    public class MeshFilterAssembler : AssetAssembler
    {

        public override void Assemble(GameObject go, string[] tokens)
        {
            //if (go.CompareTag(DelayedConfig.TAG_DELAYED))//
            //{
            //    DelayedMeshFilterAssembler com = go.AddComponent<DelayedMeshFilterAssembler>();
            //    com.tokens = tokens;
            //}
            //else
            //{
            string key = tokens[0];
            string meshHostName = tokens[1];
            MeshFilter filter = go.GetComponent<MeshFilter>();
            if (filter != null)
            {
                Object obj = ObjectPool.Instance.GetAssemblyObject(key) as Object;
                if (obj is Mesh)
                {
                    filter.mesh = obj as Mesh;
                    return;
                }
                GameObject model = obj as GameObject;
                if (model == null) return;
                Transform hostTransform = model.transform.Find(meshHostName);
                if (hostTransform == null)
                {
                    hostTransform = model.transform;
                }
                if (hostTransform != null)
                {
                    MeshFilter meshFilter = hostTransform.GetComponent<MeshFilter>();
                    if (meshFilter != null && meshFilter.sharedMesh != null)
                    {
                        filter.mesh = meshFilter.sharedMesh;
                        return;
                    }
                }
                var newMesh = GetMesh(model.transform, meshHostName);
                if (newMesh != null)
                {
                    filter.mesh = newMesh;
                }
                else
                {
                    Debug.LogError(string.Format("Mesh {0} not found in {1}!", meshHostName, key));
                }
            }
            //}
        }

        private Mesh GetMesh(Transform modelTransform, string meshHostName)
        {
            if (modelTransform.name == meshHostName)
            {
                MeshFilter meshFilter = modelTransform.GetComponent<MeshFilter>();
                if (meshFilter != null && meshFilter.sharedMesh != null)
                {
                    return meshFilter.sharedMesh;
                }
                SkinnedMeshRenderer skinnedMeshFilter = modelTransform.GetComponent<SkinnedMeshRenderer>();
                if (skinnedMeshFilter != null && skinnedMeshFilter.sharedMesh != null)
                {
                    return skinnedMeshFilter.sharedMesh;
                }
            }
            for (int i = 0; i < modelTransform.childCount; i++)
            {
                Transform childTransform = modelTransform.GetChild(i);
                Mesh mesh = GetMesh(childTransform, meshHostName);
                if (mesh != null) return mesh;
            }
            return null;
        }
    }
}