﻿using System.Collections.Generic;
using UnityEngine;

namespace GameResource
{
    public class ParticleSystemRendererAssembler : AssetAssembler
    {

        public override void Assemble(GameObject go, string[] tokens)
        {
            ParticleSystemRenderer particleSystemRenderer = go.GetComponent<ParticleSystemRenderer>();
            if (particleSystemRenderer != null)
            {
                var meshes = new List<Mesh>();
                for (int i = 0; i < tokens.Length; i += 2)
                {
                    string key = tokens[i];
                    string meshName = tokens[i + 1];
                    GameObject model = ObjectPool.Instance.GetAssemblyObject(key) as GameObject;
                    //FakeFBX fakeFBX = model.GetComponent<FakeFBX>();
                    //if (fakeFBX)
                    //{
                    //    meshes.Add(fakeFBX.GetMesh(meshName));
                    //    continue;
                    //}
                    Transform hostTransform = model.transform.Find(meshName);
                    if (hostTransform == null)
                    {
                        hostTransform = model.transform;
                    }
                    if (hostTransform != null)
                    {
                        MeshFilter meshFilter = hostTransform.GetComponent<MeshFilter>();
                        if (meshFilter != null && meshFilter.sharedMesh != null)
                        {
                            meshes.Add(meshFilter.sharedMesh);
                        }
                    }
                }
                particleSystemRenderer.SetMeshes(meshes.ToArray());
            }
        }

    }
}
