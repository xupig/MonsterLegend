﻿using UnityEngine;

namespace GameResource
{
    public class LightmapUVAssembler : AssetAssembler
    {

        public override void Assemble(GameObject go, string[] tokens)
        {
            if (!go)
                return;
            DelayedLightmapUVAssembler com = go.AddComponent<DelayedLightmapUVAssembler>();
            com.tokens = tokens;
            //if (go.CompareTag(DelayedConfig.TAG_DELAYED))
            //{
            //    DelayedLightmapUVAssembler com = go.AddComponent<DelayedLightmapUVAssembler>();
            //    com.tokens = tokens;
            //}
            //else
            //{
            //    if (tokens.Length != 6)
            //    {
            //        Debug.LogError("LightmapUV tokens.Length not equals 6, value: " + tokens.Length);
            //        return;
            //    }
            //    string type = tokens[0];
            //    var index = 0;
            //    var lightmaps = LightmapSettings.lightmaps;
            //    for (int i = 0; i < lightmaps.Length; i++)
            //    {
            //        if (lightmaps[i].lightmapFar.name == tokens[1])
            //        {
            //            index = i;
            //            break;
            //        }
            //    }

            //    var scaleOffset = new Vector4(float.Parse(tokens[2]), float.Parse(tokens[3]), float.Parse(tokens[4]), float.Parse(tokens[5]));

            //    if (type == "render")
            //    {
            //        var render = go.GetComponent<Renderer>();
            //        if (render != null)
            //        {
            //            render.lightmapIndex = index;
            //            render.lightmapScaleOffset = scaleOffset;
            //        }
            //    }
            //    else if (type == "terrain")
            //    {
            //        var terr = go.GetComponent<Terrain>();
            //        if (terr != null)
            //        {
            //            terr.lightmapIndex = index;
            //            terr.lightmapScaleOffset = scaleOffset;
            //        }
            //    }
            //    else
            //    {
            //        Debug.LogError("LightmapUV unknown type: " + type);
            //    }
            //}
        }

    }
}
