﻿using UnityEngine;

namespace GameResource
{
    public class AnimatorControllerAssembler : AssetAssembler
    {
        public override void Assemble(GameObject go, string[] tokens)
        {
            string controllerKey = tokens[0];
            string fbxKey = tokens[1];
            Animator animator = go.GetComponent<Animator>();
            if (animator != null)
            {
                animator.gameObject.AddComponent<AnimatorProxy>();
                if (!string.IsNullOrEmpty(controllerKey))
                {
                    //if (go.CompareTag(DelayedConfig.TAG_DELAYED))
                    //{
                    //    //Debug.LogError("AnimatorControllerAssembler TAG_DELAYED");
                    //    //foreach (var item in tokens)
                    //    //{
                    //    //    Debug.LogError("tokens: " + item);
                    //    //}
                    //    DelayedAnimatorControllerAssembler com = go.AddComponent<DelayedAnimatorControllerAssembler>();
                    //    com.tokens = tokens;
                    //}
                    //else
                    //{
                    //Debug.LogError("AnimatorControllerAssembler !TAG_DELAYED");
                    animator.runtimeAnimatorController = ObjectPool.Instance.GetAssemblyObject(controllerKey) as AnimatorOverrideController;
                    //}
                }
                if (!string.IsNullOrEmpty(fbxKey))
                {
                    var fbx = ObjectPool.Instance.GetAssemblyObject(fbxKey) as GameObject;
                    Animator fbxAnimator = fbx.GetComponent<Animator>();
                    animator.avatar = fbxAnimator != null ? fbxAnimator.avatar : null;
                }
            }
        }

    }
}
