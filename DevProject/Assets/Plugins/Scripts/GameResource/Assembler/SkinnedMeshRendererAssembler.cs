﻿using UnityEngine;

namespace GameResource
{
    public class SkinnedMeshRendererAssembler : AssetAssembler
    {

        public override void Assemble(GameObject go, string[] tokens)
        {
            string key = tokens[0];
            string meshName = tokens[1];
            SkinnedMeshRenderer skinnedMeshRenderer = go.GetComponent<SkinnedMeshRenderer>();
            if (skinnedMeshRenderer != null)
            {
                GameObject model = ObjectPool.Instance.GetAssemblyObject(key) as GameObject;
                //FakeFBX fakeFBX = model.GetComponent<FakeFBX>();
                //if (fakeFBX)
                //{
                //    skinnedMeshRenderer.sharedMesh = fakeFBX.GetMesh(meshName);
                //    return;
                //}
                Transform hostTransform = model.transform.Find(meshName);
                if (hostTransform == null)
                {
                    hostTransform = model.transform;
                }
                if (hostTransform != null)
                {
                    SkinnedMeshRenderer renderer = hostTransform.GetComponent<SkinnedMeshRenderer>();
                    if (renderer != null && renderer.sharedMesh != null)
                    {
                        skinnedMeshRenderer.sharedMesh = renderer.sharedMesh;
                        return;
                    }
                }
            }
        }

    }
}
