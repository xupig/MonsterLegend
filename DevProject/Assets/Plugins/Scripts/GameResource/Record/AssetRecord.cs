﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameResource
{
    public class AssetRecord
    {
        public readonly Dictionary<string, int> assetMemoryDict;
        public readonly Dictionary<string, List<string>> assetDependenceDict;

        public AssetRecord()
        {
            assetMemoryDict = new Dictionary<string, int>();
            assetDependenceDict = new Dictionary<string, List<string>>();
        }

        public ReleaseAssetRecord CreateReleaseAssetRecord()
        {
            ReleaseAssetRecord releaseAR = new ReleaseAssetRecord();
            releaseAR.assetBundleNameRecord = new string[assetMemoryDict.Count];
            releaseAR.assetBundleMemoryRecord = new int[assetMemoryDict.Count];
            releaseAR.assetPathRecord = new string[assetDependenceDict.Count];
            releaseAR.assetDependenceIdxRecord = new int[assetDependenceDict.Count];
            List<ushort> assetDependenceRecord = new List<ushort>();
            int idx = 0;
            foreach (var pair in assetMemoryDict)
            {
                releaseAR.assetBundleNameRecord[idx] = pair.Key;
                releaseAR.assetBundleMemoryRecord[idx] = (byte)pair.Value;
                releaseAR.assetBundleIdxDict[pair.Key] = (ushort)idx;
                idx++;
            }
            idx = 0;
            foreach (var pair in assetDependenceDict)
            {
                var dps = pair.Value;
                releaseAR.assetPathRecord[idx] = pair.Key;
                releaseAR.assetDependenceIdxRecord[idx] = assetDependenceRecord.Count;
                for (int i = 0; i < dps.Count; i++)
                { 
                    string abName = dps[i];
                    if (releaseAR.assetBundleIdxDict.ContainsKey(abName))
                    {
                        assetDependenceRecord.Add(releaseAR.assetBundleIdxDict[abName]);
                    }
                }   
                idx++;
            }
            releaseAR.assetDependenceRecord = assetDependenceRecord.ToArray();
            releaseAR.Reset();
            return releaseAR;
        }
    }
}
