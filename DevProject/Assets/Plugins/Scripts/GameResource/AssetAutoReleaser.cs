﻿using UnityEngine;
using System.Collections;

namespace GameResource
{
    public class AssetAutoReleaser : MonoBehaviour
    {
        public string path;

        protected void OnDestroy()
        {
            //if (path.Contains("1000.prefab"))
            //{
            //    Debug.LogError("AssetAutoReleaser 1001_sky: " + path);
            //}

            ObjectPool.Instance.Release(path);
        }
    }
}