﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;
using System.IO;

namespace GameResource
{
    public class IOSVestShaderData
    {
        public Color color;
        public bool poly;

        public IOSVestShaderData(Color ncolor, bool npoly)
        {
            color = ncolor;
            poly = npoly;
        }
    }

    public class IOSVestSetting
    {
        public const string SHADER_PROPERTY_VEST_COLOR = "_VestColor";
        public const string SHADER_PROPERTY_MAIN_COLOR = "_Main_Color";
        public const string SHADER_PROPERTY_COLOR = "_Color";
        
        public static bool ForIOSVest = false;
        public static Color VestColorDefault = Color.blue;
        public static Dictionary<string, IOSVestShaderData> VestColorMap = new Dictionary<string, IOSVestShaderData>() { };
        private static Texture2D _polyTexture = null;
        private static Texture2D polyTexture
        {
            get 
            {
                if (_polyTexture == null)
                {
                    _polyTexture = new Texture2D(128, 128);
                }
                return _polyTexture;
            }
        }

        public static void OnCreateMaterial(Material material)
        {
            if (ForIOSVest)
            {
                DoModifyMaterialColor(material);
            }
        }

        public const string TERRAIN_SHADER = "Shader$Terrain-Vest-Diffuse.u";
        public static void OnCreateTerrain(Terrain terrain)
        {
            if (ForIOSVest)
            {
                Shader shader = ObjectPool.Instance.GetAssemblyShader(TERRAIN_SHADER);
                Material material = new Material(shader);
                DoModifyMaterialColor(material);
                terrain.materialType = Terrain.MaterialType.Custom;
                terrain.materialTemplate = material;

                bool poly = false;
                IOSVestShaderData data = null;
                if (VestColorMap.TryGetValue("Terrain-Vest-Diffuse", out data))
                {
                    poly = data.poly;
                }
                if (poly)
                {
                    var splatPrototypes = terrain.terrainData.splatPrototypes;
                    for (int i = 0; i < splatPrototypes.Length; i++)
                    {
                        splatPrototypes[i].texture = polyTexture;
                    }
                    terrain.terrainData.splatPrototypes = splatPrototypes;
                }
            }
        }

        static void DoModifyMaterialColor(Material material)
        {
            string propertyName = string.Empty;
            if (material.HasProperty(SHADER_PROPERTY_VEST_COLOR))
            {
                propertyName = SHADER_PROPERTY_VEST_COLOR;
            }
            else if (material.HasProperty(SHADER_PROPERTY_COLOR))
            {
                propertyName = SHADER_PROPERTY_COLOR;
            }
            else if (material.HasProperty(SHADER_PROPERTY_MAIN_COLOR))
            {
                propertyName = SHADER_PROPERTY_MAIN_COLOR;
            }
            if (!string.IsNullOrEmpty(propertyName))
            {
                string name = Path.GetFileNameWithoutExtension(material.shader.name);
                Color color = VestColorDefault;
                bool poly = false;
                IOSVestShaderData data = null;
                if (VestColorMap.TryGetValue(name, out data))
                {
                    color = data.color;
                    poly = data.poly;
                }
                material.SetColor(propertyName, color);
                if (poly)
                {
                    material.SetTexture("_MainTex", polyTexture);
                }
            }
        }
    }
}
