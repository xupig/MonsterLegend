﻿using LitJson;
using System.Collections.Generic;
using UnityEngine;

namespace GameResource
{

    public class DelayedAnimatorController
    {
        AnimatorOverrideController _controller;
        public AnimatorOverrideController controller
        {
            get { return _controller; }
        }

        public static HashSet<string> ExceptClipNames;
        public static bool LoadAllWhenInit = false;
        Dictionary<string, string> _clipNameKeyDict;
        HashSet<string> _loadedClips = new HashSet<string>();
        AnimationClip _firstAnimationClip = null;
        public DelayedAnimatorController(string physicalPath)
        {
            var jsonData = ObjectPool.Instance.GetShadowJsonData(physicalPath);
            string name = (string)jsonData[AssetCreator.CONTROLLER];
            _controller = new AnimatorOverrideController();
            _controller.name = name;
            _controller.runtimeAnimatorController = ObjectPool.Instance.GetAssemblyObject(name) as RuntimeAnimatorController;

            _clipNameKeyDict = new Dictionary<string, string>();
            JsonData clipKeys = jsonData[AssetCreator.CLIP_KEYS];
            List<string> clipNames = new List<string>();
            List<string> physicalPaths = new List<string>();
            for (int i = 0; i < clipKeys.Count; i++)
            {
                JsonData clipData = clipKeys[i];
                string clipName = (string)clipData[0];
                string clipPhysicalPath = (string)clipData[1];
                _clipNameKeyDict[clipName] = clipPhysicalPath;
                if (LoadAllWhenInit || (ExceptClipNames != null && ExceptClipNames.Contains(clipName)))
                {
                    clipNames.Add(clipName);
                    physicalPaths.Add(clipPhysicalPath);
                }
            }
            if (clipNames.Count > 0)
            {
                LoadClips(clipNames, physicalPaths);
            }
        }

        public void LoadAllClips()
        {
            List<string> clipNames = new List<string>();
            List<string> physicalPaths = new List<string>();
            foreach (var pair in _clipNameKeyDict)
            {
                if (_loadedClips.Contains(pair.Key)) continue;
                clipNames.Add(pair.Key);
                physicalPaths.Add(pair.Value);
            }
            if (clipNames.Count > 0)
            {
                LoadClips(clipNames, physicalPaths);
            }
        }

        public void PlayClip(string clipName)
        {
            if (_loadedClips.Contains(clipName)) return;
            if (!_clipNameKeyDict.ContainsKey(clipName)) return;
            LoadClip(clipName, _clipNameKeyDict[clipName]);
        }

        void LoadClip(string clipName, string physicalPath)
        {
            _loadedClips.Add(clipName);
            ObjectPool.Instance.LoadAssetByPhysicalPath(new string[] { physicalPath }, delegate() { ResetClip(clipName, physicalPath); });
        }

        void LoadClips(List<string> clipNames, List<string> physicalPaths)
        {
            for (int i = 0; i < clipNames.Count; i++)
            {
                _loadedClips.Add(clipNames[i]);    
            }
            ObjectPool.Instance.LoadAssetByPhysicalPath(physicalPaths.ToArray(), delegate() { ResetClips(clipNames, physicalPaths); });    
        }

        void ResetClip(string clipName, string physicalPath)
        {
            var clip = ObjectPool.Instance.GetAssemblyObject(physicalPath) as AnimationClip;
            _controller[clipName] = clip;
            //unity5.X开始对空的clip会有错误提示：IsFinite (m_CachedRange.first) && IsFinite (m_CachedRange.second)
            //所以先把所有clip替换成第一个可用的clip
            if (_firstAnimationClip == null) 
            {
                _firstAnimationClip = clip;
                foreach (var pair in _clipNameKeyDict)
                {
                    _controller[pair.Key] = clip;
                }
            }
        }

        void ResetClips(List<string> clipNames, List<string> physicalPaths)
        {
            for (int i = 0; i < clipNames.Count; i++)
            {
                ResetClip(clipNames[i], physicalPaths[i]);
            }
        }

        public void Release()
        {
            Object.DestroyImmediate(_controller);
            _controller = null;
        }
    }

}
