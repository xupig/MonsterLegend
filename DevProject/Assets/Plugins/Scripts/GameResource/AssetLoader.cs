﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using LitJson;
using Object = UnityEngine.Object;
using System.IO;
using System.Threading;

#if !UNITY_WEBPLAYER
#endif

namespace GameResource
{
    /// <summary>
    /// 重要概念
    /// Path是资源的逻辑路径，是AssetRecord中DependceDict中的Key值，包含"/"， 例："Scenes/40001/40001_LightmapFar-0.exr"
    /// PhysicalPath是资源的物理路径，包含"$"，例："Scenes$40001$40001_LightmapFar-0.exr.u"
    /// </summary>
    internal class AssetLoader : MonoBehaviour
    {
        private const string FILE_HEAD = "file://";
        public static int COCURRENT_LOADING_COUNT = 5;
        public static int MIN_DELETE_ASSET_INTERVAL = 120;
        public static string POSTFIX_UNITY = ".unity.u";
        public static string POSTFIX_FOLDER = ".folder.u";
        public static string POSTFIX_ADVANCED = ".advanced.u";
        public static string POSTFIX_SELECTION = ".selection.u";
        public static string SHADER_PHYSICAL_PATH = "Shader.folder.u";
        public static string FONT_YAHEI_UNICODE_PATH = "Font${0}$MicrosoftYaHei_unicode.ttf.u";
        public static string FONT_YAHEI_PATH = "Font${0}$MicrosoftYaHei.ttf.u";
        public static string TOKEN_DOLLAR = "$";
        public static string TOKEN_DOT = ".";
        //Material和AnimatorController打包时生成的json文件名中含有_shadow关键字
        public static string SHADOW = "_shadow";
        public static string TYPE = "type";
        public static string TYPE_MATERIAL = "Material";
        public static string TYPE_CONTROLLER = "AnimatorController";
        public static string TYPE_PREFAB = "Prefab";
        public static Regex POSTFIX_FOLDER_PATTERN = new Regex(@"(\.\w+)?\.folder.u", RegexOptions.IgnoreCase);
        public static Regex POSTFIX_ADVANCED_PATTERN = new Regex(@"(\.\w+)?\.advanced.u", RegexOptions.IgnoreCase);

        public static int DELETE_ASSET_INTERVAL = MIN_DELETE_ASSET_INTERVAL;
        public static AssetLoader Instance { get; private set; }
        public static string RemoteURL = string.Empty;
        private ObjectPool _objectPool;
        private float _lastDeleteAssetTime;
        //待加载的资源队列
        private Queue<string> _assetPhysicalPathQueue;
        //仅判断存在性时性能更优
        private HashSet<string> _assetPhysicalPathSet;
        //正在加载中的资源Set
        private HashSet<string> _loadingAssetSet;
        //Key为资源的物理路径， Value为资源被加载次数
        private Dictionary<string, int> _assetLoadedCountSet;
        public Dictionary<string, int> AssetLoadedCountSet { get { return _assetLoadedCountSet; } }

        //已加载过资源路径Set
        private HashSet<string> _loadedAssetSet;
        private Dictionary<string, string> _advancedNameDict;       //Key为资源的物理路径，Value为包含Key资源的文件名字（不包括后缀）
        private Dictionary<string, AssetBundle> _advancedAssetbundleDict;//Key为资源的物理路径，Value为包含Key资源的Assetbundle
        private Dictionary<string, Object> _activeObjectDict;       //Key为资源的物理路径，Value为资源加载后的Object
        private Dictionary<string, List<string>> _activeAssetDict;  //key为资源的物理路径，Value为资源_objectDict中key集合
        private Dictionary<string, WWW> _presistWwwDict;            //key为资源的物理路径，value为资源加载后不立即释放的资源的WWW
        private Dictionary<string, bool> _parseMarkDict;            //key为资源的物理路径，value为是否在www加载完成后进行加载asset资源
        public bool canSaveToExternal = false;
        private HashSet<string> externalExistsFiles = new HashSet<string>(); //記錄已經存在于本地的遠程文件記錄

        private Dictionary<string, JsonData> _shadowCacheDict;       //Key为shadow资源的物理路径，Value为对应的JsonData

        //Shader特殊处理，只有需要使用其中的Shader的时候，才会从中加载，Shader资源包的AssetBundle不会Unload
        private AssetBundle _shaderAssetBundle;

        //advanced的AssetBundle缓存，在场景加载后释放
        private Dictionary<string, AssetBundle> _advancedAssetBundles = new Dictionary<string, AssetBundle>();
        //Scene的AssetBundle缓存，在Scene加载后释放
        private Dictionary<string, AssetBundle> _unityAssetBundles = new Dictionary<string, AssetBundle>();
        //不马上释放的AssetBundle缓存，在asset释放时跟随释放
        private Dictionary<string, AssetBundle> _assetBundles = new Dictionary<string, AssetBundle>();

        private Dictionary<string, int> _loadingErrors = new Dictionary<string, int>();

        private Dictionary<string, LoadingJson> _loadingJsonDict;
        private bool _loaderAlive = false;
        private Thread _jsonReadThread; //用于异步读取json的分线程
        private static readonly object _jsonLocker = new object();  //同步锁
        private Queue<LoadingJson> _loadingJsonQueue = new Queue<LoadingJson>();

        //正在加载资源计数
        private int _loadingCount = 0;
        //判断loader是否正在资源加在
        public bool isBusy { get { return _loadingCount > 0; } }

        //加载过的资源数量
        private int _loadAssetCount = 0;
        public int LoadAssetCount { get { return _loadAssetCount; } }

        //public bool IsWWW
        //{
        //    get
        //    {
        //        return _isWWW;
        //    }

        //    set
        //    {
        //        _isWWW = value;
        //    }
        //}

        //public bool IsAsnycLoad
        //{
        //    get
        //    {
        //        return _isAsnycLoad;
        //    }

        //    set
        //    {
        //        _isAsnycLoad = value;
        //    }
        //}

        private List<AssetLoadedCallbackWrapper> _assetLoadedCallbackList;
        private List<string> _unusedAssetPathList;

        //private bool _isWWW = false;

        //private bool _isAsnycLoad = true;
        
        static AssetLoader()
        {
            GameObject proxy = new GameObject("_ObjectPoolProxy");
            GameObject.DontDestroyOnLoad(proxy);
            Instance = proxy.AddComponent<AssetLoader>();
            Instance.Initialize();
            Instance.InitExternalExistsFilesDict();
        }

        private void Initialize()
        {
            _objectPool = ObjectPool.Instance;
            _assetPhysicalPathQueue = new Queue<string>();
            _assetPhysicalPathSet = new HashSet<string>();
            _loadingAssetSet = new HashSet<string>();
            _advancedNameDict = new Dictionary<string, string>();
            _advancedAssetbundleDict = new Dictionary<string, AssetBundle>();
            _activeObjectDict = new Dictionary<string, Object>();
            _activeAssetDict = new Dictionary<string, List<string>>();
            _presistWwwDict = new Dictionary<string, WWW>();
            _parseMarkDict = new Dictionary<string, bool>();
            _loadedAssetSet = new HashSet<string>();
            _assetLoadedCountSet = new Dictionary<string, int>();
            _assetLoadedCallbackList = new List<AssetLoadedCallbackWrapper>();
            _unusedAssetPathList = new List<string>();
            _shadowCacheDict = new Dictionary<string, JsonData>();
            _loadingJsonDict = new Dictionary<string, LoadingJson>();
            for (int i = 0; i < COCURRENT_LOADING_COUNT; i++)
            {
                if (_objectPool.IsWWW)
                    StartCoroutine(DaemonLoadWWWAsset());
                else
                    StartCoroutine(DaemonLoadFromFileAsset());
            }
            StartCoroutine(DaemonDeleteAsset());
            StartCoroutine(DaemonHandleAssetLoaded());
            StartJsonReadThread();
        }

        void OnDestroy()
        {
            if (_shaderAssetBundle != null) _shaderAssetBundle.Unload(false);
            foreach (var pair in _unityAssetBundles)
            {
                pair.Value.Unload(false);
            }
            _unityAssetBundles.Clear();
            StopJsonReadThread();
        }

        void StartJsonReadThread()
        {
            _loaderAlive = true;
            if (_jsonReadThread == null)
            {
                _jsonReadThread = new Thread(ReadingJson);
                _jsonReadThread.Start();
            }
        }

        void StopJsonReadThread()
        {
            _loaderAlive = false;
            try
            {
                if (_jsonReadThread != null) _jsonReadThread.Abort();
            }
            catch (Exception ex)
            {
                Debug.LogError("jsonReadThread 终止失败，reason：" + ex.Message + "\n" + ex.StackTrace);
            }
            finally
            {
                _jsonReadThread = null;
            }
        }

        void ReadingJson()
        {
            while (_loaderAlive)
            {
                LoadingJson loadingJson = null;
                lock (_jsonLocker)
                {
                    if (_loadingJsonQueue.Count > 0)
                    {
                        loadingJson = _loadingJsonQueue.Dequeue();
                    }
                }
                if (loadingJson != null)
                {
                    loadingJson.jsonData = JsonMapper.ToObject(loadingJson.text);
                    //Thread.Sleep(10);
                }
                else
                {
                    Thread.Sleep(100);
                }
            }
        }

        private IEnumerator ReadJsonDataAsyn(string physicalPath, string text)
        {
            if (!_shadowCacheDict.ContainsKey(physicalPath))
            {
                LoadingJson loadingJson;
                //bool neww = false;
                _loadingJsonDict.TryGetValue(physicalPath, out loadingJson);
                if (loadingJson == null)
                {
                    //neww = true;
                    loadingJson = LoadingJson.CreateLoadingJson();
                    loadingJson.text = text;
                    _loadingJsonDict.Add(physicalPath, loadingJson);
                    lock (_jsonLocker)
                    {
                        _loadingJsonQueue.Enqueue(loadingJson);
                    }
                }
                while (loadingJson.jsonData == null)
                {
                    yield return null;
                }
                _shadowCacheDict[physicalPath] = loadingJson.jsonData;
                //LoadingJson.ReleaseLoadingJson(loadingJson);
            }
        }

        void InitExternalExistsFilesDict()
        {
#if !UNITY_WEBPLAYER
            var uFiles = Directory.GetFiles(ObjectPool.ASSET_ROOT_PATH, "*.u", SearchOption.TopDirectoryOnly);
            for (int i = 0; i < uFiles.Length; i++)
            {
                string fileName = Path.GetFileName(uFiles[i]);
                externalExistsFiles.Add(fileName);
            }
#endif
        }

        private void DoEnqueueAssetPhysicalPath(string physicalPath)
        {
            _assetPhysicalPathQueue.Enqueue(physicalPath);
            _assetPhysicalPathSet.Add(physicalPath);
        }

        private string DoDequeueAssetPhysicalPath()
        {
            string physicalPath = _assetPhysicalPathQueue.Dequeue();
            _assetPhysicalPathSet.Remove(physicalPath);
            return physicalPath;
        }

        private IEnumerator DaemonLoadFromFileAsset()
        {
            while (true)
            {
                if (_assetPhysicalPathQueue.Count > 0)
                {
                    string physicalPath = DoDequeueAssetPhysicalPath();
                    _loadingAssetSet.Add(physicalPath);
                    _loadingCount++;
                    _loadAssetCount++;

                    if (_objectPool.IsLoadFromFileSystem(physicalPath))
                    {
                        yield return LoadFromFileSystem(physicalPath);
                    }
                    else
                    {
                        yield return LoadFromFile(physicalPath);
                    }

                    _loadingCount--;
                    AddToAssetLoadedCountSet(physicalPath);
                    _loadingAssetSet.Remove(physicalPath);
                }
                else
                {
                    yield return null;
                }
            }
        }

        private static AssetBundle GetAssetBundle(string url, string physicalPath)
        {
            return AssetBundle.LoadFromFile(url);
        }

        private static AssetBundleCreateRequest GetAssetBundleCreateRequest(string url, string physicalPath)
        {
            //url = url.ToLower();
#if !UNITY_WEBPLAYER
            return AssetBundle.LoadFromFileAsync(url);
#else
            int version = ObjectPool.Instance.GetVersion(physicalPath);
            return WWW.LoadFromCacheOrDownload(url, version);
#endif
        }

        private void CheckAssetbundleResult(string physicalPath, AssetBundle ab)
        {
            if (ab == null)
            {
                if (_objectPool.IsExternalAsset(physicalPath) && !externalExistsFiles.Contains(physicalPath))
                {
                    //通過遠程加載的文件，如果加載失敗繼續重試
                    DoEnqueueAssetPhysicalPath(physicalPath);
                }
                else
                {
                    //资源加载失败时，添加纪录使上层回调函数执行不受影响
                    _activeAssetDict.Add(physicalPath, new List<string>());
                }
                Debug.LogError(string.Format("Loading error in {0}", physicalPath));
            }
            else
            {
                //SaveToExternal(physicalPath, abcr);

                ParseLoadedAssetBundle(physicalPath, ab);
                CheckUnloadStrategy(physicalPath, ab);
                _loadedAssetSet.Add(physicalPath);
            }
        }

        private IEnumerator CheckAssetBundleCreateRequestResult(string physicalPath, AssetBundleCreateRequest abcr)
        {
            if (abcr.assetBundle == null)
            {
                if (_objectPool.IsExternalAsset(physicalPath) && !externalExistsFiles.Contains(physicalPath))
                {
                    //通過遠程加載的文件，如果加載失敗繼續重試
                    DoEnqueueAssetPhysicalPath(physicalPath);
                }
                else
                {
                    //资源加载失败时，添加纪录使上层回调函数执行不受影响
                    _activeAssetDict.Add(physicalPath, new List<string>());
                }
                Debug.LogError(string.Format("Loading error in {0}", physicalPath));
            }
            else
            {
                //SaveToExternal(physicalPath, abcr);
                yield return ParseLoadedAssetBundleAsync(physicalPath, abcr.assetBundle);
                CheckUnloadStrategy(physicalPath, abcr);
                _loadedAssetSet.Add(physicalPath);
            }
        }

        private IEnumerator DaemonLoadWWWAsset()
        {
            while (true)
            {
                if (_assetPhysicalPathQueue.Count > 0)
                {
                    string physicalPath = DoDequeueAssetPhysicalPath();
                    _loadingAssetSet.Add(physicalPath);
                    _loadingCount++;
                    _loadAssetCount++;

                    if (_objectPool.IsLoadFromFileSystem(physicalPath))
                    {
                        yield return LoadFromFileSystem(physicalPath);
                    }
                    else
                    {
                        yield return LoadFromWWW(physicalPath);
                    }

                    _loadingCount--;
                    AddToAssetLoadedCountSet(physicalPath);
                    _loadingAssetSet.Remove(physicalPath);
                    yield return null;
                }
                else
                {
                    yield return null;
                }
            }
        }

        private static WWW GetWWW(string url, string physicalPath)
        {
            //url = url.ToLower();
#if !UNITY_WEBPLAYER
            return new WWW(url);
#else
            int version = ObjectPool.Instance.GetVersion(physicalPath);
            return WWW.LoadFromCacheOrDownload(url, version);
#endif
        }

        private IEnumerator LoadFromFileSystem(string physicalPath)
        {
            string fileName = string.Concat("Shadow/", physicalPath.Substring(0, physicalPath.LastIndexOf(".u")).ToLower());
            string text = ObjectPool.GetJson(fileName);
            var mainAsset = ScriptableObject.CreateInstance<TextAssetEx>();
            mainAsset.text = text;
            HandleParseSinglePackedAsset(physicalPath, mainAsset);
            Object obj;
            _activeObjectDict.TryGetValue(physicalPath, out obj);
            yield return ReadJsonDataAsyn(physicalPath, text);
        }

        private IEnumerator LoadFromWWW(string physicalPath)
        {
            WWW www = null;
            bool useWWW;
            string url = GetAssetUrl(physicalPath, out useWWW);

#if UNITY_IPHONE
                    if (url.Contains(" "))
                    {
                        GameLoader.Utils.LoggerHelper.Error(string.Format("Resource path = {0} contains blank space.", url));
                    }
#endif
            www = GetWWW(url, physicalPath);
            yield return www;
            yield return CheckWWWResult(physicalPath, www);
            www = null;
        }

        private IEnumerator LoadFromFile(string physicalPath)
        {
            AssetBundleCreateRequest abcr = null;
            WWW www = null;
            bool useWWW;
            string url = GetAssetUrl(physicalPath, out useWWW);

#if UNITY_IPHONE
                    if (url.Contains(" "))
                    {
                        GameLoader.Utils.LoggerHelper.Error(string.Format("Resource path = {0} contains blank space.", url));
                    }
#endif

            if (useWWW)
            {
                www = GetWWW(url, physicalPath);
                yield return www;
                yield return CheckWWWResult(physicalPath, www);
                www = null;
            }
            else
            {
                if (_objectPool.IsAsnycLoad)
                {
                    abcr = GetAssetBundleCreateRequest(url, physicalPath);
                    yield return abcr;
                    yield return CheckAssetBundleCreateRequestResult(physicalPath, abcr);
                    abcr = null;
                }
                else
                {
                    var ab = GetAssetBundle(url, physicalPath);
                    CheckAssetbundleResult(physicalPath, ab);
                }
            }
        }

        private IEnumerator CheckWWWResult(string physicalPath, WWW www)
        {
            if (www.error != null)
            {
                if (_objectPool.IsExternalAsset(physicalPath) && !externalExistsFiles.Contains(physicalPath))
                {
                    //通過遠程加載的文件，如果加載失敗繼續重試
                    DoEnqueueAssetPhysicalPath(physicalPath);
                }
                else
                {
                    if (!_loadingErrors.ContainsKey(physicalPath))
                    {
                        _loadingErrors[physicalPath] = 0;
                    }
                    _loadingErrors[physicalPath]++;
                    if (_loadingErrors[physicalPath] < 2)
                    {
                        DoEnqueueAssetPhysicalPath(physicalPath);
                    }
                    else
                    {
                        //资源加载失败时，添加纪录使上层回调函数执行不受影响
                        _activeAssetDict.Add(physicalPath, new List<string>());
                    }
                }
                Debug.LogError(string.Format("Loading error in {0}, the reason is {1}", www.url, www.error));
            }
            else
            {
                SaveToExternal(physicalPath, www);
                yield return ParseLoadedAssetBundleAsync(physicalPath, www.assetBundle);
                CheckUnloadStrategy(physicalPath, www);
                _loadedAssetSet.Add(physicalPath);
            }
        }

        private string GetAssetUrl(string physicalPath, out bool useWWW)
        {
            useWWW = true;
            physicalPath = physicalPath.ToLower();
            string url = ObjectPool.ASSET_ROOT_PATH + physicalPath;     //ObjectPool.ASSET_ROOT_PATH:在移动端上,此值默认为“外部目录路径”

            bool exists = File.Exists(url);
            if (exists)
            {
                useWWW = false;
                if (_objectPool.IsExternalAsset(physicalPath) && !externalExistsFiles.Contains(physicalPath))
                {
                    externalExistsFiles.Add(physicalPath);
                }
                if (_objectPool.IsWWW)
                    return string.Concat(FILE_HEAD, url);
                else
                    return url;
            }

            url = ObjectPool.ASSET_SP_PATH + physicalPath;     //ObjectPool.ASSET_SP_PATH:小包资源路径

            exists = File.Exists(url);
            if (exists)
            {
                useWWW = false;
                if (_objectPool.IsWWW)
                    return string.Concat(FILE_HEAD, url);
                else
                    return url;
            }

            if (_objectPool.IsExternalAsset(physicalPath))
            {
                return string.Concat(RemoteURL, physicalPath);
            }
            if (SockpuppetLoader.Sockpuppet)
                physicalPath = SockpuppetLoader.GetSockpuppetName(physicalPath);

            if (UnityPropUtils.Platform == RuntimePlatform.Android)
            {
                //外部目录存在资源  ：从外部目录加载
                //外部目录不存在资源: 从StreamingAssets加载
                //Debug.Log(string.Format("url:{0},assetRootPath:{1},Exists:{2}", url, ObjectPool.ASSET_ROOT_PATH, File.Exists(url)));
                url = string.Concat(Application.streamingAssetsPath, "/Resources/", physicalPath);

                //旧代码：资源在外部目录不存在,则从StreaingAssetsPath路径加载
                //if (File.Exists(url) == false) url = string.Concat(Application.streamingAssetsPath, "/", physicalPath);
            }
            else if (UnityPropUtils.Platform == RuntimePlatform.IPhonePlayer || ObjectPool.isReleaseMode)
            {
                url = string.Concat(FILE_HEAD, Application.streamingAssetsPath, "/Resources/", physicalPath);

            }
            else
            {
                url = string.Concat(FILE_HEAD, url);

            }
            return url;
        }

        private IEnumerator DaemonDeleteAsset()
        {
            while (true)
            {
                if ((UnityPropUtils.realtimeSinceStartup - _lastDeleteAssetTime) > DELETE_ASSET_INTERVAL)
                {
                    _lastDeleteAssetTime = UnityPropUtils.realtimeSinceStartup;
                    _objectPool.DeleteZeroReferrencedAsset();
                }
                else
                {
                    yield return null;
                }
            }
        }

        private IEnumerator DaemonHandleAssetLoaded()
        {
            while (true)
            {
                for (int i = 0; i < _assetLoadedCallbackList.Count; i++)
                {
                    AssetLoadedCallbackWrapper wrapper = _assetLoadedCallbackList[i];
                    if (IsPhysicalPathListLoaded(wrapper.physicalPathList, wrapper.parseWhenLoaded) == true)
                    {
                        if (wrapper.isInvoked == false && wrapper.callback != null)
                        {
                            try //Ari: 协程一旦遇到异常就会中断，后续操作将无法执行，这里加个Try作为流程完整性的保护
                            {
                                wrapper.callback();
                            }
                            catch (Exception ex)
                            {
                                Debug.LogError(ex);
                            }
                        }
                        wrapper.isInvoked = true;
                    }
                }
                RecycleInvokedCallback();
                yield return null;
            }
        }

        private bool IsPhysicalPathListLoaded(List<string> physicalPathList, bool parseWhenLoaded)
        {
            for (int i = 0; i < physicalPathList.Count; i++)
            {
                string physicalPath = physicalPathList[i];
                if (parseWhenLoaded)
                {
                    if (IsActiveAsset(physicalPath) == false)
                    {
                        return false;
                    }
                }
                else
                {
                    if (!externalExistsFiles.Contains(physicalPath))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private void RecycleInvokedCallback()
        {
            for (int i = _assetLoadedCallbackList.Count - 1; i >= 0; i--)
            {
                AssetLoadedCallbackWrapper wrapper = _assetLoadedCallbackList[i];
                if (wrapper.isInvoked == true)
                {
                    _assetLoadedCallbackList.RemoveAt(i);
                    AssetLoadedCallbackWrapper.Recycle(wrapper);
                }
            }
        }

        /// <summary>
        /// 根据物理路径从常驻内存取出资源
        /// </summary>
        /// <param name="physicalPath">物理路径,例如:Scenes$1005_Race$1005_Race.prefab.u</param>
        /// <returns></returns>
        public byte[] GetPresistAssetData(string physicalPath)
        {
            return _presistWwwDict.ContainsKey(physicalPath) ? _presistWwwDict[physicalPath].bytes : null;
        }

        public void LoadAssetList(List<string> physicalPathList, Action callback, bool parseWhenLoaded = true)
        {
            EnqueueAssetPhysicalPath(physicalPathList, parseWhenLoaded);
            _assetLoadedCallbackList.Add(AssetLoadedCallbackWrapper.Get(physicalPathList, callback, parseWhenLoaded));
        }

        internal bool IsActiveAsset(string physicalPath)
        {
            return _activeAssetDict.ContainsKey(physicalPath);
        }

        internal bool IsActiveObject(string physicalPath)
        {
            //if (physicalPath.Contains("1001_sky_shadow"))
            //    Debug.LogError("_activeObjectDict contains 1001_sky_shadow: " + _activeObjectDict.ContainsKey(physicalPath));

            return _activeObjectDict.ContainsKey(physicalPath);
        }

        public bool IsLoadedAsset(string physicalPath)
        {
            return _loadedAssetSet.Contains(physicalPath);
        }

        public int GetLoadingAssetCount()
        {
            return _loadingAssetSet.Count;
        }

        private void AddToAssetLoadedCountSet(string path)
        {
            if (!_assetLoadedCountSet.ContainsKey(path))
            {
                _assetLoadedCountSet[path] = 0;
            }
            _assetLoadedCountSet[path] += 1;
        }

        public void EnqueueAssetPhysicalPath(string physicalPath, bool parseWhenLoaded = true)
        {
            //资源已加载或在等待加载队列中时,不加载
            if (IsActiveAsset(physicalPath) == false
                && _loadingAssetSet.Contains(physicalPath) == false
                && _assetPhysicalPathSet.Contains(physicalPath) == false)
            {
                DoEnqueueAssetPhysicalPath(physicalPath);
            }
            if (!_parseMarkDict.ContainsKey(physicalPath) || !_parseMarkDict[physicalPath])
            {
                _parseMarkDict[physicalPath] = parseWhenLoaded;
            }
        }

        public void EnqueueAssetPhysicalPath(List<string> physicalPathList, bool parseWhenLoaded = true)
        {
            for (int i = 0; i < physicalPathList.Count; i++)
            {
                EnqueueAssetPhysicalPath(physicalPathList[i], parseWhenLoaded);
            }
        }

        private bool ParseWhenLoaded(string physicalPath)
        {
            if (_parseMarkDict.ContainsKey(physicalPath))
            {
                return _parseMarkDict[physicalPath];
            }
            return true;
        }

        private IEnumerator ParseLoadedAssetBundleAsync(string physicalPath, AssetBundle assetBundle)
        {
            if (!ParseWhenLoaded(physicalPath))
            {
                yield break;
            }
            if (physicalPath.EndsWith(POSTFIX_UNITY) == true)
            {
                UnloadUnityAssetBundle(physicalPath);
                _unityAssetBundles.Add(physicalPath, assetBundle);
                _activeAssetDict.Add(physicalPath, new List<string>());
            }
            else if (physicalPath.EndsWith(POSTFIX_FOLDER) == true)
            {
                if (physicalPath == SHADER_PHYSICAL_PATH)
                {
                    _shaderAssetBundle = assetBundle;
                    _activeAssetDict.Add(physicalPath, new List<string>());
                }
                else
                {
                    yield return ParseFolderPackedAssetAsync(physicalPath, assetBundle);
                }
            }
            else if (physicalPath.EndsWith(POSTFIX_ADVANCED) == true)
            {
                yield return ParseAdvancedPackedAssetAsync(physicalPath, assetBundle);
            }
            else if (physicalPath.EndsWith(POSTFIX_SELECTION))
            {
                yield return ParseSelectionPackedAssetAsync(physicalPath, assetBundle);
            }
            else
            {
                if (physicalPath == FONT_YAHEI_UNICODE_PATH)
                {
                    yield return ParseFontAssetAsync(physicalPath, assetBundle);
                }
                else
                {
                    yield return ParseSinglePackedAssetAsync(physicalPath, assetBundle);
                }
            }
        }

        private void ParseLoadedAssetBundle(string physicalPath, AssetBundle assetBundle)
        {
            if (!ParseWhenLoaded(physicalPath))
            {
                return;
            }
            if (physicalPath.EndsWith(POSTFIX_UNITY) == true)
            {
                UnloadUnityAssetBundle(physicalPath);
                _unityAssetBundles.Add(physicalPath, assetBundle);
                _activeAssetDict.Add(physicalPath, new List<string>());
            }
            else if (physicalPath.EndsWith(POSTFIX_FOLDER) == true)
            {
                if (physicalPath == SHADER_PHYSICAL_PATH)
                {
                    _shaderAssetBundle = assetBundle;
                    _activeAssetDict.Add(physicalPath, new List<string>());
                }
                else
                {
                    ParseFolderPackedAsset(physicalPath, assetBundle);
                }
            }
            else if (physicalPath.EndsWith(POSTFIX_ADVANCED) == true)
            {
                ParseAdvancedPackedAsset(physicalPath, assetBundle);
            }
            else if (physicalPath.EndsWith(POSTFIX_SELECTION))
            {
                ParseSelectionPackedAsset(physicalPath, assetBundle);
            }
            else
            {
                if (physicalPath == FONT_YAHEI_UNICODE_PATH)
                {
                    ParseFontAsset(physicalPath, assetBundle);
                }
                else
                {
                    ParseSinglePackedAsset(physicalPath, assetBundle);
                }
            }
        }

        private void CheckUnloadStrategy(string physicalPath, AssetBundle assetBundle)
        {
            if (_objectPool.GetUnloadStrategy(physicalPath) == 1)
            {
                if (assetBundle != null)
                    assetBundle.Unload(false);
            }
            else
            {
                if (assetBundle && !_unityAssetBundles.ContainsKey(physicalPath) && !_advancedAssetBundles.ContainsKey(physicalPath))
                    _assetBundles.Add(physicalPath, assetBundle);
            }
        }

        private void CheckUnloadStrategy(string physicalPath, AssetBundleCreateRequest abcr)
        {
            if (_objectPool.GetUnloadStrategy(physicalPath) == 1)
            {
                if (abcr.assetBundle != null)
                    abcr.assetBundle.Unload(false);
            }
            else
            {
                if (abcr.assetBundle && !_unityAssetBundles.ContainsKey(physicalPath) && !_advancedAssetBundles.ContainsKey(physicalPath))
                    _assetBundles.Add(physicalPath, abcr.assetBundle);
            }
        }

        private void CheckUnloadStrategy(string physicalPath, WWW www)
        {
            if (_objectPool.GetUnloadStrategy(physicalPath) == 1)
            {
                UnloadWww(www, null);
            }
            else
            {
                if (www.assetBundle && !_unityAssetBundles.ContainsKey(physicalPath) && !_advancedAssetBundles.ContainsKey(physicalPath))
                {
                    _assetBundles.Add(physicalPath, www.assetBundle);
                }
                www.Dispose();
            }
        }

        private void ParseFontAsset(string physicalPath, AssetBundle asset)
        {
            Object[] objs = asset.LoadAllAssets();
            HandleParseFontAsset(physicalPath, objs);
        }

        private IEnumerator ParseFontAssetAsync(string physicalPath, AssetBundle asset)
        {
            var request = asset.LoadAllAssetsAsync();
            yield return request;
            Object[] objs = request.allAssets;
            HandleParseFontAsset(physicalPath, objs);
        }

        private void HandleParseFontAsset(string physicalPath, Object[] objs)
        {
            foreach (Object obj in objs)
            {
                if (obj.name.Contains("MicrosoftYaHei_unicode") == true)
                {
                    _activeObjectDict.Add(FONT_YAHEI_UNICODE_PATH, obj);

                }
                else
                {
                    if (obj.name.Contains("MicrosoftYaHei") == true)
                    {
                        _activeObjectDict.Add(FONT_YAHEI_PATH, obj);
                    }
                }
            }
            _activeAssetDict.Add(physicalPath, new List<string>() { FONT_YAHEI_UNICODE_PATH, FONT_YAHEI_PATH });
        }

        private void ParseSelectionPackedAsset(string physicalPath, AssetBundle asset)
        {
            Object[] objs = asset.LoadAllAssets();
            HandleParseSelectionPackedAsset(physicalPath, objs);
        }

        private IEnumerator ParseSelectionPackedAssetAsync(string physicalPath, AssetBundle asset)
        {
            var request = asset.LoadAllAssetsAsync();
            yield return request;
            Object[] objs = request.allAssets;
            HandleParseSelectionPackedAsset(physicalPath, objs);
        }

        private void HandleParseSelectionPackedAsset(string physicalPath, Object[] objs)
        {
            List<string> objKeyList = new List<string>();
            foreach (Object obj in objs)
            {
                string objKey = obj.name;
                if (_activeObjectDict.ContainsKey(objKey) == false)
                {
                    _activeObjectDict.Add(objKey, obj);
                    objKeyList.Add(objKey);
                }
                else
                {
                    Debug.LogWarning("Key already exists: " + objKey + " -- " + obj);
                }
            }
            if (_activeAssetDict.ContainsKey(physicalPath) == false)
            {
                _activeAssetDict.Add(physicalPath, objKeyList);
            }
            else
            {
                Debug.LogWarning("Asset already exists: " + physicalPath);
            }
        }

        private void ParseFolderPackedAsset(string physicalPath, AssetBundle asset)
        {
            Object[] objs = asset.LoadAllAssets();
            HandleParseFolderPackedAsset(physicalPath, objs);
        }

        private IEnumerator ParseFolderPackedAssetAsync(string physicalPath, AssetBundle asset)
        {
            var request = asset.LoadAllAssetsAsync();
            yield return request;
            Object[] objs = request.allAssets;
            if (IOSVestSetting.ForIOSVest && (physicalPath.StartsWith("GUI$") || physicalPath.StartsWith("Atlas$")))
            {
                string pngFileName = physicalPath.ToLower() + ".png"; // = string.Concat(FILE_HEAD, ObjectPool.ASSET_ROOT_PATH + physicalPath + ".png");
                if (UnityPropUtils.Platform == RuntimePlatform.Android)
                {
                    pngFileName = string.Concat(Application.streamingAssetsPath, "/Resources/", pngFileName);
                }
                else if (UnityPropUtils.Platform == RuntimePlatform.IPhonePlayer || ObjectPool.isReleaseMode)
                {
                    pngFileName = string.Concat(FILE_HEAD, Application.streamingAssetsPath, "/Resources/", pngFileName);
                }
                else
                {
                    pngFileName = string.Concat(FILE_HEAD, ObjectPool.ASSET_ROOT_PATH + pngFileName);
                }
                Texture2D texture = null;
                Texture2D alphaTexture = null;

                WWW www = new WWW(pngFileName);
                yield return www;
                if (www != null && string.IsNullOrEmpty(www.error)) texture = www.texture;
                www.Dispose();
                if (texture != null)
                {
                    alphaTexture = Texture2D.whiteTexture;
                }
                www = null;
                HandleParseUIFolderPackedAssetForVest(physicalPath, objs, texture, alphaTexture);
            }
            else
            {
                HandleParseFolderPackedAsset(physicalPath, objs);
            }
        }

        private void HandleParseFolderPackedAsset(string physicalPath, Object[] objs)
        {
            Match m = POSTFIX_FOLDER_PATTERN.Match(physicalPath);
            string folderPath = physicalPath.Substring(0, m.Index);
            List<string> objKeyList = new List<string>();
            foreach (Object obj in objs)
            {
                string objKey = string.Concat(folderPath, "$", obj.name, ".u");
                //Debug.LogError(physicalPath + " ==> " + obj.GetType() + "  " + objKey);
                //模型FBX文件中的Transform和Mesh可能会和模型本身GameObject重名，不处理这种情况
                if ((obj is Transform) == true || (obj is Mesh) == true)
                {
                    continue;
                }
                if (_activeObjectDict.ContainsKey(objKey) == false)
                {
                    _activeObjectDict.Add(objKey, obj);
                    objKeyList.Add(objKey);
                }
                else
                {
                    Debug.LogWarning("Key already exists: " + objKey + " -- " + obj);
                }
            }
            if (_activeAssetDict.ContainsKey(physicalPath) == false)
            {
                _activeAssetDict.Add(physicalPath, objKeyList);
            }
            else
            {
                Debug.LogWarning("Asset already exists: " + physicalPath);
            }
        }

        private void HandleParseUIFolderPackedAssetForVest(string physicalPath, Object[] objs, Texture2D texture, Texture2D alphaTexture)
        {
            Match m = POSTFIX_FOLDER_PATTERN.Match(physicalPath);
            string folderPath = physicalPath.Substring(0, m.Index);
            List<string> objKeyList = new List<string>();
            foreach (Object obj in objs)
            {
                string objKey = string.Concat(folderPath, "$", obj.name, ".u");
                Object newObj = obj;
                if ((obj is Sprite) && texture != null)
                {
                    var newSprite = obj as Sprite;
                    newObj = Sprite.Create(texture, newSprite.rect, newSprite.pivot, newSprite.pixelsPerUnit, 0, SpriteMeshType.Tight, newSprite.border);
                }
                else if ((obj is Texture2D) && obj.name.EndsWith("_alpha") && alphaTexture != null)
                {
                    newObj = alphaTexture;
                }
                if (_activeObjectDict.ContainsKey(objKey) == false)
                {
                    _activeObjectDict.Add(objKey, newObj);
                    objKeyList.Add(objKey);
                }
                else
                {
                    Debug.LogWarning("Key already exists: " + objKey + " -- " + obj);
                }
            }
            if (_activeAssetDict.ContainsKey(physicalPath) == false)
            {
                _activeAssetDict.Add(physicalPath, objKeyList);
            }
            else
            {
                Debug.LogWarning("Asset already exists: " + physicalPath);
            }
        }

        private void ParseAdvancedPackedAsset(string physicalPath, AssetBundle asset)
        {
            var descName = "ab_desc";
            //if (physicalPath.EndsWith(".mat.advanced.u"))
            //    descName = "ab_desc_mat";
            //else if (physicalPath.EndsWith(".mesh.advanced.u"))
            //    descName = "ab_desc_mesh";
            //Object[] objs = asset.LoadAllAssets();
            var obj = asset.LoadAsset<TextAsset>(descName);
            //Debug.LogError(obj.text);
            if (obj == null || obj.text == null)
            {
                Debug.LogError("ab_desc not exist: " + physicalPath);
            }
            else
            {
                var fileNames = obj.text.Split('\n');
                //Debug.LogError("fileNames: " + fileNames.Length);
                HandleParseAdvancedPackedAsset(physicalPath, fileNames, asset);
            }
        }

        private IEnumerator ParseAdvancedPackedAssetAsync(string physicalPath, AssetBundle asset)
        {
            var descName = "ab_desc";
            //if (physicalPath.EndsWith(".mat.advanced.u"))
            //    descName = "ab_desc_mat";
            //else if (physicalPath.EndsWith(".mesh.advanced.u"))
            //    descName = "ab_desc_mesh";
            var request = asset.LoadAssetAsync<TextAsset>(descName);
            yield return request;
            if (request.allAssets.Length > 0)
            {
                var textAb = request.allAssets[0] as TextAsset;
                //Debug.LogError(request.allAssets[0]);
                if (textAb == null || textAb.text == null)
                {
                    Debug.LogError("ab_desc not exist: " + physicalPath);
                }
                else
                {
                    var fileNames = textAb.text.Split('\n');
                    HandleParseAdvancedPackedAsset(physicalPath, fileNames, asset);
                }
            }
        }

        private void HandleParseAdvancedPackedAsset(string physicalPath, string[] fileNames, AssetBundle asset)
        {
            Match m = POSTFIX_ADVANCED_PATTERN.Match(physicalPath);
            string folderPath = physicalPath.Substring(0, m.Index);
            List<string> objKeyList = new List<string>();
            foreach (var item in fileNames)
            {
                var fileName = item.Trim();
                string objKey = string.Concat(folderPath, "$", fileName, ".u");
                if (_advancedAssetbundleDict.ContainsKey(objKey) == false)
                {
                    _advancedAssetbundleDict.Add(objKey, asset);
                    _advancedNameDict.Add(objKey, fileName);
                    objKeyList.Add(objKey);
                }
                else
                {
                    Debug.LogWarning("Key already exists: " + objKey + " -- " + asset);
                }
            }
            if (_activeAssetDict.ContainsKey(physicalPath) == false)
            {
                _activeAssetDict.Add(physicalPath, objKeyList);
            }
            else
            {
                Debug.LogWarning("Asset already exists: " + physicalPath);
            }
        }

        private void ParseSinglePackedAsset(string physicalPath, AssetBundle asset)
        {
            Object mainAsset = null;
            if (asset == null)
            {
                Debug.LogError("ParseSinglePackedAsset error in asset == null: " + physicalPath);
            }
            Object[] objs = asset.LoadAllAssets();
            mainAsset = objs[0];
            HandleParseSinglePackedAsset(physicalPath, mainAsset);
        }

        private IEnumerator ParseSinglePackedAssetAsync(string physicalPath, AssetBundle asset)
        {
            Object mainAsset = null;
            if (asset == null)
            {
                Debug.LogError("ParseSinglePackedAsset error in asset == null: " + physicalPath);
            }
            var request = asset.LoadAllAssetsAsync();
            yield return request;
            mainAsset = request.allAssets[0];
            HandleParseSinglePackedAsset(physicalPath, mainAsset);
        }

        private void HandleParseSinglePackedAsset(string physicalPath, Object mainAsset)
        {
            if (_activeObjectDict.ContainsKey(physicalPath) == false)
            {
                //if (physicalPath.Contains("1001_sky_shadow"))
                //    Debug.LogError("HandleParseSinglePackedAsset _activeObjectDict.Add: " + physicalPath);
                _activeObjectDict.Add(physicalPath, mainAsset);
                _activeAssetDict.Add(physicalPath, new List<string>() { physicalPath });
            }
            else
            {
                Debug.LogWarning("Asset already exists: " + physicalPath);
            }
        }

        private void UnloadWww(WWW www, AssetBundle assetBundle)
        {
            if (www.assetBundle != null)
            {
                www.assetBundle.Unload(false);
            }
            www.Dispose();
        }

        public void UnloadAllAdvancedAssetBundles()
        {
            foreach (var item in _advancedAssetBundles)
            {
                item.Value.Unload(false);
            }
            _advancedAssetBundles.Clear();
        }

        public void UnloadAdvancedAssetBundle(string physicalPath)
        {
            if (_advancedAssetBundles.ContainsKey(physicalPath))
            {
                _advancedAssetBundles[physicalPath].Unload(false);
                _advancedAssetBundles.Remove(physicalPath);
            }
        }

        public void UnloadUnityAssetBundle(string physicalPath)
        {
            if (_unityAssetBundles.ContainsKey(physicalPath))
            {
                _unityAssetBundles[physicalPath].Unload(false);
                _unityAssetBundles.Remove(physicalPath);
                DeleteAssetImmediate(physicalPath);
            }
        }

        private void AddToPresistWwwDict(string physicalPath, WWW www)
        {
            if (_presistWwwDict.ContainsKey(physicalPath) == false)
            {
                _presistWwwDict.Add(physicalPath, www);
            }
        }

        internal void RemovePresistWww(string physicalPath)
        {
            if (_presistWwwDict.ContainsKey(physicalPath) == true)
            {
                WWW www = _presistWwwDict[physicalPath];
                www.assetBundle.Unload(false);
                www.Dispose();
                _presistWwwDict.Remove(physicalPath);
            }
        }

        internal Shader GetActiveShader(string physicalPath)
        {
            if (_activeObjectDict.ContainsKey(physicalPath) == true)
            {
                return _activeObjectDict[physicalPath] as Shader;
            }
            int dollarIndex = physicalPath.LastIndexOf(TOKEN_DOLLAR);
            int dotIndex = physicalPath.LastIndexOf(TOKEN_DOT);
            string name = physicalPath.Substring(dollarIndex + 1, (dotIndex - dollarIndex - 1));
            Shader shader = null;
            if (_shaderAssetBundle != null && _shaderAssetBundle.Contains(name) == true)
            {
                shader = _shaderAssetBundle.LoadAsset(name) as Shader;
                _activeObjectDict.Add(physicalPath, shader);
                _activeAssetDict[SHADER_PHYSICAL_PATH].Add(name);
            }
            else
            {
                shader = Shader.Find(name);
            }
            return shader;
        }

        internal Texture2D GetDefaultParticleTexture()
        {
            if (_activeObjectDict.ContainsKey(AssetCreator.DEFAULT_PARTICLE) == true)
            {
                return _activeObjectDict[AssetCreator.DEFAULT_PARTICLE] as Texture2D;
            }
            Texture2D texture = null;
            if (_shaderAssetBundle != null)
            {
                texture = _shaderAssetBundle.LoadAsset(AssetCreator.DEFAULT_PARTICLE) as Texture2D;
                _activeObjectDict.Add(AssetCreator.DEFAULT_PARTICLE, texture);
            }
            return texture;
        }

        internal Object GetActiveObject(string physicalPath, bool delay = true)
        {
            if (string.IsNullOrEmpty(physicalPath) == true)
            {
                Debug.LogError("ActiveOjbect physicalPath can not by null or empty!!");
                return null;
            }
            if (physicalPath == AssetCreator.DEFAULT_DIFFUSE)
            {
                return AssetCreator.GetDefaultDiffuseMaterial();
            }
            if (physicalPath == AssetCreator.DEFAULT_PARTICLE)
            {
                return AssetCreator.GetDefaultParticleMaterial();
            }
            Object obj = null;
            if (_activeObjectDict.ContainsKey(physicalPath))
            {
                obj = _activeObjectDict[physicalPath];
            }
            else
            {
                if (_advancedAssetbundleDict.ContainsKey(physicalPath))
                {
                    obj = _advancedAssetbundleDict[physicalPath].LoadAsset(_advancedNameDict[physicalPath]);
                    _activeObjectDict.Add(physicalPath, obj);
                }
            }
            if (obj == null)
            {
                Debug.LogError("ActiveObject not found!! physicalPath: " + physicalPath);
                return null;
            }

            if ((obj is TextAssetEx || obj is TextAsset) && physicalPath.Contains(SHADOW) == true)
            {
                JsonData jsonData = GetShadowJsonData(physicalPath);
                string type = (string)jsonData[TYPE];
                if (type == TYPE_MATERIAL)
                {
                    Material material = AssetCreator.CreateMaterial(jsonData, obj.name.Replace(SHADOW, string.Empty));
                    ReplaceActiveObject(physicalPath, material);
                    return material;
                }
                if (type == TYPE_CONTROLLER)
                {
                    AnimatorOverrideController controller = AssetCreator.CreateAnimatorController(jsonData);
                    ReplaceActiveObject(physicalPath, controller);
                    return controller;
                }
                if (type == TYPE_PREFAB)
                {
                    GameObject go = AssetCreator.CreateGameObject(jsonData, physicalPath, delay);
                    ReplaceActiveObject(physicalPath, go);
                    return go;
                }
            }
            return obj;
        }

        internal JsonData GetShadowJsonData(string physicalPath)
        {
            try
            {
                if (!_shadowCacheDict.ContainsKey(physicalPath))
                {
                    if (!_activeObjectDict.ContainsKey(physicalPath)) Debug.LogError("!_activeObjectDict.ContainsKey:" + physicalPath);
                    var asset = _activeObjectDict[physicalPath];
                    if (asset is TextAsset)
                    {
                        TextAsset ta = _activeObjectDict[physicalPath] as TextAsset;
                        _shadowCacheDict[physicalPath] = JsonMapper.ToObject(ta.text);
                    }
                    else
                    {
                        TextAssetEx tae = _activeObjectDict[physicalPath] as TextAssetEx;
                        _shadowCacheDict[physicalPath] = JsonMapper.ToObject(tae.text);
                    }
                }
                return _shadowCacheDict[physicalPath];
            }
            catch (Exception ex)
            {
                Debug.LogError(physicalPath + " " + ex.ToString());
                return new JsonData();
            }
        }

        internal void ReplaceActiveObject(string physicalPath, Object obj)
        {
            if (_activeObjectDict.ContainsKey(physicalPath) == true)
            {
                _activeObjectDict[physicalPath] = obj;
            }
        }

        internal void DeleteAssetImmediate(string physicalPath)
        {
            if (_activeAssetDict.ContainsKey(physicalPath) == false)
            {
                Debug.LogError(string.Format("Key [{0}] not found in ActiveAssetDict!", physicalPath));
                return;
            }
            RemovePresistWww(physicalPath);
            List<string> objKeyList = _activeAssetDict[physicalPath];
            var isAdvanced = physicalPath.EndsWith(POSTFIX_ADVANCED) == true;
            foreach (string objKey in objKeyList)
            {
                //if (objKey.Contains("1001_sky_shadow"))
                //    Debug.LogError("DeleteAssetImmediate: " + physicalPath + " " + ObjectPool.Instance.PackList(objKeyList));
                //if (objKey.Contains("Sky.MAT.advanced"))
                //    Debug.LogError("DeleteAssetImmediate: " + physicalPath + " " + ObjectPool.Instance.PackList(objKeyList));
                //if (objKey.Contains("14_Sewer$Materials.MAT.advanced"))
                //    Debug.LogError("DeleteAssetImmediate: " + physicalPath + " " + ObjectPool.Instance.PackList(objKeyList));
                if (_advancedAssetbundleDict.ContainsKey(objKey))
                {
                    _advancedAssetbundleDict.Remove(objKey);
                    _advancedNameDict.Remove(objKey);
                }
                if (_activeObjectDict.ContainsKey(objKey) == true)
                {
                    Object.DestroyImmediate(_activeObjectDict[objKey], true);
                    _activeObjectDict.Remove(objKey);
                }
                else
                {
                    if (!isAdvanced)
                        Debug.LogError(string.Format("Key [{0}] not found in ActiveObjectDict !!", objKey));
                }
            }
            _activeAssetDict.Remove(physicalPath);
            if (_assetBundles.ContainsKey(physicalPath))
            {
                UnloadAdvancedAssetBundle(physicalPath);
                _assetBundles[physicalPath].Unload(true);
                _assetBundles.Remove(physicalPath);
            }
        }

        internal Dictionary<string, List<string>> GetActiveAssetDict()
        {
            return _activeAssetDict;
        }

        private IEnumerator WaitForLoadingAsset()
        {
            float waittingTime = UnityPropUtils.realtimeSinceStartup;
            while (_assetLoadedCallbackList.Count > 0)
            {
                yield return null;
                if ((UnityPropUtils.realtimeSinceStartup - waittingTime) > 10)
                {
                    Debug.LogError("WaitForLoadingAsset 超时！");
                    PrintAssetLoadedCallbackList();
                    break;
                }
            }
        }

        private void PrintAssetLoadedCallbackList()
        {
            string debugInfo = "remain resources: \n";
            for (int j = 0; j < _assetLoadedCallbackList.Count; j++)
            {
                AssetLoadedCallbackWrapper wrapper = _assetLoadedCallbackList[j];
                var physicalPathList = wrapper.physicalPathList;
                for (int i = 0; i < physicalPathList.Count; i++)
                {
                    string physicalPath = physicalPathList[i];
                    if (wrapper.parseWhenLoaded)
                    {
                        if (IsActiveAsset(physicalPath) == false)
                        {
                            debugInfo += physicalPath + " x/n";
                        }
                    }
                    else
                    {
                        if (!externalExistsFiles.Contains(physicalPath))
                        {
                            debugInfo += physicalPath + " y/n";
                        }
                    }
                }
            }
            Debug.LogError(debugInfo);
        }

        internal Boolean IsNeedLoadingAsset()
        {
            return _assetLoadedCallbackList.Count > 0;
        }

        internal Coroutine CreateWaitForLoadingAssetCoroutine()
        {
            return StartCoroutine(WaitForLoadingAsset());
        }
        /// <summary>
        /// 刪除加载了但未使用的资源，比如战斗中预加载的资源
        /// </summary>
        internal void DeleteUnusedAsset()
        {
            _unusedAssetPathList.Clear();
            foreach (KeyValuePair<string, List<string>> kvp in _activeAssetDict)
            {
                if (_objectPool.IsAssetInUse(kvp.Key) == false)
                {
                    _unusedAssetPathList.Add(kvp.Key);
                }
            }
            for (int i = 0; i < _unusedAssetPathList.Count; i++)
            {
                DeleteAssetImmediate(_unusedAssetPathList[i]);
            }
            _loadingJsonDict.Clear();
        }

        internal void WaitForOneFrame(Action callback)
        {
            StartCoroutine(CreateWaitOneFrameCoroutine(callback));
        }

        private IEnumerator CreateWaitOneFrameCoroutine(Action callback)
        {
            yield return null;
            callback();
        }

        public bool ExistsInLocalExternal(String physicalPath)
        {
            return externalExistsFiles.Contains(physicalPath);
        }

        private void SaveToExternal(String physicalPath, WWW www)
        {
            if (!canSaveToExternal) return;
            if (externalExistsFiles.Contains(physicalPath)) return;
            string fileName = ObjectPool.ASSET_ROOT_PATH + physicalPath.ToLower();
            if (!File.Exists(fileName))
            {
                string tempFileName = fileName + "_temp";
                if (File.Exists(tempFileName)) File.Delete(tempFileName);
                byte[] buffer = www.bytes;
                using (FileStream fs = new FileStream(tempFileName, FileMode.Create))
                {
                    using (BinaryWriter sw = new BinaryWriter(fs))
                    {
                        sw.Write(buffer);
                        sw.Flush();
                        sw.Close();
                    }
                    fs.Close();
                }
                File.Move(tempFileName, fileName);
            }
            externalExistsFiles.Add(physicalPath);
        }
    }

    class LoadingJson
    {
        public string text;
        public JsonData jsonData;

        static Queue<LoadingJson> _pool = new Queue<LoadingJson>();
        public static LoadingJson CreateLoadingJson()
        {
            if (_pool.Count > 0)
            {
                return _pool.Dequeue();
            }
            return new LoadingJson();
        }

        public static void ReleaseLoadingJson(LoadingJson loadingJson)
        {
            loadingJson.text = string.Empty;
            loadingJson.jsonData = null;
            _pool.Enqueue(loadingJson);
        }
    }

    public class SockpuppetLoader
    {
        static Dictionary<string, string> dictNameReference = new Dictionary<string, string>();
        public static bool Sockpuppet = false;

        public static void Initialize(string nickNameMk)
        {
            if (String.IsNullOrEmpty(nickNameMk))
            {
                Sockpuppet = false;
                return;
            }

            dictNameReference.Clear();
            string[] parts = nickNameMk.Split('\n');
            string[] nameArray = new string[2];
            foreach (string line in parts)
            {
                if (!String.IsNullOrEmpty(line))
                {
                    nameArray = line.Split('|');
                    string nickName = nameArray[0];
                    string name = nameArray[1].TrimEnd();
                    dictNameReference.Add(name, nickName);
                }
            }
        }

        //没有马甲名则返回原名
        public static string GetSockpuppetName(string name)
        {
            string lowerName = name.ToLower();
            if (dictNameReference.ContainsKey(lowerName))
            {
                return dictNameReference[lowerName];
            }
            return name;
        }

        //public static string GetSockpuppetPath(string url)
        //{
        //    string name = Path.GetFileName(url);
        //    if (dictNameReference.ContainsKey(name))
        //    {
        //        string newName = dictNameReference[name];
        //        string newPath = url.Replace(name, newName);
        //        return newPath;
        //    }
        //    return url;
        //}
    }
}
