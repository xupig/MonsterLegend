﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：SystemConfig
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.3.19
// 模块描述：系统参数配置。
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using GameLoader.Utils;
using GameLoader.Utils.XML;
using GameLoader.IO;
using GameLoader.Mgrs;
using GameLoader.LoadingBar;
using GameLoader.Utils.Timer;
using System.Security;

namespace GameLoader
{
    /// <summary>
    /// 系统参数配置类
    /// </summary>
    public class SystemConfig
    {
        #region system_config.xml节点属性
        /// <summary>
        /// 模式[true:发布模式,false:开发模式]
        /// </summary>
        public static bool ReleaseMode = false;
        /// <summary>
        /// 是否为为IphoneTest平台【true:是,false:否]
        /// </summary>
        public static bool isIphoneTest = false;
        /// <summary>
        /// 本地版本更新开启状态[true:开启,false:关闭]
        /// </summary>
        public static bool IsOpenVersionCheck = false;

        /// <summary>
        /// cfg.xml远程下载地址
        /// </summary>
        public static string CfgUrl = "";
        /// <summary>
        /// 日志提交账号
        /// </summary>
        public static string DefaultAccount = "";
        /// <summary>
        /// 日志提交IP
        /// </summary>
        public static string DefaultServer = "";
        /// <summary>
        /// 资源路径,以"/"结尾
        /// </summary>
        public static string ResPath = "";
        /// <summary>
        /// 程序库存放目录,以"/"结尾
        /// </summary>
        public static string DllPath = "";

        //预加载游戏库文件
        public static string CloseModules = "";
        /// <summary>
        /// 对象池设置信息字典
        /// </summary>
        public static Dictionary<string, string> ObjectPoolSettings = new Dictionary<string, string>();
        /// <summary>
        /// 是否显示引导
        /// </summary>
        public static bool showGuide = true;
        /// <summary>
        /// 是否可以通过Profiler提取样本
        /// </summary>
        public static bool canProfilerBeginSample = false;

        public static bool printUILog = false;
        /// <summary>
        /// 日志过滤
        /// </summary>
        public static string logFilter = "";
        #endregion


        #region cfg.xml节点属性
        /// <summary>
        /// 服务器配置下载地址
        /// </summary>
        public static string ServerUrl;
        /// <summary>
        /// 商城配置信息下载地址
        /// </summary>
        public static string MarketUrl;
        /// <summary>
        /// Apk包版本号
        /// </summary>
        public static string ProgramVersion;
        /// <summary>
        /// pkg包版本号(资源版本号)
        /// </summary>
        public static string ResourceVersion;
        /// <summary>
        /// 背景图版本
        /// </summary>
        public static string ProgressBarBgVersion;
        /// <summary>
        /// pkg包下载目录,必需以"/"结尾
        /// </summary>
        public static string PackageUrl;
        /// <summary>
        /// pkg包的Md5文件下载地址
        /// </summary>
        public static string PackageMd5Url;

        /// <summary>
        /// Apk包远程下载地址
        /// </summary>
        public static string ApkUrl;
        /// <summary>
        /// Apk包的Md5值
        /// </summary>
        public static string ApkMd5;
        /// <summary>
        /// Apk包的文件大小
        /// </summary>
        public static long ApkSize;

        /// <summary>
        /// 远程游戏资源下载目录地址
        /// </summary>
        public static string RemoteResourcesUrl;
        /// <summary>
        /// 远程数据热更新下载目录地址
        /// </summary>
        public static string RemoteHotUpdateUrl;
        /// <summary>
        /// 是否使用平臺SDK
        /// </summary>
        public static bool IsUsePlatformSdk = false;
        /// <summary>
        /// 是否使用平台更新[true:使用平台更新,false:自更新]
        /// </summary>
        public static bool IsUsePlatformUpdate = false;
        /// <summary>
        /// 是否使用文件系统[true:使用,false:不使用],默认true
        /// </summary>
        public static bool IsUseFileSystem = true;
        /// <summary>
        /// 导出StreamingAssets资源状态
        /// </summary>
        public static bool IsExportStreamingAssets;
        /// <summary>
        /// 是否采用整包apk更新[true:是,false:否]
        /// </summary>
        public static bool IsUpdateBigApk = false;
        /// <summary>
        /// apk更新是否仅采用平台单方控制[true:平台单方控制,false:采取双方控制]
        /// </summary>
        public static bool IsOnlyUsePlatformCtrUpdate = false;
        /// <summary>
        /// 是否采用跳转更新更新[true:是,false:否]
        /// </summary>
        public static bool IsOpenUrl = false;
        /// <summary>
        /// 是否根据程序版本号删除外部资源
        /// </summary>
        public static bool IsDelByProgramVersion = false;
        /// <summary>
        /// 是否打开RPC日志
        /// </summary>
        public static bool IsRecordRPCMsg = false;

        /// <summary>
        /// 服务器组配置下载地址
        /// </summary>
        public static string ServerGroupUrl;

        /// <summary>
        /// cfg.xml节点内容字典
        /// </summary>
        public static Dictionary<string, string> CfgNodeData = new Dictionary<string, string>();
        #endregion

        public static string Language = "cn";

        /// <summary>
        /// 联机状态[true:联机,false:单机]
        /// </summary>
        public static bool ONLINE = true;
        /// <summary>
        /// GameLoader.dll是否有更新[true:有更新,false:没有更新]
        /// 1、pkg更新,如果检查到有GameLoader.dll，则认为有更新，否则没有更新
        /// </summary>
        public static bool IsUpdateGameLoader = false;
        /// <summary>
        /// 外部资源的存放目录,以"/"结尾
        /// 路径为：Application.persistentDataPath+"/RuntimeResource/"
        /// </summary>
        public static string RuntimeResourcePath;

        /// <summary>
        /// 返回资源目录路径,以"/"结尾
        /// 路径为：Application.persistentDataPath+"/RuntimeResource/Resources/"
        /// </summary>
        public static string ResourcesPath;

        //private static bool needLoadCommonFile = false;//为排除警告，注释掉
        //初始化完成并回调callback<更新结果[true:成功,false:失败],cfg.xml|common.xml加载结果[true:成功,false:失败]>
        private static Action<bool, bool> initCallback;

        /// <summary>
        /// 上传运营日志的Url路径
        /// </summary>
        public static string UploadYunYingLogUrl = "";

        public static bool isShowGMGUI = false;

        public static string LoginDomainName = "";

        public static string ChargeDomainName = "";

        public static bool IsChargeTest = false;

        public static string giftCodeUrl = "";

        public static string UploadYunYingLogServer = "";

        public static bool IsDownloadFromSource = false;

        //针对IOS马甲包的内容配置
        public static string VestPackageSetting = "";

        public static string SmallPackagePath = "s/";

        /// <summary>
        /// 系统配置初始化
        /// 1、本地加载本地system_config.xml文件
        /// 2、本地加载平台的扩展配置,PC:system_config_ex.xml,安卓:system_config_android.xml,IOS:system_config_ios.xml
        /// 3、system_config和平台扩展配置文件，如果他们有相同节点，采取平台扩展配置覆盖system_config节点内容
        /// 
        /// 4、远程加载服务器cfg.xml配置文件
        /// 5、远程加载cfg.xml的父配置文件common.xml
        /// 6、如果cfg.xml和common.xml有相同节点，采取cfg.xml节点内容覆盖common.xml节点内容
        /// 7、初始化完成并回调callback<更新结果[true:成功,false:失败],cfg.xml|common.xml加载结果[true:成功,false:失败]>
        /// </summary>
        /// <param name="xmlDic">外网cfg、Common文件的字典数据 (Driver.cs中传过来)</param>
        /// <param name="callback">回调</param>
        public static void Init(Dictionary<string, string> xmlDic, Action<bool, bool> callback)
        {
            try
            {
                DoInit(xmlDic, callback);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("系统配置初始化[Fail],reason:" + ex.Message + "\n" + ex.StackTrace);
                ExecuteCallback(false);
            }
        }

        public static void InitForChangeChannel(Action<bool, bool> callback)
        {
            var channelId = PlatformSdk.PlatformSdkMgr.Instance.GetChannelId();
            if (CfgNodeData.ContainsKey(channelId))
            {
                LoggerHelper.Info("cfg contain channelId: " + channelId);
                LoadCfgInfo(CfgNodeData[channelId], (flag) =>
                {
                    if (flag)
                    {
                        DoInit(CfgInfoNew, callback);
                    }
                });
            }
            else
            {
                callback.SafeInvoke(true, true);
            }
        }

        public static void DoInit(Dictionary<string, string> xmlDic, Action<bool, bool> callback)
        {
            try
            {
                initCallback = callback;
                CfgNodeData = xmlDic;
                RuntimeResourcePath = string.Concat(Application.persistentDataPath, ConstString.RutimeResource, "/");
                ResourcesPath = string.Concat(RuntimeResourcePath, "Resources/");
                LoadLocalSystemConfig();

                LoadLoadingFloatTip(GetValueInCfg("loadingFloatTipUrl"));

                ServerUrl = GetValueInCfg("serverlist");
                MarketUrl = GetValueInCfg("market");
                ServerGroupUrl = GetValueInCfg("servergroup");

                ProgramVersion = GetValueInCfg("ProgramVersion");
                ResourceVersion = GetValueInCfg("ResourceVersion");
                ProgressBarBgVersion = GetValueInCfg("ProgressBarBgVersion");
                PackageUrl = GetValueInCfg("PackageUrl");
                PackageMd5Url = GetValueInCfg("PackageMd5List");
                ApkUrl = GetValueInCfg("ApkUrl");
                ApkMd5 = GetValueInCfg("ApkMd5");
                long.TryParse(GetValueInCfg("ApkSize"), out ApkSize);

                IsUsePlatformSdk = GetValueInCfg("IsUsePlatformSdk") == "1";
                IsUsePlatformUpdate = GetValueInCfg("IsUsePlatformUpdate") == "1";
                IsExportStreamingAssets = GetValueInCfg("IsExportStreamingAssets") == "1";
                IsUpdateBigApk = GetValueInCfg("IsUpdateBigApk") == "1";
                Language = GetValueInCfg("Language");
                IsOnlyUsePlatformCtrUpdate = GetValueInCfg("IsOnlyUsePlatformCtrUpdate") == "1";
                RemoteResourcesUrl = GetValueInCfg("RemoteResourcesUrl");
                RemoteHotUpdateUrl = GetValueInCfg("RemoteHotUpdateUrl");
                printUILog = GetValueInCfg("printUILog") == "1";
                CloseModules = GetValueInCfg("CloseModules");
                IsOpenUrl = GetValueInCfg("IsOpenUrl") == "1";
                IsDelByProgramVersion = GetValueInCfg("IsDelByProgramVersion") == "1";
                UploadYunYingLogUrl = GetValueInCfg("UploadYunYingLogUrl");
                isShowGMGUI = GetValueInCfg("isShowGMGUI") == "1";
                LoginDomainName = GetValueInCfg("LoginDomainName");
                ChargeDomainName = GetValueInCfg("ChargeDomainName");
                giftCodeUrl = GetValueInCfg("giftCodeUrl");
                UploadYunYingLogServer = GetValueInCfg("UploadYunYingLogServer");
                VestPackageSetting = GetValueInCfg("VestPackageSetting");
                IsChargeTest = GetValueInCfg("ChargeTest") == "1";
                IsDownloadFromSource = GetValueInCfg("IsDownloadFromSource") == "1";
                if (!IsRecordRPCMsg)
                    IsRecordRPCMsg = GetValueInCfg("IsRecordRPCMsg") == "1";

                LoggerHelper.Info(string.Format("[SysteConfig] ProgramVersion:{0}", GetValueInCfg("ProgramVersion")));
                LoggerHelper.Info(string.Format("[SysteConfig] ResourceVersion:{0}", GetValueInCfg("ResourceVersion")));
                LoggerHelper.Info(string.Format("[SysteConfig] ResourceVersion:{0}", GetValueInCfg("ResourceVersion")));
                LoggerHelper.Info(string.Format("[SysteConfig] ProgressBarBgVersion:{0}", GetValueInCfg("ProgressBarBgVersion")));
                LoggerHelper.Info(string.Format("[SysteConfig] IsUsePlatformUpdate:{0}", IsUsePlatformUpdate));
                LoggerHelper.Info(string.Format("[SysteConfig] IsUsePlatformSdk:{0}", IsUsePlatformSdk));
                LoggerHelper.Info(string.Format("[SysteConfig] IsUpdateBigApk:{0}", IsUpdateBigApk));
                LoggerHelper.Info(string.Format("[SysteConfig] IsOnlyUsePlatformCtrUpdate:{0}", IsOnlyUsePlatformCtrUpdate));
                ExecuteCallback(true);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("系统配置初始化[Fail],reason:" + ex.Message + "\n" + ex.StackTrace);
                ExecuteCallback(false);
            }
        }

        /// <summary>
        /// 根据节点名称--取得cfg.xml配置的节点内容
        /// </summary>
        /// <param name="key">节点名称</param>
        /// <returns>节点内容</returns>
        public static string GetValueInCfg(String key)
        {
            string result = "";
            if (CfgNodeData.ContainsKey(key))
            {
                result = CfgNodeData[key];
            }
            return result;
        }

        /// <summary>
        /// 是否为应用包[1:应用宝包，0：非应用宝包]
        /// </summary>
        /// <returns></returns>
        public static bool isyyb()
        {
            string str = SystemConfig.CfgNodeData.ContainsKey("IsYybFlag") ? SystemConfig.CfgNodeData["IsYybFlag"] : "";
            return !string.IsNullOrEmpty(str) && str.Equals("1") ? true : false;
        }

        /**返回模式
        * @return [true:开发模式,false:发布模式]
        * **/
        public static bool isDevelopeMode { get; private set; }

        //加载system_config.xml文件和平台扩展配置
        private static void LoadLocalSystemConfig()
        {
            //A-1加载&解析system_config.xml
            LoadSystemConfigForPlatform(ConstString.SystemConfigFile);
            //A-2加载&解析平台扩展配置(PC:system_config_ex.xml,安卓:system_config_android.xml,IOS:system_config_ios.xml)
            if (UnityPropUtils.IsEditor)
            {//PC编辑平台
                isDevelopeMode = true;
                string exFilePath = Path.Combine(Application.dataPath + "/Resources", ConstString.SystemConfigExFile);
                if (File.Exists(exFilePath))
                {
                    LoadSystemConfigForPlatform(ConstString.SystemConfigExFile);
                }
                if (ReleaseMode)
                {//开发模式下--发布模式
                    RuntimeResourcePath = string.Concat(Application.dataPath.Replace("\\", "/"), "/StreamingAssets/");
                    ResourcesPath = string.Concat(RuntimeResourcePath, "Resources/");
                }
                else
                {//开发模式
                    RuntimeResourcePath = ResPath;
                    ResourcesPath = string.Concat(RuntimeResourcePath, "Resources/");
                }
            }
            else if (UnityPropUtils.Platform == RuntimePlatform.Android)
            {//安卓平台
                LoadSystemConfigForPlatform(ConstString.SystemConfigAndroidFile);
            }
            else if (UnityPropUtils.Platform == RuntimePlatform.IPhonePlayer)
            {//IOS平台
                LoadSystemConfigForPlatform(ConstString.SystemConfigIOSFile);
            }
            //else if (UnityPropUtils.Platform == RuntimePlatform.WindowsWebPlayer)//为排除警告
            //{
            //    LoadSystemConfigForPlatform(ConstString.SystemConfigWebFile);
            //}
            else if (UnityPropUtils.Platform == RuntimePlatform.WindowsPlayer)
            {
                RuntimeResourcePath = string.Concat(Application.streamingAssetsPath, "/");
                ResourcesPath = string.Concat(RuntimeResourcePath, "Resources/");
            }
            LoggerHelper.Info(string.Format("RuntimeResourcePath:{0},dllPath:{1},ReleaseMode:{2}", RuntimeResourcePath, DllPath, ReleaseMode));
        }

        //解析system_config.xml和平台扩展配置
        private static void LoadSystemConfigForPlatform(string configName)
        {
            try
            {
                //ProgressBar.Instance.UpdateTip(string.Format("正在载入:{0}", configName));
                string systemConfigStr = ResLoader.LoadStringFromSystem(configName);
                XMLNode rootNode = XMLLoader.Parse(systemConfigStr);
                _LoadModeSetting(rootNode);
                _LoadCfgSetting(rootNode);
                _LoadLinkSetting(rootNode);
                _LoadLogSetting(rootNode);
                _LoadProfilerSetting(rootNode);

                if (UnityPropUtils.IsEditor)
                {
                    _LoadEditorSetting(rootNode);
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(string.Format("文件:{0} Load[Fail],reson:{1}", configName, ex.StackTrace));
                //ProgressBar.Instance.UpdateTip(string.Format("载入或解析:{0}失败:{1}", configName, ex.Message));
            }
        }

        //初始化回调
        private static void ExecuteCallback(bool result, bool downloadCfgResult = true)
        {
            try
            {
                if (result) ProgressBar.Instance.UpdateProgress(0.15f, true);
                if (initCallback != null) initCallback(result, downloadCfgResult);
                initCallback = null;
            }
            catch (Exception ex)
            {
                initCallback = null;
                LoggerHelper.Error("[系统配置初始化] 回调失败，reason:" + ex.StackTrace);
            }
        }

        #region 私有方法
        private static void _LoadModeSetting(XMLNode rootNode)
        {
            XMLNode settingNode = rootNode.GetNode("config>0>mode>0");
            if (settingNode == null) return;
            ReleaseMode = settingNode.GetValue("@releaseMode").Equals("1") ? true : false;
            string str = settingNode.GetValue("@isLocalUpdateVersion");
            IsOpenVersionCheck = str != null && str.Equals("1") ? true : false;
            LoggerHelper.Debug(string.Format("[SystemConfig] IsOpenVersionCheck:{0},str:{1}", IsOpenVersionCheck, str));
        }

        private static void _LoadCfgSetting(XMLNode rootNode)
        {
            XMLNode cfgNode = rootNode.GetNode("config>0>cfg>0");
            if (cfgNode == null) return;
            CfgUrl = cfgNode.GetValue("@url");
        }

        private static void _LoadLinkSetting(XMLNode rootNode)
        {
            XMLNode settingNode = rootNode.GetNode("config>0>link>0");
            if (settingNode == null) return;
            DefaultAccount = settingNode.GetValue("@defaultAccount");
            DefaultServer = settingNode.GetValue("@defaultServer");
        }


        private static void _LoadLogSetting(XMLNode rootNode)
        {
            XMLNode logSettingNode = rootNode.GetNode("config>0>logSetting>0");
            if (logSettingNode == null) return;
            logFilter = logSettingNode.GetValue("@filter");

            LoggerHelper.SetRegEx(logFilter);
            XMLNodeList nodeList = logSettingNode.GetNodeList("item");
            LogLevel logLevel = LogLevel.NONE;
            foreach (XMLNode node in nodeList)
            {
                var logLevelName = node.GetValue("@logLevel");
                if (node.GetValue("@state").Equals("1") ? true : false)
                {
                    if (logLevelName.Equals("DEBUG")) { logLevel = logLevel | LogLevel.DEBUG; }
                    else if (logLevelName.Equals("INFO")) { logLevel = logLevel | LogLevel.INFO; }
                    else if (logLevelName.Equals("ERROR")) { logLevel = logLevel | LogLevel.ERROR; }
                    else if (logLevelName.Equals("CRITICAL")) { logLevel = logLevel | LogLevel.CRITICAL; }
                    else if (logLevelName.Equals("EXCEPT")) { logLevel = logLevel | LogLevel.EXCEPT; }
                    else if (logLevelName.Equals("WARNING")) { logLevel = logLevel | LogLevel.WARNING; }
                }
            }
            LoggerHelper.CurrentLogLevels = logLevel;

            if (!IsRecordRPCMsg)
                IsRecordRPCMsg = logSettingNode.GetValue("@IsRecordRPCMsg") == "1";
        }

        private static void _LoadEditorSetting(XMLNode rootNode)
        {
            XMLNode settingNode = rootNode.GetNode("config>0>editorSetting>0");
            if (settingNode == null) return;
            DllPath = CheckFixPath(settingNode.GetValue("@dllPath"));
            ResPath = CheckFixPath(settingNode.GetValue("@resPath"));
            ONLINE = settingNode.GetValue("@online").Equals("1") ? true : false;
        }

        private static void _LoadProfilerSetting(XMLNode rootNode)
        {
            XMLNode settingNode = rootNode.GetNode("config>0>profilerSetting>0");
            if (settingNode == null) return;
            canProfilerBeginSample = settingNode.GetValue("@beginSample").Equals("1") ? true : false;
        }

        private static string CheckFixPath(string path)
        {
            if (UnityPropUtils.IsEditor)
            {
                if (path.StartsWith("../"))
                {
                    path = Application.dataPath + "/" + path;
                }
            }
            return path;
        }

        public const string VERSION_URL_KEY = "VersionInfo";
        public const string CFG_PARENT_KEY = "parent";
        public static Dictionary<string, string> CfgInfoNew = new Dictionary<string, string>();

        public static void LoadCfgInfo(string cfgUrl, Action<bool> callback)
        {
            DriverLogger.Info("GameLoader cfgUrl: " + cfgUrl);
            var programVerStr = "";
            var programVer = Resources.Load(VERSION_URL_KEY) as TextAsset;
            SecurityElement rootNode = XMLParser.LoadXML(programVer.text);
            SecurityElement proNode = rootNode.SearchForChildByTag("ProgramVersion");
            if (programVer && !String.IsNullOrEmpty(programVer.text))
            {
                programVerStr = "V" + proNode.Text;
            }

            //var s = Application.persistentDataPath.Split('/');
            ////Debug.Log(s.Length);
            //for (int i = s.Length - 1; i >= 0; i--)
            //{
            //    //Debug.Log(s[i]);
            //    if (s[i] == "files" && i - 1 >= 0)
            //    {
            //        BundleIdentifier = s[i - 1];
            //        DriverLogger.Info("bundleIdentifier: " + BundleIdentifier);
            //        break;
            //    }
            //}

            Action erraction = () =>
            {
                if (callback != null)
                    callback(false);
            };
            Action<string> suaction = null;
            suaction = (res) =>
            {
                var parentinfo = LoadCfgInfoList(res);
                foreach (var pair in parentinfo)
                {
                    if (!CfgInfoNew.ContainsKey(pair.Key))
                    {
                        CfgInfoNew.Add(pair.Key, pair.Value);
                    }
                }
                if (parentinfo.ContainsKey(programVerStr))//根据版本做特殊处理
                {
                    CfgInfoNew.Clear();
                    HttpWrappr.instance.LoadWwwText(parentinfo[programVerStr], suaction, erraction);
                }
                else if (parentinfo.ContainsKey(CFG_PARENT_KEY))
                {
                    HttpWrappr.instance.LoadWwwText(parentinfo[CFG_PARENT_KEY], suaction, erraction);
                }
                else if (callback != null)
                    callback(CfgInfoNew != null && CfgInfoNew.Count > 0 ? true : false);
            };
            HttpWrappr.instance.LoadWwwText(cfgUrl, suaction, erraction);
        }


        private static Dictionary<string, string> LoadCfgInfoList(string text)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            var children = XMLParser.LoadXML(text);
            if (children != null && children.Children != null && children.Children.Count != 0)
            {
                foreach (System.Security.SecurityElement item in children.Children)
                {
                    result[item.Tag] = item.Text;
                }
            }
            return result;
        }
        #endregion


        #region 下载启动游戏的提示配置
        private static void LoadLoadingFloatTip(string url)
        {
            if (string.IsNullOrEmpty(url)) return;
            //string loadingfloattip = ResLoader.LoadStringFromSystem(CommonUtils.GetFileName(url));
            //if (!string.IsNullOrEmpty(loadingfloattip))
            //{
            //    onParserNotice(url, loadingfloattip);
            //    return;
            //}

            string randomUrl = TextDownloadMgr.GetRandomParasUrl(url);
            LoggerHelper.Info(string.Format("下载文件:{0}[Start]", randomUrl));
            //ResLoader.LoadWwwText(randomUrl, onParserNotice);
            TextDownloadMgr.AsyncDownloadString(randomUrl, OnParserNotice);
        }

        private static int step = -1;
        private static bool isStop = false;
        private static int[] timeList = null;
        private static string[] contentList = null;
        private static void OnParserNotice(string content)
        {
            //LoggerHelper.Debug(string.Format("下载文件:{0},Load {1}", url, string.IsNullOrEmpty(content) ? "Fail" : "OK"));
            if (!string.IsNullOrEmpty(content))
            {

                SecurityElement node = XMLParser.LoadXML(content);
                int i = 0;
                timeList = new int[node.Children.Count];
                contentList = new string[node.Children.Count];
                foreach (SecurityElement childNode in node.Children)
                {
                    timeList[i] = int.Parse(childNode.SearchForChildByTag("lastTime_i").Text);
                    contentList[i] = childNode.SearchForChildByTag("content_s").Text;
                    i++;
                }

                ShowNextLoadingFloatTip();
            }
        }

        private static void ShowNextLoadingFloatTip()
        {
            step++;
            if (timeList != null && step >= timeList.Length)
                step = 0;
            if (isStop == false && timeList != null && step < timeList.Length)
            {
                ProgressBar.Instance.UpdateFloatText(contentList[step]);
                TimerHeap.AddTimer((uint)(timeList[step] * 1000), 0, ShowNextLoadingFloatTip);
            }
        }

        /// <summary>
        /// 停止启动游戏到登录界面期间的进度条提示文本显示
        /// </summary>
        public static void StopLoadingFloatTipShow()
        {
            isStop = true;
            //step = int.MaxValue;
            //if (timeList != null) step = timeList.Length;
        }
        #endregion

    }
}
