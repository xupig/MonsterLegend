using System;
using UnityEngine;

public class ConstString
{
	public static readonly string ASSET_FILE_HEAD="file://";
    public static readonly string U_SUFFIX = ".u";                           //assetbundle文件后缀
    public static readonly string MK_SUFFIX = ".mk";
    public static readonly string DLL_SUFFIX = ".dll";                       //程序库文件后缀
	public static readonly string XML_SUFFIX=".xml";
    public static readonly string BM_SUFFIX = ".bm";
	public static readonly string CONFIG_SUB_FOLDER="data/";

    public static readonly string ENTITY_DEFS_PATH = "entity_defs/";
    public static readonly string DYNAMIC_MAPS_PATH = "dynamic_maps/";
    public static readonly string DEFINE_LIST_FILE_NAME = "entities";
    public static readonly string RUNTIME_RESOURCE_PATH = "/../../RuntimeResource/";

    //public static readonly string SERVER_LIST_URL_KEY = "serverlist";
    //public static readonly string VERSION_URL_KEY = "version";
    //public static readonly string MARKET_URL_KEY = "market";
    //public static readonly string LOGIN_MARKET_URL_KEY = "LoginMarketData";
    //public static readonly string CFG_PARENT_KEY = "parent";

    public static readonly string GAME_LOADER_DLL = "GameLoader";      //.dll
    public static readonly string RutimeResource = "/RuntimeResource";
    public static readonly string RutimeResourceConfig = RutimeResource + "/Config";
    public static readonly string LogoPath = RutimeResource + "/Logo.png";
    public static readonly string LocalLogo = "/loginlogo.png.u";
    public static readonly string LocalLoginBG = "/loginbg.png.u";

    //系统配置文件的名称
    public readonly static string SystemConfigFile = "system_config.xml";
    //平台系统扩展配置，开发时自定义配置，用于覆盖原始配置，但又不需要频繁上传
    public readonly static string SystemConfigExFile = "system_config_ex.xml";
    public readonly static string SystemConfigAndroidFile = "system_config_android.xml";
    public readonly static string SystemConfigIOSFile = "system_config_ios.xml";
    public readonly static string SystemConfigWebFile = "system_config_web.xml";
	public static String LocalSettingPath;
	public static String ServerListPath;
}