﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ProtoBuf;

using UnityEngine;

namespace GameLoader.Config
{
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class LocalSettings
    {
        [ProtoBuf.ProtoMember(1)]
        public string SelectedCharacter = "";
        [ProtoBuf.ProtoMember(2)]
        public int SelectedServerID = 0;
        [ProtoBuf.ProtoMember(3)]
        public string UserAccount = "";
        [ProtoBuf.ProtoMember(4)]
        public string SelectedServerTemp = string.Empty;
        public void Copy(LocalSettings other)
        {
            this.SelectedCharacter = other.SelectedCharacter;
            this.SelectedServerID = other.SelectedServerID;
            this.UserAccount = other.UserAccount;
            this.SelectedServerTemp = other.SelectedServerTemp;
        }
    }

    public static class SerializableTypeExtensions
    {
        public static bool Init(this LocalSettings TYPE, string localAbsPath)
        {
            if (File.Exists(localAbsPath))
            {
                using (FileStream fs = File.OpenRead(localAbsPath))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    var temp = formatter.Deserialize(fs) as LocalSettings;
                    fs.Close();
                    fs.Dispose();
                    TYPE.Copy(temp);
                }
                return true;
            }
            return false;
        }
        public static void Save(this LocalSettings TYPE, string localAbsPath)
        {
            if (!Directory.Exists(localAbsPath.GetDirectoryName()))
            {
                Directory.CreateDirectory(localAbsPath.GetDirectoryName());
            }
            if (File.Exists(localAbsPath))
            {
                File.Delete(localAbsPath);
            }
            using (FileStream fs = File.OpenWrite(localAbsPath))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(fs, TYPE);
                fs.Close();
                fs.Dispose();
            }
        }
    }
}
