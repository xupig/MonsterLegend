﻿using GameLoader.Utils;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameLoader.LoadingBar
{
    /// <summary>
    /// 应用启动到进入游戏--使用的确认提示框
    /// 1、确认框
    /// 2、强制框
    /// </summary>
    public class ConfirmBox
    {
        private Text txtTip;
        private Text txtConfirm;
        private Text txtCancel;
        private Button btnConfirm;
        private Button btnCancel;
        private GameObject goCancel;
        private GameObject goConfirm;
        private GameObject prefab;
        private Action<bool> callback;
        public static ConfirmBox Instance;

        public GameObject GoConfirm
        {
            get
            {
                return goConfirm;
            }
            set
            {
                goConfirm = value;
            }
        }

        public ConfirmBox(GameObject prefab)
        {
            this.prefab = prefab;
            GameObject.DontDestroyOnLoad(prefab);
            SetSkin();
        }

        //提取控件
        private void SetSkin()
        {
            txtTip = prefab.transform.Find("Canvas/labTip").GetComponent<Text>();
            goConfirm = prefab.transform.Find("Canvas/btnConfirm").gameObject;
            txtConfirm = goConfirm.transform.Find("Text").GetComponent<Text>();
            btnConfirm = goConfirm.GetComponent<Button>();

            goCancel = prefab.transform.Find("Canvas/btnCancel").gameObject;
            txtCancel = goCancel.transform.Find("Text").GetComponent<Text>();
            btnCancel = goCancel.GetComponent<Button>();
            prefab.SetActive(false);
            AddEvents();
        }

        /// <summary>
        /// 更换皮肤
        /// </summary>
        /// <param name="skin">皮肤资源</param>
        public void UpdateSkin(GameObject skin)
        {
            if (skin == null) 
            {
                LoggerHelper.Error("[ConfirmBox]皮肤为空，无法更换！");
                return;
            }
            //销毁前一个皮肤资源
            Dispose();
            //设置新皮肤资源
            this.prefab = skin;
            SetSkin();
        }

        /// <summary>
        /// 显示对话框
        /// </summary>
        /// <param name="okText">提示</param>
        /// <param name="callback">点击按钮后的回调</param>
        /// <param name="okText">确认按钮文本</param>
        /// <param name="cancelText">取消按钮文本</param>
        /// <param name="isCancel">是否显示取消按钮[true:显示,false:隐藏]</param>
        public void Show(string msg, Action<bool> callback = null, string okText = null, string cancelText = null, bool isCancel = true)
        {
            this.callback = callback;
            txtConfirm.text = string.IsNullOrEmpty(okText) ? "OK" : okText ;
            txtCancel.text = string.IsNullOrEmpty(cancelText) ? "Cancel" : cancelText;
            txtTip.text = string.IsNullOrEmpty(msg) ? "" : msg;
            
            Vector3 v3 = goConfirm.transform.localPosition;
            v3.x = isCancel ? -100 : 0;
            goConfirm.transform.localPosition = v3;
            goCancel.SetActive(isCancel);
            prefab.SetActive(true);
        }

        /// <summary>
        /// 关闭对话框
        /// </summary>
        public void Close()
        {
            //callback = null;
            if (prefab.activeSelf == true) prefab.SetActive(false);
        }

        /// <summary>
        /// 释放
        /// </summary>
        public void Dispose()
        {
            Close();
            txtTip = null;
            txtConfirm = null;
            txtCancel = null;
            btnConfirm = null;
            btnCancel = null;
            goCancel = null;
            goConfirm = null;
            if (prefab != null) GameObject.Destroy(prefab);
            prefab = null;
        }

        private void AddEvents()
        {
            btnConfirm.onClick.AddListener(OnConfirm);
            btnCancel.onClick.AddListener(OnCancel);
        }

        private void RemoveEvents()
        {
            btnConfirm.onClick.RemoveListener(OnConfirm);
            btnCancel.onClick.RemoveListener(OnCancel);
        }

        private void OnConfirm()
        {
            Close();
            if (callback != null) callback(true);
        }

        private void OnCancel()
        {
            Close();
            if (callback != null) callback(false);
        }
    }
}
