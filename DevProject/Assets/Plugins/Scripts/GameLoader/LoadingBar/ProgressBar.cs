﻿using GameLoader.Mgrs;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace GameLoader.LoadingBar
{
    public class ProgressBarInfo
    {
        private bool m_isFixed;
        private List<string> m_progressPaths = new List<string>();
        private List<int[]> m_levelRange = new List<int[]>();
        private List<int[]> m_openServerDayRange = new List<int[]>();

        private int m_lastLevel = -1;
        private int m_lastOpenServerDay = -1;
        private List<string> m_fittingProgressPaths = new List<string>();
        private int m_currentIndex;
        private Dictionary<int, string> m_idsCache = new Dictionary<int, string>();

        public ProgressBarInfo(bool isFixed)
        {
            m_isFixed = isFixed;
        }

        public void SetProgressIds(Dictionary<int, string> ids)
        {
            m_idsCache = ids;
            m_progressPaths = new List<string>();
            m_levelRange = new List<int[]>();
            m_openServerDayRange = new List<int[]>();
            m_fittingProgressPaths = new List<string>();
            foreach (var item in ids)
            {
                var path = GetProgressPath(item.Key, m_isFixed);
                if (string.IsNullOrEmpty(path))
                    continue;
                var list = item.Value.ParseListAny<int>();
                if (list.Count != 4)
                    continue;
                m_progressPaths.Add(path);
                var levelRange = new int[] { list[0], list[1] };//第一二个参数为等级段
                var openServerDayRange = new int[] { list[2], list[3] };//第三四个参数为开服时间天数段
                //Debug.LogError("levelRange: " + levelRange.PackArray());
                //Debug.LogError("openServerDayRange: " + openServerDayRange.PackArray());
                m_levelRange.Add(levelRange);
                m_openServerDayRange.Add(openServerDayRange);
            }
            //Debug.LogError("ids: " + ids.PackMap());
            //Debug.LogError("m_progressPaths: " + m_progressPaths.PackList());
        }

        public void RefreshIds()
        {
            if (m_idsCache.Count != 0)
                SetProgressIds(m_idsCache);
        }

        public string GetNextBg(int level, int openServerDay)
        {
            if (m_lastLevel != level || m_lastOpenServerDay != openServerDay || m_fittingProgressPaths.Count == 0)
            {
                m_fittingProgressPaths = new List<string>();
                var count = m_levelRange.Count > m_openServerDayRange.Count ? m_openServerDayRange.Count : m_levelRange.Count;
                for (int i = 0; i < count; i++)
                {
                    if (m_levelRange[i][0] <= level && level <= m_levelRange[i][1] && m_openServerDayRange[i][0] <= openServerDay && openServerDay <= m_openServerDayRange[i][1])
                    {
                        m_fittingProgressPaths.Add(m_progressPaths[i]);
                    }
                }
                m_lastLevel = level;
                m_lastOpenServerDay = openServerDay;
                m_currentIndex = 0;
            }


            if (m_fittingProgressPaths.Count > 0)
            {
                m_currentIndex++;
                if (m_currentIndex >= m_fittingProgressPaths.Count)
                    m_currentIndex = 0;
                return m_fittingProgressPaths[m_currentIndex];
            }

            return "";
        }

        private string GetProgressPath(int id, bool isFixed)
        {
            var idString = id == 0 ? "" : id.ToString();//特殊处理0的情况，转为空
            string url = string.Format("{0}/RuntimeResource/ProgressBar/progressbg{1}.png.u", Application.persistentDataPath, idString);
            if (File.Exists(url))
            {
                return url;
            }
            else
            {
                if (isFixed)
                {
                    return string.Format("{0}/progressbg{1}.png.u", Application.streamingAssetsPath, idString);
                }
            }
            return "";
        }
    }


    /// <summary>
    /// 加载进度条
    /// 1、全局进度条
    /// </summary>
    public class ProgressBar
    {
        private Canvas canvas;            //进度条Canvas
        private Image imageBg;            //进度条背景
        private Text labTip;              //当前加载文件提示
        private Text labFloatTip;         //动态提示
        private Text labProgress;         //进度标签 

        public GameObject prefab;         //进度条资源
        private RectTransform maskRect;   //进度蒙版
        private RectTransform fxRext;     //进度条粒子特效
        private RectTransform anchor;     //进度条的描点(做自适应)
        private Button btnReboot;         //一键恢复按钮
        private float maskMaxWidth;       //蒙版最大宽度值
        private float fxInitX = 0;
        /// <summary>
        /// 文件大小，单位MB
        /// </summary>
        private float fileSize;

        //private int curProgress;
        //private int targetProgress;
        //private bool isStrongRefresh;

        public int step = 1;
        public const int PANEL_WIDTH = 1280;
        public const int PANEL_HEIGHT = 720;
        public static ProgressBar Instance;
        public bool IsShowing;
        public bool IsShowFloatText;
        public float HideDelay = 700;

        //private int _curBgIndex = 0;
        //private int _curProgressIndex = 0;
        private float _lastChangeTime = 0;
        private int _level;
        private int _openServerDay;
        private string m_lastUrl;

        //private string[] m_fixedProgressIds = new string[] { "", "2", "3" };
        //private string[] m_addtiveProgressIds = new string[] { };
        //private string[] m_fixedProgressPaths;
        //private string[] m_addtiveProgressPaths;
        //private string[] m_totalProgressPaths;
        private ProgressBarInfo m_fixedProgressInfo;
        private ProgressBarInfo m_addtiveProgressInfo;

        private int m_cacheCount = 2;
        private List<Sprite> _bgCache = new List<Sprite>();
        public List<Sprite> BgCache
        {
            get
            {
                return _bgCache;
            }

            set
            {
                _bgCache = value;
            }
        }

        /// <summary>
        /// 创建进度条
        /// </summary>
        /// <param name="prefab">进度条实例</param>
        /// <param name="fxInitX">特效初始位置</param>
        /// <param name="maskMaxWidth">蒙版的初始宽度</param>
        public ProgressBar(GameObject prefab, float fxInitX, float maskMaxWidth)
        {
            this.prefab = prefab;
            this.fxInitX = fxInitX;
            this.maskMaxWidth = maskMaxWidth;
            GameObject.DontDestroyOnLoad(prefab);
            //Debug.Log("[ProgressBar] ========  maskMaxWidth:" + maskMaxWidth + ",fxInitX:" + fxInitX);
            SetSkin();
        }

        private void SetSkin()
        {
            canvas = prefab.GetComponentInChildren<Canvas>();
            Transform trans = canvas.transform.Find("Image_bg");
            if (trans != null)
            {//进度条背景图挂载点更改，为了兼容旧版本，做个判断
                imageBg = trans.GetComponent<Image>();
            }
            else
            {
                imageBg = canvas.GetComponent<Image>();
            }
            RecalculateImageSize();
            anchor = prefab.transform.Find("Canvas/Pivot").GetComponent<RectTransform>();
            labTip = anchor.Find("labTip").GetComponent<Text>();
            labFloatTip = anchor.Find("labFloatTip").GetComponent<Text>();
            labProgress = anchor.Find("Label").GetComponent<Text>();
            maskRect = anchor.Find("LoadBar_mask").GetComponent<RectTransform>();
            fxRext = anchor.Find("LoadBar_mask/LoadBar_fx").GetComponent<RectTransform>();
            InitLabProgress();
            InitLabFloatTip();
            AdjustPosition(anchor);  //自适应调整
            RefreshMaskAndEffect(0);
            BgCache.Add(imageBg.sprite);
            m_fixedProgressInfo = new ProgressBarInfo(true);
            m_addtiveProgressInfo = new ProgressBarInfo(false);
            var fixIds = new Dictionary<int, string>();
            fixIds.Add(0, "0,0,0,9999");
            fixIds.Add(2, "1,9999,0,9999");
            fixIds.Add(3, "1,9999,0,9999");
            m_fixedProgressInfo.SetProgressIds(fixIds);
            //m_fixedProgressPaths = GetProgressPaths(m_fixedProgressIds, true);
            //m_addtiveProgressPaths = GetProgressPaths(m_addtiveProgressIds, false);
            //UpdatemTotalProgressPaths();
            UpdateImage(_level, _openServerDay);
            var btn = prefab.transform.Find("Canvas/BtnReboot");
            if (btn)
            {
                btn.gameObject.SetActive(true);
            }
            else
            {
                //如果资源里没有按钮就创建一个
                var newBtn = GameObject.Instantiate(ConfirmBox.Instance.GoConfirm);
                btn = newBtn.transform;
                btn.SetParent(prefab.transform.Find("Canvas"), false);
                var rectTrans = btn.GetComponent<RectTransform>();
                rectTrans.anchorMax = new Vector2(1, 0);
                rectTrans.anchorMin = new Vector2(1, 0);
                rectTrans.pivot = new Vector2(1, 0);
                rectTrans.anchoredPosition = new Vector2(0, 0);
                var image = btn.GetComponent<Image>();
                image.sprite = null;
                image.color = Color.clear;
            }
            btnReboot = btn.GetComponent<Button>();
            btnReboot.onClick.AddListener(RebootGame);
            var textReboot = btn.Find("Text").GetComponent<Text>();
            textReboot.fontSize = 24;
            var color = 135f / 255;
            textReboot.color = new Color(color, color, color);
            textReboot.text = DefaultLanguage.GetString("reboot_game_btn");
        }

        public void LoadDefaultImage()
        {
            m_fixedProgressInfo.RefreshIds();
            var url = m_fixedProgressInfo.GetNextBg(0, 0);
            if (!string.IsNullOrEmpty(url))
                HttpWrappr.instance.LoadWwwImage(url, OnLoadDefaultBg);
        }

        private void OnLoadDefaultBg(string url, Texture2D bg)
        {
            if (bg != null)
            {
                var oldBg = BgCache[0];
                GameObject.DestroyImmediate(oldBg.texture, true);
                GameObject.DestroyImmediate(oldBg, true);
                BgCache[0] = Sprite.Create(bg, new Rect(0, 0, bg.width, bg.height), new Vector2(0, 0));
                UpdateBg(BgCache[0]);
                LoggerHelper.Info("OnLoadDefaultBg: " + url);
            }
        }

        private void LoadImage(string url)
        {
            if (url != "" && m_lastUrl != url)
            {
                m_lastUrl = url;
                HttpWrappr.instance.LoadWwwImage(url, OnLoadNewestBg);
            }
        }

        private void OnLoadNewestBg(string url, Texture2D bg)
        {
            if (bg != null)
            {
                if (BgCache.Count >= m_cacheCount)
                {
                    var oldBg = BgCache[0];
                    BgCache.RemoveAt(0);
                    GameObject.DestroyImmediate(oldBg.texture, true);
                    GameObject.DestroyImmediate(oldBg, true);
                }
                BgCache.Add(Sprite.Create(bg, new Rect(0, 0, bg.width, bg.height), new Vector2(0, 0)));
            }
        }

        private string GetNextImage(int level, int openServerDay)
        {
            var path = m_addtiveProgressInfo.GetNextBg(level, openServerDay);
            if (path == "")
                path = m_fixedProgressInfo.GetNextBg(level, openServerDay);
            //Debug.LogError("path: " + path + " level: " + level + " openServerDay: " + openServerDay);
            return path;
        }

        private void UpdateImage(int level, int openServerDay)
        {
            //_curProgressIndex++;
            //if (_curProgressIndex >= m_totalProgressPaths.Length)
            //    _curProgressIndex = 0;
            //LoadImage(m_totalProgressPaths[_curProgressIndex]);
            if (UpdateProgressBarBgMgr.ProgressBarDownloading)
                return;
            LoadImage(GetNextImage(level, openServerDay));
        }

        private void RecalculateImageSize()
        {
            RectTransform rect = imageBg.transform as RectTransform;
            Vector2 canvasSize = canvas.GetComponent<RectTransform>().sizeDelta;
            Vector2 oldSize = rect.sizeDelta;
            float scaleWidth = canvasSize.x / oldSize.x;
            float scaleHeight = canvasSize.y / oldSize.y;
            if (scaleWidth < scaleHeight)
            {
                rect.sizeDelta = new Vector2(oldSize.x * scaleHeight, canvasSize.y);
            }
            else
            {
                rect.sizeDelta = new Vector2(canvasSize.x, canvasSize.y * scaleWidth);
            }
        }

        public void SetFixProgressIds(Dictionary<int, string> ids)
        {
            m_fixedProgressInfo.SetProgressIds(ids);
            //UpdatemTotalProgressPaths();
        }

        public void SetOutterProgressIds(Dictionary<int, string> ids)
        {
            m_addtiveProgressInfo.SetProgressIds(ids);
            //UpdatemTotalProgressPaths();
        }

        /// <summary>
        /// 更新皮肤
        /// </summary>
        /// <param name="skin">皮肤资源</param>
        public void UpdateSkin(GameObject skin)
        {
            if (skin == null)
            {
                LoggerHelper.Error("[ProgressBar]皮肤为空，无法更换！");
                return;
            }
            //销毁前一个皮肤资源
            Dispose();
            //设置新皮肤资源
            this.prefab = skin;
            SetSkin();
        }

        /// <summary>
        /// 更换背景图
        /// </summary>
        /// <param name="bg"></param>
        public void UpdateBg(Texture2D bg)
        {
            if (bg == null) return;
            if (imageBg == null) return;
            imageBg.sprite = Sprite.Create(bg, new Rect(0, 0, bg.width, bg.height), new Vector2(0, 0));
        }

        /// <summary>
        /// 更换背景图
        /// </summary>
        /// <param name="bg"></param>
        public void UpdateBg(Sprite sprite)
        {
            if (sprite == null) return;
            if (imageBg == null) return;
            imageBg.sprite = sprite;
        }

        /// <summary>
        /// 更改进度
        /// </summary>
        /// <param name="cur">当前值</param>
        /// <param name="total">总值</param>
        public void UpdateProgress(int cur, int total)
        {
            UpdateProgress(((float)cur) / total);
        }

        /// <summary>
        /// 添加GameObject到anchor
        /// </summary>
        /// <param name="go"></param>
        public void AddGameObject(GameObject go)
        {
            go.transform.SetParent(anchor, false);
        }

        /// <summary>
        /// 更改进度
        /// </summary>
        /// <param name="progress">进度值[0-1]之间</param>
        public void UpdateProgress(float progress, bool isStrongRefresh = false)
        {
            //this.isStrongRefresh = isStrongRefresh;
            //LoggerHelper.Error("UpdateProgress: " + progress + " isStrongRefresh: " + isStrongRefresh);
            if (isStrongRefresh)
            {//强制更新
                TimerHeap.DelTimer(LoadingTimer);
                RefreshMaskAndEffect(progress);
            }
            else
            {//非强制更新
                SmothLoading(progress);
            }
        }

        public void UpdateFileSize(float fileSize)
        {
            this.fileSize = fileSize;
        }

        ///// <summary>
        ///// 更改进度
        ///// </summary>
        ///// <param name="pectage">进度值[0-100]之间</param>
        //public void UpdateProgress(int pectage)
        //{
        //    UpdateProgress(pectage / 100f);
        //}

        /// <summary>
        /// 更新加载说明提示
        /// </summary>
        /// <param name="tip">加载说明提示</param>
        public void UpdateTip(string tip)
        {
            labTip.text = string.Format("{0}", string.IsNullOrEmpty(tip) ? "" : tip);
        }

        /// <summary>
        /// 设置是否显示加载说明提示
        /// </summary>
        /// <param name="visible"></param>
        public void SetFloatTipVisibility(bool visible)
        {
            if (labTip.gameObject.activeSelf != visible)
            {
                labTip.gameObject.SetActive(visible);
            }
        }

        /// <summary>
        /// 更新提示信息
        /// </summary>
        /// <param name="text"></param>
        public void UpdateFloatText(string text)
        {
            if (IsShowing && IsShowFloatText)
                labFloatTip.text = string.IsNullOrEmpty(text) ? "" : text;
        }

        public void ShowFloatText(bool isShow)
        {
            IsShowFloatText = isShow;
        }

        /// <summary>
        /// 字体设置
        /// </summary>
        /// <param name="text"></param>
        public void UpdateFont(Font font)
        {
            labTip.font = font;
            labFloatTip.font = font;
        }

        /// <summary>
        /// 显示进度条
        /// </summary>
        public void Show(bool isFirstShow = true)
        {
            //curProgress = 0;
            //targetProgress = 0;
            //UpdateProgress(0);
            ShowLoading(isFirstShow);
        }

        public void ShowChange()
        {
            UpdateBg(BgCache[0]);
            Show(false);
        }

        public void ShowChangeByLevelAndOpenServerDay(int level, int openServerDay)
        {
            _level = level;
            _openServerDay = openServerDay;
            ShowChange();
        }

        /// <summary>
        /// 关闭进度条
        /// </summary>
        public void Close()
        {
            Close(null);
        }

        /// <summary>
        /// 关闭进度条
        /// </summary>
        public void Close(Action hideLoadingFinished)
        {
            //curProgress = 0;
            //targetProgress = 0;
            //UpdateProgress(0);
            HideLoading(hideLoadingFinished);
        }

        public void Reset()
        {
            TimerHeap.DelTimer(LoadingTimer);
            fileSize = 0;
            RefreshMaskAndEffect(0);
        }

        /// <summary>
        /// 销毁
        /// </summary>
        public void Dispose()
        {
            labTip = null;
            fxRext = null;
            imageBg = null;
            labFloatTip = null;
            labProgress = null;
            //labDescription = null;
            //modelSlot = null;
            if (prefab != null) GameObject.Destroy(prefab);
            prefab = null;
        }

        public void RebootGame()
        {
            ConfirmBox.Instance.Show(DefaultLanguage.GetString("reboot_game"), OnRebootGameCallback, DefaultLanguage.GetString("confirm"), DefaultLanguage.GetString("cancel"));
        }

        private void OnRebootGameCallback(bool isConfirm)
        {
            if (isConfirm)
            {
                VersionManager.Instance.ClearClientAllFile();
            }
        }

        private void RefreshMaskAndEffect(float progress)
        {
            //更改蒙版宽度
            if (progress > 1) progress = 1;
            if (progress < 0) progress = 0;

            Vector2 v2 = maskRect.sizeDelta;
            v2.x = progress * maskMaxWidth;
            maskRect.sizeDelta = v2;
            //更改特效位置
            Vector2 v3 = fxRext.localPosition;
            v3.x = fxInitX + v2.x;
            fxRext.localPosition = v3;

            if (fileSize > 0)
            {
                labProgress.text = string.Format("{0:#}%\n{1:0.00}M/{2:0.00}M", progress * 100, fileSize * progress, fileSize);
            }
            else
            {
                labProgress.text = string.Format("{0:#}%", progress * 100);
            }
        }

        private void InitLabFloatTip()
        {
            if (labFloatTip != null)
            {
                labFloatTip.fontSize = 20;
                labFloatTip.color = new Color(255, 255, 255, 255) / 255f;
                RectTransform rect = labFloatTip.GetComponent<RectTransform>();
                rect.sizeDelta = new Vector2(997, 50);
                rect.localPosition = new Vector3(-18, -40, 0);
                Outline outline = labFloatTip.gameObject.GetComponent<Outline>();
                if (outline == null)
                {
                    outline = labFloatTip.gameObject.AddComponent<Outline>();
                    outline.effectColor = new Color(0, 0, 0, 204) / 255f;
                    outline.effectDistance = new Vector2(1, -1);
                }
            }
        }

        private void InitLabProgress()
        {
            if (labProgress != null)
            {
                Outline outline = labProgress.gameObject.GetComponent<Outline>();
                if (outline == null)
                {
                    outline = labProgress.gameObject.AddComponent<Outline>();
                    outline.effectColor = new Color(0, 0, 0, 204) / 255f;
                    outline.effectDistance = new Vector2(1, -1);
                }
            }
        }

        //进度条自适应调整
        private void AdjustPosition(RectTransform transform)
        {
            if (transform == null) return;
            Vector2 canvasSize = canvas.GetComponent<RectTransform>().sizeDelta;
            transform.anchoredPosition = new Vector2(canvasSize.x / 2 - transform.sizeDelta.x / 2, 116 - canvasSize.y);
        }

        private float CurrentLoadValue = 0;
        private uint LoadingTimer = uint.MaxValue;

        private void ShowLoading(bool isFirstShow)
        {
            //LoggerHelper.Error("ShowLoading: " + isFirstShow);
            CurrentLoadValue = 0;
            if (!isFirstShow)
                labTip.text = string.Empty;
            IsShowing = true;
            if (!prefab.activeSelf) prefab.SetActive(true);
        }

        private void HideLoading(Action hideLoadingFinished = null)
        {
            TimerHeap.DelTimer(LoadingTimer);
            //Hide();
            //if (hideLoadingFinished != null)
            //    hideLoadingFinished();
            int counter = 0;
            var times = HideDelay / 20;
            float seed = (1f - CurrentLoadValue) * (1f / times);
            //LoggerHelper.Error("HideLoading: " + CurrentLoadValue + " times: " + times + " seed: " + seed);
            LoadingTimer = TimerHeap.AddTimer(0, 20, () =>
            {
                CurrentLoadValue = CurrentLoadValue + seed;
                RefreshMaskAndEffect(CurrentLoadValue);
                counter++;
                if (counter > times)
                {
                    TimerHeap.DelTimer(LoadingTimer);
                    TimerHeap.AddTimer(50, 0, () =>
                    {
                        Hide();
                        if (hideLoadingFinished != null)
                            hideLoadingFinished();
                    });
                }
            });
        }

        private void Hide()
        {
            //LoggerHelper.Error("Hide");
            Reset();
            //LoaderDriver.Instance.SetDesignContentScale();
            prefab.SetActive(false);
            IsShowing = false;
            if (UnityPropUtils.realtimeSinceStartup - _lastChangeTime > 10)
            {
                _lastChangeTime = UnityPropUtils.realtimeSinceStartup;
                UpdateImage(_level, _openServerDay);
                //_curBgIndex++;
                //if (_curBgIndex >= BgCache.Count)
                //    _curBgIndex = 0;
            }
        }

        private void SmothLoading(float end)
        {
            //LoggerHelper.Error("SmothLoading");
            TimerHeap.DelTimer(LoadingTimer);
            SetLoading(end);
        }

        private void SetLoading(float end)
        {
            LoadingTimer = TimerHeap.AddTimer(20, 0, () =>
            {
                float seed = (end - CurrentLoadValue) * 0.1f;
                CurrentLoadValue = CurrentLoadValue + seed;
                RefreshMaskAndEffect(CurrentLoadValue);
                SetLoading(end);
            });
        }

    }
}
