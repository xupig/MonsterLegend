﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using GameLoader.Utils;
using GameLoader.Utils.XML;
using UnityEngine;
using GameLoader.Mgrs;

namespace GameLoader.IO
{
    /// <summary>
    /// 文件操作类
    /// </summary>
    public class ResLoader
    {
        #region 加载文件

        #region 加载系统目录下的文件
        /// <summary>
        /// 加载安装目录下的文件
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static String LoadStringFromSystem(String fileName)
        {
            string result;
#if !UNITY_WEBPLAYER
            var fspath = String.Concat(Application.persistentDataPath, ConstString.RutimeResourceConfig, fileName);
            if (File.Exists(fspath))
            {
                result = FileUtils.LoadFile(fspath);
            }
            else
#endif
            {
                result = LoadStringFromResource(fileName.TrimSuffix());
            }
            return result;
        }

        public static byte[] LoadByteFromSystem(String fileName)
        {
#if !UNITY_WEBPLAYER
            if (File.Exists(fileName))
                return File.ReadAllBytes(fileName);
            else
#endif
                return null;
        }
        #endregion


        #region 加载Resource目录下的文件
        public static String LoadStringFromResource(String fileName)
        {
            var text = Resources.Load(fileName);
            if (text != null)
            {
                var result = text.ToString();
                Resources.UnloadAsset(text);
                return result;
            }
            else
            {
                LoggerHelper.Error(fileName + " open error");
                return String.Empty;
            }
        }

        public static byte[] LoadByteFromResource(String fileName)
        {

            byte[] result = null;
            var binAsset = Resources.Load<TextAsset>(fileName);
            if (binAsset != null)
            {
                result = binAsset.bytes;
                Resources.UnloadAsset(binAsset);
            }
            return result;
        }

        public static Stream LoadStreamFromResource(String fileName)
        {
            Stream result = null;
            var binAsset = UnityEngine.Resources.Load<UnityEngine.Object>(fileName) as TextAsset;
            if (binAsset != null)
            {
                result = new MemoryStream(binAsset.bytes);
                //StreamWriter writer = new StreamWriter(result);
                //writer.Write(binAsset.ToString());
                //writer.Flush();
                Resources.UnloadAsset(binAsset);
            }
            return result;
        }
        #endregion


        #region 通过www加载资源
        /// <summary>
        /// 通过WWW下载字节
        /// </summary>
        /// <param name="url">下载地址</param>
        /// <param name="callback">结束回调(url,内容)</param>
        /// <param name="isShowProgressBar">显示下载进度条状态[true:显示,false:隐藏]</param>
        public static void LoadWwwBytes(string url, Action<string, byte[]> callback = null, bool isShowProgressBar = false)
        {
            if (string.IsNullOrEmpty(url))
            {
                if (callback != null) callback(url, null);
            }
            LoaderDriver.Instance.StartCoroutine(LoadWww(url, callback, isShowProgressBar));
        }

        private static IEnumerator LoadWww(string url, Action<string, byte[]> callback, bool isShowProgressBar)
        {
            WWW www = new WWW(url);
            if (isShowProgressBar && DownloadProgress.instance) DownloadProgress.instance.Show(url, 0, www);
            yield return www;
            try
            {
                if (isShowProgressBar && DownloadProgress.instance) DownloadProgress.instance.Close();
                if (string.IsNullOrEmpty(www.error))
                {//加载成功
                    if (www.assetBundle)
                    {//打包文本文件
                        if (callback != null) callback(url, (www.assetBundle.mainAsset as TextAsset).bytes);
                        www.assetBundle.Unload(true);
                        www = null;
                    }
                    else
                    {//原始文本或二进制文件
                        if (callback != null) callback(url, www.bytes);
                        www = null;
                    }
                }
                else
                {//加载失败
                    if (callback != null) callback(url, null);
                    www = null;
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(string.Format("文件加载:{0}[Fail],reason:{1}, \nStackTrace:{2}", url, ex.Message, ex.StackTrace));
                if (www != null && www.assetBundle) www.assetBundle.Unload(true);
                if (callback != null) callback(url, null);
                www = null;
            }
        }

        /// <summary>
        /// 通过WWW下载文件
        /// </summary>
        /// <param name="url">下载地址</param>
        /// <param name="localAbsPath">本地保留路径</param>
        /// <param name="callback">加载结束回调callback(bool) true:成功,false:失败</param>
        /// <param name="isShowProgressBar">显示下载进度条状态[true:显示,false:隐藏]</param>
        public static void LoadWwwBytes(string url, string localAbsPath, Action<bool> callback = null, bool isShowProgressBar = false)
        {
            LoaderDriver.Instance.StartCoroutine(LoadWww(url, localAbsPath, callback, isShowProgressBar, false));
        }

        public static void ExportGameLoaderBytes(string url, string localAbsPath, Action<bool> callback = null, bool isShowProgressBar = false)
        {
            LoaderDriver.Instance.StartCoroutine(LoadWww(url, localAbsPath, callback, isShowProgressBar, true));
        }

        public static void LoadWwwAndUnzip(string url, string localAbsPath, Action<bool> callback, bool isShowProgressBar = false)
        {
            string pkgUrl = string.Concat(url, ".pkg");
            string localPkgAbsPath = string.Concat(localAbsPath, ".pkg");
            Action<bool> cb = (result) => {
                if (result)
                {
                    string rootPath = Path.GetDirectoryName(localPkgAbsPath);
                    PkgFileUtils.DecompressToDirectory(rootPath, localPkgAbsPath, null);
                    if (File.Exists(localPkgAbsPath)) File.Delete(localPkgAbsPath);
                    callback(result);
                }
                else
                {
                    LoadWwwBytes(url, localAbsPath, callback, isShowProgressBar);
                }
            };
            if (File.Exists(localPkgAbsPath)) File.Delete(localPkgAbsPath);
            LoaderDriver.Instance.StartCoroutine(LoadWww(pkgUrl, localPkgAbsPath, cb, isShowProgressBar));
        }

        /// <summary>
        /// 加载单个文件
        /// </summary>
        /// <param name="url">加载文件路径</param>
        /// <param name="localAbsPath">存放目标路径</param>
        /// <param name="isSaveUnzipFile">是否保存文件的解压内容到本地[true:保存文件解压后的内容到本地,false:保存文件原始内容到本地]</param>
        /// <returns></returns>
        private static IEnumerator LoadWww(string url, string localAbsPath, Action<bool> callback, bool isShowProgressBar, bool isSaveUnzipFile = false)
        {
            WWW www = new WWW(url);
            if (isShowProgressBar && DownloadProgress.instance) DownloadProgress.instance.Show(url, 0, www);
            yield return www;
            try
            {
                if (isShowProgressBar && DownloadProgress.instance) DownloadProgress.instance.Close();
                if (string.IsNullOrEmpty(www.error))
                {//加载成功
#if !UNITY_WEBPLAYER
                    string tempFile = localAbsPath + "_tempfile";
                    FileUtils.SaveBytes(tempFile, isSaveUnzipFile ? (www.assetBundle.mainAsset as TextAsset).bytes : www.bytes);
                    if (File.Exists(localAbsPath)) File.Delete(localAbsPath);
                    File.Move(tempFile, localAbsPath);
#endif
                    if (www.assetBundle) www.assetBundle.Unload(true);
                    if (callback != null) callback(true);
                    www = null;
                }
                else
                {//加载失败
                    LoggerHelper.Error(string.Format("文件:{0}[not found]", url));
                    if (www.assetBundle) www.assetBundle.Unload(true);
                    if (callback != null) callback(false);
                    www = null;
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(string.Format("导出文件:{0}[Fail],message:{1}", url, ex.Message));
                LoggerHelper.Error(string.Format("导出文件:{0}[Fail],reseaon:{1}", url, ex.StackTrace));
                if (www != null && www.assetBundle) www.assetBundle.Unload(true);
                if (callback != null) callback(false);
                www = null;
            }
        }

        /// <summary>
        /// 下载文本
        /// </summary>
        /// <param name="url">下载地址</param>
        /// <param name="callback">加载结束回调callback(url,内容)</param>
        /// <param name="isShowProgressBar">显示下载进度条状态[true:显示,false:隐藏]</param>
        public static void LoadWwwText(string url, Action<string, string> callback, bool isShowProgressBar = false)
        {
            LoaderDriver.Instance.StartCoroutine(LoadText(url, callback, isShowProgressBar));
        }

        private static IEnumerator LoadText(string url, Action<string, string> callback, bool isShowProgressBar)
        {
            WWW www = new WWW(url);
            if (isShowProgressBar && DownloadProgress.instance) DownloadProgress.instance.Show(url, 0, www);
            yield return www;

            try
            {
                if (isShowProgressBar && DownloadProgress.instance) DownloadProgress.instance.Close();
                if (www.assetBundle)
                {
                    if (callback != null) callback(url, (www.assetBundle.mainAsset as TextAsset).text);
                    www.assetBundle.Unload(false);
                    www = null;
                }
                else
                {
                    if (callback != null) callback(url, www.text);
                    www = null;
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(string.Format("文本文件:{0} Load [Fail],reason:{1}", url, ex.StackTrace));
                if (callback != null) callback(url, null);
            }
        }
        #endregion



        #endregion
    }
}
