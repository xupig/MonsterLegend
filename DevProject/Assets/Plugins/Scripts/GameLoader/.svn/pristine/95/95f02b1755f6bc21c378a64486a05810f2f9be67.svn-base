﻿using System;

namespace GameLoader.Utils
{
    public class BitConverterExtend
    {
        public static byte[] GetBytes(bool value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetBytes(char value)
        {
            byte[] array = BitConverter.GetBytes(value);
            Array.Reverse(array);
            return array;
        }

        public static byte[] GetBytes(short value)
        {
            byte[] array = BitConverter.GetBytes(value);
            Array.Reverse(array);
            return array;
        }

        public static byte[] GetBytes(int value)
        {
            byte[] array = BitConverter.GetBytes(value);
            Array.Reverse(array);
            return array;
        }

        public static byte[] GetBytes(long value)
        {
            byte[] array = BitConverter.GetBytes(value);
            Array.Reverse(array);
            return array;
        }

        public static byte[] GetBytes(ushort value)
        {
            byte[] array = BitConverter.GetBytes(value);
            Array.Reverse(array);
            return array;
        }

        public static byte[] GetBytes(uint value)
        {
            byte[] array = BitConverter.GetBytes(value);
            Array.Reverse(array);
            return array;
        }

        public static byte[] GetBytes(ulong value)
        {
            byte[] array = BitConverter.GetBytes(value);
            //Array.Reverse(array); //64位不用轉了 By:Ari
            return array;
        }

        public static byte[] GetBytes(float value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] GetBytes(double value)
        {
            return BitConverter.GetBytes(value);
        }

        public static char ToChar(byte[] value, int startIndex)
        {
            return (char)BitConverterExtend.ToInt16(value, startIndex);
        }

        public static short ToInt16(byte[] value, int startIndex)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            if ((ulong)startIndex >= (ulong)((long)value.Length))
            {
                throw new ArgumentOutOfRangeException("startIndex");
            }
            if (startIndex > value.Length - 2)
            {
                throw new ArgumentException("startIndex");
            }
            short result;
            result = (short)((int)(value[startIndex]) << 8 | (int)(value[startIndex + 1]));
            return result;
        }

        public static int ToInt32(byte[] value, int startIndex)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            if ((ulong)startIndex >= (ulong)((long)value.Length))
            {
                throw new ArgumentOutOfRangeException("startIndex");
            }
            if (startIndex > value.Length - 4)
            {
                throw new ArgumentException("startIndex");
            }
            int result;
            result = ((int)(value[startIndex]) << 24 | (int)value[startIndex + 1] << 16 | (int)value[startIndex + 2] << 8 | (int)value[startIndex + 3]);
            return result;
        }

        public static long ToInt64(byte[] value, int startIndex)
        {
            return (long)BitConverter.ToInt64(value, startIndex);
            /*
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            if ((ulong)startIndex >= (ulong)((long)value.Length))
            {
                throw new ArgumentOutOfRangeException("startIndex");
            }
            if (startIndex > value.Length - 8)
            {
                throw new ArgumentException("startIndex");
            }
            long result;
            int num3 = (int)(value[startIndex]) << 24 | (int)value[startIndex + 1]<< 16 | (int)value[startIndex + 2] << 8 | (int)value[startIndex + 3];
            int num4 = (int)value[startIndex + 4] << 24 | (int)value[startIndex + 5] << 16 | (int)value[startIndex + 6] << 8 | (int)value[startIndex + 7];
            result = (long)((ulong)num4 | (ulong)((ulong)((long)num3) << 32));
            UnityEngine.Debug.LogError("result:" + result);
            return result;*/
        }

        public static ushort ToUInt16(byte[] value, int startIndex)
        {
            return (ushort)BitConverterExtend.ToInt16(value, startIndex);
        }

        public static uint ToUInt32(byte[] value, int startIndex)
        {
            return (uint)BitConverterExtend.ToInt32(value, startIndex);
        }

        public static ulong ToUInt64(byte[] value, int startIndex)
        {
            return (ulong)BitConverterExtend.ToInt64(value, startIndex);
        }

        public static double ToDouble(byte[] value, int startIndex)
        {
            return BitConverter.ToDouble(value, startIndex);
        }

        public static float ToSingle(byte[] value, int startIndex)
        {
            return BitConverter.ToSingle(value, startIndex);
        }
    }
}
