﻿//#define IPHONE_TEST
using UnityEngine;
using System;
using GameLoader.IO;
using System.Collections.Generic;
using GameLoader.Utils;
using GameLoader.Config;
using GameLoader.Mgrs;
using System.Reflection;
using GameLoader;
using GameLoader.PlatformSdk;
using GameLoader.LoadingBar;
#if !UNITY_WEBPLAYER
using System.IO;
#endif

public class DriverMonoBehaviour : MonoBehaviour
{

}

public class TimerHeapDriver : DriverMonoBehaviour
{
    void Update()
    {
        if (Time.timeScale == 0) return;
        GameLoader.Utils.Timer.TimerHeap.Tick();
    }
}

public class FrameTimerHeapDriver : DriverMonoBehaviour
{
    void Update()
    {
        GameLoader.Utils.Timer.FrameTimerHeap.Tick();
    }
}

/// <summary>
/// 应用初始化流程
/// 1、初始化SystemConfig.Init()
///    1-1、加载并初始化system_config.xml
///    1-2、加载并初始化cfg.xml
/// 2、加载GeneratedLibs中指定的引擎库(.dll)
/// 3、加载MogoEngine.dll引擎库
/// 4、实例EngineDriver.cs
///    3-1、初始化MogoWorld.Init();
///    3-2、初始化资源_resources.mk
///    3-3、调用LoaderDriver.OnEngineReady()
/// 5、加载主程序库(GameLogic.dll)
/// 6、到主程序初始化流程结束
/// </summary>
public class LoaderDriver : MonoBehaviour
{
    private delegate void VoidDelegate();

    public string versionNo = "";
    public static LoaderDriver Instance;

    private int beginTime;
    //private string cfgXml;
    //private string commonXml;
    private Dictionary<string, string> xmlDic = new Dictionary<string, string>();
    //private VoidDelegate onUpdate;

    public string GameLoaderName = "Bytes$GameLoader.bytes.u";
    private const string LIBS = "libs/";
    private const string ENGINE_DLL = "MogoEngine.dll";     //引擎库
    private const string GAME_LOGIC_DLL = "GameMain.dll";  //主程序库
    private const string CLIENT_CLASS_NAME = "GameMain.Main";
    public string activeTime = "";
    public const int FPS = 30;

    private List<string> GeneratedLibs = new List<string>
    {
        "SerializableData.dll",
        "SerializableDataSerializer.dll",
        "GameResource.dll",
        "ACTSystem.dll",
        "MogoAI.dll",
        "XMLDefine.dll",
        "XMLSerializer.dll",
        "XMLManager.dll",
        "DOTween.dll"
    };

    void Awake()
    {
        Instance = this;
        DriverLogger.Info("[LoaderDriver] ----------------------------- Awake ----------------------------- [Start]");
        LoggerHelper.Info("[LoaderDriver] ----------------------------- Awake ----------------------------- [Start]");
        Application.targetFrameRate = FPS;
        UnityEngine.Object.DontDestroyOnLoad(gameObject);
        Screen.sleepTimeout = (int)SleepTimeout.NeverSleep;
    }

    void Start()
    {
        AddComponentToDriver<TimerHeapDriver>();
        AddComponentToDriver<FrameTimerHeapDriver>();
    }

    public T AddComponentToDriver<T>() where T : DriverMonoBehaviour
    {
        var parentObj = this.gameObject;
        var subObj = new GameObject(typeof(T).ToString());
        subObj.transform.SetParent(parentObj.transform, false);
        return subObj.AddComponent<T>();
    }

    //游戏激活日志
    void LogGameStart()
    {
        activeTime = LogUploader.GetTimestamp().ToString();
        UploadLogMgr.GetInstance().UploadLog(new string[] { 
                "log_type", "1",
                "active_time", activeTime,
            });
    }

    public void SetDesignContentScale()
    {
        int targetWidth;
        if(!int.TryParse(SystemConfig.GetValueInCfg("TargetWidth"), out targetWidth))
        {
            targetWidth = 1440;
        }
        if (Screen.width > targetWidth)
        {
            var targetHeight = (Screen.height * targetWidth) / Screen.width;
            //LoggerHelper.Error(string.Format("SetResolution, before {0}*{1}, after {2}*{3}", Screen.width, Screen.height, targetWidth, targetHeight));
            Screen.SetResolution(targetWidth, targetHeight, true, FPS);
        }
    }

    //void OnApplicationPause(bool paused)
    //{
    //    if (!paused)
    //    {
    //        SetDesignContentScale();
    //    }
    //}

    void OnApplicationQuit()
    {
        if (VersionManager.Instance != null)
            VersionManager.Instance.Release();
    }

    /// <summary>
    /// 主线程回调
    /// </summary>
    public static void Invoke(Action callback)
    {
        GameLoader.Utils.Timer.TimerHeap.AddTimer(0, 0, callback);
    }

    public static void Invoke<T>(Action<T> callback, T args1)
    {
        GameLoader.Utils.Timer.TimerHeap.AddTimer(0, 0, callback, args1);
    }

    public static void Invoke<T, U>(Action<T, U> callback, T args1, U args2)
    {
        GameLoader.Utils.Timer.TimerHeap.AddTimer(0, 0, callback, args1, args2);
    }

    public static void Invoke<T, U, V>(Action<T, U, V> callback, T args1, U args2, V args3)
    {
        GameLoader.Utils.Timer.TimerHeap.AddTimer(0, 0, callback, args1, args2, args3);
    }

    /// <summary>
    /// A-1、Driver.cs中调用此方法，开始应用初始化
    /// </summary>
    /// <param name="cfgXml"></param>
    /// <param name="commonXml"></param>
    /// <param name="isIphoneTest">是否为mac平台的测试环境，true:是,false:否</param>
    /// <param name="args">额外传递参数数组</param>
    public void Setup(Dictionary<string, string> xmlDic, bool isIphoneTest, object[] args)
    {
        DriverLogger.Info(string.Format("[Setup()] xmlDic.len:{0}", xmlDic != null ? xmlDic.Count : -1));
        LoggerHelper.Info(string.Format("[Setup()] xmlDic.len:{0}", xmlDic != null ? xmlDic.Count : -1));
        ConfirmBox.Instance = new ConfirmBox((GameObject)args[0]);
        ProgressBar.Instance = new ProgressBar((GameObject)args[1], (float)args[2], (float)args[3]);
        //onUpdate += ProgressBar.Instance.OnUpdate;
        ProgressBar.Instance.Show();
        ProgressBar.Instance.UpdateProgress(0.1f, true);
        ProgressBar.Instance.UpdateTip(DefaultLanguage.GetString("init"));

        this.xmlDic = xmlDic;

        SystemConfig.isIphoneTest = isIphoneTest;
        gameObject.AddComponent<DownloadProgress>();                     //刷新进度条定时器
        VersionManager.Instance = new VersionManager();
        SystemConfig.Init(xmlDic, InitSystemConfigCallback);  //初始化系统配置
    }

    //0表示更新开始，1表示更新成功，-1表示更新失败
    public void LogUpdate(int status)
    {
        string reach_update_time = LogUploader.GetTimestamp().ToString();
        string update_status = status.ToString();
        string update_time = LogUploader.GetTimestamp().ToString();
        string use_time = (Environment.TickCount - beginTime).ToString();
        UploadLogMgr.GetInstance().UploadLog(new string[] { 
                "log_type", "2",
                "reach_update_time", reach_update_time,
                "update_status", update_status,
                "update_time", update_time,
                "use_time", use_time,
            });
    }

    /// <summary>
    /// A-2、初始化SDK，检查是否有特殊渠道配置
    /// </summary>
    /// <param name="result">初始化结果[true:初始化成功,false:初始化失败]</param>
    /// <param name="downloadCfgResult">cfg.xml|common.xml加载结果[true:成功,false:失败]</param>
    private void InitSystemConfigCallback(bool result, bool downloadCfgResult)
    {
        if (result)
        {//系统配置初始化成功
            AddPlatformSdkScript();
            SystemConfig.InitForChangeChannel(InitSystemConfigForChannelCallback);  //初始化系统配置
        }
        else
        {//失败
            if (!downloadCfgResult)
            {//cfg.xml|common.xml加载失败--表示网络不可达
                ConfirmBox.Instance.Show(DefaultLanguage.GetString("bad_network"), OnConfirm, DefaultLanguage.GetString("try"), DefaultLanguage.GetString("restartGame"));
            }
            else
            {
                ConfirmBox.Instance.Show("SystemConfig Init Fail！", OnConfirm, DefaultLanguage.GetString("try"), DefaultLanguage.GetString("restartGame"));
            }
        }
    }

    /// <summary>
    /// A-2.1、开启版本检查 或 加载引擎库
    /// </summary>
    /// <param name="result">初始化结果[true:初始化成功,false:初始化失败]</param>
    /// <param name="downloadCfgResult">cfg.xml|common.xml加载结果[true:成功,false:失败]</param>
    private void InitSystemConfigForChannelCallback(bool result, bool downloadCfgResult)
    {
        if (result)
        {//系统配置初始化成功
            LogGameStart();
            versionNo = string.Concat("版本号:", SystemConfig.GetValueInCfg("VersionNo"));  //提取版本号
            beginTime = Environment.TickCount;
            //LogUpdate(0);
            switch (UnityPropUtils.Platform)
            {
                case RuntimePlatform.Android:
                    LoggerHelper.Info("[Android发布模式] 版本检查[Start]");
                    VersionManager.Instance.CheckVersion(OnUpdateVersionCallback, true);
                    break;
                case RuntimePlatform.IPhonePlayer:
                    LoggerHelper.Info("[IPhone发布模式] 版本检查[Start]");
                    VersionManager.Instance.CheckVersion(OnUpdateVersionCallback, true);
                    break;
                //case RuntimePlatform.WindowsWebPlayer://为排除警告
                //LoggerHelper.Info("[Web发布模式] 版本检查[Start]");
                //VersionManager.Instance.CheckVersion(OnUpdateVersionCallback, false);
                //break;
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.WindowsPlayer:
                    if (SystemConfig.ReleaseMode)
                    {
                        LoggerHelper.Info(string.Format("[{0}] 版本检查开启状态:{1}", SystemConfig.isIphoneTest ? "IphoneTest模式" : "开发模式下--发布模式", SystemConfig.IsOpenVersionCheck));
                        VersionManager.Instance.CheckVersion(OnUpdateVersionCallback, SystemConfig.IsOpenVersionCheck);
                    }
                    else
                    {
                        LoggerHelper.Info("[开发模式] 加载引擎库[Start]");
                        //VersionManager.Instance.TestUpdateProgressBar();
                        OnUpdateVersionCallback();
                    }
                    break;
            }
        }
        else
        {//失败
            if (!downloadCfgResult)
            {//cfg.xml|common.xml加载失败--表示网络不可达
                ConfirmBox.Instance.Show(DefaultLanguage.GetString("bad_network"), OnConfirm, DefaultLanguage.GetString("try"), DefaultLanguage.GetString("restartGame"));
            }
            else
            {
                ConfirmBox.Instance.Show("SystemConfig Init Fail！", OnConfirm, DefaultLanguage.GetString("try"), DefaultLanguage.GetString("restartGame"));
            }
        }
    }

    //添加平台Sdk控制脚本
    private void AddPlatformSdkScript()
    {
        if (UnityPropUtils.Platform == RuntimePlatform.Android && SystemConfig.IsUsePlatformSdk)
        {//使用平台sdk更新
            if (SystemConfig.Language == "cn")
            {
                gameObject.AddComponent<AndroidSdkMgr>();
                //new AndroidSdkMgr();
                LoggerHelper.Info("[AddPlatformSdk] 添加AndroidSdkMgr[OK]");
            }
            else if (SystemConfig.Language == "kr")
            {
                //gameObject.AddComponent<AndroidKoreaSdkMgr>();
                //new AndroidKoreaSdkMgr();
                LoggerHelper.Info("[AddPlatformSdk] 添加AndroidKoreaSdkMgr[OK]");
            }
            else
            {
                gameObject.AddComponent<AndroidSdkMgr>();
                //new AndroidSdkMgr();
                LoggerHelper.Info("[AddPlatformSdk] 添加AndroidSdkMgr[OK]");
            }
        }
        else if (UnityPropUtils.Platform == RuntimePlatform.IPhonePlayer && SystemConfig.IsUsePlatformSdk)
        {
            gameObject.AddComponent<IOSSdkMgr>();
            //new IOSSdkMgr();
            LoggerHelper.Info("[AddIOSSdk] 添加IOSSdkMgr[OK]");
        }
        else
        {
            gameObject.AddComponent<PlatformSdkMgr>();
            //new PlatformSdkMgr();
            LoggerHelper.Info("[AddPlatformSdk] 添加PlatformSdkMgr[OK]");
        }
    }

    //private void OnSetNetwork(bool result)
    //{
    //    if (result) SystemConfig.Init(xmlDic, InitSystemConfigCallback);
    //    else
    //        OnResetGame(result);
    //    //else PlatformSdkMgr.Instance.SetNetwork();
    //}

    private void OnConfirm(bool result)
    {
        if (result) SystemConfig.Init(xmlDic, InitSystemConfigCallback);
        else OnResetGame(result);
    }

    private void OnResetGame(bool result)
    {
        PlatformSdkMgr.Instance.RestartGame();
    }

    //版本更新回调--延时执行
    internal void OnUpdateVersionCallback()
    {
        LoggerHelper.Info(string.Format("版本检查[OK],cost:{0}ms", Environment.TickCount - beginTime));
        ProgressBar.Instance.UpdateProgress(0.25f, true);
        ProgressBar.Instance.UpdateTip(DefaultLanguage.GetString("load_system"));
        Invoke("LoadEngine", 0.02f);
        if (VersionManager.Instance.isUpdateLog)
        {
            LogUpdate(1);
        }
    }

    //A-3、加载引擎库
    private void LoadEngine()
    {
        //ProgressBar.Instance.UpdateTip("正在载入引擎库");
        //LoggerHelper.Info("[GameLoader] 哈哈GameLoader.dll热更新成功啦----！");
        LoggerHelper.Info(string.Format("[LoadEngine] 正在载入引擎库 releaseMode:{0}[Start]", SystemConfig.ReleaseMode));
        InitFileSystem();
        switch (UnityPropUtils.Platform)
        {
            case RuntimePlatform.Android:
                LoggerHelper.Info("[Android发布模式][LoadEngine] 加载引擎库[Start]");
                LoadLibsFromFileSystem();
                break;
            case RuntimePlatform.IPhonePlayer:
                LoggerHelper.Info("[IPhone发布模式][LoadEngine] 加载引擎库[Start]");
#if UNITY_IPHONE
                 gameObject.AddComponent<EngineDriver>();
#endif
                break;
                //case RuntimePlatform.WindowsWebPlayer://为排除警告
                //    LoggerHelper.Info("[WebPlayer发布模式][LoadEngine] 加载引擎库[Start]");
//#if UNITY_WEBPLAYER
//                 gameObject.AddComponent<EngineDriver>();
//#endif
//                break;
            case RuntimePlatform.OSXEditor:
            case RuntimePlatform.WindowsEditor:
            case RuntimePlatform.WindowsPlayer:

                //LoadLibsFromFileSystem();
                if (SystemConfig.isIphoneTest)
                {
                    LoggerHelper.Info("[IphoneTest模式][LoadEngine] 加载引擎库[Start]");
                    gameObject.AddComponent(Type.GetType("EngineDriver"));
                }
                else
                {
                    if (SystemConfig.ReleaseMode)
                    {
                        LoggerHelper.Info("[开发模式下--发布模式][LoadEngine] 加载引擎库[Start]");
#if UNITY_WEBPLAYER
                            var driver = gameObject.AddComponent<EngineDriver>();
                            InspectorSetting.InspectingTool.Inspect(new GameMain.Inspectors.InspectingMogoFileSystem(null), driver.gameObject);
#else
                        LoadLibsFromFileSystem();
#endif
                    }
                    else
                    {
                        LoggerHelper.Info("开发模式[LoadEngine] 加载引擎库[Start]");
                        LoadLibsFromLocal();
                    }
                }
                break;
        }
    }

    private void InitFileSystem()
    {
        switch (UnityPropUtils.Platform)
        {
            case RuntimePlatform.Android:
            case RuntimePlatform.IPhonePlayer:
            //case RuntimePlatform.WindowsWebPlayer://为排除警告
            //    SystemConfig.IsUseFileSystem = true;
            //    break;
            case RuntimePlatform.OSXEditor:
            case RuntimePlatform.WindowsEditor:
            case RuntimePlatform.WindowsPlayer:
                if (SystemConfig.isIphoneTest)
                {
                    SystemConfig.IsUseFileSystem = true;
                }
                else
                {
                    if (SystemConfig.ReleaseMode)
                    {
                        SystemConfig.IsUseFileSystem = true;
                    }
                    else
                    {
                        SystemConfig.IsUseFileSystem = false;
                    }
                }
                break;
        }
        //加载pkg文件
        if (SystemConfig.IsUseFileSystem)
        {
            MogoFileSystem.Instance.Init();
        }
    }

    //发布模式--从pkg文件载入dll
    private void LoadLibsFromFileSystem()
    {
        for (int i = 0; i < GeneratedLibs.Count; i++)
        { //载入GeneratedLibs目录下的dll
            //Debug.LogError("LoadLibsFromFileSystem：：" + GeneratedLibs[i]);
            LoadDllFromFileSystem(GeneratedLibs[i]);
            //Debug.LogError("LoadLibsFromFileSystem：：" + GeneratedLibs[i]);
        }
        //挂载MogoEngine.dll中EngineDriver.cs脚本
        ProgressBar.Instance.UpdateProgress(0.3f, true);
        Assembly assembly = LoadDllFromFileSystem(ENGINE_DLL);
        gameObject.AddComponent(assembly.GetType("EngineDriver"));
    }

    /// <summary>
    /// 载入指定代码库
    /// </summary>
    /// <param name="fileName">指定文件代码库文件名,例如：MogoEngine.dll</param>
    private Assembly LoadDllFromFileSystem(string fileName)
    {
        fileName = string.Concat(LIBS, fileName);
        bool isExistFile = FileAccessManager.IsFileExist(fileName);
        if (isExistFile)
        {
            try
            {
                byte[] enData = FileAccessManager.LoadBytes(fileName);
                byte[] deData = DESCrypto.Decrypt(enData, KeyUtils.GetResNumber());
                deData = PkgFileUtils.UnpackMemory(deData);
                return Assembly.Load(deData);
            }
            catch (Exception e)
            {
                LoggerHelper.Error("LoadDllFromFileSystem " + fileName + " failed:" + e.Message);
                return null;
            }

        }
        else
        {
            LoggerHelper.Info(string.Format("代码库文件:{0},{1}", fileName, isExistFile ? "存在" : "找不到"));
            return null;
        }
    }

    //开发模式--载入dll
    private void LoadLibsFromLocal()
    {
#if !UNITY_WEBPLAYER
        string baseFolder = SystemConfig.DllPath;
        //foreach (string gl in GeneratedLibs)
        //{
        //    LoggerHelper.Error("GeneratedLibs:baseFolder//" + gl);
        //}
        for (int i = 0; i < GeneratedLibs.Count; i++)
        { //载入GeneratedLibs目录下的dll
            string dllPath;
            if (GeneratedLibs[i].StartsWith("XML"))
                dllPath = string.Concat(baseFolder, "GeneratedLib/", GeneratedLibs[i]);
            else
                dllPath = string.Concat(baseFolder, GeneratedLibs[i]);
            //LoggerHelper.Error("dllPath:"+dllPath);

            Assembly.Load(File.ReadAllBytes(dllPath));
        }

        //挂载MogoEngine.dll中EngineDriver.cs脚本
        Assembly assembly = Assembly.Load(File.ReadAllBytes(string.Concat(baseFolder, ENGINE_DLL)));
        gameObject.AddComponent(assembly.GetType("EngineDriver"));
#endif
    }

    //A-5、当EngineDriver的初始化完成，将调用该方法，加载客户端主程序
    public void LoadClient()
    {
        //ProgressBar.Instance.UpdateTip("正在载入主程序");
        LoggerHelper.Debug("正在载入主程序");
        switch (UnityPropUtils.Platform)
        {
            case RuntimePlatform.Android:
                //GameLoader.Interfaces.IClient client = (GameLoader.Interfaces.IClient)System.Activator.CreateInstance(MainClassType);
                LoadClientAtAndorid("Android发布模式");
                break;
            case RuntimePlatform.IPhonePlayer:
                LoadClientAtIOS("IOS发布模式");
                break;
            //case RuntimePlatform.WindowsWebPlayer://为排除警告
            //    LoadClientAtWeb("IOS发布模式");
            //    break;
            case RuntimePlatform.OSXEditor:
            case RuntimePlatform.WindowsEditor:
            case RuntimePlatform.WindowsPlayer:
                if (SystemConfig.isIphoneTest)
                {
                    LoggerHelper.Info("[IphoneTest] 加载主程序");
                    Activator.CreateInstance(Type.GetType(CLIENT_CLASS_NAME));
                }
                else
                {
                    if (SystemConfig.ReleaseMode)
                    {
#if UNITY_WEBPLAYER
                         LoadClientAtWeb("开发模式下--发布模式");
#else
                        LoadClientAtAndorid("开发模式下--发布模式");
#endif
                    }
                    else
                    {
#if !UNITY_WEBPLAYER
                        string dllPath = string.Concat(SystemConfig.DllPath, GAME_LOGIC_DLL);
                        LoggerHelper.Info(string.Format("[开发模式] 加载主程序代码库:{0}[Start]", dllPath));
                        Assembly assembly = System.Reflection.Assembly.Load(FileUtils.LoadByteFile(dllPath));
                        assembly.CreateInstance(CLIENT_CLASS_NAME);
#endif
                    }
                }
                break;
        }
    }

    //安卓下加载主程序
    private void LoadClientAtAndorid(string logTitle)
    {
        LoggerHelper.Info(string.Format("[{0}][LoadClient] 加载主程序:{1}[Start]", logTitle, GAME_LOGIC_DLL));
        ConfirmBox.Instance.Close();
        Assembly assembly = LoadDllFromFileSystem(GAME_LOGIC_DLL);
        assembly.CreateInstance(CLIENT_CLASS_NAME);
    }

    //IOS下加载主程序
    private void LoadClientAtIOS(string logTitle)
    {
#if UNITY_IPHONE
		LoggerHelper.Info(string.Format("[{0}][LoadClient] 加载主程序:{1}[Start]", logTitle, GAME_LOGIC_DLL));
		ConfirmBox.Instance.Close();
		new GameMain.Main();
#endif
    }

    //IOS下加载主程序
    private void LoadClientAtWeb(string logTitle)
    {
#if UNITY_WEBPLAYER
		LoggerHelper.Info(string.Format("[{0}][LoadClient] 加载主程序:{1}[Start]", logTitle, GAME_LOGIC_DLL));
		ConfirmBox.Instance.Close();
		new GameMain.Main();
#endif
    }
}

