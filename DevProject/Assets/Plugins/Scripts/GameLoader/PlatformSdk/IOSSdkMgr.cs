﻿using System;
using System.Collections.Generic;
using UnityEngine;
using GameLoader.Utils;
using GameLoader.Config;
using System.Runtime.InteropServices;
using LitJson;
namespace GameLoader.PlatformSdk
{
    public class LoginInfo
    {
        public static string userName = "";
        public static string platformAccount = "";
        public static string platformUid = "";
        public static string sign = "";
        public static string token = "";
        public static string timestamp = "";
        public static string platformName = "";
        public static string ip = "";
        public static string port = "";
        public static string platformId = "";     
    }

    public class IOSSdkMgr:PlatformSdkMgr
    {
    //暂时关闭SDK接口打包
//#if UNITY_IPHONE && UNITY_EDITOR
        public delegate float FLOAT_NO_ARG();
        public delegate void VOID_STR_ARG(String str);
        public delegate void VOID_2STR_ARGS(String a1, String a2);
        public delegate void VOID_3STR_ARGS(String a1, String a2, String a3);
        public delegate void VOID_6STR_ARGS(String a1, String a2, String a3, String a4, String a5, String a6);
        public delegate void VOID_10STR_ARGS(String a1, String a2, String a3, String a4, String a5, String a6, String a7, String a8, String a9, String a10);
        public delegate void VOID_INT_ARG(int data);
        public delegate void VOID_NO_ARG();

        private static Action<List<string>> loginSuccessFunction = null;
        private static Action<int> loginFailFunction = null;
        private static Action<string> setJsonToSDKFunction = null;
        private static Action switchAccountCB = null;
        private String UserID = ""; //服务器登录验证返回的 UserID
        private int state = 0;

        public override PlatformSdkType PlatformSdkMgrType
        {
            get
            {
                return PlatformSdkType.IOS;
            }
        }

        public override string Uid
        {
            get
            {
                return LoginInfo.platformUid;
            }
            set
            {
                
            }
        }

        void Start()
        {
            Init();
        }

        public void Init()
        {
            Instance = this;
            InitAccountCenter();
            RegisterAccountCenterCallbacks(InitLoginInformation);
            RegisterIAPCallbacks(VoidNoArg, VoidNoArg, LoginSDKReture, VoidInitArg);
            InitWxID();
        }

        override public void InitWxID()
        {
            //InitWeiXin(OnWXShareSucess);
        }

        public override void Login(Action<List<string>> loginSuccessCB, Action<int> loginFailtCB)
        {
            loginSuccessFunction = loginSuccessCB;
            loginFailFunction = loginFailtCB;
            LoggerHelper.Info("[SDK] OnLoginFirst");
            OnLoginFirst();
        }

        public override void SetLoginDataToSDK(string jsonData, Action<string> setJsonToSDKCB)
        {
            setJsonToSDKFunction = setJsonToSDKCB;
            OnLoginSecond(jsonData);
            ParsingJsonData(jsonData);
        }

        public override void SetSwitchAccountCB(Action _switchAccountCB)
        {
            switchAccountCB = _switchAccountCB;
        }

        //平台切换账号--回调函数
        public static void OnSwitchAccountBySDK(string msg)
        {
            if (switchAccountCB != null)
            {
                switchAccountCB();
                switchAccountCB = null;
            }
        }

        public override void ParsingJsonData(string jsonData)
        {
            JsonData jsonData2 = JsonMapper.ToObject(jsonData);

            if (!jsonData2.Keys.Contains("content"))
            {
                LoggerHelper.Info(string.Format("[AndroidSdk] Login SetLoginDataToSDK :content is null"));
                return;
            }

            JsonData contentJson = jsonData2["content"];

            if (!contentJson.Keys.Contains("data"))
            {
                LoggerHelper.Info(string.Format("[AndroidSdk] Login SetLoginDataToSDK :data is null"));
                return;
            }

            JsonData dataJson = contentJson["data"];

            if (!dataJson.Keys.Contains("userId"))
            {
                LoggerHelper.Info(string.Format("[AndroidSdk] Login SetLoginDataToSDK :userId is null"));
                return;
            }
            UserID = dataJson["userId"].ToString();
        }

        /// <summary>
        /// 充值
        /// 
        ///     	params.put(SDKParamKey.PAY.CP_ORDER_ID, orderId); // CP 订单号
        ///params.put(SDKParamKey.PAY.ROLE_ID, roleId); // 角色ID
        ///params.put(SDKParamKey.PAY.ROLE_NAME , roleName); // 角色名称
        ///params.put(SDKParamKey.PAY.ROLE_LEVEL, roleLevel); // 角色等级
        ///params.put(SDKParamKey.PAY.VIP_LEVEL, vipLevel); // 角色VIP等级
        ///params.put(SDKParamKey.PAY.SERVER_ID, serverId); // 服务器 ID
        ///params.put(SDKParamKey.PAY.SERVER_NAME, serverName); // 服务器名称
        ///params.put(SDKParamKey.PAY.AMOUNT, shopMoney); // 商品金额(单位:分)
        // 购买数量 ( 如 : 100元宝 填 100 )
        ///params.put(SDKParamKey.PAY.PRODUCT_COUNT, shopCount);
        ///params.put(SDKParamKey.PAY.PRODUCT_NAME, shopName); // 商品名称 ( 如 : XX元宝 )
        ///params.put(SDKParamKey.PAY.PRODUCT_TYPE, shopType); // 商品类型(如: 钻石, 礼包)
        ///params.put(SDKParamKey.PAY.PRODUCT_ID, shopId); // 商品ID(需要传递对应的ID)
        ///params.put(SDKParamKey.PAY.DESC, shopDes); // 商品描述
        ///params.put(SDKParamKey.PAY.RATE, shopRate); // 兑换比率 默认 填 10
        /// 
        /// </summary>
        public override void BuyMarkt(string orderId, string roleId, string roleName, string roleLevel, string vipLevel,
            string serverId, string serverName, string shopMoney, string shopCount, string shopName,
            string shopType, string shopId, string shopDes, string shopRate, string shopCallbackUrl)
        {
            MakePaymentWithParams(orderId,roleId,roleName,roleLevel,vipLevel,serverId,serverName,shopMoney,shopCount,shopName,
            shopType,shopId,shopDes,shopRate,shopCallbackUrl);
        }

        public override void GotoGameCenter()
        {
            ShowAccountCenter();
        }
        //登录成功，服务端登录验证成功后
        public override void LoginGameLog()
        {
            LogLoginGame(PlatformSdkMgr.Instance.Uid);
        }
        //选服日志
        public override void SelectServerLog(string roleLevel, string userName, string serverId)
        {
            LogForSelectServer(PlatformSdkMgr.Instance.Uid, serverId, userName, roleLevel);
        }

        //进入游戏（选完服，角色正式进入游戏时）
        public override void EnterGameLog(string serverId, string roleName = "0", string roleId = "", string roleLevel = "", string serverName = "")
        {
            LogAfterEnterGame(serverId, roleName, roleId, roleLevel, serverName);
        }

        //创建角色
        public override void CreateRoleLog(string roleId, string roleName, string roleLevel, string vipLevel, string serverId, string serverName, string diamond, string guildName,string createTime)
        {
            LogForCreateCharacter(roleId, serverId, roleName);
        }

        //角色升级
        public override void RoleLevelLog(int roleLevel, int serverId)
        {
            LogForCharacterLevelUp(PlatformSdkMgr.Instance.Uid, serverId.ToString(), roleLevel.ToString());
        }

        public override void ShowSDKLog(string functionName, string roleId, string roleName, string roleLevel, string vipLevel, string serverId, string serverName, string diamond, string guildName, string createTime)
        {
            LogSDKAllLog(functionName, UserID, roleId, roleName, roleLevel, vipLevel, serverId, serverName, diamond, guildName, createTime);
        }

        public override void OpenForum()
        {
            
        }

        public override String GetUserId()
        {
            return UserID;
        }

        public override String GetChannelId()
        {
            return GetChannelIdFromXcode();
        }

        public override String GetGameId()
        {
            return GetGameIdFromXcode();
        }

        public override void ShareLink(string title, string desc, string link)
        {
            string iconName = "";
            ShareLink(title, desc, link, iconName);
        }

        public override void ShareImage(string url)
        {
            ShareViaWeiXin(url);
        }

        public override void onBuyItemLog(string functionName, string roleId, string roleName,
            string roleLevel, string vipLevel, string serverId, string serverName,
            string diamond, string guildName, string createTime, string itemPrice, string itemAction, string itemCount,
            string itemName, string itemDes, bool isPayFromCharge, bool isGain)
        {
            BuyItemLog(UserID, roleId, roleName, roleLevel,
                vipLevel, serverId, serverName, diamond, guildName, createTime, itemPrice, itemAction, itemCount,
                itemName, itemDes, isPayFromCharge.ToString(), isGain.ToString());
        }

        public override int GetBattery()
        {
            LoggerHelper.Info("[SDK] 获取电量");
            return 0;
        }

        public override void LoginOutSDK()
        {
            LoggerHelper.Info("[SDK] LoginOutSDK");
            LoginOutSDKToXCode();
        }

        public override int GetSDKInitState()
        {
            LoggerHelper.Info("[SDK] GetInitState");
            state = int.Parse(GetInitState());
            return state;
        }

        public override void SetScreenLight(int brightness)
        {
            
        }

        #region OC回调
        [AOT.MonoPInvokeCallback(typeof(VOID_NO_ARG))]
        static void OnWXShareSucess()
        {
            LoggerHelper.Info("on wx share sucess");
        }
        [AOT.MonoPInvokeCallback(typeof(VOID_10STR_ARGS))]
        static void InitLoginInformation(String gameId, String channelId, String appId, String sessionId, String userId, String a6, String a7, String a8, String a9, String a10)
        {
            if (userId == null || userId == "")
            {
                userId = "unknown";
            }
            LoginInfo.userName = userId;
            LoginInfo.platformAccount = userId;
            LoginInfo.platformUid = sessionId;
            LoginInfo.sign = appId;
            LoginInfo.token = channelId;
            LoginInfo.timestamp = gameId;
            LoginInfo.platformName = a6;
            LoginInfo.ip = a7;
            LoginInfo.port = a8;
            LoginInfo.platformId = a9;

            if (loginSuccessFunction != null)
            {
                LoggerHelper.Info("[IOSSDK] Init [loginSuccessFunction]");
                List<string> dataList = new List<string>();
                dataList.Add(userId);
                dataList.Add(sessionId);
                dataList.Add(gameId);
                dataList.Add(channelId);
                dataList.Add(appId);
                dataList.Add(a6);
                loginSuccessFunction(dataList);

                loginSuccessFunction = null;
            }
            else
            {
                LoggerHelper.Error("loginSuccessFunction Callback is null");
            }
        }

        [AOT.MonoPInvokeCallback(typeof(VOID_NO_ARG))]
        static void VoidNoArg()
        {
            OnSwitchAccountBySDK("aa");
        }

        //回传 二次验证的数据 回 SDK
        [AOT.MonoPInvokeCallback(typeof(VOID_2STR_ARGS))]
        static void LoginSDKReture(String resCode, String args)
        {
            if (setJsonToSDKFunction != null)
            {
                setJsonToSDKFunction(resCode);
            }
        }

        [AOT.MonoPInvokeCallback(typeof(VOID_INT_ARG))]
        static void VoidInitArg(int gameId)
        {

        }

        #endregion

        #region OC调用
        [DllImport("__Internal")]
        public extern static void InitAccountCenter();
        [DllImport("__Internal")]
        public extern static bool CanPay();
        [DllImport("__Internal")]
        public extern static void InitIAPModule();
        [DllImport("__Internal")]
        public extern static void RegisterIAPCallbacks(VOID_NO_ARG a, VOID_NO_ARG b, VOID_2STR_ARGS c, VOID_INT_ARG d);
        [DllImport("__Internal")]
        public extern static void OnLoginFirst();
        [DllImport("__Internal")]
        public extern static void OnLoginSecond(String jsonData);
        [DllImport("__Internal")]
        public extern static void MakePaymentWithParams(string orderId, 
                                                        string roleId, 
                                                        string roleName, 
                                                        string roleLevel, 
                                                        string vipLevel,
                                                        string serverId, 
                                                        string serverName, 
                                                        string shopMoney, 
                                                        string shopCount, 
                                                        string shopName,
                                                        string shopType, 
                                                        string shopId, 
                                                        string shopDes, 
                                                        string shopRate, 
                                                        string shopCallbackUrl);
        [DllImport("__Internal")]
        public extern static void ShowAccountCenter();
        [DllImport("__Internal")]
        public extern static void RegisterAccountCenterCallbacks(VOID_10STR_ARGS a);

        //[DllImport("__Internal")]
        //extern static void InitWeiXin(VOID_NO_ARG a);
        [DllImport("__Internal")]
	    public extern static void ShareViaWeiXin(String imgPath);
        [DllImport("__Internal")]
        public extern static void ShareTextToTimeLineViaWeiXin(String text,String url);
        [DllImport("__Internal")]
        public extern static void ShareLink(string thetitle,string describe,string url, string iconName);

        [DllImport("__Internal")]
        public extern static void LogForSelectServer(String uid, String serverID, String nickName, String roleLevel);
        [DllImport("__Internal")]
        public extern static void LogForCreateCharacter(String uid, String serverID, String nickName);
        [DllImport("__Internal")]
        public extern static void LogForCharacterLevelUp(String uid, String serverID, String level);
        [DllImport("__Internal")]
        public extern static void LogAfterEnterGame(string serverId, string roleName, string roleId, string roleLevel, string serverName);
        [DllImport("__Internal")]
        public extern static void LogLoginGame(String uid);
        [DllImport("__Internal")]
        public extern static void LogSDKAllLog(string functionName,string useid, string roleId, string roleName, string roleLevel, string vipLevel, string serverId, string serverName, string diamond, string guildName, string createTime);
        [DllImport("__Internal")]
        public extern static string GetChannelIdFromXcode();
        [DllImport("__Internal")]
        public extern static string GetGameIdFromXcode();
        [DllImport("__Internal")]
        public extern static string GetInitState();
        [DllImport("__Internal")]
        public extern static string LoginOutSDKToXCode();
        [DllImport("__Internal")]
        public extern static void BuyItemLog(string userId,
                                            string roleId, 
                                            string roleName,
                                            string roleLevel, 
                                            string vipLevel, 
                                            string serverId, 
                                            string serverName,
                                            string diamond, 
                                            string guildName, 
                                            string createTime, 
                                            string itemPrice, 
                                            string itemAction, 
                                            string itemCount,
                                            string itemName, 
                                            string itemDes,
                                            string isPayFromCharge,
                                            string isGain);
        #endregion
//#endif
    }
}
 