﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security;
using GameLoader.Config;
using GameLoader.Defines;
using GameLoader.IO;
using GameLoader.LoadingBar;
using GameLoader.Utils;
using GameLoader.Version;
using UnityEngine;

namespace GameLoader.Mgrs
{
    /// <summary>
    /// pkg更新流程管理类
    /// 1、先加载packagemd5.xml文件
    /// 2、再加载需要更新的pkg文件
    /// 3、解压pkg文件，合并文件到MogoFileSystem
    /// 4、结束
    /// </summary>
    class UpdatePkgMgr
    {
        private int beginTime;
        private int step = 0;
        //private int curTryNum = 0;                         //当前加载文件重试次数
        //private int maxTryNum = 10;                        //单个文件加载最大重试次数,默认3次
        private string hasUpdateVer;                       //已更新的版本

        private List<PkgInfo> loadList;                    //加载列表
        private VersionInfo localVersion;                  //本地版本信息
        private Action<bool> pkgCallback;                  //更新结束回调
        private string firstPkgTempFilePath;               //有补丁包时，先拷贝pkg副本合并完成补丁资源，然后在切换回来
        private BreakPointDownload breakPointDownload;     //断点续传下载类
        private Action<Action, long> checkNetwork;
        private long totalFileSize;
        //private const string check_md5_fail = "{0}文件检验失败";

        public UpdatePkgMgr(VersionInfo localVersion, Action<Action, long> checkNetwork)
        {
            loadList = new List<PkgInfo>();
            this.localVersion = localVersion;
            this.checkNetwork = checkNetwork;
            this.firstPkgTempFilePath = string.Concat(localVersion.firstPkgOutputPath, "_temp_file");
            breakPointDownload = new BreakPointDownload();
        }

        /// <summary>
        /// 更新pkg
        /// </summary>
        /// <param name="callback">更新结束回调callback(true:更新成功,false:更新失败)</param>
        public void Update(Action<bool> callback)
        {
            VersionCodeInfo curVersion = localVersion.ResourceVersion;
            if (new VersionCodeInfo(SystemConfig.ResourceVersion).Compare(curVersion) == 1)
            {
                //curTryNum = 0;
                this.pkgCallback = callback;
                //ProgressBar.Instance.UpdateTip(string.Format("正在下载:{0}", Path.GetFileName(SystemConfig.PackageMd5Url)));
                DownloadPackageMd5();
            }
            else
            {
                LoggerHelper.Info(string.Format("[自更新] 本地版本:{0}>=服务器版本:{1}，没有可更新pkg", curVersion, SystemConfig.ResourceVersion));
                if (callback != null) callback(true);
            }

        }

        //加载packagemd5.xml文件(有3次重试机会)
        private void DownloadPackageMd5()
        {
            //if (curTryNum++ <= maxTryNum)
            //{
            beginTime = Environment.TickCount;
            //ResLoader.LoadWwwText(SystemConfig.GetRandomUrl(SystemConfig.PackageMd5Url), OnPackageMd5Loaded);
            TextDownloadMgr.AsyncDownloadString(TextDownloadMgr.GetRandomParasUrl(SystemConfig.PackageMd5Url), OnPackageMd5Loaded);
            //}
            //else
            //{
            //    LoggerHelper.Error(string.Format("[自更新] 加载文件:{0}[Fail],已超过{1}/{2}次重试，将退出", SystemConfig.PackageMd5Url, curTryNum, maxTryNum));
            //    //ProgressBar.Instance.UpdateTip(tip);
            //    ConfirmBox.Instance.Show(error_md5_file, onExit, DefaultLanguage.GetString("exit"), null, false);
            //    if (pkgCallback != null) pkgCallback(false);
            //    pkgCallback = null;
            //}
        }

        private void onExit(bool result)
        {
            Application.Quit();
        }

        /// <summary>
        /// packageMd5.xml下载成功,搜集需要下载的pkg文件列表
        /// 文件内容结构：
        /// <pl>
        ///    <p n="package0.0.0.1-0.0.0.2.pkg">6fd3555964fa0aa7f120f2caa79665cd</p> 
        ///    <p n="package0.0.0.2-0.0.0.3.pkg">427249a5819530952627c997c2fd9cdd</p> 
        ///    ...
        /// </pl>
        /// </summary>
        /// <param name="content"></param>
        private void OnPackageMd5Loaded(string content)
        {
            if (!string.IsNullOrEmpty(content))
            {//下载成功
                LoggerHelper.Info(string.Format("[自更新] 加载：{0}[OK],cost:{1}ms", SystemConfig.PackageMd5Url, Environment.TickCount - beginTime));
                //ProgressBar.Instance.UpdateTip("加载packagemd5.xml[OK]");
                SecurityElement rootNode = content.ToXMLSecurityElement();
                loadList.Clear();
                VersionCodeInfo lowVersion;
                VersionCodeInfo hightVersion;
                string fileName = null;
                long fileSize;
                long fileCount;
                totalFileSize = 0;
                //int curVersion = localVersion.intVersion(localVersion.ResourceVersion);
                VersionCodeInfo curVersion = localVersion.ResourceVersion;
                LoggerHelper.Info(string.Format("[自更新] 开始查找:{0}-{1}范围的版本", curVersion, SystemConfig.ResourceVersion));
                foreach (SecurityElement item in rootNode.Children)
                {
                    fileName = item.Attribute("n");
                    string temp = fileName.Replace("package", "").Replace(".pkg", "");
                    string[] array = temp.Split('-');
                    lowVersion = new VersionCodeInfo(array[0]);
                    hightVersion = new VersionCodeInfo(array[1]);

                    int lowCompareCur = lowVersion.Compare(curVersion);
                    int heightCompareRes = new VersionCodeInfo(SystemConfig.ResourceVersion).Compare(hightVersion);
                    //取出curVersion 到 SystemConfig.ResourceVersion 之间范围的版本
                    if (lowCompareCur >= 0 && heightCompareRes >= 0)
                    {
                        long.TryParse(item.Attribute("s"), out fileSize);
                        long.TryParse(item.Attribute("c"), out fileCount);
                        loadList.Add(new PkgInfo(lowVersion, hightVersion, item.Text, fileName, array[1], fileSize, fileCount));
                        totalFileSize += fileSize;
                        //LoggerHelper.Info(string.Format("[自更新] pkg文件:{0},md5:{1}", fileName, item.Text));
                    }

                    //if (lowVersion >= curVersion && hightVersion <= SystemConfig.ResourceVersion)
                    //{
                    //    loadList.Add(new PkgInfo(lowVersion, hightVersion, item.Text, fileName, array[1]));
                    //    //LoggerHelper.Info(string.Format("[自更新] pkg文件:{0},md5:{1}", fileName, item.Text));
                    //}
                }
                LoggerHelper.Info(string.Format("[自更新] 需下载pkg文件数：{0}", loadList.Count));
                if (loadList.Count > 0)
                {//有补丁包，先拷贝pkg文件副本，然后合并补丁包资源到pkg，合并完成把原pkg删掉,副本换成最新pkg文件
                    File.Copy(localVersion.firstPkgOutputPath, firstPkgTempFilePath, true);
                    MogoFileSystem.Instance.FileFullName = firstPkgTempFilePath;
                }
                step = -1;
                checkNetwork(DownloadNext, totalFileSize);
                //DownloadNext();
            }
            else
            {//下载失败,重试
                LoggerHelper.Info(string.Format("[自更新] 加载文件:{0}[Fail],开始重试", SystemConfig.PackageMd5Url));
                DownloadPackageMd5();
            }
        }

        //===============  按队列加载pkg包  ============//
        //加载下一个pkg文件
        private void DownloadNext()
        {
            step++;
            //curTryNum = 0;
            Download();
        }

        //加载当前step的资源
        private void Download()
        {
            if (step < loadList.Count)
            {
                PkgInfo pkgInfo = loadList[step];
                //if (curTryNum++ <= maxTryNum)
                //{//未超过最大重试次数
                beginTime = Environment.TickCount;
                string httpUrl = pkgInfo.httpUrl;
                if (SystemConfig.IsDownloadFromSource)
                    httpUrl = TextDownloadMgr.GetRandomParasUrl(httpUrl);
                LoggerHelper.Info(string.Format("[自更新] 开始加载第{0}/{1}个文件:{2} [Start]", step + 1, loadList.Count, httpUrl));
                if (!Directory.Exists(Path.GetDirectoryName(pkgInfo.localSavePath))) Directory.CreateDirectory(Path.GetDirectoryName(pkgInfo.localSavePath));
                if (DownloadProgress.instance != null) DownloadProgress.instance.ShowDownload(pkgInfo.fileName, 0, step + 1, loadList.Count, pkgInfo.fileSize, totalFileSize);
                HttpWrappr.instance.LoadWwwFile(httpUrl, pkgInfo.localSavePath, OnDownloadProgress, OnLoaded, OnLoadedFail);
                //breakPointDownload.Dispose();
                //breakPointDownload.Download(httpUrl, pkgInfo.localSavePath, OnApkUpdateProgress, OnLoaded, OnLoadedFail);
                //ProgressBar.Instance.UpdateTip(string.Format("正在下载{0}补丁文件", pkgInfo.strHightVersion));
                //}
                //else
                //{
                //    LoggerHelper.Error(string.Format("[自更新] 文件:{0}已超过{1}/{2}重试次数 [Fail]", pkgInfo.httpUrl, curTryNum, maxTryNum));
                //    string tip = string.Format("{0}补丁文件{1}", pkgInfo.strHightVersion, "下载失败");   //pkgInfo.fileName
                //    //ProgressBar.Instance.UpdateTip(tip);
                //    ConfirmBox.Instance.Show(tip, onExit, DefaultLanguage.GetString("exit"), null, false);
                //    if (pkgCallback != null) pkgCallback(false);
                //    pkgCallback = null;
                //}
            }
            else
            { //列表加载完成
                LoggerHelper.Info(string.Format("[自更新] 正在切换到新pkg文件,hasUpdateVer:{0}", hasUpdateVer));
                MogoFileSystem.Instance.FileFullName = localVersion.firstPkgOutputPath;
                if (File.Exists(firstPkgTempFilePath))
                {//补丁更新完成
#if !UNITY_WEBPLAYER
                    if (File.Exists(localVersion.firstPkgOutputPath)) File.Delete(localVersion.firstPkgOutputPath);
                    File.Move(firstPkgTempFilePath, localVersion.firstPkgOutputPath);
#endif
                    if (!string.IsNullOrEmpty(hasUpdateVer))
                    {
                        localVersion.ResourceVersion = new VersionCodeInfo(hasUpdateVer);
                        localVersion.Save();
                        LoggerHelper.Info(string.Format("[自更新] pkg补丁全部更新成功,更新后版本:{0}", localVersion.ResourceVersion));
                    }
                }

                LoggerHelper.Info(string.Format("[自更新] 全部加载完成pkg文件数:{0}", loadList.Count));
                if (DownloadProgress.instance != null) DownloadProgress.instance.Close();
                //ProgressBar.Instance.UpdateTip("全部pkg更新完成");
                if (pkgCallback != null) pkgCallback(true);
                pkgCallback = null;
            }
        }

        private void OnDownloadProgress(float progress, long bytesReceived, long totalBytesToReceive)
        {
            //DownloadProgress.instance.UpdateProgress(progress);
            //LoggerHelper.Error("bytesReceived: " + bytesReceived + " totalBytesToReceive: " + totalBytesToReceive);
            LoaderDriver.Invoke(DownloadProgress.instance.UpdateProgress, progress);
        }

        //private void OnDecompressProgress(long currentCount)
        //{
        //    LoaderDriver.Invoke(DownloadProgress.instance.UpdateProgress, (float)currentCount / loadList[step].fileCount);
        //}

        private void OnDecompressComplate(PkgInfo pkgInfo)
        {
            try
            {
                if (File.Exists(pkgInfo.localSavePath)) File.Delete(pkgInfo.localSavePath);                       //删除临时文件
                hasUpdateVer = pkgInfo.strHightVersion;
                LoggerHelper.Info(string.Format("[自更新] 文件:{0}加载并解压[OK]", pkgInfo.localSavePath));
                DownloadNext();
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(string.Format("文件:{0},本地路径:{1}解压[Fail],reason:{2}", pkgInfo.httpUrl, pkgInfo.localSavePath, ex.StackTrace));
                string tip = string.Format(DefaultLanguage.GetString("error_unzip_file"), pkgInfo.strHightVersion);    //pkgInfo.fileName
                ConfirmBox.Instance.Show(tip, onExit, DefaultLanguage.GetString("exit"), null, false);
                if (pkgCallback != null) pkgCallback(false);
                pkgCallback = null;
            }
        }

        //加载完成
        private void OnLoaded(string url)
        {
            LoggerHelper.Info(string.Format("[自更新] 文件:{0}加载[OK],cost:{1}ms", url, Environment.TickCount - beginTime));
            breakPointDownload.Dispose();
            PkgInfo pkgInfo = loadList[step];
            if (checkFileMd5(pkgInfo))
            {
                if (DownloadProgress.instance != null) DownloadProgress.instance.ShowDecompress(pkgInfo.fileName, 100, step + 1, loadList.Count);
                Action action = () =>
                {
                    try
                    {
                        FileAccessManager.DecompressFile(pkgInfo.localSavePath/*, OnDecompressProgress*/);                                          //解压加载文件
                        LoaderDriver.Invoke(OnDecompressComplate, pkgInfo);
                    }
                    catch (Exception ex)
                    {
                        LoggerHelper.Error(string.Format("文件:{0},本地路径:{1}解压[Fail],reason:{2}", pkgInfo.httpUrl, pkgInfo.localSavePath, ex.StackTrace));
                        string tip = string.Format(DefaultLanguage.GetString("error_unzip_file"), pkgInfo.strHightVersion);    //pkgInfo.fileName
                        ConfirmBox.Instance.Show(tip, onExit, DefaultLanguage.GetString("exit"), null, false);
                        if (pkgCallback != null) pkgCallback(false);
                        pkgCallback = null;
                    }
                };
                action.BeginInvoke(null, null);
            }
            else
            {//md5校验失败,加载地址加上随机数重新加载
                LoggerHelper.Info(string.Format("[自更新] 文件:{0}md5校验[Fail]", pkgInfo.localSavePath));
                if (File.Exists(pkgInfo.localSavePath)) File.Delete(pkgInfo.localSavePath);
                Download();
            }
        }

        //加载失败
        private void OnLoadedFail(string url, string errorInfo)
        {
            DownloadProgress.instance.Close();
            breakPointDownload.HandlerError(errorInfo, Download);
            //Download(); //重试加载
        }

        //对文件进行md5验证
        private bool checkFileMd5(PkgInfo pkgInfo)
        {
            string md5Compute = MD5Utils.BuildFileMd5(pkgInfo.localSavePath).Trim();
            string srcMd5 = pkgInfo.md5.Trim().ToLower();
            if (md5Compute != srcMd5)
            { //md5验证失败，删除原文件
                if (File.Exists(pkgInfo.localSavePath)) File.Delete(pkgInfo.localSavePath);
                LoggerHelper.Error(string.Format("文件:{0}  srcMd5:{1},curMd5:{2}验证失败", pkgInfo.httpUrl, srcMd5, md5Compute));
                //ProgressBar.Instance.UpdateTip(string.Format("文件:{0}校验失败 {1}!={2}", pkgInfo.fileName, srcMd5, md5Compute));
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
