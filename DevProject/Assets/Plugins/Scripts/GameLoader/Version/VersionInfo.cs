using System;
using System.IO;
using System.Security;
using System.Text;
using GameLoader.Config;
using GameLoader.IO;
using GameLoader.Utils;
using UnityEngine;

namespace GameLoader.Defines
{
    /// <summary>
    /// 热更版本信息
    /// </summary>
    class VersionInfo
    {
        public VersionCodeInfo ProgramVersion;               //Apk版本,默认从1开始
        public VersionCodeInfo ResourceVersion;              //资源版本,默认从0开始
        public VersionCodeInfo GameLoaderVersion;            //GameLoader.dll版本
        public VersionCodeInfo ProgressBarBgVersion;         //进度条背景版本号

        private StringBuilder tempArgs;             //临时变量
        private string verOutputPath;               //VersionInfo.xm外部资源目录
        private string firstPkgStreamAssetsPath;    //pkgStreamAssets路径

        public VersionInfo()
        {
            ProgramVersion = new VersionCodeInfo("0.0.0.1");
            ResourceVersion = new VersionCodeInfo("0.0.0.0");
            GameLoaderVersion = new VersionCodeInfo("0.0.0.0");
            ProgressBarBgVersion = new VersionCodeInfo("0.0.0.0");

            tempArgs = new StringBuilder();
            verOutputPath = string.Concat(Application.persistentDataPath, ConstString.RutimeResourceConfig, "/VersionInfo.xml");
            firstPkgStreamAssetsPath = IOUtils.GetStreamPath("pkg");
            if (UnityPropUtils.Platform == RuntimePlatform.WindowsPlayer)
            {//pc
                firstPkgOutputPath = string.Concat(Application.streamingAssetsPath, "/pkg"); //firstPkgStreamAssetsPath
            }
            //else if (UnityPropUtils.Platform == RuntimePlatform.WindowsWebPlayer)//为排除警告
            //{
            //    firstPkgOutputPath = "";
            //}
            else
            {//android|ios端
                firstPkgOutputPath = string.Concat(Application.persistentDataPath, ConstString.RutimeResource, "/pkg");
            }
            //不能从StreamingAssets目录读取FirstPkg,只能从外部资源目录读取FirstPkg文件，因为FirstPkg文件内容会变动
            MogoFileSystem.Instance.FileFullName = firstPkgOutputPath;
        }

        public string firstPkgOutputPath { get; private set; }

        /// <summary>
        /// 加载本地VersionInfo.xml配置文件
        /// </summary>
        /// <param name="callback">结束回调</param>
        public void LoadLocalFile(Action callback)
        {
            LoggerHelper.Info(string.Format("firstPkgOutputPath:{0},FileFullName:{1}", firstPkgOutputPath, MogoFileSystem.Instance.FileFullName));
            LoggerHelper.Info(string.Format("VersionInfo Path:{0}", verOutputPath));
            bool isExists = File.Exists(verOutputPath);
            if (isExists)
            {//外部目录存在VersinoInfo.xml
                LoggerHelper.Info(string.Format("加载本地:{0} [Start]", verOutputPath));
                var sdcontent = FileUtils.LoadFile(verOutputPath);
                ReadFromXml(sdcontent);
                //这里需要注意一个细节：每次都要从SD卡读取‘版本号’，然后跟‘本地版本号’对比，因为安装apk后‘SD卡中版本号’有可能比‘本地版本号’高
                LoggerHelper.Info(string.Format("url:{0}   content:{1}", verOutputPath, sdcontent));
                LoggerHelper.Info(string.Format("加载本地:{0} [OK]", verOutputPath));
                var content = ResLoader.LoadStringFromResource("VersionInfo");
                LoggerHelper.Info(string.Format("url:Resources.Load(VersionInfo)   content:{0}", content));
                var privateVersionInfo = content.ToXMLSecurityElement();
                //SecurityElement apkNode = rootNode.SearchForChildByTag("ProgramVersion");
                var gameLoaderNode = privateVersionInfo.SearchForChildByTag("GameLoaderVersion");
                if (gameLoaderNode != null) DeleteGameLoaderFile(gameLoaderNode.Text);
                var progNode = privateVersionInfo.SearchForChildByTag("ProgramVersion");
                var resNode = privateVersionInfo.SearchForChildByTag("ResourceVersion");
                if ((SystemConfig.IsDelByProgramVersion && SystemConfig.IsUpdateBigApk && progNode != null && CompareProgramVersion(new VersionCodeInfo(progNode.Text)))
                    //打开根据程序版本删除资源并且是整包更新开关打开的情况下，sd卡的程序版本高于外部目录的程序版本
                    || (resNode != null && CompareResourceVersion(new VersionCodeInfo(resNode.Text))))//sd卡的资源版本高于外部目录的资源版本
                {
                    DeleteReousrcesDirAndFirstPkgFile();
                }

                //方案一：检查到外部目录没有pkg，则从sd卡拷贝，PC端和web端直接StreamingAssets目录读取pkg
                if (!File.Exists(firstPkgOutputPath))
                {
                    LoggerHelper.Info(string.Format("[自更新] apk没版本升级--从SD卡:{0}拷贝FirstPkg到:{1}[Start]", firstPkgStreamAssetsPath, firstPkgOutputPath));
                    CopyFirstPkg(callback, privateVersionInfo);
                }
                else
                {
                    byte[] data1 = FileUtils.LoadByteFile(firstPkgOutputPath);
                    if (data1 != null && data1.Length > 0)
                    {
                        ParserXML(privateVersionInfo);
                        LoggerHelper.Info(string.Format("[自更新] apk没版本升级--本地ProgramVersion:{0},ResourceVersion:{1},GameLoaderVersion:{2}", ProgramVersion, ResourceVersion, GameLoaderVersion));
                        if (callback != null) callback();
                    }
                    else
                    {//文件存在，但是长度为0，删除并重新拷贝
                        LoggerHelper.Info(string.Format("firstPkgOutputPath:{0}, file content is null", firstPkgOutputPath));
                        File.Delete(firstPkgOutputPath);
                        CopyFirstPkg(callback, privateVersionInfo);
                    }
                }
            }
            else
            {//从SD卡读取
                //PC端和web端直接StreamingAssets目录读取pkg
                if (!File.Exists(firstPkgOutputPath))
                {//外部目录没有FirstPkg文件，则从sd卡拷贝过去                 
                    LoggerHelper.Info(string.Format("[自更新] 首次--从SD卡:{0}拷贝FirstPkg到:{1}[Start]", firstPkgStreamAssetsPath, firstPkgOutputPath));
                    CopyFirstPkg(callback, null);
                }
                else
                {
                    ReadFromXml(ResLoader.LoadStringFromResource("VersionInfo"), true);
                    LoggerHelper.Info(string.Format("[自更新] ProgramVersion:{0},ResourceVersion:{1},GameLoaderVersion:{2}", ProgramVersion, ResourceVersion, GameLoaderVersion));
                    if (callback != null) callback();
                }
            }
        }

        /// <summary>
        /// 整包apk安装完成后，必须检查GameLoader版本，删除本地旧版本GameLoader，删除后会使用包里的文件
        /// </summary>
        /// <param name="gameLoaderVersion"></param>
        private void DeleteGameLoaderFile(string gameLoaderVersion)
        {
#if !UNITY_WEBPLAYER
            if (CompareGameLoaderVersion(new VersionCodeInfo(gameLoaderVersion)))                                                //sd卡的gameLoader版本高于外部目录的gameLoader版本
            {
                var gameLoaderPath = SystemConfig.ResourcesPath + LoaderDriver.Instance.GameLoaderName;
                if (File.Exists(gameLoaderPath)) File.Delete(gameLoaderPath);                                   //删除外部目录的gameLoader文件
                LoggerHelper.Info("DeleteGameLoaderFile [OK]");
            }
#endif
        }

        /// <summary>
        /// 整包apk安装完成后，必须检查资源版本，删除本地旧版本的pkg和RuntimeResources/Resources目录
        /// </summary>
        private void DeleteReousrcesDirAndFirstPkgFile()
        {
#if !UNITY_WEBPLAYER
            if (File.Exists(firstPkgOutputPath)) File.Delete(firstPkgOutputPath);                                   //删除外部目录的pkg文件
            var gameLoaderPath = SystemConfig.ResourcesPath + LoaderDriver.Instance.GameLoaderName;
            var tempGameLoaderPath = "";
            if (File.Exists(gameLoaderPath))//删除资源的时候不删除GameLoader，先备份GameLoader文件
            {
                tempGameLoaderPath = SystemConfig.ResourcesPath + "../" + LoaderDriver.Instance.GameLoaderName;
                File.Copy(gameLoaderPath, tempGameLoaderPath, true);
            }
            if (Directory.Exists(SystemConfig.ResourcesPath)) Directory.Delete(SystemConfig.ResourcesPath, true);   //删除Resources下所有子目录和文件
            Directory.CreateDirectory(SystemConfig.ResourcesPath);
            if (tempGameLoaderPath != "")//恢复备份的GameLoader文件
            {
                File.Copy(tempGameLoaderPath, gameLoaderPath, true);
                File.Delete(tempGameLoaderPath);
            }
            LoggerHelper.Info("DeleteReousrcesDirAndFirstPkgFile [OK]");
#endif
        }

        /// <summary>
        /// 拷贝SD卡的FirstPkg文件到外部目录
        /// </summary>
        /// <param name="callback">结束回调</param>
        /// <param name="overwrite">VersionInfo.xml文件内容</param>
        public void CopyFirstPkg(Action callback, SecurityElement rootNode)
        {
#if !UNITY_WEBPLAYER
            CopyFirstPkgFromStream(callback, rootNode);
#else
            DownloadFirstPkgFromRemote(callback, rootNode);
#endif
        }

        /// <summary>
        /// 拷贝SD卡的FirstPkg文件到外部目录
        /// </summary>
        /// <param name="callback">结束回调</param>
        /// <param name="overwrite">VersionInfo.xml文件内容</param>
        private void CopyFirstPkgFromStream(Action callback, SecurityElement rootNode)
        {
            int beginTime = Environment.TickCount;
            //if (CompareProgramVersion(new VersionCodeInfo("0.0.0.298")))
            //{
            ResLoader.LoadWwwAndUnzip(firstPkgStreamAssetsPath, firstPkgOutputPath,
            (bool result) =>
            {
                LoggerHelper.Info(string.Format("加载FirstPkg:{0} {1},cost:{2}ms", firstPkgOutputPath, result ? "成功" : "失败或找不到文件", Environment.TickCount - beginTime));
                    //if (rootNode != null)
                    //    ParserXML(rootNode);
                    //else
                    ReadFromXml(ResLoader.LoadStringFromResource("VersionInfo"), true);//每次拷贝pkg都应该重置版本号，以保证pkg最终的内容是对的
                    LoggerHelper.Info(string.Format("[自更新1] 本地ProgramVersion:{0},ResourceVersion:{1},GameLoaderVersion:{2}", ProgramVersion, ResourceVersion, GameLoaderVersion));
                if (callback != null) callback();
            });
            //}
            //else
            //{
            //    ResLoader.LoadWwwBytes(firstPkgStreamAssetsPath, firstPkgOutputPath,
            //        (bool result) =>
            //        {
            //            LoggerHelper.Info(string.Format("加载FirstPkg:{0} {1},cost:{2}ms", firstPkgOutputPath, result ? "成功" : "失败或找不到文件", Environment.TickCount - beginTime));
            //            //if (rootNode != null)
            //            //    ParserXML(rootNode);
            //            //else
            //                ReadFromXml(ResLoader.LoadStringFromResource("VersionInfo"), true);//每次拷贝pkg都应该重置版本号，以保证pkg最终的内容是对的
            //            LoggerHelper.Info(string.Format("[自更新1] 本地ProgramVersion:{0},ResourceVersion:{1},GameLoaderVersion:{2}", ProgramVersion, ResourceVersion, GameLoaderVersion));
            //            if (callback != null) callback();
            //        });
            //}
        }


        /// <summary>
        /// 下载FirstPkg文件到内存
        /// </summary>
        /// <param name="callback">结束回调</param>
        /// <param name="overwrite">VersionInfo.xml文件内容</param>
        private void DownloadFirstPkgFromRemote(Action callback, SecurityElement rootNode)
        {
            int beginTime = Environment.TickCount;
            string pkgurl = Path.Combine(SystemConfig.RemoteResourcesUrl, "Bytes$pkg.bytes.u");
            pkgurl = string.Format("{0}?date={1}", pkgurl, DateTime.Now.ToString("yyyyMMddHHmmssffffff"));
            //Debug.LogError("path: " + pkgurl);
            ResLoader.LoadWwwBytes(pkgurl,
                (string url, byte[] data) =>
                {
                    MogoFileSystem.Instance.Init(data);
                    if (rootNode != null) ParserXML(rootNode);
                    else ReadFromXml(ResLoader.LoadStringFromResource("VersionInfo"), true);
                    LoggerHelper.Info(string.Format("[自更新2] 本地ProgramVersion:{0},ResourceVersion:{1},GameLoaderVersion:{2}", ProgramVersion, ResourceVersion, GameLoaderVersion));
                    if (callback != null) callback();
                });

        }

        private void onExit(bool result)
        {
            Application.Quit();
        }

        private void ReadFromXml(string xml, bool isStrongSave = false)
        {
            ParserXML(xml.ToXMLSecurityElement(), isStrongSave);
        }

        //isStrongSave=true:强制保存
        private void ParserXML(SecurityElement rootNode, bool isStrongSave = false)
        {
            SecurityElement apkNode = rootNode.SearchForChildByTag("ProgramVersion");
            SecurityElement resNode = rootNode.SearchForChildByTag("ResourceVersion");
            SecurityElement glNode = rootNode.SearchForChildByTag("GameLoaderVersion");
            SecurityElement pbNode = rootNode.SearchForChildByTag("ProgressBarBgVersion");

            VersionCodeInfo apkVer = apkNode != null ? new VersionCodeInfo(apkNode.Text) : new VersionCodeInfo("0.0.0.1"); //apk版本
            VersionCodeInfo resVer = apkNode != null ? new VersionCodeInfo(resNode.Text) : new VersionCodeInfo("0.0.0.0"); //资源版本
            VersionCodeInfo glVer = apkNode != null ? new VersionCodeInfo(glNode.Text) : new VersionCodeInfo("0.0.0.0"); //GameLoader版本
            VersionCodeInfo pbVer = apkNode != null ? new VersionCodeInfo(pbNode.Text) : new VersionCodeInfo("0.0.0.0"); //背景图版本号


            bool apkResult = CompareProgramVersion(apkVer);
            bool resResult = CompareResourceVersion(resVer);
            bool glResult = CompareGameLoaderVersion(glVer);
            bool pbResult = CompareProgressBarBgVersion(pbVer);

            if (apkResult) ProgramVersion = apkVer;
            if (resResult) ResourceVersion = resVer;
            if (glResult) GameLoaderVersion = glVer;
            if (pbResult) ProgressBarBgVersion = pbVer;

            if (isStrongSave || apkResult || resResult || glResult || pbResult) Save();  //强制或有变动，则保存到本地
        }

        /// <summary>
        /// 当前程序版本与目标程序版本比较
        /// </summary>
        /// <param name="targetProgramVersion">目标版本</param>
        /// <returns>[true:目标版本高于当前版本,false:目标版本低于或等于当前版本]</returns>
        public bool CompareProgramVersion(VersionCodeInfo targetProgramVersion)
        {
            return targetProgramVersion.Compare(ProgramVersion) == 1;
        }

        /// <summary>
        /// 当前资源版本与目标资源版本比较
        /// </summary>
        /// <param name="targetProgramVersion">目标版本</param>
        /// <returns>[true:目标版本高于当前版本,false:目标版本低于或等于当前版本]</returns>
        public bool CompareResourceVersion(VersionCodeInfo targetResourceVersion)
        {
            return targetResourceVersion.Compare(ResourceVersion) == 1;
        }

        /// <summary>
        /// 当前资源版本与目标资源版本比较
        /// </summary>
        /// <param name="targetProgramVersion">目标版本</param>
        /// <returns>[true:目标版本高于当前版本,false:目标版本低于或等于当前版本]</returns>
        private bool CompareGameLoaderVersion(VersionCodeInfo targetGameLoaderVersion)
        {
            return targetGameLoaderVersion.Compare(GameLoaderVersion) == 1;
        }

        /// <summary>
        /// 当前资源版本与目标进度条背景图版本比较
        /// </summary>
        /// <param name="targetProgressBarBgVersion">目标版本</param>
        /// <returns>[true:目标版本高于当前版本,false:目标版本低于或等于当前版本]</returns>
        public bool CompareProgressBarBgVersion(VersionCodeInfo targetProgressBarBgVersion)
        {
            return targetProgressBarBgVersion.Compare(ProgressBarBgVersion) == 1;
        }

        /// <summary>
        /// 返回版本的整数
        /// </summary>
        /// <param name="version">版本字符串,格式[x.x.x.x],例如：0.0.0.1</param>
        /// <returns></returns>
        //public int intVersion(string version)
        //{
        //    return int.Parse(version.Replace(".", ""));
        //}

        /// <summary>
        /// 把内容写入本地文件
        /// </summary>
        public void Save()
        {
            if (tempArgs.Length > 0) tempArgs.Remove(0, tempArgs.Length);
            tempArgs.AppendLine("<config>");
            tempArgs.AppendLine("<ProgramVersion>" + ProgramVersion + "</ProgramVersion>");
            tempArgs.AppendLine("<ResourceVersion>" + ResourceVersion + "</ResourceVersion>");
            tempArgs.AppendLine("<GameLoaderVersion>" + GameLoaderVersion + "</GameLoaderVersion>");
            tempArgs.AppendLine("<ProgressBarBgVersion>" + ProgressBarBgVersion + "</ProgressBarBgVersion>");
            tempArgs.Append("</config>");
#if !UNITY_WEBPLAYER
            if (File.Exists(verOutputPath)) File.Delete(verOutputPath);
            FileUtils.WriterFile(verOutputPath, tempArgs.ToString());
#endif
        }
    }
}


