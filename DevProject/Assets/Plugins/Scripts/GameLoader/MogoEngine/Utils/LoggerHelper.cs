#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：LoggerHelper
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.1.17
// 模块描述：日志输出类。
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;


namespace GameLoader.Utils
{
    /// <summary>
    /// 日志等级声明。
    /// </summary>
    [Flags]
    public enum LogLevel
    {
        /// <summary>
        /// 缺省
        /// </summary>
        NONE = 0,
        /// <summary>
        /// 调试
        /// </summary>
        DEBUG = 1,
        /// <summary>
        /// 信息
        /// </summary>
        INFO = 2,
        /// <summary>
        /// 警告
        /// </summary>
        WARNING = 4,
        /// <summary>
        /// 错误
        /// </summary>
        ERROR = 8,
        /// <summary>
        /// 异常
        /// </summary>
        EXCEPT = 16,
        /// <summary>
        /// 关键错误
        /// </summary>
        CRITICAL = 32,
    }

    public static class LogLevelExtensions
    {
        public static bool HasValue(this LogLevel flag, LogLevel value)
        {
            return (flag & value) == value;
        }
    }

    /// <summary>
    /// 日志控制类。
    /// </summary>
    /// 
    public class LoggerHelper
    {
        /// <summary>
        /// 当前日志记录等级。
        /// </summary>
        public static LogLevel CurrentLogLevels = LogLevel.DEBUG | LogLevel.INFO | LogLevel.WARNING | LogLevel.ERROR | LogLevel.CRITICAL | LogLevel.EXCEPT;

        /// <summary>
        /// 记录是否对日志进行处理，主要系考虑日志对效率造成的影响，打印日志都需要对这个先进行判断。
        /// </summary>
        public static bool LISTEN_LOG = true;
        private const Boolean SHOW_STACK = true;
        private static LogWriter m_logWriter;
        public static string DebugFilterStr = string.Empty;
        public static List<string> lastLogs = new List<string>();
        public static bool saveLastLogs = false;
        public delegate void LogDelegate(string str);
        public static LogDelegate logDelegate;

        public static void SetDebugFilterStr(string value)
        {
            DebugFilterStr = value;
        }

        public static LogWriter LogWriter
        {
            get { return m_logWriter; }
        }

        static LoggerHelper()
        {
            m_logWriter = new LogWriter();
            Application.logMessageReceivedThreaded += ProcessExceptionReport;
        }

        public static void Release()
        {
            m_logWriter.Release();
        }

        public static void UploadLogFile()
        {
            m_logWriter.UploadTodayLog();
        }

        static ulong index = 0;


        /// <summary>
        /// 调试日志。
        /// </summary>
        /// <param name="message">日志内容</param>
        /// <param name="isShowStack">是否显示调用栈信息</param>
        /*
        public static void Debug(object message,  string user )
        {
            if (DebugFilterStr != "") return;

            if (LogLevel.DEBUG == (CurrentLogLevels & LogLevel.DEBUG))
                Log(string.Concat(user + " [DEBUG]: ", SHOW_STACK ? GetStackInfo() : "", message, " Index = ", index++), LogLevel.DEBUG);
            
        }*/

        /// <summary>
        /// 调试日志。
        /// </summary>
        /// <param name="message">日志内容</param>
        /// <param name="isShowStack">是否显示调用栈信息</param>
        public static void Debug(object message, Boolean isShowStack = SHOW_STACK, int user = 0)
        {
            if (DebugFilterStr != "") return;

            if (LogLevel.DEBUG == (CurrentLogLevels & LogLevel.DEBUG))
                Log(string.Concat(" [DEBUG]: ", isShowStack ? GetStackInfo() : "", message, " Index = ", index++), LogLevel.DEBUG);
        }

        /// <summary>
        /// 扩展debug
        /// </summary>
        /// <param name="message"></param>
        /// <param name="filter">只输出与在DebugMsg->filter里面设置的filter一样的debug</param>
        public static void Debug(string filter, object message, Boolean isShowStack = SHOW_STACK)
        {
            if (DebugFilterStr != "" && DebugFilterStr != filter) return;
            if (LogLevel.DEBUG == (CurrentLogLevels & LogLevel.DEBUG))
            {
                Log(string.Concat(" [DEBUG]: ", isShowStack ? GetStackInfo() : "", message), LogLevel.DEBUG);
            }

        }

        /// <summary>
        /// 信息日志。
        /// </summary>
        /// <param name="message">日志内容</param>
        public static void Info(object message, Boolean isShowStack = SHOW_STACK)
        {
            if (LogLevel.INFO == (CurrentLogLevels & LogLevel.INFO))
                Log(string.Concat(" [INFO]: ", isShowStack ? GetStackInfo() : "", message), LogLevel.INFO);
        }

        /// <summary>
        /// 警告日志。
        /// </summary>
        /// <param name="message">日志内容</param>
        public static void Warning(object message, Boolean isShowStack = SHOW_STACK)
        {
            if (LogLevel.WARNING == (CurrentLogLevels & LogLevel.WARNING))
                Log(string.Concat(" [WARNING]: ", isShowStack ? GetStackInfo() : "", message), LogLevel.WARNING);
        }

        /// <summary>
        /// 异常日志。
        /// </summary>
        /// <param name="message">日志内容</param>
        public static void Error(object message, Boolean isShowStack = SHOW_STACK)
        {
            if (LogLevel.ERROR == (CurrentLogLevels & LogLevel.ERROR))
                Log(string.Concat(" [ERROR]: ", message, '\n', isShowStack ? GetStacksInfo() : ""), LogLevel.ERROR);
        }

        /// <summary>
        /// 关键日志。
        /// </summary>
        /// <param name="message">日志内容</param>
        public static void Critical(object message, Boolean isShowStack = SHOW_STACK)
        {
            if (LogLevel.CRITICAL == (CurrentLogLevels & LogLevel.CRITICAL))
                Log(string.Concat(" [CRITICAL]: ", message, '\n', isShowStack ? GetStacksInfo() : ""), LogLevel.CRITICAL);
        }

        /// <summary>
        /// 异常日志。
        /// </summary>
        /// <param name="ex">异常实例。</param>
        public static void Except(Exception ex, object message = null)
        {
            if (LogLevel.EXCEPT == (CurrentLogLevels & LogLevel.EXCEPT))
            {
                Exception innerException = ex;
                while (innerException.InnerException != null)
                {
                    innerException = innerException.InnerException;
                }
                Log(string.Concat(" [EXCEPT]: ", message == null ? "" : message + "\n", ex.Message, innerException.StackTrace), LogLevel.CRITICAL);
            }
        }

        /// <summary>
        /// 获取堆栈信息。
        /// </summary>
        /// <returns></returns>
        private static String GetStacksInfo()
        {
            StringBuilder sb = new StringBuilder();
            StackTrace st = new StackTrace();
            var sf = st.GetFrames();
            for (int i = 2; i < sf.Length; i++)
            {
                sb.AppendLine(sf[i].ToString());
            }

            return sb.ToString();
        }

        private static String MsgRegEx = "Ari";
        public static void SetRegEx(string newMsgRegEx)
        {
            MsgRegEx = newMsgRegEx;
        }

        /// <summary>
        /// 写日志。
        /// </summary>
        /// <param name="message">日志内容</param>
        private static void Log(string message, LogLevel level, bool writeEditorLog = true)
        {
            if (MsgRegEx != null && MsgRegEx != "" && level <= LogLevel.WARNING)
            {
                //m_logWriter.WriteLog("t" + Regex.IsMatch(message, MsgRegEx), LogLevel.INFO, writeEditorLog);
                if (Regex.IsMatch(message, MsgRegEx) == false)
                {
                    //m_logWriter.WriteLog("f" + Regex.IsMatch(message, MsgRegEx), LogLevel.CRITICAL, writeEditorLog);
                    return;
                }
            }

            if (saveLastLogs)
            {
                lastLogs.Add(message);
                if (lastLogs.Count > 50) lastLogs.RemoveAt(0);
            }
            if (logDelegate != null) logDelegate(message);

            var msg = string.Concat(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss,fff"), message);
            m_logWriter.WriteLog(msg, level, writeEditorLog);
            //Debugger.Log(0, "TestRPC", message);
        }

        /// <summary>
        /// 获取调用栈信息。
        /// </summary>
        /// <returns>调用栈信息</returns>
        private static String GetStackInfo()
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(2);//[0]为本身的方法 [1]为调用方法
            if (sf == null) return string.Empty; //针对IOS进行优化
            var method = sf.GetMethod();
            return String.Format("{0}.{1}(): ", method.ReflectedType.Name, method.Name);
        }

        private static void ProcessExceptionReport(string message, string stackTrace, LogType type)
        {
            var logLevel = LogLevel.DEBUG;
            switch (type)
            {
                case LogType.Assert:
                    logLevel = LogLevel.DEBUG;
                    break;
                case LogType.Error:
                    logLevel = LogLevel.ERROR;
                    break;
                case LogType.Exception:
                    logLevel = LogLevel.EXCEPT;
                    break;
                case LogType.Log:
                    logLevel = LogLevel.DEBUG;
                    break;
                case LogType.Warning:
                    logLevel = LogLevel.WARNING;
                    break;
                default:
                    break;
            }

            if (logLevel == (CurrentLogLevels & logLevel))
                Log(string.Concat(" [SYS_", logLevel, "]: ", message, '\n', stackTrace), logLevel, false);
        }

        /// <summary>
        /// 把毫秒转化为秒或分钟或小时
        ///  1、如果能转成小时,则转成小时
        ///  2、如果能转成分钟，则转成分钟
        ///  3、如果能转成秒，则转成秒
        ///  4、如果能转成毫秒，则直接返回
        /// </summary>
        /// <param name="millSecondTime">毫秒数</param>
        /// <returns></returns>
        public static string formatTime(int millSecondTime)
        {
            if (millSecondTime > 1000)
            {
                StringBuilder str = new StringBuilder();
                int h = millSecondTime / (60 * 60 * 1000);
                int modM = millSecondTime % (60 * 60 * 1000);
                int modS = modM % (60 * 1000);
                int m = modM / (60 * 1000);
                int s = modS / 1000;
                int mm = modS % 1000;

                if (h > 0) str.Append(h + "时");
                if (m > 0) str.Append(m + "分");
                if (s > 0) str.Append(s + "秒");
                if (mm > 0) str.Append(mm + "毫秒");
                return str.ToString();
            }
            else
            {
                return string.Format("{0}毫秒", millSecondTime);
            }
        }

    }

    /// <summary>
    /// 日志写入文件管理类。
    /// </summary>
    public class LogWriter
    {
        private string m_logPath;
        private string m_logFileName = "log_{0}.txt";
        private string m_logFilePath;
        private FileStream m_fs;
        private StreamWriter m_sw;
        private Action<String, LogLevel, bool> m_logWriter;
        private readonly static object m_locker = new object();

        public string LogPath
        {
            get { return m_logPath; }
        }

        public string LogFilePath
        {
            get
            {
                return m_logFilePath;
            }
        }

        private int SortFunc(string file1, string file2)
        {
            return file1.CompareTo(file2);
        }

        /// <summary>
        /// 默认构造函数。
        /// </summary>
        public LogWriter()
        {
            //if (UnityPropUtils.Platform == RuntimePlatform.WindowsWebPlayer) return;//为排除警告
            m_logPath = string.Concat(UnityEngine.Application.persistentDataPath, ConstString.RutimeResource, "/log");
            if (!Directory.Exists(m_logPath))
                Directory.CreateDirectory(m_logPath);

            string[] fileList = Directory.GetFiles(m_logPath);
            if (fileList.Length > 100)
            {
                List<string> logFiles = new List<string>();
                List<string> driverFiles = new List<string>();
                for (int i = 0; i < fileList.Length; i++)
                {
                    if (fileList[i].Contains("driver_"))
                    {
                        driverFiles.Add(fileList[i]);
                    }
                    else if (fileList[i].Contains("log_"))
                    {
                        logFiles.Add(fileList[i]);
                    }
                }
                driverFiles.Sort(SortFunc);
                logFiles.Sort(SortFunc);
                if (logFiles.Count > 50)
                {
                    for (int j = 0; j < logFiles.Count - 50; j++)
                    {
                        File.Delete(logFiles[j]);
                    }
                }

                if (driverFiles.Count > 50)
                {
                    for (int j = 0; j < driverFiles.Count - 50; j++)
                    {
                        File.Delete(driverFiles[j]);
                    }
                }

            }


            m_logFilePath = String.Concat(m_logPath, "/", String.Format(m_logFileName, DateTime.Today.ToString("yyyyMMdd")));
            try
            {
                m_logWriter = Write;
                m_fs = new FileStream(m_logFilePath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                m_sw = new StreamWriter(m_fs);
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.LogError(ex.Message);
            }
        }



        /// <summary>
        /// 释放资源。
        /// </summary>
        public void Release()
        {
            lock (m_locker)
            {
                if (m_sw != null)
                {
                    m_sw.Close();
                    m_sw.Dispose();
                    m_sw = null;
                }
                if (m_fs != null)
                {
                    m_fs.Close();
                    m_fs.Dispose();
                    m_fs = null;
                }
            }
        }

        public void UploadTodayLog()
        {
            //lock (m_locker)
            //{
            //    using (var fs = new FileStream(m_logFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            //    {
            //        using (StreamReader sr = new StreamReader(fs))
            //        {
            //            var content = sr.ReadToEnd();
            //            var fn = Utils.GetFileName(m_logFilePath);//.Replace('/', '\\')
            //            if (MogoWorld.theAccount != null)
            //            {
            //                fn = string.Concat(MogoWorld.theAccount.name, "_", fn);
            //            }
            //            DownloadMgr.Instance.UploadLogFile(fn, content);
            //        }
            //    }
            //}
        }

        /// <summary>
        /// 写日志。
        /// </summary>
        /// <param name="msg">日志内容</param>
        public void WriteLog(string msg, LogLevel level, bool writeEditorLog)
        {
            //if (UnityPropUtils.Platform == RuntimePlatform.WindowsWebPlayer) return;//为排除警告
            if (UnityPropUtils.Platform == RuntimePlatform.IPhonePlayer)
                m_logWriter(msg, level, writeEditorLog);
            else
                m_logWriter.BeginInvoke(msg, level, writeEditorLog, null, null);
            UploadBug(msg, level);
        }

        private void UploadBug(string message, LogLevel level)
        {
            if (level == LogLevel.ERROR || level == LogLevel.EXCEPT || level == LogLevel.CRITICAL)
            {
                string bugMsg = GetBugMsg(message);
                LogUploader.GetInstance().UploadBug(bugMsg);
            }
        }

        public static string GetBugMsg(string message)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("------------------------------------------------------\n");
            sb.AppendFormat("logtime: [{0}]\n", getCurrentTime());
            sb.AppendFormat("platform: {0}\n", UnityPropUtils.Platform);
            sb.AppendFormat("deviceinfo: device={0}\n", UnityPropUtils.DeviceModel);
            sb.Append(message);
            return sb.ToString();
        }

        private static string getCurrentTime()
        {
            System.DateTime now = System.DateTime.Now;
            return now.ToString("yyyy-MM-dd HH:mm:ss fff", DateTimeFormatInfo.InvariantInfo);
        }

        private void Write(string msg, LogLevel level, bool writeEditorLog)
        {
            lock (m_locker)
                try
                {
                    if (writeEditorLog)
                    {
                        switch (level)
                        {
                            case LogLevel.DEBUG:
                            case LogLevel.INFO:
                                UnityEngine.Debug.Log(msg);
                                break;
                            case LogLevel.WARNING:
                                UnityEngine.Debug.LogWarning(msg);
                                break;
                            case LogLevel.ERROR:
                            case LogLevel.EXCEPT:
                            case LogLevel.CRITICAL:
                                UnityEngine.Debug.LogError(msg);
                                break;
                            default:
                                break;
                        }
                    }
                    if (m_sw != null)
                    {
                        m_sw.WriteLine(msg);
                        m_sw.Flush();
                    }
                }
                catch (Exception ex)
                {
                    UnityEngine.Debug.LogError(ex.Message);
                }
        }
    }
}
