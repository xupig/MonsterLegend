﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security;
using System.Threading;
using System.Runtime.CompilerServices;
using GameLoader.Mgrs;

namespace GameLoader.Utils
{
    public class HttpUtils
    {
        public static void Get(string Url, Action<string> onDone, Action<HttpStatusCode> onFail)
        {
            HttpWebRequest req = null;
            HttpWebResponse rep = null;
            try
            {
                req = (HttpWebRequest)WebRequest.Create(Url);
                req.Timeout = BreakPointDownload.TIMEOUT;
                req.ReadWriteTimeout = BreakPointDownload.TIMEOUT;
                req.Proxy = null;
                uint timeID = Timer.TimerHeap.AddTimer(15000, 0, () =>
                {
                    LoaderDriver.Invoke(onFail.SafeInvoke, HttpStatusCode.RequestTimeout);
                });
                Action action = () =>
                {
                    rep = (HttpWebResponse)req.GetResponse();

                    if (rep.StatusCode != HttpStatusCode.OK)
                    {
                        Timer.TimerHeap.DelTimer(timeID);
                        if (onFail != null) onFail(rep.StatusCode);
                    }
                    else
                    {
                        Timer.TimerHeap.DelTimer(timeID);
                        Stream receiveStream = rep.GetResponseStream();
                        StreamReader readStream = new StreamReader(receiveStream, System.Text.Encoding.UTF8);

                        Char[] read = new Char[1024];
                        int count = readStream.Read(read, 0, 1024);
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        while (count > 0)
                        {
                            String readstr = new String(read, 0, count);
                            sb.Append(readstr);
                            count = readStream.Read(read, 0, 1024);
                        }
                        rep.Close();
                        readStream.Close();
                        LoaderDriver.Invoke(onDone.SafeInvoke, sb.ToString());
                    }
                };
                action.BeginInvoke(null, null);
            }
            catch (Exception e)
            {
                LoggerHelper.Error("HttpUtils Get error: " + Url + "\n" + e.Message);
                if (onFail != null) onFail(HttpStatusCode.NotAcceptable);
            }

        }
    }
}
