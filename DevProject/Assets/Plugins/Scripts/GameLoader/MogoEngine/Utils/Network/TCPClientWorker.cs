// 模块名   :  TCPClientWorker
// 创建者   :  Steven Yang
// 创建日期 :  2012-12-8
// 描    述 :  客户端网络接收类

using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Threading;

using GameLoader.Utils;
using GameLoader.Utils.Timer;
using System.Collections;

namespace GameLoader.Utils.Network
{
    /**网络状态监听**/
    public delegate void NetStatusListener(String status);

    public struct ReceiveMark
    {
        public int idx;
        public int len;
        public ReceiveMark(int nidx, int nlen)
        {
            idx = nidx;
            len = nlen;
        }
    }

    /// <summary>
    /// 网络通讯类
    /// </summary>
    public class TCPClientWorker
    {
        private uint timerId = 0;                                  //定时器ID 
        private int readNum = 0;                                   //读取字节数
        private int msgLen = 0;                                    //当前接收包的包体长度
        //private int tryNum = 0;                                    //已重试数           //为排除警告，注释掉
        private int beginTime;                                     //最后收包时间
        private Socket client;                                     //当前连接句柄
        private Thread thread;                                     //接收线程

        private byte[] headBuffer;                                 //包头缓冲区
        private byte[] reserveBuffer;                              //预留域缓冲区
        private bool isConnecting;								//正在连接中
        private bool isReceive = false;                            //接收数据状态
        public bool isReceiving { get { return isReceive; } }
        private bool isPrintClose = false;                         //是否打印网络关闭时日志

        private Queue<ReceiveMark> receiveMarkQueue;               //已收包索引队列 
        private Queue<ReceiveMark> _tempReceiveMarkQueue = new Queue<ReceiveMark>();
        private const int RECEIVE_LEN = 0xffff;
        private byte[] _receiveBuff = new byte[RECEIVE_LEN];
        private int _receivePoint = 0;

        private const int SEND_LEN = 0xffff;
        private byte[] _sendBuff = new byte[SEND_LEN];
        private int _sendPoint = 0;
        private int _sendingNum = 0;
        
        private NetStatusListener netStatusListener;               //网络状态回调
        private static readonly object recvLocker = new object();  //同步锁
        private static readonly object sendLocker = new object();    //同步锁
        private static readonly object snLocker = new object();    //同步锁

        private UInt16 _sendSerialNumber = 0;                     //包序号,从0开始,达到最大值后又重复从0开始累加,如此反复
        private Int32 _headLength = 4;//Marshal.SizeOf(typeof(UInt32));

        private List<byte[]> _needSeriNumHeadList = new List<byte[]>();
        private List<byte[]> _needIgnoreHeadList = new List<byte[]>();

        private const int RESERVE_SIZE = 2;                        //预留域长度
        public const string LINKING = "linking";                   //连接中----常量  
        public const string LINK_OK = "linkOk";                    //连接OK----常量   
        public const string LINK_FAIL = "linkFail";                //连接失败--常量
        public static int CLOSE_TIMEOUT = 30 * 1000;
        private int _closeTimeout = int.MaxValue;                  //网络断线超时(毫秒),默认无穷大相当于关闭断线检查。 
        public Action OnNetworkDisconnected;                       //连接关闭--监听

        private static Int32 UInt16TypeLength = 2;//Marshal.SizeOf(typeof(UInt16));
        private static Byte[] EncodeToUInt16(Object vValue)
        {
            var result = BitConverter.GetBytes(Convert.ToUInt16(vValue));
            Array.Reverse(result);
            return result;
        }

        private static Object DecodeFromUInt16(byte[] data, ref Int32 index)
        {
            Byte[] result = new Byte[UInt16TypeLength];
            Buffer.BlockCopy(data, index, result, 0, UInt16TypeLength);
            index += UInt16TypeLength;
            Array.Reverse(result);
            return BitConverter.ToUInt16(result, 0);
        }

        public TCPClientWorker()
        {
            maxTryNum = 3;
            batchNum = 10;
            receiveMarkQueue = new Queue<ReceiveMark>();
            headBuffer = new byte[_headLength];
            reserveBuffer = new byte[RESERVE_SIZE];
        }

        public string GetBytesString(byte[] bytes, int idx, int len)
        {
            string temp = "";
            for (int i = idx; i < idx + len; i++)
            {
                temp += Convert.ToString(bytes[i], 16) + ",";
            }
            return temp;
        }

        public void AddNeedSeriNumHead(int headid)
        {
            _needSeriNumHeadList.Add(EncodeToUInt16(headid));
        }

        public void AddNeedIgnoreHead(int headid)
        {
            this._needIgnoreHeadList.Add(EncodeToUInt16(headid));
        }

        private int closeTimeout
        {
            set { beginTime = Environment.TickCount; _closeTimeout = value; }
            get { return _closeTimeout; }
        }

        /// <summary>
        /// 设置网络断线超时检查状态
        /// </summary>
        /// <param name="state">[true:开启断线检查,false:关闭断线检查]</param>
        public void enableTimeout(bool state)
        {
            closeTimeout = state ? CLOSE_TIMEOUT : int.MaxValue;
        }

        /**连接服务器
         * @param ip   服务器IP
         * @param port 服务器断端口
         * **/
        public void Connect(string ip, int port, Action<bool> connectedCB)
        {
            if (isConnecting)
            {
                LoggerHelper.Error(string.Format("服务器已正在连接中，重复调入！ ip {0}，port {1}", ip, port));
                connectedCB(false);
                return;
            }
            try
            {
                this.ip = ip;
                this.port = port;
                var addrs = Dns.GetHostAddresses(ip);

                LoggerHelper.Info(string.Format("-connect() new 连接服务器[{0}:{1}, host: {2}, type: {3}] [Start]", addrs[0], port, ip, addrs[0].AddressFamily));
                if (client != null)client.Close();
                client = new Socket(addrs[0].AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                client.NoDelay = true;            //Negal
                //client.SendBufferSize = 0xfa00;
                //client.ReceiveBufferSize = 0xfa00;
                isConnecting = true;
                client.BeginConnect(ip, port, delegate(IAsyncResult ar)
                { //BeginConnect会创建一个新线程，连接完成后如果直接回调上层逻辑可能产生线程不安全的问题，所以回调只做标记赋值。
                    isConnecting = false;
                    client.EndConnect(ar);
                    _sendingNum = 0;
                }, null);
                //通过协程对isConnecting进行监听，获取BeginConnect中线程连接完成的状态，再进行上层逻辑回调。
                LoaderDriver.Instance.StartCoroutine(WaittingConnectCompleted(connectedCB));
            }
            catch (Exception ex)
            {
                //ios的ipv6还有bug，看后续版本是否仍存在这问题
                if (!ex.ToString().Contains("System.Net.Sockets.SocketException: An address incompatible with the requested protocol was used"))
                {
                    connectedCB(false);
                    LoggerHelper.Error("-connect() 连接服务器3[" + ip + ":" + port + "]失败,reason:" + ex.ToString());
                }
            }
        }

        private IEnumerator WaittingConnectCompleted(Action<bool> connectedCB)
        {
            while (isConnecting)
            {
                yield return null;
            }
            if (this.Connected())
            {
                try
                {
                    resetWhileConnected();
                    startReceive();
                    if (netStatusListener != null) netStatusListener(LINK_OK);
                    if (timerId > 0) TimerHeap.DelTimer(timerId);
                    timerId = TimerHeap.AddTimer(1000, 3000, closeNotice);                                                                 //连接关闭通知
                    connectedCB(true);
                    LoggerHelper.Debug("-connect() 连接服务器1[" + client.RemoteEndPoint.ToString() + "],timeId:" + timerId + " [OK]");
                }
                catch (Exception ex)
                {
                    //ios的ipv6还有bug，看后续版本是否仍存在这问题
                    if (!ex.ToString().Contains("System.Net.Sockets.SocketException: An address incompatible with the requested protocol was used"))
                    {
                        connectedCB(false);
                        LoggerHelper.Error("-connect() 连接服务器2[" + ip + ":" + port + "]失败,reason:" + ex.ToString());
                    }
                }
            }
            else
            {
                connectedCB(false);
            }
            yield return null;
        }

        /**连接成功后，重置参数值**/
        private void resetWhileConnected()
        {
            //tryNum = 0;//为排除警告，注释掉
            msgLen = 0;
            readNum = 0;
            resetSerialNumber();
            isPrintClose = false;
            beginTime = Environment.TickCount;
        }

        /**启动数据接收线程**/
        private void startReceive()
        {
            isReceive = true;
            _receivePoint = 0;
            if (thread == null)
            {
                thread = new Thread(receive);
                thread.Start();
            }
        }

        /**停止数据接收线程**/
        private void stopReceive()
        {
            try
            {
                isReceive = false;
                if (thread != null) thread.Abort();
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("-stopReceive() 接收线程终止失败，reason：" + ex.Message + "\n" + ex.StackTrace);
            }
            finally
            {
                thread = null;
            }
        }

        /**网络关闭通知**/
        private void closeNotice()
        {
            if (Environment.TickCount - beginTime > _closeTimeout)
            {//网络已断开超时 OnNetworkDisconnected
                LoggerHelper.Info(string.Format("与服务器[{0}:{1}={2}]断开连接,通知上层:{3},timerId:{4},{5}-{6}={7}[closeTimeout]", ip, port, Connected(), OnNetworkDisconnected, timerId, Environment.TickCount, beginTime, _closeTimeout));
                Close();
            }
        }

        /**服务器关闭回调**/
        private void closeCallback()
        {
            try
            {
                if (OnNetworkDisconnected != null) OnNetworkDisconnected();
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex, "服务器[" + ip + ":" + port + "]关闭回调出错!");
            }
        }

        /**发送包
         * @param data 发送数据
         * **/
        public void Send(byte[] data)
        {
            if (data == null || data.Length < 0)
            {
                LoggerHelper.Error("发送包为空或长度为零！data:" + data);
                return;
            }
            SendData(data);
        }

        private static int HEAD_BYTE_LEN_MAX = 1024;
        private byte[][] _headBytesCache = new byte[HEAD_BYTE_LEN_MAX][];
        private byte[] GetHeadBytes(int len)
        {
            if (len < HEAD_BYTE_LEN_MAX)
            {
                if (_headBytesCache[len] == null)
                {
                    byte[] cacheHeadBytes = BitConverter.GetBytes(len);  //包头
                    Array.Reverse(cacheHeadBytes);
                    _headBytesCache[len] = cacheHeadBytes;
                }
                return _headBytesCache[len];
            }
            else
            {
                byte[] headBytes = BitConverter.GetBytes(len);  //包头
                Array.Reverse(headBytes);
                return headBytes;
            }
        }

        /**进行发包
         * 发包组成:包头+序号+包体
         * 1、包头内容由：‘序号长度+包头长度+包体长度’组成
         * **/
        private void SendData(byte[] data)
        {
            if (Connected())
            {
                if (_sendingNum == 0) _sendPoint = 0;
                int sendLen = _headLength + RESERVE_SIZE + data.Length;
                //UnityEngine.Debug.LogError("SendData:" + sendLen);
                byte[] headBytes = GetHeadBytes(sendLen);  //包头

                int allLen = _sendPoint + sendLen;
                if (_sendBuff.Length < allLen)
                {
                    _sendBuff = new byte[_sendBuff.Length + SEND_LEN];
                    LoggerHelper.Info("_sendReceiveBuff--->:" + _sendBuff.Length);
                }
                headBytes.CopyTo(_sendBuff, _sendPoint);
                int seriNum = CheckSerialNumber(data);
                if (seriNum >= 0) EncodeToUInt16(seriNum).CopyTo(_sendBuff, _sendPoint + headBytes.Length);                    //需发送序号
                data.CopyTo(_sendBuff, _sendPoint + headBytes.Length + RESERVE_SIZE);
                int idx = _sendPoint;
                _sendPoint = _sendPoint + sendLen;

                //LoggerHelper.Error("BeginSend: + " + GetBytesString(data, 0, data.Length));
                lock (sendLocker){ _sendingNum++; }
                client.BeginSend(_sendBuff, idx, sendLen, SocketFlags.None, (result) =>
                {
                    client.EndSend(result);
                    lock (sendLocker){_sendingNum--;}
                }, null); 
                //LoggerHelper.Info("发送包Len:" + sendBytes.Length + ",seriNum:" + seriNum + ",bodyLen:" + data.Length + ",isReceive:" + isReceive + ",thread:" + (thread != null ? thread.IsAlive : false) + ",timeId:" + timeId);
            }
            else
            {
                //LoggerHelper.Error("!Connected() + " +  GetBytesString(data));
                if (!isPrintClose) { isPrintClose = true; LoggerHelper.Info("-sendData() 服务器[" + ip + ":" + port + "] 已关闭!"); }
            }
        }

        private int CheckSerialNumber(byte[] data)
        {
            int seriNum = -1;
            //if (CheckCanBeCache(data))
            //{
            //    seriNum = getSerialNumber(); 
            // }

            for (int i = 0; i < _needSeriNumHeadList.Count; i++)
            {
                if (CompareMsgHead(_needSeriNumHeadList[i], data))
                {//加序号，防重发
                    seriNum = getSerialNumber();
                    break;
                }
            }
            return seriNum;
        }

        private bool CheckCanBeCache(byte[] data)
        {
            for (int i = 0; i < _needIgnoreHeadList.Count; i++)
            {
                if (CompareMsgHead(_needIgnoreHeadList[i], data))
                {//加序号，防重发
                    return false;
                }
            }
            return true;
        }

        /**执行收包,收包组成：4字节包头+2字节预留域+2字节消息ID+包体**/
        private void receive()
        {
            int headLen = headBuffer.Length + RESERVE_SIZE;
            while (isReceive)
            {
                try
                {
                    if (!Connected()) continue;
                    if (msgLen == 0)
                    {//接收包头
                        if (client.Available >= headLen)
                        {
                            client.Receive(headBuffer);
                            client.Receive(reserveBuffer);
                            msgLen = BitConverterExtend.ToInt32(headBuffer, 0);
                            msgLen = msgLen - headBuffer.Length - RESERVE_SIZE;
                            readNum = 0;
                        }
                        else
                        {
                            if (_receivePoint > 0)
                            {
                                lock (recvLocker)
                                {
                                    if (receiveMarkQueue.Count == 0 && _tempReceiveMarkQueue.Count == 0)
                                    {
                                        _receivePoint = 0;
                                    }
                                    else
                                    {
                                        while (_tempReceiveMarkQueue.Count > 0)
                                        {
                                            receiveMarkQueue.Enqueue(_tempReceiveMarkQueue.Dequeue());
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Thread.Sleep(10);
                            }
                        }
                    }
                    else
                    {//接收包体
                        //方法一：收包
                        int size = msgLen - readNum;
                        if (size > 0)
                        {//包读取中
                            int allLen = _receivePoint + readNum + size;
                            if (_receiveBuff.Length < allLen)
                            {
                                byte[] newTempReceiveBuff = new byte[_receiveBuff.Length + RECEIVE_LEN];
                                _receiveBuff.CopyTo(newTempReceiveBuff, 0);
                                _receiveBuff = newTempReceiveBuff;
                                LoggerHelper.Info("_tempReceiveBuff--->:" + _receiveBuff.Length);
                            }
                            readNum += client.Receive(_receiveBuff, _receivePoint + readNum, size, SocketFlags.None);
                        }
                        else
                        {//包读取完
                            _tempReceiveMarkQueue.Enqueue(new ReceiveMark(_receivePoint, readNum));
                            //LoggerHelper.Error("<----TCP ReceiveData2:" + BitConverterExtend.ToInt16(reserveBuffer, 0) + " - " + GetBytesString(_receiveBuff, _receivePoint, readNum));
                            _receivePoint = _receivePoint + readNum;
                            msgLen = 0;
                            readNum = 0;
                            beginTime = Environment.TickCount;
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (isReceive)
                    {
                        LoggerHelper.Error(string.Format("-receive() 接收包出错, reason:{0}", ex.StackTrace));
                        LoggerHelper.Error("msgLen:" + msgLen);
                    }
                }
            }
        }

        /**提供给上层提取一个接收包**/
        public void Recv(ref byte[] handleBuff, Queue<ReceiveMark> handleQueue)
        {
            lock (recvLocker)
            {//加锁
                if (handleBuff.Length != _receiveBuff.Length)
                {
                    handleBuff = new byte[_receiveBuff.Length];
                }
                while (receiveMarkQueue.Count > 0)
                {
                    var mark = receiveMarkQueue.Dequeue();
                    handleQueue.Enqueue(mark);
                    Array.Copy(_receiveBuff, mark.idx, handleBuff, mark.idx, mark.len);
                }
            }
        }

        public int GetRecieveQueueCount()
        {
            lock (recvLocker)
            {//加锁
                return receiveMarkQueue.Count;
            }
        }

        /**连接IP**/
        public string ip { get; set; }
        /**连接端口**/
        public int port { get; set; }
        /**设置|取得最大重连次数**/
        public int maxTryNum { get; set; }
        /**设置|取得每帧处理包数**/
        public int batchNum { get; set; }
        /**开启重连状态[true:开启重连,false:关闭重连]**/
        public bool isOpenTryLink { get; set; }
        /**取得连接状态**/
        public bool Connected() { return client != null ? client.Connected : false; }
        /**连接中状态**/
        public bool Connecting() { return isConnecting; }

        /**关闭连接**/
        public void Close()
        {
            stopReceive();
            lock (recvLocker)
            {
                receiveMarkQueue.Clear();
            }
            //OnNetworkDisconnected = null;
            if (client != null) client.Close();
            if (timerId > 0) TimerHeap.DelTimer(timerId);
            closeCallback();
            LoggerHelper.Info("-close() connected:" + Connected() + " 已断开与服务器[" + ip + ":" + port + "]连接！");
        }

        /**网络状态监听**/
        public void statusListener(NetStatusListener listener)
        {
            netStatusListener = listener;
        }

        /**返回当前序号并对当前序号+1
         * 1、序号范围在[0,65535]间，达到最大值后又从0开始，如此反复
         * @return 当前序号(从0开始，最大65535)
         * **/
        private UInt16 getSerialNumber()
        {
            lock (snLocker)
            {
                if (_sendSerialNumber > UInt16.MaxValue)
                {
                    _sendSerialNumber = 0;
                }
                return _sendSerialNumber++;
            }
        }

        private void resetSerialNumber()
        {
            lock (snLocker)
            {
                _sendSerialNumber = 0;
            }
        }

        private bool CompareMsgHead(byte[] headCode, byte[] msg)
        {
            if (msg.Length < headCode.Length) return false;
            for (int i = 0; i < headCode.Length; i++)
            {
                if (headCode[i] != msg[i]) return false;
            }
            return true;
        }


        #region 暂时无用
        public void Process()
        {
            //DoSend();
        }

        /// <summary>
        /// 强迫中断线程
        /// </summary>
        public void Release()
        {//关闭发送死循环
            //m_asynSendSwitch = false;
        }
        #endregion
    }
}