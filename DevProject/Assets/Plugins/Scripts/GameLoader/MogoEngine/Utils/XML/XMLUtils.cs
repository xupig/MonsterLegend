﻿using System;
using System.Collections.Generic;
using System.Text;
using GameLoader.Utils;

namespace GameLoader.Utils.XML
{
    public class XMLUtils
	{
        public static void SaveXMLList<T>(string path, List<T> data, string attrName = "record")
        {
            try
            {
                var root = new System.Security.SecurityElement("root");
                var props = typeof(T).GetProperties();
                foreach (var item in data)
                {
                    if (item == null)
                    {
                        LoggerHelper.Error("null item: " + path);
                        continue;
                    }
                    var xml = new System.Security.SecurityElement(attrName);
                    foreach (var prop in props)
                    {
                        var type = prop.PropertyType;
                        String result = String.Empty;
                        object obj = prop.GetGetMethod().Invoke(item, null);
                        if (obj == null)
                        {
                            LoggerHelper.Error("null obj: " + prop.Name);
                            continue;
                        }
                        //var obj = prop.GetValue(item, null);
                        if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                        {
                            var o = typeof(CommonUtils).GetMethod("PackMap")
                        .MakeGenericMethod(type.GetGenericArguments())
                            .Invoke(null, new object[] { obj, ':', ',' });
                            if (o != null)
                                result = o.ToString();
                            else
                                LoggerHelper.Error("null obj: " + prop.Name);
                        }
                        else if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>))
                        {
                            var o = typeof(CommonUtils).GetMethod("PackList")
                        .MakeGenericMethod(type.GetGenericArguments())
                            .Invoke(null, new object[] { obj, ',' });

                            if (o != null)
                                result = o.ToString();
                            else
                                LoggerHelper.Error("null obj: " + prop.Name);
                        }
                        else
                        {
                            result = obj.ToString();
                        }
                        xml.AddChild(new System.Security.SecurityElement(prop.Name, result));
                    }
                    root.AddChild(xml);
                }
                FileUtils.SaveText(path, root.ToString());
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        public static List<T> LoadXMLText<T>(string text)
        {
            List<T> list = new List<T>();
            try
            {
                if (String.IsNullOrEmpty(text))
                {
                    return list;
                }
                Type type = typeof(T);
                var xml = XMLParser.LoadXML(text);
                Dictionary<Int32, Dictionary<String, String>> map = XMLParser.LoadIntMap(xml, text);
                var props = type.GetProperties(~System.Reflection.BindingFlags.Static);
                foreach (var item in map)
                {
                    var obj = type.GetConstructor(Type.EmptyTypes).Invoke(null);
                    foreach (var prop in props)
                    {
                        if (prop.Name == "id")
                            prop.SetValue(obj, item.Key, null);
                        else
                            try
                            {
                                if (item.Value.ContainsKey(prop.Name))
                                {
                                    var value = CommonUtils.GetValue(item.Value[prop.Name], prop.PropertyType);
                                    prop.SetValue(obj, value, null);
                                }
                            }
                            catch (Exception ex)
                            {
                                LoggerHelper.Except(ex);
                            }
                    }
                    list.Add((T)obj);
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
                LoggerHelper.Error("error text: \n" + text);
            }
            return list;
        }

		public static void ReflectToSimpleTypeByField<T>(string xml,T t)
		{
			try
			{
				System.Security.SecurityElement root=xml.ToXMLSecurityElement();
				if (root != null && root.Children != null && root.Children.Count != 0)
				{
					System.Reflection.FieldInfo[] fis = t.GetType().GetFields();
					foreach (System.Security.SecurityElement item in root.Children)
					{
						try
						{
							foreach (System.Reflection.FieldInfo fi in fis)
							{
								if (fi != null && fi.Name == item.Tag && !string.IsNullOrEmpty(item.Text))
								{
									var value = GameLoader.Utils.CommonUtils.GetValue(item.Text, fi.FieldType);
									if (value != null)
										fi.SetValue(t, value);
								}
							}
						}
						catch (Exception ex)
						{
							GameLoader.Utils.LoggerHelper.Error("GetVersionInXML error: " + item.Tag + "\n" + ex.Message);
						}
					}
				}
			}
			catch(System.Exception e)
			{
				GameLoader.Utils.LoggerHelper.Error(e.Message);
			}
		}
		public static void ReflectToSimpleTypeByProperty<T>(string xml,T t)
		{
			try
			{
				System.Security.SecurityElement root=xml.ToXMLSecurityElement();
				if (root != null && root.Children != null && root.Children.Count != 0)
				{
					System.Reflection.PropertyInfo[] pis = t.GetType().GetProperties();
					foreach (System.Security.SecurityElement item in root.Children)
					{
						try
						{
							foreach (System.Reflection.PropertyInfo pi in pis)
							{
								
								if (pi != null && pi.Name == item.Tag && !string.IsNullOrEmpty(item.Text))
								{
									pi.GetSetMethod().Invoke(t,new object[]{GameLoader.Utils.CommonUtils.GetValue(item.Text, pi.PropertyType)});
								}
							}
						}
						catch (Exception ex)
						{
							GameLoader.Utils.LoggerHelper.Error("GetVersionInXML error: " + item.Tag + "\n" + ex.Message);
						}
					}
				}
			}
			catch(System.Exception e)
			{
				GameLoader.Utils.LoggerHelper.Error(e.Message);
			}
		}
    }
}
