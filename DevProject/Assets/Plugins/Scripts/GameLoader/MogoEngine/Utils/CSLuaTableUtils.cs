﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.IO;
using UnityEngine;
using System.Security.Cryptography;
using System.Net;
using System.Runtime.InteropServices;
using System.Collections;
using GameLoader.Utils.CustomType;


namespace GameLoader.Utils
{
    public static class CSLuaTableUtils
    {
        /// <summary>
        /// 将 Lua table 字符串转换为 Lua table 实体
        /// </summary>
        /// <param name="inputString">Lua table 字符串</param>
        /// <param name="result">实体对象</param>
        /// <returns>返回 true/false 表示是否成功</returns>
        public static bool ParseLuaTable(string inputString, out LuaTable result)
        {
            string trimString = inputString.Trim();
            if (trimString.Length <= 2)
            {
                result = new LuaTable();
                return true;
            }
            byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(inputString);
            var flag = ParseLuaTable(byteArray, out result);
            return flag;
        }

        /// <summary>
        /// 将 Byte流 转换为 Lua table 实体
        /// </summary>
        /// <param name="inputString">Byte[]</param>
        /// <param name="result">实体对象</param>
        /// <returns>返回 true/false 表示是否成功</returns>
        public static bool ParseLuaTable(byte[] inputString, out LuaTable result)
        {
            if (inputString.Length <= 2)
            {
                result = new LuaTable();
                return true;
            }

            var index = 0;
            StringBuilder sb = new StringBuilder();
            var flag = DecodeLuaTable(inputString, ref index, sb);
            if (flag)
            {
                result = new LuaTable();
                result.sourceLuaString = Encoding.UTF8.GetString(inputString, 0, inputString.Length);
                result.SetLuaString(sb.ToString());
            }
            else
            {
                result = null;
            }
            return flag;
        }
        private static bool DecodeLuaTable(string inputString, ref int index, StringBuilder sb)
        {
            if (!CommonUtils.WaitChar(inputString, '{', ref index))
            {
                return false;
            }
            try
            {
                if (CommonUtils.WaitChar(inputString, '}', ref index))//如果下一个字符为右大括号表示为空Lua table
                {
                    sb.Append("{}");
                    return true; 
                }
                sb.Append("{");
                while (index < inputString.Length)
                {
                    if (!DecodeKey(inputString, ref index, sb)) return false;//匹配键
                    CommonUtils.WaitChar(inputString, '=', ref index);//匹配键值对分隔符
                    sb.Append("=");
                    if (!DecodeLuaValue(inputString, ref index, sb)) return false;//转换实体
                    if (!CommonUtils.WaitChar(inputString, ',', ref index))
                        break;
                    sb.Append(",");
                }
                CommonUtils.WaitChar(inputString, '}', ref index);
                sb.Append("}");
                return true;
            }
            catch (Exception e)
            {
                LoggerHelper.Error("Parse LuaTable error: " + inputString + e.ToString());
                return false;
            }
        }
        private static bool DecodeLuaTable(byte[] inputString, ref int index, StringBuilder sb)
        {
            if (!CommonUtils.WaitChar(inputString, '{', ref index))
            {
                return false;
            }
            try
            {
                if (CommonUtils.WaitChar(inputString, '}', ref index))//如果下一个字符为右大括号表示为空Lua table
                {   
                    sb.Append("{}");
                    return true;
                }
                sb.Append("{");
                while (index < inputString.Length)
                {
                    if (!DecodeKey(inputString, ref index, sb)) return false;//匹配键
                    CommonUtils.WaitChar(inputString, '=', ref index);//匹配键值对分隔符
                    sb.Append("=");
                    if (!DecodeLuaValue(inputString, ref index, sb)) return false;//转换实体
                    if (!CommonUtils.WaitChar(inputString, ',', ref index))
                        break;
                    sb.Append(",");
                }
                CommonUtils.WaitChar(inputString, '}', ref index);
                sb.Append("}");
                return true;

            }
            catch (Exception e)
            {
                LoggerHelper.Error("Parse LuaTable error: " + inputString + e.ToString());
                return false;
            }
        }

        public static bool DecodeKey(byte[] inputString, ref int index, StringBuilder sb)
        {
            if (inputString[index] == 's')
            {
                //key is string
                var szLen = Encoding.UTF8.GetString(inputString, index + 1, 3);
                var length = Int32.Parse(szLen);
                if (length > 0)
                {
                    index += 4;
                    sb.Append("['");
                    sb.Append(Encoding.UTF8.GetString(inputString, index, length));
                    sb.Append("']");
                    index += length;
                    return true;
                }
                LoggerHelper.Error("Decode Lua Table Key Error: " + index + " " + inputString);
                return false;
            }
            else
            {
                //key is number
                int offset = 0;
                while (index + offset < inputString.Length && inputString[index + offset] != '=')
                {
                    offset++;
                }

                if (offset > 0)
                {
                    sb.Append("[");
                    sb.Append(Encoding.UTF8.GetString(inputString, index, offset));
                    sb.Append("]");
                    index = index + offset;
                    return true;
                }
                else
                {
                    LoggerHelper.Error("Decode Lua Table Key Error: " + index + " " + inputString);
                    return false;
                }
            }
        }
        private static bool DecodeLuaValue(byte[] inputString, ref int index, StringBuilder sb)
        {
            var firstChar = inputString[index];
            if (firstChar == 's')
            {
                //value is string
                var szLen = Encoding.UTF8.GetString(inputString, index + 1, 3);
                var length = Int32.Parse(szLen);
                index += 4;
                if (length > 0)
                {
                    sb.Append("'");
                    sb.Append(Encoding.UTF8.GetString(inputString, index, length));
                    sb.Append("'");
                    index += length;
                    return true;
                }
                else
                {
                    sb.Append("''");
                    return true;
                }
            }
            else if (firstChar == '{')//如果第一个字符为花括号，表示接下来的内容为列表或实体类型
            {
                return DecodeLuaTable(inputString, ref index, sb);
            }
            else
            {
                //value is number
                var i = index;
                while (++index < inputString.Length)
                {
                    if (inputString[index] == ',' || inputString[index] == '}')
                    {
                        if (index > i)
                        {
                            sb.Append(Encoding.UTF8.GetString(inputString, i, index - i));
                            return true;
                        }
                    }
                }
                LoggerHelper.Error("Decode Lua Table Value Error: " + index + " " + inputString);
                return false;
            }
        }
        /// <summary>
        /// 解析 Lua table 的键。
        /// </summary>
        /// <param name="inputString">Lua table字符串</param>
        /// <param name="index">字符串偏移量</param>
        /// <param name="result">键</param>
        /// <returns>返回 true/false 表示是否成功</returns>
        public static bool DecodeKey(string inputString, ref int index, StringBuilder sb)
        {
            if (inputString[index] == 's')
            {
                //key is string
                var szLen = inputString.Substring(index + 1, 3);
                var lenth = Int32.Parse(szLen);
                if (lenth > 0)
                {
                    index += 4;
                    sb.Append("['");
                    sb.Append(inputString.Substring(index, lenth));
                    sb.Append("']");
                    index += lenth;
                    return true;
                }
                LoggerHelper.Error("Decode Lua Table Key Error: " + index + " " + inputString);
                return false;
            }
            else
            {
                //key is number
                var szLen = inputString.IndexOf('=', index);
                if (szLen > -1)
                {
                    var lenth = szLen - index;
                    sb.Append("[");
                    sb.Append(inputString.Substring(index, lenth));
                    sb.Append("]");
                    index = szLen;
                    //value = Int32.Parse(strValue);
                    return true;
                }
                else
                {
                    LoggerHelper.Error("Decode Lua Table Key Error: " + index + " " + inputString);
                    return false;
                }
            }
        }

        private static bool DecodeLuaValue(string inputString, ref int index, StringBuilder sb)
        {
            var firstChar = inputString[index];
            if (firstChar == 's')
            {
                //value is string
                var szLen = inputString.Substring(index + 1, 3);
                var lenth = Int32.Parse(szLen);
                index += 4;
                if (lenth > 0)
                {
                    sb.Append("'");
                    sb.Append(inputString.Substring(index, lenth));
                    sb.Append("'");
                    index += lenth;
                    return true;
                }
                else
                {
                    sb.Append("''");
                    return true;
                }
                //LoggerHelper.Error("Decode Lua Table Value Error: " + index + " " + inputString);
                //return false;
            }
            else if (firstChar == '{')//如果第一个字符为花括号，表示接下来的内容为列表或实体类型
            {
                return DecodeLuaTable(inputString, ref index, sb);
            }
            else
            {
                //value is number
                var i = index;
                while (++index < inputString.Length)
                {
                    if (inputString[index] == ',' || inputString[index] == '}')
                    {
                        if (index > i)
                        {
                            sb.Append(inputString.Substring(i, index - i));
                            return true;
                        }
                    }
                }
                LoggerHelper.Error("Decode Lua Table Value Error: " + index + " " + inputString);
                return false;
            }
        }
    }
}