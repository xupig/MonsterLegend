﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：Utils
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.2.1
// 模块描述：通用工具类。
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.IO;
using UnityEngine;
using System.Security.Cryptography;
using System.Net;
using System.Runtime.InteropServices;
using System.Collections;
using GameLoader.Utils.CustomType;

namespace GameLoader.Utils
{
    public enum LayerMask
    {
        Default = 1,
        Character = 1 << 8,
        Monster = 1 << 11,
        Npc = 1 << 12,
        Terrain = 1 << 9,
        Trap = 1 << 17,
        Mercenary = 1 << 18
    }

    /// <summary>
    /// 通用工具类。
    /// </summary>
    public static class CommonUtils
    {
        #region 常量

        /// <summary>
        /// 键值分隔符： ‘:’
        /// </summary>
        private const Char KEY_VALUE_SPRITER = ':';
        /// <summary>
        /// 字典项分隔符： ‘,’
        /// </summary>
        private const Char MAP_SPRITER = ',';
        /// <summary>
        /// 数组分隔符： ','
        /// </summary>
        private const Char LIST_SPRITER = ',';

        public const String RPC_HEAD = "RPC_";

        public const String SVR_RPC_HEAD = "SVR_RPC_";

        #endregion

        #region 字典转换

        /// <summary>
        /// 将字典字符串转换为键类型与值类型都为整型的字典对象。
        /// </summary>
        /// <param name="strMap">字典字符串</param>
        /// <param name="keyValueSpriter">键值分隔符</param>
        /// <param name="mapSpriter">字典项分隔符</param>
        /// <returns>字典对象</returns>
        public static Dictionary<Int32, Int32> ParseMapIntInt(this String strMap, Char keyValueSpriter = KEY_VALUE_SPRITER, Char mapSpriter = MAP_SPRITER)
        {
            Dictionary<Int32, Int32> result = new Dictionary<Int32, Int32>();
            var strResult = ParseMap(strMap, keyValueSpriter, mapSpriter);
            foreach (var item in strResult)
            {
                int key;
                int value;
                if (int.TryParse(item.Key, out key) && int.TryParse(item.Value, out value))
                    result.Add(key, value);
                else
                    LoggerHelper.Warning(String.Format("Parse failure: {0}, {1}", item.Key, item.Value));
            }
            return result;
        }

        /// <summary>
        /// 将字典字符串转换为键类型为整型，值类型为单精度浮点数的字典对象。
        /// </summary>
        /// <param name="strMap">字典字符串</param>
        /// <param name="keyValueSpriter">键值分隔符</param>
        /// <param name="mapSpriter">字典项分隔符</param>
        /// <returns>字典对象</returns>
        public static Dictionary<Int32, float> ParseMapIntFloat(this String strMap, Char keyValueSpriter = KEY_VALUE_SPRITER, Char mapSpriter = MAP_SPRITER)
        {
            var result = new Dictionary<Int32, float>();
            var strResult = ParseMap(strMap, keyValueSpriter, mapSpriter);
            foreach (var item in strResult)
            {
                int key;
                float value;
                if (int.TryParse(item.Key, out key) && float.TryParse(item.Value, out value))
                    result.Add(key, value);
                else
                    LoggerHelper.Warning(String.Format("Parse failure: {0}, {1}", item.Key, item.Value));
            }
            return result;
        }

        /// <summary>
        /// 将字典字符串转换为键类型为整型，值类型为字符串的字典对象。
        /// </summary>
        /// <param name="strMap">字典字符串</param>
        /// <param name="keyValueSpriter">键值分隔符</param>
        /// <param name="mapSpriter">字典项分隔符</param>
        /// <returns>字典对象</returns>
        public static Dictionary<Int32, String> ParseMapIntString(this String strMap, Char keyValueSpriter = KEY_VALUE_SPRITER, Char mapSpriter = MAP_SPRITER)
        {
            Dictionary<Int32, String> result = new Dictionary<Int32, String>();
            var strResult = ParseMap(strMap, keyValueSpriter, mapSpriter);
            foreach (var item in strResult)
            {
                int key;
                if (int.TryParse(item.Key, out key))
                    result.Add(key, item.Value);
                else
                    LoggerHelper.Warning(String.Format("Parse failure: {0}", item.Key));
            }
            return result;
        }

        /// <summary>
        /// 将字典字符串转换为键类型为字符串，值类型为单精度浮点数的字典对象。
        /// </summary>
        /// <param name="strMap">字典字符串</param>
        /// <param name="keyValueSpriter">键值分隔符</param>
        /// <param name="mapSpriter">字典项分隔符</param>
        /// <returns>字典对象</returns>
        public static Dictionary<String, float> ParseMapStringFloat(this String strMap, Char keyValueSpriter = KEY_VALUE_SPRITER, Char mapSpriter = MAP_SPRITER)
        {
            Dictionary<String, float> result = new Dictionary<String, float>();
            var strResult = ParseMap(strMap, keyValueSpriter, mapSpriter);
            foreach (var item in strResult)
            {
                float value;
                if (float.TryParse(item.Value, out value))
                    result.Add(item.Key, value);
                else
                    LoggerHelper.Warning(String.Format("Parse failure: {0}", item.Value));
            }
            return result;
        }

        /// <summary>
        /// 将字典字符串转换为键类型为字符串，值类型为整型的字典对象。
        /// </summary>
        /// <param name="strMap">字典字符串</param>
        /// <param name="keyValueSpriter">键值分隔符</param>
        /// <param name="mapSpriter">字典项分隔符</param>
        /// <returns>字典对象</returns>
        public static Dictionary<String, Int32> ParseMapStringInt(this String strMap, Char keyValueSpriter = KEY_VALUE_SPRITER, Char mapSpriter = MAP_SPRITER)
        {
            Dictionary<String, Int32> result = new Dictionary<String, Int32>();
            var strResult = ParseMap(strMap, keyValueSpriter, mapSpriter);
            foreach (var item in strResult)
            {
                int value;
                if (int.TryParse(item.Value, out value))
                    result.Add(item.Key, value);
                else
                    LoggerHelper.Warning(String.Format("Parse failure: {0}", item.Value));
            }
            return result;
        }

        /// <summary>
        /// 将字典字符串转换为键类型为 T，值类型为 U 的字典对象。
        /// </summary>
        /// <typeparam name="T">字典Key类型</typeparam>
        /// <typeparam name="U">字典Value类型</typeparam>
        /// <param name="strMap">字典字符串</param>
        /// <param name="keyValueSpriter">键值分隔符</param>
        /// <param name="mapSpriter">字典项分隔符</param>
        /// <returns>字典对象</returns>
        public static Dictionary<T, U> ParseMapAny<T, U>(this String strMap, Char keyValueSpriter = KEY_VALUE_SPRITER, Char mapSpriter = MAP_SPRITER)
        {
            var typeT = typeof(T);
            var typeU = typeof(U);
            var result = new Dictionary<T, U>();
            //先转为字典
            var strResult = ParseMap(strMap, keyValueSpriter, mapSpriter);
            foreach (var item in strResult)
            {
                try
                {
                    T key = (T)GetValue(item.Key, typeT);
                    U value = (U)GetValue(item.Value, typeU);

                    result.Add(key, value);
                }
                catch (Exception)
                {
                    LoggerHelper.Warning(String.Format("Parse failure: {0}, {1}", item.Key, item.Value));
                }
            }
            return result;
        }

        /// <summary>
        /// 将字典字符串转换为键类型与值类型都为字符串的字典对象。
        /// </summary>
        /// <param name="strMap">字典字符串</param>
        /// <param name="keyValueSpriter">键值分隔符</param>
        /// <param name="mapSpriter">字典项分隔符</param>
        /// <returns>字典对象</returns>
        public static Dictionary<String, String> ParseMap(this String strMap, Char keyValueSpriter = KEY_VALUE_SPRITER, Char mapSpriter = MAP_SPRITER)
        {
            Dictionary<String, String> result = new Dictionary<String, String>();
            if (String.IsNullOrEmpty(strMap))
            {
                return result;
            }

            var map = strMap.Split(mapSpriter);//根据字典项分隔符分割字符串，获取键值对字符串
            for (int i = 0; i < map.Length; i++)
            {
                if (String.IsNullOrEmpty(map[i]))
                {
                    continue;
                }

                var keyValuePair = map[i].Split(keyValueSpriter);//根据键值分隔符分割键值对字符串
                if (keyValuePair.Length == 2)
                {
                    if (!result.ContainsKey(keyValuePair[0]))
                        result.Add(keyValuePair[0], keyValuePair[1]);
                    else
                        LoggerHelper.Warning(String.Format("Key {0} already exist, index {1} of {2}.", keyValuePair[0], i, strMap));
                }
                else
                {
                    LoggerHelper.Warning(String.Format("KeyValuePair are not match: {0}, index {1} of {2}.", map[i], i, strMap));
                }
            }
            return result;
        }

        /// <summary>
        /// 将字典对象转换为字典字符串。
        /// </summary>
        /// <typeparam name="T">字典Key类型</typeparam>
        /// <typeparam name="U">字典Value类型</typeparam>
        /// <param name="map">字典对象</param>
        /// <returns>字典字符串</returns>
        public static String PackMap<T, U>(this IEnumerable<KeyValuePair<T, U>> map, Char keyValueSpriter = KEY_VALUE_SPRITER, Char mapSpriter = MAP_SPRITER)
        {
            if (map.Count() == 0)
                return "";
            else
            {
                StringBuilder sb = new StringBuilder();
                foreach (var item in map)
                {
                    sb.AppendFormat("{0}{1}{2}{3}", item.Key, keyValueSpriter, item.Value, mapSpriter);
                }
                return sb.ToString().Remove(sb.Length - 1, 1);
            }
        }

        #endregion

        #region 列表转换

        /// <summary>
        /// 将列表字符串转换为类型为 T 的列表对象。
        /// </summary>
        /// <typeparam name="T">列表值对象类型</typeparam>
        /// <param name="strList">列表字符串</param>
        /// <param name="listSpriter">数组分隔符</param>
        /// <returns>列表对象</returns>
        public static List<T> ParseListAny<T>(this String strList, Char listSpriter = LIST_SPRITER)
        {
            var type = typeof(T);
            var list = strList.ParseList(listSpriter);
            var result = new List<T>();
            foreach (var item in list)
            {
                result.Add((T)GetValue(item, type));
            }
            return result;
        }

        /// <summary>
        /// 将列表字符串转换为字符串的列表对象。
        /// </summary>
        /// <param name="strList">列表字符串</param>
        /// <param name="listSpriter">数组分隔符</param>
        /// <returns>列表对象</returns>
        public static List<String> ParseList(this String strList, Char listSpriter = LIST_SPRITER)
        {
            var result = new List<String>();
            if (String.IsNullOrEmpty(strList))
                return result;

            var trimString = strList.Trim();
            if (String.IsNullOrEmpty(strList))
            {
                return result;
            }
            var detials = trimString.Split(listSpriter);//.Substring(1, trimString.Length - 2)
            foreach (var item in detials)
            {
                if (!String.IsNullOrEmpty(item))
                    result.Add(item.Trim());
            }

            return result;
        }

        /// <summary>
        /// 将列表对象转换为列表字符串。
        /// </summary>
        /// <typeparam name="T">列表值对象类型</typeparam>
        /// <param name="list">列表对象</param>
        /// <param name="listSpriter">列表分隔符</param>
        /// <returns>列表字符串</returns>
        public static String PackList<T>(this List<T> list, Char listSpriter = LIST_SPRITER)
        {
            if (list.Count == 0)
                return "";
            else
            {
                StringBuilder sb = new StringBuilder();
                //sb.Append("[");
                foreach (var item in list)
                {
                    sb.AppendFormat("{0}{1}", item, listSpriter);
                }
                sb.Remove(sb.Length - 1, 1);
                //sb.Append("]");

                return sb.ToString();
            }

        }

        public static String PackArray<T>(this T[] array, Char listSpriter = LIST_SPRITER)
        {
            var list = new List<T>();
            list.AddRange(array);
            return PackList(list, listSpriter);
        }

        #endregion

        public static void SafeInvoke(this Action callback)
        {
            if (callback != null)
                callback();
        }

        public static void SafeInvoke<T>(this Action<T> callback, T arg1)
        {
            if (callback != null)
                callback(arg1);
        }

        public static void SafeInvoke<T, U>(this Action<T, U> callback, T arg1, U arg2)
        {
            if (callback != null)
                callback(arg1, arg2);
        }

        public static void SafeInvoke<T, U, V>(this Action<T, U, V> callback, T arg1, U arg2, V arg3)
        {
            if (callback != null)
                callback(arg1, arg2, arg3);
        }

        #region 类型转换

        /// <summary>
        /// 将字符串转换为对应类型的值。
        /// </summary>
        /// <param name="value">字符串值内容</param>
        /// <param name="type">值的类型</param>
        /// <returns>对应类型的值</returns>
        public static object GetValue(String value, Type type)
        {
            if (type == null)
                return null;
            else if (type == typeof(string))
                return value;
            else if (type == typeof(Int32))
                return Convert.ToInt32(Convert.ToDouble(value));
            else if (type == typeof(float))
                return float.Parse(value);
            else if (type == typeof(byte))
                return Convert.ToByte(Convert.ToDouble(value));
            else if (type == typeof(sbyte))
                return Convert.ToSByte(Convert.ToDouble(value));
            else if (type == typeof(UInt32))
                return Convert.ToUInt32(Convert.ToDouble(value));
            else if (type == typeof(Int16))
                return Convert.ToInt16(Convert.ToDouble(value));
            else if (type == typeof(Int64))
                return Convert.ToInt64(Convert.ToDouble(value));
            else if (type == typeof(UInt16))
                return Convert.ToUInt16(Convert.ToDouble(value));
            else if (type == typeof(UInt64))
                return Convert.ToUInt64(Convert.ToDouble(value));
            else if (type == typeof(double))
                return double.Parse(value);
            else if (type == typeof(bool))
            {
                if (value == "0")
                    return false;
                else if (value == "1")
                    return true;
                else
                    return bool.Parse(value);
            }
            else if (type.BaseType == typeof(Enum))
                return GetValue(value, Enum.GetUnderlyingType(type));
            else if (type == typeof(Vector3))
            {
                Vector3 result;
                ParseVector3(value, out result);
                return result;
            }
            else if (type == typeof(Quaternion))
            {
                Quaternion result;
                ParseQuaternion(value, out result);
                return result;
            }
            else if (type == typeof(Color))
            {
                Color result;
                ParseColor(value, out result);
                return result;
            }
            else if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Dictionary<,>))
            {
                Type[] types = type.GetGenericArguments();
                var map = ParseMap(value);
                var result = type.GetConstructor(Type.EmptyTypes).Invoke(null);
                foreach (var item in map)
                {
                    var key = GetValue(item.Key, types[0]);
                    var v = GetValue(item.Value, types[1]);
                    type.GetMethod("Add").Invoke(result, new object[] { key, v });
                }
                return result;
            }
            else if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>))
            {
                Type t = type.GetGenericArguments()[0];
                var list = ParseList(value);
                var result = type.GetConstructor(Type.EmptyTypes).Invoke(null);
                foreach (var item in list)
                {
                    var v = GetValue(item, t);
                    type.GetMethod("Add").Invoke(result, new object[] { v });
                }
                return result;
            }
            else
                return null;
        }

        /// <summary>
        /// 将指定格式(255, 255, 255, 255) 转换为 Color 
        /// </summary>
        /// <param name="_inputString"></param>
        /// <param name="result"></param>
        /// <returns>返回 true/false 表示是否成功</returns>
        public static bool ParseColor(string _inputString, out Color result)
        {
            string trimString = _inputString.Trim();
            result = Color.clear;
            if (trimString.Length < 9)
            {
                return false;
            }
            //if (trimString[0] != '(' || trimString[trimString.Length - 1] != ')')
            //{
            //    return false;
            //}
            try
            {
                string[] _detail = trimString.Split(LIST_SPRITER);//.Substring(1, trimString.Length - 2)
                if (_detail.Length != 4)
                {
                    return false;
                }
                result = new Color(float.Parse(_detail[0]) / 255, float.Parse(_detail[1]) / 255, float.Parse(_detail[2]) / 255, float.Parse(_detail[3]) / 255);
                return true;
            }
            catch (Exception e)
            {
                LoggerHelper.Error("Parse Color error: " + trimString + e.ToString());
                return false;
            }
        }

        /// <summary>
        /// 将指定格式(1.0, 2, 3.4) 转换为 Vector3 
        /// </summary>
        /// <param name="_inputString"></param>
        /// <param name="result"></param>
        /// <returns>返回 true/false 表示是否成功</returns>
        public static bool ParseVector3(string _inputString, out Vector3 result)
        {
            string trimString = _inputString.Trim();
            result = new Vector3();
            if (trimString.Length < 7)
            {
                return false;
            }
            //if (trimString[0] != '(' || trimString[trimString.Length - 1] != ')')
            //{
            //    return false;
            //}
            try
            {
                string[] _detail = trimString.Split(LIST_SPRITER);//.Substring(1, trimString.Length - 2)
                if (_detail.Length != 3)
                {
                    return false;
                }
                result.x = float.Parse(_detail[0]);
                result.y = float.Parse(_detail[1]);
                result.z = float.Parse(_detail[2]);
                return true;
            }
            catch (Exception e)
            {
                LoggerHelper.Error("Parse Vector3 error: " + trimString + e.ToString());
                return false;
            }
        }

        /// <summary>
        /// 将指定格式(1.0, 2, 3.4) 转换为 Vector3 
        /// </summary>
        /// <param name="_inputString"></param>
        /// <param name="result"></param>
        /// <returns>返回 true/false 表示是否成功</returns>
        public static bool ParseQuaternion(string _inputString, out Quaternion result)
        {
            string trimString = _inputString.Trim();
            result = new Quaternion();
            if (trimString.Length < 9)
            {
                return false;
            }
            //if (trimString[0] != '(' || trimString[trimString.Length - 1] != ')')
            //{
            //    return false;
            //}
            try
            {
                string[] _detail = trimString.Split(LIST_SPRITER);//.Substring(1, trimString.Length - 2)
                if (_detail.Length != 4)
                {
                    return false;
                }
                result.x = float.Parse(_detail[0]);
                result.y = float.Parse(_detail[1]);
                result.z = float.Parse(_detail[2]);
                result.w = float.Parse(_detail[3]);
                return true;
            }
            catch (Exception e)
            {
                LoggerHelper.Error("Parse Quaternion error: " + trimString + e.ToString());
                return false;
            }
        }

        /// <summary>
        /// 替换字符串中的子字符串。
        /// </summary>
        /// <param name="input">原字符串</param>
        /// <param name="oldValue">旧子字符串</param>
        /// <param name="newValue">新子字符串</param>
        /// <param name="count">替换数量</param>
        /// <param name="startAt">从第几个字符开始</param>
        /// <returns>替换后的字符串</returns>
        public static String ReplaceFirst(this string input, string oldValue, string newValue, int startAt = 0)
        {
            int pos = input.IndexOf(oldValue, startAt);
            if (pos < 0)
            {
                return input;
            }
            return string.Concat(input.Substring(0, pos), newValue, input.Substring(pos + oldValue.Length));
        }

        /// <summary>
        /// 替换字符串中的子字符串。
        /// </summary>
        /// <param name="input">原字符串</param>
        /// <param name="oldValue">旧子字符串</param>
        /// <param name="newValue">新子字符串</param>
        /// <param name="count">替换数量</param>
        /// <param name="startAt">从第几个字符开始</param>
        /// <returns>替换后的字符串</returns>
        public static String ReplaceLast(this string input, string oldValue, string newValue)
        {
            int pos = input.LastIndexOf(oldValue);
            if (pos < 0)
            {
                return input;
            }
            return string.Concat(input.Substring(0, pos), newValue, input.Substring(pos + oldValue.Length));
        }

        #endregion

        #region Lua table转换

        #region 实体转换

        /// <summary>
        /// 将 Lua table 实体转换为 实体
        /// </summary>
        /// <typeparam name="T">Lua table数据对应实体类型</typeparam>
        /// <param name="luaTable">Lua table 实体</param>
        /// <param name="result">实体对象</param>
        /// <returns>返回 true/false 表示是否成功</returns>
        public static bool ParseLuaTable<T>(LuaTable luaTable, out T result)
        {
            object obj;
            if (ParseLuaTable(luaTable, typeof(T), out obj))
            {
                result = (T)obj;
                return true;
            }
            else
            {
                result = default(T);
                return false;
            }
        }

        /// <summary>
        /// 将 Lua table 实体转换为 实体
        /// </summary>
        /// <param name="luaTable">Lua table 实体</param>
        /// <param name="type">Lua table数据对应实体类型</param>
        /// <param name="result">实体对象</param>
        /// <returns>返回 true/false 表示是否成功</returns>
        public static bool ParseLuaTable(LuaTable luaTable, Type type, out object result)
        {
            result = null;
            if (type.IsGenericType)
            {
                if (type.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                {
                    DecodeDic(luaTable, type, out result);
                }
                else if (type.GetGenericTypeDefinition() == typeof(List<>))
                {
                    DecodeList(luaTable, type, out result);
                }
            }
            else
            {
                DecodeEntity(luaTable, type, out result);
            }
            return true;
        }

        private static bool DecodeList(LuaTable luaTable, Type type, out object result)
        {
            result = type.GetConstructor(Type.EmptyTypes).Invoke(null);
            Type valueType = type.GetGenericArguments()[0];
            foreach (var item in luaTable)
            {
                object obj = null;
                if (luaTable.IsLuaTable(item.Key))
                    ParseLuaTable(item.Value as LuaTable, valueType, out obj);
                else
                    obj = GetValue(item.Value.ToString(), valueType);
                type.GetMethod("Add").Invoke(result, new object[] { obj });
            }
            return true;
        }

        private static bool DecodeDic(LuaTable luaTable, Type type, out object result)
        {
            result = type.GetConstructor(Type.EmptyTypes).Invoke(null);
            Type valueType = type.GetGenericArguments()[1];
            foreach (var item in luaTable)
            {
                object obj = null;
                if (luaTable.IsLuaTable(item.Key))
                {
                    ParseLuaTable(item.Value as LuaTable, valueType, out obj);
                }
                else
                {
                    obj = GetValue(item.Value.ToString(), valueType);
                }
                object oldKey = luaTable.IsKeyString(item.Key) ? item.Key : (object)Int32.Parse(item.Key);
                type.GetMethod("Add").Invoke(result, new object[] { oldKey, obj });
            }
            return true;
        }

        private static bool DecodeDic123(LuaTable luaTable, Type type, out object result)
        {
            result = type.GetConstructor(Type.EmptyTypes).Invoke(null);
            Type valueType = type.GetGenericArguments()[1];
            PropertyInfo[] pis = valueType.GetProperties();
            int nPropCount = pis.Length;
            for (int i = 0; i < nPropCount; i++)
            {
                object value;
                if (luaTable.TryGetValue(i.ToString(), out value))
                {
                    pis[i].GetSetMethod().Invoke(result, new object[] { value });
                }
            }
            return true;
        }

        private static bool DecodeEntity(LuaTable luaTable, Type type, out object result)
        {
            var constructor = type.GetConstructor(Type.EmptyTypes);
            if (constructor == null)
            {
                LoggerHelper.Error(String.Format("type {0} can not create an entity. luatable: {1}", type.Name, luaTable.ToString()));
                result = null;
                return false;
            }
            result = type.GetConstructor(Type.EmptyTypes).Invoke(null);
            //var props = type.GetProperties();//找出实体所有属性

            //------------by lyx  读取字段字典------------------//
            FieldInfo _info = type.GetField("s_dicProp", BindingFlags.Static | BindingFlags.Public);
            object _obj = _info.GetValue(null);
            Dictionary<object, object> _dicKey = (_obj as Dictionary<object, object>);
            //------------by lyx  end------------------//

            foreach (var item in luaTable)
            {
                //var s = item;
                //var x = s.Key + " " + s.Value.ToString();
                //var y = Int32.Parse(s.Value.ToString());

                PropertyInfo valueType = null;

                //------------by lyx  使用字符串来解析------------------//
                if (luaTable.IsKeyString(item.Key))
                {
                    valueType = type.GetProperty(item.Key);
                }
                else
                {
                    object oldKey = luaTable.IsKeyString(item.Key) ? item.Key : (object)Int32.Parse(item.Key);
                    object newKey = oldKey;
                    if (!_dicKey.TryGetValue(oldKey, out newKey))
                    {
                        LoggerHelper.Warning("error luaTable:" + luaTable.ToString());
                        LoggerHelper.Error("DecodeEntity Error! " + "oldkey:" + oldKey + "newkey:" + newKey);
                    }
                    valueType = type.GetProperty(newKey.ToString());

                }

                //if (luaTable.IsKeyString(item.Key))
                //{
                //    valueType = type.GetProperty(item.Key);
                //}
                //else
                //{
                //    var i = int.Parse(item.Key) - 1;//Lua Table 的键从1开始
                //    if (i < props.Length)
                //        valueType = props[i];
                //}

                //------------by lyx  使用字符串来解析------------------//

                if (valueType != null)
                {
                    //Debug.Log("valueType:" + valueType.Name);
                    object obj = null;
                    if (luaTable.IsLuaTable(item.Key))
                        ParseLuaTable(item.Value as LuaTable, valueType.PropertyType, out obj);
                    else
                        obj = GetValue(item.Value.ToString(), valueType.PropertyType);
                    valueType.SetValue(result, obj, null);
                }
                else
                {
                    LoggerHelper.Info("type:" + type.Name + " valueType:" + valueType.Name + " 找不到!");
                }
            }
            return true;
        }

        /// <summary>
        /// 将 Lua table 转换为 实体
        /// </summary>
        /// <param name="inputString">Lua table字符串</param>
        /// <param name="type">Lua table数据对应实体类型</param>
        /// <param name="result">实体对象</param>
        /// <returns>返回 true/false 表示是否成功</returns>
        public static bool ParseLuaTable(string inputString, Type type, out object result)
        {
            string trimString = inputString.Trim();
            if (trimString[0] != '{' || trimString[trimString.Length - 1] != '}')
            {
                result = null;
                return false;
            }
            else if (trimString.Length == 2)
            {
                result = type.GetConstructor(Type.EmptyTypes).Invoke(null);
                return true;
            }
            int index = 0;
            return DecodeDic(trimString, type, ref index, out result);
        }

        /// <summary>
        /// 解析列表类型 Lua table 值。
        /// </summary>
        /// <param name="inputString">Lua table字符串</param>
        /// <param name="type">Lua table数据对应实体类型</param>
        /// <param name="index">字符串偏移量</param>
        /// <param name="result">列表对象</param>
        /// <returns>返回 true/false 表示是否成功</returns>
        private static bool DecodeDic(string inputString, Type type, ref int index, out object result)
        {
            if (!type.IsGenericType || type.GetGenericTypeDefinition() != typeof(Dictionary<,>))
            {
                result = null;
                LoggerHelper.Error("Parse LuaTable error: type is not Dictionary: " + type);
                return false;
            }
            result = type.GetConstructor(Type.EmptyTypes).Invoke(null);
            if (!WaitChar(inputString, '{', ref index))
            {
                return false;
            }
            try
            {
                while (index < inputString.Length)
                {
                    string key;
                    bool isString;
                    object value;
                    DecodeKey(inputString, ref index, out key, out isString);//匹配键
                    WaitChar(inputString, '=', ref index);//匹配键值对分隔符
                    var flag = DecodeEntity(inputString, type.GetGenericArguments()[1], ref index, out value);//转换实体
                    if (flag)
                    {
                        type.GetMethod("Add").Invoke(result, new object[] { isString ? key : (object)Int32.Parse(key), value });
                    }
                    if (!WaitChar(inputString, ',', ref index))
                        break;
                }
                WaitChar(inputString, '}', ref index);
                return true;
            }
            catch (Exception e)
            {
                LoggerHelper.Error("Parse LuaTable error: " + inputString + e.ToString());
                return false;
            }
        }

        /// <summary>
        /// 解析实体类型 Lua table 值。
        /// </summary>
        /// <param name="inputString">Lua table字符串</param>
        /// <param name="type">Lua table数据对应实体类型</param>
        /// <param name="index">字符串偏移量</param>
        /// <param name="result">实体对象</param>
        /// <returns>返回 true/false 表示是否成功</returns>
        private static bool DecodeEntity(string inputString, Type type, ref int index, out object result)
        {
            result = type.GetConstructor(Type.EmptyTypes).Invoke(null);
            if (!WaitChar(inputString, '{', ref index))
            {
                return false;
            }
            try
            {
                var props = type.GetProperties();//找出实体所有属性
                while (index < inputString.Length)
                {
                    string key;
                    bool isString;
                    object value;
                    PropertyInfo valueType = null;
                    DecodeKey(inputString, ref index, out key, out isString);//获取键
                    if (isString)//如果键为字符串，则按名称获取属性
                    {
                        valueType = type.GetProperty(key as string);
                    }
                    else//如果键为整型，则按序号获取属性
                    {
                        var i = int.Parse(key) - 1;//Lua Table 的键从1开始
                        if (i < props.Length)
                        {
                            valueType = props[i];
                        }
                    }

                    WaitChar(inputString, '=', ref index);
                    if (valueType != null)
                    {
                        var flag = DecodeValue(inputString, valueType.PropertyType, ref index, out value);//根据类型转换并赋值
                        if (flag)
                        {
                            valueType.SetValue(result, value, null);
                        }
                    }
                    if (!WaitChar(inputString, ',', ref index))
                        break;
                }
                WaitChar(inputString, '}', ref index);
                return true;
            }
            catch (Exception e)
            {
                LoggerHelper.Error("Parse LuaTable error: " + inputString + e.ToString());
                return false;
            }
        }

        /// <summary>
        /// 解析值类型 Lua table 值。
        /// </summary>
        /// <param name="inputString">Lua table字符串</param>
        /// <param name="type">Lua table数据对应实体类型</param>
        /// <param name="index">字符串偏移量</param>
        /// <param name="result">值</param>
        /// <returns>返回 true/false 表示是否成功</returns>
        private static bool DecodeValue(string inputString, Type type, ref int index, out object value)
        {
            var firstChar = inputString[index];
            if (firstChar == 's')
            {
                //key is string
                var szLen = inputString.Substring(index + 1, 3);
                var lenth = Int32.Parse(szLen);
                if (lenth > 0)
                {
                    index += 4;
                    value = inputString.Substring(index, lenth);
                    index += lenth;
                    return true;
                }
                value = "";
                LoggerHelper.Error("Decode Lua Table Value Error: " + index + " " + inputString);
                return false;
            }
            else if (firstChar == '{')//如果第一个字符为花括号，表示接下来的内容为列表或实体类型
            {
                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                {
                    return DecodeDic(inputString, type, ref index, out value);
                }
                else
                    return DecodeEntity(inputString, type, ref index, out value);
            }
            else
            {
                //key is number
                var i = index;
                while (++index < inputString.Length)
                {
                    if (inputString[index] == ',' || inputString[index] == '}')
                    {
                        if (index > i)
                        {
                            var strValue = inputString.Substring(i, index - i);
                            value = GetValue(strValue, type);
                            return true;
                        }
                    }
                }
                value = GetValue("0", type);
                LoggerHelper.Error("Decode Lua Table Value Error: " + index + " " + inputString);
                return false;
            }
        }

        #endregion

        #endregion

        #region 字节操作

        /// <summary>
        /// 填充数据长度头。
        /// </summary>
        /// <param name="srcData">源二进制数组</param>
        /// <returns>填充二进制数组长度到头部的数据</returns>
        public static Byte[] FillLengthHead(Byte[] srcData)
        {
            return FillLengthHead(srcData, (UInt16)srcData.Length);
        }

        /// <summary>
        /// 填充数据长度头。
        /// </summary>
        /// <param name="srcData">源二进制数组</param>
        /// <param name="length">源二进制数组数据长度</param>
        /// <returns>填充二进制数组长度到头部的数据</returns>
        public static Byte[] FillLengthHead(Byte[] srcData, UInt16 length)
        {
            Byte[] lengthByteArray = BitConverter.GetBytes(length);//将字符串长度转换为二进制数组
            //Array.Reverse(lengthByteArray);
            Byte[] result = new Byte[length + lengthByteArray.Length];//申请存放字符串长度和字符串内容的空间
            Buffer.BlockCopy(lengthByteArray, 0, result, 0, lengthByteArray.Length);//将长度的二进制数组拷贝到目标空间
            Buffer.BlockCopy(srcData, 0, result, lengthByteArray.Length, length);//将字符串的二进制数组拷贝到目标空间
            return result;
        }

        #endregion

        #region Lua table转换

        /// <summary>
        /// 转换复杂类型的对象到LuaTable，不支持基础类型直接转换。
        /// </summary>
        /// <param name="target"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool PackLuaTable(object target, out LuaTable result)
        {
            var type = target.GetType();
            if (type == typeof(LuaTable))
            {
                result = target as LuaTable;
                return true;
            }
            if (type.IsGenericType)
            {//容器类型
                //目前只支持列表与字典的容器类型转换
                if (type.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                {
                    return PackLuaTable(target as IDictionary, out result);
                }
                else
                {
                    return PackLuaTable(target as IList, out result);
                }
            }
            else
            {//实体类型
                result = new LuaTable();
                try
                {
                    var props = type.GetProperties(~BindingFlags.Static);
                    for (int i = 0; i < props.Length; i++)
                    {
                        var prop = props[i];
                        if (IsBaseType(prop.PropertyType))
                            result.Add(i + 1, prop.GetGetMethod().Invoke(target, null));
                        else
                        {
                            LuaTable lt;
                            var value = prop.GetGetMethod().Invoke(target, null);
                            var flag = PackLuaTable(value, out lt);
                            if (flag)
                                result.Add(i + 1, lt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerHelper.Error("PackLuaTable entity error: " + ex.Message);
                }
            }
            return true;
        }

        /// <summary>
        /// 转换列表类型的对象到LuaTable。
        /// </summary>
        /// <param name="target"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool PackLuaTable(IList target, out LuaTable result)
        {
            Type[] types = target.GetType().GetGenericArguments();
            result = new LuaTable();
            try
            {
                for (int i = 0; i < target.Count; i++)
                {
                    if (IsBaseType(types[0]))
                    {
                        if (types[0] == typeof(bool))
                            result.Add(i + 1, (bool)target[i] ? 1 : 0);
                        else
                            result.Add(i + 1, target[i]);
                    }
                    else
                    {
                        LuaTable value;
                        var flag = PackLuaTable(target[i], out value);
                        if (flag)
                            result.Add(i + 1, value);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("PackLuaTable list error: " + ex.Message);
            }
            return true;
        }

        /// <summary>
        /// 转换字典类型的对象到LuaTable。
        /// </summary>
        /// <param name="target"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool PackLuaTable(IDictionary target, out LuaTable result)
        {
            Type[] types = target.GetType().GetGenericArguments();
            result = new LuaTable();
            try
            {
                foreach (DictionaryEntry item in target)
                {
                    if (IsBaseType(types[1]))
                    {
                        object value;
                        if (types[1] == typeof(bool))//判断值是否布尔类型，是则做特殊转换
                            value = (bool)item.Value ? 1 : 0;
                        else
                            value = item.Value;

                        if (types[0] == typeof(int))//判断键是否为整型，是则标记键为整型，转lua table字符串时有用
                            result.Add(item.Key.ToString(), false, value);
                        else
                            result.Add(item.Key.ToString(), value);
                    }
                    else
                    {
                        LuaTable value;
                        var flag = PackLuaTable(item.Value, out value);
                        if (flag)
                        {
                            if (types[0] == typeof(int))
                                result.Add(item.Key.ToString(), false, value);
                            else
                                result.Add(item.Key.ToString(), value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("PackLuaTable dictionary error: " + ex.Message);
            }
            return true;
        }

        /// <summary>
        /// 判断类型是否为基础类型。
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static bool IsBaseType(Type type)
        {
            if (type == typeof(byte) || type == typeof(sbyte)
                || type == typeof(short) || type == typeof(ushort)
                || type == typeof(int) || type == typeof(uint)
                || type == typeof(Int64) || type == typeof(UInt64)
                || type == typeof(float) || type == typeof(double)
                || type == typeof(string) || type == typeof(bool))
                return true;
            else
                return false;
        }

        /// <summary>
        /// 将 Lua table 打包成字符串
        /// </summary>
        /// <param name="luaTable"></param>
        /// <returns></returns>
        public static String PackLuaTable(LuaTable luaTable)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append('{');
            if (luaTable != null)
            {
                foreach (var item in luaTable)
                {
                    //拼键
                    if (luaTable.IsKeyString(item.Key))
                        sb.Append(EncodeString(item.Key));
                    else
                        sb.Append(item.Key);
                    sb.Append('=');

                    //拼值
                    var valueType = item.Value.GetType();
                    if (valueType == typeof(String))
                        sb.Append(EncodeString(item.Value as String));
                    else if (valueType == typeof(LuaTable))
                        sb.Append(PackLuaTable(item.Value as LuaTable));
                    else
                        sb.Append(item.Value.ToString());
                    sb.Append(',');
                }
                if (luaTable.Count != 0)//若lua table为空则不删除
                    sb.Remove(sb.Length - 1, 1);//去掉最后一个逗号
            }
            sb.Append('}');
            return sb.ToString();
        }

        /// <summary>
        /// 将字符串转成 Lua table 可识别的格式。
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static String EncodeString(String value)
        {
            if (value.Length > 999)
                LoggerHelper.Warning("PackLuaTable EncodeString overflow: " + value);
            return String.Concat('s', Encoding.UTF8.GetBytes(value).Length.ToString("000"), value);
        }

        /// <summary>
        /// 将 Lua table 字符串转换为 Lua table 实体
        /// </summary>
        /// <param name="inputString">Lua table 字符串</param>
        /// <param name="result">实体对象</param>
        /// <returns>返回 true/false 表示是否成功</returns>
        public static bool ParseLuaTable(string inputString, out LuaTable result)
        {
            string trimString = inputString.Trim();
            if (trimString[0] != '{' || trimString[trimString.Length - 1] != '}')
            {
                result = null;
                return false;
            }
            else if (trimString.Length == 2)
            {
                result = new LuaTable();
                return true;
            }

            var index = 0;
            object obj;
            var flag = DecodeLuaTable(inputString, ref index, out obj);
            if (flag)
                result = obj as LuaTable;
            else
                result = null;
            return flag;
        }

        /// <summary>
        /// 将 Byte流 转换为 Lua table 实体
        /// </summary>
        /// <param name="inputString">Byte[]</param>
        /// <param name="result">实体对象</param>
        /// <returns>返回 true/false 表示是否成功</returns>
        public static bool ParseLuaTable(byte[] inputString, out LuaTable result)
        {
            //inputString = Encoding.UTF8.GetBytes(str.Trim());
            //string trimString = inputString.Trim();
            if (inputString[0] != '{' || inputString[inputString.Length - 1] != '}')
            {
                result = null;
                return false;
            }
            else if (inputString.Length == 2)
            {
                result = new LuaTable();
                return true;
            }

            var index = 0;
            object obj;
            var flag = DecodeLuaTable(inputString, ref index, out obj);
            if (flag)
                result = obj as LuaTable;
            else
                result = null;
            return flag;
        }
        private static bool DecodeLuaTable(string inputString, ref int index, out object result)
        {
            var luaTable = new LuaTable();
            result = luaTable;
            if (!WaitChar(inputString, '{', ref index))
            {
                return false;
            }
            try
            {
                if (WaitChar(inputString, '}', ref index))//如果下一个字符为右大括号表示为空Lua table
                    return true;
                while (index < inputString.Length)
                {
                    string key;
                    bool isString;
                    object value;
                    DecodeKey(inputString, ref index, out key, out isString);//匹配键
                    WaitChar(inputString, '=', ref index);//匹配键值对分隔符
                    var flag = DecodeLuaValue(inputString, ref index, out value);//转换实体
                    if (flag)
                    {
                        luaTable.Add(key, isString, value);
                    }
                    if (!WaitChar(inputString, ',', ref index))
                        break;
                }
                WaitChar(inputString, '}', ref index);
                return true;
            }
            catch (Exception e)
            {
                LoggerHelper.Error("Parse LuaTable error: " + inputString + e.ToString());
                return false;
            }
        }
        private static bool DecodeLuaTable(byte[] inputString, ref int index, out object result)
        {
            var luaTable = new LuaTable();
            result = luaTable;
            if (!WaitChar(inputString, '{', ref index))
            {
                return false;
            }
            try
            {
                if (WaitChar(inputString, '}', ref index))//如果下一个字符为右大括号表示为空Lua table
                    return true;
                while (index < inputString.Length)
                {
                    string key;
                    bool isString;
                    object value;
                    DecodeKey(inputString, ref index, out key, out isString);//匹配键
                    WaitChar(inputString, '=', ref index);//匹配键值对分隔符
                    var flag = DecodeLuaValue(inputString, ref index, out value);//转换实体
                    if (flag)
                    {
                        luaTable.Add(key, isString, value);
                    }
                    if (!WaitChar(inputString, ',', ref index))
                        break;
                }
                WaitChar(inputString, '}', ref index);
                return true;

            }
            catch (Exception e)
            {
                LoggerHelper.Error("Parse LuaTable error: " + inputString + e.ToString());
                return false;
            }
        }
        private static bool DecodeLuaValue(string inputString, ref int index, out object value)
        {
            var firstChar = inputString[index];
            if (firstChar == 's')
            {
                //value is string
                var szLen = inputString.Substring(index + 1, 3);
                var lenth = Int32.Parse(szLen);
                index += 4;
                if (lenth > 0)
                {
                    value = inputString.Substring(index, lenth);
                    index += lenth;
                    return true;
                }
                else
                {
                    value = "";
                    return true;
                }
                //LoggerHelper.Error("Decode Lua Table Value Error: " + index + " " + inputString);
                //return false;
            }
            else if (firstChar == '{')//如果第一个字符为花括号，表示接下来的内容为列表或实体类型
            {
                return DecodeLuaTable(inputString, ref index, out value);
            }
            else
            {
                //value is number
                var i = index;
                while (++index < inputString.Length)
                {
                    if (inputString[index] == ',' || inputString[index] == '}')
                    {
                        if (index > i)
                        {
                            value = inputString.Substring(i, index - i);
                            return true;
                        }
                    }
                }
                LoggerHelper.Error("Decode Lua Table Value Error: " + index + " " + inputString);
                value = null;
                return false;
            }
        }

        public static bool DecodeKey(byte[] inputString, ref int index, out string key, out bool isString)
        {
            if (inputString[index] == 's')
            {
                //key is string
                var szLen = Encoding.UTF8.GetString(inputString, index + 1, 3);
                var length = Int32.Parse(szLen);
                if (length > 0)
                {
                    index += 4;
                    key = Encoding.UTF8.GetString(inputString, index, length);
                    isString = true;
                    index += length;
                    return true;
                }
                key = "";
                isString = true;
                LoggerHelper.Error("Decode Lua Table Key Error: " + index + " " + inputString);
                return false;
            }
            else
            {
                //key is number
                int offset = 0;
                while (index + offset < inputString.Length && inputString[index + offset] != '=')
                {
                    offset++;
                }

                if (offset > 0)
                {
                    key = Encoding.UTF8.GetString(inputString, index, offset);
                    //value = Int32.Parse(strValue);
                    index = index + offset;
                    isString = false;
                    return true;
                }
                else
                {
                    key = "-1";
                    isString = false;
                    LoggerHelper.Error("Decode Lua Table Key Error: " + index + " " + inputString);
                    return false;
                }
            }
        }
        private static bool DecodeLuaValue(byte[] inputString, ref int index, out object value)
        {
            var firstChar = inputString[index];
            if (firstChar == 's')
            {
                //value is string
                var szLen = Encoding.UTF8.GetString(inputString, index + 1, 3);
                var length = Int32.Parse(szLen);
                index += 4;
                if (length > 0)
                {
                    value = Encoding.UTF8.GetString(inputString, index, length);
                    index += length;
                    return true;
                }
                else
                {
                    value = "";
                    return true;
                }
                //LoggerHelper.Error("Decode Lua Table Value Error: " + index + " " + inputString);
                //return false;
            }
            else if (firstChar == '{')//如果第一个字符为花括号，表示接下来的内容为列表或实体类型
            {
                return DecodeLuaTable(inputString, ref index, out value);
            }
            else
            {
                //value is number
                var i = index;
                while (++index < inputString.Length)
                {
                    if (inputString[index] == ',' || inputString[index] == '}')
                    {
                        if (index > i)
                        {
                            value = Encoding.UTF8.GetString(inputString, i, index - i);
                            return true;
                        }
                    }
                }
                LoggerHelper.Error("Decode Lua Table Value Error: " + index + " " + inputString);
                value = null;
                return false;
            }
        }
        /// <summary>
        /// 解析 Lua table 的键。
        /// </summary>
        /// <param name="inputString">Lua table字符串</param>
        /// <param name="index">字符串偏移量</param>
        /// <param name="result">键</param>
        /// <returns>返回 true/false 表示是否成功</returns>
        public static bool DecodeKey(string inputString, ref int index, out string key, out bool isString)
        {
            if (inputString[index] == 's')
            {
                //key is string
                var szLen = inputString.Substring(index + 1, 3);
                var lenth = Int32.Parse(szLen);
                if (lenth > 0)
                {
                    index += 4;
                    key = inputString.Substring(index, lenth);
                    isString = true;
                    index += lenth;
                    return true;
                }
                key = "";
                isString = true;
                LoggerHelper.Error("Decode Lua Table Key Error: " + index + " " + inputString);
                return false;
            }
            else
            {
                //key is number
                var szLen = inputString.IndexOf('=', index);
                if (szLen > -1)
                {
                    var lenth = szLen - index;
                    key = inputString.Substring(index, lenth);
                    //value = Int32.Parse(strValue);
                    index = szLen;
                    isString = false;
                    return true;
                }
                else
                {
                    key = "-1";
                    isString = false;
                    LoggerHelper.Error("Decode Lua Table Key Error: " + index + " " + inputString);
                    return false;
                }
            }
        }



        /// <summary>
        /// 判断下个字符是否为期望的字符。
        /// </summary>
        /// <param name="inputString">Lua table字符串</param>
        /// <param name="c">期望的字符</param>
        /// <param name="index">字符串偏移量</param>
        /// <returns>返回 true/false 表示是否为期望的字符</returns>
        public static bool WaitChar(string inputString, char c, ref int index)
        {
            var szLen = inputString.IndexOf(c, index);
            if (szLen == index)
            {
                index++;
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 判断下个字符是否为期望的字符。
        /// </summary>
        /// <param name="inputString">Lua table字符串</param>
        /// <param name="c">期望的字符</param>
        /// <param name="index">字符串偏移量</param>
        /// <returns>返回 true/false 表示是否为期望的字符</returns>
        public static bool WaitChar(byte[] inputString, char c, ref int index)
        {
            if ((byte)c == inputString[index])
            {
                index++;
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region state
        public static ulong BitSet(ulong data, int nBit)
        {
            if (nBit >= 0 && nBit < (int)sizeof(ulong) * 8)
            {
                data |= (ulong)(1 << nBit);
            }

            return data;
        }

        public static ulong BitReset(ulong data, int nBit)
        {
            if (nBit >= 0 && nBit < (int)sizeof(ulong) * 8)
            {
                data &= (ulong)(~(1 << nBit));
            };

            return data;
        }

        public static int BitTest(ulong data, int nBit)
        {
            int nRet = 0;
            if (nBit >= 0 && nBit < (int)sizeof(ulong) * 8)
            {
                data &= (ulong)(1 << nBit);
                if (data != 0) nRet = 1;
            }
            return nRet;
        }
        #endregion

        #region 几何相关

        public static void CircleXYByAngle(float angle, Vector3 O, Vector3 A, out Vector3 rnt)
        {

            float r = Vector3.Distance(O, A);
            //rnt = new Vector3();
            rnt.y = A.y;
            rnt.x = r * (float)Math.Cos(angle) + O.x;
            rnt.z = r * (float)Math.Sin(angle) + O.z;



            //return rnt;
        }

        #endregion

    }
}