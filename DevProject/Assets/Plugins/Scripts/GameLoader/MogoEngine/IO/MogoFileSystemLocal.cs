﻿#region 模块信息
/*----------------------------------------------------------------
// Copyright (C) 2016 广州，雷神
//
// 模块名：MogoFileSystem
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.6.15
// 模块描述：自定义文件系统。
//----------------------------------------------------------------*/
#endregion

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using GameLoader.Config;
using GameLoader.Utils;
using System.IO;

namespace GameLoader.IO
{
#if !UNITY_WEBPLAYER
    /// <summary>
    /// 自定义文件系统。
    /// </summary>
    public class MogoFileSystemLocal : MogoFileSystem
    {
        #region 属性
        /// <summary>
        /// 文件读写处理流。
        /// </summary>
        private FileStream m_fileStream;
        
        #endregion

        #region 公有方法

        public override void Init()
        {
            lock (m_locker)
            {
                int beginTime = Environment.TickCount;
                SetDefaultFullName();                   
                LoggerHelper.Debug(string.Format("初始化MogoFileSystem:{0} [Start]", MogoFileSystem.Instance.FileFullName));
                Init(m_fileFullName);
                LoggerHelper.Debug(string.Format("初始化MogoFileSystem [OK] fileIndex.count:{0} cost:{1}ms", m_fileIndexes.Count, Environment.TickCount - beginTime));
                Close();
            }
        }

        /// <summary>
        /// 设置pkg文件默认路径
        /// </summary>
        private void SetDefaultFullName()
        {
            if (string.IsNullOrEmpty(m_fileFullName)) m_fileFullName = SystemConfig.RuntimeResourcePath + FILE_NAME;
        }

        /// <summary>
        /// 打开文件读写。
        /// </summary>
        public override void Init(string fileName)
        {
            if (m_fileStream != null) return;
            if (!File.Exists(fileName))
            {
                LoggerHelper.Debug(string.Format("[文件系统] 文件不存在:{0}", m_fileFullName));
                var dir = Path.GetDirectoryName(fileName);
                if (!string.IsNullOrEmpty(dir) && !Directory.Exists(dir)) Directory.CreateDirectory(dir);
                m_fileStream = File.Create(fileName);
                
            }
            else
            {
                LoggerHelper.Debug(string.Format("[文件系统] 文件存在:{0}", m_fileFullName));
                var fileInfo = new FileInfo(fileName);
                var fileSize = fileInfo.Length;
                m_fileStream = File.Open(fileName, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                GetIndexInfo(fileSize);
            }
            m_isClosed = false;
        }

        /// <summary>
        /// 初始化内存读写。
        /// </summary>
        public override void Init(byte[] data)
        { 
            
        }

        /// <summary>
        /// 打开文件读写。
        /// </summary>
        public override void Open()
        {
            if (m_fileStream == null)
            {
                SetDefaultFullName();
                //LoggerHelper.Info(string.Format("FileSystem:{0} [开始打开]", m_fileFullName));
                try
                {
                    m_fileStream = File.Open(m_fileFullName, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                }
                catch (Exception ex)
                {
                    LoggerHelper.Error(string.Format("[FileSystem] 文件:{0} 打开[Fail],reason:{1}", m_fileFullName, ex.StackTrace));
                }
            }
            //else 
            //{
                //LoggerHelper.Info(string.Format("FileSystem:{0} [已打开]", m_fileFullName));
            //}
        }

        /// <summary>
        /// 保存索引信息。
        /// </summary>
        public override void SaveIndexInfo()
        {
            lock (m_locker)
            {
                uint indexPosition = (uint)m_fileStream.Position;
                var indexInfos = new List<Byte>();
                foreach (var item in m_fileIndexes)
                {
                    indexInfos.AddRange(item.Value.GetEncodeData());
                }
                foreach (var item in m_deletedIndexes)
                {
                    indexInfos.AddRange(item.GetEncodeData());
                }
                var indexInfo = indexInfos.ToArray();
                if (indexInfo.Length > 0)
                    indexInfo = DESCrypto.Encrypt(indexInfo, m_number);//加密索引数据

                m_fileStream.Position = m_packageInfo.IndexOffset;//将文件偏移重置为索引偏移起始位置
                m_fileStream.Write(indexInfo, 0, indexInfo.Length);
                m_fileStream.Flush();
                SavePackageInfo(m_fileStream);
                m_fileStream.Position = indexPosition;
            }
        }

        /// <summary>
        /// 获取并备份索引信息。
        /// </summary>
        public override void GetAndBackUpIndexInfo()
        {
            var fileInfo = new FileInfo(m_fileFullName);
            var fileSize = fileInfo.Length;
            GetIndexInfo(fileSize, true);
        }

        /// <summary>
        /// 关闭文件读写。
        /// </summary>
        public override void Close()
        {
            if (m_fileStream != null)
            {
                m_fileStream.Close();
                m_fileStream.Dispose();
                m_fileStream = null;
                m_isClosed = true;
            }
        }

        /// <summary>
        /// 存储文件。
        /// </summary>
        /// <param name="fileFullName"></param>
        public override void SaveFile(string fileFullName)
        {
            if (!File.Exists(fileFullName))
                return;
            var fileInfo = new FileInfo(fileFullName);
            var info = BeginSaveFile(fileFullName, fileInfo.Length);

            byte[] data = new byte[2048];
            using (FileStream input = File.OpenRead(fileFullName))
            {
                int bytesRead;
                while ((bytesRead = input.Read(data, 0, data.Length)) > 0)
                {
                    WriteFile(info, data, 0, bytesRead);
                }
            }

            EndSaveFile(info);
        }

        private string PathNormalize(string path)
        {
            return path.Replace("\\", "/"); //.ToLower();
        }

        public override List<String> GetFilesByDirectory(String path)
        {
            var result = new List<String>();
            path = PathNormalize(path); //path.PathNormalize();
            foreach (var item in m_fileIndexes)
            {
                if (PathNormalize(item.Value.Path) == path)  //item.Value.Path.PathNormalize()
                    result.Add(item.Value.Path);
            }
            return result;
        }

        public override bool IsFileExist(String fileFullName)
        {
            fileFullName = PathNormalize(fileFullName); // fileFullName.PathNormalize();
            return m_fileIndexes.ContainsKey(fileFullName);
        }

        /// <summary>
        /// 获取文件。
        /// </summary>
        /// <param name="fileFullName"></param>
        /// <returns></returns>
        public override byte[] LoadFile(string fileFullName)
        {
            Open();
            Byte[] result;
            lock (m_locker)
            {
                fileFullName = PathNormalize(fileFullName); // fileFullName.PathNormalize();
                if (m_fileStream != null && m_fileIndexes.ContainsKey(fileFullName))
                {
                    result = DoLoadFile(fileFullName);
                }
                else
                    result = null;
            }
            return result;
        }

        private byte[] DoLoadFile(string fileFullName)
        {
            var info = m_fileIndexes[fileFullName];
            Byte[] result = new Byte[info.Length];
            m_fileStream.Position = info.Offset;
            m_fileStream.Read(result, 0, result.Length);
            if (m_encodeData)
            {
                m_bitCryto.Reset();
                int len = result.Length;
                for (int i = 0; i < len; ++i)
                {
                    result[i] = m_bitCryto.Decode(result[i]);
                }
            }
            return result;
        }

        public override byte[] LoadFileBlock(string fileFullName, uint offset, int length)
        {
            this.Open();
            byte[] result;
            lock (MogoFileSystem.m_locker)
            {
                if (this.m_fileStream != null && this.m_fileIndexes.ContainsKey(fileFullName))
                {
                    result = this.DoLoadFileBlock(fileFullName, offset, length);
                }
                else
                {
                    result = null;
                }
            }
            return result;
        }

        private byte[] DoLoadFileBlock(string fileFullName, uint offset, int length)
        {
            IndexInfo indexInfo = this.m_fileIndexes[fileFullName];
            byte[] array = new byte[length];
            this.m_fileStream.Position = (long)((ulong)(indexInfo.Offset + offset));
            this.m_fileStream.Read(array, 0, length);
            if (this.m_encodeData)
            {
                this.m_bitCryto.Reset(offset);
                int num = array.Length;
                for (int i = 0; i < num; i++)
                {
                    array[i] = this.m_bitCryto.Decode(array[i]);
                }
            }
            return array;
        }


        public override List<KeyValuePair<string, byte[]>> LoadFiles(List<KeyValuePair<string, string>> fileFullNames)
        {
            Open();
            List<KeyValuePair<string, byte[]>> result = new List<KeyValuePair<string, byte[]>>();
            lock (m_locker)
            {
                foreach (var fileFullName in fileFullNames)
                {
                    var path = PathNormalize(fileFullName.Value); //fileFullName.Value.PathNormalize();
                    if (m_fileStream != null && m_fileIndexes.ContainsKey(path))
                    {
                        var fileData = DoLoadFile(path);
                        result.Add(new KeyValuePair<string, byte[]>(fileFullName.Key, fileData));
                    }
                    else
                    {
                        LoggerHelper.Error("File not exist in MogoFileSystem: " + FileFullName);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 获取文本类型文件。
        /// </summary>
        /// <param name="fileFullName"></param>
        /// <returns></returns>
        public override String LoadTextFile(string fileFullName)
        {
            Open();
            var content = LoadFile(fileFullName);
            if (content != null)
                return System.Text.Encoding.UTF8.GetString(content);
            else
                return String.Empty;
        }

        /// <summary>
        /// 删除文件。
        /// </summary>
        /// <param name="fileFullName"></param>
        public override void DeleteFile(string fileFullName)
        {
            lock (m_locker)
            {
                fileFullName = PathNormalize(fileFullName); //fileFullName.PathNormalize();
                if (m_fileIndexes.ContainsKey(fileFullName))
                {
                    var info = m_fileIndexes[fileFullName];
                    info.Deleted = true;
                    info.Id = "";
                    m_fileIndexes.Remove(fileFullName);
                    m_deletedIndexes.Add(info);
                }
            }
        }

        /// <summary>
        /// 开始存储文件。
        /// </summary>
        /// <param name="fileFullName"></param>
        /// <param name="fileSize"></param>
        /// <returns></returns>
        public override IndexInfo BeginSaveFile(string fileFullName, long fileSize)
        {
            IndexInfo info;
            lock (m_locker)
            {
                fileFullName = PathNormalize(fileFullName); //fileFullName.PathNormalize();
                if (m_fileIndexes.ContainsKey(fileFullName))//文件已存在
                {
                    var fileInfo = m_fileIndexes[fileFullName];
                    if (fileSize > fileInfo.PageLength)//新文件大小超出预留页大小
                    {
                        DeleteFile(fileFullName);

                        info = new IndexInfo() { Id = fileFullName, FileName = fileInfo.FileName, Path = fileInfo.Path, Offset = m_packageInfo.IndexOffset, FileState = FileState.New };
                        m_fileStream.Position = m_packageInfo.IndexOffset;
                        m_fileIndexes[fileFullName] = info;
                    }
                    else//新文件大小没有超出预留页大小
                    {
                        info = fileInfo;
                        info.Length = 0;
                        info.FileState = FileState.Modify;
                        m_fileStream.Position = info.Offset;
                    }
                }
                else//文件不存在，在索引最后新建
                {
                    info = new IndexInfo()
                    {
                        Id = fileFullName,
                        FileName = Path.GetFileName(fileFullName),
                        Path = Path.GetDirectoryName(fileFullName),
                        Offset = m_packageInfo.IndexOffset,
                        FileState = FileState.New
                    };
                    m_fileStream.Position = m_packageInfo.IndexOffset;
                    m_fileIndexes[fileFullName] = info;
                }
            }
            return info;
        }

        /// <summary>
        /// 写入文件信息。
        /// </summary>
        /// <param name="info"></param>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        public override void WriteFile(IndexInfo info, Byte[] buffer, int offset, int count)
        {
            info.Length += (uint)count;
            if (m_encodeData)
            {
                m_bitCryto.Reset();
                int len = buffer.Length;
                for (int i = 0; i < len; ++i)
                {
                    buffer[i] = m_bitCryto.Encode(buffer[i]);
                }
            }
            m_fileStream.Write(buffer, offset, count);
        }

        /// <summary>
        /// 存储文件结束。
        /// </summary>
        /// <param name="info"></param>
        public override void EndSaveFile(IndexInfo info)
        {
            lock (m_locker)
            {
                var leftSize = info.Length % m_pageSize;
                if (leftSize != 0)
                {
                    var empty = m_pageSize - leftSize;
                    var emptyData = new Byte[empty];
                    m_fileStream.Write(emptyData, 0, emptyData.Length);
                    info.PageLength = info.Length + empty;
                }
                else
                    info.PageLength = info.Length;

                m_fileStream.Flush();
                if (info.FileState == FileState.New)//修改状态不用修改索引偏移
                    m_packageInfo.IndexOffset = (uint)m_fileStream.Position;
            }
        }

        public override void CleanBackUpIndex()
        {
            var path = Path.GetDirectoryName(m_fileFullName);
            var fileName = Path.Combine(path, BACK_UP).Replace("\\", "/");
            if (File.Exists(fileName))
                File.Delete(fileName);
        }

        /// <summary>
        /// 此接口只在打包資源時使用，解決連續生產FirstPkg文件時前後生產文件大小出現變化的問題。
        /// </summary>
        public override void Clear()
        {
            //UnityEngine.Debug.Log("==== 清除MogoFileSystem上次緩存信息");
            m_fileIndexes.Clear();
            m_deletedIndexes.Clear();
            m_packageInfo.IndexOffset = 0;
            Close();
            UnityEngine.Caching.CleanCache();
        }

        #endregion

    #region 私有方法

        /// <summary>
        /// 获取索引信息。
        /// </summary>
        /// <param name="fileSize">文件包大小。</param>
        private void GetIndexInfo(long fileSize)
        {
            var useBackUp = CheckBackUpIndex();
            if (!useBackUp) GetIndexInfo(fileSize, false);
        }

        /// <summary>
        /// 获取索引信息。
        /// </summary>
        /// <param name="fileSize">文件包大小。</param>
        /// <param name="needBackUpIndex">是否备份索引信息。</param>
        private void GetIndexInfo(long fileSize, bool needBackUpIndex)
        {
            lock (m_locker)
            {
                m_packageInfo = GetPackageInfo(m_fileStream, fileSize);
                LoggerHelper.Info(string.Format("==== packageSize:{0},IndexOffeset:{1}", PackageInfo.GetPackageSize(), m_packageInfo.IndexOffset));
                if (m_packageInfo == null || m_packageInfo.IndexOffset == 0) return;
                var indexSize = (int)(fileSize - PackageInfo.GetPackageSize() - m_packageInfo.IndexOffset);//计算索引信息大小
                LoggerHelper.Info(string.Format("==== fileSize:{0},packageSize:{1},IndexOffeset:{2},indexSize:{3}", fileSize, PackageInfo.GetPackageSize(), m_packageInfo.IndexOffset, indexSize));
                var indexData = new Byte[indexSize];
                m_fileStream.Position = m_packageInfo.IndexOffset;
                m_fileStream.Read(indexData, 0, indexSize);//获取索引信息
                LoadIndexInfo(indexData, needBackUpIndex);
            }
        }

        private bool CheckBackUpIndex()
        {
            var path = Path.GetDirectoryName(m_fileFullName);
            var fileName = Path.Combine(path, BACK_UP).Replace("\\", "/");
            if (File.Exists(fileName))
            {
                var fileInfo = new FileInfo(fileName);
                var fileSize = fileInfo.Length;
                var fileStream = File.Open(fileName, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                m_packageInfo = GetPackageInfo(fileStream, fileSize);

                if (m_packageInfo == null || m_packageInfo.IndexOffset == 0) return true;
                var indexSize = (int)(fileSize - PackageInfo.GetPackageSize());//计算索引信息大小
                var indexData = new Byte[indexSize];
                fileStream.Position = 0;
                fileStream.Read(indexData, 0, indexSize);//获取索引信息
                LoadIndexInfo(indexData, false);
                return true;
            }
            else
                return false;
        }

        private void LoadIndexInfo(Byte[] indexData, bool needBackUpIndex)
        {
            if (indexData.Length > 0)
            {
                if (needBackUpIndex) BackupIndexInfo(indexData);
                indexData = DESCrypto.Decrypt(indexData, m_number);//解密索引数据
            }

            m_deletedIndexes.Clear();
            m_fileIndexes.Clear();
            var index = 0;
            while (index < indexData.Length)
            {
                var info = IndexInfo.Decode(indexData, ref index);
                if (info.Deleted)
                    m_deletedIndexes.Add(info);
                else
                    m_fileIndexes[PathNormalize(info.Id)] = info;  //info.Id.PathNormalize()
            }

            m_fileStream.Position = m_packageInfo.IndexOffset;//重置回文件信息结尾处
        }

        private void BackupIndexInfo(byte[] indexData)
        {
            var path = Path.GetDirectoryName(m_fileFullName);
            var fileName = Path.Combine(path, BACK_UP).Replace("\\", "/");
            if (File.Exists(fileName)) File.Delete(fileName);
            using (var fileStream = File.Create(fileName))
            {
                fileStream.Write(indexData, 0, indexData.Length);
                fileStream.Flush();
                SavePackageInfo(fileStream);
            }
        }

        /// <summary>
        /// 获取包信息。
        /// </summary>
        /// <param name="fileSize"></param>
        private PackageInfo GetPackageInfo(FileStream fileStream, long fileSize)
        {
            var packageInfo = new PackageInfo();
            if (fileSize < PackageInfo.GetPackageSize())
                return new PackageInfo();

            fileStream.Position = fileSize - PackageInfo.GetPackageSize();//索引信息起始位置存放在文件结尾处。
            var lengthData = new Byte[PackageInfo.GetPackageSize()];
            fileStream.Read(lengthData, 0, PackageInfo.GetPackageSize());
            var index = 0;
            packageInfo.IndexOffset = EncodeDecoder.DecodeUInt32(lengthData, ref index);//获取索引信息起始位置
            return packageInfo;
        }

        private void SavePackageInfo(FileStream fileStream)
        {
            var indexPositionData = EncodeDecoder.EncodeUInt32(m_packageInfo.IndexOffset);
            fileStream.Write(indexPositionData, 0, indexPositionData.Length);
            fileStream.Flush();
        }

        #endregion
    }
#endif
}
