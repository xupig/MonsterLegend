﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace GameLoader.IO
{
    public static class IOUtils
    {
        #region 文件路径处理
        /// <summary>
        /// 根据文件名--取得资源的StreamAssets路径
        /// </summary>
        /// <param name="fileName">文件名</param>
        /// <returns></returns>
        public static string GetStreamPath(string fileName)
        {
            var Path = String.Concat(Application.streamingAssetsPath, "/", fileName);
            if (UnityPropUtils.Platform != RuntimePlatform.Android)
                Path = String.Concat(ConstString.ASSET_FILE_HEAD, Path);
            return Path;
        }

        /// <summary>
        /// 返回没有后缀名的文件名
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static string GetFileNameWithoutExtention(string fileName, char separator = '/')
        {
            var name = GetFileName(fileName, separator);
            return GetFilePathWithoutExtention(name);
        }

        public static string GetFilePathWithoutExtention(string fileName)
        {
            return fileName.Substring(0, fileName.LastIndexOf('.'));
        }

        public static string GetFileName(string path, char separator = '/')
        {
            return path.Substring(path.LastIndexOf(separator) + 1);
        }

        public static string PathNormalize(this string str)
        {
            return str.Replace("\\", "/").ToLower();
        }

        #endregion
    }
}