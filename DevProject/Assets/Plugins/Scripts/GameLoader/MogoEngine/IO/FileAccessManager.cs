﻿#region 模块信息
/*----------------------------------------------------------------
// Copyright (C) 2016 广州，雷神
//
// 模块名：FileAccessManager
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.9.12
// 模块描述：文件访问管理器。
//----------------------------------------------------------------*/
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameLoader.Config;
using GameLoader.IO;
using GameLoader.Utils;
using UnityEngine;

namespace GameLoader.IO
{
    /// <summary>
    /// 文件访问管理器。
    /// </summary>
    public class FileAccessManager
    {
        public static List<String> GetFileNamesByDirectory(String path)
        {
            return SystemConfig.IsUseFileSystem ? MogoFileSystem.Instance.GetFilesByDirectory(path) : Directory.GetFiles(Path.Combine(SystemConfig.RuntimeResourcePath, path)).ToList();
        }

        /// <summary>
        /// 读取文本文件。
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static String LoadText(String fileName)
        {
            fileName = fileName.Replace('\\', '/');
            return SystemConfig.IsUseFileSystem ? MogoFileSystem.Instance.LoadTextFile(fileName) : FileUtils.LoadFile(Path.Combine(SystemConfig.RuntimeResourcePath, fileName));
        }

        /// <summary>
        /// 读取数据文件。
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static byte[] LoadBytes(String fileName)
        {
            fileName = fileName.Replace('\\', '/');
            byte[] result = SystemConfig.IsUseFileSystem ? MogoFileSystem.Instance.LoadFile(fileName) : FileUtils.LoadByteFile(Path.Combine(SystemConfig.RuntimeResourcePath, fileName));
            return result;
        }

        public static byte[] LoadFileBlock(string fileName, uint offset, int length)
        {
            return MogoFileSystem.Instance.LoadFileBlock(fileName, offset, length);
        }

        public static void DecompressFile(String fileName, Action<long> progress = null)
        {
            if (SystemConfig.IsUseFileSystem)
            {
                //string targetPath = SystemConfig.RuntimeResourcePath;
                //if (UnityPropUtils.Platform == RuntimePlatform.WindowsPlayer || UnityPropUtils.Platform == RuntimePlatform.WindowsWebPlayer)
                //{ //PC端或web端，补丁包中的Resources目录下全部资源（除了Bytes$GameLoader.bytes.u）放到StreamingAssets目录下
               //     targetPath = string.Concat(Application.streamingAssetsPath, "/");
                //}
                PkgFileUtils.DecompressToMogoFileAndDirectory(SystemConfig.RuntimeResourcePath, fileName, progress);
                //Utils.DecompressToMogoFile(fileName);
            }
            else
            {
                //LoggerHelper.Debug("FileAccessManager.DecompressFile加压到目录");
                PkgFileUtils.DecompressToDirectory(SystemConfig.RuntimeResourcePath, fileName, progress);
            }
        }

        public static bool IsFileExist(String fileName)
        {
            fileName = fileName.Replace('\\', '/');
            return SystemConfig.IsUseFileSystem ? MogoFileSystem.Instance.IsFileExist(fileName) : File.Exists(Path.Combine(SystemConfig.RuntimeResourcePath, fileName));
        }

        /// <summary>
        /// 读取数据文件。
        /// </summary>
        /// <param name="fileFullNames"></param>
        /// <returns></returns>
        public static List<KeyValuePair<string, byte[]>> LoadFiles(List<KeyValuePair<string, string>> fileFullNames)
        {
            return SystemConfig.IsUseFileSystem ? MogoFileSystem.Instance.LoadFiles(fileFullNames) : LoadLocalFiles(fileFullNames);
        }

        private static List<KeyValuePair<string, byte[]>> LoadLocalFiles(List<KeyValuePair<string, string>> fileFullNames)
        {
            var result = new List<KeyValuePair<string, byte[]>>();
            foreach (var item in fileFullNames)
            {
                var data = FileUtils.LoadByteFile(Path.Combine(SystemConfig.RuntimeResourcePath, item.Value));
                result.Add(new KeyValuePair<string, byte[]>(item.Key, data));
            }
            return result;
        }
    }
}