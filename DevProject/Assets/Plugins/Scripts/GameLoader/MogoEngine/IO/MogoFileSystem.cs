﻿#region 模块信息
/*----------------------------------------------------------------
// Copyright (C) 2016 广州，雷神
//
// 模块名：MogoFileSystem
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.6.15
// 模块描述：自定义文件系统。
//----------------------------------------------------------------*/
#endregion

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using GameLoader.Config;
using GameLoader.Utils;
using System.IO;

namespace GameLoader.IO
{
    /// <summary>
    /// 自定义文件系统。
    /// </summary>
    public abstract class MogoFileSystem
    {
        #region 属性

        /// <summary>
        /// 页大小。
        /// </summary>
        protected const int m_pageSize = 512;
        /// <summary>
        /// 文件名称。
        /// </summary>
        public static string FILE_NAME
        {
            get
            {
                return "pkg";
            }
        }
        protected string BACK_UP
        {
            get
            {
                return "pkg_i_bak";
            }
        }
        /// <summary>
        /// 文件完整路径。
        /// </summary>
        protected string m_fileFullName;
        /// <summary>
        /// 文件索引。
        /// </summary>
        protected Dictionary<string, IndexInfo> m_fileIndexes = new Dictionary<string, IndexInfo>();
        public Dictionary<string, IndexInfo> fileIndexes
        {
            get
            {
                return m_fileIndexes;
            }
        }

        /// <summary>
        /// 删除文件索引。
        /// </summary>
        protected List<IndexInfo> m_deletedIndexes = new List<IndexInfo>();
        
        /// <summary>
        /// 包信息。
        /// </summary>
        protected PackageInfo m_packageInfo = new PackageInfo();
        /// <summary>
        /// 索引加密钥匙
        /// C#的DES只支持64bits的Key
        /// http://msdn.microsoft.com/en-us/library/system.security.cryptography.des.key(VS.80).aspx
        /// </summary>
        protected byte[] m_number
        {
            get
            {
				//if(UnityEngine.UnityPropUtils.Platform==UnityEngine.RuntimePlatform.IPhonePlayer)
                //	return new byte [] {123,52,53,9,12,6,23,26};
				//else
                //	return CryptoUtils.GetIndexNumber();
                return KeyUtils.GetIndexNumber();
            }
        }
        protected BitCryto m_bitCryto;
        protected bool m_encodeData = true;
        protected static readonly Object m_locker = new Object();
        protected bool m_isClosed = true;

        public bool IsClosed
        {
            get
            {
                return m_isClosed;
            }
        }

        /// <summary>
        /// 文件完整路径。
        /// </summary>
        public string FileFullName
        {
            get { return m_fileFullName; }
            set { m_fileFullName = value; }
        }

        public Dictionary<string, IndexInfo> FileIndexs
        {
            get { return m_fileIndexes; }
        }

        public List<IndexInfo> DeletedIndexes
        {
            get { return m_deletedIndexes; }
        }

        private static MogoFileSystem m_instance;

        public static MogoFileSystem Instance
        {
            get { return m_instance; }
        }

        #endregion

        #region 构造函数

        static MogoFileSystem()
        {
#if UNITY_WEBPLAYER
            m_instance = new MogoFileSystemRemote();
#else
            m_instance = new MogoFileSystemLocal();
#endif
        }

        protected MogoFileSystem()
        {
            var number = new List<short>();
            foreach (var item in m_number)
            {
                number.Add(item);
            }
            m_bitCryto = new BitCryto(number.ToArray());
        }

        #endregion

        #region 公有方法

        public abstract void Init();


        /// <summary>
        /// 打开文件读写。
        /// </summary>
        public abstract void Init(string fileName);

        /// <summary>
        /// 初始化内存读写。
        /// </summary>
        public abstract void Init(byte[] data);

        /// <summary>
        /// 打开文件读写。
        /// </summary>
        public abstract void Open();

        /// <summary>
        /// 保存索引信息。
        /// </summary>
        public abstract void SaveIndexInfo();

        /// <summary>
        /// 获取并备份索引信息。
        /// </summary>
        public abstract void GetAndBackUpIndexInfo();

        /// <summary>
        /// 关闭文件读写。
        /// </summary>
        public abstract void Close();

        /// <summary>
        /// 存储文件。
        /// </summary>
        /// <param name="fileFullName"></param>
        public abstract void SaveFile(string fileFullName);


        public abstract List<String> GetFilesByDirectory(String path);


        public abstract bool IsFileExist(String fileFullName);

        /// <summary>
        /// 获取文件。
        /// </summary>
        /// <param name="fileFullName"></param>
        /// <returns></returns>
        public abstract byte[] LoadFile(string fileFullName);

        public abstract byte[] LoadFileBlock(string fileFullName, uint offset, int length); 

        public abstract List<KeyValuePair<string, byte[]>> LoadFiles(List<KeyValuePair<string, string>> fileFullNames);


        /// <summary>
        /// 获取文本类型文件。
        /// </summary>
        /// <param name="fileFullName"></param>
        /// <returns></returns>
        public abstract String LoadTextFile(string fileFullName);

        /// <summary>
        /// 删除文件。
        /// </summary>
        /// <param name="fileFullName"></param>
        public abstract void DeleteFile(string fileFullName);

        /// <summary>
        /// 开始存储文件。
        /// </summary>
        /// <param name="fileFullName"></param>
        /// <param name="fileSize"></param>
        /// <returns></returns>
        public abstract IndexInfo BeginSaveFile(string fileFullName, long fileSize);


        /// <summary>
        /// 写入文件信息。
        /// </summary>
        /// <param name="info"></param>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        public abstract void WriteFile(IndexInfo info, Byte[] buffer, int offset, int count);

        /// <summary>
        /// 存储文件结束。
        /// </summary>
        /// <param name="info"></param>
        public abstract void EndSaveFile(IndexInfo info);

        public abstract void CleanBackUpIndex();

        /// <summary>
        /// 此接口只在打包資源時使用，解決連續生產FirstPkg文件時前後生產文件大小出現變化的問題。
        /// </summary>
        public abstract void Clear();

        #endregion
    }

    /// <summary>
    /// 索引实体。
    /// </summary>
    public class IndexInfo
    {
        /// <summary>
        /// 索引标识。
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 文件名称。
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 文件路径。
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// 文件存储索引偏移量。
        /// </summary>
        public uint Offset { get; set; }
        /// <summary>
        /// 文件大小。
        /// </summary>
        public uint Length { get; set; }
        /// <summary>
        /// 文件占用大小。
        /// </summary>
        public uint PageLength { get; set; }
        /// <summary>
        /// 是否已删除。
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// 文件是否修改。
        /// </summary>
        public FileState FileState { get; set; }

        public Byte[] GetEncodeData()
        {
            return Encode(this);
        }

        public static byte[] Encode(IndexInfo info)
        {
            var result = new List<byte>();
            result.AddRange(EncodeDecoder.EncodeString(info.Id));
            result.AddRange(EncodeDecoder.EncodeString(info.FileName));
            result.AddRange(EncodeDecoder.EncodeString(info.Path));
            result.AddRange(EncodeDecoder.EncodeUInt32(info.Offset));
            result.AddRange(EncodeDecoder.EncodeUInt32(info.Length));
            result.AddRange(EncodeDecoder.EncodeUInt32(info.PageLength));
            result.AddRange(EncodeDecoder.EncodeBoolean(info.Deleted));
            //info.ToString("Encode()");
            return result.ToArray();
        }

        public static IndexInfo Decode(byte[] data, ref int offset)
        {
            var info = new IndexInfo();
            info.Id = EncodeDecoder.DecodeString(data, ref offset);
            info.FileName = EncodeDecoder.DecodeString(data, ref offset);
            info.Path = EncodeDecoder.DecodeString(data, ref offset);
            info.Offset = EncodeDecoder.DecodeUInt32(data, ref offset);
            info.Length = EncodeDecoder.DecodeUInt32(data, ref offset);
            info.PageLength = EncodeDecoder.DecodeUInt32(data, ref offset);
            info.Deleted = EncodeDecoder.DecodeBoolean(data, ref offset);
            //info.ToString("Decode()");
            return info;
        }

        public void ToString(string desc)
        {
            LoggerHelper.Info(string.Format("[FileSystem" + desc + "] Id:{0},FileName:{1},Path:{2},Offset:{3},Length:{4},PageLength:{5},Deleted:{6}", Id, FileName, Path, Offset, Length, PageLength, Deleted));
        }
    }

    /// <summary>
    /// 文件编辑状态。
    /// </summary>
    public enum FileState
    {
        /// <summary>
        /// 默认状态。
        /// </summary>
        Default,
        /// <summary>
        /// 新建状态。
        /// </summary>
        New,
        /// <summary>
        /// 修改状态。
        /// </summary>
        Modify
    }

    /// <summary>
    /// 包信息实体。
    /// </summary>
    public class PackageInfo
    {
        private static int m_packageSize = -1;

        /// <summary>
        /// 索引偏移量。
        /// </summary>
        public uint IndexOffset { get; set; }

        static PackageInfo()
        {
            m_packageSize = 4;//Marshal.SizeOf(typeof(UInt32));
        }

        public static int GetPackageSize()
        {
            return m_packageSize;
        }

        /// <summary>
        /// 清除索引信息
        /// </summary>
        public void Clear()
        {
            IndexOffset = 0;
            m_packageSize = -1;
        }
    }

    public class EncodeDecoder
    {
        private static Encoding m_encoding = Encoding.UTF8;

        private static int m_uintLength = 4;//Marshal.SizeOf(typeof(UInt32));
        private static int m_uint16Length = 2;//Marshal.SizeOf(typeof(UInt16));
        private static int m_boolLength = 1;
        public static uint DecodeUInt32(byte[] data, ref Int32 index)
        {
            Byte[] result = new Byte[m_uintLength];
            Buffer.BlockCopy(data, index, result, 0, m_uintLength);
            //Array.Reverse(result);
            index += m_uintLength;

            return BitConverter.ToUInt32(result, 0);
        }
        public static byte[] EncodeUInt32(uint vValue)
        {
            var result = BitConverter.GetBytes(vValue);
            //Array.Reverse(result);
            return result;
        }

        public static byte[] EncodeBoolean(bool vValue)
        {
            var result = BitConverter.GetBytes(vValue);
            //Array.Reverse(result);
            return result;
        }

        public static bool DecodeBoolean(byte[] data, ref Int32 index)
        {
            Byte[] result = new Byte[m_boolLength];
            //Array.Reverse(result);
            Buffer.BlockCopy(data, index, result, 0, m_boolLength);
            index += m_boolLength;

            return BitConverter.ToBoolean(result, 0);
        }

        public static UInt16 DecodeUInt16(byte[] data, ref Int32 index)
        {
            Byte[] result = new Byte[m_uint16Length];
            Buffer.BlockCopy(data, index, result, 0, m_uint16Length);
            //Array.Reverse(result);
            index += m_uint16Length;

            return BitConverter.ToUInt16(result, 0);
        }

        public static byte[] EncodeString(String vValue)
        {
            String value = (String)vValue;
            //byte[] encodeValues = m_encoding.GetBytes(value);
            //Array.Reverse(encodeValues);
            Encoder ec = m_encoding.GetEncoder();//获取字符编码
            Char[] charArray = value.ToCharArray();
            Int32 length = ec.GetByteCount(charArray, 0, charArray.Length, false);//获取字符串转换为二进制数组后的长度，用于申请存放空间
            Byte[] encodeValues = new Byte[length];//申请存放空间
            ec.GetBytes(charArray, 0, charArray.Length, encodeValues, 0, true);//将字符串按照特定字符编码转换为二进制数组

            return CommonUtils.FillLengthHead(encodeValues);
        }

        public static String DecodeString(byte[] data, ref Int32 index)
        {
            Byte[] strData = CutLengthHead(data, ref index);
            return m_encoding.GetString(strData);
        }

        /// <summary>
        /// 去掉数据长度信息，返回对应数据的二进制数组，并进行相应的索引偏移。
        /// </summary>
        /// <param name="srcData">源数据</param>
        /// <param name="index">索引引用</param>
        /// <returns>对应数据的二进制数组</returns>
        protected static Byte[] CutLengthHead(Byte[] srcData, ref Int32 index)
        {
            Int32 length = (Int32)DecodeUInt16(srcData, ref index);
            Byte[] result = new Byte[length];
            Buffer.BlockCopy(srcData, index, result, 0, length);
            ////Array.Reverse(result);
            index += length;

            return result;
        }

    }
}
