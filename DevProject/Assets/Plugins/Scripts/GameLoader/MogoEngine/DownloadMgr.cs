﻿using GameLoader.Utils;
using GameLoader.Utils.XML;
using ICSharpCode.SharpZipLib.GZip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using UnityEngine;

namespace GameLoader.Mgrs
{
    //class DownloadUnit
    //{
    //    public string URL;
    //    public string LocalAbsPath;
    //    public string MD5;
    //    public Action<int, int, float, object> OnProcessing;
    //    public Action<object> OnError;
    //    public Action<object> OnFinished;

    //    public DownloadUnit(string url, string localAbsPath, string remoteMD5,
    //        Action<int, int, float, object> onProcessing, Action<object> onFinished, Action<object> onError)
    //    {
    //        URL = url;
    //        LocalAbsPath = localAbsPath;
    //        MD5 = remoteMD5;
    //        OnProcessing = onProcessing;
    //        OnError = onError;
    //        OnFinished = onFinished;
    //    }
    //}

    public class TextDownloadMgr
    {
        public static void AsyncDownloadString(string url, Action<string> callback, Action onError = null)
        {
            Action action = () =>
            {
                string content = DownloadGzipString(url, onError);
                if (content != "") LoaderDriver.Invoke(callback.SafeInvoke, content);
            };
            action.BeginInvoke(null, null);
        }

        public static void SyncDownloadFile(string url, string localAbsPath)
        {
            SyncDownloadGzipFile(url, localAbsPath);
        }

        private static bool SyncDownloadGzipFile(string url, String localPath, Action failCallback = null)
        {
            try
            {
                var content = DownloadGzipString(url, failCallback);
                if (!String.IsNullOrEmpty(content))
                {
                    FileUtils.SaveText(localPath, content);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
                return false;
            }
        }

        private static byte[] DownloadGzipData(string url, Action failCallback = null)
        {
            LoggerHelper.Info(url);
            Debug.LogWarning(url);
            try
            {
                HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
                wr.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
                HttpWebResponse response = (HttpWebResponse)wr.GetResponse();
                MemoryStream re = new MemoryStream();
                if (response.ContentEncoding.ToLower().Contains("gzip"))
                {
                    GZipInputStream gzi = new GZipInputStream(response.GetResponseStream());
                    int count = 0;
                    byte[] data = new byte[4096];

                    while ((count = gzi.Read(data, 0, data.Length)) != 0)
                    {
                        re.Write(data, 0, count);
                    }
                }
                else
                {
                    Stream s = response.GetResponseStream();
                    int count = 0;
                    byte[] data = new byte[4096];

                    while ((count = s.Read(data, 0, data.Length)) != 0)
                    {
                        re.Write(data, 0, count);
                    }
                }
                return re.ToArray();
            }
            catch (Exception e)
            {
                LoggerHelper.Error("DownloadGzipData Error:url=" + url + ",error=" + e.Message);
                LoaderDriver.Invoke(failCallback.SafeInvoke);
                return new byte[0];
            }
        }

        private static string DownloadGzipString(string url, Action failCallback = null)
        {
            return Encoding.UTF8.GetString(DownloadGzipData(url, failCallback));
        }

        public static String GetRandomParasUrl(String url)
        {
            var r = RandomUtils.CreateRandom();
            return String.Format("{0}?type={1}&ver={2}&sign={3}", url.Trim(), r.Next(100), r.Next(100), Guid.NewGuid().ToString().Substring(0, 8));
        }
    }

    public class FileDownloadMgr
    {
        private WebClient _webClient;
        private Dictionary<string, Action<float, long, long>> _progressDic = new Dictionary<string, Action<float, long, long>>();
        private Dictionary<string, Action<string>> _successDic = new Dictionary<string, Action<string>>();
        private Dictionary<string, Action<string, string>> _errorDic = new Dictionary<string, Action<string, string>>();

        private static FileDownloadMgr _instance;

        public static FileDownloadMgr Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new FileDownloadMgr();
                return _instance;
            }
        }

        public FileDownloadMgr()
        {
            _webClient = new WebClient();
            _webClient.Proxy = null;
            _webClient.DownloadProgressChanged += webClient_DownloadProgressChanged;
            _webClient.DownloadFileCompleted += webClient_DownloadFileCompleted;
        }

        public void AsyncDownloadFile(String url, String localPath, Action<float, long, long> progress, Action<string> callback, Action<string, string> errcallback)
        {
            _webClient.DownloadFileAsync(new Uri(url), localPath, url);
            _progressDic[url] = progress;
            _successDic[url] = callback;
            _errorDic[url] = errcallback;
        }

        public void CancelDownload()
        {
            _webClient.CancelAsync();
        }

        /// <summary>
        /// 下载图片
        /// </summary>
        /// <param name="url"></param>
        /// <param name="callback"></param>
        public void LoadWwwImage(string url, Action<string, Texture2D> callback)
        {
            HttpWrappr.instance.LoadWwwImage(url, callback);
        }

        void webClient_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            var key = e.UserState.ToString();
            if (e.Error != null)
            {
                if (_errorDic.ContainsKey(key))
                {
                    _errorDic[key](key, e.Error.Message);
                }
            }
            else
            {
                if (_successDic.ContainsKey(key))
                {
                    _successDic[key](key);
                }
            }
            _progressDic.Remove(key);
            _successDic.Remove(key);
            _errorDic.Remove(key);
        }

        void webClient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            var key = e.UserState.ToString();
            if (_progressDic.ContainsKey(key))
            {
                _progressDic[key]((float)(e.ProgressPercentage * 0.01), e.BytesReceived, e.TotalBytesToReceive);
            }
        }
    }
}