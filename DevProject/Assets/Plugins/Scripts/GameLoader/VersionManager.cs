﻿using System;
using System.IO;
using GameLoader.Config;
using GameLoader.Defines;
using GameLoader.LoadingBar;
using GameLoader.PlatformSdk;
using GameLoader.Utils;
using UnityEngine;
using GameLoader.Utils.Timer;
using System.Collections.Generic;
using GameLoader.IO;

namespace GameLoader.Mgrs
{
    /// <summary>
    /// 版本更新--管理类
    /// </summary>
    public class VersionManager
    {
        private Action checkCallback;
        private VersionInfo localVersion;
        private UpdateApkMgr updateApkMgr;
        private UpdatePkgMgr updatePkgMgr;
        private UpdateProgressBarBgMgr updateProgressBarBgMgr;
        public static VersionManager Instance;
        private Action checkNetWorkCallback;
        private long checkNetWorkFileSize;
        public bool isUpdateLog = false;
        public VersionManager()
        {
            localVersion = new VersionInfo();
            updateApkMgr = new UpdateApkMgr(localVersion, CheckNetWork);
            updatePkgMgr = new UpdatePkgMgr(localVersion, CheckNetWork);
            updateProgressBarBgMgr = new UpdateProgressBarBgMgr(localVersion);
        }

        /// <summary>
        /// 版本检查，检查流程：
        /// 1、加载本地VersionInfo.xml文件
        /// 2、网络检查
        /// 3、检查Apk版本
        ///    3-1、Apk有更新
        ///         3-1-1、首次更新Apk,先把SD卡上的资源全部导到内存卡,再下载并安装Apk,最后重新进入游戏。
        ///         3-1-2、非首次更新Apk,下载并安装Apk,最后重新进入游戏。
        ///    3-2、Apk没更新，则到步骤4
        /// 4、检查资源版本
        ///    4-1、资源有更新,下载Pkg包，并解压覆盖到Application.persistentDataPath/RuntimeResource/目录下，然后把最新版本写入本地文件[VersionInfo.xml],最后到步骤5
        ///    4-2、资源没更新,直接到步骤5
        /// 5、结束
        /// </summary>
        /// <param name="callback">检查结束回调</param>
        /// <param name="isOpenCheck">版本检查开启状态[true:开启,false:关闭]</param>
        public void CheckVersion(Action callback, bool isOpenCheck)
        {
            try
            {
                if (isOpenCheck)
                {//开启版本检查
                    //A-1、加载本地VersionInfo.xml文件
                    checkCallback = callback;
                    //ProgressBar.Instance.UpdateTip("正在载入本地VersionInfo文件...");
                    localVersion.LoadLocalFile(OnLocalFile);
                }
                else
                {//关闭版本检查
                    if (!SystemConfig.ReleaseMode)
                    {
                        if (callback != null) callback();
                    }
                    else
                    {
                        localVersion.CopyFirstPkg(callback, null); //每次都从StreamingAssets拷贝FirstPkg到外部目录
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(string.Format("CheckVersion [Fail] message:{0}", ex.Message));
                LoggerHelper.Error(string.Format("CheckVersion [Fail] reason:{0}", ex.StackTrace));
            }
        }

        //A-2、网络检查
        private void OnLocalFile()
        {
            LoggerHelper.Info("本地文件加载[OK]");
            //ProgressBar.Instance.UpdateProgress(0.18f, true);
            GotoStep3();
            //CheckNetWork();
        }

        //步骤2--检查网络
        private void CheckNetWork(Action func, long fileSize)
        {
            checkNetWorkCallback = func;
            checkNetWorkFileSize = fileSize;
            //NetworkReachability.NotReachable                   网络不可达
            //NetworkReachability.ReachableViaCarrierDataNetwork 网络通过运营商数据网络是可达的
            //NetworkReachability.ReachableViaLocalAreaNetwork   网络通过Wifi或有线网络是可达的
            //NetworkReachability.NotReachable 在Android版本上判断有问题，所以不能用此判断
            //不等于NotReachable 不等于就能连上internet，有可能只是wifi连上了，通过下载文件来判断网络是否可达,不可达提示重试
            //LoggerHelper.Info("检查网络[Start] Application.internetReachability:" + Application.internetReachability);
            //ProgressBar.Instance.UpdateTip("正在检查网络...");
            //if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork)
            //{
            //    LoggerHelper.Info("检查网络[3G网络]");
            //    //3G网络，弹窗口，确定下载还是退出
            //    ConfirmBox.Instance.Show(string.Format(DefaultLanguage.GetString("continue_tip"), fileSize / 1024f / 1024f), OnSetNetwork, DefaultLanguage.GetString("continue"), DefaultLanguage.GetString("setNetwork"));
            //}
            //else
            //{
            checkNetWorkCallback();
            //}
        }

        //3G网络，弹窗口，确定下载还是退出
        private void OnSetNetwork(bool result)
        {
            if (result)
            {
                checkNetWorkCallback();
            }
            else
            {
                //PlatformSdkMgr.Instance.SetNetwork();
                ConfirmBox.Instance.Show(DefaultLanguage.GetString("continue_game"), OnKeepCheckNetWork, DefaultLanguage.GetString("continue"), DefaultLanguage.GetString("exit"));
            }
        }

        //设置网络后，继续版本检查
        private void OnKeepCheckNetWork(bool result)
        {
            if (result) CheckNetWork(checkNetWorkCallback, checkNetWorkFileSize);
            else Application.Quit();
        }

        //步骤3--检查Apk版本
        private void GotoStep3()
        {
            LoggerHelper.Info(String.Format("检查Apk版本[Start] localVersion:{0}, ProgramVersion:{1}", localVersion.ProgramVersion.ToString(), SystemConfig.ProgramVersion.ToString()));
            //ProgressBar.Instance.UpdateTip("正在检查Apk包版本...");
            bool result = localVersion.CompareProgramVersion(new VersionCodeInfo(SystemConfig.ProgramVersion));
            updateProgressBarBgMgr.Update(updateApkMgr.GetOutputRuntimeResourceFolder());
            //SystemConfig.IsOnlyUsePlatformCtrUpdate：这个参数仅给平台测试环境使用
            if (result || SystemConfig.IsOnlyUsePlatformCtrUpdate)
            { //A-3、更新Apk
                LoggerHelper.Info("apk有更新[Start]");
                updateApkMgr.Update();
            }
            else
            {//pkg检查
                CheckPkgVersion();
            }
        }

        public void TestUpdateProgressBar()
        {
            updateProgressBarBgMgr.Update(updateApkMgr.GetOutputRuntimeResourceFolder());
        }

        /// <summary>
        /// 检查pkg版本
        /// </summary>
        public void CheckPkgVersion()
        {
            //删除存储apk文件的目录
            //ProgressBar.Instance.UpdateProgress(0.2f, true);
            //ProgressBar.Instance.UpdateTip("正在检查pkg版本...");
            DeleteApkFile();
            bool result = localVersion.CompareResourceVersion(new VersionCodeInfo(SystemConfig.ResourceVersion));
            if (result)
            { //A-4、更新pkg资源
                LoggerHelper.Info("pkg有更新[Start]");
                updatePkgMgr.Update(onUpdatePkgCallback);
                if (!isUpdateLog)
                {
                    LoaderDriver.Instance.LogUpdate(0);
                    isUpdateLog = true;
                }
            }
            else
            {//没有更新资源，结束
                LoggerHelper.Info("apk和pkg都没有可更新！");
                if (checkCallback != null) checkCallback();
                checkCallback = null;
            }
        }

        //删除Apk文件
        private void DeleteApkFile()
        {
            string folder = null;
            try
            {
                folder = updateApkMgr.LocalApkFolder;
                if (!Directory.Exists(folder)) return;
                string[] list = Directory.GetFiles(folder, "*.apk");
                foreach (string item in list)
                {
                    if (File.Exists(item)) File.Delete(item);
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(string.Format("删除目录下:{0} apk文件 [Fail],reason:{1}", folder, ex.StackTrace));
            }
        }

        //资源更新结束回调
        private void onUpdatePkgCallback(bool result)
        {
            if (result)
            {//更新成功
                LoggerHelper.Info("资源更新成功！");
                //ProgressBar.Instance.UpdateProgress(0.25f, true);
                //ProgressBar.Instance.UpdateTip("pkg更新成功");
                if (checkCallback != null) checkCallback();
                checkCallback = null;
            }
            else
            { //更新失败：加提示
                LoggerHelper.Error("资源更新失败！");
                //ProgressBar.Instance.UpdateTip("pkg更新失败");
                if (VersionManager.Instance.isUpdateLog)
                {
                    LoaderDriver.Instance.LogUpdate(-1);
                }
            }
        }
        //====================================================//


        //=================  跟SDK对接Apk更新接口  ================//
        //安装平台Apk
        public void OpenUrl()
        {
            updateApkMgr.OpenUrl();
        }

        /// <summary>
        /// 获取资源外部存放目录,已'/'结尾
        /// </summary>
        /// <returns></returns>
        public string GetOutputRuntimeResourceFolder()
        {
            return updateApkMgr.GetOutputRuntimeResourceFolder();
        }

        /// <summary>
        /// 获取StreamingAssets目录下_resources.mk文件的路径
        /// </summary>
        /// <returns></returns>
        public string GetStreamingAssetsmkFilePath()
        {
            return updateApkMgr.GetStreamingAssetsmkFilePath();
        }

        public void ClearClientAllFile()
        {
            string rutimeResourceConfig = String.Concat(Application.persistentDataPath, ConstString.RutimeResourceConfig);
            LoggerHelper.Info("Begin ClearClientAllFile, rutimeResourceConfig: " + rutimeResourceConfig);
            var listFile = new List<string>()
            {
                 rutimeResourceConfig + "/DownloadedInfo.txt",
                 rutimeResourceConfig + "/AddtiveResourceList.xml",
                 rutimeResourceConfig + "/DefaultLanguage.xml",
                 rutimeResourceConfig + "/ResourceMapping.xml",
                 rutimeResourceConfig + "/servergrouplist.xml",
                 rutimeResourceConfig + "/servers.xml",
                 rutimeResourceConfig + "/VersionInfo.xml",
                 SystemConfig.RuntimeResourcePath + "pkg"
            };
            var listFolder = new List<string>()
            {
                 String.Concat(Application.persistentDataPath, ConstString.RutimeResource, "/ProgressBar/"),
                 SystemConfig.RuntimeResourcePath + "ResEx/",
                 SystemConfig.RuntimeResourcePath + "Resources/"
            };
            LoggerHelper.Info("listFile: " + listFile.PackList());
            LoggerHelper.Info("listFolder: " + listFolder.PackList());
            MogoFileSystem.Instance.Close();
            for (int i = 0; i < listFile.Count; i++)
            {
                if (File.Exists(listFile[i]))
                {
                    File.Delete(listFile[i]);
                    LoggerHelper.Info("File.Delete: " + listFile[i]);
                }
            }
            for (int i = 0; i < listFolder.Count; i++)
            {
                if (Directory.Exists(listFolder[i]))
                {
                    Directory.Delete(listFolder[i], true);
                    LoggerHelper.Info("Directory.Delete: " + listFolder[i]);
                }
            }
            ConfirmBox.Instance.Show(DefaultLanguage.GetString("reboot_game_end"), (flag) => Application.Quit(), DefaultLanguage.GetString("exit"), isCancel: false);
        }

        public void Release()
        {
            if (updateApkMgr != null)
            {
                updateApkMgr.Release();
            }
        }
    }
}
