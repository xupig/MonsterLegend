﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using GameResource;

using GameLoader.Utils;
using GameLoader.LoadingBar;
using UnityEngine.SceneManagement;

namespace MogoEngine.Mgrs
{
    public class SceneMgr
    {
        public const string EMPTH_SCENE = "EmptyScene";  //Unity在切换场景时需要切换到一个空场景
        public const string CONTAINER_SCENE = "ContainerScene";  //容器场景

        private static int _canCacheSceneMax = 1; //能够cache的场景资源数量

        public static Color defaultAmbientLight = new Color(0.3f, 0.3f, 0.3f);

        private SceneMgr() { _addtiveScenesMgr = new AddtiveScenesMgr(); }
        static SceneMgr()
        {
            _instance = new SceneMgr();
            InitAccordingToSystemMemory();
        }

        private static SceneMgr _instance;
        public static SceneMgr Instance
        {
            get { return _instance; }
        }

        private SceneResource _sceneResource;
        private AddtiveScenesMgr _addtiveScenesMgr;
        private Queue<string> _sceneResourceCacheName = new Queue<string>();
        private Dictionary<string, SceneResource> _sceneResourceCache = new Dictionary<string, SceneResource>();
        private bool _needUnloadUnusedAssets = false;
        private bool _isLowModel;

        static void InitAccordingToSystemMemory()
        {
            //以下根据当前运行的机型不同，对场景的cache量也不同。
            _canCacheSceneMax = 0;
            if (UnityPropUtils.Platform != RuntimePlatform.IPhonePlayer)
            {
                if (UnityPropUtils.SystemMemorySize > 3000) //内存3G以上
                {
                    _canCacheSceneMax = 5;
                }
                else if (UnityPropUtils.SystemMemorySize > 2000) //内存2G以上
                {
                    _canCacheSceneMax = 3;
                }
                else if (UnityPropUtils.SystemMemorySize > 1000) //内存1G以上
                {
                    _canCacheSceneMax = 1;
                }
            }
            LoggerHelper.Info(string.Format("SystemInfo.systemMemorySize:{0} SceneMgr._canCacheSceneMax:{1}", UnityPropUtils.SystemMemorySize, _canCacheSceneMax));
        }

        //public List<Vector3> GetFlyPath(string targetScene)
        //{
        //    //LoggerHelper.Error("GetFlyPath: " + targetScene + " ContainsKey: " + _addtiveScenesMgr.FlyPaths.ContainsKey(targetScene) + " " + _addtiveScenesMgr.FlyPaths[targetScene].PackList());
        //    if (_addtiveScenesMgr.FlyPaths.ContainsKey(targetScene))
        //        return _addtiveScenesMgr.FlyPaths[targetScene];
        //    else
        //        return new List<Vector3>();
        //}

        public void SetHideAddStaticPrefabs(bool isHide)
        {
            _sceneResource.SetHideAddStaticPrefabs(isHide);
            _isLowModel = isHide;
            //Debug.LogError("IsLowModel: " + isHide);
        }

        public void LoadScene(LoadSceneSetting loadSceneSetting, Action loaded)
        {
            //LoggerHelper.Error("LoadScene: " + loadSceneSetting.ToString());
            EngineDriver.Instance.StartCoroutine(LoadSceneAsync(loadSceneSetting, loaded));
        }

        public void LeaveScene()
        {
            OnLeaveScene();
        }

        private IEnumerator LoadSceneAsync(LoadSceneSetting loadSceneSetting, Action loaded)
        {
            yield return ClearSceneOnResRelease(loadSceneSetting);
            if (loadSceneSetting.resetAllScenes)
            {
                yield return LoadSceneOnResReady(loadSceneSetting);
            }

            if (_needUnloadUnusedAssets)
            {
                _needUnloadUnusedAssets = false;
                //释放没在使用的资源
                if (loadSceneSetting.clearSceneMemory)
                {
                    yield return Resources.UnloadUnusedAssets();
                }
            }
            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            if (loadSceneSetting.updateProgress) ProgressBar.Instance.UpdateProgress(1f);
            OnEnterScene();

            if (loadSceneSetting.destroyOldSceneObject)
            {
                ObjectPool.Instance.BeforeLoadScene();
                CreateSceneResources(loadSceneSetting, loaded);
            }
            else
            {
                loaded.SafeInvoke();
            }
        }

        SceneResource GetSceneResource(LoadSceneSetting loadSceneSetting)
        {
            if (_sceneResourceCacheName.Contains(loadSceneSetting.sceneName))
            {
                SceneResource oldSceneResource = _sceneResourceCache[loadSceneSetting.sceneName];
                return oldSceneResource;
            }
            return new SceneResource(loadSceneSetting);
        }

        void CacheSceneResource(SceneResource sceneResource)
        {
            int cacheMax = _canCacheSceneMax;
            if (cacheMax == 0)
            {
                _needUnloadUnusedAssets = true;
                return;
            }
            if (_sceneResourceCacheName.Contains(sceneResource.sceneName)) return;
            while (_sceneResourceCacheName.Count >= cacheMax)
            {
                string oldSceneName = _sceneResourceCacheName.Dequeue();
                SceneResource oldSceneResource = _sceneResourceCache[oldSceneName];
                _sceneResourceCache.Remove(oldSceneName);
                _needUnloadUnusedAssets = true;
            }
            _sceneResourceCache.Add(sceneResource.sceneName, sceneResource);
            _sceneResourceCacheName.Enqueue(sceneResource.sceneName);
        }

        void DestroySceneResource()
        {
            if (_sceneResource != null)
            {
                if (_sceneResource.needCache)
                {
                    CacheSceneResource(_sceneResource);
                }
                else
                {
                    _needUnloadUnusedAssets = true;
                }
                _sceneResource.UnloadDynamicScenes();
                _sceneResource = null;
            }
        }

        IEnumerator ClearSceneOnResRelease(LoadSceneSetting loadSceneSetting)
        {
            while (true)
            {
                EntityMgr.Instance.DestroyAllOtherEntities();
                //RenderSettings.ambientLight = defaultAmbientLight;
                if (ObjectPool.Instance.IsNeedLoadingAsset())
                {
                    yield return ObjectPool.Instance.WaitForLoadingAsset();
                }
                if (loadSceneSetting.destroyOldSceneObject)
                {
                    DestroySceneResource();
                }
                if (loadSceneSetting.clearSceneMemory)
                {
                    yield return null;//要等一帧场景销毁逻辑触发
                    //if (loadSceneSetting.updateProgress) ProgressBar.Instance.UpdateProgress(0.3f);
                    //做一次空场景加载，先让unity释放一次资源
                    //yield return SceneManager.LoadSceneAsync(EMPTH_SCENE);
                    //RenderSettings.ambientLight = defaultAmbientLight;
                    //if (loadSceneSetting.updateProgress) ProgressBar.Instance.UpdateProgress(0.25f);
                    ObjectPool.Instance.OnBeforeEnterScene();
                    //if (loadSceneSetting.updateProgress) ProgressBar.Instance.UpdateProgress(0.3f);
                }
                yield break;
            }
        }

        IEnumerator LoadSceneOnResReady(LoadSceneSetting loadSceneSetting)
        {
            while (true)
            {
                if (loadSceneSetting.clearSceneMemory)
                {
                    if (loadSceneSetting.updateProgress) ProgressBar.Instance.UpdateProgress(0.9f);
                    //加载正式的场景
                    if (loadSceneSetting.resetAllScenes)
                    {
                        //LoggerHelper.Error("LoadSceneAsync: " + CONTAINER_SCENE);
                        yield return SceneManager.LoadSceneAsync(CONTAINER_SCENE);
                    }
                    //else
                    //{
                    //    SceneManager.UnloadScene(CONTAINER_SCENE);
                    //    yield return SceneManager.LoadSceneAsync(CONTAINER_SCENE, LoadSceneMode.Additive);
                    //    SceneManager.SetActiveScene(SceneManager.GetSceneByName(CONTAINER_SCENE));
                    //}
                    //RenderSettings.ambientLight = defaultAmbientLight;
                }
                yield break;
            }
        }

        private void OnEnterScene()
        {
            EntityMgr.Instance.OnEnterScene();
        }

        private void OnLeaveScene()
        {
            EntityMgr.Instance.OnLeaveScene();
        }

        //public void CreateSceneResources(string sceneName, Dictionary<string, bool> prefabs, string[] lightmaps, string lightProbes, bool cache = false, Action finish = null, bool isUpdateProgress = true)
        public void CreateSceneResources(LoadSceneSetting loadSceneSetting, Action finish = null)
        {
            SceneResource sceneResource = GetSceneResource(loadSceneSetting);
            sceneResource.IsLowModel = _isLowModel;
            if (_sceneResource != null)
            {
                LoggerHelper.Error("CreateSceneResources!!" + _sceneResource.sceneName);
                _sceneResource = null;
                _needUnloadUnusedAssets = true;
            }
            _sceneResource = sceneResource;
            _sceneResource.sceneTime = loadSceneSetting.sceneTime;
            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            //_addtiveScenesMgr.LoadAddtiveScenes(loadSceneSetting.addtiveSceneNames, loadSceneSetting.sceneName, () =>
            //{
            _sceneResource.LoadDynamicScenes(loadSceneSetting.sceneName, loadSceneSetting.useUnity, () =>
            {
                sw.Stop();
                LoggerHelper.Info(string.Format("LoadScene time: {0}:{1}", loadSceneSetting.sceneName, sw.ElapsedMilliseconds));
                if (DelayedConfig.CanDelayLoading)
                {
                    try
                    {
                        finish.SafeInvoke();
                    }
                    catch (Exception ex)
                    {
                        LoggerHelper.Except(ex);
                    }
                    _sceneResource.LoadEnvData(loadSceneSetting, null);//放在回调之后才加载环境资源，不然会被space设置覆盖
                }
                else
                {
                    _sceneResource.LoadEnvData(loadSceneSetting, finish);//放在回调之后才加载环境资源，不然会被space设置覆盖
                }
            });
            //});
        }

    }
}
