﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using UnityEngine;

using GameLoader.Utils;
using MogoEngine.Events;
using MogoEngine.RPC;

namespace MogoEngine.Mgrs
{
    public class EntityMgrUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class EntityMgr
    {
        private Dictionary<string, Type> m_entityNameType;

        private Type m_DefaultEntityType;

        private Dictionary<uint, Entity> m_entities;
        private List<uint> m_entityIDs;
        private object[] m_attrValue = new object[1];

        private uint m_clientEntityIDBegin = 0;
        private uint m_clientEntityIDMax = 1 << 19;
        private string PLAYER_HEAD = "Player";
        private string SHOW_PLAYER_HEAD = "SkillShow";
        private const string ENTITY_TYPE_NAME_PERI = "Peri";
        private const string ENTITY_TYPE_NAME_FABAO = "Fabao";
        private const string ENTITY_TYPE_NAME_UISHOWENTITY = "UIShowEntity";

        private EntityMgr() 
        {
            m_entityNameType = new Dictionary<string,Type>();
            m_entities = new Dictionary<uint, Entity>();
            m_entityIDs = new List<uint>();
            EventDispatcher.AddEventListener<BaseAttachedInfo>(RPCEvents.EntityAttached, OnEntityAttached);
            EventDispatcher.AddEventListener<CellAttachedInfo>(RPCEvents.EntityCellAttached, OnEntityCellAttached);
            EventDispatcher.AddEventListener<string, object[]>(RPCEvents.RPCResp, RPCCall);
            EventDispatcher.AddEventListener<AttachedInfo>(RPCEvents.AvatarAttriSync, SynAvatarAttri);
            EventDispatcher.AddEventListener<AttachedInfo>(RPCEvents.OtherAttriSync, SynOtherAttri);
            EventDispatcher.AddEventListener<CellAttachedInfo>(RPCEvents.AOINewEntity, CreateAOIEntity);
            EventDispatcher.AddEventListener<uint>(RPCEvents.AOIDelEntity, DestroyEntity);
            EventDispatcher.AddEventListener<CellAttachedInfo>(RPCEvents.EntityPosSync, OnEntityPosSync);
            EventDispatcher.AddEventListener<CellAttachedInfo>(RPCEvents.OtherEntityPosSync, OnOtherEntityPosSync);
            EventDispatcher.AddEventListener<CellAttachedInfo>(RPCEvents.EntityPosTeleport, OnEntityPosTeleport);
            EventDispatcher.AddEventListener<CellAttachedInfo>(RPCEvents.OtherEntityPosTeleport, OnOtherEntityPosTeleport);
            Pluto.GetEntity = GetEntityDef;
            MogoEngine.MogoWorld.RegisterUpdate<EntityMgrUpdateDelegate>("EntityMgr.Update", Update);
        }

        static EntityMgr()
        {
            m_instance = new EntityMgr();
        }

        private static EntityMgr m_instance;
        public static EntityMgr Instance
        {
            get { return m_instance; }
        }

        public Entity m_currentEntity;
        public Entity CurrentEntity
        {
            get { return m_currentEntity; }
            set { m_currentEntity = value; }
        }
        //技能预览实体，标记不删除
        private Entity m_currentShowEntity;

        bool _inSpace = false;
        public void OnEnterScene()
        {
            _inSpace = true;
            foreach (KeyValuePair<uint, Entity> pair in m_entities)
            {
                pair.Value.OnEnterSpace();
            }
        }

        public void OnLeaveScene()
        {
            _inSpace = false;
            foreach (KeyValuePair<uint, Entity> pair in m_entities)
            {
                pair.Value.OnLeaveSpace();
            }
        }

        public void RegisterEntityType(string entityTypeName, Type type)
        {
            if (m_entityNameType.ContainsKey(entityTypeName)) return;
            m_entityNameType.Add(entityTypeName, type);
        }

        public void RegisterDefaultEntityType(Type type)
        {
            m_DefaultEntityType = type;
        }

        private EntityDef GetEntityDef(uint entityId)
        {
            if (m_entities.ContainsKey(entityId)) { return m_entities[entityId].entityDef; }
            return null;
        }

        public bool HasEntity(uint entityId)
        {
            return m_entities.ContainsKey(entityId);
        }

        public bool IsCurrentEntity(uint entityId)
        {
            return m_currentEntity != null ? m_currentEntity.id == entityId : false;
        }

        public Entity GetEntity(uint entityId)
        {
            if (m_entities.ContainsKey(entityId)) return m_entities[entityId];
            return null;
        }

        public Dictionary<uint, Entity> GetEntitys()
        {
            return m_entities;
        }

        public List<uint> GetEntityIDs()
        {
            return m_entityIDs;
        }

        public Entity CreateEntity(uint entityId, string entityTypeName)
        {
            //Debug.LogError("CreateEntity:" + entityId + "::" + entityTypeName);
            Entity entity = null;
            if (m_entities.ContainsKey(entityId)) return m_entities[entityId];
            var type = m_DefaultEntityType;
            if (m_entityNameType.ContainsKey(entityTypeName))
            {
                type = m_entityNameType[entityTypeName];
            }
            entity = (Entity)type.Assembly.CreateInstance(type.FullName);
            entity.id = entityId;
            entity.entityDef = DefParser.Instance.GetEntityByName(entityTypeName);
            entity.entityType = entityTypeName;
            entity.OnInit();
            m_entities.Add(entityId, entity);
            m_entityIDs.Add(entityId);
            if (entityTypeName.StartsWith(PLAYER_HEAD))
            {
                if (m_currentEntity != null)
                {
                    this.DestroyEntity(m_currentEntity.id);
                }
                m_currentEntity = entity;
            }
            //技能预览实体
            if (entityTypeName.StartsWith(SHOW_PLAYER_HEAD))
            {
                if (m_currentShowEntity != null)
                {
                    this.DestroyEntity(m_currentShowEntity.id);
                }
                m_currentShowEntity = entity;
            }
            return entity;
        }

        public Entity CreateEntity(uint entityId, string entityTypeName, Action<Entity> attrSet)
        {
            Entity entity = CreateEntity(entityId, entityTypeName);
            
            if (attrSet != null)
            {
                try
                {
                    attrSet(entity);
                }
                catch (Exception ex)
                {
                    LoggerHelper.Error("CreateClientEntity error:" + "entityID:" + entityId + ":" + entityTypeName + ":" + ex.Message);
                }
            }
            if (entity == null)
            {
                LoggerHelper.Error("[EntityMgr:CreateEntity]=>4________entity == null");
            }
            entity.OnEnterWorld();
            if (_inSpace) entity.OnEnterSpace();
            return entity;
        }

        public Entity CreateClientEntity(string entityTypeName, Action<Entity> attrSet = null)
        {
            return CreateEntity(++m_clientEntityIDBegin, entityTypeName, attrSet);
        }

        public bool IsClientEntity(Entity entity)
        {
            return entity.id < m_clientEntityIDMax;
        }

        public void CreateBaseEntity(BaseAttachedInfo baseInfo)
        {
            //LoggerHelper.Debug("Ari:CreateBaseEntity:" + baseInfo);
            if (baseInfo.entity == null)
            {
                LoggerHelper.Error("CreateBaseEntity Entity Attach Error.");
                return;
            }
            //LoggerHelper.Error("Ari:CreateBaseEntity:" + baseInfo.id + "::" + baseInfo.entity.Name);
            //玩家自身在类型前加Player 以区分于其他玩家
            var playerName = PLAYER_HEAD + baseInfo.entity.Name;
            Entity entity;
            if (m_entityNameType.ContainsKey(playerName))
            {
                entity = CreateEntity(baseInfo.id, playerName);
            }
            else
            {
                entity = CreateEntity(baseInfo.id, baseInfo.entity.Name);
            }
            entity.dbid = baseInfo.dbid;
            entity.entityDef = baseInfo.entity;
            SynEntityAttrs(entity, baseInfo);
            entity.OnEnterWorld();
        }

        public void OnEntityCellAttached(CellAttachedInfo cellInfo)
        {
            //LoggerHelper.Debug("CreateCellEntity:" + cellInfo.id + ":" + cellInfo.entity.Name);
            if (cellInfo.entity == null)
            {
                LoggerHelper.Error("OnEntityCellAttached Entity Attach Error.");
                return;
            }
            if (CurrentEntity == null || CurrentEntity.entityDef.Name != cellInfo.entity.Name || !CurrentEntity.canBeAttached) return;
            SynEntityAttrs(CurrentEntity, cellInfo);
            CurrentEntity.OnCellAttached();
        }

        private Queue<CellAttachedInfo> _aoiCreateEntityQueue = new Queue<CellAttachedInfo>();
        private void Update()
        {
            if (_aoiCreateEntityQueue.Count > 0)
            {
                var cellInfo = _aoiCreateEntityQueue.Dequeue(); 
                DoCreateAOIEntity(cellInfo);
                PlutoDataObjectPool.instance.ReleaseCellAttachedInfo(cellInfo);
                //Debug.LogError("EntityMgr.Update:" + cellInfo.id);
            }
        }

        private List<string> _ServerEntity = new List<string>() 
        {
            "SpaceLoader",
            "GameMgr",
            "UserMgr",
            "BossMgr",
            "MapMgr",
            "TeamMgr",
            "MailMgr",
            "mail_private",
            "mail_public",
            "PeddleMgr",
            "ware"
        };
        public void CreateAOIEntity(CellAttachedInfo cellInfo)
        {
            _aoiCreateEntityQueue.Enqueue(cellInfo);
        }

        public void ClearAOICreateEntityQueue()
        {
            while (_aoiCreateEntityQueue.Count > 0)
            {
                PlutoDataObjectPool.instance.ReleaseCellAttachedInfo(_aoiCreateEntityQueue.Dequeue());
            }
        }

        private Queue<CellAttachedInfo> _aoiDestroyEntityQueueTemp = new Queue<CellAttachedInfo>();
        public void TryDestroyAOIEntityFromQueue(uint entityId)
        {
            while (_aoiCreateEntityQueue.Count > 0)
            {
                var cellInfo = _aoiCreateEntityQueue.Dequeue();
                if (cellInfo.id != entityId)
                {
                    _aoiDestroyEntityQueueTemp.Enqueue(cellInfo);
                }
                else {
                    PlutoDataObjectPool.instance.ReleaseCellAttachedInfo(cellInfo);
                }
            }
            while (_aoiDestroyEntityQueueTemp.Count > 0)
            {
                _aoiCreateEntityQueue.Enqueue(_aoiDestroyEntityQueueTemp.Dequeue());
            }
        }

        public void DoCreateAOIEntity(CellAttachedInfo cellInfo)
        {
            //Debug.LogError("CreateAOIEntity:" + cellInfo.id + ":" + cellInfo.entity.Name);
            //Debug.LogError("CreateAOIEntity:" + cellInfo.id + ":" + cellInfo.eulerAnglesY);
            if (cellInfo.entity == null)
            {
                return;
            }
            if (_ServerEntity.Contains(cellInfo.entity.Name))
            {//服务器专用的前端不处理
                return;
            }
            if (cellInfo.entity == null)
            {
                LoggerHelper.Error("CreateAOIEntity Entity Attach Error.");
                return;
            }

            Entity entity;
            if (m_entities.ContainsKey(cellInfo.id))
            {
                entity = m_entities[cellInfo.id];
                SynEntityAttrsForCreate(entity, cellInfo);
                //entity.SetPosition(new Vector3(cellInfo.position.x, cellInfo.position.y, cellInfo.position.z));
                entity.SetRotation(Quaternion.Euler(cellInfo.eulerAnglesX * 2, cellInfo.eulerAnglesY * 2, cellInfo.eulerAnglesZ * 2));
                entity.Teleport(new Vector3(cellInfo.position.x, cellInfo.position.y, cellInfo.position.z));
            }
            else
            {
                uint cacheTag = CreateEntityFromCache(cellInfo, out entity);
                if (entity == null)
                {
                    entity = CreateEntity(cellInfo.id, cellInfo.entity.Name);
                    if (entity == null) return;//存在服务器entity不处理
                    entity.cacheTag = cacheTag;
                    SynEntityAttrsForCreate(entity, cellInfo);
                    entity.SetPosition(new Vector3(cellInfo.position.x, cellInfo.position.y, cellInfo.position.z));
                    entity.SetRotation(Quaternion.Euler(cellInfo.eulerAnglesX * 2, cellInfo.eulerAnglesY * 2, cellInfo.eulerAnglesZ * 2));
                    entity.OnEnterWorld();
                    if (_inSpace) entity.OnEnterSpace();
                }
                else {
                    SynEntityAttrsForCreate(entity, cellInfo);
                    entity.SetPosition(new Vector3(cellInfo.position.x, cellInfo.position.y, cellInfo.position.z));
                    entity.SetRotation(Quaternion.Euler(cellInfo.eulerAnglesX * 2, cellInfo.eulerAnglesY * 2, cellInfo.eulerAnglesZ * 2));
                    if (_inSpace) entity.OnEnterSpace(); 
                }
            }
        }

        public void DestroyEntity(uint entityId)
        {
            //Debug.LogError("DestroyEntity:" + entityId);
            if (!m_entities.ContainsKey(entityId))
            {
                TryDestroyAOIEntityFromQueue(entityId);
                return;
            }
            var entity = m_entities[entityId];
            EntityPropertyManager.Instance.OnDestroyEntity(entity);
            if (!ReleaseEntityToCache(entity))
            {
                try
                {
                    entity.OnLeaveWorld();
                }
                catch (Exception ex)
                {
                    LoggerHelper.Error(ex.Message);
                }
            }
            m_entities.Remove(entityId);
            m_entityIDs.Remove(entityId);
        }

        public void DestroyAllEntities()
        {
            foreach (KeyValuePair<uint, Entity> pair in m_entities)
            {
                try
                {
                    pair.Value.OnLeaveWorld();
                }
                catch (Exception ex)
                {
                    LoggerHelper.Error(ex.Message);
                }
            }
            ClearAOICreateEntityQueue();
            m_entities.Clear();
            m_entityIDs.Clear();
        }

        private List<uint> m_cacheEntityKeys = new List<uint>();
        public void DestroyAllOtherEntities()
        {
            ClearAOICreateEntityQueue();
            m_cacheEntityKeys.Clear();
            var enumerator = m_entities.GetEnumerator();
            while (enumerator.MoveNext())
            {
                var entity = enumerator.Current.Value;
                //if (m_currentEntity != null && entity.id != m_currentEntity.id)
                //{
                //    m_cacheEntityKeys.Add(entity.id);
                //}
                if (CanClearWhenDestroyAll(entity))
                {
                    m_cacheEntityKeys.Add(entity.id);
                }
            }
            for (int i = 0; i < m_cacheEntityKeys.Count; i++)
            {
                DestroyEntity(m_cacheEntityKeys[i]);
            }
            ClearEntityPool();
        }

        private bool CanClearWhenDestroyAll(Entity entity)
        {
            if (m_currentEntity != null && entity.id == m_currentEntity.id)
            {
                return false;
            }
            if (m_currentShowEntity != null && entity.id == m_currentShowEntity.id)
            {
                return false;
            }
            if (m_currentEntity != null && entity.id == m_currentEntity.pet_eid)
            {
                return false;
            }
            if (m_currentEntity != null && entity.entityType == ENTITY_TYPE_NAME_PERI && entity.owner_id == m_currentEntity.id)
            {
                return false;
            }
            if (m_currentEntity != null && entity.entityType == ENTITY_TYPE_NAME_UISHOWENTITY)
            {
                return false;
            }
            return true;
        }

        public void DestroyAllOtherServerEntities()
        {
            m_cacheEntityKeys.Clear();
            var enumerator = m_entities.GetEnumerator();
            while (enumerator.MoveNext())
            {
                var entity = enumerator.Current.Value;
                if (!IsClientEntity(entity) && entity.id != m_currentEntity.id)
                {
                    m_cacheEntityKeys.Add(entity.id);
                }
            }
            for (int i = 0; i < m_cacheEntityKeys.Count; i++)
            {
                DestroyEntity(m_cacheEntityKeys[i]);
            }
        }

        public void OnEntityAttached(BaseAttachedInfo baseInfo)
        {
            if (m_currentEntity != null && m_currentEntity.id == baseInfo.id)
            {
                if (m_currentEntity.canBeAttached)SynEntityAttrs(m_currentEntity, baseInfo);
            }
            else {
                if (m_currentEntity != null)
                {
                    DestroyEntity(m_currentEntity.id);
                    m_currentEntity = null;
                }
                CreateBaseEntity(baseInfo);   
            }
        }

        public void SynEntityAttrs(Entity entity, AttachedInfo info)
        {
            if (info.props == null)
                return;
            if (!m_entityNameType.ContainsKey(entity.entityType)) 
                return;
            //var type = m_entityNameType[entity.entityType];
            //UnityEngine.Debug.Log(string.Concat("type:", type.Name));
            var props = info.props;
            for (int i = 0; i < props.Count; i++)
            {
                var prop = props[i];
                //UnityEngine.Debug.LogError(string.Concat("entity:", entity.GetType().Name, " : ", prop.Property.Name, " = ", prop.Value));
                SetAttr(entity, prop.Property, prop.Value);
            }
        }

        public void SynEntityAttrsForCreate(Entity entity, AttachedInfo info)
        {
            if (info.props == null)
                return;
            if (!m_entityNameType.ContainsKey(entity.entityType))
                return;

            /*
            var props = info.props;
            for (int i = 0; i < props.Count; i++)
            {
                var prop = props[i];
                SetAttrDelegate(entity, prop.Property.Name, prop.Value);
            }
            entity.SetAttrs(info.props);
            */

            StringBuilder attrsBuffer = new StringBuilder();
            attrsBuffer.Append("return {");
            foreach (var prop in info.props)
            {
                attrsBuffer.Append(prop.Property.Name);
                attrsBuffer.Append("=");
                SetAttrDelegate(entity, prop.Property.Name, prop.Value);
                if (prop.Property.VType == VString.Instance)
                {
                    attrsBuffer.Append("'");
                    attrsBuffer.Append(prop.Value.ToString());
                    attrsBuffer.Append("'");
                }
                else
                {
                    attrsBuffer.Append(prop.Value.ToString());
                }
                attrsBuffer.Append(",");
            }
            attrsBuffer.Append("}");
            entity.SetAttrsByLuaTable(attrsBuffer.ToString()); 
        }

        public void SynAvatarAttri(AttachedInfo info)
        {
            SynEntityAttrs(CurrentEntity, info);
        }

        public void SynOtherAttri(AttachedInfo info)
        {
            if (!m_entities.ContainsKey(info.id))
            {
                return;
            }
            var entity = m_entities[info.id];
            SynEntityAttrs(entity, info);
        }

        private void OnEntityPosSync(CellAttachedInfo info)
        {
            //CurrentEntity.SetPosition(new Vector3(info.position.x,CurrentEntity.position.y, info.position.z));
            CurrentEntity.SetPosition(new Vector3(info.position.x, info.position.y, info.position.z));
            CurrentEntity.SetRotation(Quaternion.Euler(info.eulerAnglesX * 2, info.eulerAnglesY * 2, info.eulerAnglesZ * 2));
        }

        private void OnOtherEntityPosSync(CellAttachedInfo info)
        {
            //UnityEngine.Debug.LogError("OnOtherEntityPosSync:" + info.x + "::" + info.y);
            if (!m_entities.ContainsKey(info.id))
            {
                return;
            }
            var entity = m_entities[info.id];
            //法宝不用服务器数据
            if (entity.entityType == ENTITY_TYPE_NAME_FABAO)
            {
                return;
            }
            //entity.SetPosition(new Vector3(info.position.x,entity.position.y, info.position.z));
            entity.SetPosition(new Vector3(info.position.x, info.position.y, info.position.z));
            entity.SetRotation(Quaternion.Euler(info.eulerAnglesX * 2, info.eulerAnglesY * 2, info.eulerAnglesZ * 2));
        }

        public void OnEntityPosTeleport(CellAttachedInfo info)
        {
            CurrentEntity.Teleport(new Vector3(info.position.x,
                CurrentEntity.position.y, info.position.z));
        }

        public void OnOtherEntityPosTeleport(CellAttachedInfo info)
        {
            if (!m_entities.ContainsKey(info.id))
            {
                return;
            }
            var entity = m_entities[info.id];
            entity.Teleport(new Vector3(info.position.x,
                entity.position.y, info.position.z));
        }

        public void RPCCall(string funcName, object[] args)
        {
            //Debug.LogError("RPCCall:" + funcName + ":" + args.Length);
            var method = CurrentEntity.GetMethodDelegate(funcName);
            if (method != null)
            {
                try
                {
                    method.Invoke(CurrentEntity, args);
                }
                catch (Exception ex)
                {
                    var sb = new System.Text.StringBuilder();
                    sb.Append("method paras are: ");
                    foreach (var methodPara in method.GetParameters())
                    {
                        sb.Append(methodPara.ParameterType + " ");
                    }
                    sb.Append(", rpc resp paras are: ");
                    foreach (var realPara in args)
                    {
                        sb.Append(realPara.GetType() + " ");
                    }

                    Exception inner = ex;
                    while (inner.InnerException != null)
                    {
                        inner = inner.InnerException;
                    }
                    LoggerHelper.Error(String.Format("RPC resp error: method name: {0}, message: {1} {2} {3}", funcName, sb.ToString(), inner.Message, inner.StackTrace));
                }
            }
            else
            {
                CurrentEntity.RpcCallResp(funcName, args);
            }
        }

        /// <summary>
        /// 设置属性值 通过EntityDefProperties
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        protected void SetAttr(Entity entity, EntityDefProperties propInfo, object value)
        {
            SetAttr(entity, propInfo.Name, value);
        }

        /// <summary>
        /// 设置属性值
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        public void SetAttr(Entity entity, string attrName, object value)
        {
            entity.SetAttr(attrName, value);            
            Delegate d = entity.GetPropertySetDelegate(attrName);
            if(d != null)
            {
                m_attrValue[0] = value;
                d.DynamicInvoke(m_attrValue);
            }
            EntityPropertyManager.Instance.TriggerEvent(entity, attrName);
        }

        /// <summary>
        /// 设置属性值
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        public void SetAttrDelegate(Entity entity, string attrName, object value)
        {
            Delegate d = entity.GetPropertySetDelegate(attrName);
            if (d != null)
            {
                m_attrValue[0] = value;
                d.DynamicInvoke(m_attrValue);
            }
            EntityPropertyManager.Instance.TriggerEvent(entity, attrName);
        }

//*************************一下是实体池相关流程**************************************

        private Dictionary<string, Dictionary<uint, Queue<Entity>>> m_entityTypePool;
        private Dictionary<string, Dictionary<uint, Queue<Entity>>> GetEntityTypePool()
        {
            if (m_entityTypePool == null)
            {
                m_entityTypePool = new Dictionary<string, Dictionary<uint, Queue<Entity>>>();
            }
            return m_entityTypePool;
        }

        public void AddEntityTypePool(string typeName, string tagName)
        {
            var pool = GetEntityTypePool();
            pool.Add(typeName, new Dictionary<uint, Queue<Entity>>());
            var entityDef = DefParser.Instance.GetEntityByName(typeName);
            var list = entityDef.PropertiesList;
            for (int i = 0; i < list.Count; i++)
            {
                var data = list[i];
                if (data.Name == tagName)
                {
                    data.CacheTag = true;
                }
            }
        }

        private Queue<Entity> GetEntityPool(string typeName, uint tagValue)
        {
            Queue<Entity> result = null;
            if (tagValue > 0)
            {
                var pool = GetEntityTypePool();
                Dictionary<uint, Queue<Entity>> typeMap = null;
                if (!pool.TryGetValue(typeName, out typeMap))
                {
                    typeMap = new Dictionary<uint, Queue<Entity>>();
                    pool[typeName] = typeMap;
                }

                if (!typeMap.TryGetValue(tagValue, out result))
                {
                    result = new Queue<Entity>();
                    typeMap[tagValue] = result;
                }
            }
            return result;
        }

        private uint CreateEntityFromCache(CellAttachedInfo cellInfo, out Entity entity)
        {
            uint tagValue = 0;
            entity = null;
            string typeName = cellInfo.entity.Name;
            var pool = GetEntityTypePool();
            if (pool.ContainsKey(typeName))
            {

                foreach (var prop in cellInfo.props)
                {
                    if (prop.Property.CacheTag)
                    {
                        tagValue = (uint)prop.Value;
                        break;
                    }
                }

                var queue = GetEntityPool(typeName, tagValue);
                if (queue != null && queue.Count > 0)
                {
                    entity = queue.Dequeue();
                    var entityId = cellInfo.id;
                    entity.id = entityId;
                    m_entities.Add(entityId, entity);
                    m_entityIDs.Add(entityId);
                    entity.OnReuse(entity.id);
                    //Debug.LogError("CreateEntityFromCache:" + typeName);
                }
            }
            return tagValue;
        }

        private bool ReleaseEntityToCache(Entity entity)
        {
            var queue = GetEntityPool(entity.entityType, entity.cacheTag);
            if (queue != null)
            {
                entity.OnRelease();
                queue.Enqueue(entity);
                //Debug.LogError("ReleaseEntityToCache:" + entity.entityType);
                return true;
            }
            return false;
        }

        private void ClearEntityPool()
        {
            var pool = GetEntityTypePool();
            foreach (var pair in pool)
            {
                var typeMap = pair.Value;
                foreach (var pair2 in typeMap)
                {
                    var queue = pair2.Value;
                    while (queue.Count > 0)
                    {
                        queue.Dequeue().OnLeaveWorld();
                    }
                }
                typeMap.Clear();
            }
        }
    }
}
