﻿using UnityEngine;
using MogoEngine.Events;
using GameLoader.Utils;

public class SceneWaitingArea : MonoBehaviour
{
    void OnTriggerEnter(Collider collider)
    {
        EventDispatcher.TriggerEvent("TriggerSceneWaitingArea", name);
    }
}