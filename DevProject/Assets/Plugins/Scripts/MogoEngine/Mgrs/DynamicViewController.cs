﻿using UnityEngine;
using MogoEngine.Events;
using GameLoader.Utils;

public class DynamicViewController : MonoBehaviour
{
    void OnTriggerEnter(Collider collider)
    {
        EventDispatcher.TriggerEvent("TriggerDynamicViewController", name);
    }
}