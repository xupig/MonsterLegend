﻿#region 模块信息
/*==========================================
// 文件名：EntityPropertyManager
// 命名空间: MogoEngine.Mgrs
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/25 16:45:43
// 描述说明：监听实体属性变化 注册对应代理方法
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MogoEngine.RPC;
using UnityEngine.Events;
using UnityEngine;
using MogoEngine.Events;

namespace MogoEngine.Mgrs
{
    public class EntityPropertyManager
    {
        public delegate void VOID_VOID();

        private Dictionary<uint, Dictionary<string, VOID_VOID>> _theRouter;

        private static EntityPropertyManager s_instance;
        public static EntityPropertyManager Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new EntityPropertyManager();
                }
                return s_instance;
            }
        }

        public EntityPropertyManager()
        {
            _theRouter = new Dictionary<uint, Dictionary<string, VOID_VOID>>();
        }

        /**属性改变时调用  只在EntityMgr利用反射修改属性值时使用**/
        public void TriggerEvent(Entity entity, string propertyName)
        {
            if ( entity == null ) return;
            if (_theRouter.ContainsKey(entity.id))
            {

                var entityIDActionMap = _theRouter[entity.id];
                if (entityIDActionMap.ContainsKey(propertyName))
                {
                    if (entityIDActionMap[propertyName] != null)
                    {
                        entityIDActionMap[propertyName]();
                    }
                }
            }
        }

        /**监听某实体的某同步属性变化**/
        public void AddEventListener(Entity entity, string propertyName, VOID_VOID action)
        {
            if (entity == null) return;
            if (!_theRouter.ContainsKey(entity.id))
            {
                _theRouter.Add(entity.id, new Dictionary<string, VOID_VOID>());
            }
            var entityIDActionMap = _theRouter[entity.id];
            if (!entityIDActionMap.ContainsKey(propertyName))
            {
                entityIDActionMap.Add(propertyName, null);
            }
            entityIDActionMap[propertyName] += action;
        }

        /**移除某实体的某同步属性变化监听**/
        public void RemoveEventListener(Entity entity, string propertyName, VOID_VOID action)
        {
            if (entity == null) return;
            if (_theRouter.ContainsKey(entity.id))
            {
                var entityIDActionMap = _theRouter[entity.id];
                if (entityIDActionMap.ContainsKey(propertyName))
                {
                    entityIDActionMap[propertyName] -= action;
                    if (entityIDActionMap[propertyName] == null)
                    {
                        entityIDActionMap.Remove(propertyName);
                    }
                }           
            }
        }

        public void OnDestroyEntity(Entity entity)
        {
            if (_theRouter.ContainsKey(entity.id))
            {
                _theRouter.Remove(entity.id);
            }
        }
    }
}
