﻿// 模块名   :  EventDispatcher
// 创建者   :  Steven Yang
// 创建日期 :  2012-12-12
// 描    述 :  事件注册与分发

using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;

using GameLoader.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace MogoEngine.Events
{
    /// <summary>
    /// 事件处理类。
    /// </summary>
    public class EventController
    {
        private Dictionary<string, GameEventBase> m_theRouter = new Dictionary<string, GameEventBase>();

        internal Dictionary<string, GameEventBase> TheRouter
        {
            get { return m_theRouter; }
        }

        /// <summary>
        /// 永久注册的事件列表
        /// </summary>
        private List<string> m_permanentEvents = new List<string>();

        /// <summary>
        /// 标记为永久注册事件
        /// </summary>
        /// <param name="eventType"></param>
        public void MarkAsPermanent(string eventType)
        {
            m_permanentEvents.Add(eventType);
        }

        /// <summary>
        /// 判断是否已经包含事件
        /// </summary>
        /// <param name="eventType"></param>
        /// <returns></returns>
        public bool ContainsEvent(string eventType)
        {
            return m_theRouter.ContainsKey(eventType);
        }

        /// <summary>
        /// 清除非永久性注册的事件
        /// </summary>
        public void Cleanup()
        {
            List<string> eventToRemove = new List<string>();

            foreach(KeyValuePair<string, GameEventBase> pair in m_theRouter)
            {
                bool wasFound = false;
                foreach(string eventType in m_permanentEvents)
                {
                    if(pair.Key == eventType)
                    {
                        wasFound = true;
                        break;
                    }
                }

                if(!wasFound)
                    eventToRemove.Add(pair.Key);
            }

            foreach(string eventType in eventToRemove)
            {
                m_theRouter.Remove(eventType);
            }
        }

        /// <summary>
        /// 处理增加监听器前的事项， 检查 参数等
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="listenerBeingAdded"></param>
        private bool OnListenerAdding(string eventType, Delegate listenerBeingAdded)
        {
            CheckEventAndActionGenericArguments(m_theRouter[eventType], listenerBeingAdded);
            return true;
        }

        private void CheckEventAndActionGenericArguments(GameEventBase evt, Delegate action)
        {
            Type[] eventTypes = evt.GetType().GetGenericArguments();
            Type[] actionTypes = action.GetType().GetGenericArguments();
            if(eventTypes.Length != actionTypes.Length)
            {
                throw new EventException("添加事件处理函数的参数类型与现有事件处理函数类型不匹配");
            }
            for(int i = 0; i < eventTypes.Length; i++)
            {
                if(eventTypes[i] != actionTypes[i])
                {
                    throw new EventException("添加事件处理函数的参数类型与现有事件处理函数类型不匹配");
                }
            }
        }

        /// <summary>
        /// 移除监听器之前的检查
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="listenerBeingRemoved"></param>
        private bool OnListenerRemoving(string eventType, Delegate listenerBeingRemoved)
        {
            if(m_theRouter.ContainsKey(eventType) == false)
            {
                return false;
            }

            CheckEventAndActionGenericArguments(m_theRouter[eventType], listenerBeingRemoved);
            return true;
        }

        #region 增加监听器
        /// <summary>
        ///  增加监听器， 不带参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        public void AddEventListener(string eventType, Action handler)
        {
            if(m_theRouter.ContainsKey(eventType) == false)
            {
                m_theRouter.Add(eventType, GameEventPool.Get(eventType));
            }
            if(OnListenerAdding(eventType, handler))
            {
                (m_theRouter[eventType] as GameEvent).AddEventListener(handler);
            }
        }

        /// <summary>
        ///  增加监听器， 1个参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        public void AddEventListener<T>(string eventType, Action<T> handler)
        {
            if(m_theRouter.ContainsKey(eventType) == false)
            {
                GameEvent<T> evt;
                if(typeof(T) == typeof(int) || typeof(T) == typeof(string))
                {
                    evt = GameEventPool<T>.Get(eventType);
                }
                else
                {
                    evt = new GameEvent<T>(eventType);
                }
                m_theRouter.Add(eventType, evt);
            }
            if(OnListenerAdding(eventType, handler))
            {
                (m_theRouter[eventType] as GameEvent<T>).AddEventListener(handler);
            }
        }

        /// <summary>
        ///  增加监听器， 2个参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        public void AddEventListener<T, U>(string eventType, Action<T, U> handler)
        {
            if(m_theRouter.ContainsKey(eventType) == false)
            {
                m_theRouter.Add(eventType, new GameEvent<T, U>(eventType));
            }
            if(OnListenerAdding(eventType, handler))
            {
                (m_theRouter[eventType] as GameEvent<T, U>).AddEventListener(handler);
            }
        }

        /// <summary>
        ///  增加监听器， 3个参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        public void AddEventListener<T, U, V>(string eventType, Action<T, U, V> handler)
        {
            if(m_theRouter.ContainsKey(eventType) == false)
            {
                m_theRouter.Add(eventType, new GameEvent<T, U, V>(eventType));
            }
            if(OnListenerAdding(eventType, handler))
            {
                (m_theRouter[eventType] as GameEvent<T, U, V>).AddEventListener(handler);
            }
        }

        /// <summary>
        ///  增加监听器， 4个参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        public void AddEventListener<T, U, V, W>(string eventType, Action<T, U, V, W> handler)
        {
            if(m_theRouter.ContainsKey(eventType) == false)
            {
                m_theRouter.Add(eventType, new GameEvent<T, U, V, W>(eventType));
            }
            if(OnListenerAdding(eventType, handler))
            {
                (m_theRouter[eventType] as GameEvent<T, U, V, W>).AddEventListener(handler);
            }
        }
        #endregion

        #region 移除监听器

        /// <summary>
        ///  移除监听器， 不带参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        public void RemoveEventListener(string eventType, Action handler)
        {
            if(OnListenerRemoving(eventType, handler))
            {
                GameEvent evt = m_theRouter[eventType] as GameEvent;
                evt.RemoveEventListener(handler);
                if(evt.GetListenerCount() == 0)
                {
                    m_theRouter.Remove(eventType);
                    GameEventPool.Recycle(evt);
                }
            }
        }

        /// <summary>
        ///  移除监听器， 1个参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        public void RemoveEventListener<T>(string eventType, Action<T> handler)
        {
            if(OnListenerRemoving(eventType, handler))
            {
                GameEvent<T> evt = m_theRouter[eventType] as GameEvent<T>;
                evt.RemoveEventListener(handler);
                if(evt.GetListenerCount() == 0)
                {
                    m_theRouter.Remove(eventType);
                    if(typeof(T) == typeof(int) || typeof(T) == typeof(string))
                    {
                        GameEventPool<T>.Recycle(evt);
                    }
                }
            }
        }

        /// <summary>
        ///  移除监听器， 2个参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        public void RemoveEventListener<T, U>(string eventType, Action<T, U> handler)
        {
            if(OnListenerRemoving(eventType, handler))
            {
                GameEvent<T, U> evt = m_theRouter[eventType] as GameEvent<T, U>;
                evt.RemoveEventListener(handler);
                if(evt.GetListenerCount() == 0)
                {
                    m_theRouter.Remove(eventType);
                }
            }
        }

        /// <summary>
        ///  移除监听器， 3个参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        public void RemoveEventListener<T, U, V>(string eventType, Action<T, U, V> handler)
        {
            if(OnListenerRemoving(eventType, handler))
            {
                GameEvent<T, U, V> evt = m_theRouter[eventType] as GameEvent<T, U, V>;
                evt.RemoveEventListener(handler);
                if(evt.GetListenerCount() == 0)
                {
                    m_theRouter.Remove(eventType);
                }
            }
        }

        /// <summary>
        ///  移除监听器， 4个参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        public void RemoveEventListener<T, U, V, W>(string eventType, Action<T, U, V, W> handler)
        {
            if(OnListenerRemoving(eventType, handler))
            {
                GameEvent<T, U, V, W> evt = m_theRouter[eventType] as GameEvent<T, U, V, W>;
                evt.RemoveEventListener(handler);
                if(evt.GetListenerCount() == 0)
                {
                    m_theRouter.Remove(eventType);
                }
            }
        }
        #endregion

        #region 触发事件
        /// <summary>
        ///  触发事件， 不带参数触发
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        public void TriggerEvent(string eventType)
        {
            GameEventBase evt;
            if(m_theRouter.TryGetValue(eventType, out evt) == false)
            {
                return;
            }
            try
            {
                (evt as GameEvent).Invoke();
            }
            catch(Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        /// <summary>
        ///  触发事件， 带1个参数触发
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        public void TriggerEvent<T>(string eventType, T arg1)
        {
            GameEventBase evt;
            if(m_theRouter.TryGetValue(eventType, out evt) == false)
            {
                return;
            }
            try
            {
                (evt as GameEvent<T>).Invoke(arg1);
            }
            catch(Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        /// <summary>
        ///  触发事件， 带2个参数触发
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        public void TriggerEvent<T, U>(string eventType, T arg1, U arg2)
        {
            GameEventBase evt;
            if(m_theRouter.TryGetValue(eventType, out evt) == false)
            {
                return;
            }
            try
            {
                (evt as GameEvent<T, U>).Invoke(arg1, arg2);
            }
            catch(Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        /// <summary>
        ///  触发事件， 带3个参数触发
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        public void TriggerEvent<T, U, V>(string eventType, T arg1, U arg2, V arg3)
        {
            GameEventBase evt;
            if(m_theRouter.TryGetValue(eventType, out evt) == false)
            {
                return;
            }
            try
            {
                (evt as GameEvent<T, U, V>).Invoke(arg1, arg2, arg3);
            }
            catch(Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        /// <summary>
        ///  触发事件， 带4个参数触发
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        public void TriggerEvent<T, U, V, W>(string eventType, T arg1, U arg2, V arg3, W arg4)
        {
            GameEventBase evt;
            if(m_theRouter.TryGetValue(eventType, out evt) == false)
            {
                return;
            }
            try
            {
                (evt as GameEvent<T, U, V, W>).Invoke(arg1, arg2, arg3, arg4);
            }
            catch(Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        #endregion
    }

    /// <summary>
    /// 事件分发函数。
    /// 提供事件注册， 反注册， 事件触发
    /// 采用 delegate, dictionary 实现
    /// 支持自定义事件。 事件采用字符串方式标识
    /// 支持 0，1，2，3 等4种不同参数个数的回调函数
    /// </summary>
    public class EventDispatcher
    {
        private static EventController m_eventController = new EventController();

        internal static Dictionary<string, GameEventBase> TheRouter
        {
            get { return m_eventController.TheRouter; }
        }

        /// <summary>
        /// 标记为永久注册事件
        /// </summary>
        /// <param name="eventType"></param>
        static public void MarkAsPermanent(string eventType)
        {
            m_eventController.MarkAsPermanent(eventType);
        }

        /// <summary>
        /// 清除非永久性注册的事件
        /// </summary>
        static public void Cleanup()
        {
            m_eventController.Cleanup();
        }

        #region 增加监听器
        /// <summary>
        ///  增加监听器， 不带参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        static public void AddEventListener(string eventType, Action handler)
        {
            m_eventController.AddEventListener(eventType, handler);
        }

        /// <summary>
        ///  增加监听器， 1个参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        static public void AddEventListener<T>(string eventType, Action<T> handler)
        {
            m_eventController.AddEventListener(eventType, handler);
        }

        /// <summary>
        ///  增加监听器， 2个参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        static public void AddEventListener<T, U>(string eventType, Action<T, U> handler)
        {
            m_eventController.AddEventListener(eventType, handler);
        }

        /// <summary>
        ///  增加监听器， 3个参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        static public void AddEventListener<T, U, V>(string eventType, Action<T, U, V> handler)
        {
            m_eventController.AddEventListener(eventType, handler);
        }

        /// <summary>
        ///  增加监听器， 4个参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        static public void AddEventListener<T, U, V, W>(string eventType, Action<T, U, V, W> handler)
        {
            m_eventController.AddEventListener(eventType, handler);
        }
        #endregion

        #region 移除监听器
        /// <summary>
        ///  移除监听器， 不带参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        static public void RemoveEventListener(string eventType, Action handler)
        {
            m_eventController.RemoveEventListener(eventType, handler);
        }

        /// <summary>
        ///  移除监听器， 1个参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        static public void RemoveEventListener<T>(string eventType, Action<T> handler)
        {
            m_eventController.RemoveEventListener(eventType, handler);
        }

        /// <summary>
        ///  移除监听器， 2个参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        static public void RemoveEventListener<T, U>(string eventType, Action<T, U> handler)
        {
            m_eventController.RemoveEventListener(eventType, handler);
        }

        /// <summary>
        ///  移除监听器， 3个参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        static public void RemoveEventListener<T, U, V>(string eventType, Action<T, U, V> handler)
        {
            m_eventController.RemoveEventListener(eventType, handler);
        }

        /// <summary>
        ///  移除监听器， 4个参数
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        static public void RemoveEventListener<T, U, V, W>(string eventType, Action<T, U, V, W> handler)
        {
            m_eventController.RemoveEventListener(eventType, handler);
        }
        #endregion

        #region 触发事件
        /// <summary>
        ///  触发事件， 不带参数触发
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        static public void TriggerEvent(string eventType)
        {
            m_eventController.TriggerEvent(eventType);
        }

        /// <summary>
        ///  触发事件， 带1个参数触发
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        static public void TriggerEvent<T>(string eventType, T arg1)
        {
            m_eventController.TriggerEvent(eventType, arg1);
        }

        /// <summary>
        ///  触发事件， 带2个参数触发
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        static public void TriggerEvent<T, U>(string eventType, T arg1, U arg2)
        {
            m_eventController.TriggerEvent(eventType, arg1, arg2);
        }

        /// <summary>
        ///  触发事件， 带3个参数触发
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        static public void TriggerEvent<T, U, V>(string eventType, T arg1, U arg2, V arg3)
        {
            m_eventController.TriggerEvent(eventType, arg1, arg2, arg3);
        }

        /// <summary>
        ///  触发事件， 带4个参数触发
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="handler"></param>
        static public void TriggerEvent<T, U, V, W>(string eventType, T arg1, U arg2, V arg3, W arg4)
        {
            m_eventController.TriggerEvent(eventType, arg1, arg2, arg3, arg4);
        }


        #endregion
    }
}