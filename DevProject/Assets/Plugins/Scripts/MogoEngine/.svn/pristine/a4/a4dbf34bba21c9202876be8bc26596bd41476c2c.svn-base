﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：AttachedEntity
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.4.17
// 模块描述：实体获取数据结构定义。
//==========================================*/
#endregion
using System.Collections.Generic;
using UnityEngine;

using GameLoader.Utils.CustomType;
using MogoEngine.Events;
using System;
using System.Reflection;
using System.Globalization;
using GameLoader.Utils;

namespace MogoEngine.RPC
{
    public class Entity
    {
        private static LuaTable s_sharedLuaTable = new LuaTable();

        public uint id;
        public ulong dbid;
        public string entityType;
        public ushort typeId;
        public Vector3 position;
        public Quaternion rotation;
        public EntityDef entityDef;
        public bool isInWorld = false;
        //用于断线重连时标记某些情况下不同步服务器属性，如在客户端副本中。
        public bool canBeAttached = true;

        public bool isOpenWait = true;

        public uint cacheTag = 0; 

        private Matrix4x4 _localToWorldMatrix;

        private uint _pet_eid = 0;
        public uint pet_eid
        {
            get { return _pet_eid; }
            set { _pet_eid = value; }
        }

        private uint _owner_id = 0;
        public uint owner_id
        {
            get { return _owner_id; }
            set { _owner_id = value; }
        }

        //Entity属性设置Delegate字典，减少运行时反射的使用量
        private Dictionary<string, Delegate> _propertySetDelegateDict = new Dictionary<string, Delegate>(24);

        //Entity方法调用Delegate字典，减少运行时反射的使用量
        private Dictionary<string, MethodInfo> _methodDelegateDict = new Dictionary<string, MethodInfo>(12);

        public Action OnEnterWorldCB;

        public Action OnLeaveWorldCB;

        virtual public void OnInit() { }

        virtual public void OnEnterWorld()
        {
            isInWorld = true;
            OnEnterWorldCB.SafeInvoke();
        }

        virtual public void OnLeaveWorld()
        {
            isInWorld = false;
            OnLeaveWorldCB.SafeInvoke();
        }

        virtual public void OnReuse(uint id) 
        {
            isInWorld = true;
        }

        virtual public void OnRelease() 
        {
            isInWorld = false;
        }

        virtual public void OnEnterSpace() { }

        virtual public void OnLeaveSpace() { }

        virtual public void OnCellAttached() { }

        virtual public void SetAttr(string attrName, object value) { }

        virtual public void SetAttrs(List<EntityPropertyValue> props) { }
        
        virtual public void SetAttrsByLuaTable(string luaTableStr) { }

        // 服务器远程过程调用
        public void RpcCall(string func, params object[] args)
        {
            ServerProxy.Instance.RpcCall(func, args);
            /*
            string log = "[RpcCall]:[" + func + "] ";
            foreach (object arg in args)
            {
                log = string.Concat(log, " ", arg.ToString());
            }
            if (func != "sync_time_req")
            {
                UnityEngine.Debug.Log(log);
            }*/
            //UnityEngine.Debug.Log("RpcCall:" + func + " "+string.Concat(args));
        }

        virtual public void RpcCallResp(string functionName, object[] args)
        {

            if (args != null && args.Length > 0 && (args[0] is int || args[0] is uint || args[0] is UInt16))
            {
                //int actionId = int.Parse(args[0].ToString());
                int actionId = (args[0] as IConvertible).ToInt32(NumberFormatInfo.CurrentInfo);
                if (waitDict.ContainsKey(actionId))
                {
                    waitDict[actionId] = 0;
                }
            }
        }

        public void ClearWait(int actionId)
        {
            if (waitDict.ContainsKey(actionId))
            {
                waitDict[actionId] = 0;
            }
        }

        public MethodInfo timeStamp;
        public object target;
        private static Dictionary<int, UInt64> waitDict = new Dictionary<int, UInt64>();

        public void RegisterWait(int actionId)
        {
            if (waitDict.ContainsKey(actionId) == false)
            {
                waitDict.Add(actionId, 0);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="func"></param>
        /// <param name="actionId"></param>
        /// <param name="args"></param>
        public void ActionRpcCall(string func, int actionId, params object[] args)
        {
            if (!WaitCheck(actionId)) { return; }
            s_sharedLuaTable.Clear();
            for (int i = 0; i < args.Length; i++)
            {
                s_sharedLuaTable.Add(i + 1, args[i]);
            }

            RpcCall(func, actionId, s_sharedLuaTable);
        }

        private bool WaitCheck(int actionId)
        {
            if (isOpenWait && waitDict.ContainsKey(actionId))
            {
                ulong time = GetServerTimeStamp();
                if (time - waitDict[actionId] < 5000)
                {
                    EventDispatcher.TriggerEvent(RPCEvents.RPCWait);
                    return false;
                }
                waitDict[actionId] = time;
            }
            return true;
        }

        private ulong GetServerTimeStamp()
        {
            ulong time = 100000;
            if (timeStamp != null)
            {
                time = ulong.Parse(timeStamp.Invoke(target, null).ToString());
            }
            return time;
        }

        public void ListRpcCall(string func, int actionId, int[] args)
        {
            s_sharedLuaTable.Clear();
            for (int i = 0; i < args.Length; i++)
            {
                s_sharedLuaTable.Add(i + 1, args[i]);
            }
            RpcCall(func, actionId, s_sharedLuaTable);
        }


        public virtual void SetPosition(Vector3 newPosition)
        {
            position = newPosition;
        }

        public virtual void SetRotation(Quaternion newRotation)
        {
            rotation = newRotation;
        }

        public virtual void SetEuler(Vector3 newEuler)
        {
            rotation = Quaternion.Euler(newEuler);
        }

        public virtual void Teleport(Vector3 newPosition)
        {
            position = newPosition;
        }

        public virtual Transform GetTransform()
        {
            return null;
        }

        public Matrix4x4 localToWorldMatrix
        {
            get
            {
                if (_localToWorldMatrix == null)
                {
                    _localToWorldMatrix = new Matrix4x4();
                }
                if (rotation.x == 0 && rotation.y == 0 && rotation.z == 0 && rotation.w == 0)
                {
                    rotation = Quaternion.Euler(Vector3.zero);
                }
                _localToWorldMatrix.SetTRS(position, rotation, Vector3.one);
                return _localToWorldMatrix;
            }
        }

        public void RegisterPropertySetDelegate(Type delegateType, string attrName)
        {
            if (_propertySetDelegateDict.ContainsKey(attrName) == false)
            {
                try
                {
                    PropertyInfo propInfo = this.GetType().GetProperty(attrName);
                    Delegate d = Delegate.CreateDelegate(delegateType, this, propInfo.GetSetMethod());
                    _propertySetDelegateDict.Add(attrName, d);
                }
                catch (Exception e)
                {
                    LoggerHelper.Error("Entity属性Set函数委托设置失败 " + attrName + "\n" + e.Message);
                }

            }
        }

        public Delegate GetPropertySetDelegate(string attrName)
        {
            Delegate d;
            _propertySetDelegateDict.TryGetValue(attrName, out d);
            return d;
        }

        public void RegisterMethodDelegate(string methodName)
        {
            if (this._methodDelegateDict.ContainsKey(methodName) == false)
            {
                try
                {
                    MethodInfo method = this.GetType().GetMethod(methodName, ~System.Reflection.BindingFlags.NonPublic
                            | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                    _methodDelegateDict.Add(methodName, method);
                }
                catch (Exception e)
                {
                    LoggerHelper.Error("Entity方法委托设置失败 " + e.Message);
                }

            }
        }

        public MethodInfo GetMethodDelegate(string methodName)
        {
            MethodInfo methodInfo;
            _methodDelegateDict.TryGetValue(methodName, out methodInfo);
            return methodInfo;
        }
    }

    public class AttachedInfo
    {
        public uint id { get; set; }
        public ushort typeId { get; set; }
        public EntityDef entity { get; set; }
        private List<EntityPropertyValue> _props = null;
        public List<EntityPropertyValue> props
        {
            get
            {
                if (_props == null) _props = new List<EntityPropertyValue>();
                return _props;
            }
            set{ _props = value;}
        }
        public override string ToString()
        {
            return String.Format("ai-id:{0},type:{1},ety:{2},props:{3}", id, typeId, entity, props == null ? null : props.PackList());
        }

        public virtual void ClearData()
        {
            id = (uint)0;
            typeId = (ushort)0;
            entity = null;
            if (props != null)
            {
                PlutoDataObjectPool.instance.ReleaseEntityPropertyValueList(props);
            }
        }
    }

    public class CellAttachedInfo : AttachedInfo
    {
        public byte eulerAnglesX { get; set; }
        public byte eulerAnglesY { get; set; }
        public byte eulerAnglesZ { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int z { get; set; }
        public uint checkFlag { get; set; }
        public UnityEngine.Vector3 position { get { return new UnityEngine.Vector3(x * 0.01f, y * 0.01f, z * 0.01f); } }//服务器坐标以厘米为单位，客户端以米为单位
                                                                                                                        //public UnityEngine.Vector3 rotation { get { return new UnityEngine.Vector3(0, face * 2, 0); } }//服务器朝向值为0-180，客户端直接放大一倍
        public override string ToString()
        {
            return String.Format("cai-id:{0},type:{1},ety:{2},ex:{3},ey:{4},ez:{5},x:{6},y:{7},z:{8},cf:{9},pos:{10},props:{11}",
                id, typeId, entity, eulerAnglesX, eulerAnglesY, eulerAnglesZ, x, y, z, checkFlag, position, props == null ? null : props.PackList());
        }

        public override void ClearData()
        {
            base.ClearData();
            eulerAnglesX = 0;
            eulerAnglesY = 0;
            eulerAnglesZ = 0;
            x = 0;
            y = 0;
            z = 0;
            checkFlag = 0;
        }
    }

    public class BaseAttachedInfo : AttachedInfo
    {
        public ulong dbid { get; set; }

        public override string ToString()
        {
            return String.Format("bai-id:{0},type:{1},ety:{2},dbid:{3},props:{4}", id, typeId, entity, dbid, props == null ? null : props.PackList());
        }

        public override void ClearData()
        {
            base.ClearData();
            dbid = 0;
        }
    }

    public class EntityPropertyValue
    {
        public EntityDefProperties Property { get; set; }
        public object Value { get; set; }

        public EntityPropertyValue() { }

        public EntityPropertyValue(EntityDefProperties property, object value)
        {
            Property = property;
            Value = value;
        }

        public void SetData(EntityDefProperties property, object value)
        {
            Property = property;
            Value = value;
        }

        public override string ToString()
        {
            if (Property.VType == VBLOB.Instance)
                return String.Format("{0}:{1}", Property, (Value as byte[]).PackArray());
            else
                return String.Format("{0}:{1}", Property, Value);
        }

        public void ClearData()
        {
            Property = null;
            Value = null;
        }
    }
}