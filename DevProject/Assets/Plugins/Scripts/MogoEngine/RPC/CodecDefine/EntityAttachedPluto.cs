﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：EntityAttachedPluto
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.4.17
// 模块描述：实体获取编解码处理类。
//==========================================*/
#endregion
using System;
using System.Collections.Generic;


using MogoEngine.Events;
using UnityEngine;
using GameLoader.Utils;
namespace MogoEngine.RPC
{
    public class EntityAttachedPluto : Pluto
    {
        /// <summary>
        /// 将远程调用的方法解码为EntityAttachedPluto调用。
        /// </summary>
        /// <param name="data">远程调用方法的二进制数组</param>
        /// <param name="unLen">数据偏移量</param>
        static BaseAttachedInfo info;
        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {
            //UnityEngine.Debug.LogError("====      EntityAttachedPluto DoDecode");
            info = PlutoDataObjectPool.instance.GetBaseAttachedInfo();
            //ushort klen = (ushort)VUInt16.Instance.Decode(data, ref unLen, true);
            string key = (string)VString.Instance.Decode(data, ref unLen);
            EventDispatcher.TriggerEvent<string>(RPCEvents.ReConnectKey, key);
            info.typeId = VUInt16.Instance.Decode(data, ref unLen, true); //entity type id
            info.id = VUInt32.Instance.Decode(data, ref unLen, true);//entity unique id
            info.dbid = VUInt32.Instance.Decode(data, ref unLen, true); //dbid
            var entity = DefParser.Instance.GetEntityByID(info.typeId);
            if (entity != null)
            {
                info.entity = entity;
                while (unLen < unEnd)
                {//还有数据就解析
                    var index = VUInt16.Instance.Decode(data, ref unLen, true);
                    EntityDefProperties prop;
                    var flag = entity.Properties.TryGetValue(index, out prop);
                    try
                    {
                        if (flag)
                        {
                            info.props.Add(PlutoDataObjectPool.instance.GetEntityPropertyValue(prop, prop.VType.Decode(data, ref unLen)));
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerHelper.Error("prop Set Error::" + index + "--" + prop.Name + "," + ex.Message + "," + prop.VType + "," + prop.VType.Decode(data, ref unLen));
                    }
                }
            }
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.MSGID_CLIENT_ENTITY_ATTACHED, info);
            }
        }

        public override void HandleData()
        {
            CurrentEntity = info.entity;
            EventDispatcher.TriggerEvent<BaseAttachedInfo>(RPCEvents.EntityAttached, info);
            PlutoDataObjectPool.instance.ReleaseBaseAttachedInfo(info);
            //UnityEngine.Debug.LogError("====EntityAttachedPluto HandleData");
        }

        /// <summary>
        /// 创建新EntityAttachedPluto实例。
        /// </summary>
        /// <returns>EntityAttachedPluto实例</returns>
        static EntityAttachedPluto pluto = new EntityAttachedPluto();
        internal static Pluto Create()
        {
            return pluto;
        }
    }
}