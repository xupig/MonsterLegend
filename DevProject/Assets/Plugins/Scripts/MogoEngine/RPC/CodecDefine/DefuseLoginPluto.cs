﻿using System;


using MogoEngine.Events;

namespace MogoEngine.RPC
{
	class DefuseLoginPluto : Pluto
	{
        public Byte[] Encode()
        {
            return null;
        }

        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {
        }

        public override void HandleData()
        {
            EventDispatcher.TriggerEvent(RPCEvents.DefuseLogin);
        }
        
        static DefuseLoginPluto pluto = new DefuseLoginPluto();
        internal static Pluto Create()
        {
            return pluto;
        }
	}
}
