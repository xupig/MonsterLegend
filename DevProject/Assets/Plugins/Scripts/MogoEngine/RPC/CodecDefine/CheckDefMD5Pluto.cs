﻿using System;

using GameLoader.Utils;


using MogoEngine.Events;

namespace MogoEngine.RPC
{
    public class CheckDefMD5Pluto : Pluto
    {
        public Byte[] Encode(Byte[] bytes)
        {
            Push(VUInt16.Instance.Encode((UInt16)MSGIDType.LOGINAPP_CHECK));
            var str = MD5Utils.FormatMD5(bytes);
            Push(VString.Instance.Encode(str));

            Byte[] result = new Byte[m_unLen];
            Buffer.BlockCopy(m_szBuff, 0, result, 0, m_unLen);
            EndPluto(result);
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Send(MSGIDType.CLIENT_CHECK_RESP, bytes.PackArray());
            }
            return result;
        }

        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {
            FuncName = MSGIDType.CLIENT_CHECK_RESP.ToString();

            Arguments = new Object[1];
            Arguments[0] = VUInt8.Instance.Decode(data, ref unLen);
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.CLIENT_CHECK_RESP, Arguments);
            }
        }

        public override void HandleData()
        {
            var result = (DefCheckResult)(byte)Arguments[0];
            EventDispatcher.TriggerEvent<DefCheckResult>(RPCEvents.CheckDef, result);
        }

        static CheckDefMD5Pluto pluto = new CheckDefMD5Pluto();
        internal static Pluto Create()
        {
            return pluto;
        }
    }
}
