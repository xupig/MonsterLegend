﻿using System;

namespace MogoEngine.RPC
{
    public class MovePluto : Pluto
    {
        Byte[] _encodeBuff = null;
        /// <summary>
        /// 将远程方法调用编码为二进制数组。
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <param name="x3"></param>
        /// <param name="y3"></param>
        /// <returns>编码后的二进制数组</returns>
        public Byte[] Encode(byte eulerAnglesX, byte eulerAnglesY, byte eulerAnglesZ, int x, int y, int z)
        {
            Push(VUInt16.Instance.Encode((UInt16)MSGIDType.MSGID_BASEAPP_CLIENT_MOVE_REQ));
            Push(VUInt8.Instance.Encode(eulerAnglesX));
            Push(VUInt8.Instance.Encode(eulerAnglesY));
            Push(VUInt8.Instance.Encode(eulerAnglesZ));
            Push(VInt32.Instance.Encode(x));
            Push(VInt32.Instance.Encode(y));
            Push(VInt32.Instance.Encode(z));

            if (_encodeBuff == null)
            {
                _encodeBuff = new Byte[m_unLen];
            }
            Buffer.BlockCopy(m_szBuff, 0, _encodeBuff, 0, m_unLen);
            EndPluto(_encodeBuff);
			
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Send(MSGIDType.MSGID_BASEAPP_CLIENT_MOVE_REQ, eulerAnglesX, eulerAnglesY, eulerAnglesZ, x, y, z);
            }
            return _encodeBuff;
        }

        /// <summary>
        /// 将远程调用的方法解码为MovePluto调用。
        /// </summary>
        /// <param name="data">远程调用方法的二进制数组</param>
        /// <param name="unLen">数据偏移量</param>
        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {

        }

        public override void HandleData()
        {
        }

        /// <summary>
        /// 创建新MovePluto实例。
        /// </summary>
        /// <returns>MovePluto实例</returns>
        static MovePluto pluto = new MovePluto();
        internal static Pluto Create()
        {
            return pluto;
        }
    }
}