﻿using System;
using System.Collections.Generic;

using GameLoader.Utils;

using MogoEngine.Events;


namespace MogoEngine.RPC
{
    /// <summary>
    /// 一组entity数据
    /// </summary>
    class AOIEntitiesPluto : Pluto
    {
        List<CellAttachedInfo> _list = new List<CellAttachedInfo>();
        CellAttachedInfo info = null;
        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {
            if (_list.Count > 0) _list.Clear();
            info = PlutoDataObjectPool.instance.GetCellAttachedInfo();
            //UnityEngine.Debug.LogError("unLen:" + unLen + ": data.Length:" + data.Length);
            info.typeId = (ushort)VUInt16.Instance.Decode(data, ref unLen, true); //etype
            info.id = VUInt32.Instance.Decode(data, ref unLen, true); // eid
            info.eulerAnglesX = (byte)VUInt8.Instance.Decode(data, ref unLen);
            info.eulerAnglesY = (byte)VUInt8.Instance.Decode(data, ref unLen);
            info.eulerAnglesZ = (byte)VUInt8.Instance.Decode(data, ref unLen);
            info.x = VInt32.Instance.Decode(data, ref unLen, true);
            info.y = VInt32.Instance.Decode(data, ref unLen, true);
            info.z = VInt32.Instance.Decode(data, ref unLen, true);
            //list.Add(info);//暂时忽略自身数据
            //UnityEngine.Debug.LogError("++++++++++++++DoDecode:" + info.eulerAnglesX + ":" + info.eulerAnglesY + ":" + info.eulerAnglesZ);
            //UnityEngine.Debug.LogError("unLen:" + unLen + ": data.Length:" + data.Length);
            while (unLen < unEnd)
            {
                var entityInfo = PlutoDataObjectPool.instance.GetCellAttachedInfo();
                var entityLength = VUInt16.Instance.Decode(data, ref unLen, true); //单个entity数据总长度
                var endIdx = unLen + entityLength; //结束位置
                entityInfo.typeId = VUInt16.Instance.Decode(data, ref unLen, true);
                entityInfo.id = VUInt32.Instance.Decode(data, ref unLen, true); // eid
                entityInfo.eulerAnglesX = (byte)VUInt8.Instance.Decode(data, ref unLen);
                entityInfo.eulerAnglesY = (byte)VUInt8.Instance.Decode(data, ref unLen);
                entityInfo.eulerAnglesZ = (byte)VUInt8.Instance.Decode(data, ref unLen);
                entityInfo.x = VInt32.Instance.Decode(data, ref unLen, true);
                entityInfo.y = VInt32.Instance.Decode(data, ref unLen, true);
                entityInfo.z = VInt32.Instance.Decode(data, ref unLen, true);
                var entity = DefParser.Instance.GetEntityByID(entityInfo.typeId);
                if (entity != null && entity.BHasCellClient)
                {
                    entityInfo.entity = entity;
                    while (unLen < endIdx)
                    {//还有数据就解析
                        var index = VUInt16.Instance.Decode(data, ref unLen, true);
                        EntityDefProperties prop;
                        var flag = entity.Properties.TryGetValue((ushort)index, out prop);
                        if (flag) {
                            entityInfo.props.Add(PlutoDataObjectPool.instance.GetEntityPropertyValue(prop, prop.VType.Decode(data, ref unLen)));
                        }
                    }
                    _list.Add(entityInfo);

                }
                else
                {
                    if (entity != null)
                    {
                        LoggerHelper.Warning(string.Format("EntityType {0} is not supported in client!!!", entity.Name));
                    }
                }
            }
            //Arguments = new Object[1];
            //Arguments[0] = list;
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.MSGID_CLIENT_AOI_ENTITIES, _list.PackList());
            }
        }

        public override void HandleData()
        {
            if (info != null)
            {
                PlutoDataObjectPool.instance.ReleaseCellAttachedInfo(info);
            }
            if (_list.Count > 0)
            {
                foreach (CellAttachedInfo v in _list)
                {
                    //UnityEngine.Debug.LogError("-AOIEntities info: " + v.ToString());
                    EventDispatcher.TriggerEvent<CellAttachedInfo>(RPCEvents.AOINewEntity, v);
                }
                //PlutoDataObjectPool.instance.ReleaseCellAttachedInfoList(_list);
            }
        }

        /// <summary>
        /// 创建新EntityAttachedPluto实例。
        /// </summary>
        /// <returns>EntityAttachedPluto实例</returns>
        static AOIEntitiesPluto pluto = new AOIEntitiesPluto();
        internal static Pluto Create()
        {
            return pluto;
        }
    }
}
