﻿using System;
using System.Collections.Generic;


using MogoEngine.Events;
using GameLoader.Utils;
namespace MogoEngine.RPC
{
	class AOINewEntityPluto : Pluto
	{
        CellAttachedInfo _info;
        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {
            _info = PlutoDataObjectPool.instance.GetCellAttachedInfo();
            _info.typeId = VUInt16.Instance.Decode(data, ref unLen, true);
            _info.id = VUInt32.Instance.Decode(data, ref unLen, true); // eid
            _info.eulerAnglesX = (byte)VUInt8.Instance.Decode(data, ref unLen);
            _info.eulerAnglesY = (byte)VUInt8.Instance.Decode(data, ref unLen);
            _info.eulerAnglesZ = (byte)VUInt8.Instance.Decode(data, ref unLen);
            _info.x = VInt32.Instance.Decode(data, ref unLen, true);
            _info.y = VInt32.Instance.Decode(data, ref unLen, true);
            _info.z = VInt32.Instance.Decode(data, ref unLen, true);
            var entity = DefParser.Instance.GetEntityByID(_info.typeId);
            if (entity != null && entity.BHasCellClient)
            {
                _info.entity = entity;
                while (unLen < unEnd)
                {//还有数据就解析
                    var index = VUInt16.Instance.Decode(data, ref unLen, true);
                    EntityDefProperties prop;
                    var flag = entity.Properties.TryGetValue((ushort)index, out prop);
                    if (flag)
                    {
                        _info.props.Add(PlutoDataObjectPool.instance.GetEntityPropertyValue(prop, prop.VType.Decode(data, ref unLen)));
                    }
                }
                //Arguments = new object[1] { _info };
                if (RPCMsgLogManager.IsRecord)
                {
                    RPCMsgLogManager.Receive(MSGIDType.MSGID_CLIENT_AOI_NEW_ENTITY, _info.ToString());
                }
            }
            else {
                Arguments = null;
                if (entity != null)
                {
                    LoggerHelper.Warning(string.Format("EntityType {0} is not supported in client!!!", entity.Name));
                }
            }
        }

        public override void HandleData()
        {
            //UnityEngine.Debug.LogError("AOINewEntity  info: " + _info.ToString());
            EventDispatcher.TriggerEvent<CellAttachedInfo>(RPCEvents.AOINewEntity, _info);
            //PlutoDataObjectPool.instance.ReleaseCellAttachedInfo(_info);
        }

        static AOINewEntityPluto pluto = new AOINewEntityPluto();
        internal static Pluto Create()
        {
            return pluto;
        }
	}
}
