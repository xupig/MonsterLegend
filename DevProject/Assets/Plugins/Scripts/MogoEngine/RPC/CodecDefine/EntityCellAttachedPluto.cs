﻿using System;
using System.Collections.Generic;

using GameLoader.Utils;

using MogoEngine.Events;

namespace MogoEngine.RPC
{
    class EntityCellAttachedPluto : Pluto
    {
        CellAttachedInfo info;
        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {
            info = PlutoDataObjectPool.instance.GetCellAttachedInfo();
            info.id = VUInt32.Instance.Decode(data, ref unLen, true); // eid
            info.eulerAnglesX = (byte)VUInt8.Instance.Decode(data, ref unLen);
            info.eulerAnglesY = (byte)VUInt8.Instance.Decode(data, ref unLen);
            info.eulerAnglesZ = (byte)VUInt8.Instance.Decode(data, ref unLen);
            info.x = VInt32.Instance.Decode(data, ref unLen, true);
            info.y = VInt32.Instance.Decode(data, ref unLen, true);
            info.z = VInt32.Instance.Decode(data, ref unLen, true);
            var entity = CurrentEntity;
            if (entity != null)
            {
                info.entity = entity;
                while (unLen < unEnd)
                {//还有数据就解析
                    var index = VUInt16.Instance.Decode(data, ref unLen, true);
                    EntityDefProperties prop;
                    var flag = entity.Properties.TryGetValue((ushort)index, out prop);
                    if (flag)
                    {
                        info.props.Add(PlutoDataObjectPool.instance.GetEntityPropertyValue(prop, prop.VType.Decode(data, ref unLen)));
                    }
                }
            }
            //Arguments = new object[1] { info };
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.MSGID_CLIENT_ENTITY_CELL_ATTACHED, info);
            }
        }

        public override void HandleData()
        {
            //var info = Arguments[0] as CellAttachedInfo;
            EventDispatcher.TriggerEvent<CellAttachedInfo>(RPCEvents.EntityCellAttached, info);
            PlutoDataObjectPool.instance.ReleaseCellAttachedInfo(info);
        }

        /// <summary>
        /// 创建新EntityCellAttachedPluto实例。
        /// </summary>
        /// <returns>EntityCellAttachedPluto实例</returns>
        static EntityCellAttachedPluto pluto = new EntityCellAttachedPluto();
        internal static Pluto Create()
        {
            return pluto;
        }
    }
}
