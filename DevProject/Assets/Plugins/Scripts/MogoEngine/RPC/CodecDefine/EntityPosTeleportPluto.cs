﻿using System;
using System.Collections.Generic;


using MogoEngine.Events;

namespace MogoEngine.RPC
{
    class EntityPosTeleportPluto : Pluto
    {
        CellAttachedInfo info;
        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {
            info = PlutoDataObjectPool.instance.GetCellAttachedInfo();
            //UInt16 x = VUInt16.Instance.Decode(data, ref unLen, true);
            //UInt16 y = VUInt16.Instance.Decode(data, ref unLen, true);
            //info.x = (short)x;
            //info.y = (short)y;
            //System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //foreach (var item in data)
            //{
            //    sb.Append(item);
            //}
            info.eulerAnglesX = (byte)VUInt8.Instance.Decode(data, ref unLen);
            info.eulerAnglesY = (byte)VUInt8.Instance.Decode(data, ref unLen);
            info.eulerAnglesZ = (byte)VUInt8.Instance.Decode(data, ref unLen);

            Int32 x = VInt32.Instance.Decode(data, ref unLen, true);
            Int32 y = VInt32.Instance.Decode(data, ref unLen, true);
            Int32 z = VInt32.Instance.Decode(data, ref unLen, true);
            info.x = x;
            info.y = y;
            info.z = z;
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.MSGID_CLIENT_ENTITY_POS_TELEPORT, info);
            }
        }

        public override void HandleData()
        {//用于同场景跳点传送
            EventDispatcher.TriggerEvent<CellAttachedInfo>(RPCEvents.EntityPosTeleport, info);
            PlutoDataObjectPool.instance.ReleaseCellAttachedInfo(info);
        }

        static EntityPosTeleportPluto pluto = new EntityPosTeleportPluto();
        internal static Pluto Create()
        {
            return pluto;
        }
    }
}
