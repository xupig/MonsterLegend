﻿using System;
using System.Collections.Generic;

using GameLoader.Utils;

using MogoEngine.Events;

namespace MogoEngine.RPC
{
    class OtherEntityPosTeleportPluto : Pluto
    {
        CellAttachedInfo info;
        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {
            info = PlutoDataObjectPool.instance.GetCellAttachedInfo();
            info.id = VUInt32.Instance.Decode(data, ref unLen, true); // eid
            //info.face = (byte)VUInt8.Instance.Decode(data, ref unLen);// rotation
            info.eulerAnglesX = (byte)VUInt8.Instance.Decode(data, ref unLen);
            info.eulerAnglesY = (byte)VUInt8.Instance.Decode(data, ref unLen);
            info.eulerAnglesZ = (byte)VUInt8.Instance.Decode(data, ref unLen);

            Int32 x = VInt32.Instance.Decode(data, ref unLen, true);
            Int32 y = VInt32.Instance.Decode(data, ref unLen, true);
            Int32 z = VInt32.Instance.Decode(data, ref unLen, true);
            info.x = x;
            info.y = y;
            info.z = z;
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.CLIENT_OTHER_ENTITY_TELEPORT, info);
            }
        }

        public override void HandleData()
        {
            EventDispatcher.TriggerEvent<CellAttachedInfo>(RPCEvents.OtherEntityPosTeleport, info);
            PlutoDataObjectPool.instance.ReleaseCellAttachedInfo(info);
        }

        static OtherEntityPosTeleportPluto pluto = new OtherEntityPosTeleportPluto();
        internal static Pluto Create()
        {
            return pluto;
        }
    }
}
