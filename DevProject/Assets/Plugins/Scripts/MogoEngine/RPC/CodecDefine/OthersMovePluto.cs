﻿using System;

namespace MogoEngine.RPC
{
    public class OthersMovePluto : Pluto
    {
        Byte[] _encodeBuff = null;
        /// <summary>
        /// 将远程方法调用编码为二进制数组。
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <param name="x3"></param>
        /// <param name="y3"></param>
        /// <returns>编码后的二进制数组</returns>
        public Byte[] Encode(uint entityID, byte eulerAnglesX, byte eulerAnglesY, byte eulerAnglesZ, int x, int y, int z)
        {
            Push(VUInt16.Instance.Encode((UInt16)MSGIDType.MSGID_BASEAPP_CLIENT_OTHERS_MOVE_REQ));
            Push(VUInt32.Instance.Encode(entityID));
            Push(VUInt8.Instance.Encode(eulerAnglesX));
            Push(VUInt8.Instance.Encode(eulerAnglesY));
            Push(VUInt8.Instance.Encode(eulerAnglesZ));
            Push(VInt32.Instance.Encode(x));
            Push(VInt32.Instance.Encode(y));
            Push(VInt32.Instance.Encode(z));


            if (_encodeBuff == null)
            {
                _encodeBuff = new Byte[m_unLen];
            }
            Buffer.BlockCopy(m_szBuff, 0, _encodeBuff, 0, m_unLen);
            EndPluto(_encodeBuff);
			if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Send(MSGIDType.MSGID_BASEAPP_CLIENT_OTHERS_MOVE_REQ, x, y);
            }
            return _encodeBuff;
        }

        /// <summary>
        /// 将远程调用的方法解码为MovePluto调用。
        /// </summary>
        /// <param name="data">远程调用方法的二进制数组</param>
        /// <param name="unLen">数据偏移量</param>
        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {

        }

        public override void HandleData()
        {
        }

        /// <summary>
        /// 创建新OthersMovePluto实例。
        /// </summary>
        /// <returns>OthersMovePluto实例</returns>
        static OthersMovePluto pluto = new OthersMovePluto();
        internal static Pluto Create()
        {
            return pluto;
        }
    }
}