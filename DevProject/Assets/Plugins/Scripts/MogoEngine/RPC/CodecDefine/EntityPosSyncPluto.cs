﻿using System;
using System.Collections.Generic;


using MogoEngine.Events;
namespace MogoEngine.RPC
{
    class EntityPosSyncPluto : Pluto
    {
        CellAttachedInfo info;
        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {
            info = PlutoDataObjectPool.instance.GetCellAttachedInfo();
            info.eulerAnglesX = (byte)VUInt8.Instance.Decode(data, ref unLen);
            info.eulerAnglesY = (byte)VUInt8.Instance.Decode(data, ref unLen);
            info.eulerAnglesZ = (byte)VUInt8.Instance.Decode(data, ref unLen);

            Int32 x = VInt32.Instance.Decode(data, ref unLen, true);
            Int32 y = VInt32.Instance.Decode(data, ref unLen, true);
            Int32 z = VInt32.Instance.Decode(data, ref unLen, true);
            info.x = x;
            info.y = y;
            info.z = z;

            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.MSGID_CLIENT_ENTITY_POS_SYNC, info);
            }
        }

        public override void HandleData()
        {
            EventDispatcher.TriggerEvent<CellAttachedInfo>(RPCEvents.EntityPosSync, info);
            PlutoDataObjectPool.instance.ReleaseCellAttachedInfo(info);
        }

        static EntityPosSyncPluto pluto = new EntityPosSyncPluto();
        internal static Pluto Create()
        {
            return pluto;
        }
    }
}
