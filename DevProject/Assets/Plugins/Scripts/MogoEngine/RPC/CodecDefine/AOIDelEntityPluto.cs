﻿using System;
using System.Collections.Generic;


using MogoEngine.Events;

namespace MogoEngine.RPC
{
	class AOIDelEntityPluto:Pluto
	{
        UInt32 entityID;
        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {
            entityID = (UInt32)VUInt32.Instance.Decode(data, ref unLen);
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Receive(MSGIDType.MSGID_CLIENT_AOI_DEL_ENTITY, entityID);
            }
        }

        public override void HandleData()
        {
            EventDispatcher.TriggerEvent<uint>(RPCEvents.AOIDelEntity, entityID);
            entityID = 0;
        }


        static AOIDelEntityPluto pluto = new AOIDelEntityPluto();
        internal static Pluto Create()
        {
            return pluto;
        }
	}
}
