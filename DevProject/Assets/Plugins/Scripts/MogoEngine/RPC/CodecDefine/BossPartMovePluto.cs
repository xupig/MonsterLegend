﻿using System;
using UnityEngine;

namespace MogoEngine.RPC
{
    public class BossPartMovePluto : Pluto
    {
        Byte[] _encodeBuff = null;
        public Byte[] Encode(uint entityID, byte partID, int x, int y, int z)
        {
            Push(VUInt16.Instance.Encode((UInt16)MSGIDType.MSGID_BASEAPP_CLIENT_MOVE_PART_REQ));
            Push(VUInt32.Instance.Encode(entityID));
            Push(VUInt8.Instance.Encode(partID));
            Push(VInt32.Instance.Encode(x));
            Push(VInt32.Instance.Encode(y));
            Push(VInt32.Instance.Encode(z));

            if (_encodeBuff == null) _encodeBuff = new Byte[m_unLen];
            Buffer.BlockCopy(m_szBuff, 0, _encodeBuff, 0, m_unLen);
            EndPluto(_encodeBuff);
            if (RPCMsgLogManager.IsRecord)
            {
                RPCMsgLogManager.Send(MSGIDType.MSGID_BASEAPP_CLIENT_MOVE_PART_REQ, entityID, partID, x, y, z);
            }
            return _encodeBuff;
        }

        /// <summary>
        /// 将远程调用的方法解码为BossPartMovePluto调用。
        /// </summary>
        /// <param name="data">远程调用方法的二进制数组</param>
        /// <param name="unLen">数据偏移量</param>
        protected override void DoDecode(byte[] data, ref int unLen, int unEnd)
        {

        }

        public override void HandleData()
        {
        }

        /// <summary>
        /// 创建新BossPartMovePluto实例。
        /// </summary>
        /// <returns>BossPartMovePluto实例</returns>
        static BossPartMovePluto pluto = new BossPartMovePluto();
        internal static Pluto Create()
        {
            return pluto;
        }
    }
}
