﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：Pluto
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.1.16
// 模块描述：数据包编解码处理类。
//==========================================*/
#endregion
using System;
using System.Collections.Generic;

using GameLoader.Utils;

using MogoEngine.Events;

namespace MogoEngine.RPC
{
    /// <summary>
    /// 数据包编解码处理类。
    /// </summary>
    public abstract class Pluto
    {
        protected static BitCryto encryto = new BitCryto(new short[] { 124, 10, 20, 230, 189, 35, 2, 190, 200, 205, 46, 222, 78, 184, 231, 15, 25, 166, 138 });
        //protected static BitCryto encryto = new BitCryto(new short[] { 0, 0 });

        private static EntityDef m_currentEntity;

        protected Byte[] m_szBuff;
        protected Int32 m_unLen;
        private Int32 m_unMaxLen;
        private String m_funcName;
        private ushort m_funcID;
        private ushort m_entityID;
		private String m_entityName;
		private Object[] m_arguments;

        /// <summary>
        /// 方法标识
        /// </summary>
        public ushort EntityID
        {
            get { return m_entityID; }
            set { m_entityID = value; }
        }

		/// <summary>
		/// 实体名称
		/// </summary>
		public String EntityName
		{
			get { return m_entityName; }
			set { m_entityName = value; }
		}

        /// <summary>
        /// 方法标识
        /// </summary>
        public ushort FuncID
        {
            get { return m_funcID; }
            set { m_funcID = value; }
        }

		/// <summary>
		/// 方法名称
		/// </summary>
		public String FuncName
		{
			get { return m_funcName; }
			set { m_funcName = value; }
		}

		/// <summary>
		/// 参数列表
		/// </summary>
		public Object[] Arguments
		{
			get { return m_arguments; }
			protected set { m_arguments = value; }
		}

        /// <summary>
        /// 当前实体对象
        /// </summary>
        public static EntityDef CurrentEntity
        {
            get { return m_currentEntity; }
            set { m_currentEntity = value; }
        }

        public static Func<uint, EntityDef> GetEntity;

        /// <summary>
        /// 默认构造函数
        /// </summary>
        protected Pluto()
        {
           
        }

        ///// <summary>
        ///// 将远程方法调用编码为二进制数组。
        ///// </summary>
        ///// <param name="args">参数列表</param>
        ///// <returns>编码后的二进制数组</returns>
        //public abstract byte[] Encode(params Object[] args);

        /// <summary>
        /// 处理解码数据。
        /// </summary>
        public abstract void HandleData();

        /// <summary>
        /// 将远程调用的方法解码为具体的方法调用。
        /// </summary>
        /// <param name="data">远程调用方法的二进制数组</param>
        /// <param name="unLen">数据偏移量</param>
        protected abstract void DoDecode(Byte[] data, ref int unLen, int unEnd);

        ///// <summary>
        ///// 将远程调用的方法解码为具体的方法调用。
        ///// </summary>
        ///// <param name="data">远程调用方法的二进制数组</param>
        public static Pluto Decode(Byte[] data, int idx, int len)
        {
            Int32 unLen = idx;
            int unEnd = idx + len;
            Pluto pluto;
            var msgId = (MSGIDType)VUInt16.Instance.Decode(data, ref unLen, true);
            encryto.Reset();
            //int len = data.Length;
            for (int i = unLen; i < unEnd; ++i)
            {
                data[i] = encryto.Decode(data[i]);
            }
            //UnityEngine.Debug.LogError("Pluto msgId: " + msgId);
        
            switch (msgId)
            {
                case MSGIDType.MSGID_CLIENT_RPC_RESP:
                    pluto = RpcCallPluto.Create();
                    break;
                case MSGIDType.MSGID_CLIENT_LOGIN_RESP:
                    pluto = LoginPluto.Create();
                    break;
                case MSGIDType.MSGID_CLIENT_NOTIFY_ATTACH_BASEAPP:
                    pluto = BaseLoginPluto.Create();
                    break;
                case MSGIDType.MSGID_CLIENT_ENTITY_ATTACHED:
                    pluto = EntityAttachedPluto.Create();
                    break;
                case MSGIDType.MSGID_CLIENT_AOI_NEW_ENTITY:
                    pluto = AOINewEntityPluto.Create();
                    break;
                case MSGIDType.MSGID_CLIENT_AOI_DEL_ENTITY:
                    pluto = AOIDelEntityPluto.Create();
                    break;
                case MSGIDType.MSGID_CLIENT_OTHER_ENTITY_POS_SYNC:
                    pluto = OtherEntityPosSyncPluto.Create();
                    break;
                case MSGIDType.CLIENT_OTHER_ENTITY_POS_PULL:
                    pluto = OtherEntityPosPullPluto.Create();
                    break;
                case MSGIDType.CLIENT_OTHER_ENTITY_TELEPORT:
                    pluto = OtherEntityPosTeleportPluto.Create();
                    break;
                case MSGIDType.MSGID_CLIENT_ENTITY_CELL_ATTACHED:
                    pluto = EntityCellAttachedPluto.Create();
                    break;
                case MSGIDType.MSGID_CLIENT_AOI_ENTITIES:
                    pluto = AOIEntitiesPluto.Create();
                    break;
                case MSGIDType.MSGID_CLIENT_AVATAR_ATTRI_SYNC:
                    pluto = AvatarAttriSyncPluto.Create();
                    break;
                case MSGIDType.MSGID_CLIENT_OTHER_ENTITY_ATTRI_SYNC:
                    pluto = OtherAttriSyncPluto.Create();
                    break;
                case MSGIDType.MSGID_CLIENT_ENTITY_POS_SYNC:
                    pluto = EntityPosSyncPluto.Create();
                    break;
                case MSGIDType.CLIENT_ENTITY_POS_PULL:
                    pluto = EntityPosPullPluto.Create();
                    break;
                case MSGIDType.MSGID_CLIENT_ENTITY_POS_TELEPORT:
                    pluto = EntityPosTeleportPluto.Create();
                    break;
                case MSGIDType.CLIENT_CHECK_RESP:
                    pluto = CheckDefMD5Pluto.Create();
                    break;
                case MSGIDType.MSGID_CLIENT_RELOGIN_RESP:
                    pluto = ReConnectPluto.Create();
                    break;
                case MSGIDType.MSGID_CLIENT_NOTIFY_MULTILOGIN:
                    pluto = DefuseLoginPluto.Create();
                    break;
                default:
                    pluto = NotImplementedPluto.Create();
                    break;
            }

            

            pluto.DoDecode(data, ref unLen, unEnd);

            return pluto;
        }

        protected void Push(Byte[] data)
        {
            //从池中获取实例
            if(m_szBuff == null)
            {
                m_szBuff = BufferPool.Get();
                m_unMaxLen = m_szBuff.Length;
            }
            //当buffer长度不够的时候，扩展buffer长度
            if (m_unLen + data.Length > m_unMaxLen)
            {
                m_unMaxLen = ((m_unLen + data.Length) / BufferPool.UNIT_BUFFER_LENGTH + 1) * BufferPool.UNIT_BUFFER_LENGTH;
                Byte[] newArray = new Byte[m_unMaxLen];
                Buffer.BlockCopy(m_szBuff, 0, newArray, 0, m_unLen);
                m_szBuff = newArray;
            }
            //将数据写入buffer
            Buffer.BlockCopy(data, 0, m_szBuff, m_unLen, data.Length);
            m_unLen += data.Length;
        }

        protected void EndPluto(byte[] bytes)
        {
            encryto.Reset();
            int len = bytes.Length;
            for (int i = 2; i < len; i++)
            {
                bytes[i] = encryto.Encode(bytes[i]);
            }
            BufferPool.Recycle(m_szBuff);
            m_szBuff = null;
            m_unLen = 0;
        }
    }

    internal class BufferPool
    {
        public const int UNIT_BUFFER_LENGTH = 128;
        private static Stack<Byte[]> _pool = new Stack<byte[]>();

        public static Byte[] Get()
        {
            if(_pool.Count == 0)
            {
                return new Byte[UNIT_BUFFER_LENGTH];
            }
            return _pool.Pop();
        }

        public static void Recycle(Byte[] buffer)
        {
            _pool.Push(buffer);
        }
    }
}