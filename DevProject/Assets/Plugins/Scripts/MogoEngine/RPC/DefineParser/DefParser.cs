﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;


using GameLoader.Utils;
using GameLoader.IO;
using GameLoader.Utils.XML;
using GameLoader;
using UnityEngine;

namespace MogoEngine.RPC
{
    /// <summary>
    /// 类接口XML解析器。
    /// </summary>
    public class DefParser// : DataLoader
    {
        #region 私有属性

        private const String EL_FLAGS = "Flags";
        private const String EL_TYPE = "Type";
        private const String EL_CLIENTLUA = "ClientLua";
        private const String EL_PARENT = "Parent";
        private const String EL_EXPOSED = "Exposed";
        private const String ATTR_PROPERTIES = "Properties";
        private const String ATTR_CLIENT_METHODS = "ClientMethods";
        private const String ATTR_BASE_METHODS = "BaseMethods";
        private const String ATTR_CELL_METHODS = "CellMethods";
        private const String VALUE_CLIENT = "CLIENT";
        private const string DATA_ENTITY_DEFS = "data/entity_defs/";

        private static DefParser m_instance;

        private Dictionary<String, EntityDef> m_entitysByName;
        private Dictionary<uint, EntityDef> m_entitysByID;

        public string m_defContent = "";
        public byte[] m_defContentMD5 { get; private set; }

        #endregion

        #region 公有属性

        public static DefParser Instance
        {
            get { return m_instance; }
        }

        public Dictionary<String, EntityDef> EntitysByName
        {
            get { return m_entitysByName; }
        }

        #endregion

        #region 构造函数

        static DefParser()
        {
            m_instance = new DefParser();
        }

        private DefParser()
        {
            m_entitysByName = new Dictionary<string, EntityDef>();
            m_entitysByID = new Dictionary<uint, EntityDef>();
        }

        #endregion

        #region 公有方法

        //public void InitEntityData()
        //{
        //    UnityEngine.Debug.LogError("InitEntityData:" + string.Concat(GameLoader.Config.SystemConfig.OuterResourceAbsRootFolder, ConstString.CONFIG_SUB_FOLDER, "/", ConstString.ENTITY_DEFS_PATH));
        //    InitEntityData(string.Concat(GameLoader.Config.SystemConfig.OuterResourceAbsRootFolder,ConstString.CONFIG_SUB_FOLDER,"/",ConstString.ENTITY_DEFS_PATH));
        //}

        public string LoadStringFromEntityFolder(string relativeFileName)
        {
            string strContent = string.Empty;
            switch (UnityPropUtils.Platform)
            { 
                case RuntimePlatform.Android:
                case RuntimePlatform.IPhonePlayer:
                     strContent = LoadEntityDefAtReleaseMode("发布模式", relativeFileName);
                     break;
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.WindowsPlayer:
                case RuntimePlatform.OSXEditor:
                    if (SystemConfig.ReleaseMode)
                    {
                        strContent = LoadEntityDefAtReleaseMode("开发模式下--发布模式", relativeFileName);
                    }
                    else
                    {
                        string path = string.Concat(SystemConfig.RuntimeResourcePath, relativeFileName, ConstString.XML_SUFFIX);
                        //LoggerHelper.Info(string.Format("[开发模式] 加载协议文件:{0}", path));
                        strContent = FileUtils.LoadFile(path);
                    }
                    break;
            }
            return strContent;
        }

        public byte[] LoadBytesFromEntityFolder(string relativeFileName)
        {
            byte[] bytesContent = null;
            switch (UnityPropUtils.Platform)
            {
                case RuntimePlatform.Android:
                case RuntimePlatform.IPhonePlayer:
                    bytesContent = LoadEntityDefBytesAtReleaseMode("发布模式", relativeFileName);
                    break;
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.WindowsPlayer:
                case RuntimePlatform.OSXEditor:
                    if (SystemConfig.ReleaseMode)
                    {
                        bytesContent = LoadEntityDefBytesAtReleaseMode("开发模式下--发布模式", relativeFileName);
                    }
                    else
                    {
                        string path = string.Concat(SystemConfig.RuntimeResourcePath, relativeFileName, ConstString.XML_SUFFIX);
                        //LoggerHelper.Info(string.Format("[开发模式] 加载协议文件:{0}", path));
                        bytesContent = FileUtils.LoadByteFile(path);
                    }
                    break;
            }
            return bytesContent;
        }

        /// <summary>
        /// 发布模式下--协议文件
        /// </summary>
        /// <param name="logTitle"></param>
        /// <param name="relativeFileName"></param>
        /// <returns></returns>
        private string LoadEntityDefAtReleaseMode(string logTitle, string relativeFileName)
        {
            string fileName = string.Concat(DATA_ENTITY_DEFS, Path.GetFileName(relativeFileName), ConstString.XML_SUFFIX);
            bool isExistFile = FileAccessManager.IsFileExist(fileName);
            //Debug.LogError("LoadEntityDefAtReleaseMode:" + fileName + " " + isExistFile + " : " + FileAccessManager.LoadText(fileName));
            //LoggerHelper.Info(string.Format("[{0}] 加载协议文件:{1},{2}", logTitle, fileName, isExistFile ? "存在" : "找不到"));
            return isExistFile ? FileAccessManager.LoadText(fileName) : null;
            //strContent = GameLoader.IO.ResLoader.LoadStringFromResource(relativeFileName);
        }

        /// <summary>
        /// 发布模式下--协议文件
        /// </summary>
        /// <param name="logTitle"></param>
        /// <param name="relativeFileName"></param>
        /// <returns></returns>
        private byte[] LoadEntityDefBytesAtReleaseMode(string logTitle, string relativeFileName)
        {
            string fileName = string.Concat(DATA_ENTITY_DEFS, Path.GetFileName(relativeFileName), ConstString.XML_SUFFIX);
            bool isExistFile = FileAccessManager.IsFileExist(fileName);
            return isExistFile ? FileAccessManager.LoadBytes(fileName) : null;
        }

        /*
        /// <summary>
        /// 初始化实体数据。
        /// </summary>
        /// <param name="xmlPath">实体声明文件路径</param>
        public void InitEntityData(int i)
        {
            //System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            //sw.Start();
            
            string strContent;
			//InitEntityData(xmlPath, );
            if (GameLoader.Config.SystemConfig.ReleaseMode)
            {
                string path = string.Concat(ConstString.CONFIG_SUB_FOLDER, "/", ConstString.ENTITY_DEFS_PATH, 
                    ConstString.DEFINE_LIST_FILE_NAME);
                strContent = GameLoader.IO.ResLoader.LoadStringFromResource(path);
            }
            else
            {
                var xmlPath = string.Concat(GameLoader.Config.SystemConfig.OuterResourceAbsRootFolder, ConstString.CONFIG_SUB_FOLDER, "/", ConstString.ENTITY_DEFS_PATH);
                strContent = string.Concat(xmlPath, String.Concat(ConstString.DEFINE_LIST_FILE_NAME, ConstString.XML_SUFFIX)).LoadFile();
            }
            SecurityElement xmlDoc = strContent.ToXMLSecurityElement();
            //sw.Stop();
            if (xmlDoc == null)
            {
                LoggerHelper.Error("Entity file load failed: " );
            }
            //InitEntityData(xmlDoc);
            EventDispatcher.TriggerEvent(RPCEvents.GetContentMD5);
        }*/

        /// <summary>
        /// 初始化实体数据。
        /// </summary>
        /// <param name="defineFilePath">实体声明文件路径</param>
        /// <param name="defineListFileName">实体声明文件名称</param>
        public void InitEntityData()
        {
            try
            {
                //SecurityElement xmlDoc;
                //xmlDoc = string.Concat(defineFilePath, defineListFileName).LoadFileExtensionForUnity().ToXMLSecurityElement();
                string entitiesListPath = string.Concat(ConstString.CONFIG_SUB_FOLDER, ConstString.ENTITY_DEFS_PATH, ConstString.DEFINE_LIST_FILE_NAME);
                SecurityElement xmlDoc = LoadStringFromEntityFolder(entitiesListPath).ToXMLSecurityElement();
                ArrayList xmlRoot = xmlDoc.Children;
                if (xmlRoot != null && xmlRoot.Count != 0)
                {
                    m_entitysByName.Clear();
                    int len = xmlRoot.Count;
                    List<string> entityNameList = new List<string>();
                    Dictionary<string, bool> entityTags = new Dictionary<string, bool>();
                    Dictionary<string, ushort> entityIndex = new Dictionary<string, ushort>();
                    for (int i = 0; i < len; i++)
                    {
                        SecurityElement element = xmlRoot[i] as SecurityElement;
                        string entityName = element.Tag;
                        //entityNameList.Add(entityName);
                        entityTags[entityName] = !String.IsNullOrEmpty(element.Text);
                        entityIndex[entityName] = (ushort)(i + 1);
                        var childrens = element.Children;
                        if (childrens != null)
                        {
                            for (int t = 0; t < childrens.Count; t++)
                            {
                                if ((childrens[t] as SecurityElement).Tag == "md5")
                                {
                                    entityNameList.Add(entityName);
                                    break;
                                }
                            }
                        } 
                    }
                    //entityNameList.Sort();
                    byte[] allBytes = new byte[0];
                    for (int i = 0; i < entityNameList.Count; i++)
                    {
                        string entityName = entityNameList[i];
                        string path = string.Concat(ConstString.CONFIG_SUB_FOLDER, ConstString.ENTITY_DEFS_PATH, entityName);
                        ParseEntitiesXmlFile(path, entityName, entityIndex[entityName], entityTags[entityName]);
                        allBytes = this.UnionBytes(allBytes, LoadBytesFromEntityFolder(path));
                    }
                    m_defContentMD5 = MD5Utils.CreateMD5(allBytes);

                    //m_defContentMD5 = MD5Utils.CreateMD5(Encoding.UTF8.GetBytes(m_defContent));
                    //m_defContent = "";
                    //找父亲
                    foreach (var childPair in m_entitysByName)
                    {
                        foreach (var parentPair in m_entitysByName)
                        {
                            if (childPair.Value.ParentName == parentPair.Value.Name)
                            {
                                childPair.Value.Parent = parentPair.Value;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.StackTrace);
                throw new DefineParseException(String.Format("Define parse error.\nreason: \n{0} \n{1}", ex.Message, ex.StackTrace), ex);
            }
            //EventDispatcher.TriggerEvent(RPCEvents.GetContentMD5);
        }

        /// <summary>
        /// 根据实体名称查找实体对象。
        /// </summary>
        /// <param name="name">实体名称</param>
        /// <returns>实体对象</returns>
        public EntityDef GetEntityByName(String name)
        {
            EntityDef result;
            m_entitysByName.TryGetValue(name, out result);

            return result;
        }

        /// <summary>
        /// 根据实体标识查找实体对象。
        /// </summary>
        /// <param name="id">实体标识</param>
        /// <returns>实体对象</returns>
        public EntityDef GetEntityByID(ushort id)
        {
            EntityDef result;
            m_entitysByID.TryGetValue(id, out result);

            return result;
        }

        #endregion

        #region 私有方法

        private void ParseEntitiesXmlFile(String fileName, String etyName, ushort entityID, bool needMD5 = false)
        {
            string strContent = LoadStringFromEntityFolder(fileName);
            //Debug.LogError("ParseEntitiesXmlFile:" + fileName + ":" + strContent + "");
            if (needMD5)
            {
				//VersionManager.SetMD5Content(strContent);
                m_defContent = m_defContent + strContent;
            }
            SecurityElement xmlDoc = XMLParser.LoadXML(strContent);
            if (xmlDoc != null)
            {
                ParseEntitiesXml(xmlDoc, etyName, entityID, needMD5);
            }
            else 
            {
                LoggerHelper.Error(string.Format("xml文件:{0}内容转化XML出错 etyName:{1},xmlDoc:{2}", fileName, etyName, xmlDoc));
            }
        }

        private byte[] UnionBytes(byte[] a, byte[] b)
        {
            byte[] resArr = new byte[a.Length + b.Length];
            a.CopyTo(resArr, 0);
            b.CopyTo(resArr, a.Length);
            return resArr;
        }

        private void ParseEntitiesXml(SecurityElement xmlDoc, String etyName, ushort entityID, bool hasCellClient)
        {
            ArrayList xmlRoot = xmlDoc.Children;
            if (xmlRoot != null && xmlRoot.Count != 0)
            {
                EntityDef etyDef = ParseDef(xmlRoot);
                etyDef.ID = entityID;
                etyDef.Name = etyName;
                etyDef.BHasCellClient = true; //hasCellClient;
                if (!m_entitysByName.ContainsKey(etyDef.Name))
                    m_entitysByName.Add(etyDef.Name, etyDef);
                if (!m_entitysByID.ContainsKey(etyDef.ID))
                    m_entitysByID.Add(etyDef.ID, etyDef);
                AddExternalClientMethod(etyDef);
            }
        }

        private EntityDef ParseDef(ArrayList xmlRoot)
        {
            EntityDef etyDef = new EntityDef();
            foreach (SecurityElement element in xmlRoot)
            {
                if (element.Tag == EL_PARENT)
                {
                    etyDef.ParentName = element.Text.Trim();//To do: Trim()优化
                }
                else if (element.Tag == ATTR_CLIENT_METHODS)
                {
                    GetClientMethods(etyDef, element.Children);
                }
                else if (element.Tag == ATTR_BASE_METHODS)
                {
                    GetBaseMethods(etyDef, element.Children);
                }
                else if (element.Tag == ATTR_CELL_METHODS)
                {
                    GetCellMethods(etyDef, element.Children);
                }
                else if (element.Tag == ATTR_PROPERTIES)
                {
                    etyDef.Properties = GetProperties(element.Children);
                    etyDef.PropertiesList = etyDef.Properties.Values.ToList();
                }
            }
            return etyDef;
        }

        private Dictionary<ushort, EntityDefProperties> GetProperties(ArrayList properties)
        {
            Dictionary<ushort, EntityDefProperties> propList = new Dictionary<ushort, EntityDefProperties>();
            if (properties != null)
            {
                short index = -1;//属性索引
                foreach (SecurityElement element in properties)//遍历实体的所有属性
                {
                    SecurityElement flag = null;
                    SecurityElement node = null;
                    SecurityElement clientLua = null;
                    foreach (SecurityElement item in element.Children)//遍历属性的字段
                    {
                        if (item.Tag == EL_TYPE)//查找类型字段
                        {
                            node = item;
                        }
                        else if (item.Tag == EL_FLAGS)//查找客户端标记字段
                        {
                            flag = item;
                        }
                        else if (item.Tag == EL_CLIENTLUA)
                        {
                            clientLua = item;
                        }
                    }
                    index++;
                    if (flag != null && !CheckFlags(flag.Text.Trim()))//判断是否为客户端使用属性
                        continue;
                    EntityDefProperties p = new EntityDefProperties();
                    p.Name = element.Tag;
                    if (node != null)
                        p.VType = TypeMapping.GetVObject(node.Text.Trim());//To do: Trim()优化

                    if (!propList.ContainsKey((ushort)index))
                        propList.Add((ushort)index, p);
                }
            }
            return propList;
        }

        private void GetClientMethods(EntityDef entity, ArrayList methods)
        {
            Dictionary<ushort, EntityDefMethod> methodByIDList = new Dictionary<ushort, EntityDefMethod>();
            Dictionary<string, EntityDefMethod> methodByNameList = new Dictionary<string, EntityDefMethod>();
            if (methods != null)
            {
                ushort index = 0;//方法索引
                foreach (SecurityElement element in methods)
                {
                    //if (element.NodeType == XmlNodeType.Comment)
                    //    continue;
                    EntityDefMethod m = new EntityDefMethod();
                    m.FuncName = element.Tag;
                    m.FuncID = index;
                    m.ArgsType = new List<VObject>();
                    if (element.Children != null)
                    {
                        foreach (SecurityElement args in element.Children)
                        {
                            //    if (args != null && args.NodeType == XmlNodeType.Element)
                            m.ArgsType.Add(TypeMapping.GetVObject(args.Text.Trim()));//To do: Trim()优化
                        }

                    }
                    methodByIDList.Add(m.FuncID, m);
                    if (!methodByNameList.ContainsKey(m.FuncName))
                        methodByNameList.Add(m.FuncName, m);
                    index++;
                }
            }
            entity.ClientMethodsByID = methodByIDList;
            entity.ClientMethodsByName = methodByNameList;
        }

        private void GetBaseMethods(EntityDef entity, ArrayList methods)
        {
            Dictionary<ushort, EntityDefMethod> methodByIDList = new Dictionary<ushort, EntityDefMethod>();
            Dictionary<string, EntityDefMethod> methodByNameList = new Dictionary<string, EntityDefMethod>();
            if (methods != null)
            {
                ushort index = 0;//方法索引
                foreach (SecurityElement element in methods)
                {
                    //判断方法参数是否为空，或者第一个Tag是否标记为公开方法
                    if (element.Children != null && element.Children.Count != 0 && (element.Children[0] as SecurityElement).Tag == EL_EXPOSED)
                    {
                        EntityDefMethod m = new EntityDefMethod();
                        m.FuncName = element.Tag;
                        m.FuncID = index;
                        m.ArgsType = new List<VObject>();
                        for (int i = 1; i < element.Children.Count; i++)//跳过第一个Tag字段
                        {
                            SecurityElement args = element.Children[i] as SecurityElement;
                            m.ArgsType.Add(TypeMapping.GetVObject(args.Text.Trim()));//To do: Trim()优化
                        }
                        methodByIDList.Add(m.FuncID, m);
                        if (!methodByNameList.ContainsKey(m.FuncName))
                            methodByNameList.Add(m.FuncName, m);
                    }
                    index++;
                }
            }
            entity.BaseMethodsByID = methodByIDList;
            entity.BaseMethodsByName = methodByNameList;
        }

        private void GetCellMethods(EntityDef entity, ArrayList methods)
        {
            Dictionary<ushort, EntityDefMethod> methodByIDList = new Dictionary<ushort, EntityDefMethod>();
            Dictionary<string, EntityDefMethod> methodByNameList = new Dictionary<string, EntityDefMethod>();
            if (methods != null)
            {
                ushort index = 0;//方法索引
                foreach (SecurityElement element in methods)
                {
                    //判断方法参数是否为空，或者第一个Tag是否标记为公开方法
                    if (element.Children != null && element.Children.Count != 0 && (element.Children[0] as SecurityElement).Tag == EL_EXPOSED)
                    {
                        EntityDefMethod m = new EntityDefMethod();
                        m.FuncName = element.Tag;
                        m.FuncID = index;
                        m.ArgsType = new List<VObject>();
                        for (int i = 1; i < element.Children.Count; i++)//跳过第一个Tag字段
                        {
                            SecurityElement args = element.Children[i] as SecurityElement;
                            m.ArgsType.Add(TypeMapping.GetVObject(args.Text.Trim()));//To do: Trim()优化
                        }
                        methodByIDList.Add(m.FuncID, m);
                        if (!methodByNameList.ContainsKey(m.FuncName))
                            methodByNameList.Add(m.FuncName, m);
                    }
                    index++;
                }
            }
            entity.CellMethodsByID = methodByIDList;
            entity.CellMethodsByName = methodByNameList;
        }

        private void AddExternalClientMethod(EntityDef entity)
        {
            if (entity.Name == "Avatar")
            {
                List<VObject> argsType = new List<VObject>();
                argsType.Add(TypeMapping.GetVObject("PB_BLOB"));
                foreach(var pair in ExternalDef.MethodsIDMap)
                {    
                    AddClientMethodToDef(entity, pair.Key, pair.Value, argsType);
                }
            }
        }

        public void AddClientMethodToDef(EntityDef entity, ushort id, string name, List<VObject> argsType)
        {
            EntityDefMethod m = new EntityDefMethod();
            m.FuncName = name;
            m.FuncID = id;
            m.ArgsType = argsType;
            entity.ClientMethodsByID.Add(id, m);
            entity.ClientMethodsByName.Add(name, m);
        }

        private Boolean CheckFlags(String flags)
        {
            if (!String.IsNullOrEmpty(flags) && flags.Contains(VALUE_CLIENT))
                return true;
            else
                return false;
        }

        #endregion

 


        public static String LoadText(String fileName)
        {
            try
            {
                //return SystemSwitch.ReleaseMode ? FileAccessManager.LoadText(fileName) : Utils.LoadResource(fileName);
                return ResLoader.LoadStringFromResource(fileName);
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
                return "";
            }
        }

        public static byte[] LoadBytes(String fileName)
        {
            //return SystemSwitch.ReleaseMode ? FileAccessManager.LoadBytes(fileName) : FileLoader.LoadByteResource(fileName);
            return ResLoader.LoadByteFromResource(fileName);
        }

    }
}