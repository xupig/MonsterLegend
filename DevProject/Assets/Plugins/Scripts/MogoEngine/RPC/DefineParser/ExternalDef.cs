﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：EntityDef
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.1.16
// 模块描述：实体类声明。
//==========================================*/
#endregion
using System.Collections.Generic;

namespace MogoEngine.RPC
{
    public class ExternalDef
    {
        public const ushort FUNCID_CLIENT_CS_SELF_CAST_SPELL_RESP     = 60001;
        public const ushort FUNCID_CLIENT_CS_OTHER_CAST_SPELL_RESP    = 60002;
        public const ushort FUNCID_CLIENT_CS_SPELL_DAMAGE_RESP        = 60003;
        public const ushort FUNCID_CLIENT_CS_SPELL_ACTION_RESP        = 60004;
        public const ushort FUNCID_CLIENT_CS_SPELL_ACTION_ORIGIN_RESP = 60005;
        public const ushort FUNCID_CLIENT_CS_SPELL_TARGET_RESP        = 60006;
        public const ushort FUNCID_CLIENT_CS_OTHER_SPELL_ACTION_RESP  = 60007;
        public const ushort FUNCID_CLIENT_CS_SPELL_CD_CHANGE_RESP     = 60008;
        public const ushort FUNCID_CLIENT_CS_ADD_BUFF_RESP            = 60009;
        public const ushort FUNCID_CLIENT_CS_ON_BUFF_HP_CHANGE        = 60010;
        public const ushort FUNCID_CLIENT_CS_ON_MONSTER_PART_DESTROY  = 60011;

        public static Dictionary<ushort, string> MethodsIDMap = new Dictionary<ushort, string>(){
            {FUNCID_CLIENT_CS_SELF_CAST_SPELL_RESP, "SelfCastSpellResp"},
            {FUNCID_CLIENT_CS_OTHER_CAST_SPELL_RESP, "OtherCastSpellResp"},
            {FUNCID_CLIENT_CS_SPELL_DAMAGE_RESP, "SpellDamageResp"},
            {FUNCID_CLIENT_CS_SPELL_ACTION_RESP, "SpellActionResp"},
            {FUNCID_CLIENT_CS_SPELL_ACTION_ORIGIN_RESP, "SpellActionOriginResp"},
            {FUNCID_CLIENT_CS_SPELL_TARGET_RESP, "SpellTargetResp"},//
            {FUNCID_CLIENT_CS_OTHER_SPELL_ACTION_RESP, "OtherSpellActionResp"},
            {FUNCID_CLIENT_CS_SPELL_CD_CHANGE_RESP, "SpellCdChangeResp"},
            {FUNCID_CLIENT_CS_ADD_BUFF_RESP, "AddBuffResp"},
            {FUNCID_CLIENT_CS_ON_BUFF_HP_CHANGE, "BuffHpChangeResp"},
            {FUNCID_CLIENT_CS_ON_MONSTER_PART_DESTROY, "MonsterPartDestroyResp"},
        };
    }
}