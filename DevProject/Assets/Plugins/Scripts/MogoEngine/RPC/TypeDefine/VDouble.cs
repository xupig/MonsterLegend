﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：VBoolean
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.1.17
// 模块描述：双精度浮点数（Boolean）。
//==========================================*/
#endregion
using GameLoader.Utils;
using System;
using System.Runtime.InteropServices;

namespace MogoEngine.RPC
{
    /// <summary>
    /// 
    /// </summary>
    public class VDouble : VObject
    {
        private static VDouble m_instance = new VDouble();
        public static VDouble Instance
        {
            get
            {
                return m_instance;
            }
        }

        public VDouble()
            : base(typeof(Double), VType.V_FLOAT64, 8)//Marshal.SizeOf(typeof(Double)))
        {
        }
        public VDouble(Object vValue)
            : base(typeof(Double), VType.V_FLOAT64, vValue)
        {
        }

        public byte[] Encode(object vValue)
        {
            var result = BitConverterExtend.GetBytes(Convert.ToDouble(vValue));
            ////Array.Reverse(result);
            return result;
        }

        public override Object Decode(byte[] data, ref Int32 index)
        {
            return Decode(data, ref index, true);
        }

        public Double Decode(byte[] data, ref Int32 index, bool fakeFlag)
        {
            Double result = BitConverterExtend.ToDouble(data, index);
            index += VTypeLength;
            return result;
        }
   }
}
