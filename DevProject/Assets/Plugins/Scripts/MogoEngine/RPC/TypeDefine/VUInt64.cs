﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：VUInt64
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.4.23
// 模块描述：64位无符号整数（UInt64）。
//==========================================*/
#endregion
using GameLoader.Utils;
using System;
using System.Runtime.InteropServices;

namespace MogoEngine.RPC
{
    /// <summary>
    /// 64位无符号整数（UInt64）。
    /// </summary>
    public class VUInt64 : VObject
    {
        private static VUInt64 m_instance = new VUInt64();
        public static VUInt64 Instance
        {
            get
            {
                return m_instance;
            }
        }

        public VUInt64()
            : base(typeof(UInt64), VType.V_UINT64, 8)//Marshal.SizeOf(typeof(UInt64)))
        {
        }
        public VUInt64(Object vValue)
            : base(typeof(UInt64), VType.V_UINT64, vValue)
        {
        }

        public byte[] Encode(object vValue)
        {
            var result = BitConverterExtend.GetBytes(Convert.ToUInt64(vValue));
            return result;
        }

        public override Object Decode(byte[] data, ref Int32 index)
        {
            return Decode(data, ref index, true);
        }

        public UInt64 Decode(byte[] data, ref Int32 index, bool fakeFlag)
        {
            UInt64 result = BitConverterExtend.ToUInt64(data, index);
            index += VTypeLength;
            return result;
        }
    }
}