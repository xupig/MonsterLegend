﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：VString
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.1.16
// 模块描述：字符串（String）。
//==========================================*/
#endregion
using System;
using System.Runtime.InteropServices;
using System.Text;
using GameLoader.Utils;


namespace MogoEngine.RPC
{
    /// <summary>
    /// 字符串（String）。
    /// </summary>
    public class VString : VObject
    {
        private static Encoding m_encoding = Encoding.UTF8;
        private static VString m_instance= new VString();
        public static VString Instance
        {
            get
            {
                return m_instance;
            }
        }

        public VString()
            : base(typeof(String), VType.V_STR, 0)
        {
        }
        public VString(Object vValue)
            : base(typeof(String), VType.V_STR, vValue)
        {
        }

        public byte[] Encode(String vValue)
        {
            //String value = (String)vValue;
            //byte[] encodeValues = m_encoding.GetBytes(value);
            //Array.Reverse(encodeValues);
            Encoder ec = m_encoding.GetEncoder();//获取字符编码
            Char[] charArray = vValue.ToCharArray();
            Int32 length = ec.GetByteCount(charArray, 0, charArray.Length, false);//获取字符串转换为二进制数组后的长度，用于申请存放空间
            Byte[] encodeValues = new Byte[length];//申请存放空间
            ec.GetBytes(charArray, 0, charArray.Length, encodeValues, 0, true);//将字符串按照特定字符编码转换为二进制数组

            return FillLengthHead(encodeValues, (UInt16)encodeValues.Length);
        }

        public override Object Decode(byte[] data, ref Int32 index)
        {
            /*
            Int32 length = (Int32)VUInt16.Instance.Decode(srcData, ref index, true);
            Byte[] result = new Byte[length];
            Buffer.BlockCopy(srcData, index, result, 0, length);
            ////Array.Reverse(result);
            index += length;

            return result;
            Byte[] strData = CutLengthHead(data, ref index);
            return m_encoding.GetString(strData);
             * */

            
            int length = (int)VUInt16.Instance.Decode(data, ref index, true);
            string result = m_encoding.GetString(data, index, length);
            index += length;
            return result;
        }

        /// <summary>
        /// 填充数据长度头。
        /// </summary>
        /// <param name="srcData">源二进制数组</param>
        /// <param name="length">源二进制数组数据长度</param>
        /// <returns>填充二进制数组长度到头部的数据</returns>
        public static Byte[] FillLengthHead(Byte[] srcData, UInt16 length)
        {
            Byte[] lengthByteArray = BitConverterExtend.GetBytes(length);//将字符串长度转换为二进制数组
            //Array.Reverse(lengthByteArray);
            Byte[] result = new Byte[length + lengthByteArray.Length];//申请存放字符串长度和字符串内容的空间
            Buffer.BlockCopy(lengthByteArray, 0, result, 0, lengthByteArray.Length);//将长度的二进制数组拷贝到目标空间
            Buffer.BlockCopy(srcData, 0, result, lengthByteArray.Length, length);//将字符串的二进制数组拷贝到目标空间
            return result;
        }
    }
}
