﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：VInt8
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.1.16
// 模块描述：8位整数（char）。
//==========================================*/
#endregion
using System;
using System.Runtime.InteropServices;

namespace MogoEngine.RPC
{
    /// <summary>
    /// 8位整数（char）。
    /// </summary>
    public class VInt8 : VObject
    {
        private static VInt8 m_instance = new VInt8();
        public static VInt8 Instance
        {
            get
            {
                return m_instance;
            }
        }

        public VInt8()
            : base(typeof(sbyte), VType.V_INT8, 1)//Marshal.SizeOf(typeof(sbyte)))
        {
        }
        public VInt8(Object vValue)
            : base(typeof(sbyte), VType.V_INT8, vValue)
        {
        }

        static byte[][] _cacheBytes = new byte[byte.MaxValue][];
        public byte[] Encode(byte vValue)
        {
            if (_cacheBytes[vValue] == null)
            {
                _cacheBytes[vValue] = new byte[1] { (byte)Convert.ToSByte(vValue) };
            }
            return _cacheBytes[vValue];
        }

        public override Object Decode(byte[] data, ref Int32 index)
        {
            return Decode(data, ref index, true);
        }

        public SByte Decode(byte[] data, ref Int32 index, bool fakeFlag)
        {
            Byte[] result = new Byte[1];
            Buffer.BlockCopy(data, index, result, 0, 1);
            index += 1;
            SByte rs = 0;

            if(result[0] > 128)
            {
                rs = (SByte)(result[0] - 256);
            }
            else
            {
                rs = (SByte)(result[0]);
            }
            return rs;
        }
    }
}