﻿#region 模块信息
/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：VInt64
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.4.23
// 模块描述：64位无符号整数（UInt64）。
//==========================================*/
#endregion
using GameLoader.Utils;
using System;
using System.Runtime.InteropServices;

namespace MogoEngine.RPC
{
    /// <summary>
    /// 64位无符号整数（UInt64）。
    /// </summary>
    public class VInt64 : VObject
    {
        private static VInt64 m_instance = new VInt64();
        public static VInt64 Instance
        {
            get
            {
                return m_instance;
            }
        }

        public VInt64()
            : base(typeof(Int64), VType.V_INT64, 8)//Marshal.SizeOf(typeof(Int64)))
        {
        }
        public VInt64(Object vValue)
            : base(typeof(Int64), VType.V_INT64, vValue)
        {
        }

        public byte[] Encode(object vValue)
        {
            var result = BitConverterExtend.GetBytes(Convert.ToInt64(vValue));
            //Array.Reverse(result);
            return result;
        }

        public override Object Decode(byte[] data, ref Int32 index)
        {
            return Decode(data, ref index, true);
        }

        public Int64 Decode(byte[] data, ref Int32 index, bool fakeFlag)
        {
            Int64 result = BitConverterExtend.ToInt64(data, index);
            index += VTypeLength;
            return result;
        }
    }
}