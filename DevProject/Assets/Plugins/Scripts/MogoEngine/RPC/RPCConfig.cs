﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MogoEngine.RPC
{
    public static class RPCConfig
    {
        public readonly static string RPC_HEAD = "RPC_";
        public readonly static string SVR_RPC_HEAD = "SVR_RPC_";

        public readonly static float POSITION_SCALE = 100f;
    }
}
