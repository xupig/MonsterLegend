﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;

using GameLoader.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace MogoEngine.Events
{
    //无参数事件池
    internal class GameEventPool
    {
        private static Stack<GameEvent> _pool = new Stack<GameEvent>();

        public static GameEvent Get(string type)
        {
            if(_pool.Count == 0)
            {
                return new GameEvent(type);
            }
            return _pool.Pop();
        }

        public static void Recycle(GameEvent evt)
        {
            evt.Reset();
            _pool.Push(evt);
        }
    }

    //单参数事件池，只池化int和string类型的事件
    internal class GameEventPool<T>
    {
        private static Stack<GameEvent<T>> _pool = new Stack<GameEvent<T>>();

        public static GameEvent<T> Get(string type)
        {
            if(_pool.Count == 0)
            {
                return new GameEvent<T>(type);
            }
            return _pool.Pop();
        }

        public static void Recycle(GameEvent<T> evt)
        {
            evt.Reset();
            _pool.Push(evt);
        }
    }

    internal class GameEvent : GameEventBase
    {
        public GameEvent(string type)
            : base(type)
        {
        }

        public void AddEventListener(Action listener)
        {
            base.AddEventListener(listener);
        }

        public void RemoveEventListener(Action listener)
        {
            base.RemoveEventListener(listener);
        }

        public void Invoke()
        {
            for(int i = 0; i < _list.Count; i++)
            {
                Action action = _list[i] as Action;
                action.Invoke();
            }
        }

    }

    internal class GameEvent<T> : GameEventBase
    {
        public GameEvent(string type)
            : base(type)
        {
        }

        public void AddEventListener(Action<T> listener)
        {
            base.AddEventListener(listener);
        }

        public void RemoveEventListener(Action<T> listener)
        {
            base.RemoveEventListener(listener);
        }

        public void Invoke(T arg1)
        {
            for(int i = 0; i < _list.Count; i++)
            {
                Action<T> action = _list[i] as Action<T>;
                action.Invoke(arg1);
            }
        }

    }

    internal class GameEvent<T, U> : GameEventBase
    {
        public GameEvent(string type)
            : base(type)
        {
        }

        public void AddEventListener(Action<T, U> listener)
        {
            base.AddEventListener(listener);
        }

        public void RemoveEventListener(Action<T, U> listener)
        {
            base.RemoveEventListener(listener);
        }

        public void Invoke(T arg1, U arg2)
        {
            for(int i = 0; i < _list.Count; i++)
            {
                Action<T, U> action = _list[i] as Action<T, U>;
                action.Invoke(arg1, arg2);
            }
        }

    }

    internal class GameEvent<T, U, V> : GameEventBase
    {
        public GameEvent(string type)
            : base(type)
        {
        }

        public void AddEventListener(Action<T, U, V> listener)
        {
            base.AddEventListener(listener);
        }

        public void RemoveEventListener(Action<T, U, V> listener)
        {
            base.RemoveEventListener(listener);
        }

        public void Invoke(T arg1, U arg2, V arg3)
        {
            for(int i = 0; i < _list.Count; i++)
            {
                Action<T, U, V> action = _list[i] as Action<T, U, V>;
                action.Invoke(arg1, arg2, arg3);
            }
        }

    }

    internal class GameEvent<T, U, V, W> : GameEventBase
    {
        public GameEvent(string type)
            : base(type)
        {
        }

        public void AddEventListener(Action<T, U, V, W> listener)
        {
            base.AddEventListener(listener);
        }

        public void RemoveEventListener(Action<T, U, V, W> listener)
        {
            base.RemoveEventListener(listener);
        }

        public void Invoke(T arg1, U arg2, V arg3, W arg4)
        {
            for(int i = 0; i < _list.Count; i++)
            {
                Action<T, U, V, W> action = _list[i] as Action<T, U, V, W>;
                action.Invoke(arg1, arg2, arg3, arg4);
            }
        }

    }

    internal abstract class GameEventBase
    {
        protected string _type;
        protected List<Delegate> _list;

        public GameEventBase(string type)
        {
            _type = type;
            _list = new List<Delegate>();
        }

        protected void RemoveEventListener(Delegate listener)
        {
            for(int i = _list.Count - 1; i >= 0; i--)
            {
                Delegate d = _list[i];
                if(d.Method == listener.Method && d.Target == listener.Target)
                {
                    _list.RemoveAt(i);
                }
            }
        }

        protected void AddEventListener(Delegate listener)
        {
            for(int i = 0; i < _list.Count; i++)
            {
                Delegate d = _list[i];
                if(d.Method == listener.Method && d.Target == listener.Target)
                {
                    LoggerHelper.Warning(string.Format("重复添加事件监听  EventType: {0} EventTarget: {1} EventListener: {2}", this._type, listener.Target, listener.Method.Name));
                    return;
                }
            }
            _list.Add(listener);
        }

        public int GetListenerCount()
        {
            return _list.Count;
        }

        public void Reset()
        {
            _type = string.Empty;
            _list.Clear();
        }
    }
}
