﻿using UnityEngine;

public class LateUpdateDelegate : MonoBehaviour
{
    public new string name;//为排除警告，加回new
    public delegate void VoidDelegate();
    public VoidDelegate onLateUpdate;
    void LateUpdate()
    {
        if (onLateUpdate != null) onLateUpdate();
    }
}
