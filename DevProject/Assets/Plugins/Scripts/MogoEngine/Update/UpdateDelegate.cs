﻿using UnityEngine;

namespace MogoEngine
{
    public class UpdateDelegateBase : MonoBehaviour
    {
        public string updateName;
        public delegate void VoidDelegate();
        public VoidDelegate onUpdate;
        virtual protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }
}