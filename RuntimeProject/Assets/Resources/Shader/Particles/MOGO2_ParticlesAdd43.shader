﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/MOGO2/Particles/MOGO2_ParticlesAdd43" {
Properties {
	_TintColor ("Tint Color", Color) = (1,1,1,1)
	_MainTex ("Particle Texture", 2D) = "white" {}
	_ZOffset ("Z Offset",float) = 0
	
	[HideInInspector]_AddSrcColorBlendFactor("Source Color Blend Factor",float) = 5		// default : SrcAlpha = 5
	[HideInInspector]_AddDstColorBlendFactor("Dest Color Blend Factor",float) = 1
	[HideInInspector]_AddSrcAlphaBlendFactor("Source Alpha Blend Factor",float) = 1
	[HideInInspector]_AddDstAlphaBlendFactor("Dest Alpha Blend Factor",float) = 1
	[HideInInspector]_AddColorBlendOp("Color Blend Operation",float) = 0
	[HideInInspector]_AddAlphaBlendOp("Alpha Blend Operation",float) = 4
}

Category {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	Cull Off Lighting Off ZWrite Off Fog { Color (0,0,0,0) }
		
	SubShader {
	
		LOD 300
		
		Blend SrcAlpha One, One One
		BlendOp Add,Max
		
		Pass {
		
			CGPROGRAM
			//#pragma exclude_renderers gles
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest

			#include "../Mogo2Include.cginc"

			sampler2D _MainTex;
			fixed4 _TintColor;
			half _ZOffset;
			
			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};
			
			float4 _MainTex_ST;

			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.vertex.z -= _ZOffset;
				
				o.color = _TintColor * v.color;
				o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
				
				return o;
			}

			fixed4 frag (v2f i) : COLOR
			{
				fixed4 c = tex2D(_MainTex, i.texcoord) * i.color * 2;
				
				//c.a = 1;
				//c.a *= max( c.r, max( c.g, c.b ) );
				ComputeAlphaForAdditive(c);
				
				return c;
			}
			ENDCG 
		}
	} 	
	
	
	
	FallBack "Particles/Additive" 
}
}
