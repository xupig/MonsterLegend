// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

#warning Upgrade NOTE: unity_Scale shader variable was removed; replaced 'unity_Scale.w' with '1.0'

Shader "XGame/Mogo/Particle/MogoCircleGradient" {
Properties {
	[HideInInspector]_MainTex ("Base (RGB)", 2D) = "white" {}
	_BaseColor ("Base Color(RGBA)", Color) = (1,1,1,1)
	_InternalAlpha( "Internal Alpha", Range(0,1) ) = 0.4
	[HideInInspector]_thickness("Thickness", float) = 0.02
	[HideInInspector]_center("Center", Vector) = (0,0,0)
	[HideInInspector]_radius("Radius", float) = 0.5
}

SubShader {
	Tags { "RenderType"="Transparent" "Queue"="Transparent" }
	//LOD 300

	Blend SrcAlpha OneMinusSrcAlpha
	ZWrite Off
	
	Pass {  
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest

			#include "UnityCG.cginc"

			fixed4 _BaseColor;
			half _InternalAlpha;
			half _thickness;
			half3 _center;
			half _radius;

			struct appdata_t {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				half4 texcoord : TEXCOORD0;
			};

			
			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.texcoord = v.vertex;
				return o;
			}
			
			fixed4 frag (v2f i) : COLOR
			{
				fixed4 col				= _BaseColor;
				
				half d = distance(i.texcoord.xyz, _center);

				col.a = step(d, _radius);   //draw ouline

				col.a *= step(_radius - d, _thickness) + max(0.1, col.a * d / _radius - (1-_InternalAlpha));  //draw thickness or fill color

				return col;
			}
		ENDCG
	}
}

}
