// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/Mogo/UIAdd_Flow" {
	Properties {
		_TintColor ("Tint Color", Color) = (1,1,1,1)
		_MainTex ("UI Texture", 2D) = "white" {}
		_FlowMap ("Flow Map (RGB)", 2D) = "black" {}
		_FluxParams0("UI Tex Flux Params", Vector)																				= (1.0, 1.0, 1.0, 1.0)
		_FluxParams1("Flow Map Flux Params", Vector)																				= (1.0, 1.0, 1.0, 1.0)
		_FluxColor ("Flow Map Flux Color", Color) 																			= (1.0, 1.0, 1.0, 1.0)
	}

	Category {
		Tags { "Queue"="Transparent+20" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Blend SrcAlpha One
		Cull Off Lighting Off ZWrite Off ZTest On Fog { Color (0,0,0,0) }
	
		BindChannels {
			Bind "Color", color
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
		}
	
		SubShader 
		{
			Pass 
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
			
				#include "UnityCG.cginc"

				uniform	fixed4		_TintColor;
				uniform sampler2D	_MainTex;
				uniform sampler2D 	_FlowMap;
				uniform fixed4		_FluxColor;
				uniform half4 		_FluxParams0;
				uniform half4 		_FluxParams1;

				struct appdata_t {
					float4 vertex : POSITION;
					fixed4 color : COLOR;
					float2 texcoord : TEXCOORD0;
				};

				struct v2f {
					float4 vertex		: POSITION;
					fixed4 color		: COLOR;
					half2  texcoord		: TEXCOORD0;
					half2  uvFlux		: TEXCOORD1;
				};
			
				float4 _MainTex_ST;

				v2f vert (appdata_t v)
				{
					v2f o = (v2f)0;

					o.vertex = UnityObjectToClipPos(v.vertex);
				
					o.color = v.color;

					o.texcoord = v.texcoord.xy * _FluxParams0.xy + _Time.x * _FluxParams0.zw;//TRANSFORM_TEX(v.texcoord,_MainTex);

					o.uvFlux.xy = v.texcoord.xy * _FluxParams1.xy + _Time.x * _FluxParams1.zw;
				
					return o;
				}

				fixed4 frag (v2f i) : COLOR
				{
					fixed4 c	= tex2D( _MainTex, i.texcoord );
					fixed4 flow = tex2D( _FlowMap,  i.uvFlux.xy );
					c.rgb = c.rgb * _TintColor + flow.rgb * _FluxColor.rgb * c.rgb * flow.a;
				
					return c * i.color;
				}

				ENDCG 
			}
		} 	

		SubShader {
			Pass {
				SetTexture [_MainTex] {
					combine texture * primary
				}
			}
		}
	}
}