// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/Shader Forge/sf_aion_RGBA_add_dissolve" {
    Properties {
        _Main_Map ("Main_Map", 2D) = "white" {}
        _Main_Color ("Main_Color", Color) = (1,1,1,1)
        _Strengh ("Strengh", Float ) = 1
        [MaterialToggle] _RGB_Channel ("RGB_Channel", Float ) = 0
        [MaterialToggle] _R_Channel ("R_Channel", Float ) = 0
        [MaterialToggle] _G_Channel ("G_Channel", Float ) = 0
        [MaterialToggle] _B_Channel ("B_Channel", Float ) = 0
        [MaterialToggle] _A_Channel ("A_Channel", Float ) = 0
        _Noise_Map ("Noise_Map", 2D) = "white" {}
        [MaterialToggle] _noise_R ("noise_R", Float ) = 0
        [MaterialToggle] _noise_G ("noise_G", Float ) = 0
        [MaterialToggle] _noise_B ("noise_B", Float ) = 0
        [MaterialToggle] _noise_A ("noise_A", Float ) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform sampler2D _Main_Map; uniform float4 _Main_Map_ST;
            uniform float4 _Main_Color;
            uniform float _Strengh;
            uniform fixed _R_Channel;
            uniform fixed _G_Channel;
            uniform fixed _B_Channel;
            uniform fixed _A_Channel;
            uniform fixed _RGB_Channel;
            uniform sampler2D _Noise_Map; uniform float4 _Noise_Map_ST;
            uniform fixed _noise_R;
            uniform fixed _noise_G;
            uniform fixed _noise_B;
            uniform fixed _noise_A;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float node_4226 = 0.0;
                float4 _Main_Map_var = tex2D(_Main_Map,TRANSFORM_TEX(i.uv0, _Main_Map));
                float node_7661 = 0.0;
                float4 _Noise_Map_var = tex2D(_Noise_Map,TRANSFORM_TEX(i.uv0, _Noise_Map));
                float3 emissive = saturate(((_Main_Color.rgb*i.vertexColor.rgb*_Strengh)*lerp( (lerp( node_4226, _Main_Map_var.r, _R_Channel )+lerp( node_4226, _Main_Map_var.g, _G_Channel )+lerp( node_4226, _Main_Map_var.b, _B_Channel )+lerp( node_4226, _Main_Map_var.a, _A_Channel )), _Main_Map_var.rgb, _RGB_Channel )*((i.vertexColor.a*_Main_Color.a*4.0)*((lerp( node_7661, _Noise_Map_var.r, _noise_R )+lerp( node_7661, _Noise_Map_var.g, _noise_G )+lerp( node_7661, _Noise_Map_var.b, _noise_B )+lerp( node_7661, _Noise_Map_var.a, _noise_A ))*5.0+-1.0))));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    //CustomEditor "ShaderForgeMaterialInspector"
}
