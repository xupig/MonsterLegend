// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/Shader Forge/sf_aion_ice" {
    Properties {
        _BumpMap ("Normal Map", 2D) = "bump" {}
        _Color ("Color", Color) = (0.5661765,0.3247189,0.3247189,1)
        _Opacity ("Opacity", Range(0, 1)) = 0.4
        _Refraction ("Refraction", Range(0, 1)) = 1
        _Normal ("Normal", Range(0, 4)) = 1.076923
        _Specular ("Specular", Range(0, 1)) = 1
        _Gloss ("Gloss", Range(0, 1)) = 0.8
        _Fresnel ("Fresnel", Range(0, 4)) = 0.78
        _Strengh ("Strengh", Float ) = 4
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
		[HideInInspector]_AmbientColor("AmbientColor", Color) = (0.212, 0.227, 0.259, 1)
    }


	CGINCLUDE
	#include "UnityCG.cginc"
	#include "UnityPBSLighting.cginc"
	#include "UnityStandardBRDF.cginc"
	uniform sampler2D _GrabTexture;
	uniform float4 _Color;
	uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
	uniform float _Opacity;
	uniform float _Normal;
	uniform float _Gloss;
	uniform float _Specular;
	uniform float _Fresnel;
	uniform float _Refraction;
	uniform float _Strengh;
	uniform fixed4 _AmbientColor;
	struct VertexInput {
		float4 vertex : POSITION;
		float3 normal : NORMAL;
		float4 tangent : TANGENT;
		float2 texcoord0 : TEXCOORD0;
		float4 vertexColor : COLOR;
	};
	struct VertexOutput {
		float4 pos : SV_POSITION;
		float2 uv0 : TEXCOORD0;
		float4 posWorld : TEXCOORD1;
		float3 normalDir : TEXCOORD2;
		float3 tangentDir : TEXCOORD3;
		float3 bitangentDir : TEXCOORD4;
		float4 screenPos : TEXCOORD5;
		float4 vertexColor : COLOR;
		UNITY_FOG_COORDS(6)
	};
	VertexOutput vert(VertexInput v) {
		VertexOutput o = (VertexOutput)0;
		o.uv0 = v.texcoord0;
		o.vertexColor = v.vertexColor;
		o.normalDir = UnityObjectToWorldNormal(v.normal);
		o.tangentDir = normalize(mul(unity_ObjectToWorld, float4(v.tangent.xyz, 0.0)).xyz);
		o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
		o.posWorld = mul(unity_ObjectToWorld, v.vertex);
		float3 lightColor = _LightColor0.rgb;
		o.pos = UnityObjectToClipPos(v.vertex);
		UNITY_TRANSFER_FOG(o, o.pos);
		o.screenPos = o.pos;
		return o;
	}

	float4 fragFinal(VertexOutput i, bool useGrabTexture) {
		#if UNITY_UV_STARTS_AT_TOP
			float grabSign = -_ProjectionParams.x;
		#else
			float grabSign = _ProjectionParams.x;
		#endif
		i.normalDir = normalize(i.normalDir);
		i.screenPos = float4(i.screenPos.xy / i.screenPos.w, 0, 0);
		i.screenPos.y *= _ProjectionParams.x;
		float3x3 tangentTransform = float3x3(i.tangentDir, i.bitangentDir, i.normalDir);
		float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
		float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
		float3 normalLocal = lerp(float3(0,0,1),_BumpMap_var.rgb,_Normal);
		float3 normalDirection = normalize(mul(normalLocal, tangentTransform)); // Perturbed normals
		float3 viewReflectDirection = reflect(-viewDirection, normalDirection);
		float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
		float3 lightColor = _LightColor0.rgb;
		float3 halfDirection = normalize(viewDirection + lightDirection);
		////// Lighting:
		float attenuation = 1;
		float3 attenColor = attenuation * _LightColor0.xyz;
		///////// Gloss:
		float gloss = _Gloss;
		float specPow = exp2(gloss * 10.0 + 1.0);
		/////// GI Data:
		UnityLight light;
		#ifdef LIGHTMAP_OFF
		light.color = lightColor;
		light.dir = lightDirection;
		light.ndotl = LambertTerm(normalDirection, light.dir);
		#else
		light.color = half3(0.f, 0.f, 0.f);
		light.ndotl = 0.0f;
		light.dir = half3(0.f, 0.f, 0.f);
		#endif
		UnityGIInput d;
		d.light = light;
		d.worldPos = i.posWorld.xyz;
		d.worldViewDir = viewDirection;
		d.atten = attenuation;
		d.ambient = 0;
		d.lightmapUV = 0;
		d.boxMax[0] = unity_SpecCube0_BoxMax;
		d.boxMin[0] = unity_SpecCube0_BoxMin;
		d.probePosition[0] = unity_SpecCube0_ProbePosition;
		d.probeHDR[0] = unity_SpecCube0_HDR;
		d.boxMax[1] = unity_SpecCube1_BoxMax;
		d.boxMin[1] = unity_SpecCube1_BoxMin;
		d.probePosition[1] = unity_SpecCube1_ProbePosition;
		d.probeHDR[1] = unity_SpecCube1_HDR;

		Unity_GlossyEnvironmentData ugls_en_data;
		ugls_en_data.roughness = 1.0 - gloss;
		ugls_en_data.reflUVW = viewReflectDirection;
		UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data);
		lightDirection = gi.light.dir;
		lightColor = gi.light.color;
		////// Specular:
		float NdotL = max(0, dot(normalDirection, lightDirection));
		float3 specularColor = float3(_Specular,_Specular,_Specular);
		float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
		float3 indirectSpecular = (gi.indirect.specular + i.vertexColor.rgb)*specularColor;
		float3 specular = (directSpecular + indirectSpecular);
		/////// Diffuse:
		NdotL = max(0.0,dot(normalDirection, lightDirection));
		float3 directDiffuse = max(0.0, NdotL) * attenColor;
		float3 indirectDiffuse = float3(0,0,0);
		indirectDiffuse += _AmbientColor.rgb; // Ambient Light
		float3 diffuseColor = (i.vertexColor.rgb*_Color.rgb);
		float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
		////// Emissive:
		float3 emissive = (_Color.rgb*pow(1.0 - max(0,dot(normalDirection, viewDirection)),_Fresnel)*_Strengh*i.vertexColor.rgb);
		/// Final Color:
		float3 finalColor = diffuse + specular + emissive;
		fixed4 finalRGBA;
		if (useGrabTexture) {
			float2 sceneUVs = float2(1, grabSign)*i.screenPos.xy*0.5 + 0.5 + (float2(_BumpMap_var.r, _BumpMap_var.g)*_Refraction);
			float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
			finalRGBA = fixed4(lerp(sceneColor.rgb, finalColor, saturate((i.vertexColor.a*_Opacity*_Color.a))), 1);
		}
		else {
			finalRGBA = fixed4(finalColor, saturate(i.vertexColor.a*_Opacity*_Color.a));
		}	
		UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
		return finalRGBA;
	}
	ENDCG

    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent+10"
            "RenderType"="Transparent"
        }
		LOD 1000
		GrabPass{ "_GrabTexture" }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            //Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
			#pragma skip_variants LIGHTMAP_ON DYNAMICLIGHTMAP_ON
            #pragma target 3.0
			float4 frag(VertexOutput i) : SV_Target {
				return fragFinal(i, true);
			}
            
            ENDCG
        }
    }

	SubShader {
		Tags {
			"IgnoreProjector" = "True"
			"Queue" = "Transparent+10"
			"RenderType" = "Transparent"
		}
		LOD 300
		Pass {
			Name "FORWARD"
			Tags {
				"LightMode" = "ForwardBase"
			}
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#define UNITY_PASS_FORWARDBASE
			#pragma multi_compile_fwdbase
			#pragma multi_compile_fog
			#pragma skip_variants LIGHTMAP_ON DYNAMICLIGHTMAP_ON
			#pragma target 3.0
			float4 frag(VertexOutput i) : SV_Target{
				return fragFinal(i, false);
			}
			ENDCG
		}
	}
}
