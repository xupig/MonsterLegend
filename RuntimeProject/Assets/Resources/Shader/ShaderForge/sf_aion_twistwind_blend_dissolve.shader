Shader "XGame/Shader Forge/sf_aion_twistwind_blend_dissolve" {
    Properties {
        _Main_Map ("Main_Map", 2D) = "black" {}
        _TintColor ("Color", Color) = (1,1,1,1)
        _Strengh ("Strengh", Float ) = 1
        _R_color ("R_color", Color) = (0.5,0.5,0.5,1)
        _R_Uspeed ("R_Uspeed", Float ) = 0
        _R_Vspeed ("R_Vspeed", Float ) = 0
        _G_color ("G_color", Color) = (0.5,0.5,0.5,1)
        _G_Uspeed ("G_Uspeed", Float ) = 0
        _G_Vspeed ("G_Vspeed", Float ) = 0
        _B_color ("B_color", Color) = (0.5,0.5,0.5,1)
        _B_Uspeed ("B_Uspeed", Float ) = 0
        _B_Vspeed ("B_Vspeed", Float ) = 0
        _A_color ("A_color", Color) = (0.5,0.5,0.5,1)
        _A_Uspeed ("A_Uspeed", Float ) = 0
        _A_Vspeed ("A_Vspeed", Float ) = 0
        _dissolve ("dissolve", Range(0, 1)) = 1
        [MaterialToggle] _Use_Opacity ("Use_Opacity", Float ) = 1.2
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float4 _TintColor;
            uniform sampler2D _Main_Map; uniform float4 _Main_Map_ST;
            uniform float _R_Uspeed;
            uniform float _R_Vspeed;
            uniform float _G_Uspeed;
            uniform float _G_Vspeed;
            uniform float _B_Uspeed;
            uniform float _B_Vspeed;
            uniform float _A_Uspeed;
            uniform float _A_Vspeed;
            uniform float4 _R_color;
            uniform float4 _G_color;
            uniform float4 _B_color;
            uniform float4 _A_color;
            uniform float _Strengh;
            uniform float _dissolve;
            uniform fixed _Use_Opacity;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 node_2318 = _Time;
                float2 node_6956 = ((node_2318.g*float2(_A_Uspeed,_A_Vspeed))+i.uv0);
                float4 node_1449 = tex2D(_Main_Map,TRANSFORM_TEX(node_6956, _Main_Map));
                float node_7971 = ((node_1449.a+((_dissolve*1.0+0.0)*1.2+-0.5))*6.0+-3.0);
                float node_9774 = saturate((_TintColor.a*i.vertexColor.a*node_7971));
                clip(lerp( node_9774, node_7971, _Use_Opacity ) - 0.5);
////// Lighting:
////// Emissive:
                float2 node_2811 = ((node_2318.g*float2(_R_Uspeed,_R_Vspeed))+i.uv0);
                float4 _MainTex = tex2D(_Main_Map,TRANSFORM_TEX(node_2811, _Main_Map));
                float2 node_2530 = ((node_2318.g*float2(_G_Uspeed,_G_Vspeed))+i.uv0);
                float4 node_5463 = tex2D(_Main_Map,TRANSFORM_TEX(node_2530, _Main_Map));
                float2 node_2773 = ((node_2318.g*float2(_B_Uspeed,_B_Vspeed))+i.uv0);
                float4 node_7251 = tex2D(_Main_Map,TRANSFORM_TEX(node_2773, _Main_Map));
                float3 emissive = (((_R_color.rgb*_MainTex.r)+(_G_color.rgb*node_5463.g)+(_B_color.rgb*node_7251.b)+(_A_color.rgb*node_1449.a*i.vertexColor.rgb))*i.vertexColor.rgb*_TintColor.rgb*_Strengh);
                float3 finalColor = emissive;
                return fixed4(finalColor,node_9774);
            }
            ENDCG
        }
    }
    //CustomEditor "ShaderForgeMaterialInspector"
}
