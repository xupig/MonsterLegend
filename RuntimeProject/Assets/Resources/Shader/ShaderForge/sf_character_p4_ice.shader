Shader "XGame/Shader Forge/sf_character_p4_ice" {
    Properties {
        _MainTex ("Main_Map", 2D) = "white" {}
        _Dissolve_Map ("Dissolve_Map", 2D) = "white" {}
        _Dissolve ("Dissolve", Range(0, 1)) = 1
        [MaterialToggle] _Dissolve_UV2 ("Dissolve_UV2", Float ) = 0
        _Ice_Color ("Ice_Color", Color) = (0.9632353,0.9893509,1,1)
        _Normal_Map ("Normal_Map", 2D) = "bump" {}
        _Gloss ("Gloss", Range(0, 1)) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float _Dissolve;
            uniform sampler2D _Dissolve_Map; uniform float4 _Dissolve_Map_ST;
            uniform fixed _Dissolve_UV2;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Ice_Color;
            uniform sampler2D _Normal_Map; uniform float4 _Normal_Map_ST;
            uniform float _Gloss;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD10;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
#if !defined(SHADER_API_GLES)
                float2 _Dissolve_UV2_var = lerp( o.uv0, o.uv1, _Dissolve_UV2 );
                float4 _Dissolve_Map_var = tex2Dlod(_Dissolve_Map,float4(TRANSFORM_TEX(_Dissolve_UV2_var, _Dissolve_Map),0.0,0));
                float node_8987 = step(_Dissolve,_Dissolve_Map_var.r);
                float3 _Normal_Map_var = UnpackNormal(tex2Dlod(_Normal_Map,float4(TRANSFORM_TEX(_Dissolve_UV2_var, _Normal_Map),0.0,0)));
                float3 node_7383 = (node_8987*_Normal_Map_var.rgb);
                float3 node_8280 = (0.02*node_7383*v.normal);
				v.vertex.xyz += node_8280;
#endif
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float2 _Dissolve_UV2_var = lerp( i.uv0, i.uv1, _Dissolve_UV2 );
                float4 _Dissolve_Map_var = tex2D(_Dissolve_Map,TRANSFORM_TEX(_Dissolve_UV2_var, _Dissolve_Map));
                float node_8987 = step(_Dissolve,_Dissolve_Map_var.r);
                float3 _Normal_Map_var = UnpackNormal(tex2D(_Normal_Map,TRANSFORM_TEX(_Dissolve_UV2_var, _Normal_Map)));
                float3 node_7383 = (node_8987*_Normal_Map_var.rgb);
                float node_5203 = 4.0;
                float3 normalLocal = lerp(float3(0,0,1),node_7383,(node_5203*(1.0 - _Dissolve)));
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0 + 1.0 );
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
				d.boxMax[0] = unity_SpecCube0_BoxMax;
				d.boxMin[0] = unity_SpecCube0_BoxMin;
				d.probePosition[0] = unity_SpecCube0_ProbePosition;
				d.probeHDR[0] = unity_SpecCube0_HDR;
				d.boxMax[1] = unity_SpecCube1_BoxMax;
				d.boxMin[1] = unity_SpecCube1_BoxMin;
				d.probePosition[1] = unity_SpecCube1_ProbePosition;
				d.probeHDR[1] = unity_SpecCube1_HDR;

                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float node_7548 = pow(1.0-max(0,dot(normalDirection, viewDirection)),node_5203);
                float3 specularColor = (_Ice_Color.rgb*node_8987*(1.0 - node_7548));
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 indirectSpecular = (gi.indirect.specular)*specularColor;
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 diffuseColor = (_MainTex_var.rgb*node_8987);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float3 emissive = lerp(_MainTex_var.rgb,(_Ice_Color.rgb*node_7548),node_8987);
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float _Dissolve;
            uniform sampler2D _Dissolve_Map; uniform float4 _Dissolve_Map_ST;
            uniform fixed _Dissolve_UV2;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Ice_Color;
            uniform sampler2D _Normal_Map; uniform float4 _Normal_Map_ST;
            uniform float _Gloss;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
#if !defined(SHADER_API_GLES)
                float2 _Dissolve_UV2_var = lerp( o.uv0, o.uv1, _Dissolve_UV2 );
                float4 _Dissolve_Map_var = tex2Dlod(_Dissolve_Map,float4(TRANSFORM_TEX(_Dissolve_UV2_var, _Dissolve_Map),0.0,0));
                float node_8987 = step(_Dissolve,_Dissolve_Map_var.r);
                float3 _Normal_Map_var = UnpackNormal(tex2Dlod(_Normal_Map,float4(TRANSFORM_TEX(_Dissolve_UV2_var, _Normal_Map),0.0,0)));
                float3 node_7383 = (node_8987*_Normal_Map_var.rgb);
                float3 node_8280 = (0.02*node_7383*v.normal);
                v.vertex.xyz += node_8280;
#endif
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float2 _Dissolve_UV2_var = lerp( i.uv0, i.uv1, _Dissolve_UV2 );
                float4 _Dissolve_Map_var = tex2D(_Dissolve_Map,TRANSFORM_TEX(_Dissolve_UV2_var, _Dissolve_Map));
                float node_8987 = step(_Dissolve,_Dissolve_Map_var.r);
                float3 _Normal_Map_var = UnpackNormal(tex2D(_Normal_Map,TRANSFORM_TEX(_Dissolve_UV2_var, _Normal_Map)));
                float3 node_7383 = (node_8987*_Normal_Map_var.rgb);
                float node_5203 = 4.0;
                float3 normalLocal = lerp(float3(0,0,1),node_7383,(node_5203*(1.0 - _Dissolve)));
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float node_7548 = pow(1.0-max(0,dot(normalDirection, viewDirection)),node_5203);
                float3 specularColor = (_Ice_Color.rgb*node_8987*(1.0 - node_7548));
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 diffuseColor = (_MainTex_var.rgb*node_8987);
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_fog
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float _Dissolve;
            uniform sampler2D _Dissolve_Map; uniform float4 _Dissolve_Map_ST;
            uniform fixed _Dissolve_UV2;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Ice_Color;
            uniform sampler2D _Normal_Map; uniform float4 _Normal_Map_ST;
            uniform float _Gloss;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
#if !defined(SHADER_API_GLES)
                float2 _Dissolve_UV2_var = lerp( o.uv0, o.uv1, _Dissolve_UV2 );
                float4 _Dissolve_Map_var = tex2Dlod(_Dissolve_Map,float4(TRANSFORM_TEX(_Dissolve_UV2_var, _Dissolve_Map),0.0,0));
                float node_8987 = step(_Dissolve,_Dissolve_Map_var.r);
                float3 _Normal_Map_var = UnpackNormal(tex2Dlod(_Normal_Map,float4(TRANSFORM_TEX(_Dissolve_UV2_var, _Normal_Map),0.0,0)));
                float3 node_7383 = (node_8987*_Normal_Map_var.rgb);
                float3 node_8280 = (0.02*node_7383*v.normal);
                v.vertex.xyz += node_8280;
#endif
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_5203 = 4.0;
                float node_7548 = pow(1.0-max(0,dot(normalDirection, viewDirection)),node_5203);
                float2 _Dissolve_UV2_var = lerp( i.uv0, i.uv1, _Dissolve_UV2 );
                float4 _Dissolve_Map_var = tex2D(_Dissolve_Map,TRANSFORM_TEX(_Dissolve_UV2_var, _Dissolve_Map));
                float node_8987 = step(_Dissolve,_Dissolve_Map_var.r);
                o.Emission = lerp(_MainTex_var.rgb,(_Ice_Color.rgb*node_7548),node_8987);
                
                float3 diffColor = (_MainTex_var.rgb*node_8987);
                float3 specColor = (_Ice_Color.rgb*node_8987*(1.0 - node_7548));
                float roughness = 1.0 - _Gloss;
                o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
   // CustomEditor "ShaderForgeMaterialInspector"
}
