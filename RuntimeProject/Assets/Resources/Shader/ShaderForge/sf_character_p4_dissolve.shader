Shader "XGame/Shader Forge/sf_character_p4_dissolve" {
    Properties {
        _MainTex ("Main_Map", 2D) = "white" {}
        _Dissolve_Map ("Dissolve_Map", 2D) = "white" {}
        _Emission_Color ("Emission_Color", Color) = (0,0,0,1)
        _Dissolve_Border_Color ("Dissolve_Border_Color", Color) = (1,0.8896552,0,1)
        _Dissolve ("Dissolve", Range(0, 1)) = 1
        [MaterialToggle] _Dissolve_UV2 ("Dissolve_UV2", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float _Dissolve;
            uniform sampler2D _Dissolve_Map; uniform float4 _Dissolve_Map_ST;
            uniform fixed _Dissolve_UV2;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Emission_Color;
            uniform float4 _Dissolve_Border_Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float2 _Dissolve_UV2_var = lerp( i.uv0, i.uv1, _Dissolve_UV2 );
                float4 _Dissolve_Map_var = tex2D(_Dissolve_Map,TRANSFORM_TEX(_Dissolve_UV2_var, _Dissolve_Map));
                float node_7419 = (_Dissolve*(_Dissolve_Map_var.r*5.454545+0.2727274));
                clip(node_7419 - 0.5);
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 emissive = (_MainTex_var.rgb+((1.0 - saturate((node_7419*16.0+-8.0)))*_Dissolve_Border_Color.rgb*4.0)+_Emission_Color.rgb);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_fog
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float _Dissolve;
            uniform sampler2D _Dissolve_Map; uniform float4 _Dissolve_Map_ST;
            uniform fixed _Dissolve_UV2;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Emission_Color;
            uniform float4 _Dissolve_Border_Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float2 _Dissolve_UV2_var = lerp( i.uv0, i.uv1, _Dissolve_UV2 );
                float4 _Dissolve_Map_var = tex2D(_Dissolve_Map,TRANSFORM_TEX(_Dissolve_UV2_var, _Dissolve_Map));
                float node_7419 = (_Dissolve*(_Dissolve_Map_var.r*5.454545+0.2727274));
                o.Emission = (_MainTex_var.rgb+((1.0 - saturate((node_7419*16.0+-8.0)))*_Dissolve_Border_Color.rgb*4.0)+_Emission_Color.rgb);
                
                float3 diffColor = float3(0,0,0);
                o.Albedo = diffColor;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
   // CustomEditor "ShaderForgeMaterialInspector"
}
