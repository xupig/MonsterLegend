// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/Shader Forge/sf_character_p4_lighting" {
    Properties {
        _MainTex ("Main_Map", 2D) = "white" {}
        _Dissolve ("Dissolve", Range(0, 1)) = 0
        [MaterialToggle] _Dissolve_UV2 ("Dissolve_UV2", Float ) = 0
        _Speed ("Speed", Float ) = 1
        _Noise_Map ("Noise_Map", 2D) = "white" {}
        _Line_Map ("Line_Map", 2D) = "white" {}
        _Emission_Color ("Emission_Color", Color) = (0.5955882,0.8995942,1,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float _Dissolve;
            uniform fixed _Dissolve_UV2;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Speed;
            uniform sampler2D _Noise_Map; uniform float4 _Noise_Map_ST;
            uniform sampler2D _Line_Map; uniform float4 _Line_Map_ST;
            uniform float4 _Emission_Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                clip(_MainTex_var.a - 0.5);
////// Lighting:
////// Emissive:
                float2 _Dissolve_UV2_var = lerp( i.uv0, i.uv1, _Dissolve_UV2 );
                float4 node_9551 = _Time + _TimeEditor;
                float node_6439 = 0.0;
                float2 node_1241 = (_Dissolve_UV2_var+(node_9551.r*float2(1.0,node_6439)*_Speed));
                float4 node_6895 = tex2D(_Noise_Map,TRANSFORM_TEX(node_1241, _Noise_Map));
                float2 node_406 = (_Dissolve_UV2_var+(node_9551.r*float2(node_6439,2.0)*_Speed));
                float4 node_3066 = tex2D(_Noise_Map,TRANSFORM_TEX(node_406, _Noise_Map));
                float2 node_731 = float2(saturate((((node_6895.r+_Dissolve)*(node_3066.r+_Dissolve))*20.0+-10.0)),0.0);
                float4 _Line_Map_var = tex2D(_Line_Map,TRANSFORM_TEX(node_731, _Line_Map));
                float3 emissive = ((_MainTex_var.rgb*_Emission_Color.rgb)+(_Line_Map_var.rgb*_Emission_Color.rgb*4.0*_Emission_Color.a));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float _Dissolve;
            uniform fixed _Dissolve_UV2;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Speed;
            uniform sampler2D _Noise_Map; uniform float4 _Noise_Map_ST;
            uniform sampler2D _Line_Map; uniform float4 _Line_Map_ST;
            uniform float4 _Emission_Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float2 _Dissolve_UV2_var = lerp( i.uv0, i.uv1, _Dissolve_UV2 );
                float4 node_9551 = _Time + _TimeEditor;
                float node_6439 = 0.0;
                float2 node_1241 = (_Dissolve_UV2_var+(node_9551.r*float2(1.0,node_6439)*_Speed));
                float4 node_6895 = tex2D(_Noise_Map,TRANSFORM_TEX(node_1241, _Noise_Map));
                float2 node_406 = (_Dissolve_UV2_var+(node_9551.r*float2(node_6439,2.0)*_Speed));
                float4 node_3066 = tex2D(_Noise_Map,TRANSFORM_TEX(node_406, _Noise_Map));
                float2 node_731 = float2(saturate((((node_6895.r+_Dissolve)*(node_3066.r+_Dissolve))*20.0+-10.0)),0.0);
                float4 _Line_Map_var = tex2D(_Line_Map,TRANSFORM_TEX(node_731, _Line_Map));
                o.Emission = ((_MainTex_var.rgb*_Emission_Color.rgb)+(_Line_Map_var.rgb*_Emission_Color.rgb*4.0*_Emission_Color.a));
                
                float3 diffColor = float3(0,0,0);
                o.Albedo = diffColor;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
   // CustomEditor "ShaderForgeMaterialInspector"
}
