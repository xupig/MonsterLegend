// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.35 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.35;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:False,nrmq:0,nrsp:0,vomd:1,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:False,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:True,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:33187,y:32630,varname:node_3138,prsc:2|emission-8120-OUT,olwid-11-OUT,olcol-4953-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32214,y:32423,ptovrint:False,ptlb:Main_Color,ptin:_Main_Color,varname:_Main_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:3775,x:31767,y:32688,ptovrint:False,ptlb:Main_Map,ptin:_Main_Map,varname:_Main_Map,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:7559,x:32214,y:32653,varname:node_7559,prsc:2|A-3775-RGB,B-1339-OUT,C-423-OUT;n:type:ShaderForge.SFN_Vector4Property,id:3243,x:31113,y:33184,ptovrint:False,ptlb:Light_Position,ptin:Light_Position,varname:_Light_Postion,prsc:2,glob:True,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5,v2:0.5,v3:0.5,v4:1;n:type:ShaderForge.SFN_NormalVector,id:5169,x:31113,y:33334,prsc:2,pt:False;n:type:ShaderForge.SFN_Append,id:16,x:32236,y:33334,varname:node_16,prsc:2|A-3522-OUT,B-3522-OUT;n:type:ShaderForge.SFN_Dot,id:3522,x:31377,y:33332,varname:node_3522,prsc:2,dt:1|A-5169-OUT,B-3243-XYZ;n:type:ShaderForge.SFN_Tex2d,id:3169,x:32471,y:33348,ptovrint:False,ptlb:Falloff_Map,ptin:_Falloff_Map,varname:_Falloff_Map,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:a07463f9e8fdf5142ad1644e6f4689c6,ntxv:0,isnm:False|UVIN-16-OUT;n:type:ShaderForge.SFN_Color,id:8494,x:32115,y:32808,ptovrint:False,ptlb:Light_Color,ptin:_Light_Color,varname:_Light_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:1339,x:32605,y:33070,varname:node_1339,prsc:2|A-3169-RGB,B-8494-RGB;n:type:ShaderForge.SFN_Color,id:2729,x:32156,y:33131,ptovrint:False,ptlb:Shadow_Color,ptin:_Shadow_Color,varname:_Shadow_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.9411765,c2:0.9411765,c3:0.9411765,c4:1;n:type:ShaderForge.SFN_Slider,id:3505,x:31442,y:32609,ptovrint:False,ptlb:Light_Strengh,ptin:_Light_Strengh,varname:_Light_Strengh,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:4;n:type:ShaderForge.SFN_OneMinus,id:678,x:32373,y:33070,varname:node_678,prsc:2|IN-1339-OUT;n:type:ShaderForge.SFN_Multiply,id:1215,x:32516,y:32809,varname:node_1215,prsc:2|A-2729-RGB,B-678-OUT,C-3775-RGB;n:type:ShaderForge.SFN_Add,id:4754,x:32466,y:32657,varname:node_4754,prsc:2|A-7559-OUT,B-1215-OUT,C-2263-OUT;n:type:ShaderForge.SFN_Multiply,id:8231,x:32418,y:32481,varname:node_8231,prsc:2|A-7241-RGB,B-4754-OUT;n:type:ShaderForge.SFN_Add,id:2075,x:32746,y:32726,varname:node_2075,prsc:2|A-8231-OUT,B-3429-RGB;n:type:ShaderForge.SFN_Color,id:3429,x:32490,y:32276,ptovrint:False,ptlb:Emission_Color,ptin:_Emission_Color,varname:_Emission_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Vector1,id:3781,x:31628,y:32404,varname:node_3781,prsc:2,v1:0.2;n:type:ShaderForge.SFN_Add,id:423,x:31911,y:32419,cmnt:ZhangGe_need_0.2_more,varname:node_423,prsc:2|A-3781-OUT,B-3505-OUT;n:type:ShaderForge.SFN_Color,id:3383,x:32901,y:32325,ptovrint:False,ptlb:GameAmbientLight,ptin:GameAmbientLight,varname:node_3383,prsc:2,glob:True,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:3324,x:32854,y:32505,varname:node_3324,prsc:2|A-2075-OUT,B-3383-RGB;n:type:ShaderForge.SFN_ToggleProperty,id:4491,x:31931,y:32925,ptovrint:False,ptlb:Side_Color_On/off,ptin:_Side_Color_Onoff,varname:node_4491,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False;n:type:ShaderForge.SFN_Multiply,id:2263,x:32134,y:32958,varname:node_2263,prsc:2|A-4491-OUT,B-3316-OUT,C-9516-OUT;n:type:ShaderForge.SFN_Add,id:6038,x:31377,y:33186,varname:node_6038,prsc:2|A-5169-OUT,B-3243-XYZ;n:type:ShaderForge.SFN_ViewVector,id:8503,x:31563,y:33409,varname:node_8503,prsc:2;n:type:ShaderForge.SFN_Dot,id:5690,x:31737,y:33204,varname:node_5690,prsc:2,dt:1|A-8730-OUT,B-8503-OUT;n:type:ShaderForge.SFN_Negate,id:8730,x:31551,y:33204,varname:node_8730,prsc:2|IN-6038-OUT;n:type:ShaderForge.SFN_Slider,id:1731,x:31440,y:32995,ptovrint:False,ptlb:Side_Color_Strengh,ptin:_Side_Color_Strengh,varname:node_1731,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:10,max:20;n:type:ShaderForge.SFN_Multiply,id:4953,x:32926,y:32974,varname:node_4953,prsc:2|A-8828-RGB,B-3775-RGB;n:type:ShaderForge.SFN_Color,id:8828,x:32746,y:32928,ptovrint:False,ptlb:Outline_Color,ptin:_Outline_Color,varname:node_8828,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Slider,id:3767,x:32718,y:33204,ptovrint:False,ptlb:Outline,ptin:_Outline,varname:node_3767,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.2,max:1;n:type:ShaderForge.SFN_Multiply,id:11,x:33093,y:33217,varname:node_11,prsc:2|A-3767-OUT,B-6307-OUT;n:type:ShaderForge.SFN_Vector1,id:6307,x:32807,y:33304,varname:node_6307,prsc:2,v1:0.01;n:type:ShaderForge.SFN_Clamp01,id:8120,x:33004,y:32605,varname:node_8120,prsc:2|IN-3324-OUT;n:type:ShaderForge.SFN_Power,id:3316,x:31931,y:32995,varname:node_3316,prsc:2|VAL-5690-OUT,EXP-1731-OUT;n:type:ShaderForge.SFN_Vector1,id:9516,x:31644,y:32889,varname:node_9516,prsc:2,v1:100;proporder:7241-3775-3169-8494-3505-2729-3429-4491-1731-3767-8828;pass:END;sub:END;*/

Shader "XGame/Shader Forge/sf_toon_character_unlit" {
    Properties {
        _Main_Color ("Main_Color", Color) = (1,1,1,1)
        _Main_Map ("Main_Map", 2D) = "white" {}
        _Falloff_Map ("Falloff_Map", 2D) = "white" {}
        _Light_Color ("Light_Color", Color) = (1,1,1,1)
        _Light_Strengh ("Light_Strengh", Range(0, 4)) = 1
        _Shadow_Color ("Shadow_Color", Color) = (0.9411765,0.9411765,0.9411765,1)
        _Emission_Color ("Emission_Color", Color) = (0,0,0,1)
        [MaterialToggle] _Side_Color_Onoff ("Side_Color_On/off", Float ) = 0
        _Side_Color_Strengh ("Side_Color_Strengh", Range(1, 20)) = 10
        _Outline ("Outline", Range(0, 1)) = 0.2
        _Outline_Color ("Outline_Color", Color) = (0.5,0.5,0.5,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            uniform sampler2D _Main_Map; uniform float4 _Main_Map_ST;
            uniform float4 _Outline_Color;
            uniform float _Outline;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(float4(v.vertex.xyz + v.normal*(_Outline*0.01),1) );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 _Main_Map_var = tex2D(_Main_Map,TRANSFORM_TEX(i.uv0, _Main_Map));
                return fixed4((_Outline_Color.rgb*_Main_Map_var.rgb),0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            uniform float4 _Main_Color;
            uniform sampler2D _Main_Map; uniform float4 _Main_Map_ST;
            uniform float4 Light_Position;
            uniform sampler2D _Falloff_Map; uniform float4 _Falloff_Map_ST;
            uniform float4 _Light_Color;
            uniform float4 _Shadow_Color;
            uniform float _Light_Strengh;
            uniform float4 _Emission_Color;
            uniform float4 GameAmbientLight;
            uniform fixed _Side_Color_Onoff;
            uniform float _Side_Color_Strengh;
            uniform float4 _Add_Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _Main_Map_var = tex2D(_Main_Map,TRANSFORM_TEX(i.uv0, _Main_Map));
                float node_3522 = max(0,dot(i.normalDir,Light_Position.rgb));
                float2 node_16 = float2(node_3522,node_3522);
                float4 _Falloff_Map_var = tex2D(_Falloff_Map,TRANSFORM_TEX(node_16, _Falloff_Map));
                float3 node_1339 = (_Falloff_Map_var.rgb*_Light_Color.rgb);
                float3 node_8730 = (-1*(i.normalDir+Light_Position.rgb));
                float3 emissive = saturate((((_Main_Color.rgb*((_Main_Map_var.rgb*node_1339*(0.2+_Light_Strengh))+(_Shadow_Color.rgb*(1.0 - node_1339)*_Main_Map_var.rgb)+(_Side_Color_Onoff*pow(max(0,dot(node_8730,viewDirection)),_Side_Color_Strengh)*100.0)))+_Emission_Color.rgb)*GameAmbientLight.rgb));
                float3 finalColor = min(emissive + _Add_Color, 1);
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
