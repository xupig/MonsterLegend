// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/Shader Forge/sf_RGBA_tint_add" {
    Properties {
        _tex_R ("tex_R", 2D) = "white" {}
        _tex_G ("tex_G", 2D) = "white" {}
        _tex_B ("tex_B", 2D) = "white" {}
        _tex_A ("tex_A", 2D) = "white" {}
        _Main_Color ("Main_Color", Color) = (1,1,1,1)
        _Strengh ("Strengh", Float ) = 1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform sampler2D _tex_R; uniform float4 _tex_R_ST;
            uniform float4 _Main_Color;
            uniform sampler2D _tex_G; uniform float4 _tex_G_ST;
            uniform sampler2D _tex_B; uniform float4 _tex_B_ST;
            uniform sampler2D _tex_A; uniform float4 _tex_A_ST;
            uniform float _Strengh;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 _tex_R_var = tex2D(_tex_R,TRANSFORM_TEX(i.uv0, _tex_R));
                float4 _tex_G_var = tex2D(_tex_G,TRANSFORM_TEX(i.uv0, _tex_G));
                float4 _tex_B_var = tex2D(_tex_B,TRANSFORM_TEX(i.uv0, _tex_B));
                float4 _tex_A_var = tex2D(_tex_A,TRANSFORM_TEX(i.uv0, _tex_A));
                float3 emissive = ((_Main_Color.rgb*i.vertexColor.rgb*_Strengh*i.vertexColor.a*_Main_Color.a)*_tex_R_var.r*_tex_G_var.g*_tex_B_var.b*_tex_A_var.a);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
   // CustomEditor "ShaderForgeMaterialInspector"
}
