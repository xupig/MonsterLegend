// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/Shader Forge/sf_aion_refraction" {
    Properties {
        _MainTex ("MainTex", 2D) = "bump" {}
        _TintColor ("Color", Color) = (1,1,1,1)
        _Color_Strengh ("Color_Strengh", Float ) = 2
        _Distortion ("Distortion", Float ) = 1
        _View_Depth ("View_Depth", Float ) = 4
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }

	CGINCLUDE
	#include "UnityCG.cginc"
	uniform sampler2D _GrabTexture;
	uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
	uniform float4 _TintColor;
	uniform float _Distortion;
	uniform float _Color_Strengh;
	uniform float _View_Depth;
	struct VertexInput {
		float4 vertex : POSITION;
		float2 texcoord0 : TEXCOORD0;
		float4 vertexColor : COLOR;
	};
	struct VertexOutput {
		float4 pos : SV_POSITION;
		float2 uv0 : TEXCOORD0;
		float4 posWorld : TEXCOORD1;
		float4 screenPos : TEXCOORD2;
		float4 vertexColor : COLOR;
		UNITY_FOG_COORDS(3)
	};
	VertexOutput vert(VertexInput v) {
		VertexOutput o = (VertexOutput)0;
		o.uv0 = v.texcoord0;
		o.vertexColor = v.vertexColor;
		float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - mul(unity_ObjectToWorld, v.vertex).xyz);
		v.vertex.xyz += (viewDirection*_View_Depth);
		o.posWorld = mul(unity_ObjectToWorld, v.vertex);
		o.pos = UnityObjectToClipPos(v.vertex);
		UNITY_TRANSFER_FOG(o, o.pos);
		o.screenPos = o.pos;
		return o;
	}
	float4 fragFinal(VertexOutput i, bool useGrabTexture) {
	#if UNITY_UV_STARTS_AT_TOP
		float grabSign = -_ProjectionParams.x;
	#else
		float grabSign = _ProjectionParams.x;
	#endif
		i.screenPos = float4(i.screenPos.xy / i.screenPos.w, 0, 0);
		i.screenPos.y *= _ProjectionParams.x;
		float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
		float3 _MainTex_var = UnpackNormal(tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex)));
		float node_106 = (1.0 - _MainTex_var.b);
	
		////// Lighting:
		////// Emissive:
		float3 emissive = (i.vertexColor.rgb*_TintColor.rgb*_Color_Strengh);
		float3 finalColor = emissive;
		fixed4 finalRGBA;
		if (useGrabTexture) {
			float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + (float2(_MainTex_var.r,_MainTex_var.g)*_Distortion*node_106*i.vertexColor.a*_TintColor.a);
			float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
			finalRGBA.rgb = lerp(sceneColor.rgb, finalColor, (i.vertexColor.a*node_106*node_106));
			finalRGBA.a = saturate((abs(_MainTex_var.r + _MainTex_var.g) - 0.01) * 25);
		}
		else {
			finalRGBA = fixed4(finalColor, i.vertexColor.a*node_106*node_106);
		}	
		UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
		return finalRGBA;
	}
	ENDCG

    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent+10"
            "RenderType"="Transparent"
        }
		LOD 1000
		GrabPass{ "_GrabTexture" }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE          
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma target 3.0    
			float4 frag(VertexOutput i) : SV_Target{
				return fragFinal(i, true);
			}
            ENDCG
        }
    }

    /*
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent+10"
            "RenderType"="Transparent"
        }
		LOD 300
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE          
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma target 3.0    
			float4 frag(VertexOutput i) : SV_Target{
				return fragFinal(i, false);
			}
            ENDCG
        }
    }
	*/
}
