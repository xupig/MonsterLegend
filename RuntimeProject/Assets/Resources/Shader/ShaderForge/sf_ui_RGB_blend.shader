// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/Shader Forge/sf_ui_RGB_blend" {
    Properties {
        _Main_Map ("Main_Map", 2D) = "white" {}
        _Main_Color ("Main_Color", Color) = (1,1,1,1)
        _Strengh ("Strengh", Float ) = 1
        [MaterialToggle] _RGB_Channel ("RGB_Channel", Float ) = 0
        [MaterialToggle] _R_Channel ("R_Channel", Float ) = 0
        [MaterialToggle] _G_Channel ("G_Channel", Float ) = 0
        [MaterialToggle] _B_Channel ("B_Channel", Float ) = 0
        [MaterialToggle] _A_Channel ("A_Channel", Float ) = 0
        //_Soft ("Soft", Range(0, 3)) = 0
        //[MaterialToggle] _Soft_OnOffDX11orDefferedMode ("Soft_On/Off(DX11 or Deffered Mode)", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Overlay+1"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            //#pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            //#pragma target 2.0
            //uniform sampler2D _CameraDepthTexture;
            uniform sampler2D _Main_Map; uniform float4 _Main_Map_ST;
            uniform float4 _Main_Color;
            uniform float _Strengh;
            uniform float _Soft;
            uniform fixed _R_Channel;
            uniform fixed _G_Channel;
            uniform fixed _B_Channel;
            uniform fixed _A_Channel;
            uniform fixed _Soft_OnOffDX11orDefferedMode;
            uniform fixed _RGB_Channel;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
                float4 projPos : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex );
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                //float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                //float partZ = max(0,i.projPos.z - _ProjectionParams.g);
////// Lighting:
////// Emissive:
               
                float4 _Main_Map_var = tex2D(_Main_Map,TRANSFORM_TEX(i.uv0, _Main_Map));
				//float node_2891 = 0.0;
                //float node_9673 = (lerp( node_2891, _Main_Map_var.r, _R_Channel )+lerp( node_2891, _Main_Map_var.g, _G_Channel )+lerp( node_2891, _Main_Map_var.b, _B_Channel )+lerp( node_2891, _Main_Map_var.a, _A_Channel ));
				float node_9673 = _Main_Map_var.r*_R_Channel + _Main_Map_var.g*_G_Channel + _Main_Map_var.b*_B_Channel + _Main_Map_var.a*_A_Channel; 
                float3 emissive = (_Main_Color.rgb*i.vertexColor.rgb*_Strengh*lerp( node_9673, _Main_Map_var.rgb, _RGB_Channel ));
                float3 finalColor = emissive;
				float alpha = saturate(_Main_Color.a/*lerp( 1.0, saturate((sceneZ-partZ)/_Soft), _Soft_OnOffDX11orDefferedMode )*/*i.vertexColor.a*(node_9673*5.0 + -1.0));
                return fixed4(finalColor, alpha);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    //CustomEditor "ShaderForgeMaterialInspector"
}
