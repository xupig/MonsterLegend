Shader "XGame/Shader Forge/sf_character_p4_fire" {
    Properties {
        _MainTex ("Main_Map", 2D) = "white" {}
        _Dissolve_Map ("Dissolve_Map", 2D) = "white" {}
        _Dissolve ("Dissolve", Range(0, 1)) = 0
        [MaterialToggle] _Dissolve_UV2 ("Dissolve_UV2", Float ) = 0
        _Emission_Color ("Emission_Color", Color) = (1,0.8896551,0,1)
        _Speed ("Speed", Float ) = 1
        _fire_Map ("fire_Map", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float _Dissolve;
            uniform sampler2D _Dissolve_Map; uniform float4 _Dissolve_Map_ST;
            uniform fixed _Dissolve_UV2;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Emission_Color;
            uniform float _Speed;
            uniform sampler2D _fire_Map; uniform float4 _fire_Map_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
#if !defined(SHADER_API_GLES)
                float2 _Dissolve_UV2_var = lerp( o.uv0, o.uv1, _Dissolve_UV2 );
                float4 node_9551 = _Time;
                float node_4210 = 1.0;
                float node_6439 = 0.0;
                float2 node_1241 = (_Dissolve_UV2_var+(node_9551.r*float2(node_4210,node_6439)*_Speed));
                float4 node_6895 = tex2Dlod(_fire_Map,float4(TRANSFORM_TEX(node_1241, _fire_Map),0.0,0));
                float2 node_406 = (_Dissolve_UV2_var+(node_9551.r*float2(node_6439,node_4210)*_Speed));
                float4 node_3066 = tex2Dlod(_fire_Map,float4(TRANSFORM_TEX(node_406, _fire_Map),0.0,0));
                float2 node_8455 = (_Dissolve_UV2_var+(node_9551.r*float2(2.0,(-1.0))*_Speed));
                float4 node_3269 = tex2Dlod(_fire_Map,float4(TRANSFORM_TEX(node_8455, _fire_Map),0.0,0));
                float node_7926 = clamp(((node_6895.r*node_3066.g*node_3269.b*lerp(0.0,2.0,_Emission_Color.a))*2.0+-1.0),0,8);
                float4 _Dissolve_Map_var = tex2Dlod(_Dissolve_Map,float4(TRANSFORM_TEX(_Dissolve_UV2_var, _Dissolve_Map),0.0,0));
                float node_8987 = step(_Dissolve,_Dissolve_Map_var.r);
                v.vertex.xyz += (0.02*node_7926*v.normal*node_8987);
#endif
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float2 _Dissolve_UV2_var = lerp( i.uv0, i.uv1, _Dissolve_UV2 );
                float4 node_9551 = _Time;
                float node_4210 = 1.0;
                float node_6439 = 0.0;
                float2 node_1241 = (_Dissolve_UV2_var+(node_9551.r*float2(node_4210,node_6439)*_Speed));
                float4 node_6895 = tex2D(_fire_Map,TRANSFORM_TEX(node_1241, _fire_Map));
                float2 node_406 = (_Dissolve_UV2_var+(node_9551.r*float2(node_6439,node_4210)*_Speed));
                float4 node_3066 = tex2D(_fire_Map,TRANSFORM_TEX(node_406, _fire_Map));
                float2 node_8455 = (_Dissolve_UV2_var+(node_9551.r*float2(2.0,(-1.0))*_Speed));
                float4 node_3269 = tex2D(_fire_Map,TRANSFORM_TEX(node_8455, _fire_Map));
                float node_7926 = clamp(((node_6895.r*node_3066.g*node_3269.b*lerp(0.0,2.0,_Emission_Color.a))*2.0+-1.0),0,8);
                float4 _Dissolve_Map_var = tex2D(_Dissolve_Map,TRANSFORM_TEX(_Dissolve_UV2_var, _Dissolve_Map));
                float node_8987 = step(_Dissolve,_Dissolve_Map_var.r);
                float3 emissive = lerp(_MainTex_var.rgb,((((_MainTex_var.r*_MainTex_var.g*_MainTex_var.b)+node_7926)*_Emission_Color.rgb)+saturate(3.0*abs(1.0-2.0*frac(_Emission_Color.r+float3(0.0,-1.0/3.0,1.0/3.0)))-1)),node_8987);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_fog
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float _Dissolve;
            uniform sampler2D _Dissolve_Map; uniform float4 _Dissolve_Map_ST;
            uniform fixed _Dissolve_UV2;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _Emission_Color;
            uniform float _Speed;
            uniform sampler2D _fire_Map; uniform float4 _fire_Map_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
#if !defined(SHADER_API_GLES)
                float2 _Dissolve_UV2_var = lerp( o.uv0, o.uv1, _Dissolve_UV2 );
                float4 node_9551 = _Time;
                float node_4210 = 1.0;
                float node_6439 = 0.0;
                float2 node_1241 = (_Dissolve_UV2_var+(node_9551.r*float2(node_4210,node_6439)*_Speed));
                float4 node_6895 = tex2Dlod(_fire_Map,float4(TRANSFORM_TEX(node_1241, _fire_Map),0.0,0));
                float2 node_406 = (_Dissolve_UV2_var+(node_9551.r*float2(node_6439,node_4210)*_Speed));
                float4 node_3066 = tex2Dlod(_fire_Map,float4(TRANSFORM_TEX(node_406, _fire_Map),0.0,0));
                float2 node_8455 = (_Dissolve_UV2_var+(node_9551.r*float2(2.0,(-1.0))*_Speed));
                float4 node_3269 = tex2Dlod(_fire_Map,float4(TRANSFORM_TEX(node_8455, _fire_Map),0.0,0));
                float node_7926 = clamp(((node_6895.r*node_3066.g*node_3269.b*lerp(0.0,2.0,_Emission_Color.a))*2.0+-1.0),0,8);
                float4 _Dissolve_Map_var = tex2Dlod(_Dissolve_Map,float4(TRANSFORM_TEX(_Dissolve_UV2_var, _Dissolve_Map),0.0,0));
                float node_8987 = step(_Dissolve,_Dissolve_Map_var.r);
                v.vertex.xyz += (0.02*node_7926*v.normal*node_8987);
#endif
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : SV_Target {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float2 _Dissolve_UV2_var = lerp( i.uv0, i.uv1, _Dissolve_UV2 );
                float4 node_9551 = _Time;
                float node_4210 = 1.0;
                float node_6439 = 0.0;
                float2 node_1241 = (_Dissolve_UV2_var+(node_9551.r*float2(node_4210,node_6439)*_Speed));
                float4 node_6895 = tex2D(_fire_Map,TRANSFORM_TEX(node_1241, _fire_Map));
                float2 node_406 = (_Dissolve_UV2_var+(node_9551.r*float2(node_6439,node_4210)*_Speed));
                float4 node_3066 = tex2D(_fire_Map,TRANSFORM_TEX(node_406, _fire_Map));
                float2 node_8455 = (_Dissolve_UV2_var+(node_9551.r*float2(2.0,(-1.0))*_Speed));
                float4 node_3269 = tex2D(_fire_Map,TRANSFORM_TEX(node_8455, _fire_Map));
                float node_7926 = clamp(((node_6895.r*node_3066.g*node_3269.b*lerp(0.0,2.0,_Emission_Color.a))*2.0+-1.0),0,8);
                float4 _Dissolve_Map_var = tex2D(_Dissolve_Map,TRANSFORM_TEX(_Dissolve_UV2_var, _Dissolve_Map));
                float node_8987 = step(_Dissolve,_Dissolve_Map_var.r);
                o.Emission = lerp(_MainTex_var.rgb,((((_MainTex_var.r*_MainTex_var.g*_MainTex_var.b)+node_7926)*_Emission_Color.rgb)+saturate(3.0*abs(1.0-2.0*frac(_Emission_Color.r+float3(0.0,-1.0/3.0,1.0/3.0)))-1)),node_8987);
                
                float3 diffColor = float3(0,0,0);
                o.Albedo = diffColor;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    //CustomEditor "ShaderForgeMaterialInspector"
}
