Shader "XGame/Shader Forge/sf_aion_RGBA_add_billboard" {
    Properties {
        _Main_Map ("Main_Map", 2D) = "white" {}
        _Main_Color ("Main_Color", Color) = (1,1,1,1)
        _Strengh ("Strengh", Float ) = 1
        [MaterialToggle] _RGB_Channel ("RGB_Channel", Float ) = 0
        [MaterialToggle] _R_Channel ("R_Channel", Float ) = 0
        [MaterialToggle] _G_Channel ("G_Channel", Float ) = 0
        [MaterialToggle] _B_Channel ("B_Channel", Float ) = 0
        [MaterialToggle] _A_Channel ("A_Channel", Float ) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            //#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform sampler2D _Main_Map; uniform float4 _Main_Map_ST;
            uniform float4 _Main_Color;
            uniform float _Strengh;
            uniform fixed _R_Channel;
            uniform fixed _G_Channel;
            uniform fixed _B_Channel;
            uniform fixed _A_Channel;
            uniform fixed _RGB_Channel;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                float4x4 bbmv = UNITY_MATRIX_MV;
                bbmv._m00 = -1.0/length(unity_WorldToObject[0].xyz);
                bbmv._m10 = 0.0f;
                bbmv._m20 = 0.0f;
                bbmv._m01 = 0.0f;
                bbmv._m11 = -1.0/length(unity_WorldToObject[1].xyz);
                bbmv._m21 = 0.0f;
                bbmv._m02 = 0.0f;
                bbmv._m12 = 0.0f;
                bbmv._m22 = -1.0/length(unity_WorldToObject[2].xyz);
                o.pos = mul( UNITY_MATRIX_P, mul( bbmv, v.vertex ));
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float node_4226 = 0.0;
                float4 _Main_Map_var = tex2D(_Main_Map,TRANSFORM_TEX(i.uv0, _Main_Map));
                float3 emissive = ((_Main_Color.rgb*i.vertexColor.rgb*_Strengh*i.vertexColor.a*_Main_Color.a)*lerp( (lerp( node_4226, _Main_Map_var.r, _R_Channel )+lerp( node_4226, _Main_Map_var.g, _G_Channel )+lerp( node_4226, _Main_Map_var.b, _B_Channel )+lerp( node_4226, _Main_Map_var.a, _A_Channel )), _Main_Map_var.rgb, _RGB_Channel ));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG_COLOR(i.fogCoord, finalRGBA, fixed4(0,0,0,1));
                return finalRGBA;
            }
            ENDCG
        }
    }
    //CustomEditor "ShaderForgeMaterialInspector"
}
