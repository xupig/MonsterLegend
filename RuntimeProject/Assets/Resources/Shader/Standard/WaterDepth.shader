// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "XGame/Aion/WaterDepth" {
	Properties {
		_WaterTex ("Normal Map (RGB)", 2D) = "white" {}
		_Cube ("Skybox", Cube) = "_Skybox" {}
		_Color0 ("Shallow Color", Color) = (1,1,1,1)
		_Color1 ("Deep Color", Color) = (0,0,0,0)
		//_Specular ("Specular", Color) = (0,0,0,0)
		_WaterColor("Water Color",Color)=(0,0,1,0)
		_Tiling ("Tiling", Range(0.0025, 0.25)) = 0.25
		_OffsetSpeed("Offset Speed",float)=0.5
		//_AlphaPower("Alpha Power",Range(0.0,1.0))=0.2
		//_FullPower("Full Power",Range(0.0,0.4))=0.1
		_ReflectionTint ("Reflection Tint", Range(0.0, 4.0)) = 1.8
		_WaterDepth("Water Depth",Float)=1.0
	}
	
	CGINCLUDE
	#pragma target 3.0
	#include "UnityCG.cginc"
	//#include "UnityPBSLighting.cginc"
	half4 _Color0;
	half4 _Color1;
	half4 _WaterColor;
	//half4 _Specular;
	//float _Shininess;
	float _Tiling;
	float _ReflectionTint;
	//half4 _InvRanges;
	float _OffsetSpeed;
	//float _AlphaPower;
	//float _FullPower;
	float _WaterDepth;
	sampler2D_float _CameraDepthTexture;
	sampler2D _WaterTex;
	float4 _WaterTex_ST;
	samplerCUBE _Cube;
	ENDCG

		
	SubShader {
		Lod 200
		Tags {"RenderType" = "Transparent" "Queue" = "Transparent-10" }// 
		Blend  SrcAlpha  OneMinusSrcAlpha
		ZWrite Off

		Pass {
			CGPROGRAM
			#pragma multi_compile SHADER_QUALITY_HIGH SHADER_QUALITY_MEDIUM SHADER_QUALITY_LOW
			#pragma vertex vert
			#pragma fragment frag  
			#pragma multi_compile_fog 

			struct v2f {
				float4 pos : SV_POSITION;
				float4 uv : TEXCOORD0;
				float3 worldPos : TEXCOORD1;
				float4 proj0 : TEXCOORD2;
				UNITY_FOG_COORDS(3)
			};

			v2f vert(appdata_base v) {
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				float offset = _Time.x * 0.5 * _OffsetSpeed;
				half2 tiling = o.worldPos.xz * _Tiling;
				o.uv.xy = TRANSFORM_TEX(half2(tiling + offset), _WaterTex);
				o.uv.zw = TRANSFORM_TEX(half2(half2(-tiling.y, tiling.x) - offset), _WaterTex);
				o.proj0 = ComputeScreenPos(o.pos);
				COMPUTE_EYEDEPTH(o.proj0.z);
				UNITY_TRANSFER_FOG(o, o.pos);

				return o;
			}

			half4 frag(v2f i) : SV_Target {
				// (Z-up)
				float3 offsetColor1 = UnpackNormal(tex2D(_WaterTex, i.uv.xy));
				float3 offsetColor2 = UnpackNormal(tex2D(_WaterTex, i.uv.zw));
				float3 Normal = (offsetColor1 + offsetColor2) * 0.5;

				//  (Y-up)
				half3 worldNormal = Normal.xzy;
				worldNormal.z = -worldNormal.z;

#if SHADER_QUALITY_HIGH
				float depth = tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.proj0)).r;
				depth = LinearEyeDepth(depth);
				float waterdepth = depth - i.proj0.z;
				half alpha = max(waterdepth / _WaterDepth, 0);
				half edge = min(alpha, 1);
#else				
				half alpha = 4;
				half edge = 1;
#endif

				//float3 worldView = UnityWorldSpaceViewDir(i.worldPos);
				float3 worldView = (i.worldPos - _WorldSpaceCameraPos);

				half4 col;
				col.rgb = lerp(_Color1.rgb, _Color0.rgb, edge);
				half fresnel = sqrt(1.0 - dot(-normalize(worldView), worldNormal));
				fresnel = (0.8 * fresnel + 0.2);

				half3 reflection = texCUBE(_Cube, reflect(worldView, worldNormal)).rgb;
				float lum = reflection.r*0.299 + reflection.g*0.587 + reflection * 0.114;
				reflection = lum* _ReflectionTint*_WaterColor;
				half3 refraction = lerp(col.rgb, col.rgb * col.rgb, edge);
				
				half4 c;
				c.rgb = lerp(refraction, reflection, fresnel);
				c.a = min(alpha * 0.2, 1);

				UNITY_APPLY_FOG(i.fogCoord, c);
				return c;
			}
			ENDCG
		}
	}
	
	Fallback Off
}
