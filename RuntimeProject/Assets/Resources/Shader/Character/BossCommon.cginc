#ifndef __BOSS_COMMON__
#define __BOSS_COMMON__

#include "../Mogo2Include.cginc"
			
uniform sampler2D 	_MainTex;
uniform half4 		_MainTex_ST;
#if defined(__ALPHATEST_ON)
uniform half		_Cutoff;
#endif
uniform sampler2D	_BumpMap;
uniform sampler2D 	_BRDFTex;
uniform half 		_SelfShadingLightDirX;
uniform half 		_SelfShadingLightDirY;
uniform half 		_SelfShadingLightDirZ;
uniform sampler2D   _Region;
uniform fixed		_SpecIntensity;
uniform fixed3 		_RimColor;
uniform fixed       _RimStrength;
uniform fixed3 		_RimPower;
uniform fixed		_HighLight;
#if defined(_FLOW_FLASH_ON) || defined(_FLOW_FLASH2_ON)
uniform fixed3      _EmissiveColor1;
uniform sampler2D 	_FlowTex;
uniform fixed3		_FlowColor;
uniform half		_FlowSpeedX;
uniform half		_FlowSpeedY;
uniform samplerCUBE _FlashCubeTex;
uniform fixed3      _FlashColor;
uniform half        _FlashStrength;
uniform float4x4	_FlashNormalRotM;
#if defined(_FLOW_FLASH2_ON)
uniform fixed3      _FlashColor2;
uniform half        _FlashStrength2;			
uniform float4x4	_FlashNormalRotM2;
#endif
#endif
uniform fixed4		_AmbientColor;
uniform fixed3      _VestColor;

struct v2f {
	half4 pos : POSITION;
	half2 uv : TEXCOORD0;
	half4 tSpace0 : TEXCOORD1;
	half4 tSpace1 : TEXCOORD2;
	half4 tSpace2 : TEXCOORD3;
	UNITY_FOG_COORDS(4)
#if defined(_FLOW_FLASH_ON) || defined(_FLOW_FLASH2_ON)
	half2 uvFlow : TEXCOORD5;
	half3 uvFlash : TEXCOORD6;
#if defined(_FLOW_FLASH2_ON)
	half3 uvFlash2 : TEXCOORD7;
#endif
#endif
};
			

v2f vert(appdata_full v) {
	v2f o = (v2f)0;
	o.pos = UnityObjectToClipPos(v.vertex);
	o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
	half3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
	half3 worldNormal = UnityObjectToWorldNormal(v.normal);
	half3 worldTangent = UnityObjectToWorldDir(v.tangent.xyz);
	half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
	half3 worldBinormal = cross(worldNormal, worldTangent) * tangentSign;
	o.tSpace0 = half4(worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x);
	o.tSpace1 = half4(worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y);
	o.tSpace2 = half4(worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z);
	UNITY_TRANSFER_FOG(o, o.pos);

#if defined(_FLOW_FLASH_ON) || defined(_FLOW_FLASH2_ON)
	o.uvFlow = worldNormal.xy + _Time.xx * half2(_FlowSpeedX, _FlowSpeedY);
	o.uvFlash = mul(_FlashNormalRotM, half4(worldNormal, 0)).xyz;
#if defined(_FLOW_FLASH2_ON)
	o.uvFlash2 = mul(_FlashNormalRotM2, half4(worldNormal, 0)).xyz;
#endif
#endif

	return o;
}
			
fixed4 frag(v2f i) : SV_Target {
	fixed4 clr = tex2D(_MainTex, i.uv);
#if defined(__ALPHATEST_ON)
	clip(clr.a - _Cutoff);
#endif

	half3 worldPos = half3(i.tSpace0.w, i.tSpace1.w, i.tSpace2.w);
	half3 lightDir = normalize(half3(_SelfShadingLightDirX, _SelfShadingLightDirY, _SelfShadingLightDirZ));
	half3 worldViewDir = normalize(UnityWorldSpaceViewDir(worldPos));
	half3 normal = UnpackNormal(tex2D(_BumpMap, i.uv));
	half3 worldNormal;
	worldNormal.x = dot(i.tSpace0.xyz, normal);
	worldNormal.y = dot(i.tSpace1.xyz, normal);
	worldNormal.z = dot(i.tSpace2.xyz, normal);

	fixed4 rg = tex2D(_Region, i.uv);
	half nl = saturate(dot(worldNormal, lightDir)) * 0.5 + 0.5;
	half nh = saturate(dot(worldNormal, normalize(lightDir + worldViewDir)));
	fixed4 brdf = tex2D(_BRDFTex, half2(nl, nh));
#if defined(_FLOW_FLASH_ON) || defined(_FLOW_FLASH2_ON)
	fixed4 fl1 = tex2D(_FlowTex, i.uvFlow);
#endif

	fixed vdn = saturate(dot(worldViewDir, worldNormal));
	fixed3 facing = (1.0 - vdn);
	facing.gb *= facing.gb;
	facing.b *= facing.b;
	fixed rim = dot(facing, _RimPower) * nl;
	fixed3 rc = Overlay(clr.rgb, _RimColor * _RimStrength);

	clr.rgb *= (brdf.rgb + _AmbientColor.rgb + brdf.a * rg.r * _SpecIntensity);
				
#if defined(_FLOW_FLASH_ON) || defined(_FLOW_FLASH2_ON)
	clr.rgb += rg.b * (fl1.rgb * _FlowColor
		+ texCUBE(_FlashCubeTex, i.uvFlash) * _FlashColor * _FlashStrength
#if defined(_FLOW_FLASH2_ON)
		+texCUBE(_FlashCubeTex, i.uvFlash2) * _FlashColor2 * _FlashStrength2
#endif
		);
#endif
	clr.rgb = saturate(clr.rgb);
	clr.rgb *= _HighLight * _VestColor;
	clr.rgb	= lerp(clr.rgb, rc, rim);
	UNITY_APPLY_FOG(i.fogCoord, clr);
#if !defined(__ALPHABLEND_ON) && !defined(__ALPHATEST_ON)
	clr.a = 1;
#endif

	return clr;
}


#endif