#ifndef __MONSTER_COMMON__
#define __MONSTER_COMMON__


#include "../Mogo2Include.cginc"
			
uniform sampler2D 	_MainTex;
uniform half4 		_MainTex_ST;
#if defined(__ALPHATEST_ON)
uniform half		_Cutoff;
#endif
uniform sampler2D 	_BRDFTex;
uniform half 		_SelfShadingLightDirX;
uniform half 		_SelfShadingLightDirY;
uniform half 		_SelfShadingLightDirZ;
uniform half		_Shininess;
uniform fixed		_SpecIntensity;
uniform fixed3 		_RimColor;
uniform fixed       _RimStrength;
uniform fixed3 		_RimPower;
uniform fixed		_HighLight;
#ifdef EMISSIVE_FLOW_ON
uniform sampler2D   _Region;
uniform fixed3      _EmissiveColor;
uniform sampler2D 	_FlowTex;
uniform float		_FlowDirX;
uniform float		_FlowDirY;
#endif
uniform fixed4		_AmbientColor;
uniform fixed3      _VestColor;

struct v2f {
	half4 pos : POSITION;
	half2 uv : TEXCOORD0;
	UNITY_FOG_COORDS(1)
	half2 brdf : TEXCOORD2;
	half3 view : TEXCOORD3;
	half4 normal : TEXCOORD4;
	float2 uvFlow : TEXCOORD6;
};
			
v2f vert(appdata_full v) {
	v2f o = (v2f)0;

	o.pos = UnityObjectToClipPos(v.vertex);
	o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);

	half3 wp = mul(unity_ObjectToWorld, v.vertex).xyz;
	o.normal.xyz = normalize(mul((half3x3)unity_ObjectToWorld, SCALED_NORMAL));
	o.view = normalize(_WorldSpaceCameraPos.xyz - wp);
	half3 ld = normalize(half3(_SelfShadingLightDirX, _SelfShadingLightDirY, _SelfShadingLightDirZ));
	half3 h = normalize(ld + o.view);
	half nh = saturate(dot(o.normal.xyz, h));
	o.brdf = half2(saturate(dot(o.normal.xyz, ld)) * 0.5 + 0.5, nh);
	o.normal.w = pow(nh, _Shininess * 128) * _SpecIntensity;
		
#ifdef EMISSIVE_FLOW_ON
	o.uvFlow = o.uv + _Time.y * float2(_FlowDirX, _FlowDirY);
#endif

	UNITY_TRANSFER_FOG(o,o.pos);
	return o;
}
			
fixed4 frag(v2f i) : SV_Target {
	fixed4 clr = tex2D(_MainTex, i.uv);
#if defined(__ALPHATEST_ON)
	clip(clr.a - _Cutoff);
#endif

	fixed3 brdf = tex2D(_BRDFTex, i.brdf).rgb;
#ifdef EMISSIVE_FLOW_ON
	fixed4 rg = tex2D(_Region, i.uv);
	fixed4 fl = tex2D(_FlowTex, i.uvFlow);
#endif

	fixed vdn = saturate(dot(i.view, i.normal.xyz));
	fixed3 facing = (1.0 - vdn);
	facing.gb *= facing.gb;
	facing.b *= facing.b;
	fixed rim = dot(facing, _RimPower);
				
	clr.rgb *= i.normal.w + brdf + _AmbientColor.rgb;

#ifdef EMISSIVE_FLOW_ON
	clr.rgb += rg.b * fl * _EmissiveColor;
#endif
	clr.rgb	= lerp(clr.rgb, _RimColor, saturate(rim * _RimStrength));
	clr.rgb *= _HighLight * _VestColor;

	UNITY_APPLY_FOG(i.fogCoord, clr);

#if !defined(__ALPHABLEND_ON) && !defined(__ALPHATEST_ON)
	clr.a = 1;
#endif

	return clr;
}
		
#endif