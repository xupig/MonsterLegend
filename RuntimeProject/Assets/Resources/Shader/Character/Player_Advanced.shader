// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'


Shader "XGame/MOGO2/Character/Player_Advanced" 
{
	Properties 
	{
		[MaterialEnum(On,0, Off,2)] _Cull("2-Sided", Float) = 2
		_MainTex ("Base(RGB)", 2D) = "grey" {}
		_BumpMap("Normalmap", 2D) = "bump" {}
		_GlossTex("Gloss(R) BRDF(G) Flow(B)", 2D) = "white" {}
		_BRDFTex ("BRDF", 2D) = "grey" {}
		_SelfShadingLightDirX("SelfShadingLight Dir X", Range(-1.0, 1.0)) = 1.0
		_SelfShadingLightDirY("SelfShadingLight Dir Y", Range(-1.0, 1.0)) = 0.5
		_SelfShadingLightDirZ("SelfShadingLight Dir Z", Range(-1.0, 1.0)) = 0.5
		_Shininess("Shininess", Range(0.01, 1.0)) = 0.5
		_SpecIntensity("Specular Intensity", Range(0, 5.0)) = 1.0
		_RimLightPower ("RimPower", Color) = (0.4, 0.0, 1.0, 0.0)
		_RimLightColorStrength ("RimColor", Color) = (1.0, 1.0, 0.0, 1.0)
		_RimLightStrength("RimStrength", Range(0.0, 4.0)) = 1
		_RimLightWarp("RimWarp", Range(-1.0, 1.0)) = -1.0
		[HideInInspector]_HighLight("HighLight", float) = 1.0
		//[MaterialToggle] Emissive_Flow("EmissiveFlow?", float) = 0
		//_EmissiveColor1("EmissiveColor 1", Color) = (1.0, 1.0, 1.0, 1)
		[HideInInspector]_FlowTex("FlowTex(RGB)", 2D) = "white" {}
		[HideInInspector]_FlowColor("FlowColor", Color) = (1, 1, 1, 1)
		[HideInInspector]_FlowSpeedX("Flow Speed X", float) = 0.5
		[HideInInspector]_FlowSpeedY("Flow Speed Y", float) = 0.5
		//_EmissiveColor2("EmissiveColor 2", Color) = (1.0, 1.0, 1.0, 1)
		[HideInInspector]_FlashCubeTex("FlashCubeTex", Cube) = "_SkyBox" {}
		[HideInInspector]_FlashColor("FlashColor", Color) = (1.0, 1.0, 1.0, 1.0)
		[HideInInspector]_FlashStrength("FlashStrength", float) = 1
		[HideInInspector]_FlashColor2("FlashColor2", Color) = (1.0, 1.0, 1.0, 1.0)
		[HideInInspector]_FlashStrength2("FlashStrength2", float) = 1
		//_FlashWhidth("FlashWhidth", float) = 0.1
		//_FlashDirX ("FlashDirX", Range(-1.0, 1.0)) = 0
		//_FlashDirY ("FlashDirY", Range(-1.0, 1.0)) = 0
		//_FlashDirZ ("FlashDirZ", Range(-1.0, 1.0)) = 1
		//_FlashSpeed("FlashSpeed", float) = 2
		//_RepeatDst("RepeatDst", float) = 3
		_AmbientColor("AmbientColor", Color) = (0.212, 0.227, 0.259, 1)
	}

	SubShader 
	{
		LOD 300
		Tags { "RenderType"="Opaque" "Queue"="Geometry+15" }	
				
		Pass
		{
			Cull[_Cull]
	
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			//#pragma multi_compile_fog
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma multi_compile __ _FLOW_FLASH_ON _FLOW_FLASH2_ON
			#include "../Mogo2Include.cginc"
			#include "Lighting.cginc"
			
			uniform sampler2D 	_MainTex;
			uniform half4 		_MainTex_ST;
			uniform sampler2D	_BumpMap;
			uniform sampler2D 	_GlossTex;
			uniform sampler2D 	_BRDFTex;
			uniform half 		_SelfShadingLightDirX;
			uniform half 		_SelfShadingLightDirY;
			uniform half 		_SelfShadingLightDirZ;
			uniform half		_Shininess;
			uniform half		_SpecIntensity;
			uniform half3 		_RimLightPower;
			uniform half3 		_RimLightColorStrength;
			uniform half       _RimLightStrength;
			uniform half 		_RimLightWarp;
			uniform half		_HighLight;
#if defined(_FLOW_FLASH_ON) || defined(_FLOW_FLASH2_ON)
			uniform half3      _EmissiveColor1;
			uniform sampler2D 	_FlowTex;
			uniform fixed3		_FlowColor;
			uniform half		_FlowSpeedX;
			uniform half		_FlowSpeedY;
			uniform samplerCUBE _FlashCubeTex;
			uniform fixed3      _FlashColor;
			uniform half        _FlashStrength;
			uniform float4x4	_FlashNormalRotM;
#if defined(_FLOW_FLASH2_ON)
			uniform fixed3      _FlashColor2;
			uniform half        _FlashStrength2;			
			uniform half4x4		_FlashNormalRotM2;
#endif
#endif
			uniform fixed4		_AmbientColor;

			struct v2f
			{
				half4 pos : POSITION;
				half2 uv : TEXCOORD0;
				half4 tSpace0 : TEXCOORD1;
				half4 tSpace1 : TEXCOORD2;
				half4 tSpace2 : TEXCOORD3;
				fixed4 fogParam : TEXCOORD4;
				/*
#if defined(_FLOW_FLASH_ON) || defined(_FLOW_FLASH2_ON)
				half2 uvFlow : TEXCOORD5;
				half3 uvFlash : TEXCOORD6;
#if defined(_FLOW_FLASH2_ON)
				half3 uvFlash2 : TEXCOORD7;
#endif
#endif
*/
				UNITY_FOG_COORDS(5)
			};

			v2f vert(appdata_full v)
			{
				v2f o = (v2f)0;

				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);

				float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				fixed3 worldNormal = UnityObjectToWorldNormal(v.normal);
				fixed3 worldTangent = UnityObjectToWorldDir(v.tangent.xyz);
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross(worldNormal, worldTangent) * tangentSign;
				o.tSpace0 = float4(worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x);
				o.tSpace1 = float4(worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y);
				o.tSpace2 = float4(worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z);

				UNITY_TRANSFER_FOG(o, o.pos); 

				return o;
			}

			
			
			fixed4 frag(v2f i) : COLOR 
			{
				half3 worldPos = half3(i.tSpace0.w, i.tSpace1.w, i.tSpace2.w);
				//half3 lightDir = normalize(UnityWorldSpaceLightDir(worldPos));
				half3 lightDir = normalize(half3(_SelfShadingLightDirX, _SelfShadingLightDirY, _SelfShadingLightDirZ));
				half3 worldViewDir = normalize(UnityWorldSpaceViewDir(worldPos));
				half3 normal = UnpackNormal(tex2D(_BumpMap, i.uv));
				half3 worldNormal;
				worldNormal.x = dot(i.tSpace0.xyz, normal);
				worldNormal.y = dot(i.tSpace1.xyz, normal);
				worldNormal.z = dot(i.tSpace2.xyz, normal);

				half3 h = normalize(lightDir + worldViewDir);
				half nh = saturate(dot(worldNormal, h));
				half2 uvBrdf = half2(saturate(dot(worldNormal, lightDir)) * 0.5 + 0.5, nh);
				half spec = pow(nh, _Shininess * 128) * _SpecIntensity;

#if defined(_FLOW_FLASH_ON) || defined(_FLOW_FLASH2_ON)
				half2 uvFlow = worldNormal.xy + _Time.xx * half2(_FlowSpeedX, _FlowSpeedY);
				half3 uvFlash = mul(_FlashNormalRotM, half4(worldNormal, 0)).xyz;
#if defined(_FLOW_FLASH2_ON)
				half3 uvFlash2 = mul(_FlashNormalRotM2, half4(worldNormal, 0)).xyz;
#endif
#endif

				fixed4 clr = tex2D(_MainTex, i.uv);
				fixed4 gls = tex2D(_GlossTex, i.uv);
				fixed3 brdf = tex2D(_BRDFTex, uvBrdf).rgb;
#if defined(_FLOW_FLASH_ON) || defined(_FLOW_FLASH2_ON)
				fixed4 fl1 = tex2D(_FlowTex, uvFlow);
#endif

				fixed vdn = saturate(dot(worldViewDir, worldNormal));
				fixed3 facing = (1.0 - vdn);
				facing.gb *= facing.gb;
				facing.b *= facing.b;
				fixed rim = dot(facing, _RimLightPower) * saturate(uvBrdf.x - _RimLightWarp);

				clr.rgb *= (spec * gls.r + brdf * gls.g) + _AmbientColor;

#if defined(_FLOW_FLASH_ON) || defined(_FLOW_FLASH2_ON)
				clr.rgb += gls.b * (fl1.rgb * _FlowColor 

					 + texCUBE(_FlashCubeTex, uvFlash) * _FlashColor * _FlashStrength
#if defined(_FLOW_FLASH2_ON)
					 + texCUBE(_FlashCubeTex, uvFlash2) * _FlashColor2 * _FlashStrength2
#endif
					);
				#endif

				

				fixed3 rc = Overlay(clr.rgb, saturate(_RimLightColorStrength * _RimLightStrength));
				clr.rgb *= _HighLight;
				clr.rgb	= lerp(clr.rgb, rc, rim);
				//clr.rgb = MixFogColor(clr.rgb, i.fogParam);
				UNITY_APPLY_FOG(i.fogCoord, clr); // apply fog
			
				return clr;
			}
			
			ENDCG
		}

	}

	Fallback "Diffuse"

	//CustomEditor "CustomTrilightMaterialInspector"
}
