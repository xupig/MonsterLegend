Shader "XGame/MOGO2/Environment/Reflective/ReflectiveWaterFlow" {
	
Properties {
	_MainTex ("Base", 2D) = "white" {}
	_Normal("Normal", 2D) = "bump" {}
	//_ReflectionTex("_ReflectionTex", 2D) = "black" {}
	_FakeReflect("Fake reflection", 2D) = "black" {}
	_DirectionUv("Wet scroll direction (2 samples)", Vector) = (1.0,1.0, -0.2,-0.2)
	_TexAtlasTiling("Tex atlas tiling", Vector) = (8.0,8.0, 4.0,4.0)	
}

CGINCLUDE
	
#include "../Mogo2Include.cginc"

half4 _DirectionUv;
half4 _TexAtlasTiling;

sampler2D _MainTex;
sampler2D _Normal;		
//sampler2D _ReflectionTex;
sampler2D _FakeReflect;
			
ENDCG 

SubShader {
	Tags { "RenderType"="Opaque" }

	LOD 300 

	Pass {
		CGPROGRAM

		//#pragma multi_compile LIGHTNING_OFF LIGHTNING_ON
		#if defined (LIGHTNING_ON)
			#define ENABLE_LIGHTNING 1
		#endif

		struct v2f_full
		{
			half4 pos : SV_POSITION;
			half2 uv : TEXCOORD0;
			half4 normalScrollUv : TEXCOORD1;	
			half4 screen : TEXCOORD2;
			half2 fakeRefl : TEXCOORD3;
			//fixed4 fogParam : TEXCOORD4;
			UNITY_FOG_COORDS(4)
			#ifdef LIGHTMAP_ON
				half2 uvLM : TEXCOORD5;
			#endif
			#ifdef ENABLE_LIGHTNING
				fixed3 lightning : TEXCOORD6;
			#endif	
		};
		
		float4 _MainTex_ST;
		// float4 unity_LightmapST;	
		// sampler2D unity_Lightmap;
		
		fixed _t = 0.0;
		v2f_full vert (appdata_full v) 
		{
			v2f_full o;
			_t = modf(_Time.x, _t);
			o.pos = UnityObjectToClipPos (v.vertex);
			o.uv.xy = TRANSFORM_TEX(v.texcoord,_MainTex);
			
			#ifdef LIGHTMAP_ON
				o.uvLM = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
			#endif
			
			o.normalScrollUv.xyzw = v.texcoord.xyxy * _TexAtlasTiling + float4(_t,_t,_t,_t) * _DirectionUv;
						
			o.fakeRefl = EthansFakeReflection(v.vertex);
			o.screen = ComputeScreenPos(o.pos);

			//float4 worldP = mul( unity_ObjectToWorld, v.vertex);
			//o.fogParam = ComputeFogParam(o.pos.w, worldP.xyz);

			#if ENABLE_LIGHTNING
				float3 worldN = mul( (float3x3)unity_ObjectToWorld, v.normal * 1.0 );
				o.lightning = Lightning(worldN);
			#endif

			UNITY_TRANSFER_FOG(o, o.pos);
				
			return o; 
		}
				
		fixed4 frag (v2f_full i) : COLOR0 
		{
			half3 nrml = UnpackNormal(tex2D(_Normal, i.normalScrollUv.xy));
			nrml += UnpackNormal(tex2D(_Normal, i.normalScrollUv.zw));
			
			nrml.xy *= 0.025;
										
			//fixed4 rtRefl = tex2D (_ReflectionTex, (i.screen.xy / i.screen.w) + nrml.xy);
			fixed4 rtRefl = tex2D (_FakeReflect, i.fakeRefl + nrml.xy * 2.0);
						
			fixed4 tex = tex2D (_MainTex, i.uv.xy + nrml.xy * 0.05);

			#ifdef LIGHTMAP_ON

				fixed3 lm = ( DecodeLightmap (UNITY_SAMPLE_TEX2D(unity_Lightmap, i.uvLM)));
				
				#if ENABLE_LIGHTNING
					tex.rgb *= lm + i.lightning.rgb * lm;
				#else
					tex.rgb *= lm;
				#endif
				
			#endif	
			
			tex  = tex + tex.a * rtRefl;

			//tex.rgb = MixFogColor(tex.rgb, i.fogParam);
			UNITY_APPLY_FOG(i.fogCoord, tex);
			
			return tex;	
		}	
		
		#pragma vertex vert
		#pragma fragment frag
		#pragma multi_compile_fog 
		#pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
		#pragma fragmentoption ARB_precision_hint_fastest 
	
		ENDCG
	}
} 


SubShader {
	Tags { "RenderType"="Opaque" }

	LOD 200 

	Pass {
		CGPROGRAM

		//#pragma multi_compile LIGHTNING_OFF LIGHTNING_ON
		
		#if defined (LIGHTNING_ON)
			#define ENABLE_LIGHTNING 1
		#endif
		
		float4 _MainTex_ST;
		// float4 unity_LightmapST;	
		// sampler2D unity_Lightmap;

		struct v2f_full
		{
			half4 pos : SV_POSITION;
			half2 uv : TEXCOORD0;
			half4 normalScrollUv : TEXCOORD1;	
			half4 screen : TEXCOORD2;
			half2 fakeRefl : TEXCOORD3;
			//fixed4 fogParam : TEXCOORD4;
			UNITY_FOG_COORDS(4)
			#ifdef LIGHTMAP_ON
				half2 uvLM : TEXCOORD5;
			#endif
			#ifdef ENABLE_LIGHTNING
				fixed3 lightning : TEXCOORD6;
			#endif	
		};
		
		fixed _t = 0.0;
		v2f_full vert (appdata_full v) 
		{
			v2f_full o = (v2f_full)0;
			_t = modf(_Time.x, _t);
			o.pos = UnityObjectToClipPos (v.vertex);
			o.uv = TRANSFORM_TEX(v.texcoord,_MainTex);
			
			#ifdef LIGHTMAP_ON
				o.uvLM = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
			#endif
			
			o.normalScrollUv.xyzw = v.texcoord.xyxy * _TexAtlasTiling + float4(_t,_t,_t,_t) * _DirectionUv;
						
			o.fakeRefl = EthansFakeReflection(v.vertex);
			o.screen = ComputeScreenPos(o.pos);
			UNITY_TRANSFER_FOG(o, o.pos);
				
			return o; 
		}
				
		fixed4 frag (v2f_full i) : COLOR0 
		{
			// assuming this is on mobile, so no texture unpacking needed
			
			fixed4 nrml = tex2D(_Normal, i.normalScrollUv.xy);
			nrml = (nrml - 0.5) * 0.1;
										
			//fixed4 rtRefl = tex2D (_ReflectionTex, (i.screen.xy / i.screen.w) + nrml.xy);
			fixed4 rtRefl = tex2D (_FakeReflect, i.fakeRefl + nrml.xy * 2.0);
			
			// needed optimization for now
			//rtRefl += tex2D (_FakeReflect, i.fakeRefl + nrml.xy);
						
			fixed4 tex = tex2D (_MainTex, i.uv);
		
			#ifdef LIGHTMAP_ON
				fixed3 lm = ( DecodeLightmap (UNITY_SAMPLE_TEX2D(unity_Lightmap, i.uvLM)));
				tex.rgb *= lm;
			#endif	
			
			tex  = tex + tex.a * rtRefl;

			UNITY_APPLY_FOG(i.fogCoord, tex);
			
			return tex;	
		}	
		
		#pragma vertex vert
		#pragma fragment frag
		#pragma multi_compile_fog
		#pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
		#pragma fragmentoption ARB_precision_hint_fastest 
	
		ENDCG
	}
} 

FallBack "Mobile/Diffuse"
}
