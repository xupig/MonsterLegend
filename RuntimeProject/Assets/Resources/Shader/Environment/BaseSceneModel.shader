Shader "XGame/MOGO2/Environment/BaseSceneModel" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
        
	SubShader 
	{
	    Tags { "Queue" = "Geometry+2" "RenderType" = "Opaque"}
	    LOD 200

		Pass 
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
			#include "../Mogo2Include.cginc"

			//#pragma multi_compile LIGHTNING_OFF LIGHTNING_ON
			#if defined (LIGHTNING_ON)
				#define ENABLE_LIGHTNING 1
			#endif

            sampler2D _MainTex;
			float4 _MainTex_ST;
		
			#ifndef LIGHTMAP_OFF
				// float4 unity_LightmapST;
				// sampler2D unity_Lightmap;
			#endif

			struct v2f 
			{
				float4 pos 	: SV_POSITION;
				half2 uv 	: TEXCOORD0;
				UNITY_FOG_COORDS(1)
			#ifndef LIGHTMAP_OFF
				half2 lmap : TEXCOORD2;
			#endif
			#ifdef ENABLE_LIGHTNING
				fixed3 lightning : TEXCOORD3;
			#endif
			};
			 
			v2f vert (appdata_full v) 
			{
			  	v2f o;

				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord * _MainTex_ST.xy + _MainTex_ST.zw;		
				
			#ifndef LIGHTMAP_OFF
				o.lmap = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
			#endif

			#if ENABLE_LIGHTNING
				float3 worldN = mul( (float3x3)unity_ObjectToWorld, v.normal * 1.0 );
				o.lightning = Lightning(worldN);
			#endif

				UNITY_TRANSFER_FOG(o, o.pos);
			  	
			  	return o;
			}
		
			fixed4 frag (v2f IN) : SV_Target 
			{
			  	fixed4 c = tex2D (_MainTex, IN.uv);
			  	
			  	#ifndef LIGHTMAP_OFF
					fixed3 lightInfo = DecodeLightmap (UNITY_SAMPLE_TEX2D(unity_Lightmap, IN.lmap));
				#else
					fixed3 lightInfo = 1;
				#endif
				    
		    	// combine lightmaps with realtime shadows
			    #ifdef SHADOWS_SCREEN
			      	lightInfo = min(lightInfo, LIGHT_ATTENUATION(IN) * 2);
			    #endif // SHADOWS_SCREEN
			  	
				// #if ENABLE_LIGHTNING
				// 	c.rgb *= ( 1 + IN.lightning.rgb ) * lightInfo;
				// #else
				// 	c.rgb *= lightInfo;
				// #endif

				UNITY_APPLY_FOG(IN.fogCoord, c);
			  	
			  	return c;
			}

			ENDCG

		}
    }

    Fallback "Mobile/Diffuse"
}