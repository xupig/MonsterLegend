﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "XGame/MOGO2/PostProcess/GaussBlur7"
{
	Properties
	{
		_MainTex ("MainTex", 2D) = "white" {}
	}

	SubShader
	{
		Pass
		{
			Cull Off
			ZTest Always
			ZWrite Off

			CGPROGRAM
			
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma vertex vert
			#pragma fragment frag
			
			#include "../Mogo2Include.cginc"

			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			sampler2D _MainTex;
			float2 _Offset;

			v2f vert(appdata_full v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord;
				return o;
			}

			half4 frag(v2f i) : COLOR
			{
				half4 c = tex2D(_MainTex, i.uv);
				c.rgb *= 0.235;
				c.rgb += tex2D(_MainTex, i.uv + _Offset).rgb * 0.1895;
				c.rgb += tex2D(_MainTex, i.uv - _Offset).rgb * 0.1895;
				c.rgb += tex2D(_MainTex, i.uv + _Offset * 2).rgb * 0.1205;
				c.rgb += tex2D(_MainTex, i.uv - _Offset * 2).rgb * 0.1205;
				c.rgb += tex2D(_MainTex, i.uv + _Offset * 3).rgb * 0.0725;
				c.rgb += tex2D(_MainTex, i.uv - _Offset * 3).rgb * 0.0725;
				return c;
			}

			ENDCG
		}
	}

	FallBack off
}
