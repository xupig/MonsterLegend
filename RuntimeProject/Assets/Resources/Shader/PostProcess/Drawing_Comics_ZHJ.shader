// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

////////////////////////////////////////////
// CameraFilterPack - by VETASOFT 2018 /////
////////////////////////////////////////////

Shader "CameraFilterPack/Drawing_Comics_ZHJ" 
{
    Properties 
    {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _TimeX ("Time", Range(0.0, 1.0)) = 1.0
        //_Distortion ("_Distortion", Range(0.0, 1.0)) = 0.3
        //_ScreenResolution ("_ScreenResolution", Vector) = (0.,0.,0.,0.)

        _DotSize ("_DotSize", Range(0.0, 1.0)) = 0
        _Color ("Tint", Color) = (1,0,0,1)
        //Hue的值范围为0-359. 其他两个为0-1 ,这里我们设置到3，因为乘以3后 都不一定能到超过.
        _Hue ("Hue", Range(0,359)) = 0
        _Saturation ("Saturation", Range(0,3.0)) = 1.0
        _Value ("Value", Range(0,3.0)) = 1.0
    }
    SubShader 
    {
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

        Pass
        {
            Cull Off 
            ZWrite Off 
            ZTest [unity_GUIZTestMode]
            //Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma fragmentoption ARB_precision_hint_fastest
            #include "UnityCG.cginc"


            uniform sampler2D _MainTex;
            uniform float _TimeX;
            //uniform float _Distortion;
            //uniform float4 _ScreenResolution;
            uniform float _DotSize;
            uniform fixed4 _Color;
            uniform half _Hue;
            uniform half _Saturation;
            uniform half _Value;
            
            //uniform float _ColorLevel;

            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float2 texcoord  : TEXCOORD0;
                float4 vertex   : SV_POSITION;
                float4 color    : COLOR;
            };   

            //RGB to HSV
            float3 RGBConvertToHSV(float3 rgb)
            {
                float R = rgb.x,G = rgb.y,B = rgb.z;
                float3 hsv;
                float max1=max(R,max(G,B));
                float min1=min(R,min(G,B));
                if (R == max1) 
                {
                    hsv.x = (G-B)/(max1-min1);
                }
                if (G == max1) 
                {
                    hsv.x = 2 + (B-R)/(max1-min1);
                    }
                if (B == max1) 
                {
                    hsv.x = 4 + (R-G)/(max1-min1);
                    }
                hsv.x = hsv.x * 60.0;   
                if (hsv.x < 0) 
                    hsv.x = hsv.x + 360;
                hsv.z=max1;
                hsv.y=(max1-min1)/max1;
                return hsv;
            }

            //HSV to RGB
            float3 HSVConvertToRGB(float3 hsv)
            {
                float R,G,B;
                //float3 rgb;
                if( hsv.y == 0 )
                {
                    R=G=B=hsv.z;
                }
                else
                {
                    hsv.x = hsv.x/60.0; 
                    int i = (int)hsv.x;
                    float f = hsv.x - (float)i;
                    float a = hsv.z * ( 1 - hsv.y );
                    float b = hsv.z * ( 1 - hsv.y * f );
                    float c = hsv.z * ( 1 - hsv.y * (1 - f ) );
                    if (i == 0) { R = hsv.z; G = c; B = a; }
                    else if(i == 1) { R = b; G = hsv.z; B = a; }
                    else if(i == 2) { R = a; G = hsv.z; B = c;  }
                    else if(i == 3) { R = a; G = b; B = hsv.z;  }
                    else if(i == 4) { R = c; G = a; B = hsv.z;  }
                    else  { R = hsv.z; G = a; B = b;  }
                }
                return float3(R,G,B);
            } 

            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color;

                return OUT;
            }


            half4 _MainTex_ST;
            float4 frag(v2f i) : COLOR
            {
                float2 uvst = UnityStereoScreenSpaceUVAdjust(i.texcoord, _MainTex_ST);
                float4 color = tex2D(_MainTex,uvst);

                float4 srcColor = color;
                float4 rt = smoothstep(_DotSize, 0.1+_DotSize, color.r) * _Color.r;
                float4 gt = smoothstep(_DotSize, 0.1+_DotSize, color.g) * _Color.g;
                float4 bt = smoothstep(_DotSize, 0.1+_DotSize, color.b) * _Color.b;
                color = rt + gt + bt;
                
                float3 colorHSV;    
                colorHSV.rgb = RGBConvertToHSV(srcColor.rgb);   //转换为HSV
                colorHSV.r += _Hue; //调整偏移Hue值
                colorHSV.r = colorHSV.r % 360;    //超过360的值从0开始

                colorHSV.g *= _Saturation;  //调整饱和度
                colorHSV.b *= _Value;                           

                colorHSV.rgb = HSVConvertToRGB(colorHSV.rgb);   //将调整后的HSV，转换为RGB颜色
                
                
                color.a = 0;

                if (_Color.a > 0)
                {
                    color.rgb = color.rgb * (colorHSV.rgb);  
                }

                color = lerp(srcColor,  color, _TimeX);

                return color;
            }

        ENDCG
        }
    }
}