﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "UGUI/UGUI_DYEONE"     
{     
    Properties     
    {     
        
         [PerRendererData]_MainTex ("Sprite Texture", 2D) = "white" {}     
        _Color ("Color", Color) = (1,1,1,1)
        _Brightness("Brightness",Range(0,1)) = 1
        _Rate("Rate",Range(0,0.5)) = 0.15
    }     
     
    SubShader     
    {     
        Tags     
        {      
            "Queue"="Transparent"      
            "IgnoreProjector"="True"      
            "RenderType"="Transparent"      
            "PreviewType"="Plane"     
            "CanUseSpriteAtlas"="True"     
        }     
        // 源rgba*源a + 背景rgba*(1-源A值)   
        Blend SrcAlpha OneMinusSrcAlpha  
     
        Pass     
        {     
            CGPROGRAM     
            #pragma vertex vert     
            #pragma fragment frag     
            #include "UnityCG.cginc"     
                 
            struct appdata_t     
            {     
                float4 vertex   : POSITION;     
                //float4 color    : COLOR;     
                float2 texcoord : TEXCOORD0;     
            };     
     
            struct v2f     
            {     
                float4 vertex   : SV_POSITION;     
                //fixed4 color    : COLOR;     
                half2 texcoord  : TEXCOORD0;     
            };     
               
            sampler2D _MainTex;       
            fixed4 _Color;
            fixed _Brightness;
            fixed _Rate;
            v2f vert(appdata_t IN)     
            {     
                v2f OUT;     
                OUT.vertex = UnityObjectToClipPos(IN.vertex);     
                OUT.texcoord = IN.texcoord;     
#ifdef UNITY_HALF_TEXEL_OFFSET     
                OUT.vertex.xy -= (_ScreenParams.zw-1.0);     
#endif        
                return OUT;  
            }  
     
            fixed4 frag(v2f IN) : SV_Target     
            {     
                fixed4 colorTex = tex2D(_MainTex, IN.texcoord);
                fixed4 col = _Color;
                fixed gray = 0.2125 * colorTex.r + 0.7154 * colorTex.g + 0.0721 * colorTex.b;
                fixed3 grayColor = fixed3(gray, gray, gray);
                col.rgb = col.rgb * grayColor.rgb + (col.rgb * _Rate);

                return col;
            }     
            ENDCG     
        }     
    }     
}