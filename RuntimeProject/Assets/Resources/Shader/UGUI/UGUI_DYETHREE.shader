﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "UGUI/UGUI_DYETHREE"     
{     
    Properties     
    {     
        
        [PerRendererData]_MainTex ("Sprite Texture", 2D) = "white" {}     
        _ColorR ("ColorR", Color) = (1,1,1,1)
        _ColorG ("ColorG", Color) = (1,1,1,1)
        _ColorB ("ColorB", Color) = (1,1,1,1)
        _Rate("Rate",Range(0,0.5)) = 0.15
        _UvRect ("UvRect", Vector) = (0, 0, 1, 1)
    }     
     
    SubShader     
    {     
        Tags     
        {      
            "Queue"="Transparent"      
            "IgnoreProjector"="True"      
            "RenderType"="Transparent"      
            "PreviewType"="Plane"     
            "CanUseSpriteAtlas"="True"     
        }     
        // 源rgba*源a + 背景rgba*(1-源A值)   
        Blend SrcAlpha OneMinusSrcAlpha  
     
        Pass     
        {     
            CGPROGRAM     
            #pragma vertex vert     
            #pragma fragment frag     
            #include "UnityCG.cginc"     
                 
            struct appdata_t     
            {     
                float4 vertex   : POSITION;     
                //float4 color    : COLOR;     
                float2 texcoord : TEXCOORD0;     
            };     
     
            struct v2f     
            {     
                float4 vertex   : SV_POSITION;     
                //fixed4 color    : COLOR;     
                float2 texcoord  : TEXCOORD0;     
            };     
               
            sampler2D _MainTex;
            float4 _MainTex_ST;   
            fixed4 _ColorR;
            fixed4 _ColorG;
            fixed4 _ColorB;
            fixed _Rate;
            float4 _UvRect;

            v2f vert(appdata_t IN)     
            {     
                v2f OUT;     
                OUT.vertex = UnityObjectToClipPos(IN.vertex);     
                OUT.texcoord = IN.texcoord;
#ifdef UNITY_HALF_TEXEL_OFFSET     
                OUT.vertex.xy -= (_ScreenParams.zw-1.0);
#endif        
                return OUT;  
            }  
     
            fixed4 frag(v2f IN) : SV_Target     
            {     
                fixed4 colorTex = tex2D(_MainTex, IN.texcoord);
                fixed gray = 0.2125 * colorTex.r + 0.7154 * colorTex.g + 0.0721 * colorTex.b;
                fixed3 grayColor = fixed3(gray, gray, gray);
                fixed3 color = colorTex.rgb;
                //转换图集sprite纹理坐标
                IN.texcoord.x = (IN.texcoord.x - _UvRect.x)/(_UvRect.z - _UvRect.x);
                IN.texcoord.y = (IN.texcoord.y - _UvRect.y)/(_UvRect.w - _UvRect.y);

                if ((IN.texcoord.x + IN.texcoord.y <= 1) && (IN.texcoord.x - IN.texcoord.y <= 0))
                {
                    color.rgb = _ColorR.rgb * grayColor.rgb + (_ColorR.rgb * _Rate);
                }
                else 
                {
                    if((IN.texcoord.x + IN.texcoord.y <= 1) && (IN.texcoord.x - IN.texcoord.y >= 0))
                    {
                        color.rgb = _ColorG.rgb * grayColor.rgb + (_ColorG.rgb * _Rate);
                    }
                    else
                    {
                        color.rgb = _ColorB.rgb * grayColor.rgb + (_ColorB.rgb * _Rate);
                    }
                }
               
                return fixed4(color, 1);
            }     
            ENDCG     
        }     
    }     
}