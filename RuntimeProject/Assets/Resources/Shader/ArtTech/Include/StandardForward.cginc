// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

#ifndef STANDARD_FORWARD_INCLUDED
#define STANDARD_FORWARD_INCLUDED

#include "StandardCommon.cginc"

struct VertexInput {
	float4 vertex : POSITION;
	float3 normal : NORMAL;
#if defined (_NORMALMAP)
	float4 tangent : TANGENT;
#endif
	float4 texcoord : TEXCOORD0;
#ifndef LIGHTMAP_OFF
	float4 texcoord1 : TEXCOORD1;
#endif
#ifndef DYNAMICLIGHTMAP_OFF
	float4 texcoord2 : TEXCOORD2;
#endif
};


struct VertexOutputBase {
	float4 pos : SV_POSITION;
	float4 pack0 : TEXCOORD0;
#if defined (_EMISSION)
	float2 pack1 : TEXCOORD1;
#endif
#if defined (_NORMALMAP)
	float4 tSpace0 : TEXCOORD2;
	float4 tSpace1 : TEXCOORD3;
	float4 tSpace2 : TEXCOORD4;
#else
	float3 worldPos : TEXCOORD2;
	fixed3 worldNormal : TEXCOORD3;
#endif


#if !defined(LIGHTMAP_OFF) || !defined(DYNAMICLIGHTMAP_OFF)
	float4 lmap : TEXCOORD5;
#else
#if UNITY_SHOULD_SAMPLE_SH
	half3 sh : TEXCOORD5; // SH
#endif	
#endif
	SHADOW_COORDS(6)
	UNITY_FOG_COORDS(7)
};

struct VertexOutputAdd {
	float4 pos : SV_POSITION;
	float4 pack0 : TEXCOORD0;
#if defined (_EMISSION)
	float2 pack1 : TEXCOORD1;
#endif
#if defined (_NORMALMAP)
	float4 tSpace0 : TEXCOORD2;
	float4 tSpace1 : TEXCOORD3;
	float4 tSpace2 : TEXCOORD4;
#else
	float3 worldPos : TEXCOORD2;
	fixed3 worldNormal : TEXCOORD3;
#endif
	SHADOW_COORDS(5)
	UNITY_FOG_COORDS(6)
};





VertexOutputBase vertBase(VertexInput v) {
	VertexOutputBase o;
	UNITY_INITIALIZE_OUTPUT(VertexOutputBase, o);

	o.pos = UnityObjectToClipPos(v.vertex);
	o.pack0.xy = TRANSFORM_TEX(v.texcoord, _MainTex);

	float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
	fixed3 worldNormal = UnityObjectToWorldNormal(v.normal);
#if defined (_NORMALMAP)
	//o.pack0.zw = TRANSFORM_TEX(v.texcoord, _BumpMap);

	fixed3 worldTangent = UnityObjectToWorldDir(v.tangent.xyz);
	fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
	fixed3 worldBinormal = cross(worldNormal, worldTangent) * tangentSign;
	o.tSpace0 = float4(worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x);
	o.tSpace1 = float4(worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y);
	o.tSpace2 = float4(worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z);
#else
	o.worldPos = worldPos;
	o.worldNormal = worldNormal;
#endif
	/*
#if defined (_EMISSION)
	o.pack1.xy = TRANSFORM_TEX(v.texcoord, _EmissionMap);
#endif
	*/

#ifndef LIGHTMAP_OFF
	o.lmap.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
#endif
#ifndef DYNAMICLIGHTMAP_OFF
	o.lmap.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
#endif

	// SH/ambient and vertex lights
#ifdef LIGHTMAP_OFF
	#if UNITY_SHOULD_SAMPLE_SH
		o.sh = 0;
		// Approximated illumination from non-important point lights
		#ifdef VERTEXLIGHT_ON
			o.sh += Shade4PointLights(
				unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,
				unity_LightColor[0].rgb, unity_LightColor[1].rgb, unity_LightColor[2].rgb, unity_LightColor[3].rgb,
				unity_4LightAtten0, worldPos, worldNormal);
		#endif
		o.sh = ShadeSHPerVertex(worldNormal, o.sh);
	#endif
#endif // LIGHTMAP_OFF

#ifndef _ALPHABLEND_ON
	TRANSFER_SHADOW(o); // pass shadow coordinates to pixel shader
#endif
	UNITY_TRANSFER_FOG(o, o.pos); // pass fog coordinates to pixel shader

	return o;
}


fixed4 fragBase(VertexOutputBase i) : SV_Target {
	Input surfIN;
	UNITY_INITIALIZE_OUTPUT(Input, surfIN);
	surfIN.uv_MainTex = i.pack0.xy;
	/*
#if defined (_NORMALMAP)
	surfIN.uv_BumpMap = i.pack0.zw;
#endif
#if defined (_EMISSION)
	surfIN.uv_Illum = i.pack1.xy;
#endif
	*/

#if defined (_NORMALMAP)
	float3 worldPos = float3(i.tSpace0.w, i.tSpace1.w, i.tSpace2.w);
#else
	float3 worldPos = i.worldPos;
#endif
#ifndef USING_DIRECTIONAL_LIGHT
	fixed3 lightDir = normalize(UnityWorldSpaceLightDir(worldPos));
#else
	fixed3 lightDir = _WorldSpaceLightPos0.xyz;
#endif
	fixed3 worldViewDir = normalize(UnityWorldSpaceViewDir(worldPos));
#ifdef UNITY_COMPILER_HLSL
	VSSurfaceOutput o = (VSSurfaceOutput)0;
#else
	VSSurfaceOutput o;
#endif
	o.Albedo = 0.0;
	o.Emission = 0.0;
#if defined (_SPECULAR)
	o.Specular = 0.0;
#endif
	o.Alpha = 0.0;
	//o.Gloss = 0.0;
	surf(surfIN, o);
#if defined(_ALPHATEST_ON)
	clip(o.Alpha - _Cutoff);
#endif

	UNITY_LIGHT_ATTENUATION(atten, i, worldPos)
	fixed4 c = 0;
	fixed3 worldN;
#if defined (_NORMALMAP)
	worldN.x = dot(i.tSpace0.xyz, o.Normal);
	worldN.y = dot(i.tSpace1.xyz, o.Normal);
	worldN.z = dot(i.tSpace2.xyz, o.Normal);
#else
	worldN = i.worldNormal;
#endif
	o.Normal = worldN;


#if defined(_CUBE_REFLECT)
	o.Emission += CalcReflectColor(worldViewDir, worldN, surfIN);
#endif


	UnityGI gi;
	UNITY_INITIALIZE_OUTPUT(UnityGI, gi);
	gi.indirect.diffuse = 1;
	gi.indirect.specular = 0;
#if defined(LIGHTMAP_OFF)
	gi.light.color = _LightColor0.rgb;
	gi.light.dir = lightDir;
	gi.light.ndotl = LambertTerm(o.Normal, gi.light.dir);
#endif
	// Call GI (lightmaps/SH/reflections) lighting function
	UnityGIInput giInput;
	UNITY_INITIALIZE_OUTPUT(UnityGIInput, giInput);
	giInput.light = gi.light;
	giInput.worldPos = worldPos;
	giInput.worldViewDir = worldViewDir;
	giInput.atten = atten;
#if !defined(LIGHTMAP_OFF) || !defined(DYNAMICLIGHTMAP_OFF)
	giInput.lightmapUV = i.lmap;
#else
	giInput.lightmapUV = 0.0;
#endif
#if UNITY_SHOULD_SAMPLE_SH
	giInput.ambient = i.sh;
#else
	giInput.ambient.rgb = 0.0;
#endif
	/*
	giInput.probeHDR[0] = unity_SpecCube0_HDR;
	giInput.probeHDR[1] = unity_SpecCube1_HDR;
#if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
	giInput.boxMin[0] = unity_SpecCube0_BoxMin; // .w holds lerp value for blending
#endif
#if UNITY_SPECCUBE_BOX_PROJECTION
	giInput.boxMax[0] = unity_SpecCube0_BoxMax;
	giInput.probePosition[0] = unity_SpecCube0_ProbePosition;
	giInput.boxMax[1] = unity_SpecCube1_BoxMax;
	giInput.boxMin[1] = unity_SpecCube1_BoxMin;
	giInput.probePosition[1] = unity_SpecCube1_ProbePosition;
#endif
	*/
	Lighting_GI(o, giInput, gi);

	// realtime lighting: call lighting function
	c += Lighting(o, worldViewDir, gi);
#if defined (_EMISSION) || defined(_CUBE_REFLECT)
	c.rgb += o.Emission;
#endif

	c = saturate(c);

	UNITY_APPLY_FOG(i.fogCoord, c); // apply fog
#if !defined (_ALPHATEST_ON) && !defined(_ALPHABLEND_ON)
	UNITY_OPAQUE_ALPHA(c.a);
#endif

	return c;
}





VertexOutputAdd vertAdd(VertexInput v) {
	VertexOutputAdd o;
	UNITY_INITIALIZE_OUTPUT(VertexOutputAdd, o);

	o.pos = UnityObjectToClipPos(v.vertex);
	o.pack0.xy = TRANSFORM_TEX(v.texcoord, _MainTex);

	float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
	fixed3 worldNormal = UnityObjectToWorldNormal(v.normal);
#if defined (_NORMALMAP)
	//o.pack0.zw = TRANSFORM_TEX(v.texcoord, _BumpMap);

	fixed3 worldTangent = UnityObjectToWorldDir(v.tangent.xyz);
	fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
	fixed3 worldBinormal = cross(worldNormal, worldTangent) * tangentSign;
	o.tSpace0 = float4(worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x);
	o.tSpace1 = float4(worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y);
	o.tSpace2 = float4(worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z);
#else
	o.worldPos = worldPos;
	o.worldNormal = worldNormal;
#endif
	/*
#if defined (_EMISSION)
	o.pack1.xy = TRANSFORM_TEX(v.texcoord, _EmissionMap);
#endif
	*/


#ifndef _ALPHABLEND_ON
	TRANSFER_SHADOW(o); // pass shadow coordinates to pixel shader
#endif
	UNITY_TRANSFER_FOG(o, o.pos); // pass fog coordinates to pixel shader

	return o;
}


fixed4 fragAdd(VertexOutputAdd i) : SV_Target {
	Input surfIN;
	UNITY_INITIALIZE_OUTPUT(Input, surfIN);
	surfIN.uv_MainTex = i.pack0.xy;
	/*
#if defined (_NORMALMAP)
	surfIN.uv_BumpMap = i.pack0.zw;
#endif
#if defined (_EMISSION)
	surfIN.uv_Illum = i.pack1.xy;
#endif
	*/

#if defined (_NORMALMAP)
	float3 worldPos = float3(i.tSpace0.w, i.tSpace1.w, i.tSpace2.w);
#else
	float3 worldPos = i.worldPos;
#endif
#ifndef USING_DIRECTIONAL_LIGHT
	fixed3 lightDir = normalize(UnityWorldSpaceLightDir(worldPos));
#else
	fixed3 lightDir = _WorldSpaceLightPos0.xyz;
#endif
	fixed3 worldViewDir = normalize(UnityWorldSpaceViewDir(worldPos));
#ifdef UNITY_COMPILER_HLSL
	VSSurfaceOutput o = (VSSurfaceOutput)0;
#else
	VSSurfaceOutput o;
#endif
	o.Albedo = 0.0;
	o.Emission = 0.0;
#if defined (_SPECULAR)
	o.Specular = 0.0;
#endif
	o.Alpha = 0.0;
	//o.Gloss = 0.0;
	surf(surfIN, o);
#if defined(_ALPHATEST_ON)
	clip(o.Alpha - _Cutoff);
#endif

	UNITY_LIGHT_ATTENUATION(atten, i, worldPos)
	fixed4 c = 0;
	fixed3 worldN;
#if defined (_NORMALMAP)
	worldN.x = dot(i.tSpace0.xyz, o.Normal);
	worldN.y = dot(i.tSpace1.xyz, o.Normal);
	worldN.z = dot(i.tSpace2.xyz, o.Normal);
#else
	worldN = i.worldNormal;
#endif
	o.Normal = worldN;

	UnityGI gi;
	UNITY_INITIALIZE_OUTPUT(UnityGI, gi);
	gi.indirect.diffuse = 0;
	gi.indirect.specular = 0;
#if !defined(LIGHTMAP_ON)
	gi.light.color = _LightColor0.rgb;
	gi.light.dir = lightDir;
	gi.light.ndotl = LambertTerm(o.Normal, gi.light.dir);
#endif
	gi.light.color *= atten;
	c += Lighting(o, worldViewDir, gi);

	UNITY_APPLY_FOG(i.fogCoord, c); // apply fog
#if !defined (_ALPHATEST_ON) && !defined(_ALPHABLEND_ON)
	UNITY_OPAQUE_ALPHA(c.a);
#endif

	return c;
}






#endif