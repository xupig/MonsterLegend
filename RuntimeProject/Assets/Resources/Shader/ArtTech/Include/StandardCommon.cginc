#ifndef STANDARD_COMMON_INCLUDED
#define STANDARD_COMMON_INCLUDED


#include "Lighting.cginc"
#include "UnityCG.cginc"
#include "AutoLight.cginc"


fixed4 _Color;
sampler2D _MainTex;
float4 _MainTex_ST;

#if defined(_ALPHATEST_ON)
half _Cutoff;
#endif

#if defined (_NORMALMAP)
float _BumpScale;
sampler2D _BumpMap;
//float4 _BumpMap_ST;
#endif

#if defined (_SPECULAR)
sampler2D _SpecMap;
//fixed4 _SpecColor;
float _Shininess;
#endif

#if defined (_EMISSION)
fixed4 _EmissionColor;
sampler2D _EmissionMap;
//float4 _EmissionMap_ST;
#endif

/*
#if defined(_CUBE_REFLECT)
fixed4 _ReflectionColor;
sampler2D _ReflectionMap;
samplerCUBE _ReflectionCube;
#endif
*/



struct Input {
	float2 uv_MainTex;
	/*
#if defined (_NORMALMAP)
	float2 uv_BumpMap;
#endif
#if defined (_EMISSION)
	float2 uv_Illum;
#endif
#if defined(_SPECULARMAP)
	float2 uv_SpecMap;
#endif
	*/
};

struct VSSurfaceOutput {
	fixed3 Albedo;
	fixed3 Normal;
	fixed3 Emission;
#if defined (_SPECULAR)
	half Specular;
	//fixed Gloss;
	fixed3 SpecColor;
#endif
	fixed Alpha;
};



inline fixed4 ComputeLight(VSSurfaceOutput s, half3 viewDir, UnityLight light) {
	fixed diff = max(0, dot(s.Normal, light.dir));
#if defined (_SPECULAR)
	half3 h = normalize(light.dir + viewDir);
	float nh = max(0, dot(s.Normal, h));
	float spec = pow(nh, s.Specular*128.0);// *s.Gloss;
#endif

	fixed4 c;
	c.rgb = s.Albedo * light.color * diff
#if defined (_SPECULAR)
		+ light.color * s.SpecColor * spec
#endif
		;
	c.a = s.Alpha;

	return c;
}

inline fixed4 Lighting(VSSurfaceOutput s, half3 viewDir, UnityGI gi) {
	fixed4 c;
	c = ComputeLight(s, viewDir, gi.light);

#if defined(DIRLIGHTMAP_SEPARATE)
#ifdef LIGHTMAP_ON
	c += ComputeLight(s, viewDir, gi.light2);
#endif
#ifdef DYNAMICLIGHTMAP_ON
	c += ComputeLight(s, viewDir, gi.light3);
#endif
#endif

#ifdef UNITY_LIGHT_FUNCTION_APPLY_INDIRECT
	c.rgb += s.Albedo * gi.indirect.diffuse;
#endif

	return c;
}

inline void Lighting_GI(
	VSSurfaceOutput s,
	UnityGIInput data,
	inout UnityGI gi)
{
	gi = UnityGlobalIllumination(data, 1.0, s.Normal);
}

void surf(Input IN, inout VSSurfaceOutput o) {
	fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
	fixed4 c = tex * _Color;
	o.Albedo = c.rgb;

	o.Alpha = c.a;

#if defined (_NORMALMAP)
	//o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
	o.Normal = UnpackScaleNormal(tex2D(_BumpMap, IN.uv_MainTex), _BumpScale);
#endif

#if defined (_SPECULAR)
	half4 SpecMap = tex2D(_SpecMap, IN.uv_MainTex);
	o.SpecColor = SpecMap.a * _SpecColor.rgb;
	o.Specular = _Shininess;
#endif

//#if defined(UNITY_PASS_FORWARDBASE)
#if defined (_EMISSION)
	o.Emission += tex2D(_EmissionMap, IN.uv_MainTex).rgb * _EmissionColor.rgb;
#endif
//#endif
}


#if defined(_CUBE_REFLECT)
fixed3 CalcReflectColor(fixed3 worldViewDir, fixed3 worldNormal, Input IN) {
	float3 worldRefl = reflect(-worldViewDir, worldNormal);
	fixed4 reflcol = texCUBE(_ReflectionCube, worldRefl);
	return reflcol.rgb * tex2D(_ReflectionMap, IN.uv_MainTex).rgb * _ReflectionColor.rgb;
}
#endif









#endif