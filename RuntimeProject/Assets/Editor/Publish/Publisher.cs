﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;


public class Publisher
{
    private static string StreamingAssetsPath = "StreamingAssets";
    private static string RuntimeResourcePath = "../RuntimeResource/";
    private static string RuntimeResourceAndroidPath = "../RuntimeResource/";
    private static string RuntimeResourceIPhonePath = "../RuntimeResource/";
    private static string GameCodePath = "../GameCode/Lib/";
    private static string ResourcesPath = "Resources";

    private static string GetResourcePath()
    {
        switch (EditorUserBuildSettings.activeBuildTarget)
        {
            case BuildTarget.Android:
                return RuntimeResourceAndroidPath;
            case BuildTarget.StandaloneWindows:
                return RuntimeResourcePath;
            case BuildTarget.StandaloneWindows64:
                return RuntimeResourcePath;
            case BuildTarget.iOS:
                return RuntimeResourceIPhonePath;
            default:
                return RuntimeResourcePath;
        }
    }

    [MenuItem("Publish/Copy RuntimeResource To My StreamingAsset")]
    public static void CopyRuntimeResourceToStreamingAsset()
    {
        Debug.Log("CopyRuntimeResourceToStreamingAsset Begin:");
        string streamingAssetsPath = Path.Combine(Application.dataPath, StreamingAssetsPath);
        if (!Directory.Exists(streamingAssetsPath))
        {
            Directory.CreateDirectory(streamingAssetsPath);
        }
        string resourcesPath = Path.Combine(Application.dataPath, ResourcesPath);
        string runtimeResourcePath = GetResourcePath();
        CopyDirectoryFiles(runtimeResourcePath, streamingAssetsPath, "*.u");
        CopyDirectoryFiles(runtimeResourcePath, resourcesPath, "*.mk", "txt");
        CopyDirectoryFiles(Path.Combine(runtimeResourcePath, "libs"), Path.Combine(resourcesPath, "libs"), "*.dll", "bytes");
        CopyDirectoryFiles(Path.Combine(runtimeResourcePath, "data"), Path.Combine(resourcesPath, "data"), "*.xml");
        CopyDirectoryFiles(Path.Combine(runtimeResourcePath, "data"), Path.Combine(resourcesPath, "data"), "*.txt");
        CopyDirectoryFiles(Path.Combine(runtimeResourcePath, "data"), Path.Combine(resourcesPath, "data"), "*.bm", "bytes");
        CopyDirectoryFiles(Path.Combine(runtimeResourcePath, "data"), Path.Combine(resourcesPath, "data"), "*.pbuf", "bytes");
        //CopyDirectoryFiles(GameCodePath, Path.Combine(resourcesPath, "libs"), "*.dll", "xml", SearchOption.TopDirectoryOnly);
        AssetDatabase.Refresh();
        Debug.Log("CopyRuntimeResourceToStreamingAsset End.");
    }

    [MenuItem("Publish/Copy RuntimeResource To My StreamingAsset(For IOS)")]
    public static void CopyRuntimeResourceToStreamingAssetForIOS()
    {
        Debug.Log("CopyRuntimeResourceToStreamingAsset Begin:");
        string streamingAssetsPath = Path.Combine(Application.dataPath, StreamingAssetsPath);
        if (!Directory.Exists(streamingAssetsPath))
        {
            Directory.CreateDirectory(streamingAssetsPath);
        }
        string resourcesPath = Path.Combine(Application.dataPath, ResourcesPath);
        string runtimeResourcePath = GetResourcePath();
        CopyDirectoryFiles(runtimeResourcePath, streamingAssetsPath, "*.u");
        CopyDirectoryFiles(runtimeResourcePath, streamingAssetsPath, "*.mk");
        string targetPkgPath = streamingAssetsPath + "/pkg";
        if (File.Exists(targetPkgPath)) File.Delete(targetPkgPath);
        File.Copy(runtimeResourcePath + "/pkg", targetPkgPath);
        AssetDatabase.Refresh();
        Debug.Log("CopyRuntimeResourceToStreamingAsset End.");
    }

    public static void CopyDirectoryFiles(string srcPath, string tarPath, string pattern, string replaceExt = null, SearchOption opt = SearchOption.AllDirectories)
    {
        var fileList = Directory.GetFiles(srcPath, pattern, opt);
        foreach (string path in fileList)
        {
            string spath = path.Replace("\\", "/");
            string srcHalfDir = srcPath;
            string srcName = ReplaceFirst(spath, srcHalfDir, "");
            //Debug.Log(srcName);
            if (replaceExt != null)
            {
                srcName = srcName.Substring(0, srcName.IndexOf(".") + 1) + replaceExt;
            }
            //Debug.Log(srcName);
            string dstPath = tarPath + "/" + srcName;
            string dstDir = dstPath.Substring(0, dstPath.LastIndexOf("/"));
            if (!Directory.Exists(dstDir))
                Directory.CreateDirectory(dstDir);
            if (File.Exists(dstPath))
                File.Delete(dstPath);
            File.Copy(spath, dstPath);
        }
    }

    public static string ReplaceFirst(string input, string oldValue, string newValue, int startAt = 0)
    {
        int pos = input.IndexOf(oldValue, startAt);
        if (pos < 0)
        {
            return input;
        }
        return string.Concat(input.Substring(0, pos), newValue, input.Substring(pos + oldValue.Length));
    }
}
