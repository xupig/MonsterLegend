﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

public class BuildToolsForiOS
{
    static void BuildByCommand()
    {
        DeleteLib();
        string[] argv = System.Environment.GetCommandLineArgs();
        if (argv.Length == 14)
        {
            if (EditorUserBuildSettings.activeBuildTarget != BuildTarget.iOS)
            {
                EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.iOS);
            }
            InitPlayerSetting();
            PlayerSettings.bundleIdentifier = argv[7];
            PlayerSettings.iPhoneBundleIdentifier = argv[7];
            PlayerSettings.allowedAutorotateToLandscapeLeft = argv[8] == "1" ? true : false;
            PlayerSettings.allowedAutorotateToLandscapeRight = argv[9] == "1" ? true : false;
            PlayerSettings.bundleVersion = argv[10];
            PlayerSettings.productName = argv[11];
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, argv[12]);
            EditorUserBuildSettings.SetBuildLocation(BuildTarget.iOS, argv[13]);
            BuildProject();
        }
        else
        {
            Debug.LogError(" BuildByCommand need 14 args right now is " + argv.Length);
        }
    }

    static void InitPlayerSetting()
    {
        PlayerSettings.accelerometerFrequency = 15;
        PlayerSettings.allowedAutorotateToLandscapeLeft = true;
        PlayerSettings.allowedAutorotateToLandscapeRight = true;
        PlayerSettings.allowedAutorotateToPortrait = false;
        PlayerSettings.allowedAutorotateToPortraitUpsideDown = false;
        PlayerSettings.useAnimatedAutorotation = true;
        PlayerSettings.defaultInterfaceOrientation = UIOrientation.AutoRotation;
        PlayerSettings.apiCompatibilityLevel = ApiCompatibilityLevel.NET_2_0;
        PlayerSettings.iOS.prerenderedIcon = true;
        PlayerSettings.iOS.exitOnSuspend = false;
        PlayerSettings.iOS.requiresPersistentWiFi = true;
        PlayerSettings.iOS.sdkVersion = iOSSdkVersion.DeviceSDK;
        PlayerSettings.iOS.targetDevice = iOSTargetDevice.iPhoneAndiPad;
        PlayerSettings.iOS.targetOSVersion = iOSTargetOSVersion.iOS_7_0;
        PlayerSettings.companyName = @"leishen";
        PlayerSettings.strippingLevel = StrippingLevel.Disabled;
        PlayerSettings.aotOptions = "ntrampolines=8192,nimt-trampolines=8192";
    }

    static void BuildProject()
    {
        var unityPathList = new string[] { "Assets/Scenes/InitScene.unity",
                                           "Assets/Scenes/EmptyScene.unity",
                                           "Assets/Scenes/ContainerScene.unity" };
        BuildPipeline.BuildPlayer(unityPathList,
            EditorUserBuildSettings.GetBuildLocation(BuildTarget.iOS),
            BuildTarget.iOS, BuildOptions.SymlinkLibraries);
    }

    public static void DeleteLib()
    {
        string[] dllPaths = new string[2];
        dllPaths[0] = "/Plugins/x86";
        dllPaths[1] = "/Plugins/x86_64";
        for (int i = 0; i < dllPaths.Length; i++)
        {
            string dllPath = Application.dataPath + dllPaths[i];
            if (System.IO.Directory.Exists(dllPath))
            {
                string[] dlls = Directory.GetFiles(dllPath, "*.dll");
                for (int j = 0; j < dlls.Length; j++)
                {
                    if (File.Exists(dlls[j]))
                    {
                        File.SetAttributes(dlls[j], FileAttributes.Normal);
                        File.Delete(dlls[j]);
                    }
                }
            }
        }
    }
}
