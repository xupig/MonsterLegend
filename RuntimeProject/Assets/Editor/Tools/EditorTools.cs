﻿using System;
using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class EditorTools
{
    static EditorTools()
    {
        EditorApplication.playmodeStateChanged = HandleOnPlayModeChanged;
    }

    static void HandleOnPlayModeChanged()
    {
        if (EditorApplication.isPlayingOrWillChangePlaymode && !EditorApplication.isPlaying)
        {//在刚启动还没正式进入Play的模式的时候进行内存清理
            GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced);
            Resources.UnloadUnusedAssets();
        }
    }
}