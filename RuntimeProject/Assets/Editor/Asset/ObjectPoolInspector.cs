﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace AssetBundle
{
    public class ObjectPoolInspector : EditorWindow
    {
        private static Vector2 _scrollPostion = Vector2.zero;
        private static ObjectPoolInspector _window;
        private static Color _greenColor = new Color(26f / 255, 171f / 255, 37f / 255);

        [MenuItem("Window/ObjectPoolInspector")]
        public static void ShowInspector()
        {
            _window = EditorWindow.GetWindow<ObjectPoolInspector>("ObjectPoolInspector");
            _window.Show();
        }

        private void OnGUI()
        {
            Refresh();
        }

        private static void Refresh()
        {
            Type type = GetObjectPoolType();
            if(type == null) return;
            _scrollPostion = EditorGUILayout.BeginScrollView(_scrollPostion);
            EditorGUILayout.BeginVertical();
            Color recordColor = GUI.backgroundColor;
            GUI.backgroundColor = _greenColor;
            GUILayout.Button("===============================Active Object Dictionary===============================");
            GUI.backgroundColor = recordColor;
            MethodInfo GetActiveAssetDict = type.GetMethod("GetActiveAssetDict", BindingFlags.Static | BindingFlags.Public);
            Dictionary<string, List<string>> activeAssetDict = GetActiveAssetDict.Invoke(null, null) as Dictionary<string, List<string>>; // ObjectPool.GetActiveAssetDict();
            foreach(string key in GetSortedKeys(activeAssetDict.Keys))
            {
                GUILayout.Label(key);
                List<string> list = activeAssetDict[key];
                if(list.Count > 1)
                {
                    foreach(string s in list)
                    {
                        GUILayout.Label("\t" + s);
                    }
                }
            }
            GUILayout.Label("  ");
            GUI.backgroundColor = _greenColor;
            GUILayout.Button("===================================Asset To Delete====================================");
            GUI.backgroundColor = recordColor;
            MethodInfo GetAssetToDeletePhysicalPathSet = type.GetMethod("GetAssetToDeletePhysicalPathSet", BindingFlags.Static | BindingFlags.Public);
            HashSet<string> physicalPathSet = GetAssetToDeletePhysicalPathSet.Invoke(null, null) as HashSet<string>;//ObjectPool.GetAssetToDeletePhysicalPathSet();
            foreach(string key in physicalPathSet)
            {
                GUILayout.Label(key);
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndScrollView();
        }

        private static Type GetObjectPoolType()
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            for(int i = 0; i < assemblies.Length; i++)
            {
                Assembly assembly = assemblies[i];
                Type type = assembly.GetType("Game.Asset.ObjectPool");
                if(type != null)
                {
                    return type;
                }
            }
            return null;
        }

        private static string[] GetSortedKeys(ICollection keyCollection)
        {
            string[] result = new string[keyCollection.Count];
            keyCollection.CopyTo(result, 0);
            Array.Sort<string>(result);
            return result;
        }
    }
}
