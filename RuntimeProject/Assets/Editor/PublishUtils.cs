﻿using GameLoader.Defines;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security;
using System.Text;
using UIExtension;
using UnityEditor;
using UnityEngine;

public static class PublishUtils
{
    static VersionCodeInfo ProgramVersion;
    static VersionCodeInfo ResourceVersion;
    static VersionCodeInfo GameLoaderVersion;
    static VersionCodeInfo ProgressBarBgVersion;
    private static string _appRootPath = Application.dataPath + "/../";
    private static string _apkFolder = "apk/";
    private static string _defaultLanguageSrcPath = Application.dataPath + "/Resources/DefaultLanguage.xml";

    /// <summary>
    /// 标识项目名字，用于apk包名以及apkurl.xml里
    /// </summary>
    private static string _apkHeader;
    /// <summary>
    /// 本地存放web配置（cfg.xml、common.xml等）的目录
    /// </summary>
    private static string _localWebPath;
    /// <summary>
    /// package文件夹相对目录
    /// </summary>
    private static string _packagePath;
    /// <summary>
    /// packagemd5.xml文件相对路径
    /// </summary>
    private static string _packageMd5Path;
    /// <summary>
    /// common.xml文件相对路径
    /// </summary>
    private static string _configSubPath;
    /// <summary>
    /// gameloader文件存放的相对目录
    /// </summary>
    private static string _gameLoaderPath;
    /// <summary>
    /// DefaultLanguage.xml文件相对路径
    /// </summary>
    private static string _defaultLanguageTargetPath;
    /// <summary>
    /// 网络访问的web目录
    /// </summary>
    private static string _webRootUrl;
    /// <summary>
    /// web上存放apk的相对目录
    /// </summary>
    private static string _apkSubPath;
    /// <summary>
    /// 上传用ftp ip
    /// </summary>
    private static string _ftpIp;
    /// <summary>
    /// 上传ftp的根目录
    /// </summary>
    private static string _ftpSubPath;
    /// <summary>
    /// 上传ftp的相对项目目录
    /// </summary>
    private static string _ftpWebPath;
    /// <summary>
    /// ftp用户名
    /// </summary>
    private static string _ftpUserName;
    /// <summary>
    /// ftp密码
    /// </summary>
    private static string _ftpPassword;
    /// <summary>
    /// 服务器操作post路径（配置原因内容不兼容问题暂时不用）
    /// </summary>
    private static string _serverControlUrl;
    /// <summary>
    /// 内网用分享安装包地址的apkUrl.xml配置的web地址
    /// </summary>
    private static string _apkUrl;
    /// <summary>
    /// keystore密码
    /// </summary>
    private static string _keystorePass;
    /// <summary>
    /// keyalias密码
    /// </summary>
    private static string _keyaliasPass;
    /// <summary>
    /// rtx推送接收者的RTX号码
    /// </summary>
    private static string _rtxReceivers;

    private static string UpdateServerSvn = "0";
    private static string ShutDownServer = "1";
    private static string RebootServer = "2";

    private static string WriteLogFile = "1";
    private static string NotWriteLogFile = "0";

    [MenuItem("点击区域开关/开关")]
    public static void ClickAreaSwitch()
    {
        ImageWrapper.showAllArea = !ImageWrapper.showAllArea;
    }

    [MenuItem("GameServer/UpdateApkDev")]
    public static void UpdateApkDevServer()
    {
        LoadConfig();
        var serverId = -1;//永恒 程序用服务器还没搭
        ServerControl(serverId, UpdateServerSvn, NotWriteLogFile, (updateResult) =>
        {
            UnityEngine.Debug.Log(updateResult);
            ServerControl(serverId, ShutDownServer, NotWriteLogFile, (shutDownResult) =>
            {
                UnityEngine.Debug.Log(shutDownResult);
                ServerControl(serverId, RebootServer, WriteLogFile, (rebootResult) =>
                {
                    UnityEngine.Debug.Log(rebootResult);
                });
            });
        });
    }

    [MenuItem("打包看这里/普通打包")]
    public static void BuildNormalApk()
    {
        RemoveToLua();
        LoadConfig();
        RecordTime(() =>
        {
            SetUpperProgramVersion();
            string[] levels = { "Assets/Scenes/InitScene.unity", "Assets/Scenes/EmptyScene.unity", "Assets/Scenes/ContainerScene.unity" };
            string unionApkName = _apkHeader + ProgramVersion + "_" + System.DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".apk";
            PlayerSettings.keystorePass = _keystorePass;
            PlayerSettings.keyaliasPass = _keyaliasPass;
            UnityEngine.Debug.Log("unionApkName: " + unionApkName);
            UnityEngine.Debug.Log("bundleVersion: " + PlayerSettings.bundleVersion);
            UnityEngine.Debug.Log("bundleVersionCode: " + PlayerSettings.Android.bundleVersionCode);
            BuildPipeline.BuildPlayer(levels, unionApkName, BuildTarget.Android, BuildOptions.None);
            DoEncrytoApk(_appRootPath + unionApkName);
        });
        RecoverToLua();
    }

    [MenuItem("打包看这里/普通不升版本打包")]
    public static void BuildNormalNoUpVerApk()
    {
        RemoveToLua();
        LoadConfig();
        RecordTime(() =>
        {
            ReadFromXml(LoadStringFromResource("VersionInfo"));
            string[] levels = { "Assets/Scenes/InitScene.unity", "Assets/Scenes/EmptyScene.unity", "Assets/Scenes/ContainerScene.unity" };
            string unionApkName = _apkHeader + ProgramVersion + "_" + System.DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".apk";
            PlayerSettings.keystorePass = _keystorePass;
            PlayerSettings.keyaliasPass = _keyaliasPass;
            UnityEngine.Debug.Log("unionApkName: " + unionApkName);
            UnityEngine.Debug.Log("bundleVersion: " + PlayerSettings.bundleVersion);
            UnityEngine.Debug.Log("bundleVersionCode: " + PlayerSettings.Android.bundleVersionCode);
            BuildPipeline.BuildPlayer(levels, unionApkName, BuildTarget.Android, BuildOptions.None);
            DoEncrytoApk(_appRootPath + unionApkName);
        });
        RecoverToLua();
    }

    [MenuItem("打包看这里/打DB包")]
    public static void BuildDBApk()
    {
        RemoveToLua();
        LoadConfig();
        RecordTime(() =>
        {
            ReadFromXml(LoadStringFromResource("VersionInfo"));
            string[] levels = { "Assets/Scenes/InitScene.unity", "Assets/Scenes/EmptyScene.unity", "Assets/Scenes/ContainerScene.unity" };
            string unionApkName = _apkHeader + ProgramVersion + "_" + System.DateTime.Now.ToString("yyyyMMdd_HHmmss") + "_db.apk";
            PlayerSettings.keystorePass = _keystorePass;
            PlayerSettings.keyaliasPass = _keyaliasPass;
            UnityEngine.Debug.Log("unionApkName: " + unionApkName);
            UnityEngine.Debug.Log("bundleVersion: " + PlayerSettings.bundleVersion);
            UnityEngine.Debug.Log("bundleVersionCode: " + PlayerSettings.Android.bundleVersionCode);
            BuildPipeline.BuildPlayer(levels, unionApkName, BuildTarget.Android, BuildOptions.Development | BuildOptions.ConnectWithProfiler);
            DoEncrytoApk(_appRootPath + unionApkName);
        });
        RecoverToLua();
    }

    [MenuItem("打包看这里/UploadFile")]
    public static void UploadFile()
    {
        LoadConfig();
        ReadFromXml(LoadStringFromResource("VersionInfo"));
        var files = Directory.GetFiles(_appRootPath, "*.apk");
        foreach (var item in files)
        {
            HandleWebConfig(item);
            var targetPath = MoveApk(Path.GetFileName(item));
            UploadFile(targetPath, _apkSubPath);
            var resTime = LoadTextEx("BuildResTime.txt");
            var apkTime = LoadTextEx("BuildApkTime.txt");
            var depFileSize = LoadTextEx("ErrorDependencies.txt").Length;
            var content = @"apk打包完成:%0D%0A" + _webRootUrl + targetPath
                + "%0D%0A资源时间:" + resTime
                + "%0D%0A打包时间:" + apkTime
                + "%0D%0A依赖检查结果大小:" + depFileSize;
            SendNotify(content);
        }
    }

    [MenuItem("打包看这里/EncrytoApk")]
    public static void EncrytoApk()
    {
        LoadConfig();
        var files = Directory.GetFiles(_appRootPath, "*.apk");
        foreach (var item in files)
        {
            DoEncrytoApk(item);
        }
    }

    [MenuItem("Update/UpdateGameLoader")]
    public static void UpdateGameLoader()
    {
        LoadConfig();
        var path = LoadTextEx("GameLoaderPath.txt");
        UpdateGameLoader(path);
    }

    [MenuItem("Update/UploadLastPackage")]
    public static void UploadLastPackage()
    {
        LoadConfig();
        var localPackageMd5Path = _localWebPath + _packageMd5Path;
        UploadFile(localPackageMd5Path, _ftpWebPath + Path.GetDirectoryName(_packageMd5Path));
        var localPackageFolder = _localWebPath + _packagePath;
        var allPkgs = Directory.GetFiles(localPackageFolder, "*.pkg", SearchOption.TopDirectoryOnly);
        FileInfo lastPkg = null;
        foreach (var pkg in allPkgs)
        {
            var pkgFile = new FileInfo(pkg);
            if (lastPkg == null || pkgFile.LastWriteTime > lastPkg.LastWriteTime)
            {
                lastPkg = pkgFile;
            }
        }
        var allLastPkgs = new List<FileInfo>();
        foreach (var pkg in allPkgs)
        {
            var pkgFile = new FileInfo(pkg);
            if ((lastPkg.LastWriteTime - pkgFile.LastWriteTime).Minutes < 10)
            {
                allLastPkgs.Add(pkgFile);
            }
        }

        foreach (var pkg in allLastPkgs)
        {
            UploadFile(pkg.FullName, _ftpWebPath + _packagePath);
        }
        UploadWebConfig();
        var resTime = LoadTextEx("BuildResTime.txt");
        var apkTime = LoadTextEx("BuildApkTime.txt");
        var depFileSize = LoadTextEx("ErrorDependencies.txt").Length;
        var content = _apkHeader + "打补丁完成:%0D%0A" + lastPkg.Name
            + "%0D%0A资源时间:" + resTime
            + "%0D%0A打包时间:" + apkTime
            + "%0D%0A补丁总数:" + allLastPkgs.Count;
        SendNotify(content);
    }

    [MenuItem("Update/UpdateDefaultLanguage")]
    public static void UpdateDefaultLanguage()
    {
        LoadConfig();
        UnityEngine.Debug.Log(_defaultLanguageSrcPath);
        var newMd5 = MD5Utils.BuildFileMd5(_defaultLanguageSrcPath);
        Dictionary<string, string> newValue = new Dictionary<string, string>();
        newValue.Add("DefaultLanguageMd5", newMd5);
        SetWebConfig(newValue);
        var localFile = _localWebPath + _defaultLanguageTargetPath;
        File.Copy(_defaultLanguageSrcPath, localFile, true);
        UploadFile(localFile, _ftpWebPath);
        UploadWebConfig();
    }

    private static void DoEncrytoApk(string apkSrcPath)
    {
        return;
        var srcName = Path.GetFileNameWithoutExtension(apkSrcPath);
        if (srcName.EndsWith("_ec"))
        {
            UnityEngine.Debug.Log("apk has encrytoed: " + apkSrcPath);
            return;
        }
        var apkTargetPath = apkSrcPath.Replace(srcName, srcName + "_ec");
        Process process = Process.Start(_appRootPath + "EncrytoTools/AllInOne.bat", apkSrcPath + " " + apkTargetPath);
        process.WaitForExit();
        var bakPath = _appRootPath + "bak/";
        var apkBakPath = bakPath + srcName + ".apk";
        if (!Directory.Exists(bakPath))
            Directory.CreateDirectory(bakPath);
        File.Move(apkSrcPath, apkBakPath);
        UnityEngine.Debug.Log("apk encryto finish: " + apkTargetPath);
    }

    private static void UpdateGameLoader(string gameLoaderPath)
    {
        UnityEngine.Debug.Log(gameLoaderPath);
        var newMd5 = MD5Utils.BuildFileMd5(gameLoaderPath);
        //var orgVersion = new VersionCodeInfo(GetWebConfigByKey("GameLoaderVersion"));
        var orgPath = GetWebConfigByKey("GameLoaderUrl");
        var orgFileName = Path.GetFileNameWithoutExtension(gameLoaderPath);
        var orgFileExt = Path.GetExtension(orgPath);
        var targetFileName = string.Concat(orgFileName, ".", newMd5, orgFileExt);
        var newPath = string.Concat(GetFilePath(orgPath), "/", targetFileName);
        UnityEngine.Debug.Log(newPath);
        Dictionary<string, string> newValue = new Dictionary<string, string>();
        //newValue.Add("GameLoaderVersion", orgVersion.GetUpperVersion());
        newValue.Add("GameLoaderUrl", newPath);
        newValue.Add("GameLoaderMd5", newMd5);
        SetWebConfig(newValue);
        var localPath = _localWebPath + _gameLoaderPath + "/" + targetFileName;
        if (!Directory.Exists(_localWebPath + _gameLoaderPath))
            Directory.CreateDirectory(_localWebPath + _gameLoaderPath);
        if (File.Exists(localPath))
            File.Delete(localPath);
        File.Copy(gameLoaderPath, localPath);
        UnityEngine.Debug.Log(localPath + " " + _ftpWebPath + _gameLoaderPath);
        UploadFile(localPath, _ftpWebPath + _gameLoaderPath);
        UploadWebConfig();
    }

    private static void UpdateApkUrl(string url)
    {
        GetResponse(_apkUrl, (result) =>
        {
            //UnityEngine.Debug.Log(result);
            var content = result.ToXMLSecurityElementEx();
            foreach (SecurityElement item in content.Children)
            {
                if (item.SearchForChildByTag("id").Text == _apkHeader + "dev")
                {
                    item.SearchForChildByTag("url").Text = url;
                    break;
                }
            }
            //UnityEngine.Debug.Log(content);
            var apkUrlFilePath = _appRootPath + _apkFolder + Path.GetFileName(_apkUrl);
            SaveTextEx(apkUrlFilePath, content.ToString());
            UploadFile(apkUrlFilePath, _apkSubPath);
        }, null);
    }

    private static void ServerControl(int serverId, string command, string isWriteLogFile, Action<string> onDone)
    {
        if (serverId < 0)
            return;
        var url = string.Format(_serverControlUrl, serverId, command, isWriteLogFile);
        GetResponse(url, onDone, (errorCode) => { UnityEngine.Debug.LogError(errorCode); });
    }

    private static void SetUpperProgramVersion()
    {
        ReadFromXml(LoadStringFromResource("VersionInfo"));
        ProgramVersion = new VersionCodeInfo(ProgramVersion.GetUpperVersion());
        //ResourceVersion = new VersionCodeInfo(ResourceVersion.GetUpperVersion());
        var resourceVersion = LoadTextEx("StreamingAssetResourceVersion.txt");
        if (!string.IsNullOrEmpty(resourceVersion))
            ResourceVersion = new VersionCodeInfo(resourceVersion);
        var gameloaderVersion = LoadTextEx("StreamingAssetGameLoaderVersion.txt");
        if (!string.IsNullOrEmpty(gameloaderVersion))
            GameLoaderVersion = new VersionCodeInfo(gameloaderVersion);

        PlayerSettings.bundleVersion = ProgramVersion.ToString();
        PlayerSettings.Android.bundleVersionCode += 1;
        SaveVersionInfo();
    }

    //[MenuItem("test/upperWeb")]
    private static void HandleWebConfig(string apkPath)
    {
        var apkUrl = _webRootUrl + _apkSubPath + "/" + Path.GetFileName(apkPath);
        var apkMd5 = MD5Utils.BuildFileMd5(apkPath);
        var apkSize = new FileInfo(apkPath).Length.ToString();
        SetUpdatedVersion(ProgramVersion, ResourceVersion, apkUrl, apkMd5, apkSize);
        UploadWebConfig();
    }

    private static void SetUpdatedVersion(VersionCodeInfo targetPV, VersionCodeInfo targetRV, string apkUrl, string apkMd5, string apkSize)
    {
        Dictionary<string, string> newValue = new Dictionary<string, string>();
        newValue.Add("ProgramVersion", targetPV.ToString());
        newValue.Add("ResourceVersion", targetRV.ToString());
        newValue.Add("ApkUrl", apkUrl);
        newValue.Add("ApkMd5", apkMd5);
        newValue.Add("ApkSize", apkSize);
        SetWebConfig(newValue);
        UpdateApkUrl(apkUrl);
    }

    private static string GetWebConfigByKey(string key)
    {
        var configPath = _localWebPath + _configSubPath;
        var configContent = LoadTextEx(configPath);
        var rootNode = configContent.ToXMLSecurityElementEx();
        if (rootNode == null)
            return "";
        var child = rootNode.SearchForChildByTag(key);
        if (child != null)
            return child.Text;
        else
            return "";
    }


    private static void SetWebConfig(Dictionary<string, string> newValue)
    {
        var configPath = _localWebPath + _configSubPath;
        var configContent = LoadTextEx(configPath);
        var rootNode = configContent.ToXMLSecurityElementEx();
        foreach (var item in newValue)
        {
            SecurityElement oldValue = rootNode.SearchForChildByTag(item.Key);
            if (oldValue != null)
            {
                //这里可以改成使用SecurityElement本身进行修改管理，没改只是做好能用不想动
                var oldContent = string.Concat("<", item.Key, ">", oldValue.Text, "</", item.Key, ">");
                var newContent = string.Concat("<", item.Key, ">", item.Value, "</", item.Key, ">");
                configContent = configContent.Replace(oldContent, newContent);
                UnityEngine.Debug.Log(item.Key + ": " + oldValue.Text + " " + item.Value);
            }
            else
            {
                var curNode = configContent.ToXMLSecurityElementEx();
                curNode.AddChild(new SecurityElement(item.Key, item.Value));
                configContent = curNode.ToString();
            }
        }
        configContent = configContent.Trim();
        if (File.Exists(configPath))
            File.Delete(configPath);
        FileUtils.WriterFile(configPath, configContent);
    }

    private static void UploadWebConfig()
    {
        var configPath = _localWebPath + _configSubPath;
        UnityEngine.Debug.Log(configPath + " " + _ftpWebPath);
        UploadFile(configPath, _ftpWebPath);

    }

    private static void RecordTime(Action action)
    {
        var start = DateTime.Now;
        if (action != null)
            action();
        var buildResTime = DateTime.Now - start;
        var buildResTimeFilePath = _appRootPath + "BuildApkTime.txt";

        SaveTextEx(buildResTimeFilePath, buildResTime.ToString());
    }

    private static string MoveApk(string fileName)
    {
        var targetPath = _apkFolder + fileName;
        if (!Directory.Exists(_apkFolder))
            Directory.CreateDirectory(_apkFolder);
        File.Move(fileName, targetPath);
        return targetPath;
    }

    private static void UploadFile(string fileName, string targetPath)
    {
        UnityEngine.Debug.Log("fileName: " + fileName);
        UnityEngine.Debug.Log("targetPath: " + targetPath);
        var ftp = new FtpUtils(_ftpIp, _ftpSubPath + targetPath, _ftpUserName, _ftpPassword);
        ftp.Upload(fileName);
    }

    private static void SendNotify(string content)
    {
        var url = @"http://192.168.11.224:8012/SendNotify.cgi?&receiver={0}&msg={1}&sender=1010";
        //var receivers = new string[] { "1010", "1007" };

        var result = string.Format(url, _rtxReceivers, content);
        UnityEngine.Debug.Log(result);
        result = UrlEncode(result);
        var www = new WWW(result);
    }

    private static void RemoveToLua()
    {
        if (!Directory.Exists("temp_build/"))
            Directory.CreateDirectory("temp_build/");
        RemoveFolder("Assets/Plugins/x86/", "temp_build/x86/");
        RemoveFolder("Assets/Plugins/x86_64/", "temp_build/x86_64/");
        RemoveFile("Assets/Plugins/x86.meta", "temp_build/x86.meta");
        RemoveFile("Assets/Plugins/x86_64.meta", "temp_build/x86_64.meta");
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
    }

    private static void RemoveFolder(string sourceFolder, string targetFolder)
    {
        if (Directory.Exists(sourceFolder))
        {
            if (Directory.Exists(targetFolder))
            {
                Directory.Delete(targetFolder, true);
            }
            Directory.Move(sourceFolder, targetFolder);
        }
    }

    private static void RemoveFile(string sourceFile, string targetFile)
    {
        if (File.Exists(sourceFile))
        {
            if (File.Exists(targetFile))
            {
                File.Delete(targetFile);
            }
            File.Move(sourceFile, targetFile);
        }
    }

    private static void RecoverToLua()
    {
        Directory.Move("temp_build/x86/", "Assets/Plugins/x86/");
        Directory.Move("temp_build/x86_64/", "Assets/Plugins/x86_64/");
        File.Move("temp_build/x86.meta", "Assets/Plugins/x86.meta");
        File.Move("temp_build/x86_64.meta", "Assets/Plugins/x86_64.meta");
        Directory.Delete("temp_build/");
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
    }

    private static string UrlEncode(string url)
    {
        byte[] bs = System.Text.Encoding.GetEncoding("GB2312").GetBytes(url);
        var sb = new System.Text.StringBuilder();
        for (int i = 0; i < bs.Length; i++)
        {
            if (bs[i] < 128)
                sb.Append((char)bs[i]);
            else
            {
                sb.Append("%" + bs[i++].ToString("x").PadLeft(2, '0'));
                sb.Append("%" + bs[i].ToString("x").PadLeft(2, '0'));
            }
        }
        return sb.ToString();
    }

    private static void LoadConfig()
    {
        var config = LoadTextEx("LocalConfig.xml").ToXMLSecurityElementEx();
        _apkHeader = SafeGetTextByTag(config, "_apkHeader");
        _localWebPath = SafeGetTextByTag(config, "_localWebPath");
        _packagePath = SafeGetTextByTag(config, "_packagePath");
        _packageMd5Path = SafeGetTextByTag(config, "_packageMd5Path");
        _configSubPath = SafeGetTextByTag(config, "_configSubPath");
        _gameLoaderPath = SafeGetTextByTag(config, "_gameLoaderPath");
        _webRootUrl = SafeGetTextByTag(config, "_webRootUrl");
        _apkSubPath = SafeGetTextByTag(config, "_apkSubPath");
        _ftpIp = SafeGetTextByTag(config, "_ftpIp");
        _ftpWebPath = SafeGetTextByTag(config, "_ftpWebPath");
        _ftpSubPath = SafeGetTextByTag(config, "_ftpSubPath");
        _ftpUserName = SafeGetTextByTag(config, "_ftpUserName");
        _ftpPassword = SafeGetTextByTag(config, "_ftpPassword");
        //_serverControlUrl = config.SearchForChildByTag("_serverControlUrl").Text;
        _apkUrl = SafeGetTextByTag(config, "_apkUrl");
        _defaultLanguageTargetPath = SafeGetTextByTag(config, "_defaultLanguageTargetPath");
        _keystorePass = SafeGetTextByTag(config, "_keystorePass");
        _keyaliasPass = SafeGetTextByTag(config, "_keyaliasPass");
        _rtxReceivers = SafeGetTextByTag(config, "_rtxReceivers");
    }

    private static string SafeGetTextByTag(SecurityElement se, string tag)
    {
        if(se != null)
        {
            var tagSe = se.SearchForChildByTag(tag);
            return tagSe == null ? "" : tagSe.Text;
        }
        return "";
    }

    /// <summary>
    /// 将列表对象转换为列表字符串。
    /// </summary>
    /// <typeparam name="T">列表值对象类型</typeparam>
    /// <param name="list">列表对象</param>
    /// <param name="listSpriter">列表分隔符</param>
    /// <returns>列表字符串</returns>
    private static String PackListEx<T>(this List<T> list, Char listSpriter = ',')
    {
        if (list.Count == 0)
            return "";
        else
        {
            StringBuilder sb = new StringBuilder();
            //sb.Append("[");
            foreach (var item in list)
            {
                sb.AppendFormat("{0}{1}", item, listSpriter);
            }
            sb.Remove(sb.Length - 1, 1);
            //sb.Append("]");

            return sb.ToString();
        }

    }

    private static String PackArrayEx<T>(this T[] array, Char listSpriter = ',')
    {
        var list = new List<T>();
        list.AddRange(array);
        return PackListEx(list, listSpriter);
    }

    /// <summary>
    /// 保存 Text 文档。
    /// </summary>
    /// <param name="fileName">文档名称</param>
    /// <param name="text">XML内容</param>
    private static void SaveTextEx(String fileName, String text)
    {
        if (!Directory.Exists(fileName.GetDirectoryNameEx()))
        {
            Directory.CreateDirectory(fileName.GetDirectoryNameEx());
        }
        if (File.Exists(fileName))
        {
            File.Delete(fileName);
        }
        using (FileStream fs = new FileStream(fileName, FileMode.Create))
        {
            using (StreamWriter sw = new StreamWriter(fs))
            {
                //开始写入
                sw.Write(text);
                //清空缓冲区
                sw.Flush();
                //关闭流
                sw.Close();
            }
            fs.Close();
        }
    }

    /// <summary>
    /// 根据文件绝对路径加载文件
    /// </summary>
    /// <param name="filePath">绝对路径</param>
    /// <returns>文件内容</returns>
    private static string LoadTextEx(string filePath)
    {
        if (File.Exists(filePath))
        {
            using (StreamReader sr = File.OpenText(filePath))
            {
                return sr.ReadToEnd();
            }
        }
        else
        {
            return String.Empty;
        }
    }

    private static string GetDirectoryNameEx(this string fileName)
    {
        return fileName.Substring(0, fileName.LastIndexOf('/'));
    }

    private static void ReadFromXml(string xml)
    {
        ParserXML(xml.ToXMLSecurityElementEx());
    }

    private static void ParserXML(SecurityElement rootNode)
    {
        SecurityElement apkNode = rootNode.SearchForChildByTag("ProgramVersion");
        SecurityElement resNode = rootNode.SearchForChildByTag("ResourceVersion");
        SecurityElement glNode = rootNode.SearchForChildByTag("GameLoaderVersion");
        SecurityElement pbNode = rootNode.SearchForChildByTag("ProgressBarBgVersion");

        ProgramVersion = apkNode != null ? new VersionCodeInfo(apkNode.Text) : new VersionCodeInfo("0.0.0.0"); //apk版本
        ResourceVersion = apkNode != null ? new VersionCodeInfo(resNode.Text) : new VersionCodeInfo("0.0.0.0"); //资源版本
        GameLoaderVersion = apkNode != null ? new VersionCodeInfo(glNode.Text) : new VersionCodeInfo("0.0.0.0"); //GameLoader版本
        ProgressBarBgVersion = apkNode != null ? new VersionCodeInfo(pbNode.Text) : new VersionCodeInfo("0.0.0.0"); //背景图版本号
    }

    private static System.Security.SecurityElement ToXMLSecurityElementEx(this string str)
    {
        string xmlStr = str;
#if !UNITY_WEBPLAYER
        if (System.IO.File.Exists(str))
        {
            using (System.IO.FileStream fs = System.IO.File.OpenRead(str))
            {
                System.IO.StreamReader sr = new System.IO.StreamReader(fs);
                xmlStr = sr.ReadToEnd();
            }
        }
#endif
        if (string.IsNullOrEmpty(xmlStr))
            return null;
        GameLoader.Utils.XML.SecurityParser sp = new GameLoader.Utils.XML.SecurityParser();
        sp.LoadXml(xmlStr);
        return sp.ToXml();
    }

    private static String LoadStringFromResource(String fileName)
    {
        var text = Resources.Load(fileName);
        if (text != null)
        {
            var result = text.ToString();
            Resources.UnloadAsset(text);
            return result;
        }
        else
        {
            UnityEngine.Debug.LogError(fileName + " open error");
            return String.Empty;
        }
    }

    private static string GetFilePath(string fileName)
    {
        return fileName.Substring(0, fileName.LastIndexOf('/'));
    }

    private static void SaveVersionInfo()
    {
        StringBuilder tempArgs = new StringBuilder();
        tempArgs.AppendLine("<config>");
        tempArgs.AppendLine("<ProgramVersion>" + ProgramVersion + "</ProgramVersion>");
        tempArgs.AppendLine("<ResourceVersion>" + ResourceVersion + "</ResourceVersion>");
        tempArgs.AppendLine("<GameLoaderVersion>" + GameLoaderVersion + "</GameLoaderVersion>");
        tempArgs.AppendLine("<ProgressBarBgVersion>" + ProgressBarBgVersion + "</ProgressBarBgVersion>");
        tempArgs.Append("</config>");
#if !UNITY_WEBPLAYER
        var verOutputPath = Application.dataPath + "/Resources/VersionInfo.xml";
        if (File.Exists(verOutputPath)) File.Delete(verOutputPath);
        FileUtils.WriterFile(verOutputPath, tempArgs.ToString());
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
#endif
    }

    private static void GetResponse(string Url, Action<string> onDone, Action<HttpStatusCode> onFail)
    {
        HttpWebRequest req = null;
        HttpWebResponse rep = null;
        try
        {
            req = (HttpWebRequest)WebRequest.Create(Url);
            req.Timeout = 15 * 1000;
            req.ReadWriteTimeout = 15 * 1000;
            req.Proxy = null;
            Action action = () =>
            {
                rep = (HttpWebResponse)req.GetResponse();

                if (rep.StatusCode != HttpStatusCode.OK)
                {
                    if (onFail != null) onFail(rep.StatusCode);
                }
                else
                {
                    Stream receiveStream = rep.GetResponseStream();
                    StreamReader readStream = new StreamReader(receiveStream, System.Text.Encoding.UTF8);

                    Char[] read = new Char[1024];
                    int count = readStream.Read(read, 0, 1024);
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    while (count > 0)
                    {
                        String readstr = new String(read, 0, count);
                        sb.Append(readstr);
                        count = readStream.Read(read, 0, 1024);
                    }
                    rep.Close();
                    readStream.Close();
                    if (onDone != null)
                        onDone(sb.ToString());
                }
            };
            action.BeginInvoke(null, null);
        }
        catch (Exception e)
        {
            UnityEngine.Debug.LogError(e.Message);
            if (onFail != null) onFail(HttpStatusCode.NotAcceptable);
        }

    }

}