using System.Linq;
using UnityEngine;
using UnityEditor.AnimatedValues;
using UnityEngine.UI;
using UIExtension;
using System.Collections.Generic;
using TMPro.EditorUtilities;

namespace UnityEditor.UI
{
    [CustomEditor(typeof(TextMeshWrapper), true)]
    [CanEditMultipleObjects]
    public class TextMeshWrapperEditor : TMP_UiEditorPanel
    {
        SerializedProperty _langId;
        public SerializedObject GetSerializedObject(){ return serializedObject;}
        public Object GetTarget() { return target; }

        public void OnEnable()
        {
            base.OnEnable();
            _langId = serializedObject.FindProperty("langId");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.BeginVertical();
            EditorGUILayout.PropertyField(_langId);
            EditorGUILayout.EndVertical();
            serializedObject.ApplyModifiedProperties();
        }
    }
}
