﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Assertions.Comparers;

namespace ArtTech {

    public class PlayableBase : MonoBehaviour {
        private bool isRunning = false;
        public IEnumerator coroutine_MoveToOverTime;
        public Action Callback;
        private bool DisableOnPlayFinish = false;
        IEnumerator MoveTo(Action<float> moveAction, float time) {
            if (!isRunning) {
                isRunning = true;
                float i = 0.0f;
                float rate = 1 / time;
                while (i < 1) {
                    i += Time.deltaTime * rate;
                    moveAction(i);
                    yield return 0;
                }
                isRunning = false;
                if (Callback != null)
                    Callback();
            }
            yield return 0;
            if (DisableOnPlayFinish) this.enabled = false;
        }

        protected void PlayBase(Action<float> moveAction, float duration, Action callback = null, bool disableOnPlayFinish = false) {
            if (coroutine_MoveToOverTime != null)
                StopCoroutine(coroutine_MoveToOverTime);
            Callback = callback;
            DisableOnPlayFinish = disableOnPlayFinish;
            coroutine_MoveToOverTime = MoveTo((ft) => {
                moveAction(ft);
            }, duration);
            StartCoroutine(coroutine_MoveToOverTime);
        }


    }
}
