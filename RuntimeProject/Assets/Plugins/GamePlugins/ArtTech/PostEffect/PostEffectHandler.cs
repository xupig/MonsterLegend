﻿using System;
using UnityEngine;
using ArtTech.ImageEffects;


namespace ArtTech {
    public class PostEffectHandler : PostEffectHandlerBase {
        static PostEffectHandler _instance;
        public static new PostEffectHandler Instance {
            get {
                if (_instance == null)
                    _instance = new PostEffectHandler();
                return _instance;
            }
        }


        Camera _currentCamera;
        PostEffectProxy _currentProxy;
        void SetCurrentCamera(Camera camera) {
            if (camera == _currentCamera)
                return;

            _currentCamera = camera;
            _currentProxy = camera.GetComponent<PostEffectProxy>();
        }



        public override void InitAll(Camera camera) {
            _currentCamera = camera;

            camera.gameObject.AddComponent<PostEffect>();
            camera.gameObject.AddComponent<ScreenWhite>().enabled = false;
            camera.gameObject.AddComponent<ScreenFog>().enabled = false;

            _currentProxy = camera.gameObject.AddComponent<PostEffectProxy>();
        }

        public override bool GetPostEffectEnabled(Camera camera) {
            SetCurrentCamera(camera);
            return _currentProxy.Enabled;
        }
        public override void SetPostEffectEnabled(Camera camera, bool enabled) {
            SetCurrentCamera(camera);
            _currentProxy.Enabled = enabled;
        }

        public override void SetBloomEnabled(Camera camera, bool enabled) {
            SetCurrentCamera(camera);
            _currentProxy.BloomEnabled = enabled;
        }
        public override void SetBloomThreshold(Camera camera, float threshold) {
            SetCurrentCamera(camera);
            _currentProxy.BloomThreshold = threshold;
        }
        public override void SetBloomMaxBrightness(Camera camera, float maxBrightness) {
            SetCurrentCamera(camera);
            _currentProxy.BloomMaxBrightness = maxBrightness;
        }
        public override void SetBloomSoftKnee(Camera camera, float softKnee) {
            SetCurrentCamera(camera);
            _currentProxy.BloomSoftKnee = softKnee;
        }
        public override void SetBloomRadius(Camera camera, float radius) {
            SetCurrentCamera(camera);
            _currentProxy.BloomRadius = radius;
        }
        public override void SetBloomIntensity(Camera camera, float intensity) {
            SetCurrentCamera(camera);
            _currentProxy.BloomIntensity = intensity;
        }

        public override void SetScreenDarkLayers(Camera camera, int layers) {
            SetCurrentCamera(camera);
            _currentProxy.ScreenDarkLayers = layers;
        }


        public override void SetScreenWhiteEnabled(Camera camera, bool enabled) {
            SetCurrentCamera(camera);
            _currentProxy.ScreenWhiteEnabled = enabled;
        }
        public override void SetScreenWhiteColor(Camera camera, Color color) {
            SetCurrentCamera(camera);
            _currentProxy.ScreenWhiteColor = color;
        }
        public override void SetScreenWhiteAlpha(Camera camera, float alpha) {
            SetCurrentCamera(camera);
            _currentProxy.ScreenWhiteAlpha = alpha;
        }

        public override void SetRadialBlurEnabled(Camera camera, bool enabled) {
            SetCurrentCamera(camera);
            _currentProxy.RadialBlurEnabled = enabled;
        }

        public override bool GetRadialBlurEnabled(Camera camera) {
            SetCurrentCamera(camera);
            return _currentProxy.RadialBlurEnabled;
        }

        public override void SetRadialBlurDarkness(Camera camera, float darkness) {
            SetCurrentCamera(camera);
            _currentProxy.RadialBlurDarkness = darkness;
        }

        public override void SetRadialBlurStrength(Camera camera, float strength) {
            SetCurrentCamera(camera);
            _currentProxy.RadialBlurStrength = strength;
        }

        public override void SetRadialBlurSharpness(Camera camera, float sharpness) {
            SetCurrentCamera(camera);
            _currentProxy.RadialBlurSharpness = sharpness;
        }

        public override void SetRadialBlurVignetteSize(Camera camera, float vignetteSize) {
            SetCurrentCamera(camera);
            _currentProxy.RadialBlurVignetteSize = vignetteSize;
        }


        public override void PlayDOF(Camera camera, DofParameter StartParameter, DofParameter endParameter, float duration) {
        }

        public override void PlayGodRay(Camera camera, GodRayParameter startParameter, GodRayParameter endParameter, float duration) {
        }

        public override void PlayHdrBloom(Camera camera, HdrBloomParameter startParameter, HdrBloomParameter endParameter, float duration) {
        }

        public override void PlayScreenWhite(Camera camera, ScreenWhiteParameter startParameter, ScreenWhiteParameter endParameter, float duration, Action finish = null, bool disableOnPlayFinish = false) {
            SetCurrentCamera(camera);
            _currentProxy.PlayScreenWhite(startParameter, endParameter, duration, finish, disableOnPlayFinish);
        }

        public override void PlayRadialBlur(Camera camera, RadiaBlurParameter startParameter, RadiaBlurParameter endParameter, float duration) {
        }


        public override void PlayRadialBlur(Camera camera, float startDarkness, float startStrength, float startSharpness, float startVignetteSize,
                    float endDarkness, float endStrength, float endSharpness, float endVignetteSize,
                    float duration, Action finish, bool disableOnPlayFinish) {

            SetCurrentCamera(camera);
            _currentProxy.PlayRadialBlur(startDarkness, startStrength, startSharpness, startVignetteSize,
                        endDarkness, endStrength, endSharpness, endVignetteSize,
                        duration, finish, disableOnPlayFinish);
        }

        public override void PlayScreenDark(Camera camera, float start, float end, float duration, Action finish, bool disableOnPlayFinish) {
            SetCurrentCamera(camera);
            _currentProxy.PlayScreenDark(start, end, duration, finish, disableOnPlayFinish);
        }

        public override void PlayScreenWhite(Camera camera, Color startColor, float startAlpha, Color endColor, float endAlpha, float duration, Action finish, bool disableOnPlayFinish) {
            SetCurrentCamera(camera);
            _currentProxy.PlayScreenWhite(startColor, startAlpha, endColor, endAlpha, duration, finish, disableOnPlayFinish);
        }

        public override void PlayScreenWhite(Camera camera, float start, float end, float duration, Action finish, bool disableOnPlayFinish)
        {
            SetCurrentCamera(camera);
            _currentProxy.PlayScreenWhite(start, end, duration, finish, disableOnPlayFinish);
        }

        public override void PlayScreenFog(Camera camera, Color fogColorStart, float fogStartStart, float fogEndStart,
                    Color fogColorEnd, float fogStartEnd, float fogEndEnd,
                    float duration, Action finish, bool disableOnPlayFinish) {
            SetCurrentCamera(camera);
            _currentProxy.PlayScreenFog(fogColorStart, fogStartStart, fogEndStart,
                fogColorEnd, fogStartEnd, fogEndEnd, duration, finish, disableOnPlayFinish);
        }
    }
}

