﻿using UnityEngine;
using System.Collections;
using ArtTech.ImageEffects;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

namespace ArtTech {
#region GraphicsQuality
    public static class GraphicsQuality {

        public enum Quality {
            High = 0,
            Medium,
            Low,
            Lowest
        }

        private static Quality _quality = Quality.High;
        public static Quality QualityLevel {
            get { return _quality; }
            set { if (_quality != value) { _quality = value; OnChangeQualityLevel(); } }
        }


        private static int _autoQualityLevel = -1;
        public static Quality AutoQualityLevel {
            get {
#if UNITY_EDITOR
                return Quality.High;
#elif UNITY_IOS
                if (Device.generation >= DeviceGeneration.iPhone7)
                    return Quality.High;
                else if (Device.generation >= DeviceGeneration.iPhone6)
                    return Quality.Medium;
                else
                    return Quality.Low;
#elif UNITY_ANDROID
                return (Quality)_autoQualityLevel;
#else
                return Quality.Low;
#endif
            }
        }

        public static bool DoesNeedBenchmark {
            get {
#if !UNITY_EDITOR && UNITY_ANDROID
                return _autoQualityLevel == -1;
#else
                return false;
#endif
            }
        }

        static int _originalScreenWidth;
        static int _originalScreenHeight;
        public static void Init() {
#if !UNITY_EDITOR
            InitSceenRes();
#if UNITY_ANDROID
            LoadAutoQualityLevel();
#endif
#endif
        }

        static void InitSceenRes() {
            _originalScreenWidth = Screen.width;
            _originalScreenHeight = Screen.height;
            Debug.Log("[GraphicsQuality.Init] original screen res " + _originalScreenWidth + " X " + _originalScreenHeight);

            SetScreenResolution(1080);      
        }

        public static void SetScreenResolution(int expectedHeight) {
            if (expectedHeight < 640) {
                Debug.LogError("[GraphicsQuality.SetScreenResolution] height must be greater than 640!");
                return;
            }

            float aspect = _originalScreenWidth / (float)_originalScreenHeight;
            int currentWidth = Screen.width;
            int currentHeight = Screen.height;

            if (currentHeight != expectedHeight) {
                if (_originalScreenHeight > expectedHeight) {
                    int newHeight = expectedHeight;
                    int newWidth = (int)(newHeight * aspect);

                    if (newWidth > _originalScreenWidth) {
                        newWidth = _originalScreenWidth;
                        newHeight = (int)(newWidth / aspect);
                    }

                    Screen.SetResolution(newWidth, newHeight, true);
                    Debug.Log("[GraphicsQuality.SetScreenResolution] screen res has been changed to " + newWidth + " X " + newHeight);
                } else {
                    if (currentHeight != _originalScreenHeight) {
                        Screen.SetResolution(_originalScreenWidth, _originalScreenHeight, true);
                        Debug.Log("[GraphicsQuality.SetScreenResolution] screen res has been changed to " + _originalScreenWidth + " X " + _originalScreenHeight);
                    }
                }
            }
        }

        public static void SetAutoQualityLevelByFps(float fps) {
            if (fps > 55) {
                _autoQualityLevel = (int)Quality.High;
            } else if (fps > 30) {
                _autoQualityLevel = (int)Quality.Medium;
            } else if (fps > 20) {
                _autoQualityLevel = (int)Quality.Low;
            } else {
                _autoQualityLevel = (int)Quality.Lowest;
            }

            SaveAutoQualityLevel();
        }

        static void LoadAutoQualityLevel() {
            _autoQualityLevel = PlayerPrefs.GetInt("QJZRArtTech_AutoQualityLevel", -1);
        }

        static void SaveAutoQualityLevel() {
            PlayerPrefs.SetInt("QJZRArtTech_AutoQualityLevel", _autoQualityLevel);
        }







        static void OnChangeQualityLevel() {


        }

    }
#endregion



#region PostEffectQuality
    public static class PostEffectQuality {
        public enum Quality {
            High = 0,
            Medium,
            Low
        }

        private static Quality _quality = Quality.High;
        public static Quality QualityLevel {
            get { return _quality; }
            set { if (_quality != value) { _quality = value; OnChangeQualityLevel(); } }
        }


        static void OnChangeQualityLevel() {
            PostEffectProxy[] proxies = Object.FindObjectsOfType<PostEffectProxy>();
            for (int i = 0; i < proxies.Length; i++) {
                proxies[i].OnChangeQualityLevel();
            }
        }
    }
#endregion





#region ShaderQuality
    public static class ShaderQuality {
        public enum Quality {
            High = 0,
            Medium,
            Low
        }

        private static Quality _quality = Quality.High;
        public static Quality QualityLevel {
            get { return _quality; }
            set { if (_quality != value) { _quality = value; OnChangeQualityLevel(); } }
        }


        static void OnChangeQualityLevel() {
            switch (_quality) {
                case Quality.High:
                    EnableShaderKeyword("SHADER_QUALITY_HIGH");
                    DisableShaderKeyword("SHADER_QUALITY_MEDIUM");
                    DisableShaderKeyword("SHADER_QUALITY_LOW");
                    Shader.globalMaximumLOD = 1000;  // 支持Distort
                    break;

                case Quality.Medium:
                    EnableShaderKeyword("SHADER_QUALITY_MEDIUM");
                    DisableShaderKeyword("SHADER_QUALITY_HIGH");
                    DisableShaderKeyword("SHADER_QUALITY_LOW");
                    Shader.globalMaximumLOD = 400;  // 支持Bumped Specular
                    break;

                case Quality.Low:
                    EnableShaderKeyword("SHADER_QUALITY_LOW");
                    DisableShaderKeyword("SHADER_QUALITY_HIGH");
                    DisableShaderKeyword("SHADER_QUALITY_MEDIUM");
                    Shader.globalMaximumLOD = 300;  // 只支持Diffuse(TODO: Shader还需要调整)
                    break;
            }
        }

        public static void EnableShaderKeyword(string keyword) {
            Shader.EnableKeyword(keyword);
        }

        public static void DisableShaderKeyword(string keyword) {
            Shader.DisableKeyword(keyword);
        }
    }
#endregion




#region WaterQuality
    public static class WaterQuality {
        public enum Quality {
            High = 0,
            Low
        }

        private static Quality _quality = Quality.High;
        public static Quality QualityLevel {
            get { return _quality; }
            set { if (_quality != value) { _quality = value; OnChangeQualityLevel(); } }
        }

        static void OnChangeQualityLevel() {
            Water[] waterObjs = Object.FindObjectsOfType<Water>();
            for (int i = 0; i < waterObjs.Length; i++)
                waterObjs[i].OnChangeQualityLevel();
        }
    }
#endregion

}