﻿using UnityEngine;
using System.Collections;

namespace ArtTech {
    [ExecuteInEditMode]
    [AddComponentMenu("ArtTech/Water")]
    [DisallowMultipleComponent]
    public class Water : MonoBehaviour {
        private void OnEnable() {
            OnChangeQualityLevel();
        }

        private void OnDestroy() {
            Camera mainCam = Camera.main;
            if (mainCam != null && mainCam.depthTextureMode != DepthTextureMode.None) {
                mainCam.depthTextureMode = DepthTextureMode.None;
            }
        }

        public void OnChangeQualityLevel() {
            Camera mainCam = Camera.main;
            if (mainCam == null)
                return;

            switch (WaterQuality.QualityLevel) {
                case WaterQuality.Quality.High:
                    mainCam.depthTextureMode |= DepthTextureMode.Depth;
                    break;

                case WaterQuality.Quality.Low:
                    mainCam.depthTextureMode = DepthTextureMode.None;
                    break;
            }
        }
    }

}

