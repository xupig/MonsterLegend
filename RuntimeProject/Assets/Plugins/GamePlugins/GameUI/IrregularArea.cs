﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class IrregularArea : Graphic
{
    public bool isEditing = false;

#if UNITY_EDITOR
    // Update is called once per frame
    void Update()
    {
        if (isEditing)SetAllDirty();
    }
#endif

    protected override void OnPopulateMesh(VertexHelper vertexHelper)
    {
        Color32 color32 = color;
        vertexHelper.Clear();

        List<Transform> vertexes = new List<Transform>();
        foreach (Transform child in transform)
        {
            if (child.name.StartsWith("vertex_"))
            {
                vertexes.Add(child);
                vertexHelper.AddVert(child.localPosition, color32, new Vector2(0f, 0f));
            }
        }
        if (vertexes.Count < 3) return;

        int triangleCount = vertexes.Count - 2;
        for (int i = 0; i < triangleCount; i++)
        {
            vertexHelper.AddTriangle(0, i+1, i+2);
        }
         
    }
}