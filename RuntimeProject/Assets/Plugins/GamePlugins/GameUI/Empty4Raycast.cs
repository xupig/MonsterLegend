﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace UnityEngine.UI
{
    public class Empty4Raycast : MaskableGraphic
    {
        protected Empty4Raycast()
        {
#if UNITY_5
            useLegacyMeshGeneration = false;
#endif
        }

#if UNITY_4_7
        protected override void OnFillVBO(List<UIVertex> toFill)
#elif UNITY_5
        protected override void OnPopulateMesh(VertexHelper toFill)
#endif
        {
            toFill.Clear();
        }

//以下是测试点击取用用的代码，用完可以去掉

        protected override void Start()
        {
            base.Start();
            if (UIExtension.ImageWrapper.showAllArea && Application.isEditor && this.raycastTarget)
            {
                GameObject showAreaGO = new GameObject("_raycastArea");
                showAreaGO.transform.SetParent(this.transform);
                var rect = showAreaGO.GetComponent<RectTransform>();
                if (rect == null)
                {
                    rect = showAreaGO.AddComponent<RectTransform>();
                }
                var iw = showAreaGO.AddComponent<UIExtension.ImageWrapper>();
                iw.raycastTarget = false;
                rect.pivot = new Vector2(0.5f, 0.5f);
                rect.anchoredPosition3D = new Vector3(0, 0, 0);
                rect.sizeDelta = this.rectTransform.sizeDelta;
                rect.localScale = this.rectTransform.localScale * 1.05f;
                Shader shader = ShaderUtils.ShaderRuntime.loader.Find("UGUI/UGUI_OUTLINE");
                if (shader != null)
                {
                    var material = new Material(shader);
                    var color = Color.green;
                    color.a = 0.5f;
                    material.SetColor("_Color", color);
                    material.SetFloat("_ShowArea", 0.5f);
                    iw.material = material;
                }
            }
        }
    }
}