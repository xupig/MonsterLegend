﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace UIExtension
{
    [AddComponentMenu("XGameUI/TextWrapper", 11)]
    public class TextWrapper : Text, IGameStyleComponent
    {
        public static Func<string, Object> GetFont;
        public static Func<int, string> GetLanguage;

        //语言配置ID
        [SerializeField]
        public int langId = -1;
        public string fontKey;
        /// <summary>
        /// 当图片或文本的Size改变了，才需要调用OnRectTransformDimensionsChange
        /// </summary>
        private bool _isDimensionsChanged = true;
        private bool _canSetAllDirty = true;
        private bool _continuousDimensionDirty = false;

        [SerializeField]
        public bool custom = false;

        [SerializeField]
        public string gameStyle = string.Empty;

        public bool IsCustom() { return custom; }
        public string GetGameStyle() { return gameStyle; }
        public void SetGameStyle(Object styleObject)
        {
            TextWrapper styleText = styleObject as TextWrapper;
            this.custom = false;
            this.gameStyle = styleText.gameStyle;
            this.fontStyle = styleText.fontStyle;
            this.fontSize = styleText.fontSize;
            this.lineSpacing = styleText.lineSpacing;
            this.supportRichText = styleText.supportRichText;
            this.alignment = styleText.alignment;
            this.alignByGeometry = styleText.alignByGeometry;
            this.horizontalOverflow = styleText.horizontalOverflow;
            this.verticalOverflow = styleText.verticalOverflow;
            this.resizeTextForBestFit = styleText.resizeTextForBestFit;
            this.color = styleText.color;
            this.raycastTarget = styleText.raycastTarget;
        }

        protected override void Awake()
        {
            base.Awake();
            if (this.langId != -1)
            {
                if (GetLanguage != null)
                {
                    this.text = GetLanguage(this.langId);
                }
            }
        }

        protected override void OnEnable()
        {
            if(font == null && string.IsNullOrEmpty(fontKey) == false)
            {
                _canSetAllDirty = false;
                if(GetFont != null)
                {
                    font = GetFont(fontKey) as Font;
                }
                _canSetAllDirty = true;
            }
            //优化性能：在OnEnable函数中，只有上一次m_ShouldRecalculate为true的时候，才将其设为true，重新计算遮罩值
            //Ari bool lastShouldRecalculate = m_ShouldRecalculate;
            base.OnEnable();
			/*Ari
            if(lastShouldRecalculate == false)
            {
                m_ShouldRecalculate = false;
            }*/
        }

        public override void SetAllDirty()
        {
            if(_canSetAllDirty == true)
            {
                base.SetAllDirty();
            }
        }

        public override void SetLayoutDirty()
        {

        }

        public void ForceRecalculateMaskable()
        {
            //Ari m_ShouldRecalculate = true;
        }

        protected override void OnBeforeTransformParentChanged()
        {
            GraphicRegistry.UnregisterGraphicForCanvas(canvas, this);
        }

        protected override void OnRectTransformDimensionsChange()
        {
            if(_isDimensionsChanged == true || _continuousDimensionDirty == true)
            {
                base.OnRectTransformDimensionsChange();
                _isDimensionsChanged = false;
            }
        }

        public void SetDimensionsDirty()
        {
            _isDimensionsChanged = true;
        }

        public void SetContinuousDimensionDirty(bool value)
        {
            _continuousDimensionDirty = value;
        }

    }
}
