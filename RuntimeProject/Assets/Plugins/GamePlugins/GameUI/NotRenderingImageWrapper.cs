﻿#region 模块信息
/*==========================================
// 文件名：NotRenderingImageWrapper
// 命名空间: Assets.Plugins.GamePlugins.GameUI
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/17 16:34:28
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UIExtension
{
    public class NotRenderingImageWrapper:ImageWrapper
    {
        public override void SetAllDirty()
        {
            //blank
        }

        public override void SetLayoutDirty()
        {
            //blank
        }

        public override void SetVerticesDirty()
        {
            //blank
        }

        public override void SetMaterialDirty()
        {
            //blank
        }

        protected override void OnFillVBO(List<UIVertex> vbo)
        {
            //blank;
        }
    }
}
