﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace UIExtension
{
    public class TMPAssets : MonoBehaviour
    {
        public TMP_FontAsset fontAsset;
        public Material[] fontMatrials;
        public TMP_SpriteAsset spriteAsset;

        Dictionary<string, Material> _materialMap = null;

        public Material GetMaterial(string key)
        {
            if (_materialMap == null)
            {
                _materialMap = new Dictionary<string, Material>();
                for (int i = 0; i < fontMatrials.Length; i++)
                {
                    Material material = fontMatrials[i];
                    _materialMap[fontMatrials[i].name] = material;
                    if (Application.isEditor && Shader.Find(material.shader.name) != null)
                    {
                        material.shader = Shader.Find(material.shader.name);
                    }
                }
            }
            if (string.IsNullOrEmpty(key)) return null;
            Material result = null;
            _materialMap.TryGetValue(key, out result);
            return result;
        }
       
    }
}
