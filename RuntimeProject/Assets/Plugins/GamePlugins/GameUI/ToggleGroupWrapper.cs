﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;
using System.Collections.Generic;
using UnityEngine.Events;

namespace UIExtension
{
    public class ToggleGroupWrapperEvent : UnityEvent<int>
    { }

    [AddComponentMenu("XGameUI/ToggleGroupWrapper", 11)]
    public class ToggleGroupWrapper : ToggleGroup
    {
        private List<ToggleWrapper> _toggleList;
        private int _selectedIndex = -1;
        private bool _canRepeatClick = false;
        private ToggleGroupWrapperEvent _onSelectedIndexChanged;
        public ToggleGroupWrapperEvent onSelectedIndexChanged
        {
            get
            {
                if (_onSelectedIndexChanged == null)
                {
                    _onSelectedIndexChanged = new ToggleGroupWrapperEvent();
                }
                return _onSelectedIndexChanged;
            }
        }

        override protected void Awake()
        {
            base.Awake();
            InitToggleList();
        }

        private void InitToggleList()
        {
            _selectedIndex = 0;
            _toggleList = new List<ToggleWrapper>();
            int toggleIndex = 0;
            for (int i = 0; i < this.transform.childCount; i++)
            {
                ToggleWrapper toggle = transform.GetChild(i).GetComponent<ToggleWrapper>();
                if (toggle != null)
                {
                    toggle.group = this;
                    toggle.isOn = false;
                    if (toggleIndex == _selectedIndex)
                    {
                        toggle.isOn = true;
                    }
                    toggle.ToggleCheckmark();
                    toggle.onValueChanged.AddListener(OnToggleValueChanged);
                    _toggleList.Add(toggle);
                    base.RegisterToggle(toggle);
                    toggleIndex++;
                }
            }
        }

        private void OnToggleValueChanged(bool isOn)
        {
            if (isOn == true)
            {
                int oldSelectIndex = _selectedIndex;
                UpdateSelectIndex();
                if (_canRepeatClick)
                {
                    TriggerChangeEvent();
                }
                else
                {
                    if (oldSelectIndex != _selectedIndex)
                    {
                        TriggerChangeEvent();
                    }
                }
            }
        }

        private void TriggerChangeEvent()
        {
            if (_onSelectedIndexChanged != null)
            {
                onSelectedIndexChanged.Invoke(_selectedIndex);
            }
        }

        public List<ToggleWrapper> GetToggleList()
        {
            return _toggleList;
        }

        public ToggleWrapper GetToggle(int index)
        {
            return _toggleList.Count > index ? _toggleList[index]:null;
        }

        public int SelectIndex
        {
            get
            {
                return _selectedIndex;
            }
            set
            {
                if (value >= 0 && value <= _toggleList.Count - 1)
                {
                    if (_selectedIndex >= 0 && _selectedIndex <= _toggleList.Count - 1)
                    {
                        _toggleList[_selectedIndex].isOn = false;
                    }
                    _selectedIndex = value;
                    _toggleList[_selectedIndex].isOn = true;
                    _toggleList[_selectedIndex].ToggleCheckmark();
                }
            }
        }

        /// <summary>
        /// 这个接口应该用到
        /// <param name="toggle">Toggle.</param>
        public void RegisterToggle(ToggleWrapper toggle)
        {
            toggle.group = this;
            base.RegisterToggle(toggle);
            UpdateToggleList();
            UpdateSelectIndex();
        }

        /// <summary>
        /// 这个接口应该用到
        /// <param name="toggle">Toggle.</param>
        public void UnregisterToggle(ToggleWrapper toggle)
        {
            toggle.group = null;
            base.UnregisterToggle(toggle);
            UpdateToggleList();
            UpdateSelectIndex();
        }

        private void UpdateToggleList()
        {
            _toggleList = new List<ToggleWrapper>();
            ToggleWrapper[] toggleArray = GetComponentsInChildren<ToggleWrapper>();
            foreach (ToggleWrapper toggle in toggleArray)
            {
                toggle.onValueChanged.RemoveListener(OnToggleValueChanged);
                if (toggle.group != null)
                {
                    _toggleList.Add(toggle);
                    toggle.onValueChanged.AddListener(OnToggleValueChanged);
                }
            }
        }

        private void UpdateSelectIndex()
        {
            _selectedIndex = -1;
            for (int i = 0; i < _toggleList.Count; i++)
            {
                ToggleWrapper toggle = _toggleList[i];
                if (toggle.isOn == true)
                {
                    _selectedIndex = i;
                    break;
                }
            }
        }

        public bool Visible
        {
            get
            {
                return gameObject.activeSelf;
            }
            set
            {
                gameObject.SetActive(value);
            }
        }

        /// <summary>
        /// 重复选择是否抛事件
        /// </summary>
        public bool canRepeatClick
        {
            set
            {
                _canRepeatClick = value; 
            }
        }
    }
}
