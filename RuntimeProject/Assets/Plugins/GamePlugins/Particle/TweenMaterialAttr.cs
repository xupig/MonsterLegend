﻿using System;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Renderer))]
public class TweenMaterialAttr : MonoBehaviour
{
    private Renderer m_renderer;
    //private Material targetMat;
    private bool needUpdate;
    private bool curNeedUpdate;

    private float fadeInBeginTime;
    private float fadeInEndTime;
    private float fadeOutBeginTime;
    private float fadeOutEndTime;

    //private float startTime;
    //private float endTime;
    private float curDeltaTime;

    private bool hasSetFadeInEnd;
    private bool hasSetFadeOutEnd;

    private bool hasFinishStartDelay;

    public bool IsLoop;
    public float StartDelay;
    public float Duration;

    public float FadeInBegin = 0;
    public float FadeInEnd = 0.3f;
    public float FadeOutBegin = 0.7f;
    public float FadeOutEnd = 1;

    public float FadeInBeginValue = 0;
    public float FadeInEndValue = 1;
    public float FadeOutBeginValue = 1;
    public float FadeOutEndValue = 0;

    public string TargetProp = "_dissolve";
    MaterialPropertyBlock prop = null;

    private void Start()
    {
        //Debug.Log();
        Init();
    }

    public void Init()
    {
        if (prop == null)
            prop = new MaterialPropertyBlock();
        if (!m_renderer)
            m_renderer = GetComponent<Renderer>();
        if (m_renderer.sharedMaterial.HasProperty(TargetProp))
        {
            if (FadeInBegin == FadeInEnd&& FadeInEnd == FadeOutBegin && FadeOutBegin == FadeOutEnd)
            {
                needUpdate = false;
            }
            else
            {
                needUpdate = true;
                fadeInBeginTime = FadeInBegin * Duration;
                fadeInEndTime = FadeInEnd * Duration;
                fadeOutBeginTime = FadeOutBegin * Duration;
                fadeOutEndTime = FadeOutEnd * Duration;
            }
            //startTime = Time.realtimeSinceStartup;
            //endTime = Time.realtimeSinceStartup + duration;
            ResetPara();
        }
    }

    private void Update()
    {
        if (!curNeedUpdate)
            return;
        curDeltaTime += Time.deltaTime;

        if (!hasFinishStartDelay)
        {
            if (curDeltaTime < StartDelay)
            {
                return;
            }
            else
            {
                hasFinishStartDelay = true;
                curDeltaTime = 0;
            }
        }

        if (curDeltaTime > fadeOutEndTime)
        {
            if (!hasSetFadeOutEnd)
            {
                //if (targetMat)
                m_renderer.GetPropertyBlock(prop);
                prop.SetFloat(TargetProp, FadeOutEndValue);
                m_renderer.SetPropertyBlock(prop);
                //targetMat.SetFloat(TargetProp, FadeOutEndValue);
                hasSetFadeOutEnd = true;
            }
        }
        else if (curDeltaTime > fadeOutBeginTime)
        {
            var timePercentage = (curDeltaTime - fadeOutBeginTime) / (fadeOutEndTime - fadeOutBeginTime);
            //if (targetMat)
            m_renderer.GetPropertyBlock(prop);
            prop.SetFloat(TargetProp, (FadeOutEndValue - FadeOutBeginValue) * timePercentage + FadeOutBeginValue);
            m_renderer.SetPropertyBlock(prop);
            //targetMat.SetFloat(TargetProp, (FadeOutEndValue - FadeOutBeginValue) * timePercentage + FadeOutBeginValue);
        }
        else if (curDeltaTime > fadeInEndTime)
        {
            var timePercentage = (curDeltaTime - fadeInEndTime) / (fadeOutBeginTime - fadeInEndTime);
            //if (targetMat)
            m_renderer.GetPropertyBlock(prop);
            prop.SetFloat(TargetProp, (FadeOutBeginValue - FadeInEndValue) * timePercentage + FadeInEndValue);
            m_renderer.SetPropertyBlock(prop);
            //if (!hasSetFadeInEnd)
            //{
            //    //if (targetMat)
            //    particleSysRenderer.GetPropertyBlock(prop);
            //    prop.SetFloat(TargetProp, FadeInEndValue);
            //    particleSysRenderer.SetPropertyBlock(prop);
            //    //targetMat.SetFloat(TargetProp, FadeInEndValue);
            //    hasSetFadeInEnd = true;
            //}
        }
        else if (curDeltaTime > fadeInBeginTime)
        {
            var timePercentage = (curDeltaTime - fadeInBeginTime) / (fadeInEndTime - fadeInBeginTime);
            //if (targetMat)
            m_renderer.GetPropertyBlock(prop);
            prop.SetFloat(TargetProp, (FadeInEndValue - FadeInBeginValue) * timePercentage + FadeInBeginValue);
            m_renderer.SetPropertyBlock(prop);
            //targetMat.SetFloat(TargetProp, (FadeInEndValue - FadeInBeginValue) * timePercentage + FadeInBeginValue);
        }

        if (curDeltaTime > Duration)
        {
            if (IsLoop)
            {
                ResetPara(false);
            }
            else
            {
                curNeedUpdate = false;
            }
        }
    }

    private void OnEnable()
    {
        ResetPara();
    }

    private void ResetPara(bool forceResetProp = true)
    {
        hasSetFadeInEnd = false;
        hasSetFadeOutEnd = false;
        curNeedUpdate = needUpdate;
        curDeltaTime = 0;
        if (m_renderer && (forceResetProp || !IsLoop))
        {
            m_renderer.GetPropertyBlock(prop);
            prop.SetFloat(TargetProp, FadeInBeginValue);
            m_renderer.SetPropertyBlock(prop);
        }
        //if (targetMat)
        //    targetMat.SetFloat(TargetProp, FadeInBeginValue);
    }
}