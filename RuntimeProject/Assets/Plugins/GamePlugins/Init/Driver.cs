﻿//#define IPHONE_TEST
using UnityEngine;
using System;
using System.IO;
using System.Reflection;
using GameLoader;
using Init.Update;
using Default;
using System.Collections;
using GameLoader.Utils;
using ArtTech;
#if UNITY_WEBPLAYER
    using GameMain.GlobalManager;
#endif

/// <summary>
/// 应用启动入口
/// </summary>
public class Driver : MonoBehaviour
{
    public static Driver Instance;
    private int beginTime;
    private UpdateMgr updateMgr;
    private DefaultConfirmBox confirmBox;
    private DefaultProgressBar progressBar;
    public GameObject benchmarkGO = null;
    private bool isBenchmarkState = false;

    public const string GameLoaderName = "bytes$gameloader.bytes.u";


    void Awake()
    {
        Instance = this;
        try
        {
            DriverLogger.Info("[Driver] ----------------------------- Awake ----------------------------- [Start]");
            LogUploader.GetInstance().DriverInstance = this;
            HttpWrappr.instance = new HttpWrappr(this);
            confirmBox = new DefaultConfirmBox();
            progressBar = new DefaultProgressBar();
            progressBar.Show();
            updateMgr = new UpdateMgr(confirmBox, progressBar);
            DefaultLanguage.LoadLocal();
            progressBar.UpdateTip(DefaultLanguage.GetString("init"));
            TryBenchmark(() =>
            {
                isBenchmarkState = false;
                if (benchmarkGO != null) {
                    GameObject.Destroy(benchmarkGO);
                    benchmarkGO = null;
                }
                LoadLoader();
            });
        }
        catch (Exception ex)
        {
            DriverLogger.Error(string.Format("[Driver] Awake[Fail],reason:{0}", ex.StackTrace));
        }
    }

    void TryBenchmark(Action callback) {       
        GraphicsQuality.Init();
        if (GraphicsQuality.DoesNeedBenchmark) {
            if (benchmarkGO) {
                //DontDestroyOnLoad(benchmarkGO);
                isBenchmarkState = true;
                benchmarkGO.SetActive(true);
                Benchmark.OnEnd = () => {
                    //benchmarkGO.SetActive(false);                  
                    callback();
                };
            } else {
                callback();
            }
        } else {
            callback();
        }
    }

    void Update()
    {
        if (isBenchmarkState)
            return;

        UnityPropUtils.deltaTime = Time.deltaTime;
        UnityPropUtils.realtimeSinceStartup = Time.realtimeSinceStartup;
        UnityPropUtils.time = Time.time;
        if (progressBar != null) progressBar.OnUpdate();
    }

#if UNITY_WEBPLAYER

    bool isShowLog = false;
    void OnGUI()
    {
        if (WebLoginInfoManager.GetInstance().showWebLogFlag == 0)
        {
            return;
        }
        if (GUI.Button(new Rect(0, 0, 10, 10), "log"))
        {
            isShowLog = !isShowLog;
        }
        if (isShowLog)ShowLog();
    }

    void ShowLog()
    {
        System.Text.StringBuilder inputString = new System.Text.StringBuilder(500000);
        var logs = DriverLogger.lastLogs;
        /*
        for (int i = 0; i < logs.Count; i++)
        {
            inputString.Append(logs[i]);
            inputString.Append("\n");
        }*/
        inputString.Append("==============================================================\n");
        logs = GameLoader.Utils.LoggerHelper.lastLogs;
        for (int i = 0; i < logs.Count; i++)
        {
            inputString.Append(logs[i]);
            inputString.Append("\n");
        }
        GUI.TextField(new Rect(0, 0, 1280, 500), inputString.ToString());
    }
#endif

    void OnApplicationQuit()
    {
        DriverLogger.Release();
    }

    private void LoadLoader()
    {
#if UNITY_WEBPLAYER
        updateMgr.Update(LoadGameLoaderAtWeb);
        return;
#endif
#if UNITY_IPHONE
#if UNITY_EDITOR
                DriverLogger.Info("[Driver] [Iphone平台 Editor]");
                updateMgr.Update(LoadGameLoaderAtIOS);
                //DriverLogger.Release();
#else
                DriverLogger.Info("[Driver] [Iphone平台]");
                updateMgr.Update(LoadGameLoaderAtIOS);
                //DriverLogger.Release();
#endif
#else
#if !UNITY_EDITOR && UNITY_ANDROID
                updateMgr.Update(LoadGameLoaderAtAndorid);
#else
#if (UNITY_EDITOR && UNITY_ANDROID)
        updateMgr.Update(() =>
        {
            DriverLogger.Info("[开发模式--Android环境]");
            //updateMgr.Update(LoadGameLoaderAtAndorid);
            string path = "../GameCode/Lib/GameLoader.dll";
            DriverLogger.Info(string.Format("[开发模式][Driver] 加载文件:{0} [Start]", path));
            beginTime = Environment.TickCount;
            AssemblyDll(path, FileUtils.LoadByteFile(path), null, false);
        });
#else
#if IPHONE_TEST
                        updateMgr.Update(() => {
                            DriverLogger.Info("[Driver] [Iphone_Test平台]");
                            LoaderDriver loaderDriver = gameObject.AddComponent<LoaderDriver>();
                            object[] args = new object[] { confirmBox.prefab, progressBar.prefab, progressBar.fxInitX, progressBar.maskMaxWidth };
                            loaderDriver.Setup(UpdateMgr.CfgInfoNew, true, args);
                            DriverLogger.Release();
                        });
#else
        if (Application.platform == RuntimePlatform.WindowsPlayer)
        {//PC端流程
            updateMgr.Update(LoadGameLoaderAtPC);
        }
        else
        {
            updateMgr.Update(() =>
            {
                string path = Application.dataPath + "/../../GameCode/Lib/GameLoader.dll";
                //string path = "D:/client/trunk/GameCode/Lib/GameLoader.dll";
                DriverLogger.Info(string.Format("[开发模式][Driver] 加载文件:{0} [Start]", path));
                beginTime = Environment.TickCount;
                AssemblyDll(path, FileUtils.LoadByteFile(path), null, false);
            });
        }
#endif
#endif
#endif
#endif
    }

#if UNITY_IPHONE
    //IOS下--加载GameLoader
    private void LoadGameLoaderAtIOS()
    {
        LoaderDriver loaderDriver = gameObject.AddComponent<LoaderDriver>();
#if UNITY_EDITOR
            object[] args = new object[] { confirmBox.prefab, progressBar.prefab, progressBar.fxInitX, progressBar.maskMaxWidth };
            loaderDriver.Setup(UpdateMgr.CfgInfoNew, true, args);
#else
            object[] args = new object[] { confirmBox.prefab, progressBar.prefab, progressBar.fxInitX, progressBar.maskMaxWidth };
            loaderDriver.Setup(UpdateMgr.CfgInfoNew, false, args);
#endif
    }
#endif

    //安卓下--加载GameLoader
    private void LoadGameLoaderAtAndorid()
    {
        try
        {
            bool isAssetBytes = false;
            DriverLogger.Info("载入GameLoader [Start]");
            string gameLoader = string.Concat("Resources/", GameLoaderName);
            string url = string.Concat(Application.persistentDataPath, "/RuntimeResource/", gameLoader);
            if (File.Exists(url))
            {//从资源外部目录加载
                isAssetBytes = true;
                url = string.Concat(Application.isEditor ? "file:///" : "file://", url);
                DriverLogger.Info(string.Format("[Driver][Android平台] 从外部目录加载文件:{0} [Start]", url));
            }
            else
            {//从SD卡加载
                isAssetBytes = false;
                url = GetStreamPath(gameLoader);
                DriverLogger.Info(string.Format("[Driver][Android平台] 从SD卡加载文件:{0} [Start]", url));
            }
            beginTime = Environment.TickCount;
            HttpWrappr.instance.LoadWwwData(url, AssemblyDll, AssemblyDll, isAssetBytes);
        }
        catch (Exception ex)
        {
            DriverLogger.Error(string.Format("载入GameLoader出错,reason:{0}", ex.StackTrace));
            confirmBox.Show(DefaultLanguage.GetString("error_file"), onExit, DefaultLanguage.GetString("exit"), null, false);
        }
    }

    //PC下--加载GameLoader
    private void LoadGameLoaderAtPC()
    {
        try
        {
            DriverLogger.Info("载入GameLoader [Start]");
            string gameLoader = string.Concat("Resources/", GameLoaderName);
            string url = string.Concat(Application.persistentDataPath, "/RuntimeResource/", gameLoader);
            if (File.Exists(url))
            {//从资源外部目录加载
                DriverLogger.Info(string.Format("[Driver][Windows平台] 从用户目录加载文件:{0} [Start]", url));
            }
            else
            {//从SD卡加载
                url = String.Concat(Application.streamingAssetsPath, "/", gameLoader);
                DriverLogger.Info(string.Format("[Driver][Windows平台] 从安装路径加载文件:{0} [Start]", url));
            }
            beginTime = Environment.TickCount;
            AssemblyDll(url, FileUtils.LoadByteFile(url), null);
            //StartCoroutine(LoadFromMemory(url)); 
        }
        catch (Exception ex)
        {
            DriverLogger.Error(string.Format("载入GameLoader出错,reason:{0}", ex.StackTrace));
            confirmBox.Show(DefaultLanguage.GetString("error_file"), onExit, DefaultLanguage.GetString("exit"), null, false);
        }
    }


    //WEB下--加载GameLoader
    private void LoadGameLoaderAtWeb()
    {
#if UNITY_WEBPLAYER
        LoaderDriver loaderDriver = gameObject.AddComponent<LoaderDriver>();
        object[] args = new object[] { confirmBox.prefab, progressBar.prefab, progressBar.fxInitX, progressBar.maskMaxWidth };
        loaderDriver.Setup(UpdateMgr.CfgInfoNew, false, args);
#endif
    }

    private IEnumerator LoadFromMemory(string url)
    {
        byte[] data = FileUtils.LoadByteFile(url);
        AssetBundleCreateRequest request = AssetBundle.LoadFromMemoryAsync(data);
        yield return request;

        DriverLogger.Info("LoadFromMeory() GameLoader[OK]");
        AssemblyDll(url, (request.assetBundle.mainAsset as TextAsset).bytes, null);
        request.assetBundle.Unload(true);
        request = null;
        data = null;
    }

    // 根据文件名--取得资源的StreamAssets路径
    private string GetStreamPath(string fileName)
    {
        string Path = String.Concat(Application.streamingAssetsPath, "/", fileName);
        return Application.platform != RuntimePlatform.Android ? Path = String.Concat("file://", Path) : Path;
    }

    private void AssemblyDll(string url)
    {
        AssemblyDll(url, null, null);
    }

    private void AssemblyDll(string url, string errorInfo)
    {
        AssemblyDll(url, null, null);
    }

    private void AssemblyDll(string url, byte[] bytes, byte[] bytes2)
    {
        AssemblyDll(url, bytes, bytes2, true);
    }

    private void AssemblyDll(string url, byte[] bytes, byte[] bytes2, bool needDecrypt)
    {
        if (bytes != null && bytes.Length > 0)
        {
            DriverLogger.Info(string.Format("载入GameLoader[OK] cost:{0}ms", Environment.TickCount - beginTime));
            progressBar.UpdateProgress(0.1f);
            beginTime = Environment.TickCount;
            if (needDecrypt)
                bytes = KeyUtils.Decrypt(bytes, KeyUtils.GetResNumber());
            Assembly assembly = Assembly.Load(bytes);
            Type loaderDriverType = assembly.GetType("LoaderDriver");
            if (loaderDriverType != null)
            {
                Component com = gameObject.AddComponent(loaderDriverType);
                //需要传递更多参数到GameLoader,在这里传递
                object[] args = new object[] { confirmBox.prefab, progressBar.prefab, progressBar.fxInitX, progressBar.maskMaxWidth };
                com.GetType().GetMethod("Setup").Invoke(com, new object[] { UpdateMgr.CfgInfoNew, false, args });
                DriverLogger.Info(string.Format("[Driver] Setup()[OK],cost:{0}ms", Environment.TickCount - beginTime));
                DriverLogger.Release();
                updateMgr.Dispose();
                progressBar.Dispose();
                confirmBox.Dispose();
                updateMgr = null;
                progressBar = null;
                confirmBox = null;
            }
            else
            {
                DriverLogger.Error(string.Format("找不到LoaderDriver 将退出应用:{0}", url));
                confirmBox.Show(DefaultLanguage.GetString("noExist_file"), onExit, DefaultLanguage.GetString("exit"), null, false);
            }
        }
        else
        {
            DriverLogger.Error(string.Format("GameLoader内容为空，将退出应用:{0}", url));
            confirmBox.Show(DefaultLanguage.GetString("null_file"), onExit, DefaultLanguage.GetString("exit"), null, false);
        }
    }

    //退出应用
    private void onExit(bool result)
    {
        Application.Quit();
    }

}
