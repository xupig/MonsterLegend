﻿using System;
using Default;
using GameLoader;
using UnityEngine;
using GameLoader.Defines;
#if !UNITY_WEBPLAYER
using System.IO;
using System.Collections.Generic;
using GameLoader.Utils.XML;
using System.Security;
using GameLoader.Utils;
#endif

namespace Init.Update
{
    /// <summary>
    /// 更新管理
    ///     1、GameLoader进行版本管理
    ///     2、Resources/Dialog/目录资源进行版本管理
    /// 加载流程：
    ///     1、加载system_config.xml文件 取到cfg.xml下载地址
    ///     2、下载cfg.xml文件，取到common.xml下载地址
    ///     3、下载common.xml文件,取到服务器GameLoaderVersion,和本地GameLoaderVersion版本比较：如果高于本地版本，则下载GameLoader.bytes.u
    ///     4、下载GameLoader.bytes.u,并放到外部目录
    ///     5、结束
    /// </summary>
    class UpdateMgr
    {
        private int beginTime;
        private Action callback;

        private string cfgUrl;
        private string commonUrl;
        private string gameLoaderUrl;
        private string gameLoaderMd5;
        private bool isCheckGameLoadermd5;               //是否对下载的GameLoader进行md5检查
        private string serverGameLoaderVersion;          //GameLoader.dll在服务器上的版本号
        private string serverProgressBarBgVersion;       //进度条背景图在服务器上的版本号
        private string versionInfoOutputPath;

        private DefaultConfirmBox confirmBox;
        private DefaultProgressBar progressBar;
        private SecurityElement versionDoc;
        private SecurityElement localGameLoaderVersionNode;

        public const string CFG_FILE = "cfg.xml";
        public const string VERSION_URL_KEY = "VersionInfo";
        public const string CFG_PARENT_KEY = "parent";
        public static readonly string RESOURCES = "/Resources";
        public static readonly string RUNTIME_RESOURCE = "/RuntimeResource";
        public static readonly string RUNTIME_RESOURCE_CONFIG = "/Config";


        public static string BundleIdentifier = "";
        public readonly static string CfgPath = String.Concat(Application.persistentDataPath, RUNTIME_RESOURCE, RUNTIME_RESOURCE_CONFIG, "/", CFG_FILE);
        public static Dictionary<string, string> CfgInfoNew = new Dictionary<string, string>();

        public UpdateMgr(DefaultConfirmBox confirmBox, DefaultProgressBar progressBar)
        {
            this.confirmBox = confirmBox;
            this.progressBar = progressBar;
            versionInfoOutputPath = string.Concat(Application.persistentDataPath, RUNTIME_RESOURCE, RUNTIME_RESOURCE_CONFIG, "/VersionInfo.xml");
            BundleIdentifier = Application.bundleIdentifier;
            DriverLogger.Info("bundleIdentifier: " + BundleIdentifier);
        }

        /// <summary>
        /// GameLoader.dll文件内容
        /// </summary>
        public byte[] gameLoaderBytes { get; private set; }

        public void Dispose()
        {
            cfgUrl = null;
            commonUrl = null;
            gameLoaderUrl = null;
            gameLoaderMd5 = null;
            serverGameLoaderVersion = null;
            versionInfoOutputPath = null;
            localGameLoaderVersionNode = null;

            callback = null;
            versionDoc = null;
            confirmBox = null;
            progressBar = null;
        }

        /// <summary>
        /// 进行版本检查
        /// 1、GameLoader.byte版本检查
        /// 2、Resources/Dialog/下一步再做
        /// </summary>
        /// <param name="callback"></param>
        public void Update(Action callback)
        {
            this.callback = callback;
            progressBar.UpdateTip(DefaultLanguage.GetString("doing_game"));
            LoadSystemConfig();
        }

        //A-1、加载本地system_config.xml文件
        private void LoadSystemConfig()
        {
            string fileName = string.Empty;
            try
            {
                beginTime = Environment.TickCount;
                fileName = "system_config";
                if (Application.platform == RuntimePlatform.Android) fileName = "system_config_android";
                if (Application.platform == RuntimePlatform.IPhonePlayer) fileName = "system_config_ios";
                DriverLogger.Info(string.Format("[UpdateMgr] 载入文件:{0} [Start]", fileName));

                string content = null;
#if !UNITY_WEBPLAYER
                string path = string.Concat(Application.persistentDataPath, RUNTIME_RESOURCE, RUNTIME_RESOURCE_CONFIG, "/", fileName, ".xml");
                if (File.Exists(path))
                {
                    content = FileUtils.LoadFile(path);
                    DriverLogger.Info("外部目录加载：" + path);
                }
                else
#endif
                {
                    var obj = Resources.Load(fileName);
                    content = obj.ToString();
                    Resources.UnloadAsset(obj);
                    DriverLogger.Info("SD卡目录加载" + fileName + ".xml");
                }

                SecurityElement xmlDoc = XMLParser.LoadXML(content);
                SecurityElement node = xmlDoc.SearchForChildByTag("cfg");

                cfgUrl = node.Attribute("url");
                DriverLogger.Info(string.Format("文件:{0}载入[OK],cost:{1}ms", fileName, Environment.TickCount - beginTime));
                progressBar.UpdateProgress(0.02f);
                //LoadCfg(cfgUrl);

                LoadCfgInfo(cfgUrl, LoadCfgInfoCallback);

                xmlDoc = null;
            }
            catch (Exception ex)
            {
                DriverLogger.Error(string.Format("载入文件:{0} [Fail],reason:{1}", fileName, ex.StackTrace));
                confirmBox.Show(DefaultLanguage.GetString("error_file"), onExit, DefaultLanguage.GetString("exit"), null, false);
            }
        }

        public static string GetCfgInfoUrl(String key)
        {
            string result = "";
            if (CfgInfoNew.ContainsKey(key))
            {
                result = CfgInfoNew[key];
            }
            return result;
        }

        private void LoadCfgInfoCallback(bool result)
        {
            if (!result)
            {
                confirmBox.Show(DefaultLanguage.GetString("error_file"), (ret) =>
                {
                    if (ret) LoadCfgInfo(cfgUrl, LoadCfgInfoCallback);
                    else onExit(ret);
                }, DefaultLanguage.GetString("try"), DefaultLanguage.GetString("exit"));
            }
            else
            {
                progressBar.UpdateProgress(0.06f);
                LoadLocalVersionInfo();


                gameLoaderUrl = GetCfgInfoUrl("GameLoaderUrl");
                gameLoaderMd5 = GetCfgInfoUrl("GameLoaderMd5");
                serverGameLoaderVersion = GetCfgInfoUrl("GameLoaderVersion");
                serverProgressBarBgVersion = GetCfgInfoUrl("ProgressBarBgVersion");
                isCheckGameLoadermd5 = GetCfgInfoUrl("IsCheckGameLoaderMd5") != "" && GetCfgInfoUrl("IsCheckGameLoaderMd5") == "1";
                if (GetCfgInfoUrl("UploadLogUrl") != "") LogUploader.GetInstance().UploadLogUrl = GetCfgInfoUrl("UploadLogUrl");
                LogUploader.GetInstance().NeedUploadLog = GetCfgInfoUrl("NeedUploadLog") != "" && GetCfgInfoUrl("NeedUploadLog") == "1";


                localGameLoaderVersionNode = versionDoc.SearchForChildByTag("GameLoaderVersion");

                var serverVer = new VersionCodeInfo(serverGameLoaderVersion);
                var localVer = new VersionCodeInfo(localGameLoaderVersionNode.Text);
                progressBar.UpdateProgress(0.07f);
                var dlUrl = GetCfgInfoUrl("DefaultLanguageUrl");
                var dlMd5 = GetCfgInfoUrl("DefaultLanguageMd5");
                if (!string.IsNullOrEmpty(dlUrl) && !string.IsNullOrEmpty(dlMd5))
                    DefaultLanguage.CheckUpdate(dlUrl, dlMd5, () =>
                    {
                        AfterLoadCfgInfo(serverVer, localVer);
                    });
                else
                    AfterLoadCfgInfo(serverVer, localVer);
            }
        }

        private void AfterLoadCfgInfo(VersionCodeInfo serverVer, VersionCodeInfo localVer)
        {

#if !UNITY_WEBPLAYER && !UNITY_IPHONE && !UNITY_EDITOR
                if (serverVer.Compare(localVer) > 0)
                {//有新版本的GameLoader
                    DownloadGameLoader();
                }
                else
                {//没有可更新的GameLoader
                    DriverLogger.Info("GameLoader没有可更新");
                    if (callback != null) callback();
                    callback = null;
                }
#else
            if (callback != null) callback();
            callback = null;
#endif
        }

        private void onExit(bool result)
        {
            Application.Quit();
        }


        //public static String LoadStringFromResource(String fileName)
        //{
        //    var text = Resources.Load(fileName);
        //    if (text != null)
        //    {
        //        var result = text.ToString();
        //        Resources.UnloadAsset(text);
        //        return result;
        //    }
        //    else
        //    {
        //        return String.Empty;
        //    }
        //}

        public static void LoadCfgInfo(string cfgUrl, Action<bool> callback)
        {
            string cfgPath = string.Concat(Application.persistentDataPath, RUNTIME_RESOURCE, RUNTIME_RESOURCE_CONFIG, "/cfg.xml");
            if (File.Exists(cfgPath))
            {
                DriverLogger.Info("load local cfg: " + cfgPath);
                var cfgStr = FileUtils.LoadFile(cfgPath);
                //CfgInfo = LoadXMLText<CfgInfo>(cfgStr);
                CfgInfoNew = LoadCfgInfoList(cfgStr);
                if (callback != null)
                    callback(CfgInfoNew != null && CfgInfoNew.Count > 0 ? true : false);
                return;
            }
            
            DriverLogger.Info("cfgUrl: " + cfgUrl);
            var programVerStr = "";
            var programVer = Resources.Load(VERSION_URL_KEY) as TextAsset;
            SecurityElement rootNode = XMLParser.LoadXML(programVer.text);
            SecurityElement proNode = rootNode.SearchForChildByTag("ProgramVersion");
            if (programVer && !String.IsNullOrEmpty(programVer.text))
            {
                programVerStr = "V" + proNode.Text;
            }

            //var s = Application.persistentDataPath.Split('/');
            ////Debug.Log(s.Length);
            //for (int i = s.Length - 1; i >= 0; i--)
            //{
            //    //Debug.Log(s[i]);
            //    if (s[i] == "files" && i - 1 >= 0)
            //    {
            //        BundleIdentifier = s[i - 1];
            //        DriverLogger.Info("bundleIdentifier: " + BundleIdentifier);
            //        break;
            //    }
            //}

            Action erraction = () =>
            {
                if (callback != null)
                    callback(false);
            };
            Action<string> suaction = null;
            suaction = (res) =>
            {
                var parentinfo = LoadCfgInfoList(res);
                foreach (var pair in parentinfo)
                {
                    if (!CfgInfoNew.ContainsKey(pair.Key))
                    {
                        CfgInfoNew.Add(pair.Key, pair.Value);
                    }
                }
                if (!string.IsNullOrEmpty(BundleIdentifier) && parentinfo.ContainsKey(BundleIdentifier))//根据包名做特殊处理
                {
                    CfgInfoNew.Clear();
                    HttpWrappr.instance.LoadWwwText(parentinfo[BundleIdentifier], suaction, erraction);
                    DriverLogger.Info("Use bundle url: " + parentinfo[BundleIdentifier]);
                }
                else if (parentinfo.ContainsKey(programVerStr))//根据版本做特殊处理
                {
                    CfgInfoNew.Clear();
                    HttpWrappr.instance.LoadWwwText(parentinfo[programVerStr], suaction, erraction);
                }
                else if (parentinfo.ContainsKey(CFG_PARENT_KEY))
                {
                    HttpWrappr.instance.LoadWwwText(parentinfo[CFG_PARENT_KEY], suaction, erraction);
                }
                else if (callback != null)
                    callback(CfgInfoNew != null && CfgInfoNew.Count > 0 ? true : false);
            };
            HttpWrappr.instance.LoadWwwText(cfgUrl, suaction, erraction);
        }


        private static Dictionary<string, string> LoadCfgInfoList(string text)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            var children = XMLParser.LoadXML(text);
            if (children != null && children.Children != null && children.Children.Count != 0)
            {
                foreach (System.Security.SecurityElement item in children.Children)
                {
                    result[item.Tag] = item.Text;
                }
            }
            return result;
        }


        //加载本地VersionInfo.xml文件
        private void LoadLocalVersionInfo()
        {
            string content = null;
#if !UNITY_WEBPLAYER
            if (File.Exists(versionInfoOutputPath))
            {//从外部目录读取VersionInfo.xml
                DriverLogger.Info(string.Format("外部目录载入:{0}", versionInfoOutputPath));
                using (StreamReader sr = File.OpenText(versionInfoOutputPath))
                {
                    content = sr.ReadToEnd();
                }
            }
            else
#endif
            {//从Sd卡读取VersionInfo.xml  
                DriverLogger.Info("SD卡载入:VersionInfo.xml");
                var v = Resources.Load("VersionInfo");
                content = v.ToString();
                Resources.UnloadAsset(v);
            }


            versionDoc = XMLParser.LoadXML(content);
        }

        private void DownloadGameLoader()
        {
            beginTime = Environment.TickCount;
            DriverLogger.Info(string.Format("下载文件:{0}[Start]", gameLoaderUrl));
            HttpWrappr.instance.LoadWwwData(gameLoaderUrl, onLoaded, onLoaded, false);
        }

        private void onLoaded(string url)
        {
            onLoaded(url, null, null);
        }

        private void onLoaded(string url, string errorInfo)
        {
            onLoaded(url, null, null);
        }

        private void onLoaded(string url, byte[] data, byte[] srcData)
        {
            if (data != null && data.Length > 0)
            {
                if (isCheckGameLoadermd5)
                {//开启GameLoader做md5检查
                    string md5 = MD5Utils.FormatMD5(MD5Utils.CreateMD5(srcData));
                    if (md5 != gameLoaderMd5)
                    {
                        DriverLogger.Error(string.Format("GameLoader md5 check fail srcmd5:{0},curmd5:{1}", gameLoaderMd5, md5));
                        confirmBox.Show(DefaultLanguage.GetString("md5Fail_file"), onGameLoaderExit, DefaultLanguage.GetString("try"), DefaultLanguage.GetString("exit"));
                        return;
                    }
                }

                //把最新的GameLoader写入本地
                string localAbsPath = string.Concat(Application.persistentDataPath, RUNTIME_RESOURCE, RESOURCES, "/", Driver.GameLoaderName);
                if (Application.platform == RuntimePlatform.WindowsPlayer)
                {//PC端或web端，资源统一放到StreamingAssets下
                    localAbsPath = string.Concat(Application.streamingAssetsPath, RESOURCES, "/", Driver.GameLoaderName);
                }
                DriverLogger.Info(string.Format("下载文件:{0}[OK] cost:{1}ms", url, Environment.TickCount - beginTime));
                beginTime = Environment.TickCount;
#if !UNITY_WEBPLAYER
                if (File.Exists(localAbsPath)) File.Delete(localAbsPath);
                FileUtils.WriterByteFile(localAbsPath, data);
                DriverLogger.Info(string.Format("写入文件:{0}，总字节:{1}[OK],cost:{2}ms", localAbsPath, data.Length, Environment.TickCount - beginTime));
#else
                gameLoaderBytes = data;
#endif
                //更新GameLoader版本并写入本地
                if (localGameLoaderVersionNode == null)
                {
                    localGameLoaderVersionNode = new SecurityElement("GameLoaderVersion", serverGameLoaderVersion);
                    versionDoc.AddChild(localGameLoaderVersionNode);
                }
                localGameLoaderVersionNode.Text = serverGameLoaderVersion;
#if !UNITY_WEBPLAYER
                if (File.Exists(versionInfoOutputPath)) File.Delete(versionInfoOutputPath);
                FileUtils.WriterFile(versionInfoOutputPath, versionDoc.ToString());
                DriverLogger.Info(string.Format("GameLoader 版本:{0}已写入本地文件:{1}", serverGameLoaderVersion, versionDoc.ToString()));
#endif
                progressBar.UpdateProgress(0.09f);
                progressBar.UpdateTip("Download And Update GameLoader OK");
                if (callback != null) callback();
                callback = null;
            }
            else
            {
                DriverLogger.Error(string.Format("下载文件:{0} 内容为空[Fail]", url));
                confirmBox.Show(DefaultLanguage.GetString("error_file"), onGameLoaderExit, DefaultLanguage.GetString("try"), DefaultLanguage.GetString("exit"));
            }
        }

        private void onGameLoaderExit(bool result)
        {
            if (result) DownloadGameLoader();
            else onExit(result);
        }

    }
}
