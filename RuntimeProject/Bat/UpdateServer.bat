@echo off
cd %~dp0

for /f "tokens=*" %%i in (unity_path.txt) do set InstallDir=%%i

if /i "%InstallDir%"=="" goto :yes

echo Unity3d的安装路径：%InstallDir%
@echo on
cd /d %InstallDir%
Unity.exe -batchmode -quit -projectPath %~dp0../ -executeMethod PublishUtils.UpdateApkDevServer

exit
:yes
color f4
echo 　　　　　==================================================
echo 　　　　　！！！！！！！你没有配置Unity安装路径！！！！！！！
echo 　　　　　==================================================
echo.&call
echo 　　　　　请参照当前目录的unity_path.template.txt文件配置Unity安装目录路径，并保存文件名为unity_path.txt
echo.&call
echo 　　　　　有什么不懂的找阿里。    (￣y▽￣)~*
echo.&call
pause
exit

