﻿using UnityEngine;
using Game.UI.UIComponent;


namespace Game.UI.Builder
{
    public class LabelBuilder : ComponentBuilder
    {
        public override string Type
        {
            get { return TYPE_LABEL; }
        }

        public override void Build(GameObject go, bool interactable)
        {
            if(GetParam(go) != JSON_STATIC)
            {
                go.AddComponent<StateText>();
            }
        }

    }
}
