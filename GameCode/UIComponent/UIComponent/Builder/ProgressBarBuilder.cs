﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/16 9:35:48
 * 描述说明：
 * *******************************************************/

using Game.UI.UIComponent;
using UnityEngine;

namespace Game.UI.Builder
{
    public class ProgressBarBuilder : ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_PROGRESS_BAR;
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, true);
            go.AddComponent<KProgressBar>();
        }
    }
}
