﻿using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using Game.UI.UIComponent;

namespace Game.UI.Builder
{
    public class InputBuilder : ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_INPUT;
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, false);
            Text text = go.GetComponentInChildren<Text>();
            RectTransform textRect = text.GetComponent<RectTransform>();
            textRect.pivot = new Vector2(0.5f, 0.5f);
            textRect.anchoredPosition = new Vector2(textRect.sizeDelta.x * 0.5f, -textRect.sizeDelta.y * 0.5f);
            Regex regex = new Regex(@"<.*?>");
            text.supportRichText = false;
            text.text = regex.Replace(text.text, "");
            KInputField input = go.AddComponent<KInputField>();
            input.transition = Selectable.Transition.None;
            input.textComponent = text;
            input.text = text.text;
        }
    }
}
