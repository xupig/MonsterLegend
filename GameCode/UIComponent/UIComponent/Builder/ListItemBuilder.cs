﻿using UnityEngine;
using Game.UI.UIComponent;

namespace Game.UI.Builder
{
    public class ListItemBuilder : ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_ITEM;
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, interactable);
        }
    }
}
