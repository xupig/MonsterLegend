﻿#region 模块信息
/*==========================================
// 文件名：TreeBuilder
// 命名空间: Assets.Plugins.UIComponent.Builder
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/27 14:45:19
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using UnityEngine;

namespace Game.UI.Builder
{
    public class TreeBuilder : ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_TREE;
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, true);
            go.AddComponent<KTree>();
        }
    }
}
