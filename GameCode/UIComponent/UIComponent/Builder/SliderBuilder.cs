﻿using UnityEngine;
using Game.UI.UIComponent;

namespace Game.UI.Builder
{
    public class SliderBuilder : ContainerBuilder
    {
        private GameObject _fillGo;
        private RectTransform _fillRect;
        private GameObject _fillAreaGo;
        private RectTransform _fillAreaRect;
        private GameObject _fillHolderGo;
        private RectTransform _fillHolderRect;

        private GameObject _handleGo;
        private RectTransform _handleRect;
        private GameObject _handleAreaGo;
        private RectTransform _handleAreaRect;
        private GameObject _handleHolderGo;
        private RectTransform _handleHolderRect;

        public override string Type
        {
            get
            {
                return TYPE_SLIDER;
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, false);
            Restructure(go);
            AddSliderComponent(go);
        }

        private void Restructure(GameObject go)
        {
            _fillGo = KComponentUtil.GetChild(go, "fill");
            _fillRect = _fillGo.GetComponent<RectTransform>();
            _fillAreaGo = CreateGameObject(go, "fillArea", _fillRect);
            _fillAreaRect = _fillAreaGo.GetComponent<RectTransform>();
            _fillHolderGo = CreateGameObject(_fillAreaGo, "fillHolder", _fillRect);
            _fillHolderRect = _fillHolderGo.GetComponent<RectTransform>();
            AttachToParent(_fillGo, _fillHolderGo);
            _fillAreaRect.anchoredPosition = new Vector2(_fillAreaRect.anchoredPosition.x, _fillAreaRect.anchoredPosition.y);
            _fillAreaRect.localScale = Vector3.one;

            _handleGo = KComponentUtil.GetChild(go, "handle");//optional
            if(_handleGo != null)
            {
                _handleRect = _handleGo.GetComponent<RectTransform>();
                _handleAreaGo = CreateGameObject(go, "handleArea", _fillAreaRect);
                _handleAreaRect = _handleAreaGo.GetComponent<RectTransform>();
                _handleHolderGo = CreateGameObject(_handleAreaGo, "handleHolder", _handleRect);
                _handleHolderRect = _handleHolderGo.GetComponent<RectTransform>();
                AttachToParent(_handleGo, _handleHolderGo);

                _handleAreaRect.anchoredPosition = new Vector2(_handleAreaRect.anchoredPosition.x, _handleAreaRect.anchoredPosition.y);
                _handleAreaRect.localScale = Vector3.one;
                _handleHolderRect.anchoredPosition = new Vector2(_handleRect.anchoredPosition.x, _handleRect.anchoredPosition.y);
                _handleHolderRect.sizeDelta = new Vector2(_handleRect.sizeDelta.x, _handleRect.sizeDelta.y - _handleAreaRect.sizeDelta.y);
                _handleHolderRect.localScale = Vector3.one;
                _handleRect.anchoredPosition = new Vector2(0, 0);
                _handleRect.localScale = Vector3.one;
            }
        }

        private void AddSliderComponent(GameObject go)
        {
            KSlider slider = go.AddComponent<KSlider>();
            slider.fillRect = _fillHolderRect;
            slider.fillRect.sizeDelta = new Vector2(0, 0);
            if(_handleGo != null)
            {
                slider.handleRect = _handleHolderRect;
                slider.handleRect.anchoredPosition = new Vector2(-_handleRect.sizeDelta.x * 0.5f, _handleRect.sizeDelta.y * 0.5f - _handleAreaRect.sizeDelta.y * 0.5f);
                slider.handleRect.sizeDelta = new Vector2(0, 0);
            }
        }
    }
}
