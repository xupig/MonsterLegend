﻿using UnityEngine;
using Game.UI.UIComponent;

namespace Game.UI.Builder
{
    public class ToggleGroupBuilder : ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_TOGGLE_GROUP;
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, true);
            go.AddComponent<KToggleGroup>();
        }
    }
}
