﻿using UnityEngine;
using Game.UI.UIComponent;

namespace Game.UI.Builder
{
    public class ImageBuilder : ComponentBuilder
    {
        public override string Type
        {
            get { return TYPE_IMAGE; }
        }

        public override void Build(GameObject go, bool interactable)
        {
            if(GetParam(go) != JSON_STATIC)
            {
                go.AddComponent<StateImage>();
            }
        }

    }

    public class ScaleImageBuilder : ImageBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_SCALE_IMAGE; 
            }
        }
    }
}
