﻿using UnityEngine;
using Game.UI.UIComponent;

namespace Game.UI.Builder
{
    public class ButtonBuilder : ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_BUTTON;
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, false);
            go.AddComponent<KButtonStateFacade>();
            if (interactable == true)
            {
                if(GetParam(go) == JSON_SHRINK)
                {
                    go.AddComponent<KShrinkableButton>();
                }
                else
                {
                    go.AddComponent<KButton>();
                }
            }
        }
    }
}
