﻿using UnityEngine;
using Game.UI.UIComponent;

namespace Game.UI.Builder
{
    public class PanelBuilder : ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_PANEL;
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, interactable);
        }
    }
}
