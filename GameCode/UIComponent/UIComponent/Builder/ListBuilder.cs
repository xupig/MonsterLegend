﻿using UnityEngine;
using Game.UI.UIComponent;


namespace Game.UI.Builder
{
    public class ListBuilder : ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_LIST;
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, true);
            KList list = null;
            if(GetParam(go) == JSON_PAGEABLE)
            {
                list = go.AddComponent<KPageableList>();
            }
            else
            {
                list = go.AddComponent<KList>();
            }
            list.itemBuilder = ComponentBuilder.typeBuilderDict[TYPE_ITEM];
        }

    }
}
