﻿using UnityEngine;
using Game.UI.UIComponent;

namespace Game.UI.Builder
{
    public class ContainerBuilder : ComponentBuilder
    {

        public override string Type
        {
            get { return TYPE_CONTAINER; }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, interactable);
            go.AddComponent<KContainer>();
        }

        public virtual void BuildChildren(GameObject parent, bool childInteractable)
        {
            for(int i = 0; i < parent.transform.childCount; i++)
            {
                GameObject child = parent.transform.GetChild(i).gameObject;
                BuildHelper helper = child.GetComponent<BuildHelper>();
                if(helper != null)
                {
                    BuildChild(child, helper, childInteractable);
                }
                else
                {
                    BuildChild(child, childInteractable);
                }
            }
        }

        private void BuildChild(GameObject child, BuildHelper helper, bool childInteractable)
        {
            ComponentBuilder builder = ComponentBuilder.GetBuilder(helper);
            ContainerBuilder containerBuilder = builder as ContainerBuilder;
            if(containerBuilder != null)
            {
                if(HasDeferredParam(helper) == true)
                {
                    ExecuteDeferredParam(child, childInteractable);
                }
                else
                {
                    containerBuilder.Build(child, childInteractable);
                }
            }
            else
            {
                builder.Build(child, childInteractable);
            }
        }

        private void BuildChild(GameObject child, bool childInteractable)
        {
            ComponentBuilder builder = ComponentBuilder.GetBuilder(child.name);
            if(builder != null)
            {
                builder.Build(child, childInteractable);
            }
        }

        protected virtual bool IgnoreChild(GameObject child)
        {
            return child.GetComponent<ParticleSystemWrapper>() != null;
        }

        protected bool HasDeferredParam(BuildHelper helper)
        {
            return string.IsNullOrEmpty(helper.param) == false && helper.param == JSON_DEFERRED;
        }

        protected void ExecuteDeferredParam(GameObject go, bool childInteractable)
        {
            KDeferredContainer container = go.AddComponent<KDeferredContainer>();
            container.childInteractable = childInteractable;
        }
    }
}
