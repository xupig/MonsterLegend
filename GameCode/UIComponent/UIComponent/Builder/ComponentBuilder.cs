﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Game.UI.Builder
{
    public abstract class ComponentBuilder
    {
        public const string TYPE_CONTAINER = "Container";
        public const string TYPE_LABEL = "Label";
        public const string TYPE_IMAGE = "Image";
        public const string TYPE_SCALE_IMAGE = "ScaleImage";
        public const string TYPE_BUTTON = "Button";
        public const string TYPE_INPUT = "Input";
        public const string TYPE_PROGRESS_BAR = "ProgressBar";
        public const string TYPE_LIST = "List";
        public const string TYPE_SCROLL_PAGE = "ScrollPage";
        public const string TYPE_SCROLL_VIEW = "ScrollView";
        public const string TYPE_SLIDER = "Slider";
        public const string TYPE_TOGGLE = "Toggle";
        public const string TYPE_TOGGLE_GROUP = "ToggleGroup";
        public const string TYPE_TREE = "Tree";
        public const string TYPE_STATIC = "Static";
        public const string TYPE_ITEM = "item";
        public const string TYPE_PANEL = "Panel";

        public const string JSON_TYPE = "type";
        public const string JSON_NAME = "name";
        public const string JSON_CHILDREN = "children";
        public const string JSON_ITEM = "item";
        public const string JSON_PARAM = "param";
        public const string JSON_DEFERRED = "deferred";
        public const string JSON_PAGEABLE = "pageable";
        public const string JSON_STATIC = "static";
        public const string JSON_SHRINK = "shrink";

        public static Char[] FULL_NAME_SPLITER = new Char[] { '_' };
        public static Regex COMPONENT_NAME_TESTER = new Regex(@"[A-Z]\w+?_\w+(?!\W)");
        

        public static Dictionary<string, ComponentBuilder> typeBuilderDict = new Dictionary<string,ComponentBuilder>();
        public static Dictionary<string, ComponentBuilder> specificBuilderDict = new Dictionary<string,ComponentBuilder>();
        //缓存面板中的具体GameObject的Builder，Key为GameObject.name
        private static Dictionary<string, ComponentBuilder> _childBuilderDict = new Dictionary<string, ComponentBuilder>();

        static ComponentBuilder()
        {
            AddSpecificBuilder(new DummyButtonBuilder());
            AddSpecificBuilder(new ItemBuilder());
            AddSpecificBuilder(new MaskBuilder());
            AddSpecificBuilder(new TopLeftContainerBuilder());
            AddSpecificBuilder(new TopCenterContainerBuilder());
            AddSpecificBuilder(new TopRightContainerBuilder());
            AddSpecificBuilder(new MiddleLeftContainerBuilder());
            AddSpecificBuilder(new MiddleCenterContainerBuilder());
            AddSpecificBuilder(new MiddleRightContainerBuilder());
            AddSpecificBuilder(new BottomLeftContainerBuilder());
            AddSpecificBuilder(new BottomCenterContainerBuilder());
            AddSpecificBuilder(new BottomRightContainerBuilder());

            AddTypeBuilder(new LabelBuilder());
            AddTypeBuilder(new InputBuilder());
            AddTypeBuilder(new ImageBuilder());
            AddTypeBuilder(new ScaleImageBuilder());
            AddTypeBuilder(new ContainerBuilder());
            AddTypeBuilder(new ButtonBuilder());
            AddTypeBuilder(new ToggleBuilder());
            AddTypeBuilder(new ToggleGroupBuilder());
            AddTypeBuilder(new SliderBuilder());
            AddTypeBuilder(new ListBuilder());
            AddTypeBuilder(new TreeBuilder());
            AddTypeBuilder(new ScrollViewBuilder());
            AddTypeBuilder(new ScrollPageBuilder());
            AddTypeBuilder(new ProgressBarBuilder());
            AddTypeBuilder(new StaticBuilder());
            AddTypeBuilder(new ListItemBuilder());
            AddTypeBuilder(new PanelBuilder());
        }

        public static void AddTypeBuilder(ComponentBuilder builder)
        {
            typeBuilderDict.Add(builder.Type, builder);
        }

        public static void AddSpecificBuilder(ComponentBuilder builder)
        {
            specificBuilderDict.Add(builder.Name, builder);
        }

        public static void ClearChildBuilder()
        {
            _childBuilderDict.Clear();
        }

        public static ComponentBuilder GetBuilder(BuildHelper helper)
        {
            return GetBuilder(helper.type, helper.name);
        }

        public static ComponentBuilder GetBuilder(string fullName)
        {
            if(_childBuilderDict.ContainsKey(fullName) == true)
            {
                return _childBuilderDict[fullName];
            }
            if(COMPONENT_NAME_TESTER.IsMatch(fullName) == true)
            {
                string[] tokenList = fullName.Split(FULL_NAME_SPLITER);
                ComponentBuilder builder = GetBuilder(tokenList[0], tokenList[1]);
                _childBuilderDict.Add(fullName, builder);
                return builder;
            }
            return null;
        }

        private static ComponentBuilder GetBuilder(string type, string name)
        {
            if(specificBuilderDict.ContainsKey(name) == true)
            {
                ComponentBuilder builder = specificBuilderDict[name];
                if(builder.Type == type)
                {
                    return builder;
                }
            }
            if(typeBuilderDict.ContainsKey(type) == true)
            {
                return typeBuilderDict[type];
            }
            return typeBuilderDict[TYPE_CONTAINER];
        }

        public abstract string Type{get;}

        public abstract void Build(GameObject go, bool interactable);

        public virtual string Name { get { return string.Empty; } }

        protected GameObject CreateGameObject(GameObject parent, string name, float x, float y, float w, float h)
        {
            GameObject go = new GameObject(name);
            go.transform.SetParent(parent.transform, false);
            RectTransform rectTransform = go.AddComponent<RectTransform>();
            rectTransform.pivot = new Vector2(0, 1);
            rectTransform.anchorMin = new Vector2(0, 1);
            rectTransform.anchorMax = new Vector2(0, 1);
            rectTransform.anchoredPosition = new Vector2(x, y);
            rectTransform.sizeDelta = new Vector2(w, h);
            return go;
        }

        protected GameObject CreateGameObject(GameObject parent, string name, RectTransform rect)
        {
            GameObject go = new GameObject(name);
            go.transform.SetParent(parent.transform, false);
            RectTransform rectTransform = go.AddComponent<RectTransform>();
            rectTransform.pivot = new Vector2(0, 1);
            rectTransform.anchorMin = new Vector2(0, 1);
            rectTransform.anchorMax = new Vector2(0, 1);
            rectTransform.anchoredPosition = rect.anchoredPosition;
            rectTransform.sizeDelta = rect.sizeDelta;
            return go;
        }

        protected void AttachToParent(GameObject child, GameObject parent)
        {
            if(parent != null)
            {
                child.transform.SetParent(parent.transform);
            }
        }

        protected string GetParam(GameObject go)
        {
            BuildHelper helper = go.GetComponent<BuildHelper>();
            if(helper != null)
            {
                return go.GetComponent<BuildHelper>().param;
            }
            return string.Empty;
        }
    }
}
