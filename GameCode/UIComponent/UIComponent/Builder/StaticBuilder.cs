﻿using UnityEngine;

namespace Game.UI.Builder
{
    public class StaticBuilder : ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_STATIC;
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
        }
    }
}
