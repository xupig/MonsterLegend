﻿using UnityEngine;
using Game.UI.UIComponent;

namespace Game.UI.Builder
{
    public class ToggleBuilder : ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_TOGGLE; 
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, false);
            go.AddComponent<KToggle>();
        }
    }
}
