﻿using UnityEngine;
using Game.UI.UIComponent;

namespace Game.UI.Builder
{
    public class ScrollViewBuilder : ContainerBuilder
    {
        private GameObject _maskGo;
        private GameObject _contentGo;
        private GameObject _arrowGo;

        public override string Type
        {
            get
            {
                return TYPE_SCROLL_VIEW;
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, true);
            Restructure(go);
            CheckContent(go);
            AddScrollViewComponent(go);
        }

        private void Restructure(GameObject go)
        {
            _maskGo = KComponentUtil.GetChild(go, "mask");
            _contentGo = KComponentUtil.GetChild(go, "mask/content");
            _arrowGo = KComponentUtil.GetChild(go, "arrow");
            RectTransform contentRect = _contentGo.GetComponent<RectTransform>();
            contentRect.anchoredPosition = new Vector2(0, 0);
            KScrollRect scrollRect = _maskGo.AddComponent<KScrollRect>();
            scrollRect.content = contentRect;
            scrollRect.horizontal = false;
            scrollRect.vertical = false;
        }

        private void CheckContent(GameObject go)
        {
            KList list = KComponentUtil.GetChildComponent<KList>(go, "mask/content");
            if(list != null)
            {
                list.isItemAddRenderingAgent = true;
            }
        }

        private void AddScrollViewComponent(GameObject go)
        {
            GameObject arrowGo = KComponentUtil.GetChild(go, "arrow");
            if (arrowGo != null)
            {
                arrowGo.AddComponent<ScrollViewArrow>();
            }
            go.AddComponent<KScrollView>();
        }
    }
}
