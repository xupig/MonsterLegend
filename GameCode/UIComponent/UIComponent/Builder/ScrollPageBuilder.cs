﻿using UnityEngine;
using Game.UI.UIComponent;

namespace Game.UI.Builder
{
    public class ScrollPageBuilder : ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_SCROLL_PAGE;
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            BuildChildren(go, true);
            CheckContent(go);
            AddScrollPageComponent(go);
        }

        private void Restructure(GameObject go)
        {
            GameObject maskGo = KComponentUtil.GetChild(go, "mask");
            RectTransform maskRect = maskGo.GetComponent<RectTransform>();
            GameObject contentGo = KComponentUtil.GetChild(go, "content");
            AttachToParent(contentGo, maskGo);
            RectTransform contentRect = contentGo.GetComponent<RectTransform>();
            contentRect.anchoredPosition = new Vector2(0, 0);
            contentRect.sizeDelta = maskRect.sizeDelta;
        }

        private void CheckContent(GameObject go)
        {
            KList list = KComponentUtil.GetChildComponent<KList>(go, "mask/content");
            if(list != null)
            {
                list.isItemAddRenderingAgent = true;
            }
        }

        private void AddScrollPageComponent(GameObject go)
        {
            GameObject arrowGo = KComponentUtil.GetChild(go, "arrow");
            if(arrowGo != null)
            {
                arrowGo.AddComponent<ScrollPageArrow>();
            }
            GameObject pagerGo = KComponentUtil.GetChild(go, "pager");
            if(pagerGo != null)
            {
                pagerGo.AddComponent<ScrollPageDisplay>();
            }
            go.AddComponent<KScrollPage>();
        }
    }
}
