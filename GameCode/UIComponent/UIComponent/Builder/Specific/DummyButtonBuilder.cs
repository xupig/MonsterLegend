﻿using UnityEngine;
using UnityEngine.UI;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;

namespace Game.UI.Builder
{
    public class DummyButtonBuilder :ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_CONTAINER;
            }
        }

        public override string Name
        {
            get
            {
                return "dummyBtn";
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            go.AddComponent<KDummyButton>();
            SetBackgroundTransparent(go);
        }

        private void SetBackgroundTransparent(GameObject go)
        {
            ImageWrapper imageWrapper = go.transform.GetComponentInChildren<ImageWrapper>();
            if (imageWrapper != null)
            {
                imageWrapper.color = new Color(0, 0, 0, 0);
            }
        }
    }
}
