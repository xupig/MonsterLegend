﻿using UnityEngine;

namespace Game.UI.Builder
{
    public class ItemBuilder : ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_CONTAINER;
            }
        }

        public override string Name
        {
            get
            {
                return "item";
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
           
        }
    }
}
