﻿using UnityEngine;
using UnityEngine.UI;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;

namespace Game.UI.Builder
{
    public class MaskBuilder : ContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_SCALE_IMAGE;
            }
        }

        public override string Name
        {
            get
            {
                return "mask";
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            base.Build(go, interactable);
        }
    }
}
