﻿using UnityEngine;
using UnityEngine.UI;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;

namespace Game.UI.Builder
{
    public class UIAnchorResizer:MonoBehaviour
    {
        public const int PANEL_WIDTH = 1280;
        public const int PANEL_HEIGHT = 720;

        public Vector3 oldPosition;
        public string type;

        public int oldScreenW;
        public int oldScreenH;

        private KComponentEvent _onChnage;
        public KComponentEvent onChnage
        {
            get
            {
                if(_onChnage == null)
                {
                    _onChnage = new KComponentEvent();
                }
                return _onChnage;
            }
        }

        protected float CalculateCanvasScaleFactor()
        {
            float scaleW = (float)Screen.width / PANEL_WIDTH;
            float scaleH = (float)Screen.height / PANEL_HEIGHT;
            return scaleW < scaleH ? scaleW : scaleH;
        }

        void Update()
        {
            if (oldScreenW != Screen.width || oldScreenH != Screen.height)
            {
                oldScreenW = Screen.width;
                oldScreenH = Screen.height;
                OnResize();
            }
        }

        private void OnResize()
        {
            float scale = CalculateCanvasScaleFactor();
            switch (type)
            {
                case "topLeft":
                    break;
                case "topCenter":
                    transform.localPosition = new Vector3(oldPosition.x + (Screen.width - PANEL_WIDTH * scale) / (2 * scale), oldPosition.y, 0);
                    break;
                case "topRight":
                    transform.localPosition = new Vector3(oldPosition.x + (Screen.width - PANEL_WIDTH * scale) / scale, oldPosition.y, 0);
                    break;
                case "middleLeft":
                    transform.localPosition = new Vector3(oldPosition.x, oldPosition.y - (Screen.height - PANEL_HEIGHT * scale) / (2 * scale), 0);
                    break;
                case "middleCenter":
                    transform.localPosition = new Vector3(oldPosition.x + (Screen.width - PANEL_WIDTH * scale) / (2 * scale), oldPosition.y - (Screen.height - PANEL_HEIGHT * scale) / (2 * scale), 0);
                    break;
                case "middleRight":
                    transform.localPosition = new Vector3(oldPosition.x + (Screen.width - PANEL_WIDTH * scale) / scale, oldPosition.y - (Screen.height - PANEL_HEIGHT * scale) / (2 * scale), 0);
                    break;
                case "bottomLeft":
                    transform.localPosition = new Vector3(oldPosition.x, oldPosition.y - (Screen.height - PANEL_HEIGHT * scale) / scale, 0);
                    break;
                case "bottomCenter":
                    transform.localPosition = new Vector3(oldPosition.x + (Screen.width - PANEL_WIDTH * scale) / (2 * scale), oldPosition.y - (Screen.height - PANEL_HEIGHT * scale) / scale, 0);
                    break;
                case "bottomRight":
                    transform.localPosition = new Vector3(oldPosition.x + (Screen.width - PANEL_WIDTH * scale) / scale, oldPosition.y - (Screen.height - PANEL_HEIGHT * scale) / scale, 0);
                    break;
            }
            if(_onChnage!=null)
            {
                _onChnage.Invoke();
            }
        }
    }

    public abstract class AnchoredContainerBuilder : ContainerBuilder
    {
        public const int PANEL_WIDTH = 1280;
        public const int PANEL_HEIGHT = 720;

        public override string Type
        {
            get
            {
                return TYPE_CONTAINER;
            }
        }

        public override void Build(GameObject go, bool interactable)
        {
            base.Build(go, interactable);
            RectTransform transform = go.GetComponent<RectTransform>();
            AddUIAnchorResizer(transform);
            AdjustPosition(transform);
            
        }

        protected abstract void AdjustPosition(RectTransform transform);

        protected float CalculateCanvasScaleFactor()
        {
            float scaleW = (float)Screen.width / PANEL_WIDTH;
            float scaleH = (float)Screen.height / PANEL_HEIGHT;
            return scaleW < scaleH ? scaleW : scaleH;
        }

        public void AddUIAnchorResizer(RectTransform transform)
        {
            UIAnchorResizer resizer = transform.gameObject.GetComponent<UIAnchorResizer>();
            if(resizer == null)
            {
                resizer = transform.gameObject.AddComponent<UIAnchorResizer>();
            }
            resizer.oldScreenW = Screen.width;
            resizer.oldScreenH = Screen.height;
            resizer.type = Name;
            resizer.oldPosition = transform.localPosition;
        }
    }

   public class TopLeftContainerBuilder : AnchoredContainerBuilder
   {
       public override string Type
       {
           get
           {
               return TYPE_CONTAINER;
           }
       }

       public override string Name
       {
           get
           {
               return "topLeft";
           }
       }

       protected override void AdjustPosition(RectTransform transform)
       {
            //Noting  
       }
   }

   public class TopCenterContainerBuilder : AnchoredContainerBuilder
   {
       public override string Type
       {
           get
           {
               return TYPE_CONTAINER;
           }
       }

       public override string Name
       {
           get
           {
               return "topCenter";
           }
       }

       protected override void AdjustPosition(RectTransform transform)
       {
           Vector3 position = transform.localPosition;
           float scale = CalculateCanvasScaleFactor();
           transform.localPosition = new Vector3(position.x + (Screen.width - PANEL_WIDTH * scale) / (2*scale), position.y, 0);
       }
   }

    public class TopRightContainerBuilder : AnchoredContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_CONTAINER;
            }
        }

        public override string Name
        {
            get
            {
                return "topRight";
            }
        }

        protected override void AdjustPosition(RectTransform transform)
        {
            Vector3 position = transform.localPosition;
            float scale = CalculateCanvasScaleFactor();
            transform.localPosition = new Vector3(position.x + (Screen.width - PANEL_WIDTH * scale) / scale, position.y, 0);
        }
    }

    public class MiddleLeftContainerBuilder : AnchoredContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_CONTAINER;
            }
        }

        public override string Name
        {
            get
            {
                return "middleLeft";
            }
        }

        protected override void AdjustPosition(RectTransform transform)
        {
            Vector3 position = transform.localPosition;
            float scale = CalculateCanvasScaleFactor();
            transform.localPosition = new Vector3(position.x, position.y - (Screen.height - PANEL_HEIGHT * scale) / (2 * scale), 0);
        }
    }

    public class MiddleCenterContainerBuilder : AnchoredContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_CONTAINER;
            }
        }

        public override string Name
        {
            get
            {
                return "middleCenter";
            }
        }

        protected override void AdjustPosition(RectTransform transform)
        {
            Vector3 position = transform.localPosition;
            float scale = CalculateCanvasScaleFactor();
            transform.localPosition = new Vector3(position.x + (Screen.width - PANEL_WIDTH * scale) / (2 * scale), position.y - (Screen.height - PANEL_HEIGHT * scale) / (2 * scale), 0);
        }
    }

    public class MiddleRightContainerBuilder : AnchoredContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_CONTAINER;
            }
        }

        public override string Name
        {
            get
            {
                return "middleRight";
            }
        }

        protected override void AdjustPosition(RectTransform transform)
        {
            Vector3 position = transform.localPosition;
            float scale = CalculateCanvasScaleFactor();
            transform.localPosition = new Vector3(position.x + (Screen.width - PANEL_WIDTH * scale) / scale, position.y - (Screen.height - PANEL_HEIGHT * scale) / (2 * scale), 0);
        }
    }

    public class BottomLeftContainerBuilder : AnchoredContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_CONTAINER;
            }
        }

        public override string Name
        {
            get
            {
                return "bottomLeft";
            }
        }

        protected override void AdjustPosition(RectTransform transform)
        {
            Vector3 position = transform.localPosition;
            float scale = CalculateCanvasScaleFactor();
            transform.localPosition = new Vector3(position.x, position.y - (Screen.height - PANEL_HEIGHT * scale) / scale, 0);
        }
    }


    public class BottomCenterContainerBuilder : AnchoredContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_CONTAINER;
            }
        }

        public override string Name
        {
            get
            {
                return "bottomCenter";
            }
        }

        protected override void AdjustPosition(RectTransform transform)
        {
            Vector3 position = transform.localPosition;
            float scale = CalculateCanvasScaleFactor();
            transform.localPosition = new Vector3(position.x + (Screen.width - PANEL_WIDTH * scale) / (2 * scale), position.y - (Screen.height - PANEL_HEIGHT * scale) / scale, 0);
        }
    }
    

    public class BottomRightContainerBuilder : AnchoredContainerBuilder
    {
        public override string Type
        {
            get
            {
                return TYPE_CONTAINER;
            }
        }

        public override string Name
        {
            get
            {
                return "bottomRight";
            }
        }

        protected override void AdjustPosition(RectTransform transform)
        {
            Vector3 position = transform.localPosition;
            float scale = CalculateCanvasScaleFactor();
            transform.localPosition = new Vector3(position.x + (Screen.width - PANEL_WIDTH * scale) / scale, position.y - (Screen.height - PANEL_HEIGHT * scale) / scale , 0);
        }
    }
}
