﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;


namespace Game.UI.UIComponent
{
    public class ScrollPageArrow : KContainer
    {
        private StateImage _nextImage;
        private StateImage _prevImage;

        protected override void Awake()
        {
            _nextImage = GetChildComponent<StateImage>("Image_arrowNext");
            _prevImage = GetChildComponent<StateImage>("Image_arrowPrev");
        }

        public void UpdateArrow(int current, int total)
        {
            HideArrow();
            if(current > 0)
            {
                _prevImage.Alpha = 1.0f;
            }
            if(current < total - 1)
            {
                _nextImage.Alpha = 1.0f;
            }
        }

        private void HideArrow()
        {
            _nextImage.Alpha = 0;
            _prevImage.Alpha = 0;
        }
    }

}
