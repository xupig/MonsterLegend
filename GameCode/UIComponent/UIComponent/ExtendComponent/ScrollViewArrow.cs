﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;


namespace Game.UI.UIComponent
{
    public class ScrollViewArrow : KContainer
    {
        public const float DEFAULT_HIDE_VALUE = 0.1f;

        private StateImage _nextImage;
        private StateImage _prevImage;

        private float _hideRate = DEFAULT_HIDE_VALUE;

        public float HideRate
        {
            get { return _hideRate; }
            set
            {
                _hideRate = value;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            _nextImage = GetChildComponent<StateImage>("Image_arrowNext");
            _prevImage = GetChildComponent<StateImage>("Image_arrowPrev");
        }

        public void UpdateArrow(RectTransform maskRect, RectTransform contentRect, bool isVertical)
        {
            HideArrow();
            if(isVertical == true)
            {
                float maskHeight = maskRect.rect.yMax - maskRect.rect.yMin;
                float contentHeight = contentRect.rect.yMax - contentRect.rect.yMin;

                if(contentRect.localPosition.y / maskHeight > HideRate)
                {
                    _prevImage.Alpha = 1.0f;
                }
                if((contentHeight - (Mathf.Abs(contentRect.localPosition.y) + maskHeight)) / maskHeight > HideRate)
                {
                    _nextImage.Alpha = 1.0f;
                }
            }
            else
            {
                float maskWidth = maskRect.rect.xMax - maskRect.rect.xMin;
                float contentWidth = contentRect.rect.xMax - contentRect.rect.xMin;

                if(contentRect.localPosition.x / maskWidth < -HideRate)
                {
                    _prevImage.Alpha = 1.0f;
                }
                if((contentWidth - (Mathf.Abs(contentRect.localPosition.x) + maskWidth)) / maskWidth > HideRate)
                {
                    _nextImage.Alpha = 1.0f;
                }
            }
        }

        private void HideArrow()
        {
            _nextImage.Alpha = 0;
            _prevImage.Alpha = 0;
        }
    }
}
