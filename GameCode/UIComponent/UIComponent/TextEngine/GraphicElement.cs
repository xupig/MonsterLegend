﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Game.UI.TextEngine
{
    public class GraphicElement : ContentElement
    {
        private static int _index = 0;

        private int _elementIndex = 0;
        private int _fragmentIndex = 0;
        private bool _isFinished = false;


        public GraphicElement(string content, TextBlock textBlock)
            : base(content, textBlock)
        {
            _elementIndex = (_index++);
        }

        public override void PopulateContent()
        {
           
        }

        public override GameObject GetFragment(GameObject parent, float availableWidth)
        {
            if(availableWidth < GetFragmentSize().x)
            {
                _isFinished = false;
                return null;
            }
            GameObject go = new GameObject(GetFragmentName());
            GameObjectHelper.AddToParent(go, parent);
            GameObjectHelper.AddBottomLeftRectTransform(go, Vector2.zero, Vector2.zero);
            AddFragmentComponent(go);
            GameObjectHelper.ResizeRectTransform(go, GetFragmentSize());
            GameObjectHelper.MoveRectTransform(go, GetFragmentPostion());
            _isFinished = true;
            return go;
        }

        public virtual bool CheckIsOutOfLineWidth(float lineWidth)
        {
            if (lineWidth < GetFragmentSize().x)
            {
                return true;
            }
            return false;
        }

        protected virtual void AddFragmentComponent(GameObject go)
        {
         
        }

        protected override string GetFragmentName()
        {
            return string.Format("graphicFragment_{0}_{1}", _elementIndex.ToString(), _fragmentIndex.ToString());
        }

        protected override Vector2 GetFragmentPostion()
        {
            return Vector2.zero;
        }

        protected override Vector2 GetFragmentSize()
        {
            return Vector2.zero;
        }

        public override bool IsFinished
        {
            get
            {
                return _isFinished;
            }
        }

        public void Abort()
        {
            _isFinished = true;
        }
    }
}
