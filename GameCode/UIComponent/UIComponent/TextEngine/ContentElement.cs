﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

namespace Game.UI.TextEngine
{
    public abstract class ContentElement
    {
        public TextBlock TextBlock{get; private set;}
        public string Content{get; protected set;}

        public ContentElement(string content, TextBlock textBlock)
        {
            this.Content = content;
            this.TextBlock = textBlock;
        }

        public abstract GameObject GetFragment(GameObject parent, float availableWidth);
        protected abstract string GetFragmentName();

        public abstract void PopulateContent();
        public abstract bool IsFinished{get;}

        protected abstract Vector2 GetFragmentPostion();
        protected abstract Vector2 GetFragmentSize();
    }
}
