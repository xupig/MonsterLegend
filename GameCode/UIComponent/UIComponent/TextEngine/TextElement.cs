﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;

namespace Game.UI.TextEngine
{
    public class TextElement : ContentElement
    {
        private static int _index = 0;
        private TextGenerator _textGenerator = new TextGenerator();

        private int _elementIndex = 0;
        private int _lastCharIndex = 0;

        protected string _fragmentContent;
        protected float _fragmentWidth;
        private float _fragmentHeight;
        private int _fragmentIndex = 0;
        private bool _isLineBreak = false;
        private int _lineBreakSignCount = 0;


        public static float GetTextPreferredWidth(string content)
        {
            return 20.0f;
        }

        public static float GetTextPreferredHeight(string content)
        {
            return 31.0f;
        }
        
        public TextElement(string elementContent, TextBlock textBlock) : base(elementContent, textBlock)
        {
            _elementIndex = (_index++);
            _lastCharIndex = 0;   
        }

        public override void PopulateContent()
        {
            if(_textGenerator.Populate(this.Content, TextBlock.textGenerationSettings) == false)
            {
                throw new Exception(string.Format("Populate content {0} error!!", this.Content));
            }
        }

        public override GameObject GetFragment(GameObject parent, float availableWidth)
        {
            DigestContent(availableWidth);
            if(string.IsNullOrEmpty(_fragmentContent) == true)
            {
                return null;
            }
            GameObject go = new GameObject(GetFragmentName());
            GameObjectHelper.AddToParent(go, parent);
            GameObjectHelper.AddBottomLeftRectTransform(go, Vector2.zero, Vector2.zero);
            AddFragmentComponent(go);
            GameObjectHelper.ResizeRectTransform(go, GetFragmentSize());
            GameObjectHelper.MoveRectTransform(go, GetFragmentPostion());
            return go;
        }

        protected override string GetFragmentName()
        {
            return string.Format("textFragment_{0}_{1}", _elementIndex.ToString(), (_fragmentIndex++).ToString());
        }

        protected override Vector2 GetFragmentPostion()
        {
            return Vector2.zero;
        }

        protected override Vector2 GetFragmentSize()
        {
            if (!_isLineBreak)
            {
                return TextBlock.GetTextContentSize(_fragmentContent);
            }
            else
            {
                Vector2 size = TextBlock.GetTextContentSize(_fragmentContent);
                size.x = _fragmentWidth;
                if (_lineBreakSignCount > 1)
                {
                    size.y = _lineBreakSignCount * size.y;
                }
                return size;
            }
        }

        protected virtual void AddFragmentComponent(GameObject go)
        {
            Text text = AddTextComponent(go);
            text.alignment = TextBlock.textGenerationSettings.textAnchor;
            text.color = TextBlock.textGenerationSettings.color;
            text.font = TextBlock.textGenerationSettings.font;
            text.fontSize = TextBlock.textGenerationSettings.fontSize;
            text.fontStyle = TextBlock.textGenerationSettings.fontStyle;
            text.lineSpacing = TextBlock.textGenerationSettings.lineSpacing;
            text.supportRichText = TextBlock.textGenerationSettings.richText;
            string leftTag = string.Empty, rightTag = string.Empty;
            if(TextBlock.textGenerationSettings.richText == true)
            {
                leftTag = this.TextBlock.HtmlHelper.GetLeftTag();
                this.TextBlock.HtmlHelper.UpdateTagMatchStack(_fragmentContent);
                rightTag = this.TextBlock.HtmlHelper.GetRightTag();
            }
            _fragmentContent = string.Format("{0}{1}{2}", leftTag, _fragmentContent, rightTag);
            text.text = _fragmentContent;
        }

        protected virtual Text AddTextComponent(GameObject go)
        {
            return go.AddComponent<Text>();
        }

        protected void DigestContent(float availableWidth)
        {
            _fragmentContent = string.Empty;
            _fragmentWidth = 0;
            _fragmentHeight = 0;
            _isLineBreak = false;
            _lineBreakSignCount = 0;
            int index = _lastCharIndex;
            while(index < this.Content.Length && _fragmentWidth < availableWidth)
            {
                if(IsLineBreak(index) == true)
                {
                    _fragmentWidth = availableWidth;
                    index++;
                    _isLineBreak = true;
                    break;
                }
                if(IsValidateChar(index) == false)
                {
                    index++;
                    continue;
                }
                float charWidth = _textGenerator.characters[index].charWidth;
                if((_fragmentWidth + charWidth) > availableWidth)
                {
                    break;
                }
                _fragmentWidth += charWidth;
                index++;
            }
            if(index != _lastCharIndex)
            {
                int startIndex = index;
                if(IsLineBreak(index - 1) == true)
                {
                    startIndex = index - 1;
                }
                _fragmentContent = this.Content.Substring(_lastCharIndex, (startIndex - _lastCharIndex));
                _lastCharIndex = index;
            }
            _fragmentWidth += 2;
            if (_isLineBreak)
            {
                _lineBreakSignCount++;
                int curCharIndex = _lastCharIndex;
                while (curCharIndex < this.Content.Length && IsLineBreak(curCharIndex))
                {
                    _lineBreakSignCount++;
                    curCharIndex++;
                }
            }
        }

        private bool IsValidateChar(int index)
        {
            if(IsBlank(index) == true)
            {
                return true;
            }
            UIVertex vertex0 = _textGenerator.verts[index * 4];
            UIVertex vertex1 = _textGenerator.verts[index * 4 + 1];
            if((vertex1.position.x - vertex0.position.x) == 0)
            {
                return false;
            }
            return true;
        }

        private bool IsBlank(int index)
        {
            if(this.Content.Substring(index, 1) == " ")
            {
                return true;
            }
            return false;
        }

        private bool IsLineBreak(int index)
        {
            if(this.Content.Substring(index, 1) == "\n")
            {
                return true;
            }
            return false;
        }

        public override bool IsFinished
        {
            get
            {
                return _lastCharIndex == this.Content.Length;
            }
        }
    }
}

