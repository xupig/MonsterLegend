using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// KToggle.为单选按钮的类
/// 必须子GameObject：checkmark
/// </summary>
namespace Game.UI.UIComponent
{
    public class KToggle : Toggle, IKContainer, IPointerUpHandler, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        public const string CHILD_CHECKMARK = "checkmark";
        public const string CHILD_BACK = "back";

        /// <summary>
        /// 该属性只在KToggleGroup初始化时赋值
        /// </summary>
        public int Index { get; set; }

        private string _currentState;
        private List<IStateChangeable> _stateChangeableChildren;
        private GameObject _checkmarkGo;
        private GameObject _backGo;
        private bool _enableAudio = true;
        public new KComponentEvent<KToggle, bool> onValueChanged = new KComponentEvent<KToggle, bool>();

        protected override void Awake()
        {
            CreateChildren();
            Initialize();
            AddEventListener();
        }

        private void CreateChildren()
        {
            _stateChangeableChildren = GetStateChangeableChildren();
            _checkmarkGo = GetChild(CHILD_CHECKMARK);
            _backGo = GetChild(CHILD_BACK);
        }

        private void Initialize()
        {
            this.transition = Transition.None;
            this.toggleTransition = ToggleTransition.None;
            SetState(KButtonState.NORMAL);
            ToggleCheckmark();
        }

        private GameObject _greenPoint;
        private GameObject _greenPointSelected;

        private bool hasInitial = false;

        private void InitGreenPoint()
        {
            if (hasInitial)
            {
                return;
            }
            hasInitial = true;
            _greenPoint = GetChild("image/Spotlight");
            _greenPointSelected = GetChild("checkmark/Spotlight");
        }

        public void SetGreenPoint(bool isShowRed)
        {
            InitGreenPoint();
            if (_greenPoint!=null)
            {
                _greenPoint.SetActive(isShowRed);
            }
            if (_greenPointSelected!=null)
            {
                _greenPointSelected.SetActive(isShowRed);
            }
        }

        public void SetLabel(string content)
        {
            InitStateText();
            _textLabel.ChangeAllStateText(content);
            _textLabelSelected.ChangeAllStateText(content);
        }

        private bool _initStateText = false;
        private StateText _textLabel;
        private StateText _textLabelSelected;

        private void InitStateText()
        {
            if (_initStateText)
            {
                return;
            }
            _initStateText = true;
            _textLabel = GetChildComponent<StateText>("image/label");
            _textLabelSelected = GetChildComponent<StateText>("checkmark/label");
        }

        private void AddEventListener()
        {
            base.onValueChanged.AddListener(OnValueChanged);
        }

        override public void OnPointerDown(PointerEventData evtData)
        {
            base.OnPointerDown(evtData);
            SetState(KButtonState.DOWN);
        }

        override public void OnPointerUp(PointerEventData evtData)
        {
            base.OnPointerUp(evtData);
            SetState(KButtonState.OVER);
        }

        override public void OnPointerEnter(PointerEventData evtData)
        {
            base.OnPointerEnter(evtData);
            SetState(KButtonState.OVER);
        }

        override public void OnPointerExit(PointerEventData evtData)
        {
            base.OnPointerExit(evtData);
            SetState(KButtonState.NORMAL);
        }

        public override void OnPointerClick(PointerEventData evtData)
        {
            base.OnPointerClick(evtData);
            bool oldIsOn = this.isOn;
            ToggleCheckmark();
            if(oldIsOn == false && this.isOn == true)
            {
                PlayClickAudio();
            }
            if(this.isOn == true)
            {
                foreach(IStateChangeable changealbe in KComponentUtil.GetStateChangeableChildren(_checkmarkGo))
                {
                    changealbe.State = KButtonState.OVER;
                }
            }
            onValueChanged.Invoke(this, this.isOn);
        }

        private void SetState(string state)
        {
            if(_currentState == state)
            {
                return;
            }
            _currentState = state;
            foreach(IStateChangeable child in _stateChangeableChildren)
            {
                child.State = _currentState;
            }
        }

        private void OnValueChanged(bool isOn)
        {
            ToggleCheckmark();
        }

        private void PlayClickAudio()
        {
            if(EnableAudio == true)
            {
                KComponentAudioCollective.PlayClickAudio(this.gameObject);
            }
        }

        //巩靖加入
        private void SetGameObjectState(GameObject go, string state)
        {
            foreach (IStateChangeable child in KComponentUtil.GetStateChangeableChildren(go))
            {
                child.State = _currentState;
            }
        }

        public void ToggleCheckmark()
        {
            if(_checkmarkGo!=null)
            {
                _checkmarkGo.SetActive(this.isOn);
            }
            if(_backGo != null)
            {
                _backGo.SetActive(!this.isOn);
                //巩靖加入
                if (isOn == false)
                {
                    SetGameObjectState(_backGo, KButtonState.NORMAL);
                }
            }
        }

        public GameObject GetChild(string childPath)
        {
            return KComponentUtil.GetChild(this.gameObject, childPath);
        }

        public T GetChildComponent<T>(string childPath) where T : UnityEngine.Component
        {
            return GetChild(childPath).GetComponent<T>();
        }

        public T AddChildComponent<T>(string childPath) where T : Component
        {
            return GetChild(childPath).AddComponent<T>();
        }

        public List<IStateChangeable> GetStateChangeableChildren()
        {
            return KComponentUtil.GetStateChangeableChildren(this.gameObject);
        }

        public void SetButtonActive()
        {
            ImageWrapper[] images = GetComponentsInChildren<ImageWrapper>(true);
            for (int i = 0; i < images.Length; i++)
            {
                images[i].SetGray(1);
            }
            interactable = true;
        }

        public void SetButtonDisable()
        {
            ImageWrapper[] images = GetComponentsInChildren<ImageWrapper>(true);
            for (int i = 0; i < images.Length; i++)
            {
                images[i].SetGray(0);
            }
            interactable = false;
        }

        public new bool interactable
        {
            get
            {
                return base.interactable;
            }
            set
            {
                base.interactable = value;
            }
        }

        public bool Visible
        {
            get
            {
                return gameObject.activeSelf;
            }
            set
            {
                gameObject.SetActive(value);
            }
        }

        public bool EnableAudio
        {
            get
            {
                return _enableAudio;
            }
            set
            {
                _enableAudio = value;
            }
        }

    }
}
