﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Game.UI.UIComponent
{
    public class KScrollView : KContainer
    {
        private GameObject _maskGo;
        private KScrollRect _scrollRect;
        private GameObject _contentGo;
        private ScrollViewArrow _arrow;

        protected override void Awake()
        {
            base.Awake();
            CreateChildren();
            _scrollRect.vertical = true;
        }

        protected override void OnEnable()
        {
            _scrollRect.onValueChanged.AddListener(OnScrollRectValueChanged);
        }

        protected override void OnDisable()
        {
            _scrollRect.onValueChanged.RemoveListener(OnScrollRectValueChanged);
        }

        private void CreateChildren()
        {
            _maskGo = GetChild("mask");
            _scrollRect = _maskGo.GetComponent<KScrollRect>();
            _contentGo = GetChild("mask/content");
            GameObject arrowGo = GetChild("arrow");
            if(arrowGo != null)
            {
                _arrow = arrowGo.GetComponent<ScrollViewArrow>();
                UpdateArrow();
            }
        }

        public Mask Mask
        {
            get
            {
                return _maskGo.GetComponent<Mask>();
            }
        }

        public GameObject Content
        {
            get
            {
                return _contentGo;
            }
        }

        public KScrollRect ScrollRect
        {
            get
            {
                return _scrollRect;
            }
        }

        public ScrollViewArrow Arrow
        {
            get
            {
                return _arrow;
            }
        }

        private void OnScrollRectValueChanged(Vector2 position)
        {
            UpdateArrow();
        }

        public void UpdateArrow()
        {
            if(_arrow == null)
            {
                return;
            }
            _arrow.UpdateArrow(_maskGo.GetComponent<RectTransform>(), _contentGo.GetComponent<RectTransform>(), _scrollRect.vertical);
        }

        public void RefreshContent()
        {
            _scrollRect.content = _contentGo.GetComponent<RectTransform>();
        }

        public void ResetContentPosition()
        {
            _contentGo.GetComponent<RectTransform>().localPosition = Vector2.zero;
            UpdateArrow();
        }

        /// <summary>
        /// KScrollView的辅助类
        /// 当Content高度或宽度小于Mask相应值时，关闭Mask和ScrollRect功能
        /// </summary>
        public class KScrollViewContent : UIBehaviour
        {
            public GameObject maskGo;
            public GameObject arrowGo;

            protected override void OnRectTransformDimensionsChange()
            {
                base.OnRectTransformDimensionsChange();
                UpdateUsability();
            }

            private void UpdateUsability()
            {
                if(maskGo == null)
                {
                    return;
                }
                RectTransform rect = GetComponent<RectTransform>();
                Mask mask = maskGo.GetComponent<Mask>();
                RectTransform maskRect = maskGo.GetComponent<RectTransform>();
                KScrollRect scrollRect = maskGo.GetComponent<KScrollRect>();
                if(scrollRect.vertical == true)
                {
                    if(rect.sizeDelta.y <= maskRect.sizeDelta.y)
                    {
                        mask.enabled = false;
                        scrollRect.verticalNormalizedPosition = 1;
                        scrollRect.enabled = false;
                        HideArrow();
                    }
                    else
                    {
                        mask.enabled = true;
                        scrollRect.enabled = true;
                        ShowArrow();
                    }
                }
                else
                {
                    if(rect.sizeDelta.x <= maskRect.sizeDelta.x)
                    {
                        mask.enabled = false;
                        scrollRect.horizontalNormalizedPosition = 0;
                        scrollRect.enabled = false;
                        HideArrow();
                    }
                    else
                    {
                        mask.enabled = true;
                        scrollRect.enabled = true;
                        ShowArrow();
                    }
                }
            }

            private void HideArrow()
            {
                if (arrowGo != null)
                {
                    arrowGo.SetActive(false);
                }
            }

            private void ShowArrow()
            {
                if (arrowGo != null)
                {
                    arrowGo.SetActive(true);
                }
            }
        }
    }

}
