﻿#region 模块信息
/*==========================================
// 文件名：KTree
// 命名空间: 
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/27 9:22:33
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.UI.UIComponent
{
    public class KTree : KContainer
    {
        public const string MENU_PREFIX = "menu_{0}";
        public const string DETAIL_PREFIX = "detail_{0}_{1}";
        public const int DEFAULT_GAP = 3;

        private GameObject _menuItemGo;
        private GameObject _detailItemGo;

        private RectTransform _menuRect;
        private RectTransform _detailRect;

        private int _menuTopToDownGap = DEFAULT_GAP;	//item列间距
        private int _itemTopToDownGap = DEFAULT_GAP;		//item行间距

        private KTreePadding _padding = new KTreePadding();

        protected List<KTreeMenuItem> _itemList = new List<KTreeMenuItem>();

        protected Dictionary<int, List<KTreeDetailItem>> _detailDict = new Dictionary<int, List<KTreeDetailItem>>();

        private KContainer _selectedItem;

        public KComponentEvent<KTree, KTreeMenuItem> onSelectedMenuIndexChanged = new KComponentEvent<KTree, KTreeMenuItem>();

        public KComponentEvent<KTree, KTreeDetailItem> onSelectedDetailIndexChanged = new KComponentEvent<KTree, KTreeDetailItem>();

        private HashSet<int> _hiddenSet;

        protected override void Awake()
        {
            base.Awake();
            _hiddenSet = new HashSet<int>();
            listRect = GetComponent<RectTransform>();
            InitTemplate();
        }

        public bool IsHidden(int menuId)
        {
            return _hiddenSet.Contains(menuId);
        }

        private void InitTemplate()
        {
            _menuItemGo = GetChild("menu");
            _menuItemGo.SetActive(false);
            _detailItemGo = GetChild("detail");
            _detailItemGo.SetActive(false);

            _menuRect = _menuItemGo.GetComponent<RectTransform>();
            _detailRect = _detailItemGo.GetComponent<RectTransform>();
        }

        public List<KTreeMenuItem> ItemList
        {
            get { return _itemList; }
        }

        public Dictionary<int, List<KTreeDetailItem>> DetailDict
        {
            get { return _detailDict; }
        }

        public virtual void SetPadding(int top, int right, int bottom, int left)
        {
            _padding.top = top;
            _padding.right = right;
            _padding.bottom = bottom;
            _padding.left = left;
        }

        public virtual void SetGap(int menuTopToDownGap, int itemTopToDownGap)
        {
            _menuTopToDownGap = menuTopToDownGap;
            _itemTopToDownGap = itemTopToDownGap;
        }

        public void RemoveAll()
        {
            RemoveMenuItemRange(0, _itemList.Count);
            RemoveAllDetailItem(false);
            UpdateItemListLayout();
        }

        private void RemoveMenuItemRange(int start, int count)
        {
            for (int i = start + count - 1; i >= start; i--)
            {
                KTreeMenuItem item = _itemList[i];
                RemoveMenuItemEventListener(item);
                _itemList.RemoveAt(i);
                item.Dispose();
                Destroy(item.gameObject);
            }
        }

        public void RemoveAllDetailItem(bool layoutImmediately = true)
        {
            List<int> list = new List<int>();
            foreach (KeyValuePair<int, List<KTreeDetailItem>> kvp in _detailDict)
            {
                list.Add(kvp.Key);
            }
            for (int i = 0; i < list.Count; i++)
            {
                RemoveDetailItemIndex(list[i], false);
            }
            if (layoutImmediately)
            {
                UpdateItemListLayout();
            }
        }

        public void RemoveDetailItemIndex(int index, bool layoutImmediately = true)
        {
            if (_detailDict.ContainsKey(index))
            {
                List<KTreeDetailItem> dataList = _detailDict[index];
                int count = dataList.Count;
                for (int i = count - 1; i >= 0; i--)
                {
                    KTreeDetailItem item = dataList[i];
                    RemoveDetailItemEventListener(item);
                    dataList.RemoveAt(i);
                    item.Dispose();
                    Destroy(item.gameObject);
                }
                if (layoutImmediately)
                {
                    UpdateItemListLayout();
                }
                _detailDict.Remove(index);
            }
        }

        public void SelectedItem(KContainer item)
        {
            if (_selectedItem != null)
            {
                SetItemSelectedState(_selectedItem, false);
            }
            SetItemSelectedState(item, true);
            _selectedItem = item;
        }

        public void SetSelectItem(int menuIndex,int detailIndex)
        {
            if(_detailDict.ContainsKey(menuIndex))
            {
               if(_detailDict[menuIndex].Count>detailIndex)
               {
                   SelectedItem(_detailDict[menuIndex][detailIndex]);
                   onSelectedDetailIndexChanged.Invoke(this, _detailDict[menuIndex][detailIndex]);
               }
            }
        }

        public void SetSelectItemByValue(int menuIndex, int detailValue)
        {
            if (_detailDict.ContainsKey(menuIndex))
            {
                for(int i=0;i<_detailDict[menuIndex].Count;i++)
                {
                    if(_detailDict[menuIndex][i].ID == detailValue)
                    {
                        SelectedItem(_detailDict[menuIndex][i]);
                        onSelectedDetailIndexChanged.Invoke(this, _detailDict[menuIndex][i]);
                    }
                }
            }
        }

        private void SetItemSelectedState(KContainer item, bool state)
        {
            if (item is KTreeMenuItem)
            {
                (item as KTreeMenuItem).IsSelected = state;
            }
            else if (item is KTreeDetailItem)
            {
                (item as KTreeDetailItem).IsSelected = state;
            }
        }

        public void SetMenuList<T>(IList dataList, int coroutineCreateCount = 0, bool layoutImmediately = false) where T : KTreeMenuItem
        {
            _hiddenSet.Clear();
            RemoveAll();
            if (coroutineCreateCount == 0)
            {
                for (int i = 0; i < dataList.Count; i++)
                {
                    AddMenuItem<T>(dataList[i]);
                }
                if (layoutImmediately)
                {
                    UpdateItemListLayout();
                }
            }
            else
            {
                StartCoroutine(AddMenuItemCoroutine<T>(dataList, coroutineCreateCount, layoutImmediately));
            }

        }

        private IEnumerator AddMenuItemCoroutine<T>(IList dataList, int coroutineCreateCount = 0, bool layoutImmediately = false) where T : KTreeMenuItem
        {
            int startIndex = 0;
            int endIndex = 0;
            while (endIndex < dataList.Count)
            {
                endIndex = Mathf.Min(dataList.Count, startIndex + coroutineCreateCount);
                for (int i = startIndex; i < endIndex; i++)
                {
                    AddMenuItem<T>(dataList[i]);
                }
                startIndex = endIndex;
                yield return null;
            }
            if (layoutImmediately == true)
            {
                UpdateItemListLayout();
            }
        }

        public void SetDetailList<T>(int menuIndex, IList dataList, int coroutineCount = 0, bool layoutImmediately = true) where T : KTreeDetailItem
        {
            if (_detailDict.ContainsKey(menuIndex))
            {
                if (_hiddenSet.Contains(menuIndex) == false)
                {
                    AddHiddenItem(menuIndex);
                }
                else
                {
                    RemoveHiddenItem(menuIndex);
                }
            }
            else
            {
                CreateDetailList<T>(menuIndex, dataList, coroutineCount, layoutImmediately);
            }
        }


        private void AddHiddenItem(int menuId)
        {
            if (_hiddenSet.Contains(menuId) == false)
            {
                _hiddenSet.Add(menuId);
            }
            if (_detailDict.ContainsKey(menuId))
            {
                List<KTreeDetailItem> dataList = _detailDict[menuId];
                for (int i = 0; i < dataList.Count; i++)
                {
                    dataList[i].gameObject.SetActive(false);
                }
            }
            UpdateItemListLayout();
        }

        private void RemoveHiddenItem(int menuId)
        {
            if (_hiddenSet.Contains(menuId))
            {
                _hiddenSet.Remove(menuId);
            }
            if (_detailDict.ContainsKey(menuId))
            {
                List<KTreeDetailItem> dataList = _detailDict[menuId];
                for (int i = 0; i < dataList.Count; i++)
                {
                    dataList[i].gameObject.SetActive(true);
                }
            }
            UpdateItemListLayout();
        }

        private void AddMenuItem<T>(System.Object data) where T : KTreeMenuItem
        {
            AddMenuItemAt<T>(data, _itemList.Count);
        }

        private void AddMenuItemAt<T>(System.Object data, int index) where T : KTreeMenuItem
        {
            GameObject itemGo = CreateMenuItemGo();
            T item = itemGo.AddComponent<T>();
            AddMenuItemAt(item, data, index);
        }

        private void AddMenuItemAt(KTreeMenuItem item, System.Object data, int index)
        {
            AddItemGoToHierarchy(item.gameObject);
            item.Index = index;
            item.Data = data;
            item.name = string.Format(MENU_PREFIX, index);
            _itemList.Insert(index, item);
            AddMenuItemEventListener(item);
        }

        private void AddMenuItemEventListener(KTreeMenuItem item)
        {
            item.onClick.AddListener(OnMenuItemClicked);
        }

        private void RemoveMenuItemEventListener(KTreeMenuItem item)
        {
            item.onClick.RemoveAllListeners();
        }

        private void AddDetailItemEventListener(KTreeDetailItem item)
        {
            item.onClick.AddListener(OnDetailItemClicked);
        }

        private void RemoveDetailItemEventListener(KTreeDetailItem item)
        {
            item.onClick.RemoveAllListeners();
        }

        private void OnDetailItemClicked(KTreeDetailItem item, int menuIndex, int index)
        {
            if (item == _selectedItem)
            {
                SetItemSelectedState(_selectedItem, false);
                return;
            }
            SelectedItem(item);
            onSelectedDetailIndexChanged.Invoke(this, item);
        }

        private void OnMenuItemClicked(KTreeMenuItem item, int index)
        {
            if(_selectedItem == item)
            {
                SetItemSelectedState(_selectedItem, !item.IsSelected);
            }
            else
            {
                SelectedItem(item);
            }
            onSelectedMenuIndexChanged.Invoke(this, item);
        }

        private void AddItemGoToHierarchy(GameObject itemGo)
        {
            itemGo.SetActive(true);
            itemGo.transform.SetParent(this.transform);
            itemGo.transform.localScale = Vector3.one;
        }

        private GameObject CreateMenuItemGo()
        {
            return Instantiate(_menuItemGo) as GameObject;
        }

        private GameObject CreateDetailItemGo()
        {
            return Instantiate(_detailItemGo) as GameObject;
        }

        private void CreateDetailList<T>(int menuIndex, IList dataList, int coroutineCount = 0, bool layoutImmediately = false) where T : KTreeDetailItem
        {
            if (layoutImmediately)
            {
                RemoveDetailItemIndex(menuIndex);
            }
            if (coroutineCount == 0)
            {
                for (int i = 0; i < dataList.Count; i++)
                {
                    AddDetailItem<T>(dataList[i], menuIndex);
                }
                if (layoutImmediately)
                {
                    UpdateItemListLayout();
                }
            }
            else
            {
                StartCoroutine(AddDetailItemCoroutine<T>(menuIndex, dataList, coroutineCount, layoutImmediately));
            }
        }

        private IEnumerator AddDetailItemCoroutine<T>(int menuIndex, IList dataList, int coroutineCount = 0, bool layoutImmediately = false) where T : KTreeDetailItem
        {
            int startIndex = 0;
            int endIndex = 0;
            while (endIndex < dataList.Count)
            {
                endIndex = Mathf.Min(dataList.Count, startIndex + coroutineCount);
                for (int i = startIndex; i < endIndex; i++)
                {
                    AddDetailItem<T>(dataList[i], menuIndex);
                }
                startIndex = endIndex;
                yield return null;
            }
            if (layoutImmediately == true)
            {
                UpdateItemListLayout();
            }
        }

        private void AddDetailItem<T>(System.Object data, int menuIndex) where T : KTreeDetailItem
        {
            if (_detailDict.ContainsKey(menuIndex) == false)
            {
                _detailDict.Add(menuIndex, new List<KTreeDetailItem>());
            }
            AddDetailItemAt<T>(data, menuIndex, _detailDict[menuIndex].Count);
        }


        private void AddDetailItemAt<T>(System.Object data, int menuIndex, int index) where T : KTreeDetailItem
        {
            GameObject itemGo = CreateDetailItemGo();
            T item = itemGo.AddComponent<T>();
            AddDetailItemAt(item, data, menuIndex, index);
        }

        private void AddDetailItemAt(KTreeDetailItem item, System.Object data, int menuIndex, int index)
        {
            AddItemGoToHierarchy(item.gameObject);
            item.Index = index;
            item.MenuIndex = menuIndex;
            item.Data = data;
            item.name = string.Format(DETAIL_PREFIX, menuIndex, index);
            _detailDict[menuIndex].Insert(index, item);
            AddDetailItemEventListener(item);
        }

        private RectTransform listRect;
        private float listBottom = 0;
        private float offsetY = 0;
        public void UpdateItemListLayout()
        {
            listBottom = 0;
            offsetY = _padding.top;
            UpdateMenuItemLayout();
            listRect.sizeDelta = new Vector2(listRect.sizeDelta.x, listBottom);
        }

        private void UpdateMenuItemLayout()
        {
            for (int i = 0; i < _itemList.Count; i++)
            {
                KTreeMenuItem item = _itemList[i];
                if (item.gameObject.activeSelf)
                {
                    RectTransform itemRect = item.GetComponent<RectTransform>();
                    offsetY = offsetY - item.offset;
                    float itemBottom = offsetY + itemRect.sizeDelta.y + _padding.bottom;
                    listBottom = Math.Max(listBottom, itemBottom);
                    itemRect.anchoredPosition3D = new Vector3(_menuRect.anchoredPosition.x, -offsetY, 0);

                    if (_detailDict.ContainsKey(i) && _hiddenSet.Contains(i) == false)
                    {
                        offsetY += itemRect.sizeDelta.y;
                        UpdateDetailItemLayout(_detailDict[i]);
                    }
                    else
                    {
                        offsetY += CalculateMenuItemPosition(itemRect);
                    }
                }
            }
        }

        private void UpdateDetailItemLayout(List<KTreeDetailItem> detailList)
        {
            for (int j = 0; j < detailList.Count; j++)
            {
                KTreeDetailItem detailItem = detailList[j];
                if (detailItem.gameObject.activeSelf)
                {
                    RectTransform detailItemRect = detailItem.GetComponent<RectTransform>();
                    offsetY = offsetY - detailItem.offset;
                    float detailItemBottom = offsetY + detailItemRect.sizeDelta.y + _padding.bottom;
                    listBottom = Math.Max(listBottom, detailItemBottom);
                    detailItemRect.anchoredPosition3D = new Vector3(_detailRect.anchoredPosition.x, -offsetY, 0);
                    offsetY += CalculateDetailItemHeight(detailItemRect);
                }
            }
            offsetY += _menuTopToDownGap;
        }

        private float CalculateMenuItemPosition(RectTransform itemRect)
        {
            return (itemRect.sizeDelta.y + _menuTopToDownGap);
        }

        private float CalculateDetailItemHeight(RectTransform itemRect)
        {
            return (itemRect.sizeDelta.y + _itemTopToDownGap);
        }

    }

    public struct KTreePadding
    {
        public int top;
        public int right;
        public int bottom;
        public int left;
    }


    public abstract class KTreeMenuItem : KContainer, IPointerClickHandler
    {
        public virtual bool IsSelected { get; set; }
        public virtual int Index { get; set; }
        public virtual System.Object Data { get; set; }
        public KComponentEvent<KTreeMenuItem, int> onClick = new KComponentEvent<KTreeMenuItem, int>();

        public float offset = 0;

        public int ID = -1;

        /// <summary>
        /// 在Awake函数中对Item的子元素建立索引
        /// </summary>
        protected abstract override void Awake();

        public override void OnPointerClick(PointerEventData evtData)
        {
            if (evtData.dragging == true)
            {
                return;
            }
            onClick.Invoke(this, this.Index);
        }

        public virtual void Show() { }
        public virtual void Hide() { }
        public abstract void Dispose();

        protected sealed override void OnEnable()
        {
            Show();
        }

        protected sealed override void OnDisable()
        {
            Hide();
        }

        protected sealed override void OnDestroy()
        {
            Dispose();
        }
    }


    public abstract class KTreeDetailItem : KContainer, IPointerClickHandler
    {
        public virtual bool IsSelected { get; set; }
        public virtual int Index { get; set; }

        public virtual int MenuIndex { get; set; }

        public virtual System.Object Data { get; set; }
        public KComponentEvent<KTreeDetailItem, int, int> onClick = new KComponentEvent<KTreeDetailItem, int, int>();

        public int ID = -1;

        public float offset = 0;

        /// <summary>
        /// 在Awake函数中对Item的子元素建立索引
        /// </summary>
        protected abstract override void Awake();

        public override void OnPointerClick(PointerEventData evtData)
        {
            if (evtData.dragging == true)
            {
                return;
            }
            onClick.Invoke(this, this.MenuIndex, this.Index);
        }

        public virtual void Show() { }
        public virtual void Hide() { }
        public abstract void Dispose();

        protected sealed override void OnEnable()
        {
            Show();
        }

        protected sealed override void OnDisable()
        {
            Hide();
        }

        protected sealed override void OnDestroy()
        {
            Dispose();
        }
    }

}
