﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Game.UI.UIComponent
{
    public class KButtonStateFacade : MonoBehaviour, IStateChangeable
    {
        private static ChildrenComponentsPool<List<StateChangeable>> _stateChangeableListPool = new ChildrenComponentsPool<List<StateChangeable>>(null, l => l.Clear());
        private string _currentState = StateChangeable.STATE_NORMAL;

        public string State
        {
            get
            {
                return _currentState;
            }
            set
            {
                if(_currentState != value)
                {
                    _currentState = value;
                    UpdateChildrenState();
                }
            }
        }

        private void UpdateChildrenState()
        {
            List<StateChangeable> list = _stateChangeableListPool.Get();
            GetComponentsInChildren(false, list);
            for(int i = 0; i < list.Count; i++)
            {
                list[i].State = _currentState;
            }
            _stateChangeableListPool.Release(list);
        }

        public List<IStateChangeable> GetStateChangeableChildren()
        {
            return KComponentUtil.GetStateChangeableChildren(this.gameObject);
        }

    }
}
