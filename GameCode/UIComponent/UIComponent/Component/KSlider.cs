using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Game.UI.UIComponent
{
    /// <summary>
    /// 必须子元素:fill进度条，可选子元素：handle
    /// </summary>
    public class KSlider : Slider, IKContainer, IPointerUpHandler, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
    {
        private string _currentState = KButtonState.NORMAL;

        private GameObject _fillGo;
        private StateImage _fillImage;

        private List<IStateChangeable> _stateChangeableChildren;

        protected override void Awake()
        {
            CreateChildren();
            Initialize();
            AddEventListener();
        }

        private void CreateChildren()
        {
            _fillGo = GetChild("fillArea/fillHolder/fill");
            _fillImage = _fillGo.GetComponent<StateImage>();
            _fillImage.AddAllStateComponent<Resizer>();
            _stateChangeableChildren = GetStateChangeableChildren();
        }

        private void Initialize()
        {
            SetState(KButtonState.NORMAL);
        }

        private void AddEventListener()
        {
            this.onValueChanged.AddListener(OnSliderValueChange);
        }

        private void OnSliderValueChange(float value)
        {
            _fillImage.ScaleX = value;
        }

        override public void OnPointerDown(PointerEventData evtData)
        {
            _fillImage.SetDimensionsDirty();
            SetState(KButtonState.DOWN);
            base.OnPointerDown(evtData);
        }

        override public void OnPointerUp(PointerEventData evtData)
        {
            SetState(KButtonState.OVER);
            base.OnPointerUp(evtData);
        }

        override public void OnPointerEnter(PointerEventData evtData)
        {
            SetState(KButtonState.OVER);
            base.OnPointerEnter(evtData);
        }

        override public void OnPointerExit(PointerEventData evtData)
        {
            SetState(KButtonState.NORMAL);
            base.OnPointerExit(evtData);
        }

        private void SetState(string state)
        {
            if(state == _currentState)
            {
                return;
            }
            _currentState = state;
            foreach(IStateChangeable child in _stateChangeableChildren)
            {
                child.State = _currentState;
            }
        }

        public GameObject GetChild(string childPath)
        {
            return KComponentUtil.GetChild(this.gameObject, childPath);
        }

        public T GetChildComponent<T>(string childPath) where T : UnityEngine.Component
        {
            return GetChild(childPath).GetComponent<T>();
        }

        public T AddChildComponent<T>(string childPath) where T : Component
        {
            return GetChild(childPath).AddComponent<T>();
        }

        public List<IStateChangeable> GetStateChangeableChildren()
        {
            return KComponentUtil.GetStateChangeableChildren(this.gameObject);
        }

        public bool Visible
        {
            get
            {
                return gameObject.activeSelf;
            }
            set
            {
                gameObject.SetActive(value);
            }
        }
    }
}
