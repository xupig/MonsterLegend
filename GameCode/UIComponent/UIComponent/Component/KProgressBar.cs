﻿using UnityEngine;

namespace Game.UI.UIComponent
{
    /// <summary>
    /// 横向进度条
    /// ProgressBar必须还有GameObject:bar<StateImage>
    /// </summary>
    public class KProgressBar : KContainer
    {
        private static string BAR = "bar";
        private float _value = 1.0f;

        private StateImage _bar;

        protected override void Awake()
        {
            base.Awake();
            CreateChildren();
            this.Value = 1.0f;
        }

        private void CreateChildren()
        {
            _bar = GetChildComponent<StateImage>(BAR);
            _bar.AddAllStateComponent<Resizer>();
        }

        public int Percent
        {
            get
            {
                return (int)(_value * 100);
            }
            set
            {
                Value = (float)value / 100;
            }
        }

        public float Value
        {
            get
            {
                return _value;
            }
            set
            {
                if(value != _value)
                {
                    _value = Mathf.Clamp(value, 0.0f, 1.0f);
                    _bar.ScaleX = _value;
                }
            }
        }
    }

}
