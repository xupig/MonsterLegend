﻿using UnityEngine;
using System.Collections.Generic;

namespace Game.UI.UIComponent
{
    /// <summary>
    /// 
    /// </summary>
    public interface IKContainer
    {
        GameObject GetChild(string childPath);
        T GetChildComponent<T>(string childPath) where T : Component;
        T AddChildComponent<T>(string childPath) where T : Component;
        List<IStateChangeable> GetStateChangeableChildren();
        bool Visible { get; set; }
    }

}
