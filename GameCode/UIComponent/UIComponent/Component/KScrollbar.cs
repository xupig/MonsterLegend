﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Game.UI.UIComponent
{
    /// <summary>
    /// 必须含有fill 和 handle 子元素
    /// </summary>
    public abstract class KScrollbar : Scrollbar, IKContainer, IPointerUpHandler, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
    {
        private string _currentState;

        private GameObject _handleGo;
        private GameObject _handleAreaGo;
        private List<IStateChangeable> _stateChangeableChildren;

        private float _recordSize;

        protected override void Awake()
        {
            base.Awake();
            CreateChildren();
            Initialize();
            SetDirection();
        }

        private void Initialize()
        {
            this.transition = Transition.None;
            SetState(KButtonState.NORMAL);
            this.value = 0;
            this.size = 0;
            UpdateHandleSize();
            _recordSize = this.size;
        }

        protected virtual void SetDirection()
        {
            this.direction = Direction.LeftToRight;
        }

        private void CreateChildren()
        {
            _handleGo = GetChild("handleArea/handle");
            _handleAreaGo = GetChild("handleArea");
            _stateChangeableChildren = GetStateChangeableChildren();
        }

        private void AddEventListener()
        {
            this.onValueChanged.AddListener(OnScrollValueChanged);
        }

        private void OnScrollValueChanged(float value)
        {
            UpdateHandleSize();
        }

        override public void OnPointerDown(PointerEventData evtData)
        {
            SetState(KButtonState.DOWN);
            base.OnPointerDown(evtData);
        }

        override public void OnPointerUp(PointerEventData evtData)
        {
            SetState(KButtonState.OVER);
            base.OnPointerUp(evtData);
        }

        override public void OnPointerEnter(PointerEventData evtData)
        {
            SetState(KButtonState.OVER);
            base.OnPointerEnter(evtData);
        }

        override public void OnPointerExit(PointerEventData evtData)
        {
            SetState(KButtonState.NORMAL);
            base.OnPointerExit(evtData);
        }

        private void SetState(string state)
        {
            if(state == _currentState)
            {
                return;
            }
            _currentState = state;
            foreach(IStateChangeable child in _stateChangeableChildren)
            {
                child.State = state;
            }
        }

        protected virtual void UpdateHandleSize()
        {
            StateImage handleImage = _handleGo.GetComponent<StateImage>();
            if(handleImage != null)
            {
                RectTransform handleAreaRect = _handleAreaGo.GetComponent<RectTransform>();
                RectTransform handleRect = _handleGo.GetComponent<RectTransform>();
                if(this.direction == Direction.LeftToRight || this.direction == Direction.RightToLeft)
                {
                    float width = handleRect.sizeDelta.x;
                    float targetWidth = handleAreaRect.sizeDelta.x * this.size + width;
                    handleImage.ScaleX = (float)(targetWidth / width);
                }
                else
                {
                    float height = handleRect.sizeDelta.y;
                    float targetHeight = handleAreaRect.sizeDelta.y * this.size + height;
                    handleImage.ScaleY = (float)(targetHeight / height);
                }
            }
        }

        protected void Update()
        {
            if(_recordSize != this.size)
            {
                _recordSize = this.size;
                UpdateHandleSize();
            }
        }

        public GameObject GetChild(string childPath)
        {
            return KComponentUtil.GetChild(this.gameObject, childPath);
        }

        public T GetChildComponent<T>(string childPath) where T : UnityEngine.Component
        {
            return GetChild(childPath).GetComponent<T>();
        }

        public T AddChildComponent<T>(string childPath) where T : Component
        {
            return GetChild(childPath).AddComponent<T>();
        }

        public List<IStateChangeable> GetStateChangeableChildren()
        {
            return KComponentUtil.GetStateChangeableChildren(this.gameObject);
        }

        public bool Visible
        {
            get
            {
                return gameObject.activeSelf;
            }
            set
            {
                gameObject.SetActive(value);
            }
        }
    }
}

