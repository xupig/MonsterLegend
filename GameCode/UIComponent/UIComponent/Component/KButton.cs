using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Game.UI.UIComponent
{
    public class KButton : Button, IKContainer, IPointerUpHandler, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
    {

        private KButtonStateFacade _stateBunch;

        private KComponentEvent<KButton, PointerEventData> _onPointerUp;
        private KComponentEvent<KButton, PointerEventData> _onPointerDown;
        private KComponentEvent<KButton, PointerEventData> _onPointerEnter;
        private KComponentEvent<KButton, PointerEventData> _onPointerExit;

        private bool _enableAudio = true;
        private bool _isPointerInside = false;
        private bool _isPointerDown = false;

        protected override void Awake()
        {
            CreateChildren();
            AddClickEventListener();
        }

        protected virtual void Initialize()
        {
            SetState(KButtonState.NORMAL);
            _isPointerDown = false;
            _isPointerInside = false;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            Initialize();
        }

        private void CreateChildren()
        {
            _stateBunch = GetComponent<KButtonStateFacade>();
        }

        private void AddClickEventListener()
        {
            onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            if(_enableAudio == true)
            {
                KComponentAudioCollective.PlayClickAudio(this.gameObject);
            }
        }

        override public void OnPointerDown(PointerEventData evtData)
        {
            base.OnPointerDown(evtData);
            _isPointerDown = true;
            SetState(KButtonState.DOWN);
            if(_onPointerDown != null)
            {
                _onPointerDown.Invoke(this, evtData);
            }
        }

        override public void OnPointerUp(PointerEventData evtData)
        {
            base.OnPointerUp(evtData);
            _isPointerDown = false;
            if(_isPointerInside == true)
            {
                SetState(KButtonState.OVER);
            }
            else
            {
                SetState(KButtonState.NORMAL);
            }
            if(_onPointerUp != null)
            {
                _onPointerUp.Invoke(this, evtData);
            }
        }

        override public void OnPointerEnter(PointerEventData evtData)
        {
            base.OnPointerEnter(evtData);
            _isPointerInside = true;
            SetState(KButtonState.OVER);
            if(_onPointerEnter != null)
            {
                _onPointerEnter.Invoke(this, evtData);
            }
        }

        override public void OnPointerExit(PointerEventData evtData)
        {
            base.OnPointerExit(evtData);
            _isPointerInside = false;
            if(_isPointerDown == true)
            {
                SetState(KButtonState.OVER);
            }
            else
            {
                SetState(KButtonState.NORMAL);
            }
            if(_onPointerExit != null)
            {
                _onPointerExit.Invoke(this, evtData);
            }
        }

        protected virtual void SetState(string state)
        {
            if(interactable == false)
            {
                return;
            }
            _stateBunch.State = state;
        }

        public void SetButtonActive()
        {
            ImageWrapper[] images = GetComponentsInChildren<ImageWrapper>(true);
            for (int i = 0; i < images.Length; i++)
            {
                images[i].SetGray(1);
            }
            interactable = true;
        }

        public void SetButtonDisable()
        {
            ImageWrapper[] images = GetComponentsInChildren<ImageWrapper>(true);
            for (int i = 0; i < images.Length; i++)
            {
                images[i].SetGray(0);
            }
            interactable = false;
        }

        public new bool interactable
        {
            get
            {
                return base.interactable;
            }
            set
            {
                if(value == true)
                {
                    _stateBunch.State = KButtonState.NORMAL;
                }
                else
                {
                    _stateBunch.State = KButtonState.DISABLE;
                }
                base.interactable = value;
            }
        }

        public bool EnableAudio
        {
            get
            {
                return _enableAudio;
            }
            set
            {
                _enableAudio = value;
            }
        }

        public StateText Label
        {
            get
            {
                for(int i = 0; i < transform.childCount; i++)
                {
                    Transform childTrans = transform.GetChild(i);
                    StateText label = childTrans.GetComponent<StateText>();
                    if(label != null)
                    {
                        return label;
                    }
                }
                return null;
            }
        }

        public GameObject GetChild(string childPath)
        {
            return KComponentUtil.GetChild(this.gameObject, childPath);
        }

        public T GetChildComponent<T>(string childPath) where T : UnityEngine.Component
        {
            return GetChild(childPath).GetComponent<T>();
        }

        public T AddChildComponent<T>(string childPath) where T : Component
        {
            return GetChild(childPath).AddComponent<T>();
        }

        public List<IStateChangeable> GetStateChangeableChildren()
        {
            return KComponentUtil.GetStateChangeableChildren(this.gameObject);
        }

        public bool Visible
        {
            get
            {
                return gameObject.activeSelf;
            }
            set
            {
                gameObject.SetActive(value);
            }
        }

        public KComponentEvent<KButton, PointerEventData> onPointerUp
        {
            get
            {
                if(_onPointerUp == null)
                {
                    _onPointerUp = new KComponentEvent<KButton, PointerEventData>();
                }
                return _onPointerUp;
            }
        }

        public KComponentEvent<KButton, PointerEventData> onPointerDown
        {
            get
            {
                if(_onPointerDown == null)
                {
                    _onPointerDown = new KComponentEvent<KButton, PointerEventData>();
                }
                return _onPointerDown;
            }
        }

        public KComponentEvent<KButton, PointerEventData> onPointerEnter
        {
            get
            {
                if(_onPointerEnter == null)
                {
                    _onPointerEnter = new KComponentEvent<KButton, PointerEventData>();
                }
                return _onPointerEnter;
            }
        }

        public KComponentEvent<KButton, PointerEventData> onPointerExit
        {
            get
            {
                if(_onPointerExit == null)
                {
                    _onPointerExit = new KComponentEvent<KButton, PointerEventData>();
                }
                return _onPointerExit;
            }
        }
    }

    public class KButtonState
    {
        public const string NORMAL = "normal";
        public const string OVER = "over";
        public const string DOWN = "down";
        public const string DISABLE = "disable";
    }

    /// <summary>
    /// 按钮透明热区，阻挡按钮中Selectable子组件对鼠标的响应
    /// </summary>
    public class KButtonHitArea : Graphic
    {
        public override void SetAllDirty()
        {
            //blank
        }

        public override void SetLayoutDirty()
        {
            //blank
        }

        public override void SetVerticesDirty()
        {
            //blank
        }

        public override void SetMaterialDirty()
        {
            //blank
        }

        protected override void OnFillVBO(List<UIVertex> vbo)
        {
            //blank;
        }

    }

    /// <summary>
    /// 热区为透明的按钮
    /// </summary>
    public class KDummyButton : Button
    {
        protected override void Awake()
        {
            base.Awake();
            if(gameObject.GetComponent<Graphic>() == null)
            {
                gameObject.AddComponent<KButtonHitArea>();
            }
        }
    }
}
