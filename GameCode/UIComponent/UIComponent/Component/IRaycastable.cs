﻿using UnityEngine;

namespace Game.UI.UIComponent
{
    public interface IRaycastable : ICanvasRaycastFilter
    {
        /// <summary>
        /// 可以被射线穿透
        /// </summary>
        bool Raycast{ get; set; }
    }
}
