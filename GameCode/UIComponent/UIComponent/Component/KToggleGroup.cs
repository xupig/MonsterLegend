using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

namespace Game.UI.UIComponent
{
    public class KToggleGroup : ToggleGroup, IKContainer
    {
        private List<KToggle> _toggleList;
        private int _selectedIndex = -1;

        private KComponentEvent<KToggleGroup, int> _onSelectedIndexChanged;
        public KComponentEvent<KToggleGroup, int> onSelectedIndexChanged
        {
            get
            {
                if(_onSelectedIndexChanged == null)
                {
                    _onSelectedIndexChanged = new KComponentEvent<KToggleGroup, int>();
                }
                return _onSelectedIndexChanged;
            }
        }

        override protected void Awake()
        {
            base.Awake();
            InitToggleList();
        }

        private void InitToggleList()
        {
            _selectedIndex = 0;
            _toggleList = new List<KToggle>();
            int toggleIndex = 0;
            for(int i = 0; i < this.transform.childCount; i++)
            {       
                KToggle toggle = transform.GetChild(i).GetComponent<KToggle>();
                if(toggle != null)
                {
                    toggle.group = this;
                    toggle.Index = toggleIndex;
                    if(toggleIndex == _selectedIndex)
                    {
                        toggle.isOn = true;
                        toggle.ToggleCheckmark();
                    }
                    toggle.onValueChanged.AddListener(OnToggleValueChanged);
                    _toggleList.Add(toggle);
                    base.RegisterToggle(toggle);
                    toggleIndex++;
                }
            }
        }

        private void OnToggleValueChanged(KToggle target, bool isOn)
        {
            if(isOn == true)
            {
                int oldSelectIndex = _selectedIndex;
                UpdateSelectIndex();
                if(oldSelectIndex != _selectedIndex)
                {
                    TriggerChangeEvent();
                }
            }
        }

        private void TriggerChangeEvent()
        {
            if(_onSelectedIndexChanged != null)
            {
                onSelectedIndexChanged.Invoke(this, _selectedIndex);
            }
        }

        public List<KToggle> GetToggleList()
        {
            return _toggleList;
        }

        public KToggle this[int index]
        {
            get
            {
                return _toggleList[index];
            }
        }

        public int SelectIndex
        {
            get
            {
                return _selectedIndex;
            }
            set
            {
                if(value >= 0 && value <= _toggleList.Count - 1)
                {
                    if (_selectedIndex >= 0 && _selectedIndex <= _toggleList.Count - 1)
                    {
                        _toggleList[_selectedIndex].isOn = false;
                    }
                    _selectedIndex = value;
                    _toggleList[_selectedIndex].isOn = true;
                    _toggleList[_selectedIndex].ToggleCheckmark();
                }
            }
        }

        /// <summary>
        /// 这个接口应该用到
        /// <param name="toggle">Toggle.</param>
        public void RegisterToggle(KToggle toggle)
        {
            toggle.group = this;
            base.RegisterToggle(toggle);
            UpdateToggleList();
            UpdateSelectIndex();
        }

        /// <summary>
        /// 这个接口应该用到
        /// <param name="toggle">Toggle.</param>
        public void UnregisterToggle(KToggle toggle)
        {
            toggle.group = null;
            base.UnregisterToggle(toggle);
            UpdateToggleList();
            UpdateSelectIndex();
        }

        private void UpdateToggleList()
        {
            _toggleList = new List<KToggle>();
            KToggle[] toggleArray = GetComponentsInChildren<KToggle>();
            foreach(KToggle toggle in toggleArray)
            {
                toggle.onValueChanged.RemoveListener(OnToggleValueChanged);
                if(toggle.group != null)
                {
                    _toggleList.Add(toggle);
                    toggle.onValueChanged.AddListener(OnToggleValueChanged);
                }
            }
        }

        private void UpdateSelectIndex()
        {
            _selectedIndex = -1;
            for(int i = 0; i < _toggleList.Count; i++)
            {
                KToggle toggle = _toggleList[i];
                if(toggle.isOn == true)
                {
                    _selectedIndex = i;
                    break;
                }
            }
        }

        public GameObject GetChild(string childPath)
        {
            return KComponentUtil.GetChild(this.gameObject, childPath);
        }

        public T GetChildComponent<T>(string childPath) where T : UnityEngine.Component
        {
            return GetChild(childPath).GetComponent<T>();
        }

        public T AddChildComponent<T>(string childPath) where T : Component
        {
            return GetChild(childPath).AddComponent<T>();
        }

        public List<IStateChangeable> GetStateChangeableChildren()
        {
            return KComponentUtil.GetStateChangeableChildren(this.gameObject);
        }

        public bool Visible
        {
            get
            {
                return gameObject.activeSelf;
            }
            set
            {
                gameObject.SetActive(value);
            }
        }
    }
}
