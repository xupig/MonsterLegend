﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Game.UI.Builder;

namespace Game.UI.UIComponent
{
    public class KDeferredContainer : KContainer
    {
        public bool childInteractable;

        protected override void Awake()
        {
            base.Awake();
            BuildChildren();
        }

        private void BuildChildren()
        {
            ComponentBuilder builder = ComponentBuilder.GetBuilder(GetComponent<BuildHelper>());
            ContainerBuilder containerBuilder = builder as ContainerBuilder;
            if(containerBuilder != null)
            {
                containerBuilder.BuildChildren(this.gameObject, childInteractable);
            }
        }
    }
}
