﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Game.UI.UIComponent
{
    public class KPageableList : KList
    {
        /// <summary>
        /// 实际只最多创建三页item
        /// </summary>
        private const int REAL_ITEM_PAGE_COUNT = 3;
        /// <summary>
        /// 设定水平或垂直方向Item数目的最大值，简单取平方根
        /// </summary>
        public static int MAX_DIRECTION_COUNT = (int)Mathf.Sqrt((float)int.MaxValue);

        private int _totalPage = 1;
        private int _currentPage = 0;
        /// <summary>
        /// 三页中的最左边页的Index值，当在第一页时_currentPage==0此时_startPage==_currentPage，其余时间这两个值不相等
        /// </summary>
        private int _startPage;
        /// <summary>
        /// 三页中的最右边页的Index值
        /// </summary>
        private int _endPage;
        private int _leftToRightCount = MAX_DIRECTION_COUNT;
        private int _topToDownCount = MAX_DIRECTION_COUNT;
        private int _itemCountPrePage;

        private IList _dataList;
        private Type _itemType;

        
        // 标记KPageableList是否是第一次设置数据，仅在第一次设置数据时，能使用协程创建Item以优化面板首次开启消耗时间
        private bool _isFirstSetData = true;


        public override void SetDirection(KList.KListDirection direction, int leftToRightCount, int topToDownCount)
        {
            base.SetDirection(direction, leftToRightCount, topToDownCount);
            _leftToRightCount = leftToRightCount >= MAX_DIRECTION_COUNT ? MAX_DIRECTION_COUNT : leftToRightCount;
            _topToDownCount = topToDownCount >= MAX_DIRECTION_COUNT ? MAX_DIRECTION_COUNT : topToDownCount;
        }

        public void SetDataList(int currentPage, Type itemType, IList dataList, int coroutineCreateCount = 0)
        {
            if (IsExecutingCoroutine == true)
            {
                StopAllCoroutines();
            }
            _dataList = dataList;
            _itemCountPrePage = _leftToRightCount * _topToDownCount;
            _totalPage = Mathf.CeilToInt((float)_dataList.Count / (float)_itemCountPrePage);
            _itemType = itemType;
            _currentPage = currentPage;
            _startPage = Mathf.Min(Mathf.Max(0, _currentPage - 1), Math.Max(0, _totalPage - REAL_ITEM_PAGE_COUNT));
            _endPage = Mathf.Min(_totalPage, _startPage + REAL_ITEM_PAGE_COUNT);
            if(_isFirstSetData == true && coroutineCreateCount > 0)
            {
                CreateItemPageByCoroutine(coroutineCreateCount);
            }
            else
            {
                RefreshAllItemPage();
                InvokeAllItemCreated();
            }
            _isFirstSetData = false;
        }

        public override void SetDataList(Type itemType, IList dataList, int coroutineCreateCount = 0)
        {
            SetDataList(CurrentPage, itemType, dataList, coroutineCreateCount);
        }



        public int CurrentPage
        {
            get
            {
                return _currentPage;
            }
            set
            {
                if(value == _currentPage)
                {
                    return;
                }
                if (IsExecutingCoroutine == true)
                {
                    StopAllCoroutines();
                }
                int oldStartPage = _startPage;
                _currentPage = value;
                _startPage = Mathf.Min(Mathf.Max(0, _currentPage - 1), Math.Max(0, _totalPage - REAL_ITEM_PAGE_COUNT));
                _endPage = Mathf.Min(_totalPage, _startPage + REAL_ITEM_PAGE_COUNT);
                RefreshItemPage(_startPage - oldStartPage);
            }
        }

        private void CreateItemPageByCoroutine(int coroutineCreateCount)
        {
            int dataCount = Mathf.Min(REAL_ITEM_PAGE_COUNT * _itemCountPrePage, _dataList.Count - _startPage * _itemCountPrePage);
            if(dataCount >= _dataList.Count)
            {
                StartCoroutine(CreateItem(_itemType, _dataList, coroutineCreateCount));
            }
            else
            {
                System.Object[] subDataList = new System.Object[dataCount];
                int startIndex = _startPage * _itemCountPrePage;
                for(int i = startIndex; i < startIndex + dataCount; i++)
                {
                    subDataList[i - startIndex] = _dataList[i];
                }
                StartCoroutine(CreateItem(_itemType, subDataList, coroutineCreateCount));
            }
        }

        /// <summary>
        /// 页码不变时不刷新
        /// 左移、右移一页时只刷新一页数据
        /// </summary>
        /// <param name="direction"></param>
        protected virtual void RefreshItemPage(int direction)
        {
            if(direction == 0)
            {
                //Do nothing
            }
            else if(direction == 1)
            {
                MoveOnePageLeft();
            }
            else if(direction == -1)
            {
                MoveOnePageRight();
            }
            else
            {
                RefreshAllItemPage();
            }
        }

        private void RefreshAllItemPage()
        {
            int itemCount = Mathf.Max(_itemList.Count, Mathf.Min((_endPage - _startPage) * _itemCountPrePage, _dataList.Count - _startPage * _itemCountPrePage));
            for(int i = 0; i < itemCount; i++)
            {
                int dataIndex = i + _startPage * _itemCountPrePage;
                if(_itemList.Count <= i)
                {
                    AddItem(_itemType, _dataList[dataIndex], false);
                }
                else
                {
                    RefreshItem(_itemList[i], dataIndex);
                }
            }
            UpdateItemListLayout();
        }

        private void MoveOnePageLeft()
        {
            for(int i = 0; i < _itemCountPrePage; i++)
            {
                KListItemBase item = _itemList[0];
                _itemList.RemoveAt(0);
                _itemList.Add(item);
                int dataIndex = i + (_startPage + 2) * _itemCountPrePage;
                RefreshItem(item, dataIndex);
            }
            UpdateItemListLayout();
        }

        private void MoveOnePageRight()
        {
            for(int i = 0; i < _itemCountPrePage; i++)
            {
                KListItemBase item = _itemList[_itemList.Count - 1];
                _itemList.RemoveAt(_itemList.Count - 1);
                _itemList.Insert(i, item);
                int dataIndex = i + _startPage * _itemCountPrePage;
                RefreshItem(item, dataIndex);
            }
            UpdateItemListLayout();
        }

        private void RefreshItem(KListItemBase item, int dataIndex)
        {
            if(_dataList.Count > dataIndex)
            {
                item.Visible = true;
                item.Index = dataIndex;
                item.Data = _dataList[dataIndex];
            }
            else
            {
                item.Visible = false;
            }
        }

        protected override int OffsetItemCount
        {
            get
            {
                return _startPage * _itemCountPrePage;
            }
        }

        private bool IsCoroutineFinished
        {
            get
            {
                return _itemList.Count >= Mathf.Min(_dataList.Count, _itemCountPrePage * REAL_ITEM_PAGE_COUNT);
            }
        }
    }
}
