﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Game.UI;

namespace Game.UI.UIComponent
{
    [DisallowMultipleComponent]
    public class StateImage : StateChangeable<ImageWrapper>
    {
        private float _alpha = 1.0f;
        private Resizer[] _resizers;

        private Resizer[] GetResizers()
        {
            if(_resizers == null)
            {
                _resizers = GetAllStateComponent<Resizer>();
            }
            return _resizers;
        }

        public override float Alpha
        {
            get
            {
                return _alpha;
            }
            set
            {
                _alpha = Mathf.Clamp(value, 0.0f, 1.0f);
                for(int i = 0; i < this.StateCount; i++)
                {
                    ImageWrapper component = GetStateGameObject(i).GetComponent<ImageWrapper>();
                    component.alpha = _alpha;
                }
            }
        }

        public float ScaleX
        {
            get
            {
                Resizer resizer = GetComponent<Resizer>();
                if(resizer == null)
                {
                    return 1.0f;
                }
                return resizer.ScaleX;
            }
            set
            {
                SetDimensionsDirty();
                Resizer[] resizers = GetResizers();
                for(int i = 0; i < resizers.Length; i++)
                {
                    if(resizers[i] != null)
                    {
                        resizers[i].ScaleX = value;
                    }
                }
            }
        }

        public float ScaleY
        {
            get
            {
                Resizer resizer = GetComponent<Resizer>();
                if(resizer == null)
                {
                    return 1.0f;
                }
                return resizer.ScaleY;
            }
            set
            {
                SetDimensionsDirty();
                Resizer[] resizers = GetResizers();
                for(int i = 0; i < resizers.Length; i++)
                {
                    if(resizers[i] != null)
                    {
                        resizers[i].ScaleY = value;
                    }
                }
            }
        }

        public float Width
        {
            get
            {
                Resizer resizer = GetComponent<Resizer>();
                if(resizer == null)
                {
                    return GetComponent<RectTransform>().sizeDelta.x;
                }
                return resizer.Width;
            }
            set
            {
                SetDimensionsDirty();
                Resizer[] resizers = GetResizers();
                for(int i = 0; i < resizers.Length; i++)
                {
                    if(resizers[i] != null)
                    {
                        resizers[i].Width = value;
                    }
                }
            }
        }

        public float Height
        {
            get
            {
                Resizer resizer = GetComponent<Resizer>();
                if(resizer == null)
                {
                    return GetComponent<RectTransform>().sizeDelta.y;
                }
                return resizer.Height;
            }
            set
            {
                SetDimensionsDirty();
                Resizer[] resizers = GetResizers();
                for(int i = 0; i < resizers.Length; i++)
                {
                    if(resizers[i] != null)
                    {
                        resizers[i].Height = value;
                    }
                }
            }
        }

		public ImageWrapper CurrentImage
        {
            get
            {
				return GetCurrentStateComponent() as ImageWrapper;
            }
        }

        public void SetDimensionsDirty()
        {
            for(int i = 0; i < this.StateCount; i++)
            {
                GetStateComponent(i).SetDimensionsDirty();
            }
        }

        public void SetContinuousDimensionsDirty(bool value)
        {
            for (int i = 0; i < this.StateCount; i++)
            {
                GetStateComponent(i).SetContinuousDimensionDirty(value);
            }
        }
    }
}
