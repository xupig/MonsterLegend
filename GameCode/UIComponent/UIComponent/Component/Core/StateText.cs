﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Game.UI;

namespace Game.UI.UIComponent
{
    [DisallowMultipleComponent]
    public class StateText : StateChangeable<TextWrapper>
    {
        private float _alpha = 1.0f;

        public Text CurrentText
        {
            get
            {
                return GetCurrentStateComponent();
            }
        }

        public override float Alpha
        {
            get
            {
                return _alpha;
            }
            set
            {
                _alpha = Mathf.Clamp(value, 0.0f, 1.0f);
                for(int i = 0; i < this.StateCount; i++)
                {
                    TextWrapper component = GetStateGameObject(i).GetComponent<TextWrapper>();
                    Color stateColor = component.color;
                    stateColor.a = _alpha;
                    component.color = stateColor;
                }
            }
        }

        public void ChangeAllStateText(string text)
        {
            for(int i = 0; i < this.StateCount; i++)
            {
                Text stateText = GetStateComponent(i);
                if(stateText.text != text)
                {
                    stateText.text = text;
                }
            }
        }

        public void Clear()
        {
            ChangeAllStateText(string.Empty);
        }

        public void RecalculateCurrentStateHeight()
        {
            RecalculateStateHeight(GetStateComponent(this.State));
        }

        public void RecalculateAllStateHeight()
        {
            for(int i = 0; i < this.StateCount; i++)
            {
                RecalculateStateHeight(GetStateComponent(i));
            }
        }

        private void RecalculateStateHeight(TextWrapper stateText)
        {
            GameObject go = stateText.gameObject;
            RectTransform rect = go.GetComponent<RectTransform>();
            rect.sizeDelta = new Vector2(rect.sizeDelta.x, stateText.preferredHeight);
            RectTransform parentRect = gameObject.GetComponent<RectTransform>();
            parentRect.sizeDelta = new Vector2(rect.sizeDelta.x, stateText.preferredHeight);
            stateText.SetDimensionsDirty();
        }

        public Text[] GetAllStateTextComponent()
        {
            Text[] result = new Text[this.StateCount];
            for(int i = 0; i < this.StateCount; i++)
            {
                Text stateText = GetStateComponent(i);
                result[i] = stateText;
            }
            return result;
        }

        public void ChangeAllStateTextAlignment(TextAnchor textAnchor)
        {
            Text[] texts = GetAllStateTextComponent();
            for(int i = 0; i < texts.Length; i++)
            {
                texts[i].alignment = textAnchor;
            }
        }

        public void SetDimensionsDirty()
        {
            for(int i = 0; i < this.StateCount; i++)
            {
                GetStateComponent(i).SetDimensionsDirty();
            }
        }

        public void SetContinuousDimensionsDirty(bool value)
        {
            for (int i = 0; i < this.StateCount; i++)
            {
                GetStateComponent(i).SetContinuousDimensionDirty(value);
            }
        }
    }

}

