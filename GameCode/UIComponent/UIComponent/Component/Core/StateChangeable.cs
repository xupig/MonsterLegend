﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Game.UI;
/// <summary>
/// TODO:
/// StateChangeable 可以尝试继承自Selectable
/// State component.用来表示可以包含多个状态的图片和文本原子组件，通过一个GameObject树模拟实现
/// -StateCompoent
/// 	|-state
/// 	|-state
/// 	|-state
/// </summary>
namespace Game.UI.UIComponent
{
    public class StateChangeable<T> : StateChangeable where T : MaskableGraphic
    {
        public T GetCurrentStateComponent()
        {
            return GetStateGameObject(this.State).GetComponent<T>();
        }

        public T GetStateComponent(string stateName)
        {
            GameObject go = GetStateGameObject(stateName);
            if(go != null)
            {
                return go.GetComponent<T>();
            }
            return null;
        }

        public T GetStateComponent(int index)
        {
            GameObject go = GetStateGameObject(index);
            if(go != null)
            {
                return go.GetComponent<T>();
            }
            return null;
        }


    }

    [DisallowMultipleComponent]
    public abstract partial class StateChangeable : MonoBehaviour, IStateChangeable, IRaycastable
    {
        public const string STATE_NORMAL = "normal";
        private string _currentState = STATE_NORMAL;
        private List<KeyValuePair<string, GameObject>> _stateList;
        private bool _isInitialized = false;

        protected virtual void Awake()
        {
            Raycast = true;
        }

        private void InitState()
        {
            if(_isInitialized == true)
            {
                return;
            }

            _isInitialized = true;
            _stateList = new List<KeyValuePair<string, GameObject>>();
            if(transform.childCount == 0)
            {
                _stateList.Add(new KeyValuePair<string, GameObject>(STATE_NORMAL, this.gameObject));
            }
            else
            {
                for(int i = 0; i < transform.childCount; i++)
                {
                    GameObject child = transform.GetChild(i).gameObject;
                    _stateList.Add(new KeyValuePair<string, GameObject>(child.name, child));
                }
            }
        }

        public string State
        {
            get
            {
                return _currentState;
            }
            set
            {
                if(_currentState != value && ChangeState(value) == true)
                {
                    _currentState = value;
                }
            }
        }

        public bool ContainsState(string state)
        {
            InitState();
            foreach(KeyValuePair<string, GameObject> kvp in _stateList)
            {
                if(kvp.Key == state)
                {
                    return true;
                }
            }
            return false;
        }

        private bool ChangeState(string state)
        {
            if(ContainsState(state) == true)
            {
                ShowCurrentStateGameObject(state);
                return true;
            }
            return false;
        }

        protected virtual void ShowCurrentStateGameObject(string state)
        {
            for(int i = 0; i < _stateList.Count; i++)
            {
                GameObject child = _stateList[i].Value;
                if(child.name == state)
                {
                    child.SetActive(true);
                }
                else
                {
                    child.SetActive(false);
                }
            }
        }

        public GameObject GetCurrentStateGameObject()
        {
            return GetStateGameObject(this.State);
        }

        public GameObject GetStateGameObject(string name)
        {
            InitState();
            foreach(KeyValuePair<string, GameObject> kvp in _stateList)
            {
                if(kvp.Key == name)
                {
                    return kvp.Value;
                }
            }
            return null;
        }

        public GameObject GetStateGameObject(int index)
        {
            InitState();
            if(index < 0 || index > _stateList.Count)
            {
                return null;
            }
            return _stateList[index].Value;
        }

        public void AddAllStateComponent<T>() where T : MonoBehaviour
        {
            for(int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.AddComponent<T>();
            }
            gameObject.AddComponent<T>();
        }

        public T[] GetAllStateComponent<T>() where T : MonoBehaviour
        {
            T[] result = new T[transform.childCount + 1];
            for(int i = 0; i < transform.childCount; i++)
            {
                GameObject child = transform.GetChild(i).gameObject;
                if(child.activeSelf == true)
                {
                    result[i] = child.GetComponent<T>();
                }
            }
            result[result.Length - 1] = gameObject.GetComponent<T>();
            return result;
        }

        public bool Raycast
        {
            get;
            set;
        }

        public bool IsRaycastLocationValid(Vector2 screenPoint, Camera eventCamera)
        {
            return Raycast;
        }

        public bool Visible
        {
            get
            {
                return gameObject.activeSelf;
            }
            set
            {
                gameObject.SetActive(value);
            }
        }

        public virtual float Alpha { get; set; }

        public int StateCount
        {
            get
            {
                InitState();
                return _stateList.Count;
            }
        }
    }

}