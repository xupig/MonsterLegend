﻿using UnityEngine;
using System.Collections;

namespace Game.UI.UIComponent
{
    public interface IStateChangeable
    {
        string State { get; set; }
    }

}
