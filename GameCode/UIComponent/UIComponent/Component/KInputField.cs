﻿#region 模块信息
/*==========================================
// 文件名：KInputField
// 命名空间: Assets.Plugins.UIComponent.Component
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/2/11 17:37:23
// 描述说明：
// 其他：
//==========================================*/
#endregion

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game.UI.UIComponent
{
    public class KInputField : InputField, IPointerDownHandler
    {
        public KComponentEvent<KInputField, PointerEventData> onPointerDown = new KComponentEvent<KInputField, PointerEventData>();
        public KComponentEvent<KInputField> onSubmit = new KComponentEvent<KInputField>();
        public Camera IMECamera;
        public static readonly int defaultWidth = 1280;
        public static readonly int defaultHeight = 720;
        public bool isEnableHalfScreenInput = false;

        public override void OnSubmit(BaseEventData eventData)
        {
            base.OnSubmit(eventData);
            onSubmit.Invoke(this);
        }

        /* ari protected override void OpenInputKeyboard()
		protected void OpenInputKeyboard()
        {
            switch (Application.platform)
            { 
                case RuntimePlatform.Android:
                    OpenInputKeyboardAndroid();
                    break;
                case RuntimePlatform.IPhonePlayer:
                    OpenInputKeyboardIPhone();
                    break;
                default:
                    base.OpenInputKeyboard();
                    break;
            }
        }*/

        private void OpenInputKeyboardAndroid()
        {
            if (!isEnableHalfScreenInput)
            {
                //Ari base.OpenInputKeyboard();
                return;
            }

            RectTransform textRect = textComponent.gameObject.GetComponent<RectTransform>();

            Vector3[] textWorldCorners = new Vector3[4];
            textRect.GetWorldCorners(textWorldCorners);
            Vector2 pos = Vector2.zero;
            if (IMECamera != null)
            {
                pos = RectTransformUtility.WorldToScreenPoint(IMECamera, textWorldCorners[0]);
            }
            else
            {
                pos = new Vector2(textWorldCorners[0].x, textWorldCorners[0].y);
            }

            int posx = (int)pos.x;
            int size = (int)textComponent.fontSize * Screen.height / defaultHeight;
            int width = Mathf.CeilToInt(textComponent.preferredWidth) * Screen.width / defaultWidth;
            int height = Mathf.CeilToInt(textComponent.preferredHeight) * Screen.height / defaultHeight;
            if (size * 5 > height)
            {
                height = size * 5;
            }
            pos.y = Screen.height - pos.y;
            int posy = (int)pos.y;

            #if UNITY_ANDROID
            try
            {
                AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject mainActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
                string[] initValues = new string[7];
                initValues[0] = text;
                initValues[1] = characterLimit.ToString();
                initValues[2] = posx.ToString();
                initValues[3] = posy.ToString();
                initValues[4] = size.ToString();
                initValues[5] = width.ToString();
                initValues[6] = height.ToString();

                mainActivity.Call("showEditBox", initValues);
            }
            catch (System.Exception ex)
            {
                Debug.LogError("OpenInputKeyboard exception = " + ex.StackTrace);
            }
            #endif
        }

        private void OpenInputKeyboardIPhone()
        {
            //Ari base.OpenInputKeyboard();
        }
    }
}
