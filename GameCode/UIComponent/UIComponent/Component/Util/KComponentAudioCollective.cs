﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.UIComponent
{
    public class KComponentAudioCollective
    {
        public static Action<string> playAudio;
        public static float audioVolume;

        public static void PlayClickAudio(GameObject go)
        {
            string path = KComponentUtil.GetAudioSettingPath(go);
            if(playAudio != null)
            {
                playAudio(path);
            }
        }
    }
}
