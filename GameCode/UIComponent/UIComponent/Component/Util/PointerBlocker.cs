﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Game.UI.UIComponent
{
    public class PointerBlocker : UIBehaviour, IRaycastable
    {
        public bool Raycast
        {
            get;
            set;
        }

        public bool IsRaycastLocationValid(Vector2 screenPoint, Camera eventCamera)
        {
            return Raycast;
        }
    }
}
