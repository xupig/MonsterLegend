﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 用于带StateText的GameObject位置调整
/// </summary>
namespace Game.UI.UIComponent
{
    public class StateTextLocater : Locater
    {
        private StateText _stateText;
        protected override void Awake()
        {
            base.Awake();
            _stateText = GetComponent<StateText>();
        }

        public override float Width
        {
            get
            {
                return _stateText.CurrentText.preferredWidth;
            }
        }

        public override float Height
        {
            get
            {
                return _stateText.CurrentText.preferredHeight;
            }
        }

    }
}
