﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Game.UI.UIComponent
{
    public class KShrinkableButton : KButton
    {
        private const float MAX_SCALE = 1.08f;
        private const float MIN_SCALE = 1.0f;
        /// <summary>
        /// 缩小
        /// </summary>
        private const int ZOOM_OUT = -1;
        /// <summary>
        /// 放大
        /// </summary>
        private const int ZOOM_IN = 1;

        private static bool _ignoreTimeScale = false;
        private static float _duration = 0.1f;

        private IEnumerator _tween;
        private float _startScale;
        private int _direction;
        private float _width;
        private float _height;
        private float _originalX;
        private float _originalY;
        private float _startX;
        private float _startY;

        private RectTransform _rect;

        public bool IsShrinkable
        {
            get;
            set;
        }

        private static IEnumerator StartTween(KShrinkableButton btn)
        {
            float elapsedTime = 0.0f;

            while (elapsedTime < _duration)
            {
                elapsedTime += _ignoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime;
                float precentage = Mathf.Clamp01(elapsedTime / _duration);
                btn.DoTween(precentage);
                yield return null;
            }
        }

        public void RefreshRectTransform()
        {
            if (_rect == null)
            {
                _rect = GetComponent<RectTransform>();
            }
            _width = _rect.sizeDelta.x;
            _height = _rect.sizeDelta.y;
            _originalX = _rect.localPosition.x;
            _originalY = _rect.localPosition.y;
        }

        protected override void Awake()
        {
            base.Awake();
            IsShrinkable = true;
            RefreshRectTransform();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            if (_tween != null)
            {
                StopCoroutine(_tween);
                _tween = null;
            }
            transform.localScale = new Vector3(1, 1, 1.0f);
            transform.localPosition = new Vector3(_originalX, _originalY, 0);
        }

        protected override void Initialize()
        {
            base.Initialize();
            transform.localScale = Vector3.one;
        }

        public override void OnPointerEnter(PointerEventData evtData)
        {
            base.OnPointerEnter(evtData);
            if (IsShrinkable)
            {
                Shrink(ZOOM_IN);
            }
        }

        public override void OnPointerExit(PointerEventData evtData)
        {
            if (IsShrinkable)
            {
                Shrink(ZOOM_OUT);
            }
            base.OnPointerExit(evtData);
        }

        private void Shrink(int direction)
        {
            _direction = direction;
            _startScale = _direction == ZOOM_OUT ? MAX_SCALE : MIN_SCALE;

            _startX = _direction == ZOOM_OUT ? _originalX - _width * (MAX_SCALE - MIN_SCALE) * 0.5f : _originalX;
            _startY = _direction == ZOOM_OUT ? _originalY + _height * (MAX_SCALE - MIN_SCALE) * 0.5f : _originalY;
            if (_tween != null)
            {
                StopCoroutine(_tween);
                _tween = null;
            }
            _tween = StartTween(this);
            StartCoroutine(_tween);
        }

        private void DoTween(float precentage)
        {
            RefreshScale(precentage);
            RefreshPosition(precentage);
        }

        private void RefreshScale(float precentage)
        {
            float scale = _startScale + (MAX_SCALE - MIN_SCALE) * precentage * _direction;
            transform.localScale = new Vector3(scale, scale, 1.0f);
        }

        private void RefreshPosition(float precentage)
        {
            float x = _startX - _width * (MAX_SCALE - MIN_SCALE) * 0.5f * precentage * _direction;
            float y = _startY + _height * (MAX_SCALE - MIN_SCALE) * 0.5f * precentage * _direction;
            transform.localPosition = new Vector3(x, y, 0);
        }
    }

}
