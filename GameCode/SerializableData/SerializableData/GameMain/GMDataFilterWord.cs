﻿using System;
using System.Collections.Generic;

namespace GameMain
{
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class GMDataFilterWordNode
    {
        [ProtoBuf.ProtoMember(1)]
        public char c;

        [ProtoBuf.ProtoMember(2)]
        public int flag; //1：表示终结，0：延续  

        [ProtoBuf.ProtoMember(3)]
        public List<GMDataFilterWordNode> nodes = new List<GMDataFilterWordNode>();

        [ProtoBuf.ProtoIgnore]
        public Dictionary<char, GMDataFilterWordNode> searchNodes = null;

        /* public ACTDataFilterWordNode(char c)
         {
             this.c = c;
             this.flag = 0;
         }

         public ACTDataFilterWordNode(char c, int flag)
         {
             this.c = c;
             this.flag = flag;
         }*/
    }

    /// <summary>
    /// actor数据
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class GMDataFilterWord
    {
        [ProtoBuf.ProtoMember(1)]
        public GMDataFilterWordNode rootNode = new GMDataFilterWordNode();
        

        public void InitFilterDict(string strContent)
        {
            if (string.IsNullOrEmpty(strContent))
            {
                return;
            }
            string[] strSplit = strContent.Split(',');
            rootNode.c = 'R';
            CreateTree(strSplit);
        }


        private void CreateTree(string[] strSplit)
        {
            int len = strSplit.Length;
            for (int i = 0; i < len; i++)
            {
                char[] chars = strSplit[i].ToCharArray();
                if (chars.Length > 0)
                    InsertNode(rootNode, chars, 0);
            }
        }

        private void InsertNode(GMDataFilterWordNode node, char[] cs, int index)
        {
            GMDataFilterWordNode n = FindNode(node, cs[index]);
            if (n == null)
            {
                n = new GMDataFilterWordNode();
                n.c = cs[index];
                node.nodes.Add( n);
            }

            if (index == (cs.Length - 1))
            {
                n.flag = 1;
            }
            index++;
            if (index < cs.Length)
                InsertNode(n, cs, index);
        }

        private GMDataFilterWordNode FindNode(GMDataFilterWordNode node, char c)
        {
            List<GMDataFilterWordNode> nodes = node.nodes;
            GMDataFilterWordNode rn = null;
            int len = nodes.Count;
            for (int i=0;i<len;i++)
            {
                if (nodes[i].c == c)
                {
                    rn = nodes[i];
                    break;
                }
            }
            return rn;
        }
    }
}
