﻿using System;
using System.Collections.Generic;

public class ResourceMappingData
{
    public int id { get; set; }
    public string SourcePath { get; set; }
    public string PackageName { get; set; }
    public string TargetPath { get; set; }
}