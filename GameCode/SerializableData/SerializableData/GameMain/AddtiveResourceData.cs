﻿using System;
using System.Collections.Generic;

public class AddtiveResourceData
{
    public int id { get; set; }
    public string PackageName { get; set; }
    public string Md5 { get; set; }
    public bool IsNecessary { get; set; }
    public int Level { get; set; }
    public long FileSize { get; set; }
    public bool IsDownloaded { get; set; }
}