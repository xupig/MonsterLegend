﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameResource
{
    public class AssetMemoryMode
    {
        public const int FOREVER = 8;
        public const int REFERENCE = 4;
        public const int ONCEONLY = 2;
    }

    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ReleaseAssetRecord
    {
        private const int UNLOAD_STRATEGY_MASK = 0x0001;
        private const int MEMORY_STRATEGY_MASK = 0x001c;
        private const int LOAD_FROM_FILE_SYSTEM_MASK = 0x0020;
        private const int EXTERNAL_MASK = 0x0080;
        private const int PRIORITY_MASK = 0x0F00;

        [ProtoBuf.ProtoMember(1)]
        public string[] assetBundleNameRecord = new string[0];
        [ProtoBuf.ProtoMember(2)]
        public int[] assetBundleMemoryRecord = new int[0];
        [ProtoBuf.ProtoMember(3)]
        public string[] assetPathRecord = new string[0];
        [ProtoBuf.ProtoMember(4)]
        public int[] assetDependenceIdxRecord = new int[0];
        [ProtoBuf.ProtoMember(5)]
        public ushort[] assetDependenceRecord = new ushort[0];

        [ProtoBuf.ProtoIgnore]
        public Dictionary<string, ushort> assetBundleIdxDict;
        [ProtoBuf.ProtoIgnore]
        public Dictionary<string, int> assetBundleMemoryDict;
        [ProtoBuf.ProtoIgnore]
        public Dictionary<string, ushort> assetIdxDict;
        [ProtoBuf.ProtoIgnore]
        public Dictionary<string, string[]> assetDependenceDict;

        public ReleaseAssetRecord()
        {
            Reset();
        }

        public void Reset()
        {
            assetBundleIdxDict = new Dictionary<string, ushort>(assetBundleNameRecord.Length);
            for (ushort i = 0; i < assetBundleNameRecord.Length; i++)
            {
                assetBundleIdxDict[assetBundleNameRecord[i]] = i;
            }
            assetIdxDict = new Dictionary<string, ushort>(assetPathRecord.Length);
            for (ushort i = 0; i < assetPathRecord.Length; i++)
            {
                assetIdxDict[assetPathRecord[i]] = i;
            }
            assetBundleMemoryDict = new Dictionary<string, int>();
            assetDependenceDict = new Dictionary<string, string[]>();
        }

        public bool ContainsAsset(string name)
        {
            return assetIdxDict.ContainsKey(name);
        }

        public int GetAssetBundleMemory(string name)
        {
            if (assetBundleMemoryDict.ContainsKey(name)) return assetBundleMemoryDict[name];
            int result = 0;
            if (assetBundleIdxDict.ContainsKey(name))
            {
                result = assetBundleMemoryRecord[assetBundleIdxDict[name]];
            }
            assetBundleMemoryDict[name] = result;
            return result;
        }

        public void ChangeMemoryStrategy(string physicalPath, int memoryMode)
        {
            int newValue = 0;
            newValue = GetAssetBundleMemory(physicalPath);
            newValue = newValue & (~MEMORY_STRATEGY_MASK);
            newValue = newValue | (memoryMode << 1) | GetUnloadStrategy(physicalPath);
            assetBundleMemoryDict[physicalPath] = newValue;
        }

        public int GetMemoryStrategy(string physicalPath)
        {
            int result = 0;
            result = GetAssetBundleMemory(physicalPath);
            if (result == 0)
            {
                return AssetMemoryMode.REFERENCE;
            }
            return (result & MEMORY_STRATEGY_MASK) >> 1;
        }

        public int GetUnloadStrategy(string physicalPath)
        {
            int result = 0;
            result = GetAssetBundleMemory(physicalPath);
            if (result == 0)
            {
                return 1;
            }
            return result & UNLOAD_STRATEGY_MASK;
        }

        public bool IsLoadFromFileSystem(string physicalPath)
        {
            int result = 0;
            result = GetAssetBundleMemory(physicalPath);
            return (result & LOAD_FROM_FILE_SYSTEM_MASK) > 0;
        }

        public bool IsExternalAsset(string physicalPath)
        {
            int result = 0;
            result = GetAssetBundleMemory(physicalPath);
            return (result & EXTERNAL_MASK) > 0;
        }

        public int GetPriority(string physicalPath)
        {
            int result = 0;
            result = GetAssetBundleMemory(physicalPath);
            return (result & PRIORITY_MASK) >> 8;
        }

        public string[] GetAssetDependence(string path)
        {
            if (assetDependenceDict.ContainsKey(path)) return assetDependenceDict[path];
            string[] result = null;
            if (assetIdxDict.ContainsKey(path))
            {
                ushort idx = assetIdxDict[path];
                int dpIdx = assetDependenceIdxRecord[idx];
                int dpLen = 0;
                if (idx < assetDependenceIdxRecord.Length - 1)
                {
                    dpLen = assetDependenceIdxRecord[idx + 1] - dpIdx;
                }
                else {
                    dpLen = assetDependenceRecord.Length - dpIdx;
                }
                result = new string[dpLen];
                for (int i = 0; i < dpLen; i++)
                {
                    result[i] = assetBundleNameRecord[assetDependenceRecord[dpIdx + i]];
                }
            }
            else
            {
                Debug.LogError("未找到该资源的打包信息：  " + path);
            }
            assetDependenceDict[path] = result;
            return result;
        }
    }
}
