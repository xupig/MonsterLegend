﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameResource
{
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class CombineAssetRecord
    {
        [ProtoBuf.ProtoMember(1)]
        public string[] assetBundleNameRecord = new string[0];
        [ProtoBuf.ProtoMember(2)]
        public int[] assetBundlePositionRecord = new int[0];

        [ProtoBuf.ProtoIgnore]
        public Dictionary<string, int[]> assetBundleInfo;

        public CombineAssetRecord()
        {
            Reset();
        }

        public void Reset()
        {
            assetBundleInfo = new Dictionary<string, int[]>(assetBundleNameRecord.Length);
            for (ushort i = 0; i < assetBundleNameRecord.Length; i++)
            {
                assetBundleInfo[assetBundleNameRecord[i]] = new int[] { assetBundlePositionRecord[i * 2], assetBundlePositionRecord[i * 2 + 1] };
            }
        }
    }
}
