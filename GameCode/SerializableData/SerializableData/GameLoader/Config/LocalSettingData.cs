﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ProtoBuf;

using UnityEngine;

namespace GameLoader.Config
{
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class LocalSettingData
    {
        [ProtoBuf.ProtoMember(1)]
        public string SelectedCharacter = "";
        [ProtoBuf.ProtoMember(2)]
        public int SelectedServerID = 0;
        [ProtoBuf.ProtoMember(3)]
        public string UserAccount = "";
        [ProtoBuf.ProtoMember(4)]
        public string SelectedServerTemp = string.Empty;
        [ProtoBuf.ProtoMember(5)]
        public ulong SelectedAvatarDbid = 0;     //最近登录的角色dbid
        [ProtoBuf.ProtoMember(6)]
        public List<int> CreateRoleServerList;   //已创建角色的服务器

        public void Copy(LocalSettingData other)
        {
            //this.SelectedCharacter = other.SelectedCharacter;
            this.SelectedServerID = other.SelectedServerID;
            this.UserAccount = other.UserAccount;
            this.SelectedServerTemp = other.SelectedServerTemp;
            this.CreateRoleServerList = other.CreateRoleServerList;
        }
    }
}
