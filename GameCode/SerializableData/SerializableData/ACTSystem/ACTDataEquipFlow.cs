﻿
using UnityEngine;
namespace ACTSystem
{
    /// <summary>
    /// 装备流光效果数据
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTDataEquipFlow
    {
        [ProtoBuf.ProtoMember(1)]
        public int ID;
        [ProtoBuf.ProtoMember(2)]
        public string MaterialPath;
        public UnityEngine.Object Material;
        [ProtoBuf.ProtoMember(3)]
        public string SlotPath;
        public UnityEngine.Object SlotGO;

        [ProtoBuf.ProtoMember(4)]
        public ACTVector2 flowSpeedReal = new ACTVector2();
        public Vector2 flowSpeed;
        [ProtoBuf.ProtoMember(5)]
        public ACTVector4 flowColorReal = new ACTVector4();
        public Color flowColor;
        [ProtoBuf.ProtoMember(6)]
        public string flowTexPath;
        public Texture2D flowTex;

        [ProtoBuf.ProtoMember(7)]
        public string flashCubeTexPath;
        public Cubemap flashCubeTex;
        [ProtoBuf.ProtoMember(8)]
        public ACTVector4 flashColorReal = new ACTVector4();
        public Color flashColor;
        [ProtoBuf.ProtoMember(9)]
        public float flashStrength;
        [ProtoBuf.ProtoMember(10)]
        public float roundTime;

        [ProtoBuf.ProtoMember(11)]
        public bool useLayer2;
        [ProtoBuf.ProtoMember(12)]
        public ACTVector4 flashColor2Real = new ACTVector4();
        public Color flashColor2;
        [ProtoBuf.ProtoMember(13)]
        public float flashStrength2;
        [ProtoBuf.ProtoMember(14)]
        public float cycleTime;
        [ProtoBuf.ProtoMember(15)]
        public float startTime2;
        [ProtoBuf.ProtoMember(16)]
        public float roundTime2;

        public ACTDataEquipFlow Clone()
        {
            ACTDataEquipFlow result = new ACTDataEquipFlow();
            result.ID = this.ID;
            result.MaterialPath = this.MaterialPath;
            result.Material = this.Material;
            result.SlotPath = this.SlotPath;
            result.SlotGO = this.SlotGO;
            result.flowSpeedReal = this.flowSpeedReal;
            result.flowSpeed = this.flowSpeed;
            result.flowColorReal = this.flowColorReal;
            result.flowColor = this.flowColor;
            result.flowTexPath = this.flowTexPath;
            result.flowTex = this.flowTex;
            result.flashCubeTexPath = this.flashCubeTexPath;
            result.flashCubeTex = this.flashCubeTex;
            result.flashColorReal = this.flashColorReal;
            result.flashColor = this.flashColor;
            result.flashStrength = this.flashStrength;
            result.roundTime = this.roundTime;
            result.useLayer2 = this.useLayer2;
            result.flashColor2Real = this.flashColor2Real;
            result.flashColor2 = this.flashColor2;
            result.flashStrength2 = this.flashStrength2;
            result.cycleTime = this.cycleTime;
            result.startTime2 = this.startTime2;
            result.roundTime2 = this.roundTime2;

            return result;
        }
    }
}
