﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

using UnityEngine;

namespace ACTSystem
{

    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTSerializableData
    {
        [ProtoBuf.ProtoMember(1)]
        public Dictionary<int, ACTDataSkill> SkillsDict = new Dictionary<int, ACTDataSkill>();

        [ProtoBuf.ProtoMember(2)]
        public Dictionary<int, ACTDataActor> ActorsDict = new Dictionary<int, ACTDataActor>();

        [ProtoBuf.ProtoMember(3)]
        public Dictionary<int, ACTDataVisualFX> VisualFXsDict = new Dictionary<int, ACTDataVisualFX>();

        [ProtoBuf.ProtoMember(4)]
        public Dictionary<int, ACTDataEquipment> EquipmentDict = new Dictionary<int, ACTDataEquipment>();

        [ProtoBuf.ProtoMember(5)]
        public Dictionary<int, ACTDataEquipmentFX> EquipmentFXDict = new Dictionary<int, ACTDataEquipmentFX>();

        public ACTSerializableData()
        { 
            
        }
    }

}
