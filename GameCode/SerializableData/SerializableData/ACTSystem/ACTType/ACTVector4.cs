﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACTSystem
{

    /// <summary>
    /// 视觉特效数据
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTVector4
    {
        [ProtoBuf.ProtoMember(11)]
        public float x;
        [ProtoBuf.ProtoMember(12)]
        public float y;
        [ProtoBuf.ProtoMember(13)]
        public float z;
        [ProtoBuf.ProtoMember(14)]
        public float w;
        

    }

     
}
