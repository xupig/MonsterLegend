﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    [System.Serializable]
    public enum ACTEquipmentType
    {
        Cloth = 1,
        Weapon = 11,
        Wing = 21,
        Head = 31,
        Hair = 41,
        DeputyWeapon = 51,
        Manteau = 61,
        Effect = 71,
    }
}
