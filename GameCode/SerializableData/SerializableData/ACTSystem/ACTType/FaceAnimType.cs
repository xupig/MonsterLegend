﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACTSystem
{    /// <summary>
    /// 面部动画类型
    /// </summary>
    [System.Serializable]
    public enum FaceAnimType
    {
        NoAnimation = 1,
        OneFrame = 11,
        MultiFrames = 21,
        Loop = 31,
    }
}
