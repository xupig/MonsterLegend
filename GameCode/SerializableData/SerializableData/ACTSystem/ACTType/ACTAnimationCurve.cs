﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTKeyframe
    {
        [ProtoBuf.ProtoMember(1)]
        public float time;

        [ProtoBuf.ProtoMember(2)]
        public float value;

        [ProtoBuf.ProtoMember(3)]
        public float inTangent;

        [ProtoBuf.ProtoMember(4)]
        public float outTangent;    
    
        [ProtoBuf.ProtoMember(5)]
        public int tangentMode;   
    }

    /// <summary>
    /// ACT重定义的AnimationCurve
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTAnimationCurve
    {
        [ProtoBuf.ProtoMember(11)]
        public List<ACTKeyframe> keyframeList;

        public void CloneFromAnimationCurve(AnimationCurve sourceCurve)
        {
            if (keyframeList == null) keyframeList = new List<ACTKeyframe>();
            var DValue = sourceCurve.length - keyframeList.Count;
            if (DValue > 0)
            {
                for (int i = 0; i < DValue; i++)
                {
                    keyframeList.Add(new ACTKeyframe());
                }
            }
            else if (DValue < 0)
            {
                keyframeList.RemoveRange(keyframeList.Count - DValue, DValue); 
            }
            for (int i = 0; i < sourceCurve.length; i++)
            {
                keyframeList[i].time = sourceCurve[i].time;
                keyframeList[i].value = sourceCurve[i].value;
                keyframeList[i].inTangent = sourceCurve[i].inTangent;
                keyframeList[i].outTangent = sourceCurve[i].outTangent;
                keyframeList[i].tangentMode = sourceCurve[i].tangentMode;
            }
        }
    }

     
}
