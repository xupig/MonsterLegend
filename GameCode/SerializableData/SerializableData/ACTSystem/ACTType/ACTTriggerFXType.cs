﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    [System.Serializable]
    public enum ACTTriggerFXType
    {
        OnGround = 0,
        OnAir = 1
    }
}
