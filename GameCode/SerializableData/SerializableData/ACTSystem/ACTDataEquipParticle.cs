﻿using System.Collections.Generic;
using UnityEngine;

namespace ACTSystem
{
    /// <summary>
    /// 装备粒子效果数据
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTDataEquipParticle
    {
        [ProtoBuf.ProtoMember(1)]
        public int ID;
        [ProtoBuf.ProtoMember(2)]
        public List<ACTDataParticlePrefab> PrefabList = new List<ACTDataParticlePrefab>();

        public ACTDataEquipParticle Clone()
        {
            ACTDataEquipParticle result = new ACTDataEquipParticle();
            result.ID = this.ID;
            result.PrefabList = new List<ACTDataParticlePrefab>();
            for (int i = 0; i < this.PrefabList.Count; ++i)
            {
                result.PrefabList.Add(this.PrefabList[i].Clone());
            }
            return result;
        }

        public ACTDataParticlePrefab CreatePrefab()
        {
            ACTDataParticlePrefab prefab = new ACTDataParticlePrefab();
            PrefabList.Add(prefab);
            return prefab;
        }

        public bool DeletePrefab(ACTDataParticlePrefab prefab)
        {
            return PrefabList.Remove(prefab);
        }
    }
}
