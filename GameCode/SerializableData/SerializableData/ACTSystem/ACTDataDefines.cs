﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    [System.Serializable]
    public enum ACTTargetDetectorType
    {
        SPHERE_CAST_ALL,
        OVERLAP_SPHERE
    }

    [System.Serializable]
    public enum ACTVFXLocationType
    {
        SELF_LOCAL = 0,
        SELF_SLOT = 1,
        SELF_WORLD = 2,
        SLOT_WORLD = 3,
        CUSTOM_WORLD = 4,
        SELF_LOCAL_ONLY_POS = 5,
        SELF_SLOT_ONLY_POS = 6,
        SELF_LOCAL_SLOT = 7,
        BEHIT_WORLD = 8,
        BEHIT_LOCAL = 9,
        SELF_LOCAL_ONLY_POS_WORLD_FACE = 10,
    }

    [System.Serializable]
    public enum ACTVFXConflictType
    {
        NotConflict = 0,
        Conflict = 1,
    }

    [System.Serializable]
    public enum ACTSoundType
    {
        /// <summary>
        /// 攻击音效
        /// </summary>
        AttackSound = 0,

        /// <summary>
        /// 吼叫音效
        /// </summary>
        Roar = 1,
    }

    [System.Serializable]
    public enum ACTCalcTargetLocation
    {
        Auto = 0,

        Custom = 1,
                                    
    }

    [System.Serializable]
    public enum ACTPlaySoundType
    {
        /// <summary>
        /// 顺序播放
        /// </summary>
        Order = 0,

        /// <summary>
        /// 随机播放
        /// </summary>
        Random = 1,
    }

}
