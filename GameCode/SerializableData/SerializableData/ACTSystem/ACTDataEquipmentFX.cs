﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{


    /// <summary>
    /// actor数据
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTDataEquipmentFX
    {
        [ProtoBuf.ProtoMember(1)]
        public int ID;
        [ProtoBuf.ProtoMember(2)]
        public string MaterialPath;
        public UnityEngine.Object Material;
        [ProtoBuf.ProtoMember(3)]
        public string ParticleSysPath;
        public UnityEngine.Object ParticleSys;
        [ProtoBuf.ProtoMember(4)]
        public string EquipSlotLeftPath;
        public UnityEngine.Object EquipSlotLeft;
        [ProtoBuf.ProtoMember(5)]
        public string EquipSlotRightPath;
        public UnityEngine.Object EquipSlotRight;
        [ProtoBuf.ProtoMember(6)]
        public int TypeID = 9999;

        public ACTDataEquipmentFX Clone()
        {
            ACTDataEquipmentFX result = new ACTDataEquipmentFX();
            result.ID = this.ID;
            result.MaterialPath = this.MaterialPath;
            result.Material = this.Material;
            result.ParticleSysPath = this.ParticleSysPath;
            result.ParticleSys = this.ParticleSys;
            result.EquipSlotLeftPath = this.EquipSlotLeftPath;
            result.EquipSlotLeft = this.EquipSlotLeft;
            result.EquipSlotRightPath = this.EquipSlotRightPath;
            result.EquipSlotRight = this.EquipSlotRight;
            result.TypeID = this.TypeID;
            return result;
        }

        public string GetMaterialPath()
        {
            return MaterialPath.Substring("Assets/Resources/".Length);
        }

        public string GetParticleSysPath()
        {
            return ParticleSysPath.Substring("Assets/Resources/".Length);
        }

        public string GetEquipSlotLeftPath()
        {
            return EquipSlotLeftPath.Substring("Assets/Resources/".Length);
        }

        public string GetEquipSlotRightPath()
        {
            return EquipSlotRightPath.Substring("Assets/Resources/".Length);
        }
    }


}
