﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

using UnityEngine;

namespace ACTSystem
{


    /// <summary>
    /// 
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTDataSkill 
    {
        /// <summary>
        /// 行为ID
        /// </summary>
        [ProtoBuf.ProtoMember(1)]
        public int ID;
        [ProtoBuf.ProtoMember(2)]
        public int TypeID = 9999;
        [ProtoBuf.ProtoMember(3)]
        public float Duration = 1;
        [ProtoBuf.ProtoMember(4)]
        public float LinkTime = 0.8f;
        [ProtoBuf.ProtoMember(5)]
        public int m_maxIdx = 1;

        /// <summary>
        /// 效果列表
        /// </summary>
        [ProtoBuf.ProtoMember(11)]
        public List<ACTEventVisualFX> VisualFXList = new List<ACTEventVisualFX>();
        [ProtoBuf.ProtoMember(12)]
        public List<ACTEventAnimationFX> AnimationFXList = new List<ACTEventAnimationFX>();
        [ProtoBuf.ProtoMember(13)]
        public List<ACTEventSoundFX> SoundFXList = new List<ACTEventSoundFX>();
        [ProtoBuf.ProtoMember(14)]
        public List<ACTEventBeHitVisualFX> BeHitVisualFXList = new List<ACTEventBeHitVisualFX>();
        [ProtoBuf.ProtoMember(15)]
        public List<ACTEventStateChange> StateChangeList = new List<ACTEventStateChange>();
        [ProtoBuf.ProtoMember(16)]
        public List<ACTEventBeHitSetting> BeHitSettingList = new List<ACTEventBeHitSetting>();
        [ProtoBuf.ProtoMember(17)]
        public List<ACTEventBeHitSoundFX> BeHitSoundFXList = new List<ACTEventBeHitSoundFX>();
        [ProtoBuf.ProtoMember(18)]
        public List<ACTEventTargetDetector> TargetDetectorList = new List<ACTEventTargetDetector>();
        [ProtoBuf.ProtoMember(19)]
        public List<ACTEventWeaponState> WeaponStateList = new List<ACTEventWeaponState>();
        [ProtoBuf.ProtoMember(20)]
        public List<ACTEventTracerVisualFX> TracerVisualFXList = new List<ACTEventTracerVisualFX>();
        [ProtoBuf.ProtoMember(21)]
        public List<ACTEventBulletVisualFX> BulletVisualFXList = new List<ACTEventBulletVisualFX>();
        [ProtoBuf.ProtoMember(22)]
        public List<ACTEventLaserVisualFX> LaserVisualFXList = new List<ACTEventLaserVisualFX>();
        [ProtoBuf.ProtoMember(23)]
        public List<ACTEventPostEffectFX> PostEffectFXList = new List<ACTEventPostEffectFX>();
        [ProtoBuf.ProtoMember(24)]
        public List<ACTEventTriggerVisualFX> TriggerVisualFXList = new List<ACTEventTriggerVisualFX>();
        [ProtoBuf.ProtoMember(25)]
        public List<ACTEventChangeMaterial> ChangeMaterialList = new List<ACTEventChangeMaterial>();
        [ProtoBuf.ProtoMember(26)]
        public List<ACTEventCameraAnimation> CameraAnimationList = new List<ACTEventCameraAnimation>();
        [ProtoBuf.ProtoMember(27)]
        public List<ACTEventGhostShadow> GhostShadowList = new List<ACTEventGhostShadow>();
        [ProtoBuf.ProtoMember(28)]
        public List<ACTEventBeHitChangeMaterial> BeHitChangeMaterialList = new List<ACTEventBeHitChangeMaterial>();
        
        [ProtoBuf.ProtoIgnore]
        private List<ACTEventBase> m_tempAllFXList = new List<ACTEventBase>();

        [ProtoBuf.ProtoIgnore]
        private bool m_isAllFXListDirty = true;

        [ProtoBuf.ProtoIgnore]
        private Dictionary<EventType, IList> m_typeListMap = new Dictionary<EventType, IList>(18);

        [ProtoBuf.ProtoIgnore]
        private Dictionary<int, ACTEventBase> m_indexEventMap = new Dictionary<int, ACTEventBase>();

        public ACTDataSkill()
        {
            m_typeListMap.Add(EventType.VisualFX, VisualFXList);
            m_typeListMap.Add(EventType.AnimationFX, AnimationFXList);
            m_typeListMap.Add(EventType.SoundFX, SoundFXList);
            m_typeListMap.Add(EventType.BeHitVisualFX, BeHitVisualFXList);
            m_typeListMap.Add(EventType.StateChange, StateChangeList);
            m_typeListMap.Add(EventType.BeHitSetting, BeHitSettingList);
            m_typeListMap.Add(EventType.BeHitSoundFX, BeHitSoundFXList);
            m_typeListMap.Add(EventType.TargetDetector, TargetDetectorList);
            m_typeListMap.Add(EventType.WeaponState, WeaponStateList);
            m_typeListMap.Add(EventType.TracerVisualFX, TracerVisualFXList);
            m_typeListMap.Add(EventType.BulletVisualFX, BulletVisualFXList);
            m_typeListMap.Add(EventType.LaserVisualFX, LaserVisualFXList);
            m_typeListMap.Add(EventType.PostEffectFX, PostEffectFXList);
            m_typeListMap.Add(EventType.TriggerVisualFX, TriggerVisualFXList);
            m_typeListMap.Add(EventType.ChangeMaterial, ChangeMaterialList);
            m_typeListMap.Add(EventType.CameraAnimation, CameraAnimationList);
            m_typeListMap.Add(EventType.GhostShadow, GhostShadowList);
            m_typeListMap.Add(EventType.BeHitChangeMaterial, BeHitChangeMaterialList);
        }

        public ACTDataSkill Clone()
        {
            ACTDataSkill result = new ACTDataSkill();
            result.ID = this.ID;
            result.TypeID = this.TypeID;
            result.Duration = this.Duration;
            result.LinkTime = this.LinkTime;
            result.m_maxIdx = this.m_maxIdx;

            CloneList(VisualFXList, result.VisualFXList);
            CloneList(AnimationFXList, result.AnimationFXList);
            CloneList(SoundFXList, result.SoundFXList);
            CloneList(BeHitVisualFXList, result.BeHitVisualFXList);
            CloneList(StateChangeList, result.StateChangeList);
            CloneList(BeHitSettingList, result.BeHitSettingList);
            CloneList(BeHitSoundFXList, result.BeHitSoundFXList);
            CloneList(TargetDetectorList, result.TargetDetectorList);
            CloneList(WeaponStateList, result.WeaponStateList);
            CloneList(TracerVisualFXList, result.TracerVisualFXList);
            CloneList(BulletVisualFXList, result.BulletVisualFXList);
            CloneList(LaserVisualFXList, result.LaserVisualFXList);
            CloneList(PostEffectFXList, result.PostEffectFXList);
            CloneList(TriggerVisualFXList, result.TriggerVisualFXList);
            CloneList(ChangeMaterialList, result.ChangeMaterialList);
            CloneList(CameraAnimationList, result.CameraAnimationList);
            CloneList(GhostShadowList, result.GhostShadowList);
            CloneList(BeHitChangeMaterialList, result.BeHitChangeMaterialList);
            result.GetEvents();
            return result;
        }

        static void CloneList<T>(List<T> srcList, List<T> tarList) where T : ACTEventBase
        { 
            //List<T> result = new List<T>();
            tarList.Clear();
            foreach (var sub in srcList)
            {
                tarList.Add(sub.Clone() as T);
            }
            //return result;
        }

        public void SetDirty(bool value = true)
        {
            m_isAllFXListDirty = value;
        }

        public void AddEvent(ACTEventBase sevent)
        {
            if (sevent.EventIndex == 0) sevent.EventIndex = GetEventIndex();
            m_typeListMap[sevent.EventFXType].Add(sevent);
            SetDirty();
        }

        public void RemoveEvent(ACTEventBase sevent)
        {
            m_typeListMap[sevent.EventFXType].Remove(sevent);
            SetDirty();
        }

        public void CheckDirty()
        {
            if (m_isAllFXListDirty)
            {
                SetDirty(false);
                m_tempAllFXList.Clear();
                m_indexEventMap.Clear();
                var enumr = m_typeListMap.GetEnumerator();
                while (enumr.MoveNext())
                {
                    IList list = enumr.Current.Value;
                    for (int i = 0; i < list.Count; i++)
                    {
                        var actEvent = list[i] as ACTEventBase;
                        m_tempAllFXList.Add(actEvent);
                        actEvent.SkillID = this.ID;
                        m_indexEventMap[actEvent.EventIndex] = actEvent;
                    }
                }
                m_tempAllFXList.Sort((a, b) => { return a.EventIndex - b.EventIndex; });
            }
        }

        public List<ACTEventBase> GetEvents()
        {
            CheckDirty();
            return m_tempAllFXList;
        }

        public ACTEventBase GetEventByIndex(int idx)
        {
            ACTEventBase result = null;
            CheckDirty();
            if (m_indexEventMap.ContainsKey(idx))
            {
                result = m_indexEventMap[idx];
            }
            return result;
        }

        public void ResetID()
        {
            CheckDirty();
            var enumr = m_indexEventMap.GetEnumerator();
            int currentEventIndex = 0;
            while (enumr.MoveNext())
            {
                currentEventIndex = enumr.Current.Value.EventIndex + 1;
                if (m_maxIdx <= currentEventIndex)
                {
                    m_maxIdx = currentEventIndex;
                }
            }
        }

        public void ChangeEvent(ACTEventBase sevent, EventType fxType)
        {
            RemoveEvent(sevent);
            CreateEvent(fxType, sevent.EventIndex);
        }

        public ACTEventBase CreateEvent(EventType fxType = EventType.VisualFX, int idx = -1)
        {
            ACTEventBase result = null;

            if (fxType == EventType.VisualFX) result = new ACTEventVisualFX();
            else if (fxType == EventType.AnimationFX) result = new ACTEventAnimationFX();
            else if (fxType == EventType.SoundFX) result = new ACTEventSoundFX();
            else if (fxType == EventType.BeHitVisualFX) result = new ACTEventBeHitVisualFX();
            else if (fxType == EventType.StateChange) result = new ACTEventStateChange();
            else if (fxType == EventType.BeHitSetting) result = new ACTEventBeHitSetting();
            else if (fxType == EventType.BeHitSoundFX) result = new ACTEventBeHitSoundFX();
            else if (fxType == EventType.TargetDetector) result = new ACTEventTargetDetector();
            else if (fxType == EventType.WeaponState) result = new ACTEventWeaponState();
            else if (fxType == EventType.TracerVisualFX) result = new ACTEventTracerVisualFX();
            else if (fxType == EventType.BulletVisualFX) result = new ACTEventBulletVisualFX();
            else if (fxType == EventType.LaserVisualFX) result = new ACTEventLaserVisualFX();
            else if (fxType == EventType.PostEffectFX) result = new ACTEventPostEffectFX();
            else if (fxType == EventType.TriggerVisualFX) result = new ACTEventTriggerVisualFX();
            else if (fxType == EventType.ChangeMaterial) result = new ACTEventChangeMaterial();
            else if (fxType == EventType.CameraAnimation) result = new ACTEventCameraAnimation();
            else if (fxType == EventType.GhostShadow) result = new ACTEventGhostShadow();
            else if (fxType == EventType.BeHitChangeMaterial) result = new ACTEventBeHitChangeMaterial();
            result.EventIndex = idx > 0 ? idx : GetEventIndex();
            AddEvent(result);
            return result;
        }

        public void MergeSoundData(ACTDataSkill soundData)
        {
            BeHitSoundFXList = soundData.BeHitSoundFXList;
            SoundFXList = soundData.SoundFXList;
            for (int i = 0; i < BeHitSoundFXList.Count; ++i)
            {
                BeHitSoundFXList[i].EventIndex = GetEventIndex();
            }
            for (int i = 0; i < SoundFXList.Count; ++i)
            {
                SoundFXList[i].EventIndex = GetEventIndex();
            }
            SetDirty(true);
        }

        private int GetEventIndex()
        {
            return m_maxIdx++;
        }
    }

    
}
