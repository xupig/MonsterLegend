﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{


    /// <summary>
    /// actor数据
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTDataEquipment
    {
        [ProtoBuf.ProtoMember(1)]
        public int ID;
        [ProtoBuf.ProtoMember(2)]
        public ACTEquipmentType EquipType = ACTEquipmentType.Cloth;

        [ProtoBuf.ProtoMember(3)]
        public string Slot;
        [ProtoBuf.ProtoMember(4)]
        public string CitySlot;
        [ProtoBuf.ProtoMember(5)]
        public string PrefabPath;
        public UnityEngine.Object Prefab;
        [ProtoBuf.ProtoMember(25)]
        public string PrefabPath_L;
        public UnityEngine.Object Prefab_L;

        [ProtoBuf.ProtoMember(6)]
        public string Slot2;
        [ProtoBuf.ProtoMember(7)]
        public string CitySlot2;
        [ProtoBuf.ProtoMember(8)]
        public string PrefabPath2;
        public UnityEngine.Object Prefab2;
        [ProtoBuf.ProtoMember(28)]
        public string PrefabPath2_L;
        public UnityEngine.Object Prefab2_L;

        [ProtoBuf.ProtoMember(10)]
        public string MeshPath;
        public UnityEngine.Object Mesh;
        [ProtoBuf.ProtoMember(20)]
        public string MeshPath_L;
        public UnityEngine.Object Mesh_L;

        [ProtoBuf.ProtoMember(11)]
        public string MaterialPath;
        public UnityEngine.Object Material;
        [ProtoBuf.ProtoMember(21)]
        public string MaterialPath_L;
        public UnityEngine.Object Material_L;

        [ProtoBuf.ProtoMember(12)]
        public string MaterialHQPath;
        public UnityEngine.Object MaterialHQ;

        [ProtoBuf.ProtoMember(14)]
        public int TypeID = 9999;

        [ProtoBuf.ProtoMember(15)]
        public List<ACTDataEquipParticle> ParticleList;

        [ProtoBuf.ProtoMember(16)]
        public List<ACTDataEquipFlow> FlowList;

        [ProtoBuf.ProtoMember(19)] 
        public FaceAnimType FaceAnimationType= FaceAnimType.NoAnimation;

        [ProtoBuf.ProtoMember(18)] 
        public List<ACTDataEquipFaceAnimation> FaceAnimations;

        [ProtoBuf.ProtoMember(17)]
        public string PhysicalClothPath;
        public UnityEngine.Object PhysicalCloth;

        public ACTDataEquipment Clone()
        {
            ACTDataEquipment result = new ACTDataEquipment();
            result.ID = this.ID;
            result.EquipType = this.EquipType;

            result.Slot = this.Slot;
            result.PrefabPath = this.PrefabPath;
            result.Prefab = this.Prefab;

            result.Slot2 = this.Slot2;
            result.PrefabPath2 = this.PrefabPath2;
            result.Prefab2 = this.Prefab2;

            result.MeshPath = this.MeshPath;
            result.Mesh = this.Mesh;

            result.MaterialPath = this.MaterialPath;
            result.Material = this.Material;

            result.TypeID = this.TypeID;

            result.ParticleList = new List<ACTDataEquipParticle>();
            for (int i = 0; i < this.ParticleList.Count; ++i)
            {
                result.ParticleList.Add(this.ParticleList[i].Clone());
            }

            result.FlowList = new List<ACTDataEquipFlow>();
            for (int i = 0; i < this.FlowList.Count; ++i)
            {
                result.FlowList.Add(this.FlowList[i].Clone());
            }
            result.FaceAnimationType = this.FaceAnimationType;
            result.FaceAnimations=new List<ACTDataEquipFaceAnimation>();
            for (int i = 0; i < this.FaceAnimations.Count; i++)
            {
                result.FaceAnimations.Add(this.FaceAnimations[i].Clone());
            }
            return result;
        }

        public string GetModelAssetPath()
        {
            return PrefabPath.Substring("Assets/Resources/".Length);
        }

        public string GetMaterialAssePath()
        {
            return MaterialPath.Substring("Assets/Resources/".Length);
        }

        public ACTDataEquipParticle LoadOrCreateParticle(int id)
        {
            int index = HasEquipParticle(id);
            if (index == -1)
            {
                return CreateACTDataEquipParticle(id);
            }
            return ParticleList[index];
        }

        private int HasEquipParticle(int id)
        {
            if (ParticleList == null)
            {
                return -1;
            }
            for (int i = 0; i < ParticleList.Count; ++i)
            {
                if (ParticleList[i].ID == id)
                {
                    return i;
                }
            }
            return -1;
        }

        ACTDataEquipParticle CreateACTDataEquipParticle(int id)
        {
            var data = new ACTDataEquipParticle();
            data.ID = id;
            ParticleList.Add(data);
            return data;
        }

        public void RemoveACTDataEquipParticle(int id)
        {
            int index = HasEquipParticle(id);
            if (index > -1)
            {
                ParticleList.RemoveAt(index);
            }
        }

        public ACTDataEquipParticle GetEquipParticleData(int id)
        {
            int index = HasEquipParticle(id);
            if (index == -1)
            {
                return null;
            }
            return ParticleList[index];
        }

        public ACTDataEquipFlow LoadOrCreateFlow(int id)
        {
            int index = HasEquipFlow(id);
            if (index == -1)
            {
                return CreateACTDataEquipFlow(id);
            }
            return FlowList[index];
        }

        private int HasEquipFlow(int id)
        {
            if (FlowList == null)
            {
                return -1;
            }
            for (int i = 0; i < FlowList.Count; ++i)
            {
                if (FlowList[i].ID == id)
                {
                    return i;
                }
            }
            return -1;
        }

        ACTDataEquipFlow CreateACTDataEquipFlow(int id)
        {
            var data = new ACTDataEquipFlow();
            data.ID = id;
            FlowList.Add(data);
            return data;
        }

        public void RemoveACTDataEquipFlow(int id)
        {
            int index = HasEquipFlow(id);
            if (index > -1)
            {
                FlowList.RemoveAt(index);
            }
        }

        public ACTDataEquipFlow GetEquipFlowData(int id)
        {
            int index = HasEquipFlow(id);
            if (index == -1)
            {
                return null;
            }
            return FlowList[index];
        }
    }
}
