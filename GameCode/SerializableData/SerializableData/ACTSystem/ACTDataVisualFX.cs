﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    /// <summary>
    /// actor数据
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTDataVisualFX
    {
        [ProtoBuf.ProtoMember(9)]
        public int TypeID = 9999;

        [ProtoBuf.ProtoMember(10)]
        public int ID;

        [ProtoBuf.ProtoMember(11)]
        public string Perfabs_H_Name;
        public UnityEngine.Object Perfabs_H;

        [ProtoBuf.ProtoMember(12)]
        public string Perfabs_M_Name;
        public UnityEngine.Object Perfabs_M;

        [ProtoBuf.ProtoMember(13)]
        public string Perfabs_L_Name;
        public UnityEngine.Object Perfabs_L;

        /// <summary>
        /// 持续时间
        /// </summary>
        [ProtoBuf.ProtoMember(16)]
        public float Duration = -1;

        /// <summary>
        /// 是否循环
        /// </summary>
        [ProtoBuf.ProtoMember(17)]
        public bool IsLoop = true;

        /// <summary>
        /// 定位类型
        /// </summary>
        [ProtoBuf.ProtoMember(18)]
        public ACTVFXLocationType LocationType;

        /// <summary>
        /// 定位坐标
        /// </summary>
        [ProtoBuf.ProtoMember(19)]
        public ACTVector3 LocationReal = new ACTVector3();
        public Vector3 Location;

        /// <summary>
        /// 定位坐标
        /// </summary>
        [ProtoBuf.ProtoMember(20)]
        public string Slot;


        [ProtoBuf.ProtoMember(21)]
        public string SkinSlot="body";

        [ProtoBuf.ProtoMember(22)]
        public float DisappearDelay = 0;

        public ACTDataVisualFX Clone()
        {
            ACTDataVisualFX result = new ACTDataVisualFX();
            result.ID = this.ID;
            result.Perfabs_H_Name = this.Perfabs_H_Name;
            result.Perfabs_H = this.Perfabs_H;
            result.Perfabs_M_Name = this.Perfabs_M_Name;
            result.Perfabs_M = this.Perfabs_M;
            result.Perfabs_L_Name = this.Perfabs_L_Name;
            result.Perfabs_L = this.Perfabs_L;
            result.Duration = this.Duration;
            result.IsLoop = this.IsLoop;
            result.LocationType = this.LocationType;
            result.LocationReal = this.LocationReal;
            result.Location = this.Location;
            result.Slot = this.Slot;
            result.SkinSlot = this.SkinSlot;
            result.DisappearDelay = this.DisappearDelay;
            return result;
        }
    }
}
