﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{

    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTEventTargetDetector : ACTEventBase
    {
        [ProtoBuf.ProtoMember(11)]
        public ACTTargetDetectorType FilterType = ACTTargetDetectorType.SPHERE_CAST_ALL;

        [ProtoBuf.ProtoMember(12)]
        public ACTVector3 OffsetReal = new ACTVector3();
        public Vector3 Offset = new Vector3(0,0,0);

        [ProtoBuf.ProtoMember(13)]
        public ACTVector3 DiretionReal = new ACTVector3();
        public Vector3 Diretion = new Vector3(0,0,0);

        [ProtoBuf.ProtoMember(14)]
        public float Radius = 0;

        [ProtoBuf.ProtoMember(15)]
        public float Distance = 0;


        public ACTEventTargetDetector()
        {
            this.EventFXType = EventType.TargetDetector;
        }

        public override ACTEventBase Clone()
        {
            ACTEventTargetDetector result = new ACTEventTargetDetector();
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            result.EventIndex = this.EventIndex;

            result.FilterType = this.FilterType;
            result.OffsetReal = this.OffsetReal;
            result.Offset = this.Offset;
            result.DiretionReal = this.DiretionReal;
            result.Diretion = this.Diretion;
            result.Radius = this.Radius;
            result.Distance = this.Distance;
            return result;
        }
    }

    
}
