﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{

    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTEventCameraAnimation : ACTEventBase
    {
        /// <summary>
        /// 资源列表
        /// </summary>
        [ProtoBuf.ProtoMember(11)]
        public string Perfab_Name;
        public UnityEngine.Object Perfab;

        /// <summary>
        /// 持续时间
        /// </summary>
        [ProtoBuf.ProtoMember(12)]
        public float Duration = 1;

        /// <summary>
        /// 定位坐标
        /// </summary>
        [ProtoBuf.ProtoMember(13)]
        public string Slot;

        public ACTEventCameraAnimation()
        {
            this.EventFXType = EventType.CameraAnimation;
        }

        public override ACTEventBase Clone()
        {
            ACTEventCameraAnimation result = new ACTEventCameraAnimation();
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            result.EventIndex = this.EventIndex;

            result.Perfab_Name = this.Perfab_Name;
            result.Duration = this.Duration;
            result.Slot = this.Slot;
            return result;
        }
    }
}
