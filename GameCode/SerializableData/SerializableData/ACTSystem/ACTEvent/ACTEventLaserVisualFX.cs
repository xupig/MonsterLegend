﻿#region 模块信息
/*==========================================
// 模块名：ACTEventLaserVisualFX
// 命名空间: ACTSystem
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/05/19
// 描述说明：技能事件激光特效
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace ACTSystem
{
   
    /// <summary>
    /// 激光定义
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTLaser
    {
        /// <summary>
        /// 激光特效ID 
        /// </summary>
        [ProtoBuf.ProtoMember(11)]
        public int VisualFXID;

        /// <summary>
        /// 开始位置激光特效ID 
        /// </summary>
        [ProtoBuf.ProtoMember(12)]
        public int startFXID;

        /// <summary>
        /// 结束位置激光特效ID 
        /// </summary>
        [ProtoBuf.ProtoMember(13)]
        public int endFXID;

        /// <summary>
        /// 投射者坐标修正
        /// </summary>
        [ProtoBuf.ProtoMember(14)]
        public ACTVector3 LocationReal = new ACTVector3();
        public Vector3 Location;

        /// <summary>
        /// 目标坐标修正
        /// </summary>
        [ProtoBuf.ProtoMember(15)]
        public ACTVector3 targetLocationReal = new ACTVector3();
        public Vector3 targetLocation;

        

        public ACTLaser Clone()
        {
            ACTLaser result = new ACTLaser();
            result.VisualFXID = this.VisualFXID;
            result.startFXID = this.startFXID;
            result.endFXID = this.endFXID;
            result.LocationReal = this.LocationReal;
            result.Location = this.Location;
            return result;
        }

    }

    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTEventLaserVisualFX : ACTEventBase
    {
        /// <summary>
        /// 起点坐标
        /// </summary>
        [ProtoBuf.ProtoMember(11)]
        public ACTVector3 startPositionReal = new ACTVector3();
        public Vector3 startPosition;

        /// <summary>
        /// 终点坐标
        /// </summary>
        [ProtoBuf.ProtoMember(12)]
        public ACTVector3 endPositionReal = new ACTVector3();
        public Vector3 endPosition;

        [ProtoBuf.ProtoMember(13)]
        public List<ACTLaser> LaserList = new List<ACTLaser>();

        /// <summary>
        /// 开始点定位坐标
        /// </summary>
        [ProtoBuf.ProtoMember(14)]
        public string startSlotName;

        /// <summary>
        /// 结束点定位坐标
        /// </summary>
        [ProtoBuf.ProtoMember(15)]
        public string endSlotName;

        /// <summary>
        /// 定位类型
        /// </summary>
        [ProtoBuf.ProtoMember(16)]
        public ACTVFXLocationType LocationType;

        /// <summary>
        /// 是否顶替
        /// </summary>
        [ProtoBuf.ProtoMember(17)]
        public ACTVFXConflictType IsReplace;

        /// <summary>
        /// 持续时间
        /// </summary>
        [ProtoBuf.ProtoMember(18)]
        public float Duration;


        public ACTEventLaserVisualFX()
        {
            this.EventFXType = EventType.LaserVisualFX;
        }

        public override ACTEventBase Clone()
        {
            ACTEventLaserVisualFX result = new ACTEventLaserVisualFX();
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            foreach (var laser in LaserList)
            {
                result.LaserList.Add(laser.Clone());
            }
            result.EventIndex = this.EventIndex;
            result.LocationType = this.LocationType;
            result.startPosition = this.startPosition;
            result.endPosition = this.endPosition;
            result.startSlotName = this.startSlotName;
            result.endSlotName = this.endSlotName;
            result.IsReplace = this.IsReplace;
            result.Duration = this.Duration;
            return result;
        }

    }
}
