﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{

    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTBeHitSound 
    {
        /// <summary>
        /// 播放概率
        /// </summary>
        [ProtoBuf.ProtoMember(11)]
        public float PlayRate;

        /// <summary>
        /// 声音文件
        /// </summary>
        [ProtoBuf.ProtoMember(12)]
        public string Clip_Name;
        public AudioClip Clip;

        /// <summary>
        /// 声音文件路径
        /// </summary>
        [ProtoBuf.ProtoMember(13)]
        public string ClipPath;

        /// <summary>
        /// 音量
        /// </summary>
        [ProtoBuf.ProtoMember(14)]
        public float Volume = 1;

        public ACTBeHitSound Clone()
        {
            ACTBeHitSound result = new ACTBeHitSound();
            result.PlayRate = this.PlayRate;
            result.Clip_Name = this.Clip_Name;
            result.Clip = this.Clip;
            result.ClipPath = this.ClipPath;
            result.Volume = this.Volume;
            return result;
        }
    }

    /// <summary>
    /// 播放概率
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTEventBeHitSoundFX : ACTEventBase
    {
        [ProtoBuf.ProtoMember(11)]
        public List<ACTBeHitSound> BeHitSoundList = new List<ACTBeHitSound>();

        /// <summary>
        /// 播放概率
        /// </summary>
        [ProtoBuf.ProtoMember(12)]
        public float PlayRate;


        public Vector2 scrollPos = Vector2.zero; 

        public ACTEventBeHitSoundFX()
        {
            this.EventFXType = EventType.BeHitSoundFX;
        }

        public override ACTEventBase Clone()
        {
            ACTEventBeHitSoundFX result = new ACTEventBeHitSoundFX();
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            result.EventIndex = this.EventIndex;
            result.PlayRate = this.PlayRate;
            result.scrollPos = this.scrollPos;

            foreach (var sound in BeHitSoundList)
            {
                result.BeHitSoundList.Add(sound.Clone());
            }
            return result;
        }
    }
}
