﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{


    /// <summary>
    /// 视觉特效数据
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTEventFaceAnimationFX : ACTEventBase
    {
        /// <summary>
        /// 动作Action值
        /// </summary>
        [ProtoBuf.ProtoMember(11)]
        public int ActionID;

        public ACTEventFaceAnimationFX()
        {
            this.EventFXType = EventType.FaceAnimationFX;
        }

        public override ACTEventBase Clone() 
        {
            ACTEventFaceAnimationFX result = new ACTEventFaceAnimationFX();
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            result.EventIndex = this.EventIndex;

            result.ActionID = this.ActionID;
            return result;
        }

    }

    
}
