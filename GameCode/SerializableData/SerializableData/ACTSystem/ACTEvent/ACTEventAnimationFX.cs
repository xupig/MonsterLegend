﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{


    /// <summary>
    /// 视觉特效数据
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTEventAnimationFX : ACTEventBase
    {

        /// <summary>
        /// 动作Action值
        /// </summary>
        [ProtoBuf.ProtoMember(11)]
        public int ActionID;

        /// <summary>
        /// 上线初始速度
        /// </summary>
        [ProtoBuf.ProtoMember(12)]
        public float UpVelocity;

        /// <summary>
        /// 滞空时间
        /// </summary>
        [ProtoBuf.ProtoMember(13)]
        public float KeepFloatTime = 0;

        /// <summary>
        /// 推力速度
        /// </summary>
        [ProtoBuf.ProtoMember(14)]
        public float PushForce = 0;

        /// <summary>
        /// 地面减速度
        /// </summary>
        [ProtoBuf.ProtoMember(15)]
        public float SlowRate = 1;

        public ACTEventAnimationFX()
        {
            this.EventFXType = EventType.AnimationFX;
        }

        public override ACTEventBase Clone() 
        {
            ACTEventAnimationFX result = new ACTEventAnimationFX();
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            result.EventIndex = this.EventIndex;

            result.ActionID = this.ActionID;
            result.UpVelocity = this.UpVelocity;
            result.KeepFloatTime = this.KeepFloatTime;
            result.PushForce = this.PushForce;
            result.SlowRate = this.SlowRate;
            return result;
        }

    }

    
}
