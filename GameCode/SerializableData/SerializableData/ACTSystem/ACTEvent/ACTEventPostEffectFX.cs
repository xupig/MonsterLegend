﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    [System.Serializable]
    public enum ACTPostEffectType
    {
        DOF = 1,
        GodRay = 2,
        HdrBloom = 3,
        RadialBlur = 4,
        ScreenWhite = 5,
    }

    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTEventPostEffectFX : ACTEventBase
    {
        [ProtoBuf.ProtoMember(11)]
        public ACTPostEffectType EffectType = ACTPostEffectType.DOF;
        [ProtoBuf.ProtoMember(12)]
        public float[] StartFloatArray;
        [ProtoBuf.ProtoMember(13)]
        public float[] EndFloatArray;
        [ProtoBuf.ProtoMember(14)]
        public float Duration;

        public Vector2 scrollPos = Vector2.zero; 

        public ACTEventPostEffectFX()
        {
            this.EventFXType = EventType.PostEffectFX;
        }

        public override ACTEventBase Clone()
        {
            ACTEventPostEffectFX result = new ACTEventPostEffectFX();
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            result.EventIndex = this.EventIndex;

            result.EffectType = this.EffectType;
            result.StartFloatArray = this.StartFloatArray;
            result.EndFloatArray = this.EndFloatArray;
            result.Duration = this.Duration;
            return result;
        }
    }
}
