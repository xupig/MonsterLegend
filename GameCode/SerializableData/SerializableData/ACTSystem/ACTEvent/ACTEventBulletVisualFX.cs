﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{

    /// <summary>
    /// 子弹定义
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTBullet
    {
        /// <summary>
        /// 飞行特效ID 
        /// </summary>
        [ProtoBuf.ProtoMember(11)]
        public int VisualFXID;

        /// <summary>
        /// 飞行速度
        /// </summary>
        [ProtoBuf.ProtoMember(12)]
        public float Speed = 1f;

        /// <summary>
        /// 坐标修正
        /// </summary>
        [ProtoBuf.ProtoMember(13)]
        public ACTVector3 LocationReal = new ACTVector3();
        public Vector3 Location;

        /// <summary>
        /// 加速度
        /// </summary>
        [ProtoBuf.ProtoMember(14)]
        public float Acceleration = 0;

        public ACTBullet Clone()
        {
            ACTBullet result = new ACTBullet();
            result.VisualFXID = this.VisualFXID;
            result.Speed = this.Speed;
            result.LocationReal = this.LocationReal;
            result.Location = this.Location;
            result.Acceleration = this.Acceleration;
            return result;
        }
    }


    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTEventBulletVisualFX : ACTEventBase
    {
        [ProtoBuf.ProtoMember(11)]
        public List<ACTBullet> BulletList = new List<ACTBullet>();

        /// <summary>
        /// 目标点
        /// </summary>
        [ProtoBuf.ProtoMember(12)]
        public ACTVector3 TargetPositionReal = new ACTVector3();
        public Vector3 TargetPosition;

        [ProtoBuf.ProtoMember(13)]
        public int HitVisualEvent;

        [ProtoBuf.ProtoMember(14)]
        public ACTCalcTargetLocation targetLocation = ACTCalcTargetLocation.Auto;


        public ACTEventBulletVisualFX()
        {
            this.EventFXType = EventType.BulletVisualFX;
        }

        public override ACTEventBase Clone()
        {
            ACTEventBulletVisualFX result = new ACTEventBulletVisualFX();
            result.SkillID = this.SkillID;
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            result.EventIndex = this.EventIndex;
            result.targetLocation = this.targetLocation;
            foreach (var bullet in BulletList)
            {
                result.BulletList.Add(bullet.Clone());
            }
            result.TargetPositionReal = this.TargetPositionReal;
            result.TargetPosition = this.TargetPosition;
            result.HitVisualEvent = this.HitVisualEvent;
            result.targetLocation = this.targetLocation;
            return result;
        }
    }

    
}
