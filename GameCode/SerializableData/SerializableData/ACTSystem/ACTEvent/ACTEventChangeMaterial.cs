﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{

    [System.Serializable]
    public enum ACTMaterialAnimationType
    {
        Float = 0,
        Color = 1,
    }

    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTMaterialAnimation
    {
        /// <summary>
        /// 值名
        /// </summary>
        [ProtoBuf.ProtoMember(12)]
        public string ValueName;

        /// <summary>
        /// 起始值
        /// </summary>
        [ProtoBuf.ProtoMember(13)]
        public float StartValue;

        /// <summary>
        /// 结束值
        /// </summary>
        [ProtoBuf.ProtoMember(14)]
        public float EndValue = 0;

        /// <summary>
        /// 时间
        /// </summary>
        [ProtoBuf.ProtoMember(15)]
        public float Duration = 0.3f;

        /// <summary>
        /// 值類型
        /// </summary>
        [ProtoBuf.ProtoMember(16)]
        public ACTMaterialAnimationType ValueType = ACTMaterialAnimationType.Float;

        /// <summary>
        /// 起始顏色值
        /// </summary>
        [ProtoBuf.ProtoMember(17)]
        public ACTVector4 StartColorReal = new ACTVector4();
        public Color StartColor;
        public Color StartColorRuntime;

        /// <summary>
        /// 结束顏色值
        /// </summary>
        [ProtoBuf.ProtoMember(18)]
        public ACTVector4 EndColorReal = new ACTVector4();
        public Color EndColor;
        public Color EndColorRuntime;

        public ACTMaterialAnimation Clone()
        {
            ACTMaterialAnimation result = new ACTMaterialAnimation();
            result.ValueName = this.ValueName;
            result.StartValue = this.StartValue;
            result.EndValue = this.EndValue;
            result.Duration = this.Duration;

            result.ValueName = this.ValueName;
            result.StartValue = this.StartValue;
            result.EndValue = this.EndValue;
            result.Duration = this.Duration;
            result.ValueType = this.ValueType;
            result.StartColorReal = this.StartColorReal;
            result.StartColor = this.StartColor;
            result.EndColorReal = this.EndColorReal;
            result.EndColor = this.EndColor;
            return result;
        }
    }

    /// <summary>
    /// 视觉特效数据
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    [ProtoBuf.ProtoInclude(301, typeof(ACTEventBeHitChangeMaterial))]
    public class ACTEventChangeMaterial : ACTEventBase
    {
        /// <summary>
        /// Material ID值
        /// </summary>
        [ProtoBuf.ProtoMember(11)]
        public int MaterialID;

        [ProtoBuf.ProtoMember(12)]
        public List<ACTMaterialAnimation> MaterialAnimationList = new List<ACTMaterialAnimation>();

        public Vector2 scrollPos = Vector2.zero; 

        public ACTEventChangeMaterial()
        {
            this.EventFXType = EventType.ChangeMaterial;
        }

        public override ACTEventBase Clone() 
        {
            ACTEventChangeMaterial result = new ACTEventChangeMaterial();
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            result.EventIndex = this.EventIndex;

            result.MaterialID = this.MaterialID;
            foreach (var sub in MaterialAnimationList)
            {
                result.MaterialAnimationList.Add(sub.Clone());
            }
            return result;
        }

    }

    
}
