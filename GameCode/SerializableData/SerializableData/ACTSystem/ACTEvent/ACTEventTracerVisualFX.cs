﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    /// <summary>
    /// 追踪器定义
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTTracer
    {
        /// <summary>
        /// 飞行特效ID 
        /// </summary>
        [ProtoBuf.ProtoMember(11)]
        public int VisualFXID;

        /// <summary>
        /// 飞行速度
        /// </summary>
        [ProtoBuf.ProtoMember(12)]
        public float Speed = 1f;

        /// <summary>
        /// 坐标修正
        /// </summary>
        [ProtoBuf.ProtoMember(13)]
        public ACTVector3 LocationReal = new ACTVector3();
        public Vector3 Location;

        public ACTTracer Clone()
        {
            ACTTracer result = new ACTTracer();
            result.VisualFXID = this.VisualFXID;
            result.Speed = this.Speed;
            result.LocationReal = this.LocationReal;
            result.Location = this.Location;
            return result;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTEventTracerVisualFX : ACTEventBase
    {
        [ProtoBuf.ProtoMember(11)]
        public List<ACTTracer> TracerList = new List<ACTTracer>();

        public ACTEventTracerVisualFX()
        {
            this.EventFXType = EventType.TracerVisualFX;
        }

        public override ACTEventBase Clone()
        {
            ACTEventTracerVisualFX result = new ACTEventTracerVisualFX();
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            result.EventIndex = this.EventIndex;

            foreach (var tracer in TracerList)
            {
                result.TracerList.Add(tracer.Clone());
            }
            return result;
        }
    }

    
}
