﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{

    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTHitSound
    {
        /// <summary>
        /// 播放概率
        /// </summary>
        [ProtoBuf.ProtoMember(11)]
        public float PlayRate;

        /// <summary>
        /// 声音文件
        /// </summary>
        [ProtoBuf.ProtoMember(12)]
        public string Clip_Name;
        public AudioClip Clip;

        /// <summary>
        /// 声音文件路径
        /// </summary>
        [ProtoBuf.ProtoMember(13)]
        public string ClipPath;

        /// <summary>
        /// 音量
        /// </summary>
        [ProtoBuf.ProtoMember(14)]
        public float Volume = 1;

        public ACTHitSound Clone()
        {
            ACTHitSound result = new ACTHitSound();
            result.PlayRate = this.PlayRate;
            result.Clip_Name = this.Clip_Name;
            result.Clip = this.Clip;
            result.ClipPath = this.ClipPath;
            result.Volume = this.Volume;
            return result;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTEventSoundFX : ACTEventBase
    {

        [ProtoBuf.ProtoMember(11)]
        public List<ACTHitSound> HitSoundList = new List<ACTHitSound>();

        /// <summary>
        /// 播放概率
        /// </summary>
        [ProtoBuf.ProtoMember(12)]
        public float PlayRate;

        /// <summary>
        /// 定位类型
        /// </summary>
        [ProtoBuf.ProtoMember(13)]
        public ACTSoundType actsoundType;

        //[ProtoBuf.ProtoMember(14)]
        public Vector2 scrollPos = Vector2.zero; 

        public ACTEventSoundFX()
        {
            this.EventFXType = EventType.SoundFX;
        }

        public override ACTEventBase Clone()
        {
            ACTEventSoundFX result = new ACTEventSoundFX();
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            result.EventIndex = this.EventIndex;
            result.PlayRate = this.PlayRate;
            result.actsoundType = this.actsoundType;
            result.scrollPos = this.scrollPos;

            foreach (var sound in HitSoundList)
            {
                result.HitSoundList.Add(sound.Clone());
            }
            return result;
        }
    }
}
