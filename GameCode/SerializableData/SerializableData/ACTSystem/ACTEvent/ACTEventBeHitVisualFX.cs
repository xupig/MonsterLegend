﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{

    /// <summary>
    /// 视觉特效数据
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTEventBeHitVisualFX : ACTEventVisualFX
    {

        public ACTEventBeHitVisualFX()
        {
            this.EventFXType = EventType.BeHitVisualFX;
        }

        public override ACTEventBase Clone()
        {
            ACTEventBeHitVisualFX result = new ACTEventBeHitVisualFX();
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            result.EventIndex = this.EventIndex;

            result.Perfabs_H_Name = this.Perfabs_H_Name;
            result.Perfabs_H = this.Perfabs_H;
            result.Perfabs_M_Name = this.Perfabs_M_Name;
            result.Perfabs_M = this.Perfabs_M;
            result.Perfabs_L_Name = this.Perfabs_L_Name;
            result.Perfabs_L = this.Perfabs_L;
            result.Duration = this.Duration;
            result.IsLoop = this.IsLoop;
            result.LocationType = this.LocationType;
            result.LocationReal = this.LocationReal;
            result.Location = this.Location;
            result.Slot = this.Slot;
            result.SkinSlot = this.SkinSlot;
            result.IsReplace = this.IsReplace;
            result.RocationReal = this.RocationReal;
            result.Rocation = this.Rocation;
            result.DisappearDelay = this.DisappearDelay;
            return result;
        }
    }

    
}
