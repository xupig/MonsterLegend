﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{


    /// <summary>
    /// 视觉特效数据
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTEventGhostShadow : ACTEventBase
    {
        /// <summary>
        /// 残影特效持续时间  
        /// </summary>
        [ProtoBuf.ProtoMember(11)]
        public float Duration; 

        /// <summary>
        /// 间隔 
        /// </summary>
        [ProtoBuf.ProtoMember(12)]
        public float Interval; 

        /// <summary>
        /// 淡出时间 
        /// </summary>
        [ProtoBuf.ProtoMember(13)]
        public float FadeTime;  

        /// <summary>
        /// 最小距离
        /// </summary>
        [ProtoBuf.ProtoMember(14)]
        public float MinDistance;

        /// <summary>
        /// 颜色
        /// </summary>
        [ProtoBuf.ProtoMember(15)]
        public ACTVector4 XrayColorReal = new ACTVector4();
        public Color XrayColor = new Color(1, 1, 1, 1);

        /// <summary>
        /// 强度
        /// </summary>
        [ProtoBuf.ProtoMember(16)]
        public float Rim = 1.2f;

        /// <summary>
        /// 宽度
        /// </summary>
        [ProtoBuf.ProtoMember(17)]
        public float Inside = 0f;

        public ACTEventGhostShadow()
        {
            this.EventFXType = EventType.GhostShadow;
        }

        public override ACTEventBase Clone() 
        {
            ACTEventGhostShadow result = new ACTEventGhostShadow();
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            result.EventIndex = this.EventIndex;

            result.Duration = this.Duration;
            result.Interval = this.Interval;
            result.FadeTime = this.FadeTime;
            result.MinDistance = this.MinDistance;
            result.XrayColorReal = this.XrayColorReal;
            result.XrayColor = this.XrayColor;
            result.Rim = this.Rim;
            result.Inside = this.Inside;
            return result;
        }

    }

    
}
