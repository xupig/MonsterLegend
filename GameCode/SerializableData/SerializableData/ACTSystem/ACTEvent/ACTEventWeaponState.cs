﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{

    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTWeaponState
    {
        [ProtoBuf.ProtoMember(1)]
        public int WeaponID = 0;
        [ProtoBuf.ProtoMember(2)]
        public bool IsTrigger = false;
        [ProtoBuf.ProtoMember(3)]
        public bool TrailEmit = false;

        public ACTWeaponState Clone()
        {
            ACTWeaponState result = new ACTWeaponState();
            result.WeaponID = this.WeaponID;
            result.IsTrigger = this.IsTrigger;
            result.TrailEmit = this.TrailEmit;
            return result;
        }
    }

    /// <summary>
    /// 视觉特效数据
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTEventWeaponState : ACTEventBase
    {
        [ProtoBuf.ProtoMember(11)]
        public List<ACTWeaponState> WeaponStateList = new List<ACTWeaponState>();

        public ACTEventWeaponState()
        {
            this.EventFXType = EventType.WeaponState;
        }

        public override ACTEventBase Clone()
        {
            ACTEventWeaponState result = new ACTEventWeaponState();
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            result.EventIndex = this.EventIndex;

            foreach (var state in WeaponStateList)
            {
                result.WeaponStateList.Add(state.Clone());
            }
            return result;
        }
    }

    
}
