﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{

    [System.Serializable]
    public enum EventType
    {
        AnimationFX,
        VisualFX,
        SoundFX,
        BeHitVisualFX,
        StateChange,
        BeHitSetting,
        BeHitSoundFX,
        TargetDetector,
        WeaponState,
        TracerVisualFX,
        BulletVisualFX,
        LaserVisualFX,
        PostEffectFX,
        TriggerVisualFX,
        ChangeMaterial,
        CameraAnimation,
        FaceAnimationFX,
        GhostShadow,
        BeHitChangeMaterial,
    }

    /// <summary>
    /// 
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    [ProtoBuf.ProtoInclude(102, typeof(ACTEventAnimationFX))]
    [ProtoBuf.ProtoInclude(103, typeof(ACTEventBeHitSetting))]
    [ProtoBuf.ProtoInclude(104, typeof(ACTEventBeHitSoundFX))]
    [ProtoBuf.ProtoInclude(106, typeof(ACTEventSoundFX))]
    [ProtoBuf.ProtoInclude(107, typeof(ACTEventStateChange))]
    [ProtoBuf.ProtoInclude(108, typeof(ACTEventTargetDetector))]
    [ProtoBuf.ProtoInclude(109, typeof(ACTEventVisualFX))]
    [ProtoBuf.ProtoInclude(110, typeof(ACTEventWeaponState))]
    [ProtoBuf.ProtoInclude(111, typeof(ACTEventTracerVisualFX))]
    [ProtoBuf.ProtoInclude(112, typeof(ACTEventBulletVisualFX))]
    [ProtoBuf.ProtoInclude(113, typeof(ACTEventLaserVisualFX))]
    [ProtoBuf.ProtoInclude(114, typeof(ACTEventChangeMaterial))]
    [ProtoBuf.ProtoInclude(115, typeof(ACTEventGhostShadow))]
    public class ACTEventBase 
    {
        [ProtoBuf.ProtoIgnore]
        public int SkillID = 0;

        [ProtoBuf.ProtoMember(1)]
        public bool IsUsed = true;

        [ProtoBuf.ProtoMember(2)]
        public EventType EventFXType;

        [ProtoBuf.ProtoMember(3)]
        public float Delay = 0;

        [ProtoBuf.ProtoMember(4)]
        public int EventIndex;


        public virtual ACTEventBase Clone() { return new ACTEventBase(); }

    }
}
