﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    /// <summary>
    /// 
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTEventBeHitSetting : ACTEventBase
    {
        [ProtoBuf.ProtoMember(11)]
        public float PushForce = 0;
        [ProtoBuf.ProtoMember(12)]
        public float RaiseForce = 0;
        [ProtoBuf.ProtoMember(13)]
        public float KeepFloatTime = 0;
        [ProtoBuf.ProtoMember(14)]
        public int BeHitAction = 0;
        [ProtoBuf.ProtoMember(15)]
        public int BeHitAirAction = 0;
        [ProtoBuf.ProtoMember(16)]
        public float HitTimeScale = 0.0f;
        [ProtoBuf.ProtoMember(17)]
        public float HitTimeScaleKeep = 0.0f;
        [ProtoBuf.ProtoMember(18)]
        public float SlowRate = 1;
        [ProtoBuf.ProtoMember(19)]
        public float AirRaiseForce = 0;
        [ProtoBuf.ProtoMember(20)]
        public int HitVisualEvent = 0;

        public ACTEventBeHitSetting()
        {
            this.EventFXType = EventType.BeHitSetting;
        }

        public override ACTEventBase Clone()
        {
            ACTEventBeHitSetting result = new ACTEventBeHitSetting();
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            result.EventIndex = this.EventIndex;

            result.PushForce = this.PushForce;
            result.RaiseForce = this.RaiseForce;
            result.KeepFloatTime = this.KeepFloatTime;
            result.BeHitAction = this.BeHitAction;
            result.BeHitAirAction = this.BeHitAirAction;
            result.HitTimeScale = this.HitTimeScale;
            result.HitTimeScaleKeep = this.HitTimeScaleKeep;
            result.SlowRate = this.SlowRate;
            result.AirRaiseForce = this.AirRaiseForce;
            result.HitVisualEvent = this.HitVisualEvent;
            return result;
        }
    }
}
