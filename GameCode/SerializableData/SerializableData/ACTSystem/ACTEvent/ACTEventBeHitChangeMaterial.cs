﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    /// <summary>
    /// 视觉特效数据
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTEventBeHitChangeMaterial : ACTEventChangeMaterial
    {
        /// <summary>
        /// 延时触发时间
        /// </summary>
        [ProtoBuf.ProtoMember(21)]
        public float DelayTriggerTime = 0f;

        public ACTEventBeHitChangeMaterial()
        {
            this.EventFXType = EventType.BeHitChangeMaterial;
        }

        public override ACTEventBase Clone() 
        {
            ACTEventBeHitChangeMaterial result = new ACTEventBeHitChangeMaterial();
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            result.EventIndex = this.EventIndex;

            result.MaterialID = this.MaterialID;
            result.DelayTriggerTime = this.DelayTriggerTime;
            foreach (var sub in MaterialAnimationList)
            {
                result.MaterialAnimationList.Add(sub.Clone());
            }
            return result;
        }

    }

    
}

