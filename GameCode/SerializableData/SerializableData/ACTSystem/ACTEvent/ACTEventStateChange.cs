﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    /// <summary>
    /// 
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTEventStateChange : ACTEventBase
    {

        //public bool HitByWeaponCollider = false;

        //public bool WeaponIsTrigger = false;
        [ProtoBuf.ProtoMember(11)]
        public bool BeHitFXLocatByCollision = false;
        [ProtoBuf.ProtoMember(12)]
        public bool ApplyRootMotion = false;
        [ProtoBuf.ProtoMember(13)]
        public bool AttackLockTarget = false;
        [ProtoBuf.ProtoMember(14)]
        public float SpeedMlp = 1;
        [ProtoBuf.ProtoMember(15)]
        public bool CanTurn = true;
        [ProtoBuf.ProtoMember(16)]
        public bool CanMove = false;
        [ProtoBuf.ProtoMember(17)]
        public bool IsReady = false;
        [ProtoBuf.ProtoMember(18)]
        public bool LeftWeaponOn = false;
        [ProtoBuf.ProtoMember(19)]
        public bool RightWeaponOn = false;
        //public bool MeleeWeaponTrailEmit = false;
        [ProtoBuf.ProtoMember(20)]
        public string SkinnedSlot;
        [ProtoBuf.ProtoMember(21)]
        public bool ShowSkinned = false;

        public ACTEventStateChange()
        {
            this.EventFXType = EventType.StateChange;
        }

        public override ACTEventBase Clone()
        {
            ACTEventStateChange result = new ACTEventStateChange();
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            result.EventIndex = this.EventIndex;

            result.BeHitFXLocatByCollision = this.BeHitFXLocatByCollision;
            result.ApplyRootMotion = this.ApplyRootMotion;
            result.AttackLockTarget = this.AttackLockTarget;
            result.SpeedMlp = this.SpeedMlp;
            result.CanTurn = this.CanTurn;
            result.CanMove = this.CanMove;
            result.SkinnedSlot = this.SkinnedSlot;
            result.ShowSkinned = this.ShowSkinned;
            return result;
        }
    }
}
