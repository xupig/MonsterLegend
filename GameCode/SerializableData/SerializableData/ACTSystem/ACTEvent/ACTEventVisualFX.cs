﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    /// <summary>
    /// 视觉特效数据
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    [ProtoBuf.ProtoInclude(201, typeof(ACTEventBeHitVisualFX))]
    [ProtoBuf.ProtoInclude(202, typeof(ACTEventTriggerVisualFX))]
    public class ACTEventVisualFX : ACTEventBase
    {
        /// <summary>
        /// 资源列表
        /// </summary>
        [ProtoBuf.ProtoMember(11)]
        public string Perfabs_H_Name;
        public UnityEngine.Object Perfabs_H;

        [ProtoBuf.ProtoMember(12)]
        public string Perfabs_M_Name; 
        public UnityEngine.Object Perfabs_M;

        [ProtoBuf.ProtoMember(13)]
        public string Perfabs_L_Name;
        public UnityEngine.Object Perfabs_L;
        

        /// <summary>
        /// 持续时间
        /// </summary>
        [ProtoBuf.ProtoMember(16)]
        public float Duration = 1;

        /// <summary>
        /// 是否循环
        /// </summary>
        [ProtoBuf.ProtoMember(17)]
        public bool IsLoop;

        /// <summary>
        /// 定位类型
        /// </summary>
        [ProtoBuf.ProtoMember(18)]
        public ACTVFXLocationType LocationType;

        /// <summary>
        /// 定位坐标
        /// </summary>
        [ProtoBuf.ProtoMember(19)]
        public ACTVector3 LocationReal = new ACTVector3();
        public Vector3 Location;

        /// <summary>
        /// 定位坐标
        /// </summary>
        [ProtoBuf.ProtoMember(20)]
        public string Slot;

        /// <summary>
        /// 旋转
        /// </summary>
        [ProtoBuf.ProtoMember(21)]
        public ACTVector3 RocationReal = new ACTVector3();
        public Vector3 Rocation;

        [ProtoBuf.ProtoMember(22)]
        public string SkinSlot="body";

        [ProtoBuf.ProtoMember(23)]
        public float DisappearDelay = 0;

        /// <summary>
        /// 是否顶替
        /// </summary>
        public ACTVFXConflictType IsReplace;


        public ACTEventVisualFX()
        {
            this.EventFXType = EventType.VisualFX;
        }

        public override ACTEventBase Clone()
        {
            ACTEventVisualFX result = new ACTEventVisualFX();
            result.IsUsed = this.IsUsed;
            result.EventFXType = this.EventFXType;
            result.Delay = this.Delay;
            result.EventIndex = this.EventIndex;

            result.Perfabs_H_Name = this.Perfabs_H_Name;
            result.Perfabs_H = this.Perfabs_H;
            result.Perfabs_M_Name = this.Perfabs_M_Name;
            result.Perfabs_M = this.Perfabs_M;
            result.Perfabs_L_Name = this.Perfabs_L_Name;
            result.Perfabs_L = this.Perfabs_L;
            result.Duration = this.Duration;
            result.IsLoop = this.IsLoop;
            result.LocationType = this.LocationType;
            result.LocationReal = this.LocationReal;
            result.Location = this.Location;
            result.Slot = this.Slot;
            result.IsReplace = this.IsReplace;
            result.RocationReal = this.RocationReal;
            result.Rocation = this.Rocation;
            result.SkinSlot = this.SkinSlot;
            result.DisappearDelay = this.DisappearDelay;
            return result;
        }
    }

    
}
