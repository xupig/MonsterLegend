﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;

namespace ACTSystem
{
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTBeHitBox
    {
        /// <summary>
        /// 定位坐标
        /// </summary>
        [ProtoBuf.ProtoMember(1)]
        public string Slot;

        /// <summary>
        /// 定位box大小
        /// </summary>
        [ProtoBuf.ProtoMember(19)]
        public ACTVector3 BoxSizeReal = new ACTVector3();
        public Vector3 BoxSize;

        public ACTBeHitBox Clone()
        {
            ACTBeHitBox result = new ACTBeHitBox();
            result.BoxSizeReal = this.BoxSizeReal;
            result.BoxSize = this.BoxSize;
            result.Slot = this.Slot;
            return result;
        }
    }

    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTActorMaterial
    {
        [ProtoBuf.ProtoMember(1)]
        public string MaterialPath;
        public UnityEngine.Object Material;

        [ProtoBuf.ProtoMember(2)]
        public string meshSlot;

        [ProtoBuf.ProtoMember(3)]
        public string meshPath;
        public UnityEngine.Object Mesh;

        public ACTActorMaterial Clone()
        {
            ACTActorMaterial result = new ACTActorMaterial();
            result.MaterialPath = this.MaterialPath;
            result.meshSlot = this.meshSlot;
            result.meshPath = this.meshPath;
            return result;
        }
    }

    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTActorMaterials
    {
        [ProtoBuf.ProtoMember(1)]
        public int ID;

        [ProtoBuf.ProtoMember(2)]
        public List<ACTActorMaterial> MaterialList = new List<ACTActorMaterial>();

        public ACTActorMaterials Clone()
        {
            ACTActorMaterials result = new ACTActorMaterials();
            result.ID = this.ID;
            foreach (var sub in MaterialList)
            {
                result.MaterialList.Add(sub.Clone());
            }
            return result;
        }
    }

    /// <summary>
    /// actor数据
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTDataActor
    {
        [ProtoBuf.ProtoMember(1)]
        public int ID;
        [ProtoBuf.ProtoMember(2)]
        public string ModelName;
        public UnityEngine.Object Model;
        [ProtoBuf.ProtoMember(3)]
        public string ModelPath;
        [ProtoBuf.ProtoMember(4)]
        public float DePushForce = 0;
        [ProtoBuf.ProtoMember(5)]
        public float DeRaiseForce = 0;
        [ProtoBuf.ProtoMember(6)]
        public float WalkSpeed = 2;
        [ProtoBuf.ProtoMember(7)]
        public float RunSpeed = 5;
        [ProtoBuf.ProtoMember(8)]
        public int BornEventsID = 0;
        [ProtoBuf.ProtoMember(9)]
        public int DieEventsID = 0;
        [ProtoBuf.ProtoMember(10)]
        public float ModelDisposeTime = 1;
        [ProtoBuf.ProtoMember(11)]
        public float ModelScale = 1;
        [ProtoBuf.ProtoMember(12)]
        public float CapsuleRadius = 0.25f;
        [ProtoBuf.ProtoMember(13)]
        public float BeHitRadius = 0.25f;
        [ProtoBuf.ProtoMember(14)]
        public int TypeID = 9999;
        [ProtoBuf.ProtoMember(15)]
        public int PlayRate = 100;
        [ProtoBuf.ProtoMember(16)]
        public ACTPlaySoundType PlaySoundType = 0;
        [ProtoBuf.ProtoMember(17)]
        public ACTBeHitSound bornSound;
        [ProtoBuf.ProtoMember(18)]
        public ACTBeHitSound deadSound;
        [ProtoBuf.ProtoMember(19)]
        public int FootPlayRate = 100;

        [ProtoBuf.ProtoMember(20)]
        public List<ACTBeHitBox> BeHitBoxList = new List<ACTBeHitBox>();
        [ProtoBuf.ProtoMember(21)]
        public List<ACTBeHitSound> BeHitSoundList = new List<ACTBeHitSound>();
        [ProtoBuf.ProtoMember(22)]
        public List<ACTBeHitSound> FootSoundList = new List<ACTBeHitSound>();
        [ProtoBuf.ProtoMember(23)]
        public List<string> FootTransList = new List<string>();
        [ProtoBuf.ProtoMember(24)]
        public List<ACTActorMaterials> MaterialsList = new List<ACTActorMaterials>();
        [ProtoBuf.ProtoMember(25)]
        public List<int> FootFxList = new List<int>();

        [ProtoBuf.ProtoMember(26)]
        public int RiderShape = 0;

        [ProtoBuf.ProtoMember(27)]
        public float[] RideScale;

        [ProtoBuf.ProtoMember(28)]
        public int RideType = 0;

        public ACTDataActor Clone()
        {
            ACTDataActor result = new ACTDataActor();
            result.ID = this.ID;
            result.ModelName = this.ModelName;
            result.Model = this.Model;
            result.ModelPath = this.ModelPath;
            result.DePushForce = this.DePushForce;
            result.DeRaiseForce = this.DeRaiseForce;
            result.WalkSpeed = this.WalkSpeed;
            result.RunSpeed = this.RunSpeed;
            result.TypeID = this.TypeID;
            result.PlayRate = this.PlayRate;
            result.PlaySoundType = this.PlaySoundType;
            result.bornSound = this.bornSound;
            result.deadSound = this.deadSound;
            result.FootPlayRate = this.FootPlayRate;

            foreach (var sub in BeHitBoxList)
            {
                result.BeHitBoxList.Add(sub.Clone());
            }
            foreach (var sub in BeHitSoundList)
            {
                result.BeHitSoundList.Add(sub.Clone());
            }
            foreach (var sub in FootSoundList)
            {
                result.FootSoundList.Add(sub.Clone());
            }
            foreach (var sub in FootTransList)
            {
                result.FootTransList.Add(sub);
            }
            foreach (var sub in MaterialsList)
            {
                result.MaterialsList.Add(sub.Clone());
            }
            foreach (var sub in FootFxList)
            {
                result.FootFxList.Add(sub);
            }

            result.RiderShape = RiderShape;
            result.RideType = RideType;
            if (RideScale != null)
            {
                result.RideScale = new float[RideScale.Length];
                for (int i = 0; i < RideScale.Length; i++)
                {
                    result.RideScale[i] = RideScale[i];
                }
            }

            return result;
        }

        public string GetModelAssetPath()
        {
            if (string.IsNullOrEmpty(ModelPath))
            {
                Debug.LogError("预加载资源未打包 ModelPath actorID = " + ID);
            }
            return ModelPath.Substring("Assets/Resources/".Length);
        }
    }
}
