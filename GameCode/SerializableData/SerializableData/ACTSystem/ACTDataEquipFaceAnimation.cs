﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACTSystem
{
    /// <summary>
    /// 面部动画数据
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTDataEquipFaceAnimation
    {
        [ProtoBuf.ProtoMember(1)]
        public int TickCount;
        [ProtoBuf.ProtoMember(2)]
        public string MaterialPath;
        public UnityEngine.Object Material;
        [ProtoBuf.ProtoMember(3)]
        public string AudioPath;
        public UnityEngine.Object Auido;
        public ACTDataEquipFaceAnimation Clone()
        {
            ACTDataEquipFaceAnimation result = new ACTDataEquipFaceAnimation();
            result.TickCount = this.TickCount;
            result.MaterialPath = this.MaterialPath;
            result.Material = this.Material;
            result.AudioPath = this.AudioPath;
            result.Auido = this.Auido;
            return result;
        }
    }
}
