﻿
namespace ACTSystem
{
    /// <summary>
    /// 装备粒子对象
    /// </summary>
    [System.Serializable]
    [ProtoBuf.ProtoContract]
    public class ACTDataParticlePrefab
    {
        [ProtoBuf.ProtoMember(1)]
        public string Slot;
        [ProtoBuf.ProtoMember(2)]
        public string PrefabPath;
        public UnityEngine.Object Prefab;

        public ACTDataParticlePrefab Clone()
        {
            ACTDataParticlePrefab result = new ACTDataParticlePrefab();
            result.Slot = this.Slot;
            result.PrefabPath = this.PrefabPath;
            result.Prefab = this.Prefab;
            return result;
        }
    }
}
