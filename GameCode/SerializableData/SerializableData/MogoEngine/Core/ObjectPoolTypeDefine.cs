﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProtoBuf;

namespace MogoEngine.Core
{
    public enum FatObjectType
    { 
        UIPrefab=1,
        CharacterPrefab, 
        SceneObjectPrefab,
        MaterialAsset,
        AnimatorAsset,
        RuntimeAnimationControllerAsset
    }

    [ProtoContract]
    public class AssetGroup
    {
        [ProtoMember(1)]
        public Type AssetType;
        [ProtoMember(2)]
        public string [] Objects;
    }

    [ProtoContract]
    public class FatObject
    {
        [ProtoMember(1)]
        public FatObjectType ObjectType;
        [ProtoMember(2)]
        public AssetGroup[] Dependences;
    }
	//an csharp class that specify all resources
    [ProtoContract]
    public class ResourcesInfo
    {
		//all resource list here,asset to asset file relative path map
        [ProtoMember(1)]
        public Dictionary<string, string> AssetName2Path;
		//if an asset name apears in this dictionary,then the asset is a fat object,it depends on some other assets
		//otherwise the asset depend on itself only
        [ProtoMember(2)]
        public Dictionary<string, FatObject> FatObjects;
		//this is an optional dic,if the dic is not null,then the key is asset name,and the value is all it's dependences
		//with this dic,you will never need to find one asset's dependeces dynamicly
        [ProtoMember(3)]
        public Dictionary<string, string[]> FatObjectPrebuiltDependences;
		//if a texture asset's name apears here,then it's a ui texture
		//the ui texture serialized data contain's the detail information for creating a ui sprite dynamicly at runtime
		[ProtoMember(4)]
		public Dictionary<string,MogoTextureInfo> MogoTextureInfos;
    }
	
	[ProtoContract]
	public class MogoTextureInfo
	{
		[ProtoMember(1)]
		public string name;
		[ProtoMember(2)]
		public uint extrude;
		[ProtoMember(3)]
		public int meshType;
		[ProtoMember(4)]
		public float pixelsToUnits;
		[ProtoMember(5)]
		public Dictionary<string,MogoSpriteInfo> sprites;
	}
	
	[ProtoContract]
	public class MogoUIRect
	{
		[ProtoMember(1)]
		public float x;
		[ProtoMember(2)]
		public float y;
		[ProtoMember(3)]
		public float height;
		[ProtoMember(4)]
		public float width;
	}

	[ProtoContract]
	public class MogoUIBorder
	{
		[ProtoMember(1)]
		public float x;
		[ProtoMember(2)]
		public float y;
		[ProtoMember(3)]
		public float z;
		[ProtoMember(4)]
		public float w;
	}

	[ProtoContract]
	public class MogoUIPivot
	{
		[ProtoMember(1)]
		public float x;
		[ProtoMember(2)]
		public float y;
	}

	[ProtoContract]
	public class MogoSpriteInfo
	{
		[ProtoMember(1)]
		public MogoUIRect rect;
		[ProtoMember(2)]
		public MogoUIBorder border;
		[ProtoMember(3)]
		public MogoUIPivot pivot;
		[ProtoMember(4)]
		public string name;
	}
}
