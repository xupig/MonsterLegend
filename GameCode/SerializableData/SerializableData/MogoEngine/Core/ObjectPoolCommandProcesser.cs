﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MogoEngine.Core
{
    //Object Pool Command --- > 0x00000000
    public class ObjectPoolCommandProcesser
    {
        byte m_Command;
        public ObjectPoolCommandProcesser(string cmd)
        {

        }

        [Tag("?")]
        public bool help
        {
            set
            {
                m_Command |= 0x1;
            }
        }

        [Tag("report")]
        public bool report
        {
            set
            {
                m_Command |= 0x2;
            }
        }
        [Tag("report_res")]
        public string report_res
        {
            set
            {
                m_Command |= 0x4;
            }
        }

        public void Process()
        {

        }
    }

    [AttributeUsage(AttributeTargets.Property,Inherited=false,AllowMultiple=true)]
    public class TagAttribute:Attribute
    {
        public TagAttribute(string tag) { this.m_tag = tag; }
        string m_tag;
        public string Tag
        {
            get
            {
                return m_tag;
            }
            set
            {
                m_tag = value;
            }
        }
    }
}
