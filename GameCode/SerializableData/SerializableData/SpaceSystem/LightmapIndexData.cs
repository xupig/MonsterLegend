﻿
public class LightmapIndexData
{
    public int id { get; set; }
    public int Index { get; set; }
    public string Intensity { get; set; }
    public string Directionality { get; set; }
}