﻿
public class SceneData
{
    public int Id { get; set; }
    public string Name { get; set; }
    //public float Width { get; set; }
    //public float Height { get; set; }
    /// <summary>
    /// [y,x]: y: Height; x: Width
    /// </summary>
    public ZoneObjData[,] Cells;
}
