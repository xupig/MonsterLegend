﻿
public class ZoneData
{
    public int id { get; set; }
    public int X { get; set; }
    public int Y { get; set; }
    public float Width { get; set; }
    public float Height { get; set; }
    public string PrefabName { get; set; }
}