﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ZoneObjData
{
    public int X { get; set; }
    public int Y { get; set; }
    public string PrefabName { get; set; }
    public string SceneName { get; set; }
    public bool Loading { get; set; }
    public bool Loaded { get; set; }
    public GameObject Prefab { get; set; }
    public List<LightmapAssetData> LightmapAssetDatas { get; set; }
    public Scene Scene { get; set; }
    public Terrain Terrain { get; set; }
}
