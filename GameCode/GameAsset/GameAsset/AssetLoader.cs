﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using LitJson;
using Object = UnityEngine.Object;
using GameCrypto.Utils;
using System.IO;
#if !UNITY_WEBPLAYER
using System.IO;
#endif

namespace Game.Asset
{
    /// <summary>
    /// 重要概念
    /// Path是资源的逻辑路径，是AssetRecord中DependceDict中的Key值，包含"/"， 例："Scenes/40001/40001_LightmapFar-0.exr"
    /// PhysicalPath是资源的物理路径，包含"$"，例："Scenes$40001$40001_LightmapFar-0.exr.u"
    /// </summary>
    internal class AssetLoader : MonoBehaviour
    {
        private const string FILE_HEAD = "file://";
        public const int COCURRENT_LOADING_COUNT = 2;
        public const int MIN_DELETE_ASSET_INTERVAL = 120;
        public const string POSTFIX_FOLDER = ".folder.u";
        public const string POSTFIX_SELECTION = ".selection.u";
        public const string SHADER_PHYSICAL_PATH = "Shader.folder.u";
        public static string FONT_YAHEI_UNICODE_PATH = "Font${0}$MicrosoftYaHei_unicode.ttf.u";
        public static string FONT_YAHEI_PATH = "Font${0}$MicrosoftYaHei.ttf.u";
        public const string TOKEN_DOLLAR = "$";
        public const string TOKEN_DOT = ".";
        //Material和AnimatorController打包时生成的json文件名中含有_shadow关键字
        public const string SHADOW = "_shadow";
        public const string TYPE = "type";
        public const string TYPE_MATERIAL = "Material";
        public const string TYPE_CONTROLLER = "AnimatorController";
        public static Regex POSTFIX_FOLDER_PATTERN = new Regex(@"(\.\w+)?\.folder.u", RegexOptions.IgnoreCase);

        public static int DELETE_ASSET_INTERVAL = MIN_DELETE_ASSET_INTERVAL;
        public static AssetLoader Instance { get; private set; }
        public static string RemoteURL = string.Empty;
        private ObjectPool _objectPool;
        private float _lastDeleteAssetTime;
        //待加载的资源队列
        private Queue<string> _assetPhysicalPathQueue;
        //仅判断存在性时性能更优
        private HashSet<string> _assetPhysicalPathSet;
        //正在加载中的资源Set
        private HashSet<string> _loadingAssetSet;
        //Key为资源的物理路径， Value为资源被加载次数
        private Dictionary<string, int> _assetLoadedCountSet;
        public Dictionary<string, int> AssetLoadedCountSet { get { return _assetLoadedCountSet; } }

        //已加载过资源路径Set
        private HashSet<string> _loadedAssetSet;
        private Dictionary<string, Object> _activeObjectDict;       //Key为资源的物理路径， Value为资源加载后的Object
        private Dictionary<string, List<string>> _activeAssetDict;  //key为资源物理路径，Value为资源_objectDict中key集合
        private Dictionary<string, WWW> _presistWwwDict;            //key为资源的物理路径，value为资源加载后不立即释放的资源的WWW
        private Dictionary<string, bool> _parseMarkDict;            //key为资源的物理路径，value为是否在www加载完成后进行加载asset资源
        private HashSet<string> externalExistsFiles = new HashSet<string>(); //記錄已經存在于本地的遠程文件記錄

        private Dictionary<string, JsonData> _shadowCacheDict;       //Key为shadow资源的物理路径， Value为对应的JsonData

        //Shader特殊处理，只有需要使用其中的Shader的时候，才会从中加载，Shader资源包的AssetBundle不会Unload
        private AssetBundle _shaderAssetBundle;
        
        //正在加载资源计数
        private int _loadingCount = 0;
        //判断loader是否正在资源加在
        public bool isBusy { get { return _loadingCount > 0; } } 

        //加载过的资源数量
        private int _loadAssetCount = 0;
        public int LoadAssetCount { get { return _loadAssetCount; } }

        private List<AssetLoadedCallbackWrapper> _assetLoadedCallbackList;
        private List<string> _unusedAssetPathList;

        static AssetLoader()
        {
            GameObject proxy = new GameObject("_ObjectPoolProxy");
            GameObject.DontDestroyOnLoad(proxy);
            Instance = proxy.AddComponent<AssetLoader>();
            Instance.Initialize();
            Instance.InitExternalExistsFilesDict();
        }

        private void Initialize()
        {
            _objectPool = ObjectPool.Instance;
            _assetPhysicalPathQueue = new Queue<string>();
            _assetPhysicalPathSet = new HashSet<string>();
            _loadingAssetSet = new HashSet<string>();
            _activeObjectDict = new Dictionary<string, Object>();
            _activeAssetDict = new Dictionary<string, List<string>>();
            _presistWwwDict = new Dictionary<string, WWW>();
            _parseMarkDict = new Dictionary<string, bool>();
            _loadedAssetSet = new HashSet<string>();
            _assetLoadedCountSet = new Dictionary<string, int>();
            _assetLoadedCallbackList = new List<AssetLoadedCallbackWrapper>();
            _unusedAssetPathList = new List<string>();
            _shadowCacheDict = new Dictionary<string, JsonData>();
            for(int i = 0; i < COCURRENT_LOADING_COUNT; i++)
            {
                StartCoroutine(DaemonLoadAsset());
            }
            StartCoroutine(DaemonDeleteAsset());
            StartCoroutine(DaemonHandleAssetLoaded());
        }

        void InitExternalExistsFilesDict()
        {
#if !UNITY_WEBPLAYER
            var uFiles = Directory.GetFiles(ObjectPool.ASSET_ROOT_PATH, "*.u", SearchOption.TopDirectoryOnly);
            for (int i = 0; i < uFiles.Length; i++)
            {
                string fileName = Path.GetFileName(uFiles[i]);
                externalExistsFiles.Add(fileName);
            }
#endif
        }

        private IEnumerator DaemonLoadAsset()
        {
            WWW www = null;
            while(true)
            {
                if (_assetPhysicalPathQueue.Count > 0)
                {
                    string physicalPath = _assetPhysicalPathQueue.Dequeue();
                    _assetPhysicalPathSet.Remove(physicalPath);
                    _loadingAssetSet.Add(physicalPath);
                    _loadingCount++;
                    _loadAssetCount++;

                    string url = GetAssetUrl(physicalPath);
#if UNITY_IPHONE
                    if (url.Contains(" "))
                    {
                        GameLoader.Utils.LoggerHelper.Error(string.Format("Resource path = {0} contains blank space.", url));
                    }
#endif
                    www = GetWWW(url, physicalPath);
                    yield return www;
                    CheckWWWResult(physicalPath, www);
                    www = null;

                    _loadingCount--;
                    AddToAssetLoadedCountSet(physicalPath);
                    _loadingAssetSet.Remove(physicalPath);
                    yield return null;
                }
                else
                {
                    yield return null;
                }
            }
        }

        private static WWW GetWWW(string url, string physicalPath)
        {
#if !UNITY_WEBPLAYER
            return new WWW(url);
#else
            int version = ObjectPool.Instance.GetVersion(physicalPath);
            return WWW.LoadFromCacheOrDownload(url, version);
#endif
        }

        private void CheckWWWResult(string physicalPath, WWW www)
        { 
            if(www.error != null)
            {
                if (_objectPool.IsExternalAsset(physicalPath) && !externalExistsFiles.Contains(physicalPath))
                {
                    //通過遠程加載的文件，如果加載失敗繼續重試
                    _assetPhysicalPathQueue.Enqueue(physicalPath);
                    _assetPhysicalPathSet.Add(physicalPath);
                }
                else
                {
                    //资源加载失败时，添加纪录使上层回调函数执行不受影响
                    _activeAssetDict.Add(physicalPath, new List<string>());
                }
                Debug.LogError(string.Format("Loading error in {0}, the reason is {1}", www.url, www.error));
            }
            else
            {
                SaveToExternal(physicalPath, www);  
                ParseLoadedAssetBundle(physicalPath, www.assetBundle);
                CheckUnloadStrategy(physicalPath, www);
                _loadedAssetSet.Add(physicalPath);
            }
        }

        private string GetAssetUrl(string physicalPath)
        {
#if !UNITY_WEBPLAYER      
            string url = ObjectPool.ASSET_ROOT_PATH + physicalPath;     //ObjectPool.ASSET_ROOT_PATH:在移动端上,此值默认为“外部目录路径”

            bool exists = File.Exists(url);
            if (exists)
            {
                if (_objectPool.IsExternalAsset(physicalPath) && !externalExistsFiles.Contains(physicalPath))
                {
                    externalExistsFiles.Add(physicalPath);
                }
                return string.Concat(FILE_HEAD, url);
            }
            if (_objectPool.IsExternalAsset(physicalPath))
            {
                return string.Concat(RemoteURL, physicalPath);
            }
            if(Application.platform == RuntimePlatform.Android)
            {
                //外部目录存在资源  ：从外部目录加载
                //外部目录不存在资源: 从StreamingAssets加载
                //Debug.Log(string.Format("url:{0},assetRootPath:{1},Exists:{2}", url, ObjectPool.ASSET_ROOT_PATH, File.Exists(url)));
                url = string.Concat(Application.streamingAssetsPath, "/Resources/", physicalPath);
                //旧代码：资源在外部目录不存在,则从StreaingAssetsPath路径加载
                //if (File.Exists(url) == false) url = string.Concat(Application.streamingAssetsPath, "/", physicalPath);
            }
            else if(Application.platform == RuntimePlatform.IPhonePlayer || ObjectPool.isReleaseMode)
            {
                url = string.Concat(FILE_HEAD, Application.streamingAssetsPath, "/Resources/", physicalPath);
            }
            else
            {
                url = string.Concat(FILE_HEAD, url);
            }
            return url;
#else
            int version = ObjectPool.Instance.GetVersion(physicalPath);
            string newPhysicalPath = physicalPath;
            if (version > 0)
            {
                newPhysicalPath = string.Format("{0}_{1}{2}",Path.GetFileNameWithoutExtension(physicalPath), version, Path.GetExtension(physicalPath));

                newPhysicalPath = Path.GetFileNameWithoutExtension(physicalPath) + "_" + version + Path.GetExtension(physicalPath);
            }
            return string.Concat(RemoteURL, newPhysicalPath); 
#endif
        }

        private IEnumerator DaemonDeleteAsset()
        {
            while(true)
            {
                if ((Time.realtimeSinceStartup - _lastDeleteAssetTime) > DELETE_ASSET_INTERVAL)
                {
                    _lastDeleteAssetTime = Time.realtimeSinceStartup;
                    _objectPool.DeleteZeroReferrencedAsset();
                }
                else
                {
                    yield return null;
                }
            }
        }

        private IEnumerator DaemonHandleAssetLoaded()
        {
            while(true)
            {
                for(int i = 0; i < _assetLoadedCallbackList.Count; i++)
                {
                    AssetLoadedCallbackWrapper wrapper = _assetLoadedCallbackList[i];
                    if (IsPhysicalPathListLoaded(wrapper.physicalPathList, wrapper.parseWhenLoaded) == true)
                    {
                        if(wrapper.isInvoked == false && wrapper.callback != null)
                        {
                            try //Ari: 协程一旦遇到异常就会中断，后续操作将无法执行，这里加个Try作为流程完整性的保护
                            {
                                wrapper.callback();
                            }
                            catch (Exception ex)
                            {
                                Debug.LogError(ex);
                            }
                        }
                        wrapper.isInvoked = true;
                    }
                }
                RecycleInvokedCallback();
                yield return null;
            }
        }

        private bool IsPhysicalPathListLoaded(List<string> physicalPathList, bool parseWhenLoaded)
        {
            for(int i = 0; i < physicalPathList.Count; i++)
            {
                string physicalPath = physicalPathList[i];
                if (parseWhenLoaded)
                {
                    if (IsActiveAsset(physicalPath) == false)
                    {
                        return false;
                    }
                }
                else 
                {
                    if (!externalExistsFiles.Contains(physicalPath))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private void RecycleInvokedCallback()
        {
            for(int i = _assetLoadedCallbackList.Count - 1; i >= 0; i--)
            {
                AssetLoadedCallbackWrapper wrapper = _assetLoadedCallbackList[i];
                if(wrapper.isInvoked == true)
                {
                    _assetLoadedCallbackList.RemoveAt(i);
                    AssetLoadedCallbackWrapper.Recycle(wrapper);
                }
            }
        }

        /// <summary>
        /// 根据物理路径从常驻内存取出资源
        /// </summary>
        /// <param name="physicalPath">物理路径,例如:Scenes$1005_Race$1005_Race.prefab.u</param>
        /// <returns></returns>
        public byte[] GetPresistAssetData(string physicalPath)
        {
            return _presistWwwDict.ContainsKey(physicalPath) ? _presistWwwDict[physicalPath].bytes : null;
        }

        public void LoadAssetList(List<string> physicalPathList, Action callback, bool parseWhenLoaded = true)
        {
            EnqueueAssetPhysicalPath(physicalPathList, parseWhenLoaded);
            _assetLoadedCallbackList.Add(AssetLoadedCallbackWrapper.Get(physicalPathList, callback, parseWhenLoaded));
        }

        internal bool IsActiveAsset(string physicalPath)
        {
            return _activeAssetDict.ContainsKey(physicalPath) == true;
        }

        internal bool IsActiveObject(string physicalPath)
        {
            return _activeObjectDict.ContainsKey(physicalPath) == true;
        }

        public bool IsLoadedAsset(string physicalPath)
        {
            return _loadedAssetSet.Contains(physicalPath);
        }

        public int GetLoadingAssetCount()
        {
            return _loadingAssetSet.Count;
        }

        private void AddToAssetLoadedCountSet(string path)
        {
            if (!_assetLoadedCountSet.ContainsKey(path))
            {
                _assetLoadedCountSet[path] = 0;
            }
            _assetLoadedCountSet[path] += 1;
        }

        public void EnqueueAssetPhysicalPath(string physicalPath, bool parseWhenLoaded = true)
        {
            //资源已加载或在等待加载队列中时,不加载
            if(IsActiveAsset(physicalPath) == false
                && _loadingAssetSet.Contains(physicalPath) == false
                && _assetPhysicalPathSet.Contains(physicalPath) == false)
            {
                _assetPhysicalPathQueue.Enqueue(physicalPath);
                _assetPhysicalPathSet.Add(physicalPath);
            }
            if (!_parseMarkDict.ContainsKey(physicalPath) || !_parseMarkDict[physicalPath])
            {
                _parseMarkDict[physicalPath] = parseWhenLoaded;
            }
        }

        public void EnqueueAssetPhysicalPath(List<string> physicalPathList, bool parseWhenLoaded = true)
        {
            for(int i = 0; i < physicalPathList.Count; i++)
            {
                EnqueueAssetPhysicalPath(physicalPathList[i], parseWhenLoaded);
            }
        }

        private bool ParseWhenLoaded(string physicalPath)
        {
            if (_parseMarkDict.ContainsKey(physicalPath))
            {
                return _parseMarkDict[physicalPath];
            }
            return true;
        }

        private void ParseLoadedAssetBundle(string physicalPath, AssetBundle assetBundle)
        {
            if (!ParseWhenLoaded(physicalPath)) return;
            if(physicalPath.EndsWith(POSTFIX_FOLDER) == true)
            {
                if(physicalPath == SHADER_PHYSICAL_PATH)
                {
                    _shaderAssetBundle = assetBundle;
                    _activeAssetDict.Add(physicalPath, new List<string>());
                }
                else
                {
                    ParseFolderPackedAsset(physicalPath, assetBundle);
                }
            }
            else if(physicalPath.EndsWith(POSTFIX_SELECTION))
            {
                ParseSelectionPackedAsset(physicalPath, assetBundle);
            }
            else
            {
                if(physicalPath == FONT_YAHEI_UNICODE_PATH)
                {
                    ParseFontAsset(physicalPath, assetBundle);
                }
                else
                {
                    ParseSinglePackedAsset(physicalPath, assetBundle);
                }
            }
        }

        private void CheckUnloadStrategy(string physicalPath, WWW www)
        { 
           if(_objectPool.GetUnloadStrategy(physicalPath) == 1)
            {
                UnloadWww(www, null);
            }
            else
            {
                www.Dispose();
            } 
        }

        private void ParseFontAsset(string physicalPath, AssetBundle asset)
        {
			Object[] objs = asset.LoadAllAssets();
            foreach(Object obj in objs)
            {
                if(obj.name.Contains("MicrosoftYaHei_unicode") == true)
                {
                    _activeObjectDict.Add(FONT_YAHEI_UNICODE_PATH, obj);

                }
                else
                {
                    if(obj.name.Contains("MicrosoftYaHei") == true)
                    {
                        _activeObjectDict.Add(FONT_YAHEI_PATH, obj);
                    }
                }
            }
            _activeAssetDict.Add(physicalPath, new List<string>() { FONT_YAHEI_UNICODE_PATH, FONT_YAHEI_PATH });
        }

        private void ParseSelectionPackedAsset(string physicalPath, AssetBundle asset)
        {
			Object[] objs = asset.LoadAllAssets();
            List<string> objKeyList = new List<string>();
            foreach(Object obj in objs)
            {
                string objKey = obj.name;
                if(_activeObjectDict.ContainsKey(objKey) == false)
                {
                    _activeObjectDict.Add(objKey, obj);
                    objKeyList.Add(objKey);
                }
                else
                {
                    Debug.LogWarning("Key already exists: " + objKey + " -- " + obj);
                }
            }
            if(_activeAssetDict.ContainsKey(physicalPath) == false)
            {
                _activeAssetDict.Add(physicalPath, objKeyList);
            }
            else
            {
                Debug.LogWarning("Asset already exists: " + physicalPath);
            }
        }

        private void ParseFolderPackedAsset(string physicalPath, AssetBundle asset)
        {
            Match m = POSTFIX_FOLDER_PATTERN.Match(physicalPath);
            string folderPath = physicalPath.Substring(0, m.Index);
			Object[] objs = asset.LoadAllAssets();
            List<string> objKeyList = new List<string>();
            foreach(Object obj in objs)
            {
                //Debug.LogError(physicalPath + " ==> " + obj.GetType() + "  " + obj);
                string objKey = string.Concat(folderPath, "$", obj.name, ".u");
                //模型FBX文件中的Transform和Mesh可能会和模型本身GameObject重名，不处理这种情况
                if((obj is Transform) == true || (obj is Mesh) == true)
                {
                    continue;
                }
                if(_activeObjectDict.ContainsKey(objKey) == false)
                {
                    _activeObjectDict.Add(objKey, obj);
                    objKeyList.Add(objKey);
                }
                else
                {
                    Debug.LogWarning("Key already exists: " + objKey + " -- " + obj);
                }
            }
            if(_activeAssetDict.ContainsKey(physicalPath) == false)
            {
                _activeAssetDict.Add(physicalPath, objKeyList);
            }
            else
            {
                Debug.LogWarning("Asset already exists: " + physicalPath);
            }
        }

        private void ParseSinglePackedAsset(string physicalPath, AssetBundle asset)
        {
            Object mainAsset = null;
            if (asset == null)
            {
                Debug.LogError("ParseSinglePackedAsset error in asset == null: " + physicalPath);
            }
            mainAsset = asset.LoadAllAssets()[0];
            if(_activeObjectDict.ContainsKey(physicalPath) == false)
            {
               
                _activeObjectDict.Add(physicalPath, mainAsset);
                _activeAssetDict.Add(physicalPath, new List<string>() { physicalPath });
            }
            else
            {
                Debug.LogWarning("Asset already exists: " + physicalPath);
            }
            
        }

        private void UnloadWww(WWW www, AssetBundle assetBundle)
        {
            if(www.assetBundle != null)
            {
                www.assetBundle.Unload(false);
            }
            www.Dispose();
        }

        private void AddToPresistWwwDict(string physicalPath, WWW www)
        {
            if(_presistWwwDict.ContainsKey(physicalPath) == false)
            {
                _presistWwwDict.Add(physicalPath, www);
            }
        }

        internal void RemovePresistWww(string physicalPath)
        {
            if(_presistWwwDict.ContainsKey(physicalPath) == true)
            {
                WWW www = _presistWwwDict[physicalPath];
                www.assetBundle.Unload(false);
                www.Dispose();
                _presistWwwDict.Remove(physicalPath);
            }
        }

        internal Shader GetActiveShader(string physicalPath)
        {
            if(_activeObjectDict.ContainsKey(physicalPath) == true)
            {
                return _activeObjectDict[physicalPath] as Shader;
            }
            int dollarIndex = physicalPath.LastIndexOf(TOKEN_DOLLAR);
            int dotIndex = physicalPath.LastIndexOf(TOKEN_DOT);
            string name = physicalPath.Substring(dollarIndex + 1, (dotIndex - dollarIndex - 1));
            Shader shader = null;
            if (_shaderAssetBundle!=null && _shaderAssetBundle.Contains(name) == true)
            {
				shader = _shaderAssetBundle.LoadAsset(name) as Shader;
                _activeObjectDict.Add(physicalPath, shader);
                _activeAssetDict[SHADER_PHYSICAL_PATH].Add(name);
            }
            else
            {
                shader = Shader.Find(name);
            }
            return shader;
        }

        internal Texture2D GetDefaultParticleTexture()
        {
            if(_activeObjectDict.ContainsKey(AssetCreator.DEFAULT_PARTICLE) == true)
            {
                return _activeObjectDict[AssetCreator.DEFAULT_PARTICLE] as Texture2D;
            }
            Texture2D texture = null;
            if(_shaderAssetBundle != null)
            {
				texture = _shaderAssetBundle.LoadAsset(AssetCreator.DEFAULT_PARTICLE) as Texture2D;
                _activeObjectDict.Add(AssetCreator.DEFAULT_PARTICLE, texture);
            }
            return texture;
        }

        internal Object GetActiveObject(string physicalPath)
        {
            if(string.IsNullOrEmpty(physicalPath) == true)
            {
                Debug.LogError("ActiveOjbect physicalPath can not by null or empty!!");
                return null;
            }
            if(physicalPath == AssetCreator.DEFAULT_DIFFUSE)
            {
                return AssetCreator.GetDefaultDiffuseMaterial();
            }
            if(physicalPath == AssetCreator.DEFAULT_PARTICLE)
            {
                return AssetCreator.GetDefaultParticleMaterial();
            }
            Object obj;
            _activeObjectDict.TryGetValue(physicalPath, out obj);
            if(obj == null)
            {
                Debug.LogError("ActiveObject not found!! physicalPath: " + physicalPath);
                return null;
            }
            
            if((obj is TextAsset) && physicalPath.Contains(SHADOW) == true)
            {
                JsonData jsonData = GetShadowJsonData(physicalPath);
                string type = (string)jsonData[TYPE];
                if(type == TYPE_MATERIAL)
                {
                    Material material = AssetCreator.CreateMaterial(jsonData, obj.name.Replace(SHADOW, string.Empty));
                    ReplaceActiveObject(physicalPath, material);
                    return material;
                }
                if(type == TYPE_CONTROLLER)
                {
                    AnimatorOverrideController controller = AssetCreator.CreateAnimatorController(jsonData);
                    ReplaceActiveObject(physicalPath, controller);
                    return controller;
                }
            }
            return obj;
        }

        internal JsonData GetShadowJsonData(string physicalPath)
        {
            if (!_shadowCacheDict.ContainsKey(physicalPath))
            {
                if (!_activeObjectDict.ContainsKey(physicalPath)) Debug.LogError("!_activeObjectDict.ContainsKey:" + physicalPath);
                Object obj = _activeObjectDict[physicalPath] as TextAsset;
                _shadowCacheDict[physicalPath] = JsonMapper.ToObject((obj as TextAsset).text);
            }
            return _shadowCacheDict[physicalPath];
        }

        internal void ReplaceActiveObject(string physicalPath, Object obj)
        {
            if(_activeObjectDict.ContainsKey(physicalPath) == true)
            {
                _activeObjectDict[physicalPath] = obj;
            }
        }

        internal void DeleteAssetImmediate(string physicalPath)
        {
            if(_activeAssetDict.ContainsKey(physicalPath) == false)
            {
                Debug.LogError(string.Format("Key [{0}] not found in ActiveAssetDict!", physicalPath));
                return;
            }
            RemovePresistWww(physicalPath);
            List<string> objKeyList = _activeAssetDict[physicalPath];
            foreach(string objKey in objKeyList)
            {
                if(_activeObjectDict.ContainsKey(objKey) == true)
                {
                    Object.DestroyImmediate(_activeObjectDict[objKey], true);
                    _activeObjectDict.Remove(objKey);
                }
                else
                {
                    Debug.LogError(string.Format("Key [{0}] not found in ActiveObjectDict !!", objKey));
                }
            }
            _activeAssetDict.Remove(physicalPath);
        }

        internal Dictionary<string, List<string>> GetActiveAssetDict()
        {
            return _activeAssetDict;
        }

        private IEnumerator WaitForLoadingAsset()
        {
            float waittingTime = Time.realtimeSinceStartup;
            while(_assetLoadedCallbackList.Count > 0)
            {
                yield return null;
                if ((Time.realtimeSinceStartup - waittingTime) > 10)
                {
                    Debug.LogError("WaitForLoadingAsset 超时！");
                    PrintAssetLoadedCallbackList();
                    break;
                }
            }
        }

        private void PrintAssetLoadedCallbackList()
        {
            string debugInfo = "remain resources: \n";
            for (int j = 0; j < _assetLoadedCallbackList.Count; j++)
            {
                AssetLoadedCallbackWrapper wrapper = _assetLoadedCallbackList[j];
                var physicalPathList = wrapper.physicalPathList;
                for (int i = 0; i < physicalPathList.Count; i++)
                {
                    string physicalPath = physicalPathList[i];
                    if (wrapper.parseWhenLoaded)
                    {
                        if (IsActiveAsset(physicalPath) == false)
                        {
                            debugInfo += physicalPath + " x/n";
                        }
                    }
                    else
                    {
                        if (!externalExistsFiles.Contains(physicalPath))
                        {
                            debugInfo += physicalPath + " y/n";
                        }
                    }
                }
            }
            Debug.LogError(debugInfo);
        }

        internal Coroutine CreateWaitForLoadingAssetCoroutine()
        {
            return StartCoroutine(WaitForLoadingAsset());
        }
        /// <summary>
        /// 刪除加载了但未使用的资源，比如战斗中预加载的资源
        /// </summary>
        internal void DeleteUnusedAsset()
        {
            _unusedAssetPathList.Clear();
            foreach(KeyValuePair<string, List<string>> kvp in _activeAssetDict)
            {
                if(_objectPool.IsAssetInUse(kvp.Key) == false)
                {
                    _unusedAssetPathList.Add(kvp.Key);
                }
            }
            for(int i = 0; i < _unusedAssetPathList.Count; i++)
            {
                DeleteAssetImmediate(_unusedAssetPathList[i]);
            }
        }

        internal void WaitForOneFrame(Action callback)
        {
            StartCoroutine(CreateWaitOneFrameCoroutine(callback));
        }

        private IEnumerator CreateWaitOneFrameCoroutine(Action callback)
        {
            yield return null;
            callback();
        }

        public bool ExistsInLocalExternal(String physicalPath)
        {
            return externalExistsFiles.Contains(physicalPath);
        }

        private void SaveToExternal(String physicalPath, WWW www)
        {
#if !UNITY_WEBPLAYER
            if (!_objectPool.IsExternalAsset(physicalPath)) return;
            if (externalExistsFiles.Contains(physicalPath)) return;
            string fileName = ObjectPool.ASSET_ROOT_PATH + physicalPath;
            if (!File.Exists(fileName))
            {
                string tempFileName = fileName + "_temp";
                if (File.Exists(tempFileName)) File.Delete(tempFileName);
                byte[] buffer = www.bytes;
                using (FileStream fs = new FileStream(tempFileName, FileMode.Create))
                {
                    using (BinaryWriter sw = new BinaryWriter(fs))
                    {
                        sw.Write(buffer);
                        sw.Flush();
                        sw.Close();
                    }
                    fs.Close();
                }
                File.Move(tempFileName, fileName);
            }
            externalExistsFiles.Add(physicalPath);
#endif
        }
    }
}
