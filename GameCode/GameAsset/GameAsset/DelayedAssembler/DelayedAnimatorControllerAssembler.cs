﻿using LitJson;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Asset
{
    public class DelayedAnimatorControllerAssembler : MonoBehaviour
    {
        public string[] tokens;

        private Animator _animator;
        private DelayedAnimatorController _delayedAnimatorController;

        void Start()
        {
            _animator = GetComponent<Animator>();
            if(_animator != null)
            {
                string controllerKey = tokens[0];
                _delayedAnimatorController = AssetCreator.CreateDelayAnimatorController(controllerKey);
                _animator.runtimeAnimatorController = _delayedAnimatorController.controller;
            }
        }

        int spaceTime = 0;
        void Update()
        {
            spaceTime--;
            if (spaceTime > 0) return;
            spaceTime = 10;
            for (int i = 0; i < _animator.layerCount; i++)
            {
                AnimatorClipInfo[] infos = _animator.GetCurrentAnimatorClipInfo(i);
                foreach (AnimatorClipInfo info in infos)
                {
                    string currentClipName = info.clip.name;
                    _delayedAnimatorController.PlayClip(currentClipName);
                }
            }
        }
    }
}
