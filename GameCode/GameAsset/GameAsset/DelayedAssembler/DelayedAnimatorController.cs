﻿using LitJson;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Asset
{

    public class DelayedAnimatorController
    {
        AnimatorOverrideController _controller;
        public AnimatorOverrideController controller
        {
            get { return _controller; }
        }

        Dictionary<string, string> _clipNameKeyDict;
        HashSet<string> _loadedClips = new HashSet<string>();

        public DelayedAnimatorController(string physicalPath)
        {
            var jsonData = ObjectPool.Instance.GetShadowJsonData(physicalPath);
            string name = (string)jsonData[AssetCreator.CONTROLLER];
            _controller = new AnimatorOverrideController();
            _controller.runtimeAnimatorController = ObjectPool.Instance.GetAssemblyObject(name) as RuntimeAnimatorController;

            _clipNameKeyDict = new Dictionary<string, string>();
            JsonData clipKeys = jsonData[AssetCreator.CLIP_KEYS];
            List<string> clipNames = new List<string>();
            List<string> physicalPaths = new List<string>();
            for (int i = 0; i < clipKeys.Count; i++)
            {
                JsonData clipData = clipKeys[i];
                string clipName = (string)clipData[0];
                string clipPhysicalPath = (string)clipData[1];
                _clipNameKeyDict[clipName] = clipPhysicalPath;
                if (ObjectPool.Instance.ExistsInLocal(clipPhysicalPath))
                {
                    clipNames.Add(clipName);
                    physicalPaths.Add(clipPhysicalPath);
                }
            }
            if (clipNames.Count > 0)
            {
                LoadClips(clipNames, physicalPaths);
            }
        }

        public void PlayClip(string clipName)
        {
            if (_loadedClips.Contains(clipName)) return;
            if (!_clipNameKeyDict.ContainsKey(clipName)) return;
            LoadClip(clipName, _clipNameKeyDict[clipName]);
        }

        void LoadClip(string clipName, string physicalPath)
        {
            _loadedClips.Add(clipName);
            ObjectPool.Instance.LoadAssetByPhysicalPath(new string[] { physicalPath }, delegate() { ResetClip(clipName, physicalPath); });
        }

        void LoadClips(List<string> clipNames, List<string> physicalPaths)
        {
            for (int i = 0; i < clipNames.Count; i++)
            {
                _loadedClips.Add(clipNames[i]);    
            }
            ObjectPool.Instance.LoadAssetByPhysicalPath(physicalPaths.ToArray(), delegate() { ResetClips(clipNames, physicalPaths); });    
        }

        void ResetClip(string clipName, string physicalPath)
        {
            _controller[clipName] = ObjectPool.Instance.GetAssemblyObject(physicalPath) as AnimationClip;
        }

        void ResetClips(List<string> clipNames, List<string> physicalPaths)
        {
            for (int i = 0; i < clipNames.Count; i++)
            {
                string clipName = clipNames[i];
                _controller[clipName] = ObjectPool.Instance.GetAssemblyObject(physicalPaths[i]) as AnimationClip;
            }
        }

        public void Release()
        {
            Object.DestroyImmediate(_controller);
            _controller = null;
        }
    }

}
