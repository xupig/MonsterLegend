﻿using LitJson;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Game.Asset
{

    public class DelayedConfig
    {
        public const string TAG_DELAYED = "Delayed";
        
        public const string CAN_DELAY_PATH_SCENES_PREFAB = "^Scenes.*?(\\.prefab)$";
        public const string CAN_DELAY_PATH_CHARACTERS_PREFAB = "^Characters.*?(\\.prefab)$";

        public const string CAN_DELAY_PHYSICAL_PATH = ".*(\\.anim\\.u|\\.png\\.u)$";
        
        private static  bool _inited = false;
        public static bool CanDelayLoading = false;


        static void InitConfig()
        {
            if (_inited) return;
            _inited = true;
            CanDelayLoading = false;
#if UNITY_WEBPLAYER
            CanDelayLoading = true;
#endif
            if (Application.platform == RuntimePlatform.WindowsPlayer
               || Application.platform == RuntimePlatform.WindowsWebPlayer
               || ObjectPool.Instance.HasExternalAsset())
            {
                CanDelayLoading = true;
            }
        }

        public static bool IsPlatformCanDelay()
        {
            InitConfig();
            return CanDelayLoading;
        }

        public static bool IsPathCanDelay(string path)
        {
            bool result = false;
            if (Regex.IsMatch(path, CAN_DELAY_PATH_SCENES_PREFAB)) return true;
            if (Regex.IsMatch(path, CAN_DELAY_PATH_CHARACTERS_PREFAB)) return true;
            return result;
        }

        public static bool IsPhysicalPathCanDelay(string physicalPath)
        {
            return Regex.IsMatch(physicalPath, CAN_DELAY_PHYSICAL_PATH);
        }

        static void SetTag(Transform trans, string tag, bool includeChildren = false)
        {
            trans.gameObject.tag = tag;
            if (includeChildren)
            {
                for (int i = 0; i < trans.childCount; i++)
                {
                    SetTag(trans.GetChild(i), tag, includeChildren);
                }
            }
        }

        public static void SetDelayTag(Transform trans)
        {
            if (trans.gameObject.CompareTag(TAG_DELAYED)) return;
            SetTag(trans, TAG_DELAYED, true);
        }
    }

}
