﻿using LitJson;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Asset
{
    public class DelayedTerrainAssembler : MonoBehaviour
    {
        public string[] tokens;

        bool loaded = false;

        List<string> _textures;

        void Start()
        {
            if (loaded) return;
            loaded = true;
            Load();
        }

        void Load()
        {
            _textures = GetTextures();
            ObjectPool.Instance.LoadAssetByPhysicalPath(_textures.ToArray(), delegate() { CreateTerrain(); });
        }

        List<string> GetTextures()
        {
            List<string> result = new List<string>();
            SplatPrototype[] splatPrototypes = new SplatPrototype[tokens.Length];
            for (int i = 0; i < tokens.Length; i++)
            {
                string token = tokens[i];
                string[] paramList = token.Split(TerrainAssembler.SEMICOLON_SPLIT);
                result.Add(paramList[0]);
            }
            return result;
        }

        void CreateTerrain()
        {
            Terrain terrain = gameObject.GetComponent<Terrain>();
            if (terrain != null)
            {
                SplatPrototype[] splatPrototypes = new SplatPrototype[tokens.Length];
                for (int i = 0; i < tokens.Length; i++)
                {
                    string token = tokens[i];
                    string[] paramList = token.Split(TerrainAssembler.SEMICOLON_SPLIT);
                    SplatPrototype splat = new SplatPrototype();
                    splat.normalMap = null;
                    splat.texture = ObjectPool.Instance.GetAssemblyObject(paramList[0]) as Texture2D;
                    splat.tileSize = AssetAssembler.GetVector2(paramList[1]);
                    splat.tileOffset = AssetAssembler.GetVector2(paramList[2]);
                    splatPrototypes[i] = splat;
                }
                terrain.terrainData.splatPrototypes = splatPrototypes;
                
            }
            else {
                Debug.LogError("terrain != null");
            }
        }
    }
}
