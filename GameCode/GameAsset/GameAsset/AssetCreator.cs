﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

namespace Game.Asset
{
    public class AssetCreator
    {
        public const string SHADER = "shader";
        public const string PROPERTY_LIST = "propertyList";
        public const string FLOAT = "Float";
        public const string COLOR = "Color";
        public const string TEX_ENV = "TexEnv";
        public const string VECTOR = "Vector";
        public const string RANGE = "Range";
        public const string CONTROLLER = "controller";
        public const string CLIP_KEYS = "clipKeys";
        public const string DEFAULT_DIFFUSE = "Default-Diffuse";
        public const string SHADER_DEFAULT_DIFFUSE_KEY = "Shader$Diffuse.u";
        public const string DEFAULT_PARTICLE = "Default-Particle";
        public const string DEFAULT_PARTICLE_TEXTURE = "Default-Particle-Texture";
        public const string SHADER_DEFAULT_PARTICLE_KEY = "Shader$Particles/Alpha Blended Premultiply.u";

        private static Material _defaultDiffuseMaterial;
        private static Material _defaultParticleMaterial;

        public static Material GetDefaultDiffuseMaterial()
        {
            if(_defaultDiffuseMaterial == null)
            {
                _defaultDiffuseMaterial = new Material(GetShader(SHADER_DEFAULT_DIFFUSE_KEY));
                _defaultDiffuseMaterial.name = DEFAULT_DIFFUSE;
            }
            return _defaultDiffuseMaterial;
        }

        public static Material GetDefaultParticleMaterial()
        {
            if(_defaultParticleMaterial == null)
            {
                _defaultParticleMaterial = new Material(GetShader(SHADER_DEFAULT_PARTICLE_KEY));
                _defaultParticleMaterial.SetTexture("_MainTex", ObjectPool.Instance.GetDefaultParticleTexture());
                _defaultParticleMaterial.name = DEFAULT_PARTICLE;
            }
            return _defaultParticleMaterial;
        }

        public static Material CreateMaterial(JsonData jsonData, string name)
        {
            Material material = new Material(GetShader((string)jsonData[SHADER]));
            material.name = name;
            JsonData propertyList = jsonData[PROPERTY_LIST];
            for(int i = 0; i < propertyList.Count; i++)
            {
                JsonData property = propertyList[i];
                ParseMaterialProperty(material, property);
            }
            return material;
        }

        static Dictionary<string, DelayedMaterial> _delayedMaterials = new Dictionary<string, DelayedMaterial>();
        public static DelayedMaterial CreateDelayedMaterial(string physicalPath)
        {
            if (!_delayedMaterials.ContainsKey(physicalPath))
            {
                _delayedMaterials[physicalPath] = new DelayedMaterial(physicalPath);
            }
            return _delayedMaterials[physicalPath];
        }

        public static Shader GetShader(string shaderKey)
        {
            Shader shader = ObjectPool.Instance.GetAssemblyShader(shaderKey);
            if(shader == null)
            {
                Debug.LogError("can't not find shader:" + shaderKey);
            }
            if(Application.isEditor == true && Shader.Find(shader.name) != null)
            {
                shader = Shader.Find(shader.name);
            }
            return shader;
        }

        public static void ParseMaterialProperty(Material material, JsonData property)
        {
            string type = (string)property[0];
            string name = (string)property[1];
            switch(type)
            {
                case RANGE:
                case FLOAT:
                    material.SetFloat(name, float.Parse((string)property[2]));
                    break;
                case COLOR:
                    material.SetColor(name, new Color(float.Parse((string)property[2]), float.Parse((string)property[3]), float.Parse((string)property[4]), float.Parse((string)property[5])));
                    break;
                case TEX_ENV:
                    material.SetTexture(name, ObjectPool.Instance.GetAssemblyObject((string)property[2]) as Texture);
                    material.SetTextureOffset(name, new Vector2(float.Parse((string)property[3]), float.Parse((string)property[4])));
                    material.SetTextureScale(name, new Vector2(float.Parse((string)property[5]), float.Parse((string)property[6])));
                    break;
                case VECTOR:
                    material.SetVector(name, new Vector4(float.Parse((string)property[2]), float.Parse((string)property[3]), float.Parse((string)property[4]), float.Parse((string)property[5])));
                    break;
            }
        }

        public static AnimatorOverrideController CreateAnimatorController(JsonData jsonData)
        {
            string name = (string)jsonData[CONTROLLER];
            AnimatorOverrideController controller = new AnimatorOverrideController();
            controller.runtimeAnimatorController = ObjectPool.Instance.GetAssemblyObject((string)jsonData[CONTROLLER]) as RuntimeAnimatorController;
            JsonData clipKeys = jsonData[CLIP_KEYS];
            for(int i = 0; i < clipKeys.Count; i++)
            {
                JsonData clipData = clipKeys[i];
                string clipName = (string)clipData[0];
                string clipKey = (string)clipData[1];
                controller[clipName] = ObjectPool.Instance.GetAssemblyObject(clipKey) as AnimationClip;
            }
            return controller;
        }

        static Dictionary<string, DelayedAnimatorController> _delayAnimatorControllers = new Dictionary<string, DelayedAnimatorController>();
        public static DelayedAnimatorController CreateDelayAnimatorController(string physicalPath)
        {
            if (!_delayAnimatorControllers.ContainsKey(physicalPath))
            {
                _delayAnimatorControllers[physicalPath] = new DelayedAnimatorController(physicalPath);
            }
            return _delayAnimatorControllers[physicalPath];
        }

        static List<string> _releasePhysicalPathList = new List<string>();
        static void ClearUnuseMaterials()
        {
            _releasePhysicalPathList.Clear();
            foreach (KeyValuePair<string, DelayedMaterial> kvp in _delayedMaterials)
            {
                string physicalPath = kvp.Key;
                if (!ObjectPool.Instance.IsActiveObject(physicalPath))
                {
                    _releasePhysicalPathList.Add(physicalPath);
                }
            }
            for (int i = 0; i < _releasePhysicalPathList.Count; i++)
            {
                string physicalPath = _releasePhysicalPathList[i];
                _delayedMaterials[physicalPath].Release();
                _delayedMaterials.Remove(physicalPath);
            }
        }

        static void ClearUnuseControllers()
        {
            _releasePhysicalPathList.Clear();
            foreach (KeyValuePair<string, DelayedAnimatorController> kvp in _delayAnimatorControllers)
            {
                string physicalPath = kvp.Key;
                if (!ObjectPool.Instance.IsActiveObject(physicalPath))
                {
                    _releasePhysicalPathList.Add(physicalPath);
                }
            }
            for (int i = 0; i < _releasePhysicalPathList.Count; i++)
            {
                string physicalPath = _releasePhysicalPathList[i];
                _delayAnimatorControllers[physicalPath].Release();
                _delayAnimatorControllers.Remove(physicalPath);
            }
        }

        public static void OnDeleteZeroReferrencedAsset()
        {
            ClearUnuseMaterials();
            ClearUnuseControllers();
        }
    }
}
