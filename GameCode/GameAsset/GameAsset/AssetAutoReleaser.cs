﻿using UnityEngine;
using System.Collections;

namespace Game.Asset
{
    public class AssetAutoReleaser : MonoBehaviour
    {
        public string path;

        protected void OnDestroy()
        {
            ObjectPool.Instance.Release(path);
        }
    }
}