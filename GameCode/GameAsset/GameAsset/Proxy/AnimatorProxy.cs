﻿using UnityEngine;
using System.Collections;
using System;

namespace Game.Asset
{
    public class AnimatorProxy : MonoBehaviour
    {
        public delegate void VoidDelegate();

        string _lastControllerPath;
        string _curControllerPath;
        bool _isLoading = false;
        VoidDelegate _loadedCallback;

        public void SetController(string path, VoidDelegate cb = null)
        {
            _lastControllerPath = path;
            if (cb !=null)_loadedCallback += cb;
            LoadController();
        }

        void LoadController()
        {
            if (_isLoading) return;
            if (_lastControllerPath == _curControllerPath)
            {
                LoadedCallback();
                return;
            }
            if (string.IsNullOrEmpty(_lastControllerPath)) return;
            var path = _lastControllerPath;
            _isLoading = true;
            var gameObjectProxy = gameObject;
            Game.Asset.ObjectPool.Instance.GetObject(path, (obj) =>
            {
                _isLoading = false;
                if (gameObjectProxy == null)
                {
                    ReleaseController(path);
                    return;
                }
                if (path != _lastControllerPath)
                {
                    ReleaseController(path);
                    LoadController();
                    return;
                }
                if (obj == null)
                {
                    Debug.LogError(path + " not found!!");
                    return;
                }
                ReleaseController(_curControllerPath);
                _curControllerPath = _lastControllerPath;
                var animator = GetComponent<Animator>();
                animator.runtimeAnimatorController = obj as RuntimeAnimatorController;
                LoadedCallback();
            });
        }

        void LoadedCallback()
        {
            if (_loadedCallback != null)
            {
                _loadedCallback();
                _loadedCallback = null;
            }
        }

        void ReleaseController(string path)
        {
            if (string.IsNullOrEmpty(path)) return;
            ObjectPool.Instance.Release(path);
        }

        void OnDestroy()
        {
            ReleaseController(_curControllerPath);
        }
    }
}