﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using Object = UnityEngine.Object;
using System.IO;

namespace Game.Asset
{
    /// <summary>
    /// 重要概念
    /// Path是资源的逻辑路径，是AssetRecord中DependceDict中的Key值，包含"/"， 例："Scenes/40001/40001_LightmapFar-0.exr"
    /// PhysicalPath是资源的物理路径，包含"$"，例："Scenes$40001$40001_LightmapFar-0.exr.u"
    /// </summary>
    public class ObjectPool
    {
        public const string ASSET_RECORD_FILE_NAME = "_resources.mk";
        private const string SLASH = "/";
        private const string DOLLAR = "$";

        private const int UNLOAD_STRATEGY_MASK = 0x0001;
        private const int MEMORY_STRATEGY_MASK = 0x001c;
        private const int EXTERNAL_MASK        = 0x0080;
        private const int PRIORITY_MASK        = 0x0F00;

        internal static bool isReleaseMode;            //是否设置为：发布模式(在PC环境下有效)
        public static string ASSET_ROOT_PATH = "";

        private static AssetRecord _assetRecord;
        public static AssetRecord assetRecord { get { return _assetRecord; } }
        private static Dictionary<string, int> _assetReferenceCountDict; //key为资源物理路径,Value为引用次数
        public static Dictionary<string, int> assetReferenceCountDict { get { return _assetReferenceCountDict; } }

        private static HashSet<string> _assetToDeletePhysicalPathSet;
        private static Dictionary<string, bool> _delayPathDict;  //Key为资源的目录路径， Value为资源是否进行延时加载的标记
        private static HashSet<string> _delayPhysicalPathSet;  //记录对应物理路径资源是否进行了延时加载
        private static AssetLoader _assetLoader;
        public static string RemoteURL { set { AssetLoader.RemoteURL = value; } }
        private static Dictionary<string, int> _versionDict;  //Key为资源的目录路径， Value为资源版本号

        private static bool _hasExternalAsset = false;
        public static ObjectPool Instance { get; private set; }

        static ObjectPool()
        {
            Instance = new ObjectPool();
        }

        private ObjectPool()
        {
            isReleaseMode = false;
            _assetReferenceCountDict = new Dictionary<string, int>();
            _assetToDeletePhysicalPathSet = new HashSet<string>();
            _delayPathDict = new Dictionary<string, bool>();
            _delayPhysicalPathSet = new HashSet<string>();
        }

        public void Initialize(string rootPath, string jsonContent, int deleteAssetInterval = 60, bool isReleaseMode = false)
        {
            ObjectPool.isReleaseMode = isReleaseMode;
            ASSET_ROOT_PATH = rootPath + "Resources/";
#if !UNITY_WEBPLAYER
            if (!Directory.Exists(ASSET_ROOT_PATH)) Directory.CreateDirectory(ASSET_ROOT_PATH);
#endif
            _assetLoader = AssetLoader.Instance;
            SetDeleteAssetInterval(deleteAssetInterval);
            _assetRecord = JsonMapper.ToObject<AssetRecord>(jsonContent);
        }

        public void SetExternalInfo(string jsonContent)
        {
            _hasExternalAsset = true;
            Dictionary<string, int> externalInfo = JsonMapper.ToObject<Dictionary<string, int>>(jsonContent);
            Dictionary<string, int> assetMemoryDict = _assetRecord.assetMemoryDict;
            foreach (KeyValuePair<string, int> pair in externalInfo)
            {
                string physicalPath = pair.Key;
                if (assetMemoryDict.ContainsKey(physicalPath))
                {
                    assetMemoryDict[physicalPath] = assetMemoryDict[physicalPath] | pair.Value;
                }
            }
        }

        public void SetVersionInfo(string jsonContent)
        {
            _versionDict = JsonMapper.ToObject<Dictionary<string, int>>(jsonContent);
        }

        public int GetVersion(string physicalPath)
        {
            int newValue = 0;
            _versionDict.TryGetValue(physicalPath, out newValue);
            return newValue;
        }

        public void SetFontAssetPath(string language)
        {
            AssetLoader.FONT_YAHEI_UNICODE_PATH = string.Format(AssetLoader.FONT_YAHEI_UNICODE_PATH, language);
            AssetLoader.FONT_YAHEI_PATH = string.Format(AssetLoader.FONT_YAHEI_PATH, language);
        }

        public void SetDeleteAssetInterval(int interval)
        {
            if(interval > AssetLoader.MIN_DELETE_ASSET_INTERVAL)
            {
                AssetLoader.DELETE_ASSET_INTERVAL = interval;
            }
        }

        public bool CheckAssetExists(string path)
        {
            if (_assetRecord.assetDependenceDict.ContainsKey(path) == false)
            { 
                Debug.LogError("未找到该资源的打包信息(@谢思宇)：  " + path);
                return false;
            }
            return true;
        }

        public List<string> GetDependAssetPhysicalPathList(string path)
        {
            List<string> result = null;
            _assetRecord.assetDependenceDict.TryGetValue(path, out result);
            if(result == null)
            {
                Debug.LogError("未找到该资源的打包信息(@谢思宇)：  " + path);
                return new List<string>();
            }
            return result;
        }

        public void GetTextAsset(string path, Action<TextAsset> callback)
        {
            GetObject(path, delegate(Object obj) { callback(obj as TextAsset); });
        }

        public void GetScene(string path, Action<Object> callback)
        {
            GetObject(path, callback);
        }

        public void GetAudioClip(string path, Action<AudioClip> callback)
        {
            GetAudioClips(new string[] { path }, delegate(AudioClip[] clips) { callback(clips[0]); });
        }

        public void GetAudioClips(string[] paths, Action<AudioClip[]> callback)
        {
            GetObjects(paths, delegate(Object[] objs){OnAudiosLoaded(paths, callback);});
        }

        private void OnAudiosLoaded(string[] paths, Action<AudioClip[]> callback)
        {
            AudioClip[] clips = new AudioClip[paths.Length];
            for(int i = 0; i < paths.Length; i++)
            {
                var path = paths[i];
                if (CheckAssetExists(path) == true)
                {
                    clips[i] = _assetLoader.GetActiveObject(ConvertPathToPhysicalPath(path)) as AudioClip;
                }
                else
                {
                    clips[i] = null;
                }
                
            }
            callback(clips);
        }

        public void GetUIGameObject(string path, Action<GameObject> callback)
        {
            GetGameObjects(new string[] { path }, delegate(GameObject[] gos) 
            {
                _assetLoader.WaitForOneFrame(delegate() { callback(gos[0]); });
            });
        }

        void CheckNeedDelayPaths(string[] paths)
        {
            if (!DelayedConfig.IsPlatformCanDelay()) return;
            for(int i = 0;i<paths.Length;i++)
            {
                string path = paths[i];
                if (DelayedConfig.IsPathCanDelay(path)) _delayPathDict[path] = true;
            }
        }

        bool IsDelayPath(string path)
        {
            return _delayPathDict.ContainsKey(path);
        }

        public void GetGameObject(string path, Action<GameObject> callback)
        {
            GetGameObjects(new string[] { path }, delegate(GameObject[] gos) { callback(gos[0]); });
        }

        public void GetGameObjects(string[] paths, Action<GameObject[]> callback)
        {
            GetObjects(paths, delegate(Object[] objs) { OnGameObjectsLoaded(paths, callback); });
        }

        private void OnGameObjectsLoaded(string[] paths, Action<GameObject[]> callback)
        {
            GameObject[] gos = new GameObject[paths.Length];
            for(int i = 0; i < paths.Length; i++)
            {
                string path = paths[i];
                if(CheckAssetExists(path) == true)
                {
                    GameObject obj = _assetLoader.GetActiveObject(ConvertPathToPhysicalPath(path)) as GameObject;
                    if (obj == null) Debug.LogError("----------" + path);
                    if (IsDelayPath(path)) DelayedConfig.SetDelayTag(obj.transform);
                    GameObject go = GameObject.Instantiate(obj) as GameObject;
                    AddAssetAutoReleaser(go, paths[i]);
                    gos[i] = go;
                }
                else
                {
                    gos[i] = null;
                }
            }
            callback(gos);
        }

        public float GetLoadingProgress(string[] paths)
        {
            float result = 0;
            for(int i = 0; i < paths.Length; i++)
            {
                var path = paths[i];
                List<string> physicalPathList = GetDependAssetPhysicalPathList(path);
                int loadedCount = 0;
                for(int j = 0; j < physicalPathList.Count; j++)
                {
                    if(_assetLoader.IsActiveAsset(physicalPathList[j]) == true)
                    {
                        loadedCount++;
                    }
                }
                result += (float)((float)loadedCount / (float)physicalPathList.Count / (float)paths.Length);
            }
            return result;
        }

        /// <summary>
        /// 根据物理路径判断资源是否已加载
        /// </summary>
        /// <param name="physicalPath">物理路径,例如:Scenes$1005_Race$1005_Race.prefab.u</param>
        /// <returns></returns>
        public bool IsLoadedAsset(string physicalPath)
        {
            return _assetLoader.IsLoadedAsset(physicalPath);
        }

        /// <summary>
        /// 根据物理路径从常驻内存取出资源
        /// </summary>
        /// <param name="physicalPath">物理路径,例如:Scenes$1005_Race$1005_Race.prefab.u</param>
        /// <returns></returns>
        public byte[] GetPresistAssetData(string physicalPath)
        {
            return _assetLoader.GetPresistAssetData(physicalPath);
        }

        private void AddAssetAutoReleaser(GameObject go, string path)
        {
            AssetAutoReleaser helper = go.AddComponent<AssetAutoReleaser>();
            helper.path = path;
        }

        public void GetObject(string path, Action<Object> callback)
        {
            GetObjects(new string[] { path }, delegate(Object[] objs) { callback(objs[0]); });
        }

        public void GetObjects(string[] paths, Action<Object[]> callback)
        {
            LoadAsset(paths, delegate() { OnObjectsLoaded(paths, callback); });
        }

        public JsonData GetShadowJsonData(string physicalPath)
        {
            return _assetLoader.GetShadowJsonData(physicalPath);
        }

        private void OnObjectsLoaded(string[] paths, Action<Object[]> callback)
        {
            Object[] objs = new Object[paths.Length];
            for(int i = 0; i < paths.Length; i++)
            {
                var path = paths[i];
                Object obj = null;
                if (CheckAssetExists(path) == true)
                {
                    obj = _assetLoader.GetActiveObject(ConvertPathToPhysicalPath(path));
                }
                objs[i] = obj;
            }
            callback(objs);
        }

        public void PreloadAsset(string[] paths, Action callback = null)
        {
            LoadAsset(paths, callback, false);
        }

        public void PreloadAssetFile(string[] physicalPaths, Action callback = null)
        {
            List<string> physicalPathList = new List<string>(physicalPaths);
            _assetLoader.LoadAssetList(physicalPathList, callback, false);
        }

        /// <summary>
        /// 某些个别资源需要永久存在于内存中，可以使用这个方法做个标记，
        /// 通过将内存管理类型修改为永不释放
        /// </summary>
        /// <returns></returns>
        public void DontDestroy(string[] paths)
        {
            for (int i = 0; i < paths.Length; i++)
            {
                List<string> physicalPathList = GetDependAssetPhysicalPathList(paths[i]);
                for(int j = 0; j < physicalPathList.Count; j++)
                {
                    ChangeMemoryStrategy(physicalPathList[j], AssetMemoryMode.FOREVER);
                }
            }
        }

        private void LoadAsset(string[] paths, Action callback, bool increaseReference = true)
        {
            CheckNeedDelayPaths(paths);
            List<string> dependPhysicalPathList = new List<string>();
            for (int i = 0; i < paths.Length; i++)
            {                  
                string path = paths[i];
                if(CheckAssetExists(path) == false)
                {
                    continue;
                }
                if(increaseReference == true)
                {
                    IncreaseAssetReference(path);
                }
                bool delay = IsDelayPath(path);
                List<string> subDependPhysicalPathList = GetDependAssetPhysicalPathList(path);
                for(int j = 0; j < subDependPhysicalPathList.Count; j++)
                {
                    string physicalPath = subDependPhysicalPathList[j];
                    if (delay && DelayedConfig.IsPhysicalPathCanDelay(physicalPath))
                    {   //开启了延时加载后，被定义为可以延时的资源不会马上加入的下载列表中
                        _delayPhysicalPathSet.Add(physicalPath);
                        continue;
                    }
                    dependPhysicalPathList.Add(physicalPath);
                }
            }
            _assetLoader.LoadAssetList(dependPhysicalPathList, callback);
        }

		//增加可以直接通过physicalpath加载资源的接口，以支持延时加载，仅供GameAsset内容模块调用
        public void LoadAssetByPhysicalPath(string[] physicalPaths, Action callback)
        {
            List<string> dependPhysicalPathList = new List<string>();
            for (int i = 0; i < physicalPaths.Length; i++)
            {
                string physicalPath = physicalPaths[i];
                dependPhysicalPathList.Add(physicalPath);
            }
            _assetLoader.LoadAssetList(dependPhysicalPathList, callback);
        }

        public Object GetAssemblyObject(string physicalPath)
        {
            return _assetLoader.GetActiveObject(physicalPath);
        }

        public Shader GetAssemblyShader(string physicalPath)
        {
            return _assetLoader.GetActiveShader(physicalPath);
        }

        public Shader GetShader(string physicalPath)
        {
            return _assetLoader.GetActiveShader(physicalPath);
        }

        public Texture2D GetDefaultParticleTexture()
        {
            return _assetLoader.GetDefaultParticleTexture();
        }

		//判断是否与本地安装包中或者在外部磁盘上
        public bool ExistsInLocal(String physicalPath)
        {
            return !IsExternalAsset(physicalPath) || _assetLoader.ExistsInLocalExternal(physicalPath);
        }

        /// <summary>
        /// 从逻辑路径获得物理路径
        /// 在资源列表中的最后一个，注意Material和AnimatorController的处理方式，最后一个文件是Json文件
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private string ConvertPathToPhysicalPath(string path)
        {
            List<string> physicalPathList = GetDependAssetPhysicalPathList(path);
            return physicalPathList[physicalPathList.Count - 1];
        }

        private void IncreaseAssetReference(string path)
        {
            List<string> dependAssePhysicalPathList = GetDependAssetPhysicalPathList(path);
            RemoveFromToDeleteAssetPhysicalPathSet(dependAssePhysicalPathList);
            for(int i = 0; i < dependAssePhysicalPathList.Count; i++)
            {
                string physicalPath = dependAssePhysicalPathList[i];
                if(GetMemoryStrategy(physicalPath) == AssetMemoryMode.REFERENCE)
                {
                    if(_assetReferenceCountDict.ContainsKey(physicalPath) == false)
                    {
                        _assetReferenceCountDict.Add(physicalPath, 1);
                    }
                    else
                    {
                        _assetReferenceCountDict[physicalPath] = _assetReferenceCountDict[physicalPath] + 1;
                    }
                }
            }
            //LogReference();
        }

        private void DecreaseAssetReference(string path)
        {
            List<string> dependPhysicalPathList = GetDependAssetPhysicalPathList(path);
            foreach(string physicalPath in dependPhysicalPathList)
            {
                if(GetMemoryStrategy(physicalPath) == AssetMemoryMode.REFERENCE)
                {
                    if (!_assetReferenceCountDict.ContainsKey(physicalPath)) 
                    {
                        Debug.LogError("_assetReferenceCountDict key Error:" + physicalPath);
                        continue;
                    }
                    _assetReferenceCountDict[physicalPath] = _assetReferenceCountDict[physicalPath] - 1;
                    if(_assetReferenceCountDict[physicalPath] == 0)
                    {
                        _assetReferenceCountDict.Remove(physicalPath);
                        AddToDeleteAssetPhysicalPathSet(physicalPath);
                    }
                }
                else if(GetMemoryStrategy(physicalPath) == AssetMemoryMode.ONCEONLY)
                {
                    DeleteAssetImmediate(physicalPath);
                }
            }
            //LogReference();
        }

        internal bool IsAssetInUse(string physicalPath)
        {
            if(GetMemoryStrategy(physicalPath) == AssetMemoryMode.FOREVER)
            {
                return true;
            }
            if(_assetReferenceCountDict.ContainsKey(physicalPath) == true)
            {
                return true;
            }
            return false;
        }
        
        internal bool IsActiveObject(string physicalPath)
        { 
            return _assetLoader.IsActiveObject(physicalPath);
        }

        private void ChangeMemoryStrategy(string physicalPath, int memoryMode)
        {
            int newValue = 0;
            _assetRecord.assetMemoryDict.TryGetValue(physicalPath, out newValue);
            newValue = newValue & (~MEMORY_STRATEGY_MASK);
            newValue = newValue | (memoryMode << 1) | GetUnloadStrategy(physicalPath);
            _assetRecord.assetMemoryDict[physicalPath] = newValue;
        }

        private int GetMemoryStrategy(string physicalPath)
        {
            int result = 0;
            _assetRecord.assetMemoryDict.TryGetValue(physicalPath, out result);
            if(result == 0)
            {
                return AssetMemoryMode.REFERENCE;
            }
			return (result & MEMORY_STRATEGY_MASK) >> 1;
        }

        internal int GetUnloadStrategy(string physicalPath)
        {
            int result = 0;
            _assetRecord.assetMemoryDict.TryGetValue(physicalPath, out result);
            if(result == 0)
            {
                return 1;
            }
            return result & UNLOAD_STRATEGY_MASK;
        }

        public bool IsExternalAsset(string physicalPath)
        {
			int result = 0;
            _assetRecord.assetMemoryDict.TryGetValue(physicalPath, out result);
            return (result & EXTERNAL_MASK) > 0;
        }

        public bool HasExternalAsset()
        {
            return _hasExternalAsset;
        }

        public int GetPriority(string physicalPath)
        {
            int result = 0;
            _assetRecord.assetMemoryDict.TryGetValue(physicalPath, out result);
            return (result & PRIORITY_MASK) >> 8;   
        }

        private void AddToDeleteAssetPhysicalPathSet(string path)
        {
            _assetToDeletePhysicalPathSet.Add(path);
        }

        private void RemoveFromToDeleteAssetPhysicalPathSet(List<string> dependAssetPhysicalPathList)
        {
            for(int i = 0; i < dependAssetPhysicalPathList.Count; i++)
            {
                //HashSet remove之前可以不做存在性检查
                _assetToDeletePhysicalPathSet.Remove(dependAssetPhysicalPathList[i]);
            }
        }

        private void DeleteAssetImmediate(string physicalPath)
        {
            if (!_assetLoader.IsActiveAsset(physicalPath) && _delayPhysicalPathSet.Contains(physicalPath)) return;
            _assetLoader.DeleteAssetImmediate(physicalPath);
        }

        public void DeleteZeroReferrencedAsset()
        {
            foreach(string physicalPath in _assetToDeletePhysicalPathSet)
            {
                DeleteAssetImmediate(physicalPath);
            }
            _assetToDeletePhysicalPathSet.Clear();
            AssetCreator.OnDeleteZeroReferrencedAsset();
        }

        private void LogReference()
        {
            foreach(string s in _assetReferenceCountDict.Keys)
            {
                Debug.Log("LogReferenceCount: " + s + "  " + _assetReferenceCountDict[s]);
            }
        }

        public void Release(string path)
        {
            DecreaseAssetReference(path);
        }

        public void RemovePresistWww(string path)
        {
            _assetLoader.RemovePresistWww(ConvertPathToPhysicalPath(path));
        }

        public bool HasLoadingAsset()
        {
            return _assetLoader.GetLoadingAssetCount() > 0;
        }

        /// <summary>
        /// Editor辅助工具ObjectPoolInspector专供
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, List<string>> GetActiveAssetDict()
        {
            if(_assetLoader == null)
            {
                return new Dictionary<string, List<string>>();
            }
            return _assetLoader.GetActiveAssetDict();
        }

        /// <summary>
        /// Editor辅助工具ObjectPoolInspector专供
        /// </summary>
        /// <returns></returns>
        public static HashSet<string> GetAssetToDeletePhysicalPathSet()
        {
            if(_assetLoader == null)
            {
                return new HashSet<string>();
            }
            return _assetToDeletePhysicalPathSet;
        }

        public int GetLoadAssetCount()
        {
            return _assetLoader.LoadAssetCount;
        }

        public Dictionary<string, int> GetAssetLoadedCountSet()
        {
            return _assetLoader.AssetLoadedCountSet;
        }

        public Coroutine WaitForLoadingAsset()
        {
            return _assetLoader.CreateWaitForLoadingAssetCoroutine();
        }

        public void OnBeforeEnterScene()
        {
            DeleteZeroReferrencedAsset();
            _assetLoader.DeleteUnusedAsset();
        }

        public bool IsBusy()
        {
            return _assetLoader.isBusy;
        }
    }

    public class AssetMemoryMode
    {
        public const int FOREVER    = 8;
        public const int REFERENCE  = 4;
        public const int ONCEONLY   = 2;
    }
}

