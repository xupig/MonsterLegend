﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Game.Asset
{
    public class AssetRecord
    {
        public readonly Dictionary<string, int> assetMemoryDict;
        public readonly Dictionary<string, List<string>> assetDependenceDict;

        public AssetRecord()
        {
            assetMemoryDict = new Dictionary<string, int>();
            assetDependenceDict = new Dictionary<string, List<string>>();
        }
    }
}
