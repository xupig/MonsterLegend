﻿using UnityEngine;

namespace Game.Asset
{
    public class ParticleSystemRendererAssembler : AssetAssembler
    {

        public override void Assemble(GameObject go, string[] tokens)
        {
            string key = tokens[0];
            string meshName = tokens[1];
            ParticleSystemRenderer particleSystemRenderer = go.GetComponent<ParticleSystemRenderer>();
            if (particleSystemRenderer != null)
            {
                GameObject model = ObjectPool.Instance.GetAssemblyObject(key) as GameObject;
                Transform hostTransform = model.transform.Find(meshName);
                if (hostTransform == null)
                {
                    hostTransform = model.transform;
                }
                if (hostTransform != null)
                {
                    MeshFilter meshFilter = hostTransform.GetComponent<MeshFilter>();
                    if (meshFilter != null && meshFilter.sharedMesh != null)
                    {
                        particleSystemRenderer.mesh = meshFilter.sharedMesh;
                        return;
                    }
                }
            }
        }

    }
}
