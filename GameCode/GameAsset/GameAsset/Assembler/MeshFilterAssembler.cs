﻿using UnityEngine;

namespace Game.Asset
{
    public class MeshFilterAssembler : AssetAssembler
    {

        public override void Assemble(GameObject go, string[] tokens)
        {
            string key = tokens[0];
            string meshHostName = tokens[1];
            MeshFilter filter = go.GetComponent<MeshFilter>();
            if(filter != null)
            {
                GameObject model = ObjectPool.Instance.GetAssemblyObject(key) as GameObject;
                Transform hostTransform = model.transform.Find(meshHostName);
                if (hostTransform == null)
                {
                    hostTransform = model.transform;
                }
                if (hostTransform != null)
                {
                    MeshFilter meshFilter = hostTransform.GetComponent<MeshFilter>();
                    if (meshFilter != null && meshFilter.sharedMesh != null)
                    {
                        filter.mesh = meshFilter.sharedMesh;
                        return;
                    }
                }
                var newMesh = GetMesh(model.transform, meshHostName);
                if (newMesh != null)
                {
                    filter.mesh = newMesh;
                }
                else
                {
                    Debug.LogError(string.Format("Mesh {0} not found in {1}!", meshHostName, key));
                }
            }
        }

        private Mesh GetMesh(Transform modelTransform, string meshHostName)
        {
            if(modelTransform.name.Equals(meshHostName))
            {
                MeshFilter meshFilter = modelTransform.GetComponent<MeshFilter>();
                if (meshFilter != null && meshFilter.sharedMesh != null)
                {
                    return meshFilter.sharedMesh;
                }
            }
            for(int i = 0; i < modelTransform.childCount; i++)
            {
                Transform childTransform = modelTransform.GetChild(i);
                Mesh mesh = GetMesh(childTransform, meshHostName);
                if (mesh != null) return mesh;
            }
            return null;
        }
    }
}