﻿using UnityEngine;

namespace Game.Asset
{
    public class RendererAssembler : AssetAssembler
    {

        public override void Assemble(GameObject go, string[] tokens)
        {
            if (go.CompareTag(DelayedConfig.TAG_DELAYED))
            {
                DelayedRendererAssembler com = go.AddComponent<DelayedRendererAssembler>();
                com.tokens = tokens;
            }
            else
            {
                string type = tokens[0];
                int materialCount = tokens.Length - 1;
                Renderer render = go.GetComponent(type) as Renderer;
                if (render != null)
                {
                    Material[] materials = new Material[materialCount];
                    for (int i = 0; i < materialCount; i++)
                    {
                        materials[i] = ObjectPool.Instance.GetAssemblyObject(tokens[i + 1]) as Material;
                    }
                    render.materials = materials;
                }
            }
        }

    }
}
