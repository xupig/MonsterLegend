﻿using System;
using System.Collections.Generic;

namespace Game.Asset
{
    internal class AssetLoadedCallbackWrapper
    {
        private static Queue<AssetLoadedCallbackWrapper> _pool = new Queue<AssetLoadedCallbackWrapper>();

        public List<string> physicalPathList;
        public Action callback;
        public bool parseWhenLoaded;
        public bool isInvoked;

        public AssetLoadedCallbackWrapper()
        {
        }

        public static AssetLoadedCallbackWrapper Get(List<string> physicalPathList, Action callback, bool parseWhenLoaded)
        {
            AssetLoadedCallbackWrapper wrapper;
            if(_pool.Count == 0)
            {
                wrapper = new AssetLoadedCallbackWrapper();
            }
            else
            {
                wrapper = _pool.Dequeue();
            }
            wrapper.physicalPathList = physicalPathList;
            wrapper.callback = callback;
            wrapper.parseWhenLoaded = parseWhenLoaded;
            wrapper.isInvoked = false;
            return wrapper;
        }

        public static void Recycle(AssetLoadedCallbackWrapper wrapper)
        {
            wrapper.isInvoked = true;
            wrapper.callback = null;
            wrapper.physicalPathList = null;
            _pool.Enqueue(wrapper);
        }
    }
}
