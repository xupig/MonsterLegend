﻿using System.Collections.Generic;
using Common;
using Common.Base;
using Common.Events;
using Common.Structs;
using Game.UI.UIComponent;
using GameLoader.Config;
using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using ModuleCommonUI;
using ModuleLogin.ChildComponent;
using MogoEngine;
using MogoEngine.Events;
using GameLoader;
using GameData;
using Common.Utils;

namespace ModuleLogin
{
    /// <summary>
    /// 选角界面--视图类
    /// </summary>
    public class SelectRolePanel : BasePanel
    {
        private StateText _loginText;             //登陆按钮标签
        private StateText _selectServerName;      //选中服务器标签
        private KButton _btnStartGame;            //开始游戏按钮
        private KButton _btnBack;                 //返回登陆界面
        private KList _roleList;
        private KToggle _toggleTitle;

        private bool isSendReq = false;
        private int _curSelectServerId;           //当前选中服务器ID
        private AvatarInfo _curSelectAvatarInfo;  //当前选中角色信息 
        private List<AvatarInfo> _avatarList;     //玩家已注册的角色列表
        private List<AvatarInfo> _roleInfoList;
   
        private KButtonHitArea _btnHitArea;
        private SelectRoleComponent _model;

        protected override void Awake()
        {
            _avatarList = new List<AvatarInfo>();
            _btnStartGame = GetChildComponent<KButton>("Container_bottomCenter/Button_kaishiyouxi");
            _selectServerName = GetChildComponent<StateText>("Container_bottomCenter/Label_txtFuwuqi");
            _loginText = _btnStartGame.GetChildComponent<StateText>("label");
            _toggleTitle = GetChildComponent<KToggle>("Container_topCenter/ToggleGroup_biaoti/Toggle_chuangjianjiaose");
           
            _btnBack = GetChildComponent<KButton>("Container_topLeft/Button_fanhui");
            _roleList = GetChildComponent<KList>("Container_topLeft/List_left");
            _roleList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _toggleTitle.isOn = true;
            _roleInfoList = new List<AvatarInfo>();
            for (int i = 0; i < 4; i++)
            {
                _roleInfoList.Add(null);
            }
            _roleList.SetDataList<RoleInfoItem>(_roleInfoList);
            AddDragComponent();
        }

        /// <summary>
        /// 添加拖拽组件
        /// </summary>
        private void AddDragComponent()
        {
            UnityEngine.GameObject goDrag = new UnityEngine.GameObject();
            goDrag.name = "RoleDrag";
            goDrag.transform.SetParent(transform, false);
            goDrag.transform.localPosition = UnityEngine.Vector3.zero;
            goDrag.transform.localEulerAngles = UnityEngine.Vector3.zero;
            goDrag.transform.localScale = UnityEngine.Vector3.one;
            _btnHitArea = goDrag.gameObject.AddComponent<KButtonHitArea>();
            _model = AddChildComponent<SelectRoleComponent>(goDrag.name);
            _model.dragCallBack = SelectRoleManager.Instance.CreateRoleRotate;
            _model.Dragable = true;
        }

        public override void OnShow(object data)
        {
            isSendReq = false;
            OnSelectServerReturn(LocalSetting.settingData.SelectedServerID);
            InitAvatarList();
            AddEventListener();
            SelectRoleManager.Instance.CreateCreatedRoleModel(_avatarList);
        }

        private void SetSelectServerName(string name)
        {
            _selectServerName.CurrentText.text = string.IsNullOrEmpty(name) ? string.Empty : string.Format("当前：{0}", name);
        }

        /// <summary>
        /// 初始化已注册角色列表
        /// </summary>
        private void InitAvatarList()
        {
            if (MogoWorld.Player is PlayerAccount)
            {
                _avatarList.Clear();
                PlayerAccount account = MogoWorld.Player as PlayerAccount;
                Dictionary<ulong, AvatarInfo> avatarDict = account.avatarList;
                AvatarInfo nearLoginAvatar = null;
                foreach (AvatarInfo item in avatarDict.Values)
                {
                    if (item.DBID != LocalSetting.settingData.SelectedAvatarDbid) _avatarList.Add(item);
                    else nearLoginAvatar = item;
                    LoggerHelper.Debug("dbid:" + item.DBID + ",name:" + item.Name + ",vocation:" + item.Vocation);
                }
                _avatarList.Sort(Sort);
                if (nearLoginAvatar != null) _avatarList.Insert(0, nearLoginAvatar);  //把最近登录账号放在列表第一位显示               
                RefreshConetent();
                if (_avatarList.Count > 0)
                { //默认让第一个角色处于选中状态
                    _roleList.SelectedIndex = 0;
                    OnSelectChanged(_roleList, 0);
                }
                else _curSelectAvatarInfo = null;
            }
        }

        // 排序 规则：按角色等级降序排序
        private int Sort(AvatarInfo a, AvatarInfo b) 
        {
            if (a.Level > b.Level) return -1;
            if (a.Level < b.Level) return 1;
            return 0;
        }

        private void RefreshConetent()
        {
            AvatarInfo item = null;
            for (int i = 0; i < _roleInfoList.Count; i++)
            {
                item = _avatarList.Count > i ? _avatarList[i] : null;
                _roleInfoList[i] = item;
                _roleList[i].Data = item;
            }
        }

        private void AddEventListener()
        {
            _btnBack.onClick.AddListener(OnBack);
            _btnStartGame.onClick.AddListener(OnStartGame);
            _roleList.onSelectedIndexChanged.AddListener(OnSelectChanged);
            EventDispatcher.AddEventListener(SelectRoleEvents.C_SERVER_REQ_TIMEOUT,onReqTimeout);                    //请求进入游戏超时
            //EventDispatcher.AddEventListener<int>(LoginEvents.C_SELECT_SERVER_RETURN, OnSelectServerReturn);       //选服界面返回选中服务器
        }

        private void RemoveEventListener()
        {
            _btnBack.onClick.RemoveListener(OnBack);
            _btnStartGame.onClick.RemoveListener(OnStartGame);
            _roleList.onSelectedIndexChanged.RemoveListener(OnSelectChanged);
            EventDispatcher.RemoveEventListener(SelectRoleEvents.C_SERVER_REQ_TIMEOUT, onReqTimeout);
            //EventDispatcher.RemoveEventListener<int>(LoginEvents.C_SELECT_SERVER_RETURN, OnSelectServerReturn);       //选服界面返回选中服务器
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.SelectRole; }
        }

        public override void OnClose()
        {
            RemoveEventListener();
            SelectRoleManager.Instance.HideCreatedRole(-1,3f);
        }

        
        //=================== 控件响应处理 ===================//
        /// <summary>
        /// 选择角色进入游戏请求
        /// </summary>
        private void OnStartGame()
        {
            //uint[] data = new uint[] { 10, 100, 3601 };
            //UIManager.Instance.ShowPanel(PanelIdEnum.ServerLine, data);
            //return;

            if (isSendReq) return;
            
            ServerInfo serverInfo = ServerListManager.GetServerInfoByID(_curSelectServerId);
            if (_curSelectAvatarInfo == null || serverInfo == null)
            {
                string title = MogoLanguageUtil.GetContent(6006003);      //错误提示
                string serverTip = MogoLanguageUtil.GetContent(6006004);  //请选择服务器
                string roleTip = MogoLanguageUtil.GetContent(6006005);    //请选择角色
                //ari SystemAlert.Show(true, title, serverInfo == null ? serverTip : roleTip);
                return;
            }
            LocalSetting.settingData.SelectedServerID = serverInfo.id;
            LocalSetting.settingData.SelectedAvatarDbid = _curSelectAvatarInfo.DBID;
            LocalSetting.Save();
            isSendReq = true;
            EventDispatcher.TriggerEvent<ulong>(LoginEvents.C_START_GAME_REQ, _curSelectAvatarInfo.DBID);
        }

        /// <summary>
        /// 选服返回
        /// </summary>
        /// <param name="serverId"></param>
        private void OnSelectServerReturn(int serverId)
        {
            //LoggerHelper.Debug("注册界面--选择服务器：" + serverId);
            ServerInfo serverInfo = ServerListManager.GetServerInfoByID(serverId);
            if (serverInfo != null)
            {
                _curSelectServerId = serverId;
                SetSelectServerName(serverInfo.name);
            }
            else 
            {
                SetSelectServerName(null);
            }
        }

        private void OnSelectChanged(KList list, int index)
        {
            RoleInfoItem roleInfoItem = list.ItemList[index] as RoleInfoItem;
            AvatarInfo data = (AvatarInfo)roleInfoItem.Data;
            if (data != null)
            { //栏位有角色：选中已有角色   
                _curSelectAvatarInfo = _roleInfoList[index];
                _curSelectServerId = _curSelectAvatarInfo != null ? LocalSetting.settingData.SelectedServerID : 0;
                OnSelectServerReturn(_curSelectServerId);
            }
            else
            { //栏位为空：跳转到创号界面
                _curSelectServerId = 0;
                _curSelectAvatarInfo = null;
                UIManager.Instance.ShowPanel(PanelIdEnum.CreateRole);
            }
        }

        private void OnBack()
        {
            (MogoWorld.Player as PlayerAccount).ExitLoginReq();
        }

        private void onReqTimeout()
        {
            isSendReq = false;
        }
        //====================================================//
    }
}
