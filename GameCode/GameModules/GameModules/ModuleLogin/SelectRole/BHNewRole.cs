﻿#region 模块信息
/*==========================================
// 模块名：BHNewRole
// 命名空间: ModuleLogin
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/06/25
// 描述说明：新建角色组件,负责响应动画控制器事件处理函数
// 其他：
//==========================================*/
#endregion

using ACTSystem;
using GameLoader.Utils;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace ModuleLogin
{
    public class BHNewRole : MonoBehaviour
    {
        [SerializeField]
        private ACTActor _actor;
        
        public ACTActor actor
        {
            get { return _actor; }
            set { _actor = value; }
        }

        #region 动画事件帧相应
        public void Sword(int swordInfo)
        {
            if (swordInfo == 0)
            {
                _actor.equipController.equipWeapon.InCity(true);
                //LoggerHelper.Error("1______________swordInfo:  " + swordInfo);
            }
            else
            {
                _actor.equipController.equipWeapon.InCity(false);
                //LoggerHelper.Error("2______________swordInfo:  " + swordInfo);
            }
            //事件
            EventDispatcher.TriggerEvent<int>(SelectRoleEvents.NEW_ROLE_PLAY_ACTION, swordInfo);
            
        }
        #endregion

    }
}
