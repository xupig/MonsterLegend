﻿#region 模块信息
/*==========================================
// 模块名：SelectRoleBase
// 命名空间: ModuleLogin
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/06/25
// 描述说明：选角,新建角色基类
// 其他： 
//==========================================*/
#endregion

using ACTSystem;
using Common.Data;
using Common.Structs;
using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleLogin
{

    public class SelectRoleBase
    {
        public AvatarInfo avatar;
        private ACTActor _actor = null;
        public ACTActor actor
        {
            get { return _actor; }
            set
            { 
                _actor = value;
                _actorGameObject = _actor.gameObject;
            }
        }

        private SelectRoleMusic _selectRoleMusic = null;
        protected SelectRoleMusic selectRoleMusic
        {
            get { return _selectRoleMusic; }
        }

        private GameObject _actorGameObject = null;
        public GameObject actorGameObject { get { return _actorGameObject; } }

        private bool _isCreated = false;
        public bool isCreated
        {
            get { return _isCreated; }
            set { _isCreated = value; }
        }

        protected bool _visible = true;

        public SelectRoleBase()
        {
            _selectRoleMusic = new SelectRoleMusic();
        }

        public bool IsActorNull(Action<ACTActor> callback = null)
        {
            if (_actor != null)
            {
                if (callback != null) callback(_actor);
                return true;
            }
            return false;
        }

        public virtual void CreateRoleModel(Action<ACTActor> callback = null)
        {
            if (IsActorNull(callback)) return;
            CreateRoleModel((Vocation)avatar.Vocation, "", callback);
        }

        public virtual void CreateRoleModel(Vocation vocation, string equipInfo = "", Action<ACTActor> callback = null)
        {
            if (IsActorNull(callback)) return;
            if (vocation == Vocation.None) return;
            int actorID = role_data_helper.GetModel((int)vocation);
            _isCreated = true;
            GameMain.GlobalManager.ACTSystemAdapter.CreateActor(actorID, (actor) =>
            {
                this.actor = actor;
                if (_actorGameObject == null) return;
                actor.name = "created_" + actor.name;
                //Debug.Log(string.Format("<color=#ffff00>actor.name: {0}</color>",actor.name));
                if (actor == null)
                {
                    GameLoader.Utils.LoggerHelper.Error("CreateActor error in ModelComponent " + actorID);
                    return;
                }
                selectRoleMusic.AddAudioSource("created_" + actorID.ToString(), vocation,SELECTROLE_TYPE.SELECTROLE);
                //穿装备
                if (PutOnEquip() == false)
                {
                    if (_actorGameObject == null) return;
                    //没有数据的话 可以考虑给出默认装备
                    var avatarModelData = ModelEquipTools.CreateAvatarModelData(vocation, equipInfo);
                    ModelEquipTools.ResetActorByAvatarModelData(actor, avatarModelData);
                }
                var controllerPath = role_data_helper.GetCityControllerPath((int)vocation);
                actor.gameObject.SetActive(false);
                SetAnimator(vocation, controllerPath, () =>
                {
                    if (_visible && callback != null) callback(actor);
                });
            });
        }

        public void SetAnimator(Vocation vocation, string controllerPath, Action callback = null)
        {
            var animatorProxy = actor.gameObject.GetComponent<Game.Asset.AnimatorProxy>();
            animatorProxy.SetController(controllerPath, () =>
            {
                if (callback != null)
                {
                    callback();
                }
            });
            actor.equipController.equipWeapon.InCity(true);
           
        }

        public virtual void SetRoleVisible(bool visible,float fadeTime = 1f)
        {
            if (_actor == null) return;
            //_actor.visible = visible;
            _visible = visible;
            _actor.gameObject.SetActive(visible);
            PlayOrStop(visible, fadeTime);
            ResetRotate();
        }

        public virtual bool PutOnEquip()
        {
            int clothID = avatar.avatarModelData.GetEquipInfo(ACTEquipmentType.Cloth).equipID;
            int weaponID = avatar.avatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).equipID;
            int wingID = avatar.avatarModelData.GetEquipInfo(ACTEquipmentType.Wing).equipID;
            if (clothID <= 0 || weaponID <= 0)
            {
                return false;
            }                        
            actor.equipController.equipCloth.PutOn(clothID);
            actor.equipController.equipWeapon.PutOn(weaponID);
            actor.equipController.equipWing.PutOn(wingID);
            return true;
        }

        public void Rotate(Vector3 eulerAngles)
        {
            if (_actor == null) return;
            if (_actor.gameObject.activeSelf == false) return;
            actor.gameObject.transform.Rotate(eulerAngles);
        }

        public void ResetRotate()
        {
            if (_actor == null) return;
            actor.gameObject.transform.eulerAngles = Vector3.zero;
        }

        #region 音乐
        public void PlayOrStop(bool visible,float fadeTime = 1f)
        {
            if (visible == true) 
            { 
                Play(); 
            }
            else 
            {
                Stop(fadeTime); 
            }
        }

        public void Play()
        {
            //LoggerHelper.Error("[SelectRoleBase:Play]=>1__________selectRoleMusic:  " + _selectRoleMusic);
            if (_selectRoleMusic == null) return;
            //LoggerHelper.Error("[SelectRoleBase:Play]=>2_________selectRoleMusic:  " + _selectRoleMusic);
            _selectRoleMusic.Play();
        }

        public void Stop(float fadeTime = 1f)
        {                                                    
            if (_selectRoleMusic == null) return;
            //LoggerHelper.Error("[SelectRoleBase:Stop]=>0_________volume:  ");
            _selectRoleMusic.FadeOut(fadeTime);
        }

        public void RemoveMusicSlot()
        {
            if (_selectRoleMusic == null) return;
            _selectRoleMusic.RemoveMusicSlot();
        }
        #endregion

    }
}
