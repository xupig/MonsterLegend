﻿#region 模块信息
/*==========================================
// 模块名：NewRole
// 命名空间: ModuleLogin
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/06/25
// 描述说明：新建角色信息
// 其他： 
//==========================================*/
#endregion

using ACTSystem;
using Common.Data;
using Common.Structs;
using GameData;
using GameLoader.Utils;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleLogin
{
    /// <summary>
    /// 新角色
    /// </summary>
    public class NewRole : SelectRoleBase
    {

        private ProficientManager _proficientMgr = null;
        public ProficientManager proficientMgr
        {
            get
            {
                return _proficientMgr;
            }
        }

        private Vocation _vocation;

        private NewRoleSkill _skill;
        public int vocation
        {
            get
            {
                return (int)_vocation;
            }
        }

        public NewRole()
            : base()
        {
 
        }

        public override void CreateRoleModel(Vocation vocation, string equipInfo = "", Action<ACTActor> callback = null)
        {
            if (IsActorNull(callback)) return;
            if (vocation == Vocation.None) return;
            int actorID = role_data_helper.GetModel((int)vocation);
            isCreated = true;
            _vocation = vocation;
            if (_proficientMgr == null)
            {
                _proficientMgr = new ProficientManager(this);
            }
            GameMain.GlobalManager.ACTSystemAdapter.CreateActor(actorID, (actor) =>
            {
                this.actor = actor;
                if (actorGameObject == null) return;
                actor.name = "new_" + actor.name;
                
                //Debug.Log(string.Format("<color=#ffff00>actor.name: {0}</color>", actor.name));
                if (actor == null)
                {
                    GameLoader.Utils.LoggerHelper.Error("CreateActor error in ModelComponent " + actorID);
                    return;
                }
                selectRoleMusic.AddAudioSource("new_" + actorID.ToString(), vocation,SELECTROLE_TYPE.CREATEROLE);
                PutOnEquip();
                //actor.equipController.TakeUpWeapon(146705);
                //actor.equipController.PutOnCloth(NewRoleHelper.instance.GetClothID((int)vocation));


                var bhNewRole = actor.gameObject.AddComponent<BHNewRole>();
                bhNewRole.actor = actor;
                //创建技能
                _skill = new NewRoleSkill(this.actor, (int)vocation);
                var controllerPath = NewRoleHelper.instance.GetAnimatorControllerPath(vocation);
                actor.gameObject.SetActive(false);
                SetAnimator(vocation, controllerPath, () =>
                {
                    if (_visible && callback != null) callback(actor);
                });
            });
        }

        public override void SetRoleVisible(bool visible, float fadeTime = 1f)
        {
            base.SetRoleVisible(visible, fadeTime);
            
            if (visible == true)
            {
                EventDispatcher.TriggerEvent<ProficientManager>(SelectRoleEvents.SET_NEWROLE_PROFICIENT, _proficientMgr);
                PlayAction();
            }
            
        }
        private void PlayAction()
        {
            //actor.animationController.PlayAction(75);
            _skill.CastSpell();
        }

        public override bool PutOnEquip()
        {
            actor.equipController.equipCloth.PutOn(NewRoleHelper.instance.GetClothID(vocation));
            actor.equipController.equipWeapon.PutOn(NewRoleHelper.instance.GetWeaponID(vocation));
            actor.equipController.equipWing.PutOn(NewRoleHelper.instance.GetWingID(vocation));
            return true;
        }

        private void EquipAction(int EquipID,Action<int> callBack)
        {
            if (EquipID <= 0) return;
            callBack(EquipID);
        }
    }
}
