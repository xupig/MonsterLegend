﻿#region 模块信息
/*==========================================
// 模块名：SelectRoleManager
// 命名空间: ModuleLogin
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/06/25
// 描述说明：选角,创角管理器
// 其他：
//==========================================*/
#endregion

using ACTSystem;
using Common;
using Common.Events;
using Common.Structs;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;
using GameLoader.Utils;
using GameData;

namespace ModuleLogin
{
    public class SelectRoleCameraInfo
    {
        //private GameObject _cameraGameObject = null;
        //private Camera _camera = null;

        public void CreateCamera()
        {
            /*return; //因为选角场景有相机，所以关闭这个动画相机
            if (_cameraGameObject == null || _camera == null)
            {
                _cameraGameObject = new GameObject("SelectRoleCamera");
                _camera = _cameraGameObject.AddComponent<Camera>();
                _camera.clearFlags = CameraClearFlags.Depth;
                _camera.cullingMask = 1 << LayerMask.NameToLayer("Actor");
                //
                _cameraGameObject.transform.position = new Vector3(0,0.9f,2.75f);
                _cameraGameObject.transform.eulerAngles = new Vector3(0,180,0);

            }*/
        }
    }

    public class SelectRoleManager
    {
        private static SelectRoleManager _instance;
        public static SelectRoleManager Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        /// 已经创建角色
        /// </summary>
        private List<SelectRoleBase> _createdRoles = null;

        /// <summary>
        /// 新角色
        /// </summary>
        private List<SelectRoleBase> _newRoles = null;

        private int _currentCreateRole = -1;
        public int currentCreateRole
        {
            get { return _currentCreateRole; }
            set { _currentCreateRole = value; }
        }

        private int _currentNewRole = -1;
        public int currentNewRole
        {
            get
            {
                return _currentNewRole;
            }
            set
            {
                _currentNewRole = value;
            }
        }

        /// <summary>
        /// 已经创建角色列表
        /// </summary>
        private List<AvatarInfo> _avatarList;     //玩家已注册的角色列表

        private SelectRoleCameraInfo _camera = null;

        private bool _isLoadSelectRoleScene = false;

        private bool isCreateScene = false;

        static SelectRoleManager()
        {
            _instance = new SelectRoleManager();
        }

        public SelectRoleManager()
        {
            _createdRoles = new List<SelectRoleBase>();
            _newRoles = new List<SelectRoleBase>();
            _camera = new SelectRoleCameraInfo();
            AddEvents();
        }

        private void Init()
        {
            isCreateScene = false;
            _isLoadSelectRoleScene = false;
            currentCreateRole = -1;
            currentNewRole = -1;
            _avatarList = null;
            if (_createdRoles != null) _createdRoles.Clear();
            if (_newRoles != null) _newRoles.Clear();
            //if (_avatarList != null) _avatarList.Clear();
            
        }

        private void AddEvents()
        {
            EventDispatcher.AddEventListener<int>(SelectRoleEvents.CLICK_CREATED_ROLE_ITEM, OnSelectRole);
            EventDispatcher.AddEventListener<int>(SelectRoleEvents.CLICK_NEW_ROLE_ITEM, OnNewRole);
            EventDispatcher.AddEventListener(LoginEvents.RETURN_LOGIN, OnReturnLogin);
            EventDispatcher.AddEventListener(SelectRoleEvents.ASK_CREATE_ROLE,OnCreateCreatedRoleSceneCreated);
            EventDispatcher.AddEventListener(SelectRoleEvents.ASK_NEW_ROLE,OnCreateNewRoleSceneCreated);
        }

       
        private void RemoveEvents()
        {
            EventDispatcher.RemoveEventListener<int>(SelectRoleEvents.CLICK_CREATED_ROLE_ITEM, OnSelectRole);
            EventDispatcher.RemoveEventListener<int>(SelectRoleEvents.CLICK_NEW_ROLE_ITEM, OnNewRole);
            EventDispatcher.RemoveEventListener(LoginEvents.RETURN_LOGIN, OnReturnLogin);
            EventDispatcher.RemoveEventListener(SelectRoleEvents.ASK_CREATE_ROLE, OnCreateCreatedRoleSceneCreated);
            EventDispatcher.RemoveEventListener(SelectRoleEvents.ASK_NEW_ROLE, OnCreateNewRoleSceneCreated);
        }

        #region 已经创建角色
        private void OnCreateCreatedRoleSceneCreated()      //场景创建完成_创建
        {
            if (_createdRoles == null) return;
            if (_createdRoles.Count <= 0) return;
            if (_createdRoles[0].isCreated == true)
            {
                return;
            }
            HideCreatedRole(0);
            isCreateScene = true;
        }

        public void CreateCreatedRoleModel(List<AvatarInfo> avatarList, Action<ACTActor> callback = null)        //面板显示后_创建
        {
            LoadSelectRoleScene(GameDefine.STATE_CHOOSE_CHARACTER);
            //Debug.LogError("[CreateCreatedRoleModel]=>22_____________avatarList: " + _avatarList);
            if (_avatarList == null) this._avatarList = avatarList;
            if (avatarList == null)
            {
                GameLoader.Utils.LoggerHelper.Error("[CreateCreatedRoleModel]=>@后端，创建角色信息推送失败");
            }
            //foreach (var item in avatarList)
            //{
            //    Debug.LogError("[CreateCreatedRoleModel]=>Vocation: " + item.Vocation);
            //}

            HideNewRole(-1);
            AddCreateRoleList();
            if (isCreateScene == true) HideCreatedRole(0);
        }

        private void LoadSelectRoleScene(string status)
        {
            if (_isLoadSelectRoleScene == false)
            {
                _isLoadSelectRoleScene = true;
                GameStateManager.GetInstance().SetState(status);
            }
        }

        private void AddCreateRoleList()
        {
            //Debug.LogError("[CreateCreatedRoleModel]=>_createdRoles.Count: " + _createdRoles.Count);
            if (_createdRoles.Count <= 0)
            {
                for (int i = 0; i < _avatarList.Count; i++)
                {
                    CreatedRole selectRole = new CreatedRole();
                    selectRole.avatar = _avatarList[i];
                    _createdRoles.Add(selectRole);
                }
            }
        }
                                     
        public void HideCreatedRole(int roleIndex = -1,float fadeTime = 1f)
        {
            if (roleIndex >= _createdRoles.Count || _createdRoles.Count <= 0) return;
            _createdRoles.ForEach((role) =>
            {
                role.SetRoleVisible(false, fadeTime);
            });
            if (roleIndex != -1)
            {
                //Debug.Log(string.Format("<color=#ffff00>roleIndex:  {0}</color>", roleIndex));
                _createdRoles[roleIndex].CreateRoleModel((actor) => 
                {
                    actor.gameObject.SetActive(true);
                    _createdRoles[roleIndex].PlayOrStop(true);
                });
                currentCreateRole = roleIndex;
            }
            currentNewRole = -1;
        }

        public void CreateRoleRotate(Vector3 eulerAngles)
        {
            if (_createdRoles.Count <= 0) return;
            _createdRoles.ForEach((role) =>
            {
                role.Rotate(eulerAngles);
            });
            
        } 
        #endregion                                        

        #region 新建角色
        private void OnCreateNewRoleSceneCreated()
        {
            if (_newRoles == null) return;
            if (_newRoles.Count <= 0) return;
            if (_newRoles[0].isCreated == true)
            {
                return;
            }
            OnNewRole(0);
            isCreateScene = true;
        }

        public void CreateNewRoleModel(List<create_player> list, Action<ACTActor> callback = null)
        {
            LoadSelectRoleScene(GameDefine.STATE_CREATE_CHARACTER);
            HideCreatedRole(-1);
            AddNewRoleList(list);
            _camera.CreateCamera();
            if (isCreateScene == true) OnNewRole(0);
        }

        private void AddNewRoleList(List<create_player> list)
        {
            if (_newRoles.Count > 0 || list == null) return;
            //for (int i = 0; i < 4; i++)
            for (int i = 0; i < list.Count; i++)
            {
                NewRole selectRole = new NewRole();
                selectRole.avatar = new AvatarInfo();
                //selectRole.avatar.Vocation = i + 1; 
                selectRole.avatar.Vocation = list[i].__vocation;  
                _newRoles.Add(selectRole);
            }
        }

        public void HideNewRole(int roleIndex = -1, float fadeTime = 1f)
        {
            if (_newRoles == null) return;
            if (roleIndex >= _newRoles.Count || _newRoles.Count <= 0) return;
            _newRoles.ForEach((role) =>
            {
                role.SetRoleVisible(false, fadeTime);
            });
            if (roleIndex != -1)
            {
                _newRoles[roleIndex].CreateRoleModel((actor) =>
                {
                    _newRoles[roleIndex].SetRoleVisible(true);
                    currentNewRole = roleIndex;
                });      
            }
            _currentCreateRole = -1;   
        }

        public void NewRoleRotate(Vector3 eulerAngles)
        {
            if (_newRoles.Count <= 0) return;
            _newRoles.ForEach((role) =>
            {
                role.Rotate(eulerAngles);
            });

        } 
        #endregion

        public void OnLoadGameSceneFinish()
        {
            if (GameStateManager.GetInstance().GetState() == GameDefine.STATE_CHOOSE_CHARACTER)
            {
                HideCreatedRole(0);
                isCreateScene = true;
            }
            else if (GameStateManager.GetInstance().GetState() == GameDefine.STATE_CREATE_CHARACTER)
            {
                OnNewRole(0);
                isCreateScene = true;
            }
        }

        #region 事件
        private void OnSelectRole(int roleIndex)
        {
            if (currentCreateRole != roleIndex)
            {
                HideCreatedRole(roleIndex);
            }
        }

        private void OnNewRole(int roleIndex)
        {
            if (currentNewRole == roleIndex) return;
            HideNewRole(roleIndex);
        }

        private void OnReturnLogin()
        {
            Init();
            

            //_newRoles.ForEach((role) =>
            //    {
            //        role = null;
            //    });
            //_newRoles.Clear();
        }
        #endregion

        #region 音乐
        public void RemoveAllMusic()
        {
            _createdRoles.ForEach((role) =>
                {
                    role.RemoveMusicSlot();
                });
            _newRoles.ForEach((role) =>
            {
                role.RemoveMusicSlot();
            });
        }
        #endregion

        #region 专精
        /// <summary>
        /// 返回当前选中角色--技能信息
        /// </summary>
        /// <returns></returns>
        public ProficientManager GetRoleProficient()
        {
            return (_newRoles[currentNewRole] as NewRole).proficientMgr;
        }

        /// <summary>
        /// 返回指定下标的创建角色--技能信息
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public ProficientManager GetRoleProficient(int index)
        {
            return (_newRoles[index] as NewRole).proficientMgr;
        } 
        #endregion
    }
}
