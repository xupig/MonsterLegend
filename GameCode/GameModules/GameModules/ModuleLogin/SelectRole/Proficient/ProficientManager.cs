﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace ModuleLogin
{
    public class ProficientManager : IEnumerable
    {
        private List<ProficientInfo> _proficientList;
        public List<ProficientInfo> proficientList
        {
            get
            {
                return _proficientList;
            }
        }

        public ProficientManager(NewRole  role)
        {
            _proficientList = new List<ProficientInfo>();
            _proficientList.Add(new ProficientInfo(role, 1));
            _proficientList.Add(new ProficientInfo(role, 2));
   
        }


        public IEnumerator GetEnumerator()
        {
            return _proficientList.GetEnumerator();
        }
    }
}
