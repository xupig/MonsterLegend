﻿using ACTSystem;
using Common.Data;
using GameLoader.Utils;
using GameMain.CombatSystem;
using GameMain.GlobalManager;
using MogoEngine;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleLogin
{
    public class NewRoleSkillData   : RoleSkillData
    {
        public int vocation { get; set; }
        
        public override void GetSkillData()
        {
            if (!GameData.XMLManager.create_player.ContainsKey(vocation))
            {
                LoggerHelper.Error("[NewRoleSkillData:NewRoleSkillData]=>has not the vocation:  " + vocation);
                return;
            }
            var data = GameData.XMLManager.create_player[vocation];
            skillEvents = CombatDataTools.ParseSkillEvents(data.__events);
        }

    }

    public class NewRoleSkill : RoleSkill
    {
        public NewRoleSkill(ACTActor actor, int vocation) 
            : base(actor, vocation) 
        {

        }

        public override void GetSkillData()
        {
            if (skillData == null)
            {
                skillData = new NewRoleSkillData();
            }
            (skillData as NewRoleSkillData).vocation = vocation;
            base.GetSkillData();
            //(skillData as NewRoleSkillData).GetSkillData();
        }
    }
}
