﻿using System.Collections.Generic;
using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Events;
using Game.UI;
using UnityEngine;
using ModuleLogin.ChildComponent;
using GameLoader.Config;
using GameLoader.PlatformSdk;
using Common;
using ModuleCommonUI;

namespace ModuleLogin
{
    /// <summary>
    /// 角色职业常量
    /// </summary>
    public enum Carser
    { 
        Warrior  = 1,   //战士   
        Assassin = 2,   //刺客
        Master   = 3,   //法师
        Gunners  = 4,   //枪手
    }

    /// <summary>
    /// 创号界面
    /// </summary>
    public class CreateRolePanel : BasePanel
    {
        //左边控件
        private const int SKILLICON_NUM = 3;     //技能图标数量
        private StateText _labCarser;            //显示职业标签
        private StateIcon _carserIcon;           //职业图标
        private StateText _labCarserDesc;        //职业描述
        private StateText _labBranch;
        private KButton _btnViewDetail;
        private KContainer _carserDuty0;         //显示职业职责1
        private KContainer _carserDuty1;         //显示职业职责2
        private KContainer _skillList;
        private KButton _btnSkillListClose;

        //private KList _pageList;                 //显示职业技能列表
        //private KScrollPage _scrollPage;

        //右边控件
        private StateText _labTips;              //提示信息
        private KInputField _inputRoleName;      //角色名称
        private KButton _btnRandomName;          //随机名字按钮
        private KButton _btnCreate;              //创号按钮
        private KButton _btnBack;                //返回按钮
        private KButton _btnInviteCode;          //邀请码
        private KContainer _nameContainer;
        private KToggleGroup _toggleGroupRole;

        private List<create_player> _roleInfoList;
        private create_player _curSelectRole;              //当前选中角色  
        private KButtonHitArea _btnHitArea;
        private SelectRoleComponent _model;
        private UnityEngine.GameObject goDrag;
        private LoginInviteCodeView _loginInviteCodeView;  //邀请码输入框
        private List<CreateRoleSkillItem> _listSkilItem;
        private List<ProficientInfo> skillDataList;
        private List<ProficientInfo> tempList;

        private int preVocation = 0;                       //上一个选择职业ID
        private bool isSendReq = false;      
        private bool isInputName = false;                             //手动输入账号状态[true:手动输入,false:随机名字]
        private string txt1 = XMLManager.chinese[103038].__content;   //"太短啦，再长一些吧";
 
        protected override void Awake()
        {
            tempList = new List<ProficientInfo>();
            _labCarser = GetChildComponent<StateText>("Container_topLeft/Container_jiaosexinxi/Label_txtZhiye");
            _carserIcon = GetChildComponent<StateIcon>("Container_topLeft/Container_jiaosexinxi/Container_stateIcon");
            //_scrollPage = GetChildComponent<KScrollPage>("Container_topLeft/Container_jiaosexinxi/ScrollPage_jiaosefenlei");
            //_pageList = _scrollPage.GetChildComponent<KList>("mask/content");
            // _pageList.SetGap(0, 0);
            //_pageList.SetPadding(0, 0, 0, 0);
            //_pageList.SetDirection(KList.KListDirection.LeftToRight, 1, 1);
            KContainer container = GetChildComponent<KContainer>("Container_topLeft/Container_jiaosexinxi");
            _skillList = container.GetChildComponent<KContainer>("Container_skillList");
            _listSkilItem = new List<CreateRoleSkillItem>();
            _listSkilItem.Add(_skillList.AddChildComponent<CreateRoleSkillItem>("Container_item0"));
            _listSkilItem.Add(_skillList.AddChildComponent<CreateRoleSkillItem>("Container_item1"));
            _listSkilItem.Add(_skillList.AddChildComponent<CreateRoleSkillItem>("Container_item2"));
            _labCarserDesc = container.GetChildComponent<StateText>("Container_xinxi/Label_txtSkillExplain");
            _labBranch = container.GetChildComponent<StateText>("Container_xinxi/Label_txtKexuanfenzhi");
            _btnViewDetail = container.GetChildComponent<KButton>("Container_xinxi/Button_viewDetail");
            _carserDuty0 = container.GetChildComponent<KContainer>("Container_xinxi/Container_item0");
            _carserDuty1 = container.GetChildComponent<KContainer>("Container_xinxi/Container_item1");
            _carserDuty0.AddChildComponent<IconContainer>("Container_teamDuty");
            _carserDuty1.AddChildComponent<IconContainer>("Container_teamDuty");

            _btnSkillListClose = _skillList.GetChildComponent<KButton>("Button_close");
            _skillList.Visible = false;
            _labCarserDesc.ChangeAllStateText("");
            _labBranch.ChangeAllStateText(MogoLanguageUtil.GetContent(6005020));

            _toggleGroupRole = GetChildComponent<KToggleGroup>("Container_bottomCenter/ToggleGroup_jiaosexuanze");
            _btnBack = GetChildComponent<KButton>("Container_topLeft/Container_panelBg/Button_fanhui");
            _btnInviteCode = GetChildComponent<KButton>("Container_topLeft/Button_invitecode");
            _inputRoleName = GetChildComponent<KInputField>("Container_bottomCenter/Container_right/Input_validateInput");
            _btnRandomName = GetChildComponent<KButton>("Container_bottomCenter/Container_right/Button_shaizi");
            _btnCreate = GetChildComponent<KButton>("Container_bottomCenter/Container_right/Button_kaishiyouxi");
            _labTips = GetChildComponent<StateText>("Container_bottomCenter/Container_right/Label_txtTips");
            _nameContainer = GetChildComponent<KContainer>("Container_bottomCenter/Container_tuijianmingzi");
            _loginInviteCodeView = GetChild("Container_tanchuang").AddComponent<LoginInviteCodeView>();
            _loginInviteCodeView.Visible = false;
            _nameContainer.Visible = false;
            AddDragComponent();
            InitRoleInfo(); 
        }

        private void AddDragComponent()
        {
            goDrag = new UnityEngine.GameObject();
            goDrag.name = "RoleDrag";
            goDrag.transform.SetParent(transform, false);
            goDrag.transform.localPosition = UnityEngine.Vector3.zero;
            goDrag.transform.localEulerAngles = UnityEngine.Vector3.zero;
            goDrag.transform.localScale = UnityEngine.Vector3.one;
            _btnHitArea = goDrag.gameObject.AddComponent<KButtonHitArea>();
            _model = AddChildComponent<SelectRoleComponent>(goDrag.name);
            _model.dragCallBack = SelectRoleManager.Instance.NewRoleRotate;
            _model.Dragable = false;
        }

        private void AddEventListener()
        {
            _inputRoleName.onValueChange.AddListener(OnValueChange);
            _btnRandomName.onClick.AddListener(OnRandomName);
            _btnCreate.onClick.AddListener(OnCreateRole);
            _btnBack.onClick.AddListener(OnBack);
            _btnInviteCode.onClick.AddListener(OnOpenInviteCode);   //打开邀请码输入框
            _btnSkillListClose.onClick.AddListener(OnCloseSkillList);
            _btnViewDetail.onClick.AddListener(OnViewDetail);
            _toggleGroupRole.onSelectedIndexChanged.AddListener(OnToggleGroupChanged);
            EventDispatcher.AddEventListener<string>(LoginEvents.RANDOM_NAME_RESP, OnRandomNameResp);
            EventDispatcher.AddEventListener<ProficientManager>(SelectRoleEvents.SET_NEWROLE_PROFICIENT, OnProficient);
            EventDispatcher.AddEventListener<int>(SelectRoleEvents.NEW_ROLE_PLAY_ACTION,OnNewRole);
            EventDispatcher.AddEventListener<ushort>(SelectRoleEvents.S_CREATE_ROLE_FAIL, OnCreateRoleFail);
            EventDispatcher.AddEventListener(SelectRoleEvents.C_SERVER_REQ_TIMEOUT, onReqTimeout);
        }

        private void RemoveEventListener()
        {
            _inputRoleName.onValueChange.RemoveListener(OnValueChange);
            _btnRandomName.onClick.RemoveListener(OnRandomName);
            _btnCreate.onClick.RemoveListener(OnCreateRole);
            _btnBack.onClick.RemoveListener(OnBack);
            _btnInviteCode.onClick.RemoveListener(OnOpenInviteCode);
            _btnSkillListClose.onClick.RemoveListener(OnCloseSkillList);
            _btnViewDetail.onClick.RemoveListener(OnViewDetail);
            _toggleGroupRole.onSelectedIndexChanged.RemoveListener(OnToggleGroupChanged);
            EventDispatcher.RemoveEventListener<string>(LoginEvents.RANDOM_NAME_RESP, OnRandomNameResp);
            EventDispatcher.RemoveEventListener<ProficientManager>(SelectRoleEvents.SET_NEWROLE_PROFICIENT, OnProficient);
            EventDispatcher.RemoveEventListener<int>(SelectRoleEvents.NEW_ROLE_PLAY_ACTION, OnNewRole);
            EventDispatcher.RemoveEventListener<ushort>(SelectRoleEvents.S_CREATE_ROLE_FAIL, OnCreateRoleFail);
            EventDispatcher.RemoveEventListener(SelectRoleEvents.C_SERVER_REQ_TIMEOUT, onReqTimeout); 
        }
        
        //角色初始化表转成列表
        private void InitRoleInfo()
        {
            _roleInfoList = new List<create_player>();
            foreach (create_player item in XMLManager.create_player.Values)   //XMLManager.role_data.Values
            {
                _roleInfoList.Add(item);
            }
            _roleInfoList.Sort(Sort);

            int iconId = 0;
            KToggle toggle;
            for (int i = 0; i < _roleInfoList.Count; i++)  
            {
                toggle = _toggleGroupRole.GetToggleList()[i];
                iconId = GetHeadIconId((Carser)_roleInfoList[i].__vocation);   //MogoPlayerUtils.GetSmallIcon(_roleInfoList[i].__vocation);
                SetIcon(toggle, iconId);
                if (i == 3) toggle.enabled = false;   //最后栏位未开放
            }
        }

        //设置toggle图标
        private void SetIcon(KToggle toggle, int iconId)
        {
            if (toggle == null) return;
            toggle.GetChildComponent<StateIcon>("back/stateIcon").SetIcon(iconId);
            toggle.GetChildComponent<StateIcon>("checkmark/stateIcon").SetIcon(iconId);
        }

        //根据职业--取得职业头像
        private int GetHeadIconId(Carser vocation)
        {
            if (vocation == Carser.Warrior)  return 629;  //战士
            if (vocation == Carser.Assassin) return 630;  //刺客
            if (vocation == Carser.Master)   return 631;  //法师
            if (vocation == Carser.Gunners)  return 632;  //枪手
            return -1;
        }

        // 临时根据策划需求做了按钮位置调整
        private void ResetTogglePos()
        {
            var toggleList = _toggleGroupRole.GetToggleList();
            Vector3 bakPos = toggleList[3].transform.position;
            toggleList[3].transform.position = toggleList[1].transform.position;
            toggleList[1].transform.position = toggleList[2].transform.position;
            toggleList[2].transform.position = bakPos;
        }

        private int Sort(create_player a, create_player b)
        {
            if (a.__sort < b.__sort) return -1;
            if (a.__sort > b.__sort) return 1;
            return 0;
        }

        private void VisiableDrag(bool b)
        {
            if (goDrag != null) goDrag.SetActive(b);
        }

        public void SetTip(string msg)
        {
            _labTips.CurrentText.text = string.IsNullOrEmpty(msg) ? string.Empty : msg;
        }

        public override void OnShow(object data)
        {
            SetTip("");
            isSendReq = false;
            _curSelectRole = null;
            isInputName = false;
            SetRoleInfo(0);
            RandomNameReq();
            VisiableDrag(true);
            AddEventListener();
            SelectRoleManager.Instance.CreateNewRoleModel(_roleInfoList);
            _toggleGroupRole.SelectIndex = 0;
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.CreateRole; }
        }

        public override void OnClose()
        {
            SelectRoleManager.Instance.HideNewRole(-1,3f);
            RemoveEventListener();
            ClosePanel();
        }

        //====================  控件响应处理  ====================//
        /// <summary>
        /// 获取随机名称--请求
        /// </summary>
        private void OnRandomName()
        {
            isInputName = false;
            preVocation = 0;
            RandomNameReq();
        }

        //随机名字请求
        private void RandomNameReq()
        {
            //vocation=1|3(战士|法师)时，职业对应着性别男(1)；vocation=2|4(刺客|枪手)时，职业对应着性别女(2)
            int curVocation = _curSelectRole != null ? _curSelectRole.__vocation : 0;
            //LoggerHelper.Info("====  curVocation:" + curVocation + ",preVocation:" + preVocation);
            if (preVocation == 0 ||
                ((preVocation == (int)Carser.Warrior || preVocation == (int)Carser.Master) && (curVocation == (int)Carser.Assassin || curVocation == (int)Carser.Gunners)) ||
                ((preVocation == (int)Carser.Assassin || preVocation == (int)Carser.Gunners) && (curVocation == (int)Carser.Warrior || curVocation == (int)Carser.Master)))
            {//性别有变化时，才随机名称请求
                (MogoWorld.Player as PlayerAccount).RandomNameReq(_curSelectRole != null ? _curSelectRole.__vocation : (int)Carser.Warrior);
            }
        }

        private bool IsValidChar(string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if (_inputRoleName.textComponent.font.HasCharacter(str[i]) == false)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 创建角色
        /// </summary>
        private void OnCreateRole()
        {
            if (isSendReq) return;
            if (IsValidChar(_inputRoleName.text) == false)
            {
                //含有不能显示字符提示
                //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74888), PanelIdEnum.MainUIField);
                return;
            }
                
            //角色名字控制在2~7个字符之间
            string roleName = _inputRoleName.text.Trim();
            if (roleName.Length < 1) { SetTip(txt1); return; }
            //if (roleName.Length > 7) { SetTip(txt2); return; }

            SetTip("请求创角中...");
            isSendReq = true;
            LocalSetting.settingData.SelectedCharacter = roleName;  //临时存储角色名称
            LoggerHelper.Info(string.Format("创号请求 用户名:{0},职业:{1}", _inputRoleName.text, _curSelectRole.__vocation));
            
            //这里有个特殊情况：如果是创角，选服日志发送一定在创角日志前面发送给SDK
            PlatformSdkMgr.Instance.SelectServerLog("1", roleName, LocalSetting.settingData.SelectedServerID.ToString());
            LoggerHelper.Info(string.Format("LogForSelectServer uid:{0},serverId:{1},roleName:{2},roleLevel:{3}", LoginInfo.platformUid, LocalSetting.settingData.SelectedServerID, roleName, 1));
            #if UNITY_IPHONE && !UNITY_EDITOR
                 IOSPlugins.LogForSelectServer(LoginInfo.platformUid, LocalSetting.settingData.SelectedServerID.ToString(), roleName, "1");
            #endif
            EventDispatcher.TriggerEvent<string, byte, byte>(LoginEvents.C_CREATE_ROLE, _inputRoleName.text, 1, (byte)_curSelectRole.__vocation);
        }

        /// <summary>
        /// 底部职业图标组--切换响应
        /// </summary>
        /// <param name="group"></param>
        /// <param name="index"></param>
        private void OnToggleGroupChanged(KToggleGroup group, int index)
        {
            SetRoleInfo(index);
            if (!isInputName) RandomNameReq();
            EventDispatcher.TriggerEvent<int>(SelectRoleEvents.CLICK_NEW_ROLE_ITEM, index);
        }

        //账号输入有变动
        private void OnValueChange(string value)
        {
            //LoggerHelper.Debug("==== value:" + value + ",isInputName:" + isInputName + ",isFocused:" + _inputRoleName.isFocused);
            if (_inputRoleName.isFocused) isInputName = string.IsNullOrEmpty(value) ? false : true;
        }

        /// <summary>
        /// 设置选择职业信息
        /// </summary>
        /// <param name="index"></param>
        private void SetRoleInfo(int index)
        {
            preVocation = _curSelectRole != null ? _curSelectRole.__vocation : 0;                    //前一个选择职业ID
            _curSelectRole = _roleInfoList[index];
            int iconId = RoleInfoItem.carserIconId(_curSelectRole.__vocation);
            _carserIcon.SetIcon(iconId);                                                             //设置职业图标 
            _labCarser.CurrentText.text = XMLManager.chinese[_curSelectRole.__vocation].__content;   //设置职业名称
            //LoggerHelper.Debug("设置职业:" + _curSelectRole.__vocation + ",图标:" + iconId);
        }

        /// <summary>
        /// 随机名称返回
        /// </summary>
        /// <param name="name"></param>
        private void OnRandomNameResp(string name)
        {
            //LoggerHelper.Info("[UI-OnRandomNameResp] RandomName Resp name:" + name);
            _inputRoleName.text = string.IsNullOrEmpty(name) ? "" : name;
        }

        /// <summary>
        /// 返回按钮
        /// </summary>
        private void OnBack()
        {
            PlayerAccount account = (PlayerAccount)MogoWorld.Player;
            if (account.HasCharacter())
            {//有创角记录，返回选择角色界面
                UIManager.Instance.ClosePanel(PanelIdEnum.CreateRole);
                UIManager.Instance.ShowPanel(PanelIdEnum.SelectRole);
            }
            else 
            {//没有创角记录,发送退出登陆请求，成功后，返回登陆界面
                account.ExitLoginReq();
            }
        }

        //打开邀请码输入框界面
        private void OnOpenInviteCode()
        {
            LoggerHelper.Debug("打开邀请码输入框");
            VisiableDrag(false);
            _loginInviteCodeView.Open(goDrag);
        }

        //创建角色失败
        private void OnCreateRoleFail(ushort errorId)
        {
            isSendReq = false;
            SetTip(LoginManager.Instance.getRegisterTip((int)errorId));
        }

        private void OnCloseSkillList()
        {
            _skillList.Visible = false;
        }

        private void OnViewDetail()
        {
            RefreshSkillList();
            _skillList.Visible = skillDataList != null;
        }

        //请求发送超时
        private void onReqTimeout()
        {
            isSendReq = false;
        }

        private void RefreshSkillList()
        {
            if (skillDataList != null)
            {
                int index = 0;
                foreach (CreateRoleSkillItem item in _listSkilItem)
                {
                    item.Data = index < skillDataList.Count ? skillDataList[index] : null;
                    index++;
                }
            }
        }

        /// <summary>
        /// 刷新选中职业的队伍职责信息
        /// </summary>
        /// <param name="carserSkillList"></param>
        private void RefreshVocationDutyList(List<ProficientInfo> carserSkillList)
        {
            //_scrollPage.CurrentPage = 0;
            //_scrollPage.TotalPage = carserSkillList.Count;
            //_pageList.SetDataList<CreateRoleSkillItem>(carserSkillList);
            skillDataList = carserSkillList;
            GetVocationDutyList(carserSkillList);
            ProficientInfo data = carserSkillList[0];
            _labCarserDesc.CurrentText.text = data.GetVocationDesc();
            if (tempList.Count > 0)
            {
                SetVocationDutyInfo(_carserDuty0, tempList[0]);
                _carserDuty0.Visible = true;
            }
            else 
            {
                _carserDuty0.Visible = false;
            }

            if (tempList.Count > 1)
            {
                SetVocationDutyInfo(_carserDuty1, tempList[1]);
                _carserDuty1.Visible = true;
            }
            else
            {
                _carserDuty1.Visible = false;
            }
        }

        //设置队伍职业信息
        private void SetVocationDutyInfo(KContainer container,ProficientInfo data)
        {
            StateText labName = container.GetChildComponent<StateText>("Label_txtZhiye");
            StateText labDesc = container.GetChildComponent<StateText>("Label_txtZhiye02");
            labName.ChangeAllStateText(data.GetVocationDutyName());
            labDesc.ChangeAllStateText(data.GetVocationDutyDesc());
            container.GetChildComponent<IconContainer>("Container_teamDuty").SetIcon(data.GetVocationDutyIconId());
        }

        private List<ProficientInfo> GetVocationDutyList(List<ProficientInfo> skillList)
        {
            int index = 0;
            tempList.Clear();
            foreach (ProficientInfo item in skillList)
            {
                if (index != item.GetVocationDutyNameId())
                {
                    tempList.Add(item);
                }
            }
            return tempList;
        }

        private void OnNewRole(int swordInfo)
        {
            _model.Dragable = swordInfo == 0 ? true : false;
        }

        private void OnProficient(ProficientManager proficientMgr)
        {
            RefreshVocationDutyList(proficientMgr.proficientList);
            if (_skillList != null && _skillList.Visible) RefreshSkillList();
        }
    }
}
