﻿using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using Common.Base;
using Common;
using Common.Events;
using MogoEngine.RPC;
using GameLoader.Utils.Timer;
using GameLoader.Utils;
using MogoEngine.Events;
using GameLoader.Config;

namespace ModuleLogin
{
    /// <summary>
    /// 登陆模块--控制类
    /// </summary>
    public class LoginModule : BaseModule
    {
        public LoginModule() : base()
        {
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.Login, "Panel_Login", MogoUILayer.LayerUIPanel, "ModuleLogin.LoginPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);                       //登陆界面
            AddPanel(PanelIdEnum.Server, "Container_SelectServerPanel", MogoUILayer.LayerUIPanel, "ModuleLogin.SelectServerPanel", BlurUnderlay.Have, HideMainUI.Hide);                    //选服界面
            AddPanel(PanelIdEnum.Notice, "Container_NoticePanel", MogoUILayer.LayerUIPanel, "ModuleLogin.NoticePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, true);                    //选服界面

            AddPanel(PanelIdEnum.SelectRole, "Container_SelectRolePanel", MogoUILayer.LayerUIPanel, "ModuleLogin.SelectRolePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);        //选角登陆
            AddPanel(PanelIdEnum.CreateRole, "Container_CreateRolePanel", MogoUILayer.LayerUIPanel, "ModuleLogin.CreateRolePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);        //创号界面
            AddPanel(PanelIdEnum.ServerLine, "Container_ServerLinePanel", MogoUILayer.LayerUITop, "ModuleLogin.ServerLinePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, true);           //服务器排队界面
            //RegisterPopPanelBunchPanel(PanelIdEnum.PlatformLogin, PanelIdEnum.Login);
        } 

    }
}
