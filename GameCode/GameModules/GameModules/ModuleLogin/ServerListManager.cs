﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using UnityEngine;
using System.Linq;

using GameLoader.IO;
using GameLoader.Utils.XML;
using GameLoader.Utils;
using ICSharpCode.SharpZipLib.GZip;
using System.Security;
using System.Xml;
using GameLoader.Utils.Timer;
using GameData;
using MogoEngine.Events;
using Common.Events;
#if UNITY_WEBPLAYER
using Init.Update;
#endif

namespace GameLoader.Config
{
    /// <summary>
    /// 服务器信息类
    /// </summary>
	public class ServerInfo
	{
        public int id;
        public string name;
        public int flag;            //服务器运行状态
        public string ip;
        public int port;
        public string text;
        public int is_new;          //是否为新服
        public int is_recommend;    //是否为推荐服
        public string detail;       //运行状态说明
        public int is_ios_show;     //IOS是否显示该服务器 1-不显示 0-显示

		public string GetInfo()
		{
			if (text != null && text != string.Empty)
			{
				return text;
			}
			if (flag == (int)ServerType.Close)
			{
				return "server close";
			}
            if (flag == (int)ServerType.Weihu)
			{
				return "maintaining...";
			}
			return string.Empty;
			
		}
		
        public bool CanLogin()
		{
            return (!(flag == (int)ServerType.Close) || (flag == (int)ServerType.Weihu));
		}
	}

    /// <summary>
    /// 服务器状态常量
    /// </summary>
	public enum ServerType : int
	{
		Hot      = 1,     //爆满
		Mang     = 2,      //繁忙
        Normal   = 3,     //流畅
        Weihu    = 4,     //维护中
		Close    = 5      //已关闭
	}

    /// <summary>
    /// 服务信息管理类
    /// </summary>
	public class ServerListManager
	{
        private static uint timerId = 0;
        private static int downloadDelayTime = 30 * 1000;            //服务器列表文件下载时间间隔(单位:毫秒)
		static Dictionary<int,ServerInfo> m_Servers;
        public static void Init(Action callback = null)
		{
            if (m_Servers != null)
            {
                if (callback != null) callback();
            }
            else
            {
                //从全局配表读取下载时间间隔
                downloadDelayTime = XMLManager.global_params.ContainsKey(219) ? int.Parse(XMLManager.global_params[219].__value) : 30 * 1000;
                LoadServerList(callback);
            }
		}

        /// <summary>
        /// 返回所有服务器信息
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, ServerInfo>  GetAllServer()
        {
            return m_Servers;
        }

        /// <summary>
        /// 根据服务器ID--取得服务器信息
        /// </summary>
        /// <param name="nID"></param>
        /// <returns></returns>
        public static ServerInfo GetServerInfoByID(int nID)
        {
            if (m_Servers != null && m_Servers.ContainsKey(nID))
                return m_Servers[nID];
            else
                return null;
        }

        public static bool IfServerInfoExist(int nID)
		{
            if (m_Servers != null && m_Servers.ContainsKey(nID))
                return true;
            else
			    return false;
		}

		public static ServerInfo GetRecommentServer()
		{
            if (m_Servers == null) return null;
			foreach (var item in m_Servers)
			{//推荐服
				if (item.Value.is_recommend == 1)
					return item.Value;
			}

            foreach (var item in m_Servers)
			{
                if (item.Value.flag == (int)ServerType.Hot)
                    return item.Value;
			}
            foreach (var item in m_Servers)
			{
                if (item.Value.flag == (int)ServerType.Normal || item.Value.flag == 0)
                    return item.Value;
			}
            foreach (var item in m_Servers)
			{
                if (item.Value.flag == (int)ServerType.Weihu)
                    return item.Value;
			}
            foreach (var item in m_Servers)
			{
                if (item.Value.flag == (int)ServerType.Close)
                    return item.Value;
			}
			
			return null;
		}

		public static void LoadServerList(Action callback = null)
		{
#if UNITY_WEBPLAYER
            LoadServerListForWeb(callback);
#else
            LoadServerListForLocal(callback);
#endif
        }

#if UNITY_WEBPLAYER
        public static void LoadServerListForWeb(Action callback = null)
        {
            var url = SystemConfig.ServerUrl;
            HttpWrappr.instance.LoadWwwText(SystemConfig.GetRandomUrl(url), (loadedurl, content) => {
                m_Servers = DecodeXMLDataByContent(content, typeof(Dictionary<int, ServerInfo>), typeof(ServerInfo)) as Dictionary<int, ServerInfo>;
                PrintServer();
                if (callback != null) GameLoader.Utils.Timer.TimerHeap.AddTimer(0, 0, callback);
            });
        }
#endif

        public static void LoadServerListForLocal(Action callback = null)
        {
            try
            {
                Action action = () =>
                {
                    string path = string.Concat(SystemConfig.RuntimeResourcePath, "Config/localServers.xml");
                    if (File.Exists(path))
                    {
                        LoggerHelper.Info("Local Load server.xml");
                        ConstString.ServerListPath = path; 
                    }
                    else
                    {
                        LoggerHelper.Info("Web Load server.xml");
                        var url = SystemConfig.ServerUrl;
                        if (!String.IsNullOrEmpty(url))
                        {
                            if (File.Exists(ConstString.ServerListPath))
                            {
                                File.Delete(ConstString.ServerListPath);
                            }
                            GameLoader.Mgrs.DownloadMgr.SyncDownloadFile(SystemConfig.GetRandomUrl(url), ConstString.ServerListPath);
                        }
                    }
                    m_Servers = DecodeXMLData(ConstString.ServerListPath, typeof(Dictionary<int, ServerInfo>), typeof(ServerInfo)) as Dictionary<int, ServerInfo>;
                    PrintServer();
                    if (callback != null) GameLoader.Utils.Timer.TimerHeap.AddTimer(0, 0, callback);
                };
                action.BeginInvoke(null, null);
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        public static Dictionary<Int32, Dictionary<String, String>> xml2Dic(System.Security.SecurityElement xml)
        {
            Dictionary<Int32, Dictionary<String, String>> result = new Dictionary<Int32, Dictionary<String, String>>();
            int index = 0;
            foreach (SecurityElement subMap in xml.Children)
            {
                index++;
                if (subMap.Children == null || subMap.Children.Count == 0)
                {
                    Console.WriteLine("empty row in row NO." + index + " of ");
                    continue;
                }
                SecurityElement se = subMap.Children[0] as SecurityElement;
                if (!se.Tag.StartsWith("id"))
                {
                    Console.WriteLine(String.Format("First element '{0}' not name as 'id_i'", se.Tag));
                }
                Int32 key = Int32.Parse(se.Text);
                if (result.ContainsKey(key))
                {
                    Console.WriteLine(String.Format("Key {0} already exist", key));
                    continue;
                }

                Dictionary<String, String> children = new Dictionary<String, String>();
                result.Add(key, children);
                for (int i = 1; i < subMap.Children.Count; i++)
                {
                    SecurityElement node = subMap.Children[i] as SecurityElement;
                    string tag;
                    if (node.Tag.Length < 3)
                    {
                        tag = node.Tag;
                    }
                    else
                    {
                        string tagTial = node.Tag.Substring(node.Tag.Length - 2, 2);
                        if (tagTial == "_i" || tagTial == "_s" || tagTial == "_f"
                            || tagTial == "_l" || tagTial == "_k" || tagTial == "_m"
                            || tagTial == "_y" || tagTial == "_n")
                            tag = node.Tag.Substring(0, node.Tag.Length - 2);
                        else
                            tag = node.Tag;
                    }

                    if (node != null && !children.ContainsKey(tag))
                    {
                        if (String.IsNullOrEmpty(node.Text))
                            children.Add(tag, "");
                        else
                            children.Add(tag, node.Text.Trim());
                    }
                    else
                        Console.WriteLine(String.Format("Key {0} already exist, index {1} of {2}.", node.Tag, i, node.ToString()));
                }
            }
            return result;
        }

        static object DecodeXMLDataByContent(string Content, Type dicType, Type T)
        {
            object result = null;
            Dictionary<Int32, Dictionary<String, String>> map = xml2Dic(Content.ToXMLSecurityElement());
            result = dicType.GetConstructor(Type.EmptyTypes).Invoke(null);
            System.Reflection.FieldInfo[] fis = T.GetFields();
            foreach (KeyValuePair<Int32, Dictionary<String, String>> item in map)
            {
                object t = T.GetConstructor(Type.EmptyTypes).Invoke(null);
                foreach (System.Reflection.FieldInfo fi in fis)
                {
                    if (fi.Name == "id")
                    {
                        fi.SetValue(t, item.Key);
                    }
                    else
                    {
                        var temp = fi.Name;
                        if (item.Value.ContainsKey(temp))
                        {
                            object value = GameData.XMLDataDecoder.GetValue(item.Value[temp], fi.FieldType);
                            fi.SetValue(t, value);
                        }
                    }
                }
                dicType.GetMethod("Add").Invoke(result, new object[] { item.Key, t });
            }
            return result;
        }

        static object DecodeXMLData(string fileName, Type dicType, Type T)
        {
            object result = null;
            try
            {
                string content = "";
                if (System.IO.File.Exists(fileName))
                {
                    using (System.IO.FileStream fs = System.IO.File.OpenRead(fileName))
                    {
                        System.IO.StreamReader sr = new System.IO.StreamReader(fs);
                        content = sr.ReadToEnd();
                    }
                }
                return DecodeXMLDataByContent(content, dicType, T);
            }
            catch (Exception ex)
            {
                Console.WriteLine("format error : " + fileName + " ex " + ex.Message);
            }
            return result;
        }

		public static List<String> GetServerNames()
		{
            List<string> serverNames = new List<string>();
            if (m_Servers == null) return serverNames;
            foreach (var kvp in m_Servers)
            {
                serverNames.Add(kvp.Value.name);
            }
            return serverNames;
		}

        public static ServerInfo GetSelectedInfoByName(string strServerName)
        {
            if (m_Servers == null) return null;
            foreach (var kvp in m_Servers)
            {
                if (kvp.Value.name == strServerName)
                {
                    return kvp.Value;
                }
            }
            return null;
        }

		public static string DownloadGzipFile(string url)
		{
			HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
			wr.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";
			HttpWebResponse response = (HttpWebResponse)wr.GetResponse();
			if (response.ContentEncoding.ToLower().Contains("gzip"))
			{
				GZipInputStream gzi = new GZipInputStream(response.GetResponseStream());
				MemoryStream re = new MemoryStream();
				int count = 0;
				byte[] data = new byte[4096];
				
				while ((count = gzi.Read(data, 0, data.Length)) != 0)
				{
					re.Write(data, 0, count);
				}
				byte[] depress = re.ToArray();
				return Encoding.UTF8.GetString(depress);
			}
			else
			{
				Stream s = response.GetResponseStream();
				MemoryStream re = new MemoryStream();
				int count = 0;
				byte[] data = new byte[4096];
				
				while ((count = s.Read(data, 0, data.Length)) != 0)
				{
					re.Write(data, 0, count);
				}
				byte[] depress = re.ToArray();
				return Encoding.UTF8.GetString(depress);
			}
		}


        /// <summary>
        /// 开启每隔delayTime秒加载servers.xml文件
        /// </summary>
        public static void StartTimer()
        {
            if (timerId > 0) return;
            timerId = TimerHeap.AddTimer((uint)downloadDelayTime, downloadDelayTime, OnLoadServerFile);
            LoggerHelper.Info("Start servers.xml download [Start]");
        }

        private static void OnLoadServerFile()
        {
            LoadServerList(OnLoadServerFileFinish);
        }

        //加载完成
        private static void OnLoadServerFileFinish()
        {
            //通知刷新正在打开的服务器界面
            EventDispatcher.TriggerEvent(LoginEvents.C_REFRESH_SERVER_LIST);
        }

        /// <summary>
        /// 停止每隔delayTime秒加载servers.xml文件
        /// </summary>
        public static void StopTimer()
        {
            if (timerId > 0) TimerHeap.DelTimer(timerId);
            timerId = 0;
            LoggerHelper.Info("Stop servers.xml download [OK]");
        }

        private static void PrintServer()
        {
            if (m_Servers == null) return;
            foreach (ServerInfo item in m_Servers.Values)
            {
                if (item.flag == (int)ServerType.Weihu || item.flag == (int)ServerType.Close) LoggerHelper.Info(string.Format("serverId:{0},flag:{1}", item.id, item.flag));
            }
        }
	}
}