﻿using Common.Base;
//using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;

namespace ModuleLogin.ChildComponent
{
    /// <summary>
    /// 选角界面--左边角色信息视图类
    /// </summary>
    public class RoleInfoItem : KList.KListItemBase
    {
        private AvatarInfo _data;
        private StateText _onName;
        private StateText _closeLabel;
        private StateText _closeName;
        //private StateIcon _onIcon;
        //private StateIcon _closeIcon;
        private string _textCreateRole = MogoLanguageUtil.GetContent(6006006);  //+创建角色

        private KButton _btnCheckmark;
        private KButton _btnBack;

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as AvatarInfo;
                Refresh();
            }
        }

        public override void Dispose()
        {
            _data = null;
        }

        protected override void Awake()
        {
            _btnCheckmark = GetChildComponent<KButton>("Button_checkmark");
            _btnBack = GetChildComponent<KButton>("Button_jiaose");
            _onName = _btnCheckmark.GetChildComponent<StateText>("name");
            //_onIcon = _btnCheckmark.GetChildComponent<StateIcon>("stateIcon");

            _closeLabel = _btnBack.GetChildComponent<StateText>("label");
            _closeName = _btnBack.GetChildComponent<StateText>("name");
            //_closeIcon = _btnBack.GetChildComponent<StateIcon>("stateIcon");
            Refresh();
            IsSelected = false;
        }

        public override void Show()
        {
            AddEventListener();
            base.Show();
        }

        public override void Hide()
        {
            RemvoeEventListener();
            base.Hide();
        }

        private void AddEventListener()
        {
            _btnCheckmark.onClick.AddListener(OnClick);
            _btnBack.onClick.AddListener(OnClick);
        }

        private void RemvoeEventListener()
        {
            _btnCheckmark.onClick.RemoveListener(OnClick);
            _btnBack.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            onClick.Invoke(this, Index);
            if (_data == null) UIManager.Instance.ClosePanel(PanelIdEnum.SelectRole); //_data==null:表示创建角色，挑战到创建角色界面
            EventDispatcher.TriggerEvent<int>(SelectRoleEvents.CLICK_CREATED_ROLE_ITEM, Index);
        }

        public override bool IsSelected
        {
            get
            {
                return base.IsSelected;
            }
            set
            {
                base.IsSelected = value;
                _btnCheckmark.Visible = value;
                _btnBack.Visible = !value;
            }
        }

        protected void Refresh()
        {
            if (_data != null)
            {
                string strName = string.Format("LV{0}  {1}", _data.Level, _data.Name); 
                _closeLabel.ChangeAllStateText("");
                _closeLabel.CurrentText.text = "";
                int iconId = carserIconId(_data.Vocation);
                if (iconId > 0)
                {//显示职业图标
                    //_onIcon.SetIcon(iconId);
                    //_closeIcon.SetIcon(iconId);
                }

                _onName.ChangeAllStateText(strName);
                _closeName.ChangeAllStateText(strName);
                _onName.CurrentText.text = strName;
                _closeName.CurrentText.text = strName;
            }
            else
            {
                _closeLabel.ChangeAllStateText(_textCreateRole);
                _closeLabel.CurrentText.text = _textCreateRole;

                _onName.ChangeAllStateText("");
                _closeName.ChangeAllStateText("");
                _onName.CurrentText.text = "";
                _closeName.CurrentText.text = "";
            }
        }

        ////vocation=1|3(战士|法师)时，职业对应着性别男(1)；vocation=2|4(刺客|枪手)时，职业对应着性别女(2)
        public static int carserIconId(int vocation)
        {
            if (vocation == (int)Carser.Warrior) return 625;    //战士图标
            if (vocation == (int)Carser.Gunners) return 626;    //枪手图标
            if (vocation == (int)Carser.Master)  return 627;    //法师图标
            return -1;
        }
    }
}
