﻿using Common.Base;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using UnityEngine;

namespace ModuleLogin
{
    /// <summary>
    /// 登录界面中的--邀请码界面
    /// </summary>
    public class LoginInviteCodeView : KList.KListItemBase
    {
        private string _data;
        private KButton _btnOk;
        private KButton _btnClose;
        private StateText _txtBtnOk;
        private StateText _txtTitle;
        private StateText _txtExplain;
        private KInputField _inputInviteCode;
        private GameObject goDrag;

        //private const string txt1 = "好友邀请";
        //private const string txt5 = "请输入好友的邀请码";
        //private string txt4 = XMLManager.chinese[84604].__content;  //"邀    请";
        //private string txt3 = XMLManager.chinese[84603].__content;  //"成为好友可获得丰富奖励哦";
        private string txt5 = "";

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as string;
            }
        }

        public override void Dispose()
        {
            _data = null;
        }

        protected override void Awake()
        {
            _btnClose = GetChildComponent<KButton>("Button_close");
            _btnOk = GetChildComponent<KButton>("Button_queding");
            _txtBtnOk = _btnOk.GetChildComponent<StateText>("label");
            _txtTitle = GetChildComponent<StateText>("Label_txtTitle");
            _txtExplain = GetChildComponent<StateText>("Label_txtInviteCodeLab");
            _inputInviteCode = GetChildComponent<KInputField>("Input_inviteCode");
            txt5 = _inputInviteCode.text;
            //_txtBtnOk.ChangeAllStateText(txt4);
            //_txtTitle.CurrentText.text = txt1;
            //_txtExplain.CurrentText.text = txt3;
            //_inputInviteCode.text = txt5;
        }

        public void Open(GameObject goDrag)
        {
            this.goDrag = goDrag;
            AddEventListener();
            Visible = true;
        }

        public override void Hide()
        {
            if (goDrag != null) goDrag.SetActive(true);
            RemvoeEventListener();
            Visible = false;
        }

        private void AddEventListener()
        {
            _btnClose.onClick.AddListener(OnClose);
            _btnOk.onClick.AddListener(OnClickOk);
        }

        private void RemvoeEventListener()
        {
            _btnClose.onClick.RemoveListener(OnClose);
            _btnOk.onClick.RemoveListener(OnClickOk);
        }

        //确定
        private void OnClickOk()
        {
            string code = _inputInviteCode.text;
            if (string.IsNullOrEmpty(code) || code.Equals(txt5))
            {
                //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, txt5, PanelIdEnum.MainUIField);
                return;
            }

            WelfareActiveManager.Instance.inputInviteCode = code;
            //LoggerHelper.Debug("输入邀请码:" + code + " 向服务器发起使用邀请码请求");
            //WelfareActiveManager.Instance.UseInviteCodeReq(code);
            Hide();
        }

        //跳过
        private void OnClose()
        {
            Hide();
        }
    }
}
