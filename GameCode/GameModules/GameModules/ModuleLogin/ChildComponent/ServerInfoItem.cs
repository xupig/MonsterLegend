﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Events;
using Game.UI;
using Game.UI.UIComponent;
using GameLoader.Config;
using GameLoader.Utils;
using GameMain.GlobalManager;
using ModuleCommonUI;
using MogoEngine.Events;
using UnityEngine;

namespace ModuleLogin.ChildComponent
{
    /// <summary>
    /// 显示单个服务器信息--视图类
    /// </summary>
    public class ServerInfoItem : KList.KListItemBase
    {
        private ServerInfo _data;
        private StateText _textDesc;
        private KButton _btnClick;
        private StateImage _imgNew;           //新服图标
        private StateImage _imgRoleIcon;      //角色小图标
        private List<StateImage> stateList;

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as ServerInfo;
                Refresh();
            }
        }

        public override void Dispose()
        {
            _btnClick.onClick.RemoveListener(OnClick);
            _data = null;
        }

        protected override void Awake()
        {
            _textDesc = GetChildComponent<StateText>("Button_server/label");
            _btnClick = GetChildComponent<KButton>("Button_server");
            _imgNew = GetChildComponent<StateImage>("Button_server/image02");   
            _imgRoleIcon = GetChildComponent<StateImage>("Button_server/image03");
            stateList = new List<StateImage>();
            stateList.Add(GetChildComponent<StateImage>("Button_server/baomantuoyuanIcon"));    //爆满Container_serverStatus/Image_baomantuoyuanIcon
            stateList.Add(GetChildComponent<StateImage>("Button_server/fanmangtuoyuanIcon"));   //繁忙
            stateList.Add(GetChildComponent<StateImage>("Button_server/liuchangtuoyuanIcon"));  //流畅
            stateList.Add(GetChildComponent<StateImage>("Button_server/weihutuoyuanIcon"));     //维护
            _btnClick.onClick.AddListener(OnClick);
        }

        protected void Refresh()
        {
            if (_data != null)
            {
                string text = _data.name; //string.Format("{0}服  {1}", _data.id, _data.name);
                _textDesc.ChangeAllStateText(text);
                _textDesc.CurrentText.text = text;
                RefreshState(_data.flag);
                RefreshNew(_data.is_new);
                RefreshCreateRecord(_data.id);
                Visible = true;
            }
            else
            {
                Visible = false;
            }
        }

        /// <summary>
        /// 刷新服务器状态
        /// </summary>
        private void RefreshState(int state)
        {
            stateList[0].Visible = (ServerType)state == ServerType.Hot;           //爆满
            stateList[1].Visible = (ServerType)state == ServerType.Mang;          //繁忙
            stateList[2].Visible = (ServerType)state == ServerType.Normal;        //流畅 
            stateList[3].Visible = (ServerType)state == ServerType.Weihu;         //维护 
            if ((ServerType)state == ServerType.Close)
            {
                _btnClick.interactable = false;
            }
        }

        //刷新新服图标
        private void RefreshNew(int isNew)
        {
            _imgNew.Visible = isNew == 1;
        }

        //刷新创建记录--头像
        private void RefreshCreateRecord(int serverId)
        {
            List<int> list = LocalSetting.settingData.CreateRoleServerList;
            _imgRoleIcon.Visible = list != null && list.Contains(serverId);
        }

        /// <summary>
        /// 点击按钮
        /// </summary>
        public void OnClick()
        {
            if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer)
            {
                DockingService.SendSelectServerLog();
            }
            LoggerHelper.Debug(string.Format("选服:{0}-{1}", _data.id, _data.name));
            if (_data.flag == (int)ServerType.Close || _data.flag==(int)ServerType.Weihu)
            {
                //ari MessageBox.Show(true, "", _data.detail);
                return;
            }
            //EventDispatcher.TriggerEvent<int>(LoginEvents.C_SELECT_SERVER_RETURN, _data.id);
            LocalSetting.settingData.SelectedServerID = _data.id;
            LoggerHelper.Info(string.Format("选中服务器ID:{0}", _data.id));
            UIManager.Instance.ClosePanel(PanelIdEnum.Server);
        }
    }
}
