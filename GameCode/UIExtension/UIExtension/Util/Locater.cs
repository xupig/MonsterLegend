﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 用于GameObject位置调整
/// </summary>
namespace UIExtension
{
    public class Locater : UIBehaviour
    {
        private RectTransform _rect;

        protected override void Awake()
        {
            base.Awake();
            _rect = GetComponent<RectTransform>();
        }

        public float X
        {
            get
            {
                return _rect.anchoredPosition.x;
            }
            set
            {
                _rect.anchoredPosition = new Vector2(value, this.Y);
            }
        }

        public float Y
        {
            get
            {
                return _rect.anchoredPosition.y;
            }
            set
            {
                _rect.anchoredPosition = new Vector2(this.X, value);
            }
        }

        public Vector2 Position
        {
            get
            {
                return _rect.anchoredPosition;
            }
            set
            {
                _rect.anchoredPosition = value;
            }
        }

        public virtual float Width
        {
            get
            {
                return _rect.sizeDelta.x;
            }
        }

        public virtual float Height
        {
            get
            {
                return _rect.sizeDelta.y;
            }
        }
    }
}
