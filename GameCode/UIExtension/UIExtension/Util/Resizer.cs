﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 用于GameObject宽高调整
/// </summary>
namespace UIExtension
{
    public class Resizer : UIBehaviour
    {
        private RectTransform _rect;

        protected override void Awake()
        {
            base.Awake();
            _rect = GetComponent<RectTransform>();
            OriginalSize = new Vector2(_rect.sizeDelta.x, _rect.sizeDelta.y);
        }

        public Vector2 OriginalSize
        {
            get;
            private set;
        }

        public float Width
        {
            get
            {
                return _rect.sizeDelta.x;
            }
            set
            {
                Size = new Vector2(value, _rect.sizeDelta.y);
            }
        }

        public float Height
        {
            get
            {
                return _rect.sizeDelta.y;
            }
            set
            {
                Size = new Vector2(_rect.sizeDelta.x, value);
            }
        }

        public float ScaleX
        {
            get
            {
                return _rect.sizeDelta.x / OriginalSize.x;
            }
            set
            {
                Size = new Vector2(OriginalSize.x * value, _rect.sizeDelta.y);
            }
        }

        public float ScaleY
        {
            get
            {
                return _rect.sizeDelta.y / OriginalSize.y;
            }
            set
            {
                Size = new Vector2(_rect.sizeDelta.x, OriginalSize.y * value);
            }
        }

        public Vector2 Size
        {
            get
            {
                return _rect.sizeDelta;
            }
            set
            {
                _rect.sizeDelta = value;
            }
        }

    }
}
