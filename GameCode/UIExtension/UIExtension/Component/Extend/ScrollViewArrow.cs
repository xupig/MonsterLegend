﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;


namespace UIExtension
{
    public class ScrollViewArrow : XContainer
    {
        public const float DEFAULT_HIDE_VALUE = 0.1f;

        private Image _nextImage;
        private Image _prevImage;

        private float _hideRate = DEFAULT_HIDE_VALUE;

        public float HideRate
        {
            get { return _hideRate; }
            set
            {
                _hideRate = value;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            _nextImage = GetChildComponent<Image>("Image_arrowNext");
            _prevImage = GetChildComponent<Image>("Image_arrowPrev");
        }

        public void UpdateArrow(RectTransform maskRect, RectTransform contentRect, bool isVertical)
        {
            HideArrow();
            Color nColor;
            if(isVertical == true)
            {
                float maskHeight = maskRect.rect.yMax - maskRect.rect.yMin;
                float contentHeight = contentRect.rect.yMax - contentRect.rect.yMin;
                if(contentRect.localPosition.y / maskHeight > HideRate)
                {
                    nColor = _prevImage.color;
                    nColor.a = 1.0f;
                    _prevImage.color = nColor;
                }
                if((contentHeight - (Mathf.Abs(contentRect.localPosition.y) + maskHeight)) / maskHeight > HideRate)
                {
                    nColor = _prevImage.color;
                    nColor.a = 1.0f;
                    _nextImage.color = nColor;
                }
            }
            else
            {
                float maskWidth = maskRect.rect.xMax - maskRect.rect.xMin;
                float contentWidth = contentRect.rect.xMax - contentRect.rect.xMin;

                if(contentRect.localPosition.x / maskWidth < -HideRate)
                {
                    nColor = _prevImage.color;
                    nColor.a = 1.0f;
                    _prevImage.color = nColor;
                }
                if((contentWidth - (Mathf.Abs(contentRect.localPosition.x) + maskWidth)) / maskWidth > HideRate)
                {
                    nColor = _prevImage.color;
                    nColor.a = 1.0f;
                    _nextImage.color = nColor;
                }
            }
        }

        private void HideArrow()
        {
            Color nColor = _nextImage.color;
            nColor.a = 1.0f;
            _nextImage.color = nColor;
            nColor = _prevImage.color;
            nColor.a = 1.0f;
            _prevImage.color = nColor;
        }
    }
}
