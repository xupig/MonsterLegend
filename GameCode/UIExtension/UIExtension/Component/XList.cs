﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace UIExtension
{
    /// <summary>
    /// List可以作为单行单列或多行多列布局容器
    /// List中必须含有名为item的子元素
    /// </summary>
    public class XList : XContainer
    {
        public const string ITEM_PREFIX = "item_";
        public const int DEFAULT_GAP = 0;

        /// <summary>
        /// 在KList上层有Mask的时候可以添加RenderingAgent以优化性能
        /// </summary>
        public bool isItemAddRenderingAgent = false;
        //列表项模板
        protected GameObject _template;
        protected List<XListItemBase> _itemList = new List<XListItemBase>();

        private int _topToDownGap = DEFAULT_GAP;        //item列间距
        private int _leftToRightGap = DEFAULT_GAP;      //item行间距
        private int _topToDownCount = 1;                //item列数量
        private int _leftToRightCount = int.MaxValue;   //item行

        private XListPadding _padding = new XListPadding();
        private XListDirection _direction;
        private Vector2 _itemSize = Vector2.zero;

        private XListItemBase _selectedItem;

        private XComponentEvent<XList, int> _onSelectedIndexChanged;
        private XComponentEvent<XList, XListItemBase> _onItemClicked;
        private XComponentEvent _onCoroutineEnd;
        private XComponentEvent _onAllItemCreated;

        private bool _isExecutingCoroutine = false;

        public XComponentEvent<XList, int> onSelectedIndexChanged
        {
            get
            {
                if(_onSelectedIndexChanged == null)
                {
                    _onSelectedIndexChanged = new XComponentEvent<XList, int>();
                }
                return _onSelectedIndexChanged;
            }
        }

        public XComponentEvent<XList, XListItemBase> onItemClicked
        {
            get
            {
                if(_onItemClicked == null)
                {
                    _onItemClicked = new XComponentEvent<XList, XListItemBase>();
                }
                return _onItemClicked;
            }
        }

        public XComponentEvent onCoroutineEnd
        {
            get
            {
                if (_onCoroutineEnd == null)
                {
                    _onCoroutineEnd = new XComponentEvent();
                }
                return _onCoroutineEnd;
            }
        }

        public XComponentEvent onAllItemCreated
        {
            get
            {
                if (_onAllItemCreated == null)
                {
                    _onAllItemCreated = new XComponentEvent();
                }
                return _onAllItemCreated;
            }
        }

        protected override void Awake()
        {
            InitItemTemplate();
            InitItemSize();
        }

        public XListPadding Padding
        {
            get { return _padding; }
        }

        public int TopToDownGap
        {
            get { return _topToDownGap; }
        }

        public int LeftToRightGap
        {
            get { return _leftToRightGap; }
        }

        public int TopToDownCount
        {
            get { return _topToDownCount; }
        }

        public int LeftToRightCount
        {
            get { return _leftToRightCount; }
        }

        public Vector2 ItemSize
        {
            get { return _itemSize; }
        }

        protected void InitItemTemplate()
        {
            _template = GetChild("item");
            _template.SetActive(false);
        }

        protected void InitItemSize()
        {
            RectTransform rect = _template.GetComponent<RectTransform>();
            _itemSize.x = rect.sizeDelta.x;
            _itemSize.y = rect.sizeDelta.y;
        }

        public virtual void SetPadding(int top, int right, int bottom, int left)
        {
            _padding.top = top;
            _padding.right = right;
            _padding.bottom = bottom;
            _padding.left = left;
        }

        public virtual void SetGap(int topToDownGap, int leftToRightGap)
        {
            _topToDownGap = topToDownGap;
            _leftToRightGap = leftToRightGap;
        }

        /// <summary>
        /// List分成多个columnCount * rowCount的部分（子页），当columnCount或rowCount == int.MaxValue的时候是无限长单页
        /// row 横向，排
        /// column 纵向，列
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="topToDownCount"></param>
        /// <param name="leftToRightCount"></param>
        public virtual void SetDirection(XListDirection direction, int leftToRightCount, int topToDownCount)
        {
            _direction = direction;
            if(_direction == XListDirection.LeftToRight)
            {
                _leftToRightCount = leftToRightCount;
                _topToDownCount = Mathf.Min(topToDownCount, (int)(int.MaxValue / leftToRightCount));
            }
            else
            {
                _leftToRightCount = Mathf.Min(leftToRightCount, (int)(int.MaxValue / topToDownCount));
                _topToDownCount = topToDownCount;
            }
        }

        public virtual void SetDirection(XListDirection direction, int count)
        {
            if(direction == XListDirection.LeftToRight)
            {
                SetDirection(XListDirection.LeftToRight, count, int.MaxValue);
            }
            else
            {
                SetDirection(XListDirection.TopToDown, int.MaxValue, count);
            }
        }

        public List<XListItemBase> ItemList
        {
            get
            {
                return _itemList;
            }
        }

        /// <summary>
        /// 用于对性能比较敏感的场合，只做一次布局操作
        /// coroutineCreateCount，当通过协程的方式创建Item时每帧创建Item的数量设置
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataList"></param>
        public virtual void SetDataList<T>(IList dataList, int coroutineCreateCount = 0) where T : XListItemBase
        {
            SetDataList(typeof(T), dataList, coroutineCreateCount);
        }

        public virtual void SetDataList(Type itemType, IList dataList, int coroutineCreateCount = 0)
        {
            if(coroutineCreateCount == 0)
            {
                for(int i = 0; i < dataList.Count; i++)
                {
                    AddItem(itemType, dataList[i], false);
                }
                UpdateItemListLayout();
            }
            else
            {
                StartCoroutine(CreateItem(itemType, dataList, coroutineCreateCount));
            }
        }

        protected virtual IEnumerator CreateItem(Type itemType, IList dataList, int coroutineCreateCount)
        {
            _isExecutingCoroutine = true;
            coroutineCreateCount = coroutineCreateCount == 0 ? dataList.Count : coroutineCreateCount;
            int startIndex = 0;
            int endIndex = 0;
            while(endIndex < dataList.Count)
            {
                endIndex = Mathf.Min(dataList.Count, startIndex + coroutineCreateCount);
                for(int i = startIndex; i < endIndex; i++)
                {
                    if(i < dataList.Count)
                    {
                        AddItem(itemType, dataList[i], false);
                    }
                }
                startIndex = endIndex;
                UpdateItemListLayout();
                yield return null;
            }
            InvokeCoroutineEnd();
            InvokeAllItemCreated();
            _isExecutingCoroutine = false;
        }

        private void InvokeCoroutineEnd()
        {
            if (_onCoroutineEnd != null)
            {
                _onCoroutineEnd.Invoke();
            }
        }

        protected void InvokeAllItemCreated()
        {
            if (_onAllItemCreated != null)
            {
                _onAllItemCreated.Invoke();
            }
        }

        protected bool IsExecutingCoroutine
        {
            get
            {
                return _isExecutingCoroutine;
            }
        }

        public void AddItem<T>(System.Object data, bool layoutImmediately = true) where T : XListItemBase
        {
            AddItemAt<T>(data, _itemList.Count, layoutImmediately);
        }

        public void AddItemAt<T>(System.Object data, int index, bool layoutImmediately = true) where T : XListItemBase
        {
            AddItemAt(typeof(T), data, index, layoutImmediately);
        }

        public void AddItem(Type itemType, System.Object data, bool layoutImmediately = true)
        {
            AddItemAt(itemType, data, _itemList.Count, layoutImmediately);
        }

        public virtual void AddItemAt(Type itemType, System.Object data, int index, bool layoutImmediately = true)
        {
            GameObject itemGo = CreateItemGo();
            AddToHierarchy(itemGo);
            XListItemBase item = itemGo.AddComponent(itemType) as XListItemBase;
            item.Index = index;
            item.name = GenerateItemName(index);
            item.Data = data;
            _itemList.Insert(index, item);
            AddItemEventListener(item);
            if(isItemAddRenderingAgent == true)
            {
                RenderingAgent agent = itemGo.AddComponent<RenderingAgent>();
                agent.SetBoundary(this.transform.parent.GetComponent<RectTransform>());
                agent.Item = item;
            }
            if(layoutImmediately == true)
            {
                UpdateItemListLayout();
            }
        }

        private string GenerateItemName(int index)
        {
            return ITEM_PREFIX + index;
        }

        protected GameObject CreateItemGo()
        {
            return Instantiate(_template) as GameObject;
        }

        public GameObject TemplateGo()
        {
            return _template; 
        }

        private void AddToHierarchy(GameObject itemGo)
        {
            itemGo.transform.SetParent(this.transform);
            itemGo.transform.localScale = Vector3.one;
            itemGo.SetActive(true);
        }

        public void DoLayout()
        {
            UpdateItemListLayout();
        }

        protected virtual void UpdateItemListLayout()
        {
            RectTransform listRect = GetComponent<RectTransform>();
            float listRight = 0;
            float listBottom = 0;
            int index = OffsetItemCount;
            Vector2 totalSizeDelta = Vector2.zero;
            for(int i = 0; i < _itemList.Count; i++)
            {
                XListItemBase item = _itemList[i];
                if(item.Visible)
                {
                    RectTransform itemRect = item.GetComponent<RectTransform>();
                    Vector2 position = CalculateItemPosition(index, itemRect);
                    float itemRight = position.x + itemRect.sizeDelta.x + _padding.right;
                    listRight = itemRight > listRight ? itemRight : listRight;
                    float itemBottom = position.y + itemRect.sizeDelta.y + _padding.bottom;
                    listBottom = itemBottom > listBottom ? itemBottom : listBottom;
                    itemRect.anchoredPosition = new Vector2(position.x, -position.y);
                    itemRect.localPosition = new Vector3(itemRect.localPosition.x, itemRect.localPosition.y, 0);
                    if (item.Index != index)
                    {
                        item.Index = index;
                        item.name = GenerateItemName(index);
                    }
                    index++;
                }
            }
            listRect.sizeDelta = new Vector2(listRight, listBottom);
        }

        protected virtual Vector2 CalculateItemPosition(int index, RectTransform itemRect)
        {
            Vector2 result = Vector2.zero;
            int pageCount = _leftToRightCount * _topToDownCount;
            if(_direction == XListDirection.LeftToRight)
            {
                result.x = _padding.left + ((int)(index / pageCount) * _leftToRightCount + index % _leftToRightCount) * (_itemSize.x + _leftToRightGap);
                result.y = _padding.top + (int)((index % pageCount) / _leftToRightCount) * (_itemSize.y + _topToDownGap);
            }
            else
            {
                result.x = _padding.left + (int)((index % pageCount) / _topToDownCount) * (_itemSize.x + _leftToRightGap);
                result.y = _padding.top + ((int)(index / pageCount) * _topToDownCount + index % _topToDownCount) * (_itemSize.y + _topToDownGap);
            }
            return result;
        }

        protected virtual int OffsetItemCount
        {
            get
            {
                return 0;
            }
        }

        private void AddItemEventListener(XListItemBase item)
        {
            item.onClick.AddListener(OnItemClicked);
        }

        private void RemoveItemEventListener(XListItemBase item)
        {
            item.onClick.RemoveAllListeners();
        }

        protected virtual void OnItemClicked(XListItemBase item, int index)
        {
            int oldSelectedIndex = this.SelectedIndex;
            if(_onItemClicked != null)
            {
                _onItemClicked.Invoke(this, item);
            }
            if(item == _selectedItem && _selectedItem != null && _selectedItem.Index == index)
            {
                return;
            }
            bool isSelectedIndexChanged = (this.SelectedIndex != oldSelectedIndex);
            if(isSelectedIndexChanged == true)//若在_onItemClicked事件中修改了KList.SelectedIndex，则不再调用后续事件
            {
                return;
            }
            if(_selectedItem != null)
            {
                _selectedItem.IsSelected = false;
            }
            _selectedItem = item;
            _selectedItem.IsSelected = true;
            if(_onSelectedIndexChanged != null)
            {
                _onSelectedIndexChanged.Invoke(this, index);
            }
        }

        public int SelectedIndex
        {
            get
            {
                if(_selectedItem == null)
                {
                    return -1;
                }
                return _selectedItem.Index;
            }
            set
            {
                if(_selectedItem != null)
                {
                    _selectedItem.IsSelected = false;
                    _selectedItem = null;
                }
                if(value >= 0 && value <= _itemList.Count - 1)
                {
                    _selectedItem = _itemList[value];
                    _selectedItem.IsSelected = true;
                }
            }
        }

        public void RemoveItemAt(int index)
        {
            XListItemBase item = _itemList[index];
            RemoveItemEventListener(item);
            item.gameObject.SetActive(false);
            _itemList.RemoveAt(index);
            item.Dispose();
            Destroy(item.gameObject);
            UpdateItemListLayout();
        }

        public void RemoveAll()
        {
            RemoveItemRange(0, _itemList.Count);
        }

        public void RemoveItemRange(int start, int count)
        {
            for(int i = start + count - 1; i >= start; i--)
            {
                XListItemBase item = _itemList[i];
                RemoveItemEventListener(item);
                _itemList.RemoveAt(i);
                item.Dispose();
                Destroy(item.gameObject);
            }
            UpdateItemListLayout();
        }

        public XListItemBase this[int index]
        {
            get
            {
                return _itemList[index];
            }
        }
    }

    //=========================================================
    [DisallowMultipleComponent]
    public abstract class XListItemBase : XContainer, IPointerClickHandler, IPointerDownHandler
    {
        public virtual bool IsSelected { get; set; }
        public virtual int Index { get; set; }
        public virtual System.Object Data { get; set; }
        protected XComponentEvent<XListItemBase, int> _onClick;
        internal bool IsDataDirty { get; set; }


        public virtual int ID
        {
            get;
            set;
        }

        public XComponentEvent<XListItemBase, int> onClick
        {
            get
            {
                if (_onClick == null)
                {
                    _onClick = new XComponentEvent<XListItemBase, int>();
                }
                return _onClick;
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            EventSystem.current.SetSelectedGameObject(gameObject, eventData);
        }

        /// <summary>
        /// 在Awake函数中对Item的子元素建立索引
        /// </summary>
        protected abstract override void Awake();

        public override void OnPointerClick(PointerEventData evtData)
        {
            if (evtData.dragging == true)
            {
                return;
            }
            if (_onClick != null)
            {
                _onClick.Invoke(this, this.Index);
            }
        }

        public void SetDataDirty()
        {
            IsDataDirty = true;
        }

        public virtual void DoRefreshData() { }
        public virtual void Show() { }
        public virtual void Hide() { }
        public abstract void Dispose();

        protected sealed override void OnEnable()
        {
            Show();
        }

        protected sealed override void OnDisable()
        {
            Hide();
        }

        protected sealed override void OnDestroy()
        {
            Dispose();
        }
    }

    public struct XListPadding
    {
        public int top;
        public int right;
        public int bottom;
        public int left;
    }

    public enum XListDirection
    {
        //
        /// <summary>
        /// List Item布局时，纵向布局优先（TopToDown）横向布局优先（LeftToRight）
        /// 如单列列表应该是（LeftToRight = 1, TopToDown = int.Max）;
        /// 如单行列表应该是（LeftToRight = int.Max, TopToDown = 1）;
        /// </summary>
        TopToDown,
        LeftToRight
    }
}
