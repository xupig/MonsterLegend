﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace UIExtension
{
    public partial class XContainer : MonoBehaviour, IXContainer
    {
        protected virtual void Awake()
        {
            
        }

        public virtual void OnPointerClick(PointerEventData evtData)
        {
            Debug.Log("OnPointerClick: " + gameObject.name + "," + gameObject.GetType());
        }

        public GameObject GetChild(string childPath)
        {
            return KComponentUtil.GetChild(this.gameObject, childPath);
        }

        public GameObject GetChild(int index)
        {
            return gameObject.transform.GetChild(index).gameObject;
        }

        public T GetChildComponent<T>(string childPath) where T : Component
        {
            return KComponentUtil.GetChildComponent<T>(this.gameObject, childPath);
        }

        public T AddChildComponent<T>(string childPath) where T : Component
        {
            return KComponentUtil.AddChildComponent<T>(this.gameObject, childPath);
        }

        public void RecalculateSize()
        {
            RectTransform rect = GetComponent<RectTransform>();
            float width = 0;
            float height = 0;
            for(int i = 0; i < transform.childCount; i++)
            {
                RectTransform childRect = transform.GetChild(i) as RectTransform;
                if(childRect.gameObject.activeSelf == false)
                {
                    continue;
                }
                float childX = childRect.anchoredPosition.x;
                float childY = childRect.anchoredPosition.y;
                float childWidth = childRect.sizeDelta.x;
                float childHeight = childRect.sizeDelta.y;
                if(width < (childX + childWidth))
                {
                    width = childX + childWidth;
                }
                if(height < (childHeight - childY))
                {
                    height = childHeight - childY;
                }
            }
            rect.sizeDelta = new Vector2(width, height);
        }

        public bool Visible
        {
            get
            {
                return gameObject.activeSelf;
            }
            set
            {
                gameObject.SetActive(value);
            }
        }

    }

}
