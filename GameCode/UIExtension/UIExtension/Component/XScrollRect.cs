﻿#region 模块信息
/*==========================================
// 文件名：KScrollRect
// 命名空间: Assets.Plugins.UIComponent.ExtendComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/12 15:35:13
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UIExtension
{
    public class XScrollRect : ScrollRect
    {
        public XComponentEvent<XScrollRect> onRequestNext = new XComponentEvent<XScrollRect>();
        public XComponentEvent<XScrollRect> onReRequest = new XComponentEvent<XScrollRect>();
        private bool _reRequest;
        private bool _requestNext;

        private bool _draggable = true;
        public bool Draggable
        {
            get { return _draggable; }
            set { _draggable = value; }
        }

        public override void OnDrag(PointerEventData eventData)
        {
            if(_draggable)
            {
                base.OnDrag(eventData);
                _reRequest = verticalNormalizedPosition >= 1.0f && eventData.delta.y < 0;
                _requestNext = verticalNormalizedPosition <= 0 && eventData.delta.y > 0;
            }
        }

        public override void OnEndDrag(UnityEngine.EventSystems.PointerEventData eventData)
        {
            if(_draggable)
            {
                base.OnEndDrag(eventData);
                if(_requestNext)
                {
                    onRequestNext.Invoke(this);
                }
                else if(_reRequest)
                {
                    onReRequest.Invoke(this);
                }
                _reRequest = false;
                _requestNext = false;
            }
        }
    }
}
