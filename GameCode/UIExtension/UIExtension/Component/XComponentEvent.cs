﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace UIExtension
{
    public class XComponentEvent : UnityEvent
    {
        private List<UnityAction> _list;

        public XComponentEvent()
            : base()
        {
            _list = new List<UnityAction>();
        }

        public new void AddListener(UnityAction listener)
        {
            if(_list.IndexOf(listener) > -1)
            {
                if(Application.platform == RuntimePlatform.WindowsEditor)
                {
                    Debug.LogWarning(string.Format("重复添加事件监听 Target {0} Method {1}", listener.Target, listener.Method.Name));
                }
                return;
            }
            base.AddListener(listener);
            _list.Add(listener);
        }

        public new void RemoveListener(UnityAction listener)
        {
            int index = _list.IndexOf(listener);
            if(index > -1)
            {
                _list.RemoveAt(index);
            }
            base.RemoveListener(listener);
        }
    }

    public class XComponentEvent<T> : UnityEvent<T>
    {
        private List<UnityAction<T>> _list;

        public XComponentEvent()
            : base()
        {
            _list = new List<UnityAction<T>>();
        }

        public new void AddListener(UnityAction<T> listener)
        {
            if(_list.IndexOf(listener) > -1)
            {
                Debug.LogWarning(string.Format("重复添加事件监听 Target {0} Method {1}", listener.Target, listener.Method.Name));
                return;
            }
            base.AddListener(listener);
            _list.Add(listener);
        }

        public new void RemoveListener(UnityAction<T> listener)
        {
            int index = _list.IndexOf(listener);
            if(index > -1)
            {
                _list.RemoveAt(index);
            }
            base.RemoveListener(listener);
        }
    }

    public class XComponentEvent<T, U> : UnityEvent<T, U>
    {
        private List<UnityAction<T, U>> _list;

        public XComponentEvent()
            : base()
        {
            _list = new List<UnityAction<T, U>>();
        }

        public new void AddListener(UnityAction<T, U> listener)
        {
            if(_list.IndexOf(listener) > -1)
            {
                Debug.LogWarning(string.Format("重复添加事件监听 Target {0} Method {1}", listener.Target, listener.Method.Name));
                return;
            }
            base.AddListener(listener);
            _list.Add(listener);
        }

        public new void RemoveListener(UnityAction<T, U> listener)
        {
            int index = _list.IndexOf(listener);
            if(index > -1)
            {
                _list.RemoveAt(index);
            }
            base.RemoveListener(listener);
        }
    }

    public class XComponentEvent<T, U, V> : UnityEvent<T, U, V>
    {
        private List<UnityAction<T, U, V>> _list;

        public XComponentEvent()
            : base()
        {
            _list = new List<UnityAction<T, U, V>>();
        }

        public new void AddListener(UnityAction<T, U, V> listener)
        {
            if(_list.IndexOf(listener) > -1)
            {
                Debug.LogWarning(string.Format("重复添加事件监听 Target {0} Method {1}", listener.Target, listener.Method.Name));
                return;
            }
            base.AddListener(listener);
            _list.Add(listener);
        }

        public new void RemoveListener(UnityAction<T, U, V> listener)
        {
            int index = _list.IndexOf(listener);
            if(index > -1)
            {
                _list.RemoveAt(index);
            }
            base.RemoveListener(listener);
        }
    }

    public class XComponentEvent<T, U, V, W> : UnityEvent<T, U, V, W>
    {
        private List<UnityAction<T, U, V, W>> _list;

        public XComponentEvent()
            : base()
        {
            _list = new List<UnityAction<T, U, V, W>>();
        }

        public new void AddListener(UnityAction<T, U, V, W> listener)
        {
            if(_list.IndexOf(listener) > -1)
            {
                Debug.LogWarning(string.Format("重复添加事件监听 Target {0} Method {1}", listener.Target, listener.Method.Name));
                return;
            }
            base.AddListener(listener);
            _list.Add(listener);
        }

        public new void RemoveListener(UnityAction<T, U, V, W> listener)
        {
            int index = _list.IndexOf(listener);
            if(index > -1)
            {
                _list.RemoveAt(index);
            }
            base.RemoveListener(listener);
        }
    }
}
