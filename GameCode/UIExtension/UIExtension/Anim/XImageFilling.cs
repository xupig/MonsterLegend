using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace UIExtension
{
    public class XImageFilling : MonoBehaviour
    {
        Image _currentImage = null;
        float _amount = 1f;
        float _fillingAmount = 1f;
        bool _isFilling = false;
        float _fillingDirection = 0;
        float _damping = 0.5f;
        float _ref = 0f;
        void Awake()
        {
            _currentImage = this.GetComponent<Image>();
            if (_currentImage == null)
            {
                Debug.LogError("XImageFilling can not find a Image in " + this.gameObject.name);
                this.enabled = false;
                return;
            }
            _amount = _currentImage.fillAmount;
            _fillingAmount = _currentImage.fillAmount;
        }

        public void SetValue(float value)
        {
            _fillingAmount = _amount;
            _amount = value;
            if (_amount != _fillingAmount)
            {
                _isFilling = true;
                _fillingDirection = Mathf.Sign(_amount - _fillingAmount);
            }
        }

        void Update()
        {
            if (!_isFilling) return;
            _fillingAmount = Mathf.SmoothDamp(_fillingAmount, _amount, ref _ref, _damping);
            if (Mathf.Abs(_fillingAmount - _amount) < 0.001f
                || Mathf.Sign(_amount - _fillingAmount) != _fillingDirection)
            {
                _isFilling = false;
                _currentImage.fillAmount = _amount;
                _fillingAmount = _amount;
            }
            else
            {
                _currentImage.fillAmount = _fillingAmount;
            }
        }
    }
}
