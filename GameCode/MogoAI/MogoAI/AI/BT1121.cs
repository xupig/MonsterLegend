namespace Mogo.AI.BT
{
	public sealed class BT1121 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1121 _instance = null;
		public static BT1121 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1121();

				return _instance;
			}
		}

		private BT1121()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					{
						Mogo.AI.SequenceNode node3 = new Mogo.AI.SequenceNode();
						node2.AddChild(node3);
						node3.AddChild(new Mogo.AI.IsFirstThinking());
						node3.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,70));
						node3.AddChild(new Mogo.AI.MonsterSpeech(1,1));
					}
					{
						Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
						node2.AddChild(node7);
						node7.AddChild(new Mogo.AI.IsInDeathState());
						node7.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,70));
						node7.AddChild(new Mogo.AI.MonsterSpeech(2,1));
						node7.AddChild(new Mogo.AI.EnterRest(5000));
					}
					{
						Mogo.AI.SequenceNode node12 = new Mogo.AI.SequenceNode();
						node2.AddChild(node12);
						node12.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,10));
						node12.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,70));
						node12.AddChild(new Mogo.AI.MonsterSpeech(3,1));
					}
					{
						Mogo.AI.SequenceNode node16 = new Mogo.AI.SequenceNode();
						node2.AddChild(node16);
						node16.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,30));
						node16.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,70));
						node16.AddChild(new Mogo.AI.MonsterSpeech(4,1));
					}
					{
						Mogo.AI.SequenceNode node20 = new Mogo.AI.SequenceNode();
						node2.AddChild(node20);
						node20.AddChild(new Mogo.AI.IsFightState());
						node20.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
						node20.AddChild(new Mogo.AI.MonsterSpeech(5,1));
					}
					{
						Mogo.AI.SequenceNode node24 = new Mogo.AI.SequenceNode();
						node2.AddChild(node24);
						node24.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.le,100));
					}
				}
				{
					Mogo.AI.SequenceNode node26 = new Mogo.AI.SequenceNode();
					node1.AddChild(node26);
					{
						Mogo.AI.PrioritySelectorNode node27 = new Mogo.AI.PrioritySelectorNode();
						node26.AddChild(node27);
						node27.AddChild(new Mogo.AI.IsFightState());
						{
							Mogo.AI.SequenceNode node29 = new Mogo.AI.SequenceNode();
							node27.AddChild(node29);
							node29.AddChild(new Mogo.AI.AOI(100,1,0,"1000",0));
							node29.AddChild(new Mogo.AI.EnterFight());
						}
					}
					{
						Mogo.AI.SequenceNode node32 = new Mogo.AI.SequenceNode();
						node26.AddChild(node32);
						node32.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
						node32.AddChild(new Mogo.AI.IsTargetCanBeAttack());
						{
							Mogo.AI.PrioritySelectorNode node35 = new Mogo.AI.PrioritySelectorNode();
							node32.AddChild(node35);
							{
								Mogo.AI.SequenceNode node36 = new Mogo.AI.SequenceNode();
								node35.AddChild(node36);
								node36.AddChild(new Mogo.AI.InSkillCoolDown(7));
								node36.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
								node36.AddChild(new Mogo.AI.CastSpell(7,0));
								node36.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node41 = new Mogo.AI.SequenceNode();
								node35.AddChild(node41);
								node41.AddChild(new Mogo.AI.InSkillCoolDown(6));
								node41.AddChild(new Mogo.AI.InSkillRange(6));
								node41.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,15));
								node41.AddChild(new Mogo.AI.CastSpell(6,0));
								node41.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node47 = new Mogo.AI.SequenceNode();
								node35.AddChild(node47);
								node47.AddChild(new Mogo.AI.InSkillCoolDown(5));
								node47.AddChild(new Mogo.AI.InSkillRange(5));
								node47.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
								node47.AddChild(new Mogo.AI.CastSpell(5,0));
								node47.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node53 = new Mogo.AI.SequenceNode();
								node35.AddChild(node53);
								node53.AddChild(new Mogo.AI.InSkillCoolDown(4));
								node53.AddChild(new Mogo.AI.InSkillRange(4));
								node53.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
								node53.AddChild(new Mogo.AI.CastSpell(4,0));
								node53.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node59 = new Mogo.AI.SequenceNode();
								node35.AddChild(node59);
								node59.AddChild(new Mogo.AI.InSkillCoolDown(3));
								node59.AddChild(new Mogo.AI.InSkillRange(3));
								node59.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,40));
								node59.AddChild(new Mogo.AI.CastSpell(3,0));
								node59.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node65 = new Mogo.AI.SequenceNode();
								node35.AddChild(node65);
								node65.AddChild(new Mogo.AI.InSkillCoolDown(2));
								node65.AddChild(new Mogo.AI.InSkillRange(2));
								node65.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
								node65.AddChild(new Mogo.AI.CastSpell(2,0));
								node65.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node71 = new Mogo.AI.SequenceNode();
								node35.AddChild(node71);
								node71.AddChild(new Mogo.AI.InSkillCoolDown(1));
								node71.AddChild(new Mogo.AI.InSkillRange(1));
								node71.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
								node71.AddChild(new Mogo.AI.CastSpell(1,0));
								node71.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node77 = new Mogo.AI.SequenceNode();
								node35.AddChild(node77);
								node77.AddChild(new Mogo.AI.InSkillRange(1));
								{
									Mogo.AI.PrioritySelectorNode node79 = new Mogo.AI.PrioritySelectorNode();
									node77.AddChild(node79);
									{
										Mogo.AI.SequenceNode node80 = new Mogo.AI.SequenceNode();
										node79.AddChild(node80);
										node80.AddChild(new Mogo.AI.HasFriendInLeftRange(50));
										{
											Mogo.AI.PrioritySelectorNode node82 = new Mogo.AI.PrioritySelectorNode();
											node80.AddChild(node82);
											{
												Mogo.AI.SequenceNode node83 = new Mogo.AI.SequenceNode();
												node82.AddChild(node83);
												{
													Mogo.AI.Not node84 = new Mogo.AI.Not();
													node83.AddChild(node84);
													node84.Proxy(new Mogo.AI.HasFriendInRightRange(50));
												}
												node83.AddChild(new Mogo.AI.LookOn(100,200,0,0,0,100,0,0,500,500,0,300,0,1));
											}
											node82.AddChild(new Mogo.AI.EnterRest(500));
										}
									}
									{
										Mogo.AI.SequenceNode node88 = new Mogo.AI.SequenceNode();
										node79.AddChild(node88);
										node88.AddChild(new Mogo.AI.HasFriendInRightRange(50));
										node88.AddChild(new Mogo.AI.LookOn(100,200,0,0,100,0,0,0,500,500,300,0,0,1));
									}
									node79.AddChild(new Mogo.AI.LookOn(150,800,0,0,0,0,100,0,500,500,500,500,500,1));
								}
							}
							{
								Mogo.AI.SequenceNode node92 = new Mogo.AI.SequenceNode();
								node35.AddChild(node92);
								{
									Mogo.AI.Not node93 = new Mogo.AI.Not();
									node92.AddChild(node93);
									node93.Proxy(new Mogo.AI.InSkillRange(1));
								}
								node92.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
								node92.AddChild(new Mogo.AI.ChooseCastPoint(1));
								node92.AddChild(new Mogo.AI.MoveTo(1f));
							}
							node35.AddChild(new Mogo.AI.EnterRest(500));
						}
					}
				}
			}
		}
	}
}
