namespace Mogo.AI.BT
{
	public sealed class BT2051 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT2051 _instance = null;
		public static BT2051 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT2051();

				return _instance;
			}
		}

		private BT2051()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				node1.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
				node1.AddChild(new Mogo.AI.Patrol(400,800,400));
			}
		}
	}
}
