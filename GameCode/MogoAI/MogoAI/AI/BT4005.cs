namespace Mogo.AI.BT
{
	public sealed class BT4005 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT4005 _instance = null;
		public static BT4005 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT4005();

				return _instance;
			}
		}

		private BT4005()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				node1.AddChild(new Mogo.AI.FindTargetByMonsterID(4004,0));
				{
					Mogo.AI.SequenceNode node3 = new Mogo.AI.SequenceNode();
					node1.AddChild(node3);
					node3.AddChild(new Mogo.AI.InSkillCoolDown(1));
					node3.AddChild(new Mogo.AI.InSkillRange(1));
					node3.AddChild(new Mogo.AI.CastSpell(1,0));
				}
				node1.AddChild(new Mogo.AI.EnterRest(500));
			}
		}
	}
}
