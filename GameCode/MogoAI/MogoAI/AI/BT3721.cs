namespace Mogo.AI.BT
{
	public sealed class BT3721 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT3721 _instance = null;
		public static BT3721 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT3721();

				return _instance;
			}
		}

		private BT3721()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					{
						Mogo.AI.SequenceNode node3 = new Mogo.AI.SequenceNode();
						node2.AddChild(node3);
						node3.AddChild(new Mogo.AI.IsFirstThinking());
						node3.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node3.AddChild(new Mogo.AI.MonsterSpeech(1,1));
					}
					{
						Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
						node2.AddChild(node7);
						node7.AddChild(new Mogo.AI.IsInDeathState());
						node7.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node7.AddChild(new Mogo.AI.MonsterSpeech(2,1));
						node7.AddChild(new Mogo.AI.EnterRest(5000));
					}
					{
						Mogo.AI.SequenceNode node12 = new Mogo.AI.SequenceNode();
						node2.AddChild(node12);
						node12.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,10));
						node12.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node12.AddChild(new Mogo.AI.MonsterSpeech(3,1));
					}
					{
						Mogo.AI.SequenceNode node16 = new Mogo.AI.SequenceNode();
						node2.AddChild(node16);
						node16.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,30));
						node16.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node16.AddChild(new Mogo.AI.MonsterSpeech(4,1));
					}
					{
						Mogo.AI.SequenceNode node20 = new Mogo.AI.SequenceNode();
						node2.AddChild(node20);
						node20.AddChild(new Mogo.AI.IsFightState());
						node20.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,5));
						node20.AddChild(new Mogo.AI.MonsterSpeech(5,1));
					}
					{
						Mogo.AI.SequenceNode node24 = new Mogo.AI.SequenceNode();
						node2.AddChild(node24);
						node24.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.le,100));
					}
				}
				{
					Mogo.AI.SequenceNode node26 = new Mogo.AI.SequenceNode();
					node1.AddChild(node26);
					node26.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
					node26.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node29 = new Mogo.AI.PrioritySelectorNode();
						node26.AddChild(node29);
						{
							Mogo.AI.SequenceNode node30 = new Mogo.AI.SequenceNode();
							node29.AddChild(node30);
							node30.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.lt,2));
							node30.AddChild(new Mogo.AI.InSkillCoolDown(1));
							node30.AddChild(new Mogo.AI.CastSpell(1,0));
							node30.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node35 = new Mogo.AI.SequenceNode();
							node29.AddChild(node35);
							node35.AddChild(new Mogo.AI.InSkillCoolDown(3));
							node35.AddChild(new Mogo.AI.InSkillRange(3));
							node35.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
							node35.AddChild(new Mogo.AI.CastSpell(3,0));
							node35.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node41 = new Mogo.AI.SequenceNode();
							node29.AddChild(node41);
							node41.AddChild(new Mogo.AI.InSkillCoolDown(2));
							node41.AddChild(new Mogo.AI.InSkillRange(2));
							node41.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,90));
							node41.AddChild(new Mogo.AI.CastSpell(2,0));
							node41.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node47 = new Mogo.AI.SequenceNode();
							node29.AddChild(node47);
							node47.AddChild(new Mogo.AI.InSkillRange(2));
							{
								Mogo.AI.PrioritySelectorNode node49 = new Mogo.AI.PrioritySelectorNode();
								node47.AddChild(node49);
								{
									Mogo.AI.SequenceNode node50 = new Mogo.AI.SequenceNode();
									node49.AddChild(node50);
									node50.AddChild(new Mogo.AI.HasFriendInLeftRange(50));
									{
										Mogo.AI.PrioritySelectorNode node52 = new Mogo.AI.PrioritySelectorNode();
										node50.AddChild(node52);
										{
											Mogo.AI.SequenceNode node53 = new Mogo.AI.SequenceNode();
											node52.AddChild(node53);
											{
												Mogo.AI.Not node54 = new Mogo.AI.Not();
												node53.AddChild(node54);
												node54.Proxy(new Mogo.AI.HasFriendInRightRange(50));
											}
											node53.AddChild(new Mogo.AI.LookOn(100,200,0,0,0,100,0,0,500,500,0,300,0,1));
										}
										node52.AddChild(new Mogo.AI.EnterCD(500));
									}
								}
								{
									Mogo.AI.SequenceNode node58 = new Mogo.AI.SequenceNode();
									node49.AddChild(node58);
									node58.AddChild(new Mogo.AI.HasFriendInRightRange(50));
									node58.AddChild(new Mogo.AI.LookOn(100,200,0,0,100,0,0,0,500,500,300,0,0,1));
								}
								node49.AddChild(new Mogo.AI.LookOn(150,800,0,0,0,0,100,0,500,500,500,500,500,1));
							}
						}
						{
							Mogo.AI.SequenceNode node62 = new Mogo.AI.SequenceNode();
							node29.AddChild(node62);
							{
								Mogo.AI.Not node63 = new Mogo.AI.Not();
								node62.AddChild(node63);
								node63.Proxy(new Mogo.AI.InSkillRange(2));
							}
							node62.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
							node62.AddChild(new Mogo.AI.ChooseCastPoint(2));
							node62.AddChild(new Mogo.AI.MoveTo(1f));
						}
						node29.AddChild(new Mogo.AI.EnterRest(500));
					}
				}
			}
		}
	}
}
