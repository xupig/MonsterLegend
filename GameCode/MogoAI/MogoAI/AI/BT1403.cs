namespace Mogo.AI.BT
{
	public sealed class BT1403 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1403 _instance = null;
		public static BT1403 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1403();

				return _instance;
			}
		}

		private BT1403()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					{
						Mogo.AI.SequenceNode node3 = new Mogo.AI.SequenceNode();
						node2.AddChild(node3);
						node3.AddChild(new Mogo.AI.IsFirstThinking());
						node3.AddChild(new Mogo.AI.MonsterSpeech(1,1));
					}
					{
						Mogo.AI.SequenceNode node6 = new Mogo.AI.SequenceNode();
						node2.AddChild(node6);
						node6.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.le,100));
					}
				}
				{
					Mogo.AI.SequenceNode node8 = new Mogo.AI.SequenceNode();
					node1.AddChild(node8);
					{
						Mogo.AI.PrioritySelectorNode node9 = new Mogo.AI.PrioritySelectorNode();
						node8.AddChild(node9);
						node9.AddChild(new Mogo.AI.IsFightState());
						{
							Mogo.AI.SequenceNode node11 = new Mogo.AI.SequenceNode();
							node9.AddChild(node11);
							node11.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
							node11.AddChild(new Mogo.AI.EnterFight());
						}
					}
					{
						Mogo.AI.SequenceNode node14 = new Mogo.AI.SequenceNode();
						node8.AddChild(node14);
						node14.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
						node14.AddChild(new Mogo.AI.IsTargetCanBeAttack());
						{
							Mogo.AI.PrioritySelectorNode node17 = new Mogo.AI.PrioritySelectorNode();
							node14.AddChild(node17);
							{
								Mogo.AI.SequenceNode node18 = new Mogo.AI.SequenceNode();
								node17.AddChild(node18);
								node18.AddChild(new Mogo.AI.MoveToAppoint("159,39,70"));
								node18.AddChild(new Mogo.AI.CmpTargetPointDistance(Mogo.AI.CmpType.gt,50));
								node18.AddChild(new Mogo.AI.AddBuff(4002,3000));
								node18.AddChild(new Mogo.AI.CancelAttackTarget());
								node18.AddChild(new Mogo.AI.MoveTo(1.5f));
							}
							{
								Mogo.AI.SequenceNode node24 = new Mogo.AI.SequenceNode();
								node17.AddChild(node24);
								node24.AddChild(new Mogo.AI.InSkillCoolDown(1));
								node24.AddChild(new Mogo.AI.MonsterSpeech(2,1));
								node24.AddChild(new Mogo.AI.CastSpell(1,0));
								node24.AddChild(new Mogo.AI.EnterCD(0));
							}
							node17.AddChild(new Mogo.AI.EnterRest(500));
						}
					}
				}
			}
		}
	}
}
