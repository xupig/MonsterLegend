namespace Mogo.AI.BT
{
	public sealed class BT100 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT100 _instance = null;
		public static BT100 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT100();

				return _instance;
			}
		}

		private BT100()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				node1.AddChild(new Mogo.AI.HasTouchMovePoint());
				node1.AddChild(new Mogo.AI.SetTouchMovePoint());
				{
					Mogo.AI.PrioritySelectorNode node4 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node4);
					{
						Mogo.AI.SequenceNode node5 = new Mogo.AI.SequenceNode();
						node4.AddChild(node5);
						node5.AddChild(new Mogo.AI.CmpTargetPointDistance(Mogo.AI.CmpType.gt,50));
						node5.AddChild(new Mogo.AI.MoveTo(1f));
					}
					node4.AddChild(new Mogo.AI.DeleteTouchMovePoint());
				}
			}
		}
	}
}
