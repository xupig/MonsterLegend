namespace Mogo.AI.BT
{
	public sealed class BT5004 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT5004 _instance = null;
		public static BT5004 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT5004();

				return _instance;
			}
		}

		private BT5004()
		{
			{
				Mogo.AI.PrioritySelectorNode node1 = new Mogo.AI.PrioritySelectorNode();
				this.AddChild(node1);
				{
					Mogo.AI.SequenceNode node2 = new Mogo.AI.SequenceNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.MoveToAppoint("146,120,144"));
					node2.AddChild(new Mogo.AI.CmpTargetPointDistance(Mogo.AI.CmpType.gt,100));
					node2.AddChild(new Mogo.AI.MoveTo(1.5f));
				}
				node1.AddChild(new Mogo.AI.EnterCD(500));
			}
		}
	}
}
