namespace Mogo.AI.BT
{
	public sealed class BT2410 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT2410 _instance = null;
		public static BT2410 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT2410();

				return _instance;
			}
		}

		private BT2410()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.FindTargetByMonsterID(12112,20000));
					node2.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
				}
				node1.AddChild(new Mogo.AI.IsTargetCanBeAttack());
				{
					Mogo.AI.PrioritySelectorNode node6 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node6);
					{
						Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
						node6.AddChild(node7);
						node7.AddChild(new Mogo.AI.InSkillCoolDown(3));
						node7.AddChild(new Mogo.AI.InSkillRange(3));
						node7.AddChild(new Mogo.AI.CastSpell(3,0));
						node7.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node12 = new Mogo.AI.SequenceNode();
						node6.AddChild(node12);
						node12.AddChild(new Mogo.AI.InSkillCoolDown(2));
						node12.AddChild(new Mogo.AI.InSkillRange(2));
						node12.AddChild(new Mogo.AI.CastSpell(2,0));
						node12.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node17 = new Mogo.AI.SequenceNode();
						node6.AddChild(node17);
						node17.AddChild(new Mogo.AI.InSkillCoolDown(1));
						node17.AddChild(new Mogo.AI.InSkillRange(1));
						node17.AddChild(new Mogo.AI.CastSpell(1,0));
						node17.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node22 = new Mogo.AI.SequenceNode();
						node6.AddChild(node22);
						node22.AddChild(new Mogo.AI.InSkillRange(1));
						{
							Mogo.AI.PrioritySelectorNode node24 = new Mogo.AI.PrioritySelectorNode();
							node22.AddChild(node24);
							{
								Mogo.AI.SequenceNode node25 = new Mogo.AI.SequenceNode();
								node24.AddChild(node25);
								node25.AddChild(new Mogo.AI.HasFriendInLeftRange(50));
								{
									Mogo.AI.PrioritySelectorNode node27 = new Mogo.AI.PrioritySelectorNode();
									node25.AddChild(node27);
									{
										Mogo.AI.SequenceNode node28 = new Mogo.AI.SequenceNode();
										node27.AddChild(node28);
										{
											Mogo.AI.Not node29 = new Mogo.AI.Not();
											node28.AddChild(node29);
											node29.Proxy(new Mogo.AI.HasFriendInRightRange(50));
										}
										node28.AddChild(new Mogo.AI.LookOn(100,200,0,0,0,100,0,0,500,500,0,300,0,1));
									}
									node27.AddChild(new Mogo.AI.EnterCD(500));
								}
							}
							{
								Mogo.AI.SequenceNode node33 = new Mogo.AI.SequenceNode();
								node24.AddChild(node33);
								node33.AddChild(new Mogo.AI.HasFriendInRightRange(0));
								node33.AddChild(new Mogo.AI.LookOn(100,200,0,0,100,0,0,0,500,500,300,0,0,1));
							}
							node24.AddChild(new Mogo.AI.LookOn(150,800,0,0,0,0,100,0,500,500,500,500,500,1));
						}
					}
					{
						Mogo.AI.SequenceNode node37 = new Mogo.AI.SequenceNode();
						node6.AddChild(node37);
						{
							Mogo.AI.Not node38 = new Mogo.AI.Not();
							node37.AddChild(node38);
							node38.Proxy(new Mogo.AI.InSkillRange(1));
						}
						node37.AddChild(new Mogo.AI.ChooseCastPoint(1));
						node37.AddChild(new Mogo.AI.MoveTo(1f));
					}
				}
			}
		}
	}
}
