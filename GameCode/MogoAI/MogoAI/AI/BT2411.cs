namespace Mogo.AI.BT
{
	public sealed class BT2411 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT2411 _instance = null;
		public static BT2411 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT2411();

				return _instance;
			}
		}

		private BT2411()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				node1.AddChild(new Mogo.AI.AddBuff(4901,5000));
				node1.AddChild(new Mogo.AI.FindTargetByMonsterID(4002,20000));
				node1.AddChild(new Mogo.AI.MoveToTargetPoint());
				{
					Mogo.AI.PrioritySelectorNode node5 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node5);
					{
						Mogo.AI.SequenceNode node6 = new Mogo.AI.SequenceNode();
						node5.AddChild(node6);
						node6.AddChild(new Mogo.AI.CmpTargetPointDistance(Mogo.AI.CmpType.gt,100));
						node6.AddChild(new Mogo.AI.MoveTo(1.5f));
					}
					node5.AddChild(new Mogo.AI.AddBuff(5398,3000));
				}
			}
		}
	}
}
