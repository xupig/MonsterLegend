namespace Mogo.AI.BT
{
	public sealed class BT2001 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT2001 _instance = null;
		public static BT2001 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT2001();

				return _instance;
			}
		}

		private BT2001()
		{
			{
				Mogo.AI.PrioritySelectorNode node1 = new Mogo.AI.PrioritySelectorNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					{
						Mogo.AI.SequenceNode node3 = new Mogo.AI.SequenceNode();
						node2.AddChild(node3);
						node3.AddChild(new Mogo.AI.IsInChase());
						node3.AddChild(new Mogo.AI.ChaseMaster(600,1));
					}
					{
						Mogo.AI.SequenceNode node6 = new Mogo.AI.SequenceNode();
						node2.AddChild(node6);
						node6.AddChild(new Mogo.AI.CmpMasterDistance(Mogo.AI.CmpType.ge,2500));
						node6.AddChild(new Mogo.AI.ChaseMaster(500,1));
					}
				}
				{
					Mogo.AI.SequenceNode node9 = new Mogo.AI.SequenceNode();
					node1.AddChild(node9);
					node9.AddChild(new Mogo.AI.PetAOI(100,0,1500,0,0,0));
					node9.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node12 = new Mogo.AI.PrioritySelectorNode();
						node9.AddChild(node12);
						{
							Mogo.AI.SequenceNode node13 = new Mogo.AI.SequenceNode();
							node12.AddChild(node13);
							node13.AddChild(new Mogo.AI.InSkillCoolDown(7));
							node13.AddChild(new Mogo.AI.InSkillRange(7));
							node13.AddChild(new Mogo.AI.CastSpell(7,0));
							node13.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node18 = new Mogo.AI.SequenceNode();
							node12.AddChild(node18);
							node18.AddChild(new Mogo.AI.InSkillCoolDown(6));
							node18.AddChild(new Mogo.AI.InSkillRange(6));
							node18.AddChild(new Mogo.AI.CastSpell(6,0));
							node18.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node23 = new Mogo.AI.SequenceNode();
							node12.AddChild(node23);
							node23.AddChild(new Mogo.AI.InSkillCoolDown(5));
							node23.AddChild(new Mogo.AI.InSkillRange(5));
							node23.AddChild(new Mogo.AI.CastSpell(5,0));
							node23.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node28 = new Mogo.AI.SequenceNode();
							node12.AddChild(node28);
							node28.AddChild(new Mogo.AI.InSkillCoolDown(4));
							node28.AddChild(new Mogo.AI.InSkillRange(4));
							node28.AddChild(new Mogo.AI.CastSpell(4,0));
							node28.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node33 = new Mogo.AI.SequenceNode();
							node12.AddChild(node33);
							node33.AddChild(new Mogo.AI.InSkillCoolDown(3));
							node33.AddChild(new Mogo.AI.InSkillRange(3));
							node33.AddChild(new Mogo.AI.CastSpell(3,0));
							node33.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node38 = new Mogo.AI.SequenceNode();
							node12.AddChild(node38);
							node38.AddChild(new Mogo.AI.InSkillCoolDown(2));
							node38.AddChild(new Mogo.AI.InSkillRange(2));
							node38.AddChild(new Mogo.AI.CastSpell(2,0));
							node38.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node43 = new Mogo.AI.SequenceNode();
							node12.AddChild(node43);
							node43.AddChild(new Mogo.AI.InSkillCoolDown(1));
							node43.AddChild(new Mogo.AI.InSkillRange(1));
							node43.AddChild(new Mogo.AI.CastSpell(1,0));
							node43.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node48 = new Mogo.AI.SequenceNode();
							node12.AddChild(node48);
							{
								Mogo.AI.Not node49 = new Mogo.AI.Not();
								node48.AddChild(node49);
								node49.Proxy(new Mogo.AI.InSkillRange(1));
							}
							node48.AddChild(new Mogo.AI.ChooseCastPoint(1));
							node48.AddChild(new Mogo.AI.MoveTo(1f));
						}
						node12.AddChild(new Mogo.AI.EnterRest(500));
					}
				}
				node1.AddChild(new Mogo.AI.ChaseMaster(200,0));
				{
					Mogo.AI.SequenceNode node55 = new Mogo.AI.SequenceNode();
					node1.AddChild(node55);
					node55.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
					node55.AddChild(new Mogo.AI.Patrol(300,600,400));
				}
			}
		}
	}
}
