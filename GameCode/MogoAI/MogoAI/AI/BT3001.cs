namespace Mogo.AI.BT
{
	public sealed class BT3001 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT3001 _instance = null;
		public static BT3001 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT3001();

				return _instance;
			}
		}

		private BT3001()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.IsFightState());
					{
						Mogo.AI.SequenceNode node4 = new Mogo.AI.SequenceNode();
						node2.AddChild(node4);
						node4.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
						node4.AddChild(new Mogo.AI.EnterFight());
					}
				}
				{
					Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
					node1.AddChild(node7);
					node7.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
					node7.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node10 = new Mogo.AI.PrioritySelectorNode();
						node7.AddChild(node10);
						{
							Mogo.AI.SequenceNode node11 = new Mogo.AI.SequenceNode();
							node10.AddChild(node11);
							node11.AddChild(new Mogo.AI.InSkillCoolDown(7));
							node11.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,70));
							node11.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.gt,30));
							{
								Mogo.AI.PrioritySelectorNode node15 = new Mogo.AI.PrioritySelectorNode();
								node11.AddChild(node15);
								{
									Mogo.AI.SequenceNode node16 = new Mogo.AI.SequenceNode();
									node15.AddChild(node16);
									node16.AddChild(new Mogo.AI.MoveToAppoint("58,47,67"));
									node16.AddChild(new Mogo.AI.CmpTargetPointDistance(Mogo.AI.CmpType.gt,50));
									node16.AddChild(new Mogo.AI.AddBuff(4002,3000));
									node16.AddChild(new Mogo.AI.CancelAttackTarget());
									node16.AddChild(new Mogo.AI.MoveTo(1.5f));
								}
								{
									Mogo.AI.SequenceNode node22 = new Mogo.AI.SequenceNode();
									node15.AddChild(node22);
									node22.AddChild(new Mogo.AI.InSkillCoolDown(7));
									node22.AddChild(new Mogo.AI.CastSpell(7,0));
									node22.AddChild(new Mogo.AI.EnterCD(0));
								}
							}
						}
						{
							Mogo.AI.SequenceNode node26 = new Mogo.AI.SequenceNode();
							node10.AddChild(node26);
							{
								Mogo.AI.Not node27 = new Mogo.AI.Not();
								node26.AddChild(node27);
								node27.Proxy(new Mogo.AI.BossPartHasBeenBroken(108));
							}
							node26.AddChild(new Mogo.AI.CmpTargetDistance(Mogo.AI.CmpType.gt,500));
							node26.AddChild(new Mogo.AI.InSkillCoolDown(6));
							node26.AddChild(new Mogo.AI.InSkillRange(6));
							node26.AddChild(new Mogo.AI.CastSpell(6,0));
							node26.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node34 = new Mogo.AI.SequenceNode();
							node10.AddChild(node34);
							{
								Mogo.AI.Not node35 = new Mogo.AI.Not();
								node34.AddChild(node35);
								node35.Proxy(new Mogo.AI.BossPartHasBeenBroken(107));
							}
							node34.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,30));
							node34.AddChild(new Mogo.AI.InSkillCoolDown(3));
							node34.AddChild(new Mogo.AI.InSkillRange(3));
							node34.AddChild(new Mogo.AI.CastSpell(3,0));
							node34.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node42 = new Mogo.AI.SequenceNode();
							node10.AddChild(node42);
							{
								Mogo.AI.Not node43 = new Mogo.AI.Not();
								node42.AddChild(node43);
								node43.Proxy(new Mogo.AI.BossPartHasBeenBroken(109));
							}
							node42.AddChild(new Mogo.AI.InSkillCoolDown(5));
							node42.AddChild(new Mogo.AI.InSkillRange(5));
							node42.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
							node42.AddChild(new Mogo.AI.CastSpell(5,0));
							node42.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node50 = new Mogo.AI.SequenceNode();
							node10.AddChild(node50);
							node50.AddChild(new Mogo.AI.InSkillCoolDown(4));
							node50.AddChild(new Mogo.AI.InSkillRange(4));
							node50.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
							node50.AddChild(new Mogo.AI.CastSpell(4,0));
							node50.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node56 = new Mogo.AI.SequenceNode();
							node10.AddChild(node56);
							node56.AddChild(new Mogo.AI.InSkillCoolDown(2));
							node56.AddChild(new Mogo.AI.InSkillRange(2));
							node56.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
							node56.AddChild(new Mogo.AI.CastSpell(2,0));
							node56.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node62 = new Mogo.AI.SequenceNode();
							node10.AddChild(node62);
							node62.AddChild(new Mogo.AI.InSkillCoolDown(1));
							node62.AddChild(new Mogo.AI.InSkillRange(1));
							node62.AddChild(new Mogo.AI.CastSpell(1,0));
							node62.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node67 = new Mogo.AI.SequenceNode();
							node10.AddChild(node67);
							{
								Mogo.AI.Not node68 = new Mogo.AI.Not();
								node67.AddChild(node68);
								node68.Proxy(new Mogo.AI.InSkillRange(1));
							}
							node67.AddChild(new Mogo.AI.ChooseCastPoint(1));
							node67.AddChild(new Mogo.AI.MoveTo(1f));
						}
						node10.AddChild(new Mogo.AI.EnterRest(500));
					}
				}
			}
		}
	}
}
