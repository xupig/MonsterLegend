namespace Mogo.AI.BT
{
	public sealed class BT1665 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1665 _instance = null;
		public static BT1665 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1665();

				return _instance;
			}
		}

		private BT1665()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.IsFightState());
					{
						Mogo.AI.SequenceNode node4 = new Mogo.AI.SequenceNode();
						node2.AddChild(node4);
						node4.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
						node4.AddChild(new Mogo.AI.EnterFight());
					}
				}
				{
					Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
					node1.AddChild(node7);
					node7.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
					node7.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node10 = new Mogo.AI.PrioritySelectorNode();
						node7.AddChild(node10);
						{
							Mogo.AI.SequenceNode node11 = new Mogo.AI.SequenceNode();
							node10.AddChild(node11);
							node11.AddChild(new Mogo.AI.InSkillCoolDown(4));
							node11.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,80));
							node11.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.gt,20));
							{
								Mogo.AI.PrioritySelectorNode node15 = new Mogo.AI.PrioritySelectorNode();
								node11.AddChild(node15);
								{
									Mogo.AI.SequenceNode node16 = new Mogo.AI.SequenceNode();
									node15.AddChild(node16);
									node16.AddChild(new Mogo.AI.MoveToAppoint("62,10,30"));
									node16.AddChild(new Mogo.AI.CmpTargetPointDistance(Mogo.AI.CmpType.gt,100));
									node16.AddChild(new Mogo.AI.CancelAttackTarget());
									node16.AddChild(new Mogo.AI.MoveTo(1.5f));
								}
								{
									Mogo.AI.SequenceNode node21 = new Mogo.AI.SequenceNode();
									node15.AddChild(node21);
									node21.AddChild(new Mogo.AI.InSkillCoolDown(4));
									node21.AddChild(new Mogo.AI.CastSpell(4,0));
									node21.AddChild(new Mogo.AI.EnterCD(0));
								}
							}
						}
						{
							Mogo.AI.SequenceNode node25 = new Mogo.AI.SequenceNode();
							node10.AddChild(node25);
							node25.AddChild(new Mogo.AI.InSkillCoolDown(3));
							node25.AddChild(new Mogo.AI.InSkillRange(3));
							node25.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,40));
							node25.AddChild(new Mogo.AI.CastSpell(3,0));
							node25.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node31 = new Mogo.AI.SequenceNode();
							node10.AddChild(node31);
							node31.AddChild(new Mogo.AI.InSkillCoolDown(2));
							node31.AddChild(new Mogo.AI.InSkillRange(2));
							node31.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
							node31.AddChild(new Mogo.AI.CastSpell(2,0));
							node31.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node37 = new Mogo.AI.SequenceNode();
							node10.AddChild(node37);
							node37.AddChild(new Mogo.AI.InSkillCoolDown(1));
							node37.AddChild(new Mogo.AI.InSkillRange(1));
							node37.AddChild(new Mogo.AI.CastSpell(1,0));
							node37.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node42 = new Mogo.AI.SequenceNode();
							node10.AddChild(node42);
							{
								Mogo.AI.Not node43 = new Mogo.AI.Not();
								node42.AddChild(node43);
								node43.Proxy(new Mogo.AI.InSkillRange(1));
							}
							node42.AddChild(new Mogo.AI.ChooseCastPoint(1));
							node42.AddChild(new Mogo.AI.MoveTo(1f));
						}
						node10.AddChild(new Mogo.AI.EnterRest(500));
					}
				}
			}
		}
	}
}
