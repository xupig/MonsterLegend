namespace Mogo.AI.BT
{
	public sealed class BT10007 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT10007 _instance = null;
		public static BT10007 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT10007();

				return _instance;
			}
		}

		private BT10007()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.Not node2 = new Mogo.AI.Not();
					node1.AddChild(node2);
					node2.Proxy(new Mogo.AI.IsCD());
				}
				node1.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
				node1.AddChild(new Mogo.AI.IsTargetCanBeAttack());
				{
					Mogo.AI.PrioritySelectorNode node6 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node6);
					{
						Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
						node6.AddChild(node7);
						node7.AddChild(new Mogo.AI.InSkillCoolDown(1));
						node7.AddChild(new Mogo.AI.InSkillRange(1));
						node7.AddChild(new Mogo.AI.CastSpell(1,0));
						node7.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node12 = new Mogo.AI.SequenceNode();
						node6.AddChild(node12);
						node12.AddChild(new Mogo.AI.ChooseCastPoint(1));
						node12.AddChild(new Mogo.AI.MoveTo(1f));
					}
				}
			}
		}
	}
}
