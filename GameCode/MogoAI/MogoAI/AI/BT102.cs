namespace Mogo.AI.BT
{
	public sealed class BT102 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT102 _instance = null;
		public static BT102 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT102();

				return _instance;
			}
		}

		private BT102()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
					{
						Mogo.AI.SequenceNode node4 = new Mogo.AI.SequenceNode();
						node2.AddChild(node4);
						{
							Mogo.AI.Not node5 = new Mogo.AI.Not();
							node4.AddChild(node5);
							node5.Proxy(new Mogo.AI.SelectAutoFightMovePoint());
						}
						{
							Mogo.AI.Not node7 = new Mogo.AI.Not();
							node4.AddChild(node7);
							node7.Proxy(new Mogo.AI.EnterCD(1000));
						}
					}
				}
				{
					Mogo.AI.PrioritySelectorNode node9 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node9);
					{
						Mogo.AI.SequenceNode node10 = new Mogo.AI.SequenceNode();
						node9.AddChild(node10);
						node10.AddChild(new Mogo.AI.InSkillCoolDown(4));
						node10.AddChild(new Mogo.AI.InSkillRange(4));
						node10.AddChild(new Mogo.AI.CastSpell(4,0));
						node10.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node15 = new Mogo.AI.SequenceNode();
						node9.AddChild(node15);
						node15.AddChild(new Mogo.AI.InSkillCoolDown(3));
						node15.AddChild(new Mogo.AI.InSkillRange(3));
						node15.AddChild(new Mogo.AI.CastSpell(3,0));
						node15.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node20 = new Mogo.AI.SequenceNode();
						node9.AddChild(node20);
						node20.AddChild(new Mogo.AI.InSkillCoolDown(1));
						node20.AddChild(new Mogo.AI.InSkillRange(1));
						node20.AddChild(new Mogo.AI.CastSpell(1,0));
						node20.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node25 = new Mogo.AI.SequenceNode();
						node9.AddChild(node25);
						node25.AddChild(new Mogo.AI.ChooseCastPoint(1));
						node25.AddChild(new Mogo.AI.MoveTo(1f));
					}
				}
			}
		}
	}
}
