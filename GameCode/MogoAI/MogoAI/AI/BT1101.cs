namespace Mogo.AI.BT
{
	public sealed class BT1101 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1101 _instance = null;
		public static BT1101 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1101();

				return _instance;
			}
		}

		private BT1101()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				node1.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
				node1.AddChild(new Mogo.AI.IsTargetCanBeAttack());
				{
					Mogo.AI.SequenceNode node4 = new Mogo.AI.SequenceNode();
					node1.AddChild(node4);
					node4.AddChild(new Mogo.AI.InSkillCoolDown(1));
					node4.AddChild(new Mogo.AI.InSkillRange(1));
					node4.AddChild(new Mogo.AI.CastSpell(1,0));
				}
				node1.AddChild(new Mogo.AI.EnterRest(500));
			}
		}
	}
}
