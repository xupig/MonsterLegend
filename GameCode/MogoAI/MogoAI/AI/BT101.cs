namespace Mogo.AI.BT
{
	public sealed class BT101 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT101 _instance = null;
		public static BT101 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT101();

				return _instance;
			}
		}

		private BT101()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				node1.AddChild(new Mogo.AI.IsFightState());
				node1.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
				{
					Mogo.AI.PrioritySelectorNode node4 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node4);
					{
						Mogo.AI.SequenceNode node5 = new Mogo.AI.SequenceNode();
						node4.AddChild(node5);
						node5.AddChild(new Mogo.AI.InSkillCoolDown(1));
						node5.AddChild(new Mogo.AI.InSkillRange(1));
						node5.AddChild(new Mogo.AI.CastSpell(1,0));
						node5.AddChild(new Mogo.AI.EnterCD(300));
					}
					{
						Mogo.AI.SequenceNode node10 = new Mogo.AI.SequenceNode();
						node4.AddChild(node10);
						node10.AddChild(new Mogo.AI.ChooseCastPoint(1));
						node10.AddChild(new Mogo.AI.MoveTo(1f));
					}
				}
			}
		}
	}
}
