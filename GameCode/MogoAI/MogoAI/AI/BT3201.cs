namespace Mogo.AI.BT
{
	public sealed class BT3201 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT3201 _instance = null;
		public static BT3201 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT3201();

				return _instance;
			}
		}

		private BT3201()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.IsFightState());
					{
						Mogo.AI.SequenceNode node4 = new Mogo.AI.SequenceNode();
						node2.AddChild(node4);
						node4.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
						node4.AddChild(new Mogo.AI.EnterFight());
					}
				}
				{
					Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
					node1.AddChild(node7);
					node7.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
					node7.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node10 = new Mogo.AI.PrioritySelectorNode();
						node7.AddChild(node10);
						{
							Mogo.AI.SequenceNode node11 = new Mogo.AI.SequenceNode();
							node10.AddChild(node11);
							node11.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,80));
							node11.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.gt,15));
							{
								Mogo.AI.Not node14 = new Mogo.AI.Not();
								node11.AddChild(node14);
								node14.Proxy(new Mogo.AI.TestBuff(0,4748));
							}
							{
								Mogo.AI.PrioritySelectorNode node16 = new Mogo.AI.PrioritySelectorNode();
								node11.AddChild(node16);
								{
									Mogo.AI.SequenceNode node17 = new Mogo.AI.SequenceNode();
									node16.AddChild(node17);
									node17.AddChild(new Mogo.AI.MoveToAppoint("32,4,23"));
									node17.AddChild(new Mogo.AI.CmpTargetPointDistance(Mogo.AI.CmpType.gt,100));
									node17.AddChild(new Mogo.AI.AddBuff(4002,3000));
									node17.AddChild(new Mogo.AI.CancelAttackTarget());
									node17.AddChild(new Mogo.AI.MoveTo(1.5f));
								}
								{
									Mogo.AI.PrioritySelectorNode node23 = new Mogo.AI.PrioritySelectorNode();
									node16.AddChild(node23);
									{
										Mogo.AI.SequenceNode node24 = new Mogo.AI.SequenceNode();
										node23.AddChild(node24);
										node24.AddChild(new Mogo.AI.InSkillCoolDown(6));
										node24.AddChild(new Mogo.AI.TestBuff(0,4712));
										node24.AddChild(new Mogo.AI.CastSpell(6,0));
										node24.AddChild(new Mogo.AI.EnterCD(0));
									}
									{
										Mogo.AI.SequenceNode node29 = new Mogo.AI.SequenceNode();
										node23.AddChild(node29);
										node29.AddChild(new Mogo.AI.InSkillCoolDown(4));
										node29.AddChild(new Mogo.AI.TestBuff(0,4713));
										node29.AddChild(new Mogo.AI.CastSpell(4,0));
										node29.AddChild(new Mogo.AI.EnterCD(0));
									}
									{
										Mogo.AI.SequenceNode node34 = new Mogo.AI.SequenceNode();
										node23.AddChild(node34);
										node34.AddChild(new Mogo.AI.InSkillCoolDown(5));
										node34.AddChild(new Mogo.AI.TestBuff(0,4713));
										node34.AddChild(new Mogo.AI.CastSpell(5,0));
										node34.AddChild(new Mogo.AI.EnterCD(0));
									}
									{
										Mogo.AI.SequenceNode node39 = new Mogo.AI.SequenceNode();
										node23.AddChild(node39);
										node39.AddChild(new Mogo.AI.InSkillCoolDown(7));
										node39.AddChild(new Mogo.AI.AddBuff(4712,20000));
										node39.AddChild(new Mogo.AI.CastSpell(7,0));
										node39.AddChild(new Mogo.AI.EnterCD(0));
									}
									{
										Mogo.AI.SequenceNode node44 = new Mogo.AI.SequenceNode();
										node23.AddChild(node44);
										node44.AddChild(new Mogo.AI.InSkillCoolDown(8));
										node44.AddChild(new Mogo.AI.AddBuff(4713,20000));
										node44.AddChild(new Mogo.AI.CastSpell(8,0));
										node44.AddChild(new Mogo.AI.EnterCD(0));
									}
								}
							}
						}
						{
							Mogo.AI.SequenceNode node49 = new Mogo.AI.SequenceNode();
							node10.AddChild(node49);
							{
								Mogo.AI.Not node50 = new Mogo.AI.Not();
								node49.AddChild(node50);
								node50.Proxy(new Mogo.AI.BossPartHasBeenBroken(104));
							}
							node49.AddChild(new Mogo.AI.InSkillCoolDown(3));
							node49.AddChild(new Mogo.AI.InSkillRange(3));
							node49.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
							node49.AddChild(new Mogo.AI.CastSpell(3,0));
							node49.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node57 = new Mogo.AI.SequenceNode();
							node10.AddChild(node57);
							{
								Mogo.AI.Not node58 = new Mogo.AI.Not();
								node57.AddChild(node58);
								node58.Proxy(new Mogo.AI.BossPartHasBeenBroken(106));
							}
							node57.AddChild(new Mogo.AI.InSkillCoolDown(2));
							node57.AddChild(new Mogo.AI.InSkillRange(2));
							node57.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
							node57.AddChild(new Mogo.AI.CastSpell(2,0));
							node57.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node65 = new Mogo.AI.SequenceNode();
							node10.AddChild(node65);
							node65.AddChild(new Mogo.AI.InSkillCoolDown(1));
							node65.AddChild(new Mogo.AI.InSkillRange(1));
							node65.AddChild(new Mogo.AI.CastSpell(1,0));
							node65.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node70 = new Mogo.AI.SequenceNode();
							node10.AddChild(node70);
							{
								Mogo.AI.Not node71 = new Mogo.AI.Not();
								node70.AddChild(node71);
								node71.Proxy(new Mogo.AI.InSkillRange(1));
							}
							node70.AddChild(new Mogo.AI.ChooseCastPoint(1));
							node70.AddChild(new Mogo.AI.MoveTo(1f));
						}
						node10.AddChild(new Mogo.AI.EnterRest(500));
					}
				}
			}
		}
	}
}
