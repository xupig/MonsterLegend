namespace Mogo.AI.BT
{
	public sealed class BT1402 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1402 _instance = null;
		public static BT1402 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1402();

				return _instance;
			}
		}

		private BT1402()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					{
						Mogo.AI.SequenceNode node3 = new Mogo.AI.SequenceNode();
						node2.AddChild(node3);
						node3.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
						node3.AddChild(new Mogo.AI.AOI(100,2,0,"0",0));
						node3.AddChild(new Mogo.AI.MonsterSpeech(5,1));
					}
					{
						Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
						node2.AddChild(node7);
						node7.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.le,100));
					}
				}
				{
					Mogo.AI.PrioritySelectorNode node9 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node9);
					{
						Mogo.AI.PrioritySelectorNode node10 = new Mogo.AI.PrioritySelectorNode();
						node9.AddChild(node10);
						{
							Mogo.AI.SequenceNode node11 = new Mogo.AI.SequenceNode();
							node10.AddChild(node11);
							node11.AddChild(new Mogo.AI.MoveToAppoint("159,39,70"));
							node11.AddChild(new Mogo.AI.CmpTargetPointDistance(Mogo.AI.CmpType.gt,50));
							node11.AddChild(new Mogo.AI.AddBuff(4002,3000));
							node11.AddChild(new Mogo.AI.CancelAttackTarget());
							node11.AddChild(new Mogo.AI.MoveTo(1.5f));
						}
						node10.AddChild(new Mogo.AI.EnterRest(500));
					}
					{
						Mogo.AI.SequenceNode node18 = new Mogo.AI.SequenceNode();
						node9.AddChild(node18);
						node18.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.le,50));
						node18.AddChild(new Mogo.AI.Patrol(5000,10000,100));
					}
				}
			}
		}
	}
}
