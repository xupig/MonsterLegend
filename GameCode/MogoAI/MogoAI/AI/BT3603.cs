namespace Mogo.AI.BT
{
	public sealed class BT3603 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT3603 _instance = null;
		public static BT3603 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT3603();

				return _instance;
			}
		}

		private BT3603()
		{
			{
				Mogo.AI.PrioritySelectorNode node1 = new Mogo.AI.PrioritySelectorNode();
				this.AddChild(node1);
				{
					Mogo.AI.SequenceNode node2 = new Mogo.AI.SequenceNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.MoveToAppoint("59,18,48"));
					node2.AddChild(new Mogo.AI.CmpTargetPointDistance(Mogo.AI.CmpType.gt,50));
					node2.AddChild(new Mogo.AI.AddBuff(4002,3000));
					node2.AddChild(new Mogo.AI.MoveTo(1.5f));
				}
				{
					Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
					node1.AddChild(node7);
					node7.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
					{
						Mogo.AI.PrioritySelectorNode node9 = new Mogo.AI.PrioritySelectorNode();
						node7.AddChild(node9);
						{
							Mogo.AI.SequenceNode node10 = new Mogo.AI.SequenceNode();
							node9.AddChild(node10);
							node10.AddChild(new Mogo.AI.InSkillCoolDown(8));
							node10.AddChild(new Mogo.AI.MonsterSpeech(5,1));
							node10.AddChild(new Mogo.AI.CastSpell(8,0));
							node10.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node15 = new Mogo.AI.SequenceNode();
							node9.AddChild(node15);
							node15.AddChild(new Mogo.AI.InSkillCoolDown(2));
							node15.AddChild(new Mogo.AI.CastSpell(2,0));
							node15.AddChild(new Mogo.AI.EnterCD(0));
						}
					}
				}
			}
		}
	}
}
