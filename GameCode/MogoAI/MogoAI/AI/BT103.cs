namespace Mogo.AI.BT
{
	public sealed class BT103 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT103 _instance = null;
		public static BT103 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT103();

				return _instance;
			}
		}

		private BT103()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
					{
						Mogo.AI.SequenceNode node4 = new Mogo.AI.SequenceNode();
						node2.AddChild(node4);
						{
							Mogo.AI.Not node5 = new Mogo.AI.Not();
							node4.AddChild(node5);
							node5.Proxy(new Mogo.AI.SelectAutoFightMovePoint());
						}
						{
							Mogo.AI.Not node7 = new Mogo.AI.Not();
							node4.AddChild(node7);
							node7.Proxy(new Mogo.AI.EnterCD(1000));
						}
					}
				}
				{
					Mogo.AI.PrioritySelectorNode node9 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node9);
					{
						Mogo.AI.SequenceNode node10 = new Mogo.AI.SequenceNode();
						node9.AddChild(node10);
						node10.AddChild(new Mogo.AI.InSkillCoolDown(4));
						node10.AddChild(new Mogo.AI.InSkillRange(4));
						node10.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,60));
						node10.AddChild(new Mogo.AI.CastSpell(4,0));
						node10.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node16 = new Mogo.AI.SequenceNode();
						node9.AddChild(node16);
						node16.AddChild(new Mogo.AI.InSkillCoolDown(3));
						node16.AddChild(new Mogo.AI.InSkillRange(3));
						node16.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,60));
						node16.AddChild(new Mogo.AI.CastSpell(3,0));
						node16.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node22 = new Mogo.AI.SequenceNode();
						node9.AddChild(node22);
						node22.AddChild(new Mogo.AI.InSkillCoolDown(1));
						node22.AddChild(new Mogo.AI.InSkillRange(1));
						node22.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,60));
						node22.AddChild(new Mogo.AI.CastSpell(1,0));
						node22.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node28 = new Mogo.AI.SequenceNode();
						node9.AddChild(node28);
						node28.AddChild(new Mogo.AI.ChooseCastPoint(1));
						node28.AddChild(new Mogo.AI.MoveTo(1f));
					}
				}
			}
		}
	}
}
