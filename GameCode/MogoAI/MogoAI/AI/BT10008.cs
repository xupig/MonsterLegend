namespace Mogo.AI.BT
{
	public sealed class BT10008 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT10008 _instance = null;
		public static BT10008 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT10008();

				return _instance;
			}
		}

		private BT10008()
		{
			{
				Mogo.AI.PrioritySelectorNode node1 = new Mogo.AI.PrioritySelectorNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					{
						Mogo.AI.SequenceNode node3 = new Mogo.AI.SequenceNode();
						node2.AddChild(node3);
						node3.AddChild(new Mogo.AI.IsForceBackState());
						node3.AddChild(new Mogo.AI.MonsterBackBornPoint(0,100));
					}
					{
						Mogo.AI.SequenceNode node6 = new Mogo.AI.SequenceNode();
						node2.AddChild(node6);
						node6.AddChild(new Mogo.AI.CmpBornPointDistance(Mogo.AI.CmpType.ge,550));
						node6.AddChild(new Mogo.AI.CancelFightState());
						node6.AddChild(new Mogo.AI.AddBuff(4110,1500));
						node6.AddChild(new Mogo.AI.MonsterBackBornPoint(1,100));
					}
				}
				{
					Mogo.AI.SequenceNode node11 = new Mogo.AI.SequenceNode();
					node1.AddChild(node11);
					{
						Mogo.AI.PrioritySelectorNode node12 = new Mogo.AI.PrioritySelectorNode();
						node11.AddChild(node12);
						node12.AddChild(new Mogo.AI.IsFightState());
						{
							Mogo.AI.SequenceNode node14 = new Mogo.AI.SequenceNode();
							node12.AddChild(node14);
							node14.AddChild(new Mogo.AI.AOI(100,1,0,"500",0));
							node14.AddChild(new Mogo.AI.EnterFight());
						}
						node12.AddChild(new Mogo.AI.MonsterBackBornPoint(1,100));
					}
					{
						Mogo.AI.SequenceNode node18 = new Mogo.AI.SequenceNode();
						node11.AddChild(node18);
						node18.AddChild(new Mogo.AI.AOI(100,1,0,"500",0));
						node18.AddChild(new Mogo.AI.IsTargetCanBeAttack());
						{
							Mogo.AI.PrioritySelectorNode node21 = new Mogo.AI.PrioritySelectorNode();
							node18.AddChild(node21);
							{
								Mogo.AI.SequenceNode node22 = new Mogo.AI.SequenceNode();
								node21.AddChild(node22);
								node22.AddChild(new Mogo.AI.InSkillCoolDown(5));
								node22.AddChild(new Mogo.AI.InSkillRange(5));
								node22.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
								node22.AddChild(new Mogo.AI.CastSpell(5,0));
								node22.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node28 = new Mogo.AI.SequenceNode();
								node21.AddChild(node28);
								node28.AddChild(new Mogo.AI.InSkillCoolDown(4));
								node28.AddChild(new Mogo.AI.InSkillRange(4));
								node28.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
								node28.AddChild(new Mogo.AI.CastSpell(4,0));
								node28.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node34 = new Mogo.AI.SequenceNode();
								node21.AddChild(node34);
								node34.AddChild(new Mogo.AI.InSkillCoolDown(3));
								node34.AddChild(new Mogo.AI.InSkillRange(3));
								node34.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,40));
								node34.AddChild(new Mogo.AI.CastSpell(3,0));
								node34.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node40 = new Mogo.AI.SequenceNode();
								node21.AddChild(node40);
								node40.AddChild(new Mogo.AI.InSkillCoolDown(2));
								node40.AddChild(new Mogo.AI.InSkillRange(2));
								node40.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
								node40.AddChild(new Mogo.AI.CastSpell(2,0));
								node40.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node46 = new Mogo.AI.SequenceNode();
								node21.AddChild(node46);
								node46.AddChild(new Mogo.AI.InSkillCoolDown(1));
								node46.AddChild(new Mogo.AI.InSkillRange(1));
								node46.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
								node46.AddChild(new Mogo.AI.CastSpell(1,0));
								node46.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node52 = new Mogo.AI.SequenceNode();
								node21.AddChild(node52);
								{
									Mogo.AI.Not node53 = new Mogo.AI.Not();
									node52.AddChild(node53);
									node53.Proxy(new Mogo.AI.InSkillRange(1));
								}
								node52.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
								node52.AddChild(new Mogo.AI.ChooseCastPoint(1));
								node52.AddChild(new Mogo.AI.MoveTo(1f));
							}
							node21.AddChild(new Mogo.AI.EnterRest(500));
						}
					}
				}
			}
		}
	}
}
