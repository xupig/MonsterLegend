namespace Mogo.AI.BT
{
	public sealed class BT1200 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1200 _instance = null;
		public static BT1200 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1200();

				return _instance;
			}
		}

		private BT1200()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.Not node2 = new Mogo.AI.Not();
					node1.AddChild(node2);
					node2.Proxy(new Mogo.AI.CmpLeaderContainsBuff(4903));
				}
				{
					Mogo.AI.Not node4 = new Mogo.AI.Not();
					node1.AddChild(node4);
					node4.Proxy(new Mogo.AI.TestBuff(0,4903));
				}
				{
					Mogo.AI.PrioritySelectorNode node6 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node6);
					{
						Mogo.AI.PrioritySelectorNode node7 = new Mogo.AI.PrioritySelectorNode();
						node6.AddChild(node7);
						{
							Mogo.AI.SequenceNode node8 = new Mogo.AI.SequenceNode();
							node7.AddChild(node8);
							node8.AddChild(new Mogo.AI.IsInChase());
							node8.AddChild(new Mogo.AI.FollowLeader(150,300,450,1));
						}
						{
							Mogo.AI.SequenceNode node11 = new Mogo.AI.SequenceNode();
							node7.AddChild(node11);
							node11.AddChild(new Mogo.AI.CmpMasterDistance(Mogo.AI.CmpType.ge,1000));
							node11.AddChild(new Mogo.AI.FollowLeader(150,300,450,1));
						}
					}
					{
						Mogo.AI.SequenceNode node14 = new Mogo.AI.SequenceNode();
						node6.AddChild(node14);
						node14.AddChild(new Mogo.AI.PetAOI(100,0,500,0,0,0));
						node14.AddChild(new Mogo.AI.IsTargetCanBeAttack());
						{
							Mogo.AI.PrioritySelectorNode node17 = new Mogo.AI.PrioritySelectorNode();
							node14.AddChild(node17);
							{
								Mogo.AI.SequenceNode node18 = new Mogo.AI.SequenceNode();
								node17.AddChild(node18);
								node18.AddChild(new Mogo.AI.InSkillCoolDown(3));
								node18.AddChild(new Mogo.AI.InSkillRange(3));
								node18.AddChild(new Mogo.AI.CastSpell(3,0));
								node18.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node23 = new Mogo.AI.SequenceNode();
								node17.AddChild(node23);
								node23.AddChild(new Mogo.AI.InSkillCoolDown(4));
								node23.AddChild(new Mogo.AI.InSkillRange(4));
								node23.AddChild(new Mogo.AI.CastSpell(4,0));
								node23.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node28 = new Mogo.AI.SequenceNode();
								node17.AddChild(node28);
								node28.AddChild(new Mogo.AI.InSkillCoolDown(1));
								node28.AddChild(new Mogo.AI.InSkillRange(1));
								node28.AddChild(new Mogo.AI.CastSpell(1,0));
								node28.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node33 = new Mogo.AI.SequenceNode();
								node17.AddChild(node33);
								{
									Mogo.AI.Not node34 = new Mogo.AI.Not();
									node33.AddChild(node34);
									node34.Proxy(new Mogo.AI.InSkillRange(1));
								}
								node33.AddChild(new Mogo.AI.ChooseCastPoint(1));
								node33.AddChild(new Mogo.AI.MoveTo(1f));
							}
						}
						node14.AddChild(new Mogo.AI.EnterRest(500));
					}
					node6.AddChild(new Mogo.AI.FollowLeader(150,300,450,0));
				}
			}
		}
	}
}
