namespace Mogo.AI.BT
{
	public sealed class BT1113 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1113 _instance = null;
		public static BT1113 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1113();

				return _instance;
			}
		}

		private BT1113()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				node1.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
				node1.AddChild(new Mogo.AI.IsTargetCanBeAttack());
				{
					Mogo.AI.PrioritySelectorNode node4 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node4);
					{
						Mogo.AI.SequenceNode node5 = new Mogo.AI.SequenceNode();
						node4.AddChild(node5);
						node5.AddChild(new Mogo.AI.TestBuff(0,4730));
						node5.AddChild(new Mogo.AI.InSkillCoolDown(2));
						node5.AddChild(new Mogo.AI.MonsterSpeech(5,1));
						node5.AddChild(new Mogo.AI.CastSpell(2,0));
						node5.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node11 = new Mogo.AI.SequenceNode();
						node4.AddChild(node11);
						node11.AddChild(new Mogo.AI.InSkillCoolDown(1));
						node11.AddChild(new Mogo.AI.InSkillRange(1));
						node11.AddChild(new Mogo.AI.CastSpell(1,0));
						node11.AddChild(new Mogo.AI.EnterCD(0));
					}
					node4.AddChild(new Mogo.AI.EnterRest(500));
				}
			}
		}
	}
}
