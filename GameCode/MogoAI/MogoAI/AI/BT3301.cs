namespace Mogo.AI.BT
{
	public sealed class BT3301 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT3301 _instance = null;
		public static BT3301 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT3301();

				return _instance;
			}
		}

		private BT3301()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.IsFightState());
					{
						Mogo.AI.SequenceNode node4 = new Mogo.AI.SequenceNode();
						node2.AddChild(node4);
						node4.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
						node4.AddChild(new Mogo.AI.EnterFight());
					}
				}
				{
					Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
					node1.AddChild(node7);
					node7.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
					node7.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node10 = new Mogo.AI.PrioritySelectorNode();
						node7.AddChild(node10);
						{
							Mogo.AI.SequenceNode node11 = new Mogo.AI.SequenceNode();
							node10.AddChild(node11);
							node11.AddChild(new Mogo.AI.TestBuff(0,4719));
							node11.AddChild(new Mogo.AI.InSkillCoolDown(9));
							node11.AddChild(new Mogo.AI.CastSpell(9,0));
							node11.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node16 = new Mogo.AI.SequenceNode();
							node10.AddChild(node16);
							node16.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.lt,70));
							node16.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.ge,30));
							{
								Mogo.AI.Not node19 = new Mogo.AI.Not();
								node16.AddChild(node19);
								node19.Proxy(new Mogo.AI.TestBuff(0,4720));
							}
							{
								Mogo.AI.PrioritySelectorNode node21 = new Mogo.AI.PrioritySelectorNode();
								node16.AddChild(node21);
								{
									Mogo.AI.SequenceNode node22 = new Mogo.AI.SequenceNode();
									node21.AddChild(node22);
									node22.AddChild(new Mogo.AI.MoveToAppoint("92,18,57"));
									node22.AddChild(new Mogo.AI.CmpTargetPointDistance(Mogo.AI.CmpType.gt,50));
									node22.AddChild(new Mogo.AI.AddBuff(4002,3000));
									node22.AddChild(new Mogo.AI.CancelAttackTarget());
									node22.AddChild(new Mogo.AI.MoveTo(2f));
								}
								{
									Mogo.AI.SequenceNode node28 = new Mogo.AI.SequenceNode();
									node21.AddChild(node28);
									node28.AddChild(new Mogo.AI.TestBuff(0,4718));
									node28.AddChild(new Mogo.AI.InSkillCoolDown(6));
									node28.AddChild(new Mogo.AI.CastSpell(6,0));
									node28.AddChild(new Mogo.AI.EnterCD(0));
								}
								{
									Mogo.AI.SequenceNode node33 = new Mogo.AI.SequenceNode();
									node21.AddChild(node33);
									node33.AddChild(new Mogo.AI.InSkillCoolDown(8));
									node33.AddChild(new Mogo.AI.CastSpell(8,0));
									node33.AddChild(new Mogo.AI.EnterCD(0));
								}
							}
						}
						{
							Mogo.AI.SequenceNode node37 = new Mogo.AI.SequenceNode();
							node10.AddChild(node37);
							{
								Mogo.AI.Not node38 = new Mogo.AI.Not();
								node37.AddChild(node38);
								node38.Proxy(new Mogo.AI.TestBuff(0,4718));
							}
							{
								Mogo.AI.PrioritySelectorNode node40 = new Mogo.AI.PrioritySelectorNode();
								node37.AddChild(node40);
								{
									Mogo.AI.SequenceNode node41 = new Mogo.AI.SequenceNode();
									node40.AddChild(node41);
									{
										Mogo.AI.Not node42 = new Mogo.AI.Not();
										node41.AddChild(node42);
										node42.Proxy(new Mogo.AI.BossPartHasBeenBroken(110));
									}
									node41.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.lt,60));
									node41.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.ge,30));
									node41.AddChild(new Mogo.AI.InSkillCoolDown(7));
									node41.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
									node41.AddChild(new Mogo.AI.CastSpell(7,0));
									node41.AddChild(new Mogo.AI.EnterCD(0));
								}
								{
									Mogo.AI.SequenceNode node50 = new Mogo.AI.SequenceNode();
									node40.AddChild(node50);
									node50.AddChild(new Mogo.AI.CmpTargetDistance(Mogo.AI.CmpType.ge,400));
									node50.AddChild(new Mogo.AI.InSkillRange(3));
									node50.AddChild(new Mogo.AI.InSkillCoolDown(3));
									node50.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
									node50.AddChild(new Mogo.AI.CastSpell(3,0));
									node50.AddChild(new Mogo.AI.EnterCD(0));
								}
								{
									Mogo.AI.SequenceNode node57 = new Mogo.AI.SequenceNode();
									node40.AddChild(node57);
									{
										Mogo.AI.Not node58 = new Mogo.AI.Not();
										node57.AddChild(node58);
										node58.Proxy(new Mogo.AI.BossPartHasBeenBroken(111));
									}
									node57.AddChild(new Mogo.AI.InSkillCoolDown(5));
									node57.AddChild(new Mogo.AI.InSkillRange(5));
									node57.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
									node57.AddChild(new Mogo.AI.CastSpell(5,0));
									node57.AddChild(new Mogo.AI.EnterCD(0));
								}
								{
									Mogo.AI.SequenceNode node65 = new Mogo.AI.SequenceNode();
									node40.AddChild(node65);
									node65.AddChild(new Mogo.AI.InSkillCoolDown(4));
									node65.AddChild(new Mogo.AI.InSkillRange(4));
									node65.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
									node65.AddChild(new Mogo.AI.CastSpell(4,0));
									node65.AddChild(new Mogo.AI.EnterCD(0));
								}
								{
									Mogo.AI.SequenceNode node71 = new Mogo.AI.SequenceNode();
									node40.AddChild(node71);
									node71.AddChild(new Mogo.AI.CmpTargetDistance(Mogo.AI.CmpType.gt,500));
									node71.AddChild(new Mogo.AI.InSkillCoolDown(2));
									node71.AddChild(new Mogo.AI.InSkillRange(2));
									node71.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
									node71.AddChild(new Mogo.AI.CastSpell(2,0));
									node71.AddChild(new Mogo.AI.EnterCD(0));
								}
								{
									Mogo.AI.SequenceNode node78 = new Mogo.AI.SequenceNode();
									node40.AddChild(node78);
									node78.AddChild(new Mogo.AI.InSkillCoolDown(1));
									node78.AddChild(new Mogo.AI.InSkillRange(1));
									node78.AddChild(new Mogo.AI.CastSpell(1,0));
									node78.AddChild(new Mogo.AI.EnterCD(0));
								}
							}
						}
						{
							Mogo.AI.SequenceNode node83 = new Mogo.AI.SequenceNode();
							node10.AddChild(node83);
							{
								Mogo.AI.Not node84 = new Mogo.AI.Not();
								node83.AddChild(node84);
								node84.Proxy(new Mogo.AI.InSkillRange(1));
							}
							node83.AddChild(new Mogo.AI.ChooseCastPoint(1));
							node83.AddChild(new Mogo.AI.MoveTo(1f));
						}
						node10.AddChild(new Mogo.AI.EnterRest(500));
					}
				}
			}
		}
	}
}
