namespace Mogo.AI.BT
{
	public sealed class BT1002 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1002 _instance = null;
		public static BT1002 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1002();

				return _instance;
			}
		}

		private BT1002()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					{
						Mogo.AI.SequenceNode node3 = new Mogo.AI.SequenceNode();
						node2.AddChild(node3);
						node3.AddChild(new Mogo.AI.IsFirstThinking());
						node3.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node3.AddChild(new Mogo.AI.MonsterSpeech(1));
					}
					{
						Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
						node2.AddChild(node7);
						node7.AddChild(new Mogo.AI.IsInDeathState());
						node7.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node7.AddChild(new Mogo.AI.MonsterSpeech(2));
						node7.AddChild(new Mogo.AI.EnterRest(5000));
					}
					{
						Mogo.AI.SequenceNode node12 = new Mogo.AI.SequenceNode();
						node2.AddChild(node12);
						node12.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,10));
						node12.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node12.AddChild(new Mogo.AI.MonsterSpeech(3));
					}
					{
						Mogo.AI.SequenceNode node16 = new Mogo.AI.SequenceNode();
						node2.AddChild(node16);
						node16.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,30));
						node16.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node16.AddChild(new Mogo.AI.MonsterSpeech(4));
					}
					{
						Mogo.AI.SequenceNode node20 = new Mogo.AI.SequenceNode();
						node2.AddChild(node20);
						node20.AddChild(new Mogo.AI.IsFightState());
						node20.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,5));
						node20.AddChild(new Mogo.AI.MonsterSpeech(5));
					}
					{
						Mogo.AI.SequenceNode node24 = new Mogo.AI.SequenceNode();
						node2.AddChild(node24);
						node24.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.le,100));
					}
				}
				{
					Mogo.AI.PrioritySelectorNode node26 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node26);
					{
						Mogo.AI.SequenceNode node27 = new Mogo.AI.SequenceNode();
						node26.AddChild(node27);
						node27.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,0));
						node27.AddChild(new Mogo.AI.FindTargetBySkillRange(1));
						node27.AddChild(new Mogo.AI.IsTargetCanBeAttack());
						{
							Mogo.AI.PrioritySelectorNode node31 = new Mogo.AI.PrioritySelectorNode();
							node27.AddChild(node31);
							{
								Mogo.AI.SequenceNode node32 = new Mogo.AI.SequenceNode();
								node31.AddChild(node32);
								node32.AddChild(new Mogo.AI.InSkillCoolDown(7));
								node32.AddChild(new Mogo.AI.InSkillRange(7));
								node32.AddChild(new Mogo.AI.CastSpell(7,0));
								node32.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node37 = new Mogo.AI.SequenceNode();
								node31.AddChild(node37);
								node37.AddChild(new Mogo.AI.InSkillCoolDown(6));
								node37.AddChild(new Mogo.AI.InSkillRange(6));
								node37.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,15));
								node37.AddChild(new Mogo.AI.CastSpell(6,0));
								node37.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node43 = new Mogo.AI.SequenceNode();
								node31.AddChild(node43);
								node43.AddChild(new Mogo.AI.InSkillCoolDown(5));
								node43.AddChild(new Mogo.AI.InSkillRange(5));
								node43.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
								node43.AddChild(new Mogo.AI.CastSpell(5,0));
								node43.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node49 = new Mogo.AI.SequenceNode();
								node31.AddChild(node49);
								node49.AddChild(new Mogo.AI.InSkillCoolDown(4));
								node49.AddChild(new Mogo.AI.InSkillRange(4));
								node49.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
								node49.AddChild(new Mogo.AI.CastSpell(4,0));
								node49.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node55 = new Mogo.AI.SequenceNode();
								node31.AddChild(node55);
								node55.AddChild(new Mogo.AI.InSkillCoolDown(3));
								node55.AddChild(new Mogo.AI.InSkillRange(3));
								node55.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,40));
								node55.AddChild(new Mogo.AI.CastSpell(3,0));
								node55.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node61 = new Mogo.AI.SequenceNode();
								node31.AddChild(node61);
								node61.AddChild(new Mogo.AI.InSkillCoolDown(2));
								node61.AddChild(new Mogo.AI.InSkillRange(2));
								node61.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
								node61.AddChild(new Mogo.AI.CastSpell(2,0));
								node61.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node67 = new Mogo.AI.SequenceNode();
								node31.AddChild(node67);
								node67.AddChild(new Mogo.AI.InSkillCoolDown(1));
								node67.AddChild(new Mogo.AI.InSkillRange(1));
								node67.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
								node67.AddChild(new Mogo.AI.CastSpell(1,0));
								node67.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node73 = new Mogo.AI.SequenceNode();
								node31.AddChild(node73);
								node73.AddChild(new Mogo.AI.InSkillRange(1));
								{
									Mogo.AI.PrioritySelectorNode node75 = new Mogo.AI.PrioritySelectorNode();
									node73.AddChild(node75);
									{
										Mogo.AI.SequenceNode node76 = new Mogo.AI.SequenceNode();
										node75.AddChild(node76);
										node76.AddChild(new Mogo.AI.HasFriendInLeftRange(50));
										{
											Mogo.AI.PrioritySelectorNode node78 = new Mogo.AI.PrioritySelectorNode();
											node76.AddChild(node78);
											{
												Mogo.AI.SequenceNode node79 = new Mogo.AI.SequenceNode();
												node78.AddChild(node79);
												{
													Mogo.AI.Not node80 = new Mogo.AI.Not();
													node79.AddChild(node80);
													node80.Proxy(new Mogo.AI.HasFriendInRightRange(50));
												}
												node79.AddChild(new Mogo.AI.LookOn(100,200,0,0,0,100,0,0,500,500,0,300,0,1));
											}
											node78.AddChild(new Mogo.AI.EnterRest(500));
										}
									}
									{
										Mogo.AI.SequenceNode node84 = new Mogo.AI.SequenceNode();
										node75.AddChild(node84);
										node84.AddChild(new Mogo.AI.HasFriendInRightRange(50));
										node84.AddChild(new Mogo.AI.LookOn(100,200,0,0,100,0,0,0,500,500,300,0,0,1));
									}
									node75.AddChild(new Mogo.AI.LookOn(150,800,0,0,0,0,100,0,500,500,500,500,500,1));
								}
							}
							{
								Mogo.AI.SequenceNode node88 = new Mogo.AI.SequenceNode();
								node31.AddChild(node88);
								node88.AddChild(new Mogo.AI.ChooseCastPoint(1));
								node88.AddChild(new Mogo.AI.MoveTo(1f));
								node88.AddChild(new Mogo.AI.EnterRest(500));
							}
						}
					}
					{
						Mogo.AI.SequenceNode node92 = new Mogo.AI.SequenceNode();
						node26.AddChild(node92);
						node92.AddChild(new Mogo.AI.FindTargetByMonsterID(12200,50000));
						node92.AddChild(new Mogo.AI.IsTargetCanBeAttack());
						{
							Mogo.AI.PrioritySelectorNode node95 = new Mogo.AI.PrioritySelectorNode();
							node92.AddChild(node95);
							{
								Mogo.AI.SequenceNode node96 = new Mogo.AI.SequenceNode();
								node95.AddChild(node96);
								node96.AddChild(new Mogo.AI.InSkillCoolDown(7));
								node96.AddChild(new Mogo.AI.InSkillRange(7));
								node96.AddChild(new Mogo.AI.CastSpell(7,0));
								node96.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node101 = new Mogo.AI.SequenceNode();
								node95.AddChild(node101);
								node101.AddChild(new Mogo.AI.InSkillCoolDown(6));
								node101.AddChild(new Mogo.AI.InSkillRange(6));
								node101.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,15));
								node101.AddChild(new Mogo.AI.CastSpell(6,0));
								node101.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node107 = new Mogo.AI.SequenceNode();
								node95.AddChild(node107);
								node107.AddChild(new Mogo.AI.InSkillCoolDown(5));
								node107.AddChild(new Mogo.AI.InSkillRange(5));
								node107.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
								node107.AddChild(new Mogo.AI.CastSpell(5,0));
								node107.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node113 = new Mogo.AI.SequenceNode();
								node95.AddChild(node113);
								node113.AddChild(new Mogo.AI.InSkillCoolDown(4));
								node113.AddChild(new Mogo.AI.InSkillRange(4));
								node113.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
								node113.AddChild(new Mogo.AI.CastSpell(5,0));
								node113.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node119 = new Mogo.AI.SequenceNode();
								node95.AddChild(node119);
								node119.AddChild(new Mogo.AI.InSkillCoolDown(3));
								node119.AddChild(new Mogo.AI.InSkillRange(3));
								node119.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,40));
								node119.AddChild(new Mogo.AI.CastSpell(3,0));
								node119.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node125 = new Mogo.AI.SequenceNode();
								node95.AddChild(node125);
								node125.AddChild(new Mogo.AI.InSkillCoolDown(2));
								node125.AddChild(new Mogo.AI.InSkillRange(2));
								node125.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
								node125.AddChild(new Mogo.AI.CastSpell(2,0));
								node125.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node131 = new Mogo.AI.SequenceNode();
								node95.AddChild(node131);
								node131.AddChild(new Mogo.AI.InSkillCoolDown(1));
								node131.AddChild(new Mogo.AI.InSkillRange(1));
								node131.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
								node131.AddChild(new Mogo.AI.CastSpell(1,0));
								node131.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node137 = new Mogo.AI.SequenceNode();
								node95.AddChild(node137);
								node137.AddChild(new Mogo.AI.InSkillRange(1));
								{
									Mogo.AI.PrioritySelectorNode node139 = new Mogo.AI.PrioritySelectorNode();
									node137.AddChild(node139);
									{
										Mogo.AI.SequenceNode node140 = new Mogo.AI.SequenceNode();
										node139.AddChild(node140);
										node140.AddChild(new Mogo.AI.HasFriendInLeftRange(0));
										{
											Mogo.AI.PrioritySelectorNode node142 = new Mogo.AI.PrioritySelectorNode();
											node140.AddChild(node142);
											{
												Mogo.AI.SequenceNode node143 = new Mogo.AI.SequenceNode();
												node142.AddChild(node143);
												{
													Mogo.AI.Not node144 = new Mogo.AI.Not();
													node143.AddChild(node144);
													node144.Proxy(new Mogo.AI.HasFriendInRightRange(0));
												}
												node143.AddChild(new Mogo.AI.LookOn(100,200,0,0,0,100,0,38,500,500,0,300,0,1));
											}
											node142.AddChild(new Mogo.AI.EnterRest(500));
										}
									}
									{
										Mogo.AI.SequenceNode node148 = new Mogo.AI.SequenceNode();
										node139.AddChild(node148);
										node148.AddChild(new Mogo.AI.HasFriendInRightRange(0));
										node148.AddChild(new Mogo.AI.LookOn(100,200,0,0,100,0,0,38,500,500,300,0,0,1));
									}
									node139.AddChild(new Mogo.AI.LookOn(150,800,0,0,0,0,100,38,500,500,500,500,500,1));
								}
							}
							{
								Mogo.AI.SequenceNode node152 = new Mogo.AI.SequenceNode();
								node95.AddChild(node152);
								node152.AddChild(new Mogo.AI.ChooseCastPoint(1));
								node152.AddChild(new Mogo.AI.MoveTo(1f));
								node152.AddChild(new Mogo.AI.EnterRest(1500));
							}
						}
					}
				}
			}
		}
	}
}
