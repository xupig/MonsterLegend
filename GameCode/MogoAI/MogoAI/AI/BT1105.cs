namespace Mogo.AI.BT
{
	public sealed class BT1105 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1105 _instance = null;
		public static BT1105 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1105();

				return _instance;
			}
		}

		private BT1105()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				node1.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
				node1.AddChild(new Mogo.AI.IsTargetCanBeAttack());
				{
					Mogo.AI.PrioritySelectorNode node4 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node4);
					{
						Mogo.AI.PrioritySelectorNode node5 = new Mogo.AI.PrioritySelectorNode();
						node4.AddChild(node5);
						{
							Mogo.AI.SequenceNode node6 = new Mogo.AI.SequenceNode();
							node5.AddChild(node6);
							node6.AddChild(new Mogo.AI.BossPartCanBeRecovered(201));
							node6.AddChild(new Mogo.AI.AddBuff(4601,0));
						}
						{
							Mogo.AI.SequenceNode node9 = new Mogo.AI.SequenceNode();
							node5.AddChild(node9);
							node9.AddChild(new Mogo.AI.BossPartCanBeRecovered(202));
							node9.AddChild(new Mogo.AI.AddBuff(4602,0));
						}
						{
							Mogo.AI.SequenceNode node12 = new Mogo.AI.SequenceNode();
							node5.AddChild(node12);
							node12.AddChild(new Mogo.AI.BossPartCanBeRecovered(203));
							node12.AddChild(new Mogo.AI.AddBuff(4603,0));
						}
					}
					{
						Mogo.AI.SequenceNode node15 = new Mogo.AI.SequenceNode();
						node4.AddChild(node15);
						node15.AddChild(new Mogo.AI.InSkillCoolDown(7));
						node15.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
						node15.AddChild(new Mogo.AI.CastSpell(7,0));
						node15.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node20 = new Mogo.AI.SequenceNode();
						node4.AddChild(node20);
						node20.AddChild(new Mogo.AI.InSkillCoolDown(6));
						node20.AddChild(new Mogo.AI.InSkillRange(6));
						node20.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,15));
						node20.AddChild(new Mogo.AI.CastSpell(6,0));
						node20.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node26 = new Mogo.AI.SequenceNode();
						node4.AddChild(node26);
						node26.AddChild(new Mogo.AI.InSkillCoolDown(5));
						node26.AddChild(new Mogo.AI.InSkillRange(5));
						node26.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
						node26.AddChild(new Mogo.AI.CastSpell(5,0));
						node26.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node32 = new Mogo.AI.SequenceNode();
						node4.AddChild(node32);
						node32.AddChild(new Mogo.AI.InSkillCoolDown(4));
						node32.AddChild(new Mogo.AI.InSkillRange(4));
						node32.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
						node32.AddChild(new Mogo.AI.CastSpell(4,0));
						node32.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node38 = new Mogo.AI.SequenceNode();
						node4.AddChild(node38);
						node38.AddChild(new Mogo.AI.InSkillCoolDown(3));
						node38.AddChild(new Mogo.AI.InSkillRange(3));
						node38.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,40));
						node38.AddChild(new Mogo.AI.CastSpell(3,0));
						node38.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node44 = new Mogo.AI.SequenceNode();
						node4.AddChild(node44);
						node44.AddChild(new Mogo.AI.InSkillCoolDown(2));
						node44.AddChild(new Mogo.AI.InSkillRange(2));
						node44.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node44.AddChild(new Mogo.AI.CastSpell(2,0));
						node44.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node50 = new Mogo.AI.SequenceNode();
						node4.AddChild(node50);
						node50.AddChild(new Mogo.AI.InSkillCoolDown(1));
						node50.AddChild(new Mogo.AI.InSkillRange(1));
						node50.AddChild(new Mogo.AI.CastSpell(1,0));
						node50.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node55 = new Mogo.AI.SequenceNode();
						node4.AddChild(node55);
						{
							Mogo.AI.Not node56 = new Mogo.AI.Not();
							node55.AddChild(node56);
							node56.Proxy(new Mogo.AI.InSkillRange(1));
						}
						node55.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
						node55.AddChild(new Mogo.AI.ChooseCastPoint(1));
						node55.AddChild(new Mogo.AI.MoveTo(1f));
					}
					node4.AddChild(new Mogo.AI.EnterRest(500));
				}
			}
		}
	}
}
