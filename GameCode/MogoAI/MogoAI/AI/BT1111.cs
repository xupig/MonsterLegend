namespace Mogo.AI.BT
{
	public sealed class BT1111 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1111 _instance = null;
		public static BT1111 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1111();

				return _instance;
			}
		}

		private BT1111()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.IsFightState());
					{
						Mogo.AI.SequenceNode node4 = new Mogo.AI.SequenceNode();
						node2.AddChild(node4);
						node4.AddChild(new Mogo.AI.AOI(0,0,0,"0",0));
						node4.AddChild(new Mogo.AI.EnterFight());
					}
				}
				{
					Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
					node1.AddChild(node7);
					node7.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
					node7.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node10 = new Mogo.AI.PrioritySelectorNode();
						node7.AddChild(node10);
						{
							Mogo.AI.SequenceNode node11 = new Mogo.AI.SequenceNode();
							node10.AddChild(node11);
							node11.AddChild(new Mogo.AI.InSkillCoolDown(1));
							node11.AddChild(new Mogo.AI.InSkillRange(1));
							node11.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
							node11.AddChild(new Mogo.AI.CastSpell(1,0));
							node11.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node17 = new Mogo.AI.SequenceNode();
							node10.AddChild(node17);
							{
								Mogo.AI.Not node18 = new Mogo.AI.Not();
								node17.AddChild(node18);
								node18.Proxy(new Mogo.AI.InSkillRange(1));
							}
							node17.AddChild(new Mogo.AI.ChooseCastPoint(1));
							node17.AddChild(new Mogo.AI.MoveTo(1f));
						}
						{
							Mogo.AI.SequenceNode node22 = new Mogo.AI.SequenceNode();
							node10.AddChild(node22);
							node22.AddChild(new Mogo.AI.HasFriendInLeftRange(50));
							{
								Mogo.AI.PrioritySelectorNode node24 = new Mogo.AI.PrioritySelectorNode();
								node22.AddChild(node24);
								{
									Mogo.AI.SequenceNode node25 = new Mogo.AI.SequenceNode();
									node24.AddChild(node25);
									{
										Mogo.AI.Not node26 = new Mogo.AI.Not();
										node25.AddChild(node26);
										node26.Proxy(new Mogo.AI.HasFriendInRightRange(50));
									}
									node25.AddChild(new Mogo.AI.LookOn(100,200,0,0,0,100,0,0,0,0,0,300,0,1));
								}
								node24.AddChild(new Mogo.AI.EnterRest(500));
							}
						}
						{
							Mogo.AI.SequenceNode node30 = new Mogo.AI.SequenceNode();
							node10.AddChild(node30);
							node30.AddChild(new Mogo.AI.HasFriendInRightRange(50));
							node30.AddChild(new Mogo.AI.LookOn(100,200,0,0,100,0,0,0,0,0,300,0,0,1));
						}
						node10.AddChild(new Mogo.AI.LookOn(150,800,0,0,0,0,100,0,500,500,500,500,500,1));
						node10.AddChild(new Mogo.AI.EnterRest(500));
					}
				}
			}
		}
	}
}
