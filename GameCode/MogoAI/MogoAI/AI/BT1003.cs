namespace Mogo.AI.BT
{
	public sealed class BT1003 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1003 _instance = null;
		public static BT1003 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1003();

				return _instance;
			}
		}

		private BT1003()
		{
			{
				Mogo.AI.PrioritySelectorNode node1 = new Mogo.AI.PrioritySelectorNode();
				this.AddChild(node1);
				{
					Mogo.AI.SequenceNode node2 = new Mogo.AI.SequenceNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.InSkillCoolDown(2));
					node2.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
					node2.AddChild(new Mogo.AI.CastSpell(2,0));
				}
				{
					Mogo.AI.SequenceNode node6 = new Mogo.AI.SequenceNode();
					node1.AddChild(node6);
					node6.AddChild(new Mogo.AI.InSkillCoolDown(1));
					node6.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
					node6.AddChild(new Mogo.AI.CastSpell(1,0));
				}
				node1.AddChild(new Mogo.AI.EnterRest(2000));
			}
		}
	}
}
