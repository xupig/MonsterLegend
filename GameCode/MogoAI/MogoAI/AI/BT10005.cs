namespace Mogo.AI.BT
{
	public sealed class BT10005 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT10005 _instance = null;
		public static BT10005 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT10005();

				return _instance;
			}
		}

		private BT10005()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				node1.AddChild(new Mogo.AI.InSkillCoolDown(1));
				node1.AddChild(new Mogo.AI.CastSpell(1,0));
			}
		}
	}
}
