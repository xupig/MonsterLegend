namespace Mogo.AI.BT
{
	public sealed class BT1109 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1109 _instance = null;
		public static BT1109 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1109();

				return _instance;
			}
		}

		private BT1109()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					{
						Mogo.AI.SequenceNode node3 = new Mogo.AI.SequenceNode();
						node2.AddChild(node3);
						node3.AddChild(new Mogo.AI.IsFirstThinking());
						node3.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node3.AddChild(new Mogo.AI.MonsterSpeech(1,1));
					}
					{
						Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
						node2.AddChild(node7);
						node7.AddChild(new Mogo.AI.IsInDeathState());
						node7.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node7.AddChild(new Mogo.AI.MonsterSpeech(2,1));
						node7.AddChild(new Mogo.AI.EnterRest(5000));
					}
					{
						Mogo.AI.SequenceNode node12 = new Mogo.AI.SequenceNode();
						node2.AddChild(node12);
						node12.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,10));
						node12.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node12.AddChild(new Mogo.AI.MonsterSpeech(3,1));
					}
					{
						Mogo.AI.SequenceNode node16 = new Mogo.AI.SequenceNode();
						node2.AddChild(node16);
						node16.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,30));
						node16.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node16.AddChild(new Mogo.AI.MonsterSpeech(4,1));
					}
					{
						Mogo.AI.SequenceNode node20 = new Mogo.AI.SequenceNode();
						node2.AddChild(node20);
						node20.AddChild(new Mogo.AI.IsFightState());
						node20.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,5));
						node20.AddChild(new Mogo.AI.MonsterSpeech(5,1));
					}
					{
						Mogo.AI.SequenceNode node24 = new Mogo.AI.SequenceNode();
						node2.AddChild(node24);
						node24.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.le,100));
					}
				}
				{
					Mogo.AI.SequenceNode node26 = new Mogo.AI.SequenceNode();
					node1.AddChild(node26);
					node26.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
					node26.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node29 = new Mogo.AI.PrioritySelectorNode();
						node26.AddChild(node29);
						{
							Mogo.AI.SequenceNode node30 = new Mogo.AI.SequenceNode();
							node29.AddChild(node30);
							node30.AddChild(new Mogo.AI.InSkillCoolDown(7));
							node30.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
							node30.AddChild(new Mogo.AI.CastSpell(7,0));
							node30.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node35 = new Mogo.AI.SequenceNode();
							node29.AddChild(node35);
							node35.AddChild(new Mogo.AI.InSkillCoolDown(6));
							node35.AddChild(new Mogo.AI.InSkillRange(6));
							node35.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,15));
							node35.AddChild(new Mogo.AI.CastSpell(6,0));
							node35.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node41 = new Mogo.AI.SequenceNode();
							node29.AddChild(node41);
							node41.AddChild(new Mogo.AI.InSkillCoolDown(5));
							node41.AddChild(new Mogo.AI.InSkillRange(5));
							node41.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
							node41.AddChild(new Mogo.AI.CastSpell(5,0));
							node41.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node47 = new Mogo.AI.SequenceNode();
							node29.AddChild(node47);
							node47.AddChild(new Mogo.AI.InSkillCoolDown(4));
							node47.AddChild(new Mogo.AI.InSkillRange(4));
							node47.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
							node47.AddChild(new Mogo.AI.CastSpell(4,0));
							node47.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node53 = new Mogo.AI.SequenceNode();
							node29.AddChild(node53);
							node53.AddChild(new Mogo.AI.InSkillCoolDown(3));
							node53.AddChild(new Mogo.AI.InSkillRange(3));
							node53.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,40));
							node53.AddChild(new Mogo.AI.CastSpell(3,0));
							node53.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node59 = new Mogo.AI.SequenceNode();
							node29.AddChild(node59);
							node59.AddChild(new Mogo.AI.InSkillCoolDown(2));
							node59.AddChild(new Mogo.AI.InSkillRange(2));
							node59.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
							node59.AddChild(new Mogo.AI.CastSpell(2,0));
							node59.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node65 = new Mogo.AI.SequenceNode();
							node29.AddChild(node65);
							node65.AddChild(new Mogo.AI.InSkillCoolDown(1));
							node65.AddChild(new Mogo.AI.InSkillRange(1));
							node65.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
							node65.AddChild(new Mogo.AI.CastSpell(1,0));
							node65.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node71 = new Mogo.AI.SequenceNode();
							node29.AddChild(node71);
							node71.AddChild(new Mogo.AI.InSkillRange(1));
							{
								Mogo.AI.PrioritySelectorNode node73 = new Mogo.AI.PrioritySelectorNode();
								node71.AddChild(node73);
								{
									Mogo.AI.SequenceNode node74 = new Mogo.AI.SequenceNode();
									node73.AddChild(node74);
									node74.AddChild(new Mogo.AI.HasFriendInLeftRange(50));
									{
										Mogo.AI.PrioritySelectorNode node76 = new Mogo.AI.PrioritySelectorNode();
										node74.AddChild(node76);
										{
											Mogo.AI.SequenceNode node77 = new Mogo.AI.SequenceNode();
											node76.AddChild(node77);
											{
												Mogo.AI.Not node78 = new Mogo.AI.Not();
												node77.AddChild(node78);
												node78.Proxy(new Mogo.AI.HasFriendInRightRange(50));
											}
											node77.AddChild(new Mogo.AI.LookOn(100,200,0,0,0,100,0,0,500,500,0,300,0,1));
										}
										node76.AddChild(new Mogo.AI.EnterCD(500));
									}
								}
								{
									Mogo.AI.SequenceNode node82 = new Mogo.AI.SequenceNode();
									node73.AddChild(node82);
									node82.AddChild(new Mogo.AI.HasFriendInRightRange(50));
									node82.AddChild(new Mogo.AI.LookOn(100,200,0,0,100,0,0,0,500,500,300,0,0,1));
								}
								node73.AddChild(new Mogo.AI.LookOn(150,800,0,0,0,0,100,0,500,500,500,500,500,1));
							}
						}
						{
							Mogo.AI.SequenceNode node86 = new Mogo.AI.SequenceNode();
							node29.AddChild(node86);
							{
								Mogo.AI.Not node87 = new Mogo.AI.Not();
								node86.AddChild(node87);
								node87.Proxy(new Mogo.AI.InSkillRange(1));
							}
							node86.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
							node86.AddChild(new Mogo.AI.ChooseCastPoint(1));
							node86.AddChild(new Mogo.AI.MoveTo(1f));
						}
						node29.AddChild(new Mogo.AI.EnterRest(500));
					}
				}
			}
		}
	}
}
