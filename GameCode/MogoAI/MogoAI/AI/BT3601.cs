namespace Mogo.AI.BT
{
	public sealed class BT3601 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT3601 _instance = null;
		public static BT3601 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT3601();

				return _instance;
			}
		}

		private BT3601()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.IsFightState());
					{
						Mogo.AI.SequenceNode node4 = new Mogo.AI.SequenceNode();
						node2.AddChild(node4);
						node4.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
						node4.AddChild(new Mogo.AI.EnterFight());
					}
				}
				{
					Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
					node1.AddChild(node7);
					node7.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
					node7.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node10 = new Mogo.AI.PrioritySelectorNode();
						node7.AddChild(node10);
						{
							Mogo.AI.SequenceNode node11 = new Mogo.AI.SequenceNode();
							node10.AddChild(node11);
							node11.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,20));
							node11.AddChild(new Mogo.AI.InSkillCoolDown(7));
							node11.AddChild(new Mogo.AI.CastSpell(7,0));
							node11.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node16 = new Mogo.AI.SequenceNode();
							node10.AddChild(node16);
							node16.AddChild(new Mogo.AI.InSkillCoolDown(4));
							node16.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,80));
							node16.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.gt,20));
							{
								Mogo.AI.PrioritySelectorNode node20 = new Mogo.AI.PrioritySelectorNode();
								node16.AddChild(node20);
								{
									Mogo.AI.SequenceNode node21 = new Mogo.AI.SequenceNode();
									node20.AddChild(node21);
									node21.AddChild(new Mogo.AI.MoveToAppoint("47,64,64"));
									node21.AddChild(new Mogo.AI.CmpTargetPointDistance(Mogo.AI.CmpType.gt,50));
									node21.AddChild(new Mogo.AI.AddBuff(4002,3000));
									node21.AddChild(new Mogo.AI.MoveTo(1.5f));
								}
								{
									Mogo.AI.SequenceNode node26 = new Mogo.AI.SequenceNode();
									node20.AddChild(node26);
									node26.AddChild(new Mogo.AI.InSkillCoolDown(3));
									node26.AddChild(new Mogo.AI.CastSpell(3,0));
									node26.AddChild(new Mogo.AI.EnterCD(0));
								}
								{
									Mogo.AI.SequenceNode node30 = new Mogo.AI.SequenceNode();
									node20.AddChild(node30);
									node30.AddChild(new Mogo.AI.InSkillCoolDown(4));
									node30.AddChild(new Mogo.AI.CastSpell(4,0));
									node30.AddChild(new Mogo.AI.EnterCD(0));
								}
							}
						}
						{
							Mogo.AI.SequenceNode node34 = new Mogo.AI.SequenceNode();
							node10.AddChild(node34);
							{
								Mogo.AI.Not node35 = new Mogo.AI.Not();
								node34.AddChild(node35);
								node35.Proxy(new Mogo.AI.BossPartHasBeenBroken(112));
							}
							node34.AddChild(new Mogo.AI.InSkillCoolDown(5));
							node34.AddChild(new Mogo.AI.InSkillRange(5));
							node34.AddChild(new Mogo.AI.CastSpell(5,0));
							node34.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node41 = new Mogo.AI.SequenceNode();
							node10.AddChild(node41);
							node41.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,90));
							node41.AddChild(new Mogo.AI.InSkillCoolDown(6));
							node41.AddChild(new Mogo.AI.InSkillRange(6));
							node41.AddChild(new Mogo.AI.CastSpell(6,0));
							node41.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
							node41.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node48 = new Mogo.AI.SequenceNode();
							node10.AddChild(node48);
							node48.AddChild(new Mogo.AI.CmpTargetDistance(Mogo.AI.CmpType.gt,500));
							node48.AddChild(new Mogo.AI.InSkillCoolDown(2));
							node48.AddChild(new Mogo.AI.InSkillRange(2));
							node48.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
							node48.AddChild(new Mogo.AI.CastSpell(2,0));
							node48.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node55 = new Mogo.AI.SequenceNode();
							node10.AddChild(node55);
							node55.AddChild(new Mogo.AI.InSkillCoolDown(1));
							node55.AddChild(new Mogo.AI.InSkillRange(1));
							node55.AddChild(new Mogo.AI.CastSpell(1,0));
							node55.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node60 = new Mogo.AI.SequenceNode();
							node10.AddChild(node60);
							{
								Mogo.AI.Not node61 = new Mogo.AI.Not();
								node60.AddChild(node61);
								node61.Proxy(new Mogo.AI.InSkillRange(1));
							}
							node60.AddChild(new Mogo.AI.ChooseCastPoint(1));
							node60.AddChild(new Mogo.AI.MoveTo(1f));
						}
						node10.AddChild(new Mogo.AI.EnterRest(500));
					}
				}
			}
		}
	}
}
