namespace Mogo.AI.BT
{
	public sealed class BT13400 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT13400 _instance = null;
		public static BT13400 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT13400();

				return _instance;
			}
		}

		private BT13400()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.IsFightState());
					{
						Mogo.AI.SequenceNode node4 = new Mogo.AI.SequenceNode();
						node2.AddChild(node4);
						node4.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
						node4.AddChild(new Mogo.AI.EnterFight());
					}
				}
				{
					Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
					node1.AddChild(node7);
					node7.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
					node7.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node10 = new Mogo.AI.PrioritySelectorNode();
						node7.AddChild(node10);
						{
							Mogo.AI.PrioritySelectorNode node11 = new Mogo.AI.PrioritySelectorNode();
							node10.AddChild(node11);
							{
								Mogo.AI.SequenceNode node12 = new Mogo.AI.SequenceNode();
								node11.AddChild(node12);
								node12.AddChild(new Mogo.AI.TestBuff(0,5370));
								{
									Mogo.AI.PrioritySelectorNode node14 = new Mogo.AI.PrioritySelectorNode();
									node12.AddChild(node14);
									{
										Mogo.AI.SequenceNode node15 = new Mogo.AI.SequenceNode();
										node14.AddChild(node15);
										node15.AddChild(new Mogo.AI.BossPartHasBeenBroken(153));
										node15.AddChild(new Mogo.AI.AddBuff(5371,2000));
									}
									{
										Mogo.AI.SequenceNode node18 = new Mogo.AI.SequenceNode();
										node14.AddChild(node18);
										node18.AddChild(new Mogo.AI.InSkillCoolDown(5));
										node18.AddChild(new Mogo.AI.MonsterSpeech(1,1));
										node18.AddChild(new Mogo.AI.CastSpell(5,0));
										node18.AddChild(new Mogo.AI.EnterCD(0));
									}
								}
							}
							{
								Mogo.AI.SequenceNode node23 = new Mogo.AI.SequenceNode();
								node11.AddChild(node23);
								node23.AddChild(new Mogo.AI.TestBuff(0,5371));
								{
									Mogo.AI.PrioritySelectorNode node25 = new Mogo.AI.PrioritySelectorNode();
									node23.AddChild(node25);
									{
										Mogo.AI.SequenceNode node26 = new Mogo.AI.SequenceNode();
										node25.AddChild(node26);
										node26.AddChild(new Mogo.AI.BossPartHasBeenBroken(152));
										node26.AddChild(new Mogo.AI.AddBuff(5372,2000));
									}
									{
										Mogo.AI.SequenceNode node29 = new Mogo.AI.SequenceNode();
										node25.AddChild(node29);
										node29.AddChild(new Mogo.AI.InSkillCoolDown(4));
										node29.AddChild(new Mogo.AI.MonsterSpeech(2,1));
										node29.AddChild(new Mogo.AI.CastSpell(4,0));
										node29.AddChild(new Mogo.AI.EnterCD(0));
									}
								}
							}
							{
								Mogo.AI.SequenceNode node34 = new Mogo.AI.SequenceNode();
								node11.AddChild(node34);
								node34.AddChild(new Mogo.AI.TestBuff(0,5372));
								{
									Mogo.AI.PrioritySelectorNode node36 = new Mogo.AI.PrioritySelectorNode();
									node34.AddChild(node36);
									{
										Mogo.AI.SequenceNode node37 = new Mogo.AI.SequenceNode();
										node36.AddChild(node37);
										node37.AddChild(new Mogo.AI.BossPartHasBeenBroken(153));
										node37.AddChild(new Mogo.AI.AddBuff(5373,2000));
									}
									{
										Mogo.AI.SequenceNode node40 = new Mogo.AI.SequenceNode();
										node36.AddChild(node40);
										node40.AddChild(new Mogo.AI.InSkillCoolDown(3));
										node40.AddChild(new Mogo.AI.MonsterSpeech(1,1));
										node40.AddChild(new Mogo.AI.CastSpell(3,0));
										node40.AddChild(new Mogo.AI.EnterCD(0));
									}
								}
							}
							{
								Mogo.AI.SequenceNode node45 = new Mogo.AI.SequenceNode();
								node11.AddChild(node45);
								node45.AddChild(new Mogo.AI.TestBuff(0,5373));
								{
									Mogo.AI.PrioritySelectorNode node47 = new Mogo.AI.PrioritySelectorNode();
									node45.AddChild(node47);
									{
										Mogo.AI.SequenceNode node48 = new Mogo.AI.SequenceNode();
										node47.AddChild(node48);
										node48.AddChild(new Mogo.AI.BossPartHasBeenBroken(151));
										node48.AddChild(new Mogo.AI.AddBuff(5388,0));
									}
									{
										Mogo.AI.SequenceNode node51 = new Mogo.AI.SequenceNode();
										node47.AddChild(node51);
										node51.AddChild(new Mogo.AI.InSkillCoolDown(2));
										node51.AddChild(new Mogo.AI.MonsterSpeech(3,1));
										node51.AddChild(new Mogo.AI.CastSpell(2,0));
										node51.AddChild(new Mogo.AI.EnterCD(0));
									}
								}
							}
							{
								Mogo.AI.SequenceNode node56 = new Mogo.AI.SequenceNode();
								node11.AddChild(node56);
								node56.AddChild(new Mogo.AI.InSkillCoolDown(1));
								node56.AddChild(new Mogo.AI.InSkillRange(1));
								node56.AddChild(new Mogo.AI.CastSpell(1,0));
								node56.AddChild(new Mogo.AI.EnterCD(0));
							}
						}
						{
							Mogo.AI.SequenceNode node61 = new Mogo.AI.SequenceNode();
							node10.AddChild(node61);
							node61.AddChild(new Mogo.AI.InSkillRange(1));
							{
								Mogo.AI.PrioritySelectorNode node63 = new Mogo.AI.PrioritySelectorNode();
								node61.AddChild(node63);
								{
									Mogo.AI.SequenceNode node64 = new Mogo.AI.SequenceNode();
									node63.AddChild(node64);
									node64.AddChild(new Mogo.AI.HasFriendInLeftRange(50));
									{
										Mogo.AI.PrioritySelectorNode node66 = new Mogo.AI.PrioritySelectorNode();
										node64.AddChild(node66);
										{
											Mogo.AI.SequenceNode node67 = new Mogo.AI.SequenceNode();
											node66.AddChild(node67);
											{
												Mogo.AI.Not node68 = new Mogo.AI.Not();
												node67.AddChild(node68);
												node68.Proxy(new Mogo.AI.HasFriendInRightRange(50));
											}
											node67.AddChild(new Mogo.AI.LookOn(100,200,0,0,0,100,0,0,500,500,0,300,0,1));
										}
										node66.AddChild(new Mogo.AI.EnterCD(500));
									}
								}
								{
									Mogo.AI.SequenceNode node72 = new Mogo.AI.SequenceNode();
									node63.AddChild(node72);
									node72.AddChild(new Mogo.AI.HasFriendInRightRange(50));
									node72.AddChild(new Mogo.AI.LookOn(100,200,0,0,100,0,0,0,500,500,300,0,0,1));
								}
								node63.AddChild(new Mogo.AI.LookOn(150,800,0,0,0,0,100,0,500,500,500,500,500,1));
							}
						}
						{
							Mogo.AI.SequenceNode node76 = new Mogo.AI.SequenceNode();
							node10.AddChild(node76);
							{
								Mogo.AI.Not node77 = new Mogo.AI.Not();
								node76.AddChild(node77);
								node77.Proxy(new Mogo.AI.InSkillRange(1));
							}
							node76.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,100));
							node76.AddChild(new Mogo.AI.ChooseCastPoint(1));
							node76.AddChild(new Mogo.AI.MoveTo(1f));
						}
						node10.AddChild(new Mogo.AI.EnterRest(500));
					}
				}
			}
		}
	}
}
