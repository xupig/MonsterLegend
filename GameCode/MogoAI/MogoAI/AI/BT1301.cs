namespace Mogo.AI.BT
{
	public sealed class BT1301 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1301 _instance = null;
		public static BT1301 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1301();

				return _instance;
			}
		}

		private BT1301()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.Not node2 = new Mogo.AI.Not();
					node1.AddChild(node2);
					node2.Proxy(new Mogo.AI.IsCD());
				}
				{
					Mogo.AI.PrioritySelectorNode node4 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node4);
					node4.AddChild(new Mogo.AI.FindTargetBySkillRange(1));
					node4.AddChild(new Mogo.AI.FindTargetByMonsterID(11001,99999));
				}
				node1.AddChild(new Mogo.AI.IsTargetCanBeAttack());
				{
					Mogo.AI.PrioritySelectorNode node8 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node8);
					{
						Mogo.AI.SequenceNode node9 = new Mogo.AI.SequenceNode();
						node8.AddChild(node9);
						node9.AddChild(new Mogo.AI.InSkillCoolDown(1));
						node9.AddChild(new Mogo.AI.InSkillRange(1));
						node9.AddChild(new Mogo.AI.CastSpell(1,0));
						node9.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node14 = new Mogo.AI.SequenceNode();
						node8.AddChild(node14);
						node14.AddChild(new Mogo.AI.ChooseCastPoint(1));
						node14.AddChild(new Mogo.AI.MoveTo(1f));
					}
				}
			}
		}
	}
}
