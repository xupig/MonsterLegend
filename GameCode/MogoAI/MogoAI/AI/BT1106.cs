namespace Mogo.AI.BT
{
	public sealed class BT1106 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1106 _instance = null;
		public static BT1106 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1106();

				return _instance;
			}
		}

		private BT1106()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					{
						Mogo.AI.SequenceNode node3 = new Mogo.AI.SequenceNode();
						node2.AddChild(node3);
						node3.AddChild(new Mogo.AI.IsFirstThinking());
						node3.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node3.AddChild(new Mogo.AI.MonsterSpeech(1,1));
					}
					{
						Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
						node2.AddChild(node7);
						node7.AddChild(new Mogo.AI.IsInDeathState());
						node7.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node7.AddChild(new Mogo.AI.MonsterSpeech(2,1));
						node7.AddChild(new Mogo.AI.EnterRest(5000));
					}
					{
						Mogo.AI.SequenceNode node12 = new Mogo.AI.SequenceNode();
						node2.AddChild(node12);
						node12.AddChild(new Mogo.AI.TestBuff(0,4931));
						node12.AddChild(new Mogo.AI.MonsterSpeech(1,1));
					}
					{
						Mogo.AI.SequenceNode node15 = new Mogo.AI.SequenceNode();
						node2.AddChild(node15);
						node15.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,10));
						node15.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node15.AddChild(new Mogo.AI.MonsterSpeech(3,1));
					}
					{
						Mogo.AI.SequenceNode node19 = new Mogo.AI.SequenceNode();
						node2.AddChild(node19);
						node19.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,30));
						node19.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node19.AddChild(new Mogo.AI.MonsterSpeech(4,1));
					}
					{
						Mogo.AI.SequenceNode node23 = new Mogo.AI.SequenceNode();
						node2.AddChild(node23);
						node23.AddChild(new Mogo.AI.IsFightState());
						node23.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,5));
						node23.AddChild(new Mogo.AI.MonsterSpeech(5,1));
					}
					{
						Mogo.AI.SequenceNode node27 = new Mogo.AI.SequenceNode();
						node2.AddChild(node27);
						node27.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.le,100));
					}
				}
				{
					Mogo.AI.SequenceNode node29 = new Mogo.AI.SequenceNode();
					node1.AddChild(node29);
					node29.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
					node29.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node32 = new Mogo.AI.PrioritySelectorNode();
						node29.AddChild(node32);
						{
							Mogo.AI.SequenceNode node33 = new Mogo.AI.SequenceNode();
							node32.AddChild(node33);
							node33.AddChild(new Mogo.AI.InSkillCoolDown(1));
							node33.AddChild(new Mogo.AI.InSkillRange(1));
							node33.AddChild(new Mogo.AI.CastSpell(1,0));
							node33.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node38 = new Mogo.AI.SequenceNode();
							node32.AddChild(node38);
							node38.AddChild(new Mogo.AI.InSkillCoolDown(6));
							node38.AddChild(new Mogo.AI.InSkillRange(6));
							node38.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,15));
							node38.AddChild(new Mogo.AI.CastSpell(6,0));
							node38.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node44 = new Mogo.AI.SequenceNode();
							node32.AddChild(node44);
							node44.AddChild(new Mogo.AI.InSkillCoolDown(5));
							node44.AddChild(new Mogo.AI.InSkillRange(5));
							node44.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
							node44.AddChild(new Mogo.AI.CastSpell(5,0));
							node44.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node50 = new Mogo.AI.SequenceNode();
							node32.AddChild(node50);
							node50.AddChild(new Mogo.AI.InSkillCoolDown(4));
							node50.AddChild(new Mogo.AI.InSkillRange(4));
							node50.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
							node50.AddChild(new Mogo.AI.CastSpell(4,0));
							node50.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node56 = new Mogo.AI.SequenceNode();
							node32.AddChild(node56);
							node56.AddChild(new Mogo.AI.InSkillCoolDown(3));
							node56.AddChild(new Mogo.AI.InSkillRange(3));
							node56.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,40));
							node56.AddChild(new Mogo.AI.CastSpell(3,0));
							node56.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node62 = new Mogo.AI.SequenceNode();
							node32.AddChild(node62);
							node62.AddChild(new Mogo.AI.InSkillCoolDown(2));
							node62.AddChild(new Mogo.AI.InSkillRange(2));
							node62.AddChild(new Mogo.AI.CastSpell(2,0));
							node62.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node67 = new Mogo.AI.SequenceNode();
							node32.AddChild(node67);
							{
								Mogo.AI.Not node68 = new Mogo.AI.Not();
								node67.AddChild(node68);
								node68.Proxy(new Mogo.AI.InSkillRange(2));
							}
							node67.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
							node67.AddChild(new Mogo.AI.ChooseCastPoint(2));
							node67.AddChild(new Mogo.AI.MoveTo(1f));
						}
						node32.AddChild(new Mogo.AI.EnterRest(500));
					}
				}
			}
		}
	}
}
