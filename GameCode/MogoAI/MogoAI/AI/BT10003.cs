namespace Mogo.AI.BT
{
	public sealed class BT10003 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT10003 _instance = null;
		public static BT10003 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT10003();

				return _instance;
			}
		}

		private BT10003()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.Not node2 = new Mogo.AI.Not();
					node1.AddChild(node2);
					node2.Proxy(new Mogo.AI.IsCD());
				}
				node1.AddChild(new Mogo.AI.FindTargetBySkillRange(1));
				node1.AddChild(new Mogo.AI.IsTargetCanBeAttack());
				{
					Mogo.AI.PrioritySelectorNode node6 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node6);
					{
						Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
						node6.AddChild(node7);
						node7.AddChild(new Mogo.AI.InSkillCoolDown(1));
						node7.AddChild(new Mogo.AI.InSkillRange(1));
						node7.AddChild(new Mogo.AI.CastSpell(1,0));
						node7.AddChild(new Mogo.AI.EnterCD(0));
					}
					node6.AddChild(new Mogo.AI.EnterRest(1000));
				}
			}
		}
	}
}
