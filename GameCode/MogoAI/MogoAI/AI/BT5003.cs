namespace Mogo.AI.BT
{
	public sealed class BT5003 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT5003 _instance = null;
		public static BT5003 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT5003();

				return _instance;
			}
		}

		private BT5003()
		{
			{
				Mogo.AI.PrioritySelectorNode node1 = new Mogo.AI.PrioritySelectorNode();
				this.AddChild(node1);
				node1.AddChild(new Mogo.AI.CmpEnemyNum(Mogo.AI.CmpType.eq,0));
				{
					Mogo.AI.SequenceNode node3 = new Mogo.AI.SequenceNode();
					node1.AddChild(node3);
					node3.AddChild(new Mogo.AI.Patrol(500,3000,800));
				}
			}
		}
	}
}
