namespace Mogo.AI.BT
{
	public sealed class BT4002 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT4002 _instance = null;
		public static BT4002 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT4002();

				return _instance;
			}
		}

		private BT4002()
		{
			{
				Mogo.AI.PrioritySelectorNode node1 = new Mogo.AI.PrioritySelectorNode();
				this.AddChild(node1);
				{
					Mogo.AI.SequenceNode node2 = new Mogo.AI.SequenceNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.AOI(100,2,0,"500",0));
					node2.AddChild(new Mogo.AI.InSkillCoolDown(1));
					node2.AddChild(new Mogo.AI.InSkillRange(1));
					node2.AddChild(new Mogo.AI.CastSpell(1,0));
					node2.AddChild(new Mogo.AI.EnterCD(0));
				}
				{
					Mogo.AI.SequenceNode node8 = new Mogo.AI.SequenceNode();
					node1.AddChild(node8);
					node8.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
					node8.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node11 = new Mogo.AI.PrioritySelectorNode();
						node8.AddChild(node11);
						{
							Mogo.AI.SequenceNode node12 = new Mogo.AI.SequenceNode();
							node11.AddChild(node12);
							{
								Mogo.AI.Not node13 = new Mogo.AI.Not();
								node12.AddChild(node13);
								node13.Proxy(new Mogo.AI.TestBuff(0,4740));
							}
							node12.AddChild(new Mogo.AI.CmpTargetDistance(Mogo.AI.CmpType.ge,500));
							node12.AddChild(new Mogo.AI.InSkillCoolDown(5));
							node12.AddChild(new Mogo.AI.InSkillRange(5));
							node12.AddChild(new Mogo.AI.CastSpell(5,0));
							node12.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node20 = new Mogo.AI.SequenceNode();
							node11.AddChild(node20);
							node20.AddChild(new Mogo.AI.InSkillCoolDown(4));
							node20.AddChild(new Mogo.AI.InSkillRange(4));
							node20.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
							node20.AddChild(new Mogo.AI.CastSpell(4,0));
							node20.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node26 = new Mogo.AI.SequenceNode();
							node11.AddChild(node26);
							node26.AddChild(new Mogo.AI.InSkillCoolDown(3));
							node26.AddChild(new Mogo.AI.InSkillRange(3));
							node26.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,60));
							node26.AddChild(new Mogo.AI.CastSpell(3,0));
							node26.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node32 = new Mogo.AI.SequenceNode();
							node11.AddChild(node32);
							node32.AddChild(new Mogo.AI.InSkillCoolDown(2));
							node32.AddChild(new Mogo.AI.InSkillRange(2));
							node32.AddChild(new Mogo.AI.CastSpell(2,0));
							node32.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node37 = new Mogo.AI.SequenceNode();
							node11.AddChild(node37);
							{
								Mogo.AI.Not node38 = new Mogo.AI.Not();
								node37.AddChild(node38);
								node38.Proxy(new Mogo.AI.InSkillRange(2));
							}
							node37.AddChild(new Mogo.AI.ChooseCastPoint(2));
							node37.AddChild(new Mogo.AI.MoveTo(1f));
						}
						node11.AddChild(new Mogo.AI.EnterRest(500));
					}
				}
			}
		}
	}
}
