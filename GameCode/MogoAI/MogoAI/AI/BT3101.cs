namespace Mogo.AI.BT
{
	public sealed class BT3101 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT3101 _instance = null;
		public static BT3101 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT3101();

				return _instance;
			}
		}

		private BT3101()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.IsFightState());
					{
						Mogo.AI.SequenceNode node4 = new Mogo.AI.SequenceNode();
						node2.AddChild(node4);
						node4.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
						node4.AddChild(new Mogo.AI.EnterFight());
					}
				}
				{
					Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
					node1.AddChild(node7);
					node7.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
					node7.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node10 = new Mogo.AI.PrioritySelectorNode();
						node7.AddChild(node10);
						{
							Mogo.AI.SequenceNode node11 = new Mogo.AI.SequenceNode();
							node10.AddChild(node11);
							node11.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,20));
							node11.AddChild(new Mogo.AI.InSkillCoolDown(6));
							node11.AddChild(new Mogo.AI.CastSpell(6,0));
							node11.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node16 = new Mogo.AI.SequenceNode();
							node10.AddChild(node16);
							node16.AddChild(new Mogo.AI.InSkillCoolDown(7));
							node16.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,85));
							node16.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.gt,20));
							{
								Mogo.AI.PrioritySelectorNode node20 = new Mogo.AI.PrioritySelectorNode();
								node16.AddChild(node20);
								{
									Mogo.AI.SequenceNode node21 = new Mogo.AI.SequenceNode();
									node20.AddChild(node21);
									node21.AddChild(new Mogo.AI.MoveToAppoint("147,61,92"));
									node21.AddChild(new Mogo.AI.CmpTargetPointDistance(Mogo.AI.CmpType.gt,100));
									node21.AddChild(new Mogo.AI.AddBuff(4002,3000));
									node21.AddChild(new Mogo.AI.CancelAttackTarget());
									node21.AddChild(new Mogo.AI.MoveTo(1.5f));
								}
								{
									Mogo.AI.SequenceNode node27 = new Mogo.AI.SequenceNode();
									node20.AddChild(node27);
									{
										Mogo.AI.Not node28 = new Mogo.AI.Not();
										node27.AddChild(node28);
										node28.Proxy(new Mogo.AI.BossPartHasBeenBroken(102));
									}
									node27.AddChild(new Mogo.AI.InSkillCoolDown(5));
									node27.AddChild(new Mogo.AI.CastSpell(5,0));
									node27.AddChild(new Mogo.AI.EnterCD(0));
								}
								{
									Mogo.AI.SequenceNode node33 = new Mogo.AI.SequenceNode();
									node20.AddChild(node33);
									node33.AddChild(new Mogo.AI.CastSpell(7,0));
									node33.AddChild(new Mogo.AI.EnterCD(0));
								}
							}
						}
						{
							Mogo.AI.SequenceNode node36 = new Mogo.AI.SequenceNode();
							node10.AddChild(node36);
							node36.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,20));
							{
								Mogo.AI.Not node38 = new Mogo.AI.Not();
								node36.AddChild(node38);
								node38.Proxy(new Mogo.AI.BossPartHasBeenBroken(102));
							}
							node36.AddChild(new Mogo.AI.InSkillCoolDown(8));
							node36.AddChild(new Mogo.AI.InSkillRange(8));
							node36.AddChild(new Mogo.AI.CastSpell(8,0));
							node36.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node44 = new Mogo.AI.SequenceNode();
							node10.AddChild(node44);
							node44.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,95));
							{
								Mogo.AI.Not node46 = new Mogo.AI.Not();
								node44.AddChild(node46);
								node46.Proxy(new Mogo.AI.BossPartHasBeenBroken(103));
							}
							node44.AddChild(new Mogo.AI.InSkillCoolDown(4));
							node44.AddChild(new Mogo.AI.InSkillRange(4));
							node44.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
							node44.AddChild(new Mogo.AI.CastSpell(4,0));
							node44.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node53 = new Mogo.AI.SequenceNode();
							node10.AddChild(node53);
							node53.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,95));
							{
								Mogo.AI.Not node55 = new Mogo.AI.Not();
								node53.AddChild(node55);
								node55.Proxy(new Mogo.AI.BossPartHasBeenBroken(101));
							}
							node53.AddChild(new Mogo.AI.InSkillCoolDown(3));
							node53.AddChild(new Mogo.AI.InSkillRange(3));
							node53.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,40));
							node53.AddChild(new Mogo.AI.CastSpell(3,0));
							node53.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node62 = new Mogo.AI.SequenceNode();
							node10.AddChild(node62);
							node62.AddChild(new Mogo.AI.InSkillCoolDown(2));
							node62.AddChild(new Mogo.AI.InSkillRange(2));
							node62.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
							node62.AddChild(new Mogo.AI.CastSpell(2,0));
							node62.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node68 = new Mogo.AI.SequenceNode();
							node10.AddChild(node68);
							node68.AddChild(new Mogo.AI.InSkillCoolDown(1));
							node68.AddChild(new Mogo.AI.InSkillRange(1));
							node68.AddChild(new Mogo.AI.CastSpell(1,0));
							node68.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node73 = new Mogo.AI.SequenceNode();
							node10.AddChild(node73);
							{
								Mogo.AI.Not node74 = new Mogo.AI.Not();
								node73.AddChild(node74);
								node74.Proxy(new Mogo.AI.InSkillRange(1));
							}
							node73.AddChild(new Mogo.AI.ChooseCastPoint(1));
							node73.AddChild(new Mogo.AI.MoveTo(1f));
						}
						node10.AddChild(new Mogo.AI.EnterRest(500));
					}
				}
			}
		}
	}
}
