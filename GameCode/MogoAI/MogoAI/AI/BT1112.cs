namespace Mogo.AI.BT
{
	public sealed class BT1112 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1112 _instance = null;
		public static BT1112 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1112();

				return _instance;
			}
		}

		private BT1112()
		{
			{
				Mogo.AI.PrioritySelectorNode node1 = new Mogo.AI.PrioritySelectorNode();
				this.AddChild(node1);
				{
					Mogo.AI.SequenceNode node2 = new Mogo.AI.SequenceNode();
					node1.AddChild(node2);
					{
						Mogo.AI.Not node3 = new Mogo.AI.Not();
						node2.AddChild(node3);
						node3.Proxy(new Mogo.AI.CmpTargetMapType("0,1"));
					}
					node2.AddChild(new Mogo.AI.QuitFollowing());
				}
				{
					Mogo.AI.SequenceNode node6 = new Mogo.AI.SequenceNode();
					node1.AddChild(node6);
					{
						Mogo.AI.Not node7 = new Mogo.AI.Not();
						node6.AddChild(node7);
						node7.Proxy(new Mogo.AI.CmpLeaderMapType("0,1"));
					}
					node6.AddChild(new Mogo.AI.QuitFollowing());
				}
				{
					Mogo.AI.SequenceNode node10 = new Mogo.AI.SequenceNode();
					node1.AddChild(node10);
					{
						Mogo.AI.Not node11 = new Mogo.AI.Not();
						node10.AddChild(node11);
						node11.Proxy(new Mogo.AI.CmpLeaderInTheSameScene());
					}
					{
						Mogo.AI.PrioritySelectorNode node13 = new Mogo.AI.PrioritySelectorNode();
						node10.AddChild(node13);
						{
							Mogo.AI.SequenceNode node14 = new Mogo.AI.SequenceNode();
							node13.AddChild(node14);
							{
								Mogo.AI.Not node15 = new Mogo.AI.Not();
								node14.AddChild(node15);
								node15.Proxy(new Mogo.AI.CmpFlagTime(Mogo.AI.CmpType.gt,1,0));
							}
							node14.AddChild(new Mogo.AI.TeleportToLeader());
							node14.AddChild(new Mogo.AI.SetFlag(1));
						}
						{
							Mogo.AI.SequenceNode node19 = new Mogo.AI.SequenceNode();
							node13.AddChild(node19);
							node19.AddChild(new Mogo.AI.CmpFlagTime(Mogo.AI.CmpType.gt,1,2000));
							node19.AddChild(new Mogo.AI.TeleportToLeader());
							node19.AddChild(new Mogo.AI.SetFlag(1));
						}
					}
					node10.AddChild(new Mogo.AI.EnterRest(500));
				}
				{
					Mogo.AI.SequenceNode node24 = new Mogo.AI.SequenceNode();
					node1.AddChild(node24);
					node24.AddChild(new Mogo.AI.CmpMasterDistance(Mogo.AI.CmpType.ge,4000));
					node24.AddChild(new Mogo.AI.TeleportToLeader());
					node24.AddChild(new Mogo.AI.EnterRest(500));
				}
				{
					Mogo.AI.PrioritySelectorNode node28 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node28);
					{
						Mogo.AI.SequenceNode node29 = new Mogo.AI.SequenceNode();
						node28.AddChild(node29);
						node29.AddChild(new Mogo.AI.IsInChase());
						node29.AddChild(new Mogo.AI.FollowLeader(150,300,450,1));
					}
					{
						Mogo.AI.SequenceNode node32 = new Mogo.AI.SequenceNode();
						node28.AddChild(node32);
						node32.AddChild(new Mogo.AI.CmpMasterDistance(Mogo.AI.CmpType.ge,2000));
						node32.AddChild(new Mogo.AI.FollowLeader(150,300,450,1));
					}
				}
				{
					Mogo.AI.SequenceNode node35 = new Mogo.AI.SequenceNode();
					node1.AddChild(node35);
					node35.AddChild(new Mogo.AI.PetAOI(100,0,1500,0,0,0));
					node35.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node38 = new Mogo.AI.PrioritySelectorNode();
						node35.AddChild(node38);
						{
							Mogo.AI.SequenceNode node39 = new Mogo.AI.SequenceNode();
							node38.AddChild(node39);
							node39.AddChild(new Mogo.AI.InSkillCoolDown(7));
							node39.AddChild(new Mogo.AI.InSkillRange(7));
							node39.AddChild(new Mogo.AI.CastSpell(7,0));
							node39.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node44 = new Mogo.AI.SequenceNode();
							node38.AddChild(node44);
							node44.AddChild(new Mogo.AI.InSkillCoolDown(6));
							node44.AddChild(new Mogo.AI.InSkillRange(6));
							node44.AddChild(new Mogo.AI.CastSpell(6,0));
							node44.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node49 = new Mogo.AI.SequenceNode();
							node38.AddChild(node49);
							node49.AddChild(new Mogo.AI.InSkillCoolDown(5));
							node49.AddChild(new Mogo.AI.InSkillRange(5));
							node49.AddChild(new Mogo.AI.CastSpell(5,0));
							node49.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node54 = new Mogo.AI.SequenceNode();
							node38.AddChild(node54);
							node54.AddChild(new Mogo.AI.InSkillCoolDown(4));
							node54.AddChild(new Mogo.AI.InSkillRange(4));
							node54.AddChild(new Mogo.AI.CastSpell(4,0));
							node54.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node59 = new Mogo.AI.SequenceNode();
							node38.AddChild(node59);
							node59.AddChild(new Mogo.AI.InSkillCoolDown(3));
							node59.AddChild(new Mogo.AI.InSkillRange(3));
							node59.AddChild(new Mogo.AI.CastSpell(3,0));
							node59.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node64 = new Mogo.AI.SequenceNode();
							node38.AddChild(node64);
							node64.AddChild(new Mogo.AI.InSkillCoolDown(2));
							node64.AddChild(new Mogo.AI.InSkillRange(2));
							node64.AddChild(new Mogo.AI.CastSpell(2,0));
							node64.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node69 = new Mogo.AI.SequenceNode();
							node38.AddChild(node69);
							node69.AddChild(new Mogo.AI.InSkillCoolDown(1));
							node69.AddChild(new Mogo.AI.InSkillRange(1));
							node69.AddChild(new Mogo.AI.CastSpell(1,0));
							node69.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node74 = new Mogo.AI.SequenceNode();
							node38.AddChild(node74);
							{
								Mogo.AI.Not node75 = new Mogo.AI.Not();
								node74.AddChild(node75);
								node75.Proxy(new Mogo.AI.InSkillRange(1));
							}
							node74.AddChild(new Mogo.AI.ChooseCastPoint(1));
							node74.AddChild(new Mogo.AI.MoveTo(1f));
						}
					}
					node35.AddChild(new Mogo.AI.EnterRest(500));
				}
				node1.AddChild(new Mogo.AI.FollowLeader(150,300,450,0));
			}
		}
	}
}
