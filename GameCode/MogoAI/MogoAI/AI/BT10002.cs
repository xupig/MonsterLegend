namespace Mogo.AI.BT
{
	public sealed class BT10002 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT10002 _instance = null;
		public static BT10002 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT10002();

				return _instance;
			}
		}

		private BT10002()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.Not node2 = new Mogo.AI.Not();
					node1.AddChild(node2);
					node2.Proxy(new Mogo.AI.IsCD());
				}
				{
					Mogo.AI.PrioritySelectorNode node4 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node4);
					node4.AddChild(new Mogo.AI.FindTargetBySkillRange(1));
					node4.AddChild(new Mogo.AI.FindTargetByMonsterID(10005,80000));
					node4.AddChild(new Mogo.AI.FindTargetByMonsterID(10006,80000));
				}
				node1.AddChild(new Mogo.AI.IsTargetCanBeAttack());
				{
					Mogo.AI.PrioritySelectorNode node9 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node9);
					{
						Mogo.AI.SequenceNode node10 = new Mogo.AI.SequenceNode();
						node9.AddChild(node10);
						node10.AddChild(new Mogo.AI.InSkillCoolDown(1));
						node10.AddChild(new Mogo.AI.InSkillRange(1));
						node10.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
						node10.AddChild(new Mogo.AI.CastSpell(1,0));
						node10.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node16 = new Mogo.AI.SequenceNode();
						node9.AddChild(node16);
						node16.AddChild(new Mogo.AI.InSkillRange(1));
						{
							Mogo.AI.PrioritySelectorNode node18 = new Mogo.AI.PrioritySelectorNode();
							node16.AddChild(node18);
							{
								Mogo.AI.SequenceNode node19 = new Mogo.AI.SequenceNode();
								node18.AddChild(node19);
								node19.AddChild(new Mogo.AI.HasFriendInLeftRange(50));
								{
									Mogo.AI.PrioritySelectorNode node21 = new Mogo.AI.PrioritySelectorNode();
									node19.AddChild(node21);
									{
										Mogo.AI.SequenceNode node22 = new Mogo.AI.SequenceNode();
										node21.AddChild(node22);
										{
											Mogo.AI.Not node23 = new Mogo.AI.Not();
											node22.AddChild(node23);
											node23.Proxy(new Mogo.AI.HasFriendInRightRange(50));
										}
										node22.AddChild(new Mogo.AI.LookOn(100,200,0,0,0,100,0,0,500,500,0,300,0,1));
									}
									node21.AddChild(new Mogo.AI.EnterRest(500));
								}
							}
							{
								Mogo.AI.SequenceNode node27 = new Mogo.AI.SequenceNode();
								node18.AddChild(node27);
								node27.AddChild(new Mogo.AI.HasFriendInRightRange(50));
								node27.AddChild(new Mogo.AI.LookOn(100,200,0,0,100,0,0,0,500,500,300,0,0,1));
							}
							node18.AddChild(new Mogo.AI.LookOn(150,800,0,0,0,0,100,0,500,500,500,500,500,1));
						}
					}
					{
						Mogo.AI.SequenceNode node31 = new Mogo.AI.SequenceNode();
						node9.AddChild(node31);
						node31.AddChild(new Mogo.AI.ChooseCastPoint(1));
						node31.AddChild(new Mogo.AI.MoveTo(1f));
					}
				}
			}
		}
	}
}
