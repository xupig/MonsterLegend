namespace Mogo.AI.BT
{
	public sealed class BT2412 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT2412 _instance = null;
		public static BT2412 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT2412();

				return _instance;
			}
		}

		private BT2412()
		{
			{
				Mogo.AI.PrioritySelectorNode node1 = new Mogo.AI.PrioritySelectorNode();
				this.AddChild(node1);
				{
					Mogo.AI.SequenceNode node2 = new Mogo.AI.SequenceNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
					node2.AddChild(new Mogo.AI.Patrol(1000,2000,200));
				}
				{
					Mogo.AI.SequenceNode node5 = new Mogo.AI.SequenceNode();
					node1.AddChild(node5);
					{
						Mogo.AI.PrioritySelectorNode node6 = new Mogo.AI.PrioritySelectorNode();
						node5.AddChild(node6);
						{
							Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
							node6.AddChild(node7);
							{
								Mogo.AI.PrioritySelectorNode node8 = new Mogo.AI.PrioritySelectorNode();
								node7.AddChild(node8);
								node8.AddChild(new Mogo.AI.FindTargetByMonsterID(12100,20000));
								node8.AddChild(new Mogo.AI.FindTargetByMonsterID(12102,20000));
							}
							node7.AddChild(new Mogo.AI.MoveToTargetPoint());
							node7.AddChild(new Mogo.AI.CmpTargetDistance(Mogo.AI.CmpType.gt,200));
							node7.AddChild(new Mogo.AI.MoveTo(1f));
						}
						node6.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
					}
					node5.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node16 = new Mogo.AI.PrioritySelectorNode();
						node5.AddChild(node16);
						{
							Mogo.AI.SequenceNode node17 = new Mogo.AI.SequenceNode();
							node16.AddChild(node17);
							node17.AddChild(new Mogo.AI.InSkillCoolDown(4));
							node17.AddChild(new Mogo.AI.InSkillRange(4));
							node17.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
							node17.AddChild(new Mogo.AI.CastSpell(4,0));
							node17.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node23 = new Mogo.AI.SequenceNode();
							node16.AddChild(node23);
							node23.AddChild(new Mogo.AI.InSkillCoolDown(3));
							node23.AddChild(new Mogo.AI.InSkillRange(3));
							node23.AddChild(new Mogo.AI.CastSpell(3,0));
							node23.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node28 = new Mogo.AI.SequenceNode();
							node16.AddChild(node28);
							node28.AddChild(new Mogo.AI.InSkillCoolDown(1));
							node28.AddChild(new Mogo.AI.InSkillRange(1));
							node28.AddChild(new Mogo.AI.CastSpell(1,0));
							node28.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node33 = new Mogo.AI.SequenceNode();
							node16.AddChild(node33);
							node33.AddChild(new Mogo.AI.InSkillRange(1));
							{
								Mogo.AI.PrioritySelectorNode node35 = new Mogo.AI.PrioritySelectorNode();
								node33.AddChild(node35);
								{
									Mogo.AI.SequenceNode node36 = new Mogo.AI.SequenceNode();
									node35.AddChild(node36);
									node36.AddChild(new Mogo.AI.HasFriendInLeftRange(50));
									{
										Mogo.AI.PrioritySelectorNode node38 = new Mogo.AI.PrioritySelectorNode();
										node36.AddChild(node38);
										{
											Mogo.AI.SequenceNode node39 = new Mogo.AI.SequenceNode();
											node38.AddChild(node39);
											{
												Mogo.AI.Not node40 = new Mogo.AI.Not();
												node39.AddChild(node40);
												node40.Proxy(new Mogo.AI.HasFriendInRightRange(50));
											}
											node39.AddChild(new Mogo.AI.LookOn(100,200,0,0,0,100,0,0,500,500,0,300,0,1));
										}
										node38.AddChild(new Mogo.AI.EnterCD(500));
									}
								}
								{
									Mogo.AI.SequenceNode node44 = new Mogo.AI.SequenceNode();
									node35.AddChild(node44);
									node44.AddChild(new Mogo.AI.HasFriendInRightRange(0));
									node44.AddChild(new Mogo.AI.LookOn(100,200,0,0,100,0,0,0,500,500,300,0,0,1));
								}
								node35.AddChild(new Mogo.AI.LookOn(150,800,0,0,0,0,100,0,500,500,500,500,500,1));
							}
						}
						{
							Mogo.AI.SequenceNode node48 = new Mogo.AI.SequenceNode();
							node16.AddChild(node48);
							{
								Mogo.AI.Not node49 = new Mogo.AI.Not();
								node48.AddChild(node49);
								node49.Proxy(new Mogo.AI.InSkillRange(1));
							}
							node48.AddChild(new Mogo.AI.ChooseCastPoint(1));
							node48.AddChild(new Mogo.AI.MoveTo(1f));
						}
					}
				}
			}
		}
	}
}
