namespace Mogo.AI.BT
{
	public sealed class BT1001 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1001 _instance = null;
		public static BT1001 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1001();

				return _instance;
			}
		}

		private BT1001()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					{
						Mogo.AI.SequenceNode node3 = new Mogo.AI.SequenceNode();
						node2.AddChild(node3);
						node3.AddChild(new Mogo.AI.IsFirstThinking());
						node3.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node3.AddChild(new Mogo.AI.MonsterSpeech(1,1));
					}
					{
						Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
						node2.AddChild(node7);
						node7.AddChild(new Mogo.AI.IsInDeathState());
						node7.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node7.AddChild(new Mogo.AI.MonsterSpeech(2,1));
						node7.AddChild(new Mogo.AI.EnterRest(0));
					}
					{
						Mogo.AI.SequenceNode node12 = new Mogo.AI.SequenceNode();
						node2.AddChild(node12);
						node12.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,10));
						node12.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node12.AddChild(new Mogo.AI.MonsterSpeech(3,1));
					}
					{
						Mogo.AI.SequenceNode node16 = new Mogo.AI.SequenceNode();
						node2.AddChild(node16);
						node16.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,30));
						node16.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node16.AddChild(new Mogo.AI.MonsterSpeech(4,1));
					}
					{
						Mogo.AI.SequenceNode node20 = new Mogo.AI.SequenceNode();
						node2.AddChild(node20);
						node20.AddChild(new Mogo.AI.IsFightState());
						node20.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,5));
						node20.AddChild(new Mogo.AI.MonsterSpeech(5,1));
					}
					{
						Mogo.AI.SequenceNode node24 = new Mogo.AI.SequenceNode();
						node2.AddChild(node24);
						node24.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.le,100));
					}
				}
				{
					Mogo.AI.PrioritySelectorNode node26 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node26);
					{
						Mogo.AI.PrioritySelectorNode node27 = new Mogo.AI.PrioritySelectorNode();
						node26.AddChild(node27);
						{
							Mogo.AI.SequenceNode node28 = new Mogo.AI.SequenceNode();
							node27.AddChild(node28);
							node28.AddChild(new Mogo.AI.IsForceBackState());
							node28.AddChild(new Mogo.AI.CancelFightState());
							node28.AddChild(new Mogo.AI.CancelAttackTarget());
							node28.AddChild(new Mogo.AI.MonsterBackBornPoint(1,100));
						}
						{
							Mogo.AI.SequenceNode node33 = new Mogo.AI.SequenceNode();
							node27.AddChild(node33);
							node33.AddChild(new Mogo.AI.CmpBornPointDistance(Mogo.AI.CmpType.ge,2000));
							node33.AddChild(new Mogo.AI.CancelFightState());
							node33.AddChild(new Mogo.AI.CancelAttackTarget());
							node33.AddChild(new Mogo.AI.MonsterBackBornPoint(1,100));
						}
						{
							Mogo.AI.SequenceNode node38 = new Mogo.AI.SequenceNode();
							node27.AddChild(node38);
							{
								Mogo.AI.Not node39 = new Mogo.AI.Not();
								node38.AddChild(node39);
								node39.Proxy(new Mogo.AI.IsFightState());
							}
							{
								Mogo.AI.Not node41 = new Mogo.AI.Not();
								node38.AddChild(node41);
								node41.Proxy(new Mogo.AI.AOI(100,1,0,"500",0));
							}
							node38.AddChild(new Mogo.AI.CancelAttackTarget());
							node38.AddChild(new Mogo.AI.MonsterBackBornPoint(1,100));
						}
					}
					{
						Mogo.AI.SequenceNode node45 = new Mogo.AI.SequenceNode();
						node26.AddChild(node45);
						{
							Mogo.AI.PrioritySelectorNode node46 = new Mogo.AI.PrioritySelectorNode();
							node45.AddChild(node46);
							node46.AddChild(new Mogo.AI.IsFightState());
							{
								Mogo.AI.SequenceNode node48 = new Mogo.AI.SequenceNode();
								node46.AddChild(node48);
								node48.AddChild(new Mogo.AI.AOI(100,1,0,"500",0));
								node48.AddChild(new Mogo.AI.EnterFight());
							}
						}
						{
							Mogo.AI.SequenceNode node51 = new Mogo.AI.SequenceNode();
							node45.AddChild(node51);
							node51.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
							node51.AddChild(new Mogo.AI.IsTargetCanBeAttack());
							{
								Mogo.AI.PrioritySelectorNode node54 = new Mogo.AI.PrioritySelectorNode();
								node51.AddChild(node54);
								{
									Mogo.AI.SequenceNode node55 = new Mogo.AI.SequenceNode();
									node54.AddChild(node55);
									node55.AddChild(new Mogo.AI.InSkillCoolDown(5));
									node55.AddChild(new Mogo.AI.InSkillRange(5));
									node55.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
									node55.AddChild(new Mogo.AI.CastSpell(5,0));
									node55.AddChild(new Mogo.AI.EnterCD(0));
								}
								{
									Mogo.AI.SequenceNode node61 = new Mogo.AI.SequenceNode();
									node54.AddChild(node61);
									node61.AddChild(new Mogo.AI.InSkillCoolDown(4));
									node61.AddChild(new Mogo.AI.InSkillRange(4));
									node61.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
									node61.AddChild(new Mogo.AI.CastSpell(4,0));
									node61.AddChild(new Mogo.AI.EnterCD(0));
								}
								{
									Mogo.AI.SequenceNode node67 = new Mogo.AI.SequenceNode();
									node54.AddChild(node67);
									node67.AddChild(new Mogo.AI.InSkillCoolDown(3));
									node67.AddChild(new Mogo.AI.InSkillRange(3));
									node67.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,40));
									node67.AddChild(new Mogo.AI.CastSpell(3,0));
									node67.AddChild(new Mogo.AI.EnterCD(0));
								}
								{
									Mogo.AI.SequenceNode node73 = new Mogo.AI.SequenceNode();
									node54.AddChild(node73);
									node73.AddChild(new Mogo.AI.InSkillCoolDown(2));
									node73.AddChild(new Mogo.AI.InSkillRange(2));
									node73.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
									node73.AddChild(new Mogo.AI.CastSpell(2,0));
									node73.AddChild(new Mogo.AI.EnterCD(0));
								}
								{
									Mogo.AI.SequenceNode node79 = new Mogo.AI.SequenceNode();
									node54.AddChild(node79);
									node79.AddChild(new Mogo.AI.InSkillCoolDown(1));
									node79.AddChild(new Mogo.AI.InSkillRange(1));
									node79.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
									node79.AddChild(new Mogo.AI.CastSpell(1,0));
									node79.AddChild(new Mogo.AI.EnterCD(0));
								}
								{
									Mogo.AI.SequenceNode node85 = new Mogo.AI.SequenceNode();
									node54.AddChild(node85);
									node85.AddChild(new Mogo.AI.InSkillRange(1));
									{
										Mogo.AI.PrioritySelectorNode node87 = new Mogo.AI.PrioritySelectorNode();
										node85.AddChild(node87);
										{
											Mogo.AI.SequenceNode node88 = new Mogo.AI.SequenceNode();
											node87.AddChild(node88);
											node88.AddChild(new Mogo.AI.HasFriendInLeftRange(50));
											{
												Mogo.AI.PrioritySelectorNode node90 = new Mogo.AI.PrioritySelectorNode();
												node88.AddChild(node90);
												{
													Mogo.AI.SequenceNode node91 = new Mogo.AI.SequenceNode();
													node90.AddChild(node91);
													{
														Mogo.AI.Not node92 = new Mogo.AI.Not();
														node91.AddChild(node92);
														node92.Proxy(new Mogo.AI.HasFriendInRightRange(50));
													}
													node91.AddChild(new Mogo.AI.LookOn(100,200,0,0,0,100,0,0,500,500,0,300,0,1));
												}
												node90.AddChild(new Mogo.AI.EnterRest(500));
											}
										}
										{
											Mogo.AI.SequenceNode node96 = new Mogo.AI.SequenceNode();
											node87.AddChild(node96);
											node96.AddChild(new Mogo.AI.HasFriendInRightRange(50));
											node96.AddChild(new Mogo.AI.LookOn(100,200,0,0,100,0,0,0,500,500,300,0,0,1));
										}
										node87.AddChild(new Mogo.AI.LookOn(150,800,0,0,0,0,100,0,500,500,500,500,500,1));
									}
								}
								{
									Mogo.AI.SequenceNode node100 = new Mogo.AI.SequenceNode();
									node54.AddChild(node100);
									{
										Mogo.AI.Not node101 = new Mogo.AI.Not();
										node100.AddChild(node101);
										node101.Proxy(new Mogo.AI.InSkillRange(1));
									}
									node100.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
									node100.AddChild(new Mogo.AI.ChooseCastPoint(1));
									node100.AddChild(new Mogo.AI.MoveTo(1f));
								}
								node54.AddChild(new Mogo.AI.EnterRest(500));
							}
						}
					}
				}
			}
		}
	}
}
