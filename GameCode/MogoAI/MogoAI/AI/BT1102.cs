namespace Mogo.AI.BT
{
	public sealed class BT1102 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1102 _instance = null;
		public static BT1102 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1102();

				return _instance;
			}
		}

		private BT1102()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					{
						Mogo.AI.SequenceNode node3 = new Mogo.AI.SequenceNode();
						node2.AddChild(node3);
						node3.AddChild(new Mogo.AI.IsFirstThinking());
						node3.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node3.AddChild(new Mogo.AI.MonsterSpeech(1,1));
					}
					{
						Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
						node2.AddChild(node7);
						node7.AddChild(new Mogo.AI.IsInDeathState());
						node7.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node7.AddChild(new Mogo.AI.MonsterSpeech(2,1));
						node7.AddChild(new Mogo.AI.EnterRest(5000));
					}
					{
						Mogo.AI.SequenceNode node12 = new Mogo.AI.SequenceNode();
						node2.AddChild(node12);
						node12.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,10));
						node12.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node12.AddChild(new Mogo.AI.MonsterSpeech(3,1));
					}
					{
						Mogo.AI.SequenceNode node16 = new Mogo.AI.SequenceNode();
						node2.AddChild(node16);
						node16.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,30));
						node16.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node16.AddChild(new Mogo.AI.MonsterSpeech(4,1));
					}
					{
						Mogo.AI.SequenceNode node20 = new Mogo.AI.SequenceNode();
						node2.AddChild(node20);
						node20.AddChild(new Mogo.AI.IsFightState());
						node20.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,5));
						node20.AddChild(new Mogo.AI.MonsterSpeech(5,1));
					}
					{
						Mogo.AI.SequenceNode node24 = new Mogo.AI.SequenceNode();
						node2.AddChild(node24);
						node24.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.le,100));
					}
				}
				{
					Mogo.AI.SequenceNode node26 = new Mogo.AI.SequenceNode();
					node1.AddChild(node26);
					node26.AddChild(new Mogo.AI.IsFightState());
					node26.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
					node26.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node30 = new Mogo.AI.PrioritySelectorNode();
						node26.AddChild(node30);
						{
							Mogo.AI.SequenceNode node31 = new Mogo.AI.SequenceNode();
							node30.AddChild(node31);
							node31.AddChild(new Mogo.AI.InSkillCoolDown(7));
							node31.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
							node31.AddChild(new Mogo.AI.CastSpell(7,0));
							node31.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node36 = new Mogo.AI.SequenceNode();
							node30.AddChild(node36);
							node36.AddChild(new Mogo.AI.InSkillCoolDown(6));
							node36.AddChild(new Mogo.AI.InSkillRange(6));
							node36.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,15));
							node36.AddChild(new Mogo.AI.CastSpell(6,0));
							node36.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node42 = new Mogo.AI.SequenceNode();
							node30.AddChild(node42);
							node42.AddChild(new Mogo.AI.InSkillCoolDown(5));
							node42.AddChild(new Mogo.AI.InSkillRange(5));
							node42.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
							node42.AddChild(new Mogo.AI.CastSpell(5,0));
							node42.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node48 = new Mogo.AI.SequenceNode();
							node30.AddChild(node48);
							node48.AddChild(new Mogo.AI.InSkillCoolDown(4));
							node48.AddChild(new Mogo.AI.InSkillRange(4));
							node48.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
							node48.AddChild(new Mogo.AI.CastSpell(4,0));
							node48.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node54 = new Mogo.AI.SequenceNode();
							node30.AddChild(node54);
							node54.AddChild(new Mogo.AI.InSkillCoolDown(3));
							node54.AddChild(new Mogo.AI.InSkillRange(3));
							node54.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,40));
							node54.AddChild(new Mogo.AI.CastSpell(3,0));
							node54.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node60 = new Mogo.AI.SequenceNode();
							node30.AddChild(node60);
							node60.AddChild(new Mogo.AI.InSkillCoolDown(2));
							node60.AddChild(new Mogo.AI.InSkillRange(2));
							node60.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
							node60.AddChild(new Mogo.AI.CastSpell(2,0));
							node60.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node66 = new Mogo.AI.SequenceNode();
							node30.AddChild(node66);
							node66.AddChild(new Mogo.AI.InSkillCoolDown(1));
							node66.AddChild(new Mogo.AI.InSkillRange(1));
							node66.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
							node66.AddChild(new Mogo.AI.CastSpell(1,0));
							node66.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node72 = new Mogo.AI.SequenceNode();
							node30.AddChild(node72);
							node72.AddChild(new Mogo.AI.InSkillRange(1));
							{
								Mogo.AI.PrioritySelectorNode node74 = new Mogo.AI.PrioritySelectorNode();
								node72.AddChild(node74);
								{
									Mogo.AI.SequenceNode node75 = new Mogo.AI.SequenceNode();
									node74.AddChild(node75);
									node75.AddChild(new Mogo.AI.HasFriendInLeftRange(50));
									{
										Mogo.AI.PrioritySelectorNode node77 = new Mogo.AI.PrioritySelectorNode();
										node75.AddChild(node77);
										{
											Mogo.AI.SequenceNode node78 = new Mogo.AI.SequenceNode();
											node77.AddChild(node78);
											{
												Mogo.AI.Not node79 = new Mogo.AI.Not();
												node78.AddChild(node79);
												node79.Proxy(new Mogo.AI.HasFriendInRightRange(50));
											}
											node78.AddChild(new Mogo.AI.LookOn(100,200,0,0,0,100,0,0,500,500,0,300,0,1));
										}
										node77.AddChild(new Mogo.AI.EnterCD(500));
									}
								}
								{
									Mogo.AI.SequenceNode node83 = new Mogo.AI.SequenceNode();
									node74.AddChild(node83);
									node83.AddChild(new Mogo.AI.HasFriendInRightRange(50));
									node83.AddChild(new Mogo.AI.LookOn(100,200,0,0,100,0,0,0,500,500,300,0,0,1));
								}
								node74.AddChild(new Mogo.AI.LookOn(150,800,0,0,0,0,100,0,500,500,500,500,500,1));
							}
						}
						{
							Mogo.AI.SequenceNode node87 = new Mogo.AI.SequenceNode();
							node30.AddChild(node87);
							{
								Mogo.AI.Not node88 = new Mogo.AI.Not();
								node87.AddChild(node88);
								node88.Proxy(new Mogo.AI.InSkillRange(1));
							}
							node87.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
							node87.AddChild(new Mogo.AI.ChooseCastPoint(1));
							node87.AddChild(new Mogo.AI.MoveTo(1f));
						}
						node30.AddChild(new Mogo.AI.EnterRest(500));
					}
				}
			}
		}
	}
}
