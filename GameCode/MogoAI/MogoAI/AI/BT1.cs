namespace Mogo.AI.BT
{
	public sealed class BT1 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1 _instance = null;
		public static BT1 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1();

				return _instance;
			}
		}

		private BT1()
		{
		}
	}
}
