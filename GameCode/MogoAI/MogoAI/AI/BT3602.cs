namespace Mogo.AI.BT
{
	public sealed class BT3602 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT3602 _instance = null;
		public static BT3602 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT3602();

				return _instance;
			}
		}

		private BT3602()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.IsFightState());
					{
						Mogo.AI.SequenceNode node4 = new Mogo.AI.SequenceNode();
						node2.AddChild(node4);
						node4.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
						node4.AddChild(new Mogo.AI.EnterFight());
					}
				}
				{
					Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
					node1.AddChild(node7);
					node7.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
					node7.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node10 = new Mogo.AI.PrioritySelectorNode();
						node7.AddChild(node10);
						{
							Mogo.AI.SequenceNode node11 = new Mogo.AI.SequenceNode();
							node10.AddChild(node11);
							node11.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,20));
							node11.AddChild(new Mogo.AI.InSkillCoolDown(7));
							node11.AddChild(new Mogo.AI.CastSpell(7,0));
							node11.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node16 = new Mogo.AI.SequenceNode();
							node10.AddChild(node16);
							node16.AddChild(new Mogo.AI.InSkillCoolDown(4));
							node16.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,80));
							node16.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.gt,20));
							{
								Mogo.AI.PrioritySelectorNode node20 = new Mogo.AI.PrioritySelectorNode();
								node16.AddChild(node20);
								{
									Mogo.AI.SequenceNode node21 = new Mogo.AI.SequenceNode();
									node20.AddChild(node21);
									node21.AddChild(new Mogo.AI.InSkillCoolDown(3));
									node21.AddChild(new Mogo.AI.CastSpell(3,0));
									node21.AddChild(new Mogo.AI.EnterCD(0));
								}
								{
									Mogo.AI.SequenceNode node25 = new Mogo.AI.SequenceNode();
									node20.AddChild(node25);
									node25.AddChild(new Mogo.AI.InSkillCoolDown(4));
									node25.AddChild(new Mogo.AI.CastSpell(4,0));
									node25.AddChild(new Mogo.AI.EnterCD(0));
								}
							}
						}
						{
							Mogo.AI.SequenceNode node29 = new Mogo.AI.SequenceNode();
							node10.AddChild(node29);
							{
								Mogo.AI.Not node30 = new Mogo.AI.Not();
								node29.AddChild(node30);
								node30.Proxy(new Mogo.AI.BossPartHasBeenBroken(112));
							}
							node29.AddChild(new Mogo.AI.InSkillCoolDown(5));
							node29.AddChild(new Mogo.AI.InSkillRange(5));
							node29.AddChild(new Mogo.AI.CastSpell(5,0));
							node29.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node36 = new Mogo.AI.SequenceNode();
							node10.AddChild(node36);
							node36.AddChild(new Mogo.AI.CmpSelfHP(Mogo.AI.CmpType.le,90));
							node36.AddChild(new Mogo.AI.InSkillCoolDown(6));
							node36.AddChild(new Mogo.AI.InSkillRange(6));
							node36.AddChild(new Mogo.AI.CastSpell(6,0));
							node36.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
							node36.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node43 = new Mogo.AI.SequenceNode();
							node10.AddChild(node43);
							node43.AddChild(new Mogo.AI.CmpTargetDistance(Mogo.AI.CmpType.gt,500));
							node43.AddChild(new Mogo.AI.InSkillCoolDown(2));
							node43.AddChild(new Mogo.AI.InSkillRange(2));
							node43.AddChild(new Mogo.AI.CastSpell(2,0));
							node43.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node49 = new Mogo.AI.SequenceNode();
							node10.AddChild(node49);
							node49.AddChild(new Mogo.AI.InSkillCoolDown(1));
							node49.AddChild(new Mogo.AI.InSkillRange(1));
							node49.AddChild(new Mogo.AI.CastSpell(1,0));
							node49.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node54 = new Mogo.AI.SequenceNode();
							node10.AddChild(node54);
							{
								Mogo.AI.Not node55 = new Mogo.AI.Not();
								node54.AddChild(node55);
								node55.Proxy(new Mogo.AI.InSkillRange(1));
							}
							node54.AddChild(new Mogo.AI.ChooseCastPoint(1));
							node54.AddChild(new Mogo.AI.MoveTo(1f));
						}
						node10.AddChild(new Mogo.AI.EnterRest(500));
					}
				}
			}
		}
	}
}
