namespace Mogo.AI.BT
{
	public sealed class BT1104 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1104 _instance = null;
		public static BT1104 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1104();

				return _instance;
			}
		}

		private BT1104()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				node1.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
				node1.AddChild(new Mogo.AI.IsTargetCanBeAttack());
				{
					Mogo.AI.PrioritySelectorNode node4 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node4);
					{
						Mogo.AI.SequenceNode node5 = new Mogo.AI.SequenceNode();
						node4.AddChild(node5);
						{
							Mogo.AI.Not node6 = new Mogo.AI.Not();
							node5.AddChild(node6);
							node6.Proxy(new Mogo.AI.TestBuff(0,4749));
						}
						node5.AddChild(new Mogo.AI.CmpAttribute(Mogo.AI.CmpType.ge,160,260));
						node5.AddChild(new Mogo.AI.AddBuff(4749,0));
					}
					{
						Mogo.AI.PrioritySelectorNode node10 = new Mogo.AI.PrioritySelectorNode();
						node4.AddChild(node10);
						{
							Mogo.AI.SequenceNode node11 = new Mogo.AI.SequenceNode();
							node10.AddChild(node11);
							node11.AddChild(new Mogo.AI.BossPartCanBeRecovered(201));
							node11.AddChild(new Mogo.AI.AddBuff(4601,0));
						}
						{
							Mogo.AI.SequenceNode node14 = new Mogo.AI.SequenceNode();
							node10.AddChild(node14);
							node14.AddChild(new Mogo.AI.BossPartCanBeRecovered(202));
							node14.AddChild(new Mogo.AI.AddBuff(4602,0));
						}
						{
							Mogo.AI.SequenceNode node17 = new Mogo.AI.SequenceNode();
							node10.AddChild(node17);
							node17.AddChild(new Mogo.AI.BossPartCanBeRecovered(203));
							node17.AddChild(new Mogo.AI.AddBuff(4603,0));
						}
					}
					{
						Mogo.AI.SequenceNode node20 = new Mogo.AI.SequenceNode();
						node4.AddChild(node20);
						node20.AddChild(new Mogo.AI.InSkillCoolDown(7));
						node20.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
						node20.AddChild(new Mogo.AI.CastSpell(7,0));
						node20.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node25 = new Mogo.AI.SequenceNode();
						node4.AddChild(node25);
						node25.AddChild(new Mogo.AI.InSkillCoolDown(6));
						node25.AddChild(new Mogo.AI.InSkillRange(6));
						node25.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,15));
						node25.AddChild(new Mogo.AI.CastSpell(6,0));
						node25.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node31 = new Mogo.AI.SequenceNode();
						node4.AddChild(node31);
						node31.AddChild(new Mogo.AI.InSkillCoolDown(5));
						node31.AddChild(new Mogo.AI.InSkillRange(5));
						node31.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
						node31.AddChild(new Mogo.AI.CastSpell(5,0));
						node31.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node37 = new Mogo.AI.SequenceNode();
						node4.AddChild(node37);
						node37.AddChild(new Mogo.AI.InSkillCoolDown(4));
						node37.AddChild(new Mogo.AI.InSkillRange(4));
						node37.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
						node37.AddChild(new Mogo.AI.CastSpell(4,0));
						node37.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node43 = new Mogo.AI.SequenceNode();
						node4.AddChild(node43);
						node43.AddChild(new Mogo.AI.InSkillCoolDown(3));
						node43.AddChild(new Mogo.AI.InSkillRange(3));
						node43.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,40));
						node43.AddChild(new Mogo.AI.CastSpell(3,0));
						node43.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node49 = new Mogo.AI.SequenceNode();
						node4.AddChild(node49);
						node49.AddChild(new Mogo.AI.InSkillCoolDown(2));
						node49.AddChild(new Mogo.AI.InSkillRange(2));
						node49.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
						node49.AddChild(new Mogo.AI.CastSpell(2,0));
						node49.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node55 = new Mogo.AI.SequenceNode();
						node4.AddChild(node55);
						node55.AddChild(new Mogo.AI.InSkillCoolDown(1));
						node55.AddChild(new Mogo.AI.InSkillRange(1));
						node55.AddChild(new Mogo.AI.CastSpell(1,0));
						node55.AddChild(new Mogo.AI.EnterCD(0));
					}
					{
						Mogo.AI.SequenceNode node60 = new Mogo.AI.SequenceNode();
						node4.AddChild(node60);
						{
							Mogo.AI.Not node61 = new Mogo.AI.Not();
							node60.AddChild(node61);
							node61.Proxy(new Mogo.AI.InSkillRange(1));
						}
						node60.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
						node60.AddChild(new Mogo.AI.ChooseCastPoint(1));
						node60.AddChild(new Mogo.AI.MoveTo(1f));
					}
					node4.AddChild(new Mogo.AI.EnterRest(500));
				}
			}
		}
	}
}
