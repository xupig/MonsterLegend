namespace Mogo.AI.BT
{
	public sealed class BT1401 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT1401 _instance = null;
		public static BT1401 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT1401();

				return _instance;
			}
		}

		private BT1401()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					{
						Mogo.AI.SequenceNode node3 = new Mogo.AI.SequenceNode();
						node2.AddChild(node3);
						node3.AddChild(new Mogo.AI.IsFirstThinking());
						node3.AddChild(new Mogo.AI.MonsterSpeech(1,1));
					}
					{
						Mogo.AI.SequenceNode node6 = new Mogo.AI.SequenceNode();
						node2.AddChild(node6);
						node6.AddChild(new Mogo.AI.TestBuff(0,4742));
						node6.AddChild(new Mogo.AI.MonsterSpeech(5,1));
					}
					{
						Mogo.AI.SequenceNode node9 = new Mogo.AI.SequenceNode();
						node2.AddChild(node9);
						node9.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.le,100));
					}
				}
				{
					Mogo.AI.SequenceNode node11 = new Mogo.AI.SequenceNode();
					node1.AddChild(node11);
					{
						Mogo.AI.PrioritySelectorNode node12 = new Mogo.AI.PrioritySelectorNode();
						node11.AddChild(node12);
						node12.AddChild(new Mogo.AI.IsFightState());
						{
							Mogo.AI.SequenceNode node14 = new Mogo.AI.SequenceNode();
							node12.AddChild(node14);
							node14.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
							node14.AddChild(new Mogo.AI.EnterFight());
						}
					}
					{
						Mogo.AI.SequenceNode node17 = new Mogo.AI.SequenceNode();
						node11.AddChild(node17);
						node17.AddChild(new Mogo.AI.AOI(100,1,0,"0",1));
						node17.AddChild(new Mogo.AI.IsTargetCanBeAttack());
						{
							Mogo.AI.PrioritySelectorNode node20 = new Mogo.AI.PrioritySelectorNode();
							node17.AddChild(node20);
							{
								Mogo.AI.SequenceNode node21 = new Mogo.AI.SequenceNode();
								node20.AddChild(node21);
								node21.AddChild(new Mogo.AI.InSkillCoolDown(4));
								node21.AddChild(new Mogo.AI.InSkillRange(4));
								node21.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
								node21.AddChild(new Mogo.AI.CastSpell(4,0));
								node21.AddChild(new Mogo.AI.MonsterSpeech(2,1));
								node21.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node28 = new Mogo.AI.SequenceNode();
								node20.AddChild(node28);
								node28.AddChild(new Mogo.AI.InSkillCoolDown(3));
								node28.AddChild(new Mogo.AI.InSkillRange(3));
								node28.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
								node28.AddChild(new Mogo.AI.CastSpell(3,0));
								node28.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node34 = new Mogo.AI.SequenceNode();
								node20.AddChild(node34);
								node34.AddChild(new Mogo.AI.InSkillCoolDown(2));
								node34.AddChild(new Mogo.AI.InSkillRange(2));
								node34.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
								node34.AddChild(new Mogo.AI.CastSpell(2,0));
								node34.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node40 = new Mogo.AI.SequenceNode();
								node20.AddChild(node40);
								node40.AddChild(new Mogo.AI.InSkillCoolDown(1));
								node40.AddChild(new Mogo.AI.InSkillRange(1));
								node40.AddChild(new Mogo.AI.CastSpell(1,0));
								node40.AddChild(new Mogo.AI.EnterCD(0));
							}
							{
								Mogo.AI.SequenceNode node45 = new Mogo.AI.SequenceNode();
								node20.AddChild(node45);
								{
									Mogo.AI.Not node46 = new Mogo.AI.Not();
									node45.AddChild(node46);
									node46.Proxy(new Mogo.AI.InSkillRange(1));
								}
								node45.AddChild(new Mogo.AI.ChooseCastPoint(1));
								node45.AddChild(new Mogo.AI.MoveTo(1f));
							}
							node20.AddChild(new Mogo.AI.EnterRest(500));
						}
					}
				}
			}
		}
	}
}
