namespace Mogo.AI.BT
{
	public sealed class BT10006 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT10006 _instance = null;
		public static BT10006 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT10006();

				return _instance;
			}
		}

		private BT10006()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				node1.AddChild(new Mogo.AI.InSkillCoolDown(1));
				node1.AddChild(new Mogo.AI.CastSpell(1,0));
				{
					Mogo.AI.PrioritySelectorNode node4 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node4);
					{
						Mogo.AI.SequenceNode node5 = new Mogo.AI.SequenceNode();
						node4.AddChild(node5);
						node5.AddChild(new Mogo.AI.AOI(0,1,0,"0",0));
						node5.AddChild(new Mogo.AI.ChooseCastPoint(1));
						node5.AddChild(new Mogo.AI.MoveTo(1f));
					}
					node4.AddChild(new Mogo.AI.Patrol(500,3000,800));
				}
			}
		}
	}
}
