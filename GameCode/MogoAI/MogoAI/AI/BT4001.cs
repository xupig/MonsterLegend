namespace Mogo.AI.BT
{
	public sealed class BT4001 : Mogo.AI.BehaviorTreeRoot
	{
		private static BT4001 _instance = null;
		public static BT4001 Instance
		{
			get
			{
				if(_instance == null)
					_instance = new BT4001();

				return _instance;
			}
		}

		private BT4001()
		{
			{
				Mogo.AI.SequenceNode node1 = new Mogo.AI.SequenceNode();
				this.AddChild(node1);
				{
					Mogo.AI.PrioritySelectorNode node2 = new Mogo.AI.PrioritySelectorNode();
					node1.AddChild(node2);
					node2.AddChild(new Mogo.AI.IsFightState());
					{
						Mogo.AI.SequenceNode node4 = new Mogo.AI.SequenceNode();
						node2.AddChild(node4);
						node4.AddChild(new Mogo.AI.AOI(100,1,0,"0",0));
						node4.AddChild(new Mogo.AI.EnterFight());
					}
				}
				{
					Mogo.AI.SequenceNode node7 = new Mogo.AI.SequenceNode();
					node1.AddChild(node7);
					node7.AddChild(new Mogo.AI.AOI(0,0,0,"0",0));
					node7.AddChild(new Mogo.AI.IsTargetCanBeAttack());
					{
						Mogo.AI.PrioritySelectorNode node10 = new Mogo.AI.PrioritySelectorNode();
						node7.AddChild(node10);
						{
							Mogo.AI.SequenceNode node11 = new Mogo.AI.SequenceNode();
							node10.AddChild(node11);
							node11.AddChild(new Mogo.AI.BossPartHasBeenBroken(1));
							node11.AddChild(new Mogo.AI.BossPartCanBeRecovered(1));
							node11.AddChild(new Mogo.AI.InSkillCoolDown(8));
							node11.AddChild(new Mogo.AI.CastSpell(8,0));
							node11.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node17 = new Mogo.AI.SequenceNode();
							node10.AddChild(node17);
							node17.AddChild(new Mogo.AI.InSkillCoolDown(7));
							node17.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
							node17.AddChild(new Mogo.AI.CastSpell(7,0));
							node17.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node22 = new Mogo.AI.SequenceNode();
							node10.AddChild(node22);
							node22.AddChild(new Mogo.AI.InSkillCoolDown(6));
							node22.AddChild(new Mogo.AI.InSkillRange(6));
							node22.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,15));
							node22.AddChild(new Mogo.AI.CastSpell(6,0));
							node22.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node28 = new Mogo.AI.SequenceNode();
							node10.AddChild(node28);
							node28.AddChild(new Mogo.AI.InSkillCoolDown(5));
							node28.AddChild(new Mogo.AI.InSkillRange(5));
							node28.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,20));
							node28.AddChild(new Mogo.AI.CastSpell(5,0));
							node28.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node34 = new Mogo.AI.SequenceNode();
							node10.AddChild(node34);
							node34.AddChild(new Mogo.AI.InSkillCoolDown(4));
							node34.AddChild(new Mogo.AI.InSkillRange(4));
							node34.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,30));
							node34.AddChild(new Mogo.AI.CastSpell(4,0));
							node34.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node40 = new Mogo.AI.SequenceNode();
							node10.AddChild(node40);
							node40.AddChild(new Mogo.AI.InSkillCoolDown(3));
							node40.AddChild(new Mogo.AI.InSkillRange(3));
							node40.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,40));
							node40.AddChild(new Mogo.AI.CastSpell(3,0));
							node40.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node46 = new Mogo.AI.SequenceNode();
							node10.AddChild(node46);
							node46.AddChild(new Mogo.AI.InSkillCoolDown(2));
							node46.AddChild(new Mogo.AI.InSkillRange(2));
							node46.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
							node46.AddChild(new Mogo.AI.CastSpell(2,0));
							node46.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node52 = new Mogo.AI.SequenceNode();
							node10.AddChild(node52);
							node52.AddChild(new Mogo.AI.InSkillCoolDown(1));
							node52.AddChild(new Mogo.AI.InSkillRange(1));
							node52.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,50));
							node52.AddChild(new Mogo.AI.CastSpell(1,0));
							node52.AddChild(new Mogo.AI.EnterCD(0));
						}
						{
							Mogo.AI.SequenceNode node58 = new Mogo.AI.SequenceNode();
							node10.AddChild(node58);
							node58.AddChild(new Mogo.AI.CmpRate(Mogo.AI.CmpType.lt,80));
							node58.AddChild(new Mogo.AI.ChooseCastPoint(1));
							node58.AddChild(new Mogo.AI.MoveTo(1f));
						}
						node10.AddChild(new Mogo.AI.EnterRest(500));
					}
				}
			}
		}
	}
}
