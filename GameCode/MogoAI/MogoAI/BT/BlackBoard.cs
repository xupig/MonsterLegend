﻿/*----------------------------------------------------------------
// Copyright (C) 2013 广州，爱游
//
// 模块名：BehaviorTree
// 创建者：Hooke Hu
// 修改者列表：
// 创建日期：
// 模块描述：行为树框架,具体实现要继承自DecoratorNode, ConditionNode, ActionNode, ImpulseNode
//----------------------------------------------------------------*/
using System.Collections.Generic;
using UnityEngine;

namespace Mogo.AI
{

    public enum AIState
    {
        THINK_STATE = 0,
        REST_STATE,
        PATROL_STATE,
        ESCAPE_STATE,
        FIGHT_STATE,
        CD_STATE,
        PATROL_CD_STATE
    }

    public enum AIEvent
    {
        MoveEnd = 1,     
        Born,        
        AvatarDie,
        CDEnd,       
        RestEnd,        
        BeHit,       
        AvatarPosSync,
        Self,
        StiffEnd,
    }

    /// <summary>
    /// 行为树状态黑板，用于记录行为树的一些状态
    /// </summary>
	public class BTBlackBoard
	{
        public AIState aiState = AIState.THINK_STATE;
        public AIEvent aiEvent = AIEvent.MoveEnd;

        public Vector3 bornPosition = Vector3.zero;

        public uint enemyId = 0;
        public uint timeoutId = 0;

        public Vector3 movePoint = Vector3.zero;
        public int lastCastIndex = -1;
        public uint skillActTime = 0;
        public uint navTargetDistance = 0;

        public int[] skillUseCount = new int[15];
        public int skillReversal = 0;   //技能反向释放
        public float speedFactor = 1.0f;
        public Vector3 lastCastCoord = new Vector3();//上次使用技能时候的坐标
        
        //lookon
        public bool inLookOn = false;
        public int turnAngleDir = -1;
        public int LookOn_LastMode = -1;
        public int LookOn_Mode5Skill = 0;
        public float LookOn_DistanceMax = 0.0f;
        public float LookOn_DistanceMin = 0.0f;
        public int[] LookOn_ModePercent = new int[6];
        public float[] LookOn_ModeInterval = new float[5];
        public System.UInt64 LookOn_Tick = 0;
        public uint LookOn_ActTime = 0;
        public int LookOn_Mode
        {
            get { return turnAngleDir; }
            set
            {
                turnAngleDir = value;
                if (value >= 0)
                    LookOn_LastMode = value;
            }
        }

        public bool isFirstThinking = true;

        public int targetMonsterID = 0;
 
        //see
        public Dictionary<uint, int> mHatred = new Dictionary<uint, int>();
        public uint patrolActTime = 0;

        public bool inFightingState = false;
        public bool inChasingState = false;

        //外部控制AI可思考的计数器，只当计数器变回0时才是可思考
        private uint _thinkableCounter = 0;
        public bool thinkable
        {
            get
            {
                return _thinkableCounter == 0;
            }
        }

        public bool AddThinkableCount()
        {
            _thinkableCounter++;
            return true;
        }

        public bool SubtractThinkableCount()
        {
            if (_thinkableCounter == 0)
            {
                return false;
            }
            _thinkableCounter--;
            return true;
        }

        public float cdTimeStamp = 0;

        public Dictionary<int, int> flagDict = new Dictionary<int, int>();

        public int GetFlagTime(int id)
        {
            return (flagDict.ContainsKey(id) ? flagDict[id] : -1);
        }

        public void SetFlagTime(int id)
        {
            int time = (int)(Time.realtimeSinceStartup * 1000);
            if (flagDict.ContainsKey(id))
            {
                flagDict[id] = time;
            }
            else
            {
                flagDict.Add(id, time);
            }
        }

        public bool DeleteFlagTime(int id)
        {
            if (flagDict.ContainsKey(id))
            {
                flagDict.Remove(id);
                return true;
            }
            else
            {
                return false;
            }
        }

        public BTBlackBoard()
        {
            for (int i = 0; i < 15; i++)
            {
                skillUseCount[i] = 0;
            }
        }

        public void ChangeState(AIState newState)
        {
            aiState = newState;
        }

        public void ChangeEvent(AIEvent newEvent)
        {
            aiEvent = newEvent;
        }

        public void EditHatred(uint key, int value)
        {
            if (mHatred.ContainsKey(key))
            {
                mHatred[key] = value;
            }
            else
            {
                mHatred.Add(key, value);
            }
        }

        public bool HasHatred(uint key)
        {
            return mHatred.ContainsKey(key);
        }

        public int HatredCount()
        {
            return mHatred.Count;
        }
	}
}
