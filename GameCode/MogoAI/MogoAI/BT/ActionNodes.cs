﻿/*----------------------------------------------------------------
// Copyright (C) 2013 广州，爱游
//
// 模块名：ActionNodes
// 创建者：Hooke Hu
// 修改者列表：
// 创建日期：
// 模块描述：继承自ActionNode各种行为节点
//----------------------------------------------------------------*/


namespace Mogo.AI
{
    //---------------------------01.按怪物ID找目标---------------------------------
    public class FindTargetByMonsterID : ActionNode
    {
        protected int m_monsterId = 0;
        protected int m_radius = 0;

        public FindTargetByMonsterID(int monsterId, int radius)
        {
            m_monsterId = monsterId;
            m_radius = radius;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            return theOwner.FindTargetByMonsterID(m_monsterId, m_radius);
        }
    }

    //---------------------------02.按技能搜索范围找目标----------------------------
    public class FindTargetBySkillRange : ActionNode
    {
        protected int m_skillIndex = 0;

        public FindTargetBySkillRange(int skillIndex)
        {
            m_skillIndex = skillIndex;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            return theOwner.FindTargetBySkillRange(m_skillIndex);
        }
    }

    //---------------------------03.查找对手---------------------------------------
    public class AOI : ActionNode
    {
        
        protected int _searchChance = 0;    //重新搜索概率
        protected int _targetType = 1; //目标类型 0: 任意  1: 敌方  2: 我方
        protected int _targetRangeType = 0;        //搜索类型 0: 圆形 1: 扇形
        protected string _targetRangeArgs = "0";    //搜索参数
        protected int _targetPriority = 0;    //目标优先级 0: 优先玩家 1: 优先非玩家

        public AOI(int searchChance, int targetType = 1, int targetRangeType = 0, string targetRangeArgs = "0", int targetPriority = 0)
        {
            _searchChance = searchChance;
            _targetType = targetType;
            _targetRangeType = targetRangeType;
            _targetRangeArgs = targetRangeArgs;
            _targetPriority = targetPriority;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //查找目标，记录到blackBoard
            //Mogo.Util.LoggerHelper.Debug("AI:" + "AOI");
            bool rnt = theOwner.ProcAOI(_searchChance, _targetType, _targetRangeType, _targetRangeArgs, _targetPriority);
            return rnt;
        }
    }

    //---------------------------04.查找离跟随者最近的对手----------------------------
    public class PetAOI : ActionNode
    {
        protected int m_iSearchChance = 0;
        protected int m_iTargetType = 1; //玩家是1  其他怪物ID
        protected int m_iDis = 0;        //最大搜索距离
        protected int m_iCenterX = 0;    //圆心坐标Xcm
        protected int m_iCenterZ = 0;    //圆心坐标Zcm

        public PetAOI(int _searchChance, int _targetType = 1, int _dis = 0, int _centerX = 0, int _centerZ = 0, int iMonsterTypePriority = 0)
        {
            m_iSearchChance = _searchChance;
            m_iTargetType = _targetType;
            m_iDis = _dis;
            m_iCenterX = _centerX;
            m_iCenterZ = _centerZ;

        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //查找目标，记录到blackBoard
            //Mogo.Util.LoggerHelper.Debug("AI:" + "AOI");
            bool rnt = theOwner.ProcPetAOI(m_iSearchChance, m_iTargetType, m_iDis, m_iCenterX, m_iCenterZ);
            return rnt;
        }
    }

    //---------------------------05.宠物跟随主人-----------------------------------
    public class ChaseMaster : ActionNode
    {
        protected int m_distance = 0;
        protected bool m_isForce = false;
        public ChaseMaster(int distance, int isForce)
        {
            m_distance = distance;
            m_isForce = isForce == 1;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            var result = theOwner.ProcChaseMaster(m_distance, m_isForce);
            return result;
        }
    }

    //---------------------------06.给自己增加BUFF---------------------------------
    public class AddBuff : ActionNode
    {
        protected int _buffId = 0;
        protected int _sec = 0;

        public AddBuff(int buffId, int sec)
        {
            _buffId = buffId;
            _sec = sec;
        }

        public override bool Proc(Game.IAIProc theOwner)
        {
            theOwner.ProcAddBuff(_buffId, _sec);//
            return true;
        }
    }

    //---------------------------07.怪物返回出生点----------------------------------
    public class MonsterBackBornPoint : ActionNode
    {
        protected bool m_isForce = false;
        protected int m_distance = 0;

        public MonsterBackBornPoint(int isForce, int distance)
        {
            m_distance = distance;
            m_isForce = isForce == 1;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            var result = theOwner.ProcMonsterBackBornPoint(m_isForce, m_distance);
            return result;
        }
    }

    //---------------------------08.怪物说话----------------------------------------
    public class MonsterSpeech : ActionNode
    {
        protected int _id = 0;
        protected int _speechType = 1;

        public MonsterSpeech(int id, int speechType = 1)
        {
            _id = id;
            _speechType = speechType;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            theOwner.ProcMonsterSpeech(_id, _speechType);
            return true;
        }
    }

    //---------------------------09.观望--------------------------------------------
    public class LookOn : ActionNode
    {
        protected int m_Mode5Skill = 0;
        protected float m_DistanceMax = 0.0f;
        protected float m_DistanceMin = 0.0f;
        protected int[] m_ModePercent = new int[6];
        protected int[] m_ModeInterval = new int[5];

        public LookOn(int _DistanceMin, int _DistanceMax,
             int _Mode0Percent, int _Mode1Percent,
            int _Mode2Percent, int _Mode3Percent,
            int _Mode4Percent, int _Mode5Percent,
            int _Mode0Interval, int _Mode1Interval,
            int _Mode2Interval, int _Mode3Interval,
            int _Mode4Interval, int _Mode5Skill
            )
        {
            m_Mode5Skill = _Mode5Skill;
            m_DistanceMax = _DistanceMax;
            m_DistanceMin = _DistanceMin;

            m_ModePercent[0] = _Mode0Percent;
            m_ModePercent[1] = _Mode1Percent;
            m_ModePercent[2] = _Mode2Percent;
            m_ModePercent[3] = _Mode3Percent;
            m_ModePercent[4] = _Mode4Percent;
            m_ModePercent[5] = _Mode5Percent;

            m_ModeInterval[0] = _Mode0Interval;
            m_ModeInterval[1] = _Mode1Interval;
            m_ModeInterval[2] = _Mode2Interval;
            m_ModeInterval[3] = _Mode3Interval;
            m_ModeInterval[4] = _Mode4Interval;

            m_Mode5Skill = _Mode5Skill;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //Mogo.Util.LoggerHelper.Debug("AI:" + "LookOn");

            theOwner.GetBlackBoard().LookOn_DistanceMax = m_DistanceMax * 0.01f;
            theOwner.GetBlackBoard().LookOn_DistanceMin = m_DistanceMin * 0.01f;

            for (int i = 0; i < m_ModePercent.Length; i++)
            {
                theOwner.GetBlackBoard().LookOn_ModePercent[i] = m_ModePercent[i];
            }

            for (int i = 0; i < m_ModeInterval.Length; i++)
            {
                theOwner.GetBlackBoard().LookOn_ModeInterval[i] = m_ModeInterval[i] * 0.001f;
            }

            theOwner.GetBlackBoard().LookOn_Mode5Skill = m_Mode5Skill;

            theOwner.ProcLookOn();

            return true;
        }
    }

    //---------------------------10.进入冷却状态-------------------------------------
    public class EnterCD : ActionNode
    {
        protected int m_iSec = 0;

        public EnterCD(int _sec)
        {
            m_iSec = _sec;
        }
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //Mogo.Util.LoggerHelper.Debug("AI:" + "EnterCD:" + m_iSec);
            theOwner.ProcEnterCD(m_iSec);//kevintestcd
            return true;
        }
    }

    //---------------------------11.进入休息状态-------------------------------------
    public class EnterRest : ActionNode
    {
        protected uint m_iSec = 0;

        public EnterRest(uint _sec)
        {
            m_iSec = _sec;
        }
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //Mogo.Util.LoggerHelper.Debug("AI:" + "EnterRest:" + m_iSec);
            theOwner.ProcEnterRest(m_iSec);
            return true;
        }
    }

    //---------------------------12.进入战斗状态-------------------------------------
    public class EnterFight : ActionNode
    {
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //Mogo.Util.LoggerHelper.Debug("AI:" + "EnterFight");
            theOwner.ProcEnterFight();
            return true;
        }
    }

    //---------------------------13.使用技能-----------------------------------------
    public class CastSpell : ActionNode
    {
        protected int m_iSkillId = 0;
        protected int m_iReversal = 0;

        public CastSpell(int _skillId, int _reversal)
        {
            m_iSkillId = _skillId;
            m_iReversal = _reversal;
        }
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //Mogo.Util.LoggerHelper.Debug("AI:" + "CastSpell:" + m_iSkillId);
            return theOwner.ProcCastSpell(m_iSkillId, m_iReversal);
        }
    }

    //---------------------------14.选择施法目标点------------------------------------
    public class ChooseCastPoint : ActionNode
    {
        protected int m_iSkillId = 0;

        public ChooseCastPoint(int _skillId)
        {
            m_iSkillId = _skillId;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {

            bool rnt = theOwner.ProcChooseCastPoint(m_iSkillId);

            if (rnt == true)
            {
                //Mogo.Util.LoggerHelper.Debug("AI:" + "ChooseCastPoint:true");
            }
            else
            {
                //Mogo.Util.LoggerHelper.Debug("AI:" + "ChooseCastPoint:false");
            }
            return rnt;
        }
    }

    //---------------------------15.巡逻---------------------------------------------
    public class Patrol : ActionNode
    {
        protected int m_iCDTimeMin = 0;
        protected int m_iCDTimeMax = 0;
        protected int m_iSkillIndex = 0;

        public Patrol(int _CDTimeMin, int _CDTimeMax, int _SkillIndex)
        {
            m_iCDTimeMin = _CDTimeMin;
            m_iCDTimeMax = _CDTimeMax;
            m_iSkillIndex = _SkillIndex;
        }
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            bool rnt = theOwner.ProcPatrol(m_iCDTimeMin, m_iCDTimeMax, m_iSkillIndex);
            if (rnt)
            {
                //Mogo.Util.LoggerHelper.Debug("AI:" + "PatrolAction" + "true");
            }
            else
            {
                //Mogo.Util.LoggerHelper.Debug("AI:" + "PatrolAction" + "false");
            }
            return rnt;
        }
    }

    //---------------------------16.移动---------------------------------------------
    public class MoveTo : ActionNode
    {
        protected float m_iSec = 0;

        public MoveTo(float _sec)
        {
            m_iSec = _sec;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //Mogo.Util.LoggerHelper.Debug("AI:" + "MoveTo");
            theOwner.ProcMoveTo(m_iSec);
            return true;
        }
    }

    //---------------------------17.指定目标点----------------------------------------
    public class MoveToAppoint : ActionNode
    {
        protected string _pos = "";

        public MoveToAppoint(string _sec)
        {
            _pos = _sec;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            theOwner.ProcMoveToAppoint(_pos);
            return true;
        }
    }

    //---------------------------18.目标节点移动--------------------------------------
    public class MoveToTargetPoint : ActionNode
    {
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //Mogo.Util.LoggerHelper.Debug("AI:" + "MoveTo");
            theOwner.ProcMoveToTargetPoint();
            return true;
        }
    }

    //---------------------------19.取消战斗状态--------------------------------------
    public class CancelFightState : ActionNode
    {
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //Mogo.Util.LoggerHelper.Debug("AI:" + "EnterFight");
            theOwner.ProcCancelFightState();
            return true;
        }
    }

    //---------------------------20.选择托管移动路点-----------------------------------
    public class SelectAutoFightMovePoint : ActionNode
    {
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //Mogo.Util.LoggerHelper.Debug("AI:" + "SelectAutoFightMovePoint");
            theOwner.ProcSelectAutoFightMovePoint();
            return true;
        }
    }

    //---------------------------21.传送到队长场景与位置-----------------------------------
    public class TeleportToLeader : ActionNode
    {
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            theOwner.ProcTeleportToLeader();
            return true;
        }
    }

    //---------------------------22.队友跟随队长---------------------------------------
    public class FollowLeader : ActionNode
    {
        private int _distance2 = 0;
        private int _distance3 = 0;
        private int _distance4 = 0;
        private bool _isForce = false;

        public FollowLeader(int distance2, int distance3, int distance4, int isForce)
        {
            _distance2 = distance2;
            _distance3 = distance3;
            _distance4 = distance4;
            _isForce = (isForce == 1);
        }
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            return theOwner.ProcFollowLeader(_distance2, _distance3, _distance4, _isForce);
        }
    }

    //---------------------------23.取消跟随-----------------------------------
    public class QuitFollowing : ActionNode
    {
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            theOwner.ProcQuitFollowing();
            return true;
        }
    }

    //---------------------------24.取消攻击目标-----------------------------------
    public class CancelAttackTarget : ActionNode
    {
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            theOwner.ProcCancelAttackTarget();
            return true;
        }
    }

    //---------------------------25.设置点击目标为寻路点----------------------------
    public class SetTouchMovePoint : ActionNode
    {
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            theOwner.ProcSetTouchMovePoint();
            return true;
        }
    }

    //---------------------------26.移除点击目标-----------------------------------
    public class DeleteTouchMovePoint : ActionNode
    {
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            theOwner.ProcDeleteTouchMovePoint();
            return true;
        }
    }

    //---------------------------27.立标志---------------------------------------
    public class SetFlag : ActionNode
    {
        private int _id = 0;

        public SetFlag(int id)
        {
            _id = id;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            theOwner.ProcSetFlag(_id);
            return true;
        }
    }

    //---------------------------28.删除标志---------------------------------------
    public class DeleteFlag : ActionNode
    {
        private int _id = 0;

        public DeleteFlag(int id)
        {
            _id = id;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            return theOwner.ProcDeleteFlag(_id);
        }
    }
}
  