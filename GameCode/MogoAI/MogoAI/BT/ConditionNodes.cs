﻿/*----------------------------------------------------------------
// Copyright (C) 2013 广州，爱游
//
// 模块名：ConditionNodes
// 创建者：Hooke Hu
// 修改者列表：
// 创建日期：
// 模块描述：继承自ConditionNode各种条件节点
//----------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Mogo.AI
{
    public enum CmpType { lt, le, eq, ge, gt };

    internal class CmpTypeMethod
    {
        public static bool Cmp(CmpType cmp, int lv, int rv)
        {
            switch (cmp)
            {
                case CmpType.lt:
                    {
                        return lv < rv;
                    }
                case CmpType.le:
                    {
                        return lv <= rv;
                    }
                case CmpType.eq:
                    {
                        return lv == rv;
                    }
                case CmpType.ge:
                    {
                        return lv >= rv;
                    }
                case CmpType.gt:
                    {
                        return lv > rv;
                    }
            }
            return false;
        }
    }

    //-----------------------01.比较概率--------------------------
    public class CmpRate : ConditionNode
    {
        protected CmpType _cmp;
        protected int _rate;

        public CmpRate(CmpType cmp, int rate)
        {
            _cmp = cmp;
            _rate = rate;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //todo计算概率
            int rate = theOwner.Random(0, 100);//Mogo.Util.RandomHelper.GetRandomInt(0, 100);
            return CmpTypeMethod.Cmp(_cmp, rate, _rate);
        }
    }

    // ----------------------02.部位是否被破坏---------------------
    public class BossPartHasBeenBroken : ConditionNode
    {
        protected int _partId;
        public BossPartHasBeenBroken(int partId)
        {
            _partId = partId;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            bool rnt = theOwner.ProcBossPartHasBeenBroken(_partId);
            return rnt;
        }
    }

    // ----------------------03.部位是否可修复---------------------
    public class BossPartCanBeRecovered : ConditionNode
    {
        protected int _partId;
        public BossPartCanBeRecovered(int partId)
        {
            _partId = partId;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            bool rnt = theOwner.ProcBossPartCanBeRecovered(_partId);
            return rnt;
        }
    }

    //-----------------------04.测试身上的buff---------------------
    public class TestBuff : ConditionNode
    {
        protected int _overlayCount;
        protected int _buffId;

        public TestBuff(int overlayCount, int buffId)
        {
            _buffId = buffId;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            bool rnt = theOwner.ProTestBuff(_buffId);

            return rnt;
        }
    }

    // ----------------------05.判断与跟随者距离------------------
    public class CmpMasterDistance : ConditionNode
    {
        protected CmpType _cmp;
        protected int _distance;

        public CmpMasterDistance(CmpType cmp, int distance)
        {
            _cmp = cmp;
            _distance = distance;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            float curDistance = theOwner.GetMasterDistance();
            return CmpTypeMethod.Cmp(_cmp, (int)(curDistance * 100), _distance);
        }
    }

    // ----------------------06.判断是否跟随状态----------------------
    public class IsInChase : ConditionNode
    {
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            bool rnt = theOwner.ProcIsInChase();
            return rnt;
        }
    }

    //-----------------------07.技能冷却完毕------------------------
    public class InSkillCoolDown : ConditionNode
    {
        protected int m_iSkillId = 0;

        public InSkillCoolDown(int skillId)
        {
            m_iSkillId = skillId;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            bool rnt = theOwner.ProcInSkillCoolDown(m_iSkillId);
            if (rnt)
            {
                //Mogo.Util.LoggerHelper.Debug("AI:" + "InSkillCoolDown:" + m_iSkillId + " true");
            }
            else
            {
                //Mogo.Util.LoggerHelper.Debug("AI:" + "InSkillCoolDown:" + m_iSkillId + " false");
            }

            return rnt;
        }
    }

    //-----------------------08.检查敌人数量------------------------
    public class CmpEnemyNum : ConditionNode
    {
        protected CmpType _cmp;
        protected int _num;

        public CmpEnemyNum(CmpType cmp, int num)
        {
            _cmp = cmp;
            _num = num;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //todo检查敌人数量
            int num = theOwner.GetEnemyNum();
            bool rnt = CmpTypeMethod.Cmp(_cmp, num, _num);
            if (rnt == true)
            {
                //Mogo.Util.LoggerHelper.Debug("AI:" + "CmpEnemyNum:" + "true");
            }
            else
            {
                //Mogo.Util.LoggerHelper.Debug("AI:" + "CmpEnemyNum:" + "false");
            }
            return rnt;
        }
    }

    //-----------------------09.检查与出生点距离(服)-----------------
    public class CmpBornPointDistance : ConditionNode
    {
        protected CmpType _cmp;
        protected int _distance;

        public CmpBornPointDistance(CmpType cmp, int distance)
        {
            _cmp = cmp;
            _distance = distance;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //Vector3 postion = new Vector3(float.Parse(_pos.Split(',')[0]),float.Parse(_pos.Split(',')[1]),float.Parse(_pos.Split(',')[2]));
            float testDis = Vector3.Distance(theOwner.GetBlackBoard().movePoint, theOwner.GetTransform().position);
            bool rnt = CmpTypeMethod.Cmp(_cmp, (int)(testDis * 100), _distance);
            if (rnt)
            {

            }
            else
            {
                //Mogo.Util.LoggerHelper.Debug("AI:" + "CmpTargetDistance:" + "false");
            }

            return rnt;
        }
    }

    //-----------------------10.检查与目标点距离-----------------------
    public class CmpTargetPointDistance : ConditionNode
    {
        protected CmpType _cmp;
        protected int _distance;

        public CmpTargetPointDistance(CmpType cmp, int distance)
        {
            _cmp = cmp;
            _distance = distance;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //Vector3 postion = new Vector3(float.Parse(_pos.Split(',')[0]),float.Parse(_pos.Split(',')[1]),float.Parse(_pos.Split(',')[2]));
            float testDis = Vector3.Distance(theOwner.GetBlackBoard().movePoint, theOwner.GetTransform().position);
            bool rnt = CmpTypeMethod.Cmp(_cmp, (int)(testDis * 100), _distance);
            if (rnt)
            {

            }
            else
            {
                //Mogo.Util.LoggerHelper.Debug("AI:" + "CmpTargetDistance:" + "false");
            }

            return rnt;
            //return false;
        }
    }

    //-----------------------11.检查与目标距离-----------------------
    public class CmpTargetDistance : ConditionNode
    {
        protected CmpType _cmp;
        protected int _distance;

        public CmpTargetDistance(CmpType cmp, int distance)
        {
            _cmp = cmp;
            _distance = distance;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            Transform enemyTransform = theOwner.GetEnemyTransform();
            if (enemyTransform == null)
            {
                return false;
            }
            float testDis = Vector3.Distance(enemyTransform.position, theOwner.GetTransform().position);
            bool rnt = CmpTypeMethod.Cmp(_cmp, (int)(testDis * 100), _distance);
            return rnt;
        }
    }

    //-----------------------12.检查自身血量-------------------------
    public class CmpSelfHP : ConditionNode
    {
        protected CmpType _cmp;
        protected int _percent;

        public CmpSelfHP(CmpType cmp, int percent)
        {
            _cmp = cmp;
            _percent = percent;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //todo检查自身血量
            int percent = (int)(theOwner.GetHpPercent() * 100);
            return CmpTypeMethod.Cmp(_cmp, percent, _percent);
        }
    }

    //-----------------------13.目标能被攻击-------------------------
    public class IsTargetCanBeAttack : ConditionNode
    {
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //todo检查敌人数量
            bool rnt = theOwner.ProcIsTargetCanBeAttack();
            if (rnt == true)
            {
                //Mogo.Util.LoggerHelper.Debug("AI:" + "IsTargetCanBeAttack:" + "true");
            }
            else
            {
                //Mogo.Util.LoggerHelper.Debug("AI:" + "IsTargetCanBeAttack:" + "false");
            }
            return rnt;
        }
    }

    // ----------------------14.是否是第一次思考----------------------
    public class IsFirstThinking : ConditionNode
    {
        public IsFirstThinking()
        {
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            return theOwner.ProcIsFirstThinking();
        }
    }

    // ----------------------15.是否是死亡状态------------------------
    public class IsInDeathState : ConditionNode
    {
        public IsInDeathState()
        {
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            return theOwner.ProcIsInDeathState();
        }
    }

    // ----------------------16.是强制返回状态------------------------
    public class IsForceBackState : ConditionNode
    {
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            bool rnt = theOwner.ProcIsForceBackState();
            return rnt;
        }
    }

    // ----------------------17.是战斗状态----------------------------
    public class IsFightState : ConditionNode
    {
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            bool rnt = theOwner.ProIsFightState();
            return rnt;
        }
    }

    // ----------------------18.右边距离范围内是否有友方实体------------
    public class HasFriendInRightRange : ConditionNode
    {
        protected int _distance;
        public HasFriendInRightRange(int distance)
        {
            _distance = distance;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            return theOwner.ProcHasFriendInRightRange(_distance);
        }
    }

    // ----------------------19.左边距离范围内是否有友方实体------------
    public class HasFriendInLeftRange : ConditionNode
    {
        protected int _distance;
        public HasFriendInLeftRange(int distance)
        {
            _distance = distance;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            return theOwner.ProcHasFriendInLeftRange(_distance);
        }
    }

    //-----------------------20.在技能施法范围内-----------------------
    public class InSkillRange : ConditionNode
    {
        protected int m_iSkillId = 0;

        public InSkillRange(int skillId)
        {
            m_iSkillId = skillId;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            bool rnt = theOwner.ProcInSkillRange(m_iSkillId);
            if (rnt)
            {
                //Mogo.Util.LoggerHelper.Debug("AI:" + "InSkillRange:" + m_iSkillId  + " true");
            }
            else
            {
                //Mogo.Util.LoggerHelper.Debug("AI:" + "InSkillRange:" + m_iSkillId + " false");
            }

            return rnt;
        }
    }

    //-----------------------21.是冷却状态----------------------------
    public class IsCD : ConditionNode
    {
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            if (theOwner.GetBlackBoard().aiState == AIState.CD_STATE)
            {
                //Mogo.Util.LoggerHelper.Debug("AI:" + "ISCD:" + "true");
            }
            else
            {
                //Mogo.Util.LoggerHelper.Debug("AI:" + "ISCD:" + "false");
            }
            return theOwner.GetBlackBoard().aiState == AIState.CD_STATE;
        }
    }

    //-----------------------22.判断队长与自己是否同场景（同ID且相同分线）
    public class CmpLeaderInTheSameScene : ConditionNode
    {
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            return theOwner.ProcCmpLeaderInTheSameScene();
        }
    }

    //-----------------------23.判断目标是否在所填关卡类型的副本中-------
    public class CmpTargetMapType : ConditionNode
    {
        private string _id = "";
        public CmpTargetMapType(string id)
        {
            _id = id;
        }
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            return theOwner.ProcCmpTargetMapType(_id);
        }
    }

    //-----------------------24.判断队长是否在所填关卡类型的副本中-------
    public class CmpLeaderMapType : ConditionNode
    {
        private string _id = "";
        public CmpLeaderMapType(string id)
        {
            _id = id;
        }
        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            return theOwner.ProcCmpLeaderMapType(_id);
        }
    }

    //-----------------------25.判断队长是否有buff---------------------
    public class CmpLeaderContainsBuff : ConditionNode
    {
        protected int _id;
        public CmpLeaderContainsBuff(int id)
        {
            _id = id;
        }

        public override bool Proc(Game.IAIProc theOwner)
        {
            return theOwner.ProcLeaderContainsBuff(_id);
        }
    }

    //-----------------------26.判断是否有点击目标---------------------
    public class HasTouchMovePoint : ConditionNode
    {
        public HasTouchMovePoint()
        {
        }

        public override bool Proc(Game.IAIProc theOwner)
        {
            return theOwner.ProcHasTouchMovePoint();
        }
    }

    //-----------------------27.判断玩家属性值-------------------------
    public class CmpAttribute : ConditionNode
    {
        protected CmpType _cmp;
        protected int _attriID;
        protected int _attriValue;

        public CmpAttribute(CmpType cmp, int attriID, int attriValue)
        {
            _cmp = cmp;
            _attriID = attriID;
            _attriValue = attriValue;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            //todo检查自身血量
            int attriValue = (int)(theOwner.GetAttriValue(_attriID));
            return CmpTypeMethod.Cmp(_cmp, attriValue, _attriValue);
        }
    }

    //-----------------------28.判断标志时间-------------------------
    public class CmpFlagTime : ConditionNode
    {
        protected CmpType _cmp;
        protected int _id;
        protected int _time;

        public CmpFlagTime(CmpType cmp, int id, int time)
        {
            _cmp = cmp;
            _id = id;
            _time = time;
        }

        public override bool Proc(Mogo.Game.IAIProc theOwner)
        {
            int timeStamp = theOwner.ProcGetFlagTimeStamp(_id);
            if (timeStamp == -1)
            {
                return false;
            }
            int now = (int)(Time.realtimeSinceStartup * 1000);
            int time = now - timeStamp;
            return CmpTypeMethod.Cmp(_cmp, time, _time);
        }
    }
}
