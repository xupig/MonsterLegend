﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using GameLoader.Config;
using GameLoader.IO;
using GameLoader.Utils;
using GameLoader.Utils.XML;
using UnityEngine;
using LitJson;
using GameLoader.Version;
using GameLoader.PlatformSdk;
using GameLoader.Defines;
using GameLoader.LoadingBar;

namespace GameLoader.Mgrs
{
    /// <summary>
    /// Apk更新管理类
    ///    1、自更新
    ///       1-1、导出StreamingAssets资源
    ///       1-2、下载apk包
    ///       1-3、自动提示安装apk包
    ///       1-4、结束
    ///    2、平台更新
    ///       同自更新
    /// </summary>
    class UpdateApkMgr
    {
        private const int EXTERNAL_MASK = 0x0080;
        private int step = 0;
        //private int curTryNum;
        //private int maxTryNum = 3;
        private int beginTime = 0;
        private string curExpAssetPath;
        private string localApkPath = "";
        private string outputResourceFolder = null;          //资源存放--外部目录
        private string outputRuntimeResourceFolder = null;   //资源存放--外部目录 
        private List<string> exportFileList;                 //需要导出文件数量
        private Action exportCallback;                       //资源从SD卡导出结束回调
        private VersionInfo localVersion;                    //本地版本信息
        private BreakPointDownload breakPointDownload;
        private Action<Action, long> checkNetwork;

        public UpdateApkMgr(VersionInfo localVersion, Action<Action, long> checkNetwork)
        {
            this.localVersion = localVersion;
            this.checkNetwork = checkNetwork;
            exportFileList = new List<string>();
            breakPointDownload = new BreakPointDownload();
        }

        /// <summary>
        /// 返回本地存储apk文件目录
        /// </summary>
        public string LocalApkFolder
        {
            get
            {
                GetOutputRuntimeResourceFolder();
                return outputRuntimeResourceFolder;
            }
        }

        public void Update()
        {
            GetOutputRuntimeResourceFolder();
            if (SystemConfig.IsOpenUrl)
            {//跳转更新
                ConfirmBox.Instance.Show(DefaultLanguage.GetString("jump_update_game"), (result) =>
                {
                    if (result)
                    {
                        //TODO 跳转更新
                        LoggerHelper.Info(String.Format("跳转更新: apkUrl:{0}", SystemConfig.ApkUrl));
                        Application.OpenURL(SystemConfig.ApkUrl);
                    }
                    Application.Quit();
                }, DefaultLanguage.GetString("update_now"), DefaultLanguage.GetString("exit"));
                return;
            }

            if (SystemConfig.IsUsePlatformUpdate)
            { //平台更新
                if (SystemConfig.IsUpdateBigApk)
                {//使用整包apk更新
                    SetPlatformUpdateCallback();
                    OnPlatformUpdate();
                }
                else
                {//使用补丁apk更新
                    SetPlatformUpdateCallback();
                    ExportFromStreamingAssets(OnPlatformUpdate);
                }
            }
            else
            { //自更新
                //curTryNum = 0;
                string localApkFolder = LocalApkFolder;
                localApkPath = string.Concat(localApkFolder, Path.GetFileName(SystemConfig.ApkUrl));
                LoggerHelper.Info(string.Format("localApkFolder:{0},localApkPath:{1}", localApkFolder, localApkPath));
                if (SystemConfig.IsUpdateBigApk)
                {//使用整包apk更新
                    if (UnityPropUtils.Platform != RuntimePlatform.IPhonePlayer)
                    {
                        checkNetwork(BreakPointDownloadApk, SystemConfig.ApkSize);
                    }
                    else
                    {
                        LoggerHelper.Info("[自更新] IOS大包更新！");
                    }
                }
                else
                {
                    ExportFromStreamingAssets(OnExportFinished);
                }
            }
        }

        /// <summary>
        /// 获取资源外部存放目录,已'/'结尾
        /// </summary>
        public string GetOutputRuntimeResourceFolder()
        {
            if (string.IsNullOrEmpty(outputRuntimeResourceFolder))
            {
                outputRuntimeResourceFolder = UnityPropUtils.IsEditor ? string.Concat(Application.persistentDataPath, ConstString.RutimeResource, "/") : SystemConfig.RuntimeResourcePath;
                outputResourceFolder = string.Concat(outputRuntimeResourceFolder, "Resources/");
                LoggerHelper.Info("[UpdateApkMgr] outputResourceFolder:" + outputResourceFolder);
            }
            return outputRuntimeResourceFolder;
        }

        /// <summary>
        /// 获取StreamingAssets目录下_resources.mk文件的路径
        /// </summary>
        /// <returns></returns>
        public string GetStreamingAssetsmkFilePath()
        {
            return IOUtils.GetStreamPath("Resources/_resources.mk");
        }

        //自更新导出完成
        private void OnExportFinished()
        {
            if (UnityPropUtils.Platform != RuntimePlatform.IPhonePlayer)
            {
                //队列进行：先导出StreamingAssets再加载apk
                LoggerHelper.Info("[自更新] StreamingAssets资源导出[Finish]！");
                //ProgressBar.Instance.UpdateTip("开始下载apk文件");
                checkNetwork(WWWDownloadApk, SystemConfig.ApkSize);
            }
            else
            {
                LoggerHelper.Info("[自更新] IOS小包更新 StreamingAssets资源导出[Finish]！");
            }
        }

        private void BreakPointDownloadApk()
        {
            DownloadApk(true);
        }

        private void WWWDownloadApk()
        {
            DownloadApk(false);
        }

        //加载Apk
        //isUseRandomUrl [true:地址加载随机参数,false:不加随机参数]
        private void DownloadApk(bool useBreakPointDownload)
        {
            //if (curTryNum++ <= maxTryNum)
            //{
            beginTime = Environment.TickCount;
            string apkHttpUrl = SystemConfig.ApkUrl;
            if (SystemConfig.IsDownloadFromSource)
                apkHttpUrl = TextDownloadMgr.GetRandomParasUrl(apkHttpUrl);
            if (DownloadProgress.instance != null) DownloadProgress.instance.ShowDownload(Path.GetFileName(apkHttpUrl), 0, 1, 1, SystemConfig.ApkSize, SystemConfig.ApkSize);
            //FileDownloadMgr.Instance.AsyncDownloadFile(apkHttpUrl, localApkPath, OnApkUpdateProgress, OnApkFinished, OnApkFail);
            if (useBreakPointDownload)
            {
                breakPointDownload.Dispose();
                breakPointDownload.Download(apkHttpUrl, localApkPath, OnUpdateProgress, OnApkFinished, OnApkFail);
            }
            else
            {
                HttpWrappr.instance.LoadWwwFile(apkHttpUrl, localApkPath, OnUpdateProgress, OnApkFinished, OnApkFail);
            }
            //}
            //else
            //{//已超过最大重次次数:失败提示
            //    LoggerHelper.Error(string.Format("[自更新apk] 加载:{0} 超过重试次数:{1}/{2}[Fail]", SystemConfig.ApkUrl, curTryNum, maxTryNum));
            //    string tip = string.Format("{0}", "下载apk失败");
            //    //ProgressBar.Instance.UpdateTip(tip);
            //    ConfirmBox.Instance.Show(tip, OnExit, DefaultLanguage.GetString("exit"), null, false);
            //}
        }

        private void OnUpdateProgress(float progress, long bytesReceived, long totalBytesToReceive)
        {
            //DownloadProgress.instance.UpdateProgress(progress);
            //LoggerHelper.Error("bytesReceived: " + bytesReceived + " totalBytesToReceive: " + totalBytesToReceive);
            LoaderDriver.Invoke(DownloadProgress.instance.UpdateProgress, progress);
        }

        //Apk加载完成
        private void OnApkFinished(string url)
        {
            LoaderDriver.Invoke(() =>
            {
                LoggerHelper.Info(string.Format("[自更新apk] apk:{0}下载完成！", url));
                DownloadProgress.instance.Close();
                //breakPointDownload.Dispose();  //加载完成，需要手动释放资源
                CheckMd5();
            });
        }

        private void OnApkFail(string url, string errorInfo)
        {
            breakPointDownload.HandlerError(errorInfo, () =>
            {
                LoggerHelper.Info(string.Format("[自更新apk] Apk:{0}载失败！", url));
                DownloadProgress.instance.Close();
                BreakPointDownloadApk();
            });

        }

        private void OnExit(bool result)
        {
            Application.Quit();
        }

        private void CheckMd5()
        {
            //队列进行：先导出StreamingAssets再加载apk
            if (CheckFileMd5(localApkPath, SystemConfig.ApkMd5))
            {
                //ProgressBar.Instance.UpdateTip(string.Format("安装apk:{0}", Path.GetFileName(localApkPath)));
                InstallApk(localApkPath);
            }
            else
            { //md5校验失败,重新加载;
                if (File.Exists(localApkPath)) File.Delete(localApkPath);
                BreakPointDownloadApk();
            }
        }

        /// <summary>
        /// 导出SD卡资源
        /// </summary>
        /// <param name="callback">完成回调，只有导出全部完成才做回调，否则不做回调</param>
        private void ExportFromStreamingAssets(Action callback)
        {
            //首先从StreamingAssets加载_resources.mk
            LoggerHelper.Info(string.Format("更新apk包ProgramVersion:{0} 将导出sd卡资源", localVersion.ProgramVersion));
            //ProgressBar.Instance.UpdateTip("正在导出SD卡资源...");
            this.exportCallback = callback;
            this.beginTime = Environment.TickCount;
            //string resourcesmkStreamingAssetsPath = IOUtils.GetStreamPath("Resources/_resources.mk");
            ResLoader.LoadWwwBytes(GetStreamingAssetsmkFilePath(), OnLoadmk);
        }

        //从StreamingAssets加载_resources.mk结束
        private void OnLoadmk(string url, byte[] bytes)
        {
            //没有此文件，表示没有可导出资源
            if (bytes == null || bytes.Length < 1)
            {
                LoggerHelper.Info(string.Format("[自更新] file:{0}[Not Found],StreamingAssets没有可导出资源,SD卡资源导出完成", url));
                //ProgressBar.Instance.UpdateTip("导出SD卡资源完成...");
                if (exportCallback != null) exportCallback();
                exportCallback = null;
                return;
            }

            string mkOutputPath = string.Concat(outputResourceFolder, "_resources.mk");
            if (!File.Exists(mkOutputPath)) FileUtils.SaveBytes(mkOutputPath, bytes);
            string jsonContent = Encoding.GetEncoding("utf-8").GetString(bytes);
            AssetList _assetList = JsonMapper.ToObject<AssetList>(jsonContent);
            Dictionary<string, int> assetDict = _assetList.assetMemoryDict;
            LoggerHelper.Info(string.Format("[自更新] resources.mk文件: {0} 加载[OK],cost:{1}ms", url, Environment.TickCount - beginTime));

            if (assetDict != null && assetDict.Count > 0)
            {
                beginTime = Environment.TickCount;
                exportFileList.Clear();
                foreach (string assetPath in assetDict.Keys)
                {
                    if ((assetDict[assetPath] & EXTERNAL_MASK) > 0) continue;
                    string targetPath = string.Concat(outputResourceFolder, assetPath);
                    if (!File.Exists(targetPath)) exportFileList.Add(assetPath);
                }

                LoggerHelper.Info(string.Format("[自更新] 搜集导出资源:{0}个,cost:{1}ms", exportFileList.Count, Environment.TickCount - beginTime));
                beginTime = Environment.TickCount;
                step = -1;
                ExportNext();
            }
            else
            {
                LoggerHelper.Info("[自更新] StreamingAssets没有可导出资源");
                //ProgressBar.Instance.UpdateTip("导出SD卡资源完成...");
                if (exportCallback != null) exportCallback();
                exportCallback = null;
            }
        }

        private void ExportNext()
        {
            step++;
            if (step >= exportFileList.Count)
            {//导出完成
                LoggerHelper.Info(string.Format("[自更新] StreamingAssets已全部导出{0}/{1},cost:{2},导出目录:{3}", step, exportFileList.Count, LoggerHelper.formatTime(Environment.TickCount - beginTime), SystemConfig.ResourcesPath));
                //ProgressBar.Instance.UpdateProgress(1);
                if (exportCallback != null) exportCallback();
                exportCallback = null;
            }
            else
            {
                curExpAssetPath = exportFileList[step];
                string streamPath = IOUtils.GetStreamPath(string.Concat("Resources/", curExpAssetPath));
                string targetPath = string.Concat(outputResourceFolder, curExpAssetPath);
                if (!File.Exists(targetPath))
                {
                    //ProgressBar.Instance.UpdateTip(string.Format("导出文件:{0}", curExpAssetPath));
                    ProgressBar.Instance.UpdateTip(string.Format(DefaultLanguage.GetString("exporting"), step + 1 > exportFileList.Count ? exportFileList.Count : step + 1, exportFileList.Count));
                    bool isGameLoader = curExpAssetPath.IndexOf(LoaderDriver.Instance.GameLoaderName) != -1 ? true : false;  //导出GameLoader.bytes.u并保存它解压后的文件到本地
                    if (isGameLoader)
                    {
                        ResLoader.ExportGameLoaderBytes(streamPath, targetPath, OnLoaded);
                        LoggerHelper.Info("Export Bytes$GameLoader file!");
                    }
                    else
                    {
                        ResLoader.LoadWwwBytes(streamPath, targetPath, OnLoaded);
                    }
                }
                else
                {
                    OnLoaded(true);
                }
            }
        }

        private void OnLoaded(bool result)
        {
            ProgressBar.Instance.UpdateProgress((step + 0f) / exportFileList.Count);  //更新进度条
            ExportNext();
        }

        //对文件进行md5验证
        private bool CheckFileMd5(string filePath, string srcMd5)
        {
            string md5Compute = MD5Utils.BuildFileMd5(filePath).Trim();
            srcMd5 = srcMd5.Trim().ToLower();
            if (md5Compute != srcMd5)
            { //md5验证失败，删除原文件
                if (File.Exists(filePath)) File.Delete(filePath);
                LoggerHelper.Error(string.Format("apk文件:{0} 验证失败 srcMd5:{1},curMd5:{2}", filePath, srcMd5, md5Compute));
                //ProgressBar.Instance.UpdateTip(string.Format("{0}校验失败,{1}!={2}", Path.GetFileName(filePath), srcMd5, md5Compute));
                return false;
            }
            else
            {
                return true;
            }
        }

        public void Release()
        {
            FileDownloadMgr.Instance.CancelDownload();
            breakPointDownload.Dispose();
        }

        //=================  跟SDK对接Apk更新接口  ================//
        //设置平台更新的回调
        private void SetPlatformUpdateCallback()
        {
#if UNITY_ANDROID
            if (UnityPropUtils.Platform == RuntimePlatform.Android)
            {
                LoggerHelper.Info("[Android] Init PlatformUpateCallback");
                PlatformUpdateCallback puc = LoaderDriver.Instance.gameObject.GetComponent<PlatformUpdateCallback>();
                if (puc == null) LoaderDriver.Instance.gameObject.AddComponent<PlatformUpdateCallback>();
                var jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                var mainActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
                mainActivity.Call("setUpdateCallBack", "Driver");
            }
#endif
        }

        //请求平台--进行平台更新检查
        private void OnPlatformUpdate()
        {
#if UNITY_ANDROID
            if (UnityPropUtils.Platform == RuntimePlatform.Android)
            {
                LoggerHelper.Info("[Android] update apk");
                var jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                var mainActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
                mainActivity.Call("updateVersion");
            }
#endif
        }

        //自更新安装Apk
        private void InstallApk(string apkPath)
        {
            LoggerHelper.Info("[自更新apk] 开始安装apk: " + localApkPath);
            Application.OpenURL(localApkPath);
            Application.Quit();
        }

        //安装平台Apk
        public void OpenUrl()
        {
#if !UNITY_IPHONE && !UNITY_WEBPLAYER
            if (UnityPropUtils.Platform == RuntimePlatform.Android)
            {
                LoggerHelper.Info("[Android] 打开一个url");
                var jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                var mainActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
                mainActivity.Call("OpenUrl");
            }
#endif
        }
        //===============================================//
    }
}
