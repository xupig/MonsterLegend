using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using GameLoader.Utils;

public static class DictionaryExtensions
{
    public static bool Add<T, V>(this Dictionary<T, V> mDic, T[] keys, V[] values)
    {
        if (keys != null && values != null)
        {
            if (keys.Length != values.Length)
            {

                LoggerHelper.Error(string.Concat("error happend when Add<T,V> keys.Length != values.Length "));
                return false;
            }
        }
        else
        {

            LoggerHelper.Error(string.Concat("error happend when Add<T,V> keys or values is null", string.Concat("keys ", keys, " values ", values)));
            return false;
        }
        int nCount = keys.Length;
        for (int nIndex = 0; nIndex < nCount; nIndex++)
        {
            mDic.Add(keys[nIndex], values[nIndex]);
        }
        return true;
    }
}
public static class StringExternsions
{
    public static string UnityAssetBundleName(this string str)
    {
        return string.Concat(str, ".u");
    }
    public static string GetDirectoryName(this string fileName)
    {
        return fileName.Substring(0, fileName.LastIndexOf('/'));
    }

    public static String ReplaceFirst(this string input, string oldValue, string newValue, int startAt = 0)
    {
        int pos = input.IndexOf(oldValue, startAt);
        if (pos < 0)
        {
            return input;
        }
        return string.Concat(input.Substring(0, pos), newValue, input.Substring(pos + oldValue.Length));
    }

    public static String PackList<T>(this List<T> list, Char listSpriter = ',')
    {
        if (list.Count == 0)
            return "";
        else
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in list)
            {
                sb.AppendFormat("{0}{1}", item, listSpriter);
            }
            sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }

    }
    public static String PackArray<T>(this T[] array, Char listSpriter = ',')
    {
        var list = new List<T>();
        list.AddRange(array);
        return PackList(list, listSpriter);
    }

    public static string TrimSuffix(this string str)
    {
        str.Replace("\\", "/");
        if (-1 == str.LastIndexOf('/'))
        {
            if (-1 == str.LastIndexOf('.'))
            {
                return str;
            }
            else
            {
                return str.Substring(0, str.LastIndexOf('.'));
            }
        }
        else
        {
            if (-1 == str.LastIndexOf('.'))
            {
                return str.Substring(str.LastIndexOf('/') + 1);
            }
            else
            {
                return str.Substring(str.LastIndexOf('/') + 1, str.LastIndexOf('.'));
            }
        }
    }

    public static string FileName(this string str)
    {
        str.Replace("\\", "/");
        if (-1 == str.LastIndexOf('/'))
        {
            return str;
        }
        else
        {
            return str.Substring(str.LastIndexOf('/') + 1);
        }
    }
    //file or xmlstr ---> xml security element
    public static System.Security.SecurityElement ToXMLSecurityElement(this string str)
    {
        string xmlStr = str;
#if !UNITY_WEBPLAYER
        if (System.IO.File.Exists(str))
        {
            using (System.IO.FileStream fs = System.IO.File.OpenRead(str))
            {
                System.IO.StreamReader sr = new System.IO.StreamReader(fs);
                xmlStr = sr.ReadToEnd();
            }
        }
#endif
        if (string.IsNullOrEmpty(xmlStr))
            return null;
        GameLoader.Utils.XML.SecurityParser sp = new GameLoader.Utils.XML.SecurityParser();
        sp.LoadXml(xmlStr);
        return sp.ToXml();
    }
}