﻿using System;
using System.Collections.Generic;
using GameLoader.Utils;
using UnityEngine;

namespace GameLoader.PlatformSdk
{
    public class AndroidKoreaSdkMgr// : AndroidSdkMgr
    {
   /*     public override PlatformSdkType PlatformSdkMgrType
        {
            get
            {
                return PlatformSdkType.AndroidKorea;
            }
        }

        #if UNITY_ANDROID
        public AndroidKoreaSdkMgr()
        {
            Start();
        }

        void Start()
        {
            Instance = this;
            LoggerHelper.Info("[AndroidKoreaSDK] Init [Start]");
            m_config = new AndroidJavaClass("com.qjphs.utils.Config");
            AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            m_mainActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
            IsLoginUI = true;
            //AndroidUtils com = LoaderDriver.Instance.gameObject.AddComponent<AndroidUtils>();
            AndroidUtils com = new AndroidUtils();
            LoggerHelper.Info(string.Format("[AndroidKoreaSDK] AndroidUtils:{0} [OK]", com));
        }

        private void BeforeLoginLog()
        {
            m_mainActivity.Call("logBeforeLogin");
        }

        public override void Login()
        {
            BeforeLoginLog();
            base.Login();
        }

        public override void SetupInfo()
        {
            Log("AfterGetInfo");
            // 游戏中将要使用到的唯一id，这个id将取代 username
            string username = m_config.GetStatic<string>("username");
            string suid = m_config.GetStatic<string>("suid");
            string timeStamp = m_config.GetStatic<string>("timestamp");
            string strSign = m_config.GetStatic<string>("signStr");  //签名，由平台返回的对指定数据进行的md5字符串
            if (loginReturnValue == null)
            {
                loginReturnValue = new string[4];
            }
            loginReturnValue[0] = suid;
            loginReturnValue[1] = username;
            loginReturnValue[2] = timeStamp;
            loginReturnValue[3] = strSign;
            LoggerHelper.Info("username:" + username);
            LoggerHelper.Info("uid:" + suid);
            LoggerHelper.Info("timeStamp:" + timeStamp);
            LoggerHelper.Info("signStr:" + strSign);
            string actualName = m_config.GetStatic<string>("actualUsername");
            if (string.IsNullOrEmpty(actualName))
            {
                actualName = username;
            }
            if (sdkLoginOkHandler != null)
            {
                sdkLoginOkHandler(actualName, loginReturnValue); 
            }
            LoggerHelper.Info(string.Format("[Sdk-SetupInfo()] username:{0},showName:{1}", username, actualName));
        }

        public void PayByGooglePlay(string productId, string price, string serverId, string callbackInfo)
        {
            m_mainActivity.Call("payByGoogleWallet", productId, price, serverId, callbackInfo);
        }

        #endif
    * 
    * */
    }
}
