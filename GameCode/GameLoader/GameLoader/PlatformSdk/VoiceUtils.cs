﻿//using System;
//using UnityEngine;

//namespace GameLoader.PlatformSdk
//{
//    public class VoiceUtils
//    {
//#if UNITY_ANDROID
//        private static VoiceUtils _instance;
//        private AndroidJavaObject m_voiceTool;

//        public static VoiceUtils Instance
//        {
//            get
//            {
//                if (_instance == null)
//                {
//                    _instance = new VoiceUtils();
//                }
//                return _instance;
//            }
//        }

//        public VoiceUtils()
//        {
//            Init();
//        }

//        private void Init()
//        {
//            m_voiceTool = new AndroidJavaObject("com.zhj.utils.VoiceTool");
//            //GameLoader.Utils.LoggerHelper.Debug("[VoiceUtils] 1");
//            using (AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
//            {
//                AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
//                //GameLoader.Utils.LoggerHelper.Debug("[VoiceUtils] 2");
//                if (!m_voiceTool.Call<bool>("onCreate", jo) )
//                {
//                    //GameLoader.Utils.LoggerHelper.Debug("[VoiceUtils] 3");
//                    m_voiceTool = null;
//                }
//            }
//           // GameLoader.Utils.LoggerHelper.Debug("[VoiceUtils] 4");
//        }
//        /// <summary>
//        /// 开始录音
//        /// </summary>
//        public void StartToRecordVoice()
//        {
//            //GameLoader.Utils.LoggerHelper.Debug("[VoiceUtils] 5");
//            if (m_voiceTool != null)
//            {
//                //GameLoader.Utils.LoggerHelper.Debug("[VoiceUtils] 6");
//                m_voiceTool.Call("StartToRecordVoice");
//            }
//        }

//        /// <summary>
//        /// 停止录音
//        /// </summary>
//        public void StopRecordVoice()
//        {
//            if (m_voiceTool != null)
//            {
//                m_voiceTool.Call("stopTalk");
//            }
//        }

//        /// <summary>
//        /// 播放录音
//        /// </summary>
//        /// <param name="path"></param>
//        public void PlayMusic(string path)
//        {
//            m_voiceTool.Call("playMusic", path);
//        }

//#endif
//    }

//}
