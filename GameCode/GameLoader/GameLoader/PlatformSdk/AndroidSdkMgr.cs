// 模块名   :  AndroidSdkManager
// 创建者   :  莫卓豪
// 创建日期 :  2013-8-13
// 描    述 :  android平台sdk登陆对接

using UnityEngine;
using System;
using GameLoader.Utils;
using GameLoader.Config;
using System.Collections.Generic;
using LitJson;
namespace GameLoader.PlatformSdk
{
    public class AndroidSdkMgr : PlatformSdkMgr
    {
#if UNITY_ANDROID
        private bool hasInitVoice = false;                   // 语音组件初始化状态
        protected string[] loginReturnValue;
        protected AndroidJavaClass m_config;
        protected AndroidJavaObject m_mainActivity;
        private uint[] m_ShowEnergyTipsTime = new uint[2];   // 设置一个下次体力满的提示通知

        private Action<List<string>> loginSuccessFunction = null;
        private Action<int> loginFailFunction = null;
        private Action<string> setJsonToSDKFunction = null;
        private Action switchAccountCB = null;
        private String UserID = ""; //服务器登录验证返回的 UserID

        public override PlatformSdkType PlatformSdkMgrType
        {
            get
            {
                return PlatformSdkType.AndroidFn;
            }
        }

        public override int RecommendedServerID
        {
            get
            {
                return int.Parse(m_config.GetStatic<string>("targetServerId"));
            }
        }

        public override bool IsLoginUI
        {
            get
            {
                return m_config.GetStatic<bool>("bLoginUi");

            }
            set
            {
                m_config.SetStatic<bool>("bLoginUi", value);
            }
        }

        public override string Uid
        {
            get
            {
                return m_config.GetStatic<string>("uid");
            }
            set
            {
                m_config.SetStatic<string>("uid", value);
            }
        }

        public override String GetUserId() 
        { 
            return UserID;
        }

        public override String GetChannelId()
        {
            return m_config.GetStatic<string>("channelId");
        }

        public override String GetGameId()
        {
            return m_config.GetStatic<string>("gameId");
        }

        public override String GetAppId()
        {
            return m_config.GetStatic<string>("appId");
        }

        public override String GetIPAddress()
        {
            return Network.player.ipAddress;
        }

        public override bool IsLoginDone
        {
            get
            {
                return m_config.GetStatic<bool>("isLoginDone");
            }
            set
            {
                m_config.SetStatic<bool>("isLoginDone", value);
            }
        }

        /// <summary>
        /// 获取设备信息
        /// 1、设备ID
        /// 2、设备类型(小米、三星、苹果)
        /// 3、设备名称(三星GT-S5830、小米2S、iphone4S)
        /// 4、操作系统版本号(2.3.4)
        /// 5、屏幕分辨率(1280*720)
        /// 6、移动网络运营商（中国移动、中国联通）
        /// 7、联网方式（3G、WiFi）
        /// </summary>
        public override string[] GetDeviceInfo()
        {
            return m_config.GetStatic<string[]>("deviceInfo");
        }

        /// <summary>
        /// 开启推送服务
        /// </summary>
        /// <param name="gameVersion">应用的版本号</param>
        /// <param name="uid">若App有自己的账号体系，这里填写自己的账号；否则填写0</param>
        /// <param name="family">用户家族标记</param>
        public override void StartPushService(string gameVersion, string uid, string family) 
        {
            LoggerHelper.Info(string.Format("[AndroidSdkMgr] StartPushService gameVersion:{0},uid:{1},family:{2}", gameVersion, uid, family));
            m_mainActivity.Call("startPushService", gameVersion, uid, family);
        }

        /// <summary>
        /// 停止推送服務
        /// </summary>
        public override void StopPushService() 
        {
            LoggerHelper.Info("[AndroidSdkMgr] StopPushService");
            m_mainActivity.Call("stopPushService");
        }

        public AndroidSdkMgr()
        {
            Start();
        }

        void Start()
        {
            Instance = this;
            LoggerHelper.Info("[AndroidSDK] Init [Start]");
            m_config = new AndroidJavaClass("com.leishen.utils.Config");
            AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            m_mainActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
            //SendStartGameLog();
            LoggerHelper.Info(string.Format("[AndroidSDK] setAndroidCallbackUnityObjectName:{0} [OK]", LoaderDriver.Instance.name));
            m_mainActivity.Call("setAndroidCallbackUnityObjectName", LoaderDriver.Instance.name);
            IsLoginUI = true;
            //AndroidUtils com = LoaderDriver.Instance.gameObject.AddComponent<AndroidUtils>();   //Type.GetType("AndroidUtils")
            AndroidUtils com = new AndroidUtils();
            LoggerHelper.Info(string.Format("[AndroidSDK] AndroidUtils:{0} [OK]", com));
            //InitWxID();

            /*if (Application.persistentDataPath.Contains("com.xgame.mc"))
            {
                Action downloadServerInfo = () =>
                {
                    GameLoader.Utils.GetHttp(SystemConfig.GetCfgInfoUrl("popgame_server"),
                        (resp) =>
                        {
                            LoggerHelper.Info("ActualserverInfo:" + resp);
                            ActualserverInfo = resp;
                        },
                        (code) =>
                        {
                            LoggerHelper.Error("download ActualserverInfo fail!");
                        });
                };
                downloadServerInfo.BeginInvoke(null, null);
            }*/
        }

        override public void InitWxID()
        {
            string wxAppId = SystemConfig.GetValueInCfg("wxAppId");
            if (string.IsNullOrEmpty(wxAppId))
            {
                LoggerHelper.Info(string.Format("Not Found wxAppId:{0}", wxAppId));
            }
            else
            {
                LoggerHelper.Info(string.Format("wxAppId:{0}", wxAppId));
                m_mainActivity.Call("InitWXSdk", wxAppId);
            }
        }

        //void Update()
        //{
        //    //退出游戏按钮
        //    if (Input.GetKeyDown(KeyCode.Escape))
        //    {
        //        ExitSdk();
        //    }
        //}

        /// <summary>
        /// 平台登录发送
        /// </summary>
        public override void Login(Action<List<string>> loginSuccessCB, Action<int> loginFailtCB)
        {

            loginSuccessFunction = loginSuccessCB;
            loginFailFunction = loginFailtCB;

            LoggerHelper.Info(string.Format("[AndroidSdk] Login Platform:{0}", IsLoginDone ? "[Start]" : "[Has Login]"));
            m_mainActivity.Call("login");
        }

        public override void SetLoginDataToSDK(string jsonData, Action<string> setJsonToSDKCB)
        {
            LoggerHelper.Info(string.Format("[AndroidSdk] Login SetLoginDataToSDK :{0}",jsonData));
            setJsonToSDKFunction = setJsonToSDKCB;
            m_mainActivity.Call("setLoginDataToSDK", jsonData);
            ParsingJsonData(jsonData);
        }

        public override void ParsingJsonData(string jsonData)
        {
            JsonData jsonData2 = JsonMapper.ToObject(jsonData);

            if (!jsonData2.Keys.Contains("content"))
            {
                LoggerHelper.Info(string.Format("[AndroidSdk] Login SetLoginDataToSDK :content is null"));
                return;
            }

            JsonData contentJson = jsonData2["content"];

            if (!contentJson.Keys.Contains("data"))
            {
                LoggerHelper.Info(string.Format("[AndroidSdk] Login SetLoginDataToSDK :data is null"));
                return;
            }

            JsonData dataJson = contentJson["data"];

            if (!dataJson.Keys.Contains("userId"))
            {
                LoggerHelper.Info(string.Format("[AndroidSdk] Login SetLoginDataToSDK :userId is null"));
                return;
            }
            UserID = dataJson["userId"].ToString();
        }

        /// <summary>
        /// 充值
        /// 
        ///     	params.put(SDKParamKey.PAY.CP_ORDER_ID, orderId); // CP 订单号
    	///params.put(SDKParamKey.PAY.ROLE_ID, roleId); // 角色ID
    	///params.put(SDKParamKey.PAY.ROLE_NAME , roleName); // 角色名称
    	///params.put(SDKParamKey.PAY.ROLE_LEVEL, roleLevel); // 角色等级
    	///params.put(SDKParamKey.PAY.VIP_LEVEL, vipLevel); // 角色VIP等级
    	///params.put(SDKParamKey.PAY.SERVER_ID, serverId); // 服务器 ID
    	///params.put(SDKParamKey.PAY.SERVER_NAME, serverName); // 服务器名称
    	///params.put(SDKParamKey.PAY.AMOUNT, shopMoney); // 商品金额(单位:分)
    	// 购买数量 ( 如 : 100元宝 填 100 )
    	///params.put(SDKParamKey.PAY.PRODUCT_COUNT, shopCount);
    	///params.put(SDKParamKey.PAY.PRODUCT_NAME, shopName); // 商品名称 ( 如 : XX元宝 )
    	///params.put(SDKParamKey.PAY.PRODUCT_TYPE, shopType); // 商品类型(如: 钻石, 礼包)
    	///params.put(SDKParamKey.PAY.PRODUCT_ID, shopId); // 商品ID(需要传递对应的ID)
    	///params.put(SDKParamKey.PAY.DESC, shopDes); // 商品描述
        ///params.put(SDKParamKey.PAY.RATE, shopRate); // 兑换比率 默认 填 10
        /// 
        /// </summary>
        public override void BuyMarkt(string orderId, string roleId, string roleName, string roleLevel, string vipLevel,
            string serverId, string serverName, string shopMoney, string shopCount, string shopName,
            string shopType, string shopId, string shopDes, string shopRate, string shopCallbackUrl)
        {
            m_mainActivity.Call("charge", orderId, roleId, roleName, roleLevel, vipLevel,
             serverId,  serverName,  shopMoney,  shopCount,  shopName,
             shopType,  shopId,  shopDes,  shopRate,  shopCallbackUrl);




            LoggerHelper.Info("[SDK] orderId = " + orderId + ",roleId=" + roleId +
                ",roleName=" + roleName + ",vipLevel=" + vipLevel + ",serverId=" + serverId +
                ",serverName=" + serverName + ",shopMoney=" + shopMoney + ",shopCount=" + shopCount +
                ",serverId=" + serverId + ",roleName=" + roleName + ",shopCallbackUrl=" + shopCallbackUrl +
                ",shopName=" + shopName + ",shopType=" + shopType);
        }

        private void ShowMsg(int msgId, string msg)
        {
            if (sdkTipHandler != null) sdkTipHandler(msgId, false, msg);
        }

        public override void ExitSdk()
        {
            LoggerHelper.Info("[AndroidSDK] ShowExitDialog");
            m_mainActivity.Call("exit");
        }

        public override void GotoGameCenter()
        {
            m_mainActivity.Call("gotoGameCenter");
        }

        public override void CleanLocalData()
        {
            //m_mainActivity.Call("clean");
        }

        #region 平台日志
        //登录成功，服务端登录验证成功后
        public override void LoginGameLog()
        {
            m_mainActivity.Call("logLoginFinish", PlatformSdkMgr.Instance.Uid);
        }

        //选服日志
        public override void SelectServerLog(string roleLevel, string userName, string serverId)
        {
            m_mainActivity.Call("logSelectServer", roleLevel, userName, serverId);
        }

        //进入游戏（选完服，角色正式进入游戏时）
        public override void EnterGameLog(string serverId, string roleName = "0", string roleId = "", string roleLevel = "", string serverName = "")
        {
            m_mainActivity.Call("logEnterGame", roleId, roleName, roleLevel, serverId, serverName);
        }

        //创建角色
        public override void CreateRoleLog(string roleId, string roleName, string roleLevel, string vipLevel, string serverId, string serverName, string diamond, string guildName,string createTime)
        {
            m_mainActivity.Call("logCreateRole", UserID, roleId, roleName, roleLevel,vipLevel,serverId,serverName,diamond,guildName,createTime);
        }

        //角色升级
        public override void RoleLevelLog(int roleLevel, int serverId)
        {
            m_mainActivity.Call("logRoleLevel", roleLevel.ToString(), serverId.ToString());
        }

        public override void ShowSDKLog(string functionName, string roleId, string roleName, string roleLevel, string vipLevel, string serverId, string serverName, string diamond, string guildName, string createTime)
        {
            m_mainActivity.Call(functionName, UserID, roleId, roleName, roleLevel, vipLevel, serverId, serverName, diamond, guildName, createTime);
        }


        public override void onBuyItemLog(string functionName, string roleId, string roleName,
            string roleLevel, string vipLevel, string serverId, string serverName,
            string diamond, string guildName, string createTime, string itemPrice, string itemAction, string itemCount,
            string itemName, string itemDes, bool isPayFromCharge, bool isGain)
        {
            m_mainActivity.Call("onBuyItem", UserID, roleId, roleName, roleLevel, vipLevel, 
                serverId, serverName, diamond, guildName, createTime,itemPrice,itemAction,itemCount,itemName,itemDes,isPayFromCharge,isGain);
        }
        #endregion

        public override void OpenForum()
        {
            // m_mainActivity.Call("loginForm");
        }

        public override void OnSwitchAccount()
        {
            m_mainActivity.Call(IsLoginDone ? "switchUser" : "login");  //如果已登录过，则调用切换账号接口
            LoggerHelper.Info(string.Format("[OnSwitchAccount] OperResult:{0}", IsLoginDone ? "switchUser" : "login"));
        }

        public override void RestartGame()
        {
            m_mainActivity.Call("restartGame");
        }

        public override void InitVoiceCom(string roleName)
        {
            LoggerHelper.Debug("InitVoiceCom: ", this.GetType().Name);
            //Mogo.Util.Utils.DeleteDir(SystemConfig.VoiceMsgPath);
            hasInitVoice = true;
            //m_mainActivity.Call("initVoiceCom", roleName, SystemConfig.VoiceMsgPath);
        }

        public override void ExitVoiceCom()
        {
            if (hasInitVoice)
            {
                m_mainActivity.Call("clearGotyeAPI");
                //Mogo.Util.Utils.DeleteDir(SystemConfig.VoiceMsgPath);
            }
            else
            {
                LoggerHelper.Debug("Has not init VoiceCom");
                return;
            }
        }

        public override void Shake(long milliseconds)
        {
            m_mainActivity.Call("Shake", milliseconds);
        }

        public override void StopShake()
        {
            m_mainActivity.Call("StopShake");
        }

        /// <summary>
        /// 分享链接
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="desc">分享内容</param>
        /// <param name="link">分享跳转链接</param>
        public override void ShareLink(string title, string desc, string link)
        {
            m_mainActivity.Call("shareLink", title, desc, link);
        }

        /// <summary>
        /// 分享图片
        /// </summary>
        /// <param name="url">图片所在本地路径</param>
        public override void ShareImage(string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                LoggerHelper.Info("[微信分享] 分享图片:" + url);
                m_mainActivity.Call("shareImage", url);
            }
        }

        /// <summary>
        /// 获取推广分享链接
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="dbid"></param>
        /// <param name="roleName"></param>
        /// <param name="roleLevel"></param>
        /// <param name="serverId"></param>
        /// <param name="serverName"></param>
        /// <param name="callbackInfo"></param>
        public override void GetShareUrl(string uid, string dbid, string roleName, string roleLevel, string serverId, string serverName, string callbackInfo)
        {
            LoggerHelper.Info(string.Format("获取分享链接uid:{0},dbid:{1},roleName:{2},roleLevel:{3},serverId:{4},serverName:{5},callbackInfo:{6}", uid, dbid, roleName, roleLevel, serverId, serverName, callbackInfo));
            string localAbsPath = Application.persistentDataPath + "/ewm.png";
            m_mainActivity.Call("getShareUrl", uid, dbid, roleName, roleLevel, serverId, serverName, callbackInfo, localAbsPath);
        }

        /// <summary>
        /// 获取玩家所在城市地理位置
        /// </summary>
        public override void GetLocality() 
        {
            m_mainActivity.Call("getLocality");
        }

        /// <summary>
        /// 获取玩家所在城市哪个区地理位置
        /// </summary>
        public override void GetSubLocality() 
        {
            m_mainActivity.Call("getSubLocality");
        }

        /// <summary>
        /// 获取玩家所在城市所在区的哪条街道地理位置
        /// </summary>
        public override void GetThoroughfare() 
        {
            m_mainActivity.Call("getThoroughfare");
        }

        public override void SetNetwork()
        {
            m_mainActivity.Call("gotoNetworkSetting");
        }

        public override void Log(string msg)
        {
            m_mainActivity.Call("showLog", msg);
        }

        public override void ShowUserCenterForCW()
        {
            m_mainActivity.Call("ShowUserCenterForCW");
        }

        #region 登陆回调
        public override string PackageName
        {
            get
            {
                return m_mainActivity.Call<string>("getPackageName");
            }
        }

        public override string PlatformName
        {
            get
            {
                return m_config.GetStatic<string>("platformName");
            }
        }

        private string GetStringFromJava(string key)
        {
            string fromJava = "";
            //#if UNITY_ANDROID
            try
            {
                fromJava = m_config.GetStatic<string>(key);
            }
            catch (Exception e)
            {
                fromJava = "";
                LoggerHelper.Error(e);
            }
            //#endif
            return fromJava;
        }

        //平台登陆成功--回调函数
        public void OnComplete(string msg)
        {
            LoggerHelper.Info("[AndroidSDK] Init [OnComplete]");
            SetupInfo();
            IsLoginDone = true;
            LoginGameLog();         //登陆成功日志
            //if (isJustAutoLogin)
            //{
            //    LoggerHelper.Info("[SDK] AutoLogin");
            //    isJustAutoLogin = false;
            //    if (webLoginHandler != null) webLoginHandler();
            //}
        }

        //回传 二次验证的数据 回 SDK
        public void OnLoginPC(string msg)
        {
            if (setJsonToSDKFunction != null)
            {
                setJsonToSDKFunction(msg);
            }
        }

        public override void SetSwitchAccountCB(Action _switchAccountCB)
        {
            switchAccountCB = _switchAccountCB;
        }

        //平台切换账号--回调函数
        public void OnSwitchAccountBySDK(string msg)
        {
            if(switchAccountCB != null)
            {
                switchAccountCB();
                if (msg == "1")
                {
                    OnComplete("");
                }
            }
        }

        public void OnException()
        {
            LoggerHelper.Info("平台登录异常");
            isJustAutoLogin = false;
            ShowMsg(1, null);
        }

        public void OnCancel()
        {
            LoggerHelper.Info("平台登录被取消");
            isJustAutoLogin = false;
            ShowMsg(2, null);
        }

        public void LoginError(string msg)
        {
            LoggerHelper.Info("平台登录出错 msg:" + msg);
            isJustAutoLogin = false;
            ShowMsg(-1, msg);
        }

        public override void SetupInfo()
        {
            Log("AfterGetInfo");
            // 游戏中将要使用到的唯一id，这个id将取代 username
            //string username = m_config.GetStatic<string>("username");
            //string suid = m_config.GetStatic<string>("suid");
            //string sext = m_config.GetStatic<string>("sext");
            //string platfromname = m_config.GetStatic<string>("platformName");

            //构造返回给应用层的参数
            //if (loginReturnValue == null) loginReturnValue = new string[4];
            //loginReturnValue[0] = suid;
            //loginReturnValue[1] = username;
            //loginReturnValue[2] = sext;
            //loginReturnValue[3] = platfromname;
            //LoggerHelper.Info("platformName:" + platfromname);
            //LoggerHelper.Info("username:" + username);
            //LoggerHelper.Info("uid:" + suid);
            //LoggerHelper.Info("ext:" + sext);

            //string actualName = m_config.GetStatic<string>("actualUsername");
            //if (string.IsNullOrEmpty(actualName)) actualName = username;
            //if (sdkLoginOkHandler != null) sdkLoginOkHandler(actualName, loginReturnValue);
            //LoggerHelper.Info(string.Format("[Sdk-SetupInfo()] username:{0},showName:{1}", username, actualName));

            if (loginSuccessFunction != null)
            {
                LoggerHelper.Info("[AndroidSDK] Init [loginSuccessFunction]");
                string uid = m_config.GetStatic<string>("uid");
                string sid = m_config.GetStatic<string>("sid");
                string gameId = GetGameId();// m_config.GetStatic<string>("gameId");
                string channelId = GetChannelId();// m_config.GetStatic<string>("channelId");
                string appId = GetAppId();// m_config.GetStatic<string>("appId");
                string extra =  m_config.GetStatic<string>("extra");
                List<string> dataList = new List<string>();
                dataList.Add(uid);
                dataList.Add(sid);
                dataList.Add(gameId);
                dataList.Add(channelId);
                dataList.Add(appId);
                dataList.Add(extra);
                loginSuccessFunction(dataList);
            }
            else
            {
                Log("no loginsuccessfunction");
            }
        }


        #endregion

        #region 充值回调
        public void FnSdkPaySuccess(string data)
        {
            //MogoMsgBox.Instance.ShowFloatingText(LanguageData.GetContent(4112));
            LoggerHelper.Info("[SDK]成功提交充值订单: " + data);
            if (voucherHandler != null) voucherHandler(0, data);
        }

        public void FnSdkPayFailed(string data)
        {
            LoggerHelper.Info("[SDK]提交充值订单失败：" + data);
            if (voucherHandler != null) voucherHandler(1, data);
        }

        public void FnSdkPayCancel(string data)
        {
            LoggerHelper.Info("[SDK]取消充值订单：" + data);
            if (voucherHandler != null) voucherHandler(2, data);
        }
        #endregion

        #region 系统通知
        public override void SetupNotificationData(string id, string time, string date, string type, string tag, string title, string titleBanner, string content, int isAdd = 0)
        {
            m_mainActivity.Call("setupDailyNotification", id, time, date, type, tag, title, titleBanner, content, isAdd);
            LoggerHelper.Info("[SDK]setupDailyNotification [OK]");
        }

        override public void AddNotificationRecord(int tag)
        {
            m_mainActivity.Call("addRecord", tag);
            LoggerHelper.Info("[SDK] addRecord tag:" + tag);
        }

        override public void CancelNotificationRecord(int tag)
        {
            m_mainActivity.Call("cancelRecord", tag);
            LoggerHelper.Info("[SDK] CancelNotificationRecord tag:" + tag);
        }

        //下一次体力满通知
        override public void OnSetupNotification()
        {
            if (maxEnergyHandler != null) maxEnergyHandler();
        }

        /// <summary>
        /// 增加通知
        /// </summary>
        /// <param name="needTime">剩余时间(分钟)</param>
        public override void addNotification(int needTime)
        {
            m_mainActivity.Call("addNotification", (long)needTime);
            LoggerHelper.Info("[SDK] addNotification needTime:" + needTime);
        }
        #endregion

        #region 微信回调

        public void OnWxDone()
        {
            Log("OnWxDone");
            //ShareManager.Instance.OnShareSuccess();
            LoggerHelper.Info("[OnWxDone] Wx Share Success!");
            if (wxShareResultHandler != null) wxShareResultHandler(true, null);
        }

        public void OnWxNotFound()
        {
            Log("OnWxNotFound");
            LoggerHelper.Info("[OnWxNotFound] Wx Share Fail!");
            if (wxShareResultHandler != null) wxShareResultHandler(false, "OnWxNotFound");
        }

        public void OnWxCancel()
        {
            Log("OnWxCancel");
            LoggerHelper.Info("[OnWxCancel] Wx Share cancel!");
            if (wxShareResultHandler != null) wxShareResultHandler(false, "OnWxCancel");
        }

        public void OnWxFail()
        {
            Log("OnWxFail");
            LoggerHelper.Info("[OnWxFail] Wx Share fail!");
        }

        public void OnWxDenied()
        {
            Log("OnWxDenied");
            LoggerHelper.Info("[OnWxDenied] Wx Share denied!");
            if (wxShareResultHandler != null) wxShareResultHandler(false, "OnWxDenied");
        }

        public void OnWxUnknow()
        {
            Log("onWxUnknow");
            LoggerHelper.Info("[OnWxUnknow] Wx Share unkown!");
            if (wxShareResultHandler != null) wxShareResultHandler(false, "OnWxUnknow");
        }

        public void onWxNotInstalledOrNotSupport()
        {
            Log("onWxNotInstalledOrNotSupport");
            LoggerHelper.Info("[onWxNotInstalledOrNotSupport] Wx NotInstall Or NotSupport!");
            if (wxShareResultHandler != null) wxShareResultHandler(false, "onWxNotInstalledOrNotSupport");
        }

        /// <summary>
        /// 获取推广分享链接回调
        /// </summary>
        /// <param name="url">短链接</param>
        /// <param name="ewmPath">二维码图本地路径</param>
        public void OnGetShareUrl(string url)
        {
            string localAbsPath = Application.persistentDataPath + "/ewm.png";
            LoggerHelper.Info("[OnGetShareUrl] url:" + url);
            if (shareUrlHandler != null) shareUrlHandler(url, localAbsPath);
        }
        #endregion

        #region 语音枚举变量
        enum GotyeStatusCode
        {
            STATUS_ENTER_ROOM_DULPLICATE = 404,
            STATUS_FORBIDDEN_SEND_MSG = 500,
            STATUS_FORCE_LOGOUT = 303,
            STATUS_LOGIN_FAILED = 302,
            STATUS_NETWORK_DISCONNECTED = 304,
            STATUS_NOT_IN_ROOM = 402,
            STATUS_OK = 0,
            STATUS_PARAME_ERROR = 700,
            STATUS_RECORDING_ERROR = 600,
            STATUS_REQUEST_MIC_FAILED = 503,
            STATUS_ROOM_FULL = 401,
            STATUS_ROOM_NOT_EXISTS = 400,
            STATUS_SEND_MSG_TO_SELF = 502,
            STATUS_SYSTEM_ERROR = 800,
            STATUS_TIMEOUT = 300,
            STATUS_UNKOWN_ERROR = 9999,
            STATUS_USER_NOT_EXISTS = 501,
            STATUS_VAD_VOICE_TIME_UP = 505,
            STATUS_VERIFY_FAILED = 301,
            STATUS_VOICE_SERVER_UNAVAILABLE = 403,
            STATUS_VOICE_TIME_UP = 504
        }
        #endregion

        #region Gotye call

        /// <summary>
        /// 开始录音
        /// </summary>
        /// <param name="tarName"></param>
        override public void GotyeStartTalkTo(string tarName)
        {
            LoggerHelper.Info("[VoiceManager] GotyeStartTalkTo = " + tarName, false);
            m_mainActivity.Call("StartToRecordVoice", tarName);
        }

        /// <summary>
        /// 结束录音
        /// </summary>
        override public void GotyeStopTalk()
        {
            LoggerHelper.Info("[VoiceManager] GotyeStopTalk  ");
            m_mainActivity.Call("StopRecordVoice");
        }

        /// <summary>
        /// 发送语音信息
        /// </summary>
        /// <param name="isOk">大于0正常发送，小于0清除信息</param>
        override public void GotyeSendVoiceMessage(int isOk)
        {
            LoggerHelper.Info("[VoiceManager] GotyeSendVoiceMessage = " + isOk, false);
            m_mainActivity.Call("gotyeSendVoiceMessage", isOk);
        }

        /// <summary>
        /// 播放录音
        /// </summary>
        /// <param name="senderName"></param>
        /// <param name="msgID"></param>
        override public void GotyePlayVoiceMessage(string senderName, string msgID)
        {
            LoggerHelper.Info("[VoiceManager] GotyePlayVoiceMessage = " + senderName + "   msgID = " + msgID, false);
            m_mainActivity.Call("PlayMusic", senderName);
        }

        #endregion

        #region Gotye callback
        //private float music = 0;
        //private float sound = 0;
        override public void GotyeLoginResult(string errCode)
        {
            if (errCode == GotyeStatusCode.STATUS_OK.ToString())
            {
                LoggerHelper.Info("GotyeLoginResult  = OK");
            }
            else
            {
                LoggerHelper.Debug("GotyeLoginResult Error Code = " + errCode + "  Means = " + Enum.Parse(typeof(GotyeStatusCode), errCode));
            }
        }

        override public void GotyeLogoutResult(string errCode)
        {
            if (errCode == GotyeStatusCode.STATUS_OK.ToString())
            {
                LoggerHelper.Info("GotyeLogoutResult  = OK");
            }
            else
            {
                LoggerHelper.Debug("GotyeLogoutResult Error Code = " + errCode + "  Means = " + Enum.Parse(typeof(GotyeStatusCode), errCode));
            }
        }

        override public void GotyeStartTalkToCallback()
        {
            //music = SoundManager.MusicVolume;
            //sound = SoundManager.SoundVolume;
            LoggerHelper.Info("[VoiceManager] GotyeStartTalkToCallback = ");
            //无操作
            //EventDispatcher.TriggerEvent(SettingEvent.MotityMusicVolume, 0f);
            //EventDispatcher.TriggerEvent(SettingEvent.MotitySoundVolume, 0f);
        }

        override public void GotyeStopTalkCallback(string msgStr)
        {
            LoggerHelper.Info("[VoiceManager] Stop Talk Callback = " + msgStr);
            //EventDispatcher.TriggerEvent(SettingEvent.MotityMusicVolume, music);
            //EventDispatcher.TriggerEvent(SettingEvent.MotitySoundVolume, sound);
        }

        public override void GotyeReceiveMessageCallback(string resultStr)
        {
            //CommunityUILogicManager.Instance.SendVoiceMessage(resultStr, true);
        }

        override public void GotyeSendVoiceMessageCallback(string result)
        {
            if (result == GotyeStatusCode.STATUS_OK.ToString())
            {
                LoggerHelper.Info("GotyeLoginResult  = OK");
            }
            else
            {
                LoggerHelper.Debug("GotyeLoginResult Error Code = " + result + "  Means = " + Enum.Parse(typeof(GotyeStatusCode), result));
            }
        }

        override public void GotyeVoicePlayCallback(string result)
        {
            /*string errorString = "";
          switch (int.Parse(result))
           {
               case 1:
                   errorString = LanguageData.GetContent(79058);
                   break;
               case 2:
                   errorString = LanguageData.GetContent(79059);
                   break;
               case 3:
                   errorString = LanguageData.GetContent(79060);
                   break;
           }
           if (int.Parse(result) != 0)
           {
               MogoMsgBox.Instance.ShowFloatingText(errorString);
           }*/
        }

        override public void GotyeVoicePlayStartCallback()
        {
            //music = SoundManager.MusicVolume;
            //sound = SoundManager.SoundVolume;
            //EventDispatcher.TriggerEvent(SettingEvent.MotityMusicVolume, 0f);
            //EventDispatcher.TriggerEvent(SettingEvent.MotitySoundVolume, 0f);
        }

        override public void GotyeVoicePlayingCallback(string tStr)
        {
            //获取播放时间
            //目前没有作用
        }

        override public void GotyeVoicePlayEndCallback()
        {
            //EventDispatcher.TriggerEvent(SettingEvent.MotityMusicVolume, music);
            //EventDispatcher.TriggerEvent(SettingEvent.MotitySoundVolume, sound);
        }

        #endregion

        public override string GetProxyIp(string baseIP, int basePort)
        {
            return m_mainActivity.Call<string>("getProxyIp", baseIP, basePort, ActualserverInfo);
        }

        public override bool IsShowLoginFirst()
        {
            bool islf = false;
            //#if UNITY_ANDROID
            if (UnityPropUtils.Platform == RuntimePlatform.Android)
            {
                try
                {
                    islf = m_config.GetStatic<bool>("isShowLoginFirst");
                    LoggerHelper.Info("IsShowLoginFirst:" + islf);
                }
                catch (Exception e)
                {
                    LoggerHelper.Error(e);
                    islf = false;
                }
            }
            //#endif
            return islf;
        }

        public override bool IsGetUidFromServer()
        {
            bool igufs = false;
            //#if UNITY_ANDROID
            if (UnityPropUtils.Platform == RuntimePlatform.Android)
            {
                try
                {
                    igufs = m_config.GetStatic<bool>("isGetUidFromServer");
                    LoggerHelper.Info("isGetUidFromServer:" + igufs);
                }
                catch (Exception e)
                {
                    LoggerHelper.Error(e);
                    igufs = false;
                }
            }
            //#endif
            return igufs;
        }

        public override bool IsLoginGameLog()
        {
            bool ilgl = false;
            //#if UNITY_ANDROID
            if (UnityPropUtils.Platform == RuntimePlatform.Android)
            {
                try
                {
                    ilgl = m_config.GetStatic<bool>("isLoginGameLog");
                    LoggerHelper.Info("isLoginGameLog:" + ilgl);
                }
                catch (Exception e)
                {
                    LoggerHelper.Error(e);
                    ilgl = false;
                }
            }
            //#endif
            return ilgl;
        }

        public override bool IsHideSwitchAccount()
        {
            bool ihsa = false;
            //#if UNITY_ANDROID
            if (UnityPropUtils.Platform == RuntimePlatform.Android)
            {
                try
                {
                    ihsa = m_config.GetStatic<bool>("isHideSwitchAccount");
                    LoggerHelper.Info("isHideSwitchAccount:" + ihsa);
                }
                catch (Exception e)
                {
                    LoggerHelper.Error(e);
                    ihsa = false;
                }
            }
            //#endif
            return ihsa;
        }

        //显示平台小助手
        public override void ShowFloatBar()
        {
            m_mainActivity.Call("showFloatBar");
            LoggerHelper.Info("[SDK] 显示平台小助手");
        }

        //隐藏平台小助手
        public override void HideFloatBar()
        {
            m_mainActivity.Call("hideFloatBar");
            LoggerHelper.Info("[SDK] 隐藏平台小助手");
        }

        //SDK回调的用于打印游戏日志
        public override void PrintLogInfo(string msg)
        {
            LoggerHelper.Info("[SDK]" + msg);
        }

        public override int GetBattery()
        {
            int battery = m_mainActivity.Call<int>("GetBattery");
            //LoggerHelper.Info("[SDK] 获取电量" + battery);
            return battery;
        }

        public override void SetScreenLight(int brightness)
        {
            m_mainActivity.Call("SetLight",brightness);
        }

        public override int GetCutoutScreen()
        {
            return m_mainActivity.Call<int>("initCutout");
        }

        public override int GetSDKInitState()
        {
            return 1;
        }

        public override void LoginOutSDK()
        {

        }

        #region Android生命周期回调事件
        public void OnStart()
        {
            LoggerHelper.Debug("OnStart");
        }

        public void OnPause()
        {
            LoggerHelper.Debug("OnPause");
        }

        public void OnStop()
        {
            LoggerHelper.Debug("OnStop");
        }

        public void OnRestart()
        {
            LoggerHelper.Debug("OnRestart");
        }

        public void OnDestory()
        {
            LoggerHelper.Debug("OnDestory");
        }

        public void OnResume()
        {
            LoggerHelper.Debug("OnResume");
        }

        #endregion
#endif
    }
}
