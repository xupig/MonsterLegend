﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameLoader.Mgrs;
using GameLoader.Utils;
using UnityEngine;

namespace GameLoader.PlatformSdk
{
    /// <summary>
    /// 平台更新回调监听
    /// </summary>
    public class PlatformUpdateCallback : MonoBehaviour
    {
        //没有新版本时
        public void OnNotNewVersion(string s)
        {
            LoggerHelper.Info("[OnNotNewVersion] result:" + s);
            //gameObject.GetComponent<Driver>().CheckVersionFinish();
            //gameObject.GetComponent<LoaderDriver>().OnUpdateVersionCallback();
            VersionManager.Instance.CheckPkgVersion(); //平台没有apk更新，接着检查pkg版本
        }

        //没有sd卡时
        public void OnNotSDCard(string s)
        {
            LoggerHelper.Info("[OnNotSDCard] resp:" + s);
        }

        //取消普通更新
        public void OnCancelNormalUpdate(string s)
        {
            LoggerHelper.Info("[OnCancelNormalUpdate] resp:" + s);
            Application.Quit();
        }

        //核对版本失败
        public void OnCheckVersionFailure(string s)
        {
            LoggerHelper.Info("[OnCheckVersionFailure] resp:" + s);
        }

        //强制更新中
        public void OnForceUpdateLoading(string s)
        {
            // 强制更新对话框已打开，正在自动下载更新，游戏请停在初始化完成界面，暂停登录
            // 下载完成后会自动提示安装。如果本次不安装，下次检查更新时仍会继续提示安装
            LoggerHelper.Info("[OnForceUpdateLoading] resp:" + s);
        }

        //普通更新中
        public void OnNormalUpdateLoading(string s)
        {
            LoggerHelper.Info("[OnNormalUpdateLoading] resp:" + s);
        }

        //网络错误
        public void OnNetWorkError(string s)
        {
            LoggerHelper.Info("[OnNetWorkError] resp:" + s);
        }

        //更新异常
        public void OnUpdateException(string s)
        {
            LoggerHelper.Info("[OnUpdateException] resp:" + s);
        }

        //取消强制更新
        public void OnCancelForceUpdate(string s)
        {
            LoggerHelper.Info("[OnCancelForceUpdate] resp:" + s);
            Application.Quit();
        }
    }
}
