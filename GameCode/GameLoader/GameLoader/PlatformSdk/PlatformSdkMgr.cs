// 模块名   :  EquipTipManager
// 创建者   :  莫卓豪
// 创建日期 :  2013-8-13
// 描    述 :  android平台sdk登陆对接
using UnityEngine;
using System;
using System.Collections.Generic;
namespace GameLoader.PlatformSdk
{
    public enum PlatformSdkType
    {
        None = 0,
        AndroidFn,
        AndroidKorea,
        IOS
    }

    public class PlatformSdkMgr:MonoBehaviour
    {
        //这三个方法在LoginPanel.cs中注册
        public Action maxEnergyHandler;                          //体力满检查(PlayerAvatar_Resp_Partial.cs中注册)
        public Action webLoginHandler;                           //进行Web登录函数
        public Action<int, bool, string> sdkTipHandler;          //Sdk层提示回调Action<提示内容ID,是否为显示退出游戏确认框[true:是,false:否],平台返回提示> 
        public Action<int, string> voucherHandler;               //充值结果回调
        public Action<string, string[]> sdkLoginOkHandler;       //平台登录成功回调处理
        public Action<bool, string> wxShareResultHandler;        //微信分享結果囘調
        public Action<string, string> shareUrlHandler;           //分享链接获取成功回调

        public bool isJustAutoLogin = false;
        public string ActualserverInfo = "";
        public static PlatformSdkMgr Instance;

        public virtual PlatformSdkType PlatformSdkMgrType
        {
            get { return PlatformSdkType.None; }
        }

        public PlatformSdkMgr()
        {
            Start();
        }

        void Start()
        {
            Instance = this;
        }

        //void Update()
        //{
        //    if (Input.GetKeyDown(KeyCode.Escape))
        //    {//移动设备--按下退出游戏键
        //        if (sdkTipHandler != null) sdkTipHandler(0, true, null);
        //    }
        //}

        public virtual void ExitSdk()
        {
            if (sdkTipHandler != null) sdkTipHandler(0, true, null);
        }

        /// <summary>
        /// 开启推送服务
        /// </summary>
        /// <param name="gameVersion">应用的版本号</param>
        /// <param name="uid">若App有自己的账号体系，这里填写自己的账号；否则填写0</param>
        /// <param name="family">用户家族标记</param>
        virtual public void StartPushService(string gameVersion, string uid, string family) { }

        /// <summary>
        /// 停止推送服務
        /// </summary>
        virtual public void StopPushService() { }

        /// <summary>
        /// 平台名称,例如:4399
        /// </summary>
        virtual public string PlatformName { get { return ""; } }

        virtual public bool IsLoginUI { get { return true; } set { } }

        /// <summary>
        /// 平台账户的uid
        /// </summary>
        virtual public string Uid { get; set; }

        /// <summary>
        /// 初始化微信接口
        /// </summary>
        virtual public void InitWxID() { }

        /// <summary>
        /// 推荐服务器ID
        /// </summary>
        virtual public int RecommendedServerID { get { return 0; } }

        /// <summary>
        /// 平台日志接口
        /// </summary>
        /// <param name="msg"></param>
        virtual public void Log(string msg) { }

        public bool m_isLoginDone = false;
        virtual public bool IsLoginDone
        {
            get
            {
                return false;
            }
            set
            {
                m_isLoginDone = value;
            }
        }

        /// <summary>
        /// 获取设备信息
        /// </summary>
        /// <returns></returns>
        virtual public string[] GetDeviceInfo() { return null; }

        string GetSdkManagerName()
        {
            string name = string.Empty;
            if (UnityPropUtils.Platform == RuntimePlatform.IPhonePlayer)
            {
                name = "IOSPlatformSDKManager";
            }
            else if (UnityPropUtils.Platform == RuntimePlatform.Android)
            {
                name = "AndroidSdkMgr";
            }
            return name;
        }

        virtual public void Shake(long milliseconds)
        {
        }

        virtual public void StopShake()
        {

        }

        virtual public void RestartGame()
        {
            Application.Quit();
        }

        virtual public void Login(Action<List<string>> loginSuccessCB, Action<int> loginFailtCB)
        {
        }
        virtual public void ShowUserCenterForCW()
        {
        }

        virtual public void SetSwitchAccountCB(Action switchAccountCB)
        {

        }

        virtual public void ParsingJsonData(string jsonData)
        {

        }

        /// <summary>
        /// 充值
        /// </summary>
        public virtual void BuyMarkt(string orderId, string roleId, string roleName, string roleLevel, string vipLevel,
            string serverId, string serverName, string shopMoney, string shopCount, string shopName,
            string shopType, string shopId, string shopDes, string shopRate, string shopCallbackUrl)
        {

        }

        virtual public void GotoGameCenter()
        {
        }

        virtual public void CleanLocalData()
        {
        }

        virtual public void LoginGameLog()
        {
        }

        virtual public void OpenForum()
        {
        }

        virtual public void OnSwitchAccount()
        {
        }

        virtual public void SetNetwork()
        {

        }

        /// <summary>
        /// 分享图片
        /// </summary>
        /// <param name="desp"></param>
        /// <param name="url"></param>
        virtual public void ShareImage(string url)
        {
        }

        /// <summary>
        /// 分享连接
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="desc">描述</param>
        /// <param name="link">分享连接，例如：http://www.4399sy.com</param>
        virtual public void ShareLink(string title, string desc, string link)
        {
        }

        /// <summary>
        /// 获取推广分享链接
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="dbid"></param>
        /// <param name="roleName"></param>
        /// <param name="roleLevel"></param>
        /// <param name="serverId"></param>
        /// <param name="serverName"></param>
        /// <param name="callbackInfo"></param>
        virtual public void GetShareUrl(string uid, string dbid, string roleName, string roleLevel, string serverId, string serverName, string callbackInfo)
        { 
        }

        /// <summary>
        /// 获取玩家所在城市地理位置
        /// </summary>
        virtual public void GetLocality() { }

        /// <summary>
        /// 获取玩家所在城市哪个区地理位置
        /// </summary>
        virtual public void GetSubLocality() { }

        /// <summary>
        /// 获取玩家所在城市所在区的哪条街道地理位置
        /// </summary>
        virtual public void GetThoroughfare() { }

        virtual public bool IsShowLoginFirst()
        {
            return false;
        }

        virtual public bool IsGetUidFromServer()
        {
            return false;
        }

        virtual public bool IsLoginGameLog()
        {
            return false;
        }
        virtual public bool IsHideSwitchAccount()
        {
            return false;
        }
        virtual public void ShowFloatBar()
        {
        }

        virtual public void HideFloatBar()
        {
        }

        virtual public void PrintLogInfo(string msg)
        {
        }

        virtual public void AddNotificationRecord(int tag)
        {

        }

        virtual public void CancelNotificationRecord(int tag)
        {

        }

        virtual public void SetupNotificationData(string id, string time, string date, string type, string tag, string title, string titleBanner, string content, int isAdd = 0)
        {

        }

        virtual public void OnSetupNotification()
        {
        }

        virtual public void addNotification(int needTime){ }

        virtual public void CreateRoleLog(string roleId, string roleName, string roleLevel, string vipLevel, string serverId, string serverName, string diamond, string guildName, string createTime)
        {

        }

        virtual public void ShowSDKLog(string functionName, string roleId, string roleName, string roleLevel, string vipLevel, string serverId, string serverName, string diamond, string guildName, string createTime)
        {

        }

        virtual public void onBuyItemLog(string functionName, string roleId, string roleName,
            string roleLevel, string vipLevel, string serverId, string serverName,
            string diamond, string guildName, string createTime, string itemPrice, string itemAction, string itemCount,
            string itemName, string itemDes, bool isPayFromCharge, bool isGain)
        {
        }

        virtual public void RoleLevelLog(int roleLevel, int serverId)
        {
        }

        virtual public void EnterGameLog(string serverId, string roleName = "0", string roleId = "", string roleLevel = "", string serverName = "")
        {
        }

        virtual public void SelectServerLog(string roleLevel, string userName, string serverId)
        {
        }

        virtual public void SetupInfo()
        {
        }

        virtual public string GetProxyIp(string baseIP, int basePort)
        {
            return "";
        }

        virtual public String PackageName
        {
            get { return ""; }
            set { }
        }

        virtual public void SetLoginDataToSDK(string jsonData, Action<string> setJsonToSDKCB)
        {

        }

        virtual public String GetUserId() 
        { 
            return ""; 
        }

        virtual public String GetChannelId()
        {
            return "default_channel";
        }

        virtual public String GetGameId()
        {
            return "";
        }

        virtual public string GetAppId()
        {
            return "";
        }

        virtual public string GetIPAddress()
        {
            return "";
        }

        virtual public int GetBattery()
        {
            return 0;
        }

        virtual public int GetSDKInitState()
        {
            return 1;
        }

        virtual public void LoginOutSDK()
        {

        }

        virtual public void SetScreenLight(int brightness)
        {

        }

        virtual public int GetCutoutScreen()
        {
            return 0;
        }

        #region Gotye Plugins
        virtual public void InitVoiceCom(string roleName)
        {
            //LoggerBuilder.Debug("InitVoiceCom: ", roleName);
        }

        virtual public void ExitVoiceCom()
        {
            //LoggerBuilder.Debug("ExitVoiceCom");
        }

        virtual public void GotyeStartTalkTo(string tarName)
        {
            //LoggerBuilder.Debug("GotyeStartTalkTo: ", tarName);
        }

        virtual public void GotyeStopTalk()
        {
            //LoggerBuilder.Debug("GotyeStopTalk");
        }

        virtual public void GotyeSendVoiceMessage(int isOk)
        {
            //LoggerBuilder.Debug("GotyeSendVoiceMessage: ", isOk);
        }

        virtual public void GotyePlayVoiceMessage(string senderName, string msgID)
        {
            //LoggerBuilder.Debug("GotyePlayVoiceMessage: ", senderName, ", ", msgID);
        }

        virtual public void GotyeLoginResult(string errCode)
        {
            //LoggerBuilder.Debug("GotyeLoginResult: ", errCode);
        }

        virtual public void GotyeLogoutResult(string errCode)
        {
            //LoggerBuilder.Debug("GotyeLogoutResult: ", errCode);
        }

        virtual public void GotyeStartTalkToCallback()
        {
            //LoggerBuilder.Debug("GotyeStartTalkToCallback");
        }

        virtual public void GotyeStopTalkCallback(string msgStr)
        {
            //LoggerBuilder.Debug("GotyeStopTalkCallback: ", msgStr);
        }
        virtual public void GotyeReceiveMessageCallback(string resultStr)
        {

        }
        virtual public void GotyeSendVoiceMessageCallback(string result)
        {
            //LoggerBuilder.Debug("GotyeSendVoiceMessageCallback: ", result);
        }

        virtual public void GotyeVoicePlayCallback(string result)
        {
            //LoggerBuilder.Debug("GotyeVoicePlayCallback: ", result);
        }

        virtual public void GotyeVoicePlayStartCallback()
        {
            // LoggerBuilder.Debug("GotyeVoicePlayStartCallback");
        }

        virtual public void GotyeVoicePlayingCallback(string tStr)
        {
            // LoggerBuilder.Debug("GotyeVoicePlayingCallback: ", tStr);
        }

        virtual public void GotyeVoicePlayEndCallback()
        {
            //LoggerBuilder.Debug("GotyeVoicePlayEndCallback");
        }
        #endregion

    }
}