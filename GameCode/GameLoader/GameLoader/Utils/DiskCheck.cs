﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GameLoader.Utils
{
    /// <summary>
    /// 磁盘检查
    /// </summary>
    public class DiskCheck
    {
        public void TryCreateFile(long size)
        {
            try
            {
                //var fileName = Path.Combine(unRootPath, fileName);
                //fileName = fileName.Replace('\\', '/');
                //using (FileStream streamWriter = File.Create(fileName))
                //{
                //    WriteFile(s, streamWriter);

                //    streamWriter.Flush();
                //    streamWriter.Close();
                //}
            }
            catch (Exception ex)
            {
                //UpdateLog.ERROR_LOG(ex.Message);
                return;
            }
        }

        protected int _bufferSize = 1024 * 10;
        protected byte[] _buffer = new byte[1024 * 10];

        private void WriteFile(ZipInputStream zi, FileStream outFile)
        {
            int size = 0;
            while ((size = zi.Read(_buffer, 0, _bufferSize)) > 0)
            {
                outFile.Write(_buffer, 0, size);
            }
        }
    }
}
