﻿using GameLoader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using GameLoader.LoadingBar;

namespace GameLoader.Mgrs
{
    /// <summary>
    /// 下载进度定时刷新类
    /// 1、目录仅支持刷新单个文件的进度
    /// 2、暂用在断点续传类中(BreakPointDownload)
    /// </summary>
    public class DownloadProgress : MonoBehaviour
    {
        private WWW www;
        private string url;
        private string fileName;
        private bool isFinished;
        private float progress = 0;
        private ProgressBar progressBar;
        public static DownloadProgress instance;

        void Awake()
        {
            instance = this;
            isFinished = true;
            fileName = string.Empty;
            SetDefaultProgressBar();
        }

        //更新进度
        void Update()
        {
            if (isFinished == false)
            {
                if (www != null) progress = www.progress;
                if (progress < 0) progress = 0;
                if (progress > 1) progress = 1;
                if (progressBar != null)
                {
                    progressBar.UpdateProgress(progress, true);
                }
            }
            if (Input.GetKeyDown(KeyCode.Escape) && GameLoader.PlatformSdk.PlatformSdkMgr.Instance != null)
            {
                GameLoader.PlatformSdk.PlatformSdkMgr.Instance.ExitSdk();
            }
        }

        /// <summary>
        /// 使用默认进度条
        /// </summary>
        public void SetDefaultProgressBar()
        {
            progressBar = ProgressBar.Instance;
        }

        /// <summary>
        /// 更换进度条
        /// </summary>
        /// <param name="progressBar"></param>
        public void SetProgressBar(ProgressBar progressBar)
        {
            if (progressBar != null) this.progressBar = progressBar;
        }

        /// <summary>
        /// 显示进度
        /// </summary>
        /// <param name="url">显示进度资源的url</param>
        /// <param name="progress">初始显示进度</param>
        /// <param name="www">www引用</param>
        public void Show(string url, float progress, WWW www)
        {
            if (progressBar != null)
            {
                this.url = url;
                this.www = www;
                this.fileName = GetFileName();
                this.progress = progress;
                this.isFinished = false;
                progressBar.Show();
                progressBar.UpdateTip(string.Format(DefaultLanguage.GetString("downloading"), fileName, 1, 1, www.size / 1024f / 1024f));
                progressBar.UpdateFileSize(0);
            }
        }

        /// <summary>
        /// 显示进度
        /// </summary>
        /// <param name="fileName">显示进度资源的名字</param>
        /// <param name="progress">初始显示进度</param>
        public void ShowDownload(string fileName, float progress, int currentCount, int totalCount, long fileSize, long totalFileSize)
        {
            if (progressBar != null)
            {
                this.fileName = fileName;
                this.www = null;
                this.progress = progress;
                this.isFinished = false;
                progressBar.Show();
                progressBar.UpdateTip(string.Format(DefaultLanguage.GetString("downloading"), fileName, currentCount, totalCount, totalFileSize / 1024f / 1024f));
                progressBar.UpdateFileSize(fileSize / 1024f / 1024f);
            }
        }

        public void ShowDecompress(string fileName, float progress, int currentCount, int totalCount)
        {
            if (progressBar != null)
            {
                this.fileName = fileName;
                this.www = null;
                this.progress = progress;
                this.isFinished = false;
                progressBar.Show();
                progressBar.UpdateTip(string.Format(DefaultLanguage.GetString("decompressing"), fileName, currentCount, totalCount));
                progressBar.UpdateFileSize(0);
            }
        }

        //获取下载地址的文件名
        private string GetFileName()
        {
            if (string.IsNullOrEmpty(url)) return string.Empty;
            int index = url.LastIndexOf("?");
            string s = index != -1 ? url.Substring(0, index) : url;
            return Path.GetFileName(s);
        }

        /// <summary>
        /// 关闭进度
        /// </summary>
        public void Close()
        {
            isFinished = true;
            progress = 0;
            progressBar.UpdateTip("");
            progressBar.Reset();
            //progressBar.Close();
        }

        public void ClearWww()
        {
            www = null;
        }

        /// <summary>
        /// 更新进度
        /// </summary>
        /// <param name="p"></param>
        public void UpdateProgress(float p)
        {
            this.progress = p;
        }

        public void UpdateProgress(int cur, int total)
        {
            this.progress = ((float)cur) / total;
        }

        public void UpdateProgress(int percentage)
        {
            if (percentage > 100) percentage = 100;
            if (percentage < 0) percentage = 0;
            this.progress = percentage / 100f;
        }
    }
}
