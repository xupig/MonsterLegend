﻿using GameLoader.PlatformSdk;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using System;
using System.IO;
using System.Net;
using System.Threading;
using UnityEngine;
using GameLoader.LoadingBar;

namespace GameLoader.Mgrs
{
    /// <summary>
    /// 断点续传异步下载--包装类
    /// 1、支持断点续传
    /// </summary>
    public class BreakPointDownload
    {
        private string _url;
        private string _localAbsPath;
        Action<float, long, long> _progress;
        private Action<string> _onCompleted;
        private Action<string, string> _onFail;
        //private bool _isSetNetwork;
        private long _totalFileSize = 0;
        private long _bytesDownloaded = 0;
        private long _needDownloadSize = 0;
        private Uri _uri;
        private Thread _thread;
        private bool _cancelDownload;

        /// <summary>
        /// 请求或响应超时(60秒)
        /// </summary>
        public const int TIMEOUT = 60 * 1000;

        public BreakPointDownload() { }

        /// <summary>
        /// 释放资源
        /// </summary>
        public void Dispose()
        {
            StopThread();
            Clear();
        }

        //终止下载线程
        private void StopThread()
        {
            try
            {
                //DownloadProgress.instance.Close();
                _cancelDownload = true;
                if (_thread != null) _thread.Abort();
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(string.Format("[断点续传] 终止下载线程失败:{0}", ex.StackTrace));
            }
            finally
            {
                _thread = null;
            }
        }

        /// <summary>
        /// 下载
        /// </summary>
        /// <param name="url">下载地址</param>
        /// <param name="localAbsPath">下载文件本地保存路径</param>
        /// <param name="isShowProgress">是否显示加载进度[true:显示,false:不显示]</param>
        /// <param name="onCompleted">下载完成回调onCompleted(url)</param>
        /// <param name="onFail">下载失败回调onFail(url)</param>
        public void Download(string url, string localAbsPath, Action<float, long, long> progress = null, Action<string> onCompleted = null, Action<string, string> onFail = null)
        {
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            try
            {
                _url = url;
                _localAbsPath = localAbsPath;
                _progress = progress;
                _onCompleted = onCompleted;
                _onFail = onFail;
                _bytesDownloaded = 0;

                //A-1、获取需要下载字节数
                _uri = new Uri(url);
                request = (HttpWebRequest)WebRequest.Create(_uri);
                request.Timeout = TIMEOUT;
                request.ReadWriteTimeout = TIMEOUT;
                request.Proxy = null; //听说能解决第一次连接慢的问题 
                response = (HttpWebResponse)request.GetResponse();
                _totalFileSize = response.ContentLength;
                _needDownloadSize = _totalFileSize;
                response.Close();
                request.Abort();
                response = null;
                request = null;

                string folder = Path.GetDirectoryName(localAbsPath);
                if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);
                if (File.Exists(localAbsPath))
                {
                    using (FileStream fs = new FileStream(localAbsPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                    {
                        _bytesDownloaded = fs.Length;
                        _needDownloadSize = _totalFileSize - _bytesDownloaded;
                    }
                }
                LoggerHelper.Info(string.Format("[断点续传] 文件:{0},总字节:{1}-已下载字节:{2}=需下载字节:{3}", url, _totalFileSize, _bytesDownloaded, _needDownloadSize));

                //A-2、开始加载剩余字节数
                if (_needDownloadSize > 0)
                {
                    //if (isShowProgress)
                    //{
                    //    DownloadProgress.instance.Show(url, ((float)bytesDownloaded) / totalFileSize);          //加入主线程更新进度
                    //}
                    //else DownloadProgress.instance.Close();
                    _cancelDownload = false;
                    _thread = new Thread(Download);
                    _thread.Start();
                }
                else
                {
                    if (onCompleted != null) onCompleted(url);
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(string.Format("文件：{0}下载失败!reason:{1}", url, ex.StackTrace));
                onFail.SafeInvoke(url, ex.Message);
            }
            finally
            {
                if (response != null) response.Close();
                if (request != null) request.Abort();
                response = null;
                request = null;
            }
        }

        //开始异步下载剩余文件
        private void Download()
        {
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            try
            {
                request = (HttpWebRequest)WebRequest.Create(_uri);
                request.AddRange((int)_bytesDownloaded);
                request.Timeout = TIMEOUT;
                request.ReadWriteTimeout = TIMEOUT;
                request.Proxy = null;
                response = (HttpWebResponse)request.GetResponse();
                //方式一
                using (Stream s = response.GetResponseStream())
                {
                    int nOffset = 0;
                    int bufferLen = 2048;
                    byte[] buffer = new byte[bufferLen];
                    long maxOffset = _bytesDownloaded;
                    int reciveByteCount = s.Read(buffer, 0, bufferLen);
                    while (reciveByteCount > 0 && !_cancelDownload)
                    {
                        using (FileStream fs = new FileStream(_localAbsPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                        {
                            fs.Position = maxOffset;
                            fs.Write(buffer, 0, reciveByteCount);
                            fs.Close();
                        }
                        nOffset += reciveByteCount;
                        maxOffset += reciveByteCount;
                        reciveByteCount = s.Read(buffer, 0, bufferLen);
                        _progress.SafeInvoke(((float)maxOffset / _totalFileSize), maxOffset, _totalFileSize);
                        //DownloadProgress.instance.UpdateProgress(((float)maxOffset) / totalFileSize);
                    }
                }

                //下载完成
                response.Close();
                request.Abort();
                response = null;
                request = null;

                long fileSize = 0;
                if (File.Exists(_localAbsPath))
                {
                    using (FileStream fs = new FileStream(_localAbsPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        fileSize = fs.Length;
                    }

                    if (fileSize == _totalFileSize)
                    {//判断下载到本地的文件是否与服务器文件大小一样 
                        LoggerHelper.Info(string.Format("[断点续传] 文件:{0}下载[OK],下载字节:{1},总字节:{2}", _url, _needDownloadSize, _totalFileSize));
                        //不能在子线程中调用主线程方法onCompleted，因此延时50毫秒再在主线程回调onCompleted,切记
                        LoaderDriver.Invoke(_onCompleted.SafeInvoke, _url);
                        return;
                    }
                }
                LoggerHelper.Info(string.Format("[断点续传] 文件:{0}下载[Fail],本地文件字节:{1},总字节:{2}", _url, fileSize, _totalFileSize));
                LoaderDriver.Invoke(_onFail.SafeInvoke, _url, "file size not match.");
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(string.Format("文件:{0}下载[Fail],reason:{1}", _url, ex.StackTrace));
                LoaderDriver.Invoke(_onFail.SafeInvoke, _url, ex.Message);
            }
            finally
            {
                if (response != null) response.Close();
                if (request != null) request.Abort();
                response = null;
                request = null;
            }
        }

        public void HandlerError(string msg, Action confirmCallback)
        {
            LoggerHelper.Info("[断点续传] 失败：" + msg);
            var errorText = "";
            if (msg.Contains("ConnectFailure") || msg.Contains("NameResolutionFailure") || msg.Contains("No route to host"))
            {//连接失败||域名解析失败||找不到主机，则弹出网络设置对话框
             //ShowConfirm("Network anomaly " + msg);
                errorText = "无法下载，请稍后重试！";
            }
            else if (msg.Contains("(404) Not Found") || msg.Contains("403"))
            {//服务器维护中，请稍后再试 
             //ShowConfirm("Server maintenance,please try again later " + msg);
                errorText = "服务器维护中，请稍后重试!";
            }
            else if (msg.Contains("Disk full"))
            {//磁盘已满 
             //ShowConfirm("Hard disk is full " + msg);
                errorText = "您的磁盘已满，请清理磁盘后再重试！";
            }
            else if (msg.Contains("timed out") || msg.Contains("Error getting response stream"))
            {//下载超时 
             //ShowConfirm("Download timeout " + msg);
                errorText = "下载超时，请稍后重试！";
            }
            else if (msg.Contains("Sharing violation on path"))
            {//路径共享违规
             //ShowConfirm("Path Sharing Violation " + msg);
                errorText = "路径共享违规，请稍后重试！";
            }
            else
            {//未知错误 
             //ShowConfirm("Unknown Error " + msg);
                errorText = "下载出现未知情况，请稍后重试！";
            }
            ShowConfirm(errorText, confirmCallback);
        }

        //确认框
        private void ShowConfirm(string msg, Action confirmCallback)
        {
            LoaderDriver.Invoke(() =>
            {
                ConfirmBox.Instance.Show(msg, (result) =>
                {
                    if (result)
                    {
                        confirmCallback.SafeInvoke();
                    }
                    else Application.Quit();
                }, DefaultLanguage.GetString("try"), DefaultLanguage.GetString("exit"));//isSetNetwork ? LoaderDriver.DefaultLanguage.GetString("setNetwork") :
            });
        }

        //private void OnConfirm(bool result)
        //{
        //}

        //private void onCompleted2()
        //{
        //    if (onCompleted != null) onCompleted(url);
        //}

        //private void onFail2()
        //{
        //    StopThread();
        //    if (onFail != null) onFail(url);
        //}

        private void Clear()
        {
            _uri = null;
            _url = null;
            _thread = null;
            _onFail = null;
            _onCompleted = null;
            _localAbsPath = null;
            _totalFileSize = 0;
            _bytesDownloaded = 0;
        }
    }
}
