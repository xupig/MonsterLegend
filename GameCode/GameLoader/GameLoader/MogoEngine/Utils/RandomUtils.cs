/*==========================================
// Copyright (C) 2016 广州，雷神
//
// 模块名：RandomHelper
// 创建者：Key Pan
// 修改者列表：Key Pan
// 创建日期：20160227
// 最后修改日期：20130228
// 模块描述：各种随机数
// 代码版本：测试版V1.1
//==========================================*/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace GameLoader.Utils
{
    public class RandomUtils
    {
        private static System.Random globalRandomGenerator = CreateRandom();

        /// <summary>
        /// 创建一个产生不重复随机数的随机数生成器。
        /// </summary>
        /// <returns>随机数生成器</returns>
        public static System.Random CreateRandom()
        {
            long tick = DateTime.Now.Ticks;
            return new System.Random((int)(tick & 0xffffffffL) | (int)(tick >> 32));
        }

        public static void GenerateNewRandomGenerator()
        {
            globalRandomGenerator = CreateRandom();
        }

        /// <summary>
        /// 在一个范围内生成非重复随机序列
        /// </summary>
        /// <param name="min">范围内最小值</param>
        /// <param name="max">范围内最大值</param>
        /// <returns>返回非重复随机数组</returns>
        public static int[] RandomSequence(int min, int max)
        {
            int x = 0;
            int temp = 0;
            if (min > max)
            {
                temp = min;
                min = max;
                max = temp;
            }
            int[] arr = new int[max - min + 1];
            for (int i = min; i <= max; ++i)
            {
                arr[i - min] = i;
            }
            for (int i = arr.Length - 1; i > 0; --i)
            {
                x = RandomUtils.GetRandomInt(0, i + 1);
                temp = arr[i];
                arr[i] = arr[x];
                arr[x] = temp;
            }
            return arr;
        }

        public static void RandomList(IList list, int start = -1, int end = -1)
        {
            if (start == -1)
            {
                start = 0;
            }
            if (end == -1)
            {
                end = list.Count;
            }
            for (int i = 0; i < list.Count * 2; i++)
            {
                int j = RandomUtils.GetRandomInt(start, end);
                int k = RandomUtils.GetRandomInt(start, end);
                if (j != k)
                {
                    var temp = list[j];
                    list[j] = list[k];
                    list[k] = temp;
                }
            }
        }

        public static T Choice<T>(List<T> list)
        {
            if (list.Count == 0)
            {
                return default(T);
            }

            int index = GetRandomInt(0, list.Count);
            return list[index];
        }

        /// <summary>
        /// 加权随机数，参数的长度为随机值的范围。
        /// </summary>
        /// <param name="weights">权重数组</param>
        /// <returns>随机结果</returns>
        public static int WeightedRandom(float[] weights)
        {
            if (weights.Length == 0)
            {
                return -1;
            }
            float totalWeight = 0;
            for (int i = 0; i < weights.Length; ++i)
            {
                totalWeight += weights[i];
            }
            float random = RandomUtils.GetRandomFloat(0, totalWeight);
            float sum = 0;
            for (int i = 0; i < weights.Length; ++i)
            {
                sum += weights[i];
                if (random <= sum)
                {
                    return i;
                }
            }
            return weights.Length - 1;
        }

        /// <summary>
        /// 加权随机数，参数的长度为随机值的范围。
        /// </summary>
        /// <param name="weights">权重数组</param>
        /// <returns>随机结果</returns>
        public static int WeightedRandom(int[] weights)
        {
            if (weights.Length == 0)
            {
                return -1;
            }
            int totalWeight = 0;
            for (int i = 0; i < weights.Length; ++i)
            {
                totalWeight += weights[i];
            }
            int random = RandomUtils.GetRandomInt(0, totalWeight + 1);
            int sum = 0;
            for (int i = 0; i < weights.Length; ++i)
            {
                sum += weights[i];
                if (random <= sum)
                {
                    return i;
                }
            }
            return weights.Length - 1;
        }

        public static Byte GetRandomByte()
        {
            Byte[] temp = new Byte[1];
            globalRandomGenerator.NextBytes(temp);
            return temp[0];
        }

        public static void GetRandomBytes(Byte[] bytes)
        {
            globalRandomGenerator.NextBytes(bytes);
        }

        public static Color GetRandomColor()
        {
            Byte[] temp = new Byte[4];
            globalRandomGenerator.NextBytes(temp);
            return new Color(temp[0], temp[1], temp[2], temp[3]);
        }

        public static Color GetRandomColor(int alpha)
        {
            Byte[] temp = new Byte[3];
            globalRandomGenerator.NextBytes(temp);

            if (alpha < 0)
                alpha = 0;
            if (alpha > 255)
                alpha = 255;

            return new Color(temp[0], temp[1], temp[2], alpha);
        }

        /// <summary>
        /// 获取一个0.0到1.0之间的随机数（float）
        /// </summary>
        /// <returns>随机数</returns>
        public static float GetRandomFloat()
        {
            return (float)globalRandomGenerator.NextDouble();
        }

        /// <summary>
        /// 获取一个0.0到指定最大值范围内的随机数（float）
        /// </summary>
        /// <param name="max">指定最大值</param>
        /// <returns>随机数</returns>
        public static float GetRandomFloat(float max)
        {
            return (float)globalRandomGenerator.NextDouble() * max;
        }

        /// <summary>
        /// 获取一个指定范围内的随机数（float）
        /// </summary>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <returns>随机数</returns>
        public static float GetRandomFloat(float min, float max)
        {
            if (min < max)
                return (float)globalRandomGenerator.NextDouble() * (max - min) + min;
            else
                return max;
        }

        /// <summary>
        /// 获取一个随机整数（）
        /// </summary>
        /// <param name="positive">是否只随机正整数</param>
        /// <returns></returns>
        public static int GetRandomInt(bool positive = true)
        {
            if (positive)
                return globalRandomGenerator.Next();
            else
            {
                return globalRandomGenerator.Next(int.MinValue, int.MaxValue);
            }
        }


        /// <summary>
        /// 获取一个0.0到指定最大值范围内的随机数（int）
        /// </summary>
        /// <param name="max">指定最大值</param>
        /// <returns>随机数</returns>
        public static int GetRandomInt(int max)
        {
            if (max > 0)
                return globalRandomGenerator.Next() % max;
            else if (max < 0)
                return -globalRandomGenerator.Next() % max;
            else
                return 0;
        }


        /// <summary>
        /// 获取一个指定范围内的随机数（int）
        /// </summary>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <returns>随机数</returns>
        public static int GetRandomInt(int min, int max)
        {
            if (min < max)
                return globalRandomGenerator.Next(min, max);
            else
                return max;
        }


        public static Vector3 GetRandomNormalVector3()
        {
            return new Vector3(GetRandomInt(false),
                GetRandomInt(false),
                GetRandomInt(false)).normalized;
        }

        public static Vector3 GetRandomVector3()
        {
            return new Vector3(GetRandomInt(false) * GetRandomFloat(),
                GetRandomInt(false) * GetRandomFloat(),
                GetRandomInt(false) * GetRandomFloat());
        }

        public static Vector3 GetRandomVector3(float xMax, float yMax = 0, float zMax = 0)
        {
            return new Vector3(GetRandomFloat(xMax), GetRandomFloat(yMax), GetRandomFloat(zMax));
        }

        public static Vector3 GetRandomVector3(float xMin, float xMax,
            float yMin, float yMax,
            float zMin, float zMax)
        {
            return new Vector3(GetRandomFloat(xMin, xMax),
                GetRandomFloat(yMin, yMax),
                GetRandomFloat(zMin, zMax));
        }

        public static Vector2 GetRandomVector2()
        {
            return new Vector2(GetRandomInt(false) * GetRandomFloat(),
                GetRandomInt(false) * GetRandomFloat());
        }

        public static Vector2 GetRandomVector2(float xMax, float yMax = 0)
        {
            return new Vector3(GetRandomFloat(xMax), GetRandomFloat(yMax));
        }

        public static Vector2 GetRandomVector2(float xMin, float xMax,
            float yMin, float yMax)
        {
            return new Vector3(GetRandomFloat(xMin, xMax),
                GetRandomFloat(yMin, yMax));
        }

        public static Vector3 GetRandomVector3InRangeCircle(float range, float y = 0)
        {
            float length = GetRandomFloat(0, range);
            float angle = GetRandomFloat(0, 360);
            return new Vector3((float)(length * Math.Sin(angle * (Math.PI / 180))),
                y,
                (float)(length * Math.Cos(angle * (Math.PI / 180))));
        }

        public static bool GetRandomBoolean()
        {
            if (GetRandomInt(2) == 0)
                return false;
            return true;
        }
    }
}
