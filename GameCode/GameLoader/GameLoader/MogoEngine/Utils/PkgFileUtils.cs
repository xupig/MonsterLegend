﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using GameLoader.Config;
using GameLoader.IO;
using ICSharpCode.SharpZipLib.Zip;

namespace GameLoader.Utils
{
    /// <summary>
    /// pkg文件解、加压缩工具类
    /// </summary>
    public static class PkgFileUtils
    {
        #region zip解压缩处理
        public static void CompressDirectory(string sourcePath, string outputFilePath, int zipLevel = 0, bool password = true)
        {
            FileStream compressed = new FileStream(outputFilePath, FileMode.OpenOrCreate);
            compressed.CompressDirectory(sourcePath, zipLevel, password);
        }

        public static void DecompressToDirectory(string targetPath, string zipFilePath, Action<long> progress, bool isOverWriteFile = true)
        {
            if (File.Exists(zipFilePath))
            {
                var compressed = File.OpenRead(zipFilePath);
                compressed.DecompressToDirectory(targetPath, progress, isOverWriteFile);
            }
            else
            {
                LoggerHelper.Error("Zip不存在: " + zipFilePath);
            }
        }

        public static void DecompressToMogoFile(string zipFilePath)
        {
            if (File.Exists(zipFilePath))
            {
                FileStream source = File.OpenRead(zipFilePath);
                LoggerHelper.Info(string.Format("生成pkg文件前zip文件：{0},字节数：{1}", zipFilePath, source.Length));
                MogoFileSystem.Instance.Open();
                MogoFileSystem.Instance.GetAndBackUpIndexInfo();
                using (ZipInputStream decompressor = new ZipInputStream(source))
                {
                    ZipEntry entry;
                    decompressor.Password = KeyUtils.GetResNumber().PackArray();
                    while ((entry = decompressor.GetNextEntry()) != null)
                    {
                        if (entry.IsDirectory) continue;
                        string filePath = entry.Name;
                        if (String.IsNullOrEmpty(filePath)) continue;

                        IndexInfo info = MogoFileSystem.Instance.BeginSaveFile(filePath, entry.Size);

                        byte[] data = new byte[2048];
                        int bytesRead;
                        while ((bytesRead = decompressor.Read(data, 0, data.Length)) > 0)
                        {
                            MogoFileSystem.Instance.WriteFile(info, data, 0, bytesRead);
                        }
                        MogoFileSystem.Instance.EndSaveFile(info);
                        LoggerHelper.Debug("        " + info.ToString());
                    }
                }
                MogoFileSystem.Instance.SaveIndexInfo();
                MogoFileSystem.Instance.CleanBackUpIndex();
                MogoFileSystem.Instance.Close();
            }
            else
            {
                LoggerHelper.Error("Zip file not exist: " + zipFilePath);
            }
        }

        public static void DecompressToMogoFileAndDirectory(string targetPath, string zipFilePath, Action<long> progress)
        {
            if (File.Exists(zipFilePath))
            {
                LoggerHelper.Info(string.Format("[DecompressToMogoFileAndDirectory] targetPath:{0},fileFullName:{1}", targetPath, MogoFileSystem.Instance.FileFullName));
                FileStream source = File.OpenRead(zipFilePath);
                MogoFileSystem.Instance.Open();
                MogoFileSystem.Instance.GetAndBackUpIndexInfo();
                using (ZipInputStream decompressor = new ZipInputStream(source))
                {
                    ZipEntry entry;
                    decompressor.Password = KeyUtils.GetResNumber().PackArray();
                    long counter = 0;
                    while ((entry = decompressor.GetNextEntry()) != null)
                    {
                        counter++;
                        if (entry.IsDirectory) continue;
                        string filePath = entry.Name;
                        if (String.IsNullOrEmpty(filePath)) continue;
                        byte[] data = new byte[2048];
                        int bytesRead;

                        //跳过对GameLoader.dll更新
                        if (filePath.IndexOf(ConstString.GAME_LOADER_DLL) != -1)
                        {
                            //SystemConfig.IsUpdateGameLoader = true;
                            LoggerHelper.Info(string.Format("[自更新] GameLoader.dll有更新将跳过,路径:{0}", filePath));
                            continue;
                        }

                        if (filePath.EndsWith(ConstString.U_SUFFIX) || filePath.EndsWith(ConstString.MK_SUFFIX))
                        {//.u文件||.mk文件
                            string outputPath = Path.Combine(targetPath, filePath);
                            string directoryPath = Path.GetDirectoryName(outputPath);
                            //LoggerHelper.Info(string.Format("[解压.u文件] filePath:{0},outputPath:{1}", filePath, outputPath));

                            if (!string.IsNullOrEmpty(directoryPath) && !Directory.Exists(directoryPath)) Directory.CreateDirectory(directoryPath);
                            using (FileStream streamWriter = File.Create(outputPath))
                            {
                                while ((bytesRead = decompressor.Read(data, 0, data.Length)) > 0)
                                {
                                    streamWriter.Write(data, 0, bytesRead);
                                }
                            }
                        }
                        else
                        {
                            //LoggerHelper.Info(string.Format("[解压非.u文件] filePath:{0}", filePath));
                            IndexInfo info = MogoFileSystem.Instance.BeginSaveFile(filePath, entry.Size);
                            while ((bytesRead = decompressor.Read(data, 0, data.Length)) > 0)
                            {
                                MogoFileSystem.Instance.WriteFile(info, data, 0, bytesRead);
                            }
                            MogoFileSystem.Instance.EndSaveFile(info);
                        }
                        progress.SafeInvoke(counter);
                    }
                }
                MogoFileSystem.Instance.SaveIndexInfo();
                MogoFileSystem.Instance.CleanBackUpIndex();
                MogoFileSystem.Instance.Close();
            }
            else
            {
                LoggerHelper.Error("Zip file not exist: " + zipFilePath);
            }
        }

        public static void CompressDirectory(this Stream target, string sourcePath, int zipLevel, bool password = true)
        {
#if !UNITY_WEBPLAYER
            sourcePath = Path.GetFullPath(sourcePath);
            //string parentDirectory = Path.GetDirectoryName(sourcePath);
            int trimOffset = (string.IsNullOrEmpty(sourcePath) ? Path.GetPathRoot(sourcePath).Length : sourcePath.Length);
            List<string> fileSystemEntries = new List<string>();
            fileSystemEntries.AddRange(Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories).Select(d => d + "\\"));
            fileSystemEntries.AddRange(Directory.GetFiles(sourcePath, "*", SearchOption.AllDirectories));

            using (ZipOutputStream compressor = new ZipOutputStream(target))
            {
                compressor.SetLevel(zipLevel);
                if (password) compressor.Password = KeyUtils.GetResNumber().PackArray();

                foreach (string filePath in fileSystemEntries)
                {
                    var trimFile = filePath.Substring(trimOffset);
                    var file = trimFile.StartsWith(@"\") ? trimFile.ReplaceFirst(@"\", "") : trimFile;
                    file = file.Replace(@"\", "/");
                    compressor.PutNextEntry(new ZipEntry(file));

                    if (filePath.EndsWith(@"\"))
                    {
                        continue;
                    }

                    byte[] data = new byte[2048];

                    using (FileStream input = File.OpenRead(filePath))
                    {
                        int bytesRead;

                        while ((bytesRead = input.Read(data, 0, data.Length)) > 0)
                        {
                            compressor.Write(data, 0, bytesRead);
                        }
                    }
                }

                compressor.Finish();
            }
#endif
        }

        public static void DecompressToDirectory(this Stream source, string targetPath, Action<long> progress, bool isOverWriteFile)
        {
            targetPath = Path.GetFullPath(targetPath);
            using (ZipInputStream decompressor = new ZipInputStream(source))
            {
                ZipEntry entry;
                decompressor.Password = KeyUtils.GetResNumber().PackArray();
                long counter = 0;
                while ((entry = decompressor.GetNextEntry()) != null)
                {
                    counter++;
                    string name = entry.Name;
                    if (entry.IsDirectory && entry.Name.StartsWith("\\"))
                        name = entry.Name.ReplaceFirst("\\", "");

                    string filePath = Path.Combine(targetPath, name);
                    string directoryPath = Path.GetDirectoryName(filePath);

                    if (!string.IsNullOrEmpty(directoryPath) && !Directory.Exists(directoryPath))
                        Directory.CreateDirectory(directoryPath);

                    if (entry.IsDirectory)
                        continue;

                    if (!isOverWriteFile && File.Exists(filePath))
                        continue;

                    byte[] data = new byte[2048];
                    using (FileStream streamWriter = File.Create(filePath))
                    {
                        int bytesRead;
                        while ((bytesRead = decompressor.Read(data, 0, data.Length)) > 0)
                        {
                            streamWriter.Write(data, 0, bytesRead);
                        }
                    }
                    progress.SafeInvoke(counter);
                }
            }
        }

        /// <summary>
        /// Create a zip archive.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="directory">The directory to zip.</param> 
        public static void PackFiles(string filename, string directory, string fileFilter)
        {
            try
            {
                FastZip fz = new FastZip();
                fz.CreateEmptyDirectories = true;
                fz.CreateZip(filename, directory, false, fileFilter);
                fz = null;
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        public static byte[] UnpackMemory(byte[] zipMemory)
        {
            MemoryStream stream = new MemoryStream(zipMemory);
            stream.Seek(0, SeekOrigin.Begin);
            ZipInputStream s = new ZipInputStream(stream);

            ZipEntry theEntry;
            List<byte> result = new List<byte>();
            while ((theEntry = s.GetNextEntry()) != null)
            {
                string fileName = Path.GetFileName(theEntry.Name);

                if (fileName != String.Empty)
                {
                    int size = 2048;
                    byte[] data = new byte[2048];
                    while (true)
                    {
                        size = s.Read(data, 0, data.Length);
                        if (size > 0)
                        {
                            var bytes = new Byte[size];
                            Array.Copy(data, bytes, size);
                            result.AddRange(bytes);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            s.Close();
            stream.Close();
            return result.ToArray();
        }

        /// <summary>
        /// Unpacks the files.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>if succeed return true,otherwise false.</returns>
        public static bool UnpackFiles(string file, string dir)
        {
            try
            {
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);

                ZipInputStream s = new ZipInputStream(File.OpenRead(file));

                ZipEntry theEntry;
                while ((theEntry = s.GetNextEntry()) != null)
                {

                    string directoryName = Path.GetDirectoryName(theEntry.Name);
                    string fileName = Path.GetFileName(theEntry.Name);

                    if (directoryName != String.Empty)
                        Directory.CreateDirectory(dir + directoryName);

                    if (fileName != String.Empty)
                    {
                        FileStream streamWriter = File.Create(dir + theEntry.Name);

                        int size = 2048;
                        byte[] data = new byte[2048];
                        while (true)
                        {
                            size = s.Read(data, 0, data.Length);
                            if (size > 0)
                            {
                                streamWriter.Write(data, 0, size);
                            }
                            else
                            {
                                break;
                            }
                        }

                        streamWriter.Close();
                    }
                }
                s.Close();
                return true;
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
                return false;
            }
        }

        public static long GetEntityCount(string zipFilePath)
        {
            if (File.Exists(zipFilePath))
            {
                var compressed = File.OpenRead(zipFilePath);
                return compressed.GetEntityCount();
            }
            else
            {
                LoggerHelper.Error("Zip不存在: " + zipFilePath);
                return 0;
            }
        }

        public static long GetEntityCount(this Stream source)
        {
            using (ZipInputStream decompressor = new ZipInputStream(source))
            {
                ZipEntry entry;
                decompressor.Password = KeyUtils.GetResNumber().PackArray();
                long counter = 0;
                while ((entry = decompressor.GetNextEntry()) != null)
                {
                    counter++;
                }
                return counter;
            }
        }

        #endregion
    }
}
