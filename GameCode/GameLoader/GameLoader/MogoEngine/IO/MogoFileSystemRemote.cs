﻿#region 模块信息
/*----------------------------------------------------------------
// Copyright (C) 2016 广州，雷神
//
// 模块名：MogoFileSystem
// 创建者：Ash Tang
// 修改者列表：
// 创建日期：2016.6.15
// 模块描述：自定义文件系统。
//----------------------------------------------------------------*/
#endregion

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using GameLoader.Config;
using GameLoader.Utils;
using System.IO;

namespace GameLoader.IO
{
#if UNITY_WEBPLAYER
    /// <summary>
    /// 自定义文件系统。
    /// </summary>
    public class MogoFileSystemRemote : MogoFileSystem
    {
        #region 属性
        /// <summary>
        /// 内存读写处理流。
        /// </summary>
        private MemoryStream m_memoryStream;
        
        #endregion

        #region 公有方法

        public override void Init()
        {
            // 此模式不用操作
        }

        /// <summary>
        /// 打开文件读写。
        /// </summary>
        public override void Init(string fileName)
        {
            // 此模式不用操作
        }

        /// <summary>
        /// 初始化内存读写。
        /// </summary>
        public override void Init(byte[] data)
        {
            if (m_memoryStream == null)
            {
                m_memoryStream = new MemoryStream(data);
                GetIndexInfo(data.Length);
            }
        }

        /// <summary>
        /// 打开文件读写。
        /// </summary>
        public override void Open()
        {
            // 此模式不用操作
        }

        /// <summary>
        /// 保存索引信息。
        /// </summary>
        public override void SaveIndexInfo()
        {
            lock (m_locker)
            {
                uint indexPosition = (uint)m_memoryStream.Position;
                var indexInfos = new List<Byte>();
                foreach (var item in m_fileIndexes)
                {
                    indexInfos.AddRange(item.Value.GetEncodeData());
                }
                foreach (var item in m_deletedIndexes)
                {
                    indexInfos.AddRange(item.GetEncodeData());
                }
                var indexInfo = indexInfos.ToArray();
                if (indexInfo.Length > 0)
                    indexInfo = DESCrypto.Encrypt(indexInfo, m_number);//加密索引数据

                m_memoryStream.Position = m_packageInfo.IndexOffset;//将文件偏移重置为索引偏移起始位置
                m_memoryStream.Write(indexInfo, 0, indexInfo.Length);
                m_memoryStream.Flush();
                SavePackageInfo(m_memoryStream);
                m_memoryStream.Position = indexPosition;
            }
        }

        /// <summary>
        /// 获取并备份索引信息。
        /// </summary>
        public override void GetAndBackUpIndexInfo()
        {
            GetIndexInfo(m_memoryStream.Length, true);
        }

        /// <summary>
        /// 关闭文件读写。
        /// </summary>
        public override void Close()
        {
            if (m_memoryStream != null)
            {
                m_memoryStream.Close();
                m_memoryStream.Dispose();
                m_memoryStream = null;
                m_isClosed = true;
            }
        }

        /// <summary>
        /// 存储文件。
        /// </summary>
        /// <param name="fileFullName"></param>
        public override void SaveFile(string fileFullName)
        {
            // 此模式不用操作
        }

        private string PathNormalize(string path)
        {
            return path.Replace("\\", "/").ToLower();
        }

        public override List<String> GetFilesByDirectory(String path)
        {
            var result = new List<String>();
            path = path.PathNormalize();
            foreach (var item in m_fileIndexes)
            {
                if (item.Value.Path.PathNormalize() == path)
                    result.Add(item.Value.Path);
            }
            return result;
        }

        public override bool IsFileExist(String fileFullName)
        {
            fileFullName = PathNormalize(fileFullName); // fileFullName.PathNormalize();
            return m_fileIndexes.ContainsKey(fileFullName);
        }

        /// <summary>
        /// 获取文件。
        /// </summary>
        /// <param name="fileFullName"></param>
        /// <returns></returns>
        public override byte[] LoadFile(string fileFullName)
        {
            Open();
            Byte[] result;
            lock (m_locker)
            {
                fileFullName = fileFullName.PathNormalize();
                if (m_memoryStream != null && m_fileIndexes.ContainsKey(fileFullName))
                {
                    result = DoLoadFile(fileFullName);
                }
                else
                    result = null;
            }
            return result;
        }

        public override List<KeyValuePair<string, byte[]>> LoadFiles(List<KeyValuePair<string, string>> fileFullNames)
        {
            Open();
            List<KeyValuePair<string, byte[]>> result = new List<KeyValuePair<string, byte[]>>();
            lock (m_locker)
            {
                foreach (var fileFullName in fileFullNames)
                {
                    var path = fileFullName.Value.PathNormalize();
                    if (m_memoryStream != null && m_fileIndexes.ContainsKey(path))
                    {
                        var fileData = DoLoadFile(path);
                        result.Add(new KeyValuePair<string, byte[]>(fileFullName.Key, fileData));
                    }
                    else
                    {
                        LoggerHelper.Error("File not exist in MogoFileSystem: " + FileFullName);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 获取文本类型文件。
        /// </summary>
        /// <param name="fileFullName"></param>
        /// <returns></returns>
        public override String LoadTextFile(string fileFullName)
        {
            Open();
            var content = LoadFile(fileFullName);
            if (content != null)
                return System.Text.Encoding.UTF8.GetString(content);
            else
                return String.Empty;
        }

        /// <summary>
        /// 删除文件。
        /// </summary>
        /// <param name="fileFullName"></param>
        public override void DeleteFile(string fileFullName)
        {
            lock (m_locker)
            {
                fileFullName = PathNormalize(fileFullName); //fileFullName.PathNormalize();
                if (m_fileIndexes.ContainsKey(fileFullName))
                {
                    var info = m_fileIndexes[fileFullName];
                    info.Deleted = true;
                    info.Id = "";
                    m_fileIndexes.Remove(fileFullName);
                    m_deletedIndexes.Add(info);
                }
            }
        }

        /// <summary>
        /// 开始存储文件。
        /// </summary>
        /// <param name="fileFullName"></param>
        /// <param name="fileSize"></param>
        /// <returns></returns>
        public override IndexInfo BeginSaveFile(string fileFullName, long fileSize)
        {
            IndexInfo info;
            lock (m_locker)
            {
                fileFullName = PathNormalize(fileFullName);
                if (m_fileIndexes.ContainsKey(fileFullName))//文件已存在
                {
                    var fileInfo = m_fileIndexes[fileFullName];
                    if (fileSize > fileInfo.PageLength)//新文件大小超出预留页大小
                    {
                        DeleteFile(fileFullName);

                        info = new IndexInfo() { Id = fileFullName, FileName = fileInfo.FileName, Path = fileInfo.Path, Offset = m_packageInfo.IndexOffset, FileState = FileState.New };
                        m_memoryStream.Position = m_packageInfo.IndexOffset;
                        m_fileIndexes[fileFullName] = info;
                    }
                    else//新文件大小没有超出预留页大小
                    {
                        info = fileInfo;
                        info.Length = 0;
                        info.FileState = FileState.Modify;
                        m_memoryStream.Position = info.Offset;
                    }
                }
                else//文件不存在，在索引最后新建
                {
                    info = new IndexInfo()
                    {
                        Id = fileFullName,
                        FileName = Path.GetFileName(fileFullName),
                        Path = Path.GetDirectoryName(fileFullName),
                        Offset = m_packageInfo.IndexOffset,
                        FileState = FileState.New
                    };
                    m_memoryStream.Position = m_packageInfo.IndexOffset;
                    m_fileIndexes[fileFullName] = info;
                }
            }
            return info;
        }

        /// <summary>
        /// 写入文件信息。
        /// </summary>
        /// <param name="info"></param>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        public override void WriteFile(IndexInfo info, Byte[] buffer, int offset, int count)
        {
            info.Length += (uint)count;
            if (m_encodeData)
            {
                m_bitCryto.Reset();
                int len = buffer.Length;
                for (int i = 0; i < len; ++i)
                {
                    buffer[i] = m_bitCryto.Encode(buffer[i]);
                }
            }
            m_memoryStream.Write(buffer, offset, count);
        }

        /// <summary>
        /// 存储文件结束。
        /// </summary>
        /// <param name="info"></param>
        public override void EndSaveFile(IndexInfo info)
        {
            lock (m_locker)
            {
                var leftSize = info.Length % m_pageSize;
                if (leftSize != 0)
                {
                    var empty = m_pageSize - leftSize;
                    var emptyData = new Byte[empty];
                    m_memoryStream.Write(emptyData, 0, emptyData.Length);
                    info.PageLength = info.Length + empty;
                }
                else
                    info.PageLength = info.Length;

                m_memoryStream.Flush();
                if (info.FileState == FileState.New)//修改状态不用修改索引偏移
                    m_packageInfo.IndexOffset = (uint)m_memoryStream.Position;
            }
        }

        public override void CleanBackUpIndex()
        {
            // 此模式不用操作
        }

        /// <summary>
        /// 此接口只在打包資源時使用，解決連續生產FirstPkg文件時前後生產文件大小出現變化的問題。
        /// </summary>
        public override void Clear()
        {
            //UnityEngine.Debug.Log("==== 清除MogoFileSystem上次緩存信息");
            m_fileIndexes.Clear();
            m_deletedIndexes.Clear();
            m_packageInfo.IndexOffset = 0;
            Close();
            UnityEngine.Caching.CleanCache();
        }

        #endregion

        #region 私有方法

        private byte[] DoLoadFile(string fileFullName)
        {
            var info = m_fileIndexes[fileFullName];
            Byte[] result = new Byte[info.Length];
            m_memoryStream.Position = info.Offset;
            m_memoryStream.Read(result, 0, result.Length);
            if (m_encodeData)
            {
                m_bitCryto.Reset();
                int len = result.Length;
                for (int i = 0; i < len; ++i)
                {
                    result[i] = m_bitCryto.Decode(result[i]);
                }
            }
            return result;
        }

        /// <summary>
        /// 获取索引信息。
        /// </summary>
        /// <param name="fileSize">文件包大小。</param>
        private void GetIndexInfo(long fileSize)
        {
            var useBackUp = CheckBackUpIndex();
            if (!useBackUp)
                GetIndexInfo(fileSize, false);
        }

        /// <summary>
        /// 获取索引信息。
        /// </summary>
        /// <param name="fileSize">文件包大小。</param>
        /// <param name="needBackUpIndex">是否备份索引信息。</param>
        private void GetIndexInfo(long fileSize, bool needBackUpIndex)
        {
            lock (m_locker)
            {
                m_packageInfo = GetPackageInfo(m_memoryStream, fileSize);
                if (m_packageInfo == null || m_packageInfo.IndexOffset == 0)
                    return;
                var indexSize = (int)(fileSize - PackageInfo.GetPackageSize() - m_packageInfo.IndexOffset);//计算索引信息大小
                var indexData = new Byte[indexSize];
                m_memoryStream.Position = m_packageInfo.IndexOffset;
                m_memoryStream.Read(indexData, 0, indexSize);//获取索引信息
                LoadIndexInfo(indexData, needBackUpIndex);
            }
        }

        private bool CheckBackUpIndex()
        {
            // 此模式不用操作
            return false;
        }

        private void LoadIndexInfo(Byte[] indexData, bool needBackUpIndex)
        {
            if (indexData.Length > 0)
            {
                if (needBackUpIndex)
                    BackupIndexInfo(indexData);
                indexData = DESCrypto.Decrypt(indexData, m_number);//解密索引数据
            }

            m_deletedIndexes.Clear();
            m_fileIndexes.Clear();
            var index = 0;
            while (index < indexData.Length)
            {
                var info = IndexInfo.Decode(indexData, ref index);
                if (info.Deleted)
                    m_deletedIndexes.Add(info);
                else
                    m_fileIndexes[PathNormalize(info.Id)] = info;
            }

            m_memoryStream.Position = m_packageInfo.IndexOffset;//重置回文件信息结尾处
        }

        private void BackupIndexInfo(byte[] indexData)
        {
            // 此模式不用操作
        }

        /// <summary>
        /// 获取包信息。
        /// </summary>
        /// <param name="fileSize"></param>
        private PackageInfo GetPackageInfo(MemoryStream fileStream, long fileSize)
        {
            var packageInfo = new PackageInfo();
            if (fileSize < PackageInfo.GetPackageSize())
                return new PackageInfo();

            fileStream.Position = fileSize - PackageInfo.GetPackageSize();//索引信息起始位置存放在文件结尾处。
            var lengthData = new Byte[PackageInfo.GetPackageSize()];
            fileStream.Read(lengthData, 0, PackageInfo.GetPackageSize());
            var index = 0;
            packageInfo.IndexOffset = EncodeDecoder.DecodeUInt32(lengthData, ref index);//获取索引信息起始位置
            return packageInfo;
        }

        private void SavePackageInfo(MemoryStream fileStream)
        {
            var indexPositionData = EncodeDecoder.EncodeUInt32(m_packageInfo.IndexOffset);
            fileStream.Write(indexPositionData, 0, indexPositionData.Length);
            fileStream.Flush();
        }

        #endregion
    }
#endif
}
