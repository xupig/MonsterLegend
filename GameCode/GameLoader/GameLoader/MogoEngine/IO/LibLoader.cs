﻿using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GameLoader.IO
{
    class LibLoader
    {
        //资源路径
        public static string RES_PATH = "";
        //程序库路径
        public static string Dll_PATH = "";
        //库文件后缀
        public static string DLL_SUFFIX = "";
        public static byte[] loadLib(string libName)
        {
#if !UNITY_WEBPLAYER
            return FileUtils.LoadByteFile(Dll_PATH + libName + DLL_SUFFIX);
#else
            return null;
#endif
        }
    }
}
