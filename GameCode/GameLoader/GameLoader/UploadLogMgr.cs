﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.IO;
using GameLoader;
using GameLoader.Utils;

using System.Collections;
using GameLoader.IO;
using LitJson;

namespace GameLoader.Mgrs
{
    public class UploadLogMgr
    {
        private string m_contentHead = "0|";
        private string m_urlPath;

        private static UploadLogMgr s_instance = null;
        public static UploadLogMgr GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new UploadLogMgr();
            }
            return s_instance;
        }

        Dictionary<string, string> _logHead;
        public Dictionary<string, string> logHead
        {
            get { InitLogHead(); return _logHead; }
        }

        public string ContentHead
        {
            get
            {
                return m_contentHead;
            }

            set
            {
                m_contentHead = value;
            }
        }

        UploadLogMgr()
        {
            InitLogHead();
            UpdateUrl(SystemConfig.UploadYunYingLogServer);
        }

        void InitLogHead()
        {
            _logHead = new Dictionary<string, string>();
            var ip = Network.player.ipAddress;
            var deviceModel = UnityPropUtils.DeviceModel;
            var osname = UnityPropUtils.OperatingSystem;
            if (osname.IndexOf("/") > 0)
            {
                osname = osname.Split('/')[0];
            }
            var osversion = Environment.Version.ToString();
            var mac_addr = "unknow";

            var udid = "unknow";
            var app_channel = "unknow";
            var app_ver = "unknow";

            string network = "unknow network";
            if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork)
                network = "3G/4G";
            else if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
                network = "WIFI";
            var platform_tag = "bingniao";
            var groupid = "3"; //IOS为1，android为2，越狱为3
            var channelId = "unknow";

            if (UnityPropUtils.Platform == RuntimePlatform.Android)
            {
                groupid = "2";
                osversion = UnityPropUtils.OperatingSystem;
                mac_addr = UnityPropUtils.DeviceUniqueIdentifier;
                udid = GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GetUserId();
                app_channel = GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GetChannelId();
                app_ver = GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GetGameId();
                channelId = GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GetChannelId();
            }
            else if (UnityPropUtils.Platform == RuntimePlatform.IPhonePlayer)
            {
                groupid = "1";
                osversion = UnityPropUtils.OperatingSystem;
                mac_addr = UnityPropUtils.DeviceUniqueIdentifier;
                udid = GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GetUserId();
                app_channel = GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GetChannelId();
                app_ver = GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GetGameId();
                channelId = GameLoader.PlatformSdk.PlatformSdkMgr.Instance.GetChannelId();
            }

            _logHead.Add("ip", ip);
            _logHead.Add("device_model", deviceModel);
            _logHead.Add("os_name", osname);
            _logHead.Add("os_ver", osversion);
            _logHead.Add("mac_addr", mac_addr);

            _logHead.Add("udid", udid);
            _logHead.Add("app_channel", app_channel);
            _logHead.Add("app_ver", app_ver);

            _logHead.Add("network", network);
            _logHead.Add("platform_tag", platform_tag);
            _logHead.Add("group_id", groupid);
            _logHead.Add("channel_id", channelId);
        }

        public void UpdateUrl(string server)
        {
            m_urlPath = string.Format(SystemConfig.UploadYunYingLogUrl, server);
            LoggerHelper.Info("UploadLog m_urlPath: " + m_urlPath);
        }

        //log_type:1
        public void UploadLog(string[] data)
        {
            //LoggerHelper.Info("UploadLogMgr UploadLog");
            if (data.Length % 2 != 0)
            {
                LoggerHelper.Error("UploadLogMgr UploadLog:data.Length % 2 != 0");
                return;
            }
            if (!string.IsNullOrEmpty(SystemConfig.UploadYunYingLogUrl))
            {
                Dictionary<string, string> logDict = new Dictionary<string, string>(_logHead);
                var length = data.Length / 2;
                for (int i = 0; i < length; i++)
                {
                    var key = data[i * 2];
                    var value = data[i * 2 + 1];
                    if (key == "activate_game_time") value = LoaderDriver.Instance.activeTime;
                    logDict[key] = value;
                }
                //LoggerHelper.Info(string.Format("<color=#00ffff>UploadLog: {0} </color> {1}", SystemConfig.UploadYunYingLogUrl, SystemConfig.UploadYunYingLogServer));
                string log = string.Concat(m_urlPath, m_contentHead, JsonMapper.ToJson(logDict));
                //LoggerHelper.Info(string.Format("<color=#00ffff>UploadLog: {0} </color>", log));
                //log = string.Format(log, SystemConfig.UploadYunYingLogServer);
                //log = SystemConfig.UploadYunYingLogUrl + "0|" + log;
                //LoggerHelper.Info(string.Format("<color=#00ffff>UploadLog: {0} </color>", log));
                HttpUtils.Get(log,
                    (resp) =>
                    {
                        //LoggerHelper.Info("UploadLogMgr UploadLog Resp:" + resp);
                    }, (statusCode) =>
                    {
                        LoggerHelper.Info("UploadLogMgr UploadLog statusCode:" + statusCode);
                    }
                );
            }
            //else
            //{
            //    LoggerHelper.Warning("UploadLog error UploadYunYingLogUrl is empty!");
            //}
        }
    }
}