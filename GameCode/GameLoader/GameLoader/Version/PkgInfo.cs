﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using GameLoader.Config;
using GameLoader.Defines;

namespace GameLoader.Version
{
    /// <summary>
    /// pkg包版本信息
    /// </summary>
    class PkgInfo
    {
        /// <summary>
        /// 该包的低版本号
        /// </summary>
        public VersionCodeInfo lowVersion;
        /// <summary>
        /// 该包的高版本号
        /// </summary>
        public VersionCodeInfo hightVersion;
        /// <summary>
        /// 该包的md5值
        /// </summary>
        public string md5;
        /// <summary>
        /// 文件名
        /// </summary>
        public string fileName;
        /// <summary>
        /// 高版本字符串
        /// </summary>
        public string strHightVersion;
        /// <summary>
        /// 补丁文件大小
        /// </summary>
        public long fileSize;
        /// <summary>
        /// 补丁包含文件数
        /// </summary>
        public long fileCount;

        /// <summary>
        /// 包信息节点，数据结构为：
        /// <p n="package0.0.0.1-0.0.0.2.pkg">6fd3555964fa0aa7f120f2caa79665cd</p> 
        /// </summary>
        /// <param name="node"></param>
        public PkgInfo(VersionCodeInfo lowVersion, VersionCodeInfo hightVersion, string md5, string fileName, string strHightVersion, long fileSize, long fileCount)
        {
            this.lowVersion = lowVersion;
            this.hightVersion = hightVersion;
            this.md5 = md5;
            this.fileName = fileName;
            this.strHightVersion = strHightVersion;
            this.fileSize = fileSize;
            if (fileCount > 0)
                this.fileCount = fileCount;
            else
                this.fileCount = 100;//如果没有文件数量，只能随便给个值
        }

        /// <summary>
        /// 取得远程加载地址
        /// </summary>
        public string httpUrl { get { return string.Concat(SystemConfig.PackageUrl, fileName); } }

        /// <summary>
        /// 取得本地保存路径
        /// </summary>
        public string localSavePath { get { return string.Concat(SystemConfig.RuntimeResourcePath, "package_temp_dir/", fileName); } }
    }
}
