﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameLoader.Version
{
    /// <summary>
    /// StreamingAssets资源列表
    /// </summary>
    public class AssetList
    {
        public readonly Dictionary<string, int> assetMemoryDict;
        //public readonly Dictionary<string, List<string>> assetDependenceDict;

        public AssetList()
        {
            assetMemoryDict = new Dictionary<string, int>();
            //assetDependenceDict = new Dictionary<string, List<string>>();
        }
    }

}
