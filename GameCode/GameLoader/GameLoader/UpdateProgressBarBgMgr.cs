﻿using GameLoader.Defines;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using System;
using System.IO;
using UnityEngine;

namespace GameLoader.Mgrs
{
    /// <summary>
    /// 进度条背景版本管理器
    /// </summary>
    class UpdateProgressBarBgMgr
    {
        private VersionInfo localVersion;
        private string runtimeResourceFolder;
        private int m_downloadFailCounter;
        private Action<string, byte[], string> CheckPackageMd5Action;
        public static bool ProgressBarDownloading = false;

        public UpdateProgressBarBgMgr(VersionInfo localVersion)
        {
            CheckPackageMd5Action = CheckPackageMd5;
            this.localVersion = localVersion;
        }

        /// <summary>
        /// 更新背景图
        /// </summary>
        /// <param name="runtimeResourceFolder"></param>
        public void Update(string runtimeResourceFolder)
        {
            if (string.IsNullOrEmpty(SystemConfig.ProgressBarBgVersion)) return;
            this.runtimeResourceFolder = runtimeResourceFolder;

            VersionCodeInfo serverVersion = new VersionCodeInfo(SystemConfig.ProgressBarBgVersion);
            bool result = localVersion.CompareProgressBarBgVersion(serverVersion);
            if (result)
            {
                DownloadProgressBar();
                //string localSavePath = string.Concat(runtimeResourceFolder, "ProgressBar/ProgressBarBg.png.u");
                //string url = TextDownloadMgr.GetRandomParasUrl();
                //LoggerHelper.Info("ProgressBarBgUrl:" + url);
                //LoggerHelper.Info("localSavePath:" + localSavePath);
                //ResLoader.LoadWwwBytes(url, localSavePath, OnLoaded);
            }
        }

        public void DownloadProgressBar()
        {
            var url = SystemConfig.GetValueInCfg("ProgressBarBgUrl");
            if (!string.IsNullOrEmpty(url))
            {
                ProgressBarDownloading = true;
                url = string.Concat(url, SystemConfig.ProgressBarBgVersion, ".pkg");
                if (SystemConfig.IsDownloadFromSource)
                    url = TextDownloadMgr.GetRandomParasUrl(url);
                HttpWrappr.instance.LoadWwwData(url, DownloadPackageFinish, DownloadPackageError, true);
            }
        }

        private void DownloadPackageError(string url, string error)
        {
            m_downloadFailCounter++;
            if (m_downloadFailCounter <= 5)
            {
                LoggerHelper.Info("DownloadPackageError: " + url + " error: " + error + " retry: " + m_downloadFailCounter);
            }
            else
            {
                DownloadProgressBar();
            }
        }

        private void DownloadPackageFinish(string url, byte[] bytes1, byte[] bytes2)
        {
            CheckPackageMd5Action.BeginInvoke(url, bytes1, SystemConfig.GetValueInCfg("ProgressBarBgMd5"), null, null);
        }

        private void CheckPackageMd5(string fileName, byte[] bytes, string targetMd5)
        {
            var curMd5 = MD5Utils.FormatMD5(MD5Utils.CreateMD5(bytes));
            if (targetMd5 == curMd5)
            {
                BeginDecompressPackage(bytes, fileName, targetMd5);
                //m_actionBeginDecompressPackage.BeginInvoke(filePath, url, targetMd5, null, null);
                return;
            }
            else
            {
                LoggerHelper.Error("DownloadPackage Error: md5 not match " + fileName + " targetMd5: " + targetMd5 + " curMd5: " + curMd5);
                TimerHeap.AddTimer(1000, 0, DownloadProgressBar);
            }
        }

        private void BeginDecompressPackage(byte[] data, string fileName, string targetMd5)
        {
            try
            {
                //Debug.LogWarning("BeginDecompressPackage: " + m_resourcePath + " " + filePath);
                MemoryStream ms = new MemoryStream(data);
                string localSavePath = string.Concat(runtimeResourceFolder, "ProgressBar/");
                ms.DecompressToDirectory(localSavePath, null, true);
                //File.Delete(filePath);
                //var fileName = Path.GetFileName(url);
                LoaderDriver.Invoke(DownloadPackageFinish);
            }
            catch (Exception ex)
            {
                LoggerHelper.Except(ex);
            }
        }

        //加载完成
        private void DownloadPackageFinish()
        {
            localVersion.ProgressBarBgVersion = new VersionCodeInfo(SystemConfig.ProgressBarBgVersion);
            localVersion.Save();
            LoggerHelper.Info("ProgressBarBg VersionInfo Has Write: " + SystemConfig.ProgressBarBgVersion);
            ProgressBarDownloading = false;
            TimerHeap.AddTimer(500, 0, LoadingBar.ProgressBar.Instance.LoadDefaultImage);
        }
    }
}
