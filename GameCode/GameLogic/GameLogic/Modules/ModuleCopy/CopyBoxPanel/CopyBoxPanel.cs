﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/13 13:58:06
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using UnityEngine;
using Common.Utils;

namespace ModuleCopy
{
    public class CopyBoxPanel : BasePanel
    {
        private int _chapterId;
        private int _starNum;
        private BoxState _boxState;

        private KList _list;
        private GameObject _goCanNotGet;
        private KButton _btnCanGet;
        private StateText _textStarNum;
        private KParticle _particle;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _goCanNotGet = GetChild("Button_weidacheng");
            _btnCanGet = GetChildComponent<KButton>("Button_lingqu");
            _textStarNum = GetChildComponent<StateText>("Label_txtStarNum");
            _particle = AddChildComponent<KParticle>("Button_lingqu/fx_ui_3_2_lingqu");
            AddChildComponent<CopyBackground>("Container_bg");
            InitList();
        }

        private void InitList()
        {
            _list = GetChildComponent<KList>("List_content");
            _list.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _list.SetGap(0, 27);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.CopyBox; }
        }

        public override void OnShow(object data)
        {
            AddListener();
            CopyProgressBoxDataWrapper wrapper = data as CopyProgressBoxDataWrapper;
            _chapterId = wrapper.ChapterId;
            _starNum = wrapper.StarNum;
            CopyData copyData = PlayerDataManager.Instance.CopyData;
            _boxState = copyData.GetBoxState(_chapterId, _starNum);
            Refresh();
        }

        public override void OnClose()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnCanGet.onClick.AddListener(OnClick);
            EventDispatcher.AddEventListener(CopyEvents.GotStarRewardListChange, RefreshBoxState);
            EventDispatcher.AddEventListener<int, int>(CopyEvents.GET_BOX_REWARD, GetBoxReward);
            
        }

        private void RemoveListener()
        {
            _btnCanGet.onClick.RemoveListener(OnClick);
            EventDispatcher.RemoveEventListener(CopyEvents.GotStarRewardListChange, RefreshBoxState);
            EventDispatcher.RemoveEventListener<int, int>(CopyEvents.GET_BOX_REWARD, GetBoxReward);
        }

        private void RefreshBoxState()
        {
            CopyData copyData = PlayerDataManager.Instance.CopyData;
            _boxState = copyData.GetBoxState(_chapterId, _starNum);
            Refresh();
        }

        private void OnClick()
        {
            MissionManager.Instance.RequsetGetStarSpiritReward(_chapterId, _starNum);
        }

        private void GetBoxReward(int chapterId, int starNum)
        {
            if ((_chapterId == chapterId)
                && (starNum == _starNum))
            {
                List<KList.KListItemBase> itemList = _list.ItemList;
                for (int i = 0; i < itemList.Count; i++)
                {
                    CopyBoxRewardGrid rewardGrid = itemList[i] as CopyBoxRewardGrid;
                    rewardGrid.DoTween();
                }
            }
        }

        private void Refresh()
        {
            RefreshReward();
            RefreshState();
        }

        private void RefreshState()
        {
            HideAllState();
            switch (_boxState)
            {
                case BoxState.CAN_GET:
                    _btnCanGet.Visible = true;
                    break;
                case BoxState.CANNOT_GET:
                    _goCanNotGet.SetActive(true);
                    break;
            }
            _textStarNum.CurrentText.text = _starNum.ToString();

        }

        private void HideAllState()
        {
            _btnCanGet.Visible = false;
            _goCanNotGet.SetActive(false);
        }

        //要求策划奖励不随机
        //客户端图标不用刷新
        private void RefreshReward()
        {
            List<CopyBoxDataWrapper> wrapperList = GetWrapperList();
            _list.RemoveAll();
            _list.SetDataList<CopyBoxRewardGrid>(wrapperList);
            MogoLayoutUtils.SetCenter(_list.gameObject);
        }

        private List<CopyBoxDataWrapper> GetWrapperList()
        {
            List<CopyBoxDataWrapper> wrapperList = new List<CopyBoxDataWrapper>();
            int rewardId = chapters_helper.GetStarRewardId(_chapterId, _starNum);
            item_reward itemReward = item_reward_helper.GetItemReward(rewardId);
            List<BaseItemData> baseItemDataList = item_reward_helper.GetItemDataList(itemReward, PlayerAvatar.Player.vocation);
            for (int i = 0; i < baseItemDataList.Count; i++)
            {
                CopyBoxDataWrapper wrapper = new CopyBoxDataWrapper(baseItemDataList[i], _boxState);
                wrapperList.Add(wrapper);
            }
            return wrapperList;
        }
    }
}
