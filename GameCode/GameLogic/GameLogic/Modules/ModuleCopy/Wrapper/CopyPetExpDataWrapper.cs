﻿using Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCopy
{
    public class CopyPetExpDataWrapper
    {
        public PetInfo PetInfo;
        public int AddExp;

        public CopyPetExpDataWrapper(PetInfo petInfo, int addExp)
        {
            this.PetInfo = petInfo;
            this.AddExp = addExp;
        }
    }
}
