﻿using Common.Data;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCopy
{
    public class CopySweepDataWrapper
    {
        public const int InvalidMissionId = -1;

        public int SweepNum;
        public List<List<BaseItemData>> NewList;
        public int StarNum;
        public int MissionId;

        public CopySweepDataWrapper(int sweepNum, List<List<BaseItemData>> list, int starNum, int missionId = InvalidMissionId)
        {
            this.SweepNum = sweepNum;
            this.NewList = list;
            this.StarNum = starNum;
            this.MissionId = missionId;
        }
    }
}

