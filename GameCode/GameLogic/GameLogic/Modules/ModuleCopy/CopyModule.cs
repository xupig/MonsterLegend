﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/10 16:56:34
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using MogoEngine.Events;
using Common.Events;
using GameData;
using UnityEngine;
using GameMain.GlobalManager;
using GameMain.Entities;
using Common.ServerConfig;

namespace ModuleCopy
{
    public class CopyModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            RegisterPopPanelBunchPanel(PanelIdEnum.CopyBox, PanelIdEnum.Copy);
            RegisterPopPanelBunchPanel(PanelIdEnum.CopyPreview, PanelIdEnum.Copy);
            RegisterPopPanelBunchPanel(PanelIdEnum.CopySweep, PanelIdEnum.Copy);

            AddPanel(PanelIdEnum.Copy, "Container_CopyPanel", MogoUILayer.LayerUIPanel, "ModuleCopy.CopyPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.CopyBox, "Container_CopyBoxPanel", MogoUILayer.LayerUIPopPanel, "ModuleCopy.CopyBoxPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.CopyPreview, "Container_CopyPreviewPanel", MogoUILayer.LayerUIPopPanel, "ModuleCopy.CopyPreviewPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.CopyResult, "Container_CopyResultPanel", MogoUILayer.LayerUIPanel, "ModuleCopy.CopyResultPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.CopySweep, "Container_CopySweepPanel", MogoUILayer.LayerUIPopPanel, "ModuleCopy.CopySweepNewPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.CopyTaskResult, "Container_TaskEffect", MogoUILayer.LayerUIToolTip, "ModuleCopy.CopyTaskResultPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);

            EventDispatcher.AddEventListener<int,int>(CopyEvents.SHOW_COPY, ShowCopy);
        }


        private void ShowCopy(int instanceId, int activityId)
        {
            ChapterType chapterType = instance_helper.GetChapterType(instanceId);
            switch (chapterType)
            {
                case ChapterType.Copy:
                    PanelIdEnum.Copy.Show(instanceId);
                    break;
                case ChapterType.Branch:
                    PanelIdEnum.BranchPreview.Show(instanceId);
                    break;
                case ChapterType.EveningActivity:
                    PanelIdEnum.PVEPreview.Show(new int[] { instanceId, activityId });
                    break;
                case ChapterType.GuildCopy:
                    PanelIdEnum.GuildInstance.Show(new int[] { instanceId, activityId });
                    break;
                case ChapterType.GuildBoss:
                    PanelIdEnum.GuildBoss.Show(new int[] { instanceId, activityId });
                    break;
            }
        }
    }
}
