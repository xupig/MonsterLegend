﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/17 15:30:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.UIComponent;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleCopy
{
    public class CopySweepTween : UIBehaviour
    {
        private Vector3 _initVec3;

        private TweenPosition _tween;
        public KComponentEvent onPlayOnceEnd = new KComponentEvent();

        protected override void Awake()
        {
            _initVec3 = transform.localPosition;
        }

        public void Refresh(int x)
        {
            gameObject.transform.localPosition = _initVec3;
            Vector3 vet3DragonEnd = new Vector3(_initVec3.x + x, _initVec3.y, _initVec3.z);
            _tween = TweenPosition.Begin(gameObject, 1f, vet3DragonEnd);
            _tween.method = UITweener.Method.BounceIn;
            _tween.onFinished = OnFinish;
        }

        public void Stop()
        {
            gameObject.transform.localPosition = _initVec3;
            _tween.enabled = false;
        }

        private void OnFinish(UITweener tween)
        {
            onPlayOnceEnd.Invoke();
            _tween.Reset();
        }

    }
}
