﻿using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public abstract class CopySweepBaseTween : KContainer
    {
        public uint DelayTime = 500;

        private int currentIndex;
        protected List<Action> actionList;

        protected CopySweepFinish _sweepFinish;
        protected StarList _starList;
        protected GameObject _goBottom;

        protected override void Awake()
        {
            _sweepFinish = GetChildComponent<CopySweepFinish>("Container_sweepFinish");
            _starList = GetChildComponent<StarList>("Container_bottom/Container_xingxing");
            _starList.SetStarData(3, "Container_star", "Image_xingxingIcon");
            _goBottom = GetChild("Container_bottom");

            InitActionList();
            AddListener();
        }

        private void InitActionList()
        {
            actionList = new List<Action>();
            actionList.Add(DoRewardTween);
            actionList.Add(Delay);
            actionList.Add(DoFinishTween);
            actionList.Add(DoStarTween);
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _sweepFinish.OnTweenFinish.AddListener(OnSweepFinishFinish);
        }

        private void RemoveListener()
        {
            _sweepFinish.OnTweenFinish.RemoveListener(OnSweepFinishFinish);
        }

        public void DoTween()
        {
            HideAll();
            currentIndex = 0;
            MoveNext();
        }

        private void Delay()
        {
            TimerHeap.AddTimer(DelayTime, 0, MoveNext);
        }

        protected virtual void HideAll()
        {
            _sweepFinish.Visible = false;
            _starList.Visible = false;
            _goBottom.SetActive(false);
        }

        protected void MoveNext()
        {
            if (currentIndex < actionList.Count)
            {
                int now = currentIndex;
                currentIndex++;
                actionList[now].Invoke();
            }
        }

        protected abstract void DoRewardTween();

        protected void DoFinishTween()
        {
            _sweepFinish.Visible = true;
            _sweepFinish.DoTween();
        }

        private void OnSweepFinishFinish()
        {
            MoveNext();
        }

        private void DoStarTween()
        {
            _sweepFinish.Visible = false;
            _starList.Visible = true;
            _goBottom.SetActive(true);
        }
    }
}
