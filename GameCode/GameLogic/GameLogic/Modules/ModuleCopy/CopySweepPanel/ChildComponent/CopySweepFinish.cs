﻿using Common.Base;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopySweepFinish : KContainer
    {
        public float StarTweenTime = 0.2f;
        public float FlyOutTime = 0.2f;
        public float FlyOutDelayTime = 0.5f;
        private Vector3 _initPosition;

        private StarList _starList;
        private List<Star> _listInStarList;
        
        public KComponentEvent OnTweenFinish = new KComponentEvent();

        protected override void Awake()
        {
            _starList = AddChildComponent<StarList>("Container_xingxing");
            _starList.SetStarData(3, "Container_star", "Image_xingxingIcon");
            _listInStarList = _starList.ContainerStarList;
            MogoLayoutUtils.SetMiddleCenter(gameObject, 2);
            _initPosition = transform.localPosition;
        }

        public void SetData(int starNum)
        {
            _starList.SetStarNum(starNum);
        }

        public void DoTween()
        {
            transform.localPosition = _initPosition;
            TweenScale tween = TweenScale.Begin(gameObject, StarTweenTime, Vector3.one);
            tween.from = new Vector3(3f, 3f, 1f);
            tween.onFinished = OnSweepFinishFinished;
        }

        private void OnSweepFinishFinished(UITweener noUse)
        {
            Vector3 tarPosition = _initPosition + new Vector3(0, UIManager.PANEL_HEIGHT, 0);
            TweenPosition tween = TweenPosition.Begin(gameObject, FlyOutTime, tarPosition);
            tween.delay = FlyOutDelayTime;
            tween.onFinished = OnFinished;
        }

        private void OnFinished(UITweener tween)
        {
            OnTweenFinish.Invoke();
        }
    }
}
