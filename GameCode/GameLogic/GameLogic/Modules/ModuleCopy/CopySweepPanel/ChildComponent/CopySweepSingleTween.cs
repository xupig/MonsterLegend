﻿using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopySweepSingleTween : CopySweepBaseTween
    {
        private KList _list;

        protected override void Awake()
        {
            base.Awake();

            _list = GetChildComponent<KList>("ScrollView_sweep/mask/content");
        }

        protected override void HideAll()
        {
            base.HideAll();
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                RewardItem rewardItem = _list.ItemList[i] as RewardItem;
                rewardItem.transform.localScale = Vector3.zero;
                rewardItem.SetDimensionDirty();
            }
        }

        protected override void DoRewardTween()
        {
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                KList.KListItemBase itemBase = _list.ItemList[i];
                TweenScale tween = TweenScale.Begin(itemBase.gameObject, ItemObtainPanel.ItemScaleTime, Vector3.one);
                tween.delay = i * ItemObtainPanel.ItemDelayTime;
                if (i == _list.ItemList.Count - 1)
                {
                    tween.onFinished = OnFinish;
                }
                else
                {
                    tween.onFinished = null;
                }
            }
        }

        private void OnFinish(UITweener tween)
        {
            MoveNext();
        }
    }
}
