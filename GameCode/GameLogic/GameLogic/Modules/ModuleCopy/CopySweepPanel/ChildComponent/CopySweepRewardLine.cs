﻿using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCopy
{
    public class CopySweepRewardLine : KList.KListItemBase
    {
        private KList _list;
        public KList List
        {
            get
            {
                return _list;
            }
        }

        protected override void Awake()
        {
            _list = GetChildComponent<KList>("List_singleReward");
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                List<BaseItemData> list = value as List<BaseItemData>;
                _list.SetDataList<RewardItem>(list);
            }
        }
    }
}
