﻿using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopySweepMultiTween : CopySweepBaseTween
    {
        private float ItemBaseHeight;

        private KScrollView _scrollView;
        private KList _list;
        private int _currentLine;


        protected override void Awake()
        {
            base.Awake();

            _scrollView = GetChildComponent<KScrollView>("ScrollView_sweep");
            _list = GetChildComponent<KList>("ScrollView_sweep/mask/content");
        }

        protected override void HideAll()
        {
            base.HideAll();

            //最多隐藏两行
            int hideLineNum = Mathf.Min(2, _list.ItemList.Count);
            for (int i = 0; i < hideLineNum; i++)
            {
                CopySweepRewardLine rewardLine = _list.ItemList[i] as CopySweepRewardLine;
                RectTransform rect = rewardLine.transform.GetComponent<RectTransform>();
                if(i == 0)
                {
                    ItemBaseHeight = rect.sizeDelta.y;
                }
                for (int j = 0; j < rewardLine.List.ItemList.Count; j++)
                {
                    RewardItem rewardItem = rewardLine.List.ItemList[j] as RewardItem;
                    rewardItem.SetDimensionDirty();
                    rewardItem.transform.localScale = Vector3.zero;
                }
            }
        }

        protected override void DoRewardTween()
        {
            _scrollView.ResetContentPosition();

            int hideLineNum = Mathf.Min(2, _list.ItemList.Count);
            int delayCount = 0;
            for (int i = 0; i < hideLineNum; i++)
            {
                CopySweepRewardLine rewardLine = _list.ItemList[i] as CopySweepRewardLine;
                for (int j = 0; j < rewardLine.List.ItemList.Count; j++)
                {
                    KList.KListItemBase itemBase = rewardLine.List.ItemList[j];
                    TweenScale tween = TweenScale.Begin(itemBase.gameObject, ItemObtainPanel.ItemScaleTime, Vector3.one);
                    tween.delay = delayCount * ItemObtainPanel.ItemDelayTime;
                    delayCount++;
                }
            }
            uint delayTime = (uint)(delayCount * (uint)(ItemObtainPanel.ItemDelayTime * 1000)) + (uint)(ItemObtainPanel.ItemScaleTime * 1000);
            TimerHeap.AddTimer(delayTime, 0, DragList);
        }

        private void DragList()
        {
            if (_list.ItemList.Count <= 2)
            {
                OnFinished();
                return;
            }
            _currentLine = 1;
            DragOne();
        }

        private void DragOne(UITweener noUse = null)
        {
            _currentLine++;
            Vector3 srcPosition = _list.transform.localPosition;
            Vector3 tarPosition = new Vector3(srcPosition.x, srcPosition.y + ItemBaseHeight + _list.TopToDownGap, srcPosition.z);
            TweenPosition tween = TweenPosition.Begin(_list.gameObject, ItemObtainPanel.ScrollViewDragTime, tarPosition);
            tween.method = UITweener.Method.Linear;
            tween.delay = 0.3f;
            if (_currentLine < _list.ItemList.Count - 1)
            {
                tween.onFinished = DragOne;
            }
            else
            {
                tween.onFinished = OnFinished;
            }
        }

        private void OnFinished(UITweener tween = null)
        {
            MoveNext();
        }
    }
}
