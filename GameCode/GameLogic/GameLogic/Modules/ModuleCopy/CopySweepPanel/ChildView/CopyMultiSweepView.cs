﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyMultiSweepView : KContainer
    {
        private Locater _locaterScrollView;
        private KList _multiList;
        private CopySweepFinish _sweepFinish;
        private StarList _starList;
        private KButton _btnClose;
        private Dictionary<int, GameObject> _numDict;
        private KParticle _particle;

        protected override void Awake()
        {
            _locaterScrollView = AddChildComponent<Locater>("ScrollView_sweep");
            InitMultiList();
            _sweepFinish = AddChildComponent<CopySweepFinish>("Container_sweepFinish");
            _starList = AddChildComponent<StarList>("Container_bottom/Container_xingxing");
            _starList.SetStarData(3, "Container_star", "Image_xingxingIcon");
            _btnClose = GetChildComponent<KButton>("Container_bottom/Button_queding");
            _numDict = new Dictionary<int,GameObject>();
            for(int i = 0;i < 5;i++)
            {
                _numDict.Add(i + 1, GetChild(string.Format("Container_title/Image_cishu0{0}", i + 1)));
            }
            _particle = AddChildComponent<KParticle>("Container_title/fx_ui_26_guangshuhuanrao_01");
            _particle.transform.localPosition = new Vector3(270f, 0f, 0f);
            AddListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(OnClose);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(OnClose);
        }

        private void OnClose()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.CopySweep);
        }

        private void InitMultiList()
        {
            _multiList = GetChildComponent<KList>("ScrollView_sweep/mask/content");
            _multiList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _multiList.SetGap(17, 37);
            _multiList.SetPadding(5, 0, 0, 0);
        }

        public void SetData(List<List<BaseItemData>> list, int starNum)
        {
            _sweepFinish.SetData(starNum);
            _starList.SetStarNum(starNum);
            foreach (KeyValuePair<int, GameObject> go in _numDict)
            {
                go.Value.SetActive(false);
            }
            _numDict[list.Count].SetActive(true);
            _multiList.RemoveAll();
            _multiList.SetDataList<CopySweepRewardLine>(list);
            Layout();
        }

        private void Layout()
        {
            for (int i = 0; i < _multiList.ItemList.Count; i++)
            {
                CopySweepRewardLine rewardLine = _multiList.ItemList[i] as CopySweepRewardLine;
                rewardLine.RecalculateSize();
                Locater locater = rewardLine.gameObject.AddComponent<Locater>();
                locater.X = (_locaterScrollView.Width - locater.Width) / 2f;
                for (int j = 0; j < rewardLine.List.ItemList.Count; j++)
                {
                    KList.KListItemBase itemBase = rewardLine.List.ItemList[j];
                    MogoLayoutUtils.SetMiddleCenter(itemBase.gameObject, 2);
                }
            }
        }
    }
}
