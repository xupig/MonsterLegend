﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopySingleSweepView : KContainer
    {
        private int _missionId;

        private Locater _locaterScrollView;
        private KList _list;
        private Locater _locaterList;
        private CopySweepFinish _sweepFinish;
        private StarList _starList;
        private KButton _btnClose1;
        private KButton _btnClose;
        private KButton _btnAgain;
        private KParticle _particle;

        protected override void Awake()
        {
            _locaterScrollView = AddChildComponent<Locater>("ScrollView_sweep");
            InitList();
            _sweepFinish = AddChildComponent<CopySweepFinish>("Container_sweepFinish");
            _starList = AddChildComponent<StarList>("Container_bottom/Container_xingxing");
            _starList.SetStarData(3, "Container_star", "Image_xingxingIcon");
            _btnClose1 = GetChildComponent<KButton>("Container_bottom/Button_queding1");
            _btnClose = GetChildComponent<KButton>("Container_bottom/Button_queding");
            _btnAgain = GetChildComponent<KButton>("Container_bottom/Button_yici");
            _particle = AddChildComponent<KParticle>("Container_title/fx_ui_26_guangshuhuanrao_01");
            _particle.transform.localPosition = new Vector3(270f, 0f, 0f);
            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(OnClose);
            _btnClose1.onClick.AddListener(OnClose);
            _btnAgain.onClick.AddListener(OnAgain);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(OnClose);
            _btnClose1.onClick.RemoveListener(OnClose);
            _btnAgain.onClick.RemoveListener(OnAgain);
        }

        private void OnAgain()
        {
            if (DemonGateManager.Instance.isNormalInstance(_missionId))
            {
                DemonGateManager.Instance.RequestDemonGateSweep();
                return;
            }
            if (!PlayerDataManager.Instance.CopyData.CheckLeftSweepNum(instance_helper.GetChapterType(_missionId)))
            {
                return;
            }
            MissionManager.Instance.RequsetCleanUpMission(_missionId, 1);
        }

        private void OnClose()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.CopySweep);
        }

        private void InitList()
        {
            _locaterList = AddChildComponent<Locater>("ScrollView_sweep/mask/content");
            _list = GetChildComponent<KList>("ScrollView_sweep/mask/content");
            _list.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _list.SetGap(0, 37);
            _list.SetPadding(0, 0, 0, 0);
        }

        public void SetData(List<BaseItemData> baseItemDataList, int starNum, int missionId)
        {
            _missionId = missionId;
            _list.RemoveAll();
            _list.SetDataList<RewardItem>(baseItemDataList);
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                KList.KListItemBase itemBase = _list.ItemList[i];
                MogoLayoutUtils.SetMiddleCenter(itemBase.gameObject, 2);
            }
            _locaterList.X = (_locaterScrollView.Width - _locaterList.Width) / 2f;

            _sweepFinish.SetData(starNum);
            _starList.SetStarNum(starNum);
            RefreshBtn(missionId);
        }

        private void RefreshBtn(int missionId)
        {
            if (missionId == CopySweepDataWrapper.InvalidMissionId)
            {
                _btnClose1.Visible = true;
                _btnAgain.Visible = false;
                _btnClose.Visible = false;
            }
            else
            {
                _btnAgain.Visible = true;
                _btnClose.Visible = true;
                _btnClose1.Visible = false;
            }
        }
    }
}
