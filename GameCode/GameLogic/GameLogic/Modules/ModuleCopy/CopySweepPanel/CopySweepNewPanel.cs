﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopySweepNewPanel : BasePanel
    {
        private CopySweepDataWrapper _wrapper;

        private CopySingleSweepView _singleSweepView;
        private CopySweepSingleTween _singleTween;
        private CopyMultiSweepView _multiSweepView;
        private CopySweepMultiTween _multiTween;


        protected override void Awake()
        {
            _singleSweepView = AddChildComponent<CopySingleSweepView>("Container_single");
            _singleTween = AddChildComponent<CopySweepSingleTween>("Container_single");
            _multiSweepView = AddChildComponent<CopyMultiSweepView>("Container_multi");
            _multiTween = AddChildComponent<CopySweepMultiTween>("Container_multi");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
        }

        private void RemoveListener()
        {
        }

        public override void OnClose()
        {
        }

        protected override PanelIdEnum Id
        {
            get
            {
                return PanelIdEnum.CopySweep;
            }
        }

        public override void OnShow(object data)
        {
            _wrapper = data as CopySweepDataWrapper;
            Show();
            
        }

        public override void SetData(object data)
        {
            _wrapper = data as CopySweepDataWrapper;
            Show();
        }

        private void Show()
        {
            _singleSweepView.Visible = false;
            _multiSweepView.Visible = false;
            CopySweepBaseTween baseTween;
            if (_wrapper.SweepNum == 1)
            {
                _singleSweepView.Visible = true;
                _singleSweepView.SetData(_wrapper.NewList[0], _wrapper.StarNum, _wrapper.MissionId);
                baseTween = _singleTween;
            }
            else
            {
                _multiSweepView.Visible = true;
                _multiSweepView.SetData(_wrapper.NewList, _wrapper.StarNum);
                baseTween = _multiTween;
            }
            baseTween.DoTween();
        }
    }
}
