﻿using System.Collections.Generic;
using Game.UI.UIComponent;
using GameData;
using UnityEngine;
using Common.Data;

namespace ModuleCopy
{
    public class CopyChapterView : KList.KListItemBase
    {
        private const int CHAPTER_INSTANCE_NUM1 = 3;
        private const int CHAPTER_INSTANCE_NUM2 = 6;

        private Dictionary<int, CopyPath> _pathDict = new Dictionary<int,CopyPath>();
        private CopyChapterViewTemplate _template;

        protected override void Awake()
        {
            AddChildComponent<CopyBackground>("Container_bg");
            _template = GetTemplateGameObject().AddComponent<CopyChapterViewTemplate>();
        }

        private GameObject GetTemplateGameObject()
        {
            return transform.parent.FindChild("template").gameObject;
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                Refresh(value as CopyChapterData);
            }
        }

        private void Refresh(CopyChapterData chapterData)
        {
            HideAllPath();
            int index = GetPathIndex(chapterData.id);
            CopyPath copyPath = GetPath(index);
            copyPath.Visible = true;
            copyPath.Refresh(_template, chapterData);
        }

        private CopyPath GetPath(int index)
        {
            if (!_pathDict.ContainsKey(index))
            {
                string path = string.Format("Container_luxian{0}", index);
                GameObject goPath = _template.CloneGameObject(path, transform);
                Vector3 pathPosition = goPath.transform.localPosition;
                goPath.transform.localPosition = pathPosition + _template.transform.localPosition;
                CopyPath copyPath = goPath.AddComponent<CopyPath>();
                _pathDict.Add(index, copyPath);
            }
            return _pathDict[index];
        }

        private void HideAllPath()
        {
            foreach (CopyPath path in _pathDict.Values)
            {
                path.Visible = false;
            }
        }

        private int GetPathIndex(int id)
        {
            int instanceCount = chapters_helper.GetInstanceListCount(id);
            if(instanceCount == CHAPTER_INSTANCE_NUM1)
            {
                return 0;
            }
            else if(instanceCount == CHAPTER_INSTANCE_NUM2)
            {
                return 1;
            }
            else
            {
                return id % 2 == 1 ? 2 : 3;
            }
        }
    }
}