﻿using System.Collections.Generic;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using UnityEngine;
using Common.Utils;

namespace ModuleCopy
{
    public class CopyProgressView : KContainer
    {
        private const int CHAPTER_NUM_PER_SCROLLVIEW = 7;

        private float BOX_WIDTH;
        private float ITEM_HEIGHT;
        private float POP_BG_INIT_HEIGHT;
        private Vector3 SCROLL_VIEW_INIT_POSITION;

        private int _currentIndex;
        private List<CopyChapterData> _chapterDataList;
        private bool _isPopShow = false;

        private KButton _btnCurrentChapter;
        private StateText _textChapterName;
        private StateImage _imageUp;
        private StateImage _imageDown;

        private KProgressBar _progressBar;
        private RectTransform _rectProgressBar;
        private StateText _textProgress;

        private KList _boxList;
        private KContainer _containerPop;
        private KList _chapterNameList;
        private KDummyButton _hotArea;
        private StateImage _imagePopBg;
        private KScrollView _scrollView;
        private Locater _locaterScrollView;

        public KComponentEvent<int> onSelectChapter = new KComponentEvent<int>();

        protected override void Awake()
        {
            _btnCurrentChapter = GetChildComponent<KButton>("Button_fubenxuanze");
            _textChapterName = GetChildComponent<StateText>("Button_fubenxuanze/label");
            _imageUp = GetChildComponent<StateImage>("Button_fubenxuanze/up");
            _imageDown = GetChildComponent<StateImage>("Button_fubenxuanze/down");

            _progressBar = GetChildComponent<KProgressBar>("Container_jindu/ProgressBar_jindu");
            _rectProgressBar = _progressBar.GetComponent<RectTransform>();
            _textProgress = GetChildComponent<StateText>("Container_jindu/Label_txtJd");

            InitPop();
            InitBoxList();
        }

        private void InitPop()
        {
            _containerPop = GetChildComponent<KContainer>("Container_tanchu");
            _imagePopBg = GetChildComponent<StateImage>("Container_tanchu/ScaleImage_sharedKuangDT02");
            _imagePopBg.AddAllStateComponent<Resizer>();
            RectTransform rect = _imagePopBg.GetComponent<RectTransform>();
            rect.pivot = Vector2.zero;
            rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, rect.anchoredPosition.y - _imagePopBg.Height);
            POP_BG_INIT_HEIGHT = _imagePopBg.Height;
            _locaterScrollView = AddChildComponent<Locater>("Container_tanchu/ScrollView_fubenxuanze");
            SCROLL_VIEW_INIT_POSITION = _locaterScrollView.Position;
            InitPopList();
            _scrollView = GetChildComponent<KScrollView>("Container_tanchu/ScrollView_fubenxuanze");
            ITEM_HEIGHT = GetChildComponent<RectTransform>("Container_tanchu/ScrollView_fubenxuanze/mask/content/item").sizeDelta.y;
            _hotArea = AddChildComponent<KDummyButton>("Container_tanchu/Container_hotArea");
        }

        private void InitPopList()
        {
            _chapterNameList = GetChildComponent<KList>("Container_tanchu/ScrollView_fubenxuanze/mask/content");
            _chapterNameList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _chapterNameList.SetPadding(7, 0, 5, 0);
            _chapterNameList.SetGap(11, 0);
        }

        private void InitBoxList()
        {
            _boxList = GetChildComponent<KList>("List_xiangzi");
            _boxList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            BOX_WIDTH = GetChildComponent<RectTransform>("List_xiangzi/item").sizeDelta.x;
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnCurrentChapter.onClick.AddListener(OnChangePopVisible);
            _hotArea.onClick.AddListener(OnHidePop);
            _chapterNameList.onSelectedIndexChanged.AddListener(OnSelectIndexChanged);
            _chapterNameList.onAllItemCreated.AddListener(OnAllItemCreated);
            EventDispatcher.AddEventListener(CopyEvents.GotStarRewardListChange, Refresh);
        }

        private void RemoveListener()
        {
            _btnCurrentChapter.onClick.RemoveListener(OnChangePopVisible);
            _hotArea.onClick.RemoveListener(OnHidePop);
            _chapterNameList.onSelectedIndexChanged.RemoveListener(OnSelectIndexChanged);
            _chapterNameList.onAllItemCreated.RemoveListener(OnAllItemCreated);
            EventDispatcher.RemoveEventListener(CopyEvents.GotStarRewardListChange, Refresh);
        }

        public void SetData(List<CopyChapterData> chapterDataList, int index)
        {
            _chapterDataList = chapterDataList;
            _currentIndex = index;
            _chapterNameList.SelectedIndex = index;
            _isPopShow = false;
            Refresh();
        }

        private void OnChangePopVisible()
        {
            _isPopShow = !_isPopShow;
            RefreshPop();
            RefreshButtonImageUpAndDown();
        }

        private void OnHidePop()
        {
            _isPopShow = false;
            RefreshPop();
            RefreshButtonImageUpAndDown();
        }

        private void OnSelectIndexChanged(KList list, int index)
        {
            _isPopShow = false;
            onSelectChapter.Invoke(index);
        }

        private void Refresh()
        {
            CopyChapterData chapterData = _chapterDataList[_currentIndex];
            RefreshPopBtn(chapterData);
            RefreshButtonImageUpAndDown();
            RefreshProgress(chapterData);
            RefreshTreasureBox(chapterData);
            RefreshPop();
        }

        private void RefreshPopBtn(CopyChapterData chapterData)
        {
            _textChapterName.ChangeAllStateText(
                string.Format(MogoLanguageUtil.GetContent(49997), chapters_helper.GetPage(chapterData.id), chapters_helper.GetName(chapterData.id)));
        }

        private void RefreshButtonImageUpAndDown()
        {
            if (_isPopShow)
            {
                _imageDown.Visible = true;
                _imageUp.Visible = false;
            }
            else
            {
                _imageDown.Visible = false;
                _imageUp.Visible = true;
            }
        }

        private void RefreshProgress(CopyChapterData chapterData)
        {
            int totalStar = chapters_helper.GetTotalStar(chapterData.id);
            int nowStar = chapterData.TotalStarNum;
            _progressBar.Value = nowStar / (float) totalStar;
            _textProgress.CurrentText.text = string.Format("{0}/{1}", nowStar, totalStar);
        }

        private void RefreshTreasureBox(CopyChapterData chapterData)
        {
            List<chapters_helper.StarConfig> starConfigList = chapters_helper.GetStarConfigList(chapterData.id);
            int interval = (int)((_rectProgressBar.sizeDelta.x / starConfigList.Count) - BOX_WIDTH);
            _boxList.SetPadding(0, 0, 0, interval);
            _boxList.SetGap(0, interval);
            _boxList.SetDataList<CopyProgressBox>(starConfigList);
            //需要list布局完再缓动
            DoTween();
        }

        private void DoTween()
        {
            for (int i = 0; i < _boxList.ItemList.Count; i++)
            {
                CopyProgressBox progressBox = _boxList.ItemList[i] as CopyProgressBox;
                progressBox.DoTween();
            }
        }

        private void RefreshPop()
        {
            _containerPop.Visible = _isPopShow;
            if (_isPopShow)
            {
                RefreshPopBgHeightAndPosition();
                RefreshPopScrollViewPosition();
                _chapterNameList.SetDataList<CopyProgressChapterItem>(_chapterDataList, 1);
            }
        }

        private void RefreshPopBgHeightAndPosition()
        {
            if (_chapterDataList.Count <= CHAPTER_NUM_PER_SCROLLVIEW)
            {
                _imagePopBg.Height = _chapterDataList.Count * ITEM_HEIGHT +
                    _chapterNameList.TopToDownGap * (_chapterDataList.Count - 1) + _chapterNameList.Padding.top;
            }
            else
            {
                _imagePopBg.Height = POP_BG_INIT_HEIGHT;
            }
        }

        private void RefreshPopScrollViewPosition()
        {
            if (_chapterDataList.Count <= CHAPTER_NUM_PER_SCROLLVIEW)
            {
                _scrollView.Arrow.Visible = false;
                _locaterScrollView.Y = SCROLL_VIEW_INIT_POSITION.y - (POP_BG_INIT_HEIGHT - _imagePopBg.Height);
            }
            else
            {
                _scrollView.Arrow.Visible = true;
                _locaterScrollView.Y = SCROLL_VIEW_INIT_POSITION.y;
            }
        }

        private void OnAllItemCreated()
        {
            List<KList.KListItemBase> itemList = _chapterNameList.ItemList;
            for (int i = 0; i < itemList.Count; i++)
            {
                CopyProgressChapterItem item = itemList[i] as CopyProgressChapterItem;
                item.SetSelectFrameActive(i == _currentIndex);
            }
        }
    }
}
