﻿using Common.Utils;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyChapterViewTemplate : KContainer
    {
        private Dictionary<string, GameObject> _templateDict;

        protected override void Awake()
        {
            _templateDict = new Dictionary<string, GameObject>();
            AddStar("Container_xingxing01");
            AddStar("Container_xingxing02");
            AddStar("Container_xingxing03");
            AddTemplateDict("Container_exp");
            AddTemplateDict("Container_renwu");
            AddTemplateDict("Container_luxian0");
            AddTemplateDict("Container_luxian1");
            AddTemplateDict("Container_luxian2");
            AddTemplateDict("Container_luxian3");
        }

        private void AddStar(string name)
        {
            GameObject go = AddTemplateDict(name);
            go.transform.localPosition = new Vector3(-1000f, -1000f, 0f);
        }

        private GameObject AddTemplateDict(string name)
        {
            GameObject go = GetChild(name);
            _templateDict.Add(name, go);
            return go;
        }

        public GameObject CloneGameObject(string name, Transform parent)
        {
            GameObject go = GameObject.Instantiate(_templateDict[name]) as GameObject;
            go.SetActive(true);
            go.transform.SetParent(parent, false);
            go.transform.localScale = Vector3.one;
            return go;
        }

    }
}
