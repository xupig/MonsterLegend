﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Utils;
using Common.Data;

namespace ModuleCopy
{
    public class CopyInstanceStar : KContainer
    {
        private CopyChapterViewTemplate _template;
        private Dictionary<int, GameObject> _starDict = new Dictionary<int, GameObject>();

        public void SetStar(CopyChapterViewTemplate template, int num)
        {
            _template = template;
            HideAllStar();
            ShowStar(num);
        }

        private void HideAllStar()
        {
            foreach (GameObject go in _starDict.Values)
            {
                go.SetActive(false);
            }
        }

        private void ShowStar(int num)
        {
            if ((num > 0) && (num <= CopyData.MAX_STAR_NUM_PER_INSTANCE))
            {
                if (!_starDict.ContainsKey(num))
                {
                    string path = string.Format("Container_xingxing0{0}", num);
                    GameObject go = _template.CloneGameObject(path, transform);
                    RectTransform rect = go.GetComponent<RectTransform>();
                    rect.anchorMin = new Vector2(0.5f, 0.5f);
                    rect.anchorMax = new Vector2(0.5f, 0.5f);
                    rect.pivot = new Vector2(0.5f, 0.5f);
                    rect.anchoredPosition = new Vector2(0f, -102);
                    _starDict.Add(num, go);
                }
                _starDict[num].SetActive(true);
            }
        }
    }
}
