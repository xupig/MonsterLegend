﻿using Common.Utils;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCopy
{
    public class CopyBackground : KContainer
    {
        protected override void Awake()
        {
            MogoAtlasUtils.AddSprite(GetChild("Container_bg0"), "copyBg0");
            MogoAtlasUtils.AddSprite(GetChild("Container_bg1"), "copyBg1");
            MogoAtlasUtils.AddSprite(GetChild("Container_bg2"), "copyBg2");
            MogoAtlasUtils.AddSprite(GetChild("Container_bg3"), "copyBg3");
        }
    }
}
