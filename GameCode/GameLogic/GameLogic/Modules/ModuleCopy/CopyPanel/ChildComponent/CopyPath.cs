﻿using System.Collections.Generic;
using Game.UI.UIComponent;
using GameData;
using Common.Data;
using GameMain.GlobalManager;
using Common.ServerConfig;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyPath : KContainer
    {
        private List<CopyInstance> _instanceList;
        private CopyData _copyData
        {
            get
            {
                return PlayerDataManager.Instance.CopyData;
            }
        }

        protected override void Awake()
        {
            _instanceList = new List<CopyInstance>();
            for (int i = 0; i < transform.childCount; i++)
            {
                CopyInstance copyInstance = AddChildComponent<CopyInstance>(string.Format("Container_fuben{0}", i));
                copyInstance.Index = i;
                _instanceList.Add(copyInstance);
            }
        }

        public void Refresh(CopyChapterViewTemplate template, CopyChapterData chapterData)
        {
            int maxExpInstanceId = GetMaxExpInstanceId();
            List<CopyInstanceData> instanceDataList = GetInstanceDataList(chapterData.id);
            Dictionary<int, int> instanceIdDict = GetInstanceIdToAcceptTaskIdDict(); //未接任务列表
            for (int i = 0; i < _instanceList.Count; i++)
            {
                if (i < instanceDataList.Count)
                {
                    CopyInstanceData instanceData = instanceDataList[i];
                    _instanceList[i].Visible = true;
                    _instanceList[i].SetData(template, instanceData);
                    if (maxExpInstanceId == instanceData.id)
                    {
                        _instanceList[i].ShowExp();
                    }
                    if (instanceIdDict.ContainsKey(instanceData.id))
                    {
                        int taskId = instanceIdDict[instanceData.id];
                        _instanceList[i].ShowTask(taskId);
                    }
                }
                else
                {
                    _instanceList[i].Visible = false;
                }
            }
        }

        private int GetMaxExpInstanceId()
        {
            int largestInstanceId = instance_helper.InvalidInstanceId;
            foreach (int instanceId in _copyData.InstanceDict.Keys)
            {
                if (instance_helper.GetChapterType(instanceId) == ChapterType.Copy)
                {
                    if ((instanceId > largestInstanceId) && (_copyData.IsInstancePass(instanceId)))
                    {
                        largestInstanceId = instanceId;
                    }
                }
            }
            return largestInstanceId;
        }

        private List<CopyInstanceData> GetInstanceDataList(int chapteId)
        {
            List<CopyInstanceData> instanceDataList = new List<CopyInstanceData>();
            List<int> instanceIdList = chapters_helper.GetInstanceIdList(chapteId);
            for (int i = 0; i < instanceIdList.Count; i++)
            {
                int instanceId = instanceIdList[i];
                if (_copyData.InstanceDict.ContainsKey(instanceId))
                {
                    CopyInstanceData instanceData = _copyData.InstanceDict[instanceId];
                    if (instanceData.IsCopyOpen)
                    {
                        instanceDataList.Add(instanceData);
                    }
                }
            }
            return instanceDataList;
        }

        public Dictionary<int, int> GetInstanceIdToAcceptTaskIdDict()
        {
            TaskData taskData = PlayerDataManager.Instance.TaskData;
            Dictionary<int, int> instanceIdDict = new Dictionary<int, int>();
            List<int> taskIdList = taskData.GetAcceptTaskIdList();
            foreach (int taskId in taskIdList)
            {
                int instanceId = task_data_helper.GetTaskInstanceId(taskId);
                if (instanceId != 0)
                {
                    //如果主线支线任务重叠，优先显示主线
                    if (instanceIdDict.ContainsKey(instanceId))
                    {
                        int taskType = task_data_helper.GetTaskType(taskId);
                        if (taskType == public_config.TASK_TYPE_MAIN)
                        {
                            instanceIdDict.Remove(instanceId);
                            instanceIdDict.Add(instanceId, taskId);
                        }
                    }
                    else
                    {
                        instanceIdDict.Add(instanceId, taskId);
                    }
                }
            }
            return instanceIdDict;
        }
    }
}
