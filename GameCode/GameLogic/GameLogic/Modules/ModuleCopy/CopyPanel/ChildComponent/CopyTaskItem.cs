﻿using Common.ServerConfig;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyTaskItem : KContainer
    {
        private Vector3 GO_TASK_INIT_POSITION;

        private StateImage _imageMainTaskBg;
        private StateImage _imageBranchTaskBg;
        private StateText _textTaskType;
        private StateText _textTaskName;

        protected override void Awake()
        {
            GO_TASK_INIT_POSITION = transform.localPosition;
            _imageMainTaskBg = GetChildComponent<StateImage>("Container_txt/Image_tishiIcon1");
            _imageBranchTaskBg = GetChildComponent<StateImage>("Container_txt/Image_tishiIcon");
            _textTaskType = GetChildComponent<StateText>("Container_txt/Label_txtRenwubiaoti");
            _textTaskName = GetChildComponent<StateText>("Container_txt/Label_txtDangqianrenwu");
        }

        public void Refresh(int taskId)
        {
            int taskType = task_data_helper.GetTaskType(taskId);
            if (taskType == public_config.TASK_TYPE_MAIN)
            {
                _textTaskType.CurrentText.text = MogoLanguageUtil.GetContent(1425);
                _imageMainTaskBg.Alpha = 1f;
                _imageBranchTaskBg.Alpha = 0f;
            }
            else
            {
                _textTaskType.CurrentText.text = MogoLanguageUtil.GetContent(1426);
                _imageMainTaskBg.Alpha = 0f;
                _imageBranchTaskBg.Alpha = 1f;
            }
            _textTaskName.CurrentText.text = task_data_helper.GetTaskName(task_data_helper.GetTaskData(taskId));
            PlayAnimation();
        }

        private void PlayAnimation()
        {
            Vector3 vector3 = new Vector3(GO_TASK_INIT_POSITION.x, GO_TASK_INIT_POSITION.y + 10, GO_TASK_INIT_POSITION.z);
            TweenPosition tween = TweenPosition.Begin(gameObject, 0.6f, vector3);
            tween.from = new Vector2(GO_TASK_INIT_POSITION.x, GO_TASK_INIT_POSITION.y);
            tween.style = UITweener.Style.PingPong;
        }
    }
}
