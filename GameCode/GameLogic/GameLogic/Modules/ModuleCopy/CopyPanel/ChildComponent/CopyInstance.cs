﻿using Common.Base;
using Common.Data;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using UnityEngine;
using MogoEngine.Events;
using Common.Events;

namespace ModuleCopy
{
    public class CopyInstance : KContainer
    {
        private CopyChapterViewTemplate _template;

        private CopyInstanceData _instanceData;
        public int Index;
        private int _taskId;

        private KButton _btnSelect;
        private CopyInstanceStar _instanceStar;
        private GameObject _goExp;
        private CopyTaskItem _taskItem;

        protected override void Awake()
        {
            _btnSelect = GetChildComponent<KButton>("Container_rukou/Button_fuben");
            _instanceStar = AddChildComponent<CopyInstanceStar>("Container_rukou");
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnSelect.onClick.AddListener(OnClick);
            EventDispatcher.AddEventListener<int>(TaskEvent.TASK_REACHED, OnTaskReached);
        }

        private void RemoveListener()
        {
            _btnSelect.onClick.RemoveListener(OnClick);
            EventDispatcher.RemoveEventListener<int>(TaskEvent.TASK_REACHED, OnTaskReached);
        }

        private void OnClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.CopyPreview, _instanceData);
        }

        private void OnTaskReached(int taskId)
        {
            if (_taskId == taskId)
            {
                HideTaskItem();
            }
        }


        public void SetData(CopyChapterViewTemplate template, CopyInstanceData instanceData)
        {
            _template = template;
            _instanceData = instanceData;
            Refresh();
        }

        private void Refresh()
        {
            RefreshStar();
            HideExp();
            HideTaskItem();
        }

        private void RefreshStar()
        {
            _instanceStar.SetStar(_template, _instanceData.HistoryMaxStar);
        }

        private void HideExp()
        {
            if (_goExp != null)
            {
                _goExp.SetActive(false);
            }
        }

        private void HideTaskItem()
        {
            if (_taskItem != null)
            {
                _taskItem.Visible = false;
            }
        }

        public void ShowExp()
        {
            if (_goExp == null)
            {
                string path = string.Format("Container_exp");
                GameObject go = _template.CloneGameObject(path, _instanceStar.transform);
                go.transform.localPosition = new Vector3(-26.5f, -37, 0);
                _goExp = go;
            }
            _goExp.SetActive(true);
        }

        public void ShowTask(int taskId)
        {
            if (_taskItem == null)
            {
                GameObject go = _template.CloneGameObject("Container_renwu", _instanceStar.transform);
                if (Index % 3 == 2)
                {
                    go.transform.localPosition = new Vector3(4.4f, 60f, 0);
                }
                else
                {
                    go.transform.localPosition = new Vector3(-18.3f, 60f, 0);
                }
                _taskItem = go.AddComponent<CopyTaskItem>();
            }
            _taskId = taskId;
            _taskItem.Visible = true;
            _taskItem.Refresh(taskId);
        }
    }
}
