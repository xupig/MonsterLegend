﻿using Common.Data;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyProgressChapterItem : KList.KListItemBase
    {
        private StateImage _imageCheckMark;
        private StateText _textName;

        protected override void Awake()
        {
            _imageCheckMark = GetChildComponent<StateImage>("Image_checkmark");
            _imageCheckMark.Alpha = 0f;
            _textName = GetChildComponent<StateText>("Label_Label");
            _textName.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
        }

        public override object Data
        {
            set
            {
                Refresh(value as CopyChapterData);
            }
        }

        public override void Dispose()
        {
        }

        private void Refresh(CopyChapterData chapterData)
        {
            _textName.ChangeAllStateText(string.Format(MogoLanguageUtil.GetContent(49997), chapters_helper.GetPage(chapterData.id), chapters_helper.GetName(chapterData.id)));
        }

        public void SetSelectFrameActive(bool isShow)
        {
            _imageCheckMark.Alpha = isShow ? 1f : 0f;
        }
    }
}
