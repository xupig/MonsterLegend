﻿using System.Collections.Generic;
using Common.Base;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using UnityEngine;
using Common.Data;
using UnityEngine.UI;

namespace ModuleCopy
{
    public class CopyPanel : BasePanel
    {
        private List<CopyChapterData> _chapterDataList;
        private int _currentIndex;

        private CopyData _copyData
        {
            get
            {
                return PlayerDataManager.Instance.CopyData;
            }
        }

        private KScrollPage _scrollPage;
        private KPageableList _list;
        private CopyProgressView _progressView;
        private KToggle _toggleTitle;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_luxian");
            _scrollPage.MoveType = ScrollRect.MovementType.Clamped;
            _progressView = AddChildComponent<CopyProgressView>("Container_progress");
            _toggleTitle = GetChildComponent<KToggle>("ToggleGroup_biaoti/Toggle_zhanshenzhilu");
            _list = GetChildComponent<KPageableList>("ScrollPage_luxian/mask/content");
            _list.SetDirection(KList.KListDirection.LeftToRight, 1, 1);
        }

        public override void OnClose()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _scrollPage.onCurrentPageChanged.AddListener(onCurrentPageChanged);
            _progressView.onSelectChapter.AddListener(OnSelectChapter);
        }

        private void RemoveListener()
        {
            _scrollPage.onCurrentPageChanged.RemoveListener(onCurrentPageChanged);
            _progressView.onSelectChapter.RemoveListener(OnSelectChapter);
        }

        private void onCurrentPageChanged(KScrollPage scrollPage, int index)
        {
            OnSelectChapter(index);
        }

        private void OnSelectChapter(int index)
        {
            _currentIndex = index;
            _scrollPage.CurrentPage = _currentIndex;
            RefreshProgress();
            RefreshTitle();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Copy; }
        }

        //可以传递副本类型 也可以传关卡id
        public override void OnShow(object data)
        {
            AddListener();
            _copyData.CheckChapterAndInstance();
            _chapterDataList = _copyData.GetCopyChapterDataList();
            _currentIndex = ParseSelectIndex(data);
            if (_chapterDataList.Count <= _currentIndex)
            {
                Debug.LogError(string.Format("剧情章节数量为{0}  要访问的下标为{1} 传入id为{2}", _chapterDataList.Count, _currentIndex, (int)(data ?? -1)));
                _currentIndex = 0;//容错处理
            }
            Refresh();
        }

        private int ParseSelectIndex(object data)
        {
            int index = 0;
            int id = data != null ? (int)data : 0;
            //剧情副本关卡id都大于10000
            if (id > 10000)
            {
                index = instance_helper.GetChapterIndex(id);
            }
            else
            {
                int instanceId = _copyData.GetLargestInstanceId();
                if (instanceId != instance_helper.InvalidInstanceId)
                {
                    index = instance_helper.GetChapterIndex(instanceId);
                }
            }
            return index;
        }

        private void Refresh()
        {
            RefreshProgress();
            RefreshChapterList();
            RefreshTitle();
        }

        private void RefreshProgress()
        {
            _progressView.SetData(_chapterDataList, _currentIndex);
        }

        private void RefreshChapterList()
        {
            _list.SetDataList(_currentIndex, typeof(CopyChapterView), _chapterDataList, 1);
            _scrollPage.TotalPage = _chapterDataList.Count;
            _scrollPage.CurrentPage = _currentIndex;
        }

        private void RefreshTitle()
        {
            _toggleTitle.SetLabel(chapters_helper.GetName(_chapterDataList[_currentIndex].id));
        }
    }
}
