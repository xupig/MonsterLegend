﻿using Common.Data;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class CopyPassConditionListInBattle : KContainer
    {
        public float PlayTime = 0.2f;

        private const int MAX_CONDITION_NUM = 3;

        private List<CopyPassConditionInBattle> _conditionList;

        protected override void Awake()
        {
            _conditionList = new List<CopyPassConditionInBattle>();
            for (int i = 0; i < MAX_CONDITION_NUM; i++)
            {
                CopyPassConditionInBattle condition = AddChildComponent<CopyPassConditionInBattle>(string.Format("Container_NR0{0}", i));
                _conditionList.Add(condition);
            }
        }

        public void Refresh(CopyCardLogic cardLogic)
        {
            int mapId = cardLogic.MapId;
            Dictionary<string, string[]> dicScoreRule = map_helper.GetScoreRule(mapId);
            List<PbActionInfo> conditionResultList = cardLogic.ConditionResultList;
            //服务端只发送有星的条件 需要把没有的添加在后面
            AddUnpassCondition(conditionResultList, dicScoreRule);
            for (int i = 0; i < MAX_CONDITION_NUM; i++)
            {
                CopyPassConditionInBattle condition = _conditionList[i];
                if (i < conditionResultList.Count)
                {
                    PbActionInfo pbActionInfo = conditionResultList[i];
                    condition.Visible = true;
                    string conditionValue = dicScoreRule[pbActionInfo.action_id.ToString()][0];
                    string conditionStr = string.Format(map_helper.GetDesc(mapId, (int)pbActionInfo.action_id),
                        map_helper.ParseScoreRuleParam(conditionValue));
                    condition.Refresh(conditionStr, pbActionInfo.score == 1);
                }
                else
                {
                    condition.Visible = false;
                }
            }
        }

        private void AddUnpassCondition(List<PbActionInfo> conditionResultList, Dictionary<string, string[]> dicScoreRule)
        {
            HashSet<int> actionIdHash = InitHash(conditionResultList);
            foreach (string actionIdStr in dicScoreRule.Keys)
            {
                uint actionId = uint.Parse(actionIdStr);
                if (!actionIdHash.Contains((int)actionId))
                {
                    PbActionInfo pbActionInfo = new PbActionInfo();
                    pbActionInfo.action_id = actionId;
                    pbActionInfo.score = 0;//没有星
                    conditionResultList.Add(pbActionInfo);
                }
            }
        }

        private HashSet<int> InitHash(List<PbActionInfo> conditionResultList)
        {
            HashSet<int> actionIdHash = new HashSet<int>();
            for (int i = 0; i < conditionResultList.Count; i++)
            {
                actionIdHash.Add((int)conditionResultList[i].action_id);
            }
            return actionIdHash;
        }

        public void HideAll()
        {
            for (int i = 0; i < _conditionList.Count; i++)
            {
                _conditionList[i].Visible = false;
            }
        }

        public void DoTween(int index)
        {
            CopyPassConditionInBattle condition = _conditionList[index];
            condition.Visible = true;
            Vector3 position = condition.transform.localPosition;
            condition.transform.localPosition = new Vector3(position.x + 1280, position.y, position.z);
            TweenPosition tween = TweenPosition.Begin(condition.gameObject, PlayTime, position);
            tween.method = UITweener.Method.EaseOut;
        }
    }
}
