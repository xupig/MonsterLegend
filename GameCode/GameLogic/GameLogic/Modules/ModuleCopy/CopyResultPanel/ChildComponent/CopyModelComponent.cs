﻿using ACTSystem;
using Common.Data;
using Common.ExtendComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyModelComponent : ModelComponent
    {
        private CopyResult _result;
        private Action<ACTActor> _callback;

        protected override void Awake()
        {
            base.Awake();
            SetStartPosition(new Vector3(0, 0, 1000));
            SetStartRotationY(177.6f);
            DragComponent.Dragable = false;
        }

        public void LoadPlayerModel(CopyResult result, Action<ACTActor> callback = null)
        {
            _result = result;
            _callback = callback;
            this.LoadPlayerModel(LoadModelCallback, false);
        }

        private void LoadModelCallback(ACTActor actor)
        {
            if (_result == CopyResult.Win)
            {
                actor.animationController.ShowWin();
            }
            else if (_result == CopyResult.Lose)
            {
                actor.animationController.ShowLose();
            }
            if (_callback != null)
            {
                _callback.Invoke(actor);
                _callback = null;
            }
        }
    }
}
