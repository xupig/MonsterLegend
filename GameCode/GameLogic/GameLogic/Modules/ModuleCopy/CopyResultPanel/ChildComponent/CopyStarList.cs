﻿using Common.Data;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCopy
{
    public class CopyStarList : KContainer
    {
        private List<CopyStar> _starList;

        public KComponentEvent onAllStarPlayComplete = new KComponentEvent();
        public KComponentEvent<int> onOneStarPlayComplete = new KComponentEvent<int>();

        protected override void Awake()
        {
            _starList = new List<CopyStar>();
            for (int i = 0; i < CopyData.MAX_STAR_NUM_PER_INSTANCE; i++)
            {
                CopyStar star = AddChildComponent<CopyStar>(string.Format("Container_star{0}", i));
                star.Index = i;
                _starList.Add(star);
            }
        }

        public void SetStarNum(int num)
        {
            for (int i = 0; i < _starList.Count; i++)
            {
                _starList[i].SetBright(i < num);
            }
        }

        public void HideAll()
        {
            for (int i = 0; i < _starList.Count; i++)
            {
                _starList[i].Visible = false;
            }
        }

        public void DoTween()
        {
            AddListener();
            HideAll();
            _starList[0].DoTween();
        }

        private void AddListener()
        {
            for (int i = 0; i < _starList.Count; i++)
            {
                CopyStar star = _starList[i];
                star.onPlayComplete.AddListener(OnStarFinish);
            }
        }

        private void RemoveListener()
        {
            for (int i = 0; i < _starList.Count; i++)
            {
                CopyStar star = _starList[i];
                star.onPlayComplete.RemoveListener(OnStarFinish);
            }
        }

        private void OnStarFinish(int index)
        {
            index++;
            if (index < _starList.Count)
            {
                onOneStarPlayComplete.Invoke(index);
                _starList[index].DoTween();
            }
            else
            {
                onAllStarPlayComplete.Invoke();
                RemoveListener();
            }
        }
    }
}
