﻿using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace ModuleCopy
{
    public class CopyCard : KContainer
    {
        public float ROTATION_TIME = 0.4f;
        private int FrameIconId = 6904;

        public bool IsOpen = false;
        public bool IsOpened = false;
        private BaseItemData _baseItemData;
        private bool _isMyCard = false;

        private KToggle _toggle;
        private IconContainer _goIcon;
        private StateText _txtName;
        private StateText _txtCount;
        private GameObject _goBack;
        private GameObject _goFront;
        private IconContainer _goQualityFrame;
        private RectTransform _rectFront;
        private KParticle _particle;

        public KComponentEvent<CopyCard> onClick = new KComponentEvent<CopyCard>();
        public KComponentEvent onOpenedCard = new KComponentEvent();

        protected override void Awake()
        {
            _toggle = gameObject.GetComponent<KToggle>();
            
            _goIcon = AddChildComponent<IconContainer>("checkmark/Container_icon");
            _txtName = GetChildComponent<StateText>("checkmark/Label_Name");
            _goQualityFrame = AddChildComponent<IconContainer>("checkmark/Container_pinzhikuang");
            _txtCount = GetChildComponent<StateText>("checkmark/Label_shuliang");
            _particle = AddChildComponent<KParticle>("checkmark/fx_ui_9_4_xuanzhuanguangsu");
            InitImage();
            InitCheckmark();

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _toggle.onValueChanged.AddListener(OnValueChanged);
        }

        private void RemoveListener()
        {
            _toggle.onValueChanged.RemoveListener(OnValueChanged);
        }

        private void InitImage()
        {
            _goBack = GetChild("image");
        }

        private void InitCheckmark()
        {
            _goFront = GetChild("checkmark");
            _rectFront = GetChildComponent<RectTransform>("checkmark");
        }

        private void OnValueChanged(KToggle toggle, bool isOn)
        {
            if (isOn)
            {
                onClick.Invoke(this);
            }
        }

        protected override void OnEnable()
        {
            IsOpen = false;
            IsOpened = false;
            _isMyCard = false;
            _toggle.isOn = false;
            _goFront.SetActive(false);
            _particle.Stop();
        }

        public void OpenCard(BaseItemData baseItemData, bool isMyCard = false)
        {
            if (isMyCard)
            {
                UIManager.Instance.PlayAudio("Sound/UI/mission_card_get.mp3");
            }
            _isMyCard = isMyCard;
            _goFront.SetActive(false);
            _goBack.SetActive(true);
            Refresh(baseItemData);
            TweenRotation tween = TweenRotation.Begin(_goBack, ROTATION_TIME, Quaternion.Euler(0, 180, 0), OnCardCartoonEndHandler);
            _toggle.enabled = false;
            IsOpen = true;
        }

        private void OnCardCartoonEndHandler(UITweener tween)
        {
            IsOpened = true;
            _goFront.SetActive(true);
            _goBack.SetActive(false);
            //需要保证particle的activeInHierarchy为true
            if (_isMyCard)
            {
                _particle.Play(true);
                _particle.Position = new Vector3(-92.23f, 120f, 0);
            }
            _rectFront.transform.localRotation = Quaternion.Euler(Vector3.zero);
            onOpenedCard.Invoke();
        }

        public void SetCanNotClick()
        {
            _toggle.enabled = false;
        }

        public void Refresh(BaseItemData baseItemData)
        {
            _baseItemData = baseItemData;
            _goQualityFrame.SetIcon(FrameIconId, OnLoadCallback);
            _txtName.CurrentText.text = item_helper.GetName(baseItemData.Id);
            _txtCount.CurrentText.text = baseItemData.StackCount > 1 ? baseItemData.StackCount.ToString() : "";
            _goIcon.SetIcon(baseItemData.Icon);
            //在手机上需要调整下层次
            _goIcon.transform.FindChild("holder").transform.localPosition = new Vector3(0f, 0f, -1f);
        }

        private void OnLoadCallback(GameObject go)
        {
            ImageWrapper imageWrapper = go.GetComponent<ImageWrapper>();
            imageWrapper.color = ColorDefine.GetNewQualityColor(_baseItemData.Quality);
        }
    }
}
