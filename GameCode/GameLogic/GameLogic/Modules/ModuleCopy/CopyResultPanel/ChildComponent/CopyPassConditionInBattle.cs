﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class CopyPassConditionInBattle : KContainer
    {
        private StateText _textCondition;
        private GameObject _goStar;

        protected override void Awake()
        {
            _textCondition = GetChildComponent<StateText>("Label_txtNr");
            _goStar = GetChild("Container_star/Image_xingxingIcon");
        }

        public void Refresh(string condition, bool showStar)
        {
            _textCondition.CurrentText.text = condition;
            _goStar.SetActive(showStar);
        }
    }
}
