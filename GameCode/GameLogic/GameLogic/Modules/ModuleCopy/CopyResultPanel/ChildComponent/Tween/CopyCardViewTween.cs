﻿using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyCardViewTween : KContainer
    {
        public uint DelayTime = 400;

        public static List<Vector3> CardInitPositionList = new List<Vector3>();

        private CopyCardTween[] _cardTweenArray;

        private int _actionIndex;
        private List<Action> _actionList = new List<Action>();

        public KComponentEvent OnShuffleCardFinish = new KComponentEvent();

        protected override void Awake()
        {
            _cardTweenArray = new CopyCardTween[CopyCardView.MAX_CARD_NUM];
            for (int i = 0; i < CopyCardView.MAX_CARD_NUM; i++)
            {
                CopyCardTween cardTween = AddChildComponent<CopyCardTween>(string.Format("Container_fanpai/Toggle_kapai0{0}", i + 1));
                _cardTweenArray[i] = cardTween;
                cardTween.Index = i;
                cardTween.OnDealFinish.AddListener(DoNextAction);
                cardTween.OnCloseCardFinish.AddListener(DoNextAction);
                cardTween.OnShuffleCardFinish.AddListener(DoNextAction);

                CardInitPositionList.Add(cardTween.transform.localPosition);
            }
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < CopyCardView.MAX_CARD_NUM; i++)
            {
                CopyCardTween cardTween = _cardTweenArray[i];
                cardTween.OnDealFinish.RemoveListener(DoNextAction);
                cardTween.OnCloseCardFinish.RemoveListener(DoNextAction);
                cardTween.OnShuffleCardFinish.RemoveListener(DoNextAction);
            }
        }

        private void DoNextAction()
        {
            if (IsAllCardTweenFinish())
            {
                DoAction();
            }
        }

        private void ClearAllCardTweenFinish()
        {
            for (int i = 0; i < _cardTweenArray.Length; i++)
            {
                _cardTweenArray[i].TweenFinish = false;
            }
        }

        private bool IsAllCardTweenFinish()
        {
            for (int i = 0; i < _cardTweenArray.Length; i++)
            {
                if (!_cardTweenArray[i].TweenFinish)
                {
                    return false;
                }
            }
            return true;
        }

        private void DoAction()
        {
            if (_actionIndex < _actionList.Count)
            {
                int index = _actionIndex;
                _actionIndex++;
                _actionList[index].Invoke();
            }
        }

        public void DoTween()
        {
            _actionIndex = 0;
            _actionList.Clear();
            _actionList.Add(Deal);//发牌
            _actionList.Add(Delay);
            _actionList.Add(CloseCard);
            _actionList.Add(ShuffleCard);
            _actionList.Add(ShuffleCardFinish);
            DoAction();
        }

        private void Deal()
        {
            ClearAllCardTweenFinish();
            UIManager.Instance.PlayAudio("Sound/UI/mission_card_show.mp3");
            for (int i = 0; i < CopyCardView.MAX_CARD_NUM; i++)
            {
                CopyCardTween cardTween = _cardTweenArray[i];
                cardTween.Deal();
            }
        }

        private void Delay()
        {
            TimerHeap.AddTimer(DelayTime, 0, DoAction);
        }

        private void CloseCard()
        {
            ClearAllCardTweenFinish();
            for (int i = 0; i < CopyCardView.MAX_CARD_NUM; i++)
            {
                CopyCardTween cardTween = _cardTweenArray[i];
                cardTween.CloseCard();
            }
        }

        private void ShuffleCard()
        {
            ClearAllCardTweenFinish();
            for (int i = 0; i < CopyCardView.MAX_CARD_NUM; i++)
            {
                CopyCardTween cardTween = _cardTweenArray[i];
                cardTween.ShuffleCard();
            }
        }

        private void ShuffleCardFinish()
        {
            OnShuffleCardFinish.Invoke();
        }
    }
}
