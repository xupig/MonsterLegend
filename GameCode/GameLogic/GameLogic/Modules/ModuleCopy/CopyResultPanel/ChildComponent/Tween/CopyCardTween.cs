﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyCardTween : KContainer
    {
        public float INTERVAL_TIME = 0.15f;
        public float ENTER_TIME = 0.2f;
        public float ROTATION_TIME_ANIMATION = 0.3f;
        public float XIPAI_TIME = 0.1f;

        private static readonly Vector3 StartPosition = new Vector3(-300f, -300f, 0f);

        public int Index;
        public bool TweenFinish;

        private KToggle _toggle;
        private GameObject _goBack;
        private RectTransform _rectBack;
        private GameObject _goFront;
        private RectTransform _rectFront;

        public KComponentEvent OnDealFinish = new KComponentEvent();
        public KComponentEvent OnCloseCardFinish = new KComponentEvent();
        public KComponentEvent OnShuffleCardFinish = new KComponentEvent();

        protected override void Awake()
        {
            _toggle = gameObject.GetComponent<KToggle>();
            InitBack();
            InitFront();
        }

        private void InitBack()
        {
            _goBack = GetChild("image");
            _rectBack = GetChildComponent<RectTransform>("image");
            _rectBack.pivot = new Vector2(0.5f, 0.5f);
            _rectBack.anchorMax = new Vector2(0.5f, 0.5f);
            _rectBack.anchorMin = new Vector2(0.5f, 0.5f);
        }

        private void InitFront()
        {
            _goFront = GetChild("checkmark");
            _rectFront = GetChildComponent<RectTransform>("checkmark");
            _rectFront.pivot = new Vector2(0.5f, 0.5f);
            _rectFront.anchorMax = new Vector2(0.5f, 0.5f);
            _rectFront.anchorMin = new Vector2(0.5f, 0.5f);
        }

        private void SetTweenFinish(KComponentEvent componentEvent)
        {
            TweenFinish = true;
            componentEvent.Invoke();
        }

        public void Deal()
        {
            _toggle.enabled = false;
            _goFront.SetActive(true);
            gameObject.transform.localPosition = StartPosition;
            TweenPosition tween = TweenPosition.Begin(gameObject, ENTER_TIME, CopyCardViewTween.CardInitPositionList[Index], Index * INTERVAL_TIME, UITweener.Method.EaseOut);
            tween.onFinished = DealFinish;
            transform.localScale = new Vector3(2, 2, 1);
            TweenScale.Begin(gameObject, ENTER_TIME, Vector3.one);
        }

        private void DealFinish(UITweener tween)
        {
            SetTweenFinish(OnDealFinish);
        }

        public void CloseCard()
        {
            _goBack.SetActive(false);
            _goFront.SetActive(true);
            TweenRotation tween = TweenRotation.Begin(_goFront, ROTATION_TIME_ANIMATION, Quaternion.Euler(0, 180, 0), CloseCardFinish);
            tween.from = Vector3.zero;
        }

        private void CloseCardFinish(UITweener tween)
        {
            _goBack.SetActive(true);
            _goFront.SetActive(false);
            SetTweenFinish(OnCloseCardFinish);
        }

        public void ShuffleCard()
        {
            TweenPosition tweenPosition = TweenPosition.Begin(gameObject, XIPAI_TIME, CopyCardViewTween.CardInitPositionList[2]);
            tweenPosition.delay = 0f;
            tweenPosition.onFinished = ShuffleCardBack;
        }

        private void ShuffleCardBack(UITweener tween)
        {
            TweenPosition tweenPosition = TweenPosition.Begin(gameObject, XIPAI_TIME, CopyCardViewTween.CardInitPositionList[Index]);
            tweenPosition.delay = 0f;
            tweenPosition.onFinished = ShuffleCardFinish;
        }

        private void ShuffleCardFinish(UITweener tween)
        {
            _goFront.transform.localRotation = Quaternion.Euler(Vector3.zero);
            _goBack.transform.localRotation = Quaternion.Euler(Vector3.zero);
            _toggle.enabled = true;
            SetTweenFinish(OnShuffleCardFinish);
        }
    }
}
