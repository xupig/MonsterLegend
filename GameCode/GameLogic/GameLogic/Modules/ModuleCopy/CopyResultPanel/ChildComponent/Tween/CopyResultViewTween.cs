﻿using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyResultViewTween : KContainer
    {
        public float ResultPlayTime = 0.2f;
        public float EndPlayTime = 0.1f;
        public float ResultScale = 5f;

        private Vector3 InitPosition;
        private RectTransform _rectTransform;

        private CopyStarList _starList;
        private GameObject _goWin;
        private GameObject _goFail;
        private CopyPassConditionListInBattle _passConditionList;
        private KParticle _particle;
        private KDummyButton _btnNext;
        private TweenPosition tweenPosition;

        public KComponentEvent onTweenFinished = new KComponentEvent();

        protected override void Awake()
        {
            _rectTransform = gameObject.GetComponent<RectTransform>();
            InitPosition = _rectTransform.anchoredPosition;

            _starList = GetChildComponent<CopyStarList>("Container_xingxing");
            _passConditionList = GetChildComponent<CopyPassConditionListInBattle>("Container_shenglitiaojian");
            _goWin = GetChild("Container_shengli");
            _particle = GetChildComponent<KParticle>("Container_shengli/fx_ui_9_3_zhandoushengli_01");
            _goFail = GetChild("Container_shibai");
            _btnNext = GetChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
        }

        private Action<UITweener> _OnTweenFinished;
        public void OnNext(Action<UITweener> OnTweenFinished)
        {
            _OnTweenFinished = OnTweenFinished;
            tweenPosition = TweenPosition.Begin(gameObject, EndPlayTime, new Vector3(InitPosition.x, InitPosition.y + 1000, 0));
            tweenPosition.enabled = true;
            tweenPosition.from = InitPosition;
            tweenPosition.method = UITweener.Method.EaseIn;
            tweenPosition.onFinished = OnFinish;
        }

        private void OnFinish(UITweener tween)
        {
            //如果在tween的Finish报错，就会导致下次结算时立即进入OnFinish，所以需要在这里修改enabled为false
            tweenPosition.enabled = false;
            _OnTweenFinished.Invoke(tween);
        }

        public void DoFirstTween(CopyResult copyResult)
        {
            HideAll();
            _rectTransform.anchoredPosition = InitPosition;
            GameObject go;
            if (copyResult == CopyResult.Win)
            {
                go = _goWin;
            }
            else
            {
                go = _goFail;
            }
            go.SetActive(true);
            TweenScale tween = TweenScale.Begin(go, ResultPlayTime, Vector3.one, UITweener.Method.EaseOut);
            tween.from = Vector3.one * ResultPlayTime;
            if (copyResult == CopyResult.Win)
            {
                tween.onFinished = ResultTweenFinished;
            }
            else
            {
                tween.onFinished = null;
            }
        }

        private void ResultTweenFinished(UITweener tween)
        {
            _particle.Play();
        }

        public void HideAll()
        {
            _starList.HideAll();
            _passConditionList.HideAll();
            _goWin.SetActive(false);
            _goFail.SetActive(false);
            _particle.Stop();
            _btnNext.enabled = false;
        }

        public void DoSecondTween()
        {
            AddTweenListner();
            _starList.DoTween();
            _passConditionList.DoTween(0);
        }

        private void AddTweenListner()
        {
            _starList.onOneStarPlayComplete.AddListener(OnOneStarPlayComplete);
            _starList.onAllStarPlayComplete.AddListener(OnAllStarPlayComplete);
        }

        private void RemoveTweenListner()
        {
            _starList.onOneStarPlayComplete.RemoveListener(OnOneStarPlayComplete);
            _starList.onAllStarPlayComplete.RemoveListener(OnAllStarPlayComplete);
        }

        private void OnOneStarPlayComplete(int index)
        {
            _passConditionList.DoTween(index);
        }

        private void OnAllStarPlayComplete()
        {
            RemoveTweenListner();
            _btnNext.enabled = true;
            onTweenFinished.Invoke();
        }
    }
}
