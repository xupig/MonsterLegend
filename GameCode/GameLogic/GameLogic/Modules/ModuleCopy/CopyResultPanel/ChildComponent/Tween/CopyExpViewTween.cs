﻿using Common.Data;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyExpViewTween : KContainer
    {
        public float EnterTime = 0.3f;
        public float EndPlayTime = 0.1f;

        private Vector3 InitPosition;
        private RectTransform _rectTransform;
        private CopyCardLogic _cardLogic;

        private TweenPosition tweenPosition;
        private KDummyButton _btnNext;
        private GameObject _goPlayerInfo;
        private GameObject _goPetInfo;
        private GameObject _goTeamInfo;

        public KComponentEvent onTweenFinished = new KComponentEvent();

        protected override void Awake()
        {
            _rectTransform = gameObject.GetComponent<RectTransform>();
            InitPosition = _rectTransform.anchoredPosition;

            _btnNext = GetChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
            _goPlayerInfo = GetChild("Container_playerInfo");
            _goPetInfo = GetChild("Container_chongwujiangli");
            _goTeamInfo = GetChild("Container_duiyouxinxi");
        }

        private Action<UITweener> _OnTweenFinished;
        public void OnNext(Action<UITweener> OnTweenFinished)
        {
            _OnTweenFinished = OnTweenFinished;
            tweenPosition = TweenPosition.Begin(gameObject, EndPlayTime, new Vector3(InitPosition.x, InitPosition.y + 1000, 0));
            tweenPosition.enabled = true;
            tweenPosition.from = InitPosition;
            tweenPosition.method = UITweener.Method.EaseIn;
            tweenPosition.onFinished = OnFinish;
        }

        private void OnFinish(UITweener tween)
        {
            //如果在tween的Finish报错，就会导致下次结算时立即进入OnFinish，所以需要在这里修改enabled为false
            tweenPosition.enabled = false;
            _OnTweenFinished.Invoke(tween);
        }

        public void DoTween(CopyCardLogic cardLogic)
        {
            _cardLogic = cardLogic;
            _btnNext.enabled = false;
            DoPlayerInfoTween();
            _goTeamInfo.SetActive(false);
            _goPetInfo.SetActive(false);
            if (cardLogic.ShowTeamExp())
            {
                _goTeamInfo.SetActive(true);
                DoTeammateTween();
            }
            else
            {
                _goPetInfo.SetActive(true);
                DoPetTween();
            }
        }

        private void DoPlayerInfoTween()
        {
            _rectTransform.anchoredPosition = InitPosition;
            Vector3 playerInitPosition = _goPlayerInfo.transform.localPosition;
            TweenPosition tween = TweenPosition.Begin(_goPlayerInfo, EnterTime, playerInitPosition, OnEnterTweenFinished);
            tween.from = new Vector3(playerInitPosition.x, playerInitPosition.y - UIManager.PANEL_HEIGHT, playerInitPosition.z);
            tween.method = UITweener.Method.EaseOut;
        }

        private void DoTeammateTween()
        {
            Vector3 teamInfoPosition = _goTeamInfo.transform.localPosition;
            TweenPosition tween = TweenPosition.Begin(_goTeamInfo, EnterTime, teamInfoPosition);
            tween.from = new Vector3(teamInfoPosition.x, teamInfoPosition.y - UIManager.PANEL_HEIGHT, teamInfoPosition.z);
            tween.method = UITweener.Method.EaseOut;
        }

        private void DoPetTween()
        {
            Vector3 petInfoPosition = _goPetInfo.transform.localPosition;
            TweenPosition tween = TweenPosition.Begin(_goPetInfo, EnterTime, petInfoPosition);
            tween.from = new Vector3(petInfoPosition.x, petInfoPosition.y - UIManager.PANEL_HEIGHT, petInfoPosition.z);
            tween.method = UITweener.Method.EaseOut;
        }

        private void OnEnterTweenFinished(UITweener tween)
        {
            _btnNext.enabled = true;
            onTweenFinished.Invoke();
        }
    }
}
