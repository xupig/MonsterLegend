﻿using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyStar : KContainer
    {
        public float StarPlayTime = 0.15f;
        public float StarScale = 3f;
        public uint NextTime = 400;
        public uint StarTime = 300;

        public int Index;
        private bool _isBright;

        private KParticle _kparticle;
        private GameObject _goBrightStar;
        private GameObject _goDarkStar;

        public KComponentEvent<int> onPlayComplete = new KComponentEvent<int>();

        protected override void Awake()
        {
            _goBrightStar = GetChild("Image_xingxingIcon");
            MogoLayoutUtils.SetMiddleCenter(_goBrightStar);
            _goDarkStar = GetChild("Image_xingxingGreyIcon");
            MogoLayoutUtils.SetMiddleCenter(_goDarkStar);
            _kparticle = AddChildComponent<KParticle>("fx_ui_9_2_star_01");
            _kparticle.Stop();
        }
        
        public void SetBright(bool isBright)
        {
            _isBright = isBright;
        }

        public void DoTween()
        {
            Visible = true;
            if (_isBright)
            {
                UIManager.Instance.PlayAudio("Sound/UI/mission_grade.mp3");
                _kparticle.Play();
                _goBrightStar.SetActive(false);
                TimerHeap.AddTimer(NextTime, 0, OnNext);
                TimerHeap.AddTimer(StarTime, 0, OnShowStar);
            }
            else
            {
                _goDarkStar.SetActive(true);
                _goBrightStar.SetActive(false);
                TweenScale tween = TweenScale.Begin(_goDarkStar, StarPlayTime, Vector3.one);
                tween.from = Vector3.one * StarScale;
                tween.method = UITweener.Method.EaseOut;
                tween.onFinished = OnScaleFinish;
            }
        }

        private void OnShowStar()
        {
            _goBrightStar.SetActive(true);
        }

        private void OnNext()
        {
            onPlayComplete.Invoke(Index);
        }

        private void OnScaleFinish(UITweener tween)
        {
            onPlayComplete.Invoke(Index);
        }
    }
}
