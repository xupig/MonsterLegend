﻿using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyPetRewardGrid : KList.KListItemBase
    {
        private int _addExp;
        private PetInfo _petInfo;

        private StateText _textExp;
        private CopyProgressBar _progressBar;
        private IconContainer _goIcon;
        private StateText _textLevel;
        private StateImage _imageLevelUp;
        private ItemGrid _itemGrid;

        protected override void Awake()
        {
            _textExp = GetChildComponent<StateText>("Label_txtShuzhi");
            _progressBar = AddChildComponent<CopyProgressBar>("ProgressBar_shengming");
            _goIcon = AddChildComponent<IconContainer>("Container_icon");
            _textLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            _imageLevelUp = GetChildComponent<StateImage>("Image_jiangtouIcon");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
        }

        public override object Data
        {
            set
            {
                CopyPetExpDataWrapper wrapper = value as CopyPetExpDataWrapper;
                PetInfo petInfo = wrapper.PetInfo;
                _petInfo = petInfo;
                _addExp = wrapper.AddExp;
                _goIcon.SetIcon(pet_helper.GetIcon(petInfo.Id));
                _textLevel.CurrentText.text = petInfo.Level.ToString();
                _textExp.CurrentText.text = string.Format("x{0}", wrapper.AddExp);
                _itemGrid.SetItemData(public_config.ITEM_SPECIAL_TYPE_PET_EXP);
                RefreshExpBeforeTween(petInfo, wrapper.AddExp);
                _imageLevelUp.Alpha = 0f;
                
            }
        }

        private void RefreshExpBeforeTween(PetInfo petInfo, int addExp)
        {
            LevelConfig lastLevelConfig = pet_level_helper.GetLastLevelConfig(petInfo.Id, petInfo.Level, petInfo.Exp, addExp);
            float lastRate = pet_level_helper.GetExpPercent(petInfo.Id, lastLevelConfig.Exp, lastLevelConfig.Level);
            _progressBar.SetProgress(lastRate);
        }

        public override void Dispose()
        {
        }

        public void PlayIncreaseExp()
        {
            PetInfo petInfo = _petInfo;
            LevelConfig lastLevelConfig = pet_level_helper.GetLastLevelConfig(petInfo.Id, petInfo.Level, petInfo.Exp, _addExp);
            float nowRate = pet_level_helper.GetExpPercent(petInfo.Id, petInfo.Exp, petInfo.Level);
            float lastRate = pet_level_helper.GetExpPercent(petInfo.Id, lastLevelConfig.Exp, lastLevelConfig.Level);
            _progressBar.SetProgress(lastRate - (int)lastRate, nowRate + (petInfo.Level - lastLevelConfig.Level));
            if (petInfo.Level != lastLevelConfig.Level)
            {
                _imageLevelUp.Alpha = 1f;
            }
            else
            {
                _imageLevelUp.Alpha = 0f;
            }
        }
    }
}
