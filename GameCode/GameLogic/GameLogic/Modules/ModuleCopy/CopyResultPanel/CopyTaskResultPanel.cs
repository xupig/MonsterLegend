﻿using ACTSystem;
using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyTaskResultPanel : BasePanel
    {
        private KContainer _finishTask;
        private KParticle _finishParticle;
        private KContainer _failTask;

        private uint _timerId;

        protected override void Awake()
        {
            base.Awake();
            _finishTask = GetChildComponent<KContainer>("Container_wanchengrenwu");
            _failTask = GetChildComponent<KContainer>("Container_renwushibai");
            _finishParticle = AddChildComponent<KParticle>("fx_ui_3_3_wanchengrenwu");
            _finishTask.Visible = false;
            _finishParticle.Stop();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.CopyTaskResult; }
        }

        public override void OnShow(object data)
        {
            HideFinish();
            HideFail();
            CopyBoomLogic logic = data as CopyBoomLogic;
            if (logic.Result == CopyResult.Win)
            {
                ShowFinish();
            }
            else
            {
                ShowFail();
            }
            ResetTimer();
            _timerId = TimerHeap.AddTimer(2000, 0, DoClose);
        }

        public override void OnClose()
        {
            ResetTimer();
        }

        private void DoClose()
        {
            ResetTimer();
            ClosePanel();
            QuitMission();
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private void QuitMission()
        {
            GameSceneManager.GetInstance().LeaveCombatScene();
        }

        private void ShowFinish()
        {
            _finishTask.Visible = true;
            _finishParticle.Play();
            _finishParticle.Position = new Vector3(-311, 119, 0);
        }

        private void HideFinish()
        {
            _finishTask.Visible = false;
            _finishParticle.Stop();
        }

        private void ShowFail()
        {
            _failTask.Visible = true;
        }

        private void HideFail()
        {
            _failTask.Visible = false;
        }
    }
}
