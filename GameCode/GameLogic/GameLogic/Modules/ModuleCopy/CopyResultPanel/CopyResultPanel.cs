﻿using ACTSystem;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyResultPanel : BasePanel
    {
        private CopyCardLogic _logic;

        private CopyModelComponent _model;
        private CopyResultView _resultView;
        private CopyExpView _expView;
        private CopyCardView _cardView;

        protected override void Awake()
        {
            _resultView = AddChildComponent<CopyResultView>("Container_result");
            _expView = AddChildComponent<CopyExpView>("Container_exp");
            _cardView = AddChildComponent<CopyCardView>("Container_reward");
            _model = AddChildComponent<CopyModelComponent>("Container_model");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _resultView.onNext.AddListener(OnResultNext);
            _expView.onNext.AddListener(OnExpNext);
            _cardView.onQuit.AddListener(OnQuit);
            EventDispatcher.AddEventListener(CopyEvents.TRY_QUIT_COPY, OnQuit);
            
        }

        private void RemoveListener()
        {
            _resultView.onNext.RemoveListener(OnResultNext);
            _expView.onNext.RemoveListener(OnExpNext);
            _cardView.onQuit.RemoveListener(OnQuit);
            EventDispatcher.RemoveEventListener(CopyEvents.TRY_QUIT_COPY, OnQuit);
        }

        private void OnResultNext()
        {
            if(_logic.CanShowNext())
            {
                try
                {
                    _resultView.Visible = false;
                    _expView.Visible = true;
                    _expView.Data = _logic;
                }
                catch (Exception ex)
                {
                    LoggerHelper.Error(ex);
                    OnQuit();
                }
            }
            else
            {
                OnQuit();
            }
        }

        private void OnExpNext()
        {
            try
            {
                _expView.Visible = false;
                _cardView.Visible = true;
                _cardView.Data = _logic;
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex);
                OnQuit();
            }
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.CopyResult; }
        }

        public override void OnShow(object data)
        {
            try
            {
                CopyCardLogic cardLogic = data as CopyCardLogic;
                _logic = cardLogic;
                ShowResultView();
                _resultView.Refresh(_logic);
                _resultView.DoFirstTween(cardLogic.Result);
                _model.LoadPlayerModel(_logic.Result, LoalPlayerModelCallback);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex);
                OnQuit();
            }
        }

        public override void SetData(object data)
        {
            Debug.LogError("【副本结算错误】 在没关闭结算界面的情况下再次打开副本结算界面！");
        }

        private void ShowResultView()
        {
            _resultView.Visible = true;
            _expView.Visible = false;
            _cardView.Visible = false;
        }

        private void LoalPlayerModelCallback(ACTActor actor)
        {
            _resultView.DoSecondTween();
        }

        public override void OnClose()
        {
        }

        private void OnQuit()
        {
            if (_logic.CanPlayAgain())
            {
                LoggerHelper.Debug("PlayAgain");
                try
                {
                    MissionManager.Instance.RequestCanPlayAgain();
                }
                catch (Exception ex)
                {
                    Debug.LogError("副本再来一次报错:" + ex);
                    GameSceneManager.GetInstance().LeaveCombatScene();
                }
            }
            else
            {
                LoggerHelper.Debug("CopyResult OnQuit");
                GameSceneManager.GetInstance().LeaveCombatScene();
            }
        }
    }
}
