﻿using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.Enum;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyExpView : KContainer
    {

        private CopyCardLogic _cardLogic;
        private Vector3 PetListInitPosition;
        private Vector3 TeamListInitPosition;
        private float PetItemWidth;
        private float TeamItemWidth;
        private uint _timerId = TimerHeap.INVALID_ID;

        private StateText _textName;
        private StateText _textLevel;
        private GameObject _goLevelUp;
        private CopyProgressBar _progressBar;
        private KList _petList;
        private KDummyButton _btnNext;
        private KList _dropRewardList;
        private KList _teammateList;
        private GameObject _goPetInfo;
        private GameObject _goTeamInfo;
        private CopyExpViewTween _tween;

        public KComponentEvent onNext = new KComponentEvent();

        protected override void Awake()
        {
            _textName = GetChildComponent<StateText>("Container_playerInfo/Label_txtWwanjiamingziqigezi");
            _textLevel = GetChildComponent<StateText>("Container_playerInfo/Container_Dengjijingyan/Label_txtdengji");
            _goLevelUp = GetChild("Container_playerInfo/Container_Dengjijingyan/Container_levelUp");
            _progressBar = AddChildComponent<CopyProgressBar>("Container_playerInfo/Container_Dengjijingyan/ProgressBar_jindu");
            InitRewardList();
            _goPetInfo = GetChild("Container_chongwujiangli");
            _goTeamInfo = GetChild("Container_duiyouxinxi");
            InitPetList();
            _btnNext = AddChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
            InitTeammateList();

            _tween = gameObject.AddComponent<CopyExpViewTween>();

            AddListener();
        }

        private void InitRewardList()
        {
            _dropRewardList = GetChildComponent<KList>("Container_playerInfo/List_content");
            _dropRewardList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _dropRewardList.SetGap(0, 5);
        }

        private void InitPetList()
        {
            _petList = GetChildComponent<KList>("Container_chongwujiangli/List_content");
            _petList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _petList.SetGap(0, 15);
            PetListInitPosition = _petList.transform.localPosition;
            PetItemWidth = GetChildComponent<RectTransform>("Container_chongwujiangli/List_content/item").sizeDelta.x;
        }

        private void InitTeammateList()
        {
            _teammateList = GetChildComponent<KList>("Container_duiyouxinxi/List_content");
            _teammateList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _teammateList.SetGap(0, 15);
            TeamListInitPosition = _teammateList.transform.localPosition;
            TeamItemWidth = GetChildComponent<RectTransform>("Container_chongwujiangli/List_content/item").sizeDelta.x;
        }

        protected override void OnDisable()
        {
            if (_timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = TimerHeap.INVALID_ID;
            }
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnNext.onClick.AddListener(OnNext);
            _progressBar.Over100PercentOnce.AddListener(OnLevelUp);
            _tween.onTweenFinished.AddListener(OnTweenFinished);
        }

        private void RemoveListener()
        {
            _btnNext.onClick.RemoveListener(OnNext);
            _progressBar.Over100PercentOnce.RemoveListener(OnLevelUp);
            _tween.onTweenFinished.RemoveListener(OnTweenFinished);
        }

        private void OnNext()
        {
            _tween.OnNext(OnFinished);
        }

        private void OnFinished(UITweener tween)
        {
            onNext.Invoke();
        }

        private void OnLevelUp(int level)
        {
            RefreshLevel(level);
            _goLevelUp.SetActive(true);
        }

        public CopyCardLogic Data
        {
            set
            {
                _cardLogic = value;
                Refresh(value);
            }
        }

        private void Refresh(CopyCardLogic cardLogic)
        {
            RefreshName(PlayerAvatar.Player.name);
            RefreshReward(cardLogic.DropReward);
            Dictionary<int, int> rewardDic = PbItemHelper.ParseRewardToDic(cardLogic.DropReward);
            int exp = rewardDic.ContainsKey(SpecialItemType.EXP_PLAYER) ? rewardDic[SpecialItemType.EXP_PLAYER] : 0;
            LevelConfig lastLevel = avatar_level_helper.GetLastLevelConfig(PlayerAvatar.Player.level, PlayerAvatar.Player.exp, exp);
            RefreshLevel(lastLevel.Level);
            RefreshExpBeforeTween(lastLevel);
            if (cardLogic.ShowTeamExp())
            {
                RefreshTeammateExp(cardLogic);
            }
            else
            {
                RefreshPetExp(cardLogic.PetExpList);
            }

            _tween.DoTween(_cardLogic);
        }

        private void OnTweenFinished()
        {
            RefreshExpAfterTween(_cardLogic.DropReward);
            for (int i = 0; i < _petList.ItemList.Count; i++)
            {
                CopyPetRewardGrid petRewardGrid = _petList.ItemList[i] as CopyPetRewardGrid;
                petRewardGrid.PlayIncreaseExp();
            }
            for (int i = 0; i < _teammateList.ItemList.Count; i++)
            {
                CopyTeamExpGrid teamExpGrid = _teammateList.ItemList[i] as CopyTeamExpGrid;
                teamExpGrid.PlayIncreaseExp();
            }
            _timerId = TimerHeap.AddTimer(15000, 0, MoveNext);
        }

        private void MoveNext()
        {
            onNext.Invoke();
        }

        private void RefreshExpBeforeTween(LevelConfig lastLevel)
        {
            _goLevelUp.SetActive(false);
            float lastRate = avatar_level_helper.GetExpPercent(lastLevel.Exp, lastLevel.Level);
            _progressBar.SetProgress(lastRate);
        }

        private void RefreshName(string name)
        {
            _textName.CurrentText.text = name;
        }

        private void RefreshReward(List<PbItem> itemList)
        {
            itemList = PbItemHelper.MergePbItemList(itemList);
            List<BaseItemData> baseItemDataList = PbItemHelper.ToBaseItemDataList(itemList);
            baseItemDataList = baseItemDataList.FindAll(
                (baseItemData) =>{return baseItemData.Id != public_config.ITEM_SPECIAL_TYPE_PET_EXP;}
            );
            _dropRewardList.SetDataList<RewardItem>(baseItemDataList);
        }

        private void RefreshLevel(int level)
        {
            _textLevel.CurrentText.text = level.ToString();
        }

        private void RefreshExpAfterTween(List<PbItem> itemList)
        {
            Dictionary<int, int> rewardDic = PbItemHelper.ParseRewardToDic(itemList);
            int exp = rewardDic.ContainsKey(SpecialItemType.EXP_PLAYER) ? rewardDic[SpecialItemType.EXP_PLAYER] : 0;
            LevelConfig lastLevel = avatar_level_helper.GetLastLevelConfig(PlayerAvatar.Player.level, PlayerAvatar.Player.exp, exp);
            float nowRate = avatar_level_helper.GetExpPercent(PlayerAvatar.Player.exp, PlayerAvatar.Player.level);
            float lastRate = avatar_level_helper.GetExpPercent(lastLevel.Exp, lastLevel.Level);
            _progressBar.SetInitLevel(lastLevel.Level);
            _progressBar.SetProgress(lastRate, nowRate + (PlayerAvatar.Player.level - lastLevel.Level));
        }

        private void RefreshTeammateExp(CopyCardLogic cardLogic)
        {
            _goTeamInfo.SetActive(true);
            _teammateList.SetDataList<CopyTeamExpGrid>(cardLogic.TeammateList);
            int maxNum = 4;
            float totalWidth = maxNum * TeamItemWidth + (maxNum - 1) * _teammateList.LeftToRightGap;
            float _teamListWidth = _teammateList.GetComponent<RectTransform>().sizeDelta.x;
            _teammateList.transform.localPosition = new Vector3(TeamListInitPosition.x + ((totalWidth - _teamListWidth) / 2f),
                TeamListInitPosition.y, TeamListInitPosition.z);
        }

        private void RefreshPetExp(List<PbPetExpInfo> list)
        {
            _goPetInfo.SetActive(true);
            List<CopyPetExpDataWrapper> wrapperList = GetWrapperList(list);
            _petList.SetDataList<CopyPetRewardGrid>(wrapperList);
            float totalWidth = PetDefine.MAX_FIGHTING_PET_NUM * PetItemWidth + (PetDefine.MAX_FIGHTING_PET_NUM - 1) * _petList.LeftToRightGap;
            float petListWidth = _petList.GetComponent<RectTransform>().sizeDelta.x;
            _petList.transform.localPosition = new Vector3(PetListInitPosition.x + ((totalWidth - petListWidth) / 2f), 
                PetListInitPosition.y, PetListInitPosition.z);
        }

        private List<CopyPetExpDataWrapper> GetWrapperList(List<PbPetExpInfo> list)
        {
            List<CopyPetExpDataWrapper> wrapperList = new List<CopyPetExpDataWrapper>();
            Dictionary<int, PetInfo> petDict = PlayerDataManager.Instance.PetData.PetDict;
            for (int i = 0; i < list.Count; i++)
            {
                PbPetExpInfo petExpInfo = list[i];
                wrapperList.Add(new CopyPetExpDataWrapper(petDict[(int)petExpInfo.pet_id], (int)petExpInfo.exp));
            }
            return wrapperList;
        }
    }
}
