﻿using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyTeamExpGrid : KList.KListItemBase
    {
        private StateText _textExp;
        private StateText _nameText;
        private CopyProgressBar _progressBar;
        private IconContainer _iconContainer;
        private StateText _textLevel;
        private StateImage _imageLevelUp;
        private IconContainer _icon;

        private PbTeamMissionPlayerInfo _data;

        protected override void Awake()
        {
            _textExp = GetChildComponent<StateText>("Container_jingyan/Label_txtJingyan");
            _nameText = GetChildComponent<StateText>("Label_txtWanjiamingzi");
            _progressBar = AddChildComponent<CopyProgressBar>("ProgressBar_shengming");
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _textLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            _imageLevelUp = GetChildComponent<StateImage>("Image_jiangtouIcon");
            _icon = AddChildComponent<IconContainer>("Container_jingyan/Container_icon");
            _progressBar.Over100PercentOnce.AddListener(OnLevelUp);
        }

        private void RefreshContent()
        {
            _nameText.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.avatar_name);
            _textExp.CurrentText.text = "+" + _data.gain_exp.ToString();
            _icon.SetIcon(item_helper.GetIcon(public_config.ITEM_SPECIAL_TYPE_EXP));
            _iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_data.vocation));
            LevelConfig lastLevel = avatar_level_helper.GetLastLevelConfig((int)_data.level, (int)_data.exp, (int)_data.gain_exp);
            RefreshLevel(lastLevel.Level);
            RefreshExpBeforeEnter(lastLevel);
        }

        private void RefreshExpBeforeEnter(LevelConfig lastLevel)
        {
            float lastRate = avatar_level_helper.GetExpPercent(lastLevel.Exp, lastLevel.Level);
            _progressBar.SetProgress(lastRate);
            _imageLevelUp.gameObject.SetActive(false);
        }

        public void PlayIncreaseExp()
        {
            LevelConfig lastLevel = avatar_level_helper.GetLastLevelConfig((int)_data.level, (int)_data.exp, (int)_data.gain_exp);
            float nowRate = avatar_level_helper.GetExpPercent((int)_data.exp, (int)_data.level);
            float lastRate = avatar_level_helper.GetExpPercent(lastLevel.Exp, lastLevel.Level);
            _progressBar.SetInitLevel(lastLevel.Level);
            _progressBar.SetProgress(lastRate, nowRate + (_data.level - lastLevel.Level));
        }

        private void OnLevelUp(int level)
        {
            RefreshLevel(level);
            _imageLevelUp.gameObject.SetActive(true);
        }

        private void RefreshLevel(int level)
        {
            _textLevel.CurrentText.text = level.ToString();
        }

        public override void Dispose()
        {
            base.Data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null && value != base.Data)
                {
                    _data = value as PbTeamMissionPlayerInfo;
                    RefreshContent();
                }
            }
        }

    }
}
