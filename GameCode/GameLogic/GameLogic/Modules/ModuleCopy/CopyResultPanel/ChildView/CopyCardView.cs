﻿using Common.Base;
using Common.Data;
using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.ExtendComponent;
using GameLoader.Utils;

namespace ModuleCopy
{
    public class CopyCardView : KContainer
    {
        private const int AUTO_GET_SECOND = 10;
        public const int MAX_CARD_NUM = 5;

        private CopyCardLogic _cardLogic;

        private List<CopyCardRewardData> _rewardDataList = new List<CopyCardRewardData>();
        private List<CopyCardRewardData> _gotRewardDataList = new List<CopyCardRewardData>();
        private List<CopyCardRewardData> _ungetRewardDataList = new List<CopyCardRewardData>();

        private StateText _textCountdown;
        private KShrinkableButton _btnSure;
        private KShrinkableButton _statisticBtn;
        private CopyCard[] _cardArray;
        private Countdown _countdown;
        private StateText _textTitle;
        private CopyCardViewTween _tween;

        private Vector3 CENTER_POSITION = new Vector3(390, -516, 0);
        private Vector3 OFFSET = new Vector3(190, 0, 0);

        public KComponentEvent onQuit = new KComponentEvent();

        protected override void Awake()
        {
            _textTitle = GetChildComponent<StateText>("Container_biaoti/Label_txtSanxing");
            _textCountdown = GetChildComponent<StateText>("Label_txtShijian");
            _btnSure = GetChildComponent<KShrinkableButton>("Button_queding");
            _statisticBtn = GetChildComponent<KShrinkableButton>("Button_zhandoutongji");
            InitCountDown();
            InitCardList();
            _tween = gameObject.AddComponent<CopyCardViewTween>();

            AddListener();
        }

        private void InitCountDown()
        {
            _countdown = gameObject.AddComponent<Countdown>();
            _countdown.SetTemplate(
                string.Format("{0}{1}",
                    ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, "{0}"),
                    ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_WHITE, " 秒后自动选择"))
            );
        }

        private void InitCardList()
        {
            _cardArray = new CopyCard[MAX_CARD_NUM];
            for (int i = 0; i < MAX_CARD_NUM; i++)
            {
                CopyCard copyCard = AddChildComponent<CopyCard>(string.Format("Container_fanpai/Toggle_kapai0{0}", i + 1));
                _cardArray[i] = copyCard;
            }
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnSure.onClick.AddListener(OnSure);
            _statisticBtn.onClick.AddListener(ShowStatisticPanel);
            _countdown.onEnd.AddListener(OnFinishing);
            _tween.OnShuffleCardFinish.AddListener(OnShuffleCardFinish);
            for (int i = 0; i < MAX_CARD_NUM; i++)
            {
                CopyCard copyCard = _cardArray[i];
                copyCard.onClick.AddListener(OnClickCard);
                copyCard.onOpenedCard.AddListener(OnOpenedCard);
            }
        }

        private void RemoveListener()
        {
            _btnSure.onClick.RemoveListener(OnSure);
            _statisticBtn.onClick.RemoveListener(ShowStatisticPanel);
            _countdown.onEnd.RemoveListener(OnFinishing);
            _tween.OnShuffleCardFinish.RemoveListener(OnShuffleCardFinish);
            for (int i = 0; i < MAX_CARD_NUM; i++)
            {
                CopyCard copyCard = _cardArray[i];
                copyCard.onClick.RemoveListener(OnClickCard);
                copyCard.onOpenedCard.RemoveListener(OnOpenedCard);
            }
        }

        private void OnSure()
        {
            onQuit.Invoke();
        }

        private void ShowStatisticPanel()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.BattleDataStatistic);
        }

        private void OnShuffleCardFinish()
        {
            _countdown.StartCountdown(AUTO_GET_SECOND, _textCountdown);
        }

        public CopyCardLogic Data
        {
            set
            {
                _cardLogic = value;
                InitRewardItemList(value);
                RefreshButtonPosition();
                _btnSure.Visible = false;
                _statisticBtn.Visible = false;
                _textCountdown.Clear();
                RefreshTitle(_cardLogic.StarNum);
                RefreshCardList();

                _tween.DoTween();
            }
        }

        private void RefreshButtonPosition()
        {
            int insId = GameSceneManager.GetInstance().curInstanceID;
            instance ins = instance_helper.GetInstanceCfg(insId);
            if(instance_helper.GetStatisticType(ins) != 0)
            {
                _btnSure.transform.localPosition =  CENTER_POSITION + OFFSET;
                _statisticBtn.transform.localPosition = CENTER_POSITION - OFFSET;
            }
            else
            {
                _btnSure.transform.localPosition =  CENTER_POSITION;
            }

            _btnSure.RefreshRectTransform();
            _statisticBtn.RefreshRectTransform();
        }

        private void RefreshTitle(int starNum)
        {
            string starNumStr = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GOLDEN, MogoLanguageUtil.GetContent(1433, starNum));
            string cardNumStr = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GOLDEN, MogoLanguageUtil.GetContent(1434, starNum));
            _textTitle.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_WHITE);
            _textTitle.CurrentText.text = MogoLanguageUtil.GetContent(1435, starNumStr, cardNumStr);
        }

        private void RefreshCardList()
        {
            for (int i = 0; i < _cardArray.Length; i++)
            {
                _cardArray[i].Refresh(_rewardDataList[i].ShowReward);
            }
        }

        private BaseItemData CreateItemData(uint itemId, uint count)
        {
            BaseItemData baseItemData = ItemDataCreator.Create((int)itemId);
            baseItemData.StackCount = (int)count;
            return baseItemData;
        }

        private void InitRewardItemList(CopyCardLogic cardLogic)
        {
            if (cardLogic.BoxReward.Count != 5)
            {
                Debug.LogError("接受服务器协议数据错误 instanceId:" + cardLogic.MissionId + "  mapId:" + cardLogic.MapId + "   count:" + cardLogic.BoxReward.Count);
                for (int i = 0; i < cardLogic.BoxReward.Count; i++)
                {
                    PbBoxReward boxReward = cardLogic.BoxReward[i];
                    Debug.LogError(string.Format("{0}   {1}   {2}   {3}", boxReward.box_id, boxReward.box_count, boxReward.item_id, boxReward.item_count));
                }
            }

            _rewardDataList.Clear();
            _ungetRewardDataList.Clear();
            _gotRewardDataList.Clear();
            for (int i = 0; i < cardLogic.BoxReward.Count; i++)
            {
                PbBoxReward boxReward = cardLogic.BoxReward[i];
                CopyCardRewardData copyCardRewardData = new CopyCardRewardData();
                copyCardRewardData.ShowReward = CreateItemData(boxReward.box_id, boxReward.box_count);
                if (boxReward.item_id != 0)
                {
                    copyCardRewardData.RealReward = CreateItemData(boxReward.item_id, boxReward.item_count);
                }
                _rewardDataList.Add(copyCardRewardData);
                if (i < cardLogic.StarNum)
                {
                    _gotRewardDataList.Add(copyCardRewardData);
                }
                else
                {
                    _ungetRewardDataList.Add(copyCardRewardData);
                }
            }
            MogoRandomUtils.RandomList(_rewardDataList);
            MogoRandomUtils.RandomList(_ungetRewardDataList);
            MogoRandomUtils.RandomList(_gotRewardDataList);
            if (_gotRewardDataList.Count != cardLogic.StarNum)
            {
                LoggerHelper.Error(string.Format("获得奖励数量不等于星级数，获得奖励数量为{0}，但星级是{1}。", _gotRewardDataList.Count, cardLogic.StarNum));
            }
        }

        private void OnClickCard(CopyCard rewardCard)
        {
            try
            {
                int openNum = GetOpenCardNum();
                rewardCard.OpenCard(_gotRewardDataList[openNum].GetRealReward(), true);
                if (GetOpenCardNum() >= _gotRewardDataList.Count)
                {
                    OnFinishing();
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex);
                onQuit.Invoke();
            }
        }

        private void OnOpenedCard()
        {
            int openedNum = GetOpenedCardNum();
            if (openedNum >= MAX_CARD_NUM)
            {
                OnFinish();
            }
            else if (openedNum >= _cardLogic.StarNum)
            {
                _countdown.StopCountdown();
                _textCountdown.Clear();
                ShowUnopenCard();
            }
        }

        /// <summary>
        /// 两种情况会触发OnFinishing
        /// 1、到达自动翻牌的时间
        /// 2、能翻的牌已经翻完
        /// </summary>
        private void OnFinishing()
        {
            for (int i = 0; i < MAX_CARD_NUM; i++)
            {
                _cardArray[i].SetCanNotClick();
            }
            _countdown.StopCountdown();
            _textCountdown.Clear();
            ShowCanOpenCard();
        }

        /// <summary>
        /// 所有卡牌都翻到正面，等待玩家按确认按钮
        /// </summary>
        private void OnFinish()
        {
            _btnSure.Visible = true;
            RefreshStatisticButton();
        }

        private void ShowCanOpenCard()
        {
            for (int i = 0; i < _cardArray.Length; i++)
            {
                if (!_cardArray[i].IsOpen)
                {
                    int openNum = GetOpenCardNum();
                    if (openNum >= _cardLogic.StarNum)
                    {
                        break;
                    }
                    _cardArray[i].OpenCard(_gotRewardDataList[openNum].GetRealReward(), true);
                }
            }
        }

        private void ShowUnopenCard()
        {
            int index = 0;
            for (int i = 0; i < _cardArray.Length; i++)
            {
                if (!_cardArray[i].IsOpen)
                {
                    _cardArray[i].OpenCard(_ungetRewardDataList[index].GetRealReward());
                    index++;
                }
            }
        }

        private int GetOpenCardNum()
        {
            int openNum = 0;
            for (int i = 0; i < _cardArray.Length; i++)
            {
                if (_cardArray[i].IsOpen)
                {
                    openNum++;
                }
            }
            return openNum;
        }

        private int GetOpenedCardNum()
        {
            int openedNum = 0;
            for (int i = 0; i < _cardArray.Length; i++)
            {
                if (_cardArray[i].IsOpened)
                {
                    openedNum++;
                }
            }
            return openedNum;
        }


        private void RefreshStatisticButton()
        {
            int insId = GameSceneManager.GetInstance().curInstanceID;
            instance ins = instance_helper.GetInstanceCfg(insId);
            _statisticBtn.Visible = instance_helper.GetStatisticType(ins) != 0;
        }


        public class CopyCardRewardData
        {
            public BaseItemData ShowReward;
            public BaseItemData RealReward;

            public BaseItemData GetRealReward()
            {
                if (RealReward != null)
                {
                    return RealReward;
                }
                return ShowReward;
            }

            public override string ToString()
            {
                string showRewardStr = "showReward:" + ShowReward.ToString();
                string realRewardStr = "realReward:";
                if (RealReward == null)
                {
                    realRewardStr += "null";
                }
                else
                {
                    realRewardStr += RealReward.ToString();
                }
                return string.Format("{0}   {1}", showRewardStr, realRewardStr);
            }
        }
    }
}
