﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyResultView : KContainer
    {
        private uint _timerId = TimerHeap.INVALID_ID;

        private CopyStarList _starList;
        private GameObject _goWin;
        private GameObject _goFail;
        private CopyPassConditionListInBattle _passConditionList;
        private KParticle _particle;
        private KDummyButton _btnNext;
        private CopyResultViewTween _tween;
        private CopyCardLogic _cardLogic;

        public KComponentEvent onNext = new KComponentEvent();

        protected override void Awake()
        {
            _starList = AddChildComponent<CopyStarList>("Container_xingxing");
            _goWin = GetChild("Container_shengli");
            MogoLayoutUtils.SetMiddleCenter(_goWin, 2);
            _goFail = GetChild("Container_shibai");
            MogoLayoutUtils.SetMiddleCenter(_goFail, 2);
            _passConditionList = AddChildComponent<CopyPassConditionListInBattle>("Container_shenglitiaojian");
            _btnNext = AddChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
            _particle = AddChildComponent<KParticle>("Container_shengli/fx_ui_9_3_zhandoushengli_01");
            _particle.transform.localPosition = new Vector3(-318, 248, 0);
            _particle.Stop();

            _tween = gameObject.AddComponent<CopyResultViewTween>();

            AddListner();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListner()
        {
            _btnNext.onClick.AddListener(OnNext);
            _tween.onTweenFinished.AddListener(OnTweenFinish);
        }

        private void RemoveListener()
        {
            _btnNext.onClick.RemoveListener(OnNext);
            _tween.onTweenFinished.RemoveListener(OnTweenFinish);
        }

        protected override void OnDisable()
        {
            if (_timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = TimerHeap.INVALID_ID;
            }
        }

        private void OnNext()
        {
            _tween.OnNext(OnFinished);
        }

        private void OnFinished(UITweener tween)
        {
            if (_cardLogic.CanShowStatistic() && _cardLogic.Result == CopyResult.Win)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.BattleDataStatistic);
            }
            else
            {
                onNext.Invoke();
            }
        }

        private void OnTweenFinish()
        {
            _timerId = TimerHeap.AddTimer(15000, 0, MoveNext);
        }

        public void Refresh(CopyCardLogic cardLogic)
        {
            _cardLogic = cardLogic;
            if (cardLogic.Result == CopyResult.Win)
            {
                _goWin.SetActive(true);
                UIManager.Instance.PlayAudio("Sound/UI/fight_victory.mp3");
            }
            else
            {
                _goFail.SetActive(true);
                UIManager.Instance.PlayAudio("Sound/UI/fight_fail.mp3");
            }
            _starList.SetStarNum(cardLogic.StarNum);
            _passConditionList.Refresh(cardLogic);
        }

        public void DoFirstTween(CopyResult copyResult)
        {
            _tween.DoFirstTween(copyResult);
        }

        public void DoSecondTween()
        {
            _tween.DoSecondTween();
        }

        private void MoveNext()
        {
            onNext.Invoke();
        }
    }
}
