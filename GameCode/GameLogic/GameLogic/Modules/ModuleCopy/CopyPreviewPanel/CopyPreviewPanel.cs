﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using UnityEngine;
using MogoEngine.Events;
using Common.Events;
using Common.Utils;
using Common.ServerConfig;
using Common.Structs;
using System.Collections.Generic;

namespace ModuleCopy
{
    public class CopyPreviewPanel : BasePanel
    {
        private int CopySweepItemId = 0;

        private CopyInstanceData _instanceData;

        private CopyPreviewRight _right;
        private CopyTeamView _teamView;
        private CopyHead _head;
        private KToggleGroup _toggleGroup;
        private List<CopyPreviewToggle> _previewToggleList;

        private List<GameObject> _goNormalBgList;
        private List<GameObject> _goEliteBgList;

        private MoneyItem _moneyItem;
        private StateText _textSweepCard;

        private CopyData _copyData
        {
            get
            {
                return PlayerDataManager.Instance.CopyData;
            }
        }

        protected override void Awake()
        {
            InitBtn();
            _right = AddChildComponent<CopyPreviewRight>("Container_right");
            _textSweepCard = GetChildComponent<StateText>("Container_saodangjuan/Label_txtShuzhi");
            _head = AddChildComponent<CopyHead>("Container_left");
            _teamView = AddChildComponent<CopyTeamView>("Container_left/Container_duiyou");
            InitDifficultyList();
            InitToggleGroup();
            AddChildComponent<CopyBackground>("Container_bg");
            InitIcon();
        }

        private void InitDifficultyList()
        {
            _goNormalBgList = new List<GameObject>();
            _goNormalBgList.Add(GetChild("Container_left/Container_bg/ScaleImage_sharedTipKuang02"));
            _goNormalBgList.Add(GetChild("Container_right/Container_panelBg"));
            _goEliteBgList = new List<GameObject>();
            _goEliteBgList.Add(GetChild("Container_left/Container_bg/ScaleImage_TipKuang03"));
            _goEliteBgList.Add(GetChild("Container_right/Container_panelBg02"));
        }

        private void InitBtn()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _moneyItem = AddChildComponent<MoneyItem>("Container_tili");
            _moneyItem.SetMoneyType(public_config.MONEY_TYPE_ENERGY);
        }

        private void InitToggleGroup()
        {
            _toggleGroup = GetChildComponent<KToggleGroup>("Container_left/ToggleGroup_nandu");
            _previewToggleList = new List<CopyPreviewToggle>();
            List<KToggle> toggleList = _toggleGroup.GetToggleList();
            for (int i = 0; i < toggleList.Count; i++)
            {
                CopyPreviewToggle previewToggle = toggleList[i].gameObject.AddComponent<CopyPreviewToggle>();
                _previewToggleList.Add(previewToggle);
            }
        }

        private void InitIcon()
        {
            CopySweepItemId = inst_type_operate_helper.GetSweepItemId(ChapterType.Copy);
            AddChildComponent<IconContainer>("Container_tili/Container_icon").SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_ENERGY));
            GetChildComponent<ItemGrid>("Container_saodangjuan/Container_wupin").SetItemData(CopySweepItemId);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.CopyPreview; }
        }

        public override void OnShow(object data)
        {
            _instanceData = data as CopyInstanceData;
            List<instance> instanceList = instance_helper.GetGroupInstanceList(_instanceData.id);
            for (int i = 0; i < _previewToggleList.Count; i++)
            {
                if (i < instanceList.Count)
                {
                    _previewToggleList[i].Visible = true;
                    _previewToggleList[i].InstanceId = instanceList[i].__id;
                }
                else
                {
                    _previewToggleList[i].Visible = false;
                    _previewToggleList[i].InstanceId = instance_helper.InvalidInstanceId;
                }
            }
            AddListener();
            _toggleGroup.SelectIndex = 0;
            Refresh(true);
        }

        public override void OnClose()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _moneyItem.onAddClick = OnBuyEnergy;
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, OnItemUpdate);
            EventDispatcher.AddEventListener(CopyEvents.SHOW_CONFIRM, ShowConfirm);
            EventDispatcher.AddEventListener(CopyEvents.UpdatePreview, OnRefresh);
            EventDispatcher.AddEventListener(MissionEvents.MISSION_SWEEP_SUCCESSFUL, OnRefresh);
            EventDispatcher.AddEventListener(TeamInstanceEvents.On_Call_People_Succeed, OnCallPeopelSucceed);
        }

        private void RemoveListener()
        {
            _moneyItem.onAddClick = null;
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, OnItemUpdate);
            EventDispatcher.RemoveEventListener(CopyEvents.SHOW_CONFIRM, ShowConfirm);
            EventDispatcher.RemoveEventListener(CopyEvents.UpdatePreview, OnRefresh);
            EventDispatcher.RemoveEventListener(MissionEvents.MISSION_SWEEP_SUCCESSFUL, OnRefresh);
            EventDispatcher.RemoveEventListener(TeamInstanceEvents.On_Call_People_Succeed, OnCallPeopelSucceed);
        }

        private void OnCallPeopelSucceed()
        {
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83026), PanelIdEnum.MainUIField);
        }

        private void ShowConfirm()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceData.id);
            InstanceTeamType teamType = instance_helper.GetTeamType(_instanceData.id);
            if ((teamType == InstanceTeamType.SinglePlayer) && (PlayerDataManager.Instance.TeamData.TeammateDic.Count > 0))
            {
                string content = MogoLanguageUtil.GetContent(1008, instance_helper.GetInstanceName(_instanceData.id));
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, onConfirm, onCancel);
            }
        }

        private void onConfirm()
        {
            MissionManager.Instance.RequsetEnterMission(_instanceData.id, 0, 1);
        }

        private void onCancel(){}

        private void OnItemUpdate(BagType type, uint pos)
        {
            if (type == BagType.ItemBag)
            {
                BaseItemInfo itemInfo = PlayerDataManager.Instance.BagData.GetBagData(type).GetItemInfo((int)pos);
                if (itemInfo.Id == CopySweepItemId)
                {
                    RefreshSweepCard();
                }
            }
        }

        private void RefreshSweepCard()
        {
            int itemNum = PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemNum(CopySweepItemId);
            _textSweepCard.CurrentText.text = itemNum.ToString();
        }

        private void RefreshDiffcultyBg()
        {
            if (_toggleGroup.SelectIndex == 0)
            {
                for (int i = 0; i < _goNormalBgList.Count; i++)
                {
                    _goNormalBgList[i].SetActive(true);
                }
                for (int i = 0; i < _goEliteBgList.Count; i++)
                {
                    _goEliteBgList[i].SetActive(false);
                }
            }
            else
            {
                for (int i = 0; i < _goNormalBgList.Count; i++)
                {
                    _goNormalBgList[i].SetActive(false);
                }
                for (int i = 0; i < _goEliteBgList.Count; i++)
                {
                    _goEliteBgList[i].SetActive(true);
                }
            }
        }

        private void OnBuyEnergy()
        {
            PanelIdEnum.EnergyBuying.Show();
        }

        private void OnSelectedIndexChanged(KToggleGroup toggleGroup, int index)
        {
            CopyPreviewToggle toggle = _previewToggleList[index];
            _instanceData = _copyData.GetInstanceData(toggle.InstanceId);
            Refresh(false);
        }

        private void OnRefresh()
        {
            Refresh(false);
        }

        private void Refresh(bool doTween)
        {
            _right.SetData(_instanceData);
            _head.Refresh(_instanceData.id, _instanceData.HistoryMaxStar);
            RefreshSweepCard();
            RefreshDiffcultyBg();
            RefreshTeamView();
            if (doTween)
            {
                DoTween();
            }
        }

        private void RefreshTeamView()
        {
            InstanceTeamType teamType = instance_helper.GetTeamType(_instanceData.id);
            if (teamType == InstanceTeamType.Team)
            {
                _teamView.Visible = true;
                _teamView.RefreshContent(_instanceData.id);
            }
            else
            {
                _teamView.Visible = false;
            }
        }

        private void DoTween()
        {
            TweenViewMove.Begin(_head.gameObject, MoveType.Show, MoveDirection.Left);
            TweenViewMove.Begin(_right.gameObject, MoveType.Show, MoveDirection.Right);
        }
    }
}
