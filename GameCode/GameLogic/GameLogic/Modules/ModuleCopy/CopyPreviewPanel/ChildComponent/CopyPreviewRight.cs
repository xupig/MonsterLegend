﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCopy
{
    public class CopyPreviewRight : KContainer
    {
        private CopyInstanceData _instanceData;

        private KButton _btnFight;
        private KButton _btnReset;
        private KButton _btnSweepOnce;
        private KButton _btnSweepAll;
        private KButton _btnCall;
        private StateText _textEnergyFightCost;
        private StateText _textEnergySweepCost;
        private KContainer _containerSweep;
        private StateText _textMultiSweep;

        private CopyFightConditionList _fightConditionList;
        private CopyPassConditionList _passConditionList;
        private CopyPreviewReward _reward;

        private CopyData _copyData
        {
            get
            {
                return PlayerDataManager.Instance.CopyData;
            }
        }

        protected override void Awake()
        {
            _textEnergyFightCost = GetChildComponent<StateText>("Container_bottomBtn/Label_txtXuyaotili");
            _textEnergySweepCost = GetChildComponent<StateText>("Container_bottomBtn/Container_saodang/Label_txtXuyaotili");
            _containerSweep = GetChildComponent<KContainer>("Container_bottomBtn/Container_saodang");
            _fightConditionList = AddChildComponent<CopyFightConditionList>("Container_tiaozhanyaoqiu");
            _passConditionList = AddChildComponent<CopyPassConditionList>("Container_xuyaoxiaohao/Container_tiaojian");
            _reward = AddChildComponent<CopyPreviewReward>("Container_zhuangbei");

            InitBtn();
        }

        private void InitBtn()
        {
            GetChildComponent<KButton>("Container_upperBtn/Button_wenhao").Visible = false;
            _btnFight = GetChildComponent<KButton>("Container_bottomBtn/Button_tiaozhanPutong");
            _btnReset = GetChildComponent<KButton>("Container_bottomBtn/Button_chongzhiPutong");
            _btnSweepOnce = GetChildComponent<KButton>("Container_bottomBtn/Container_saodang/Button_saodang");
            _btnSweepAll = GetChildComponent<KButton>("Container_bottomBtn/Container_saodang/Button_saodangNci");
            _btnCall = GetChildComponent<KButton>("Container_bottomBtn/Button_hanren");
            _textMultiSweep = GetChildComponent<StateText>("Container_bottomBtn/Container_saodang/Button_saodangNci/label");
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnFight.onClick.AddListener(OnFight);
            _btnCall.onClick.AddListener(OnCall);
            _btnSweepAll.onClick.AddListener(OnSweepN);
            _btnSweepOnce.onClick.AddListener(OnSweepOnce);
            _btnReset.onClick.AddListener(OnReset);
        }

        private void RemoveListener()
        {
            _btnFight.onClick.RemoveListener(OnFight);
            _btnCall.onClick.RemoveListener(OnCall);
            _btnSweepAll.onClick.RemoveListener(OnSweepN);
            _btnSweepOnce.onClick.RemoveListener(OnSweepOnce);
            _btnReset.onClick.RemoveListener(OnReset);
        }

        private void OnFight()
        {
            if (_copyData.GetDailyLeftTimes(_instanceData.id) <= 0)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(1416), PanelIdEnum.Copy);
                return;
            }
            CopyLogicManager.Instance.RequestEnterCopy(ChapterType.Copy, RequestEnterCopy, _instanceData.id);
        }

        private void RequestEnterCopy()
        {
            MissionManager.Instance.RequsetEnterMission(_instanceData.id);
        }

        private void OnCall()
        {
            TeamInstanceManager.Instance.CallPeople(team_helper.GetGameIdByInstanceId(_instanceData.id));
        }

        private void OnSweepOnce()
        {
            if (!_copyData.CheckLeftSweepNum(instance_helper.GetChapterType(_instanceData.id)))
            {
                return;
            }
            MissionManager.Instance.RequsetCleanUpMission(_instanceData.id, 1);
        }

        private void OnSweepN()
        {
            if (!_copyData.CheckLeftSweepNum(instance_helper.GetChapterType(_instanceData.id)))
            {
                return;
            }
            int leftSweepNum = _copyData.GetLeftSweepNum(instance_helper.GetChapterType(_instanceData.id));
            int multiMaxSweepTimes = global_params_helper.GetCopySweepMaxTimes();
            int multiSweepNum = Mathf.Min(multiMaxSweepTimes, leftSweepNum, _instanceData.GetLeftDailyEnterTimes());
            MissionManager.Instance.RequsetCleanUpMission(_instanceData.id, multiSweepNum);
        }
        private void OnReset()
        {
            int resetTimes = _copyData.GetResetTimes(ChapterType.Copy, _instanceData.id);
            int leftResetTimes = int.Parse(vip_helper.GetCopyMaxResetTimes(PlayerAvatar.Player.vip_level)) - resetTimes;
            if (leftResetTimes <= 0)
            {
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(36017),
                    BuyDiamond, CancelReset, MogoLanguageUtil.GetContent(36018), MogoLanguageUtil.GetContent(36019));
                return;
            }

            BaseItemData baseItemData = price_list_helper.GetCost((int)PriceListId.copyReset, resetTimes + 1);
            MogoTipsUtils.ShowTips(baseItemData, MogoLanguageUtil.GetContent(1445), OnConfirmReset);
        }

        private void OnConfirmReset()
        {
            MissionManager.Instance.RequestResetTimes(_instanceData.id);
        }

        private void CancelReset()
        {
            MessageBox.CloseMessageBox();
        }

        private void BuyDiamond()
        {
            MessageBox.CloseMessageBox();
        }

        public void SetData(CopyInstanceData instanceData)
        {
            _instanceData = instanceData;
            _fightConditionList.Refresh(_instanceData.id, _instanceData.DailyTimes);
            _passConditionList.Refresh(_instanceData.id);
            _reward.Refresh(_instanceData.id);
            RefreshCallButton();
            RefreshSweepButton();
            RefreshFightButton();
        }

        private void RefreshCallButton()
        {
            InstanceTeamType teamType = instance_helper.GetTeamType(_instanceData.id);
            if (teamType == InstanceTeamType.Team && !_copyData.IsInstanceThreeStarPass(_instanceData.id))
            {
                _btnCall.Visible = true;
            }
            else
            {
                _btnCall.Visible = false;
            }
        }

        private void RefreshSweepButton()
        {
            int multiMaxSweepTimes = global_params_helper.GetCopySweepMaxTimes();
            int leftSweepNum = _copyData.GetLeftSweepNum(instance_helper.GetChapterType(_instanceData.id));
            int multiSweepNum = Mathf.Min(multiMaxSweepTimes, leftSweepNum, _instanceData.GetLeftDailyEnterTimes());
            if (_instanceData.HistoryMaxStar == CopyData.MAX_STAR_NUM_PER_INSTANCE)
            {
                _containerSweep.Visible = true;
                _textEnergySweepCost.CurrentText.text = MogoLanguageUtil.GetContent(1432, instance_helper.GetCostEnergy(_instanceData.id) * multiSweepNum);
            }
            else
            {
                _containerSweep.Visible = false;
            }
            _textEnergyFightCost.CurrentText.text = MogoLanguageUtil.GetContent(1432, instance_helper.GetCostEnergy(_instanceData.id));
            _textMultiSweep.ChangeAllStateText(MogoLanguageUtil.GetContent(1420, multiSweepNum));
            if (_instanceData.GetLeftDailyEnterTimes() == 0)
            {
                _textEnergyFightCost.Clear();
            }
        }

        private void RefreshFightButton()
        {
            if ((instance_helper.GetDailyTimes(_instanceData.id) - _instanceData.DailyTimes) == 0)
            {
                _btnReset.Visible = true;
                _btnFight.Visible = false;
            }
            else
            {
                _btnReset.Visible = false;
                _btnFight.Visible = true;
            }
        }
    }
}
