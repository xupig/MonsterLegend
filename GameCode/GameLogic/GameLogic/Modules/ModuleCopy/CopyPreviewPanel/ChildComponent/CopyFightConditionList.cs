﻿using Common.Data;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.ExtendComponent
{
    public class CopyFightConditionList : KContainer
    {
        private StateText _textRecommendLevel;
        private StateText _textEnterTimes;
        private StateText _textSweepTimes;
        private StateText _textEnterNum;
        private StateImage _imageSuggestion;

        protected override void Awake()
        {
            _textRecommendLevel = GetChildComponent<StateText>("Container_command/Container_NR00/Label_txtXuyaodengji");
            _textEnterTimes = GetChildComponent<StateText>("Container_command/Container_NR01/Label_txtJinrucishu");
            _textSweepTimes = GetChildComponent<StateText>("Container_command/Container_NR02/Label_txtShaodangcishuo");
            _textEnterNum = GetChildComponent<StateText>("Container_command/Container_NR03/Label_txtJinrurenshu");
            _imageSuggestion = GetChildComponent<StateImage>("Image_jianyizudui");
        }

        public void Refresh(int instanceId, int dailyTimes)
        {
            RefreshLevel(instanceId);
            RefreshEnterTimes(instanceId, dailyTimes);
            RefreshSweepTimes(instanceId);
            RefreshEnterNum(instanceId);
            RefreshSuggestion(instanceId);
        }

        private void RefreshSuggestion(int instanceId)
        {
            InstanceTeamType teamType = instance_helper.GetTeamType(instanceId);
            if (teamType == InstanceTeamType.Team && !PlayerDataManager.Instance.CopyData.IsInstanceThreeStarPass(instanceId))
            {
                _imageSuggestion.Alpha = 1f;
            }
            else
            {
                _imageSuggestion.Alpha = 0;
            }
        }

        private void RefreshLevel(int instanceId)
        {
            instance instance = instance_helper.GetInstanceCfg(instanceId);
            int minLevel = instance_helper.GetMinLevel(instance);
            string minLevelStr = minLevel.ToString();
            if (PlayerAvatar.Player.level < minLevel)
            {
                minLevelStr = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, minLevelStr);
            }
            _textRecommendLevel.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(1422), minLevelStr);
        }

        private void RefreshEnterTimes(int instanceId, int dailyTimes)
        {
            int leftEnterTimes = instance_helper.GetDailyTimes(instanceId) - dailyTimes;
            string leftEnterTimesStr = leftEnterTimes.ToString();
            if (leftEnterTimes <= 0)
            {
                leftEnterTimesStr = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, leftEnterTimesStr);
            }
            string timesStr = string.Format("{0}/{1}", leftEnterTimesStr, instance_helper.GetDailyTimes(instanceId));
            _textEnterTimes.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(1429), timesStr);
        }

        private void RefreshSweepTimes(int instanceId)
        {
            int dailyMaxSweepNum = int.Parse(vip_helper.GetDailyMaxSweepNum(PlayerAvatar.Player.vip_level));
            string leftSweepTimes = PlayerAvatar.Player.mission_mop_up_num.ToString();
            if (PlayerAvatar.Player.mission_mop_up_num <= 0)
            {
                leftSweepTimes = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, leftSweepTimes);
            }
            string timesStr = string.Format("{0}/{1}", leftSweepTimes, dailyMaxSweepNum);
            if (PlayerAvatar.Player.vip_level > 0)
            {
                _textSweepTimes.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(1437), PlayerAvatar.Player.vip_level, timesStr);
            }
            else
            {
                _textSweepTimes.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(49999), leftSweepTimes, dailyMaxSweepNum);
            }
        }

        private void RefreshEnterNum(int instanceId)
        {
            instance instance = instance_helper.GetInstanceCfg(instanceId);
            int minPlayerNum = instance_helper.GetMinPlayerNum(instance);
            int maxPlayerNum = instance_helper.GetMaxPlayerNum(instance);
            string numStr = string.Empty;
            if(minPlayerNum != maxPlayerNum)
            {
                numStr = string.Format("{0}~{1}", minPlayerNum, maxPlayerNum);
            }
            else
            {
                numStr = minPlayerNum.ToString();
            }
            _textEnterNum.CurrentText.text = MogoLanguageUtil.GetContent(10121, numStr); 
        }
    }
}
