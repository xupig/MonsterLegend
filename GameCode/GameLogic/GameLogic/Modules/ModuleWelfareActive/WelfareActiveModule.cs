﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using GameData;

namespace ModuleWelfareActive
{
    /// <summary>
    /// 福利活动系统
    /// </summary>
    public class WelfareActiveModule : BaseModule
    {
        public WelfareActiveModule() : base()
        {
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            RegisterPanelModule(PanelIdEnum.WelfareActiveTab, PanelIdEnum.WelfareActiveTab);
            RegisterPanelModule(PanelIdEnum.LuckyTurntable, PanelIdEnum.WelfareActiveTab);
            RegisterPanelModule(PanelIdEnum.ChargeActivity, PanelIdEnum.WelfareActiveTab);
            RegisterPanelModule(PanelIdEnum.WelfareActive, PanelIdEnum.WelfareActiveTab);
            RegisterPanelModule(PanelIdEnum.InviteFriendRecord, PanelIdEnum.WelfareActiveTab);
            RegisterPopPanelBunchPanel(PanelIdEnum.LuckyTurntable, PanelIdEnum.WelfareActive);
            //RegisterPopPanelBunchPanel(PanelIdEnum.InviteFriendRecord, PanelIdEnum.WelfareActiveTab);
            AddPanel(PanelIdEnum.LuckyTurntable, "Container_LuckyTurntablePanel", MogoUILayer.LayerUIPopPanel, "ModuleWelfareActive.LuckyTurntablePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.WelfareActiveTab, "Container_WelfareActiveTab", MogoUILayer.LayerSpecialUIPanel, "ModuleWelfareActive.WelfareActiveTab", BlurUnderlay.Empty, HideMainUI.Hide);
            AddPanel(PanelIdEnum.WelfareActive, "Container_WelfareActivePanel", MogoUILayer.LayerUIPanel, "ModuleWelfareActive.WelfareActivePanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.OpenServerActivity, "Container_OpenServerActivityPanel", MogoUILayer.LayerUIPanel, "ModuleWelfareActive.OpenServerActivityPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.ChargeActivity, "Container_ChargeActivityPanel", MogoUILayer.LayerUIPanel, "ModuleWelfareActive.ChargeActivityPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.InviteFriendRecord, "Container_InviteFriendRecordPanel", MogoUILayer.LayerUIPanel, "ModuleWelfareActive.InviteFriendRecordPanel", BlurUnderlay.Empty, HideMainUI.Hide);
            AddPanel(PanelIdEnum.Festival, "Container_TabPanel", MogoUILayer.LayerSpecialUIPanel, "ModuleWelfareActive.FestivalPanel", BlurUnderlay.Empty, HideMainUI.Hide);
            RegisterPanelModule(PanelIdEnum.Festival, PanelIdEnum.Festival);
            RegisterPanelModule(PanelIdEnum.OpenServerActivity, PanelIdEnum.Festival);

        }

        protected override void OnBeforeShowPanel(PanelIdEnum panelId)
        {
            if (panelId == PanelIdEnum.OpenServerActivity)
            {
                event_cnds_helper.Init();
            }
        }
    }
}
