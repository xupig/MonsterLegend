﻿using System;
using System.Collections.Generic;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;

namespace ModuleWelfareActive
{
    /// <summary>
    /// 手机绑定视图类
    /// </summary>
    public class TabBindMobileView : KContainer
    {
        private StateText _labzidongfasong;         //完成奖励后自动发送 标签
        private StateText _labjianglifenghou;       //每30天验证还可以领取丰厚奖励奥 标签
        private List<BindMobileItem> _itemList;     //奖励物品

        private KButton _btnSubmit;                 //提交按钮
        private KButton _btnGetCheckCode;           //获取验证码按钮  
        private StateText _labBtnSumbit;            //提交按钮文本标签
        private StateText _labBtnGetCheckCode;    
        private StateText _labMobileNum;            //手机号码输入提示标签
        private StateText _labKeYanZheng;           //xx天后可验证
        private StateText _labYiBangDing;           //已绑定标签
        private KInputField _inputMobileNum;        //手机号码输入框
        private StateText _labCheckCode;            //验证码输入提示标签
        private KInputField _inputCheckCode;        //验证码输入框
        
        private uint timerId = 0;
        private int beginTime;
        private int needCdTime;
        private bool isGetVerCode;

        private string txt2 =MogoLanguageUtil.GetContent(102904);    //"点击输入验证码";
        private string txt3 =MogoLanguageUtil.GetContent(102901);    //"点击输入手机号";
        private string txt4 =MogoLanguageUtil.GetContent(103035);    //"请用数字输入验证码" 
        private string txt5 =MogoLanguageUtil.GetContent(103036);    //"请用数字输入11位手机号码"
        private string txt6 =MogoLanguageUtil.GetContent(102911);    //已绑定
        private string txt8 =MogoLanguageUtil.GetContent(102910);    //(点击可以修改手机号)
        private string txt9 =MogoLanguageUtil.GetContent(102912);    //已提交

        private const int CD_TIME = 30 * 1000;
        private string txt1 =MogoLanguageUtil.GetContent(103032);   //"获取验证码";
        private string txt7 =MogoLanguageUtil.GetContent(103033);   //"{0}天后可验证";
        private string txt10 =MogoLanguageUtil.GetContent(103034);  //"提  交";
        
        protected override void Awake()
        {
            _labzidongfasong = GetChildComponent<StateText>("Container_left/Label_txtzidongfasong");
            _labjianglifenghou = GetChildComponent<StateText>("Container_left/Label_txtjianglifenghou");
            _itemList = new List<BindMobileItem>();
            _itemList.Add(GetChild("Container_left/Container_item01").AddComponent<BindMobileItem>());
            _itemList.Add(GetChild("Container_left/Container_item02").AddComponent<BindMobileItem>());
            _itemList.Add(GetChild("Container_left/Container_item03").AddComponent<BindMobileItem>());

            //右边控件
            _btnSubmit = GetChildComponent<KButton>("Container_right/Button_tijiao");
            _labBtnSumbit = _btnSubmit.GetChildComponent<StateText>("label");
            _btnGetCheckCode = GetChildComponent<KButton>("Container_right/Container_yanzhengma/Button_Getyanzhengma");
            _labBtnGetCheckCode = _btnGetCheckCode.GetChildComponent<StateText>("label");

            _labMobileNum = GetChildComponent<StateText>("Container_right/Container_shoujihao/Label_txtDianjishurushoujihao");
            _inputMobileNum = GetChildComponent<KInputField>("Container_right/Container_shoujihao/Input_Haoma");
            _labCheckCode = GetChildComponent<StateText>("Container_right/Container_yanzhengma/Label_txtDianjishuruyanzhengma");
            _inputCheckCode = GetChildComponent<KInputField>("Container_right/Container_yanzhengma/Input_Yanzhengma");
            _labKeYanZheng = GetChildComponent<StateText>("Container_right/Container_yanzhengma/Label_txtKeYanZheng");
            _labYiBangDing = GetChildComponent<StateText>("Container_right/Container_shoujihao/Label_txtYiBangDing");

            _inputCheckCode.text = "";
            _inputMobileNum.text = "";
            _labKeYanZheng.ChangeAllStateText("");
            _labYiBangDing.ChangeAllStateText("");
            _labMobileNum.ChangeAllStateTextAlignment(UnityEngine.TextAnchor.MiddleLeft);
            _labCheckCode.ChangeAllStateTextAlignment(UnityEngine.TextAnchor.MiddleLeft);
            _labBtnSumbit.ChangeAllStateTextAlignment(UnityEngine.TextAnchor.MiddleCenter);
            _labYiBangDing.ChangeAllStateTextAlignment(UnityEngine.TextAnchor.MiddleRight);
            _labKeYanZheng.ChangeAllStateTextAlignment(UnityEngine.TextAnchor.MiddleRight);

            _labMobileNum.CurrentText.text = txt3;
            _labCheckCode.CurrentText.text = txt2;
            _labBtnGetCheckCode.ChangeAllStateTextAlignment(UnityEngine.TextAnchor.MiddleCenter);
            _labBtnGetCheckCode.ChangeAllStateText(txt1);
            AddEventListener();
            //WelfareActiveManager.Instance.GetBindMobileInfoReq();  //请求获取验证码信息
        }

        public void Show()
        {
            Visible = true;
            OnGetBindMobileInfoResult(WelfareActiveManager.Instance.pbBindMobileInfo);
        }

        public void Close()
        {
            DelCDTimer();
            Visible = false;
        }

        private void AddEventListener()
        {
            _btnSubmit.onClick.AddListener(OnSubmit);
            _btnGetCheckCode.onClick.AddListener(OnGetCheckCode);
            _inputCheckCode.onValueChange.AddListener(OnCheckCodeValueChange);
            _inputMobileNum.onValueChange.AddListener(OnMobileNumValueChange);
            EventDispatcher.AddEventListener<PbVerificationCode>(WelfareActiveEvent.C_GET_CHECK_CODE,OnVerificationCodeResult);        //获取验证码成功返回
            EventDispatcher.AddEventListener<PbBindMobileInfo>(WelfareActiveEvent.C_GET_BIND_MOBILE_INFO, OnGetBindMobileInfoResult);  //获取绑定手机信息成功返回
            EventDispatcher.AddEventListener(WelfareActiveEvent.C_BIND_MOBILE_RESULT, OnBindMobileResult);                             //绑定手机成功返回
            //EventDispatcher.AddEventListener(WelfareActiveEvent.C_BIND_REWARD_RESULT, OnGetBindRewardResult);                          //获取绑定奖励信息
        }

        private void RemoveEventListener()
        {
            _btnSubmit.onClick.RemoveListener(OnSubmit);
            _btnGetCheckCode.onClick.RemoveListener(OnGetCheckCode);
            _inputCheckCode.onValueChange.RemoveListener(OnCheckCodeValueChange);
            _inputMobileNum.onValueChange.RemoveListener(OnMobileNumValueChange);
            EventDispatcher.RemoveEventListener<PbVerificationCode>(WelfareActiveEvent.C_GET_CHECK_CODE, OnVerificationCodeResult);
            EventDispatcher.RemoveEventListener<PbBindMobileInfo>(WelfareActiveEvent.C_GET_BIND_MOBILE_INFO, OnGetBindMobileInfoResult);
            EventDispatcher.RemoveEventListener(WelfareActiveEvent.C_BIND_MOBILE_RESULT, OnBindMobileResult); 
        }

        /// <summary>
        /// 获取验证码--请求
        /// </summary>
        private void OnGetCheckCode()
        {
            LoggerHelper.Debug("isGetVerCode:" + isGetVerCode);
            if (isGetVerCode)
            {
                ulong mobileId = CheckNumber(_inputMobileNum.text, txt5);
                if (mobileId <= 0) return;
                if (mobileId.ToString().Length != 11)
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, txt5, PanelIdEnum.MainUIField);
                    return;
                }
                AddCdTimer(CD_TIME);
                WelfareActiveManager.Instance.GetCheckCode(mobileId);
            }
        }

        /// <summary>
        /// 绑定手机--请求
        /// </summary>
        private void OnSubmit()
        {
            if (!_labBtnSumbit.CurrentText.text.Equals(txt9))
            {//出于已提交状态，不提交
                int checkCode = (int)CheckNumber(_inputCheckCode.text, txt4);
                if (checkCode > 0) WelfareActiveManager.Instance.BindMobileReq(checkCode);
            }
        }

        //增加CD冷却时间倒计时
        private void AddCdTimer(int needCdTime)
        {
            if (needCdTime > 0)
            {
                isGetVerCode = false;
                this.needCdTime = needCdTime;
                this.beginTime = Environment.TickCount;
                if (timerId <= 0) timerId = TimerHeap.AddTimer(0, 1000, onHanlder);
            }
            else
            {
                _labBtnGetCheckCode.ChangeAllStateText(txt1);
                //LoggerHelper.Error("[AddCdTimer] needCdTime:" + needCdTime);
            }
        }

        private void DelCDTimer()
        {
            if (timerId != 0)
            {
                TimerHeap.DelTimer(timerId);
                timerId = 0;
            }
        }

        private void onHanlder()
        {
            int yuTime = needCdTime - (Environment.TickCount - beginTime);
            if (yuTime <= 0)
            {//CD时间已结束
                isGetVerCode = true;
                TimerHeap.DelTimer(timerId);
                _labBtnGetCheckCode.ChangeAllStateText(txt1);
                timerId = 0;
                if (WelfareActiveManager.Instance.pbBindMobileInfo != null) WelfareActiveManager.Instance.pbBindMobileInfo.last_generate_time = CD_TIME / 1000;
            }
            else 
            {
                string text = (yuTime % 1000 == 0 ? yuTime / 1000 : yuTime / 1000 + 1).ToString();
                _labBtnGetCheckCode.ChangeAllStateText(string.Concat(text, MogoLanguageUtil.GetContent(6147016)));
                if (WelfareActiveManager.Instance.pbBindMobileInfo != null) WelfareActiveManager.Instance.pbBindMobileInfo.last_generate_time = (ulong)((CD_TIME - yuTime) / 1000);
            }
        }

        //对手机号进行校验
        private ulong CheckNumber(string value, string desc)
        {
            if (string.IsNullOrEmpty(value))
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, desc, PanelIdEnum.MainUIField);
                return 0;
            }

            try
            {
                return ulong.Parse(value);
            }
            catch (Exception ex)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, desc, PanelIdEnum.MainUIField);
                return 0;
            }
        }


        //=====================================================//
        private void OnCheckCodeValueChange(string value)
        {
            //LoggerHelper.Error("[OnCheckCodeValueChange] value:" + value);
            if (string.IsNullOrEmpty(value) || value.Trim().Length < 1)
            {
                _labCheckCode.Visible = true;
            }
            else _labCheckCode.Visible = false;
        }

        private void OnMobileNumValueChange(string value)
        {
            //LoggerHelper.Error("[OnMobileNumValueChange] value:" + value);
            if (string.IsNullOrEmpty(value) || value.Trim().Length < 1)
            {
                PbBindMobileInfo data = WelfareActiveManager.Instance.pbBindMobileInfo;
                if (data != null && data.mobile_id > 0)
                {
                    _labMobileNum.Visible = false;
                    _inputMobileNum.text = data.mobile_id.ToString();
                }
                else
                {
                    _labMobileNum.Visible = true;
                }
            }
            else _labMobileNum.Visible = false;
        }

        //获取验证码成功返回
        private void OnVerificationCodeResult(PbVerificationCode data)
        {
            //LoggerHelper.Error(string.Format("获取验证码成功ver_code:{0}", data.ver_code));
            //_labCheckCode.ChangeAllStateText("");
            needCdTime = 0;
            onHanlder();
            OnCheckCodeValueChange(data.ver_code.ToString());
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(6147017), PanelIdEnum.MainUIField);
        }

        //获取手机绑定信息返回
        private void OnGetBindMobileInfoResult(PbBindMobileInfo data)
        {
            LoggerHelper.Debug("获取手机绑定信息 data:" + data);
            if (data != null)
            {
                LoggerHelper.Debug("mobileId:" + data.mobile_id);
                LoggerHelper.Debug("last_time:" + data.last_time);
                LoggerHelper.Debug("reward_id:" + data.reward_id);
                LoggerHelper.Debug("reward_flag:" + data.reward_flag);
                LoggerHelper.Debug("last_generate_time:" + data.last_generate_time);
                LoggerHelper.Debug("next_get_time:" + data.next_get_time);
                LoggerHelper.Debug("ver_code:" + data.ver_code);

                _inputMobileNum.text = data.mobile_id > 0 ? data.mobile_id.ToString() : "";
                OnMobileNumValueChange(_inputMobileNum.text);
                ulong m = data.last_generate_time * 1000;
                RefreshReward(data.reward_id);

                if (data.last_time > 0)
                {//已绑定
                    //_labMobileNum.CurrentText.text = txt6; //已绑定
                    _labYiBangDing.CurrentText.text = txt6;
                    _inputCheckCode.text = "";

                    if (data.next_get_time > 0)
                    {//离下次验证还需next_get_time天
                        DelCDTimer();
                        //_labCheckCode.CurrentText.text = string.Format(txt7, data.next_get_time);
                        _labKeYanZheng.CurrentText.text = string.Format(txt7, data.next_get_time);
                        _labBtnSumbit.ChangeAllStateText(txt9);
                        _labBtnGetCheckCode.ChangeAllStateText(txt1);
                        _btnSubmit.SetButtonDisable();
                        _btnGetCheckCode.SetButtonDisable();
                        OnCheckCodeValueChange("1");
                        isGetVerCode = false;
                    }
                    else
                    {//可以验证
                        _btnSubmit.SetButtonActive();
                        _btnGetCheckCode.SetButtonActive();
                        _labBtnSumbit.ChangeAllStateText(txt10);
                        _labCheckCode.ChangeAllStateText(data.ver_code > 0 ? "" : txt2);
                        _labKeYanZheng.CurrentText.text = "";
                        OnCheckCodeValueChange("");
                        isGetVerCode = true;
                    }
                }
                else
                {//未绑定
                    _btnSubmit.SetButtonActive();
                    _btnGetCheckCode.SetButtonActive();  
                    _labBtnSumbit.ChangeAllStateText(txt10);
                    //_labCheckCode.ChangeAllStateText(data.ver_code > 0 ? "" : txt2);
                    //_labMobileNum.CurrentText.text = data.mobile_id > 0 ? txt8 : txt3;
                    _labYiBangDing.CurrentText.text = "";
                    _labKeYanZheng.CurrentText.text = "";
                    _inputCheckCode.text = "";
                    OnCheckCodeValueChange("");

                    if (m >= CD_TIME)
                    {//获取验证码：剩余CD操作时间(ms)
                        _labBtnGetCheckCode.CurrentText.text = txt1;
                        isGetVerCode = true;
                    }
                    else
                    {
                        AddCdTimer((int)(CD_TIME - m));
                    }
                }
                OnMobileNumValueChange(_inputCheckCode.text);
            }
        }

        private void RefreshReward(uint rewardId)
        {
            List<RewardData> list = null;
            if (rewardId > 0) list = item_reward_helper.GetRewardDataList((int)rewardId, PlayerAvatar.Player.vocation);
            PbBindMobileInfo data = WelfareActiveManager.Instance.pbBindMobileInfo;
            //LoggerHelper.Error("==== rewardId:" + rewardId);
            for (int i = 0; i < _itemList.Count; i++)
            {
                if (list != null && list.Count == 1)
                {//如果只有1项奖励，就居中对齐
                    _itemList[1].Data = list[0];
                    _itemList[1].SetImage(data != null && data.last_time > 0 ? true : false);
                }
                else
                {
                    _itemList[i].Data = list != null && i < list.Count ? list[i] : null;
                    _itemList[i].SetImage(data != null && data.last_time > 0 ? true : false);
                }
            }
        }

        //绑定手机成功返回
        private void OnBindMobileResult()
        {
            LoggerHelper.Debug("绑定手机成功返回");
            //_labMobileNum.CurrentText.text = txt6;
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(6147018), PanelIdEnum.MainUIField);
        }

        //获取绑定奖励信息返回
        private void OnGetBindRewardResult()
        {
            LoggerHelper.Debug("获取绑定奖励信息");
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            DelCDTimer();
            base.OnDestroy();
        }
    }
}
