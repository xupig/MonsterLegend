﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.UIComponent;
using GameLoader.Utils;

namespace ModuleWelfareActive
{
    /// <summary>
    /// 分享邀请码--视图类
    /// </summary>
    public class ShareInviteCodeView : KList.KListItemBase
    {
        private string _data;
        private KButton _btnClose;             //关闭
        private KButton _btnWeiXinShare;       //微信分享
        private KButton _btnFriendShare;       //朋友网分享
        private KButton _btnQQ;                //QQ 
        private KButton _btnWeiBoShare;        //新浪微博分享
        private KButton _btnFacebookShare;     //Facebook分享
        
        private const string txt1 = "已领取{0}/{1}次";
        private const string txt2 = "召集{0}名好友,成功进入游戏";
        private const string txt3 = "领取";

        protected override void Awake()
        {
            _btnClose = GetChildComponent<KButton>("Button_close");
            _btnWeiXinShare = GetChildComponent<KButton>("Button_weixin");
            _btnFriendShare = GetChildComponent<KButton>("Button_pengyouquan");
            _btnQQ = GetChildComponent<KButton>("Button_QQ");
            _btnWeiBoShare = GetChildComponent<KButton>("Button_weibo");
            _btnFacebookShare = GetChildComponent<KButton>("Button_facebook");
            AddEventListener();
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as string;
                Refresh();
            }
        }

        public override void Dispose()
        {
            RemoveEventListener();
            _data = null;
        }

        public void Close()
        {
            Visible = false;
        }

        protected void Refresh()
        {
            if (!string.IsNullOrEmpty(_data))
            {
                Visible = true;
            }
            else
            {
                Visible = false;
            }
        }

        private void AddEventListener()
        {
            _btnClose.onClick.AddListener(OnClose);
            _btnWeiXinShare.onClick.AddListener(OnWeiXinShare);
            _btnWeiBoShare.onClick.AddListener(OnWeiBoShare);
            _btnQQ.onClick.AddListener(OnQQShare);
            _btnFacebookShare.onClick.AddListener(OnFackbook);
            _btnFriendShare.onClick.AddListener(OnFriendShare);
        }

        private void RemoveEventListener()
        {
            _btnWeiXinShare.onClick.RemoveListener(OnWeiXinShare);
            _btnWeiBoShare.onClick.RemoveListener(OnWeiBoShare);
            _btnFacebookShare.onClick.RemoveListener(OnFackbook);
            _btnFriendShare.onClick.RemoveListener(OnFriendShare);
        }

        private void OnClose()
        {
            Visible = false;
        }

        //微信分享
        private void OnWeiXinShare()
        {
            Close();
            ShareMgr.Instance.Share();
        }

        //新浪微博分享
        private void OnWeiBoShare()
        {

        }

        //QQ分享
        private void OnQQShare()
        { 
        }

        //Fackbook分享
        private void OnFackbook()
        {

        }

        //朋友网分享
        private void OnFriendShare()
        {

        }
    }
}
