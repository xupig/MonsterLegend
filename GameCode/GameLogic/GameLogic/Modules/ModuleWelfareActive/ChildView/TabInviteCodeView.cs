﻿using System.Collections;
using System.Collections.Generic;
using Common;
using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Config;
using GameLoader.PlatformSdk;
using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine;
using MogoEngine.Events;
using UnityEngine;
using LitJson;
using GameLoader.Utils.Timer;

namespace ModuleWelfareActive
{
    /// <summary>
    /// 邀请码Tab界面
    /// </summary>
    public class TabInviteCodeView: KContainer
    {
        private KButton _btnShare;
        private KButton _btnInviteFriendRecord;
        private StateText _txtShareBtn;

        private StateText _txtXinxi;
        private StateText _txtTishi;
        private StateText _txtCode;
        private StateText _txtShareLink;
        private StateText _txtXiaZai;
        private ImageWrapper _ewmIcon;
        private List<InviteCodeItem> _itemList;
  
        private string txt1 =MogoLanguageUtil.GetContent(103029);  // "生成邀请码";
        //private string txt2 =MogoLanguageUtil.GetContent(103030);  // "生  成";
        //private string txt3 =MogoLanguageUtil.GetContent(103031);  // "分  享";
        private StateImage _logoImage;
        private StateImage _logoImageyyb;

        protected override void Awake()
        {
            _btnShare = GetChildComponent<KButton>("Container_fengxiang/Button_fenxiang");                   //分享或生成二维码按钮
            _btnInviteFriendRecord = GetChildComponent<KButton>("Container_fengxiang/Button_yaoqingjilu");   //邀请记录
            StateText txtbtn = _btnInviteFriendRecord.GetChildComponent<StateText>("label");
            txtbtn.ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);
            _txtShareBtn = _btnShare.GetChildComponent<StateText>("label");                                  //分享按钮标签
            _txtXinxi = GetChildComponent<StateText>("Label_txtXinxi");
            _txtTishi = GetChildComponent<StateText>("Label_txtTishi");
            _txtCode = GetChildComponent<StateText>("Label_txtYaoqingma");                              //邀请码  
            _txtShareLink = GetChildComponent<StateText>("Container_fengxiang/Label_txtfengxiang");     //分享短链接(SDK返回)
            _txtXiaZai = GetChildComponent<StateText>("Container_fengxiang/Label_txtXiazai");           //下载链接标签
            //_btnInviteFriendRecord.Visible = false;
            _logoImage = GetChildComponent<StateImage>("Image_logo");
            _logoImageyyb = GetChildComponent<StateImage>("Image_logoyyb");
            if (SystemConfig.isyyb())
            {
                _logoImage.Visible = false;
                _logoImageyyb.Visible = true;
            }
            else
            {
                _logoImage.Visible = true;
                _logoImageyyb.Visible = false;
            }
            _ewmIcon = AddChildComponent<ImageWrapper>("Container_ewm");
            _txtXinxi.RecalculateAllStateHeight();
            //_txtXinxi.CurrentText.preferredHeight += 10;
            //_txtShareBtn.ChangeAllStateText(txt3);
            _txtCode.ChangeAllStateText("");
            _txtXiaZai.ChangeAllStateText("");
            _txtShareLink.ChangeAllStateText("");

            _itemList = new List<InviteCodeItem>();
            _itemList.Add(GetChild("ScrollView_jinagli/mask/content/Container_item0").AddComponent<InviteCodeItem>());
            _itemList.Add(GetChild("ScrollView_jinagli/mask/content/Container_item1").AddComponent<InviteCodeItem>());
            _itemList.Add(GetChild("ScrollView_jinagli/mask/content/Container_item2").AddComponent<InviteCodeItem>());
            _itemList.Add(GetChild("ScrollView_jinagli/mask/content/Container_item3").AddComponent<InviteCodeItem>());
            WelfareActiveManager.Instance.CreateInviteCodeReq();
            InitInviteCodeItem();
            OnRefreshRewardList();
            AddEventListener();
            //GetInviteCodeReq();
            GetShareLink();
        }

        public void Show()
        {
            Visible = true;
            OnRefreshInviteCode(WelfareActiveManager.Instance.playerInviteCode);
        }

        public void Close()
        {
            Visible = false;
        }

        /// <summary>
        /// 是否有奖励
        /// </summary>
        /// <returns></returns>
        private bool hasRewardable()
        {
            foreach (InviteCodeItem item in _itemList)
            {
                if (item.hasRewardable()) return true;
            }
            return false;
        }

        //是否已有邀请码
        private bool hasInviteCode 
        { 
            get 
            {
                string code = _txtCode.CurrentText.text;
                return !string.IsNullOrEmpty(code);
                //return string.IsNullOrEmpty(code) || code.Equals(txt1) ? false : true; 
            }
        }

        //获取分享短链接
        private void GetShareLink()
        {
            if(Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer)
            {
                DockingService.GetShareData();
            }
            else
            {
                LoggerHelper.Info("Send GetShareUrl Request");
                PlayerAvatar player = (PlayerAvatar)MogoWorld.Player;
                ServerInfo serverInfo = ServerListManager.GetServerInfoByID(LocalSetting.settingData.SelectedServerID);
                if (Application.platform != RuntimePlatform.IPhonePlayer)
                {
                    PlatformSdkMgr.Instance.shareUrlHandler = OnShareUrlHandler;
                    PlatformSdkMgr.Instance.GetShareUrl(LoginInfo.platformId_uid, player.dbid.ToString(), player.name, player.level.ToString(), serverInfo.id.ToString(), serverInfo.name, "");
                }
                #if UNITY_IPHONE && !UNITY_EDITOR
			                IOSPlugins.RegisterGeShareUrlCallback (OnShareUrlHandler);
			                IOSPlugins.getShareUrl (player.dbid.ToString (), player.name, serverInfo.name, player.level.ToString (),"");
			                LoggerHelper.Info (string.Format("[IOSPlugins] Get EWM Request roleId:{0},roleName:{1},serverName:{2},roleLevel:{3}",player.dbid.ToString(),player.name,serverInfo.name,player.level.ToString()));
                #endif
            }
        }

        //分享链接返回
        private void OnShareUrlHandler(string url, string ewmPath)
        {
            //ewmPath = "d:/ewm.png";
            _txtShareLink.CurrentText.text = string.IsNullOrEmpty(url) ? "" : url;
            if (Application.platform != RuntimePlatform.IPhonePlayer)
            {
                StartCoroutine(LoadEwmPicture(ewmPath));
            }
            else
            {
                TimerHeap.AddTimer(100, 0, () => {
                    byte[] assetBytes = ewmPath.LoadByteFile();
                    LoggerHelper.Info("IOS ewmPath:" + ewmPath + ",assetBytes:" + assetBytes);
                    Texture2D texture2D = new Texture2D(248, 248);
                    texture2D.LoadImage(assetBytes);
                    _ewmIcon.sprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), new Vector2(0, 0));
                });
            }
        }

        private IEnumerator LoadEwmPicture(string ewmPath)
        {
            Texture2D textrue2D = null;
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WindowsWebPlayer || Application.platform == RuntimePlatform.WindowsPlayer)
            {
                if (Application.platform == RuntimePlatform.Android)
                {
                    ewmPath = string.Concat(ConstString.ASSET_FILE_HEAD, ewmPath);
                }
                
                WWW www = new WWW(ewmPath);
                yield return www;
                if (www.isDone)
                {
                    textrue2D = www.texture;
                    //LoggerHelper.Info(string.Format("3==== ewmPath:{0},texture2D:{1} Download [OK]",ewmPath,textrue2D));
                    _ewmIcon.sprite = Sprite.Create(textrue2D, new Rect(0, 0, textrue2D.width, textrue2D.height), new Vector2(0, 0));
                }
                www.Dispose();
            }
            else 
            {
                byte[] assetBytes = ewmPath.LoadByteFile();
                if (assetBytes != null)
                {
                    AssetBundleCreateRequest abCreateRequest = AssetBundle.LoadFromMemoryAsync(assetBytes);
                    yield return abCreateRequest;
                    if (abCreateRequest.isDone == true)
                    {
                        textrue2D = abCreateRequest.assetBundle.mainAsset as Texture2D;
                        _ewmIcon.sprite = Sprite.Create(textrue2D, new Rect(0, 0, textrue2D.width, textrue2D.height), new Vector2(0, 0));
                    }
                }
            }
        }

        //显示格子，关联(invite.cs)邀请信息
        private void InitInviteCodeItem()
        {
            //排序
            List<invite> dataList = new List<invite>();
            foreach (invite data in XMLManager.invite.Values)
            {
                dataList.Add(data);
            }
            dataList.Sort(Sort);

            //A-2、显示
            int index = 0;
            foreach (invite data in dataList)
            {
                _itemList[index].baseData = data;
                index++;
            }

            //把多余的显示项隐藏
            LoggerHelper.Debug("邀请码奖励总项:" + index);
            for (int i = index; i < _itemList.Count; i++)
            {
                _itemList[i].Visible = false;
            }
        }

        private int Sort(invite a, invite b)
        {
            //type按降序排
            if (a.__type > b.__type) return -1;
            if (a.__type < b.__type) return 1;
            
            //type相等的情况下，按id升序排
            if (a.__id < b.__id) return -1;
            if (a.__id > b.__id) return 1;
            return 0;
        }

        //监听事件
        private void AddEventListener()
        {
            //WelfareActiveManager.cs 派发出事件
            _btnShare.onClick.AddListener(OnShare);
            _btnInviteFriendRecord.onClick.AddListener(OnOpenInviteFriendRecordPanel);
            EventDispatcher.AddEventListener(WelfareActiveEvent.C_REFRESH_REWARD_LIST, OnRefreshRewardList);
            EventDispatcher.AddEventListener<string>(WelfareActiveEvent.C_REFRESH_INVITE_CODE, OnRefreshInviteCode);
            EventDispatcher.AddEventListener<PbCompleteId>(WelfareActiveEvent.C_REFRESH_RECEVIE, OnRefreshRecevieTimes);
            EventDispatcher.AddEventListener<string>(WelfareActiveEvent.DOCKING_SHARE, OnDocking); 
        }

        //移除事件
        private void RemoveEventListener()
        {
            _btnShare.onClick.RemoveListener(OnShare);
            _btnInviteFriendRecord.onClick.RemoveListener(OnOpenInviteFriendRecordPanel);
            EventDispatcher.RemoveEventListener(WelfareActiveEvent.C_REFRESH_REWARD_LIST, OnRefreshRewardList);
            EventDispatcher.RemoveEventListener<string>(WelfareActiveEvent.C_REFRESH_INVITE_CODE, OnRefreshInviteCode);
            EventDispatcher.RemoveEventListener<PbCompleteId>(WelfareActiveEvent.C_REFRESH_RECEVIE, OnRefreshRecevieTimes);
            EventDispatcher.RemoveEventListener<string>(WelfareActiveEvent.DOCKING_SHARE, OnDocking); 
        }

        //刷新邀请码
        private void OnRefreshInviteCode(string inviteCode)
        {
            if (_txtCode != null) _txtCode.CurrentText.text = !string.IsNullOrEmpty(inviteCode) ? inviteCode : "";
        }

        //刷新奖励信息列表
        private void OnRefreshRewardList()
        { 
            bool isFind = false;
            PbCompleteId data = null;
            InviteCodeItem item = null;
            PbCompleteList rewardList = WelfareActiveManager.Instance.rewardList;
            int len = _itemList.Count;
            int count = rewardList != null ? rewardList.id.Count : 0;

            for (int i = 0; i < len; i++)
            {
                isFind = false;
                item = _itemList[i];
                for (int j = 0; j < count; j++)
                {
                    data = rewardList.id[j];
                    if (item.baseData != null && item.baseData.__id == data.id)
                    {
                        item.Data = data;
                        isFind = true;
                        LoggerHelper.Info(string.Format("====刷新奖励列表 id:{0},reward_num:{1},can_get:{2}", data.id, data.reward_num, data.can_get));
                        break;
                    }
                }
                if (!isFind) item.Data = null;  //没找到，显示默认信息
                if (data != null) LoggerHelper.Debug("id:" + data.id + ",reward_num:" + data.reward_num + ",isFind:" + isFind + ",can_get:" + data.can_get);
            }
            rewardList = null;
            EventDispatcher.TriggerEvent<bool>(WelfareActiveEvent.C_REFRESH0_REWARD_TIP,hasRewardable());  //刷新领取状态图标
        }

        //领取成功--刷新领取次数
        private void OnRefreshRecevieTimes(PbCompleteId data)
        {
            if (data == null) return;
            LoggerHelper.Info(string.Format("====领奖成功 id:{0},reward_num:{1},can_get:{2}", data.id, data.reward_num, data.can_get));
            PbCompleteList rewardList = WelfareActiveManager.Instance.rewardList;
            int i = 0;
            foreach(InviteCodeItem item in _itemList)
            {
                if (rewardList != null && i < rewardList.id.Count && rewardList.id[i].id == data.id)
                {//更新列表中数值
                    rewardList.id[i].can_get = data.can_get;
                    rewardList.id[i].reward_num = data.reward_num;
                }
                i++;

                if (item.baseData != null && item.baseData.__id == data.id)
                {
                    item.Data = data;
                    break;
                }
            }
            EventDispatcher.TriggerEvent<bool>(WelfareActiveEvent.C_REFRESH0_REWARD_TIP, hasRewardable());  //刷新领取状态图标
        }

        private void OnDocking(string str)
        {
            JsonData jd = JsonMapper.ToObject(str);
            JsonData data = jd["data"];
            string url = data["url"].ToString();
            string qrdata = data["qrcode"].ToString();
            LoggerHelper.Info("url:" + url + "qrcode:" + qrdata);
            /*DockingShareResData dockingShareResData = JsonMapper.ToObject<DockingShareResData>(str);
            string json = dockingShareResData.data;
            
            DockingShareData dockingShareData = JsonMapper.ToObject<DockingShareData>(json);*/
            OnShareUrlHandler(url, qrdata);
        }

        //分享或生产邀请码
        private void OnShare()
        {
            //MessageBox.Show(false, MessageBox.DEFAULT_TITLE, "暂未开放！");
            bool result = hasInviteCode;
            string link = _txtShareLink.CurrentText.text;
            LoggerHelper.Debug(string.Format("邀请码:{0},分享链接:{1},result:{2}", _txtCode.CurrentText.text, link, result));
            //if (!result) WelfareActiveManager.Instance.CreateInviteCodeReq();                                                                //生成邀请码请求
            if (!string.IsNullOrEmpty(link)) ShareMgr.Instance.ShareLink(_txtShareLink.CurrentText.text);
        }

        //打开邀请好友记录
        private bool isSendGetFriendList = false;
        private void OnOpenInviteFriendRecordPanel()
        {
            if (!isSendGetFriendList)
            {
                FriendManager.Instance.SendGetFriendListMsg();
                isSendGetFriendList = true;
            }
            UIManager.Instance.ShowPanel(PanelIdEnum.InviteFriendRecord);
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }
    }
}

