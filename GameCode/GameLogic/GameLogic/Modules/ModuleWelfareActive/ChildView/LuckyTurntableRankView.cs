﻿using System;
using System.Collections.Generic;
using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using MogoEngine.Events;
using GameMain.GlobalManager;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager.SubSystem;
using Common.ExtendComponent;

namespace ModuleWelfareActive
{
    public class LuckyTurntableRankView : KContainer
    {
        private KScrollView _luckyRankScrollView;
        private KList _luckyRankList;
        private ListLayoutComponent _layoutComponent;
        private int _curTurntableType = 0;
        private uint _requestRankTimerId = 0;

        protected override void Awake()
        {
            _luckyRankScrollView = GetChildComponent<KScrollView>("ScrollView_shouqibang");
            _luckyRankList = _luckyRankScrollView.GetChildComponent<KList>("mask/content");
            _luckyRankList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _luckyRankList.SetPadding(5, 1, 5, 1);
            _luckyRankList.SetGap(1, 0);
            _layoutComponent = _luckyRankList.gameObject.AddComponent<ListLayoutComponent>();
            _layoutComponent.InitListLayoutInfo(KList.KListDirection.TopToDown);
        }

        protected override void OnEnable()
        {
            AddEventListener();
            AddRequestRankTimer();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            RemoveTimer();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<int, List<LuckyRankItemData>>(LuckyTurntableEvents.GET_LUCKY_RANK_INFO, OnRefreshRankList);
            EventDispatcher.AddEventListener(LuckyTurntableEvents.STOP_REQUEST_LUCKY_RANK_TIMER, OnStopRequestRankTimer);
            EventDispatcher.AddEventListener(LuckyTurntableEvents.START_REQUEST_LUCKY_RANK_TIMER, OnStartRequestRankTimer);
            EventDispatcher.AddEventListener<int, List<LuckyRankItemData>>(LuckyTurntableEvents.ON_SHOW_LUCKY_RANK_SPOT_LIGHT, OnShowLuckyRankSpotLight);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<int, List<LuckyRankItemData>>(LuckyTurntableEvents.GET_LUCKY_RANK_INFO, OnRefreshRankList);
            EventDispatcher.RemoveEventListener(LuckyTurntableEvents.STOP_REQUEST_LUCKY_RANK_TIMER, OnStopRequestRankTimer);
            EventDispatcher.RemoveEventListener(LuckyTurntableEvents.START_REQUEST_LUCKY_RANK_TIMER, OnStartRequestRankTimer);
            EventDispatcher.RemoveEventListener<int, List<LuckyRankItemData>>(LuckyTurntableEvents.ON_SHOW_LUCKY_RANK_SPOT_LIGHT, OnShowLuckyRankSpotLight);
        }

        private void OnRefreshRankList(int turntableType, List<LuckyRankItemData> rankItemInfoList)
        {
            if (_curTurntableType == turntableType)
            {
                ShowContent(rankItemInfoList);
            }
        }

        private void OnStopRequestRankTimer()
        {
            RemoveTimer();
        }

        private void OnStartRequestRankTimer()
        {
            AddRequestRankTimer();
        }

        private void OnShowLuckyRankSpotLight(int turntableType, List<LuckyRankItemData> spotLightRankInfoList)
        {
            //跑马灯
            for (int i = 0; i < spotLightRankInfoList.Count; i++)
            {
                LuckyRankItemData itemData = spotLightRankInfoList[i];
                SystemInfoManager.Instance.Show(itemData.descId, itemData.playerName, itemData.itemId, itemData.itemNum);
            }
        }

        public void Refresh(int turntableType)
        {
            _curTurntableType = turntableType;
            LuckyTurntableData turntableData = PlayerDataManager.Instance.LuckyTurntableData;
            bool isRequestTag = turntableData.GetRequestRankTag((LuckyTurntableType)turntableType);
            if (!isRequestTag)
            {
                OnRequestRank();
                turntableData.SetHasRequestRankTag((LuckyTurntableType)turntableType);
            }
            else
            {
                ShowContent(turntableData.GetLuckyRankInfoList((LuckyTurntableType)turntableType));
            }
        }

        private void ShowContent(List<LuckyRankItemData> rankItemInfoList)
        {
            _luckyRankList.SetDataList<LuckyRankItem>(rankItemInfoList);
            _layoutComponent.RecalculateItemPosition();
            _luckyRankList.RecalculateSize();
            _luckyRankScrollView.ResetContentPosition();
        }

        private void AddRequestRankTimer()
        {
            if (_requestRankTimerId == 0)
            {
                _requestRankTimerId = TimerHeap.AddTimer(5 * 1000, 5 * 1000, OnRequestRank);
            }
        }

        private void OnRequestRank()
        {
            LuckyTurntableManager.Instance.RequestLuckyRankList(_curTurntableType);
        }

        private void RemoveTimer()
        {
            if (_requestRankTimerId != 0)
            {
                TimerHeap.DelTimer(_requestRankTimerId);
                _requestRankTimerId = 0;
            }
        }
    }
}
