﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using UnityEngine.EventSystems;
using System.Net;
using GameLoader.Utils;
using GameLoader.Config;
using GameMain.Entities;
using System.Text;
using ModuleCommonUI;
using Common.Base;
using Common.Utils;
using GameLoader.Utils.Timer;
using UnityEngine;
using System.Text.RegularExpressions;
using GameMain.GlobalManager.SubSystem;

namespace ModuleWelfareActive
{
    public class TabActivateCodeView : KContainer
    {
        private KInputField _input;
        private KDummyButton _hintBtn;
        private KButton _commitCodeBtn;

        private const string ACTIVATE_CODE_URL = "http://activity.qjphs.4399sy.com/cgi-bin/card?server=s{0}.qjphs.4399sy.com&dbid={1}&serial_number={2}";
        private Dictionary<ActivateCodeRespCode, int> _respCodeDescIdDict = new Dictionary<ActivateCodeRespCode, int>();

        private static Regex ELEVEN_DIGIT_PATTERN = new Regex(@"\d{11}");

        protected override void Awake()
        {
            _input = GetChildComponent<KInputField>("Input_Yanzhengma");
            _input.characterLimit = 30;
            _input.text = "";
            _hintBtn = GetChild("Container_inputHint").AddComponent<KDummyButton>();
            _commitCodeBtn = GetChildComponent<KButton>("Button_tijiao");
            SetHintBtnSize();
            InitRespCodeDescIdDict();
            AddEventListener();
        }

        private void SetHintBtnSize()
        {
            RectTransform inputBgRect = GetChildComponent<RectTransform>("ScaleImage_txtBg02");
            RectTransform hintRect = _hintBtn.gameObject.GetComponent<RectTransform>();
            RectTransform hintTextRect = _hintBtn.transform.FindChild("Label_txtDianjishuruyanzhengma").gameObject.GetComponent<RectTransform>();
            float deltaX = (inputBgRect.sizeDelta.x - hintRect.sizeDelta.x) / 2.0f;
            hintRect.sizeDelta = new Vector2(inputBgRect.sizeDelta.x, hintRect.sizeDelta.y);
            hintRect.anchoredPosition = new Vector2(hintRect.anchoredPosition.x - deltaX, hintRect.anchoredPosition.y);
            hintTextRect.anchoredPosition = new Vector2(hintTextRect.anchoredPosition.x + deltaX, hintTextRect.anchoredPosition.y);
        }

        private void InitRespCodeDescIdDict()
        {
            _respCodeDescIdDict.Add(ActivateCodeRespCode.ENUM_CARD_SUCCSEE, 103053);
            _respCodeDescIdDict.Add(ActivateCodeRespCode.ENUM_DB_QUERY_ERROR, 103054);
            _respCodeDescIdDict.Add(ActivateCodeRespCode.ENUM_NUMBER_ERROR, 103055);
            _respCodeDescIdDict.Add(ActivateCodeRespCode.ENUM_NUMBER_ALREADY_USED, 103056);
            _respCodeDescIdDict.Add(ActivateCodeRespCode.ENUM_SERVER_NOT_FOUND, 103057);
            _respCodeDescIdDict.Add(ActivateCodeRespCode.ENUM_ERROR_SAME_TYPE, 103058);
            _respCodeDescIdDict.Add(ActivateCodeRespCode.ENUM_ERROR_PLAT, 103059);
            _respCodeDescIdDict.Add(ActivateCodeRespCode.ENUM_LESS_THAN_STARTTIME, 103060);
            _respCodeDescIdDict.Add(ActivateCodeRespCode.ENUM_MORE_THAN_ENDTIME, 103061);
        }

        private void AddEventListener()
        {
            _hintBtn.onClick.AddListener(OnClickHintBtn);
            _commitCodeBtn.onClick.AddListener(OnClickCommitBtn);
        }

        private void RemoveEventListener()
        {
            _hintBtn.onClick.RemoveListener(OnClickHintBtn);
            _commitCodeBtn.onClick.RemoveListener(OnClickCommitBtn);
        }

        private void OnClickHintBtn()
        {
            HideHintText();
            EventSystem.current.SetSelectedGameObject(_input.gameObject);
            _input.OnPointerClick(new PointerEventData(EventSystem.current));
        }

        private void OnClickCommitBtn()
        {
            if (!string.IsNullOrEmpty(_input.text.Trim()))
            {
                string text = _input.text;
                if (text.Length == 11 && ELEVEN_DIGIT_PATTERN.IsMatch(text))
                {
                    DoFCodeRequest();
                }
                else
                {
                    DoHttpRequest();
                }
            }
        }

        private void DoFCodeRequest()
        {
            ulong fCodeNum = ulong.Parse(_input.text);
            WelfareActiveManager.Instance.FCodeReq(fCodeNum);
        }

        private void DoHttpRequest()
        {
            Action action = () =>
            {
                string url = string.Format(ACTIVATE_CODE_URL, LocalSetting.settingData.SelectedServerID, PlayerAvatar.Player.dbid, _input.text);
                byte[] postData = new byte[4];
                byte[] result = ApplyHttpData(url, postData);
                if (result == null)
                {
                    LoggerHelper.Error("Response result is null !!!");
                    return;
                }
                string str = Encoding.UTF8.GetString(result);
                int respCode = -1;
                int.TryParse(str, out respCode);
                if (respCode == -1)
                {
                    LoggerHelper.Error("Response result string is not correct !!! result = " + str);
                    return;
                }
                TimerHeap.AddTimer(0, 0, ShowFloatTips, respCode);
            };
            action.BeginInvoke(null, null);
        }

        private void ShowFloatTips(int respCode)
        {
            if (respCode == (int)ActivateCodeRespCode.ENUM_CARD_SUCCSEE) 
			{
				string activeCode = _input.text;
				if(!string.IsNullOrEmpty(activeCode))
				{
					#if UNITY_IPHONE && !UNITY_EDITOR
					string roleName = PlayerAvatar.Player.name;
					LoggerHelper.Info(string.Format("Active Code:{0},roleName:{1} Send to sdk",activeCode,roleName));
					IOSPlugins.sendGameGiftCode(activeCode,roleName);
                    #endif
				}
				return;
			}
            if (_respCodeDescIdDict.ContainsKey((ActivateCodeRespCode)respCode))
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(_respCodeDescIdDict[(ActivateCodeRespCode)respCode]), PanelIdEnum.MainUIField);
            }
        }

        public void Show()
        {
            Visible = true;
            Refresh();
        }

        public void Close()
        {
            if (Visible)
            {
                Visible = false;
            }
        }

        public void Refresh()
        {
            ShowHintText();
        }

        private void HideHintText()
        {
            if (_hintBtn.gameObject.activeSelf)
            {
                _hintBtn.gameObject.SetActive(false);
            }
        }

        private void ShowHintText()
        {
            if (_input.text == "" && !_hintBtn.gameObject.activeSelf)
            {
                _hintBtn.gameObject.SetActive(true);
            }
        }

        private byte[] ApplyHttpData(string url, byte[] postData)
        {
            byte[] responseData = null;
            try
            {
                WebClient webClient = new WebClient();
                responseData = webClient.UploadData(url, postData);
            }
            catch (System.Exception ex)
            {
                LoggerHelper.Except(ex);
            }
            return responseData;
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

        public enum ActivateCodeRespCode
        {
            ENUM_CARD_SUCCSEE = 0,
            ENUM_DB_QUERY_ERROR,
            ENUM_NUMBER_ERROR,
            ENUM_NUMBER_ALREADY_USED,
            ENUM_SERVER_NOT_FOUND,
            ENUM_ERROR_SAME_TYPE,
            ENUM_ERROR_PLAT,
            ENUM_LESS_THAN_STARTTIME,
            ENUM_MORE_THAN_ENDTIME
        }
    }
}
