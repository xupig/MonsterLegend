﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.Data;
using GameMain.GlobalManager;
using UnityEngine;
using UnityEngine.EventSystems;
using Common.ExtendComponent;
using GameData;
using GameLoader.Utils.Timer;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager.SubSystem;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Base;
using ModuleCommonUI;
using Common.Global;
using Common.Utils;

namespace ModuleWelfareActive
{
    public class TabLuckyTurntableView : KContainer
    {
        private KToggleGroup _turntableToggleGroup;
        private List<TurntableRewardItem> _turntableRewardList = new List<TurntableRewardItem>();
        private KContainer[] _turntablePointers;
        private KButton[] _turntablePointerBtns;
        private KButton _resetBtn;
        private StateText _resetTimeText;
        private TurntablePointerRotateAnim[] _pointerRotateAnims;
        private StateImage[] _turntableBgs;
        private KToggle _skipToggle;
        private MoneyItem _luckyMoneyItem;
        private MoneyItem _diamondItem;

        private LuckyTurntableData _luckyTurntableData;
        private bool[] _isPointersAddListener;
        private uint _resetTimerId = 0;
        private bool _isInitMoneyItem = false;
        private LuckyTurntableType _curTurntableType = 0;
        private int _curPointerIndex = 0;

        private const int TURNTABLE_POINTER_NORMAL = 0;
        private const int TURNTABLE_POINTER_ADVANCED = 1;

        protected override void Awake()
        {
            _luckyTurntableData = PlayerDataManager.Instance.LuckyTurntableData;
            _turntableToggleGroup = GetChildComponent<KToggleGroup>("Container_zhuanpan/ToggleGroup_zhuanpan");
            _resetBtn = GetChildComponent<KButton>("Container_zhuanpan/Button_chongzhi");
            _resetTimeText = GetChildComponent<StateText>("Container_zhuanpan/Label_txtChongzhiTime");
            _skipToggle = GetChildComponent<KToggle>("Container_zhuanpan/Toggle_Xuanze");
            _skipToggle.isOn = _luckyTurntableData.IsSkipPointerAnim;
            _luckyMoneyItem = AddChildComponent<MoneyItem>("Container_dibu/Container_buyGold");
            _diamondItem = AddChildComponent<MoneyItem>("Container_dibu/Container_buyDiamond");
            _diamondItem.SetMoneyType(public_config.MONEY_TYPE_DIAMOND);
            for (int i = 0; i < 8; i++)
            {
                KContainer itemContainer = GetChildComponent<KContainer>("Container_zhuanpan/Container_zhuanpanneirong/Container_item0" + (i + 1));
                TurntableRewardItem rewardItem = itemContainer.gameObject.AddComponent<TurntableRewardItem>();
                _turntableRewardList.Add(rewardItem);
            }
            _turntableBgs = new StateImage[2];
            _turntableBgs[TURNTABLE_POINTER_NORMAL] = GetChildComponent<StateImage>("Container_zhuanpan/Image_xingyunzhuanpan");
            _turntableBgs[TURNTABLE_POINTER_ADVANCED] = GetChildComponent<StateImage>("Container_zhuanpan/Image_gaojizhuanpan");
            _turntableBgs[TURNTABLE_POINTER_ADVANCED].Visible = false;
            InitTurntablePointers();
        }

        private void InitTurntablePointers()
        {
            _turntablePointers = new KContainer[2];
            _turntablePointers[TURNTABLE_POINTER_NORMAL] = GetChildComponent<KContainer>("Container_zhuanpan/Container_zhuanpanzhizhen/Container_xingyun");
            _turntablePointers[TURNTABLE_POINTER_ADVANCED] = GetChildComponent<KContainer>("Container_zhuanpan/Container_zhuanpanzhizhen/Container_gaoji");

            _pointerRotateAnims = new TurntablePointerRotateAnim[2];
            GameObject pointerContainerGo = _turntablePointers[TURNTABLE_POINTER_NORMAL].GetChild("Container_xingyunjiantou");
            _pointerRotateAnims[TURNTABLE_POINTER_NORMAL] = pointerContainerGo.AddComponent<TurntablePointerRotateAnim>();
            pointerContainerGo.AddComponent<RaycastComponent>();
            pointerContainerGo = _turntablePointers[TURNTABLE_POINTER_ADVANCED].GetChild("Container_gaojijiantou");
            _pointerRotateAnims[TURNTABLE_POINTER_ADVANCED] = pointerContainerGo.AddComponent<TurntablePointerRotateAnim>();
            pointerContainerGo.AddComponent<RaycastComponent>();
            _pointerRotateAnims[TURNTABLE_POINTER_NORMAL].RegisterCallback(OnPlayAnimEnd);
            _pointerRotateAnims[TURNTABLE_POINTER_ADVANCED].RegisterCallback(OnPlayAnimEnd);

            RectTransform normalRect = _pointerRotateAnims[TURNTABLE_POINTER_NORMAL].gameObject.GetComponent<RectTransform>();
            Vector2 destPivot = new Vector2(0.5f, 0.436f);
            normalRect.pivot = destPivot;
            float deltaX = normalRect.sizeDelta.x * (1 - destPivot.x);
            float deltaY = normalRect.sizeDelta.y * (1 - destPivot.y);
            normalRect.anchoredPosition = new Vector2(normalRect.anchoredPosition.x + deltaX, normalRect.anchoredPosition.y - deltaY);
            RectTransform advancedRect = _pointerRotateAnims[TURNTABLE_POINTER_ADVANCED].gameObject.GetComponent<RectTransform>();
            advancedRect.pivot = destPivot;
            deltaX = advancedRect.sizeDelta.x * (1 - destPivot.x);
            deltaY = advancedRect.sizeDelta.y * (1 - destPivot.y);
            advancedRect.anchoredPosition = new Vector2(advancedRect.anchoredPosition.x + deltaX, advancedRect.anchoredPosition.y - deltaY);

            SetPointersRotation();

            _turntablePointerBtns = new KButton[2];
            _isPointersAddListener = new bool[2];
            _isPointersAddListener[TURNTABLE_POINTER_NORMAL] = false;
            _isPointersAddListener[TURNTABLE_POINTER_ADVANCED] = false;
        }

        private void SetPointersRotation()
        {
            int rewardIndex = _luckyTurntableData.GetPointerRewardIndex(LuckyTurntableType.Normal);
            if (rewardIndex >= 0)
            {
                float destAngle = _pointerRotateAnims[TURNTABLE_POINTER_NORMAL].CalculateDestAngle(rewardIndex);
                SetPointerRotation(TURNTABLE_POINTER_NORMAL, destAngle);
            }
            else
            {
                ResetPointerRotation(TURNTABLE_POINTER_NORMAL);
            }
            
            rewardIndex = _luckyTurntableData.GetPointerRewardIndex(LuckyTurntableType.Advanced);
            if (rewardIndex >= 0)
            {
                float destAngle = _pointerRotateAnims[TURNTABLE_POINTER_ADVANCED].CalculateDestAngle(rewardIndex);
                SetPointerRotation(TURNTABLE_POINTER_ADVANCED, destAngle);
            }
            else
            {
                ResetPointerRotation(TURNTABLE_POINTER_ADVANCED);
            }
        }

        private void SetPointerRotation(int pointerIndex, float angle)
        {
            Transform pointerTrans = _pointerRotateAnims[pointerIndex].gameObject.transform;
            pointerTrans.localEulerAngles = new Vector3(pointerTrans.localEulerAngles.x, pointerTrans.localEulerAngles.y, angle);
        }

        private void ResetPointerRotation(int pointerIndex)
        {
            SetPointerRotation(pointerIndex, 0f);
        }

        private void InitTurntablePointerBtn(int pointerBtnIndex)
        {
            if (_turntablePointerBtns[pointerBtnIndex] == null)
            {
                string path = "";
                if (pointerBtnIndex == TURNTABLE_POINTER_NORMAL)
                {
                    path = "Button_xingyunzhuan";
                }
                else if (pointerBtnIndex == TURNTABLE_POINTER_ADVANCED)
                {
                    path = "Button_gaojizhuan";
                }
                _turntablePointerBtns[pointerBtnIndex] = _turntablePointers[pointerBtnIndex].GetChildComponent<KButton>(path);
                KShrinkableButton shrinkBtn = _turntablePointerBtns[pointerBtnIndex] as KShrinkableButton;
                shrinkBtn.RefreshRectTransform();
            }
        }

        private void InitLuckyMoneyItem()
        {
            LuckyTurntableInfo turntableInfo = _luckyTurntableData.GetTurntableInfo(LuckyTurntableType.Normal);
            _luckyMoneyItem.SetMoneyType(turntableInfo.NextGetRewardCostId);
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            RemovePointerBtnListener(TURNTABLE_POINTER_NORMAL);
            RemovePointerBtnListener(TURNTABLE_POINTER_ADVANCED);
            RemoveTimer();
        }

        private void AddEventListener()
        {
            _turntableToggleGroup.onSelectedIndexChanged.AddListener(OnTurntableToggleChanged);
            _resetBtn.onClick.AddListener(OnClickResetBtn);
            _skipToggle.onValueChanged.AddListener(OnSkipToggleChanged);
            EventDispatcher.AddEventListener(LuckyTurntableEvents.GET_TURNTABLE_INFO, OnGetTurntableInfo);
            EventDispatcher.AddEventListener<int, int>(LuckyTurntableEvents.GET_TURNTABLE_REWARD, OnGetTurntableReward);
            EventDispatcher.AddEventListener<int>(LuckyTurntableEvents.RESET_TURNTABLE, OnResetTurntable);
        }

        private void RemoveEventListener()
        {
            _turntableToggleGroup.onSelectedIndexChanged.RemoveListener(OnTurntableToggleChanged);
            _resetBtn.onClick.RemoveListener(OnClickResetBtn);
            _skipToggle.onValueChanged.RemoveListener(OnSkipToggleChanged);
            EventDispatcher.RemoveEventListener(LuckyTurntableEvents.GET_TURNTABLE_INFO, OnGetTurntableInfo);
            EventDispatcher.RemoveEventListener<int, int>(LuckyTurntableEvents.GET_TURNTABLE_REWARD, OnGetTurntableReward);
            EventDispatcher.RemoveEventListener<int>(LuckyTurntableEvents.RESET_TURNTABLE, OnResetTurntable);
        }

        private void AddPointerBtnListener(int pointerBtnIndex)
        {
            if (!_isPointersAddListener[pointerBtnIndex])
            {
                _turntablePointerBtns[pointerBtnIndex].onPointerUp.AddListener(OnClickPointerBtn);
                _isPointersAddListener[pointerBtnIndex] = true;
            }
        }

        private void RemovePointerBtnListener(int pointerBtnIndex)
        {
            if (_turntablePointerBtns[pointerBtnIndex] != null && _isPointersAddListener[pointerBtnIndex])
            {
                _turntablePointerBtns[pointerBtnIndex].onPointerUp.RemoveListener(OnClickPointerBtn);
                _isPointersAddListener[pointerBtnIndex] = false;
            }
        }

        private void OnTurntableToggleChanged(KToggleGroup target, int index)
        {
            ShowTurntableContent(index);
            EventDispatcher.TriggerEvent<int>(LuckyTurntableEvents.SHOW_LUCKY_RANK_VIEW, (int)_curTurntableType);
        }

        private void OnClickPointerBtn(KButton target, PointerEventData evtData)
        {
            MessageBox.Show(true, MogoLanguageUtil.GetContent(6245006), MogoLanguageUtil.GetContent(6245007), OnConfirmRunTurntable, OnCancelRunTurntable, "", "");
        }

        private void OnConfirmRunTurntable()
        {
            LuckyTurntableManager.Instance.RequestRunTurntable((int)_curTurntableType);
        }

        private void OnCancelRunTurntable()
        {

        }

        private void OnClickResetBtn()
        {
            MessageBox.Show(true, MogoLanguageUtil.GetContent(6245008), MogoLanguageUtil.GetContent(6245009), OnConfirmResetTurntable, OnCancelResetTurntable, "", "");
        }

        private void OnConfirmResetTurntable()
        {
            LuckyTurntableManager.Instance.RequestResetTurntable((int)_curTurntableType);
        }

        private void OnCancelResetTurntable()
        {

        }

        private void OnSkipToggleChanged(KToggle target, bool isSelect)
        {
            _luckyTurntableData.IsSkipPointerAnim = isSelect;
        }

        private void OnGetTurntableInfo()
        {
            ShowTurntableContent(_turntableToggleGroup.SelectIndex);
        }

        private void OnGetTurntableReward(int turntableType, int rewardIndex)
        {
            if (!_skipToggle.isOn)
            {
                LuckyTurntableType curTurntableType = GetTurntableType(_turntableToggleGroup.SelectIndex);
                if (curTurntableType == (LuckyTurntableType)turntableType)
                {
                    int pointerIndex = GetTurntablePointerIndex((LuckyTurntableType)turntableType);
                    _pointerRotateAnims[pointerIndex].StartPlay(rewardIndex);
                    EventDispatcher.TriggerEvent(LuckyTurntableEvents.TURNTABLE_POINTER_RUNING);
                    //防止在请求手气榜数据刷新时，会把自己抽到的要加入手气榜的信息也显示出来
                    LuckyTurntableInfo turntableInfo = _luckyTurntableData.GetTurntableInfo(curTurntableType);
                    if (turntableInfo.RewardItemList[rewardIndex].isEnterLuckyRank)
                    {
                        EventDispatcher.TriggerEvent(LuckyTurntableEvents.STOP_REQUEST_LUCKY_RANK_TIMER);
                    }
                }
            }
            else
            {
                LuckyTurntableType curTurntableType = GetTurntableType(_turntableToggleGroup.SelectIndex);
                if (curTurntableType == (LuckyTurntableType)turntableType)
                {
                    int pointerIndex = GetTurntablePointerIndex((LuckyTurntableType)turntableType);
                    _pointerRotateAnims[pointerIndex].PlayToDestImmediately(rewardIndex);
                    RefreshGetTurntableReward(rewardIndex);
                }
            }
        }

        private void OnResetTurntable(int turntableType)
        {
            LuckyTurntableType curTurntableType = GetTurntableType(_turntableToggleGroup.SelectIndex);
            if (curTurntableType == (LuckyTurntableType)turntableType)
            {
                if (_pointerRotateAnims[_curPointerIndex].IsPlaying)
                {
                    _luckyTurntableData.IsTurntableReset = true;
                    return;
                }
                ShowTurntableContent(_turntableToggleGroup.SelectIndex);
                ResetPointerRotation(_curPointerIndex);
            }
        }

        private void OnPlayAnimEnd(int rewardIndex)
        {
            List<PbItem> itemList = new List<PbItem>();
            LuckyTurntableInfo turntableInfo = _luckyTurntableData.GetTurntableInfo(_curTurntableType);
            PbItem itemInfo = new PbItem();
            itemInfo.item_id = (uint)turntableInfo.RewardItemList[rewardIndex].itemId;
            itemInfo.item_count = (uint)turntableInfo.RewardItemList[rewardIndex].itemNum;
            itemList.Add(itemInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.ItemObtain, itemList);
            if (_luckyTurntableData.IsTurntableReset)
            {
                ShowTurntableContent(_turntableToggleGroup.SelectIndex);
                ResetPointerRotation(_curPointerIndex);
                _luckyTurntableData.IsTurntableReset = false;
            }
            else
            {
                RefreshGetTurntableReward(rewardIndex);
            }
            EventDispatcher.TriggerEvent(LuckyTurntableEvents.TURNTABLE_POINTER_STOP_RUN);
            EventDispatcher.TriggerEvent(LuckyTurntableEvents.START_REQUEST_LUCKY_RANK_TIMER);
        }

        public void Show()
        {
            Visible = true;
            Refresh();
        }

        public void Close()
        {
            if (Visible)
            {
                Visible = false;
            }
        }

        public void Refresh()
        {
            LuckyTurntableInfo normalTurntableInfo = _luckyTurntableData.GetTurntableInfo(LuckyTurntableType.Normal);
            LuckyTurntableInfo advancedTurntableInfo = _luckyTurntableData.GetTurntableInfo(LuckyTurntableType.Advanced);
            bool isShowContent = true;
            if (normalTurntableInfo.RewardItemList.Count == 0 || advancedTurntableInfo.RewardItemList.Count == 0)
            {
                LuckyTurntableManager.Instance.RequestGetTurntableInfo();
                isShowContent = false;
            }
            _turntableToggleGroup.SelectIndex = 0;
            if (isShowContent)
            {
                ShowTurntableContent(_turntableToggleGroup.SelectIndex);
            }
            else
            {
                UpdateCurSelectTurntableInfo(_turntableToggleGroup.SelectIndex);
            }
            EventDispatcher.TriggerEvent<int>(LuckyTurntableEvents.SHOW_LUCKY_RANK_VIEW, (int)_curTurntableType);
        }

        private void ShowTurntableContent(int toggleIndex)
        {
            if (!_isInitMoneyItem)
            {
                InitLuckyMoneyItem();
                _isInitMoneyItem = true;
            }
            UpdateCurSelectTurntableInfo(toggleIndex);
            LuckyTurntableInfo turntableInfo = _luckyTurntableData.GetTurntableInfo(_curTurntableType);
            RefreshTurntableBg(_curTurntableType);
            RefreshTurntableRewardList(turntableInfo);
            RefreshTurntablePointerInfo(turntableInfo);
            RefreshResetButton(turntableInfo);
            RefreshResetLeftTime(turntableInfo);
        }

        private void UpdateCurSelectTurntableInfo(int toggleIndex)
        {
            LuckyTurntableType type = GetTurntableType(toggleIndex);
            _curTurntableType = type;
            _curPointerIndex = GetTurntablePointerIndex(type);
        }

        private void RefreshTurntableBg(LuckyTurntableType curTurntableType)
        {
            if (curTurntableType == LuckyTurntableType.Normal)
            {
                _turntableBgs[TURNTABLE_POINTER_NORMAL].Visible = true;
                _turntableBgs[TURNTABLE_POINTER_ADVANCED].Visible = false;
            }
            else if (curTurntableType == LuckyTurntableType.Advanced)
            {
                _turntableBgs[TURNTABLE_POINTER_NORMAL].Visible = false;
                _turntableBgs[TURNTABLE_POINTER_ADVANCED].Visible = true;
            }
        }

        private void RefreshTurntableRewardList(LuckyTurntableInfo turntableInfo)
        {
            List<TurntableRewardItemData> rewardItemList = turntableInfo.RewardItemList;
            for (int i = 0; i < rewardItemList.Count; i++)
            {
                _turntableRewardList[i].SetItemData(rewardItemList[i]);
            }
        }

        private void RefreshGetTurntableReward(int rewardIndex)
        {
            LuckyTurntableInfo turntableInfo = _luckyTurntableData.GetTurntableInfo(_curTurntableType);
            _turntableRewardList[rewardIndex].SetItemData(turntableInfo.RewardItemList[rewardIndex]);
            RefreshTurntablePointerBtnInfo(turntableInfo);
            if (turntableInfo.RewardItemList[rewardIndex].isEnterLuckyRank)
            {
                LuckyTurntableManager.Instance.RequestLuckyRankList((int)_curTurntableType);
            }
        }

        private void RefreshTurntablePointerInfo(LuckyTurntableInfo turntableInfo)
        {
            int pointerBtnIndex = _curPointerIndex;
            _turntablePointers[pointerBtnIndex].Visible = true;
            if (pointerBtnIndex == TURNTABLE_POINTER_NORMAL)
            {
                _turntablePointers[TURNTABLE_POINTER_ADVANCED].Visible = false;
            }
            else
            {
                _turntablePointers[TURNTABLE_POINTER_NORMAL].Visible = false;
            }
            InitTurntablePointerBtn(pointerBtnIndex);
            AddPointerBtnListener(pointerBtnIndex);
            RefreshTurntablePointerBtnInfo(turntableInfo);
        }

        private void RefreshTurntablePointerBtnInfo(LuckyTurntableInfo turntableInfo)
        {
            StateIcon btnStateIcon = _turntablePointerBtns[_curPointerIndex].GetChildComponent<StateIcon>("stateIcon");
            int costId = turntableInfo.NextGetRewardCostId;
            int costNum = turntableInfo.NextGetRewardCostNum;
            int iconId = item_helper.GetIcon(costId);
            btnStateIcon.SetIcon(iconId);
            StateText costNumText = _turntablePointerBtns[_curPointerIndex].GetChildComponent<StateText>("label");
            string strNum = string.Format("x{0}", costNum);
            if (costNumText.CurrentText.text != strNum)
            {
                costNumText.ChangeAllStateText(strNum);
            }
        }

        private void RefreshResetButton(LuckyTurntableInfo turntableInfo)
        {
            int resetIconId = item_helper.GetIcon(turntableInfo.ResetCostId);
            StateIcon resetBtnIcon = _resetBtn.GetChildComponent<StateIcon>("stateIcon");
            resetBtnIcon.SetIcon(resetIconId);
            StateText resetCostNumText = _resetBtn.GetChildComponent<StateText>("costNum");
            string strResetNum = string.Format("x{0}", turntableInfo.ResetCostNum);
            if (resetCostNumText.CurrentText.text != strResetNum)
            {
                resetCostNumText.ChangeAllStateText(strResetNum);
            }
        }

        private void RefreshResetLeftTime(LuckyTurntableInfo turntableInfo)
        {
            if (turntableInfo.IsAutoReset)
            {
                if (!_resetTimeText.Visible)
                {
                    _resetTimeText.Visible = true;
                }
                RemoveTimer();
                if (turntableInfo.ResetLeftTime > 0)
                {
                    if (_resetTimerId == 0)
                    {
                        _resetTimerId = TimerHeap.AddTimer<LuckyTurntableInfo>(0, 1000, OnRefreshResetLeftTime, turntableInfo);
                    }
                }
                else
                {
                    OnRefreshResetLeftTime(turntableInfo);
                }
            }
            else
            {
                _resetTimeText.Visible = false;
                RemoveTimer();
            }
        }

        private void OnRefreshResetLeftTime(LuckyTurntableInfo turntableInfo)
        {
            int leftSeconds = turntableInfo.ResetLeftTime;
            string timeFormat = "";
            if (leftSeconds >= 3600)
            {
                int hour = leftSeconds / 3600;
                int minute = (leftSeconds - hour * 3600) / 60;
                int second = leftSeconds - hour * 3600 - minute * 60;
                timeFormat = string.Format("{0}:{1}:{2}", GetTimeString(hour), GetTimeString(minute), GetTimeString(second));
            }
            else if (leftSeconds >= 60)
            {
                int minute = leftSeconds / 60;
                int second = leftSeconds - minute * 60;
                timeFormat = string.Format("00:{0}:{1}", GetTimeString(minute), GetTimeString(second));
            }
            else
            {
                timeFormat = string.Format("00:00:{0}", GetTimeString(leftSeconds));
            }
            string timeDesc = string.Format(MogoLanguageUtil.GetContent(6245010), timeFormat);
            _resetTimeText.CurrentText.text = timeDesc;
            if (leftSeconds == 0)
            {
                RemoveTimer();
            }
        }

        private string GetTimeString(int timeNum)
        {
            string timeStr = timeNum > 9 ? timeNum.ToString() : string.Format("0{0}", timeNum);
            return timeStr;
        }

        private void RemoveTimer()
        {
            if (_resetTimerId != 0)
            {
                TimerHeap.DelTimer(_resetTimerId);
                _resetTimerId = 0;
            }
        }

        private LuckyTurntableType GetTurntableType(int toggleIndex)
        {
            switch (toggleIndex)
            {
                case 0:
                    return LuckyTurntableType.Normal;
                case 1:
                    return LuckyTurntableType.Advanced;
                default:
                    return LuckyTurntableType.Normal;
            }
        }

        private int GetTurntablePointerIndex(LuckyTurntableType type)
        {
            int pointerBtnIndex = 0;
            switch (type)
            {
                case LuckyTurntableType.Normal:
                    pointerBtnIndex = TURNTABLE_POINTER_NORMAL;
                    break;
                case LuckyTurntableType.Advanced:
                    pointerBtnIndex = TURNTABLE_POINTER_ADVANCED;
                    break;
                default:
                    break;
            }
            return pointerBtnIndex;
        }

        protected override void OnDestroy()
        {
            RemoveTimer();
        }
        
    }
}
