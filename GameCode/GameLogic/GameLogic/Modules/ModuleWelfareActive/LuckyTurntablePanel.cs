﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Events;
using Game.UI.UIComponent;
using MogoEngine.Events;

namespace ModuleWelfareActive
{
    public class LuckyTurntablePanel : BasePanel
    {
        private TabLuckyTurntableView _tabLuckyTurntableView;   //幸运转盘Tab界面
        private LuckyTurntableRankView _luckyRankView;
        private StateImage _turntableAnimMaskImg;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.LuckyTurntable; }
        }

        protected override void Awake()
        {
            _tabLuckyTurntableView = AddChildComponent<TabLuckyTurntableView>("Container_xingyunzhuanpan");
            _luckyRankView = AddChildComponent<LuckyTurntableRankView>("Container_xingyunzhuanpan/Container_zhuanpan/Container_shouqibang");
            _turntableAnimMaskImg = GetChildComponent<StateImage>("Container_xingyunzhuanpan/Image_turntableAnimMask");
            ModalMask = _turntableAnimMaskImg;
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
            _tabLuckyTurntableView.Show();
        }

        public override void SetData(object data)
        {
            HideTurntableAnimMask();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<int>(LuckyTurntableEvents.SHOW_LUCKY_RANK_VIEW, OnShowLuckyRankView);
            EventDispatcher.AddEventListener(LuckyTurntableEvents.TURNTABLE_POINTER_RUNING, OnTurntablePointerRunning);
            EventDispatcher.AddEventListener(LuckyTurntableEvents.TURNTABLE_POINTER_STOP_RUN, OnTurntablePointerStopRun);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<int>(LuckyTurntableEvents.SHOW_LUCKY_RANK_VIEW, OnShowLuckyRankView);
            EventDispatcher.RemoveEventListener(LuckyTurntableEvents.TURNTABLE_POINTER_RUNING, OnTurntablePointerRunning);
            EventDispatcher.RemoveEventListener(LuckyTurntableEvents.TURNTABLE_POINTER_STOP_RUN, OnTurntablePointerStopRun);
        }

        private void OnShowLuckyRankView(int turntableType)
        {
            _luckyRankView.Refresh(turntableType);
        }

        private void OnTurntablePointerRunning()
        {
            if (!_turntableAnimMaskImg.Visible)
            {
                _turntableAnimMaskImg.Visible = true;
            }
        }

        private void OnTurntablePointerStopRun()
        {
            HideTurntableAnimMask();
        }

        private void HideTurntableAnimMask()
        {
            if (_turntableAnimMaskImg.Visible)
            {
                _turntableAnimMaskImg.Visible = false;
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }
    }
}
