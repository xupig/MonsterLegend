﻿using Common.Events;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleWelfareActive
{
    public class OpenServerCountdown : KContainer
    {
        private uint _timerId = TimerHeap.INVALID_ID;

        private StateText _text;

        protected override void Awake()
        {
            _text = GetChildComponent<StateText>("Label_txtShijian");
        }

        protected override void OnDisable()
        {
            RemoveTimer();
        }

        public void Refresh()
        {
            if (!PlayerDataManager.Instance.OpenServerData.IsEnd())
            {
                AddTimer();
            }
            else
            {
                RemoveTimer();
            }
        }

        private void AddTimer()
        {
            if (_timerId == TimerHeap.INVALID_ID)
            {
                _timerId = TimerHeap.AddTimer(0, 1000, OnTick);
            }
        }

        private void OnTick()
        {
            if (PlayerDataManager.Instance.OpenServerData.IsEnd())
            {
                RemoveTimer();
                EventDispatcher.TriggerEvent(OpenServerEvents.CountdownEnd);
                return;
            }
            _text.CurrentText.text = PlayerDataManager.Instance.OpenServerData.GetLeftTimeString();
        }

        private void RemoveTimer()
        {
            if (_timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = TimerHeap.INVALID_ID;
            }
        }
    }
}
