﻿using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleWelfareActive
{
    public class OpenServerRewardView : KContainer
    {
        private int _day;
        private int _toggleGroupIndex;
        private List<int> _typeList;
        private List<OpenServerData.Info> _idList;

        private OpenServerData _data
        {
            get
            {
                return PlayerDataManager.Instance.OpenServerData;
            }
        }

        private StateText _textCountdown;
        private KScrollView _scrollView;
        private KList _list;
        private KToggleGroup _toggleGroup;
        private List<KToggle> _sourceToggleList;
        private OpenServerCountdown _countdown;

        protected override void Awake()
        {
            _textCountdown = GetChildComponent<StateText>("Label_txtShijian");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_tianhuodong");
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            _countdown = gameObject.AddComponent<OpenServerCountdown>();
            _sourceToggleList = _toggleGroup.GetToggleList();
            uint createTime = PlayerAvatar.Player.create_time;
            InitList();
        }

        private void InitList()
        {
            _list = GetChildComponent<KList>("ScrollView_tianhuodong/mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _list.SetGap(5, 0);
        }

        protected override void OnEnable()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            AddListenerWhenEnable();
        }

        protected override void OnDisable()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            AddListenerWhenDisable();
        }

        private void AddListenerWhenEnable()
        {
            EventDispatcher.AddEventListener(OpenServerEvents.Update, OnUpdate);
        }

        private void AddListenerWhenDisable()
        {
            EventDispatcher.RemoveEventListener(OpenServerEvents.Update, OnUpdate);
        }

        public void Refresh(int day)
        {
            _day = day;
            _toggleGroupIndex = 0;

            _toggleGroup.SelectIndex = _toggleGroupIndex;
            InitToggeGroupData(day);
            RefreshToggleGroup();
            OnSelectedIndexChanged(_toggleGroup, _toggleGroup.SelectIndex);
        }

        private void OnUpdate()
        {
            RefreshToggleGroup();
            OnSelectedIndexChanged(_toggleGroup, _toggleGroup.SelectIndex);
        }

        private void InitToggeGroupData(int day)
        {
            OpenServerData.TypeData typeData = _data.DayDict[day];
            List<int> typeList = typeData.TypeDict.Keys.ToList();
            typeList.Sort(SortTypeData);
            _typeList = typeList;
        }

        private void RefreshToggleGroup()
        {
            OpenServerData.TypeData typeData = _data.DayDict[_day];
            int currentDay = _data.GetCurrentDay();
            for (int i = 0; i < _sourceToggleList.Count; i++)
            {
                KToggle toggle = _sourceToggleList[i];
                if (i < _typeList.Count)
                {
                    toggle.Visible = true;
                    OpenServerData.IdData idData = typeData.TypeDict[_typeList[i]];
                    string toggleStr = string.Empty;
                    int gotRewardCount = idData.getGotRewardCount();
                    if(gotRewardCount == idData.IdDict.Count)
                    {
                        toggle.SetLabel(MogoLanguageUtil.GetContent(_typeList[i]));
                    }
                    else
                    {
                        toggle.SetLabel(string.Format("{0}({1}/{2})", MogoLanguageUtil.GetContent(_typeList[i]), gotRewardCount, idData.IdDict.Count));
                    }
                    toggle.SetGreenPoint(idData.HaveReward() && (currentDay >= _day));
                }
                else
                {
                    toggle.Visible = false;
                }
            }
        }

        private int SortTypeData(int x, int y)
        {
            if (x < y)
            {
                return -1;
            }
            return 1;
        }

        private void OnSelectedIndexChanged(KToggleGroup toggleGroup, int index)
        {
            _toggleGroup.SelectIndex = index;
            _scrollView.ResetContentPosition();
            OpenServerData.TypeData typeData = _data.DayDict[_day];
            OpenServerData.IdData idDataList = typeData.TypeDict[_typeList[index]];
            _idList = idDataList.IdDict.Values.ToList();
            _countdown.Refresh();
            _idList.Sort(SortIdData);
            _list.SetDataList<OpenServerRewardItem>(_idList, 1);
        }

        private int SortIdData(OpenServerData.Info x, OpenServerData.Info y)
        {
            if (x.State == y.State)
            {
                if (x.Issue.__id < y.Issue.__id)
                {
                    return -1;
                }
            }
            else
            {
                if (x.State == OpenServerData.State.completed)
                {
                    return -1;
                }
                else if (x.State == OpenServerData.State.processing && y.State == OpenServerData.State.gotReward)
                {
                    return -1;
                }
            }
            return 1;
        }

        class OpenServerRewardItem : KList.KListItemBase
        {
            private StateText _textDescription;
            private KProgressBar _progressBar;
            private StateText _textProgress;
            private RewardItemList _rewardItemList;
            private KButton _btnGoTo;
            private KButton _btnGetReward;
            private KParticle _particle;

            private OpenServerData.Info _data;

            protected override void Awake()
            {
                _textDescription = GetChildComponent<StateText>("Label_txtMiaoshu");
                _progressBar = GetChildComponent<KProgressBar>("ProgressBar_jindu");
                _textProgress = GetChildComponent<StateText>("Label_txtJindu");
                _btnGoTo = GetChildComponent<KButton>("Button_qianwang");
                _btnGetReward = GetChildComponent<KButton>("Button_queding");
                _particle = AddChildComponent<KParticle>("Button_queding/fx_ui_3_2_lingqu");
                MaskEffectItem effectItem1 = gameObject.AddComponent<MaskEffectItem>();
                effectItem1.Init(_btnGetReward.gameObject, CheckEffect);
                _rewardItemList = AddChildComponent<RewardItemList>("Container_jiangli");

                AddListenerWhenAwake();
            }

            private bool CheckEffect()
            {
                switch (_data.State)
                {
                    case OpenServerData.State.processing:
                    case OpenServerData.State.gotReward:
                        return false;
                    case OpenServerData.State.completed:
                        return true;
                }
                return true;
            }

            public override void Dispose()
            {
                RemoveListenerWhenDispose();
            }

            private void AddListenerWhenAwake()
            {
                _btnGoTo.onClick.AddListener(OnGoTo);
                _btnGetReward.onClick.AddListener(OnGetReward);
            }

            private void RemoveListenerWhenDispose()
            {
                _btnGoTo.onClick.RemoveListener(OnGoTo);
                _btnGetReward.onClick.RemoveListener(OnGetReward);
            }

            private void OnGoTo()
            {
                if (function_helper.IsFunctionOpen(_data.Issue.__sys_id))
                {
                    int eventId = activities_issue_helper.GetEventId(_data.Issue);
                    event_cnds_helper.Follow(eventId);
                }
                else
                {
                    MessageBox.Show(true, MessageBox.DEFAULT_TITLE, function_helper.GetConditionDesc(_data.Issue.__sys_id));
                }
            }

            private void OnGetReward()
            {
                OpenServerManager.Instance.RequesetGetReward(_data.Issue.__id);
            }

            public override object Data
            {
                set
                {
                    _data = value as OpenServerData.Info;
                    Refresh();
                }
            }

            private void Refresh()
            {
                RefreshDesciption();
                RefreshProgress();
                _rewardItemList.SetData(_data.Issue.__reward_id);
                RefreshBtn();
            }

            private void RefreshDesciption()
            {
                _textDescription.CurrentText.text = MogoLanguageUtil.GetContent(_data.Issue.__chinese, activities_issue_helper.GetEventCount(_data.Issue));
            }

            private void RefreshProgress()
            {
                int totalNum = int.Parse(_data.Issue.__conds.Values.ToList()[0]);
                int nowNum = _data.State == OpenServerData.State.processing ? _data.Num : totalNum;
                _textProgress.CurrentText.text = string.Format("{0}/{1}", nowNum, totalNum);
                _progressBar.Value = ((float)nowNum) / totalNum;
            }

            private void RefreshBtn()
            {
                _btnGoTo.Visible = false;
                _btnGetReward.Visible = false;
                switch (_data.State)
                {
                    case OpenServerData.State.processing:
                        int eventId = activities_issue_helper.GetEventId(_data.Issue);
                        if(eventId != -1 && event_cnds_helper.ContainFollowId(eventId, event_cnds_helper.FollowId.showView))
                        {
                            _btnGoTo.Visible = true;
                        }
                        break;
                    case OpenServerData.State.completed:
                        _btnGetReward.Visible = true;
                        _btnGetReward.SetButtonActive();
                        _particle.Play(true);
                        MogoGameObjectHelper.SetButtonLabel(_btnGetReward, MogoLanguageUtil.GetContent(114977));
                        break;
                    case OpenServerData.State.gotReward:
                        _btnGetReward.Visible = true;
                        _btnGetReward.SetButtonDisable();
                        _particle.Stop();
                        MogoGameObjectHelper.SetButtonLabel(_btnGetReward, MogoLanguageUtil.GetContent(114978));
                        break;
                }
            }
        }
    }
}
