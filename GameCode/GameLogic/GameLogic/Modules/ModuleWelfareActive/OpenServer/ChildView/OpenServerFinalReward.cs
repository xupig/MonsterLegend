﻿using ACTSystem;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleWelfareActive
{
    public class OpenServerFinalReward : KContainer
    {
        private GameObject _wingGameObject;

        private WelfareRewardItemList _rewardItemList;
        private StateText _textProgress;
        private StateText _textCurrent;
        private StateText _textTotal;
        private KButton _btnGetReward;
        private KContainer _containerWing;
        private StateText _textDesciption;
        private OpenServerCountdown _countdown;

        private OpenServerData _data
        {
            get
            {
                return PlayerDataManager.Instance.OpenServerData;
            }
        }

        public KComponentEvent onGoTo = new KComponentEvent();

        protected override void Awake()
        {
            _countdown = gameObject.AddComponent<OpenServerCountdown>();
            _rewardItemList = AddChildComponent<WelfareRewardItemList>("Container_left");
            _textProgress = GetChildComponent<StateText>("Container_right/Label_txtDangqianjindu");
            _textCurrent = GetChildComponent<StateText>("Container_right/Label_txtDangqianjincheng");
            _textTotal = GetChildComponent<StateText>("Container_right/Label_txtSuoyoujincheng");
            _btnGetReward = GetChildComponent<KButton>("Container_right/Button_lingqu");
            _containerWing = GetChildComponent<KContainer>("Container_chibangmoxing");
            _textDesciption = GetChildComponent<StateText>("Container_right/Label_txtJieshao02");
            _textDesciption.CurrentText.text = MogoLanguageUtil.GetContent(114984, activities_open_helper.GetCreateRoleDay(), activities_open_helper.GetCreateRoleDay());

            AddListenerWhenAwake();
        }

        protected override void OnDestroy()
        {
            RemoveListenerWhenDestroy();
        }

        private void AddListenerWhenAwake()
        {
            _btnGetReward.onClick.AddListener(OnGetReward);
        }

        private void RemoveListenerWhenDestroy()
        {
            _btnGetReward.onClick.RemoveListener(OnGetReward);
        }

        protected override void OnEnable()
        {
            AddListenerWhenEnable();
        }

        protected override void OnDisable()
        {
            AddListenerWhenDisable();
        }

        private void AddListenerWhenEnable()
        {
            EventDispatcher.AddEventListener(OpenServerEvents.Update, OnUpdate);
        }

        private void AddListenerWhenDisable()
        {
            EventDispatcher.RemoveEventListener(OpenServerEvents.Update, OnUpdate);
        }

        private void OnGetReward()
        {
            if (_data.CanGetFinalReward())
            {
                OpenServerManager.Instance.RequesetGetFinalReward();
            }
            else
            {
                onGoTo.Invoke();
            }
        }

        private void OnUpdate()
        {
            Refresh();
        }

        public void Refresh()
        {
            int rewardId = int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.open_server_final_rewards));
            RefreshWingModel(rewardId);
            _rewardItemList.RefreshReward(rewardId, false);
            RefreshLabel();
            RefreshBtn();
            _countdown.Refresh();
        }

        private void RefreshWingModel(int rewardId)
        {
            if (_wingGameObject != null)
            {
                return;
            }
            List<BaseItemData> itemDataList = item_reward_helper.GetItemDataList(rewardId);
            int wingId = 0;
            for (int i = 0; i < itemDataList.Count; i++)
            {
                BaseItemData baseItemData = itemDataList[i];
                if (baseItemData.Type == BagItemType.Wing)
                {
                    wingId = item_helper.GetWingId(baseItemData.Id);
                    break;
                }
            }
            WingItemData wingItemData = PlayerDataManager.Instance.WingData.GetItemDataById(wingId);
            var equipData = ACTRunTimeData.GetEquipmentData(wingItemData.ModelId);
            ACTSystemDriver.GetInstance().ObjectPool.assetLoader.GetGameObject(equipData.PrefabPath, delegate(UnityEngine.Object obj)
            {
                if (_wingGameObject != null)
                {
                    return;
                }
                var wingGameObject = obj as GameObject;
                _wingGameObject = wingGameObject;
                wingGameObject.transform.SetParent(_containerWing.transform, false);
                ACTSystemTools.SetLayer(wingGameObject.transform, gameObject.layer, true);
                int quality = (int)ACTSystemDriver.GetInstance().CurVisualQuality;
                ACTWingQuality.UpdateWingQuality(wingGameObject);
                wingGameObject.AddComponent<SortOrderedRenderAgent>();
                ComputeWingPosition(wingGameObject);
            });
        }

        private void ComputeWingPosition(GameObject go)
        {
            RectTransform rectContainer = _containerWing.GetComponent<RectTransform>();
            go.transform.localPosition = new Vector3(rectContainer.sizeDelta.x / 2, -rectContainer.sizeDelta.y / 2 - 40, -300);
            go.transform.localRotation = Quaternion.Euler(90, 270, 0);
            go.transform.localScale = new Vector3(150, 150, 150);
            ModelComponentUtil.ChangeUIModelParam(go);
        }

        private void RefreshLabel()
        {
            int currentRate = _data.GetCurrentRate();
            _textProgress.CurrentText.text = MogoLanguageUtil.GetContent(114974, currentRate, 100);
            _textCurrent.CurrentText.text = MogoLanguageUtil.GetContent(114975,
                string.Concat(currentRate, "%"));
            _textTotal.CurrentText.text = MogoLanguageUtil.GetContent(114976, 
                string.Concat(global_params_helper.GetGlobalParam(GlobalParamId.oper_server_final_rewards_advance), "%"));
        }

        private void RefreshBtn()
        {
            if (_data.HaveGotFinalReward())
            {
                _btnGetReward.SetButtonDisable();
                MogoGameObjectHelper.SetButtonLabel(_btnGetReward, MogoLanguageUtil.GetContent(114992));
            }
            else if (_data.CanGetFinalReward())
            {
                _btnGetReward.SetButtonActive();
                MogoGameObjectHelper.SetButtonLabel(_btnGetReward, MogoLanguageUtil.GetContent(114993));
            }
            else
            {
                _btnGetReward.SetButtonActive();
                MogoGameObjectHelper.SetButtonLabel(_btnGetReward, MogoLanguageUtil.GetContent(114994));
            }
        }
    }
}
