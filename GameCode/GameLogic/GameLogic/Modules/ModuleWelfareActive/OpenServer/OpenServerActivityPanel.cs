﻿using Common;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleWelfareActive
{
    public class OpenServerActivityPanel : BasePanel
    {
        private int _currentIndex = 0;
        private bool _haveRead = false;
        private static List<int> _isFirstOpen;

        private OpenServerData _data
        {
            get
            {
                return PlayerDataManager.Instance.OpenServerData;
            }
        }

        private CategoryList _categoryList;
        private OpenServerRewardView _rewardView;
        private OpenServerFinalReward _finalReward;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.OpenServerActivity; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            InitCategoryList();
            _rewardView = AddChildComponent<OpenServerRewardView>("Container_renwujindu");
            _finalReward = AddChildComponent<OpenServerFinalReward>("Container_zuizhongdajiang");
        }

        private void InitCategoryList()
        {
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            List<CategoryItemData> dataList = new List<CategoryItemData>();
            dataList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(114973)));
            for (int i = 0; i < OpenServerData.MAX_DAY; i++)
            {
                dataList.Add(
                    CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(114972, MogoLanguageUtil.GetChineseNum(i+1)))
                );
            }
            _categoryList.SetDataList<CategoryListItem>(dataList, 2);
        }

        public override void OnShow(object data)
        {
            AddListener();
            ParseIndex(); 
            OnSelectedIndexChanged(_categoryList, _currentIndex);
        }

        private void ParseIndex()
        {
            if(!_haveRead)
            {
                _haveRead = true;
                _isFirstOpen = LocalCache.Read<List<int>>(LocalName.OpenServer);
            }

            if (_isFirstOpen == null)
            {
                _isFirstOpen = new List<int>();
                LocalCache.Write(LocalName.OpenServer, _isFirstOpen, CacheLevel.Permanent);
                _currentIndex = 0;
            }
            else
            {
                _currentIndex = GetAutoSelectDay();
            }
        }

        private int GetAutoSelectDay()
        {
            int currentDay = _data.GetCurrentDay();
            int selectDay = OpenServerData.MAX_DAY;
            if (currentDay > OpenServerData.MAX_DAY)
            {
                for (int i = 0; i < OpenServerData.MAX_DAY; i++)
                {
                    if (_data.IsDayHaveReward(i + 1))
                    {
                        selectDay = i + 1;
                        break;
                    }
                }
            }
            else
            {
                selectDay = currentDay;
            }
            return selectDay;
        }

        private void AddListener()
        {
            _categoryList.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            _categoryList.onCoroutineEnd.AddListener(OnCoroutineEnd);
            _finalReward.onGoTo.AddListener(OnGoTo);
            EventDispatcher.AddEventListener(OpenServerEvents.Update, RefreshCatrgoryListGreenPoint);
        }

        private void RemoveListener()
        {
            _categoryList.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            _finalReward.onGoTo.RemoveListener(OnGoTo);
            _categoryList.onCoroutineEnd.RemoveListener(OnCoroutineEnd);
            EventDispatcher.RemoveEventListener(OpenServerEvents.Update, RefreshCatrgoryListGreenPoint);
        }

        private void OnCoroutineEnd()
        {
            _categoryList.SelectedIndex = _currentIndex;
            RefreshCatrgoryListGreenPoint();
        }

        private void OnCountdownEnd()
        {
            ClosePanel();
        }

        private void OnGoTo()
        {
            int currentDay = _data.GetCurrentDay();
            if (currentDay <= OpenServerData.MAX_DAY)
            {
                _currentIndex = currentDay;
            }
            else
            {
                _currentIndex = GetAutoSelectDay();
            }
            OnSelectedIndexChanged(_categoryList, _currentIndex);
        }

        private void OnSelectedIndexChanged(CategoryList catrgoryList, int index)
        {
            _currentIndex = index;
            _categoryList.SelectedIndex = _currentIndex;
            RefreshCatrgoryListGreenPoint();
            RefreshContent();
        }

        private void RefreshCatrgoryListGreenPoint()
        {
            if (_categoryList.ItemList.Count != (OpenServerData.MAX_DAY + 1))
            {
                return;
            }
            int currentDay = _data.GetCurrentDay();
            for (int i = 0; i < _categoryList.ItemList.Count; i++)
            {
                CategoryListItem item = _categoryList.ItemList[i] as CategoryListItem;
                if (i == 0)
                {
                    item.SetPoint(_data.CanGetFinalReward());
                }
                else
                {
                    item.SetPoint(_data.IsDayHaveReward(i) && currentDay >= i);
                }
            }
        }

        private void RefreshContent()
        {
            if(_currentIndex == 0)
            {
                _rewardView.Visible = false;
                _finalReward.Visible = true;
                _finalReward.Refresh();
            }
            else
            {
                _finalReward.Visible = false;
                _rewardView.Visible = true;
                _rewardView.Refresh(_currentIndex);
            }
        }

        public override void OnClose()
        {
            RemoveListener();
        }
    }
}
