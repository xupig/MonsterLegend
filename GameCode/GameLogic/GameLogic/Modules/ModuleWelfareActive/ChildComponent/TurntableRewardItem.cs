﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.Data;
using Common.ExtendComponent;

namespace ModuleWelfareActive
{
    public class TurntableRewardItem : KContainer
    {
        private TurntableRewardItemData _data;
        private ItemGrid _rewardGrid;
        private StateImage _haveReceivedImg;
        private StateText _rewardNumText;
        private int _rewardItemId = 0;

        protected override void Awake()
        {
            _rewardGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _haveReceivedImg = GetChildComponent<StateImage>("Image_sharedyilingqu");
            _rewardNumText = GetChildComponent<StateText>("Label_txtShuliang");
            _haveReceivedImg.Visible = false;
        }

        public void SetItemData(TurntableRewardItemData itemData)
        {
            _data = itemData;
            Refresh();
        }

        private void Refresh()
        {
            RefreshRewardInfo();
            if (_data.isReceived)
            {
                _haveReceivedImg.Visible = true;
            }
            else
            {
                _haveReceivedImg.Visible = false;
            }
        }

        private void RefreshRewardInfo()
        {
            if (_rewardItemId != _data.itemId)
            {
                _rewardItemId = _data.itemId;
                _rewardGrid.SetItemData(_data.itemId);
            }
            _rewardNumText.CurrentText.text = string.Format("x{0}", _data.itemNum);
        }
    }
}
