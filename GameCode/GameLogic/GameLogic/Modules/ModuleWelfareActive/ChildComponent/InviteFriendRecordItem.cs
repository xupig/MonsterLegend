﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleLogin;
using MogoEngine.Utils;

namespace ModuleWelfareActive
{
    public class InviteFriendRecordItem : KList.KListItemBase
    {
        private KContainer _container;
        private KButton _btn;
        private StateText _btnText;
        private StateText _txtLevel;
        private StateText _txtPower;
        private StateText _txtName;
        private StateText _txtLoginDayNum;
        private StateText _txtInvitePlayerName;
        private IconContainer _iconContainer;
        private InviteFriendRecordData _data;

        protected override void Awake()
        {
            _container = GetChildComponent<KContainer>("Container_friend");
            _btn = _container.GetChildComponent<KButton>("Button_yaoqing");
            _btnText = _btn.GetChildComponent<StateText>("label");
            _txtLevel = _container.GetChildComponent<StateText>("Container_dengji/Label_txtLevel");
            _txtPower = _container.GetChildComponent<StateText>("Label_txtBattlePower");
            _txtName = _container.GetChildComponent<StateText>("Label_txtFriendName");
            _txtLoginDayNum = _container.GetChildComponent<StateText>("Label_txtLoginDayNum");
            _txtInvitePlayerName = _container.GetChildComponent<StateText>("Label_txtInvitePlayerName");
            _iconContainer = _container.AddChildComponent<IconContainer>("Container_icon");
            AddEventListener();
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = (InviteFriendRecordData)value;
                Refresh();
            }
        }

        public override void Dispose()
        {
            RemoveEventListener();
        }

        private void Refresh()
        {
            Visible = _data != null;
            if (_data != null)
            {
                uint loginDay = _data.inviteRecord.login_days;
                _txtLevel.ChangeAllStateText(_data.inviteRecord.level.ToString());
                _txtPower.ChangeAllStateText(_data.inviteRecord.fight_force.ToString());
                _txtName.ChangeAllStateText(MogoProtoUtils.ParseByteArrToString(_data.inviteRecord.name_bytes));
                _iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_data.inviteRecord.vocation));
                _txtInvitePlayerName.ChangeAllStateText(MogoLanguageUtil.GetContent(_data.isBeInvite ? 85023 : 85022));
                _txtLoginDayNum.ChangeAllStateText(string.Format(MogoLanguageUtil.GetContent(85027), loginDay > 9 ? "" + loginDay : "0" + loginDay)); //"已登录：{0}天"
                _txtLoginDayNum.Visible = _data.isBeInvite;
                
                if (_data.isFriend)
                { //已为好友
                    _btnText.ChangeAllStateText(MogoLanguageUtil.GetContent(85025));
                }
                else
                { //未为好友
                    _btnText.ChangeAllStateText(MogoLanguageUtil.GetContent(85024));
                }
            }
        }

        private void AddEventListener()
        {
            _btn.onClick.AddListener(OnHandler);
        }

        private void RemoveEventListener()
        {
            _btn.onClick.RemoveListener(OnHandler);
        }

        //加为好友
        private void OnHandler()
        {
            if (_data.isFriend)
            {//密聊
                PbFriendAvatarInfo avatarInfo = new PbFriendAvatarInfo();
                avatarInfo.name = MogoProtoUtils.ParseByteArrToString(_data.inviteRecord.name_bytes);
                UIManager.Instance.ClosePanel(PanelIdEnum.InviteFriendRecord);
                UIManager.Instance.ShowPanel(PanelIdEnum.Chat, avatarInfo);
            }
            else
            {//发起：加为好友申请
                PbFriendAvatarInfo avatarInfo = new PbFriendAvatarInfo();
                avatarInfo.dbid = _data.inviteRecord.dbid;
                avatarInfo.name = MogoProtoUtils.ParseByteArrToString(_data.inviteRecord.name_bytes);
                FriendManager.Instance.AddFriend(avatarInfo);
            }
        }
    }
}
