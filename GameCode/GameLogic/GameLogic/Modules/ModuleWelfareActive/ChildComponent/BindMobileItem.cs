﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using ModuleCommonUI;

namespace ModuleWelfareActive
{
    /// <summary>
    /// 绑定手机中单个奖励项
    /// </summary>
    public class BindMobileItem : KList.KListItemBase
    {
        private RewardData _data;
        private StateText _labNum;
        private ItemGrid _itemGrid;
        private StateImage _stateImage;
        private KDummyButton _goodsDummy;

        protected override void Awake()
        {
            _labNum = GetChildComponent<StateText>("Label_txtShuliang");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _stateImage = GetChildComponent<StateImage>("Image_sharedyilingqu");
            _goodsDummy = _itemGrid.AddChildComponent<KDummyButton>("Container_icon");
            _goodsDummy.onClick.AddListener(onViewGoods);
            _labNum.ChangeAllStateTextAlignment(UnityEngine.TextAnchor.MiddleCenter);
            _labNum.CurrentText.text = "";
            Refresh();
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = (RewardData)value;
                Refresh();
            }
        }

        public override void Dispose()
        {
            //_labNum = null;
            _goodsDummy.onClick.RemoveListener(onViewGoods);
            //_goodsDummy = null;
        }

        public void SetImage(bool visiable)
        {
            if (_stateImage != null) _stateImage.Visible = visiable;
        }

        private void Refresh()
        {
            if (_labNum == null || _itemGrid == null) return;
            if (_data != null && XMLManager.item_commom.ContainsKey(_data.id))
            {
                _labNum.CurrentText.text = string.Concat("X", _data.num);
                _itemGrid.SetItemData(_data.id);
                Visible = true;
            }
            else
            {
                _labNum.CurrentText.text = "";
                Visible = false;
            }
        }

        //查看物品信息
        private void onViewGoods()
        {
            if (_data != null && XMLManager.item_commom.ContainsKey(_data.id))
            {
                ToolTipsManager.Instance.ShowItemTip(_data.id, PanelIdEnum.ItemToolTips, false);
            }
        }
    }
}
