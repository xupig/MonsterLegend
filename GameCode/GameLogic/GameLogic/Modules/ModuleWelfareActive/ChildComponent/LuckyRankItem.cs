﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.Data;
using GameMain.GlobalManager.SubSystem;
using GameMain.GlobalManager;
using Common.Utils;
using UnityEngine;
using GameData;

namespace ModuleWelfareActive
{
    public class LuckyRankItem : KList.KListItemBase
    {
        private StateText _descText;
        private StateImage _separateLineImg;
        private LuckyRankItemData _data;
        private static readonly float SEPARATE_LINE_OFFSET_Y = 4.63f;

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                LuckyRankItemData itemData = value as LuckyRankItemData;
                if (_data != null && _data.Id == itemData.Id)
                    return;
                _data = itemData;
                Refresh();
            }
        }
        protected override void Awake()
        {
            _descText = GetChildComponent<StateText>("Label_txtXinxi");
            _separateLineImg = GetChildComponent<StateImage>("Image_linebg03&zhaungshi");
        }

        public override void Show()
        {

        }

        public override void Hide()
        {

        }

        private void Refresh()
        {
            string content = system_info_helper.GetContent(_data.descId);
            if (SystemInfoUtil.IsContentWithParam(content))
            {
                content = SystemInfoUtil.GenerateContent(content, _data.playerName, _data.itemId, _data.itemNum);
                _descText.CurrentText.text = content;
                _descText.RecalculateCurrentStateHeight();
                SetSeparateLinePosition();
                RecalculateSize();
            }
            else
            {
                _descText.CurrentText.text = content;
            }
        }

        private void SetSeparateLinePosition()
        {
            RectTransform textRect = _descText.gameObject.GetComponent<RectTransform>();
            RectTransform lineImgRect = _separateLineImg.gameObject.GetComponent<RectTransform>();
            lineImgRect.anchoredPosition = new Vector2(lineImgRect.anchoredPosition.x, -(textRect.sizeDelta.y + SEPARATE_LINE_OFFSET_Y));
        }

        public override void Dispose()
        {
            
        }
    }
}
