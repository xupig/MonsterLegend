﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using UnityEngine;

namespace ModuleWelfareActive
{
    /// <summary>
    /// 单个邀请码展现--视图项
    /// </summary>
    public class InviteCodeItem : KList.KListItemBase
    {
        public invite baseData;
        private PbCompleteId _data;              //领取信息
        private StateText _txtBtn;               //按钮文本
        private KButton _btnReceive;             //领取按钮
        private ItemGrid _itemGridIcon;
        private StateText _txtReceiveTimes;      //领取次数标签
        private StateText _txtCondition;         //领取条件标签
        private StateText _txtShuLiang;          //数量
        private KParticle _wrapper;              //按钮特效包装类

        private string txt1 = MogoLanguageUtil.GetContent(6147019);   // "已领取 {0}/{1}次";
        private string txt3 =MogoLanguageUtil.GetContent(85018);      //领取
        private string txt4 =MogoLanguageUtil.GetContent(85019);      //已领取

        protected override void Awake()
        {
            _btnReceive =GetChildComponent<KButton>("Button_lingqu");
            _txtBtn = _btnReceive.GetChildComponent<StateText>("label");
            _itemGridIcon = GetChildComponent<ItemGrid>("Container_wupin");
            _wrapper = AddChildComponent<KParticle>("Button_lingqu/fx_ui_3_2_lingqu"); //按钮特效
            _wrapper.gameObject.transform.localScale = new Vector3(0.8f, 0.8f, 1);
            _wrapper.Stop();
            PlayBtnEffect();

            _txtReceiveTimes = GetChildComponent<StateText>("Label_txtYiLingQu");
            _txtCondition = GetChildComponent<StateText>("Label_txtTiaoJian");
            _txtShuLiang = GetChildComponent<StateText>("Label_txtShuliang");
            _btnReceive.onClick.AddListener(OnReceive);
            _txtBtn.ChangeAllStateText(txt3);
            _txtShuLiang.ChangeAllStateText("");
            _txtReceiveTimes.ChangeAllStateText("");
            _txtCondition.ChangeAllStateText("");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbCompleteId;
                Refresh();
            }
        }

        public override void Dispose()
        {
            _btnReceive.onClick.RemoveListener(OnReceive);
            _data = null;
            PlayBtnEffect();
            _wrapper = null;
        }

        /// <summary>
        /// 返回可领取状态
        /// </summary>
        /// <returns></returns>
        public bool hasRewardable()
        {
            if (_data == null || baseData == null) return false;               //找不到配置或没有领取数据，认为不可领取
            if (baseData.__type != 2)
            {//邀请码奖励
                if (_data.reward_num >= baseData.__time_limit) return false;   //超过领取次数，设置为不可领取
            }
            return _data.can_get == 1 ? true : false;                        //当次可以领取状态
        }

        //控制按钮特效播放
        private void PlayBtnEffect()
        {
            if (hasRewardable())
            {
                _btnReceive.interactable = true;
                if (_wrapper != null) _wrapper.Play(true);
            }
            else
            {
                _btnReceive.interactable = false;
                if (_wrapper != null) _wrapper.Stop();
            }
        }

        protected void Refresh()
        {
            if (_data != null && baseData != null)
            {
                Refresh((int)_data.reward_num);
            }
            else
            {
                if (baseData != null && baseData.__id > 0)
                {
                    Refresh(0);
                }
                else
                {//没关联到邀请奖励信息，隐藏
                    PlayBtnEffect();
                    Visible = false;
                }
            }
        }

        /// <summary>
        /// 刷新显示
        /// </summary>
        /// <param name="inviteId">invite.id</param>
        /// <param name="hasNum">已邀请好友数量</param>
        private void Refresh(int hasNum)
        {
            if (baseData.__time_limit > 0)
            {
                _txtReceiveTimes.CurrentText.text = string.Format(txt1, hasNum, baseData.__time_limit);
            }
            else
            {
                _txtReceiveTimes.CurrentText.text = "";
            }

            if (baseData.__type == 2)
            {//分享链接领取后，需要把领取按钮文字改为：已领取 
                _txtBtn.ChangeAllStateText(_data != null && _data.reward_num > 0 && _data.can_get != 1 ? txt4 : txt3);
                _txtCondition.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(baseData.__describe), baseData.__day_limit);
            }
            else 
            {
                _txtBtn.ChangeAllStateText(txt3);
                _txtCondition.CurrentText.text = MogoLanguageUtil.GetContent(baseData.__describe);
            }
            _itemGridIcon.SetItemData(baseData.__reward);
            PlayBtnEffect();
            Visible = true;
        }

        //点击--领取按钮
        private void OnReceive()
        {
            if (_data != null)
            {
                LoggerHelper.Debug(string.Format("邀请码奖励领取请求 id:{0}", _data.id));
                WelfareActiveManager.Instance.ReciveRewardReq((int)_data.id);
            }
        }
    }
}
