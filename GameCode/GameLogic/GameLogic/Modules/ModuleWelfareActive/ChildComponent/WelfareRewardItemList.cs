﻿using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleWelfareActive
{
    public class WelfareRewardItemList : KContainer
    {
        private List<Item> _itemList;
        protected override void Awake()
        {
            _itemList = new List<Item>();
            for (int i = 0; i < transform.childCount; i++)
            {
                Item item = AddChildComponent<Item>(string.Format("Container_item0{0}", i + 1));
                _itemList.Add(item);
            }
        }

        public void RefreshReward(int rewardId, bool haveGot)
        {
            List<BaseItemData> itemDataList = item_reward_helper.GetItemDataList(rewardId);
            RefreshReward(itemDataList, haveGot);
        }

        public void RefreshReward(List<BaseItemData> itemDataList, bool haveGot)
        {
            for (int i = 0; i < _itemList.Count; i++)
            {
                if (i < itemDataList.Count)
                {
                    _itemList[i].Visible = true;
                    _itemList[i].Refresh(itemDataList[i], haveGot);
                }
                else
                {
                    _itemList[i].Visible = false;
                }
            }
        }

        class Item : KContainer
        {
            private RewardItem _rewardItem;
            private StateImage _imageHaveGot;

            protected override void Awake()
            {
                _rewardItem = gameObject.AddComponent<RewardItem>();
                _imageHaveGot = GetChildComponent<StateImage>("Image_sharedyilingqu");
                _imageHaveGot.gameObject.AddComponent<RaycastComponent>();
            }

            public void Refresh(BaseItemData baseItemData, bool haveGot)
            {
                _rewardItem.Data = baseItemData;
                _imageHaveGot.Visible = haveGot;
            }
        }
    }
}
