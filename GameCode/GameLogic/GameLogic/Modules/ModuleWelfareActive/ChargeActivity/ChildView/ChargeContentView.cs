﻿using ACTSystem;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleWelfareActive
{
    public class ChargeContentView : KContainer
    {
        private WelfareRewardItemList _rewardItemList;
        private KButton _btnGetReward;
        private StateText _textDescription;
        private ModelComponent _model;
        private IconContainer _iconContainer;
        private KParticle _obtainParticle;

        private ChargeActivityItemData _currentChargeData;

        protected override void Awake()
        {
            _rewardItemList = AddChildComponent<WelfareRewardItemList>("Container_right/Container_rewardList");
            _btnGetReward = GetChildComponent<KButton>("Container_right/Button_chongzhi");
            _btnGetReward.Label.ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);
            _textDescription = GetChildComponent<StateText>("Container_right/Label_txtzidongfasong");
            _iconContainer = AddChildComponent<IconContainer>("Container_panelBg/Container_icon");
            _model = AddChildComponent<ModelComponent>("Container_model");
            _iconContainer = AddChildComponent<IconContainer>("Container_panelBg/Container_icon");
            _obtainParticle = AddChildComponent<KParticle>("Container_right/Button_chongzhi/fx_ui_3_2_lingqu");
            _obtainParticle.Stop();
        }

        private void AddListener()
        {
            _btnGetReward.onClick.AddListener(OnClickGetReward);
        }

        private void RemoveListener()
        {
            _btnGetReward.onClick.RemoveListener(OnClickGetReward);
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        public void Refresh(ChargeActivityItemData activityData)
        {
            _currentChargeData = activityData;
            RefreshDesc();
            RefreshRewardList();
            RefreshArtDesc();
            RefreshBtnState();
            RefreshModel();
        }

        private void RefreshArtDesc()
        {
            _iconContainer.SetIcon(_currentChargeData.Icon);
        }

        private void RefreshDesc()
        {
            _textDescription.CurrentText.text = _currentChargeData.Desc;
        }

        private void RefreshRewardList()
        {
            _rewardItemList.RefreshReward(_currentChargeData.RewardId, _currentChargeData.State == ActivityState.REWARDED);
        }

        private void RefreshBtnState()
        {
            _obtainParticle.Stop();
            switch (_currentChargeData.State)
            {
                case ActivityState.SHOW:
                    _btnGetReward.SetButtonActive();
                    _btnGetReward.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(114981));
                    break;
                case ActivityState.COMPLETE:
                    _btnGetReward.SetButtonActive();
                    _btnGetReward.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(114977));
                    _obtainParticle.Play(true);
                    break;
                case ActivityState.REWARDED:
                    _btnGetReward.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(114978));
                    _btnGetReward.SetButtonDisable();
                    break;
            }
        }

        private void OnClickGetReward()
        {
            switch (_currentChargeData.State)
            {
                case ActivityState.SHOW:
                    if (function_helper.IsFunctionOpen(FunctionId.charge))
                    {
                        UIManager.Instance.ShowPanel(PanelIdEnum.Charge);
                    }
                    else
                    {
                        MessageBox.Show(true, string.Empty, MogoLanguageUtil.GetContent(114982));
                    }
                    break;
                case ActivityState.COMPLETE:
                    ChargeActivityManager.Instance.RequestGetReward(_currentChargeData.ID);
                    break;
            }
        }

        private void RefreshModel()
        {
            _model.ClearModelGameObject();
            List<BaseItemData> rewardList = item_reward_helper.GetItemDataList(_currentChargeData.RewardId);
            switch (_currentChargeData.showModelType)
            {
                case BagItemType.Wing:
                    int wingId = GetWingId(rewardList);
                    _model.LoadModelWithWing(wingId);
                    break;
                case BagItemType.PetCard:
                    int petId = GetPetId(rewardList);
                    int petQuality = pet_helper.GetPetInitQuality(petId);
                    _model.LoadPetModel(petId, petQuality);
                    break;
                case BagItemType.Fashion:
                    int fashionId = GetFashionId(rewardList);
                    _model.LoadModelWithFashion(fashionId);
                    break;
                case BagItemType.Mount:
                    int mountId = GetMountId(rewardList);
                    _model.LoadMountModel(mountId, RotateMountModel);
                    break;
            }
        }

        private void RotateMountModel(ACTActor mountActor)
        {
            mountActor.gameObject.transform.localRotation = Quaternion.Euler(0, 135, 0);
        }

        private int GetWingId(List<BaseItemData> itemDataList)
        {
            for (int i = 0; i < itemDataList.Count; i++)
            {
                if (itemDataList[i].Type == BagItemType.Wing)
                {
                    return item_helper.GetWingId(itemDataList[i].Id);
                }
            }
            UnityEngine.Debug.LogError("@张敬 奖励配置表中ID为" + _currentChargeData.RewardId + "中缺少道具类型为翅膀["+(int)BagItemType.Wing+"]的道具奖励");
            return 0;
        }

        private int GetPetId(List<BaseItemData> itemDataList)
        {
            for (int i = 0; i < itemDataList.Count; i++)
            {
                if (itemDataList[i].Type == BagItemType.PetCard)
                {
                    return item_helper.GetPetId(itemDataList[i].Id);
                }
            }
            UnityEngine.Debug.LogError("@张敬 奖励配置表中ID为" + _currentChargeData.RewardId + "中缺少道具类型为魔灵整卡[" + (int)BagItemType.PetCard + "]的道具奖励");
            return 0;
        }

        private int GetFashionId(List<BaseItemData> itemDataList)
        {
            for (int i = 0; i < itemDataList.Count; i++)
            {
                if (itemDataList[i].Type == BagItemType.Fashion)
                {
                    return item_helper.GetFashionId(itemDataList[i].Id);
                }
            }
            UnityEngine.Debug.LogError("@张敬 奖励配置表中ID为" + _currentChargeData.RewardId + "中缺少道具类型为时装[" + (int)BagItemType.Fashion + "]的道具奖励");
            return 0;
        }

        private int GetMountId(List<BaseItemData> itemDataList)
        {
            for (int i = 0; i < itemDataList.Count; i++)
            {
                if (itemDataList[i].Type == BagItemType.Mount)
                {
                    return item_helper.GetMountId(itemDataList[i].Id);
                }
            }
            UnityEngine.Debug.LogError("@张敬 奖励配置表中ID为" + _currentChargeData.RewardId + "中缺少道具类型为时装[" + (int)BagItemType.Mount + "]的道具奖励");
            return 0;
        }
    }
}
