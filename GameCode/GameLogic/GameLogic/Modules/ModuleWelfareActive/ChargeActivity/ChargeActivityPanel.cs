﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System.Collections.Generic;

namespace ModuleWelfareActive
{
    public class ChargeActivityPanel : BasePanel
    {
        private CategoryList _categoryList;
        private ChargeContentView _chargeContentView;

        private int _currentIndex = 0;
        private List<ChargeActivityItemData> chargeActivityDataList;

        protected override void Awake()
        {
            _chargeContentView = AddChildComponent<ChargeContentView>("Container_shouchong");
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
        }

        public override void OnShow(object data)
        {
            _currentIndex = 0;
            InitChargeItemDataList();
            InitCategoryList();
            _chargeContentView.Refresh(chargeActivityDataList[_currentIndex]);
            AddEventListener();
        }

        private void Refresh()
        {
            InitChargeItemDataList();
            InitCategoryList();
            _chargeContentView.Refresh(chargeActivityDataList[_currentIndex]);
        }

        private void InitChargeItemDataList()
        {
            chargeActivityDataList = PlayerDataManager.Instance.ChargeActivityData.GetShowActivityNameList();
        }

        private void InitCategoryList()
        {
            List<CategoryItemData> dataList = new List<CategoryItemData>();
            for (int i = 0; i < chargeActivityDataList.Count; i++)
            {
                dataList.Add(CategoryItemData.GetCategoryItemData(chargeActivityDataList[i].Name));
            }
            _categoryList.RemoveAll();
            _categoryList.SetDataList<CategoryListItem>(dataList);
            for (int i = 0; i < chargeActivityDataList.Count; i++)
            {
                (_categoryList.ItemList[i] as CategoryListItem).SetPoint(chargeActivityDataList[i].State == ActivityState.COMPLETE);
            }
            SetCategoryIndex();
        }

        private void SetCategoryIndex()
        {
            _currentIndex = -1;
            for (int i = 0; i < chargeActivityDataList.Count; i++)
            {
                if (chargeActivityDataList[i].State == ActivityState.COMPLETE)
                {
                    _currentIndex = i;
                    _categoryList.SelectedIndex = _currentIndex;
                    return;
                }
            }
            for (int i = 0; i < chargeActivityDataList.Count; i++)
            {
                if (chargeActivityDataList[i].State == ActivityState.SHOW)
                {
                    _currentIndex = i;
                    _categoryList.SelectedIndex = _currentIndex;
                    return;
                }
            }
            _currentIndex = chargeActivityDataList.Count - 1;
            _categoryList.SelectedIndex = _currentIndex;
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.ChargeActivity; }
        }

        private void AddEventListener()
        {
            _categoryList.onSelectedIndexChanged.AddListener(OnSelectedIndexChange);
            EventDispatcher.AddEventListener(ChargeActivityEvents.RefreshChargeContent, Refresh);
        }

        private void RemoveEventListener()
        {
            _categoryList.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChange);
            EventDispatcher.RemoveEventListener(ChargeActivityEvents.RefreshChargeContent, Refresh);
        }

        private void OnSelectedIndexChange(CategoryList categoryList, int index)
        {
            if (_currentIndex == index)
            {
                return;
            }
            _currentIndex = index;
            _chargeContentView.Refresh(chargeActivityDataList[index]);
        }

    }
}
