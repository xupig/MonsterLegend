﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using UnityEngine;

namespace ModuleWelfareActive
{
    public class TurntablePointerRotateAnim : KContainer
    {
        public delegate void PlayEndCallback(int rewardIndex);
        private GameObject _pointerGo;
        private float _fastSpeed = 720f;
        private float _fastDuration = 3f;
        private float _slowSpeed = 360f;
        private float _acceleration = -1f;
        private float _curTime = 0f;
        private float _curReducedSpeed = 0f;
        private float _destAngle = 0f;
        private int _curStage = 0;
        private float _curDuration = 0;
        private float _lastReducedSpeed = 0;
        private bool _isStartPlaying = false;
        private bool _isInitEnterNextStage = false;
        private int _rewardIndex = 0;
        private PlayEndCallback _callback;
        public bool IsPlaying
        {
            get { return _isStartPlaying; }
        }

        protected override void Awake()
        {
            _pointerGo = transform.gameObject;
        }

        public void RegisterCallback(PlayEndCallback callback)
        {
            _callback = callback;
        }

        public void StartPlay(int rewardIndex)
        {
            _isStartPlaying = true;
            _rewardIndex = rewardIndex;
            _curTime = 0f;
            _curReducedSpeed = _fastSpeed;
            _destAngle = CalculateDestAngle(rewardIndex);
            _curStage = 0;
            _curDuration = 0;
            _lastReducedSpeed = 0f;
            _isInitEnterNextStage = false;
        }

        public float CalculateDestAngle(int rewardIndex)
        {
            return 360f - (22.5f + rewardIndex * 45f);
        }

        public void StopPlay()
        {
            _isStartPlaying = false;
        }

        public void PlayToDestImmediately(int rewardIndex)
        {
            _destAngle = CalculateDestAngle(rewardIndex);
            _isStartPlaying = false;
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, _destAngle);
            if (_callback != null)
            {
                _callback(rewardIndex);
            }
        }

        private void Update()
        {
            if (!_isStartPlaying)
                return;
            if (_curStage == 0)
            {
                _curTime += Time.deltaTime;
                if (!_isInitEnterNextStage)
                {
                    _curDuration = _fastDuration;
                    _isInitEnterNextStage = true;
                }
                if (_curTime <= _curDuration)
                {
                    transform.Rotate(0, 0, -_fastSpeed * Time.deltaTime);
                }
                else
                {
                    _curStage++;
                    _curTime = 0;
                    _curDuration = 0f;
                    _isInitEnterNextStage = false;
                }
            }
            else if (_curStage == 1)
            {
                _curTime += Time.deltaTime;
                if (!_isInitEnterNextStage)
                {
                    float deltaAngle = CalculateDeltaAngle(transform.localEulerAngles.z, _destAngle);
                    float destDeltaAngle = deltaAngle + 360f;
                    _curDuration = (destDeltaAngle - 180f) / _slowSpeed;
                    _isInitEnterNextStage = true;
                }
                if (_curTime <= _curDuration)
                {
                    transform.Rotate(0, 0, -_slowSpeed * Time.deltaTime);
                }
                else
                {
                    float destAngle = 0f;
                    if (_destAngle >= 180f)
                    {
                        destAngle = _destAngle - 180f;
                    }
                    else
                    {
                        destAngle = 360f - (180f - _destAngle);
                    }
                    float deltaAngle = CalculateDeltaAngle(transform.localEulerAngles.z, destAngle);
                    if (deltaAngle <= _slowSpeed * Time.deltaTime)
                    {
                        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, destAngle);
                    }
                    _curStage++;
                    _curTime = 0;
                    _curDuration = 0f;
                    _isInitEnterNextStage = false;
                }
            }
            else if (_curStage == 2)
            {
                _curTime += Time.deltaTime;
                if (!_isInitEnterNextStage)
                {
                    //根据匀加速直线公式
                    float angle = 180f - 67f;
                    _curDuration = angle / (_slowSpeed + (50f - _slowSpeed) / 2.0f);
                    _acceleration = 30f -_slowSpeed / _curDuration;
                    _curReducedSpeed = _slowSpeed;
                    _isInitEnterNextStage = true;
                }
                if (_curReducedSpeed >= 50f)
                {
                    _curReducedSpeed = _curReducedSpeed + _acceleration * Time.deltaTime;
                }
                float deltaAngle = CalculateDeltaAngle(transform.localEulerAngles.z, _destAngle);
                if (deltaAngle <= 67f)
                {
                    _lastReducedSpeed = _curReducedSpeed;
                    _curTime = 0;
                    _curStage++;
                    _isInitEnterNextStage = false;
                }
                else
                {
                    transform.Rotate(0, 0, -_curReducedSpeed * Time.deltaTime);
                }
            }
            else if (_curStage == 3)
            {
                float deltaAngle = CalculateDeltaAngle(transform.localEulerAngles.z, _destAngle);
                if (!_isInitEnterNextStage)
                {
                    _curReducedSpeed = _lastReducedSpeed;
                    float duration = deltaAngle / (_curReducedSpeed + (-_curReducedSpeed) / 2.0f);
                    _acceleration = -_curReducedSpeed / duration;
                    _isInitEnterNextStage = true;
                }
                if (_curReducedSpeed > 0f)
                {
                    _curReducedSpeed = _curReducedSpeed + _acceleration * Time.deltaTime;
                }
                else
                {
                    _curReducedSpeed = 0f;
                    _curStage++;
                }
                if (deltaAngle <= _curReducedSpeed * Time.deltaTime)
                {
                    transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, _destAngle);
                    _curReducedSpeed = 0;
                    if (_curStage == 3)
                    {
                        _curStage++;
                    }
                }
                if (_curStage > 3)
                {
                    _isStartPlaying = false;
                }
                transform.Rotate(0, 0, -_curReducedSpeed * Time.deltaTime);
            }
            if (!_isStartPlaying)
            {
                if (_callback != null)
                {
                    _callback(_rewardIndex);
                }
            }
        }

        private float CalculateDeltaAngle(float curAngle, float destAngle)
        {
            float deltaAngle = 0f;
            if (destAngle > curAngle)
            {
                deltaAngle = curAngle + (360f - destAngle);
            }
            else if (destAngle < curAngle)
            {
                deltaAngle = curAngle - destAngle;
            }
            return deltaAngle;
        }
    }
}
