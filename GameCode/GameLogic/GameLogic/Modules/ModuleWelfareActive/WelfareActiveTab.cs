﻿using Common.Base;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleWelfareActive
{
    public class WelfareActiveTab : BasePanel
    {
        private delegate bool CheckDelegate();

        private class TabConfigData
        {
            public Tab Tab;
            public string Title;
            public CheckDelegate CheckFun;
            public Func<bool> HaveRewardFun;

            public TabConfigData(Tab tab, string title, Func<bool> haveRewardFun, CheckDelegate checkFun = null)
            {
                this.Tab = tab;
                this.Title = title;
                this.CheckFun = checkFun;
                this.HaveRewardFun = haveRewardFun;
            }
        }

        private enum Tab
        {
            Charge,
            Welfare,
        }

        private readonly List<TabConfigData> _configList;
        private List<Tab> _currentToggleIndexList = new List<Tab>();
        private int _currentIndex;
        private int _secondTab;

        private KToggleGroup _toggleGroup;
        private List<KToggle> _sourceToggleList;

        public WelfareActiveTab()
        {
            _configList = new List<TabConfigData>() 
            { 
                new TabConfigData(Tab.Charge, MogoLanguageUtil.GetContent(114971), ChargeTabHaveReward, ChargeTabCanShow), 
                new TabConfigData(Tab.Welfare, MogoLanguageUtil.GetContent(6147001), WelfareTabHaveReward), 
            };
        }

        private bool OpenServerTabHaveReward()
        {
            if (PlayerDataManager.Instance.OpenServerData.HaveReward())
            {
                return true;
            }
            return false;
        }

        private bool ChargeTabHaveReward()
        {
            if (PlayerDataManager.Instance.ChargeActivityData.CanGetReward())
            {
                return true;
            }
            return false;
        }

        private bool WelfareTabHaveReward()
        {
            if (WelfareActiveManager.Instance.MobileBindable())
            {
                return true;
            }
            return false;
        }

        private bool OpenServerTabCanShow()
        {
            if (function_helper.IsFunctionOpen(FunctionId.openServerActivity) && !PlayerDataManager.Instance.OpenServerData.IsEnd())
            {
                return true;
            }
            return false;
        }

        private bool ChargeTabCanShow()
        {
            if (function_helper.IsFunctionOpen(FunctionId.chargeActivity) && PlayerAvatar.Player.accumulated_charge > 0)
            {
                return true;
            }
            return false;
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            _sourceToggleList = _toggleGroup.GetToggleList();
            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
        }

        private void RemoveListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
        }

        private void OnSelectedIndexChanged(KToggleGroup toggleGroup, int index)
        {
            _currentIndex = index;
            RefreshTabContent();
        }

        private void CloseAll()
        {
            PanelIdEnum.WelfareActive.Close();
            PanelIdEnum.ChargeActivity.Close();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.WelfareActiveTab; }
        }

        public override void OnShow(object data)
        {
            AddListenerWhenShow();
            CheckToggleGroup();
            ParseData(data);
            RefreshToggleGroup();
            RefreshTabContent();
        }

        private void AddListenerWhenShow()
        {
            EventDispatcher.AddEventListener(OpenServerEvents.Update, RefreshToggleGroupGreenPoint);
            EventDispatcher.AddEventListener(ChargeActivityEvents.RefreshChargeContent, RefreshToggleGroupGreenPoint);
        }

        private void RemoveListenerWhenClose()
        {
            EventDispatcher.RemoveEventListener(OpenServerEvents.Update, RefreshToggleGroupGreenPoint);
            EventDispatcher.RemoveEventListener(ChargeActivityEvents.RefreshChargeContent, RefreshToggleGroupGreenPoint);
        }

        private void CheckToggleGroup()
        {
            _currentToggleIndexList.Clear();
            for (int i = 0; i < _configList.Count; i++)
            {
                TabConfigData configData = _configList[i];
                CheckDelegate checkFun = configData.CheckFun;
                if (checkFun == null || checkFun.Invoke() == true)
                {
                    _currentToggleIndexList.Add(configData.Tab);
                }
            }
        }

        private void ParseData(object data)
        {
            _secondTab = -1;
            if (data != null && data is int[])
            {
                int[] array = data as int[];
                Tab tab = (Tab)(array[0] - 1);
                for (int i = 0; i < _currentToggleIndexList.Count; i++)
                {
                    if (tab == _currentToggleIndexList[i])
                    {
                        _currentIndex = i;
                        break;
                    }
                }
                if (array.Length > 1)
                {
                    _secondTab = array[1];
                }
            }
            else
            {
                _currentIndex = 0;
                for (int i = 0; i < _currentToggleIndexList.Count; i++)
                {
                    if (_currentToggleIndexList[i] == Tab.Charge)
                    {
                        _currentIndex = i;
                    }
                }
            }
        }

        private void RefreshToggleGroup()
        {
            _toggleGroup.SelectIndex = _currentIndex;
            UnregisterAllToggleGroup();
            RegisterCurrentToggleGroup();
            RefreshToggleGroupGreenPoint();
            MogoLayoutUtils.RecalculateSize(_toggleGroup.gameObject);
            MogoLayoutUtils.SetCenter(_toggleGroup.gameObject);
        }

        private void UnregisterAllToggleGroup()
        {
            for (int i = 0; i < _sourceToggleList.Count; i++)
            {
                _toggleGroup.UnregisterToggle(_sourceToggleList[i]);
            }
        }

        private void RegisterCurrentToggleGroup()
        {
            for (int i = 0; i < _sourceToggleList.Count; i++)
            {
                KToggle toggle = _sourceToggleList[i];
                if (i < _currentToggleIndexList.Count)
                {
                    toggle.Visible = true;
                    _toggleGroup.RegisterToggle(toggle);
                    TabConfigData configData = GetTabConfigData(_currentToggleIndexList[i]);
                    toggle.SetLabel(configData.Title);
                }
                else
                {
                    toggle.Visible = false;
                }
            }
        }

        private void RefreshToggleGroupGreenPoint()
        {
            for (int i = 0; i < _sourceToggleList.Count; i++)
            {
                KToggle toggle = _sourceToggleList[i];
                if (i < _currentToggleIndexList.Count)
                {
                    TabConfigData configData = GetTabConfigData(_currentToggleIndexList[i]);
                    toggle.SetGreenPoint(configData.HaveRewardFun());
                }
            }
        }

        private TabConfigData GetTabConfigData(Tab tab)
        {
            for (int i = 0; i < _configList.Count; i++)
            {
                if (_configList[i].Tab == tab)
                {
                    return _configList[i];
                }
            }
            return null;
        }

        private void RefreshTabContent()
        {
            CloseAll();
            Tab tab = _currentToggleIndexList[_currentIndex];
            switch (tab)
            {
                case Tab.Charge:
                    PanelIdEnum.ChargeActivity.Show();
                    break;
                case Tab.Welfare:
                    PanelIdEnum.WelfareActive.Show(_secondTab);
                    break;
            }
        }

        public override void OnClose()
        {
            RemoveListenerWhenClose();
            CloseAll();
        }
    }
}
