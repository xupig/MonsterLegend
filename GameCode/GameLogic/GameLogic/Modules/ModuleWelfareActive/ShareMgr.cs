﻿using UnityEngine;
using System;
using System.IO;
using GameLoader.PlatformSdk;
using GameLoader.Utils.Timer;
using ModuleCommonUI;
using Common.Base;
using GameLoader.Utils;
using MogoEngine;
using GameMain.Entities;
using Common;
using GameLoader.Config;
using GameData;
using GameMain.GlobalManager.SubSystem;
using Common.Utils;

namespace ModuleWelfareActive
{
    /// <summary>
    /// 微信分享接口
    /// </summary>
    public class ShareMgr
    {
        private readonly string SharePath = "Share.png";
        public static readonly ShareMgr Instance = new ShareMgr();

        public void ShareLink(string url)
        {
            string title = XMLManager.chinese[85014].__content;
            string desc = XMLManager.chinese[85015].__content;
            PlatformSdkMgr.Instance.ShareLink(title, desc, url);  

			#if UNITY_IPHONE && !UNITY_EDITOR && USE_WX_SHARE	    
			//IOSPlugins.ShareLink(title,desc,url,"qjphs_icon.png");
			IOSPlugins.ShareTextToTimeLineViaWeiXin(desc, url);
            #endif
        }

        public void ShareLink(string title, string desc, string url)
        {
            PlatformSdkMgr.Instance.ShareLink(title, desc, url);
        }

        public void Share()
        {
            Share(string.Empty);
        }

        public void Share(string desc)
        {
            var path = String.Concat(Application.persistentDataPath, '/', SharePath);
            if (!Directory.Exists(Path.GetDirectoryName(path)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(path));
            }
            Application.CaptureScreenshot(SharePath);
            if (Application.platform == RuntimePlatform.Android)
            {
                TimerHeap.AddTimer(1000, 0, () =>
                {
                    PlatformSdkMgr.Instance.ShareImage(path);
                });
            }
            else if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                TimerHeap.AddTimer(1000, 0, () => { CallWX(desc); });
            }
        }

        private void CallWX(string desc)
        {
#if UNITY_IPHONE && !UNITY_EDITOR && USE_WX_SHARE
                string sharePath = string.Concat(Application.persistentDataPath, "/", SharePath);
                if (File.Exists(sharePath))
                {
                    IOSPlugins.ShareViaWeiXin(sharePath);
                }
                else
                {
                    TimerHeap.AddTimer(1000, 0, () => { CallWX(desc); });
                }
#endif
        }

        /// <summary>
        /// 微信分享囘調
        /// </summary>
        public void OnWxShareCallback(bool result, string type)
        { 
            if(result)
            {//分享成功
                //m_myself.RpcCall("FriendsShareReq");
                LoggerHelper.Info(string.Format("[ShareMgr-OnWxShareCallback()] 分享成功，并发送分享成功请求! result:{0},type:{1}", result, type));
                WelfareActiveManager.Instance.ShareLinkOkReq();                                               //链接分享成功上送
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(99206), PanelIdEnum.MainUIField);  // "分享成功"
            }
            else
            {//分享失敗
                string tip = MogoLanguageUtil.GetContent(6148003); // "分享失败";
                switch (type)
                {
                    case "OnWxNotFound": tip = MogoLanguageUtil.GetContent(6148004); break; //"微信号未找到"
                    case "OnWxFail": tip = MogoLanguageUtil.GetContent(6148003); break;     //"分享失败"
                    case "OnWxCancel": tip = MogoLanguageUtil.GetContent(6148005); break;   //"已取消分享"
                    case "OnWxDenied": tip = MogoLanguageUtil.GetContent(6148006); break;   //"分享被拒绝"
                    case "OnWxUnknow": tip = MogoLanguageUtil.GetContent(6148007); break;                     //未知操作
                    case "onWxNotInstalledOrNotSupport": tip = MogoLanguageUtil.GetContent(6148008); break;   //"未安装微信或版本过低";
                    default: break;
                }
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, tip, PanelIdEnum.MainUIField);
            }
        }
    }
}
