﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using UnityEngine;
using GameMain.GlobalManager;
using UnityEngine.Events;
using Common.Utils;

namespace ModuleWelfareActive
{
    /// <summary>
    /// 福利活动系统--视图类
    /// </summary>
    public class WelfareActivePanel : BasePanel
    {
        private enum WelfareTab
        {
            InviteCode = 0,
            BindMobileCode = 1,
            ActivateCode = 2,
            LuckyTurntable = 3
        }

        private WelfareTab _curTab;
        private CategoryList _categoryList;                          //左边分页列表
        private TabInviteCodeView _tabInviteCodeView;                //邀请码Tab界面
        private TabBindMobileView _tabBindMobileView;                //手机绑定Tab界面
        private TabActivateCodeView _tabActivateCodeView;            //激活码Tab界面
        private List<CategoryItemData> _categoryDataList;            //左边列表
        private StateImage _turntableAnimMaskImg;

        private string txt1 = MogoLanguageUtil.GetContent(84600);   //"好友邀请";
        private string txt2 = MogoLanguageUtil.GetContent(103028);  //"绑定手机";
        private string txt3 = MogoLanguageUtil.GetContent(6147014); //"激活码";
        private string txt4 = MogoLanguageUtil.GetContent(6147015); //"幸运转盘";

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.WelfareActive; }
        }

        protected override void Awake()
        {
            _curTab = WelfareTab.InviteCode;
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            _tabInviteCodeView = GetChild("Container_invitecode").AddComponent<TabInviteCodeView>();
            _tabBindMobileView = GetChild("Container_shoujibangding").AddComponent<TabBindMobileView>();
            _tabActivateCodeView = GetChild("Container_jihuoma").AddComponent<TabActivateCodeView>();
            _turntableAnimMaskImg = GetChildComponent<StateImage>("Image_turntableAnimMask");
            RectTransform maskRect = _turntableAnimMaskImg.GetComponent<RectTransform>();
            maskRect.sizeDelta = new Vector2(UIManager.CANVAS_WIDTH, UIManager.CANVAS_HEIGHT);
            InitCategorySize();
        }

        private void HideAll()
        {
            _tabInviteCodeView.Close();
            _tabBindMobileView.Close();
            _tabActivateCodeView.Close();
            PanelIdEnum.LuckyTurntable.Close();
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void SetData(object data)
        {
            if(data is int)
            {
                int tab = (int)data;
                if(tab != -1)
                {
                    _curTab = (WelfareTab)(tab - 1);
                }
            }
            ChangeTab(_curTab);

            //验证码提示
            bool isVerify = WelfareActiveManager.Instance.MobileBindable();
            OnRefresh1RewardTip(isVerify);
        }

        private void ChangeTab(WelfareTab tab)
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.LuckyTurntable);
            _categoryList.SelectedIndex = (int)tab;
            switch(tab)
            {
                case WelfareTab.InviteCode: 
                    _tabInviteCodeView.Show();
                    _tabBindMobileView.Close();
                    _tabActivateCodeView.Close();
                    PanelIdEnum.LuckyTurntable.Close();
                     break;
                case WelfareTab.BindMobileCode:
                     _tabInviteCodeView.Close();
                    _tabBindMobileView.Show();
                    _tabActivateCodeView.Close();
                    PanelIdEnum.LuckyTurntable.Close();
                    break;
                case WelfareTab.ActivateCode:
                    _tabInviteCodeView.Close();
                    _tabBindMobileView.Close();
                    _tabActivateCodeView.Show();
                    PanelIdEnum.LuckyTurntable.Close();
                    break;
                case WelfareTab.LuckyTurntable:
                    _tabInviteCodeView.Close();
                    _tabBindMobileView.Close();
                    _tabActivateCodeView.Close();
                    PanelIdEnum.LuckyTurntable.Show();
                    break;
            }
        }

        public override void OnClose()
        {
            _curTab = WelfareTab.InviteCode;
            _categoryList.SelectedIndex = 0;
            RemoveEventListener();
            UIManager.Instance.ClosePanel(PanelIdEnum.LuckyTurntable);
        }

        //设置左边Tab容量
        private void InitCategorySize()
        {
            _categoryDataList = new List<CategoryItemData>();
            CategoryItemData itemData1 = new CategoryItemData();
            itemData1.name = txt1;
            _categoryDataList.Add(itemData1);

            CategoryItemData itemData2 = new CategoryItemData();
            itemData2.name = txt2;
            _categoryDataList.Add(itemData2);

            CategoryItemData itemData3 = new CategoryItemData();
            itemData3.name = txt3;
            _categoryDataList.Add(itemData3);

            CategoryItemData itemData4 = new CategoryItemData();
            itemData4.name = txt4;
            _categoryDataList.Add(itemData4);

            _categoryList.RemoveAll();
            _categoryList.SetDataList<CategoryListItem>(_categoryDataList);
            _categoryList.SelectedIndex = 0;
        }

        private void AddEventListener()
        {
            _categoryList.onSelectedIndexChanged.AddListener(OnCategoryChanged);
            EventDispatcher.AddEventListener<bool>(WelfareActiveEvent.C_REFRESH0_REWARD_TIP, OnRefresh0RewardTip);
            EventDispatcher.AddEventListener<bool>(WelfareActiveEvent.C_REFRESH1_REWARD_TIP, OnRefresh1RewardTip);
            
        }

        private void RemoveEventListener()
        {
            _categoryList.onSelectedIndexChanged.RemoveListener(OnCategoryChanged);
            EventDispatcher.RemoveEventListener<bool>(WelfareActiveEvent.C_REFRESH0_REWARD_TIP, OnRefresh0RewardTip);
            EventDispatcher.RemoveEventListener<bool>(WelfareActiveEvent.C_REFRESH1_REWARD_TIP, OnRefresh1RewardTip);
        }

        //====================  控件响应处理  ========================//
        /// <summary>
        /// 左边Tab切换
        /// </summary>
        /// <param name="toggleGroup"></param>
        /// <param name="index"></param>
        private void OnCategoryChanged(CategoryList toggleGroup, int index)
        {
            ChangeTab((WelfareTab)index);
        }

        /// <summary>
        /// 刷新奖励领取状态
        /// </summary>
        /// <param name="result"></param>
        private void OnRefresh0RewardTip(bool result)
        {
            LoggerHelper.Debug("好友领取状态：" + result);
            RefreshRewardTip(0, result);
        }

        //刷新手机绑定奖励提示
        private void OnRefresh1RewardTip(bool result)
        {
            LoggerHelper.Debug("手机绑定领取状态：" + result);
            RefreshRewardTip(1, result);
        }

        //刷新指定位置的奖励提示图标
        private void RefreshRewardTip(int index, bool result)
        {
            CategoryListItem item = (CategoryListItem)_categoryList[index];
            GameObject checkmark = item.gameObject.transform.FindChild("Button_checkmark/xiaodian").gameObject;
            GameObject back = item.gameObject.transform.FindChild("Button_back/xiaodian").gameObject;
            checkmark.SetActive(result);
            back.SetActive(result);
        }
        //============================================================//
    }
}
