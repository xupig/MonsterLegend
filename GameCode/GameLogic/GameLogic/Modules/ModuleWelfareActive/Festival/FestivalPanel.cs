﻿using Common.Base;
using Common.Events;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleWelfareActive
{
    public class FestivalPanel:BasePanel
    {
        private KToggleGroup _toggleGroup;
        private List<int> tabList;

        protected override void Awake()
        {
            base.Awake();
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            tabList = function_helper.GetFunctionTabList((int)FunctionId.Festival);
            InitToggleName();
            SetTabList((int)FunctionId.Festival, _toggleGroup);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Festival; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            RefreshTabPoint();
            if (data != null)
            {
                if (data is int)
                {
                    _toggleGroup.SelectIndex = (int)data - 1;
                    OnSelectedIndexChanged(_toggleGroup, (int)data - 1);
                }
            }
            SelectDefaultOpenFestival();
        }

        private void AddEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            EventDispatcher.AddEventListener<int>(FunctionEvents.UPDATE_FUNCTION, CheckFestivalState);
            EventDispatcher.AddEventListener(FunctionEvents.UPDATE_NOTICE, RefreshTabPoint);
        }

        private void RemoveEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            EventDispatcher.RemoveEventListener<int>(FunctionEvents.UPDATE_FUNCTION, CheckFestivalState);
            EventDispatcher.RemoveEventListener(FunctionEvents.UPDATE_NOTICE, RefreshTabPoint);
        }

        private void OnSelectedIndexChanged(KToggleGroup toggleGroup, int index)
        {
            function_helper.Follow(tabList[index], null);
        }

        private void RefreshTabPoint()
        {
            List<KToggle> toggleList = _toggleGroup.GetToggleList();
            for (int i = 0; i < tabList.Count; i++)
            {
                toggleList[i].SetGreenPoint(PlayerDataManager.Instance.FunctionData.IsFunctionPoint((FunctionId)tabList[i]));
            }
        }

        private void CheckFestivalState(int functionId)
        {
            if (functionId == (int)FunctionId.Festival)
            {
                if (PlayerDataManager.Instance.FunctionData.IsFunctionOpen(FunctionId.Festival) == true)
                {
                    SelectDefaultOpenFestival();
                    CheckTabList();
                }
                else
                {
                    ClosePanel();
                }
            }
        }

        private void InitToggleName()
        {
            List<KToggle> toggleList = _toggleGroup.GetToggleList();
            for (int i = 0; i < tabList.Count; i++)
            {
                toggleList[i].SetLabel(function_helper.GetName(tabList[i]));
            }
        }

        protected override void ClosePanel()
        {
            base.ClosePanel();
            for (int i = 0; i < tabList.Count; i++)
            {
                function func = function_helper.GetFunction(tabList[i]);
                if(func.__follow.ContainsKey("1"))
                {
                    PanelIdEnum panelId = view_helper.GetViewPanelId(int.Parse(func.__follow["1"][0]));
                    UIManager.Instance.ClosePanel(panelId);
                }
            }
        }

        private void SelectDefaultOpenFestival()
        {
            ///找到第一个开启的功能
            for (int i = 0; i < tabList.Count; i++)
            {
                if (PlayerDataManager.Instance.FunctionData.IsFunctionOpen(tabList[i]))
                {
                    OnSelectedIndexChanged(_toggleGroup, i);
                    return;
                }
            }
        }
    }
}
