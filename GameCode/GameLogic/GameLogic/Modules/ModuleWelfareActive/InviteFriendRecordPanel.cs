﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;

namespace ModuleWelfareActive
{
    /// <summary>
    /// 邀请好友记录列表
    /// </summary>
    public class InviteFriendRecordPanel : BasePanel
    {
        private StateText _txtTishi;
        private KScrollView _kScrollView;
        private KPageableList _kpageableList;
        private List<InviteFriendRecordData> list;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.InviteFriendRecord; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_biaoti/Button_close");
            _txtTishi = GetChildComponent<StateText>("Label_txtTishi");
            _kScrollView = GetChildComponent<KScrollView>("ScrollView_xuanzegonghui");
            _kpageableList = _kScrollView.GetChildComponent<KPageableList>("mask/content");
            _txtTishi.ChangeAllStateText(MogoLanguageUtil.GetContent(85021));

            _kpageableList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _kpageableList.SetGap(0, 5);
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            WelfareActiveManager.Instance.QueryInviteRecordList();   //查询邀请记录
        }

        public override void OnClose()
        {
            RemoveEventListener();
            if (list != null) list.Clear();
            list = null;
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<ulong>(FriendEvents.ADD_FRIEND, OnAddFriendRefresh);  //添加好友成功刷新，邀请记录列表
            EventDispatcher.AddEventListener<PbInviteRecordList>(WelfareActiveEvent.C_REFRESH_INVITE_RECORD_LIST, OnRefresh);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<ulong>(FriendEvents.ADD_FRIEND, OnAddFriendRefresh);
            EventDispatcher.RemoveEventListener<PbInviteRecordList>(WelfareActiveEvent.C_REFRESH_INVITE_RECORD_LIST, OnRefresh);
        }

        private void OnRefresh(PbInviteRecordList data)
        { 
            list = new List<InviteFriendRecordData>();
            if (data != null)
            {
                if (data.inviter != null) list.Add(new InviteFriendRecordData(data.inviter,false));
                if (data.invitees != null && data.invitees.Count > 0)
                {
                    foreach (PbInviteRecord item in data.invitees)
                    {
                        if (item != null) list.Add(new InviteFriendRecordData(item, true));
                    }
                }
            }
            OnAddFriendRefresh(0);
        }

        //添加好友成功后，刷新邀请记录列表
        private void OnAddFriendRefresh(ulong dbid)
        {
            if (!Visible || list == null) return;
            _kpageableList.RemoveAll();
            _kpageableList.SetDataList<InviteFriendRecordItem>(list);
            _kpageableList.RecalculateSize();
            if (list.Count > 0)
            {//有邀请记录
                _kScrollView.Visible = true;
                _txtTishi.Visible = false;
            }
            else
            {//没邀请记录
                _kScrollView.Visible = false;
                _txtTishi.Visible = true;
            }
        }
    }
}
