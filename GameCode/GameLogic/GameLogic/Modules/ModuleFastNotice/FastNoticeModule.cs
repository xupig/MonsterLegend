﻿#region 模块信息
/*==========================================
// 文件名：FastNoticeManager
// 命名空间: ModuleCommonUI.FastNotice
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/29 20:05:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Events;
using GameLoader.Utils;
using MogoEngine.Events;
using UnityEngine;
using GameMain.GlobalManager.SubSystem;

namespace ModuleFastNotice
{
    public class FastNoticeModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
        }

    }
}
