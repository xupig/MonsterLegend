﻿#region 模块信息
/*==========================================
// 文件名：FastNoticeItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.FastNoticeEntry
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/29 16:29:06
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Game.UI;
using Game.UI.UIComponent;

using Common.Base;
using Common.Events;
using MogoEngine.Events;
using GameMain.GlobalManager.SubSystem;
using GameMain.GlobalManager;
using GameData;
using Common.ServerConfig;
using Common.Utils;
using GameLoader.Utils;

namespace ModuleFastNotice
{
    public class FastNoticeItem : KList.KListItemBase
    {
        private FastNoticeDataWrapper _data;

        private StateText _txtCount;
        private StateText _txtContent;

        protected override void Awake()
        {
            _txtCount = GetChildComponent<StateText>("Label_txtCount");
            _txtCount.CurrentText.text = string.Empty;
            _txtContent = GetChildComponent<StateText>("Label_content");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as FastNoticeDataWrapper;
                Refresh();
            }
        }

        private void Refresh()
        {
            _txtCount.CurrentText.text = _data.count.ToString();
            _txtContent.CurrentText.text = _data.lastData.Content;
        }

        public override void Dispose()
        {
            _data = null;
        }
    }
}
