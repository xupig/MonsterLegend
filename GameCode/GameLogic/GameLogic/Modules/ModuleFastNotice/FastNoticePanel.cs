﻿#region 模块信息
/*==========================================
// 模块名：FloatTips
// 命名空间: ModuleCommonUI
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/26
// 描述说明：快速通知入口
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using UnityEngine.UI;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;
using Common.Data;
using GameData;
using Common;
using Common.Utils;
using MogoEngine.Mgrs;
using GameMain.Entities;
using Common.ClientConfig;
using GameLoader.Utils.Timer;
using Common.ExtendComponent;
using ModuleMainUI;
using UnityEngine.EventSystems;

namespace ModuleFastNotice
{

    public class FastNoticeView : KContainer
    {
        private Dictionary<PanelIdEnum, FastNoticeDataWrapper> _dataWrapperDict = new Dictionary<PanelIdEnum, FastNoticeDataWrapper>();
        private int _totalCount;
        private StateText _txtCount;
        private KButton _openBtn;
        private bool _isListOpened = false;
        private KList _list;
        private RectTransform _listRect;
        private Vector2 _pos;
        private Vector2 _size;
        private RectTransform _speakersRect;
        private RectTransform _noticeTipRect; //通知小弹窗
        private uint timerId = TimerHeap.INVALID_ID;
        private Vector2 _noticeTipPosition;
        private KParticle _rotateParticle;
        private StateText _txtNotice;
        public bool HasNotice()
        {
            return _dataWrapperDict!=null && _dataWrapperDict.Count > 0;
        }

        protected override void Awake()
        {
            _txtNotice = GetChildComponent<StateText>("Container_tishi/Label_txtXinyaoqing");
            _txtNotice.CurrentText.text = MogoLanguageUtil.GetContent(55028);
            _noticeTipRect = GetChildComponent<RectTransform>("Container_tishi");
            _noticeTipRect.gameObject.SetActive(false);
            _noticeTipRect.pivot = new Vector2(0, 0);
            _noticeTipPosition = _noticeTipRect.anchoredPosition;
            _noticeTipPosition.y -= 50;
            _speakersRect = gameObject.GetComponent<RectTransform>();
            _txtCount = GetChildComponent<StateText>("Label_txtCount");
            _txtCount.Raycast = false;
            _txtCount.CurrentText.text = string.Empty;
            _openBtn = GetChildComponent<KButton>("Button_shijieyuyin");
            _rotateParticle = AddChildComponent<KParticle>("Button_shijieyuyin/fx_ui_23_zhiyin_02");
            _rotateParticle.transform.localPosition = new Vector2(23, 0);

            _list = GetChildComponent<KList>("List_tongzhiliebiao");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _list.SetGap(2, 2);
            _listRect = _list.gameObject.GetComponent<RectTransform>();
            _pos = _listRect.anchoredPosition;
            _size = _listRect.sizeDelta;
            _list.gameObject.SetActive(false);
            
            AddEventListener();
        }

        private void AddEventListener()
        {
            _openBtn.onClick.AddListener(OnBtnClick);
            _list.onSelectedIndexChanged.AddListener(OnSelectItem);
            EventDispatcher.AddEventListener<PanelIdEnum>(FastNoticeEvents.REMOVE_FAST_NOTICE, OnRemoveFastNotice);
            //EventDispatcher.AddEventListener<MogoUILayer>(LayerEvents.HideLayer, OnMainUIHide);
           // EventDispatcher.AddEventListener<MogoUILayer>(LayerEvents.ShowLayer, OnMainUIShow);
            EventDispatcher.AddEventListener(FastNoticeEvents.HIDE, OnHidePanel);
        }

        private void OnHidePanel()
        {
            Visible = false;
        }

        //private void OnMainUIShow(MogoUILayer layer)
        //{
        //    if (layer == MogoUILayer.LayerUIMain)
        //    {
        //        ShowNoticeInfo();
        //    }
        //}

        //private void OnMainUIHide(MogoUILayer layer)
        //{
        //    if (layer == MogoUILayer.LayerUIMain)
        //    {
        //        Visible = false;
        //    }
        //}

        private void OnRemoveFastNotice(PanelIdEnum panelId)
        {
            if(_dataWrapperDict.ContainsKey(panelId))
            {
                _totalCount -= _dataWrapperDict[panelId].count;
                _dataWrapperDict.Remove(panelId);
                ShowNoticeInfo();
            }
           
        }

        protected override void OnDestroy()
        {
            _openBtn.onClick.RemoveListener(OnBtnClick);
            _list.onSelectedIndexChanged.RemoveListener(OnSelectItem);
            EventDispatcher.RemoveEventListener<PanelIdEnum>(FastNoticeEvents.REMOVE_FAST_NOTICE, OnRemoveFastNotice);
            //EventDispatcher.RemoveEventListener<MogoUILayer>(LayerEvents.HideLayer, OnMainUIHide);
            //EventDispatcher.RemoveEventListener<MogoUILayer>(LayerEvents.ShowLayer, OnMainUIShow);
            EventDispatcher.RemoveEventListener(FastNoticeEvents.HIDE, OnHidePanel);
            base.OnDestroy();
        }


        public void Show(FastNoticeData data)
        {
            if(data != null)
            {
                SetData(data);
            }
            ShowNoticeInfo();
        }

        public void Hide()
        {
            Visible = false;
            _noticeTipRect.gameObject.SetActive(false);
        }

        public void SetData(FastNoticeData fastNoticeData)
        {
            _totalCount++;
            if (fastNoticeData.PanelId == PanelIdEnum.Mail)
            {
                UIManager.Instance.PlayAudio("Sound/UI/notice_mail.mp3");
            }
            if(_dataWrapperDict.ContainsKey(fastNoticeData.PanelId) == true)
            {
                _dataWrapperDict[fastNoticeData.PanelId].count++;
                _dataWrapperDict[fastNoticeData.PanelId].lastData = fastNoticeData;
            }
            else
            {
                FastNoticeDataWrapper wrapper = new FastNoticeDataWrapper();
                wrapper.count = 1;
                wrapper.lastData = fastNoticeData;
                _dataWrapperDict.Add(fastNoticeData.PanelId, wrapper);
            }
            ShowNoticeTips();
            ShowNoticeInfo();
        }

        private void ShowNoticeTips()
        {
            _noticeTipRect.gameObject.SetActive(true);
            _noticeTipRect.anchoredPosition = _noticeTipPosition;
            _noticeTipRect.localScale = new Vector2(1, 0);

            TweenScale.Begin(_noticeTipRect.gameObject, 0.3f, Vector2.one, UITweener.Method.Linear, WaitTime);
        }

        private void WaitTime(UITweener tween)
        {
            timerId = TimerHeap.AddTimer(2000, 0, PlayMoveAnimation);
        }

        private void PlayMoveAnimation()
        {
            Vector2 speakerCenter = _speakersRect.anchoredPosition + _speakersRect.sizeDelta / 2;
            TweenPosition.Begin(_noticeTipRect.gameObject, 1.0f, _speakersRect.anchoredPosition, 0, UITweener.Method.EaseIn, OnMoveEnd);
            TweenScale.Begin(_noticeTipRect.gameObject, 1.0f, Vector2.zero, UITweener.Method.EaseIn);
        }

        private void OnMoveEnd(UITweener tween)
        {
            _noticeTipRect.gameObject.SetActive(false);
        }

        private void RefreshList()
        {
            _list.RemoveAll();
            foreach(FastNoticeDataWrapper wrapper in _dataWrapperDict.Values)
            {
                _list.AddItemAt<FastNoticeItem>(wrapper,0, false);
            }
            _list.DoLayout();
            _listRect.anchoredPosition = _pos + new Vector2(0, _listRect.sizeDelta.y - _size.y);
        }

        private void RefreshCount()
        {
            _txtCount.CurrentText.text = _totalCount.ToString();
        }

        private void OnBtnClick()
        {
            ClickBtn();
        }

        private void ClickBtn()
        {
            if (MainUIFunctionEntryView.isVisible)
            {
                EventDispatcher.AddEventListener(MainUIFunctionEntryView.CLOSE_END, OnCallBack);
                EventDispatcher.TriggerEvent(MainUIFunctionEntryView.CLOSE_FUNCTION_ENTRY_VIEW);
                return;
            }
            if (_isListOpened == false)
            {
                _isListOpened = true;
                ShowList();
            }
            else
            {
                _isListOpened = false;
                HideList();
            }
        }

        private void OnCallBack()
        {
            EventDispatcher.RemoveEventListener(MainUIFunctionEntryView.CLOSE_END, OnCallBack);
            if (_isListOpened == false)
            {
                _isListOpened = true;
                ShowList();
            }
            else
            {
                _isListOpened = false;
                HideList();
            }
        }

        private void ShowList()
        {
            _list.gameObject.SetActive(true);
            RefreshList();

            List<Vector3> targetVectorList = new List<Vector3>();
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                targetVectorList.Add(_list.ItemList[i].transform.localPosition);
                _list.ItemList[i].transform.localPosition = _list.ItemList[_list.ItemList.Count-1].transform.localPosition - new Vector3(0, 20, 0);
                TweenPosition.Begin(_list.ItemList[i].gameObject, 0.2f, targetVectorList[i]);
            }
        }

        private void HideList()
        {
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                TweenPosition.Begin(_list.ItemList[i].gameObject, 0.2f, _list.ItemList[_list.ItemList.Count-1].transform.localPosition - new Vector3(0, 20, 0), onTweenEnd);
            }

        }

        private void onTweenEnd(UITweener tween)
        {
            _list.gameObject.SetActive(false);
        }

        private void OnCallBack2()
        {
            EventDispatcher.RemoveEventListener(MainUIFunctionEntryView.CLOSE_END, OnCallBack2);
            FastNoticeDataWrapper wrapper = _list.ItemList[_list.SelectedIndex].Data as FastNoticeDataWrapper;
            _dataWrapperDict.Remove(wrapper.lastData.PanelId);
            _totalCount -= wrapper.count;
            RefreshCount();
            RefreshList();
            if (_totalCount <= 0)
            {
                Visible = false;
            }
            if (wrapper.lastData.followData != null)
            {
                Follow(wrapper.lastData.followData);
            }
            else
            {
                UIManager.Instance.ShowPanel(wrapper.lastData.PanelId, wrapper.lastData.Data);
            }
        }


        private void OnSelectItem(KList target, int index)
        {
            if (MainUIFunctionEntryView.isVisible)
            {
                EventDispatcher.AddEventListener(MainUIFunctionEntryView.CLOSE_END, OnCallBack2);
                EventDispatcher.TriggerEvent(MainUIFunctionEntryView.CLOSE_FUNCTION_ENTRY_VIEW);
                return;
            }
            FastNoticeDataWrapper wrapper = target.ItemList[target.SelectedIndex].Data as FastNoticeDataWrapper;
            _dataWrapperDict.Remove(wrapper.lastData.PanelId);
            _totalCount -= wrapper.count;
            RefreshCount();
            RefreshList();
            if(_totalCount <= 0)
            {
                Visible = false;
            }
            if (wrapper.lastData.followData != null)
            {
                Follow(wrapper.lastData.followData);
            }
            else
            {
                UIManager.Instance.ShowPanel(wrapper.lastData.PanelId, wrapper.lastData.Data);
            }
            //UIManager.Instance.ShowPanel(wrapper.lastData.PanelId, wrapper.lastData.PanelViewIndex);
        }

        private static void Follow(Dictionary<string,string[]> followData)
        {
            foreach (KeyValuePair<string, string[]> kvp in followData)
            {
                if (kvp.Key == "0")
                {
                    view_helper.OpenView(int.Parse(kvp.Value[0]));
                }
                else
                {
                    EventDispatcher.TriggerEvent(kvp.Value[0]);
                }
            }
        }


        private void ShowNoticeInfo()
        {
            if (GameSceneManager.GetInstance().CanShowNotice() == true)
            {
                if (_totalCount == 0)
                {
                    Visible = false;
                    return;
                }

                Visible = true;
                if (_isListOpened == true)
                {
                    RefreshList();
                }
                RefreshCount();
            }
            else
            {
                Visible = false;
            }
        }

        //void Update()
        //{
        //    if (_isListOpened && CheckTouchScreen() && IsMouseOverMap())
        //    {
        //        ClickBtn();
        //    }
        //}

        private bool IsMouseOverMap()
        {
            return !EventSystem.current.IsPointerOverGameObject();
        }

        private bool CheckTouchScreen()
        {
            return Input.GetMouseButtonUp(0);
        }
    }
}
