﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Events;
using ModuleGM;
using MogoEngine.Events;

namespace ModuleGM
{
    /// <summary>
    /// GM--模块控制类
    /// </summary>
    public class GMModule : BaseModule
    {
        public GMModule() { }

        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            //EventDispatcher.AddEventListener<int>(GMEvent.C_CLOSE, OnClientTestEvent);
            AddPanel(PanelIdEnum.GM, "Container_GMPanel", MogoUILayer.LayerUIPanel, "ModuleGM.GMPanel", BlurUnderlay.Have, HideMainUI.Hide); 
        } 

        #region 客户端事件监听 
        /// <summary>
        /// 客户端测试事件监听
        /// </summary>
        /// <param name="eventId"></param>
        private void OnClientTestEvent(int eventId)
        { 
        }
        #endregion
    }
}
