﻿using System.Collections.Generic;
using System.Linq;
using Common.Base;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using ModuleGM.ChildComponent;

namespace ModuleGM
{
    /// <summary>
    /// GM--视图类
    /// </summary>
    public class GMPanel : BasePanel
    {
        private KList _contentList;
        private KScrollPage _scrollPage;
        private CategoryList _categoryList;
        private List<CategoryItemData> _leftList;
        
        private List<int> _typeIdList;
        private Dictionary<int, List<gm>> _dataDict;

        private const int PAGE_SIZE = 18;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            _scrollPage = GetChildComponent<KScrollPage>("Container_shenqingliebiao/ScrollPage_fuwuqixuanze");
            _contentList = _scrollPage.GetChildComponent<KList>("mask/content");
            InitListSize();
        }

        private void InitListSize()
        {
            _contentList.SetPadding(0, 45, 15, 55);
            _contentList.SetGap(10, 45);
            _contentList.SetDirection(KList.KListDirection.LeftToRight, 3, 6);
        }

        /// <summary>
        /// 打开界面时的处理
        /// </summary>
        /// <param name="data"></param>
        public override void OnShow(System.Object data)
        {
            InitDataDict();
            InitCategoryList();
            RefreshRightList(0);
            AddEventListener();
        }

        private void InitDataDict()
        {
            if (_dataDict == null)
            {
                List<gm> list = null;
                _typeIdList = new List<int>();
                _dataDict = new Dictionary<int, List<gm>>();
                foreach (gm item in XMLManager.gm.Values)
                {
                    if (!_typeIdList.Contains(item.__type)) _typeIdList.Add(item.__type);
                    list = _dataDict.ContainsKey(item.__type) ? _dataDict[item.__type] : null;
                    if (list == null)
                    {
                        list = new List<gm>();
                        _dataDict.Add(item.__type, list);
                    }
                    list.Add(item);
                }
            }
        }

        private void InitCategoryList()
        {
            if (_leftList == null)
            {
                CategoryItemData item = null;
                _leftList = new List<CategoryItemData>();
                for (int i = 0; i < _typeIdList.Count; i++)
                {
                    item = new CategoryItemData();
                    item.name = _dataDict[_typeIdList[i]][0].__type_name;
                    _leftList.Add(item);
                }
                _categoryList.RemoveAll();
                _categoryList.SetDataList<CategoryListItem>(_leftList);
                _categoryList.SelectedIndex = 0;
            }
        }

        private void RefreshRightList(int index)
        {
            List<gm> list = _dataDict[_typeIdList[index]];
            int count = list.Count;
            _scrollPage.CurrentPage = 0;
            _scrollPage.TotalPage = count % PAGE_SIZE == 0 ? count / PAGE_SIZE : count / PAGE_SIZE + 1;
            _contentList.SetDataList<GmItemGrid>(list);
        }

        private void AddEventListener()
        {
            _categoryList.onSelectedIndexChanged.AddListener(OnCategoryChanged);
        }

        private void RemoveEventListener()
        {
            _categoryList.onSelectedIndexChanged.RemoveListener(OnCategoryChanged);
        }

        private void OnCategoryChanged(CategoryList toggleGroup, int index)
        {
            RefreshRightList(index);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GM; }
        }
    }
}
