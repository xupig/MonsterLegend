﻿using Common.Data;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;

namespace ModuleGM.ChildComponent
{
    /// <summary>
    /// GM列表中单项--视图类
    /// </summary>
    public class GmItemGrid : KList.KListItemBase
    {
        private gm _data;
        private StateText _textDesc;
        private KButton _btnClick;

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as gm;
                Refresh();
            }
        }

        public override void Dispose()
        {
            _data = null;
        }

        protected override void Awake()
        {
            _textDesc = GetChildComponent<StateText>("Button_server/label");
            _btnClick = GetChildComponent<KButton>("Button_server");
            _btnClick.onClick.AddListener(OnClick);
        }

        protected void Refresh()
        {
            if (_data != null)
            {
                _textDesc.ChangeAllStateText(_data.__desc);
                Visible = true;
            }
            else 
            {
                Visible = false;
            }
        }

        /// <summary>
        /// 点击按钮
        /// </summary>
        private static bool isOpenGm = false;
        public void OnClick()
        {
            //点中按钮，并关闭GM界面，同时打开聊天框界面显示GM命令
            LoggerHelper.Debug(string.Format("点中:{0}  cmd:{1}  isOpenGm:{2}", _data.__desc, _data.__cmd, isOpenGm));
            //UIManager.Instance.ShowPanel(PanelIdEnum.Chat, _data.Desc);
            if (!isOpenGm) 
            {
                isOpenGm = true; 
                ChatManager.Instance.RequestSendChat(public_config.CHANNEL_ID_WORLD, "@su admin", 0, (byte)ChatContentType.Text, null);
            }

            //组合命令，采取分号分割
            string[] cmds = _data.__cmd.Split(';');
            if (cmds == null) return;
            foreach (string cmd in cmds)
            {
                if (string.IsNullOrEmpty(cmd)) continue;
                LoggerHelper.Debug("发送命令:" + cmd);
                ChatManager.Instance.RequestSendChat(public_config.CHANNEL_ID_WORLD, string.Format(cmd, PlayerAvatar.Player.level + 1), 0, (byte)ChatContentType.Text, null);
            }
        }
    }
}
