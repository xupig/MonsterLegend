﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleStarSoul
{
    public class StarToolTips : BasePanel
    {
        private KButton _closeButton;
        private KDummyButton _dummyButton;
        private StateText _titleText;
        private StateText _conditionText;
        private StateText _costText;
        private StateText _rewardText;
        private KContainer _itemContainer;
        private ItemGrid _itemGrid;
        private StateText _itemName;
        private StateText _valueText;
        private KContainer _tipContainer;
        private int _itemId;

        protected override void Awake()
        {
            _tipContainer = GetChildComponent<KContainer>("Container_tip");
            _closeButton = _tipContainer.GetChildComponent<KButton>("Container_bg/Button_close");
            _dummyButton = AddChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
            _dummyButton.gameObject.SetActive(false);
            _titleText = _tipContainer.GetChildComponent<StateText>("Label_txtXingzuo");
            _conditionText = _tipContainer.GetChildComponent<StateText>("Label_txtTiaojian");
            _costText = _tipContainer.GetChildComponent<StateText>("Label_txtXiaohao");
            _rewardText = _tipContainer.GetChildComponent<StateText>("Label_txtJiangli");
            _itemContainer = _tipContainer.GetChildComponent<KContainer>("Container_wupin");
            _itemGrid = _itemContainer.GetChildComponent<ItemGrid>("Container_wupin");
            _itemName = _itemContainer.GetChildComponent<StateText>("Label_txtMingcheng");
            _valueText = _itemContainer.GetChildComponent<StateText>("Label_txtDengji");
            _valueText.CurrentText.alignment = TextAnchor.UpperRight;
            _itemName.CurrentText.alignment = TextAnchor.UpperCenter;
            RectTransform rect = _tipContainer.gameObject.GetComponent<RectTransform>();
            rect.pivot = new Vector2(0.5f, 0.5f);
            Vector3 localposition = rect.localPosition;
            localposition.x += rect.sizeDelta.x * 0.5f;
            localposition.y -= rect.sizeDelta.y * 0.5f;
            rect.localPosition = localposition;
        }

        protected void AddEventListener()
        {
            _closeButton.onClick.AddListener(CloseToolTips);
            _dummyButton.onClick.AddListener(CloseToolTips);
        }

        protected void RemoveEventListener()
        {
            _closeButton.onClick.RemoveListener(CloseToolTips);
            _dummyButton.onClick.RemoveListener(CloseToolTips);
        }


        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.StarSoulTips; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            if (data is StarToolTipsData)
            {
                StarToolTipsData info = data as StarToolTipsData;
                if (info.rewardId != 0)
                {
                    ShowToolTips(info.title, info.condition, info.cost, info.rewardId);
                }
                else
                {
                    ShowToolTips(info.title, info.condition, info.cost, info.rewardContent);
                }
            }
            else
            {
                ClosePanel();
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void CloseToolTips()
        {
            Hide();
        }

        private void ShowToolTips(string title, string condition, string cost, string reward)
        {
            _titleText.CurrentText.text = title;
            _conditionText.CurrentText.text = condition;
            _costText.CurrentText.text = cost;
            _rewardText.CurrentText.text = reward;
            _rewardText.Visible = true;
            _itemContainer.Visible = false;
            Show();
        }

        private void ShowToolTips(string title, string condition, string cost, int rewardId)
        {
            _titleText.CurrentText.text = title;
            _conditionText.CurrentText.text = condition;
            _costText.CurrentText.text = cost;
            _rewardText.Visible = false;
            _itemContainer.Visible = true;
            List<RewardData> rewardData = item_reward_helper.GetRewardDataList(rewardId, PlayerAvatar.Player.vocation);
            _valueText.CurrentText.text = rewardData[0].num > 1 ? rewardData[0].num.ToString() : string.Empty;
            _itemName.CurrentText.text = item_helper.GetName(rewardData[0].id);
            _itemId = rewardData[0].id;
            _itemGrid.SetItemData(_itemId);
            Show();
        }

        private void Show()
        {
            _tipContainer.transform.localScale = new Vector3(0.1f, 0.1f, 1);
            SetContinuousDimensionsDirty();
            TweenScale.Begin(_tipContainer.gameObject, 0.1f, Vector3.one, UITweener.Method.EaseOut);
        }

        private void SetContinuousDimensionsDirty()
        {
            ImageWrapper[] imgWrapper = _tipContainer.GetComponentsInChildren<ImageWrapper>();
            for (int i = 0; i < imgWrapper.Length; i++)
            {
                imgWrapper[i].SetContinuousDimensionDirty(true);
            }
            TextWrapper[] textWrapper = _tipContainer.GetComponentsInChildren<TextWrapper>();
            for (int i = 0; i < textWrapper.Length; i++)
            {
                textWrapper[i].SetContinuousDimensionDirty(true);
            }
        }

        private void Hide()
        {
            TweenScale.Begin(_tipContainer.gameObject, 0.1f, new Vector3(0.1f, 0.1f, 1), UITweener.Method.EaseOut, OnTweenEnd);
        }

        private void OnTweenEnd(UITweener tween)
        {
            _tipContainer.transform.localScale = Vector3.one;
            ClosePanel();
        }

    }
}
