﻿using Common.Utils;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleStarSoul
{
    public class StarSoulBackground : KContainer
    {
        protected override void Awake()
        {
            MogoAtlasUtils.AddSprite(GetChild("Container_bg0"), "xinghun0");
            MogoAtlasUtils.AddSprite(GetChild("Container_bg1"), "xinghun1");
            MogoAtlasUtils.AddSprite(GetChild("Container_bg2"), "xinghun2");
            MogoAtlasUtils.AddSprite(GetChild("Container_bg3"), "xinghun3");
        }
    }
}
