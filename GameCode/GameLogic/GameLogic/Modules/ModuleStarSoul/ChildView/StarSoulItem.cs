﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Common.Base;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils;

using Game.UI.UIComponent;
using Common.Data;
using UnityEngine.UI;
using Common.Structs;
using GameMain.GlobalManager;
using Common.Utils;
using ModuleMainUI;
using GameMain.Entities;
using Common.ExtendComponent;
using GameLoader.Utils.Timer;
using GameData;

namespace ModuleStarSoul
{
    public class StarSoulItem : KContainer
    {
        private KDummyButton _canActivate;
        private KDummyButton _notActivate;
        private KDummyButton _isActivated;
        private IconContainer _canActivateIcon;
        private IconContainer _notActivateIcon;
        private IconContainer _isActivatedIcon;

        private Locater _locater;
        private KParticle _fogEffcet;
        private KParticle _ActivateEffect;
        private KParticle _lightEffect;

        private int _page;
        private int _level;
        private StarItemData _data;

        public KComponentEvent<StarSoulItem> onIsActivatedButtonClick = new KComponentEvent<StarSoulItem>();
        public KComponentEvent<StarSoulItem> onNotActivateButtonClick = new KComponentEvent<StarSoulItem>();
        public KComponentEvent<StarSoulItem> onCanActivateButtonClick = new KComponentEvent<StarSoulItem>();

        private static GameObject _template;
        private GameObject _foregroundGo;

        protected override void Awake()
        {
            _canActivate = AddChildComponent<KDummyButton>("Container_kejihuo");
            _notActivate = AddChildComponent<KDummyButton>("Container_weijihuo");
            _isActivated = AddChildComponent<KDummyButton>("Container_jihuo");
            _canActivateIcon = AddChildComponent<IconContainer>("Container_kejihuo");
            _notActivateIcon = AddChildComponent<IconContainer>("Container_weijihuo");
            _isActivatedIcon = AddChildComponent<IconContainer>("Container_jihuo");
            _locater = gameObject.AddComponent<Locater>();
            if (_template == null)
            {
                _template = transform.parent.parent.FindChild("Image_paopao").gameObject;
            }
        }

        protected void AddEventListener()
        {
            _canActivate.onClick.AddListener(ShowCanActivateTip);
            _notActivate.onClick.AddListener(ShowNotActivateTip);
            _isActivated.onClick.AddListener(ShowIsActivatedTip);
        }

        protected void RemoveEventListener()
        {
            _canActivate.onClick.RemoveListener(ShowCanActivateTip);
            _notActivate.onClick.RemoveListener(ShowNotActivateTip);
            _isActivated.onClick.RemoveListener(ShowIsActivatedTip);
        }

        public Locater GetLocater()
        {
            return _locater;
        }

        private void LoadIconSpirte()
        {
            HideForeground();
            if (TryLoadPetModel())
            {
                _canActivateIcon.RemoveSprite();
                _notActivateIcon.RemoveSprite();
                _isActivatedIcon.RemoveSprite();
                return;
            }
            if (_data.RewardId != 0)
            {
                ShowForeground();
            }
            _canActivateIcon.SetIcon(_data.CanActivateIcon);
            _notActivateIcon.SetIcon(_data.NotActivateIcon);
            _isActivatedIcon.SetIcon(_data.IsActivatedIcon);
        }

        private bool TryLoadPetModel()
        {
            ModelComponent model = gameObject.GetComponent<ModelComponent>();
            if (model != null)
            {
                model.ClearModelGameObject();
            }
            if (_data.RewardId != 0)
            {
                int itemId = item_reward_helper.GetRewardDataList(_data.RewardId, PlayerAvatar.Player.vocation)[0].id;
                if (itemId != 0 && item_helper.GetItemType(itemId) == BagItemType.PetCard)
                {
                    Dictionary<int, float> effect = item_helper.GetUseEffect(itemId);
                    if (effect.ContainsKey(9))
                    {
                        if (model == null)
                        {
                            model = gameObject.AddComponent<ModelComponent>();
                        }
                        model.LoadPetModel((int)effect[9], OnModelLoaded);
                        return true;
                    }
                }
            }
            return false;
        }

        private void OnModelLoaded(ACTSystem.ACTActor actor)
        {
            actor.transform.localScale *= 3;
            Vector3 position = actor.localPosition;
            position.y -= 50;
            actor.localPosition = position;
        }

        private void ShowForeground()
        {
            if (_foregroundGo == null)
            {
                GameObject go = GameObject.Instantiate(_template) as GameObject;
                go.transform.SetParent(transform, false);
                go.transform.localPosition = Vector3.zero;
                _foregroundGo = go;
                _foregroundGo.AddComponent<RaycastComponent>();
            }
            _foregroundGo.SetActive(true);
        }

        private void HideForeground()
        {
            if (_foregroundGo != null)
            {
                _foregroundGo.SetActive(false);
            }
        }

        private void ShowIsActivatedTip()
        {
            onIsActivatedButtonClick.Invoke(this);
        }

        private void ShowNotActivateTip()
        {
            onNotActivateButtonClick.Invoke(this);
        }

        private void ShowCanActivateTip()
        {
            onCanActivateButtonClick.Invoke(this);
        }

        public void RefreshStarItem(int page, int level)
        {
            this.Visible = true;
            _page = page;
            _level = level;
            _data = PlayerDataManager.Instance.StarSoulData.GetItemDataByPageAndLevel(page, level);
            LoadIconSpirte();
            RefreshStarState();
        }

        public StarItemData GetItemData()
        {
            return _data;
        }

        public void RefreshStarState()
        {
            switch (_data.StarState)
            {
                case StarState.IsActivated:
                    RefreshIsActivatedView();
                    break;
                case StarState.NotActivate:
                    if (PlayerDataManager.Instance.StarSoulData.CanLight(_data.Page,_data.Level) && _data.Page == PlayerDataManager.Instance.StarSoulData.GetCurrentPage())
                    {
                        RefreshCanActivateView();
                    }
                    else
                    {
                        RefreshNotActivateView();
                    }
                    break;
            }

        }

        private void RefreshNotActivateView()
        {
            _notActivate.gameObject.SetActive(true);
            _canActivate.gameObject.SetActive(false);
            _isActivated.gameObject.SetActive(false);
            HideFogEffect();
            HideActivateEffect();
        }

        public void LightUpStar()
        {
            ShowLightEffect();
            RefreshIsActivatedView();
        }

        private void RefreshIsActivatedView()
        {
            _notActivate.gameObject.SetActive(false);
            _canActivate.gameObject.SetActive(false);
            _isActivated.gameObject.SetActive(true);
            ShowFogEffect();
            HideActivateEffect();
        }

        private void RefreshCanActivateView()
        {
            _notActivate.gameObject.SetActive(false);
            _canActivate.gameObject.SetActive(true);
            _isActivated.gameObject.SetActive(false);
            HideFogEffect();
            ShowActivateEffect(_data.RewardId != 0);
        }

        private void HideFogEffect()
        {
            if (_fogEffcet != null)
            {
                _fogEffcet.Stop();
            }
        }

        private void ShowFogEffect()
        {
            if (_fogEffcet == null)
            {
                Transform trans = Instantiate(transform.parent.parent.FindChild("fx_ui_14_1_yanwutexiao")) as Transform;
                trans.SetParent(transform, false);
                trans.localPosition = Vector3.zero;
                _fogEffcet = trans.gameObject.AddComponent<KParticle>();
            }
            _fogEffcet.Play(true);
        }

        private void ShowActivateEffect(bool isBig)
        {
            if (_ActivateEffect == null)
            {
                Transform trans = Instantiate(transform.parent.parent.FindChild("fx_ui_14_2_xuanzhuanguangquan" + (isBig ? "" : "_1"))) as Transform;
                trans.SetParent(transform, false);
                trans.localScale = Vector3.one;
                _ActivateEffect = trans.gameObject.AddComponent<KParticle>();
                _ActivateEffect.Visible = true;
                _ActivateEffect.Position = new Vector3(0, 0, -50);
            }
            _ActivateEffect.Play(true);
        }

        private void HideActivateEffect()
        {
            if (_ActivateEffect != null)
            {
                _ActivateEffect.Stop();
            }
        }

        private void ShowLightEffect()
        {
            if (_lightEffect == null)
            {
                Transform trans = Instantiate(transform.parent.parent.FindChild("fx_ui_5_2_shengji")) as Transform;
                trans.SetParent(transform, false);
                trans.localScale = Vector3.one;
                _lightEffect = trans.gameObject.AddComponent<KParticle>();
                _lightEffect.Visible = true;
                _lightEffect.Position = new Vector3(-10, 8, 0);
            }
            _lightEffect.Play();
        }

        protected override void OnEnable()
        {
            AddEventListener();
            TweenItemShake.Begin(gameObject);
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

    }
}
