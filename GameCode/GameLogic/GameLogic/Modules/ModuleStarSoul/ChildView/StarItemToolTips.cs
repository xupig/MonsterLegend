﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Common.Base;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils;

using Game.UI.UIComponent;
using Common.Data;
using Common.Utils;
using Common.ExtendComponent;

namespace ModuleStarSoul
{
    public class StarItemToolTips : KContainer
    {
        private StateText _itemNameText;
        private KButton _itemEnsureButton;
        private IconContainer _iconPlaceholder;

        protected override void Awake()
        {
            _itemEnsureButton = this.GetChildComponent<KButton>("Button_btn3");
            _itemNameText = this.GetChildComponent<StateText>("Label_txtChongwumingcheng");
            _iconPlaceholder = this.AddChildComponent<IconContainer>("Container_icon");
            this.gameObject.SetActive(false);
        }

        protected void AddEventListener()
        {
            _itemEnsureButton.onClick.AddListener(CloseToolTips);
        }

        protected void RemoveEventListener()
        {
            _itemEnsureButton.onClick.RemoveListener(CloseToolTips);
        }

        public void ShowGoodToolTips(string name, int icon)
        {
            _itemNameText.CurrentText.text = name;
            _iconPlaceholder.SetIcon(icon);
            this.gameObject.SetActive(true);
        }

        public void CloseToolTips()
        {
            this.gameObject.SetActive(false);
        }

        public void ShowToolTips(string title, string content)
        {
            this.gameObject.SetActive(true);

        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }

    }
}
