﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Common.Base;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils;

using Game.UI.UIComponent;
using Common.Data;
using UnityEngine.UI;
using Common.Structs;
using GameMain.GlobalManager;
using ModuleCommonUI;
using Common.Utils;
using Common.Global;
using GameData;
using GameMain.GlobalManager.SubSystem;
using GameLoader.Utils.Timer;
using Common.ExtendComponent;

namespace ModuleStarSoul
{
    public class StarSoulView : KContainer
    {
        private List<StarSoulItem> _starItemList;
        //private StarItemToolTips _starItemToolTips;
        private KContainer _lineContainer;

        private int _page;
        private int _starCount;
        private int _selectLevel;
        private uint _timerId;
        private int _step;

        private const int MAX_COUNT = 9;

        public KComponentEvent<StarSoulView> onLastStarLight = new KComponentEvent<StarSoulView>();

        public bool IsLighting()
        {
            return _timerId != 0;
        }

        protected override void Awake()
        {
            _starItemList = new List<StarSoulItem>();
            for (int i = 0; i < MAX_COUNT; i++)
            {
                string index = string.Format("{0:00}", i + 1);
                StarSoulItem starItem = AddChildComponent<StarSoulItem>("Container_xingdian" + index);
                _starItemList.Add(starItem);
            }
            InitEffect();
            _lineContainer = GetChildComponent<KContainer>("Container_line");
            _lineContainer.Visible = true;
        }

        public void RefreshStarSoulPage(int page)
        {
            this.Visible = true;
            ResetTimer();
            _page = page;
            _starCount = PlayerDataManager.Instance.StarSoulData.GetStarCountByPage(page);
            int i = 0;
            for (; i < _starCount; i++)
            {
                StarSoulItem starItem = _starItemList[i];
                starItem.RefreshStarItem(_page, i + 1);
                starItem.Visible = true;
                if (i != 0)
                {
                    _lineContainer.GetChild("Image_xinghunlianjie" + (i + 1).ToString()).SetActive(true);
                }
            }
            for (; i < MAX_COUNT; i++)
            {
                StarSoulItem starItem = _starItemList[i];
                starItem.Visible = false;
                if (i != 0)
                {
                    _lineContainer.GetChild("Image_xinghunlianjie" + (i + 1).ToString()).SetActive(false);
                }
            }
        }

        private void InitEffect()
        {
            transform.parent.FindChild("fx_ui_14_1_yanwutexiao").gameObject.SetActive(false);
            transform.parent.FindChild("fx_ui_14_2_xuanzhuanguangquan").gameObject.SetActive(false);
            transform.parent.FindChild("fx_ui_14_2_xuanzhuanguangquan_1").gameObject.SetActive(false);
            transform.parent.FindChild("fx_ui_5_2_shengji").gameObject.SetActive(false);
        }

        protected void AddEventListener()
        {
            for (int i = 0; i < _starItemList.Count; i++)
            {
                _starItemList[i].onCanActivateButtonClick.AddListener(ShowCanActivateToolTips);
                _starItemList[i].onIsActivatedButtonClick.AddListener(ShowIsActivatedToolTips);
                _starItemList[i].onNotActivateButtonClick.AddListener(ShowNotActivateToolTips);
            }
            EventDispatcher.AddEventListener(StarSoulEvents.REFRESH_STARSOUL_VIEW, RefreshStarSoulView);
            EventDispatcher.AddEventListener<StarItemData>(StarSoulEvents.SHOW_STAR_SOUL_GUIDE, GuideShowToolTips);
            EventDispatcher.AddEventListener(CommonUIEvents.ON_ITEM_OBTAIN_PANEL_CLOSE, ChangeStarSoulView);
            EventDispatcher.AddEventListener(PetEvents.OnPetGetPanelClose, ChangeStarSoulView);
        }

        protected void RemoveEventListener()
        {
            for (int i = 0; i < _starItemList.Count; i++)
            {
                _starItemList[i].onCanActivateButtonClick.RemoveListener(ShowCanActivateToolTips);
                _starItemList[i].onIsActivatedButtonClick.RemoveListener(ShowIsActivatedToolTips);
                _starItemList[i].onNotActivateButtonClick.RemoveListener(ShowNotActivateToolTips);
            }
            EventDispatcher.RemoveEventListener(StarSoulEvents.REFRESH_STARSOUL_VIEW, RefreshStarSoulView);
            EventDispatcher.RemoveEventListener<StarItemData>(StarSoulEvents.SHOW_STAR_SOUL_GUIDE, GuideShowToolTips);
            EventDispatcher.RemoveEventListener(CommonUIEvents.ON_ITEM_OBTAIN_PANEL_CLOSE, ChangeStarSoulView);
            EventDispatcher.RemoveEventListener(PetEvents.OnPetGetPanelClose, ChangeStarSoulView);
        }

        private void ShowIsActivatedToolTips(StarSoulItem target)
        {
            if (_timerId == 0)
            {
                _selectLevel = target.GetItemData().Level;
                ShowContent(target.GetItemData());
            }
        }

        private void GuideShowToolTips(StarItemData data)
        {
            //ShowContent(data);
            OnToolTipConfirm();
        }

        private void ShowCanActivateToolTips(StarSoulItem target)
        {
            if (_timerId == 0)
            {
                _selectLevel = target.GetItemData().Level;
                GuideShowToolTips(target.GetItemData());
            }
        }

        private void ShowNotActivateToolTips(StarSoulItem target)
        {
            if (_timerId == 0)
            {
                _selectLevel = target.GetItemData().Level;
                ShowContent(target.GetItemData());
            }
        }

        private void ShowContent(StarItemData data)
        {
            StarItemData priorData = GetPriorData(data);
            string name = AppendNameContent(data);
            string condition = "";
            if (priorData != null)
            {
                condition = MogoLanguageUtil.GetString(LangEnum.STARSOUL_TIP_ACTIVATED_CONDITION, priorData.Name);
            }
            else
            {
                condition = MogoLanguageUtil.GetContent(0);
            }
            string cost = MogoLanguageUtil.GetString(LangEnum.STARSOUL_TIP_ACTIVATED_COST, data.Cost);
            if (data.RewardId != 0)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.StarSoulTips, new StarToolTipsData(name, condition, cost, data.RewardId));
            }
            else
            {
                string reward = AppendRewardContent(data);
                UIManager.Instance.ShowPanel(PanelIdEnum.StarSoulTips, new StarToolTipsData(name, condition, cost, reward));
            }
        }

        private StarItemData GetPriorData(StarItemData data)
        {
            if (data.Page == 1 && data.Level == 1)
            {
                return null;
            }
            int page, level;
            if (data.Level > 1)
            {
                page = data.Page;
                level = data.Level - 1;
            }
            else
            {
                page = data.Page - 1;
                level = PlayerDataManager.Instance.StarSoulData.GetStarCountByPage(page);
            }
            StarItemData priorInfo = PlayerDataManager.Instance.StarSoulData.GetItemDataByPageAndLevel(page, level);
            return priorInfo;
        }

        private string AppendNameContent(StarItemData data)
        {
            string name = "";
            if (data.StarState == StarState.NotActivate)
            {
                name = string.Format("{0} ({1})", data.Name, MogoLanguageUtil.GetString(LangEnum.STARSOUL_TIP_NOTACTIVATE_LABEL));
            }
            else
            {
                name = string.Format("{0} ({1})", data.Name, MogoLanguageUtil.GetString(LangEnum.STARSOUL_TIP_ISACTIVATED_LABEL));
            }
            return name;
        }

        private string AppendRewardContent(StarItemData data)
        {
            string reward = string.Empty;
            if (data.EffectId != 0)
            {
                reward = attri_effect_helper.GetAttributeDescList(data.EffectId)[0];
                string[] content = reward.Split(' ');
                reward = MogoLanguageUtil.GetString(LangEnum.STARSOUL_TIP_ACTIVATED_PROPERTY, content[0], content[1].Substring(1));
            }
            return reward;
        }

        private void OnToolTipConfirm()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.StarSoulTips);
            UIManager.Instance.PlayAudio("Sound/UI/active.mp3");
            StartLightStar();
        }

        private void StartLightStar()
        {
            int start = -1;
            int end = _starItemList.Count - 1;
            for (int i = 0; i < _starItemList.Count; i++)
            {
                if (start == -1 && _starItemList[i].GetItemData().StarState == StarState.NotActivate)
                {
                    start = i;
                }
                if (_starItemList[i].GetItemData().Page == _page && _starItemList[i].GetItemData().Level == _selectLevel)
                {
                    end = i;
                    break;
                }
            }
            if (start != -1)
            {
                ResetTimer();
                _step = start;
                _timerId = TimerHeap.AddTimer(100, 400, OnLightStarStep, start, end);
            }
        }


        private void OnLightStarStep(int start, int end)
        {
            if (_step >= end)
            {
                ResetTimer();
                StarSoulManager.Instance.LightStar(_page, _selectLevel);
            }
            _starItemList[_step].LightUpStar();
            _step++;
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private void RefreshStarSoulView()
        {
            RefreshStarItem();
            if (PlayerDataManager.Instance.StarSoulData.GetCurrentStarItemData().RewardId == 0)
            {
                ChangeStarSoulView();
            }
        }

        public void RefreshStarItem()
        {
            for (int i = 0; i < _starItemList.Count; i++)
            {
                _starItemList[i].RefreshStarState();
            }
        }

        private void ChangeStarSoulView()
        {
            if (_selectLevel == PlayerDataManager.Instance.StarSoulData.GetStarCountByPage(_page))
            {
                onLastStarLight.Invoke(this);
            }
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            ResetTimer();
        }

    }
}
