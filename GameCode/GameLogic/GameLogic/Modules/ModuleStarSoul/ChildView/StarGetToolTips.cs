﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Common.Base;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils;

using Game.UI.UIComponent;
using Common.Data;

namespace ModuleStarSoul
{
    public class StarGetToolTips : KContainer
    {
        private KButton _closeButton;
        private KButton _sportButton;
        private KButton _copyButton;
        private KButton _guildButton;
        private KButton _achievementButton;
        private KButton _dragonButton;

        protected override void Awake()
        {
            _closeButton = GetChildComponent<KButton>("Button_close");
            _sportButton = GetChildComponent<KButton>("Container_zhizao/Button_jingjichang");
            _copyButton = GetChildComponent<KButton>("Container_zhizao/Button_fuben");
            _guildButton = GetChildComponent<KButton>("Container_zhizao/Button_gonghuizhan");
            _achievementButton = GetChildComponent<KButton>("Container_zhizao/Button_chengjiuzhan");
            _dragonButton = GetChildComponent<KButton>("Container_zhizao/Button_feilongdasai");
        }

        protected void AddEventListener()
        {
            _closeButton.onClick.AddListener(OnCloseButtonClick);
            _sportButton.onClick.AddListener(OnSportButtonClick);
            _copyButton.onClick.AddListener(OnCopyButtonClick);
            _guildButton.onClick.AddListener(OnGuildButtonClick);
            _achievementButton.onClick.AddListener(OnAchievementButtonClick);
            _dragonButton.onClick.AddListener(OnDragonButtonClick);
        }

        protected void RemoveEventListener()
        {
            _closeButton.onClick.RemoveListener(OnCloseButtonClick);
            _sportButton.onClick.RemoveListener(OnSportButtonClick);
            _copyButton.onClick.RemoveListener(OnCopyButtonClick);
            _guildButton.onClick.RemoveListener(OnGuildButtonClick);
            _achievementButton.onClick.RemoveListener(OnAchievementButtonClick);
            _dragonButton.onClick.RemoveListener(OnDragonButtonClick);
        }

        private void OnDragonButtonClick()
        {

        }

        private void OnAchievementButtonClick()
        {

        }

        private void OnGuildButtonClick()
        {

        }

        private void OnCopyButtonClick()
        {

        }

        private void OnSportButtonClick()
        {

        }

        private void OnCloseButtonClick()
        {
            this.Visible = false;
        }


        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }

    }
}
