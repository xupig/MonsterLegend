﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Common.Base;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils;

using Game.UI.UIComponent;
using Common.Data;
using Common.Utils;
using GameLoader.Utils.Timer;
using Common.ExtendComponent;
using GameMain.GlobalManager;

namespace ModuleStarSoul
{
    public class StarOpenView : KContainer
    {
        private StateText _titleText;
        private IconContainer _iconContainer;
        private RectTransform _rect;
        private RectTransform _imgRect;
        private static string _titleContent;
        private uint _timerId;
        private KParticle _effectGo;
        private KContainer _lightContainer;

        private Vector3 _originalPosition;
        private Vector3 _imgPosition;

        protected override void Awake()
        {
            _titleText = GetChildComponent<StateText>("Label_txtBiaoti");
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _lightContainer = GetChildComponent<KContainer>("Container_diguang");
            _imgRect = _lightContainer.GetComponent<RectTransform>();
            _imgPosition = _imgRect.anchoredPosition3D;
            _rect = _iconContainer.GetComponent<RectTransform>();
            _originalPosition = _rect.anchoredPosition3D;
            if (_titleContent == null)
            {
                _titleContent = _titleText.CurrentText.text;
            }
            InitEffect();
        }

        private void InitEffect()
        {
            _effectGo = AddChildComponent<KParticle>("fx_ui_14_4_xingzuokaiqi");
            _effectGo.Stop();
            _effectGo.transform.SetParent(_lightContainer.transform, false);
            _effectGo.transform.localPosition = new Vector3(-90, 100, 0);
        }

        public void Show(int index)
        {
            this.Visible = true;
            _titleText.CurrentText.text = string.Format(_titleContent,MogoLanguageUtil.GetContent(63400 + index));
            _iconContainer.SetIcon(9204 + index);
            _effectGo.Play();
            ResetTimer();
            _rect.anchoredPosition3D = _originalPosition;
            _rect.localScale = Vector3.one;
            _imgRect.anchoredPosition3D = _imgPosition;
            _imgRect.localScale = Vector3.one;
            _timerId = TimerHeap.AddTimer(2000, 0, Move);
            UIManager.Instance.PlayAudio("Sound/UI/active.mp3");
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private void Move()
        {
            ResetTimer();
            _effectGo.Stop();
            TweenPosition3D.Begin(_rect.gameObject, 1f, new Vector3(38, -335, 0));
            TweenScale.Begin(_rect.gameObject, 0.8f, new Vector3(0.2f, 0.2f, 1));
            TweenPosition3D.Begin(_imgRect.gameObject, 1f, new Vector3(38, -335, 0), OnTweenEnd);
            TweenScale.Begin(_imgRect.gameObject, 0.8f, new Vector3(0.2f, 0.2f, 1));
        }

        private void OnTweenEnd(UITweener tween)
        {
            ResetTimer();
            this.Visible = false;
            EventDispatcher.TriggerEvent(StarSoulEvents.SHOW_BACKGROUND);
            EventDispatcher.TriggerEvent(StarSoulEvents.ON_STAR_PAGE_OPEN);
        }

    }
}
