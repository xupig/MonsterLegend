﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleStarSoul
{
    public class StarToolTipsData
    {
        public string title;
        public string condition;
        public string cost;
        public string rewardContent;
        public int rewardId;

        public StarToolTipsData(string title, string condition, string cost, string rewardContent)
        {
            this.title = title;
            this.condition = condition;
            this.cost = cost;
            this.rewardContent = rewardContent;
        }

        public StarToolTipsData(string title, string condition, string cost, int rewardId)
        {
            this.title = title;
            this.condition = condition;
            this.cost = cost;
            this.rewardId = rewardId;
        }
    }
}
