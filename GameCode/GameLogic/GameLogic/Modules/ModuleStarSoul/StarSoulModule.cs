﻿using System;
using System.Collections.Generic;

using System.Text;

using UnityEngine;
using Common.Base;
using Common;
using Common.Events;
using MogoEngine.RPC;
using MogoEngine.Events;
using GameLoader.Config;

using GameMain.Entities;
using MogoEngine;
using Common.ServerConfig;

using GameLoader.Utils.CustomType;

namespace ModuleStarSoul
{
    public class StarSoulModule : BaseModule
    {

        public StarSoulModule()
            : base()
        {
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init( )
        {
            AddPanel(PanelIdEnum.StarSoul, "Container_StarSoulPanel", MogoUILayer.LayerUIPanel, "ModuleStarSoul.StarSoulPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.StarSoulTips, "Container_StarSoulTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleStarSoul.StarToolTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }

    }
}
