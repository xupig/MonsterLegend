﻿using Common.Base;
using Common.ClientConfig;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleStarSoul
{
    public class StarSoulPanel : BasePanel
    {
        private int _pageCount;
        private int _currentIndex = 0;
        private CategoryList _starSoulCategory;
        private MoneyItem _starSoulItem;
        private StarSoulView _starSoulView;
        //private StarGetToolTips _starGetToolTops;
        private KButton _introduceButton;
        private KButton _spriteButton;
        private StarOpenView _starOpenView;
        private KDummyButton _backgroundButton;
        private KContainer _backgroundContainer;
        private KContainer _starSoulContainer;
        private bool _isInit = false;

        public override void OnShow(object data)
        {
            AddEventListener();
            ShowCurrentView();
            RefreshStarPagePoint();
        }

        public override void OnClose()
        {
            RemoveEventListener();
            UIManager.Instance.ClosePanel(PanelIdEnum.StarSoulTips);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.StarSoul; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _backgroundContainer = GetChildComponent<KContainer>("Container_panelBg");
            _starSoulContainer = GetChildComponent<KContainer>("Container_xinghun");
            _starSoulCategory = GetChildComponent<CategoryList>("Container_panelBg/Container_category/List_categoryList");
            _starSoulItem = AddChildComponent<MoneyItem>("Container_panelBg/Container_xinghunshuliang");
            _starSoulItem.SetMoneyType(public_config.MONEY_TYPE_STAR_SPIRIT);
            _spriteButton = GetChildComponent<KButton>("Container_panelBg/Container_xinghunshuliang/Button_tianjia");
            //_starGetToolTops = AddChildComponent<StarGetToolTips>("Container_shilianzhiluBG");
            //_starGetToolTops.transform.localPosition = new Vector3(_starGetToolTops.transform.localPosition.x, _starGetToolTops.transform.localPosition.y, -2000);
            _introduceButton = GetChildComponent<KButton>("Container_panelBg/Button_wenhao");
            _starOpenView = AddChildComponent<StarOpenView>("Container_kaiqixingzuo");
            _backgroundButton = AddChildComponent<KDummyButton>("Container_xingkong");
            AddChildComponent<StarSoulBackground>("Container_xingkong");
            _starSoulView = AddChildComponent<StarSoulView>("Container_xinghun/Container_xingzuo");
            InitStarSoulView();
        }

        protected override void OnCloseBtnClick()
        {
            if (_starSoulView.IsLighting() == false)
            {
                base.OnCloseBtnClick();
            }
        }

        private void RefreshCategoryList(int count)
        {
            List<CategoryItemData> listToggleData = new List<CategoryItemData>();
            for (int i = 0; i < count; i++)
            {
                listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(63400 + i), 11409 + i));
            }
            _starSoulCategory.RemoveAll();
            _starSoulCategory.SetDataList<CategoryListItem>(listToggleData);
        }

        private void InitStarSoulView()
        {
            _pageCount = PlayerDataManager.Instance.StarSoulData.GetPageCount();
            RefreshCategoryList(_pageCount);
            _isInit = true;
            OnDataResp();
        }

        private void ShowCurrentView()
        {
            if (_isInit == false) return;
            _currentIndex = PlayerDataManager.Instance.StarSoulData.GetCurrentPage() - 1;
            ShowStarSoulView(_currentIndex);
            _starSoulCategory.SelectedIndex = _currentIndex;
            RefreshCategoryGroup();
        }

        private void ShowStarSoulView(int page)
        {
            _starSoulView.RefreshStarSoulPage(page + 1);
        }

        public void AddEventListener()
        {
            _starSoulCategory.onSelectedIndexChanged.AddListener(OnStarSoulToggleGroupSelect);
            _introduceButton.onClick.AddListener(OnIntorduceButtonClick);
            _spriteButton.onClick.AddListener(OnSpriteButtonClick);
            _backgroundButton.onClick.AddListener(OnCloseToolTips);
            _starSoulView.onLastStarLight.AddListener(ChangeStarSoulView);
            EventDispatcher.AddEventListener(StarSoulEvents.SHOW_BACKGROUND, ShowBackground);
            EventDispatcher.AddEventListener(StarSoulEvents.REFRESH_STAR_PAGE_POINT, RefreshStarPagePoint);
        }

        public void RemoveEventListener()
        {
            _starSoulCategory.onSelectedIndexChanged.RemoveListener(OnStarSoulToggleGroupSelect);
            _introduceButton.onClick.RemoveListener(OnIntorduceButtonClick);
            _spriteButton.onClick.RemoveListener(OnSpriteButtonClick);
            _backgroundButton.onClick.RemoveListener(OnCloseToolTips);
            _starSoulView.onLastStarLight.RemoveListener(ChangeStarSoulView);
            EventDispatcher.RemoveEventListener(StarSoulEvents.SHOW_BACKGROUND, ShowBackground);
            EventDispatcher.RemoveEventListener(StarSoulEvents.REFRESH_STAR_PAGE_POINT, RefreshStarPagePoint);
        }

        private void RefreshStarPagePoint()
        {
            for (int i = 0; i < _starSoulCategory.ItemList.Count; i++)
            {
                (_starSoulCategory.ItemList[i] as CategoryListItem).SetPoint(false);
            }
            if (PlayerDataManager.Instance.StarSoulData.CanLight())
            {
                (_starSoulCategory.ItemList[PlayerDataManager.Instance.StarSoulData.GetCurrentPage() - 1] as CategoryListItem).SetPoint(true);
            }
        }

        private void OnCloseToolTips()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.StarSoulTips);
        }

        private void OnIntorduceButtonClick()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.StarSoulTips);
            string title = MogoLanguageUtil.GetString(LangEnum.STARSOUL_TIP_INTRORDUCE_TITLE);
            string content = MogoLanguageUtil.GetString(LangEnum.STARSOUL_TIP_INTRORDUCE_CONTENT);
            MessageBox.Show(true, title, content, null, null, null, null, TextAnchor.MiddleLeft);
        }


        private void OnSpriteButtonClick()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.StarSoulTips);
            //_starGetToolTops.Visible = true;
        }

        private void ChangeStarSoulView(StarSoulView target)
        {
            if (_currentIndex + 1 < _pageCount)
            {
                _currentIndex++;
                ShowStarSoulView(_currentIndex);
                _starSoulCategory.SelectedIndex = _currentIndex;
                _starSoulView.RefreshStarItem();
                RefreshCategoryGroup();
                _starOpenView.Show(_currentIndex);
                _backgroundContainer.Visible = false;
                _starSoulContainer.Visible = false;
            }
        }

        private void ShowBackground()
        {
            _backgroundContainer.Visible = true;
            _starSoulContainer.Visible = true;
        }

        private void RefreshCategoryGroup()
        {
            List<int> hideList = new List<int>();
            for (int i = _currentIndex + 2; i < _starSoulCategory.ItemList.Count; i++)
            {
                hideList.Add(i);
            }
            _starSoulCategory.SetHiddenList(hideList);
        }

        private void OnStarSoulToggleGroupSelect(CategoryList group, int index)
        {
            if (_starSoulView.IsLighting())
            {
                _starSoulCategory.SelectedIndex = _currentIndex;
                return;
            }
            UIManager.Instance.ClosePanel(PanelIdEnum.StarSoulTips);
            if (_currentIndex != index && index < _pageCount)
            {
                _currentIndex = index;
                ShowStarSoulView(_currentIndex);
            }
        }

    }
}
