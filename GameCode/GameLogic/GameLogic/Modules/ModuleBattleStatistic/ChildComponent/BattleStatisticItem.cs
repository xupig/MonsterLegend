﻿using Common.Data;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleBattleStatistic
{
    public abstract class BattleStatisticItem:KList.KListItemBase
    {
        protected StateText _txtProgress;
        protected StateText _txtStatisticValue;
        protected StateText _playerName;
        protected KProgressBar _progressBarYellow;
        protected KProgressBar _progressBarBlue;

        protected CopyStatisticData _data;

        protected override void Awake()
        {
            _txtProgress = GetChildComponent<StateText>("Label_txtJindu");
            _txtStatisticValue = GetChildComponent<StateText>("Label_txtShanghai");
            _playerName = GetChildComponent<StateText>("Label_txtWanjiamingziqigezi");
            _progressBarYellow = GetChildComponent<KProgressBar>("ProgressBar_jindu");
            _progressBarBlue = GetChildComponent<KProgressBar>("ProgressBar_jindu02");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as CopyStatisticData;
                Refresh();
            }
        }

        public override void Dispose()
        {
            _data = null;
        }

        public virtual void Refresh()
        {
            ;
        }
    }
}
