﻿using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleBattleStatistic
{
    public class SingStatisticItem:BattleStatisticItem
    {
        private KContainer _petHead;
        private KContainer _playerHead;

        private StateText _playerLevel;
        private IconContainer _playerHeadIcon;

        private ItemGrid _petHeadGrid;
        private StateText _txtPetLevel;
        private List<StateImage> _starList;

        protected override void Awake()
        {
            base.Awake();
            _petHead = GetChildComponent<KContainer>("Container_petinfo");
            _playerHead = GetChildComponent<KContainer>("Container_touxiang");
            _playerLevel = GetChildComponent<StateText>("Container_touxiang/Container_dengji/Label_txtDengji");
            _playerHeadIcon = AddChildComponent<IconContainer>("Container_touxiang/Container_icon");

            _petHeadGrid = GetChildComponent<ItemGrid>("Container_petinfo/Container_wupin");
            _txtPetLevel = GetChildComponent<StateText>("Container_petinfo/Label_txtDengji");
            InitPetStar();
        }

        public override void Refresh()
        {
            _progressBarYellow.Visible = true;
            _progressBarBlue.Visible = false;
            if (_data is PetCopyStatisticData)
            {
                _playerHead.Visible = false;
                _playerName.Visible = false;
                _petHead.Visible = true;
                PetCopyStatisticData petCopyStatisticData = _data as PetCopyStatisticData;
                _petHeadGrid.SetItemData(pet_helper.GetIcon(petCopyStatisticData.petInfo.Id), petCopyStatisticData.petInfo.Quality);
                SetStarNum(petCopyStatisticData.petInfo.Star);
                _txtPetLevel.CurrentText.text = petCopyStatisticData.petInfo.Level.ToString();
            }
            else
            {
                PlayerCopyStatisticData playerCopyStatisticData = _data as PlayerCopyStatisticData;
                _playerHead.Visible = true;
                _playerName.Visible = true;
                _petHead.Visible = false;
                _playerHeadIcon.SetIcon( MogoPlayerUtils.GetSmallIcon(playerCopyStatisticData.vocation));
                _playerName.CurrentText.text = playerCopyStatisticData.Name;
                _playerLevel.CurrentText.text = playerCopyStatisticData.Level.ToString();
            }

            _progressBarYellow.Percent = _data.percent;
            _txtProgress.CurrentText.text = string.Format("({0}%)", _data.percent);
            _txtStatisticValue.CurrentText.text = _data.value.ToString();
        }

        private void InitPetStar()
        {
            _starList = new List<StateImage>();
            for (int i = 0; i < 5; i++)
            {
                _starList.Add(GetChildComponent<StateImage>(string.Format("Container_petinfo/Container_xingxing/Image_xingxing{0}", i)));
            }
        }

        private void SetStarNum(int starNum)
        {
            for (int i = 0; i < starNum; i++)
            {
                _starList[i].Visible = true;
            }
            for (int i = starNum; i < 5; i++)
            {
                _starList[i].Visible = false;
            }
        }
    }
}
