﻿using Common.Data;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleBattleStatistic
{
    public class PVPStatisticItem:BattleStatisticItem
    {
        private KProgressBar _progressBarRed;

        protected override void Awake()
        {
            base.Awake();
            _progressBarRed = GetChildComponent<KProgressBar>("ProgressBar_jindu03");
        }

        public override void Refresh()
        {
            PlayerCopyStatisticData playerCopyStatisticData = _data as PlayerCopyStatisticData;
            int ourCampId = (int)PlayerDataManager.Instance.InstanceAvatarData.OurCampId;
            if (playerCopyStatisticData.Name == PlayerAvatar.Player.name)
            {
                _progressBarRed.Visible = false;
                _progressBarYellow.Visible = false;
                _progressBarBlue.Visible = true;
                _progressBarBlue.Percent = _data.percent;
            }
            else if (ourCampId == playerCopyStatisticData.campId)
            {
                _progressBarRed.Visible = false;
                _progressBarYellow.Visible = true;
                _progressBarBlue.Visible = false;
                _progressBarYellow.Percent = _data.percent;
            }
            else
            {
                _progressBarRed.Visible = true;
                _progressBarYellow.Visible = false;
                _progressBarBlue.Visible = false;
                _progressBarRed.Percent = _data.percent;
            }

            _playerName.CurrentText.text = playerCopyStatisticData.Name;
            _txtProgress.CurrentText.text = string.Format("({0}%)", _data.percent);
            _txtStatisticValue.CurrentText.text = _data.value.ToString();
        }
    }
}
