﻿using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleBattleStatistic
{
    public class MultiStatisticItem:BattleStatisticItem
    {
        private KContainer _playerHead;
        private StateText _playerLevel;
        private IconContainer _playerHeadIcon;

        protected override void Awake()
        {
            base.Awake();
            _playerHead = GetChildComponent<KContainer>("Container_touxiang");
            _playerLevel = GetChildComponent<StateText>("Container_touxiang/Container_dengji/Label_txtDengji");
            _playerHeadIcon = AddChildComponent<IconContainer>("Container_touxiang/Container_icon");
        }

        public override void Refresh()
        {
            PlayerCopyStatisticData playerCopyStatisticData = _data as PlayerCopyStatisticData;

            if (playerCopyStatisticData.Name == PlayerAvatar.Player.name)
            {
                _progressBarYellow.Visible = false;
                _progressBarBlue.Visible = true;
                _progressBarBlue.Percent = _data.percent;
            }
            else
            {
                _progressBarYellow.Visible = true;
                _progressBarBlue.Visible = false;
                _progressBarYellow.Percent = _data.percent;
            }
            _playerHeadIcon.SetIcon(MogoPlayerUtils.GetSmallIcon(playerCopyStatisticData.vocation));
            _playerName.CurrentText.text = playerCopyStatisticData.Name;
            _playerLevel.CurrentText.text = playerCopyStatisticData.Level.ToString();
            _txtProgress.CurrentText.text = string.Format("({0}%)", _data.percent);
            _txtStatisticValue.CurrentText.text = _data.value.ToString();
        }
    }
}
