﻿
using Common.Data;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleBattleStatistic
{
    public class PVPStatisticView : BattleStatisticView
    {
        private KList _ourStatisticList;
        private KList _enemyStatisticList;

        protected override void Awake()
        {
            base.Awake();
            _ourStatisticList = GetChildComponent<KList>("List_wofang");
            _enemyStatisticList = GetChildComponent<KList>("List_difang");

            _ourStatisticList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _ourStatisticList.SetGap(16, 0);
            _enemyStatisticList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _enemyStatisticList.SetGap(16, 0);
        }

        public override void ShowStatisticInfo(List<CopyStatisticData> statisticData)
        {
            List<CopyStatisticData> ourStatisticDataList = GetOurCopyStatisticData(statisticData);
            List<CopyStatisticData> enemyStatisticDataList = GetEnemyCopyStatisticData(statisticData);
            _ourStatisticList.SetDataList<PVPStatisticItem>(ourStatisticDataList, 1);
            _enemyStatisticList.SetDataList<PVPStatisticItem>(enemyStatisticDataList, 1);
        }

        private List<CopyStatisticData> GetOurCopyStatisticData(List<CopyStatisticData> statisticData)
        {
            List<CopyStatisticData> result = new List<CopyStatisticData>();
            int ourCampId = (int)PlayerDataManager.Instance.InstanceAvatarData.OurCampId;
            for(int i = 0; i < statisticData.Count; i++)
            {
                if ((statisticData[i] as PlayerCopyStatisticData).campId == ourCampId)
                {
                    result.Add(statisticData[i]);
                }
            }
            return result;
        }

        private List<CopyStatisticData> GetEnemyCopyStatisticData(List<CopyStatisticData> statisticData)
        {
            List<CopyStatisticData> result = new List<CopyStatisticData>();
            int ourCampId = (int)PlayerDataManager.Instance.InstanceAvatarData.OurCampId;
            for (int i = 0; i < statisticData.Count; i++)
            {
                if ((statisticData[i] as PlayerCopyStatisticData).campId != ourCampId)
                {
                    result.Add(statisticData[i]);
                }
            }
            return result;
        }
    }
}
