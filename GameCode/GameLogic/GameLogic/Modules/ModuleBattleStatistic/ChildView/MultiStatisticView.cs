﻿using Common.Data;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleBattleStatistic
{
    public class MultiStatisticView : BattleStatisticView
    {
        private KList _statisticList;

        protected override void Awake()
        {
            base.Awake();
            _statisticList = GetChildComponent<KList>("ScrollView_geren/mask/content");
            _statisticList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _statisticList.SetGap(4, 0);
        }

        public override void ShowStatisticInfo(List<CopyStatisticData> statisticData)
        {
            _statisticList.SetDataList<MultiStatisticItem>(statisticData, 1);
        }
    }
}
