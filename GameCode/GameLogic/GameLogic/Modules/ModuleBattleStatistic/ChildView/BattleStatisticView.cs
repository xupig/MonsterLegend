﻿using Common.Base;
using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleBattleStatistic
{
    public abstract class BattleStatisticView:KContainer
    {
        protected KButton _okButton;

        protected override void Awake()
        {
            base.Awake();
            _okButton = GetChildComponent<KButton>("Button_queding");
            _okButton.onClick.AddListener(ClosePanel);
        }

        public virtual void ShowStatisticInfo(List<CopyStatisticData> statisticData)
        {

        }

        private void ClosePanel()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.BattleDataStatistic);
            if (GameSceneManager.GetInstance().chapterType == ChapterType.WanderLand)
            {
                EventDispatcher.TriggerEvent(CopyEvents.TRY_QUIT_COPY);
            }
        }
    }
}
