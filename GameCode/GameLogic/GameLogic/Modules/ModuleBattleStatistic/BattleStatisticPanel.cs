﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleBattleStatistic
{
    public class BattleStatisticPanel:BasePanel
    {
        private CategoryList _categoryList;
        private KToggleGroup _toggleGroup;
        private SingleStatisticView _singleStatisticView;
        private MultiStatisticView _multiStatisticView;
        private PVPStatisticView _pvpStatisticView;

        private const int OUTPUT = 0;
        private const int BEAR = 1;

        private List<int> _haveActionIdList;

        protected override void Awake()
        {
            base.Awake();
            _haveActionIdList = new List<int>();
            _singleStatisticView = AddChildComponent<SingleStatisticView>("Container_gerenjiesuan");
            _multiStatisticView = AddChildComponent<MultiStatisticView>("Container_zuduijiesuan");
            _pvpStatisticView = AddChildComponent<PVPStatisticView>("Container_chengshoutongji");

            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.BattleDataStatistic; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            _toggleGroup.SelectIndex = 0;
            OnStatisticTypeChanged(_toggleGroup, 0);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _categoryList.onSelectedIndexChanged.AddListener(OnCategoryListChanged);
            _toggleGroup.onSelectedIndexChanged.AddListener(OnStatisticTypeChanged);
        }

        private void RemoveEventListener()
        {
            _categoryList.onSelectedIndexChanged.RemoveListener(OnCategoryListChanged);
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnStatisticTypeChanged);
        }

        private void OnCategoryListChanged(CategoryList list, int index)
        {
            List<CopyStatisticData> staticsticData;
            if (_toggleGroup.SelectIndex == OUTPUT)
            {
                staticsticData = CopyStatisticDataManager.Instance.GetCopyStaticsticData(StatisticOutputOrBear.Output, _haveActionIdList[index]);
            }
            else
            {
                staticsticData = CopyStatisticDataManager.Instance.GetCopyStaticsticData(StatisticOutputOrBear.Bear, _haveActionIdList[index]);
            }
            FillStatisticPercent(staticsticData);
            ShowStatisticInfo(staticsticData);
        }

        private void OnStatisticTypeChanged(KToggleGroup toggleGroup, int index)
        {
            int mapId = GameSceneManager.GetInstance().curMapID;
            instance cfg = instance_helper.GetInstanceCfg(map_helper.GetInstanceIDByMapID(mapId));
            int statisticType = instance_helper.GetStatisticType(cfg);
            List<CopyStatisticData> staticsticData;
            if (index == OUTPUT)
            {
                _haveActionIdList = CopyStatisticDataManager.Instance.GetActionIdList(StatisticOutputOrBear.Output, statisticType);
                RefreshCategoryList();
                staticsticData = CopyStatisticDataManager.Instance.GetCopyStaticsticData(StatisticOutputOrBear.Output, _haveActionIdList[0]);
            }
            else
            {
                _haveActionIdList = CopyStatisticDataManager.Instance.GetActionIdList(StatisticOutputOrBear.Bear, statisticType);
                RefreshCategoryList();
                staticsticData = CopyStatisticDataManager.Instance.GetCopyStaticsticData(StatisticOutputOrBear.Bear, _haveActionIdList[0]);
            }
            FillStatisticPercent(staticsticData);
            ShowStatisticInfo(staticsticData);
        }

        private void RefreshCategoryList()
        {
            List<CategoryItemData> listToggleData = new List<CategoryItemData>();
            int count = _haveActionIdList.Count;
            for (int i = 0; i < count; i++)
            {
                listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(mission_battle_statistics_helper.GetBattleStatistics(_haveActionIdList[i]).__action_name)));
            }

            _categoryList.RemoveAll();
            _categoryList.SetDataList<CategoryListItem>(listToggleData, 1);
            if(count > 0)
            {
                _categoryList.SelectedIndex = 0;
            }
        }

        private void FillStatisticPercent(List<CopyStatisticData> staticsticDataList)
        {
            int totalValue = 0;
            for (int i = 0; i < staticsticDataList.Count; i++)
            {
                totalValue += staticsticDataList[i].value;
            }
            for (int i = 0; i < staticsticDataList.Count; i++)
            {
                if (totalValue != 0)
                {
                    staticsticDataList[i].percent = (int)Mathf.Round(((float)staticsticDataList[i].value / totalValue * 100));
                }
                else
                {
                    staticsticDataList[i].percent = 0;
                }
            }
        }

        private void ShowStatisticInfo(List<CopyStatisticData> staticsticData)
        {
            int mapId = GameSceneManager.GetInstance().curMapID;
            instance cfg = instance_helper.GetInstanceCfg(map_helper.GetInstanceIDByMapID(mapId));
            int statisticType = instance_helper.GetStatisticType(cfg);
            switch ((StatisticCopyType)statisticType)
            {
                case StatisticCopyType.Single:
                    _singleStatisticView.Visible = true;
                    _multiStatisticView.Visible = false;
                    _pvpStatisticView.Visible = false;
                    _singleStatisticView.ShowStatisticInfo(staticsticData);
                    break;
                case StatisticCopyType.Multi:
                    _singleStatisticView.Visible = false;
                    _multiStatisticView.Visible = true;
                    _pvpStatisticView.Visible = false;
                    _multiStatisticView.ShowStatisticInfo(staticsticData);
                    break;
                case StatisticCopyType.PVP:
                    _singleStatisticView.Visible = false;
                    _multiStatisticView.Visible = false;
                    _pvpStatisticView.Visible = true;
                    _pvpStatisticView.ShowStatisticInfo(staticsticData);
                    break;
            }
        }
    }
}
