﻿
using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleBattleStatistic
{
    public class BattleStatisticModule:BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.BattleDataStatistic, "Container_BattleDataStatisticPanel", MogoUILayer.LayerUIPanel, "ModuleBattleStatistic.BattleStatisticPanel", BlurUnderlay.Have, HideMainUI.Hide);
        }
    }
}
