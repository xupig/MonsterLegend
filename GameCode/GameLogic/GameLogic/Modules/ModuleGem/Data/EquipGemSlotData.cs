﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Structs;
using GameData;

namespace ModuleGem
{
    public class EquipGemSlotData
    {
        /// <summary>
        /// 槽索引，仅用来确定开启的等级
        /// </summary>
        public int slotIndex;
        /// <summary>
        /// 宝石槽类型
        /// </summary>
        public int slotType;
        /// <summary>
        /// 宝石槽状态:未激活，未解封，空，有宝石
        /// </summary>
        public EquipGemSlotStateEnum slotState;
        /// <summary>
        /// 宝石id
        /// </summary>
        public int gemId;
        /// <summary>
        /// 宝石等级
        /// </summary>
        public int gemLevel;
        /// <summary>
        /// 槽编号
        /// </summary>
        public int slotPosition;
        /// <summary>
        /// 槽所属装备格子编号
        /// </summary>
        public int slotEquipPosition;
        
    }
}
