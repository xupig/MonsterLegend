﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Structs;
using GameData;

namespace ModuleGem
{
    public class GemPopData
    {
        public int viewIndex;
        public int param0;
        public int param1;
        public int param2;
        public int param3;
        public int param4;
        public Dictionary<int, int> costDict;
    }
}
