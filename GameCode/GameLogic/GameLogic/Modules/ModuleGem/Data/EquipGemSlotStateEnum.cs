﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGem
{
    public enum EquipGemSlotStateEnum
    {
        Inactived,
        Locked,
        Empty,
        Occupied
    }
}
