﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGem
{
    public class GemModule : BaseModule
    {
        public GemModule()
            : base()
        {
        }

        public override void Init( )
        {
            RegisterPopPanelBunchPanel(PanelIdEnum.GemPop, PanelIdEnum.Gem);
            AddPanel(PanelIdEnum.Gem, "Container_GemPanel", MogoUILayer.LayerUIPanel, "ModuleGem.GemPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.GemPop, "Container_GemPopPanel", MogoUILayer.LayerUIPopPanel, "ModuleGem.GemPopPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

    }
}
