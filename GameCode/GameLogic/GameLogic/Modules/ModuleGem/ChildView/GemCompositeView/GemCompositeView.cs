﻿using Common.Events;
using Game.UI.UIComponent;
using MogoEngine.Events;
using GameData;
using GameMain.GlobalManager.SubSystem;
using GameMain.GlobalManager;
using UnityEngine;
using GameLoader.Utils.Timer;
using Common.ExtendComponent;
using System.Collections.Generic;
using Common.Base;

namespace ModuleGem
{
    public class GemCompositeView : KContainer
    {
        private GemMenuView _menuView;
        private GemSeatView _seatView;

        private KButton _compositeBtn;
        private KButton _buyBtn;
        private KContainer _compositeSuccessImageContainer;

        private int _currentGemCategory;
        private int _currentGemId;
        private KParticle _compositeParticle;
        /// <summary>
        /// 界面上合成成功的动画的初始位置和结束位置
        /// </summary>
        private Vector3 startPosition = new Vector3(457, -400, 0);
        private Vector3 endPosition = new Vector3(457, -120, 0);

        protected override void Awake()
        {
            _menuView = AddChildComponent<GemMenuView>("Container_GemChoose");
            _seatView = AddChildComponent<GemSeatView>("Container_content");
            _compositeBtn = GetChildComponent<KButton>("Button_hecheng");
            _buyBtn = GetChildComponent<KButton>("Button_Goumai");
            _compositeSuccessImageContainer = GetChildComponent<KContainer>("Container_hechengchonggong");
            _compositeSuccessImageContainer.Visible = false;
            _compositeSuccessImageContainer.transform.localPosition = startPosition;
            _compositeParticle = AddChildComponent<KParticle>("Container_content/Container_zuanshiCenter/fx_ui_6_2_hecheng_01");
            _compositeParticle.Stop();
        }

        public void Show()
        {
            this.Visible = true;
            AddEventListener();
            _menuView.ResetSelectedIndex();
            _menuView.Refresh();
        }

        public void Hide()
        {
            if (this.Visible == true)
            {
                RemoveEventListener();
                this.Visible = false;
            }
        }

        private void AddEventListener()
        {
            _menuView.onItemSelected.AddListener(OnMenuItemSelected);
            _compositeBtn.onClick.AddListener(OnCompositeClick);
            _buyBtn.onClick.AddListener(OnBuyClick);

            EventDispatcher.AddEventListener(GemEvents.COMPOSITE_SUCCESS, OnCompositeSuccess);
        }

        private void RemoveEventListener()
        {
            _menuView.onItemSelected.RemoveListener(OnMenuItemSelected);
            _compositeBtn.onClick.RemoveListener(OnCompositeClick);
            _buyBtn.onClick.RemoveListener(OnBuyClick);

            EventDispatcher.RemoveEventListener(GemEvents.COMPOSITE_SUCCESS, OnCompositeSuccess);
        }

        private void OnCompositeSuccess()
        {
            UIManager.Instance.PlayAudio("Sound/UI/mystery.mp3");
            ShowCompositeSuccessTip();
            UIManager.Instance.ClosePanel(PanelIdEnum.GemPop);
            _compositeParticle.Play();
            _menuView.Refresh();
        }

        private void ShowCompositeSuccessTip()
        {
            _compositeSuccessImageContainer.Visible = false;
            TweenPosition.Begin(_compositeSuccessImageContainer.gameObject, 0.3f, endPosition, 0.0f, UITweener.Method.BounceIn);
            TimerHeap.AddTimer<GameObject>(1000, 0, HideGameObject, _compositeSuccessImageContainer.gameObject);
        }

        private void HideGameObject(GameObject gameObject)
        {
            gameObject.SetActive(false);
            gameObject.transform.localPosition = startPosition;
        }

        private void OnMenuItemSelected(int gemCategory, int gemId)
        {
            _currentGemCategory = gemCategory;
            _currentGemId = gemId;
            _seatView.Refresh(gemCategory, gemId);
        }

        private void OnCompositeClick()
        {
            GemSettingData data = GemManager.Instance.settingData;
            if (data.ShowCompositeCost == true)
            {
                Dictionary<int, int> costDict = item_gem_helper.CalculateUpgradeMaterialNeed(_currentGemId);
                EventDispatcher.TriggerEvent<int, Dictionary<int, int>>(GemEvents.SHOW_GEM_COMPOSITE_COST, _currentGemId, costDict);
            }
            else
            {
                GemManager.Instance.CompositeInBagAnyway(_currentGemId);
            }
        }

        private void OnDiamondCompositeClick()
        {
            int score = item_gem_helper.CalculateUpgradeScroeNeed(_currentGemId - 1, GemManager.COMPOSITE_NEEN_COUNT);
            int needDiamond = item_gem_helper.ConvertGemScoreToDiamond(score);
            if (needDiamond > 0)
            {
                if (GemManager.Instance.settingData.ShowGemNotEnoughPop == true)
                {
                    EventDispatcher.TriggerEvent<int, int, int>(GemEvents.SHOW_GEM_NOT_ENOUGH_TIP, _currentGemId, 0, needDiamond);
                }
                else
                {
                    GemManager.Instance.CompositeInBagAnyway(_currentGemId);
                }
            }
            else
            {
                GemManager.Instance.CompositeInBagAnyway(_currentGemId);
            }
        }

        private void OnBuyClick()
        {
            view_helper.OpenView(62);
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }


    }
}
