﻿using Common.Base;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using GameData;
using GameMain.GlobalManager.SubSystem;
using GameMain.GlobalManager;
using Common.ServerConfig;
using Common.Global;
using Common.Data;
using Common.Structs;
using ModuleCommonUI;

namespace ModuleGem
{
    public class GemSeatView : KContainer
    {
        private KContainer _center;
        private KContainer _top;
        private KContainer _right;
        private KContainer _left;

        private KDummyButton _centerDummyBtn;
        private KDummyButton _topDummyBtn;
        private KDummyButton _rightDummyBtn;
        private KDummyButton _leftDummyBtn;

        private List<KContainer> _underlayList;
        private List<KContainer> _seatList;

        private int currentGemId = -1;

        protected override void Awake()
        {
            _center = GetChildComponent<KContainer>("Container_zuanshiCenter");
            _top = GetChildComponent<KContainer>("Container_zuanshiTop");
            _right = GetChildComponent<KContainer>("Container_zuanshiRight");
            _left = GetChildComponent<KContainer>("Container_zuanshiLeft");
            _centerDummyBtn = _center.GetChildComponent<KDummyButton>("Container_dummyBtn");
            _topDummyBtn = _top.GetChildComponent<KDummyButton>("Container_dummyBtn");
            _rightDummyBtn = _right.GetChildComponent<KDummyButton>("Container_dummyBtn");
            _leftDummyBtn = _left.GetChildComponent<KDummyButton>("Container_dummyBtn");
            
            _seatList = new List<KContainer>();
            _seatList.Add(_top);
            _seatList.Add(_left);
            _seatList.Add(_right);

            _underlayList = new List<KContainer>();
            _underlayList.Add(_center.GetChildComponent<KContainer>("Container_underlay"));
            _underlayList.Add(_top.GetChildComponent<KContainer>("Container_underlay"));
            _underlayList.Add(_right.GetChildComponent<KContainer>("Container_underlay"));
            _underlayList.Add(_left.GetChildComponent<KContainer>("Container_underlay"));

            AddDummyBtnClickListener();
        }

        //gemCategory为0时表示可合成宝石这个Category
        public void Refresh(int gemCategory, int gemId)
        {
            if (gemId > 0)
            {
                currentGemId = gemId;
            }
            RefreshCenterSeat(gemId);
            RefreshSeatList(gemId - 1);
            RefreshUnderlay(gemId);
        }

        private void AddDummyBtnClickListener()
        {
            _centerDummyBtn.onClick.AddListener(ShowCenterItemTip);
            _topDummyBtn.onClick.AddListener(ShowItemTip);
            _rightDummyBtn.onClick.AddListener(ShowItemTip);
            _leftDummyBtn.onClick.AddListener(ShowItemTip);
        }

        private void ShowCenterItemTip()
        {
            ToolTipsManager.Instance.ShowItemTip(currentGemId, PanelIdEnum.Empty, false);
        }

        private void ShowItemTip()
        {
            ToolTipsManager.Instance.ShowItemTip(currentGemId - 1, PanelIdEnum.Empty, false);
        }

        private void RefreshCenterSeat(int gemId)
        {
            GameObject go = _center.GetChild("Container_icon");
            MogoAtlasUtils.AddIcon(go, item_helper.GetIcon(gemId));
        }

        private void RefreshSeatList(int gemId)
        {
            int gemCountInBag = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(gemId);
            int iconId = item_helper.GetIcon(gemId);
            for (int i = 0; i < _seatList.Count; i++)
            {
                GameObject go = _seatList[i].GetChild("Container_icon");
                if (i < gemCountInBag)
                {
                    MogoAtlasUtils.AddSprite(go, icon_item_helper.GetIcon(iconId), icon_item_helper.GetIconBg(iconId), icon_item_helper.GetIconColor(iconId));
                }
                else
                {
                    MogoAtlasUtils.AddSprite(go, icon_item_helper.GetIcon(iconId), icon_item_helper.GetIconBg(iconId), 0);
                }
            }
        }

        private void RefreshUnderlay(int gemId)
        {
            for (int i = 0; i < _underlayList.Count; i++)
            {
                int gemType = item_gem_helper.GetGemSubtype(gemId);
                int id = item_gem_helper.GetUnderlayByGemType(gemType);
                MogoAtlasUtils.AddIcon(_underlayList[i].gameObject, id);
            }
        }
    }
}

