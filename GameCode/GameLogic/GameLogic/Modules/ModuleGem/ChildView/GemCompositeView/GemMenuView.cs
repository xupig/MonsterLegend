﻿using Common.Base;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using GameData;
using GameMain.GlobalManager.SubSystem;
using GameMain.GlobalManager;
using Common.ServerConfig;
using Common.Global;
using Common.Data;
using Common.Structs;
using GameMain.Entities;

namespace ModuleGem
{
    public class GemMenuView : KContainer
    {
        private const int ITEM_GAP = 4;
        private List<KToggle> _menuToggleList;
        //其中第一个KList是根据数量判断可合成的宝石列表（数量大于等于3），其他是根据宝石类型和VIP等级决定的可合成宝石列表
        private List<KList> _detailListList;
        private List<Locater> _locaterList;
        private RectTransform _contentRect;
        private KList _currentDetailList;
        private List<ItemData>[] _itemDataLisArray;
        //第一个参数为宝石类型，0为可合成宝石类型，第二个参数为目标宝石id
        public KComponentEvent<int, int> onItemSelected = new KComponentEvent<int, int>();

        private int _selectedListIndex = -1;
        private int _selectedGemId = -1;

        protected override void Awake()
        {
            _menuToggleList = new List<KToggle>();
            _detailListList = new List<KList>();
            _locaterList = new List<Locater>();
            _itemDataLisArray = new List<ItemData>[10];

            for (int i = 0; i < 10; i++)
            {
                KToggle menuToggle = GetChildComponent<KToggle>("ScrollView_GemToggle/mask/content/Toggle_menu" + i.ToString());
                _locaterList.Add(menuToggle.gameObject.AddComponent<Locater>());
                menuToggle.Index = i;
                _menuToggleList.Add(menuToggle);
                KList detailList = GetChildComponent<KList>("ScrollView_GemToggle/mask/content/List_detail" + i.ToString());
                _locaterList.Add(detailList.gameObject.AddComponent<Locater>());
                detailList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
                detailList.SetPadding(0, 0, 0, 0);
                detailList.SetGap(0, ITEM_GAP);
                detailList.RecalculateSize();
                _detailListList.Add(detailList);
                _contentRect = GetChildComponent<RectTransform>("ScrollView_GemToggle/mask/content");
            }

            InitMenuLabel();
            AddEventListener();
        }

        private void AddEventListener()
        {
            for (int i = 0; i < _menuToggleList.Count; i++)
            {
                _menuToggleList[i].onValueChanged.AddListener(OnMenuToggle);
            }
            for (int i = 0; i < _detailListList.Count; i++)
            {
                _detailListList[i].onSelectedIndexChanged.AddListener(OnDetailListItemClick);
            }
        }

        private void OnMenuToggle(KToggle target, bool isOn)
        {
            int index = target.Index;
            if (isOn == true)
            {
                ShowDetailList(index);
                RefreshLayout();
                ShowMenuAnimation(target.transform.localPosition, index);
            }
            else
            {
                ShowMenuCloseAnimation(target.transform.localPosition, index);
            }
        }

        private void ShowMenuCloseAnimation(Vector3 vector3, int index)
        {
            KList detailList = _detailListList[index];
            for (int i = 0; i < detailList.ItemList.Count; i++)
            {
                TweenPosition.Begin(detailList.ItemList[i].gameObject, 0.2f, detailList.ItemList[0].transform.localPosition + new Vector3(0, 60, 0), onTweenEnd);
            }
           
        }

        private void onTweenEnd(UITweener tween)
        {
            tween.gameObject.transform.parent.gameObject.SetActive(false);
            RefreshLayout();
        }

        private void ShowMenuAnimation(Vector3 startPosition, int index)
        {
            KList detailList = _detailListList[index];
            List<Vector3> targetVectorList = new List<Vector3>();
            for (int i = 0; i < detailList.ItemList.Count; i++)
            {
                targetVectorList.Add(detailList.ItemList[i].transform.localPosition);
                detailList.ItemList[i].transform.localPosition = detailList.ItemList[0].transform.localPosition + new Vector3(0, 20, 0);
                TweenPosition.Begin(detailList.ItemList[i].gameObject, 0.2f, targetVectorList[i]);
            }
        }

        private void OnDetailListItemClick(KList target, int index)
        {
            if (_currentDetailList != null && _currentDetailList != target)
            {
                _currentDetailList.SelectedIndex = -1;
            }
            _currentDetailList = target;
            _selectedListIndex = FindDetailListIndex(_currentDetailList);
            _selectedGemId = (_currentDetailList.ItemList[_currentDetailList.SelectedIndex].Data as ItemData).Id;
            onItemSelected.Invoke(_selectedListIndex, _selectedGemId);
        }

        private int FindDetailListIndex(KList target)
        {
            for (int i = 0; i < _detailListList.Count; i++)
            {
                if (_detailListList[i] == target)
                {
                    return i;
                }
            }
            return 0;
        }

        private void InitMenuLabel()
        {
            List<string> labelList = GetMenuLabelList();
            for (int i = 0; i < _menuToggleList.Count; i++)
            {
                KToggle toggle = _menuToggleList[i];
                StateText backLabel = toggle.GetChildComponent<StateText>("image/label");
                StateText markLabel = toggle.GetChildComponent<StateText>("checkmark/label");
                backLabel.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
                backLabel.ChangeAllStateText(labelList[i]);
                markLabel.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
                markLabel.ChangeAllStateText(labelList[i]);
            }
        }

        private List<string> GetMenuLabelList()
        {
            List<string> result = new List<string>();
            result.Add(MogoLanguageUtil.GetContent((int)LangEnum.GEM_COMPOSABLE));
            for (int i = 0; i < 9; i++)
            {
                result.Add(item_gem_helper.GetGemSlotDesc(i + 1) + MogoLanguageUtil.GetContent((int)LangEnum.ITEM_GEM));
            }
            return result;
        }

        public void Refresh()
        {
            //_selectedListIndex = -1;
            HideAllDetailList();
            RefreshFirstDetalList();
            RefreshOthersDetailList();
            ClearAllToggleState();
            SelectDetailItem();
            RefreshLayout();
        }

        private void HideAllDetailList()
        {
            for (int i = 0; i < _detailListList.Count; i++)
            {
                HideDetailList(i);
            }
        }

        private void ClearAllToggleState()
        {
            int startIndex = 0;
            if (_detailListList[0].Visible == false)
            {
                startIndex = 1;
            }
            for (int i = startIndex; i < _menuToggleList.Count; i++)
            {
                _menuToggleList[i].isOn = false;
            }
        }

        private void RefreshFirstDetalList()
        {
            KList firstList = _detailListList[0];
            List<ItemData> itemDataList = GetComposableGemList();
            _itemDataLisArray[0] = itemDataList;
            firstList.Visible = true;
            firstList.RemoveAll();
            firstList.SetDataList<GemDetailListItem>(itemDataList);
            firstList.RecalculateSize();
            if (itemDataList.Count == 0)
            {
                HideDetailList(0);
                _menuToggleList[0].Visible = false;
            }
            else
            {
                ShowDetailList(0);
                _menuToggleList[0].Visible = true;
            }
        }

        private void RefreshOthersDetailList()
        {
            int maxLevel = vip_rights_helper.GetComposableGemMaxLevel(PlayerAvatar.Player.vip_level);
            for (int i = 1; i < _detailListList.Count; i++)
            {
                List<ItemData> itemDataList = new List<ItemData>();
                int subType = i;
                int startLevel = 2;
                for (int j = startLevel; j <= maxLevel; j++)
                {
                    int id = item_gem_helper.GetGemIdBySubTypeAndLevel(subType, j);
                    itemDataList.Add(new ItemData(id));
                }
                _itemDataLisArray[i] = itemDataList;
            }
        }

        private void SelectDetailItem()
        {
            //第一次选择
            if (_selectedListIndex == -1)
            {
                if (_itemDataLisArray[0].Count > 0)
                {
                    _selectedListIndex = 0;
                }
                else
                {
                    _selectedListIndex = 1;
                }
            }
            //当第一个次级列表长度为0时修正
            if (_selectedListIndex == 0 && _itemDataLisArray[0].Count == 0)
            {
                _menuToggleList[0].isOn = false;
                HideDetailList(0);
                _selectedListIndex = 1;
            }
            SelectDetailListItem();
        }

        private void SelectDetailListItem()
        {
            _currentDetailList = _detailListList[_selectedListIndex];
            ShowDetailList(_selectedListIndex);
            _menuToggleList[_selectedListIndex].isOn = true;
            int selectedIndex = 0;
            for (int i = 0; i < _itemDataLisArray[_selectedListIndex].Count; i++)
            {
                if (_itemDataLisArray[_selectedListIndex][i].Id == _selectedGemId)
                {
                    selectedIndex = i;
                    break;
                }
            }
            _currentDetailList.SelectedIndex = selectedIndex;
            OnDetailListItemClick(_currentDetailList, _currentDetailList.SelectedIndex);
        }

        private void ShowDetailList(int index)
        {
            if (_itemDataLisArray[index].Count > 0)
            {
                _detailListList[index].Visible = true;
                _detailListList[index].SetDataList<GemDetailListItem>(_itemDataLisArray[index]);
            }
        }

        private void HideDetailList(int index)
        {
            _detailListList[index].Visible = false;
        }

        private List<ItemData> GetComposableGemList()
        {
            HashSet<int> set = new HashSet<int>();
            List<ItemData> result = new List<ItemData>();
            BagData itemBag = PlayerDataManager.Instance.BagData.GetItemBagData();
            List<BaseItemInfo> itemInfoList = itemBag.GetTypeItemInfo(BagItemType.Gem);
            int maxGemLevel = vip_rights_helper.GetComposableGemMaxLevel(PlayerAvatar.Player.vip_level);
            for (int i = 0; i < itemInfoList.Count; i++)
            {
                GemItemInfo itemInfo = itemInfoList[i] as GemItemInfo;
                if (itemBag.GetItemNum(itemInfo.Id) >= GemManager.COMPOSITE_NEEN_COUNT
                    && item_gem_helper.GetGemLevel(itemInfo.Id) < maxGemLevel)
                {
                    if (itemInfo.NextId > 0 && set.Contains(itemInfo.NextId) == false)
                    {
                        set.Add(itemInfo.NextId);
                        ItemData data = new ItemData(itemInfo.NextId);
                        result.Add(data);
                    }
                }
            }
            result.Sort(GemItemDataComparison);
            return result;
        }

        private int GemItemDataComparison(ItemData x, ItemData y)
        {
            return item_gem_helper.GemComparison(x, y);
        }

        private void RefreshLayout()
        {
            float y = _locaterList[0].Y;
            float height = 0;
            for (int i = 0; i < _locaterList.Count; i++)
            {
                Locater locater = _locaterList[i];
                if (locater.gameObject.activeSelf == true)
                {
                    locater.Y = y;
                    y -= locater.Height + ITEM_GAP;
                    height += locater.Height + ITEM_GAP;
                }
            }
            _contentRect.sizeDelta = new Vector2(_contentRect.sizeDelta.x, height);
        }


        internal void ResetSelectedIndex()
        {
            _selectedListIndex = -1;
        }
    }
}
