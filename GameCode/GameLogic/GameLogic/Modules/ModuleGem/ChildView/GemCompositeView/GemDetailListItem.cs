﻿using Common.Base;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using MogoEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Common.Data;

namespace ModuleGem
{
    public class GemDetailListItem : KList.KListItemBase
    {
        private ItemData _data;
        private StateText _checkmarkTxt;
        private StateText _backTxt;
        private KToggle _toggle;

        protected override void Awake()
        {
            _toggle = GetChildComponent<KToggle>("Toggle_toggle");
            _checkmarkTxt = GetChildComponent<StateText>("Toggle_toggle/checkmark/label");
            _checkmarkTxt.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
            _backTxt = GetChildComponent<StateText>("Toggle_toggle/image/label");
            _backTxt.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
            _toggle.onValueChanged.AddListener(OnToggleClick);
        }

        private void OnToggleClick(KToggle target, bool isOn)
        {
            onClick.Invoke(this, Index);
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if(value != null)
                {
                    _data = value as ItemData;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            _checkmarkTxt.ChangeAllStateText(_data.Name);
            _backTxt.ChangeAllStateText(_data.Name);
        }

        public override bool IsSelected
        {
            get
            {
                return _toggle.isOn;
            }
            set
            {
                _toggle.isOn = value;
                if(_toggle.isOn == true)
                {
                    _toggle.interactable = false;
                }
                else
                {
                    _toggle.interactable = true;
                }
            }
        }

        public override void Dispose()
        {
            _data = null;
        }
    }
}
