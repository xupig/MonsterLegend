﻿using Common.Data;
using Common.Events;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using System.Linq;

namespace ModuleGem
{
    public class InlayableGemListView : KContainer
    {
        private const int ITEM_COUNT_PERPAGE = 7;
        private KScrollPage _addableGemPage;
        private KList _addableGemList;
        private EquipItemInfo _currentEquipInfo;
        private int _gemSlotType;

        protected override void Awake()
        {
            _addableGemPage = GetComponent<KScrollPage>();
            _addableGemList = GetChildComponent<KList>("mask/content");
            InitAddableGemList();
        }

        private void InitAddableGemList()
        {
            _addableGemList.SetDirection(KList.KListDirection.LeftToRight, ITEM_COUNT_PERPAGE, 1);
            _addableGemList.SetGap(0, 10);
            _addableGemList.SetPadding(10, 5, 10, 5);
            _addableGemList.SetDataList<GemAddableItem>(GetGemItemArray(null), ITEM_COUNT_PERPAGE);
            _addableGemPage.TotalPage = BagDataManager.MaxItemNum / ITEM_COUNT_PERPAGE;
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }
        
        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, OnUpdateItemBag);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, OnUpdateItemBag);
        }

        private void OnUpdateItemBag(BagType bagType, uint position)
        {
            if(bagType == BagType.ItemBag)
            {
                if(_currentEquipInfo != null)
                {
                    RefreshAddableGemList();
                }
            }
        }

        public void SetEquipItemInfo(EquipItemInfo equipItemInfo, int gemSlotType)
        {
            _currentEquipInfo = equipItemInfo;
            _gemSlotType = gemSlotType;
            RefreshAddableGemList();
        }

        private void RefreshAddableGemList()
        {
            List<GemItemInfo> gemItemList;
            if(_gemSlotType != 0)
            {
                gemItemList = GetGemItemListBySlotType();
            }
            else
            {
                gemItemList = GetGemItemListByEquip();
            }
            gemItemList.Sort(GemItemInfoComparison);
            _addableGemList.SetDataList<GemAddableItem>(GetGemItemArray(gemItemList));
            _addableGemPage.CurrentPage = 0;
        }

        private int GemItemInfoComparison(GemItemInfo x, GemItemInfo y)
        {
            return item_gem_helper.GemComparison(x, y);
        }

        private GemItemInfo[] GetGemItemArray(List<GemItemInfo> gemItemList)
        {
            GemItemInfo[] result = new GemItemInfo[BagDataManager.MaxItemNum];
            if(gemItemList != null)
            {
                for(int i = 0; i < gemItemList.Count; i++)
                {
                    result[i] = gemItemList[i];
                }
            }
            return result;
        }

        private List<GemItemInfo> GetGemItemListBySlotType()
        {
            List<BaseItemInfo> itemInfoList = PlayerDataManager.Instance.BagData.GetItemBagData().GetTypeItemInfo(BagItemType.Gem);
            List<GemItemInfo> result = new List<GemItemInfo>();
            for(int i = 0; i < itemInfoList.Count; i++)
            {
                GemItemInfo gemItemInfo = itemInfoList[i] as GemItemInfo;
                if(_gemSlotType == GemManager.SLOT_TYPE_COMMON)
                {
                    result.Add(gemItemInfo);
                }
                else if(gemItemInfo.SlotList.Contains(_gemSlotType) == true)
                {
                    result.Add(gemItemInfo);
                }
            }
            return result;
        }

        private List<GemItemInfo> GetGemItemListByEquip()
        {
            List<BaseItemInfo> gemItemInfoList = PlayerDataManager.Instance.BagData.GetItemBagData().GetTypeItemInfo(BagItemType.Gem);
            HashSet<GemItemInfo> result = new HashSet<GemItemInfo>();
            for(int i = 0; i < gemItemInfoList.Count; i++)
            {
                GemItemInfo gemItemInfo = gemItemInfoList[i] as GemItemInfo;
                for(int j = 0; j < _currentEquipInfo.SlotList.Count; j++)
                {
                    if(gemItemInfo.SlotList.Contains(_currentEquipInfo.SlotList[j]) == true)
                    {
                        result.Add(gemItemInfo);
                    }
                }
                for(int k = 0; k < _currentEquipInfo.NewSlotList.Count; k++)
                {
                    if(k < _currentEquipInfo.OpenedNewSlotCount)
                    {
                        if(gemItemInfo.SlotList.Contains(_currentEquipInfo.NewSlotList[k]) == true)
                        {
                            result.Add(gemItemInfo);
                        }
                    }
                }
            }
            return result.ToList<GemItemInfo>();
        }
    }

}
