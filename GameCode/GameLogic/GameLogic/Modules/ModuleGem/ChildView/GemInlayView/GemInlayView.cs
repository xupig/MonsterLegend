﻿using Common.Events;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;
using Common.ExtendComponent;
using GameLoader.Utils.Timer;
using Common.Base;
using Common.Utils;
using ModuleCommonUI;
using System.Collections;
using Common.Data;

namespace ModuleGem
{
    public class GemInlayView : KContainer
    {
        private KButton _buyBtn;
        private CategoryList _equipCategory;
        private KContainer _tip;
        private EquipGemSlotView _equipGemSlotView;
        private InlayableGemListView _inlayableGemListView;
        private EquipGemSlotItem _selectedSlot;
        private EquipItemInfo _currentEquipInfo;
        private List<int> _haveEquipPosList;

        private KContainer _inlaySuccessImageContainer;
        private KContainer _compositeSuccessImageContainer;
        private KParticle _inlayParticle;

        List<CategoryItemData> listToggleData;
        List<bool> hasRedUpgradePointList;
        /// <summary>
        /// 界面上合成成功和镶嵌成功的动画的初始位置和结束位置
        /// </summary>
        private Vector3 startPosition = new Vector3(457, -400, 0);
        private Vector3 endPosition = new Vector3(457, -120, 0);

        protected override void Awake()
        {
            _buyBtn = GetChildComponent<KButton>("Button_Goumai");
            _equipCategory = GetChildComponent<CategoryList>("List_categoryList");
            _tip = GetChildComponent<KContainer>("Container_content/Container_shanghaitip");
            _tip.gameObject.SetActive(false);
            _equipGemSlotView = AddChildComponent<EquipGemSlotView>("Container_content");
            _inlayableGemListView = AddChildComponent<InlayableGemListView>("ScrollPage_xiangqianbaoshi");
            _inlaySuccessImageContainer = GetChildComponent<KContainer>("Container_xiangqianchenggong");
            _inlaySuccessImageContainer.Visible = false;
            _inlaySuccessImageContainer.transform.localPosition = startPosition;
            _compositeSuccessImageContainer = GetChildComponent<KContainer>("Container_hechengchonggong");
            _compositeSuccessImageContainer.Visible = false;
            _compositeSuccessImageContainer.transform.localPosition = startPosition;     
            _inlayParticle = AddChildComponent<KParticle>("Container_content/fx_ui_10_2_xuanwo_01");
            _inlayParticle.Stop();
        }

        private void ShowFightForce()
        {
            PlayerDataManager.Instance.FightTipsManager.ShowTipsNow();
        }

        private void RefreshEquipCategory()
        {
            hasRedUpgradePointList = new List<bool>();
            Dictionary<int, BaseItemInfo> equipItemInfoDic = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetAllItemInfo();
            listToggleData = new List<CategoryItemData>();
            _haveEquipPosList = new List<int>();
            foreach (int key in equipItemInfoDic.Keys)
            {
                _haveEquipPosList.Add(key);
            }
            _haveEquipPosList.Sort();
            int count = _haveEquipPosList.Count;
            for (int i = 0; i < count; i++)
            {
                listToggleData.Add(CategoryItemData.GetCategoryItemData(item_helper.GetEquipPositionDesc(_haveEquipPosList[i]), item_helper.GetEquipPositionIcon(_haveEquipPosList[i])));
                if (CanUpgradeGem(equipItemInfoDic[_haveEquipPosList[i]]) == true
                    || HasGemToInlay(equipItemInfoDic[_haveEquipPosList[i]]) == true)
                {
                    hasRedUpgradePointList.Add(true);
                }
                else
                {
                    hasRedUpgradePointList.Add(false);
                }
            }
            _equipCategory.RemoveAll();
            _equipCategory.SetDataList<CategoryListItem>(listToggleData, 4);
            StartCoroutine(WaitForItemCreated());
        }

        private IEnumerator WaitForItemCreated()
        {
            while (_equipCategory.ItemList.Count < listToggleData.Count)
            {
                yield return null;
            }
            SetHasUpgradePoint(hasRedUpgradePointList);
        }

        private void UpdateRedPoint()
        {
            hasRedUpgradePointList = new List<bool>();
            Dictionary<int, BaseItemInfo> equipItemInfoDic = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetAllItemInfo();
            _haveEquipPosList = new List<int>();
            foreach (int key in equipItemInfoDic.Keys)
            {
                _haveEquipPosList.Add(key);
            }
            _haveEquipPosList.Sort();
            int count = _haveEquipPosList.Count;
            for (int i = 0; i < count; i++)
            {
                if (CanUpgradeGem(equipItemInfoDic[_haveEquipPosList[i]]) == true
                    || HasGemToInlay(equipItemInfoDic[_haveEquipPosList[i]]) == true)
                {
                    hasRedUpgradePointList.Add(true);
                }
                else
                {
                    hasRedUpgradePointList.Add(false);
                }
            }
            SetHasUpgradePoint(hasRedUpgradePointList);
        }

        private bool HasGemToInlay(BaseItemInfo baseItemInfo)
        {
            const int NEW_SLOT_OFFSET = 20;
            EquipItemInfo equipInfo = baseItemInfo as EquipItemInfo;
            List<int> slotList = equipInfo.SlotList;
            List<int> newSlotList = equipInfo.NewSlotList;
            Dictionary<int, int> hasEquipedGemDict = new Dictionary<int, int>();
            bool result = false;
            for (int i = 0; i < equipInfo.GemList.Count; i++)
            {
                var item = equipInfo.GemList[i];
                hasEquipedGemDict.Add((int)item.index, (int)item.gem_id);
            }
            for (int i = 0; i < slotList.Count; i++)
            {
                if (hasEquipedGemDict.ContainsKey(i + 1))
                {
                    continue;
                }
                else
                {
                    result |= item_gem_helper.HasGemOfType(slotList[i]);
                    if (result == true)
                    {
                        return true;
                    }
                }
            }

            for (int i = 0; i < newSlotList.Count && i < equipInfo.OpenedNewSlotCount; i++)
            {
                if (hasEquipedGemDict.ContainsKey(i + 1 + NEW_SLOT_OFFSET))
                {
                    continue;
                }
                else
                {
                    result |= item_gem_helper.HasGemOfType(newSlotList[i]);
                    if (result == true)
                    {
                        return true;
                    }
                }
            }
            return result;
        }

        private bool CanUpgradeGem(BaseItemInfo baseItemInfo)
        {
            EquipItemInfo equipItemInfo = baseItemInfo as EquipItemInfo;
            for (int i = 0; i < equipItemInfo.GemList.Count; i++)
            {
                int gemId = (int)equipItemInfo.GemList[i].gem_id;
                if (item_gem_helper.IsCanUpgradeByTheSameGem(gemId) == true
                        && item_gem_helper.IsCanUpgradeByLevel(gemId) == true
                        && item_gem_helper.IsCanUpgradeByVipLevel(gemId) == true)
                {
                    return true;
                }
            }
            return false;
        }

        private void SetHasUpgradePoint(List<bool> hasRedUpgradePointList)
        {
            List<KList.KListItemBase> categoryList = _equipCategory.ItemList;
            for (int i = 0; i < categoryList.Count; i++)
            {
                CategoryListItem item = categoryList[i] as CategoryListItem;
                item.SetPoint(hasRedUpgradePointList[i]);
            }
        }

        public void Show(int categoryIndex = 0)
        {
            this.Visible = true;
            _inlayParticle.Stop();
            AddEventListener();
            RefreshEquipCategory();
            _equipCategory.SelectedIndex = categoryIndex;
            Refresh(_equipCategory.SelectedIndex);
        }

        public void Hide()
        {
            if (this.Visible == true)
            {
                RemoveEventListener();
                this.Visible = false;
            }
        }

        protected void AddEventListener()
        {
            _buyBtn.onClick.AddListener(OnBuyBtnClicked);
            _equipCategory.onSelectedIndexChanged.AddListener(OnBodyEquipChanged);
            _equipGemSlotView.onEquipGemSlotSelected.AddListener(OnEquipSlotSelected);

            EventDispatcher.AddEventListener<int>(GemEvents.INLAY, OnInlay);
            EventDispatcher.AddEventListener(GemEvents.INLAY_SUCCESS, OnInlaySuccess);
            EventDispatcher.AddEventListener(GemEvents.OUTLAY_SUCCESS, OnOutlaySuccess);
            EventDispatcher.AddEventListener(GemEvents.UPGRADE_SUCCESS, OnUpgradeSuccess);
            EventDispatcher.AddEventListener(GemEvents.PUNCH_SUCCESS, OnPunchSuccess);
            EventDispatcher.AddEventListener(GemEvents.GemPanelRefreshCategoryRedPoint, UpdateRedPoint);
            EventDispatcher.AddEventListener<int>(BagEvents.AddItem, OnGetNewItem);
        }

        private void RemoveEventListener()
        {
            _buyBtn.onClick.RemoveListener(OnBuyBtnClicked);
            _equipCategory.onSelectedIndexChanged.RemoveListener(OnBodyEquipChanged);
            _equipGemSlotView.onEquipGemSlotSelected.RemoveListener(OnEquipSlotSelected);

            EventDispatcher.RemoveEventListener<int>(GemEvents.INLAY, OnInlay);
            EventDispatcher.RemoveEventListener(GemEvents.INLAY_SUCCESS, OnInlaySuccess);
            EventDispatcher.RemoveEventListener(GemEvents.OUTLAY_SUCCESS, OnOutlaySuccess);
            EventDispatcher.RemoveEventListener(GemEvents.UPGRADE_SUCCESS, OnUpgradeSuccess);
            EventDispatcher.RemoveEventListener(GemEvents.PUNCH_SUCCESS, OnPunchSuccess);
            EventDispatcher.RemoveEventListener(GemEvents.GemPanelRefreshCategoryRedPoint, UpdateRedPoint);
            EventDispatcher.RemoveEventListener<int>(BagEvents.AddItem, OnGetNewItem);
        }

        private void OnGetNewItem(int itemId)
        {
            if (item_helper.GetItemType(itemId) == BagItemType.Gem)
            {
                UpdateRedPoint();
            }
        }

        private void OnInlay(int position)
        {
            const int NO_SLOT_TO_INLAY = 34109;
            int slotPosition = 0;
            if (_selectedSlot != null)
            {
                slotPosition = (_selectedSlot.Data as EquipGemSlotData).slotPosition;
            }
            else
            {
                GemItemInfo gemItemInfo = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemInfo(position) as GemItemInfo;
                slotPosition = _equipGemSlotView.FindSlotToInlay(gemItemInfo.SubType);
            }
            if (slotPosition != 0)
            {
                PlayerDataManager.Instance.FightTipsManager.SetDelayShow(true);
                GemManager.Instance.Inlay(_currentEquipInfo.GridPosition, slotPosition, position);
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(NO_SLOT_TO_INLAY), PanelIdEnum.Gem);
            }
        }

        private void OnInlaySuccess()
        {
            ShowInlaySuccessTip();
            Refresh(_equipCategory.SelectedIndex);
            RefreshCurrentCategoryListItem();
            UIManager.Instance.PlayAudio("Sound/UI/surprise_sall.mp3");
        }

        private void ShowParticleEffect()
        {
            _inlayParticle.transform.position = _selectedSlot.transform.position;
            _inlayParticle.Play();
        }

        private void DestroyGemIcon(GameObject go)
        {
            GameObject.Destroy(go);
        }

        private void RefreshCurrentCategoryListItem()
        {
            BaseItemInfo equipItemInfo = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo(_haveEquipPosList[_equipCategory.SelectedIndex]);
            CategoryListItem item = _equipCategory.ItemList[_equipCategory.SelectedIndex] as CategoryListItem;
        }

        private void ShowInlaySuccessTip()
        {
            _inlaySuccessImageContainer.Visible = true;
            TweenPosition.Begin(_inlaySuccessImageContainer.gameObject, 0.3f, endPosition, 0.0f, UITweener.Method.BounceIn);
            TimerHeap.AddTimer<GameObject>(1500, 0, HideGameObject, _inlaySuccessImageContainer.gameObject);
        }

        private void HideGameObject(GameObject go)
        {
            PlayerDataManager.Instance.FightTipsManager.ShowTipsNow();
            if (go != null)
            {
                go.SetActive(false);
                go.transform.localPosition = startPosition;
            }
        }

        private void OnOutlaySuccess()
        {
            Refresh(_equipCategory.SelectedIndex);
            RefreshCurrentCategoryListItem();
        }

        private void OnUpgradeSuccess()
        {
            RefreshCurrentCategoryListItem();
            ShowUpgradeSuccessTip();
            ShowParticleEffect();
            Refresh(_equipCategory.SelectedIndex);
        }

        private void ShowUpgradeSuccessTip()
        {
            _compositeSuccessImageContainer.Visible = true;
            TweenPosition.Begin(_compositeSuccessImageContainer.gameObject, 0.3f, endPosition, 0.0f, UITweener.Method.BounceIn);
            TimerHeap.AddTimer<GameObject>(1500, 0, HideGameObject, _compositeSuccessImageContainer.gameObject);
        }

        private void OnPunchSuccess()
        {
            Refresh(_equipCategory.SelectedIndex);
        }

        private void OnBodyEquipChanged(CategoryList group, int index)
        {
            Refresh(index);
        }

        private void OnEquipSlotSelected(EquipGemSlotItem slotItem)
        {
            if (_currentEquipInfo != null)
            {
                _selectedSlot = slotItem;
                if (_selectedSlot != null && _selectedSlot.OccupiedGemId != 0)
                {
                    ShowGemTip(_selectedSlot);
                }
                else
                {
                    HideGemTip();
                }
                _inlayableGemListView.SetEquipItemInfo(_currentEquipInfo, _selectedSlot == null ? 0 : _selectedSlot.GetGemSlotType());
            }
        }

        private void ShowGemTip(EquipGemSlotItem slotItem)
        {
            StateText label = _tip.GetChildComponent<StateText>("Container_txt/Label_label01");
            string[] gemDesc = slotItem.GetGemDesc();
            label.CurrentText.text = string.Format("{0}   {1}", gemDesc[0], gemDesc[1]);

            SetTipPosition(slotItem);

            _tip.gameObject.SetActive(true);
        }

        private void SetTipPosition(EquipGemSlotItem slotItem)
        {
            _tip.transform.position = slotItem.gameObject.transform.position;
            Vector3 localPosition = _tip.transform.localPosition;
            localPosition.x -= 10;
            localPosition.y += 80;
            _tip.transform.localPosition = localPosition;
        }

        private void HideGemTip()
        {
            _tip.gameObject.SetActive(false);
        }

        private void Refresh(int index)
        {
            HideGemTip();
            int equipPosition = _haveEquipPosList[index];
            int lastEquipId = _currentEquipInfo == null ? 0 : _currentEquipInfo.Id;
            _currentEquipInfo = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo(equipPosition) as EquipItemInfo;
            if (_currentEquipInfo != null)
            {
                if (lastEquipId != _currentEquipInfo.Id)
                {
                    _selectedSlot = null;
                }
                _equipGemSlotView.SetEquipItemInfo(_currentEquipInfo);
                _inlayableGemListView.SetEquipItemInfo(_currentEquipInfo, _selectedSlot == null ? 0 : _selectedSlot.GetGemSlotType());
            }
        }

        private void OnBuyBtnClicked()
        {
            int[] data = new int[] { 0, _equipCategory.SelectedIndex };
            view_helper.OpenView(62);
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }

    }
}