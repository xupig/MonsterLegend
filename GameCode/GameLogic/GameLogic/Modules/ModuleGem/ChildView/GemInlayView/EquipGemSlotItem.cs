﻿using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Structs.ProtoBuf;
using GameMain.Entities;
using Common.Structs;
using UnityEngine.EventSystems;
using Common.Data;
using Common.ExtendComponent;
using ModuleCommonUI;
using Common.Base;

namespace ModuleGem
{
    /// <summary>
    /// 装备镶嵌宝石槽
    /// </summary>
    public class EquipGemSlotItem : KList.KListItemBase
    {
        protected KContainer _icon;
        protected KContainer _iconUnderlay;
        protected KButton _upgradeBtn;
        protected KButton _outlayBtn;
        protected KToggle _chooseToggle;
        protected StateText _descTxt;
        protected StateText _lockTxt;
        private string _lockStringTemplate;
        protected GameObject _lockGo;
        protected EquipGemSlotData _data;
        public bool hasGem;
        private bool isLocked;

        //public KComponentEvent<int> upgradeEvent = new KComponentEvent<int>();

        protected override void Awake()
        {
            _icon = GetChildComponent<KContainer>("Container_icon");
            _iconUnderlay = GetChildComponent<KContainer>("Container_underlay");
            _upgradeBtn = GetChildComponent<KButton>("Button_shengji");
            _outlayBtn = GetChildComponent<KButton>("Button_xiexia");
            _chooseToggle = GetChildComponent<KToggle>("Toggle_baoshi");
            _chooseToggle.enabled = false;
            _descTxt = GetChildComponent<StateText>("Label_txtMiaoshu");
            _lockTxt = GetChildComponent<StateText>("Label_txtjiesuo");
            _lockTxt.Visible = true;
            _lockStringTemplate = _lockTxt.CurrentText.text;
            _lockGo = GetChild("Static_ItemLocked");
            AddEventListener();
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as EquipGemSlotData;
                    hasGem = true;
                    Refresh();
                }
            }
        }

        private void AddEventListener()
        {
            _outlayBtn.onClick.AddListener(OnGemTakeOff);
            _upgradeBtn.onClick.AddListener(OnGemUpgrade);
        }

        private void Refresh()
        {
            RefreshState();
        }

        private void RefreshDesc()
        {
            _descTxt.CurrentText.text = item_gem_helper.GetGemSlotDesc(_data.slotType);
        }

        private void RefreshIconUnderlay()
        {
            int id;
            if (_data.slotState == EquipGemSlotStateEnum.Locked)
            {
                id = item_gem_helper.GetUnderlayBySlotType(0);
            }
            else
            {
                id = item_gem_helper.GetUnderlayBySlotType(_data.slotType);
            }
            MogoAtlasUtils.AddIcon(_iconUnderlay.gameObject, id);
        }

        private void RefreshState()
        {
            if (_data.slotState == EquipGemSlotStateEnum.Inactived)
            {
                ShowInactivedState();
            }
            else if (_data.slotState == EquipGemSlotStateEnum.Locked)
            {
                ShowLockedState();
                RefreshIconUnderlay();
            }
            else if (_data.slotState == EquipGemSlotStateEnum.Empty)
            {
                ShowEmptyState();
                RefreshDesc();
                RefreshIconUnderlay();
            }
            else if (_data.slotState == EquipGemSlotStateEnum.Occupied)
            {
                ShowOccupiedState();
                RefreshIconUnderlay();
            }
            this.IsSelected = this.IsSelected;
        }

        private void ShowInactivedState()
        {
            this.gameObject.SetActive(false);
        }

        private void ShowLockedState()
        {
            _chooseToggle.Visible = true;
            _lockGo.SetActive(true);
            _icon.Visible = false;
            _descTxt.Visible = false;
            SetLockTxt();
        }

        private void SetLockTxt()
        {

            int level = PlayerAvatar.Player.level;
            int needLevel = item_gem_helper.GetSlotUnlockedLevel(_data.slotIndex);
            if (level < needLevel)
            {
                isLocked = true;
                _lockTxt.CurrentText.text = string.Format(_lockStringTemplate, item_gem_helper.GetSlotUnlockedLevel(_data.slotIndex));
            }
            else
            {
                isLocked = false;
                _lockTxt.CurrentText.text = string.Empty;
            }
            _outlayBtn.Visible = false;
            _upgradeBtn.Visible = false;
        }

        private void ShowOccupiedState()
        {
            _icon.Visible = true;
            _outlayBtn.Visible = false;
            _upgradeBtn.Visible = false;
            _descTxt.Visible = false;
            _iconUnderlay.Visible = true;
            _chooseToggle.Visible = true;
            _lockGo.SetActive(false);
            _lockTxt.CurrentText.text = string.Empty;
            MogoAtlasUtils.AddIcon(_icon.gameObject, item_helper.GetIcon(_data.gemId));
        }

        private void ShowEmptyState()
        {
            //MogoAtlasUtils.RemoveSprite(_icon.gameObject);
            _icon.Visible = false;
            _outlayBtn.Visible = false;
            _upgradeBtn.Visible = false;
            _iconUnderlay.Visible = true;
            _chooseToggle.Visible = true;
            _lockGo.SetActive(false);
            _descTxt.Visible = true;
            _lockTxt.CurrentText.text = string.Empty;
        }

        private void OnGemTakeOff()
        {
            if (_data.slotState == EquipGemSlotStateEnum.Occupied)
            {
                GemManager.Instance.Outlay(_data.slotEquipPosition, _data.slotPosition);
            }
        }

        public override bool IsSelected
        {
            get
            {
                return _chooseToggle.isOn;
            }
            set
            {
                _chooseToggle.isOn = value;
                RefreshSelectedStateBtn();
            }
        }

        private void RefreshSelectedStateBtn()
        {
            if (_data.slotState == EquipGemSlotStateEnum.Occupied)
            {
                _outlayBtn.gameObject.SetActive(_chooseToggle.isOn);
                bool upgradeBtnVisible = false;
                if (_chooseToggle.isOn == true)
                {
                    if (item_gem_helper.IsCanUpgradeByLevel(_data.gemId)
                        && item_gem_helper.IsCanUpgradeByVipLevel(_data.gemId))
                    {
                        upgradeBtnVisible = true;
                    }
                }
                else
                {
                    if (item_gem_helper.IsCanUpgradeByLevel(_data.gemId)
                        && item_gem_helper.IsCanUpgradeByVipLevel(_data.gemId)
                        && item_gem_helper.IsCanUpgradeByTheSameGem(_data.gemId))
                    {
                        upgradeBtnVisible = true;
                    }
                }
                _upgradeBtn.Visible = upgradeBtnVisible;
            }
        }

        private void OnGemUpgrade()
        {
            PlayerDataManager.Instance.FightTipsManager.SetDelayShow(true);
            onClick.Invoke(this, this.Index);
            //if (IsSelected == false)
            //{
            //    upgradeEvent.Invoke(this.Index);
            //}
            int needScore = item_gem_helper.CalculateUpgradeScroeNeed(_data.gemId, GemManager.UPGRADE_NEED_COUNT);
            if (needScore == 0)
            {
                GemManager.Instance.UpgradeInEquipAnyway(_data.slotEquipPosition, _data.slotPosition);
            }
            else
            {
                if (GemManager.Instance.settingData.ShowGemNotEnoughPop == true)
                {
                    int needDiamond = item_gem_helper.ConvertGemScoreToDiamond(needScore);
                    EventDispatcher.TriggerEvent<int, int, int>(GemEvents.SHOW_GEM_NOT_ENOUGH_TIP, _data.slotEquipPosition, _data.slotPosition, needDiamond);
                }
                else
                {
                    GemManager.Instance.UpgradeInEquipAnyway(_data.slotEquipPosition, _data.slotPosition);
                }
            }
        }

        /// <summary>
        /// 可镶嵌宝石类型
        /// </summary>
        /// <returns></returns>
        public int GetGemSlotType()
        {
            return _data.slotType;
        }

        public int OccupiedGemId
        {
            get
            {
                return _data.gemId;
            }
        }

        public override void OnPointerClick(PointerEventData evtData)
        {
            if (_data.slotState == EquipGemSlotStateEnum.Inactived)
            {
                return;
            }
            if (_data.slotState == EquipGemSlotStateEnum.Locked)
            {
                if (ClientCheckCanPunch() == true)
                {
                    GemManager.Instance.Punch(_data.slotEquipPosition, _data.slotPosition);
                }
                else
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(34106), PanelIdEnum.Gem);
                }
                return;
            }
            if (_data.slotState == EquipGemSlotStateEnum.Empty)
            {
                if (CheckHasGemToInlay() == false)
                {
                    if (GemManager.Instance.settingData.ShowQuickInlay == true)
                    {
                        GemPopData popData = new GemPopData();
                        popData.viewIndex = 5;
                        popData.param0 = item_gem_helper.GetGemIdBySubTypeAndLevel(_data.slotType, 1);
                        popData.param1 = _data.slotEquipPosition;
                        popData.param2 = _data.slotPosition;
                        popData.param3 = _data.slotType;
                        UIManager.Instance.ShowPanel(PanelIdEnum.GemPop, popData);
                    }
                    else
                    {
                        GemManager.Instance.QuickInlay(_data.slotEquipPosition, _data.slotPosition);
                    }
                 }
            }
            base.OnPointerClick(evtData);
        }

        private bool CheckHasGemToInlay()
        {
            List<BaseItemInfo> itemInfoList = PlayerDataManager.Instance.BagData.GetItemBagData().GetTypeItemInfo(BagItemType.Gem);
            List<GemItemInfo> result = new List<GemItemInfo>();
            for (int i = 0; i < itemInfoList.Count; i++)
            {
                GemItemInfo gemItemInfo = itemInfoList[i] as GemItemInfo;
                if (_data.slotType == GemManager.SLOT_TYPE_COMMON)
                {
                    result.Add(gemItemInfo);
                }
                else if (gemItemInfo.SlotList.Contains(_data.slotType) == true)
                {
                    result.Add(gemItemInfo);
                }
            }
            return result.Count > 0;
        }

        private bool ClientCheckCanPunch()
        {
            EquipItemInfo equipItemInfo = PlayerDataManager.Instance.BagData.GetEquipedItem(_data.slotEquipPosition);
            if (equipItemInfo == null)
            {
                return false;
            }
            int quality = equipItemInfo.Quality;
            int punchId = item_gem_helper.GetPunchItemId(quality, _data.slotIndex);
            if (isLocked == true)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, _lockTxt.CurrentText.text, PanelIdEnum.Gem);
                return false;
            }
            if (PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(punchId) > 0)
            {
                return true;
            }
            else
            {
                string failInfo = string.Format(MogoLanguageUtil.GetContent(34108), item_helper.GetName(punchId));
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, failInfo, PanelIdEnum.Gem);
            }
            return false;
        }

        public override void Dispose()
        {
            _data = null;
        }

        public string[] GetGemDesc()
        {
            List<string> attributeList = attri_effect_helper.GetAttributeDescList(item_gem_helper.GetAttriEffectId(_data.gemId));

            string[] gemDesc = attributeList[0].Split();

            return gemDesc;
        }

    }
}
