﻿using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Structs.ProtoBuf;
using GameMain.Entities;
using Common.Structs;
using Common.ExtendComponent;
using Common.Global;
using Common.Base;
using ModuleCommonUI;
using Common.Data;

namespace ModuleGem
{
    public class EquipGemSlotView : KContainer
    {
        private ItemGrid _equipItemGrid;
        private KList _slotList;
        private Locater _slotListLocater;
        private EquipItemInfo _currentEquipInfo;
        private KDummyButton _dummyBtn;
        private KDummyButton _equipDummyBtn;

        private List<EquipGemSlotData> _slotDataList;

        public KComponentEvent<EquipGemSlotItem> onEquipGemSlotSelected = new KComponentEvent<EquipGemSlotItem>();

        protected override void Awake()
        {
            _equipItemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _slotList = GetChildComponent<KList>("List_PutongKuang");
            _slotList.SetGap(0, 0);
            _slotListLocater = AddChildComponent<Locater>("List_PutongKuang");
            _dummyBtn = GetChildComponent<KDummyButton>("Container_dummyBtn");
            _equipDummyBtn = GetChildComponent<KDummyButton>("Container_wupin");

            AddEventListener();
        }

        private void AddEventListener()
        {
            _slotList.onSelectedIndexChanged.AddListener(OnSlotSelected);
            _dummyBtn.onClick.AddListener(OnDummyClick);
        }

        private void onEquipBtnClicked()
        {
            EquipToolTipsDataWrapper data = new EquipToolTipsDataWrapper();
            EquipItemInfo bodyEquip = PlayerDataManager.Instance.BagData.GetEquipedItem(_currentEquipInfo.GridPosition);
            data.leftEquipData = EquipToolTipsData.CopyFromEquipInfo(bodyEquip);
            UIManager.Instance.ShowPanel(PanelIdEnum.EquipToolTips, new ToolTipsDataWrapper(data, PanelIdEnum.Gem));
        }

        private void RemoveEventListener()
        {
            _slotList.onSelectedIndexChanged.RemoveListener(OnSlotSelected);
            _dummyBtn.onClick.RemoveListener(OnDummyClick);
        }

        private void OnSlotSelected(KList target, int index)
        {
            onEquipGemSlotSelected.Invoke(target.ItemList[index] as EquipGemSlotItem);
        }

        private void OnDummyClick()
        {
            if (_slotList.SelectedIndex != -1)
            {
                _slotList.SelectedIndex = -1;
                onEquipGemSlotSelected.Invoke(null);
            }
        }

        public void SetEquipItemInfo(EquipItemInfo equipItemInfo)
        {
            int lastEquipId = _currentEquipInfo == null ? 0 : _currentEquipInfo.Id;
            _currentEquipInfo = equipItemInfo;
            if (lastEquipId != _currentEquipInfo.Id)
            {
                RefreshEquipIcon();
                RefreshSlotListSetting();
            }
            RefreshSlotList();
        }

        private void RefreshEquipIcon()
        {
            _equipItemGrid.SetItemData(_currentEquipInfo);
            _equipItemGrid.equipState = EquipState.Wearing;
            _equipItemGrid.onClick = onEquipBtnClicked;
        }

        private void RefreshSlotListSetting()
        {
            _slotList.SetDirection(KList.KListDirection.LeftToRight, _currentEquipInfo.SlotList.Count, 2);
            _slotList.SelectedIndex = -1;
        }

        private void RefreshSlotList()
        {
            _slotDataList = GetAllGemSlotDataList();
            _slotList.SetDataList<EquipGemSlotItem>(_slotDataList);
            _slotList.RecalculateSize();
            _slotListLocater.X = (this.GetComponent<RectTransform>().sizeDelta.x - _slotListLocater.Width) * 0.5f;
            _slotListLocater.Y = -145 - (240 - _slotListLocater.Height) * 0.5f;
        }

        private void OnSlotListIndexChanged(int index)
        {
            _slotList.SelectedIndex = index;
            OnSlotSelected(_slotList, index);
        }

        private List<EquipGemSlotData> GetAllGemSlotDataList()
        {
            List<EquipGemSlotData> slotDataList = GetGemSlotDataList();
            List<EquipGemSlotData> newSlotDataList = GetGemNewSlotDataList();
            slotDataList.AddRange(newSlotDataList);
            return slotDataList;
        }

        private List<EquipGemSlotData> GetGemSlotDataList()
        {
            List<EquipGemSlotData> result = new List<EquipGemSlotData>();
            for (int i = 0; i < _currentEquipInfo.SlotList.Count; i++)
            {
                EquipGemSlotData data = new EquipGemSlotData();
                data.slotType = _currentEquipInfo.SlotList[i];
                data.slotEquipPosition = _currentEquipInfo.GridPosition;
                data.slotPosition = i + GemManager.SLOT_INDEX_START;
                data.slotState = EquipGemSlotStateEnum.Empty;
                int gemId = FindGemIdInSlot(i + GemManager.SLOT_INDEX_START);
                if (gemId != 0)
                {
                    data.slotState = EquipGemSlotStateEnum.Occupied;
                    data.gemId = gemId;
                }
                result.Add(data);
            }
            return result;
        }

        private List<EquipGemSlotData> GetGemNewSlotDataList()
        {
            List<EquipGemSlotData> result = new List<EquipGemSlotData>();
            for (int i = 0; i < _currentEquipInfo.NewSlotList.Count; i++)
            {
                EquipGemSlotData data = new EquipGemSlotData();
                data.slotIndex = i;
                data.slotType = _currentEquipInfo.SlotList[i];
                data.slotEquipPosition = _currentEquipInfo.GridPosition;
                data.slotPosition = i + GemManager.NEW_SLOT_INDEX_START;
                if (GlobalParams.GetNewSlotOpenLevel() > PlayerAvatar.Player.level)
                {
                    data.slotState = EquipGemSlotStateEnum.Inactived;
                }
                else if (i < _currentEquipInfo.OpenedNewSlotCount)
                {
                    data.slotState = EquipGemSlotStateEnum.Empty;
                    int gemId = FindGemIdInSlot(i + GemManager.NEW_SLOT_INDEX_START);
                    if (gemId != 0)
                    {
                        data.slotState = EquipGemSlotStateEnum.Occupied;
                        data.gemId = gemId;
                    }
                }
                else
                {
                    data.slotState = EquipGemSlotStateEnum.Locked;
                }
                result.Add(data);
            }
            return result;
        }

        private int FindGemIdInSlot(int slotIndex)
        {
            for (int i = 0; i < _currentEquipInfo.GemList.Count; i++)
            {
                PbEquipGems gem = _currentEquipInfo.GemList[i];
                if (gem.index == slotIndex)
                {
                    return (int)gem.gem_id;
                }
            }
            return 0;
        }

        public int FindSlotToInlay(int gemType)
        {
            List<int> commonList = new List<int>();
            for (int i = _slotDataList.Count - 1; i >= 0; i--)
            {
                EquipGemSlotData slotData = _slotDataList[i];
                if (slotData.slotState == EquipGemSlotStateEnum.Empty)
                {
                    if (slotData.slotType == gemType)
                    {
                        return slotData.slotPosition;
                    }
                    if (slotData.slotType == GemManager.SLOT_TYPE_COMMON)
                    {
                        commonList.Add(slotData.slotPosition);
                    }
                }
            }
            if (commonList.Count > 0)
            {
                return commonList[0];
            }
            else
            {
                return 0;
            }
        }

        public Vector3 FindSlotPosition(int gemType)
        {
            int index = 0;
            for (int i = _slotDataList.Count - 1; i >= 0; i--)
            {
                EquipGemSlotData slotData = _slotDataList[i];
                if (slotData.slotState == EquipGemSlotStateEnum.Empty)
                {
                    if (slotData.slotType == gemType || slotData.slotType == GemManager.SLOT_TYPE_COMMON)
                    {
                        index = i;
                    }
                }
            }
            return _slotList.ItemList[index].transform.Find("holder").position;
        }

    }
}
