﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Utils;
using GameData;
using GameMain.GlobalManager.SubSystem;
using GameMain.GlobalManager;
using UnityEngine;
using Common.Structs;
using ModuleCommonUI;
using Common.Base;
using Common.ExtendComponent;

namespace ModuleGem
{
    public class GemAddableItem : KList.KListItemBase
    {
        private ItemGrid _itemGrid;
        private StateText _countTxt;
        private GemItemInfo _data;

        protected override void Awake()
        {
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _countTxt = GetChildComponent<StateText>("Label_txtNum");

            onClick.AddListener(OnShowGemTip);
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if(value == null || value != _data)
                {
                    _data = value as GemItemInfo;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            if(_data == null)
            {
                _itemGrid.Clear();
                _countTxt.CurrentText.text = string.Empty;
            }
            else
            {
                _itemGrid.SetItemData(_data);
                _itemGrid.Context = PanelIdEnum.Gem;
                _countTxt.CurrentText.text = "X" + _data.StackCount.ToString();
            }
        }

        private void OnShowGemTip(KList.KListItemBase target, int index)
        {
            if(_data != null)
            {
                GemManager.gemType = _data.SubType;
                ToolTipsManager.Instance.ShowItemTip(_data, PanelIdEnum.Gem);
            }
        }

        public override void Dispose()
        {
            _data = null;
        }
    }
}
