﻿#region 模块信息
/*==========================================
// 文件名：GemCompositeCostTip
// 命名空间: GameLogic.GameLogic.Modules.ModuleGem.ChildView.GemPopView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/7/27 16:55:46
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using System.Collections.Generic;
using Common.ServerConfig;
using Common.Utils;
using GameData;
using UnityEngine;

namespace ModuleGem
{
    public class GemCompositeCostTip : KContainer, IGemPopView
    {
        private const int ITEM_COUNTS_PER_PAGE = 4;
        private KContainer _costDiamondContainer;
        private KScrollPage _scrollPage;
        private KList _costList;
        private KToggle _showToggle;
        private KButton _cancelBtn;
        private KButton _okBtn;
        private StateText _costDiamondTxt;
        private string _diamondCostTemplate;
        private KContainer _diamondIconContainer;
        private Vector3 _scrollPagePosition;

        private StateText _titleText;
        private int currentGemId;
        private int costDiamond = 0;
        public KComponentEvent onClose = new KComponentEvent();

        protected override void Awake()
        {
            _titleText = GetChildComponent<StateText>("Label_txthechengxiaohao");
            _costDiamondContainer = GetChildComponent<KContainer>("Container_zuanshixiaohao");
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_baoshicailiao");
            _scrollPagePosition = transform.Find("ScrollPage_baoshicailiao").localPosition;
            _costList = GetChildComponent<KList>("ScrollPage_baoshicailiao/mask/content");
            _costList.SetGap(0, 18);
            _costList.SetPadding(6, 0, 0, 10);
            _showToggle = GetChildComponent<KToggle>("Toggle_Xuanze");
            _cancelBtn = GetChildComponent<KButton>("Button_quxiao");
            _okBtn = GetChildComponent<KButton>("Button_queding");
            _costDiamondTxt = _costDiamondContainer.GetChildComponent<StateText>("Label_shuliang");
            _diamondCostTemplate = _costDiamondTxt.CurrentText.text;
            _diamondIconContainer = _costDiamondContainer.GetChildComponent<KContainer>("Container_zuanshiIcon");
            AddEventListener();
        }

        public void Show(GemPopData data)
        {
            this.Visible = true;
            currentGemId = data.param0;
            SetCostGemList(data.costDict);
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

        private void AddEventListener()
        {
            _okBtn.onClick.AddListener(OnOkBtnClicked);
            _cancelBtn.onClick.AddListener(onCancelBtnClicked);
        }

        private void RemoveEventListener()
        {
            _okBtn.onClick.RemoveListener(OnOkBtnClicked);
            _cancelBtn.onClick.RemoveListener(onCancelBtnClicked);
        }

        private void SetCostGemList(Dictionary<int, int> dictionary)
        {
            costDiamond = 0;
            _costList.RemoveAll();
            List<GemGridData> rewardDataList = GetRewardDataList(dictionary);
            if (costDiamond == 0)
            {
                _costDiamondContainer.Visible = false;
                _titleText.CurrentText.text = MogoLanguageUtil.GetContent(3117);
                _scrollPage.transform.localPosition = _scrollPagePosition - new Vector3(0, 15, 0);
            }
            else
            {
                _costDiamondContainer.Visible = true;
                _scrollPage.transform.localPosition = _scrollPagePosition;
                MogoAtlasUtils.AddIcon(_diamondIconContainer.gameObject, item_helper.GetIcon(public_config.MONEY_TYPE_DIAMOND));
                _costDiamondTxt.CurrentText.text = string.Format(_diamondCostTemplate, costDiamond);
                _titleText.CurrentText.text = MogoLanguageUtil.GetContent(3116);
            }
            _scrollPage.TotalPage = (rewardDataList.Count + 3) / ITEM_COUNTS_PER_PAGE;
            _scrollPage.CurrentPage = 0;
            if (rewardDataList.Count == 0)
            {
                rewardDataList.Add(new GemGridData(currentGemId - 1, 3, true, true));
            }
            _costList.SetDataList<GemGrid>(rewardDataList);
            if (rewardDataList.Count < ITEM_COUNTS_PER_PAGE)
            {
                AdjustItemsPosition();
            }
        }

        private void AdjustItemsPosition()
        {
            float maskWidth = GetChildComponent<RectTransform>("ScrollPage_baoshicailiao/mask").rect.width;
            float contentWidth = _costList.GetComponent<RectTransform>().rect.width;
            Vector3 oldLocalPosition = _costList.transform.localPosition;
            oldLocalPosition.x = (maskWidth - contentWidth) * 0.5f;
            _costList.transform.localPosition = oldLocalPosition;
        }

        private List<GemGridData> GetRewardDataList(Dictionary<int, int> dictionary)
        {
            List<GemGridData> result = new List<GemGridData>();
            foreach (var pair in dictionary)
            {
                if (pair.Key != public_config.MONEY_TYPE_DIAMOND)
                {
                    result.Add(new GemGridData(pair.Key, pair.Value));
                }
                else
                {
                    costDiamond = pair.Value > 0 ? pair.Value : 0;
                }
            }
            return result;
        }

        public void Hide()
        {
            this.Visible = true;
            if (this.Visible == true)
            {
                SavePopSetting(_showToggle.isOn);
                this.Visible = false;
            }
        }

        private void SavePopSetting(bool value)
        {
            GemManager.Instance.settingData.ShowCompositeCost = !value;
        }

        private void onCancelBtnClicked()
        {
            ResetToggle();
            OnClose();
        }

        private void ResetToggle()
        {
            _showToggle.isOn = false;
        }

        private void OnClose()
        {
            SavePopSetting(_showToggle.isOn);
            onClose.Invoke();
        }

        private void OnOkBtnClicked()
        {
            GemManager.Instance.CompositeInBagAnyway(currentGemId);
            OnClose();
        }

    }
}