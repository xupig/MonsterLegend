﻿#region 模块信息
/*==========================================
// 文件名：GemFastInlayTips
// 命名空间: GameLogic.GameLogic.Modules.ModuleGem.ChildView.GemPopView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/4 11:06:49
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
namespace ModuleGem
{
    public class GemQuickInlayTip : KContainer, IGemPopView
    {
        private StateText _txtMessage;
        private string _template;
        private KToggle _showToggle;
        private KButton _okBtn;
        private KButton _cancelBtn;
        private int quickInlayGemId;
        private GemPopData _currentData;

        public KComponentEvent onClose = new KComponentEvent();

        protected override void Awake()
        {
            _txtMessage = GetChildComponent<StateText>("Label_txtGoumaiXiaohao");
            _template = _txtMessage.CurrentText.text;
            _showToggle = GetChildComponent<KToggle>("Toggle_Xuanze");
            _okBtn = GetChildComponent<KButton>("Button_queding");
            _cancelBtn = GetChildComponent<KButton>("Button_quxiao");

            AddEventListener();
        }

        public void Show(GemPopData data)
        {
            this.Visible = true;
            _currentData = data;
            quickInlayGemId = data.param0;
            SetQuickInlayView();
        }

        private void SetQuickInlayView()
        {
            _txtMessage.CurrentText.text = string.Format(_template, item_gem_helper.GetGemDimondCost(quickInlayGemId), item_gem_helper.GetGemName(quickInlayGemId));
        }

        private void AddEventListener()
        {
            _okBtn.onClick.AddListener(OnOkClicked);
            _cancelBtn.onClick.AddListener(OnCancelClicked);
        }

        private void RemoveEventListener()
        {
            _okBtn.onClick.RemoveListener(OnOkClicked);
            _cancelBtn.onClick.RemoveListener(OnCancelClicked);
        }

        private void OnOkClicked()
        {
            if (CheckHasEnoughDiamond(_currentData.param0) == true)
            {
                GemManager.Instance.QuickInlay(_currentData.param1, _currentData.param2);
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(34203));
            }
        }

        private bool CheckHasEnoughDiamond(int gemId)
        {
            return PlayerAvatar.Player.money_diamond >= item_gem_helper.GetGemDimondCost(gemId);
        }

        private void OnCancelClicked()
        {
            onClose.Invoke();
        }

        private void SavePopSetting(bool value)
        {
            GemManager.Instance.settingData.ShowQuickInlay = !value;
        }

        public void Hide()
        {
            if (this.Visible == true)
            {
                SavePopSetting(_showToggle.isOn);
            }
        }

        protected override void OnDisable()
        {
            SavePopSetting(_showToggle.isOn);
        }

    }
}