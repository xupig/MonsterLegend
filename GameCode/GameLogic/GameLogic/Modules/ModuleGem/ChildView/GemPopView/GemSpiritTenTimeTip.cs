﻿using Common.Base;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.Enum;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.ExtendComponent;

namespace ModuleGem
{
    public class GemSpiritTenTimeTip : KContainer, IGemPopView
    {
        private KButton _cancelBtn;
        private KButton _confirmBtn;

        private KToggle _notPopOption;

        private StateText _costGoldTxt;
        private StateText _costSoulStoneTxt;
        private ItemGrid _goldIcon;
        private ItemGrid _soulStoneIcon;

        public KComponentEvent onClose = new KComponentEvent();

        protected override void Awake()
        {
            _cancelBtn = GetChildComponent<KButton>("Button_quxiao");
            _confirmBtn = GetChildComponent<KButton>("Button_queding");
            _notPopOption = GetChildComponent<KToggle>("Toggle_Xuanze");
            _costGoldTxt = GetChildComponent<StateText>("Label_txtGoldValue");
            _costSoulStoneTxt = GetChildComponent<StateText>("Label_txtLinghunshiValue");
            _goldIcon = GetChildComponent<ItemGrid>("Container_goldIcon/Container_wupin");
            _soulStoneIcon = GetChildComponent<ItemGrid>("Container_linghunshiIcon/Container_wupin");
            _goldIcon.SetItemData(item_helper.GetIcon(public_config.MONEY_TYPE_GOLD), item_helper.GetQuality(public_config.MONEY_TYPE_GOLD));
            _soulStoneIcon.SetItemData(item_helper.GetIcon(GemManager.SOUL_STONE_ITEM_ID), item_helper.GetQuality(GemManager.SOUL_STONE_ITEM_ID));
            AddEventListener();
        }

        public void Show(GemPopData data)
        {
            this.Visible = true;
            Refresh();
        }

        public void Hide()
        {
            this.Visible = false;
        }

        private void Refresh()
        {
            int spiritLevel = (int)PlayerAvatar.Player.spirit_level;
            int goldCost = item_gem_helper.GetSpiritGoldCost(spiritLevel) * 10;
            int stoneCost = item_gem_helper.GetSpiritSoulStoneCost(spiritLevel) * 10;
            _costGoldTxt.CurrentText.text = "X " + goldCost;
            _costSoulStoneTxt.CurrentText.text = "X " + stoneCost;
        }

        protected void AddEventListener()
        {
            _confirmBtn.onClick.AddListener(OnConfirmButtonClick);
            _cancelBtn.onClick.AddListener(OnCancelButtonClick);
        }

        protected override void OnDestroy()
        {
            _confirmBtn.onClick.RemoveListener(OnConfirmButtonClick);
            _cancelBtn.onClick.RemoveListener(OnCancelButtonClick);
            base.OnDestroy();
        }

        private void OnClose()
        {
            SavePopSetting(_notPopOption.isOn);
            Hide();
            onClose.Invoke();
        }

        private void SavePopSetting(bool value)
        {
            GemManager.Instance.settingData.ShowSpiritTenTimePop = !value;
        }

        private void OnConfirmButtonClick()
        {
            GemManager.Instance.SpiritTenTime();
            OnClose();
        }

        private void OnCancelButtonClick()
        {
            ResetToggle();
            OnClose();
        }

        private void ResetToggle()
        {
            _notPopOption.isOn = false;
        }

    }
}
