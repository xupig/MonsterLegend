﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Utils;
using GameData;
using GameMain.GlobalManager.SubSystem;
using GameMain.GlobalManager;
using UnityEngine;
using Common.Structs;
using ModuleCommonUI;
using Common.Base;
using Common.ServerConfig;
using Common.ExtendComponent;

namespace ModuleGem
{
    public class GemSpiritHelpTip : KContainer, IGemPopView
    {
        private KButton _confirmBtn;

        public KComponentEvent onClose = new KComponentEvent();

        protected override void Awake()
        {
            base.Awake();
            _confirmBtn = GetChildComponent<KButton>("Button_queding");

            _confirmBtn.onClick.AddListener(OnClose);
        }

        private void OnClose()
        {
            Hide();
            onClose.Invoke();
        }

        public void Show(GemPopData data)
        {
            this.Visible = true;
        }

        public void Hide()
        {
            this.Visible = false;
        }
    }
}
