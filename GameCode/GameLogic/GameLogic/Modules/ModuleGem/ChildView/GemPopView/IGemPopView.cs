﻿using Common.Base;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.Enum;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.ExtendComponent;

namespace ModuleGem
{
    public interface IGemPopView
    {
        void Show(GemPopData data);
        void Hide();
    }
}
