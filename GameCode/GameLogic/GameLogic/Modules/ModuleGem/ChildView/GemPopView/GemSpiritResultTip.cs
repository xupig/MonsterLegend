﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Utils;
using GameData;
using GameMain.GlobalManager.SubSystem;
using GameMain.GlobalManager;
using UnityEngine;
using Common.Structs;
using ModuleCommonUI;
using Common.Base;
using Common.ServerConfig;
using Common.ExtendComponent;
using Common.Events;
using MogoEngine.Events;

namespace ModuleGem
{
    public class GemSpiritResultTip : KContainer, IGemPopView
    {
        private KButton _confirmBtn;
        private StateText _commonTxt;
        private string _commonTemplate;
        private StateText _littleTxt;
        private string _littleTemplate;
        private StateText _bigTxt;
        private string _bigTemplate;
        private StateText _blessTxt;
        private string _blessTemplate;

        public KComponentEvent onClose = new KComponentEvent();

        protected override void Awake()
        {
            base.Awake();
            _confirmBtn = GetChildComponent<KButton>("Button_tihuan");
            _commonTxt = GetChildComponent<StateText>("Label_txtCommon");
            _commonTemplate = _commonTxt.CurrentText.text;
            _littleTxt = GetChildComponent<StateText>("Label_txtLittle");
            _littleTemplate = _littleTxt.CurrentText.text;
            _bigTxt = GetChildComponent<StateText>("Label_txtBig");
            _bigTemplate = _bigTxt.CurrentText.text;
            _blessTxt = GetChildComponent<StateText>("Label_txtBless");
            _blessTemplate = _blessTxt.CurrentText.text;

            AddEventListener();
        }

        private void AddEventListener()
        {
            _confirmBtn.onClick.AddListener(OnClose);
        }

        private void OnClose()
        {
            Hide();
            onClose.Invoke();
        }

        public void Show(GemPopData data)
        {
            this.Visible = true;

            _commonTxt.CurrentText.text = string.Format(_commonTemplate, data.param0);
            _littleTxt.CurrentText.text = string.Format(_littleTemplate, data.param1);
            _bigTxt.CurrentText.text = string.Format(_bigTemplate, data.param2);
            _blessTxt.CurrentText.text = string.Format(_blessTemplate, data.param3, data.param4);
        }

        public void Hide()
        {
            this.Visible = false;
        }
    }
}
