﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Utils;
using GameData;
using GameMain.GlobalManager.SubSystem;
using GameMain.GlobalManager;
using UnityEngine;
using Common.Structs;
using ModuleCommonUI;
using Common.Base;
using Common.ServerConfig;
using Common.ExtendComponent;

namespace ModuleGem
{
    public class GemNotEnoughTip : KContainer, IGemPopView
    {
        private KButton _cancelBtn;
        private KButton _confirmBtn;
        private StateText _diamondTxt;
        private KToggle _chooseToggle;
        private IconContainer _diamondIcon;

        private int _param0;
        private int _param1;
        private int _diamondCount;

        public KComponentEvent onClose = new KComponentEvent();

        protected override void Awake()
        {
            _cancelBtn = GetChildComponent<KButton>("Button_quxiao");
            _confirmBtn = GetChildComponent<KButton>("Button_queding");
            _diamondTxt = GetChildComponent<StateText>("Label_txtHuafei");
            _chooseToggle = GetChildComponent<KToggle>("Toggle_Xuanze");
            _diamondIcon = AddChildComponent<IconContainer>("Container_zuanshiIcon");
            _diamondIcon.SetMoneyIcon(public_config.MONEY_TYPE_DIAMOND);
            AddEventListener();
        }

        private void AddEventListener()
        {
            _cancelBtn.onClick.AddListener(OnCloseClick);
            _confirmBtn.onClick.AddListener(OnConfirmClick);
            _chooseToggle.onValueChanged.AddListener(OnSettingValueChanged);
        }

        private void OnCloseClick()
        {
            SavePopSetting(_chooseToggle.isOn);
            Hide();
            onClose.Invoke();
        }

        private void OnConfirmClick()
        {
            if(_param0 != 0 && _param1 != 0)
            {
                GemManager.Instance.UpgradeInEquipAnyway(_param0, _param1);
            }
            else
            {
                GemManager.Instance.CompositeInBagAnyway(_param0);
            }
            _param0 = 0;
            _param1 = 0;
            _diamondCount = 0;
            OnCloseClick();

        }

        private void OnSettingValueChanged(KToggle target, bool value)
        {
            SavePopSetting(value);
        }

        private void SavePopSetting(bool value)
        {
            GemManager.Instance.settingData.ShowGemNotEnoughPop = !value;
        }

        public void Show(GemPopData data)
        {
            this.Visible = true;
            _param0 = data.param0;
            _param1 = data.param1;
            _diamondCount = data.param2;
            _diamondTxt.CurrentText.text = "x" + _diamondCount.ToString();
        }

        public void Hide()
        {
            this.Visible = false;
        }

    }
}
