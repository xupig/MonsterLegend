﻿using Common.Events;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using GameMain.GlobalManager;
using Common.Data;
using Common.ExtendComponent;


namespace ModuleGem
{
    public class GemSpiritCurrencyView : KContainer
    {
        private const int SPIRIT_STONE = 11000; 
        private MoneyItem _goldItem;
        private MoneyItem _spirtStoneItem;


        private StateText _goldTxt;
        private StateText _soulStoneTxt;

        protected override void Awake()
        {
            _goldItem = AddChildComponent<MoneyItem>("Container_buyGold");
            _goldItem.SetMoneyType(public_config.MONEY_TYPE_GOLD);
            _spirtStoneItem = AddChildComponent<MoneyItem>("Container_buyDiamond");
            _spirtStoneItem.SetMoneyType(SPIRIT_STONE);
            _goldTxt = GetChildComponent<StateText>("Container_buyGold/Label_txtGold");
            _soulStoneTxt = GetChildComponent<StateText>("Container_buyDiamond/Label_txtGold");
        }

        private void AddEventListener()
        {
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_diamond, Refresh);
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, OnBagUpdate);
        }

        private void RemoveEventListener()
        {
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_diamond, Refresh);
            EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, OnBagUpdate);
        }

        private void Refresh()
        { 
            _goldTxt.CurrentText.text = string.Concat(" ", PlayerAvatar.Player.money_gold.ToString());
            _soulStoneTxt.CurrentText.text = string.Concat(" ",PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(GemManager.SOUL_STONE_ITEM_ID).ToString());
        }

        private void OnBagUpdate(BagType bagType, uint position)
        {
            if(bagType == BagType.ItemBag)
            {
                Refresh();
            }
        }

        protected override void OnEnable()
        {
            Refresh();
            AddEventListener();
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }
    }
}
