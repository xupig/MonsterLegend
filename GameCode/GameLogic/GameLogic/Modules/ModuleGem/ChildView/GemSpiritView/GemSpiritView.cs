﻿using Common.Events;
using Common.ServerConfig;
using Common.Structs.Enum;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using Common.ExtendComponent;
using UnityEngine;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using ModuleCommonUI;
using Common.Base;
using Common.Utils;

namespace ModuleGem
{
    public class GemSpiritView : KContainer
    {
        private ArtNumberList _levelList;

        private KButton _helpBtn;
        private KButton _oneTimeBtn;
        private KButton _tenTimeBtn;

        private TweenProgressBar _currentBlessValueBar;

        private StateText _costGoldTxt;
        private StateText _costSoulStoneTxt;
        private ItemGrid _goldIcon;
        private ItemGrid _soulStoneIcon;

        private StateText _currentBlessValueTxt;

        private StateText _currentLevelBufTxt;
        private StateText _nextLevelBufTxt;

        private int _playerSpiritLevel;
        private int _playerBlessValue;
        private int _nextLevelBlessValue;
        private KParticle _spiritSuccessFx;

        private const int MAX_SPIRIT_LEVEL = 100;

        protected override void Awake()
        {
            AddChildComponent<GemSpiritCurrencyView>("Container_dibu");

            _levelList = AddChildComponent<ArtNumberList>("Container_dengji/Container_level/List_level");
            _currentBlessValueBar = AddChildComponent<TweenProgressBar>("Container_zhufuzhi/ProgressBar_exp");
            _currentBlessValueBar.SetProgressFx("fx_ui_21_1_jindutiao_01");
            _currentBlessValueTxt = GetChildComponent<StateText>("Container_zhufuzhi/Label_txtjieshi");
            _currentBlessValueBar.SetProgress(_playerBlessValue, _nextLevelBlessValue);

            _currentLevelBufTxt = GetChildComponent<StateText>("Container_Dangqiandengji/Label_txtShuju");
            _nextLevelBufTxt = GetChildComponent<StateText>("Container_Xiajidengji/Label_txtShuju");

            _costSoulStoneTxt = GetChildComponent<StateText>("Container_dancixiaohao/Label_txtLinghunshiValue");
            _costGoldTxt = GetChildComponent<StateText>("Container_dancixiaohao/Label_txtGoldValue");
            _goldIcon = GetChildComponent<ItemGrid>("Container_dancixiaohao/Container_goldIcon/Container_wupin");
            _soulStoneIcon = GetChildComponent<ItemGrid>("Container_dancixiaohao/Container_linghunshiIcon/Container_wupin");
            _goldIcon.SetItemData(item_helper.GetIcon(public_config.MONEY_TYPE_GOLD), item_helper.GetQuality(public_config.MONEY_TYPE_GOLD));
            _soulStoneIcon.SetItemData(item_helper.GetIcon(GemManager.SOUL_STONE_ITEM_ID), item_helper.GetQuality(GemManager.SOUL_STONE_ITEM_ID));

            _helpBtn = GetChildComponent<KButton>("Button_wenhao");
            _oneTimeBtn = GetChildComponent<KButton>("Button_fenglingyici");
            _tenTimeBtn = GetChildComponent<KButton>("Button_fenglingshici");
            _spiritSuccessFx = AddChildComponent<KParticle>("Container_dengji/Container_level/List_level/fx_ui_4_2_2_shanguang_01");
            _spiritSuccessFx.Stop();
            _spiritSuccessFx.transform.position += new Vector3(0, 0, -10);
        }

        private void AddEventListener()
        {
            _oneTimeBtn.onClick.AddListener(OnOneTimeClick);
            _tenTimeBtn.onClick.AddListener(OnTenTimeClick);
            _helpBtn.onClick.AddListener(OnHelpClick);
            EventDispatcher.AddEventListener(GemEvents.SPIRIT_SUCCESS, OnSpiritSuccess);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.spirit_level, OnSpiritLevelUp);
        }

        private void OnSpiritSuccess()
        {
            Refresh();
        }

        private void RemoveEventListener()
        {
            _oneTimeBtn.onClick.RemoveListener(OnOneTimeClick);
            _tenTimeBtn.onClick.RemoveListener(OnTenTimeClick);
            _helpBtn.onClick.RemoveListener(OnHelpClick);
            EventDispatcher.RemoveEventListener(GemEvents.SPIRIT_SUCCESS, OnSpiritSuccess);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.spirit_level, OnSpiritLevelUp);
        }

        private void OnSpiritLevelUp()
        {
            _levelList.SetNumber((int)PlayerAvatar.Player.spirit_level);
            _spiritSuccessFx.Play();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }

        public void Show()
        {
            this.Visible = true;
            AddEventListener();
            Refresh();
        }

        public void Hide()
        {
            if (this.Visible == true)
            {
                RemoveEventListener();
                this.Visible = false;
            }
        }

        private void Refresh()
        {
            _playerSpiritLevel = (int)PlayerAvatar.Player.spirit_level;
            _playerBlessValue = (int)PlayerAvatar.Player.spirit_bless;
            RefreshLevel();
            RefreshCurrentBlessValue();
            RefreshSpiritLevelBufTxt();
            RefreshCostTxt();
        }

        private void RefreshLevel()
        {
            _levelList.SetNumber(_playerSpiritLevel);
        }

        private void RefreshCurrentBlessValue()
        {
            if (_playerSpiritLevel >= MAX_SPIRIT_LEVEL)
            {
                _nextLevelBlessValue = XMLManager.gem_spirit[MAX_SPIRIT_LEVEL].__need_blessing;
                _currentBlessValueTxt.CurrentText.text = _nextLevelBlessValue + "/" + _nextLevelBlessValue;
                _currentBlessValueBar.SetProgress(_nextLevelBlessValue, _nextLevelBlessValue);
                return;
            }
            _nextLevelBlessValue = XMLManager.gem_spirit[_playerSpiritLevel + 1].__need_blessing;
            _currentBlessValueTxt.CurrentText.text = _playerBlessValue + "/" + _nextLevelBlessValue;
            _currentBlessValueBar.SetProgress(_playerBlessValue, _nextLevelBlessValue);
        }

        private void RefreshCostTxt()
        {
            if (_playerSpiritLevel < 100)
            {
                _costGoldTxt.CurrentText.text = "x " + XMLManager.gem_spirit[_playerSpiritLevel].__cost[SpecialItemType.GOLD.ToString()];
                _costSoulStoneTxt.CurrentText.text = "x " + XMLManager.gem_spirit[_playerSpiritLevel].__cost[SpecialItemType.SOULSTONE.ToString()];
            }
            else
            {
                _costGoldTxt.CurrentText.text = "x " + XMLManager.gem_spirit[MAX_SPIRIT_LEVEL - 1].__cost[SpecialItemType.GOLD.ToString()];
                _costSoulStoneTxt.CurrentText.text = "x " + XMLManager.gem_spirit[_playerSpiritLevel - 1].__cost[SpecialItemType.GOLD.ToString()];
            }
        }

        private void RefreshSpiritLevelBufTxt()
        {
            if (_playerSpiritLevel >= MAX_SPIRIT_LEVEL)
            {
                int buf = XMLManager.gem_spirit[MAX_SPIRIT_LEVEL].__spirit_effect / 100;
                _currentLevelBufTxt.CurrentText.text = buf + "%";
                _nextLevelBufTxt.CurrentText.text = buf + "%";
            }
            else
            {
                int buf = XMLManager.gem_spirit[_playerSpiritLevel].__spirit_effect / 100;
                _currentLevelBufTxt.CurrentText.text = buf + "%";
                int nextBuf = XMLManager.gem_spirit[_playerSpiritLevel + 1].__spirit_effect / 100;
                _nextLevelBufTxt.CurrentText.text = nextBuf + "%";
            }
        }

        private void OnOneTimeClick()
        {
            const int CHINESE_ID = 34204;
            if (PlayerAvatar.Player.spirit_level >= MAX_SPIRIT_LEVEL)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(CHINESE_ID), PanelIdEnum.Gem);
                return;
            }
            GemManager.Instance.SpriteOneTime();
        }

        private void OnTenTimeClick()
        {
            const int CHINESE_ID = 34204;
            if (PlayerAvatar.Player.spirit_level >= MAX_SPIRIT_LEVEL)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(CHINESE_ID), PanelIdEnum.Gem);
                return;
            }
            if (GemManager.Instance.settingData.ShowSpiritTenTimePop == true)
            {
                EventDispatcher.TriggerEvent(GemEvents.SHOW_GEM_SPIRIT_TIP);
            }
            else
            {
                GemManager.Instance.SpiritTenTime();
            }
        }

        private void OnHelpClick()
        {
            EventDispatcher.TriggerEvent(GemEvents.SHOW_GEM_SPIRIT_HELP);
        }
    }
}
