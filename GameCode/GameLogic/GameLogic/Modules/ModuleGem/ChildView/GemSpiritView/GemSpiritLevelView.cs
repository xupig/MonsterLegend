﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Game.UI.UIComponent;
using GameMain.Entities;


namespace ModuleGem
{
    public class GemSpiritLevelView : KContainer
    {
        private List<GameObject> _digitalList0;
        private List<GameObject> _digitalList1;
        private List<GameObject> _digitalList2;

        protected override void Awake()
        {
            _digitalList0 = GetDigitalList(GetChild("Container_shuzi0"));
            _digitalList1 = GetDigitalList(GetChild("Container_shuzi1"));
            _digitalList2 = GetDigitalList(GetChild("Container_shuzi2"));
        }

        private List<GameObject> GetDigitalList(GameObject parent)
        {
            List<GameObject> result = new List<GameObject>();
            for(int i = 0; i < 10; i++)
            {
                result.Add(parent.transform.FindChild("Image_sharedlianji" + i).gameObject);
            }
            return result;
        }

        private void HideDigitalList(List<GameObject> digitalList)
        {
            for(int i = 0; i < digitalList.Count; i++)
            {
                digitalList[i].SetActive(false);
            }
        }

        public void SetLevel(int level)
        {
            if (level >= 100)
            {
                HideDigitalList(_digitalList0);
                HideDigitalList(_digitalList1);
                HideDigitalList(_digitalList2);
                _digitalList0[1].SetActive(true);
                _digitalList1[0].SetActive(true);
                _digitalList2[0].SetActive(true);
                return;
            }
            HideDigitalList(_digitalList2);
            int digital0 = level / 10;
            int digital1 = level % 10;
            HideDigitalList(_digitalList0);
            HideDigitalList(_digitalList1);
            _digitalList0[digital0].SetActive(true);
            _digitalList1[digital1].SetActive(true);
        }
    }
}
