﻿using Common.Base;
using System.Collections.Generic;

namespace ModuleGem
{
    public class GemPopPanel : BasePanel
    {
        private GemNotEnoughTip _notEnoughTip;
        private GemSpiritTenTimeTip _tenTimeTip;
        private GemSpiritHelpTip _spiritHelp;
        private GemSpiritResultTip _spiritResult;
        private GemCompositeCostTip _compositeCostTip;
        private GemQuickInlayTip _quickInlayTip;

        private List<IGemPopView> _viewList = new List<IGemPopView>();

        protected override void Awake()
        {
            base.Awake();
            _notEnoughTip = AddChildComponent<GemNotEnoughTip>("Container_cailiaobugouTip");
            _tenTimeTip = AddChildComponent<GemSpiritTenTimeTip>("Container_tips");
            _spiritHelp = AddChildComponent<GemSpiritHelpTip>("Container_fenglingHelp");
            _spiritResult = AddChildComponent<GemSpiritResultTip>("Container_fenglingResult");
            _compositeCostTip = AddChildComponent<GemCompositeCostTip>("Container_hechengxiaohao");
            _quickInlayTip = AddChildComponent<GemQuickInlayTip>("Container_kuaijiexiangqian");

            _viewList.Add(_notEnoughTip);
            _viewList.Add(_tenTimeTip);
            _viewList.Add(_spiritHelp);
            _viewList.Add(_spiritResult);
            _viewList.Add(_compositeCostTip);
            _viewList.Add(_quickInlayTip);

            AddEventListener();
        }

        private void AddEventListener()
        {
            _notEnoughTip.onClose.AddListener(OnSubViewClose);
            _tenTimeTip.onClose.AddListener(OnSubViewClose);
            _spiritHelp.onClose.AddListener(OnSubViewClose);
            _spiritResult.onClose.AddListener(OnSubViewClose);
            _compositeCostTip.onClose.AddListener(OnSubViewClose);
            _quickInlayTip.onClose.AddListener(OnSubViewClose);
        }

        private void OnSubViewClose()
        {
            ClosePanel();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GemPop; }
        }

        public override void OnShow(object data)
        {
            for(int i = 0; i < _viewList.Count; i++)
            {
                _viewList[i].Hide();
            }
            SetData(data);
        }

        public override void SetData(object data)
        {
            GemPopData popData = data as GemPopData;
            _viewList[popData.viewIndex].Show(popData);
        }

        public override void OnClose()
        {
            
        }
    }
}
