﻿#region 模块信息
/*==========================================
// 文件名：GemGrid
// 命名空间: GameLogic.GameLogic.Modules.ModuleGem.ChildComponent
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/7/28 10:14:58
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using Game.UI.UIComponent;
using GameData;

namespace ModuleGem
{
    public class GemGrid : KList.KListItemBase
    {
        private StateText _numTxt;
        private KContainer _icon;
        private KContainer _iconUnderlay;
        private GemGridData _data;

        protected override void Awake()
        {
            _numTxt = GetChildComponent<StateText>("Label_shuliang");
            _icon = GetChildComponent<KContainer>("Container_icon");
            _iconUnderlay = GetChildComponent<KContainer>("Container_underlay");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as GemGridData;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            SetCount();
            RefreshIcon();
            RefreshIconUnderlay();
        }

        private void SetCount()
        {
            _numTxt.CurrentText.text = _data.num.ToString();
        }

        private void RefreshIcon()
        {
            if (_data.isGray == true)
            {
                MogoAtlasUtils.AddSprite(_icon.gameObject, icon_item_helper.GetIcon(_data.id), icon_item_helper.GetIconBg(_data.id), 0);
            }
            else
            {
                MogoAtlasUtils.AddIcon(_icon.gameObject, item_helper.GetIcon(_data.id));
            }
        }

        private void RefreshIconUnderlay()
        {
            int gemType = item_gem_helper.GetGemSubtype(_data.id);
            if (_data.isBackGroundGray == true)
            {
                MogoAtlasUtils.AddIcon(_iconUnderlay.gameObject, item_gem_helper.GetUnderlayByGemType(0));
            }
            else
            {
                MogoAtlasUtils.AddIcon(_iconUnderlay.gameObject, item_gem_helper.GetUnderlayByGemType(gemType));
            }
        }


        public override void Dispose()
        {
            
        }
    }
}