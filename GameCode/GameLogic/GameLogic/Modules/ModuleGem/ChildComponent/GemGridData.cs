﻿
namespace ModuleGem
{
    public class GemGridData
    {
        public int id;
        public int num;
        public bool isGray;
        public bool isBackGroundGray;

        public GemGridData(int id, int num, bool isGray = false, bool isBackGroundGray = false)
        {
            this.id = id;
            this.num = num;
            this.isGray = isGray;
            this.isBackGroundGray = isBackGroundGray;
        }
    }
}
