﻿using Common.Base;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System.Collections.Generic;
using MogoEngine.Events;
using Common.Events;
using Common.Structs.ProtoBuf;
using GameData;
using Common.Data;

namespace ModuleGem
{
    public class GemPanel : BasePanel
    {
        private GemInlayView _inlayView;
        private GemSpiritView _spiritView;
        private GemCompositeView _compositeView;

        private KToggleGroup _tab;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Gem; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _inlayView = AddChildComponent<GemInlayView>("Container_xiangqian");
            _spiritView = AddChildComponent<GemSpiritView>("Container_fengling");
            _compositeView = AddChildComponent<GemCompositeView>("Container_hecheng");
            _tab = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            _tab.onSelectedIndexChanged.AddListener(OnTabIndexChanged);
            SetTabList(13, _tab);
        }

        public override void OnShow(object data)
        {
            if (data is int)
            {
                int index = 0;
                index = (int)data;
                _tab.SelectIndex = index;
                ShowTab(_tab.SelectIndex);
            }
            else if (data is int[])
            {
                int[] indexs = data as int[];
                int tabIndex = indexs[0];
                int categoryIndex = indexs[1];
                _tab.SelectIndex = tabIndex;
                ShowTab(tabIndex, categoryIndex);
            }
            else
            {
                _tab.SelectIndex = 0;
                ShowTab(_tab.SelectIndex);
            }
            InitTabPoint();
            AddEventListener();
        }

        private void InitTabPoint()
        {
            KToggle inlayToggle = _tab.GetChildComponent<KToggle>("Toggle_xiangqian");
            KToggle compositeToggle = _tab.GetChildComponent<KToggle>("Toggle_hecheng");
            KToggle spiritToggle = _tab.GetChildComponent<KToggle>("Toggle_fengling");
            inlayToggle.SetGreenPoint(item_gem_helper.HasGemToInlay());
            compositeToggle.SetGreenPoint(item_gem_helper.HasGemToComposite());
            spiritToggle.SetGreenPoint(item_gem_helper.CanSpirit());
        }

        public override void OnClose()
        {
            GemManager.Instance.ResetSettingData();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<int, int, int>(GemEvents.SHOW_GEM_NOT_ENOUGH_TIP, OnShowNotEnoughTip);
            EventDispatcher.AddEventListener(GemEvents.SHOW_GEM_SPIRIT_TIP, OnShowSpiritTip);
            EventDispatcher.AddEventListener(GemEvents.SHOW_GEM_SPIRIT_HELP, OnShowSpiritHelp);
            EventDispatcher.AddEventListener<PbGemSpiritInfo>(GemEvents.SHOW_SPIRIT_RESULT, OnShowSpiritResult);
            EventDispatcher.AddEventListener<int, Dictionary<int, int>>(GemEvents.SHOW_GEM_COMPOSITE_COST, OnShowGemCompositeCost);
            EventDispatcher.AddEventListener(GemEvents.GemPanelRefreshTabSpotLight, InitTabPoint);
            EventDispatcher.AddEventListener<int>(BagEvents.AddItem, OnGetNewItem);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<int, int, int>(GemEvents.SHOW_GEM_NOT_ENOUGH_TIP, OnShowNotEnoughTip);
            EventDispatcher.RemoveEventListener(GemEvents.SHOW_GEM_SPIRIT_TIP, OnShowSpiritTip);
            EventDispatcher.RemoveEventListener(GemEvents.SHOW_GEM_SPIRIT_HELP, OnShowSpiritHelp);
            EventDispatcher.RemoveEventListener<PbGemSpiritInfo>(GemEvents.SHOW_SPIRIT_RESULT, OnShowSpiritResult);
            EventDispatcher.RemoveEventListener<int, Dictionary<int, int>>(GemEvents.SHOW_GEM_COMPOSITE_COST, OnShowGemCompositeCost);
            EventDispatcher.RemoveEventListener(GemEvents.GemPanelRefreshTabSpotLight, InitTabPoint);
            EventDispatcher.RemoveEventListener<int>(BagEvents.AddItem, OnGetNewItem);
        }

        private void OnGetNewItem(int itemId)
        {
            if (item_helper.GetItemType(itemId) == BagItemType.Gem)
            {
                InitTabPoint();
            }
        }

        private void OnShowGemCompositeCost(int gemId, Dictionary<int, int> costDict)
        {
            GemPopData popData = new GemPopData();
            popData.viewIndex = 4;
            popData.param0 = gemId;
            popData.costDict = costDict;
            UIManager.Instance.ShowPanel(PanelIdEnum.GemPop, popData);
        }

        private void OnBuy()
        {
            view_helper.OpenView(62);
        }

        private void OnShowNotEnoughTip(int equipPosition, int slotPosition, int diamondCount)
        {
            GemPopData popData = new GemPopData();
            popData.viewIndex = 0;
            popData.param0 = equipPosition;
            popData.param1 = slotPosition;
            popData.param2 = diamondCount;
            UIManager.Instance.ShowPanel(PanelIdEnum.GemPop, popData);
        }

        private void OnShowSpiritTip()
        {
            GemPopData popData = new GemPopData();
            popData.viewIndex = 1;
            UIManager.Instance.ShowPanel(PanelIdEnum.GemPop, popData);
        }

        private void OnShowSpiritHelp()
        {
            GemPopData popData = new GemPopData();
            popData.viewIndex = 2;
            UIManager.Instance.ShowPanel(PanelIdEnum.GemPop, popData);
        }

        private void OnShowSpiritResult(PbGemSpiritInfo pbGemSpiritInfo)
        {
            GemPopData popData = new GemPopData();
            popData.viewIndex = 3;
            popData.param0 = (int)pbGemSpiritInfo.common_bless_num;
            popData.param1 = (int)pbGemSpiritInfo.little_hit_bless_num;
            popData.param2 = (int)pbGemSpiritInfo.big_hit_bless_num;
            int spiritLevel = (int)PlayerAvatar.Player.spirit_level;
            popData.param3 = item_gem_helper.GetSpiritBlessImprove(spiritLevel, 0) * 10;
            popData.param4 = (int)pbGemSpiritInfo.all_bless - popData.param3;
            UIManager.Instance.ShowPanel(PanelIdEnum.GemPop, popData);
        }

        private void OnTabIndexChanged(KToggleGroup target, int index)
        {
            ShowTab(index);
        }

        private void ShowTab(int index)
        {
            switch (index)
            {
                case 0:
                    _inlayView.Show();
                    _compositeView.Hide();
                    _spiritView.Hide();
                    break;
                case 1:
                    _inlayView.Hide();
                    _compositeView.Show();
                    _spiritView.Hide();
                    break;
                case 2:
                    _compositeView.Hide();
                    _inlayView.Hide();
                    _spiritView.Show();
                    break;
            }
        }

        private void ShowTab(int tabIndex, int categoryIndex)
        {
            switch (tabIndex)
            {
                case 0:
                    _inlayView.Show(categoryIndex);
                    _compositeView.Hide();
                    _spiritView.Hide();
                    break;
                case 1:
                    _inlayView.Hide();
                    _compositeView.Show();
                    _spiritView.Hide();
                    break;
                case 2:
                    _inlayView.Hide();
                    _compositeView.Hide();
                    _spiritView.Show();
                    break;
            }
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

    }
}
