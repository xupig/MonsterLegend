﻿using Common.Base;
using Common.Data;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System.Collections.Generic;
using UnityEngine;


namespace ModuleTreasure
{
    public class TreasureEntryPanel : BasePanel
    {
        private StateText _descText;
        private StateText _countText;
        private KButton _goButton;
        private StateText _notOpenText;
        private KButton _rewardBtn;

        private KContainer _chestContainer;
        private KContainer _duelChestContainer;

        private int _activityId;
        private TreasureBoxType _type;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _descText = GetChildComponent<StateText>("Container_ditu/Label_txtXinxi");
            _countText = GetChildComponent<StateText>("Container_bottom/Label_txtCishu");
            _goButton = GetChildComponent<KButton>("Container_bottom/Button_Xianzaijiuqu");
            _notOpenText = GetChildComponent<StateText>("Container_bottom/Label_txtHaiweidao");
            _rewardBtn = GetChildComponent<KButton>("Button_jiangliyulan");
            _chestContainer = GetChildComponent<KContainer>("Container_ditu/Container_yijiebaoxiang");
            _duelChestContainer = GetChildComponent<KContainer>("Container_ditu/Container_juedoubaoxiang");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TreasureEntry; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        public override void SetData(object data)
        {
            if (data is List<string>)
            {
                _activityId = int.Parse((data as List<string>)[0]);
            }
            else if(data is int)
            {
                _activityId = (int)data;
            }
            _type = chest_data_helper.GetTreasureTypeByActivityId(_activityId);
            RefreshContent();
            RefreshBackground();
        }

        private void RefreshBackground()
        {
            if (_type == TreasureBoxType.Chest)
            {
                _chestContainer.Visible = true;
                _duelChestContainer.Visible = false;
            }
            else if (_type == TreasureBoxType.DuelChest)
            {
                _chestContainer.Visible = false;
                _duelChestContainer.Visible = true;
            }
            else
            {
                _chestContainer.Visible = false;
                _duelChestContainer.Visible = false;
            }
        }
     
        private void AddEventListener()
        {
            _goButton.onClick.AddListener(OnGoButtonClick);
            _rewardBtn.onClick.AddListener(OnRewardBtnClick);
        }

        private void RemoveEventListener()
        {
            _goButton.onClick.RemoveListener(OnGoButtonClick);
            _rewardBtn.onClick.RemoveListener(OnRewardBtnClick);
        }

        private void OnRewardBtnClick()
        {
            PanelIdEnum.TreasureRewardPreview.Show(_type);
        }

        private void OnGoButtonClick()
        {
            if (_type == TreasureBoxType.Chest)
            {
                PanelIdEnum.FieldWorldMap.Show();
            }
            else if (_type == TreasureBoxType.DuelChest)
            {
                DuelManager.Instance.RequestQueryInfo();
            }
        }

        private void RefreshContent()
        {
            if (_type == TreasureBoxType.Chest)
            {
                int totalCount = int.Parse(vip_rights_helper.GetVipRights(VipRightType.ChestTimes, PlayerAvatar.Player.vip_level));
                _countText.CurrentText.text = string.Format("{0}", totalCount - (int)PlayerAvatar.Player.chest_times);
            }
            else if (_type == TreasureBoxType.DuelChest)
            {
                int totalCount = int.Parse(vip_rights_helper.GetVipRights(VipRightType.DuelChestTimes, PlayerAvatar.Player.vip_level));
                _countText.CurrentText.text = string.Format("{0}", totalCount - (int)PlayerAvatar.Player.duelchest_times);
            }
            else
            {
                _countText.CurrentText.text = string.Empty;
            }
            if (activity_helper.IsOpen(_activityId))
            {
                _goButton.Visible = true;
                _notOpenText.Visible = false;
            }
            else
            {
                _goButton.Visible = false;
                _notOpenText.Visible = true;
            }
            RefreshDescContent();
        }

        private void RefreshDescContent()
        {
            LimitActivityData activityData = activity_helper.GetActivity(_activityId);
            string content = string.Empty;
            for (int i = 0; i < activityData.openTimeStart.Count; i++)
            {
                string time = string.Format("{0}-{1}", MogoTimeUtil.ToUniversalTime(activityData.openTimeStart[i], "hh:mm"), MogoTimeUtil.ToUniversalTime(activityData.openTimeEnd[i], "hh:mm"));
                if (i != 0)
                {
                    content = string.Concat(content, MogoLanguageUtil.GetContent(102514));
                }
                content = string.Concat(content, time);
            }
            if (_type == TreasureBoxType.Chest)
            {
                _descText.CurrentText.text = MogoLanguageUtil.GetContent(99130, content);
            }
            else if (_type == TreasureBoxType.DuelChest)
            {
                _descText.CurrentText.text = MogoLanguageUtil.GetContent(122128, content);
            }
            else
            {
                _descText.CurrentText.text = string.Empty;
            }
        }


    }
}
