﻿using Common.Base;

namespace ModuleTreasure
{
    public class TreasureModule : BaseModule
    {
        public TreasureModule()
        {

        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.TreasureEntry, "Container_TreasureEntryPanel", MogoUILayer.LayerUIPanel, "ModuleTreasure.TreasureEntryPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.TreasureRewardPreview, "Container_TreasureRewardPreviewPanel", MogoUILayer.LayerUIPanel, "ModuleTreasure.TreasureRewardPreviewPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.TreasureReward, "Container_TreasureRewardPanel", MogoUILayer.LayerUIPanel, "ModuleTreasure.TreasureRewardPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.TreasureMapUse, "Container_TreasureMapUsePanel", MogoUILayer.LayerUnderPanel, "ModuleTreasure.TreasureMapUsePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.PortalInstance, "Container_PortalInstancePanel", MogoUILayer.LayerUIPanel, "ModuleTreasure.PortalInstancePanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.CombatPortalInstance, "Container_CombatPortalInstancePanel", MogoUILayer.LayerUIPanel, "ModuleTreasure.CombatPortalInstancePanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.PortalFight, "Container_PortalFightPanel", MogoUILayer.LayerUnderPanel, "ModuleTreasure.PortalFightPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }
    }
}
