﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using ModuleTeamInstance;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTreasure
{
    public class PortalInstanceEntryView : KContainer
    {
        private int _instanceId;
        private int _portalId;
        private KButton _inviteButton;
        private KButton _callButton;
        private KButton _entryButton;
        private PortalInstanceConditionView _conditionContainer;
        private KScrollView _itemScrollView;
        private KList _itemList;
        private CopyTeamView _teamView;
        private CopyPassConditionList _passView;
        private KContainer _rightContainer;
        private TeamRewardView _teamRewardView;

        protected override void Awake()
        {
            _rightContainer = gameObject.GetComponent<KContainer>();
            _teamView = AddChildComponent<CopyTeamView>("Container_duiyou");
            _inviteButton = GetChildComponent<KButton>("Container_bottomBtn/Button_yaoqing");
            _callButton = GetChildComponent<KButton>("Container_bottomBtn/Button_hanren");
            _entryButton = GetChildComponent<KButton>("Container_bottomBtn/Button_tiaozhan");
            _conditionContainer = AddChildComponent<PortalInstanceConditionView>("Container_tiaojian");
            _passView = AddChildComponent<CopyPassConditionList>("Container_tongguan");
            _itemScrollView = GetChildComponent<KScrollView>("Container_zhuangbei/ScrollView_wupinliebiao");
            _teamRewardView = AddChildComponent<TeamRewardView>("Container_tuanduijiangli");
            InitScrollView();
        }

        public void RefreshContent(object data)
        {
            if (data is int[])
            {
                int[] arr = data as int[];
                if (arr.Length == 2)
                {
                    _portalId = arr[0];
                    _instanceId = arr[1];
                    RefreshContent();
                    TweenViewMove.Begin(_rightContainer.gameObject, MoveType.Show, MoveDirection.Right);
                }
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void OnCallPeopelSucceed()
        {
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83026), PanelIdEnum.MainUIField);
        }

        private void InitScrollView()
        {
            _itemList = _itemScrollView.GetChildComponent<KList>("mask/content");
            _itemList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue);
            _itemScrollView.ScrollRect.horizontal = true;
            _itemScrollView.ScrollRect.vertical = false;
            _itemScrollView.UpdateArrow();
            _itemList.SetGap(0, 28);
        }

        private void RefreshContent()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            _conditionContainer.RefreshContent(_instanceId);
            _passView.Refresh(_instanceId);
            RefreshGetView();
            _teamView.RefreshContent(_instanceId);
            int maxNum = instance_helper.GetMaxPlayerNum(ins);
            _callButton.Visible = maxNum > 1;
            if (_teamRewardView != null)
            {
                _teamRewardView.RefreshContent(_instanceId);
            }
        }


        private void RefreshGetView()
        {
            _itemList.RemoveAll();
            List<int> rewardIdList = instance_reward_helper.GetRewardIdListByRewardLevel(_instanceId, RewardLevel.S);
            List<RewardData> rewardDataList = item_reward_helper.GetRewardDataList(rewardIdList, PlayerAvatar.Player.vocation);
            rewardDataList = RewardData.FilterSameItemId(rewardDataList);
            _itemList.SetDataList<RewardGrid>(rewardDataList);
        }


        public void AddEventListener()
        {
            _inviteButton.onClick.AddListener(OnInviteButtonClick);
            _callButton.onClick.AddListener(OnCallButtonClick);
            _entryButton.onClick.AddListener(OnEntryButtonClick);
            EventDispatcher.AddEventListener(TeamInstanceEvents.On_Call_People_Succeed, OnCallPeopelSucceed);
        }

        public void RemoveEventListener()
        {
            _inviteButton.onClick.RemoveListener(OnInviteButtonClick);
            _callButton.onClick.RemoveListener(OnCallButtonClick);
            _entryButton.onClick.RemoveListener(OnEntryButtonClick);
            EventDispatcher.RemoveEventListener(TeamInstanceEvents.On_Call_People_Succeed, OnCallPeopelSucceed);
        }

        private void OnEntryButtonClick()
        {
            CopyLogicManager.Instance.RequestEnterCopy(ChapterType.TeamCopy, () =>
            {
                TreasureManager.Instance.EnterPortalMission((uint)_portalId);
            }, _instanceId);
        }

        private void OnCallButtonClick()
        {
            CopyLogicManager.Instance.RequestCallPeople(ChapterType.TeamCopy, () =>
            {
                TeamInstanceManager.Instance.CallPeople(team_helper.GetGameIdByInstanceId(_instanceId));
            }, _instanceId);
        }

        private void OnInviteButtonClick()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int maxNum = instance_helper.GetMaxPlayerNum(ins);
            if (PlayerDataManager.Instance.TeamData.TeammateDic.Count >= maxNum)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83031), PanelIdEnum.MainUIField);
                return;
            }
            view_helper.OpenView(112, new InteractivePanelParameter(_instanceId));
        }

    }
}
