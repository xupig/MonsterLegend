﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System.Collections.Generic;
using UnityEngine;


namespace ModuleTreasure
{
    public class TreasureRewardPreviewItem : KList.KListItemBase
    {
        private int _chestId;

        private StateText _titleTxt;
        private KList _itemList;
        private IconContainer _icon;

        protected override void Awake()
        {
            _titleTxt = GetChildComponent<StateText>("Label_txtKnengjiangli");
            _itemList = GetChildComponent<KList>("List_jiangli");
            _icon = AddChildComponent<IconContainer>("Container_icon");
            _itemList.SetPadding(0, 15, 0, 15);
            _itemList.SetGap(15, 30);
            _itemList.SetDirection(KList.KListDirection.LeftToRight, 8, 3);
        }

        public override object Data
        {
            get
            {
                return _chestId;
            }
            set
            {
                _chestId = (int)value;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            _icon.SetIcon(chest_data_helper.GetChestIcon(_chestId));
            if (chest_data_helper.GetTreasureTypeByChestId(_chestId) == TreasureBoxType.Chest)
            {
                _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(99297 + 1 - chest_data_helper.GetChestType(_chestId));
            }
            else if (chest_data_helper.GetTreasureTypeByChestId(_chestId) == TreasureBoxType.DuelChest)
            {
                _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(122125 + 4 - chest_data_helper.GetChestType(_chestId));
            }
            else
            {
                _titleTxt.CurrentText.text = string.Empty;
            }
            List<RewardData> rewardDataList = item_reward_helper.GetRewardDataList(chest_data_helper.GetChestRewardId(_chestId), PlayerAvatar.Player.vocation);
            _itemList.SetDataList<RewardGridBase>(rewardDataList, 4);
        }


        public override void Dispose()
        {

        }
    }
}
