﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTreasure
{
    public class PortalInstancePanel : BasePanel
    {
        private int _instanceId;
        private PortalInstanceEntryView _entryViewOne;
        private PortalInstanceEntryView _entryViewTwo;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _entryViewOne = AddChildComponent<PortalInstanceEntryView>("Container_right01");
            _entryViewTwo = AddChildComponent<PortalInstanceEntryView>("Container_right02");
        }


        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.PortalInstance; }
        }

        public override void OnShow(object data)
        {
            if (data is int[])
            {
                int[] arr = data as int[];
                if (arr.Length == 2)
                {
                    _instanceId = arr[1];
                    List<int> rewardIdList = instance_reward_helper.GetRewardIdListByRewardLevel(_instanceId, RewardLevel.Team);
                    if (rewardIdList.Count == 0)
                    {
                        _entryViewOne.Visible = true;
                        _entryViewTwo.Visible = false;
                        _entryViewOne.RefreshContent(data);
                    }
                    else
                    {
                        _entryViewOne.Visible = false;
                        _entryViewTwo.Visible = true;
                        _entryViewTwo.RefreshContent(data);
                    }
                }
            }
        }

        public override void OnClose()
        {

        }

       

    }
}
