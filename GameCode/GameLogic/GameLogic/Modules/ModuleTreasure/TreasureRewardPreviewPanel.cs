﻿using Common.Base;
using Common.Data;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System.Collections.Generic;


namespace ModuleTreasure
{
    public class TreasureRewardPreviewPanel : BasePanel
    {
        private KScrollView _scrollView;
        private KList _list;
        private TreasureBoxType _type;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _scrollView = GetChildComponent<KScrollView>("Container_paihangbang/ScrollView_zhanlibang");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TreasureRewardPreview; }
        }

        public override void OnShow(object data)
        {
            SetData(data);
        }

        public override void OnClose()
        {

        }

        public override void SetData(object data)
        {
            _type = (TreasureBoxType)data;
            RefreshContent();
        }

        private void RefreshContent()
        {
            List<int> chestList;
            chestList = chest_data_helper.GetChestRewardIdList(_type);
            _list.SetDataList<TreasureRewardPreviewItem>(chestList, 3);
        }
      

    }
}
