﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTreasure
{
    public class CombatPortalInstancePanel : BasePanel
    {
        private int _instanceId;
        private uint _entityId;
        private StateText _nameText;
        private KButton _recordButton;
        private KButton _entryButton;
        private KButton _callButton;
        private CombatPortalConditionView _conditionContainer;
        private KScrollView _itemScrollView;
        private KList _itemList;
        private KContainer _leftContainer;
        private KContainer _rightContainer;
        private IconContainer _containerIcon;
        private StateText _descText;
        private StateText _descTitle;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _leftContainer = GetChildComponent<KContainer>("Container_left");
            _rightContainer = GetChildComponent<KContainer>("Container_right");
            _nameText = GetChildComponent<StateText>("Container_left/Container_biaoti/Label_txtFuben");
            _recordButton = GetChildComponent<KButton>("Container_right/Container_upperBtn/Button_wenhao");
            _recordButton.Visible = false;
            _conditionContainer = AddChildComponent<CombatPortalConditionView>("Container_right/Container_word/Container_tiaozhanyaoqiu/Container_command/Container_tiaozhan");
            _entryButton = GetChildComponent<KButton>("Container_right/Container_bottomBtn/Container_saodang/Button_tiaozhanPutong");
            _itemScrollView = GetChildComponent<KScrollView>("Container_right/Container_word/Container_zhuangbei/ScrollView_wupinliebiao");
            _containerIcon = AddChildComponent<IconContainer>("Container_left/Container_mijing");
            InitScrollView();
            KContainer teamContainer = GetChildComponent<KContainer>("Container_left/Container_duoren");
            teamContainer.Visible = false;
            _callButton = GetChildComponent<KButton>("Container_right/Container_bottomBtn/Container_saodang/Button_saodang");
            KContainer descritionContainer = GetChildComponent<KContainer>("Container_right/Container_word/Container_xuyaoxiaohao/Container_miaoshu");
            descritionContainer.Visible = true;
            _descText = GetChildComponent<StateText>("Container_right/Container_word/Container_xuyaoxiaohao/Container_miaoshu/Label_txtNr");
            _descTitle = GetChildComponent<StateText>("Container_right/Container_word/Container_xuyaoxiaohao/Label_txtXuyaoxiaohao");
        }


        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.CombatPortalInstance; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            if (data is int[])
            {
                int[] arr = data as int[];
                if (arr.Length == 2)
                {
                    _entityId = (uint)arr[0];
                    _instanceId = arr[1];
                    RefreshContent();
                    TweenViewMove.Begin(_leftContainer.gameObject, MoveType.Show, MoveDirection.Left);
                    TweenViewMove.Begin(_rightContainer.gameObject, MoveType.Show, MoveDirection.Right);
                }
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void InitScrollView()
        {
            _itemList = _itemScrollView.GetChildComponent<KList>("mask/content");
            _itemList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue);
            _itemScrollView.ScrollRect.horizontal = true;
            _itemScrollView.ScrollRect.vertical = false;
            _itemScrollView.UpdateArrow();
            _itemList.SetGap(0, 28);
        }

        private void RefreshContent()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            _nameText.CurrentText.text = instance_helper.GetInstanceName(_instanceId);
            _conditionContainer.RefreshContent(_entityId, _instanceId);
            _containerIcon.SetIcon(instance_helper.GetIcon(_instanceId));
            RefreshGetView();
            RefreshPassView();
        }

        private void RefreshPassView()
        {
            _descTitle.CurrentText.text = MogoLanguageUtil.GetContent(102414);
            EntityCombatPortal portal = MogoWorld.GetEntity(_entityId) as EntityCombatPortal;
            if (portal != null)
            {
                _descText.CurrentText.text = transmit_portal_helper.GetPortalDesc(portal.portal_id);
            }
        }

        private void RefreshGetView()
        {
            _itemList.RemoveAll();

            List<RewardData> rewardDataList = night_activity_reward_helper.GetRewardList(_instanceId, PlayerAvatar.Player.level, PlayerAvatar.Player.vocation);
            rewardDataList = RewardData.FilterSameItemId(rewardDataList);
            _itemList.SetDataList<RewardGrid>(rewardDataList);
        }


        public void AddEventListener()
        {
            _entryButton.onClick.AddListener(OnEntryButtonClick);
            _recordButton.onClick.AddListener(OnRecordButtonClick);
            _callButton.onClick.AddListener(OnCallButtonClick);
        }

        public void RemoveEventListener()
        {
            _entryButton.onClick.RemoveListener(OnEntryButtonClick);
            _recordButton.onClick.RemoveListener(OnRecordButtonClick);
            _callButton.onClick.RemoveListener(OnCallButtonClick);
        }

        private void OnCallButtonClick()
        {
            TreasureManager.Instance.SharePositionToWorld(MogoLanguageUtil.GetContent(102445), GameSceneManager.GetInstance().curMapID, (int)PlayerAvatar.Player.curLine, PlayerAvatar.Player.position);
        }

        private void OnEntryButtonClick()
        {
            if (CheckMeetCondition())
            {
                if (PlayerDataManager.Instance.TeamData.TeammateDic.Count > 0)
                {
                    MessageBox.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(102442), () =>
                    {
                        OnCallButtonClick();
                        TreasureManager.Instance.EnterCombatPortalMission(_entityId, (uint)_instanceId);
                    });
                    return;
                }
                OnCallButtonClick();
                TreasureManager.Instance.EnterCombatPortalMission(_entityId, (uint)_instanceId);
            }
        }

        private bool CheckMeetCondition()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int minLevel = instance_helper.GetMinLevel(ins);
            if (PlayerAvatar.Player.level < minLevel)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83027, minLevel), PanelIdEnum.MainUIField);
                return false;
            }
            return true;
        }

        private void OnRecordButtonClick()
        {

        }

    }
}