﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTreasure
{
    public class PortalInstanceConditionView : KContainer
    {
        private StateText _numberText;
        private StateText _levelText;
        private int _instanceId;

        protected override void Awake()
        {
            _numberText = GetChildComponent<StateText>("Container_NR01/Label_txtJinrudengji");
            _levelText = GetChildComponent<StateText>("Container_NR00/Label_txtJinrudengji");
        }


        public void RefreshContent(int instId)
        {
            _instanceId = instId;
            RefreshLevelConditionContent();
            RefreshNumberConditionContent();
        }


        private void RefreshNumberConditionContent()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int minNum = instance_helper.GetMinPlayerNum(ins);
            int maxNum = instance_helper.GetMaxPlayerNum(ins);
            bool meet = (minNum <= 1 || PlayerDataManager.Instance.TeamData.TeammateDic.Count >= minNum);
            string num = meet ? minNum.ToString() : ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, minNum.ToString());
            if (minNum == maxNum)
            {
                _numberText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(83015), ":", MogoLanguageUtil.GetContent(49215, num));
            }
            else
            {
                _numberText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(83015), ":", MogoLanguageUtil.GetContent(83018, num, maxNum));
            }
        }

        private void RefreshLevelConditionContent()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int minLevel = instance_helper.GetMinLevel(ins);
            int maxLevel = instance_helper.GetMaxLevel(ins);
            string num = PlayerAvatar.Player.level >= minLevel ? minLevel.ToString() : ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, minLevel.ToString());
            _levelText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(83013), ":", MogoLanguageUtil.GetContent(83020, num));
        }

        public void AddEventListener()
        {
            EventDispatcher.AddEventListener(TeamEvent.TEAM_REFRESH, RefreshNumberConditionContent);
        }

        public void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_REFRESH, RefreshNumberConditionContent);
        }


        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

    }
}
