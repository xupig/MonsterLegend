﻿using ACTSystem;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;


namespace ModuleTreasure
{
    public class TreasureRewardItem : KList.KListItemBase
    {
        private ItemGrid _itemGrid;
        private StateImage _checkmarkImage;
        private StateText _countText;

        private RewardData _data;

        protected override void Awake()
        {
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _checkmarkImage = GetChildComponent<StateImage>("Image_checkmark");
            _countText = GetChildComponent<StateText>("Label_txtNum");
            _checkmarkImage.Visible = false;
        }

        public void ToggleCheckmark(bool isOn)
        {
            _checkmarkImage.Visible = isOn;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as RewardData;
                RefreshItem();
            }
        }

        private void RefreshItem()
        {
            _checkmarkImage.Visible = false;
            _itemGrid.SetItemData(_data.id);
            _countText.CurrentText.text = _data.num > 1 ? _data.num.ToString() : string.Empty;
        }

        public override void Dispose()
        {
            _data = null;
            _itemGrid.Clear();
        }
    }
}
