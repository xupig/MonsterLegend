﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTreasure
{
    public class CombatPortalConditionView : KContainer
    {
        private KContainer _numberContainer;
        private KContainer _levelContainer;
        private StateText _numberText;
        private StateText _levelText;
        private int _instanceId;
        private uint _entityId;

        protected override void Awake()
        {
            _numberContainer = GetChildComponent<KContainer>("Container_NR1");
            _numberText = _numberContainer.GetChildComponent<StateText>("Label_txtRenshu");
            _levelContainer = GetChildComponent<KContainer>("Container_NR0");
            _levelText = _levelContainer.GetChildComponent<StateText>("Label_txtJinrudengji");
            this.Visible = false;
        }

        public void RefreshContent(uint entityId, int instId)
        {
            _instanceId = instId;
            _entityId = entityId;
            RefreshLevelConditionContent();
            RefreshNumberConditionContent();
            this.Visible = true;
        }

        private void RefreshNumberConditionContent()
        {
            _numberContainer.Visible = true;
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int maxNum = instance_helper.GetMaxPlayerNum(ins);
            EntityCombatPortal portal = MogoWorld.GetEntity(_entityId) as EntityCombatPortal;
            int curNum = maxNum;
            if (portal != null)
            {
                curNum = (int)portal.enter_count;
            }
            if (curNum == maxNum)
            {
                _numberText.CurrentText.text = string.Concat(ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, curNum.ToString()), "/", maxNum);
            }
            else
            {
                _numberText.CurrentText.text = MogoLanguageUtil.GetContent(102413, curNum, maxNum);
            }
        }

        private void RefreshLevelConditionContent()
        {
            _levelContainer.Visible = true;
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int minLevel = instance_helper.GetMinLevel(ins);
            int maxLevel = instance_helper.GetMaxLevel(ins);
            string minNum = PlayerAvatar.Player.level >= minLevel ? minLevel.ToString() : ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, minLevel.ToString());
            string maxNum = PlayerAvatar.Player.level <= maxLevel ? maxLevel.ToString() : ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, maxLevel.ToString());
            string num = string.Empty;
            if (maxLevel == int.MaxValue)
            {
                num = string.Format("{0}", minNum);
            }
            else
            {
                num = string.Format("{0}-{1}", minNum, maxNum);
            }
            _levelText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(102416), num);
        }

        public void AddEventListener()
        {
            EntityCombatPortal portal = MogoWorld.GetEntity(_entityId) as EntityCombatPortal;
            EntityPropertyManager.Instance.AddEventListener(portal, EntityPropertyDefine.enter_count, RefreshNumberConditionContent);
        }

        public void RemoveEventListener()
        {
            EntityCombatPortal portal = MogoWorld.GetEntity(_entityId) as EntityCombatPortal;
            EntityPropertyManager.Instance.RemoveEventListener(portal, EntityPropertyDefine.enter_count, RefreshNumberConditionContent);
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            this.Visible = false;
        }

    }
}
