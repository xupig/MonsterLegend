﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTreasure
{
    public class IntegralContainer : KContainer
    {
        private KContainer _unitContainer;
        private KContainer _tensContainer;

        protected override void Awake()
        {
            _unitContainer = GetChildComponent<KContainer>("Container_jifen0");
            _tensContainer = GetChildComponent<KContainer>("Container_jifen1");
        }

        public void SetNumber(int num)
        {
            int unitDigit = num % 10;
            int tensDigit = num / 10;
            for (int i = 0; i < 10; i++)
            {
                _unitContainer.GetChild(i).SetActive(i == unitDigit);
                _tensContainer.GetChild(i).SetActive(i == tensDigit);
            }
        }

    }
}
