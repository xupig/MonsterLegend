﻿using Common.ExtendComponent;
using Game.UI.UIComponent;

namespace ModuleTreasure
{
    public class TreasureRewardCurrencyView : KContainer
    {
        private MoneyItem _goldItem;
        private MoneyItem _silverItem;
        private MoneyItem _copperItem;

        protected override void Awake()
        {
            _goldItem = AddChildComponent<MoneyItem>("Container_jin");
            _silverItem = AddChildComponent<MoneyItem>("Container_yin");
            _copperItem = AddChildComponent<MoneyItem>("Container_tong");
            _goldItem.SetMoneyType(1461);
            _silverItem.SetMoneyType(1460);
            _copperItem.SetMoneyType(1459);
        }


    }
}
