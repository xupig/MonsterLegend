﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using SpaceSystem;
using System.Collections.Generic;
using UnityEngine;


namespace ModuleTreasure
{
    public class PortalFightPanel : BasePanel
    {
        private IntegralContainer _myIntegral;
        private IntegralContainer _targetIntegral;
        private IntegralContainer _firstIntegral;

        private int _targetScore = 0;

        private const int ACTION_ID = 3002;

        protected override void Awake()
        {
            _myIntegral = AddChildComponent<IntegralContainer>("Container_wode");
            _targetIntegral = AddChildComponent<IntegralContainer>("Container_mubiao");
            _firstIntegral = AddChildComponent<IntegralContainer>("Container_diyiming");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.PortalFight; }
        }

        public override void OnShow(object data)
        {
            GetTargetScore();
            AddEventListener();
            RefreshContent();
        }

        private void GetTargetScore()
        {
            EntityData entityData = GameSceneManager.GetInstance().CurrActionEntityData(ACTION_ID);
            EntityCondTriggerActionData condData = entityData as EntityCondTriggerActionData;
            _targetScore = int.Parse(condData.event_trigger_num_l);
        }

        public override void OnClose()
        {
            RemoveEventListener();
            _targetScore = 0;
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(BattleUIEvents.REFRESH_AVATAR_SCORE, RefreshContent);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(BattleUIEvents.REFRESH_AVATAR_SCORE, RefreshContent);
        }

        private void RefreshContent()
        {
            int myScore = 0;
            int firstScore = 0;
            foreach (KeyValuePair<int, int> kvp in PlayerDataManager.Instance.InstanceAvatarData.ScoreDict)
            {
                if (kvp.Key == PlayerAvatar.Player.id)
                {
                    myScore = kvp.Value;
                }
                if (kvp.Value > firstScore)
                {
                    firstScore = kvp.Value;
                }
            }
            _myIntegral.SetNumber(myScore);
            _firstIntegral.SetNumber(firstScore);
            _targetIntegral.SetNumber(_targetScore);
        }


    }
}
