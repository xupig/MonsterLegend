﻿using ACTSystem;
using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;


namespace ModuleTreasure
{
    public class TreasureMapUsePanel : BasePanel
    {
        private ItemGrid _itemGrid;
        private KButton _useBtn;
        private StateText _nameText;

        private int _itemId;
        private uint _timerId;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _useBtn = GetChildComponent<KButton>("Button_shiyong");
            _nameText = GetChildComponent<StateText>("Container_zhuangbeiXinxi/Label_txtWupinmingcheng");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TreasureMapUse; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        public override void SetData(object data)
        {
            _itemId = (int)data;
            RefreshContent();
        }

        private void RefreshContent()
        {
            _itemGrid.SetItemData(_itemId);
            _nameText.CurrentText.text = item_helper.GetName(_itemId);
        }


        private void AddEventListener()
        {
            _useBtn.onClick.AddListener(OnUseBtnClick);
            EventDispatcher.AddEventListener(BattleUIEvents.BATTLE_CONTROL_STICK_DOWN, ClosePanel);
        }

        private void RemoveEventListener()
        {
            _useBtn.onClick.RemoveListener(OnUseBtnClick);
            EventDispatcher.RemoveEventListener(BattleUIEvents.BATTLE_CONTROL_STICK_DOWN, ClosePanel);
        }

        private void OnUseBtnClick()
        {
            SystemNoticeProgressData notice = new SystemNoticeProgressData();
            notice.type = InstanceDefine.SYSTEM_NOTICE_ACTION_PROGRESSBAR_TYPE;
            notice.progressbarPlayType = 1;
            notice.time = 2000;
            notice.content = 99287;
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
            }
            _timerId = TimerHeap.AddTimer(2000, 0, UseTreasureMap);
            SpaceNoticeActionManager.Instance.AddSystemNotice(notice);
            ClosePanel();
        }

        private void UseTreasureMap()
        {
            int position = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemPosition(_itemId);
            wild_helper.RemoveTreasurePosition(_itemId);
            ContinueUseTreasureMap();
            BagManager.Instance.RequestUseItem(position, 1);
        }


        private void ContinueUseTreasureMap()
        {
            if (PlayerAvatar.Player.treasure_map_count >= int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.treasureMapDayUseTimes)))
                return;
            BagData bagData = PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag);
            int num = bagData.GetItemNum(_itemId);
            if (num > 1)
            {
                int position = bagData.GetItemPosition(_itemId);
                BaseItemInfo itemInfo = bagData.GetItemInfo(position);
                EventDispatcher.TriggerEvent<BaseItemInfo>(EquipUpgradeTipEvents.Add_Item_To_First, itemInfo);
            }
        }

    }
}
