﻿using ACTSystem;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;


namespace ModuleTreasure
{
    public class TreasureRewardPanel : BasePanel
    {
        private KContainer _shareContainer;
        private KButton _shareButton;
        private KScrollPage _scrollPage;
        private KList _contentList;
        private KButton _confirmButton;
        private KButton _costBtn;
        private IconContainer _costIcon;
        private uint _timerId;
        private int _index;
        private KContainer _tipContainer;
        private StateText _tipsTxt;
        private KButton _tipsBtn;
        private TreasureRewardCurrencyView _currencyView;
        private Locater _contentLocater;
        private Locater _currencyLocater;

        private TreasureBoxType _boxType;

        private PbRewardList _data;
        private uint _entityId;

        private const int INTERVAL = 100;
        private const int PICKUP_NORMAL_CHEST = 81;
        private const int PICKUP_GOLD_CHEST = 82;
        private const int USE_TREASURE_MAP = 83;
        private float _slowSpeed = 1;
        private float _count = 0;
        private bool _isConfirm = false;
        private bool _isSlow = false;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _shareContainer = GetChildComponent<KContainer>("Container_neirong/Container_fenxiang");
            _shareButton = GetChildComponent<KButton>("Container_neirong/Container_fenxiang/Button_fenxiang");
            _confirmButton = GetChildComponent<KButton>("Container_neirong/Button_queding");
            _costBtn = GetChildComponent<KButton>("Container_neirong/Button_xiaohao");
            _costIcon = AddChildComponent<IconContainer>("Container_neirong/Button_xiaohao/icon");
            _scrollPage = GetChildComponent<KScrollPage>("Container_neirong/ScrollPage_jiangli");
            _contentList = GetChildComponent<KList>("Container_neirong/ScrollPage_jiangli/mask/content");
            _currencyView = AddChildComponent<TreasureRewardCurrencyView>("Container_dibu");
            _contentLocater = AddChildComponent<Locater>("Container_neirong");
            _currencyLocater = AddChildComponent<Locater>("Container_dibu");
            InitTipsContainer();
            InitScrollPage();
        }

        private void InitTipsContainer()
        {
            _tipContainer = GetChildComponent<KContainer>("Container_neirong/Container_tishi");
            _tipsTxt = GetChildComponent<StateText>("Container_neirong/Container_tishi/Label_txtFenxiangweizhi");
            _tipsBtn = GetChildComponent<KButton>("Container_neirong/Container_tishi/Button_wenhao");
            _tipsTxt.CurrentText.text = MogoLanguageUtil.GetContent(122126);
        }

        private void InitScrollPage()
        {
            _scrollPage.TotalPage = 1;
            _contentList.SetPadding(0, 0, 0, 20);
            _contentList.SetGap(-11, 0);
            _contentList.SetDirection(KList.KListDirection.LeftToRight, 6, 4);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TreasureReward; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void OnClose()
        {
            RemoveEventListener();
            ResetTimer();
            _data = null;
        }

        public override void SetData(object data)
        {
            if (data is uint)
            {
                _entityId = (uint)data;
                RefreshNotStartContent();
            }
            else if (data is PbRewardList)
            {
                _data = data as PbRewardList;
                StartToDraw();
            }
        }

        private void RefreshNotStartContent()
        {
            EntityChestBase chest = MogoWorld.GetEntity(_entityId) as EntityChestBase;
            if (chest == null)
            {
                Debug.LogError("找不到宝箱实体id：" + _entityId);
                return;
            }
            int chestType = chest_data_helper.GetChestType(chest.chest_id);
            _shareContainer.Visible = chestType == 3 || chestType == 6;
            _boxType = chest_data_helper.GetTreasureTypeByChestId(chest.chest_id);
            if (_boxType == TreasureBoxType.Chest)
            {
                _tipContainer.Visible = false;
                _currencyView.Visible = false;
                _contentLocater.Y = -55;
            }
            else if (_boxType == TreasureBoxType.DuelChest)
            {
                _tipContainer.Visible = true;
                _currencyView.Visible = true;
                _contentLocater.Y = -27;
                _currencyLocater.Y = -647;
            }
            RefreshCostView(chest.chest_id);
            _isStart = false;
            CloseBtn.Visible = true;
            List<RewardData> rewardList = item_reward_helper.GetRewardDataList(chest_data_helper.GetChestRewardId(chest.chest_id), PlayerAvatar.Player.vocation);
            _contentList.SetDataList<TreasureRewardItem>(rewardList);
        }

        private void RefreshCostView(int chestId)
        {
            KeyValuePair<string, string> costKvp = chest_data_helper.GetOpenCost(chestId);
            int costItemId = int.Parse(costKvp.Key);
            int costItemNum = int.Parse(costKvp.Value);
            if (costItemId == 0)
            {
                _costBtn.Visible = false;
                _confirmButton.Visible = true;
            }
            else
            {
                _costBtn.Visible = true;
                _confirmButton.Visible = false;
                bool isEnough = item_helper.GetMyMoneyCountByItemId(costItemId) >= costItemNum;
                string numContent = isEnough ? string.Format("x{0}", costItemNum) : ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, string.Format("x{0}", costItemNum));
                _costIcon.SetIcon(item_helper.GetIcon(costItemId));
                _costBtn.Label.ChangeAllStateText(numContent);
                _costBtn.Label.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
            }
        }

        private void PlayAnimation()
        {
            ResetTimer();
            _index = -1;
            _slowSpeed = 1;
            _count = -1;
            _isConfirm = false;
            _isSlow = false;
            _timerId = TimerHeap.AddTimer(100, INTERVAL, OnStep);
        }

        private void OnStep()
        {
            int totalCount = _contentList.ItemList.Count;
            CalculateSlowSpeed(totalCount);
            _count = (_count + _slowSpeed);
            _index = Mathf.FloorToInt(_count) % _contentList.ItemList.Count;
            TreasureRewardItem item = _contentList[_index] as TreasureRewardItem;
            item.ToggleCheckmark(true);
            int lastIndex = (_index + totalCount - 1) % totalCount;
            TreasureRewardItem lastItem = _contentList[lastIndex] as TreasureRewardItem;
            lastItem.ToggleCheckmark(false);
            CheckAnimationEnd();
        }

        private void CheckAnimationEnd()
        {
            int rewardIndex = GetRewardIndex();
            if (_isConfirm && _isSlow && _index == rewardIndex)
            {
                ResetTimer();
                TimerHeap.AddTimer(1000, 0, OnClosePanel);
            }
        }

        private void OnClosePanel()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.ItemObtain, _data);
            int itemId = (int)_data.item_list[0].id;
            if (item_helper.GetQuality(itemId) >= public_config.ITEM_QUALITY_ORANGE)
            {
                if (_data.get_way == USE_TREASURE_MAP)
                {
                    TreasureManager.Instance.SendSystemInfo(50202, itemId);
                }
                else
                {
                    if (_boxType == TreasureBoxType.Chest)
                    {
                        TreasureManager.Instance.SendSystemInfo(50201, itemId);
                    }
                    else if (_boxType == TreasureBoxType.DuelChest)
                    {
                        TreasureManager.Instance.SendSystemInfo(50203, itemId);
                    }
                }
            }
            ClosePanel();
        }

        private void CalculateSlowSpeed(int totalCount)
        {
            if (_isConfirm == true)
            {
                int rewardIndex = GetRewardIndex();
                int slowIndex = (rewardIndex - 6 + totalCount) % totalCount;
                if (_index == slowIndex)
                {
                    _isSlow = true;
                }
                if (_isSlow == true)
                {
                    _slowSpeed *= 0.8f;
                }
                _slowSpeed = Mathf.Max(_slowSpeed, 0.2f);
            }
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private void AddEventListener()
        {
            _confirmButton.onClick.AddListener(OnConfirmButtonClick);
            _shareButton.onClick.AddListener(OnShareButtonClick);
            _tipsBtn.onClick.AddListener(OnTipsBtnCilck);
            _costBtn.onClick.AddListener(OnConfirmButtonClick);
        }

        private void RemoveEventListener()
        {
            _confirmButton.onClick.RemoveListener(OnConfirmButtonClick);
            _shareButton.onClick.RemoveListener(OnShareButtonClick);
            _tipsBtn.onClick.RemoveListener(OnTipsBtnCilck);
            _costBtn.onClick.RemoveListener(OnConfirmButtonClick);
        }

        private void OnTipsBtnCilck()
        {
            RuleTipsDataWrapper wrapper = new RuleTipsDataWrapper(MogoLanguageUtil.GetContent(122127), _tipsBtn.GetComponent<RectTransform>(), RuleTipsDirection.TopRight);
            UIManager.Instance.ShowPanel(PanelIdEnum.RuleTips, wrapper);
        }

        private void OnShareButtonClick()
        {
            string content = string.Empty;
            int type = 0;
            if (_boxType == TreasureBoxType.Chest)
            {
                content = MogoLanguageUtil.GetContent(99103);
                type = 1;
            }
            else if (_boxType == TreasureBoxType.DuelChest)
            {
                content = MogoLanguageUtil.GetContent(122129);
                type = 3;
            }
            TreasureManager.Instance.ShareTreasurePosition(content, GameSceneManager.GetInstance().curMapID, (int)PlayerAvatar.Player.curLine, PlayerAvatar.Player.position, type);
        }

        private void OnConfirmButtonClick()
        {
            if (_boxType == TreasureBoxType.Chest)
            {
                TreasureManager.Instance.PickUpChest(_entityId);
            }
            else if (_boxType == TreasureBoxType.DuelChest)
            {
                TreasureManager.Instance.PickUpDuelChest(_entityId);
            }
        }

        private bool _isStart = false;
        private void StartToDraw()
        {
            if (_isStart) return;
            List<RewardData> rewardList = item_reward_helper.GetRewardDataList(_data.reward_id, PlayerAvatar.Player.vocation);
            _contentList.SetDataList<TreasureRewardItem>(rewardList);
            _isStart = true;
            CloseBtn.Visible = false;
            PlayAnimation();
            TimerHeap.AddTimer(2000, 0, StartSlow);
        }

        private void StartSlow()
        {
            _isConfirm = true;
        }

        private int GetRewardIndex()
        {
            for (int i = 0; i < _contentList.ItemList.Count; i++)
            {
                TreasureRewardItem item = _contentList[i] as TreasureRewardItem;
                RewardData data = item.Data as RewardData;
                if (data.id == _data.item_list[0].id && data.num == _data.item_list[0].count - _data.item_list[0].addi_count)
                {
                    return i;
                }
            }
            return 0;
        }


    }
}