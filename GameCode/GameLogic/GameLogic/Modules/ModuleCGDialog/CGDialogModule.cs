﻿using Common.Base;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/10 21:03:15 
 * function: 
 * *******************************************************/

namespace ModuleCGDialog
{
    public class CGDialogModule : BaseModule
    {
        public CGDialogModule()
        {

        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.CGDialog, "Container_NpcDialogPanel", MogoUILayer.LayerHideOther, "ModuleCGDialog.CGDialogPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);
            AddPanel(PanelIdEnum.BossIntroduce, "Container_BossIntroducePanel", MogoUILayer.LayerHideOther, "ModuleCGDialog.BossIntroducePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);
        }
    }
}
