﻿using ACTSystem;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Events;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace ModuleCGDialog
{
    public class BossIntroducePanel : BasePanel
    {
        private RawImage _rawImage;
        private StateText _descriptionText;
        private StateText _englishText;
        private StateText _nameText;
        private KContainer _nameContainer;
        private KContainer _nameBgContainer;

        private const float SCALE_TIME = 0.15f;
        private const float MOVE_TIME = 0.25f;

        private Vector3 _namePosition;
        private Locater _nameLocater;
        private Vector3 _nameFrom;

        private RectTransform _nameLeftBgRect;
        private RectTransform _nameRightBgRect;

        private uint _closeTimerId;

        protected override void Awake()
        {
            _rawImage = AddChildComponent<RawImage>("Container_Sboss/Container_jietu");
            _descriptionText = GetChildComponent<StateText>("Container_Sboss/Container_bottomCenter/Container_pangbai/Label_txtjieshao");
            _nameLeftBgRect = GetChildComponent<RectTransform>("Container_Sboss/Container_bottomCenter/Container_desc/Image_Bossmodian01");
            _nameRightBgRect = GetChildComponent<RectTransform>("Container_Sboss/Container_bottomCenter/Container_desc/Image_Bossmodian02");
            _englishText = GetChildComponent<StateText>("Container_Sboss/Container_bottomCenter/Container_pangbai/Label_txtyingyu");
            _nameBgContainer = GetChildComponent<KContainer>("Container_Sboss/Container_bottomCenter/Container_desc");
            _nameContainer = GetChildComponent<KContainer>("Container_Sboss/Container_bottomCenter/Container_desc/Container_name");
            _nameText = GetChildComponent<StateText>("Container_Sboss/Container_bottomCenter/Container_desc/Container_name/Label_txtBossmingcheng");
            MogoGameObjectHelper.SetFullScreenSize(_rawImage.gameObject);
            _namePosition = _nameContainer.transform.localPosition;
            _nameLocater = _nameContainer.gameObject.AddComponent<Locater>();
            InitBgView();
        }

        private void InitBgView()
        {
            _nameLeftBgRect.pivot = new Vector2(0.5f, 0.5f);
            _nameRightBgRect.pivot = new Vector2(0.5f, 0.5f);
            Vector3 localposition = _nameLeftBgRect.localPosition;
            localposition.x += _nameLeftBgRect.sizeDelta.x * 0.5f;
            localposition.y -= _nameLeftBgRect.sizeDelta.y * 0.5f;
            _nameLeftBgRect.localPosition = localposition;
            localposition = _nameRightBgRect.localPosition;
            localposition.x += _nameRightBgRect.sizeDelta.x * 0.5f;
            localposition.y -= _nameRightBgRect.sizeDelta.y * 0.5f;
            _nameRightBgRect.localPosition = localposition;
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.BossIntroduce; }
        }

        public override void OnShow(object data)
        {
            ResetCloseTimer();
            _closeTimerId = TimerHeap.AddTimer(10000, 0, ExceptionalClose);
            AddEventListener();
            BossIntroduceData bossData = data as BossIntroduceData;
            RefreshImage(bossData.texture);
            RefreshContent(bossData.monsterId);
        }

        public override void OnClose()
        {
            ResetCloseTimer();
            RemoveEventListener();
            _rawImage.texture = null;
        }

        private void ResetCloseTimer()
        {
            if (_closeTimerId != 0)
            {
                TimerHeap.DelTimer(_closeTimerId);
                _closeTimerId = 0;
            }
        }

        private void ExceptionalClose()
        {
            LoggerHelper.Error("BossIntroduce Close Exception");
            ClosePanel();
        }

        private void RefreshImage(Texture texture)
        {
            if (texture != null)
            {
                _rawImage.texture = texture;
            }
        }

        private void CloseBossIntroduce()
        {
            TweenPosition tween = TweenPosition.Begin(_nameContainer.gameObject, MOVE_TIME, _nameFrom, 0, UITweener.Method.EaseIn);
            tween.onFinished = OnCloseTweenEnd;
        }

        private void OnCloseTweenEnd(UITweener tween)
        {
            ClosePanel();
        }

        private void RefreshContent(int monsterId)
        {
            _descriptionText.Visible = false;
            _englishText.Visible = false;
            _descriptionText.CurrentText.text = monster_helper.GetMonsterDesc(monsterId);
            _englishText.CurrentText.text = monster_helper.GetMonsterEnglishDesc(monsterId);
            RefreshMonsterName(monsterId);
            PlayAnimation();

        }

        private void PlayAnimation()
        {
            _nameLocater.X = -_nameLocater.Width - _nameLocater.transform.parent.localPosition.x;
            _nameFrom = _nameLocater.Position;
            _nameLeftBgRect.gameObject.SetActive(false);
            _nameLeftBgRect.transform.localScale = new Vector3(3, 3, 1);
            _nameRightBgRect.gameObject.SetActive(false);
            _nameRightBgRect.transform.localScale = new Vector3(3, 3, 1);
            PlayLeftBgAnimation();
        }

        private void PlayLeftBgAnimation()
        {
            _nameLeftBgRect.gameObject.SetActive(true);
            TweenScale.Begin(_nameLeftBgRect.gameObject, SCALE_TIME, Vector3.one, UITweener.Method.EaseOut, PlayRightBgAnimation);
        }

        private void PlayRightBgAnimation(UITweener tween)
        {
            _nameRightBgRect.gameObject.SetActive(true);
            TweenScale.Begin(_nameRightBgRect.gameObject, SCALE_TIME, Vector3.one, UITweener.Method.EaseOut, PlayMoveAnimation);
        }

        private void PlayMoveAnimation(UITweener tween)
        {
            TweenPosition.Begin(_nameContainer.gameObject, MOVE_TIME, _namePosition, 0, UITweener.Method.EaseOut, OnTweenFinshed);
        }

        private void OnTweenFinshed(UITweener tween)
        {
            _descriptionText.Visible = true;
            if (string.IsNullOrEmpty(_englishText.CurrentText.text) == false)
            {
                _englishText.Visible = true;
            }
        }

        private void RefreshMonsterName(int monsterId)
        {
            _nameText.CurrentText.text = monster_helper.GetMonsterName(monsterId);
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(CGEvent.HIDE_BOSS_INTRODUCE, CloseBossIntroduce);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(CGEvent.HIDE_BOSS_INTRODUCE, CloseBossIntroduce);
        }

    }
}
