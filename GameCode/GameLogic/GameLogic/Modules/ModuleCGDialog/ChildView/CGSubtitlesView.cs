﻿using Common.Data;
using Common.Events;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/7/30 10:32:12 
 * function: 
 * *******************************************************/

namespace ModuleCGDialog
{
    public class CGSubtitlesView : KContainer
    {
        private StateText _txtSubtitles;
        private StateText _txtSubtitlesEng;
        private float _pastTime = 0;
        private float _endTime = 0;

        protected override void Awake()
        {
            _txtSubtitles = GetChildComponent<StateText>("Label_txtjieshao");
            _txtSubtitlesEng = GetChildComponent<StateText>("Label_txtyingyu");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void AddEventListener()
        {

        }

        private void RemoveEventListener()
        {

        }

        void Update()
        {
            if (_endTime > 0)
            {
                _pastTime += Time.deltaTime;
                if (_pastTime >= _endTime)
                {
                    _endTime = 0;
                    Visible = false;
                    EventDispatcher.TriggerEvent(CGEvent.HIDE_DIALOG);
                }
            }
        }

        public void UpdateDialog(object data)
        {
            if (data is CGDialogShowSubtitles)
            {
                RefreshSubtitles(data as CGDialogShowSubtitles);
            }
        }

        private void RefreshSubtitles(CGDialogShowSubtitles data)
        {
            _pastTime = 0;
            _endTime = data.duration;
            UpdateSubtitlesContent(data.content, data.contentEng);
        }

        private void UpdateSubtitlesContent(int content, int contentEng)
        {
            _txtSubtitles.CurrentText.text = SystemInfoUtil.GenerateContent(MogoLanguageUtil.GetContent(content), PlayerAvatar.Player.name);
            _txtSubtitlesEng.CurrentText.text = SystemInfoUtil.GenerateContent(MogoLanguageUtil.GetContent(contentEng), PlayerAvatar.Player.name);
        }
    }
}
