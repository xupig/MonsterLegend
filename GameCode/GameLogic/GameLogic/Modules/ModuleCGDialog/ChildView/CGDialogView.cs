﻿using ACTSystem;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/7/30 10:31:54 
 * function: 
 * *******************************************************/

namespace ModuleCGDialog
{
    public class CGDialogView : KContainer
    {
        private static string PLAYER = "Player";
        private static string NPC = "NPC";
        private static string DUMMY = "Dummy";
        private static string HAVE_PARAM = "PARAM";

        private static List<string> _contentList = new List<string>();

        private bool _isWaitingEvent = false;

        private string _entityType;
        private KContainer _myContainer;
        private KContainer _npcContainer;
        private StateText _txtMyName;
        private StateText _txtNpcName;
        private KContainer _imageMyName;
        private KContainer _imageNpcName;
        private StateText _txtMyContent;
        private StateText _txtNpcContent;
        private KButton _btnClose;
        private KButton _btnMyNext;
        private KButton _btnNpcNext;

        private ModelComponent _selfModel;
        private ModelComponent _npcModel;
        private DialogComponent _dialogComponent;

        private float _pastTime = 0;
        private float _endTime = 0;
        protected override void Awake()
        {
            _myContainer = GetChildComponent<KContainer>("Container_my");
            _npcContainer = GetChildComponent<KContainer>("Container_npc");
            _txtMyName = _myContainer.GetChildComponent<StateText>("Label_txtMingcheng");
            _txtNpcName = _npcContainer.GetChildComponent<StateText>("Label_txtMingcheng");
            _imageMyName = _myContainer.GetChildComponent<KContainer>("Container_NPCmingziditu");
            _imageNpcName = _npcContainer.GetChildComponent<KContainer>("Container_NPCmingziditu");
            _txtMyContent = _myContainer.GetChildComponent<StateText>("Label_txtxiangximiaoshu");
            _txtNpcContent = _npcContainer.GetChildComponent<StateText>("Label_txtxiangximiaoshu");
            _btnMyNext = GetChildComponent<KButton>("Button_xiangxiamy");
            _btnNpcNext = GetChildComponent<KButton>("Button_xiangxianpc");
            _btnClose = GetChildComponent<KButton>("Button_close");

            //StateImage myBg = _myContainer.GetChildComponent<StateImage>("ScaleImage_KuangZJ");
            //StateImage npcBg = _npcContainer.GetChildComponent<StateImage>("ScaleImage_KuangZJ");


            //myBg.SetDimensionsDirty();
            //RectTransform rect = myBg.CurrentImage.GetComponent<RectTransform>();
            //rect.sizeDelta = new Vector2(UIManager.CANVAS_WIDTH + 2, rect.sizeDelta.y);

            //npcBg.SetDimensionsDirty();
            //rect = npcBg.CurrentImage.GetComponent<RectTransform>();
            //rect.sizeDelta = new Vector2(UIManager.CANVAS_WIDTH + 2, rect.sizeDelta.y);

            _selfModel = _myContainer.AddChildComponent<ModelComponent>("Container_icon");
            _npcModel = _npcContainer.AddChildComponent<ModelComponent>("Container_icon");

            _selfModel.DragComponent.Dragable = false;
            _npcModel.DragComponent.Dragable = false;
            SetComponentsLayer();
            _dialogComponent = gameObject.AddComponent<DialogComponent>();
        }

        private void SetComponentsLayer()
        {
            SetComponentLayer(_selfModel);
            SetComponentLayer(_npcModel);
        }

        private void SetComponentLayer(MonoBehaviour component)
        {
            var originalLocalPosition = component.transform.localPosition;
            component.transform.localPosition = new Vector3(originalLocalPosition.x, originalLocalPosition.y, 2000f);
        }

        private void LoadSelfModel()
        {
            _selfModel.LoadPlayerModel(OnLoadAvatarModelFinished, !GameSceneManager.GetInstance().inCombatScene);
        }

        private void OnLoadAvatarModelFinished(ACTActor actor)
        {
            actor.localEulerAngles = new Vector3(0, 200f, 0);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            LoadSelfModel();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _btnClose.onClick.AddListener(OnSkipDialog);
            _btnNpcNext.onClick.AddListener(OnSkipDialog);
            _btnMyNext.onClick.AddListener(OnSkipDialog);
        }

        private void RemoveEventListener()
        {
            _btnClose.onClick.RemoveListener(OnSkipDialog);
            _btnNpcNext.onClick.RemoveListener(OnSkipDialog);
            _btnMyNext.onClick.RemoveListener(OnSkipDialog);
        }

        void Update()
        {
            if (_endTime > 0)
            {
                _pastTime += Time.deltaTime;
                if (_pastTime >= _endTime)
                {
                    _endTime = 0;
                    Visible = false;
                    EventDispatcher.TriggerEvent(CGEvent.HIDE_DIALOG);
                }
            }
        }

        public void UpdateDialog(object data)
        {
            EventDispatcher.TriggerEvent(MainUIEvents.RESET_CONTROL_STICK);
            if (data is CGDialogData)
            {
                RefreshCGDialog(data as CGDialogData);
            }
        }

        private void RefreshCGDialog(CGDialogData data)
        {
            _btnClose.gameObject.SetActive(true);
            _isWaitingEvent = data.isWaitingEvent;
            _entityType = data.entityType;
            _pastTime = 0;
            _endTime = data.duration + 0.1f;
            string[] dialog = data.content.Split(',');
            UpdateDialogContent(dialog);
        }

        private void UpdateDialogContent(string[] dialog)
        {
            bool showModel = dialog.Length >= 3 ? Convert.ToBoolean(int.Parse(dialog[2])) : true;
            if (_entityType == PLAYER)//自己的对话
            {
                _myContainer.gameObject.SetActive(true);
                _npcContainer.gameObject.SetActive(false);
                _btnMyNext.gameObject.SetActive(_isWaitingEvent);
                _btnNpcNext.gameObject.SetActive(false);
                _txtMyName.CurrentText.text = (MogoWorld.Player as PlayerAvatar).name;
                //_txtMyContent.CurrentText.text = GetContent(dialog);// dialog[1]
                _dialogComponent.SetTxtContent(_txtMyContent, GetContent(dialog));
                _selfModel.Visible = showModel;
            }
            else//NPC or Dummy
            {
                _npcContainer.gameObject.SetActive(true);
                _myContainer.gameObject.SetActive(false);
                _btnMyNext.gameObject.SetActive(false);
                _btnNpcNext.gameObject.SetActive(_isWaitingEvent);
                if (_entityType == NPC)
                {
                    UpdateNPCContent(dialog, showModel);
                }
                else if (_entityType == DUMMY)
                {
                    UpdateDummyContent(dialog, showModel);
                }
            }
        }

        private void UpdateNPCContent(string[] dialog, bool showModel)
        {
            npc _npc = XMLManager.npc[int.Parse(dialog[0])];
            _txtNpcName.CurrentText.text = MogoLanguageUtil.GetContent(_npc.__name);
            //_txtNpcContent.CurrentText.text = GetContent(dialog);// dialog[1]
            _dialogComponent.SetTxtContent(_txtNpcContent, GetContent(dialog));
            _npcModel.Visible = showModel;
            if (showModel)
            {
                _npcModel.LoadNPCModel(_npc.__id, (actor) =>
                {
                    actor.localEulerAngles = new Vector3(0, 160f, 0);
                });
            }
        }

        private void UpdateDummyContent(string[] dialog, bool showModel)
        {
            int monsterId = int.Parse(dialog[0]);
            _txtNpcName.CurrentText.text = monster_helper.GetMonsterName(monsterId);
            _dialogComponent.SetTxtContent(_txtNpcContent, GetContent(dialog));
            _npcModel.Visible = showModel;
            if (showModel)
            {
                _npcModel.LoadMonsterModel(monsterId, (actor) =>
                {
                    actor.localEulerAngles = new Vector3(0, 160f, 0);
                });
            }
        }

        private string GetContent(string[] dialog)
        {
            if (dialog[1] == HAVE_PARAM)
            {
                return SystemInfoUtil.GenerateContent(MogoLanguageUtil.GetContent(dialog[2], dialog[3]));
            }
            _contentList.Clear();
            _contentList.Add(dialog[1]);
            return SystemInfoUtil.GenerateContent(MogoLanguageUtil.GetContent(_contentList), PlayerAvatar.Player.name);
        }

        private void OnSkipDialog()
        {
            if (!_isWaitingEvent)
            {
                return;
            }
            if (_dialogComponent.index == 0)
            {
                EventDispatcher.TriggerEvent(CGEvent.HIDE_DIALOG);
                DramaManager.GetInstance().Trigger(DramaConfig.SKIP_EVENT_NAME, DramaConfig.SKIP_EVENT_VALUE);
            }
            else
            {
                _dialogComponent.Stop();
            }
        }
    }
}
