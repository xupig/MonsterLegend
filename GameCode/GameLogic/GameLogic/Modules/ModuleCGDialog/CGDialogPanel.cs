﻿using ACTSystem;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleMainUI;
using MogoEngine;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/6/10 21:08:45 
 * function: 
 * *******************************************************/

namespace ModuleCGDialog
{
    public class CGDialogPanel : BasePanel
    {
        private SpeechInfoCGList _speechInfoList;
        private KButton _btnSkip;
        private CGDialogView _cgDialogView;
        private CGSubtitlesView _cgSubtitlesView;
        private KContainer _blackContainer;
        public static bool isShow = false;

        public static string dramaKey;
        private bool _skipped;

        private CGData _data;

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("Container_bottomLeft/Button_close/image");
            _speechInfoList = AddChildComponent<SpeechInfoCGList>("Container_speechInfoList");
            MogoGameObjectHelper.SetFullScreenSize(_speechInfoList.gameObject);
            _cgDialogView = AddChildComponent<CGDialogView>("Container_bottomLeft");
            _cgSubtitlesView = AddChildComponent<CGSubtitlesView>("Container_bottomCenter/Container_pangbai");
            _btnSkip = GetChildComponent<KButton>("Container_topRight/Button_btn3");
            _blackContainer = GetChildComponent<KContainer>("Container_heibian");

            (_btnSkip as KShrinkableButton).RefreshRectTransform();
            _cgDialogView.Visible = false;

            KContainer topBg = GetChildComponent<KContainer>("Container_topLeft");
            //MogoGameObjectHelper.AttachFullScreen(topBg.gameObject);
            topBg.Visible = false;
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.CGDialog; }
        }

        public override void OnShow(System.Object data)
        {
            TaskManager.Instance.ContinueAutoTask();
            isShow = true;
            _skipped = false;
            _btnSkip.Visible = false;
            _blackContainer.Visible = false;
            OnUpdateDialog(data);
            AddEventListener();
        }

        public override void OnClose()
        {
            isShow = false;
            RemoveEventListener();
        }

        public override void SetData(object data)
        {
            OnUpdateDialog(data);
        }

        private void OnUpdateDialog(object data)
        {
            if (!(data is CGData))
            {
                return;
            }
            dramaKey = (data as CGData).dramaKey;
            RefreshCGView(data);
        }

        private void RefreshCGView(object data)
        {
            _data = data as CGData;
            if (data is CGDialogShowSkip)
            {
                _btnSkip.Visible = (data as CGDialogShowSkip).isShow;
            }
            else if (data is CGDialogShowBlackEdge)
            {
                _blackContainer.Visible = (data as CGDialogShowBlackEdge).isShow;
            }
            else if (data is CGDialogCallBackData)
            {
                _cgDialogView.Visible = true;
                _cgSubtitlesView.Visible = false;
                _cgDialogView.UpdateDialog(data);
            }
            else if (data is CGDialogData)
            {
                _cgDialogView.Visible = true;
                _cgSubtitlesView.Visible = false;
                _cgDialogView.UpdateDialog(data);
            }
            else if (data is CGDialogShowSubtitles)
            {
                _cgSubtitlesView.Visible = true;
                _cgDialogView.Visible = false;
                _cgSubtitlesView.UpdateDialog(data);
            }
        }
     
        private void AddEventListener()
        {
            _btnSkip.onClick.AddListener(OnSkipDrama);
            EventDispatcher.AddEventListener(CGEvent.HIDE_SKIP, OnHideSkip);
            EventDispatcher.AddEventListener(CGEvent.HIDE_BLACK_EDGE, OnHideBlackEdge);
            EventDispatcher.AddEventListener(CGEvent.HIDE_DIALOG, OnHideDialog);
            EventDispatcher.AddEventListener<uint, int>(CGEvent.SHOW_CG_SPEECH, CreateSpeechInfo);
        }

        private void RemoveEventListener()
        {
            _btnSkip.onClick.RemoveListener(OnSkipDrama);
            EventDispatcher.RemoveEventListener(CGEvent.HIDE_SKIP, OnHideSkip);
            EventDispatcher.RemoveEventListener(CGEvent.HIDE_BLACK_EDGE, OnHideBlackEdge);
            EventDispatcher.RemoveEventListener(CGEvent.HIDE_DIALOG, OnHideDialog);
            EventDispatcher.RemoveEventListener<uint, int>(CGEvent.SHOW_CG_SPEECH, CreateSpeechInfo);
        }

        public void CreateSpeechInfo(uint entityId, int chineseId)
        {
            SpeechInfoCGManager.Instance.CreateSpeechInfo(entityId, chineseId);
        }

        private void OnHideDialog()
        {
            _cgDialogView.Visible = false;
            TryClosePanel();
        }

        private void OnHideSkip()
        {
            _btnSkip.Visible = false;
            TryClosePanel();
        }

        private void OnHideBlackEdge()
        {
            _blackContainer.Visible = false;
            TryClosePanel();
        }

        private void TryClosePanel()
        {
            if (!_cgDialogView.IsActive() && !_cgSubtitlesView.IsActive() && !_blackContainer.IsActive() && !_btnSkip.IsActive())
            {
                ClosePanel();
                if (_data is CGDialogCallBackData)
                {
                    CGDialogCallBackData cgDialog = _data as CGDialogCallBackData;
                    if (cgDialog.callback != null)
                    {
                        cgDialog.callback.Invoke();
                    }
                }
            }
        }

        private void OnSkipDrama()
        {
            if (!_skipped)
            {
                _skipped = true;
                DramaManager.GetInstance().SkipDrama(dramaKey);
            }
        }
    }
}
