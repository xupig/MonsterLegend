﻿using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTutor
{
    public class TutorPanel : BasePanel
    {
        private CategoryList _categoryList;
        private TutorRecordView _recordView;
        private TutorLevelView _levelView;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Tutor; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            _recordView = AddChildComponent<TutorRecordView>("Container_daoshijilu");
            _levelView = AddChildComponent<TutorLevelView>("Container_daoshi");
            InitCategoryList();
        }

        private void InitCategoryList()
        {
            List<CategoryItemData> listToggleData = new List<CategoryItemData>();
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(120101)));
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(120102)));
            _categoryList.RemoveAll();
            _categoryList.SetDataList<CategoryListItem>(listToggleData);
        }

        private void RefreshGreenPoint()
        {
            (_categoryList.ItemList[0] as CategoryListItem).SetPoint(false);
            (_categoryList.ItemList[1] as CategoryListItem).SetPoint(false);
            if (TutorManager.Instance.canGetReward || TutorManager.Instance.noticeBuyTutorShopItem)
            {
                (_categoryList.ItemList[0] as CategoryListItem).SetPoint(true);
            }
        }

        private void AddEventListener()
        {
            _categoryList.onSelectedIndexChanged.AddListener(OnCategoryListChange);
            EventDispatcher.AddEventListener(TutorEvents.REFRESH_GREEN_POINT, RefreshGreenPoint);
        }

        private void RemoveEventListener()
        {
            _categoryList.onSelectedIndexChanged.RemoveListener(OnCategoryListChange);
            EventDispatcher.RemoveEventListener(TutorEvents.REFRESH_GREEN_POINT, RefreshGreenPoint);
        }

        private void OnCategoryListChange(CategoryList arg0, int index)
        {
            if (index == 0)
            {
                _recordView.Visible = false;
                _levelView.Visible = true;
            }
            else if (index == 1)
            {
                _recordView.Visible = true;
                _levelView.Visible = false;
            }
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            if (data != null)
            {
                if (data is int[])
                {
                    int[] list = data as int[];
                    _categoryList.SelectedIndex = list[1] - 1;
                }
                else if (data is int)
                {
                    int index = (int)data;
                    _categoryList.SelectedIndex = index;
                }
            }
            else
            {
                _categoryList.SelectedIndex = 0;
            }
            OnCategoryListChange(_categoryList, _categoryList.SelectedIndex);
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
            RefreshGreenPoint();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }
    }
}
