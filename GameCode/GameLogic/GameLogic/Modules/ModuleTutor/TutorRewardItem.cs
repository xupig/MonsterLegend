﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTutor
{
    public class TutorRewardItem : KList.KListItemBase
    {
        private StateText _titleTxt;
        private KButton _getBtn;
        private KParticle _particle;
        private List<RewardGridBase> _rewardList;
        private int _rewardId;
        private KContainer _rewardContainer;

        protected override void Awake()
        {
            _titleTxt = GetChildComponent<StateText>("Label_txtBiaoti");
            _getBtn = GetChildComponent<KButton>("Button_lingqu");
            _particle = _getBtn.AddChildComponent<KParticle>("fx_ui_3_2_lingqu");
            _rewardContainer = GetChildComponent<KContainer>("Container_jiangli");
            _particle.Stop();
            _rewardList = new List<RewardGridBase>();
            _rewardList.Add(AddChildComponent<RewardGridBase>("Container_jiangli/Container_jiangli1"));
            _rewardList.Add(AddChildComponent<RewardGridBase>("Container_jiangli/Container_jiangli2"));
            _rewardList.Add(AddChildComponent<RewardGridBase>("Container_jiangli/Container_jiangli3"));
            _titleTxt.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
            MaskEffectItem maskEffectItem = gameObject.AddComponent<MaskEffectItem>();
            maskEffectItem.Init(_getBtn.gameObject, CanShowEffect);
        }

        private int GetLevel()
        {
            return this.Index + 1;
        }

        private bool CanShowEffect()
        {
            if (tutor_helper.HasGetReward(GetLevel()) == false)
            {
                if (PlayerAvatar.Player.tutor_level > 0 && GetLevel() <= PlayerAvatar.Player.tutor_level)
                {
                    return true;
                }
            }
            return false;
        }

        public override object Data
        {
            get
            {
                return _rewardId;
            }
            set
            {
                _rewardId = (int)value;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(120103, GetLevel());
            List<RewardData> rewardDataList = item_reward_helper.GetRewardDataList(_rewardId, PlayerAvatar.Player.vocation);
            for (int i = 0; i < 3; i++)
            {
                if (rewardDataList.Count > i)
                {
                    _rewardList[i].Visible = true;
                    _rewardList[i].Data = rewardDataList[i];
                }
                else
                {
                    _rewardList[i].Visible = false;
                }
            }
            _rewardContainer.RecalculateSize();
            RecalculatePosition();
            RefreshBtnView();
        }

        private void RecalculatePosition()
        {
            RectTransform rect = _rewardContainer.transform as RectTransform;
            RectTransform parentRect = _rewardContainer.transform.parent as RectTransform;
            rect.anchoredPosition = new Vector2(parentRect.sizeDelta.x / 2 - rect.sizeDelta.x / 2, rect.anchoredPosition.y);
        }

        private void RefreshBtnView()
        {
            if (tutor_helper.HasGetReward(GetLevel()))
            {
                _getBtn.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(85019));
                _getBtn.SetButtonDisable();
            }
            else
            {
                _getBtn.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(85018));
                _getBtn.SetButtonActive();
            }
            if (CanShowEffect())
            {
                _particle.Play(true);
            }
            else
            {
                _particle.Stop();
            }
        }

        public override void Show()
        {
            base.Show();
            AddEventListener();
        }

        public override void Hide()
        {
            base.Hide();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _getBtn.onClick.AddListener(OnGetBtnClick);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.tutor_reward, RefreshBtnView);
        }

        private void RemoveEventListener()
        {
            _getBtn.onClick.RemoveListener(OnGetBtnClick);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.tutor_reward, RefreshBtnView);
        }

        private void OnGetBtnClick()
        {
            if (GetLevel() > PlayerAvatar.Player.tutor_level)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(120114, GetLevel()));
                return;
            }
            TutorManager.Instance.GetReward(GetLevel());
        }

        public override void Dispose()
        {
            _rewardId = 0;
        }
    }
}
