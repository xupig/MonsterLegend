﻿using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTutor
{
    public class TutorRecordItem : KList.KListItemBase
    {
        private StateText _descTxt;

        private PbTutorEventInfo _data;

        protected override void Awake()
        {
            _descTxt = GetChildComponent<StateText>("Label_txtJilv");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbTutorEventInfo;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            string time = MogoTimeUtil.ToLocalTime((long)_data.time, "yyyy-MM-dd");
            string eventName = tutor_event_helper.GetEventDescription((int)_data.event_id);
            int diffLevel = int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.level_control));
            if (_data.exp != 0)
            {
                if (_data.value != 0)
                {
                    _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(120111, diffLevel, eventName, _data.exp, _data.value, time);
                }
                else
                {
                    _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(120113, diffLevel, eventName, _data.exp, time);
                }
            }
            else
            {
                _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(120112, diffLevel, eventName, _data.value, time);
            }
        }

        public override void Dispose()
        {
            _data = null;
        }
    }
}
