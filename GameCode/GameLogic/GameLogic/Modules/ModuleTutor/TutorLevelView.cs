﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using ModuleCommonUI.Token;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTutor
{
    public class TutorLevelView : KContainer
    {
        private StateText _levelTxt;
        private KScrollPage _scrollPage;
        private KList _list;
        private MoneyItem _money;
        private StateText _expTxt;
        private KProgressBar _expBar;
        private KButton _ruleBtn;
        private KButton _promoteBtn;
        private KButton _shopBtn;
        private KParticle _shopParticle;

        protected override void Awake()
        {
            base.Awake();
            _levelTxt = GetChildComponent<StateText>("Label_txtDaoshidengji");
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_libao");
            _list = _scrollPage.GetChildComponent<KList>("mask/content");
            _money = AddChildComponent<MoneyItem>("Container_daoshizhi");
            _expTxt = GetChildComponent<StateText>("Label_jingyanzhi");
            _expBar = GetChildComponent<KProgressBar>("ProgressBar_jindu");
            _ruleBtn = GetChildComponent<KButton>("Button_guize");
            _promoteBtn = GetChildComponent<KButton>("Button_tisheng");
            _shopBtn = GetChildComponent<KButton>("Button_shangcheng");
            _shopParticle = AddChildComponent<KParticle>("Button_shangcheng/fx_ui_23_zhiyin_01");
            _shopParticle.Stop();
            _scrollPage.SetDirection(KScrollPage.KScrollPageDirection.LeftToRight);
            _list.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue);
            _list.SetPadding(20, 0, 0, 165);
            _list.SetGap(0, 321);
            _money.SetMoneyType(public_config.MONEY_TYPE_TUTOR_VALUE);
        }

        private void RefreshContent()
        {
            _levelTxt.CurrentText.text = MogoLanguageUtil.GetContent(120118, PlayerAvatar.Player.tutor_level);
            int needExp = tutor_helper.GetNeedExpByLevel(PlayerAvatar.Player.tutor_level);
            int curExp = PlayerAvatar.Player.tutor_exp;
            _expBar.Value = needExp == 0 ? 1 : (float)curExp / (float)needExp;
            _expTxt.CurrentText.text = needExp == 0 ? MogoLanguageUtil.GetContent(120139) : string.Concat(curExp.ToString(), "/", needExp.ToString());
            List<int> rewardIdList = tutor_helper.GetRewardIdList();
            _list.SetDataList<TutorRewardItem>(rewardIdList);
            _scrollPage.TotalPage = rewardIdList.Count;
            RefreshPage();
            _shopParticle.Stop();
            if (TutorManager.Instance.noticeBuyTutorShopItem)
            {
                _shopParticle.Play(true);
                _shopParticle.Position = new Vector3(10, 12, -50);
            }
        }

        private void RefreshPage()
        {
            int currentPage = 0;
            if (PlayerAvatar.Player.tutor_reward != 0)
            {
                for (int i = 1; i < 30; i++)
                {
                    if (tutor_helper.HasGetReward(i) == false)
                    {
                        currentPage = i - 1;
                        break;
                    }
                }
            }
            _scrollPage.CurrentPage = currentPage;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            RefreshContent();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _ruleBtn.onClick.AddListener(OnRuleBtnClick);
            _promoteBtn.onClick.AddListener(OnPromoteBtnClick);
            _shopBtn.onClick.AddListener(OnShopBtnClick);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.tutor_reward, RefreshPage);
        }

        private void RemoveEventListener()
        {
            _ruleBtn.onClick.RemoveListener(OnRuleBtnClick);
            _promoteBtn.onClick.RemoveListener(OnPromoteBtnClick);
            _shopBtn.onClick.RemoveListener(OnShopBtnClick);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.tutor_reward, RefreshPage);
        }

        private void OnShopBtnClick()
        {
            TutorManager.Instance.noticeBuyTutorShopItem = false;
            _shopParticle.Stop();
            PanelIdEnum.Token.Show(TokenType.TUTOR);
        }

        private void OnPromoteBtnClick()
        {
            if (item_channel_helper.ContainId(public_config.MONEY_TYPE_TUTOR_EXP) == false)
            {
                MessageBox.Show(true, "", "该物品暂时没有获取渠道！");
                return;
            }
            UIManager.Instance.ShowPanel(PanelIdEnum.ItemChannel, public_config.MONEY_TYPE_TUTOR_EXP);
        }

        private void OnRuleBtnClick()
        {
            int levelControl = int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.level_control));
            string result = string.Empty;
            result = string.Concat(result, MogoLanguageUtil.GetContent(120107, levelControl, levelControl), '\n');
            for (int i = 1; i < 4; i++)
            {
                result = string.Concat(result, MogoLanguageUtil.GetContent(120107 + i), '\n');
            }
            UIManager.Instance.ShowPanel(PanelIdEnum.RuleTips, new RuleTipsDataWrapper(result, _ruleBtn.transform as RectTransform, RuleTipsDirection.BottomLeft));
            //UIManager.Instance.ShowPanel(PanelIdEnum.ItemStory, new ItemStoryData(MogoLanguageUtil.GetContent(120106), result));
        }


    }
}
