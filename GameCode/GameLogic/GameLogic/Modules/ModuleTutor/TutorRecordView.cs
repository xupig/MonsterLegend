﻿using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTutor
{
    public class TutorRecordView : KContainer
    {
        private KScrollView _scrollView;
        private KList _list;
        private StateText _noneTxt;

        protected override void Awake()
        {
            base.Awake();
            _scrollView = GetChildComponent<KScrollView>("ScrollView_zhanlibang");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _noneTxt = GetChildComponent<StateText>("Label_txtmeiyoushuju");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }


        private void RefreshLogs(PbTutorEventInfoList pbList)
        {
            _noneTxt.Visible = pbList.tutorEventInfoList.Count == 0;
            pbList.tutorEventInfoList.Reverse();
            _scrollView.ResetContentPosition();
            _list.SetDataList<TutorRecordItem>(pbList.tutorEventInfoList, 2);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            TutorManager.Instance.GetLogs();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<PbTutorEventInfoList>(TutorEvents.REFRESH_TUTOR_LOGS, RefreshLogs);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<PbTutorEventInfoList>(TutorEvents.REFRESH_TUTOR_LOGS, RefreshLogs);
        }
    }
}
