﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTutor
{
    public class TutorModule : BaseModule
    {

        public TutorModule()
            : base()
        {
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.Tutor, "Container_TutorPanel", MogoUILayer.LayerUIPanel, "ModuleTutor.TutorPanel", BlurUnderlay.Have, HideMainUI.Hide);
        }


    }
}
