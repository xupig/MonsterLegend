﻿#region 模块信息
/*==========================================
// 文件名：EquipChannelPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleEquipChannel
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/22 11:07:58
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.ServerConfig;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System.Collections.Generic;
namespace ModuleEquipChannel
{
    public class EquipChannelPanel : BasePanel
    {
        private KScrollView _equipScrollView;
        private KScrollView _equipMenu;
        private KList _menuList;
        private KList _equipList;
        private MenuItemData currentMenuData;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.EquipChannel; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _equipMenu = GetChildComponent<KScrollView>("ScrollView_zhuangbeicaidan");
            _menuList = _equipMenu.GetChildComponent<KList>("mask/content");
            _menuList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _equipScrollView = GetChildComponent<KScrollView>("ScrollView_zhuangbei");
            _equipList = _equipScrollView.GetChildComponent<KList>("mask/content");
            _equipList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            EventDispatcher.AddEventListener(EquipChannelEvents.CloseAllPreviousPanel, ClosePanelWithPreviousPanel);
        }

        private void RefreshEquipList(MenuItemData itemData)
        {
            int playerLevel = PlayerAvatar.Player.level;
            int equipLevel = equip_channel_helper.GetEquipLevel(playerLevel);
            List<EquipItemData> equipItemDataList;
            if (itemData.isRecommend == true)
            {
                equipItemDataList = GetRecommendEquipList(equipLevel);
            }
            else
            {
                equipItemDataList = GetEquipList(equipLevel ,itemData);
            }
            _equipList.RemoveAll();
            _equipList.SetDataList<EquipItem>(equipItemDataList);
        }

        private List<EquipItemData> GetEquipList(int equipLevel, MenuItemData itemData)
        {
            List<EquipItemData> result = new List<EquipItemData>();
            for (int i = public_config.EQUIP_POS_WEAPON; i <= public_config.EQUIP_POS_LEFTRING; i++)
            {
                int index = item_channel_helper.GetIndex(equipLevel, itemData.quality, i, itemData.starCount);
                if (index != 0)
                {
                    result.Add(new EquipItemData(index));
                }
            }
            return result;
        }

        private List<EquipItemData> GetRecommendEquipList(int equipLevel)
        {
            List<EquipItemData> result = new List<EquipItemData>();
            for (int i = public_config.EQUIP_POS_WEAPON; i <= public_config.EQUIP_POS_LEFTRING; i++)
            {
                EquipItemInfo equipInfo = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo(i) as EquipItemInfo;
                if (equipInfo == null)
                {
                    int index = item_channel_helper.GetDefaultLevelEquipIndex(equipLevel, i);
                    if (index != 0)
                    {
                        result.Add(new EquipItemData(index, true));
                    }
                }
                else
                {
                    int quality = equipInfo.Quality;
                    int position = (int)equipInfo.SubType;
                    int star = equipInfo.Star;
                    int index = item_channel_helper.GetNextLevelEquipIndex(equipLevel, quality, position, star);
                    if (index != 0)
                    {
                        result.Add(new EquipItemData(index, true));
                    }
                }
            }
            return result;
        }

        public override void OnShow(object data)
        {
            InitMenuList();
            RefreshEquipList(currentMenuData);
        }

        private void InitMenuList()
        {
            List<MenuItemData> menuItemDataList = GetMenuItemDataList(equip_channel_helper.GetAllEquipTypeCfg());
            currentMenuData = menuItemDataList[0];
            _menuList.RemoveAll();
            _menuList.SetDataList<MenuItem>(menuItemDataList);
            for (int i = 0; i < _menuList.ItemList.Count; i++)
            {
                _menuList.ItemList[i].onClick.AddListener(OnMenuSelected);
            }
        }

        private void OnMenuSelected(KList.KListItemBase item, int index)
        {
            for (int i = 0; i < _menuList.ItemList.Count; i++)
            {
                MenuItem menuItem = _menuList.ItemList[i] as MenuItem;
                if (i != index)
                {
                    menuItem._menuToggle.isOn = false;
                }
            }
            MenuItemData itemData = (item as MenuItem).Data as MenuItemData;
            if (itemData != currentMenuData)
            {
                RefreshEquipList(itemData);
                currentMenuData = itemData;
            }
        }

        private List<MenuItemData> GetMenuItemDataList(List<EquipDataWrapper> list)
        {
            List<MenuItemData> result = new List<MenuItemData>();
            // 第一行是设置推荐给玩家的装备列表
            MenuItemData recommendMenuData = new MenuItemData();
            recommendMenuData.isRecommend = true;
            recommendMenuData.nameId = 100906;
            result.Add(recommendMenuData);
            for (int i = 0; i < list.Count; i++)
            {
                MenuItemData item = new MenuItemData();
                item.id = list[i].id;
                item.nameId = list[i].descId;
                item.quality = list[i].quality;
                item.starCount = list[i].star;
                item.isRecommend = false;
                result.Add(item);
            }
            return result;
        }

        public override void OnClose()
        {
            EventDispatcher.RemoveEventListener(EquipChannelEvents.CloseAllPreviousPanel, ClosePanelWithPreviousPanel);
        }

        public void ClosePanelWithPreviousPanel()
        {
            ClosePanel();
        }
    }
}