﻿#region 模块信息
/*==========================================
// 文件名：MenuItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleEquipChannel.MenuItem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/22 14:06:32
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
namespace ModuleEquipChannel
{
    public class MenuItem : KList.KListItemBase
    {
        private MenuItemData _menuData;
        public KToggle _menuToggle;

        public override object Data
        {
            get
            {
                return _menuData;
            }
            set
            {
                if (value != null)
                {
                    _menuData = value as MenuItemData;
                    Refresh();
                }
            }
        }

        protected override void Awake()
        {
            _menuToggle = GetChildComponent<KToggle>("Toggle_zhuangbei");
            _menuToggle.onValueChanged.AddListener(OnToggleClick);
        }

        private void OnToggleClick(KToggle toggle, bool value)
        {
            if (value == true)
            {
                onClick.Invoke(this, Index);
            }
        }

        public override void Dispose()
        {
            _menuToggle.onValueChanged.RemoveListener(OnToggleClick);
        }

        private void Refresh()
        {
            TextWrapper[] textWrapper = gameObject.GetComponentsInChildren<TextWrapper>(true);
            for (int i = 0; i < textWrapper.Length; i++)
            {
                textWrapper[i].text = MogoLanguageUtil.GetContent(_menuData.nameId);
            }
        }
    }
}