﻿#region 模块信息
/*==========================================
// 文件名：MenuItemData
// 命名空间: GameLogic.GameLogic.Modules.ModuleEquipChannel.MenuItem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/22 14:06:46
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace ModuleEquipChannel
{
    public class MenuItemData
    {
        public int id;
        public int quality;
        public int starCount;
        public int nameId;
        public bool isRecommend = false;
    }
}