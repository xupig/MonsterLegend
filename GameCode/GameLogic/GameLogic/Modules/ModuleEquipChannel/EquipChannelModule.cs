﻿#region 模块信息
/*==========================================
// 文件名：EquipChannelModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleEquipChannel
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/22 11:07:45
// 描述说明：
// 其他：
//==========================================*/
#endregion
using Common.Base;

namespace ModuleEquipChannel
{
    public class EquipChannelModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.EquipChannel, "Container_EquipChannel", MogoUILayer.LayerUINotice, "ModuleEquipChannel.EquipChannelPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, true);
        }
    }
}