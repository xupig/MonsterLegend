﻿#region 模块信息
/*==========================================
// 文件名：EquipItemData
// 命名空间: GameLogic.GameLogic.Modules.ModuleEquipChannel.EquipItem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/22 14:06:19
// 描述说明：
// 其他：
//==========================================*/
#endregion

using GameData;
using GameMain.Entities;
namespace ModuleEquipChannel
{
    public class EquipItemData
    {
        public int equipId;
        public int functionId;
        public int shortDescId;
        public int longDescId;
        public bool isRecommend;

        public string viewKey;
        public string[] viewValues;

        public EquipItemData(int index, bool isRecommend = false)
        {
            if (XMLManager.equip_sources.ContainsKey(index))
            {
                equip_sources cfg = XMLManager.equip_sources[index];
                int vocation = (int)PlayerAvatar.Player.vocation;
                equipId = 100000 * vocation + 10000 * (cfg.__equip_position % 10) +
                    1000 * cfg.__equip_quality_id + 10 * cfg.__equip_level + cfg.__equip_star;
                shortDescId = int.Parse(cfg.__desc[0]);
                longDescId = int.Parse(cfg.__desc[1]);
                foreach (var pair in cfg.__view)
                {
                    viewKey = pair.Key;
                    viewValues = pair.Value;
                }
                this.isRecommend = isRecommend;
             }
        }
    }
}