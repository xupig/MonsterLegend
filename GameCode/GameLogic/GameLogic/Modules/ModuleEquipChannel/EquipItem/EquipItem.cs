﻿#region 模块信息
/*==========================================
// 文件名：EquipItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleEquipChannel.EquipItem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/22 14:05:56
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using ModuleCommonUI;
using MogoEngine.Events;
namespace ModuleEquipChannel
{
    public class EquipItem : KList.KListItemBase
    {
        private EquipItemData _equipData;
        private ItemGrid _equipIcon;
        private StateImage _recommendImage;
        private StateText _shortDesc;
        private StateText _longDesc;
        private KButton _goButton;

        public override object Data
        {
            get
            {
                return _equipData;
            }
            set
            {
                if (value != null)
                {
                    _equipData = value as EquipItemData;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            _equipIcon.SetItemData(_equipData.equipId);
            _recommendImage.Visible = _equipData.isRecommend;
            _shortDesc.CurrentText.text = MogoLanguageUtil.GetContent(_equipData.shortDescId);
            _longDesc.CurrentText.text = MogoLanguageUtil.GetContent(_equipData.longDescId);
            CheckButtonShow();
        }

        private void CheckButtonShow()
        {
            if (_equipData.viewKey == null || _equipData.viewKey == string.Empty)
            {
                _goButton.Visible = false;
            }
        }
        protected override void Awake()
        {
            _equipIcon = GetChildComponent<ItemGrid>("Container_wupin");
            _recommendImage = GetChildComponent<StateImage>("Image_tiaokuang");
            _shortDesc = GetChildComponent<StateText>("Label_txtMingcheng");
            _longDesc = GetChildComponent<StateText>("Label_txtMiaoshu");
            _goButton = GetChildComponent<KButton>("Button_qianwang");
            _goButton.onClick.AddListener(onGoBtnClicked);
            _equipIcon.onClick = OnEquipIconClick;
        }

        private void OnEquipIconClick()
        {
            ToolTipsManager.Instance.ShowItemTip(_equipData.equipId, PanelIdEnum.Empty);
        }

        private void onGoBtnClicked()
        {
            EventDispatcher.TriggerEvent(EquipChannelEvents.CloseAllPreviousPanel);
            function_helper.Follow(_equipData.viewKey, _equipData.viewValues, null);
        }

        public override void Dispose()
        {

        }
    }
}