﻿using Common.Base;
using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleVip
{
    public class VipModule:BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.Vip, "Container_VipPanel", MogoUILayer.LayerUIPanel, "ModuleVip.VipPanel", BlurUnderlay.Have, HideMainUI.Hide);

            RegisterPanelModule(PanelIdEnum.Vip, PanelIdEnum.Charge);
        }

    }
}
