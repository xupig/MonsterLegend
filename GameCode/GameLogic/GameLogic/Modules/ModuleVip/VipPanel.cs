﻿using Common.Base;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using ModuleCommonUI;
using MogoEngine.Events;

namespace ModuleVip
{
    public class VipPanel : BasePanel
    {
        private VipInformationView _vipInformationView;
        private VipDetailItemGridView _vipDetailItemGridView;
        private VipCategoryView _vipCategoryView;
        private KContainer _vipBGContainer;
        private int currentIndex = 0;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Vip; }
        }

        protected override void Awake()
        {
            _vipInformationView = AddChildComponent<VipInformationView>("Container_VIP/Container_VIPbiaoti");
            _vipDetailItemGridView = AddChildComponent<VipDetailItemGridView>("Container_VIP");
            _vipCategoryView = AddChildComponent<VipCategoryView>("Container_vipCategory");
            _vipBGContainer = GetChildComponent<KContainer>("Container_VIPbg");
        }

        private void AddEventListener()
        {

            _vipCategoryView.onSelectIndexChanged.AddListener(OnCategoryIndexChange);
            _vipDetailItemGridView.onScrollPageIndexChange.AddListener(OnScrollPageIndexChange);
            EventDispatcher.AddEventListener(VipEvents.ON_RECEIVE_VIP_REWARD_RECORD, RefreshVipView);
        }


        private void RemoveEventListener()
        {
            _vipCategoryView.onSelectIndexChanged.RemoveListener(OnCategoryIndexChange);
            _vipDetailItemGridView.onScrollPageIndexChange.RemoveListener(OnScrollPageIndexChange);
            EventDispatcher.RemoveEventListener(VipEvents.ON_RECEIVE_VIP_REWARD_RECORD, RefreshVipView);
        }

        private void RefreshVipView()
        {
            _vipCategoryView.Refresh();
        }

        private void OnScrollPageIndexChange(int currentPage)
        {
            _vipCategoryView.OnScrollPageIndexChange(currentPage);
            currentIndex = currentPage;
            
        }

        private void OnCategoryIndexChange(int selectIndex)
        {
            _vipDetailItemGridView.OnCategoryIndexChange(selectIndex);
            currentIndex = selectIndex;
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            int vipLevel = 1;
            if (data != null)
            {
                vipLevel = (int)data;
                _vipCategoryView.OnScrollPageIndexChange(vipLevel - 1);
                _vipDetailItemGridView.OnCategoryIndexChange(vipLevel - 1);
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }
    }
}
