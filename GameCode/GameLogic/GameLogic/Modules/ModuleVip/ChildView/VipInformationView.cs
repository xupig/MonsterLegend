﻿using Common.ClientConfig;
using Common.Data;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleVip
{
    public class VipInformationView : KContainer
    {
        private VipLevelItem _currentVipLevel;
        private VipLevelUpInfoView _levelUpInfoView;
        protected override void Awake()
        {
            _currentVipLevel = AddChildComponent<VipLevelItem>("Container_VIPBT");
            _currentVipLevel.MoveDistanceX = -20;
            _levelUpInfoView = AddChildComponent<VipLevelUpInfoView>("Container_jindu");
        }

        protected override void Start()
        {
            RefleshLevelUPProgress();
            RefleshVipLevel();
        }

        private void AddEventListener()
        {
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.accumulated_charge, OnAccumulatedChargeChanged);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.vip_level, RefleshVipLevel);
        }

        private void RemoveEventListener()
        {
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.accumulated_charge, OnAccumulatedChargeChanged);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.vip_level, RefleshVipLevel);
        }

        protected override void OnEnable()
        {
            RefleshLevelUPProgress();
            RefleshVipLevel();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void OnAccumulatedChargeChanged()
        {
            RefleshLevelUPProgress();
        }

        private void RefleshVipLevel()
        {
            _currentVipLevel.SetVipLevel(PlayerDataManager.Instance.VipData.CurrenVipLevel);
            VipManager.Instance.RequestGetRewardsRecord();
        }

        private void RefleshLevelUPProgress()
        {
            _levelUpInfoView.RefreshLevelUp(PlayerDataManager.Instance.VipData.CurrenVipLevel + 1);
        }
    }
}
