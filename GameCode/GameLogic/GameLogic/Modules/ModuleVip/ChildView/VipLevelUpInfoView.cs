﻿using Common.Base;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleVip
{
    public class VipLevelUpInfoView : KContainer
    {
        private VipLevelItem _nextVipLevel;

        private KContainer _maxLevelContainer;
        private KContainer _normalLevelContainer;
        private KContainer _progressBarContainer;
        private KContainer _diamondIcon;

        private StateText _diamondRequiredText;
        private KProgressBar _diamondRequiredProgressBar;
        private StateText _currentDiamondText;
        private StateText _maxLevelText;
        
        //充值按钮
        private KButton _payButton;
        protected override void Awake()
        {
            _maxLevelContainer = GetChildComponent<KContainer>("Container_manji");
            _maxLevelText = _maxLevelContainer.GetChildComponent<StateText>("Label_zuigaodengji");
            _normalLevelContainer = GetChildComponent<KContainer>("Container_weimanji");
            _diamondIcon = _normalLevelContainer.GetChildComponent<KContainer>("Container_icon");
            _nextVipLevel = _normalLevelContainer.AddChildComponent<VipLevelItem>("Container_VIPBT");
            _diamondRequiredText = _normalLevelContainer.GetChildComponent<StateText>("Label_txtChongzhishuju");
            MogoAtlasUtils.AddIcon(_diamondIcon.gameObject, item_helper.GetIcon(public_config.MONEY_TYPE_DIAMOND));
            _progressBarContainer = GetChildComponent<KContainer>("Container_jindutiao");
            _diamondRequiredProgressBar = _progressBarContainer.GetChildComponent<KProgressBar>("ProgressBar_jindu");
            _currentDiamondText = _progressBarContainer.GetChildComponent<StateText>("Label_txtDangqian");
            _payButton = GetChildComponent<KButton>("Button_chongzhi");
            _nextVipLevel.MoveDistanceX = -10;
            AddChildComponent<KParticle>("Button_chongzhi/fx_ui_15_VIP");
            AddEventListener();
        }

        public void RefreshLevelUp(int nextLevel)
        {
            string vipLevelCost = vip_helper.GetVipLevelCost(PlayerAvatar.Player.vip_level);
            string[] costStringArray = vipLevelCost.Split(',');
            int startCost = int.Parse(costStringArray[0]);
            int endCost = int.Parse(costStringArray[1]);
            int currentDiamond = (int)PlayerAvatar.Player.accumulated_charge;
            int nextLevelDiamondRequired = endCost + 1;
            int needDiamond = nextLevelDiamondRequired - currentDiamond;
            if (XMLManager.vip.ContainsKey(nextLevel))
            {
                _maxLevelContainer.Visible = false;
                _normalLevelContainer.Visible = true;
                _diamondRequiredText.CurrentText.text = needDiamond.ToString();
                _currentDiamondText.CurrentText.text = string.Format("{0}/{1}", currentDiamond.ToString(), nextLevelDiamondRequired.ToString());
                _diamondRequiredProgressBar.Value = currentDiamond / (float)nextLevelDiamondRequired;
                _nextVipLevel.SetVipLevel(nextLevel);
            }
            else
            {
                const int MAX_LEVEL_DESC_ID = 36022;
                _currentDiamondText.CurrentText.text = string.Format("{0}", currentDiamond.ToString());
                _diamondRequiredProgressBar.Value = currentDiamond / (float)nextLevelDiamondRequired;
                _normalLevelContainer.Visible = false;
                _maxLevelContainer.Visible = true;
                _maxLevelText.CurrentText.text = MogoLanguageUtil.GetContent(MAX_LEVEL_DESC_ID);
            }
        }

        private void AddEventListener()
        {
            _payButton.onClick.AddListener(OnPayButtonClicked);
        }

        private void RemoveEventListener()
        {
            _payButton.onClick.RemoveListener(OnPayButtonClicked);
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }

        private void OnPayButtonClicked()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.Charge);
        }
    }
}
