﻿using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleVip
{
    public class VipCategoryView : KContainer
    {
        public CategoryList _vipCategoryToggleGroup;
        private List<CategoryItemData> _toggleGroupDataList;
        public KComponentEvent<int> onSelectIndexChanged;
        protected override void Awake()
        {
            _vipCategoryToggleGroup = GetChildComponent<CategoryList>("List_categoryList");
            onSelectIndexChanged = new KComponentEvent<int>();
            InitCategoryToggleGroup();
            StartCoroutine(WaitForCategoryItemCreated());
        }

        public IEnumerator WaitForCategoryItemCreated()
        {
            while (_vipCategoryToggleGroup.ItemList.Count < _toggleGroupDataList.Count)
            {
                yield return null;
            }
            Refresh();
        }

        private void InitCategoryToggleGroup()
        {
            _toggleGroupDataList = new List<CategoryItemData>();
            for (int index = 1; index <= VipDataManager.MAX_VIP_LEVEL; index++)
            {
                _toggleGroupDataList.Add(CategoryItemData.GetCategoryItemData(string.Format("VIP {0}",index)));
            }
            _vipCategoryToggleGroup.SetDataList<CategoryListItem>(_toggleGroupDataList, 2);
            _vipCategoryToggleGroup.SelectedIndex = 0;
        }

        private void AddEventListener()
        {
            _vipCategoryToggleGroup.onSelectedIndexChanged.AddListener(OnSelectIndexChanged);
        }

        private void RemoveEventListener()
        {
            _vipCategoryToggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectIndexChanged);
        }

        private void OnSelectIndexChanged(CategoryList selectedItem, int selectIndex)
        {
            onSelectIndexChanged.Invoke(selectIndex);
        }
       
        public void OnScrollPageIndexChange(int currentPage)
        {
            _vipCategoryToggleGroup.SelectedIndex = currentPage;
        }

        public void Refresh()
        {
            List<KList.KListItemBase> toggleItemList = _vipCategoryToggleGroup.ItemList;
            VipRewardState vipRewardState;
            for (int index = 0; index < toggleItemList.Count; index++)
            {
                vipRewardState = PlayerDataManager.Instance.VipData.GetVipDataById(index + 1).vipRewardState;
                CategoryListItem item = toggleItemList[index] as CategoryListItem;
                item.SetPoint(vipRewardState == VipRewardState.RewardAvailable);
            }
        }

        protected override void OnEnable()
        {
            Refresh();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            _toggleGroupDataList.Clear();
            _toggleGroupDataList = null;
            _vipCategoryToggleGroup = null;
        }

    }
}
