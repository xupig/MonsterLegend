﻿using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using GameLoader.Utils;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleVip
{
    public class VipDetailItemGridView : KContainer
    {
        public static int currentVip;

        private KScrollPage _vipDetailScrollPage;
        private KList _vipDetailItemList;

        public KComponentEvent<int> onScrollPageIndexChange;

        protected override void Awake()
        {
            _vipDetailScrollPage = GetChildComponent<KScrollPage>("ScrollPage_neirong");
            _vipDetailItemList = GetChildComponent<KList>("ScrollPage_neirong/mask/content");
            onScrollPageIndexChange = new KComponentEvent<int>();
            Initialize();
        }

        private void Initialize()
        {
            _vipDetailScrollPage.TotalPage = VipDataManager.MAX_VIP_LEVEL;
            _vipDetailItemList.SetDirection(KList.KListDirection.LeftToRight, 1, 1);
            _vipDetailItemList.SetGap(0, 43);
            _vipDetailItemList.SetPadding(0, 0, 0, 21);
            List<int> dataList = new List<int>();
            dataList.Clear();
            for (int index = 0; index < VipDataManager.MAX_VIP_LEVEL; index++)
            {
                dataList.Add(index + 1);
            }
            _vipDetailItemList.SetDataList<VipDetailItemGrid>(dataList);
        }

        private void AddEventListener()
        {
            _vipDetailScrollPage.onCurrentPageChanged.AddListener(OnScrollPageIndexChange);
        }

        private void RemoveEventListener()
        {
            _vipDetailScrollPage.onCurrentPageChanged.RemoveListener(OnScrollPageIndexChange);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }
        
        public void OnCategoryIndexChange(int selectIndex)
        {
            int lastPage = _vipDetailScrollPage.CurrentPage;
            currentVip = selectIndex;
            _vipDetailScrollPage.CurrentPage = selectIndex;
        }

        private void OnScrollPageIndexChange(KScrollPage dragedScrollPage, int currentPage)
        {
            onScrollPageIndexChange.Invoke(currentPage);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }
    }
}
