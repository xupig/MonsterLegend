﻿using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleVip
{
    public class VipRewards : KContainer
    {
        private int _vipLevel = 1;
        private KList _vipRewardsList;
        private List<RewardsData> _rewardDataList;

        private KContainer _getConditionsContainer;
        private StateText _vipLevelRequiredText;

        private KButton _getRewardsButton;
        private StateImage _receivedImage;
        private KParticle _getRewardParticle;
        private bool isParticleShow;
        private KContainer[] _vipRewardStateContainerArray;

        string vipTipsStringTemplate; 

        protected override void Awake()
        {
            _vipRewardsList = GetChildComponent<KList>("ScrollView_zhuangbei/mask/content");
            _getConditionsContainer = GetChildComponent<KContainer>("Container_kelingqu01");
            _vipLevelRequiredText = GetChildComponent<StateText>("Container_kelingqu01/Label_txtKelingqu");
            vipTipsStringTemplate = _vipLevelRequiredText.CurrentText.text;
            _getRewardsButton = GetChildComponent<KButton>("Container_kelingqu02/Button_lingqu");
            _getRewardParticle = AddChildComponent<KParticle>("Container_kelingqu02/Button_lingqu/fx_ui_3_2_lingqu");
            _getRewardParticle.Stop();
            isParticleShow = false;
            MaskEffectItem _effectItem = gameObject.AddComponent<MaskEffectItem>();
            _effectItem.Init(_getRewardParticle.transform.parent.gameObject, ShowParticle);
            _receivedImage = GetChildComponent<StateImage>("Container_kelingqu03/Image_sharedyilingqu");
            _rewardDataList = new List<RewardsData>();
            _vipRewardStateContainerArray = new KContainer[3];
            _vipRewardStateContainerArray[0] = GetChildComponent<KContainer>("Container_kelingqu01");
            _vipRewardStateContainerArray[1] = GetChildComponent<KContainer>("Container_kelingqu02");
            _vipRewardStateContainerArray[2] = GetChildComponent<KContainer>("Container_kelingqu03");
            InitRewardList();
            SetRewardState(VipRewardState.RewardLocked);
        }

        private bool ShowParticle()
        {
            return isParticleShow;
        }

        private void AddEventListener()
        {
            _getRewardsButton.onClick.AddListener(OnGetButtonClicked);
            EventDispatcher.AddEventListener(VipEvents.ON_RECEIVE_VIP_REWARD_RECORD, RefreshGetRewardState);
        }

        private void RemoveEventListener()
        {
            _getRewardsButton.onClick.RemoveListener(OnGetButtonClicked);
            EventDispatcher.RemoveEventListener(VipEvents.ON_RECEIVE_VIP_REWARD_RECORD, RefreshGetRewardState);
        }

        private void RefreshGetRewardState()
        {
            SetRewardState(PlayerDataManager.Instance.VipData.GetVipDataById(_vipLevel).vipRewardState);
        }

        private void OnGetButtonClicked()
        {
            VipManager.Instance.RequestGetRewards(_vipLevel);
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }


        private void InitRewardList()
        {
            _vipRewardsList.SetDirection(KList.KListDirection.LeftToRight, 2, int.MaxValue);
            _vipRewardsList.SetGap(10, 20);
            _vipRewardsList.SetPadding(20, 0, 0, 25);
        }

        public void RefreshRewards(int vipLevel)
        {
            _vipLevel = vipLevel;
            _rewardDataList.Clear();
            Dictionary<string, string> rewardsDictionary = vip_helper.GetRewardsDictionaryByVipId(vipLevel);
            int itemCount;
            int itemId;
            foreach (KeyValuePair<string, string> valuePair in rewardsDictionary)
            {
                itemCount = int.Parse(valuePair.Value);
                itemId = int.Parse(valuePair.Key);
                _rewardDataList.Add(new RewardsData(itemId, itemCount));
            }
            _vipRewardsList.RemoveAll();
            _vipRewardsList.SetDataList<VipRewardsItem>(_rewardDataList);
            SetText(vipLevel);
            SetRewardState(PlayerDataManager.Instance.VipData.GetVipDataById(vipLevel).vipRewardState);
        }

        private void SetText(int vipLevel)
        {
            string vipX = string.Concat("VIP", vipLevel.ToString());
            _vipLevelRequiredText.CurrentText.text = string.Format(vipTipsStringTemplate, vipX);
        }

        private void SetRewardState(VipRewardState vipRewardState)
        {
            _vipRewardStateContainerArray[0].gameObject.SetActive(false);
            _vipRewardStateContainerArray[1].gameObject.SetActive(false);
            _vipRewardStateContainerArray[2].gameObject.SetActive(false);
            _vipRewardStateContainerArray[(int)vipRewardState].gameObject.SetActive(true);
            if (vipRewardState == VipRewardState.RewardAvailable)
            {
                _getRewardParticle.Play(true);
                isParticleShow = true;
            }
            else
            {
                _getRewardParticle.Stop();
                isParticleShow = false;
            }
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            _rewardDataList.Clear();
            _rewardDataList = null;
        }
    }


    public struct RewardsData
    {
        public int itemId;
        public int itemCount;
        public RewardsData(int itemId, int itemCount)
        {
            this.itemId = itemId;
            this.itemCount = itemCount;
        }
    }
}
