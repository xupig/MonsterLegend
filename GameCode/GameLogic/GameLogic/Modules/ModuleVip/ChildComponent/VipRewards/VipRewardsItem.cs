﻿using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.ExtendComponent;

namespace ModuleVip
{
    public class VipRewardsItem : KList.KListItemBase
    {
        private StateText _rewardsNameText;
        private StateText _rewardsCountText;

        private ItemGrid _rewardsGrid;
        private RewardsData _rewardData;
        public override Object Data
        {
            get { return _rewardData; }
            set
            {
                _rewardData = (RewardsData)value;
                Refresh();
            }
        }
        protected override void Awake()
        {
            _rewardsNameText = GetChildComponent<StateText>("Label_txtNr");
            _rewardsCountText = GetChildComponent<StateText>("Label_txtShuzi");
            _rewardsGrid = GetChildComponent<ItemGrid>("Container_wupin");
        }

        private void Refresh()
        {
            _rewardsGrid.SetItemData(_rewardData.itemId);
            _rewardsCountText.CurrentText.text = _rewardData.itemCount.ToString();
            _rewardsNameText.CurrentText.text = item_helper.GetName(_rewardData.itemId);
        }

        /// <summary>
        /// 珍贵物品的特殊效果
        /// </summary>
        private void SetPreciousEffect()
        {

        }

        public override void Dispose()
        {
        }
    }
}
