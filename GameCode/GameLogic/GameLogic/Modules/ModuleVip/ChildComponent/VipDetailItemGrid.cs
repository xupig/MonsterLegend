﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleVip
{
    public class VipDetailItemGrid : KList.KListItemBase
    {
        private VipRights _vipRights;
        private VipRewards _vipRewards;
        private int _vipLevel = 1;
        public override object Data
        {
            get
            {
                return _vipLevel;
            }
            set
            {
                if(value != null)
                {
                    _vipLevel = (int)value;
                    RefreshItemGrid();
                }
            }
        }

        protected override void Awake()
        {
            _vipRights = AddChildComponent<VipRights>("Container_xiangshoutequan");
            _vipRewards = AddChildComponent<VipRewards>("Container_tequan");
        }

        private void RefreshItemGrid()
        {
            _vipRights.RefreshItem(_vipLevel);
            _vipRewards.RefreshRewards(_vipLevel);
        }

        public override void Dispose()
        {
        }
    }
}
