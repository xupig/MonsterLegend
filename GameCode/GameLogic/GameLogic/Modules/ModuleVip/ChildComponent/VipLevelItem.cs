﻿using Game.UI.UIComponent;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleVip
{
    /// <summary>
    /// 用于显示vip等级
    /// </summary>
    public class VipLevelItem:KContainer
    {
        private List<GameObject> _stateImageNumList1;
        private List<GameObject> _stateImageNumList2;
        private int _currentVipLevel = -1;
        private KContainer _singleNumContainer;
        private Vector3 _singleNumStartPosition;
        //vip数字为个位数时，调整数字显示位置
        private float _moveDistanceX = -20;
        public float MoveDistanceX
        {
            get { return _moveDistanceX; }
            set 
            { 
                _moveDistanceX = value;
                RefreshGraphic();
            }
        }

        protected override void Awake()
        {
            _stateImageNumList1 = new List<GameObject>();
            _stateImageNumList2 = new List<GameObject>();
            GetAllNumStateImage("Container_shuwei1", _stateImageNumList1);
            GetAllNumStateImage("Container_shuwei2", _stateImageNumList2);
            _singleNumContainer = GetChildComponent<KContainer>("Container_shuwei2");
            _singleNumStartPosition = _singleNumContainer.transform.localPosition;
        }
        /// <summary>
        /// 获取所有数字图片,并隐藏
        /// </summary>
        private void GetAllNumStateImage(string prePath, List<GameObject> stateImageNumList)
        {
            string imageName;
            for (int index = 0; index < 10; index++)
            {
                imageName = prePath + "/Image_vip0" + index.ToString();
                stateImageNumList.Add(transform.FindChild(imageName).gameObject);
            }
        }

        private void HideAllStateImage(List<GameObject> stateImageNumList)
        {
            for (int index = 0; index < 10; index++)
            {
                stateImageNumList[index].SetActive(false);
            }
        }

        public void SetVipLevel(int levelToSet)
        {
            if (levelToSet == _currentVipLevel)
                return;
            HideAllStateImage(_stateImageNumList1);
            HideAllStateImage(_stateImageNumList2);
            _currentVipLevel = levelToSet;
            RefreshGraphic();
        }

        /// <summary>
        /// 此处假设Vip不超过99级
        /// </summary>
        private void RefreshGraphic()
        {
            if (_currentVipLevel < 0)
                return;
            int singleNum = _currentVipLevel % 10;
            int tensNum=(_currentVipLevel - singleNum) / 10;
            _stateImageNumList2[singleNum].SetActive(true);
            if (tensNum > 0)
            {
                _stateImageNumList1[tensNum].SetActive(true);
                _singleNumContainer.transform.localPosition = _singleNumStartPosition;
            }
            else
            {
                _singleNumContainer.transform.localPosition = _singleNumStartPosition + new Vector3(_moveDistanceX, 0, 0);
            }
        }

    }
}
