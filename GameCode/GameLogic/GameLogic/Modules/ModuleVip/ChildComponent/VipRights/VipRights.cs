﻿using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleVip
{
    public class VipRights : KContainer
    {
        private KPageableList _vipRightItemList;
        private List<string> _currentVipLevelRightsList;
        protected override void Awake()
        {
            _vipRightItemList = GetChildComponent<KPageableList>("ScrollView_tequan/mask/content");
            _currentVipLevelRightsList = new List<string>();
            InitRightsItem();
        }

        private void InitRightsItem()
        {
            _vipRightItemList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
        }

        public void RefreshItem(int vipLevel)
        {
            RefreshVipRightsList(vipLevel);
            _vipRightItemList.SetDataList<VipRightsItem>(_currentVipLevelRightsList, 3);
        }

        private void RefreshVipRightsList(int vipLevel)
        {
            _currentVipLevelRightsList = vip_helper.GetRightsIdListByVipId(vipLevel);
        }

        protected override void OnDestroy()
        {
            _vipRightItemList = null;
            _currentVipLevelRightsList.Clear();
            _currentVipLevelRightsList = null;
        }

    }
}
