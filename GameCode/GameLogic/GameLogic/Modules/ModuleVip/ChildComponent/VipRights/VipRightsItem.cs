﻿using Game.UI.UIComponent;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleVip
{
    public class VipRightsItem : KList.KListItemBase
    {
        private StateText _vipRightsDescribeText;
        private string _data;

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as string;
                    Refresh();
                }
            }
        }
        protected override void Awake()
        {
            _vipRightsDescribeText = GetChildComponent<StateText>("Label_txtxs");
        }

        public void Refresh()
        {
            _vipRightsDescribeText.CurrentText.text = _data;
        }

        public override void Dispose()
        {
            _vipRightsDescribeText = null;
        }
    }
}
