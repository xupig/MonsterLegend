﻿using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCloud
{
    public abstract class CloudBasePanel:BasePanel
    {
        public static float FADE_OUT = 0.2f;
        private const float CLEAR_TIME = 2f;
        public static float PINGPONG_OFFSET = 50;
        public const float PINGPONG_MIN_TIME = 1.4f;
        public const float PER_PINGPONG_TIME_GAP = 0.4f;
        public const int TOTAL_PINGPONG_TIME_NUM = 3;
        private KContainer _cloudRight;
        private KContainer _cloudLeft;
        private StateImage[] _leftImageArray;
        private StateImage[] _rightImageArray;
        protected RectTransform _leftRect;
        protected RectTransform _rightRect;

        protected TweenPosition _leftTweenPosition;
        protected TweenPosition _rightTweenPosition;

        protected Vector2 _originLeftPosition;
        protected Vector2 _originRightPosition;
        protected Vector2 _originLeftSize;
        protected Vector2 _originRightSize;

        private bool _canClearCloud = false;

        private int _isPlayingAnimatorCount = 0;

        protected float _showTime;

        protected bool IsIgnoreLoadingTime = false;

        private StringBuilder DebugString;
        private uint timerid = TimerHeap.INVALID_ID;

        protected override void Awake()
        {
            base.Awake();
            _cloudRight = GetChildComponent<KContainer>("Container_cloudright");
            _rightImageArray = _cloudRight.GetComponentsInChildren<StateImage>();
            _cloudLeft = GetChildComponent<KContainer>("Container_cloudleft");
            _leftImageArray = _cloudLeft.GetComponentsInChildren<StateImage>();
            _leftRect = GetChildComponent<RectTransform>("Container_cloudleft");
            _rightRect = GetChildComponent<RectTransform>("Container_cloudright");
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void SetData(object data)
        {
            DebugString = new StringBuilder();
            DebugString.Append("SetData");
            CloudGather();
        }

        public override void OnClose()
        {
            if (timerid != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(timerid);
                timerid = TimerHeap.INVALID_ID;
            }
            RemoveEventListener();
        }

        protected virtual void AddEventListener()
        {
            
        }

        protected virtual void RemoveEventListener()
        {
            
        }

        protected virtual void CloudGatherAfter()
        {

        }

        protected virtual void TryCloudClear()
        {
            _canClearCloud = true;
            CloudClear();
        }

        void Update()
        {
            if (Input.GetKey(KeyCode.S) && Input.GetKeyUp(KeyCode.D))
            {
                LoggerHelper.Error(DebugString);
                LoggerHelper.Error("IsPlayingAnimator:" + _isPlayingAnimatorCount);
            }
        }

        private void CloudGather()
        {
            _showTime = RealTime.time;
            DebugString.Append("CloudGather");
            StopPlayingAnimator();
            ResetCloudState();
            CloudGatherAfter();
            CheckCloudClear();

            timerid = TimerHeap.AddTimer(10000, 0, ExceptionClosePanel);
        }

        private void ExceptionClosePanel()
        {
            if (Application.isEditor)
            {
                LoggerHelper.Error("云界面不正常关闭,请把下面的错误信息发给李晓帅");
                LoggerHelper.Error(DebugString);
                LoggerHelper.Error("IsPlayingAnimator:" + _isPlayingAnimatorCount);
            }
            ClosePanel();
        }

        private void CloudClear()
        {
            DebugString.AppendFormat(";{0}:{1}", "CloudClear", _canClearCloud);
            if (_canClearCloud == true)
            {
                _canClearCloud = false;
                StopPlayingAnimator();
                ResetCloudState();
                DebugString.AppendFormat(";{0},{1}", RealTime.time - _showTime, IsIgnoreLoadingTime);
                if (RealTime.time - _showTime < 1.0f && IsIgnoreLoadingTime == false)
                {
                    FadeOutImage(_leftImageArray, FADE_OUT, CloudClearFinish);
                    FadeOutImage(_rightImageArray, FADE_OUT, CloudClearFinish);
                    DebugString.AppendFormat(";{0}:{1}", "isPlayingAnimatorCount", _isPlayingAnimatorCount);
                    return;
                }

                Vector2 screenOffset = new Vector2(UIManager.CANVAS_WIDTH - _originLeftSize.x, -(UIManager.CANVAS_HEIGHT - _originLeftSize.y)) / 2;
                _leftTweenPosition = TweenPosition.Begin(_leftRect.gameObject, CLEAR_TIME, new Vector2(_originLeftPosition.x - _originLeftSize.x, _originLeftPosition.y) + screenOffset, 0.0f, UITweener.Method.Linear, CloudClearFinish);
                _isPlayingAnimatorCount++;
                _rightTweenPosition = TweenPosition.Begin(_rightRect.gameObject, CLEAR_TIME, new Vector2(_originRightPosition.x + _originRightSize.x, _originRightPosition.y) + screenOffset, 0.0f, UITweener.Method.Linear, CloudClearFinish);
                _isPlayingAnimatorCount++;

                FadeOutImage(_leftImageArray, CLEAR_TIME, CloudClearFinish);
                FadeOutImage(_rightImageArray, CLEAR_TIME, CloudClearFinish);
                DebugString.AppendFormat(";{0}:{1}", "isPlayingAnimatorCount", _isPlayingAnimatorCount);
            }
        }

        private void StopPlayingAnimator()
        {
            _isPlayingAnimatorCount = 0;
            if (_leftTweenPosition != null)
            {
                _leftTweenPosition.Stop();
            }
            if (_rightTweenPosition != null)
            {
                _rightTweenPosition.Stop();
            }
            for (int i = 0; i < _leftImageArray.Length; i++)
            {
                TweenStateChangeableAlpha tweenAlpha = _leftImageArray[i].GetComponent<TweenStateChangeableAlpha>();
                if (tweenAlpha != null) { tweenAlpha.Stop(); }
            }

            for (int i = 0; i < _rightImageArray.Length; i++)
            {
                TweenStateChangeableAlpha tweenAlpha = _rightImageArray[i].GetComponent<TweenStateChangeableAlpha>();
                if (tweenAlpha != null) { tweenAlpha.Stop(); }
            }

        }

        private void FadeOutImage(StateImage[] imageArray, float duration, UITweener.OnFinished onFinished)
        {
            for (int i = 0; i < imageArray.Length; i++)
            {
                _isPlayingAnimatorCount++;
                imageArray[i].Alpha = 1.0f;
                TweenStateChangeableAlpha.Begin(imageArray[i].gameObject, duration, 0.0f, onFinished);
            }
        }

        private void CheckCloudClear()
        {
            DebugString.AppendFormat(";{0}:{1}", "CheckCloudClear", _canClearCloud);
            CloudClear();
        }

        private void CloudShowFinish(UITweener tween)
        {
            CheckCloudClear();
        }

        private void CloudClearFinish(UITweener tween)
        {
            DebugString.AppendFormat(";{0}:{1}", "CloudClearFinish", _isPlayingAnimatorCount);
            _isPlayingAnimatorCount--;
            if (_isPlayingAnimatorCount == 0)
            {
                StopPlayingAnimator();
                ClosePanel();
            }
        }

        private void ResetCloudState()
        {
            _leftRect.localScale = new Vector2(UIManager.CANVAS_WIDTH / _originLeftSize.x, UIManager.CANVAS_HEIGHT / _originLeftSize.y);
            _rightRect.localScale = new Vector2(UIManager.CANVAS_WIDTH / _originRightSize.x, UIManager.CANVAS_HEIGHT / _originRightSize.y);
            Vector2 screenOffset = new Vector2(UIManager.CANVAS_WIDTH - _originLeftSize.x, -(UIManager.CANVAS_HEIGHT - _originLeftSize.y)) / 2;
            _leftRect.anchoredPosition = _originLeftPosition + screenOffset;
            _rightRect.anchoredPosition = _originRightPosition + screenOffset;
            for (int i = 0; i < _leftImageArray.Length; i++)
            {
                _leftImageArray[i].Alpha = 1.0f;
            }

            for (int i = 0; i < _rightImageArray.Length; i++)
            {
                _rightImageArray[i].Alpha = 1.0f;
            }
        }
    }
}
