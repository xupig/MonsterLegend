﻿using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCloud
{
    public class CloudPanel : CloudBasePanel
    {
        protected override void Awake()
        {
            base.Awake();
            _leftRect.Rotate(new Vector3(0, 0, 180));
            _leftRect.anchorMax = new Vector2(0.5f, 0.5f);
            _leftRect.anchorMin = new Vector2(0.5f, 0.5f);
            _leftRect.pivot = new Vector2(0.5f, 0.5f);
            _rightRect.anchorMax = new Vector2(0.5f, 0.5f);
            _rightRect.anchorMin = new Vector2(0.5f, 0.5f);
            _rightRect.pivot = new Vector2(0.5f, 0.5f);
            _originRightPosition = _rightRect.anchoredPosition;
            _originLeftPosition = _leftRect.anchoredPosition;
            _originLeftSize = _leftRect.sizeDelta;
            _originRightSize = _rightRect.sizeDelta;
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.CloudPanel; }
        }

        protected override void AddEventListener()
        {
            EventDispatcher.AddEventListener(CloudEvents.CloseCloudPanel, TryCloudClear);
        }

        protected override void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(CloudEvents.CloseCloudPanel, TryCloudClear);
        }

        protected override void CloudGatherAfter()
        {
            PlayShowingAnimator();
        }

        private void PlayShowingAnimator()
        {
            float pingpongTime = GetOnePingpongTime();
            Vector2 screenOffset = new Vector2(UIManager.CANVAS_WIDTH - _originLeftSize.x, -(UIManager.CANVAS_HEIGHT - _originLeftSize.y)) / 2;
            _leftTweenPosition = TweenPosition.Begin(_leftRect.gameObject, pingpongTime, new Vector2(_originLeftPosition.x - PINGPONG_OFFSET, _originLeftPosition.y) + screenOffset, UITweener.Method.Linear);
            _leftTweenPosition.style = UITweener.Style.PingPong;
            _rightTweenPosition = TweenPosition.Begin(_rightRect.gameObject, pingpongTime, new Vector2(_originRightPosition.x + PINGPONG_OFFSET, _originRightPosition.y) + screenOffset, UITweener.Method.Linear);
            _rightTweenPosition.style = UITweener.Style.PingPong;
        }

        private float GetOnePingpongTime()
        {
            System.Random rdm = new System.Random();
            int pingpongTimeIndex = rdm.Next(0, TOTAL_PINGPONG_TIME_NUM);
            return PINGPONG_MIN_TIME + PER_PINGPONG_TIME_GAP * pingpongTimeIndex;
        }
    }

}
