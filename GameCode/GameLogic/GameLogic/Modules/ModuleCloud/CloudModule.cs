﻿
using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCloud
{
    public class CloudModule:BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.CloudPanel, "Container_CloudPanel", MogoUILayer.LayerUITop, "ModuleCloud.CloudPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);
            AddPanel(PanelIdEnum.SceneChange, "Container_SceneChangePanel", MogoUILayer.LayerUITop, "ModuleCloud.SceneChangePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);
        }
    }
}
