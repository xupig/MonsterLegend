﻿using Common.Base;
using Common.Events;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCloud
{
    public class SceneChangePanel:CloudBasePanel
    {
        protected override void Awake()
        {
            base.Awake();
            _leftRect.anchorMax = new Vector2(0.5f, 0.5f);
            _leftRect.anchorMin = new Vector2(0.5f, 0.5f);
            _leftRect.pivot = new Vector2(0.5f, 0.5f);
            _rightRect.anchorMax = new Vector2(0.5f, 0.5f);
            _rightRect.anchorMin = new Vector2(0.5f, 0.5f);
            _rightRect.pivot = new Vector2(0.5f, 0.5f);
            _originRightPosition = _rightRect.anchoredPosition;
            _originLeftPosition = _leftRect.anchoredPosition;
            _originLeftSize = _leftRect.sizeDelta;
            _originRightSize = _rightRect.sizeDelta;

            IsIgnoreLoadingTime = true;
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.SceneChange; }
        }

        protected override void AddEventListener()
        {
            EventDispatcher.AddEventListener(CloudEvents.CloseSceneChangePanel, TryCloudClear);
        }

        protected override void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(CloudEvents.CloseSceneChangePanel, TryCloudClear);
        }
    }
}
