﻿
using Common.ClientConfig;
using Common.Data;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTitle
{
    public class TitleToggleItem:KList.KListItemBase
    {
        private StateImage _equipIcon;
        private StateImage _lockIcon;
        private StateImage _newIcon;
        private StateImage _overdueIcon;
        private KContainer _checkmark;

        private StateText _txtTitleName;
        private StateText _txtCheckTitleName;
        private Locater _locater;


        private int _titleId = 0;

        protected override void Awake()
        {
            _equipIcon = GetChildComponent<StateImage>("Image_sharedyizhuangbei");
            _lockIcon = GetChildComponent<StateImage>("Image_suo");
            _newIcon = GetChildComponent<StateImage>("Image_NEW");
            _overdueIcon = GetChildComponent<StateImage>("Image_sharedguoqi");
            _checkmark = GetChildComponent<KContainer>("Container_checkmark");

            _txtTitleName = GetChildComponent<StateText>("Container_base/Label_Label");
            _txtCheckTitleName = GetChildComponent<StateText>("Container_checkmark/Label_Label");

            _locater = gameObject.AddComponent<Locater>();
        }

        public override void Show()
        {
            base.Show();
            AddEventListener();

        }

        public override void Hide()
        {
            base.Hide();
            RemoveEventListener();
        }

        public Locater Locater
        {
            get
            {
                return _locater;
            }
        }

        public override void Dispose()
        {
            _titleId = 0;
            RemoveEventListener();
        }

        public override bool IsSelected
        {
            get
            {
                return _checkmark.Visible;
            }
            set
            {
                _checkmark.Visible = value;
            }
        }

        public override object Data
        {
            get
            {
                return _titleId;
            }
            set
            {
                if ((int)value != 0)
                {
                    _titleId = (int)value;
                    SetDataDirty();
                }
            }
        }

        public override void DoRefreshData()
        {
            Refresh();
        }

        private void AddEventListener()
        {
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player as EntityAvatar, EntityPropertyDefine.title, Refresh);
        }

        private void RemoveEventListener()
        {
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player as EntityAvatar, EntityPropertyDefine.title, Refresh);
        }

        private void Refresh()
        {
            _checkmark.Visible = false;
            string titleName = title_helper.GetTitleName(_titleId);
            _txtTitleName.ChangeAllStateText(titleName);
            _txtCheckTitleName.ChangeAllStateText(titleName);

            switch(PlayerDataManager.Instance.TitleData.GetTitleState(_titleId))
            {
                case TitleStateType.NeverGet:
                case TitleStateType.OverdueChecked:
                    _newIcon.Visible = false;
                    _lockIcon.Visible = true;
                    _overdueIcon.Visible = false;
                    break;
                case TitleStateType.OverdueUncheck:
                    _newIcon.Visible = false;
                    _lockIcon.Visible = true;
                    _overdueIcon.Visible = true;
                    break;
                case TitleStateType.New:
                    _newIcon.Visible = true;
                    _lockIcon.Visible = false;
                    _overdueIcon.Visible = false;
                    break;
                case TitleStateType.Normal:
                     _newIcon.Visible = false;
                    _lockIcon.Visible = false;
                    _overdueIcon.Visible = false;
                    break;
            }
            _equipIcon.Visible = (PlayerAvatar.Player.title == _titleId);
        }
    }
}
