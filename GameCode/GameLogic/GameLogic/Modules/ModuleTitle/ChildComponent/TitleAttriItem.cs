﻿
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTitle
{
    public class TitleAttriItem : KList.KListItemBase
    {
        private KeyValuePair<int, float> _data;
        private StateText _txtAttriInfo;

        private const string attriFormat = "{0}+{1}";

        protected override void Awake()
        {
            _txtAttriInfo = GetChildComponent<StateText>("Label_txtJianglineir");
        }

        public override void Dispose()
        {
            _data = new KeyValuePair<int,float>(0,0);
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = (KeyValuePair<int, float>)value;
                Refresh();
            }
        }

        private void Refresh()
        {
            string attributeName = attri_config_helper.GetAttributeName(_data.Key);
            _txtAttriInfo.CurrentText.text = string.Format(attriFormat, attributeName, Math.Floor(_data.Value));
        }
    }
}
