﻿
using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTitle
{
    public class TitlePanel:BasePanel
    {
        private TitleDetailView _titleDetailView;
        private TitleListView _titleListView;
        private CategoryList _categoryList;

        protected override void Awake()
        {
            base.Awake();
            _titleDetailView = AddChildComponent<TitleDetailView>("Container_content");
            _titleListView = AddChildComponent<TitleListView>("ScrollView_chenghao");
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");

            CloseBtn = GetChildComponent<KButton>("Button_fanhui");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Title; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            TitleManager.Instance.RequestTitleBag();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _titleListView.onSelectedTitleItemChanged.AddListener(OnSelectedTitleItemChange);
            _categoryList.onSelectedIndexChanged.AddListener(OnSelectedCategoryChanged);

            EventDispatcher.AddEventListener<int>(TitleEvents.UpdateTitleData, RefreshTitleData);
            EventDispatcher.AddEventListener(TitleEvents.InitTitleData, RefreshCategoryList);
        }

        private void RemoveEventListener()
        {
            _titleListView.onSelectedTitleItemChanged.RemoveListener(OnSelectedTitleItemChange);
            _categoryList.onSelectedIndexChanged.RemoveListener(OnSelectedCategoryChanged);

            EventDispatcher.RemoveEventListener<int>(TitleEvents.UpdateTitleData, RefreshTitleData);
            EventDispatcher.RemoveEventListener(TitleEvents.InitTitleData, RefreshCategoryList);
        }

        private void OnSelectedCategoryChanged(CategoryList list, int index)
        {
            _titleListView.ShowTitleList((TitleType)index);
        }

        private void OnSelectedTitleItemChange(int titleId)
        {
            _titleDetailView.ShowSelectedTitle(titleId);
        }

        private void RefreshTitleData(int titleId)
        {
            RefreshCategoryList();
        }

        private void RefreshCategoryList()
        {
            List<CategoryItemData> toggleNameList = new List<CategoryItemData>();

            toggleNameList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(9500009)));
            if (PlayerDataManager.Instance.TitleData.GetGetTitleIdList().Count != 0)
            {
                toggleNameList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(9500010)));
            }

            _categoryList.RemoveAll();
            _categoryList.SetDataList<CategoryListItem>(toggleNameList);
            _categoryList.SelectedIndex = 0;
            _titleListView.ShowTitleList(TitleType.AllTitle);
            _titleDetailView.ShowSelectedTitle(0);
        }
    }

    public enum TitleType
    {
        AllTitle = 0,
        AreadyGetTitle = 1,
    }
}
