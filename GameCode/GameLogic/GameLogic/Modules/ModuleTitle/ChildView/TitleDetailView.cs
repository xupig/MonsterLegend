﻿
using Common.Base;
using Common.Chat;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTitle
{
    public class TitleDetailView:KContainer
    {
        static string timeFormat = "yyyy-MM-dd";
        //static string remainTimeFormat = "
        static int NONE_TITLE = 0; 

        private StateText _txtLevel;
        private StateText _titleName;

        private IconContainer _iconContainer;

        private StateText _txtSelectedTitleDescribe;
        private StateText _txtSelectedTitleGetTime;
        private StateText _txtRemainTime;

        private KContainer _titleDescribeContainer;
        private KContainer _titleGetTimeContainer;
        private KContainer _titleRemainTimeContainer;

        private KButton _useTitleBtn;
        private KButton _showTitleBtn;
        private KButton _unuseTitleBtn;

        private int _selectedTitleId;

        private uint _timerId = TimerHeap.INVALID_ID;

        protected override void Awake()
        {
            base.Awake();
            _txtLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            _titleName = GetChildComponent<StateText>("Label_txtChenghao");
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _txtSelectedTitleDescribe = GetChildComponent<StateText>("Container_dachengyaoqiu/Label_txtDachengyaoqiu");
            _txtSelectedTitleGetTime = GetChildComponent<StateText>("Container_dachengshijian/Label_txtDachengshijian");
            _txtRemainTime = GetChildComponent<StateText>("Container_shengyushijian/Label_txtDachengshijian");
            _titleDescribeContainer = GetChildComponent<KContainer>("Container_dachengyaoqiu");
            _titleGetTimeContainer = GetChildComponent<KContainer>("Container_dachengshijian");
            _titleRemainTimeContainer = GetChildComponent<KContainer>("Container_shengyushijian");

            _useTitleBtn = GetChildComponent<KButton>("Button_shiyong");
            _showTitleBtn = GetChildComponent<KButton>("Button_zhanshi");
            _unuseTitleBtn = GetChildComponent<KButton>("Button_xiexia");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            _selectedTitleId = NONE_TITLE;
            RefreshAvatarLevel();
            RefreshCurrentUseTitle();
            RefreshHeadIcon();
            ShowSelectedTitle(NONE_TITLE);
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            RemoveTimer();
            base.OnDisable();
        }

        public void ShowSelectedTitle(int titleId)
        {
            RemoveTimer();
            _selectedTitleId = titleId;
            if (titleId == NONE_TITLE)
            {
                _titleDescribeContainer.Visible = false;
                _titleGetTimeContainer.Visible = false;
                _useTitleBtn.Visible = false;
                _showTitleBtn.Visible = false;
                _unuseTitleBtn.Visible = false;
                _titleRemainTimeContainer.Visible = false;
                return;
            }
            switch (PlayerDataManager.Instance.TitleData.GetTitleState(titleId))
            {
                case TitleStateType.NeverGet:
                case TitleStateType.OverdueChecked:
                case TitleStateType.OverdueUncheck:
                    ShowNotOwnTitleDetail();
                    break;
                case TitleStateType.New:
                case TitleStateType.Normal:
                    ShowOwnTitleDetail();
                    break;

            }

        }

        private void RemoveTimer()
        {
            if (_timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = TimerHeap.INVALID_ID;
            }
        }

        private void ShowNotOwnTitleDetail()
        {
            _titleDescribeContainer.Visible = true;
            _txtSelectedTitleDescribe.CurrentText.text = title_helper.GetTitleDescribe(_selectedTitleId);

            _titleGetTimeContainer.Visible = false;
            _titleRemainTimeContainer.Visible = false;
            _useTitleBtn.Visible = false;
            _showTitleBtn.Visible = false;
            _unuseTitleBtn.Visible = false;
        }

        private void ShowOwnTitleDetail()
        {
            _titleDescribeContainer.Visible = true;
            _txtSelectedTitleDescribe.CurrentText.text = title_helper.GetTitleDescribe(_selectedTitleId);

            _titleGetTimeContainer.Visible = true;
            PbTitleInfo titleInfo = PlayerDataManager.Instance.TitleData.GetTitleInfo(_selectedTitleId);
            DateTime dateTime = MogoTimeUtil.ServerTimeStamp2UtcTime((long)titleInfo.get_time);
            _txtSelectedTitleGetTime.CurrentText.text = dateTime.ToString(timeFormat);

            if (title_helper.GetTitleTimeLimit(_selectedTitleId) != 0)
            {
                _titleRemainTimeContainer.Visible = true;
                _timerId = TimerHeap.AddTimer(0, 60000, RefreshRemainingTimeLabel);
            }
            else
            {
                _txtRemainTime.CurrentText.text = MogoLanguageUtil.GetContent(9500607);
                _titleRemainTimeContainer.Visible = false;
            }

            if (PlayerAvatar.Player.title != _selectedTitleId)
            {
                _useTitleBtn.Visible = true;
                _unuseTitleBtn.Visible = false;
            }
            else
            {
                _unuseTitleBtn.Visible = true;
                _useTitleBtn.Visible = false;
            }
            _showTitleBtn.Visible = true;
        }

        private void RefreshRemainingTimeLabel()
        {
            PbTitleInfo titleInfo = PlayerDataManager.Instance.TitleData.GetTitleInfo(_selectedTitleId);
            DateTime endTime = MogoTimeUtil.ServerTimeStamp2UtcTime((long)titleInfo.get_time + title_helper.GetTitleTimeLimit(_selectedTitleId));
            DateTime nowTime = PlayerTimerManager.GetInstance().GetNowServerDateTime();
            TimeSpan timeSpan = endTime.Subtract(nowTime);
            int days = timeSpan.Days;
            int hours = timeSpan.Hours;
            if (days == 0 && hours == 0)
            {
                hours = 1;
            }

            _txtRemainTime.CurrentText.text = MogoLanguageUtil.GetContent(9500606, days, hours);
        }

        private void AddEventListener()
        {
            _useTitleBtn.onClick.AddListener(OnUseTitleButtonClick);
            _showTitleBtn.onClick.AddListener(OnShowTitleButtonClick);
            _unuseTitleBtn.onClick.AddListener(OnUnstallTitleButtonClick);

            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player as EntityAvatar, EntityPropertyDefine.title, OnUsedTitleChanged);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player as EntityAvatar, EntityPropertyDefine.level, RefreshAvatarLevel);

            EventDispatcher.AddEventListener<int>(TitleEvents.UpdateTitleData, RefreshTitleDetail);
        }

        private void RemoveEventListener()
        {
            _useTitleBtn.onClick.RemoveListener(OnUseTitleButtonClick);
            _showTitleBtn.onClick.RemoveListener(OnShowTitleButtonClick);
            _unuseTitleBtn.onClick.RemoveListener(OnUnstallTitleButtonClick);

            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player as EntityAvatar, EntityPropertyDefine.title, OnUsedTitleChanged);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player as EntityAvatar, EntityPropertyDefine.level, RefreshAvatarLevel);

            EventDispatcher.RemoveEventListener<int>(TitleEvents.UpdateTitleData, RefreshTitleDetail);
        }

        private void OnUsedTitleChanged()
        {
            RefreshCurrentUseTitle();
            ShowSelectedTitle(_selectedTitleId);
        }

        private void RefreshCurrentUseTitle()
        {
            if (PlayerAvatar.Player.title != 0)
            {
                _titleName.CurrentText.text = title_helper.GetTitleName(PlayerAvatar.Player.title);
            }
            else
            {
                _titleName.CurrentText.text = MogoLanguageUtil.GetContent(9500006);
            }
        }

        private void RefreshTitleDetail(int titleId)
        {
            if (titleId == _selectedTitleId)
            {
                ShowSelectedTitle(titleId);
            }
        }

        private void RefreshAvatarLevel()
        {
            _txtLevel.CurrentText.text = PlayerAvatar.Player.level.ToString();
        }

        private void RefreshHeadIcon()
        {
            _iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon(PlayerAvatar.Player.vocation.ToInt()));
        }

        private void OnUseTitleButtonClick()
        {
            TitleManager.Instance.RequestUseTitle((uint)_selectedTitleId);
        }

        private void OnShowTitleButtonClick()
        {
            PbTitleInfo titleInfo = PlayerDataManager.Instance.TitleData.GetTitleInfo(_selectedTitleId);
            UIManager.Instance.ShowPanel(PanelIdEnum.Chat, new ChatItemLinkWrapper(title_helper.GetTitleName((int)titleInfo.title_id), (int)titleInfo.title_id, titleInfo.get_time));
            UIManager.Instance.ClosePanel(PanelIdEnum.Information);
            UIManager.Instance.ClosePanel(PanelIdEnum.Title);
        }

        private void OnUnstallTitleButtonClick()
        {
            TitleManager.Instance.RequestUnuseTitle();
        }

    }
}
