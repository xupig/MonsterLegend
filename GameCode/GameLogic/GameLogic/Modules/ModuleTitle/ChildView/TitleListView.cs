﻿using Common.Data;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTitle
{
    public class TitleListView:KContainer
    {
        private KScrollView _titleListScrollView;
        private KList _titleKList;
        private RectTransform _contentRect;

        private Locater _attriContentLocater;
        private KList _attriList;
        private StateText _txtNoneAttri;

        private TitleType _titleType = TitleType.AllTitle;

        private const int PER_FRAME_CREATE_NUM = 8;

        public KComponentEvent<int> onSelectedTitleItemChanged = new KComponentEvent<int>();

        protected override void Awake()
        {
            base.Awake();

            _titleListScrollView = gameObject.GetComponent<KScrollView>();
            _titleKList = _titleListScrollView.GetChildComponent<KList>("mask/content");
            _contentRect = _titleListScrollView.GetChildComponent<RectTransform>("mask/content");

            _attriContentLocater = _titleListScrollView.AddChildComponent<Locater>("mask/content/content");
            _attriList = GetChildComponent<KList>("mask/content/content/List_jiangli");
            _txtNoneAttri = GetChildComponent<StateText>("mask/content/content/Label_txtwushuxing");

            _attriList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);

            _titleListScrollView.ScrollRect.vertical = false;
            _titleListScrollView.ScrollRect.horizontal = true;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        public void ShowTitleList(TitleType titleType)
        {
            _titleType = titleType;
            HideAttriContent();
            RefreshTitleList();
        }

        private void AddEventListener()
        {
            _titleKList.onItemClicked.AddListener(OnTitleItemClicked);
        }

        private void RemoveEventListener()
        {
            _titleKList.onItemClicked.RemoveListener(OnTitleItemClicked);
        }

        private void OnTitleItemClicked(KList list, KList.KListItemBase item)
        {
            if(item.IsSelected == false)
            {
                item.IsSelected = true;
                list.SelectedIndex = item.Index;
                ShowAttriContent(item.Index);
                DoLayout(item.Index);
                int titleId = (int)item.Data;
                if (CheckAndChangeTitleState(titleId) == true)
                {
                    item.SetDataDirty();
                }
                onSelectedTitleItemChanged.Invoke(titleId);
            }
            else
            {
                item.IsSelected = false;
                list.SelectedIndex = -1;
                HideAttriContent();
                onSelectedTitleItemChanged.Invoke(0);
                DoLayout(-1);
            }
        }

        private bool CheckAndChangeTitleState(int titleId)
        {
            PbTitleInfo titleInfo = PlayerDataManager.Instance.TitleData.GetTitleInfo(titleId);
            if (titleInfo != null && titleInfo.has_checked == 0)
            {
                TitleManager.Instance.RequestCheckTitle((uint)titleId);
                PlayerDataManager.Instance.TitleData.GetTitleInfo(titleId).has_checked = 1;
                return true;
            }
            return false;
        }

        private void RefreshTitleList()
        {
            List<int> showTitleList = GetShowTitleList();
            _titleKList.SetDataList<TitleToggleItem>(showTitleList, PER_FRAME_CREATE_NUM);
        }

        private List<int> GetShowTitleList()
        {
            switch (_titleType)
            {
                case TitleType.AllTitle:
                    List<int> titleIdList = title_helper.GetTitleIdList();
                    titleIdList.Sort(SortTitleList);
                    return titleIdList;
                case TitleType.AreadyGetTitle:
                    List<int> getTitleIdList = PlayerDataManager.Instance.TitleData.GetGetTitleIdList();
                    getTitleIdList.Sort(SortTitleList);
                    return getTitleIdList;
            }
            return null;
        }

        private void DoLayout(int contentInsertIndex)
        {
            float x = (_titleKList.ItemList[0] as TitleToggleItem).Locater.X;
            float width = 0;
            for (int i = 0; i < _titleKList.ItemList.Count; i++)
            {
                Locater locater = (_titleKList.ItemList[i] as TitleToggleItem).Locater;
                if (locater.gameObject.activeSelf == true)
                {
                    locater.X = x;
                    x += locater.Width;
                    width += locater.Width;
                }
                if (i == contentInsertIndex)
                {
                    _attriContentLocater.X = x - 40;
                    x += _attriContentLocater.Width - 40;
                    width += _attriContentLocater.Width -40;
                }
            }
            _contentRect.sizeDelta = new Vector2(width, _contentRect.sizeDelta.y);
        }

        private int SortTitleList(int first, int second)
        {
            PbTitleInfo firstTitleInfo = PlayerDataManager.Instance.TitleData.GetTitleInfo(first);
            PbTitleInfo secondTitleInfo = PlayerDataManager.Instance.TitleData.GetTitleInfo(second);
            if (firstTitleInfo != null && secondTitleInfo != null)
            {
                TitleStateType firstStateType = PlayerDataManager.Instance.TitleData.GetTitleState(first);
                TitleStateType secondStateType = PlayerDataManager.Instance.TitleData.GetTitleState(second);

                if (firstStateType != secondStateType)
                {
                    return firstStateType - secondStateType;
                }
                int getTimeGap = (int)(firstTitleInfo.get_time - secondTitleInfo.get_time);
                if (getTimeGap != 0)
                {
                    return getTimeGap;
                }
            }
            else if (firstTitleInfo != null)
            {
                return -1;
            }
            else if (secondTitleInfo != null)
            {
                return 1;
            }
            return first - second;
        }

        private void ShowAttriContent(int index)
        {
            _attriContentLocater.gameObject.SetActive(true);
            _attriList.RemoveAll();
            int titleId = (int)_titleKList.ItemList[index].Data;
            List<KeyValuePair<int, float>> attriKeyValueList = title_helper.GetTitleAttriEffect(titleId).ToList();

            if (attriKeyValueList.Count == 0)
            {
                ///9500013,此称号无属性奖励
                _txtNoneAttri.CurrentText.text = MogoLanguageUtil.GetContent(9500013);
            }
            else
            {
                _txtNoneAttri.Clear();
                _attriList.SetDataList<TitleAttriItem>(attriKeyValueList);
            }
        }

        private void HideAttriContent()
        {
            _attriContentLocater.gameObject.SetActive(false);
        }
    }
}
