﻿
using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTitle
{
    public class TitleModule:BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.TitleDetail, "Container_TitleDetailPanel", MogoUILayer.LayerUIToolTip, "ModuleTitle.TitleDetailPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.Title, "Container_TitlePanel", MogoUILayer.LayerUIPanel, "ModuleTitle.TitlePanel", BlurUnderlay.Have, HideMainUI.Hide);
        }
    }
}
