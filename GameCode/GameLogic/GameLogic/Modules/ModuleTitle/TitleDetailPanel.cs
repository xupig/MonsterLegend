﻿using Common.Base;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTitle
{
    public class TitleDetailPanel:BasePanel
    {
        private const string timeFormat = "yyyy-MM-dd";
        private const float TOP_PADDING = 30.0f;
        private const float TEXT_ROW_GAP = 5.0f;

        private List<KContainer> _containerList;
        private List<Locater> _locaterList;

        private StateText _txtNoneAttri;
        private StateText _txtTitleName;
        private StateText _txtTitleGetTime;
        private StateText _txtTitleDescribe;
        private StateImage _imgBg;
        private KList _attriList;

        protected override void Awake()
        {
            base.Awake();
            _containerList = new List<KContainer>();
            _locaterList = new List<Locater>();
            _containerList.Add(GetChildComponent<KContainer>("Container_chengjiuchakan/Container_chenghao"));
            _locaterList.Add(AddChildComponent<Locater>("Container_chengjiuchakan/Container_chenghao"));
            _containerList.Add(GetChildComponent<KContainer>("Container_chengjiuchakan/Container_wanchengshijian"));
            _locaterList.Add(AddChildComponent<Locater>("Container_chengjiuchakan/Container_wanchengshijian"));
            _containerList.Add(GetChildComponent<KContainer>("Container_chengjiuchakan/Container_shuoming"));
            _locaterList.Add(AddChildComponent<Locater>("Container_chengjiuchakan/Container_shuoming"));
            _containerList.Add(GetChildComponent<KContainer>("Container_chengjiuchakan/Container_jiangli"));
            _locaterList.Add(AddChildComponent<Locater>("Container_chengjiuchakan/Container_jiangli"));

            _txtTitleName = GetChildComponent<StateText>("Container_chengjiuchakan/Container_chenghao/Label_txtChenghaoming");
            _txtTitleGetTime = GetChildComponent<StateText>("Container_chengjiuchakan/Container_wanchengshijian/Label_txtShijian");
            _txtTitleDescribe = GetChildComponent<StateText>("Container_chengjiuchakan/Container_shuoming/Label_txtshuomingwenzi");
            _attriList = GetChildComponent<KList>("Container_chengjiuchakan/Container_jiangli/List_jiangli");
            _txtNoneAttri = GetChildComponent<StateText>("Container_chengjiuchakan/Container_jiangli/Label_txtwushuxing");

            CloseBtn = GetChildComponent<KButton>("Container_chengjiuchakan/Button_close");
            gameObject.AddComponent<KDummyButton>();

            _imgBg = GetChildComponent<StateImage>("Container_chengjiuchakan/ScaleImage_bg");
            _imgBg.AddAllStateComponent<Resizer>();

            _attriList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TitleDetail; }
        }

        public override void OnShow(object data)
        {
            SetData(data);
            RefreshSecondaryLayout();
            RefreshMasterLayout();
        }

        public override void OnClose()
        {
            
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            object[] dataArr = data as object[];
            int titleId = (int)dataArr[0];
            ulong titleGetTime = (ulong)dataArr[1];

            DateTime dateTime = MogoTimeUtil.ServerTimeStamp2UtcTime((long)titleGetTime);

            _txtTitleName.CurrentText.text = title_helper.GetTitleName(titleId);
            _txtTitleGetTime.CurrentText.text = dateTime.ToString(timeFormat);
            _txtTitleDescribe.CurrentText.text = title_helper.GetTitleDescribe(titleId);

            List<KeyValuePair<int, float>> attriKeyValueList = title_helper.GetTitleAttriEffect(titleId).ToList();

            if (attriKeyValueList.Count == 0)
            {
                _txtNoneAttri.CurrentText.text = MogoLanguageUtil.GetContent(9500013);
            }
            else
            {
                _txtNoneAttri.Clear();
                _attriList.SetDataList<TitleAttriItem>(attriKeyValueList);
            }
        }

        private void RefreshSecondaryLayout()
        {
            _txtTitleDescribe.RecalculateCurrentStateHeight();
            _containerList[2].RecalculateSize();
            _attriList.RecalculateSize();
            _containerList[3].RecalculateSize();
        }

        private void RefreshMasterLayout()
        {
            float height = 0;
            float y = _locaterList[0].Y;
            for (int i = 0; i < 4; i++)
            {
                _locaterList[i].Y = y;
                y -= (_locaterList[i].Height + TEXT_ROW_GAP);
                height += (_locaterList[i].Height + TEXT_ROW_GAP);
            }

            _imgBg.Height = height + 2 * TOP_PADDING;
        }
    }
}
