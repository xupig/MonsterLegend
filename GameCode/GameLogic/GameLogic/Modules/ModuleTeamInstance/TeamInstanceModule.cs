﻿using Common.Base;

namespace ModuleTeamInstance
{
    public class TeamInstanceModule : BaseModule
    {

        public TeamInstanceModule()
            : base()
        {
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.TeamInstance, "Container_TeamInstancePanel", MogoUILayer.LayerUIPanel, "ModuleTeamInstance.TeamInstancePanel", BlurUnderlay.Have, HideMainUI.Hide);
        }


    }
}
