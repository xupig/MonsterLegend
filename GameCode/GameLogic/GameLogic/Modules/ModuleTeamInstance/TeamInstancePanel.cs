﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI.Token;
using ModuleCopy;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ModuleTeamInstance
{
    public class TeamInstancePanel : BasePanel
    {
        //private KScrollRect _scrollRect;
        private KContainer _mapContainer;
        private TeamInstanceEntryView _entryView;
        private StateText _enterText;
        private KContainer _buttonContainer;
        private List<TeamInstanceButton> _buttonList;
        private KContainer _lineContainer;
        private KToggleGroup _titleToggle;
        private KContainer _bottomContainer;
        //private CopyBackground _background;
        private KButton _shopButton;
        private KParticle _shopParticle;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_mijingfuben/Button_close");
            //_scrollRect = AddChildComponent<KScrollRect>("Container_mijingfuben/ScaleImage_mask");
            _mapContainer = GetChildComponent<KContainer>("Container_mijingfuben/Container_BG");
            _enterText = GetChildComponent<StateText>("Container_mijingfuben/Container_dibian/Label_txtjinrucishu");
            _entryView = AddChildComponent<TeamInstanceEntryView>("Container_fubenrukou");
            _buttonContainer = GetChildComponent<KContainer>("Container_mijingfuben/Container_BG/Container_btn");
            _lineContainer = GetChildComponent<KContainer>("Container_mijingfuben/Container_BG/Container_jindu");
            _titleToggle = GetChildComponent<KToggleGroup>("Container_mijingfuben/ToggleGroup_biaoti");
            _bottomContainer = GetChildComponent<KContainer>("Container_mijingfuben/Container_dibian");
            AddChildComponent<CopyBackground>("Container_mijingfuben/Container_BG/Container_fbbg/Container_bg");
            _shopButton = GetChildComponent<KButton>("Container_mijingfuben/Container_dibian/Button_youaishangdian");
            _shopParticle = AddChildComponent<KParticle>("Container_mijingfuben/Container_dibian/Button_youaishangdian/fx_ui_3_2_lingqu");
            _shopButton.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(114009));
            //InitBackground();
            //InitScrollRect();
            InitCopyButton();
        }

        //private void InitBackground()
        //{
        //    Vector2 _backSize = (_background.transform as RectTransform).sizeDelta;
        //    Vector2 _parentSize = (_background.transform.parent as RectTransform).sizeDelta;
        //    _background.transform.localScale = new Vector3(_parentSize.x / _backSize.x, _parentSize.y / _backSize.y, 1);
        //}

        public override void OnEnterMap(int mapType)
        {
            Id.Close();
        }

        private void GetBuyCount()
        {
            int priceId = inst_type_operate_helper.GetTimePriceId(ChapterType.TeamCopy);
            TeamInstanceManager.Instance.GetBuyCount(priceId);
        }

        //private void InitScrollRect()
        //{
        //    _mapContainer.transform.SetParent(_scrollRect.transform, false);
        //    _scrollRect.content = _mapContainer.gameObject.GetComponent<RectTransform>();
        //    _scrollRect.movementType = ScrollRect.MovementType.Clamped;
        //}

        private void ShowBackgroundView()
        {
            CloseBtn.Visible = true;
            _titleToggle.Visible = true;
            _bottomContainer.Visible = true;
            _buttonContainer.Visible = true;
            _lineContainer.Visible = true;
        }

        private void HideBackgroundView()
        {
            CloseBtn.Visible = false;
            _titleToggle.Visible = false;
            _bottomContainer.Visible = false;
            _buttonContainer.Visible = false;
            _lineContainer.Visible = false;
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            ShowBackgroundView();
            GetBuyCount();
            RefreshTimesContent();
            RefreshCopyButtonView();
            RefreshShopBtn();
            //RefreshContentPosition();
            SetData(data);
        }

        public override void SetData(object data)
        {
            //_buyView.Visible = false;
            _entryView.Visible = false;
            if (data != null)
            {
                int instId = Convert.ToInt32(data);
                SetEntryViewInstanceId(instId);
            }
        }

        private void RefreshTimesContent()
        {
            _enterText.CurrentText.text = string.Concat(PlayerDataManager.Instance.TeamInstanceData.MaxEnterTimes - PlayerDataManager.Instance.TeamInstanceData.HasEnterTimes, "/", PlayerDataManager.Instance.TeamInstanceData.MaxEnterTimes);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TeamInstance; }
        }

        public void AddEventListener()
        {
            //_addButton.onClick.AddListener(OnAddButtonClick);
            _entryView.onClose.AddListener(ShowBackgroundView);
            //_buyView.onClose.AddListener(ShowBackgroundView);
            _shopButton.onClick.AddListener(OnShopButtonClick);
            EventDispatcher.AddEventListener<int>(TeamInstanceEvents.Set_Entry_View_Instance_Id, SetEntryViewInstanceId);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, RefreshCopyButtonView);
            EventDispatcher.AddEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshShopBtn);
        }

        public void RemoveEventListener()
        {
            //_addButton.onClick.RemoveListener(OnAddButtonClick);
            _entryView.onClose.RemoveListener(ShowBackgroundView);
            //_buyView.onClose.RemoveListener(ShowBackgroundView);
            _shopButton.onClick.RemoveListener(OnShopButtonClick);
            EventDispatcher.RemoveEventListener<int>(TeamInstanceEvents.Set_Entry_View_Instance_Id, SetEntryViewInstanceId);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, RefreshCopyButtonView);
            EventDispatcher.RemoveEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshShopBtn);
        }

        private void OnShopButtonClick()
        {
            PanelIdEnum.EquipSmelter.Show();//熔炼
            //PanelIdEnum.Token.Show(TokenType.FRIENDSHIP);
        }

        private void OnVipButtonClick()
        {
            PanelIdEnum.Charge.Show(3);
        }

        private void InitCopyButton()
        {
            _buttonList = new List<TeamInstanceButton>();
            List<chapters> chapters = chapters_helper.GetChapterListSortByPage(ChapterType.TeamCopy);
            for (int i = 0; i < _buttonContainer.transform.childCount; i++)
            {
                Transform child = _buttonContainer.transform.GetChild(i);
                TeamInstanceButton btn = child.gameObject.AddComponent<TeamInstanceButton>();
                if (i >= chapters.Count)
                {
                    btn.Visible = false;
                }
                else
                {
                    btn.SetButtonInstanceId(int.Parse(chapters[i].__instance_ids[0]));
                    _buttonList.Add(btn);
                }
            }
        }

        private TeamInstanceButton GetCurrentButton()
        {
            int minLevel = 0;
            TeamInstanceButton button = null;
            for (int i = 0; i < _buttonList.Count; i++)
            {
                if (_buttonList[i].Visible == true)
                {
                    instance ins = instance_helper.GetInstanceCfg(_buttonList[i].InstanceId);
                    if (instance_helper.GetMinLevel(ins) >= minLevel)
                    {
                        minLevel = instance_helper.GetMinLevel(ins);
                        button = _buttonList[i];
                    }
                }
            }
            return button;
        }

        private void RefreshShopBtn()
        {
            if (PlayerDataManager.Instance.EquipPointManager.CanSmelt() == true)
            {
                _shopParticle.Play(true);
            }
            else
            {
                _shopParticle.Stop();
            }
        }

        //private void RefreshContentPosition()
        //{
        //    TeamInstanceButton button = GetCurrentButton();
        //    if (button != null)
        //    {
        //        RectTransform rect = button.GetComponent<RectTransform>();
        //        Vector2 position = rect.localPosition + new Vector3(rect.sizeDelta.x / 2, -rect.sizeDelta.y / 2, 0) + rect.parent.localPosition;
        //        Vector2 size = _scrollRect.GetComponent<RectTransform>().sizeDelta;
        //        _scrollRect.horizontalNormalizedPosition = Mathf.Clamp01(position.x / size.x);
        //        _scrollRect.verticalNormalizedPosition = Mathf.Clamp01(1 + position.y / size.y);
        //    }
        //}

        private void RefreshCopyButtonView()
        {
            for (int i = 0; i < _buttonList.Count; i++)
            {
                TeamInstanceButton btn = _buttonList[i];
                instance ins = instance_helper.GetInstanceCfg(btn.InstanceId);
                int minLevel = instance_helper.GetMinLevel(ins);
                int maxLevel = instance_helper.GetMaxLevel(ins);
                bool visible = minLevel <= PlayerAvatar.Player.level && PlayerAvatar.Player.level <= maxLevel;
                btn.Visible = visible;
            }
            RefreshLineView();
        }

        private void RefreshLineView()
        {
            for (int i = 1; i < _buttonList.Count; i++)
            {
                TeamInstanceButton btn = _buttonList[i];
                GameObject go = _lineContainer.GetChild(string.Format("Container_jindu{0:00}", i));
                go.SetActive(btn.Visible);
            }
        }

        //private void OnAddButtonClick()
        //{
        //    _buyView.Visible = true;
        //    HideBackgroundView();
        //}

        private void SetEntryViewInstanceId(int instanceId)
        {
            _entryView.SetInstanceId(instanceId);
            HideBackgroundView();
        }

    }
}
