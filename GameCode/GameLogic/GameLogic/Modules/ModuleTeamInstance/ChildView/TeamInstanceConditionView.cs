﻿using Common.Events;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;

namespace ModuleTeamInstance
{
    public class TeamInstanceConditionView : KContainer
    {
        private StateText _countText;
        private StateText _numberText;
        private StateText _levelText;
        private int _instanceId;

        protected override void Awake()
        {
            _countText = GetChildComponent<StateText>("Container_NR01/Label_txtJinrucishu02");
            _numberText = GetChildComponent<StateText>("Container_NR02/Label_txtJinrurenshu02");
            _levelText = GetChildComponent<StateText>("Container_NR00/Label_txtJinrudengji02");
        }


        public void RefreshContent(int instId)
        {
            _instanceId = instId;
            RefreshLevelConditionContent();
            RefreshTimesContent();
            RefreshNumberConditionContent();
        }

        private void RefreshTimesContent()
        {
            _countText.CurrentText.text = MogoLanguageUtil.GetContent(83016, instance_helper.GetTeamCopyLeftTimes(_instanceId), instance_helper.GetTeamCopyTotalTimes(_instanceId));
        }

        private void RefreshNumberConditionContent()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int minNum = instance_helper.GetMinPlayerNum(ins);
            int maxNum = instance_helper.GetMaxPlayerNum(ins);
            if (minNum == maxNum)
            {
                string num = PlayerDataManager.Instance.TeamData.TeammateDic.Count >= minNum ? minNum.ToString() : ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, minNum.ToString());
                _numberText.CurrentText.text = MogoLanguageUtil.GetContent(49215, num);
            }
            else
            {
                string num = PlayerDataManager.Instance.TeamData.TeammateDic.Count >= minNum ? minNum.ToString() : ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, minNum.ToString());
                _numberText.CurrentText.text = MogoLanguageUtil.GetContent(83018, num, maxNum);
            }
        }

        private void RefreshLevelConditionContent()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int minLevel = instance_helper.GetMinLevel(ins);
            int maxLevel = instance_helper.GetMaxLevel(ins);
            string num = PlayerAvatar.Player.level >= minLevel ? minLevel.ToString() : ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, minLevel.ToString());
            _levelText.CurrentText.text = MogoLanguageUtil.GetContent(83020, num);
        }

        public void AddEventListener()
        {
            EventDispatcher.AddEventListener(TeamEvent.TEAM_REFRESH, RefreshNumberConditionContent);
        }

        public void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_REFRESH, RefreshNumberConditionContent);
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

    }
}
