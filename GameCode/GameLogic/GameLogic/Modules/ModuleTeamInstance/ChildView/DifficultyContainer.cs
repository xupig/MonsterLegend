﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTeamInstance
{
    public class DifficultyContainer : KContainer
    {
        private KToggleGroup _toggleGroup;

        public KComponentEvent<int> onDifficultyChange = new KComponentEvent<int>();

        public int Difficulty
        {
            get
            {
                return _toggleGroup.SelectIndex;
            }
            set
            {
                _toggleGroup.SelectIndex = value;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            _toggleGroup = gameObject.GetComponent<KToggleGroup>();
            List<KToggle> toggleList = _toggleGroup.GetToggleList();
            for (int i = 0; i < toggleList.Count; i++)
            {
                SetDifficultyLock(i, false);
            }
        }

        public void SetDifficultyCount(int count)
        {
            List<KToggle> toggleList = _toggleGroup.GetToggleList();
            for (int i = 0; i < toggleList.Count; i++)
            {
                toggleList[i].Visible = i < count;
                //SetDifficultyLock(i, i >= count);
            }
        }

        private void SetDifficultyLock(int index, bool isLock)
        {
            _toggleGroup[index].GetChild("image/suo").SetActive(isLock);
            _toggleGroup[index].GetChild("checkmark/suo").SetActive(isLock);
            if (isLock)
            {
                _toggleGroup[index].SetButtonDisable();
            }
            else
            {
                _toggleGroup[index].SetButtonActive();
            }
        }

        public void SetDifficultyLabel(int index, string label)
        {
            _toggleGroup[index].GetChildComponent<StateText>("image/label").ChangeAllStateText(label);
            _toggleGroup[index].GetChildComponent<StateText>("checkmark/label").ChangeAllStateText(label);
        }

        private void OnSelectedIndexChanged(KToggleGroup target, int index)
        {
            onDifficultyChange.Invoke(index);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
        }
    }
}
