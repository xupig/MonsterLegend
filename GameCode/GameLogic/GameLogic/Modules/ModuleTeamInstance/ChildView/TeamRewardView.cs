﻿using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTeamInstance
{
    public class TeamRewardView : KContainer
    {
        private KScrollView _itemScrollView;
        private KList _itemList;
        private int _instanceId;

        protected override void Awake()
        {
            base.Awake();
            _itemScrollView = GetChildComponent<KScrollView>("ScrollView_wupinliebiao");
            InitScrollView();
        }

        private void InitScrollView()
        {
            _itemList = _itemScrollView.GetChildComponent<KList>("mask/content");
            _itemList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue);
            _itemScrollView.ScrollRect.horizontal = true;
            _itemScrollView.ScrollRect.vertical = false;
            _itemScrollView.UpdateArrow();
            _itemList.SetGap(0, 28);
        }

        private void RefreshGetView()
        {
            List<int> rewardIdList = instance_reward_helper.GetRewardIdListByRewardLevel(_instanceId, RewardLevel.Team);
            Visible = (rewardIdList.Count > 0);
            List<RewardData> rewardDataList = new List<RewardData>();
            for (int i = 0; i < rewardIdList.Count; i++)
            {
                rewardDataList.Add(new RewardData(rewardIdList[i], 1));
            }
            _itemList.SetDataList<RewardGrid>(rewardDataList);
        }

        public void RefreshContent(int instanceId)
        {
            _instanceId = instanceId;
            RefreshGetView();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

    }
}
