﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTeamInstance
{
    public class TeamInstanceRightView : KContainer
    {
        private KButton _inviteButton;
        //private KButton _recordButton;
        private StateText _matchText;
        private StateText _costText;
        private KButton _matchButton;
        private KButton _callButton;
        private KButton _entryButton;
        private KButton _cancelButton;
        private KContainer _matchContainer;
        private TeamInstanceConditionView _conditionContainer;
        private KScrollView _itemScrollView;
        private KList _itemList;
        private CopyTeamView _teamView;
        private CopyPassConditionList _passView;
        private TeamRewardView _teamRewardView;

        private int _instanceId;

        protected override void Awake()
        {
            _teamView = AddChildComponent<CopyTeamView>("Container_duiyou");
            _inviteButton = GetChildComponent<KButton>("Container_bottomBtn/Button_yaoqing");
            //_recordButton = GetChildComponent<KButton>("Container_right/Container_upperBtn/Button_Gantang");
            //_recordButton.Visible = false;
            _matchContainer = GetChildComponent<KContainer>("Container_bottomBtn/Container_pipei");
            _matchText = GetChildComponent<StateText>("Container_bottomBtn/Container_pipei/Label_txtPipei");
            _costText = GetChildComponent<StateText>("Container_bottomBtn/Label_txtXuyaotili");
            _matchButton = GetChildComponent<KButton>("Container_bottomBtn/Container_pipei/Button_pipei");
            _callButton = GetChildComponent<KButton>("Container_bottomBtn/Button_hanren");
            _entryButton = GetChildComponent<KButton>("Container_bottomBtn/Button_tiaozhan");
            _cancelButton = GetChildComponent<KButton>("Container_bottomBtn/Container_pipei/Button_quxiao");
            _conditionContainer = AddChildComponent<TeamInstanceConditionView>("Container_tiaojian");
            _passView = AddChildComponent<CopyPassConditionList>("Container_tongguan");
            _itemScrollView = GetChildComponent<KScrollView>("Container_zhuangbei/ScrollView_wupinliebiao");
            _teamRewardView = AddChildComponent<TeamRewardView>("Container_tuanduijiangli");
            InitScrollView();
        }

        private void OnCallPeopelSucceed()
        {
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83026), PanelIdEnum.MainUIField);
        }

        private void InitScrollView()
        {
            _itemList = _itemScrollView.GetChildComponent<KList>("mask/content");
            _itemList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue);
            _itemScrollView.ScrollRect.horizontal = true;
            _itemScrollView.ScrollRect.vertical = false;
            _itemScrollView.UpdateArrow();
            _itemList.SetGap(0, 28);
        }


        public void RefreshContent(int instanceId)
        {
            _instanceId = instanceId;
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            RefreshCostView(ins);
            _conditionContainer.RefreshContent(_instanceId);
            _passView.Refresh(_instanceId);
            RefreshGetView();
            RefreshButtonView();
            RefreshMatchView();
            _teamView.RefreshContent(_instanceId);
            if (_teamRewardView != null)
            {
                _teamRewardView.RefreshContent(_instanceId);
            }
        }

        private void RefreshButtonView()
        {
            //if (_matchContainer == null) { return; }
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int minNum = instance_helper.GetMinPlayerNum(ins);
            if (instance_helper.GetAutoTeamMatchId(_instanceId) > 0 && PlayerDataManager.Instance.TeamData.TeammateDic.Count < minNum)
            {
                _matchContainer.Visible = true;
                _entryButton.Visible = false;
            }
            else
            {
                _matchContainer.Visible = false;
                _entryButton.Visible = true;
            }
        }

        private void RefreshCostView(instance ins)
        {
            int recommendFightForce = instance_helper.GetRecommendFightForce(ins);
            string minFightForceStr = recommendFightForce.ToString();
            if (PlayerAvatar.Player.fight_force <= 0.9f*recommendFightForce)
            {
                minFightForceStr = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, minFightForceStr);
            }
            else if (PlayerAvatar.Player.fight_force >= 1.1f * recommendFightForce)
            {
                minFightForceStr = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, minFightForceStr);
            }
            _costText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(1421), minFightForceStr);
        }

        private void RefreshGetView()
        {
            List<int> rewardIdList = instance_reward_helper.GetRewardIdListByRewardLevel(_instanceId, RewardLevel.S);
            List<RewardData> rewardDataList = item_reward_helper.GetRewardDataList(rewardIdList, PlayerAvatar.Player.vocation);
            rewardDataList = RewardData.FilterSameItemId(rewardDataList);
            _itemList.SetDataList<RewardGrid>(rewardDataList);
        }

        private bool _isMatching = false;
        private uint _matchTimerId = 0;
        private void RefreshMatchView()
        {
            //if (_matchText == null) { return; }
            ulong matchTime = PlayerDataManager.Instance.CopyData.GetMatchTime(_instanceId);
            ResetMatchTimer();
            if (matchTime > 0)
            {
                _matchTimerId = TimerHeap.AddTimer(0, 1000, OnMatchTextShowStep);
                _matchText.Visible = true;
                _costText.Visible = false;
                _matchButton.Visible = false;
                _cancelButton.Visible = true;
                _isMatching = true;
            }
            else
            {
                _matchText.Visible = false;
                _costText.Visible = true;
                _matchButton.Visible = true;
                _cancelButton.Visible = false;
                _isMatching = false;
            }
        }

        private void OnMatchTextShowStep()
        {
            uint waitTime = uint.Parse(global_params_helper.GetConfig(104).__value) * 1000;
            ulong currentTime = Global.serverTimeStamp;
            ulong matchTime = PlayerDataManager.Instance.CopyData.GetMatchTime(_instanceId);
            ulong leftTime = waitTime - (currentTime - matchTime);
            int second = Mathf.RoundToInt((float)leftTime / 1000);
            second = second > 0 ? second : 0;
            _matchText.CurrentText.text = MogoLanguageUtil.GetContent(83000, second);
        }

        private void ResetMatchTimer()
        {
            if (_matchTimerId != 0)
            {
                TimerHeap.DelTimer(_matchTimerId);
                _matchTimerId = 0;
            }
        }


        public void AddEventListener()
        {
            _inviteButton.onClick.AddListener(OnInviteButtonClick);
            _callButton.onClick.AddListener(OnCallButtonClick);
            _entryButton.onClick.AddListener(OnEntryButtonClick);
            //_recordButton.onClick.AddListener(OnRecordButtonClick);
            _matchButton.onClick.AddListener(OnMatchButtonClick);
            _cancelButton.onClick.AddListener(OnCancelButtonClick);
            EventDispatcher.AddEventListener(CopyEvents.ON_MATCH_MISSION_CHANGE, RefreshMatchView);
            EventDispatcher.AddEventListener(TeamInstanceEvents.On_Call_People_Succeed, OnCallPeopelSucceed);
            EventDispatcher.AddEventListener(TeamEvent.TEAM_REFRESH, RefreshButtonView);
        }

        public void RemoveEventListener()
        {
            _inviteButton.onClick.RemoveListener(OnInviteButtonClick);
            _callButton.onClick.RemoveListener(OnCallButtonClick);
            _entryButton.onClick.RemoveListener(OnEntryButtonClick);
            //_recordButton.onClick.RemoveListener(OnRecordButtonClick);
            _matchButton.onClick.RemoveListener(OnMatchButtonClick);
            _cancelButton.onClick.RemoveListener(OnCancelButtonClick);
            EventDispatcher.RemoveEventListener(CopyEvents.ON_MATCH_MISSION_CHANGE, RefreshMatchView);
            EventDispatcher.RemoveEventListener(TeamInstanceEvents.On_Call_People_Succeed, OnCallPeopelSucceed);
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_REFRESH, RefreshButtonView);
        }

        private void OnEntryButtonClick()
        {
            if (_isMatching)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.COPY_MATCHING_CAN_NOT_ENTRY));
                return;
            }
            CopyLogicManager.Instance.RequestEnterCopy(ChapterType.TeamCopy, () =>
                {
                    MissionManager.Instance.RequsetEnterMission(_instanceId);
                }, _instanceId);
        }

        private void OnRecordButtonClick()
        {

        }

        private void OnCallButtonClick()
        {
            if (_isMatching)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.COPY_MATCHING_CAN_NOT_CALL));
                return;
            }
            CopyLogicManager.Instance.RequestCallPeople(ChapterType.TeamCopy, () =>
            {
                TeamInstanceManager.Instance.CallPeople(team_helper.GetGameIdByInstanceId(_instanceId));
            }, _instanceId);
        }

        private void OnMatchButtonClick()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int maxNum = instance_helper.GetMaxPlayerNum(ins);
            if (PlayerDataManager.Instance.TeamData.TeammateDic.Count >= maxNum)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83033), PanelIdEnum.MainUIField);
                return;
            }
            int minLevel = instance_helper.GetMinLevel(ins);
            if (PlayerAvatar.Player.level < minLevel)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83027, minLevel), PanelIdEnum.MainUIField);
                return;
            }
            MissionManager.Instance.RequsetEnterMission(_instanceId, 1);
        }

        private void OnCancelButtonClick()
        {
            if (PlayerDataManager.Instance.TeamData.TeammateDic.Count > 0 && PlayerAvatar.Player.dbid != PlayerDataManager.Instance.TeamData.CaptainId)
            {
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(97024));
                return;
            }
            MissionManager.Instance.RequsetCancelMatchMission(_instanceId);
        }

        private void OnInviteButtonClick()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int maxNum = instance_helper.GetMaxPlayerNum(ins);
            if (PlayerDataManager.Instance.TeamData.TeammateDic.Count >= maxNum)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83031), PanelIdEnum.MainUIField);
                return;
            }
            view_helper.OpenView(112, new InteractivePanelParameter(_instanceId));
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            ResetMatchTimer();
        }
    }
}
