﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Common.Base;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils;
using UnityEngine.Events;

using Game.UI.UIComponent;
using Common.ServerConfig;
using GameMain.GlobalManager;
using Common.ExtendComponent;
using GameMain.Entities;
using Common.ClientConfig;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using Common.Utils;
using Common.Global;
using UnityEngine.UI;
using GameData;
using Game.UI;

namespace ModuleTeamInstance
{
    public class TeamInstanceButton : KContainer
    {
        private int _instanceId;

        private KButton _button;
        private StateImage _image;
        private StateText _text;

        public int InstanceId
        {
            get
            {
                return _instanceId;
            }
        }

        protected override void Awake()
        {
            _button = GetComponent<KButton>();
            _image = GetChild(0).GetComponent<StateImage>();
            _text = GetChild(2).GetComponent<StateText>();
            SetStateTextAlignment(_text);
            _button.onClick.AddListener(OnCopyButtonClick);
        }

        public void SetButtonInstanceId(int instanceId)
        {
            _instanceId = instanceId;
            RefreshContent();
        }

        private void OnCopyButtonClick()
        {
            if (_instanceId == 0)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83001), PanelIdEnum.MainUIField);
            }
            else
            {
                EventDispatcher.TriggerEvent<int>(TeamInstanceEvents.Set_Entry_View_Instance_Id, _instanceId);
            }
        }

        private void SetStateImageWrapper(StateImage image, float value)
        {
            for (int i = 0; i < image.StateCount; i++)
            {
                ImageWrapper wrapper = image.GetStateComponent(i);
                wrapper.SetGray(value);
            }
        }

        private void SetStateTextAlignment(StateText text)
        {
            for (int i = 0; i < text.StateCount; i++)
            {
                Text txt = text.GetStateComponent(i);
                txt.alignment = TextAnchor.UpperCenter;
            }
        }

        private void RefreshContent()
        {
            if (_instanceId == 0)
            {
                SetStateImageWrapper(_image, 0);
                _text.ChangeAllStateText(MogoLanguageUtil.GetContent(83001));
            }
            else
            {
                SetStateImageWrapper(_image, 1);
                _text.ChangeAllStateText(chapters_helper.GetName(chapters_helper.GetChapterId(_instanceId)));
            }
        }

        protected override void OnEnable()
        {
            RefreshContent();
        }

        protected override void OnDisable()
        {


        }

    }
}
