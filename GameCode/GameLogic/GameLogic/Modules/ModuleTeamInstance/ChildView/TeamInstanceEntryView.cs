﻿using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTeamInstance
{
    public class TeamInstanceEntryView : KContainer
    {
        private TeamInstanceRightView _rightViewOne;
        private TeamInstanceRightView _rightViewTwo;
        private int _instanceId;
        private KButton _closeButton;
        //private StateText _nameText;
        private KContainer _leftContainer;
        private KContainer _rightContainer;
        private Vector2 _rightContainerPos;
        //private IconContainer _containerIcon;
        private DifficultyContainer _difficultyContainer;

        public KComponentEvent onClose = new KComponentEvent();

        protected override void Awake()
        {
            _closeButton = GetChildComponent<KButton>("Button_close");
            _leftContainer = GetChildComponent<KContainer>("Container_left");
            _rightContainer = GetChildComponent<KContainer>("Container_right");
            //_nameText = GetChildComponent<StateText>("Container_left/Container_biaoti/Label_txtzuduimijing");
            //_containerIcon = AddChildComponent<IconContainer>("Container_left/Container_mijing");
            _rightViewOne = AddChildComponent<TeamInstanceRightView>("Container_right/Container_right01");
            _rightViewTwo = AddChildComponent<TeamInstanceRightView>("Container_right/Container_right02");
            _difficultyContainer = AddChildComponent<DifficultyContainer>("Container_left/ToggleGroup_category");
            _rightContainerPos = (_rightContainer.transform as RectTransform).anchoredPosition;
        }

        public void SetInstanceId(int instanceId)
        {
            _instanceId = instanceId;
            this.Visible = true;
            RefreshContent();
        }

        private void RefreshContent()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            //_nameText.CurrentText.text = instance_helper.GetTeamInstanceName(_instanceId);
            //_containerIcon.SetIcon(instance_helper.GetIcon(_instanceId));
            RefreshDifficulty();
            List<int> rewardIdList = instance_reward_helper.GetRewardIdListByRewardLevel(_instanceId, RewardLevel.Team);
            if (rewardIdList.Count == 0)
            {
                _rightViewOne.Visible = true;
                _rightViewTwo.Visible = false;
                _rightViewOne.RefreshContent(_instanceId);
            }
            else
            {
                _rightViewOne.Visible = false;
                _rightViewTwo.Visible = true;
                _rightViewTwo.RefreshContent(_instanceId);
            }
        }

        private void RefreshDifficulty()
        {
            List<chapters> chapters = chapters_helper.GetChapterListSortByPage(ChapterType.TeamCopy);
            int page = chapters_helper.GetInstancePage(ChapterType.TeamCopy, _instanceId);
            int difficultyCount = chapters[page - 1].__instance_ids.Count;
            _difficultyContainer.SetDifficultyCount(difficultyCount);
            for (int i = 0; i < difficultyCount; i++)
            {
                _difficultyContainer.SetDifficultyLabel(i, MogoLanguageUtil.GetContent(83035 + i));
            }
            int index = chapters_helper.GetInstanceIndex(ChapterType.TeamCopy, _instanceId);
            _difficultyContainer.Difficulty = index;
            DoLayout(difficultyCount);
        }

        private void DoLayout(int difficultyCount)
        {
            _difficultyContainer.RecalculateSize();
            RectTransform parentRect = _leftContainer.transform.parent as RectTransform;
            RectTransform leftRect = _leftContainer.transform as RectTransform;
            RectTransform rightRect = _rightContainer.transform as RectTransform;
            leftRect.anchoredPosition = new Vector2(leftRect.anchoredPosition.x, -parentRect.sizeDelta.y * 0.5f + leftRect.sizeDelta.y * 0.5f);
            if (difficultyCount > 1)
            {
                _leftContainer.Visible = true;
                rightRect.anchoredPosition = _rightContainerPos;
                TweenViewMove.Begin(_leftContainer.gameObject, MoveType.Show, MoveDirection.Left, true);
            }
            else
            {
                _leftContainer.Visible = false;
                rightRect.anchoredPosition = new Vector2(parentRect.sizeDelta.x * 0.5f - rightRect.sizeDelta.x * 0.5f, rightRect.anchoredPosition.y);
            }
            TweenViewMove.Begin(_rightContainer.gameObject, MoveType.Show, MoveDirection.Right, true);
        }

        private void OnDifficultyChange(int index)
        {
            List<chapters> chapters = chapters_helper.GetChapterListSortByPage(ChapterType.TeamCopy);
            int page = chapters_helper.GetInstancePage(ChapterType.TeamCopy, _instanceId);
            if (index < chapters.Count)
            {
                List<int> instanceIds = global_helper.ConvertToIntList(chapters[page-1].__instance_ids);
                SetInstanceId(instanceIds[index]);
            }
        }

        public void AddEventListener()
        {
            _closeButton.onClick.AddListener(OnCloseButtonClick);
            _difficultyContainer.onDifficultyChange.AddListener(OnDifficultyChange);
        }

        public void RemoveEventListener()
        {
            _closeButton.onClick.RemoveListener(OnCloseButtonClick);
            _difficultyContainer.onDifficultyChange.RemoveListener(OnDifficultyChange);
        }

        private void OnCloseButtonClick()
        {
            Visible = false;
            onClose.Invoke();
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

    }
}
