﻿#region 模块信息
/*==========================================
// 文件名：SkillCgPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleSkill
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/26 15:03:54
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleSkill
{
    public class SkillCgPanel:BasePanel
    {
        private StateText _txtCurrentContent;
        //private StateText _txtNextContent;

        private KContainer _currentContainer;
        //private KContainer _nextContainer;

        private KButton _btnPlay;

        protected override void Awake()
        {

            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");

            _currentContainer = GetChildComponent<KContainer>("Container_jinengyanshidangqian");
            //_nextContainer = GetChildComponent<KContainer>("Container_jinengyanshixiaji");

            _txtCurrentContent = _currentContainer.GetChildComponent<StateText>("Label_txtNeirong");
            //_txtNextContent = _currentContainer.GetChildComponent<StateText>("Label_txtNeirong");
            _btnPlay = GetChildComponent<KButton>("Container_bofang/Button_bofang");
            EventDispatcher.AddEventListener(SpellEvents.SPELL_CG_END, OnSpellCgEnd);
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.SkillCg; }
        }

        protected override void OnDestroy()
        {
            EventDispatcher.RemoveEventListener(SpellEvents.SPELL_CG_END, OnSpellCgEnd);
            base.OnDestroy();
        }

        public override void OnShow(object data)
        {
            spell spell = data as spell;
            _txtCurrentContent.CurrentText.text = spell.__desc.ToLanguage();
            _btnPlay.onClick.AddListener(OnPlay);
            _btnPlay.Visible = false;
        }

        public override void OnClose()
        {
            _btnPlay.onClick.RemoveListener(OnPlay);
            SkillPreviewManager.GetInstance().Close();
        }

        private void OnPlay()
        {
            _btnPlay.Visible = false;
            EventDispatcher.TriggerEvent(SpellEvents.SPELL_CG_PLAY);
            //UnityEngine.Debug.LogError("[SkillCgPanel:OnPlay]=>1_____________btnPlay.Visible:   " + _btnPlay.Visible);
        }

        private void OnSpellCgEnd()
        {
            _btnPlay.Visible = true;
        }

    }
}
