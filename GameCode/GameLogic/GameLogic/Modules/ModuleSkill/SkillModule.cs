﻿#region 模块信息
/*==========================================
// 文件名：SkillModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleSkill
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/13 19:21:22
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleSkill
{
    public class SkillModule:BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            //AddPanel(PanelIdEnum.Skill, "Container_SkillPanel", MogoUILayer.LayerUIPanel, "ModuleSkill.SkillPanel");
            AddPanel(PanelIdEnum.SkillCg, "Container_SkillCGPanel", MogoUILayer.LayerUIPanel, "ModuleSkill.SkillCgPanel", BlurUnderlay.Empty,HideMainUI.Hide);
        }
    }
}
