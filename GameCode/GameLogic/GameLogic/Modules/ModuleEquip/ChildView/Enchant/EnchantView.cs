﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class EnchantView : KContainer
    {
        private CategoryList _categoryList;
        private EnchantLeftView _enchantLeftView;
        private EnchantRightView _enchantRightView;

        private KContainer _bottomContainer;
        private KContainer _iconContainer;
        private MoneyItem _moneyItem;

        private List<int> _haveEquipPosList;
        private EquipItemInfo _currentEnchantEquip;

        protected override void Awake()
        {
            base.Awake();
            _haveEquipPosList = new List<int>();
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");

            _enchantLeftView = AddChildComponent<EnchantLeftView>("Container_fumoleft");
            _enchantRightView = AddChildComponent<EnchantRightView>("Container_fumoright");
            _bottomContainer = GetChildComponent<KContainer>("Container_bottom");
            _moneyItem = AddChildComponent<MoneyItem>("Container_bottom/Container_buyGold");
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        public void SetData(object data)
        {
            RefreshEnchantPanel();
            int categoryIndex = 0;
            if (data != null)
            {
                int bodyPosition = (int)data - 1;
                if (_haveEquipPosList.Contains(bodyPosition) == true)
                {
                    categoryIndex = _haveEquipPosList.IndexOf(bodyPosition);
                }
            }
            else if (_haveEquipPosList.Count != 0)
            {
                categoryIndex = 0;
            }
            _categoryList.SelectedIndex = categoryIndex;
            RefreshEquipEnchant(_haveEquipPosList[categoryIndex]);
            RefreshRedPoint();

        }

        private void AddEventListener()
        {
            _categoryList.onSelectedIndexChanged.AddListener(OnBodySelectedChanged);

            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, OnRefreshEquipEnchant);
            EventDispatcher.AddEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshRedPoint);
            EventDispatcher.AddEventListener<int>(EnchantEvents.UpdateEnchantByBody, RefreshEquipEnchant);
        }

        private void RemoveEventListener()
        {
            _categoryList.onSelectedIndexChanged.RemoveListener(OnBodySelectedChanged);

            EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, OnRefreshEquipEnchant);
            EventDispatcher.RemoveEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshRedPoint);
            EventDispatcher.RemoveEventListener<int>(EnchantEvents.UpdateEnchantByBody, RefreshEquipEnchant);
        }

        private void OnRefreshEquipEnchant(BagType bagType, uint gridPosition)
        {
            if (bagType == BagType.ItemBag)
            {
                int materialId = enchant_scroll_helper.GetEnchantScrollId(_currentEnchantEquip);
                ShowMineMaterial(materialId);
            }
        }

        private void OnBodySelectedChanged(CategoryList toggleGroup, int index)
        {
            RefreshEquipEnchant(_haveEquipPosList[index]);
        }

        private void RefreshEquipEnchant(int bodyPosition)
        {
            _currentEnchantEquip = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo(bodyPosition) as EquipItemInfo;
            int materialId = enchant_scroll_helper.GetEnchantScrollId(_currentEnchantEquip);
            if (materialId != 0)
            {
                _bottomContainer.Visible = true;
                ShowMineMaterial(materialId);
            }
            else
            {
                _bottomContainer.Visible = false;
            }
            _enchantLeftView.ShowEnchantInfo(bodyPosition);
            _enchantRightView.ShowEnchantInfo(bodyPosition);

        }

        private void ShowMineMaterial(int materialId)
        {
            ItemData myItemData = new ItemData(materialId);
            myItemData.StackCount = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(materialId);

            _moneyItem.SetMoneyType(materialId);
            if (myItemData.StackCount < 1)
            {
                _moneyItem.SetNumTxtColor(Color.red);
            }
            else
            {
                _moneyItem.SetNumTxtColor(Color.white);
            }
        }

        private void RefreshRedPoint()
        {
            HashSet<int> enchantPointSet = PlayerDataManager.Instance.EquipPointManager.EnchantPointSet;
            int count = _haveEquipPosList.Count;
            for (int i = 0; i < count; i++)
            {
                (_categoryList[i] as CategoryListItem).SetPoint(enchantPointSet.Contains(_haveEquipPosList[i]));
            }
        }

        private void RefreshEnchantPanel()
        {
            Dictionary<int, BaseItemInfo> equipItemInfoDic = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetAllItemInfo();
            List<CategoryItemData> listToggleData = new List<CategoryItemData>();

            _haveEquipPosList = equipItemInfoDic.Keys.ToList();
            _haveEquipPosList.Sort();
            int count = _haveEquipPosList.Count;
            for (int i = 0; i < count; i++)
            {
                listToggleData.Add(CategoryItemData.GetCategoryItemData(item_helper.GetEquipPositionDesc(_haveEquipPosList[i]), item_helper.GetEquipPositionIcon(_haveEquipPosList[i])));
            }
            _categoryList.RemoveAll();
            if (count > 0)
            {
                _categoryList.SetDataList<CategoryListItem>(listToggleData);
            }
        }
    }
}
