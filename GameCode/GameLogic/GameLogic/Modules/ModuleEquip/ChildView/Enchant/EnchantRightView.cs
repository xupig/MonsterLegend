﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class EnchantRightView : KContainer
    {
        private StateText _needMaterialNum;

        private KContainer _needMaterialContainer;

        private KDummyButton _shelterContainer;

        private KButton _enchantButton;

        private ItemGrid _materialGrid;

        private EquipItemInfo _enchantEquipItem;
        private EquipInfoView _equipInfoView;

        private KParticle _particle;

        protected override void Awake()
        {
            base.Awake();

            _needMaterialContainer = GetChildComponent<KContainer>("Container_xuyaoxiaohao");
            _needMaterialNum = GetChildComponent<StateText>("Container_xuyaoxiaohao/Label_txtNum");

            _enchantButton = GetChildComponent<KButton>("Button_fumo");

            _materialGrid = GetChildComponent<ItemGrid>("Container_xuyaoxiaohao/Container_wupin");
            _equipInfoView = AddChildComponent<EquipInfoView>("Container_wupinitem");
            _shelterContainer = AddChildComponent<KDummyButton>("Container_zhezhao");
            _shelterContainer.gameObject.SetActive(false);
            _particle = AddChildComponent<KParticle>("fx_ui_10_2_xuanwo_01");
            _particle.Stop();

            _particle.transform.localPosition = new Vector3(960.6f, -169.8f, -335.3f);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        public void ShowEnchantInfo(int equipPosition)
        {
            _enchantEquipItem = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo(equipPosition) as EquipItemInfo;
            if (_enchantEquipItem != null)
            {
                ShowEquipInfo();
                RefreshNeedMaterial();
            }
        }

        private void ShowEquipInfo()
        {
            _equipInfoView.ShowEquip(_enchantEquipItem);
        }

        private void RefreshNeedMaterial()
        {
            int materialId = enchant_scroll_helper.GetEnchantScrollId(_enchantEquipItem);

            if (materialId != 0)
            {
                _enchantButton.Visible = true;
                _needMaterialContainer.Visible = true;
                ItemData needItemData = new ItemData(materialId);
                needItemData.StackCount = 1;
                _materialGrid.SetItemData(needItemData);
                _needMaterialNum.CurrentText.text = string.Format("x{0}",needItemData.StackCount);
            }
            else
            {
                _enchantButton.Visible = false;
                _needMaterialContainer.Visible = false;
            }
        }

        private void AddEventListener()
        {
            _enchantButton.onClick.AddListener(OnEnchantButtonClick);
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, OnRefreshEquipEnchant);
            EventDispatcher.AddEventListener<PbEquipEnchantAttriInfo>(EnchantEvents.ReplaceAttribute, OnShowEnchantResult);
            EventDispatcher.AddEventListener<bool>(EnchantEvents.RefreshAttribute, HideShelter);
        }

        private void RemoveEventListener()
        {
            _enchantButton.onClick.RemoveListener(OnEnchantButtonClick);
            EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, OnRefreshEquipEnchant);
            EventDispatcher.RemoveEventListener<PbEquipEnchantAttriInfo>(EnchantEvents.ReplaceAttribute, OnShowEnchantResult);
            EventDispatcher.RemoveEventListener<bool>(EnchantEvents.RefreshAttribute, HideShelter);
        }

        private void OnShowEnchantResult(PbEquipEnchantAttriInfo arg1)
        {
            _particle.Play();
        }

        private void OnRefreshEquipEnchant(BagType bagType, uint gridPosition)
        {
            if (bagType == BagType.BodyEquipBag && (int)gridPosition == _enchantEquipItem.GridPosition)
            {
                ShowEnchantInfo((int)gridPosition);
            }
        }

        private void OnEnchantButtonClick()
        {
            int materialId = enchant_scroll_helper.GetEnchantScrollId(_enchantEquipItem);
            if (materialId != 0)
            {
                if (PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(materialId) == 0)
                {
                    SystemInfoManager.Instance.Show(305);
                    return;
                }
                UIManager.Instance.PlayAudio("Sound/UI/mystery.mp3");
                EquipManager.Instance.RequestEnchant(_enchantEquipItem.GridPosition);

                _shelterContainer.gameObject.SetActive(true);
                TimerHeap.AddTimer(10000, 0, HideShelter);
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(30701), PanelIdEnum.Equip);
            }
        }

        private void HideShelter(bool isEnchantReplace)
        {
            HideShelter();
            PlayerDataManager.Instance.FightTipsManager.ShowTipsNow();
        }

        private void HideShelter()
        {
            if (_shelterContainer != null)
            {
                _shelterContainer.gameObject.SetActive(false);
            }
        }
    }
}
