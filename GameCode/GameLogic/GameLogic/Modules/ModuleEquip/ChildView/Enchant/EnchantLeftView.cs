﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ModuleEquip
{
    public class EnchantLeftView : KContainer
    {
        private const int ENCHANT_ATTRI_NUM = 3;
        private const float ENCHANT_ANIMATOR_TIME = 4.0f;

        private KContainer _noneAttributeContainer;
        private KContainer _currentAttributeContainer;

        private StateText _txtEnchantTip;
        private EnchantAttriItem[] _enchantAttributeList;

        private KContainer _luckyInfoContainer;
        private StateText _txtLuckValue;
        private KProgressBar _luckValueProgress;
        private KContainer _starContaier;
        private KContainer _starTemplate;
        private StateText _enchantFightForce;

        private PbEquipEnchantAttriInfo _replaceAttriInfo;
        private KList _attributeList;
        private int _equipPosition;
        private List<int> _enchantAttributeIdList;
        private uint timerId = TimerHeap.INVALID_ID;

        private List<GameObject> _starGameObjectList;
        private List<float> _animatorDistanceList;
        private int[] _selectedIndexList;

        private RectTransform _rect; //模拟随机选择效果
        private bool _isEnchantReplace;

        private PbEquipEnchantAttriInfo _enchantAttriInfo;

        protected override void Awake()
        {
            base.Awake();
            _enchantAttributeList = new EnchantAttriItem[ENCHANT_ATTRI_NUM];
            _starGameObjectList = new List<GameObject>();
            _animatorDistanceList = new List<float>();
            _selectedIndexList = new int[ENCHANT_ATTRI_NUM];

            _noneAttributeContainer = GetChildComponent<KContainer>("Container_weifumo");
            _currentAttributeContainer = GetChildComponent<KContainer>("Container_dangqianshuxing");

            _txtEnchantTip = GetChildComponent<StateText>("Label_txtfumojiaojian");
            for (int i = 0; i < ENCHANT_ATTRI_NUM; i++)
            {
                _enchantAttributeList[i] = _currentAttributeContainer.AddChildComponent<EnchantAttriItem>(string.Format("Container_dangqianshuxing{0}", i + 1));
            }

            _luckyInfoContainer = GetChildComponent<KContainer>("Container_xingyundengji");

            _txtLuckValue = _luckyInfoContainer.GetChildComponent<StateText>("Container_xingxing/Label_txtXingyunzhi");
            _luckValueProgress = _luckyInfoContainer.GetChildComponent<KProgressBar>("Container_xingxing/ProgressBar_jindu");
            _starContaier = _luckyInfoContainer.GetChildComponent<KContainer>("Container_xingxing/Container_star");
            _starTemplate = _luckyInfoContainer.GetChildComponent<KContainer>("Container_xingxing/Container_star/Container_template");
            _attributeList = GetChildComponent<KList>("List_kenengshuxing");

            _rect = _attributeList.GetChildComponent<RectTransform>("template");
            _rect.GetComponentInChildren<StateImage>().Alpha = 0;
            _enchantFightForce = GetChildComponent<StateText>("Container_fumozhanli/Label_txtZhanli");

            _attributeList.SetDirection(KList.KListDirection.TopToDown, 5);
            _attributeList.SetPadding(0, 10, 0, 10);

        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        float time = 0;

        void Update()
        {
            if (_animatorDistanceList.Count == 0) return;
            time += Time.deltaTime;
            for (int i = 0; i < _selectedIndexList.Length; i++)
            {
                if (_selectedIndexList[i] != -1)
                {
                    _attributeList[_selectedIndexList[i]].IsSelected = false;
                    _selectedIndexList[i] = -1;
                }
            }
            for (int i = 0; i < _animatorDistanceList.Count; i++)
            {
                float s = (-Mathf.Cos(Mathf.PI * time / ENCHANT_ANIMATOR_TIME) + 1) * 0.5f * _animatorDistanceList[i];
                int selectedIndex = ((int)(s / _rect.sizeDelta.y)) % 10;
                if (_selectedIndexList[i] != -1)
                {
                    _attributeList[_selectedIndexList[i]].IsSelected = false;
                }
                _attributeList[selectedIndex].IsSelected = true;
                _selectedIndexList[i] = selectedIndex;
            }
            if (time >= ENCHANT_ANIMATOR_TIME)
            {
                time = 0;
                _animatorDistanceList.Clear();
                PlayAnimatorEnd();
            }
        }

        public void ShowEnchantInfo(int equipPosition)
        {
            _equipPosition = equipPosition;

            ShowLuckyInfo(_equipPosition);
            ShowCurrentEnchantInfo();
            RefreshContent();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<PbEquipEnchantAttriInfo>(EnchantEvents.ReplaceAttribute, OnShowEnchantResult);
            EventDispatcher.AddEventListener<bool>(EnchantEvents.RefreshAttribute, RefreshCurrentEnchant);
            EventDispatcher.AddEventListener<int>(EnchantEvents.UpdateLuckyValue, ShowLuckyInfo);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<PbEquipEnchantAttriInfo>(EnchantEvents.ReplaceAttribute, OnShowEnchantResult);
            EventDispatcher.RemoveEventListener<bool>(EnchantEvents.RefreshAttribute, RefreshCurrentEnchant);
            EventDispatcher.RemoveEventListener<int>(EnchantEvents.UpdateLuckyValue, ShowLuckyInfo);
        }

        private void RefreshCurrentEnchant(bool isEnchantReplace)
        {
            _isEnchantReplace = isEnchantReplace;
            _attributeList.SelectedIndex = -1;
            ShowCurrentEnchantInfo();
        }

        private void ShowLuckyInfo(int gridPosition)
        {
            PbEquipEnchantAttriInfo enchantAttriInfo = PlayerDataManager.Instance.EnchantData.GetEnchantAttriInfoByBodyId(_equipPosition);
            if (enchantAttriInfo == null)
            {
                _luckyInfoContainer.Visible = false;
                return;
            }
            _luckyInfoContainer.Visible = true;
            KeyValuePair<int, List<float>> luckyAdjustInfo = enchant_helper.GetEnchantLuckyAdjustInfo((int)enchantAttriInfo.luck_value);

            if (luckyAdjustInfo.Key != 0)
            {
                float currentValue = enchantAttriInfo.luck_value - luckyAdjustInfo.Value[0] + 1;
                float totalValue = luckyAdjustInfo.Value[1] - luckyAdjustInfo.Value[0] + 1;
                _txtLuckValue.CurrentText.text = string.Format("{0}/{1}", currentValue, totalValue);
                _luckValueProgress.Value = (float)currentValue / totalValue;

                CreateStarGameObject(luckyAdjustInfo.Key);
                RefreshStarLayout();
            }
        }

        private void CreateStarGameObject(int starNum)
        {
            if (_starGameObjectList.Count > starNum)
            {
                for (int i = _starGameObjectList.Count - 1; i >= starNum; i--)
                {
                    Object.Destroy(_starGameObjectList[i]);
                    _starGameObjectList.RemoveAt(i);
                }
            }
            else if(_starGameObjectList.Count < starNum)
            {
                for (int i = 0; i < starNum - _starGameObjectList.Count; i++)
                {
                    GameObject starObj = Instantiate(_starTemplate.gameObject) as GameObject;
                    starObj.transform.SetParent(_starContaier.transform);
                    starObj.transform.localPosition = Vector3.zero;
                    starObj.transform.localScale = Vector3.one;
                    _starGameObjectList.Add(starObj);
                    
                }
            }
        }

        private void RefreshStarLayout()
        {
            if(_starGameObjectList.Count == 0) return;
            float GAP_X = 50;
            Vector3 originalPosition = _starGameObjectList[0].transform.localPosition;
            for (int i = 0; i < _starGameObjectList.Count; i++)
            {
                originalPosition.x = GAP_X * i;
                _starGameObjectList[i].transform.localPosition = originalPosition;
                _starGameObjectList[i].gameObject.SetActive(true);
            }
        }

        private void ShowCurrentEnchantInfo()
        {
            PbEquipEnchantAttriInfo enchantAttriInfo = PlayerDataManager.Instance.EnchantData.GetEnchantAttriInfoByBodyId(_equipPosition);


            if (enchantAttriInfo == null || enchantAttriInfo.enchant_attri.Count == 0)
            {
                _enchantFightForce.CurrentText.text = "0";
                _noneAttributeContainer.Visible = true;
                _currentAttributeContainer.Visible = false;
            }
            else
            {
                _enchantFightForce.CurrentText.text = enchant_helper.ComputeEnchantFightForce(enchantAttriInfo).ToString();
                _noneAttributeContainer.Visible = false;
                _currentAttributeContainer.Visible = true;
                RefreshEnchantAttriInfo();
            }
        }

        private void RefreshEnchantAttriInfo()
        {
            PbEquipEnchantAttriInfo enchantAttriInfo = PlayerDataManager.Instance.EnchantData.GetEnchantAttriInfoByBodyId(_equipPosition);
            EquipItemInfo equipItem = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo(_equipPosition) as EquipItemInfo;

            int enchantScrollId = enchant_scroll_helper.GetEnchantScrollId(equipItem);
            List<PbEquipEnchantAttri> enchantAttriList = enchantAttriInfo.enchant_attri;
            for (int i = 0; i < enchantAttriList.Count; i++)
            {
                string attributeName = attri_config_helper.GetAttributeName((int)enchantAttriList[i].enchant_attri_id);
                int attributeValue = (int)enchantAttriList[i].enchant_attri_value;

                List<int> range = enchant_random_helper.GetAttributeRange(enchantScrollId, (int)enchantAttriList[i].enchant_attri_id, equipItem.Level, equipItem.Quality, (int)enchantAttriInfo.luck_value);

                EnchantAttriData attriData = new EnchantAttriData();
                attriData.enchantAttri = enchantAttriList[i];
                attriData.range = range;

                _enchantAttributeList[i].Visible = true;
                _enchantAttributeList[i].Data = attriData;
            }
            for (int i = enchantAttriList.Count; i < ENCHANT_ATTRI_NUM; i++)
            {
                _enchantAttributeList[i].Visible = false;
            }
        }

        private void OnShowEnchantResult(PbEquipEnchantAttriInfo enchantAttriInfo)
        {
            _replaceAttriInfo = enchantAttriInfo;
            PlaySelectedAnimator(_replaceAttriInfo);
        }

        private void PlaySelectedAnimator(PbEquipEnchantAttriInfo enchantAttriInfo)
        {
            _animatorDistanceList.Clear();
            _enchantAttriInfo = enchantAttriInfo;
            List<PbEquipEnchantAttri> enchantAttri = enchantAttriInfo.enchant_attri;
            for (int i = 0; i < enchantAttri.Count; i++)
            {
                int attriIdIndex = _enchantAttributeIdList.IndexOf((int)enchantAttri[i].enchant_attri_id);
                int totalTime = 4 * 10 + attriIdIndex;
                float y = _rect.sizeDelta.y * totalTime;

                _animatorDistanceList.Add(y);
                _selectedIndexList[i] = -1;
            }
        }

        private void RefreshContent()
        {
            EquipItemInfo equipItem = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo(_equipPosition) as EquipItemInfo;
            int materialId = enchant_scroll_helper.GetEnchantScrollId(equipItem);
            if (materialId != 0)
            {
                _txtEnchantTip.Visible = false;
                _enchantAttributeIdList = enchant_scroll_helper.GetEnchantAttributeIdList(materialId);

                _attributeList.SetDataList<EnchantAttributeItem>(_enchantAttributeIdList.GetRange(0,10), 1);

            }
            else
            {
                _attributeList.SetDataList<EnchantAttributeItem>(new List<int>());
                _txtEnchantTip.Visible = true;
                _txtEnchantTip.CurrentText.text = MogoLanguageUtil.GetContent((int)LangEnum.ENCHANT_Unenchant);
            }
        }

        private void PlayAnimatorEnd()
        {
            for (int i = 0; i < _selectedIndexList.Length; i++)
            {
                if (_selectedIndexList[i] != -1)
                {
                    _attributeList[_selectedIndexList[i]].IsSelected = false;
                    _selectedIndexList[i] = -1;
                }
            }
            for (int i = 0; i < _enchantAttriInfo.enchant_attri.Count; i++)
            {
                int attriIdIndex = _enchantAttributeIdList.IndexOf((int)_enchantAttriInfo.enchant_attri[i].enchant_attri_id);
                _attributeList[attriIdIndex].IsSelected = true;
                _selectedIndexList[i] = attriIdIndex;
            }
            timerId = TimerHeap.AddTimer(300, 0, ShowEquipEnchantTip);
        }

        private void ShowEquipEnchantTip()
        {
            for (int i = 0; i < _selectedIndexList.Length; i++)
            {
                if (_selectedIndexList[i] != -1)
                {
                    _attributeList[_selectedIndexList[i]].IsSelected = false;
                    _selectedIndexList[i] = -1;
                }
            }
            TimerHeap.DelTimer(timerId);
            EnchantAttriComparisonData comparisonData = new EnchantAttriComparisonData();
            comparisonData.CurrentEnchantAttri = PlayerDataManager.Instance.EnchantData.GetEnchantAttriInfoByBodyId(_equipPosition);
            comparisonData.AfterEnchantAttri = _replaceAttriInfo;
            UIManager.Instance.ShowPanel(PanelIdEnum.EquipEnchantTip, comparisonData);
        }


    }
}
