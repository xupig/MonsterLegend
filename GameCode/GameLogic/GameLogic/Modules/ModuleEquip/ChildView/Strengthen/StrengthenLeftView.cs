﻿using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class StrengthenLeftView:KContainer
    {
        private string _levelContent;

        private StateText _txtBodyName;
        private StateText _txtLevel;

        private IconContainer _equipIconContainer;
        private StrengthenStarView _starView;

        private uint _gridPosition;
        private int _preStrengthenLevel = 0;

        protected override void Awake()
        {
            base.Awake();
            _txtBodyName = GetChildComponent<StateText>("Container_zhuangbeiinfo/Label_txtzhuangbeibuweimingcheng");
            _txtLevel = GetChildComponent<StateText>("Container_zhuangbeiinfo/Label_txtzhuangbeidengji");
            _levelContent = _txtLevel.CurrentText.text;
            _equipIconContainer = AddChildComponent<IconContainer>("Container_icon");
            _starView = AddChildComponent<StrengthenStarView>("Container_star");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        public void ShowEquipStrengthenInfo(int bodyPosition)
        {
            _gridPosition = (uint)bodyPosition;
            EquipStrengthenData equipStrengthenData = PlayerDataManager.Instance.EquipStrengthenData;
            if (equipStrengthenData.GetAllEquipStrengthenInfo().ContainsKey(bodyPosition) == true)
            {
                EquipStrengthenItemInfo strengthenItem = equipStrengthenData.GetAllEquipStrengthenInfo()[bodyPosition];
                _preStrengthenLevel = strengthenItem.StrengthenLevel;
                LoadEquipBodyIcon(bodyPosition);
                ShowEquipLevelInfo(bodyPosition);
                _starView.ShowStarLevel(strengthenItem.StrengthenLevel);
            }
        }

        private void AddEventListener()
        {
            _starView.onStarPlayComplete.AddListener(OnStarParticleFinish);
            EventDispatcher.AddEventListener<int>(EquipStrengthenEvents.Update, StrengthenLevelChanged);
        }

        private void RemoveEventListener()
        {
            _starView.onStarPlayComplete.RemoveListener(OnStarParticleFinish);
            EventDispatcher.RemoveEventListener<int>(EquipStrengthenEvents.Update, StrengthenLevelChanged);
        }


        private void StrengthenLevelChanged(int gridPosition)
        {
            EquipStrengthenData equipStrengthenData = PlayerDataManager.Instance.EquipStrengthenData;
            if (gridPosition == _gridPosition)
            {
                UIManager.Instance.PlayAudio("Sound/UI/mission_grade.mp3");

                EquipStrengthenItemInfo equipStrengthenItemInfo = equipStrengthenData.GetEquipStrengthenInfo((int)gridPosition);
                _starView.PlayParticle(_preStrengthenLevel, equipStrengthenItemInfo.StrengthenLevel);
                _preStrengthenLevel = equipStrengthenItemInfo.StrengthenLevel;
            }
        }

        private void OnStarParticleFinish()
        {
            EventDispatcher.TriggerEvent(EquipStrengthenEvents.PlayStepAnimator);
            ShowEquipStrengthenInfo((int)_gridPosition);
        }

        private void LoadEquipBodyIcon(int bodyPosition)
        {
            EquipStrengthenData equipStrengthenData = PlayerDataManager.Instance.EquipStrengthenData;
            EquipStrengthenItemInfo strengthenItem = equipStrengthenData.GetAllEquipStrengthenInfo()[bodyPosition];
            _equipIconContainer.SetIcon(strengthenItem.EquipItemInfo.Icon);

            _txtBodyName.CurrentText.text = item_helper.GetEquipPositionDesc(bodyPosition);
        }

        private void ShowEquipLevelInfo(int bodyPosition)
        {
            EquipStrengthenData equipStrengthenData = PlayerDataManager.Instance.EquipStrengthenData;
            EquipStrengthenItemInfo strengthenItem = equipStrengthenData.GetAllEquipStrengthenInfo()[bodyPosition];
            List<AttributeChangedItemInfo> changeList = strengthenItem.ChangedInfoList;

            int strengthenLevel = strengthenItem.StrengthenLevel;

            //表示强化等级已达到上限，配置表读不到下一等级强化信息
            if (changeList[0].AfterChanged == -1)
            {
                _txtLevel.CurrentText.text = MogoLanguageUtil.GetContent(11018, strengthenLevel / 10 + 1);
            }
            else if (strengthenLevel != 0)
            {
                _txtLevel.CurrentText.text = MogoLanguageUtil.GetContent(11017, (strengthenLevel - 1) / 10 + 1, (strengthenLevel - 1) % 10 + 1); 
            }
            else
            {
                _txtLevel.CurrentText.text = MogoLanguageUtil.GetContent(11017, 1, 0);
            }
        }
    }
}
