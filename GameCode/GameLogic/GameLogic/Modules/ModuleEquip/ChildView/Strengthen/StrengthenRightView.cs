﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.CustomType;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class StrengthenRightView : KContainer
    {
        private string _attributeContent;
        private string _needLevelContent;
        private const int MAX_ATTRIBUTE_COUNT = 2;
        private const int MAX_USE_ITEM_TYPE_NUM = 2;

        private KContainer _currentAttributeContainer;
        private KContainer _afterAttributeContainer;
        private KContainer _needMaterialContainer;
        private KContainer _needLevelContainer;
        private KContainer _upperLimitContainer;
        private KDummyButton _shelterContainer;

        private StateText _txtNeedLevel;
        private StateText[] _txtCurrentAttribute;
        private StateText[] _txtAfterAttribute;

        private KButton _oneClickStrengthenBtn;
        private KButton _strengthenBtn;
        private NeedMaterialItem[] _needMaterailArray;
        private StrengthenStepView _stepView;

        private EquipStrengthenItemInfo _currentStrengthenEquipItem;

        private int _gridPosition;
        private int _preStrengthenLevel;
        private Dictionary<int, int> _preAttributeDic; //key属性id,value是属性值

        protected override void Awake()
        {
            base.Awake();
            _txtCurrentAttribute = new StateText[MAX_ATTRIBUTE_COUNT];
            _txtAfterAttribute = new StateText[MAX_ATTRIBUTE_COUNT];
            _needMaterailArray = new NeedMaterialItem[MAX_USE_ITEM_TYPE_NUM];
            _preAttributeDic = new Dictionary<int, int>();

            _currentAttributeContainer = GetChildComponent<KContainer>("Container_qianghuaqian");
            _afterAttributeContainer = GetChildComponent<KContainer>("Container_qianghuahou");
            _needMaterialContainer = GetChildComponent<KContainer>("Container_xuyaocailiao");
            _needLevelContainer = GetChildComponent<KContainer>("Container_xuyaodengji");
            _upperLimitContainer = GetChildComponent<KContainer>("Container_yidashangxian");

            _shelterContainer = AddChildComponent<KDummyButton>("Container_zhezhao");
            _shelterContainer.gameObject.SetActive(false);
            _oneClickStrengthenBtn = GetChildComponent<KButton>("Button_yijianqianghua");
            _strengthenBtn = GetChildComponent<KButton>("Button_qianghua");

            _txtNeedLevel = _needLevelContainer.GetChildComponent<StateText>("Label_txtdengji");

            _needLevelContent = _txtNeedLevel.CurrentText.text;
            for (int i = 0; i < MAX_ATTRIBUTE_COUNT; i++)
            {
                _txtCurrentAttribute[i] = _currentAttributeContainer.GetChildComponent<StateText>(string.Format("Label_txtqianghuashuxing{0}", i + 1));
                _txtAfterAttribute[i] = _afterAttributeContainer.GetChildComponent<StateText>(string.Format("Label_txtqianghuashuxing{0}", i + 1));
            }
            for (int i = 0; i < MAX_USE_ITEM_TYPE_NUM; i++)
            {
                _needMaterailArray[i] = _needMaterialContainer.AddChildComponent<NeedMaterialItem>(string.Format("Container_cailiao{0}", i + 1));
            }

            _attributeContent = _txtAfterAttribute[0].CurrentText.text;
            _stepView = AddChildComponent<StrengthenStepView>("Container_qianghuadengji");

            AddEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

        public void Show(int bodyPosition)
        {
            EquipStrengthenData equipStrengthenData = PlayerDataManager.Instance.EquipStrengthenData;
            if (equipStrengthenData.GetAllEquipStrengthenInfo().ContainsKey(bodyPosition) == true)
            {
                _currentStrengthenEquipItem = equipStrengthenData.GetAllEquipStrengthenInfo()[bodyPosition];
                _gridPosition = bodyPosition;
                _preStrengthenLevel = _currentStrengthenEquipItem.StrengthenLevel;
                ShowAttribute();
                ShowNeedLevel();
                RefreshNeedMaterial();

                _stepView.ShowStepInfo(bodyPosition);

            }
        }
        
        private void AddEventListener()
        {
            _oneClickStrengthenBtn.onClick.AddListener(OneClickStrengthen);
            _strengthenBtn.onClick.AddListener(OnRequestStrengthen);
            _stepView.onPlayComplete.AddListener(OnStepAnimatorComplete);

            EventDispatcher.AddEventListener<int>(EquipStrengthenEvents.Update, PlayAttributeAnimator);
            EventDispatcher.AddEventListener(EquipStrengthenEvents.PlayStepAnimator, PlayStepAnimator);
        }

        private void RemoveEventListener()
        {
            _oneClickStrengthenBtn.onClick.RemoveListener(OneClickStrengthen);
            _strengthenBtn.onClick.RemoveListener(OnRequestStrengthen);
            _stepView.onPlayComplete.RemoveListener(OnStepAnimatorComplete);

            EventDispatcher.RemoveEventListener<int>(EquipStrengthenEvents.Update, PlayAttributeAnimator);
            EventDispatcher.RemoveEventListener(EquipStrengthenEvents.PlayStepAnimator, PlayStepAnimator);
        }

        private void OneClickStrengthen()
        {
            if (IsCanStrenghten() == true)
            {
                PlayerDataManager.Instance.FightTipsManager.SetDelayShow(true); 
                EquipManager.Instance.RequestOneClickStrengthen(_currentStrengthenEquipItem.StrengthenId);
                PlayerDataManager.Instance.EquipPointManager.ResetStrengthenPointNeedStrengthenTimes();
                _shelterContainer.gameObject.SetActive(true);
                TimerHeap.AddTimer(5000, 0, HideShelter);
            }
        }

        private void OnRequestStrengthen()
        {
            if (IsCanStrenghten() == true)
            {
                PlayerDataManager.Instance.FightTipsManager.SetDelayShow(true);
                EquipManager.Instance.RequestStrengthen(_currentStrengthenEquipItem.StrengthenId, new LuaTable(), 0);
                PlayerDataManager.Instance.EquipPointManager.ResetStrengthenPointNeedStrengthenTimes();
                _shelterContainer.gameObject.SetActive(true);
                TimerHeap.AddTimer(5000, 0, HideShelter);
            }
        }

        private void HideShelter()
        {
            if (_shelterContainer != null)
            {
                _shelterContainer.gameObject.SetActive(false);
            }
        }

        private void PlayStepAnimator()
        {
            EquipStrengthenData equipStrengthenData = PlayerDataManager.Instance.EquipStrengthenData;
            if (equipStrengthenData.GetAllEquipStrengthenInfo().ContainsKey(_gridPosition) == true)
            {
                _currentStrengthenEquipItem = equipStrengthenData.GetAllEquipStrengthenInfo()[_gridPosition];

                _stepView.PlayStepAnimator(GetStepByStrengthenLevel(_preStrengthenLevel), GetStepByStrengthenLevel(_currentStrengthenEquipItem.StrengthenLevel));
                _preStrengthenLevel = _currentStrengthenEquipItem.StrengthenLevel;
            }
        }

        private int GetStepByStrengthenLevel(int level)
        {
            List<AttributeChangedItemInfo> changeList = _currentStrengthenEquipItem.ChangedInfoList;

            //表示强化等级已达到上限，配置表读不到下一等级强化信息
            if (changeList[0].AfterChanged == -1)
            {
                return level / 10 + 1;
            }
            else if (level == 0)
            {
                return 1;
            }
            else
            {
                return (level - 1) / 10 + 1;
            }
        }

        private void OnStepAnimatorComplete()
        {
            _stepView.ShowStepInfo(_currentStrengthenEquipItem.EquipItemInfo.GridPosition);
            HideShelter();
            PlayerDataManager.Instance.FightTipsManager.ShowTipsNow();
        }

        private void PlayAttributeAnimator(int gridPosition)
        {
            EquipStrengthenData equipStrengthenData = PlayerDataManager.Instance.EquipStrengthenData;
            if (equipStrengthenData.GetAllEquipStrengthenInfo().ContainsKey(_gridPosition) == true)
            {
                _currentStrengthenEquipItem = equipStrengthenData.GetAllEquipStrengthenInfo()[_gridPosition];

                List<AttributeChangedItemInfo> changeList = _currentStrengthenEquipItem.ChangedInfoList;
                for (int i = 0; i < changeList.Count; i++)
                {
                    int change = changeList[i].BeforeChanged - _preAttributeDic[changeList[i].AttributeId];
                    int gap = Mathf.CeilToInt(change / 19.0f);
                    ///
                    /// 三个参数依次表示播放时的属性值，每次播放增加的值，属性对应的changeList的index
                    ///
                    TimerHeap.AddTimer<int,int,int>(300,0,RefreshAttributeText,_preAttributeDic[changeList[i].AttributeId],gap,i);
                }
            }
        }

        private void RefreshAttributeText(int curAttributeValue, int attributeGap, int changeListIndex)
        {
            
            List<AttributeChangedItemInfo> changeList = _currentStrengthenEquipItem.ChangedInfoList;
            if (curAttributeValue >= changeList[changeListIndex].BeforeChanged)
            {
                ShowNeedLevel();
                RefreshNeedMaterial();
                ShowAttribute();
            }
            else
            {
                _txtCurrentAttribute[changeListIndex].CurrentText.text = string.Format(_attributeContent, changeList[changeListIndex].AttributeName, curAttributeValue);
                TimerHeap.AddTimer<int, int, int>(30, 0, RefreshAttributeText, curAttributeValue + attributeGap, attributeGap, changeListIndex);
            }
        }


        private bool IsCanStrenghten()
        {
            int start = _currentStrengthenEquipItem.StrengthenLevel;
            int end = _currentStrengthenEquipItem.StrengthenUpperLimit;
            List<AttributeChangedItemInfo> changeList = _currentStrengthenEquipItem.ChangedInfoList;
            if (changeList[0].AfterChanged == -1)
            {
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.STRENGTHEN_UPLIMIT), PanelIdEnum.Equip);

                return false;
            }
            if (start >= end)
            {
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(11007), PanelIdEnum.Equip);
                return false;
            }

            int result = PlayerAvatar.Player.CheckCostLimit(_currentStrengthenEquipItem.NeedMaterial);
            if (result != 0)
            {
                if (item_helper.isMoneyType(result))
                {
                    ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.STRENGTHEN_CURRENCY_NONE), PanelIdEnum.Equip);
                }
                else
                {
                    ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.STRENGTHEN_MATERAIL_NONE), PanelIdEnum.Equip);
                }
                UIManager.Instance.ShowPanel(PanelIdEnum.ItemChannel, result);
                return false;
            }

            return true;
        }

        private void ShowAttribute()
        {
            List<AttributeChangedItemInfo> changeList = _currentStrengthenEquipItem.ChangedInfoList;
            if (changeList[0].AfterChanged != -1)
            {
                ResetView();
            }
            else
            {
                SetReachUpperLimitView();
            }
            for (int i = 0; i < changeList.Count; i++)
            {
                _txtCurrentAttribute[i].CurrentText.text = string.Format(_attributeContent, changeList[i].AttributeName, changeList[i].BeforeChanged);
                _txtCurrentAttribute[i].Visible = true;
                _txtAfterAttribute[i].CurrentText.text = string.Format(_attributeContent, changeList[i].AttributeName, changeList[i].AfterChanged);
                _txtAfterAttribute[i].Visible = true;
                _preAttributeDic[changeList[i].AttributeId] = changeList[i].BeforeChanged;
            }
            for (int i = changeList.Count; i < MAX_ATTRIBUTE_COUNT; i++)
            {
                _txtCurrentAttribute[i].Visible = false;
                _txtAfterAttribute[i].Visible = false;
            }
        }

        private void ShowNeedLevel()
        {
            int needLevel = _currentStrengthenEquipItem.StrengthenLevel / 2 + 1;
            _txtNeedLevel.CurrentText.text = string.Format(_needLevelContent, needLevel);
        }

        private void RefreshNeedMaterial()
        {
            List<BaseItemData> needMaterialList = _currentStrengthenEquipItem.NeedMaterial;
            int count = needMaterialList.Count;
            for (int i = 0; i < count; i++)
            {
                _needMaterailArray[i].Visible = true;
                _needMaterailArray[i].Data = needMaterialList[i];
            }
            for (int i = count; i < MAX_USE_ITEM_TYPE_NUM; i++)
            {
                _needMaterailArray[i].Visible = false;
            }
        }

        private void ResetView()
        {
            _afterAttributeContainer.Visible = true;
            _needLevelContainer.Visible = true;
            _needMaterialContainer.Visible = true;

            _oneClickStrengthenBtn.Visible = true;
            _strengthenBtn.Visible = true;
            _upperLimitContainer.Visible = false;
        }

        private void SetReachUpperLimitView()
        {
            _afterAttributeContainer.Visible = false;
            _needLevelContainer.Visible = false;
            _needMaterialContainer.Visible = false;

            _oneClickStrengthenBtn.Visible = false;
            _strengthenBtn.Visible = false;
            _upperLimitContainer.Visible = true;
        }
    }
}
