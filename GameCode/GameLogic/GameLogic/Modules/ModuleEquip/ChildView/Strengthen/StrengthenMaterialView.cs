﻿
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class StrengthenMaterialView : KContainer
    {
        private MoneyItem[] _materialList;
        private int _bodyPosition = -1;

        protected override void Awake()
        {
            _materialList = new MoneyItem[2];
            InitMaterialItem();
            
        }

        private void InitMaterialItem()
        {
            _materialList[0] = AddChildComponent<MoneyItem>("Container_buyGold1");
            _materialList[1] = AddChildComponent<MoneyItem>("Container_buyGold2");
        }

        protected override void OnEnable()
        {
            HideMaterialItem();
            AddEventsListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        public void ShowMaterialByBodyPosition(int bodyPosition)
        {
            EquipStrengthenData equipStrengthenData = PlayerDataManager.Instance.EquipStrengthenData;
            if (equipStrengthenData.GetAllEquipStrengthenInfo().ContainsKey(bodyPosition) == true)
            {
                _bodyPosition = bodyPosition;
                ShowMineMaterial();
            }
        }

        private void ShowMineMaterial()
        {
            EquipStrengthenData equipStrengthenData = PlayerDataManager.Instance.EquipStrengthenData;
            EquipStrengthenItemInfo strengthenItem = equipStrengthenData.GetAllEquipStrengthenInfo()[_bodyPosition];

            HideMaterialItem();
            List<BaseItemData> needMaterial = strengthenItem.NeedMaterial;
            for (int i = 0; i < needMaterial.Count; i++)
            {
                _materialList[i].Visible = true;
                _materialList[i].SetMoneyType(needMaterial[i].Id, needMaterial[i]);
            }
        }

        public void HideMaterialItem()
        {
            for (int k = 0; k < 2; k++)
            {
                _materialList[k].gameObject.SetActive(false);
            }
        }

        private void AddEventsListener()
        {
            EventDispatcher.AddEventListener<BagType,uint>(BagEvents.Update, Refresh);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<BagType,uint>(BagEvents.Update, Refresh);
        }

        private void Refresh(BagType bagType,uint index)
        {
            ShowMineMaterial();
        }


    }
}
