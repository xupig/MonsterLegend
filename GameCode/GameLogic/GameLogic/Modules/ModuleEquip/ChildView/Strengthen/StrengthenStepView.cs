﻿using Common.Data;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class StrengthenStepView:KContainer
    {
        private const int ICONCOUNT = 5;
        private string _strengthenLevelContent;
        private StateText _txtStrengthenLevel;

        private StepIconItem[] stepIconList;

        private int _beforeStep;
        private int _afterStep;

        public KComponentEvent onPlayComplete = new KComponentEvent();

        protected override void Awake()
        {
            base.Awake();
            stepIconList = new StepIconItem[ICONCOUNT];

            _txtStrengthenLevel = GetChildComponent<StateText>("Label_txtzhuangbeidengji");
            _strengthenLevelContent = _txtStrengthenLevel.CurrentText.text;

            for (int i = 0; i < ICONCOUNT; i++)
            {
                stepIconList[i] = AddChildComponent<StepIconItem>(string.Format("Container_jie{0}",i));
            }

            AddEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

        private void AddEventListener()
        {
            for (int i = 0; i < ICONCOUNT; i++)
            {
                stepIconList[i].onPlayComplete.AddListener(PlayRecursion);
            }
        }

        private void RemoveEventListener()
        {
            for (int i = 0; i < ICONCOUNT; i++)
            {
                stepIconList[i].onPlayComplete.RemoveListener(PlayRecursion);
            }
        }

        private void PlayRecursion(int step)
        {

            PlayStepAnimator(++step, _afterStep);

        }

        public void PlayStepAnimator(int beforeStep, int afterStep)
        {
            _beforeStep = beforeStep;
            _afterStep = afterStep;
            if (beforeStep < afterStep)
            {
                if (beforeStep % 5 == 0)
                {
                    ShowStepIcon(0);
                }
                stepIconList[beforeStep % 5].Play(beforeStep, beforeStep / 5);
            }
            else
            {
                onPlayComplete.Invoke();
            }

        }

        public void ShowStepInfo(int bodyPosition)
        {
            EquipStrengthenData equipStrengthenData = PlayerDataManager.Instance.EquipStrengthenData;
            EquipStrengthenItemInfo strengthenItem = equipStrengthenData.GetAllEquipStrengthenInfo()[bodyPosition];
            List<AttributeChangedItemInfo> changeList = strengthenItem.ChangedInfoList;
            int strengthenLevel = strengthenItem.StrengthenLevel;
            int step = 1;

            //表示强化等级已达到上限，配置表读不到下一等级强化信息
            if (changeList[0].AfterChanged == -1)
            {
                _txtStrengthenLevel.CurrentText.text = MogoLanguageUtil.GetContent(11018, strengthenLevel / 10 + 1);
                step = strengthenLevel / 10 + 1;
            }
            else if (strengthenLevel != 0)
            {
                _txtStrengthenLevel.CurrentText.text = MogoLanguageUtil.GetContent(11017, (strengthenLevel - 1) / 10 + 1, (strengthenLevel - 1) % 10 + 1);
                step = (strengthenLevel - 1) / 10 + 1;
            }
            else
            {
                _txtStrengthenLevel.CurrentText.text = MogoLanguageUtil.GetContent(11017, 1, 0);
            }
            ShowStepIcon(step);
        }

        private void ShowStepIcon(int step)
        {
            int primaryStep = (step - 1) / ICONCOUNT;
            int secondaryStep = (step - 1) % ICONCOUNT;

            for (int i = 0; i < secondaryStep + 1; i++)
            {
                stepIconList[i].ShowStepIcon(primaryStep);
            }

            for (int i = secondaryStep + 1; i < ICONCOUNT; i++)
            {
                stepIconList[i].HideAllChild();
            }
        }
    }
}
