﻿using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Structs.Enum;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class StrengthenView:KContainer
    {
        private List<int> _haveEquipPosList;

        private CategoryList _categoryList;
        private StrengthenLeftView _leftView;
        private StrengthenRightView _rightView;
        private StrengthenMaterialView _materialView;

        private bool _isStartCategoryCoroutine = false;
        private object _data = null;
        protected override void Awake()
        {
            base.Awake();

            _haveEquipPosList = new List<int>();
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");

            _leftView = AddChildComponent<StrengthenLeftView>("Container_qianghualeft");
            _rightView = AddChildComponent<StrengthenRightView>("Container_qianghuaright");
            _materialView = AddChildComponent<StrengthenMaterialView>("Container_bottommaterial");
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        protected override void OnDestroy()
        {
            StopCategoryCoroutine();
            base.OnDestroy();
        }

        public void SetData(object data)
        {
            _data = data;
            RefreshEquipPositionList();
            StopCategoryCoroutine();
            RefreshStrenghthenCategory();
            StartCoroutine(WaitingCategory());
        }

        private void AddEventListener()
        {
            _categoryList.onSelectedIndexChanged.AddListener(OnBodySelectedChanged);
            EventDispatcher.AddEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshRedPoint);
        }

        private void RemoveEventListener()
        {
            _categoryList.onSelectedIndexChanged.RemoveListener(OnBodySelectedChanged);
            EventDispatcher.RemoveEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshRedPoint);
        }

        private void OnBodySelectedChanged(CategoryList toggleGroup, int index)
        {
            RefreshStrengthenDetail(_haveEquipPosList[index]);
        }

        private void RefreshEquipPositionList()
        {
            Dictionary<int, BaseItemInfo> equipItemInfoDic = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetAllItemInfo();
            _haveEquipPosList.Clear();
            foreach (KeyValuePair<int, BaseItemInfo> kvp in equipItemInfoDic)
            {
                EquipStrengthenItemInfo strengthenItemInfo = PlayerDataManager.Instance.EquipStrengthenData.GetEquipStrengthenInfo(kvp.Key);
                EquipItemInfo equipItemInfo = kvp.Value as EquipItemInfo;
                if (equipItemInfo.SubType == EquipType.glove)
                {
                    _haveEquipPosList.Add(kvp.Key);
                }
                else if (equipItemInfo.Quality >= 4 || (strengthenItemInfo != null && equipItemInfo.StrengthenLevel > 0))
                {
                    _haveEquipPosList.Add(kvp.Key);
                }
            }
            _haveEquipPosList.Sort();
        }

        private void RefreshStrenghthenCategory()
        {
            List<CategoryItemData> listToggleData = new List<CategoryItemData>();
            int count = _haveEquipPosList.Count;
            for (int i = 0; i < count; i++)
            {
                listToggleData.Add(CategoryItemData.GetCategoryItemData(item_helper.GetEquipPositionDesc(_haveEquipPosList[i]), item_helper.GetEquipPositionIcon(_haveEquipPosList[i])));
            }
            _categoryList.RemoveAll();
            if (count > 0)
            {
                _categoryList.SetDataList<CategoryListItem>(listToggleData, 1);
            }

        }

        private IEnumerator WaitingCategory()
        {
            _isStartCategoryCoroutine = true;
            while (_categoryList.ItemList.Count < _haveEquipPosList.Count)
            {
                yield return 0;
            }
            _isStartCategoryCoroutine = false;
            CategoryFinish(_data);
        }

        private void StopCategoryCoroutine()
        {
            if (_isStartCategoryCoroutine)
            {
                StopCoroutine(WaitingCategory());
                _isStartCategoryCoroutine = false;
            }
        }

        private void CategoryFinish(object data)
        {
            if (data != null)
            {
                int bodyPosition = (int)data;
                if (_haveEquipPosList.Contains(bodyPosition) == true)
                {
                    int categoryIndex = _haveEquipPosList.IndexOf(bodyPosition);
                    _categoryList.SelectedIndex = categoryIndex;
                    RefreshStrengthenDetail(bodyPosition);
                }
                else if (_haveEquipPosList.Count != 0)
                {
                    _categoryList.SelectedIndex = 0;
                    RefreshStrengthenDetail(_haveEquipPosList[0]);
                }
                else
                {
                    LoggerHelper.Error("无可强化部位");
                }
            }
            else if (_haveEquipPosList.Count != 0)
            {
                //显示第一个可强化装备
                int index = 0;
                ///可强化的所有中强化等级最低的部位id, 返回-1表示没有找到可强化的装备
                int minStrengthenPointLevelPosition = PlayerDataManager.Instance.EquipPointManager.GetMinStrengthenLevelPosition();
                if (minStrengthenPointLevelPosition != -1)
                {
                    index = _haveEquipPosList.IndexOf(minStrengthenPointLevelPosition);
                }
                _categoryList.SelectedIndex = index;
                RefreshStrengthenDetail(_haveEquipPosList[index]);
            }
            RefreshRedPoint();
        }

        private void RefreshRedPoint()
        {
            ///可强化的所有中强化等级最低的部位id
            int minStrengthenPointLevelPosition = PlayerDataManager.Instance.EquipPointManager.GetMinStrengthenLevelPosition();
            int count = _categoryList.ItemList.Count;
            for (int i = 0; i < count; i++)
            {
                (_categoryList[i] as CategoryListItem).SetPoint(minStrengthenPointLevelPosition == _haveEquipPosList[i]);
            }
        }

        private void RefreshStrengthenDetail(int bodyPosition)
        {
            _rightView.Show(bodyPosition);
            _leftView.ShowEquipStrengthenInfo(bodyPosition);
            _materialView.ShowMaterialByBodyPosition(bodyPosition);
        }
    }
}
