﻿
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class StrengthenStarView:KContainer
    {
        private const int STAR_COUNT = 10;
        private StarWithParticle[] _starWithParticle;
        private List<StarPaticleInfo> _playParticleList;
        private int _playingParticleCount = 0;

        public KComponentEvent onStarPlayComplete = new KComponentEvent();

        protected override void Awake()
        {
            base.Awake();
            _playParticleList = new List<StarPaticleInfo>();
            _starWithParticle = new StarWithParticle[10];
            for (int i = 0; i < STAR_COUNT; i++)
            {
                _starWithParticle[i] = AddChildComponent<StarWithParticle>(string.Format("Container_star{0}", i));
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        private void AddEventListener()
        {
            for (int i = 0; i < STAR_COUNT; i++)
            {
                _starWithParticle[i].onPlayComplete.AddListener(RefreshPlayingParticle);
            }
        }

        private void RemoveEventListener()
        {
            for (int i = 0; i < STAR_COUNT; i++)
            {
                _starWithParticle[i].onPlayComplete.RemoveListener(RefreshPlayingParticle);
            }
        }

        public void PlayParticle(int beforeLevel, int afterLevel)
        {
            if (afterLevel < beforeLevel) return;
            _playParticleList.Clear();
            if ((beforeLevel / 10) != (afterLevel / 10))
            {
                _playParticleList.Add(new StarPaticleInfo(beforeLevel % 10, 10, ParseStarQuality(beforeLevel)));
                if (afterLevel % 10 != 0)
                {
                    _playParticleList.Add(new StarPaticleInfo(0, afterLevel % 10, ParseStarQuality(afterLevel)));
                }
            }
            else
            {
                _playParticleList.Add(new StarPaticleInfo(beforeLevel % 10, afterLevel % 10, ParseStarQuality(afterLevel)));
            }
            PlayNextStarPartice();
        }

        public void ShowStarLevel(int strengthenLevel)
        {
            int starCount = 0;
            int step = 1;
            if (strengthenLevel != 0)
            {
                starCount = (strengthenLevel-1) % 10 + 1;
                step = (strengthenLevel - 1) / 10 + 1;
            }
            for (int i = 0; i < starCount; i++)
            {
                _starWithParticle[i].ShowStarQuality(step / 3 + 2);
            }
            for (int i = starCount; i < STAR_COUNT; i++)
            {
                _starWithParticle[i].HideStarQuality();
            }
        }

        private int ParseStarQuality(int strengthenLevel)
        {
            return (strengthenLevel == 0 ? 1 : ((strengthenLevel - 1) / 10 + 1)) / 3 + 2;
        }


        private void RefreshPlayingParticle()
        {
            _playingParticleCount--;
            if (_playingParticleCount <= 0)
            {
                if (_playParticleList.Count != 0)
                {
                    PlayNextStarPartice();
                }
                else
                {
                    onStarPlayComplete.Invoke();
                }
            }
        }

        private void PlayNextStarPartice()
        {
            _playingParticleCount = _playParticleList[0].AfterStar - _playParticleList[0].BeforeStar;
            if (_playParticleList[0].BeforeStar == 0)
            {
                ShowStarLevel(0);
                PlayNextShowStar();
            }
            else
            {
                PlayNextShowStar();
            }
        }

        private void PlayNextShowStar()
        {
            if (_playParticleList.Count > 0)
            {
                PlayStarParticleRange(_playParticleList[0]);
                _playParticleList.RemoveAt(0);
            }
        }

        private void PlayStarParticleRange(StarPaticleInfo kvp)
        {
            for (int i = kvp.BeforeStar; i < kvp.AfterStar; i++)
            {
                _starWithParticle[i].Play(kvp.StarQuality);
            }
        }
    }

    public class StarPaticleInfo
    {
        public int BeforeStar;
        public int AfterStar;
        public int StarQuality;

        public StarPaticleInfo(int beforeStar, int afterStar, int starQuality)
        {
            this.BeforeStar = beforeStar;
            this.AfterStar = afterStar;
            this.StarQuality = starQuality;
        }
    }
}
