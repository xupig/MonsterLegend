﻿
using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class EquipDecompositionView:KContainer
    {
        private DecompositionFilterView _decompositionFilterView;
        private DecompositionGridView _decompositinGridView;
        private KButton _decompositionButton;
        private List<EquipItemInfo> _selectedEquipList;

        protected override void Awake()
        {
            _decompositionFilterView = this.gameObject.AddComponent<DecompositionFilterView>();
            _decompositinGridView = AddChildComponent<DecompositionGridView>("Container_content");
            _decompositionButton = GetChildComponent<KButton>("Button_fenjie");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventsListener();
            OnFilterOptionChanged(new DecompositionFilterOption());
        }

        protected override void OnDisable()
        {
            RemoveEventsListener();
            base.OnDisable();
        }

        private void AddEventsListener()
        {
            _decompositionButton.onClick.AddListener(OnDecompositionButtonClick);
            _decompositionFilterView.onFilterOptionSelectedChanged.AddListener(OnFilterOptionChanged);
        }

        private void RemoveEventsListener()
        {
            _decompositionButton.onClick.RemoveListener(OnDecompositionButtonClick);
            _decompositionFilterView.onFilterOptionSelectedChanged.RemoveListener(OnFilterOptionChanged);
        }

        private void OnFilterOptionChanged(DecompositionFilterOption filterOption)
        {
            _decompositinGridView.ShowEquipItem(filterOption);
        }

        private void OnDecompositionButtonClick()
        {
            _selectedEquipList = _decompositinGridView.SelectedEquipList;
            if (_selectedEquipList.Count == 0)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent((int)LangEnum.COMPOSITION_PLEASE_SELECTED_EQUIP), PanelIdEnum.Equip);
            }
            else if (IsContainsQualityEquip(_selectedEquipList, (int)public_config.ITEM_QUALITY_PURPLE) == true)
            {
                string content = MogoLanguageUtil.GetString(LangEnum.COMPOSITION_ARE_YOU_OK);
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, OnConfirm);
            }
            else
            {
                OnConfirm();
            }
        }

        private void OnConfirm()
        {
            if (_selectedEquipList == null) return;
            List<int> gridPosList = new List<int>();
            for (int i = 0; i < _selectedEquipList.Count; i++)
            {
                gridPosList.Add(_selectedEquipList[i].GridPosition);
            }
            _selectedEquipList = null;
            EquipManager.Instance.RequestDecomposeList(gridPosList);
        }

        private bool IsContainsQualityEquip(List<EquipItemInfo> equipList,int quality)
        {
            for (int i = 0; i < equipList.Count; i++)
            {
                if (equipList[i].Quality >= quality)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
