﻿using Common.Data;
using Common.Structs;
using Common.Structs.Enum;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class DecompositionGridView : KContainer
    {
        private const int gridNumPerPage = 21;
        private KScrollPage _scrollPage;
        private KPageableList _contentList;

        protected override void Awake()
        {
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_itemBag");
            _contentList = GetChildComponent<KPageableList>("ScrollPage_itemBag/mask/content");

            InitScrollPage();
        }

        public void ShowEquipItem(DecompositionFilterOption filterOption)
        {
            switch (filterOption.EquipClassify)
            {
                case DecompositionEquipType.UselessEquip:
                    ShowUselessEquip(filterOption.EquipQuality);
                    break;
                case DecompositionEquipType.AllEquip:
                    ShowAllEquip(filterOption.EquipQuality);
                    break;
                default:
                    break;
            }
        }

        public List<EquipItemInfo> SelectedEquipList
        {
            get
            {
                List<EquipItemInfo> selectedEquipList = new List<EquipItemInfo>();
                for (int i = 0; i < _contentList.ItemList.Count; i++)
                {
                    if (_contentList[i].IsSelected == true)
                    {
                        selectedEquipList.Add(_contentList[i].Data as EquipItemInfo);
                    }
                }
                return selectedEquipList;
            }
        }

        private void ShowUselessEquip(int quality)
        {
            List<BaseItemInfo> itemBagEquipList = PlayerDataManager.Instance.BagData.GetItemBagData().GetTypeItemInfo(BagItemType.Equip);
            List<BaseItemInfo> uselessEquipList = new List<BaseItemInfo>();

            for (int i = 0; i < itemBagEquipList.Count; i++)
            {
                EquipItemInfo equipItemInfo = itemBagEquipList[i] as EquipItemInfo;

                if (item_helper.IsEquipBetter(equipItemInfo) == false)
                {
                    uselessEquipList.Add(equipItemInfo);
                }
            }

            ShowEquipItem(uselessEquipList, quality);
        }

        private void ShowAllEquip(int quality)
        {
            List<BaseItemInfo> itemBagEquipList = FilterHighLevelEquip();
            ShowEquipItem(itemBagEquipList, quality);
        }

        private List<BaseItemInfo> FilterHighLevelEquip()
        {
            List<BaseItemInfo> result = new List<BaseItemInfo>();
            List<BaseItemInfo> itemBagEquipList = PlayerDataManager.Instance.BagData.GetItemBagData().GetTypeItemInfo(BagItemType.Equip);
            for (int i = 0; i < itemBagEquipList.Count; i++)
            {
                EquipItemInfo equipItemInfo = itemBagEquipList[i] as EquipItemInfo;
                if ((itemBagEquipList[i] as EquipItemInfo).Level <= PlayerAvatar.Player.level || equipItemInfo.UsageVocationRequired != PlayerAvatar.Player.vocation)
                {
                    result.Add(itemBagEquipList[i]);
                }
            }
            return result;

        }

        private void ShowEquipItem(List<BaseItemInfo> itemEquipList, int quality)
        {
            List<BaseItemInfo> showEquipList = null;
            if (quality == 0)
            {
                showEquipList = itemEquipList;
            }
            else
            {
                showEquipList = new List<BaseItemInfo>();
                for (int i = 0; i < itemEquipList.Count; i++)
                {
                    if (itemEquipList[i].Quality == quality)
                    {
                        showEquipList.Add(itemEquipList[i]);
                    }
                }
            }
            showEquipList.Sort(ListCompareSort);
            BaseItemInfo[] showEquipArray = FillToWholePage(showEquipList);
            RefreshGridView(showEquipArray);
        }

        private void RefreshGridView(BaseItemInfo[] showEquipArray)
        {
            _scrollPage.CurrentPage = 0;
            _scrollPage.TotalPage = Mathf.CeilToInt(showEquipArray.Length / 21f);
            _contentList.SetDataList<CompositionGridItem>(showEquipArray, 7);
        }

        private BaseItemInfo[] FillToWholePage(List<BaseItemInfo> showEquipList)
        {
            int totalPage = Mathf.CeilToInt(showEquipList.Count / 21f);
            if (totalPage <= 0)
            {
                totalPage = 1;
            }
            BaseItemInfo[] showEquipArray = new BaseItemInfo[totalPage * 21];
            for (int i = 0; i < showEquipList.Count; i++)
            {
                showEquipArray[i] = showEquipList[i];
            }
            return showEquipArray;
        }

        private void InitScrollPage()
        {
            _contentList.SetPadding(15, 15, 15, 15);
            _contentList.SetGap(30, 32);
            _contentList.SetDirection(KList.KListDirection.LeftToRight, 7, 3);
        }

        private int ListCompareSort(BaseItemInfo first, BaseItemInfo second)
        {
            EquipItemInfo firstEquip = first as EquipItemInfo;
            EquipItemInfo secondEquip = second as EquipItemInfo;

            ///低品质>低战力>低道具ID的方式进行排序
            if (firstEquip.Quality < secondEquip.Quality ||
                (firstEquip.Quality == secondEquip.Quality && firstEquip.FightForce < secondEquip.FightForce) ||
                (firstEquip.Quality == secondEquip.Quality && firstEquip.FightForce == secondEquip.FightForce && firstEquip.Id < secondEquip.Id))
            {
                return 1;
            }
            else if (firstEquip.Quality == secondEquip.Quality && firstEquip.FightForce == secondEquip.FightForce && firstEquip.Id == secondEquip.Id)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }
    }
}
