﻿using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class DecompositionFilterView : KContainer
    {
        private KToggle _isSelectedAll;
        private CategoryList _categoryToggleGroup;
        private KToggleGroup _equipClassifyToggleGroup;

        private List<int> _haveEquipQualityList;
        private DecompositionFilterOption _filterOption;

        private uint _timeId = TimerHeap.INVALID_ID;  //用于延迟刷新，防止装备批量分解时背包数据大量更新导致界面卡顿
        public static bool IsSelectedAll = true;
        public KComponentEvent<DecompositionFilterOption> onFilterOptionSelectedChanged = new KComponentEvent<DecompositionFilterOption>();

        protected override void Awake()
        {
            _categoryToggleGroup = GetChildComponent<CategoryList>("List_categoryList");
            _equipClassifyToggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_zhuangbeileixing");

            _isSelectedAll = GetChildComponent<KToggle>("Toggle_quxiao");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            RefreshCategory();
            RefreshToggleGroup();
            AddEventsListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventsListener();
            if (_timeId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timeId);
                _timeId = TimerHeap.INVALID_ID;
            }
        }

        private void AddEventsListener()
        {
            _categoryToggleGroup.onSelectedIndexChanged.AddListener(OnSelectedQualityChanged);
            _equipClassifyToggleGroup.onSelectedIndexChanged.AddListener(OnSelectedClassifyChanged);
            _isSelectedAll.onValueChanged.AddListener(OnSelectedAllChanged);
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, OnRefreshGridView);
        }

        private void RemoveEventsListener()
        {
            _categoryToggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedQualityChanged);
            _equipClassifyToggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedClassifyChanged);
            _isSelectedAll.onValueChanged.RemoveListener(OnSelectedAllChanged);
            EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, OnRefreshGridView);
        }

        private void RefreshCategory()
        {
            RefreshCategoryData();
            RefreshCategoryUI();
            _filterOption = new DecompositionFilterOption();
            onFilterOptionSelectedChanged.Invoke(_filterOption);
        }

        private void RefreshToggleGroup()
        {
            _equipClassifyToggleGroup.SelectIndex = (int)DecompositionEquipType.UselessEquip;
        }

        private void OnSelectedAllChanged(KToggle toggle, bool isSelected)
        {
            DecompositionFilterView.IsSelectedAll = !isSelected;
            RefreshGridView();
        }

        private void OnRefreshGridView(BagType bagType, uint gridPosition)
        {
            if (bagType == BagType.ItemBag)
            {
                BaseItemInfo baseItemInfo = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemInfo((int)gridPosition);

                /*
                 * 需要更新的情况如下：
                 * 1、当为null表示背包格子被清空，表示物品消失
                 * 2、当为BagItemType.Equip表示新获得装备
                 */
                if (baseItemInfo == null || baseItemInfo.Type == BagItemType.Equip)
                {
                    //重新计时，
                    if (_timeId != TimerHeap.INVALID_ID)
                    {
                        TimerHeap.DelTimer(_timeId);
                        _timeId = TimerHeap.INVALID_ID;
                    }
                    _timeId = TimerHeap.AddTimer(100, 0, RefreshGridView);
                }
            }
        }

        private void RefreshGridView()
        {
            RefreshCategoryData();
            RefreshCategoryUI();
            onFilterOptionSelectedChanged.Invoke(_filterOption);
        }

        private void OnSelectedClassifyChanged(KToggleGroup toggleGroup, int index)
        {
            _filterOption.EquipClassify = (DecompositionEquipType)index;
            onFilterOptionSelectedChanged.Invoke(_filterOption);
        }

        private void OnSelectedQualityChanged(CategoryList categoryToggleGroup, int index)
        {
            _filterOption.EquipQuality = _haveEquipQualityList[index];
            onFilterOptionSelectedChanged.Invoke(_filterOption);
        }

        private void RefreshCategoryData()
        {
            List<BaseItemInfo> equipData = PlayerDataManager.Instance.BagData.GetItemBagData().GetTypeItemInfo(BagItemType.Equip);

            if (_haveEquipQualityList == null)
            {
                _haveEquipQualityList = new List<int>();
            }

            _haveEquipQualityList.Clear();
            _haveEquipQualityList.Add(0); /////添加装备品质中“所有”选项

            for (int i = 0; i < equipData.Count; i++)
            {
                if (_haveEquipQualityList.Contains(equipData[i].Quality) == false)
                {
                    _haveEquipQualityList.Add(equipData[i].Quality);
                }
            }
            _haveEquipQualityList.Sort();
        }

        private void RefreshCategoryUI()
        {
            List<CategoryItemData> toggleNameList = new List<CategoryItemData>();

            for (int i = 0; i < _haveEquipQualityList.Count; i++)
            {
                if (_haveEquipQualityList[i] == 0)
                {
                    toggleNameList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent((int)LangEnum.ALL_ITEM_TXT)));
                    continue;
                }
                toggleNameList.Add(CategoryItemData.GetCategoryItemData(item_helper.GetItemQualityDesc(_haveEquipQualityList[i])));
            }
            _categoryToggleGroup.RemoveAll();
            _categoryToggleGroup.SetDataList<CategoryListItem>(toggleNameList);
            _categoryToggleGroup.SelectedIndex = 0;
        }
    }
}
