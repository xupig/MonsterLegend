﻿using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Game.UI.UIComponent;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class SmelterBottomView:KContainer
    {
        private MoneyItem _goldMoneyItem;
        private MoneyItem _diamondMoneyItem;
        private MoneyItem _consumeItem;

        protected override void Awake()
        {
            base.Awake();
            _goldMoneyItem = AddChildComponent<MoneyItem>("Container_buyGold");
            _goldMoneyItem.SetMoneyType(public_config.MONEY_TYPE_GOLD);
            _diamondMoneyItem = AddChildComponent<MoneyItem>("Container_buyDiamond");
            _diamondMoneyItem.SetMoneyType(public_config.MONEY_TYPE_DIAMOND);
            _consumeItem = AddChildComponent<MoneyItem>("Container_baoshi");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventsListener();
        }

        protected override void OnDisable()
        {
            RemoveEventsListener();
            base.OnDisable();
        }

        private void AddEventsListener()
        {
            EventDispatcher.AddEventListener<int, int, BaseItemData>(EquipEvents.SMELTER_CUSTOME_ITEM_CHANGED, OnCustomItemChange);
        }

        private void RemoveEventsListener()
        {
            EventDispatcher.RemoveEventListener<int, int, BaseItemData>(EquipEvents.SMELTER_CUSTOME_ITEM_CHANGED, OnCustomItemChange);
        }

        private void OnCustomItemChange(int itemId, int index, BaseItemData needItem)
        {
            switch (index)
            {
                case 1:
                    _goldMoneyItem.Visible = itemId != 0;
                    _goldMoneyItem.SetMoneyType(itemId, needItem);
                    break;
                case 2:
                    _diamondMoneyItem.Visible = itemId != 0;
                    _diamondMoneyItem.SetMoneyType(itemId, needItem);
                    break;
                case 3:
                    _consumeItem.Visible = itemId != 0;
                    _consumeItem.SetMoneyType(itemId, needItem);
                    break;
            }
        }
    }
}
