﻿

using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class SmeltSelectorView:EquipSelectorBaseView
    {
        public KComponentEvent<equip_smelting> onSelectedMaterialChanged = new KComponentEvent<equip_smelting>();

        public override void InitCategory()
        {
            List<int> levelList = equip_smelting_helper.GetLevelList();

            List<CategoryItemData> categoryDataList = new List<CategoryItemData>();
            for (int i = 0; i < levelList.Count; i++)
            {
                categoryDataList.Add(CategoryItemData.GetCategoryItemData(equip_smelting_helper.GetLevelName(levelList[i])));
            }
            _filterList.SetDataList<CategoryListItem>(categoryDataList);
        }

        public override void OnSecondCategoryChanged(KList list, int index)
        {
            equip_smelting smeltInfo = list[index].Data as equip_smelting;
            List<BaseItemData> costList = equip_smelting_helper.GetCostList(smeltInfo.__id);
            onSelectedMaterialChanged.Invoke(smeltInfo);
            DecideBottomShowItem(costList);
        }

        public override void OnFirstCategoryChanged(CategoryList list, int index)
        {
            RefreshSelectorView(index);
        }

        private void RefreshSelectorView(int filterIndex)
        {
            List<int> levelList = equip_smelting_helper.GetLevelList();
            List<equip_smelting> smeltList = equip_smelting_helper.GetAllSmeltByLevel(levelList[filterIndex]);
            _selectableList.SetDataList<SmeltMeterialItem>(smeltList, 1);
            _selectableList.SelectedIndex = 0;
            List<BaseItemData> costList = equip_smelting_helper.GetCostList(smeltList[0].__id);
            onSelectedMaterialChanged.Invoke(smeltList[0]);
            DecideBottomShowItem(costList);
        }

        public override void ShowSelectorDefault()
        {
            List<int> levelList = equip_smelting_helper.GetLevelList();
            //默认选中符合角色等级段，如果没有则选中第一个
            int levelSegment = PlayerAvatar.Player.level - PlayerAvatar.Player.level % 10;
            int selectedLevelIndex = levelList.FindIndex((item) => { return item == levelSegment; });
            if(selectedLevelIndex == -1)
            {
                selectedLevelIndex = 0;
            }
            _filterList.SelectedIndex = selectedLevelIndex;

            RefreshSelectorView(selectedLevelIndex);
        }

        public override void RefreshPoint()
        {
            List<int> levelList = equip_smelting_helper.GetLevelList();
            HashSet<int> smeltPointSet = PlayerDataManager.Instance.EquipPointManager.SmeltPointSet;
            for (int i = 0; i < _filterList.ItemList.Count; i++)
            {
                (_filterList[i] as CategoryListItem).SetPoint(smeltPointSet.Contains(levelList[i]));
            }
        }

        /// <summary>
        /// 优先显示所需消耗的物品，如果不够三个则按照金币，钻石，绑钻的顺序显示
        /// </summary>
        /// <param name="costList"></param>
        private void DecideBottomShowItem(List<BaseItemData> costList)
        {
            List<int> needItemIdList = new List<int>();
            for (int i = 0; i < costList.Count && i < 3; i++)
            {
                needItemIdList.Add(costList[i].Id);
                EventDispatcher.TriggerEvent<int, int, BaseItemData>(EquipEvents.SMELTER_CUSTOME_ITEM_CHANGED, costList[i].Id, 3 - i, costList[i]);
            }
            int minMoneyId = public_config.MONEY_TYPE_GOLD;
            for (int i = costList.Count; i < 3; i++ )
            {
                while(needItemIdList.Contains(minMoneyId))
                {
                    minMoneyId++;
                }
                EventDispatcher.TriggerEvent<int, int, BaseItemData>(EquipEvents.SMELTER_CUSTOME_ITEM_CHANGED, minMoneyId, 3 - i, null);
            }
        }
    }
}
