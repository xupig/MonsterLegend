﻿using Common.Global;
using Common.Utils;
using GameData;
using GameMain.Entities;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEquip
{
    public class RandomPropertyView:RecastPropertyView
    {
        protected override void RefreshContent()
        {
            base.RefreshContent();
            _propertyContentList.Clear();

            if (_equipItemInfo != null)
            {
                RefreshRandomPropertyName();
                RefreshRandomPropertyRegion();
            }
            else
            {
                _propertyContentList.Add(MogoLanguageUtil.GetContent(0));
            }
            _propertyList.SetPadding(0, 10, 0, 10);
            _propertyList.SetDataList<EquipPropertyItem>(_propertyContentList);
        }

        private void RefreshRandomPropertyName()
        {
            List<int> recommentAttriList = GlobalParams.GetRecommentAttri(PlayerAvatar.Player.vocation);
            List<int> attriIdList = data_parse_helper.ParseListInt(XMLManager.equip_random[_equipItemInfo.Id].__attri_type);
            string content = string.Empty;
            for (int i = 0; i < attriIdList.Count; i++)
            {
                if (i != 0)
                {
                    content += "、";
                }
                if (recommentAttriList.Contains(attriIdList[i]) == true)
                {
                    content += string.Format("{0}<color=#77ff00>({1})</color>", attri_config_helper.GetAttributeName(attriIdList[i]), MogoLanguageUtil.GetContent(114011));
                }
                else
                {
                    content += attri_config_helper.GetAttributeName(attriIdList[i]);
                }
            }

            _propertyContentList.Add(content);
        }

        private void RefreshRandomPropertyRegion()
        {
            List<int> attriIdList = data_parse_helper.ParseListInt(XMLManager.equip_random[_equipItemInfo.Id].__attri_type);
            Dictionary<int, List<int>> attriIdToValueRegionDic = data_parse_helper.ParseDictionaryIntListInt(XMLManager.equip_random[_equipItemInfo.Id].__attri_value);
            List<int> exampleValueRegion = attriIdToValueRegionDic[attriIdList[0]];
            _propertyContentList.Add(string.Format("数值范围（{0}-{1})", exampleValueRegion[0], exampleValueRegion[1]));
        }
    }
}
