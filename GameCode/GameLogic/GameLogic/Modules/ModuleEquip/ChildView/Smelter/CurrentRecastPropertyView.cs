﻿
using Common.Global;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class CurrentRecastPropertyView:RecastPropertyView
    {
        protected override void RefreshContent()
        {
            base.RefreshContent();
            _propertyContentList.Clear();
            if (_equipItemInfo != null)
            {
                RefreshPropertyContent();
                RefreshBuffContent();
                RefreshFightForce();
            }
            else
            {
                _propertyContentList.Add(MogoLanguageUtil.GetContent(0));
            }
            _propertyList.SetDataList<EquipPropertyItem>(_propertyContentList);
        }

        private void RefreshPropertyContent()
        {
            List<int> recommentAttriList = GlobalParams.GetRecommentAttri(PlayerAvatar.Player.vocation);
            List<int> propertyIdList = _equipItemInfo.GetRarityPropertyIdList();
            List<int> propertyValueIdList = _equipItemInfo.GetRarityPropertyValueList();
            for (int i = 0; i < propertyIdList.Count; i++)
            {
                if (recommentAttriList.Contains(propertyIdList[i]) == true)
                {
                    string content = string.Format("{0}<color=#77ff00>({1})</color>  +{2}", attri_config_helper.GetAttributeName(propertyIdList[i]),MogoLanguageUtil.GetContent(114011), propertyValueIdList[i].ToString());
                    _propertyContentList.Add(content);
                }
                else
                {
                    string content = string.Format("{0}  +{1}", attri_config_helper.GetAttributeName(propertyIdList[i]), propertyValueIdList[i].ToString());
                    _propertyContentList.Add(content);
                }
            }
            
        }

        private void RefreshBuffContent()
        {
            List<uint> buffList = _equipItemInfo.BuffList;
            if (buffList == null || buffList.Count <= 0)
            {
                return;
            }
            for (int i = 0; i < buffList.Count; i++)
            {
                if (buff_helper.CheckBuffId((int)buffList[i]) == false)
                {
                    continue;
                }
                string buffName = buff_helper.GetName((int)buffList[i]);
                string buffDescibe = buff_helper.GetDesc((int)buffList[i]);
                string content = string.Format("{0}:{1}", buffName, buffDescibe);
                _propertyContentList.Add(content);
            }
        }

        private void RefreshFightForce()
        {
            if (_equipItemInfo != null)
            {
                //为了达到换行效果
                string lastStr = _propertyContentList[_propertyContentList.Count - 1];
                _propertyContentList[_propertyContentList.Count - 1] = string.Concat(lastStr, "\n");

                int baseFightForce = fight_force_helper.GetFightForce(_equipItemInfo.GetBasisPropertyIdList(), _equipItemInfo.GetBasisPropertyValueList(), _equipItemInfo.UsageVocationRequired);
                int rarityFightForce = fight_force_helper.GetFightForce(_equipItemInfo.GetRarityPropertyIdList(), _equipItemInfo.GetRarityPropertyValueList(), _equipItemInfo.UsageVocationRequired);
                int buffFightForce = 0;
                int buffCount = _equipItemInfo.BuffList.Count;
                for (int i = 0; i < buffCount; i++)
                {
                    buffFightForce += buff_helper.GetFightForce((int)_equipItemInfo.BuffList[i]);
                }
                int extraMaxFightForce = equip_random_helper.GetExtraMaxFightForce(_equipItemInfo.Id);
                int extraMinFightForce = equip_random_helper.GetExtraMinFightForce(_equipItemInfo.Id);
                //114067， $(4,当前战力：{0})\n战力范围：{1}~{{2}
                _propertyContentList.Add(MogoLanguageUtil.GetContent(114067, (baseFightForce + rarityFightForce + buffFightForce), baseFightForce + extraMinFightForce, baseFightForce + extraMaxFightForce));
            }
        }
    }
}
