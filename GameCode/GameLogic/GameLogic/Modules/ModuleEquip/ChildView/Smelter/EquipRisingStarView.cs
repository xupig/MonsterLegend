﻿
using Common.Data;
using Common.Events;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEquip
{

    public class EquipRisingStarView:KContainer
    {
        private RisingStarSelectorView _selectorView;
        private RisingStarDetailView _detailView;

        private EquipItemInfo _currentEquipItemInfo;

        protected override void Awake()
        {
 	         base.Awake();
             _selectorView = AddChildComponent<RisingStarSelectorView>("Container_shengxing/Container_mulu");
             _detailView = AddChildComponent<RisingStarDetailView>("Container_shengxing/Container_right");

        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        public void Show()
        {
            _selectorView.ShowSelectorDefault();
        }

        private void AddEventListener()
        {
            _selectorView.onSelectedEquipChanged.AddListener(ShowRisingStarDetail);
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, ShowRisingStarDetail);
        }

        private void RemoveEventListener()
        {
            _selectorView.onSelectedEquipChanged.RemoveListener(ShowRisingStarDetail);
            EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, ShowRisingStarDetail);
        }

        private void ShowRisingStarDetail(BagType bagType, uint gridPosition)
        {
            ShowRisingStarDetail(bagType, (int)gridPosition);
        }

        private void ShowRisingStarDetail(BagType bagType, int gridPosition)
        {
            if (bagType == BagType.BodyEquipBag)
            {
                _currentEquipItemInfo = null;
                if (gridPosition != -1)
                {
                    _currentEquipItemInfo = PlayerDataManager.Instance.BagData.GetBagData(bagType).GetItemInfo(gridPosition) as EquipItemInfo;
                }
                if (_currentEquipItemInfo != null)
                {
                    PlayerDataManager.Instance.EquipPointManager.RemovePoint(EquipSubTab.RisingStar, _currentEquipItemInfo.GridPosition);
                }
                _detailView.Show(_currentEquipItemInfo);
            }
        }

    }
}
