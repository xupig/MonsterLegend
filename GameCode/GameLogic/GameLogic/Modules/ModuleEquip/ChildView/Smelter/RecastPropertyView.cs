﻿
using Common.Structs;
using Game.UI.UIComponent;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class RecastPropertyView:KContainer
    {
        protected KList _propertyList;
        protected List<string> _propertyContentList = new List<string>();
        protected EquipItemInfo _equipItemInfo;

        protected override void Awake()
        {
            base.Awake();
            _propertyList = GetChildComponent<KList>("List_shuxing");
            _propertyList.SetPadding(0, 0, 0, 0);
            _propertyList.SetGap(5, 0);
            _propertyList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        public void Refresh(EquipItemInfo equipItemInfo)
        {
            _equipItemInfo = equipItemInfo;
            RefreshContent();
            AdjustProperityList();
            RecalculateSize();
        }

        protected virtual void RefreshContent()
        {

        }


        private void AdjustProperityList()
        {
            Vector2 offsetVector = new Vector2(_propertyList.Padding.left, _propertyList.Padding.top);
            for (int i = 0; i < _propertyList.ItemList.Count; i++)
            {
                EquipPropertyItem item = _propertyList.ItemList[i] as EquipPropertyItem;
                StateText[] texts = item.transform.GetComponentsInChildren<StateText>();
                for (int j = 0; j < texts.Length; j++)
                {
                    texts[j].RecalculateAllStateHeight();
                }
                item.RecalculateSize();
                RectTransform rectTransform = item.GetComponent<RectTransform>();
                rectTransform.anchoredPosition = offsetVector;
                offsetVector.y -= Mathf.CeilToInt(rectTransform.rect.height);
            }

            _propertyList.RecalculateSize();
        }
    }
}
