﻿using Common.Data;
using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class RecastSelectorView : EquipSelectorBaseView
    {
        private const int EQUIPED_BAG = 0; //身上装备背包
        private const int ITME_BAG = 1; //普通背包
        private StateText _txtNoneEquip;

        public static BagType BagType = BagType.BodyEquipBag;
        public static int GridPosition = 1;

        public KComponentEvent<BagType, int> onSelectedEquipChanged = new KComponentEvent<BagType, int>();

        protected override void Awake()
        {
            base.Awake();
            _txtNoneEquip = GetChildComponent<StateText>("Container_zhizaoleft/Label_wuzhuzhuangbei");
        }

        public override void InitCategory()
        {
            List<CategoryItemData>  recastFilterList = new List<CategoryItemData>();
            recastFilterList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(114002)));
            recastFilterList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(114003)));
            _filterList.SetDataList<CategoryListItem>(recastFilterList, 1);
        }

        public override void ShowSelectorDefault()
        {
            //默认选中第一个
            _filterList.SelectedIndex = 0;
            RefreshSelectorView(0);
        }

        public override void OnSecondCategoryChanged(KList list, int index)
        {
            EquipItemInfo equipItemInfo = _selectableList[index].Data as EquipItemInfo;
            GridPosition = equipItemInfo.GridPosition;
            onSelectedEquipChanged.Invoke(BagType, equipItemInfo.GridPosition);
        }

        public override void OnFirstCategoryChanged(CategoryList list, int index)
        {
            RefreshSelectorView(index);
        }

        private void RefreshSelectorView(int filterIndex)
        {
            List<BaseItemInfo> equipItemList = null;
            if (filterIndex == EQUIPED_BAG)
            {
                BagType = BagType.BodyEquipBag;
                equipItemList = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfoList(IsCanRecast);
            }
            else if (filterIndex == ITME_BAG)
            {
                BagType = BagType.ItemBag;
                equipItemList = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemInfoList(IsCanRecast);
            }

            _selectableList.SetDataList<RecastEquipItem>(equipItemList, 1);
            _txtNoneEquip.Visible = equipItemList.Count == 0;
            if (equipItemList.Count > 0)
            {
                _selectableList.SelectedIndex = 0;
                EquipItemInfo equipItemInfo = _selectableList[0].Data as EquipItemInfo;
                GridPosition = equipItemInfo.GridPosition;
                onSelectedEquipChanged.Invoke(BagType, equipItemInfo.GridPosition);
            }
            else
            {
                //表示无可重铸装备
                GridPosition = -1;
                onSelectedEquipChanged.Invoke(BagType, -1);
            }
        }

        private bool IsCanRecast(BaseItemInfo itemInfo)
        {
            if (itemInfo is EquipItemInfo)
            {
                return equip_recast_helper.CanRecast(itemInfo as EquipItemInfo);
            }
            return false;
        }
    }
}
