﻿
using Common.Utils;
using GameData;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEquip
{
    public class RandomSkillDetailView:RecastPropertyView
    {
        protected override void RefreshContent()
        {
            base.RefreshContent();

            _propertyContentList.Clear();
            if (_equipItemInfo != null)
            {
                RefreshRandomSkillContent();
            }
            else
            {
                _propertyContentList.Add(MogoLanguageUtil.GetContent(0));
            }
            _propertyList.SetDataList<EquipPropertyItem>(_propertyContentList);
        }

        private void RefreshRandomSkillContent()
        {
            List<int> buffIdList = data_parse_helper.ParseListInt(XMLManager.equip_random[_equipItemInfo.Id].__buff_id);
            for (int i = 0; i < buffIdList.Count; i++)
            {
                if (buff_helper.CheckBuffId(buffIdList[i]) == false)
                {
                    continue;
                }
                string content = string.Format("{0} : <color=#F4D8B3>{1}</color>", buff_helper.GetName(buffIdList[i]), buff_helper.GetDesc(buffIdList[i]));
                _propertyContentList.Add(content);
            }

            if (_propertyContentList.Count == 0)
            {
                _propertyContentList.Add(MogoLanguageUtil.GetContent(114006));
            }
        }
    }
}
