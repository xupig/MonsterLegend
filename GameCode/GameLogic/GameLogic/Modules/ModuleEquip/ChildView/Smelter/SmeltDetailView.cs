﻿
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class SmeltDetailView :KContainer
    {
        private SmeltBodyPolygonList _smeltBodyList;
        private KList _materialCostList;
        private KButton _smeltOneButton;
        private KButton _getMaterialButton;
        private StateText _txtUnlockLabel;
        private StateText _txtSmeltNum;
        private equip_smelting _smeltInfo;
        private PbEquipSmeltingItemList _smeltItemList;
        private KContainer _costContainer;
        private KContainer _smeltNumContainer;
        private uint _timerId = TimerHeap.INVALID_ID;

        public static EquipItemInfo BetterEquip = null;
        private List<EquipItemInfo> _equipList = new List<EquipItemInfo>(); // 存储在播放熔炼动画过程中获得的装备

        protected override void Awake()
        {
            base.Awake();
            _smeltBodyList = AddChildComponent<SmeltBodyPolygonList>("Container_ronglianbuwei");
            _costContainer = GetChildComponent<KContainer>("Container_cost");
            _materialCostList = GetChildComponent<KList>("Container_cost/List_cailiao");
            _smeltOneButton = GetChildComponent<KButton>("Button_ronglianyici");
            _getMaterialButton = GetChildComponent<KButton>("Button_qianwangcailiao");
            _txtUnlockLabel = GetChildComponent<StateText>("Label_txtJiesuo");
            _smeltNumContainer = GetChildComponent<KContainer>("Container_rongliancishu");
            _txtSmeltNum = GetChildComponent<StateText>("Container_rongliancishu/Label_txtrongliancishu");
            _materialCostList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventsListener();
        }

        protected override void OnDisable()
        {
            ResetSmeltButton();
            RemoveEventsListener();
            base.OnDisable();
        }

        public void Show(equip_smelting smeltInfo)
        {
            _smeltInfo = smeltInfo;
            ShowBodyList();
            RefreshSmeltDetailView();
            RefreshMaterialCost();
            RefreshLockLabel();
        }

        private void AddEventsListener()
        {
            _smeltBodyList.onComplete.AddListener(OnCompleteAnimator);
            _smeltOneButton.onClick.AddListener(SmeltOneButtonClick);
            _getMaterialButton.onClick.AddListener(ShowChannel);
            EventDispatcher.AddEventListener<PbEquipSmeltingItemList>(EquipEvents.EQUIP_SMELT_RESULT, SmeltResultResp);
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, OnBagUpdate);
        }

        private void RemoveEventsListener()
        {
            _smeltBodyList.onComplete.RemoveListener(OnCompleteAnimator);
            _smeltOneButton.onClick.RemoveListener(SmeltOneButtonClick);
            _getMaterialButton.onClick.RemoveListener(ShowChannel);
            EventDispatcher.RemoveEventListener<PbEquipSmeltingItemList>(EquipEvents.EQUIP_SMELT_RESULT, SmeltResultResp);
            EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, OnBagUpdate);
        }

        private void ShowChannel()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.ItemChannel, _smeltInfo.__display_item);
        }

        private void OnBagUpdate(BagType bagType, uint position)
        {
            RefreshSmeltDetailView();
            if (bagType == BagType.ItemBag)
            {
                BaseItemInfo itemInfo = PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemInfo((int)position);
                if (itemInfo != null && itemInfo.Type == BagItemType.Equip)
                {
                    _equipList.Add(itemInfo as EquipItemInfo);
                }
            }
        }

        private void OnCompleteAnimator()
        {
            List<BaseItemData> itemList = new List<BaseItemData>();
            if (_smeltItemList == null) return;
            for (int i = 0; i < _smeltItemList.item_list.Count; i++)
            {
                EquipData equipData = new EquipData((int)_smeltItemList.item_list[i].item_id);
                equipData.StackCount = (int)_smeltItemList.item_list[i].num;
                itemList.Add(equipData);
                EquipItemInfo equipItemInfo = GetSmeltResultEquip(equipData);
                if (equipItemInfo != null && item_equipment_helper.CheckEquipFightForceBetter(equipItemInfo) == true)
                {
                    BetterEquip = equipItemInfo;
                }
            }
            UIManager.Instance.ShowPanel(PanelIdEnum.ItemObtain, itemList);
            ResetSmeltButton();
            _smeltItemList = null;
        }

        private EquipItemInfo GetSmeltResultEquip(EquipData item)
        {
            for (int i = 0; i < _equipList.Count; i++)
            {
                if (item.Id == _equipList[i].Id)
                {
                    return _equipList[i];
                }
            }
            return null;
        }

        private void SmeltResultResp(PbEquipSmeltingItemList smeltingItemList)
        {
            _smeltItemList = smeltingItemList;
            int subType = item_helper.GetEquipConfig((int)_smeltItemList.item_list[0].item_id).__subtype;
            PlayAnimator(subType);
        }

        private void PlayAnimator(int bodySubType)
        {
            _smeltBodyList.StartPlaying(bodySubType);
        }

        private void SmeltOneButtonClick()
        {
            RequestSmelt(1);
        }

        private void RequestSmelt(int smeltTimes)
        {
            List<BaseItemData> costList = equip_smelting_helper.GetCostList(_smeltInfo.__id);
            for (int i = 0; i < costList.Count; i++)
            {
                if (PlayerAvatar.Player.CheckCostLimit(costList[i].Id, costList[i].StackCount * smeltTimes, true) != 0)
                {
                    return;
                }
            }

            if ((int)PlayerAvatar.Player.cur_bag_count - PlayerDataManager.Instance.BagData.GetItemBagData().GetAllItemCount() < smeltTimes)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.BAG_NOT_ENOUGH), PanelIdEnum.Equip);
            }
            else
            {
                EquipManager.Instance.RequestSmelt(_smeltInfo.__id, smeltTimes);
                UIManager.Instance.ShowPanel(PanelIdEnum.UIOperationShield);
                _timerId = TimerHeap.AddTimer(10000, 0, ResetSmeltButton);
            }
        }

        private void ResetSmeltButton()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.UIOperationShield);
            if (_timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = TimerHeap.INVALID_ID;
            }
        }

        private void ShowBodyList()
        {
            List<int> bodyPositionList = equip_smelting_helper.GetProductEquipBody(_smeltInfo.__id);
            _smeltBodyList.SetData(bodyPositionList);
        }

        private void RefreshMaterialCost()
        {
            _costContainer.Visible = PlayerAvatar.Player.level >= _smeltInfo.__level_limit;
            if (_costContainer.Visible == true)
            {
                List<BaseItemData> costList = equip_smelting_helper.GetCostList(_smeltInfo.__id);
                _materialCostList.SetDataList<SmeltNeedMaterialItem>(costList);
            }
        }

        private void RefreshSmeltDetailView()
        {
            List<BaseItemData> costList = equip_smelting_helper.GetCostList(_smeltInfo.__id);
            _smeltNumContainer.Visible = PlayerAvatar.Player.level >= _smeltInfo.__level_limit;
            BaseItemData displayItem = costList.Find((item) => { return item.Id == _smeltInfo.__display_item; });
            int bagNum = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(_smeltInfo.__display_item);
            //114008, 熔炼次数：{0}
            _txtSmeltNum.CurrentText.text = MogoLanguageUtil.GetContent(114008, bagNum / displayItem.StackCount);
            _getMaterialButton.Visible = (bagNum / displayItem.StackCount == 0 && PlayerAvatar.Player.level >= _smeltInfo.__level_limit);
            _smeltOneButton.Visible = (bagNum / displayItem.StackCount != 0 && PlayerAvatar.Player.level >= _smeltInfo.__level_limit);

        }

        private void RefreshLockLabel()
        {
            _txtUnlockLabel.Visible = PlayerAvatar.Player.level < _smeltInfo.__level_limit;
            _txtUnlockLabel.CurrentText.text = MogoLanguageUtil.GetContent(114005, _smeltInfo.__level_limit);
        }
    }
}
