﻿using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public abstract class EquipSelectorBaseView:KContainer
    {
        protected KList _selectableList;
        protected CategoryList _filterList;
        protected KScrollView _scrollView;
        protected override void Awake()
        {
 	        base.Awake();
            _scrollView = GetChildComponent<KScrollView>("Container_zhizaoleft/ScrollView_ScrollView");
            _selectableList = GetChildComponent<KList>("Container_zhizaoleft/ScrollView_ScrollView/mask/content");
            _filterList = GetChildComponent<CategoryList>("List_categoryList");
            _selectableList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _selectableList.SetPadding(5, 0, 5, 0);

            InitCategory();

        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            RefreshPoint();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        protected virtual void AddEventListener()
        {
            _filterList.onSelectedIndexChanged.AddListener(FirstCategoryChanged);
            _selectableList.onSelectedIndexChanged.AddListener(OnSecondCategoryChanged);
            EventDispatcher.AddEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshPoint);
        }

        protected virtual void RemoveEventListener()
        {
            _filterList.onSelectedIndexChanged.RemoveListener(FirstCategoryChanged);
            _selectableList.onSelectedIndexChanged.RemoveListener(OnSecondCategoryChanged);
            EventDispatcher.RemoveEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshPoint);
        }

        public abstract void InitCategory();
        public abstract void ShowSelectorDefault();
        public abstract void OnFirstCategoryChanged(CategoryList list, int index);
        public abstract void OnSecondCategoryChanged(KList list, int index);

        public virtual void RefreshPoint()
        {
            ;
        }

        private void FirstCategoryChanged(CategoryList list, int index)
        {
            OnFirstCategoryChanged(list, index);
            PlayShowAnimator();
        }

        private void PlayShowAnimator()
        {
            for (int i = 0; i < _selectableList.ItemList.Count; i++)
            {
                _selectableList.ItemList[i].transform.localPosition = _selectableList.ItemList[0].transform.localPosition;
                TweenPosition.Begin(_selectableList.ItemList[i].gameObject, 0.2f, _selectableList.ItemList[0].transform.localPosition - i * (new Vector3(0, 95f, 0)));
            }
        }

    }
}
