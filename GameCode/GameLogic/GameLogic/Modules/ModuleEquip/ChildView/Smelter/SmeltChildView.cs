﻿using Common.ClientConfig;
using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class SmeltChildView:KContainer
    {
        private SmeltSelectorView _selectorView;
        private SmeltDetailView _detailView;
        private int _oldFightForce = 0;

        protected override void Awake()
        {
            base.Awake();

            _selectorView = AddChildComponent<SmeltSelectorView>("Container_mulu");
            _detailView = AddChildComponent<SmeltDetailView>("Container_right");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            _oldFightForce = (int)PlayerAvatar.Player.fight_force;
            AddEventsListener();
        }

        protected override void OnDisable()
        {
            RemoveEventsListener();
            base.OnDisable();
        }

        public void Show()
        {
            _selectorView.ShowSelectorDefault();
        }

        private void AddEventsListener()
        {
            _selectorView.onSelectedMaterialChanged.AddListener(ShowSmeltInfo);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.fight_force, FightForceChange);
        }

        private void RemoveEventsListener()
        {
            _selectorView.onSelectedMaterialChanged.RemoveListener(ShowSmeltInfo);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.fight_force, FightForceChange);
        }

        private void ShowSmeltInfo(equip_smelting smeltInfo)
        {
            _detailView.Show(smeltInfo);
        }

        private void FightForceChange()
        {
            PlayerDataManager.Instance.SmelterData.SmeltFightForceChange += (int)PlayerAvatar.Player.fight_force - _oldFightForce;
        }
    }
}
