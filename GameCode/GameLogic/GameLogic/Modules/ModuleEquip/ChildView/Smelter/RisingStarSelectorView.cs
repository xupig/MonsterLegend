﻿
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class RisingStarSelectorView:EquipSelectorBaseView
    {
        private const int EQUIPED_BAG = 0; //身上装备背包
        private StateText _txtNoneEquip;
        public KComponentEvent<BagType, int> onSelectedEquipChanged = new KComponentEvent<BagType, int>();

        protected override void Awake()
        {
            base.Awake();
            _txtNoneEquip = GetChildComponent<StateText>("Container_zhizaoleft/Label_txtWukeshengji");

        }

        protected override void AddEventListener()
        {
            base.AddEventListener();
            EventDispatcher.AddEventListener<int,string,string>(EquipEvents.SELECT_RISING_STAR, TrySelectGridPosition);
        }

        protected override void RemoveEventListener()
        {
            base.RemoveEventListener();
            EventDispatcher.RemoveEventListener<int, string, string>(EquipEvents.SELECT_RISING_STAR, TrySelectGridPosition);
        }

        public override void InitCategory()
        {
            List<CategoryItemData> risingStarFilterList = new List<CategoryItemData>();
            risingStarFilterList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(114002)));
            _filterList.SetDataList<CategoryListItem>(risingStarFilterList, 1);
        }

        public override void ShowSelectorDefault()
        {
            //默认选中第一个
            _filterList.SelectedIndex = 0;
            RefreshSelectorView(0);
        }

        public override void OnFirstCategoryChanged(CategoryList list, int index)
        {
            RefreshSelectorView(index);
        }

        public override void OnSecondCategoryChanged(KList list, int index)
        {
            EquipItemInfo equipItemInfo = _selectableList[index].Data as EquipItemInfo;
            onSelectedEquipChanged.Invoke(BagType.BodyEquipBag, equipItemInfo.GridPosition);
        }

        private void RefreshSelectorView(int filterIndex)
        {
            List<BaseItemInfo> equipItemList = null;
            if (filterIndex == EQUIPED_BAG)
            {
                equipItemList = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetAllItemInfo().Values.ToList();
            }
            _selectableList.SetDataList<RisingStarEquipItem>(equipItemList);
            _txtNoneEquip.Visible = equipItemList.Count == 0;

            if (equipItemList.Count > 0)
            {
                _selectableList.SelectedIndex = 0;
                onSelectedEquipChanged.Invoke(BagType.BodyEquipBag, equipItemList[0].GridPosition);
            }
            else
            {
                //表示无可重铸装备
                onSelectedEquipChanged.Invoke(BagType.BodyEquipBag, -1);
            }
        }

        private void TrySelectGridPosition(int gridPosition, string direction, string langId)
        {
            RectTransform selectRect =  GetRectAndMoveToGridPosition(gridPosition);
            if(selectRect != null)
            {
                //显示箭头
                string[] result = MogoUtils.CopyGameObjectPath(selectRect.transform);
                if (result != null)
                {
                    PanelIdEnum.GuideArrow.Show(new string[] { direction, "url", result[0], result[1], langId });
                }
            }
        }

        private RectTransform GetRectAndMoveToGridPosition(int gridPosition)
        {
            List<BaseItemInfo> equipItemList = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetAllItemInfo().Values.ToList();
            RectTransform maskRect = _scrollView.Mask.gameObject.GetComponent<RectTransform>();
            RectTransform contentRect = _scrollView.Content.gameObject.GetComponent<RectTransform>();
            RectTransform selectRect = null;

            if (contentRect.sizeDelta.y > maskRect.sizeDelta.y)
            {
                for (int i = 0; i < equipItemList.Count; i++)
                {
                    if(equipItemList[i].GridPosition == gridPosition)
                    {
                        selectRect = _selectableList[i].gameObject.GetComponent<RectTransform>();
                    }
                }
                if (selectRect != null)
                {
                    contentRect.anchoredPosition = new Vector2(contentRect.anchoredPosition.x, Mathf.Min(contentRect.sizeDelta.y - maskRect.sizeDelta.y, -selectRect.anchoredPosition.y));
                }
            }

            return selectRect;

        }
    }
}
