﻿using Common.Base;
using Common.Chat;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class RecastChildView:KContainer
    {

        private RecastSelectorView _selectorView;
        private CurrentRecastPropertyView _currentPropertyView;
        private RandomPropertyView _randomPropertyView;
        private RandomSkillView _randomSkillView;
        private KParticle _particle;
        private StateText _txtRecastNum;

        private KContainer _costContainer;
        private KButton _recastButton;
        private KButton _recastSkillDetailButton;
        private KList _costList;

        private PbEquipRecastResp _recastResult;

        private EquipItemInfo _currentEquipItemInfo;

        private int _oldFightForce = 0;

        private bool _isRecast = false;
        private uint timerId = TimerHeap.INVALID_ID;

        protected override void Awake()
        {
            base.Awake();
            _selectorView = AddChildComponent<RecastSelectorView>("Container_mulu");
            _currentPropertyView = AddChildComponent<CurrentRecastPropertyView>("Container_right/Container_xiyoushuxing/ScrollView_content/mask/content");
            _randomPropertyView = AddChildComponent<RandomPropertyView>("Container_left/Container_suijishuzhi/ScrollView_content/mask/content");
            _randomSkillView = AddChildComponent<RandomSkillView>("Container_left/Container_jineng/ScrollView_jinengshuxing/mask/content");
            _recastSkillDetailButton = GetChildComponent<KButton>("Container_left/Container_jineng/Button_Gantang");
            _particle = AddChildComponent<KParticle>("Container_right/fx_ui_chongzhu");

            _recastButton = GetChildComponent<KButton>("Button_chongzhu");
            _txtRecastNum = GetChildComponent<StateText>("Label_txtChongzhucishu");
            _costContainer = GetChildComponent<KContainer>("Container_cost");
            _costList = GetChildComponent<KList>("Container_cost/List_cailiao");
            _costList.SetDirection(KList.KListDirection.LeftToRight, 1);

            _particle.Stop();

            _particle.transform.localPosition = new Vector3(38, 0, 0);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            _oldFightForce = (int)PlayerAvatar.Player.fight_force;
            EquipManager.Instance.RequesetLastRecastInfo();
            AddEventsListener();
        }

        protected override void OnDisable()
        {
            RemoveTimer();
            RemoveEventsListener();
            base.OnDisable();
        }

        public void Show()
        {
            _recastResult = null;
            _selectorView.ShowSelectorDefault();
            PlayerDataManager.Instance.EquipPointManager.RemovePoint(EquipSubTab.Recast);
        }

        private void AddEventsListener()
        {
            _recastButton.onClick.AddListener(OnRecastButtonClick);
            _selectorView.onSelectedEquipChanged.AddListener(ShowRecastDetail);
            _particle.onComplete.AddListener(OnCompleteParticle);
            _recastSkillDetailButton.onClick.AddListener(ShowRecastSkillDetail);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.fight_force, FightForceChange);
            EventDispatcher.AddEventListener<PbEquipRecastResp>(EquipEvents.RECAST_RESULT, OnRecastResp);
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, OnBagUpdate);
        }

        private void RemoveEventsListener()
        {
            _recastButton.onClick.RemoveListener(OnRecastButtonClick);
            _selectorView.onSelectedEquipChanged.RemoveListener(ShowRecastDetail);
            _particle.onComplete.RemoveListener(OnCompleteParticle);
            _recastSkillDetailButton.onClick.RemoveListener(ShowRecastSkillDetail);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.fight_force, FightForceChange);
            EventDispatcher.RemoveEventListener<PbEquipRecastResp>(EquipEvents.RECAST_RESULT, OnRecastResp);
            EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, OnBagUpdate);
        }

        private void OnRecastResp(PbEquipRecastResp recastResult)
        {
            ResetRecastState();
            _recastResult = recastResult;
            RecastCompareWrapper wrapper = new RecastCompareWrapper();
            if (_recastResult.pkg_type == 0) return; //表示为空
            wrapper.CurrentEquipInfo = PlayerDataManager.Instance.BagData.GetBagData((BagType)_recastResult.pkg_type).GetItemInfo((int)_recastResult.pos) as EquipItemInfo;
            PbGridItem gridItem = new PbGridItem();
            gridItem.grid_index = _recastResult.pos;
            gridItem.item = _recastResult.recast_equip;
            EquipItemInfo equipItemInfo = new EquipItemInfo(gridItem);
            wrapper.AfterEquipInfo = equipItemInfo;
            UIManager.Instance.ShowPanel(PanelIdEnum.RecastCompare, wrapper);
        }

        private void PlayParticle()
        {
            _particle.Play();
        }

        private void OnCompleteParticle()
        {
            EquipManager.Instance.RequesetRecast(RecastSelectorView.BagType, RecastSelectorView.GridPosition);
        }

        private void OnBagUpdate(BagType bagType, uint gridPosition)
        {
            if (bagType == RecastSelectorView.BagType && gridPosition == RecastSelectorView.GridPosition)
            {
                ShowRecastDetail(RecastSelectorView.BagType, RecastSelectorView.GridPosition);
            }
        }

        private void ShowRecastSkillDetail()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.RecastSkill, _currentEquipItemInfo);
        }

        private void ShowRecastDetail(BagType bagType, int gridPosition)
        {
            _currentEquipItemInfo = null;
            if (gridPosition != -1)
            {
                _currentEquipItemInfo = PlayerDataManager.Instance.BagData.GetBagData(bagType).GetItemInfo(gridPosition) as EquipItemInfo;
            }
            _currentPropertyView.Refresh(_currentEquipItemInfo);
            _randomPropertyView.Refresh(_currentEquipItemInfo);
            _randomSkillView.Refresh(_currentEquipItemInfo);
            RefreshCostList(_currentEquipItemInfo);
            _recastButton.Visible = _currentEquipItemInfo != null;
            _txtRecastNum.Visible = _currentEquipItemInfo != null;
            if (_currentEquipItemInfo != null)
            {
                _txtRecastNum.CurrentText.text = MogoLanguageUtil.GetContent(114010, _currentEquipItemInfo.RecastCount);
            }
        }

        private void OnRecastButtonClick()
        {
            if (_isRecast == true) return;
            BaseItemData costItemData = equip_recast_helper.GetRecastCost(_currentEquipItemInfo);
            List<BaseItemData> costItemDataList = equip_recast_helper.GetRecastCostList(_currentEquipItemInfo);
            int limit = PlayerAvatar.Player.CheckCostLimit(costItemData.Id, costItemData.StackCount, false);
            if (limit == 0) // 材料充足
            {
                OnConfirm();
            }
            else // 材料不足，需要消耗货币类材料
            {
                BaseItemData replaceItemData = equip_recast_helper.GetRecastReplaceCost(_currentEquipItemInfo);
                limit = PlayerAvatar.Player.CheckCostLimit(replaceItemData.Id, replaceItemData.StackCount, false);
                if (limit == -1)//绑钻不足，用钻石替换
                {

                }
                else if (limit == 0)
                {
                    string content = string.Empty;
                    if (costItemDataList.Count > 1)
                    {
                        content = MogoLanguageUtil.GetContent(114014, item_helper.GetName(costItemDataList[0].Id), costItemDataList[0].StackCount, item_helper.GetName(costItemDataList[1].Id), costItemDataList[1].StackCount);
                    }
                    else
                    {
                        content = MogoLanguageUtil.GetContent(114015, item_helper.GetName(costItemDataList[0].Id), costItemDataList[0].StackCount);
                    }
                    MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, OnConfirm, OnCancel, true, false, true);
                }
                else
                {
                    ///74652, {0}不足
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74652, item_helper.GetName(limit)), PanelIdEnum.EquipSmelter);
                }
            }
        }

        private void OnConfirm()
        {
            _isRecast = true;
            timerId = TimerHeap.AddTimer(5000, 0, ResetRecastState);
            PlayParticle();
        }

        private void OnCancel()
        {

        }

        private void RefreshCostList(EquipItemInfo equipItemInfo)
        {
            if (equipItemInfo != null)
            {
                _costContainer.Visible = true;
                _costList.RemoveAll();
                List<BaseItemData> costItemDataList = equip_recast_helper.GetRecastCostList(equipItemInfo);
                BaseItemData replaceItemData = equip_recast_helper.GetRecastReplaceCost(equipItemInfo);
                BaseItemData costItemData = equip_recast_helper.GetRecastCost(equipItemInfo);
                BaseItemData cancelItemData = equip_recast_helper.GetRecastCancelItemCost(equipItemInfo);
                _costList.SetDataList<SmeltNeedMaterialItem>(costItemDataList);
                EventDispatcher.TriggerEvent<int, int, BaseItemData>(EquipEvents.SMELTER_CUSTOME_ITEM_CHANGED, replaceItemData.Id, 1, replaceItemData);
                EventDispatcher.TriggerEvent<int, int, BaseItemData>(EquipEvents.SMELTER_CUSTOME_ITEM_CHANGED, costItemData.Id, 3, costItemData);

                ///消耗材料不足时显示绑钻
                if (PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(cancelItemData.Id) > 0)
                {
                    EventDispatcher.TriggerEvent<int, int, BaseItemData>(EquipEvents.SMELTER_CUSTOME_ITEM_CHANGED, cancelItemData.Id, 2, cancelItemData);
                }
                else
                {
                    EventDispatcher.TriggerEvent<int, int, BaseItemData>(EquipEvents.SMELTER_CUSTOME_ITEM_CHANGED, public_config.MONEY_TYPE_BIND_DIAMOND, 2, null);
                }
            }
            else
            {
                _costContainer.Visible = false;
            }
        }

        private void FightForceChange()
        {
            PlayerDataManager.Instance.SmelterData.RecastFightForceChange += (int)PlayerAvatar.Player.fight_force - _oldFightForce;
        }

        private void ResetRecastState()
        {
            _isRecast = false;
            RemoveTimer();
        }

        private void RemoveTimer()
        {
            if (timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(timerId);
                timerId = TimerHeap.INVALID_ID;
            }
        }
    }
}
