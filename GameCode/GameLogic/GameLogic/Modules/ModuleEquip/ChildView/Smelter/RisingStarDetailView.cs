﻿
using Common.Base;
using Common.Data;
using Common.Events;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class RisingStarDetailView:KContainer
    {
        private EquipInfoView _currentEquip;
        private EquipInfoView _risingStarEquip;
        private StateText _txtOpenCondition;
        private StateText _txtTip;
        private StateText _txtOpenTips;
        private KButton _unopenBtn;
        private KButton _goRisingBtn;
        private KButton _ruleBtn;

        private KList _costUIList;
        private KContainer _costContainer;
        private KContainer _unknownEquip;

        private StateImage _imgCurrent;
        private StateImage _imgRisingStar;

        private EquipItemInfo _itemInfo;

        protected override void Awake()
        {
            base.Awake();
            _currentEquip = AddChildComponent<EquipInfoView>("Container_dangqian");
            _risingStarEquip = AddChildComponent<EquipInfoView>("Container_shengxingzhuangbei");
            _txtOpenCondition = GetChildComponent<StateText>("Label_txtKaiqi");
            _txtTip = GetChildComponent<StateText>("Label_txtTishi");
            _txtOpenTips = GetChildComponent<StateText>("Label_txtWeikaifangxingji");
            _unopenBtn = GetChildComponent<KButton>("Button_Weikaifang");
            _goRisingBtn = GetChildComponent<KButton>("Button_Shengxing");
            _ruleBtn = GetChildComponent<KButton>("Button_Gantang");
            _unknownEquip = GetChildComponent<KContainer>("Container_suiji");
            _imgCurrent = _unknownEquip.gameObject.transform.GetChild(1).GetComponent<StateImage>();
            _imgRisingStar = _unknownEquip.gameObject.transform.GetChild(0).GetComponent<StateImage>();
            _costContainer = GetChildComponent<KContainer>("Container_cost");
            _costUIList = GetChildComponent<KList>("Container_cost/List_cailiao");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        public void Show(EquipItemInfo itemInfo)
        {
            _itemInfo = itemInfo;
            RefreshCurrentEquipInfo();
            RefreshCost();
            RefreshButtonTextVisible();
            RisingStarState state = GetRisingStarState(_itemInfo);
            if (state == RisingStarState.Open)
            {
                EquipManager.Instance.CheckUpgradeEquipInfo(BagType.BodyEquipBag, itemInfo.GridPosition);
            }
            else
            {
                RefreshRisingEquipInfo(null);
            }
        }

        private void AddEventListener()
        {
            _ruleBtn.onClick.AddListener(ShowRule);
            _goRisingBtn.onClick.AddListener(RisingButtonClick);
            EventDispatcher.AddEventListener<PbEquipUpgradeResp>(EquipEvents.GET_RISING_STAR_CHECK, RefreshRisingEquipInfo);
        }

        private void RemoveEventListener()
        {
            _ruleBtn.onClick.RemoveListener(ShowRule);
            _goRisingBtn.onClick.RemoveListener(RisingButtonClick);
            EventDispatcher.RemoveEventListener<PbEquipUpgradeResp>(EquipEvents.GET_RISING_STAR_CHECK, RefreshRisingEquipInfo);
        }

        private void RefreshCurrentEquipInfo()
        {
            _imgCurrent.Visible = _itemInfo == null;
            _currentEquip.ShowEquip(_itemInfo);
        }

        private void RefreshRisingEquipInfo(PbEquipUpgradeResp equipUpgrade)
        {
            _imgRisingStar.Visible = equipUpgrade == null;
            if (equipUpgrade == null)
            {
                _risingStarEquip.ShowEquip(null);
            }
            else
            {
                PbGridItem item = new PbGridItem();
                item.grid_index = equipUpgrade.pos;
                item.item = equipUpgrade.upgraded_equip;
                EquipItemInfo itemInfo = new EquipItemInfo(item);
                _risingStarEquip.ShowEquip(itemInfo);
            }
        }

        private void ShowRule()
        {
            RuleTipsDataWrapper wrapper = new RuleTipsDataWrapper(MogoLanguageUtil.GetContent(114063), _ruleBtn.GetComponent<RectTransform>(), RuleTipsDirection.BottomLeft);
            UIManager.Instance.ShowPanel(PanelIdEnum.RuleTips, wrapper);
        }

        private void RisingButtonClick()
        {
            string openContent = equip_upgrade_helper.GetLimitContent(_itemInfo);
            if (openContent == string.Empty)
            {
                List<ItemData> costList = equip_upgrade_helper.GetCost(_itemInfo);
                List<BaseItemData> baseItemDataList = new List<BaseItemData>();
                for (int i = 0; i < costList.Count; i++)
                {
                    baseItemDataList.Add(costList[i]);
                }
                int result = PlayerAvatar.Player.CheckCostLimit(baseItemDataList);
                if (result == 0)
                {
                    EquipManager.Instance.RequestUpgradeEquip(BagType.BodyEquipBag, _itemInfo.GridPosition);
                }
                else
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, (56743).ToLanguage(item_helper.GetName(result)), PanelIdEnum.EquipSmelter);
                    UIManager.Instance.ShowPanel(PanelIdEnum.ItemChannel, result);
                }
            }
        }

        private void RefreshCost()
        {
            RisingStarState state = GetRisingStarState(_itemInfo);
            if (state != RisingStarState.None && state != RisingStarState.Upstage)
            {
                _costContainer.Visible = true;
                List<ItemData> costList = equip_upgrade_helper.GetCost(_itemInfo);
                _costUIList.RemoveAll();
                _costUIList.SetDataList<SmeltNeedMaterialItem>(costList);
                EventDispatcher.TriggerEvent<int, int, BaseItemData>(EquipEvents.SMELTER_CUSTOME_ITEM_CHANGED, 0, 1, null);
                EventDispatcher.TriggerEvent<int, int, BaseItemData>(EquipEvents.SMELTER_CUSTOME_ITEM_CHANGED, costList[0].Id, 2, costList[0]);
                EventDispatcher.TriggerEvent<int, int, BaseItemData>(EquipEvents.SMELTER_CUSTOME_ITEM_CHANGED, costList[1].Id, 3, costList[1]);
            }
            else
            {
                _costContainer.Visible = false;
                EventDispatcher.TriggerEvent<int, int, BaseItemData>(EquipEvents.SMELTER_CUSTOME_ITEM_CHANGED, 0, 1, null);
                EventDispatcher.TriggerEvent<int, int, BaseItemData>(EquipEvents.SMELTER_CUSTOME_ITEM_CHANGED, 0, 2, null);
                EventDispatcher.TriggerEvent<int, int, BaseItemData>(EquipEvents.SMELTER_CUSTOME_ITEM_CHANGED, 0, 3, null);
            }
        }

        private void RefreshButtonTextVisible()
        {
            RisingStarState state = GetRisingStarState(_itemInfo);
            switch (state)
            {
                case RisingStarState.None:
                    _txtTip.Visible = true;
                    _txtTip.CurrentText.text = MogoLanguageUtil.GetContent(114056);
                    _unopenBtn.Visible = false;
                    _goRisingBtn.Visible = false;
                    _txtOpenTips.Visible = false;
                    _txtOpenCondition.Clear();
                    break;
                case RisingStarState.Upstage:
                    _txtTip.Visible = true;
                    _txtTip.CurrentText.text = MogoLanguageUtil.GetContent(114065);
                    _unopenBtn.Visible = false;
                    _goRisingBtn.Visible = false;
                    _txtOpenTips.Visible = false;
                    _txtOpenCondition.Clear();
                    break;
                case RisingStarState.Unopen:
                    _txtTip.Visible = false;
                    _unopenBtn.Visible = true;
                    _goRisingBtn.Visible = false;
                    _txtOpenTips.Visible = true;
                    break;
                case RisingStarState.Open:
                    _txtTip.Visible = false;
                    _unopenBtn.Visible = false;
                    _goRisingBtn.Visible = true;
                    _txtOpenTips.Visible = false;
                    string openContent = equip_upgrade_helper.GetLimitContent(_itemInfo);
                    _txtOpenCondition.CurrentText.text = openContent;
                    if (openContent == string.Empty)
                    {
                        SetButtonGray(_goRisingBtn, 1);
                    }
                    else
                    {
                        SetButtonGray(_goRisingBtn, 0);
                    }
                    break;
            }

        }

        private void SetButtonGray(KButton button, float grayValue)
        {
            ImageWrapper[] wrappers = button.gameObject.GetComponentsInChildren<ImageWrapper>(true);
            for (int i = 0; i < wrappers.Length; i++)
            {
                wrappers[i].SetGray(grayValue);
            }
        }

        private RisingStarState GetRisingStarState(EquipItemInfo itemInfo)
        {
            equip_upgrade config = equip_upgrade_helper.GetConfig((int)itemInfo.SubType, itemInfo.Level, itemInfo.Quality, itemInfo.Star);
            equip_upgrade nextConfig = equip_upgrade_helper.GetNextConfig((int)itemInfo.SubType, itemInfo.Level, itemInfo.Quality, itemInfo.Star);
            if (itemInfo == null || config == null)
            {
                if (nextConfig == null)
                {
                    return RisingStarState.None;
                }
                else
                {
                    return RisingStarState.Upstage;
                }
            }
            else if (equip_upgrade_helper.CanUpgrade(itemInfo) == false)
            {
                return RisingStarState.Unopen;
            }
            else
            {
                return RisingStarState.Open;
            }
        }
    }

    public enum RisingStarState
    {
        None = 0, //无法升星
        Unopen = 1, //未开启升星
        Open = 2, //可升星
        Upstage = 3, //顶级装备
    }
}
