﻿
using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Game.UI;
using Game.UI.UIComponent;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class EquipInfoView : KContainer
    {
        private StateText _txtEquipName;
        private IconContainer _equipQualityContainer;
        private IconContainer _equipIconContainer;
        private KContainer _starContainer;
        private KDummyButton _iconButton;

        private EquipItemInfo _data;

        protected override void Awake()
        {
            base.Awake();
            _txtEquipName = GetChildComponent<StateText>("Label_Name");
            _equipQualityContainer = AddChildComponent<IconContainer>("Container_pinzhikuang");
            _iconButton = AddChildComponent<KDummyButton>("Container_pinzhikuang");
            _iconButton.onClick.AddListener(ShowTips);
            _equipIconContainer = AddChildComponent<IconContainer>("Container_icon");
            AddChildComponent<RaycastComponent>("Container_icon");
            _starContainer = GetChildComponent<KContainer>("Container_star");
            AddChildComponent<RaycastComponent>("Container_star");
        }

        public void ShowEquip(EquipItemInfo equipItemInfo)
        {
            _data = equipItemInfo;
            if (equipItemInfo != null)
            {
                _txtEquipName.CurrentText.text = _data.Name;
                _equipIconContainer.Visible = true;
                _equipIconContainer.Visible = true;
                _starContainer.Visible = true;
                _equipQualityContainer.Visible = true;
                _equipQualityContainer.SetIcon(6904, OnLoadedQuality);
                _equipIconContainer.SetIcon(_data.Icon);
                RefreshEquipStar();
            }
            else
            {
                _txtEquipName.Clear();
                _equipIconContainer.Visible = false;
                _equipIconContainer.Visible = false;
                _starContainer.Visible = false;
                _equipQualityContainer.Visible = false;
            }
        }


        private void OnLoadedQuality(GameObject obj)
        {
            ImageWrapper imageWrapper = obj.GetComponent<ImageWrapper>();

            switch (_data.Quality)
            {
                case public_config.ITEM_QUALITY_GREEN:
                    imageWrapper.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_NEW_GREEN);
                    break;
                case public_config.ITEM_QUALITY_BLUE:
                    imageWrapper.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_NEW_BLUE);
                    break;
                case public_config.ITEM_QUALITY_PURPLE:
                    imageWrapper.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_NEW_PURPLE);
                    break;
                case public_config.ITEM_QUALITY_ORANGE:
                    imageWrapper.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_NEW_ORANGE);
                    break;
                case public_config.ITEM_QUALITY_GOLDEN:
                    imageWrapper.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_NEW_DARK_GOLDEN);
                    break;
            }
        }


        private void RefreshEquipStar()
        {
            int starLevel = 0;
            if (_data != null && _data.Quality >= 4)
            {
                starLevel = _data.Star;
            }
            if (starLevel == 0)
            {
                _starContainer.Visible = false;
            }
            else
            {
                _starContainer.Visible = true;
            }
            for (int i = 0; i < starLevel; i++)
            {
                _starContainer.GetChild(i).transform.GetChild(1).gameObject.SetActive(true);
            }
            for (int i = starLevel; i < 5; i++)
            {
                _starContainer.GetChild(i).transform.GetChild(1).gameObject.SetActive(false);
            }
        }

        private void ShowTips()
        {
            if (_data != null)
            {
                ToolTipsManager.Instance.ShowItemTip(_data, PanelIdEnum.Equip, EquipState.Wearing, false);
            }
        }
    }
}
