﻿
using Common.Utils;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEquip
{
    public class SuitEffectRightToolView:SuitEffectToolView
    {
        public override void RefreshEffectLabel()
        {
            string suitName = equip_suit_helper.GetName(_data.SuitEffectId);
            int equipedBodyCount = equip_suit_helper.GetEquipedSuitPartCount(_data.SuitEffectId);
            int effectId = equip_suit_helper.GetEffectId(_data.SuitEffectId, equipedBodyCount + 1);
            int fightForce = equip_suit_helper.GetFightForce(_data.SuitEffectId, equipedBodyCount);
            List<string> contentList = effectId != -1 ? attri_effect_helper.GetAttributeDescList(effectId) : new List<string>();

            Dictionary<int, int> suitEffectDic = equip_suit_helper.GetSuitEffectDict(_data.SuitEffectId);
            string effectChangeContent = string.Empty;
            string effectContent = string.Empty;

            //如果套装效果中包含当前装备的该套装效果的装备数量+1，则获得此数量的套装效果。
            //如果目前装备数量为0，则获得套装效果
            if (suitEffectDic.ContainsKey(equipedBodyCount + 1) == true || equipedBodyCount == 0)
            {
                for (int i = 0; i < contentList.Count; i++)
                {
                    if (i == 0)
                    {
                        effectContent = contentList[i];
                    }
                    else
                    {
                        effectContent += "," + contentList[i];
                    }
                }
                effectChangeContent = MogoLanguageUtil.GetContent(108015, suitName, effectContent);
            }
            _txtSuitEffectTip.CurrentText.text = effectChangeContent;
            _txtSuitFightForce.CurrentText.text = MogoLanguageUtil.GetContent(108013, fightForce);
            _txtSuitName.CurrentText.text = suitName;
        }
    }
}
