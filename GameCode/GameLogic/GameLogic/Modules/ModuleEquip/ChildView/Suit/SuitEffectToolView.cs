﻿using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class SuitEffectToolView : KContainer
    {
        private KList _effectList;
        protected StateText _txtSuitFightForce;
        protected StateText _txtSuitEffectTip;
        protected StateText _txtSuitName;

        protected SuitEffectDataWrapper _data;

        protected override void Awake()
        {
            base.Awake();
            _effectList = GetChildComponent<KList>("List_xiaoguo");
            _txtSuitFightForce = GetChildComponent<StateText>("Label_txtZhanli");
            _txtSuitEffectTip = GetChildComponent<StateText>("Label_txtTanzhuang");

            _txtSuitName = GetChildComponent<StateText>("Container_biaoti/Label_txtJuanzhoumingcheng");

            _effectList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        public void Show(SuitEffectDataWrapper wrapper)
        {
            _data = wrapper;
            RefreshEffectLabel();
            RefreshEffectList();
        }

        public virtual void RefreshEffectLabel()
        {

        }

        private void RefreshEffectList()
        {
            List<SuitEquipNumEffectDataWrapper> dataWrapperList = GetPackageSuitEffectData(_data.SuitEffectId);
            _effectList.RemoveAll();
            _effectList.SetDataList<SuitEquipNumEffectItem>(dataWrapperList);
            RecalculateListLayout();
        }

        private List<SuitEquipNumEffectDataWrapper> GetPackageSuitEffectData(int suitEffectId)
        {
            //key表示装备该套装效果的装备的数量，value表示带来的属性效果
            Dictionary<int, int> suitEffectDic = equip_suit_helper.GetSuitEffectDict(suitEffectId);
            int suitEffectEquipedNum = equip_suit_helper.GetEquipedSuitPartCount(suitEffectId);
            List<SuitEquipNumEffectDataWrapper> result = new List<SuitEquipNumEffectDataWrapper>();
            //记录将要显示已装备的那条数据
            SuitEquipNumEffectDataWrapper showNameDataWrapper = null;
            int meetMaxKey = 0;
            foreach (KeyValuePair<int, int> pair in suitEffectDic)
            {
                SuitEquipNumEffectDataWrapper dataWrapper = new SuitEquipNumEffectDataWrapper(pair);
                dataWrapper.IsShowEquipedBodyName = false;
                dataWrapper.SuitEffectId = suitEffectId;
                if (pair.Key <= suitEffectEquipedNum)
                {
                    dataWrapper.IsMeetEquipedNum = true;
                }
                else
                {
                    dataWrapper.IsMeetEquipedNum = false;
                }
                result.Add(dataWrapper);

                ////找到将要显示已装备的那条数据
                if (pair.Key > meetMaxKey && suitEffectEquipedNum >= pair.Key)
                {
                    if (showNameDataWrapper != null)
                    {
                        showNameDataWrapper.IsShowEquipedBodyName = false;
                    }
                    meetMaxKey = pair.Key;
                    showNameDataWrapper = dataWrapper;
                }
            }
            if (showNameDataWrapper != null)
            {
                showNameDataWrapper.IsShowEquipedBodyName = true;
            }

            return result;
        }

        private void RecalculateListLayout()
        {
            RectTransform rect = _effectList.GetComponent<RectTransform>();
            float height = 0;
            float y = 0;
            for (int i = 0; i < _effectList.ItemList.Count; i++)
            {
                RectTransform childRect = _effectList[i].GetComponent<RectTransform>();
                if (childRect.gameObject.activeSelf == false)
                {
                    continue;
                }
                childRect.anchoredPosition = new Vector2(childRect.anchoredPosition.x, y);
                float childHeight = childRect.sizeDelta.y;
                height += childHeight;
                y -= childHeight;
            }
            rect.sizeDelta = new Vector2(rect.sizeDelta.x, height);
        }

    }

    public class SuitEffectDataWrapper
    {
        public int SuitEffectId;
        public int SuitId;
        public int GridPosition;

        public SuitEffectDataWrapper(int suitEffectId)
        {
            this.SuitEffectId = suitEffectId;
        }
    }
}
