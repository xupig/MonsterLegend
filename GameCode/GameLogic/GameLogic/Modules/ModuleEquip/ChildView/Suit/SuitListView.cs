﻿using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class SuitListView:KContainer
    {
        private StateText _txtNoneSuitLabel;
        private StateText _txtTitleLabel;
        private KScrollView _suitListScrollView;
        private KList _suitKList;
        private RectTransform _contentRect;
        private SuitDetailView _suitDetailView;
        private Locater _attriContentLocater;

        private EquipItemInfo _currentEquipInfo;

        protected override void Awake()
        {
            base.Awake();
            _txtNoneSuitLabel = GetChildComponent<StateText>("Label_txtwutaozhuang");
            _txtTitleLabel = GetChildComponent<StateText>("Container_biaoti/Label_txtBiaoti");
            _suitListScrollView = GetChildComponent<KScrollView>("ScrollView_taozhuangxiaoguo");
            _suitKList = GetChildComponent<KList>("ScrollView_taozhuangxiaoguo/mask/content");
            _contentRect = GetChildComponent<RectTransform>("ScrollView_taozhuangxiaoguo/mask/content");
            _attriContentLocater = AddChildComponent<Locater>("ScrollView_taozhuangxiaoguo/mask/content/content");
            _suitDetailView = AddChildComponent<SuitDetailView>("ScrollView_taozhuangxiaoguo/mask/content/content");
            _suitKList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        public void Show(EquipItemInfo equipItemInfo)
        {
            _currentEquipInfo = equipItemInfo;
            List<int> suitList = equip_suit_scroll_helper.GetSuitScrollByEquip(equipItemInfo);
            _suitKList.SetDataList<SuitInfoItem>(suitList);
            _suitDetailView.Visible = false;
            _txtNoneSuitLabel.Visible = suitList.Count == 0;
            _txtNoneSuitLabel.CurrentText.text = MogoLanguageUtil.GetContent(108012);
            if (_currentEquipInfo.SuitId != 0)
            {
                _txtTitleLabel.CurrentText.text = MogoLanguageUtil.GetContent(6052029);
            }
            else if (suitList.Count != 0)
            {
                _txtTitleLabel.CurrentText.text = MogoLanguageUtil.GetContent(108010);
            }
            else
            {
                _txtTitleLabel.CurrentText.text = MogoLanguageUtil.GetContent(108011);
            }
            if (suitList.Count != 0)
            {
                ChangeItemSelectedState(_suitKList[0], true);
            }
        }

        private void AddEventListener()
        {
            _suitKList.onItemClicked.AddListener(OnSuitItemClick);
            _suitDetailView.onCloseDetail.AddListener(OnCloseDetailView);
        }

        private void RemoveEventListener()
        {
            _suitKList.onItemClicked.RemoveListener(OnSuitItemClick);
            _suitDetailView.onCloseDetail.RemoveListener(OnCloseDetailView);
        }

        private void OnSuitItemClick(KList list, KList.KListItemBase item)
        {
            ChangeItemSelectedState(item, !item.IsSelected);
        }

        private void ChangeItemSelectedState(KList.KListItemBase item, bool state)
        {
            item.IsSelected = state;
            _suitDetailView.Visible = state;
            if (state == true)
            {
                _suitKList.SelectedIndex = item.Index;
                _suitDetailView.Show((int)item.Data, _currentEquipInfo);
                DoLayout(item.Index);
            }
            else
            {
                _suitKList.SelectedIndex = -1;
                DoLayout(-1);
            }
        }

        private void OnCloseDetailView()
        {
            ChangeItemSelectedState(_suitKList[_suitKList.SelectedIndex], false);
        }

        private void DoLayout(int contentInsertIndex)
        {
            Locater locater;
            float Y = (_suitKList.ItemList[0] as SuitInfoItem).Locater.Y;
            float height = 0;
            for (int i = 0; i < _suitKList.ItemList.Count; i++)
            {
                locater = (_suitKList.ItemList[i] as SuitInfoItem).Locater;
                if (locater.gameObject.activeSelf == true)
                {
                    locater.Y = Y;
                    Y -= locater.Height;
                    height += locater.Height;
                }
                if (i == contentInsertIndex)
                {
                    _attriContentLocater.Y = Y + 50;
                    Y -= _attriContentLocater.Height - 50 + 15;
                    height += _attriContentLocater.Height - 50 + 15;
                }
            }
            _contentRect.sizeDelta = new Vector2(_contentRect.sizeDelta.x, height);
        }

    }
}
