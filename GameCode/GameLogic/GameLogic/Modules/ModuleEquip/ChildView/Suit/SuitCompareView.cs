﻿
using Common.ExtendComponent;
using Common.Global;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class SuitCompareView:KContainer
    {
        private SuitEffectLeftToolView _leftEffectToolView;
        private SuitEffectRightToolView _rightEffectToolView;

        private KButton _replaceButton;
        private KButton _cancelButton;
        private IconContainer _iconContainer;
        private StateText _txtReplacePrice;

        private SuitEffectDataWrapper _replaceDataWrapper;

        public KComponentEvent onCloseView = new KComponentEvent();

        protected override void Awake()
        {
            base.Awake();
            _leftEffectToolView = AddChildComponent<SuitEffectLeftToolView>("Container_left");
            _rightEffectToolView = AddChildComponent<SuitEffectRightToolView>("Container_right");
            _iconContainer = AddChildComponent<IconContainer>("Button_tihuan/icon");

            _replaceButton = GetChildComponent<KButton>("Button_tihuan");
            _cancelButton = GetChildComponent<KButton>("Button_quxiao");
            _txtReplacePrice = _replaceButton.GetComponentInChildren<StateText>();

        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventsListener();
        }

        protected override void OnDisable()
        {
            RemoveEventsListener();
            base.OnDisable();
        }

        public void Show(SuitEffectDataWrapper current, SuitEffectDataWrapper replace)
        {
            _replaceDataWrapper = replace;
            _leftEffectToolView.Show(current);
            _rightEffectToolView.Show(replace);
            _iconContainer.SetIcon(item_helper.GetIcon(GlobalParams.GetSuitReplaceCostMoneyType()));
            _txtReplacePrice.ChangeAllStateText(string.Format("x{0}", GlobalParams.GetSuitReplaceCost().ToString()));

        }

        private void AddEventsListener()
        {
            _cancelButton.onClick.AddListener(OnCancelButtonClick);
            _replaceButton.onClick.AddListener(OnReplaceButtonClick);
        }

        private void RemoveEventsListener()
        {
            _cancelButton.onClick.RemoveListener(OnCancelButtonClick);
            _replaceButton.onClick.RemoveListener(OnReplaceButtonClick);
        }

        private void OnCancelButtonClick()
        {
            onCloseView.Invoke();
        }

        private void OnReplaceButtonClick()
        {
            PlayerAvatar.Player.RpcCall("suit_req", _replaceDataWrapper.SuitId, _replaceDataWrapper.GridPosition);
            onCloseView.Invoke();
        }

        
    }
}
