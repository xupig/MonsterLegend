﻿using Common.Base;
using Common.Events;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class SuitDetailView : KContainer
    {
        private KList _effectList;
        private StateText _txtOpenBody;
        private KButton _makeButton;
        private KButton _buyButton;
        private KButton _useButton;
        private KButton _replaceButton;
        private KButton _upCloseButton;

        private int _suitId;
        private EquipItemInfo _equipItemInfo;

        public KComponentEvent onCloseDetail = new KComponentEvent();

        protected override void Awake()
        {
            base.Awake();
            _effectList = GetChildComponent<KList>("List_xiaoguo");
            _txtOpenBody = GetChildComponent<StateText>("Label_txtKaiqibuwei");

            _makeButton = GetChildComponent<KButton>("Button_zhizao");
            _buyButton = GetChildComponent<KButton>("Button_qiugou");
            _useButton = GetChildComponent<KButton>("Button_shiyong");
            _replaceButton = GetChildComponent<KButton>("Button_tihuan");
            _upCloseButton = GetChildComponent<KButton>("Button_shousuoup");

            _effectList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        public void Show(int suitId, EquipItemInfo equipItemInfo)
        {
            _suitId = suitId;
            _equipItemInfo = equipItemInfo;
            int suitEffectId = equip_suit_scroll_helper.GetSuitId(suitId);
            ShowEffectList(suitEffectId);
            ShowEquipedBodyName(suitEffectId);
            RefreshButtonVisible();
        }

        private void AddEventListener()
        {
            if (_useButton != null)
            {
                _useButton.onClick.AddListener(OnUseButtonClick);
            }
            if (_replaceButton != null)
            {
                _replaceButton.onClick.AddListener(OnReplaceButtonClick);
            }
            _makeButton.onClick.AddListener(OnMakeButtonClick);
            _buyButton.onClick.AddListener(OnBuyButtonClick);
            _upCloseButton.onClick.AddListener(OnUpCloseButton);
        }

        private void RemoveEventListener()
        {
            if (_useButton != null)
            {
                _useButton.onClick.RemoveListener(OnUseButtonClick);
            }
            if (_replaceButton != null)
            {
                _replaceButton.onClick.RemoveListener(OnReplaceButtonClick);
            }
            _makeButton.onClick.RemoveListener(OnMakeButtonClick);
            _buyButton.onClick.RemoveListener(OnBuyButtonClick);
            _upCloseButton.onClick.RemoveListener(OnUpCloseButton);
        }

        private void ShowEffectList(int suitEffectId)
        {
            List<SuitEquipNumEffectDataWrapper> dataWrapperList = GetPackageSuitEffectData(suitEffectId);
            _effectList.RemoveAll();
            _effectList.SetDataList<SuitEquipNumEffectItem>(dataWrapperList);
        }

        private void ShowEquipedBodyName(int suitEffectId)
        {
            List<int> equipedBodyList = equip_suit_helper.GetEquipedSuitPartList(suitEffectId);

            //0, 无
            string content = MogoLanguageUtil.GetContent(0);
            for (int i = 0; i < equipedBodyList.Count; i++)
            {
                if (i == 0)
                {
                    content = item_helper.GetEquipPositionDesc(equipedBodyList[i]);
                }
                else
                {
                    content += "、" + item_helper.GetEquipPositionDesc(equipedBodyList[i]);
                }
            }
            _txtOpenBody.CurrentText.text = content;
        }

        private void RefreshButtonVisible()
        {
            int itemNum = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(_suitId);
            _makeButton.Visible = itemNum == 0;
            _buyButton.Visible = itemNum == 0;
            if(_useButton != null)
            {
                _useButton.Visible = itemNum != 0;
            }
            if (_replaceButton != null)
            {
                int suitEffectId = equip_suit_scroll_helper.GetSuitId(_suitId);
                _replaceButton.Visible = suitEffectId != _equipItemInfo.SuitId;
            }
        }

        private void OnMakeButtonClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.Make);
        }

        private void OnBuyButtonClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.TradeMarket);
        }

        private void OnReplaceButtonClick()
        {
            SuitEffectDataWrapper current = new SuitEffectDataWrapper(_equipItemInfo.SuitId);
            SuitEffectDataWrapper replace = new SuitEffectDataWrapper(equip_suit_scroll_helper.GetSuitId(_suitId));
            replace.GridPosition = _equipItemInfo.GridPosition;
            replace.SuitId = _suitId;

            EventDispatcher.TriggerEvent<SuitEffectDataWrapper, SuitEffectDataWrapper>(EquipEvents.REPLACE_SUIT, current, replace);
        }

        private void OnUseButtonClick()
        {
            PlayerAvatar.Player.RpcCall("suit_req", (UInt32)_suitId, (Int32)_equipItemInfo.GridPosition);
        }

        private void OnUpCloseButton()
        {
            onCloseDetail.Invoke();
        }

        private List<SuitEquipNumEffectDataWrapper> GetPackageSuitEffectData(int suitEffectId)
        {
            //key表示装备该套装效果的装备的数量，value表示带来的属性效果
            Dictionary<int, int> suitEffectDic = equip_suit_helper.GetSuitEffectDict(suitEffectId);
            int suitEffectEquipedNum = equip_suit_helper.GetEquipedSuitPartCount(suitEffectId);
            List<SuitEquipNumEffectDataWrapper> result = new List<SuitEquipNumEffectDataWrapper>();
            foreach (KeyValuePair<int, int> pair in suitEffectDic)
            {
                SuitEquipNumEffectDataWrapper dateWrapper = new SuitEquipNumEffectDataWrapper(pair);
                dateWrapper.IsShowEquipedBodyName = false;
                dateWrapper.SuitEffectId = suitEffectId;
                if (pair.Key <= suitEffectEquipedNum)
                {
                    dateWrapper.IsMeetEquipedNum = true;
                }
                else
                {
                    dateWrapper.IsMeetEquipedNum = false;
                }
                result.Add(dateWrapper);
            }

            return result;
        }

    }
}
