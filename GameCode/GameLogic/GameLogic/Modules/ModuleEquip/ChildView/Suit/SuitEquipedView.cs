﻿using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEquip
{
    public class SuitEquipedView:KContainer
    {
        private string _fightForceContent;

        private StateText _txtFightForce;
        private KButton _closeButton;
        private CurrentSuitView _currentSuitView;
        private EquipInfoView _equipInfoView;
        private SuitListView _suitListView;

        private int _bodyPosition;

        protected override void Awake()
        {
            base.Awake();
            _currentSuitView = AddChildComponent<CurrentSuitView>("Container_taozhuangleft01");
            _equipInfoView = AddChildComponent<EquipInfoView>("Container_taozhuangright/Container_wupinitem");
            _suitListView = AddChildComponent<SuitListView>("Container_taozhuangleft02");
            _closeButton = GetChildComponent<KButton>("Container_taozhuangleft02/Container_biaoti/Button_fanhui");
            _txtFightForce = GetChildComponent<StateText>("Container_taozhuangright/Label_txtZhanli");
            _fightForceContent = _txtFightForce.CurrentText.text;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        public void Show(int bodyPosition)
        {
            _bodyPosition = bodyPosition;
            this.Visible = true;
            _suitListView.Visible = false;
            EquipItemInfo suitEquipItem = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo(bodyPosition) as EquipItemInfo;
            _currentSuitView.Show(suitEquipItem.SuitId);
            _equipInfoView.ShowEquip(suitEquipItem);
            _txtFightForce.CurrentText.text = string.Format(_fightForceContent, suitEquipItem.FightForce);
        }

        private void AddEventListener()
        {
            _closeButton.onClick.AddListener(OnCloseReplaceView);
            _currentSuitView.onReplaceButtonClick.AddListener(OnCloseCurrentSuitView);
        }

        private void RemoveEventListener()
        {
            _closeButton.onClick.RemoveListener(OnCloseReplaceView);
            _currentSuitView.onReplaceButtonClick.RemoveListener(OnCloseCurrentSuitView);
        }

        private void OnCloseReplaceView()
        {
            EquipItemInfo suitEquipItem = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo(_bodyPosition) as EquipItemInfo;

            _suitListView.Visible = false;
            _currentSuitView.Visible = true;
            _currentSuitView.Show(suitEquipItem.SuitId);
        }

        private void OnCloseCurrentSuitView()
        {
            EquipItemInfo suitEquipItem = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo(_bodyPosition) as EquipItemInfo;
            _currentSuitView.Visible = false;
            _suitListView.Visible = true;

            _suitListView.Show(suitEquipItem);
        }
    }
}
