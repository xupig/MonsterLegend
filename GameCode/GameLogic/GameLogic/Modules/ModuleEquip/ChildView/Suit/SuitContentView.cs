﻿using Common.Structs;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class SuitContentView:KContainer
    {
        private string _fightForceContent;

        private StateText _txtFightForce;
        private EquipInfoView _equipInfoView;
        private SuitListView _suitListView;

        protected override void Awake()
        {
            base.Awake();
            _equipInfoView = AddChildComponent<EquipInfoView>("Container_taozhuangright/Container_wupinitem");
            _suitListView = AddChildComponent<SuitListView>("Container_taozhuangleft");
            _txtFightForce = GetChildComponent<StateText>("Container_taozhuangright/Label_txtZhanli");

            _fightForceContent = _txtFightForce.CurrentText.text;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        public void Show(int bodyPosition)
        {
            this.Visible = true;
            EquipItemInfo enchantEquipItem = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo(bodyPosition) as EquipItemInfo;
            _equipInfoView.ShowEquip(enchantEquipItem);
            _suitListView.Show(enchantEquipItem);
            _txtFightForce.CurrentText.text = string.Format(_fightForceContent, enchantEquipItem.FightForce);
        }
    }
}
