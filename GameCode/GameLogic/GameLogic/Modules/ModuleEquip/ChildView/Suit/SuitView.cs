﻿using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEquip
{
    public class SuitView:KContainer
    {
        private List<int> _haveEquipPosList;

        private CategoryList _categoryToggleGroup;

        private SuitContentView _suitContentView;
        private SuitCompareView _suitCompareView;
        private SuitEquipedView _suitEquipedView;

        protected override void Awake()
        {
            base.Awake();
            _haveEquipPosList = new List<int>();
            _categoryToggleGroup = GetChildComponent<CategoryList>("List_categoryList");
            _suitContentView = AddChildComponent<SuitContentView>("Container_taozhuang");
            _suitCompareView = AddChildComponent<SuitCompareView>("Container_huanzhuangtanchuang");
            _suitEquipedView = AddChildComponent<SuitEquipedView>("Container_genghuantaozhuang");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            RefreshSuitCategory();
            DecideDefaultShowPosition();
            RefreshGreenPoint();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void RefreshSuitCategory()
        {
            Dictionary<int, BaseItemInfo> equipItemInfoDic = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetAllItemInfo();
            List<CategoryItemData> listToggleData = new List<CategoryItemData>();

            _haveEquipPosList = equipItemInfoDic.Keys.ToList();
            _haveEquipPosList.Sort();
            int count = _haveEquipPosList.Count;
            for (int i = 0; i < count; i++)
            {
                listToggleData.Add(CategoryItemData.GetCategoryItemData(item_helper.GetEquipPositionDesc(_haveEquipPosList[i]), item_helper.GetEquipPositionIcon(_haveEquipPosList[i])));
            }
            _categoryToggleGroup.RemoveAll();
            if (count > 0)
            {
                _categoryToggleGroup.SetDataList<CategoryListItem>(listToggleData);
            }
        }

        private void AddEventListener()
        {
            _categoryToggleGroup.onSelectedIndexChanged.AddListener(OnBodySelectedChanged);
            _suitCompareView.onCloseView.AddListener(OnCloseCompareView);
            EventDispatcher.AddEventListener<SuitEffectDataWrapper, SuitEffectDataWrapper>(EquipEvents.REPLACE_SUIT, ShowCompareView);
            EventDispatcher.AddEventListener(EquipSuitScrollEvents.Refresh, OnRefreshSuitInfo);
            EventDispatcher.AddEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshGreenPoint);
        }

        private void RemoveEventListener()
        {
            _categoryToggleGroup.onSelectedIndexChanged.RemoveListener(OnBodySelectedChanged);
            _suitCompareView.onCloseView.RemoveListener(OnCloseCompareView);
            EventDispatcher.RemoveEventListener<SuitEffectDataWrapper, SuitEffectDataWrapper>(EquipEvents.REPLACE_SUIT, ShowCompareView);
            EventDispatcher.RemoveEventListener(EquipSuitScrollEvents.Refresh, OnRefreshSuitInfo);
            EventDispatcher.RemoveEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshGreenPoint);
        }

        private void RefreshGreenPoint()
        {
            int count = _haveEquipPosList.Count;
            HashSet<int> showGreenPointSet = PlayerDataManager.Instance.EquipPointManager.SuitPointSet;
            for (int i = 0; i < count; i++)
            {
                (_categoryToggleGroup[i] as CategoryListItem).SetPoint(showGreenPointSet.Contains(_haveEquipPosList[i]));
            }
        }

        private void ShowCompareView(SuitEffectDataWrapper current, SuitEffectDataWrapper replace)
        {
            _suitEquipedView.Visible = false;
            _suitContentView.Visible = false;
            _suitCompareView.Visible = true;
            _suitCompareView.Show(current, replace);
        }

        private void OnCloseCompareView()
        {
            _suitEquipedView.Visible = true;
            _suitContentView.Visible = false;
            _suitCompareView.Visible = false;
        }

        private void OnRefreshSuitInfo()
        {
            ShowSuitInfo(_haveEquipPosList[_categoryToggleGroup.SelectedIndex]);
        }

        private void OnBodySelectedChanged(CategoryList toggleGroup, int index)
        {
            ShowSuitInfo(_haveEquipPosList[index]);
        }

        private void ShowSuitInfo(int bodyPosition)
        {
            EquipItemInfo suitEquipItem = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo(bodyPosition) as EquipItemInfo;
            //当前装备未使用套装卷轴
            if (suitEquipItem.SuitId == 0)
            {
                _suitCompareView.Visible = false;
                _suitEquipedView.Visible = false;
                _suitContentView.Show(bodyPosition);
            }
            else
            {
                _suitCompareView.Visible = false;
                _suitContentView.Visible = false;
                _suitEquipedView.Show(bodyPosition);
            }
        }

        private void DecideDefaultShowPosition()
        {
            int bodyPosition = _haveEquipPosList[0];
            int selectedIndex = 0;

            List<BaseItemInfo> equipList = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetAllItemInfo().Values.ToList();
            for(int i = 0; i < equipList.Count; i++)
            {
                EquipItemInfo equipItemInfo = equipList[i] as EquipItemInfo;
                List<int> suitList = equip_suit_scroll_helper.GetSuitScrollByEquip(equipItemInfo);
                if (equipItemInfo.SuitId == 0 && suitList.Count != 0)
                {
                    bodyPosition = equipItemInfo.GridPosition;
                    selectedIndex = _haveEquipPosList.IndexOf(bodyPosition);
                    break;
                }
            }
            _categoryToggleGroup.SelectedIndex = selectedIndex;
            ShowSuitInfo(bodyPosition);
        }
    }
}
