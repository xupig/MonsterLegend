﻿using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class CurrentSuitView:KContainer
    {
        private KList _effectList;
        private KButton _replaceSuitButton;
        private StateText _txtFightForce;
        private StateText _txtSuitName;
        private string _suitNumContent;

        public KComponentEvent onReplaceButtonClick = new KComponentEvent();

        protected override void Awake()
        {
            base.Awake();
            _txtSuitName = GetChildComponent<StateText>("Label_txtTaozhuangmingcheng");
            _suitNumContent = _txtSuitName.CurrentText.text;
            _txtFightForce = GetChildComponent<StateText>("Label_txtZhanli");
            _replaceSuitButton = GetChildComponent<KButton>("Button_genghuan");
            _effectList = GetChildComponent<KList>("List_xiaoguo");
            _effectList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _effectList.SetGap(10,0);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

        public void Show(int suitEffectId)
        {
            this.Visible = true;
            int equipedBodyCount = equip_suit_helper.GetEquipedSuitPartCount(suitEffectId);
            int fightForce = equip_suit_helper.GetFightForce(suitEffectId, equipedBodyCount);

            _txtSuitName.CurrentText.text = string.Format(_suitNumContent, equip_suit_helper.GetName(suitEffectId), equip_suit_helper.GetEquipedSuitPartCount(suitEffectId), equip_suit_helper.GetSuitPartCount(suitEffectId));

            //108013,当前套装战力{0}
            _txtFightForce.CurrentText.text = MogoLanguageUtil.GetContent(108013, fightForce);

            ShowEffectList(suitEffectId);
        }

        private void ShowEffectList(int suitEffectId)
        {
            List<SuitEquipNumEffectDataWrapper> dataWrapperList = GetPackageSuitEffectData(suitEffectId);
            _effectList.RemoveAll();
            _effectList.SetDataList<SuitEquipNumEffectItem>(dataWrapperList);
            RecalculateListLayout();
        }

        private void AddEventListener()
        {
            _replaceSuitButton.onClick.AddListener(OnReplaceButtonClick);
        }

        private void RemoveEventListener()
        {
            _replaceSuitButton.onClick.RemoveListener(OnReplaceButtonClick);
        }

        private void OnReplaceButtonClick()
        {
            onReplaceButtonClick.Invoke();
        }

        private List<SuitEquipNumEffectDataWrapper> GetPackageSuitEffectData(int suitEffectId)
        {
            //key表示装备该套装效果的装备的数量，value表示带来的属性效果
            Dictionary<int, int> suitEffectDic = equip_suit_helper.GetSuitEffectDict(suitEffectId);
            int suitEffectEquipedNum = equip_suit_helper.GetEquipedSuitPartCount(suitEffectId);
            List<SuitEquipNumEffectDataWrapper> result = new List<SuitEquipNumEffectDataWrapper>();
            //记录将要显示已装备的那条数据
            SuitEquipNumEffectDataWrapper showNameDataWrapper = null;
            int meetMaxKey = 0;
            foreach (KeyValuePair<int, int> pair in suitEffectDic)
            {
                SuitEquipNumEffectDataWrapper dataWrapper = new SuitEquipNumEffectDataWrapper(pair);        
                dataWrapper.IsShowEquipedBodyName = false;
                dataWrapper.SuitEffectId = suitEffectId;
                if (pair.Key <= suitEffectEquipedNum)
                {
                    dataWrapper.IsMeetEquipedNum = true;
                }
                else
                {
                    dataWrapper.IsMeetEquipedNum = false;
                }
                result.Add(dataWrapper);

                ////找到将要显示已装备的那条数据
                if (pair.Key > meetMaxKey && suitEffectEquipedNum >= pair.Key)
                {
                    if (showNameDataWrapper != null)
                    {
                        showNameDataWrapper.IsShowEquipedBodyName = false;
                    }
                    meetMaxKey = pair.Key;
                    showNameDataWrapper = dataWrapper;
                }
            }
            if (showNameDataWrapper != null)
            {
                showNameDataWrapper.IsShowEquipedBodyName = true;
            }

            return result;
        }

        private void RecalculateListLayout()
        {
            RectTransform rect = _effectList.GetComponent<RectTransform>();
            float height = 0;
            float y = 0;
            for (int i = 0; i < _effectList.ItemList.Count; i++)
            {
                RectTransform childRect = _effectList[i].GetComponent<RectTransform>();
                if (childRect.gameObject.activeSelf == false)
                {
                    continue;
                }
                childRect.anchoredPosition = new Vector2(childRect.anchoredPosition.x, y);
                float childHeight = childRect.sizeDelta.y;
                height += childHeight;
                y -= childHeight;
            }
            rect.sizeDelta = new Vector2(rect.sizeDelta.x, height);
        }
    }
}
