﻿using Common.Data;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEquip
{
    public class LuckyStoneData:ItemData
    {
        private Dictionary<int, int> _luckyStoneAttriDic;

        public LuckyStoneData(int id)
            : base(id)
        {
            InitConfig();
        }

        /// <summary>
        /// 幸运石增加的成功率
        /// </summary>
        public int LuckyStoneRate
        {
            
            get { return _luckyStoneAttriDic[Id]; }
        }

        private void InitConfig()
        {
            string _luckyStoneValue = XMLManager.global_params[25].__value;
            _luckyStoneAttriDic = MogoStringUtils.Convert2Dic(_luckyStoneValue);
        }
    }
}
