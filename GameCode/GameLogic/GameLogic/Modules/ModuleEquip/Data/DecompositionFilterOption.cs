﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEquip
{
    /// <summary>
    /// 用于装备分解界面的筛选
    /// </summary>
    public class DecompositionFilterOption
    {
        /// <summary>
        /// 装备品质，0表示全部。
        /// </summary>
        public int EquipQuality;

        /// <summary>
        /// 选择的装备的类型
        /// </summary>
        public DecompositionEquipType EquipClassify;

        public DecompositionFilterOption(int equipQuality = 0, DecompositionEquipType equipClassify = DecompositionEquipType.UselessEquip)
        {
            EquipQuality = equipQuality;
            EquipClassify = equipClassify;
        }

    }

    public enum DecompositionEquipType
    {
        UselessEquip = 0, //垃圾装备

        AllEquip = 1, //全部装备
    }
}
