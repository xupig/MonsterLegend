﻿using Common.Structs;
using Common.Structs.ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEquip
{
    public class EnchantAttriComparisonData
    {
        public EnchantResult EnchantResult;

        public EquipItemInfo EquipItemInfo;

        public PbEquipEnchantAttriInfo CurrentEnchantAttri;

        public PbEquipEnchantAttriInfo AfterEnchantAttri;
    }


    public enum EnchantResult
    {
        /// <summary>
        /// 放弃
        /// </summary>
        Abandon,

        /// <summary>
        /// 替换
        /// </summary>
        Replace,

        /// <summary>
        /// 结果展示
        /// </summary>
        ResultShow,
    }
}
