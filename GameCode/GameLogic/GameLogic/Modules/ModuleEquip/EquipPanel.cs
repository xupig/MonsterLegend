﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class EquipPanel : BasePanel
    {
        public static int ShowingTab = -1;
        private const int STRENGTHEN = 0;
        private const int ENCHANT = 1;
        private const int SUIT = 2;

        private StrengthenView _strengthenView;
        private EnchantView _enchantView;
        private SuitView _suitView;

        private KToggleGroup _toggleGroup;

        private KToggle _strengthenToggle;
        private KToggle _enchantToggle;
        private KToggle _suitToggle;

        private List<int> toggleList = new List<int>();

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            _strengthenToggle = GetChildComponent<KToggle>("ToggleGroup_biaoti/Toggle_qianghua");
            _enchantToggle = GetChildComponent<KToggle>("ToggleGroup_biaoti/Toggle_fumo");
            _suitToggle = GetChildComponent<KToggle>("ToggleGroup_biaoti/Toggle_taozhuang");
            _strengthenView = AddChildComponent<StrengthenView>("Container_qianghua");
            _enchantView = AddChildComponent<EnchantView>("Container_fumo");
            _suitView = AddChildComponent<SuitView>("Container_taozhuang");


            toggleList.Add((int)FunctionId.equip);
            toggleList.Add((int)FunctionId.enchant);
            toggleList.Add((int)FunctionId.suit);
            SetTabList(toggleList, _toggleGroup);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Equip; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
            RefreshPoint();
        }

        public override void OnClose()
        {
            RemoveEventListener();
            ShowingTab = -1;
        }

        public override void SetData(object data)
        {
            if (data != null)
            {
                if (data is EquipPanelOption)
                {
                    EquipPanelOption equipPanelData = data as EquipPanelOption;
                    ShowToggle(equipPanelData.EquipOperateType, equipPanelData.BodyPosition);
                }
                else if (data is int[])
                {
                    int[] list = data as int[];
                    ShowToggle(list[0] - 1, list[1]);
                }
                else
                {
                    ShowToggle((int)data - 1);
                }

                return;
            }

            ShowToggle(STRENGTHEN);
        }

        private void AddEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            EventDispatcher.AddEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshPoint);
        }

        private void RemoveEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            EventDispatcher.RemoveEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshPoint);
        }

        private void RefreshPoint()
        {
            _strengthenToggle.SetGreenPoint(PlayerDataManager.Instance.EquipPointManager.StrengthenPointSet.Count != 0);
            _enchantToggle.SetGreenPoint(PlayerDataManager.Instance.EquipPointManager.EnchantPointSet.Count != 0);
            _suitToggle.SetGreenPoint(PlayerDataManager.Instance.EquipPointManager.SuitPointSet.Count != 0);
        }

        private void OnSelectedIndexChanged(KToggleGroup toggleGroup, int index)
        {
            ShowToggle(index);
        }

        private void ShowToggle(int toggleIndex, object data = null)
        {
            _toggleGroup.SelectIndex = toggleIndex;
            switch (toggleIndex)
            {
                case STRENGTHEN:
                    ShowStrengthenView(data);
                    break;
                case ENCHANT:
                    ShowEnchantView(data);
                    break;
                case SUIT:
                    ShowSuitView(data);
                    break;
                default:
                    break;
            }
        }

        private void ShowStrengthenView(object data)
        {
            _strengthenView.Visible = true;
            _suitView.Visible = false;
            _enchantView.Visible = false;

            if (PlayerDataManager.Instance.EquipPointManager.IsHintStrengthenEntry() == true)
            {
                PlayerDataManager.Instance.EquipPointManager.IncreaseStrengthenPointNeedStrengthenTimes();
            }
            _strengthenView.SetData(data);
            ShowingTab = STRENGTHEN;
        }

        private void ShowEnchantView(object data)
        {
            _strengthenView.Visible = false;
            _suitView.Visible = false;
            _enchantView.Visible = true;

            _enchantView.SetData(data);
            ShowingTab = ENCHANT;
        }

        private void ShowSuitView(object data)
        {
            _strengthenView.Visible = false;
            _enchantView.Visible = false;

            _suitView.Visible = true;
            ShowingTab = SUIT;
        }
    }
}
