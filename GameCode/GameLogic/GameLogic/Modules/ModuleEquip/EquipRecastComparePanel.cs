﻿
using Common.Base;
using Common.Chat;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class EquipRecastComparePanel:BasePanel
    {
        private CurrentRecastPropertyView _currentPropertyView;
        private CurrentRecastPropertyView _afterPropertyView;

        private KButton _okButton;
        private KButton _cancelButton;

        private StateText _txtFightForce;
        private StateImage _imgFightForceUp;
        private StateImage _imgFightForceDown;

        private IconContainer _iconContainer;
        private KDummyButton _materialButton;
        private StateText _txtMaterailNum;

        private RecastCompareWrapper _wrapper;

        protected override void Awake()
        {
            base.Awake();
            _okButton = GetChildComponent<KButton>("Container_shuxingtanchuang/Button_tihuan");
            _cancelButton = GetChildComponent<KButton>("Container_shuxingtanchuang/Button_quxiao");
            _currentPropertyView = AddChildComponent<CurrentRecastPropertyView>("Container_shuxingtanchuang/Container_left/Container_xiyoushuxing");
            _afterPropertyView = AddChildComponent<CurrentRecastPropertyView>("Container_shuxingtanchuang/Container_right/Container_xiyoushuxing");

            _txtFightForce = GetChildComponent<StateText>("Container_shuxingtanchuang/Container_tisheng/Label_txttip");
            _imgFightForceUp = GetChildComponent<StateImage>("Container_shuxingtanchuang/Container_tisheng/Image_tisheng");
            _imgFightForceDown = GetChildComponent<StateImage>("Container_shuxingtanchuang/Container_tisheng/Image_xiajiang");

            _iconContainer = AddChildComponent<IconContainer>("Container_shuxingtanchuang/Container_xiaohao/Container_icon");
            _materialButton = _iconContainer.gameObject.AddComponent<KDummyButton>();
            _txtMaterailNum = GetChildComponent<StateText>("Container_shuxingtanchuang/Container_xiaohao/Label_txtShuliang");

            _okButton.onClick.AddListener(OnOkButtonClick);
            _cancelButton.onClick.AddListener(OnCancelButtonClick);
            _materialButton.onClick.AddListener(ShowMaterailTip);
        }

        protected override void OnDestroy()
        {
            _okButton.onClick.RemoveListener(OnOkButtonClick);
            base.OnDestroy();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.RecastCompare; }
        }

        public override void OnShow(object data)
        {
            RecastCompareWrapper wrapper = data as RecastCompareWrapper;
            _wrapper = wrapper;
            _currentPropertyView.Refresh(wrapper.CurrentEquipInfo);
            _afterPropertyView.Refresh(wrapper.AfterEquipInfo);
            RefreshCancelMaterial(wrapper.CurrentEquipInfo);
            RefreshFightForceChange(wrapper.AfterEquipInfo.FightForce - wrapper.CurrentEquipInfo.FightForce);
        }

        public override void OnClose()
        {

        }

        private void OnOkButtonClick()
        {
            EquipManager.Instance.ConfirmRecastResult(1);
            ClosePanel();
        }

        private void OnCancelButtonClick()
        {
            BaseItemData recastCancelItem = equip_recast_helper.GetRecastCancelItemCost(_wrapper.CurrentEquipInfo);
            List<BaseItemData> recastCancelItemDataList = equip_recast_helper.GetRecastCancelCost(_wrapper.CurrentEquipInfo);
            for (int i = 0; i < recastCancelItemDataList.Count; i++)
            {
                BaseItemData itemData = recastCancelItemDataList[i];
                if (itemData.Id == public_config.MONEY_TYPE_BIND_DIAMOND)
                {

                    int bagNum = item_helper.GetMyMoneyCountByItemId(recastCancelItemDataList[i].Id);
                    if (bagNum < recastCancelItemDataList[i].StackCount)
                    {
                        MogoTipsUtils.ShowTips(itemData, MogoLanguageUtil.GetContent(114042), OnConfirm);
                        return;
                    }
                    else
                    {
                        string content = MogoLanguageUtil.GetContent(114042, string.Concat(recastCancelItemDataList[i].StackCount, recastCancelItemDataList[i].Name));
                        MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, OnConfirm, OnCancel, true, false, true);
                        return;
                    }
                }
            }
            OnConfirm();
        }

        private void OnConfirm()
        {
            EquipManager.Instance.ConfirmRecastResult(0);
            ClosePanel();
        }

        private void OnCancel()
        {

        }

        private void ShowMaterailTip()
        {
            BaseItemData recastCancelItemData = equip_recast_helper.GetRecastCancelItemCost(_wrapper.CurrentEquipInfo);
            ToolTipsManager.Instance.ShowItemTip(recastCancelItemData.Id, PanelIdEnum.EquipSmelter);
        }

        private void RefreshFightForceChange(int fightForceChange)
        {
            if (fightForceChange >= 0)
            {
                _txtFightForce.CurrentText.text = MogoLanguageUtil.GetContent(114040, fightForceChange);
            }
            else
            {
                _txtFightForce.CurrentText.text = MogoLanguageUtil.GetContent(114041, -fightForceChange);
            }
            _imgFightForceUp.Visible = fightForceChange > 0;
            _imgFightForceDown.Visible = fightForceChange < 0;
        }

        private void RefreshCancelMaterial(EquipItemInfo item)
        {
            BaseItemData recastCancelItemData = equip_recast_helper.GetRecastCancelItemCost(item);
            _iconContainer.SetIcon(item_helper.GetIcon(recastCancelItemData.Id));
            _txtMaterailNum.CurrentText.text = string.Format("x{0}", recastCancelItemData.StackCount);
        }
    }

    public class RecastCompareWrapper
    {
        public EquipItemInfo CurrentEquipInfo;
        public EquipItemInfo AfterEquipInfo;
    }
}
