﻿
using Common.Base;
using Common.Structs;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEquip
{
    public class EquipRecastSkillPanel:BasePanel
    {
        private RandomSkillDetailView _randomSkillDetailView;

        protected override void Awake()
        {
            base.Awake();

            CloseBtn = GetChildComponent<KButton>("Container_rongliantanchuang/Container_BG/Button_close");
            _randomSkillDetailView = AddChildComponent<RandomSkillDetailView>("Container_rongliantanchuang/ScrollView_chongzhusuijijineng/mask/content");
        }

        public override void OnShow(object data)
        {
            _randomSkillDetailView.Refresh(data as EquipItemInfo);
        }

        public override void OnClose()
        {
            ;
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.RecastSkill; }
        }
    }
}
