﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace ModuleEquip
{
    public class MaterialItem : KContainer
    {
        private IconContainer _iconContainer;
        private StateText _txtGold;
        private KButton _btnAdd;
        private KDummyButton _materialIconButton;

        private ItemData _data;

        public ItemData Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value;
                    Refresh();
                }
                else
                {
                    ResetData();
                }
            }
        }

        protected override void Awake()
        {
            base.Awake();
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _materialIconButton = AddChildComponent<KDummyButton>("Container_icon");
            _txtGold = GetChildComponent<StateText>("Label_txtNum");
            _btnAdd = GetChildComponent<KButton>("Button_tianjia");

            AddEventListener();
        }

        protected override void OnDestroy()
        {
            _data = null;
            RemoveEventListener();
            base.OnDestroy();
        }

        private void AddEventListener()
        {
            _materialIconButton.onClick.AddListener(ShowItemTip);

        }

        private void RemoveEventListener()
        {
            _materialIconButton.onClick.RemoveListener(ShowItemTip);

        }

        private void ResetData()
        {
            _iconContainer.Visible = false;
            _data = null;
        }

        private void Refresh()
        {
            _iconContainer.Visible = true;
            _iconContainer.SetIcon(item_helper.GetIcon(_data.Id));
            _txtGold.ChangeAllStateText(_data.StackCount.ToString());
        }

        public void SetColor(Color color)
        {
            _txtGold.CurrentText.color = color;
        }

        private void ShowItemTip()
        {
            ToolTipsManager.Instance.ShowItemTip(_data.Id, PanelIdEnum.Equip);
        }
    }
}
