﻿using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class StarWithParticle:KContainer
    {
        private const int STAR_SHOW_TIME = 400;

        private StateImage _imgStar;
        private ImageWrapper _imgStarWrapper;
        private KParticle _particle;
        private Color _originalColor;

        public KComponentEvent onPlayComplete = new KComponentEvent();

        protected override void Awake()
        {
            base.Awake();
            _imgStar = GetChildComponent<StateImage>("Image_qianghuastar");
            _imgStarWrapper = GetChildComponent<StateImage>("Image_qianghuastar").CurrentImage as ImageWrapper;
            _originalColor = _imgStarWrapper.color;
            _originalColor.a = 0.0f;
            _imgStarWrapper.color = _originalColor;
            MogoShaderUtils.ChangeShader(_imgStar.CurrentImage, MogoShaderUtils.IconShaderPath);
            _particle = AddChildComponent<KParticle>("fx_ui_9_2_star_01");
            _particle.GetComponent<ParticleSystemWrapper>().lifeTime = 0.6f;
            _particle.Stop();
            AddEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

        private void AddEventListener()
        {
            _particle.onComplete.AddListener(OnComplete);
        }

        private void RemoveEventListener()
        {
            _particle.onComplete.RemoveListener(OnComplete);
        }

        private void OnComplete()
        {
            onPlayComplete.Invoke();
        }

        public void Play(int starQuality)
        {
            _particle.Play();
            TimerHeap.AddTimer<int>((uint)STAR_SHOW_TIME, 0, ShowStarQuality, starQuality);
        }

        public void HideStarQuality()
        {
            if (_imgStarWrapper != null)
            {
                _imgStarWrapper.color = _originalColor;
            }
        }

        public void ShowStarQuality(int starQuality)
        {
            if (_imgStarWrapper != null)
            {
                Color color = ColorDefine.GetColorById(starQuality);
                _imgStarWrapper.color = color;
            }
        }
    }
}
