﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class StepIconItem:KContainer
    {
        private const float SCALE = 3.0f;
        private const float PLAY_TIME = 0.15f;

        private int _step;
        private TweenScale tween;
        public KComponentEvent<int> onPlayComplete = new KComponentEvent<int>();

        protected override void Awake()
        {
            base.Awake();
            HideAllChild();
        }

        public void Play(int step, int imageIndex)
        {
            HideAllChild();
            _step = step;
            GameObject imgGoObj = transform.GetChild(imageIndex).gameObject;
            imgGoObj.SetActive(true);
            Vector3 scale = imgGoObj.transform.localScale;
            imgGoObj.transform.localScale = new Vector3(SCALE, SCALE, SCALE);
            tween = TweenScale.Begin(imgGoObj, PLAY_TIME, scale);
            tween.method = UITweener.Method.EaseOut;
            tween.onFinished = OnScaleFinish;
            tween.Play(true);
        }

        public void ShowStepIcon(int imageIndex)
        {
            if (tween != null)
            {
                tween.enabled = false;
            }
            HideAllChild();
            GameObject imgGoObj = gameObject.transform.GetChild(imageIndex).gameObject;
            imgGoObj.transform.localScale = Vector3.one;
            imgGoObj.SetActive(true);
        }

        public void HideAllChild()
        {
            int count = transform.childCount;
            for (int i = 0; i < count; i++)
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
        }

        private void OnScaleFinish(UITweener tween)
        {
            onPlayComplete.Invoke(_step);
        }


    }
}
