﻿
using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class SmeltNeedMaterialItem:KList.KListItemBase
    {
        private StateText _needMaterailNum;
        private ItemGrid _itemIconGrid;
        private BaseItemData _data;
        private Color _originalColor;

        protected override void Awake()
        {
            _needMaterailNum = GetChildComponent<StateText>("Label_txtCost");
            _itemIconGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _originalColor = _needMaterailNum.CurrentText.color;
        }

        public override void Dispose()
        {
            _data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as BaseItemData;
                Refresh();
            }
        }

        private void Refresh()
        {
            int myItemCount = 0;
            if (item_helper.isMoneyType(_data.Id))
            {
                myItemCount = item_helper.GetMyMoneyCountByItemId(_data.Id);
            }
            else
            {
                myItemCount = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(_data.Id);        
            }
            if (myItemCount < _data.StackCount)
            {
                _needMaterailNum.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_RED);
            }
            else
            {
                _needMaterailNum.CurrentText.color = _originalColor;
            }
            _needMaterailNum.CurrentText.text = string.Format("x{0}", _data.StackCount);
            _itemIconGrid.SetItemData(_data.Id);
            _itemIconGrid.Context = PanelIdEnum.Equip;
        }
    }
}
