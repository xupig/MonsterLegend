﻿using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class SmeltBodyPolygonList:KContainer
    {
        private const float FLOAT_OFFSET = 0.1f;

        private KPageableList _list;
        private List<int> _bodyList;
        private RectTransform _bgRect;
        private KParticle _particle;

        private Vector2 _center = Vector2.zero;
        public float _radius = 0;

        public KComponentEvent onComplete = new KComponentEvent();

        protected override void Awake()
        {
            base.Awake();
            _list = GetChildComponent<KPageableList>("List_content");
            _bgRect = GetChildComponent<RectTransform>("List_content/ronglianditu");
            _particle = AddChildComponent<KParticle>("List_content/fx_ui_21_5_zhaohuan_02");
            _particle.Stop();
            _particle.transform.localPosition = new Vector3(106f, -48f, 0);
            _center = _bgRect.anchoredPosition + new Vector2(_bgRect.sizeDelta.x, -_bgRect.sizeDelta.y) / 2;
            _radius = 140f;

            _particle.onComplete.AddListener(OnParticleComplete);
        }

        public void SetData(List<int> bodyList)
        {
            _bodyList = bodyList;
            _list.SetDataList<SmeltProductBodyItem>(bodyList);
            LayoutList();
        }

        private void OnParticleComplete()
        {
            onComplete.Invoke();
        }

        bool _isPlaying = false;
        float _destAngle = 0f;
        float _uspeedTime = 0; //匀速时间
        float _playingTime = 0;
        private float _maxSpeed = 1440;
        private float _startAcceleration = 2880;
        private float _endAcceleration = 720;

        public void StartPlaying(int bodyId)
        {
            _isPlaying = true;
            _playingTime = 0f;
            _destAngle = CalculateDestAngle(bodyId);
            _uspeedTime = _destAngle / _maxSpeed - _maxSpeed / (2 *_startAcceleration) -  _maxSpeed / (2 *_endAcceleration);
        }

        void Update()
        {
            if (!_isPlaying)
            {
                return;
            }

            _playingTime += Time.deltaTime;
            float angle = GetAngleByTime(_playingTime);
            if (_bodyList.Count % 2 == 1)
            {
                angle += 180f / _bodyList.Count;
            }
            _list.SelectedIndex = (int)(angle / (360f / _bodyList.Count)) % _bodyList.Count;
            if (Math.Abs(angle - _destAngle) < FLOAT_OFFSET)
            {
                _isPlaying = false;
                _particle.Play();
            }
        }

        private float GetAngleByTime(float time)
        {
            //加速阶段
            if (time < _maxSpeed / _startAcceleration)
            {
                return 0.5f * _startAcceleration * time * time;
            }//匀速阶段
            else if (time < _maxSpeed / _startAcceleration + _uspeedTime)
            {
                return time * _maxSpeed - 0.5f * _maxSpeed * _maxSpeed / _startAcceleration;
            }//减速阶段
            else if (time < _maxSpeed / _startAcceleration + _maxSpeed / _endAcceleration + _uspeedTime)
            {
                float leftTime = _maxSpeed / _startAcceleration + _maxSpeed / _endAcceleration + _uspeedTime - time;
                return _destAngle - 0.5f * _endAcceleration * leftTime * leftTime;
            }
            else
            {
                return _destAngle;
            }
        }

        private float CalculateDestAngle(int bodyId)
        {
            int bodyIndex = _bodyList.IndexOf(bodyId);
            int bodyListCount = _bodyList.Count;
            return 360.0f / bodyListCount * bodyIndex + 180.0f / bodyListCount * ((bodyListCount + 1) % 2) + 5 * 360;
        }

        private void LayoutList()
        {
            List<Vector2> vertexList = GetPolygonVertex(_center, _radius, _bodyList.Count);
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                SmeltProductBodyItem item = _list.ItemList[i] as SmeltProductBodyItem;
                if (item.Visible)
                {
                    RectTransform rectTransform = item.GetComponent<RectTransform>();
                    rectTransform.pivot = new Vector2(0.5f, 0.5f);
                    rectTransform.anchoredPosition = vertexList[i];
                }
            }
        }

        private List<Vector2> GetPolygonVertex(Vector2 center, float radius, int edgeNum)
        {
            List<Vector2> result = new List<Vector2>();
            Vector2 vec = new Vector2(0, radius);
            if (edgeNum % 2 == 1)
            {
                result.Add(vec + center);
            }
            else
            {
                vec = Quaternion.AngleAxis(-180 / edgeNum, Vector3.forward) * vec;
                result.Add(vec + center);
            }
            for (int i = 1; i < edgeNum; i++)
            {
                vec = Quaternion.AngleAxis(-360 / edgeNum, Vector3.forward) * vec;
                result.Add(vec + center);
            }
            return result;
        }
    }
}
