﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class RisingStarEquipItem:KList.KListItemBase
    {
        private StateImage _checkmark;
        private StateText _txtEquipTypeName;
        private StateText _txtEquipLevel;

        private ItemGrid _itemIcon;
        private KParticle _particle;
        private EquipItemInfo _data;

        private MaskEffectItem _effectItem;

        protected override void Awake()
        {
            _checkmark = GetChildComponent<StateImage>("Image_xuanzhongkuang_left");
            _checkmark.AddAllStateComponent<RaycastComponent>();
            _checkmark.Visible = false;
            _txtEquipTypeName = GetChildComponent<StateText>("Label_txtBuwei");
            _txtEquipLevel = GetChildComponent<StateText>("Label_txtdengji");
            _particle = AddChildComponent<KParticle>("fx_ui_3_1");
            _effectItem = gameObject.AddComponent<MaskEffectItem>();
            _particle.transform.localPosition = new Vector2(0, -4);
            _particle.transform.localScale = new Vector2(1.26f, 1.3f);
            _particle.Stop();
            _itemIcon = GetChildComponent<ItemGrid>("Container_wupin");
            _itemIcon.Context = PanelIdEnum.EquipSmelter;
            _itemIcon.onClick = OnClickEquipIcon;
        }


        public override void Show()
        {
            base.Show();
            AddEventListener();
        }

        public override void Hide()
        {
            RemoveEventListener();
            base.Hide();
        }

        public override void Dispose()
        {
            _data = null;
        }

        public override bool IsSelected
        {
            get
            {
                return _checkmark.Visible;
            }
            set
            {
                _checkmark.Visible = value;
            }
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as EquipItemInfo;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            Refresh();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(EquipEvents.REFRESH_EQUIP_POINT, Refresh);
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, RefreshEquip);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(EquipEvents.REFRESH_EQUIP_POINT, Refresh);
            EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, RefreshEquip);
        }

        private void RefreshEquip(BagType bagType, uint index)
        {
            if(bagType == BagType.BodyEquipBag && index == _data.GridPosition)
            {
                _data = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo(_data.GridPosition) as EquipItemInfo;
            }
            Refresh();
        }

        private void Refresh()
        {
            _itemIcon.SetItemData(_data);
            _txtEquipTypeName.CurrentText.text = item_helper.GetEquipTypeDesc((int)_data.SubType);
            _txtEquipLevel.CurrentText.text = MogoLanguageUtil.GetContent(114004, _data.Level.ToString());

            if (CanShowParticle() == true)
            {
                _particle.Play(true);
            }
            else
            {
                _particle.Stop();
            }
            _effectItem.Init(gameObject, CanShowParticle, true);
        }

        private bool CanShowParticle()
        {
            return PlayerDataManager.Instance.EquipPointManager.RisingStarSet.Contains(_data.GridPosition);
        }

        private void OnClickEquipIcon()
        {

        }
    }
}
