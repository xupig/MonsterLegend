﻿
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class SmeltMeterialItem:KList.KListItemBase
    {
        private StateImage _imgLock;
        private StateImage _checkmark;
        private StateText _txtMaterialName;
        private StateText _txtMaterialDesc;
        private ItemGrid _itemIcon;

        private KParticle _partice;

        private equip_smelting _data;

        protected override void Awake()
        {
            _imgLock = GetChildComponent<StateImage>("Image_suo");
            _checkmark = GetChildComponent<StateImage>("Image_xuanzhongkuang_left");
            _checkmark.AddAllStateComponent<RaycastComponent>();
            _checkmark.Visible = false;
            _txtMaterialName = GetChildComponent<StateText>("Label_txtCailiaomingcheng");
            _txtMaterialDesc = GetChildComponent<StateText>("Label_txtCailiaoShuliang");
            _partice = AddChildComponent<KParticle>("fx_ui_3_1");
            _partice.transform.localPosition = new Vector2(0, -4);
            _partice.transform.localScale = new Vector2(1.26f, 1.3f);
            _itemIcon = GetChildComponent<ItemGrid>("Container_wupin");
        }

        public override void Show()
        {
            base.Show();
            AddEventListener();
        }

        public override void Hide()
        {
            RemoveEventListener();
            base.Hide();
        }

        public override void Dispose()
        {
            _data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data  = value as equip_smelting;
                SetDataDirty();
            }
        }

        public override bool IsSelected
        {
            get
            {
                return _checkmark.Visible;
            }
            set
            {
                _checkmark.Visible = (bool)value;
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            Refresh();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, OnBagUpdate);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, OnBagUpdate);
        }

        private void OnBagUpdate(BagType bagType, uint gridPosition)
        {
            BaseItemInfo itemInfo = PlayerDataManager.Instance.BagData.GetBagData(bagType).GetItemInfo((int)gridPosition);
            if (itemInfo == null || itemInfo.Id == _data.__display_item)
            {
                Refresh();
            }
        }

        private void Refresh()
        {
            List<BaseItemData> costList = equip_smelting_helper.GetCostList(_data.__id);
            _txtMaterialDesc.CurrentText.text = MogoLanguageUtil.GetContent(_data.__desc);
            _txtMaterialName.CurrentText.text = string.Format("<color={0}>{1}</color>", ColorDefine.GetColorHexToken(item_helper.GetQuality(_data.__display_item)), item_helper.GetName(_data.__display_item));
            _itemIcon.SetItemData(_data.__display_item);
            _itemIcon.Context = PanelIdEnum.EquipSmelter;
            _imgLock.Visible = PlayerAvatar.Player.level < _data.__level_limit;
            RefreshParticle();
        }

        public void RefreshParticle()
        {
            List<BaseItemData> costList = equip_smelting_helper.GetCostList(_data.__id);
            if (PlayerAvatar.Player.CheckCostLimit(costList) <= 0 && _data.__level_limit <= PlayerAvatar.Player.level)
            {
                _partice.Play(true);
            }
            else
            {
                _partice.Stop();
            }
        }

        public void HideParticle()
        {
            _partice.Stop();
        }
    }
}
