﻿
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.EventSystems;

namespace ModuleEquip
{
    public class SmeltProductBodyItem:KList.KListItemBase
    {
        private int _subType;
        private IconContainer _bodyIconContainer;
        private StateText _equipSubTypeName;
        private StateImage _checkmark;

        protected override void Awake()
        {
            _bodyIconContainer = AddChildComponent<IconContainer>("Container_icon");
            _equipSubTypeName = GetChildComponent<StateText>("Label_txtBuwei");
            _checkmark = GetChildComponent<StateImage>("Image_xuanzhong");
            _checkmark.Visible = false;
        }

        public override void Dispose()
        {
            _subType = -1;
        }

        public override bool IsSelected
        {
            get
            {
                return _checkmark.Visible;
            }
            set
            {
                _checkmark.Visible = value;
            }
        }

        public override object Data
        {
            get
            {
                return _subType;
            }
            set
            {
                _subType = (int)value;
                _checkmark.Visible = false;
                Dictionary<int, int> bodyToBodyIcon = equip_smelting_helper.GetBodyToBodyIconDic();
                _bodyIconContainer.SetIcon(bodyToBodyIcon[_subType]);
                _equipSubTypeName.CurrentText.text = item_helper.GetEquipTypeDesc(_subType);
            }
        }
        public override void OnPointerClick(PointerEventData evtData)
        {
            //屏蔽点击事件
        }
    }
}
