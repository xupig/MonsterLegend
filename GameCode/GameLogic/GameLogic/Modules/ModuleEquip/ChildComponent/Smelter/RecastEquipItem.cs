﻿
using Common.Base;
using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEquip
{
    public class RecastEquipItem:KList.KListItemBase
    {
        private StateImage _imgLock;
        private StateImage _checkmark;
        private StateText _txtEquipTypeName;
        private StateText _txtEquipLevel;

        private ItemGrid _itemIcon;

        private EquipItemInfo _data;

        protected override void Awake()
        {
            _imgLock = GetChildComponent<StateImage>("Image_suo");
            _imgLock.Visible = false;
            _checkmark = GetChildComponent<StateImage>("Image_xuanzhongkuang_left");
            _checkmark.AddAllStateComponent<RaycastComponent>();
            _checkmark.Visible = false;
            _txtEquipTypeName = GetChildComponent<StateText>("Label_txtBuwei");
            _txtEquipLevel = GetChildComponent<StateText>("Label_txtdengji");
            _itemIcon = GetChildComponent<ItemGrid>("Container_wupin");
            _itemIcon.Context = PanelIdEnum.Equip;
            _itemIcon.onClick = OnClickEquipIcon;
        }

        public override void Dispose()
        {
            _data = null;
        }

        public override bool IsSelected
        {
            get
            {
                return _checkmark.Visible;
            }
            set
            {
                _checkmark.Visible = value;
            }
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as EquipItemInfo;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            Refresh();
        }

        private void Refresh()
        {
            _itemIcon.SetItemData(_data);
            _txtEquipTypeName.CurrentText.text = item_helper.GetEquipTypeDesc((int)_data.SubType);
            _txtEquipLevel.CurrentText.text = MogoLanguageUtil.GetContent(114004, _data.Level.ToString());
        }

        private void OnClickEquipIcon()
        {
            
        }
    }
}
