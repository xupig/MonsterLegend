﻿using Common.Data;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ModuleEquip
{
    public class ReplaceMaterialItem : KList.KListItemBase
    {
        private StateText _needCount;
        private StateText _materialName;
        private ItemData _data;

        protected override void Awake()
        {
            _materialName = GetChildComponent<StateText>("Label_txtName");
            _needCount = GetChildComponent<StateText>("Label_txtshuzi");
        }

        public override void Dispose()
        {
            _data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as ItemData;
                    Refresh();
                }
            }
        }
        private void Refresh()
        {
            _materialName.CurrentText.text = MogoLanguageUtil.GetContent(item_helper.GetItemConfig(_data.Id).__name);
            _needCount.CurrentText.text = _data.StackCount.ToString();
        }
    }
}
