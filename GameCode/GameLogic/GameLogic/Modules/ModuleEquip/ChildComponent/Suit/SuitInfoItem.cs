﻿using Common.Base;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEquip
{
    public class SuitInfoItem:KList.KListItemBase
    {
        private string _suitNumContent;
        private string _suitUseNumContent;
        private int _suitId;
        private StateText _txtSuitName;
        private StateText _txtSuitNum;
        private ItemGrid _itemGrid;
        private KButton _downButton;
        private Locater _locater;

        protected override void Awake()
        {
            _txtSuitName = GetChildComponent<StateText>("Label_txtJuanzhoumingcheng");
            _suitNumContent = _txtSuitName.CurrentText.text;
            _txtSuitNum = GetChildComponent<StateText>("Label_txtShuliang");
            _suitUseNumContent = _txtSuitNum.CurrentText.text;
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _downButton = GetChildComponent<KButton>("Button_shousuodown");
            _locater = gameObject.AddComponent<Locater>();

            _downButton.onClick.AddListener(OnClick);
        }

        public override void Dispose()
        {
            _suitId = -1;
            _downButton.onClick.RemoveListener(OnClick);
        }

        public override object Data
        {
            get
            {
                return _suitId;
            }
            set
            {
                _suitId = (int)value;
                SetDataDirty();
            }
        }

        public Locater Locater
        {
            get
            {
                return _locater;
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            Refresh();
        }

        private void Refresh()
        {
            int suitEffectId = equip_suit_scroll_helper.GetSuitId(_suitId);
            _txtSuitName.CurrentText.text = string.Format(_suitNumContent, equip_suit_helper.GetName(suitEffectId), equip_suit_helper.GetEquipedSuitPartCount(suitEffectId), equip_suit_helper.GetSuitPartCount(suitEffectId));
            int itemNum = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(_suitId);
            _txtSuitNum.CurrentText.text = string.Format(_suitUseNumContent, itemNum);
            _itemGrid.SetItemData(_suitId);
            _itemGrid.Context = PanelIdEnum.Equip;
        }


        private void OnClick()
        {
            onClick.Invoke(this, Index);
        }
    }
}
