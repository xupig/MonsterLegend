﻿
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class SuitEquipNumEffectItem:KList.KListItemBase
    {
        private SuitEquipNumEffectDataWrapper _data;
        private StateText _txtSuitEquipNumEffect;

        protected override void Awake()
        {
            _txtSuitEquipNumEffect = GetChildComponent<StateText>("Label_txtTaozhuang");
        }

        public override void Dispose()
        {
            ;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as SuitEquipNumEffectDataWrapper;
                Refresh();
            }
        }

        private void Refresh()
        {
            List<string> contentList = attri_effect_helper.GetAttributeDescList(_data.EquipedNumEffectPair.Value);
            string content = string.Empty;
            for (int i = 0; i < contentList.Count; i++)
            {
                if (i == 0)
                {
                    content = _data.EquipedNumEffectPair.Key.ToString() + "件：" + contentList[i].Replace(" ", "");
                }
                else
                {
                    content = " " + contentList[i];
                }
            }
            if (_data.IsShowEquipedBodyName == true)
            {
                //108016,已开启
                content += "(" + SpliceEquipedBodyNameString(_data.SuitEffectId) + MogoLanguageUtil.GetContent(108016) + ")";
            }
            if (_data.IsMeetEquipedNum == true)
            {
                _txtSuitEquipNumEffect.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_SUIT_MEET);
            }
            else
            {
                _txtSuitEquipNumEffect.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_SUIT_NOT_MEET);
            }
            _txtSuitEquipNumEffect.CurrentText.text = content;
            _txtSuitEquipNumEffect.RecalculateAllStateHeight();
            RecalculateSize();
        }

        private string SpliceEquipedBodyNameString(int suitEffectId)
        {
            List<int> equipedBodyList = equip_suit_helper.GetEquipedSuitPartList(suitEffectId);
            string content = string.Empty;
            for (int i = 0; i < equipedBodyList.Count; i++)
            {
                if (i == 0)
                {
                    content += item_helper.GetEquipPositionDesc(equipedBodyList[i]);
                }
                else
                {
                    content += "、" + item_helper.GetEquipPositionDesc(equipedBodyList[i]);
                }
            }
            return content;
        }
    }

    public class SuitEquipNumEffectDataWrapper
    {
        public KeyValuePair<int, int> EquipedNumEffectPair;
        public bool IsShowEquipedBodyName;
        public bool IsMeetEquipedNum;
        public int SuitEffectId;

        public SuitEquipNumEffectDataWrapper(KeyValuePair<int, int> equipedNumEffectPair)
        {
            EquipedNumEffectPair = equipedNumEffectPair;
        }
    }
}
