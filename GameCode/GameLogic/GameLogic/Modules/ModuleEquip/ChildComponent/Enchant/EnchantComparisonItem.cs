﻿using Common.Global;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class EnchantComparisonItem : KContainer
    {
        private const int MAX_ENCHANT_NUM = 3;
        private PbEquipEnchantAttriInfo _data;

        private EnchantAttriItem[] _enchantAttributeList;

        protected override void Awake()
        {
            _enchantAttributeList = new EnchantAttriItem[MAX_ENCHANT_NUM];
            for (int i = 0; i < MAX_ENCHANT_NUM; i++)
            {
                _enchantAttributeList[i] = AddChildComponent<EnchantAttriItem>(string.Format("Container_dangqianshuxing{0}", i + 1));
            }

        }

        public PbEquipEnchantAttriInfo Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
                Refresh();
            }
        }

        private void Refresh()
        {
            int attriCount = 0;
            if (_data != null && _data.enchant_attri.Count != 0)
            {
                EquipItemInfo equipItemInfo = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo((int)_data.pos) as EquipItemInfo;
                int enchantScrollId = enchant_scroll_helper.GetEnchantScrollId(equipItemInfo);
                attriCount = _data.enchant_attri.Count;
                for (int i = 0; i < attriCount; i++)
                {
                    List<int> range = enchant_random_helper.GetAttributeRange(enchantScrollId, (int)_data.enchant_attri[i].enchant_attri_id, equipItemInfo.Level, equipItemInfo.Quality, (int)_data.luck_value);
                    EnchantAttriData attriData = new EnchantAttriData();
                    attriData.enchantAttri = _data.enchant_attri[i];
                    attriData.range = range;
                    _enchantAttributeList[i].Data = attriData;
                }
            }
            for (int i = attriCount; i < MAX_ENCHANT_NUM; i++)
            {
                _enchantAttributeList[i].Data = null;
            }
        }

    }

    public class EnchantAttriItem : KContainer
    {
        private StateText attribute;
        private StateText rangeTxt;

        private EnchantAttriData _data;

        protected override void Awake()
        {
 	         base.Awake();
             rangeTxt = GetChildComponent<StateText>("Label_txtfanwei");
             attribute = GetChildComponent<StateText>("Label_txtshuxing");
        }

        public EnchantAttriData Data
        {
            set
            {
                _data = value;
                Refresh();
            }
        }

        private void Refresh()
        {
            if(_data == null)
            {
                attribute.ChangeAllStateText(MogoLanguageUtil.GetContent(30704));
                rangeTxt.ChangeAllStateText(string.Empty);
                return;
            }

            string attributeName = attri_config_helper.GetAttributeName((int)_data.enchantAttri.enchant_attri_id);
            uint attributeValue = _data.enchantAttri.enchant_attri_value;

            List<int> recommentAttriList = GlobalParams.GetRecommentAttri(PlayerAvatar.Player.vocation);
            string attriContent;
            if (recommentAttriList.Contains((int)_data.enchantAttri.enchant_attri_id) == true)
            {
                attriContent = string.Format("{0}<color=#77ff00>({1})</color>+{2}", attributeName, MogoLanguageUtil.GetContent(114011), attributeValue);
            }
            else
            {
                attriContent = string.Format("{0}+{1}", attributeName, attributeValue);
            }
            attribute.CurrentText.text = attriContent;

            if (_data.range == null)
            {
                rangeTxt.ChangeAllStateText(string.Empty);
            }
            else
            {
                rangeTxt.ChangeAllStateText(string.Format("【{0}-{1}】", _data.range[0], _data.range[1]));
            }
        }
    }

    public class EnchantAttriData
    {
        public PbEquipEnchantAttri enchantAttri;

        public List<int> range;
    }
}
