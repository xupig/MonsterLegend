﻿using Common.Global;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.EventSystems;

namespace ModuleEquip
{
    public class EnchantAttributeItem:KList.KListItemBase
    {
        private StateImage _checkmark;
        private StateImage _oddImg;

        private StateText _txtAttributeName;

        private int _data;

        protected override void Awake()
        {
            _txtAttributeName = GetChildComponent<StateText>("Label_txtlajizhuanbei");
            _checkmark = GetChildComponent<StateImage>("ScaleImage_sharedGrid1Bg");
            _oddImg = GetChildComponent<StateImage>("ScaleImage_sharedGridBg");
        }

        public override void Dispose()
        {
            _data = 0;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = 0;
                _checkmark.Visible = false;
                if (value != null)
                {
                    _data = (int)value;
                }
                Refresh();
            }
        }

        public override bool IsSelected
        {
            get
            {
                return base.IsSelected;
            }
            set
            {
                base.IsSelected = value;
                _checkmark.Visible = value;
            }
        }

        public override void OnPointerClick(PointerEventData evtData)
        {
            //屏蔽点击事件
        }

        private void Refresh()
        {
            if ((Index % 5) % 2 == 0)
            {
                _oddImg.Visible = true;
            }
            else
            {
                _oddImg.Visible = false;
            }
            List<int> recommentAttriList = GlobalParams.GetRecommentAttri(PlayerAvatar.Player.vocation);
            string content = string.Empty;
            if (recommentAttriList.Contains(_data) == true)
            {
                content = string.Format("{0}<color=#77ff00>({1})</color>", attri_config_helper.GetAttributeName(_data), MogoLanguageUtil.GetContent(114011));
            }
            else
            {
                content = attri_config_helper.GetAttributeName(_data);
            }
            _txtAttributeName.CurrentText.text = content;

        }
    }

    /**当前附魔状态**/
    public enum EnchantAttributeState
    {
        Empty,   //未附魔
        Enchant, //已附魔
        UnableEnchant, //不能附魔
    }
}
