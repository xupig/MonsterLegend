﻿using Common.Data;
using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.EventSystems;

namespace ModuleEquip
{
    public class CompositionGridItem : KList.KListItemBase
    {
        private KToggle _toggle;
        private ItemGrid _itemGrid;
        private EquipItemInfo _data;

        protected override void Awake()
        {
            _toggle = GetChildComponent<KToggle>("Toggle_Xuanze");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            GetChild("Label_txtNum").gameObject.SetActive(false);

            _toggle.Visible = false;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                Clear();
                if (value != null)
                {
                    _data = value as EquipItemInfo;
                    Refresh();
                }
            }
        }

        public override void OnPointerClick(PointerEventData evtData)
        {
            if (evtData.dragging == true)
            {
                return;
            }
            _toggle.isOn = !_toggle.isOn;
        }

        public override bool IsSelected
        {
            get
            {
                return _toggle.isOn;
            }
            set
            {
                _toggle.isOn = value;
            }
        }

        public override void Dispose()
        {
            _data = null;
        }

        private void Clear()
        {
            _toggle.Visible = false;
            _toggle.isOn = false;
            _itemGrid.Clear();
        }

        private void Refresh()
        {
            IsSelected = DecompositionFilterView.IsSelectedAll;
            _toggle.Visible = true;
            _itemGrid.SetItemData(_data);
        }

    }
}
