﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class NeedMaterialItem : KContainer
    {
        private StateText _needCount;
        private ItemGrid _itemGrid;

        private BaseItemData _data;

        protected override void Awake()
        {
            base.Awake();
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _needCount = GetChildComponent<StateText>("Label_txtLinghunshiValue");
        }

        protected override void OnDestroy()
        {
             _data = null;
 	         base.OnDestroy();
        }

        public BaseItemData Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            _itemGrid.SetItemData(_data);
            _needCount.CurrentText.text = string.Format("x{0}", _data.StackCount);
        }
    }
}
