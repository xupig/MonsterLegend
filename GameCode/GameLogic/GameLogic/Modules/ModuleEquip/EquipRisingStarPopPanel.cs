﻿using Common.Base;
using Common.ExtendComponent;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class EquipRisingStarPopPanel:BasePanel
    {
        private GameObject _goDetail;
        private LStarList _starList;
        private KDummyButton _btnClose;
        private StateText _textDesc;
        private ItemGrid _itemGrid;

        private KParticle _particleUp;
        private List<KParticle> _particleStarList;

        protected override void Awake()
        {
            base.Awake();
            _goDetail = GetChild("Container_detail");
            _starList = AddChildComponent<LStarList>("Container_xingxing");
            _btnClose = AddChildComponent<KDummyButton>("Container_detail/ScaleImage_sharedzhezhao");
            _textDesc = GetChildComponent<StateText>("Container_detail/Label_txtShengxing");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _particleUp = AddChildComponent<KParticle>("Container_wupin/fx_ui_21_4_star_02_wupin");
            _particleUp.Stop();

            _particleStarList = new List<KParticle>();
            for (int i = 0; i < 5; i++)
            {
                KParticle particle = AddChildComponent<KParticle>(string.Format("Container_xingxing/Container_star{0}/fx_ui_21_4_star_02", i));
                particle.Stop();
                _particleStarList.Add(particle);
            }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void SetData(object data)
        {
            PbEquipUpgradeResp equipUpgrade = data as PbEquipUpgradeResp;
            PbGridItem item = new PbGridItem();
            item.grid_index = equipUpgrade.pos;
            item.item = equipUpgrade.upgraded_equip;
            ShowUpStarContent(new EquipItemInfo(item));
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.EquipRisingStarPop; }
        }

        private void AddEventListener()
        {
            _btnClose.onClick.AddListener(ClosePanel);
            for (int i = 0; i < _particleStarList.Count; i++)
            {
                _particleStarList[i].onComplete.AddListener(OnComplete);
            }
        }

        private void RemoveEventListener()
        {
            _btnClose.onClick.RemoveListener(ClosePanel);
            for (int i = 0; i < _particleStarList.Count; i++)
            {
                _particleStarList[i].onComplete.RemoveListener(OnComplete);
            }
        }

        private void ShowUpStarContent(EquipItemInfo itemInfo)
        {
            _textDesc.CurrentText.text = MogoLanguageUtil.GetContent(114062, itemInfo.Star);
            _itemGrid.SetItemData(itemInfo);
            _starList.SetNum(itemInfo.Star);
            _goDetail.SetActive(false);
            _particleStarList[itemInfo.Star - 1].Play();
            TimerHeap.AddTimer(1000, 0, PlayUpParticle);
        }

        private void PlayUpParticle()
        {
            _particleUp.Play();
        }

        private void OnComplete()
        {
            _goDetail.SetActive(true);
        }
    }
}
