﻿using Common.Base;
using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEquip
{
    public class EquipSmelterPanel:BasePanel
    {
        private KToggleGroup _toggleGroup;
        private KToggle _recastToggle;
        private KToggle _smeltToggle;
        private KToggle _upgradeToggle;
        private RecastChildView _recastChildView;
        private SmeltChildView _smeltChildView;
        private EquipRisingStarView _risingStarView;
        private SmelterBottomView _smelterBottomView;
        private KButton _closeButton;
        private int _fightForce;

        private List<int> toggleList = new List<int>();

        protected override void Awake()
        {
            base.Awake();
            _closeButton = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _smelterBottomView = AddChildComponent<SmelterBottomView>("Container_dibu");
            _smeltChildView = AddChildComponent<SmeltChildView>("Container_ronglian");
            _recastChildView = AddChildComponent<RecastChildView>("Container_chongzhu");
            _risingStarView = AddChildComponent<EquipRisingStarView>("Container_shengxing");
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            _smeltToggle = GetChildComponent<KToggle>("ToggleGroup_biaoti/Toggle_Ronglian");
            _recastToggle = GetChildComponent<KToggle>("ToggleGroup_biaoti/Toggle_chongzhu");
            _upgradeToggle = GetChildComponent<KToggle>("ToggleGroup_biaoti/Toggle_shengxing");

            toggleList.Add((int)FunctionId.smelt);
            toggleList.Add((int)FunctionId.EquipRecast);
            toggleList.Add((int)FunctionId.risingStar);
            SetTabList(toggleList, _toggleGroup);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.EquipSmelter; }
        }

        public override void EnableCanvas()
        {
            base.EnableCanvas();
            if (SmeltDetailView.BetterEquip != null)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.UIOperationShield);
                ToolTipsManager.Instance.ShowItemTip(SmeltDetailView.BetterEquip, PanelIdEnum.EquipSmelter);
                SmeltDetailView.BetterEquip = null;
            }
        }

        public override void OnShow(object data)
        {
            _fightForce = (int)PlayerAvatar.Player.fight_force;
            AddEventListener();
            SetData(data);
            RefreshPoint();
            ResetFightForceChangeValue();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            if (data is int)
            {
                _toggleGroup.SelectIndex = (int)data - 1;
                RefreshContent((SmelterType)data - 1);
            }
            else if (data is int[])
            {
                int[] list = data as int[];
                _toggleGroup.SelectIndex = list[0] - 1;
                RefreshContent((SmelterType)list[0] - 1);
            }
            else
            {
                _toggleGroup.SelectIndex = (int)SmelterType.Smelt;
                RefreshContent(SmelterType.Smelt);
            }
        }


        private void RefreshPoint()
        {
            _smeltToggle.SetGreenPoint(PlayerDataManager.Instance.EquipPointManager.CanSmelt() && _smeltChildView.Visible == false);
            _recastToggle.SetGreenPoint(PlayerDataManager.Instance.EquipPointManager.CanRecast());
            _upgradeToggle.SetGreenPoint(PlayerDataManager.Instance.EquipPointManager.RisingStarSet.Count != 0);
        }

        private void RefreshContent(SmelterType smeltType)
        {
            if (smeltType == SmelterType.Recast)
            {
                _risingStarView.Visible = false;
                _smeltChildView.Visible = false;
                _recastChildView.Visible = true;
                _recastChildView.Show();
            }
            else if (smeltType == SmelterType.Smelt)
            {
                _risingStarView.Visible = false;
                _recastChildView.Visible = false;
                _smeltChildView.Visible = true;
                PlayerDataManager.Instance.EquipPointManager.RemovePoint(EquipSubTab.Smelt);
                _smeltChildView.Show();
            }
            else
            {
                _recastChildView.Visible = false;
                _smeltChildView.Visible = false;
                _risingStarView.Visible = true;
                _risingStarView.Show();
            }
        }

        private void AddEventListener()
        {
            _closeButton.onClick.AddListener(CloseButtonClick);
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedToggleChange);
            EventDispatcher.AddEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshPoint);
            EventDispatcher.AddEventListener<PanelIdEnum>(EquipEvents.EQUIP_TIPS_SHOW_FINISH, EquipTipsShowFinish);
        }

        private void RemoveEventListener()
        {
            _closeButton.onClick.RemoveListener(CloseButtonClick);
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedToggleChange);
            EventDispatcher.RemoveEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshPoint);
            EventDispatcher.RemoveEventListener<PanelIdEnum>(EquipEvents.EQUIP_TIPS_SHOW_FINISH, EquipTipsShowFinish);
        }

        private void EquipTipsShowFinish(PanelIdEnum context)
        {
            if (context == PanelIdEnum.EquipSmelter)
            {
                UIManager.Instance.ClosePanel(PanelIdEnum.UIOperationShield);
            }
        }

        private void OnSelectedToggleChange(KToggleGroup toggleGroup, int index)
        {
            RefreshContent((SmelterType)index);
        }

        private void ResetFightForceChangeValue()
        {
            PlayerDataManager.Instance.SmelterData.RecastFightForceChange = 0;
            PlayerDataManager.Instance.SmelterData.SmeltFightForceChange = 0;
        }

        private void CloseButtonClick()
        {
            ClosePanel();
            PlayerDataManager.Instance.SmelterData.Check();
        }
    }

    public enum SmelterType
    {
        Smelt = 0,
        Recast = 1,
        RisingStar = 2,
    }
}
