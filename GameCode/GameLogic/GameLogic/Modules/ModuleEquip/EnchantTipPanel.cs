﻿using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class EnchantTipPanel:BasePanel
    {
        private KContainer _replaceContainer;
        private EnchantComparisonItem _beforeAttribute;
        private EnchantComparisonItem _afterAttribute;

        private StateText _resultAnalysisTip;
        private StateImage _increaseImg;
        private StateImage _decreaseImg;

        private KButton _replaceBtn;
        private KButton _abandonBtn;

        private EnchantAttriComparisonData _comparisonData;

        protected override void Awake()
        {
            _replaceContainer = GetChildComponent<KContainer>("Container_fumotip/Container_shifoutihuan");

            _replaceBtn = GetChildComponent<KButton>("Container_fumotip/Container_shifoutihuan/Button_tihuan");
            _abandonBtn = GetChildComponent<KButton>("Container_fumotip/Container_shifoutihuan/Button_fangqi");

            _resultAnalysisTip = GetChildComponent<StateText>("Container_fumotip/Label_txttip");
            _increaseImg = GetChildComponent<StateImage>("Container_fumotip/Image_tisheng");
            _decreaseImg = GetChildComponent<StateImage>("Container_fumotip/Image_xiajiang");
            _beforeAttribute = AddChildComponent<EnchantComparisonItem>("Container_fumotip/Container_fumoqian");
            _afterAttribute = AddChildComponent<EnchantComparisonItem>("Container_fumotip/Container_fumohou");
        }

        public override void OnShow(object data)
        {
            AddEventsListener();
            SetData(data);
        }

        public override void OnClose()
        {
            RemoveEventsListener();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.EquipEnchantTip; }
        }

        public override void SetData(object data)
        {
            _comparisonData = data as EnchantAttriComparisonData;
            FillEnchantData();
            ShowResultTip();
        }

        private void AddEventsListener()
        {
            _replaceBtn.onClick.AddListener(OnReplaceBtnClick);
            _abandonBtn.onClick.AddListener(OnCloseView);
        }

        private void RemoveEventsListener()
        {
            _replaceBtn.onClick.RemoveListener(OnReplaceBtnClick);
            _abandonBtn.onClick.RemoveListener(OnCloseView);
        }

        private void OnCloseView()
        {
            _comparisonData = null;
            EventDispatcher.TriggerEvent<bool>(EnchantEvents.RefreshAttribute, false);
            ClosePanel();
        }

        private void OnReplaceBtnClick()
        {
            EquipManager.Instance.RequestReplace((int)_comparisonData.AfterEnchantAttri.pos);
            EventDispatcher.TriggerEvent<bool>(EnchantEvents.RefreshAttribute, true);
            ClosePanel();
            _comparisonData = null;
        }

        private void FillEnchantData()
        {
            _beforeAttribute.Data = _comparisonData.CurrentEnchantAttri;
            _afterAttribute.Data = _comparisonData.AfterEnchantAttri;
        }

        private void ShowResultTip()
        {
            Dictionary<int, float> currentAttributeDic = new Dictionary<int, float>();
            Dictionary<int, float> afterAttributeDic = new Dictionary<int, float>();
            int current = enchant_helper.ComputeEnchantFightForce(_comparisonData.CurrentEnchantAttri);
            int after = enchant_helper.ComputeEnchantFightForce(_comparisonData.AfterEnchantAttri);
            _increaseImg.Visible = false;
            _decreaseImg.Visible = false;
            if (current < after)
            {
                _resultAnalysisTip.CurrentText.text = MogoLanguageUtil.GetContent(30702, after - current);
                _increaseImg.Visible = true;
            }
            else
            {
                _decreaseImg.Visible = true;
                _resultAnalysisTip.CurrentText.text = MogoLanguageUtil.GetContent(30705, current - after);
            }

        }
    }
}
