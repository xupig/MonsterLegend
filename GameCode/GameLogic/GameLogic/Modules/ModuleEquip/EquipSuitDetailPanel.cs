﻿using Common.Base;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquip
{
    public class EquipSuitDetailPanel:BasePanel
    {
        private string _titleContent;
        private EquipInfoView _equipInfoView;
        private KList _effectList;
        private StateText _txtTitle;
        private StateText _txtFightForce;
        private StateImage _imgFightForceUp;
        private StateImage _imgFightForceDown;
        private KDummyButton _closeButton;

        private int gridPosition;
        private int oldSuitEffectId;
        private int newSuitEffectId;

        protected override void Awake()
        {
            base.Awake();
            _closeButton = gameObject.AddComponent<KDummyButton>();
            _closeButton.onClick.AddListener(ClosePanel);
            _equipInfoView = AddChildComponent<EquipInfoView>("Container_kaiqitaochan/Container_nr/Container_wupinitem");
            _imgFightForceUp = GetChildComponent<StateImage>("Container_kaiqitaochan/Container_nr/Image_tisheng");
            _imgFightForceDown = GetChildComponent<StateImage>("Container_kaiqitaochan/Container_nr/Image_xiajiang");
            _txtFightForce = GetChildComponent<StateText>("Container_kaiqitaochan/Container_nr/Label_txtZhanli");
            _txtTitle = GetChildComponent<StateText>("Container_kaiqitaochan/Label_txtBiaoti");
            _titleContent = _txtTitle.CurrentText.text;
            _effectList = GetChildComponent<KList>("Container_kaiqitaochan/Container_nr/List_xiaoguo");

            _effectList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.SuitDetail; }
        }

        public override void OnShow(object data)
        {
            int[] list = data as int[];
            gridPosition = list[0];
            oldSuitEffectId = list[1];
            newSuitEffectId = list[2];

            ShowTitleInfo();
            ShowEquipInfoView();
            ShowEffectList();
            ShowFightForce();
        }

        public override void OnClose()
        {
            
        }

        private void ShowFightForce()
        {
            int equipedBodyCount = 0;
            int effectId = 0;
            int changeValue = 0;
            if (oldSuitEffectId != 0)
            {
                equipedBodyCount = equip_suit_helper.GetEquipedSuitPartCount(oldSuitEffectId);
                effectId = equip_suit_helper.GetEffectId(oldSuitEffectId, equipedBodyCount + 1);

                //原套装的战力变化
                changeValue -= equip_suit_helper.GetFightForce(oldSuitEffectId, equipedBodyCount + 1);
                changeValue += equip_suit_helper.GetFightForce(oldSuitEffectId, equipedBodyCount);
            }
            equipedBodyCount = equip_suit_helper.GetEquipedSuitPartCount(newSuitEffectId);
            effectId = equip_suit_helper.GetEffectId(newSuitEffectId, equipedBodyCount);

            //现套装战力变化
            changeValue -= equip_suit_helper.GetFightForce(newSuitEffectId, equipedBodyCount - 1);
            changeValue += equip_suit_helper.GetFightForce(newSuitEffectId, equipedBodyCount);
            string content = string.Empty;
            if (changeValue < 0)
            {
                content = changeValue.ToString();
            }
            else
            {
                content = string.Format("+{0}", changeValue);
            }
            _txtFightForce.CurrentText.text = MogoLanguageUtil.GetContent(108017, content);

            float x = _txtFightForce.CurrentText.preferredWidth + _txtFightForce.CurrentText.transform.localPosition.x;
            _imgFightForceUp.Visible = changeValue > 0;
            _imgFightForceDown.Visible = changeValue < 0;
            _imgFightForceUp.transform.localPosition = new Vector2(x, _imgFightForceUp.transform.localPosition.y);
            _imgFightForceDown.transform.localPosition = new Vector2(x, _imgFightForceDown.transform.localPosition.y);
        }

        private void ShowTitleInfo()
        {
            string suitName = equip_suit_helper.GetName(newSuitEffectId);
            int currentNum = equip_suit_helper.GetEquipedSuitPartCount(newSuitEffectId);
            int totalNum = equip_suit_helper.GetSuitPartCount(newSuitEffectId);
            _txtTitle.CurrentText.text = string.Format(_titleContent, suitName, currentNum, totalNum);
        }

        private void ShowEquipInfoView()
        {
            EquipItemInfo equipItemInfo = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo(gridPosition) as EquipItemInfo;
            _equipInfoView.ShowEquip(equipItemInfo);
        }

        private void ShowEffectList()
        {
            List<SuitEquipNumEffectDataWrapper> dataWrapperList = GetPackageSuitEffectData(newSuitEffectId);
            _effectList.RemoveAll();
            _effectList.SetDataList<SuitEquipNumEffectItem>(dataWrapperList);
            RecalculateListLayout();
        }

        private List<SuitEquipNumEffectDataWrapper> GetPackageSuitEffectData(int suitEffectId)
        {
            //key表示装备该套装效果的装备的数量，value表示带来的属性效果
            Dictionary<int, int> suitEffectDic = equip_suit_helper.GetSuitEffectDict(suitEffectId);
            int suitEffectEquipedNum = equip_suit_helper.GetEquipedSuitPartCount(suitEffectId);
            List<SuitEquipNumEffectDataWrapper> result = new List<SuitEquipNumEffectDataWrapper>();
            //记录将要显示已装备的那条数据
            SuitEquipNumEffectDataWrapper showNameDataWrapper = null;
            int meetMaxKey = 0;
            foreach (KeyValuePair<int, int> pair in suitEffectDic)
            {
                SuitEquipNumEffectDataWrapper dataWrapper = new SuitEquipNumEffectDataWrapper(pair);
                dataWrapper.IsShowEquipedBodyName = false;
                dataWrapper.SuitEffectId = suitEffectId;
                if (pair.Key <= suitEffectEquipedNum)
                {
                    dataWrapper.IsMeetEquipedNum = true;
                }
                else
                {
                    dataWrapper.IsMeetEquipedNum = false;
                }
                result.Add(dataWrapper);

                ////找到将要显示已装备的那条数据
                if (pair.Key > meetMaxKey && suitEffectEquipedNum >= pair.Key)
                {
                    if (showNameDataWrapper != null)
                    {
                        showNameDataWrapper.IsShowEquipedBodyName = false;
                    }
                    meetMaxKey = pair.Key;
                    showNameDataWrapper = dataWrapper;
                }
            }
            if (showNameDataWrapper != null)
            {
                showNameDataWrapper.IsShowEquipedBodyName = true;
            }

            return result;
        }

        private void RecalculateListLayout()
        {
            RectTransform rect = _effectList.GetComponent<RectTransform>();
            float height = 0;
            float y = 0;
            for (int i = 0; i < _effectList.ItemList.Count; i++)
            {
                RectTransform childRect = _effectList[i].GetComponent<RectTransform>();
                if (childRect.gameObject.activeSelf == false)
                {
                    continue;
                }
                childRect.anchoredPosition = new Vector2(childRect.anchoredPosition.x, y);
                float childHeight = childRect.sizeDelta.y;
                height += childHeight;
                y -= childHeight;
            }
            rect.sizeDelta = new Vector2(rect.sizeDelta.x, height);
        }
    }
}
