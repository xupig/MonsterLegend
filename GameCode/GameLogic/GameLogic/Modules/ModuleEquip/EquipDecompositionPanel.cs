﻿using Common.Base;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEquip
{
    public class EquipDecompositionPanel:BasePanel
    {
        private KToggle _decomposeToggle;
        private EquipDecompositionView _equipDecompositionView;

        protected override void Awake()
        {
            base.Awake();
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _decomposeToggle = GetChildComponent<KToggle>("ToggleGroup_biaoti/Toggle_fenjie");
            _equipDecompositionView = AddChildComponent<EquipDecompositionView>("Container_fenjie");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.EquipDecomposition; }
        }

        public override void OnShow(object data)
        {
            _equipDecompositionView.Visible = true;
            AddEventsListener();
            RefreshPoint();
        }

        public override void OnClose()
        {
            _equipDecompositionView.Visible = false;
            RemoveEventsListener();
        }

        private void AddEventsListener()
        {
            EventDispatcher.AddEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshPoint);
        }

        private void RemoveEventsListener()
        {
            EventDispatcher.RemoveEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshPoint);
        }

        private void RefreshPoint()
        {
            _decomposeToggle.SetGreenPoint(PlayerDataManager.Instance.EquipPointManager.Decompose);
        }
    }
}
