﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using GameData;

namespace ModuleEquip
{
    public class EquipModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            RegisterPopPanelBunchPanel(PanelIdEnum.EquipEnchantTip, PanelIdEnum.Equip);
            AddPanel(PanelIdEnum.Equip, "Container_EquipPanel", MogoUILayer.LayerUIPanel, "ModuleEquip.EquipPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.EquipSmelter, "Container_EquipSmelterPanel", MogoUILayer.LayerUIPanel, "ModuleEquip.EquipSmelterPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.EquipDecomposition, "Container_EquipDecompositionPanel", MogoUILayer.LayerUIPanel, "ModuleEquip.EquipDecompositionPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.EquipEnchantTip, "Container_EnchantTipPanel", MogoUILayer.LayerUIPopPanel, "ModuleEquip.EnchantTipPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.EquipRisingStarPop, "Container_EquipRisingStarPopPanel", MogoUILayer.LayerUIPanel, "ModuleEquip.EquipRisingStarPopPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.SuitDetail, "Container_EquipSuitDetailPanel", MogoUILayer.LayerUIPanel, "ModuleEquip.EquipSuitDetailPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.RecastSkill, "Container_EquipRecastSkillPanel", MogoUILayer.LayerUINotice, "ModuleEquip.EquipRecastSkillPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.RecastCompare, "Container_EquipRecastComparePanel", MogoUILayer.LayerUINotice, "ModuleEquip.EquipRecastComparePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);

            AddEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<int, int, int>(EquipEvents.EQUIP_SUIT_EFFECT_CHANGED, TryShowSuitDetailPanel);
        }

        public void TryShowSuitDetailPanel(int gridPosition, int oldSuitEffectId, int newSuitEffectId)
        {
            if (IsMeetShowCondition(oldSuitEffectId, newSuitEffectId) == true)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.SuitDetail, new int[] { gridPosition, oldSuitEffectId, newSuitEffectId });
            }
        }

        private bool IsMeetShowCondition(int oldSuitEffectId, int newSuitEffectId)
        {
            //某装备失去套装
            if (oldSuitEffectId == 0 && newSuitEffectId == 0)
            {
                return false;
            }

            Dictionary<int, int> oldSuitEffectDic = new Dictionary<int,int>();
            int oldSuitEffectEquipedNum = 0;
            Dictionary<int, int> newSuitEffectDic = new Dictionary<int, int>();
            int newSuitEffectEquipedNum = 0;

            if (oldSuitEffectId != 0)
            {
                oldSuitEffectDic = equip_suit_helper.GetSuitEffectDict(oldSuitEffectId);
                oldSuitEffectEquipedNum = equip_suit_helper.GetEquipedSuitPartCount(oldSuitEffectId);
            }
            if (newSuitEffectId != 0)
            {
                newSuitEffectDic = equip_suit_helper.GetSuitEffectDict(newSuitEffectId);
                newSuitEffectEquipedNum = equip_suit_helper.GetEquipedSuitPartCount(newSuitEffectId);
            }

            return oldSuitEffectDic.ContainsKey(oldSuitEffectEquipedNum + 1)
                || newSuitEffectDic.ContainsKey(newSuitEffectEquipedNum);
        }
    }
}
