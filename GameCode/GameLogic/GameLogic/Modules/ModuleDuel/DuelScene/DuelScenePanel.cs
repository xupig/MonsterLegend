﻿using Common.Base;
using Common.Events;
using MogoEngine.Events;

namespace ModuleDuel
{
    public class DuelScenePanel : BasePanel
    {
        public DuelSceneScoreView _scoreView;
        public DuelMinimize _minimize;

        protected override void Awake()
        {
            _scoreView = AddChildComponent<DuelSceneScoreView>("Container_juedouleft");
            _minimize = AddChildComponent<DuelMinimize>("Container_zuixiaohua");
        }

        public override void OnShow(object data)
        {
            _scoreView.Visible = true;
            _scoreView.Refresh();

            AddListener();
        }

        public override void OnClose()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            EventDispatcher.AddEventListener(DuelEvents.Minimize, OnMinimize);
            _minimize.onMaximize.AddListener(onMaximize);
        }

        private void RemoveListener()
        {
            EventDispatcher.RemoveEventListener(DuelEvents.Minimize, OnMinimize);
            _minimize.onMaximize.RemoveListener(onMaximize);
        }

        private void OnMinimize()
        {
            _minimize.Visible = true;
            _minimize.Refresh();
        }

        private void onMaximize()
        {
            _minimize.Visible = false;
            EventDispatcher.TriggerEvent(DuelEvents.Maximize);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.DuelScene; }
        }
    }
}
