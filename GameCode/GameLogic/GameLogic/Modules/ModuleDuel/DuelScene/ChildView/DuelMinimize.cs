﻿using Common.Data;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleDuel
{
    public class DuelMinimize : KContainer
    {
        private uint _timerId = TimerHeap.INVALID_ID;

        private KButton _btnMaximize;
        private StateText _textInfo;

        public KComponentEvent onMaximize = new KComponentEvent();

        public DuelData _duelData
        {
            get
            {
                return PlayerDataManager.Instance.DuelData;
            }
        }

        protected override void Awake()
        {
            _btnMaximize = GetChildComponent<KButton>("Button_zhankai");
            _textInfo = GetChildComponent<StateText>("Label_txtXinxi");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnMaximize.onClick.AddListener(OnMaximize);
        }

        private void RemoveListener()
        {
            _btnMaximize.onClick.RemoveListener(OnMaximize);
        }

        protected override void OnEnable()
        {
            EventDispatcher.AddEventListener(DuelEvents.RefreshFightView, Refresh);
        }

        protected override void OnDisable()
        {
            RemoveTimer();
            EventDispatcher.RemoveEventListener(DuelEvents.RefreshFightView, Refresh);
        }

        private void OnMaximize()
        {
            onMaximize.Invoke();
        }

        public void Refresh()
        {
            AddTimer();
        }

        private void AddTimer()
        {
            if (_timerId == TimerHeap.INVALID_ID)
            {
                _timerId = TimerHeap.AddTimer(0, 1000, OnTick);
                OnTick();
            }
        }

        private void RemoveTimer()
        {
            if (_timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = TimerHeap.INVALID_ID;
            }
        }

        private void OnTick()
        {
            int leftTime = _duelData.GetLeftSecond();
            if (leftTime <= 0)
            {
                RemoveTimer();
            }
            switch (_duelData.DuelState)
            {
                case DuelData.State.preparing:
                    _textInfo.CurrentText.text = MogoLanguageUtil.GetContent(122105, leftTime);
                    break;
                case DuelData.State.ready:
                    _textInfo.CurrentText.text = MogoLanguageUtil.GetContent(122106, leftTime);
                    break;
            }
        }
    }
}
