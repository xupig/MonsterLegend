﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleDuel
{
    public class DuelSceneScoreView : KContainer
    {
        private KButton _btnBillboad;
        private KButton _btnSearch;
        private KButton _btnQuestion;
        private StateText _textMyScore;
        private MyDivision _hideDivision;
        private MyDivision _division;

        private PlayerAvatar _player
        {
            get
            {
                return PlayerAvatar.Player;
            }
        }

        protected override void Awake()
        {
            _btnBillboad = GetChildComponent<KButton>("Button_juedoubang");
            _btnSearch = GetChildComponent<KButton>("Button_xunzhaoduishou");
            _btnQuestion = GetChildComponent<KButton>("Button_wenhao");
            _textMyScore = GetChildComponent<StateText>("Label_txtJifen");
            _hideDivision = AddChildComponent<MyDivision>("Container_paimingkeneng");
            _division = AddChildComponent<MyDivision>("Container_paiming");
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnBillboad.onClick.AddListener(OnShowBillboard);
            _btnSearch.onClick.AddListener(OnSearch);
            _btnQuestion.onClick.AddListener(OnQuestion);
            EntityPropertyManager.Instance.AddEventListener(_player, EntityPropertyDefine.duel_rank_show, RefreshScore);
            EntityPropertyManager.Instance.AddEventListener(_player, EntityPropertyDefine.duel_rank_level, RefreshDivision);
            EntityPropertyManager.Instance.AddEventListener(_player, EntityPropertyDefine.duel_match_count, RefreshDivision);
        }

        private void RemoveListener()
        {
            _btnBillboad.onClick.RemoveListener(OnShowBillboard);
            _btnSearch.onClick.RemoveListener(OnSearch);
            _btnQuestion.onClick.RemoveListener(OnQuestion);
            EntityPropertyManager.Instance.RemoveEventListener(_player, EntityPropertyDefine.duel_rank_show, RefreshScore);
            EntityPropertyManager.Instance.RemoveEventListener(_player, EntityPropertyDefine.duel_rank_level, RefreshDivision);
            EntityPropertyManager.Instance.RemoveEventListener(_player, EntityPropertyDefine.duel_match_count, RefreshDivision);
        }

        private void OnShowBillboard()
        {
            DuelManager.Instance.RequestQueryInfo();
        }

        private void OnSearch()
        {
            DuelManager.Instance.RequestSearch();
        }

        private void OnQuestion()
        {
            RuleTipsDataWrapper wrapper = new RuleTipsDataWrapper(MogoLanguageUtil.GetContent(122001), _btnQuestion.GetComponent<RectTransform>(), RuleTipsDirection.BottomRight);
            UIManager.Instance.ShowPanel(PanelIdEnum.RuleTips, wrapper);
        }

        public void Refresh()
        {
            RefreshScore();
            RefreshDivision();
        }

        private void RefreshDivision()
        {
            duel_rank_reward duelRankReward = duel_rank_reward_helper.GetConfig(_player.duel_rank_show, _player.duel_rank_level);
            _division.Visible = false;
            //_hideDivision.Visible = false;
            string name = duel_rank_reward_helper.GetName(duelRankReward, _player.duel_rank_level);
            //if (_player.duel_rank_level < 0)
            //{
                _hideDivision.Visible = true;
                _hideDivision.Refresh(name);
            //}
            //else
            //{
            //    _division.Visible = true;
            //    _division.Refresh(name);
            //}
        }

        private void RefreshScore()
        {
            _textMyScore.CurrentText.text = PlayerAvatar.Player.duel_rank_show.ToString();
        }

        class MyDivision : KContainer
        {
            private StateText _textDivision;
            private StateText _textNoDivision;

            protected override void Awake()
            {
                _textDivision = GetChildComponent<StateText>("Label_txtDuanwei");
                if (GetChild("Label_txtTishi") != null)
                {
                    _textNoDivision = GetChildComponent<StateText>("Label_txtTishi");
                }
            }

            public void Refresh(string divisionStr)
            {
                _textDivision.CurrentText.text = divisionStr;
                if (_textNoDivision != null)
                {
                    _textNoDivision.CurrentText.text = MogoLanguageUtil.GetContent(122107) + 
                        string.Format("({0}/{1})", PlayerAvatar.Player.duel_match_count, duel_data_helper.GetMatchRecordCountLimit());
                }
            }
        }
    }
}
