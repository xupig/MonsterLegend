﻿using ACTSystem;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Utils;

namespace ModuleDuel
{
    public class DuelFightShared : KContainer
    {
        private DuelData.Player _player;

        private bool _hideScoreIsHigher;
        private ACTActor _actor;

        private StateText _textLevel;
        private StateText _textName;
        private StateText _textGuildName;
        private StateText _textFightValue;
        private StateText _textRank;
        private ModelComponent _model;
        private DuelState _duelState;

        private KButton _btnStakeAccumulate;
        private KButton _btnStake;
        private KButton _btnDetail;
        private KButton _btnReady;

        private StateImage _imageStaked;
        private StateText _textOdds;
        private StateText _textOdds1;
        private StateText _textCostItemNum;
        private StateIcon _stateIcon;
        private RewardItem _costItem;

        private DuelData _duelData
        {
            get
            {
                return PlayerDataManager.Instance.DuelData;
            }
        }

        public bool IsLeft;

        protected override void Awake()
        {
            MogoGameObjectHelper.SetZ(GetChild("Container_front").transform, -1000);
            _model = AddChildComponent<ModelComponent>("Container_model");
            _duelState = AddChildComponent<DuelState>("Container_front/Container_zhunbeizhuangtai");
            _textLevel = GetChildComponent<StateText>("Label_txtDengji");
            _textName = GetChildComponent<StateText>("Label_txtWanjiamingziqigezi");
            _textGuildName = GetChildComponent<StateText>("Label_txtGonghuimingzi");
            _textFightValue = GetChildComponent<StateText>("Label_txtZhanli");
            _textRank = GetChildComponent<StateText>("Label_txtJuedoupaihang");

            _btnDetail = GetChildComponent<KButton>("Container_front/Button_jianshao");
            _btnStakeAccumulate = GetChildComponent<KButton>("Container_front/Button_jiazhu");
            _btnStakeAccumulate.interactable = false;
            _btnStake = GetChildComponent<KButton>("Container_front/Button_xiazhu");
            _stateIcon = GetChildComponent<StateIcon>("Container_front/Button_xiazhu/stateIcon");
            _textCostItemNum = GetChildComponent<StateText>("Container_front/Button_xiazhu/number");
            _btnReady = GetChildComponent<KButton>("Container_front/Button_zhunbei");

            _imageStaked = GetChildComponent<StateImage>("Container_front/Image_yixiazhu");
            _textOdds = GetChildComponent<StateText>("Container_front/Label_txtPeilv");
            _textOdds1 = GetChildComponent<StateText>("Container_front/Label_txtPeilv1");
            _costItem = AddChildComponent<RewardItem>("Container_front/Container_cost");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnDetail.onClick.AddListener(OnShowDetail);
            _btnStake.onClick.AddListener(OnStake);
            _btnReady.onClick.AddListener(OnReady);
        }

        private void RemoveListener()
        {
            _btnDetail.onClick.RemoveListener(OnShowDetail);
            _btnStake.onClick.RemoveListener(OnStake);
            _btnReady.onClick.RemoveListener(OnReady);
        }

        protected override void OnEnable()
        {
            AddEnableListener();
        }

        protected override void OnDisable()
        {
            RemoveDisableListener();
        }

        private void AddEnableListener()
        {
            EventDispatcher.AddEventListener<PbOfflineData>(RankingListEvents.GET_RANK_PLAYER_FACADE_INFO, RefreshMemberFacade);
        }

        private void RemoveDisableListener()
        {
            EventDispatcher.RemoveEventListener<PbOfflineData>(RankingListEvents.GET_RANK_PLAYER_FACADE_INFO, RefreshMemberFacade);
        }

        private void RefreshMemberFacade(PbOfflineData data)
        {
            data.facade = MogoProtoUtils.ParseByteArrToString(data.facade_bytes);
            if (_actor != null && _player.PbDuelTarget.vocation == data.vocation && data.facade != null)
            {
                var avatarModelData = ModelEquipTools.CreateAvatarModelData((Vocation)data.vocation, data.facade);
                var clothInfo = avatarModelData.GetEquipInfo(ACTEquipmentType.Cloth);
                _actor.equipController.equipCloth.PutOn(clothInfo.equipID, clothInfo.particle, clothInfo.flow);
                _actor.equipController.equipWeapon.PutOn(avatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).equipID);
                _actor.equipController.equipWing.onLoadEquipFinished = OnWingGoLoaded;
                _actor.equipController.equipWing.PutOn(avatarModelData.GetEquipInfo(ACTEquipmentType.Wing).equipID);
            }
        }

        private void OnWingGoLoaded()
        {
            ModelComponentUtil.ChangeUIModelParam(_actor.gameObject);
        }

        private void OnShowDetail()
        {
            PanelIdEnum.PlayerInfoDetail.Show(_player.PbDuelTarget.dbid);
        }

        private void OnStake()
        {
            PbDuelTarget pbDuelTarget = _player.PbDuelTarget;
            EventDispatcher.TriggerEvent<ulong, string, bool>(DuelEvents.Stake, pbDuelTarget.dbid, pbDuelTarget.name, _hideScoreIsHigher);
        }

        private void OnReady()
        {
            DuelManager.Instance.RequestReady();
        }

        public void RefreshFight(DuelData.Player player, int stakeNum)
        {
            _player = player;
            HideStake();
            MogoGameObjectHelper.SetButtonLabel(_btnStakeAccumulate, stakeNum.ToString());
            RefreshPlayerInfo();
            RefreshFightState();
            RefreshFightBtn();
        }

        private void HideStake()
        {
            _costItem.Visible = false;
            _btnStake.Visible = false;
            _textOdds.Clear();
            _textOdds1.Clear();
            _imageStaked.Visible = false;
        }

        private void RefreshPlayerInfo()
        {
            PbDuelTarget pbDuelTarget = _player.PbDuelTarget;
            _textName.CurrentText.text = pbDuelTarget.name;
            _textLevel.CurrentText.text = string.Format("LV:{0}", pbDuelTarget.level);
            _textGuildName.CurrentText.text = pbDuelTarget.guild_name;
            _textFightValue.CurrentText.text = MogoLanguageUtil.GetContent(122079, pbDuelTarget.fight_force);
            _textRank.CurrentText.text = MogoLanguageUtil.GetContent(122080, duel_rank_reward_helper.GetName(pbDuelTarget.rank_show, pbDuelTarget.rank_level));
            if (_player.InitPlayerModel == false)
            {
                _player.InitPlayerModel = true;
                if (pbDuelTarget.dbid == PlayerAvatar.Player.dbid)
                {
                    _model.LoadPlayerModel();
                }
                else
                {
                    _model.LoadAvatarModel((Vocation)pbDuelTarget.vocation, "", (actor) => { _actor = actor; });
                    RankingListManager.Instance.GetRankPlayerFacadeInfo(pbDuelTarget.dbid);
                }
            }
        }

        private void RefreshFightState()
        {
            if (_player.State == DuelData.PlayerState.responsingFight && _player.IsMe)
            {
                _duelState.Visible = false;
            }
            else
            {
                _duelState.Visible = true;
                _duelState.SetState(_player.State);
            }
        }

        private void RefreshFightBtn()
        {
            if (_player.State == DuelData.PlayerState.preparing && _player.IsMe)
            {
                _btnReady.Visible = true;
            }
            else
            {
                _btnReady.Visible = false;
            }
        }

        public void RefreshStake(DuelData.Player player, int stakeNum, bool hideScoreIsHigher)
        {
            _player = player;
            _hideScoreIsHigher = hideScoreIsHigher;
            RefreshStakeState(stakeNum, hideScoreIsHigher);
            RefreshPlayerInfo();
            HideFightState();
        }

        private void RefreshStakeState(int stakeNum, bool hideScoreIsHigher)
        {
            _btnStake.Visible = false;
            _textOdds.Clear();
            _textOdds1.Clear();
            _costItem.Visible = false;
            _imageStaked.Visible = false;
            _btnStakeAccumulate.Visible = true;
            MogoGameObjectHelper.SetButtonLabel(_btnStakeAccumulate, stakeNum.ToString());
            int[] array;
            switch (_player.State)
            {
                case DuelData.PlayerState.unstake:
                    _btnStake.Visible = true;
                    if (_duelData.IsLuckMode)
                    {
                        BaseItemData baseItemData = duel_data_helper.GetLuckModeCostItem();
                        _stateIcon.SetIcon(baseItemData.Icon);
                        _textCostItemNum.ChangeAllStateText("x" + baseItemData.StackCount);
                    }
                    else
                    {
                        _stateIcon.SetIcon(item_helper.GetIcon(duel_gamble_helper.GetCostItemId(_duelData.BetId)));
                        array = duel_gamble_helper.GetOdds(hideScoreIsHigher, _duelData.BetId);
                        _textOdds1.CurrentText.text = MogoLanguageUtil.GetContent(122135, array[0], array[1]);
                        _textCostItemNum.ChangeAllStateText("x" + array[0]);
                    }
                    break;
                case DuelData.PlayerState.staked:
                    _imageStaked.Visible = true;
                    if (_duelData.IsLuckMode)
                    {
                        _costItem.Visible = true;
                        _costItem.Data = duel_data_helper.GetLuckModeCostItem();
                    }
                    else
                    {
                        array = duel_gamble_helper.GetOdds(hideScoreIsHigher, _duelData.BetId);
                        _textOdds.CurrentText.text = MogoLanguageUtil.GetContent(122135, array[0], array[1]);
                    }
                    break;
            }
        }

        private void HideFightState()
        {
            _duelState.Visible = false;
            _btnReady.Visible = false;
        }

        class DuelState : KContainer
        {
            private StateImage _imageFight;
            private StateImage _imageWaitForSure;
            private StateImage _imageWaiting;
            private StateImage _imageReady;

            protected override void Awake()
            {
                _imageFight = GetChildComponent<StateImage>("Image_faqijuedou");
                _imageWaitForSure = GetChildComponent<StateImage>("Image_querenzhong");
                _imageWaiting = GetChildComponent<StateImage>("Image_zhunbei"); 
                _imageReady = GetChildComponent<StateImage>("Image_yizhunbei");
            }

            public void SetState(DuelData.PlayerState state)
            {
                _imageFight.Visible = false;
                _imageWaiting.Visible = false;
                _imageWaitForSure.Visible = false;
                _imageReady.Visible = false;
                switch (state)
                {
                    case DuelData.PlayerState.fight:
                        _imageFight.Visible = true;
                        break;
                    case DuelData.PlayerState.responsingFight:
                        _imageWaitForSure.Visible = true;
                        break;
                    case DuelData.PlayerState.preparing:
                        _imageWaiting.Visible = true;
                        break;
                    case DuelData.PlayerState.prepared:
                        _imageReady.Visible = true;
                        break;
                }
            }
        }
    }
}
