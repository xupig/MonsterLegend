﻿using Common.Data;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleDuel
{
    public class DuelFightRight : KContainer
    {
        private int _stakeNum;
        private bool _hideScoreIsHigher;

        private DuelFightShared _shared;
        private KButton _btnAgree;
        private KButton _btnReject;

        private DuelData _duelData
        {
            get
            {
                return PlayerDataManager.Instance.DuelData;
            }
        }

        protected override void Awake()
        {
            _shared = gameObject.AddComponent<DuelFightShared>();
            _shared.IsLeft = false;
            _btnAgree = GetChildComponent<KButton>("Container_front/Button_tongyi");
            _btnReject = GetChildComponent<KButton>("Container_front/Button_jujue");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnAgree.onClick.AddListener(OnAgree);
            _btnReject.onClick.AddListener(OnReject);
        }

        private void RemoveListener()
        {
            _btnAgree.onClick.RemoveListener(OnAgree);
            _btnReject.onClick.RemoveListener(OnReject);
        }

        private void OnAgree()
        {
            DuelManager.Instance.RequestInviteResp(true);
        }

        private void OnReject()
        {
            switch (_duelData.InviteType)
            {
                case 1:
                    MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(122078), ConfirmAction, CancelAction);
                    break;
                case 2:
                    DuelManager.Instance.RequestInviteResp(false);
                    break;
            }
        }

        private void ConfirmAction()
        {
            DuelManager.Instance.RequestInviteResp(false);
        }

        private void CancelAction()
        {
            MessageBox.CloseMessageBox();
        }

        public void InitFightData()
        {
            _stakeNum = 0;
        }

        public void Fill(uint stakeNum)
        {
            _stakeNum = (int)stakeNum;
        }

        public void InitStakeData()
        {
            _stakeNum = 0;
        }

        public void Fill(uint stakeNum, bool hideScoreIsHigher)
        {
            Fill(stakeNum);
            _hideScoreIsHigher = hideScoreIsHigher;
        }

        public void Refresh()
        {
            _btnAgree.Visible = false;
            _btnReject.Visible = false;
            if (_duelData.InviteType != 0 && _duelData.RightPlayer.State == DuelData.PlayerState.responsingFight)
            {
                _btnAgree.Visible = true;
                _btnReject.Visible = true;
            }

            if (_duelData.IsFightState)
            {
                _shared.RefreshFight(_duelData.RightPlayer, _stakeNum);
            }
            else if (_duelData.IsStakeState)
            {
                _shared.RefreshStake(_duelData.RightPlayer, _stakeNum, _hideScoreIsHigher);
            }
        }
    }
}
