﻿using Common.Data;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleDuel
{
    public class DuelFightLeft : KContainer
    {
        private int _stakeNum;
        private bool _hideScoreIsHigher;
        private DuelData.Player _player;

        private DuelFightShared _shared;

        private DuelData _duelData
        {
            get
            {
                return PlayerDataManager.Instance.DuelData;
            }
        }

        protected override void Awake()
        {
            _shared = gameObject.AddComponent<DuelFightShared>();
            _shared.IsLeft = true;
        }

        public void InitFightData()
        {
            _stakeNum = 0;
        }

        public void Fill(uint stakeNum)
        {
            _stakeNum = (int)stakeNum;
        }

        public void InitStakeData()
        {
            _stakeNum = 0;
        }

        public void Fill(uint stakeNum, bool hideScoreIsHigher)
        {
            Fill(stakeNum);
            _hideScoreIsHigher = hideScoreIsHigher;
        }

        public void Refresh()
        {
            _player = _duelData.LeftPlayer;
            if (_duelData.IsFightState)
            {
                _shared.RefreshFight(_player, _stakeNum);
            }
            else if (_duelData.IsStakeState)
            {
                _shared.RefreshStake(_player, _stakeNum, _hideScoreIsHigher);
            }
        }
    }
}
