﻿using Common.Base;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Mogo.Util;

namespace ModuleDuel
{
    public class DuelSearch : KContainer
    {
        private KButton _btnRefresh;
        private KButton _btnClose;
        private StateText _textNoPlayer;
        private KList _list;

        protected override void Awake()
        {
            _btnRefresh = GetChildComponent<KButton>("Button_shuaxin");
            _btnClose = GetChildComponent<KButton>("Button_close");
            _textNoPlayer = GetChildComponent<StateText>("Label_txtGonghuimingzi");
            _list = GetChildComponent<KList>("List_content");
            _list.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _list.SetGap(0, 50);

            MogoUtils.AdaptScreen(GetChildComponent<StateImage>("ScaleImage_sharedZhezhao"));
            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnRefresh.onClick.AddListener(OnRefresh);
            _btnClose.onClick.AddListener(OnClose);
        }

        private void RemoveListener()
        {
            _btnRefresh.onClick.RemoveListener(OnRefresh);
            _btnClose.onClick.AddListener(OnClose);
        }

        private void OnRefresh()
        {
            DuelManager.Instance.RequestSearch();
        }

        private void OnClose()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.Duel);
        }

        public void Refresh(object data)
        {
            PbDuelInviteTargetList pbDuelInviteTargetList = data as PbDuelInviteTargetList;
            if (pbDuelInviteTargetList.targets.Count == 0)
            {
                _textNoPlayer.Visible = true;
                _list.Visible = false;
            }
            else
            {
                _textNoPlayer.Visible = false;
                _list.Visible = true;
                _list.SetDataList<SearchItem>(pbDuelInviteTargetList.targets, 1);
                RefreshPosition(_list);
            }
        }

        private void RefreshPosition(KList _list)
        {
            _list.RecalculateSize();
            MogoLayoutUtils.SetCenter(_list.gameObject);
        }

        class SearchItem : KList.KListItemBase
        {
            private PbDuelTarget _data;

            private KButton _btnFight;
            private KButton _btnDetail;
            private IconContainer _head;
            private StateText _textName;
            private StateText _textGuildName;
            private StateText _textFightValue;
            private StateText _textRank;
            private StateText _textLevel;

            protected override void Awake()
            {
                _btnFight = GetChildComponent<KButton>("Button_juedou");
                _btnDetail = GetChildComponent<KButton>("Button_xiangxi");
                _head = AddChildComponent<IconContainer>("Container_icon");
                _textName = GetChildComponent<StateText>("Label_txtWanjiamingzi");
                _textGuildName = GetChildComponent<StateText>("Label_txtGonghuimingzi");
                _textFightValue = GetChildComponent<StateText>("Label_txtZhanli");
                _textRank = GetChildComponent<StateText>("Label_txtJuedoupaihang");
                _textLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");

                AddListener();
            }

            public override void Dispose()
            {
                RemoveListener();
            }

            private void AddListener()
            {
                _btnFight.onClick.AddListener(OnFight);
                _btnDetail.onClick.AddListener(OnShowDetail);
            }

            private void RemoveListener()
            {
                _btnFight.onClick.RemoveListener(OnFight);
                _btnDetail.onClick.RemoveListener(OnShowDetail);
            }

            private void OnFight()
            {
                DuelManager.Instance.RequestQueryPlayerInfo(_data.dbid);
            }

            private void OnShowDetail()
            {
                PanelIdEnum.PlayerInfoDetail.Show(_data.dbid);
            }

            public override object Data
            {
                set
                {
                    _data = value as PbDuelTarget;
                    _textName.CurrentText.text = _data.name;
                    _textLevel.CurrentText.text = _data.level.ToString();
                    _textGuildName.CurrentText.text = _data.guild_name;
                    _textFightValue.CurrentText.text = MogoLanguageUtil.GetContent(122079, _data.fight_force);
                    _textRank.CurrentText.text = MogoLanguageUtil.GetContent(122080, duel_rank_reward_helper.GetName(_data.rank_show, _data.rank_level));
                    _head.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_data.vocation));
                }
            }
        }
    }
}
