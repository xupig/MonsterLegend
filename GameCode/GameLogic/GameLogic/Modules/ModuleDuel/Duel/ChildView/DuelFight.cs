﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using Mogo.Util;
using MogoEngine.Events;
using UnityEngine;

namespace ModuleDuel
{
    public class DuelFight : KContainer
    {
        private uint _timerId = TimerHeap.INVALID_ID;
        private uint _stakeRefreshTimerId = TimerHeap.INVALID_ID;

        private bool _isAvailable;
        private uint _luckMoney;
        private ulong _lastRefreshTime;

        private Title _title;
        private LuckTitle _luckTitle;
        private KButton _btnMinimize;
        private KButton _btnWatch;
        private KButton _btnClose;
        private StateText _textNoScore;
        private StateText _textWait;
        private StateText _textWaitCountdown;
        private StateText _textWaitTips;
        private GameObject _goCountdown;
        private StateText _textCountdown;
        private DuelFightLeft _left;
        private DuelFightRight _right;
        public GameObject _goActivityTips;
        private KButton _btnActivityTips;

        private DuelData _duelData
        {
            get
            {
                return PlayerDataManager.Instance.DuelData;
            }
        }

        public KComponentEvent onClose = new KComponentEvent();

        protected override void Awake()
        {
            _luckTitle = AddChildComponent<LuckTitle>("Container_biaoti");
            _title = AddChildComponent<Title>("Container_biaoti02");
            _btnMinimize = GetChildComponent<KButton>("Button_jianshao");
            _btnWatch = GetChildComponent<KButton>("Button_guangzhan");
            _btnClose = GetChildComponent<KButton>("Button_close");
            _textWait = GetChildComponent<StateText>("Container_dengdai/Label_txtTongyidengdai");
            _textWaitCountdown = GetChildComponent<StateText>("Container_dengdai/Label_txtDaojishi");
            _textWaitTips = GetChildComponent<StateText>("Label_txtTishixinxi");
            _textNoScore = GetChildComponent<StateText>("Label_txtTishixinxi02");
            _goCountdown = GetChild("Container_dengdai");
            _textCountdown = GetChildComponent<StateText>("Label_txtDaojishi");
            _left = AddChildComponent<DuelFightLeft>("Container_left");
            _right = AddChildComponent<DuelFightRight>("Container_right");
            _goActivityTips = GetChild("Container_activityTips");
            _btnActivityTips = GetChildComponent<KButton>("Container_activityTips/Button_Gantang");

            MogoUtils.AdaptScreen(GetChildComponent<StateImage>("ScaleImage_sharedZhezhao"));
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
            RemoveRefreshTimer();
            RemoveTimer();
        }

        private void AddListener()
        {
            _btnMinimize.onClick.AddListener(OnMinimize);
            _btnWatch.onClick.AddListener(OnWatch);
            _btnClose.onClick.AddListener(OnClose);
            _btnActivityTips.onClick.AddListener(OnShowActivityTips);
            EventDispatcher.AddEventListener(DuelEvents.RefreshFightView, RefreshView);
            EventDispatcher.AddEventListener<PbDuelMatchStateInfo>(DuelEvents.RefreshStakeView, OnRefreshStakeView);
            EventDispatcher.AddEventListener<PbDuelMatchStateInfo>(DuelEvents.ResponseStake, OnRefreshStakeView);
            EventDispatcher.AddEventListener<PbDuelMatchBetChange>(DuelEvents.RewardChanged, OnRewardChanged);
        }

        private void RemoveListener()
        {
            _btnMinimize.onClick.RemoveListener(OnMinimize);
            _btnWatch.onClick.RemoveListener(OnWatch);
            _btnClose.onClick.RemoveListener(OnClose);
            _btnActivityTips.onClick.RemoveListener(OnShowActivityTips);
            EventDispatcher.RemoveEventListener(DuelEvents.RefreshFightView, RefreshView);
            EventDispatcher.RemoveEventListener<PbDuelMatchStateInfo>(DuelEvents.RefreshStakeView, OnRefreshStakeView);
            EventDispatcher.RemoveEventListener<PbDuelMatchStateInfo>(DuelEvents.ResponseStake, OnRefreshStakeView);
            EventDispatcher.RemoveEventListener<PbDuelMatchBetChange>(DuelEvents.RewardChanged, OnRewardChanged);
        }

        private void OnMinimize()
        {
            Visible = false;
            EventDispatcher.TriggerEvent(DuelEvents.Minimize);
        }

        private void OnWatch()
        {
            MessageBox.ShowFunctionUnrealized();
        }

        private void OnClose()
        {
            onClose.Invoke();
        }

        private void OnShowActivityTips()
        {
            RuleTipsDataWrapper wrapper = new RuleTipsDataWrapper(MogoLanguageUtil.GetContent(122127), _btnActivityTips.GetComponent<RectTransform>(), RuleTipsDirection.Top);
            UIManager.Instance.ShowPanel(PanelIdEnum.RuleTips, wrapper);
        }

        private void OnRewardChanged(PbDuelMatchBetChange pbDuelMatchBetChange)
        {
            if (pbDuelMatchBetChange.match_id == _duelData.CurrentMatchId)
            {
                RefreshTitle();
                _left.Fill(pbDuelMatchBetChange.bet_sponsor_num);
                _right.Fill(pbDuelMatchBetChange.bet_invitee_num);
                RefreshPlayer();
            }
        }

        private void OnRefreshStakeView(PbDuelMatchStateInfo pb)
        {
            _luckMoney = pb.luck_money;
            _left.Fill(pb.bet_sponsor_num);
            _right.Fill(pb.bet_invitee_num);
            Refresh();
        }

        public void InitFightData(object data)
        {
            _isAvailable = (data as PbDuelInviteTrigger).is_rank_mode == 1;
            _left.InitFightData();
            _right.InitFightData();
        }

        public void InitStakeData(object data)
        {
            PbDuelMatchBetInfo pbDuelMatchBetInfo = (data as PbDuelMatchBetInfo);
            _luckMoney = pbDuelMatchBetInfo.match_state.luck_money;
            _isAvailable = true;
            _left.InitStakeData();
            _right.InitStakeData();
        }

        public void RefreshFight(object data)
        {
            Refresh();
        }

        public void RefreshStake(object data)
        {
            PbDuelMatchStateInfo pbDuelMatchStateInfo = (data as PbDuelMatchBetInfo).match_state;
            _left.Fill(pbDuelMatchStateInfo.bet_sponsor_num, pbDuelMatchStateInfo.bet_rank_side == 1);
            _right.Fill(pbDuelMatchStateInfo.bet_invitee_num, pbDuelMatchStateInfo.bet_rank_side == 2);
            Refresh();
        }

        private void RefreshView()
        {
            Refresh();
        }

        public void Refresh()
        {
            RefreshTitle();
            RefreshPlayer();
            RefreshBtn();
            RefreshWaitInfo();
            RefreshActivityTips();
        }

        private void RefreshTitle()
        {
            _title.Visible = false;
            _luckTitle.Visible = false;
            PbDuelMatchBetChange pbDuelMatchBetChange = _duelData.GetPbDuelMatchBetChange(_duelData.CurrentMatchId);
            if (_duelData.IsFightState)
            {
                if (_duelData.IsLuckMode)
                {
                    _luckTitle.Visible = true;
                    _luckTitle.Refresh(pbDuelMatchBetChange);
                }
                else if (pbDuelMatchBetChange != null)
                {
                    _title.Visible = true;
                    _title.Refresh(pbDuelMatchBetChange);
                }
            }
            else if(_duelData.IsStakeState)
            {
                if (_duelData.IsLuckMode)
                {
                    _luckTitle.Visible = true;
                    if(pbDuelMatchBetChange != null)
                    {
                        _luckTitle.Refresh(pbDuelMatchBetChange);
                    }
                    else
                    {
                        _luckTitle.Refresh(_luckMoney);
                    }
                }
            }
        }

        private void RefreshPlayer()
        {
            _left.Refresh();
            _right.Refresh();
        }

        private void RefreshBtn()
        {
            _btnClose.Visible = false;
            _btnMinimize.Visible = false;
            _btnWatch.Visible = false;
            switch (_duelData.DuelState)
            {
                case DuelData.State.preparing:
                case DuelData.State.ready:
                    _btnMinimize.Visible = true;
                    break;
                case DuelData.State.fighting:
                case DuelData.State.stakePreparing:
                case DuelData.State.stakeReady:
                    _btnClose.Visible = true;
                    break;
            }
        }

        private void RefreshWaitInfo()
        {
            switch (_duelData.DuelState)
            {
                case DuelData.State.requestFight:
                    _textWait.CurrentText.text = MogoLanguageUtil.GetContent(122082);
                    _textWaitTips.CurrentText.text = MogoLanguageUtil.GetContent(122083, duel_data_helper.GetPrepareTime());
                    _textNoScore.Clear();
                    if (!_isAvailable)
                    {
                        _textNoScore.CurrentText.text = MogoLanguageUtil.GetContent(122084);//无效局
                    }
                    else if (_duelData.InviteType == 2)
                    {
                        _textNoScore.CurrentText.text = MogoLanguageUtil.GetContent(122085);//可拒绝局
                    }
                    break;
                case DuelData.State.preparing:
                case DuelData.State.stakePreparing:
                    _textWait.CurrentText.text = MogoLanguageUtil.GetContent(122086);
                    _textWaitTips.CurrentText.text = MogoLanguageUtil.GetContent(122087);
                    break;
                case DuelData.State.ready:
                case DuelData.State.stakeReady:
                    _textWait.CurrentText.text = MogoLanguageUtil.GetContent(122088);
                    _textWaitTips.CurrentText.text = MogoLanguageUtil.GetContent(122089);
                    break;
                case DuelData.State.fighting:
                    _textWait.CurrentText.text = MogoLanguageUtil.GetContent(122136);
                    _textWaitTips.Clear();
                    break;
            }
            AddTimer();

            RemoveRefreshTimer();
            if (_duelData.IsStakeState)
            {
                _textNoScore.Clear();
                AddRefreshTimer();
            }
        }

        private void AddTimer()
        {
            if (_timerId == TimerHeap.INVALID_ID)
            {
                _timerId = TimerHeap.AddTimer(0, 1000, OnTick);
                OnTick();
            }
        }

        private void RemoveTimer()
        {
            if (_timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = TimerHeap.INVALID_ID;
            }
        }

        private void AddRefreshTimer()
        {
            if (_stakeRefreshTimerId == TimerHeap.INVALID_ID)
            {
                _lastRefreshTime = Global.serverTimeStamp;
                _stakeRefreshTimerId = TimerHeap.AddTimer(0, 1000, OnRefreshTick);
            }
        }

        private void OnRefreshTick()
        {
            if (_timerId == TimerHeap.INVALID_ID)
            {
                RemoveRefreshTimer();
                return;
            }
            if (Global.serverTimeStamp - _lastRefreshTime > 10000)
            {
                DuelManager.Instance.RequestStakePartInfo(_duelData.CurrentMatchId);
                _lastRefreshTime = Global.serverTimeStamp;
            }
        }

        private void RemoveRefreshTimer()
        {
            if (_stakeRefreshTimerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_stakeRefreshTimerId);
                _stakeRefreshTimerId = TimerHeap.INVALID_ID;
            }
        }

        private void OnTick()
        {
            int leftSecond = _duelData.GetLeftSecond();
            _textWaitCountdown.CurrentText.text = string.Format("{0}s", leftSecond);
            if (leftSecond <= 0)
            {
                if (_duelData.DuelState == DuelData.State.stakePreparing
                    || _duelData.DuelState == DuelData.State.stakeReady)
                {
                    DuelManager.Instance.RequestStakePartInfo(_duelData.CurrentMatchId);
                    _lastRefreshTime = Global.serverTimeStamp;
                }
                if (_duelData.DuelState == DuelData.State.fighting)
                {
                    _textWait.CurrentText.text = MogoLanguageUtil.GetContent(122137);
                    _textWaitCountdown.Clear();
                }
                RemoveTimer();
            }
        }

        private void RefreshActivityTips()
        {
            _goActivityTips.SetActive(false);
            if (_duelData.IsFightState)
            {
                _goActivityTips.SetActive(activity_helper.IsOpen(25));
            }
        }

        public class LuckTitle : KContainer
        {
            private ArtNumberList _artNumberList;
            private ItemGrid _itemGrid;

            protected override void Awake()
            {
                _artNumberList = AddChildComponent<ArtNumberList>("List_numberList");
                _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            }

            public void Refresh(PbDuelMatchBetChange pbDuelMatchBetChange)
            {
                if (pbDuelMatchBetChange != null)
                {
                    _artNumberList.SetNumber((int)pbDuelMatchBetChange.item_num);
                    _itemGrid.SetItemData((int)pbDuelMatchBetChange.item_id);
                }
                else
                {
                    BaseItemData baseItemData = duel_data_helper.GetLuckModeRewardItem();
                    _itemGrid.SetItemData(baseItemData.Id);
                    _artNumberList.SetNumber(baseItemData.StackCount);
                }
            }

            public void Refresh(uint luckMoney)
            {
                BaseItemData baseItemData = duel_data_helper.GetLuckModeRewardItem();
                _itemGrid.SetItemData(baseItemData.Id);
                _artNumberList.SetNumber((int)luckMoney);
            }
        }

        public class Title : KContainer
        {
            private ArtNumberList _artNumberList;
            private ItemGrid _itemGrid;
            private StateText _textTips;

            protected override void Awake()
            {
                _artNumberList = AddChildComponent<ArtNumberList>("List_numberList");
                _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
                _textTips = GetChildComponent<StateText>("Label_txtTishixinxi");
                _textTips.CurrentText.text = MogoLanguageUtil.GetContent(122138);
            }

            public void Refresh(PbDuelMatchBetChange pbDuelMatchBetChange)
            {
                _artNumberList.SetNumber((int)pbDuelMatchBetChange.item_num);
                _itemGrid.SetItemData((int)pbDuelMatchBetChange.item_id);
            }
        }
    }
}
