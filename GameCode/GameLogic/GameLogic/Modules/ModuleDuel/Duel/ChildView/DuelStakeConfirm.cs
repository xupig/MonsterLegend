﻿using Common.Data;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Mogo.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleDuel
{
    public class DuelStakeConfirm : KContainer
    {
        private ulong _dbid;
        private PbDuelMatchStateInfo _data;

        private RewardItem _costItem;
        private RewardItem _awardItem;
        private StateText _textName;
        private KButton _btnCancel;
        private KButton _btnSure;
        private GameObject _goNormal;

        private RewardItem _luckCostItem;
        private StateText _textLuckName;
        private GameObject _goLuck;

        private DuelData _duelData
        {
            get
            {
                return PlayerDataManager.Instance.DuelData;
            }
        }

        protected override void Awake()
        {
            _costItem = AddChildComponent<RewardItem>("Container_normal/Container_cost");
            _awardItem = AddChildComponent<RewardItem>("Container_normal/Container_reward");
            _textName = GetChildComponent<StateText>("Container_normal/Label_txttName");
            _goNormal = GetChild("Container_normal");

            _luckCostItem = AddChildComponent<RewardItem>("Container_luck/Container_cost");
            _textLuckName = GetChildComponent<StateText>("Container_luck/Label_txttName");
            _goLuck = GetChild("Container_luck");

            _btnCancel = GetChildComponent<KButton>("Button_quxiao");
            _btnSure = GetChildComponent<KButton>("Button_queding");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnCancel.onClick.AddListener(OnCancel);
            _btnSure.onClick.AddListener(OnSure);
        }

        private void RemoveListener()
        {
            _btnCancel.onClick.RemoveListener(OnCancel);
            _btnSure.onClick.RemoveListener(OnSure);
        }

        private void OnCancel()
        {
            Visible = false;
        }

        private void OnSure()
        {
            if(_duelData.IsLuckMode)
            {
                DuelManager.Instance.RequestSpecialBet(_duelData.CurrentMatchId, _dbid);
            }
            else
            {
                DuelManager.Instance.RequestBet(_duelData.CurrentMatchId, _dbid);
            }
        }

        public void Refresh(ulong dbid, string name, bool hideScoreIsHigher)
        {
            _dbid = dbid;

            _goNormal.SetActive(false);
            _goLuck.SetActive(false);
            if (_duelData.IsLuckMode)
            {
                _goLuck.SetActive(true);
                _textLuckName.CurrentText.text = name;
                BaseItemData luckBaseItemData = duel_data_helper.GetLuckModeCostItem();
                _luckCostItem.Data = luckBaseItemData;
                return;
            }

            _goNormal.SetActive(true);
            _textName.CurrentText.text = name;
            int[] array = duel_gamble_helper.GetOdds(hideScoreIsHigher, _duelData.BetId);
            int itemId = duel_gamble_helper.GetCostItemId(_duelData.BetId);
            BaseItemData baseItemData = ItemDataCreator.Create(itemId);
            baseItemData.StackCount = array[0];
            _costItem.Data = baseItemData;
            baseItemData = ItemDataCreator.Create(itemId);
            baseItemData.StackCount = array[1];
            _awardItem.Data = baseItemData;
        }
    }
}
