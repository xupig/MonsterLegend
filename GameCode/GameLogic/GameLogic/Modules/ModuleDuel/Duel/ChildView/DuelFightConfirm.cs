﻿using Common.Base;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Mogo.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleDuel
{
    public class DuelFightConfirm : KContainer
    {
        private PbDuelQueryPlayerInfo _pbDuelQueryPlayerInfo;
        private PbDuelTarget _data;

        private KButton _btnFight;
        private KButton _btnCancel;
        private IconContainer _head;
        private StateText _textName;
        private StateText _textGuildName;
        private StateText _textFightValue;
        private StateText _textRank;
        private StateText _textLevel;
        private StateText _textTips;

        protected override void Awake()
        {
            _btnFight = GetChildComponent<KButton>("Button_tiaozhan");
            _btnCancel = GetChildComponent<KButton>("Button_quxiao");
            _head = AddChildComponent<IconContainer>("Container_icon");
            _textName = GetChildComponent<StateText>("Label_txtWanjiamingzi");
            _textGuildName = GetChildComponent<StateText>("Label_txtGonghuimingzi");
            _textFightValue = GetChildComponent<StateText>("Label_txtZhanli");
            _textRank = GetChildComponent<StateText>("Label_txtJuedoupaihang");
            _textLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            _textTips = GetChildComponent<StateText>("Label_txtTishi");

            MogoUtils.AdaptScreen(GetChildComponent<StateImage>("ScaleImage_sharedZhezhao"));

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnFight.onClick.AddListener(OnFight);
            _btnCancel.onClick.AddListener(OnCancel);
        }

        private void RemoveListener()
        {
            _btnFight.onClick.RemoveListener(OnFight);
            _btnCancel.onClick.RemoveListener(OnCancel);
        }

        private void OnFight()
        {
            DuelManager.Instance.RequestInvite(_data.dbid);
        }

        private void OnCancel()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.Duel);
        }

        public void Refresh(object data)
        {
            PbDuelQueryPlayerInfo pbDuelQueryPlayerInfo = data as PbDuelQueryPlayerInfo;
            _pbDuelQueryPlayerInfo = pbDuelQueryPlayerInfo;
            _data = pbDuelQueryPlayerInfo.target;
            _head.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_data.vocation));
            _textName.CurrentText.text = _data.name;
            _textLevel.CurrentText.text = _data.level.ToString();
            _textGuildName.CurrentText.text = _data.guild_name;
            _textFightValue.CurrentText.text = MogoLanguageUtil.GetContent(122079, _data.fight_force);
            _textRank.CurrentText.text = MogoLanguageUtil.GetContent(122080, duel_rank_reward_helper.GetName(_data.rank_show, _data.rank_level));
            _textTips.Clear();
            if (pbDuelQueryPlayerInfo.is_rank_mode == 0)
            {
                _textTips.CurrentText.text = MogoLanguageUtil.GetContent(122091);
            }
            else
            {
                if (pbDuelQueryPlayerInfo.can_refuse_type == 1)
                {
                    _textTips.CurrentText.text = MogoLanguageUtil.GetContent(122092);
                }
            }
        }
    }
}
