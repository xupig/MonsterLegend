﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleDuel
{
    public class DuelPanel : BasePanel
    {
        public DuelSearch _search;
        public DuelFight _fight;
        public DuelFightConfirm _fightConfirm;
        public DuelStakeConfirm _stakeConfirm;

        protected override void Awake()
        {
            _search = AddChildComponent<DuelSearch>("Container_juedousousuo");
            _fight = AddChildComponent<DuelFight>("Container_jueou");
            _fightConfirm = AddChildComponent<DuelFightConfirm>("Container_tichutiaozhan");
            _stakeConfirm = AddChildComponent<DuelStakeConfirm>("Container_stake");
            MogoGameObjectHelper.SetZ(_stakeConfirm.transform, -600);
        }

        private void AddListener()
        {
            _fight.onClose.AddListener(ClosePanel);
            EventDispatcher.AddEventListener(DuelEvents.Maximize, onMaximize);
            EventDispatcher.AddEventListener<ulong, string, bool>(DuelEvents.Stake, OnStake);
            EventDispatcher.AddEventListener<PbDuelMatchStateInfo>(DuelEvents.ResponseStake, OnResponseStake);
        }

        private void RemoveListener()
        {
            _fight.onClose.RemoveListener(ClosePanel);
            EventDispatcher.RemoveEventListener(DuelEvents.Maximize, onMaximize);
            EventDispatcher.RemoveEventListener<ulong, string, bool>(DuelEvents.Stake, OnStake);
            EventDispatcher.RemoveEventListener<PbDuelMatchStateInfo>(DuelEvents.ResponseStake, OnResponseStake);
        }

        private void OnResponseStake(PbDuelMatchStateInfo obj)
        {
            _stakeConfirm.Visible = false;
        }

        private void onMaximize()
        {
            _fight.Visible = true;
            _fight.Refresh();
        }

        private void OnStake(ulong dbid, string name, bool highHideScore)
        {
            _stakeConfirm.Visible = true;
            _stakeConfirm.Refresh(dbid, name, highHideScore);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Duel; }
        }

        public override void OnShow(object data)
        {
            SetData(data);
        }

        public override void SetData(object data)
        {
            AddListener();
            DuelPanelParameter param = (DuelPanelParameter)data;
            HideAll();
            switch (param.View)
            {
                case DuelData.View.fight:
                    _fight.Visible = true;
                    _fight.InitFightData(param.Data);
                    _fight.RefreshFight(param.Data);
                    break;
                case DuelData.View.stake:
                    _fight.Visible = true;
                    _fight.InitStakeData(param.Data);
                    _fight.RefreshStake(param.Data);
                    break;
                case DuelData.View.search:
                    _search.Visible = true;
                    _search.Refresh(param.Data);
                    break;
                case DuelData.View.fightConfirm:
                    _fightConfirm.Visible = true;
                    _fightConfirm.Refresh(param.Data);
                    break;
            }
        }

        private void HideAll()
        {
            _search.Visible = false;
            _fight.Visible = false;
            _fightConfirm.Visible = false;
            _stakeConfirm.Visible = false;
        }

        public override void OnClose()
        {
            RemoveListener();
        }
    }
}
