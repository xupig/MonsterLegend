﻿using Common.Base;
using Common.Events;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;

namespace ModuleDuel
{
    public class DuelModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.DuelEntry, "Container_DuelEntryPanel", MogoUILayer.LayerUIPanel, "ModuleDuel.DuelEntryPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.Duel, "Container_DuelPanel", MogoUILayer.LayerUIPanel, "ModuleDuel.DuelPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.DuelScene, "Container_DuelScenePanel", MogoUILayer.LayerUnderPanel, "ModuleDuel.DuelScenePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);


            EventDispatcher.AddEventListener(DuelEvents.RequestData, OnRequestData);
        }

        private void OnRequestData()
        {
            DuelManager.Instance.RequestQueryInfo();
        }
    }
}
