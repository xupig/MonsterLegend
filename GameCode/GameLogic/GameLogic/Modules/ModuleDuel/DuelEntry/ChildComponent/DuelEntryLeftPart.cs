﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleDuel
{
    public class DuelEntryLeftPart : KContainer
    {
        private KButton _btnTips;
        private SelfDivisiion _selfDivision;
        private NextDivision _nextDivision;
        private GameObject _goTwoDivision;
        private FinalDivision _finalDivision;

        protected override void Awake()
        {
            _goTwoDivision = GetChild("Container_twoDivision");
            _btnTips = GetChildComponent<KButton>("Container_twoDivision/Button_Gantang");
            _selfDivision = AddChildComponent<SelfDivisiion>("Container_twoDivision/Container_selfDivision");
            _nextDivision = AddChildComponent<NextDivision>("Container_twoDivision/Container_nextDivision");
            _finalDivision = AddChildComponent<FinalDivision>("Container_oneDivision");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnTips.onClick.AddListener(OnShowTips);
        }

        private void RemoveListener()
        {
            _btnTips.onClick.RemoveListener(OnShowTips);
        }

        private void OnShowTips()
        {
            RuleTipsDataWrapper wrapper = new RuleTipsDataWrapper(MogoLanguageUtil.GetContent(122001), _btnTips.GetComponent<RectTransform>(), RuleTipsDirection.TopRight);
            UIManager.Instance.ShowPanel(PanelIdEnum.RuleTips, wrapper);
        }

        public void Refresh(uint myScore, bool noDivisionNow, duel_rank_reward duelRankReward = null, duel_rank_reward nextDuelRankReward = null)
        {
            _goTwoDivision.SetActive(false);
            _finalDivision.Visible = false;
            if (noDivisionNow)
            {
                _finalDivision.Visible = true;
                _finalDivision.Refresh(myScore, noDivisionNow);
            }
            else if (nextDuelRankReward == null)
            {
                _finalDivision.Visible = true;
                _finalDivision.Refresh(myScore, noDivisionNow, duelRankReward);
            }
            else
            {
                _goTwoDivision.SetActive(true);
                _selfDivision.Refresh(myScore, duelRankReward);
                _nextDivision.Refresh(nextDuelRankReward);
            }
        }

        class SelfDivisiion : KContainer
        {
            private IconContainer _icon;
            private StateText _textScore;

            protected override void Awake()
            {
                _icon = AddChildComponent<IconContainer>("Container_icon");
                GetChildComponent<StateText>("Label_txtWodeduanwei").CurrentText.text = MogoLanguageUtil.GetContent(122093);
                _textScore = GetChildComponent<StateText>("Label_txtWodejifen");
            }

            public void Refresh(uint myScore, duel_rank_reward duelRankReward)
            {
                _icon.SetIcon(duel_rank_reward_helper.GetIconId(duelRankReward));
                _textScore.CurrentText.text = MogoLanguageUtil.GetContent(122094, myScore);
            }
        }

        class NextDivision : KContainer
        {
            private IconContainer _icon;
            private StateText _textScore;
            private Locater _locater;

            protected override void Awake()
            {
                _icon = AddChildComponent<IconContainer>("Container_icon");
                GetChildComponent<StateText>("Label_txtXiayijieduan").CurrentText.text = MogoLanguageUtil.GetContent(122095);
                _textScore = GetChildComponent<StateText>("Label_txtSuoxujifen");
                _locater = AddChildComponent<Locater>("Label_txtSuoxujifen");
            }

            public void Refresh(duel_rank_reward duelRankReward)
            {
                _icon.SetIcon(duel_rank_reward_helper.GetIconId(duelRankReward));
                string needStr = MogoLanguageUtil.GetContent(122096, duel_rank_reward_helper.GetMinScoreRecommand(duelRankReward));
                _locater.Y = -186f;
                if(duelRankReward.__rank.Count != 0)
                {
                    needStr += MogoLanguageUtil.GetContent(122097, duel_rank_reward_helper.GetMinRank(duelRankReward));
                    _locater.Y = -170f;
                }
                _textScore.CurrentText.text = needStr;
            }
        }

        class FinalDivision : KContainer
        {
            private IconContainer _icon;
            private StateText _textDesc;
            private StateText _textScore;
            private StateText _textNoDivision;
            private KButton _btnTips;

            protected override void Awake()
            {
                _icon = AddChildComponent<IconContainer>("Container_icon");
                _textDesc = GetChildComponent<StateText>("Label_txtZuigao");
                _textScore = GetChildComponent<StateText>("Label_txtSuoxujifen");
                _textNoDivision = GetChildComponent<StateText>("Label_txtNoDivision");
                _btnTips = GetChildComponent<KButton>("Button_Gantang");
                AddListener();
            }

            protected override void OnDestroy()
            {
                RemoveListener();
            }

            private void AddListener()
            {
                _btnTips.onClick.AddListener(OnShowTips);
            }

            private void RemoveListener()
            {
                _btnTips.onClick.RemoveListener(OnShowTips);
            }

            private void OnShowTips()
            {
                RuleTipsDataWrapper wrapper = new RuleTipsDataWrapper(MogoLanguageUtil.GetContent(122001), _btnTips.GetComponent<RectTransform>(), RuleTipsDirection.TopRight);
                UIManager.Instance.ShowPanel(PanelIdEnum.RuleTips, wrapper);
            }

            public void Refresh(uint myScore, bool noDivisionNow, duel_rank_reward duelRankReward = null)
            {
                if (noDivisionNow)
                {
                    _textDesc.CurrentText.text = MogoLanguageUtil.GetContent(122093);
                    _icon.RemoveSprite();
                    _textNoDivision.Visible = true;
                }
                else
                {
                    _textDesc.CurrentText.text = MogoLanguageUtil.GetContent(122098);
                    _icon.SetIcon(duel_rank_reward_helper.GetIconId(duelRankReward));
                    _textNoDivision.Visible = false;
                }
                _textScore.CurrentText.text = MogoLanguageUtil.GetContent(122094, myScore);
            }
        }
    }
}
