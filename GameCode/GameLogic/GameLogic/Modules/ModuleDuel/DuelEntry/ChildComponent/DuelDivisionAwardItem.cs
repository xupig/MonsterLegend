﻿using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleDuel
{
    public class DuelDivisionAwardItem : KList.KListItemBase
    {
        private RewardItemList _firstUpAward;
        private RewardItemList _seasonAward;
        private StateText _textScore;
        private StateText _textDivision;

        protected override void Awake()
        {
            _firstUpAward = AddChildComponent<RewardItemList>("Container_firstUpAward");
            _seasonAward = AddChildComponent<RewardItemList>("Container_seasonAward");
            _textScore = GetChildComponent<StateText>("Label_txtJuedingjifen");
            _textDivision = GetChildComponent<StateText>("Label_txtDuanwei");
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                duel_rank_reward duelRankReward = value as duel_rank_reward;
                _textScore.CurrentText.text = duelRankReward.__score[0];
                _textDivision.CurrentText.text = duel_rank_reward_helper.GetNameInReward(duelRankReward);
                _firstUpAward.SetData(duelRankReward.__achieve_reward);
                _seasonAward.SetData(duelRankReward.__month_reward);
            }
        }
    }
}
