﻿using Common.Base;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleDuel
{
    public class DuelEntryPanel : BasePanel
    {
        private DuelEntryView _entryView;
        private DuelDivisionAward _divisionAward;
         
        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _entryView = AddChildComponent<DuelEntryView>("Container_juedou");
            _divisionAward = AddChildComponent<DuelDivisionAward>("Container_jifen");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _entryView.onShowDivisionAward.AddListener(OnShowDivisionAward);
            _divisionAward.onCloseDivisionAward.AddListener(OnCloseDivisionAward);
        }

        private void RemoveListener()
        {
            _entryView.onShowDivisionAward.RemoveListener(OnShowDivisionAward);
            _divisionAward.onCloseDivisionAward.RemoveListener(OnCloseDivisionAward);
        }

        private void OnShowDivisionAward()
        {
            _divisionAward.Visible = true;
            _entryView.Visible = false;
        }

        private void OnCloseDivisionAward()
        {
            _divisionAward.Visible = false;
            _entryView.Visible = true;
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.DuelEntry; }
        }

        public override void OnShow(object data)
        {
            PbDuelQueryInfo pbDuelQueryInfo = data as PbDuelQueryInfo;
            _entryView.Refresh(pbDuelQueryInfo);
        }

        public override void OnClose()
        {
        }
    }
}
