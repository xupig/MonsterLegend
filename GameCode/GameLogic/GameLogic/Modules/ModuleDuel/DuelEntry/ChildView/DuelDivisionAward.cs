﻿using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleDuel
{
    public class DuelDivisionAward : KContainer
    {
        private KScrollView _scrollView;
        private KList _list;
        private KButton _btnClose;

        public KComponentEvent onCloseDivisionAward = new KComponentEvent();

        protected override void Awake()
        {
            _btnClose = GetChildComponent<KButton>("Button_close");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_zhanlibang");
            _list = GetChildComponent<KList>("ScrollView_zhanlibang/mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);

            GetChildComponent<StateText>("Label_txtMiaoshu").CurrentText.text = MogoLanguageUtil.GetContent(122108);

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(OnClose);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(OnClose);
        }

        protected override void OnEnable()
        {
            _list.SetDataList<DuelDivisionAwardItem>(duel_rank_reward_helper.ConfigList, 1);
        }

        private void OnClose()
        {
            onCloseDivisionAward.Invoke();
        }
    }
}
