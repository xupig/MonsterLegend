﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleDuel
{
    public class DuelEntryView : KContainer
    {
        private DuelEntryLeftPart _leftPart;
        private RightPart _rightPart;
        private KButton _btnTips;
        private KButton _btnEnter;
        private KButton _btnBillBoard;
        private StateText _textLeftTime;

        public KComponentEvent onShowDivisionAward = new KComponentEvent();

        private DuelData _data
        {
            get
            {
                return PlayerDataManager.Instance.DuelData;
            }
        }

        private PlayerAvatar _player
        {
            get
            {
                return PlayerAvatar.Player;
            }
        }

        protected override void Awake()
        {
            _leftPart = AddChildComponent<DuelEntryLeftPart>("Container_left");
            _rightPart = AddChildComponent<RightPart>("Container_right");
            _btnTips = GetChildComponent<KButton>("Button_Gantang");
            _btnEnter = GetChildComponent<KButton>("Button_guangzhan");
            _btnBillBoard = GetChildComponent<KButton>("Button_juedoubang");
            _textLeftTime = GetChildComponent<StateText>("Label_txtSaijishengyushijian");
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnTips.onClick.AddListener(OnShowTips);
            _btnEnter.onClick.AddListener(OnEnter);
            _btnBillBoard.onClick.AddListener(OnShowBillBoard);
            EventDispatcher.AddEventListener<PbDuelQueryInfo>(DuelEvents.RefreshEntryView, Refresh);
        }

        private void RemoveListener()
        {
            _btnTips.onClick.RemoveListener(OnShowTips);
            _btnEnter.onClick.RemoveListener(OnEnter);
            _btnBillBoard.onClick.RemoveListener(OnShowBillBoard);
            EventDispatcher.RemoveEventListener<PbDuelQueryInfo>(DuelEvents.RefreshEntryView, Refresh);
        }

        private void OnShowTips()
        {
            onShowDivisionAward.Invoke();
        }

        private void OnEnter()
        {
            DuelManager.Instance.RequestEnter();
        }

        private void OnShowBillBoard()
        {
            view_helper.OpenView(42);
        }

        public void Refresh(PbDuelQueryInfo pbDuelQueryInfo)
        {
            //Debug.LogError(string.Format("rank:{0} score:{1} hideScore:{2} stageId:{3} hasReward:{4}",
            //    _player.duel_rank_level, _player.duel_rank_show, _player.duel_rank_true, pbDuelQueryInfo.max_rank_stage_id, pbDuelQueryInfo.has_award));
            RefreshLeft();
            RefreshRight(pbDuelQueryInfo);
            _textLeftTime.CurrentText.text = MogoLanguageUtil.GetContent(122099, GetLeftTime());
        }

        private void RefreshLeft()
        {
            bool noDivisionNow = _player.duel_rank_level < 0;
            if (noDivisionNow)
            {
                _leftPart.Refresh(_player.duel_rank_show, noDivisionNow);
            }
            else
            {
                duel_rank_reward duelRankReward = duel_rank_reward_helper.GetConfig(_player.duel_rank_show, _player.duel_rank_level);
                duel_rank_reward nextDuelRankReward = duel_rank_reward_helper.GetNext(duelRankReward);
                _leftPart.Refresh(_player.duel_rank_show, noDivisionNow, duelRankReward, nextDuelRankReward);
            }
        }

        private void RefreshRight(PbDuelQueryInfo pbDuelQueryInfo)
        {
            bool noDivisionInHistory = NoDivisionInHistory(pbDuelQueryInfo);
            bool hasAward = pbDuelQueryInfo.has_award == 1;
            if (noDivisionInHistory)
            {
                _rightPart.Refresh(noDivisionInHistory, hasAward);
            }
            else
            {
                duel_rank_reward historyDuelRankReward = duel_rank_reward_helper.GetConfig((int)pbDuelQueryInfo.max_rank_stage_id);
                duel_rank_reward historyNextDuelRankReward = duel_rank_reward_helper.GetNext(historyDuelRankReward);
                _rightPart.Refresh(noDivisionInHistory, hasAward, historyDuelRankReward, historyNextDuelRankReward);
            }
        }

        public bool NoDivisionInHistory(PbDuelQueryInfo pbDuelQueryInfo)
        {
            return pbDuelQueryInfo.max_rank_stage_id == 0;
        }

        private string GetLeftTime()
        {
            DateTime nextMonthDate = MogoTimeUtil.GetEndDateOfMonth(Global.serverTimeStampSecond);
            DateTime currentDate = MogoTimeUtil.ServerTimeStamp2LocalTime(Global.serverTimeStampSecond);
            int totalSecond = Convert.ToInt32((nextMonthDate - currentDate).TotalSeconds);
            return MogoTimeUtil.ToUniversalTime(totalSecond, MogoLanguageUtil.GetContent(122100));
        }

        class RightPart : KContainer
        {
            public const int CloseBoxIconId = 6898;
            public const int OpenBoxIconId = 6899;

            private GameObject _twoDivisionAward;
            private CurrentDivisionAward _divisionAward;
            private NextDivisionAward _nextAward;
            private FinalDivisionAward _fianlAward;

            protected override void Awake()
            {
                _twoDivisionAward = GetChild("Container_twoDivision");
                _divisionAward = AddChildComponent<CurrentDivisionAward>("Container_twoDivision/Container_selfDivision");
                _nextAward = AddChildComponent<NextDivisionAward>("Container_twoDivision/Container_nextDivision");
                _fianlAward = AddChildComponent<FinalDivisionAward>("Container_oneDivision");
            }

            public void Refresh(bool noDivisionInHistory, bool hasAward, duel_rank_reward duelRankReward = null, duel_rank_reward nextDuelRankReward = null)
            {
                _twoDivisionAward.gameObject.SetActive(false);
                _fianlAward.Visible = false;
                if (noDivisionInHistory)
                {
                    _fianlAward.Visible = true;
                    _fianlAward.Refresh(noDivisionInHistory, hasAward);
                }
                else if (nextDuelRankReward == null)
                {
                    _fianlAward.Visible = true;
                    _fianlAward.Refresh(noDivisionInHistory, hasAward, duelRankReward);
                }
                else
                {
                    _twoDivisionAward.gameObject.SetActive(true);
                    _divisionAward.Refresh(hasAward, duelRankReward);
                    _nextAward.Refresh(nextDuelRankReward);
                }
            }
        }

        class CurrentDivisionAward : KContainer
        {
            private IconContainer _icon;
            private KDummyButton _btnGetAward;
            private StateText _textDivision;
            private KParticle _particle;

            protected override void Awake()
            {
                _particle = AddChildComponent<KParticle>("Container_box/fx_ui_24_2_baoxianglingqu");
                _icon = AddChildComponent<IconContainer>("Container_box/Container_icon");
                _btnGetAward = AddChildComponent<KDummyButton>("Container_box");
                _textDivision = GetChildComponent<StateText>("Label_txtZuigaoduanwei");

                AddListener();
            }

            protected override void OnDestroy()
            {
                RemoveListener();
            }

            private void AddListener()
            {
                _btnGetAward.onClick.AddListener(OnGetAward);
            }

            private void RemoveListener()
            {
                _btnGetAward.onClick.RemoveListener(OnGetAward);
            }

            private void OnGetAward()
            {
                DuelManager.Instance.RequestGotReward();
            }

            public void Refresh(bool hasReward, duel_rank_reward duelRankReward)
            {
                _particle.Visible = false;
                TweenTreasureBox.Stop(_btnGetAward.gameObject);
                if (duelRankReward != null)
                {
                    _textDivision.CurrentText.text = MogoLanguageUtil.GetContent(122101,
                        ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GOLDEN, duel_rank_reward_helper.GetNameInReward(duelRankReward)));
                }
                else
                {
                    _textDivision.CurrentText.text = MogoLanguageUtil.GetContent(122102);
                }
                _icon.SetIcon(hasReward ? RightPart.CloseBoxIconId : RightPart.OpenBoxIconId);
                if (hasReward)
                {
                    _particle.Visible = true;
                    _particle.transform.localPosition = new Vector3(17, 0, 0);
                    TweenTreasureBox.Begin(_btnGetAward.gameObject);
                }
                _btnGetAward.enabled = hasReward;
            }
        }

        class NextDivisionAward : KContainer
        {
            private duel_rank_reward _duelRankReward;

            private IconContainer _icon;
            private KDummyButton _btn;

            protected override void Awake()
            {
                _icon = AddChildComponent<IconContainer>("Container_icon");
                _btn = AddChildComponent<KDummyButton>("Container_icon");

                AddListener();
            }

            protected override void OnDestroy()
            {
                RemoveListener();
            }

            private void AddListener()
            {
                _btn.onClick.AddListener(OnShowTips);
            }

            private void RemoveListener()
            {
                _btn.onClick.RemoveListener(OnShowTips);
            }

            private void OnShowTips()
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.RewardBox, _duelRankReward.__achieve_reward);
            }

            public void Refresh(duel_rank_reward duelRankReward)
            {
                _duelRankReward = duelRankReward;
                _icon.SetIcon(RightPart.CloseBoxIconId);
            }
        }

        class FinalDivisionAward : KContainer
        {
            private duel_rank_reward _duelRankReward;
            private bool _hasReward;

            private IconContainer _icon;
            private StateText _textDivision;
            private StateText _textNoDivision;
            private KDummyButton _btnGetAward;
            private KParticle _particle;

            protected override void Awake()
            {
                _icon = AddChildComponent<IconContainer>("Container_box/Container_icon");
                _particle = AddChildComponent<KParticle>("Container_box/fx_ui_24_2_baoxianglingqu");
                _btnGetAward = AddChildComponent<KDummyButton>("Container_box");
                _textDivision = GetChildComponent<StateText>("Label_txtZuigaoduanwei");
                _textNoDivision = GetChildComponent<StateText>("Label_txtNoDivision");

                AddListener();
            }

            protected override void OnDestroy()
            {
                RemoveListener();
            }

            private void AddListener()
            {
                _btnGetAward.onClick.AddListener(OnGetAward);
            }

            private void RemoveListener()
            {
                _btnGetAward.onClick.RemoveListener(OnGetAward);
            }

            private void OnGetAward()
            {
                if (_hasReward)
                {
                    DuelManager.Instance.RequestGotReward();
                }
                else
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.RewardBox, _duelRankReward.__achieve_reward);
                }
            }

            public void Refresh(bool noDivisionInHistory, bool hasReward, duel_rank_reward duelRankReward = null)
            {
                _duelRankReward = duelRankReward;
                _hasReward = hasReward;
                _particle.Visible = false;
                TweenTreasureBox.Stop(_btnGetAward.gameObject);
                if (noDivisionInHistory)
                {
                    _textNoDivision.Visible = true;
                    _icon.RemoveSprite();
                    _textDivision.CurrentText.text = MogoLanguageUtil.GetContent(122103);
                }
                else
                {
                    _textNoDivision.Visible = false;
                    _icon.SetIcon(hasReward ? RightPart.CloseBoxIconId : RightPart.OpenBoxIconId);
                    if (hasReward)
                    {
                        _particle.Visible = true;
                        _particle.transform.localPosition = new Vector3(17, 0, 0);
                        TweenTreasureBox.Begin(_btnGetAward.gameObject);
                    }
                    _textDivision.CurrentText.text = MogoLanguageUtil.GetContent(122104,
                    ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GOLDEN, duel_rank_reward_helper.GetNameInReward(duelRankReward)));
                }
            }
        }
    }
}
