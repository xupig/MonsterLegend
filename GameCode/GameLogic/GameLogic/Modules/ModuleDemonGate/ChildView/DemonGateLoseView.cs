﻿using Common.Base;
using Common.Data;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleDemonGate
{
    public class DemonGateLoseView : KContainer
    {
        private const int MAX_COUNT_DOWN_SECOND = 10;
        
        private uint _timerId = TimerHeap.INVALID_ID;
        private ulong _endTimeStamp;

        private KButton _btnSure;
        private StateText _textCountDown;

        protected override void Awake()
        {
            _btnSure = GetChildComponent<KButton>("Button_queding");
            _textCountDown = GetChildComponent<StateText>("Label_txtTuichu");
            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnSure.onClick.AddListener(OnSure);
        }

        private void RemoveListener()
        {
            _btnSure.onClick.RemoveListener(OnSure);
        }

        protected override void OnEnable()
        {
            AddTimer();
        }

        protected override void OnDisable()
        {
            RemoveTimer();
        }

        private void OnSure()
        {
            GameSceneManager.GetInstance().LeaveCombatScene();
        }

        private void AddTimer()
        {
            if (_timerId == TimerHeap.INVALID_ID)
            {
                _endTimeStamp = Global.serverTimeStamp + MAX_COUNT_DOWN_SECOND * 1000;
                _timerId = TimerHeap.AddTimer(0, 1000, OnTick);
            }
        }

        private void RemoveTimer()
        {
            if (_timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = TimerHeap.INVALID_ID;
            }
        }

        private void OnTick()
        {
            int leftSecond = Mathf.CeilToInt((_endTimeStamp - Global.serverTimeStamp) / 1000f);
            _textCountDown.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(72800), 
                ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, leftSecond.ToString()));
            if (leftSecond <= 0)
            {
                OnFinish();
            }
        }

        private void OnFinish()
        {
            RemoveTimer();
            OnSure();
        }
    }
}
