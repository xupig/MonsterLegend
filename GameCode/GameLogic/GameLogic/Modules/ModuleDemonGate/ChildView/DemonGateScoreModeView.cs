﻿#region 模块信息
/*==========================================
// 文件名：DemonGateScoreModeView
// 命名空间: GameLogic.GameLogic.Modules.ModuleDemonGate.ChildView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/9 16:06:22
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;
namespace ModuleDemonGate
{
    public class DemonGateScoreModeView : KContainer
    {
        private GameObject _maskGo;
        private StateText _diffultyTxt;
        private string _diffultyTemplate;
        private KContainer _contentContainer;

        private DemonGateScoreView _scoreView;
        private DemonGateRewardView _rewardView;

        private KButton _transformBtn;
        private KButton _fightBtn;
        private StateText _descTxt;

        private int targetNpcId;

        protected override void Awake()
        {
            _maskGo = GetChild("ScaleImage_mask");
            KScrollRect scrollRect = _maskGo.AddComponent<KScrollRect>();
            _contentContainer = GetChildComponent<KContainer>("Container_content");
            _contentContainer.transform.SetParent(_maskGo.transform);
            scrollRect.content = _contentContainer.GetComponent<RectTransform>();
            scrollRect.horizontal = false;

            _diffultyTxt = _contentContainer.GetChildComponent<StateText>("Container_jinrucishu/Container_nandudengji/Label_txtNandudengji");
            _diffultyTemplate = _diffultyTxt.CurrentText.text;

            _scoreView = _contentContainer.AddChildComponent<DemonGateScoreView>("Container_jifenxinxi");
            _rewardView = _contentContainer.AddChildComponent<DemonGateRewardView>("Container_jianglixinxi");

            _transformBtn = GetChildComponent<KButton>("Button_chuansong");
            _fightBtn = GetChildComponent<KButton>("Button_qianwangtiaozhan");
            _descTxt = GetChildComponent<StateText>("Label_txtKeshuaxinxin");
            InitTransformBtn();
        }

        private void InitTransformBtn()
        {
            GameObject iconGo = _transformBtn.GetChild("icon");
            StateText label = _transformBtn.GetChildComponent<StateText>("num");
            string[] cfg = global_params_helper.GetGlobalParam(GlobalParamId.demonGateTransportCostItem).Split(':');
            int itemId = int.Parse(cfg[0]);
            int number = int.Parse(cfg[1]);
            MogoAtlasUtils.AddIcon(iconGo, item_helper.GetIcon(itemId));
            label.ChangeAllStateText(number.ToString());
        }

        public void Refresh()
        {
            AddEventListener();
            // 每次开启界面都向服务器重新请求世界等级和个人的排行榜数据
            DemonGateManager.Instance.RequestWorldLevel();
            DemonGateManager.Instance.RequestDemonGateInfo();
        }

        private void RefreshBtnState(int level)
        {
            int normalInstance = evil_mission_helper.GetNormalInstanceId(level);
            if (PlayerDataManager.Instance.CopyData.IsInstancePass(normalInstance) == true)
            {
                SetButtonActive(_fightBtn);
                SetButtonActive(_transformBtn);
                _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(84008);
            }
            else
            {
                SetButtonGray(_fightBtn);
                SetButtonGray(_transformBtn);
                _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(84007);
            }
        }

        private void SetButtonActive(KButton button)
        {
            ImageWrapper[] images = button.GetComponentsInChildren<ImageWrapper>();
            for (int i = 0; i < images.Length; i++)
            {
                images[i].SetGray(1);
            }
            button.interactable = true;
        }

        private void SetButtonGray(KButton button)
        {
            ImageWrapper[] images = button.GetComponentsInChildren<ImageWrapper>();
            for (int i = 0; i < images.Length; i++)
            {
                images[i].SetGray(0);
            }
            button.interactable = false;
        }

        private void RefreshInstanceScore()
        {
            PbEvilMissionInfo rankData = PlayerDataManager.Instance.DemonGateData.SelfRankInfo;
            _scoreView.Refresh(rankData);
            _rewardView.Refresh(rankData);
        }

        private void AddEventListener()
        {
            _transformBtn.onClick.AddListener(OnTransformClick);
            _fightBtn.onClick.AddListener(OnFightClick);
            EventDispatcher.AddEventListener<int, bool>(TaskEvent.TALK_TO_NPC, ArriveNPC);
            EventDispatcher.AddEventListener(DemonGateEvents.ACTION_DEMONGATE_INFO_EVENTS, RefreshWorldLevel);
            EventDispatcher.AddEventListener(DemonGateEvents.ACTION_DEMONGATE_FIGHT_EVENTS, RefreshInstanceScore);
        }

        private void RefreshWorldLevel()
        {
            int worldLevel = (int)PlayerDataManager.Instance.DemonGateData.WorldLevel;
            _diffultyTxt.CurrentText.text = string.Format(_diffultyTemplate, worldLevel);
            int instanceId = evil_mission_helper.GetScoreInstanceId(worldLevel);
            RefreshBtnState(worldLevel);
        }

        private void RemoveEventListener()
        {
            _transformBtn.onClick.RemoveListener(OnTransformClick);
            _fightBtn.onClick.RemoveListener(OnFightClick);
            EventDispatcher.RemoveEventListener<int, bool>(TaskEvent.TALK_TO_NPC, ArriveNPC);
            EventDispatcher.RemoveEventListener(DemonGateEvents.ACTION_DEMONGATE_INFO_EVENTS, RefreshWorldLevel);
            EventDispatcher.RemoveEventListener(DemonGateEvents.ACTION_DEMONGATE_FIGHT_EVENTS, RefreshInstanceScore);
        }

        private void OnTransformClick()
        {
            if (activity_helper.IsOpen(activity_helper.DemonGateActivity) == false)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(44317));
                return;
            }
            if (PlayerDataManager.Instance.TeamData.TeammateDic.Count > 0)
            {
                string content = MogoLanguageUtil.GetContent(84006);
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content);
                return;
            }
            targetNpcId = DemonGateManager.Instance.GetNPCId();
            int mapId = npc_helper.GetNpcMap(targetNpcId);
            List<int> locationList = npc_helper.GetNpcLocation(targetNpcId);
            int xPosition = locationList[0];
            int zPosition = locationList[2];
            UIManager.Instance.ClosePanel(PanelIdEnum.DemonGate);
            DemonGateManager.Instance.RequestTransport(mapId, xPosition, zPosition);

        }

        private void OnFightClick()
        {
            if (activity_helper.IsOpen(activity_helper.DemonGateActivity) == false)
            {
                //MessageBox.Show(false, string.Empty, "活动暂未开放");
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(44317));
                return;
            }
            if (PlayerDataManager.Instance.TeamData.TeammateDic.Count > 0)
            {
                string content = MogoLanguageUtil.GetContent(84006);
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content);
                return;
            }
            int copyId = DemonGateManager.Instance.GetCopyID();
            targetNpcId = DemonGateManager.Instance.GetNPCId();
            UIManager.Instance.ClosePanel(PanelIdEnum.Activity);
            UIManager.Instance.ClosePanel(PanelIdEnum.DemonGate);
            UIManager.Instance.ClosePanel(PanelIdEnum.Reward);
            EventDispatcher.TriggerEvent(PlayerCommandEvents.FIND_NPC, targetNpcId, true);
        }

        private void ArriveNPC(int npcId, bool checkTask)
        {
            if (npcId == targetNpcId)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.DemonGatePop);
            }
        }


        public void Hide()
        {
            if (Visible == true)
            {
                RemoveEventListener();
            }
            Visible = false;
        }
    }
}