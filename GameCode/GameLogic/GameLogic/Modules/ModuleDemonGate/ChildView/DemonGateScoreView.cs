﻿#region 模块信息
/*==========================================
// 文件名：DemonGateScoreView
// 命名空间: GameLogic.GameLogic.Modules.ModuleDemonGate.ChildView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/9 18:59:00
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
namespace ModuleDemonGate
{
    public class DemonGateScoreView : KContainer
    {
        private StateText _todayMaxScore;
        private string _todayMaxScoreTemplate;
        private StateText _weekMaxScore;
        private string _weekMaxScoreTemplate;
        private StateText _todayRank;
        private string _todayRankTemplate;
        private StateText _weekRank;
        private string _weekRankTemplate;

        protected override void Awake()
        {
            _todayMaxScore = GetChildComponent<StateText>("Container_benrentongguanjifen/Label_txtBencitongguanjifen");
            _todayMaxScoreTemplate = _todayMaxScore.CurrentText.text;
            _weekMaxScore = GetChildComponent<StateText>("Container_jinrizuigaojifen/Label_txtJinrizuigao");
            _weekMaxScoreTemplate = _weekMaxScore.CurrentText.text;
            _todayRank = GetChildComponent<StateText>("Container_benrenjinrizuigao/Label_txtBenrenzuigao");
            _todayRankTemplate = _todayRank.CurrentText.text;
            _weekRank = GetChildComponent<StateText>("Container_benzhouleijijifen/Label_txtBenzhouleiji");
            _weekRankTemplate = _weekRank.CurrentText.text;

        }

        public void Refresh(PbEvilMissionInfo rankData)
        {
            _todayMaxScore.CurrentText.text = string.Format(_todayMaxScoreTemplate, rankData.max_score);
            _weekMaxScore.CurrentText.text = string.Format(_weekMaxScoreTemplate, rankData.week_score);
            _todayRank.CurrentText.text = string.Format(_todayRankTemplate, rankData.rank_index_day);
            _weekRank.CurrentText.text = string.Format(_weekRankTemplate, rankData.rank_index_week);
        }
    }
}