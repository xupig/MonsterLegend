﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleDemonGate
{
    public class DemonGateWinView : KContainer
    {
        private const int MAX_COUNT_DOWN_SECOND = 10;
        private uint _timerId = TimerHeap.INVALID_ID;

        private CopyDemonGateLogic _logic;

        private KList _rewardList;
        private KList _conditionList;
        private StateText _textCountDown;
        private KButton _btnQuit;
        private KButton _btnAgain;
        private KButton _btnSure;
        private KButton _statisticBtn;
        private StateText _textTotalScore;
        private GameObject _goHightestRecord;
        private Locater _locaterReward;
        private GameObject _winImageGo;
        private int count = 0;

        private Vector3 startVector = new Vector3(5.0f, 5.0f, 5.0f);
        private const float ImageGoAnimationTime = 0.2f;
        private GameObject _highScoreContainer;
        protected override void Awake()
        {
            InitConditionList();
            InitRewardList();
            _textCountDown = GetChildComponent<StateText>("Label_txtTuichu");
            _btnQuit = GetChildComponent<KButton>("Button_quxiao");
            _btnAgain = GetChildComponent<KButton>("Button_zailaiyiju");
            _btnSure = GetChildComponent<KButton>("Button_queren");
            _statisticBtn = GetChildComponent<KButton>("Button_zhandoutongji");
            _textTotalScore = GetChildComponent<StateText>("Container_jifen/Label_txtZongjifen");
            _goHightestRecord = GetChild("Container_jifen/Image_shuaxinchengji");
            _winImageGo = GetChild("Container_biaoti");
            _highScoreContainer = GetChild("Container_jifen");
            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnQuit.onClick.AddListener(OnQuit);
            _btnAgain.onClick.AddListener(OnAgain);
            _btnSure.onClick.AddListener(OnQuit);
            _statisticBtn.onClick.AddListener(ShowStatisticPanel);
            EventDispatcher.AddEventListener(DemonGateEvents.SCORE_MODE_AGAIN, AgreePlayAgain);
        }

        private void AgreePlayAgain()
        {
            MissionManager.Instance.RequestAgreePlayAgain(true);
        }

        private void HandlePlayAgainError(error_code errorId)
        {

        }

        private void RemoveListener()
        {
            _btnQuit.onClick.RemoveListener(OnQuit);
            _btnAgain.onClick.RemoveListener(OnAgain);
            _btnSure.onClick.RemoveListener(OnQuit);
            _statisticBtn.onClick.RemoveListener(ShowStatisticPanel);
            EventDispatcher.RemoveEventListener(DemonGateEvents.SCORE_MODE_AGAIN, AgreePlayAgain);
        }

        private void OnAgain()
        {
            MissionManager.Instance.RequestCanPlayAgain();
        }

        private void OnQuit()
        {
            GameSceneManager.GetInstance().LeaveCombatScene();
        }

        private void ShowStatisticPanel()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.BattleDataStatistic);
        }

        private void InitConditionList()
        {
            _conditionList = GetChildComponent<KList>("List_condition");
            _conditionList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _conditionList.SetGap(8, 0);
        }

        private void InitRewardList()
        {
            _rewardList = GetChildComponent<KList>("List_rewardContent");
            _rewardList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _rewardList.SetGap(0, 53);
            _locaterReward = AddChildComponent<Locater>("List_rewardContent");
        }

        protected override void OnEnable()
        {
            AddTimer();
        }

        protected override void OnDisable()
        {
            RemoveTimer();
        }

        private void AddTimer()
        {
            if (_timerId == TimerHeap.INVALID_ID)
            {
                count = MAX_COUNT_DOWN_SECOND;
                _timerId = TimerHeap.AddTimer(0, 1000, OnTick);
            }
        }

        private void RemoveTimer()
        {
            if (_timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = TimerHeap.INVALID_ID;
            }
        }

        private void OnTick()
        {
            if (count <= 0)
            {
                OnFinish();
            }
            _textCountDown.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(72800),
            ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, count.ToString()));
            count--;
        }

        private void OnFinish()
        {
            RemoveTimer();
            OnQuit();
        }

        public CopyDemonGateLogic Data
        {
            set
            {
                _logic = value;
                Visible = true;
                RefreshWinImage();
                HideGameObject();
                UIManager.Instance.PlayAudio("Sound/UI/fight_victory.mp3");
                TimerHeap.AddTimer<CopyDemonGateLogic>(500, 0, ShowConditionAndReward, value);
                TimerHeap.AddTimer<CopyDemonGateLogic>(1500, 0, ShowTitleAndBtn, value);
            }
        }

        private void HideGameObject()
        {
            _conditionList.Visible = false;
            _rewardList.Visible = false;
            _highScoreContainer.SetActive(false);
            _textCountDown.Visible = false;
            _btnQuit.Visible = false;
            _btnAgain.Visible = false;
            _btnSure.Visible = false;
            _statisticBtn.Visible = false;
        }

        private void ShowTitleAndBtn(CopyDemonGateLogic logic)
        {
            _highScoreContainer.SetActive(true);
            _textCountDown.Visible = true;
            if (_logic.CanPlayAgain())
            {
                _btnQuit.Visible = true;
                _btnSure.Visible = false;
                _btnAgain.Visible = true;
            }
            else
            {
                _btnQuit.Visible = false;
                _btnSure.Visible = true;
                _btnAgain.Visible = false;
            }
            int insId = GameSceneManager.GetInstance().curInstanceID;
            instance ins = instance_helper.GetInstanceCfg(insId);
            if (instance_helper.GetStatisticType(ins) != 0)
            {
                _statisticBtn.Visible = true;
            }
            RefreshTitle(logic);
        }

        private void RefreshWinImage()
        {
            _winImageGo.transform.localScale = startVector;
            Vector3 endVector = new Vector3(1.0f, 1.0f, 1.0f);
            TweenScale.Begin(_winImageGo, ImageGoAnimationTime, endVector);
        }

        private void ShowConditionAndReward(CopyDemonGateLogic logic)
        {
            _conditionList.Visible = true;
            _rewardList.Visible = true;
            RefreshCoditionResult(logic);
            RefreshReward(logic);
        }

        private void RefreshTitle(CopyDemonGateLogic logic)
        {
            _textTotalScore.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(84000), logic.Score);
            _goHightestRecord.SetActive(logic.IsNewRecord);
        }

        private void RefreshCoditionResult(CopyDemonGateLogic logic)
        {
            List<DemonGateConditionData> conditionDataList = new List<DemonGateConditionData>();
            for (int i = 0; i < logic.ConditionResultList.Count; i++)
            {
                DemonGateConditionData conditionData = new DemonGateConditionData(logic.MapId, logic.ConditionResultList[i]);
                conditionDataList.Add(conditionData);
            }
            _conditionList.RemoveAll();
            _conditionList.SetDataList<DemonGateConditionResult>(conditionDataList);
            TweenListEntry.Begin(_conditionList.gameObject);
        }

        private void RefreshReward(CopyDemonGateLogic logic)
        {
            List<RewardData> rewardDataList = PbItemHelper.ParsePbItemListToRewardDataList(logic.DropReward);
            _rewardList.RemoveAll();
            _rewardList.SetDataList<RewardGrid>(rewardDataList);
            RectTransform rect = transform.GetComponent<RectTransform>();
            _locaterReward.X = (rect.sizeDelta.x - _locaterReward.Width) / 2f;
            ShowRewardAnimation(_rewardList);
        }

        private void ShowRewardAnimation(KList _rewardList)
        {
            for (int i = 0; i < _rewardList.ItemList.Count; i++)
            {
                _rewardList.ItemList[i].transform.localScale = new Vector3(0.1f, 0.1f, 1);
                _rewardList.ItemList[i].Visible = false;
            }
            float delay = 0.0f;
            for (int i = 0; i < _rewardList.ItemList.Count; i++)
            {
                TweenScale.Begin(_rewardList.ItemList[i].gameObject, delay, 0.20f, new Vector3(1.0f, 1.0f, 1.0f));
                delay += 0.10f;
            }
        }
    }
}