﻿#region 模块信息
/*==========================================
// 文件名：DemonGateRewardView
// 命名空间: GameLogic.GameLogic.Modules.ModuleDemonGate.ChildView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/9 19:01:31
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using System.Collections.Generic;
namespace ModuleDemonGate
{
    public class DemonGateRewardView : KContainer
    {
        private StateText _todayNotFightTxt;
        private StateText _weekNotFightTxt;
        private KContainer _ArrowUp;
        private KContainer _ArrowDown;

        private RankRewardGroup _todayReward;
        private RankRewardGroup _todayNextReward;
        private RankRewardGroup _weekReward;
        private RankRewardGroup _weekNextReward;

        protected override void Awake()
        {
            _todayNotFightTxt = GetChildComponent<StateText>("Label_txtbenriweijinru");
            _weekNotFightTxt = GetChildComponent<StateText>("Label_txtbenzhouweijinru");
            _ArrowUp = GetChildComponent<KContainer>("Container_shangJiantou");
            _ArrowDown = GetChildComponent<KContainer>("Container_xiajiantou");
            _todayReward = AddChildComponent<RankRewardGroup>("Container_jiangli01");
            _todayNextReward = AddChildComponent<RankRewardGroup>("Container_jiangli02");
            _weekReward = AddChildComponent<RankRewardGroup>("Container_jiangli03");
            _weekNextReward = AddChildComponent<RankRewardGroup>("Container_jiangli04");
            this.Visible = false;
        }

        public void Refresh(PbEvilMissionInfo rankData)
        {
            this.Visible = true;

            int todayIndex = rankData.rank_index_day;
            int weekIndex = rankData.rank_index_week;

            RefreshTodayReward(todayIndex);
            RefreshWeekReward(weekIndex);
        }

        private void RefreshTodayReward(int todayIndex)
        {
            // 恶魔之门日榜索引
            const int demon_day_rank_index = 4;
            if (todayIndex == 0)
            {
                _todayReward.Visible = false;
                _todayNextReward.Visible = false;
                _ArrowUp.Visible = false;
                _todayNotFightTxt.Visible = true;
            }
            else if (todayIndex == 1)
            {
                _todayReward.Visible = true;
                _todayNextReward.Visible = false;
                _ArrowUp.Visible = false;
                _todayNotFightTxt.Visible = false;
                _todayReward.Refresh(DemonGateManager.Instance.GetSelfRewardDataList(todayIndex, demon_day_rank_index));
            }
            else
            {
                _todayReward.Visible = true;
                _todayNextReward.Visible = true;
                _ArrowUp.Visible = true;
                _todayNotFightTxt.Visible = false;
                _todayReward.Refresh(DemonGateManager.Instance.GetSelfRewardDataList(todayIndex, demon_day_rank_index));
                _todayNextReward.Refresh(DemonGateManager.Instance.GetNextStageRewardDataList(todayIndex, demon_day_rank_index));
            }

        }

        private void RefreshWeekReward(int weekIndex)
        {
            const int demongate_week_rank_index = 5;
            if (weekIndex == 0)
            {
                _weekReward.Visible = false;
                _weekNextReward.Visible = false;
                _ArrowDown.Visible = false;
                _weekNotFightTxt.Visible = true;
            }
            else if (weekIndex == 1)
            {
                _weekReward.Visible = true;
                _weekNextReward.Visible = false;
                _ArrowDown.Visible = false;
                _weekNotFightTxt.Visible = false;
                _weekReward.Refresh(DemonGateManager.Instance.GetSelfRewardDataList(weekIndex, demongate_week_rank_index));
            }
            else
            {
                _weekReward.Visible = true;
                _weekNextReward.Visible = true;
                _ArrowDown.Visible = false;
                _weekNotFightTxt.Visible = false;
                _weekReward.Refresh(DemonGateManager.Instance.GetSelfRewardDataList(weekIndex, demongate_week_rank_index));
                _weekNextReward.Refresh(DemonGateManager.Instance.GetNextStageRewardDataList(weekIndex, demongate_week_rank_index));
            }

        }
    }
}