﻿

using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameData;
using GameMain.Entities;

namespace ModuleDemonGate
{
    /// <summary>
    /// 恶魔之门界面
    /// </summary>
    public class DemonGateContentView : KContainer
    {
        private KToggleGroup _modeToggleGroup;
        private DemonGateNormalModeView _noramlModeView;
        private DemonGateScoreModeView _scoreModeView;

        private const int NORMAL_INDEX = 1;
        private const int SCORE_INDEX = 0;

        protected override void Awake()
        {
            _modeToggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_moshi");
            _noramlModeView = AddChildComponent<DemonGateNormalModeView>("Container_putong");
            _scoreModeView = AddChildComponent<DemonGateScoreModeView>("Container_jifen");
        }

        public void Show(int mode = 1)
        {
            if (CheckCanEnterNoramlState() == false)
            {
                mode = 2;
            }
            switch (mode)
            {
                case 1:
                    _scoreModeView.Hide();
                    _noramlModeView.Visible = true;
                    _noramlModeView.Refresh();
                    _modeToggleGroup.SelectIndex = 1;
                    break;
                case 2:
                    _noramlModeView.Hide();
                    _scoreModeView.Visible = true;
                    _scoreModeView.Refresh();
                    _modeToggleGroup.SelectIndex = 0;
                    break;
            }
            AddEventListener();
        }

        private bool CheckCanEnterNoramlState()
        {
            int level = PlayerAvatar.Player.level;
            int normalInstanceId = evil_mission_helper.GetNormalInstanceId(level);
            int dailyTimes = instance_helper.GetInstanceCfg(normalInstanceId).__daily_times;
            int leftTimes = dailyTimes - PlayerDataManager.Instance.CopyData.GetDailyTimes(normalInstanceId);
            return leftTimes > 0;
        }

        private void AddEventListener()
        {
            _modeToggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
        }

        private void RemoveEventListener()
        {
            _modeToggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
        }

        public void Hide()
        {
            RemoveEventListener();
            _noramlModeView.Hide();
            _scoreModeView.Hide();
            Visible = false;
        }

        private void OnSelectedIndexChanged(KToggleGroup arg0, int index)
        {
            switch (index)
            {
                case NORMAL_INDEX:
                    _scoreModeView.Hide();
                    _noramlModeView.Visible = true;
                    _noramlModeView.Refresh();
                    break;
                case SCORE_INDEX:
                    _noramlModeView.Hide();
                    _scoreModeView.Visible = true;
                    _scoreModeView.Refresh();
                    break;
            }
        }

    }
}
