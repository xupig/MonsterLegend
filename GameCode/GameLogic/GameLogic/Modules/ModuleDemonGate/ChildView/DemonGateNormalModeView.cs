﻿#region 模块信息
/*==========================================
// 文件名：DemonGateNormalModeView
// 命名空间: GameLogic.GameLogic.Modules.ModuleDemonGate.ChildView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/9 16:06:00
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;
namespace ModuleDemonGate
{
    public class DemonGateNormalModeView : KContainer
    {
        private StateText _enterTxt;
        private string _enterTxtTemplate;
        private StateText _diffultyLevelTxt;
        private string _diffultyTemplate;
        private CopyPassConditionList _passContainerList;
        private KList _rewardList;
        private KButton _sweepBtn;
        private KButton _fightBtn;
        private StateText _sweepDescTxt;
        private StateText _lastScoreDescTxt;
        private KContainer _starContainer;
        private KContainer _openContainer;
        private StateText _openConditionTxt;
        private int currentInstanceId;

        protected override void Awake()
        {
            _enterTxt = GetChildComponent<StateText>("Container_jinrucishu/Container_kejincishu/Label_txtKejincishu");
            _enterTxtTemplate = _enterTxt.CurrentText.text;
            _diffultyLevelTxt = GetChildComponent<StateText>("Container_jinrucishu/Container_nandudengji/Label_txtNandudengji");
            _diffultyTemplate = _diffultyLevelTxt.CurrentText.text;
            _passContainerList = AddChildComponent<CopyPassConditionList>("Container_tongguantiaojian");
            _rewardList = GetChildComponent<KList>("Container_tiaozhanJiangli/List_jiangli");
            _openContainer = GetChildComponent<KContainer>("Container_yikaiqi");
            _sweepBtn = _openContainer.GetChildComponent<KButton>("Button_chuansong");
            _fightBtn = _openContainer.GetChildComponent<KButton>("Button_qianwangtiaozhan");
            _starContainer = _openContainer.GetChildComponent<KContainer>("Container_xingxing");
            _sweepDescTxt = _openContainer.GetChildComponent<StateText>("Label_txtjiesuotiaojian");
            _sweepDescTxt.CurrentText.text = MogoLanguageUtil.GetContent(84010);
            _lastScoreDescTxt = _openContainer.GetChildComponent<StateText>("Container_xingxing/Label_txtShangcitongguan");
            _lastScoreDescTxt.CurrentText.text = MogoLanguageUtil.GetContent(84011);
            _openConditionTxt = GetChildComponent<StateText>("Label_kaiqitiaojian");
        }

        public void Refresh()
        {
            AddEventListener();
            int level = PlayerAvatar.Player.level;
            _diffultyLevelTxt.CurrentText.text = string.Format(_diffultyTemplate, level);
            int instanceId = evil_mission_helper.GetNormalInstanceId(level);
            currentInstanceId = instanceId;
            RefreshActivityOpen();
            RefreshCanEnterTimes();
            RefreshSweepState();
            _passContainerList.Refresh(instanceId);
            RefreshRewardList(instanceId);
        }

        private void RefreshActivityOpen()
        {
            if (PlayerDataManager.Instance.FunctionData.IsFunctionOpen(FunctionId.demongate))
            {
                _openContainer.Visible = true;
                _openConditionTxt.CurrentText.text = string.Empty;
            }
            else
            {
                _openContainer.Visible = false;
                _openConditionTxt.CurrentText.text = string.Format("{0}{1}", MogoLanguageUtil.GetContent(84012),
                    function_helper.GetConditionDesc((int)FunctionId.demongate));
            }
        }

        private void RefreshSweepState()
        {
            int instanceId = currentInstanceId;
            int starNum = PlayerDataManager.Instance.CopyData.GetHistoryMaxStar(instanceId);
            if (starNum == 0)
            {
                _starContainer.Visible = false;
            }
            else
            {
                RefreshStarNum(_starContainer, starNum);
            }
            if (starNum == 3)
            {
                _sweepDescTxt.Visible = false;
            }
        }

        private void RefreshStarNum(KContainer _starContainer, int starNum)
        {
            for (int i = 0; i < 3; i++)
            {
                KContainer starContainer = _starContainer.GetChildComponent<KContainer>("Container_star" + i);
                if (i < starNum)
                {
                    ShowStar(starContainer);
                }
                else
                {
                    HideStar(starContainer);
                }
            }
        }

        private void ShowStar(KContainer starContainer)
        {
            StateImage lightStar = starContainer.GetChildComponent<StateImage>("Image_xingxingIcon");
            StateImage darkStar = starContainer.GetChildComponent<StateImage>("Image_jiesuanxingxing02Icon");
            lightStar.Visible = true;
            darkStar.Visible = false;
        }

        private void HideStar(KContainer starContainer)
        {
            StateImage lightStar = starContainer.GetChildComponent<StateImage>("Image_xingxingIcon");
            StateImage darkStar = starContainer.GetChildComponent<StateImage>("Image_jiesuanxingxing02Icon");
            lightStar.Visible = false;
            darkStar.Visible = true;
        }

        private void RefreshRewardList(int instanceId)
        {
            _rewardList.RemoveAll();
            List<int> rewardIdList = instance_reward_helper.GetInstanceRewardId(instanceId, PlayerAvatar.Player.level);
            List<RewardData> rewardDataList = RemoveSameReward(item_reward_helper.GetRewardDataList(rewardIdList, (Vocation)PlayerAvatar.Player.vocation));
            _rewardList.SetDataList<RewardGrid>(rewardDataList);
        }

        private List<RewardData> RemoveSameReward(List<RewardData> list)
        {
            List<RewardData> result = new List<RewardData>();
            Dictionary<int, RewardData> dict = new Dictionary<int, RewardData>();
            for (int i = 0; i < list.Count; i++)
            {
                RewardData listData = list[i];
                if (dict.ContainsKey(listData.id))
                {

                }
                else
                {
                    listData.num = 1;
                    dict.Add(listData.id, listData);
                }
            }
            foreach (var pair in dict)
            {
                result.Add(pair.Value);
            }
            return result;
        }

        private void RefreshCanEnterTimes()
        {
            int instanceId = currentInstanceId;
            int dailyTimes = instance_helper.GetInstanceCfg(instanceId).__daily_times;
            int leftTimes = dailyTimes - PlayerDataManager.Instance.CopyData.GetDailyTimes(instanceId);
            string content;
            if (leftTimes <= 0)
            {
                content = string.Format("<color=#FF0000>{0}</color><color=#F3D7B3>/{1}</color>", leftTimes, dailyTimes);
            }
            else
            {
                content = string.Format("<color=#F3D7B3>{0}/{1}</color>", leftTimes, dailyTimes);
            }
            _enterTxt.CurrentText.text = string.Format(_enterTxtTemplate, content);
        }

        private void AddEventListener()
        {
            _sweepBtn.onClick.AddListener(OnSweepClick);
            _fightBtn.onClick.AddListener(OnFightClick);
            EventDispatcher.AddEventListener(MissionEvents.MISSION_SWEEP_SUCCESSFUL, RefreshCanEnterTimes);
        }

        private void RemoveEventListener()
        {
            _sweepBtn.onClick.RemoveListener(OnSweepClick);
            _fightBtn.onClick.RemoveListener(OnFightClick);
            EventDispatcher.RemoveEventListener(MissionEvents.MISSION_SWEEP_SUCCESSFUL, RefreshCanEnterTimes);
        }

        private void OnSweepClick()
        {
            DemonGateManager.Instance.RequestDemonGateSweep();
        }

        private void OnFightClick()
        {
            const int normalType = 2;
            DemonGateManager.Instance.RequestDemonGateFight(normalType);
        }

        public void Hide()
        {
            RemoveEventListener();
            Visible = false;
        }
    }
}