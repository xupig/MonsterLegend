﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/14 22:47:17
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using UnityEngine;
using ModuleCopy;
using GameMain.GlobalManager;
using Common.ExtendComponent;

namespace ModuleDemonGate
{
    public class DemonGateDetailConditionList : KContainer
    {
        private const int MAX_CROSS_CONDITION_NUM = 3;

        private CopyPassCondition[] _crossConditionArray;

        protected override void Awake()
        {
            _crossConditionArray = new CopyPassCondition[MAX_CROSS_CONDITION_NUM];
            for (int i = 0; i < MAX_CROSS_CONDITION_NUM; i++)
            {
                _crossConditionArray[i] = AddChildComponent<CopyPassCondition>(string.Format("Container_NR0{0}", i + 1));
            }
        }

        public void Refresh(instance cfg)
        {
            Dictionary<string, string[]> dicScoreRule = XMLManager.map[instance_helper.GetMapId(cfg)].__score_rule;
            int count = 0;
            foreach (KeyValuePair<string, string[]> pair in dicScoreRule)
            {
                float param = float.Parse(pair.Value[0]);
                string strParam = "";
                if (param < 0)
                {
                    param = param * -100;
                    strParam = param.ToString() + "%";
                }
                else
                {
                    strParam = param.ToString();
                }

                _crossConditionArray[count].SetDescribe(MogoLanguageUtil.GetContent(pair.Value[4], strParam));
                count++;
            }
            for (int i = 0; i < _crossConditionArray.Length; i++)
            {
                _crossConditionArray[i].Visible = i < dicScoreRule.Count;
            }
        }

    }
}
