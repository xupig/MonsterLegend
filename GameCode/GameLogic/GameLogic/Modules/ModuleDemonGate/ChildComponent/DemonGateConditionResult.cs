﻿using Common.Data;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleDemonGate
{
    public class DemonGateConditionResult : KList.KListItemBase
    {
        private GameObject _goIcon;
        private StateText _textCondition;
        private StateText _textScore;
        private StateImage _imageMaxScore;
        private StateText _textConditionValue;

        protected override void Awake()
        {
            _goIcon = GetChild("Container_icon");
            _textCondition = GetChildComponent<StateText>("Label_txtcondition");
            _textScore = GetChildComponent<StateText>("Label_txtscore");
            _imageMaxScore = GetChildComponent<StateImage>("Image_manfen");
            _textConditionValue = GetChildComponent<StateText>("Label_txtconditionValue");
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                DemonGateConditionData actionInfo = value as DemonGateConditionData;
                _textCondition.CurrentText.text = actionInfo.desc;
                _textConditionValue.CurrentText.text = actionInfo.actionValueStr;
                _textScore.CurrentText.text = actionInfo.score.ToString();
                _imageMaxScore.Alpha = actionInfo.isMaxScore ? 1f : 0f;
            }
        }
    }
}
