﻿#region 模块信息
/*==========================================
// 文件名：RankRewardGroup
// 命名空间: GameLogic.GameLogic.Modules.ModuleDemonGate.ChildComponent
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/10 14:26:53
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using System.Collections.Generic;
namespace ModuleDemonGate
{
    public class RankRewardGroup : KContainer
    {
        private RewardGrid itemOne;
        private RewardGrid itemTwo;

        protected override void Awake()
        {
            itemOne = AddChildComponent<RewardGrid>("Container_item01");
            itemTwo = AddChildComponent<RewardGrid>("Container_item02");
        }

        public void Refresh(List<RewardData> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i].num = 1;
            }
            itemOne.Data = list.Count > 0 ? list[0] : null;
            itemTwo.Data = list.Count > 1 ? list[1] : null;
        }
    }
}