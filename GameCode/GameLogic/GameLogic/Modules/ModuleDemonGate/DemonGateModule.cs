﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;

namespace ModuleDemonGate
{
    class DemonGateModule : BaseModule
    {
        public DemonGateModule()
            : base()
        {
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init( )
        {
            AddPanel(PanelIdEnum.DemonGate, "Container_DemonGatePanel", MogoUILayer.LayerUIPanel, "ModuleDemonGate.DemonGatePanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.DemonGatePop, "Container_DemonGatePopPanel", MogoUILayer.LayerUINotice, "ModuleDemonGate.DemonGatePopPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.DemonGateResult, "Container_DemonGateResultPanel", MogoUILayer.LayerUIPanel, "ModuleDemonGate.DemonGateResultPanel", BlurUnderlay.Have, HideMainUI.Hide);
        }

    }
}
