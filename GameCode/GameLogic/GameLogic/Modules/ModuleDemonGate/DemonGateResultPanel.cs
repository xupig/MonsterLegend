﻿using Common.Base;
using Common.Data;
using Common.Structs.ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.ExtendComponent;
using GameLoader.Utils.Timer;

namespace ModuleDemonGate
{
    public class DemonGateResultPanel : BasePanel
    {
        private DemonGateWinView _winView;
        private ModelComponent _avtarModel;

        protected override void Awake()
        {
            _winView = AddChildComponent<DemonGateWinView>("Container_shengli");
            _avtarModel = AddChildComponent<ModelComponent>("Container_model");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.DemonGateResult; }
        }

        public override void OnShow(object data)
        {
            CopyDemonGateLogic logic = data as CopyDemonGateLogic;
            _avtarModel.LoadPlayerModel(OnPlayerModelLoaded, false);
            _winView.Data = logic;
        }

        private void OnPlayerModelLoaded(ACTSystem.ACTActor actor)
        {
            actor.animationController.ShowWin();
        }

        public override void OnClose()
        {

        }
    }
}
