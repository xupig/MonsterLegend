﻿#region 模块信息
/*==========================================
// 文件名：DemonGatePanel
// 命名空间: ModuleDemonGate
// 创建者：DuWilliam
// 修改者列表：
// 创建日期：2015年5月5日11:24:29
// 描述说明：恶魔之门
// 其他：
//==========================================*/
#endregion
using UnityEngine;

using Common.Base;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using Common.Data;
using Common.ExtendComponent;
using Common.ExtendComponent.GeneralRankingList;
using GameMain.GlobalManager.SubSystem;

namespace ModuleDemonGate
{
    public class DemonGatePanel : BasePanel
    {
        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.DemonGate; }
        }

        private DemonGateContentView _demonGateContentView;
        private GeneralRankingList _genenralRankingList;
        private CategoryList _categoryToggleGroup;
        private DemonGateData _demonGateData;

        protected override void Awake()
        {
            _genenralRankingList = AddChildComponent<GeneralRankingList>("Container_emozhimen/Container_zongbang");
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _demonGateContentView = AddChildComponent<DemonGateContentView>("Container_emozhimen/Container_emozhimenxinxi");
            _categoryToggleGroup = GetChildComponent<CategoryList>("List_categoryList");
            _categoryToggleGroup.onSelectedIndexChanged.AddListener(OnCategoryChanged);
            _demonGateData = PlayerDataManager.Instance.DemonGateData;
        }

        public override void OnShow(object data)
        {
            if (data is int[])
            {
                if ((data as int[])[1] == 1)
                {
                    _categoryToggleGroup.SelectedIndex = 0;
                    int mode = (data as int[])[0];
                    _demonGateContentView.Show(mode);
                }
            }
            else
            {
                _categoryToggleGroup.SelectedIndex = 0;
                _demonGateContentView.Visible = true;
                _demonGateContentView.Show();
            }
        }

        public override void OnClose()
        {
            _genenralRankingList.Hide();
            _demonGateContentView.Hide();
        }

        private void OnCategoryChanged(CategoryList toggleGroup, int index)
        {
            if (index == 0)
            {
                _genenralRankingList.Visible = false;
                _demonGateContentView.Visible = true;
                _demonGateContentView.Show();
            }
            else if (index == 1)
            {
                _genenralRankingList.Visible = true;
                _genenralRankingList.Refresh(RankType.demonGateDayRank);
                _demonGateContentView.Hide();
            }
            else if (index == 2)
            {
                _genenralRankingList.Visible = true;
                _genenralRankingList.Refresh(RankType.demonGateWeekRank);
                _demonGateContentView.Hide();
            }
        }
    }
}
