﻿#region 模块信息
/*==========================================
// 文件名：DemonGatePopPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleDemonGate
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/6/1 19:04:38
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
namespace ModuleDemonGate
{
    public class DemonGatePopPanel : BasePanel
    {
        private KButton _goButton;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.DemonGatePop; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_tanchuan/Container_pipei/Button_quxiao");
            _goButton = GetChildComponent<KButton>("Container_tanchuan/Container_pipei/Button_pipeijinru");
        }

        public override void OnShow(object data)
        {
            _goButton.onClick.AddListener(OnGoButtonClick);
        }

        private void OnGoButtonClick()
        {
            const int scoreMode = 1;
            if (CheckNormalInstanceHasPassed() == false)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(84009), PanelIdEnum.DemonGatePop);
            }
            else
            {
                DemonGateManager.Instance.RequestDemonGateFight(scoreMode);
            }
            ClosePanel();
        }

        private bool CheckNormalInstanceHasPassed()
        {
            int normalInstanceId = evil_mission_helper.GetNormalInstanceId(PlayerAvatar.Player.level);
            return PlayerDataManager.Instance.CopyData.IsInstancePass(normalInstanceId);
        }

        public override void OnClose()
        {
            _goButton.onClick.RemoveListener(OnGoButtonClick);
        }
    }
}