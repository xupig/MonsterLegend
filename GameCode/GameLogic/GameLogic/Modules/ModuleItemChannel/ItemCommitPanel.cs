﻿#region 模块信息
/*==========================================
// 文件名：ItemCommitPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleItemChannel
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/12/30 10:23:57
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleItemChannel
{
    public class ItemCommitPanel : BasePanel
    {
        public static string OPEN_COMMIT_ITEM = "ItemCommitPanel_OPEN_COMMIT_ITEM";
        public static string COMMIT_ITEM = "ItemCommitPanel_COMMIT_ITEM";

        private ItemCommitView _commitView;
        private ItemCommitSelectView _selectView;

        private RectTransform _rect;
        private Vector2 _pos;

        private static int num;
        private static int current;

        public static bool isCommit = false;

        public static Dictionary<int, int> commitNumDict = new Dictionary<int, int>();
        public static int GetCommitItemNum(int pos)
        {
            if (commitNumDict.ContainsKey(pos))
            {
                return commitNumDict[pos];
            }
            return 0;
        }

        public static void AddCommitItem(int pos, int num)
        {
            if (commitNumDict.ContainsKey(pos) == false)
            {
                if (current + num > ItemCommitPanel.num)
                {
                    MogoUtils.FloatTips(54001);
                    return;
                }
                commitNumDict.Add(pos, num);
            }
            else
            {
                commitNumDict.Remove(pos);
            }
            current = 0;
            foreach (KeyValuePair<int, int> kvp in commitNumDict)
            {
                current += kvp.Value;
            }
            EventDispatcher.TriggerEvent(COMMIT_ITEM, pos);
        }

        protected override void Awake()
        {
            _rect = GetComponent<RectTransform>();
            _pos = _rect.anchoredPosition;
            _commitView = AddChildComponent<ItemCommitView>("Container_tijiaoshuliang");
            _selectView = AddChildComponent<ItemCommitSelectView>("Container_renwumubiao");
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.ItemCommit; }
        }

        public override void OnShow(object data)
        {
            SetData(data);
            EventDispatcher.AddEventListener(ItemChannelEvents.Open_Item_Channel_Tips, OnOpenItemChannelTips);
            EventDispatcher.AddEventListener(ItemChannelEvents.Close_Item_Channel_Tips, OnCloseItemChannelTips);
            EventDispatcher.AddEventListener<int>(OPEN_COMMIT_ITEM, OnOpenCommitItem);
        }

        private void OnOpenCommitItem(int pos)
        {
            if (num - current <= 0)
            {
                MogoUtils.FloatTips(54001);
                return;
            }
            BaseItemInfo itemInfo = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemInfo(pos);
            if (itemInfo != null && itemInfo.StackCount == 1)
            {
                AddCommitItem(pos, itemInfo.StackCount);
            }
            else
            {
                _commitView.Visible = true;
                _commitView.SetInfo(pos, num-current);
            }
        }

        public override void SetData(object obj)
        {
            int taskId = (int)obj;
            commitNumDict.Clear();

            PbTaskInfo info = PlayerDataManager.Instance.TaskData.GetTaskInfo(taskId);
            task_data data = task_data_helper.GetTaskData(taskId);
            current = 0;
            num = 0;
            if (data.__target != null)
            {
                foreach (KeyValuePair<string, string> kvp in data.__target)
                {
                    num = int.Parse(kvp.Value);
                }
            }
            if (info.target != null && info.target.Count > 0)
            {
                num -= info.target[0].value;
            }
            _commitView.Visible = false;
            _selectView.UpdateContent(taskId, num);
            base.SetData(data);
        }

        public override void OnClose()
        {
            EventDispatcher.RemoveEventListener(ItemChannelEvents.Open_Item_Channel_Tips, OnOpenItemChannelTips);
            EventDispatcher.RemoveEventListener(ItemChannelEvents.Close_Item_Channel_Tips, OnCloseItemChannelTips);
            EventDispatcher.RemoveEventListener<int>(OPEN_COMMIT_ITEM, OnOpenCommitItem);
        }

        private void OnCloseItemChannelTips()
        {
            isResizeByMyself = false;
            _rect.anchoredPosition = _pos;
        }

        private void OnOpenItemChannelTips()
        {
            isResizeByMyself = true;
            _rect.anchoredPosition = _pos - new Vector2(245, 0);
        }

        public override void OnResize()
        {
            float offset = hasMovePos ? -2000 : 0;
            MogoGameObjectHelper.SetPosition(gameObject, new Vector3(UIManager.CANVAS_WIDTH * 0.5f - 245, -UIManager.CANVAS_HEIGHT * 0.5f + offset, 0.0f));
        }

    }
}
