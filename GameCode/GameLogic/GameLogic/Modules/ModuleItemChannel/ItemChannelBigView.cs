﻿#region 模块信息
/*==========================================
// 文件名：ItemChannelView
// 命名空间: GameLogic.GameLogic.Modules.ModuleItemChannel
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/8/31 9:56:03
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System.Collections.Generic;
namespace ModuleItemChannel
{
    public class ItemChannelBigView : KContainer
    {
        private KButton CloseBtn;
        private KScrollView _channelView;
        private KList _channelList;
        private int currentItemId;

        public KComponentEvent onClose = new KComponentEvent();

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_bg/Button_close");
            _channelView = GetChildComponent<KScrollView>("ScrollView_shijianjilu");
            _channelList = GetChildComponent<KList>("ScrollView_shijianjilu/mask/content");
            _channelList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            CloseBtn.onClick.AddListener(onCloseBtnClicked);
        }

        private void onCloseBtnClicked()
        {
            onClose.Invoke();
        }

        public void Refresh(int id)
        {
            Visible = true;
            RefreshContent(id);
        }

        private void RefreshContent(int id)
        {
            currentItemId = id;
            List<ItemChannelData> itemChannelData = GetChannelData(id);
            _channelView.ResetContentPosition();
            _channelList.RemoveAll();
            _channelList.SetDataList<ItemChannelItem>(itemChannelData);
        }

        private List<ItemChannelData> GetChannelData(int id)
        {
            List<ItemChannelData> result = new List<ItemChannelData>();
            List<int> channelIdList = item_channel_helper.GetItemChannelViewList(id);
            List<int> channelIconList = item_channel_helper.GetItemChannelIconList(id);
            List<int> channelFunctionId = item_channel_helper.GetItemChannelFunctionList(id);
            List<int> channelDescList = item_channel_helper.GetItemChannelDescList(id);
            if (channelIdList.Count != channelIconList.Count || channelIdList.Count != channelDescList.Count || channelIdList.Count != channelFunctionId.Count)
            {
                UnityEngine.Debug.LogError("道具产出渠道配置表ID为：" + id + " 出错");
                return null;
            }
            for (int i = 0; i < channelIdList.Count; i++)
            {
                result.Add(new ItemChannelData(id, channelIdList[i], channelIconList[i], channelFunctionId[i], channelDescList[i]));
            }
            return result;
        }

        public void Hide()
        {
            Visible = false;
        }
       
    }
}