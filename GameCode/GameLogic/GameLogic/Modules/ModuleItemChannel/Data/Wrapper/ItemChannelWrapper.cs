﻿#region 模块信息
/*==========================================
// 文件名：ItemChannelWrapper
// 命名空间: GameLogic.GameLogic.Modules.ModuleItemChannel.Data.Wrapper
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/8/31 10:08:35
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
namespace ModuleItemChannel
{
    public class ItemChannelWrapper
    {
        /// <summary>
        /// type = 1: 来自于加号的点击，显示大界面
        /// type = 2: 来自于Icon的点击，显示小界面
        /// </summary>
        public int type;
        public int id;
        public PanelIdEnum context;
    }
}