﻿#region 模块信息
/*==========================================
// 文件名：ItemChannelItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleItemChannel.Data
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/8/17 20:34:24
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using UnityEngine;
using GameMain.Entities;
using ModuleCommonUI;
namespace ModuleItemChannel
{
    public class ItemChannelItem : KList.KListItemBase
    {
        const int GUILD_FUNCTION = 18;

        private IconContainer _iconContainer;
        private StateText _descTxt;
        private StateText _openTxt;
        private KButton _aheadBtn;
        public static PanelIdEnum _context = PanelIdEnum.Empty;

        private ItemChannelData _data;

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as ItemChannelData;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            RefreshIcon();
            RefreshChannelDesc();
            RefreshOpenDesc();
        }

        private void RefreshOpenDesc()
        {
            
            if (PlayerDataManager.Instance.FunctionData.IsFunctionOpen(_data.functionId) == true)
            {
                if (_data.functionId == GUILD_FUNCTION && PlayerAvatar.Player.guild_id == 0)
                {
                    _openTxt.CurrentText.text = MogoLanguageUtil.GetContent(99099);
                }
                else
                {
                    _openTxt.CurrentText.text = string.Empty;
                }
            }
            else
            {
                _openTxt.CurrentText.text = function_helper.GetConditionDesc(_data.functionId);
                SetButtonGray(_aheadBtn.transform);
                _aheadBtn.interactable = false;
            }
        }

        private void SetButtonGray(Transform tran)
        {
            ImageWrapper[] images = tran.GetComponentsInChildren<ImageWrapper>(true);
            for (int i = 0; i < images.Length; i++)
            {
                images[i].SetGray(0.0f);
            }
        }

        private void RefreshChannelDesc()
        {
            _descTxt.CurrentText.text = _data.descString;
        }

        private void RefreshIcon()
        {
            _iconContainer.SetIcon(_data.iconId);
        }

        protected override void Awake()
        {
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _descTxt = GetChildComponent<StateText>("Label_txtChanchumingzi");
            _openTxt = GetChildComponent<StateText>("Label_txtDengjixianzhi");
            _aheadBtn = GetChildComponent<KButton>("Button_fuchou");
            _aheadBtn.onClick.AddListener(OnAheadBtnClicked);
        }

        private void OnAheadBtnClicked()
        {
            if (_data.functionId == GUILD_FUNCTION && PlayerAvatar.Player.guild_id == 0)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(99099));
                return;
            }
            UIManager.Instance.ClosePanel(PanelIdEnum.ItemChannel);
            EventDispatcher.TriggerEvent(ItemChannelEvents.Go_Item_Get_Panel);
            UIManager.Instance.ClosePanel(_context);
            view_helper.OpenView(_data.viewId);
        }

        public override void Dispose()
        {

        }
    }
}