﻿#region 模块信息
/*==========================================
// 文件名：ItemChannelData
// 命名空间: GameLogic.GameLogic.Modules.ModuleItemChannel.Data
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/8/17 20:34:00
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
namespace ModuleItemChannel
{
    public class ItemChannelData
    {
        public int itemId;
        public int viewId;
        public int iconId;
        public int functionId;
        public string descString;

        public ItemChannelData(int itemId, int viewId, int iconId, int functionId, int chineseId)
        {
            this.itemId = itemId;
            this.viewId = viewId;
            this.iconId = iconId;
            this.functionId = functionId;
            this.descString = MogoLanguageUtil.GetContent(chineseId);
        }
    }
}