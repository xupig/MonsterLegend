﻿#region 模块信息
/*==========================================
// 文件名：ItemChannelModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleItemChannel
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/8/17 19:40:53
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using GameData;

namespace ModuleItemChannel
{
    public class ItemChannelModule : BaseModule
    {

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.ItemChannel, "Container_ItemChannel", MogoUILayer.LayerUIToolTip, "ModuleItemChannel.ItemChannelPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, true);
            AddPanel(PanelIdEnum.ItemCommit, "Container_ItemCommit", MogoUILayer.LayerUIToolTip, "ModuleItemChannel.ItemCommitPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }
    }
}