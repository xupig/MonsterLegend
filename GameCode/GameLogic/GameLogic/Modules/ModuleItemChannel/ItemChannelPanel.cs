﻿#region 模块信息
/*==========================================
// 文件名：ItemChannelPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleItemChannel
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/8/17 19:41:10
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System.Collections.Generic;
namespace ModuleItemChannel
{
    public class ItemChannelPanel : BasePanel
    {
        private ItemChannelBigView _bigItemChannel;
        private ItemChannelLittleView _littleItemChannel;
        private ItemChannelWrapper _wrapper;
        private KDummyButton _closeBtn;
        private const int BigItemChannaleView = 1;
        private const int LittleItemChannelView = 2;
        private int type = 1;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.ItemChannel; }
        }

        protected override void Awake()
        {
            _bigItemChannel = AddChildComponent<ItemChannelBigView>("Container_da");
            _littleItemChannel = AddChildComponent<ItemChannelLittleView>("Container_xiao");
            _bigItemChannel.onClose.AddListener(ClosePanel);
            _littleItemChannel.onClose.AddListener(ClosePanel);
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _closeBtn = AddChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
            _closeBtn.onClick.AddListener(OnDummyBtnClick);
        }

        private void OnDummyBtnClick()
        {
           if (type == LittleItemChannelView)
            {
                _littleItemChannel.CloseLittleView();
            }
        }

        public override void OnShow(object data)
        {
            if (data is int)
            {
                _bigItemChannel.Refresh((int)data);
                _littleItemChannel.Hide();
            }
            if (data is ItemChannelWrapper)
            {
                _wrapper = data as ItemChannelWrapper;
                ItemChannelWrapper wrapper = data as ItemChannelWrapper;
                ItemChannelItem._context = wrapper.context;
                type = wrapper.type;
                switch (wrapper.type)
                {
                    case 1:
                        _bigItemChannel.Refresh(wrapper.id);
                        _littleItemChannel.Hide();
                        ModalMask.Alpha = 0.7f;
                        break;
                    case 2:
                        _littleItemChannel.Refresh(wrapper.id);
                        _bigItemChannel.Hide();
                        ModalMask.Alpha = 0.0f;
                        break;
                    default:
                        break;
                }
            }
            return;
        }

        public override void OnClose()
        {
            
        }
    }
}