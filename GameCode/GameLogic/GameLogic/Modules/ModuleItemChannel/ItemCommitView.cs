﻿#region 模块信息
/*==========================================
// 文件名：ItemCommitView
// 命名空间: GameLogic.GameLogic.Modules.ModuleItemChannel
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/12/30 10:40:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using Mogo.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleItemChannel
{
    public class ItemCommitView:KContainer
    {

        private int _totalNum;
        private int _currentNum;
        private int _pos;

        private StateText _txtName;
        private StateText _txtLeftCount;
        private StateText _txtCount;
        private KButton _btnMinusTen;
        private KButton _btnMinus;
        private KButton _btnAdd;
        private KButton _btnAddTen;
        private KButton _btnCancel;
        private KButton _btnCommit;


        protected override void Awake()
        {
            _txtName = GetChildComponent<StateText>("Container_shuliang/Label_txtTitle");
            _txtLeftCount = GetChildComponent<StateText>("Container_shuliang/Label_txtTijiaoshuliangLeft");
            _txtCount = GetChildComponent<StateText>("Container_shuliang/Label_textCount");
            _btnMinusTen = GetChildComponent<KButton>("Container_Buttons/Button_shuangjian");
            _btnMinus = GetChildComponent<KButton>("Container_Buttons/Button_jianshao");
            _btnAdd = GetChildComponent<KButton>("Container_Buttons/Button_tianjia");
            _btnAddTen = GetChildComponent<KButton>("Container_Buttons/Button_shuangjia");
            _btnCancel = GetChildComponent<KButton>("Container_Buttons/Button_quxiao");
            _btnCommit = GetChildComponent<KButton>("Container_Buttons/Button_shiyong");
            base.Awake();
        }

        public void SetInfo(int pos,int num)
        {
            _pos = pos;
            BaseItemInfo itemInfo = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemInfo(pos);
            
            _totalNum = Math.Min(num - ItemCommitPanel.GetCommitItemNum(pos), itemInfo.StackCount);
            _txtName.CurrentText.text = itemInfo.Name;
            _txtLeftCount.CurrentText.text = (91123).ToLanguage(_totalNum);
            _currentNum = Math.Min(1, _totalNum);
            _txtCount.CurrentText.text = _currentNum.ToString();
            _btnMinus.enabled = false;
            _btnMinusTen.enabled = false;
        }


        protected override void OnEnable()
        {
            _btnMinusTen.onClick.AddListener(OnClickMinusTen);
            _btnMinus.onClick.AddListener(OnClickMinus);
            _btnAdd.onClick.AddListener(OnClickAdd);
            _btnAddTen.onClick.AddListener(OnClickAddTen);
            _btnCancel.onClick.AddListener(OnClickCancel);
            _btnCommit.onClick.AddListener(OnClickCommit);
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            _btnMinusTen.onClick.RemoveListener(OnClickMinusTen);
            _btnMinus.onClick.RemoveListener(OnClickMinus);
            _btnAdd.onClick.RemoveListener(OnClickAdd);
            _btnAddTen.onClick.RemoveListener(OnClickAddTen);
            _btnCancel.onClick.RemoveListener(OnClickCancel);
            _btnCommit.onClick.RemoveListener(OnClickCommit);
            base.OnDisable();
        }

        private void UpdateBtnState()
        {
            _txtCount.CurrentText.text = _currentNum.ToString();
            _btnMinus.enabled = _currentNum > 1;
            _btnMinusTen.enabled = _currentNum > 1;
        }

        private void OnClickAddTen()
        {
            if (_currentNum >= _totalNum)
            {
                MogoUtils.FloatTips(95154);
                return;
            }
            _currentNum = Math.Min(_totalNum, _currentNum + 10);
            UpdateBtnState();
        }

        private void OnClickAdd()
        {
            if(_currentNum>=_totalNum)
            {
                MogoUtils.FloatTips(95154);
                return;
            }
            _currentNum = Math.Min(_totalNum, _currentNum + 1);
            UpdateBtnState();
        }

        private void OnClickMinus()
        {
            _currentNum = Math.Max(1, _currentNum - 1);
            UpdateBtnState();
        }

        private void OnClickMinusTen()
        {
            _currentNum = Math.Max(1, _currentNum - 10);
            UpdateBtnState();
        }


        private void OnClickCancel()
        {
            Visible = false;
        }

        private void OnClickCommit()
        {
            Visible = false;
            ItemCommitPanel.AddCommitItem(_pos, _currentNum);
        }
    }
}
