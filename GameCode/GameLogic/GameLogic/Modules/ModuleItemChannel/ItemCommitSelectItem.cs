﻿#region 模块信息
/*==========================================
// 文件名：ItemCommitSelectItemcs
// 命名空间: GameLogic.GameLogic.Modules.ModuleItemChannel
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/12/30 14:40:10
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Common.Structs;
using Game.UI.UIComponent;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleItemChannel
{
    public class ItemCommitSelectItem:KList.KListItemBase
    {
        private BaseItemInfo _baseItemInfo;

        private StateText _txtCommitNum;
        private StateText _txtNum;
        private ItemGrid _itemGrid;
        private KToggle _toggle;
        private StateImage _selected;
        private static string _template;

        protected override void Awake()
        {
            _txtCommitNum = GetChildComponent<StateText>("Label_txtShuliang");
            _txtNum = GetChildComponent<StateText>("Label_txtShuxing");
            _toggle = GetChildComponent<KToggle>("Toggle_Xuanze");
            _toggle.gameObject.AddComponent<RaycastComponent>().RayCast = false;
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _itemGrid.onClick = OnClick;
            _selected = GetChildComponent<StateImage>("Image_EquipSelectedImageguang02");
            if (_template==null)
            {
                _template = _txtCommitNum.CurrentText.text;
            }
        }

        public override void Dispose()
        {
            
        }

        public override bool IsSelected
        {
            get
            {
                return base.IsSelected;
            }
            set
            {
                base.IsSelected = value;
                _selected.Visible = value;
            }
        }

        public override void Show()
        {
            EventDispatcher.AddEventListener<int>(ItemCommitPanel.COMMIT_ITEM, OnCommitItem);
            base.Show();
        }

        public override void Hide()
        {
            EventDispatcher.RemoveEventListener<int>(ItemCommitPanel.COMMIT_ITEM, OnCommitItem);
            base.Hide();
        }

        public override void OnPointerClick(UnityEngine.EventSystems.PointerEventData evtData)
        {
            base.OnPointerClick(evtData);
            OnClick();
        }

        public void OnClick()
        {
            if(_baseItemInfo!=null)
            {
                EventDispatcher.TriggerEvent(ItemCommitPanel.OPEN_COMMIT_ITEM, _baseItemInfo.GridPosition);
            }
        }

        public override object Data
        {
            get
            {
                return _baseItemInfo;
            }
            set
            {
                _baseItemInfo = value as BaseItemInfo;
                RefreshData();
            }
        }

        private void RefreshData()
        {
            IsSelected = false;
            if(_baseItemInfo == null)
            {
                Visible = false;
                return;
            }
            Visible = true;
            _txtNum.CurrentText.text = "x" + _baseItemInfo.StackCount;
            _txtCommitNum.CurrentText.text = string.Format(_template, ItemCommitPanel.GetCommitItemNum(_baseItemInfo.GridPosition));
            _toggle.isOn = ItemCommitPanel.GetCommitItemNum(_baseItemInfo.GridPosition) > 0;
            _itemGrid.SetItemData(_baseItemInfo);
        }

        public void OnCommitItem(int grid)
        {
            if(_baseItemInfo!=null && _baseItemInfo.GridPosition == grid)
            {
                _txtCommitNum.CurrentText.text = string.Format(_template, ItemCommitPanel.GetCommitItemNum(_baseItemInfo.GridPosition));
                _toggle.isOn = ItemCommitPanel.GetCommitItemNum(_baseItemInfo.GridPosition) > 0;
            }
        }
    }
}
