﻿#region 模块信息
/*==========================================
// 文件名：ItemCommitSelectView
// 命名空间: GameLogic.GameLogic.Modules.ModuleItemChannel
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/12/30 14:17:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Global;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.CustomType;
using GameMain.Entities;
using GameMain.GlobalManager;
using ModuleTask.Handle;
using Mogo.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleItemChannel
{
    public class ItemCommitSelectView:KContainer
    {
        private KScrollView _scrollView;
        private KList _list;
        private StateText _txtEmpty;
        private int _taskId;
        private KShrinkableButton _btnChannel;
        private KShrinkableButton _btnCommit;
        private StateText _txtName;
        private Vector2 _position;
        private string template;
        private KButton _btnClose;
        private KList.KListItemBase _selectedItem;
        private ItemChannelWrapper wrapper;
        protected override void Awake()
        {
            wrapper = new ItemChannelWrapper();
            wrapper.type = 2;
            wrapper.context = PanelIdEnum.ItemCommit;
            _scrollView = GetChildComponent<KScrollView>("ScrollView_shijianjilu");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, 4, 1);
            _list.SetGap(1, -16);
            _txtEmpty = GetChildComponent<StateText>("Label_txtZanbumanzu");
            _txtName = GetChildComponent<StateText>("Label_txtRenwumubiao");
            template = _txtName.CurrentText.text;
            _btnChannel = GetChildComponent<KShrinkableButton>("Button_laiyuan");
            _btnCommit = GetChildComponent<KShrinkableButton>("Button_tijiao");
            _position = (_btnChannel.transform as RectTransform).anchoredPosition;
            _btnClose = GetChildComponent<KButton>("Container_bg/Button_close");
            base.Awake();
        }

        protected override void OnEnable()
        {
            _btnClose.onClick.AddListener(OnClose);
            _btnChannel.onClick.AddListener(OnClickChannel);
            _btnCommit.onClick.AddListener(OnClickCommit);
            _list.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            _btnClose.onClick.RemoveListener(OnClose);
            _btnChannel.onClick.RemoveListener(OnClickChannel);
            _btnCommit.onClick.RemoveListener(OnClickCommit);
            _list.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            base.OnDisable();
        }

        private void OnSelectedIndexChanged(KList list,int index)
        {
            if (_selectedItem!=null)
            {
                _selectedItem.IsSelected = false;
            }
            _selectedItem = list.ItemList[index];
            _selectedItem.IsSelected = true;
        }

        private void OnClickChannel()
        {
           
            PanelIdEnum.ItemChannel.Show(wrapper);
        }

        private void OnClickCommit()
        {
            if (ItemCommitPanel.isCommit)
            {
                MogoUtils.FloatTips(10006);
                return;
            }
            if (ItemCommitPanel.commitNumDict.Count == 0)
            {
                MogoUtils.FloatTips(95153);
                return;
            }
            ItemCommitPanel.isCommit = true;
            LuaTable table = new LuaTable();
            foreach (KeyValuePair<int, int> kvp in ItemCommitPanel.commitNumDict)
            {
                table.Add(kvp.Key, kvp.Value);
            }
            PlayerAvatar.Player.RpcCall("task_hand_in_items_req", table);
        }

        private void OnClose()
        {
            PanelIdEnum.ItemCommit.Close();
        }

        private void GetItemId()
        {
            Dictionary<string, string[]> taskFollow = task_data_helper.GetTaskFollow(_taskId);
            if (taskFollow!=null && taskFollow.ContainsKey("6"))
            {
                wrapper.id = int.Parse(taskFollow["6"][0]);
            }
            else
            {
                wrapper.id = 0;
            }
        }

        public void UpdateContent(int taskId,int num)
        {
            if (_selectedItem != null)
            {
                _selectedItem.IsSelected = false;
            }
            _taskId = taskId;
            GetItemId();
            task_data data = task_data_helper.GetTaskData(taskId);
            _txtName.CurrentText.text = task_data_helper.GetTaskSummary(data);//string.Format(template, data.__task_summary.ToLanguage());
            _list.RemoveAll();
            List<BaseItemInfo> result = GetItems();
            if(result.Count == 0)
            {
                _txtEmpty.Visible = true;
                _btnCommit.Visible = false;
                _btnChannel.Visible = true;

                (_btnChannel.transform as RectTransform).anchoredPosition = _position;
                _btnChannel.RefreshRectTransform();
            }
            else
            {
                int count = 0;
                for (int i = 0; i < result.Count;i++ )
                {
                    count += result[i].StackCount;
                }
                if(count>=num)
                {
                    _btnChannel.Visible = false;
                    _btnCommit.Visible = true;
                    (_btnCommit.transform as RectTransform).anchoredPosition = _position;
                    _btnCommit.RefreshRectTransform();
                }
                else
                {
                    _btnChannel.Visible = true;
                    _btnCommit.Visible = true;
                    (_btnChannel.transform as RectTransform).anchoredPosition = _position - new Vector2(100,0);
                    (_btnCommit.transform as RectTransform).anchoredPosition = _position + new Vector2(100,0);
                    _btnChannel.RefreshRectTransform();
                    _btnCommit.RefreshRectTransform();
                }
                _list.SetDataList<ItemCommitSelectItem>(result);
                _txtEmpty.Visible = false;
            }
        }


        private List<BaseItemInfo> GetItems()
        {
            List<BaseItemInfo> result = new List<BaseItemInfo>();
            if(TaskFollowHandle.taskConditionDict.ContainsKey(_taskId) == false)
            {
                return result;
            }
            List<Condition> conds = TaskFollowHandle.taskConditionDict[_taskId];
            
            Dictionary<int,BaseItemInfo> dict = PlayerDataManager.Instance.BagData.GetItemBagData().GetAllItemInfo();
            foreach(KeyValuePair<int,BaseItemInfo> kvp in dict)
            {
                if (kvp.Value!=null && CheckCondition(conds, kvp.Value))
                {
                    result.Add(kvp.Value);
                }
            }
            return result;
        }

        private bool CheckCondition(List<Condition> conds,BaseItemInfo itemInfo)
        {
            bool operation = false;
            for(int i=0;i<conds.Count;i++)
            {
                operation = conds[i].isAnd;
                bool result = conds[i].Check(itemInfo);
                if (conds[i].isAnd == false && result)
                {
                    return true;
                }
                if(conds[i].isAnd == true && result == false)
                {
                    return false;
                }
            }
            return operation;
        }

    }
}
