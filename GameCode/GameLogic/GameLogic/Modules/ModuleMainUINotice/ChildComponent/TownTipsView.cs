﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalSystem;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUINotice
{
    public class TownTipsView : KContainer
    {
        private TownTipsId _id;

        private ItemGrid _itemGrid;
        private KButton _btnClose;
        private KButton _showViewBtn;
        private StateText _textTitle;
        private KContainer _functionContainer;
        private IconContainer _iconContainer;

        private TownTipsData TownTipsData
        {
            get { return PlayerDataManager.Instance.TownTipsData; }
        }

        protected override void Awake()
        {
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _btnClose = GetChildComponent<KButton>("Button_close");
            _textTitle = GetChildComponent<StateText>("Container_zhuangbeiXinxi/Label_txtWupinmingcheng");
            _functionContainer = GetChildComponent<KContainer>("Container_gongneng");
            _iconContainer = _functionContainer.AddChildComponent<IconContainer>("Container_icon");
            _showViewBtn = GetChildComponent<KButton>("Button_xianzaijiuqu");
            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(OnClose);
            _showViewBtn.onClick.AddListener(OnShowView);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(OnClose);
            _showViewBtn.onClick.RemoveListener(OnShowView);
        }

        private void OnShowView()
        {
            TownTipsData.GetBaseTownTips(_id).ShowView();
            TownTipsData.Dequeue();
            EventDispatcher.TriggerEvent(TownTipsEvent.Show);
        }

        private void OnClose()
        {
            TownTipsData.Dequeue();
            EventDispatcher.TriggerEvent(TownTipsEvent.Show);
        }

        public void OnShow(TownTipsId id, int itemId = 0)
        {
            _id = id;
            Visible = true;
            RefreshTitle(id);
            RefreshIcon(id, itemId);
        }

        private void RefreshTitle(TownTipsId id)
        {
            _textTitle.CurrentText.text = TownTipsData.GetBaseTownTips(id).GetTitle();
        }

        private void RefreshIcon(TownTipsId id, int itemId = 0)
        {
            if (itemId != 0)
            {
                _functionContainer.Visible = false;
                _itemGrid.Visible = true;
                SetIconByTownTipsType(id, itemId);
            }
            else
            {
                _functionContainer.Visible = true;
                _itemGrid.Visible = false;
                _iconContainer.SetIcon(TownTipsData.GetBaseTownTips(id).GetIcon());
            }
        }

        private void SetIconByTownTipsType(TownTipsId type, int itemId)
        {
            switch (type)
            {
                case TownTipsId.newEquipEffect:
                    _itemGrid.SetItemData(equip_facade_helper.GetEffectIcon(itemId), 0);
                    break;
                case TownTipsId.newFashion:
                    _itemGrid.SetItemData(fashion_helper.GetIconId(itemId, (int)PlayerAvatar.Player.vocation), 0);
                    break;
                case TownTipsId.newMount:
                    _itemGrid.SetItemData(mount_helper.GetMountIcon(itemId), 0);
                    break;
                case TownTipsId.newWing:
                    _itemGrid.SetItemData(wing_helper.GetWingIcon(itemId), 0);
                    _itemGrid.Context = PanelIdEnum.MainUINotice;
                    break;
            }
        }
    }
}
