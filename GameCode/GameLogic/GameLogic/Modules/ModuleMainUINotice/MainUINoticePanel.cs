﻿#region 模块信息
/*==========================================
// 文件名：MainUINoticePanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleMainUINotice
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/8/3 21:17:15
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Game.UI.UIComponent;
using GameData;
using ModuleMainUI;
using MogoEngine.Events;
using UnityEngine;
namespace ModuleMainUINotice
{
    public class MainUINoticePanel : BasePanel
    {
        private TownTipsView _townTipsView;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.MainUINotice; }
        }

        protected override void Awake()
        {
            _townTipsView = AddChildComponent<TownTipsView>("Container_zhishiTips");
        }

        public override void OnShow(object data)
        {
            SetData(data);
        }

        public override void SetData(object data)
        {
            if (data is TownTipsId)
            {
                TownTipsId id = (TownTipsId)data;
                _townTipsView.OnShow(id);
            }
            if (data is TownTipsWrapper)
            {
                TownTipsWrapper wrapper = data as TownTipsWrapper;
                _townTipsView.OnShow(wrapper.tipType, wrapper.itemId);
            }
        }

        public override void OnLeaveMap(int mapType)
        {
            ClosePanel();
        }

        public override void OnClose()
        {
        }

    }
}