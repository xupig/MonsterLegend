﻿#region 模块信息
/*==========================================
// 文件名：MainUIModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleMainUINotice
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/8/3 21:16:43
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
namespace ModuleMainUINotice
{
    public class MainUINoticeModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.MainUINotice, "Container_MainUINotice", MogoUILayer.LayerUnderPanel, "ModuleMainUINotice.MainUINoticePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }
    }
}