﻿
using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCharge
{
    

    public class ChargeOtherListView:KContainer
    {
        private KList _chargeInfoList;
        private charge_list _chargeDataList;
        public static object SelectedPlayerInfo;

        protected override void Awake()
        {
            base.Awake();
            _chargeInfoList = GetChildComponent<KList>("ScrollView_daichong/mask/content");
            _chargeInfoList.SetDirection(KList.KListDirection.LeftToRight, 1);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            if (_chargeDataList == null)
            {
                ChargeManager.Instance.RequestChargeInfoList();
            }
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            _chargeDataList = null;
            base.OnDisable();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<charge_list>(ChargeEvents.RefreshChargeInfoList, RefreshChargeContent);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<charge_list>(ChargeEvents.RefreshChargeInfoList, RefreshChargeContent);
        }

        private void RefreshChargeContent(charge_list chargeDataList)
        {
            _chargeDataList = chargeDataList;
            List<charge_item> chargeItemList = GetChargeItemList();
            _chargeInfoList.SetDataList<ChargeOtherItem>(chargeItemList);
        }

        public void ShowChargeInfoByPlayerInfo(object data)
        {
            SelectedPlayerInfo = data;
            _chargeInfoList.RemoveAll();
            List<charge_item> chargeItemList = GetChargeItemList();
            _chargeInfoList.SetDataList<ChargeOtherItem>(chargeItemList);
        }

        private List<charge_item> GetChargeItemList()
        {
            List<charge_item> result = new List<charge_item>();
            if (_chargeDataList == null) return result;

            for (int i = 0; i < _chargeDataList.charge_datas.Count; i++)
            {
                charge_item chargeItem = _chargeDataList.charge_datas[i];
                if (chargeItem.pay_for_other == 1)
                {
                    result.Add(chargeItem);
                }
            }
            if (SelectedPlayerInfo == null)
            {
                result.Sort(SortChargeItemByDefault);
            }
            else if (SelectedPlayerInfo is PbFriendAvatarInfo)
            {
                result.Sort(SortChargeItemByFriend);
            }
            else
            {
                result.Sort(SortChargeItemByGuild);
            }
            return result;
        }

        private int SortChargeItemByDefault(charge_item first, charge_item second)
        {
            return (int)first.sort_id - (int)second.sort_id;
        }

        private int SortChargeItemByFriend(charge_item first, charge_item second)
        {
            PbFriendAvatarInfo friendInfo = SelectedPlayerInfo as PbFriendAvatarInfo;
            if ((first.honey_value > friendInfo.degree) == (second.honey_value > friendInfo.degree))
            {
                return (int)first.sort_id - (int)second.sort_id;
            }
            else
            {
                if (first.honey_value > friendInfo.degree)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
        }

        private int SortChargeItemByGuild(charge_item first, charge_item second)
        {
            if ((first.honey_value != 0) == (second.honey_value != 0))
            {
                return (int)first.sort_id - (int)second.sort_id;
            }
            else if (first.honey_value != 0)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }
}
