﻿
using Common.Base;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleCharge
{
    public class ChargePlayerSearchView:KContainer
    {
        private const int SEARCH_STATE = 0;
        private const int RETURN_STATE = 1;

        private KDummyButton _hintButton;

        private KInputField _input;
        private KButton _searchButton;
        private StateText _txtSearchButtonLabel;
        private StateText _hintLabel;

        private int _searchButtonState = 0;

        public KComponentEvent<object> onSearchResultChanged = new KComponentEvent<object>();

        protected override void Awake()
        {
            base.Awake();
            _hintButton = AddChildComponent<KDummyButton>("Container_inputHint");
            _hintLabel = GetChildComponent<StateText>("Container_inputHint/Label_label");
            _searchButton = GetChildComponent<KButton>("Button_sousuo");
            _txtSearchButtonLabel = _searchButton.GetChildComponent<StateText>("label");

            _input = GetChildComponent<KInputField>("Input_input");
            _input.text = string.Empty;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            ShowHintButton();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        public void ShowSearchType(int searchType)
        {
            ShowHintButton();
            _input.text = string.Empty;
            _searchButtonState = SEARCH_STATE;
            RefreshSearchButton();
            if (searchType == 0)
            {
                _hintLabel.CurrentText.text = MogoLanguageUtil.GetContent(111025);
            }
            else
            {
                _hintLabel.CurrentText.text = MogoLanguageUtil.GetContent(111026);
            }
        }

        private void AddEventListener()
        {
            _hintButton.onClick.AddListener(OnHintClick);
            _searchButton.onClick.AddListener(OnSearchButtonClick);
        }

        private void RemoveEventListener()
        {
            _hintButton.onClick.RemoveListener(OnHintClick);
            _searchButton.onClick.RemoveListener(OnSearchButtonClick);
        }

        private void OnSearchButtonClick()
        {
            string content = _input.text;
            if (_searchButtonState == SEARCH_STATE)
            {
                if (content == string.Empty) return;
                GetSearchResult(ChargeOtherPanel.SelectedType, content);
            }
            else
            {
                GetSearchResult(ChargeOtherPanel.SelectedType, string.Empty);
            }

            _searchButtonState = _searchButtonState == SEARCH_STATE ? RETURN_STATE : SEARCH_STATE;
            RefreshSearchButton();

        }

        public void GetSearchResult(int searchPlayerType, string searchContent)
        {
            if (searchPlayerType == 0)
            {
                GetFriendSearchResult(searchContent);
            }
            else
            {
                GetGuildSearchResult(searchContent);
            }

        }

        private void GetFriendSearchResult(string searchContent)
        {
            List<PbFriendAvatarInfo> friendListData = PlayerDataManager.Instance.FriendData.GetFriendInfoByDefault();
            if (searchContent == string.Empty)
            {
                onSearchResultChanged.Invoke(friendListData);
                return;
            }
            PbFriendAvatarInfo friendAvatarInfo = friendListData.Find((friendInfo) =>
            {
                return friendInfo.name == searchContent || friendInfo.dbid.ToString() == searchContent;
            });
            List<PbFriendAvatarInfo> result = new List<PbFriendAvatarInfo>();
            if (friendAvatarInfo != null)
            {
                result.Add(friendAvatarInfo);
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(57012), PanelIdEnum.Charge);
            }
            onSearchResultChanged.Invoke(result);
        }

        private void GetGuildSearchResult(string searchContent)
        {
            List<PbGuildMemberInfo> list = PlayerDataManager.Instance.GuildData.GuildMemberInfoList.member_info;
            if (searchContent == string.Empty)
            {
                onSearchResultChanged.Invoke(list);
                return;
            }
            PbGuildMemberInfo guildMemberInfo = list.Find((member) =>
            {
                return member.name == searchContent || member.dbid.ToString() == searchContent;
            });

            List<PbGuildMemberInfo> guildSearchResult = new List<PbGuildMemberInfo>();
            if (guildMemberInfo != null)
            {
                guildSearchResult.Add(guildMemberInfo);
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(57012), PanelIdEnum.Charge);
            }
            onSearchResultChanged.Invoke(guildSearchResult);
        }

        private void OnHintClick()
        {
            HideHintButton();
            EventSystem.current.SetSelectedGameObject(_input.gameObject);
            _input.OnPointerClick(new PointerEventData(EventSystem.current));
        }

        private void RefreshSearchButton()
        {
            if (_searchButtonState == SEARCH_STATE)
            {
                _txtSearchButtonLabel.ChangeAllStateText(MogoLanguageUtil.GetContent(111029));
            }
            else
            {
                _txtSearchButtonLabel.ChangeAllStateText(MogoLanguageUtil.GetContent(111030));
            }
        }

        private void HideHintButton()
        {
            _hintButton.gameObject.SetActive(false);
        }

        private void ShowHintButton()
        {
            _hintButton.gameObject.SetActive(true);
        }
    }
}
