﻿
using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCharge
{
    public class ChargeSelfView : KContainer
    {
        private KScrollPage _scrollPage;
        private KPageableList _chargeItemList;

        private KToggleGroup _toggleGroupTitle;

        protected override void Awake()
        {
            base.Awake();

            _scrollPage = gameObject.GetComponent<KScrollPage>();
            _chargeItemList = GetChildComponent<KPageableList>("mask/content");
            _chargeItemList.SetDirection(KList.KListDirection.LeftToRight, 4, 2);
            _chargeItemList.SetGap(5, 5);
        }

        public void OnShow(object data = null)
        {
            this.Visible = true;
            AddEventListener();
            ChargeManager.Instance.RequestChargeInfoList();
        }

        public void OnClose()
        {
            this.Visible = false;
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<charge_list>(ChargeEvents.RefreshChargeInfoList, RefreshChargeContent);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<charge_list>(ChargeEvents.RefreshChargeInfoList, RefreshChargeContent);
        }

        private void RefreshChargeContent(charge_list chargeList)
        {
            List<charge_item> chargeableList = GetChargeList(chargeList.charge_datas);

            int totalPage = Mathf.CeilToInt(chargeableList.Count / 8f);
            if (totalPage <= 0) totalPage = 1;
            _scrollPage.CurrentPage = 0;
            _scrollPage.TotalPage = totalPage;
            chargeableList.Sort(SortComparer);
            _chargeItemList.SetDataList<ChargeSelfItem>(chargeableList);
        }

        private int SortComparer(charge_item first, charge_item second)
        {
            return (int)(first.sort_id - second.sort_id);
        }

        private List<charge_item> GetChargeList(List<charge_item> chargeList)
        {
            List<charge_item> result = new List<charge_item>();
            for(int i = 0; i < chargeList.Count; i++)
            {
                if(chargeList[i].pay_for_other == 0)
                {
                    result.Add(chargeList[i]);
                }
            }
            return result;
        }
    }
}
