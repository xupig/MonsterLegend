﻿using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCharge
{
    public class ChargeGuildView:KContainer
    {
        private KList _guildMemberList;
        private StateText _txtNoneLabel;
        private KButton _addGuildButton;

        private const int PER_FRAME_CREATE_NUM = 6;

        public KComponentEvent<PbGuildMemberInfo> onSelectedGuildMemberChanged = new KComponentEvent<PbGuildMemberInfo>();

        protected override void Awake()
        {
            base.Awake();

            _guildMemberList = GetChildComponent<KList>("ScrollView_xuanzegonghui/mask/content");
            _txtNoneLabel = GetChildComponent<StateText>("Label_txtDangqianmeiyougonghui");
            _addGuildButton = GetChildComponent<KButton>("Button_tianjia");

            _guildMemberList.SetDirection(KList.KListDirection.LeftToRight, 1);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            GuildManager.Instance.GetMemberList();
        }


        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        private void AddEventListener()
        {
            _addGuildButton.onClick.AddListener(OnAddGuildButtonClick);
            _guildMemberList.onSelectedIndexChanged.AddListener(OnSelectedGuildMemberChanged);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Member_List, RefreshGuildMemberList);
        }

        private void RemoveEventListener()
        {
            _addGuildButton.onClick.RemoveListener(OnAddGuildButtonClick);
            _guildMemberList.onSelectedIndexChanged.RemoveListener(OnSelectedGuildMemberChanged);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Member_List, RefreshGuildMemberList);
        }

        private void OnAddGuildButtonClick()
        {
            function functionData = function_helper.GetFunction((int)FunctionId.guild);
            if (activity_helper.IsOpen(functionData))
            {
                function_helper.Follow((int)FunctionId.guild, null);
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, function_helper.GetConditionDesc((int)FunctionId.guild));
            }
        }

        private void OnSelectedGuildMemberChanged(KList list, int index)
        {
            onSelectedGuildMemberChanged.Invoke(list[index].Data as PbGuildMemberInfo);
        }

        private void RefreshGuildMemberList()
        {
            if (PlayerAvatar.Player.guild_id != 0)
            {
                List<PbGuildMemberInfo> guildListExceptSelft = new List<PbGuildMemberInfo>();
                List<PbGuildMemberInfo> list = PlayerDataManager.Instance.GuildData.GetMemberList();

                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].dbid != PlayerAvatar.Player.dbid)
                    {
                        guildListExceptSelft.Add(list[i]);
                    }
                }
                _txtNoneLabel.Visible = false;
                _addGuildButton.Visible = false;
                _guildMemberList.SetDataList<ChargeGuildInfoItem>(guildListExceptSelft, PER_FRAME_CREATE_NUM);
            }
            else
            {
                _txtNoneLabel.Visible = true;
                _addGuildButton.Visible = true;
                _guildMemberList.SetDataList<ChargeGuildInfoItem>(new List<PbGuildMemberInfo>());
            }
        }

        public void ShowSearchResult(List<PbGuildMemberInfo> guildSearchResult)
        {
            List<PbGuildMemberInfo> guildListExceptSelft = new List<PbGuildMemberInfo>();

            for (int i = 0; i < guildSearchResult.Count; i++)
            {
                if (guildSearchResult[i].dbid != PlayerAvatar.Player.dbid)
                {
                    guildListExceptSelft.Add(guildSearchResult[i]);
                }
            }

            _guildMemberList.SetDataList<ChargeGuildInfoItem>(guildListExceptSelft, PER_FRAME_CREATE_NUM);
        }
    }
}
