﻿using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCharge
{
    public class ChargeFriendView:KContainer
    {
        private KScrollView _friendScrollView;
        private KList _friendList;
        private StateText _txtNoneLabel;
        private KButton _addFriendButton;

        private const int PER_FRAME_CREATE_NUM = 6;

        public KComponentEvent<PbFriendAvatarInfo> onSelectedFriendChanged = new KComponentEvent<PbFriendAvatarInfo>();

        protected override void Awake()
        {
            base.Awake();

            _friendScrollView = GetChildComponent<KScrollView>("ScrollView_xuanzehaoyouzengsong");
            _friendList = GetChildComponent<KList>("ScrollView_xuanzehaoyouzengsong/mask/content");
            _txtNoneLabel = GetChildComponent<StateText>("Label_txtDangqianmeiyouhaoyou");
            _addFriendButton = GetChildComponent<KButton>("Button_tianjia");

            _friendList.SetDirection(KList.KListDirection.LeftToRight, 1);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            TryShowFriendList();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        private void AddEventListener()
        {
            _addFriendButton.onClick.AddListener(OnAddFriendButtonClick);
            _friendList.onSelectedIndexChanged.AddListener(OnSelectedFriendChanged);
            EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_LIST, RefreshFriendList);
        }

        private void RemoveEventListener()
        {
            _addFriendButton.onClick.RemoveListener(OnAddFriendButtonClick);
            _friendList.onSelectedIndexChanged.RemoveListener(OnSelectedFriendChanged);
            EventDispatcher.RemoveEventListener(FriendEvents.REFRESH_FRIEND_LIST, RefreshFriendList);
        }

        private void OnSelectedFriendChanged(KList list, int index)
        {
            onSelectedFriendChanged.Invoke(list[index].Data as PbFriendAvatarInfo);
        }

        private void OnAddFriendButtonClick()
        {
            function functionData = function_helper.GetFunction((int)FunctionId.friend);
            if (activity_helper.IsOpen(functionData))
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.Interactive, new int[] { 1, 4 });
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, function_helper.GetConditionDesc((int)FunctionId.friend));
            }
        }

        private void TryShowFriendList()
        {
            if (PlayerDataManager.Instance.FriendData.GetMyFriendCount() == 0)
            {
                FriendManager.Instance.SendGetFriendListMsg();
            }
            else
            {
                RefreshFriendList();
            }
        }

        private void RefreshFriendList()
        {
            List<PbFriendAvatarInfo> friendListData = PlayerDataManager.Instance.FriendData.GetFriendInfoByDefault();
            _txtNoneLabel.Visible = friendListData.Count == 0;
            _addFriendButton.Visible = friendListData.Count == 0;
            _friendList.SetDataList<ChargeFriendInfoItem>(friendListData, PER_FRAME_CREATE_NUM);
            _friendList.SelectedIndex = -1;
        }

        public void ShowSearchResult(List<PbFriendAvatarInfo> friendListData)
        {
            _friendList.SetDataList<ChargeFriendInfoItem>(friendListData, PER_FRAME_CREATE_NUM);
            _friendList.SelectedIndex = -1;
        }
    }
}
