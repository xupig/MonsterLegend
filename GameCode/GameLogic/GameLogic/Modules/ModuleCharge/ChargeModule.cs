﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCharge
{
    public class ChargeModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.Charge, "Container_ChargePanel", MogoUILayer.LayerSpecialUIPanel, "ModuleCharge.ChargePanel", BlurUnderlay.Have, HideMainUI.Hide);

            AddPanel(PanelIdEnum.ChargeSelf, "Container_ChargeSelfPanel", MogoUILayer.LayerUIPanel, "ModuleCharge.ChargeSelfPanel", BlurUnderlay.Have, HideMainUI.Hide);

            AddPanel(PanelIdEnum.ChargeOther, "Container_ChargeOtherPanel", MogoUILayer.LayerUIPanel, "ModuleCharge.ChargeOtherPanel", BlurUnderlay.Have, HideMainUI.Hide);

            RegisterPanelModule(PanelIdEnum.Charge, PanelIdEnum.Charge);
            RegisterPanelModule(PanelIdEnum.ChargeSelf, PanelIdEnum.Charge);
            RegisterPanelModule(PanelIdEnum.ChargeOther, PanelIdEnum.Charge);
        }
    }
}
