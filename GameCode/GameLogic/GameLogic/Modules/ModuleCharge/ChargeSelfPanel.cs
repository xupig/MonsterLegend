﻿
using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCharge
{
    public class ChargeSelfPanel : BasePanel
    {
        private ChargeSelfView _chargeView;
        private KToggleGroup _toggleGroupTitle;

        protected override void Awake()
        {
            base.Awake();

            _chargeView = AddChildComponent<ChargeSelfView>("ScrollPage_chongzji");
        }

        public override void OnShow(object data)
        {
            _chargeView.OnShow();
        }

        public override void OnClose()
        {
            _chargeView.OnClose();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.ChargeSelf; }
        }

    }
}
