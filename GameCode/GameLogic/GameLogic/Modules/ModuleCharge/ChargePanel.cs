﻿using Common.Base;
using Common.Events;
using Game.UI.UIComponent;
using GameData;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCharge
{
    public class ChargePanel : BasePanel
    {
        private KToggleGroup _toggleGroupTitle;

        protected override void Awake()
        {
            base.Awake();

            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _toggleGroupTitle = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            _toggleGroupTitle.SelectIndex = 0;

            SetTabList((int)FunctionId.Charge, _toggleGroupTitle);
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            if (data is int)
            {
                ShowSubViewByIndex((int)data);
            }
            else if (data is int[])
            {
                int[] list = data as int[];
                ShowSubViewByIndex(list[0], list[1]);
            }
            else
            {
                ShowSubViewByIndex(1);
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
            PanelIdEnum.Vip.Close();
            PanelIdEnum.ChargeSelf.Close();
            PanelIdEnum.ChargeOther.Close();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Charge; }
        }

        private void AddEventListener()
        {
            _toggleGroupTitle.onSelectedIndexChanged.AddListener(OnToggleGroupIndexChange);

            EventDispatcher.AddEventListener<int>(ChargeEvents.ChangeChargeTab, ShowSubViewByIndex);
        }

        private void RemoveEventListener()
        {
            _toggleGroupTitle.onSelectedIndexChanged.RemoveListener(OnToggleGroupIndexChange);

            EventDispatcher.RemoveEventListener<int>(ChargeEvents.ChangeChargeTab, ShowSubViewByIndex);
        }

        private void OnToggleGroupIndexChange(KToggleGroup kToggleGroup, int selectedIndex)
        {
            ShowSubViewByIndex(selectedIndex + 1);
        }

        private void ShowSubViewByIndex(int subTab, object data)
        {
            _toggleGroupTitle.SelectIndex = subTab - 1;
            if (subTab == 1)
            {
                PanelIdEnum.ChargeOther.Close();
                PanelIdEnum.Vip.Close();
                PanelIdEnum.ChargeSelf.Show(data);

            }
            else if (subTab == 2)
            {
                PanelIdEnum.ChargeSelf.Close();
                PanelIdEnum.Vip.Close();
                PanelIdEnum.ChargeOther.Show(data);
            }
        }

        private void ShowSubViewByIndex(int subTab)
        {
            ShowSubViewByIndex(subTab, null);
        }
    }
}
