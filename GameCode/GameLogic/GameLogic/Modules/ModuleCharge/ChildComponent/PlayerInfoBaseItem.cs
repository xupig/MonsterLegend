﻿using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCharge
{
    public abstract class PlayerInfoBaseItem:KList.KListItemBase
    {
        protected KContainer _checkmark;
        protected IconContainer _headIcon;
        protected StateText _txtHeadLevel;
        protected StateText _txtPlayerName;
        protected StateText _txtVipLevel;
        protected StateText _txtPlayerFightForce;

        private uint _onlinestate = 1;

        protected override void Awake()
        {
            _checkmark = GetChildComponent<KContainer>("Container_selected");
            _checkmark.Visible = false;
            _headIcon = AddChildComponent<IconContainer>("Container_icon");
            _txtHeadLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            _txtPlayerName = GetChildComponent<StateText>("Label_txtwanjiamingzi");
            _txtVipLevel = GetChildComponent<StateText>("Label_txtDengji");
            _txtPlayerFightForce = GetChildComponent<StateText>("Label_txtZhanli02");
        }

        public override bool IsSelected
        {
            get
            {
                return _checkmark.Visible;
            }
            set
            {
                _checkmark.Visible = value;
            }
        }

        protected void SetPlayerFightForce(uint fightForce)
        {
            _txtPlayerFightForce.CurrentText.text = fightForce.ToString();
        }

        protected void SetPlayerName(string name)
        {
            _txtPlayerName.CurrentText.text = name;
        }

        protected void SetPlayerLevel(uint level)
        {
            _txtHeadLevel.CurrentText.text = level.ToString();
        }

        protected void SetVipLevel(uint vipLevel)
        {
            _txtVipLevel.CurrentText.text = string.Empty;
        }

        protected void SetHeadIcon(uint vocation , uint onlinestate)
        {
            _onlinestate = onlinestate;
            _headIcon.SetIcon(MogoPlayerUtils.GetSmallIcon((int)vocation), UpdateOnlineState);
        }

        private void UpdateOnlineState(GameObject go)
        {
            if (_onlinestate == 1)
            {
                SetItemGray(1);
            }
            else
            {
                SetItemGray(0);
            }
        }

        private void SetItemGray(float gray)
        {
            ImageWrapper[] imgWrappers = _headIcon.GetComponentsInChildren<ImageWrapper>();
            for (int i = 0; i < imgWrappers.Length; i++)
            {
                imgWrappers[i].SetGray(gray);
            }
        }
    }
}
