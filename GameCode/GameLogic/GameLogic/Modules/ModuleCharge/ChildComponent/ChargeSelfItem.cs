﻿using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameLoader.Utils;
using GameMain.Entities;
using Common.Base;
using MogoEngine;
using GameLoader.Config;
using Common;
using MogoEngine.Utils;
using UnityEngine;
using GameLoader.PlatformSdk;
using Common.Data;
using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;
using Game.UI;
using System.Net;

namespace ModuleCharge
{
    public class ChargeSelfItem : KList.KListItemBase
    {
        private StateText _txtPrice;
        private StateText _txtState;
        private StateText _txtDiscount;
        private StateText _diamondCount;
        private StateImage _imgMonthlyCard;
        private StateImage _imgRecommond;
        private StateImage _imgPurchase;
        private KContainer _discountContainer;
        private IconContainer _diamondIconContainer;
        private IconContainer _iconContainer;
        private KDummyButton _clickButton;
        private charge_item _data;

        protected override void Awake()
        {
            _txtPrice = GetChildComponent<StateText>("Label_txtjiazhi");
            _txtState = GetChildComponent<StateText>("Label_txtzhuangtai");
            _txtState.GetComponent<TextWrapper>().alignment = TextAnchor.MiddleCenter;
            _txtDiscount = GetChildComponent<StateText>("Container_youhui/Label_txtXinxi");
            _txtDiscount.GetComponent<TextWrapper>().alignment = TextAnchor.MiddleCenter;
            _diamondCount = GetChildComponent<StateText>("Container_jiazhi/Label_txtXiaonianka");
            _imgMonthlyCard = GetChildComponent<StateImage>("Image_biaoshiIconyueka");
            _imgRecommond = GetChildComponent<StateImage>("Image_biaoshiIcontuijian");
            _imgPurchase = GetChildComponent<StateImage>("Image_xiangou");
            _diamondIconContainer = AddChildComponent<IconContainer>("Container_jiazhi/Container_icon");
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _discountContainer = GetChildComponent<KContainer>("Container_youhui");
            _clickButton = gameObject.AddComponent<KDummyButton>();
            AddEventListener();
        }

        public override void Dispose()
        {
            _data = null;
            RemoveEventListener();
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as charge_item;
                _data.ios_product = MogoProtoUtils.ParseByteArrToString(_data.ios_product_bytes);
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            Refresh();
        }

        private void AddEventListener()
        {
            _clickButton.onClick.AddListener(OnChargeItemClick);
            EventDispatcher.AddEventListener(RewardEvents.UPDATE_CARD_INFO, RefreshCardInfo);
            EventDispatcher.AddEventListener(ChargeEvents.CheckChargeConditionOK, CanCharge);
        }

        private void RemoveEventListener()
        {
            _clickButton.onClick.RemoveListener(OnChargeItemClick);
            EventDispatcher.RemoveEventListener(RewardEvents.UPDATE_CARD_INFO, RefreshCardInfo);
            EventDispatcher.RemoveEventListener(ChargeEvents.CheckChargeConditionOK, CanCharge);
        }

        private void Refresh()
        {
            string moneyChar = MogoLanguageUtil.GetContent((int)_data.money_char);
            _txtPrice.CurrentText.text = string.Format("{0}{1}", moneyChar, _data.money);

            _txtState.Visible = false;

            ///首充额外赠送
            _discountContainer.Visible = false;
            if (_data.first_charge > 0 && _data.every_charge > 0)
            {
                _discountContainer.Visible = true;
                if (_data.is_first == 1)
                {
                    _txtDiscount.CurrentText.text = MogoLanguageUtil.GetContent((int)_data.desc_list[0], _data.every_charge, _data.first_charge);
                }
                else
                {
                    _txtDiscount.CurrentText.text = MogoLanguageUtil.GetContent((int)_data.desc_list[1], _data.every_charge);
                }
            }
            else if (_data.first_charge > 0)
            {
                if (_data.is_first == 1)
                {
                    _discountContainer.Visible = true;
                    _txtDiscount.CurrentText.text = MogoLanguageUtil.GetContent((int)_data.desc_list[0],_data.first_charge);
                }
            }
            else if (_data.every_charge > 0)
            {
                _discountContainer.Visible = true;
                _txtDiscount.CurrentText.text = MogoLanguageUtil.GetContent((int)_data.desc_list[0], _data.every_charge);
            }
            _diamondCount.CurrentText.text = string.Format("x{0}",_data.diamond);
            _diamondIconContainer.SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_DIAMOND));

            _iconContainer.SetIcon((int)_data.icon);

            _imgMonthlyCard.Visible = _data.tag_type == 3;
            _imgRecommond.Visible = _data.tag_type == 2;
            _imgPurchase.Visible = _data.tag_type == 1;

            RefreshCardInfo();
        }

        private void RefreshCardInfo()
        {
            if(_data.tag_type != 3) return;
            DailyTaskData dailyTaskData = PlayerDataManager.Instance.RewardData.GetDailyTaskData();
            if (dailyTaskData.IsMonthCardValid() == false)
            {
                return;
            }

            uint leftSeconds = dailyTaskData.TaskCardInfo.leftSeconds;
            int day = Mathf.CeilToInt((float)leftSeconds / (24 * 3600 * 1.0f));
            _discountContainer.Visible = leftSeconds > 0;
            _txtDiscount.CurrentText.text = MogoLanguageUtil.GetContent(111014, day);
        }

        private void OnChargeItemClick()
        {
            ///判断月卡是否过期
            DailyTaskData dailyTaskData = PlayerDataManager.Instance.RewardData.GetDailyTaskData();
            uint leftSeconds = dailyTaskData.TaskCardInfo.leftSeconds;
            if (leftSeconds > 0 && _data.tag_type == 3 && ChargeManager.IgnoreClientChargeCheck == false)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(111015), PanelIdEnum.Charge);
                return;
            }

            ChargeManager.Instance.CheckChargeCondition((int)_data.charge_id);
        }

        private void CanCharge()
        {
            if (ChargeManager.ChargeId != _data.charge_id) return;
            ChargeManager.Instance.JumpToCharge(_data);  
        }     
    }
}
