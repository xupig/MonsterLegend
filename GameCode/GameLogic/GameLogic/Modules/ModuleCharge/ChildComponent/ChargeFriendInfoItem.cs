﻿
using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCharge
{
    public class ChargeFriendInfoItem:PlayerInfoBaseItem
    {
        private KProgressBar _intimateProgressBar;
        private StateText _txtIntimate;
        private LoveStarItemList _loveStarItemList;
        private PbFriendAvatarInfo _data;

        protected override void Awake()
        {
            base.Awake();
            _intimateProgressBar = GetChildComponent<KProgressBar>("ProgressBar_jindu");
            _txtIntimate = GetChildComponent<StateText>("Label_txtJindu");
            _loveStarItemList = gameObject.AddComponent<LoveStarItemList>();

            AddEventListener();
        }

        public override void Dispose()
        {
            RemoveEventListener();
            _data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbFriendAvatarInfo;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            RefreshData();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<UInt64>(FriendEvents.FRIEND_INTIMATE_CHANGE, OnIntimateChange);
            EventDispatcher.AddEventListener<UInt64>(FriendEvents.FRIEND_ONLINE_STATE_CHANGE, OnFriendOnlineStateChange);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<UInt64>(FriendEvents.FRIEND_INTIMATE_CHANGE, OnIntimateChange);
            EventDispatcher.RemoveEventListener<UInt64>(FriendEvents.FRIEND_ONLINE_STATE_CHANGE, OnFriendOnlineStateChange);
        }

        private void RefreshData()
        {
            SetPlayerName(_data.name);
            SetPlayerLevel(_data.level);
            SetPlayerFightForce(_data.fight);
            SetVipLevel(_data.vip_level);
            SetHeadIcon(_data.vocation, _data.online);

            UpdateInitmate();
        }

        private void UpdateInitmate()
        {
            float level = intimate_helper.GetIntimateLevel((int)_data.degree);
            int max = intimate_helper.GetIntimateLevelMax((int)_data.degree);
            _loveStarItemList.SetLevel(level);
            _intimateProgressBar.Value = (_data.degree) / (float)max;
            _txtIntimate.CurrentText.text = string.Format("{0}/{1}", _data.degree, max);
        }

        private void OnIntimateChange(UInt64 dbId)
        {
            if (_data != null && _data.dbid == dbId)
            {
                UpdateInitmate();
            }
        }

        private void OnFriendOnlineStateChange(UInt64 dbId)
        {
            if (_data != null && dbId == _data.dbid)
            {
                SetHeadIcon(_data.vocation, _data.online);
            }
        }
    }
}
