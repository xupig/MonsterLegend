﻿using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCharge
{

    public class ChargeGuildInfoItem : PlayerInfoBaseItem
    {
        private StateText _txtMilitaryRank;

        private PbGuildMemberInfo _data;

        protected override void Awake()
        {
            base.Awake();
            _txtMilitaryRank = GetChildComponent<StateText>("Label_txtzhiwei");
        }

        public override void Dispose()
        {
            _data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbGuildMemberInfo;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            RefreshData();
        }

        private void RefreshData()
        {
            SetPlayerName(_data.name);
            SetHeadIcon(_data.vocation, _data.online);
            SetPlayerLevel(_data.level);
            SetVipLevel(0);
            SetPlayerFightForce(_data.fight);
            _txtMilitaryRank.CurrentText.text = MogoLanguageUtil.GetContent(74626 + (int)_data.guild_position - 1);
        }
    }
}
