﻿using Common.Base;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Config;
using GameLoader.PlatformSdk;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCharge
{
    public class ChargeOtherItem:KList.KListItemBase
    {
        private IconContainer _chargeInfoIcon;
        private StateText _txtDiamondNum;
        private StateText _txtIntimateValue;
        private StateText _txtPrice;
        private KButton _chargeByOtherButton;

        private charge_item _data;

        protected override void Awake()
        {
            _chargeInfoIcon = AddChildComponent<IconContainer>("Container_icon");
            _txtDiamondNum = GetChildComponent<StateText>("Label_txtZuanshi");
            _txtIntimateValue = GetChildComponent<StateText>("Label_txtQinmizhi");
            _txtPrice = GetChildComponent<StateText>("Label_txtRMB");

            _chargeByOtherButton = GetChildComponent<KButton>("Button_daichong");
            _chargeByOtherButton.onClick.AddListener(OnChargeButtonClick);
        }

        public override void Dispose()
        {
            _chargeByOtherButton.onClick.RemoveListener(OnChargeButtonClick);
            _data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as charge_item;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            Refresh();
        }

        private void OnChargeButtonClick()
        {
            if (ChargeOtherListView.SelectedPlayerInfo == null)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(111017), PanelIdEnum.Charge);
            }
            else
            {
                PbFriendAvatarInfo friendAvatarInfo = ChargeOtherListView.SelectedPlayerInfo as PbFriendAvatarInfo;
                
                if (friendAvatarInfo != null)
                {
                    if (friendAvatarInfo.online == 0)
                    {
                        ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(111018), PanelIdEnum.Charge);
                        return;
                    }
                    else if(friendAvatarInfo.degree < _data.honey_value)
                    {
                        ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(111019, _data.honey_value), PanelIdEnum.Charge);
                        return;
                    }
                    string content = MogoLanguageUtil.GetContent(111020, friendAvatarInfo.name, _data.diamond);
                    MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, OnConfirmCharge, OnCancel, MogoLanguageUtil.GetContent(111021), MogoLanguageUtil.GetContent(111022), null, true, false, true);
                }
                else
                {
                    PbGuildMemberInfo guildMemberInfo = ChargeOtherListView.SelectedPlayerInfo as PbGuildMemberInfo;
                    if (guildMemberInfo.online == 0)
                    {
                        ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(111018), PanelIdEnum.Charge);
                        return;
                    }
                    else if (_data.honey_value != 0)
                    {
                        ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(111019, _data.honey_value), PanelIdEnum.Charge);
                        return;
                    }
                    string content = MogoLanguageUtil.GetContent(111027, guildMemberInfo.name, _data.diamond);
                    MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, OnConfirmCharge, OnCancel, MogoLanguageUtil.GetContent(111021), MogoLanguageUtil.GetContent(111022), null, true, false, true);
                }
            }
        }

        private void OnCancel()
        {
        }

        private void OnConfirmCharge()
        {
            //todo：充值逻辑，平台相关
            if (ChargeOtherListView.SelectedPlayerInfo is PbFriendAvatarInfo)
            {
                ChargeManager.Instance.JumpToCharge(_data, (ChargeOtherListView.SelectedPlayerInfo as PbFriendAvatarInfo).dbid);
            }
            else if (ChargeOtherListView.SelectedPlayerInfo is PbGuildMemberInfo)
            {
                ChargeManager.Instance.JumpToCharge(_data, (ChargeOtherListView.SelectedPlayerInfo as PbGuildMemberInfo).dbid);
            }
        }

        private void Refresh()
        {
            _chargeInfoIcon.SetIcon((int)_data.icon);
            _txtDiamondNum.CurrentText.text = string.Format("{0}x{1}",item_helper.GetName(public_config.MONEY_TYPE_DIAMOND),  _data.diamond);
            _txtIntimateValue.Visible = _data.honey_value != 0;
            _txtIntimateValue.CurrentText.text = _data.honey_value.ToString();

            string moneyChar = MogoLanguageUtil.GetContent((int)_data.money_char);
            _txtPrice.CurrentText.text = string.Format("{0}{1}", moneyChar, _data.money);

            RefreshChargeByOtherButton();
        }

        private void RefreshChargeByOtherButton()
        {
            if(_data.honey_value == 0)
            {
                SetItemGray(1);
            }
            else
            {
                PbFriendAvatarInfo friendAvatarInfo = ChargeOtherListView.SelectedPlayerInfo as PbFriendAvatarInfo;
                if(friendAvatarInfo != null && friendAvatarInfo.degree >= _data.honey_value)
                {
                    SetItemGray(0);
                }
                else
                {
                    SetItemGray(1);
                }
            }
        }

        private void SetItemGray(float gray)
        {
            ImageWrapper[] imgWrappers = _chargeByOtherButton.GetComponentsInChildren<ImageWrapper>(true);
            for (int i = 0; i < imgWrappers.Length; i++)
            {
                imgWrappers[i].SetGray(gray);
            }
        }
    }
}
