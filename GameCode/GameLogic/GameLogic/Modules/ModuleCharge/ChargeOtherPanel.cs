﻿
using Common.Base;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCharge
{
    public class ChargeOtherPanel:BasePanel
    {
        public static int SelectedType = 0;
        private const int FRIEND = 0;
        private const int GUILD = 1;

        private KToggleGroup _chargeOtherCategory;
        private ChargeGuildView _chargeGuildView;
        private ChargeFriendView _chargeFriendView;
        private ChargeOtherListView _chargeOtherListView;
        private ChargePlayerSearchView _searchView;
        
        protected override void Awake()
        {
            base.Awake();

            _chargeOtherCategory = GetChildComponent<KToggleGroup>("Container_left/ToggleGroup_mulu");
            _chargeFriendView = AddChildComponent<ChargeFriendView>("Container_left/Container_haoyou");
            _chargeGuildView = AddChildComponent<ChargeGuildView>("Container_left/Container_gonghui");
            _chargeOtherListView = AddChildComponent<ChargeOtherListView>("Container_right");
            _searchView = AddChildComponent<ChargePlayerSearchView>("Container_left/Container_sousuo");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.ChargeOther; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            if (data != null)
            {
                OnSelectedChargeOtherTypeChanged(_chargeOtherCategory, (int)data);
            }
            else
            {
                OnSelectedChargeOtherTypeChanged(_chargeOtherCategory, FRIEND);
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _chargeOtherCategory.onSelectedIndexChanged.AddListener(OnSelectedChargeOtherTypeChanged);
            _searchView.onSearchResultChanged.AddListener(OnSearchResultChange);
            _chargeFriendView.onSelectedFriendChanged.AddListener(OnSelectedFriendChanged);
            _chargeGuildView.onSelectedGuildMemberChanged.AddListener(OnSelectedGuildMemberChanged);
        }

        private void RemoveEventListener()
        {
            _chargeOtherCategory.onSelectedIndexChanged.RemoveListener(OnSelectedChargeOtherTypeChanged);
            _searchView.onSearchResultChanged.RemoveListener(OnSearchResultChange);
            _chargeFriendView.onSelectedFriendChanged.RemoveListener(OnSelectedFriendChanged);
            _chargeGuildView.onSelectedGuildMemberChanged.RemoveListener(OnSelectedGuildMemberChanged);
        }

        private void OnSelectedGuildMemberChanged(PbGuildMemberInfo guildMemberInfo)
        {
            _chargeOtherListView.ShowChargeInfoByPlayerInfo(guildMemberInfo);
        }

        private void OnSelectedFriendChanged(PbFriendAvatarInfo friendInfo)
        {
            _chargeOtherListView.ShowChargeInfoByPlayerInfo(friendInfo);
        }

        private void OnSelectedChargeOtherTypeChanged(KToggleGroup toggleGroup, int index)
        {
            SelectedType = index;
            _searchView.ShowSearchType(index);
            if (toggleGroup.SelectIndex == FRIEND)
            {
                _chargeGuildView.Visible = false;
                _chargeFriendView.Visible = true;
            }
            else if (toggleGroup.SelectIndex == GUILD)
            {
                _chargeFriendView.Visible = false;
                _chargeGuildView.Visible = true;
            }
            _chargeOtherListView.ShowChargeInfoByPlayerInfo(null);
        }

        private void OnSearchResultChange(object searchResult)
        {
            if (searchResult is List<PbFriendAvatarInfo>)
            {
                _chargeFriendView.ShowSearchResult(searchResult as List<PbFriendAvatarInfo>);
            }
            else if (searchResult is List<PbGuildMemberInfo>)
            {
                _chargeGuildView.ShowSearchResult(searchResult as List<PbGuildMemberInfo>);
            }
        }
    }
}
