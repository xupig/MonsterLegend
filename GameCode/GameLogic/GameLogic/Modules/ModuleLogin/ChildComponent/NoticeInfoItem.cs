﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Events;
using Game.UI.UIComponent;
using MogoEngine.Events;
using UnityEngine.UI;

namespace ModuleLogin.ChildComponent
{
    /// <summary>
    /// 单条公告显示项
    /// </summary>
    public class NoticeInfoItem : KList.KListItemBase
    {
        private NoticeInfo _data;
        private StateText _textContent;
        
        protected override void Awake()
        {
            _textContent = GetChildComponent<StateText>("Label_txtNeirong");
            _textContent.CurrentText.lineSpacing = 1.2f;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as NoticeInfo;
                Refresh();
            }
        }

        public override void Dispose()
        {
            _data = null;
            _textContent = null;
        }

        protected void Refresh()
        {
            if (_data != null)
            {
                _textContent.CurrentText.text = _data.noticeContent;
                _textContent.RecalculateAllStateHeight();
                RecalculateSize();
                Visible = true;
            }
            else
            {
                Visible = false;
            }
        }
    }
}
