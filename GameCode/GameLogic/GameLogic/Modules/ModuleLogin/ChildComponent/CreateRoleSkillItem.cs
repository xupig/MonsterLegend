﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine;

namespace ModuleLogin.ChildComponent
{
    /// <summary>
    /// 创建角色界面--显示技能项
    /// </summary>
    public class CreateRoleSkillItem : KList.KListItemBase
    {
        private KContainer _content;
        private KContainer _qingdai;
        private ProficientInfo _data;

        private PentagonImage _pentagonImage;     //雷大图
        private StateText _txtSkillExplain;       //技能说明
        private StateText _txtOpenLimit;          //技能开放条件
        private StateText _txtSkillName;          //技能名称
        private StateIcon _skillIcon;             //技能图标

        private StateText _vocationDutyDesc;      //队伍描述
        private StateText _vocationDutyName;      //队伍名称
        private IconContainer _teamIcon;          //技能图标 

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as ProficientInfo;
                Refresh();
            }
        }

        public override void Dispose()
        {
            _data = null;
        }

        protected override void Awake()
        {
            _content = GetChildComponent<KContainer>("Container_content");
            _qingdai = GetChildComponent<KContainer>("Container_jingqingqidai");
            _pentagonImage = _content.GetChildComponent<StateImage>("Container_leidatu/Image_pentagon").CurrentImage as PentagonImage;
            _vocationDutyDesc = _content.GetChildComponent<StateText>("Container_jieshi/Label_txtZhizeDesc");
            _vocationDutyName = _content.GetChildComponent<StateText>("Container_jieshi/Label_txtZhiye");
            _teamIcon = _content.AddChildComponent<IconContainer>("Container_jieshi/Container_teamDuty");
            _vocationDutyDesc.ChangeAllStateText("");

            _txtSkillExplain = _content.GetChildComponent<StateText>("Label_txtZhineng");
            _txtOpenLimit = _content.GetChildComponent<StateText>("Label_txtOpenLimit");
            _txtSkillName = _content.GetChildComponent<StateText>("Label_txtSkillName");
            _skillIcon = _content.GetChildComponent<StateIcon>("Container_stateIcon");
            Refresh();
        }

        protected void Refresh()
        {
            if (_data != null)
            {
                _txtOpenLimit.CurrentText.text = _data.GetLevel();
                _txtSkillName.CurrentText.text =_data.GetName();
                _txtSkillExplain.CurrentText.text = MogoStringUtils.ReplaceChangeLine(_data.GetDiscription());
                _vocationDutyDesc.CurrentText.text = MogoLanguageUtil.GetContent(6006007); //_data.GetVocationDutyDesc();
                _vocationDutyName.CurrentText.text = _data.GetVocationDutyName();
                _pentagonImage.SetValues(_data.GetEvaluation());
                _skillIcon.SetIcon(_data.GetIconID());
                _teamIcon.SetIcon(_data.GetVocationDutyIconId());
                _content.Visible = true;
                _qingdai.Visible = false;
            }
            else
            {
                _content.Visible = false;
                _qingdai.Visible = true;
            }
        }
    }
}
