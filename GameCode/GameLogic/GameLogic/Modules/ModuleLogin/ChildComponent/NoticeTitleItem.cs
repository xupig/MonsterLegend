﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils;
using MogoEngine.Events;
using UnityEngine;

namespace ModuleLogin.ChildComponent
{
    /// <summary>
    /// 左边--公告标题项
    /// </summary>
    public class NoticeTitleItem : KList.KListItemBase
    {
        private NoticeInfo _data;

        private KToggle _ktoggle;
        private StateText _labNotSelect;
        private StateText _labSelect;
        private StateImage _backNew;
        private StateImage _backHot;

        protected override void Awake()
        {
            _ktoggle = GetChildComponent<KToggle>("Toggle_zhuangbei");
            _labNotSelect = _ktoggle.GetChildComponent<StateText>("image/label");
            _labSelect = _ktoggle.GetChildComponent<StateText>("checkmark/label");
            _backNew = _ktoggle.GetChildComponent<StateImage>("NEW");
            _backHot = _ktoggle.GetChildComponent<StateImage>("HOT");
            _ktoggle.onValueChanged.AddListener(OnValueChanged);
            _labNotSelect.ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);
            _labSelect.ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);
        }

        private void OnValueChanged(KToggle toggle, bool result)
        {
            RefreshLab();
            EventDispatcher.TriggerEvent<NoticeInfo>("select_notice_item", _data);
        }

        public override bool IsSelected
        {
            get
            {
                return _ktoggle.isOn;
            }
            set
            {
                _ktoggle.isOn = value;
                RefreshLab();
            }
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as NoticeInfo;
                Refresh();
            }
        }

        public override void Dispose()
        {
            _ktoggle.onValueChanged.RemoveListener(OnValueChanged);
            _data = null;
            _ktoggle = null;
            _labNotSelect = null;
            _labSelect = null;
            _backNew = null;
            _backHot = null;
        }

        protected void Refresh()
        {
            if (_data != null)
            {
                RefreshLab();
                UpdateStateImage(_data.typeTag);
                Visible = true;
            }
            else
            {
                Visible = false;
            }
        }

        private void RefreshLab()
        {
            _labSelect.ChangeAllStateText(_data.noticeType);
            _labNotSelect.ChangeAllStateText(_data.noticeType);
        }

        /// <summary>
        /// 控制图标显示
        /// </summary>
        /// <param name="typeTag"></param>
        private void UpdateStateImage(int typeTag)
        {
            _backHot.Visible = false;
            _backNew.Visible = false;
            switch (typeTag)
            {
                case 1:
                    _backNew.Visible = true;
                    break;
                case 2:
                    _backHot.Visible = true;
                    break;
            }
        }

    }
}
