﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Common;
using Common.Utils;
using GameData;
using GameLoader.Config;
using GameLoader.IO;
using GameLoader.Utils;
using GameLoader.Utils.XML;
using ModuleLogin;
using UnityEngine;

namespace Modules.ModuleLogin
{
    /// <summary>
    /// 登录公告管理类
    /// </summary>
    public class LoginNoticeMgr
    {
        private Action callback;
        private string localPath;

        public bool isNewNotice = true;      //是否有新公告[true:有新公告,false:没有新公告]
        public bool isShowLoginNoticeUI;     //是否需要自动弹出公告界面[true:需要,false:不需要]
        public static LoginNoticeMgr instance = new LoginNoticeMgr();

        public LoginNoticeMgr() 
        {
            data = new Dictionary<string, List<NoticeInfo>>();
            localPath = string.Concat(Application.persistentDataPath, ConstString.RutimeResource, ConstString.RutimeResourceConfig, "/LocalLoginNoticeSetting.so");
        }

        public Dictionary<string, List<NoticeInfo>> data { get; private set; }

        public void LoadData(Action callback)
        {
            if (data.Count < 1)
            {
                if (Application.isEditor)
                {//开发模式下
                    CreateNoticeDict();
                    GetNewNoticeState();
                    if (callback != null) callback();
                }
                else
                {//通过Web方式下载公告Xml文件
                    this.callback = callback;
                    string url = SystemConfig.GetValueInCfg("NoticeData");
                    string randomUrl = SystemConfig.GetRandomUrl(url);      //增加随机数地址
                    LoggerHelper.Debug("开始下载公告文件:" + randomUrl);
                    ResLoader.LoadWwwText(randomUrl, onLoad, false);
                }
            }
            else
            {
                if (callback != null) callback();
            }
        }


        //公告文件下载完成
        private void onLoad(string url, string content)
        {
            if (string.IsNullOrEmpty(content))
            { //下载失败
                LoggerHelper.Info("公告文件:" + url + " 下载失败！");
                if (callback != null) callback();
                return;
            }
            CreateNoticeDict(content);
            //GetNewNoticeState();             //让公共一直弹出
            if (callback != null) callback();
        }

        //发布模式下
        private void CreateNoticeDict(string content)
        {
            data.Clear();
            XMLNode rootNode = XMLLoader.Parse(content);
            XMLNodeList colorList = rootNode.GetNodeList("root>0>color");
 
            string id = null;
            string beta = null;
            string newornot = null;
            string noticeType = null;
            string noticeTypeTag = null;
            string noticeContent = null;
            foreach (XMLNode node in colorList)
            {
                id = node.GetNode("id_i>0").GetValue("_text");                               //公告id 
                beta = node.GetNode("beta_i>0").GetValue("_text");                           //版本号
                newornot = node.GetNode("newornot_i>0").GetValue("_text");                   //公告显示标记
                if (!string.IsNullOrEmpty(newornot) && newornot.Equals("1"))                 //1:显示公告，非1：不显示公告
                {//这次需要显示的公告
                    noticeType = node.GetNode("notice_type_s>0").GetValue("_text");          //公告类型ID列表(多个ID用逗号分割)
                    noticeTypeTag = node.GetNode("type_tag_i>0").GetValue("_text");          //公告类型描述ID列表(多个ID用逗号分割)
                    noticeContent = node.GetNode("notice_content_s>0").GetValue("_text");    //公告内容ID列表(多个ID用逗号分割)
                    
                    if (string.IsNullOrEmpty(noticeType)) continue;
                    if (!data.ContainsKey(noticeType)) data.Add(noticeType, new List<NoticeInfo>());
                    int typeId = int.Parse(noticeTypeTag);
                    NoticeInfo item = new NoticeInfo();
                    item.id = int.Parse(id);
                    item.beta = int.Parse(beta);
                    //item.typeId = typeId;
                    item.typeTag = typeId;
                    item.noticeType = MogoStringUtils.ConvertStyle(noticeType.Replace("\\n", "\n"));
                    item.noticeContent = MogoStringUtils.ConvertStyle(noticeContent.Replace("\\n", "\n"));
                    data[noticeType].Add(item);
                    LoggerHelper.Info(string.Format("====  id:{0},newornot:{1}", id, newornot));
                }
            }
            LoggerHelper.Debug("[发布模式] data.count:" + data.Count);
        }

        //开发模式下
        private void CreateNoticeDict()
        {
            data.Clear();
            foreach (login_notic noticeData in XMLManager.login_notic.Values)
            {
                //noticeData.__newornot==1:表示当前要发布的公告
                NoticeInfo item = null;
                if (noticeData.__newornot == 1)
                {
                    if (!data.ContainsKey(noticeData.__notice_type)) data.Add(noticeData.__notice_type, new List<NoticeInfo>());
                    item = new NoticeInfo();
                    item.id = noticeData.__id;
                    item.beta = noticeData.__beta;
                    //item.typeId = noticeData.__type_tag;
                    item.typeTag = noticeData.__type_tag;
                    item.noticeType = MogoStringUtils.ConvertStyle(noticeData.__notice_type.Replace("\\n", "\n"));
                    item.noticeContent = MogoStringUtils.ConvertStyle(noticeData.__notice_content.Replace("\\n", "\n"));
                    data[noticeData.__notice_type].Add(item);
                    LoggerHelper.Info(string.Format("id:{0},newornot:{1}", noticeData.__id, noticeData.__newornot));
                }
            }
            LoggerHelper.Debug("[开发模式] data.count:" + data.Count);
        }

        /// <summary>
        /// 获取是否有新公告状态
        /// [true:有新公告,false:没有新公告]
        /// </summary>
        private void GetNewNoticeState()
        {
            Dictionary<int, int> dict = Read();
            LoggerHelper.Info("==== local dict:" + dict);
            if (dict == null) dict = new Dictionary<int, int>();

            foreach (List<NoticeInfo> list in data.Values)
            {
                foreach (NoticeInfo item in list)
                {
                    if (dict.ContainsKey(item.id))
                    {
                        if (dict[item.id] < item.beta)
                        {//本地缓存id=item.__id的公告其本地beta比服务器中低，表示有新公告
                            dict[item.id] = item.beta;
                            isNewNotice = true;
                        }
                    }
                    else
                    {//本地还没有该公告id,则表示有新公告
                        dict.Add(item.id, item.beta);
                        isNewNotice = true;
                    }
                }
            }

            //重新写入本地
            LoggerHelper.Info("==== isNewNotice:" + isNewNotice + ",dict:" + dict.Count);
            if (dict.Count > 0 && isNewNotice) Write(dict);
        }

        private Dictionary<int, int> Read()
        {
            try
            {
                if (File.Exists(localPath))
                {
                    using (FileStream stream = File.OpenRead(localPath))
                    {
                        BinaryFormatter formatter = new BinaryFormatter();
                        return (Dictionary<int, int>)formatter.Deserialize(stream);
                    }
                }
                else return null;
            }
            catch (Exception ex)
            {
                LoggerHelper.Error("本地文件：" + ConstString.LocalSettingPath + " 反序列化失败! reason:" + ex.StackTrace);
                return null;
            }
        }

        private void Write(Dictionary<int,int> data)
        {
            string folder = Path.GetDirectoryName(localPath);
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            if (File.Exists(localPath)) File.Delete(localPath);
            using (FileStream stream = File.OpenWrite(localPath))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, data);
            }
        }
    }
}
