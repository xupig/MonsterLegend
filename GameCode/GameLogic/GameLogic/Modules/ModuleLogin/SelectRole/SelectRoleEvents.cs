﻿using System;
using System.Collections.Generic;


namespace ModuleLogin
{
    public class SelectRoleEvents
    {
        public const string CLICK_CREATED_ROLE_ITEM = "clickCreatedRoleItem";
        public const string CLICK_NEW_ROLE_ITEM = "clickNewRoleItem";
        //
        public const string SET_NEWROLE_PROFICIENT = "setNewRoleProficient";
        public const string NEW_ROLE_PLAY_ACTION = "newRolePlayAction";

        /// <summary>
        /// 创建角色失败
        /// </summary>
        public const string S_CREATE_ROLE_FAIL = "createRoleFail";
        /// <summary>
        /// 开始游戏请求|创建角色请求超时
        /// </summary>
        public const string C_SERVER_REQ_TIMEOUT = "serverReqTimeout";

        /// <summary>
        /// 请求创建已经创建角色
        /// </summary>
        public const string ASK_CREATE_ROLE = "askCreateCreatedRole";

        /// <summary>
        /// 请求创建新角色
        /// </summary>
        public const string ASK_NEW_ROLE = "askCreateNewRole";

    }
}
