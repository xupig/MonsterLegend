﻿using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;
using Common.Utils;


namespace ModuleLogin
{
    public class ProficientInfo
    {
        private NewRole _role;
        spell_proficient proficient = null;
        private int _group = 0;

        //private List<int> evalution;

        private string _name = string.Empty;
        private string _discription = string.Empty;
        /// <summary>
        /// 专精解锁等级
        /// </summary>
        private string _level = string.Empty;

        public ProficientInfo(NewRole role,int group)
        {
            this._role = role;
            this._group = group;
            GetProficient();
        }

        private spell_proficient GetProficient()
        {
            if (proficient == null)
            {
                proficient = skill_helper.GetSpellProficient(_role.vocation, _group);
            }
            return proficient;
        }

        public string GetName()
        {
            if (_name == string.Empty)
            {
                if (GetProficient() == null) return "";

                _name = MogoLanguageUtil.GetContent(proficient.__name); // XMLManager.chinese[proficient.__name].__content;
                 //_name += "_vocation_" + _role.vocation;
                 //LoggerHelper.Error("13______________name:    " + _name);
            }
            return _name;
        }

        public string GetDiscription()
        {
            if (_discription == string.Empty)
            {
                if (GetProficient() == null) return "";
                _discription = MogoLanguageUtil.GetContent(proficient.__description); // XMLManager.chinese[proficient.__description].__content;
            }
            return _discription;
        }

        public string GetLevel()
        {
            if (_level != string.Empty) return _level;

            string strLevel = GameData.XMLManager.global_params[75].__value;
            var levels = strLevel.Split(',');
            //LoggerHelper.Error("[ProficientInfo:GetLevel]=>1_______________levels.Length:   " + levels.Length);
            int level = int.Parse(levels[_group - 1]); ;
            _level = level > 0 ? "LV" + level + MogoLanguageUtil.GetContent(6005018) : MogoLanguageUtil.GetContent(6005017);  // "初始分支";
            //LoggerHelper.Error("_group:" + _group + ",strLevel:" + strLevel + ",_level:" + _level);
            return _level;
        }

        public string GetVocationDesc()
        {
            //return proficient != null ? MogoLanguageUtil.GetContent(proficient.__vocation_desc) : string.Empty;
            return MogoLanguageUtil.GetContent(proficient.__vocation_desc);
        }

        public int GetVocationDutyNameId() 
        {
            //return proficient != null ? proficient.__vocation_duty_name : 0;
            return proficient.__vocation_duty_name;
        }

        public string GetVocationDutyName()
        {
            return MogoLanguageUtil.GetContent(proficient.__vocation_duty_name);
            //return proficient != null ? MogoLanguageUtil.GetContent(proficient.__vocation_duty_name) : string.Empty;
        }

        public string GetVocationDutyDesc()
        {
            return MogoLanguageUtil.GetContent(proficient.__vocation_duty_desc);
            //return proficient != null ? MogoLanguageUtil.GetContent(proficient.__vocation_duty_desc) : string.Empty;
        }

        public int GetVocationDutyIconId()
        {
            return proficient.__team_duty_icon;
        }

        public float[] GetEvaluation()
        {
            List<float> retList = new List<float>();
            if (GetProficient() == null)
            {
                LoggerHelper.Error("[ProficientInfo:GetEvaluation]=>GetProficient() == null,检查专精表数据是否填写正确!");
                return new float[] { 0, 0, 0, 0, 0 };
            } 
            GetIconID();
            proficient.__evaluation.ForEach((item) =>
                {
                    float num = float.Parse(item) / 10;
                    if (num <0) num = 0;
                    if (num > 1) num = 1;
                    //Debug.Log(string.Format("<color=#ffff00>num:    {0}</color>",num));
                    retList.Add(num);
                });
            return retList.ToArray();
        }

        public int GetIconID()
        {
            if (GetProficient() == null) return 0;
            return proficient.__icon;
        }

        

    }

  

}
