﻿#region 模块信息
/*==========================================
// 模块名：SelectRoleMusic
// 命名空间: ModuleLogin
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/07/25
// 描述说明：选角音乐
// 其他：
//==========================================*/
#endregion

using ACTSystem;
using Common.Data;
using Common.Structs;
using Game.Asset;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace ModuleLogin
{
    public enum SELECTROLE_TYPE
    {
        CREATEROLE = 0,

        SELECTROLE
    }

    public class SelectRoleMusic
    {
        private static Dictionary<Vocation, string> _musicDic = new Dictionary<Vocation, string>();
        private static Dictionary<Vocation, string> _voiceDic = new Dictionary<Vocation, string>();

        private SELECTROLE_TYPE _selectRoleType = SELECTROLE_TYPE.CREATEROLE;
        static SelectRoleMusic()
        {
            _musicDic.Add(Vocation.Warrior, "Sound/Music/login_zhanshi.mp3");
            _musicDic.Add(Vocation.Assassin, "Sound/Music/login_cike.mp3");
            _musicDic.Add(Vocation.Wizard, "Sound/Music/login_fashi.mp3");
            _musicDic.Add(Vocation.Archer, "Sound/Music/login_qiangshou.mp3");
            //
            _voiceDic.Add(Vocation.Warrior, "Sound/Login/login_Warror.mp3");
            _voiceDic.Add(Vocation.Assassin, "Sound/Login/login_gunner.mp3");
            _voiceDic.Add(Vocation.Wizard, "Sound/Login/login_wizard.mp3");
            _voiceDic.Add(Vocation.Archer, "Sound/Login/login_gunner.mp3");
        }

        private AudioSource _canas;
        private MusicTween _musicTween;
        private SelectRoleAudio _voiceAS = null;
        private AudioSource _voiceCanas;
        private BHRecoverMusic _recoverMusic = null;
        public float settingVolume
        {
            get { return PlayerDataManager.Instance.SettingData.GetSettingValue(SettingType.Music) / 100f; }
        }
        private uint _playTimerID = 0;
        private GameObject _musicGameObject;

        /// <summary>
        /// 重新播放定时器
        /// </summary>
        private uint _rePlayTimerID = 0;

        public SelectRoleMusic()
        {
            _musicTween = new MusicTween();
        }

        public void AddAudioSource(string name, Vocation vocation,SELECTROLE_TYPE selectRoleType)
        {
            this._selectRoleType = selectRoleType;
            GetMusicSlot(name);
            _canas = _musicGameObject.GetComponent<AudioSource>();
            if (_canas == null)
            {
                _canas = _musicGameObject.AddComponent<AudioSource>();
                ObjectPool.Instance.GetAudioClip(_musicDic[vocation], (clip) =>
                {
                    _recoverMusic.AddAudioClip(_musicDic[vocation]);
                    _canas.clip = clip;
                    _canas.volume = settingVolume;            
                    _canas.loop = false;
                    //_canas.Play();
                });
            }
            //
            if (selectRoleType == SELECTROLE_TYPE.CREATEROLE)
            {
                AddVoiceAudioSource(vocation);
            }
            
        }

        private void AddVoiceAudioSource(Vocation vocation)
        {
            if (_musicGameObject == null) return;
            SerchAudioClip(_voiceDic[vocation], (audioSource) =>
                {
                    if (audioSource != null)
                    {
                        _voiceCanas = audioSource;
                        return;
                    }
                    
                });
            if (_voiceCanas == null)
            {
                _voiceCanas = _musicGameObject.AddComponent<AudioSource>();
            }
            if (_voiceAS == null)
            {
                _voiceAS = new SelectRoleAudio(_voiceCanas);
            }
            if (_voiceCanas.clip == null)
            ObjectPool.Instance.GetAudioClip(_voiceDic[vocation], (clip) =>
            {
                _recoverMusic.AddAudioClip(_voiceDic[vocation]);
                _voiceCanas.clip = clip;
                _voiceCanas.volume = settingVolume;             
                _voiceCanas.loop = false;
                //_canas.Play();
            });
        }

        public void SerchAudioClip(string filePath,Action<AudioSource> callBack)
        {
            if (_musicGameObject == null)
            {
                callBack(null);
                return;
            }
            AudioSource[] audioSoures = _musicGameObject.GetComponentsInChildren<AudioSource>();
            if (audioSoures.Length >= 2)
            {
                for (int i = 0; i < audioSoures.Length; i++)
                {
                    string fileName = System.IO.Path.GetFileName(filePath);
                    fileName = fileName.Remove(fileName.LastIndexOf("."));    //去掉文件扩展名
                    if (audioSoures[i].clip.name == fileName)
                    {
                        callBack(audioSoures[i]);
                        return;
                    }
                }
            }
            callBack(null);
            return;
        }

        private void GetMusicSlot(string name)
        {
            string musicName = name + "_music";
            _musicGameObject = GameObject.Find(musicName);
            if (_musicGameObject == null)
            {
                _musicGameObject = new GameObject();
                _musicGameObject.name = name + "_music";
                _recoverMusic = _musicGameObject.AddComponent<BHRecoverMusic>();
                GameObject.DontDestroyOnLoad(_musicGameObject);
                //LoggerHelper.Error("1___________________________________");
            }
        }

        public void RemoveMusicSlot()
        {
            if (_musicGameObject == null) return;
            Transform.Destroy(_musicGameObject);
        }

        /// <summary>
        /// 目前播放时强进,音量立即达到设定值
        /// </summary>
        public void Play(float fadeTime = 1f)
        {
            if (_canas == null)
            {
                LoggerHelper.Error("[SelectRoleBase:Play]=>_canas == null");
                return;
            }
            //if (_canas.isPlaying == true) return;
            if (_canas.isPlaying == true)
            {
                TimerHeap.AddTimer(100, 0, () =>
                    {
                        Play(0.001f);
                    });
                return;
            }
            _canas.volume = settingVolume;
            //重新开始的时候,原来的定时器要复位
            _musicTween.ResetTimer();
            //定时器延时_fadeTime开始淡入
            ResetPlayTimer();
            _playTimerID = TimerHeap.AddTimer((uint)(fadeTime * 1000), 0, () =>
                {
                    ResetPlayTimer();
                    //_musicTween.Fade(0, settingVolume, _fadeTime, 20, PlayFinishedCallBack, PlayIntervalCallBack);
                    _canas.Play();
                    PlayVoice();

                    //启动重新播放定时器
                    ResetReplayTimer();
                    _rePlayTimerID = TimerHeap.AddTimer((uint)(_canas.clip.length * 1000f) + 2000, 0, () =>
                        {
                            ResetReplayTimer();
                            Play(fadeTime);
                        });
                });
            
        }

        private void PlayVoice()
        {
            if (_voiceAS == null) return;

            _voiceAS.Play();
        }

        private void PlayIntervalCallBack(float volume)
        {
            if (_canas == null) return;
            _canas.volume = volume;
        }

        private void PlayFinishedCallBack(float volume)
        {
            if (_canas == null) return;
            _canas.volume = volume;
        }

        /// <summary>
        /// 复位播放定时器
        /// </summary>
        private void ResetPlayTimer()
        {
            if (_playTimerID != 0)
            {
                TimerHeap.DelTimer(_playTimerID);
                _playTimerID = 0;
            }
        }

        private void ResetReplayTimer()
        {
            if (_rePlayTimerID != 0)
            {
                TimerHeap.DelTimer(_rePlayTimerID);
                _rePlayTimerID = 0;
            }
        }

        public void VoiceFadeOut()
        {
            if (_voiceAS == null) return;
            _voiceAS.FadeOut(1f);
        }

        /// <summary>
        /// 淡出
        /// </summary>
        public void FadeOut(float fadeTime = 1f)
        {
            //LoggerHelper.Error("[SelectRoleMusic:FadeOut]=>1_____________________canas.isPlaying:   " + _canas.isPlaying);
            VoiceFadeOut();
            ResetReplayTimer();
            if (_canas.isPlaying == false)
            {
                ResetPlayTimer();
                return;
            }
            //LoggerHelper.Error("[SelectRoleMusic:FadeOut]=>2____________________canas.isPlaying:   " + _canas.isPlaying);
            //LoggerHelper.Error("06______________fadeTime:    " + fadeTime);
            _musicTween.Fade(_canas.volume, 0, fadeTime, 20, MusicStopCallBack, MusicIntervalCallBack);

        }

        private void MusicIntervalCallBack(float volume)
        {
            if (_canas == null) return;
            _canas.volume = volume;
            //LoggerHelper.Error("[SelectRoleMusic:MusicIntervalCallBack]=>1_________volume:  " + volume);
        }

        private void MusicStopCallBack(float volume)
        {
            if (_canas == null) return;
            _canas.Stop();
            //LoggerHelper.Error("[SelectRoleMusic:MusicStopCallBack]=>2_________volume:  " + volume);
        }

    }
}
