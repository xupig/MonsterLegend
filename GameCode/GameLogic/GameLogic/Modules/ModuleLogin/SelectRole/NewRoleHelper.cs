﻿#region 模块信息
/*==========================================
// 模块名：NewRoleHelper
// 命名空间: ModuleLogin
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/06/25
// 描述说明：新建角色辅助信息
// 其他： 负责提供角色动画控制器,装备数据
//==========================================*/
#endregion

using ACTSystem;
using Common.Structs;
using GameData;
using GameLoader.Utils;
using System;
using System.Collections.Generic;

namespace ModuleLogin
{
    public class NewRoleHelper
    {
        private static NewRoleHelper _instance;
        public static NewRoleHelper instance
        {
            get
            {
                return _instance;
            }
        }

        public Dictionary<Vocation, string> animatorControllers;

        static NewRoleHelper()
        {
            _instance = new NewRoleHelper();       
        }

        public NewRoleHelper()
        {
            animatorControllers = new Dictionary<Vocation, string>();
            animatorControllers.Add(Vocation.Warrior, "Characters/Avatar/101/ani/101_controller_selectRole.controller");
            animatorControllers.Add(Vocation.Assassin, "Characters/Avatar/101/ani/101_controller_selectRole.controller");
            animatorControllers.Add(Vocation.Wizard, "Characters/Avatar/103/ani/103_controller_selectRole.controller");
            animatorControllers.Add(Vocation.Archer, "Characters/Avatar/104/ani/104_controller_selectRole.controller");

        }

        public string GetAnimatorControllerPath(Vocation vocation)
        {
            if (animatorControllers.ContainsKey(vocation)) return animatorControllers[vocation];
            return null;
        }

        #region 装备

        public int GetClothID(int vocation)
        {
            int clothID = int.Parse(XMLManager.create_player[vocation].__subtype[1]);
            //LoggerHelper.Error("[NewRoleHelper:GetClothID]=>1________________clothID:   " + clothID);
            return clothID;
        }

        public int GetWeaponID(int vocation)
        {
            int weaponID = int.Parse(XMLManager.create_player[vocation].__subtype[0]);
            //LoggerHelper.Error("[NewRoleHelper:GetWeaponID]=>1________________clothID:   " + weaponID);
            return weaponID;
        }

        public int GetWingID(int vocation)
        {
            int wingID = XMLManager.create_player[vocation].__wing;
            //LoggerHelper.Error("[NewRoleHelper:GetWingID]=>1________________clothID:   " + wingID);
            return wingID;
        }
        #endregion

        #region 时间列表


        #endregion





    }
}
