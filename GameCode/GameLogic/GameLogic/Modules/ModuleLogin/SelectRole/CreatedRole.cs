﻿#region 模块信息
/*==========================================
// 模块名：CreatedRole
// 命名空间: ModuleLogin
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/06/25
// 描述说明：选角信息
// 其他： 
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleLogin
{
    public class CreatedRole : SelectRoleBase
    {
        public CreatedRole()
            : base()
        {
            
        }
        
    }
}
