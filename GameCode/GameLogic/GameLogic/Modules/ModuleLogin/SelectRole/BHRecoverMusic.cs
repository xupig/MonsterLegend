﻿using Common;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleLogin
{
    public class BHRecoverMusic : MonoBehaviour
    {
        private bool _startRecoveryMusic = false;
        private int _nowFrame = 0;
        private List<string> _audioClipList = null;

        public void AddAudioClip(string name)
        {
            if (_audioClipList == null)
            {
                _audioClipList = new List<string>();
            }
            _audioClipList.Add(name);
        }

        void Update()
        {
            _nowFrame++;
            if (_nowFrame <30)
            {
                return;
            }

            CheckMusicCanRecovery();
            _nowFrame = 0;
        }

        private void CheckMusicCanRecovery()
        {
            if (_startRecoveryMusic == true) return;
            //bool isInCity = GameSceneManager.GetInstance().IsInCity();
            //  Debug.LogError("1____________isInCity:    " + isInCity);
            string state = GameStateManager.GetInstance().GetState();
            //Debug.LogError("2____________state:    " + state);
            if (state == GameDefine.STATE_LOGIN || state == GameDefine.STATE_CREATE_CHARACTER || state == GameDefine.STATE_CHOOSE_CHARACTER || state == GameDefine.STATE_LOADING) return;

            if (gameObject == null) return;
            _startRecoveryMusic = true;

            TimerHeap.AddTimer(2000, 0, () =>
                {
                    if (gameObject != null)
                    {
                        GameObject.Destroy(gameObject);
                        ReleaseAudioAsset();
                    }
                });

        }

        private void ReleaseAudioAsset()
        {
            if (_audioClipList == null) return;
              
            for (int i = 0; i < _audioClipList.Count; i++)
            {
                Game.Asset.ObjectPool.Instance.Release(_audioClipList[i]);
                //LoggerHelper.Error("[BHRecoverMusic:ReleaseAudioAsset]=>1______________audioClipList[i]:    " + _audioClipList[i]);
            }
            _audioClipList.Clear();
        }



    }
}
