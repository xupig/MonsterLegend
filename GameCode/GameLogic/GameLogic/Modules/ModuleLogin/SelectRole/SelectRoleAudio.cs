﻿using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using MogoEngine;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace ModuleLogin
{
    public class SelectRoleAudio   : MogoAudioSource
    {
        private bool _isinCD = false;
        private uint _cdTimerID = 0;

        public SelectRoleAudio(AudioSource audioSource)
            : base(audioSource)
        {
            
            
        }

        public override void Play()
        {
            if (_isinCD == true) return;
            base.Play();
            _isinCD = true;
            ResetCDTimer();
            _cdTimerID = TimerHeap.AddTimer(30000, 0, () =>
                {
                    ResetCDTimer();
                    _isinCD = false;
                });
        }

        private void ResetCDTimer()
        {
            if (_cdTimerID != 0)
            {
                TimerHeap.DelTimer(_cdTimerID);
                _cdTimerID = 0;
            }
        }

       
       

    }
}
