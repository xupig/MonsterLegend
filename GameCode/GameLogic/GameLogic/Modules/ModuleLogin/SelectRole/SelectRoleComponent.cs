﻿using Game.UI.UIComponent;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleLogin
{
    public class SelectRoleComponent : KContainer, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        private const float ROTE_SPEED = 1.0f;

        private RectTransform _rectTransform;
        private Vector2 _currentPosition;

        public Action<Vector3> dragCallBack = null;

        public bool Dragable { get; set; }
        protected override void Awake()
        {
            _rectTransform = this.GetComponent<RectTransform>();
            _rectTransform.sizeDelta = new Vector2(600, 480);
        }


        public void OnBeginDrag(PointerEventData eventData)
        {
            if (Dragable == false) return;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform,
            eventData.position, eventData.pressEventCamera, out _currentPosition);
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (Dragable == false) return;
            Vector2 localPosition;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform,
            eventData.position, eventData.pressEventCamera, out localPosition))
            {
                float length = (localPosition - _currentPosition).x;
                _currentPosition = localPosition;
                dragCallBack(new Vector3(0, -length * ROTE_SPEED, 0));
            }

        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (Dragable == false) return;
        }
    }
}
