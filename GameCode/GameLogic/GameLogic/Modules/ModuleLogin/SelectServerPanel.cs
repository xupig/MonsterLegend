﻿using Common.Base;
using GameLoader.Config;
using System.Collections.Generic;
using Game.UI.UIComponent;
using GameLoader.Utils;
using Common.ExtendComponent;
using ModuleLogin.ChildComponent;
using GameMain.GlobalManager;
using UnityEngine;
using GameData;
using MogoEngine.Events;
using Common.Events;
using Common.Data;

namespace ModuleLogin
{
    /// <summary>
    /// 服务器界面--视图类
    /// </summary>
    public class SelectServerPanel : BasePanel
    {
        private ServerInfo recommendServerInfo;          //推荐服务器信息
        private ServerInfo nearLoginServerInfo;          //最近登录服务器信息
        private List<ServerInfo> _serverList;
        private List<ServerInfo> _curPageServerList;
        private List<CategoryItemData> _pageCountList;
        private CategoryList _categoryList;             //左边分页列表
  
        private KList _serverPageList;                  //右边每页服务器列表 
        private KScrollPage _scrollPage;
        private StateText _nearLoginText;               //最近登录标签
        private StateText _recommendServerText;         //推荐服标签
        private StateImage _leftArrow;                  //左箭头
        private StateImage _rightArrow;                 //右箭头
        private ServerInfoItem _latestServerItem;
        private ServerInfoItem _nearLoginServerItem;
        private const string IS_IOS = "2";

        private const int MAX_PAGE_SIZE = 20;           //大分页
        private const int MIN_PAGE_SIZE = 12;           //小分页
        //private string txt1 = XMLManager.chinese[103027].__content;  // "推荐服务器：";

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_fanhui");
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            _scrollPage = GetChildComponent<KScrollPage>("Container_shenqingliebiao/ScrollPage_fuwuqixuanze");
            _serverPageList = _scrollPage.GetChildComponent<KList>("mask/content"); 
            _nearLoginText = GetChildComponent<StateText>("Container_shenqingliebiao/Label_txtZuijindenglu");
            _recommendServerText = GetChildComponent<StateText>("Container_shenqingliebiao/Label_txtZuixinfuwuqi");
            _leftArrow = GetChildComponent<StateImage>("Container_shenqingliebiao/ScrollPage_fuwuqixuanze/arrow/Image_arrowPrev");
            _rightArrow = GetChildComponent<StateImage>("Container_shenqingliebiao/ScrollPage_fuwuqixuanze/arrow/Image_arrowNext");
            _latestServerItem = AddChildComponent<ServerInfoItem>("Container_shenqingliebiao/Container_latestServer");
            _nearLoginServerItem = AddChildComponent<ServerInfoItem>("Container_shenqingliebiao/Container_nearLoginServer");
            //_recommendServerText.CurrentText.text = txt1;

            _curPageServerList = new List<ServerInfo>();
            InitListSize();
        }

        private void InitListSize()
        {
            _serverPageList.SetPadding(0, 0, 0, 0);
            _serverPageList.SetGap(23, 49);
            _serverPageList.SetDirection(KList.KListDirection.LeftToRight, 3, 4);
        }

        public override void OnShow(object data)
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.Login);       //登录界面和选服界面互斥
            if (data != null && data is int)
            {//最近登录服务器ID
                int nearLoginServerId = (int)data;
                nearLoginServerInfo = ServerListManager.GetServerInfoByID(nearLoginServerId);
                _nearLoginServerItem.Data = nearLoginServerInfo;
            }
            else
            {//没有最近登录服
                _nearLoginServerItem.Data = null;
            }
            recommendServerInfo = ServerListManager.GetRecommentServer();
            _latestServerItem.Data = recommendServerInfo;

            OnRefreshServerList();
            //InitServerInfoData();
            //InitCategorySize();
            //RefreshServerList(0);
            AddEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(LoginEvents.C_REFRESH_SERVER_LIST, OnRefreshServerList);
            _categoryList.onSelectedIndexChanged.AddListener(OnCategoryChanged);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(LoginEvents.C_REFRESH_SERVER_LIST, OnRefreshServerList);
            _categoryList.onSelectedIndexChanged.RemoveListener(OnCategoryChanged);
        }

        private void OnRefreshServerList()
        {
            InitServerInfoData();
            InitCategorySize();
            RefreshServerList(0);
        }

        //====================  控件响应处理  ========================//
        /// <summary>
        /// 左边Tab切换
        /// </summary>
        /// <param name="toggleGroup"></param>
        /// <param name="index"></param>
        private void OnCategoryChanged(CategoryList toggleGroup, int index)
        {
            RefreshServerList(index);
        }
        //============================================================//

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Server; }
        }

        public override void OnClose( )
        {
            RemoveEventListener();
            UIManager.Instance.ShowPanel(PanelIdEnum.Login);
        }

        /// <summary>
        /// 初始化服务器数据
        /// </summary>
        private void InitServerInfoData()
        {
            if (_serverList == null) _serverList = new List<ServerInfo>();
            Dictionary<int, ServerInfo> _serverDict = ServerListManager.GetAllServer();
            _serverList.Clear();
            if (_serverDict != null)
            {
                foreach (ServerInfo _serverInfo in _serverDict.Values)
                {
                    if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.IPhonePlayer)
                    {
                        string fnpid = "";
                        if (Application.platform == RuntimePlatform.IPhonePlayer)
                        {
                            fnpid = IS_IOS;
                        }
                        else
                        {
                            fnpid = LoginManager.Instance.GetWindowsPlatformLoginData().fnpid;
                        }
                       if (fnpid == IS_IOS && _serverInfo.is_ios_show == 0)
                       {
                           _serverList.Add(_serverInfo);
                       }
                       else
                       {
                           _serverList.Add(_serverInfo);
                       }
                    }
                    else
                    {
                        _serverList.Add(_serverInfo);
                    }
                }
                _serverList.Sort(Sort);
            }
        }

        /// <summary>
        /// 服务器排序
        /// 1、根据服务器编号升序排序
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        private int Sort(ServerInfo a, ServerInfo b)
        {
            if (a.id < b.id) return -1;
            if (a.id > b.id) return 1;
            return 0;
        }

        /// <summary>
        /// 初始化左边Tab个数
        /// </summary>
        private void InitCategorySize()
        {
            if (_pageCountList == null)
            {
                int count = _serverList.Count;
                int totalPage = count % MAX_PAGE_SIZE == 0 ? count / MAX_PAGE_SIZE : count / MAX_PAGE_SIZE + 1; //总页数
                _pageCountList = new List<CategoryItemData>();
                CategoryItemData itemData;
                string title = XMLManager.chinese.ContainsKey(6002008) ? XMLManager.chinese[6002008].__content : "{0}-{1}";
                for (int i = 0; i < totalPage; i++)
                {
                    itemData = new CategoryItemData();
                    itemData.name = string.Format(title, i * totalPage + 1, (i + 1) * MAX_PAGE_SIZE > count ? count : (i + 1) * MAX_PAGE_SIZE);
                    _pageCountList.Add(itemData);
                }
                _categoryList.RemoveAll();
                _categoryList.SelectedIndex = 0;
                if (_pageCountList.Count > 0)
                {
                    _categoryList.SetDataList<CategoryListItem>(_pageCountList);
                }
            }
        }

        /// <summary>
        /// 刷新服务器列表
        /// </summary>
        /// <param name="index">当前页下标(从0开始)</param>
        private void RefreshServerList(int index)
        {
            int count = _serverList.Count;
            int pageMaxSum = (index + 1) * MAX_PAGE_SIZE;
            int startIndex = index * MAX_PAGE_SIZE;
            int endIndex = pageMaxSum > count ? count : pageMaxSum;
            //LoggerHelper.Debug(string.Format("startIndex:{0},endIndex:{1}", startIndex, endIndex));
            _curPageServerList.Clear();
            for (int i = startIndex; i < endIndex; i++)
            {
                _curPageServerList.Add(_serverList[i]);
            }

            //小分页
            count = _curPageServerList.Count;
            _scrollPage.CurrentPage = 0;
            if(count>0)
            {
                _scrollPage.TotalPage = count % MIN_PAGE_SIZE == 0 ? count / MIN_PAGE_SIZE : count / MIN_PAGE_SIZE + 1;
                _serverPageList.SetDataList<ServerInfoItem>(_curPageServerList, _curPageServerList.Count);
                //LoggerHelper.Error("===== TotalPage:" + _scrollPage.TotalPage + ",count:" + _curPageServerList.Count);
            }
        }
    }
}
