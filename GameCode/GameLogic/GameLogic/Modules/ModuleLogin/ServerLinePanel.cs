﻿using Common.Base;
using Game.UI.UIComponent;
using GameMain.Entities;
using MogoEngine;
using GameLoader.Utils;
using Common.Utils;
using GameLoader.Config;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;

namespace ModuleLogin
{
    /// <summary>
    /// 服务器排队界面--视图类
    /// </summary>
    public class ServerLinePanel : BasePanel
    {
        private StateText _txtPaiDui;        //排队人数
        private StateText _txtWaitTime;      //等待时间
        private StateText _txtServerName;    //服务器名称
        private KButton _btnConfirm;         //确定按钮

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.ServerLine; }
        }

        protected override void Awake()
        {
            _txtPaiDui = GetChildComponent<StateText>("Container_tanchuang/Label_txtPaiDui");
            _txtWaitTime = GetChildComponent<StateText>("Container_tanchuang/Label_txtWaitTime");
            _txtServerName = GetChildComponent<StateText>("Container_tanchuang/Label_txtServerName");
            CloseBtn = GetChildComponent<KButton>("Container_tanchuang/Button_close");
            _btnConfirm = GetChildComponent<KButton>("Container_tanchuang/Button_queding");
            _txtWaitTime.ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);
            _txtPaiDui.ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);
            _txtServerName.ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);
            _txtPaiDui.ChangeAllStateText("");
            _txtWaitTime.ChangeAllStateText("");
        }

        public override void OnShow(object data)
        {
            uint[] arry = (uint[])data;
            if (arry != null && arry.Length == 3) Refresh(arry[0], arry[1], arry[2]);
            AddEventListener();
        }

        private void AddEventListener()
        {
            _btnConfirm.onClick.AddListener(OnExitLogin);
            EventDispatcher.AddEventListener<uint, uint, uint>(LoginEvents.REFRESH_SERVER_LINE_INFO, Refresh);
        }

        private void RemoveEventListener()
        {
            _btnConfirm.onClick.RemoveListener(OnExitLogin);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        //刷新数据
        private void Refresh(uint curPos, uint maxQueue, uint waitTime)
        {
            uint hour = waitTime / 3600;                                                               //计算出小时  
            uint minute = waitTime % 3600 % 60 > 0 ? waitTime % 3600 / 60 + 1 : waitTime % 3600 / 60;    //计算出分钟
            //LoggerHelper.Debug(string.Format("[Refresh] curPos:{0},maxQueue:{1},waitTime:{2}", data[0], data[1], data[2]));
            ServerInfo serverInfo = ServerListManager.GetServerInfoByID(LocalSetting.settingData.SelectedServerID);
            _txtServerName.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(6301001), serverInfo.name);
            _txtPaiDui.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(6301002), curPos, maxQueue);
            _txtWaitTime.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(6301003), hour, minute);
        }

        //返回登陆界面
        private void OnExitLogin()
        {
            OnCloseBtnClick();
        }

        //关闭，发送退出登陆请求
        protected override void OnCloseBtnClick()
        {
            if (MogoWorld.Player is PlayerAccount)
            {
                LoggerHelper.Info(string.Format("[服务器排队] 返回登录界面请求"));
                (MogoWorld.Player as PlayerAccount).ExitLoginReq();
                //base.OnCloseBtnClick();
            }
            else
            {
                LoggerHelper.Info(string.Format("[服务器排队] MogoWorld.Player is not PlayerAccount"));
                //base.OnCloseBtnClick();
            }
        }
    }
}
