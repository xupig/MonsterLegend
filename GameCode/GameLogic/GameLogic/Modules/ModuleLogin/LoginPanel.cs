﻿using Common.Base;
using Common.Events;
using MogoEngine.Events;
using GameLoader.Config;
using Game.UI.UIComponent;
using Common;
using GameMain.GlobalManager;
using System;
using GameLoader.Utils;
using GameLoader.PlatformSdk;
using ModuleCommonUI;
using UnityEngine;
using Common.Utils;
using Modules.ModuleLogin;
using Common.ExtendComponent;

namespace ModuleLogin
{
    /// <summary>
    /// 登录界面--视图类
    /// </summary>
    public class LoginPanel : BasePanel
    {
        private KButton _btnVoice;              //声音按钮 
        private KButton _btnAccount;            //账号
        private KButton _btnLogin;              //登陆 
        private KButton _btnQuickLogin;         //快速进入
        private KButton _btnSelectServer;       //选服
        
        private KButton _btnChangeAccount;      //切换账号  
        private KDummyButton _dbtnInputAccount; //热区按钮
        private KDummyButton _dbtnInputServer;  //热区按钮
        private KInputField _inputServer;       //选中服务器
        private KInputField _inputAccount;      //用户账号

        private StateText _labVersionNo;        //显示版本号标签
        //private StateText _labTip;              //提示文本  
        private KContainer _logoContainer;      //logo图标容器
        private KParticle _logoEffect;         //logo特效
        private StateText _labGameTip;
        private StateImage _logoImage;
        private StateImage _logoImageYyb;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Login; }
        }

        /**提取UI子级组件[UI资源加载完成时调用]**/
        protected override void Awake()
        {
            
            LoggerHelper.Info(string.Format("[LoginPanel] Load cost:{0}ms", Environment.TickCount - GameStateManager.beginTime));
            _btnLogin = GetChildComponent<KButton>("Container_bottomCenter/Button_kaishiyouxi");
            _btnQuickLogin = GetChildComponent<KButton>("Container_bottomCenter/Button_kuaisujinru");
            _btnSelectServer = GetChildComponent<KButton>("Container_bottomCenter/Button_qianjin");
            _inputAccount = GetChildComponent<KInputField>("Container_bottomCenter/Container_userName/Input_userName");
            _inputServer = GetChildComponent<KInputField>("Container_bottomCenter/Container_serverName/Input_serverName");
            _btnVoice = GetChildComponent<KButton>("Container_bottomCenter/Button_shengyin");
            _btnAccount = GetChildComponent<KButton>("Container_bottomCenter/Button_account");
            _btnChangeAccount = GetChildComponent<KButton>("Container_bottomCenter/Button_huanhao");
            //_labTip = GetChildComponent<StateText>("Container_bottomCenter/Label_txtTips");
            _labVersionNo = GetChildComponent<StateText>("Container_bottomCenter/Label_txtVersionNo");
            _logoContainer = GetChildComponent<KContainer>("Container_bottomCenter/Container_logo");
            _logoEffect = AddChildComponent<KParticle>("Container_bottomCenter/fx_ui_logoflash");
            _labGameTip = GetChildComponent<StateText>("Container_bottomCenter/Label_txtgame");
            _labGameTip.ChangeAllStateText(MogoLanguageUtil.GetContent(123512));
            _labGameTip.ChangeAllStateTextAlignment(TextAnchor.LowerCenter);
            _logoImage = GetChildComponent<StateImage>("Container_bottomCenter/Container_logo/Image_logo");
            _logoImageYyb = GetChildComponent<StateImage>("Container_bottomCenter/Container_logo/Image_logoyyb");
            if (SystemConfig.isyyb())
            {
                _logoImage.Visible = false;
                _logoImageYyb.Visible = true;
            }
            else
            {
                _logoImage.Visible = true;
                _logoImageYyb.Visible = false;
            }

            GameObject goEffect = GetChild("Container_bottomCenter/fx_ui_logoflash");
            Transform tranModel = goEffect.transform.FindChild("root/logoflash_model");
            Transform tranStar = goEffect.transform.FindChild("root/Particle System_star");
            if (tranModel != null) tranModel.localPosition = new Vector3(618, -223, tranModel.localPosition.z);
            if (tranStar != null) tranStar.localPosition = new Vector3(618, -224.1f, tranStar.localPosition.z);
            _inputServer.text = "";
            _inputAccount.text = "";
            //SetTip("");
            if (SystemConfig.IsUsePlatformSdk)
            {//采用平台更新，则开启输入框热区点击打开平台登录框
                _dbtnInputAccount = AddChildComponent<KDummyButton>("Container_bottomCenter/Container_userName");
                _dbtnInputServer = AddChildComponent<KDummyButton>("Container_bottomCenter/Container_serverName");
                _inputAccount.enabled = false;
                _inputServer.enabled = false;
            }
			if (Application.isEditor) {
				_inputAccount.enabled = true;
			}
            if(Application.platform == RuntimePlatform.WindowsPlayer)
            {
                _inputAccount.enabled = false;
                _inputServer.enabled = false;
                //_btnSelectServer.enabled = false;
            }
            if (Application.platform == RuntimePlatform.WindowsWebPlayer)
            {
                _inputAccount.enabled = false;
                _inputServer.enabled = false;
                _btnSelectServer.enabled = false;
            }
            SystemConfig.StopLoadingFloatTipShow();
            LoginManager.Instance.nearLoginServerId = LocalSetting.settingData.SelectedServerID;  //最近登录服务器
            PlatformSdkMgr.Instance.sdkTipHandler = SdkTipHandler;                                //提示显示
            if (PlatformSdkMgr.Instance.PlatformSdkMgrType == PlatformSdkType.AndroidFn || PlatformSdkMgr.Instance.PlatformSdkMgrType == PlatformSdkType.None)
            {
                PlatformSdkMgr.Instance.sdkLoginOkHandler = SdkLoginOk;                               //平台登录成功监听
            }
            else if (PlatformSdkMgr.Instance.PlatformSdkMgrType == PlatformSdkType.AndroidKorea)
            {
                PlatformSdkMgr.Instance.sdkLoginOkHandler = AndroidKoreaSdkLoginOk;
            }
            PlatformSdkMgr.Instance.voucherHandler = VoucherHandler;                              //平台充值回调
            PlatformSdkMgr.Instance.webLoginHandler = LoginManager.Instance.Login;                //平台登录成功后，开始Web登录函数
            PlatformSdkMgr.Instance.wxShareResultHandler = ShareMgr.Instance.OnWxShareCallback;   //註冊分享囘調
            PlatformSdkMgr.Instance.Login();                                                      //登录平台
            #if UNITY_IPHONE && !UNITY_EDITOR
                if (!LoginManager.Instance.isInvokePlatformLogin)
                {
                    LoginManager.Instance.isInvokePlatformLogin = true;
                    OnChangeAccount();
                }
            #endif
            _labVersionNo.ChangeAllStateText("");
            _labVersionNo.CurrentText.text = LoaderDriver.Instance.versionNo;  //显示版本号
        }


        public override void OnShow(object data)
        {
            /*if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer)
            {
                PlatfromManager.GetInstance().OpenLoginPanel();
            }*/
            _logoEffect.Play(true);
            //SetTip("");
            LoginManager.Instance.ResetLoginDefaultValue();  //设置为可发送登录请求状态
            ServerListManager.Init(ServerListInitCallback);  //加载server.xml文件(服务器列表配置文件)
            AddEventListener();
            LoginNoticeMgr.instance.LoadData(onLoaded);      //加载登录公告  
        }

        public void SetTip(string txt)
        {
            //_labTip.CurrentText.text = !string.IsNullOrEmpty(txt) ? txt : "";
            if (!string.IsNullOrEmpty(txt)) MessageBox.Show(true, MessageBox.DEFAULT_TITLE, txt, false, true);
        }

        /// <summary>
        /// Sdk登录成功回调
        /// </summary>
        /// <param name="accountName">账号显示名称</param>
        /// <param name="args">返回参数列表</param>
        private void SdkLoginOk(string accountName, string[] args)
        {
            LoggerHelper.Info(string.Format("PlatformLoginOk accountName={0}", accountName));
            //args[0]=suid , args[1]=username , args[2]=sext , args[3]=platfromname
            LoginInfo.InitWithParams(args[0], args[1], args[2], args[3]);
            //这里需要注意一下：如果SDK没有返回账号名(比如应用宝就没有返回)，则用uid临时代替
            if (!string.IsNullOrEmpty(accountName))
            {
                LoginInfo.isReturnEmptyAccount = false;
                LoggerHelper.Info(string.Format("PlatformLoginOk accountName is not null,_inputAccount:{0}", _inputAccount));
                LocalSetting.settingData.UserAccount = accountName;
                if (_inputAccount != null) _inputAccount.text = accountName;
                LocalSetting.Save();
            }
            else
            {
                LoginInfo.isReturnEmptyAccount = true;
                LocalSetting.settingData.UserAccount = LoginInfo.platformUid;
                LoggerHelper.Info(string.Format("PlatformLoginOk accountName is null,_inputAccount:{0}", _inputAccount));
                if (_inputAccount != null) _inputAccount.text = LoginInfo.platformUid;
            }
            //SetTip(MogoLanguageUtil.GetContent(103043));  //"平台登录成功"
        }
		
		/// <summary>
        /// korea版Android Sdk登录成功回调
        /// </summary>
        /// <param name="accountName">账号显示名称</param>
        /// <param name="args">返回参数列表</param>
        private void AndroidKoreaSdkLoginOk(string accountName, string[] args)
        {
            #if UNITY_ANDROID
            LoggerHelper.Info(string.Format("PlatformLoginOk accountName={0}", accountName));
            //args[0]=suid , args[1]=username , args[2]=timestamp, args[3]=signStr
            LoginInfo.InitKoreaWithParams(args[0], args[1], args[2], args[3]);
            LocalSetting.settingData.UserAccount = accountName;
            _inputAccount.text = accountName;
            LocalSetting.Save();
            //SetTip(XMLManager.chinese[103043].__content);  //"平台登录成功"
            #endif
        }

        /// <summary>
        /// SDK层--返回提示处理
        /// </summary>
        /// <param name="tips">提示Id</param>
        /// <param name="isConfirmBox">是否为确认框[true:确认框提示,false:浮动提示]</param>
        /// <param name="msg">平台返回错误提示</param>
        private void SdkTipHandler(int tipId, bool isConfirmBox, string msg)
        {
            if (isConfirmBox)
            {
                //SystemAlert.Show(true, "退出游戏", "您确定退出游戏吗？", Application.Quit);
                SystemAlert.Show(true, MogoLanguageUtil.GetContent(103013), MogoLanguageUtil.GetContent(103014), Application.Quit);
            }
            else
            {
                string tip = null;
                if (string.IsNullOrEmpty(msg))
                {
                    if (tipId == 1) tip = MogoLanguageUtil.GetContent(103015);  //"平台登录异常";
                    if (tipId == 2) tip = MogoLanguageUtil.GetContent(103016);  //"平台登录被取消";
                    //if (tipId == 3) tip = MogoLanguageUtil.GetContent(103015);  //"平台登录出错";
                }
                else
                {
                    tip = msg;
                }
                if (!string.IsNullOrEmpty(tip)) ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, tip, PanelIdEnum.MainUIField);
                LoggerHelper.Info(string.Format("平台登录错误提示 respCode:{0},respDesc:{1}", tipId, tip));
            }
        }

        //充值回调处理
        private void VoucherHandler(int respCode, string data)
        {
            string desc = null;
            if (respCode == 0) desc = MogoLanguageUtil.GetContent(103017);   //"充值成功";
            if (respCode == 1) desc = MogoLanguageUtil.GetContent(103018);   //"充值失败";
            if (respCode == 2) desc = MogoLanguageUtil.GetContent(103019);   //"充值已取消";
            if (!string.IsNullOrEmpty(desc)) ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, desc, PanelIdEnum.MainUIField);
            LoggerHelper.Info(string.Format("充值回调 respCode:{0},respDesc:{1}", respCode, desc));
        }

        /// <summary>
        /// 注册事件
        /// </summary>
        private void AddEventListener()
        {
            _btnLogin.onClick.AddListener(OnLoginClick);             //普通登陆
            _btnQuickLogin.onClick.AddListener(OnQuickLogin);        //快速登陆
            _btnChangeAccount.onClick.AddListener(OnChangeAccount);  //切换账号 
            _btnSelectServer.onClick.AddListener(OnOpenServerList);  //选服（打开服务器列表界面）
            _btnVoice.onClick.AddListener(OnOpenNotice);             //打开公告界面
            _btnAccount.onClick.AddListener(OnChangeAccount);        //切换账号
            EventDispatcher.AddEventListener(LoginEvents.C_RELOGIN_REQ, OnReLogin);                              //重新登录请求,来自LoginManager.cs
            EventDispatcher.AddEventListener<string>(LoginEvents.REFRESH_LOGIN_ACCOUNTNAME, IosLoginCallback);   //刷新登录界面账号显示
            EventDispatcher.AddEventListener<string>(LoginEvents.SHOW_PLATFORM_LOGIN_ACCOUNT, PCLoginCallback);   //PC端刷新登录界面账号显示
            if (_dbtnInputAccount != null) _dbtnInputAccount.onClick.AddListener(OnChangeAccount);               //手机上点击输入框热点，则打开平台登录框，进行账号切换
            if (_dbtnInputServer != null) _dbtnInputServer.onClick.AddListener(OnOpenServerList);                //选服   
        }

        private void RemoveEventListener()
        {
            _btnLogin.onClick.RemoveListener(OnLoginClick);           
            _btnQuickLogin.onClick.RemoveListener(OnQuickLogin);    
            _btnChangeAccount.onClick.RemoveListener(OnChangeAccount); 
            _btnSelectServer.onClick.RemoveListener(OnOpenServerList);
            _btnVoice.onClick.RemoveListener(OnOpenNotice);             //打开公告界面
            _btnAccount.onClick.RemoveListener(OnChangeAccount);        //切换账号
            EventDispatcher.RemoveEventListener(LoginEvents.C_RELOGIN_REQ, OnReLogin);
            EventDispatcher.RemoveEventListener<string>(LoginEvents.REFRESH_LOGIN_ACCOUNTNAME, IosLoginCallback);
            EventDispatcher.RemoveEventListener<string>(LoginEvents.SHOW_PLATFORM_LOGIN_ACCOUNT, PCLoginCallback);   //PC端刷新登录界面账号显示
            if (_dbtnInputAccount != null) _dbtnInputAccount.onClick.RemoveListener(OnChangeAccount);
            if (_dbtnInputServer != null) _dbtnInputServer.onClick.RemoveListener(OnOpenServerList);
        }

        //加载服务器信息配置表(server.xml), 并转化为ServerInfo
        private void ServerListInitCallback()
        {
            try
            {
                if (_inputServer == null)
                {
                    LoggerHelper.Warning("LoginPanel._inputServer is null");
                    return;
                }
                if (LocalSetting.settingData.SelectedServerID == 0)
                {
                    _inputServer.text = ServerListManager.GetRecommentServer().name;
                }
                else
                {
                    ServerInfo serverInfo = ServerListManager.GetServerInfoByID(LocalSetting.settingData.SelectedServerID);
                    if (serverInfo != null)
                    {
                        _inputServer.text = serverInfo.name;
                        if (serverInfo.name.Contains(":") == true)
                        {
                            _inputServer.text = ServerListManager.GetRecommentServer().name;
                        }
                    }
                    else _inputServer.text = "";
                }
                if (!string.IsNullOrEmpty(LocalSetting.settingData.UserAccount)) _inputAccount.text = LocalSetting.settingData.UserAccount;
                LoggerHelper.Info(string.Format("LocalSetting.UserAccount:{0},inputAccount.text:{1}", LocalSetting.settingData.UserAccount, _inputAccount.text));
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.StackTrace);
            }
            ServerListManager.StartTimer();   //开启定时加载servers.xml文件
        }

        //============== 控件响应处理 ===============//
        public override void OnClose()
        {
            RemoveEventListener();
            ClosePanel();
        }

        //重新登录请求
        private void OnReLogin()
        {
            _temp = false;
            LoginReq(LoginManager.Instance.isQuickLogin);
        }

        /// <summary>
        /// 普通登陆
        /// TODO:将UserAccount 和 serverName 传递给LoginManager
        /// </summary>
        private void OnLoginClick()
        {
            LoginReq(false);
        }

        /// <summary>
        /// 快速登陆
        /// </summary>
        private void OnQuickLogin()
        {
            LoginReq(true);  
        }

        /// <summary>
        /// 到平台切换账号
        /// </summary>
        private void OnChangeAccount()
        {
            /*if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer)
            LoggerHelper.Info("==== OnChangeAccount");
            if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsWebPlayer)
            {
                PlatfromManager.GetInstance().AutoFlag = true;
                LoggerHelper.Info("==== OnChangeAccount,  打开平台登录界面");
                UIManager.Instance.ShowPanel(PanelIdEnum.PlatformLogin);
            }
            else
            {
                PlatformSdkMgr.Instance.OnSwitchAccount();
            }*/

            PlatformSdkMgr.Instance.OnSwitchAccount();

            #if UNITY_IPHONE && !UNITY_EDITOR
                //IOSPlugins.LoginAccountCenterWithCallBack(IosLoginCallback);
                IOSPlugins.ChangeAccount(IosLoginCallback);
            #endif
        }

        //IOS平台登录回调
        private void IosLoginCallback(string a10)
        {
            LoggerHelper.Info(string.Format("[IOSPlugins] platformAccount:{0},a10:{1}", LoginInfo.platformAccount, a10));
            if (_inputAccount != null)
            {
                _inputAccount.text = LoginInfo.platformAccount;
                LocalSetting.settingData.UserAccount = LoginInfo.platformAccount;
                LocalSetting.Save();
            }
        }

        //PC端登录回调
        private void PCLoginCallback(string account)
        {
            _inputAccount.text = account;
        }

        /// <summary>
        /// 发送登陆请求
        /// 1、快速登陆(一键试玩)和普通登陆的区别：
        ///    1-1、快速登陆成功后不会进入'选角界面'和‘创号界面’，直接到‘进入游戏’
        ///    1-2、普通登陆登陆成功后，必需先进入‘选角界面’，然后再到‘创号界面’或‘进入游戏’
        /// </summary>
        /// <param name="isQuickLogin">快速登陆状态[true:快速登陆,false:普通登陆]</param>
        private bool _temp = false;
        private void LoginReq(bool isQuickLogin)
        {
            string userAccount = _inputAccount.text;
            string serverName = _inputServer.text;
            if (string.IsNullOrEmpty(userAccount) || userAccount.Trim().Length < 1)
            { //"请输入账号"
                SetTip(MogoLanguageUtil.GetContent(103042)); 
                return;
            }
            ServerInfo serverInfo = string.IsNullOrEmpty(serverName) ? null : ServerListManager.GetSelectedInfoByName(serverName);
            if (serverInfo == null)
            {
                SetTip(MogoLanguageUtil.GetContent(103041));    //请选择服务器
                return; 
            }
            if (serverInfo.flag == (int)ServerType.Weihu)
            {
                SetTip(MogoLanguageUtil.GetContent(103040));    //维护中
                return;
            }
            if (serverInfo.flag == (int)ServerType.Close)
            {
                SetTip(MogoLanguageUtil.GetContent(103039));    //已关闭
                return;
            }

            if (!_temp && isQuickLogin)
            {//设置账户是否为第一次登录
                _temp = true;
                //LoginManager.Instance.isFristLogin = string.IsNullOrEmpty(LocalSetting.settingData.UserAccount);
                //LoggerHelper.Debug("是否为第一次登陆 isFristLogin:" + LoginManager.Instance.isFristLogin);
                //LoginManager.Instance.isFristLogin = true;
            }
            LoginInfo.userName = userAccount;
            LocalSetting.settingData.UserAccount = userAccount;
            LocalSetting.settingData.SelectedServerID = serverInfo.id;
            LocalSetting.Save();


            //SetTip(MogoLanguageUtil.GetContent(103037));   //"正在登录..."
            EventDispatcher.TriggerEvent(LoginEvents.LOGIN_R_LOGIN);
            LoginManager.Instance.isQuickLogin = isQuickLogin; 
            LoginManager.Instance.Login();
        }

        /// <summary>
        /// 打开选服界面
        /// </summary>
        private void OnOpenServerList()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.Server, LoginManager.Instance.nearLoginServerId);  //LocalSetting.settingData.SelectedServerID
        }

        //打开公告界面
        private void OnOpenNotice()
        {
            LoggerHelper.Debug("打开公告界面");
            UIManager.Instance.ClosePanel(PanelIdEnum.Login);
            UIManager.Instance.ShowPanel(PanelIdEnum.Notice);
        }

        //下载+初始化公告数据完成
        private void onLoaded()
        {
            if (Visible && LoginNoticeMgr.instance.isNewNotice && !LoginNoticeMgr.instance.isShowLoginNoticeUI)
            {
                LoggerHelper.Info("==== 自动弹出登录公告界面");
                LoginNoticeMgr.instance.isShowLoginNoticeUI = true;  //控制有新公告的状态下，公告界面只弹出一次
                OnOpenNotice();
            }
        }
    }
}
