﻿using System.Collections.Generic;
using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using GameMain.GlobalManager;
using ModuleLogin.ChildComponent;
using MogoEngine.Events;
using Modules.ModuleLogin;

namespace ModuleLogin
{
    /// <summary>
    /// 公告信息
    /// </summary>
    public class NoticeInfo
    {
        public int id;                //公告id
        public int beta;              //公告版本号(用于比较是否有新公告的标记)
        //public int typeId;            //类型ID
        public int typeTag;           //公告标签
        public string noticeType;     //公告标题
        public string noticeContent;  //公告内容
    }

    /// <summary>
    /// 公告界面 ScrollView_
    /// </summary>
    public class NoticePanel : BasePanel
    { 
        private KList _leftScrollPageList;                      //左边列表
        private KScrollView _leftScrollView;
        private KList _rightScrollPageList;
        private KScrollView _rightScrollView;                   //右边分页内容 
        private StateText _labNoticeTitle;                      //公告标题

        private List<NoticeInfo> _noticeDataList;               //当前显示的公告列表 
        private List<NoticeInfo> _leftNoticeInfoList;
        private Dictionary<string, List<NoticeInfo>> _noticeDict;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Notice; }
        }

        protected override void Awake()
        {
            _noticeDataList = new List<NoticeInfo>();
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close"); 
            _leftScrollView = GetChildComponent<KScrollView>("Container_fenlei/ScrollView_zhuangbeicaidan");
            _leftScrollPageList = _leftScrollView.GetChildComponent<KList>("mask/content");
            _leftScrollPageList.SetPadding(0, 0, 0, 0);
            _leftScrollPageList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            
            _labNoticeTitle = GetChildComponent<StateText>("Label_txtBiaoti");
            _rightScrollView = GetChildComponent<KScrollView>("Container_neirong/ScrollView_haoyou");
            _rightScrollPageList = _rightScrollView.GetChildComponent<KList>("mask/content");
            _rightScrollPageList.SetPadding(0, 0, 0, 0);
            _rightScrollPageList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
        }

        public override void OnShow(object data)
        {
            _noticeDict = LoginNoticeMgr.instance.data;
            if (_noticeDict != null)
            {
                RefreshLeftList();
                RefreshNoticeList(0);
                AddEventListener();
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
            UIManager.Instance.ShowPanel(PanelIdEnum.Login);
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<NoticeInfo>("select_notice_item", OnSelected);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<NoticeInfo>("select_notice_item", OnSelected);
        }

        private void OnSelected(NoticeInfo data)
        {
            int index = _leftNoticeInfoList.IndexOf(data);
            RefreshNoticeList(index);
        }

        private void UpdateTitleList(int index)
        {
            int count=_leftScrollPageList.ItemList.Count;
            for (int i = 0; i < count; i++)
            {
                _leftScrollPageList.ItemList[i].IsSelected = i == index;
            }
        }

        //刷新左边列表
        private void RefreshLeftList()
        {
            if (_leftNoticeInfoList == null)
            {
                _leftNoticeInfoList = new List<NoticeInfo>();
                foreach (var notice in _noticeDict)
                {
                    _leftNoticeInfoList.Add(notice.Value[0]);
                }
                _leftScrollPageList.SetDataList<NoticeTitleItem>(_leftNoticeInfoList);
                LoggerHelper.Debug("_leftNoticeInfoList.count:" + _leftNoticeInfoList.Count);
            }
        }

        //刷新右边通知列表
        private void RefreshNoticeList(int index)
        {
            _rightScrollPageList.RemoveAll();
            string noticeType = _leftNoticeInfoList[index].noticeType;
            _noticeDataList = _noticeDict[noticeType];
            _rightScrollPageList.SetDataList<NoticeInfoItem>(_noticeDataList);
            UpdateTitleList(index);
        }

    }
}
