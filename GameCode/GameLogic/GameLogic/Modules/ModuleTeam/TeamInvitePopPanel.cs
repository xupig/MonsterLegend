﻿#region 模块信息
/*==========================================
// 文件名：TeamInvitePopPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleTeam
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/3/1 21:57:36
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTeam
{
    public class TeamInvitePopPanel:BasePanel
    {

        private List<TeamReceivedInvitation> invitationList;
        private KButton _btnAccept;
        private KButton _btnReject;
        private StateText _txtContent;
        private StateText _txtAccept;
        private StateText _txtReject;
        private StateText _txtTime;
        private TeamReceivedInvitation _currentData;

        private ulong time;

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("Container_Bg/ScaleImage_sharedZhezhao");
            _txtContent = GetChildComponent<StateText>("Label_txtContent");
            _btnAccept = GetChildComponent<KButton>("Button_accept");
            _btnReject = GetChildComponent<KButton>("Button_reject");
            _txtAccept = _btnAccept.GetChildComponent<StateText>("label");
            _txtAccept.ChangeAllStateText((12).ToLanguage());
            _txtReject = _btnReject.GetChildComponent<StateText>("label");
            _txtReject.ChangeAllStateText((13).ToLanguage());
            _txtTime = _btnReject.GetChildComponent<StateText>("label02");
            invitationList = new List<TeamReceivedInvitation>();
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TeamInvitePop; }
        }

        public override void OnShow(object data)
        {
            SetData(data);
            _btnReject.onClick.AddListener(OnClickReject);
            _btnAccept.onClick.AddListener(OnClickAccept);
        }

        public override void OnClose()
        {
            time = 0;
            _btnReject.onClick.RemoveListener(OnClickReject);
            _btnAccept.onClick.RemoveListener(OnClickAccept);
        }

        private void OnClickAccept()
        {
            Accept(1);
        }

        private void OnClickReject()
        {
            Accept(0);
        }

        private void Accept(int accept)
        {
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            teamData.inviteDic.Remove(_currentData.inviterId);
            teamData.InvitationList.Remove(_currentData);
            TeamManager.Instance.AcceptInvite(accept, _currentData.teamId, _currentData.inviterId);
            Id.Close();
        }

        public override void SetData(object data)
        {
            if (time == 0)
            {
                time = Global.serverTimeStamp;
                RefreshData(data as TeamReceivedInvitation);
            }
            else
            {
                invitationList.Add(data as TeamReceivedInvitation);
            }
            base.SetData(data);
        }

        private void RefreshData(TeamReceivedInvitation data)
        {
            _currentData = data;

            if (instance_helper.IsInstanceIdInvalid(_currentData.instanceId))
            {
                _txtContent.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.TEAM_CAPTAIN_INVITE_INFO, _currentData.inviterName, _currentData.inviterLevel, _currentData.captainName);
            }
            else
            {
                _txtContent.CurrentText.text = MogoLanguageUtil.GetContent(10126, _currentData.inviterName, _currentData.inviterLevel,
                    MogoLanguageUtil.GetContent(instance_helper.GetName(_currentData.instanceId)));
            }

            //_txtContent.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.TEAM_CAPTAIN_INVITE_INFO, data.inviterName, data.inviterLevel, data.captainName);
        }

        void Update()
        {
            if(Global.serverTimeStamp - time > 20000)
            {
                
                
                if(invitationList.Count > 0)
                {
                    time = Global.serverTimeStamp;
                    RefreshData(invitationList[0]);
                    invitationList.RemoveAt(0);
                }
                else
                {
                    Accept(0);
                    Id.Close();
                }
            }
            else
            {
                int left = 20 - Convert.ToInt32((Global.serverTimeStamp - time) / 1000);
                _txtTime.ChangeAllStateText((6016142).ToLanguage(left));
            }
        }

    }
}
