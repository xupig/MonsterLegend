﻿using Common.Base;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System.Collections.Generic;

namespace ModuleTeam
{
    public class KickTeammatePanel : BasePanel
    {
        List<KickTeammateItem> itemList;

        protected override void Awake()
        {
            base.Awake();
            CloseBtn = GetChildComponent<KButton>("Button_close");
            itemList = new List<KickTeammateItem>();
            for (int i = 0; i < 3; i++)
            {
                itemList.Add(AddChildComponent<KickTeammateItem>("Container_Duiyuan" + i));
            }
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.KickTeammate; }
        }

        public override void OnShow(object data)
        {
            RefreshMemberList();
            AddEventListener();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(TeamEvent.TEAM_REFRESH, RefreshMemberList);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_REFRESH, RefreshMemberList);
        }

        private void RefreshMemberList()
        {
            for (int i = 0; i < itemList.Count; i++)
            {
                itemList[i].Visible = false;
            }
            int index = 0;
            foreach (var data in PlayerDataManager.Instance.TeamData.TeammateDic.Values)
            {
                if (data.guild_id != PlayerAvatar.Player.guild_id)
                {
                    itemList[index].Visible = true;
                    itemList[index].Refresh(data);
                    index++;
                }
            }
            if (index == 0)
            {
                ClosePanel();
            }
        }

    }
}
