﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/19 15:41:49
 * 描述说明：组队队伍信息Item
 * *******************************************************/

using Common.Base;
using Common.Structs;
using Common.Utils;
using Common.ExtendComponent.ListToggleGroup;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Structs.ProtoBuf;
using Common.ExtendComponent;
using MogoEngine.Utils;
using GameData;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using GameMain.GlobalManager;
using GameMain.Entities;
using Mogo.Util;
using Common.Global;

namespace ModuleTeam.ChildComponent
{
    public class TeamInfoItem:KList.KListItemBase
    {
        private PbAutoTeamInfo _data;
        private team_detail _detail;
        private StateText m_txtApply;
        private KButton m_btnApply;
        private StateText m_txtLevel;
        private IconContainer m_iconContainer;
        private StateText m_txtPeopleNum;
        private StateText m_txtTeamName;
        private StateText m_txtName;
        private KProgressBar _numProgressBar;

        protected override void Awake()
        {
            m_btnApply = GetChildComponent<KButton>("Button_shenqing");
            m_txtApply = m_btnApply.GetChildComponent<StateText>("label");
            m_txtLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            m_iconContainer = AddChildComponent<IconContainer>("Container_icon");
            m_txtPeopleNum = GetChildComponent<StateText>("Label_txtProgressBar");
            m_txtName = GetChildComponent<StateText>("Label_txtWanjiamingziqigezi");
            m_txtTeamName = GetChildComponent<StateText>("Label_txtZuduimijing");
            _numProgressBar = GetChildComponent<KProgressBar>("ProgressBar_jindu");
        }

        public override void Dispose()
        {
            RemoveEvents();
        }

        private void AddEvents()
        {
            m_btnApply.onClick.AddListener(OnClickHandler);
            EventDispatcher.AddEventListener<UInt64>(TeamEvent.TEAM_APPLY_RESP, OnTeamApplyResp);
        }

        private void RemoveEvents()
        {
            m_btnApply.onClick.RemoveListener(OnClickHandler);
            EventDispatcher.RemoveEventListener<UInt64>(TeamEvent.TEAM_APPLY_RESP, OnTeamApplyResp);
        }


        public void OnTeamApplyResp(UInt64 dbId)
        {
            if(_data.dbid == dbId)
            {
                m_txtApply.ChangeAllStateText((6016131).ToLanguage());
                m_btnApply.SetButtonDisable();
            }
        }

        public override void Show()
        {
            AddEvents();
        }

        public override void Hide()
        {
            RemoveEvents();
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbAutoTeamInfo;
                _detail = team_helper.GetTeamDetail((int)_data.game_id);
                UpdateContent();
            }
        }

        private void UpdateContent()
        {
            Visible = _data != null;

           if(_data == null || _detail == null)
           {
               return;
           }
            m_btnApply.Visible = PlayerDataManager.Instance.TeamData.takeInfo == null;
            if(m_btnApply.Visible)
            {
                m_btnApply.SetButtonActive();
            }
            m_txtApply.ChangeAllStateText((6016130).ToLanguage());
           // m_txtApply.gameObject.SetActive(teamInfo.had_apply == 1);
           // m_btnApply.gameObject.SetActive(teamInfo.had_apply != 1);

            m_txtLevel.CurrentText.text = _data.level.ToString();
            if(_data.team_cnt == 0)
            {
                _data.team_cnt = 1;
            }
            m_txtPeopleNum.CurrentText.text = string.Concat(_data.team_cnt.ToString(), "/4");
            _numProgressBar.Percent = Convert.ToInt32(_data.team_cnt * 25);
            if (_data.name == null || _data.name.Length == 0)
            {
                _data.name = MogoProtoUtils.ParseByteArrToString(_data.name_bytes);
            }
            m_txtTeamName.CurrentText.text = (6016134).ToLanguage(GetTeamName(),_data.min_level,_data.max_level);
            m_txtName.CurrentText.text = _data.name;
            m_iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_data.vocation));
        }


        private string GetTeamName()
        {
            if(_detail == null)
            {
                return string.Empty;
            }
            return team_helper.GetDetailInstanceName(_detail);
        }

        private void OnClickHandler()
        {
            if (PlayerAvatar.Player.level < _data.min_level)
            {
                MogoUtils.FloatTips(6016135);
                return;
            }
            if (PlayerAvatar.Player.level > _data.max_level)
            {
                MogoUtils.FloatTips(6016136);
                return;
            }
            if(_data.team_id == 0)
            {
                TeamManager.Instance.AcceptInvite(1, _data.team_id, _data.dbid);
            }
            else
            {
                TeamManager.Instance.Apply(_data.dbid);
            }
            
        }

    }
}
