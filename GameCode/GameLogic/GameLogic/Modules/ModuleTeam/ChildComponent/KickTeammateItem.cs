﻿using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using UnityEngine;

namespace ModuleTeam
{
    public class KickTeammateItem : KContainer
    {
        private StateText _nameText;
        private StateText _levelText;
        private StateText _guildNameText;
        private IconContainer _iconContainer;
        private KButton _kickButton;

        public PbTeamMember _data;

        protected override void Awake()
        {
            base.Awake();
            _nameText = GetChildComponent<StateText>("Label_txtName");
            _levelText = GetChildComponent<StateText>("Container_jiaosexuanze/Container_dengji/Label_txtDengji");
            _guildNameText = GetChildComponent<StateText>("Label_txtGonghuiName");
            _iconContainer = AddChildComponent<IconContainer>("Container_jiaosexuanze/Container_icon");
            _kickButton = GetChildComponent<KButton>("Button_yitichuduiwu");
            _nameText.CurrentText.alignment = TextAnchor.UpperCenter;
            _guildNameText.CurrentText.alignment = TextAnchor.UpperCenter;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            RefreshContent();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
            _data = null;
        }

        public void Refresh(PbTeamMember data)
        {
            _data = data;
            RefreshContent();
        }

        private void RefreshContent()
        {
            if (_data == null) return;
            _nameText.CurrentText.text = _data.name;
            _iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_data.vocation));
            _levelText.CurrentText.text = MogoLanguageUtil.GetContent(43407, _data.level);
            _guildNameText.Visible = false;
            if (_data.guild_id != 0)
            {
                GuildDataManager.GetInstance().RequestGuildDataById(_data.guild_id);
            }
            else
            {
                _guildNameText.Visible = true;
                _guildNameText.CurrentText.text = MogoLanguageUtil.GetContent(102508);
            }
        }


        private void RefreshGuildInfo(ulong guildId)
        {
            if (guildId != _data.guild_id) { return; }
            PbGuildSimpleInfo info = GuildDataManager.GetInstance().GetGuildSimpleInfo(guildId);
            if (info != null)
            {
                _guildNameText.Visible = true;
                _guildNameText.CurrentText.text = info.guild_name;
            }
        }

        protected void AddEventListener()
        {
            _kickButton.onClick.AddListener(OnKickButtonClick);
            EventDispatcher.AddEventListener<ulong>(GuildEvents.Refresh_Guild_Simple_Info, RefreshGuildInfo);
        }

        protected void RemoveEventListener()
        {
            _kickButton.onClick.RemoveListener(OnKickButtonClick);
            EventDispatcher.RemoveEventListener<ulong>(GuildEvents.Refresh_Guild_Simple_Info, RefreshGuildInfo);
        }

        private void OnKickButtonClick()
        {
            if (_data != null)
            {
                TeamManager.Instance.KickTeammate(_data.dbid);
            }
        }


    }
}
