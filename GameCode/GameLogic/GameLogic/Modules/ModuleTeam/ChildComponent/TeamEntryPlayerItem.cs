﻿#region 模块信息
/*==========================================
// 文件名：TeamEntryPlayerItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleTeam.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/19 11:01:26
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using ModuleCommonUI;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTeam
{
    public class TeamEntryPlayerItem:KContainer
    {
        private List<StateImage> _waitList;
        private IconContainer _iconContainer;
        private KToggle _selectedImg;
        private StateText _levelText;
        private StateText _nameText;

        private uint timeId;
        private int index = 0;

        private PbTeamMember _member;
        private IconContainer _teamDutyIcon;

        protected override void Awake()
        {
            _waitList = new List<StateImage>();
            for (int i = 0; i < 3;i++ )
            {
                StateImage image = GetChildComponent<StateImage>(string.Format("Image_wait{0}",i));
                _waitList.Add(image);
            }
            _selectedImg = GetChildComponent<KToggle>("Toggle_Xuanze");
            _iconContainer = AddChildComponent<IconContainer>("Container_jiaosexuanze/Container_icon");
            _selectedImg.interactable = false;
            _levelText = GetChildComponent<StateText>("Container_jiaosexuanze/Container_dengji/Label_txtDengji");
            _nameText = GetChildComponent<StateText>("Label_txtName");
            _nameText.CurrentText.alignment = UnityEngine.TextAnchor.UpperCenter;
            _teamDutyIcon = AddChildComponent<IconContainer>("Container_jiaosexuanze/Container_teamDuty");
            base.Awake();
        }


        public void SetInfo(PbTeamMember memberInfo, bool isCaptain = false)
        {
            Visible = memberInfo != null;
            _member = memberInfo;
            if(memberInfo!=null)
            {
                RefreshTick(isCaptain);
                RefreshPlayerInfo((int)memberInfo.vocation, memberInfo.level, memberInfo.name);
                RefreshNameColor(memberInfo.dbid == PlayerAvatar.Player.dbid);
                if (isCaptain)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        _waitList[i].Visible = true;
                    }
                }
                else
                {
                    for (int i = 0; i < 3; i++)
                    {
                        _waitList[i].Visible = false;
                    }
                    RemoveTimer();
                    timeId = TimerHeap.AddTimer(0, 500, TimeStep);
                }
                RefreshTeamDutyIcon();
            }
        }

        private void RefreshTeamDutyIcon()
        {
            if (_member.spell_proficient != 0)
            {
                _teamDutyIcon.Visible = true;
                _teamDutyIcon.SetIcon(skill_helper.GetTeamDutyIconId((Vocation)_member.vocation,(int)_member.spell_proficient));
            }
            else
            {
                _teamDutyIcon.Visible = false;
            }
        }

        private void RefreshPlayerInfo(int vocation, uint level, string name)
        {
            _levelText.CurrentText.text = level.ToString();
            _nameText.CurrentText.text = name;
            _iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon(vocation));
        }

        private void RefreshNameColor(bool isMe)
        {
            if (isMe)
            {
                _nameText.CurrentText.color = ColorDefine.GetColorById(ColorDefine.PLAYER_AVATAR_INFO_COLOR);
            }
            else
            {
                _nameText.CurrentText.color = ColorDefine.GetColorById(ColorDefine.ENTITY_AVATAR_INFO_COLOR);
            }
        }

        private void RefreshTick(bool isCaptain)
        {
            if (isCaptain)
            {
                _selectedImg.isOn = true;
            }
            else
            {
                _selectedImg.isOn = false;
            }
        }

        protected override void OnEnable()
        {
            RemoveTimer();
        }

        protected override void OnDisable()
        {
            RemoveTimer();
        }

        private void RemoveTimer()
        {
            if (timeId != 0)
            {
                TimerHeap.DelTimer(timeId);
                timeId = 0;
            }
        }

        public void SetAgree(UInt64 dbId)
        {
            if (_member!=null && dbId == _member.dbid)
            {
                SetAgree();
            }
        }

        public void SetAgree()
        {
            _selectedImg.isOn = true;
            index = 0;
            RemoveTimer();
            for (int i = 0; i < 3; i++)
            {
                _waitList[i].Visible = true;
            }
        }

        private void TimeStep()
        {
            index++;
            if(index > 3)
            {
                index = 0;
            }
            int i = 0;
            for (i = 0; i < index; i++)
            {
                _waitList[i].Visible = true;
            }
            for (; i < 3; i++)
            {
                _waitList[i].Visible = false;
            }
        }

        public void SetData(TeamEntryPlayerItemData itemData)
        {
            RefreshTick(false);
            RefreshPlayerInfo(itemData.Vocation, itemData.Level, itemData.Name);
            RefreshNameColor(itemData.Dbid == PlayerAvatar.Player.dbid);
            for (int i = 0; i < 3; i++)
            {
                _waitList[i].Visible = false;
            }
            RemoveTimer();
            timeId = TimerHeap.AddTimer(0, 500, TimeStep);
        }
    }
}
