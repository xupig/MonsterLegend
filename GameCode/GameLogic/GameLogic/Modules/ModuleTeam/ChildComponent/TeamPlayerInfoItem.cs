﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/19 17:18:39
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.Builder;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using UnityEngine;

namespace ModuleTeam.ChildComponent
{
    public class TeamPlayerInfoItem:KContainer
    {
        private static Color SelectColor = ColorDefine.GetColorById(ColorDefine.PLAYER_AVATAR_INFO_COLOR);
        private static Color WhiteColor = ColorDefine.GetColorById(ColorDefine.COLOR_ID_CHAT_NEARBY);

        private static string positionContent;
        private static string fightValueContent;

        private StateImage _selectedImg;

        private StateText m_txtLevel;
        private IconContainer m_iconContainer;
        private StateText m_txtPositionName;
        
        private StateText m_txtFightValueName;
        private StateText m_txtName;

        private StateImage m_captainContainer;

        private KContainer _matchContainer;
        private KContainer _playerInfoContainer;


        private StateText _txtGame;
        private StateText _txtLevelRegion;
        private StateText _txtMatch;
        private KButton _btnMatch;
        private StateText _txtBtnMatch;
        private StateImage _imgMatch;
        private StateImage _imgSlash;

        private KContainer _content;
        private KDummyButton _btnContent;

        private IconContainer _teamDutyIcon;

        public KComponentEvent<TeamPlayerInfoItem> onClick = new KComponentEvent<TeamPlayerInfoItem>();

        public PbTeamMember Data
        {
            get;
            set;
        }

        protected override void Awake()
        {
            _content = GetChildComponent<KContainer>("Container_content");
            _btnContent = _content.gameObject.AddComponent<KDummyButton>();
            _playerInfoContainer = _content.GetChildComponent<KContainer>("Container_info");
            _matchContainer = _content.GetChildComponent<KContainer>("Container_pipeizhong");
            _txtGame = _matchContainer.GetChildComponent<StateText>("Label_txtMijing");
            _txtGame.CurrentText.fontSize = 22;
            _txtLevelRegion = _matchContainer.GetChildComponent<StateText>("Label_txtDengji");
           
            _btnMatch = GetChildComponent<KButton>("Button_pipeiduiyuan");
            _txtBtnMatch = _btnMatch.GetChildComponent<StateText>("label");
            _selectedImg = _content.GetChildComponent<StateImage>("Image_checkmark");
            _txtMatch = _matchContainer.GetChildComponent<StateText>("Label_txtPipei");
            _txtMatch.CurrentText.text = (6016128).ToLanguage();
            _imgMatch = _matchContainer.GetChildComponent<StateImage>("Image_dawenhao");
            _imgSlash = _matchContainer.GetChildComponent<StateImage>("Image_duiyoudi");
            m_txtLevel = _playerInfoContainer.GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            m_iconContainer = _playerInfoContainer.AddChildComponent<IconContainer>("Container_icon");
            m_txtPositionName = _playerInfoContainer.GetChildComponent<StateText>("Label_txtWeizhi");
            m_txtFightValueName = _playerInfoContainer.GetChildComponent<StateText>("Label_txtZhanli");
            m_captainContainer = _playerInfoContainer.GetChildComponent<StateImage>("Container_biaoshi/Image_biaoshiDB");
            if (positionContent == null)
            {
                positionContent = m_txtPositionName.CurrentText.text;
                fightValueContent = m_txtFightValueName.CurrentText.text;
            }
            _selectedImg.Visible = false;
            m_captainContainer.gameObject.SetActive(false);
            m_txtName = _playerInfoContainer.GetChildComponent<StateText>("Label_txtName");
            _teamDutyIcon = _content.AddChildComponent<IconContainer>("Container_info/Container_teamDuty");
        }

        public void SetSelected(bool isSelected)
        {
            _selectedImg.Visible = isSelected;
        }

        public void SetInfo(PbTeamMember info)
        {
            Data = info;
            if(info == null)
            {
                _playerInfoContainer.Visible = false;
                _matchContainer.Visible = true;
                TeamData teamData = PlayerDataManager.Instance.TeamData;
                if (teamData.auto_team_game_id != 0 && teamData.auto_team_is_start == 1)
                {
                    _txtBtnMatch.ChangeAllStateText((6016060).ToLanguage());
                    _txtGame.Visible = true;
                    _txtLevelRegion.Visible = true;
                    _txtMatch.Visible = true;
                    _btnMatch.Visible = true;
                    _imgMatch.Visible = true;
                    team_detail detail = team_helper.GetTeamDetail(Convert.ToInt32(teamData.auto_team_game_id));
                    _txtGame.ChangeAllStateText(team_helper.GetDetailInstanceName(detail));
                    _txtLevelRegion.CurrentText.text = (6016127).ToLanguage(teamData.auto_team_min_level, teamData.auto_team_max_level);
                }
                else
                {
                    _txtBtnMatch.ChangeAllStateText((6016057).ToLanguage());
                    _txtGame.Visible = false;
                    _txtLevelRegion.Visible = false;
                    _txtMatch.Visible = false;
                    _btnMatch.Visible = true;
                    _imgMatch.Visible = true;
                }
            }
            else
            {
                _playerInfoContainer.Visible = true;
                _matchContainer.Visible = false;
                _btnMatch.Visible = false;
                m_txtLevel.CurrentText.text = info.level.ToString();
                m_txtFightValueName.CurrentText.text = string.Format(fightValueContent, info.fight_force);
                m_txtName.CurrentText.text = info.name;
                int instId = map_helper.GetInstanceIDByMapID((int)info.map_id);

                string instName = instance_helper.GetInstanceName(instId);
                if (instName.Length > 7)
                {
                    m_txtPositionName.CurrentText.text = string.Format(positionContent, string.Concat(instName.Substring(0,7), "..."));
                }
                else
                {
                    m_txtPositionName.CurrentText.text = string.Format(positionContent, instName);
                }
               
                MogoAtlasUtils.AddIcon(m_iconContainer.gameObject, MogoPlayerUtils.GetSmallIcon((int)info.vocation), OnIconLoaded);
                m_captainContainer.gameObject.SetActive(info.dbid == PlayerDataManager.Instance.TeamData.CaptainId);
                SetIsSelfInfo(info.eid == PlayerAvatar.Player.id);
                RefreshTeamDutyIcon();
            }
        }

        private void RefreshTeamDutyIcon()
        {
            if (Data.spell_proficient != 0)
            {
                _teamDutyIcon.Visible = true;
                _teamDutyIcon.SetIcon(skill_helper.GetTeamDutyIconId((Vocation)Data.vocation,(int)Data.spell_proficient));
            }
            else
            {
                _teamDutyIcon.Visible = false;
            }
        }

        public void SetIsSelfInfo(bool select)
        {
            m_txtLevel.CurrentText.color = select ? SelectColor : WhiteColor;
            m_txtPositionName.CurrentText.color = select ? SelectColor : WhiteColor;
            m_txtFightValueName.CurrentText.color = select ? SelectColor : WhiteColor;
            m_txtName.CurrentText.color = select ? SelectColor : WhiteColor;
        }

        private void OnIconLoaded(GameObject go)
        {
            RefreshImageState();
        }

        private void RefreshOnlineContent(ulong dbid, int online)
        {
            if(Data!=null && Data.dbid == dbid)
            {
                RefreshImageState();
            }
        }

        private void RefreshImageState()
        {
            if (PlayerDataManager.Instance.TeamData.TeammateDic.ContainsKey(Data.dbid))
            {
                PbTeamMember memberInfo = PlayerDataManager.Instance.TeamData.TeammateDic[Data.dbid];
                ImageWrapper[] wrappers = m_iconContainer.gameObject.GetComponentsInChildren<ImageWrapper>();
                for (int i = 0; i < wrappers.Length; i++)
                {
                    wrappers[i].SetGray(memberInfo.online);
                }
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        public void AddEventListener()
        {
            EventDispatcher.AddEventListener<ulong, int>(TeamEvent.TEAM_MATE_ONLINE_REFRESH, RefreshOnlineContent);
            _btnMatch.onClick.AddListener(OnClickMatch);
            _btnContent.onClick.AddListener(OnClickContent);
        }

        public void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<ulong, int>(TeamEvent.TEAM_MATE_ONLINE_REFRESH, RefreshOnlineContent);
            _btnMatch.onClick.RemoveListener(OnClickMatch);
            _btnContent.onClick.RemoveListener(OnClickContent);
        }

        private void OnClickContent()
        {
            if(Data == null)
            {
                return;
            }
            _selectedImg.Visible = true;
            onClick.Invoke(this);
        }

        private void OnClickMatch()
        {
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            if (teamData.auto_team_game_id != 0 && teamData.auto_team_is_start == 1)
            {
                TeamManager.Instance.StopAutoMatch();
            }
            else
            {
                PanelIdEnum.TeamMatch.Show();
            }
            
        }
    }
}
