﻿#region 模块信息
/*==========================================
// 文件名：TeamMenuItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleTeam.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/2/22 14:55:57
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTeam
{
    public class TeamMenuItem : KTreeMenuItem
    {
        private team_menu _menuData;
        private KToggle _menuToggle;
        private IconContainer _icon1;
        private IconContainer _icon2;
        public bool selected;
        private StateImage _greenPoint;
        private StateImage _greenPointSelected;
        public override object Data
        {
            get
            {
                return _menuData;
            }
            set
            {
                _menuData = value as team_menu;
                Refresh();
            }
        }

        public override bool IsSelected
        {
            get
            {
                return base.IsSelected;
            }
            set
            {
                base.IsSelected = value;
                _menuToggle.isOn = value;
                selected = value;
            }
        }

        protected override void Awake()
        {
            _menuToggle = GetChildComponent<KToggle>("Toggle_item");
            _menuToggle.onValueChanged.AddListener(OnMenuBtnClick);
            SetGreenPoint(false);
            _icon1 = _menuToggle.AddChildComponent<IconContainer>("checkmark/Container_icon");
            _icon2 = _menuToggle.AddChildComponent<IconContainer>("back/Container_icon");
        }

        private bool hasInitial = false;

        private void InitGreenPoint()
        {
            if (hasInitial)
            {
                return;
            }
            hasInitial = true;
            _greenPoint = _menuToggle.GetChildComponent<StateImage>("back/Image_xiaodian");
            _greenPointSelected = _menuToggle.GetChildComponent<StateImage>("checkmark/Image_xiaodian");
        }

        public void SetGreenPoint(bool isShowRed)
        {
            InitGreenPoint();
            if (_greenPoint != null)
            {
                _greenPoint.Visible = isShowRed;
            }
            if (_greenPointSelected != null)
            {
                _greenPointSelected.Visible = isShowRed;
            }
        }

        private void OnMenuBtnClick(KToggle toggle, bool value)
        {
            onClick.Invoke(this, this.Index);
        }

        private void Refresh()
        {
            ID = _menuData.__id;
            TextWrapper[] btnTxts = _menuToggle.transform.GetComponentsInChildren<TextWrapper>(true);
            for (int i = 0; i < btnTxts.Length; i++)
            {
                btnTxts[i].text = _menuData.__name.ToLanguage();
            }
            _icon1.SetIcon(_menuData.__icon);
            _icon2.SetIcon(_menuData.__icon);
        }

        public override void Dispose()
        {

        }
    }
}
