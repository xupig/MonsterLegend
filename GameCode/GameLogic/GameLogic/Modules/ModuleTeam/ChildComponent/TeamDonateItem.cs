﻿#region 模块信息
/*==========================================
// 文件名：TeamDonateItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleTeam.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/3/9 14:19:16
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Common.Structs;
using Game.UI.UIComponent;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTeam
{
    public class TeamDonateItem:KList.KListItemBase
    {
        private BaseItemInfo _data;
        private ItemGrid _itemGrid;
        private KToggle _toggle;
        protected override void Awake()
        {
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _itemGrid.Context = PanelIdEnum.TeamDonate;
            _toggle = GetChildComponent<KToggle>("Toggle_Xuanze");
        }

        public override bool IsSelected
        {
            get
            {
                return base.IsSelected;
            }
            set
            {
                base.IsSelected = value;
                _toggle.isOn = value;
            }
        }

        public override void Show()
        {
            _toggle.onValueChanged.AddListener(OnValueChanged);
            base.Show();
        }

        public override void Hide()
        {
            _toggle.onValueChanged.RemoveListener(OnValueChanged);
            base.Hide();
        }

        private void OnValueChanged(KToggle toggle,bool isOn)
        {
            if(isOn)
            {
                OnGridClick();
            }
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as BaseItemInfo;
                _toggle.isOn = false;
                _itemGrid.SetItemData(_data);
            }
        }

        private void OnGridClick()
        {
            IsSelected = true;
            onClick.Invoke(this, Index);
        }

        public override void Dispose()
        {
            
        }

    }
}
