﻿#region 模块信息
/*==========================================
// 文件名：TeamAwardAllocateItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleTeam.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/2/23 11:36:16
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTeam
{
    public class TeamAwardAllocateItem:KList.KListItemBase
    {
        private PbGroupRecordItem _data;
        private StateText _txtContent;
        private KDummyButton _btn;
        private KButton _btnDonate;
        private StateText _txtDonate;
        private bool _hasApply = false;
        protected override void Awake()
        {
            _txtContent = GetChildComponent<StateText>("Label_txtjishi");
            _btn = _txtContent.gameObject.AddComponent<KDummyButton>();
            _btnDonate = GetChildComponent<KButton>("Button_qiuzengsong");
            _txtDonate = _btnDonate.GetChildComponent<StateText>("label");
        }

        public override void Show()
        {
            _btnDonate.onClick.AddListener(OnClickDonate);
            _btn.onClick.AddListener(OnClickTips);
            EventDispatcher.AddEventListener<uint>(TeamEvent.TRANSFER_APPLY,OnTransferApply);
            base.Show();
        }

        private void OnTransferApply(uint idx)
        {
            if(_data.record_idx == idx)
            {
                UpdateBtnDonate();
            }
        }

        private void UpdateBtnDonate()
        {
            if(PlayerDataManager.Instance.TeamData.requiredDict.ContainsKey(_data.record_idx) == false)
            {
                PlayerDataManager.Instance.TeamData.requiredDict.Add(_data.record_idx, _data.expire_time);
            }
            if(PlayerDataManager.Instance.TeamData.requiredDict[_data.record_idx] == uint.MaxValue)
            {
                _hasApply = true;
                _btnDonate.SetButtonDisable();
                _txtDonate.ChangeAllStateText((6016137).ToLanguage());
            }
            else
            {
                _hasApply = false;
                _btnDonate.SetButtonActive();
                _txtDonate.ChangeAllStateText((6016041).ToLanguage());
            }
        }

        private void OnClickDonate()
        {
            if (_hasApply)
            {
                return;
            }
            MissionManager.Instance.RequestTransfer(Convert.ToInt32(_data.record_idx));
        }

        public override void Hide()
        {
            _btnDonate.onClick.RemoveListener(OnClickDonate);
            _btn.onClick.RemoveListener(OnClickTips);
            EventDispatcher.RemoveEventListener<uint>(TeamEvent.TRANSFER_APPLY, OnTransferApply);
            base.Hide();
        }

        private void OnClickTips()
        {
            int itemId = Convert.ToInt32(_data.item_list[0].id);
            ToolTipsManager.Instance.ShowItemTip(itemId, Common.Base.PanelIdEnum.Team);
        }

        public override void Dispose()
        {
            
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbGroupRecordItem;
                RefreshContent();
            }
        }

        private void RefreshContent()
        {
            UpdateBtnDonate();
            string name = MogoProtoUtils.ParseByteArrToString(_data.role_name_bytes);
            string instanceName = instance_helper.GetInstanceName(Convert.ToInt32(_data.mission_id));
            string itemName = item_helper.GetName(Convert.ToInt32(_data.item_list[0].id));

            uint utime = Convert.ToUInt32(Global.serverTimeStamp / 1000);
            string time = (6016133).ToLanguage((_data.expire_time - utime) / 60);
            _txtContent.CurrentText.text = (6016058).ToLanguage(name, instanceName, itemName, time);
            
        }

    }
}
