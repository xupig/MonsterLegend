﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2015/1/5 9:55:44
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTeam.ChildComponent
{
    public class TeamMessageItem:KList.KListItemBase
    {
        public int InstanceId;

        #region 私有变量
        private KButton m_btnYes;
        private KButton m_btnNo;
        private StateText m_txtApplyContent;
        private StateText m_txtInviteContent;
        private StateText m_txtCaptainContent;
        private StateText m_txtLevel;
        private IconContainer m_iconContainer;
        #endregion

        #region 基础接口实现

        protected override void Awake()
        {
            KContainer _container = this.gameObject.GetComponent<KContainer>();
            m_btnYes = _container.GetChildComponent<KButton>("Button_Yes");
            m_btnNo = _container.GetChildComponent<KButton>("Button_No");
            m_txtApplyContent = _container.GetChildComponent<StateText>("Label_txtApplyContent");
            m_txtInviteContent = _container.GetChildComponent<StateText>("Label_txtInviteContent");
            m_txtCaptainContent = _container.GetChildComponent<StateText>("Label_txtCaptainInviteContent");
            m_txtLevel = _container.GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            m_iconContainer = _container.AddChildComponent<IconContainer>("Container_icon");
        }

        public override void Dispose()
        {
            RemoveEvents();
            base.Data = null;
        }

        private void AddEvents()
        {
            m_btnYes.onClick.AddListener(OnClickYes);
            m_btnNo.onClick.AddListener(OnClickNo);
        }

        private void RemoveEvents()
        {
            m_btnYes.onClick.RemoveListener(OnClickYes);
            m_btnNo.onClick.RemoveListener(OnClickNo);
        }

        public override void Show()
        {
            AddEvents();
        }

        public override void Hide()
        {
            RemoveEvents();
        }

        public override object Data
        {
            get
            {
                return base.Data;
            }
            set
            {
                base.Data = value;
                UpdateContent();
            }
        }


        private void UpdateContent()
        {
            m_txtApplyContent.gameObject.SetActive(false);
            m_txtInviteContent.gameObject.SetActive(false);
            m_txtCaptainContent.gameObject.SetActive(false); 
            if (Data is TeamReceivedApplicant)
            {
                 TeamReceivedApplicant applicant = Data as TeamReceivedApplicant;
                 m_txtLevel.CurrentText.text = applicant.applicantLevel.ToString();

                 m_txtApplyContent.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.TEAM_APPLY_INFO,applicant.applicantName,applicant.applicantLevel); 
                 m_txtApplyContent.gameObject.SetActive(true);
                 m_iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon(applicant.applicantVocation.ToInt()));
                //头像处理
            }
            else if (Data is TeamReceivedInvitation)
            {
                TeamReceivedInvitation invitation = Data as TeamReceivedInvitation;
                m_txtLevel.CurrentText.text = invitation.inviterLevel.ToString();
                if (instance_helper.IsInstanceIdInvalid(invitation.instanceId))
                {
                    m_txtInviteContent.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.TEAM_CAPTAIN_INVITE_INFO, invitation.inviterName, invitation.inviterLevel, invitation.captainName);
                }
                else
                {
                    m_txtInviteContent.CurrentText.text = MogoLanguageUtil.GetContent(10126, invitation.inviterName, invitation.inviterLevel,
                        MogoLanguageUtil.GetContent(instance_helper.GetName(invitation.instanceId)));
                }
                m_txtInviteContent.gameObject.SetActive(true);
                m_iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon(invitation.inviterVocation.ToInt()));
            }
            else if (Data is TeamCaptainReceivedInvitation)
            {
                TeamCaptainReceivedInvitation invitation = Data as TeamCaptainReceivedInvitation;
                m_txtLevel.CurrentText.text = invitation.inviteeLevel.ToString();
                m_txtCaptainContent.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.TEAM_OTHER_INVITE_INFO, invitation.inviterName, invitation.inviteeName, invitation.inviteeLevel);
                m_txtCaptainContent.gameObject.SetActive(true);
                m_iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon(invitation.inviteeVocation.ToInt()));
            }
            //PlayerDataManager.Instance.TeamData.ReceivedList.Remove(Data as TeamReceivedData);
        }


        #endregion

        #region UI回调事件处理

        private void OnClickYes()
        {
            Accept(1);
        }

        private void OnClickNo()
        {
            Accept(0);
        }

        private void Accept(int accept)
        {
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            if (Data is TeamReceivedApplicant)
            {
                TeamReceivedApplicant applicant = Data as TeamReceivedApplicant;
                teamData.CaptainList.Remove(applicant);
                teamData.applyDic.Remove(applicant.applicantId);
                TeamManager.Instance.CaptainAcceptApply(accept, applicant.teamId, applicant.applicantId);
            }
            else if (Data is TeamReceivedInvitation)
            {
                TeamReceivedInvitation invitation = (Data as TeamReceivedInvitation);
                teamData.inviteDic.Remove(invitation.inviterId);
                teamData.InvitationList.Remove(invitation);
                TeamManager.Instance.AcceptInvite(accept, invitation.teamId, invitation.inviterId);
            }
            else if (Data is TeamCaptainReceivedInvitation)
            {
                TeamCaptainReceivedInvitation invitation = (Data as TeamCaptainReceivedInvitation);
                teamData.captainInviteDic[invitation.inviterId].Remove(invitation.inviteeId);
                teamData.CaptainList.Remove(invitation);
                TeamManager.Instance.CaptainAcceptInvite(accept, invitation.teamId, invitation.inviterId, invitation.inviteeId, InstanceId);
            }
            
        }

        #endregion

    }
}
