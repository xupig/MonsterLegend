﻿#region 模块信息
/*==========================================
// 文件名：TeamDetailItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleTeam.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/2/22 16:15:00
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTeam
{
    public class TeamDetailItem:KTreeDetailItem
    {
        private team_detail _data;
        private StateText _txtName;
        private StateImage _selected;
        private KParticle _particle;
        protected override void Awake()
        {
            _particle = AddChildComponent<KParticle>("fx_ui_3_2_team");
            if (_particle != null)
            {
                _particle.Stop();
            }
            _selected = GetChildComponent<StateImage>("Image_xuanzhongkuang01");
            _txtName = GetChildComponent<StateText>("Label_txtXiangxixinxi");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                ID = -1;
                if (value != null)
                {
                    _data = value as team_detail;
                    ID = _data.__id;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            TeamLimitData item = team_helper.GetDetailEffectData(_data.__id);
            if(_particle!=null)
            {
                if (IsOpen(item))
                {
                    _particle.Play(true);
                }
                else
                {
                    _particle.Stop();
                }
            }
            HideSelectedImage();
            _txtName.ChangeAllStateText(team_helper.GetDetailInstanceName(_data));
        }

        private bool IsOpen(TeamLimitData data)
        {
            if (data == null)
            {
                return false;
            }
            if (data.type == 0)
            {
                return true;
            }
            if (PlayerAvatar.Player.level < data.minLevel)
            {
                return false;
            }
            if (data.startDate > (long)(Global.serverTimeStamp / 1000) || data.endDate < (long)(Global.serverTimeStamp / 1000))//日期判断
            {
                return false;
            }
            DateTime dateTime = MogoTimeUtil.ServerTimeStamp2UtcTime((long)(Global.serverTimeStamp / 1000));
            if (data.openWeekOfDay.Count > 0 && data.openWeekOfDay.Contains((int)dateTime.DayOfWeek) == false)//星期判断
            {
                return false;
            }

            int currentSecond = PlayerTimerManager.GetInstance().GetCurrentSecond();
            if (data.openTimeStart.Count > 0)
            {
                for (int i = 0; i < data.openTimeStart.Count; i++)
                {
                    if (currentSecond >= data.openTimeStart[i] && currentSecond <= data.openTimeEnd[i])//时间判断
                    {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        public void SetSelectedImage()
        {
            _selected.Visible = true;
        }

        public void HideSelectedImage()
        {
            _selected.Visible = false;
        }

        public override void Dispose()
        {

        }

    }
}
