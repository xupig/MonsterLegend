﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/19 14:22:42
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using UnityEngine;

namespace ModuleTeam.ChildComponent
{

    public class TeamInviteItem : KList.KListItemBase
    {
        public int InstanceId;

        private KContainer m_cellInfo;
        private StateText m_txtInvite;
        private KButton m_btnAdd;
        private StateText m_txtMapName;
        private StateText m_txtFightValue;
        private StateText m_txtFightValueName;
        private StateText m_txtName;
        private StateText m_txtLevel;
        private IconContainer m_iconContainer;


        protected override void Awake()
        {
            m_cellInfo = GetChildComponent<KContainer>("Container_cellInfo");
            m_txtInvite = m_cellInfo.GetChildComponent<StateText>("Label_txtYiyaoqing");
            m_btnAdd = m_cellInfo.GetChildComponent<KButton>("Button_zengjia");
            m_txtMapName = m_cellInfo.GetChildComponent<StateText>("Label_txtDituName");
            m_txtFightValue = m_cellInfo.GetChildComponent<StateText>("Label_txtZhanliContent");
            m_txtFightValueName = m_cellInfo.GetChildComponent<StateText>("Label_txtZhanli");
            m_txtName = m_cellInfo.GetChildComponent<StateText>("Label_txtWanjiaName");
            m_txtLevel = m_cellInfo.GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            m_iconContainer = m_cellInfo.AddChildComponent<IconContainer>("Container_icon");
            
        }

        public override void Dispose()
        {
            RemoveEvents();
            base.Data = null;
        }

        private void AddEvents()
        {
            EventDispatcher.AddEventListener<UInt64, int>(TeamEvent.TEAM_INVITE_RESP, OnTeamInviteResp);
            m_btnAdd.onClick.AddListener(OnClickAdd);
        }

        private void RemoveEvents()
        {
            EventDispatcher.RemoveEventListener<UInt64, int>(TeamEvent.TEAM_INVITE_RESP, OnTeamInviteResp);
            m_btnAdd.onClick.RemoveListener(OnClickAdd);
        }


        public void OnTeamInviteResp(UInt64 dbId, int type)
        {
            if(gameObject.activeSelf == false)
            {
                return;
            }
            if (type == 0)
            {
                PbNearPlayer playerInfo = Data as PbNearPlayer;
                if (dbId == playerInfo.dbid)
                {
                    SetInviteState(playerInfo.dbid);
                }
            }
            else if (type == 1)
            {
                PbFriendAvatarInfo playerInfo = Data as PbFriendAvatarInfo;
                if (dbId == playerInfo.dbid)
                {
                    SetInviteState(playerInfo.dbid);
                }
            }
            else
            {
                PbGuildMemberInfo playerInfo = Data as PbGuildMemberInfo;
                if (dbId == playerInfo.dbid)
                {
                    SetInviteState(playerInfo.dbid);
                }
            }

        }

        public override void Show()
        {
            AddEvents();
        }

        public override void Hide()
        {
            RemoveEvents();
        }

        public override object Data
        {
            get
            {
                return base.Data;
            }
            set
            {
                base.Data = value;
                UpdateContent();
            }
        }

        private void UpdateContent()
        {
            this.gameObject.SetActive(Data != null);
            if(Data is PbNearPlayer)  //附近玩家
            {
                PbNearPlayer playerInfo = Data as PbNearPlayer;
                m_txtInvite.gameObject.SetActive(playerInfo.had_invite == 1);


                SetInviteState(playerInfo.dbid);
                m_txtMapName.CurrentText.text = instance_helper.GetInstanceName((int)playerInfo.map_id);
                m_txtFightValue.CurrentText.text = playerInfo.fight_force.ToString();
                m_txtName.CurrentText.text = playerInfo.name;
                m_txtLevel.CurrentText.text = playerInfo.level.ToString();
                m_iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)playerInfo.vocation));
            }
            else if(Data is PbFriendAvatarInfo)
            {
                PbFriendAvatarInfo friendInfo = Data as PbFriendAvatarInfo;
                m_txtInvite.gameObject.SetActive(friendInfo.had_invite == 1);
                SetInviteState(friendInfo.dbid);
                m_txtMapName.CurrentText.text = instance_helper.GetInstanceName((int)friendInfo.map_id);
                m_txtFightValue.CurrentText.text = friendInfo.fight.ToString();
                m_txtName.CurrentText.text = friendInfo.name;
                m_txtLevel.CurrentText.text = friendInfo.level.ToString();
                m_iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)friendInfo.vocation));
            }
            else if (Data is PbGuildMemberInfo)
            {
                PbGuildMemberInfo guildInfo = Data as PbGuildMemberInfo;
                m_txtInvite.gameObject.SetActive(guildInfo.had_invite == 1);
                SetInviteState(guildInfo.dbid);
                 m_txtMapName.CurrentText.text = string.Empty;
               // m_txtMapName.CurrentText.text = instance_helper.GetInstanceName((int)guildInfo.);
                m_txtFightValue.CurrentText.text = guildInfo.fight.ToString();
                m_txtName.CurrentText.text = guildInfo.name;
                m_txtLevel.CurrentText.text = guildInfo.level.ToString();
                m_iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)guildInfo.vocation));
            }
        }


        private void SetInviteState(UInt64 dbId)
        {
            bool had_invite = PlayerDataManager.Instance.TeamData.inviteDict.ContainsKey(dbId) && Global.serverTimeStamp - PlayerDataManager.Instance.TeamData.inviteDict[dbId] < GlobalParams.GetInviteCd();
            m_txtInvite.gameObject.SetActive(had_invite);
            m_btnAdd.gameObject.SetActive(!had_invite);
            if (!had_invite && PlayerDataManager.Instance.TeamData.CaptainId != dbId && PlayerDataManager.Instance.TeamData.CaptainId != 0)
            {
                m_txtInvite.ChangeAllStateText(MogoLanguageUtil.GetString(LangEnum.TEAM_WAIT_CAPTAIN));
            }
            else
            {
                m_txtInvite.ChangeAllStateText(MogoLanguageUtil.GetString(LangEnum.TEAM_HAS_INVITE));
            }
        }

        private void OnClickAdd()
        {
            if(Data is PbNearPlayer)
            {
                PbNearPlayer playerInfo = Data as PbNearPlayer;
                TeamManager.Instance.Invite(playerInfo.dbid,0, InstanceId);
            }
            else if(Data is PbGuildMemberInfo)
            {
                PbGuildMemberInfo guildInfo = Data as PbGuildMemberInfo;
                TeamManager.Instance.Invite(guildInfo.dbid, 2, InstanceId);
            }
            else if(Data is PbFriendAvatarInfo)
            {
                PbFriendAvatarInfo friendInfo = Data as PbFriendAvatarInfo;
                TeamManager.Instance.Invite(friendInfo.dbid, 1, InstanceId);
            }
        }
    }
}
