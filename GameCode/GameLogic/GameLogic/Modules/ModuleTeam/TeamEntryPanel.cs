﻿#region 模块信息
/*==========================================
// 文件名：TeamEntryPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleTeam
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/19 10:39:46
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using Mogo.Util;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTeam
{
    public class TeamEntryPanel : TeamEntryBasePanel
    {
        private const int WAIT_INTERVAL = 60;

        private TeamEntryDataWrapper.TeamEntryType _teamEntryType;
        private int _instId;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TeamEntry; }
        }

        protected override void Awake()
        {
            base.Awake();
            RectTransform bgTransform = GetChildComponent<RectTransform>("Container_Bg/Container_zudui");
            for (int i = 0; i < bgTransform.childCount; i++)
            {
                bgTransform.GetChild(i).gameObject.SetActive(true);
            }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            TeamEntryDataWrapper wrapper = data as TeamEntryDataWrapper;
            _teamEntryType = wrapper.Type;
            _instId = (int)wrapper.instId;
            _btnClose.Visible = PlayerDataManager.Instance.TeamData.CaptainId == PlayerAvatar.Player.dbid;
            _btnCancel.Visible = PlayerDataManager.Instance.TeamData.CaptainId == PlayerAvatar.Player.dbid;
            Refresh();
            AutoAgreeWhenFollowCaptain();
        }

        public override void OnEnterMap(int mapType)
        {

        }

        public override void OnLeaveMap(int mapType)
        {
            
        }

        private void Refresh()
        {
            RefreshTitle();
            SetEndTime(Global.serverTimeStamp + WAIT_INTERVAL * 1000);
            RefreshList();
            RefreshButtonVisibility();
            RefreshChatView();
        }

        private void RefreshTitle()
        {
            copyName = instance_helper.GetTeamInstanceName(_instId);
            if (_teamEntryType == TeamEntryDataWrapper.TeamEntryType.immediacy)
            {
                _txtTitle.CurrentText.text = MogoLanguageUtil.GetContent(1021, copyName);
            }
            else
            {
                _txtTitle.CurrentText.text = MogoLanguageUtil.GetContent(1027, copyName);
            }
        }

        private void RefreshList()
        {
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            Dictionary<ulong, PbTeamMember> teammateDic = teamData.TeammateDic;
            PbTeamMember captain = teammateDic[teamData.CaptainId];
            itemList[0].SetInfo(captain, true);
            int index = 1;
            foreach (KeyValuePair<ulong, PbTeamMember> kvp in teammateDic)
            {
                if (kvp.Value.dbid != captain.dbid)
                {
                    itemList[index].SetInfo(kvp.Value);
                    index++;
                }
            }
            for (; index < 4; index++)
            {
                itemList[index].SetInfo(null);
            }
        }

        private void RefreshButtonVisibility()
        {
            if (PlayerDataManager.Instance.TeamData.CaptainId != PlayerAvatar.Player.dbid)
            {
                _btnAgree.Visible = true;
                _btnRefuse.Visible = true;
            }
            else
            {
                _btnAgree.Visible = false;
                _btnRefuse.Visible = false;
            }
        }

        private void RefreshChatView()
        {
            _entryChatView.Visible = true;
            _entryChatView.Refresh();
        }

        public override void OnClose()
        {
            base.OnClose();
            RemoveEventListener();
        }

        protected virtual void AddEventListener()
        {
            EventDispatcher.AddEventListener<UInt64, byte, UInt32>(TeamEvent.TEAM_MEMBER_REPLY, OnTeamMemberReply);
            EventDispatcher.AddEventListener(CopyEvents.ON_MATCH_MISSION_CHANGE, OnMatchMissionChange);
            _btnCancel.onClick.AddListener(OnCancelTeam);
            EventDispatcher.AddEventListener<PanelIdEnum>(PanelEvents.ENABLE_PANEL_CANVAS, OnEnablePanelCanvas);
        }

        protected virtual void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<UInt64, byte, UInt32>(TeamEvent.TEAM_MEMBER_REPLY, OnTeamMemberReply);
            EventDispatcher.RemoveEventListener(CopyEvents.ON_MATCH_MISSION_CHANGE, OnMatchMissionChange);
            _btnCancel.onClick.RemoveListener(OnCancelTeam);
            EventDispatcher.RemoveEventListener<PanelIdEnum>(PanelEvents.ENABLE_PANEL_CANVAS, OnEnablePanelCanvas);
        }

        private void OnCancelTeam()
        {
            TeamManager.Instance.Agree(0);
        }

        private void OnTeamMemberReply(UInt64 dbId, byte state, UInt32 instId)
        {
            if (state == 1)
            {
                for (int index = 0; index < 4; index++)
                {
                    itemList[index].SetAgree(dbId);
                }
            }
            else
            {
                ClosePanel();
            }
            if (dbId == PlayerAvatar.Player.dbid)
            {
                _btnAgree.Visible = false;
                _btnRefuse.Visible = false;
                _btnClose.Visible = true;
            }
        }

        private void OnMatchMissionChange()
        {
            ClosePanel();
            ShowInstanceView();
        }

        private void ShowInstanceView()
        {
            switch (instance_helper.GetChapterType(_instId))
            {
                case ChapterType.TeamCopy:
                    UIManager.Instance.ShowPanel(PanelIdEnum.TeamInstance, _instId);
                    break;
            }
        }

        protected override void OnClickAgree()
        {
            TeamManager.Instance.Agree(1);
        }

        protected override void OnClickReject()
        {
            TeamManager.Instance.Agree(0);
        }

        private void OnEnablePanelCanvas(PanelIdEnum panelId)
        {
            //if (panelId != Id)
            //    return;
            //_entryChatView.HideConvenientView();
        }
    }
}
