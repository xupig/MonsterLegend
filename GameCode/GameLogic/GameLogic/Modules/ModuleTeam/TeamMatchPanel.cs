﻿#region 模块信息
/*==========================================
// 文件名：TeamMatchPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleTeam
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/2/23 10:38:12
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTeam
{
    public class TeamMatchPanel:BasePanel
    {
        private TeamScrollMenu _scrollMenu;
        private NumberPicker _pickerLeft;
        private NumberPicker _pickerRight;
        //private KScrollView _scrollView;
        //private KList _list;
        private KInputField _input;
        private KToggle _needReview;
        private KButton _btnWorldSpeak;
        private KButton _btnAutoMatch;
        private StateText _txtAutoMatch;
        private int _currentSelectedIndex = -1;
        private StateText _txtJi;
        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TeamMatch; }
        }

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("Container_bg/ScaleImage_sharedZhezhao");
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _scrollMenu = AddChildComponent<TeamScrollMenu>("Container_xuanze/ScrollView_xuanzehuodong");

            _txtJi = GetChildComponent<StateText>("Container_xuanzeright/Container_levelPicker/Label_txtJi");
            _txtJi.CurrentText.text = (6016132).ToLanguage();
            _pickerLeft = AddChildComponent<NumberPicker>("Container_xuanzeright/Container_levelPicker/ScrollView_Num1");

            _pickerRight = AddChildComponent<NumberPicker>("Container_xuanzeright/Container_levelPicker/ScrollView_Num2");
            //_scrollView = GetChildComponent<KScrollView>("Container_xuanzeright/ScrollView_huafen");
           // _list = _scrollView.GetChildComponent<KList>("mask/content");
            _input = GetChildComponent<KInputField>("Container_xuanzeright/Input_input");
            _input.lineType = UnityEngine.UI.InputField.LineType.MultiLineSubmit;
            _needReview = GetChildComponent<KToggle>("Container_xuanzeright/Toggle_shifoushencha");
            _needReview.GetChildComponent<StateText>("txtShifoushencha").ChangeAllStateText((6016035).ToLanguage());
            _btnAutoMatch = GetChildComponent<KButton>("Button_zidongpipei");
            _btnWorldSpeak = GetChildComponent<KButton>("Button_shijiehanhua");
            _btnWorldSpeak.GetChildComponent<StateText>("label").ChangeAllStateText((6016018).ToLanguage());
            _txtAutoMatch = _btnAutoMatch.GetChildComponent<StateText>("label");
            base.Awake();
        }

        private void AddEventListener()
        {
            _needReview.onValueChanged.AddListener(OnValueChanged);
            _btnAutoMatch.onClick.AddListener(OnClickAutoMatch);
            _btnWorldSpeak.onClick.AddListener(OnClickWorldSpeak);
            //_list.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            _scrollMenu.onClick.AddListener(OnClickScrollMenu);
            EventDispatcher.AddEventListener(TeamEvent.CAPTAIN_MATCH_INFO_REFRESH, OnCaptainMatchInfoRefresh);
        }

        private void RemoveEventListener()
        {
            _needReview.onValueChanged.RemoveListener(OnValueChanged);
            _btnAutoMatch.onClick.RemoveListener(OnClickAutoMatch);
            _btnWorldSpeak.onClick.RemoveListener(OnClickWorldSpeak);
            //_list.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            _scrollMenu.onClick.RemoveListener(OnClickScrollMenu);
            EventDispatcher.RemoveEventListener(TeamEvent.CAPTAIN_MATCH_INFO_REFRESH, OnCaptainMatchInfoRefresh);
        }

        private void OnClear()
        {
            _currentSelectedIndex = -1;
            _btnAutoMatch.Visible = false;
            _btnWorldSpeak.Visible = false;
        }

        private void OnCaptainMatchInfoRefresh()
        {
            if (PlayerDataManager.Instance.TeamData.auto_team_game_id == 0 || PlayerDataManager.Instance.TeamData.auto_team_is_start == 0)
            {
                _txtAutoMatch.ChangeAllStateText((6016144).ToLanguage());
            }
            else
            {
                _txtAutoMatch.ChangeAllStateText((6016060).ToLanguage());
            }
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            team_detail detail = team_helper.GetTeamDetail(_currentSelectedIndex);
            int minLevel = team_helper.GetDetailMinLevel(detail);
            int maxLevel = team_helper.GetDetailMaxLevel(detail);
            int currentMinLevel = minLevel;
            int currentMaxLevel = maxLevel;
            if (teamData.auto_team_game_id != 0 && _currentSelectedIndex == teamData.auto_team_game_id)
            {
                currentMinLevel = Convert.ToInt32(PlayerDataManager.Instance.TeamData.auto_team_min_level);
                currentMaxLevel = Convert.ToInt32(PlayerDataManager.Instance.TeamData.auto_team_max_level);
                _input.text = teamData.auto_team_content;
                _needReview.isOn = teamData.auto_review;
            }
            else
            {
                _input.text = string.Empty;
                _needReview.isOn = true;
            }
            dailyLeftTimes = 10;
            dailyTotalTimes = 10;
            if (detail.__task == 0)
            {
                dailyLeftTimes = instance_helper.GetDailyLeftTimes(detail.__instance_id);
                dailyTotalTimes = instance_helper.GetDailyTimes(detail.__instance_id);
            }
            _pickerLeft.InitPicker(minLevel, maxLevel);
            _pickerLeft.CurrentValue = currentMinLevel;
            _pickerRight.InitPicker(minLevel, maxLevel);
            _pickerRight.CurrentValue = currentMaxLevel;
        }

        private int dailyLeftTimes = 10;
        private int dailyTotalTimes = 10;

        private void OnClickScrollMenu(int id)
        {
            _currentSelectedIndex = id;
            team_detail detail = team_helper.GetTeamDetail(_currentSelectedIndex);
            int minLevel = team_helper.GetDetailMinLevel(detail);
            int maxLevel = team_helper.GetDetailMaxLevel(detail);
            int currentMinLevel = minLevel;
            int currentMaxLevel = maxLevel;
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            if (teamData.auto_team_game_id != 0 && _currentSelectedIndex == teamData.auto_team_game_id)
            {
                currentMinLevel = Convert.ToInt32(PlayerDataManager.Instance.TeamData.auto_team_min_level);
                currentMaxLevel = Convert.ToInt32(PlayerDataManager.Instance.TeamData.auto_team_max_level);
                _input.text = teamData.auto_team_content;
                _needReview.isOn = teamData.auto_review;
            }
            else
            {
                _input.text = string.Empty;
                _needReview.isOn = true;
            }
            dailyLeftTimes = 10;
            dailyTotalTimes = 10;
            if (detail.__task == 0)
            {
                dailyLeftTimes = instance_helper.GetDailyLeftTimes(detail.__instance_id);
                dailyTotalTimes = instance_helper.GetDailyTimes(detail.__instance_id);
            }
            _pickerLeft.InitPicker(minLevel, maxLevel);
            _pickerLeft.CurrentValue = currentMinLevel;
            _pickerRight.InitPicker(minLevel, maxLevel);
            _pickerRight.CurrentValue = currentMaxLevel;            
        }

        private void OnClickWorldSpeak()
        {
            if (_currentSelectedIndex == -1)
            {
                return;
            }
            if (dailyTotalTimes > 0 && dailyLeftTimes == 0)
            {
                MogoUtils.FloatTips(1416);
                return;
            }

            TeamManager.Instance.TeamCall(_currentSelectedIndex, _pickerLeft.CurrentValue, _pickerRight.CurrentValue, ChatManager.Instance.FilterContent(_input.text), Convert.ToInt32(_needReview.isOn));
        }

        private void OnClickAutoMatch()
        {
            if (_currentSelectedIndex == -1)
            {
                return;
            }
            if(PlayerDataManager.Instance.TeamData.auto_team_game_id == 0 || PlayerDataManager.Instance.TeamData.auto_team_is_start == 0)
            {
                if (dailyTotalTimes > 0 && dailyLeftTimes == 0)
                {
                    MogoUtils.FloatTips(1416);
                    return;
                }

                TeamManager.Instance.CaptainAutoMatch(_currentSelectedIndex, _pickerLeft.CurrentValue, _pickerRight.CurrentValue, ChatManager.Instance.FilterContent(_input.text), Convert.ToInt32(_needReview.isOn));
            }
            else
            {
                TeamManager.Instance.StopAutoMatch();
            }
        }

        private void OnValueChanged(KToggle toggle,bool isOn)
        {

        }



        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
           
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            if (PlayerDataManager.Instance.TeamData.auto_team_game_id == 0 || PlayerDataManager.Instance.TeamData.auto_team_is_start == 0)
            {
                _txtAutoMatch.ChangeAllStateText((6016144).ToLanguage());
            }
            else
            {
                _txtAutoMatch.ChangeAllStateText((6016060).ToLanguage());
            }
            _currentSelectedIndex = -1;
            _scrollMenu.SetMenuData(team_helper.GetMiniTeamMenuList());
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            if(data != null)
            {
                team_detail detail = team_helper.GetTeamDetail((int)data);
                _scrollMenu.SelectItemByValue(detail.__type, detail.__id);
            }
            else if(teamData.auto_team_game_id == 0)
            {
                _input.text = string.Empty;
                _needReview.isOn = true;
                _scrollMenu.SelectItem(0, 0);
            }
            else
            {
                team_detail detail = team_helper.GetTeamDetail(Convert.ToInt32(teamData.auto_team_game_id));
                _scrollMenu.SelectItemByValue(detail.__type, detail.__id);
            }           
        }

    }
}
