﻿using Common.Base;
using Common.Global;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;

namespace ModuleTeam
{
    public abstract class TeamEntryBasePanel : BasePanel
    {
        private uint _timeId = TimerHeap.INVALID_ID;
        private string timeCountTemplate;
        private ulong _endTime;

        protected KButton _btnAgree;
        protected KButton _btnRefuse;
        protected StateText _txtTitle;
        protected StateText _txtTimeCount;
        protected KButton _btnClose;
        protected KButton _btnCancel;
        protected TeamEntryChatView _entryChatView;

        protected List<TeamEntryPlayerItem> itemList;
        public static string copyName = string.Empty;
        public static ulong teamEndTime = 0;

        protected override void Awake()
        {
            _btnClose = GetChildComponent<KButton>("Button_jianshao");
            _btnAgree = GetChildComponent<KButton>("Button_tongyi");
            _btnRefuse = GetChildComponent<KButton>("Button_jujue");
            _btnCancel = GetChildComponent<KButton>("Button_quxiao");
            _txtTitle = GetChildComponent<StateText>("Label_txtJihe");
            _txtTimeCount = GetChildComponent<StateText>("Label_txtDaojishi");
            timeCountTemplate = _txtTimeCount.CurrentText.text;
            _entryChatView = AddChildComponent<TeamEntryChatView>("Container_liaotian");

            itemList = new List<TeamEntryPlayerItem>();
            for (int i = 0; i < 4; i++)
            {
                TeamEntryPlayerItem item = AddChildComponent<TeamEntryPlayerItem>(string.Format("Container_Duiyuan{0}", i));
                itemList.Add(item);
            }

            AddButtonListener();
        }

        protected override void OnDestroy()
        {
            RemoveButtonListener();
        }

        private void AddButtonListener()
        {
            _btnClose.onClick.AddListener(OnClickClose);
            _btnAgree.onClick.AddListener(OnClickAgree);
            _btnRefuse.onClick.AddListener(OnClickReject);
        }

        private void RemoveButtonListener()
        {
            _btnClose.onClick.RemoveListener(OnClickClose);
            _btnAgree.onClick.RemoveListener(OnClickAgree);
            _btnRefuse.onClick.RemoveListener(OnClickReject);
        }

        private void OnClickClose()
        {
            _entryChatView.HideConvenientView();
            UIManager.Instance.DisablePanelCanvas(Id);
            EventDispatcher.TriggerEvent(MainUIEvents.SHOW_TEAM_ENTRY_VIEW, Id);
        }

        protected virtual void OnClickAgree() {}
        protected virtual void OnClickReject() {}

        protected void AutoAgreeWhenFollowCaptain()
        {
            if (PlayerAutoFightManager.GetInstance().fightType == FightType.FOLLOW_FIGHT)
            {
                OnClickAgree();
            }
        }

        public override void OnClose()
        {
            RemoveTimer();
        }

        protected void SetEndTime(ulong endTime)
        {
            _endTime = endTime;
            teamEndTime = endTime;
            RemoveTimer();
            _timeId = TimerHeap.AddTimer(0, 500, TimeStep);
        }

        private void RemoveTimer()
        {
            if (_timeId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timeId);
                _timeId = TimerHeap.INVALID_ID;
            }
        }

        protected void TimeStep()
        {
            _txtTimeCount.CurrentText.text = string.Format(timeCountTemplate, (_endTime - Global.serverTimeStamp) / 1000);
            if (_endTime <= Global.serverTimeStamp)
            {
                TimerEnd();
            }
        }

        protected virtual void TimerEnd()
        {
            RemoveTimer();
            ClosePanel();
        }
    }
}
