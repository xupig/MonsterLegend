﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/15 10:37:27
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Events;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTeam
{
    public class TeamModule:BaseModule
    {
        public TeamModule() 
        {
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            //AddPanel(PanelIdEnum.TeamInvite, "Container_TeamInvitePanel.prefab", MogoUILayer.LayerUIPanel, "ModuleTeam.TeamInvitePanel");
            AddPanel(PanelIdEnum.Team, "Container_InteractiveTeamPanel", MogoUILayer.LayerUIPanel, "ModuleTeam.TeamPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.TeamEntry, "Container_TeamEntryPanel", MogoUILayer.LayerUIToolTip, "ModuleTeam.TeamEntryPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.KickTeammate, "Container_KickTeammateTip", MogoUILayer.LayerUIToolTip, "ModuleTeam.KickTeammatePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.PlayAgain, "Container_TeamEntryPanel", MogoUILayer.LayerUIPanel, "ModuleTeam.PlayAgainPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.TeamMatch, "Container_TeamPopPanel", MogoUILayer.LayerUIToolTip, "ModuleTeam.TeamMatchPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.TeamInvitePop, "Container_TeamInvitePopPanel", MogoUILayer.LayerAlert, "ModuleTeam.TeamInvitePopPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.TeamDonate, "Container_TeamDonatePanel", MogoUILayer.LayerUIToolTip, "ModuleTeam.TeamDonatePopPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            RegisterPanelModule(PanelIdEnum.Team, PanelIdEnum.Interactive);
        }
    }
}
