﻿#region 模块信息
/*==========================================
// 文件名：TeamEntryMainPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleTeam
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/1/16 17:21:47
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Global;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTeam
{

    public class TeamEntryMainView:KContainer
    {
        private StateText _txtContent;
        private int count = 0;
        private PanelIdEnum _context;
        private KButton CloseBtn;
        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_zhankai");
            _txtContent = GetChildComponent<StateText>("Label_txtXinxi");
            CloseBtn.onClick.AddListener(OnClose);
            base.Awake();
        }

        public void SetData(PanelIdEnum data)
        {
            Visible = true;
            _context = data;
            _txtContent.CurrentText.text = (6260002).ToLanguage(TeamEntryBasePanel.copyName, (TeamEntryBasePanel.teamEndTime - Global.serverTimeStamp) / 1000);
        }

        public void OnClose()
        {
            Visible = false;
            UIManager.Instance.EnablePanelCanvas(_context);
        }

        void Update()
        {
            count++;
            long time = 10;
            if(count>=15)
            {
                count = 0;
                time = Convert.ToInt64(TeamEntryBasePanel.teamEndTime) - Convert.ToInt64(Global.serverTimeStamp);
                _txtContent.CurrentText.text = (6260002).ToLanguage(TeamEntryBasePanel.copyName, time/1000);
            }
            if (time < 0)
            {
                Visible = false;
            }
        }

    }
}
