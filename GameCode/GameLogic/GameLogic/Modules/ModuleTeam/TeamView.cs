﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/15 10:38:11
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleTeam.ChildView;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ModuleTeam
{
    public class TeamPanelParamter : BasePanelParameter
    {
        public int InstanceId;

        public TeamPanelParamter(int firstTab, int secondTab, int instanceId)
        {
            this.FirstTab = firstTab;
            this.SecondTab = secondTab;
            this.InstanceId = instanceId;
        }
    }

    public class TeamPanel:BasePanel
    {

        private CategoryList m_dyToggleGroup;
        private CategoryList m_dzToggleGroup;
        private CategoryList m_wzdToggleGroup;
        private MyTeamView m_MyTeam;              //我的队伍
        private InvitePlayerView m_InvitePlayer; //邀请玩家
        private ApplyTeamView m_ApplyTeam; //申请队伍
        private ApplyListView m_ApplyList; //申请列表
        private TeamAwardAllocateView _awardAllocateView;

        private int _instanceId;

        private uint teamId = 0;

        public override void OnShow(System.Object data)
        {
            Visible = true;
            RefreshToggleGroup();
            //OnReceivedListChange();
           // m_InvitePlayer.UpdateContent(0);
           // m_ApplyTeam.UpdateContent(m_ApplyTeam.tab);
            SetData(data);
            AddEventListener();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Team; }
        }

        public override void SetData(System.Object data)
        {
            teamId = PlayerDataManager.Instance.TeamData.TeamId;
            _instanceId = instance_helper.InvalidInstanceId;
            int index = 0;
            int secondIndex = 0;
            if (data == null)
            {
                index = 0;
            }
            else if (data is int)
            {
                index = (int)data - 1;
            }
            else if (data is TeamPanelParamter)
            {
                TeamPanelParamter teamPanelParamter = data as TeamPanelParamter;
                index = teamPanelParamter.FirstTab - 1;
                if (teamPanelParamter.SecondTab != BasePanelParameter.InvalidTab)
                {
                    secondIndex = teamPanelParamter.SecondTab - 1;
                }
                _instanceId = teamPanelParamter.InstanceId;
            }
            else
            {
                int[] list = data as int[];
                index = list[0] - 1;
                secondIndex = list[1] - 1;
            }
            ChangeTeamToggleGroup(index, secondIndex);
            RefreshPoint();
        }

        private void ChangeTeamToggleGroup(int index, int secondIndex = 0)
        {
            if (m_dzToggleGroup.gameObject.activeSelf)
            {
                m_dzToggleGroup.SelectedIndex = index;
                OnCaptainSelectedIndexChanged(m_dzToggleGroup, index, secondIndex);
            }
            else if (m_wzdToggleGroup.gameObject.activeSelf)
            {
                m_wzdToggleGroup.SelectedIndex = index;
                OnTeamlessSelectedIndexChanged(m_wzdToggleGroup, index, secondIndex-1);
            }
            else if (m_dyToggleGroup.gameObject.activeSelf)
            {
                m_dyToggleGroup.SelectedIndex = index;
                OnTeammateSelectedIndexChanged(m_dyToggleGroup, index, secondIndex);
            }
        }

        public override void OnClose()
        {
            Visible = false;
            RemoveEventListener();
        }


        /**提取UI子级组件[UI资源加载完成时调用]**/
        protected override void Awake()
        {
            m_dyToggleGroup = GetChildComponent<CategoryList>("Container_dycategory/List_categoryList");

            m_dzToggleGroup = GetChildComponent<CategoryList>("Container_dzcategory/List_categoryList");
            m_wzdToggleGroup = GetChildComponent<CategoryList>("Container_wzdcategory/List_categoryList");
            
            m_MyTeam = AddChildComponent<MyTeamView>("Container_WDDW");
            m_InvitePlayer = AddChildComponent<InvitePlayerView>( "Container_YQWJ");
            m_ApplyTeam = AddChildComponent<ApplyTeamView>( "Container_SQDW");
            m_ApplyList = AddChildComponent<ApplyListView>( "Container_SQLB");
            _awardAllocateView = AddChildComponent<TeamAwardAllocateView>("Container_JLFP");
            m_dyToggleGroup.gameObject.SetActive(false);
            m_dzToggleGroup.gameObject.SetActive(false);
            m_wzdToggleGroup.gameObject.SetActive(false);
            TeamManager.Instance.GetAutoTeamInfo();
            //TeamManager.Instance.GetNearPlayerList(1);
        }

        private void OnTeammateSelectedIndexChanged(CategoryList toggleGroup, int index)
        {
            OnTeammateSelectedIndexChanged(toggleGroup, index, 0);
        }

        private void OnTeammateSelectedIndexChanged(CategoryList toggleGroup, int index, int secondIndex)
        {
            m_ApplyTeam.Hide();
            m_ApplyList.Hide();
            switch(index)
            {
                case 0:
                    m_MyTeam.Show();
                    m_InvitePlayer.Hide();
                    _awardAllocateView.Hide();
                    break;
                case 1:
                    m_MyTeam.Hide();
                    m_InvitePlayer.Show(secondIndex, _instanceId);
                    _awardAllocateView.Hide();
                    break;
                case 2:
                    m_MyTeam.Hide();
                    m_InvitePlayer.Hide();
                    _awardAllocateView.Show();
                    break;
            }
        }

        private void OnCaptainSelectedIndexChanged(CategoryList toggleGroup, int index)
        {
            OnCaptainSelectedIndexChanged(toggleGroup, index, 0);
        }

        private void OnCaptainSelectedIndexChanged(CategoryList toggleGroup, int index, int secondIndex)
        {
            m_ApplyTeam.Hide();
            switch (index)
            {
                case 0:
                    m_MyTeam.Show();
                    m_InvitePlayer.Hide();
                    m_ApplyList.Hide();
                    _awardAllocateView.Hide();
                    break;
                case 1:
                    m_MyTeam.Hide();
                    m_InvitePlayer.Show(secondIndex, _instanceId);
                    m_ApplyList.Hide();
                    _awardAllocateView.Hide();
                    break;
                case 2:
                    m_MyTeam.Hide();
                    m_InvitePlayer.Hide();
                    m_ApplyList.Show(_instanceId);
                    _awardAllocateView.Hide();
                    break;
                case 3:
                    m_MyTeam.Hide();
                    m_InvitePlayer.Hide();
                    m_ApplyList.Hide();
                    _awardAllocateView.Show();
                    break;
                default:
                    toggleGroup.SelectedIndex = 0;
                    m_MyTeam.Show();
                    m_InvitePlayer.Hide();
                    m_ApplyList.Hide();
                    break;
            }
        }
       
        private void OnTeamlessSelectedIndexChanged(CategoryList toggleGroup, int index)
        {
            OnTeamlessSelectedIndexChanged(toggleGroup, index, -1);
        }

        private void OnTeamlessSelectedIndexChanged(CategoryList toggleGroup, int index, int secondIndex)
        {
            m_MyTeam.Hide();
            switch (index)
            {
                case 0:
                    m_ApplyTeam.Show(secondIndex);
                    m_InvitePlayer.Hide();
                    m_ApplyList.Hide();
                    _awardAllocateView.Hide();
                    break;
                case 1:
                    m_ApplyTeam.Hide();
                    m_InvitePlayer.Show(secondIndex+1, _instanceId);
                    m_ApplyList.Hide();
                    _awardAllocateView.Hide();
                    break;

                case 2:
                    m_ApplyTeam.Hide();
                    m_InvitePlayer.Hide();
                    m_ApplyList.Show(_instanceId);
                    _awardAllocateView.Hide();
                    break;
                case 3:
                    m_ApplyTeam.Hide();
                    m_InvitePlayer.Hide();
                    m_ApplyList.Hide();
                    _awardAllocateView.Show();
                    break;
            }
        }


        /**统一注册事件监听**/
        private void AddEventListener()
        {
            m_dyToggleGroup.onSelectedIndexChanged.AddListener(OnTeammateSelectedIndexChanged);
            m_dzToggleGroup.onSelectedIndexChanged.AddListener(OnCaptainSelectedIndexChanged);
            m_wzdToggleGroup.onSelectedIndexChanged.AddListener(OnTeamlessSelectedIndexChanged);

            EventDispatcher.AddEventListener(TeamEvent.TEAM_REFRESH, OnRefreshTeam);
            EventDispatcher.AddEventListener(TeamEvent.TEAM_RECEIVED_LIST_CHANGE, OnReceivedListChange);

            EventDispatcher.AddEventListener(TeamEvent.TEAM_POINT_REFRESH, RefreshPoint);
        }

        /**统一移除事件监听**/
        private void RemoveEventListener()
        {
            if (m_dyToggleGroup!=null)
            {
                m_dyToggleGroup.onSelectedIndexChanged.RemoveListener(OnTeammateSelectedIndexChanged);
                m_dzToggleGroup.onSelectedIndexChanged.RemoveListener(OnCaptainSelectedIndexChanged);
                m_wzdToggleGroup.onSelectedIndexChanged.RemoveListener(OnTeamlessSelectedIndexChanged);

                EventDispatcher.RemoveEventListener(TeamEvent.TEAM_REFRESH, OnRefreshTeam);
                EventDispatcher.RemoveEventListener(TeamEvent.TEAM_RECEIVED_LIST_CHANGE, OnReceivedListChange);
                EventDispatcher.RemoveEventListener(TeamEvent.TEAM_POINT_REFRESH, RefreshPoint);
            }
        }

        #region UI事件回调处理

        private void RefreshPoint()
        {
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            List<TeamReceivedData> dataList = new List<TeamReceivedData>();
            if (teamData.CaptainId == 0)   //没有队伍
            {
                foreach (TeamReceivedData data in teamData.InvitationList)
                {
                    dataList.Add(data);
                }
            }
            else if (teamData.CaptainId == MogoWorld.Player.dbid)  //自己是队长
            {
                foreach (TeamReceivedData data in teamData.CaptainList)
                {
                    dataList.Add(data);
                }
            }
            (m_dzToggleGroup[2] as CategoryListItem).SetPoint(dataList.Count != 0);
            (m_wzdToggleGroup[2] as CategoryListItem).SetPoint(dataList.Count != 0);
        }

        public void OnRefreshTeam()
        {
            if (teamId!=PlayerDataManager.Instance.TeamData.TeamId || m_MyTeam.Visible)
            {
                RefreshToggleGroup();
                ChangeTeamToggleGroup(0);
            }
            teamId = PlayerDataManager.Instance.TeamData.TeamId;
        }

        private void RefreshToggleGroup()
        {
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            if (teamData.CaptainId == 0)//没有队伍
            {
                m_wzdToggleGroup.gameObject.SetActive(true);
                m_dzToggleGroup.gameObject.SetActive(false);
                m_dyToggleGroup.gameObject.SetActive(false);
            }
            else if (teamData.CaptainId == MogoWorld.Player.dbid)   //自己是队长
            {
                m_wzdToggleGroup.gameObject.SetActive(false);
                m_dzToggleGroup.gameObject.SetActive(true);
                m_dyToggleGroup.gameObject.SetActive(false);
                m_MyTeam.UpdateContent();
            }
            else    //自己是队员
            {
                m_wzdToggleGroup.gameObject.SetActive(false);
                m_dzToggleGroup.gameObject.SetActive(false);
                m_dyToggleGroup.gameObject.SetActive(true);
                m_MyTeam.UpdateContent();
            }
        }

        public void OnReceivedListChange()
        {
            m_ApplyList.UpdateContent();
        }




        #endregion

    }
}
