﻿using System;
using System.Collections.Generic;
using ModuleChat;

namespace ModuleTeam
{
    public class TeamChatConvenientView : ChatConvenientMenuView
    {
        protected override void InitMultifunctionView()
        {
            _multifunctionView = AddChildComponent<TeamChatMultifunctionView>("Container_chatMenu");
        }
    }
}
