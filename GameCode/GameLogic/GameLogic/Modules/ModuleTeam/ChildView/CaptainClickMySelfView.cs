﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2015/1/4 13:50:56
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Data.Chat;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.ServerConfig;
using Common.Data;
using GameMain.Entities;
using Common.Chat;
using GameMain.GlobalManager;
using Mogo.Util;
using GameData;

namespace ModuleTeam.ChildView
{
    public class CaptainClickMySelfView:KContainer
    {
        private KButton m_btnLeaveTeam;
        private KButton m_btnWorldSpeak;
        private KButton m_btnCallTeammate;

        protected override void Awake()
        {
            m_btnLeaveTeam = GetChildComponent<KButton>("Button_likaiduiwu");
            m_btnWorldSpeak = GetChildComponent<KButton>("Button_shijiehanhua");
            m_btnCallTeammate = GetChildComponent<KButton>("Button_zhaohuanquandui");
            AddEventListener();
        }

        protected void AddEventListener()
        {
            m_btnLeaveTeam.onClick.AddListener(OnClickLeaveTeam);
            m_btnWorldSpeak.onClick.AddListener(OnClickWorldSpeak);
            m_btnCallTeammate.onClick.AddListener(OnClickCallTeammate);
        }

        protected override void OnDestroy()
        {
            m_btnLeaveTeam.onClick.RemoveListener(OnClickLeaveTeam);
            m_btnWorldSpeak.onClick.RemoveListener(OnClickWorldSpeak);
            m_btnCallTeammate.onClick.RemoveListener(OnClickCallTeammate);
            base.OnDestroy();
        }


        private void OnClickLeaveTeam()
        {
            MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.TEAM_LEAVE_TEAM), LeaveTeam);
        }

        private void LeaveTeam()
        {
            TeamManager.Instance.LeaveTeam();
        }

        private void OnClickWorldSpeak()
        {
            PanelIdEnum.TeamMatch.Show();
            //SendBox.ShowSendBox(MogoLanguageUtil.GetString(LangEnum.TEAM_WORLR_SPEAK_TITLE), string.Empty, true, WorldSpeak, null);
        }

        private void WorldSpeak(string content)
        {
            //ChatManager.Instance.RequestSendChat(public_config.CHANNEL_ID_WORLD, content, 0, (byte)ChatContentType.Text);
            string contents = MogoLanguageUtil.GetContent(10118, content);
            ChatTeamLinkWrapper link = new ChatTeamLinkWrapper(contents, PlayerAvatar.Player.dbid, 0,1,team_helper.GetAvatarMaxLevel());
            ChatManager.Instance.RequestInviteLinkSendChat(public_config.CHANNEL_ID_WORLD, link);
        }

        private void OnClickCallTeammate()
        {
            MessageBox.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.TEAM_CALL_TEAMMATE), CallTeammate);
        }

        private void CallTeammate()
        {
            
            TeamManager.Instance.TeamCallAll();
        }
 

    }
}
