﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2015/1/4 16:19:59
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTeam.ChildView
{
    public class TeammateClickMySelfView:KContainer
    {
        #region 私有变量
        private KButton m_btnLeaveTeam;
        private KButton m_btnCallTeammate;
        #endregion

        #region 基础UI接口实现

        protected override void Awake()
        {
            m_btnLeaveTeam = GetChildComponent<KButton>("Button_likaiduiwu");
            m_btnCallTeammate = GetChildComponent<KButton>("Button_shijiehanhua");
            AddEventListener();
        }

        protected void AddEventListener()
        {
            m_btnLeaveTeam.onClick.AddListener(OnClickLeaveTeam);
            m_btnCallTeammate.onClick.AddListener(OnClickCallTeammate);
        }

        protected override void OnDestroy()
        {
            m_btnLeaveTeam.onClick.RemoveListener(OnClickLeaveTeam);
            m_btnCallTeammate.onClick.RemoveListener(OnClickCallTeammate);
            base.OnDestroy();
        }
        #endregion

        #region UI回调事件处理

        private void OnClickLeaveTeam()
        {
            MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.TEAM_LEAVE_TEAM), LeaveTeam);
        }

        private void OnClickCallTeammate()
        {
            MessageBox.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.TEAM_CALL_TEAMMATE), CallTeammate);
        }

        private void LeaveTeam()
        {
            TeamManager.Instance.LeaveTeam();
        }

        private void CallTeammate()
        {

            TeamManager.Instance.TeamCallAll();
        }

        #endregion
    }
}
