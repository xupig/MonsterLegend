﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/29 11:30:27
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ExtendComponent.PageableComponent;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleTeam.ChildComponent;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTeam.ChildView
{
    public class InvitePlayerView:KContainer
    {
        private KToggleGroup m_toggleGroup;

        private KScrollView m_scrollView;
        private KPageableList _invitePlayerList;
        private KButton _btnRefresh;

        private StateText _txtEmpty;

        private int _tab = 0;
        private int _instanceId;
        //private List<PbNearPlayer> m_nearPlayerList = new List<PbNearPlayer>();
        private List<PbFriendAvatarInfo> m_friendList = new List<PbFriendAvatarInfo>();
        private List<PbGuildMemberInfo> m_guildList = new List<PbGuildMemberInfo>();

        static readonly int[] s_Padding = new int[] { 5, 2, 5, 2 };
        static readonly int[] s_Gap = new int[] { -14, 0 };
        private static int capacity = 10;
        private uint _timerId;

        private int _currentItemCount = 6;
        private int PER_PAGE_COUNT = 6;

        protected override void Awake()
        {
            m_toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_PlayerContent");
            _txtEmpty = GetChildComponent<StateText>("Label_txtContent");
            _btnRefresh = GetChildComponent<KButton>("Button_shuaxin");
            
            _txtEmpty.Raycast = false;
            m_scrollView = GetChildComponent<KScrollView>("ScrollView_haoyou");
            _invitePlayerList = m_scrollView.GetChildComponent<KPageableList>("mask/content");

            _invitePlayerList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _invitePlayerList.SetGap(-14, 0);
            _invitePlayerList.SetPadding(5, 2, 5, 2);
            AddEventListener();

        }

        public void Show(int tab, int instanceId)
        {
            Visible = true;
            m_toggleGroup.SelectIndex = tab;
            _tab = tab;
            _instanceId = instanceId;
            OnClickRefresh();
        }
        
        private void UpdateData()
        {
            _btnRefresh.enabled = false;
            if(_timerId==0)
            {
                _timerId = TimerHeap.AddTimer(30, 1, OnTimeEnd);
            }
            for (int i = 0; i < _invitePlayerList.ItemList.Count; i++)
            {
                TeamInviteItem item = _invitePlayerList.ItemList[i] as TeamInviteItem;
                item.InstanceId = _instanceId;
            }
        }

        private void OnTimeEnd()
        {
            TimerHeap.DelTimer(_timerId);
            _timerId = 0;
            _btnRefresh.enabled = true;
        }

        public void Hide()
        {
            Visible = false;
        }

        protected void AddEventListener()
        {
            m_toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChangeHandler);
            m_scrollView.ScrollRect.onRequestNext.AddListener(ShowNextPage);
            _btnRefresh.onClick.AddListener(OnClickRefresh);
            EventDispatcher.AddEventListener<int>(TeamEvent.TEAM_NEAR_PLAYER_LIST_CHANGE, OnTeamNearPlayerListChange);
            EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_LIST,OnRefreshFriendList);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Member_List, OnGuildMemberListChange);
        }

        protected override void OnDestroy()
        {
            m_toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChangeHandler);
            m_scrollView.ScrollRect.onRequestNext.RemoveListener(ShowNextPage);
            _btnRefresh.onClick.RemoveListener(OnClickRefresh);
            EventDispatcher.RemoveEventListener<int>(TeamEvent.TEAM_NEAR_PLAYER_LIST_CHANGE, OnTeamNearPlayerListChange);
            EventDispatcher.RemoveEventListener(FriendEvents.REFRESH_FRIEND_LIST, OnRefreshFriendList);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Member_List, OnGuildMemberListChange);
            base.OnDestroy();
        }

        private void ShowNextPage(KScrollRect scrollRect)
        {
            _currentItemCount += PER_PAGE_COUNT;
            UpdateContent(m_toggleGroup.SelectIndex);
        }

        private void OnTeamNearPlayerListChange(int index)
        {
            UpdateContent(index);
            TweenListEntry.Begin(_invitePlayerList.gameObject);

            //if (PlayerDataManager.Instance.TeamData.hasNearPlayerDataResp == false)
            //{
            //    TweenListEntry.Begin(m_scrollView.List.gameObject);

            //}
            //PlayerDataManager.Instance.TeamData.hasNearPlayerDataResp = true;
        }


        private void OnRefreshFriendList()
        {
            if(m_toggleGroup.SelectIndex == 1)
            {
                UpdateContent(m_toggleGroup.SelectIndex);
            }
        }

        private void OnGuildMemberListChange()
        {
            if (m_toggleGroup.SelectIndex == 2)
            {
                UpdateContent(m_toggleGroup.SelectIndex);
            }
        }

        private void OnClickRefresh()
        {
            UpdateTextContent();
            if (m_toggleGroup.SelectIndex == 0)
            {
                //OnRequestPage(m_scrollView, m_scrollView.CurrentPage);
                TeamManager.Instance.GetNearPlayerList(0);
            }
            else if (m_toggleGroup.SelectIndex == 1)
            {
                if (FriendManager.Instance.SendGetFriendListMsg() == false)
                {
                    UpdateContent(m_toggleGroup.SelectIndex);
                }
            }
            else if (m_toggleGroup.SelectIndex == 2)
            {
                GuildManager.Instance.GetMemberList();
            }
        }

        public void UpdateContent(int index)
        {
            if (Visible == false)
            {
                return;
            }
            if (index != _tab)
            {
                return;
            }

            if(_tab == 0)//附近玩家
            {
                UpdateNearPlayerList();
            }
            else if(_tab == 1)//好友玩家
            {
                UpdateFriendList();
            }
            else   //公会玩家
            {
                UpdateGuildList();
            }
            UpdateData();
            UpdateTextContent();
        }

        private void UpdateTextContent()
        {
            int langId = 0;
            if (_tab == 0)//附近玩家
            {
                langId = 10106;
            }
            else if (_tab == 1)//好友玩家
            {
                langId = 10107;
            }
            else   //公会玩家
            {
                if (PlayerAvatar.Player.guild_id == 0)
                {
                    langId = 10109;
                }
                else
                {
                    langId = 10108;
                }
            }
            _txtEmpty.CurrentText.text = langId.ToLanguage();
        }

        private void UpdateGuildList()
        {
            m_guildList.Clear();
            List<PbGuildMemberInfo> list = PlayerDataManager.Instance.GuildData.GuildMemberInfoList.member_info;
            for (int i = 0; i < list.Count;i++ )
            {
                if (list[i].dbid != PlayerAvatar.Player.dbid && list[i].online != 0)
                {
                    m_guildList.Add(list[i]);
                }
            }
            int onlineGuildMemberListCount = m_guildList.Count;
            int showGuildMemberCount = _currentItemCount > onlineGuildMemberListCount ? onlineGuildMemberListCount : _currentItemCount;

            List<PbGuildMemberInfo> showMemberList = m_guildList.GetRange(0, showGuildMemberCount);
            _invitePlayerList.SetDataList<TeamInviteItem>(showMemberList);

            _txtEmpty.Visible = m_guildList.Count == 0;
            //m_scrollView.Visible = false;
        }

        private void UpdateFriendList()
        {
            m_friendList.Clear();
            List<PbFriendAvatarInfo> list =  PlayerDataManager.Instance.FriendData.FriendAvatarList;
            for(int i=0;i<list.Count;i++)
            {
                if (list[i].online == 1)
                {
                    m_friendList.Add(list[i]);
                }
            }

            int friendListCount = m_friendList.Count;
            int showFriendCount = _currentItemCount > friendListCount ? friendListCount : _currentItemCount;

            List<PbFriendAvatarInfo> showFriendList = m_friendList.GetRange(0, showFriendCount);
            _invitePlayerList.SetDataList<TeamInviteItem>(showFriendList);
            _txtEmpty.Visible = m_friendList.Count == 0;
            //m_scrollView.Visible = m_friendList.Count > 0;
        }

        private void UpdateNearPlayerList()
        {
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            for (int i = 0; i < 3;i++ )
            {
                teamData.NearPlayerList.Concat(teamData.NearPlayerList);
            }

            int nearPlayerCount = teamData.NearPlayerList.Count;
            int showNearPlayerCount = _currentItemCount > nearPlayerCount ? nearPlayerCount : _currentItemCount;

            List<PbNearPlayer> showNearPlayerList = teamData.NearPlayerList.GetRange(0, showNearPlayerCount);
            _invitePlayerList.SetDataList<TeamInviteItem>(showNearPlayerList);
            _txtEmpty.Visible = teamData.NearPlayerList.Count == 0;
            //m_scrollView.Visible = teamData.NearPlayerList.Count > 0;
        }


        private void OnSelectedIndexChangeHandler(KToggleGroup toggleGroup,int index)
        {
            _tab = index;
            UpdateTextContent();
            if(index == 1)
            {
                if (FriendManager.Instance.SendGetFriendListMsg() == false)
                {
                    UpdateContent(m_toggleGroup.SelectIndex);
                }
            }
            else if(index == 2)
            {
                GuildManager.Instance.GetMemberList();
            }
            else
            {
                UpdateContent(_tab);
            }
            TweenListEntry.Begin(_invitePlayerList.gameObject);
        }

    }
}
