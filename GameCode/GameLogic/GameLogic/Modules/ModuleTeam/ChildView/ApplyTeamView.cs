﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/29 11:27:47
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Structs;
using Common.ExtendComponent.ListToggleGroup;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleGem;
using ModuleTeam.ChildComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using MogoEngine.Events;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.ExtendComponent.PageableComponent;
using Common.Utils;
using GameMain.Entities;
using GameData;
using Mogo.Util;

namespace ModuleTeam.ChildView
{
    public class ApplyTeamView:KContainer
    {
        private KButton m_btnRefresh;
        private KButton _btnAutoMatch;
        private KButton _btnIWantBeCaptain;
        private StateText _txtContent;
        private StateText _txtAutoMatch;
        private StateText _txtIWantBrCaptain;
        private TeamScrollMenu _scrollMenu;
        private KScrollView _scrollView;
        private StateText _txtEmpty;
        private KPageableList _applyTeamList;
        private StateText _txtLeftCount;
        private int _currentSelectedId = -1;

        protected override void Awake()
        {
            _scrollMenu = AddChildComponent<TeamScrollMenu>("ScrollView_xuanzehuodong");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_applyScrollView");
            _applyTeamList = _scrollView.GetChildComponent<KPageableList>("mask/content");
            _txtEmpty = GetChildComponent<StateText>("Label_txtContent");
            _txtEmpty.CurrentText.text = (10103).ToLanguage();
            _txtLeftCount = GetChildComponent<StateText>("Label_txtShengyucishu");
            _txtContent = GetChildComponent<StateText>("Label_txtXunzhaozudui");
            m_btnRefresh = GetChildComponent<KButton>("Button_shuaxin");
            _btnAutoMatch = GetChildComponent<KButton>("Button_zidongpipei");
            _txtAutoMatch = _btnAutoMatch.GetChildComponent<StateText>("label");
            _txtAutoMatch.ChangeAllStateText((6016034).ToLanguage());
            _btnIWantBeCaptain = GetChildComponent<KButton>("Button_woyaodaidui");
            _txtIWantBrCaptain = _btnIWantBeCaptain.GetChildComponent<StateText>("label");
            _txtIWantBrCaptain.ChangeAllStateText((6016037).ToLanguage());
            InitApplyTeamList();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void InitApplyTeamList()
        {
            _applyTeamList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _applyTeamList.SetGap(-14, 0);
        }


        protected void AddEventListener()
        {
            m_btnRefresh.onClick.AddListener(OnClickRefresh);
            _btnAutoMatch.onClick.AddListener(OnClickAutoMatch);
            _btnIWantBeCaptain.onClick.AddListener(OnClickIWantBeCaptain);
            _scrollMenu.onClick.AddListener(OnSelectIndexChanged);
            EventDispatcher.AddEventListener(TeamEvent.TEAM_AUTO_TEAM_LIST_REFRESH, UpdateContent);
            EventDispatcher.AddEventListener(TeamEvent.TEAM_AUTO_TEAM_TAKE_INFO_REFRESH, OnTakeInfoRefresh);
            EventDispatcher.AddEventListener(TeamEvent.TEAM_AUTO_TEAM_MATCH_INFO_REFRESH, OnMatchInfoRefresh);
            EventDispatcher.AddEventListener(TeamEvent.REQUEST_APPLY_FAIL, OnClickRefresh); 
        }

        protected void RemoveEventListener()
        {
            if(m_btnRefresh!=null)
            {
                m_btnRefresh.onClick.RemoveListener(OnClickRefresh);
                _btnAutoMatch.onClick.RemoveListener(OnClickAutoMatch);
                _btnIWantBeCaptain.onClick.RemoveListener(OnClickIWantBeCaptain);
                _scrollMenu.onClick.RemoveListener(OnSelectIndexChanged);
            }
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_AUTO_TEAM_LIST_REFRESH, UpdateContent);
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_AUTO_TEAM_TAKE_INFO_REFRESH, OnTakeInfoRefresh);
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_AUTO_TEAM_MATCH_INFO_REFRESH, OnMatchInfoRefresh);
            EventDispatcher.RemoveEventListener(TeamEvent.REQUEST_APPLY_FAIL, OnClickRefresh); 
        }

        private void OnTakeInfoRefresh()
        {
            //Debug.LogError("OnTakeInfoRefresh:" + PlayerDataManager.Instance.TeamData.takeInfo + "," + PlayerDataManager.Instance.TeamData.takeInfo);
            team_detail detail = team_helper.GetTeamDetail(_currentSelectedId);
            if(PlayerDataManager.Instance.TeamData.takeInfo!=null)
            {
                team_detail takeDetail = team_helper.GetTeamDetail(Convert.ToInt32(PlayerDataManager.Instance.TeamData.takeInfo.game_id));
                _txtContent.CurrentText.text = (6016145).ToLanguage(team_helper.GetDetailInstanceName(takeDetail));
                _txtIWantBrCaptain.ChangeAllStateText((6016059).ToLanguage());
                _txtContent.Visible = PlayerDataManager.Instance.TeamData.takeInfo.game_id != detail.__id;
                m_btnRefresh.Visible = !_txtContent.Visible;
                _btnAutoMatch.Visible = false;
            }
            else
            {
                _txtContent.Visible = false;
                if(detail!=null && detail.__type!=0)
                {
                    _txtIWantBrCaptain.ChangeAllStateText((6016037).ToLanguage());
                    _btnAutoMatch.Visible = true;
                    m_btnRefresh.Visible = true;
                }
                else
                {
                    _btnAutoMatch.Visible = false;
                }
            }
        }

        private void OnMatchInfoRefresh()
        {
            if (PlayerDataManager.Instance.TeamData.matchInfo != null)
            {
                _txtAutoMatch.ChangeAllStateText((6016060).ToLanguage());
                _btnIWantBeCaptain.Visible = false;
                _txtContent.Visible = false;
            }
            else
            {
                team_detail detail = team_helper.GetTeamDetail(_currentSelectedId);
                if (detail != null && detail.__type != 0)
                {
                    _txtAutoMatch.ChangeAllStateText((6016034).ToLanguage());
                    _btnIWantBeCaptain.Visible = true;
                    if (PlayerDataManager.Instance.TeamData.takeInfo!=null)
                    {
                        _txtContent.Visible = PlayerDataManager.Instance.TeamData.takeInfo.game_id != detail.__id;
                    }
                    else
                    {
                        _txtContent.Visible = false;
                    }
                    m_btnRefresh.Visible = !_txtContent.Visible;
                }
                else
                {
                    _btnIWantBeCaptain.Visible = false;
                    _txtContent.Visible = false;
                }
               
            }
        }

        private void OnClickAutoMatch()
        {
            if(PlayerDataManager.Instance.TeamData.matchInfo!=null)
            {
                TeamManager.Instance.CancelAutoMatchTeam();
                return;
            }
            if (dailyTotalTimes > 0 && dailyLeftTimes == 0)
            {
                MogoUtils.FloatTips(1416);
                return;
            }
            TeamManager.Instance.AutoMatchTeam(_currentSelectedId);
        }

        private void OnClickIWantBeCaptain()
        {
            if(PlayerDataManager.Instance.TeamData.takeInfo!=null)
            {
                TeamManager.Instance.CancelTakeTeam();
                TeamManager.Instance.RequestTeamList(_currentSelectedId);
                return;
            }
            if (dailyTotalTimes > 0 && dailyLeftTimes == 0)
            {
                MogoUtils.FloatTips(1416);
                return;
            }
            team_detail detail = team_helper.GetTeamDetail(_currentSelectedId);
            int minLevel = team_helper.GetDetailMinLevel(detail);
            int maxLevel = team_helper.GetDetailMaxLevel(detail);
            
            TeamManager.Instance.TakeTeam(_currentSelectedId, minLevel, maxLevel);
        }

        private void OnClickRefresh()
        {
            TeamManager.Instance.RequestTeamList(_currentSelectedId);
        }

        public void Show(int id)
        {
            Visible = true;
            _currentSelectedId = -1;
            _scrollMenu.SetMenuData(team_helper.GetTeamMenuList());
            if(id!=-1)
            {
                team_detail detail = team_helper.GetTeamDetail(id+1);
                _scrollMenu.SelectItemByValue(detail.__type, detail.__id);
            }
            else
            {
                if(PlayerDataManager.Instance.TeamData.takeInfo!=null)
                {
                    team_detail detail = team_helper.GetTeamDetail(Convert.ToInt32(PlayerDataManager.Instance.TeamData.takeInfo.game_id));
                    _scrollMenu.SelectItemByValue(detail.__type, detail.__id);
                }
                else
                {
                    _scrollMenu.SelectItem(0, 0);
                    _btnIWantBeCaptain.Visible = false;
                    _btnAutoMatch.Visible = false;
                    _txtContent.Visible = false;
                }
                
            }
            
        }

        public void Hide()
        {
            Visible = false;
            _currentSelectedId = -1;
            if(_btnIWantBeCaptain!=null)
            {
                _btnIWantBeCaptain.Visible = false;
                _btnAutoMatch.Visible = false;
                m_btnRefresh.Visible = false;
            }
        }

        public void UpdateContent()
        {
            if(gameObject.activeInHierarchy == false)
            {
               return;
            }
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            _applyTeamList.SetDataList<TeamInfoItem>(teamData.autoTeamInfoList);
            _txtEmpty.Visible = teamData.autoTeamInfoList.Count == 0;
        }

        private void OnClear()
        {
            _applyTeamList.SetDataList<TeamInfoItem>(new List<PbAutoTeamInfo>());
            _txtEmpty.Visible = true;
            _currentSelectedId = -1;
            _btnAutoMatch.Visible = false;
            _btnIWantBeCaptain.Visible = false;
        }

        private int dailyLeftTimes = 10;
        private int dailyTotalTimes = 10;

        private void OnSelectIndexChanged(int id)
        {
            _currentSelectedId = id;
            if(_currentSelectedId == -1)
            {
                _btnIWantBeCaptain.Visible = false;
                _btnAutoMatch.Visible = false;
                _txtContent.Visible = false;
                return;
            }
            m_btnRefresh.Visible = true;
            TeamManager.Instance.RequestTeamList(id);
            team_detail detail = team_helper.GetTeamDetail(_currentSelectedId);
            dailyLeftTimes = 10;
            dailyTotalTimes = 10;
            if(detail.__task != 0)
            {
                _txtLeftCount.Visible = false;
            }
            else
            {
                dailyLeftTimes = instance_helper.GetDailyLeftTimes(detail.__instance_id);
                dailyTotalTimes = instance_helper.GetDailyTimes(detail.__instance_id);
                _txtLeftCount.CurrentText.text = (6016036).ToLanguage(dailyLeftTimes, dailyTotalTimes);
                _txtLeftCount.Visible = dailyTotalTimes>0;
            }
            if (detail.__type == 0)
            {
                _btnAutoMatch.Visible = false;
                _btnIWantBeCaptain.Visible = false;
                _txtContent.Visible = false;
                return;
            }
            if (PlayerDataManager.Instance.TeamData.takeInfo != null)
            {
                team_detail takeDetail = team_helper.GetTeamDetail(Convert.ToInt32(PlayerDataManager.Instance.TeamData.takeInfo.game_id));
                _txtContent.CurrentText.text = (6016145).ToLanguage(team_helper.GetDetailInstanceName(takeDetail));
                _txtIWantBrCaptain.ChangeAllStateText((6016059).ToLanguage());
                _btnAutoMatch.Visible = false;
                _btnIWantBeCaptain.Visible = true;
                _txtContent.Visible = PlayerDataManager.Instance.TeamData.takeInfo.game_id != detail.__id;
                m_btnRefresh.Visible = !_txtContent.Visible;
            }
            else
            {
                _txtIWantBrCaptain.ChangeAllStateText((6016037).ToLanguage());
                if (PlayerDataManager.Instance.TeamData.matchInfo != null)
                {
                    _txtAutoMatch.ChangeAllStateText((6016060).ToLanguage());
                    _btnAutoMatch.Visible = true;
                    _btnIWantBeCaptain.Visible = false;
                }
                else
                {
                    _txtAutoMatch.ChangeAllStateText((6016034).ToLanguage());
                    _btnIWantBeCaptain.Visible = true;
                    _btnAutoMatch.Visible = true;
                }
            }
        }

    }
}
