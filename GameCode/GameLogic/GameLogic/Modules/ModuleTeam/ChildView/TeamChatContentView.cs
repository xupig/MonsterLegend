﻿using System;
using System.Collections.Generic;
using Common.Data;
using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using ModuleChat;
using MogoEngine.Events;
using UnityEngine;

namespace ModuleTeam
{
    public class TeamChatContentView : ChatAllChannelContentView
    {
        protected override void Awake()
        {
            base.Awake();
            _itemType = typeof(TeamChatContentItem);
        }

        protected override KScrollView GetChatScrollView()
        {
            KScrollView scrollView = this.gameObject.GetComponent<KScrollView>();
            return scrollView;
        }

        protected override void AddEventListener()
        {
            EventDispatcher.AddEventListener(TeamEvent.TEAM_RECEIVE_CHAT_CONTENT, OnReceiveTeamChatContent);
        }

        protected override void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_RECEIVE_CHAT_CONTENT, OnReceiveTeamChatContent);
        }

        private void OnReceiveTeamChatContent()
        {
            ReadContent();
        }

        protected override void ReadContent()
        {
            List<PbChatInfoResp> chatInfoList = _chatData.TeamEntryChatData.TeamChatInfoList;
            ShowContent(chatInfoList);
        }

        protected override void RefreshAfterNewInfo()
        {

        }

        protected override void RefreshBeforeNewInfo()
        {

        }
    }

    public class TeamChatContentItem : ChatAllContentItem
    {
        private const float CONTENT_MAX_WIDTH = 732f;

        protected override void Awake()
        {
            _msgPassedTimeComponent = this.gameObject.AddComponent<ChatMsgPassedTimeComponent>();
            AddEventListener();            
        }

        protected override void Refresh()
        {
            AddTextBlock();
            RefreshMsgPassedTimeText();
            RecalculateSize();
            if (_data.msg_type == (int)ChatContentType.Voice)
            {
                ShowLoudSpeaker();
            }
        }

        protected override float GetTextMaxWidth()
        {
            return CONTENT_MAX_WIDTH;
        }

        protected override void SetTextBlockPosition(RectTransform blockRect)
        {
            blockRect.anchoredPosition = new Vector2(15f, -3f);
        }
    }
}
