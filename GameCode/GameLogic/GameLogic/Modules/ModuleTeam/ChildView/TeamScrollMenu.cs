﻿#region 模块信息
/*==========================================
// 文件名：TeamScrollMenu
// 命名空间: GameLogic.GameLogic.Modules.ModuleTeam.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/2/22 14:53:31
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTeam
{
    public class TeamScrollMenu:KContainer
    {
        private KTree _menu;
        private KScrollView _menuView;
        private TeamDetailItem currentDetailMenu;
        private TeamMenuItem _currentMenuItem;
        private List<team_detail> _detailList;
        public KComponentEvent<int> onClick = new KComponentEvent<int>();

        protected override void Awake()
        {
            _menuView = gameObject.GetComponent<KScrollView>();
            _menu = _menuView.GetChildComponent<KTree>("mask/content");
            _menu.SetGap(10,-5);
        }

        protected override void OnEnable()
        {
            AddEventListener();
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            if(_currentMenuItem!=null)
            {
                _currentMenuItem.IsSelected = false;
            }
            _currentMenuItem = null;
            if(currentDetailMenu!=null)
            {
                currentDetailMenu.HideSelectedImage();
            }
            currentDetailMenu = null;

            base.OnDisable();
        }

        private void AddEventListener()
        {
            _menu.onSelectedMenuIndexChanged.AddListener(OpenMenu);
            _menu.onSelectedDetailIndexChanged.AddListener(ChooseSubMenu);
        }

        private void RemoveEventListener()
        {
            _menu.onSelectedMenuIndexChanged.RemoveListener(OpenMenu);
            _menu.onSelectedDetailIndexChanged.RemoveListener(ChooseSubMenu);
        }

        public void SetMenuData(List<team_menu> menuList)
        {
            menuList.Sort(SortMenuOrder);
            _menu.SetMenuList<TeamMenuItem>(menuList,0,true);
        }

        private int SortMenuOrder(team_menu menu1,team_menu menu2)
        {
            List<TeamLimitData> effectList1 = team_helper.GetEffectList(menu1.__id);
            List<TeamLimitData> effectList2 = team_helper.GetEffectList(menu2.__id);
            bool hasEffectItem1 = hasEffectItem(effectList1);
            bool hasEffectItem2 = hasEffectItem(effectList2);
            if(hasEffectItem1 == hasEffectItem2)
            {
                return menu1.__rank - menu2.__rank;
            }
            if(hasEffectItem1)
            {
                return -1;
            }
            return 1;
        }

        private bool hasEffectItem( List<TeamLimitData> effectList)
        {
            if(effectList == null || effectList.Count == 0)
            {
                return false;
            }
            for(int i=0;i<effectList.Count;i++)
            {
                if(IsOpen(effectList[i]))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsOpen(TeamLimitData data)
        {
            if(data == null)
            {
                return false;
            }
            if(data.type == 0)
            {
                return true;
            }
            if(PlayerAvatar.Player.level < data.minLevel)
            {
                return false;
            }
            if (data.startDate > (long)(Global.serverTimeStamp / 1000) || data.endDate < (long)(Global.serverTimeStamp / 1000))//日期判断
            {
                return false;
            }
            DateTime dateTime = MogoTimeUtil.ServerTimeStamp2UtcTime((long)(Global.serverTimeStamp / 1000));
            if (data.openWeekOfDay.Count > 0 && data.openWeekOfDay.Contains((int)dateTime.DayOfWeek) == false)//星期判断
            {
                return false;
            }

            int currentSecond = PlayerTimerManager.GetInstance().GetCurrentSecond();
            if (data.openTimeStart.Count > 0)
            {
                for (int i = 0; i < data.openTimeStart.Count; i++)
                {
                    if (currentSecond >= data.openTimeStart[i] && currentSecond <= data.openTimeEnd[i])//时间判断
                    {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        public void SelectItem(int menuIndex,int detailIndex)
        {
            if( _menu.ItemList!=null&& _menu.ItemList.Count>0)
            {
                SelectMenu(_menu.ItemList[menuIndex]);
            }
            _menu.SetSelectItem(menuIndex, detailIndex);
        }

        public void SelectItemByValue(int menuValue,int detailValue)
        {
            if(_menu.ItemList!=null)
            {
                for(int i=0;i<_menu.ItemList.Count;i++)
                {
                    if(_menu.ItemList[i].ID == menuValue)
                    {
                        SelectMenu(_menu.ItemList[i]);
                        _menu.SetSelectItemByValue(i, detailValue);
                    }
                }
            }
        }

        private void SelectMenu(KTreeMenuItem menuItem)
        {
            if (_currentMenuItem != null && _currentMenuItem != menuItem && _menu.IsHidden(_currentMenuItem.Index) == false)
            {
                _currentMenuItem.IsSelected = false;
                _menu.SetDetailList<TeamDetailItem>(_currentMenuItem.Index, null);
            }
            TeamMenuItem item = menuItem as TeamMenuItem;
            _currentMenuItem = item;
            team_menu menuData = item.Data as team_menu;
            _detailList = team_helper.GetTeamDetailList(menuData.__id);
            _detailList.Sort(DetailSortFunc);
            _menu.SetDetailList<TeamDetailItem>(item.Index, _detailList);
           
        }


        private void OpenMenu(KTree tree, KTreeMenuItem menuItem)
        {
            if(_currentMenuItem!=null && _currentMenuItem!=menuItem && _menu.IsHidden(_currentMenuItem.Index) == false)
            {
                _currentMenuItem.IsSelected = false;
                _menu.SetDetailList<TeamDetailItem>(_currentMenuItem.Index, null);
            }
            TeamMenuItem item = menuItem as TeamMenuItem;
            _currentMenuItem = item;
            team_menu menuData = item.Data as team_menu;
            _detailList = team_helper.GetTeamDetailList(menuData.__id);
            _detailList.Sort(DetailSortFunc);
            _menu.SetDetailList<TeamDetailItem>(item.Index, _detailList);
            _menu.SetSelectItem(item.Index, 0);
        }

        private void ChooseSubMenu(KTree tree, KTreeDetailItem detailItem)
        {
            if (currentDetailMenu != null)
            {
                currentDetailMenu.HideSelectedImage();
            }
            onClick.Invoke((detailItem.Data as team_detail).__id);
            currentDetailMenu = detailItem as TeamDetailItem;
            currentDetailMenu.SetSelectedImage();
        }

        private int DetailSortFunc(team_detail d1,team_detail d2)
        {
            TeamLimitData item1 = team_helper.GetDetailEffectData(d1.__id);
            TeamLimitData item2 = team_helper.GetDetailEffectData(d2.__id);
            bool hasEffectItem1 = IsOpen(item1);
            bool hasEffectItem2 = IsOpen(item2);
            if (hasEffectItem1 == hasEffectItem2)
            {
                int minLevel1 = team_helper.GetDetailMinLevel(d1);
                int minLevel2 = team_helper.GetDetailMinLevel(d2);
                if(minLevel1 == minLevel2)
                {
                    return d2.__instance_id - d1.__instance_id;
                }
                if (minLevel1 > PlayerAvatar.Player.level)
                {
                    if (minLevel2 > PlayerAvatar.Player.level)
                    {
                        return minLevel1 - minLevel2;
                    }
                    else
                    {
                        return 1;
                    }
                }
                else
                {
                    if (minLevel2 > PlayerAvatar.Player.level)
                    {
                        return -1;
                    }
                    return minLevel2 - minLevel1;
                }
            }
            if (hasEffectItem1)
            {
                return -1;
            }
            return 1;
        }

    }
}
