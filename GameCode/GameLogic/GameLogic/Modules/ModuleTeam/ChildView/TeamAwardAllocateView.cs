﻿#region 模块信息
/*==========================================
// 文件名：TeamAwardAllocateView
// 命名空间: GameLogic.GameLogic.Modules.ModuleTeam.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/2/23 11:24:15
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTeam
{
    public class TeamAwardAllocateView:KContainer
    {

        private KScrollView _scrollView;
        private KPageableList _pageableList;

        protected override void Awake()
        {
            _scrollView = GetChildComponent<KScrollView>("ScrollView_xinxi");
            _scrollView.Arrow.gameObject.AddComponent<RaycastComponent>().RayCast = false;
            _pageableList = _scrollView.GetChildComponent<KPageableList>("mask/content");
            _pageableList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            base.Awake();
        }

        protected override void OnEnable()
        {
            EventDispatcher.AddEventListener(TeamEvent.TRANSFER_INFO_CHANGE, OnTransferInfoChange);
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            EventDispatcher.RemoveEventListener(TeamEvent.TRANSFER_INFO_CHANGE, OnTransferInfoChange);
            base.OnDisable();
        }

        public void Show()
        {
            Visible = true;
            MissionManager.Instance.RequestAllTransferInfo();
            OnTransferInfoChange();
        }

        public void Hide()
        {
            Visible = false;
        }

        private void OnTransferInfoChange()
        {
            _pageableList.SetDataList<TeamAwardAllocateItem>(PlayerDataManager.Instance.TeamData.groupRecordList);
        }

    }
}
