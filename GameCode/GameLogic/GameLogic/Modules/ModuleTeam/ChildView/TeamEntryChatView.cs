﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using ModuleChat;
using Common.ServerConfig;
using MogoEngine.Events;
using Common.Events;

namespace ModuleTeam
{
    public class TeamEntryChatView : KContainer
    {
        private TeamChatContentView _chatContentView;
        private ChatInputView _inputView;
        private TeamChatConvenientView _convenientMenuView;
        private ChatVoiceAgent _chatVoiceAgent;

        protected override void Awake()
        {
            _chatContentView = AddChildComponent<TeamChatContentView>("ScrollView_chatView");
            _chatContentView.Channel = public_config.CHANNEL_ID_TEAM;
            _inputView = AddChildComponent<ChatInputView>("Container_Shuruku");
            _convenientMenuView = AddChildComponent<TeamChatConvenientView>("Container_commonUseMenu");
            InitVoiceComponents();
            KContainer maskContainer = GetChildComponent<KContainer>("ScaleImage_mask");
            maskContainer.Visible = false;
        }

        private void InitVoiceComponents()
        {
            _chatVoiceAgent = gameObject.AddComponent<ChatVoiceAgent>();
            ChatVoiceNoticeView voiceNoticeView = AddChildComponent<ChatVoiceNoticeView>("Container_yuyintishi");
            List<ChatVoiceInputView> voiceInputList = new List<ChatVoiceInputView>();
            voiceInputList.Add(_inputView.VoiceInputView);
            _chatVoiceAgent.InitView(voiceNoticeView, voiceInputList);
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(ChatEvents.OPEN_EMOJI, OnOpenEmoji);
            EventDispatcher.AddEventListener(ChatEvents.CLOSE_EMOJI, OnCloseEmoji);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(ChatEvents.OPEN_EMOJI, OnOpenEmoji);
            EventDispatcher.RemoveEventListener(ChatEvents.CLOSE_EMOJI, OnCloseEmoji);
        }

        private void OnOpenEmoji()
        {
            if (!_convenientMenuView.Visible)
            {
                _convenientMenuView.Visible = true;
                _convenientMenuView.Refresh();
            }
            else
            {
                ChatPopupViewFadeIn.BeginHide(_convenientMenuView.gameObject);
            }
        }

        private void OnCloseEmoji()
        {
            if (_convenientMenuView.Visible == true)
            {
                _convenientMenuView.Visible = false;
            }
        }

        public void Refresh()
        {
            _inputView.Refresh(_chatContentView.Channel);
        }

        public void ShowConvenientView()
        {
            if (!_convenientMenuView.Visible)
            {
                _convenientMenuView.Visible = true;
            }
        }

        public void HideConvenientView()
        {
            if (_convenientMenuView.Visible)
            {
                _convenientMenuView.Visible = false;
            }
        }
    }
}
