﻿using System;
using System.Collections.Generic;
using ModuleChat;
using Game.UI.UIComponent;

namespace ModuleTeam
{
    public class TeamChatMultifunctionView : ChatMultifunctionMenuView
    {
        protected override void Awake()
        {
            base.Awake();
            _openEditPhraseBtn.Visible = false;
            KContainer showLinkContainer = GetChildComponent<KContainer>("Container_lianjie");
            showLinkContainer.Visible = false;
        }

        public override void Refresh()
        {
            ShowPhraseContent();
        }
    }
}
