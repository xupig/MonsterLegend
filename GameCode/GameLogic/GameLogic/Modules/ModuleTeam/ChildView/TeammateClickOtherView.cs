﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2015/1/4 16:27:45
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTeam.ChildView
{
    public class TeammateClickOtherView:KContainer
    {
        #region 私有变量

        private KButton m_btnAddFriend;
        private KButton m_btnTransmit;
        private KButton m_btnChat;
        private KButton _btnFollow;
        private StateText _txtFollow;

        #endregion

        public KComponentEvent<TeamOperation> onTeamOperation = new KComponentEvent<TeamOperation>();

        #region 基础接口实现

        protected override void Awake()
        {
            KContainer _container = this.gameObject.GetComponent<KContainer>();
            m_btnAddFriend = _container.GetChildComponent<KButton>("Button_jiaweihaoyou");
            m_btnTransmit = _container.GetChildComponent<KButton>("Button_chuansong");
            m_btnChat = _container.GetChildComponent<KButton>("Button_liaotian");
            _btnFollow = GetChildComponent<KButton>("Button_gensuizhandou");
            _txtFollow = _btnFollow.GetChildComponent<StateText>("label");
            AddEventListener();
        }

        protected void AddEventListener()
        {
            m_btnAddFriend.onClick.AddListener(OnClickAddFriend);
            m_btnTransmit.onClick.AddListener(OnClickTransmit);
            m_btnChat.onClick.AddListener(OnClickChat);
            _btnFollow.onClick.AddListener(OnFollowBtnCLick);
        }

        protected override void OnDestroy()
        {
            m_btnAddFriend.onClick.RemoveListener(OnClickAddFriend);
            m_btnTransmit.onClick.RemoveListener(OnClickTransmit);
            m_btnChat.onClick.RemoveListener(OnClickChat);
            _btnFollow.onClick.RemoveListener(OnFollowBtnCLick);
            base.OnDestroy();
        }

        public void SetTeammateInfo(ulong dbId)
        {
            _btnFollow.Visible = PlayerDataManager.Instance.TeamData.CaptainId == dbId;
            FightType fightType = PlayerAutoFightManager.GetInstance().fightType;
            if (fightType != FightType.FOLLOW_FIGHT)
            {
                _txtFollow.ChangeAllStateText((6314002).ToLanguage());//跟随战斗
            }
            else
            {
                _txtFollow.ChangeAllStateText((6314003).ToLanguage());//跟随战斗
            }
        }

        #endregion

        #region UI回调事件处理

        private void OnClickAddFriend()
        {
            onTeamOperation.Invoke(TeamOperation.ADD_FRIEND);
        }

        private void OnClickTransmit()
        {
            onTeamOperation.Invoke(TeamOperation.TRANSMIT);
        }

        private void OnClickChat()
        {
            onTeamOperation.Invoke(TeamOperation.CHAT);
        }

        private void OnFollowBtnCLick()
        {
            FightType fightType = PlayerAutoFightManager.GetInstance().fightType;
            if (fightType != FightType.FOLLOW_FIGHT)
            {
                if (PlayerDataManager.Instance.TaskData.ring > 0)
                {
                    MessageBox.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(115029));
                    return;
                }
                TeamManager.Instance.FollowCaptain();
            }
            else
            {
                PlayerAutoFightManager.GetInstance().Stop();
                TeamManager.Instance.CancelFollowCaptain();
            }
            
        }

        #endregion

    }
}
