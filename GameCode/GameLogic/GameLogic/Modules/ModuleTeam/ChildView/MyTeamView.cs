﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/29 11:25:43
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Events;
using Common.Global;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using ModuleTeam.ChildComponent;
using Mogo.Util;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTeam.ChildView
{
    public class MyTeamView:KContainer
    {

        private CaptainClickMySelfView m_captainClickMySelfPanel;
        private CaptainClickOtherView m_captainClickOtherPanel;
        private TeammateClickMySelfView m_teammateClickMySelfPanel;
        private TeammateClickOtherView m_teammateClickOtherPanel;

        private List<TeamPlayerInfoItem> m_playerInfoItemList;
        private TeamPlayerInfoItem _selectedItem;

        protected override void Awake()
        {
            m_captainClickMySelfPanel = KComponentUtil.AddChildComponent<CaptainClickMySelfView>(this.gameObject, "Container_dzdjzj");
            m_captainClickOtherPanel = KComponentUtil.AddChildComponent<CaptainClickOtherView>(this.gameObject, "Container_dzdjdy");
            m_teammateClickMySelfPanel = KComponentUtil.AddChildComponent<TeammateClickMySelfView>(this.gameObject, "Container_dydjzj");
            m_teammateClickOtherPanel = KComponentUtil.AddChildComponent<TeammateClickOtherView>(this.gameObject, "Container_dydjdy");

            m_playerInfoItemList = new List<TeamPlayerInfoItem>();
            for(int i = 1; i < 5; i++)
            {
                TeamPlayerInfoItem item = AddChildComponent<TeamPlayerInfoItem>("Container_duiyuan/Container_daunyuan0" + i);
                item.onClick.AddListener(OnSelectIndexChange);
                m_playerInfoItemList.Add(item);
            }
            ClearSelectdIndex();
        }

        public void Show()
        {
            Visible = true;
            UpdateContent();
        }

        public void Hide()
        {
            Visible = false;
        }

        protected override void OnEnable()
        {
            m_captainClickOtherPanel.onTeamOperation.AddListener(OnTeamOperationClick);
            m_teammateClickOtherPanel.onTeamOperation.AddListener(OnTeamOperationClick);
            EventDispatcher.AddEventListener(TeamEvent.CAPTAIN_MATCH_INFO_REFRESH, OnCaptainMatchInfoRefresh);
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            m_captainClickOtherPanel.onTeamOperation.RemoveListener(OnTeamOperationClick);
            m_teammateClickOtherPanel.onTeamOperation.RemoveListener(OnTeamOperationClick);
            EventDispatcher.RemoveEventListener(TeamEvent.CAPTAIN_MATCH_INFO_REFRESH, OnCaptainMatchInfoRefresh);
            base.OnDisable();
        }

        private void OnCaptainMatchInfoRefresh()
        {
            UpdateContent();
        }

        public void ClearSelectdIndex()
        {
            if (_selectedItem != null)
            {
                _selectedItem.SetSelected(false);
                _selectedItem = null;
            }
            m_captainClickMySelfPanel.gameObject.SetActive(false);
            m_captainClickOtherPanel.gameObject.SetActive(false);
            m_teammateClickMySelfPanel.gameObject.SetActive(false);
            m_teammateClickOtherPanel.gameObject.SetActive(false);
        }

       
        public void UpdateContent()
        {
            if (m_captainClickMySelfPanel == null)
            {
                return;
            }
            ClearSelectdIndex();
            int index = 0;
            if (PlayerDataManager.Instance.TeamData.TeammateDic.Count>0)
            {
                m_playerInfoItemList[0].gameObject.SetActive(true);
                PbTeamMember captain = PlayerDataManager.Instance.TeamData.TeammateDic[PlayerDataManager.Instance.TeamData.CaptainId];
                m_playerInfoItemList[0].SetInfo(captain);
                if (captain.dbid == PlayerAvatar.Player.dbid)
                {
                    OnSelectIndexChange(m_playerInfoItemList[0]);
                }
                index++;
            }
            foreach (KeyValuePair<UInt64, PbTeamMember> kvp in PlayerDataManager.Instance.TeamData.TeammateDic)
            {
                if (kvp.Key != PlayerDataManager.Instance.TeamData.CaptainId)
                {
                    m_playerInfoItemList[index].gameObject.SetActive(true);
                    m_playerInfoItemList[index].SetInfo(kvp.Value);
                    if (kvp.Key == PlayerAvatar.Player.dbid)
                    {
                        OnSelectIndexChange(m_playerInfoItemList[index]);
                    }
                    index++;
                }
            }
            if (index < 4 && PlayerDataManager.Instance.TeamData.CaptainId == PlayerAvatar.Player.dbid)
            {
                m_playerInfoItemList[index].gameObject.SetActive(true);
                m_playerInfoItemList[index].SetInfo(null);
                index++;
            }
            for(int i = index;i<4;i++)
            {
                m_playerInfoItemList[i].gameObject.SetActive(false);
            }
        }

        private void OnSelectIndexChange(TeamPlayerInfoItem item)
        {
            ClearSelectdIndex();
            _selectedItem = item;
            _selectedItem.SetSelected(true);
            //m_playerInfoItemList[selectIndex].SetSelect(true);
            if (item.Data == null)
            {
                return;
            }
            if(PlayerDataManager.Instance.TeamData.CaptainId == MogoWorld.Player.dbid)//自己是队长
            {
                if (item.Data.dbid == MogoWorld.Player.dbid)//选中的是自己
                {
                    m_captainClickMySelfPanel.gameObject.SetActive(true);
                }
                else
                {
                    m_captainClickOtherPanel.gameObject.SetActive(true);
                }
            }
            else
            {
                if (item.Data.dbid == MogoWorld.Player.dbid)//选中的是自己
                {
                    m_teammateClickMySelfPanel.gameObject.SetActive(true);
                }
                else
                {
                    m_teammateClickOtherPanel.gameObject.SetActive(true);
                    m_teammateClickOtherPanel.SetTeammateInfo(item.Data.dbid);
                }
            }
        }

        private void OnTeamOperationClick(TeamOperation operation)
        {
            switch(operation)
            {
                case TeamOperation.ADD_FRIEND:
                    OnAddFriend();
                    break;
                case TeamOperation.CALL:
                    OnCall();
                    break;
                case TeamOperation.CHANGE_CAPTAIN:
                    OnChangeCaptain();
                    break;
                case TeamOperation.CHAT:
                    OnChat();
                    break;
                case TeamOperation.KICK_TEAMMATE:
                    OnKickTeammate();
                    break;
                case TeamOperation.TRANSMIT:
                    OnTransmit();
                    break;
                default:
                    break;
            }
        }
        public void OnKickTeammate()
        {
            string content = MogoLanguageUtil.GetString(LangEnum.TEAM_KICK, _selectedItem.Data.name);
            MessageBox.Show(false, MessageBox.DEFAULT_TITLE, content, Kick);
        }

        private void Kick()
        {
            TeamManager.Instance.KickTeammate(_selectedItem.Data.dbid);
        }

        public void OnAddFriend()
        {
            FriendManager.Instance.AddFriend(GetFriendAvatarInfo());
        }

        public void OnChangeCaptain()
        {
            string content = MogoLanguageUtil.GetString(LangEnum.TEAM_CHANGE_CAPTAIN, _selectedItem.Data.name);
            MessageBox.Show(false, MessageBox.DEFAULT_TITLE, content, ChangeCaptain);
        }

        private void ChangeCaptain()
        {
            TeamManager.Instance.ChangeCaptain(_selectedItem.Data.dbid);
        }

        public void OnCall()
        {
            //string name = _selectedItem.Data.name;
            MessageBox.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.TEAM_CALL_ONE, _selectedItem.Data.name), OnCallConfirm);
        }

        private void OnCallConfirm()
        {

            TeamManager.Instance.TeamCallOne(_selectedItem.Data.dbid);
        }


        private PbFriendAvatarInfo GetFriendAvatarInfo()
        {
            PbTeamMember data = _selectedItem.Data;
            PbFriendAvatarInfo avatarInfo = new PbFriendAvatarInfo();
            avatarInfo.dbid = data.dbid;
            avatarInfo.name = data.name;
            avatarInfo.name_bytes = data.name_bytes;
            avatarInfo.level = data.level;
            avatarInfo.vocation = data.vocation;
            avatarInfo.gender = data.gender;
            return avatarInfo;
        }
        public void OnChat()
        {
            PanelIdEnum.Chat.Show(GetFriendAvatarInfo());
        }

        public void OnTransmit()
        {
            string content = MogoLanguageUtil.GetString(LangEnum.TEAM_TRANSMIT, _selectedItem.Data.name); 
            MessageBox.Show(false, MessageBox.DEFAULT_TITLE, content, Transmit);
        }

        private void Transmit()
        {
            TeamManager.Instance.TeamTransmit(_selectedItem.Data.dbid);
        }

    }
}
