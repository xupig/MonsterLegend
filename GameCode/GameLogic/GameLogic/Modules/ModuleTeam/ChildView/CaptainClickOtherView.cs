﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2015/1/4 16:02:56
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTeam.ChildView
{
    public enum TeamOperation
    {
        KICK_TEAMMATE = 1,
        ADD_FRIEND = 2,
        CHANGE_CAPTAIN = 3,
        TRANSMIT = 4,
        CALL = 5,
        CHAT = 6,
    }

    public class CaptainClickOtherView:KContainer
    {
        #region 私有变量
        private KButton m_btnKickOut;
        private KButton m_btnAddFriend;
        private KButton m_btnAppointCaptain;
        private KButton m_btnTransmit;
        private KButton m_btnCall;
        private KButton m_btnChat;
        #endregion

        public KComponentEvent<TeamOperation> onTeamOperation = new KComponentEvent<TeamOperation>();

        #region 基础接口实现

        protected override void Awake()
        {
            m_btnKickOut = GetChildComponent<KButton>("Button_qingliduiwu");
            m_btnAddFriend = GetChildComponent<KButton>("Button_jiaweihaoyou");
            m_btnAppointCaptain = GetChildComponent<KButton>("Button_renmingduizhang");
            m_btnTransmit = GetChildComponent<KButton>("Button_chuansong");
            m_btnCall = GetChildComponent<KButton>("Button_zhaohuan");
            m_btnChat = GetChildComponent<KButton>("Button_liaotian");
            AddEventListener();
        }

        protected void AddEventListener()
        {
            m_btnKickOut.onClick.AddListener(OnClickKickOut);
            m_btnAddFriend.onClick.AddListener(OnClickAddFriend);
            m_btnAppointCaptain.onClick.AddListener(OnClickAppointCaptain);
            m_btnTransmit.onClick.AddListener(OnClickTransmit);
            m_btnCall.onClick.AddListener(OnClickCall);
            m_btnChat.onClick.AddListener(OnClickChat);
        }

        protected override void OnDestroy()
        {
            m_btnKickOut.onClick.RemoveListener(OnClickKickOut);
            m_btnAddFriend.onClick.RemoveListener(OnClickAddFriend);
            m_btnAppointCaptain.onClick.RemoveListener(OnClickAppointCaptain);
            m_btnTransmit.onClick.RemoveListener(OnClickTransmit);
            m_btnCall.onClick.RemoveListener(OnClickCall);
            m_btnChat.onClick.RemoveListener(OnClickChat);
            base.OnDestroy();
        }

        #endregion

        #region UI回调事件处理
        
        private void OnClickKickOut()
        {
            onTeamOperation.Invoke(TeamOperation.KICK_TEAMMATE);
        }

        private void OnClickAddFriend()
        {
            onTeamOperation.Invoke(TeamOperation.ADD_FRIEND);
        }

        private void OnClickAppointCaptain()
        {
            onTeamOperation.Invoke(TeamOperation.CHANGE_CAPTAIN);
        }

        private void OnClickTransmit()
        {
            onTeamOperation.Invoke(TeamOperation.TRANSMIT);
        }

        private void OnClickCall()
        {
            onTeamOperation.Invoke(TeamOperation.CALL);
        }

        private void OnClickChat()
        {
            onTeamOperation.Invoke(TeamOperation.CHAT);
        }
        
        #endregion
    }
}
