﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/29 11:37:04
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using ModuleTeam.ChildComponent;
using MogoEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTeam.ChildView
{
    public class ApplyListView:KContainer
    {
        private KScrollView m_scrollView;
        private KPageableList m_list;
        private List<TeamReceivedData> m_dataList = new List<TeamReceivedData>();
        private StateText _txtEmpty;

        static readonly int[] s_Padding = new int[] { 0, 2, 0, 2 };
        static readonly int[] s_Gap = new int[] { 6, 0 };

        private int _instanceId;
        private int _currentItemCount = 6;
        private const int PER_PAGE_COUNT = 6;

        protected override void Awake()
        {
            m_scrollView = GetChildComponent<KScrollView>("ScrollView_applyScrollView");
            m_list = m_scrollView.GetChildComponent<KPageableList>("mask/content");
            _txtEmpty = GetChildComponent<StateText>("Label_txtContent");
            InitScrollView();
        }

        private void InitScrollView()
        {
            m_list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            m_list.SetGap(s_Gap[0], s_Gap[1]);
            m_list.SetPadding(s_Padding[0], s_Padding[1], s_Padding[2], s_Padding[3]);
        }

       // private bool _isWaitForAwake = false;

        public void Show(int instanceId)
        {
            Visible = true;
            _instanceId = instanceId;
            AddEventListener();
            UpdateContent();

        }

        public void Hide()
        {
            RemoveEventListener();
            Visible = false;
        }

        public void UpdateContent()
        {
            if(Visible == false)
            {
                return;
            }
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            m_dataList.Clear();
            int langId = 0;
            if(teamData.CaptainId == 0)   //没有队伍
            {
                foreach(TeamReceivedData data in teamData.InvitationList)
                {
                    m_dataList.Add(data);
                }
                langId = 10101;
            }
            else if (teamData.CaptainId == MogoWorld.Player.dbid)  //自己是队长
            {
                foreach (TeamReceivedData data in teamData.CaptainList)
                {
                    m_dataList.Add(data);
                }
                langId = 10102;
            }
            _txtEmpty.CurrentText.text = langId.ToLanguage();
            m_list.RemoveAll();
           
            _txtEmpty.Visible = m_dataList.Count == 0;

            int applyListCount = m_dataList.Count;
            int showApplyListCount = _currentItemCount > applyListCount ? applyListCount : _currentItemCount;
            List<TeamReceivedData> showApplyList = m_dataList.GetRange(0, showApplyListCount);
            m_list.SetDataList<TeamMessageItem>(showApplyList);
            SetInstaneId();
        }

        private void SetInstaneId()
        {
            for (int i = 0; i < m_list.ItemList.Count; i++)
            {
                TeamMessageItem item = m_list.ItemList[i] as TeamMessageItem;
                item.InstanceId = _instanceId;
            }
        }

        private void AddEventListener()
        {
            m_scrollView.ScrollRect.onRequestNext.AddListener(ShowNextPage);
        }

        private void RemoveEventListener()
        {
            if (m_scrollView != null)
            {
                m_scrollView.ScrollRect.onRequestNext.RemoveListener(ShowNextPage);
            }
        }

        private void ShowNextPage(KScrollRect scrollRect)
        {
            _currentItemCount += PER_PAGE_COUNT;
            UpdateContent();
        }

    }
}
