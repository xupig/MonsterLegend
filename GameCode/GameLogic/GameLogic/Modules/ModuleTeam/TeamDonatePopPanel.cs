﻿#region 模块信息
/*==========================================
// 文件名：TeamDonatePopPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleTeam
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/2/23 14:18:14
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using Mogo.Util;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTeam
{
    public class TeamDonatePopPanel:BasePanel
    {
        private KScrollView _scrollView;
        private KPageableList _list;
        private KButton _btnGiveHim;
        private TeamDonateItem _selectedItem;
        private ulong _dbId;
        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_zengsongwupin");
            _list = _scrollView.GetChildComponent<KPageableList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, 3, 1);
            _list.SetGap(28, 54);
            _btnGiveHim = GetChildComponent<KButton>("Button_hecheng");
            _btnGiveHim.GetChildComponent<StateText>("label").ChangeAllStateText((6016149).ToLanguage());
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TeamDonate; }
        }

        public override void OnShow(object data)
        {
            PanelIdEnum.ItemToolTips.Close();
            PanelIdEnum.EquipToolTips.Close();
            _btnGiveHim.onClick.AddListener(OnClickGiveHim);
            _list.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            SetData(data);
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            object[] dataList = data as object[];
            _dbId = (ulong)dataList[0];
            List<BaseItemInfo> itemList = dataList[1] as List<BaseItemInfo>;
            _list.SetDataList<TeamDonateItem>(itemList);
        }

        public override void OnClose()
        {
            if (_selectedItem != null)
            {
                _selectedItem.IsSelected = false;
            }
            _selectedItem = null;
            _btnGiveHim.onClick.RemoveListener(OnClickGiveHim);
            _list.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
        }

        private void OnSelectedIndexChanged(KList list,int index)
        {
            if(_selectedItem != null)
            {
                _selectedItem.IsSelected = false;
            }
            for(int i=0;i<list.ItemList.Count;i++)
            {
                if(list.ItemList[i].IsSelected)
                {
                    _selectedItem = list.ItemList[i] as TeamDonateItem;
                }
            }
        }

        private void OnClickGiveHim()
        {
            if (_selectedItem == null)
            {
                for (int i = 0; i < _list.ItemList.Count; i++)
                {
                    if (_list.ItemList[i].IsSelected)
                    {
                        _selectedItem = _list.ItemList[i] as TeamDonateItem;
                    }
                }
            }
            if(_selectedItem == null)
            {
                MogoUtils.FloatTips(6016146);
                return;
            }
            TeamInstanceManager.Instance.TransferItemToPlayer(_dbId, (_selectedItem.Data as BaseItemInfo).GridPosition);
            ID.Close();
        }

    }
}
