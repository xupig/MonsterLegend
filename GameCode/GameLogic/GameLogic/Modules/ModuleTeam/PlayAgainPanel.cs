﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTeam
{
    public class PlayAgainPanel : TeamEntryBasePanel
    {
        private PlayAgainData _data
        {
            get
            {
                return PlayerDataManager.Instance.PlayAgainData;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            RectTransform bgTransform = GetChildComponent<RectTransform>("Container_Bg/Container_playagain");
            for (int i = 0; i < bgTransform.childCount; i++)
            {
                bgTransform.GetChild(i).gameObject.SetActive(true);
            }
            _entryChatView.Visible = false;
            _btnClose.Visible = false;

            AddAwakeListener();
        }

        protected override void OnDestroy()
        {
            RemoveAwakeListener();
        }

        private void AddAwakeListener()
        {
            _btnCancel.onClick.AddListener(OnClickCancel);
        }

        private void RemoveAwakeListener()
        {
            _btnCancel.onClick.RemoveListener(OnClickCancel);
        }

        protected override void OnClickAgree()
        {
            MissionManager.Instance.RequestAgreePlayAgain(true);
        }

        protected override void OnClickReject()
        {
            MissionManager.Instance.RequestAgreePlayAgain(false);
        }

        private void OnClickCancel()
        {
            MissionManager.Instance.RequestAgreePlayAgain(false);
        }

        protected override PanelIdEnum Id
        {
            get
            {
                return PanelIdEnum.PlayAgain;
            }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            Refresh();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(CopyEvents.PLAY_AGAIN_AGREE, Refresh);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(CopyEvents.PLAY_AGAIN_AGREE, Refresh);
        }

        public override void OnClose()
        {
            RemoveEventListener();
            base.OnClose();
        }

        private void Refresh()
        {
            RefreshTitle();
            SetEndTime(_data.EndTime);
            RefreshList();
            RefreshButtonVisibility();
            AutoAgreeWhenFollowCaptain();
        }

        private void RefreshTitle()
        {
            _txtTitle.CurrentText.text = MogoLanguageUtil.GetContent(10117);
        }

        private void RefreshList()
        {
            List<TeamEntryPlayerItemData> dataList = _data.DataList;
            for (int i = 0; i < itemList.Count; i++)
            {
                TeamEntryPlayerItem item = itemList[i];
                if (i < dataList.Count)
                {
                    item.Visible = true;
                    item.SetData(dataList[i]);
                    if (dataList[i].IsAgree)
                    {
                        item.SetAgree();
                    }
                }
                else
                {
                    itemList[i].Visible = false;
                }
            }
        }

        private void RefreshButtonVisibility()
        {
            bool Agree = false;
            List<TeamEntryPlayerItemData> dataList = _data.DataList;
            for (int i = 0; i < dataList.Count; i++)
            {
                if (dataList[i].Dbid == PlayerAvatar.Player.dbid && dataList[i].IsAgree)
                {
                    Agree = true;
                    break;
                }
            }
            if (Agree)
            {
                _btnCancel.Visible = true;
                _btnAgree.Visible = false;
                _btnRefuse.Visible = false;
            }
            else
            {
                _btnCancel.Visible = false;
                _btnAgree.Visible = true;
                _btnRefuse.Visible = true;
            }
        }

        private void AgreePlayAgain(PbPlayAgainNotify pb)
        {
            List<TeamEntryPlayerItemData> dataList = PlayerDataManager.Instance.PlayAgainData.DataList;
            for (int i = 0; i < dataList.Count; i++)
            {
                if (dataList[i].EntityId == pb.ent_id)
                {
                    itemList[i].SetAgree();
                    if (pb.ent_id == PlayerAvatar.Player.id)
                    {
                        _btnAgree.Visible = false;
                        _btnRefuse.Visible = false;
                        _btnCancel.Visible = true;
                    }
                    break;
                }
            }
        }

        protected override void TimerEnd()
        {
            base.TimerEnd();
            GameSceneManager.GetInstance().LeaveCombatScene();
        }
    }
}
