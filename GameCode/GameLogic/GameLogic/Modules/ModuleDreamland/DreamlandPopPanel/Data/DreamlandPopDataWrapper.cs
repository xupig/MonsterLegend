﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/15 15:15:28
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Structs.ProtoBuf;

namespace ModuleDreamland
{
    public class DreamlandPopDataWrapper
    {
        public DreamlandPopType PopType;
        public DreamlandInfo DreamlandInfo;
        public PbDreamlandEventList RecordList;
        public PbDreamlandBoxInfoList RetrieveRewardList;

        public DreamlandPopDataWrapper(DreamlandPopType popType, DreamlandInfo dreamlandInfo)
        {
            this.PopType = popType;
            this.DreamlandInfo = dreamlandInfo;
        }

        public DreamlandPopDataWrapper(DreamlandPopType popType, DreamlandInfo dreamlandInfo, PbDreamlandEventList recordList):this(popType, dreamlandInfo)
        {
            this.RecordList = recordList;
        }

        public DreamlandPopDataWrapper(DreamlandPopType popType, PbDreamlandBoxInfoList retrieveRewardList)
        {
            this.PopType = popType;
            this.RetrieveRewardList = retrieveRewardList;
        }
    }
}
