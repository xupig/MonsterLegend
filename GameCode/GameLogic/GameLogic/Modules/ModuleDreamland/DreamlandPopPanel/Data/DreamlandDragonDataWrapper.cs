﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/15 18:49:44
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleDreamland
{
    public class DreamlandDragonDataWrapper
    {
        public int LandId;
        public int Quality;
        public int CurrentSelectDragonQuality;
        public bool IsQualityUp;

        public DreamlandDragonDataWrapper(int landId, int quality, int currentSelectDragonQuality, bool isQualityUp)
        {
            this.LandId = landId;
            this.Quality = quality;
            this.CurrentSelectDragonQuality = currentSelectDragonQuality;
            this.IsQualityUp = isQualityUp;
        }
    }
}
