﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/20 18:40:24
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameData;

namespace ModuleDreamland
{
    public class DreamlandRetrieveRewardDataWrapper
    {
        public int LandId;
        public dreamland_award_retrieve AwardRetrieve;

        public DreamlandRetrieveRewardDataWrapper(int id, dreamland_award_retrieve dreamland_award_retrieve)
        {
            this.LandId = id;
            this.AwardRetrieve = dreamland_award_retrieve;
        }
    }
}
