﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/25 20:51:21
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Structs.ProtoBuf;

namespace ModuleDreamland
{
    public class DreamlandMissionRecordDataWrapper
    {
        public bool IsNewRecord;
        public PbDreamlandEvent DreamlandEvent;
        public DreamlandMissionRecordDataWrapper(bool isNewRecord, PbDreamlandEvent dreamlandEvent)
        {
            this.IsNewRecord = isNewRecord;
            this.DreamlandEvent = dreamlandEvent;
        }
    }
}
