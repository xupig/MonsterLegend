﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/13 16:35:53
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using Common.Base;
using Common.Data;
using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using UnityEngine;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using Mogo.Util;

namespace ModuleDreamland
{
    public class DreamlandMissionRecordView : KContainer
    {
        private PbDreamlandEventList _recordDataList;
        private DreamlandContext _context;
        //private bool hadReadCache = false;
        //private DreamLandInfoLocal _dreamLandInfoLocal;
        private PbDreamlandAtkInfo _pbDreamlandAtkInfo;

        private KButton _btnClose;
        private StateText _textRevengeTimes;
        private KScrollView _scrollView;
        private KList _recordList;

        protected override void Awake()
        {
            _btnClose = GetChildComponent<KButton>("Button_close");
            _textRevengeTimes = GetChildComponent<StateText>("Label_txtKefuchou");
            InitScrollView();

            MogoUtils.AdaptScreen(GetChildComponent<StateImage>("ScaleImage_sharedZhezhao"));
        }

        private void InitScrollView()
        {
            _scrollView = GetChildComponent<KScrollView>("ScrollView_shijianjilu");
            _recordList = GetChildComponent<KList>("ScrollView_shijianjilu/mask/content");
            _recordList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _recordList.SetPadding(0, 0, 0, 0);
            _recordList.SetGap(0, 0);
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(OnClick);
            EventDispatcher.AddEventListener<PbDreamlandAtkInfo>(DreamlandEvents.ReceiveBeRobedByMeTimes, ReceiveBeRobedByMeTimes);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(OnClick);
            EventDispatcher.RemoveEventListener<PbDreamlandAtkInfo>(DreamlandEvents.ReceiveBeRobedByMeTimes, ReceiveBeRobedByMeTimes);
        }

        private void ReceiveBeRobedByMeTimes(PbDreamlandAtkInfo atkInfo)
        {
            if (atkInfo.atk_count == 0)
            {
                DreamlandManager.Instance.RequestRevenge((int)atkInfo.land_id, atkInfo.to_dbid, atkInfo.loot_time);
            }
            else if (atkInfo.atk_count > 0)
            {
                _pbDreamlandAtkInfo = atkInfo;
                BaseItemData baseItemData = dreamland_explore_base_helper.GetAttackSameTargetCost((int)atkInfo.land_id, (int)atkInfo.atk_count + 1);
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE,
                    string.Format(MogoLanguageUtil.GetContent(37829, baseItemData.StackCount, item_helper.GetName(baseItemData.Id))),
                    RobConfirmAction);
            }
        }

        private void RobConfirmAction()
        {
            DreamlandManager.Instance.RequestRevenge((int)_pbDreamlandAtkInfo.land_id, _pbDreamlandAtkInfo.to_dbid, _pbDreamlandAtkInfo.loot_time);
        }

        private void OnClick()
        {
            switch (_context)
            {
            case DreamlandContext.main:
                UIManager.Instance.ClosePanel(PanelIdEnum.DreamlandPop);
                break;
            case DreamlandContext.exploreFinish:
                Visible = false;
                break;
            }
        }

        public DreamlandPopDataWrapper Data
        {
            set
            {
                Visible = true;
                _recordDataList = value.RecordList;
                _context = (DreamlandContext) value.RecordList.context;
                //ReadLocalCache();
                PlayerDataManager.Instance.DreamlandData.GetDreamLandInfoLocal();
                RefreshTimes(value.DreamlandInfo);
                RefreshContentList(value.RecordList);
            }
        }

        //private void ReadLocalCache()
        //{
        //    if (!hadReadCache)
        //    {
        //        hadReadCache = true;
        //        _dreamLandInfoLocal = LocalCache.Read<DreamLandInfoLocal>(LocalName.Dreamland);
        //    }
        //}

        private void RefreshTimes(DreamlandInfo dreamlandInfo)
        {
            int maxRevengeTimes = dreamland_explore_base_helper.GetMaxRevengeTimes(dreamlandInfo.Id);
            string colorMaxRevengeTimes = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, maxRevengeTimes.ToString());
            _textRevengeTimes.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37850), maxRevengeTimes - dreamlandInfo.RevengeTimes, colorMaxRevengeTimes);
        }

        private void RefreshContentList(PbDreamlandEventList pbDreamlandEventList)
        {
            List<DreamlandMissionRecordDataWrapper> wrapperList = GetWrapperList(pbDreamlandEventList.dreamland_event);
            _recordList.SetDataList<DreamlandRecord>(wrapperList);
        }

        private List<DreamlandMissionRecordDataWrapper> GetWrapperList(List<PbDreamlandEvent> recordList)
        {
            List<DreamlandMissionRecordDataWrapper> wrapperList = new List<DreamlandMissionRecordDataWrapper>();
            Dictionary<uint, bool> timeDict = GetTimeDict();
            for (int i = 0; i < recordList.Count; i++)
            {
                DreamlandMissionRecordDataWrapper wrapper = new DreamlandMissionRecordDataWrapper(!timeDict.ContainsKey(recordList[i].loot_time), recordList[i]);
                wrapperList.Add(wrapper);
            }
            wrapperList.Sort(SortByLootTime);
            return wrapperList;
        }

        private int SortByLootTime(DreamlandMissionRecordDataWrapper x, DreamlandMissionRecordDataWrapper y)
        {
            if (x.DreamlandEvent.loot_time > y.DreamlandEvent.loot_time)
            {
                return -1;
            }
            return 1;
        }

        private Dictionary<uint, bool> GetTimeDict()
        {
            Dictionary<uint, bool> timeDict = new Dictionary<uint, bool>();
            DreamLandInfoLocal dreamLandInfoLocal = PlayerDataManager.Instance.DreamlandData.GetDreamLandInfoLocal();
            if (dreamLandInfoLocal != null)
            {
                for (int i = 0; i < dreamLandInfoLocal.timeList.Count; i++)
                {
                    if (!timeDict.ContainsKey(dreamLandInfoLocal.timeList[i]))
                    {
                        timeDict.Add(dreamLandInfoLocal.timeList[i], true);
                    }
                }
            }
            return timeDict;
        }

        public void SaveCache()
        {
            if (_recordDataList != null && _recordDataList.dreamland_event.Count > 0)
            {
                DreamLandInfoLocal dreamLandInfoLocal = PlayerDataManager.Instance.DreamlandData.GetDreamLandInfoLocal();
                dreamLandInfoLocal.timeList = new List<uint>();
                for (int i = 0; i < _recordDataList.dreamland_event.Count; i++)
                {
                    dreamLandInfoLocal.timeList.Add(_recordDataList.dreamland_event[i].loot_time);
                }
                PlayerDataManager.Instance.DreamlandData.SaveDreamLandInfoLocal();
            }
        }
    }
}
