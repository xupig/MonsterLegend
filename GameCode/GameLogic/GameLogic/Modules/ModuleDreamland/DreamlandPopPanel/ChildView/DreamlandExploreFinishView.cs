﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/13 16:40:09
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Common.Utils;
using UnityEngine;
using MogoEngine.Events;
using Common.Events;
using ModuleCommonUI;
using Mogo.Util;
using Common.ServerConfig;

namespace ModuleDreamland
{
    public class DreamlandExploreFinishView : KContainer
    {
        private DreamlandInfo _dreamlandInfo;

        private KButton _btnClose;
        private KButton _btnShowRecord;
        private StateImage _imageNewRecord;
        private KButton _btnShowSelectDragon;
        private StateText _textShowSelectDragonText;
        private KList _rewardList;
        private StateText _textProgress;
        private StateText _textRecord;

        protected override void Awake()
        {
            _btnClose = GetChildComponent<KButton>("Button_close");
            _btnShowRecord = GetChildComponent<KButton>("Button_chakanqiangjiejilu");
            _imageNewRecord = GetChildComponent<StateImage>("Button_chakanqiangjiejilu/image03");
            _btnShowSelectDragon = GetChildComponent<KButton>("Button_kaishixiacitansuo");
            _textShowSelectDragonText = GetChildComponent<StateText>("Button_kaishixiacitansuo/label");
            InitList();
            _textProgress = GetChildComponent<StateText>("Label_txtJindu");
            _textRecord = GetChildComponent<StateText>("Label_txtTansuo");

            MogoUtils.AdaptScreen(GetChildComponent<StateImage>("ScaleImage_sharedZhezhao"));
        }

        private void InitList()
        {
            _rewardList = GetChildComponent<KList>("List_jianglineirong");
            _rewardList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _rewardList.SetGap(0, 5);
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(OnClose);
            _btnShowRecord.onClick.AddListener(OnShowRecord);
            _btnShowSelectDragon.onClick.AddListener(OnSelectDragon);
            EventDispatcher.AddEventListener(DreamlandEvents.NewRecordChanged, RefreshNewRecord);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(OnClose);
            _btnShowRecord.onClick.RemoveListener(OnShowRecord);
            _btnShowSelectDragon.onClick.RemoveListener(OnSelectDragon);
            EventDispatcher.RemoveEventListener(DreamlandEvents.NewRecordChanged, RefreshNewRecord);
        }

        private void OnClose()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.DreamlandPop);
        }

        private void OnShowRecord()
        {
            DreamlandManager.Instance.RequestMissionInfo((int) DreamlandContext.exploreFinish);
        }

        private void OnSelectDragon()
        {
            if (_dreamlandInfo.FinishedExploreToday())
            {
                UIManager.Instance.ClosePanel(PanelIdEnum.DreamlandPop);
                //ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(37856), PanelIdEnum.Dreamland);
                return;
            }

            UIManager.Instance.ClosePanel(PanelIdEnum.DreamlandPop);
            DreamlandPopDataWrapper wrapper = new DreamlandPopDataWrapper(DreamlandPopType.selectDragon, _dreamlandInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.DreamlandPop, wrapper);
        }

        public DreamlandInfo Data
        {
            set
            {
                Visible = true;
                _dreamlandInfo = value;
                RefreshProgress(value);
                RefreshReward(value);
                RefreshRobedTimes(value);
                RefreshNewRecord();
                RefreshButton(value);
            }
        }

        private void RefreshButton(DreamlandInfo dreamlandInfo)
        {
            if (_dreamlandInfo.FinishedExploreToday())
            {
                _textShowSelectDragonText.ChangeAllStateText(MogoLanguageUtil.GetContent(39034));
            }
            else
            {
                _textShowSelectDragonText.ChangeAllStateText(MogoLanguageUtil.GetContent(39035));
            }
        }

        private void RefreshProgress(DreamlandInfo dreamlandInfo)
        {
            _textProgress.CurrentText.text = string.Format("{0}/{1}", dreamlandInfo.GetCurrentTimesWhenFinished(), dreamland_explore_station_helper.GetDreamlandChapterMaxStationNum(dreamlandInfo.Id));
        }

        private void RefreshReward(DreamlandInfo dreamlandInfo)
        {
            List<BaseItemData> baseItemDataList = GetBaseItemDataList(dreamlandInfo.RewardList);
            baseItemDataList = dreamland_explore_reward_helper.AttachGuildAddition(baseItemDataList);
            if (baseItemDataList.Count > 0)
            {
                _rewardList.Visible = true;
                _rewardList.SetDataList<RewardItem>(baseItemDataList);
            }
            else
            {
                _rewardList.Visible = false;
            }
        }

        private List<BaseItemData> GetBaseItemDataList(List<PBDreamlandItem> list)
        {
            List<BaseItemData> baseItemDataList = new List<BaseItemData>();
            for (int i = 0; i < list.Count; i++)
            {
                BaseItemData baseItemData = ItemDataCreator.Create((int)list[i].item_id);
                baseItemData.StackCount = (int)list[i].item_sum;
                baseItemDataList.Add(baseItemData);

            }
            return baseItemDataList;
        }

        private void RefreshRobedTimes(DreamlandInfo dreamlandInfo)
        {
            if (dreamlandInfo.BeRobedTimes > 0)
            {
                _textRecord.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37852), dreamlandInfo.BeRobedTimes);
            }
            else
            {
                _textRecord.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37853));
            }
        }

        private void RefreshNewRecord()
        {
            DreamlandData dreamlandData = PlayerDataManager.Instance.DreamlandData;
            if(dreamlandData.HaveNewRecord)
            {
                _imageNewRecord.Alpha = 1f;
            }
            else
            {
                _imageNewRecord.Alpha = 0f;
            }
        }
    }
}
