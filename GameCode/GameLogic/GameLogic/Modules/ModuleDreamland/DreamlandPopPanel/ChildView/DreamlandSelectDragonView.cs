﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/13 16:35:17
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using UnityEngine;
using Mogo.Util;

namespace ModuleDreamland
{
    public class DreamlandSelectDragonView : KContainer
    {
        private const int MAX_DRAGON_NUM = 5;

        private DreamlandInfo _dreamlandInfo;

        private StateText _textCurrentSelect;
        private StateText _textRule;
        private KList _rewardList;
        private KButton _btnQualityUp;
        private IconContainer _containerDiamond;
        private StateText _textQualityUpCost;
        private KButton _btnExplore;
        private KList _dragonList;
        private StateText _textTitle;
        private KButton _btnClose;
        private GameObject _goParticle;
        private ItemGrid _itemGridTreasure;

        protected override void Awake()
        {
            _btnQualityUp = GetChildComponent<KButton>("Button_tisheng");
            _textCurrentSelect = GetChildComponent<StateText>("Container_dangqianxuanze/Label_txtDangqianxuanze");
            _textRule = GetChildComponent<StateText>("Container_guize/Label_txtDangqiandiercibisai");
            InitRewardList();
            _containerDiamond = AddChildComponent<IconContainer>("Container_xiaohaozuanshi/Container_zuanshi");
            _textQualityUpCost = GetChildComponent<StateText>("Container_xiaohaozuanshi/Label_txtXiaohao");
            _btnExplore = GetChildComponent<KButton>("Button_kaishitansuo");
            InitDragonList();
            _textTitle = GetChildComponent<StateText>("Label_txtBiaoti");
            _btnClose = GetChildComponent<KButton>("Button_close");
            _goParticle = GetChild("fx_ui_8_2_shanguang_01");
            _goParticle.SetActive(false);
            _itemGridTreasure = GetChildComponent<ItemGrid>("Container_kenengjiangli/Container_wupin");

            MogoUtils.AdaptScreen(GetChildComponent<StateImage>("ScaleImage_sharedZhezhao"));
        }

        private void InitRewardList()
        {
            _rewardList = GetChildComponent<KList>("Container_jiangli/List_rewardContent");
            _rewardList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _rewardList.SetGap(0, 5);
        }

        private void InitDragonList()
        {
            _dragonList = GetChildComponent<KList>("List_xuanzefeilongliebiao");
            _dragonList.SetDirection(KList.KListDirection.LeftToRight, MAX_DRAGON_NUM, 1);
            _dragonList.SetGap(0, 26);
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnQualityUp.onClick.AddListener(OnQualityUp);
            _btnExplore.onClick.AddListener(OnExplore);
            _btnClose.onClick.AddListener(OnClose);
            EventDispatcher.AddEventListener(DreamlandEvents.ReceiveQualityUp, OnRefresh);
        }

        private void RemoveListener()
        {
            _btnQualityUp.onClick.RemoveListener(OnQualityUp);
            _btnExplore.onClick.RemoveListener(OnExplore);
            _btnClose.onClick.RemoveListener(OnClose);
            EventDispatcher.RemoveEventListener(DreamlandEvents.ReceiveQualityUp, OnRefresh);
        }

        private void OnQualityUp()
        {
            ItemData itemData = dreamland_explore_base_helper.GetQualityUpItemCost(_dreamlandInfo.Id);
            if (_dreamlandInfo.DragonQuality == public_config.DREAMLAND_QUALITY_GOLD)
            {
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(37804, dreamland_explore_base_helper.GetShortName(_dreamlandInfo.Id)));
                return;
            }
            string qualityStr = ColorDefine.GetColorName(_dreamlandInfo.DragonQuality);
            if (PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemNum(itemData.Id) >= itemData.StackCount)
            {
                DreamlandManager.Instance.RequestQualityUp(_dreamlandInfo.Id, DreamlandManager.USE_ITEM_QUALITY_UP_PROBABILITY);
            }
            else
            {
                BaseItemData baseItemData = dreamland_explore_base_helper.GetQualityUpCost(_dreamlandInfo.Id);
                string enoughBindDiamondContent = string.Format(MogoLanguageUtil.GetContent(37800, baseItemData.StackCount, baseItemData.Name, qualityStr));
                string notEnoughBindDiamondContent = MogoLanguageUtil.GetContent(39015);
                MogoTipsUtils.ShowBindDiamondTips(baseItemData.StackCount, enoughBindDiamondContent, notEnoughBindDiamondContent, UseDiamondQualityUpConfirmAction);
            }
        }

        private void UseDiamondQualityUpConfirmAction()
        {
            DreamlandManager.Instance.RequestQualityUp(_dreamlandInfo.Id, DreamlandManager.USE_DIAMOND_QUALITY_UP_PROBABILITY);
        }

        private void OnExplore()
        {
            DreamLandInfoLocal dreamLandInfoLocal = PlayerDataManager.Instance.DreamlandData.GetDreamLandInfoLocal();
            SettingData settingData = PlayerDataManager.Instance.SettingData;
            dreamLandInfoLocal.exploreTime++;
            PlayerDataManager.Instance.DreamlandData.SaveDreamLandInfoLocal();
            if (PlayerAvatar.Player.level < GlobalParams.GetDreamLandPushHintMaxLevel() && dreamLandInfoLocal.exploreTime == 2)
            {
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(39033), OnConfirm, OnCancel, true, false, true);
            }
            else
            {
                DreamlandManager.Instance.RequestExplore(_dreamlandInfo.Id, _dreamlandInfo.CurrentStationId);
            }

        }

        private void OnConfirm()
        {
            DreamlandManager.Instance.RequestExplore(_dreamlandInfo.Id, _dreamlandInfo.CurrentStationId);
            SettingData settingData = PlayerDataManager.Instance.SettingData;
            settingData.SetPushValue(10, true);
            settingData.SaveSettingData();
        }

        private void OnCancel()
        {
            DreamlandManager.Instance.RequestExplore(_dreamlandInfo.Id, _dreamlandInfo.CurrentStationId);
            SettingData settingData = PlayerDataManager.Instance.SettingData;
            settingData.SetPushValue(10, false);
            settingData.SaveSettingData();
        }

        private void OnClose()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.DreamlandPop);
        }

        private void OnRefresh()
        {
            Refresh(_dreamlandInfo);
        }

        public DreamlandInfo Data
        {
            set
            {
                Visible = true;
                _dreamlandInfo = value;
                Refresh(value);
            }
        }

        private void Refresh(DreamlandInfo dreamlandInfo)
        {
            RefreshTitle(dreamlandInfo);
            RefreshCurrentSelect(dreamlandInfo);
            RefreshRule(dreamlandInfo);
            RefreshReward(dreamlandInfo);
            RefreshCost(dreamlandInfo);
            RefreshDragonList(dreamlandInfo);
            RefreshTreasureBoxItem(dreamlandInfo.Id);
        }

        private void RefreshTitle(DreamlandInfo dreamlandInfo)
        {
            _textTitle.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37845), dreamland_explore_base_helper.GetShortName(dreamlandInfo.Id));
        }

        private void RefreshCurrentSelect(DreamlandInfo dreamlandInfo)
        {
            int quality = dreamlandInfo.DragonQuality;
            string currentDragonName = string.Concat(ColorDefine.GetColorName(quality), dreamland_explore_base_helper.GetShortName(dreamlandInfo.Id));
            string colorCurrentDragonName = ColorDefine.GetColorHtmlString(quality, currentDragonName);
            if (quality != public_config.ITEM_QUALITY_GOLDEN)
            {
                RefreshQualityCurrentSelect(dreamlandInfo, colorCurrentDragonName);
            }
            else
            {
                RefreshMaxQualityCurrentSelect(dreamlandInfo, colorCurrentDragonName);
            }
        }

        private void RefreshQualityCurrentSelect(DreamlandInfo dreamlandInfo, string colorCurrentDragonName)
        {
            int quality = dreamlandInfo.DragonQuality;
            string nextDragonName = string.Concat(ColorDefine.GetColorName(quality + 1), dreamland_explore_base_helper.GetShortName(dreamlandInfo.Id));
            string colorNextDragonName = ColorDefine.GetColorHtmlString(quality + 1, nextDragonName);
            _textCurrentSelect.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37846), colorCurrentDragonName, colorNextDragonName);
        }

        private void RefreshMaxQualityCurrentSelect(DreamlandInfo dreamlandInfo, string colorCurrentDragonName)
        {
            _textCurrentSelect.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37847), colorCurrentDragonName);
        }

        private void RefreshRule(DreamlandInfo dreamlandInfo)
        {
            _textRule.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37848), dreamlandInfo.GetCurrentTimesWhenUnfinish());
        }

        private void RefreshReward(DreamlandInfo dreamlandInfo)
        {
            List<BaseItemData> rewardDataList = dreamland_explore_reward_helper.GetBaseItemDataList(dreamlandInfo.Id, 
                PlayerAvatar.Player.level, dreamlandInfo.DragonQuality, dreamlandInfo.CurrentStationId);
            rewardDataList = dreamland_explore_reward_helper.AttachGuildAddition(rewardDataList);
            _rewardList.RemoveAll();
            _rewardList.SetDataList<RewardItem>(rewardDataList);
        }

        private void RefreshCost(DreamlandInfo dreamlandInfo)
        {
            ItemData itemData = dreamland_explore_base_helper.GetQualityUpItemCost(dreamlandInfo.Id);
            int ownNum = PlayerAvatar.Player.GetItemNum(itemData.Id);
            if(ownNum >= itemData.StackCount)
            {
                _containerDiamond.SetIcon(item_helper.GetIcon(itemData.Id));
                _textQualityUpCost.CurrentText.text = string.Format("{1}/{0}", itemData.StackCount, ownNum);
            }
            else
            {
                BaseItemData baseItemData = dreamland_explore_base_helper.GetQualityUpCost(dreamlandInfo.Id);
                _containerDiamond.SetIcon(baseItemData.Icon);
                ownNum = PlayerAvatar.Player.GetItemNum(baseItemData.Id);
                if (ownNum < baseItemData.StackCount)
                {
                    _textQualityUpCost.CurrentText.text = string.Format("{1}/{0}", baseItemData.StackCount,
                        ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, ownNum.ToString()));
                }
                else
                {
                    _textQualityUpCost.CurrentText.text = string.Format("{1}/{0}", baseItemData.StackCount, ownNum);
                }
            }
        }

        private void RefreshDragonList(DreamlandInfo dreamlandInfo)
        {
            List<DreamlandDragonDataWrapper> wrapperList = GetWrapperList(dreamlandInfo);
            _dragonList.SetDataList<DreamlandDragonGrid>(wrapperList);
        }

        private List<DreamlandDragonDataWrapper> GetWrapperList(DreamlandInfo dreamlandInfo)
        {
            List<DreamlandDragonDataWrapper> wrapperList = new List<DreamlandDragonDataWrapper>();
            for (int i = 0; i < MAX_DRAGON_NUM; i++)
            {
                DreamlandDragonDataWrapper wrapper = new DreamlandDragonDataWrapper(dreamlandInfo.Id, 
                    public_config.ITEM_QUALITY_GREEN + i, dreamlandInfo.DragonQuality, dreamlandInfo.IsQualityUp);
                wrapperList.Add(wrapper);
            }
            return wrapperList;
        }

        private void RefreshTreasureBoxItem(int dreamlandId)
        {
            BaseItemData baseItemData = dreamland_explore_reward_helper.GetTreasureBoxBaseItemData(dreamlandId, PlayerAvatar.Player.level);
            _itemGridTreasure.SetItemData(baseItemData);
        }
    }
}
