﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/13 16:39:10
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Base;
using Common.Global;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using Common.Utils;
using Mogo.Util;

namespace ModuleDreamland
{
    public class DreamlandRetrieveRewardView : KContainer
    {
        private StateText _textDesc;
        private KList _list;
        private KButton _btnClose;
        private StateText _textName;

        protected override void Awake()
        {
            InitList();
            _btnClose = GetChildComponent<KButton>("Button_close");
            _textDesc = GetChildComponent<StateText>("Label_txtZhaohuijiangli");
            _textName = GetChildComponent<StateText>("Label_txtZhufuzhi");
            _textName.CurrentText.text = MogoLanguageUtil.GetContent(37825);

            MogoUtils.AdaptScreen(GetChildComponent<StateImage>("Container_bg/ScaleImage_sharedZhezhao"));
            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(OnClose);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(OnClose);
        }

        private void OnClose()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.DreamlandPop);
        }

        private void InitList()
        {
            _list = GetChildComponent<KList>("List_content");
            _list.SetDirection(KList.KListDirection.LeftToRight, 3, 1);
            _list.SetGap(5, 20);
        }

        public PbDreamlandBoxInfoList Data
        {
            set
            {
                Visible = true;
                //只会有一个可以找回
                PbDreamlandBoxInfo pbDreamlandBoxInfo = value.box_info[0];
                RefreshDesc(pbDreamlandBoxInfo);
                RefreshList(pbDreamlandBoxInfo);
            }
        }

        private void RefreshDesc(PbDreamlandBoxInfo pbDreamlandBoxInfo)
        {
            int canRetrieveTimes = (int) pbDreamlandBoxInfo.remain_count;
            int exploredTimes = dreamland_explore_station_helper.GetDreamlandChapterMaxStationNum((int) pbDreamlandBoxInfo.land_id) - canRetrieveTimes;
            _textDesc.CurrentText.text = 
                string.Format(MogoLanguageUtil.GetContent(37849), 
                ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, exploredTimes.ToString()), 
                ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, canRetrieveTimes.ToString()),
                MogoLanguageUtil.GetContent(37825));
        }

        private void RefreshList(PbDreamlandBoxInfo pbDreamlandBoxInfo)
        {
            List<DreamlandRetrieveRewardDataWrapper> wrapperList = GetWrapperList(pbDreamlandBoxInfo);
            _list.SetDataList<DreamlandRetrieveRewardGrid>(wrapperList);
        }

        private List<DreamlandRetrieveRewardDataWrapper> GetWrapperList(PbDreamlandBoxInfo pbDreamlandBoxInfo)
        {
            List<dreamland_award_retrieve> awardRetrieveList = dreamland_award_retrieve_helper.GetRetrieveRewardList((int) pbDreamlandBoxInfo.remain_count, PlayerAvatar.Player.level);
            List<DreamlandRetrieveRewardDataWrapper> wrapperList = new List<DreamlandRetrieveRewardDataWrapper>();
            for (int i = 0; i < awardRetrieveList.Count; i++)
            {
                DreamlandRetrieveRewardDataWrapper wrapper = new DreamlandRetrieveRewardDataWrapper((int) pbDreamlandBoxInfo.land_id, awardRetrieveList[i]);
                wrapperList.Add(wrapper);
            }
            return wrapperList;
        }
    }
}
