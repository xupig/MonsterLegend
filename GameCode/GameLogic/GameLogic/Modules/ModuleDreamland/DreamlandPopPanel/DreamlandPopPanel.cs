﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/13 16:32:17
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Events;
using MogoEngine.Events;
using Common.Structs.ProtoBuf;
using UnityEngine;
using GameMain.GlobalManager;

namespace ModuleDreamland
{
    public enum DreamlandContext
    {
        main,
        exploreFinish,
    }

    public enum DreamlandPopType
    {
        selectDragon,
        missionRecord,
        retrieveReward,
        exploreFinish,
    }

    public class DreamlandPopPanel : BasePanel
    {
        private DreamlandExploreFinishView _exploreFinish;
        private DreamlandMissionRecordView _missionRecord;
        private DreamlandRetrieveRewardView _retrieveReward;
        private DreamlandSelectDragonView _selectDragon;

        protected override void Awake()
        {
            _exploreFinish = AddChildComponent<DreamlandExploreFinishView>("Container_tansuowancheng");
            _missionRecord = AddChildComponent<DreamlandMissionRecordView>("Container_shijianjilu");
            _retrieveReward = AddChildComponent<DreamlandRetrieveRewardView>("Container_yueguangbaohe");
            _selectDragon = AddChildComponent<DreamlandSelectDragonView>("Container_Xuanzefeilong");
        }

        private void AddListener()
        {
            EventDispatcher.AddEventListener<DreamlandPopDataWrapper>(DreamlandEvents.ShowMissionRecord, ShowMissionRecord);
        }

        private void RemoveListener()
        {
            EventDispatcher.RemoveEventListener<DreamlandPopDataWrapper>(DreamlandEvents.ShowMissionRecord, ShowMissionRecord);
        }

        private void ShowMissionRecord(DreamlandPopDataWrapper wrapper)
        {
            _missionRecord.Data = wrapper;
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.DreamlandPop; }
        }

        public override void OnClose()
        {
            _missionRecord.SaveCache();
            RemoveListener();
            UIManager.Instance.EnablePanelCanvas(PanelIdEnum.Dreamland);
        }

        public override void OnShow(object data)
        {
            UIManager.Instance.DisablePanelCanvas(PanelIdEnum.Dreamland);
            AddListener();
            DreamlandPopDataWrapper wrapper = data as DreamlandPopDataWrapper;
            DreamlandPopType popType = wrapper.PopType;
            HideAll();
            switch (popType)
            {
            case DreamlandPopType.exploreFinish:
                _exploreFinish.Data = wrapper.DreamlandInfo;
                break;
            case DreamlandPopType.missionRecord:
                _missionRecord.Data = wrapper;
                break;
            case DreamlandPopType.retrieveReward:
                _retrieveReward.Data = wrapper.RetrieveRewardList;
                break;
            case DreamlandPopType.selectDragon:
                _selectDragon.Data = wrapper.DreamlandInfo;
                break;
            }
        }

        private void HideAll()
        {
            _exploreFinish.Visible = false;
            _missionRecord.Visible = false;
            _retrieveReward.Visible = false;
            _selectDragon.Visible = false;
        }
    }
}
