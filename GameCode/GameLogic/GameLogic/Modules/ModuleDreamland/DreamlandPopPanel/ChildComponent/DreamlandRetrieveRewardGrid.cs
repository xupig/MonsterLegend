﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/13 21:59:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using UnityEngine;

namespace ModuleDreamland
{
    public class DreamlandRetrieveRewardGrid : KList.KListItemBase
    {
        private int _dreamlandId;
        private dreamland_award_retrieve _awardRetrieve;

        private StateText _textName;
        private KButton _btnUse;
        private StateIcon _iconDiamond;
        private GameObject _goFree;
        private GameObject _goRetrieve;
        private StateText _textCost;
        private DreamlandReward _rewardMoney;
        private DreamlandReward _rewardExp;
        private StateText _textDesc;

        protected override void Awake()
        {
            _textName = GetChildComponent<StateText>("Label_txtShanzhaiyueguangbao");
            _btnUse = GetChildComponent<KButton>("Button_goumai");
            _iconDiamond = GetChildComponent<StateIcon>("Button_goumai/stateIcon");
            _goFree = GetChild("Button_goumai/mianfei");
            _goRetrieve = GetChild("Button_goumai/zhaohui");
            _textCost = GetChildComponent<StateText>("Button_goumai/huafei");
            _rewardExp = AddChildComponent<DreamlandReward>("Container_jingan");
            _rewardMoney = AddChildComponent<DreamlandReward>("Container_jinbi");
            _textDesc = GetChildComponent<StateText>("Label_txtMiaoshu");

            AddListener();
        }

        public override void Dispose()
        {
            _awardRetrieve = null;
            RemoveListener();
        }

        private void AddListener()
        {
            _btnUse.onClick.AddListener(OnRetrieveReward);
        }

        private void RemoveListener()
        {
            _btnUse.onClick.RemoveListener(OnRetrieveReward);
        }

        private void OnRetrieveReward()
        {
            if((_awardRetrieve.__get_back == dreamland_award_retrieve_helper.GetBackTypeCost1)
                ||(_awardRetrieve.__get_back == dreamland_award_retrieve_helper.GetBackTypeCost2))
            {
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE,
                    string.Format(MogoLanguageUtil.GetContent(37854), _awardRetrieve.__cost[public_config.MONEY_TYPE_DIAMOND.ToString()], MogoLanguageUtil.GetContent(37825)), ConfirmAction);
            }
            else
            {
                DreamlandManager.Instance.RequestAwardRetrieve(_dreamlandId, _awardRetrieve.__get_back);
            }
        }

        private void ConfirmAction()
        {
            DreamlandManager.Instance.RequestAwardRetrieve(_dreamlandId, _awardRetrieve.__get_back);
        }

        public override object Data
        {
            set
            {
                DreamlandRetrieveRewardDataWrapper wrapper = value as DreamlandRetrieveRewardDataWrapper;
                dreamland_award_retrieve awardRetrieve = wrapper.AwardRetrieve;
                _dreamlandId = wrapper.LandId;
                _awardRetrieve = awardRetrieve;
                RefreshName();
                RefreshDesc(awardRetrieve);
                RefreshBtn(awardRetrieve);
                RefreshReward(awardRetrieve);
            }
        }

        private void RefreshName()
        {
            _textName.CurrentText.text = MogoLanguageUtil.GetContent(37826 + Index);
        }

        private void RefreshDesc(dreamland_award_retrieve awardRetrieve)
        {
            _textDesc.CurrentText.text = MogoLanguageUtil.GetContent(awardRetrieve.__name);
        }

        private void RefreshBtn(dreamland_award_retrieve awardRetrieve)
        {
            HideBtnDesc();
            switch(awardRetrieve.__get_back)
            {
            case dreamland_award_retrieve_helper.GetBackTypeFree:
                _goFree.SetActive(true);
                break;
            case dreamland_award_retrieve_helper.GetBackTypeCost1:
            case dreamland_award_retrieve_helper.GetBackTypeCost2:
                _iconDiamond.Visible = true;
                _goRetrieve.SetActive(true);
                _textCost.Visible = true;
                _iconDiamond.SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_DIAMOND));
                _textCost.ChangeAllStateText(awardRetrieve.__cost[public_config.MONEY_TYPE_DIAMOND.ToString()]);
                break;
            }
        }

        private void HideBtnDesc()
        {
            _iconDiamond.Visible = false;
            _goFree.SetActive(false);
            _goRetrieve.SetActive(false);
            _textCost.Visible = false;
        }

        private void RefreshReward(dreamland_award_retrieve awardRetrieve)
        {
            int count = 0;
            foreach (string itemId in _awardRetrieve.__reward.Keys)
            {
                ItemData itemData = new ItemData(int.Parse(itemId));
                itemData.StackCount = int.Parse(_awardRetrieve.__reward[itemId]);
                if (count == 0)
                {
                    _rewardExp.Data = itemData;
                }
                else if (count == 1)
                {
                    _rewardMoney.Data = itemData;
                }
                if (count >= 1)
                {
                    break;
                }
                count++;
            }
        }
    }
}
