﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/13 16:55:39
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Utils;
using UnityEngine;
using ModuleCommonUI;
using Common.Data;
using GameMain.GlobalManager;

namespace ModuleDreamland
{
    public enum DreamlandRecordRevengeState
    {
        cannotRevenge = 0,
        canRevenge,
        hadBeat,
    }

    public class DreamlandRecord : KList.KListItemBase
    {
        private PbDreamlandEvent _record;

        private KButton _btnRevenge;
        private StateImage _imageBeat;
        private StateImage _imageNew;
        private StateText _textDesc;

        protected override void Awake()
        {
            _btnRevenge = GetChildComponent<KButton>("Button_fuchou");
            _imageBeat = GetChildComponent<StateImage>("Image_sharedyijibai");
            _imageNew = GetChildComponent<StateImage>("Image_Point");
            _textDesc = GetChildComponent<StateText>("Label_txtfuchouxinxi");

            AddListener();
        }

        public override void Dispose()
        {
            _record = null;
            RemoveListener();
        }

        private void AddListener()
        {
            _btnRevenge.onClick.AddListener(OnRevenge);
        }

        private void RemoveListener()
        {
            _btnRevenge.onClick.RemoveListener(OnRevenge);
        }

        private void OnRevenge()
        {

            TeamData teamData = PlayerDataManager.Instance.TeamData;
            if (teamData.IsInTeam())
            {
                string content = MogoLanguageUtil.GetContent(39032);
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, OnConfirm, OnCancel);
            }
            else
            {
                OnConfirm();
            }
        }

        private void OnConfirm()
        {
            DreamlandManager.Instance.RequestBeRobedByMeTimes(_record.dbid, (int)_record.land_id, _record.loot_time);

        }

        private void OnCancel()
        {

        }

        public override object Data
        {
            set
            {
                DreamlandMissionRecordDataWrapper wrapper = value as DreamlandMissionRecordDataWrapper;
                PbDreamlandEvent record = wrapper.DreamlandEvent;
                _record = record;
                record.quality_id = record.quality_id == 0 ? public_config.DREAMLAND_QUALITY_GREEN : record.quality_id;
                record.name = MogoProtoUtils.ParseByteArrToString(record.name_bytes);
                //Debug.LogError(string.Format("dbid:{0} isWin:{1} landId:{2} lootTime:{3} name:{4}  qualityId:{5}, state:{6}", record.dbid, record.is_win, record.land_id, record.loot_time, record.name, record.quality_id, record.state));
                RefreshBtn(record);
                RefreshNew(wrapper.IsNewRecord);
                RefreshContent(record);
            }
        }

        private void RefreshBtn(PbDreamlandEvent record)
        {
            _btnRevenge.Visible = false;
            _imageBeat.Visible = false;
            switch ((DreamlandRecordRevengeState) record.state)
            {
            case DreamlandRecordRevengeState.cannotRevenge:
                break;
            case DreamlandRecordRevengeState.canRevenge:
                _btnRevenge.Visible = true;
                break;
            case DreamlandRecordRevengeState.hadBeat:
                _imageBeat.Visible = true;
                break;
            }
        }

        private void RefreshNew(bool isNew)
        {
            if (isNew)
            {
                _imageNew.Alpha = 1f;
            }
            else
            {
                _imageNew.Alpha = 0f;
            }
        }

        private void RefreshContent(PbDreamlandEvent record)
        {
            DateTime dateTime = MogoTimeUtil.ServerTimeStamp2UtcTime(record.loot_time);
            string time = string.Format("[{0}-{1}-{2}-{3}:{4}]", dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute);
            string colorName = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, record.name);
            string dragonName = string.Concat(ColorDefine.GetColorName((int) record.quality_id), dreamland_explore_base_helper.GetShortName((int) record.land_id));
            string colorDragonName = ColorDefine.GetColorHtmlString((int)record.quality_id, dragonName);
            //主动打劫记录
            if (record.loot_type == 1)
            {
                if (record.is_win == 1)
                {
                    _textDesc.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37821), time, colorName, colorDragonName, record.atk_gold, record.atk_exp);
                }
                else
                {
                    _textDesc.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37822), time, colorName, colorDragonName);
                }
            }
            //被打劫记录
            else
            {
                if (record.is_win == 1)
                {
                    _textDesc.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37824), time, colorName, colorDragonName, record.atk_gold, record.atk_exp);
                }
                else
                {
                    _textDesc.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37823), time, colorName, colorDragonName);
                }
            }
            
        }
    }
}
