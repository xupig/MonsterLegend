﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/13 17:29:46
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using UnityEngine;
using Common.ExtendComponent;

namespace ModuleDreamland
{
    public class DreamlandDragonGrid : KList.KListItemBase
    {
        private int _landId;
        private int _quality;
        private int DragonIconId = 12000;

        private KContainer _containerIcon;
        private StateText _textRewardRate;
        private StateText _textDragonSoul;
        private GameObject _goDragonSoul;
        private KButton _btnUseDragonSoul;
        private KButton _btnBuy;
        private StateText _textName;
        private GameObject _goSelectedFrame;
        private IconContainer _containerDragonSoulIcon;
        private GameObject _goParticle;

        protected override void Awake()
        {
            _containerIcon = GetChildComponent<KContainer>("Container_icon");
            _textRewardRate = GetChildComponent<StateText>("Label_txtJiangli");
            _textDragonSoul = GetChildComponent<StateText>("Container_longhun/Label_txtLonghun");
            _goDragonSoul = GetChild("Container_longhun");
            _btnUseDragonSoul = GetChildComponent<KButton>("Container_longhun/Button_zhaikuang");
            _containerDragonSoulIcon = AddChildComponent<IconContainer>("Container_longhun/Container_tubiao");
            _btnBuy = GetChildComponent<KButton>("Button_goumai");
            _textName = GetChildComponent<StateText>("Label_txtFeilongName");
            _goSelectedFrame = GetChild("ScaleImage_guangzhao");
            _goParticle = transform.parent.parent.FindChild("fx_ui_8_2_shanguang_01").gameObject;

            AddListener();
        }

        public override void Dispose()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnUseDragonSoul.onClick.AddListener(OnUseDragonSoul);
            _btnBuy.onClick.AddListener(OnBuy);
        }

        private void RemoveListener()
        {
            _btnUseDragonSoul.onClick.RemoveListener(OnUseDragonSoul);
            _btnBuy.onClick.RemoveListener(OnBuy);
        }

        private void OnUseDragonSoul()
        {
            ItemData itemData = dreamland_explore_quality_helper.GetDragonSoulCost(_landId, _quality);
            string dragonName = string.Concat(ColorDefine.GetColorName(_quality), dreamland_explore_base_helper.GetShortName(_landId));
            string colorDragonName = ColorDefine.GetColorHtmlString(_quality, dragonName);
            MessageBox.Show(true, MessageBox.DEFAULT_TITLE, string.Format(
                MogoLanguageUtil.GetContent(37808), item_helper.GetName(itemData.Id), dreamland_explore_base_helper.GetShortName(_landId), colorDragonName), 
                UseDragonSoulConfirmAction);
        }

        private void UseDragonSoulConfirmAction()
        {
            DreamlandManager.Instance.RequestQualityUp(_landId, DreamlandManager.USE_ITEM_QUALITY_UP, _quality);
        }

        private void OnBuy()
        {
            dreamland_explore_base exploreBase = dreamland_explore_base_helper.GetConfig(_landId);
            BaseItemData baseItemData = price_list_helper.GetCost(exploreBase.__gold_dragon_price);
            string enoughBindDiamondContent = string.Format(MogoLanguageUtil.GetContent(37801), baseItemData.StackCount, item_helper.GetName(baseItemData.Id),
                dreamland_explore_base_helper.GetShortName(_landId));
            string notEnoughBindDiamondContent = MogoLanguageUtil.GetContent(39016);
            MogoTipsUtils.ShowBindDiamondTips(baseItemData.StackCount, enoughBindDiamondContent, notEnoughBindDiamondContent, BuyConfirmAction);
        }

        private void BuyConfirmAction()
        {
            DreamlandManager.Instance.RequestBuyDragon(_landId);
        }

        public override object Data
        {
            set
            {
                DreamlandDragonDataWrapper wrapper = value as DreamlandDragonDataWrapper;
                dreamland_explore_quality qualityConfig = dreamland_explore_quality_helper.GetConfig(wrapper.LandId, wrapper.Quality);
                _landId = wrapper.LandId;
                _quality = wrapper.Quality;
                RefreshIcon();
                RefreshName(wrapper.LandId, wrapper.Quality);
                RefreshAddion(qualityConfig);
                RefreshSelectFrame(wrapper.Quality, wrapper.CurrentSelectDragonQuality);
                RefreshDragonSoul(qualityConfig, wrapper.CurrentSelectDragonQuality);
                if (wrapper.IsQualityUp)
                {
                    RefreshParticle(wrapper.Quality, wrapper.CurrentSelectDragonQuality);
                }
            }
        }

        private void RefreshIcon()
        {
            MogoAtlasUtils.AddSprite(_containerIcon.gameObject, icon_item_helper.GetIcon(DragonIconId), icon_item_helper.GetIconBg(DragonIconId), ColorDefine.COLOR_ID_GREEN + Index);
        }

        private void RefreshName(int landId, int quality)
        {
            string dragonName = string.Concat(ColorDefine.GetColorName(quality), dreamland_explore_base_helper.GetShortName(landId));
            _textName.CurrentText.text = dragonName;
        }

        private void RefreshAddion(dreamland_explore_quality qualityConfig)
        {
            _textRewardRate.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37855), qualityConfig.__reward_addition / 100);
        }

        private void RefreshSelectFrame(int quality, int currentSelectDragonQuality)
        {
            _goSelectedFrame.SetActive(quality == currentSelectDragonQuality);
        }

        private void RefreshDragonSoul(dreamland_explore_quality qualityConfig, int currentSelectDragonQuality)
        {
            HideAll();
            if (NeedShowDragonSoul(qualityConfig, currentSelectDragonQuality))
            {
                _goDragonSoul.SetActive(true);
                ItemData itemData = dreamland_explore_quality_helper.GetDragonSoulCost(qualityConfig);
                _textDragonSoul.CurrentText.text = string.Format("X{0}", PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemNum(itemData.Id));
                _containerDragonSoulIcon.SetIcon(item_helper.GetIcon(itemData.Id));
            }
            else if (CanBuy(qualityConfig, currentSelectDragonQuality))
            {
                _btnBuy.Visible = true;
            }
        }

        private bool NeedShowDragonSoul(dreamland_explore_quality qualityConfig, int currentSelectDragonQuality)
        {
            if (qualityConfig.__quality == public_config.ITEM_QUALITY_GREEN)
            {
                return false;
            }
            ItemData itemData = dreamland_explore_quality_helper.GetDragonSoulCost(qualityConfig);
            if (currentSelectDragonQuality >= qualityConfig.__quality)
            {
                return false;
            }

            return HaveDragonSoul(itemData);
        }

        private void HideAll()
        {
            _btnBuy.Visible = false;
            _goDragonSoul.SetActive(false);
        }

        private bool HaveDragonSoul(ItemData itemData)
        {
            return PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemNum(itemData.Id) > 0;
        }

        private bool CanBuy(dreamland_explore_quality qualityConfig, int currentSelectDragonQuality)
        {
            if (currentSelectDragonQuality == public_config.ITEM_QUALITY_GOLDEN)
            {
                return false;
            }
            return qualityConfig.__quality == public_config.ITEM_QUALITY_GOLDEN;
        }

        private void RefreshParticle(int quality, int currentSelectDragonQuality)
        {
            if (currentSelectDragonQuality == quality)
            {
                KParticle particle = CloneParticle().AddComponent<KParticle>();
                particle.transform.SetParent(transform);
                particle.transform.localPosition = Vector3.zero;
                particle.transform.localScale = Vector3.one;
                particle.Visible = true;
                particle.Play();
            }
        }

        private GameObject CloneParticle()
        {
            GameObject go = Instantiate(_goParticle) as GameObject;
            go.transform.SetParent(transform);
            go.transform.localScale = Vector3.one;
            go.transform.localPosition = Vector3.zero;
            return go;
        }
    }
}
