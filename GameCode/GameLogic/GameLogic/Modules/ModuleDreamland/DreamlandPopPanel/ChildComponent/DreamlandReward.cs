﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/13 17:13:45
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Game.UI.UIComponent;

namespace ModuleDreamland
{
    public class DreamlandReward : KContainer
    {
        private ItemGrid _itemGrid;
        private StateText _textCost;

        protected override void Awake()
        {
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _textCost = GetChildComponent<StateText>("Label_txtShuliang");
        }

        public ItemData Data
        {
            set
            {
                Refresh(value);
            }
        }

        private void Refresh(ItemData itemData)
        {
            _itemGrid.SetItemData(itemData.Id);
            _textCost.CurrentText.text = string.Concat("x", itemData.StackCount.ToString());
        }
    }
}
