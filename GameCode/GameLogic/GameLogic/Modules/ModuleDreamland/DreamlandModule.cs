﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/12 14:48:24
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Events;
using Game.Asset;
using GameMain.GlobalManager;
using MogoEngine.Events;
using UnityEngine;
using GameMain.GlobalManager.SubSystem;

namespace ModuleDreamland
{
    public class DreamlandModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.Dreamland, "Container_DreamlandPanel", MogoUILayer.LayerUIPanel, "ModuleDreamland.DreamlandPanel", BlurUnderlay.Empty,HideMainUI.Hide);
            AddPanel(PanelIdEnum.DreamlandStationTips, "Container_DreamlandStationTips", MogoUILayer.LayerUIToolTip, "ModuleDreamland.DreamlandStationTips", BlurUnderlay.Empty, HideMainUI.Hide);
            AddPanel(PanelIdEnum.DreamlandResult, "Container_DreamlandResultPanel", MogoUILayer.LayerUIPanel, "ModuleDreamland.DreamlandResultPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.DreamlandPop, "Container_DreamlandPopPanel", MogoUILayer.LayerUIPanel, "ModuleDreamland.DreamlandPopPanel", BlurUnderlay.Have, HideMainUI.Hide);
            EventDispatcher.AddEventListener(DreamlandEvents.ShowScene, ShowScene);
        }

        private void ShowScene()
        {
            ////预加载准备界面的场景
            //ObjectPool.Instance.PreloadAsset(new string[] { DreamlandScene.PREPARE_SCENE_PATH }, LoadCallback);
            UIManager.Instance.ShowPanel(PanelIdEnum.Dreamland);
        }

        //private void LoadCallback()
        //{
        //    UIManager.Instance.ShowPanel(PanelIdEnum.Dreamland);
        //}
    }
}
