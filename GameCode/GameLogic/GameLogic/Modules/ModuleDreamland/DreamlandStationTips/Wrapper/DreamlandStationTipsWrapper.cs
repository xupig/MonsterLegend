﻿using Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleDreamland
{
    class DreamlandStationTipsWrapper
    {
        public DreamlandInfo DreamlandInfo;
        public int StationId;
        public bool IsFinish;

        public DreamlandStationTipsWrapper(DreamlandInfo dreamlandInfo, int stationId, bool isFinish)
        {
            this.DreamlandInfo = dreamlandInfo;
            this.StationId = stationId;
            this.IsFinish = isFinish;
        }
    }
}
