﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using Mogo.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleDreamland
{
    public class DreamlandStationTips : BasePanel
    {
        private StateText _textTitle;
        private StateText _textContent;
        private KList _listReward;
        private GameObject _goReward;

        protected override void Awake()
        {
            _textTitle = GetChildComponent<StateText>("Label_txtTitle");
            _textContent = GetChildComponent<StateText>("Label_txtNeirong");
            _listReward = GetChildComponent<KList>("List_jianglineirong");
            CloseBtn = GetChildComponent<KButton>("Container_Bg/Button_close");
            _goReward = GetChild("Label_txtBaoxiangjiangli");

            MogoUtils.AdaptScreen(GetChildComponent<StateImage>("Container_Bg/ScaleImage_sharedZhezhao"));
            AddListener();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.DreamlandStationTips; }
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
        }

        private void RemoveListener()
        {
        }

        public override void OnClose()
        {
        }

        public override void OnShow(object data)
        {
            DreamlandStationTipsWrapper wrapper = data as DreamlandStationTipsWrapper;
            Refresh(wrapper.DreamlandInfo, wrapper.StationId, wrapper.IsFinish);
        }

        public void Refresh(DreamlandInfo dreamlandInfo, int stationId, bool isFinish)
        {
            Visible = true;
            string title = dreamland_explore_station_helper.GetTitle(dreamlandInfo.Id, stationId);
            List<BaseItemData> rewardDataList = dreamland_explore_reward_helper.GetBaseItemDataList(dreamlandInfo.Id,
                PlayerAvatar.Player.level, dreamlandInfo.DragonQuality, stationId);
            if (isFinish)
            {
                _textTitle.CurrentText.text = MogoLanguageUtil.GetContent(39031, title);
                _goReward.SetActive(false);
                _listReward.Visible = false;
            }
            else
            {
                _textTitle.CurrentText.text = MogoLanguageUtil.GetContent(39030, title);
                _goReward.SetActive(true);
                _listReward.Visible = true;
                _listReward.SetDataList<RewardItem>(rewardDataList);
            }
            _textContent.CurrentText.text = dreamland_explore_station_helper.GetDesc(dreamlandInfo.Id, stationId);
            
        }
    }
}
