﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/12 14:48:45
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using UnityEngine;
using Common.Utils;
using ModuleCommonUI;
using System;
using Mogo.Util;

namespace ModuleDreamland
{
    public class DreamlandPanel : BasePanel
    {
        private List<DreamlandInfo> _dreamlandInfoList;
        private int _currentIndex = 0;

        private KToggleGroup _toggleGroup;
        private List<KToggle> _toggleList;
        private GameObject _goLine;
        private DreamlandScene _scene;
        private DreamlandUnexplore _unexplore;
        private DreamlandExploring _exploring;
        private DreamlandPlayerInfoView _playerInfo;
        private DreamlandBoxView _boxView;
        private List<DreamlandTitleGrid> _titleGridList;
        private Dictionary<int, GameObject> _titleLineDict;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Dreamland; }
        }

        protected override void Awake()
        {
            RequestNewRecord();
            InitToggleGroup();
            _scene = gameObject.AddComponent<DreamlandScene>();
            _unexplore = AddChildComponent<DreamlandUnexplore>("Container_feilong");
            _exploring = AddChildComponent<DreamlandExploring>("Container_dajie");
            _playerInfo = AddChildComponent<DreamlandPlayerInfoView>("Container_dajiexinxi");
            _boxView = AddChildComponent<DreamlandBoxView>("Container_lingqubaoxiang");
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
        }

        private static bool requestOnce = true;
        private void RequestNewRecord()
        {
            if (requestOnce)
            {
                requestOnce = false;
                DreamlandManager.Instance.RequestNewMission();
            }
        }

        private void InitToggleGroup()
        { 
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            List<KToggle> toggleList = _toggleGroup.GetToggleList();
            _toggleList = toggleList;
            _titleGridList = new List<DreamlandTitleGrid>();
            for (int i = 0; i < toggleList.Count; i++)
            {
                DreamlandTitleGrid grid = toggleList[i].gameObject.AddComponent<DreamlandTitleGrid>();
                _titleGridList.Add(grid);
            }
            _titleLineDict = new Dictionary<int, GameObject>();
            for (int i = 0; i < toggleList.Count; i++)
            {
                _titleLineDict.Add(i + 1, GetChild(string.Format("Container_line/Container_line{0}", i)));
            }
            _goLine = GetChild("Container_line");
        }

        private void AddListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectIndexChanged);
            _exploring.onFinish.AddListener(OnExploringFinish);
            EventDispatcher.AddEventListener(DreamlandEvents.ReceiveStartExplore, ReceiveStartExplore);
            EventDispatcher.AddEventListener(DreamlandEvents.ReceiveFinishExplore, ReceiveFinishExplore);
            EventDispatcher.AddEventListener(DreamlandEvents.RefreshChapter, Refresh);
            EventDispatcher.AddEventListener<PbDreamlandEventList>(DreamlandEvents.ReceiveMissionRecord, ReceiveMissionRecord);
            EventDispatcher.AddEventListener<PbDreamlandBoxInfoList>(DreamlandEvents.ReceiveDreamlandBoxInfo, ReceiveDreamlandBoxInfo);
            EventDispatcher.AddEventListener<uint>(DreamlandEvents.ClickDragon, ClickDragon);
            EventDispatcher.AddEventListener<DreamlandPlayerInfo>(DreamlandEvents.ReceivePlayerInfo, ReceivePlayerInfo);
            EventDispatcher.AddEventListener<PbDreamlandItemList>(DreamlandEvents.ReceiveGetYestodayReward, ReceiveGetYestodayReward);
            EventDispatcher.AddEventListener<int, bool>(DreamlandEvents.ShowStationTips, OnShowStationTips);
        }

        private void RemoveListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectIndexChanged);
            _exploring.onFinish.RemoveListener(OnExploringFinish);
            EventDispatcher.RemoveEventListener(DreamlandEvents.ReceiveStartExplore, ReceiveStartExplore);
            EventDispatcher.RemoveEventListener(DreamlandEvents.ReceiveFinishExplore, ReceiveFinishExplore);
            EventDispatcher.RemoveEventListener(DreamlandEvents.RefreshChapter, Refresh);
            EventDispatcher.RemoveEventListener<PbDreamlandEventList>(DreamlandEvents.ReceiveMissionRecord, ReceiveMissionRecord);
            EventDispatcher.RemoveEventListener<PbDreamlandBoxInfoList>(DreamlandEvents.ReceiveDreamlandBoxInfo, ReceiveDreamlandBoxInfo);
            EventDispatcher.RemoveEventListener<uint>(DreamlandEvents.ClickDragon, ClickDragon);
            EventDispatcher.RemoveEventListener<DreamlandPlayerInfo>(DreamlandEvents.ReceivePlayerInfo, ReceivePlayerInfo);
            EventDispatcher.RemoveEventListener<PbDreamlandItemList>(DreamlandEvents.ReceiveGetYestodayReward, ReceiveGetYestodayReward);
            EventDispatcher.RemoveEventListener<int, bool>(DreamlandEvents.ShowStationTips, OnShowStationTips);
        }

        private void OnExploringFinish()
        {
            _playerInfo.Visible = false;
            Refresh();
        }

        public override void OnShow(object data)
        {
            AddListener();
            _playerInfo.Visible = false;
            Refresh();
        }

        public override void OnClose()
        {
            RemoveListener();
        }

        private void OnSelectIndexChanged(KToggleGroup toggleGroup, int index)
        {
            _currentIndex = index;
            RefreshChapter(_dreamlandInfoList);
        }

        private void ReceiveGetYestodayReward(PbDreamlandItemList itemList)
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.DreamlandPop);
            string rewardContent = "";
            for (int i = 0; i < itemList.item.Count; i++)
            {
                PBDreamlandItem item = itemList.item[i];
                rewardContent = string.Format("{0}{1}{2}", rewardContent, item.item_sum, item_helper.GetName((int)item.item_id));
                if (i != itemList.item.Count - 1)
                {
                    rewardContent = string.Concat(rewardContent, "、");
                }
                else
                {
                    rewardContent = string.Concat(rewardContent, "！");
                }
            }
            MessageBox.Show(true, MessageBox.DEFAULT_TITLE, string.Format(MogoLanguageUtil.GetContent(37857), rewardContent), true);
            RefreshChapter(_dreamlandInfoList);
        }

        private void OnShowStationTips(int stationId, bool isFinish)
        {
            DreamlandInfo dreamlandInfo = _dreamlandInfoList[_currentIndex];
            UIManager.Instance.ShowPanel(PanelIdEnum.DreamlandStationTips,
                new DreamlandStationTipsWrapper(dreamlandInfo, stationId, isFinish));
        }

        private void ReceiveMissionRecord(PbDreamlandEventList recordList)
        {
            DreamlandInfo dreamlandInfo = _dreamlandInfoList[_currentIndex];
            DreamlandPopDataWrapper wrapper = new DreamlandPopDataWrapper(DreamlandPopType.missionRecord, dreamlandInfo, recordList);
            switch ((DreamlandContext)recordList.context)
            {
                case DreamlandContext.main:
                    UIManager.Instance.ShowPanel(PanelIdEnum.DreamlandPop, wrapper);
                    break;
                case DreamlandContext.exploreFinish:
                    EventDispatcher.TriggerEvent<DreamlandPopDataWrapper>(DreamlandEvents.ShowMissionRecord, wrapper);
                    break;
            }
        }

        private void ReceiveDreamlandBoxInfo(PbDreamlandBoxInfoList retrieveRewardList)
        {
            DreamlandPopDataWrapper wrapper = new DreamlandPopDataWrapper(DreamlandPopType.retrieveReward, retrieveRewardList);
            UIManager.Instance.ShowPanel(PanelIdEnum.DreamlandPop, wrapper);
        }

        private void ClickDragon(uint index)
        {
            if (_playerInfo.Visible == false)
            {
                DreamlandInfo dreamlandInfo = _dreamlandInfoList[_currentIndex];
                DreamlandManager.Instance.RequestPlayerDetailInfo(dreamlandInfo.Id, dreamlandInfo.PlayerDataList[(int)index].Dbid);
            }
        }

        private void ReceivePlayerInfo(DreamlandPlayerInfo playerInfo)
        {
            DreamlandInfo dreamlandInfo = _dreamlandInfoList[_currentIndex];
            _playerInfo.SetData(dreamlandInfo, playerInfo);
        }

        private Action _refreshCallback;
        private void ReceiveStartExplore()
        {
            _refreshCallback = StartCallback;
            Refresh();
        }

        private void StartCallback()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.DreamlandPop);
        }

        private void ReceiveFinishExplore()
        {
            Refresh();
            DreamlandPopDataWrapper wrapper = new DreamlandPopDataWrapper(DreamlandPopType.exploreFinish, _dreamlandInfoList[_currentIndex]);
            UIManager.Instance.ShowPanel(PanelIdEnum.DreamlandPop, wrapper);
        }

        private void Refresh()
        {
            DreamlandData dreamlandData = PlayerDataManager.Instance.DreamlandData;
            List<DreamlandInfo> dreamlandInfoList = dreamlandData.GetDreamlandInfoList();
            _dreamlandInfoList = dreamlandInfoList;
            DreamlandInfo dreamlandInfo = dreamlandInfoList[_currentIndex];
            _scene.LoadScene(dreamlandInfo, LoadSceneCallback);
        }

        private void LoadSceneCallback()
        {
            RefreshTitle(_dreamlandInfoList);
            RefreshChapter(_dreamlandInfoList);
            if (_refreshCallback != null)
            {
                _refreshCallback.Invoke();
                _refreshCallback = null;
            }
        }

        private void RefreshTitle(List<DreamlandInfo> dreamlandInfoList)
        {
            UnregisterAllToggle();
            RegisterToggle(dreamlandInfoList);
            MogoLayoutUtils.RecalculateSize(_toggleGroup.gameObject);
            MogoLayoutUtils.SetCenter(_toggleGroup.gameObject);
        }

        private void UnregisterAllToggle()
        {
            List<KToggle> toggleList = _toggleGroup.GetToggleList();
            for (int i = 0; i < toggleList.Count; i++)
            {
                _toggleGroup.UnregisterToggle(toggleList[i]);
            }
        }

        private void RegisterToggle(List<DreamlandInfo> dreamlandInfoList)
        {
            List<string> nameList = GetChapterNameList(dreamlandInfoList);
            for (int i = 0; i < _titleGridList.Count; i++)
            {
                if (i < nameList.Count)
                {
                    _toggleGroup.RegisterToggle(_toggleList[i]);
                    DreamlandTitleGrid dreamlandTitleGrid = _titleGridList[i];
                    dreamlandTitleGrid.RefreshLabel(nameList[i]);
                    _toggleList[i].Visible = true;
                }
                else
                {
                    _toggleList[i].Visible = false;
                }
            }
            foreach (GameObject go in _titleLineDict.Values)
            {
                go.SetActive(false);
            }
            _titleLineDict[nameList.Count].SetActive(true);
        }

        private List<string> GetChapterNameList(List<DreamlandInfo> dreamlandInfoList)
        {
            List<string> nameList = new List<string>();
            for (int i = 0; i < dreamlandInfoList.Count; i++)
            {
                DreamlandInfo dreamlandInfo = dreamlandInfoList[i];
                nameList.Add(dreamland_explore_base_helper.GetName(dreamlandInfo.Id));
            }
            return nameList;
        }

        private void RefreshChapter(List<DreamlandInfo> dreamlandInfoList)
        {
            DreamlandInfo dreamlandInfo = dreamlandInfoList[_currentIndex];
            HideAll();
            DreamlandChapterState state = dreamlandInfo.GetState();
            switch (state)
            {
            case DreamlandChapterState.exploring:
                _exploring.Data = dreamlandInfo;
                break;
            case DreamlandChapterState.unexplore:
                _unexplore.Data = dreamlandInfo;
                _goLine.SetActive(true);
                _toggleGroup.Visible = true;
                break;
            case DreamlandChapterState.haveReward:
                _boxView.Data = dreamlandInfo;
                break;
            }
        }

        private void HideAll()
        {
            _boxView.Visible = false;
            _unexplore.Visible = false;
            _exploring.Hide();
            _goLine.SetActive(false);
            _toggleGroup.Visible = false;
        }
    }
}
