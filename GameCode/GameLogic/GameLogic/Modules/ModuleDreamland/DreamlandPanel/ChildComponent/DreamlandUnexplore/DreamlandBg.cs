﻿using Common.Utils;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleDreamland
{
    public class DreamlandBg : KContainer
    {
        protected override void Awake()
        {
            MogoAtlasUtils.AddSprite(GetChild("Container_bg0"), "dreamlandBg0");
            MogoAtlasUtils.AddSprite(GetChild("Container_bg1"), "dreamlandBg1");
            MogoAtlasUtils.AddSprite(GetChild("Container_bg2"), "dreamlandBg2");
            MogoAtlasUtils.AddSprite(GetChild("Container_bg3"), "dreamlandBg3");
        }
    }
}
