﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/12 15:15:36
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Global;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using UnityEngine;
using Common.ExtendComponent;
using Common.Events;
using MogoEngine.Events;

namespace ModuleDreamland
{
    public enum DreamlandStationState
    {
        unstart,
        finished,
        haveReward,
    }

    public class DreamlandUnexplorePath : KContainer
    {
        private const int MaxStationNum = 10;

        private DreamlandInfo _dreamlandInfo;

        private List<DreamlandUnexploreStation> _stationList;
        private List<DreamlandUnexploreLine> _lineList;

        protected override void Awake()
        {
            InitStationList();
            InitLineList();
        }

        private void InitStationList()
        {
            _stationList = new List<DreamlandUnexploreStation>();
            for (int i = 0; i < MaxStationNum; i++)
            {
                DreamlandUnexploreStation station = AddChildComponent<DreamlandUnexploreStation>(string.Format("Container_feilongdasai{0}", i));
                station.UnfinishShowTips.AddListener(OnUnfinishShowTips);
                station.FinishedShowTips.AddListener(OnFinishedShowTips);
                station.Id = i;
                _stationList.Add(station);
            }
        }

        private void InitLineList()
        {
            _lineList = new List<DreamlandUnexploreLine>();
            for (int i = 0; i < MaxStationNum - 1; i++)
            {
                _lineList.Add(AddChildComponent<DreamlandUnexploreLine>(string.Format("Container_xian0{0}", i)));
            }
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < _stationList.Count; i++)
            {
                _stationList[i].UnfinishShowTips.RemoveListener(OnUnfinishShowTips);
                _stationList[i].FinishedShowTips.RemoveListener(OnFinishedShowTips);
            }
        }

        private void OnUnfinishShowTips(int stationId)
        {
            EventDispatcher.TriggerEvent<int, bool>(DreamlandEvents.ShowStationTips, stationId, false);
        }

        private void OnFinishedShowTips(int stationId)
        {
            EventDispatcher.TriggerEvent<int, bool>(DreamlandEvents.ShowStationTips, stationId, true);
        }

        public void SetData(DreamlandInfo dreamlandInfo)
        {
            _dreamlandInfo = dreamlandInfo;
            RefreshStationList(dreamlandInfo);
        }

        private void RefreshStationList(DreamlandInfo dreamlandInfo)
        {
            for (int i = 0; i < _stationList.Count; i++)
            {
                DreamlandUnexploreStation station = _stationList[i];
                DreamlandStationState state;
                if (station.Id > dreamlandInfo.CurrentStationId)
                {
                    state = DreamlandStationState.unstart;
                }
                else if (station.Id < dreamlandInfo.CurrentStationId)
                {
                    state = DreamlandStationState.finished;
                }
                else
                {
                    if (dreamlandInfo.EndTime == 0)
                    {
                        state = DreamlandStationState.unstart;
                    }
                    else// if (dreamlandInfo.EndTime <= Global.serverTimeStampSecond)
                    {
                        state = DreamlandStationState.haveReward;
                    }
                }
                station.Data = state;
                if (i != 0)
                {
                    DreamlandUnexploreLine line = _lineList[i - 1];
                    line.SetBright(state == DreamlandStationState.haveReward || state == DreamlandStationState.finished);
                }
            }
        }
    }
}
