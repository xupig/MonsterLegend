﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/14 14:03:39
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.UIComponent;

namespace ModuleDreamland
{
    public class DreamlandTitleGrid : KContainer
    {
        private StateText _textSelected;
        private StateText _textUnselected;

        protected override void Awake()
        {
            _textSelected = GetChildComponent<StateText>("checkmark/label");
            _textUnselected = GetChildComponent<StateText>("image/label");
        }

        public void RefreshLabel(string name)
        {
            _textSelected.ChangeAllStateText(name);
            _textUnselected.ChangeAllStateText(name);
        }
    }
}
