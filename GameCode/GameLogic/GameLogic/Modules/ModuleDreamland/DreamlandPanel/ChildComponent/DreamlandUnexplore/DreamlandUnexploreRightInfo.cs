﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/12 15:10:57
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Mgrs;

namespace ModuleDreamland
{
    public class DreamlandUnexploreRightInfo : KContainer
    {
        private DreamlandInfo _dreamlandInfo;

        private StateText _textLevel;
        private StateText _textRobTimes;
        private StateText _textProgress;
        private ArtNumberList _artNumberList;

        private KButton _btnAddRobTimes;
        private KButton _btnShowRecord;
        private KButton _btnTurnBack;
        private StateImage _imageNewRecord;

        protected override void Awake()
        {
            InitText();
            InitBtn();
            _imageNewRecord = GetChildComponent<StateImage>("Button_shijianjilu/image3");
        }

        private void InitText()
        {
            _textLevel = GetChildComponent<StateText>("Label_txtDengji");
            _artNumberList = AddChildComponent<ArtNumberList>("List_zhandouli");
            _textRobTimes = GetChildComponent<StateText>("Label_txtDajie");
            _textProgress = GetChildComponent<StateText>("Label_txtDangqian");
        }

        private void InitBtn()
        {
            _btnAddRobTimes = GetChildComponent<KButton>("Button_tianjia");
            _btnShowRecord = GetChildComponent<KButton>("Button_shijianjilu");
            _btnTurnBack = GetChildComponent<KButton>("Button_yueguangbaoh");
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnAddRobTimes.onClick.AddListener(OnAddRobTimes);
            _btnShowRecord.onClick.AddListener(OnShowRecord);
            _btnTurnBack.onClick.AddListener(OnRetrieveReward);
            EventDispatcher.AddEventListener(DreamlandEvents.NewRecordChanged, RefreshNewRecord);
        }

        private void RemoveListener()
        {
            _btnAddRobTimes.onClick.RemoveListener(OnAddRobTimes);
            _btnShowRecord.onClick.RemoveListener(OnShowRecord);
            _btnTurnBack.onClick.RemoveListener(OnRetrieveReward);
            EventDispatcher.RemoveEventListener(DreamlandEvents.NewRecordChanged, RefreshNewRecord);
        }

        private void OnAddRobTimes()
        {
            BaseItemData baseItemData = dreamland_explore_base_helper.GetBuyRobTimesCost(_dreamlandInfo.Id,
                _dreamlandInfo.TotalRobTimes - dreamland_explore_base_helper.GetDailyMaxRobTimes(_dreamlandInfo.Id) + 1);
            string enoughBindDiamondContent = string.Format(MogoLanguageUtil.GetContent(37813), baseItemData.StackCount, item_helper.GetName(baseItemData.Id));
            string notEnoughBindDiamondContent = MogoLanguageUtil.GetContent(39020);
            MogoTipsUtils.ShowBindDiamondTips(baseItemData.StackCount, enoughBindDiamondContent, notEnoughBindDiamondContent, ConfirmAction);
        }

        private void ConfirmAction()
        {
            DreamlandManager.Instance.RequestBuyRobTimes(_dreamlandInfo.Id);
        }

        private void OnShowRecord()
        {
            DreamlandManager.Instance.RequestMissionInfo((int)DreamlandContext.main);
        }

        private void OnRetrieveReward()
        {
            DreamlandManager.Instance.RequestRetrieveRewardInfo();
        }

        public DreamlandInfo Data
        {
            set
            {
                _dreamlandInfo = value;
                if (value.IsYestodayData)
                {
                    Visible = false;
                }
                else
                {
                    Visible = true;
                    RefreshLevel();
                    RefreshFightValue();
                    RefreshRobTimes(value);
                    RefreshProgress(value);
                    RefreshBtn(value);
                    RefreshNewRecord();
                }
            }
        }

        private void RefreshLevel()
        {
            _textLevel.CurrentText.text = PlayerAvatar.Player.level.ToString();
        }

        private void RefreshFightValue()
        {
            _artNumberList.SetNumber((int)PlayerAvatar.Player.fight_force);
        }

        private void RefreshRobTimes(DreamlandInfo dreamlandInfo)
        {
            string leftTimesColorString = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GOLDEN, (dreamlandInfo.TotalRobTimes - dreamlandInfo.RobTimes).ToString());
            string maxTimesColorString = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, dreamlandInfo.TotalRobTimes.ToString());
            _textRobTimes.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37843), leftTimesColorString, maxTimesColorString);
        }

        private void RefreshProgress(DreamlandInfo dreamlandInfo)
        {
            int maxStationNum = dreamland_explore_station_helper.GetDreamlandChapterMaxStationNum(dreamlandInfo.Id);
            string currentStationNumColorString = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GOLDEN, dreamlandInfo.GetCurrentTimesWhenUnfinish().ToString());
            string maxStationNumColorString = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, maxStationNum.ToString());
            _textProgress.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37844), currentStationNumColorString, maxStationNumColorString);
        }

        private void RefreshBtn(DreamlandInfo dreamlandInfo)
        {
            _btnTurnBack.Visible = false;
        }

        private void RefreshNewRecord()
        {
            DreamlandData dreamlandData = PlayerDataManager.Instance.DreamlandData;
            if (dreamlandData.HaveNewRecord)
            {
                _imageNewRecord.Alpha = 1f;
            }
            else
            {
                _imageNewRecord.Alpha = 0f;
            }
        }
    }
}
