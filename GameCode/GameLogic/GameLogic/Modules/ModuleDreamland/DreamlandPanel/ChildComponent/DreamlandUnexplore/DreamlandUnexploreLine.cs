﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleDreamland
{
    public class DreamlandUnexploreLine : KContainer
    {
        private List<DreamlandUnexplorePoint> _pointList;

        protected override void Awake()
        {
            _pointList = new List<DreamlandUnexplorePoint>();
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                GameObject goChild = gameObject.transform.GetChild(i).gameObject;
                _pointList.Add(goChild.AddComponent<DreamlandUnexplorePoint>());
            }
        }

        public void SetBright(bool isBright)
        {
            for (int i = 0; i < _pointList.Count; i++)
            {
                _pointList[i].SetBright(isBright);
            }
        }
    }

    class DreamlandUnexplorePoint : KContainer
    {
        private GameObject _goBrightPoint;
        private GameObject _goDarkPoint;

        protected override void Awake()
        {
            _goBrightPoint = GetChild("Static_huanjingdian01");
            _goDarkPoint = GetChild("Static_huanjingdian00");
        }

        public void SetBright(bool isBright)
        {
            _goBrightPoint.SetActive(false);
            _goDarkPoint.SetActive(false);
            if (isBright)
            {
                _goBrightPoint.SetActive(true);
            }
            else
            {
                _goDarkPoint.SetActive(true);
            }
        }
    }
}
