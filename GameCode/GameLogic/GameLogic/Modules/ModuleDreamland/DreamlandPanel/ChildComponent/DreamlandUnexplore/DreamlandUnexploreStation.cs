﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/12 15:54:28
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using Common.ExtendComponent;
using UnityEngine;

namespace ModuleDreamland
{
    public class DreamlandUnexploreStation : KContainer
    {
        public int Id;

        private KButton _btnUnfinish;
        private KButton _btnGotReward;
        private KParticle _particle;

        public KComponentEvent<int> UnfinishShowTips = new KComponentEvent<int>();
        public KComponentEvent<int> FinishedShowTips = new KComponentEvent<int>();

        protected override void Awake()
        {
            _btnUnfinish = GetChildComponent<KButton>("Button_weiwancheng");
            _btnGotReward = GetChildComponent<KButton>("Button_lingqu");
            _particle = AddChildComponent<KParticle>("Button_lingqu/fx_ui_14_1_yanwutexiao");
            _particle.transform.localPosition = new Vector3(21.66f, -14.37f, 0f);

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnUnfinish.onClick.AddListener(OnUnfinishClick);
            _btnGotReward.onClick.AddListener(OnGotRewardClick);
        }

        private void RemoveListener()
        {
            _btnUnfinish.onClick.RemoveListener(OnUnfinishClick);
            _btnGotReward.onClick.RemoveListener(OnGotRewardClick);
        }

        private void OnUnfinishClick()
        {
            UnfinishShowTips.Invoke(Id);
        }

        private void OnGotRewardClick()
        {
            FinishedShowTips.Invoke(Id);
        }

        public DreamlandStationState Data
        {
            set
            {
                HideAll();
                ShowBtn(value);
            }
        }

        private void HideAll()
        {
            _btnUnfinish.Visible = false;
            _btnGotReward.Visible = false;
        }

        private void ShowBtn(DreamlandStationState state)
        {
            switch (state)
            {
            case DreamlandStationState.unstart:
                _btnUnfinish.Visible = true;
                break;
            case DreamlandStationState.finished:
            case DreamlandStationState.haveReward:
                _btnGotReward.Visible = true;
                break;
            }
        }

        public void AddParticle(KParticle boxParticle)
        {
            boxParticle.transform.SetParent(transform, false);
            boxParticle.transform.localPosition = new Vector3(16, -12, 0);
            boxParticle.transform.localScale = Vector3.one;
            boxParticle.Play(true);
        }
    }
}
