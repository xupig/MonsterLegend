﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/19 15:07:43
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Game.Asset;
using Game.UI.UIComponent;
using UnityEngine;

namespace ModuleDreamland
{
    public class DreamlandDragonList : KContainer
    {
        private const int MAX_DRAGON_NUM = 5;

        private List<DreamlandDragon> _dragonList;

        protected override void Awake()
        {
            _dragonList = new List<DreamlandDragon>();
            for (int i = 0; i < MAX_DRAGON_NUM; i++)
            {
                DreamlandDragon dragon = AddChildComponent<DreamlandDragon>(string.Format("Container_feilong{0}", i));
                dragon.Index = i;
                _dragonList.Add(dragon);
            }
        }

        public DreamlandInfo Data
        {
            set
            {
                for (int i = 0; i < _dragonList.Count; i++)
                {
                    DreamlandDragon dragon = _dragonList[i];
                    if (i < value.PlayerDataList.Count)
                    {
                        dragon.Data = value.PlayerDataList[i];
                    }
                    else
                    {
                        dragon.DestroyDragon();
                        dragon.Visible = false;
                    }
                }
            }
        }

        public void HideDragonList()
        {
            for (int i = 0; i < _dragonList.Count; i++)
            {
                DreamlandDragon dragon = _dragonList[i];
                dragon.DestroyDragon();
            }
        }
    }
}
