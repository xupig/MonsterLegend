﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/19 14:20:33
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Events;
using Game.Asset;
using Game.UI.UIComponent;
using MogoEngine.Events;
using UnityEngine;
using UnityEngine.EventSystems;
using GameMain.GlobalManager;
using Common.Global;
using Common.Utils;
using Common.ServerConfig;

namespace ModuleDreamland
{
    public class DreamlandDragon : KContainer
    {
        public static List<Vector3> PositionOffsetList = new List<Vector3>()
        {
            new Vector3(-130f, -10f, 0f),
            new Vector3(-150f, -10f, 0f),
            new Vector3(-100f, 0f, 0f),
            new Vector3(-100f, -30f, 0f),
            new Vector3(-150f, -50f, 0f),
        };

        private const string PathTemplate = "Characters/RaceNPC/FeiLong/NPC_206{0}.prefab";
        private const string DragonPositionTemplate = "NPC_2061_0{0}";

        public int Index;

        private StateText _textName;
        private GameObject _goDragon;

        protected override void Awake()
        {
            _textName = GetChildComponent<StateText>("Label_txtName");
        }

        public DreamlandPlayerInfo Data
        {
            set
            {
                Visible = true;
                string path = string.Format(PathTemplate, value.DragonQuality - 1);
                ObjectPool.Instance.GetGameObject(path, LoadFinish);
                _textName.CurrentText.text = string.Format("Lv{0}   {1}", value.Level, value.Name);
            }
        }

        public void DestroyDragon()
        {
            if (_goDragon != null)
            {
                Destroy(_goDragon);
                _goDragon = null;
            }
        }

        private void LoadFinish(GameObject go)
        {
            if (InitDragon(go, Index))
            {
                RefreshPosition();
            }
            else
            {
                DestroyDragon();
            }
        }

        private bool InitDragon(GameObject go, int Index)
        {
            DestroyDragon();
            _goDragon = go;
            GameObject scene = GameObject.Find(DreamlandScene.EXPLORING_SCENE_NAME);
            //有可能场景被销毁了
            if (scene == null)
            {
                return false;
            }
            DreamlandDragonTrigger trigger = go.AddComponent<DreamlandDragonTrigger>();
            trigger.id = (uint)Index;
            go.name = string.Format("Feilong{0}", Index);
            go.transform.SetParent(scene.transform);
            go.transform.localScale = Vector3.one;
            go.transform.localRotation = Quaternion.Euler(0, 0, 0);
            Vector3 position = scene.transform.FindChild(string.Format(DragonPositionTemplate, Index + 1)).localPosition;
            go.transform.localPosition = position;
            return true;
        }

        private void RefreshPosition()
        {
            GameObject scene = GameObject.Find(DreamlandScene.EXPLORING_SCENE_NAME);
            Camera camera = scene.GetComponentInChildren<Camera>();
            Vector3 position = _goDragon.transform.position;
            Vector3 screenPosition = camera.WorldToScreenPoint(position);
            RectTransform rect = gameObject.transform.parent.GetComponent<RectTransform>();
            transform.localPosition = MogoGameObjectHelper.ScreenPointToLocalPointInRectangle(rect, screenPosition) + PositionOffsetList[Index];
        }
    }
}
