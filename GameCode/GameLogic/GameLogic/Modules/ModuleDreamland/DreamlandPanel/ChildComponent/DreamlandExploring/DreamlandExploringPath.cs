﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/12 16:53:39
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Game.UI.UIComponent;
using UnityEngine;

namespace ModuleDreamland
{
    public class DreamlandExploringPath : KContainer
    {
        private const int MAX_STATION_NUM = 10;

        private List<DreamlandExploringStation> _stationList;


        protected override void Awake()
        {
            InitStationList();
        }

        private void InitStationList()
        {
            _stationList = new List<DreamlandExploringStation>();
            for (int i = 0; i < MAX_STATION_NUM; i++)
            {
                DreamlandExploringStation station = AddChildComponent<DreamlandExploringStation>(string.Format("Container_jiedian{0}", i));
                _stationList.Add(station);
            }
        }

        public DreamlandInfo Data
        {
            set
            {
                for (int i = 0; i < _stationList.Count; i++)
                {
                    if(i >= value.CurrentStationId)
                    {
                        _stationList[i].Data = DreamlandStationState.unstart;
                    }
                    else
                    {
                        _stationList[i].Data = DreamlandStationState.finished;
                    }
                }
            }
        }
    }
}
