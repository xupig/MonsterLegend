﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/22 21:21:56
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Events;
using GameMain.GlobalManager;
using MogoEngine.Events;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleDreamland
{
    public class DreamlandDragonTrigger : ActorSelectTrigger
    {
        protected override void OnTouchEvent()
        {
            if (EventSystem.current.currentSelectedGameObject != null)
            {
                return;
            }
            EventDispatcher.TriggerEvent<uint>(DreamlandEvents.ClickDragon, this.id);
        }
    }
}
