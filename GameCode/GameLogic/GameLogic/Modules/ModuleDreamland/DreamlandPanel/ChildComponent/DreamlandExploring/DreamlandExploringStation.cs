﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/12 17:01:52
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.UIComponent;

namespace ModuleDreamland
{
    public class DreamlandExploringStation : KContainer
    {
        private StateImage _imageFinish;
        private StateImage _imageUnfinish;

        protected override void Awake()
        {
            _imageFinish = GetChildComponent<StateImage>("Image_shi");
            _imageUnfinish = GetChildComponent<StateImage>("Image_kong");
        }

        public DreamlandStationState Data
        {
            set
            {
                if (value != DreamlandStationState.unstart)
                {
                    _imageUnfinish.Alpha = 0f;
                }
                else
                {
                    _imageUnfinish.Alpha = 1f;
                }
            }
        }
    }
}
