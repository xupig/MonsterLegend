﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/12 16:32:27
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using Common.Utils;

namespace ModuleDreamland
{
    public class DreamlandExploringRightInfo : DreamlandUnexploreRightInfo
    {
        private int _landId;

        private KButton _btnRefresh;
        private DreamlandExploringPath _path;

        protected override void Awake()
        {
            base.Awake();
            _btnRefresh = GetChildComponent<KButton>("Button_shuaxin");
            _path = AddChildComponent<DreamlandExploringPath>("Container_luxian");

            AddListener();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            RemoveListener();
        }

        private void AddListener()
        {
            _btnRefresh.onClick.AddListener(OnRefresh);
        }

        private void RemoveListener()
        {
            _btnRefresh.onClick.RemoveListener(OnRefresh);
        }

        private void OnRefresh()
        {
            BaseItemData baseItemData = dreamland_explore_base_helper.GetRefreshCost(_landId);
            MessageBox.Show(true, MessageBox.DEFAULT_TITLE, string.Format(MogoLanguageUtil.GetContent(37810), baseItemData.StackCount, item_helper.GetName(baseItemData.Id)), ConfirmAction);
        }

        private void ConfirmAction()
        {
            DreamlandManager.Instance.RequestPlayerRefresh(_landId);
        }

        public new DreamlandInfo Data
        {
            set
            {
                if (value.IsYestodayData)
                {
                    Visible = false;
                }
                else
                {
                    Visible = true;
                    base.Data = value;
                    _landId = value.Id;
                    _path.Data = value;
                }
            }
        }

    }
}
