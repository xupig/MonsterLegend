﻿using ACTSystem;
using UnityEngine;

namespace ModuleDreamland
{
    public class DreamlandCamera : MonoBehaviour
    {
        public Camera Camera = null;

        void Update()
        {
            if (Input.GetMouseButtonUp(0))
            {
                if (Camera == null) return;
                RaycastHit hit;
                Ray ray = Camera.ScreenPointToRay(Input.mousePosition);
                int layerMask = 1 << UnityEngine.LayerMask.NameToLayer("Actor");
                bool isok = Physics.Raycast(ray, out hit, 100, layerMask);
                if (hit.transform == null) return;
                ActorSelectTrigger actorSelectTrigger = hit.transform.GetComponent<ActorSelectTrigger>();
                if (actorSelectTrigger != null)
                {
                    actorSelectTrigger.OnMouseButtonUp();
                }
            }
        }
    }
}
