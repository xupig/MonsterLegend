﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/12 15:03:32
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Global;
using Common.Utils;
using Game.Asset;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using UnityEngine;

namespace ModuleDreamland
{
    public class DreamlandExploring : KContainer
    {
        private uint _dragonTimerId = TimerHeap.INVALID_ID;
        private uint _robTimerId = TimerHeap.INVALID_ID;
        private DreamlandInfo _dreamlandInfo;

        private DreamlandExploringRightInfo _exploringInfo;
        private DreamlandDragonList _dragonList;

        private StateText _textRobCooldown;
        private KButton _btnClearCooldown;
        private GameObject _goRobCooldown;

        private StateText _textLeftTime;
        private KButton _btnFinishImmediate;
        private KButton _btnMinusTime;
        private GameObject _goDragonCooldown;

        public KComponentEvent onFinish = new KComponentEvent();

        protected override void Awake()
        {
            _dragonList = AddChildComponent<DreamlandDragonList>("Container_feilongliebiao");
            _exploringInfo = AddChildComponent<DreamlandExploringRightInfo>("Container_detail");

            _textRobCooldown = GetChildComponent<StateText>("Container_qingchu/Label_txtDajieshijian");
            _btnClearCooldown = GetChildComponent<KButton>("Container_qingchu/Button_qingchu");
            _goRobCooldown = GetChild("Container_qingchu");

            _textLeftTime = GetChildComponent<StateText>("Container_shengyushijian/Label_txtBisaishijianshijian");
            _btnFinishImmediate = GetChildComponent<KButton>("Container_shengyushijian/Button_lijiwancheng");
            _btnMinusTime = GetChildComponent<KButton>("Container_shengyushijian/Button_suoduanshijian");
            _btnMinusTime.Visible = false;
            _goDragonCooldown = GetChild("Container_shengyushijian");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        protected override void OnDisable()
        {
            RemoveRobTimer();
            RemoveDragonTimer();
        }

        private void AddListener()
        {
            _btnFinishImmediate.onClick.AddListener(OnFinishImmediate);
            _btnMinusTime.onClick.AddListener(OnMinusTime);
            _btnClearCooldown.onClick.AddListener(OnClearRobCooldown);
        }

        private void RemoveListener()
        {
            _btnFinishImmediate.onClick.RemoveListener(OnFinishImmediate);
            _btnMinusTime.onClick.RemoveListener(OnMinusTime);
            _btnClearCooldown.onClick.RemoveListener(OnClearRobCooldown);
        }

        private void OnFinishImmediate()
        {
            int second = (int)(_dreamlandInfo.EndTime - Global.serverTimeStampSecond);
            int muliple = Mathf.CeilToInt((float)second / 30);
            BaseItemData baseItemData = dreamland_explore_base_helper.GetFinishImmediateCost(_dreamlandInfo.Id);
            baseItemData.StackCount *= muliple;
            string enoughBindDiamondContent = string.Format(MogoLanguageUtil.GetContent(37807), baseItemData.StackCount, item_helper.GetName(baseItemData.Id));
            string notEnoughBindDiamondContent = MogoLanguageUtil.GetContent(39018);
            MogoTipsUtils.ShowBindDiamondTips(baseItemData.StackCount, enoughBindDiamondContent, notEnoughBindDiamondContent, FinishImmediateConfirmAction);
        }

        private void FinishImmediateConfirmAction()
        {
            DreamlandManager.Instance.RequestExploreFinishImmediate(_dreamlandInfo.Id);
        }

        private void OnMinusTime()
        {
            BaseItemData baseItemData = dreamland_explore_base_helper.GetMinusTimeCost(_dreamlandInfo.Id);
            string enoughBindDiamondContent = string.Format(MogoLanguageUtil.GetContent(37806), baseItemData.StackCount, item_helper.GetName(baseItemData.Id));
            string notEnoughBindDiamondContent = MogoLanguageUtil.GetContent(39019);
            MogoTipsUtils.ShowBindDiamondTips(baseItemData.StackCount, enoughBindDiamondContent, notEnoughBindDiamondContent, MinusTimeConfirmAction);
        }

        private void MinusTimeConfirmAction()
        {
            DreamlandManager.Instance.RequestExploreTimeMinus(_dreamlandInfo.Id);
        }

        private void OnClearRobCooldown()
        {
            BaseItemData baseItemData = dreamland_explore_base_helper.GetClearRobCooldownCost(_dreamlandInfo.Id);
            string enoughBindDiamondContent = string.Format(MogoLanguageUtil.GetContent(37831), baseItemData.StackCount, item_helper.GetName(baseItemData.Id));
            string notEnoughBindDiamondContent = MogoLanguageUtil.GetContent(39021);
            MogoTipsUtils.ShowBindDiamondTips(baseItemData.StackCount, enoughBindDiamondContent, notEnoughBindDiamondContent, ClearRobCooldownConfirmAction);
        }

        private void ClearRobCooldownConfirmAction()
        {
            DreamlandManager.Instance.RequestClearRobCooldown(_dreamlandInfo.Id);
        }

        public DreamlandInfo Data
        {
            set
            {
                Visible = true;
                _dreamlandInfo = value;
                _exploringInfo.Data = value;
                _dragonList.Data = value;
                RefreshRobCooldown(value);
                RefreshDragonCooldown(value);
            }
        }

        private void RefreshRobCooldown(DreamlandInfo dreamlandInfo)
        {
            int second = (int)(dreamlandInfo.NextCanRobTime - Global.serverTimeStampSecond);
            if ((dreamlandInfo.NextCanRobTime != 0)
                &&(second > 0))
            {
                _goRobCooldown.SetActive(true);
                AddRobTimer();
            }
            else
            {
                _goRobCooldown.SetActive(false);
            }
        }

        private void AddRobTimer()
        {
            if (_robTimerId == TimerHeap.INVALID_ID)
            {
                _robTimerId = TimerHeap.AddTimer(0, 1000, OnRobTick);
            }
        }

        private void RemoveRobTimer()
        {
            if (_robTimerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_robTimerId);
                _robTimerId = TimerHeap.INVALID_ID;
            }
        }

        private void OnRobTick()
        {
            int second = (int) (_dreamlandInfo.NextCanRobTime - Global.serverTimeStampSecond);
            string timeStamp = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_ORANGE, MogoTimeUtil.ToUniversalTime(second, "mm:ss"));
            _textRobCooldown.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37842), timeStamp);
            if (second <= 0)
            {
                _goRobCooldown.SetActive(false);
                RemoveRobTimer();
            }
        }

        private void RefreshDragonCooldown(DreamlandInfo dreamlandInfo)
        {
            int second = (int) (dreamlandInfo.EndTime - Global.serverTimeStampSecond);
            if (second > 0)
            {
                AddDragonTimer(second);
            }
        }

        private void AddDragonTimer(int second)
        {
            if (_dragonTimerId == TimerHeap.INVALID_ID)
            {
                _dragonTimerId = TimerHeap.AddTimer(0, 1000, OnDragonTick);
            }
        }

        private void RemoveDragonTimer()
        {
            if (_dragonTimerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_dragonTimerId);
                _dragonTimerId = TimerHeap.INVALID_ID;
            }
        }

        private void OnDragonTick()
        {
            int second = (int) (_dreamlandInfo.EndTime - Global.serverTimeStampSecond);
            string timeStamp = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, MogoTimeUtil.ToUniversalTime(second, "hh:mm:ss"));
            _textLeftTime.CurrentText.text = string.Format("{0}", timeStamp);
            if (second <= 0)
            {
                RemoveDragonTimer();
                onFinish.Invoke();
            }
        }

        public void Hide()
        {
            if (Visible)
            {
                Visible = false;
                _dragonList.HideDragonList();
            }
        }
    }
}
