﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/12 15:05:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Data;
using Common.Global;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using Common.Utils;
using Common.ServerConfig;
using Mogo.Util;

namespace ModuleDreamland
{
    public class DreamlandPlayerInfoView : KContainer
    {
        private DreamlandPlayerInfo _playerInfo;
        private DreamlandInfo _dreamlandInfo;

        private KButton _btnClose;
        private KButton _btnRob;
        private StateText _textName;
        private StateText _textLevel;
        private StateText _textFightValue;
        private StateText _textGuild;
        private StateText _textDragonType;
        private StateText _textState;
        private StateText _textRobTimes;
        private StateText _textRewardGold;
        private StateText _textRewardExp;

        protected override void Awake()
        {
            _btnClose = GetChildComponent<KButton>("Button_close");
            _btnRob = GetChildComponent<KButton>("Button_dajie");

            _textName = GetChildComponent<StateText>("Container_dajieneirong/Label_txtName");
            _textLevel = GetChildComponent<StateText>("Container_dajieneirong/Label_txtLevel");
            _textFightValue = GetChildComponent<StateText>("Container_dajieneirong/Label_txtFightValue");
            _textGuild = GetChildComponent<StateText>("Container_dajieneirong/Label_txtGuide");
            _textDragonType = GetChildComponent<StateText>("Container_dajieneirong/Label_txtfeilongType");
            _textState = GetChildComponent<StateText>("Container_dajieneirong/Label_txtState");
            _textRobTimes = GetChildComponent<StateText>("Container_dajieneirong/Label_txtDajieTimes");
            _textRewardGold = GetChildComponent<StateText>("Container_dajieneirong/Label_txtjianglijinbi");
            _textRewardExp = GetChildComponent<StateText>("Container_dajieneirong/Label_txtjianglijingyan");

            MogoUtils.AdaptScreen(GetChildComponent<StateImage>("ScaleImage_sharedZhezhao"));
            GetChild("ScaleImage_sharedZhezhao").SetActive(false);

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(OnClose);
            _btnRob.onClick.AddListener(OnRob);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(OnClose);
            _btnRob.onClick.RemoveListener(OnRob);
        }

        private void OnRob()
        {
            CopyLogicManager.Instance.RequestEnterCopy(ChapterType.Single, RequestEnterCopy);
        }

        private void RequestEnterCopy()
        {
            if (_playerInfo.BeRobedByMeTimes >= 1 && _playerInfo.BeRobedTimes < dreamland_explore_base_helper.GetOnceBeRobedMaxTimes(_dreamlandInfo.Id))
            {
                BaseItemData baseItemData = dreamland_explore_base_helper.GetAttackSameTargetCost(_dreamlandInfo.Id, _playerInfo.BeRobedByMeTimes + 1);
                string enoughBindDiamondContent = string.Format(MogoLanguageUtil.GetContent(37829, baseItemData.StackCount, item_helper.GetName(baseItemData.Id)));
                string notEnoughBindDiamondContent = MogoLanguageUtil.GetContent(39017);
                MogoTipsUtils.ShowBindDiamondTips(baseItemData.StackCount, enoughBindDiamondContent, notEnoughBindDiamondContent, RobConfirmAction);
            }
            else
            {
                DreamlandManager.Instance.RequestRob(_dreamlandInfo.Id, _playerInfo.Dbid);
            }
        }

        private void RobConfirmAction()
        {
            DreamlandManager.Instance.RequestRob(_dreamlandInfo.Id, _playerInfo.Dbid);
        }

        private void OnClose()
        {
            Visible = false;
        }

        public void SetData(DreamlandInfo dreamlandInfo, DreamlandPlayerInfo playerInfo)
        {
            _dreamlandInfo = dreamlandInfo;
            Visible = true;
            _playerInfo = playerInfo;
            _textFightValue.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37832), playerInfo.FightValue);
            _textGuild.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37833), playerInfo.GuildName);
            _textName.CurrentText.text = playerInfo.Name;
            _textLevel.CurrentText.text = string.Format("LV{0}", playerInfo.Level);
            string currentDragonName = string.Concat(ColorDefine.GetColorName(playerInfo.DragonQuality), dreamland_explore_base_helper.GetShortName(dreamlandInfo.Id));
            _textDragonType.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37834), dreamland_explore_base_helper.GetShortName(dreamlandInfo.Id), ColorDefine.GetColorHtmlString(playerInfo.DragonQuality, currentDragonName));
            RefreshCurrentState(playerInfo);
            int maxBeRobedTimes = dreamland_explore_base_helper.GetOnceBeRobedMaxTimes(dreamlandInfo.Id);
            _textRobTimes.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37835), playerInfo.BeRobedTimes, ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, maxBeRobedTimes.ToString()));

            _textRewardGold.CurrentText.text = string.Format("{0}{1}", 
                ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GOLDEN, playerInfo.RobedCanGetGold.ToString()),
                item_helper.GetName(public_config.MONEY_TYPE_GOLD));
            _textRewardExp.CurrentText.text = string.Format("{0}{1}", 
                ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GOLDEN, playerInfo.RobedCanGetExp.ToString()),
                item_helper.GetName(public_config.ITEM_SPECIAL_TYPE_EXP));
            RefreshBtn(playerInfo);
        }

        private void RefreshCurrentState(DreamlandPlayerInfo playerInfo)
        {
            string desc = "";
            switch (playerInfo.CurrentState)
            {
            case DreamlandPlayerRobState.cannotRob:
                desc = MogoLanguageUtil.GetContent(37838);
                break;
            case DreamlandPlayerRobState.canRob:
                desc = MogoLanguageUtil.GetContent(37839);
                break;
            case DreamlandPlayerRobState.hadRobed:
                desc = MogoLanguageUtil.GetContent(37840);
                break;
            }
            _textState.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(37841), desc);
        }

        private void RefreshBtn(DreamlandPlayerInfo playerInfo)
        {
            if (playerInfo.Dbid == PlayerAvatar.Player.dbid)
            {
                _btnRob.Visible = false;
            }
            else
            {
                _btnRob.Visible = true;
            }
        }
    }
}
