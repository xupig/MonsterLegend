﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Mogo.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleDreamland
{
    public class DreamlandBoxView : KContainer
    {
        private DreamlandInfo _dreamlandInfo;

        private KButton _btnClose;
        private KButton _btnGetReward;
        private StateText _textFinish;
        private StateText _textYestodayFinish;
        private KParticle _particleBox;

        protected override void Awake()
        {
            _btnClose = GetChildComponent<KButton>("Button_close");
            _btnGetReward = GetChildComponent<KButton>("Button_box");
            _textFinish = GetChildComponent<StateText>("Label_txtWanchengjiangli");
            _textYestodayFinish = GetChildComponent<StateText>("Label_txtZuorijiangli");
            _particleBox = AddChildComponent<KParticle>("Button_box/fx_ui_24_2_baoxianglingqu");
            _particleBox.transform.localPosition = new Vector3(-55.5f, 90.5f, 0f);
            _particleBox.transform.localScale = new Vector3(4f, 4f, 4f);

            MogoUtils.AdaptScreen(GetChildComponent<StateImage>("ScaleImage_sharedZhezhao"));
            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnGetReward.onClick.AddListener(OnGetReward);
            _btnClose.onClick.AddListener(OnClose);
        }

        private void RemoveListener()
        {
            _btnGetReward.onClick.RemoveListener(OnGetReward);
            _btnClose.onClick.RemoveListener(OnClose);
        }

        private void OnGetReward()
        {
            DreamlandManager.Instance.RequestExploreFinish(_dreamlandInfo.Id);
        }

        private void OnClose()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.Dreamland);
        }

        public DreamlandInfo Data
        {
            set
            {
                Visible = true;
                _dreamlandInfo = value;
                RefreshLabel(value.IsYestodayData);
                TweenTreasureBox.Begin(_btnGetReward.gameObject);
            }
        }

        private void RefreshLabel(bool isYestodayData)
        {
            _textYestodayFinish.Visible = false;
            _textFinish.Visible = false;
            if (isYestodayData)
            {
                _textYestodayFinish.Visible = true;
            }
            else
            {
                _textFinish.Visible = true;
            }
        }
    }
}
