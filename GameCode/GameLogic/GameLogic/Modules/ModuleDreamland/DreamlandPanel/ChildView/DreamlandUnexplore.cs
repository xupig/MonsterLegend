﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/12 14:59:03
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameData;
using ModuleCommonUI;
using Common.Utils;
using Common.ExtendComponent;
using UnityEngine;

namespace ModuleDreamland
{
    public class DreamlandUnexplore : KContainer
    {
        private DreamlandInfo _dreamlandInfo;

        private DreamlandUnexploreRightInfo _unexploreInfo;
        private DreamlandUnexplorePath _unexplorePath;
        private KButton _btnExplore;
        private KParticle _exploreParticle;

        protected override void Awake()
        {
            _unexploreInfo = AddChildComponent<DreamlandUnexploreRightInfo>("Container_detail");
            _unexplorePath = AddChildComponent<DreamlandUnexplorePath>("Container_luxian");
            _btnExplore = GetChildComponent<KButton>("Button_kaishitansuo");
            _exploreParticle = AddChildComponent<KParticle>("Button_kaishitansuo/fx_ui_8_1_huanjing_01");
            _exploreParticle.transform.localPosition = new Vector3(-3.22f, 3.38f, 0f);

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnExplore.onClick.AddListener(OnShowSelectDragon);
        }

        private void RemoveListener()
        {
            _btnExplore.onClick.RemoveListener(OnShowSelectDragon);
        }

        private void OnShowSelectDragon()
        {
            if (_dreamlandInfo.CurrentStationId == dreamland_explore_station_helper.GetDreamlandChapterMaxStationNum(_dreamlandInfo.Id))
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(37856), PanelIdEnum.Dreamland);
                return;
            }

            DreamlandPopDataWrapper wrapper = new DreamlandPopDataWrapper(DreamlandPopType.selectDragon, _dreamlandInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.DreamlandPop, wrapper);
        }

        public DreamlandInfo Data
        {
            set
            {
                Visible = true;
                _dreamlandInfo = value;
                _unexploreInfo.Data = value;
                _unexplorePath.SetData(value);
                RefreshBtn(value);
            }
        }

        private void RefreshBtn(DreamlandInfo dreamlandInfo)
        {
            DreamlandChapterState state = dreamlandInfo.GetState();
            if (state == DreamlandChapterState.haveReward || dreamlandInfo.FinishedExploreToday())
            {
                _exploreParticle.Stop();
                _btnExplore.Visible = false;
            }
            else
            {
                _btnExplore.Visible = true;
                _exploreParticle.Play(true);
            }
        }
    }
}
