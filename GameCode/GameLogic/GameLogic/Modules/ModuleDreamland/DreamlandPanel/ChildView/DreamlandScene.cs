﻿using Common.Data;
using Common.Utils;
using Game.Asset;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleDreamland
{
    public class DreamlandScene : KContainer
    {
        //public const string PREPARE_SCENE_NAME = "PrepareRace";
        public const string EXPLORING_SCENE_NAME = "Race";

        //public const string PREPARE_SCENE_PATH = "Scenes/1007_Race_ready/1007_Race.prefab";
        private const string EXPLORING_SCENE_PATH = "Scenes/10{0}_Race/10{0}_Race.prefab";

        //private GameObject _prepareScene;
        private GameObject _exploringScene;
        private DreamlandBg _dreamlandBg;
        private GameObject _goMask;

        private DreamlandInfo _dreamlandInfo;
        private string _currentExploringPath;
        private Action _callback;

        protected override void Awake()
        {
            _dreamlandBg = AddChildComponent<DreamlandBg>("Container_bg");
            _goMask = GetChild("ScaleImage_sharedZhezhao");
        }

        protected override void OnEnable()
        {
            CameraManager.GetInstance().HideMainCamera();
        }

        protected override void OnDisable()
        {
            CameraManager.GetInstance().ShowMainCamera();
        }

        protected override void OnDestroy()
        {
            //if (_prepareScene != null)
            //{
            //    Destroy(_prepareScene);
            //    _prepareScene = null;
            //}

            if (_exploringScene != null)
            {
                Destroy(_exploringScene);
                _exploringScene = null;
            }
        }

        public void LoadScene(DreamlandInfo dreamlandInfo, Action callback)
        {
            _dreamlandInfo = dreamlandInfo;
            _callback = callback;
            DreamlandChapterState state = dreamlandInfo.GetState();
            switch (state)
            {
                case DreamlandChapterState.exploring:
                    RefreshExploringScene(dreamlandInfo, callback);
                    HidePrepareScene();
                    _goMask.SetActive(false);
                    break;
                case DreamlandChapterState.unexplore:
                case DreamlandChapterState.haveReward:
                    RefreshPrepareScene(callback);
                    _goMask.SetActive(true);
                    HideExploringScene();
                    break;
            }
        }

        private void HidePrepareScene()
        {
            //if (_prepareScene != null)
            //{
            //    _prepareScene.SetActive(false);
            //}
            _dreamlandBg.Visible = false;
        }

        private void HideExploringScene()
        {
            if (_exploringScene != null)
            {
                _exploringScene.SetActive(false);
            }
        }

        private void RefreshExploringScene(DreamlandInfo dreamlandInfo, Action callback)
        {
            int stationNum = 8 + dreamlandInfo.CurrentStationId;
            string loadPath = string.Format(EXPLORING_SCENE_PATH, stationNum.ToString().PadLeft(2, '0'));
            if (_exploringScene == null)
            {
                ObjectPool.Instance.GetGameObject(loadPath, LoadExploringScene);
            }
            else
            {
                if (_currentExploringPath != loadPath)
                {
                    ObjectPool.Instance.GetGameObject(loadPath, LoadExploringScene);
                }
                else
                {
                    _exploringScene.SetActive(true);
                    _callback.Invoke();
                }
            }
            _currentExploringPath = loadPath;
        }

        private void LoadExploringScene(GameObject scene)
        {
            Destroy(_exploringScene);
            _exploringScene = scene;
            _exploringScene.SetActive(true);
            _exploringScene.name = EXPLORING_SCENE_NAME;
            Camera camera = _exploringScene.GetComponentInChildren<Camera>();
            DreamlandCamera dreamlandCamera = camera.gameObject.GetComponent<DreamlandCamera>();
            if (dreamlandCamera == null)
            {
                dreamlandCamera = camera.gameObject.AddComponent<DreamlandCamera>();
                dreamlandCamera.Camera = camera;
            }
            MogoLayoutUtils.SetPositionX(scene, 2000f);
            _callback.Invoke();
        }

        private void RefreshPrepareScene(Action callback)
        {
            _dreamlandBg.Visible = true;
            _callback.Invoke();
            //if (_prepareScene == null)
            //{
            //    ObjectPool.Instance.GetGameObject(PREPARE_SCENE_PATH, LoadPrepareScene);
            //}
            //else
            //{
            //    _prepareScene.SetActive(true);
            //    _callback.Invoke();
            //}
        }

        //private void LoadPrepareScene(GameObject scene)
        //{
        //    _prepareScene = scene;
        //    _prepareScene.name = PREPARE_SCENE_NAME;
        //    MogoLayoutUtils.SetPositionX(scene, 2000f);
        //    _callback.Invoke();
        //}
    }
}
