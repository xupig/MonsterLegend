﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.Enum;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using ModuleCopy;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleDreamland
{
    public class DreamlandResultPanel : BasePanel
    {
        private CopyResult _result;

        private StateText _textLeftSecond;
        private GameObject _goWin;
        private GameObject _goFail;
        private StateText _textName;
        private StateText _textGold;
        private StateText _textExp;
        private StateText _textLevel;
        private CopyProgressBar _progressBar;
        private GameObject _goLevelUp;
        private Countdown _countdown;
        private CopyModelComponent _model;
        private KButton _statisticBtn;

        protected override void Awake()
        {
            _textLeftSecond = GetChildComponent<StateText>("Label_txtShijian");
            CloseBtn = GetChildComponent<KButton>("Button_queding");
            _statisticBtn = GetChildComponent<KButton>("Button_zhandoutongji");
            _goWin = GetChild("Container_shengli");
            _goFail = GetChild("Container_shibai");
            _textName = GetChildComponent<StateText>("Container_xinxi/Label_txtWwanjiamingziqigezi");
            _textGold = GetChildComponent<StateText>("Container_xinxi/Container_gold/Label_txtGoldshuzhi");
            _textExp = GetChildComponent<StateText>("Container_xinxi/Container_jingyan/Label_txtJingyanshuzhi");
            _textLevel = GetChildComponent<StateText>("Container_xinxi/Container_Dengjijingyan/Label_txtdengji");
            _progressBar = AddChildComponent<CopyProgressBar>("Container_xinxi/Container_Dengjijingyan/ProgressBar_jindu");
            _goLevelUp = GetChild("Container_xinxi/Container_Dengjijingyan/Container_levelUp");
            _countdown = gameObject.AddComponent<Countdown>();

            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");

            InitModel();
            InitIcon();
            AddListener();
        }

        private void InitModel()
        {
            _model = AddChildComponent<CopyModelComponent>("Container_model");
        }

        private void InitIcon()
        {
            AddChildComponent<IconContainer>("Container_xinxi/Container_gold/Container_icon").SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_GOLD));
            AddChildComponent<IconContainer>("Container_xinxi/Container_jingyan/Container_icon").SetIcon(item_helper.GetIcon(public_config.ITEM_SPECIAL_TYPE_EXP));
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _countdown.onEnd.AddListener(OnClose);
            _statisticBtn.onClick.AddListener(ShowStatisticPanel);
        }

        private void RemoveListener()
        {
            _countdown.onEnd.RemoveListener(OnClose);
            _statisticBtn.onClick.RemoveListener(ShowStatisticPanel);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.DreamlandResult; }
        }

        public override void OnShow(object data)
        {
            CopyWinLoseLogic pkLogic = data as CopyWinLoseLogic;
            _result = pkLogic.Result;
            RefreshResult(pkLogic.Result);
            RefreshName(PlayerAvatar.Player.name);
            RefreshLevel(PlayerAvatar.Player.level);
            RefreshGold(pkLogic.BaseItemDataList);
            RefreshExp(pkLogic.BaseItemDataList);
            _model.LoadPlayerModel(pkLogic.Result);
            _countdown.StartCountdown(10, _textLeftSecond);
           
            if(PlayerDataManager.Instance.TaskData.isTeammateInTeamTask())
            {
                CloseBtn.Visible = false;
            }
            else
            {
                CloseBtn.Visible = true;
            }
            RefreshStatisticButton();
        }

        private void RefreshStatisticButton()
        {
            int insId = GameSceneManager.GetInstance().curInstanceID;
            instance ins = instance_helper.GetInstanceCfg(insId);
            _statisticBtn.Visible = instance_helper.GetStatisticType(ins) != 0;
        }

        private void RefreshResult(CopyResult result)
        {
            if (result == CopyResult.Win)
            {
                _goWin.SetActive(true);
                _goFail.SetActive(false);
                UIManager.Instance.PlayAudio("Sound/UI/fight_victory.mp3");
            }
            else
            {
                _goFail.SetActive(true);
                _goWin.SetActive(false);
                UIManager.Instance.PlayAudio("Sound/UI/fight_fail.mp3");
            }
        }

        private void RefreshName(string name)
        {
            _textName.CurrentText.text = name;
        }

        private void RefreshLevel(byte level)
        {
            _textLevel.CurrentText.text = level.ToString();
        }

        private Dictionary<int, int> ParseListToDict(List<BaseItemData> baseItemDataList)
        {
            Dictionary<int, int> itemIdDict = new Dictionary<int, int>();
            if (baseItemDataList != null)
            {
                for (int i = 0; i < baseItemDataList.Count; i++)
                {
                    int itemId = (int)baseItemDataList[i].Id;
                    int itemCount = (int)baseItemDataList[i].StackCount;
                    if (itemIdDict.ContainsKey(itemId))
                    {
                        itemIdDict[itemId] = itemIdDict[itemId] + itemCount;
                    }
                    else
                    {
                        itemIdDict.Add(itemId, itemCount);
                    }
                }
            }
            return itemIdDict;
        }

        private void RefreshGold(List<BaseItemData> baseItemDataList)
        {
            Dictionary<int, int> rewardDic = ParseListToDict(baseItemDataList);
            int gold = rewardDic.ContainsKey(SpecialItemType.GOLD) ? rewardDic[SpecialItemType.GOLD] : 0;
            _textGold.CurrentText.text = string.Format("+{0}", gold);
        }

        private void RefreshExp(List<BaseItemData> baseItemDataList)
        {
            Dictionary<int, int> rewardDic = ParseListToDict(baseItemDataList);
            int exp = rewardDic.ContainsKey(SpecialItemType.EXP_PLAYER) ? rewardDic[SpecialItemType.EXP_PLAYER] : 0;
            _textExp.CurrentText.text = string.Format("+{0}", exp);

            LevelConfig lastLevel = avatar_level_helper.GetLastLevelConfig(PlayerAvatar.Player.level, PlayerAvatar.Player.exp, exp);
            //Debug.LogError(string.Format("now exp:{0}   level:{1}", PlayerAvatar.Player.exp, PlayerAvatar.Player.level));
            //Debug.LogError(string.Format("last exp:{0}   level:{1}", lastLevel.Exp, lastLevel.Level));
            float nowRate = avatar_level_helper.GetExpPercent(PlayerAvatar.Player.exp, PlayerAvatar.Player.level);
            float lastRate = avatar_level_helper.GetExpPercent(lastLevel.Exp, lastLevel.Level);
            _progressBar.SetProgress(lastRate - (int)lastRate, nowRate + (PlayerAvatar.Player.level - lastLevel.Level));
            _goLevelUp.SetActive(lastLevel.Level != PlayerAvatar.Player.level);
        }

        public override void OnClose()
        {
            if(PlayerDataManager.Instance.TaskData.isTeammateInTeamTask() == false)
            {
                GameSceneManager.GetInstance().LeaveCombatScene();
            }
        }

        private void ShowStatisticPanel()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.BattleDataStatistic);
        }
    }
}
