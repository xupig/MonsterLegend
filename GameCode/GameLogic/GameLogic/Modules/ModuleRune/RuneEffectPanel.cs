﻿#region 模块信息
/*==========================================
// 文件名：RuneEffectPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleRune
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/5 21:07:45
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleRune
{
    public class RuneEffectPanel:BasePanel
    {
        private KParticle _runeEffect1;
        private KParticle _runeEffect2;

        private PbRuneItemInfoList runeData;

        protected override void Awake()
        {
            _runeEffect1 = AddChildComponent<KParticle>("fx_ui_10_4_huode_02");
            _runeEffect2 = AddChildComponent<KParticle>("fx_ui_10_4_huode_12");
            _runeEffect1.Stop();
            _runeEffect2.Stop();
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.RuneEffect; }
        }

        public override void OnShow(object data)
        {
            SetData(data);
        }

        public override void OnClose()
        {
            if (_runeEffect1.Visible)
            {
                _runeEffect1.Stop();
            }
            if (_runeEffect1.Visible)
            {
                _runeEffect1.Stop();
            }
        }

        public override void SetData(object data)
        {
            runeData = data as PbRuneItemInfoList;
            rune_random runeRandom = rune_helper.GetRuneRandomById(runeData.location_id);
            if (runeRandom.__cost.ContainsKey(public_config.MONEY_TYPE_RUNE_STONE.ToString()))
            {
                PlayParticle(_runeEffect1,new Vector3(15,-10,0));
            }
            else
            {
                PlayParticle(_runeEffect2,new Vector3(465,-10,0));
            }
        }

        private void PlayParticle(KParticle particle,Vector3 pos)
        {
            if (particle.Visible == false)
            {
                particle.Play();
                particle.Position = pos;// new Vector3(237, -40, 0);
                particle.onComplete.AddListener(OnPlayEnd);
            }
        }

        private void OnPlayEnd()
        {
            _runeEffect1.onComplete.RemoveListener(OnPlayEnd);
            _runeEffect2.onComplete.RemoveListener(OnPlayEnd);
            PanelIdEnum.ItemObtain.Show(runeData);
            DramaTrigger.RuneWishEnd();
            ClosePanel();
        }

    }
}
