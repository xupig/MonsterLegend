﻿#region 模块信息
/*==========================================
// 文件名：RuneLevelUpView
// 命名空间: GameLogic.GameLogic.Modules.ModuleGlyphVow
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/10 11:43:56
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleRune
{
    public class RuneUpgradePanel : BasePanel
    {


        private static string levelTemplate;
        private static string expTemplate;
        private StateText _txtName;
        private StateText _txtLevel;

        private int levelCurrent;

        private ItemGrid _itemGrid;
        //private KContainer _iconContainer;

        private string template1;
        private string template2;

        private StateText _txtAttribute;
        private StateText _txtAttribute2;
        private StateText _txtExp;
        private KProgressBar _progressBar;
        private StateText _txtProgress;

        private static List<RuneUpgradeGrid> _upgradeGridList;
        private int _total;
       
        //private int[] _dataList;

        private int[] _combineDataList;
        private bool _isCombineSeniorRune = false;

        private KButton _btnSwallow;
        private KButton _btnAutoAdd;

        private TweenProgressBar _progressBarWrapper;

        private KParticle _particle;

        private KContainer _imgUpgrade;

        private KDummyButton _btnClose;

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _btnClose = ModalMask.gameObject.AddComponent<KDummyButton>();
            _imgUpgrade = GetChildComponent<KContainer>("Container_shengji");
            _imgUpgrade.Visible = false;
            _particle = AddChildComponent<KParticle>("fx_ui_6_3_1_tisheng");
            _particle.Stop();
            _txtName = GetChildComponent<StateText>("Label_txtBiaoti");
            _txtLevel = GetChildComponent<StateText>("Label_txtDengji");
            levelTemplate = _txtLevel.CurrentText.text;
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _itemGrid.IsClickable = false;
            _txtAttribute = GetChildComponent<StateText>("Container_Nr01/Label_txtBaoji");
            _txtAttribute2 = GetChildComponent<StateText>("Container_Nr01/Label_txtBaojiNext");
            template1 = _txtAttribute.CurrentText.text;
            template2 = _txtAttribute2.CurrentText.text;
            _txtExp = GetChildComponent<StateText>("Container_Nr02/Label_txtJingyan");
            expTemplate = _txtExp.CurrentText.text;
            _progressBar = GetChildComponent<KProgressBar>("Container_Nr02/ProgressBar_jindu");
            _progressBarWrapper = _progressBar.gameObject.AddComponent<TweenProgressBar>();
            _txtProgress = GetChildComponent<StateText>("Container_Nr02/Label_txtJindu");
            InitUpgradeGrids();
            _btnAutoAdd = GetChildComponent<KButton>("Button_zidongtianjia");
            _btnSwallow = GetChildComponent<KButton>("Button_shengji");
            CloseBtn = GetChildComponent<KButton>("Button_close");

        }

        private void InitUpgradeGrids()
        {
            _upgradeGridList = new List<RuneUpgradeGrid>();
            for(int i = 0; i < 5; i++)
            {
                RuneUpgradeGrid grids = AddChildComponent<RuneUpgradeGrid>("Container_itemGrids/Button_fuwen" + i);
                _upgradeGridList.Add(grids);
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }


        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<List<int>>(RuneEvent.RUNE_LEVEL_UP_PUT_ON, OnAddItems);
            EventDispatcher.AddEventListener(RuneEvent.RUNE_LEVEL_UP_VIEW_UPDATE, OnCombineFinish);
            _progressBarWrapper.onProgress.AddListener(OnProgressChange);
            _btnSwallow.onClick.AddListener(OnClickCombine);
            _btnAutoAdd.onClick.AddListener(OnClickAutoAdd);
            _btnClose.onClick.AddListener(OnClickClose);
            foreach (RuneUpgradeGrid grids in _upgradeGridList)
            {
                grids.onGridClick.AddListener(OnClickGrid);
            }
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<List<int>>(RuneEvent.RUNE_LEVEL_UP_PUT_ON, OnAddItems);
            EventDispatcher.RemoveEventListener(RuneEvent.RUNE_LEVEL_UP_VIEW_UPDATE, OnCombineFinish);
            _progressBarWrapper.onProgress.RemoveListener(OnProgressChange);
            _btnSwallow.onClick.RemoveListener(OnClickCombine);
            _btnAutoAdd.onClick.RemoveListener(OnClickAutoAdd);
            _btnClose.onClick.RemoveListener(OnClickClose);
            foreach (RuneUpgradeGrid grids in _upgradeGridList)
            {
                grids.onGridClick.RemoveListener(OnClickGrid);
            }
        }

        private void OnClickClose()
        {
            Id.Close();
        }

        private void OnProgressChange(float current,float total)
        {
            _txtProgress.CurrentText.text = string.Format("{0}/{1}", Convert.ToUInt32(current), Convert.ToUInt32(total));
        }

        public override void OnShow(System.Object data)
        {
            _progressBarWrapper.Clear();
            AddEventListener();
            SetContent();
        }

        private void SetUpgradeGridContent()
        {
            RuneItemInfo info = PlayerDataManager.Instance.BagData.GetBagData(RuneManager.levelUpData.bagType).GetItemInfo(RuneManager.levelUpData.pos) as RuneItemInfo;
            _txtName.CurrentText.text = string.Concat(item_helper.GetName(info.Id), item_helper.GetItemQualityDesc(info.Quality));
            levelCurrent = info.Level;
            _txtLevel.CurrentText.text = string.Format(levelTemplate, info.Level, "+0");
            Dictionary<int, List<int[]>> attributeLevelValueDict = rune_helper.GetRuneAttributeData(info.RuneConfig);

            SetTxtAttribute(info.Level, attributeLevelValueDict, _txtAttribute, template1);
            SetTxtAttribute(info.Level + 1, attributeLevelValueDict, _txtAttribute2, template2);
        }

        private void SetTxtAttribute(int level, Dictionary<int, List<int[]>> attributeLevelValueDict, StateText txt, string template)
        {
            if (attributeLevelValueDict.ContainsKey(level))
            {
                txt.Visible = true;
                List<int[]> dataList = attributeLevelValueDict[level];
                GameData.attri_config attri1 = XMLManager.attri_config[dataList[0][0]];
                string attri2Content;
                string name = string.Empty;
                if (dataList.Count > 1)
                {
                    attri2Content = string.Format("-{0}", dataList[1][1]);
                    name = attri1.__name_id.ToLanguage().Substring(0, 2);
                }
                else
                {
                    attri2Content = string.Empty;
                    name = attri1.__name_id.ToLanguage();
                }
                txt.CurrentText.text = string.Format(template, name, dataList[0][1], attri2Content);
            }
            else
            {
                txt.Visible = false;
            }
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.RuneLevelUp; }
        }

        private void OnClickGrid(RuneUpgradeGrid grid)
        {
            UpdateCombineGridInfo();
        }

        private void GetCombineData()
        {
            _combineDataList = new int[_total + 1];
            _combineDataList[0] = RuneManager.levelUpData.pos;
            int index = 1;
            _isCombineSeniorRune = false;
            for (int i = 0; i < _upgradeGridList.Count; i++)
            {
                RuneItemInfo info = _upgradeGridList[i].ItemInfo;
                if (info != null)
                {
                    if (_isCombineSeniorRune == false)
                    {
                        _isCombineSeniorRune = info.Quality >= GlobalParams.RuneAutoSwallowLevelLimit && info.RuneConfig.__subtype!=public_config.RUNE_EXP;
                    }
                    _combineDataList[index] = _upgradeGridList[i].ItemPos;
                    index++;
                }
            }
        }

        private void OnClickCombine()
        {
            GetCombineData();
            if (_combineDataList.Length == 1)
            {
                MessageBox.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.RUNE_HAS_NOTHING_TO_SWALLOW));
                return;
            }
            if (_isCombineSeniorRune)
            {
                MessageBox.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.RUNE_HAS_SENIOR_RUNE), Combine);
            }
            else
            {
                Combine();
            }
        }



        private void Combine()
        {
            if(RuneManager.levelUpData != null)
            {
                if(RuneManager.levelUpData.bagType == BagType.BodyRuneBag)
                {
                    RuneManager.Instance.LevelUpBodyBagRune(_combineDataList);
                }
                else if(RuneManager.levelUpData.bagType == BagType.RuneBag)
                {
                    RuneManager.Instance.LevelUpBagRune(_combineDataList);
                }
            }
        }

        public void OnAddItems(List<int> _list)
        {
            for(int i = 0; i < _upgradeGridList.Count; i++)
            {
                if (_list.Count > 0 && _upgradeGridList[i].ItemPos == 0)
                {
                    _upgradeGridList[i].ItemPos = _list[0];
                    _list.RemoveAt(0);
                }
            }
            UpdateCombineGridInfo();
        }

        private uint _timerId = 0;

        private void OnCombineFinish()
        {
            RuneItemInfo info = PlayerDataManager.Instance.BagData.GetBagData(RuneManager.levelUpData.bagType).GetItemInfo(RuneManager.levelUpData.pos) as RuneItemInfo;
            if (levelCurrent < info.Level)
            {
                _particle.Play();
                _imgUpgrade.Visible = true;
                //_imgUpgrade.transform.localPosition = new Vector3(imageOldPosition.x, imageOldPosition.y- 200, imageOldPosition.z);

                TweenViewMove tween = TweenViewMove.Begin(_imgUpgrade.gameObject, MoveType.Show, Common.ExtendComponent.MoveDirection.Down, 0.5f);
                tween.Tweener.method = UITweener.Method.BounceIn;
                ResetTimer();
                _timerId = TimerHeap.AddTimer(1500, 0, OnRiseEnd);

            }
           SetContent();
        }

        private void OnRiseEnd()
        {
            _imgUpgrade.Visible = false;
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }


        private void OnFinish(UITweener tween)
        {
            _imgUpgrade.Visible = false;
        }

        public void SetContent()
        {
            SetUpgradeGridContent();
            for(int i = 0; i < _upgradeGridList.Count; i++)
            {
                _upgradeGridList[i].ItemPos = 0;
            }
            UpdateCombineGridInfo();
        }

        private void OnClickAutoAdd()
        {
            if (RuneManager.levelUpData.maxNeedNum<=0)
            {
                MogoUtils.FloatTips(4353);
                return;
            }
           
            RuneItemInfo upgradeInfo = PlayerDataManager.Instance.BagData.GetBagData(RuneManager.levelUpData.bagType).GetItemInfo(RuneManager.levelUpData.pos) as RuneItemInfo;
            int count = 0;
            for (int i = 0; i < _upgradeGridList.Count; i++)
            {
                if (_upgradeGridList[i].ItemPos == 0 && FindARuneItem(i, upgradeInfo))
                {
                    count++;
                }
            }
            if(count == 0)
            {
                Dictionary<int, BaseItemInfo> dic = PlayerDataManager.Instance.BagData.GetRuneBagData().GetAllItemInfo();
                foreach (KeyValuePair<int, BaseItemInfo> kvp in dic)
                {
                    RuneItemInfo itemInfo = kvp.Value as RuneItemInfo;
                    if (kvp.Value != null && itemInfo.IsSelectable && RuneManager.NotSelectedRune(kvp.Key) && upgradeInfo.CanCombine(itemInfo))//不锁定,可吞噬，或者被吞噬的符文比吞噬符文的等级高
                    {
                        UIManager.Instance.ShowPanel(PanelIdEnum.RuneAdd);
                        return;
                    }
                }
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.RUNE_NO_AUTO_ADD_RUNE), PanelIdEnum.Rune);
            }
            else
            {
                UpdateCombineGridInfo();
            }
            
        }


        private bool FindARuneItem(int index, RuneItemInfo info)
        {

            Dictionary<int, BaseItemInfo> dic = PlayerDataManager.Instance.BagData.GetRuneBagData().GetAllItemInfo();
            RuneItemInfo currentItemInfo = null;
            foreach(KeyValuePair<int, BaseItemInfo> kvp in dic)
            {
                RuneItemInfo itemInfo = kvp.Value as RuneItemInfo;
                if (itemInfo != null && itemInfo.CanAutoCombine && RuneManager.NotSelectedRune(kvp.Key) && IsNotSelected(itemInfo.GridPosition))
                {
                    if(info.CanCombine(itemInfo))  //可吞噬，或者被吞噬的符文比吞噬符文的等级高
                    {
                        if(IsPreferredSwallow(itemInfo,currentItemInfo))
                        {
                            currentItemInfo = itemInfo;
                        }
                    }
                }
            }
            if(currentItemInfo!=null)
            {
                _upgradeGridList[index].ItemPos = currentItemInfo.GridPosition;
                return true;
            }
            return false;
        }

        public static bool IsNotSelected(int position)
        {
            if(_upgradeGridList!=null)
            {
                for (int i = 0; i < _upgradeGridList.Count; i++)
                {
                    if (_upgradeGridList[i] != null && _upgradeGridList[i].ItemPos == position)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private bool IsPreferredSwallow(RuneItemInfo before,RuneItemInfo after)
        {
            if(after == null)
            {
                return true;
            }

            if(after.Quality!=before.Quality)
            {
                return after.Quality > before.Quality;
            }
            else if (after.Level != before.Level)
            {
                return after.Level > before.Level;
            }
            else if (after.RuneConfig.__subtype != before.RuneConfig.__subtype)
            {
                return after.RuneConfig.__subtype > before.RuneConfig.__subtype;
            }
            return false;
        }

        private void UpdateCombineGridInfo()
        {
            int addedExp = 0;
            _total = 0;
            for(int i = 0; i < _upgradeGridList.Count; i++)
            {
                RuneItemInfo itemInfo = _upgradeGridList[i].ItemInfo;
                if (itemInfo!=null)
                {
                    addedExp += itemInfo.CurExp + itemInfo.RuneConfig.__exp_value;
                    _total++;
                }
            }
            _txtExp.CurrentText.text = string.Format(expTemplate, addedExp);
            RuneItemInfo runeToBeLevelUp = PlayerDataManager.Instance.BagData.GetBagData(RuneManager.levelUpData.bagType).GetItemInfo(RuneManager.levelUpData.pos) as RuneItemInfo;
            int addExpLevel = runeToBeLevelUp.Exp2Level(runeToBeLevelUp.CurExp + addedExp);
            if(addExpLevel >= runeToBeLevelUp.MaxLevel)
            {
                int levelMaxExp = runeToBeLevelUp.LevelMaxExp(runeToBeLevelUp.CurExp);
                int curLevelMinExp = runeToBeLevelUp.LevelMinExp(runeToBeLevelUp.CurExp);
                _txtProgress.CurrentText.text = (4334).ToLanguage();//已满级
                _progressBarWrapper.SetProgress(levelMaxExp - curLevelMinExp, levelMaxExp - curLevelMinExp, true);
            }
            else
            {
                int nextLevelExp = runeToBeLevelUp.LevelMaxExp(runeToBeLevelUp.CurExp + addedExp);
                int curLevelMinExp = runeToBeLevelUp.LevelMinExp(runeToBeLevelUp.CurExp + addedExp);
               // _txtProgress.CurrentText.text = string.Format("{0}/{1}", runeToBeLevelUp.CurExp + addedExp - curLevelMinExp, nextLevelExp - curLevelMinExp);
                _progressBarWrapper.SetProgress((float)(runeToBeLevelUp.CurExp + addedExp - curLevelMinExp), (float)nextLevelExp - curLevelMinExp, addExpLevel);
            }

           
            _txtLevel.CurrentText.text = string.Format(levelTemplate, levelCurrent, "+" + (addExpLevel - levelCurrent));
            _itemGrid.SetItemData(runeToBeLevelUp);
            RuneManager.levelUpData.maxNeedNum = _upgradeGridList.Count - _total;
        }

    }
}
