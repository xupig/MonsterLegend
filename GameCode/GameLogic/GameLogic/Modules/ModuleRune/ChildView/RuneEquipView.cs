﻿#region 模块信息
/*==========================================
// 文件名：RuneEquipView
// 命名空间: GameLogic.GameLogic.Modules.ModuleRune.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/7 10:03:32
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using ModuleRune.ChildComponent;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace ModuleRune
{
    public class RuneEquipView:KContainer
    {
        /// <summary>
        /// Key为物品格子编号
        /// </summary>
        private Dictionary<int, RuneBodyGrid> m_bodyItemDic;

        private Dictionary<int, Dictionary<int, int>> holeIndex2IdDic;


        private StateText m_txtTitle;
        private StateText m_txtTextSoil;

        private StateText _txtEmpty;

        private string titleTemplate;
        private string soilTemplate;

        private KContainer _leftContainer;

        private KScrollView _scrollView;
        private KPageableList _pageableList;

        private CategoryList _toggleGroup;

        private int _selectedIndex;
        
        public static int toPos;

        private List<CategoryItemData> list;

        private rune_hole _hole;

        protected override void Awake()
        {
            _leftContainer = GetChildComponent<KContainer>("Container_left");
            m_txtTitle = GetChildComponent<StateText>("Container_left/Label_Title");
            titleTemplate = m_txtTitle.CurrentText.text;
            m_txtTextSoil = GetChildComponent<StateText>("Container_left/Label_TextSoil");
            soilTemplate = m_txtTextSoil.CurrentText.text;

            _scrollView = GetChildComponent<KScrollView>("Container_right/ScrollView_fuwenxinxi");
            _pageableList = _scrollView.GetChildComponent<KPageableList>("mask/content");
            //_scrollView.SetCapacity(12);
            _pageableList.SetDirection(KList.KListDirection.TopToDown, 1, 6);
            _pageableList.SetGap(-7, 0);
            _pageableList.SetPadding(0, 0, 0, 9);

            _txtEmpty = _pageableList.GetChildComponent<StateText>("txtTishi");
            //_scrollView.IsPageable = false;

            holeIndex2IdDic = rune_helper.GetHoleIndex2IdDic();
            InitBodyGrids();

            _toggleGroup = GetChildComponent<CategoryList>("List_categoryList");


            
           
            
            base.Awake();
        }

        private void OnSelectedIndexChanged(CategoryList toggle, int index)
        {
            _selectedIndex = index + 1;
            OnRuneBodyBagAllChange();
        }

        public void Show()
        {
            gameObject.SetActive(true);
            list = rune_helper.GetEquipList();
            _toggleGroup.RemoveAll();
            _toggleGroup.SetDataList<CategoryListItem>(list);
            AddEventListener();
            int level = rune_helper.GetMaxUnlockHole();
            _toggleGroup.SelectedIndex = level - 1;
            OnSelectedIndexChanged(_toggleGroup, level - 1);
            TweenViewMove.Begin(_scrollView.gameObject, MoveType.Show, MoveDirection.Right);
            SetOperationState();
        }

        public void SetOperationState()
        {
            List<KList.KListItemBase> list = _toggleGroup.ItemList;
            Dictionary<int, HashSet<int>> dataSet = PlayerDataManager.Instance.BagData.GetRuneBagData().bagRuneCanEquipedSet;
            for (int i = 0; i < list.Count; i++)
            {
                (list[i] as CategoryListItem).SetPoint(dataSet.ContainsKey(i + 1));
            }
        }

        public void Hide()
        {
            gameObject.SetActive(false);
            RemoveEventListener();
        }

        private void OnPutOn(int pos)
        {
            rune_hole hole = rune_helper.GetRuneHole(pos);
            if(hole.__page == _selectedIndex && m_bodyItemDic.ContainsKey(hole.__pos))
            {
                m_bodyItemDic[hole.__pos - 1].ShowEffect();
            }
        }

        private void AddEventListener()
        {
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, OnLevelChange);
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            EventDispatcher.AddEventListener(RuneEvent.RUNE_BODY_BAG_ALL_CHANGE, OnRuneBodyBagAllChange);
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, OnUpdateItem);
            EventDispatcher.AddEventListener<int>(RuneEvent.RUNE_PUT_ON, OnPutOn);
            //EventDispatcher.AddEventListener<int, RuneItemInfo>(RuneEvent.RUNE_BODY_BAG_ITEM_CHANGE, OnRuneBodyBagItemChange);
        }

        private void RemoveEventListener()
        {
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, OnLevelChange);
            EventDispatcher.RemoveEventListener(RuneEvent.RUNE_BODY_BAG_ALL_CHANGE, OnRuneBodyBagAllChange);
            EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, OnUpdateItem);
            EventDispatcher.RemoveEventListener<int>(RuneEvent.RUNE_PUT_ON, OnPutOn);
            //EventDispatcher.RemoveEventListener<int, RuneItemInfo>(RuneEvent.RUNE_BODY_BAG_ITEM_CHANGE, OnRuneBodyBagItemChange);
            if(_toggleGroup!=null)
            {
                _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
                for (int i = 0; i < 8; i++)
                {
                    m_bodyItemDic[i].onClick.RemoveListener(OnClickEquipItem);
                }
            }
        }

        private void OnLevelChange()
        {
            list = rune_helper.GetEquipList();
            _toggleGroup.RemoveAll();
            _toggleGroup.SetDataList<CategoryListItem>(list);
        }

        public void OnUpdateItem(BagType type, uint position)
        {
            if (type == BagType.BodyRuneBag)
            {
                RuneItemInfo runeItemInfo = PlayerDataManager.Instance.BagData.GetBagData(type).GetItemInfo((int)position) as RuneItemInfo;
                OnRuneBodyBagItemChange((int)position, runeItemInfo);
                UIManager.Instance.ClosePanel(PanelIdEnum.RuneToolTips);
                if(_hole != null)
                {
                    OnClickEquipItem(_hole);
                }
                else
                {
                    SetBagList();
                }
               
            }
            else if(type == BagType.RuneBag)
            {
                SetBagList();
            }
        }

       private void UpdateFightForce()
        {
           Dictionary<int,BaseItemInfo> itemInfoDict = PlayerDataManager.Instance.BagData.GetBodyRuneBagData().GetAllItemInfo();
           int totalFightForce = 0;
           int currentFightForce = 0;
           foreach(KeyValuePair<int,BaseItemInfo> kvp in itemInfoDict)
           {
               RuneItemInfo itemInfo = kvp.Value as RuneItemInfo;
               int fightForce = fight_force_helper.GetFightForce(itemInfo.EffectId, PlayerAvatar.Player.vocation);
               totalFightForce += fightForce;
               if(itemInfo.RuneConfig.__page == _selectedIndex)
               {
                   currentFightForce += fightForce;
               }
           }
           m_txtTitle.CurrentText.text = string.Format(titleTemplate,totalFightForce);
           if (list == null)
           {
               list = rune_helper.GetEquipList();
           }
           m_txtTextSoil.CurrentText.text = string.Format(soilTemplate, list[_selectedIndex - 1].name, currentFightForce);
        }

        public void OnRuneBodyBagAllChange()
        {
            UpdateBodyGrids();
            SetBagList();
        }

        private void InitBodyGrids()
        {
            m_bodyItemDic = new Dictionary<int, RuneBodyGrid>();
            for (int i = 0; i < 8; i++)
            {
                RuneBodyGrid item = AddChildComponent<RuneBodyGrid>("Container_left/Container_equipGrids/Container_LockMask" + (i+1));
                item.onClick.AddListener(OnClickEquipItem);
                m_bodyItemDic.Add(i, item);
            }
        }

        private void OnClickEquipItem(rune_hole hole)
        {
            _hole = hole;
            toPos = hole.__id;
            Dictionary<int, BaseItemInfo> itemDict = PlayerDataManager.Instance.BagData.GetRuneBagData().GetAllItemInfo();
            List<RuneItemInfo> dataList = new List<RuneItemInfo>();
           
            foreach(KeyValuePair<int,BaseItemInfo> kvp in itemDict)
            {
                RuneItemInfo itemInfo = kvp.Value as RuneItemInfo;
                if (itemInfo != null && itemInfo.RuneConfig.__page == hole.__page && (hole.__subtypes.Count == 0 || hole.__subtypes.Contains(itemInfo.RuneConfig.__subtype.ToString())))
                {
                    dataList.Add(itemInfo);
                }
            }
            dataList.Sort(SortFunc);
            _pageableList.SetDataList<RuneBagItem>(dataList);
            _txtEmpty.Visible = dataList.Count == 0;
        }

        private void SetBagList()
        {
            toPos = -1;
            _hole = null;
            Dictionary<int, BaseItemInfo> itemDict = PlayerDataManager.Instance.BagData.GetRuneBagData().GetAllItemInfo();
           
            List<RuneItemInfo> dataList = new List<RuneItemInfo>();

            foreach (KeyValuePair<int, BaseItemInfo> kvp in itemDict)
            {
                RuneItemInfo itemInfo = kvp.Value as RuneItemInfo;

                if (itemInfo != null && itemInfo.RuneConfig.__page == _selectedIndex)
                {
                    dataList.Add(itemInfo);
                }
            }

            dataList.Sort(SortFunc);
            _pageableList.SetDataList<RuneBagItem>(dataList,1);
            _txtEmpty.Visible = dataList.Count == 0;
        }

        private int SortFunc(RuneItemInfo item2, RuneItemInfo item1)
        {
            if (item1.Quality != item2.Quality)
            {
                return item1.Quality - item2.Quality;
            }
            bool item2CanEquiped = PlayerDataManager.Instance.BagData.GetRuneBagData().CanEquiped(item2.RuneConfig.__page, item2.GridPosition);
            bool item1CanEquiped = PlayerDataManager.Instance.BagData.GetRuneBagData().CanEquiped(item1.RuneConfig.__page, item1.GridPosition);

            if (item1CanEquiped!=item2CanEquiped)
            {
                return item1CanEquiped?1:-1;
            }
            return 0;
        }

        private void UpdateBodyGrids()
        {
            BagData runeBodyBagData = PlayerDataManager.Instance.BagData.GetBodyRuneBagData();
            foreach (KeyValuePair<int, RuneBodyGrid> k in m_bodyItemDic)
            {
                if (holeIndex2IdDic[_selectedIndex].ContainsKey(k.Key + 1))
                {
                    int id = holeIndex2IdDic[_selectedIndex][k.Key + 1];
                    RuneItemInfo info = runeBodyBagData.GetItemInfo(id) as RuneItemInfo;
                    rune_hole hole = rune_helper.GetRuneHole(id);
                    k.Value.setInfo(info, hole);
                }
                else
                {
                    k.Value.setInfo(null, null);
                }
            }
            UpdateFightForce();
        }

        private void OnRuneBodyBagItemChange(int pos, RuneItemInfo info)
        {
            rune_hole hole = rune_helper.GetRuneHole(pos);
            if (hole != null && hole.__page == _selectedIndex && m_bodyItemDic.ContainsKey(hole.__pos - 1))
            {
                m_bodyItemDic[hole.__pos - 1].setInfo(info, hole);
            }
            UpdateFightForce();
        }

    }
}
