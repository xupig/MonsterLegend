﻿#region 模块信息
/*==========================================
// 文件名：RuneCurrencyView
// 命名空间: GameLogic.GameLogic.Modules.ModuleRune.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/14 10:48:50
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ClientConfig;
using Common.ExtendComponent;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleRune
{
    public class RuneCurrencyView:KContainer
    {
        //private MoneyItem _goldItem;
        private MoneyItem _diamondItem;
        private MoneyItem _scrollItem;

        protected override void Awake()
        {
            //_goldItem = AddChildComponent<MoneyItem>("Container_buyGold");
            _diamondItem = AddChildComponent<MoneyItem>("Container_buyDiamond");
            _scrollItem = AddChildComponent<MoneyItem>("Container_buyTicket");
            //_goldItem.SetMoneyType(public_config.MONEY_TYPE_GOLD);
            _diamondItem.SetMoneyType(public_config.MONEY_TYPE_GOLD);
            _scrollItem.SetMoneyType(public_config.MONEY_TYPE_DIAMOND);

            base.Awake();
        }


        protected override void OnEnable()
        {

        }

        protected override void OnDisable()
        {

        }

    }
}
