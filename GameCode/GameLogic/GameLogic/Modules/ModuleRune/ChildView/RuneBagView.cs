﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/24 11:37:12
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using ModuleRune.ChildComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Data;
using MogoEngine.Events;
using Common.Events;
using Common.ExtendComponent;
using GameLoader.Utils.Timer;
using Mogo.Util;
using UnityEngine.UI;

namespace ModuleRune.ChildView
{
    public class RuneBagView : KContainer
    {
        public static int NUM_PER_PAGE = 15;


        private KScrollPage _scrollPage;
        private StateText _textCapacity;
        private string _capacityTemplate;
        private KPageableList _contentList;

        private KButton m_GlyphCombineButton;

        private KButton _btnQuestion;

        private int totalExp = 0;
        private List<int> combineList;
        private List<int> itemIdList;
        private Dictionary<int, int> posIndexDict;
        //private Dictionary<int, Vector3> posDict;
        private RuneItemInfo itemInfo = null;

        private CategoryList _toggleGroup;

        private int _selectedIndex;

        private List<RuneItemInfo> _itemList;

        private List<IconContainer> iconContainerList;

        private int[] posList;
        private int[] posItemIdList;

        private KParticle _particle;


        protected override void Awake()
        {
            
            _btnQuestion = GetChildComponent<KButton>("Button_Gantang");
            _itemList = new List<RuneItemInfo>();
            combineList = new List<int>();
            itemIdList = new List<int>();
            //posDict = new Dictionary<int, Vector3>();
            posIndexDict = new Dictionary<int, int>();
            m_GlyphCombineButton = GetChildComponent<KButton>("Button_yijianhecheng");
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_itemBag");
            _scrollPage.TotalPage = public_config.MAX_COUNT_PKG_RUNE/15;
            _contentList = _scrollPage.GetChildComponent<KPageableList>("mask/content");
            _contentList.SetDirection(KList.KListDirection.LeftToRight, 5, 3);
            _contentList.SetPadding(0, 0, 0, 16);
            _contentList.SetGap(14, 32);
            _textCapacity = GetChildComponent<StateText>("Label_capacity");
            _capacityTemplate = _textCapacity.CurrentText.text;

            _toggleGroup = GetChildComponent<CategoryList>("List_categoryList");

           
            _particle = _contentList.AddChildComponent<KParticle>("fx_ui_10_2_xuanwo_01");
            _particle.Visible = false;

            GetBagRootPos();
           // InitContentList();

            _toggleGroup.SetDataList<CategoryListItem>(rune_helper.GetBagList());
            for(int i=0;i<_toggleGroup.ItemList.Count;i++)
            {
                CategoryListItem item = _toggleGroup.ItemList[i] as CategoryListItem;
                item.SetPoint(false);
            }
        }

        private void InitTweenIcon()
        {
            if (iconContainerList == null)
            {
                iconContainerList = new List<IconContainer>();
                
                GameObject templateIcon = _contentList.TemplateGo().transform.FindChild("Container_icon").gameObject;
                Quaternion zero = new Quaternion();
                for (int i = 0; i < 5 * 3 - 1; i++)
                {
                    GameObject template = GameObject.Instantiate(templateIcon) as GameObject;
                    template.SetActive(false);
                    template.transform.SetParent(transform);
                    RectTransform templateTransform = template.transform as RectTransform;
                    templateTransform.localPosition = Vector3.zero;
                    templateTransform.localScale = Vector3.one;
                    templateTransform.localRotation = zero;
                    IconContainer icon = template.GetComponent<IconContainer>();
                    if(icon == null)
                    {
                        icon = template.AddComponent<IconContainer>();
                    }
                    iconContainerList.Add(icon);
                }
            }
        }

        private Vector2 _bagRootPos;

        private void GetBagRootPos()
        {
            RectTransform _scrollRect = _scrollPage.GetComponent<RectTransform>();
            RectTransform _maskRect = _scrollPage.GetChildComponent<RectTransform>("mask");
            _bagRootPos = _scrollRect.anchoredPosition + _maskRect.anchoredPosition;
        }

        private void OnSelectedIndexChanged(CategoryList toggle, int index)
        {
            _selectedIndex = index;
            OnRuneBagAllChange();
        }

        //private void InitContentList()
        //{
        //    List<object> dataList = new List<object>();
        //    for (int i = 0; i < public_config.MAX_COUNT_PKG_RUNE; i++)
        //    {
        //        dataList.Add(null);
        //       // _contentList.AddItem<RuneBagGrid>(null, false);
        //    }
        //    _contentList.SetDataList<RuneBagGrid>(dataList);
        //   // _contentList.DoLayout();
        //}

        public void Show()
        {
            this.gameObject.SetActive(true);
            AddEventListener();
            InitTweenIcon();
            _toggleGroup.SelectedIndex = 0;
            _selectedIndex = 0;
        }

        public void Hide()
        {
            this.gameObject.SetActive(false);
            RemoveEventListener();
        }
        
        protected void AddEventListener()
        {
           
            EventDispatcher.AddEventListener(RuneEvent.RUNE_BAG_ALL_CHANGE, OnRuneBagAllChange);
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, OnUpdateItem);
            EventDispatcher.AddEventListener(RuneEvent.RUNE_LEVEL_UP_VIEW_UPDATE, OnRuneLevelUpResp);
            EventDispatcher.AddEventListener(RuneEvent.UPDATE_CAN_EQUIPED_LIST, OnUpdateCanEquipedList);
            if (_toggleGroup!=null)
            {
                _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
                _particle.onComplete.AddListener(OnParticleComplate);
                m_GlyphCombineButton.onClick.AddListener(OnCombineButtonUp);
                _btnQuestion.onClick.AddListener(OnClickQuestion);
            }
        }

        protected void RemoveEventListener()
        {
          
            EventDispatcher.RemoveEventListener(RuneEvent.RUNE_BAG_ALL_CHANGE, OnRuneBagAllChange);
            EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, OnUpdateItem);
            EventDispatcher.RemoveEventListener(RuneEvent.RUNE_LEVEL_UP_VIEW_UPDATE, OnRuneLevelUpResp);
            EventDispatcher.RemoveEventListener(RuneEvent.UPDATE_CAN_EQUIPED_LIST, OnUpdateCanEquipedList);
            if (_toggleGroup != null)
            {
                _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
                _particle.onComplete.RemoveListener(OnParticleComplate);
                m_GlyphCombineButton.onClick.RemoveListener(OnCombineButtonUp);
                _btnQuestion.onClick.RemoveListener(OnClickQuestion);
            }
        }

        private void OnUpdateCanEquipedList()
        {
            List<KList.KListItemBase> list = _contentList.ItemList;
            for(int i=0;i<list.Count;i++)
            {
                RuneBagGrid grid = list[i] as RuneBagGrid;
                grid.UpdateCanEquiped();
            }
        }

        private void OnClickQuestion()
        {
            PanelIdEnum.CommonRule.Show(new string[] { MessageBox.DEFAULT_TITLE, (4350).ToLanguage() });
           // MessageBox.Show(true, MessageBox.DEFAULT_TITLE, (4350).ToLanguage(), null, null);
            //PanelIdEnum.CommonTips.Show((4350).ToLanguage());
        }

        private void OnRuneLevelUpResp()
        {
            RectTransform contentRect = _contentList.transform as RectTransform;
            if (posList != null && posItemIdList!=null)
            {
                RuneBagGrid bagGrid = _contentList.ItemList[posIndexDict[posList[0]]] as RuneBagGrid;
                Vector2 endPos = bagGrid.Position;
                UIManager.Instance.PlayAudio("Sound/UI/pet_update.mp3");
                _particle.Play();
                _particle.Position = endPos;
                for(int i=0;i<posItemIdList.Length;i++)
                {
                    int pos = posList[i + 1];
                    int itemId = posItemIdList[i];
                    iconContainerList[i].Visible = true;
                    RuneBagGrid bagGridItem = _contentList.ItemList[posIndexDict[pos]] as RuneBagGrid;

                    (iconContainerList[i].transform as RectTransform).anchoredPosition = bagGridItem.IconPosition + _bagRootPos + contentRect.anchoredPosition;
                    bagGridItem.Data = null;
                    iconContainerList[i].SetIcon(item_helper.GetIcon(itemId));
                    TweenPosition.Begin(iconContainerList[i].gameObject, 1, endPos + _bagRootPos + contentRect.anchoredPosition, delegate(UITweener tween)
                    {
                        if (tween.gameObject)
                        {
                            tween.gameObject.SetActive(false);
                        }
                    });
                }
            }
            else
            {
                OnTimerEnd();
            }
        }

        private void OnParticleComplate()
        {
            posList = null;
            _isCombine = false;
            OnRuneBagAllChange();
        }


        public void OnUpdateItem(BagType type, uint position)
        {
            if(type == BagType.RuneBag)
            {
                OnRuneBagAllChange();
            }
        }
        

        public void OnRuneBagAllChange()
        {
            if(_isCombine)
            {
                return;
            }
            BaseItemInfo[] itemInfoArray = new BaseItemInfo[public_config.MAX_COUNT_PKG_RUNE];
            Dictionary<int,BaseItemInfo> dataDict = PlayerDataManager.Instance.BagData.GetRuneBagData().GetAllItemInfo();
            _itemList.Clear();
            foreach (KeyValuePair<int, BaseItemInfo> kvp in dataDict)
            {
                RuneItemInfo itemInfo = kvp.Value as RuneItemInfo;
                if (_selectedIndex == 0 || _selectedIndex == itemInfo.RuneConfig.__page)
                {
                    _itemList.Add(itemInfo);
                }
            }
            _itemList.Sort(SortByLevel);
            for (int i = 0; i < _itemList.Count;i++ )
            {
                itemInfoArray[i] = _itemList[i];
            }
            
            _contentList.SetDataList<RuneBagGrid>(itemInfoArray, 5);
            _textCapacity.CurrentText.text = string.Format(_capacityTemplate,dataDict.Count,public_config.MAX_COUNT_PKG_RUNE);
        }

        private int SortByLevel(RuneItemInfo item2, RuneItemInfo item1)
        {
            if (item1.Quality != item2.Quality)
            {
                return item1.Quality - item2.Quality;
            }
            if (item1.Level != item2.Level)
            {
                return item1.Level - item2.Level;
            }
            if (item1.CurExp != item2.CurExp)
            {
                return item1.CurExp - item2.CurExp;
            }
            return item1.Id - item2.Id;
        }

        private void OnTimerEnd()
        {
            DelTimer();
            _isCombine = false;
            posList = null;
        }

        private void DelTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private uint _timerId;
        private bool _isCombine;
        
        private void OnCombineButtonUp()
        {
            if( posList != null)
            {
                return;
            }
            DelTimer();
            int start = _scrollPage.CurrentPage * NUM_PER_PAGE;
            int end = (_scrollPage.CurrentPage+1) * NUM_PER_PAGE;
            GetRuneCombineListInRange(start, end);
            GetCombineRunePosList();
            if (posList == null || posList.Length <= 1)
            {
                posList = null;
                _isCombine = false;
                if(PlayerDataManager.Instance.BagData.GetBagData(BagType.RuneBag).GetAllItemInfo().Count > 1)
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, LangEnum.RUNE_NO_RUNE_CAN_BE_COMBINED.ToLanguage(), PanelIdEnum.MainUIField);
                }
                else
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, LangEnum.RUNE_HAS_NOTHING_TO_SWALLOW.ToLanguage(), PanelIdEnum.MainUIField);
                }
               
            }
            else if(itemInfo!=null)
            {
                if(itemInfo.Level >= itemInfo.MaxLevel)
                {
                    MogoUtils.FloatTips(4352);
                    _isCombine = false;
                    posList = null;
                    return;
                }
                string content = MogoLanguageUtil.GetString(LangEnum.RUNE_COMBINE_MESSAGE, itemInfo.Name, itemInfo.Level, itemInfo.Exp2Level(totalExp));
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, delegate()
                {
                    _isCombine = true;
                    _timerId = TimerHeap.AddTimer(5000, 0, OnTimerEnd);
                    RuneManager.Instance.LevelUpBagRune(posList);
                }, delegate()
                {
                    posList = null;
                    _isCombine = false;
                });
            }
            else
            {
                RuneManager.Instance.LevelUpBagRune(posList);
            }
        }

        private void GetCombineRunePosList()
        {
            if (combineList.Count == 0)
            {
                return;
            }
            if (itemInfo != null && (itemInfo.Quality >= GlobalParams.RuneAutoSwallowLevelLimit || itemInfo.RuneLock == 1))
            {
                totalExp += itemInfo.CurExp;
                posList = new int[combineList.Count + 1];
                posItemIdList = new int[combineList.Count];
            }
            else
            {
                posList = new int[combineList.Count];
                if(itemInfo == null)
                {
                    posItemIdList = null;
                }
                else
                {
                    posItemIdList = new int[combineList.Count - 1];
                }
               
            }
            
            if(itemInfo!=null)
            {
                posList[0] = itemInfo.GridPosition;
                int index = 1;
                for (int i = 0; i < combineList.Count; i++)
                {
                    if (combineList[i] != itemInfo.GridPosition)
                    {
                        posList[index] = combineList[i];
                        posItemIdList[index - 1] = itemIdList[i];
                        index++;
                    }
                }
            }
            else
            {
                for (int i = 0; i < combineList.Count; i++)
                {
                    posList[i] = combineList[i];
                }
            }
        }



        private void GetRuneCombineListInRange(int start,int end)
        {
            combineList.Clear();
            itemIdList.Clear();
            //posDict.Clear();
            posIndexDict.Clear();
            totalExp = 0;
            itemInfo = null;
            RuneItemInfo lockedItemInfo = null;
            for (int i = start; i < end; i++)
            {
                RuneBagGrid bagGrid = null;
                
                if (i < _contentList.ItemList.Count)
                {
                    bagGrid = _contentList.ItemList[i] as RuneBagGrid;
                }
                if (bagGrid != null && bagGrid.Data!= null)//低级符文
                {
                     RuneItemInfo runeItemInfo = bagGrid.Data as RuneItemInfo;
                     posIndexDict.Add(runeItemInfo.GridPosition,i);
                    // posDict.Add(runeItemInfo.GridPosition, bagGrid.Position);
                     if (runeItemInfo.RuneLock == 0)
                     {
                         if (runeItemInfo.RuneConfig.__subtype != public_config.RUNE_EXP && runeItemInfo.RuneConfig.__subtype != public_config.RUNE_MONEY &&
                         (itemInfo == null || itemInfo.Quality < runeItemInfo.Quality || (itemInfo.Quality == runeItemInfo.Quality && itemInfo.CurExp < runeItemInfo.CurExp)))
                         {
                             itemInfo = runeItemInfo;
                         }
                         if (runeItemInfo.Quality < GlobalParams.RuneAutoSwallowLevelLimit || runeItemInfo.RuneConfig.__subtype == public_config.RUNE_EXP || runeItemInfo.RuneConfig.__subtype == public_config.RUNE_MONEY)
                         {
                             if (runeItemInfo.RuneConfig.__subtype != public_config.RUNE_MONEY)
                             {
                                 totalExp += runeItemInfo.CurExp + runeItemInfo.RuneConfig.__exp_value;
                             }
                             combineList.Add(runeItemInfo.GridPosition);
                             itemIdList.Add(runeItemInfo.Id);
                         }
                     }
                     else
                     {
                         if (runeItemInfo.RuneConfig.__subtype != public_config.RUNE_EXP && runeItemInfo.RuneConfig.__subtype != public_config.RUNE_MONEY &&
                         (lockedItemInfo == null || lockedItemInfo.Quality < runeItemInfo.Quality || (lockedItemInfo.Quality == runeItemInfo.Quality && lockedItemInfo.CurExp < runeItemInfo.CurExp)))
                         {
                             lockedItemInfo = runeItemInfo;
                             if ((itemInfo == null || itemInfo.Quality < lockedItemInfo.Quality || (itemInfo.Quality == lockedItemInfo.Quality && itemInfo.CurExp < lockedItemInfo.CurExp)))
                             {
                                 itemInfo = lockedItemInfo;
                             }
                         }
                     }
                }
            }
        }

    }
}
