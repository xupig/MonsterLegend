﻿#region 模块信息
/*==========================================
// 文件名：RuneView
// 命名空间: GameLogic.GameLogic.Modules.ModuleRune.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/7 10:21:27
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using ModuleRune.ChildView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleRune
{
    public class RuneView:KContainer
    {
        private RuneBagView _bagView;
        private RuneEquipView _equipView;
        

        protected override void Awake()
        {
            _bagView = AddChildComponent<RuneBagView>("Container_bag");
            _equipView = AddChildComponent<RuneEquipView>("Container_equip");
            
            base.Awake();
            
        }

        public void UpdateData()
        {

        }

        public void Hide()
        {
            this.gameObject.SetActive(false);
            if (_bagView!=null)
            {
                _bagView.Hide();
               
            }
            if(_equipView!=null)
            {
                _equipView.Hide();
            }
        }

        public void ShowBagView()
        {
            if (_bagView != null)
            {
                _bagView.Show();
                _bagView.OnRuneBagAllChange();
            }
            if (_equipView!=null)
            {
                 _equipView.Hide();
            }
        }

        public void ShowEquipView()
        {
            if (_bagView != null)
            {
                _bagView.Hide();
            }
            if(_equipView!=null)
            {
                _equipView.Show();
                _equipView.OnRuneBodyBagAllChange();
            }
        }

         public void SetOperationState()
        {
             if(_equipView!=null && _equipView.Visible)
             {
                 _equipView.SetOperationState();
             }
        }

    }
}
