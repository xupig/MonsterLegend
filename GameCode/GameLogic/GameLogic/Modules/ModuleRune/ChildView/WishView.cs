﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/24 11:37:56
 * 描述说明：
 * *******************************************************/

using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using ModuleRune.ChildComponent;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleRune.ChildView
{
    public class WishView : KContainer
    {

        private List<WishItem> _itemList;
        private List<KParticle> _particleList;

        private RuneCurrencyView _currencyView;


        protected override void Awake()
        {
            _currencyView = AddChildComponent<RuneCurrencyView>("Container_currency");
            _itemList = new List<WishItem>();
            _particleList = new List<KParticle>();
            for (int i = 0; i < 2;i++ )
            {
                WishItem item = AddChildComponent<WishItem>("Container_wishItem"+i);
                KParticle particle = item.AddChildComponent<KParticle>("Container_icon/fx_ui_10_1_shuijingqiu_0"+(i+1));
                _itemList.Add(item);
                _particleList.Add(particle);
            }
             AddEventListener();
        }

        public void Show()
        {
            Visible = true;
            OnWishInfoChange();
        }

        public void Hide()
        {
            Visible = false;
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(RuneEvent.WISH_INFO_CHANGE, OnWishInfoChange);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(RuneEvent.WISH_INFO_CHANGE, OnWishInfoChange);
        }

        public void UpdateWishItems()
        {
            Dictionary<int, Dictionary<int, rune_random>> wishData = rune_helper.GetWishData();
            int i = 0;
            foreach (KeyValuePair<int, Dictionary<int, rune_random>> kvp in wishData)
            {
                _itemList[i].Data = kvp.Value;
                i++;
            }
        }


        public bool Tick()
        {
            bool result = false;
            for (int i = 0; i < _itemList.Count; i++)
            {
                result = _itemList[i].Tick() | result;
            }
            return result;
        }

        public void OnWishInfoChange()
        {
            for(int i=0;i<_itemList.Count;i++)
            {
                _itemList[i].Tick();
            }
        }
    }
}
