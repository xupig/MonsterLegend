using Common.Base;
using Common.ClientConfig;
using Common.Events;
using Common.Structs;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;

namespace ModuleRune
{
	public class RuneModule:BaseModule
	{
		public RuneModule ()
		{
		}

        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }
		
		public override void Init()
		{
            RegisterPopPanelBunchPanel(PanelIdEnum.RuneLevelUp, PanelIdEnum.Rune);
            AddPanel(PanelIdEnum.Rune, "Container_RunePanel", MogoUILayer.LayerUIPanel, "ModuleRune.RunePanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.RuneLevelUp, "Container_RuneLevelUpPanel", MogoUILayer.LayerUIPopPanel, "ModuleRune.RuneUpgradePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            
            AddPanel(PanelIdEnum.RuneAdd, "Container_RuneAddPanel", MogoUILayer.LayerUIToolTip, "ModuleRune.RuneAddPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.RuneEffect, "Container_RuneEffect", MogoUILayer.LayerEffect, "ModuleRune.RuneEffectPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
		}


	}
}