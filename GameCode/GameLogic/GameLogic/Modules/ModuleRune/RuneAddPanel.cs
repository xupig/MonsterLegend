﻿#region 模块信息
/*==========================================
// 文件名：RuneLevelUpBagView
// 命名空间: GameLogic.GameLogic.Modules.ModuleGlyphVow
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/14 15:15:17
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleRune.ChildComponent;
using ModuleRune.ChildView;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleRune
{
    public class RuneAddPanel : BasePanel
    {

        public static int NUM_PER_PAGE = 15;
        #region 私有变量
        private KButton _btnConfirm;
        private StateText _txtNum;
        private PageableScrollView _scrollView;
        //private List<RuneAddGrid> _gridList;
        private List<int> _itemInfoList;
        private List<RuneItemInfo> _selectDataList;//满足吞噬条件的符文
        public static int currentNum;
        private string numTemplate;
        #endregion

        #region 基础接口实现

        protected override void Awake()
        {
            //KContainer _container = this.gameObject.GetComponent<KContainer>();
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _scrollView = AddChildComponent<PageableScrollView>("ScrollView_tianjiafuwen");
            _scrollView.IsPageable = false;
            _scrollView.SetCapacity(30);
            _scrollView.SetDirection(4, int.MaxValue);
            _scrollView.SetGap(16,14);
            _btnConfirm = GetChildComponent<KButton>("Button_tianjia");
            _txtNum = GetChildComponent<StateText>("Label_txtfanye");
            numTemplate = _txtNum.CurrentText.text;
            //_gridList = new List<RuneAddGrid>();
            _itemInfoList = new List<int>();
            _selectDataList = new List<RuneItemInfo>();
            //InitGrids();
        }

        

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.RuneAdd; }
        }

        private void AddEventListener()
        {
            _btnConfirm.onClick.AddListener(OnConfirmClick);
            EventDispatcher.AddEventListener<bool, RuneItemInfo>(RuneEvent.ADD_RUNE_ITEM, OnClickGrid);
        }

        private void RemoveEventListener()
        {
            _btnConfirm.onClick.RemoveListener(OnConfirmClick);
            EventDispatcher.RemoveEventListener<bool, RuneItemInfo>(RuneEvent.ADD_RUNE_ITEM, OnClickGrid);
        }

        public override void OnShow(System.Object data)
        {
            currentNum = 0;
            Refresh();
            AddEventListener();
        }

        public override void OnClose()
        {
            RemoveEventListener();
            if (_itemInfoList!=null)
            {
                _itemInfoList.Clear();
            }
            for (int i = 0; i < _selectDataList.Count; i++)
            {
                if (_selectDataList[i]!=null)
                {
                    _selectDataList[i].IsSelect = false;
                }
            }
            _selectDataList.Clear();
        }

        private void Refresh()
        {
            GetSelectedDataList();
            UpdateNum();
        }

        private void GetSelectedDataList()
        {
            RuneItemInfo condition = PlayerDataManager.Instance.BagData.GetBagData(RuneManager.levelUpData.bagType).GetItemInfo(RuneManager.levelUpData.pos) as RuneItemInfo;
            Dictionary<int, BaseItemInfo> dic = PlayerDataManager.Instance.BagData.GetRuneBagData().GetAllItemInfo();
            _selectDataList.Clear();
            foreach (KeyValuePair<int, BaseItemInfo> kvp in dic)
            {
                RuneItemInfo itemInfo = kvp.Value as RuneItemInfo;
               
                if (kvp.Value != null && itemInfo.IsSelectable && RuneManager.NotSelectedRune(kvp.Key) && condition.CanCombine(itemInfo) && RuneUpgradePanel.IsNotSelected(itemInfo.GridPosition))//不锁定,可吞噬，或者被吞噬的符文比吞噬符文的等级高
                {
                    _selectDataList.Add(itemInfo);
                }
            }
            while (_selectDataList.Count < 16)
            {
                _selectDataList.Add(null);
            }
            while(_selectDataList.Count%4!=0)
            {
                _selectDataList.Add(null);
            }
            _scrollView.SetDataList<RuneAddGrid>(_selectDataList,1,1);
        }

        private void UpdateNum()
        {
            _txtNum.CurrentText.text = string.Format(numTemplate, currentNum, RuneManager.levelUpData.maxNeedNum);
        }

        #endregion

        #region UI事件回调处理

        private void OnConfirmClick()
        {
            EventDispatcher.TriggerEvent(RuneEvent.RUNE_LEVEL_UP_PUT_ON, _itemInfoList);
            ClosePanel();
        }

        public void OnClickGrid(bool isAdd, BaseItemInfo itemInfo)
        {
            if(itemInfo != null)
            {
                if (isAdd)
                {
                    AddItem(itemInfo);
                }
                else if (_itemInfoList.IndexOf(itemInfo.GridPosition) != -1)
                {
                    RemoveItem(itemInfo);
                }
            }
        }

        private void AddItem(BaseItemInfo itemInfo)
        {
            _itemInfoList.Add(itemInfo.GridPosition);
            currentNum++;
            UpdateNum();
        }

        private void RemoveItem(BaseItemInfo itemInfo)
        {
            _itemInfoList.Remove(itemInfo.GridPosition);
            currentNum--;
            UpdateNum();
        }

        #endregion
    }
}
