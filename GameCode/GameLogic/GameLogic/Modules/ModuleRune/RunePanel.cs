﻿using Common.Base;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;
using D = GameLoader.Utils.LoggerHelper;
using GameMain.GlobalManager.SubSystem;
using GameMain.Entities;
using MogoEngine;
using Common.Structs;
using Common.Events;
using ModuleRune.ChildView;
using MogoEngine.Mgrs;
using UnityEngine;
using Common.ClientConfig;
using Common.Data;
using Common.Structs.ProtoBuf;
using GameLoader.Utils.Timer;
using System.Collections.Generic;
using Common.ServerConfig;
using GameData;

namespace ModuleRune
{
    public class RunePanel : BasePanel
    {
        private KToggleGroup _toggleGroup;

        
        private WishView _wishView;
        private RuneView _runeView;

        private const int WISH_VIEW_INDEX = 0;
        private const int RUNE_EQUIP_VIEW_INDEX = 1;
        private const int RUNE_BAG_VIEW_INDEX = 2;

        public static bool isShow = false;

        private uint m_uTimerId;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Rune; }
        }

        //private HashSet<int> bodyRuneCanEquipedSet;
       

        /**提取UI子级组件[UI资源加载完成时调用]**/
        protected override void Awake()
        {
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close02");
            _runeView = AddChildComponent<RuneView>("Container_Rune");
            _wishView = AddChildComponent<WishView>("Container_Vow");
            _runeView.gameObject.SetActive(false);
           
        }


        public override void OnShow(System.Object data)
        {
            isShow = true;
            OnSelect(_toggleGroup, WISH_VIEW_INDEX);
            _toggleGroup.SelectIndex = WISH_VIEW_INDEX;
            _wishView.UpdateWishItems();
            AddEventListener();
            CheckCanEquipedRune();
            m_uTimerId = TimerHeap.AddTimer(0, 500, Tick);
        }

        public override void OnClose()
        {
            isShow = false;
            RemoveEventListener();
            _wishView.Hide();
            _runeView.Hide();
            PanelIdEnum.RuneToolTips.Close();
            PanelIdEnum.ExpRuneToolTips.Close();
            if (m_uTimerId != 0)
            {
                TimerHeap.DelTimer(m_uTimerId);
                m_uTimerId = 0;
            }
        }

        public void Tick()
        {
            _toggleGroup.GetToggleList()[0].SetGreenPoint(_wishView.Tick());
        }

        private void AddEventListener()
        {
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, this.UpdateLevel);
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelect);
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, BagItemUpdate);
        }

        private void RemoveEventListener()
        {
           
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, this.UpdateLevel);
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelect);
            EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, BagItemUpdate);
        }

        private void BagItemUpdate(BagType bagType,uint id)
        {
            if(bagType == BagType.RuneBag || bagType == BagType.BodyRuneBag)//符文背包或者符文装备背包
            {
                CheckCanEquipedRune();
            }
        }

        private void CheckCanEquipedRune()
        {
            Dictionary<int,HashSet<int>> dataSet = PlayerDataManager.Instance.BagData.GetRuneBagData().bagRuneCanEquipedSet;
            dataSet.Clear();
            Dictionary<int, BaseItemInfo> dataDict = PlayerDataManager.Instance.BagData.GetRuneBagData().GetAllItemInfo();
            foreach(KeyValuePair<int,BaseItemInfo> kvp in dataDict)
            {
                RuneItemInfo itemInfo = kvp.Value as RuneItemInfo;
                if (itemInfo!=null && itemInfo.RuneConfig.__subtype != public_config.RUNE_MONEY && itemInfo.RuneConfig.__subtype != public_config.RUNE_EXP)
                {
                    if(CanEquiped(itemInfo))
                    {
                        if(dataSet.ContainsKey(itemInfo.RuneConfig.__page) == false)
                        {
                            dataSet.Add(itemInfo.RuneConfig.__page, new HashSet<int>());
                        }
                        dataSet[itemInfo.RuneConfig.__page].Add(itemInfo.GridPosition);
                    }
                }
            }
            _toggleGroup.GetToggleList()[1].SetGreenPoint(dataSet.Count>0);
            if(_runeView!=null && _runeView.Visible)
            {
                _runeView.SetOperationState();
            }
            EventDispatcher.TriggerEvent(RuneEvent.UPDATE_CAN_EQUIPED_LIST);
        }

        private bool CanEquiped(RuneItemInfo itemInfo)
        {
            BagData runeBodyBagData = PlayerDataManager.Instance.BagData.GetBodyRuneBagData();
            Dictionary<int, int> holeDict = rune_helper.GetPageHoleIndex(itemInfo.RuneConfig.__page);
            string subType = itemInfo.RuneConfig.__subtype.ToString();
            List<int> idList = new List<int>();
            foreach(KeyValuePair<int,int> kvp in holeDict)
            {
                int id = kvp.Value;
                RuneItemInfo equipedItemInfo = runeBodyBagData.GetItemInfo(id) as RuneItemInfo;
                if (equipedItemInfo!=null)
                {
                    if (itemInfo.RuneConfig.__subtype == equipedItemInfo.RuneConfig.__subtype)
                    {
                        int attribute1 = fight_force_helper.GetFightForce(rune_helper.GetLevelAttribute(itemInfo.RuneConfig, itemInfo.Level), PlayerAvatar.Player.vocation);
                        int attribute2 = fight_force_helper.GetFightForce(rune_helper.GetLevelAttribute(equipedItemInfo.RuneConfig, equipedItemInfo.Level), PlayerAvatar.Player.vocation);
                        return attribute1 > attribute2;
                    }
                }
                else
                {
                    idList.Add(id);
                }
            }
            for (int i = 0; i < idList.Count;i++ )
            {
                rune_hole hole = rune_helper.GetRuneHole(idList[i]);
                if (hole.__unlock_level <= PlayerAvatar.Player.level && (hole.__subtypes == null || hole.__subtypes.Count == 0 || hole.__subtypes.Contains(subType)))
                {
                    return true;
                }
            }
            return false;
        }

        public void UpdateLevel()
        {
            _wishView.UpdateWishItems();
        }


        void OnSelect(KToggleGroup toggleGroup,int nIndex)
        {
            switch (nIndex)
            {
                case WISH_VIEW_INDEX:
                    ShowWishView();
                    break;
                case RUNE_EQUIP_VIEW_INDEX:
                    ShowRuneEquipView();
                    break;
                case RUNE_BAG_VIEW_INDEX:
                    ShowRuneBagView();
                    break;
                default:
                    D.Error("cannot find the state of "+nIndex);
                    break;
            }
        }

        private void ShowWishView()
        {
            _wishView.Show();
            _runeView.Hide();
        }

        private void ShowRuneEquipView()
        {
            _wishView.Hide();
            _runeView.gameObject.SetActive(true);
            _runeView.ShowEquipView();
        }

        private void ShowRuneBagView()
        {
            _wishView.Hide();
            _runeView.gameObject.SetActive(true);
            _runeView.ShowBagView();
        }

       
    }
}

