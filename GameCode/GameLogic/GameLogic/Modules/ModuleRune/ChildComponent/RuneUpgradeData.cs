﻿#region 模块信息
/*==========================================
// 文件名：RuneLevelUpSelectCondition
// 命名空间: GameLogic.GameLogic.Modules.ModuleGlyphVow.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/14 15:30:45
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Structs;
using UnityEngine;

namespace ModuleRune.ChildComponent
{
    public class RuneUpgradeData
    {
        public int maxNeedNum = 5;
        public BagType bagType = 0;

        private int _pos;
        public int pos
        {
            get { return _pos; }
            set { _pos = value;}
        }
    }
}
