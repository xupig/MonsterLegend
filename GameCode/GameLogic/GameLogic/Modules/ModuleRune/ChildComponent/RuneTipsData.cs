﻿#region 模块信息
/*==========================================
// 文件名：GlyphBagData
// 命名空间: GameLogic.GameLogic.Modules.ModuleGlyphVow.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/10 11:24:03
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleRune.ChildComponent
{
    public class RuneTipsData
    {
        //public ItemInfo itemInfo;
        public int pos;
        public BagType bagType;
        public int toPos = -1;
    }
}
