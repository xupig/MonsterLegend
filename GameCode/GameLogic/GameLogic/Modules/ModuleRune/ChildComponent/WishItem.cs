﻿#region 模块信息
/*==========================================
// 文件名：VowItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleGlyphVow.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/19 11:16:29
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleBag;
using ModuleCommonUI;
using MogoEngine;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleRune.ChildComponent
{
    public class WishItem : KList.KListItemBase
    {
        private static string freeCountContent;

        
        private StateText _txtName;
        private IconContainer _imgTenCost;
        private StateText _txtTenCost;

        private IconContainer _imgOnceCost;
        private StateText _txtOnceCost;
        
        private KButton _btnFree;
        private KButton _btnOnce;
        private KButton _btnTen;
        private StateText _txtCount;
        private IconContainer _iconContainer;
        private StateImage _freeImage;
        private int _location;
        private int _cd;
        private int _max;

        private static bool _noTipsAgain = false;

        protected override void Awake()
        {
            KContainer _container = this.gameObject.GetComponent<KContainer>();
            _txtName = _container.GetChildComponent<StateText>("Label_txtName");
            _txtCount = _container.GetChildComponent<StateText>("Label_txtFreeCount");
            if (freeCountContent==null)
            {
                freeCountContent = _txtCount.CurrentText.text;
            }
            _btnFree = _container.GetChildComponent<KButton>("Button_btn1");
            _btnOnce = _container.GetChildComponent<KButton>("Button_btn2");
            _btnTen = _container.GetChildComponent<KButton>("Button_btn3");

            _imgTenCost = _container.AddChildComponent<IconContainer>("Button_btn3/cost");
            _txtTenCost = _container.GetChildComponent<StateText>("Button_btn3/label02");

            _imgOnceCost = _container.AddChildComponent<IconContainer>("Button_btn2/cost");
            _txtOnceCost = _container.GetChildComponent<StateText>("Button_btn2/label02");

            _iconContainer = _container.AddChildComponent<IconContainer>("Container_icon/Container_image");
            _freeImage = _container.GetChildComponent<StateImage>("Image_daojishidi");
        }

        public override void Dispose()
        {
            RemoveEvents();
            base.Data = null;
        }

        private void AddEvents()
        {
            _btnFree.onClick.AddListener(OnClickFreeHandler);
            _btnOnce.onClick.AddListener(OnClickOnceHandler);
            _btnTen.onClick.AddListener(OnClickTenHandler);
          
        }

        private void RemoveEvents()
        {
            _btnFree.onClick.RemoveListener(OnClickFreeHandler);
            _btnOnce.onClick.RemoveListener(OnClickOnceHandler);
            _btnTen.onClick.RemoveListener(OnClickTenHandler);
            
        }

        public override void Show()
        {
            AddEvents();
        }

        public override void Hide()
        {
            RemoveEvents();
        }

        public override object Data
        {
            get
            {
                return base.Data;
            }
            set
            {
                base.Data = value;
                this.gameObject.SetActive(value != null);
                if (value != null)
                {
                    UpdateContent();
                }
            }
        }

        private int[] GetCostType(rune_random random)
        {
            int[] cost = new int[] { 0, 0 };
            if(random.__cost == null)
            {
                return cost;
            }
            foreach(KeyValuePair<string,string> kvp in random.__cost)
            {
                cost[0] = int.Parse(kvp.Key);
                cost[1] = int.Parse(kvp.Value);
                return cost;
            }
            return cost;
        }

        private void AddCostImage(int costType,IconContainer go)
        {
            go.SetIcon(item_helper.GetIcon(costType));
        }

        private void SetCostOnce(int[] cost)
        {
            AddCostImage(cost[0],_imgOnceCost);
            _txtOnceCost.ChangeAllStateText(string.Concat("x",cost[1]));
        }

        private void SetCostTen(int[] cost)
        {
            AddCostImage(cost[0], _imgTenCost);
            _txtTenCost.ChangeAllStateText(string.Concat("x", cost[1]));
        }

        private void UpdateContent()
        {
            Dictionary<int, rune_random> dic = Data as Dictionary<int, rune_random>;
            SetLocation(dic);
            UpdateButtonsState(dic);
            SetItemContents();
        }

        private void SetLocation(Dictionary<int, rune_random> dic)
        {
            foreach (KeyValuePair<int, rune_random> kvp in dic)
            {
                _location = kvp.Value.__location;
                return;
            }
        }

        private void UpdateButtonsState(Dictionary<int, rune_random> dic)
        {
            _freeImage.Visible = false;
            _txtCount.Visible = false;
            _btnFree.gameObject.SetActive(false);

            SetBtnOnceState();
            if (dic.ContainsKey(public_config.RUNE_WISH_ONE))
            {
                SetCostOnce(GetCostType(dic[public_config.RUNE_WISH_ONE]));
            }

            _btnTen.gameObject.SetActive(dic.ContainsKey(public_config.RUNE_WISH_TEN));
            if (dic.ContainsKey(public_config.RUNE_WISH_TEN))
            {
                SetCostTen(GetCostType(dic[public_config.RUNE_WISH_TEN]));
            }
        }

        private void SetItemContents()
        {
            _txtName.CurrentText.text = MogoLanguageUtil.GetString((LangEnum)GlobalParams.GetLocationName(_location));
            _iconContainer.SetIcon(GlobalParams.GetRuneLocationIcon(_location));
            _txtName.CurrentText.color = ColorDefine.GetColorById(GlobalParams.GetRuneLocationColor(_location));
            _max = GlobalParams.GetRuneLocationDailyTimes(_location);
            _cd = GlobalParams.GetRuneLocationCd(_location);
        }


        public bool Tick()
        {
            uint lastUseTime = RuneManager.Instance.GetLastUseTime(_location);
            int remainTimes = RuneManager.Instance.GetRemainTimes(_location);
            if (lastUseTime != 0 && remainTimes < _max)
            {
                
                long left = (long)_cd * 1000 - (long)Global.serverTimeStamp + (long)lastUseTime * 1000;
                if (left < 0)
                {
                    if(Visible)
                    {
                        Timeout();
                    }
                    return true;
                }
                else
                {
                    if(Visible)
                    {
                        TimeCount(left);
                    }
                }
            }
            else
            {
                if(Visible)
                {
                    RemainTimeExhaust(remainTimes);
                }
                return remainTimes < _max;
            }
            return false;
        }

        private void RemainTimeExhaust(int remainTimes)
        {
            _freeImage.Visible = remainTimes >= _max;
            _txtCount.Visible = remainTimes >= _max;
            _txtCount.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.RUNE_NO_FREE_TIMES);
            _btnFree.gameObject.SetActive(remainTimes < _max);
            SetBtnOnceState();
        }

        private void SetBtnOnceState()
        {
            if(_btnFree.Visible)
            {
                _btnOnce.Visible = false;
            }
            else
            {
                if(Data == null)
                {
                    _btnOnce.Visible = true;
                }
                else
                {
                    Dictionary<int, rune_random> dic = Data as Dictionary<int, rune_random>;
                    _btnOnce.Visible = dic.ContainsKey(public_config.RUNE_WISH_ONE);
                }
               
            }
        }

        private void Timeout()
        {
            RuneManager.Instance.SetLastUseTime(_location, 0);
            _freeImage.Visible = false;
            _txtCount.Visible = false;
            _btnFree.gameObject.SetActive(true);
            SetBtnOnceState();
        }

        private void TimeCount(long left)
        {
            _freeImage.Visible = true;
            _txtCount.Visible = true;
            _btnFree.gameObject.SetActive(false);
            SetBtnOnceState();
            _txtCount.CurrentText.text = string.Format(freeCountContent, MogoTimeUtil.ToUniversalTime(Convert.ToInt32(left / 1000)));
        }

        private void OnClickFreeHandler()
        {
            OnClickRefreshRune(1, public_config.RUNE_WISH_FREE);
        }

        private void OnClickOnceHandler()
        {
            OnClickRefreshRune(1, public_config.RUNE_WISH_ONE);
        }

        private void OnClickTenHandler()
        {
            Dictionary<int, rune_random> _dic = Data as Dictionary<int, rune_random>;
            rune_random hole = _dic[public_config.RUNE_WISH_TEN];
            if (hole.__cost.ContainsKey(public_config.MONEY_TYPE_DIAMOND.ToString()))
            {
                if (_noTipsAgain == true)
                {
                    OnClickRefreshRune(10, public_config.RUNE_WISH_TEN);
                }
                else
                {
                    int cost = int.Parse(hole.__cost[public_config.MONEY_TYPE_DIAMOND.ToString()]);
                    string content = cost + item_helper.GetName(public_config.MONEY_TYPE_DIAMOND);
                    BagTipsPanel.Show(string.Empty, (4356).ToLanguage(content), delegate(bool noTips)
                    {
                        _noTipsAgain = noTips;
                        RuneManager.Instance.OnRefreshRune(hole.__id);
                    });
                }
            }
            else
            {
                OnClickRefreshRune(10, public_config.RUNE_WISH_TEN);
            }
        }

        private void OnClickRefreshRune(int needCellNum,int refreshType)
        {
            if (HasEnoughCell(needCellNum))
            {
                UIManager.Instance.PlayAudio("Sound/UI/rune_wish.mp3");
                OnRefreshRune(refreshType, needCellNum);
            }
            else
            {
                SystemInfoManager.Instance.Show(error_code.ERR_RUNE_BAG_FULL);
            }
        }

        private bool HasEnoughCell(int count)
        {
            return public_config.MAX_COUNT_PKG_RUNE - PlayerDataManager.Instance.BagData.GetRuneBagData().GetAllItemInfo().Count - count >= 0;
        }

        private void OnRefreshRune(int type, int needCellNum)
        {
            Dictionary<int, rune_random> _dic = Data as Dictionary<int, rune_random>;
            rune_random hole = _dic[type];
            int result = PlayerAvatar.Player.CheckCostLimit(hole.__cost,true);
            if(result <= 0)//
            {
                
                if(result == -1)
                {
                    int cost = int.Parse(hole.__cost[public_config.MONEY_TYPE_BIND_DIAMOND.ToString()]);
                    string bindName = item_helper.GetName(public_config.MONEY_TYPE_BIND_DIAMOND);
                    cost = cost - PlayerAvatar.Player.money_bind_diamond;
                    string name = item_helper.GetName(public_config.MONEY_TYPE_DIAMOND);
                    int oneAlert = 4148;//花费xx元宝许愿一次
                    int tenAlert = 4149;//花费xx元宝许愿十次
                    string content = needCellNum == 1 ? oneAlert.ToLanguage(bindName, cost, name) : tenAlert.ToLanguage(bindName, cost, name);
                    MessageBox.Show(true, string.Empty,content ,delegate()
                    {
                            RuneManager.Instance.OnRefreshRune(hole.__id);
                    },null);
                }
                else
                {
                     RuneManager.Instance.OnRefreshRune(hole.__id);
                }
            }
            else
            {
                if (hole.__cost.ContainsKey( public_config.MONEY_TYPE_RUNE_STONE.ToString()))
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.ItemChannel, public_config.MONEY_TYPE_RUNE_STONE);
                }
            }
        }

    }
}
