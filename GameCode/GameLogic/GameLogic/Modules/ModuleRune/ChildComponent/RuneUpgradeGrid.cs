﻿#region 模块信息
/*==========================================
// 文件名：RuneUpgradeGrid
// 命名空间: GameLogic.GameLogic.Modules.ModuleRune.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/7 10:54:42
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleRune
{
    public class RuneUpgradeGrid:KContainer
    {
        //private KContainer _icon;
        private ItemGrid _itemGrid;
        private StateImage _addIcon;
        private StateText _txtName;
        private StateText _txtLevel;
        private KButton _btnButton;
        private static string template;

        public KComponentEvent<RuneUpgradeGrid> onGridClick = new KComponentEvent<RuneUpgradeGrid>();

        public RuneItemInfo ItemInfo
        {
            get { return  PlayerDataManager.Instance.BagData.GetRuneBagData().GetItemInfo(_itemPos) as RuneItemInfo;}
        }

        private int _itemPos;
        public int ItemPos { 
            get 
            { 
                return _itemPos;
            }
            set
            {
                UpdateItemInfoState(value);
                _itemPos = value;
                RefreshGrid();
            }
        }

        private void UpdateItemInfoState(int value)
        {
            if (value == 0)
            {
                RuneItemInfo info = ItemInfo;
                if (info != null)
                {
                    info.IsSelect = false;
                }
            }
            else
            {
                RuneItemInfo info = PlayerDataManager.Instance.BagData.GetRuneBagData().GetItemInfo(value) as RuneItemInfo;
                if(info!=null)
                {
                    info.IsSelect = true;
                }
            }
        }

        protected override void Awake()
        {
            base.Awake();
            _btnButton = GetComponent<KButton>();
            _itemGrid = GetChildComponent<ItemGrid>("wupin");
           
            _addIcon = GetChildComponent<StateImage>("sharedTianjia");
           // _icon = _itemGrid.GetChildComponent<KContainer>("Container_icon");
            _txtName = GetChildComponent<StateText>("txtName");
            _txtLevel = GetChildComponent<StateText>("txtLevel");
           
           // _addIcon.Raycast = false;
            //_txtName.Raycast = false;
            //_txtLevel.Raycast = false;

            if (template == null)
            {
                template = _txtLevel.CurrentText.text;
            }
           // _icon.gameObject.SetActive(false);
            AddEventListener();
        }

        protected override void OnDestroy()
        {
            _btnButton.onClick.RemoveListener(OnClickGrid);
            _itemGrid.onClick = null;
            base.OnDestroy();
        }

        private void AddEventListener()
        {
            _btnButton.onClick.AddListener(OnClickGrid);
            _itemGrid.onClick = OnClickGrid;
        }

        private void OnClickGrid()
        {
            if (ItemPos == 0)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.RuneAdd);
            }
            else
            {
                ItemPos = 0;
                onGridClick.Invoke(this);
            }
        }

        private void RefreshGrid()
        {
            if (_itemPos != 0)
            {
                RuneItemInfo info = ItemInfo;
                _itemGrid.SetItemData(info);
                _itemGrid.onClick = OnClickGrid;
                _txtName.gameObject.SetActive(true);
                _txtName.CurrentText.text = info.Name;
                _txtName.CurrentText.color = item_helper.GetColor(info.Id);
                _addIcon.gameObject.SetActive(false);
                _txtLevel.gameObject.SetActive(true);
                _txtLevel.CurrentText.text = string.Format(template,info.Level);
            }
            else
            {
                _txtName.gameObject.SetActive(false);
                _txtLevel.gameObject.SetActive(false);
                _addIcon.gameObject.SetActive(true);
                _itemGrid.Clear();
            }
        }
    }
}
