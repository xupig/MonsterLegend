﻿#region 模块信息
/*==========================================
// 文件名：RuneBagItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleRune.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/7 20:07:52
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using ModuleRune.ChildComponent;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleRune
{
    public class RuneBagItem:KList.KListItemBase
    {
        private IconContainer _iconContainer;
        private StateText _txtName;
        private StateText _txtLevel;
        private StateImage _imgLock;

        private static string levelTemplate;
        
        private RuneItemInfo _itemInfo;

        private StateImage _canEquipedImg;

        private static RuneTipsData bagData = new RuneTipsData();
        protected override void Awake()
        {
            _canEquipedImg = GetChildComponent<StateImage>("Image_hechengIcon02");
            _canEquipedImg.Visible = false;
            _iconContainer = AddChildComponent<IconContainer>("Container_fuwenxinxi/Container_icon");
            _txtName = GetChildComponent<StateText>("Container_fuwenxinxi/Label_txtFuwenmingcheng");
            _txtLevel = GetChildComponent<StateText>("Container_fuwenxinxi/Label_txtDengji");
            _imgLock = GetChildComponent<StateImage>("Container_fuwenxinxi/Image_suo");
            if (levelTemplate== null)
            {
                levelTemplate = _txtLevel.CurrentText.text;
            }
            onClick.AddListener(OnClickHandler);
        }

        public override object Data
        {
            get
            {
                return _itemInfo;
            }
            set
            {
                _itemInfo = value as RuneItemInfo;
                Refresh();
            }
        }

        public override void Dispose()
        {
            onClick.RemoveListener(OnClickHandler);
        }

        public override void Show()
        {
            EventDispatcher.AddEventListener<BagType,int, RuneItemInfo>(RuneEvent.RUNE_BAG_ITEM_CHANGE, OnRuneBagItemChange);
            base.Show();
        }

        public override void Hide()
        {
            EventDispatcher.RemoveEventListener<BagType,int, RuneItemInfo>(RuneEvent.RUNE_BAG_ITEM_CHANGE, OnRuneBagItemChange);
            base.Hide();
        }

        private void OnRuneBagItemChange(BagType bagType,int position, RuneItemInfo info)
        {
            if (_itemInfo != null && gameObject.activeSelf && bagType == BagType.RuneBag)
            {
                if (_itemInfo.GridPosition == info.GridPosition)
                {
                    _itemInfo = info;
                    _imgLock.gameObject.SetActive(_itemInfo.RuneLock == 1);
                }
            }
        }

        private void Refresh()
        {
            if(_itemInfo!=null)
            {
                _txtLevel.CurrentText.text = string.Format(levelTemplate,_itemInfo.Level);
                _txtName.CurrentText.text = _itemInfo.Name;
                _txtName.CurrentText.color = item_helper.GetColor(_itemInfo.Id);
                _imgLock.gameObject.SetActive(_itemInfo.RuneLock == 1);
                _iconContainer.SetIcon(_itemInfo.Icon);
                _canEquipedImg.Visible = PlayerDataManager.Instance.BagData.GetRuneBagData().CanEquiped(_itemInfo.RuneConfig.__page,_itemInfo.GridPosition);
            }
            else
            {
                _canEquipedImg.Visible = false;
            }
        }

        private void OnClickHandler(KList.KListItemBase item,int index)
        {
            if (_itemInfo != null)
            {
                if (_itemInfo.RuneConfig.__subtype == public_config.RUNE_MONEY)
                {
                    ShowExpRuneTips();
                }
                else if (_itemInfo.RuneConfig.__subtype == public_config.RUNE_EXP)
                {
                    ShowExpRuneTips();
                }
                else
                {
                    ShowRuneTips();
                }
            }
        }

        private void ShowExpRuneTips()
        {
            bagData.pos = _itemInfo.GridPosition;
            bagData.toPos = RuneEquipView.toPos;
            bagData.bagType = BagType.RuneBag;
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.ExpRuneToolTips, bagData, PanelIdEnum.Rune);
        }

        private void ShowRuneTips()
        {
            bagData.pos = _itemInfo.GridPosition;
            bagData.toPos = RuneEquipView.toPos;
            bagData.bagType = BagType.RuneBag;
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.RuneToolTips, bagData, PanelIdEnum.Rune);
        }

    }
}
