﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/22 18:01:57
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using ModuleCommonUI;
using MogoEngine.Events;
using UnityEngine;

namespace ModuleRune.ChildComponent
{
    public class RuneBagGrid : KList.KListItemBase
    {
        private IconContainer m_iconContainer;
        private StateText m_txtName;
        private StateText m_txtLevel;
        private StateImage m_iconLock;
        private StateImage _imgShadow;

        private StateImage _imgEmpty;
        private StateImage _imgFill;

        private static string levelTemplate = "Lv{0}";

        private RuneItemInfo _runeItemInfo;

        private static RuneTipsData bagData = new RuneTipsData();
        private StateImage _canEquipedImg;

        private RectTransform _rectTransform;
        private Vector2 _iconPos;


        #region 基础接口实现

        protected override void Awake()
        {
            _canEquipedImg = GetChildComponent<StateImage>("Image_hechengIcon02");
            _canEquipedImg.Visible = false;
            m_iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _iconPos = m_iconContainer.GetComponent<RectTransform>().anchoredPosition;
            m_txtName = GetChildComponent<StateText>("Label_txtName");
            m_txtLevel = GetChildComponent<StateText>("Label_txtLevel");
            m_iconLock = GetChildComponent<StateImage>("Image_suo");
            _imgShadow = GetChildComponent<StateImage>("Image_zhuangbeitouying");
            m_iconLock.gameObject.SetActive(false);
            onClick.AddListener(OnGridClick);
            _imgEmpty = GetChildComponent<StateImage>("Image_sharedkuang03");
            _imgFill = GetChildComponent<StateImage>("Image_sharedkuang02");
            _rectTransform = gameObject.GetComponent<RectTransform>();
        }

        public Vector2 IconPosition
        {
            get
            {
                return _rectTransform.anchoredPosition + _iconPos; 
            }
        }

        public Vector2 Position
        {
            get
            {
                return _rectTransform.anchoredPosition;
            }
        }

        public override void Dispose()
        {
            onClick.RemoveListener(OnGridClick);
        }

        public override void Show()
        {
            EventDispatcher.AddEventListener<BagType, int, RuneItemInfo>(RuneEvent.RUNE_BAG_ITEM_CHANGE, OnRuneBagItemChange);
            base.Show();
        }

        public override void Hide()
        {
            EventDispatcher.RemoveEventListener<BagType, int, RuneItemInfo>(RuneEvent.RUNE_BAG_ITEM_CHANGE, OnRuneBagItemChange);
            base.Hide();
        }

        private void OnRuneBagItemChange(BagType bagType, int position, RuneItemInfo info)
        {
            if (_runeItemInfo != null && gameObject.activeSelf && bagType == BagType.RuneBag)
            {
                if (_runeItemInfo.GridPosition == info.GridPosition)
                {
                    _runeItemInfo = info;
                    m_iconLock.gameObject.SetActive(_runeItemInfo.RuneLock == 1);
                }
            }
        }

        public override object Data
        {
            get
            {
                return _runeItemInfo;
            }
            set
            {
                _runeItemInfo = value as RuneItemInfo;
                Refresh();
            }
        }

        protected void Refresh()
        {
            if (_runeItemInfo != null)
            {
                _imgShadow.gameObject.SetActive(true);
               
                m_iconContainer.gameObject.SetActive(true);
                m_txtName.gameObject.SetActive(true);
                m_txtLevel.gameObject.SetActive(true);
                m_txtLevel.CurrentText.text = string.Format(levelTemplate, _runeItemInfo.Level);
                m_txtName.CurrentText.text = item_helper.GetName(_runeItemInfo.Id);
                m_iconLock.gameObject.SetActive(_runeItemInfo.RuneLock == 1);
                m_txtName.CurrentText.color = item_helper.GetColor(_runeItemInfo.Id);
                m_iconContainer.SetIcon(_runeItemInfo.Icon);
                _imgFill.Visible = true;
                _imgEmpty.Visible = false;
                _canEquipedImg.Visible = PlayerDataManager.Instance.BagData.GetRuneBagData().CanEquiped(_runeItemInfo.RuneConfig.__page,_runeItemInfo.GridPosition);
            }
            else
            {
                _imgFill.Visible = false;
                _imgEmpty.Visible = true;
                m_iconLock.gameObject.SetActive(false);
                _imgShadow.gameObject.SetActive(false);
                m_iconContainer.gameObject.SetActive(false);
                m_txtName.gameObject.SetActive(false);
                m_txtLevel.gameObject.SetActive(false);
                _canEquipedImg.Visible = false;
            }
        }

        public void UpdateCanEquiped()
        {
            if (_runeItemInfo != null)
            {
                _canEquipedImg.Visible = PlayerDataManager.Instance.BagData.GetRuneBagData().CanEquiped(_runeItemInfo.RuneConfig.__page,_runeItemInfo.GridPosition);
            }
            else
            {
                _canEquipedImg.Visible = false;
            }
        }

        #endregion


        #region UI回调事件处理
        private void OnGridClick(KList.KListItemBase item, int index)
        {
            if (_runeItemInfo != null)
            {
                if (_runeItemInfo.RuneConfig.__subtype == public_config.RUNE_MONEY)
                {
                    ShowExpRuneTips();
                }
                else if (_runeItemInfo.RuneConfig.__subtype == public_config.RUNE_EXP)
                {
                    ShowExpRuneTips();
                }
                else
                {
                    ShowRuneTips();
                }
            }
        }

        private void ShowExpRuneTips()
        {
            bagData.pos = _runeItemInfo.GridPosition;
            bagData.toPos = -1;
            bagData.bagType = BagType.RuneBag;
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.ExpRuneToolTips, bagData, PanelIdEnum.Rune);
        }

        private void ShowRuneTips()
        {
            bagData.pos = _runeItemInfo.GridPosition;
            bagData.toPos = -1;
            bagData.bagType = BagType.RuneBag;
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.RuneToolTips, bagData, PanelIdEnum.Rune);
        }

        #endregion

    }
}
