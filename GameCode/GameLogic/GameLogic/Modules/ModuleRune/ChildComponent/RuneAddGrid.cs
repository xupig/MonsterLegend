﻿#region 模块信息
/*==========================================
// 文件名：RuneLevelUpBagGrid
// 命名空间: GameLogic.GameLogic.Modules.ModuleGlyphVow.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/14 14:31:06
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager.SubSystem;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleRune.ChildComponent
{
    public class RuneAddGrid :KList.KListItemBase
    {
        #region 私有变量
        private IconContainer _iconContainer;
        private KToggle m_Toggle;
        private StateText m_txtName;
        private StateText m_txtLevel;
        private KButton m_btnBg;
        private StateImage _imgShadow;
        private static string levelTemplate;
        #endregion

        //public KComponentEvent<bool, RuneItemInfo> onClick = new KComponentEvent<bool, RuneItemInfo>();

        protected RuneItemInfo _data;

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as RuneItemInfo;
                Refresh();
            }
        }

        public override void Dispose()
        {
            
        }

        #region 基础接口实现

        protected override void Awake()
        {
            m_btnBg = GetChildComponent<KButton>("Button_bg");
            _iconContainer = m_btnBg.AddChildComponent<IconContainer>("icon");
            m_txtName = m_btnBg.GetChildComponent<StateText>("txtName");
            m_txtLevel = m_btnBg.GetChildComponent<StateText>("txtLevel");
            _imgShadow = m_btnBg.GetChildComponent<StateImage>("zhuangbeitouying");
            m_Toggle = m_btnBg.GetChildComponent<KToggle>("Xuanze");
            m_Toggle.enabled = false;
            if (levelTemplate == null)
            {
                levelTemplate = m_txtLevel.CurrentText.text;
            }
            
        }

        public override void Show()
        {
            AddEventListener();
            base.Show();
        }

        public override void Hide()
        {
            m_btnBg.onClick.RemoveListener(OnClickBg);
            base.Hide();
        }

        protected void AddEventListener()
        {
            m_btnBg.onClick.AddListener(OnClickBg);
        }


        protected void Refresh()
        {
            _iconContainer.Visible = _data != null;
            m_txtName.Visible = _data != null;
            m_txtLevel.Visible = _data != null;
            m_Toggle.Visible = _data != null;
            _imgShadow.Visible = _data != null;
            if (_data != null)
            {
                m_Toggle.isOn = _data.IsSelect;
            }
            SetContent(_data);
        }

        private void SetContent(RuneItemInfo info)
        {
            if (info != null)
            {
                int level = info.Level;
                m_txtLevel.CurrentText.text = string.Format(levelTemplate,level);
                m_txtName.CurrentText.text = info.Name;
                m_txtName.CurrentText.color = item_helper.GetColor(info.Id);
                _iconContainer.SetIcon(info.Icon);
            }
        }

        #endregion

        #region UI事件回调处理

        public void OnValueChange(KToggle target, bool select)
        {
            if (_data == null)
            {
                return;
            }
            if (select)
            {
                EventDispatcher.TriggerEvent<bool, RuneItemInfo>(RuneEvent.ADD_RUNE_ITEM, true, _data);
            }
            else
            {
                EventDispatcher.TriggerEvent<bool, RuneItemInfo>(RuneEvent.ADD_RUNE_ITEM, false, _data);
            }
        }

        private void OnClickBg()
        {
            if (_data == null)
            {
                return;
            }
            if (m_Toggle.isOn == false && RuneAddPanel.currentNum == RuneManager.levelUpData.maxNeedNum)
            {
                if (RuneAddPanel.currentNum == RuneManager.levelUpData.maxNeedNum)
                {
                    MogoUtils.FloatTips(4353);
                }
                return;
            }
            m_Toggle.isOn = !m_Toggle.isOn;
            if (m_Toggle.isOn)
            {
                EventDispatcher.TriggerEvent<bool, RuneItemInfo>(RuneEvent.ADD_RUNE_ITEM, true, _data);
            }
            else
            {
                EventDispatcher.TriggerEvent<bool, RuneItemInfo>(RuneEvent.ADD_RUNE_ITEM, false, _data);
            }
        }

        #endregion
    }
}
