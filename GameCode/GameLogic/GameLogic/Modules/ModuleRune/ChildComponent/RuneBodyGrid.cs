﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/23 15:21:25
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using rune = GameData.rune;
namespace ModuleRune.ChildComponent
{
    public class RuneBodyGrid : KContainer, IPointerClickHandler
    {
        private StateText m_txtLevel;
       // private StateText m_txtName;
        private StateText m_txtLock;
        private IconContainer m_iconContainer;
        private StateImage m_imgLock;
        private StateImage _lockBg;
        //private static string expLevelTemplate;
        private static string levelTemplate = "Lv{0}";
        private StateImage _imgAdd;
        public KComponentEvent<rune_hole> onClick = new KComponentEvent<rune_hole>();

        private static RuneTipsData bagData = new RuneTipsData();

        private RuneItemInfo _runeItemInfo;

        private rune_hole _hole;

        private KParticle _particle;

        #region 基础接口实现

        protected override void Awake()
        {
           
            m_txtLock = GetChildComponent<StateText>("Label_lockText");
            //m_txtName = GetChildComponent<StateText>("Label_txtName");
            m_txtLevel = GetChildComponent<StateText>("Label_txtLevel");
            m_iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _lockBg = GetChildComponent<StateImage>("Image_jinengdi");
            m_imgLock = GetChildComponent<StateImage>("Image_sharedGridLocks");
            _imgAdd = GetChildComponent<StateImage>("Image_sharedTianjia");
            _particle = AddChildComponent<KParticle>("fx_ui_10_5_zhuangbei");
            _particle.Stop();
            base.Awake();
        }

        public void ShowEffect()
        {
            if(_particle.Visible == false)
            {
                _particle.Play();
            }
        }

        protected void Refresh()
        {
            this.gameObject.SetActive(_hole != null);
            if (_runeItemInfo != null)
            {
                SetHasDataState();
            }
            else if (_hole != null)
            {
                if (PlayerAvatar.Player.level < _hole.__unlock_level)
                {
                    SetLockState();
                }
                else
                {
                    SetNoDataState();
                }
            } 
            else
            {
                SetEmptyState();
            }
        }

        private void SetHasDataState()
        {
            m_iconContainer.gameObject.SetActive(true);
            m_txtLevel.gameObject.SetActive(true);
           // m_txtName.gameObject.SetActive(true);
            m_imgLock.gameObject.SetActive(false);
            m_txtLock.gameObject.SetActive(false);
            _imgAdd.gameObject.SetActive(false);
            _lockBg.Visible = true;
           // m_txtName.CurrentText.text = item_helper.GetName(_runeItemInfo.Id);
            m_txtLevel.CurrentText.text = string.Format(levelTemplate, _runeItemInfo.Level);
           // m_txtName.CurrentText.color = item_helper.GetColor(_runeItemInfo.Id);
            m_iconContainer.SetIcon(_runeItemInfo.Icon);
        }

        private void SetNoDataState()
        {
            m_imgLock.gameObject.SetActive(false);
            m_txtLock.gameObject.SetActive(false);
            _lockBg.Visible = true;
            m_iconContainer.gameObject.SetActive(false);
            m_txtLevel.gameObject.SetActive(false);
           // m_txtName.gameObject.SetActive(false);
            _imgAdd.gameObject.SetActive(true);
        }

        private void SetLockState()
        {
            m_imgLock.gameObject.SetActive(true);
            m_txtLock.gameObject.SetActive(true);
            _lockBg.Visible = true;
            m_iconContainer.gameObject.SetActive(false);
            m_txtLevel.gameObject.SetActive(false);
           // m_txtName.gameObject.SetActive(false);
            _imgAdd.gameObject.SetActive(false);

            m_txtLock.CurrentText.text = (43400).ToLanguage(_hole.__unlock_level);
        }

        public void setInfo(RuneItemInfo info, rune_hole rune_hole)
        {
            _hole = rune_hole;
            _runeItemInfo = info;
            Refresh();
        }

        private void SetEmptyState()
        {
            m_iconContainer.gameObject.SetActive(false);
            m_txtLevel.gameObject.SetActive(false);
            m_imgLock.gameObject.SetActive(false);
            m_txtLock.gameObject.SetActive(false);
            //m_txtName.gameObject.SetActive(false);
            _imgAdd.gameObject.SetActive(false);
        }


       

        #endregion

        public override void OnPointerClick(PointerEventData eventData)
        {
            if (_runeItemInfo != null)
            {
                if (_runeItemInfo.RuneConfig.__subtype == public_config.RUNE_EXP)
                {
                    ShowExpRuneTips();
                }
                else
                {
                    ShowRuneTips();
                }
            }
            else if (_hole != null && PlayerAvatar.Player.level >= _hole.__unlock_level)
            {
                onClick.Invoke(_hole);
            } 
        }

        private void ShowExpRuneTips()
        {
            bagData.pos = _hole.__id;
            bagData.toPos = -1;
            bagData.bagType = BagType.BodyRuneBag;
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.ExpRuneToolTips, bagData, PanelIdEnum.Rune);
        }

        private void ShowRuneTips()
        {
            bagData.pos = _hole.__id;
            bagData.toPos = -1;
            bagData.bagType = BagType.BodyRuneBag;
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.RuneToolTips, bagData, PanelIdEnum.Rune);
        }



    }
}
