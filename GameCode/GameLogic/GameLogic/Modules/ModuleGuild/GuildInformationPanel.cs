﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using ModuleActivity;
using ModuleCommonUI;
using MogoEngine.Events;
using UnityEngine;

namespace ModuleGuild
{
    public class GuildInformationPanel : BasePanel
    {
        private KToggleGroup _tabToggleGroup;
        private int _currentIndex;

        private MyGuildView _myGuildView;
        private GuildActivityView _activityView;
        private GuildShopView _shopView;

        private const int TAB_MY_GUILD = 0;
        private const int TAB_GUILD_ACTIVITY = 1;
        private const int TAB_GUILD_WAR = 2;
        private const int TAB_GUILD_WELFARE = 3;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _tabToggleGroup = GetChildComponent<KToggleGroup>("Container_panelBg/ToggleGroup_biaoti");
            _tabToggleGroup.SelectIndex = 0;
            _myGuildView = AddChildComponent<MyGuildView>("Container_wodegonghui");
            _activityView = AddChildComponent<GuildActivityView>("Container_xianshihuodong");
            _shopView = AddChildComponent<GuildShopView>("Container_gonghuishangdian");
            SetTabList(18, _tabToggleGroup);
        }

        public override void OnShow(object data)
        {
            if (PlayerAvatar.Player.guild_id == 0)
            {
                ClosePanel();
                PanelIdEnum.GuildCreatePanel.Show();
                return;
            }
            AddEventListener();
            RefreshGreenPoint();
            SetData(data);
        }

        public override void SetData(object data)
        {
            if (data != null)
            {
                if (data is int[])
                {
                    int[] list = data as int[];
                    _tabToggleGroup.SelectIndex = list[0] - 1;
                    _currentIndex = list[0] - 1;
                    if (_currentIndex == 0)
                    {
                        ShowMyGuild(list[1]);
                    }
                    else if (_currentIndex == 3)
                    {
                        ShowWelfareView(list[1]);
                    }
                    else
                    {
                        OnTabChange(_tabToggleGroup, _tabToggleGroup.SelectIndex);
                    }
                }
                else if (data is int)
                {
                    int index = (int)data;
                    _tabToggleGroup.SelectIndex = index;
                    _currentIndex = index;
                    OnTabChange(_tabToggleGroup, index);
                }
            }
            else
            {
                _tabToggleGroup.SelectIndex = 0;
                _currentIndex = 0;
                ShowMyGuild(2);
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
            PanelIdEnum.GuildWar.Close();
            //PanelIdEnum.GuildWelfare.Close();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildInformation; }
        }

        public void AddEventListener()
        {
            _tabToggleGroup.onSelectedIndexChanged.AddListener(OnTabChange);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_Point, RefreshGreenPoint);
        }

        public void RemoveEventListener()
        {
            _tabToggleGroup.onSelectedIndexChanged.RemoveListener(OnTabChange);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_Point, RefreshGreenPoint);
        }

        private void RefreshGreenPoint()
        {
            _tabToggleGroup[0].SetGreenPoint(false);
            _tabToggleGroup[1].SetGreenPoint(false);
            _tabToggleGroup[2].SetGreenPoint(false);
            _tabToggleGroup[3].SetGreenPoint(false);
            if (PlayerDataManager.Instance.GuildData.HasNewNotice || guild_helper.HasApplyJoinInfo() || guild_helper.CanUpgrade() || guild_helper.CanReward() || guild_helper.CanLearnSkill())
            {
                _tabToggleGroup[0].SetGreenPoint(true);
            }
        }

        private void OnTabChange(KToggleGroup toggleGroup, int index)
        {
            switch (index)
            {
                case TAB_MY_GUILD:
                    ShowMyGuild(2);
                    break;
                case TAB_GUILD_ACTIVITY:
                    ShowActivityView();
                    break;
                case TAB_GUILD_WAR:
                    ShowGuildWarView();
                    break;
                case TAB_GUILD_WELFARE:
                    ShowWelfareView(0);
                    break;
            }
            _currentIndex = toggleGroup.SelectIndex;
        }

        private void ShowGuildWarView()
        {
            _myGuildView.Visible = false;
            _activityView.Visible = false;
            PanelIdEnum.GuildWar.Show();
            _shopView.Hide();
           // PanelIdEnum.GuildWelfare.Close();
        }

        private void ShowMyGuild(int viewId)
        {
            _myGuildView.Visible = true;
            _myGuildView.SetViewId(viewId);
            _activityView.Visible = false;
            PanelIdEnum.GuildWar.Close();
            _shopView.Hide();
            //PanelIdEnum.GuildWelfare.Close();
        }

        private void ShowActivityView()
        {
            _myGuildView.Visible = false;
            _activityView.Visible = true;
            PanelIdEnum.GuildWar.Close();
            _shopView.Hide();
            //PanelIdEnum.GuildWelfare.Close();
        }

        private void ShowWelfareView(int index)
        {
            _myGuildView.Visible = false;
            _activityView.Visible = false;
            PanelIdEnum.GuildWar.Close();
            _shopView.Show(index);
            //PanelIdEnum.GuildWelfare.Show(index);
        }

    }
}
