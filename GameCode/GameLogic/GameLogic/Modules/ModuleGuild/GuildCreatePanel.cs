﻿using Common.Base;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;

namespace ModuleGuild
{
    public class GuildCreatePanel : BasePanel
    {
        private GuildRankingListView _rankingListView;

        protected override void Awake()
        {
            CloseBtn = this.GetChildComponent<KButton>("Container_gonghuipaihangbang/Container_paihangbang/Button_close");
            _rankingListView = AddChildComponent<GuildRankingListView>("Container_gonghuipaihangbang");
        }

        public override void OnShow(object data)
        {
            AddEventListener();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildCreatePanel; }
        }

        public void AddEventListener()
        {
            EventDispatcher.AddEventListener(GuildEvents.Show_My_Guild_Info, OnGetGuildInfomation);
        }

        public void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(GuildEvents.Show_My_Guild_Info, OnGetGuildInfomation);
        }

        private void OnGetGuildInfomation()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.Task);
            UIManager.Instance.ClosePanel(PanelIdEnum.GuildCreatePanel);
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildInformation);
        }

    }
}
