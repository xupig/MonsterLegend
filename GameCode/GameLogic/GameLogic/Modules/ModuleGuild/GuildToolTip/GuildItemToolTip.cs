﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using UnityEngine;

namespace ModuleGuild
{
    public class GuildItemToolTip : KContainer
    {
        private StateText _nameText;
        private StateText _remainText;
        private KButton _backButton;
        private KButton _addButton;
        private KButton _reduceButton;
        private KButton _doubleAddButton;
        private KButton _doubleReduceButton;
        private StateText _countText;
        private KButton _cancelButton;
        private KButton _donateButton;
        private IconContainer _contributeIcon;
        private IconContainer _fundIcon;
        private StateText _contributeText;
        private StateText _fundText;
        private guild_contribution _data;
        private int _count = 0;

        protected override void Awake()
        {
            _nameText = GetChildComponent<StateText>("Label_txtDaojumingcheng");
            _remainText = GetChildComponent<StateText>("Label_txtShengyuzhi");
            _backButton = GetChildComponent<KButton>("Button_fanhui");
            _addButton = GetChildComponent<KButton>("Button_tianjia");
            _reduceButton = GetChildComponent<KButton>("Button_jianshao");
            _cancelButton = GetChildComponent<KButton>("Button_quxiao");
            _donateButton = GetChildComponent<KButton>("Button_juanxian");
            _doubleAddButton = GetChildComponent<KButton>("Button_shuangjia");
            _doubleReduceButton = GetChildComponent<KButton>("Button_shuangjian");
            _countText = GetChildComponent<StateText>("Label_txtShuliang");
            _contributeIcon = AddChildComponent<IconContainer>("Container_goldIcon");
            _fundIcon = AddChildComponent<IconContainer>("Container_goldIcon2");
            _contributeText = GetChildComponent<StateText>("Label_txtGold");
            _fundText = GetChildComponent<StateText>("Label_txtGold2");
            _countText.CurrentText.alignment = TextAnchor.UpperCenter;
            _remainText.CurrentText.alignment = TextAnchor.UpperCenter;
            _nameText.CurrentText.alignment = TextAnchor.UpperCenter;
            _contributeIcon.SetIcon(item_helper.GetIcon(8));
            _fundIcon.SetIcon(item_helper.GetIcon(9));
            AddEventListener();
        }

        protected void AddEventListener()
        {
            _backButton.onClick.AddListener(OnBackButtonClick);
            _addButton.onClick.AddListener(OnAddButtonClick);
            _reduceButton.onClick.AddListener(OnReduceButtonClick);
            _cancelButton.onClick.AddListener(OnBackButtonClick);
            _donateButton.onClick.AddListener(OnDonateButtonClick);
            _doubleAddButton.onClick.AddListener(OnDoubleAddButtonClick);
            _doubleReduceButton.onClick.AddListener(OnDoubleReduceButtonClick);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Donate_Item_View, RefreshView);
        }

        protected void RemoveEventListener()
        {
            _backButton.onClick.RemoveListener(OnBackButtonClick);
            _addButton.onClick.RemoveListener(OnAddButtonClick);
            _reduceButton.onClick.RemoveListener(OnReduceButtonClick);
            _cancelButton.onClick.RemoveListener(OnBackButtonClick);
            _donateButton.onClick.RemoveListener(OnDonateButtonClick);
            _doubleAddButton.onClick.RemoveListener(OnDoubleAddButtonClick);
            _doubleReduceButton.onClick.RemoveListener(OnDoubleReduceButtonClick);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Donate_Item_View, RefreshView);
        }

        private void OnDonateButtonClick()
        {
            if (_count == 0)
            {
                return;
            }
            GuildData guildData = PlayerDataManager.Instance.GuildData;
            if (guild_level_helper.GetFundLimit(guildData.MyGuildData.Level) <= guildData.MyGuildData.Money)
            {
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74655), PanelIdEnum.MainUIField);
                return;
            }
            if (guild_level_helper.GetContributionLimit(guildData.MyGuildData.Level) <= guildData.GetMyTodayContribution())
            {
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(74656), delegate() { GuildManager.Instance.DonateItem(_data.__id, _count / _data.__least_limit); });
                return;
            }
            GuildManager.Instance.DonateItem(_data.__id, _count / _data.__least_limit);
        }

        private void OnAddButtonClick()
        {
            int addLimit = GetAddLimit();
            if (_count + _data.__least_limit + PlayerDataManager.Instance.GuildData.GetMyTodayDonate(_data.__item) * _data.__least_limit > _data.__day_limit + addLimit)
            {
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74861, item_helper.GetName(_data.__item)), PanelIdEnum.MainUIField);
                return;
            }
            int hasCount = GetItemNum();
            if (_count + _data.__least_limit <= hasCount)
            {
                _count += _data.__least_limit;
                _countText.CurrentText.text = _count.ToString();
            }
            RefreshView();
        }

        private void OnReduceButtonClick()
        {
            if (_count - _data.__least_limit >= 0)
            {
                _count -= _data.__least_limit;
                _countText.CurrentText.text = _count.ToString();
            }
            RefreshView();
        }

        private void OnDoubleAddButtonClick()
        {
            int addLimit = GetAddLimit();
            int hasCount = GetItemNum();
            for (int i = 0; i < 10; i++)
            {
                if (_count + _data.__least_limit + PlayerDataManager.Instance.GuildData.GetMyTodayDonate(_data.__item) * _data.__least_limit > _data.__day_limit + addLimit)
                {
                    ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74861, item_helper.GetName(_data.__item)), PanelIdEnum.MainUIField);
                    break;
                }
                if (_count + _data.__least_limit > hasCount)
                {
                    break;
                }
                _count += _data.__least_limit;
                if (_count % (10 * _data.__least_limit) == 0)
                {
                    break;
                }
            }
            _countText.CurrentText.text = _count.ToString();
            RefreshView();
        }

        private int GetAddLimit()
        {
            int addLimit = 0;
            if (_data.__item == public_config.MONEY_TYPE_GOLD)
            {
                addLimit = PlayerDataManager.Instance.GuildData.EffectData.GoldLimit;
            }
            else if (_data.__item == public_config.MONEY_TYPE_DIAMOND)
            {
                addLimit = PlayerDataManager.Instance.GuildData.EffectData.DiamondLimit;
            }
            else if (_data.__item == public_config.MONEY_TYPE_BIND_DIAMOND)
            {
                addLimit = PlayerDataManager.Instance.GuildData.EffectData.BindDiamondLimit;
            }
            return addLimit;
        }

        private void OnDoubleReduceButtonClick()
        {
            for (int i = 0; i < 10; i++)
            {
                if (_count - _data.__least_limit < 0)
                {
                    break;
                }
                _count -= _data.__least_limit;
                if (_count % (10 * _data.__least_limit) == 0)
                {
                    break;
                }
            }
            _countText.CurrentText.text = _count.ToString();
            RefreshView();
        }

        private void OnBackButtonClick()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.GuildPop);
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }

        public void Show(guild_contribution data)
        {
            this.Visible = true;
            _data = data;
            _count = _data.__least_limit;
            RefreshView();
        }

        private void RefreshView()
        {
            _nameText.CurrentText.text = item_helper.GetName(_data.__item);
            _count = Math.Max(_count, _data.__least_limit);
            int hasCount = GetItemNum();
            _count = Math.Min(_count, hasCount);
            int addLimit = GetAddLimit();
            if (_count + PlayerDataManager.Instance.GuildData.GetMyTodayDonate(_data.__item) * _data.__least_limit > _data.__day_limit + addLimit)
            {
                _count = _data.__day_limit + addLimit - PlayerDataManager.Instance.GuildData.GetMyTodayDonate(_data.__item) * _data.__least_limit;
            }
            _count = _count < _data.__least_limit ? 0 : _count;
            _countText.CurrentText.text = _count.ToString();
            _remainText.CurrentText.text = hasCount.ToString();
            _contributeText.CurrentText.text = "+" + ((_data.__contribution_value + PlayerDataManager.Instance.GuildData.EffectData.DonateGetContribute) * _count / _data.__least_limit).ToString();
            _fundText.CurrentText.text = "+" + ((_data.__guild_money_get + PlayerDataManager.Instance.GuildData.EffectData.DonateGetFund) * _count / _data.__least_limit).ToString();
        }

        private int GetItemNum()
        {
            if (_data.__item == 1)
            {
                return PlayerAvatar.Player.money_gold;
            }
            else if (_data.__item == 2)
            {
                return PlayerAvatar.Player.money_diamond;
            }
            else if (_data.__item == 3)
            {
                return PlayerAvatar.Player.money_bind_diamond;
            }
            return PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(_data.__item);
        }
    }
}
