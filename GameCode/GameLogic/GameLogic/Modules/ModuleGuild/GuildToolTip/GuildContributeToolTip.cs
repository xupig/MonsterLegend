﻿using Game.UI.UIComponent;

namespace ModuleGuild
{
    public class GuildContributeToolTip : KContainer
    {

        protected override void Awake()
        {
            AddEventListener();
        }

        protected void AddEventListener()
        {
            
        }

        protected void RemoveEventListener()
        {
           
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }

    }
}
