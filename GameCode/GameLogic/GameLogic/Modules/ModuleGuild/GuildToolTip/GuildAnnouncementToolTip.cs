﻿using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Utils;
using System.Collections.Generic;

namespace ModuleGuild
{
    public class GuildAnnouncementToolTip : KContainer
    {
        private KButton _closeButton;
        private KButton _releaseButton;
        private KInputField _announcementInput;
        private KScrollView _scrollView;
        private KList _list;

        protected override void Awake()
        {
            _releaseButton = GetChildComponent<KButton>("Button_queren");
            _closeButton = GetChildComponent<KButton>("Button_close");
            _announcementInput = GetChildComponent<KInputField>("Input_input");
            _announcementInput.text = string.Empty;
            _scrollView = GetChildComponent<KScrollView>("ScrollView_shijianjilu");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _list.SetPadding(5, 0, 0, 0);
            _list.SetGap(0, 0);
        }

        protected void AddEventListener()
        {
            _releaseButton.onClick.AddListener(OnReleaseButtonClick);
            _closeButton.onClick.AddListener(OnCloseButtonClick);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_Notice_List, RefreshNoticeContent);
        }

        protected void RemoveEventListener()
        {
            _releaseButton.onClick.RemoveListener(OnReleaseButtonClick);
            _closeButton.onClick.RemoveListener(OnCloseButtonClick);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_Notice_List, RefreshNoticeContent);
        }

        private void OnCloseButtonClick()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.GuildPop);
        }

        private void OnReleaseButtonClick()
        {
            if (guild_rights_helper.CheckHasRights(GuildRightId.notice, 74855) == false) return;
            if (_announcementInput.text == string.Empty)
            {
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74649), PanelIdEnum.MainUIField);
                return;
            }
            GuildManager.Instance.SendNotice(_announcementInput.text);
            _announcementInput.text = string.Empty;
        }

        private void RefreshNoticeContent()
        {
            List<PbGuildNoticeInfo> noticeList = PlayerDataManager.Instance.GuildData.GetGuildNoticeList();
            _list.RemoveAll();
            _list.SetDataList<GuildAnnouncementItem>(noticeList);
            _scrollView.ResetContentPosition();
        }

        protected override void OnEnable()
        {
            GuildManager.Instance.GetNoticeList();
            AddEventListener();
            RefreshNoticeContent();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            PlayerDataManager.Instance.GuildData.HasNewNotice = false;
        }

    }

    public class GuildAnnouncementItem : KList.KListItemBase
    {
        private PbGuildNoticeInfo _data;
        private StateText _nameText;
        private StateText _timeText;
        private StateText _contentText;

        protected override void Awake()
        {
            _nameText = GetChildComponent<StateText>("Label_txtFabuzhemingcheng");
            _timeText = GetChildComponent<StateText>("Label_txtShijian");
            _contentText = GetChildComponent<StateText>("Label_txtNeirong");
        }

        private void RefreshContent()
        {
            _nameText.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.name_bytes);
            _contentText.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.content_bytes);
            _timeText.CurrentText.text = MogoTimeUtil.ToLocalTime(_data.time);
        }

        public override void Dispose()
        {
            base.Data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null && value != base.Data)
                {
                    _data = value as PbGuildNoticeInfo;
                    RefreshContent();
                }
            }
        }

    }
}
