﻿using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuild
{
    public class GuildDonateToolTip : KContainer
    {
        private KButton _closeButton;
        private IconContainer _contributeIcon;
        private IconContainer _fundIcon;
        private StateText _contributeText;
        private StateText _fundText;
        private KScrollView _scrollView;
        private KList _list;

        protected override void Awake()
        {
            _closeButton = GetChildComponent<KButton>("Container_bg/Button_close");
            _contributeIcon = AddChildComponent<IconContainer>("Container_buyGold/Container_goldIcon");
            _fundIcon = AddChildComponent<IconContainer>("Container_buyDiamond/Container_goldIcon");
            _contributeText = GetChildComponent<StateText>("Container_buyGold/Label_txtGold");
            _fundText = GetChildComponent<StateText>("Container_buyDiamond/Label_txtGold");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_gongxianhuode");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _list.SetGap(0, 0);
            InitView();
        }

        private void InitView()
        {
            _contributeIcon.SetIcon(item_helper.GetIcon(8));
            _fundIcon.SetIcon(item_helper.GetIcon(9));
            _contributeText.CurrentText.alignment = TextAnchor.UpperCenter;
            _fundText.CurrentText.alignment = TextAnchor.UpperCenter;
        }

        private void RefreshListView()
        {
            List<guild_contribution> contributionList = new List<guild_contribution>();
            foreach (guild_contribution data in XMLManager.guild_contribution.Values)
            {
                if (data.__guild_level == PlayerDataManager.Instance.GuildData.MyGuildData.Level)
                {
                    contributionList.Add(data);
                }
            }
            _list.RemoveAll();
            _list.SetDataList<GuildDonateItem>(contributionList);
        }

        protected void AddEventListener()
        {
            _closeButton.onClick.AddListener(OnCloseButtonClick);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_Money, RefreshMoneyView);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_Contribute, RefreshMoneyView);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_Level, RefreshListView);
        }

        protected void RemoveEventListener()
        {
            _closeButton.onClick.RemoveListener(OnCloseButtonClick);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_Money, RefreshMoneyView);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_Contribute, RefreshMoneyView);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_Level, RefreshListView);
        }

        private void OnCloseButtonClick()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.GuildPop);
        }

        private void RefreshMoneyView()
        {
            MyGuildData data = PlayerDataManager.Instance.GuildData.MyGuildData;
            _fundText.CurrentText.text = data.Money.ToString();
            _contributeText.CurrentText.text = PlayerDataManager.Instance.GuildData.GetMyContribution().ToString();
        }

        protected override void OnEnable()
        {
            AddEventListener();
            RefreshMoneyView();
            RefreshListView();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

    }

    public class GuildDonateItem : KList.KListItemBase
    {
        private guild_contribution _data;
        private KButton _donateButton;
        private StateText _nameText;
        private StateText _contributeText;
        private StateText _fundText;
        private ItemGrid _itemGrid;
        private StateText _countText;

        protected override void Awake()
        {
            _donateButton = GetChildComponent<KButton>("Button_juanxuan");
            _nameText = GetChildComponent<StateText>("Label_txtDaojumingcheng");
            _contributeText = GetChildComponent<StateText>("Label_txtGongxianzhi");
            _fundText = GetChildComponent<StateText>("Label_txtZijinzhi");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _countText = GetChildComponent<StateText>("Label_txtShuliang");
            _countText.CurrentText.alignment = TextAnchor.UpperRight;
            AddEventListener();
        }

        private void RefreshContent()
        {
            _nameText.CurrentText.text = item_helper.GetName(_data.__item);
            _contributeText.CurrentText.text = "+" + (_data.__contribution_value + PlayerDataManager.Instance.GuildData.EffectData.DonateGetContribute);
            _fundText.CurrentText.text = "+" + (_data.__guild_money_get +PlayerDataManager.Instance.GuildData.EffectData.DonateGetFund);
            _countText.CurrentText.text = _data.__least_limit.ToString();
            _itemGrid.SetItemData(_data.__item);
        }

        protected void AddEventListener()
        {
            _donateButton.onClick.AddListener(OnDonateButtonClick);
        }

        protected void RemoveEventListener()
        {
            _donateButton.onClick.RemoveListener(OnDonateButtonClick);
        }

        private void OnDonateButtonClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildPop, _data);
        }

        public override void Dispose()
        {
            RemoveEventListener();
            base.Data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null && value != base.Data)
                {
                    _data = value as guild_contribution;
                    RefreshContent();
                }
            }
        }

    }

}
