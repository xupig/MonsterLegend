﻿using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;

namespace ModuleGuild
{
    public class GuildRecordView : KContainer
    {
        private KScrollView _scrollView;
        private KList _list;
        private StateText _noDataText;

        protected override void Awake()
        {
            _scrollView = GetChildComponent<KScrollView>("ScrollView_jishi");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _noDataText = GetChildComponent<StateText>("Container_xiaoniankaBG/Label_txtmeiyoushuju");
        }

        protected void AddEventListener()
        {
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_Record, RefreshContent);
        }

        protected void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_Record, RefreshContent);
        }

        private void RefreshContent()
        {
            List<string> infoList = PlayerDataManager.Instance.GuildData.GetGuildRecordInfoList();
            _list.SetDataList<GuildRecordItem>(infoList, 2);
            _scrollView.ResetContentPosition();
            _noDataText.gameObject.SetActive(infoList.Count == 0);
        }

        protected override void OnEnable()
        {
            GuildManager.Instance.GetGuildRecordList();
            AddEventListener();
            RefreshContent();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

    }

    public class GuildRecordItem : KList.KListItemBase
    {
        private string _data;
        private StateText _contentText;

        protected override void Awake()
        {
            _contentText = GetChildComponent<StateText>("Label_txtjishi");
        }

        private void RefreshContent()
        {
            _contentText.CurrentText.text = _data;
        }

        public override void Dispose()
        {
            base.Data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as string;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            RefreshContent();
        }
    }
}
