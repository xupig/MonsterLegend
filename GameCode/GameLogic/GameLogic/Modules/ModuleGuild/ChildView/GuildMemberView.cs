﻿using ACTSystem;
using Common.Base;
using Common.Chat;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuild
{
    public class GuildMemberView : KContainer
    {
        private KButton _fireButton;
        private KButton _appointButton;
        private KButton _addFriendButton;
        private KButton _recruitButton;
        private KButton _infoButton;
        private GuildPositionView _positionView;
        private StateText _idText;
        private StateText _vocationText;
        private StateText _timeText;
        private StateText _positionText;
        private StateText _contributeText;
        private StateText _statusTxt;
        private KScrollView _scrollView;
        private KList _list;
        private KButton _addButton;
        private List<string> _contentList = new List<string>();
        private ModelComponent _avatarModel;
        private SiftTitleGroup _titleGroup;

        private List<PbGuildMemberInfo> _memberList = new List<PbGuildMemberInfo>();
        private int _currentMemberIndex = 0;
        private int _currentOrderType;
        private int _isAscending;

        protected override void Awake()
        {
            _fireButton = GetChildComponent<KButton>("Button_kaichuhuiyuan");
            _appointButton = GetChildComponent<KButton>("Button_renmian");
            _addFriendButton = GetChildComponent<KButton>("Button_jiaweihaoyou");
            _recruitButton = GetChildComponent<KButton>("Button_zhaomu1");
            _infoButton = GetChildComponent<KButton>("Button_Gantang");
            _positionView = AddChildComponent<GuildPositionView>("Container_xiaokuang");
            _idText = GetChildComponent<StateText>("Container_chongwuleft/Label_txtWanjia");
            _vocationText = GetChildComponent<StateText>("Container_chongwuleft/Label_txtZhiye");
            _timeText = GetChildComponent<StateText>("Container_chongwuleft/Label_txtRuhuishijian");
            _positionText = GetChildComponent<StateText>("Container_chongwuleft/Label_txtZhiwei");
            _contributeText = GetChildComponent<StateText>("Container_chongwuleft/Label_txtGongxian");
            _statusTxt = GetChildComponent<StateText>("Container_chongwuleft/Label_txtZhuangtai");
            _scrollView = GetChildComponent<KScrollView>("Container_chongwuright/ScrollView_chengyuanxinxi");
            _avatarModel = AddChildComponent<ModelComponent>("Container_chongwuleft/Container_roleModel");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _addButton = GetChildComponent<KButton>("Container_chongwuleft/Button_tianjia");
            _titleGroup = AddChildComponent<SiftTitleGroup>("Container_chongwuright/Container_title");
            InitDetailView();
        }

        private void InitDetailView()
        {
            _idText.CurrentText.text = MogoLanguageUtil.GetContent(74952);
            _vocationText.CurrentText.text = MogoLanguageUtil.GetContent(74953);
            _timeText.CurrentText.text = MogoLanguageUtil.GetContent(74954);
            _positionText.CurrentText.text = MogoLanguageUtil.GetContent(74955);
            _contributeText.CurrentText.text = MogoLanguageUtil.GetContent(74956);
        }

        protected void AddEventListener()
        {
            _fireButton.onClick.AddListener(OnFireButtonClick);
            _appointButton.onClick.AddListener(OnAppointButtonClick);
            _addFriendButton.onClick.AddListener(OnFriendButtonClick);
            _recruitButton.onClick.AddListener(OnRecruitButtonClick);
            _addButton.onClick.AddListener(OnDetailButtonClick);
            _list.onItemClicked.AddListener(OnItemClick);
            _positionView.onPositionClick.AddListener(OnPositionClick);
            _infoButton.onClick.AddListener(OnInfoButtonClick);
            _titleGroup.onSiftTitleChange.AddListener(OnSiftTitleChange);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Member_List, RefreshMemberList);
            EventDispatcher.AddEventListener<PbOfflineData>(RankingListEvents.GET_RANK_PLAYER_FACADE_INFO, RefreshMemberFacade);
            EventDispatcher.AddEventListener(GuildEvents.On_Guild_Recuit_Succeed, OnGuildRecuitSucceed);
        }

        protected void RemoveEventListener()
        {
            _fireButton.onClick.RemoveListener(OnFireButtonClick);
            _appointButton.onClick.RemoveListener(OnAppointButtonClick);
            _addFriendButton.onClick.RemoveListener(OnFriendButtonClick);
            _recruitButton.onClick.RemoveListener(OnRecruitButtonClick);
            _addButton.onClick.RemoveListener(OnDetailButtonClick);
            _list.onItemClicked.RemoveListener(OnItemClick);
            _positionView.onPositionClick.RemoveListener(OnPositionClick);
            _infoButton.onClick.RemoveListener(OnInfoButtonClick);
            _titleGroup.onSiftTitleChange.RemoveListener(OnSiftTitleChange);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Member_List, RefreshMemberList);
            EventDispatcher.RemoveEventListener(GuildEvents.On_Guild_Recuit_Succeed, OnGuildRecuitSucceed);
            EventDispatcher.RemoveEventListener<PbOfflineData>(RankingListEvents.GET_RANK_PLAYER_FACADE_INFO, RefreshMemberFacade);
        }

        private void OnSiftTitleChange(int index, SiftTitleState state)
        {
            GetMemberList(index + 1, state == SiftTitleState.Descending ? 2 : 1);
        }

        private void OnInfoButtonClick()
        {
            string result = string.Empty;
            for (int i = 0; i < 4; i++)
            {
                result = string.Concat(result, MogoLanguageUtil.GetContent(74870 + i), '\n');
            }
            UIManager.Instance.ShowPanel(PanelIdEnum.ItemStory, new ItemStoryData(MogoLanguageUtil.GetContent(77), result));
        }

        private void OnPositionClick(int position)
        {
            if (_memberList.Count == 0) return;
            PbGuildMemberInfo pbInfo = _memberList[_currentMemberIndex];
            if (pbInfo.dbid == PlayerAvatar.Player.dbid)
            {
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74859), PanelIdEnum.MainUIField);
                return;
            }
            if (position == public_config.GUILD_POSITION_LEADER && PlayerDataManager.Instance.GuildData.GetMyPosition() == public_config.GUILD_POSITION_LEADER)
            {
                MessageBox.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(74874, pbInfo.name), delegate() { GuildManager.Instance.TransferLeader(pbInfo.dbid); });
            }
            else
            {
                GuildManager.Instance.AppointPosition(pbInfo.dbid, position);
            }
        }

        private void OnItemClick(KList list, KList.KListItemBase item)
        {
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                GuildMemberItem _currentItem = _list[i] as GuildMemberItem;
                _currentItem.SetSelect(false);
            }
            _currentMemberIndex = item.Index;
            RefreshItemSelect(_currentMemberIndex, true);
            RefreshMemberDetailView();
        }

        private void RefreshMemberFacade(PbOfflineData data)
        {
            PbGuildMemberInfo pbInfo = _memberList[_currentMemberIndex];
            data.facade = MogoProtoUtils.ParseByteArrToString(data.facade_bytes);
            if (_actor != null && pbInfo.vocation == data.vocation && data.facade != null)
            {
                var avatarModelData = ModelEquipTools.CreateAvatarModelData((Vocation)data.vocation, data.facade);
                var clothInfo = avatarModelData.GetEquipInfo(ACTEquipmentType.Cloth);
                _actor.equipController.equipCloth.PutOn(clothInfo.equipID, clothInfo.particle, clothInfo.flow);
                _actor.equipController.equipWeapon.PutOn(avatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).equipID);
                _actor.equipController.equipWing.onLoadEquipFinished = OnWingGoLoaded;
                _actor.equipController.equipWing.PutOn(avatarModelData.GetEquipInfo(ACTEquipmentType.Wing).equipID);
            }
        }

        private void OnWingGoLoaded()
        {
            ModelComponentUtil.ChangeUIModelParam(_actor.gameObject);
        }

        private void RefreshMemberDetailView()
        {
            if (_memberList.Count == 0)
            {
                InitDetailView();
                return;
            }
            PbGuildMemberInfo pbInfo = _memberList[_currentMemberIndex];
            _idText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(74952), ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_TEXT_GOLDEN, pbInfo.dbid.ToString()));
            _vocationText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(74953), ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_TEXT_GOLDEN, MogoLanguageUtil.GetContent((int)pbInfo.vocation)));
            _timeText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(74954), ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_TEXT_GOLDEN, MogoTimeUtil.ToLocalTime(pbInfo.join_time, "yyyy-MM-dd")));
            _positionText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(74955), ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_TEXT_GOLDEN, MogoLanguageUtil.GetContent(74626 + (int)pbInfo.guild_position - 1)));
            _contributeText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(74956), ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_TEXT_GOLDEN, pbInfo.history_contribution.ToString()));
            RefreshStatusView(pbInfo);

            _avatarModel.LoadAvatarModel((Vocation)pbInfo.vocation, "", OnActorLoaded);
            RankingListManager.Instance.GetRankPlayerFacadeInfo(pbInfo.dbid);
        }

        private void RefreshStatusView(PbGuildMemberInfo pbInfo)
        {
            if (pbInfo.online == 1)
            {
                _statusTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(124500), MogoLanguageUtil.GetContent(124501));
            }
            else
            {
                uint dayHasSecond = 24 * 60 * 60;
                uint todayLeftSecond = (uint)(MogoTimeUtil.ServerTimeStamp2LocalTimeStamp((long)Global.serverTimeStampSecond) % dayHasSecond);
                uint lastOnlineSpan = Global.serverTimeStampSecond - pbInfo.last_online_time;
                if (lastOnlineSpan < todayLeftSecond)
                {
                    _statusTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(124500), MogoLanguageUtil.GetContent(124502));
                }
                else
                {
                    int daySpan = (int)((lastOnlineSpan - todayLeftSecond) / dayHasSecond);
                    daySpan = Mathf.Min(daySpan, 4);
                    _statusTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(124500), MogoLanguageUtil.GetContent(124503 + (int)daySpan));
                }
            }
        }

        private ACTActor _actor;
        private void OnActorLoaded(ACTActor actor)
        {
            _actor = actor;
        }

        private void RefreshItemSelect(int index, bool select)
        {
            if (_list.ItemList.Count > index)
            {
                GuildMemberItem _currentItem = _list[index] as GuildMemberItem;
                _currentItem.SetSelect(select);
            }
        }

        private void RefreshMemberList()
        {
            _memberList = PlayerDataManager.Instance.GuildData.GetMemberList();
            _list.SetDataList<GuildMemberItem>(_memberList, 4);
            _currentMemberIndex = 0;
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                GuildMemberItem _currentItem = _list[i] as GuildMemberItem;
                _currentItem.SetSelect(false);
            }
            RefreshItemSelect(_currentMemberIndex, true);
            RefreshMemberDetailView();
        }

        private void OnFriendButtonClick()
        {
            if (_memberList.Count == 0) return;
            PbFriendAvatarInfo avatarInfo = new PbFriendAvatarInfo();
            PbGuildMemberInfo pbInfo = _memberList[_currentMemberIndex];
            if (pbInfo.dbid == PlayerAvatar.Player.dbid)
            {
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74859), PanelIdEnum.MainUIField);
                return;
            }
            avatarInfo.dbid = pbInfo.dbid;
            FriendManager.Instance.AddFriend(avatarInfo);
        }

        private void OnDetailButtonClick()
        {
            if (_memberList.Count == 0) return;
            PbFriendAvatarInfo avatarInfo = new PbFriendAvatarInfo();
            PbGuildMemberInfo pbInfo = _memberList[_currentMemberIndex];
            PanelIdEnum.PlayerInfoDetail.Show(pbInfo.dbid);
        }

        private void OnRecruitButtonClick()
        {
            if (guild_rights_helper.CheckHasRights(GuildRightId.recruit, 74623) == false) return;
            MyGuildData data = PlayerDataManager.Instance.GuildData.MyGuildData;
            if (data.MemberCount >= guild_level_helper.GetMemberCount(data.Level))
            {
                if (data.Level < guild_level_helper.GetMaxLevel())
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74631));
                }
                else
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74876));
                }
                return;
            }
            string tip = MogoLanguageUtil.GetContent(74644, data.GuildName, data.Level);
            ChatManager.Instance.RequestInviteLinkSendChat(public_config.CHANNEL_ID_WORLD, new ChatGuildLinkWrapper(data.Id, tip));
        }

        private void OnGuildRecuitSucceed()
        {
            ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74646), PanelIdEnum.MainUIField);
        }

        private void OnAppointButtonClick()
        {
            if (guild_rights_helper.CheckHasRights(GuildRightId.appoint, 74622) == false) return;
            _positionView.Visible = true;
        }

        private void OnFireButtonClick()
        {
            if (guild_rights_helper.CheckHasRights(GuildRightId.fire, 74621) == false) return;
            if (_memberList.Count == 0) return;
            PbGuildMemberInfo pbInfo = _memberList[_currentMemberIndex];
            if (pbInfo.dbid == PlayerAvatar.Player.dbid)
            {
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74859), PanelIdEnum.MainUIField);
                return;
            }
            string position = MogoLanguageUtil.GetContent(74626 + (int)pbInfo.guild_position - 1);
            string name = pbInfo.name;
            string tip = string.Format(MogoLanguageUtil.GetContent(74636), position, name);
            MessageBox.Show(true, MessageBox.DEFAULT_TITLE, tip, delegate() { GuildManager.Instance.FireMember(pbInfo.dbid); });
        }

        private void GetMemberList(int orderType, int isAscending)
        {
            _currentOrderType = orderType;
            _isAscending = isAscending;
            GuildManager.Instance.GetMemberList(_currentOrderType, _isAscending);
        }

        protected override void OnEnable()
        {
            AddEventListener();
            _currentMemberIndex = 0;
            _titleGroup.SetState(3, SiftTitleState.Descending);
            GetMemberList(public_config.GUILD_MEMBER_LIST_ORDER_TYPE_POSITION, public_config.GUILD_MEMBER_LIST_ORDER_DESC);
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

    }
}
