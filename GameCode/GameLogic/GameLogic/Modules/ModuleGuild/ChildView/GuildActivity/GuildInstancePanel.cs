﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using ModuleEveningActivity;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuild
{
    public class GuildInstancePanel : BasePanel
    {
        private PVEPreviewLeftView _leftView;
        private PVEPreviewWordView _wordView;
        private CopyPassConditionList _passView;
        private KContainer _rightConrtainer;
        private KButton _callButton;
        private KButton _entryButton;
        private StateText _rewardCount;

        private int _instanceId;
        private int _activityId;

        protected override void Awake()
        {
            base.Awake();
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _leftView = AddChildComponent<PVEPreviewLeftView>("Container_left");
            _rightConrtainer = GetChildComponent<KContainer>("Container_right");
            _wordView = AddChildComponent<PVEPreviewWordView>("Container_right/Container_word");
            _passView = AddChildComponent<CopyPassConditionList>("Container_right/Container_word/Container_zhuangbei/Container_xuyaoxiaohao/Container_tiaojian");
            _callButton = GetChildComponent<KButton>("Container_right/Container_bottomBtn/Container_saodang/Button_saodangNci");
            _entryButton = GetChildComponent<KButton>("Container_right/Container_bottomBtn/Container_saodang/Button_tiaozhanSaodang");
            _rewardCount = GetChildComponent<StateText>("Container_right/Container_bottomBtn/Label_txtXuyaotili");
            _rewardCount.Visible = false;
        }

        protected override PanelIdEnum Id
        {
            get 
            {
                return PanelIdEnum.GuildInstance;
            }
        }

        public override void OnShow(object data)
        {
            SetData(data);
            AddEventListener();
            TweenViewMove.Begin(_leftView.gameObject, MoveType.Show, MoveDirection.Left);
            TweenViewMove.Begin(_rightConrtainer.gameObject, MoveType.Show, MoveDirection.Right);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            if (data is int[])
            {
                int[] list = data as int[];
                if (list.Length == 2)
                {
                    _leftView.Refresh(PanelIdEnum.GuildInstance, data);
                    _instanceId = list[0];
                    _activityId = list[1];
                    _wordView.RefreshWord(_activityId);
                    _passView.Refresh(_instanceId);
                }
            }
            
        }

        private void AddEventListener()
        {
            _entryButton.onClick.AddListener(OnEntryButtonClick);
            _callButton.onClick.AddListener(OnCallButtonClick);
            EventDispatcher.AddEventListener(TeamInstanceEvents.On_Call_People_Succeed, OnCallPeopelSucceed);
        }

        private void RemoveEventListener()
        {
            _entryButton.onClick.RemoveListener(OnEntryButtonClick);
            _callButton.onClick.RemoveListener(OnCallButtonClick);
            EventDispatcher.RemoveEventListener(TeamInstanceEvents.On_Call_People_Succeed, OnCallPeopelSucceed);
        }

        private void OnEntryButtonClick()
        {
            if (activity_helper.IsOpen(_activityId) == false)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.ACTIVITY_NOT_OPEN));
                return;
            }
            if (CheeckInSameGuild())
            {
                CopyLogicManager.Instance.RequestEnterCopy(ChapterType.TeamCopy, () =>
                {
                    MissionManager.Instance.RequsetEnterMission(_instanceId);
                }, _instanceId);
            }
        }

        private bool CheeckInSameGuild()
        {
            foreach (PbTeamMember member in PlayerDataManager.Instance.TeamData.TeammateDic.Values)
            {
                if (member.guild_id != PlayerAvatar.Player.guild_id)
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.KickTeammate);
                    return false;
                }
            }
            return true;
        }

        private void OnCallButtonClick()
        {
            if (activity_helper.IsOpen(_activityId) == false)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.ACTIVITY_NOT_OPEN));
                return;
            }
            //公会任务
            CopyLogicManager.Instance.RequestCallPeople(ChapterType.TeamCopy, () =>
            {
                TeamInstanceManager.Instance.CallPeople(team_helper.GetGameIdByInstanceId(_instanceId), public_config.CHANNEL_ID_GUILD);
            }, _instanceId);
        }

        private void OnCallPeopelSucceed()
        {
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83026), PanelIdEnum.MainUIField);
        }

    }
}
