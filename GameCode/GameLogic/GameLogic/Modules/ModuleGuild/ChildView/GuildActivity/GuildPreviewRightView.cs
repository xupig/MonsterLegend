﻿using Common.Data;
using Game.UI.UIComponent;
using GameData;
using ModuleEveningActivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuild
{
    public class GuildPreviewRightView : KContainer
    {
        private PVEPreviewWordView _wordView;

        protected override void Awake()
        {
            base.Awake();
            _wordView = AddChildComponent<PVEPreviewWordView>("Container_word");
            AddEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

        public void RefreshRightView(int activityId)
        {
            _wordView.RefreshWord(activityId);

        }

        private void AddEventListener()
        {

        }

        private void RemoveEventListener()
        {

        }

    }
}
