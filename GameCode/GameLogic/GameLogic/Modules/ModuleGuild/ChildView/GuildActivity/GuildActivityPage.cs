﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using ModuleActivity;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuild
{
    public class GuildActivityPage : LimitActivityPage
    {
        protected override void RefreshContent<T>(List<LimitActivityData> dataList)
        {
            base.RefreshContent<T>(dataList);
        }

        public void RefreshGuildActivityContent(List<LimitActivityData> dataList)
        {
            RefreshContent<GuildActivityItem>(dataList);
        }
    }
}
