﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using ModuleActivity;
using System.Collections.Generic;

namespace ModuleGuild
{
    public class GuildActivityItem : LimitActivityItem
    {
        protected override void OnGotoActivity()
        {
            if (_activityData != null)
            {
                if (_activityData.openData != null && _activityData.openData.__fid != 0)
                {
                    function_helper.ActivityFollow(_activityData.openData.__fid, 
                        _activityData.openData.__id, data_parse_helper.ParseListString(_activityData.openData.__fid_arg));
                }
            }
        }

        protected override void Refresh()
        {
            base.Refresh();
            if (_activityData != null)
            {
                string content = GetTimeContent();
                List<int> days = new List<int>();
                for (int i = 0; i < _activityData.openWeekOfDay.Count; i++)
                {
                    if (_activityData.openWeekOfDay[i] == 0)
                    {
                        days.Add(7);
                    }
                    else
                    {
                        days.Add(_activityData.openWeekOfDay[i]);
                    }
                }
                days.Sort();
                string zhou = string.Empty;
                for (int i = 0; i < days.Count; i++)
                {
                    if (i == 0)
                    {
                        zhou = string.Concat(zhou, MogoLanguageUtil.GetContent(102513), MogoLanguageUtil.GetContent(102514 + days[i]));
                    }
                    else
                    {
                        zhou = string.Concat(zhou, MogoLanguageUtil.GetContent(102514), MogoLanguageUtil.GetContent(102514 + days[i]));
                    }
                }
                _txtTime.CurrentText.text = string.Format("{0}：{1}", zhou, content);
            }
        }

    }
}
