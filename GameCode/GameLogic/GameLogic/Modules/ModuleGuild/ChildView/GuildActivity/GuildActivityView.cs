﻿using Common.Base;
using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using ModuleActivity;
using MogoEngine.Events;
using System.Collections.Generic;

namespace ModuleGuild
{
    public class GuildActivityView : KContainer
    {
        private GuildActivityPage _activityPage;

        protected override void Awake()
        {
            base.Awake();
            _activityPage = gameObject.AddComponent<GuildActivityPage>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            UpdateContent();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            //EventDispatcher.AddEventListener(ActivityEvents.ACTIVITY_TIME_UPDATE_LIST, UpdateContent);
        }

        private void RemoveEventListener()
        {
            //EventDispatcher.RemoveEventListener(ActivityEvents.ACTIVITY_TIME_UPDATE_LIST, UpdateContent);
        }


        private void UpdateContent()
        {
            List<LimitActivityData> dataList = new List<LimitActivityData>();
            dataList.AddRange(activity_helper.GetActivityByType(ActivityType.GuildActivity));
            _activityPage.RefreshGuildActivityContent(dataList);
        }

    }
}
