﻿using ACTSystem;
using Common.Base;
using Common.Chat;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Utils;
using System.Collections.Generic;

namespace ModuleGuild
{
    public enum SiftTitleState
    {
        Normal = 0,
        Ascending = 1,
        Descending  = 2,
    }

    public class SiftTitleGroup : KContainer
    {
        private List<SiftTitleItem> itemList;
        public KComponentEvent<int, SiftTitleState> onSiftTitleChange = new KComponentEvent<int, SiftTitleState>();

        protected override void Awake()
        {
            itemList = new List<SiftTitleItem>();
            for (int i = 0; i < this.transform.childCount; i++)
            {
                SiftTitleItem item = transform.GetChild(i).gameObject.AddComponent<SiftTitleItem>();
                if (item != null)
                {
                    item.SetStatus(SiftTitleState.Normal);
                    item.onClick.AddListener(OnItenClick);
                    itemList.Add(item);
                }
            }
        }

        private void OnItenClick(SiftTitleItem target, SiftTitleState state)
        {
            int selectIndex = 0;
            for (int i = 0; i < itemList.Count; i++)
            {
                SiftTitleItem item = itemList[i];
                if (item != target)
                {
                    item.SetStatus(SiftTitleState.Normal);
                }
                else
                {
                    item.SetStatus(state);
                    selectIndex = i;
                }
            }
            onSiftTitleChange.Invoke(selectIndex, state);
        }


        public void SetState(int index, SiftTitleState siftTitleState)
        {
            itemList[index].SetStatus(siftTitleState);
        }
    }

    public class SiftTitleItem : KContainer
    {
        private StateImage _upImg;
        private StateImage _downImg;
        private StateImage _backImg;
        private KButton _selectBtn;
        private KContainer _normalContainer;

        private SiftTitleState _currentState = SiftTitleState.Normal;

        public KComponentEvent<SiftTitleItem, SiftTitleState> onClick = new KComponentEvent<SiftTitleItem, SiftTitleState>();

        protected override void Awake()
        {
            _upImg = GetChildComponent<StateImage>("Image_up");
            _downImg = GetChildComponent<StateImage>("Image_down");
            _backImg = GetChildComponent<StateImage>("Image_back");
            _selectBtn = GetChildComponent<KButton>("Button_shaixuan");
            _normalContainer = GetChildComponent<KContainer>("Container_shangxia");
        }

        public void SetStatus(SiftTitleState state)
        {
            _currentState = state;
            switch (state)
            {
                case SiftTitleState.Normal:
                    _upImg.Visible = false;
                    _downImg.Visible = false;
                    _backImg.Visible = false;
                    _normalContainer.Visible = true;
                    break;
                case SiftTitleState.Ascending:
                    _upImg.Visible = true;
                    _downImg.Visible = false;
                    _backImg.Visible = true;
                    _normalContainer.Visible = false;
                    break;
                case SiftTitleState.Descending:
                    _upImg.Visible = false;
                    _downImg.Visible = true;
                    _backImg.Visible = true;
                    _normalContainer.Visible = false;
                    break;
            }
        }

        public SiftTitleState GetCurrentState()
        {
            return _currentState;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            _selectBtn.onClick.AddListener(OnSelectBtnClick);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            _selectBtn.onClick.RemoveListener(OnSelectBtnClick);
        }

        private void OnSelectBtnClick()
        {
            UpdateCurrentState();
            onClick.Invoke(this, _currentState);
        }

        private void UpdateCurrentState()
        {
            switch (_currentState)
            {
                case SiftTitleState.Normal:
                    _currentState = SiftTitleState.Ascending;
                    break;
                case SiftTitleState.Ascending:
                    _currentState = SiftTitleState.Descending;
                    break;
                case SiftTitleState.Descending:
                    _currentState = SiftTitleState.Ascending;
                    break;
            }
        }
      

    }
}
