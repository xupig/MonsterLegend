﻿using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;

namespace ModuleGuild
{
    public class GuildUpgradeView : KContainer
    {
        private KButton _upgradeButton;
        private StateText _levelTextLeft;
        private StateText _memberTextLeft;
        private StateText _fundTextLeft;
        private StateText _viceLeaderTextLeft;
        private StateText _deaconTextLeft;
        private StateText _eliteTextLeft;
        private StateText _levelTextRight;
        private StateText _memberTextRight;
        private StateText _fundTextRight;
        private StateText _viceLeaderTextRight;
        private StateText _deaconTextRight;
        private StateText _eliteTextRight;
        private StateText _conditionText;
        private IconContainer _iconContainer;
        private StateText _costText;
        private KParticle _levelUpEffect;
        private KContainer _levelUpContainer;

        private KContainer _conditionContainer;
        private KContainer _afterContainer;
        private Locater _beforeLocater;

        protected override void Awake()
        {
            _upgradeButton = GetChildComponent<KButton>("Button_shengji");
            _levelTextLeft = GetChildComponent<StateText>("Container_shengjiqian/Label_txtDengji1");
            _memberTextLeft = GetChildComponent<StateText>("Container_shengjiqian/Label_txtShangxian1");
            _fundTextLeft = GetChildComponent<StateText>("Container_shengjiqian/Label_txtZijin1");
            _viceLeaderTextLeft = GetChildComponent<StateText>("Container_shengjiqian/Label_txtFuhuizhangrenshu1");
            _deaconTextLeft = GetChildComponent<StateText>("Container_shengjiqian/Label_txtZhishirenshu1");
            _eliteTextLeft = GetChildComponent<StateText>("Container_shengjiqian/Label_txtJingyingrenshu1");
            _levelTextRight = GetChildComponent<StateText>("Container_shengjihou/Label_txtDengji2");
            _memberTextRight = GetChildComponent<StateText>("Container_shengjihou/Label_txtShangxian2");
            _fundTextRight = GetChildComponent<StateText>("Container_shengjihou/Label_txtZijin2");
            _viceLeaderTextRight = GetChildComponent<StateText>("Container_shengjihou/Label_txtFuhuizhangrenshu2");
            _deaconTextRight = GetChildComponent<StateText>("Container_shengjihou/Label_txtZhishirenshu2");
            _eliteTextRight = GetChildComponent<StateText>("Container_shengjihou/Label_txtJingyingrenshu2");
            _conditionText = GetChildComponent<StateText>("Container_tiaojian/Label_txtTiaojian");
            _costText = GetChildComponent<StateText>("Container_tiaojian/Label_txtShuliang");
            _iconContainer = AddChildComponent<IconContainer>("Container_tiaojian/Container_icon");
            _levelUpContainer = GetChildComponent<KContainer>("Container_shengji");
            _beforeLocater = AddChildComponent<Locater>("Container_shengjiqian");
            _afterContainer = GetChildComponent<KContainer>("Container_shengjihou");
            _conditionContainer = GetChildComponent<KContainer>("Container_tiaojian");
            _levelUpContainer.Visible = false;
            _levelUpEffect = AddChildComponent<KParticle>("Container_effect/fx_ui_6_3_1_tisheng");
            _levelUpEffect.Stop();
        }

        private void RefreshUpgradeView()
        {
            MyGuildData data = PlayerDataManager.Instance.GuildData.MyGuildData;
            _levelTextLeft.CurrentText.text = data.Level.ToString();
            _memberTextLeft.CurrentText.text = guild_level_helper.GetMemberCount(data.Level).ToString();
            _fundTextLeft.CurrentText.text = guild_level_helper.GetFundLimit(data.Level).ToString();
            _viceLeaderTextLeft.CurrentText.text = guild_level_helper.GetViceChairmanCount(data.Level).ToString();
            _deaconTextLeft.CurrentText.text = guild_level_helper.GetDeconCount(data.Level).ToString();
            _eliteTextLeft.CurrentText.text = guild_level_helper.GetEliteCount(data.Level).ToString();
            if (data.Level < guild_level_helper.GetMaxLevel())
            {
                _levelTextRight.CurrentText.text = (data.Level + 1).ToString();
                _memberTextRight.CurrentText.text = guild_level_helper.GetMemberCount(data.Level + 1).ToString();
                _fundTextRight.CurrentText.text = guild_level_helper.GetFundLimit(data.Level + 1).ToString();
                _viceLeaderTextRight.CurrentText.text = guild_level_helper.GetViceChairmanCount(data.Level + 1).ToString();
                _deaconTextRight.CurrentText.text = guild_level_helper.GetDeconCount(data.Level + 1).ToString();
                _eliteTextRight.CurrentText.text = guild_level_helper.GetEliteCount(data.Level + 1).ToString();
                _conditionText.CurrentText.text = guild_level_helper.GetGuildUpgradeConditionContent(data.Level + 1);
                int goldId = guild_level_helper.GetUpgradeCostCurrency(data.Level + 1);
                _iconContainer.SetIcon(item_helper.GetIcon(goldId));
                RefreshCostView(data);
                _beforeLocater.X = 147;
                _afterContainer.Visible = true;
                _conditionContainer.Visible = true;
                _upgradeButton.Visible = true;
            }
            else
            {
                _levelTextLeft.CurrentText.text = string.Concat(data.Level.ToString(), MogoLanguageUtil.GetContent(74889));
                _beforeLocater.X = 365;
                _conditionContainer.Visible = false;
                _afterContainer.Visible = false;
                _upgradeButton.Visible = false;
            }
        }

        private void RefreshCostView(MyGuildData data)
        {
            string value = guild_level_helper.GetUpgradeCostValue(data.Level + 1).ToString();
            if (guild_level_helper.IsItemEnough(guild_level_helper.GetUpgradeCostCurrency(data.Level + 1), int.Parse(value)) == false)
            {
                value = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, value);
            }
            _costText.CurrentText.text = value;
        }

        protected void AddEventListener()
        {
            _upgradeButton.onClick.AddListener(OnUpgradeButtonClick);
            EventDispatcher.AddEventListener(GuildEvents.Show_My_Guild_Info, RefreshUpgradeView);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_Level, OnGuildLevelUp);
        }

        protected void RemoveEventListener()
        {
            _upgradeButton.onClick.RemoveListener(OnUpgradeButtonClick);
            EventDispatcher.RemoveEventListener(GuildEvents.Show_My_Guild_Info, RefreshUpgradeView);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_Level, OnGuildLevelUp);
        }

        private uint _timerId;
        private void OnGuildLevelUp()
        {
            _levelUpEffect.Play();
            TweenViewMove tween = TweenViewMove.Begin(_levelUpContainer.gameObject, MoveType.Show, MoveDirection.Down, 0.5f);
            tween.Tweener.method = UITweener.Method.BounceIn;
            ResetTimer();
            _timerId = TimerHeap.AddTimer(1500, 0, OnRiseEnd);
        }

        private void OnRiseEnd()
        {
            _levelUpContainer.Visible = false;
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private void OnUpgradeButtonClick()
        {
            if (guild_rights_helper.CheckHasRights(GuildRightId.upgrade, 74624) == false) return;
            if (IsItemEnough() == false) return;
            GuildManager.Instance.UpgradeGuild();
        }

        private bool IsItemEnough()
        {
            MyGuildData data = PlayerDataManager.Instance.GuildData.MyGuildData;
            int goldId = guild_level_helper.GetUpgradeCostCurrency(data.Level + 1);
            int value = guild_level_helper.GetUpgradeCostValue(data.Level + 1);
            if (item_helper.GetMyMoneyCountByItemId(goldId) < value)
            {
                string tip = string.Format(MogoLanguageUtil.GetContent(74607), MogoLanguageUtil.GetContent(74599));
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, tip, PanelIdEnum.MainUIField);
                return false;
            }
            return true;
        }

        protected override void OnEnable()
        {
            AddEventListener();
            RefreshUpgradeView();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            ResetTimer();
            _levelUpContainer.Visible = false;
        }

    }
}
