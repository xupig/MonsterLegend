﻿using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuild
{
    public class MyGuildView : KContainer
    {
        private GuildBasisInformationView _basisInformationView;
        private GuildMemberView _memberView;
        private GuildApplyListView _applyView;
        private GuildUpgradeView _upgradView;
        private GuildRecordView _recordView;
        private GuildSkillView _skillView;
        private GuildAchievementView _achievementView;
        private GuildWarRecordView _warRecordView;
        private CategoryList _categoryList;

        private const int CATEGORY_GUILD_ACHIEVEMENT = 0;
        private const int CATEGORY_GUILD_SKILL = 1;
        private const int CATEGORY_BASIS_INFORMATION = 2;
        private const int CATEGORY_MEMBER_INFORMATION = 3;
        private const int CATEGORY_APPLY_LIST = 4;
        private const int CATEGORY_GUILD_UPGRAD = 5;
        private const int CATEGORY_GUILD_WAR_RECORD = 6;
        private const int CATEGORY_GUILD_RECORD = 7;

        protected override void Awake()
        {
            _basisInformationView = AddChildComponent<GuildBasisInformationView>("Container_jibenxinxi");
            _memberView = AddChildComponent<GuildMemberView>("Container_chengyuanxinxi");
            _applyView = AddChildComponent<GuildApplyListView>("Container_shenqingleibao");
            _upgradView = AddChildComponent<GuildUpgradeView>("Container_gonghuishengji");
            _recordView = AddChildComponent<GuildRecordView>("Container_gonghuijishi");
            _skillView = AddChildComponent<GuildSkillView>("Container_gonghuijineng");
            _achievementView = AddChildComponent<GuildAchievementView>("Container_gonghuichengjiu");
            _warRecordView = AddChildComponent<GuildWarRecordView>("Container_gonghuizhanji");
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            InitCategoryList();
            ShowBasisInformation();
        }

        private void InitCategoryList()
        {
            List<CategoryItemData> listToggleData = new List<CategoryItemData>();
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(74665)));
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(74856)));
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(74660)));
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(74661)));
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(74662)));
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(74663)));
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(111515)));
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(74664)));
            _categoryList.RemoveAll();
            _categoryList.SetDataList<CategoryListItem>(listToggleData);
        }

        protected void AddEventListener()
        {
            _categoryList.onSelectedIndexChanged.AddListener(OnCategoryChange);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_Point, RefreshGreenPoint);
        }

        protected void RemoveEventListener()
        {
            _categoryList.onSelectedIndexChanged.RemoveListener(OnCategoryChange);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_Point, RefreshGreenPoint);
        }

        private void RefreshGreenPoint()
        {
            (_categoryList.ItemList[CATEGORY_BASIS_INFORMATION] as CategoryListItem).SetPoint(PlayerDataManager.Instance.GuildData.HasNewNotice);
            (_categoryList.ItemList[CATEGORY_APPLY_LIST] as CategoryListItem).SetPoint(guild_helper.HasApplyJoinInfo());
            (_categoryList.ItemList[CATEGORY_GUILD_UPGRAD] as CategoryListItem).SetPoint(guild_helper.CanUpgrade());
            (_categoryList.ItemList[CATEGORY_GUILD_ACHIEVEMENT] as CategoryListItem).SetPoint(guild_helper.CanReward());
            (_categoryList.ItemList[CATEGORY_GUILD_SKILL] as CategoryListItem).SetPoint(guild_helper.CanLearnSkill());
        }

        public void SetViewId(int viewId)
        {
            _categoryList.SelectedIndex = viewId;
            OnCategoryChange(_categoryList, viewId);
        }

        private void OnCategoryChange(CategoryList categoryList, int index)
        {
            switch (index)
            {
                case CATEGORY_BASIS_INFORMATION:
                    ShowBasisInformation();
                    break;
                case CATEGORY_MEMBER_INFORMATION:
                    ShowMemberInformation();
                    break;
                case CATEGORY_APPLY_LIST:
                    ShowApplyList();
                    break;
                case CATEGORY_GUILD_UPGRAD:
                    ShowGuildUpgrad();
                    break;
                case CATEGORY_GUILD_RECORD:
                    ShowGuildRecord();
                    break;
                case CATEGORY_GUILD_ACHIEVEMENT:
                    ShowGuildAchievement();
                    break;
                case CATEGORY_GUILD_SKILL:
                    ShowGuildSkill();
                    break;
                case CATEGORY_GUILD_WAR_RECORD:
                    ShowGuildWarRecord();
                    break;

            }
        }

        private void ShowGuildWarRecord()
        {
            _basisInformationView.Visible = false;
            _memberView.Visible = false;
            _applyView.Visible = false;
            _upgradView.Visible = false;
            _recordView.Visible = false;
            _skillView.Visible = false;
            _achievementView.Visible = false;
            _warRecordView.Visible = true;
        }

        private void ShowGuildSkill()
        {
            _basisInformationView.Visible = false;
            _memberView.Visible = false;
            _applyView.Visible = false;
            _upgradView.Visible = false;
            _recordView.Visible = false;
            _skillView.Visible = true;
            _achievementView.Visible = false;
            _warRecordView.Visible = false;
        }

        private void ShowGuildAchievement()
        {
            _basisInformationView.Visible = false;
            _memberView.Visible = false;
            _applyView.Visible = false;
            _upgradView.Visible = false;
            _recordView.Visible = false;
            _skillView.Visible = false;
            _achievementView.Visible = true;
            _warRecordView.Visible = false;
        }

        private void ShowGuildRecord()
        {
            _basisInformationView.Visible = false;
            _memberView.Visible = false;
            _applyView.Visible = false;
            _upgradView.Visible = false;
            _recordView.Visible = true;
            _skillView.Visible = false;
            _achievementView.Visible = false;
            _warRecordView.Visible = false;
        }

        private void ShowGuildUpgrad()
        {
            _basisInformationView.Visible = false;
            _memberView.Visible = false;
            _applyView.Visible = false;
            _upgradView.Visible = true;
            _recordView.Visible = false;
            _skillView.Visible = false;
            _achievementView.Visible = false;
            _warRecordView.Visible = false;
        }

        private void ShowApplyList()
        {
            _basisInformationView.Visible = false;
            _memberView.Visible = false;
            _applyView.Visible = true;
            _upgradView.Visible = false;
            _recordView.Visible = false;
            _skillView.Visible = false;
            _achievementView.Visible = false;
            _warRecordView.Visible = false;
        }

        private void ShowMemberInformation()
        {
            _basisInformationView.Visible = false;
            _memberView.Visible = true;
            _applyView.Visible = false;
            _upgradView.Visible = false;
            _recordView.Visible = false;
            _skillView.Visible = false;
            _achievementView.Visible = false;
            _warRecordView.Visible = false;
        }

        private void ShowBasisInformation()
        {
            _basisInformationView.Visible = true;
            _memberView.Visible = false;
            _applyView.Visible = false;
            _upgradView.Visible = false;
            _recordView.Visible = false;
            _skillView.Visible = false;
            _achievementView.Visible = false;
            _warRecordView.Visible = false;
        }

        private void RefreshCategoryList()
        {
            List<int> hideList = function_helper.GetHideTabList((int)FunctionId.myGuild);
            _categoryList.SetHiddenList(hideList);
        }

        protected override void OnEnable()
        {
            _categoryList.SelectedIndex = 0;
            ShowBasisInformation();
            RefreshGreenPoint();
            RefreshCategoryList();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }
    }
}
