﻿#region 模块信息
/*==========================================
// 文件名：GuildShopView
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuild.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/1/26 17:36:12
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleGuild
{
    public class GuildShopView:KContainer
    {
        private StateText _txtContent;
        private MoneyItem _contributeItem;
        private MoneyItem _currencyItem;
        private KScrollView _scrollPage;
        private KPageableList _pageableList;
        private CategoryList _categoryList;
        private List<int> _pageIndexList;
        public static int selectedIndex = 0;

        protected override void Awake()
        {
            _txtContent = GetChildComponent<StateText>("Label_txtTishi");
            _contributeItem = AddChildComponent<MoneyItem>("Container_buyGold");
            _contributeItem.SetMoneyType(public_config.MONEY_TYPE_GUILD_CONTRIBUTION);
            _txtContent.CurrentText.text = (74966).ToLanguage();
            _currencyItem = AddChildComponent<MoneyItem>("Container_buyDiamond");
            _currencyItem.SetMoneyType(public_config.MONEY_TYPE_GUILD_FUND);
            _scrollPage = GetChildComponent<KScrollView>("ScrollView_scrollview");
            _scrollPage.Arrow.gameObject.AddComponent<RaycastComponent>().RayCast = false;
            _pageableList = _scrollPage.GetChildComponent<KPageableList>("mask/content");
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            _pageableList.SetDirection(KList.KListDirection.LeftToRight, 2, int.MaxValue);
            _pageableList.SetGap(10,28);
            _pageIndexList = new List<int>();
            SetCategoryList();
            base.Awake();
            
        }

        private int SortFunc(guild_shop_page page1,guild_shop_page page2)
        {
            return page1.__rank - page2.__rank;
        }

        private void SetCategoryList()
        {
            List<CategoryItemData> list = new List<CategoryItemData>();
            List<guild_shop_page> pageList = new List<guild_shop_page>();
            foreach(KeyValuePair<int,guild_shop_page> kvp in XMLManager.guild_shop_page)
            {
                pageList.Add(kvp.Value);
            }
            pageList.Sort(SortFunc);
            list.Add(CategoryItemData.GetCategoryItemData((74971).ToLanguage()));
            list.Add(CategoryItemData.GetCategoryItemData((74972).ToLanguage()));
            for (int i = 0; i < pageList.Count;i++ )
            {
                CategoryItemData data = CategoryItemData.GetCategoryItemData(pageList[i].__name.ToLanguage());
                list.Add(data);
                _pageIndexList.Add(pageList[i].__id);
            }
            _categoryList.SetDataList<CategoryListItem>(list);
        }

        private bool isFirst = true;
        public void Show(int index)
        {
            Visible = true;
            EventDispatcher.AddEventListener(GuildShopEvents.Show_List_View, OnRefreshGuild);
            _categoryList.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            EventDispatcher.AddEventListener(GuildShopEvents.Refresh_Praise,OnRefreshPraise);
            if(index == 0)
            {
                isFirst = true;
            }
            else
            {
                isFirst = false;
                _categoryList.SelectedIndex = index - 1;
            }
            GuildShopManager.Instance.GetShopInfo();
        }

        public void Hide()
        {
            Visible = false;
            EventDispatcher.RemoveEventListener(GuildShopEvents.Show_List_View, OnRefreshGuild);
            EventDispatcher.RemoveEventListener(GuildShopEvents.Refresh_Praise, OnRefreshPraise);
            if(_categoryList!=null)
            {
                _categoryList.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            }
        }

        private void OnRefreshGuild()
        {
            guild_shop_helper.UpdateMyGuildShop();
            RefreshData(isFirst);
            isFirst = false;
        }

        private void OnRefreshPraise()
        {
            if(_categoryList.SelectedIndex == 0)
            {
                RefreshData(false);
            }
        }

        private void OnSelectedIndexChanged(CategoryList list,int index)
        {
            RefreshData(false);
        }

        private void RefreshData(bool isShow)
        {
            if(isShow)
            {
                if(PlayerDataManager.Instance.GuildShopData.MyPraiseData.Count == 0)
                {
                    _categoryList.SelectedIndex = 1;

                }
                else
                {
                    _categoryList.SelectedIndex = 0;
                }
            }
            List<guild_shop> dataList;
            if (_categoryList.SelectedIndex == 0)
            {
                dataList = guild_shop_helper.GetPraisedShopList();
            }
            else if (_categoryList.SelectedIndex == 1)
            {
                dataList = guild_shop_helper.GetRareShopList();
            }
            else
            {
                dataList = guild_shop_helper.GetGuildShopList(_pageIndexList[_categoryList.SelectedIndex - 2]);
            }
            FilterInvalidData(dataList);
            dataList.Sort(SortGuildShopFunction);
            _pageableList.SetDataList<GuildShopItem>(dataList);
            selectedIndex = _categoryList.SelectedIndex;
        }

        private int SortGuildShopFunction(guild_shop shop1,guild_shop shop2)
        {
            if(shop1.__best_level!=shop2.__best_level)
            {
                if(shop1.__best_level == 0)
                {
                    return -1;
                }
                if(shop2.__best_level == 0)
                {
                    return 1;
                }
                if (shop1.__best_level <= PlayerAvatar.Player.level)
                {
                    if(shop2.__best_level <= PlayerAvatar.Player.level)
                    {
                        return shop2.__best_level - shop1.__best_level;
                    }
                    else
                    {
                        return -1;
                    }
                }
                else
                {
                    if(shop2.__best_level <= PlayerAvatar.Player.level)
                    {
                        return 1;
                    }
                    else
                    {
                        return shop1.__best_level - shop2.__best_level;
                    }
                }
            }
            return shop1.__sort_id - shop2.__sort_id;
        }

        private void FilterInvalidData(List<guild_shop> dataList)
        {
            GuildShopData shopData = PlayerDataManager.Instance.GuildShopData;
            for (int i = dataList.Count - 1; i >= 0; i--)
            {
                if (shopData.GetGuildShopData(Convert.ToUInt32(dataList[i].__id)) == null)
                {
                    dataList.RemoveAt(i);
                }
            }
        }

    }
}
