﻿using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuild
{
    public class GuildPositionView : KContainer
    {
        private KList _positionList;
        private KDummyButton _closeButton;
        private StateImage _contentImage;
        private float _originalHeight;

        private List<string> _contentList = new List<string>();
        public KComponentEvent<int> onPositionClick = new KComponentEvent<int>();

        protected override void Awake()
        {
            _positionList = GetChildComponent<KList>("List_content");
            _closeButton = AddChildComponent<KDummyButton>("Container_close");
            _contentImage = GetChildComponent<StateImage>("Image_content");
            _contentImage.AddAllStateComponent<Resizer>();
            _positionList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -2000);
            AddEventListener();
        }

        private void RefreshView()
        {
            _contentList.Clear();
            _contentList.Add(AppendMemberNumContent(MogoLanguageUtil.GetContent(74626), public_config.GUILD_POSITION_LEADER));
            _contentList.Add(AppendMemberNumContent(MogoLanguageUtil.GetContent(74627), public_config.GUILD_POSITION_VICE_LEADER));
            _contentList.Add(AppendMemberNumContent(MogoLanguageUtil.GetContent(74628), public_config.GUILD_POSITION_DEACON));
            _contentList.Add(AppendMemberNumContent(MogoLanguageUtil.GetContent(74629), public_config.GUILD_POSITION_ELITE));
            _contentList.Add(MogoLanguageUtil.GetContent(74630));
            _positionList.SetDataList<GuildPositionItem>(_contentList);
            DoLayout();
        }

        private string AppendMemberNumContent(string result, int position)
        {
            int level = PlayerDataManager.Instance.GuildData.MyGuildData.Level;
            result = string.Concat(result, " (", GetMemberCountByPosition(position), "/", guild_level_helper.GetMemberCountByPosition(level, position), ")");
            return result;
        }

        private int GetMemberCountByPosition(int position)
        {
            int count = 0;
            List<PbGuildMemberInfo> infoList = PlayerDataManager.Instance.GuildData.GuildMemberInfoList.member_info;
            for (int i = 0; i < infoList.Count; i++)
            {
                if (infoList[i].guild_position == position)
                {
                    count++; ;
                }
            }
            return count;
        }

        private void DoLayout()
        {
            Locater listLocater = _positionList.gameObject.AddComponent<Locater>();
            Locater imageLocater = _contentImage.gameObject.AddComponent<Locater>();
            _positionList.RecalculateSize();
            _originalHeight = _contentImage.Height;
            _contentImage.Height = listLocater.Height + 20;
            float offY = _originalHeight - _contentImage.Height;
            listLocater.Y -= offY;
            imageLocater.Y -= offY;
        }

        protected void AddEventListener()
        {
            _closeButton.onClick.AddListener(OnCloseButtonClick);
            _positionList.onItemClicked.AddListener(OnPositionClick);
        }

        protected void RemoveEventListener()
        {
            _closeButton.onClick.RemoveListener(OnCloseButtonClick);
            _positionList.onItemClicked.RemoveListener(OnPositionClick);
        }

        private void OnPositionClick(KList list, KList.KListItemBase item)
        {
            onPositionClick.Invoke(item.Index + 1);
            this.Visible = false;
        }

        private void OnCloseButtonClick()
        {
            this.Visible = false;
        }

        protected override void OnEnable()
        {
            RefreshView();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }

    }

    public class GuildPositionItem : KList.KListItemBase
    {
        private string _data;
        private StateText _contentText;
        private KButton _itemButton;

        protected override void Awake()
        {
            _itemButton = GetChildComponent<KButton>("Button_item");
            _contentText = GetChildComponent<StateText>("Button_item/label");
            _itemButton.onClick.AddListener(OnButtonClick);
        }

        private void OnButtonClick()
        {
            onClick.Invoke(this, this.Index);
        }

        public override void Dispose()
        {
            _itemButton.onClick.RemoveListener(OnButtonClick);
            base.Data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null && value != base.Data)
                {
                    _data = value as string;
                    RefreshContent();
                }
            }
        }

        private void RefreshContent()
        {
            _contentText.ChangeAllStateText(_data);
        }
    }
}
