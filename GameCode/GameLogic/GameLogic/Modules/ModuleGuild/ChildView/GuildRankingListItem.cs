﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Common.Base;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils;

using Game.UI.UIComponent;
using Common.Data;
using UnityEngine.UI;
using Common.Structs;
using GameMain.GlobalManager;
using ModuleCommonUI;
using Common.Utils;
using Common.Global;
using GameData;
using GameMain.GlobalManager.SubSystem;
using UnityEngine.EventSystems;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using GameMain.Entities;


namespace ModuleGuild
{
    public class GuildRankingListItem : KList.KListItemBase
    {
        private PbRankGuildInfo _info;
        private KContainer _backgroundContainer;
        private KContainer _rankingContainer;
        private StateText _rankingText;
        private StateText _nameText;
        private StateText _levelText;
        private StateText _leaderText;
        private StateText _numberText;
        private KButton _applyButton;
        private KButton _cancelbutton;
        private KContainer _fullContainer;

        protected override void Awake()
        {
            _backgroundContainer = GetChildComponent<KContainer>("Container_bg");
            _rankingContainer = GetChildComponent<KContainer>("Container_mingci");
            _rankingText = _rankingContainer.GetChildComponent<StateText>("Label_txtPaiming");
            _nameText = GetChildComponent<StateText>("Label_txtWanjiamingzi");
            _levelText = GetChildComponent<StateText>("Label_txtDengji");
            _leaderText = GetChildComponent<StateText>("Label_txtHuizhangmingcheng");
            _numberText = GetChildComponent<StateText>("Label_txtRenshu");
            _applyButton = GetChildComponent<KButton>("Button_shenqing");
            _cancelbutton = GetChildComponent<KButton>("Button_quxiao");
            _fullContainer = GetChildComponent<KContainer>("Container_yiman");
            AddEventListener();
        }

        public override object Data
        {
            get
            {
                return _info;
            }
            set
            {
                if (value != null && value != base.Data)
                {
                    if (value is PbGuildSearchInfo)
                    {
                        ConvertData2GuildInfo(value);
                    }
                    else
                    {
                        _info = value as PbRankGuildInfo;
                    }
                    RefreshItemView();
                }
            }
        }

        private void ConvertData2GuildInfo(object data)
        {
            _info = new PbRankGuildInfo();
            PbGuildSearchInfo info = data as PbGuildSearchInfo;
            _info.guild_name_bytes = info.guild_name_bytes;
            _info.guild_id = info.guild_id;
            _info.guild_level = info.guild_level;
            _info.guild_member_num = info.guild_member_count;
            _info.leader_name_bytes = info.leader_name_bytes;
            _info.rank_index = info.ranking;
        }

        private void RefreshItemView()
        {
            _nameText.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_info.guild_name_bytes);
            _levelText.CurrentText.text = _info.guild_level.ToString();
            _leaderText.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_info.leader_name_bytes);
            _numberText.CurrentText.text = _info.guild_member_num.ToString();
            RefreshRankingView();
            RefreshBackgroundView();
            RefreshButtonVisibility();
        }

        private void RefreshButtonVisibility()
        {
            _applyButton.Visible = false;
            _cancelbutton.Visible = false;
            _fullContainer.Visible = false;
            List<ulong> list = PlayerDataManager.Instance.GuildData.GetGuildApplyJoinList();
            for (int i = 0; i < list.Count; i++)
            {
                if (_info.guild_id == list[i])
                {
                    _cancelbutton.Visible = true;
                    return;
                }
            }
            if (guild_level_helper.GetMemberCount((int)_info.guild_level) <= (int)_info.guild_member_num)
            {
                _fullContainer.Visible = true;
            }
            else
            {
                _applyButton.Visible = true;
            }
        }

        private void RefreshBackgroundView()
        {
            _backgroundContainer.GetChild("ScaleImage_sharedGridBg").SetActive(Index % 2 == 0);
            _backgroundContainer.GetChild("ScaleImage_sharedGridBg02").SetActive(Index % 2 == 1);
        }

        private void RefreshRankingView()
        {
            _rankingText.CurrentText.text = _info.rank_index > 3 ? _info.rank_index.ToString() : string.Empty;
            _rankingContainer.GetChild("Image_firstLcon").SetActive(_info.rank_index == 1);
            _rankingContainer.GetChild("Image_secondLcon").SetActive(_info.rank_index == 2);
            _rankingContainer.GetChild("Image_thirdLcon").SetActive(_info.rank_index == 3);
            _rankingContainer.GetChild("Label_txtWeishangbang").SetActive(_info.rank_index == 0);
        }

        public void AddEventListener()
        {
            _applyButton.onClick.AddListener(OnApplyButtonClick);
            _cancelbutton.onClick.AddListener(OnCancelButtonClick);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_list_Item, RefreshButtonVisibility);
        }

        public void RemoveEventListener()
        {
            _applyButton.onClick.RemoveListener(OnApplyButtonClick);
            _cancelbutton.onClick.RemoveListener(OnCancelButtonClick);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_list_Item, RefreshButtonVisibility);
        }

        private void OnCancelButtonClick()
        {
            GuildManager.Instance.CancelJoinGuild(_info.guild_id);
        }

        private void OnApplyButtonClick()
        {
            int needLevel = int.Parse(global_params_helper.GetGlobalParam(82));
            if (PlayerAvatar.Player.level < needLevel)
            {
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74612, needLevel), PanelIdEnum.MainUIField);
                return;
            }
            GuildManager.Instance.ApplyJoinGuild(_info.guild_id);
        }

        public override void Dispose()
        {
            RemoveEventListener();
            base.Data = null;
        }
    }
}