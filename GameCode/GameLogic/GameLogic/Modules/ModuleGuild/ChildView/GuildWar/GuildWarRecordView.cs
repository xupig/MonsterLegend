﻿using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;

namespace ModuleGuild
{
    public class GuildWarRecordView : KContainer
    {
        private KButton _historyBtn;
        private KButton _backBtn;
        private GuildWarRecordListView _listView;
        private GuildWarHistoryView _historyView;

        protected override void Awake()
        {
            _historyBtn = GetChildComponent<KButton>("Button_wangjiegonghuizhan");
            _backBtn = GetChildComponent<KButton>("Button_fanhui");
            _listView = AddChildComponent<GuildWarRecordListView>("Container_paihangbang");
            _historyView = AddChildComponent<GuildWarHistoryView>("Container_wangjiepaihang");
        }

        protected void AddEventListener()
        {
            _historyBtn.onClick.AddListener(OnhistoryBtnClick);
            _backBtn.onClick.AddListener(OnBackBtnClick);
        }

        protected void RemoveEventListener()
        {
            _historyBtn.onClick.RemoveListener(OnhistoryBtnClick);
            _backBtn.onClick.RemoveListener(OnBackBtnClick);
        }

        private void RefreshContent()
        {
            ShowListView();
        }

        private void OnBackBtnClick()
        {
            ShowListView();
        }

        private void OnhistoryBtnClick()
        {
            ShowHistoryView();
        }

        private void ShowListView()
        {
            _backBtn.Visible = false;
            _historyBtn.Visible = true;
            _listView.Visible = true;
            _historyView.Visible = false;
        }

        private void ShowHistoryView()
        {
            _backBtn.Visible = true;
            _historyBtn.Visible = false;
            _listView.Visible = false;
            _historyView.Visible = true;
        }

        protected override void OnEnable()
        {
            AddEventListener();
            RefreshContent();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

    }
}
