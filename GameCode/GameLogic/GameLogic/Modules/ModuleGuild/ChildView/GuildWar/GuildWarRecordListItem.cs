﻿using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuild
{
    public class GuildWarRecordListItem : KList.KListItemBase
    {
        private StateText _guildTxt;
        private StateText _dateTxt;
        private StateText _gradeTxt;
        private KButton _detailBtn;

        private PbGuildBattleLog _data;

        protected override void Awake()
        {
            _guildTxt = GetChildComponent<StateText>("Label_txtGonghui");
            _dateTxt = GetChildComponent<StateText>("Label_txtRiqi");
            _gradeTxt = GetChildComponent<StateText>("Label_txtBifen");
            _detailBtn = GetChildComponent<KButton>("Button_xiangxi");
        }

        protected void AddEventListener()
        {
            _detailBtn.onClick.AddListener(OnDetailBtnClick);
        }

        protected void RemoveEventListener()
        {
            _detailBtn.onClick.RemoveListener(OnDetailBtnClick);
        }

        private void OnDetailBtnClick()
        {
            GuildWarManager.Instance.GetGuildWarResultRecord(_data.log_dbid, PlayerDataManager.Instance.GuildData.MyGuildData.Id);
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbGuildBattleLog;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            RefreshContent();
        }

        private void RefreshContent()
        {
            _guildTxt.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.guild_name_bytes);
            _dateTxt.CurrentText.text = MogoTimeUtil.ToLocalTime((long)_data.date, "yyyy-MM-dd");
            _gradeTxt.CurrentText.text = string.Format("{0}-{1}", _data.self_point, _data.to_point);
        }

        public override void Show()
        {
            AddEventListener();
        }

        public override void Hide()
        {
            RemoveEventListener();
        }


        public override void Dispose()
        {
            _data = null;
        }
    }
}
