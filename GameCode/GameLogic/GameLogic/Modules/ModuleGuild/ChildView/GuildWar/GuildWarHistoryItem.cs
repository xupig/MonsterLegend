﻿using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Utils;
using System.Collections.Generic;

namespace ModuleGuild
{
    public class GuildWarHistoryItem : KList.KListItemBase
    {
        private StateText _guildTxt;
        private StateText _dateTxt;
        private StateText _seasonTxt;
        private KButton _detailBtn;
        private RankContainer _rankContainer;

        private PbGuildBattleResultIndexInfo _data;

        protected override void Awake()
        {
            _guildTxt = GetChildComponent<StateText>("Label_txtGonghui");
            _dateTxt = GetChildComponent<StateText>("Label_txtRiqi");
            _seasonTxt = GetChildComponent<StateText>("Label_txtJieshu");
            _detailBtn = GetChildComponent<KButton>("Button_xiangxi");
            _rankContainer = AddChildComponent<RankContainer>("Container_mingci");
        }

        protected void AddEventListener()
        {
            _detailBtn.onClick.AddListener(OnDetailBtnClick);
        }

        protected void RemoveEventListener()
        {
            _detailBtn.onClick.RemoveListener(OnDetailBtnClick);
        }

        private void OnDetailBtnClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarRankingTips, (int)_data.session);
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbGuildBattleResultIndexInfo;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            RefreshContent();
        }

        private void RefreshContent()
        {
            _seasonTxt.CurrentText.text = MogoLanguageUtil.GetContent(111631,_data.session);
            _guildTxt.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.guild_name_bytes);
            _rankContainer.SetRank((int)_data.self_index);
            List<string> openList = guild_match_open_helper.GetOpenDate((int)_data.session);
            string start = string.Concat(openList[0].Substring(0, 4), "-", openList[0].Substring(4, 2), "-", openList[0].Substring(6, 2));
            string end = string.Concat(openList[1].Substring(0, 4), "-", openList[1].Substring(4, 2), "-", openList[1].Substring(6, 2));
            _dateTxt.CurrentText.text = MogoLanguageUtil.GetContent(111633, start, end);
        }

        public override void Show()
        {
            AddEventListener();
        }

        public override void Hide()
        {
            RemoveEventListener();
        }


        public override void Dispose()
        {
            _data = null;
        }
    }
}
