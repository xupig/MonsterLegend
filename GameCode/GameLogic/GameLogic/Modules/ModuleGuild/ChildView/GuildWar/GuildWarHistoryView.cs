﻿using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuild
{
    public class GuildWarHistoryView : KContainer
    {
        private StateText _noneTxt;
        private KScrollView _scrollView;
        private KList _list;

        private List<PbGuildBattleResultIndexInfo> _dataList = new List<PbGuildBattleResultIndexInfo>();

        protected override void Awake()
        {
            base.Awake();
            _noneTxt = GetChildComponent<StateText>("Label_txtmeiyoushuju");
            _scrollView = GetChildComponent<KScrollView>("Container_zhankuangduibi/ScrollView_zhanlibang");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            RefreshContent();
            OnReRequest(_scrollView.ScrollRect);
        }

        private void RefreshContent()
        {
            _noneTxt.Visible = false;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        protected void AddEventListener()
        {
            _scrollView.ScrollRect.onReRequest.AddListener(OnReRequest);
            _scrollView.ScrollRect.onRequestNext.AddListener(OnRequestNext);
            EventDispatcher.AddEventListener<PbGuildBattleResultIndexInfoList>(GuildWarEvents.On_Get_Guild_War_History_Log, RefreshListContent);
        }

        protected void RemoveEventListener()
        {
            _scrollView.ScrollRect.onReRequest.RemoveListener(OnReRequest);
            _scrollView.ScrollRect.onRequestNext.RemoveListener(OnRequestNext);
            EventDispatcher.RemoveEventListener<PbGuildBattleResultIndexInfoList>(GuildWarEvents.On_Get_Guild_War_History_Log, RefreshListContent);
        }

        private void RefreshListContent(PbGuildBattleResultIndexInfoList list)
        {
            _dataList.AddRange(list.list);
            if (_dataList.Count == 0)
            {
                _noneTxt.Visible = true;
            }
            else
            {
                _noneTxt.Visible = false;
            }
            _scrollView.ScrollRect.StopMovement();
            _list.SetDataList<GuildWarHistoryItem>(_dataList, 4);
        }

        private void OnRequestNext(KScrollRect arg0)
        {
            GuildWarManager.Instance.GetGuildWarHistoryLog(_dataList.Count + 1, 10);
        }

        private void OnReRequest(KScrollRect arg0)
        {
            _dataList.Clear();
            GuildWarManager.Instance.GetGuildWarHistoryLog(1, 10);
        }

    }
}
