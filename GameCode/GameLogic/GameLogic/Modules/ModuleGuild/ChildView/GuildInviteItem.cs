﻿
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuild
{
    public class GuildInviteItem:KList.KListItemBase
    {
        private KButton _rejectButton;
        private KButton _acceptButton;

        private StateText _txtLevel;
        private StateText _txtName;
        private StateText _txtFightForce;
        private StateText _txtInviteInfo;

        private IconContainer _headIconContainer;

        private PbGuildInviteJoin _data;

        protected override void Awake()
        {
            _rejectButton = GetChildComponent<KButton>("Button_jujue");
            _acceptButton = GetChildComponent<KButton>("Button_jieshou");

            _txtLevel = GetChildComponent<StateText>("Container_Duiyuan0/Container_playerInfo/Container_dengji/Label_txtDengji");
            _txtName = GetChildComponent<StateText>("Container_Duiyuan0/Label_txtName");
            _txtFightForce = GetChildComponent<StateText>("Container_Duiyuan0/Label_txtZhanli");
            _txtInviteInfo = GetChildComponent<StateText>("Container_Duiyuan0/Label_yaoqingxinxi");

            _headIconContainer = AddChildComponent<IconContainer>("Container_Duiyuan0/Container_playerInfo/Container_icon");
            AddEventListener();
        }

        public override void Dispose()
        {
            _data = null;
            RemoveEventsListener();
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbGuildInviteJoin;
                Refresh();
            }
        }

        private void AddEventListener()
        {
            _rejectButton.onClick.AddListener(RejectButtonClick);
            _acceptButton.onClick.AddListener(AcceptButtonClick);
        }

        private void RemoveEventsListener()
        {
            _rejectButton.onClick.RemoveListener(RejectButtonClick);
            _acceptButton.onClick.RemoveListener(AcceptButtonClick);
        }

        private void RejectButtonClick()
        {
            PlayerDataManager.Instance.GuildData.RemoveInviteInfo(_data.dbid);
        }

        private void AcceptButtonClick()
        {
            GuildManager.Instance.AcceptInvite(_data.dbid);
            PlayerDataManager.Instance.GuildData.RemoveInviteInfo(_data.dbid);
        }

        private void Refresh()
        {
            _txtLevel.CurrentText.text = _data.level.ToString();
            _txtFightForce.CurrentText.text = _data.fight_force.ToString();
            _txtName.CurrentText.text = _data.name;
            _txtInviteInfo.CurrentText.text = MogoLanguageUtil.GetContent(74886, _data.guild_level, _data.guild_name);
            _headIconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_data.vocation));
        }
    }
}
