﻿using ACTSystem;
using Common.Base;
using Common.Chat;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuild
{
    public class GuildMemberItem : KList.KListItemBase
    {
        private PbGuildMemberInfo _data;
        private StateText _nameText;
        private StateText _levelText;
        private StateText _fightForceText;
        private StateText _positionText;
        private StateText _contributeText;
        private StateImage _selectImage;

        private Color _originalColor;

        protected override void Awake()
        {
            _nameText = GetChildComponent<StateText>("Label_txtWanjia");
            _levelText = GetChildComponent<StateText>("Label_txtDengji");
            _fightForceText = GetChildComponent<StateText>("Label_txtZhandouli");
            _positionText = GetChildComponent<StateText>("Label_txtZhiwei");
            _contributeText = GetChildComponent<StateText>("Label_txtgongxian");
            _selectImage = GetChildComponent<StateImage>("Image_xuanzhong");
            _originalColor = _nameText.CurrentText.color;
        }

        public void SetSelect(bool select)
        {
            _selectImage.Visible = select;
        }

        public override void Dispose()
        {
            SetSelect(false);
            base.Data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbGuildMemberInfo;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            RefreshContent();
        }

        private void RefreshContent()
        {
            _nameText.CurrentText.text = _data.name;
            _levelText.CurrentText.text = _data.level.ToString();
            _fightForceText.CurrentText.text = _data.fight.ToString();
            _positionText.CurrentText.text = MogoLanguageUtil.GetContent(74626 + (int)_data.guild_position - 1);
            _contributeText.CurrentText.text = _data.yesterday_contribution.ToString();
            RefreshTextGray(_data.online == 0);
        }

        private void RefreshTextGray(bool isGray)
        {
            TextWrapper[] textArray = GetComponentsInChildren<TextWrapper>();
            for (int i = 0; i < textArray.Length; i++)
            {
                if (isGray)
                {
                    textArray[i].color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_GRAY);
                }
                else
                {
                    textArray[i].color = _originalColor;
                }
            }
        }

    }
}
