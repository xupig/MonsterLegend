﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Utils;
using UnityEngine;

namespace ModuleGuild
{
    public class GuildApplyListView : KContainer
    {
        private KScrollView _scrollView;
        private KList _list;
        private StateText _noDataText;

        protected override void Awake()
        {
            _noDataText = GetChildComponent<StateText>("Label_txtmeiyoushuju");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_haoyou");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _list.SetGap(-10, 0);
        }

        protected void AddEventListener()
        {
            EventDispatcher.AddEventListener(GuildEvents.Show_Apply_List, ShowApplyList);
        }

        protected void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(GuildEvents.Show_Apply_List, ShowApplyList);
        }

        private void ShowApplyList()
        {
            _list.SetDataList<GuildApplyItem>(PlayerDataManager.Instance.GuildData.GuildApplyJoinInfoList.apply_join_info, 2);
            _noDataText.gameObject.SetActive(PlayerDataManager.Instance.GuildData.GuildApplyJoinInfoList.apply_join_info.Count == 0);
        }

        protected override void OnEnable()
        {
            GuildManager.Instance.GetGuildJoinList();
            AddEventListener();
            ShowApplyList();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            PlayerDataManager.Instance.GuildData.RefreshNotice();
        }
    }

    public class GuildApplyItem : KList.KListItemBase
    {
        private PbGuildApplyJoinInfo _data;
        private StateText _levelText;
        private StateText _nameText;
        private StateText _fightForceText;
        private KButton _checkButton;
        private KButton _corssButton;
        private IconContainer _iconContainer;
        private StateText _vipLevel;

        protected override void Awake()
        {
            _levelText = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            _nameText = GetChildComponent<StateText>("Label_txtwanjiamingzi");
            _fightForceText = GetChildComponent<StateText>("Label_txtZhanli02");
            _checkButton = GetChildComponent<KButton>("Button_zhengque");
            _corssButton = GetChildComponent<KButton>("Button_cuowu");
            _vipLevel = GetChildComponent<StateText>("Label_txtDengji");
            _vipLevel.Visible = false;
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _checkButton.onClick.AddListener(OnCheckButtonClick);
            _corssButton.onClick.AddListener(OnCrossButtonClick);
        }

        private void RefreshContent()
        {
            _levelText.CurrentText.text = _data.level.ToString();
            _nameText.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.name_bytes);
            _fightForceText.CurrentText.text = _data.fight.ToString();
            _vipLevel.CurrentText.text = string.Format("VIP{0}", _data.vip_level);
            _iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_data.vocation));
        }

        private void OnCrossButtonClick()
        {
            if (guild_rights_helper.CheckHasRights(GuildRightId.examine, 74625) == false) return;
            GuildManager.Instance.RefuseApply(_data.dbid);
        }

        private void OnCheckButtonClick()
        {
            if (guild_rights_helper.CheckHasRights(GuildRightId.examine, 74625) == false) return;
            GuildManager.Instance.AcceptApply(_data.dbid);
        }

        public override void Dispose()
        {
            _checkButton.onClick.RemoveListener(OnCheckButtonClick);
            _corssButton.onClick.RemoveListener(OnCrossButtonClick);
            base.Data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbGuildApplyJoinInfo;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            RefreshContent();
        }

    }
}
