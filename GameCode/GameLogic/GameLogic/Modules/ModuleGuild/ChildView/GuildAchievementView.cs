﻿using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuild
{
    public class GuildAchievementView : KContainer
    {
        private MoneyItem _fundItem;
        private MoneyItem _contributeItem;
        private KToggleGroup _siftToggleGroup;
        private KScrollView _scrollView;
        private KList _list;
        private int _currentIndex = 0;

        protected override void Awake()
        {
            _fundItem = AddChildComponent<MoneyItem>("Container_buyDiamond");
            _contributeItem = AddChildComponent<MoneyItem>("Container_buyGold");
            _fundItem.SetMoneyType(public_config.MONEY_TYPE_GUILD_FUND);
            _contributeItem.SetMoneyType(public_config.MONEY_TYPE_GUILD_CONTRIBUTION);
            _siftToggleGroup = GetChildComponent<KToggleGroup>("Container_chengjiu/ToggleGroup_saixuan");
            _scrollView = GetChildComponent<KScrollView>("Container_chengjiu/ScrollView_haoyou");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _list.SetGap(-10, 0);
        }


        protected void AddEventListener()
        {        
            _siftToggleGroup.onSelectedIndexChanged.AddListener(OnSiftChanged);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Achievement_View, RefreshAchievementContent);
        }

        protected void RemoveEventListener()
        {
            _siftToggleGroup.onSelectedIndexChanged.RemoveListener(OnSiftChanged);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Achievement_View, RefreshAchievementContent);
        }

        private void OnSiftChanged(KToggleGroup toggleGroup, int index)
        {
            if(_currentIndex != index)
            {
                _currentIndex = index;
                RefreshAchievementContent();
            }
        }

        private void RefreshAchievementContent()
        {
            if (_currentIndex == 0)
            {
                List<GuildAchievementData> list = PlayerDataManager.Instance.GuildData.GetAllAchievementList();
                _list.SetDataList<GuildAchievementItem>(list);
            }
            else if (_currentIndex == 1)
            {
                List<GuildAchievementData> list = PlayerDataManager.Instance.GuildData.GetNotReachAchievementList();
                _list.SetDataList<GuildAchievementItem>(list);
            }
            else
            {
                List<GuildAchievementData> list = PlayerDataManager.Instance.GuildData.GetReachAchievementList();
                _list.SetDataList<GuildAchievementItem>(list);
            }
            TweenListEntry.Begin(_list.gameObject);
        }

        protected override void OnEnable()
        {
            GuildManager.Instance.GetAchievementList();
            AddEventListener();
            RefreshAchievementContent();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

    }

    public class GuildAchievementItem : KList.KListItemBase
    {
        private GuildAchievementData _data;
        private StateText _nameText;
        private StateText _descriptionText;
        private StateImage _reachedImage;
        private StateText _underwayText;
        private KButton _getButton;
        private KProgressBar _progressBar;
        private StateText _progressText;
        private IconContainer _rewardIcon1;
        private StateText _rewardText1;
        private IconContainer _rewardIcon2;
        private StateText _rewardText2;

        protected override void Awake()
        {
            _nameText = GetChildComponent<StateText>("Label_txtChengjiu");
            _descriptionText = GetChildComponent<StateText>("Label_txtMiaoshu");
            _reachedImage = GetChildComponent<StateImage>("Image_yidacheng");
            _underwayText = GetChildComponent<StateText>("Label_txtJinxingzhong");
            _getButton = GetChildComponent<KButton>("Button_btn3");
            _progressBar = GetChildComponent<KProgressBar>("ProgressBar_jindu");
            _progressText = GetChildComponent<StateText>("Label_txtJindu");
            _rewardIcon1 = AddChildComponent<IconContainer>("Container_icon1");
            _rewardText1 = GetChildComponent<StateText>("Label_txtshuliang1");
            _rewardIcon2 = AddChildComponent<IconContainer>("Container_icon2");
            _rewardText2 = GetChildComponent<StateText>("Label_txtshuliang2");
            _getButton.onClick.AddListener(OnGetButtonClick);
        }

        private void OnGetButtonClick()
        {
            GuildManager.Instance.GetAchievementReward(_data.Id);
        }

        private void RefreshContent()
        {
            _nameText.CurrentText.text = _data.Name;
            _descriptionText.CurrentText.text = _data.Description;
            RefreshStatus();
            RefreshReward();
            RefreshProgress();
        }

        private void RefreshProgress()
        {
            _progressBar.Value = _data.Progress;
            _progressText.CurrentText.text = _data.ProgressContent;
        }

        private void RefreshReward()
        {
            _rewardIcon1.Visible = false;
            _rewardText1.Visible = false;
            _rewardIcon2.Visible = false;
            _rewardText2.Visible = false;
            int rewardIndex = 0;
            foreach (KeyValuePair<string, string> kvp in _data.Reward)
            {
                rewardIndex++;
                if (rewardIndex == 1)
                {
                    _rewardText1.CurrentText.text = kvp.Value;
                    _rewardIcon1.SetIcon(item_helper.GetIcon(int.Parse(kvp.Key)));
                    _rewardIcon1.Visible = true;
                    _rewardText1.Visible = true;
                }
                if (rewardIndex == 2)
                {
                    _rewardText2.CurrentText.text = kvp.Value;
                    _rewardIcon2.SetIcon(item_helper.GetIcon(int.Parse(kvp.Key)));
                    _rewardIcon2.Visible = true;
                    _rewardText2.Visible = true;
                }
            }
        }

        private void RefreshStatus()
        {
            _reachedImage.Visible = false;
            _underwayText.Visible = false;
            _getButton.Visible = false;
            if (_data.Status == public_config.ACHIEVEMENT_STATE_REWARDED)
            {
                _reachedImage.Visible = true;
            }
            else if (_data.Status == public_config.ACHIEVEMENT_STATE_ACCOMPLISH)
            {
                _getButton.Visible = true;
            }
            else
            {
                _underwayText.Visible = true;
            }
        }

        public override void Dispose()
        {
            _getButton.onClick.RemoveListener(OnGetButtonClick);
            base.Data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null && value != base.Data)
                {
                    _data = value as GuildAchievementData;
                    RefreshContent();
                }
            }
        }
    }
}
