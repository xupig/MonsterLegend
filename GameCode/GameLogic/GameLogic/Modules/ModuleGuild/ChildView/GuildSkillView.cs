﻿using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuild
{
    public class GuildSkillView : KContainer
    {
        private MoneyItem _fundItem;
        private MoneyItem _contributeItem;
        private KScrollView _scrollView;
        private KList _list;
        private StateText _nameText;
        private StateText _levelText;
        private StateText _typeText;
        private StateText _effectText;
        private StateText _nextEffectText;
        private StateText _nextEffectName;
        private StateText _conditionText;
        private KButton _learnButton;
        private KButton _upgradeButton;
        private IconContainer _costIcon1;
        private StateText _costText1;
        private IconContainer _costIcon2;
        private StateText _costText2;
        private KContainer _upgradeContainer;
        private List<GuildSkillData> _skillList = new List<GuildSkillData>();
        private int _currentSkillIndex = 0;

        protected override void Awake()
        {
            _fundItem = AddChildComponent<MoneyItem>("Container_buyDiamond");
            _contributeItem = AddChildComponent<MoneyItem>("Container_buyGold");
            _fundItem.SetMoneyType(public_config.MONEY_TYPE_GUILD_FUND);
            _contributeItem.SetMoneyType(public_config.MONEY_TYPE_GUILD_CONTRIBUTION);
            _scrollView = GetChildComponent<KScrollView>("Container_left/ScrollView_jineng");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _list.SetGap(-10, 0);
            _nameText = GetChildComponent<StateText>("Container_right/Label_txtJinengmingcheng");
            _levelText = GetChildComponent<StateText>("Container_right/Label_txtDengji1");
            _typeText = GetChildComponent<StateText>("Container_right/Label_txtLeixing1");
            _effectText = GetChildComponent<StateText>("Container_right/Label_txtDangqianxiaoguo1");
            _nextEffectText = GetChildComponent<StateText>("Container_right/Label_txtXiajixiaoguo1");
            _nextEffectName = GetChildComponent<StateText>("Container_right/Label_txtXiajixiaoguo");
            _conditionText = GetChildComponent<StateText>("Container_right/Container_shengji/Label_txtSuoxutiaojian1");
            _costIcon1 = AddChildComponent<IconContainer>("Container_right/Container_shengji/Container_icon1");
            _costText1 = GetChildComponent<StateText>("Container_right/Container_shengji/Label_txtSuoxuxiaohao1");
            _costIcon2 = AddChildComponent<IconContainer>("Container_right/Container_shengji/Container_icon2");
            _costText2 = GetChildComponent<StateText>("Container_right/Container_shengji/Label_txtSuoxuxiaohao2");
            _learnButton = GetChildComponent<KButton>("Container_right/Container_shengji/Button_xuexi");
            _upgradeButton = GetChildComponent<KButton>("Container_right/Container_shengji/Button_shengji");
            _upgradeContainer = GetChildComponent<KContainer>("Container_right/Container_shengji");
        }

        protected void AddEventListener()
        {        
            _learnButton.onClick.AddListener(OnLearnButtonClick);
            _list.onItemClicked.AddListener(OnItemClick);
            _upgradeButton.onClick.AddListener(OnUpgradeButtonClick);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Skill_View, RefreshContent);
        }

        protected void RemoveEventListener()
        {
            _learnButton.onClick.RemoveListener(OnLearnButtonClick);
            _list.onItemClicked.RemoveListener(OnItemClick);
            _upgradeButton.onClick.RemoveListener(OnUpgradeButtonClick);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Skill_View, RefreshContent);
        }

        private void RefreshContent()
        {
            if (_currentSkillIndex >= _skillList.Count)
            {
                _currentSkillIndex = 0;
            }
            _skillList = PlayerDataManager.Instance.GuildData.GetSkillList();
            _list.SetDataList<GuildSkillItem>(_skillList);
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                GuildSkillItem _currentItem = _list[i] as GuildSkillItem;
                _currentItem.SetSelect(false);
            }
            RefreshItemSelect(_currentSkillIndex, true);
            RefreshSkillDetailView();
        }

        private void OnItemClick(KList list, KList.KListItemBase item)
        {
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                GuildSkillItem _currentItem = _list[i] as GuildSkillItem;
                _currentItem.SetSelect(false);
            }
            _currentSkillIndex = item.Index;
            RefreshItemSelect(_currentSkillIndex, true);
            RefreshSkillDetailView();
        }

        private void InitDetailView()
        {
            _nameText.CurrentText.text = "";
            _levelText.CurrentText.text = "";
            _typeText.CurrentText.text = "";
            _effectText.CurrentText.text = "";
            _nextEffectText.CurrentText.text = "";
            _conditionText.CurrentText.text = "";
            _costIcon1.Visible = false;
            _costText1.Visible = false;
            _costIcon2.Visible = false;
            _costText2.Visible = false;
            _learnButton.Visible = false;
            _upgradeButton.Visible = false;
        }

        private void RefreshSkillDetailView()
        {
            if (_skillList.Count == 0)
            {
                InitDetailView();
                return;
            }
            GuildSkillData data = _skillList[_currentSkillIndex];
            _nameText.CurrentText.text = data.Name;
            _levelText.CurrentText.text = data.Level.ToString();
            _typeText.CurrentText.text = data.Type;
            _effectText.CurrentText.text = data.Effect;
            _conditionText.CurrentText.text = data.ConditionContent;
            int nextId = guild_spell_helper.GetNextLevelId(data.Id);
            if (nextId == 0)
            {
                _upgradeContainer.Visible = false;
                _nextEffectName.Visible = false;
                _nextEffectText.Visible = false;
            }
            else
            {
                _upgradeContainer.Visible = true;
                _nextEffectName.Visible = true;
                _nextEffectText.Visible = true;
                _nextEffectText.CurrentText.text = data.NextEffect;
                RefreshButtonVisibility(data);
                RefreshCost(data);
            }
        }

        private void RefreshButtonVisibility(GuildSkillData data)
        {
            _learnButton.Visible = false;
            _upgradeButton.Visible = false;
            int nextId = guild_spell_helper.GetNextLevelId(data.Id);
            if (nextId == 0) return;
            if (data.HasLearned == true)
            {
                _upgradeButton.Visible = true;
            }
            else
            {
                _learnButton.Visible = true;
            }
        }

        private void RefreshItemSelect(int index, bool select)
        {
            if (_skillList.Count > index)
            {
                GuildSkillItem _currentItem = _list[index] as GuildSkillItem;
                _currentItem.SetSelect(select);
            }
        }

        private void RefreshCost(GuildSkillData data)
        {
            _costIcon1.Visible = false;
            _costText1.Visible = false;
            _costIcon2.Visible = false;
            _costText2.Visible = false;
            int costIndex = 0;
            if (data.Cost == null) return;
            foreach (KeyValuePair<string, string> kvp in data.Cost)
            {
                string value = kvp.Value;
                if (guild_level_helper.IsItemEnough(int.Parse(kvp.Key), int.Parse(kvp.Value)) == false)
                {
                    value = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, value);
                }
                costIndex++;
                if (costIndex == 1)
                {
                    _costText1.CurrentText.text = value;
                    _costIcon1.SetIcon(item_helper.GetIcon(int.Parse(kvp.Key)));
                    _costIcon1.Visible = true;
                    _costText1.Visible = true;
                }
                if (costIndex == 2)
                {
                    _costText2.CurrentText.text = value;
                    _costIcon2.SetIcon(item_helper.GetIcon(int.Parse(kvp.Key)));
                    _costIcon2.Visible = true;
                    _costText2.Visible = true;
                }
            }
        }

        private void OnLearnButtonClick()
        {
            if (_skillList.Count == 0) return;
            GuildSkillData data = _skillList[_currentSkillIndex];
            StudyGuildSkill(data.Id);
        }

        private void OnUpgradeButtonClick()
        {
            if (_skillList.Count == 0) return;
            GuildSkillData data = _skillList[_currentSkillIndex];
            int nextId = guild_spell_helper.GetNextLevelId(data.Id);
            if (nextId != 0)
            {
                StudyGuildSkill(nextId);
            }
        }

        private void StudyGuildSkill(int skillId)
        {
            if (guild_rights_helper.CheckHasRights(GuildRightId.spell, 74666) == false) return;
            if (_skillList.Count == 0) return;
            GuildSkillData data = _skillList[_currentSkillIndex];
            if (data.Cost == null) return;
            foreach (KeyValuePair<string, string> kvp in data.Cost)
            {
                if (CheckIsItemEnough(int.Parse(kvp.Key), int.Parse(kvp.Value)) == false) return;
            }
            GuildManager.Instance.StudyGuildSkill(skillId);
        }

        private bool CheckIsItemEnough(int itemId,int value)
        {
            MyGuildData data = PlayerDataManager.Instance.GuildData.MyGuildData;
            if (item_helper.GetMyMoneyCountByItemId(itemId) < value)
            {
                string tip = string.Format(MogoLanguageUtil.GetContent(74607), MogoLanguageUtil.GetContent(74599));
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, tip, PanelIdEnum.MainUIField);
                return false;
            }
            return true;
        }

        protected override void OnEnable()
        {
            GuildManager.Instance.GetSkillList();
            AddEventListener();
            _currentSkillIndex = 0;
            RefreshContent();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

    }

    public class GuildSkillItem : KList.KListItemBase
    {
        private GuildSkillData _data;
        private StateText _nameText;
        private StateText _levelText;
        private StateIcon _iconContainer;
        private StateImage _selectImage;

        protected override void Awake()
        {
            _nameText = GetChildComponent<StateText>("Label_name");
            _levelText = GetChildComponent<StateText>("Label_dengji");
            _iconContainer = GetChildComponent<StateIcon>("Container_stateIcon");
            _selectImage = GetChildComponent<StateImage>("Image_xuanzhongkuang");
            _selectImage.Visible = false;
        }

        private void RefreshContent()
        {
            _nameText.CurrentText.text = _data.Name;
            if (_data.HasLearned == true)
            {
                _levelText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(74857), _data.Level);
            }
            else
            {
                _levelText.CurrentText.text = MogoLanguageUtil.GetContent(74852);
            }
            _iconContainer.SetIcon( _data.Icon);
        }

        public void SetSelect(bool select)
        {
            _selectImage.Visible = select;
        }

        public override void Dispose()
        {
            base.Data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null && value != base.Data)
                {
                    _data = value as GuildSkillData;
                    RefreshContent();
                }
            }
        }
    }
}
