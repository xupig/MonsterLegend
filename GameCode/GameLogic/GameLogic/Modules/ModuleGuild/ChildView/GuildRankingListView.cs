﻿using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace ModuleGuild
{
    public class GuildRankingListView : KContainer
    {
        private GuildCreateView _createView;
        private KButton _searchButton;
        private KButton _createButton;
        private KButton _backButton;
        private KButton _closeButton;
        private KDummyButton _hintButton;
        private KInputField _input;
        private KScrollView _scrollView;
        private KList _list;
        private StateText _noneTxt;
        private int _currentItemCount = 10;
        private PbRankGuildInfoList _guildList = new PbRankGuildInfoList();

        protected override void Awake()
        {
            _createView = AddChildComponent<GuildCreateView>("Container_tanchuang");
            _createView.Visible = false;
            _searchButton = GetChildComponent<KButton>("Container_paihangbang/Button_sousuo");
            _createButton = GetChildComponent<KButton>("Container_paihangbang/Button_chuangjiangonghui");
            _backButton = GetChildComponent<KButton>("Container_paihangbang/Button_fanhui");
            _closeButton = GetChildComponent<KButton>("Container_paihangbang/Button_close");
            _hintButton = AddChildComponent<KDummyButton>("Container_paihangbang/Container_panghangbangxianshi/Container_Shurukuang/Container_inputHint");
            _input = GetChildComponent<KInputField>("Container_paihangbang/Container_panghangbangxianshi/Container_Shurukuang/Input_input");
            _input.text = string.Empty;
            _scrollView = GetChildComponent<KScrollView>("Container_paihangbang/Container_panghangbangxianshi/Container_gonghuipaihang/ScrollView_zhanlibang");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _noneTxt = GetChildComponent<StateText>("Container_paihangbang/Container_panghangbangxianshi/Container_gonghuipaihang/Label_txtMeiyoushujv");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        protected void AddEventListener()
        {
            _searchButton.onClick.AddListener(OnSearchButtonClick);
            _createButton.onClick.AddListener(OnCreateButtonClick);
            _hintButton.onClick.AddListener(OnHintButtonClick);
            _backButton.onClick.AddListener(OnBackButtonClick);
            _scrollView.ScrollRect.onRequestNext.AddListener(OnRequestNext);
            EventDispatcher.AddEventListener<PbRankGuildInfoList>(GuildEvents.Show_Guild_List, RefreshListContent);
            EventDispatcher.AddEventListener<PbGuildSearchList>(GuildEvents.Show_Search_List, RefreshResearchContent);
        }

        protected void RemoveEventListener()
        {
            _searchButton.onClick.RemoveListener(OnSearchButtonClick);
            _createButton.onClick.RemoveListener(OnCreateButtonClick);
            _hintButton.onClick.RemoveListener(OnHintButtonClick);
            _backButton.onClick.RemoveListener(OnBackButtonClick);
            _scrollView.ScrollRect.onRequestNext.RemoveListener(OnRequestNext);
            EventDispatcher.RemoveEventListener<PbRankGuildInfoList>(GuildEvents.Show_Guild_List, RefreshListContent);
            EventDispatcher.RemoveEventListener<PbGuildSearchList>(GuildEvents.Show_Search_List, RefreshResearchContent);
        }

        private void OnRequestNext(KScrollRect target)
        {
            _currentItemCount += 10;
            RefreshListContent(_guildList);
        }

        private void OnBackButtonClick()
        {
            RefreshListContent(_guildList);
            _scrollView.ResetContentPosition();
        }

        private void OnHintButtonClick()
        {
            _hintButton.gameObject.SetActive(false);
            EventSystem.current.SetSelectedGameObject(_input.gameObject);
            _input.OnPointerClick(new PointerEventData(EventSystem.current));
        }

        private void OnCreateButtonClick()
        {
            _createView.Visible = true;
        }

        private List<PbRankGuildInfo> GetShowListData(List<PbRankGuildInfo> dataList)
        {
            List<PbRankGuildInfo> result = new List<PbRankGuildInfo>();
            for (int i = 0; i < _currentItemCount && i < dataList.Count; i++)
            {
                result.Add(dataList[i]);
            }
            return result;
        }

        private void RefreshListContent(PbRankGuildInfoList pbRankInfoList)
        {
            _closeButton.Visible = true;
            _backButton.Visible = false;
            _guildList = pbRankInfoList;
            List<PbRankGuildInfo> showList = GetShowListData(_guildList.rank_guild_info);
            _list.SetDataList<GuildRankingListItem>(showList);
            _noneTxt.Visible = showList.Count == 0;
        }

        private void RefreshResearchContent(PbGuildSearchList pbSearchList)
        {
            _closeButton.Visible = false;
            _backButton.Visible = true;
            _list.SetDataList<GuildRankingListItem>(pbSearchList.guild_search_info);
            _scrollView.ResetContentPosition();
        }

        private void OnSearchButtonClick()
        {
            if (_input.text == string.Empty)
            {
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74601), PanelIdEnum.MainUIField);
                return;
            }
            GuildManager.Instance.SearchGuild(_input.text);
        }

        protected override void OnEnable()
        {
            AddEventListener();
            _createView.Visible = false;
            RefreshListContent(_guildList);
            _scrollView.ResetContentPosition();
            GuildManager.Instance.GetGuildList();
            GuildManager.Instance.GetApplyJoinList();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

    }
}
