﻿#region 模块信息
/*==========================================
// 文件名：GuildShopPopPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuild.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/1/27 14:09:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuild
{
    public class GuildShopPopPanel:BasePanel
    {
        private StateText _txtName;
        private StateText _txtContent1;
        private StateText _txtContent2;
        private StateText _txtContent3;
        private StateText _txtBuy1;
        private StateText _txtBuy2;

        private KButton _btnBuy1;
        private KButton _btnBuy2;
        private ItemGrid _icon;
        private PbGuildShopData _shopData;
        private guild_shop _guildShop;
        private StateText _txtBuy1Tips;
        private StateText _txtBuy2Tips;
        private StateIcon _icon1;
        private StateIcon _icon2;
        private int buyNum;

       

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _txtName = GetChildComponent<StateText>("Container_biaoti/Label_txtWupinmingziqigezi");
            _txtContent1 = GetChildComponent<StateText>("Label_txtShoucangpaiming");
            _txtContent2 = GetChildComponent<StateText>("Label_txtYuliang");
            _txtContent3 = GetChildComponent<StateText>("Label_txtKebuhuoliang");
            _btnBuy1 = GetChildComponent<KButton>("Button_goumai");
            _txtBuy1 = _btnBuy1.GetChildComponent<StateText>("num");
            _icon1 = _btnBuy1.GetChildComponent<StateIcon>("stateIcon");
            _btnBuy2 = GetChildComponent<KButton>("Button_buhuo");
            _txtBuy2 = _btnBuy2.GetChildComponent<StateText>("num");
            _icon2 = _btnBuy2.GetChildComponent<StateIcon>("stateIcon");
            _icon = GetChildComponent<ItemGrid>("Container_wupin");
            _txtBuy1Tips = GetChildComponent<StateText>("Label_txtBuhuo1ge");
            _txtBuy2Tips = GetChildComponent<StateText>("Label_txtBuhuo10ge");
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildShopPop; }
        }

        public override void OnShow(object data)
        {
            _btnBuy1.onClick.AddListener(OnClickBuy1);
            _btnBuy2.onClick.AddListener(OnClickBuy2);
            EventDispatcher.AddEventListener<uint>(GuildShopEvents.Refresh_Item, OnRefreshItem);
            EventDispatcher.AddEventListener(GuildShopEvents.Show_List_View,OnShowListView);
            SetData(data);
        }

        public override void OnClose()
        {
            _btnBuy1.onClick.RemoveListener(OnClickBuy1);
            _btnBuy2.onClick.RemoveListener(OnClickBuy2);
            EventDispatcher.AddEventListener<uint>(GuildShopEvents.Refresh_Item, OnRefreshItem);
            EventDispatcher.RemoveEventListener(GuildShopEvents.Show_List_View, OnShowListView);
        }

        private void OnClickBuy1()
        {
            if(_shopData!=null && _guildShop!=null)
            {
                if(PlayerAvatar.Player.CheckCostLimit(_guildShop.__cost,true) == 0)
                {
                    GuildShopManager.Instance.BuyGuildShopItem(Convert.ToInt32(_shopData.grid_id), 1, _guildShop.__item_id);
                }
            }
        }

        private void OnClickBuy2()
        {
            if (_shopData != null && _guildShop != null && buyNum > 0)
            {
                int moneyType = 0;
                int moneyCount = 0;
                foreach (KeyValuePair<string, string> kvp in _guildShop.__cost)
                {
                    moneyType = int.Parse(kvp.Key);
                    moneyCount = int.Parse(kvp.Value);
                }
                string content = MogoLanguageUtil.GetContent(74994, moneyCount * buyNum, item_helper.GetName(moneyType), buyNum, item_helper.GetName(_guildShop.__item_id));
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, ConfirmBuy2);
            }
        }

        private void ConfirmBuy2()
        {
            if (PlayerAvatar.Player.CheckCostLimit(_guildShop.__cost, buyNum, true) == 0)
            {
                GuildShopManager.Instance.BuyGuildShopItem(Convert.ToInt32(_shopData.grid_id), buyNum, _guildShop.__item_id);
            }
        }

        private void OnRefreshItem(uint id)
        {
            if (_shopData != null)
            {
                uint grid_id = _shopData.grid_id;
                _shopData = PlayerDataManager.Instance.GuildShopData.GetGuildShopData(grid_id);
                SetData(_shopData);
            }
        }

        private void OnShowListView()
        {
            if(_shopData!=null)
            {
                uint grid_id = _shopData.grid_id;
                _shopData = PlayerDataManager.Instance.GuildShopData.GetGuildShopData(grid_id);
                SetData(_shopData);
            }
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            _shopData = data as PbGuildShopData;
            _guildShop = guild_shop_helper.GetGuildShop(Convert.ToInt32(_shopData.grid_id));
            _txtName.CurrentText.text = item_helper.GetName(_guildShop.__item_id);
            _txtContent1.CurrentText.text = (74968).ToLanguage(PlayerDataManager.Instance.GuildShopData.GetGuildShopItemRank(_shopData.grid_id),_shopData.praise_count);
            _txtContent2.CurrentText.text = (74969).ToLanguage(PlayerDataManager.Instance.GuildShopData.GetGuildShopStoreCount(_shopData.grid_id));
            _txtContent3.CurrentText.text = (74970).ToLanguage(_shopData.remain_count);
            _txtBuy1Tips.CurrentText.text = (74967).ToLanguage(1);
            int costId = 0;
            int num = 0;
            foreach(KeyValuePair<string,string> kvp in _guildShop.__cost)
            {
                costId = int.Parse(kvp.Key);
                num = int.Parse(kvp.Value);
            }
            _txtBuy1.ChangeAllStateText(string.Concat("x",num.ToString()));
            _icon1.SetIcon(item_helper.GetIcon(costId));
            int total = item_helper.GetMyMoneyCountByItemId(costId);
            buyNum = Math.Min(total / num, Convert.ToInt32(_shopData.remain_count));
            if(buyNum == 0)
            {
                buyNum = 1;
            }
            _txtBuy2Tips.CurrentText.text = (74967).ToLanguage(buyNum);
            _txtBuy2.ChangeAllStateText(string.Concat("x", (buyNum * num).ToString()));
            _icon2.SetIcon(item_helper.GetIcon(costId));
            _icon.SetItemData(_guildShop.__item_id);
        }

    }
}
