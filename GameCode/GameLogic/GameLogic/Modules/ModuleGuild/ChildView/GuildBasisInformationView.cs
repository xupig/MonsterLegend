﻿using Common.Base;
using Common.Chat;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ModuleGuild
{
    public class GuildBasisInformationView : KContainer
    {
        private KButton _recruitButton;
        private KButton _chatButton;
        private KButton _announcementButton;
        private KButton _escapeButton;
        private KButton _infoButton;
        private KButton _fundInfoButton;
        private MoneyItem _fundItem;
        private MoneyItem _contributeItem;
        private StateText _numberText;
        private StateText _leaderText;
        private StateText _rankText;
        private StateText _moneyText;
        private StateText _timeText;
        private StateText _costText;
        private StateText _memberText;
        private StateText _nameText;
        private StateText _levelText;
        private StateText _honorText;
        private StateText _freezeDescTxt;
        private KButton _editButton;
        private StateText _purposeText;
        private KInputField _purposeInput;
        private IconContainer _iconContainer;
        private KButton _rankButton;
        private IconContainer _costIconContainer;
        private string countContent;
        private StateImage _newNoticePoint;

        protected override void Awake()
        {
            _recruitButton = GetChildComponent<KButton>("Container_neirong/Button_zhaomu");
            _chatButton = GetChildComponent<KButton>("Container_neirong/Button_liaotian");
            _announcementButton = GetChildComponent<KButton>("Container_neirong/Button_gonggao");
            _newNoticePoint = GetChildComponent<StateImage>("Container_neirong/Button_gonggao/xiaodian");
            _escapeButton = GetChildComponent<KButton>("Container_neirong/Button_tuoligonghui");
            _fundItem = AddChildComponent<MoneyItem>("Container_neirong/Container_buyDiamond");
            _contributeItem = AddChildComponent<MoneyItem>("Container_neirong/Container_buyGold");
            _fundItem.SetMoneyType(public_config.MONEY_TYPE_GUILD_FUND);
            _contributeItem.SetMoneyType(public_config.MONEY_TYPE_GUILD_CONTRIBUTION);
            _numberText = GetChildComponent<StateText>("Container_neirong/Container_left/Label_txtGonghuibianhao");
            _leaderText = GetChildComponent<StateText>("Container_neirong/Container_left/Label_txtHuizhangmingzi");
            _rankText = GetChildComponent<StateText>("Container_neirong/Container_left/Label_txtpaiming");
            _moneyText = GetChildComponent<StateText>("Container_neirong/Container_left/Label_txtZijin");
            _timeText = GetChildComponent<StateText>("Container_neirong/Container_left/Label_txtShijian");
            _costText = GetChildComponent<StateText>("Container_neirong/Container_left/Label_txtFeiyong");
            _costIconContainer = AddChildComponent<IconContainer>("Container_neirong/Container_left/Container_costIcon");
            _memberText = GetChildComponent<StateText>("Container_neirong/Container_left/Label_txtChengyuan");
            _fundInfoButton = GetChildComponent<KButton>("Container_neirong/Container_left/Button_Gantang");
            _nameText = GetChildComponent<StateText>("Container_neirong/Container_rightup/Label_txtGonghuimingzi");
            _levelText = GetChildComponent<StateText>("Container_neirong/Container_rightup/Label_txtgonghuiDengji");
            _honorText = GetChildComponent<StateText>("Container_neirong/Container_rightup/Label_txtgonghuiFanrongdu");
            _freezeDescTxt = GetChildComponent<StateText>("Container_neirong/Container_rightup/Label_txtDongjieqi");
            _infoButton = GetChildComponent<KButton>("Container_neirong/Container_rightup/Button_Gantang");
            _editButton = GetChildComponent<KButton>("Container_neirong/Container_rightdown/Button_shuxie");
            _purposeText = GetChildComponent<StateText>("Container_neirong/Container_rightdown/Label_txtzongzhi");
            _purposeInput = GetChildComponent<KInputField>("Container_neirong/Container_rightdown/Input_input");
            _iconContainer = AddChildComponent<IconContainer>("Container_neirong/Container_rightup/Container_icon");
            _rankButton = GetChildComponent<KButton>("Container_neirong/Container_left/Button_gonghuiqpaihang");
            _purposeInput.text = string.Empty;
            _purposeInput.lineType = InputField.LineType.MultiLineSubmit;
            _purposeInput.gameObject.SetActive(false);
            countContent = _moneyText.CurrentText.text;
        }

        private void RefreshContent()
        {
            MyGuildData data = PlayerDataManager.Instance.GuildData.MyGuildData;
            _numberText.CurrentText.text = data.Id.ToString();
            _leaderText.CurrentText.text = data.LeaderName;
            _rankText.CurrentText.text = data.Rank.ToString();
            _timeText.CurrentText.text = MogoTimeUtil.ToLocalTime(data.Time, "yyyy-MM-dd");
            _moneyText.CurrentText.text = string.Format(countContent, data.Money, guild_level_helper.GetFundLimit(data.Level));
            _costText.CurrentText.text = (guild_level_helper.GetDayCostValue(data.Level) - PlayerDataManager.Instance.GuildData.EffectData.MaintainFundReduce).ToString();
            _costIconContainer.SetIcon(item_helper.GetIcon(guild_level_helper.GetDayCostCurrency(data.Level)));
            _memberText.CurrentText.text = string.Format(countContent, data.MemberCount,guild_level_helper.GetMemberCount(data.Level));
            _nameText.CurrentText.text = data.GuildName;
            _levelText.CurrentText.text = data.Level.ToString();
            _purposeText.CurrentText.text = data.Manifesto;
            _iconContainer.SetIcon(data.BadgeIconId);
            RefreshManifesto();
            RefreshBoomingView();
        }

        protected void AddEventListener()
        {
            _recruitButton.onClick.AddListener(OnRecuitButtonClick);
            _chatButton.onClick.AddListener(OnChatButtonClick);
            _announcementButton.onClick.AddListener(OnAnnouncementButtonClick);
            _escapeButton.onClick.AddListener(OnEscapeButtonClick);
            _editButton.onClick.AddListener(OnEditButtonClick);
            _purposeInput.onEndEdit.AddListener(OnEndEdit);
            _rankButton.onClick.AddListener(OnRankButtonClick);
            _infoButton.onClick.AddListener(OnInfoButtonClick);
            _fundInfoButton.onClick.AddListener(OnFundInfoButtonClick);
            EventDispatcher.AddEventListener(GuildEvents.Show_My_Guild_Info, RefreshContent);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Manifesto, RefreshManifesto);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_Money, RefreshMoneyView);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_Contribute, RefreshMoneyView);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_Booming, RefreshBoomingView);
            EventDispatcher.AddEventListener(GuildEvents.On_Guild_Recuit_Succeed, OnGuildRecuitSucceed);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_Point, RefreshGreenPoint);
        }

        protected void RemoveEventListener()
        {
            _recruitButton.onClick.RemoveListener(OnRecuitButtonClick);
            _chatButton.onClick.RemoveListener(OnChatButtonClick);
            _announcementButton.onClick.RemoveListener(OnAnnouncementButtonClick);
            _escapeButton.onClick.RemoveListener(OnEscapeButtonClick);
            _editButton.onClick.RemoveListener(OnEditButtonClick);
            _purposeInput.onEndEdit.RemoveListener(OnEndEdit);
            _rankButton.onClick.RemoveListener(OnRankButtonClick);
            _infoButton.onClick.RemoveListener(OnInfoButtonClick);
            _fundInfoButton.onClick.RemoveListener(OnFundInfoButtonClick);
            EventDispatcher.RemoveEventListener(GuildEvents.Show_My_Guild_Info, RefreshContent);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Manifesto, RefreshManifesto);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_Money, RefreshMoneyView);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_Contribute, RefreshMoneyView);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_Booming, RefreshBoomingView);
            EventDispatcher.RemoveEventListener(GuildEvents.On_Guild_Recuit_Succeed, OnGuildRecuitSucceed);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_Point, RefreshGreenPoint);
        }

        private void RefreshGreenPoint()
        {
            _newNoticePoint.Visible = PlayerDataManager.Instance.GuildData.HasNewNotice;
        }

        private void OnInfoButtonClick()
        {
            string result = string.Empty;
            for (int i = 0; i < 4; i++)
            {
                result = string.Concat(result, MogoLanguageUtil.GetContent(74866 + i), '\n');
            }
            UIManager.Instance.ShowPanel(PanelIdEnum.ItemStory, new ItemStoryData(MogoLanguageUtil.GetContent(77),result));
        }

        private void OnFundInfoButtonClick()
        {
            string result = string.Empty;
            result = string.Concat(result, MogoLanguageUtil.GetContent(74899));
            UIManager.Instance.ShowPanel(PanelIdEnum.ItemStory, new ItemStoryData(MogoLanguageUtil.GetContent(77), result));
        }

        private void OnEndEdit(string content)
        {
            if (content == string.Empty)
            {
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74853), PanelIdEnum.MainUIField);
                return;
            }
            GuildManager.Instance.SetManifesto(content);
        }

        private void RefreshManifesto()
        {
            _isEditing = false;
            _purposeInput.gameObject.SetActive(false);
            MyGuildData data = PlayerDataManager.Instance.GuildData.MyGuildData;
            _purposeText.CurrentText.text = data.Manifesto;
            _purposeText.gameObject.SetActive(true);
        }

        private bool _isEditing = false;
        private void OnEditButtonClick()
        {
            if (guild_rights_helper.CheckHasRights(GuildRightId.manifesto, 74854) == false) return;
            if (_isEditing) return;
            _isEditing = true;
            _purposeText.gameObject.SetActive(false);
            _purposeInput.text = _purposeText.CurrentText.text;
            _purposeInput.gameObject.SetActive(true);
            EventSystem.current.SetSelectedGameObject(_purposeInput.gameObject);
            _purposeInput.OnPointerClick(new PointerEventData(EventSystem.current));
        }

        private void RefreshPurposeView()
        {
            _purposeInput.gameObject.SetActive(false);
            _purposeText.CurrentText.text = _purposeInput.text;
            _purposeText.gameObject.SetActive(true);
        }

        private void OnRankButtonClick()
        {
            PanelIdEnum.RankingList.Show(2);
        }

        private void OnEscapeButtonClick()
        {
            if (PlayerDataManager.Instance.GuildData.GetMyPosition() == 0) return;
            if (PlayerDataManager.Instance.GuildData.GuildMemberInfoList.member_info.Count > 1 && PlayerDataManager.Instance.GuildData.GetMyPosition() == public_config.GUILD_POSITION_LEADER)
            {
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(74633));
                return;
            }
            int hour = int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.guild_apply_join_cd_i))/60;
            MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(74634, hour), delegate() { GuildManager.Instance.LeaveGuild(); });
        }

        private void OnAnnouncementButtonClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildPop, 2);
        }

        private void OnChatButtonClick()
        {
            PanelIdEnum.Chat.Show(public_config.CHANNEL_ID_GUILD);
        }

        private void OnRecuitButtonClick()
        {
            if (guild_rights_helper.CheckHasRights(GuildRightId.recruit, 74623) == false) return;
            MyGuildData data = PlayerDataManager.Instance.GuildData.MyGuildData;
            if (data.MemberCount >= guild_level_helper.GetMemberCount(data.Level))
            {
                if (data.Level < guild_level_helper.GetMaxLevel())
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74631));
                }
                else
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74876));
                }
                return;
            }
            string tip = MogoLanguageUtil.GetContent(74644, data.GuildName, data.Level);
            ChatManager.Instance.RequestInviteLinkSendChat(public_config.CHANNEL_ID_WORLD, new ChatGuildLinkWrapper(data.Id, tip));
        }

        private void OnGuildRecuitSucceed()
        {
            ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74646), PanelIdEnum.MainUIField);
        }

        private void RefreshMoneyView()
        {
            MyGuildData data = PlayerDataManager.Instance.GuildData.MyGuildData;
            _moneyText.CurrentText.text = string.Format(countContent, data.Money, guild_level_helper.GetFundLimit(data.Level));
        }

        private void RefreshBoomingView()
        {
            MyGuildData data = PlayerDataManager.Instance.GuildData.MyGuildData;
            _honorText.CurrentText.text = data.Booming.ToString();
            uint daySecond = 24 * 60 * 60;
            uint hourSecond = 60 * 60;
            if (data.Booming == 0)
            {
                uint leftTime = data.DeleteTime > Global.serverTimeStampSecond ? data.DeleteTime - Global.serverTimeStampSecond : 0;
                if (leftTime < daySecond)
                {
                    _freezeDescTxt.CurrentText.text = MogoLanguageUtil.GetContent(74951, leftTime / hourSecond);
                }
                else
                {
                    _freezeDescTxt.CurrentText.text = MogoLanguageUtil.GetContent(74658, leftTime / daySecond);
                }
            }
            else
            {
                _freezeDescTxt.CurrentText.text = string.Empty;
            }
        }

        protected override void OnEnable()
        {
            GuildManager.Instance.GetGuildInformation();
            AddEventListener();
            RefreshMoneyView();
            RefreshBoomingView();
            RefreshContent();
            RefreshGreenPoint();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

    }
}
