﻿using Common.Base;
using Common.ClientConfig;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Mgrs;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Game.UI;
using Common.Data;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager;
using Common.Global;
using System;

namespace ModuleGuild
{
    public class GuildCreateView : KContainer
    {
        private KButton _closeButton;
        //配置了3种创建方式
        private KButton _createBtn1;
        private KButton _createBtn2;
        private KButton _createBtn3;
        private KDummyButton _nameButton;
        private KInputField _nameInput;
        private KDummyButton _declarationButton;
        private KInputField _declarationInput;
        private KToggleGroup _badgeToggleGroup;
        private StateText _needText;
        private MoneyItem _moneyItem1;
        private MoneyItem _moneyItem2;
        private MoneyItem _moneyItem3;
        private StateText _filterNameText;
        private StateText _filterDeclarationText;
        private int _needLevel;
        private List<int> _costList;
        private int _nameLimit;

        private List<int> _iconList = new List<int>();

        private int _badgeId;
        private string _name;
        private string _declaration;

        protected override void Awake()
        {
            _closeButton = GetChildComponent<KButton>("Container_jianglibaoxiangBG/Button_close");
            _createBtn1 = GetChildComponent<KButton>("Button_chuangjian1");
            _createBtn2 = GetChildComponent<KButton>("Button_chuangjian2");
            _createBtn3 = GetChildComponent<KButton>("Button_chuangjian3");
            _nameButton = AddChildComponent<KDummyButton>("Container_gonghuimingcheng/Container_inputHint");
            _nameInput = GetChildComponent<KInputField>("Container_gonghuimingcheng/Input_validateInput");
            _declarationButton = AddChildComponent<KDummyButton>("Container_gonghuixuanyan/Container_inputHint");
            _declarationInput = GetChildComponent<KInputField>("Container_gonghuixuanyan/Input_validateInput");
            _moneyItem1 = AddChildComponent<MoneyItem>("Container_currency/Container_buyGold");
            _moneyItem2 = AddChildComponent<MoneyItem>("Container_currency/Container_buyTicket");
            _moneyItem3 = AddChildComponent<MoneyItem>("Container_currency/Container_buyDiamond");
            _needText = GetChildComponent<StateText>("Label_txtxvyaodengji");
            _badgeToggleGroup = GetChildComponent<KToggleGroup>("Container_huizhang/ToggleGroup_huizhang");
            InitGlobalParams();
            InitView();
        }

        private void InitView()
        {
            _nameInput.text = string.Empty;
            _declarationInput.text = string.Empty;
            _declarationInput.lineType = InputField.LineType.MultiLineNewline;
            _moneyItem1.SetMoneyType(_costList[0]);
            _moneyItem2.SetMoneyType(_costList[2]);
            _moneyItem3.SetMoneyType(_costList[4]);
            _createBtn1.Label.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
            _createBtn2.Label.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
            _createBtn3.Label.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
            _createBtn1.Label.ChangeAllStateText(GetCreateBtnContent(_costList[0], _costList[1]));
            _createBtn2.Label.ChangeAllStateText(GetCreateBtnContent(_costList[2], _costList[3]));
            _createBtn3.Label.ChangeAllStateText(GetCreateBtnContent(_costList[4], _costList[5]));
            _needText.CurrentText.text = "LV" + _needLevel;
            List<KToggle> toggleList = _badgeToggleGroup.GetToggleList();
            for (int i = 0; i < toggleList.Count; i++)
            {
                StateIcon icon = toggleList[i].GetChildComponent<StateIcon>("gonghuiicon/stateIcon");
                icon.SetIcon(_iconList[i]);
            }
            StateText nameText = _nameInput.transform.FindChild("Label_txtGonghuimingcheng").GetComponent<StateText>();
            _filterNameText = CloneInputText(nameText);
            StateText declarationText = _declarationInput.transform.FindChild("Label_txtGonghuixuanyanneirong").GetComponent<StateText>();
            _filterDeclarationText = CloneInputText(declarationText);
        }

        private string GetCreateBtnContent(int itemId, int num)
        {
            if (num >= 10000000)
            {
                return MogoLanguageUtil.GetContent(124508, string.Concat(num / 10000000, MogoLanguageUtil.GetContent(124511)), item_helper.GetName(itemId));
            }
            else if (num >= 1000000)
            {
                return MogoLanguageUtil.GetContent(124508, string.Concat(num / 1000000, MogoLanguageUtil.GetContent(124510)), item_helper.GetName(itemId));
            }
            else if (num >= 10000)
            {
                return MogoLanguageUtil.GetContent(124508, string.Concat(num / 10000, MogoLanguageUtil.GetContent(124509)), item_helper.GetName(itemId));
            }
            return MogoLanguageUtil.GetContent(124508, num, item_helper.GetName(itemId));
        }

        private void InitGlobalParams()
        {
            _needLevel = int.Parse(global_params_helper.GetGlobalParam(82));
            _nameLimit = int.Parse(global_params_helper.GetGlobalParam(84));
            _iconList = MogoStringUtils.Convert2List(global_params_helper.GetGlobalParam(87));
            _costList = new List<int>();
            string value = global_params_helper.GetGlobalParam(83);
            string[] golds = value.Split(',');
            for (int i = 0; i < golds.Length; i++)
            {
                string[] items = golds[i].Split(':');
                _costList.Add(int.Parse(items[0]));
                _costList.Add(int.Parse(items[1]));
            }
        }

        private StateText CloneInputText(StateText srcText)
        {
            GameObject textGo = GameObject.Instantiate(srcText.gameObject) as GameObject;
            textGo.transform.SetParent(srcText.transform.parent);
            textGo.transform.localScale = Vector3.one;
            textGo.transform.localPosition = Vector3.zero;
            textGo.name = srcText.gameObject.name + "Filter";
            RectTransform newTextRect = textGo.GetComponent<RectTransform>();
            RectTransform srcTextRect = srcText.gameObject.GetComponent<RectTransform>();
            newTextRect.anchoredPosition = srcTextRect.anchoredPosition;
            textGo.SetActive(true);
            StateText stateText = textGo.GetComponent<StateText>();
            TextWrapper textWrapper = stateText.CurrentText as TextWrapper;
            textWrapper.supportRichText = true;
            Color textColor = textWrapper.color;
            textColor.a = 0f;
            textWrapper.color = textColor;
            Destroy(textGo.GetComponent<Shadow>());
            return stateText;
        }

        protected void AddEventListener()
        {
            _closeButton.onClick.AddListener(OnCloseButtonClick);
            _createBtn1.onClick.AddListener(OnCreate1ButtonClick);
            _createBtn2.onClick.AddListener(OnCreate2ButtonClick);
            _createBtn3.onClick.AddListener(OnCreate3ButtonClick);
            _nameButton.onClick.AddListener(OnNameButtonClick);
            _declarationButton.onClick.AddListener(OnDeclarationButton);
            _nameInput.onValueChange.AddListener(OnNameInputValueChange);
            _declarationInput.onValueChange.AddListener(OnDeclarationInputValueChange);
        }

        protected void RemoveEventListener()
        {
            _closeButton.onClick.RemoveListener(OnCloseButtonClick);
            _createBtn1.onClick.RemoveListener(OnCreate1ButtonClick);
            _createBtn2.onClick.RemoveListener(OnCreate2ButtonClick);
            _createBtn3.onClick.RemoveListener(OnCreate3ButtonClick);
            _nameButton.onClick.RemoveListener(OnNameButtonClick);
            _declarationButton.onClick.RemoveListener(OnDeclarationButton);
            _nameInput.onValueChange.RemoveListener(OnNameInputValueChange);
            _declarationInput.onValueChange.RemoveListener(OnDeclarationInputValueChange);
        }

        private void OnCreate1ButtonClick()
        {
            CreateGuild(_costList[0]);
        }

        private void OnCreate2ButtonClick()
        {
            CreateGuild(_costList[2]);
        }

        private void OnCreate3ButtonClick()
        {
            CreateGuild(_costList[4]);
        }

        private void OnDeclarationButton()
        {
            _declarationButton.gameObject.SetActive(false);
            EventSystem.current.SetSelectedGameObject(_declarationInput.gameObject);
            _declarationInput.OnPointerClick(new PointerEventData(EventSystem.current));
        }

        private void OnNameButtonClick()
        {
            _nameButton.gameObject.SetActive(false);
            EventSystem.current.SetSelectedGameObject(_nameInput.gameObject);
            _nameInput.OnPointerClick(new PointerEventData(EventSystem.current));
            
        }

        private void CreateGuild(int itemId)
        {
            _badgeId = _badgeToggleGroup.SelectIndex;
            _name = _nameInput.text;
            _declaration = _declarationInput.text;
            if (CanCreate() == false) return;
            GuildManager.Instance.CreateGuild(_name, _declaration, _badgeId, itemId);
            _nameInput.text = string.Empty;
            _declarationInput.text = string.Empty;
        }

        private void OnNameInputValueChange(string text)
        {
            uint filterWordsRange = (uint)FilterWordsRangeType.ShieldWords | (uint)FilterWordsRangeType.RegexWords | (uint)FilterWordsRangeType.SpecialWords;
            string filterText = FilterWordsData.Instance.FilterContent(text, FilterWordsType.ChangedWithRedColor, filterWordsRange);
            _filterNameText.CurrentText.text = filterText;
        }

        private void OnDeclarationInputValueChange(string text)
        {
            uint filterWordsRange = (uint)FilterWordsRangeType.ShieldWords | (uint)FilterWordsRangeType.RegexWords | (uint)FilterWordsRangeType.SpecialWords;
            string filterText = FilterWordsData.Instance.FilterContent(text, FilterWordsType.ChangedWithRedColor, filterWordsRange);
            _filterDeclarationText.CurrentText.text = filterText;
        }

        private bool CanCreate()
        {
            string name = _nameInput.text;
            string declaration = _declarationInput.text;
            if (PlayerAvatar.Player.level < _needLevel)
            {
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74605, _needLevel), PanelIdEnum.MainUIField);
                return false;
            }
            if (name == string.Empty)
            {
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74608), PanelIdEnum.MainUIField);
                return false;
            }
            if (declaration == string.Empty)
            {
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74853), PanelIdEnum.MainUIField);
                return false;
            }
            return true;
        }

        private void OnCloseButtonClick()
        {
            this.Visible = false;
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

    }
}
