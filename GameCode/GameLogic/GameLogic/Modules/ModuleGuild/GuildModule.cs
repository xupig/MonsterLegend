﻿using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using UnityEngine;

namespace ModuleGuild
{
    public class GuildModule : BaseModule
    {

        public GuildModule()
            : base()
        {
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            RegisterPopPanelBunchPanel(PanelIdEnum.GuildPop, PanelIdEnum.GuildInformation);
            //RegisterPopPanelBunchPanel(PanelIdEnum.GuildWelfare, PanelIdEnum.GuildInformation);
            RegisterPopPanelBunchPanel(PanelIdEnum.GuildShopPop, PanelIdEnum.GuildInformation);
            AddPanel(PanelIdEnum.GuildInformation, "Container_GuildInformationPanel", MogoUILayer.LayerUIPanel, "ModuleGuild.GuildInformationPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.GuildCreatePanel, "Container_GuildCreatePanel", MogoUILayer.LayerUIPanel, "ModuleGuild.GuildCreatePanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.GuildPop, "Container_GuildPopPanel", MogoUILayer.LayerUIPopPanel, "ModuleGuild.GuildPopPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.GuildInstance, "Container_GuildInstancePanel", MogoUILayer.LayerUIPanel, "ModuleGuild.GuildInstancePanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.GuildInvite, "Container_GuildInvitePanel", MogoUILayer.LayerUIToolTip, "ModuleGuild.GuildInvitePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            //AddPanel(PanelIdEnum.GuildWelfare, "Container_TokenPanel", MogoUILayer.LayerUIPopPanel, "ModuleGuild.GuildWelfarePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.GuildShopPop, "Container_GuildShopPopPanel", MogoUILayer.LayerUIPopPanel, "ModuleGuild.GuildShopPopPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.GuildBoss, "Container_GuildWarBoss", MogoUILayer.LayerUIPanel, "ModuleGuild.GuildBossPanel", BlurUnderlay.Have, HideMainUI.Hide);
            EventDispatcher.AddEventListener(GuildEvents.OPEN_GUILD, OnOpenGuild);
        }

        private void OnOpenGuild()
        {
            if (PlayerAvatar.Player.guild_id == 0)
            {
                PanelIdEnum.GuildCreatePanel.Show();
            }
            else
            {
                PanelIdEnum.GuildInformation.Show();
            }
        }

    }
}
