﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuild
{
    public class GuildBossPanel : BasePanel
    {
        private StateText _tipTxt;
        private KButton _gotoBtn;
        private List<RewardContainer> _rewardList;
        private KContainer _rewardContainer;

        private int _instanceId;
        private int _activityId;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _tipTxt = GetChildComponent<StateText>("Container_ditu/Label_txtJieshao");
            _gotoBtn = GetChildComponent<KButton>("Container_ditu/Button_tiaozhanPutong");
            _rewardList = new List<RewardContainer>();
            _rewardList.Add(AddChildComponent<RewardContainer>("Container_ditu/Container_jiangli/Container_jiangli/Container_item01"));
            _rewardList.Add(AddChildComponent<RewardContainer>("Container_ditu/Container_jiangli/Container_jiangli/Container_item02"));
            _rewardList.Add(AddChildComponent<RewardContainer>("Container_ditu/Container_jiangli/Container_jiangli/Container_item03"));
            _rewardContainer = GetChildComponent<KContainer>("Container_ditu/Container_jiangli/Container_jiangli");
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            if (data is int[])
            {
                int[] list = data as int[];
                if (list.Length == 2)
                {
                    _instanceId = list[0];
                    _activityId = list[1];
                    RefreshContent();
                }
            }
        }

        private void RefreshContent()
        {
            LimitActivityData activityData = activity_helper.GetActivity(_activityId);
            _tipTxt.CurrentText.text = MogoLanguageUtil.GetContent(activityData.openData.__desc);
            List<RewardData> rewardDataList = new List<RewardData>();
            List<string> itemIdList = activityData.openData.__fid_arg;
            for (int i = 0; i < itemIdList.Count; i++)
            {
                rewardDataList.Add(RewardData.GetRewardData(int.Parse(itemIdList[i]), 1));
            }
            for (int i = 0; i < 3; i++)
            {
                if (rewardDataList.Count > i)
                {
                    _rewardList[i].Visible = true;
                    _rewardList[i].SetData(rewardDataList[i]);
                }
                else
                {
                    _rewardList[i].Visible = false;
                }
            }
            _rewardContainer.RecalculateSize();
            RecalculatePosition();
        }

        private void RecalculatePosition()
        {
            RectTransform rect = _rewardContainer.transform as RectTransform;
            RectTransform parentRect = _rewardContainer.transform.parent as RectTransform;
            rect.anchoredPosition = new Vector2(parentRect.sizeDelta.x / 2 - rect.sizeDelta.x / 2, rect.anchoredPosition.y);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildBoss; }
        }

        public void AddEventListener()
        {
            _gotoBtn.onClick.AddListener(OnGotoBtnClick);
        }

        public void RemoveEventListener()
        {
            _gotoBtn.onClick.RemoveListener(OnGotoBtnClick);
        }

        private void OnGotoBtnClick()
        {
            if (activity_helper.IsOpen(_activityId) == false)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.ACTIVITY_NOT_OPEN));
                return;
            }
            CopyLogicManager.Instance.RequestEnterCopy(ChapterType.GuildBoss, () =>
                {
                    GuildManager.Instance.GotoGuildBossScene();
                }, _instanceId);
        }

    }
}
