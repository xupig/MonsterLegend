﻿using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuild
{
    public class GuildInvitePanel:BasePanel
    {
        private KScrollView _scrollView;
        private KPageableList _inviteList;

        private StateText _txtNoneInviteLabel;

        protected override void Awake()
        {
            base.Awake();
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_yaoqingliebiao");
            _inviteList = GetChildComponent<KPageableList>("ScrollView_yaoqingliebiao/mask/content");
            _txtNoneInviteLabel = GetChildComponent<StateText>("Label_wuyaoqingxinxi");

            _inviteList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        protected override PanelIdEnum Id
        {
            get 
            { 
                return PanelIdEnum.GuildInvite; 
            }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            RefreshInviteList();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_Invite, RefreshInviteList);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_Invite, RefreshInviteList);
        }

        private void RefreshInviteList()
        {
            List<PbGuildInviteJoin> inviteJoinInfoList = PlayerDataManager.Instance.GuildData.InviteJoinInfoList;
            _inviteList.SetDataList<GuildInviteItem>(inviteJoinInfoList, 4);

            _txtNoneInviteLabel.Visible = inviteJoinInfoList.Count == 0;
        }
    }
}
