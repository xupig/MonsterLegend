﻿#region 模块信息
/*==========================================
// 文件名：GuildShopItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuild.ChildCompontent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/1/26 17:54:36
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuild
{
    public class GuildShopItem:KList.KListItemBase
    {
        private PbGuildShopData _data;
        private StateText _txtName;
        private KButton _btnBuhuo;
        private KButton _btnBuy;
        private StateText _txtQuehuo;
        private StateText _txtWeijiesuo;
        private StateText _txtTips;
        private StateText _txtCount;
        private KButton _btnAdd;
        private IconContainer _icon;
        private Star _star;
        private guild_shop _guildShop;
        private KDummyButton _btnStar;
        private StateText _txtBuy;
        private StateIcon _icon1;
        private StateImage _color;
        private KDummyButton _iconBtn;

        protected override void Awake()
        {
            _color = GetChildComponent<StateImage>("Image_shangchengpinzhikuang02");
            MogoShaderUtils.ChangeShader(_color.CurrentImage, MogoShaderUtils.IconShaderPath);
            _txtName = GetChildComponent<StateText>("Label_txtName");
            _btnBuhuo = GetChildComponent<KButton>("Button_buhuo");
            _btnBuhuo.GetChildComponent<StateText>("label").ChangeAllStateText((74962).ToLanguage());
            _btnBuy = GetChildComponent<KButton>("Button_goumai");
            _txtBuy = _btnBuy.GetChildComponent<StateText>("num");
            _icon1 = _btnBuy.GetChildComponent<StateIcon>("stateIcon");
            _txtQuehuo = GetChildComponent<StateText>("Label_txtQuehuo");
            _txtQuehuo.CurrentText.text = (74964).ToLanguage();//缺货
            _txtWeijiesuo = GetChildComponent<StateText>("Label_txtWweijiesuo");
            _txtWeijiesuo.CurrentText.text = (74965).ToLanguage();
            _star = AddChildComponent<Star>("Container_xin0");
            _star.SetBrightStarName("Image_xinxin2");
            _txtTips = GetChildComponent<StateText>("Label_txtTishi");
            _txtTips.Raycast = false;
            _txtCount = GetChildComponent<StateText>("Label_txtShengyu");
            _btnAdd = GetChildComponent<KButton>("Button_tianjia");
            _icon = AddChildComponent<IconContainer>("Container_icon");
            _star.GetChildComponent<StateImage>("ScaleImage_sharedZhezhao").Raycast = true;
            _btnStar = _star.gameObject.AddComponent<KDummyButton>();
            _iconBtn = _icon.gameObject.AddComponent<KDummyButton>();
            _iconBtn.onClick.AddListener(OnIConClickHandler);
        }

        private void OnIConClickHandler()
        {
            if (_guildShop != null && _guildShop.__item_id != 0)
            {
                int itemId = _guildShop.__item_id;
                ToolTipsManager.Instance.ShowItemTip(itemId, PanelIdEnum.Empty);
            }
        }

        public override object Data
        {
            get
            {
                return _guildShop;
            }
            set
            {
                _guildShop = value as guild_shop;
                Refresh();
            }
        }

        public override void Show()
        {
            AddEventListener();
            base.Show();
        }

        public override void Hide()
        {
            RemoveEventListener();
            base.Hide();
        }

        private void AddEventListener()
        {
            _btnAdd.onClick.AddListener(OnClickAdd);
            _btnBuhuo.onClick.AddListener(OnClickBuhuo);
            _btnBuy.onClick.AddListener(OnClickBuy);
            _btnStar.onClick.AddListener(OnClickStar);
            EventDispatcher.AddEventListener<uint>(GuildShopEvents.Refresh_Item, OnRefreshItem);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_Level, OnRefreshLevel);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Guild_Member_Count, OnRefreshLevel);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Skill_View, OnRefreshLevel);
            EventDispatcher.AddEventListener<bool, PbGuildPraiseData>(GuildShopEvents.Refresh_Praise_Count, OnRefreshPraiseCount);
        }

        private void RemoveEventListener()
        {
            _btnAdd.onClick.RemoveListener(OnClickAdd);
            _btnBuhuo.onClick.RemoveListener(OnClickBuhuo);
            _btnBuy.onClick.RemoveListener(OnClickBuy);
            _btnStar.onClick.RemoveListener(OnClickStar);
            EventDispatcher.RemoveEventListener<uint>(GuildShopEvents.Refresh_Item, OnRefreshItem);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_Level,OnRefreshLevel);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Guild_Member_Count, OnRefreshLevel);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Skill_View, OnRefreshLevel);
            EventDispatcher.RemoveEventListener<bool, PbGuildPraiseData>(GuildShopEvents.Refresh_Praise_Count, OnRefreshPraiseCount);
        }

        private void OnRefreshItem(uint id)
        {
            if(_data!=null && _data.grid_id == id)
            {
                Refresh();
            }
        }

        private void OnRefreshLevel()
        {
            Refresh();
        }

        private void OnRefreshPraiseCount(bool isPraise,PbGuildPraiseData data)
        {
            GuildShopData shopData = PlayerDataManager.Instance.GuildShopData;
            if (_data.praise_count == 0)
            {
                _txtName.CurrentText.text = item_helper.GetName(_guildShop.__item_id);
            }
            else
            {
                _txtName.CurrentText.text = (74963).ToLanguage(item_helper.GetName(_guildShop.__item_id), _data.praise_count);
            }
            _color.CurrentImage.color = item_helper.GetColor(_guildShop.__item_id);
            RefreshStarView(shopData);
        }

        private void OnClickStar()
        {
            if(PlayerDataManager.Instance.GuildShopData.HasPraised(_data.grid_id) == false)
            {
                GuildShopManager.Instance.PraiseGuildShopItem(_guildShop.__id);
            }
            else
            {
                GuildShopManager.Instance.DispraiseGuildShopItem(_guildShop.__id);
            }
        }

        private void OnClickStarCancel()
        {
            GuildShopManager.Instance.DispraiseGuildShopItem(_guildShop.__id);
        }

        private void OnClickAdd()
        {
            PanelIdEnum.GuildShopPop.Show(_data);
        }

        private void OnClickBuhuo()
        {
            GuildShopManager.Instance.GuildShopRequest(_guildShop.__id);
        }

        private void OnClickBuy()
        {
            if(_data!=null && _guildShop!=null)
            {
                LimitEnum limit = PlayerAvatar.Player.CheckLimit<string>(_guildShop.__rule);
                if(limit!=LimitEnum.NO_LIMIT)
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, player_condition_helper.GetConditionDescribe(_guildShop.__rule), PanelIdEnum.MainUIField);
                    return;
                }
                int result = PlayerAvatar.Player.CheckCostLimit(_guildShop.__buy,true);
                if(result == 0)
                {
                    ConfirmExchangeItem();
                }
                else if (result == -1)
                {
                    UseDiamondReplaceBindDiamond();
                }
            }
        }

        private void UseDiamondReplaceBindDiamond()
        {
            int need = int.Parse(_guildShop.__buy[string.Format("{0}", public_config.MONEY_TYPE_BIND_DIAMOND)]);
            int current = PlayerAvatar.Player.money_bind_diamond;
            string content = MogoLanguageUtil.GetContent(56231, need - current);
            Action confirmAction = new Action(() =>
            {
                ConfirmExchangeItem();
            });
            Action cancelAction = new Action(() =>
            {
                MessageBox.CloseMessageBox();
            });
            MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, confirmAction, cancelAction, false, true, true);
        }

        private void ConfirmExchangeItem()
        {
            GuildShopManager.Instance.ExchangeGuildShopItem(Convert.ToInt32(_data.grid_id), 1, _guildShop.__item_id);
        }

        public override void Dispose()
        {
            RemoveEventListener();
        }

        private void SetCost()
        {
            int costId = 0;
            int num = 0;
            foreach(KeyValuePair<string,string> kvp in _guildShop.__buy)
            {
                costId = int.Parse(kvp.Key);
                num = int.Parse(kvp.Value);
            }
            _txtBuy.ChangeAllStateText(string.Concat("x", num.ToString()));
            _icon1.SetIcon(item_helper.GetIcon(costId));

            LimitEnum limit = PlayerAvatar.Player.CheckLimit<string>(_guildShop.__rule);
            if (limit != LimitEnum.NO_LIMIT)
            {
                _btnBuy.Visible = false;
                _txtTips.Visible = true;
                _txtTips.CurrentText.text = player_condition_helper.GetConditionDescribe(_guildShop.__rule);
            }
        }

        private void Refresh()
        {
            GuildData guildData = PlayerDataManager.Instance.GuildData;
            GuildShopData shopData = PlayerDataManager.Instance.GuildShopData;
            _data = shopData.GetGuildShopData(Convert.ToUInt32(_guildShop.__id));
            _icon.SetIcon(item_helper.GetIcon(_guildShop.__item_id));
            if (_data.praise_count == 0)
            {
                _txtName.CurrentText.text = item_helper.GetName(_guildShop.__item_id);
            }
            else
            {
                _txtName.CurrentText.text = (74963).ToLanguage(item_helper.GetName(_guildShop.__item_id),_data.praise_count);
            }
            _color.CurrentImage.color = item_helper.GetColor(_guildShop.__item_id);

            RefreshStarView(shopData);
            bool guildWarRank = _guildShop.__guild_battle_rank_need == 0 || (guildData.MyGuildData.GuildWarRank != 0 && guildData.MyGuildData.GuildWarRank <= _guildShop.__guild_battle_rank_need);
            if (guildWarRank && _guildShop.__guild_level_need <= guildData.MyGuildData.Level && _guildShop.__guild_member_need <= guildData.MyGuildData.MemberCount && (_guildShop.__guild_skill_need == 0 || guildData.HasLearnSkill(_guildShop.__guild_skill_need)))
            {
                _txtWeijiesuo.Visible = false;
                if(PlayerDataManager.Instance.GuildShopData.GetGuildShopStoreCount(_data.grid_id) > 0)//仓库数量不够
                {
                    PbGuildShopData guildShopData = shopData.GetGuildShopData(_data.grid_id);
                    if (guildShopData.daily_buy_count >= _guildShop.__player_buy_limit)
                    {
                        _txtCount.Visible = false;
                        _txtTips.Visible = true;
                        _txtTips.CurrentText.text = (74973).ToLanguage(_guildShop.__player_buy_limit);//购买达上限
                        _btnBuy.Visible = false;
                    }
                    else
                    {
                        _txtCount.Visible = true;
                        _txtTips.Visible = false;
                        _txtCount.CurrentText.text = (74961).ToLanguage(PlayerDataManager.Instance.GuildShopData.GetGuildShopStoreCount(_data.grid_id));
                        _btnBuy.Visible = true;
                        SetCost();
                       
                    }
                    _txtQuehuo.Visible = false;
                    _btnBuhuo.Visible = false;
                    if (_guildShop.__cost == null || _guildShop.__cost.Count == 0)
                    {
                        _btnAdd.Visible = false;
                    }
                    else
                    {
                        _btnAdd.Visible = guild_rights_helper.CheckHasRights(GuildRightId.guild_shop);
                    }
                }
                else
                {
                    _txtQuehuo.Visible = true;
                    _btnBuy.Visible = false;
                    _txtCount.Visible = false;
                    if(_guildShop.__cost == null || _guildShop.__cost.Count == 0)
                    {
                        _btnBuhuo.Visible = false;
                        _btnAdd.Visible = false;
                        _txtTips.Visible = true;
                        _txtTips.CurrentText.text = (74987).ToLanguage();
                    }
                    else
                    {
                        if (guild_rights_helper.CheckHasRights(GuildRightId.guild_shop))
                        {
                            _btnBuhuo.Visible = false;
                            _btnAdd.Visible = true;
                            _txtTips.Visible = true;
                            _txtTips.CurrentText.text = (74986).ToLanguage();
                        }
                        else
                        {
                            _btnBuhuo.Visible = true;
                            _btnAdd.Visible = false;
                            _txtTips.Visible = false;
                        }
                    }
                    
                   
                }
            }
            else
            {
                _txtWeijiesuo.Visible = true;
                _txtQuehuo.Visible = false;
                _txtCount.Visible = false;
                _btnBuhuo.Visible = false;
                _btnBuy.Visible = false;
                _btnAdd.Visible = false;
                _txtTips.Visible = true;
                _txtTips.CurrentText.text = GetTips();
            }
        }

        private void RefreshStarView(GuildShopData shopData)
        {
            _star.Visible = true;
            if (shopData.HasPraised(_data.grid_id))
            {
                _star.ShowBrightStar();
            }
            else
            {
                _star.HideBrightStar();
            }
            if (shopData.GetMyPraiseCount() >= int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.guild_shop_admire_limit)))
            {
                if (shopData.HasPraised(_data.grid_id) == false)
                {
                    _star.Visible = false;
                }
            }
        }

        private string GetTips()
        {
             GuildData guildData = PlayerDataManager.Instance.GuildData;

             if (guildData.MyGuildData.GuildWarRank == 0 || (_guildShop.__guild_battle_rank_need != 0 && _guildShop.__guild_battle_rank_need < guildData.MyGuildData.GuildWarRank))
             {
                 return (74997).ToLanguage(_guildShop.__guild_battle_rank_need);
             }
             if (_guildShop.__guild_level_need > guildData.MyGuildData.Level)
             {
                 return (74974).ToLanguage(_guildShop.__guild_level_need);
             }
            if(_guildShop.__guild_member_need > guildData.MyGuildData.MemberCount)
            {
                return (74975).ToLanguage(_guildShop.__guild_member_need);
            }
            //Debug.LogError(_guildShop.__guild_level_need + "," + guildData.MyGuildData.Level + "," + _guildShop.__guild_member_need + "," + guildData.MyGuildData.MemberCount + "," + _guildShop.__guild_skill_need + "," + guildData.HasLearnSkill(_guildShop.__guild_skill_need));
            return (74976).ToLanguage(guild_spell_helper.GetName(_guildShop.__guild_skill_need).ToLanguage());
        }
    }
}
