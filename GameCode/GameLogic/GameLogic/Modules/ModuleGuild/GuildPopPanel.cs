﻿using Common.Base;
using Common.Events;
using Game.UI.UIComponent;
using GameData;
using MogoEngine.Events;

namespace ModuleGuild
{
    public class GuildPopPanel : BasePanel
    {
        private GuildContributeToolTip _contributeTip;
        private GuildDonateToolTip _donateTip;
        private GuildAnnouncementToolTip _announcementTip;
        private GuildItemToolTip _itemTip;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildPop; }
        }

        public override void OnShow(object data)
        {
            SetData(data);
        }

        public override void SetData(object data)
        {
            if (data is guild_contribution)
            {
                ShowItemTip(data as guild_contribution);
            }
            else if (data is int)
            {
                switch ((int)data)
                {
                    case 0:
                        ShowContributeTip();
                        break;
                    case 1:
                        ShowDonateTip();
                        break;
                    case 2:
                        ShowAnnouncementTip();
                        break;
                }
            }
        }

        public override void OnClose()
        {
            
        }

        protected override void Awake()
        {
            _contributeTip = AddChildComponent<GuildContributeToolTip>("Container_gongxian");
            _donateTip = AddChildComponent<GuildDonateToolTip>("Container_juanxian");
            _announcementTip = AddChildComponent<GuildAnnouncementToolTip>("Container_gonggao");
            _itemTip = AddChildComponent<GuildItemToolTip>("Container_shijianjilu");
        }

        private void ShowContributeTip()
        {
            this.Visible = true;
            _contributeTip.Visible = true;
            _donateTip.Visible = false;
            _announcementTip.Visible = false;
            _itemTip.Visible = false;
        }

        private void ShowDonateTip()
        {
            this.Visible = true;
            _contributeTip.Visible = false;
            _donateTip.Visible = true;
            _announcementTip.Visible = false;
            _itemTip.Visible = false;
        }

        private void ShowAnnouncementTip()
        {
            this.Visible = true;
            _contributeTip.Visible = false;
            _donateTip.Visible = false;
            _announcementTip.Visible = true;
            _itemTip.Visible = false;
        }

        private void ShowItemTip(guild_contribution data)
        {
            this.Visible = true;
            _contributeTip.Visible = false;
            _donateTip.Visible = false;
            _announcementTip.Visible = false;
            _itemTip.Show(data);
        }

        public void Hide()
        {
            this.Visible = false;
        }


    }
}
