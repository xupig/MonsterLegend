﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePlayerInfo
{
    public class PKInviteItem : KList.KListItemBase
    {
        private KButton _agreeButton;
        private KButton _rejectButton;
        private KButton _inviteButton;
        private KButton _cancelBUtton;
        private StateText _timeText;
        private StateText _nameText;
        private StateText _levelText;
        private StateText _fightText;
        private StateText _guildNameText;
        private IconContainer _icon;

        private ulong _waitTime;
        private ulong _playerDbid;

        private uint _timerId;
        PKInviteData _inviteData;

        protected override void Awake()
        {
            _agreeButton = GetChildComponent<KButton>("Button_jieshou");
            _rejectButton = GetChildComponent<KButton>("Button_jujue");
            _inviteButton = GetChildComponent<KButton>("Button_faqi");
            _cancelBUtton = GetChildComponent<KButton>("Button_hulve");
            _timeText = GetChildComponent<StateText>("Container_Duiyuan0/Label_txtJujue");
            _nameText = GetChildComponent<StateText>("Container_Duiyuan0/Label_txtName");
            _levelText = GetChildComponent<StateText>("Container_Duiyuan0/Container_playerInfo/Container_dengji/Label_txtDengji");
            _fightText = GetChildComponent<StateText>("Container_Duiyuan0/Label_txtZhanli");
            _icon = AddChildComponent<IconContainer>("Container_Duiyuan0/Container_playerInfo/Container_icon");
            _guildNameText = GetChildComponent<StateText>("Container_Duiyuan0/Label_txtGonghui");

            _waitTime = ulong.Parse(global_params_helper.GetGlobalParam(GlobalParamId.pk_wait_time)) * 1000;
        }


        private void AddEventListener()
        {
            _agreeButton.onClick.AddListener(OnAgreeButtonClick);
            _rejectButton.onClick.AddListener(OnRejectButtonClick);
            _inviteButton.onClick.AddListener(OnInviteButtonClick);
            _cancelBUtton.onClick.AddListener(OnCancelButtonClick);
        }

        private void RemoveEventListener()
        {
            _agreeButton.onClick.RemoveListener(OnAgreeButtonClick);
            _rejectButton.onClick.RemoveListener(OnRejectButtonClick);
            _inviteButton.onClick.RemoveListener(OnInviteButtonClick);
            _cancelBUtton.onClick.RemoveListener(OnCancelButtonClick);
        }

        private void OnAgreeButtonClick()
        {
            ResetTimer();
            PKManager.Instance.AccpetPK(_playerDbid, 1);
            PlayerDataManager.Instance.PKData.RemovePKInviteData(_playerDbid);
        }

        private void OnRejectButtonClick()
        {
            ResetTimer();
            PKManager.Instance.AccpetPK(_playerDbid, 0);
            PlayerDataManager.Instance.PKData.RemovePKInviteData(_playerDbid);
        }

        private void OnInviteButtonClick()
        {
            ResetTimer();
            PKManager.Instance.InvitePK(_playerDbid);
            PlayerDataManager.Instance.PKData.RemovePKInviteData(_playerDbid);

        }

        private void OnCancelButtonClick()
        {
            ResetTimer();
            PlayerDataManager.Instance.PKData.RemovePKInviteData(_playerDbid);
        }

        public override void Dispose()
        {
            ResetTimer();
            _inviteData = null;
        }

        public override void Show()
        {
            base.Show();
            AddEventListener();
        }

        public override void Hide()
        {
            RemoveEventListener();
            base.Hide();
            ResetTimer();
        }

        public override object Data
        {
            get
            {
                return _inviteData;
            }
            set
            {
                _inviteData = value as PKInviteData;
                Refresh(_inviteData.data);
            }
        }

        private void Refresh(PbFriendAvatarInfo data)
        {
            _agreeButton.Visible = true;
            _rejectButton.Visible = true;
            _cancelBUtton.Visible = false;
            _inviteButton.Visible = false;
            _playerDbid = data.dbid;
            _nameText.CurrentText.text = data.name;
            _levelText.CurrentText.text = data.level.ToString();
            _fightText.CurrentText.text = data.fight.ToString();
            _icon.SetIcon(MogoPlayerUtils.GetSmallIcon((int)data.vocation));
            if (string.IsNullOrEmpty(data.guild_name))
            {
                _guildNameText.CurrentText.text = MogoLanguageUtil.GetContent(102508); ;
            }
            else
            {
                _guildNameText.CurrentText.text = data.guild_name;
            }
            ulong leftTime = _waitTime - (Global.serverTimeStamp - _inviteData.inviteTime);
            _timeText.CurrentText.text = MogoLanguageUtil.GetContent(105002, Mathf.FloorToInt(leftTime / 1000.0f));
            _timerId = TimerHeap.AddTimer(0, 100, OnStep);
        }

        private void OnStep()
        {
            if (_waitTime <= (Global.serverTimeStamp - _inviteData.inviteTime))
            {
                ResetTimer();
                RefreshOutdateView();
                return;
            }
            ulong leftTime = _waitTime - (Global.serverTimeStamp - _inviteData.inviteTime);
            _timeText.CurrentText.text = MogoLanguageUtil.GetContent(105002, Mathf.FloorToInt(leftTime / 1000.0f));
        }

        private void RefreshOutdateView()
        {
            _agreeButton.Visible = false;
            _rejectButton.Visible = false;
            _cancelBUtton.Visible = true;
            _inviteButton.Visible = true;
            _timeText.CurrentText.text = MogoLanguageUtil.GetContent(105013);
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }
    }
}
