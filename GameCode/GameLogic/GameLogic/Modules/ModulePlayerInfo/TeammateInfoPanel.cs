﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePlayerInfo
{
    public class TeammateInfoData
    {
        public ulong dbid;
        public Vector2 position;
        public string name;
    }

    public class TeammateInfoPanel : BasePanel
    {
        private KDummyButton _closeBtn;
        //private KButton _detailBtn;
        //private KButton _followBtn;
        //private KButton _cancelFollowBtn;
        //private KButton _kickBtn;
        //private KButton _addFriendBtn;
        //private KButton _chatBtn;

        protected List<KButton> _positionBtnList = new List<KButton>();
        protected List<Vector2> _positionList = new List<Vector2>();
        private List<StateText> _positionTxtList = new List<StateText>();
        private KContainer _lineContainer;
        private StateImage _bgImg;
        private float _btnHeight;
        private RectTransform _contentRect;

        private TeammateInfoData _data;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TeammateInfo; }
        }

        protected override void Awake()
        {
            base.Awake();
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _closeBtn = ModalMask.gameObject.AddComponent<KDummyButton>();
           
            KButton btn1 = GetChildComponent<KButton>("Container_xinxiliebiao/Button_gensuiduizhang");
            KButton btn2 = GetChildComponent<KButton>("Container_xinxiliebiao/Button_quxiaogensui");
            KButton btn3 = GetChildComponent<KButton>("Container_xinxiliebiao/Button_qingliduiwu");
            KButton btn4 = GetChildComponent<KButton>("Container_xinxiliebiao/Button_tianjiahaoyou");
            KButton btn5 = GetChildComponent<KButton>("Container_xinxiliebiao/Button_liaotian");
            KButton btn6 = GetChildComponent<KButton>("Container_xinxiliebiao/Button_chakanxiangqing");
            KButton btn7 = GetChildComponent<KButton>("Container_xinxiliebiao/Button_likaiduiwu");
            _lineContainer = GetChildComponent<KContainer>("Container_xinxiliebiao/Container_xian");
            _bgImg = GetChildComponent<StateImage>("Container_xinxiliebiao/ScaleImage_chakanxiangqing");
            _btnHeight = (btn1.transform as RectTransform).sizeDelta.y;
            AddBtnPosition(btn1);
            AddBtnPosition(btn2);
            AddBtnPosition(btn3);
            AddBtnPosition(btn4);
            AddBtnPosition(btn5);
            AddBtnPosition(btn6);
            AddBtnPosition(btn7);
            _contentRect = GetChildComponent<RectTransform>("Container_xinxiliebiao");
        }

        private void AddBtnPosition(KButton btn)
        {
            _positionBtnList.Add(btn);
            _positionTxtList.Add(btn.GetChildComponent<StateText>("Label"));
            _positionList.Add(btn.gameObject.GetComponent<RectTransform>().anchoredPosition);
        }

        private void SetBtnPosition(KButton btn, int index)
        {
            btn.gameObject.GetComponent<RectTransform>().anchoredPosition = _positionList[index];
            if (btn is KShrinkableButton)
            {
                (btn as KShrinkableButton).RefreshRectTransform();
            }
        }

        private void RefreshBtnLayout()
        {
            int count = 0;
            for (int i = 0; i < _positionBtnList.Count; i++)
            {
                if (_positionBtnList[i].Visible == true)
                {
                    SetBtnPosition(_positionBtnList[i], count);
                    count++;
                }
            }
            for (int i = 0; i < _lineContainer.transform.childCount; i++)
            {
                _lineContainer.transform.GetChild(i).gameObject.SetActive(i + 1 < count);
            }
            _bgImg.SetDimensionsDirty();
            RectTransform bgRect = _bgImg.transform as RectTransform;
            bgRect.sizeDelta = new Vector2(bgRect.sizeDelta.x, count * _btnHeight);
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void OnClose()
        {
            RemoveEventListener();        }

        public override void SetData(object data)
        {
            _contentRect.gameObject.SetActive(false);
            _data = data as TeammateInfoData;
            if (FriendManager.Instance.SendGetFriendListMsg() == false)
            {
                RefreshContent();
            }
        }

        private void RefreshContent()
        {
            RefreshPosition();
            RefreshBtnVisibility();
            RefreshBtnLayout();
            _contentRect.gameObject.SetActive(true);
        }

        private void RefreshPosition()
        {
            _contentRect.localPosition = MogoGameObjectHelper.ScreenPointToLocalPointInRectangle(_contentRect.parent as RectTransform, _data.position) + new Vector3(250, 0, 0);
        }

        private void RefreshBtnVisibility()
        {
            HideAllBtn();
            if (PlayerDataManager.Instance.TeamData.IsCaptain())
            {
                //_kickBtn.Visible = true;
                for(int i=0;i<_positionBtnList.Count;i++)
                {
                    _positionBtnList[i].Visible = true;
                }

                _positionTxtList[0].ChangeAllStateText((6016016).ToLanguage());//请离队伍
                _positionBtnList[0].onClick.AddListener(OnKcikBtnCLick);

                _positionTxtList[1].ChangeAllStateText((6016014).ToLanguage());//召唤
                _positionBtnList[1].onClick.AddListener(OnCallBtnClick);

                _positionTxtList[2].ChangeAllStateText((6016017).ToLanguage());//任命队长
                _positionBtnList[2].onClick.AddListener(OnChangeCaptainClick);

                _positionTxtList[3].ChangeAllStateText((6016011).ToLanguage());//传送
                _positionBtnList[3].onClick.AddListener(OnTransmitBtnClick);

                if (PlayerDataManager.Instance.FriendData.HasFriend(_data.dbid) == false)
                {
                    _positionTxtList[4].ChangeAllStateText((6016010).ToLanguage());//加为好友
                    _positionBtnList[4].onClick.AddListener(OnAddFriendBtnCLick);

                    _positionTxtList[5].ChangeAllStateText((6016012).ToLanguage());//聊天
                    _positionBtnList[5].onClick.AddListener(OnChatBtnCLick);

                    _positionTxtList[6].ChangeAllStateText((6016013).ToLanguage());//离开队伍
                    _positionBtnList[6].onClick.AddListener(OnLeaveTeamClick);
                }
                else
                {
                    _positionTxtList[4].ChangeAllStateText((6016012).ToLanguage());//聊天
                    _positionBtnList[4].onClick.AddListener(OnChatBtnCLick);

                    _positionTxtList[5].ChangeAllStateText((6016013).ToLanguage());//离开队伍
                    _positionBtnList[5].onClick.AddListener(OnLeaveTeamClick);

                    _positionBtnList[6].Visible = false;
                }
            }
            else
            {
                FightType fightType = PlayerAutoFightManager.GetInstance().fightType;
                if(PlayerDataManager.Instance.TeamData.CaptainId == _data.dbid)
                {
                    _positionBtnList[0].Visible = true;
                    if (fightType != FightType.FOLLOW_FIGHT)
                    {
                        _positionTxtList[0].ChangeAllStateText((6314002).ToLanguage());//跟随战斗
                        _positionBtnList[0].onClick.AddListener(OnFollowBtnCLick);
                    }
                    else
                    {
                        _positionTxtList[0].ChangeAllStateText((6314003).ToLanguage());//取消跟随
                        _positionBtnList[0].onClick.AddListener(OnCancelFollowBtnCLick);
                    }
                    _positionBtnList[1].Visible = true;
                    _positionTxtList[1].ChangeAllStateText((6016011).ToLanguage());//传送
                    _positionBtnList[1].onClick.AddListener(OnTransmitBtnClick);
                    if (PlayerDataManager.Instance.FriendData.HasFriend(_data.dbid) == false)
                    {
                        _positionBtnList[2].Visible = true;
                        _positionTxtList[2].ChangeAllStateText((6016010).ToLanguage());//加为好友
                        _positionBtnList[2].onClick.AddListener(OnAddFriendBtnCLick);

                        _positionBtnList[3].Visible = true;
                        _positionTxtList[3].ChangeAllStateText((6016012).ToLanguage());//聊天
                        _positionBtnList[3].onClick.AddListener(OnChatBtnCLick);

                        _positionBtnList[4].Visible = true;
                        _positionTxtList[4].ChangeAllStateText((6016013).ToLanguage());//离开队伍
                        _positionBtnList[4].onClick.AddListener(OnLeaveTeamClick);
                    }
                    else
                    {
                        _positionBtnList[2].Visible = true;
                        _positionTxtList[2].ChangeAllStateText((6016012).ToLanguage());//聊天
                        _positionBtnList[2].onClick.AddListener(OnChatBtnCLick);

                        _positionBtnList[3].Visible = true;
                        _positionTxtList[3].ChangeAllStateText((6016013).ToLanguage());//离开队伍
                        _positionBtnList[3].onClick.AddListener(OnLeaveTeamClick);
                    }
                }
                else
                {
                    _positionBtnList[0].Visible = true;
                    _positionTxtList[0].ChangeAllStateText((6016011).ToLanguage());//传送
                    _positionBtnList[0].onClick.AddListener(OnTransmitBtnClick);
                    if (PlayerDataManager.Instance.FriendData.HasFriend(_data.dbid) == false)
                    {
                        _positionBtnList[1].Visible = true;
                        _positionTxtList[1].ChangeAllStateText((6016010).ToLanguage());//加为好友
                        _positionBtnList[1].onClick.AddListener(OnAddFriendBtnCLick);

                        _positionBtnList[2].Visible = true;
                        _positionTxtList[2].ChangeAllStateText((6016012).ToLanguage());//聊天
                        _positionBtnList[2].onClick.AddListener(OnChatBtnCLick);

                        _positionBtnList[3].Visible = true;
                        _positionTxtList[3].ChangeAllStateText((6016013).ToLanguage());//离开队伍
                        _positionBtnList[3].onClick.AddListener(OnLeaveTeamClick);
                    }
                    else
                    {
                        _positionBtnList[1].Visible = true;
                        _positionTxtList[1].ChangeAllStateText((6016012).ToLanguage());//聊天
                        _positionBtnList[1].onClick.AddListener(OnChatBtnCLick);

                        _positionBtnList[2].Visible = true;
                        _positionTxtList[2].ChangeAllStateText((6016013).ToLanguage());//离开队伍
                        _positionBtnList[2].onClick.AddListener(OnLeaveTeamClick);
                    }
                }
            }
        }

        private void HideAllBtn()
        {
            for (int i = 0; i < _positionBtnList.Count;i++ )
            {
                _positionBtnList[i].Visible = false;
                _positionBtnList[i].onClick.RemoveAllListeners();
            }
        }

        private void AddEventListener()
        {
            _closeBtn.onClick.AddListener(OnCloseBtnCLick);
            
            EventDispatcher.AddEventListener(TeamEvent.TEAM_REFRESH, TeammateChange);
            EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_LIST, RefreshContent);
            EventDispatcher.AddEventListener<FightType>(PlayerEvents.ON_FIGHT_TYPE_CHANGED, OnChangeAutoFight);
        }

        private void RemoveEventListener()
        {
            _closeBtn.onClick.RemoveListener(OnCloseBtnCLick);
            HideAllBtn();
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_REFRESH, TeammateChange);
            EventDispatcher.RemoveEventListener(FriendEvents.REFRESH_FRIEND_LIST, RefreshContent);
            EventDispatcher.RemoveEventListener<FightType>(PlayerEvents.ON_FIGHT_TYPE_CHANGED, OnChangeAutoFight);
        }

        private void TeammateChange()
        {
            ClosePanel();
        }

        private void OnChangeCaptainClick()
        {
            string content = MogoLanguageUtil.GetString(LangEnum.TEAM_CHANGE_CAPTAIN, _data.name);
            MessageBox.Show(false, MessageBox.DEFAULT_TITLE, content, delegate()
            {
                TeamManager.Instance.ChangeCaptain(_data.dbid);
                ClosePanel();
            });
           
        }

        private void OnCallBtnClick()
        {
            MessageBox.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.TEAM_CALL_ONE, _data.name), OnCallConfirm);
        }

        private void OnCallConfirm()
        {
            TeamManager.Instance.TeamCallOne(_data.dbid);
            ClosePanel();
        }


        private void OnTransmitBtnClick()
        {
            string content = MogoLanguageUtil.GetString(LangEnum.TEAM_TRANSMIT, _data.name);
            MessageBox.Show(false, MessageBox.DEFAULT_TITLE, content, Transmit);
        }

        private void Transmit()
        {
            TeamManager.Instance.TeamTransmit(_data.dbid);
            ClosePanel();
        }

        private void OnDetailBtnCLick()
        {
            if (PlayerDataManager.Instance.TeamData.TeammateDic.ContainsKey(_data.dbid))
            {
                PbTeamMember member = PlayerDataManager.Instance.TeamData.TeammateDic[_data.dbid];
                FriendManager.Instance.GetPlayerDetailInfo(member.name);
            }
            ClosePanel();
        }

        private void OnFollowBtnCLick()
        {
            if (PlayerDataManager.Instance.TaskData.ring > 0)
            {
                MessageBox.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(115029));
                return;
            }
            TeamManager.Instance.FollowCaptain();
            ClosePanel();
        }

        private void OnCancelFollowBtnCLick()
        {
            PlayerAutoFightManager.GetInstance().Stop();
            TeamManager.Instance.CancelFollowCaptain();
            ClosePanel();
        }

        private void OnKcikBtnCLick()
        {
            if (PlayerDataManager.Instance.TeamData.TeammateDic.ContainsKey(_data.dbid))
            {
                PbTeamMember member = PlayerDataManager.Instance.TeamData.TeammateDic[_data.dbid];
                string content = MogoLanguageUtil.GetString(LangEnum.TEAM_KICK, member.name);
                MessageBox.Show(false, MessageBox.DEFAULT_TITLE, content, Kick);
            }
            ClosePanel();
        }

        private void Kick()
        {
            TeamManager.Instance.KickTeammate(_data.dbid);
        }

        private void OnAddFriendBtnCLick()
        {
            PbFriendAvatarInfo avatarInfo = new PbFriendAvatarInfo();
            avatarInfo.dbid = _data.dbid;
            FriendManager.Instance.AddFriend(avatarInfo);
            ClosePanel();
        }

        private void OnLeaveTeamClick()
        {
            MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.TEAM_LEAVE_TEAM), delegate() 
            { 
                TeamManager.Instance.LeaveTeam();
                ClosePanel();
            });
            
        }

        private void OnChatBtnCLick()
        {
            if (PlayerDataManager.Instance.TeamData.TeammateDic.ContainsKey(_data.dbid))
            {
                PbTeamMember member = PlayerDataManager.Instance.TeamData.TeammateDic[_data.dbid];
                PbFriendAvatarInfo avatarInfo = new PbFriendAvatarInfo();
                avatarInfo.dbid = _data.dbid;
                avatarInfo.name = member.name;
                PanelIdEnum.Chat.Show(avatarInfo);
            }
            ClosePanel();
        }

        private void OnCloseBtnCLick()
        {
            ClosePanel();
        }

        private void OnChangeAutoFight(FightType fightType)
        {
            RefreshContent();
        }

    }
}
