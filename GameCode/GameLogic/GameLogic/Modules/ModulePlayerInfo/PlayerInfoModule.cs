﻿#region 模块信息
/*==========================================
// 文件名：PlayerInfoModule
// 命名空间: GameLogic.GameLogic.Modules.ModulePlayerInfo
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/19 15:40:44
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePlayerInfo
{
    public class PlayerInfoModule:BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.PlayerInfo, "Container_PlayerInfoPanel", MogoUILayer.LayerUIToolTip, "ModulePlayerInfo.PlayerInfoPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.PK, "Container_PKPanel", MogoUILayer.LayerUIToolTip, "ModulePlayerInfo.PKPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.PlayerInfoDetail, "Container_RankingListDetail", MogoUILayer.LayerUIPanel, "ModulePlayerInfo.PlayerInfoDetailPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.TeammateInfo, "Container_TeammateInfoPanel", MogoUILayer.LayerUIToolTip, "ModulePlayerInfo.TeammateInfoPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }
    }
}
