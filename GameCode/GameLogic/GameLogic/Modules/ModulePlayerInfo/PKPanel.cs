﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;

namespace ModulePlayerInfo
{
    public class PKPanel : BasePanel
    {
        private KScrollView _scrollView;
        private KPageableList _list;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.PK; }
        }

        protected override void Awake()
        {
            base.Awake();
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_PK");
            _list = GetChildComponent<KPageableList>("ScrollView_PK/mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        public override void OnShow(object data)
        {
            EventDispatcher.AddEventListener(PKEvents.REFRESH_INVITE_CONTENT, RefreshContent);
            RefreshContent();
        }

        public override void OnClose()
        {
            RefreshNotice();
            EventDispatcher.RemoveEventListener(PKEvents.REFRESH_INVITE_CONTENT, RefreshContent);
        }

        private void RefreshContent()
        {
            _list.SetDataList<PKInviteItem>(PlayerDataManager.Instance.PKData.InviteDataList);
        }

        private void RefreshNotice()
        {
            for (int i = 0; i < PlayerDataManager.Instance.PKData.InviteDataList.Count; i++)
            {
                string content = MogoLanguageUtil.GetContent(105001);
                FastNoticeData data = new FastNoticeData(content, PanelIdEnum.PK);
                PlayerDataManager.Instance.NoticeData.AddNoticeData(data);
            }
        }

    }
}
