﻿#region 模块信息
/*==========================================
// 文件名：PlayerInfoDetailPanel
// 命名空间: GameLogic.GameLogic.Modules.ModulePlayerInfo
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/10/10 10:11:32
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using ModuleRankingList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePlayerInfo
{
    public class PlayerInfoDetailPanel:BasePanel
    {

        private CategoryList _playerInfoCategory;
        private RankPlayerRoleInfoView _playerRoleInfoView;
        private RankPlayerGemView _playerGemView;
        private RankPlayerRuneView _playerRuneView;
        private RankPlayerFightInfoView _playerFightInfoView;

        private ulong playerDbId;
        public static int index = 1;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_fanhui");
            _playerInfoCategory = GetChildComponent<CategoryList>("List_categoryList");
            _playerRoleInfoView = AddChildComponent<RankPlayerRoleInfoView>("Container_roleInfoView");
            _playerRoleInfoView.OnPlayerInfoDetailPanelHide();
            _playerGemView = AddChildComponent<RankPlayerGemView>("Container_baoshi");
            _playerRuneView = AddChildComponent<RankPlayerRuneView>("Container_fuwen");
            _playerFightInfoView = AddChildComponent<RankPlayerFightInfoView>("Container_gonghuizhan");
            InitCategory();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.PlayerInfoDetail; }
        }

        public override void OnShow(object data)
        {
            this.playerDbId = (ulong)data;
            InitDetailView();
            AddEventListener();
        }

        public override void OnClose()
        {
            CloseAllSubView();
            RemoveEventListener();
        }

        private void CloseAllSubView()
        {
            _playerRuneView.Hide();
            _playerGemView.Hide();
            _playerRoleInfoView.Hide();
            _playerFightInfoView.Hide();
        }


        private void InitDetailView()
        {
            _playerInfoCategory.SelectedIndex = 0;
            OnSelectedInfoList(_playerInfoCategory, 0);
        }

        private void AddEventListener()
        {
            _playerInfoCategory.onSelectedIndexChanged.AddListener(OnSelectedInfoList);
        }

        private void RemoveEventListener()
        {
            _playerInfoCategory.onSelectedIndexChanged.RemoveListener(OnSelectedInfoList);
        }

        private void InitCategory()
        {
            List<CategoryItemData> listToggleData = rank_helper.GetRankingDetailCategoryNames();
            _playerInfoCategory.RemoveAll();
            _playerInfoCategory.SetDataList<CategoryListItem>(listToggleData);
        }

        private void OnSelectedInfoList(CategoryList target, int index)
        {
            _playerRoleInfoView.Hide();
            _playerGemView.Hide();
            _playerRuneView.Hide();
            _playerFightInfoView.Hide();
            switch (index)
            {
                case 0:
                    _playerRoleInfoView.Show(playerDbId);
                    break;
                case 1:
                    _playerGemView.Show(playerDbId);
                    break;
                case 2:
                    _playerRuneView.Show(playerDbId);
                    break;
                case 3:
                    _playerFightInfoView.Show(playerDbId);
                    break;
            }
        }
    }
}
