﻿#region 模块信息
/*==========================================
// 文件名：PlayerInfoPanel
// 命名空间: GameLogic.GameLogic.Modules.ModulePlayerInfo
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/19 15:34:39
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePlayerInfo
{
    public class PlayerInfoPanel : BasePanel
    {
        private StateText _txtName;
        private StateText _txtLevel;
        private StateText _txtGender;
        private StateText _txtId;
        private StateText _txtGuildName;
        private StateText _txtTitle;
        private StateText _txtIntimate;

        private KButton _btnQuestion;
        private KButton _btnInformation;
        private KButton _btnChat;
        private KButton _btnAddFriend;
        private KButton _btnDelFriend;
        private KButton _btnAddBlackList;
        private KButton _btnDelBlackList;
        private KButton _btnInviteTeam;
        private KButton _btnApplyTeam;
        private KButton _btnApplyGuild;
        private KButton _btnInviteGuild;
        private KButton _btnPK;
        private KButton _btnReport;
        private KButton _btnPresent;
        private KButton _btnDuel;
        private Locater _locater;
        private KDummyButton _dummyBtnMask;

        private IconContainer _vocationIconContainer;

        private string _levelTemplate;
        private string _genderTemplate;
        private string _idTemplate;
        private string _titleTemplate;
        private string _guildTemplate;
        private string _intimateTemplate;

        private PbFriendAvatarInfo _avatarInfo;

        private Vector2 _orignalPos;

        private Vector2 _srcOffsetPos = Vector2.zero;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.PlayerInfo; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");

            _btnInformation = GetChildComponent<KButton>("Button_xinxi");
            _btnQuestion = GetChildComponent<KButton>("Button_wenhao");
            _btnChat = GetChildComponent<KButton>("Button_kaishiliaotian");
            _btnAddFriend = GetChildComponent<KButton>("Button_jiaweihaoyou");
            _btnDelFriend = GetChildComponent<KButton>("Button_shanchuhaoyou");
            _btnDelBlackList = GetChildComponent<KButton>("Button_quxiaopingbi");
            _btnAddBlackList = GetChildComponent<KButton>("Button_pinbi");
            _btnInviteTeam = GetChildComponent<KButton>("Button_yaoqingrudui");
            _btnApplyTeam = GetChildComponent<KButton>("Button_shenqingqingruduii");
            _btnInviteGuild = GetChildComponent<KButton>("Button_yaoqingruhui");
            _btnApplyGuild = GetChildComponent<KButton>("Button_shenqingruhui");
            _btnPK = GetChildComponent<KButton>("Button_PK");
            _btnDuel = GetChildComponent<KButton>("Button_duel");
            _btnReport = GetChildComponent<KButton>("Button_jubao");
            _btnPresent = GetChildComponent<KButton>("Button_zengsong");
            _txtName = GetChildComponent<StateText>("Label_txtbiaoti");
            _txtLevel = GetChildComponent<StateText>("Label_txtDengji");
            _txtGender = GetChildComponent<StateText>("Label_txtXingbie");
            _txtId = GetChildComponent<StateText>("Label_txtId");
            _txtTitle = GetChildComponent<StateText>("Label_txtChengwei");
            _txtGuildName = GetChildComponent<StateText>("Label_txtGongHui");
            _txtIntimate = GetChildComponent<StateText>("Label_txtYouhao");
            _vocationIconContainer = AddChildComponent<IconContainer>("Container_zhiye");
            _dummyBtnMask = this.gameObject.AddComponent<KDummyButton>();
            _levelTemplate = _txtLevel.CurrentText.text;
            _genderTemplate = _txtGender.CurrentText.text;
            _idTemplate = _txtId.CurrentText.text;
            _titleTemplate = _txtTitle.CurrentText.text;
            _guildTemplate = _txtGuildName.CurrentText.text;
            _intimateTemplate = _txtIntimate.CurrentText.text;
            _locater = gameObject.AddComponent<Locater>();
            _orignalPos = _locater.Position;
            RectTransform bgRectTrans = GetChildComponent<RectTransform>("Container_lashen");
            _srcOffsetPos = bgRectTrans.anchoredPosition;
            base.Awake();
        }

        public override void OnShow(object data)
        {
            SetData(data);
            AddEventListener();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private object _object;

        public override void SetData(object data)
        {
            _object = data;
            if(data is PbFriendAvatarInfo)
            {
                _avatarInfo = data as PbFriendAvatarInfo;
                _locater.Position = _orignalPos;
            }
            else if (data is PositionInfo)
            {
                PositionInfo positionInfo = data as PositionInfo;
                _avatarInfo = positionInfo.avatarInfo;
                _locater.Position = positionInfo.pos + _orignalPos;
            }
            else if (data is PlayerInfoParam)
            {
                PlayerInfoParam info = data as PlayerInfoParam;
                SetPosition(info);
            }
            if(_avatarInfo != null)
            {
                Refresh();
            }
        }

        private void SetPosition(PlayerInfoParam info)
        {
            _avatarInfo = info.avatarInfo;
            Vector3 destPos = info.targetPos;
            if (info.horizontalAlignType == PlayerInfoParam.AlignmentType.SpecifyPos)
            {
                //在这个范围的值说明是使用Panel自身的坐标值
                if (destPos.x <= -5000 || destPos.x >= 5000)
                {
                    destPos.x = _orignalPos.x;
                }
            }
            else if (info.horizontalAlignType == PlayerInfoParam.AlignmentType.ScreenCenter)
            {
                RectTransform bgRect = GetChildComponent<RectTransform>("Container_lashen");
                float destX = (UIManager.CANVAS_WIDTH / 2.0f - bgRect.sizeDelta.x / 2.0f);
                RectTransform panelRect = gameObject.GetComponent<RectTransform>();
                float panelPosX = (_locater.Width * panelRect.pivot.x);
                float offsetX = destX - bgRect.anchoredPosition.x;
                destPos.x = panelPosX + offsetX;
            }
            if (info.verticalAlignType == PlayerInfoParam.AlignmentType.SpecifyPos)
            {
                if (destPos.y <= -5000 || destPos.y >= 5000)
                {
                    destPos.y = _orignalPos.y;
                }
            }
            else if (info.verticalAlignType == PlayerInfoParam.AlignmentType.ScreenCenter)
            {
                RectTransform bgRect = GetChildComponent<RectTransform>("Container_lashen");
                float destY = - (UIManager.CANVAS_HEIGHT / 2.0f - bgRect.sizeDelta.y / 2.0f);
                RectTransform panelRect = gameObject.GetComponent<RectTransform>();
                float panelPosY = - (_locater.Height * panelRect.pivot.y);
                float offsetY = destY - bgRect.anchoredPosition.y;
                destPos.y = panelPosY + offsetY;
            }
            _locater.Position = new Vector2(destPos.x, destPos.y);
        }

        private void Refresh()
        {
            RefreshTxt();
            RefreshBtn();
        }

        private void RefreshTxt()
        {
            _txtName.CurrentText.text = _avatarInfo.name;
            _txtLevel.CurrentText.text = string.Format(_levelTemplate, _avatarInfo.level);
            int genderLangId = 6;
            if (_avatarInfo.vocation == (uint)Vocation.Wizard || _avatarInfo.vocation == (uint)Vocation.Warrior)
            {
                genderLangId = 5;
            }
            _vocationIconContainer.SetIcon(icon_item_helper.GetVocationIconId((Vocation)_avatarInfo.vocation));
            _txtGender.CurrentText.text = string.Format(_genderTemplate, MogoLanguageUtil.GetContent(genderLangId));
            _txtId.CurrentText.text = string.Format(_idTemplate, _avatarInfo.dbid);
            string title = _avatarInfo.title.ToString();
            if(_avatarInfo.title == 0)
            {
                title = MogoLanguageUtil.GetContent(63508); 
            }
            _txtTitle.CurrentText.text = string.Format(_titleTemplate, title);
            _avatarInfo.guild_name = MogoProtoUtils.ParseByteArrToString(_avatarInfo.guild_name_bytes);
            string guildName = _avatarInfo.guild_name;
            if(_avatarInfo.guild_id == 0)
            {
                guildName = MogoLanguageUtil.GetContent(63509);
            }
            _txtGuildName.CurrentText.text = string.Format(_guildTemplate, guildName);
            _txtIntimate.CurrentText.text = string.Format(_intimateTemplate, _avatarInfo.degree);
        }

        private void RefreshBtn()
        {
            _btnAddFriend.gameObject.SetActive(_avatarInfo.is_friend != 1);
            _btnDelFriend.gameObject.SetActive(_avatarInfo.is_friend == 1);
            count = 1;
            _btnPresent.gameObject.SetActive(_avatarInfo.is_friend == 1);
            if(_avatarInfo.is_friend == 1)
            {
                count++;
            }
            UpdateGuildInfo();
            UpdateTeamInfo();
            ChapterType chapterType = GameSceneManager.GetInstance().chapterType;
            switch (chapterType)
            {
                case ChapterType.DuelWait:
                    _btnPK.Visible = false;
                    UpdateDuelInfo();
                    break;
                default:
                    _btnDuel.Visible = false;
                    UpdatePKInfo();
                    break;
            }
            UpdateButtonPosition(_btnChat, count);
            bool isShield = PlayerDataManager.Instance.ChatData.isShield(_avatarInfo.dbid);
            _btnDelBlackList.gameObject.SetActive(isShield);
            _btnAddBlackList.gameObject.SetActive(isShield == false);
        }

        private int count = 4;

        private void UpdateTeamInfo()
        {
            uint myTeamId = PlayerDataManager.Instance.TeamData.TeamId;
            //玩家是否已经有队伍
            if (_avatarInfo.team_flag == 1)
            {
                _btnApplyTeam.gameObject.SetActive(myTeamId == 0);
                _btnInviteTeam.gameObject.SetActive(false);
                if (myTeamId == 0)
                {
                    UpdateButtonPosition(_btnApplyTeam, count);
                    count++;
                }
            }
            else
            {
                _btnApplyTeam.gameObject.SetActive(false);
                _btnInviteTeam.gameObject.SetActive(true);
                UpdateButtonPosition(_btnInviteTeam, count);
                count++;
            }
        }

        private void UpdateDuelInfo()
        {
            _btnDuel.Visible = true;
            UpdateButtonPosition(_btnDuel, count);
            count++;
        }

        private void UpdatePKInfo()
        {
            _btnPK.Visible = false;
            function func = function_helper.GetFunction(38);
            int level = int.Parse(func.__condition["1"]);
            if (PlayerAvatar.Player.level >= level && _avatarInfo.level >= level)
            {
                _btnPK.Visible = true;
                _btnPK.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(105000));
                UpdateButtonPosition(_btnPK, count);
                count++;
            }
        }

        private void UpdateGuildInfo()
        {
            ulong guildId = PlayerDataManager.Instance.GuildData.MyGuildData.Id;
            //双方都有公会，隐藏按钮
            if (_avatarInfo.guild_id != 0 && guildId != 0)
            {
                _btnInviteGuild.gameObject.SetActive(false);
                _btnApplyGuild.gameObject.SetActive(false);
            }
            //双方都没有公会，隐藏按钮
            else if (_avatarInfo.guild_id == 0 && guildId == 0)
            {
                _btnInviteGuild.gameObject.SetActive(false);
                _btnApplyGuild.gameObject.SetActive(false);
            }
            //对方有公会，自己没有，显示申请入会
            else if(_avatarInfo.guild_id != 0 && guildId == 0)
            {
                _btnApplyGuild.gameObject.SetActive(true);
                _btnInviteGuild.gameObject.SetActive(false);
                UpdateButtonPosition(_btnApplyGuild, count);
                count++;
            }
            //对方没有公会，自己有，显示邀请入会
            else 
            {
                if (CheckIsDisplayInviteGuildButton() == true)
                {
                    _btnInviteGuild.gameObject.SetActive(true);
                    _btnApplyGuild.gameObject.SetActive(false);
                    UpdateButtonPosition(_btnInviteGuild, count);
                    count++;
                }
                else
                {
                    _btnInviteGuild.gameObject.SetActive(false);
                    _btnApplyGuild.gameObject.SetActive(false);
                }
            }
        }

        private bool CheckIsDisplayInviteGuildButton()
        {
            //公会未解锁，不显示邀请入会按钮
            if (PlayerDataManager.Instance.FunctionData.IsFunctionOpen(FunctionId.guild) == false)
            {
                return false;
            }
            //玩家不在线，不显示邀请入会按钮
            else if (_avatarInfo.online != 1)
            {
                return false;
            }
            //玩家不满足公会开启等级，不显示邀请入会按钮
            else if (_avatarInfo.level < int.Parse(global_params_helper.GetGlobalParam(82)))
            {
                return false;
            }
            return true;
        }

        private void UpdateButtonPosition(KButton button, int count)
        {
            RectTransform rectTransform = button.gameObject.GetComponent<RectTransform>();
            rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, count * 51 - 616);
            (button as KShrinkableButton).RefreshRectTransform();
        }

        private void AddEventListener()
        {
            _btnQuestion.onClick.AddListener(OnClickQuestion);
            _btnChat.onClick.AddListener(OnClickChat);
            _btnAddFriend.onClick.AddListener(OnClickFriend);
            _btnDelFriend.onClick.AddListener(OnDelFriend);
            _btnAddBlackList.onClick.AddListener(OnClickBlackList);
            _btnDelBlackList.onClick.AddListener(OnDelBlackList);
            _btnInviteTeam.onClick.AddListener(OnClickTeam);
            _btnApplyTeam.onClick.AddListener(OnApplyTeam);
            _btnInviteGuild.onClick.AddListener(OnClickGuild);
            _btnApplyGuild.onClick.AddListener(OnApplyGuild);
            _btnPK.onClick.AddListener(OnClickPK);
            _btnDuel.onClick.AddListener(OnClickDuel);
            _btnReport.onClick.AddListener(OnClickReport);
            _btnPresent.onClick.AddListener(OnClickPresent);
            _dummyBtnMask.onClick.AddListener(OnClickMask);
            _btnInformation.onClick.AddListener(OnClickInformation);
            EventDispatcher.AddEventListener<UInt64>(FriendEvents.ADD_FRIEND, OnAddFriend);
            EventDispatcher.AddEventListener<UInt64>(FriendEvents.REMOVE_FRIEND, OnRemoveFriend);
            EventDispatcher.AddEventListener<UInt64>(FriendEvents.ADD_BALCK_LIST, OnAddBlackList);
            EventDispatcher.AddEventListener<UInt64>(FriendEvents.REMOVE_BLACK_LIST, OnRemoveBlackList);
            EventDispatcher.AddEventListener(MainUIEvents.CLOSE_PLAYERINFO, OnClosePlayerInfo);
        }

        private void RemoveEventListener()
        {
            _btnQuestion.onClick.RemoveListener(OnClickQuestion);
            _btnChat.onClick.RemoveListener(OnClickChat);
            _btnAddFriend.onClick.RemoveListener(OnClickFriend);
            _btnDelFriend.onClick.RemoveListener(OnDelFriend);
            _btnAddBlackList.onClick.RemoveListener(OnClickBlackList);
            _btnDelBlackList.onClick.RemoveListener(OnDelBlackList);
            _btnInviteTeam.onClick.RemoveListener(OnClickTeam);
            _btnApplyTeam.onClick.RemoveListener(OnApplyTeam);
            _btnInviteGuild.onClick.RemoveListener(OnClickGuild);
            _btnApplyGuild.onClick.RemoveListener(OnApplyGuild);
            _btnPK.onClick.RemoveListener(OnClickPK);
            _btnDuel.onClick.RemoveListener(OnClickDuel);
            _btnReport.onClick.RemoveListener(OnClickReport);
            _btnPresent.onClick.RemoveListener(OnClickPresent);
            _btnInformation.onClick.RemoveListener(OnClickInformation);
            EventDispatcher.RemoveEventListener<UInt64>(FriendEvents.ADD_FRIEND, OnAddFriend);
            EventDispatcher.RemoveEventListener<UInt64>(FriendEvents.REMOVE_FRIEND, OnRemoveFriend);
            EventDispatcher.RemoveEventListener<UInt64>(FriendEvents.ADD_BALCK_LIST, OnAddBlackList);
            EventDispatcher.RemoveEventListener<UInt64>(FriendEvents.REMOVE_BLACK_LIST, OnRemoveBlackList);
            EventDispatcher.RemoveEventListener(MainUIEvents.CLOSE_PLAYERINFO, OnClosePlayerInfo);
        }

        private void OnClickInformation()
        {
            ClosePanel();
            PanelIdEnum.PlayerInfoDetail.Show(_avatarInfo.dbid);
        }

        private void OnClickQuestion()
        {
            RuleTipsDataWrapper wrapper = new RuleTipsDataWrapper(MogoLanguageUtil.GetContent(57112), _btnQuestion.GetComponent<RectTransform>(), RuleTipsDirection.Bottom);
            UIManager.Instance.ShowPanel(PanelIdEnum.RuleTips, wrapper);
        }

        private void OnClosePlayerInfo()
        {
            ClosePanel();
        }

        private void OnAddFriend(UInt64 dbId)
        {
            if(dbId == _avatarInfo.dbid)
            {
                _avatarInfo.is_friend = 1;
                Refresh();
            }
        }

        private void OnRemoveFriend(UInt64 dbId)
        {
            if(dbId == _avatarInfo.dbid)
            {
                _avatarInfo.is_friend = 0;
                Refresh();
            }
        }

        private void OnAddBlackList(UInt64 dbId)
        {
            if(dbId == _avatarInfo.dbid)
            {
                _avatarInfo.is_shield = 1;
                Refresh();
            }
        }

        private void OnRemoveBlackList(UInt64 dbId)
        {
            if(dbId == _avatarInfo.dbid)
            {
                _avatarInfo.is_shield = 0;
                Refresh();
            }
        }

        private void OnClickChat()
        {
            ClosePanel();
            PanelIdEnum.Chat.Show(_avatarInfo);
        }

        private void OnClickFriend()
        {
            FriendManager.Instance.AddFriend(_avatarInfo);
        }

        private void OnDelFriend()
        {
            MessageBox.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.FRIEND_DEL_CONFIRM, _avatarInfo.name), OnDelCallBack);
        }

        private void OnDelCallBack()
        {
            FriendManager.Instance.DelFriend(_avatarInfo);
        }

        private void OnClickBlackList()
        {
            PbChatAvatarInfo chatAvatarInfo = new PbChatAvatarInfo();
            chatAvatarInfo.dbid = _avatarInfo.dbid;
            chatAvatarInfo.gender = _avatarInfo.gender;
            chatAvatarInfo.level = _avatarInfo.level;
            chatAvatarInfo.name_bytes = _avatarInfo.name_bytes;
            chatAvatarInfo.name = _avatarInfo.name;
            chatAvatarInfo.vocation = _avatarInfo.vocation;
            chatAvatarInfo.fight = _avatarInfo.fight;
            ChatManager.Instance.AddToBlackList(chatAvatarInfo);
        }

        private void OnDelBlackList()
        {
            ChatManager.Instance.RemoveFromBlackList(_avatarInfo.dbid);
        }

        private void OnClickTeam()
        {
            TeamManager.Instance.Invite(_avatarInfo.dbid, 0);
            ClosePanel();
        }

        private void OnApplyTeam()
        {
            TeamManager.Instance.Apply(_avatarInfo.dbid);
            ClosePanel();
        }

        private void OnClickGuild()
        {
            if (CheckAgainIsCanInvite() == true)
            {
                GuildManager.Instance.InviteJoin(_avatarInfo.dbid);
            }
            ClosePanel();
        }

        private bool CheckAgainIsCanInvite()
        {
            int currentLevel = PlayerDataManager.Instance.GuildData.MyGuildData.Level;
            int currentMemberCount = PlayerDataManager.Instance.GuildData.MyGuildData.MemberCount;
            int currentLevelMaxMemberCount = guild_level_helper.GetMemberCount(currentLevel);
            int guildMaxLevel = guild_level_helper.GetMaxLevel();

            if(currentMemberCount < currentLevelMaxMemberCount)
            {
                return true;
            }
            else if(currentLevel < guildMaxLevel)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74631), PanelIdEnum.PlayerInfo);
                return false;
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74876), PanelIdEnum.PlayerInfo);
                return false;
            }
        }

        private void OnClickPK()
        {
            if (_avatarInfo != null)
            {
                PKManager.Instance.InvitePK(_avatarInfo.dbid);
            }
            ClosePanel();
        }

        private void OnClickDuel()
        {
            if (_avatarInfo != null)
            {
                DuelManager.Instance.RequestQueryPlayerInfo(_avatarInfo.dbid);
            }
            ClosePanel();
        }

        private void OnApplyGuild()
        {
            int needLevel = int.Parse(global_params_helper.GetGlobalParam(82));
            if (PlayerAvatar.Player.level < needLevel)
            {
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74612, needLevel));
                return;
            }
            GuildManager.Instance.ApplyJoinGuild(_avatarInfo.guild_id);
            ClosePanel();
        }

        private void OnClickReport()
        {
            ClosePanel();
            PanelIdEnum.ChatReport.Show(_avatarInfo);
        }

        private void OnClickPresent()
        {
            ClosePanel();
            PanelIdEnum.FriendSend.Show(_avatarInfo);
        }

        private void OnClickMask()
        {
            ClosePanel();
        }

    }
}
