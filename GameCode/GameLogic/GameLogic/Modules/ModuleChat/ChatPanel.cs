﻿#region 模块信息
/*==========================================
// 模块名：ChatView
// 命名空间: ModuleChat
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/26
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;

using Common.Base;
using Game.UI.UIComponent;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Chat;
using UnityEngine;
using GameLoader.Utils;
using GameMain.Entities;
using Common.Data;

namespace ModuleChat
{
    /// <summary>
    /// 聊天视图类
    /// </summary>
    public class ChatPanel : BasePanel
    {
        private KToggleGroup _channelToggleGroup;
        private KButton _privateChatBackBtn;
        private ChatInputView _inputView;
        private ChatTeamListView _teamListView;
        private ChatGuildListView _guildListView;
        private ChatPrivateView _privateView;
        private ChatVoiceNoticeView _voiceNoticeView;
        private ChatConvenientMenuView _convenientMenuView;
        private ChatVoiceAgent _chatVoiceAgent;
        private ChatQuestionView _chatQuestionView;

        private KContainer _currentView;

        private ChatAllChannelContentView _allChannelView;
        private ChatAllChannelContentView _worldChannelView;
        private ChatAllChannelContentView _systemChannelView;
        private ChatTeamContentView _teamChannelView;
        private ChatAllChannelContentView _guildChannelView;
        private ChatAllChannelContentView _tipsChannelView;

        private ChatData _chatData;
        private PbFriendAvatarInfo _avatarInfo;
        private bool _isExecuteOnShow = false;
        private int _curSelectIndex = 0;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Chat; }
        }

        protected override void Awake()
        {
            _chatData = PlayerDataManager.Instance.ChatData;
            CloseBtn = GetChildComponent<KButton>("Container_chatBg/Button_shousuo");
            _channelToggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_category");
            _privateChatBackBtn = GetChildComponent<KButton>("ToggleGroup_category/Button_miliaoBack");
            GameObject backBtnCheckMarkGo = _privateChatBackBtn.transform.FindChild("checkmark").gameObject;
            backBtnCheckMarkGo.SetActive(true);
            _inputView = AddChildComponent<ChatInputView>("Container_Shuruku");
            _teamListView = AddChildComponent<ChatTeamListView>("Container_DuiwuLiebiao");
            _guildListView = AddChildComponent<ChatGuildListView>("Container_GonghuiLiebiao");
            _privateView = AddChildComponent<ChatPrivateView>("Container_ChatMiliao");
            _voiceNoticeView = AddChildComponent<ChatVoiceNoticeView>("Container_yuyintishi");
            _convenientMenuView = GetChild("Container_bianjieMenu/Container_commonUseMenu").AddComponent<ChatConvenientMenuView>();
            _chatQuestionView = AddChildComponent<ChatQuestionView>("Container_kaishidati");

            _allChannelView = AddChildComponent<ChatAllChannelContentView>("Container_ChatZonghe");
            _allChannelView.Channel = public_config.CHANNEL_ID_ALL;
            _worldChannelView = AddChildComponent<ChatAllChannelContentView>("Container_ChatShijie");
            _worldChannelView.Channel = public_config.CHANNEL_ID_WORLD;
            _systemChannelView = AddChildComponent<ChatAllChannelContentView>("Container_ChatFujin");
            _systemChannelView.Channel = public_config.CHANNEL_ID_SYSTEM;
            _teamChannelView = AddChildComponent<ChatTeamContentView>("Container_ChatDuiwu");
            _teamChannelView.Channel = public_config.CHANNEL_ID_TEAM;
            _guildChannelView = AddChildComponent<ChatAllChannelContentView>("Container_ChatGonghui");
            _guildChannelView.Channel = public_config.CHANNEL_ID_GUILD;
            _tipsChannelView = AddChildComponent<ChatAllChannelContentView>("Container_ChatXitong");
            _tipsChannelView.Channel = public_config.CHANNEL_ID_HELP;

            GameObject leftVoiceAnimGo = GetChild("Container_yuyinbofang/Container_shengyinzuo");
            GameObject rightVoiceAnimGo = GetChild("Container_yuyinbofang/Container_shengyinyou");
            ChatVoiceAnimPlayPool.Instance.AddVoiceAnimTemplate(VoiceAnimTemplateId.LeftVoice, leftVoiceAnimGo);
            ChatVoiceAnimPlayPool.Instance.AddVoiceAnimTemplate(VoiceAnimTemplateId.RightVoice, rightVoiceAnimGo);
            GameObject redEnvelopGo = GetChild("Container_widgetTemplate/Button_hongbao");
            ChatWidgetPool.Instance.AddWidgetTemplate(ChatWidgetPool.ChatWidgetTemplateId.RedEnvelope, redEnvelopGo);

            _chatVoiceAgent = gameObject.AddComponent<ChatVoiceAgent>();
            List<ChatVoiceInputView> voiceInputList = new List<ChatVoiceInputView>();
            voiceInputList.Add(_inputView.VoiceInputView);
            _chatVoiceAgent.InitView(_voiceNoticeView, voiceInputList);
        }

        private void AddEventListener()
        {
            _channelToggleGroup.onSelectedIndexChanged.AddListener(OnChannelChanged);
            _privateChatBackBtn.onClick.AddListener(OnPrivateChannelBtnClick);
            EventDispatcher.AddEventListener(ChatEvents.OPEN_EMOJI, OnOpenEmoji);
            EventDispatcher.AddEventListener(ChatEvents.CLOSE_EMOJI, OnCloseEmoji);
            EventDispatcher.AddEventListener(ChatEvents.HIDE_INPUT, OnHideInput);
            EventDispatcher.AddEventListener(ChatEvents.SHOW_INPUT, OnShowInput);
            EventDispatcher.AddEventListener<int, PbChatInfoResp>(ChatEvents.RECEIVE_CONTENT, OnReceiveContent);
            EventDispatcher.AddEventListener<int>(ChatEvents.ReadContent, OnReadContent);
            EventDispatcher.AddEventListener(ChatEvents.ENABLE_PRIVATE_CHAT_BACK_BTN, OnEnablePrivateChatBackBtn);
            EventDispatcher.AddEventListener<UInt64>(ChatEvents.SELECT_FRIEND, OnRefreshPrivateChannelPoint);
            EventDispatcher.AddEventListener(ChatEvents.QUIT_TEAM, OnUpdateNewMsgFlagQuitTeam);
            EventDispatcher.AddEventListener(ChatEvents.LEAVE_GUILD, OnUpdateNewMsgFlagLeaveGuild);
            EventDispatcher.AddEventListener<RedEnvelopeInfo>(ChatEvents.GRAB_RED_ENVELOPE, OnGrabRedEnvelope);
            EventDispatcher.AddEventListener<ulong, string>(ChatEvents.ON_CLICK_RESPONSE_PRIVATE_PLAYER, OnResponsePrivatePlayer);
            EventDispatcher.AddEventListener<string>(ChatEvents.CHANGE_PRIVATE_PLAYER_NAME, OnChangePrivatePlayerName);
            EventDispatcher.AddEventListener<ChatQuestionInfo>(ChatEvents.START_CHAT_QUESTION, OnStartChatQuestion);
            EventDispatcher.AddEventListener(ChatEvents.END_CHAT_QUESTION, OnEndChatQuestion);
        }

        private void RemoveEventListener()
        {
            _channelToggleGroup.onSelectedIndexChanged.RemoveListener(OnChannelChanged);
            _privateChatBackBtn.onClick.RemoveListener(OnPrivateChannelBtnClick);
            EventDispatcher.RemoveEventListener(ChatEvents.OPEN_EMOJI, OnOpenEmoji);
            EventDispatcher.RemoveEventListener(ChatEvents.CLOSE_EMOJI, OnCloseEmoji);
            EventDispatcher.RemoveEventListener(ChatEvents.HIDE_INPUT, OnHideInput);
            EventDispatcher.RemoveEventListener(ChatEvents.SHOW_INPUT, OnShowInput);
            EventDispatcher.RemoveEventListener<int, PbChatInfoResp>(ChatEvents.RECEIVE_CONTENT, OnReceiveContent);
            EventDispatcher.RemoveEventListener<int>(ChatEvents.ReadContent, OnReadContent);
            EventDispatcher.RemoveEventListener(ChatEvents.ENABLE_PRIVATE_CHAT_BACK_BTN, OnEnablePrivateChatBackBtn);
            EventDispatcher.RemoveEventListener<UInt64>(ChatEvents.SELECT_FRIEND, OnRefreshPrivateChannelPoint);
            EventDispatcher.RemoveEventListener(ChatEvents.QUIT_TEAM, OnUpdateNewMsgFlagQuitTeam);
            EventDispatcher.RemoveEventListener(ChatEvents.LEAVE_GUILD, OnUpdateNewMsgFlagLeaveGuild);
            EventDispatcher.RemoveEventListener<RedEnvelopeInfo>(ChatEvents.GRAB_RED_ENVELOPE, OnGrabRedEnvelope);
            EventDispatcher.RemoveEventListener<ulong, string>(ChatEvents.ON_CLICK_RESPONSE_PRIVATE_PLAYER, OnResponsePrivatePlayer);
            EventDispatcher.RemoveEventListener<string>(ChatEvents.CHANGE_PRIVATE_PLAYER_NAME, OnChangePrivatePlayerName);
            EventDispatcher.RemoveEventListener<ChatQuestionInfo>(ChatEvents.START_CHAT_QUESTION, OnStartChatQuestion);
            EventDispatcher.RemoveEventListener(ChatEvents.END_CHAT_QUESTION, OnEndChatQuestion);
        }

        protected override void OnCloseBtnClick()
        {
            ChatPanelFadeIn.BeginHide(gameObject);
        }

        private void OnChannelChanged(KToggleGroup target, int index)
        {
            if (_curSelectIndex == public_config.CHANNEL_ID_PRIVATE && index != public_config.CHANNEL_ID_PRIVATE)
            {
                _inputView.UpdateKeyboardPrivateInputInfo();
            }
            _curSelectIndex = index;
            RefreshInputView(index);
            SetPrivateBackBtnVisible(false);
            OnCloseEmoji();
            int channel = GetContentChannel(index);
            ShowChannelContent(channel);
            RefreshTogglePointByIndex(index);
            RefreshChatQuestionView(channel);
        }

        private void ShowChannelContent(int channel)
        {
            if (channel != public_config.CHANNEL_ID_SYSTEM && channel != public_config.CHANNEL_ID_HELP)
            {
                _inputView.Visible = true;
            }
            else
            {
                _inputView.Visible = false;
            }
            switch(channel)
            {
                case public_config.CHANNEL_ID_WORLD:
                    ShowWorldChannel();
                    break;
                case public_config.CHANNEL_ID_VICINITY:
                    ShowVicinityChannel();
                    break;
                case public_config.CHANNEL_ID_ALL:
                    ShowAllChannel();
                    break;
                case public_config.CHANNEL_ID_TEAM:
                    ShowTeamChannel();
                    break;
                case public_config.CHANNEL_ID_GUILD:
                    ShowGuildChannel();
                    break;
                case public_config.CHANNEL_ID_PRIVATE:
                    ShowPrivateChannel();
                    break;
                case public_config.CHANNEL_ID_HELP:
                    ShowTipsChannel();
                    break;
                case public_config.CHANNEL_ID_SYSTEM:
                    ShowSystemChannel();
                    break;
            }
        }

        private void ShowAllChannel()
        {
            RefreshCurrentView(_allChannelView);
        }

        private void ShowWorldChannel()
        {
            RefreshCurrentView(_worldChannelView);
        }

        private void ShowVicinityChannel()
        {
            RefreshCurrentView(_systemChannelView);
        }

        public void ShowTeamChannel()
        {
            ChatData chatData = PlayerDataManager.Instance.ChatData;
            RefreshCurrentView(_teamChannelView);
        }

        public void ShowGuildChannel()
        {
            if (PlayerAvatar.Player.guild_id != 0)
            {
                RefreshCurrentView(_guildChannelView);
            }
            else
            {
                RefreshCurrentView(_guildListView);
            }
        }

        private void ShowPrivateChannel()
        {
            RefreshCurrentView(_privateView);
            SetPrivateBackBtnVisible(false);
            _privateView.ShowChannelContentView();
        }

        private void ShowTipsChannel()
        {
            RefreshCurrentView(_tipsChannelView);
        }

        private void ShowSystemChannel()
        {
            RefreshCurrentView(_systemChannelView);
        }

        private void SetPrivateBackBtnVisible(bool isVisible)
        {
            if (_privateChatBackBtn.Visible != isVisible)
            {
                _privateChatBackBtn.Visible = isVisible;
            }
        }

        private void OnPrivateChannelBtnClick()
        {
            //更新密聊界面为最近聊天玩家列表
            ShowPrivateChannel();
            OnCloseEmoji();
        }

        private void RefreshCurrentView(KContainer view)
        {
            if (_currentView == view)
            {
                return;
            }
            if(_currentView != null)
            {
                _currentView.Visible = false;
            }
            _currentView = view;
            ChatChannelContentView channelView = view as ChatChannelContentView;
            if (channelView != null)
            {
                channelView.CheckIsExsitScrollViewGo();
            }
            _currentView.Visible = true;
        }

        private void RefreshTogglePoint()
        {
            _chatData.ProcessNewMsgFlagNotInTeam();
            _chatData.ProcessNewMsgFlagNotInGuild();
            for (int i = 0; i < _channelToggleGroup.GetToggleList().Count; i++)
            {
                RefreshTogglePointByIndex(i);
            }
        }

        private void RefreshTogglePointByIndex(int index)
        {
            int channel = GetContentChannel(index);
            if (channel == public_config.CHANNEL_ID_TEAM || channel == public_config.CHANNEL_ID_GUILD || channel == public_config.CHANNEL_ID_PRIVATE)
            {
                StateImage pointImg = _channelToggleGroup[index].GetChildComponent<StateImage>("xiaodian");
                if (_channelToggleGroup.SelectIndex == index && channel != public_config.CHANNEL_ID_PRIVATE)
                {
                    pointImg.Visible = false;
                }
                else
                {
                    pointImg.Visible = _chatData.CheckChannelHasNewMsg(channel);
                    if (channel == public_config.CHANNEL_ID_PRIVATE)
                    {
                        StateImage privateBackBtnPoint = _privateChatBackBtn.GetChildComponent<StateImage>("xiaodian");
                        privateBackBtnPoint.Visible = _chatData.CheckChannelHasNewMsg(channel);
                    }
                }
            }
        }

        private void OnOpenEmoji()
        {
            if (_convenientMenuView.Visible == false)
            {
                _convenientMenuView.Visible = true;
                _convenientMenuView.Refresh();
            }
            else
            {
                ChatPopupViewFadeIn.BeginHide(_convenientMenuView.gameObject);
            }
        }

        private void OnCloseEmoji()
        {
            if (_convenientMenuView.Visible == true)
            {
                _convenientMenuView.Visible = false;
            }
        }

        private void OnHideInput()
        {
            _inputView.Visible = false;
        }

        private void OnShowInput()
        {
            _inputView.Visible = true;
        }

        private void OnReceiveContent(int channelId, PbChatInfoResp chatInfo)
        {
            RefreshTogglePointByIndex(channelId);
        }

        private void OnReadContent(int channelId)
        {
            RefreshTogglePointByIndex(channelId);
        }

        private void OnEnablePrivateChatBackBtn()
        {
            SetPrivateBackBtnVisible(true);
        }

        private void OnRefreshPrivateChannelPoint(UInt64 playerDbid)
        {
            RefreshTogglePointByIndex(_channelToggleGroup.GetToggleList().Count - 1);
        }

        private void OnUpdateNewMsgFlagQuitTeam()
        {
            _chatData.ProcessNewMsgFlagNotInTeam();
        }

        private void OnUpdateNewMsgFlagLeaveGuild()
        {
            _chatData.ProcessNewMsgFlagNotInGuild();
        }

        private void OnGrabRedEnvelope(RedEnvelopeInfo envelopeInfo)
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.RedEnvelope, envelopeInfo);
        }

        private void OnResponsePrivatePlayer(ulong playerDbid, string playerName)
        {
            if (_avatarInfo == null)
            {
                _avatarInfo = new PbFriendAvatarInfo();
            }
            _avatarInfo.dbid = playerDbid;
            _avatarInfo.name = playerName;
            _channelToggleGroup.SelectIndex = 5;
            _inputView.RefreshPrivate(public_config.CHANNEL_ID_PRIVATE, _avatarInfo);
            DoShowContent();
            _curSelectIndex = _channelToggleGroup.SelectIndex;
        }

        private void OnChangePrivatePlayerName(string playerName)
        {
            if (_avatarInfo == null)
            {
                _avatarInfo = new PbFriendAvatarInfo();
            }
            _avatarInfo.dbid = 0;
            _avatarInfo.name = playerName;
        }

        private void OnStartChatQuestion(ChatQuestionInfo questionInfo)
        {
            int channel = GetContentChannel(_channelToggleGroup.SelectIndex);
            if (channel == public_config.CHANNEL_ID_ALL || channel == public_config.CHANNEL_ID_WORLD)
            {
                _chatQuestionView.Visible = true;
                _chatQuestionView.Refresh(questionInfo);
            }
            else
            {
                _chatQuestionView.Visible = false;
            }
        }

        private void OnEndChatQuestion()
        {
            _chatQuestionView.Visible = false;
            _chatQuestionView.ResetQuestionInfo();
        }

        public override void OnShow(object data)
        {
            //处理当已打开聊天界面，点击玩家头像弹出角色信息tips，点击里面的开始聊天，进入密聊后需要刷新内容的情况
            //由于已打开该界面，当再次ShowPanel时，只调用SetData
            _isExecuteOnShow = true;

            //先监听事件，防止有事件触发时，没有监听
            AddEventListener();
            ResetForceMoveContentToBottom();
            SetData(data);
            DoShowContent();
            _isExecuteOnShow = false;
        }

        private void DoShowContent()
        {
            if (_isExecuteOnShow)
            {
                ChatPanelFadeIn.BeginShow(gameObject);
            }
            SetPrivateBackBtnVisible(false);
            int channel = GetContentChannel(_channelToggleGroup.SelectIndex);
            ShowChannelContent(channel);
            RefreshTogglePoint();
            RefreshChatQuestionView(channel);
            if (_convenientMenuView.Visible == true)
            {
                _convenientMenuView.Visible = false;
            }
        }

        private void ResetForceMoveContentToBottom()
        {
            SetChannelContentForceMoveToBottom(_allChannelView);
            SetChannelContentForceMoveToBottom(_worldChannelView);
            SetChannelContentForceMoveToBottom(_systemChannelView);
            SetChannelContentForceMoveToBottom(_teamChannelView);
            SetChannelContentForceMoveToBottom(_guildChannelView);
            SetChannelContentForceMoveToBottom(_privateView.PrivateContentView);
            SetChannelContentForceMoveToBottom(_tipsChannelView);
        }

        private void SetChannelContentForceMoveToBottom(ChatChannelContentView contentView)
        {
            if (contentView == null)
                return;
            contentView.IsForceContentMoveBottom = true;
        }

        public override void SetData(object data)
        {
            _channelToggleGroup.SelectIndex = 0;
            if(data is ChatItemLinkWrapper)
            {
                _channelToggleGroup.SelectIndex = _curSelectIndex;
                RefreshInputView(_channelToggleGroup.SelectIndex);
                _inputView.AddItemLink(data as ChatItemLinkWrapper);
                if (_isExecuteOnShow == false)
                {
                    DoShowContent();
                }
                return;
            }
            if(data is PbFriendAvatarInfo)
            {
                PbFriendAvatarInfo avatarInfo = data as PbFriendAvatarInfo;
                _avatarInfo = avatarInfo;
                _channelToggleGroup.SelectIndex = 5;
                RefreshInputView(_channelToggleGroup.SelectIndex);
                if (_isExecuteOnShow == false)
                {
                    DoShowContent();
                }
                _curSelectIndex = _channelToggleGroup.SelectIndex;
                return;
            }
            if (data is int)
            {
                _channelToggleGroup.SelectIndex = (int)data;
                RefreshInputView(_channelToggleGroup.SelectIndex);
                if (_isExecuteOnShow == false)
                {
                    DoShowContent();
                }
                _curSelectIndex = _channelToggleGroup.SelectIndex;
                return;
            }
            if(string.IsNullOrEmpty((string)data) == false)
            {
                _inputView.SetContent((string)data);
            }
            _channelToggleGroup.SelectIndex = _curSelectIndex;
            RefreshInputView(_channelToggleGroup.SelectIndex);
            if (_isExecuteOnShow == false)
            {
                DoShowContent();
            }
        }

        private void RefreshInputView(int index)
        {
            int inputChannel = GetInputChannel(index);
            if (inputChannel == public_config.CHANNEL_ID_PRIVATE)
            {
                _inputView.RefreshPrivate(inputChannel, _avatarInfo);
            }
            else
            {
                _inputView.Refresh(inputChannel);
            }
        }

        private void RefreshChatQuestionView(int channel)
        {
            if (_chatData.ChatQuestionData.CurQuestionInfo != null && (channel == public_config.CHANNEL_ID_ALL || channel == public_config.CHANNEL_ID_WORLD))
            {
                _chatQuestionView.Visible = true;
                _chatQuestionView.Refresh(_chatData.ChatQuestionData.CurQuestionInfo);
            }
            else
            {
                _chatQuestionView.Visible = false;
            }
        }

        public void DoClose()
        {
            ClosePanel();
        }

        public override void OnClose()
        {
            EventDispatcher.TriggerEvent(ChatEvents.CLOSE_CHAT_PANEL);
            RemoveEventListener();
            SavePrivateChatInfo();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            ChatVoiceAnimPlayPool.Instance.Clear();
            SavePrivateChatInfo();
        }

        private void SavePrivateChatInfo()
        {
            ChatPrivateData privateData = PlayerDataManager.Instance.ChatData.ChatPrivateData;
            privateData.SavePrivateChatInfo();
        }

        private int GetInputChannel(int index)
        {
            int channel = public_config.CHANNEL_ID_WORLD;
            switch(index)
            {
                case 0:
                case 1:
                    channel = public_config.CHANNEL_ID_WORLD;
                    break;
                case 2:
                    channel = public_config.CHANNEL_ID_SYSTEM;
                    break;
                case 3:
                    channel = public_config.CHANNEL_ID_TEAM;
                    break;
                case 4:
                    channel = public_config.CHANNEL_ID_GUILD;
                    break;
                case 5:
                    channel = public_config.CHANNEL_ID_PRIVATE;
                    break;
                case 6:
                    channel = public_config.CHANNEL_ID_HELP;
                    break;
            }
            return channel;
        }

        private int GetContentChannel(int index)
        {
            int channel = public_config.CHANNEL_ID_ALL;
            switch(index)
            {
                case 0:
                    channel = public_config.CHANNEL_ID_ALL;
                    break;
                case 1:
                    channel = public_config.CHANNEL_ID_WORLD;
                    break;
                case 2:
                    channel = public_config.CHANNEL_ID_SYSTEM;
                    break;
                case 3:
                    channel = public_config.CHANNEL_ID_TEAM;
                    break;
                case 4:
                    channel = public_config.CHANNEL_ID_GUILD;
                    break;
                case 5:
                    channel = public_config.CHANNEL_ID_PRIVATE;
                    break;
                case 6:
                    channel = public_config.CHANNEL_ID_HELP;
                    break;
            }
            return channel;
        }

    }
}
