﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using Common.Data.Chat;

namespace ModuleChat
{
    public class ChatPhraseEditPanel : BasePanel
    {
        private const int PHRASE_COUNT = 6;
        private List<KInputField> _phraseList = new List<KInputField>();
        private KButton _saveBtn;
        private KButton _restoreBtn;
        private ChatPhraseData _phraseData;
        private bool _isClickRestore = false;

        protected override void Awake()
        {
            base.Awake();
            gameObject.AddComponent<KDummyButton>();
            _phraseData = PlayerDataManager.Instance.ChatData.ChatPhraseData;
            CloseBtn = GetChildComponent<KButton>("Container_BG/Button_close");
            for(int i = 0; i < PHRASE_COUNT; i++)
            {
                _phraseList.Add(GetChildComponent<KInputField>("Input_label" + i));
            }
            _saveBtn = GetChildComponent<KButton>("Button_baocun");
            _restoreBtn = GetChildComponent<KButton>("Button_moren");
            AddEventListener();
        }

        protected override PanelIdEnum Id
        {
            get 
            {
                return PanelIdEnum.ChatPhraseEdit;
            }
        }

        private void AddEventListener()
        {
            _saveBtn.onClick.AddListener(OnSaveClick);
            _restoreBtn.onClick.AddListener(OnRestoreClick);
        }

        private void OnSaveClick()
        {
            for(int i = 0; i < PHRASE_COUNT; i++)
            {
                _phraseData.SetPhrase(i, _phraseList[i].text);
            }
            _phraseData.WriteToLocal();
            _isClickRestore = false;
            ClosePanel();
            UIManager.Instance.ShowPanel(PanelIdEnum.Chat);
        }

        private void OnRestoreClick()
        {
            _phraseData.ResetPhrase();
            _isClickRestore = true;
            InitPhraseList();
        }

        protected override void OnCloseBtnClick()
        {
            ClosePanel();
            UIManager.Instance.ShowPanel(PanelIdEnum.Chat);
        }

        private void InitPhraseList()
        {
            for(int i = 0; i < _phraseList.Count; i++)
            {
                _phraseList[i].text = _phraseData.GetPhrase(i);
            }
        }

        public override void OnShow(object data)
        {
            InitPhraseList();
        }

        public override void OnClose()
        {
            if (_isClickRestore)
            {
                _phraseData.ReadFromLocal();
                _isClickRestore = false;
            }
            EventDispatcher.TriggerEvent(ChatEvents.END_EDIT_PHRASE);
        }
    }
}
