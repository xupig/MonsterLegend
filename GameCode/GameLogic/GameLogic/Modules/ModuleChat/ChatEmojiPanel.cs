﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using Common.Chat;

namespace ModuleChat
{
    public class ChatEmojiPanel : BasePanel
    {

        private KButton _returnBtn;
        private KButton _sendBtn;
        private KList _emojiList;
        private bool _isInitEmojiList;

        protected override PanelIdEnum Id
        {
            get 
            {
                return PanelIdEnum.ChatEmoji;
            }
        }

        protected override void Awake()
        {
            _returnBtn = GetChildComponent<KButton>("Button_fanhui");
            _sendBtn = GetChildComponent<KButton>("Button_fasong");
            _emojiList = GetChildComponent<KList>("List_emoji");
            _emojiList.SetPadding(10, 10, 10, 10);
            _emojiList.SetGap(20, 20);
            _emojiList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue);
            AddEventListener();
        }

        private void AddEventListener()
        {
            _returnBtn.onClick.AddListener(OnReturnClick);
            _sendBtn.onClick.AddListener(OnSendClick);
            _emojiList.onSelectedIndexChanged.AddListener(OnEmojiSelected);
        }

        private void RemoveEventListener()
        {
            _returnBtn.onClick.RemoveListener(OnReturnClick);
            _sendBtn.onClick.RemoveListener(OnSendClick);
            _emojiList.onSelectedIndexChanged.RemoveListener(OnEmojiSelected);
        }

        private void OnReturnClick()
        {
            ClosePanel();
        }

        private void OnSendClick()
        {
            EventDispatcher.TriggerEvent(ChatEvents.SEND_FORM_MENU);
        }

        private void OnEmojiSelected(KList target, int index)
        {
            _emojiList.SelectedIndex = -1;
            EventDispatcher.TriggerEvent<int>(ChatEvents.SELECT_EMOJI, index);
        }

        private void InitEmojiList()
        {
            List<string> nameList = ChatEmojiDefine.GetEmojiFirstSpriteNameList();
            for(int i = 0; i < nameList.Count; i++)
            {
                _emojiList.AddItem<ChatEmojiItem>(nameList[i], false);
            }
            _emojiList.DoLayout();
        }

        public override void OnShow(object data)
        {
            if(_isInitEmojiList == false)
            {
                _isInitEmojiList = true;
                InitEmojiList();
            }
        }

        public override void OnClose()
        {
            
        }
    }
}
