﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using UnityEngine.UI;
using Common.Utils;

namespace ModuleChat
{
    public class ChatReportPanel : BasePanel
    {
        private StateText _nameTxt;
        private StateText _idTxt;
        private StateText _detailTxt;
        private KButton _submitBtn;
        private KInputField _additionDescInput;
        private string _nameTemplate;
        private string _idTemplate;
        private string _additionInputTips;

        private List<KToggle> _toggleList = new List<KToggle>();

        protected override void Awake()
        {
            base.Awake();
            gameObject.AddComponent<KDummyButton>();
            CloseBtn = GetChildComponent<KButton>("Container_BG/Button_close");
            _nameTxt = GetChildComponent<StateText>("Label_txtName");
            _nameTemplate = _nameTxt.CurrentText.text;
            _idTxt = GetChildComponent<StateText>("Label_txtId");
            _idTemplate = _idTxt.CurrentText.text;
            _detailTxt = GetChildComponent<StateText>("Label_txtYuanyin");
            _detailTxt.CurrentText.text = string.Empty;
            _submitBtn = GetChildComponent<KButton>("Button_tijiao");
            _additionDescInput = GetChildComponent<KInputField>("Input_xiangqing");
            _additionInputTips = MogoLanguageUtil.GetContent(32556);
            _additionDescInput.text = _additionInputTips;
            for (int i = 0; i < 4; i++)
            {
                _toggleList.Add(GetChildComponent<KToggle>("Toggle_Xuanze" + i));
            }
        }

        protected override PanelIdEnum Id
        {
            get 
            {
                return PanelIdEnum.ChatReport;
            }
        }

        private void AddEventListener()
        {
            _submitBtn.onClick.AddListener(OnSubmitClick);
        }

        private void RemoveEventListener()
        {
            _submitBtn.onClick.RemoveListener(OnSubmitClick);
        }

        private void OnSubmitClick()
        {
            ClosePanel();
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void SetData(object data)
        {
            PbFriendAvatarInfo avatarInfo = data as PbFriendAvatarInfo;
            if(avatarInfo != null)
            {
                _nameTxt.CurrentText.text = string.Format(_nameTemplate, avatarInfo.name);
                _idTxt.CurrentText.text = string.Format(_idTemplate, avatarInfo.dbid.ToString());
                _detailTxt.CurrentText.text = string.Empty;
                ResetToggleList();
            }
        }

        private void ResetToggleList()
        {
            for(int i = 0; i < _toggleList.Count; i++)
            {
                _toggleList[i].isOn = false;
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }
    }
}
