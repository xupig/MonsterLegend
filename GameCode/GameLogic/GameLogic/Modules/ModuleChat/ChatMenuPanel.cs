﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using Common.Data.Chat;
using UnityEngine.EventSystems;

namespace ModuleChat
{
    public class ChatMenuPanel : BasePanel
    {
        private const int PHRASE_COUNT = 6;
        private KButton _openEditBtn;
        private List<KButton> _phraseBtnList = new List<KButton>();
        private KButton _sendBtn;
        private KButton _backspaceBtn;
        private KButton _bagBtn;

        protected override void Awake()
        {
            base.Awake();
            gameObject.AddComponent<KDummyButton>();
            CloseBtn = GetChildComponent<KButton>("Button_fanhui");
            _openEditBtn = GetChildComponent<KButton>("Container_changyongyu/Button_shuxie");
            _sendBtn = GetChildComponent<KButton>("Button_fasong");
            _backspaceBtn = GetChildComponent<KButton>("Button_shanchu");
            //_bagBtn = GetChildComponent<KButton>("Container_lianjie/Button_beibao");
            for(int i = 0; i < PHRASE_COUNT; i++)
            {
                _phraseBtnList.Add(GetChildComponent<KButton>("Container_changyongyu/Button_btn" + i));
            }
            AddEventListener();
        }

        protected override PanelIdEnum Id
        {
            get
            {
                return PanelIdEnum.ChatMenu;
            }
        }

        private void AddEventListener()
        {
            _openEditBtn.onClick.AddListener(OnEditClick);
            for(int i = 0; i < _phraseBtnList.Count; i++)
            {
                _phraseBtnList[i].onPointerUp.AddListener(OnPhraseClick);
            }
            _sendBtn.onClick.AddListener(OnSendClick);
            _backspaceBtn.onClick.AddListener(OnBackspaceClick);
            //_bagBtn.onClick.AddListener(OnBagClick);
            EventDispatcher.AddEventListener(ChatEvents.END_EDIT_PHRASE, OnEndEditPhrase);
        }

        private void OnEditClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.ChatPhraseEdit);
        }

        private void OnPhraseClick(KButton target, PointerEventData evtData)
        {
            string content = target.GetChildComponent<StateText>("label").CurrentText.text;
            EventDispatcher.TriggerEvent<string>(ChatEvents.SELECT_PHRASE, content);
        }

        private void OnSendClick()
        {
            EventDispatcher.TriggerEvent(ChatEvents.SEND_FORM_MENU);
        }

        private void OnBackspaceClick()
        {

        }

        private void OnBagClick()
        {
            ClosePanel();
            UIManager.Instance.ShowPanel(PanelIdEnum.Bag);
        }

        private void OnEndEditPhrase()
        {
            InitPhraseBtnListLabel();
        }

        public override void OnShow(object data)
        {
            InitPhraseBtnListLabel();
        }

        private void InitPhraseBtnListLabel()
        {
            ChatPhraseData phraseData = PlayerDataManager.Instance.ChatData.ChatPhraseData;
            for(int i = 0; i < _phraseBtnList.Count; i++)
            {
                _phraseBtnList[i].GetChildComponent<StateText>("label").ChangeAllStateText(phraseData.GetPhrase(i));
            }
        }

        public override void OnClose()
        {
            
        }
    }
}
