﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Common.ExtendComponent;
using GameMain.GlobalManager;
using GameLoader.Utils.Timer;

namespace ModuleChat
{
    //在屏幕内随机位置生成特效，并进行播放，一定时间间隔内生成一个随机位置的特效
    //总共持续一定时间
    public class RedEnvelopeOpenEffect : MonoBehaviour
    {
        private KParticle _templateParticle;
        private Queue<KParticle> _cachedParticleQueue = new Queue<KParticle>();
        private Queue<KParticle> _playingParticleQueue = new Queue<KParticle>();
        private Queue<Vector2> _createScreenPosQueue = new Queue<Vector2>();

        private float _duration = 3f;  //单位s
        private float _playParticleTimeInterval = 0.2f;
        private const int SEGMENT_NUM = 10;
        private int _perSegmentWidth = 0;
        private int _perSegmentHeight = 0;
        private uint _endEffectTimerId = 0;
        private uint _particlePlayIntervalTimerId = 0;
        private int _curParticleId = 0;

        public void Init(KParticle templateParticle)
        {
            _templateParticle = templateParticle;
            _templateParticle.Stop();
            _perSegmentWidth = Mathf.FloorToInt(UIManager.CANVAS_WIDTH / (float)SEGMENT_NUM);
            _perSegmentHeight = Mathf.FloorToInt(UIManager.CANVAS_HEIGHT / (float)SEGMENT_NUM);
        }

        public void StartPlay()
        {
            if (_endEffectTimerId == 0)
            {
                _endEffectTimerId = TimerHeap.AddTimer((uint)(_duration * 1000), 0, OnEndEffect);
            }
            GetParticleToPlay();
            if (_particlePlayIntervalTimerId == 0)
            {
                _particlePlayIntervalTimerId = TimerHeap.AddTimer((uint)(_playParticleTimeInterval * 1000), (int)(_playParticleTimeInterval * 1000), OnPlayNextParticle);
            }
        }

        public void StopPlay()
        {
            StopParticlePlayTimer();
            StopEndEffectTimer();
            while (_playingParticleQueue.Count > 0)
            {
                KParticle playParticle = _playingParticleQueue.Dequeue();
                playParticle.Stop();
                _cachedParticleQueue.Enqueue(playParticle);
            }
            _createScreenPosQueue.Clear();
        }

        private void GetParticleToPlay()
        {
            if (_cachedParticleQueue.Count == 0)
            {
                KParticle cloneParticle = CloneParticle();
                _cachedParticleQueue.Enqueue(cloneParticle);
            }
            //生成随机位置
            //播放特效
            Vector3 destPos = CreateRandomPosition();
            KParticle particle = _cachedParticleQueue.Dequeue();
            particle.Play();
            //要在play之后设置位置，因为首次创建新的特效时，先设置了位置再Play，设置的位置可能会无效
            particle.transform.localPosition = destPos;
            _playingParticleQueue.Enqueue(particle);
        }

        private KParticle CloneParticle()
        {
            GameObject cloneParticleGo = GameObject.Instantiate(_templateParticle.gameObject) as GameObject;
            cloneParticleGo.SetActive(false);
            cloneParticleGo.transform.SetParent(this.transform);
            cloneParticleGo.transform.localScale = Vector3.one;
            cloneParticleGo.transform.localPosition = Vector3.zero;
            _curParticleId++;
            string name = _templateParticle.gameObject.name + "_" + _curParticleId.ToString();
            cloneParticleGo.name = name;
            KParticle particle = cloneParticleGo.GetComponent<KParticle>();
            particle.onComplete.AddListener(OnParticlePlayEnded);
            return particle;
        }

        private Vector3 CreateRandomPosition()
        {
            int widthIndex = UnityEngine.Random.Range(0, SEGMENT_NUM - 1);
            int heightIndex = UnityEngine.Random.Range(0, SEGMENT_NUM - 1);
            Vector2 screenPos = new Vector2((widthIndex + 1) * _perSegmentWidth, -(heightIndex + 1) * _perSegmentHeight);
            if (_createScreenPosQueue.Count > 0)
            {
                Vector2[] screenPosArray = _createScreenPosQueue.ToArray();
                _createScreenPosQueue.Enqueue(screenPos);
                for (int i = 0; i < screenPosArray.Length; i++)
                {
                    Vector2 pos = screenPosArray[i];
                    if ((int)screenPos.x == (int)pos.x && (int)screenPos.y == (int)pos.y)
                    {
                        screenPos.x += 10;
                        screenPos.y += 10;
                        break;
                    }
                }
            }
            else
            {
                _createScreenPosQueue.Enqueue(screenPos);
            }
            Vector2 localPos = screenPos;
            RectTransform rect = this.gameObject.GetComponent<RectTransform>();
            localPos.x -= rect.anchoredPosition.x;
            localPos.y -= rect.anchoredPosition.y;
            Vector3 destPos = new Vector3(localPos.x, localPos.y, 0f);
            return destPos;
        }

        private void OnParticlePlayEnded()
        {
            KParticle endPlayParticle = _playingParticleQueue.Dequeue();
            _cachedParticleQueue.Enqueue(endPlayParticle);
            _createScreenPosQueue.Dequeue();
        }

        private void OnEndEffect()
        {
            StopParticlePlayTimer();
            StopEndEffectTimer();
        }

        private void OnPlayNextParticle()
        {
            GetParticleToPlay();
        }

        private void StopEndEffectTimer()
        {
            if (_endEffectTimerId != 0)
            {
                TimerHeap.DelTimer(_endEffectTimerId);
                _endEffectTimerId = 0;
            }
        }

        private void StopParticlePlayTimer()
        {
            if (_particlePlayIntervalTimerId != 0)
            {
                TimerHeap.DelTimer(_particlePlayIntervalTimerId);
                _particlePlayIntervalTimerId = 0;
            }
        }

        private void OnDestroy()
        {
            StopParticlePlayTimer();
            StopEndEffectTimer();
            _cachedParticleQueue.Clear();
            while (_playingParticleQueue.Count > 0)
            {
                KParticle playParticle = _playingParticleQueue.Dequeue();
                playParticle.Stop();
            }
            _createScreenPosQueue.Clear();
        }
    }
}
