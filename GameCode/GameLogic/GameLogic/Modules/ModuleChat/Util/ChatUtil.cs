﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Structs.ProtoBuf;
using GameMain.Entities;
using GameMain.GlobalManager;
using UnityEngine;
using Common.Utils;

namespace ModuleChat
{
    public class ChatUtil
    {
        public static UInt64 GetFriendDbid(PbChatInfoResp chatInfo)
        {
            if(chatInfo.send_dbid == PlayerAvatar.Player.dbid)
            {
                return chatInfo.accept_dbid;
            }
            return chatInfo.send_dbid;
        }

        public static uint GetFriendLevel(PbChatInfoResp chatInfo)
        {
            if(chatInfo.send_dbid == PlayerAvatar.Player.dbid)
            {
                return chatInfo.accept_level;
            }
            return chatInfo.send_level;
        }

        public static string GetFriendName(PbChatInfoResp chatInfo)
        {
            if(chatInfo.send_dbid == PlayerAvatar.Player.dbid)
            {
                return chatInfo.accept_name;
            }
            return chatInfo.send_name;
        }

        public static uint GetFriendVocation(PbChatInfoResp chatInfo)
        {
            if(chatInfo.send_dbid == PlayerAvatar.Player.dbid)
            {
                return chatInfo.accept_vocation;
            }
            return chatInfo.send_vocation;
        }

        public static void SetPopupViewParentToMask(GameObject maskGo,  GameObject viewGo)
        {
            UIUtils.SetWidgetParentWithOriginalPos(viewGo, maskGo, UIManager.Instance.UICamera);
        }

    }
}
