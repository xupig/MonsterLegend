﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using UnityEngine;
using MogoEngine.Events;
using Common.Events;

namespace ModuleChat
{
    public class ChatPanelFadeIn : KContainer
    {
        private RectTransform _rect;
        private Vector2 _showPos = Vector2.zero;
        private Vector2 _hidePos = Vector2.zero;
        private TweenPosition _tweener;

        private bool _isFadingIn = false;

        protected override void Awake()
        {
            _rect = transform as RectTransform;
            _showPos = new Vector2(UIManager.PANEL_WIDTH / 2.0f, _rect.anchoredPosition.y);
            KContainer chatBg = GetChildComponent<KContainer>("Container_chatBg");
            RectTransform bgRect = chatBg.gameObject.GetComponent<RectTransform>();
            _hidePos = new Vector2(_showPos.x - bgRect.sizeDelta.x, _rect.anchoredPosition.y);
            _tweener = gameObject.AddComponent<TweenPosition>();
            _tweener.enabled = false;
        }

        private void FadeIn()
        {
            if (_isFadingIn)
                return;
            if (gameObject.activeInHierarchy)
            {
                _isFadingIn = true;
                _rect.anchoredPosition = _hidePos;
                _tweener = TweenPosition.Begin(gameObject, 0.2f, _showPos, 0f, UITweener.Method.EaseIn, OnShowTweenFinished);
            }
        }

        private void FadeOut()
        {
            if (_isFadingIn)
                return;
            _tweener = TweenPosition.Begin(gameObject, 0.2f, _hidePos, 0f, UITweener.Method.EaseOut, OnHideTweenFinished);
        }

        private void OnShowTweenFinished(UITweener tweener)
        {
            _isFadingIn = false;
            EventDispatcher.TriggerEvent(ChatEvents.OPEN_CHAT_PANEL);
        }

        private void OnHideTweenFinished(UITweener tweener)
        {
            ChatPanel panel = gameObject.GetComponent<ChatPanel>();
            panel.DoClose();
        }

        public static ChatPanelFadeIn BeginShow(GameObject go)
        {
            ChatPanelFadeIn comp = go.GetComponent<ChatPanelFadeIn>();
            if (comp == null)
            {
                comp = go.AddComponent<ChatPanelFadeIn>();
            }
            comp.FadeIn();
            return comp;
        }

        public static ChatPanelFadeIn BeginHide(GameObject go)
        {
            ChatPanelFadeIn comp = go.GetComponent<ChatPanelFadeIn>();
            if (comp == null)
            {
                comp = go.AddComponent<ChatPanelFadeIn>();
            }
            comp.FadeOut();
            return comp;
        }
    }
}
