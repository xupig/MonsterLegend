﻿using System;
using System.Collections.Generic;
using Common.Events;
using Game.UI.UIComponent;
using GameLoader.Utils;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Data;
using GameMain.Entities;
using GameLoader.Utils.Timer;

namespace ModuleChat
{
    public class ChatVoiceAgent : KContainer
    {
        private Dictionary<string, ChatVoiceInputView> _voiceInputViewDict;
        private ChatVoiceNoticeView _voiceNoticeView;

        private uint _timerId = 0;
        private ChatData _chatData;

        protected override void Awake()
        {
            _voiceInputViewDict = new Dictionary<string, ChatVoiceInputView>();
            _chatData = PlayerDataManager.Instance.ChatData;
        }

        public void InitView(ChatVoiceNoticeView voiceNoticeView, List<ChatVoiceInputView> voiceInputViewList)
        {
            _voiceNoticeView = voiceNoticeView;
            for (int i = 0; i < voiceInputViewList.Count; i++)
            {
                _voiceInputViewDict.Add(voiceInputViewList[i].VoiceBtnName, voiceInputViewList[i]);
            }
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(ChatEvents.SDK_START_TALK, OnStartVoiceTalk);
            EventDispatcher.AddEventListener<string>(ChatEvents.SDK_VOICE_MESSAGE, OnConvertVoiceMessage);
            EventDispatcher.AddEventListener<int>(ChatEvents.SDK_VOICE_VOLUME, OnVoiceVolumeChange);
            EventDispatcher.AddEventListener<string, int>(ChatEvents.SDK_END_TALK, OnEndVoiceTalk);
            EventDispatcher.AddEventListener<string>(ChatEvents.SDK_TALK_ERROR, OnShowVoiceTalkError);
            EventDispatcher.AddEventListener(ChatEvents.SHOW_VOICE_NOTICE_VIEW, OnShowVoiceNoticeView);
            EventDispatcher.AddEventListener<string, ulong>(ChatEvents.PLAY_VOICE, OnPlayVoice);
            EventDispatcher.AddEventListener<string>(ChatEvents.SDK_START_PLAY_VOICE, OnStartPlayVoice);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(ChatEvents.SDK_START_TALK, OnStartVoiceTalk);
            EventDispatcher.RemoveEventListener<string>(ChatEvents.SDK_VOICE_MESSAGE, OnConvertVoiceMessage);
            EventDispatcher.RemoveEventListener<int>(ChatEvents.SDK_VOICE_VOLUME, OnVoiceVolumeChange);
            EventDispatcher.RemoveEventListener<string, int>(ChatEvents.SDK_END_TALK, OnEndVoiceTalk);
            EventDispatcher.RemoveEventListener<string>(ChatEvents.SDK_TALK_ERROR, OnShowVoiceTalkError);
            EventDispatcher.RemoveEventListener(ChatEvents.SHOW_VOICE_NOTICE_VIEW, OnShowVoiceNoticeView);
            EventDispatcher.RemoveEventListener<string, ulong>(ChatEvents.PLAY_VOICE, OnPlayVoice);
            EventDispatcher.RemoveEventListener<string>(ChatEvents.SDK_START_PLAY_VOICE, OnStartPlayVoice);
        }

        private void OnStartVoiceTalk()
        {
            LoggerHelper.Info("[Chat]OnStartVoiceTalk");
            StopMusicAndSound();
            //显示开始录音时音量的界面
            _voiceNoticeView.ShowNotice(ChatVoiceNoticeView.VOICE_RECORD);
        }

        private void OnConvertVoiceMessage(string str)
        {
            LoggerHelper.Info("[Chat]OnConvertVoiceMessage");
            //调用inputview中的关于语音的函数
            ChatVoiceInputView voiceInputView = GetVoiceInputView();
            if (voiceInputView != null)
            {
                voiceInputView.ConcatVoiceMessage(str);
            }
        }

        private void OnVoiceVolumeChange(int volume)
        {
            //LoggerHelper.Info("[Chat]OnVoiceVolumeChange");
            //显示语音音量变化
            _voiceNoticeView.ChangeVoiceVolume(volume);
        }

        private void OnEndVoiceTalk(string path, int audioDuration)
        {
            LoggerHelper.Info("[Chat]OnEndVoiceTalk,  path = " + path + " audio duration = " + audioDuration);
            EndVoiceTalk(path, audioDuration);
            PlayMusicAndSound();
            _voiceNoticeView.HideNotice(ChatVoiceNoticeView.VOICE_RECORD);
            _voiceNoticeView.HideNotice(ChatVoiceNoticeView.CANCEL_VOICE_NOTICE);
        }

        private void OnShowVoiceTalkError(string errorId)
        {
            LoggerHelper.Info("[Chat]OnShowVoiceTalkError,  error id = " + errorId);
            VoiceManager.Instance.IsStartTalk = false;
            _chatData.IsPlayingVoice = false;
            PlayMusicAndSound();
        }

        private void OnShowVoiceNoticeView()
        {
            if (_voiceNoticeView.Visible == false)
            {
                _voiceNoticeView.Visible = true;
            }
        }

        private ChatVoiceInputView GetVoiceInputView()
        {
            if (_voiceInputViewDict.ContainsKey(_chatData.CurVoiceBtnName))
            {
                return _voiceInputViewDict[_chatData.CurVoiceBtnName];
            }
            return null;
        }

        private void EndVoiceTalk(string path, int audioDuration)
        {
            ChatVoiceInputView voiceInputView = GetVoiceInputView();
            if (voiceInputView != null)
            {
                string strVoice = voiceInputView.StrVoice;
                if (strVoice[strVoice.Length - 1] == '。')
                {
                    strVoice = strVoice.Remove(strVoice.Length - 1, 1);
                }
                string voiceText = string.Format(ChatData.VOICE_TEMPLETE, strVoice, (int)ChatLinkType.Voice, audioDuration);
                voiceInputView.OnEndVoiceTalk(voiceText, path);
                voiceInputView.ResetStrVoice();
            }
        }

        #region 控制语音播放

        //当语音发送出去后，点击语音，开始播放
        //如果是自己发的语音，可以在本地找到
        //如果是其他玩家发的，要先从服务器下载下来，再播放
        private void OnPlayVoice(string path, ulong sendDbid)
        {
            StopMusicAndSound();
            VoiceManager.Instance.OnDownloadAndPlayVoice(path);
            if (sendDbid != PlayerAvatar.Player.dbid)
            {
                ChatData chatData = PlayerDataManager.Instance.ChatData;
                if (!chatData.IsVoiceHasPlayed(path))
                {
                    chatData.SetVoiceHasPlayed(path);
                }
            }
        }

        //从SDK回调unity的PlayVoice接口中派发的消息监听
        private void OnStartPlayVoice(string path)
        {
            LoggerHelper.Info("[ChatVoiceAgent] OnStartPlayVoice, path = " + path);
            StopMusicAndSound();
            ResetTimer();
            _chatData.IsPlayingVoice = true;
            string[] splits = path.Split(new char[] { '/', '\\' });
            if (splits.Length > 0)
            {
                string voiceName = splits[splits.Length - 1];
                string voiceId = voiceName.Substring(0, voiceName.IndexOf(VoiceManager.AUDIO_extension) - 1);
                ChatData chatData = PlayerDataManager.Instance.ChatData;
                int duration = chatData.GetChatVoiceDuration(voiceId);
                if (duration > 0)
                {
                    if (_timerId == 0)
                    {
                        _timerId = TimerHeap.AddTimer((uint)duration, 0, EndPlayVoice, path);
                    }
                }
            }
        }

        //开始录音时，需停止背景音乐和音效
        public void StopMusicAndSound()
        {
            AllSoundManger.Instance.TempCloseVolume();
        }

        public void PlayMusicAndSound()
        {
            AllSoundManger.Instance.ResetTempVolume();
        }

        private void EndPlayVoice(string path)
        {
            //开启背景音乐和音效
            PlayMusicAndSound();
            ResetTimer();
            EventDispatcher.TriggerEvent<string>(ChatEvents.END_PLAY_VOICE, path);
            _chatData.IsPlayingVoice = false;
            _chatData.StartAutoPlayVoice();
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }
        #endregion

        protected override void OnDestroy()
        {
            _voiceInputViewDict.Clear();
            _voiceNoticeView = null;
        }
    }
}
