﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using UnityEngine;
using GameMain.GlobalManager;

namespace ModuleChat
{
    public class ChatPopupViewFadeIn : KContainer
    {
        private RectTransform _rect;
        private Vector2 _showPos = Vector2.zero;
        private Vector2 _hidePos = Vector2.zero;
        private float _distanceYFromChatPanel = 0f;
        private GameObject _standardPosChildGo;  //以该子物体为基准位置进行移动
        private float _maskHeight = 0f;
        private TweenPosition _tweener;

        private bool _isInit = false;
        private bool _isFadingIn = false;

        protected override void Awake()
        {
            _rect = transform as RectTransform;
            _showPos = _rect.anchoredPosition;
            _tweener = gameObject.AddComponent<TweenPosition>();
            _tweener.enabled = false;
        }

        private void FadeIn()
        {
            if (_isFadingIn)
                return;
            if (gameObject.activeSelf)
            {
                _isFadingIn = true;
                _rect.anchoredPosition = _hidePos;
                _tweener = TweenPosition.Begin(gameObject, 0.2f, _showPos, 0f, UITweener.Method.EaseIn, OnFadeInFinished);
            }
        }

        private void FadeOut()
        {
            if (_isFadingIn)
                return;
            _tweener = TweenPosition.Begin(gameObject, 0.2f, _hidePos, 0f, UITweener.Method.EaseOut, OnFadeOutFinished);
        }

        private void OnFadeInFinished(UITweener tweener)
        {
            _isFadingIn = false;
        }

        private void OnFadeOutFinished(UITweener tweener)
        {
            if (gameObject.activeSelf)
            {
                gameObject.SetActive(false);
            }
        }

        private void CalculateHidePos()
        {
            float offsetY = _distanceYFromChatPanel;
            if (_standardPosChildGo != null)
            {
                RectTransform childRect = _standardPosChildGo.GetComponent<RectTransform>();
                offsetY += -childRect.anchoredPosition.y;
            }
            float y = _maskHeight - offsetY;
            _hidePos = new Vector2(_rect.anchoredPosition.x, -y);
        }

        protected override void OnDisable()
        {
            if (_isFadingIn)
            {
                _isFadingIn = false;
            }
            if (_isInit)
            {
                _rect.anchoredPosition = _hidePos;
            }
        }

        protected override void OnDestroy()
        {
            _standardPosChildGo = null;
        }

        public static ChatPopupViewFadeIn BeginShow(GameObject go, float distanceYFromChatPanel, float maskHeight, GameObject standardPosChildGo = null)
        {
            ChatPopupViewFadeIn comp = go.GetComponent<ChatPopupViewFadeIn>();
            if (comp == null)
            {
                comp = go.AddComponent<ChatPopupViewFadeIn>();
            }
            if (!comp._isInit)
            {
                comp._distanceYFromChatPanel = distanceYFromChatPanel;
                comp._standardPosChildGo = standardPosChildGo;
                comp._maskHeight = maskHeight;
                comp.CalculateHidePos();
                comp._isInit = true;
            }
            comp.FadeIn();
            return comp;
        }

        public static ChatPopupViewFadeIn BeginHide(GameObject go)
        {
            ChatPopupViewFadeIn comp = go.GetComponent<ChatPopupViewFadeIn>();
            if (comp == null)
            {
                return null;
            }
            comp.FadeOut();
            return comp;
        }
    }
}
