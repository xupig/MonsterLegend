﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleChat
{
    public class ChatWidgetPool
    {
        private static ChatWidgetPool _instance;
        public static ChatWidgetPool Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ChatWidgetPool();
                }
                return _instance;
            }
        }

        private Dictionary<ChatWidgetTemplateId, GameObject> _widgetTemplateDict;
        private Dictionary<string, GameObject> _widgetDict;
        private uint _curId = 0;

        private ChatWidgetPool()
        {
            _widgetTemplateDict = new Dictionary<ChatWidgetTemplateId, GameObject>();
            _widgetDict = new Dictionary<string, GameObject>();
        }

        public bool AddWidgetTemplate(ChatWidgetTemplateId widgetTemplateId, GameObject widgetTemplateGo)
        {
            if (widgetTemplateGo == null)
            {
                return false;
            }
            if (!_widgetTemplateDict.ContainsKey(widgetTemplateId))
            {
                _widgetTemplateDict.Add(widgetTemplateId, widgetTemplateGo);
                return true;
            }
            return true;
        }

        public GameObject CreateWidgetGo(ChatWidgetTemplateId widgetTemplateId)
        {
            GameObject cloneGo = CloneWidgetGo(widgetTemplateId);
            if (cloneGo == null)
            {
                return null;
            }
            cloneGo.SetActive(true);

            if (_curId >= 90000)
            {
                _curId = 0;
            }
            _curId++;
            string name = widgetTemplateId.ToString() + "_" + _curId.ToString();
            cloneGo.name = name;
            //_widgetDict.Add(name, cloneGo);
            return cloneGo;
        }

        private GameObject CloneWidgetGo(ChatWidgetTemplateId widgetTemplateId)
        {
            if (_widgetTemplateDict.ContainsKey(widgetTemplateId))
            {
                return GameObject.Instantiate(_widgetTemplateDict[widgetTemplateId]) as GameObject;
            }
            return null;
        }

        //public void RemoveWidgetGo(string name)
        //{
        //    if (_widgetDict.ContainsKey(name))
        //    {
        //        _widgetDict.Remove(name);
        //    }
        //}

        public void Clear()
        {
            _widgetDict.Clear();
            _widgetTemplateDict.Clear();
            _curId = 0;
        }

        public enum ChatWidgetTemplateId
        {
            RedEnvelope = 0
        }
    }
}
