﻿using System;
using System.Collections.Generic;
using UnityEngine;
using GameLoader.Utils;

namespace ModuleChat
{
    public class ChatVoiceAnimPlayPool
    {
        private GameObject _leftVoiceAnimGo;
        private GameObject _rightVoiceAnimGo;
        private Dictionary<string, GameObject> _voiceAnimDict = new Dictionary<string, GameObject>();
        private Dictionary<VoiceAnimTemplateId, GameObject> _animTemplateDict = new Dictionary<VoiceAnimTemplateId, GameObject>();
        private uint _curId = 0;

        private static ChatVoiceAnimPlayPool _instance;
        public static ChatVoiceAnimPlayPool Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ChatVoiceAnimPlayPool();
                }
                return _instance;
            }
        }

        private ChatVoiceAnimPlayPool()
        {

        }

        public bool AddVoiceAnimTemplate(VoiceAnimTemplateId animTemplateId, GameObject animTemplateGo)
        {
            if (animTemplateGo == null)
                return false;
            if (!_animTemplateDict.ContainsKey(animTemplateId))
            {
                _animTemplateDict.Add(animTemplateId, animTemplateGo);
                return true;
            }
            return false;
        }

        public void Init(GameObject srcLeftVoiceAnimGo, GameObject srcRightVoiceAnimGo)
        {
            _leftVoiceAnimGo = srcLeftVoiceAnimGo;
            _rightVoiceAnimGo = srcRightVoiceAnimGo;
        }

        public GameObject AddAnimationGo(VoiceAnimTemplateId animTemplateId)
        {
            GameObject cloneGo = CloneGameObject(animTemplateId);
            cloneGo.SetActive(true);
            cloneGo.AddComponent<ChatVoiceAnimPlayer>();
            string name = animTemplateId.ToString() + "Anim";
            
            if (_curId < 900000)
            {
                _curId++;
            }
            else
            {
                _curId = 0;
            }
            name += "_" + _curId;
            cloneGo.name = name;
            if (_voiceAnimDict.ContainsKey(name))
            {
                LoggerHelper.Error("AddAnimationGo voice anim dict has exsit this name key !!!");
                return null;
            }
            _voiceAnimDict.Add(name, cloneGo);
            return cloneGo;
        }

        public void PlayAnim(string animGoName)
        {
            ChatVoiceAnimPlayer animPlayer = _voiceAnimDict[animGoName].GetComponent<ChatVoiceAnimPlayer>();
            animPlayer.StartPlay();
        }

        public void StopPlay(string animGoName)
        {
            ChatVoiceAnimPlayer animPlayer = _voiceAnimDict[animGoName].GetComponent<ChatVoiceAnimPlayer>();
            animPlayer.StopPlay();
        }

        public void RemoveAnimGo(string animGoName)
        {
            if (_voiceAnimDict.ContainsKey(animGoName))
            {
                _voiceAnimDict.Remove(animGoName);
            }
        }

        private GameObject CloneGameObject(VoiceAnimTemplateId animTemplateId)
        {
            GameObject go = null;
            if (_animTemplateDict.ContainsKey(animTemplateId))
            {
                go = GameObject.Instantiate(_animTemplateDict[animTemplateId]) as GameObject;
            }
            return go;
        }

        public void Clear()
        {
            _voiceAnimDict.Clear();
            _animTemplateDict.Clear();
            _leftVoiceAnimGo = null;
            _rightVoiceAnimGo = null;
        }

    }

    public enum VoiceAnimTemplateId
    {
        LeftVoice = 1,
        RightVoice,
        LoudSpeaker
    }
}
