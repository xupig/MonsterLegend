﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.ExtendComponent;
using Common.Data;
using Common.Utils;
using GameMain.GlobalManager.SubSystem;
using GameData;
using UnityEngine;

namespace ModuleChat
{
    public class RedEnvelopeView : KContainer
    {
        private IconContainer _playerHeadIcon;
        private StateText _playerLvText;
        private StateText _playerNameText;
        private StateText _tipsText;
        private StateText _notGetRewardText;
        private List<ItemGrid> _rewardGridList = new List<ItemGrid>();
        private List<StateText> _rewardNumList = new List<StateText>();
        private KContainer _playerInfoContainer;
        private StateImage _haveRewardBgTop;
        private StateImage _notHaveRewardBgTop;

        private RedEnvelopeInfo _envelopeInfo;
        private string _rewardNumFormat = string.Empty;
        private Vector2 _originLeftRewardPos = Vector2.zero;
        private Vector2 _centerRewardPos = Vector2.zero;

        protected override void Awake()
        {
            _playerHeadIcon = AddChildComponent<IconContainer>("Container_wanjia/Container_icon");
            _playerLvText = GetChildComponent<StateText>("Container_wanjia/Container_dengji/Label_txtDengji");
            _playerNameText = GetChildComponent<StateText>("Label_txtWanjiamingziqigezi");
            _tipsText = GetChildComponent<StateText>("Label_txtTishi");
            _notGetRewardText = GetChildComponent<StateText>("Label_txtShoumanle");
            _playerInfoContainer = GetChildComponent<KContainer>("Container_wanjia");
            _haveRewardBgTop = GetChildComponent<StateImage>("Image_man");
            _notHaveRewardBgTop = GetChildComponent<StateImage>("Image_kong");
            for (int i = 0; i < 2; i++)
            {
                KContainer rewardContainer = GetChildComponent<KContainer>("Container_reward" + (i + 1).ToString());
                ItemGrid itemGrid = rewardContainer.GetChildComponent<ItemGrid>("Container_wupin");
                StateText numText = rewardContainer.GetChildComponent<StateText>("Label_txtShuliang");
                _rewardGridList.Add(itemGrid);
                _rewardNumList.Add(numText);
            }
            _rewardNumFormat = _rewardNumList[0].CurrentText.text;
            GameObject rewardContainerGo = _rewardGridList[0].transform.parent.gameObject;
            RectTransform rewardRect = rewardContainerGo.GetComponent<RectTransform>();
            _originLeftRewardPos = rewardRect.anchoredPosition;
            _centerRewardPos = new Vector2(608f, -456f);
        }

        public void SetData(RedEnvelopeInfo envelopeInfo)
        {
            _envelopeInfo = envelopeInfo;
        }

        public void Refresh()
        {
            if (_envelopeInfo == null)
            {
                if (Visible)
                {
                    Visible = false;
                }
                return;
            }
            if (_envelopeInfo.playerDbid != 0)
            {
                _playerInfoContainer.Visible = true;
                _playerNameText.Visible = true;
                _playerHeadIcon.SetIcon(MogoPlayerUtils.GetSmallIcon(_envelopeInfo.playerVocation));
                _playerLvText.CurrentText.text = _envelopeInfo.playerLv.ToString();
                _playerNameText.CurrentText.text = _envelopeInfo.playerName.ToString();
            }
            else
            {
                _playerInfoContainer.Visible = false;
                _playerNameText.Visible = false;
            }

            string strTips = system_info_helper.GetContent(_envelopeInfo.systemInfoId);
            strTips = GetTipsDesc(strTips);
            _tipsText.CurrentText.text = strTips;

            if (_envelopeInfo.rewardList != null && _envelopeInfo.rewardList.Count > 0)
            {
                _notGetRewardText.Visible = false;
                _haveRewardBgTop.Visible = true;
                _notHaveRewardBgTop.Visible = false;
                RefreshRewards();
            }
            else
            {
                _notGetRewardText.Visible = true;
                _haveRewardBgTop.Visible = false;
                _notHaveRewardBgTop.Visible = true;
                for (int i = 0; i < _rewardGridList.Count; i++)
                {
                    _rewardGridList[i].transform.parent.gameObject.SetActive(false);
                }
            }
        }

        private void RefreshRewards()
        {
            for (int i = 0; i < _envelopeInfo.rewardList.Count; i++)
            {
                if (i >= _rewardGridList.Count)
                    break;
                RewardData rewardData = _envelopeInfo.rewardList[i];
                _rewardGridList[i].transform.parent.gameObject.SetActive(true);
                _rewardGridList[i].SetItemData(rewardData.id);
                _rewardNumList[i].CurrentText.text = string.Format(_rewardNumFormat, rewardData.num.ToString());
            }
            for (int i = _envelopeInfo.rewardList.Count; i < _rewardGridList.Count; i++)
            {
                _rewardGridList[i].transform.parent.gameObject.SetActive(false);
            }
            SetRewardsPosition();
        }

        private void SetRewardsPosition()
        {
            GameObject rewardContainerGo = _rewardGridList[0].transform.parent.gameObject;
            RectTransform rewardRect = rewardContainerGo.GetComponent<RectTransform>();
            if (_envelopeInfo.rewardList.Count == 1)
            {
                rewardRect.anchoredPosition = _centerRewardPos;
            }
            else
            {
                rewardRect.anchoredPosition = _originLeftRewardPos;
            }
        }

        private string GetTipsDesc(string srcTips)
        {
            string destTips = srcTips;
            switch (_envelopeInfo.envelopeType)
            {
                case RedEnvelopeType.Level:
                    destTips = string.Format(srcTips, _envelopeInfo.playerLv.ToString());
                    break;
                default:
                    break;
            }
            return destTips;
        }
    }
}
