﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Chat;
using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using Game.UI.TextEngine;
using System.Text.RegularExpressions;
using Common.Global;
using Common.ExtendComponent;
using GameData;
using Common.Utils;

namespace ModuleChat
{
    public class ChatAllChannelContentView : ChatChannelContentView
    {
        private const int MAX_SYSTEM_MSG_COUNT = 3;

        protected override void Awake()
        {
            base.Awake();
            _itemType = typeof(ChatAllContentItem);
            GameObject loudSpeakerGo = _list.GetChildComponent<KContainer>("templete/Container_labaAnim").gameObject;
            ChatVoiceAnimPlayPool.Instance.AddVoiceAnimTemplate(VoiceAnimTemplateId.LoudSpeaker, loudSpeakerGo);
        }

        protected override GameObject GetTemplateScrollViewGo()
        {
            Transform parent = this.transform.parent;
            return parent.FindChild("Container_ChatTextStyleContentList/ScrollView_chatView").gameObject;
        }

        protected override void ConfigList()
        {
            _list.SetGap(4, 4);
            _list.SetPadding(2, 1, 2, 1);
        }

        protected override void ReadContent()
        {
            if (Channel == public_config.CHANNEL_ID_ALL || Channel == public_config.CHANNEL_ID_GUILD)
            {
                List<PbChatInfoResp> chatInfoList = _chatData.GetAndClearRecentChatInfoList(this.Channel);
                ProcessRedEnvelopMsg(chatInfoList);
                int chatInfoCount = chatInfoList.Count;
                if (chatInfoCount > 0)
                {
                    if (chatInfoList.Count + _list.ItemList.Count > MAX_ITEM_COUNT)
                    {
                        ProcessSystemContentsWithOutOfMaxItemCount(chatInfoList);
                    }
                    else
                    {
                        ProcessSystemContentsWithInMaxItemCount(chatInfoList);
                    }
                }

                ShowContent(chatInfoList);
            }
            else
            {
                base.ReadContent();
            }
        }

        private void ProcessSystemContentsWithOutOfMaxItemCount(List<PbChatInfoResp> chatInfoList)
        {
            List<PbChatInfoResp> totalMsgList = new List<PbChatInfoResp>();
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                ChatAllContentItem contentItem = _list[i] as ChatAllContentItem;
                PbChatInfoResp chatInfo = contentItem.Data as PbChatInfoResp;
                totalMsgList.Add(chatInfo);
            }
            totalMsgList.AddRange(chatInfoList.ToArray());
            Dictionary<int, List<PbChatInfoResp>> showChatMsgDict = new Dictionary<int, List<PbChatInfoResp>>();
            showChatMsgDict.Add(0, new List<PbChatInfoResp>());  //数据中要显示的列表
            showChatMsgDict.Add(1, new List<PbChatInfoResp>());  //已显示在界面列表中要显示出来的项中引用数据的列表

            int peekCount = 0;
            int peekValidCount = 0;
            int peekSystemMsgCount = 0;
            for (int i = totalMsgList.Count - 1; i >= 0; i--)
            {
                if (peekValidCount == MAX_ITEM_COUNT)
                    break;
                if (peekCount < chatInfoList.Count)
                {
                    if (CheckIsSystemMsg(totalMsgList[i]))
                    {
                        if (peekSystemMsgCount == MAX_SYSTEM_MSG_COUNT)
                        {
                            peekCount++;
                            continue;
                        }
                        peekSystemMsgCount++;
                    }
                    showChatMsgDict[0].Add(totalMsgList[i]);
                }
                else
                {
                    if (CheckIsSystemMsg(totalMsgList[i]))
                    {
                        if (peekSystemMsgCount == MAX_SYSTEM_MSG_COUNT)
                        {
                            peekCount++;
                            continue;
                        }
                        peekSystemMsgCount++;
                    }
                    showChatMsgDict[1].Add(totalMsgList[i]);
                }
                peekCount++;
                peekValidCount++;
            }
            //showChatMsgDict中保存的列表此时是开头的为最新消息
            List<PbChatInfoResp> chatDataValidChatInfoList = showChatMsgDict[0];
            List<PbChatInfoResp> contentValidChatInfoList = showChatMsgDict[1];
            bool isFind = false;
            if (contentValidChatInfoList.Count > 0)
            {
                List<int> removeContentIndexList = new List<int>();
                for (int i = _list.ItemList.Count - 1; i >= 0; i--)
                {
                    ChatAllContentItem contentItem = _list[i] as ChatAllContentItem;
                    PbChatInfoResp chatInfo = contentItem.Data as PbChatInfoResp;
                    isFind = false;
                    for (int j = 0; j < contentValidChatInfoList.Count; j++)
                    {
                        if (object.ReferenceEquals(chatInfo, contentValidChatInfoList[j]))
                        {
                            isFind = true;
                            break;
                        }
                    }
                    if (!isFind)
                    {
                        removeContentIndexList.Add(i);
                    }
                }
                for (int i = 0; i < removeContentIndexList.Count; i++)
                {
                    _list.RemoveItemAt(removeContentIndexList[i]);
                }
            }
            else
            {
                _list.RemoveAll();
            }

            List<int> removeInfoIndexList = new List<int>();
            for (int i = chatInfoList.Count - 1; i >= 0; i--)
            {
                isFind = false;
                for (int j = 0; j < chatDataValidChatInfoList.Count; j++)
                {
                    if (object.ReferenceEquals(chatInfoList[i], chatDataValidChatInfoList[j]))
                    {
                        isFind = true;
                        break;
                    }
                }
                if (!isFind)
                {
                    removeInfoIndexList.Add(i);
                }
            }
            for (int i = 0; i < removeInfoIndexList.Count; i++)
            {
                chatInfoList.RemoveAt(removeInfoIndexList[i]);
            }
        }

        private void ProcessSystemContentsWithInMaxItemCount(List<PbChatInfoResp> chatInfoList)
        {
            //将关于系统消息的数据选出来
            List<PbChatInfoResp> systemMsgList = new List<PbChatInfoResp>();
            for (int i = 0; i < chatInfoList.Count; i++)
            {
                if (CheckIsSystemMsg(chatInfoList[i]))
                {
                    systemMsgList.Add(chatInfoList[i]);
                }
            }
            List<ChatAllContentItem> systemContentItemList = new List<ChatAllContentItem>();
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                ChatAllContentItem contentItem = _list[i] as ChatAllContentItem;
                PbChatInfoResp chatInfo = contentItem.Data as PbChatInfoResp;
                if (CheckIsSystemMsg(chatInfo))
                {
                    systemContentItemList.Add(contentItem);
                }
            }

            int systemMsgCount = systemMsgList.Count + systemContentItemList.Count;
            if (systemMsgCount > MAX_SYSTEM_MSG_COUNT)
            {
                int deltaCount = systemMsgCount - MAX_SYSTEM_MSG_COUNT;
                //先删除显示内容列表中关于系统消息的项
                int count = 0;
                for (int i = 0; i < systemContentItemList.Count; i++)
                {
                    if (count == deltaCount)
                        break;
                    for (int j = 0; j < _list.ItemList.Count; j++)
                    {
                        ChatAllContentItem contentItem = _list[j] as ChatAllContentItem;
                        if (object.ReferenceEquals(contentItem, systemContentItemList[i]))
                        {
                            _list.RemoveItemAt(j);
                            break;
                        }
                    }
                    count++;
                }
                if (deltaCount > count)
                {
                    //删除数据中的关于系统消息的数据
                    deltaCount = deltaCount - count;
                    for (int i = 0; i < deltaCount; i++)
                    {
                        chatInfoList.Remove(systemMsgList[i]);
                    }
                }
            }
        }

        private bool CheckIsSystemMsg(PbChatInfoResp chatInfo)
        {
            return _chatData.IsLimitCountTypeMsg(chatInfo, Channel);
        }

        protected override void OnDestroy()
        {
            EventDispatcher.RemoveEventListener<GameObject>(ChatEvents.SHOW_PASSED_TIME_IN_NEW_LINE, OnShowPassedTime);
        }
    }

    public class ChatAllContentItem : KList.KListItemBase
    {
        protected PbChatInfoResp _data;
        private Dictionary<int, string> _channelImageDict;
        private Dictionary<int, string> _msgTypeImageDict;

        private ChatTextBlock _block;
        private GameObject _loudSpeakerGo;
        private KDummyButton _loudSpeakerBtn;
        private Transform _parent;

        protected ChatMsgPassedTimeComponent _msgPassedTimeComponent;

        private const float CHAT_QUESTION_MAX_WIDTH = 465.0f;
        private const float HAVE_VOCATION_ICON_MAX_WIDTH = 396.0f;

        protected override void Awake()
        {
            _channelImageDict = new Dictionary<int, string>();
            _channelImageDict.Add(public_config.CHANNEL_ID_GUILD, "Image_gonghui");
            _channelImageDict.Add(public_config.CHANNEL_ID_PRIVATE, "Image_miliao");
            _channelImageDict.Add(public_config.CHANNEL_ID_TEAM, "Image_duiwu");
            _channelImageDict.Add(public_config.CHANNEL_ID_VICINITY, "Image_fujin");
            _channelImageDict.Add(public_config.CHANNEL_ID_WORLD, "Image_shijie");
            _msgTypeImageDict = new Dictionary<int, string>();
            _msgTypeImageDict.Add((int)ChatContentType.TipsMsg, "Image_tishi");
            _msgTypeImageDict.Add((int)ChatContentType.SystemMsg, "Image_xitong");
            _msgTypeImageDict.Add((int)ChatContentType.Question, "Image_dati");
            _msgPassedTimeComponent = this.gameObject.AddComponent<ChatMsgPassedTimeComponent>();
            _parent = transform.parent;
            AddEventListener();
        }

        protected void AddEventListener()
        {
            _msgPassedTimeComponent.onCreateTimeText.AddListener(OnCreatePassedTime);
        }

        protected void RemoveEventListener()
        {
            _msgPassedTimeComponent.onCreateTimeText.RemoveListener(OnCreatePassedTime);
        }

        private void OnCreatePassedTime(GameObject componentGo, bool isInNewLine)
        {
            RecalculateSize();
            if (isInNewLine)
            {
                GameObject contentGo = componentGo.GetComponentInParent<KList>().gameObject;
                EventDispatcher.TriggerEvent<GameObject>(ChatEvents.SHOW_PASSED_TIME_IN_NEW_LINE, contentGo);
            }
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbChatInfoResp;
                Refresh();
            }
        }

        protected virtual void Refresh()
        {
            if (_data.msg_type != (int)ChatContentType.Question)
            {
                ShowChannelImage();
            }
            else
            {
                ShowQuestionImage();
            }
            if (!ChatInfoHelper.isSystem(_data))
            {
                ShowVocationIcon();
            }
            AddTextBlock();
            RefreshMsgPassedTimeText();
            RecalculateSize();
            if (_data.msg_type == (int)ChatContentType.Voice)
            {
                ShowLoudSpeaker();
            }
        }

        private void ShowChannelImage()
        {
            if (ChatManager.Instance.IsSendToSystemChannel(_data))
            {
                CloneChannelImgGo(_msgTypeImageDict[(int)ChatContentType.SystemMsg]);
            }
            else if (_data.msg_type == (int)ChatContentType.TipsMsg)
            {
                if (_msgTypeImageDict.ContainsKey((int)_data.msg_type))
                {
                    CloneChannelImgGo(_msgTypeImageDict[(int)_data.msg_type]);
                }
            }
            else
            {
                if (_channelImageDict.ContainsKey((int)_data.channel_id) == true)
                {
                    CloneChannelImgGo(_channelImageDict[(int)_data.channel_id]);
                }
            }
        }

        private void ShowQuestionImage()
        {
            CloneChannelImgGo(_msgTypeImageDict[(int)_data.msg_type]);
        }

        private void ShowVocationIcon()
        {
            GameObject vocationGo = CloneChannelImgGo("Container_zhiyeIcon");
            IconContainer vocationIcon = vocationGo.AddComponent<IconContainer>();
            vocationIcon.SetIcon(MogoPlayerUtils.GetVocationIcon((int)_data.send_vocation));
            GameObjectHelper.MoveRectTransform(vocationGo, new Vector2(77.5f, 0f));
        }

        private GameObject CloneChannelImgGo(string imgName)
        {
            GameObject channelImgGo = Instantiate(_parent.FindChild("templete/" + imgName).gameObject) as GameObject;
            channelImgGo.transform.SetParent(this.transform);
            channelImgGo.SetActive(true);
            channelImgGo.transform.localScale = Vector3.one;
            channelImgGo.transform.localPosition = Vector3.zero;
            return channelImgGo;
        }

        protected void AddTextBlock()
        {
            ChatTextBlock block = null;
            float maxWidth = GetTextMaxWidth();
            if (ChatInfoHelper.isSystem(_data))
            {
                block = ChatTextBlock.CreateBlock(_data, maxWidth, ChatTextBlock.COLOR_SYSTEM_WORLD_CANNEL, true);
            }
            else
            {
                if (ChatManager.Instance.IsSystemTypeChannelMsg(_data))
                {
                    block = ChatTextBlock.CreateBlock(_data, maxWidth, ChatTextBlock.COLOR_SYSTEM_WORLD_CANNEL, true);
                }
                else
                {
                    block = ChatTextBlock.CreateBlock(_data, maxWidth, ChatTextBlock.GetChannelFontColor((int)_data.channel_id), true);
                }
            }
            block.GameObject.transform.SetParent(gameObject.transform);
            RectTransform rect = block.GameObject.GetComponent<RectTransform>();
            rect.localScale = Vector3.one;
            SetTextBlockPosition(rect);
            _block = block;
        }

        protected virtual float GetTextMaxWidth()
        {
            if (_data.msg_type == (int)ChatContentType.Question)
            {
                return CHAT_QUESTION_MAX_WIDTH;
            }
            else
            {
                if (!ChatInfoHelper.isSystem(_data))
                {
                    return HAVE_VOCATION_ICON_MAX_WIDTH;
                }
                else
                {
                    return ChatTextBlock.MAX_WIDTH;
                }
            }
        }

        protected virtual void SetTextBlockPosition(RectTransform blockRect)
        {
            if (_data.msg_type == (int)ChatContentType.Question)
            {
                blockRect.anchoredPosition = new Vector2(42f, -6.0f);
            }
            else
            {
                if (!ChatInfoHelper.isSystem(_data))
                {
                    blockRect.anchoredPosition = new Vector2(120.0f, -6.0f);
                }
                else
                {
                    blockRect.anchoredPosition = new Vector2(80.0f, -6.0f);
                }
            }
        }

        protected void RefreshMsgPassedTimeText()
        {
            _msgPassedTimeComponent.SetData(_data, _block, ChatTextBlock.MAX_WIDTH, "templete/Label_txtPassedTime");
        }

        public void ShowLoudSpeaker()
        {
            if (ChatManager.Instance.IsCancelChatVoiceFunction())
                return;
            if (_loudSpeakerGo == null)
            {
                Transform textLineTrans = _block.GameObject.transform.GetChild(_block.GameObject.transform.childCount - 1);
                RectTransform lineRect = textLineTrans.gameObject.GetComponent<RectTransform>();
                GameObject loudSpeakerGo = CloneLoudSpeakerGameObject();
                loudSpeakerGo.SetActive(true);
                loudSpeakerGo.transform.localScale = new Vector3(0.8f, 0.8f, 1);
                _loudSpeakerBtn = loudSpeakerGo.AddComponent<KDummyButton>();
                _loudSpeakerBtn.onClick.AddListener(OnClickLoudSpeakerBtn);

                RectTransform rectTrans = loudSpeakerGo.GetComponent<RectTransform>();
                RectTransform blockRect = _block.GameObject.GetComponent<RectTransform>();
                Vector2 loudSpeakerSize = Vector2.zero;
                loudSpeakerSize.x = loudSpeakerGo.transform.localScale.x * rectTrans.sizeDelta.x;
                loudSpeakerSize.y = loudSpeakerGo.transform.localScale.y * rectTrans.sizeDelta.y;
                float gap = 4.0f;
                if (lineRect.sizeDelta.x + loudSpeakerSize.x + gap > ChatTextBlock.MAX_WIDTH)
                {
                    float y = -blockRect.sizeDelta.y;
                    rectTrans.anchoredPosition = new Vector2(0, y);
                    blockRect.sizeDelta = new Vector2(blockRect.sizeDelta.x, blockRect.sizeDelta.y + loudSpeakerSize.y);
                    RecalculateSize();
                }
                else
                {
                    float x = lineRect.sizeDelta.x + gap;
                    float y = lineRect.anchoredPosition.y;
                    rectTrans.anchoredPosition = new Vector2(x, y);
                }
                _loudSpeakerGo = loudSpeakerGo;
            }
            else
            {
                if (!_loudSpeakerGo.activeSelf)
                {
                    _loudSpeakerGo.SetActive(true);
                }
            }
        }

        private GameObject CloneLoudSpeakerGameObject()
        {
            GameObject go = ChatVoiceAnimPlayPool.Instance.AddAnimationGo(VoiceAnimTemplateId.LoudSpeaker);
            go.transform.SetParent(_block.GameObject.transform);
            go.transform.localPosition = Vector3.zero;
            go.transform.localScale = Vector3.one;
            return go;
        }

        public override void Show()
        {
            EventDispatcher.AddEventListener<string>(ChatEvents.SDK_START_PLAY_VOICE, OnStartPlayVoice);
            EventDispatcher.AddEventListener<string>(ChatEvents.END_PLAY_VOICE, OnEndPlayVoice);
            EventDispatcher.AddEventListener<GameObject, GameObject, int>(ChatEvents.ON_CLICK_ACHIEVEMENT_LINK, OnShowAchievementTips);
        }

        public override void Hide()
        {
            if (_loudSpeakerGo != null)
            {
                ChatVoiceAnimPlayPool.Instance.StopPlay(_loudSpeakerGo.name);
            }
            EventDispatcher.RemoveEventListener<string>(ChatEvents.SDK_START_PLAY_VOICE, OnStartPlayVoice);
            EventDispatcher.RemoveEventListener<string>(ChatEvents.END_PLAY_VOICE, OnEndPlayVoice);
            EventDispatcher.RemoveEventListener<GameObject, GameObject, int>(ChatEvents.ON_CLICK_ACHIEVEMENT_LINK, OnShowAchievementTips);
        }

        private void OnClickLoudSpeakerBtn()
        {
            string voiceId = _data.voice_id;
            if (!string.IsNullOrEmpty(voiceId))
            {
                EventDispatcher.TriggerEvent<string, ulong>(ChatEvents.PLAY_VOICE, voiceId, _data.send_dbid);
            }
        }

        private void OnStartPlayVoice(string path)
        {
            if (_loudSpeakerGo == null)
                return;
            if (path.IndexOf(_data.voice_id) == -1)
                return;
            ChatVoiceAnimPlayPool.Instance.PlayAnim(_loudSpeakerGo.name);
        }

        private void OnEndPlayVoice(string path)
        {
            if (_loudSpeakerGo == null)
                return;
            if (path.IndexOf(_data.voice_id) == -1)
                return;
            ChatVoiceAnimPlayPool.Instance.StopPlay(_loudSpeakerGo.name);
        }

        private void OnShowAchievementTips(GameObject blockGo, GameObject textGo, int achieveId)
        {
            if (_block != null && _block.GameObject == blockGo)
            {
                AchievementTipsParam param = new AchievementTipsParam();
                param.achieveId = achieveId;
                param.targetPos = textGo.transform.position;
                param.targetSizeDelta = textGo.GetComponent<RectTransform>().sizeDelta;
                param.targetSizeDelta.y = 0;
                UIManager.Instance.ShowPanel(PanelIdEnum.AchievementTips, param);
            }
        }

        public override void Dispose()
        {
            RemoveEventListener();
            if (_loudSpeakerGo != null)
            {
                _loudSpeakerBtn.onClick.RemoveListener(OnClickLoudSpeakerBtn);
                ChatVoiceAnimPlayPool.Instance.RemoveAnimGo(_loudSpeakerGo.name);
                _loudSpeakerGo = null;
            }
            if (_data.msg_type == (int)ChatContentType.RedEnvelope)
            {
                ChatData chatData = PlayerDataManager.Instance.ChatData;
                List<ChatLinkBaseWrapper> linkWrapperList = chatData.ChatLinkData.GetChatLinkWrapperList(_data);
                if (linkWrapperList == null)
                {
                    LoggerHelper.Error("Dispose, Can not find this link wrapper list ! chat id = " + _data.seq_id + ", channel = " + _data.channel_id);
                    return;
                }
                ChatRedEnvelopeWrapper redEnvelopeWrapper = null;
                for (int i = 0; i < linkWrapperList.Count; i++)
                {
                    redEnvelopeWrapper = linkWrapperList[i] as ChatRedEnvelopeWrapper;
                    if (redEnvelopeWrapper != null)
                    {
                        int envelopeId = (int)redEnvelopeWrapper.redEnvelopeId;
                        RedEnvelopeData envelopeData = PlayerDataManager.Instance.RedEnvelopeData;
                        envelopeData.RemoveRedEnvelope(envelopeId);
                        break;
                    }
                }
            }
        }
    }
}
