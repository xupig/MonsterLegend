﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.Data;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using Common.Base;

namespace ModuleChat
{
    public class ChatTeamContentView : ChatAllChannelContentView
    {
        private KContainer _convenientUseContainer;
        private KButton _convenientUseBtn;
        private StateText _convenientUseText;

        private bool _isFollowingCaptain = false;

        private TeamData _teamData;

        protected override void Awake()
        {
            base.Awake();
            ResizeScrollView();
            RefreshButtonRect();
            _teamData = PlayerDataManager.Instance.TeamData;
            _convenientUseContainer = GetChildComponent<KContainer>("Container_bianjiecaozuo");
            _convenientUseBtn = _convenientUseContainer.GetChildComponent<KButton>("Button_caozuo");
            _convenientUseText = _convenientUseBtn.GetChildComponent<StateText>("desc");
        }

        protected override KScrollView GetChatScrollView()
        {
            KScrollView scrollView = base.GetChatScrollView();
            RectTransform rect = scrollView.gameObject.GetComponent<RectTransform>();
            rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, -33f);
            return scrollView;
        }

        private void ResizeScrollView()
        {
            RectTransform maskRect = _scrollView.Mask.gameObject.GetComponent<RectTransform>();
            maskRect.sizeDelta = new Vector2(maskRect.sizeDelta.x, 516f);
            RectTransform arrowNextRect = _scrollView.GetChildComponent<RectTransform>("arrow/Image_arrowNext");
            arrowNextRect.anchoredPosition = new Vector2(arrowNextRect.anchoredPosition.x, -542.5f);
            RectTransform scrollViewRect = _scrollView.gameObject.GetComponent<RectTransform>();
            scrollViewRect.sizeDelta = new Vector2(scrollViewRect.sizeDelta.x, 588f);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            RefreshConvenientUseInfo();
        }


        private void RefreshButtonRect()
        {
            if (_lookNewChatInfoButton != null)
            {
                RectTransform buttonRect = _scrollView.GetChildComponent<RectTransform>("fasong");
                buttonRect.anchoredPosition = new Vector2(buttonRect.anchoredPosition.x, -524.0f);
                _lookNewChatInfoButton.RefreshRectTransform();
            }
        }

        protected override void AddEventListener()
        {
            base.AddEventListener();
            _convenientUseBtn.onClick.AddListener(OnClickConvenientUseBtn);
            EventDispatcher.AddEventListener(TeamEvent.TEAM_REFRESH, OnTeamRefresh);
        }

        protected override void RemoveEventListener()
        {
            base.RemoveEventListener();
            _convenientUseBtn.onClick.RemoveListener(OnClickConvenientUseBtn);
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_REFRESH, OnTeamRefresh);
        }

        private void OnClickConvenientUseBtn()
        {
            if (_teamData.IsInTeam())
            {
                if (!_teamData.IsCaptain())
                {
                    if (!_isFollowingCaptain)
                    {
                        _isFollowingCaptain = true;
                        TeamManager.Instance.FollowCaptain();
                    }
                    else
                    {
                        _isFollowingCaptain = false;
                        PlayerAutoFightManager.GetInstance().Stop();
                        TeamManager.Instance.CancelFollowCaptain();
                    }
                }
                else
                {
                    TeamManager.Instance.TeamCallAll();
                }
            }
            else
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.Interactive, new int[] {2, 1});
            }
            RefreshConvenientUseInfo();
        }

        private void OnTeamRefresh()
        {
            RefreshConvenientUseInfo();
        }

        public void RefreshConvenientUseInfo()
        {
            if (_teamData.IsInTeam())
            {
                if (!_teamData.IsCaptain())
                {
                    if (_isFollowingCaptain)
                    {
                        _convenientUseText.ChangeAllStateText(string.Format(ChatConst.IN_TEAM_TIPS_TEMPLATE, ChatConst.CANCEL_FOLLOW_CAPTAIN));
                    }
                    else
                    {
                        _convenientUseText.ChangeAllStateText(string.Format(ChatConst.IN_TEAM_TIPS_TEMPLATE, ChatConst.FOLLOW_CAPTAIN));
                    }
                }
                else
                {
                    _convenientUseText.ChangeAllStateText(string.Format(ChatConst.IN_TEAM_TIPS_TEMPLATE, ChatConst.CAPTAIN_CALL_ALL));
                }
            }
            else
            {
                _convenientUseText.ChangeAllStateText(ChatConst.GO_TO_AUTO_MATCH_TEAM_TIPS);
                _isFollowingCaptain = false;
            }
        }
    }
}
