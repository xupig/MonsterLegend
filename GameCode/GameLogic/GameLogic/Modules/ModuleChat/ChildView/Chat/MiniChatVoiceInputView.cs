﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;

namespace ModuleChat
{
    public class MiniChatVoiceInputView : ChatVoiceInputView
    {
        protected override KButton GetTalkRecordBtn()
        {
            return GetComponent<KButton>();
        }
    }
}
