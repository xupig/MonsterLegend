﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using GameMain.Entities;
using Common.ExtendComponent;

namespace ModuleChat
{
    public class ChatChannelContentView : KContainer
    {
        public const int MAX_ITEM_COUNT = 30;
        public const float IGNORE_PADDING = 5; 

        public int Channel { get; set; }

        protected KList _list;
        protected ChatData _chatData;
        protected Type _itemType;
        protected KScrollView _scrollView;
        protected GameObject _scrollViewGo;
        protected KShrinkableButton _lookNewChatInfoButton;
        protected StateImage _upLookNewChatInfoImage;
        protected StateImage _downLookNewChatInfoImage;
        protected ListLayoutComponent _layoutComponent;

        protected RectTransform _maskRect;
        protected RectTransform _contentRect;

        private const int ADD_ITEM_STEP = 12;

        protected bool _hasPassedTimeInNewLine = false;

        private bool _hasNewChatInfoInContent = false;
        private int _earliestNotReadItemDistanceIndex = 0; //最早的未读消息里最新消息的索引差值, 

        private bool _isForceContentMoveBottom = false;
        public bool IsForceContentMoveBottom
        {
            get { return _isForceContentMoveBottom; }
            set { _isForceContentMoveBottom = value; }
        }

        protected override void Awake()
        {
            _itemType = typeof(ChatContentItem);
            InitContentList();
            EventDispatcher.AddEventListener<GameObject>(ChatEvents.SHOW_PASSED_TIME_IN_NEW_LINE, OnShowPassedTime);
        }

        public virtual GameObject CheckIsExsitScrollViewGo()
        {
            if (_scrollViewGo == null)
            {
                _scrollViewGo = CloneScrollViewGo();
                RecalculateSize();
            }
            return _scrollViewGo;
        }

        protected void InitContentList()
        {
            _scrollView = GetChatScrollView();
            _maskRect = _scrollView.Mask.gameObject.GetComponent<RectTransform>();
            _contentRect = _scrollView.Content.gameObject.GetComponent<RectTransform>();
            _lookNewChatInfoButton = _scrollView.GetChildComponent<KShrinkableButton>("fasong");
            _upLookNewChatInfoImage = _scrollView.GetChildComponent<StateImage>("fasong/shang");
            _downLookNewChatInfoImage = _scrollView.GetChildComponent<StateImage>("fasong/xia");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            ConfigList();
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _chatData = PlayerDataManager.Instance.ChatData;
            _layoutComponent = _list.gameObject.AddComponent<ListLayoutComponent>();
            InitListLayoutInfo();
            SetScrollArrowRaycastInvalid();
        }

        protected virtual KScrollView GetChatScrollView()
        {
            KScrollView scrollView = _scrollViewGo.GetComponent<KScrollView>();
            SetScrollViewPosition();
            return scrollView;
        }

        protected virtual GameObject GetTemplateScrollViewGo()
        {
            Transform parent = this.transform.parent;
            return parent.FindChild("Container_ChatContentList/ScrollView_chatView").gameObject;
        }

        protected GameObject CloneScrollViewGo()
        {
            GameObject scrollViewGo = Instantiate(GetTemplateScrollViewGo()) as GameObject;
            scrollViewGo.transform.SetParent(this.transform);
            scrollViewGo.SetActive(true);
            scrollViewGo.transform.localScale = Vector3.one;
            scrollViewGo.transform.localPosition = Vector3.zero;
            return scrollViewGo;
        }

        private void SetScrollViewPosition()
        {
            GameObject templateScrollViewGo = GetTemplateScrollViewGo();
            Vector3 worldPos = templateScrollViewGo.transform.position;
            Vector2 screenPos = RectTransformUtility.WorldToScreenPoint(UIManager.Instance.UICamera, worldPos);
            RectTransform scrollViewParentRect = this.gameObject.GetComponent<RectTransform>();
            Vector2 destPos = Vector2.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(scrollViewParentRect, screenPos, UIManager.Instance.UICamera, out destPos);
            RectTransform scrollViewRect = _scrollViewGo.GetComponent<RectTransform>();
            scrollViewRect.anchoredPosition = destPos;
        }

        protected virtual void ConfigList()
        {
            _list.SetPadding(-5, 1, 5, 15);
            _list.SetGap(5, 0);
        }

        private void InitListLayoutInfo()
        {
            _layoutComponent.InitListLayoutInfo(KList.KListDirection.TopToDown);
        }

        private void SetScrollArrowRaycastInvalid()
        {
            StateImage nextArrow = _scrollView.GetChildComponent<StateImage>("arrow/Image_arrowNext");
            StateImage prevArrow = _scrollView.GetChildComponent<StateImage>("arrow/Image_arrowPrev");
            nextArrow.gameObject.AddComponent<RaycastComponent>();
            prevArrow.gameObject.AddComponent<RaycastComponent>();
        }

        protected virtual void AddEventListener()
        {
            _scrollView.ScrollRect.onRequestNext.AddListener(RefreshMoreChatMsg);
            if (_lookNewChatInfoButton != null)
            {
                _lookNewChatInfoButton.onClick.AddListener(ShowNewChatInfo);
            }
            _scrollView.ScrollRect.onValueChanged.AddListener(OnRectValueChanged);
            EventDispatcher.AddEventListener<int, PbChatInfoResp>(ChatEvents.RECEIVE_CONTENT, OnReceiveContent);
        }

        protected virtual void RemoveEventListener()
        {
            _scrollView.ScrollRect.onRequestNext.RemoveListener(RefreshMoreChatMsg);
            if (_lookNewChatInfoButton != null)
            {
                _lookNewChatInfoButton.onClick.RemoveListener(ShowNewChatInfo);
            }
            _scrollView.ScrollRect.onValueChanged.RemoveListener(OnRectValueChanged);
            EventDispatcher.RemoveEventListener<int, PbChatInfoResp>(ChatEvents.RECEIVE_CONTENT, OnReceiveContent);
        }

        private void OnRectValueChanged(Vector2 position)
        {
            if (_hasNewChatInfoInContent)
            {
                Vector2 newChatInfoSize = _layoutComponent.RecalculateItemSize(Math.Max(0,_list.ItemList.Count - _earliestNotReadItemDistanceIndex), _earliestNotReadItemDistanceIndex);
                if (_contentRect.sizeDelta.y - newChatInfoSize.y > _contentRect.anchoredPosition.y)
                {
                    _hasNewChatInfoInContent = false;
                    _earliestNotReadItemDistanceIndex = 0;
                    CheckLookButtonState();
                }
            }
        }

        private void RefreshMoreChatMsg(KScrollRect scrollRect)
        {
            List<PbChatInfoResp> nextPageChatInfoList = _chatData.PopNewChatInfoList(Channel, 10);
            if (nextPageChatInfoList.Count > 0)
            {
                ShowContent(nextPageChatInfoList);
                ProcessReadNewMsgFlag();
                CheckLookButtonState();
            }
        }

        private void ShowNewChatInfo()
        {
            ///新消息已经显示在视图中，但未查看
            if (_hasNewChatInfoInContent)
            {
                MoveToEarliestChatInfoView(_earliestNotReadItemDistanceIndex);
                _earliestNotReadItemDistanceIndex = 0;
                _hasNewChatInfoInContent = false;
                CheckLookButtonState();
            }
            else
            {
                List<PbChatInfoResp> nextPageChatInfoList = _chatData.PopNewChatInfoList(Channel, MAX_ITEM_COUNT);
                int newChatInfoCount = nextPageChatInfoList.Count;
                ShowContent(nextPageChatInfoList);
                MoveToEarliestChatInfoView(newChatInfoCount);
                CheckLookButtonState();
            }
        }

        private void OnReceiveContent(int channel, PbChatInfoResp chatInfo)
        {
            ///如果在底部
            if (IsContentBottom())
            {
                ReadContent();
            }
            else if (chatInfo.send_dbid == PlayerAvatar.Player.dbid) //如果是玩家自己发言，则置底
            {
                ReadContent();
            }
            ProcessReadNewMsgFlag();
            CheckLookButtonState();
        }

        protected void OnShowPassedTime(GameObject contentGo)
        {
            if (contentGo != _scrollView.Content)
                return;
            _hasPassedTimeInNewLine = true;
            if (this.gameObject.activeSelf)
            {
                _hasPassedTimeInNewLine = false;
                RecalculateItemPosition();
                _list.RecalculateSize();
            }
        }

        protected virtual void ReadContent()
        {
            List<PbChatInfoResp> chatInfoList = _chatData.GetAndClearRecentChatInfoList(this.Channel);
            ProcessRedEnvelopMsg(chatInfoList);
            ShowContent(chatInfoList);
        }

        protected void ShowContent(List<PbChatInfoResp> chatInfoList)
        {
            int chatInfoCount = chatInfoList.Count;
            for (int i = 0; i < chatInfoCount; i++)
            {
                RemoveBottomItem();
                _list.AddItemAt(_itemType, chatInfoList[i], _list.ItemList.Count, false);
            }
            if (chatInfoCount > 0)
            {
                RecalculateItemPosition();
                _list.RecalculateSize();
                if (_hasNewChatInfoInContent)
                {
                    _earliestNotReadItemDistanceIndex += chatInfoCount;
                    _earliestNotReadItemDistanceIndex = Math.Min(_earliestNotReadItemDistanceIndex, MAX_ITEM_COUNT);
                }
            }
            Vector2 bottomPos = Vector2.zero;
            if (!IsForceMoveBottom())
            {
                if (CheckCanMoveBottom(chatInfoList, ref bottomPos))
                {
                    MoveContentToBottom(bottomPos);
                }
            }
            else
            {
                bottomPos = CalculateBottomPosition();
                MoveContentToBottom(bottomPos);
            }
            if (chatInfoList.Count > 0)
            {
                chatInfoList.Clear();
            }
            ProcessReadNewMsgFlag();
        }

        private void CheckLookButtonState()
        {
            if (_lookNewChatInfoButton != null)
            {
                _lookNewChatInfoButton.Visible = _hasNewChatInfoInContent || _chatData.GetNewChatInfoList(Channel).Count != 0;
                _upLookNewChatInfoImage.Visible = _hasNewChatInfoInContent;
                _downLookNewChatInfoImage.Visible = !_hasNewChatInfoInContent && _chatData.GetNewChatInfoList(Channel).Count != 0;
            }
        }

        protected void RecalculateItemPosition()
        {
            _layoutComponent.RecalculateItemPosition();
        }

        protected bool CheckCanMoveBottom(List<PbChatInfoResp> chatInfoList, ref Vector2 bottomPos)
        {
            if (chatInfoList.Count == 0)
                return false;
            PbChatInfoResp lastChatInfo = chatInfoList[chatInfoList.Count - 1];
            if (lastChatInfo.send_dbid == PlayerAvatar.Player.dbid)
            {
                bottomPos = CalculateBottomPosition(_maskRect, _contentRect);
                return true;
            }
            else
            {
                bottomPos = CalculateBottomPosition(_maskRect, _contentRect);
                Vector2 lastItemSize = _layoutComponent.RecalculateItemSize(_list.ItemList.Count - 1, 1);
                if (bottomPos.y - _contentRect.anchoredPosition.y <= IGNORE_PADDING + lastItemSize.y)
                {
                    return true;
                }
            }
            return false;
        }

        protected virtual bool IsForceMoveBottom()
        {
            if (IsForceContentMoveBottom)
            {
                IsForceContentMoveBottom = false;
                return true;
            }
            return false;
        }

        protected void MoveContentToBottom(Vector2 bottomPos)
        {
            if (_list.ItemList.Count == 0)
                return;
            _contentRect.anchoredPosition = bottomPos;
        }

        protected Vector2 CalculateBottomPosition()
        {
            return CalculateBottomPosition(_maskRect, _contentRect);
        }

        protected Vector2 CalculateBottomPosition(RectTransform maskRect, RectTransform contentRect)
        {
            if (contentRect.sizeDelta.y > maskRect.sizeDelta.y)
            {
                KList.KListItemBase bottomItem = _list.ItemList[_list.ItemList.Count - 1];
                RectTransform itemRect = bottomItem.gameObject.GetComponent<RectTransform>();
                float y = Mathf.Abs(itemRect.anchoredPosition.y) + itemRect.sizeDelta.y - maskRect.sizeDelta.y;
                return new Vector2(contentRect.anchoredPosition.x, y);
            }
            else
            {
                return new Vector2(contentRect.anchoredPosition.x, 0f);
            }
        }

        protected void ProcessReadNewMsgFlag()
        {
            if (this.Channel == public_config.CHANNEL_ID_TEAM || this.Channel == public_config.CHANNEL_ID_GUILD || this.Channel == public_config.CHANNEL_ID_PRIVATE)
            {
                _chatData.ProcessReadNewMsg(this.Channel, 0);
            }
        }

        protected void RemoveBottomItem()
        {
            if(_list.ItemList.Count >= MAX_ITEM_COUNT)
            {
                PbChatInfoResp chatInfo = _list.ItemList[0].Data as PbChatInfoResp;
                _list.RemoveItemAt(0);
                _chatData.ChatLinkData.ProcessRemoveChatLinkInfo(this.Channel, chatInfo);
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            IsForceContentMoveBottom = true;
            RefreshBeforeNewInfo();
            ReadContent();
            RefreshAfterNewInfo();
            CheckLookButtonState();
            AddEventListener();
            if (_hasPassedTimeInNewLine)
            {
                _hasPassedTimeInNewLine = false;
                RecalculateItemPosition();
                _list.RecalculateSize();
            }
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        protected void ResetContentPostion()
        {
            _scrollView.ResetContentPosition();
        }

        protected override void OnDestroy()
        {
            EventDispatcher.RemoveEventListener<GameObject>(ChatEvents.SHOW_PASSED_TIME_IN_NEW_LINE, OnShowPassedTime);
            base.OnDestroy();
        }

        protected virtual void RefreshBeforeNewInfo()
        {
            _earliestNotReadItemDistanceIndex = Math.Min(_chatData.GetNewChatInfoList(Channel).Count,MAX_ITEM_COUNT);
        }

        protected virtual void RefreshAfterNewInfo()
        {
            Vector2 newChatInfoContentSize = _layoutComponent.RecalculateItemSize(_list.ItemList.Count - _earliestNotReadItemDistanceIndex, _earliestNotReadItemDistanceIndex);
            _hasNewChatInfoInContent = newChatInfoContentSize.y > _maskRect.sizeDelta.y;
        }

        private bool IsContentBottom()
        {
            return _contentRect.sizeDelta.y - _maskRect.sizeDelta.y - _contentRect.anchoredPosition.y <= IGNORE_PADDING;
        }

        private void MoveToEarliestChatInfoView(int chatInfoCount)
        {
            if (chatInfoCount != 0)
            {
                Vector2 newChatInfoContentSize = _layoutComponent.RecalculateItemSize(_list.ItemList.Count - chatInfoCount, chatInfoCount);
                _contentRect.anchoredPosition = new Vector2(_contentRect.anchoredPosition.x, _contentRect.sizeDelta.y - Mathf.Max(newChatInfoContentSize.y, _maskRect.sizeDelta.y));
            }
        }

        //处理只在列表里显示一条最新的红包消息
        //先查找聊天数据中是否存在红包的消息，如果存在，再查找聊天列表里是否存在，存在则删除这条内容。
        protected void ProcessRedEnvelopMsg(List<PbChatInfoResp> chatInfoList)
        {
            if (_list.ItemList.Count == 0)
                return;
            bool isFindRedEnvelopInfo = false;
            for (int i = 0; i < chatInfoList.Count; i++)
            {
                if (chatInfoList[i].msg_type == (uint)ChatContentType.RedEnvelope)
                {
                    isFindRedEnvelopInfo = true;
                    break;
                }
            }
            if (!isFindRedEnvelopInfo)
                return;
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                PbChatInfoResp chatInfo = _list.ItemList[i].Data as PbChatInfoResp;
                if (chatInfo.msg_type == (uint)ChatContentType.RedEnvelope)
                {
                    _list.RemoveItemAt(i);
                    break;
                }
            }
        }

    }

    public class ChatContentItem : KList.KListItemBase
    {
        private PbChatInfoResp _data;
        private LeftChatItemView _leftView;
        private RightChatItemView _rightView;
        private LeftVoiceChatItemView _leftVoiceView;
        private RightVoiceChatItemView _rightVoiceView;
        private GameObject _leftVoiceGo;
        private GameObject _rightVoiceGo;
        private SystemChatItemView _systemView;

        private KList _parent;
        private PassedTimeShowDirection _passedTimeShowDir = PassedTimeShowDirection.None;

        protected override void Awake()
        {
            _parent = transform.parent.GetComponent<KList>();
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbChatInfoResp;
                Refresh();
            }
        }

        private void Refresh()
        {
            if(_data.send_dbid == PlayerAvatar.Player.dbid)
            {
                if (_data.msg_type == public_config.CHAT_MSG_TYPE_VOICE)
                {
                    if (_rightVoiceGo == null)
                    {
                        GameObject rightVoiceGo = CloneGameObject("templete/Container_rightYuyin");
                        _rightVoiceGo = rightVoiceGo;
                        SetViewPosition(_rightVoiceGo, new Vector2(125f, 0f));
                    }
                    _rightVoiceGo.SetActive(true);
                    if (_rightVoiceView == null)
                    {
                        _rightVoiceView = _rightVoiceGo.AddComponent<RightVoiceChatItemView>();
                    }
                    _rightVoiceView.Data = _data;
                    ResizeItem(_rightVoiceView);
                }
                else
                {
                    if (_rightView == null)
                    {
                        GameObject rightViewGo = CloneGameObject("templete/Container_right");
                        rightViewGo.SetActive(true);
                        _rightView = rightViewGo.AddComponent<RightChatItemView>();
                        SetViewPosition(rightViewGo, new Vector2(27f, 0f));
                    }
                    //需在设置Data前添加监听
                    AddLeftItemViewListener(_rightView);
                    _rightView.Data = _data;
                    ResizeItem(_rightView);
                }
            }
            else
            {
                if (_data.msg_type == public_config.CHAT_MSG_TYPE_VOICE)
                {
                    if (_leftVoiceGo == null)
                    {
                        GameObject leftVoiceGo = CloneGameObject("templete/Container_leftYuyin");
                        _leftVoiceGo = leftVoiceGo;
                        SetViewPosition(_leftVoiceGo, new Vector2(-22f, 0f));
                    }
                    _leftVoiceGo.SetActive(true);
                    if (_leftVoiceView == null)
                    {
                        _leftVoiceView = _leftVoiceGo.AddComponent<LeftVoiceChatItemView>();
                    }
                    _leftVoiceView.Data = _data;
                    ResizeItem(_leftVoiceView);
                }
                else
                {
                    if (ChatInfoHelper.isSystem(_data) == false)
                    {
                        if (_leftView == null)
                        {
                            GameObject leftViewGo = CloneGameObject("templete/Container_left");
                            leftViewGo.SetActive(true);
                            _leftView = leftViewGo.AddComponent<LeftChatItemView>();
                            SetViewPosition(leftViewGo, new Vector2(-22f, 0f));
                        }
                        AddLeftItemViewListener(_leftView);
                        _leftView.Data = _data;
                        ResizeItem(_leftView);
                    }
                    else
                    {
                        if (_systemView == null)
                        {
                            _systemView = this.gameObject.AddComponent<SystemChatItemView>();
                        }
                        _systemView.Data = _data;
                        ResizeItem(_systemView);
                    }
                }
            }
        }

        private void SetViewPosition(GameObject viewGo, Vector2 pos)
        {
            RectTransform rect = viewGo.GetComponent<RectTransform>();
            rect.anchoredPosition = pos;
        }

        private void ResizeItem(LeftChatItemView itemView)
        {
            RectTransform rect = itemView.GetComponent<RectTransform>();
            RectTransform itemRect = GetComponent<RectTransform>();
            if (itemRect.sizeDelta.x != rect.sizeDelta.x || itemRect.sizeDelta.y != rect.sizeDelta.y)
            {
                itemRect.sizeDelta = new Vector2(rect.sizeDelta.x, rect.sizeDelta.y);
            }
        }

        private void ResizeItem(RightVoiceChatItemView voiceItemView)
        {
            RectTransform rect = voiceItemView.GetComponent<RectTransform>();
            RectTransform itemRect = GetComponent<RectTransform>();
            if (itemRect.sizeDelta.x != rect.sizeDelta.x || itemRect.sizeDelta.y != rect.sizeDelta.y)
            {
                itemRect.sizeDelta = rect.sizeDelta;
            }
        }

        private void ResizeItem(SystemChatItemView systemItemView)
        {
            RectTransform rect = systemItemView.GetComponent<RectTransform>();
            RectTransform itemRect = GetComponent<RectTransform>();
            if (itemRect.sizeDelta.x != rect.sizeDelta.x || itemRect.sizeDelta.y != rect.sizeDelta.y)
            {
                itemRect.sizeDelta = rect.sizeDelta;
            }
        }

        private GameObject CloneGameObject(string path)
        {
            GameObject go = Instantiate(_parent.GetChild(path)) as GameObject;
            go.transform.SetParent(this.transform);
            go.transform.localScale = Vector3.one;
            Vector3 anchorPos = _parent.GetChild(path).GetComponent<RectTransform>().anchoredPosition;
            go.transform.localPosition = new Vector3(anchorPos.x, 0f, 0f);
            return go;
        }

        private void AddLeftItemViewListener(LeftChatItemView leftView)
        {
            if (ChatManager.Instance.IsChatMsgContainsSendTime(_data))
            {
                leftView.onCreatePassedTimeInNewLine.AddListener(OnShowPassedTimeInNewLine);
                if (leftView is RightChatItemView)
                {
                    _passedTimeShowDir = PassedTimeShowDirection.Right;
                }
                else
                {
                    _passedTimeShowDir = PassedTimeShowDirection.Left;
                }
            }
        }

        private void OnShowPassedTimeInNewLine(GameObject componentGo)
        {
            RecalculateSize();
            GameObject contentGo = componentGo.GetComponentInParent<KList>().gameObject;
            EventDispatcher.TriggerEvent<GameObject>(ChatEvents.SHOW_PASSED_TIME_IN_NEW_LINE, contentGo);
        }

        public override void Dispose()
        {
            if (_leftVoiceGo != null)
            {
                GameObject.Destroy(_leftVoiceGo);
                _leftVoiceGo = null;
            }
            if (_rightVoiceGo != null)
            {
                GameObject.Destroy(_rightVoiceGo);
                _rightVoiceGo = null;
            }
            if (_passedTimeShowDir == PassedTimeShowDirection.Left)
            {
                _leftView.onCreatePassedTimeInNewLine.RemoveListener(OnShowPassedTimeInNewLine);
            }
            else if (_passedTimeShowDir == PassedTimeShowDirection.Right)
            {
                _rightView.onCreatePassedTimeInNewLine.RemoveListener(OnShowPassedTimeInNewLine);
            }
        }
    }

    public enum PassedTimeShowDirection
    {
        None = 0,
        Left,
        Right
    }
}
