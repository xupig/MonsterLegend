﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.Data;
using UnityEngine;

namespace ModuleChat
{
    public class ChatQuestionView : KContainer
    {
        private StateText _questionDescText;
        private KContainer _questionDetailContainer;
        private KContainer _questionHidedContainer;
        private KDummyButton _hideDetailBtn;
        private KDummyButton _showDetailBtn;
        private StateImage _detailBgImg;
        private StateImage _upArrowImg;

        private ChatQuestionInfo _questionInfo;
        private float _textLineHeight = 0f;
        private float _curTextHeight = 0f;

        protected override void Awake()
        {
            _questionDetailContainer = GetChildComponent<KContainer>("Container_dati02");
            _questionDescText = _questionDetailContainer.GetChildComponent<StateText>("Label_txtWanjiamingzi");
            _questionHidedContainer = GetChildComponent<KContainer>("Container_dati");
            _hideDetailBtn = _questionDetailContainer.gameObject.AddComponent<KDummyButton>();
            _showDetailBtn = _questionHidedContainer.gameObject.AddComponent<KDummyButton>();
            _detailBgImg = _questionDetailContainer.GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _upArrowImg = _questionDetailContainer.GetChildComponent<StateImage>("Image_shousuo_up");
            _questionDescText.CurrentText.text = "a";
            _textLineHeight = _questionDescText.CurrentText.preferredHeight;
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _hideDetailBtn.onClick.AddListener(OnClickHideDetailBtn);
            _showDetailBtn.onClick.AddListener(OnClickShowDetailBtn);
        }

        private void RemoveEventListener()
        {
            _hideDetailBtn.onClick.RemoveListener(OnClickHideDetailBtn);
            _showDetailBtn.onClick.RemoveListener(OnClickShowDetailBtn);
        }

        private void OnClickHideDetailBtn()
        {
            _questionDetailContainer.Visible = false;
            _questionHidedContainer.Visible = true;
        }

        private void OnClickShowDetailBtn()
        {
            _questionDetailContainer.Visible = true;
            _questionHidedContainer.Visible = false;
        }

        public void Refresh(ChatQuestionInfo questionInfo)
        {
            if (!object.ReferenceEquals(_questionInfo, questionInfo))
            {
                _questionInfo = questionInfo;
                _questionDetailContainer.Visible = true;
                _questionHidedContainer.Visible = false;
            }
            _questionDescText.CurrentText.text = ChatConst.CHAT_QUESTION_TITLE + _questionInfo.Desc;
            SetDetailBgSize();
        }

        public void ResetQuestionInfo()
        {
            _questionInfo = null;
        }

        private void SetDetailBgSize()
        {
            float textHeight = _questionDescText.CurrentText.preferredHeight;
            if (_curTextHeight != textHeight)
            {
                _curTextHeight = textHeight;
                RectTransform bgRect = _detailBgImg.gameObject.GetComponent<RectTransform>();
                RectTransform textRect = _questionDescText.gameObject.GetComponent<RectTransform>();
                bgRect.sizeDelta = new Vector2(bgRect.sizeDelta.x, _curTextHeight + _textLineHeight + Mathf.Abs(textRect.anchoredPosition.y));
                RectTransform upArrowRect = _upArrowImg.gameObject.GetComponent<RectTransform>();
                float y = -(bgRect.sizeDelta.y - upArrowRect.sizeDelta.y);
                upArrowRect.anchoredPosition = new Vector2(upArrowRect.anchoredPosition.x, y);
            }
        }
    }
}
