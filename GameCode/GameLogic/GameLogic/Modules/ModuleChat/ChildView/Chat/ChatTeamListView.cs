﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;

namespace ModuleChat
{
    public class ChatTeamListView : KContainer
    {
        private KList _teamList;

        protected override void Awake()
        {
            base.Awake();
            _teamList = GetChildComponent<KList>("ScrollView_Duiwu/mask/content");
            _teamList.SetPadding(5, 5, 5, 13);
            _teamList.SetGap(-5, 1);
            _teamList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<int>(TeamEvent.TEAM_NEAR_TEAM_LIST_CHANGE, OnGetNearTeamList);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<int>(TeamEvent.TEAM_NEAR_TEAM_LIST_CHANGE, OnGetNearTeamList);
        }

        private void OnGetNearTeamList(int page)
        {
            _teamList.RemoveAll();
            foreach(PbNearTeam team in PlayerDataManager.Instance.TeamData.NearTeamList)
            {
                _teamList.AddItem<TeamListItem>(team, false);
            }
            _teamList.DoLayout();
        }


        private void Test()
        {
            for(int i = 0; i < 10; i++)
            {
                PbNearTeam team = new PbNearTeam();
                team.captain_name = "Kramer";
                team.team_cnt = 40;
                _teamList.AddItem<TeamListItem>(team, false);
            }
            _teamList.DoLayout();
        }

    }

    public class TeamListItem : KList.KListItemBase
    {
        private StateText _captainNameTxt;
        private StateText _memberNumTxt;
        private KButton _joinBtn;

        private string _memberNumTemplate;

        private PbNearTeam _data;

        protected override void Awake()
        {
            _captainNameTxt = GetChildComponent<StateText>("Label_txtDuizhang");
            _memberNumTxt = GetChildComponent<StateText>("Label_txtRenshu");
            _memberNumTemplate = _memberNumTxt.CurrentText.text;
            _joinBtn = GetChildComponent<KButton>("Button_jiaru");

            AddEventListener();
        }

        private void AddEventListener()
        {
            _joinBtn.onClick.AddListener(OnJoinClick);
        }

        private void OnJoinClick()
        {
            if(_data != null)
            {
                //TeamManager.Instance.JoinTeam(_data.team_id);
                TeamManager.Instance.Apply(_data.captain_id);
            }
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbNearTeam;
                Refresh();
            }
        }

        private void Refresh()
        {
            _captainNameTxt.CurrentText.text = _data.captain_name;
            _memberNumTxt.CurrentText.text = string.Format(_memberNumTemplate, _data.team_cnt.ToString());
        }

        public override void Dispose()
        {
            _data = null;
        }
    }
}
