﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using UnityEngine.EventSystems;
using GameLoader.Utils.Timer;
using GameMain.Entities;

namespace ModuleChat
{
    public class ChatVoiceInputView : KContainer
    {
        private int _channel;
        private UInt64 _playerId;
        private string _playerName = string.Empty;

        private KButton _talkRecordBtn;

        private string _strVoice = string.Empty;
        private int _curDragType = DRAG_TYPE_IN;
        private string _voiceBtnName = "";
        private ChatData _chatData;

        private const int DRAG_TYPE_IN = 0;
        private const int DRAG_TYPE_OUT = 1;

        public string StrVoice
        {
            get { return _strVoice; }
        }

        public string VoiceBtnName
        {
            get { return _voiceBtnName; }
        }

        protected override void Awake()
        {
            _chatData = PlayerDataManager.Instance.ChatData;
            _talkRecordBtn = GetTalkRecordBtn();
            _voiceBtnName = _talkRecordBtn.gameObject.name;
        }

        protected virtual KButton GetTalkRecordBtn()
        {
            return GetChildComponent<KButton>("Button_anzhushuohua");
        }

        protected override void OnEnable()
        {
            //在各个聊天内容列表中如果有语音，则派发事件让其播放语音
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _talkRecordBtn.onPointerUp.AddListener(OnRecordBtnUp);
            _talkRecordBtn.onPointerDown.AddListener(OnRecordBtnDown);
            _talkRecordBtn.onPointerEnter.AddListener(OnRecordBtnDragIn);
            _talkRecordBtn.onPointerExit.AddListener(OnRecordBtnDragOut);
            EventDispatcher.AddEventListener(ChatEvents.START_RECORD_VOICE, OnStartRecordVoice);
            EventDispatcher.AddEventListener<string, string>(ChatEvents.END_VOICE_TALK, OnEndVoiceTalk);
        }

        private void RemoveEventListener()
        {
            _talkRecordBtn.onPointerUp.RemoveListener(OnRecordBtnUp);
            _talkRecordBtn.onPointerDown.RemoveListener(OnRecordBtnDown);
            _talkRecordBtn.onPointerEnter.RemoveListener(OnRecordBtnDragIn);
            _talkRecordBtn.onPointerExit.RemoveListener(OnRecordBtnDragOut);
            EventDispatcher.RemoveEventListener(ChatEvents.START_RECORD_VOICE, OnStartRecordVoice);
            EventDispatcher.RemoveEventListener<string, string>(ChatEvents.END_VOICE_TALK, OnEndVoiceTalk);
        }

        protected override void OnDestroy()
        {
            
        }

        public void Refresh(int channel)
        {
            _channel = channel;
            ResetInfo();
        }

        public void RefreshPrivate(int channel, UInt64 playerId, string playerName)
        {
            _channel = channel;
            ResetInfo();
            if (_channel != public_config.CHANNEL_ID_PRIVATE)
                return;
            _playerName = playerName;
        }

        private void ResetInfo()
        {
            VoiceManager.Instance.IsStartTalk = false;
            _strVoice = string.Empty;
            _curDragType = DRAG_TYPE_IN;
        }

        public void ConcatVoiceMessage(string str)
        {
            _strVoice += str;
        }

        public void ResetStrVoice()
        {
            _strVoice = string.Empty;
        }

        //录音按钮上放开
        private void OnRecordBtnUp(KButton btn, PointerEventData evtData)
        {
            //Debug.LogError("OnRecordBtnUp");
            if (!VoiceManager.Instance.IsStartTalk)
                return;

            VoiceManager.Instance.IsStartTalk = false;

            EventDispatcher.TriggerEvent(ChatEvents.HIDE_ALL_VOICE_NOTICE);

            if (_curDragType == DRAG_TYPE_IN)
            {
                VoiceManager.Instance.OnStopTalk();
            }
            else
            {
                PlayMusicAndSound();
                VoiceManager.Instance.OnCancelTalk();
                ResetStrVoice();
                _chatData.CurVoiceBtnName = "";
            }
        }

        //录音按钮上按下
        private void OnRecordBtnDown(KButton btn, PointerEventData evtData)
        {
            //Debug.LogError("OnRecordBtnDown");
            if (VoiceManager.Instance.IsStartTalk)
                return;

            VoiceManager.Instance.IsStartTalk = true;
            _strVoice = string.Empty;
            _curDragType = DRAG_TYPE_IN;
            _chatData.CurVoiceBtnName = _voiceBtnName;

            EventDispatcher.TriggerEvent(ChatEvents.SHOW_VOICE_NOTICE_VIEW);

            //通知SDK开始录音
            VoiceManager.Instance.CallVoiceSdk();

            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                EventDispatcher.TriggerEvent(ChatEvents.SHOW_START_RECORD_VOICE_NOTICE);
                if (!Common.States.GMState.enableVoiceTest)
                {
                    VoiceManager.Instance.OnEndVoiceTalk("别试了，语音只有在手机上才有用", "thumb1.png", SendVoiceText);
                }
                else
                {
                    if (ChatManager.Instance.curVoiceTestId >= 1000000)
                    {
                        ChatManager.Instance.curVoiceTestId = 0;
                    }
                    ChatManager.Instance.curVoiceTestId += 1;
                    int duration = Mathf.CeilToInt(ChatManager.Instance.voiceTestText.Length * 1.0f / 5) * 1000;
                    string strVoice = ChatManager.Instance.voiceTestText;
                    if (strVoice[strVoice.Length - 1] == '。')
                    {
                        strVoice = strVoice.Remove(strVoice.Length - 1, 1);
                    }
                    string voiceText = string.Format("[{0},{1},{2}]<vmsg={3}>", strVoice, (int)ChatLinkType.Voice, duration, ChatManager.Instance.curVoiceTestId);
                    VoiceManager.Instance.OnEndVoiceTalk(voiceText, "thumb1.png", SendVoiceText);
                }
            }
        }

        private void OnRecordBtnDragIn(KButton btn, PointerEventData evtData)
        {
            //Debug.LogError("OnRecordBtnDragIn");
            if (!VoiceManager.Instance.IsStartTalk)
                return;

            //显示录音的过程界面
            _curDragType = DRAG_TYPE_IN;
            EventDispatcher.TriggerEvent(ChatEvents.SHOW_START_RECORD_VOICE_NOTICE);
        }

        private void OnRecordBtnDragOut(KButton btn, PointerEventData evtData)
        {
            //Debug.LogError("OnRecordBtnDragOut");
            if (!VoiceManager.Instance.IsStartTalk)
                return;

            //显示提示当放开后，取消录音
            _curDragType = DRAG_TYPE_OUT;
            EventDispatcher.TriggerEvent(ChatEvents.SHOW_CAN_CANCEL_TALK_NOTICE);
        }

        private void OnStartRecordVoice()
        {
            if (_chatData.CurVoiceBtnName != _voiceBtnName)
                return;
            VoiceManager.Instance.CallVoiceSdk();
        }

        public void OnEndVoiceTalk(string voiceText, string path)
        {
            if (_chatData.CurVoiceBtnName != _voiceBtnName)
                return;
            if (_curDragType == DRAG_TYPE_OUT)
                return;
            VoiceManager.Instance.OnEndVoiceTalk(voiceText, path, SendVoiceText);
        }

        private void SendVoiceText(string voiceText)
        {
            LoggerHelper.Info("[Chat]voiceText = " + voiceText);
            string strChat = voiceText;
            if (strChat.Length > 0 && strChat[strChat.Length - 1] == '|')
            {
                strChat = strChat.Substring(0, strChat.Length - 1);
            }

            //strChat = "语音测试用，测试聊天入口的mini聊天，哈哈<vmsg=voice id 10012345>";
            //发送聊天语音内容到服务器
            string strVoicePathTag = "<vmsg=";
            if (strChat.IndexOf(strVoicePathTag) != -1)
            {
                int subIndex = strChat.IndexOf(strVoicePathTag);
                string strVoiceId = strChat.Substring(subIndex + strVoicePathTag.Length, strChat.Length - (subIndex + strVoicePathTag.Length));
                strVoiceId = strVoiceId.Substring(0, strVoiceId.Length - 1);
                if (_channel == public_config.CHANNEL_ID_PRIVATE)
                {
                    ChatManager.Instance.RequestSendChat(_channel, strChat.Substring(0, subIndex), _playerName, (byte)public_config.CHAT_MSG_TYPE_VOICE, strVoiceId);
                }
                else
                {
                    ChatManager.Instance.RequestSendChat(_channel, strChat.Substring(0, subIndex), 0, (byte)public_config.CHAT_MSG_TYPE_VOICE, strVoiceId);
                }
            }
            else
            {
                if (_channel == public_config.CHANNEL_ID_PRIVATE)
                {
                    ChatManager.Instance.RequestSendChat(_channel, strChat, _playerName, (byte)public_config.CHAT_MSG_TYPE_TEXT);
                }
                else
                {
                    ChatManager.Instance.RequestSendChat(_channel, strChat, 0, (byte)public_config.CHAT_MSG_TYPE_TEXT);
                }
            }
            _chatData.CurVoiceBtnName = "";
        }

        public void PlayMusicAndSound()
        {
            AllSoundManger.Instance.ResetTempVolume();
        }
    }
}
