﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using GameData;
using ModuleCommonUI;
using GameMain.Entities;
using Common.Utils;

namespace ModuleChat
{
    public class ChatGuildListView : KContainer
    {
        private KList _guildList;
        private KScrollView _scrollView;
        private List<PbRankGuildInfo> _contentGuildList = new List<PbRankGuildInfo>();
        private StateText _joinGuildTipsText;

        private const int ADD_ITEM_STEP = 10;

        protected override void Awake()
        {
            _guildList = GetChildComponent<KList>("ScrollView_Gonghui/mask/content");
            _guildList.SetPadding(5, 5, 5, 13);
            _guildList.SetGap(-5, 1);
            _guildList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _scrollView = GetChildComponent<KScrollView>("ScrollView_Gonghui");
            _joinGuildTipsText = GetChildComponent<StateText>("Label_txtgonghui");
            RectTransform tipsRect = _joinGuildTipsText.GetComponent<RectTransform>();
            if (tipsRect.sizeDelta.x < _joinGuildTipsText.CurrentText.preferredWidth)
            {
                tipsRect.sizeDelta = new Vector2(_joinGuildTipsText.CurrentText.preferredWidth, tipsRect.sizeDelta.y);
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            ReqGetRankGuildList();
            RefreshContent();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        private void AddEventListener()
        {
            _scrollView.ScrollRect.onRequestNext.AddListener(RefreshMoreGuildData);
            EventDispatcher.AddEventListener<PbRankGuildInfoList>(RankingListEvents.GET_GUILD_RANK_IN_CHAT, OnRefreshContent);
        }

        private void RemoveEventListener()
        {
            _scrollView.ScrollRect.onRequestNext.RemoveListener(RefreshMoreGuildData);
            EventDispatcher.RemoveEventListener<PbRankGuildInfoList>(RankingListEvents.GET_GUILD_RANK_IN_CHAT, OnRefreshContent);
        }

        private void OnRefreshContent(PbRankGuildInfoList guildInfoList)
        {
            ProcessContentGuildList();
            RefreshContent();
        }

        private void RefreshMoreGuildData(KScrollRect scrollRect)
        {
            List<PbRankGuildInfo> infoList = CreateGuildInfoList(_guildList.ItemList.Count, _guildList.ItemList.Count + ADD_ITEM_STEP);
            for (int i = 0; i < infoList.Count; i++)
            {
                _guildList.AddItem<ChatGuildListItem>(infoList[i]);
            }
        }

        private void ReqGetRankGuildList()
        {
            ChatData chatData = PlayerDataManager.Instance.ChatData;
            chatData.DoGetRankGuildList();
        }

        private void RefreshContent()
        {
            //只刷新目前显示在列表中的项
            _scrollView.ResetContentPosition();
            List<PbRankGuildInfo> infoList = null;
            if (_guildList.ItemList.Count <= ADD_ITEM_STEP)
            {
                infoList = CreateGuildInfoList(0, ADD_ITEM_STEP);
            }
            else
            {
                infoList = CreateGuildInfoList(0, _guildList.ItemList.Count);
            }
            _guildList.SetDataList<ChatGuildListItem>(infoList);
        }

        private List<PbRankGuildInfo> CreateGuildInfoList(int start, int count)
        {
            List<PbRankGuildInfo> infoList = new List<PbRankGuildInfo>();
            int endCount = _contentGuildList.Count >= count ? count : _contentGuildList.Count;
            for (int i = start; i < endCount; i++)
            {
                infoList.Add(_contentGuildList[i]);
            }
            return infoList;
        }

        private void ProcessContentGuildList()
        {
            PbRankGuildInfoList guildInfoList = RankingListManager.Instance.rankInfo.guildRankingList;
            if (guildInfoList == null)
                return;
            _contentGuildList.Clear();
            int guildCount = guildInfoList.rank_guild_info.Count;
            int index = 0;
            //如果公会的人数已达上限，就不添加到推荐的列表中
            for (int i = 0; i < guildCount; i++)
            {
                if (index > guildInfoList.rank_guild_info.Count - 1)
                    break;
                PbRankGuildInfo guildInfo = guildInfoList.rank_guild_info[index];
                int maxMemberNum = guild_level_helper.GetMemberCount((int)guildInfo.guild_level);
                if (guildInfo.guild_member_num < maxMemberNum)
                {
                    _contentGuildList.Add(guildInfo);
                }
                index++;
            }
        }
    }

    public class ChatGuildListItem : KList.KListItemBase
    {
        private PbRankGuildInfo _data;
        private StateText _name;
        private StateText _guildLv;
        private StateText _leaderName;
        private StateText _memberNumText;
        private KButton _joinBtn;

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbRankGuildInfo;
                Refresh();
            }
        }

        protected override void Awake()
        {
            _name = GetChildComponent<StateText>("Label_txtGonghuimingcheng");
            _guildLv = GetChildComponent<StateText>("Label_txtDengji");
            _leaderName = GetChildComponent<StateText>("Label_txtHuizhangName");
            _memberNumText = GetChildComponent<StateText>("Label_txtRenshu");
            _joinBtn = GetChildComponent<KButton>("Button_jiaru");
            AddEventListener();
        }

        private void AddEventListener()
        {
            _joinBtn.onClick.AddListener(OnClickJoinBtn);
        }

        private void RemoveEventListener()
        {
            _joinBtn.onClick.RemoveListener(OnClickJoinBtn);
        }

        private void OnClickJoinBtn()
        {
            int needLevel = int.Parse(global_params_helper.GetGlobalParam(82));
            if (PlayerAvatar.Player.level < needLevel)
            {
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74612, needLevel), PanelIdEnum.MainUIField);
                return;
            }
            GuildManager.Instance.ApplyJoinGuild(_data.guild_id);
        }

        private void Refresh()
        {
            _name.CurrentText.text = _data.guild_name;
            _guildLv.CurrentText.text = _data.guild_level.ToString();
            _leaderName.CurrentText.text = _data.leader_name;
            int maxMemberNum = guild_level_helper.GetMemberCount((int)_data.guild_level);
            _memberNumText.CurrentText.text = string.Format("{0}/{1}", _data.guild_member_num, maxMemberNum);
        }

        public override void Dispose()
        {
            RemoveEventListener();
        }
    }
}
