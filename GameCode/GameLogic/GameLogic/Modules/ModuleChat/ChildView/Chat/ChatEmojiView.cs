﻿using System;
using System.Collections.Generic;
using Common.Chat;
using Common.Events;
using Game.UI.UIComponent;
using MogoEngine.Events;

namespace ModuleChat
{
    public class ChatEmojiView : KContainer
    {
        private KList _emojiList;
        private KScrollPage _scrollPage;
        private bool _isInitEmojiList = false;

        protected override void Awake()
        {
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_biaoqing");
            _scrollPage.TotalPage = 1;
            _emojiList = GetChildComponent<KList>("ScrollPage_biaoqing/mask/content");
            _emojiList.SetPadding(18, 20, 10, 20);
            _emojiList.SetGap(50, 76);
            _emojiList.SetDirection(KList.KListDirection.LeftToRight, 7, 5);
            
            AddEventListener();
        }

        private void AddEventListener()
        {
            _emojiList.onSelectedIndexChanged.AddListener(OnEmojiSelected);
        }

        private void RemoveEventListener()
        {
            _emojiList.onSelectedIndexChanged.RemoveListener(OnEmojiSelected);
        }

        private void OnEmojiSelected(KList taget, int index)
        {
            _emojiList.SelectedIndex = -1;
            EventDispatcher.TriggerEvent<int>(ChatEvents.SELECT_EMOJI, index);
        }

        private void InitEmojiList()
        {
            List<string> nameList = ChatEmojiDefine.GetEmojiFirstSpriteNameList();
            for (int i = 0; i < nameList.Count; i++)
            {
                _emojiList.AddItem<ChatEmojiItem>(nameList[i], false);
            }
            _emojiList.DoLayout();
        }

        public void Refresh()
        {
            if (_isInitEmojiList == false)
            {
                _isInitEmojiList = true;
                InitEmojiList();
            }
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }
    }
}
