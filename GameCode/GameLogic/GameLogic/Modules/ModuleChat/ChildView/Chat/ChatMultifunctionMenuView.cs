﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.Data;
using Common.ExtendComponent;
using GameData;
using Common.Base;
using GameMain.GlobalManager;
using UnityEngine;
using UnityEngine.EventSystems;
using MogoEngine.Events;
using Common.Events;
using Common.Data.Chat;
using Game.UI;

namespace ModuleChat
{
    public class ChatMultifunctionMenuView : KContainer
    {
        protected KButton _openEditPhraseBtn;
        private List<KButton> _phraseBtnList = new List<KButton>();

        private KScrollPage _showLinkScrollPage;
        private KList _showLinkList;

        private ChatShowData _chatShowData;
        private bool _hasRefreshPhraseContent = false;
        private const int SHOW_ONE_PAGE_COUNT = 5;

        protected override void Awake()
        {
            _chatShowData = PlayerDataManager.Instance.ChatData.ChatShowData;
            _openEditPhraseBtn = GetChildComponent<KButton>("Container_changyongyu/Button_shuxie");
            for (int i = 0; i < 6; i++)
            {
                KButton phraseBtn = GetChildComponent<KButton>("Container_changyongyu/Button_btn" + i);
                _phraseBtnList.Add(phraseBtn);
            }
            InitShowLinkContentList();
            AddEventListener();
        }

        private void InitShowLinkContentList()
        {
            _showLinkScrollPage = GetChildComponent<KScrollPage>("Container_lianjie/ScrollPage_lianjie");
            _showLinkList = _showLinkScrollPage.GetChildComponent<KList>("mask/content");
            _showLinkScrollPage.TotalPage = 1;
            _showLinkList.SetPadding(19, 15, 19, 15);
            _showLinkList.SetGap(0, 5);
            _showLinkList.SetDirection(KList.KListDirection.LeftToRight, SHOW_ONE_PAGE_COUNT, 1);
        }

        private void AddEventListener()
        {
            _openEditPhraseBtn.onClick.AddListener(OnClickEdit);
            for (int i = 0; i < _phraseBtnList.Count; i++)
            {
                _phraseBtnList[i].onPointerUp.AddListener(OnClickPhrase);
            }
            EventDispatcher.AddEventListener(ChatEvents.END_EDIT_PHRASE, OnEndEditPhrase);
        }

        private void RemoveEventListener()
        {
            _openEditPhraseBtn.onClick.RemoveListener(OnClickEdit);
            for (int i = 0; i < _phraseBtnList.Count; i++)
            {
                _phraseBtnList[i].onPointerUp.RemoveListener(OnClickPhrase);
            }
            EventDispatcher.RemoveEventListener(ChatEvents.END_EDIT_PHRASE, OnEndEditPhrase);
        }

        private void OnClickEdit()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.ChatPhraseEdit);
        }

        private void OnClickPhrase(KButton target, PointerEventData evtData)
        {
            string content = target.GetChildComponent<StateText>("label").CurrentText.text;
            EventDispatcher.TriggerEvent<string>(ChatEvents.SELECT_PHRASE, content);
        }

        private void OnEndEditPhrase()
        {
            RefreshPhraseContent();
        }

        public virtual void Refresh()
        {
            RefreshShowLinkContentList();
            ShowPhraseContent();
        }

        protected void RefreshShowLinkContentList()
        {
            List<ShowLinkItemData> showItemList = _chatShowData.ShowItemList;
            int pageCount = Mathf.CeilToInt(showItemList.Count * 1.0f / SHOW_ONE_PAGE_COUNT);
            if (_showLinkScrollPage.TotalPage != pageCount)
            {
                _showLinkScrollPage.TotalPage = pageCount;
            }
            _showLinkList.SetDataList<ChatShowItem>(showItemList);
        }

        protected void ShowPhraseContent()
        {
            if (!_hasRefreshPhraseContent)
            {
                RefreshPhraseContent();
                _hasRefreshPhraseContent = true;
            }
        }

        private void RefreshPhraseContent()
        {
            ChatPhraseData phraseData = PlayerDataManager.Instance.ChatData.ChatPhraseData;
            for (int i = 0; i < _phraseBtnList.Count; i++)
            {
                _phraseBtnList[i].GetChildComponent<StateText>("label").ChangeAllStateText(phraseData.GetPhrase(i));
            }
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }
    }

    public class ChatShowItem : KList.KListItemBase
    {
        private KButton _showLinkBtn;
        private StateIcon _showLinkIcon;

        private ShowLinkItemData _data;

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as ShowLinkItemData;
                if (_data == null || ID == _data.Id)
                    return;
                ID = _data.Id;
                Refresh();
            }
        }

        protected override void Awake()
        {
            _showLinkBtn = GetChildComponent<KButton>("Button_item");
            _showLinkIcon = GetChildComponent<StateIcon>("Button_item/stateIcon");
            AddEventListener();
        }

        private void AddEventListener()
        {
            _showLinkBtn.onClick.AddListener(OnClickShowLinkBtn);
        }

        private void RemoveEventListener()
        {
            _showLinkBtn.onClick.RemoveListener(OnClickShowLinkBtn);
        }

        private void OnClickShowLinkBtn()
        {
            if (_data.FunctionId == (int)FunctionId.Achievement)
            {
                bool showHasGainedAchievement = true;
                UIManager.Instance.ShowPanel(PanelIdEnum.Reward, showHasGainedAchievement);
                return;
            }
            function_helper.Follow(_data.FunctionId, null);
        }

        private void Refresh()
        {
            _showLinkIcon.SetIcon(_data.IconId);
        }

        private void OnIconLoaded(GameObject stateIconGo)
        {
            for (int i = 0;i < stateIconGo.transform.childCount;i++)
            {
                Transform child = stateIconGo.transform.GetChild(i);
                Transform holder = child.FindChild("holder");
                if (holder.childCount > 0)
                {
                    ImageWrapper iconWrapper = holder.GetChild(0).GetComponent<ImageWrapper>();
                    RectTransform holderRect = holder.gameObject.GetComponent<RectTransform>();
                    RectTransform iconRect = iconWrapper.gameObject.GetComponent<RectTransform>();
                    if (iconWrapper.preferredWidth >= iconWrapper.preferredHeight)
                    {
                        float height = iconWrapper.preferredHeight * holderRect.sizeDelta.x / iconWrapper.preferredWidth;
                        iconWrapper.SetDimensionsDirty();
                        iconRect.sizeDelta = new Vector2(holderRect.sizeDelta.x, height);
                    }
                    else
                    {
                        float width = iconWrapper.preferredWidth * holderRect.sizeDelta.y / iconWrapper.preferredHeight;
                        iconWrapper.SetDimensionsDirty();
                        iconRect.sizeDelta = new Vector2(width, holderRect.sizeDelta.y);
                    }
                }
            }
        }

        public override void Dispose()
        {
            RemoveEventListener();
        }
    }
}
