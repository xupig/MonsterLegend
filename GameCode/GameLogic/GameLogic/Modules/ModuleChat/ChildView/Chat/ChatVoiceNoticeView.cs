﻿using System;
using System.Collections.Generic;
using Common.Events;
using Game.UI.UIComponent;
using MogoEngine.Events;

namespace ModuleChat
{
    public class ChatVoiceNoticeView : KContainer
    {
        private KContainer _voiceRecord;
        private KContainer _cancelVoiceNotice;
        private KContainer _talkShortTimeNotice;
        private KContainer _talkCountDownNotice;

        private List<KContainer> _noticeContainers = new List<KContainer>();
        private List<KContainer> _voiceVolumeList = new List<KContainer>();

        public const int VOICE_RECORD = 0;
        public const int CANCEL_VOICE_NOTICE = 1;
        public const int TALK_SHORT_TIME_NOTICE = 2;
        public const int TALK_COUNT_DOWN_NOTICE = 3;

        protected override void Awake()
        {
            _voiceRecord = GetChildComponent<KContainer>("Container_shuohua");
            _cancelVoiceNotice = GetChildComponent<KContainer>("Container_fanhui");
            _talkShortTimeNotice = GetChildComponent<KContainer>("Container_shuohuashijianduan");
            _talkCountDownNotice = GetChildComponent<KContainer>("Container_daojishi");
            _noticeContainers.Add(_voiceRecord);
            _noticeContainers.Add(_cancelVoiceNotice);
            _noticeContainers.Add(_talkShortTimeNotice);
            _noticeContainers.Add(_talkCountDownNotice);
            for (int i = 0; i < 5; i++)
            {
                KContainer container = GetChildComponent<KContainer>("Container_shuohua/Container_volume0" + (i + 1));
                _voiceVolumeList.Add(container);
            }
            AddEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(ChatEvents.SHOW_START_RECORD_VOICE_NOTICE, OnShowStartRecordVoice);
            EventDispatcher.AddEventListener(ChatEvents.SHOW_CAN_CANCEL_TALK_NOTICE, OnShowCanCancelTalk);
            EventDispatcher.AddEventListener(ChatEvents.HIDE_ALL_VOICE_NOTICE, OnHideAllVoiceNotice);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(ChatEvents.SHOW_START_RECORD_VOICE_NOTICE, OnShowStartRecordVoice);
            EventDispatcher.RemoveEventListener(ChatEvents.SHOW_CAN_CANCEL_TALK_NOTICE, OnShowCanCancelTalk);
            EventDispatcher.RemoveEventListener(ChatEvents.HIDE_ALL_VOICE_NOTICE, OnHideAllVoiceNotice);
        }

        protected override void OnDestroy()
        {
            _noticeContainers.Clear();
            RemoveEventListener();
        }

        private void OnShowStartRecordVoice()
        {
            ShowNotice(VOICE_RECORD);
        }

        private void OnShowCanCancelTalk()
        {
            ShowNotice(CANCEL_VOICE_NOTICE);
        }

        private void OnHideAllVoiceNotice()
        {
            for (int i = 0; i < _noticeContainers.Count; i++)
            {
                HideNotice(i);
            }
        }

        public void ShowNotice(int widgetIndex)
        {
            EnableNoticeWidget(widgetIndex);
            if (widgetIndex == VOICE_RECORD)
            {
                EnableVoiceVolume(0);
            }
        }

        public void HideNotice(int widgetIndex)
        {
            if (_noticeContainers[widgetIndex].Visible == true)
            {
                _noticeContainers[widgetIndex].Visible = false;
            }
        }

        public void ChangeVoiceVolume(int volume)
        {
            int index = 0;
            if (volume == 0)
            {
                index = 0;
            }
            else if (volume <= 5)
            {
                index = 1;
            }
            else if (volume <= 10)
            {
                index = 2;
            }
            else if (volume <= 20)
            {
                index = 3;
            }
            else
            {
                index = 4;
            }
            EnableVoiceVolume(index);
        }

        private void EnableNoticeWidget(int widgetIndex)
        {
            for (int i = 0; i < _noticeContainers.Count; i++)
            {
                if (widgetIndex == i)
                {
                    _noticeContainers[i].Visible = true;
                }
                else
                {
                    _noticeContainers[i].Visible = false;
                }
            }
        }

        private void EnableVoiceVolume(int index)
        {
            for (int i = 0; i < _voiceVolumeList.Count; i++)
            {
                if (index == i)
                {
                    _voiceVolumeList[i].Visible = true;
                }
                else
                {
                    if (_voiceVolumeList[i].Visible == true)
                    {
                        _voiceVolumeList[i].Visible = false;
                    }
                }
            }
        }
    }
}
