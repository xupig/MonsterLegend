﻿using System;
using System.Collections.Generic;
using Common.ExtendComponent;
using Game.UI;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using UnityEngine;

namespace ModuleChat
{
    public class ChatConvenientMenuView : KContainer
    {
        private KToggleGroup _menuToggleGroup;
        private KDummyButton _maskCloseDummyBtn;
        private ChatEmojiView _emojiView;
        protected ChatMultifunctionMenuView _multifunctionView;
        private StateImage _menuBgImg;
        private RaycastComponent _maskRaycast;

        private const int EMOJI_VIEW = 0;
        private const int MULTIFUNCTION_MENU_VIEW = 1;

        protected override void Awake()
        {
            _menuToggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_menu");
            _emojiView = AddChildComponent<ChatEmojiView>("Container_biaoqing");
            InitMultifunctionView();
            StateImage maskImg = GetChildComponent<StateImage>("Image_closeMask");
            _maskCloseDummyBtn = maskImg.gameObject.AddComponent<KDummyButton>();
            _menuBgImg = GetChildComponent<StateImage>("ScaleImage_sharedKuangDT02");
            SetParentToMask();
            AddEventListener();
        }

        protected virtual void InitMultifunctionView()
        {
            _multifunctionView = AddChildComponent<ChatMultifunctionMenuView>("Container_chatMenu");
        }

        private void SetParentToMask()
        {
            GameObject maskGo = transform.parent.FindChild("ScaleImage_mask").gameObject;
            maskGo.SetActive(true);
            _maskRaycast = maskGo.AddComponent<RaycastComponent>();
            ChatUtil.SetPopupViewParentToMask(maskGo, gameObject);
        }

        private void AddEventListener()
        {
            _menuToggleGroup.onSelectedIndexChanged.AddListener(OnMenuChanged);
            _maskCloseDummyBtn.onClick.AddListener(OnClickCloseMask);
        }

        private void OnMenuChanged(KToggleGroup target, int index)
        {
            ShowSelectMenu(index);
        }

        private void OnClickCloseMask()
        {
            ChatPopupViewFadeIn.BeginHide(gameObject);
        }

        public void Refresh()
        {
            ChatPopupViewFadeIn.BeginShow(gameObject, 0f, UIManager.CANVAS_HEIGHT, _menuBgImg.gameObject);
            _menuToggleGroup.SelectIndex = 0;
            ShowSelectMenu(_menuToggleGroup.SelectIndex);
        }

        private void ShowSelectMenu(int index)
        {
            switch (index)
            {
                case EMOJI_VIEW:
                    _emojiView.Visible = true;
                    _multifunctionView.Visible = false;
                    _emojiView.Refresh();
                    break;
                case MULTIFUNCTION_MENU_VIEW:
                    _emojiView.Visible = false;
                    _multifunctionView.Visible = true;
                    _multifunctionView.Refresh();
                    break;
                default:
                    break;
            }
        }

        protected override void OnEnable()
        {
            _maskRaycast.RayCast = true;
        }

        protected override void OnDisable()
        {
            _maskRaycast.RayCast = false;
        }
    }
}
