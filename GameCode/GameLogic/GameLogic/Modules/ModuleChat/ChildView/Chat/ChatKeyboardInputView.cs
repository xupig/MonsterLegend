﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using UnityEngine.EventSystems;
using Common.Chat;
using Common.Global;
using ModuleCommonUI;
using Common.Utils;
using Common.ExtendComponent;
using System.Text.RegularExpressions;
using GameMain.Entities;

namespace ModuleChat
{
    public class ChatKeyboardInputView : KContainer
    {
        private static int MAX_CHAR_COUNT = 60;
        private const string EMOJI_TEMPLATE = "[E{0}]";
        private const string ITEM_LINK_TEMPLATE = "[{0},{1},{2},{3},{4},{5}]";
        private const string WING_ITEM_LINK_TEMPLATE = "[{0},{1},{2},{3}]";
        private const string ACHIEVEMENT_ITEM_LINK_TEMPLATE = "[{0},{1},{2},{3}]";
        private const string TITLE_ITEM_LINK_TEMPLATE = "[{0},{1},{2},{3},{4}]";

        private const string PRIVATE_CHAT_PLAYER_TEMPLATE = "/{0} ";
        private static readonly string NOT_HAVE_PRIVATE_CHAT_PLAYER_TIPS = MogoLanguageUtil.GetContent(32553);

        private static Regex PRIVATE_CHAT_PLAYER_PATTERN = new Regex(@"/.*? ");

        private KButton _sendBtn;
        private KButton _emojiBtn;
        private KDummyButton _hintBtn;
        private KInputField _input;
        private KInputField _normalInput;
        private KInputField _privateInput;
        private int _channel;
        private UInt64 _playerId;
        private string _playerName;
        private List<ChatItemLinkWrapper> _itemLinkList = new List<ChatItemLinkWrapper>();
        private string _content = string.Empty;
        private ChatPrivateInputInfo _curPrivateInputInfo = new ChatPrivateInputInfo();
        private bool _isValidPrivateName = true;

        private ChatData _chatData;

        protected override void Awake()
        {
            MAX_CHAR_COUNT = GlobalParams.GetChatMsgMaxCharCount();
            _chatData = PlayerDataManager.Instance.ChatData;
            _emojiBtn = GetChildComponent<KButton>("Button_biaoqing");
            _hintBtn = AddChildComponent<KDummyButton>("Container_inputHint");
            _sendBtn = GetChildComponent<KButton>("Button_fasong");
            _normalInput = GetChildComponent<KInputField>("Input_normalInput");
            _privateInput = GetChildComponent<KInputField>("Input_privateInput");
            _privateInput.gameObject.SetActive(false);
            _input = _normalInput;
            _input.text = string.Empty;
            _input.characterLimit = MAX_CHAR_COUNT;
            _input.gameObject.AddComponent<HalfScreenInputAgent>();
            _privateInput.text = string.Empty;
            _privateInput.characterLimit = MAX_CHAR_COUNT + 10;
            _privateInput.gameObject.AddComponent<HalfScreenInputAgent>();
        }

        protected override void OnEnable()
        {
            AddEventListener();  
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _hintBtn.onClick.AddListener(OnHintClick);
            _sendBtn.onClick.AddListener(OnSendClick);
            _emojiBtn.onClick.AddListener(OnEmojiClick);
            _normalInput.onSubmit.AddListener(OnEndEdit);
            _privateInput.onSubmit.AddListener(OnEndEdit);
            _privateInput.onValueChange.AddListener(OnValueChanged);
            EventDispatcher.AddEventListener<int>(ChatEvents.SELECT_EMOJI, OnSelectEmoji);
            EventDispatcher.AddEventListener<string>(ChatEvents.SELECT_PHRASE, OnSelectPhrase);
            EventDispatcher.AddEventListener(ChatEvents.SEND_FORM_MENU, OnSendFromMenu);
        }

        private void RemoveEventListener()
        {
            _hintBtn.onClick.RemoveListener(OnHintClick);
            _sendBtn.onClick.RemoveListener(OnSendClick);
            _emojiBtn.onClick.RemoveListener(OnEmojiClick);
            _normalInput.onSubmit.RemoveListener(OnEndEdit);
            _privateInput.onSubmit.RemoveListener(OnEndEdit);
            _privateInput.onValueChange.RemoveListener(OnValueChanged);
            EventDispatcher.RemoveEventListener<int>(ChatEvents.SELECT_EMOJI, OnSelectEmoji);
            EventDispatcher.RemoveEventListener<string>(ChatEvents.SELECT_PHRASE, OnSelectPhrase);
            EventDispatcher.RemoveEventListener(ChatEvents.SEND_FORM_MENU, OnSendFromMenu);
        }

        private void OnHintClick()
        {
            HideHintBtn();
            EventSystem.current.SetSelectedGameObject(_input.gameObject);
            _input.OnPointerClick(new PointerEventData(EventSystem.current));
            EventDispatcher.TriggerEvent(ChatEvents.CLOSE_EMOJI);
        }

        private void OnSendClick()
        {
            SendContent();
            EventDispatcher.TriggerEvent(ChatEvents.CLOSE_EMOJI);
        }

        private void SendContent()
        {
            string content = _input.text;
            if (_channel == public_config.CHANNEL_ID_PRIVATE)
            {
                content = ProcessPrivateChatContent(content);
            }
            if(content != string.Empty)
            {
                content = content.Replace("\\n", "\n");
                if (_itemLinkList.Count > 0)
                {
                    //在发送时检测是否还存在该链接
                    content = _chatData.CreateLinkMsg(content, _itemLinkList.ToArray(), true);
                    if (_channel == public_config.CHANNEL_ID_PRIVATE)
                    {
                        ChatManager.Instance.RequestSendChat(_channel, content, _playerName, (byte)ChatContentType.Item, _itemLinkList.ToArray());
                    }
                    else
                    {
                        ChatManager.Instance.RequestSendChat(_channel, content, 0, (byte)ChatContentType.Item, _itemLinkList.ToArray());
                    }
                }
                else
                {
                    if (_channel == public_config.CHANNEL_ID_PRIVATE)
                    {
                        ChatManager.Instance.RequestSendChat(_channel, content, _playerName, (byte)ChatContentType.Text);
                    }
                    else
                    {
                        ChatManager.Instance.RequestSendChat(_channel, content, 0, (byte)ChatContentType.Text);
                    }
                }
                _itemLinkList.Clear();
            }

            if (_channel == public_config.CHANNEL_ID_PRIVATE)
            {
                if (_isValidPrivateName)
                {
                    _input.text = _curPrivateInputInfo.chatPlayer;
                }
                else
                {
                    _input.text = string.Empty;
                }
            }
            else
            {
                _input.text = string.Empty;
            }
            _isValidPrivateName = true;
        }

        private string ProcessPrivateChatContent(string content)
        {
            string destContent = string.Empty;
            if (content.Trim() == NOT_HAVE_PRIVATE_CHAT_PLAYER_TIPS)
            {
                return destContent;
            }
            string chatPlayer = PRIVATE_CHAT_PLAYER_PATTERN.Match(content).Value;
            bool isFindChatPlayer = false;
            if (!string.IsNullOrEmpty(chatPlayer))
            {
                if (content.IndexOf(chatPlayer) == 0)
                {
                    isFindChatPlayer = true;
                    if (chatPlayer.IndexOf(PlayerAvatar.Player.name) == -1)
                    {
                        destContent = content.Substring(chatPlayer.Length, content.Length - chatPlayer.Length);
                        _curPrivateInputInfo.chatPlayer = chatPlayer;
                        _curPrivateInputInfo.chatContent = string.Empty;
                    }
                    else
                    {
                        _curPrivateInputInfo.chatPlayer = string.Empty;
                        _curPrivateInputInfo.chatContent = string.Empty;
                        _isValidPrivateName = false;
                        ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, ChatConst.NOT_SEND_PRIVATE_MSG_TO_SELF, PanelIdEnum.MainUIField);
                    }
                }
                else
                {
                    _isValidPrivateName = false;
                    ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(32554), PanelIdEnum.MainUIField);
                }
            }
            else
            {
                _isValidPrivateName = false;
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(32554), PanelIdEnum.MainUIField);
            }

            if (!isFindChatPlayer)
            {
                if (_playerId != 0)
                {
                    _curPrivateInputInfo.chatPlayer = string.Format(PRIVATE_CHAT_PLAYER_TEMPLATE, _playerName);
                    _curPrivateInputInfo.chatContent = string.Empty;
                }
            }
            return destContent;
        }

        private void OnMenuClick()
        {
            EventDispatcher.TriggerEvent(ChatEvents.CLOSE_EMOJI);
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83001), PanelIdEnum.MainUIField);
        }

        private void OnEmojiClick()
        {
            EventDispatcher.TriggerEvent(ChatEvents.OPEN_EMOJI);
        }

        private void OnSelectEmoji(int index)
        {
            HideHintBtn();
            string content = string.Format(EMOJI_TEMPLATE, index.ToString());
            if(ValidateContent(content) == true)
            {
                _input.text += content;
                _input.MoveTextEnd(false);
            }
        }

        private void OnSelectPhrase(string content)
        {
            HideHintBtn();
            if(ValidateContent(content) == true)
            {
                _input.text += content;
                _input.MoveTextEnd(false);
            }
        }

        private void OnSendFromMenu()
        {
            SendContent();
        }

        private void OnEndEdit(KInputField target)
        {
            SendContent();
        }

        private void OnValueChanged(string content)
        {
            UpdateCurPrivateInputInfo();
        }

        public void Refresh(int channel)
        {
            _channel = channel;
            _input = _normalInput;
            ShowInput(_normalInput);
            HideInput(_privateInput);
            ShowHintBtn();
        }

        public void RefreshPrivate(int channel, UInt64 playerId, string playerName)
        {
            _channel = channel;
            if (_channel != public_config.CHANNEL_ID_PRIVATE)
                return;
            _input = _privateInput;
            ShowInput(_privateInput);
            HideInput(_normalInput);
            HideHintBtn();
            if (string.IsNullOrEmpty(_playerName))
            {
                if (string.IsNullOrEmpty(playerName))
                {
                    _playerId = playerId;
                    _curPrivateInputInfo.chatPlayer = string.Empty;
                    _curPrivateInputInfo.chatContent = string.Empty;
                    _input.text = NOT_HAVE_PRIVATE_CHAT_PLAYER_TIPS;
                }
                else if (_playerName != playerName)
                {
                    _playerId = playerId;
                    _curPrivateInputInfo.chatPlayer = string.Format(PRIVATE_CHAT_PLAYER_TEMPLATE, playerName);
                    _curPrivateInputInfo.chatContent = string.Empty;
                    _input.text = _curPrivateInputInfo.chatPlayer;
                }
                _playerName = playerName;
            }
            else
            {
                if (!string.IsNullOrEmpty(playerName))
                {
                    if (_playerName != playerName)
                    {
                        _playerName = playerName;
                        _curPrivateInputInfo.chatPlayer = string.Format(PRIVATE_CHAT_PLAYER_TEMPLATE, playerName);
                        _curPrivateInputInfo.chatContent = string.Empty;
                        _input.text = _curPrivateInputInfo.chatPlayer;
                    }
                    else
                    {
                        _input.text = _curPrivateInputInfo.chatPlayer + _curPrivateInputInfo.chatContent;
                    }
                }
            }
        }

        public void UpdateCurPrivateInputInfo()
        {
            string content = _input.text;
            if (content.Trim() == string.Empty || content.Trim() == NOT_HAVE_PRIVATE_CHAT_PLAYER_TIPS)
            {
                ResetPrivatePlayerInfo();
                return;
            }
            string chatPlayer = PRIVATE_CHAT_PLAYER_PATTERN.Match(content).Value;
            if (!string.IsNullOrEmpty(chatPlayer))
            {
                if (content.IndexOf(chatPlayer) == 0)
                {
                    _curPrivateInputInfo.chatPlayer = chatPlayer;
                    _curPrivateInputInfo.chatContent = content.Substring(chatPlayer.Length, content.Length - chatPlayer.Length);
                    string playerName = chatPlayer.Remove(0, 1);
                    playerName = playerName.Remove(playerName.Length - 1, 1);
                    if (_playerName != playerName)
                    {
                        _playerName = playerName;
                        EventDispatcher.TriggerEvent<string>(ChatEvents.CHANGE_PRIVATE_PLAYER_NAME, _playerName);
                    }
                }
            }
            else
            {
                ResetPrivatePlayerInfo();
            }
        }

        private void ResetPrivatePlayerInfo()
        {
            if (!string.IsNullOrEmpty(_playerName))
            {
                _playerName = string.Empty;
                EventDispatcher.TriggerEvent<string>(ChatEvents.CHANGE_PRIVATE_PLAYER_NAME, _playerName);
            }
            _curPrivateInputInfo.chatPlayer = string.Empty;
            _curPrivateInputInfo.chatContent = string.Empty;
        }

        private void ShowInput(KInputField input)
        {
            if (!input.gameObject.activeSelf)
            {
                input.gameObject.SetActive(true);
            }
        }

        private void HideInput(KInputField input)
        {
            if (input.gameObject.activeSelf)
            {
                input.gameObject.SetActive(false);
            }
        }

        private void HideHintBtn()
        {
            if(_hintBtn.gameObject.activeSelf == true)
            {
                _hintBtn.gameObject.SetActive(false);
            }
        }

        private void ShowHintBtn()
        {
            if(_input.text.Trim() == string.Empty)
            {
                _hintBtn.gameObject.SetActive(true);
            }
        }

        public void AddItemLink(ChatItemLinkWrapper link)
        {
            HideHintBtn();
            string inputContent = GetLinkInputContent(link);
            if(ValidateContent(inputContent) == true)
            {
                _input.text += inputContent;
                _input.MoveTextEnd(false);
                _itemLinkList.Add(link);
            }
        }

        public void AddContent(string content)
        {
            if(ValidateContent(content) == true)
            {
                _input.text += content;
                _input.MoveTextEnd(false);
            }
        }

        public void SetContent(string content)
        {
            if (ValidateContent(content) == true)
            {
                _input.text = content;
                _input.MoveTextEnd(false);
            }
        }

        private bool ValidateContent(string content)
        {
            if (_channel == public_config.CHANNEL_ID_PRIVATE)
            {
                if (string.IsNullOrEmpty(_playerName) || _input.text.Trim() == NOT_HAVE_PRIVATE_CHAT_PLAYER_TIPS)
                {
                    return false;
                }
                else
                {
                    string chatPlayer = PRIVATE_CHAT_PLAYER_PATTERN.Match(_input.text).Value;
                    string chatContent = _input.text.Substring(chatPlayer.Length, _input.text.Length - chatPlayer.Length);
                    if (!string.IsNullOrEmpty(chatPlayer))
                    {
                        return (chatContent.Length + content.Length) <= MAX_CHAR_COUNT;
                    }
                    else
                    {
                        //私聊对象的格式不正确，但仍可以往后面添加内容
                        return (_input.text.Length + content.Length) <= MAX_CHAR_COUNT;
                    }
                }
            }
            else
            {
                return (_input.text.Length + content.Length) <= MAX_CHAR_COUNT;
            }
        }

        private string GetLinkInputContent(ChatItemLinkWrapper link)
        {
            string content = _chatData.CreateLinkContent(link);
            //if (link.itemLinkType == (int)ChatItemLinkWrapper.ItemLinkType.Normal)
            //{
            //    content = string.Format(ITEM_LINK_TEMPLATE, link.name, (int)ChatLinkType.Item, link.itemLinkType, link.bag, link.grid, link.quality);
            //}
            //else if (link.itemLinkType == (int)ChatItemLinkWrapper.ItemLinkType.Wing)
            //{
            //    content = string.Format(WING_ITEM_LINK_TEMPLATE, link.name, (int)ChatLinkType.Item, link.itemLinkType, link.itemId);
            //}
            //else if (link.itemLinkType == (int)ChatItemLinkWrapper.ItemLinkType.Title)
            //{
            //    content = string.Format(TITLE_ITEM_LINK_TEMPLATE, link.name, (int)ChatLinkType.Item, link.itemLinkType, link.itemId, link.getTime);
            //}
            //else if (link.itemLinkType == (int)ChatItemLinkWrapper.ItemLinkType.Achievement)
            //{
            //    content = string.Format(ACHIEVEMENT_ITEM_LINK_TEMPLATE, link.name, (int)ChatLinkType.Item, link.itemLinkType, link.achievementId);
            //}

            return content;
        }

        public class ChatPrivateInputInfo
        {
            public string chatPlayer = string.Empty;
            public string chatContent = string.Empty;
        }
    }
}
