﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using Common.Chat;

namespace ModuleChat
{
    public class ChatInputView : KContainer
    {
        private ChatKeyboardInputView _keyboardInput;
        private ChatVoiceInputView _voiceInput;

        private KButton _switchKeyboardBtn;
        private KButton _switchVoiceBtn;

        private int _channel;
        /// <summary>
        /// 私聊对象DbId
        /// </summary>
        private UInt64 _playerId;
        private string _playerName = string.Empty;

        public ChatVoiceInputView VoiceInputView
        {
            get { return _voiceInput; }
        }

        protected override void Awake()
        {
            _keyboardInput = AddChildComponent<ChatKeyboardInputView>("Container_wenzi");
            _voiceInput = AddChildComponent<ChatVoiceInputView>("Container_yuyin");
            _voiceInput.gameObject.SetActive(false);
            _switchKeyboardBtn = GetChildComponent<KButton>("Container_yuyin/Button_jianpan");
            _switchVoiceBtn = GetChildComponent<KButton>("Container_wenzi/Button_huatong");
            if (ChatManager.Instance.IsCancelChatVoiceFunction())
            {
                _switchKeyboardBtn.Visible = false;
                _switchVoiceBtn.Visible = false;
            }

            _switchKeyboardBtn.onClick.AddListener(OnSwitchKeyboardClick);
            _switchVoiceBtn.onClick.AddListener(OnSwitchVoiceClick);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<UInt64>(ChatEvents.SELECT_FRIEND, OnSelectFriend);
            EventDispatcher.AddEventListener<string>(ChatEvents.CHANGE_PRIVATE_PLAYER_NAME, OnChangePrivatePlayerName);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<UInt64>(ChatEvents.SELECT_FRIEND, OnSelectFriend);
            EventDispatcher.RemoveEventListener<string>(ChatEvents.CHANGE_PRIVATE_PLAYER_NAME, OnChangePrivatePlayerName);
        }

        private void OnSelectFriend(UInt64 friendDbid)
        {
            _channel = public_config.CHANNEL_ID_PRIVATE;
            //_keyboardInput.Refresh(_channel, friendDbid);
            //_voiceInput.Refresh(_channel, friendDbid);
        }

        private void OnChangePrivatePlayerName(string playerName)
        {
            //可能导致playerId和playerName不一致，后续考虑去掉playerId
            _voiceInput.RefreshPrivate(_channel, _playerId, playerName);
        }

        private void OnSwitchKeyboardClick()
        {
            EventDispatcher.TriggerEvent(ChatEvents.CLOSE_EMOJI);
            SwitchToKeyboard();
        }

        private void SwitchToKeyboard()
        {
            _voiceInput.gameObject.SetActive(false);
            _keyboardInput.gameObject.SetActive(true);
        }

        private void OnSwitchVoiceClick()
        {
            EventDispatcher.TriggerEvent(ChatEvents.CLOSE_EMOJI);
            SwitchToVoice();
        }

        private void SwitchToVoice()
        {
            _voiceInput.gameObject.SetActive(true);
            _keyboardInput.gameObject.SetActive(false);
        }

        public void Refresh(int channel)
        {
            _channel = channel;
            _keyboardInput.Refresh(_channel);
            _voiceInput.Refresh(_channel);
        }

        public void RefreshPrivate(int channel, PbFriendAvatarInfo avatarInfo)
        {
            _channel = channel;
            if (_channel != public_config.CHANNEL_ID_PRIVATE)
                return;
            if (avatarInfo != null)
            {
                _playerId = avatarInfo.dbid;
                _playerName = avatarInfo.name;
            }
            _keyboardInput.RefreshPrivate(_channel, _playerId, _playerName);
            _voiceInput.RefreshPrivate(_channel, _playerId, _playerName);
        }

        public void UpdateKeyboardPrivateInputInfo()
        {
            _keyboardInput.UpdateCurPrivateInputInfo();
        }

        public void AddItemLink(ChatItemLinkWrapper link)
        {
            SwitchToKeyboard();
            _keyboardInput.AddItemLink(link);
        }

        public void AddContent(string content)
        {
            SwitchToKeyboard();
            _keyboardInput.AddContent(content);
        }

        public void SetContent(string content)
        {
            SwitchToKeyboard();
            _keyboardInput.SetContent(content);
        }

        public void ConcatVoiceMessage(string str)
        {
            _voiceInput.ConcatVoiceMessage(str);
        }

        public void ResetStrVoice()
        {
            _voiceInput.ResetStrVoice();
        }
    }
}
