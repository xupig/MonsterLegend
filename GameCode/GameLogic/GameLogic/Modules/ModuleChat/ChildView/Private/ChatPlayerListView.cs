﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using Common.Chat;
using GameMain.Entities;
using Common.Utils;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ModuleCommonUI;
using Common.Global;
using Common.ExtendComponent;

namespace ModuleChat
{
    public class ChatFriendListView : KContainer
    {
        private KList _friendList;
        private KButton _findFriendBtn;
        private KInputField _findFriendInput;
        private KButton _friendOperationBtn;
        private KDummyButton _findFriendDummyBtn;

        public KComponentEvent<UInt64> onFriendSelected = new KComponentEvent<ulong>();

        private int _curFunction = FUNCTION_FRIEND_LIST;
        private bool _isFindFriend = false;
        private PbChatInfoResp _curChatFriendInfo;
        private string _defaultInputMsg = string.Empty;

        public bool IsFindFriend
        {
            get { return _isFindFriend; }
            set { _isFindFriend = value; }
        }

        public PbChatInfoResp CurChatFriendInfo
        {
            get { return _curChatFriendInfo; }
            set { _curChatFriendInfo = value; }
        }

        private ChatRightFriendListView _rightFriendListView;

        private const int MAX_CHAR_COUNT = 60;
        private const int FUNCTION_FIND = 1;  //搜索
        private const int FUNCTION_FRIEND_LIST = 2;  //显示好友列表

        protected override void Awake()
        {
            base.Awake();
            _defaultInputMsg = MogoLanguageUtil.GetString(LangEnum.FRIEND_SEARCH);//输入昵称查找
            _friendList = GetChildComponent<KList>("ScrollView_haoyou/mask/content");
            _friendList.SetPadding(5, 5, 5, 5);
            _friendList.SetGap(0, 0);
            _friendList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _findFriendBtn = GetChildComponent<KButton>("Container_sousuo/Button_dianjishuru");
            _findFriendInput = GetChildComponent<KInputField>("Container_sousuo/Input_input");
            _friendOperationBtn = GetChildComponent<KButton>("Container_sousuo/Button_sousuo");
            _findFriendInput.text = _defaultInputMsg;
            //_findFriendInput.text = string.Empty;
            _findFriendInput.characterLimit = MAX_CHAR_COUNT;
            _findFriendDummyBtn = AddChildComponent<KDummyButton>("Container_sousuo/ScaleImage_txtBg");
            _rightFriendListView = AddChildComponent<ChatRightFriendListView>("Container_xiaokuang");

            AddEventListener();
        }

        private void AddEventListener()
        {
            _friendList.onItemClicked.AddListener(OnFriendItemClick);
            _findFriendBtn.onClick.AddListener(OnFindFriendBtnClick);
            _findFriendInput.onSubmit.AddListener(OnFindFriendEndEdit);
            _friendOperationBtn.onClick.AddListener(OnFriendOperationBtnClick);
            _findFriendDummyBtn.onClick.AddListener(OnFindFriendBtnClick);
            EventDispatcher.AddEventListener<PbFriendAvatarInfo>(FriendEvents.GET_PLAYERINFO, OnRefreshFindFriendList);
            EventDispatcher.AddEventListener<KList, KList.KListItemBase>(ChatEvents.SELECT_PRIVATE_CHAT_RIGHT_FRIEND_ITEM, OnRightFriendItemSelect);
            EventDispatcher.AddEventListener(FriendEvents.FRIEND_UPDATE_ONLINE, OnRefreshRightFriendList);
        }

        private void RemoveEventListener()
        {
            _friendList.onItemClicked.RemoveListener(OnFriendItemClick);
            _findFriendBtn.onClick.RemoveListener(OnFindFriendBtnClick);
            _findFriendInput.onSubmit.RemoveListener(OnFindFriendEndEdit);
            _friendOperationBtn.onClick.RemoveListener(OnFriendOperationBtnClick);
            _findFriendDummyBtn.onClick.RemoveListener(OnFindFriendBtnClick);
            EventDispatcher.RemoveEventListener<PbFriendAvatarInfo>(FriendEvents.GET_PLAYERINFO, OnRefreshFindFriendList);
            EventDispatcher.RemoveEventListener<KList, KList.KListItemBase>(ChatEvents.SELECT_PRIVATE_CHAT_RIGHT_FRIEND_ITEM, OnRightFriendItemSelect);
            EventDispatcher.RemoveEventListener(FriendEvents.FRIEND_UPDATE_ONLINE, OnRefreshRightFriendList);
        }

        protected override void OnDestroy()
        {
            _curChatFriendInfo = null;
            RemoveEventListener();
        }

        private void OnFriendItemClick(KList target, KList.KListItemBase item)
        {
            PbChatInfoResp chatInfo = item.Data as PbChatInfoResp;
            onFriendSelected.Invoke(ChatUtil.GetFriendDbid(chatInfo));
        }

        private void OnRightFriendItemSelect(KList target, KList.KListItemBase item)
        {
            PbChatInfoResp chatInfo = CreateChatInfoFromFriendAvatarInfo(item.Data as PbFriendAvatarInfo);
            _isFindFriend = true;
            _curChatFriendInfo = chatInfo;
            onFriendSelected.Invoke(ChatUtil.GetFriendDbid(chatInfo));
        }

        private void OnFindFriendBtnClick()
        {
            _curFunction = FUNCTION_FIND;
            SetFriendListVisible(false);
            SetRightFriendListVisible(false);
            _findFriendBtn.Visible = false;
            _findFriendInput.gameObject.SetActive(true);
            _findFriendInput.text = _defaultInputMsg;
            EventDispatcher.TriggerEvent(ChatEvents.ENABLE_PRIVATE_CHAT_BACK_BTN);
        }

        private void OnFindFriendEndEdit(KInputField target)
        {
            
        }

        private void OnFriendOperationBtnClick()
        {
            if (_curFunction == FUNCTION_FIND)
            {
                if (!string.IsNullOrEmpty(_findFriendInput.text) && _findFriendInput.text != _defaultInputMsg)
                {
                    if (_findFriendInput.text == PlayerAvatar.Player.name)
                    {
                        MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.FRIEND_CANT_SEARCH_MYSELF));
                    }
                    else
                    {
                        FriendManager.Instance.SendSearchPlayerMsg(_findFriendInput.text);
                    }
                }
            }
            else if (_curFunction == FUNCTION_FRIEND_LIST)
            {
                if (_rightFriendListView.Visible == false)
                {
                    FriendManager.Instance.SendGetFriendOnlineListMsg();
                }
                else
                {
                    ChatPopupViewFadeIn.BeginHide(_rightFriendListView.gameObject);
                }
            }
        }

        public void Refresh(List<PbChatInfoResp> chatInfoList)
        {
            ResetFindFriendInfo();
            RefreshFriendList(chatInfoList);
            RefreshBtns();
            SetRightFriendListVisible(false);
        }

        private void SetFriendListVisible(bool isVisible)
        {
            if (_friendList.Visible != isVisible)
            {
                _friendList.Visible = isVisible;
            }
        }

        private void RefreshFriendList(List<PbChatInfoResp> chatInfoList)
        {
            SetFriendListVisible(true);
            _friendList.RemoveAll();
            _friendList.SetDataList<ChatFriendListItem>(chatInfoList);
        }

        private void RefreshBtns()
        {
            _curFunction = FUNCTION_FRIEND_LIST;
            _findFriendBtn.Visible = true;
            _findFriendInput.gameObject.SetActive(false);
            _findFriendInput.text = string.Empty;
        }

        private void SetFriendOperationBtnText(string text)
        {
            StateText btnText = _friendOperationBtn.GetChildComponent<StateText>("label");
            Text[] texts = btnText.GetAllStateTextComponent();
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].text = text;
            }
        }

        private void OnRefreshFindFriendList(PbFriendAvatarInfo friendAvatarInfo)
        {
            PbChatInfoResp chatInfoResp = CreateChatInfoFromFriendAvatarInfo(friendAvatarInfo);
            List<PbChatInfoResp> chatInfoList = new List<PbChatInfoResp>();
            chatInfoList.Add(chatInfoResp);
            RefreshFriendList(chatInfoList);
            _isFindFriend = true;
            _curChatFriendInfo = chatInfoResp;
        }

        private void OnRefreshRightFriendList()
        {
            FriendData friendData = PlayerDataManager.Instance.FriendData;
            if (friendData.OnlineSortedFriendList.Count == 0)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(57030), PanelIdEnum.MainUIField);
            }
            else
            {
                _rightFriendListView.Visible = true;
                _rightFriendListView.Refresh();
            }
        }

        private PbChatInfoResp CreateChatInfoFromFriendAvatarInfo(PbFriendAvatarInfo friendAvatarInfo)
        {
            PbChatInfoResp chatInfoResp = new PbChatInfoResp();
            chatInfoResp.send_dbid = friendAvatarInfo.dbid;
            chatInfoResp.send_name = friendAvatarInfo.name;
            chatInfoResp.send_level = friendAvatarInfo.level;
            chatInfoResp.send_vocation = friendAvatarInfo.vocation;
            return chatInfoResp;
        }

        public void ResetFindFriendInfo()
        {
            _isFindFriend = false;
            _curChatFriendInfo = null;
        }

        private void RefreshRightFriendList()
        {
            if (_rightFriendListView.Visible == true)
            {
                _rightFriendListView.Refresh();
            }
        }

        private void SetRightFriendListVisible(bool isVisible)
        {
            if (_rightFriendListView.Visible != isVisible)
            {
                _rightFriendListView.Visible = isVisible;
            }
        }

        public class ChatFriendListItem : KList.KListItemBase, IPointerEnterHandler, IPointerExitHandler
        {
            private PbChatInfoResp _data;
            private KButton _addFriendBtn;
            private IconContainer _icon;
            private StateText _nameTxt;
            private StateText _levelTxt;
            private KButton _btn;
            private StateImage _pointImg;
            private StateImage _selectImg;

            protected override void Awake()
            {
                _addFriendBtn = GetChildComponent<KButton>("Button_haoyou");
                _icon = AddChildComponent<IconContainer>("Container_icon");
                _nameTxt = GetChildComponent<StateText>("Label_txtwanjiamingzi");
                _levelTxt = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
                _btn = GetChildComponent<KButton>("Button_btn");
                _pointImg = GetChildComponent<StateImage>("Image_xiaodian");
                _selectImg = GetChildComponent<StateImage>("Image_xuanzhongkuang");
                _selectImg.gameObject.AddComponent<RaycastComponent>();
                _selectImg.Visible = false;
                _btn.enabled = false;
                AddEventListener();
            }

            private void AddEventListener()
            {
                this.onClick.AddListener(OnItemClick);
                _addFriendBtn.onClick.AddListener(OnAddFriend);
            }

            private void RemoveEventListener()
            {
                this.onClick.RemoveListener(OnItemClick);
                _addFriendBtn.onClick.RemoveListener(OnAddFriend);
            }

            private void OnAddFriend()
            {
                Vector3 targetPos = new Vector3(941f, 5000f, 5000f);
                PlayerInfoParam param = new PlayerInfoParam(targetPos, _addFriendBtn.GetComponent<RectTransform>().sizeDelta,
                    PlayerInfoParam.AlignmentType.SpecifyPos, PlayerInfoParam.AlignmentType.ScreenCenter);
                FriendManager.Instance.GetPlayerDetailInfo(ChatUtil.GetFriendName(_data), param);
            }

            private void OnItemClick(KList.KListItemBase item, int index)
            {
                _pointImg.Visible = false;
            }

            public virtual void OnPointerEnter(PointerEventData eventData)
            {
                _selectImg.Visible = true;
            }

            public virtual void OnPointerExit(PointerEventData eventData)
            {
                _selectImg.Visible = false;
            }

            public override object Data
            {
                get
                {
                    return _data;
                }
                set
                {
                    _data = value as PbChatInfoResp;
                    Refresh();
                }
            }

            private void Refresh()
            {
                _nameTxt.CurrentText.text = ChatUtil.GetFriendName(_data);
                _levelTxt.CurrentText.text = ChatUtil.GetFriendLevel(_data).ToString();
                _icon.SetIcon(MogoPlayerUtils.GetSmallIcon((int)ChatUtil.GetFriendVocation(_data)));
                RefreshNewMsgPoint();
            }

            private void RefreshNewMsgPoint()
            {
                ChatData chatData = PlayerDataManager.Instance.ChatData;
                _pointImg.Visible = chatData.CheckPrivateChatPlayerHasNewMsg(ChatUtil.GetFriendDbid(_data));
            }

            public override void Dispose()
            {
                _data = null;
                RemoveEventListener();
            }
        }
    }
}
