﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using Common.Chat;

namespace ModuleChat
{
    public class PrivateChannelContentView : ChatAllChannelContentView
    {
        private StateText _displayTxt;
        private KButton _returnBtn;
        private string _displayTemplate;
        private string _friendName;
        private UInt64 _friendDbid;

        public KComponentEvent onChannelReturn = new KComponentEvent();

        protected override void Awake()
        {
            base.Awake();
            _displayTxt = GetChildComponent<StateText>("Container_biaoti/Label_txtMiliao");
            _displayTemplate = _displayTxt.CurrentText.text;
            _returnBtn = GetChildComponent<KButton>("Container_biaoti/Button_fanhui");
            KContainer titleContainer = GetChildComponent<KContainer>("Container_biaoti");
            titleContainer.Visible = false;
            AddBtnEventListener();
        }

        protected override GameObject GetTemplateScrollViewGo()
        {
            GameObject panelGo = this.gameObject.GetComponentInParent<ChatPanel>().gameObject;
            return panelGo.transform.FindChild("Container_ChatTextStyleContentList/ScrollView_chatView").gameObject;
        }

        protected override void ConfigList()
        {
            _list.SetPadding(2, 1, 2, 1);
            _list.SetGap(4, 4);
        }

        private void AddBtnEventListener()
        {
            _returnBtn.onClick.AddListener(OnReturnClick);
        }

        private void RemoveBtnEventListener()
        {
            _returnBtn.onClick.RemoveListener(OnReturnClick);
        }

        private void OnReturnClick()
        {
            onChannelReturn.Invoke();
        }

        public void SetFriendName(string friendName)
        {
            _friendName = friendName;
        }

        public void SetFriendDbid(UInt64 friendDbid)
        {
            _friendDbid = friendDbid;
        }

        public void SetFriendChatInfoList(List<PbChatInfoResp> chatInfoList)
        {
            _list.RemoveAll();
            for(int i = 0; i < chatInfoList.Count; i++)
            {
                _list.AddItemAt(_itemType, chatInfoList[i], _list.ItemList.Count, false);
            }
            RecalculateItemPosition();
            _list.RecalculateSize();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            RefreshFriendName();
        }

        private void RefreshFriendName()
        {
            if(string.IsNullOrEmpty(_friendName) == false)
            {
                _displayTxt.CurrentText.text = string.Format(_displayTemplate, _friendName);
            }
        }

        protected override void OnDestroy()
        {
            RemoveBtnEventListener();
            base.OnDestroy();
        }
    }
}
