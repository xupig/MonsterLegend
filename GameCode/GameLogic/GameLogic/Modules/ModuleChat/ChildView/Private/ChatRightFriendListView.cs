﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.Data;
using GameMain.GlobalManager;
using Common.Structs.ProtoBuf;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;

namespace ModuleChat
{
    public class ChatRightFriendListView : KContainer
    {
        private KList _contentList;
        private float _distanceYFromChatPanel = 0f;
        private bool _isInitFadeInInfo = false;

        protected override void Awake()
        {
            base.Awake();
            _contentList = GetChildComponent<KList>("ScrollView_friendList/mask/content");
            InitScrollView();
            SetParentToMask();
            AddEventListener();
        }

        private void InitScrollView()
        {
            _contentList.SetPadding(10, 10, 2, 25);
            _contentList.SetGap(7, 1);
            _contentList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        private void SetParentToMask()
        {
            GameObject maskGo = transform.parent.FindChild("ScaleImage_mask").gameObject;
            ChatUtil.SetPopupViewParentToMask(maskGo, gameObject);
        }

        private void InitFadeInInfo()
        {
            RectTransform[] parentRects = gameObject.GetComponentsInParent<RectTransform>();
            int findParentCount = 0;
            for (int i = 0; i < parentRects.Length; i++)
            {
                string parentName = parentRects[i].gameObject.name;
                if (parentName == "Container_ChatMiliao" || parentName == "Container_liebiao")
                {
                    _distanceYFromChatPanel += -parentRects[i].anchoredPosition.y;
                    findParentCount++;
                    if (findParentCount == 2)
                    {
                        break;
                    }
                }
            }
        }

        private void AddEventListener()
        {
            _contentList.onItemClicked.AddListener(OnFriendItemClick);
        }

        private void RemoveEventListener()
        {
            _contentList.onItemClicked.RemoveListener(OnFriendItemClick);
        }

        public void Refresh()
        {
            if (!_isInitFadeInInfo)
            {
                InitFadeInInfo();
                _isInitFadeInInfo = true;
            }
            ChatPopupViewFadeIn.BeginShow(gameObject, _distanceYFromChatPanel, UIManager.CANVAS_HEIGHT);
            FriendData friendData = PlayerDataManager.Instance.FriendData;
            _contentList.SetDataList<RightFriendListItem>(friendData.OnlineSortedFriendList);
        }

        private void OnFriendItemClick(KList target, KList.KListItemBase item)
        {
            EventDispatcher.TriggerEvent<KList, KList.KListItemBase>(ChatEvents.SELECT_PRIVATE_CHAT_RIGHT_FRIEND_ITEM, target, item);
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }

        public class RightFriendListItem : KList.KListItemBase
        {
            private PbFriendAvatarInfo _data;
            private KButton _itemBtn;

            public override object Data
            {
                get
                {
                    return _data;
                }
                set
                {
                    _data = value as PbFriendAvatarInfo;
                    Refresh();
                }
            }

            protected override void Awake()
            {
                _itemBtn = GetChildComponent<KButton>("Button_item");
                _itemBtn.onClick.AddListener(OnClickItemBtn);
            }

            public override void OnPointerClick(PointerEventData evtData)
            {
                
            }

            private void Refresh()
            {
                if (_data.online == 1)
                {
                    _itemBtn.interactable = true;
                }
                else
                {
                    _itemBtn.interactable = false;
                }
                StateText btnLabel = _itemBtn.Label;
                if (btnLabel != null)
                {
                    Text text = btnLabel.CurrentText;
                    if (text.gameObject.name == KButtonState.DISABLE)
                    {
                        text.color = new Color(98f / 255f, 98f / 255f, 98f / 255f, 1f);
                    }
                }
                List<IStateChangeable> changeables = _itemBtn.GetStateChangeableChildren();
                for (int i = 0; i < changeables.Count; i++)
                {
                    if (changeables[i] is StateText)
                    {
                        StateText stateText = changeables[i] as StateText;
                        stateText.ChangeAllStateText(_data.name);
                    }
                }

            }

            private void OnClickItemBtn()
            {
                onClick.Invoke(this, this.Index);
            }

            public override void Dispose()
            {
                _itemBtn.onClick.RemoveListener(OnClickItemBtn);
            }
        }
    }
}
