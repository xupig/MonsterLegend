﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using Common.Chat;
using GameMain.Entities;

namespace ModuleChat
{
    public class ChatPrivateView : KContainer
    {
        private PrivateChannelContentView _channelContentView;
        public PrivateChannelContentView PrivateContentView
        {
            get { return _channelContentView; }
        }

        protected override void Awake()
        {
            base.Awake();
            _channelContentView = AddChildComponent<PrivateChannelContentView>("Container_liaotian");
            _channelContentView.Channel = public_config.CHANNEL_ID_PRIVATE;
            _channelContentView.IsForceContentMoveBottom = true;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            if (_channelContentView.Visible)
            {
                _channelContentView.Visible = false;
            }
        }

        public void ShowChannelContentView()
        {
            _channelContentView.CheckIsExsitScrollViewGo();
            _channelContentView.Visible = true;
        }

        private void HideChannelContentView()
        {
            _channelContentView.Visible = false;
        }

    }
}
