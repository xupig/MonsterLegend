﻿using System;
using System.Collections.Generic;
using Common.Base;
using Common.Data;
using Game.UI.UIComponent;
using Common.ExtendComponent;
using GameMain.GlobalManager;

namespace ModuleChat
{
    public class RedEnvelopePanel : BasePanel
    {
        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.RedEnvelope; }
        }

        private RedEnvelopeView _redEnvelopeView;
        private RedEnvelopeOpenEffect _openEffect;
        private KParticle _openEnvelopeParticle;

        private RedEnvelopeInfo _redEnvelopeInfo;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_zhankaihongbao/Button_close");
            _redEnvelopeView = AddChildComponent<RedEnvelopeView>("Container_zhankaihongbao");
            ModalMask = GetChildComponent<StateImage>("Container_zhankaihongbao/ScaleImage_sharedZhezhao");
            _openEffect = AddChildComponent<RedEnvelopeOpenEffect>("Container_particleEffect");
            _openEnvelopeParticle = _openEffect.transform.FindChild("fx_ui_yanhua_01").gameObject.AddComponent<KParticle>();
            _openEffect.Init(_openEnvelopeParticle);
        }

        public override void OnShow(object data)
        {
            SetData(data);
            _redEnvelopeInfo = data as RedEnvelopeInfo;
            if (_redEnvelopeInfo != null)
            {
                if (_redEnvelopeInfo.rewardList != null && _redEnvelopeInfo.rewardList.Count > 0)
                {
                    _openEffect.gameObject.SetActive(true);
                    _openEffect.StartPlay();
                }
                else
                {
                    _openEffect.gameObject.SetActive(false);
                }
            }
            else
            {
                _openEffect.gameObject.SetActive(false);
            }
        }

        public override void SetData(object data)
        {
            RedEnvelopeInfo envelopeInfo = data as RedEnvelopeInfo;
            if (envelopeInfo != null)
            {
                _redEnvelopeView.Visible = true;
                _redEnvelopeView.SetData(envelopeInfo);
                _redEnvelopeView.Refresh();
            }
            else
            {
                _redEnvelopeView.Visible = false;
            }
        }

        public override void OnClose()
        {
            _openEffect.StopPlay();
            UIManager.Instance.ShowPanel(PanelIdEnum.Chat);
        }
    }
}
