﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using UnityEngine;
using MogoEngine.Events;
using Common.Events;
using System.Text.RegularExpressions;
using Common.Data;
using Common.Chat;

namespace ModuleChat
{
    public class RightVoiceChatItemView : KContainer
    {
        protected PbChatInfoResp _data;
        protected KContainer _playerIcon;
        protected StateText _playerLvText;
        protected StateText _playerNameText;
        protected KDummyButton _playVoiceBtn;
        protected StateText _voiceTimeLengthText;
        protected KDummyButton _playerInfoBtn;
        protected GameObject _voiceAnimGo;

        private static readonly string VOICE_DURATION_TEMPLETE = "{0}''";

        public PbChatInfoResp Data
        {
            get { return _data; }
            set
            {
                _data = value;
                Refresh();
            }
        }

        protected override void Awake()
        {
            _playerIcon = GetChildComponent<KContainer>("Container_icon");
            _playerLvText = GetChildComponent<StateText>("Label_txtDengji");
            _playerNameText = GetChildComponent<StateText>("Label_txtName");
            _voiceTimeLengthText = GetChildComponent<StateText>("Label_txtShichang");
            _playerInfoBtn = _playerIcon.gameObject.AddComponent<KDummyButton>();
            DisableOriginSpeakerImg();
            InitVoiceAnimGo();
            InitPlayVoiceBtn();
            ResizeBackground();
        }

        protected virtual void InitPlayVoiceBtn()
        {
            _playVoiceBtn = GetChildComponent<StateImage>("ScaleImage_yuyinKuangBieren").gameObject.AddComponent<KDummyButton>();
        }

        protected virtual void InitVoiceAnimGo()
        {
            _voiceAnimGo = ChatVoiceAnimPlayPool.Instance.AddAnimationGo(VoiceAnimTemplateId.RightVoice);
            _voiceAnimGo.transform.SetParent(this.transform);
            _voiceAnimGo.transform.localScale = Vector3.one;
            RectTransform originRect = GetChildComponent<StateImage>("Image_shengyin_left").GetComponent<RectTransform>();
            RectTransform voiceAnimRect = _voiceAnimGo.GetComponent<RectTransform>();
            voiceAnimRect.anchoredPosition = originRect.anchoredPosition;
        }

        protected virtual void DisableOriginSpeakerImg()
        {
            GetChildComponent<StateImage>("Image_shengyin_left").Visible = false;
        }

        protected virtual void ResizeBackground()
        {
            RectTransform bgRect = GetChildComponent<RectTransform>("ScaleImage_yuyinKuangBieren");
            Vector2 originPos = bgRect.anchoredPosition;
            float deltaWidth = bgRect.sizeDelta.x - 334f;
            bgRect.sizeDelta = new Vector2(334f, bgRect.sizeDelta.y);
            bgRect.anchoredPosition = new Vector2(originPos.x + deltaWidth, bgRect.anchoredPosition.y);
            RecalculateSize();
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            ChatVoiceAnimPlayPool.Instance.StopPlay(_voiceAnimGo.name);
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _playVoiceBtn.onClick.AddListener(OnClickPlayVoiceBtn);
            _playerInfoBtn.onClick.AddListener(OnClickPlayerInfoBtn);
            EventDispatcher.AddEventListener<string>(ChatEvents.SDK_START_PLAY_VOICE, OnStartPlayVoice);
            EventDispatcher.AddEventListener<string>(ChatEvents.END_PLAY_VOICE, OnEndPlayVoice);
        }

        private void RemoveEventListener()
        {
            _playVoiceBtn.onClick.RemoveListener(OnClickPlayVoiceBtn);
            _playerInfoBtn.onClick.RemoveListener(OnClickPlayerInfoBtn);
            EventDispatcher.RemoveEventListener<string>(ChatEvents.SDK_START_PLAY_VOICE, OnStartPlayVoice);
            EventDispatcher.RemoveEventListener<string>(ChatEvents.END_PLAY_VOICE, OnEndPlayVoice);
        }

        protected virtual void OnClickPlayVoiceBtn()
        {
            PlayVoice();
        }

        private void OnStartPlayVoice(string path)
        {
            if (path.IndexOf(_data.voice_id) == -1)
                return;
            ChatVoiceAnimPlayPool.Instance.PlayAnim(_voiceAnimGo.name);
        }

        private void OnEndPlayVoice(string path)
        {
            if (path.IndexOf(_data.voice_id) == -1)
                return;
            ChatVoiceAnimPlayPool.Instance.StopPlay(_voiceAnimGo.name);
        }

        protected void PlayVoice()
        {
            string voiceId = _data.voice_id;
            if (!string.IsNullOrEmpty(voiceId))
            {
                EventDispatcher.TriggerEvent<string, ulong>(ChatEvents.PLAY_VOICE, voiceId, _data.send_dbid);
            }
        }

        private void OnClickPlayerInfoBtn()
        {
            if (_data.send_dbid == PlayerAvatar.Player.dbid)
                return;
            PlayerInfoParam param = new PlayerInfoParam(_playerInfoBtn.transform.position, _playerInfoBtn.GetComponent<RectTransform>().sizeDelta);
            FriendManager.Instance.GetPlayerDetailInfo(_data.send_name, param);
        }

        protected virtual void Refresh()
        {
            RefreshPlayerIcon();
            _playerLvText.CurrentText.text = _data.send_level.ToString();
            _playerNameText.CurrentText.text = _data.send_name;
            _voiceTimeLengthText.CurrentText.text = GetDurationDesc();
        }

        private void RefreshPlayerIcon()
        {
            MogoAtlasUtils.AddIcon(_playerIcon.gameObject, MogoPlayerUtils.GetSmallIcon((int)_data.send_vocation));
        }

        private string GetDurationDesc()
        {
            int duration = 0;
            if (ChatData.VOICE_DURATION_PATTERN.IsMatch(_data.msg))
            {
                duration = int.Parse(ChatData.VOICE_DURATION_PATTERN.Match(_data.msg).Value);
                duration = Mathf.CeilToInt(duration * 1.0f / 1000.0f);
            }
            return string.Format(VOICE_DURATION_TEMPLETE, duration);
        }

        protected override void OnDestroy()
        {
            if (_voiceAnimGo != null)
            {
                ChatVoiceAnimPlayPool.Instance.RemoveAnimGo(_voiceAnimGo.name);
                _voiceAnimGo = null;
            }
        }
    }
}
