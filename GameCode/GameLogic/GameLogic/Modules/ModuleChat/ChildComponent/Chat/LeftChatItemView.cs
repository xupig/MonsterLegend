﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Chat;
using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using Common.Utils;
using GameMain.Entities;
using UnityEngine.UI;
using MogoEngine.Utils;
using Common.Global;
using Common.ExtendComponent;
using GameLoader.Utils.Timer;

namespace ModuleChat
{
    public class LeftChatItemView : KContainer
    {
        protected const float TEXT_WIDTH = 320.0f;
        protected KContainer _icon;
        protected StateText _nameTxt;
        protected Locater _nameLocater;
        protected StateText _levelTxt;
        protected StateImage _bgImage;
        protected Locater _bgLocater;
        protected KDummyButton _itemDummyBtn;
        protected string _itemLink;
        protected KDummyButton _playerInfoBtn;

        protected ChatTextBlock _block;

        protected PbChatInfoResp _data;

        protected ChatMsgPassedTimeComponent _msgPassedTimeComponent;

        protected KComponentEvent<GameObject> _onCreatePassedTimeInNewLine;
        public KComponentEvent<GameObject> onCreatePassedTimeInNewLine
        {
            get
            {
                if (_onCreatePassedTimeInNewLine == null)
                {
                    _onCreatePassedTimeInNewLine = new KComponentEvent<GameObject>();
                }
                return _onCreatePassedTimeInNewLine;
            }
        }
        
        protected override void Awake()
        {
            _icon = GetChildComponent<KContainer>("Container_icon");
            _playerInfoBtn = AddChildComponent<KDummyButton>("Container_icon");
            _nameTxt = GetChildComponent<StateText>("Label_txtName");
            _nameLocater = AddChildComponent<Locater>("Label_txtName");
            _levelTxt = GetChildComponent<StateText>("Label_txtDengji");
            _msgPassedTimeComponent = this.gameObject.AddComponent<ChatMsgPassedTimeComponent>();
            InitBgImage();
            AddEventListener();
        }

        protected virtual void InitBgImage()
        {
            _bgImage = GetChildComponent<StateImage>("ScaleImage_yuyinKuangZiji");
            _bgImage.AddAllStateComponent<Resizer>();
            _bgLocater = AddChildComponent<Locater>("ScaleImage_yuyinKuangZiji");
        }

        protected void AddEventListener()
        {
            _playerInfoBtn.onClick.AddListener(OnPlayerInfoClick);
            _msgPassedTimeComponent.onCreateTimeText.AddListener(OnCreatePassedTime);
        }

        protected void RemoveEventListener()
        {
            _playerInfoBtn.onClick.RemoveListener(OnPlayerInfoClick);
            _msgPassedTimeComponent.onCreateTimeText.RemoveListener(OnCreatePassedTime);
        }

        private void OnPlayerInfoClick()
        {
            if (_data.send_dbid == PlayerAvatar.Player.dbid)
                return;
            Vector3 targetPos = new Vector3(941f, 5000f, 5000f);
            PlayerInfoParam param = new PlayerInfoParam(targetPos, _playerInfoBtn.GetComponent<RectTransform>().sizeDelta, 
                PlayerInfoParam.AlignmentType.SpecifyPos, PlayerInfoParam.AlignmentType.ScreenCenter);
            FriendManager.Instance.GetPlayerDetailInfo(_data.send_name, param);
        }

        private void OnCreatePassedTime(GameObject componentGo, bool isInNewLine)
        {
            ResizeAfterShowPassedTime();
            if (isInNewLine)
            {
                if (_onCreatePassedTimeInNewLine != null)
                {
                    _onCreatePassedTimeInNewLine.Invoke(componentGo);
                }
            }
        }

        public PbChatInfoResp Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
                Refresh();
            }
        }

        private void Refresh()
        {
            RefreshIcon();
            _levelTxt.CurrentText.text = _data.send_level.ToString();
            _nameTxt.CurrentText.text = _data.send_name;
            AddTextBlock();
            _msgPassedTimeComponent.SetData(_data, _block, TEXT_WIDTH, "templete/Label_txtPassedTime");
            AdjustLayout();
        }

        protected override void OnEnable()
        {
            EventDispatcher.AddEventListener<GameObject, GameObject, int>(ChatEvents.ON_CLICK_ACHIEVEMENT_LINK, OnShowAchievementTips);
        }

        protected override void OnDisable()
        {
            EventDispatcher.RemoveEventListener<GameObject, GameObject, int>(ChatEvents.ON_CLICK_ACHIEVEMENT_LINK, OnShowAchievementTips);
        }

        private void OnShowAchievementTips(GameObject blockGo, GameObject textGo, int achieveId)
        {
            if (_block != null && _block.GameObject == blockGo)
            {
                AchievementTipsParam param = new AchievementTipsParam();
                param.achieveId = achieveId;
                param.targetPos = _bgImage.transform.position;
                param.targetSizeDelta = new Vector2(_bgLocater.Width, _bgLocater.Height);
                UIManager.Instance.ShowPanel(PanelIdEnum.AchievementTips, param);
            }
        }

        private void RefreshIcon()
        {
            MogoAtlasUtils.AddIcon(_icon.gameObject, MogoPlayerUtils.GetSmallIcon((int)_data.send_vocation));
        }

        private void AddTextBlock()
        {
            _block = ChatTextBlock.CreateBlock(_data, TEXT_WIDTH, Color.black);
            _block.GameObject.transform.SetParent(gameObject.transform);
            RectTransform rect = _block.GameObject.GetComponent<RectTransform>();
            rect.localScale = Vector3.one;
        }

        protected virtual void AdjustLayout()
        {
            RectTransform rect = _block.GameObject.GetComponent<RectTransform>();
            _bgImage.Width = rect.sizeDelta.x + 25.0f;
            if(_block.LineCount == 1)
            {
                _nameLocater.Y = -5.0f;
                float height = 50;
                if (_msgPassedTimeComponent.IsPassedTimeInNewLine)
                {
                    RectTransform textRect = _msgPassedTimeComponent.PassedTimeTextGo.GetComponent<RectTransform>();
                    height += textRect.sizeDelta.y;
                }
                _bgImage.Height = height;
                _bgLocater.Y = -37.0f;
                rect.anchoredPosition = new Vector2(118.0f, -48.0f);
                if (_msgPassedTimeComponent.IsPassedTimeInNewLine)
                {
                    RecalculateSize();
                }
            }
            else if (_block.LineCount >= 3)
            {
                float height = _block.Height + (float)Math.Round(_block.Height / _block.LineCount, 2) / 2;
                if (_msgPassedTimeComponent.IsPassedTimeInNewLine)
                {
                    RectTransform textRect = _msgPassedTimeComponent.PassedTimeTextGo.GetComponent<RectTransform>();
                    height += textRect.sizeDelta.y;
                }
                _bgImage.Height = height;
                _nameLocater.Y = -5.0f;
                _bgLocater.Y = -37.0f;
                rect.anchoredPosition = new Vector2(118.0f, -44.0f);
                RecalculateSize();
            }
            else
            {
                rect.anchoredPosition = new Vector2(118.0f, -35.0f);
                float height = _bgImage.Height;
                if (_msgPassedTimeComponent.IsPassedTimeInNewLine)
                {
                    RectTransform textRect = _msgPassedTimeComponent.PassedTimeTextGo.GetComponent<RectTransform>();
                    height += textRect.sizeDelta.y;
                    _bgImage.Height = height;
                    RecalculateSize();
                }
            }
            
        }

        protected void ResizeAfterShowPassedTime()
        {
            if (_msgPassedTimeComponent.IsPassedTimeInNewLine)
            {
                if (_block.LineCount == 1)
                {
                    float height = 50;
                    RectTransform textRect = _msgPassedTimeComponent.PassedTimeTextGo.GetComponent<RectTransform>();
                    height += textRect.sizeDelta.y;
                    _bgImage.Height = height;
                    RecalculateSize();
                }
                else if (_block.LineCount >= 3)
                {
                    float height = _block.Height + (float)Math.Round(_block.Height / _block.LineCount, 2) / 2;
                    RectTransform textRect = _msgPassedTimeComponent.PassedTimeTextGo.GetComponent<RectTransform>();
                    height += textRect.sizeDelta.y;
                    _bgImage.Height = height;
                    RecalculateSize();
                }
                else
                {
                    float height = _bgImage.Height;
                    RectTransform textRect = _msgPassedTimeComponent.PassedTimeTextGo.GetComponent<RectTransform>();
                    height += textRect.sizeDelta.y;
                    _bgImage.Height = height;
                    RecalculateSize();
                }
            }
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }
    }
}
