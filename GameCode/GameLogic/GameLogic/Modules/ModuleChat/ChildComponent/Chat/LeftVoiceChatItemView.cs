﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.Structs.ProtoBuf;
using Common.Chat;
using UnityEngine;
using Common.Data;
using GameMain.GlobalManager;

namespace ModuleChat
{
    public class LeftVoiceChatItemView : RightVoiceChatItemView
    {
        private StateImage _notPlayTips;

        protected override void Awake()
        {
            _notPlayTips = GetChildComponent<StateImage>("Image_weitingtishi");
            base.Awake();
        }

        protected override void InitPlayVoiceBtn()
        {
            _playVoiceBtn = GetChildComponent<RectTransform>("ScaleImage_yuyinKuangZiji").gameObject.AddComponent<KDummyButton>();
        }

        protected override void InitVoiceAnimGo()
        {
            _voiceAnimGo = ChatVoiceAnimPlayPool.Instance.AddAnimationGo(VoiceAnimTemplateId.LeftVoice);
            _voiceAnimGo.transform.SetParent(this.transform);
            _voiceAnimGo.transform.localScale = Vector3.one;
            RectTransform voiceAnimRect = _voiceAnimGo.GetComponent<RectTransform>();
            voiceAnimRect.anchoredPosition = new Vector2(125f, -36f);
        }

        protected override void DisableOriginSpeakerImg()
        {
            GetChildComponent<StateImage>("Image_shengyin_right").Visible = false;
        }

        protected override void ResizeBackground()
        {
            RectTransform bgRect = GetChildComponent<RectTransform>("ScaleImage_yuyinKuangZiji");
            float deltaWidth = 334f - bgRect.sizeDelta.x;
            bgRect.sizeDelta = new Vector2(334f, bgRect.sizeDelta.y);
            RectTransform tipsPointRect = GetChildComponent<RectTransform>("Image_weitingtishi");
            tipsPointRect.anchoredPosition = new Vector2(tipsPointRect.anchoredPosition.x + deltaWidth, tipsPointRect.anchoredPosition.y);
            RecalculateSize();
        }

        protected override void OnClickPlayVoiceBtn()
        {
            ChatData chatData = PlayerDataManager.Instance.ChatData;
            if (!chatData.IsVoiceHasPlayed(_data.voice_id))
            {
                chatData.SetVoiceHasPlayed(_data.voice_id);
            }
            if (_notPlayTips.Visible == true)
            {
                _notPlayTips.Visible = false;
            }
            //播放语音
            PlayVoice();
        }

        protected override void Refresh()
        {
            base.Refresh();
            SetNotPlayTipsInvisible();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            SetNotPlayTipsInvisible();
        }

        private void SetNotPlayTipsInvisible()
        {
            if (_data == null || !_notPlayTips.Visible)
                return;
            ChatData chatData = PlayerDataManager.Instance.ChatData;
            if (chatData.IsVoiceHasPlayed(_data.voice_id) && _notPlayTips.Visible == true)
            {
                _notPlayTips.Visible = false;
            }
        }
    }
}
