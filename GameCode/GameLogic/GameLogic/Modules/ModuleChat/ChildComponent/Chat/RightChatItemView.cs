﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;

namespace ModuleChat
{
    public class RightChatItemView : LeftChatItemView
    {
        protected override void InitBgImage()
        {
            _bgImage = GetChildComponent<StateImage>("ScaleImage_yuyinKuangBieren");
            _bgImage.AddAllStateComponent<Resizer>();
            _bgLocater = AddChildComponent<Locater>("ScaleImage_yuyinKuangBieren");
        }

        protected override void AdjustLayout()
        {
            float oldWidth = _bgImage.Width;
            RectTransform rect = _block.GameObject.GetComponent<RectTransform>();
            _bgImage.Width = rect.sizeDelta.x + 25.0f;
            _bgLocater.X += oldWidth - _bgImage.Width;
            if(_block.LineCount == 1)
            {
                _nameLocater.Y = -5.0f;
                float height = 50;
                if (_msgPassedTimeComponent.IsPassedTimeInNewLine)
                {
                    RectTransform textRect = _msgPassedTimeComponent.PassedTimeTextGo.GetComponent<RectTransform>();
                    height += textRect.sizeDelta.y;
                }
                _bgImage.Height = height;
                _bgLocater.Y = -37.0f;
                rect.anchoredPosition = new Vector2(_bgLocater.X + 12.0f, -48.0f);
                if (_msgPassedTimeComponent.IsPassedTimeInNewLine)
                {
                    RecalculateSize();
                }
            }
            else if (_block.LineCount >= 3)
            {
                float height = _block.Height + (float)Math.Round(_block.Height / _block.LineCount, 2) / 2;
                if (_msgPassedTimeComponent.IsPassedTimeInNewLine)
                {
                    RectTransform textRect = _msgPassedTimeComponent.PassedTimeTextGo.GetComponent<RectTransform>();
                    height += textRect.sizeDelta.y;
                }
                _bgImage.Height = height;
                _nameLocater.Y = -5.0f;
                _bgLocater.Y = -37.0f;
                rect.anchoredPosition = new Vector2(_bgLocater.X + 12.0f, -44.0f);
                RecalculateSize();
            }
            else
            {
                rect.anchoredPosition = new Vector2(_bgLocater.X + 12.0f, -35.0f);
                float height = _bgImage.Height;
                if (_msgPassedTimeComponent.IsPassedTimeInNewLine)
                {
                    RectTransform textRect = _msgPassedTimeComponent.PassedTimeTextGo.GetComponent<RectTransform>();
                    height += textRect.sizeDelta.y;
                    _bgImage.Height = height;
                    RecalculateSize();
                }
            }
            
        }
    }
}
