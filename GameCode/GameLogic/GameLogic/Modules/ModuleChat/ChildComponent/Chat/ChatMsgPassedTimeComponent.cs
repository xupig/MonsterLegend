﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using GameLoader.Utils.Timer;
using Common.Chat;
using Common.Global;
using Common.Data;

namespace ModuleChat
{
    public class ChatMsgPassedTimeComponent : MonoBehaviour
    {
        private GameObject _contentGo;
        protected GameObject _passedTimeTextGo;
        private bool _isShowPassedTime = false;
        protected bool _isPassedTimeInNewLine = false;
        protected uint _passedTimeSeconds = 0;
        private uint _passedTimeTimer = 0;

        private PbChatInfoResp _data;
        private ChatTextBlock _block;
        private float _textMaxWidth = 0;
        private string _templateGoPath = "";

        private KComponentEvent<GameObject, bool> _onCreateTimeText;
        public KComponentEvent<GameObject, bool> onCreateTimeText
        {
            get
            {
                if (_onCreateTimeText == null)
                {
                    _onCreateTimeText = new KComponentEvent<GameObject, bool>();
                }
                return _onCreateTimeText;
            }
        }

        public bool IsPassedTimeInNewLine
        {
            get { return _isPassedTimeInNewLine; }
        }
        public GameObject PassedTimeTextGo
        {
            get { return _passedTimeTextGo; }
        }

        protected void Awake()
        {
            GameObject scrollRectGo = transform.GetComponentInParent<KScrollRect>().gameObject;
            _contentGo = scrollRectGo.transform.FindChild("content").gameObject;
        }

        public void SetData(PbChatInfoResp chatInfo, ChatTextBlock textBlock, float textMaxWidth, string templateGoPath)
        {
            _data = chatInfo;
            _block = textBlock;
            _textMaxWidth = textMaxWidth;
            _templateGoPath = templateGoPath;
            Refresh();
        }

        private void Refresh()
        {
            if (ChatManager.Instance.IsChatMsgContainsSendTime(_data))
            {
                if (_passedTimeTimer == 0)
                {
                    _passedTimeTimer = TimerHeap.AddTimer(0, 60 * 1000, OnRefreshPassedTime);
                }
            }
        }

        protected void OnEnable()
        {
            if (_isShowPassedTime)
            {
                RefreshPassedTime();
            }
        }

        private void AddPassedTimeText()
        {
            Transform textLineTrans = _block.GameObject.transform.GetChild(_block.GameObject.transform.childCount - 1);
            RectTransform lineRect = textLineTrans.GetComponent<RectTransform>();
            _passedTimeTextGo = CloneTextPassedTimeGo();
            StateText stateText = _passedTimeTextGo.GetComponent<StateText>();
            stateText.ChangeAllStateText(GetPassedTimeDesc(_passedTimeSeconds));
            RectTransform blockRect = _block.GameObject.GetComponent<RectTransform>();
            RectTransform textRect = _passedTimeTextGo.GetComponent<RectTransform>();
            float gap = 10f;
            if (lineRect.sizeDelta.x + textRect.sizeDelta.x + gap > _textMaxWidth)
            {
                //转到下一行
                float y = -blockRect.sizeDelta.y;
                textRect.anchoredPosition = new Vector2(0, y);
                blockRect.sizeDelta = new Vector2(blockRect.sizeDelta.x, blockRect.sizeDelta.y + textRect.sizeDelta.y);
                _isPassedTimeInNewLine = true;
            }
            else
            {
                float x = lineRect.sizeDelta.x + gap;
                float y = lineRect.anchoredPosition.y;
                textRect.anchoredPosition = new Vector2(x, y);
                //blockRect.sizeDelta = new Vector2(lineRect.sizeDelta.x + textRect.sizeDelta.x + gap, blockRect.sizeDelta.y);
                _isPassedTimeInNewLine = false;
            }
        }

        private GameObject CloneTextPassedTimeGo()
        {
            GameObject textGo = Instantiate(_contentGo.transform.FindChild(_templateGoPath).gameObject) as GameObject;
            textGo.transform.SetParent(_block.GameObject.transform);
            textGo.transform.localScale = Vector3.one;
            textGo.transform.localPosition = Vector3.zero;
            textGo.SetActive(true);
            return textGo;
        }

        private bool CanShowPassedTime()
        {
            UpdatePassedTime();
            int chatNoticeTime = GlobalParams.GetChatNoticeTime();
            if (_passedTimeSeconds >= chatNoticeTime)
            {
                return true;
            }
            return false;
        }

        protected void UpdatePassedTime()
        {
            ulong sendTimeStamp = ChatContentUtils.GetSendLinkTimeStamp(_data.msg);
            if (sendTimeStamp > 0)
            {
                if (Global.serverTimeStamp / 1000 < sendTimeStamp)
                {
                    _passedTimeSeconds = 0;
                }
                else
                {
                    _passedTimeSeconds = (uint)(Global.serverTimeStamp / 1000 - sendTimeStamp);
                }
            }
        }

        private void OnRefreshPassedTime()
        {
            if (!_isShowPassedTime)
            {
                _isShowPassedTime = CanShowPassedTime();
                if (!_isShowPassedTime || !this.gameObject.activeInHierarchy)
                    return;
                RefreshPassedTime();
            }
            else
            {
                if (!this.gameObject.activeInHierarchy)
                    return;
                RefreshPassedTime();
            }
        }

        private void RefreshPassedTime()
        {
            UpdatePassedTime();
            if (_passedTimeTextGo == null)
            {
                AddPassedTimeText();
                if (_onCreateTimeText != null)
                {
                    _onCreateTimeText.Invoke(this.gameObject, _isPassedTimeInNewLine);
                }
            }
            else
            {
                StateText stateText = _passedTimeTextGo.GetComponent<StateText>();
                stateText.ChangeAllStateText(GetPassedTimeDesc(_passedTimeSeconds));
            }
        }

        private string GetPassedTimeDesc(uint passedTimeSeconds)
        {
            string descFormat = ChatConst.SEND_LINK_TIME_LENGTH_TEMPLATE;
            string desc = "";
            if (passedTimeSeconds >= 60 && passedTimeSeconds < 3600)
            {
                uint minute = passedTimeSeconds / 60;
                desc = string.Format(descFormat, minute.ToString() + ChatConst.STR_MINUTE);
            }
            else if (passedTimeSeconds >= 3600 && passedTimeSeconds < 24 * 3600)
            {
                uint hour = passedTimeSeconds / 3600;
                desc = string.Format(descFormat, hour.ToString() + ChatConst.STR_HOUR);
            }
            else if (passedTimeSeconds >= 24 * 3600)
            {
                uint day = passedTimeSeconds / (24 * 3600);
                desc = string.Format(descFormat, day.ToString() + ChatConst.STR_DAY);
            }
            desc = desc.Insert(0, "<color=#999999>");
            desc = desc.Insert(desc.Length, "</color>");
            return desc;
        }

        protected void OnDestroy()
        {
            if (_passedTimeTimer != 0)
            {
                TimerHeap.DelTimer(_passedTimeTimer);
                _passedTimeTimer = 0;
            }
            if (_block != null)
            {
                _block = null;
            }
        }
    }
}
