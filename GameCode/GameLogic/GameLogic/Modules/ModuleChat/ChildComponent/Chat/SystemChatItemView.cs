﻿using System;
using System.Collections.Generic;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using UnityEngine;
using Common.ServerConfig;
using Common.Chat;
using Common.Structs;

namespace ModuleChat
{
    public class SystemChatItemView : KContainer
    {
        private PbChatInfoResp _data;
        private StateImage _systemTitleImg;
        private KList _parent;

        private string[] _imgNameList = { "Image_duiwu", "Image_gonghui", "Image_shijie", "Image_fujin" };

        public PbChatInfoResp Data
        {
            get { return _data; }
            set
            {
                _data = value;
                Refresh();
            }
        }

        protected override void Awake()
        {
            _parent = transform.parent.GetComponent<KList>();
        }

        private GameObject CloneGameObject(string path)
        {
            GameObject go = Instantiate(_parent.GetChild(path)) as GameObject;
            go.transform.SetParent(transform);
            go.transform.localPosition = Vector3.zero;
            go.transform.localScale = Vector3.one;
            return go;
        }

        private void Refresh()
        {
            if (_data.channel_id == public_config.CHANNEL_ID_TEAM)
            {
                ShowSystemTitleImg(_imgNameList[0]);
            }
            else if (_data.channel_id == public_config.CHANNEL_ID_GUILD)
            {
                ShowSystemTitleImg(_imgNameList[1]);
            }
            else if (_data.channel_id == public_config.CHANNEL_ID_WORLD)
            {
                ShowSystemTitleImg(_imgNameList[2]);
            }
            else if (_data.channel_id == public_config.CHANNEL_ID_VICINITY)
            {
                ShowSystemTitleImg(_imgNameList[3]);
            }
            if (_systemTitleImg == null)
                return;
            AddTextBlock();
            RecalculateSize();
        }

        private void ShowSystemTitleImg(string imgName)
        {
            if (_systemTitleImg == null)
            {
                GameObject titleImgGo = CloneGameObject("templete/" + imgName);
                _systemTitleImg = titleImgGo.GetComponent<StateImage>();
            }
            else if (_systemTitleImg.gameObject.name != imgName)
            {
                GameObject.Destroy(_systemTitleImg.gameObject);
                _systemTitleImg = null;
                GameObject titleImgGo = CloneGameObject("templete/" + imgName);
                _systemTitleImg = titleImgGo.GetComponent<StateImage>();
            }
            _systemTitleImg.Visible = true;
            RectTransform rect = _systemTitleImg.gameObject.GetComponent<RectTransform>();
            rect.anchoredPosition = new Vector2(-5.0f, -6.0f);
        }

        private void HideTemplateTitleImg(string imgName)
        {
            GameObject templateTitleImgGo = _parent.GetChild("templete/" + imgName);
            if (templateTitleImgGo.activeSelf)
            {
                templateTitleImgGo.SetActive(false);
            }
        }

        private void AddTextBlock()
        {
            ChatTextBlock block = null;
            if (_data.channel_id == public_config.CHANNEL_ID_WORLD && ChatInfoHelper.isSystem(_data))
            {
                block = ChatTextBlock.CreateBlock(_data, ChatTextBlock.MAX_WIDTH, ChatTextBlock.COLOR_SYSTEM_WORLD_CANNEL, true);
            }
            else
            {
                block = ChatTextBlock.CreateBlock(_data, ChatTextBlock.MAX_WIDTH, ChatTextBlock.GetChannelFontColor((int)_data.channel_id), true);
            }
            block.GameObject.transform.SetParent(gameObject.transform);
            RectTransform systemTitleImgRect = _systemTitleImg.gameObject.GetComponent<RectTransform>();
            RectTransform rect = block.GameObject.GetComponent<RectTransform>();
            rect.localScale = Vector3.one;
            rect.anchoredPosition = new Vector2(80.0f + systemTitleImgRect.anchoredPosition.x, -9.0f);
        }
    }
}
