﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Game.UI.UIComponent;

namespace ModuleChat
{
    public class ChatVoiceAnimPlayer : KContainer
    {
        private List<StateImage> _imgList = new List<StateImage>();
        private bool _isPlay = false;
        private int _curPlayIndex = 0;
        private float _curTime = 0;
        public float _interval = 0.4f;  //单位s

        protected override void Awake()
        {
            Dictionary<int, string> imgIndexNameDict = new Dictionary<int, string>();
            //默认从索引1开始查找
            for (int i = 0; i < transform.childCount; i++)
            {
                for (int j = 0; j < transform.childCount; j++)
                {
                    Transform child = transform.GetChild(j);
                    if (child.name.IndexOf((i + 1).ToString()) != -1)
                    {
                        StateImage img = child.GetComponent<StateImage>();
                        _imgList.Add(img);
                        break;
                    }
                }
            }
            StopPlay();
        }

        public void StartPlay()
        {
            _isPlay = true;
            _curPlayIndex = 0;
        }

        public void StopPlay()
        {
            _isPlay = false;
            _curPlayIndex = _imgList.Count - 1;
            PlayAnimation();
        }

        protected void Update()
        {
            if (!_isPlay)
                return;
            _curTime += Time.deltaTime;
            if (_curTime > _interval)
            {
                PlayAnimation();
                _curTime = 0f;
            }
        }

        private void PlayAnimation()
        {
            _imgList[_curPlayIndex].Visible = true;
            for (int i = 0; i < _imgList.Count; i++)
            {
                if (i != _curPlayIndex)
                {
                    _imgList[i].Visible = false;
                }
            }
            _curPlayIndex++;
            if (_curPlayIndex > _imgList.Count - 1)
            {
                _curPlayIndex = 0;
            }
        }

        protected override void OnDestroy()
        {
            _imgList.Clear();
        }
    }
}
