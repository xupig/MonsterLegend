﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using GameLoader.Utils;
using UnityEngine.Events;
using UnityEngine;

using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;

using Common.Chat;
using Common.Structs;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using Common.Utils;

namespace ModuleChat
{
    public class ChatEmojiItem : KList.KListItemBase
    {
        private string _data;

        protected override void Awake()
        {
            
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = (string)value;
                Refresh();
            }
        }

        private void Refresh()
        {
            int index = ChatEmojiDefine.GetEmojiIndex(_data);
            float width = ChatEmojiDefine.GetEmojiWidth(index);
            float height = ChatEmojiDefine.GetEmojiHeight(index);
            MogoSpriteAnimationUtils.AddAnimation(this.gameObject, ChatEmojiDefine.GetEmojiSpriteNameList(_data), width, height);
        }

        public override void Dispose()
        {
            
        }
    }
}
