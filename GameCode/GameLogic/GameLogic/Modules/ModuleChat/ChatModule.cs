﻿#region 模块信息
/*==========================================
// 文件名：ChatModule
// 命名空间: ModuleChat
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/29 20:05:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using GameLoader.Utils;
using MogoEngine.Events;
using UnityEngine;

namespace ModuleChat
{
    public class ChatModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.Chat, "Container_ChatPanel", MogoUILayer.LayerUIPanel, "ModuleChat.ChatPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            //AddPanel(PanelIdEnum.ChatEmoji, "Container_ChatEmojiPanel", MogoUILayer.LayerSecondUIPanel, "ModuleChat.ChatEmojiPanel", false);
            //AddPanel(PanelIdEnum.ChatGift, "Container_ChatGiftPanel", MogoUILayer.LayerUIPanel, "ModuleChat.ChatGiftPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            //AddPanel(PanelIdEnum.ChatMenu, "Container_ChatMenuPanel", MogoUILayer.LayerUIPanel, "ModuleChat.ChatMenuPanel", BlurUnderlay.Empty);
            AddPanel(PanelIdEnum.ChatPhraseEdit, "Container_ChatPhraseEditPanel", MogoUILayer.LayerUIPanel, "ModuleChat.ChatPhraseEditPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.ChatReport, "Container_ChatReportPanel", MogoUILayer.LayerUIPanel, "ModuleChat.ChatReportPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.RedEnvelope, "Container_RedEnvelopePanel", MogoUILayer.LayerUIToolTip, "ModuleChat.RedEnvelopePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }

    }
}
