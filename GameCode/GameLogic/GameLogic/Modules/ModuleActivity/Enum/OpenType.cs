﻿#region 模块信息
/*==========================================
// 文件名：OpenType
// 命名空间: GameLogic.GameLogic.Modules.ModuleActivity.Enum
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/16 16:10:41
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleActivity
{
    public enum OpenType
    {
        Week = 1,
        SpecialDay = 2,
        Daily = 3,
    }
}
