﻿#region 模块信息
/*==========================================
// 文件名：ChallengeView
// 命名空间: GameLogic.GameLogic.Modules.ModuleActivity.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/15 15:15:01
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleActivity
{
    public class ChallengeView:KContainer
    {

        private KButton _btnChallenge;
        private KContainer _introduceContainer;
        private StateText _txtIntroduce;
        private CircleToggleGroup _toggleGroup;

        protected override void Awake()
        {
            _btnChallenge = GetChildComponent<KButton>("Button_tiaozhan");
            _introduceContainer = GetChildComponent<KContainer>("Container_introduce");
            _txtIntroduce = _introduceContainer.GetChildComponent<StateText>("Label_txtjianjiexinxi");
            _toggleGroup = AddChildComponent<CircleToggleGroup>("Container_toggleGroup");
          
            base.Awake();
        }

        public void Show()
        {
            Visible = true;
            UpdateContent();
            AddEventListener();
        }

        public void Hide()
        {
            Visible = false;
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _btnChallenge.onClick.AddListener(OnClickChallenge);
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            _toggleGroup.onClickSelectedItem.AddListener(OnClickChallenge);
            EventDispatcher.AddEventListener<int, string, string>(ActivityEvents.SELECT_CHALLENGE, OnSelectChallenge);
        }

        private void RemoveEventListener()
        {
            _btnChallenge.onClick.RemoveListener(OnClickChallenge);
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            _toggleGroup.onClickSelectedItem.RemoveListener(OnClickChallenge);
            EventDispatcher.RemoveEventListener<int,string,string>(ActivityEvents.SELECT_CHALLENGE, OnSelectChallenge);
        }

        private void OnSelectChallenge(int activityId,string direction,string langId)
        {
            List<CircleToggleItem> list = _toggleGroup.GetItemList();
            for(int i=0;i<list.Count;i++)
            {
                LimitActivityData data = list[i].Data as LimitActivityData;
                if( data!=null && data.openData.__id == activityId)
                {
                    _toggleGroup.SetSelected(i);
                    OnSelectedIndexChanged(_toggleGroup, i);
                    string[] result = MogoUtils.CopyGameObjectPath(list[i].transform);
                    if(result!=null)
                    {
                        PanelIdEnum.GuideArrow.Show(new string[] { direction, string.Format("activity{0}",activityId), result[0],result[1],langId });
                    }
                }
            }
        }

        private void OnClickChallenge()
        {
            if (_toggleGroup.GetItemList().Count > _toggleGroup.SelectedIndex)
            {
                LimitActivityData data = _toggleGroup.GetItemList()[_toggleGroup.SelectedIndex].Data as LimitActivityData;
                if (data!=null && data.functionData != null)
                {
                    function_helper.Follow(data.functionData.__id, data.openData.__fid_arg);
                }
            }
        }

        public void UpdateContent()
        {
            List<LimitActivityData> dataList = new List<LimitActivityData>();
            List<LimitActivityData> normalList =  activity_helper.GetNormalFunctionList();
            List<LimitActivityData> activityList = new List<LimitActivityData>();
            List<LimitActivityData> notactivityList = new List<LimitActivityData>();
            for (int i = 0; i < normalList.Count;i++ )
            {
                if (activity_helper.IsOpen(normalList[i]))
                {
                    activityList.Add(normalList[i]);
                }
                else
                {
                    notactivityList.Add(normalList[i]);
                }
            }
            activityList.Concat(notactivityList);
            if (activityList.Count < 7)
            {
                int left = 7 - activityList.Count;
                for(int i=0;i<left;i++)
                {
                    activityList.Add(null);
                }
            }
            for (int i = 0; i < activityList.Count;i++ )
            {
                if(i%2==1)
                {
                    dataList.Add(activityList[i]);
                }
                else
                {
                    dataList.Insert(0,activityList[i]);
                }
            }
            _toggleGroup.RemoveAll();
            _toggleGroup.SetDataList(dataList);
            if (dataList.Count > 0)
            {
                _toggleGroup.SetSelected(dataList.Count / 2);
                OnSelectedIndexChanged(_toggleGroup, dataList.Count / 2);
            }
            else
            {
                _introduceContainer.Visible = false;
                _btnChallenge.Visible = false;
            }
        }

        private void OnSelectedIndexChanged(CircleToggleGroup toggleGroup,int index)
        {
            if(toggleGroup.GetItemList().Count>index)
            {
                _introduceContainer.Visible = true;
                _btnChallenge.Visible = true;
                LimitActivityData data = toggleGroup.GetItemList()[index].Data as LimitActivityData;
                bool isActivity = false;
                if(data!=null)
                {
                    if (data.openData.__desc != 0)
                    {
                        _txtIntroduce.CurrentText.text = MogoLanguageUtil.GetContent(data.openData.__desc);
                    }
                    else
                    {
                        _txtIntroduce.CurrentText.text = MogoLanguageUtil.GetContent(data.functionData.__descrip);
                    }
                    isActivity = activity_helper.IsOpen(data);
                }
                else
                {
                    _txtIntroduce.CurrentText.text = (56741).ToLanguage();
                }
                
                StateImage[] list = _btnChallenge.GetComponentsInChildren<StateImage>();
               
                _btnChallenge.enabled = isActivity;
                for (int i = 0; i < list.Length;i++ )
                {
                    ImageWrapper[] imageWrappers = list[i].GetAllStateComponent<ImageWrapper>();
                    for(int j=0;j<imageWrappers.Length;j++)
                    {
                        if (imageWrappers[j]!=null)
                        {
                            if (isActivity)
                            {
                                imageWrappers[j].SetGray(1);
                            }
                            else
                            {
                                imageWrappers[j].SetGray(0);
                            }
                        }
                    }
                }
                    
            }
            else
            {
                _introduceContainer.Visible = false;
                _btnChallenge.Visible = false;
            }
           
        }


    }
}
