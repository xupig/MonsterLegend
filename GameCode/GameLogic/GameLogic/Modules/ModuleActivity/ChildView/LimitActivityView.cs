﻿#region 模块信息
/*==========================================
// 文件名：LimitActivityView
// 命名空间: GameLogic.GameLogic.Modules.ModuleActivity.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/15 14:55:10
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleActivity
{
    public class LimitActivityView:KContainer
    {
        private KButton _btnTomorrowActivity;
        private LimitActivityPage _activityPage;

        protected override void Awake()
        {
            base.Awake();
            _btnTomorrowActivity = GetChildComponent<KButton>("Button_chakanhuodong");
            _activityPage = gameObject.AddComponent<LimitActivityPage>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            UpdateContent();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _btnTomorrowActivity.onClick.AddListener(OnTomorrowClick);
            //EventDispatcher.AddEventListener(ActivityEvents.ACTIVITY_TIME_UPDATE_LIST, UpdateContent);
        }

        private void RemoveEventListener()
        {
            _btnTomorrowActivity.onClick.RemoveListener(OnTomorrowClick);
            EventDispatcher.RemoveEventListener(ActivityEvents.ACTIVITY_TIME_UPDATE_LIST, UpdateContent);
        }


        public void UpdateContent()
        {
            List<LimitActivityData> dataList = activity_helper.GetActivityByType(ActivityType.LimitActivity);
            _activityPage.RefreshContent(dataList);
        }

        private void OnTomorrowClick()
        {
            PanelIdEnum.TomorrowActivity.Show();
        }

    }
}
