﻿#region 模块信息
/*==========================================
// 文件名：LimitActivityView
// 命名空间: GameLogic.GameLogic.Modules.ModuleActivity.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/15 14:55:10
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleActivity
{
    public class LimitActivityPage : KContainer
    {
        private KScrollPage _scrollPage;
        private KPageableList _list;
       

        protected override void Awake()
        {
            base.Awake();
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_xianshihuodong");
            _list = _scrollPage.GetChildComponent<KPageableList>("mask/content");
            _list.SetDirection(KList.KListDirection.LeftToRight, 4, 1);
            _list.SetGap(0, 36);
            _list.SetPadding(2, 0, 0, 18);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _list.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            EventDispatcher.AddEventListener<int, string, string>(ActivityEvents.SELECT_CHALLENGE, OnSelectChallenge);
        }

        private void RemoveEventListener()
        {
            _list.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            EventDispatcher.RemoveEventListener<int, string, string>(ActivityEvents.SELECT_CHALLENGE, OnSelectChallenge);
        }


        private void OnSelectChallenge(int activityId, string direction, string langId)
        {
            List<KList.KListItemBase> list = _list.ItemList;
            for (int i = 0; i < list.Count; i++)
            {
                LimitActivityData data = list[i].Data as LimitActivityData;
                if (data != null && data.openData.__id == activityId)
                {
                    _list.SelectedIndex = i;
                    OnSelectedIndexChanged(_list, i);
                    string[] result = MogoUtils.CopyGameObjectPath(list[i].transform);
                    if (result != null)
                    {
                        PanelIdEnum.GuideArrow.Show(new string[] { direction, string.Format("activity{0}", activityId), result[0], result[1], langId });
                    }
                }
            }
        }


        private void OnSelectedIndexChanged(KList list,int index)
        {

        }


        public void RefreshContent(List<LimitActivityData> dataList)
        {
            RefreshContent<LimitActivityItem>(dataList);
        }

        protected virtual void RefreshContent<T>(List<LimitActivityData> dataList) where T : KList.KListItemBase
        {
            if (_scrollPage == null || Visible == false)
            {
                return;
            }
            _list.RemoveAll();
            _scrollPage.TotalPage = Math.Max(1, Mathf.CeilToInt((float)dataList.Count / 4));
            _scrollPage.CurrentPage = 0;
            _list.SetDataList<T>(dataList);
        }

    }
}
