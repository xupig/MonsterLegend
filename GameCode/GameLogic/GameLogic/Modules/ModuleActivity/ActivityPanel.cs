﻿#region 模块信息
/*==========================================
// 文件名：ActivityPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleActivity
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/15 14:50:36
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleActivity
{
    public class ActivityPanel:BasePanel
    {

        public const int Challenge = 0;
        public const int LimitActivity = 1;
        public const int GuildWar = 2;

        private LimitActivityView _activityView;
        private ChallengeView _challengeView;
        private KToggleGroup _toggleGroup;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _activityView = AddChildComponent<LimitActivityView>("Container_xianshihuodong");
            _challengeView = AddChildComponent<ChallengeView>("Container_tiaozhan");
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            ActivityManager.Instance.GetActivityList();
            List<int> tabList = function_helper.GetFunctionTabList(9);
            tabList.Add(1803);
            SetTabList(tabList, _toggleGroup);
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Activity; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            int tabIndex = 0;
            int categoryIndex = 0;
            if(data == null)
            {
                tabIndex = Challenge;
            }
            else if(data is int)
            {
                tabIndex = (int)data - 1;
            }
            else if (data is int[])
            {
                tabIndex = (data as int[])[0] - 1;
                categoryIndex = (data as int[])[1];
            }
            _toggleGroup.SelectIndex = tabIndex;
            SetSelectedIndex(tabIndex, categoryIndex);
            if(PlayerDataManager.Instance.ActivityData.isDataResponsed)
            {
                OnActivityLeftCountInited();
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
            _activityView.Visible = false;
            _challengeView.Hide();
            PanelIdEnum.GuildWar.Close();
        }


        private void AddEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            EventDispatcher.AddEventListener(ActivityEvents.ACTIVITY_LEFT_COUNT_INIT,OnActivityLeftCountInited);
            EventDispatcher.AddEventListener(ActivityEvents.ACTIVITY_CHANGE_STATE,OnActivityStateChange);
        }

        private void RemoveEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            EventDispatcher.RemoveEventListener(ActivityEvents.ACTIVITY_LEFT_COUNT_INIT, OnActivityLeftCountInited);
            EventDispatcher.RemoveEventListener(ActivityEvents.ACTIVITY_CHANGE_STATE, OnActivityStateChange);
        }

        private void OnActivityStateChange()
        {
            _toggleGroup.GetToggleList()[0].SetGreenPoint(PlayerDataManager.Instance.ActivityData.hasNewChallenge);
            _toggleGroup.GetToggleList()[1].SetGreenPoint(PlayerDataManager.Instance.ActivityData.hasActiveActivity || PlayerDataManager.Instance.ActivityData.hasNewActivity);
        }

        private void OnActivityLeftCountInited()
        {
            _challengeView.UpdateContent();
            _activityView.UpdateContent();
            OnDataResp();
        }

        private void OnSelectedIndexChanged(KToggleGroup toggleGroup, int index)
        {
            SetSelectedIndex(index);
        }

        private void SetSelectedIndex(int tabIndex, int categoryIndex = 0)
        {
            if (tabIndex == Challenge)
            {
                _challengeView.Show();
                _activityView.Visible = false;
                PanelIdEnum.GuildWar.Close();
                PlayerDataManager.Instance.ActivityData.hasNewChallenge = false;
            }
            else if (tabIndex == LimitActivity)
            {
                _challengeView.Hide();
                _activityView.Visible = true;
                PanelIdEnum.GuildWar.Close();
                PlayerDataManager.Instance.ActivityData.hasActiveActivity = false;
                PlayerDataManager.Instance.ActivityData.hasNewActivity = false;
            }
            else if (tabIndex == GuildWar)
            {
                _challengeView.Hide();
                _activityView.Visible = false;
                PanelIdEnum.GuildWar.Show(categoryIndex);
            }
        }

    }
}
