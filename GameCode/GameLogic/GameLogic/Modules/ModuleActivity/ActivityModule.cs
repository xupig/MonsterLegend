﻿#region 模块信息
/*==========================================
// 文件名：ActivityModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleActivity
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/15 14:46:36
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleActivity
{
    public class ActivityModule:BaseModule
    {
        public override void Init()
        {
            AddPanel(PanelIdEnum.Activity, "Container_ActivityPanel", MogoUILayer.LayerUIPanel, "ModuleActivity.ActivityPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.ActivityNotice, "Container_ActivityNoticePanel", MogoUILayer.LayerUIPanel, "ModuleActivity.ActivityNoticePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.TomorrowActivity, "Container_TomorrowActivityPanel", MogoUILayer.LayerUIPanel, "ModuleActivity.TomorrowActivityPanel", BlurUnderlay.Have, HideMainUI.Hide);
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

    }
}
