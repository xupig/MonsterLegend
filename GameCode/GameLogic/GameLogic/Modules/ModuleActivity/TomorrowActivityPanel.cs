﻿#region 模块信息
/*==========================================
// 文件名：TomorrowActivityPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleActivity
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/21 10:35:36
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleActivity
{
    public class TomorrowActivityPanel:BasePanel
    {
        private PageableScrollView _scrollView;
        private KDummyButton _btnClose;

        protected override void Awake()
        {
            _btnClose = AddChildComponent<KDummyButton>("Container_hotArea");
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _scrollView = AddChildComponent<PageableScrollView>("ScrollView_yulan");
            _scrollView.IsPageable = false;
            _scrollView.SetDirection(2, int.MaxValue);
            _scrollView.SetCapacity(14);
            _scrollView.SetGap(0,-35);
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TomorrowActivity; }
        }

        public override void OnShow(object data)
        {
            List<LimitActivityData> dataList = activity_helper.GetTomorrowLimitActivityList();
            _scrollView.SetDataList<TomorrowActivityItem>(dataList,1,1);
            _btnClose.onClick.AddListener(ClosePanel);
        }

        public override void OnClose()
        {
            _btnClose.onClick.RemoveListener(ClosePanel);
        }

    }
}
