﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleActivity
{
    public class ActivityNoticePanel : BasePanel
    {
        private KButton _gotoBtn;
        private KToggle _noticeToggle;
        private StateText _noticeTip;
        private StateText _content;

        private int _noticeId = 0;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _gotoBtn = GetChildComponent<KButton>("Button_anjian");
            _noticeToggle = GetChildComponent<KToggle>("Container_butishi/Toggle_Xuanze");
            _noticeTip = GetChildComponent<StateText>("Container_butishi/Label_txtButishi");
            _content = GetChildComponent<StateText>("Label_txtXinxi");
            _noticeTip.CurrentText.text = MogoLanguageUtil.GetContent(106602);
            _gotoBtn.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(106601));
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.ActivityNotice; }
        }

        public override void OnShow(object data)
        {
            _noticeId = (int)data;
            RefreshContent();
            AddEventListener();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }


        private void AddEventListener()
        {
            _gotoBtn.onClick.AddListener(OnGotoButtonClick);
            _noticeToggle.onValueChanged.AddListener(OnToggleChange);
        }

        private void RemoveEventListener()
        {
            _gotoBtn.onClick.RemoveListener(OnGotoButtonClick);
            _noticeToggle.onValueChanged.RemoveListener(OnToggleChange);
        }

        private void RefreshContent()
        {
            LimitActivityData _activityData = activity_helper.GetLimitActivity(_noticeId);
            if (_activityData == null)
            {
                Debug.LogError(string.Format("当前没有开启activiyId为{0}的活动!", _noticeId));
                return;
            }
            string title = _activityData.openData.__name != 0 ? MogoLanguageUtil.GetContent(_activityData.openData.__name) : MogoLanguageUtil.GetContent(_activityData.functionData.__name);
            _content.CurrentText.text = MogoLanguageUtil.GetContent(106600, title);
        }

        private void OnGotoButtonClick()
        {
            LimitActivityData _activityData = activity_helper.GetLimitActivity(_noticeId);
            if (_activityData != null)
            {
                if (_activityData.openData != null && _activityData.openData.__fid != 0)
                {
                    function_helper.ActivityFollow(_activityData.openData.__fid, _activityData.openData.__id, _activityData.openData.__fid_arg);
                }
                else
                {
                    function_helper.Follow(_activityData.functionData.__id, _activityData.openData.__fid_arg);
                }
            }
            ClosePanel();
        }

        private void OnToggleChange(KToggle arg0, bool isOn)
        {
            ActivityNoticeManager.Instance.showActivityNotice = !isOn;
        }

    }
}
