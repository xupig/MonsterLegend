﻿#region 模块信息
/*==========================================
// 文件名：LimitActivityItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleActivity.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/16 19:45:11
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace ModuleActivity
{
    public class LimitActivityItem : KList.KListItemBase
    {
        private StateText _txtUnderWay;
        protected StateText _txtTime;
        private StateText _txtName;
        private StatePlaceHolder _placeHolder;
        private StateImage _imgCheckmark;
        private KParticle _particle;
       // private StateImage _imgHasFinish;
       // private StateImage _imgLock;
        protected LimitActivityData _activityData;

        private IconContainer _activityTypeContainer;
        private static string timeTemplate;
        private bool isOpen;
        private MaskEffectItem _effectItem;
        protected override void Awake()
        {
            _particle = AddChildComponent<KParticle>("fx_ui_28_huanraoguangxiao");
            
            _activityTypeContainer = AddChildComponent<IconContainer>("Container_huodongleixing");
            _txtUnderWay = GetChildComponent<StateText>("Label_txtZhengzaijinxing");
            _txtTime = GetChildComponent<StateText>("Label_txtHuodongshijian");
            _placeHolder = AddChildComponent<StatePlaceHolder>("Container_icon");
            _txtName = GetChildComponent<StateText>("Label_label");
            _imgCheckmark = GetChildComponent<StateImage>("Image_checkmark");
            _imgCheckmark.Visible = false;
            //_imgHasFinish = GetChildComponent<StateImage>("Image_jinriyiwancheng");
           // _imgLock = GetChildComponent<StateImage>("Image_image3");
           // _imgLock.Visible = false;
            if(timeTemplate == null)
            {
                timeTemplate = _txtTime.CurrentText.text;
            }
            _txtUnderWay.Visible = false;
            _txtTime.Visible = false;
            //_imgHasFinish.Visible = false;
            _imgCheckmark.Visible = false;
            onClick.AddListener(OnClick);
            _placeHolder.onClick.AddListener(OnGotoActivity);

            _effectItem = gameObject.AddComponent<MaskEffectItem>();
            _effectItem.Init(_particle.transform.parent.gameObject, IsParticleShow);

            _particle.Stop();
        }

        private bool IsParticleShow()
        {
            return isOpen;
        }

        public override bool IsSelected
        {
            get
            {
                return base.IsSelected;
            }
            set
            {
                base.IsSelected = value;
                //_imgCheckmark.Visible = value;
            }
        }

        public override object Data
        {
            get
            {
                return _activityData;
            }
            set
            {
                _activityData = value as LimitActivityData;
                Refresh();
            }
        }

        public override void Dispose()
        {
            onClick.RemoveListener(OnClick);
            _placeHolder.onClick.RemoveListener(OnGotoActivity);
        }


        private void OnClick(KList.KListItemBase item,int index)
        {
            OnGotoActivity();
        }

        protected virtual void OnGotoActivity()
        {
            if (_activityData != null)
            {
                if (_activityData.openData!=null&&_activityData.openData.__fid != 0)
                {
                    function_helper.ActivityFollow(_activityData.openData.__fid, _activityData.openData.__id, _activityData.openData.__fid_arg);
                }
                else
                {
                    function_helper.Follow(_activityData.functionData.__id, _activityData.openData.__fid_arg);
                }
            }
        }


        protected virtual void Refresh()
        {
            if(_activityData != null)
            {
                string content = GetTimeContent();
                _txtTime.CurrentText.text = string.Format(timeTemplate, content);
                RecalculatePosition();
                if (_activityData.openData.__icon != 0)
                {
                    _placeHolder.SetIcon(_activityData.openData.__icon);
                }
                else
                {
                    _placeHolder.SetIcon(_activityData.functionData.__icon);
                }
                if (_activityTypeContainer != null)
                {
                    if (_activityData.openData.__type_icon != 0)
                    {
                        _activityTypeContainer.Visible = true;
                        _activityTypeContainer.SetIcon(_activityData.openData.__type_icon);
                    }
                    else
                    {
                        _activityTypeContainer.Visible = false;
                    }
                }
               
                _txtName.CurrentText.text = _activityData.openData.__name != 0 ? MogoLanguageUtil.GetContent(_activityData.openData.__name) : MogoLanguageUtil.GetContent(_activityData.functionData.__name);
                //SetActivityLeftCount();
                isOpen = activity_helper.IsOpen(_activityData);
                _txtTime.Visible = !isOpen;
                _txtUnderWay.Visible = isOpen;
                if(isOpen)
                {
                    if(_particle.Visible == false)
                    {
                        _particle.Play(true);
                    }
                }
                else
                {
                    _particle.Stop();
                }
            }
            else
            {
                isOpen = false;
            }
            _effectItem.UpdateMaskEffect();
        }

        protected string GetTimeContent()
        {
            string content = string.Empty;
            for (int i = 0; i < _activityData.openTimeStart.Count; i++)
            {
                string time = string.Format("{0}-{1}", MogoTimeUtil.ToUniversalTime(_activityData.openTimeStart[i], "hh:mm"), MogoTimeUtil.ToUniversalTime(_activityData.openTimeEnd[i], "hh:mm"));
                if (i != 0)
                {
                    content = string.Concat(content, '\n');
                }
                content = string.Concat(content, time);
            }
            return content;
        }

        private void RecalculatePosition()
        {
            _txtTime.CurrentText.alignment = TextAnchor.UpperRight;
            MogoGameObjectHelper.RecalculateTextSize(_txtTime.CurrentText);
            _txtTime.RecalculateCurrentStateHeight();
            RectTransform bgRect = GetChildComponent<StateImage>("Image_shijiandi").transform as RectTransform;
            RectTransform txtRect = _txtTime.transform as RectTransform;
            _txtTime.transform.localPosition = new Vector3(bgRect.localPosition.x + bgRect.sizeDelta.x * 0.5f - txtRect.sizeDelta.x * 0.5f, bgRect.localPosition.y - bgRect.sizeDelta.y * 0.5f + txtRect.sizeDelta.y * 0.5f, txtRect.localPosition.z);
        }
       
    }
}
