﻿#region 模块信息
/*==========================================
// 文件名：TomorrowActivityItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleActivity.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/21 14:04:51
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleActivity
{
    public class TomorrowActivityItem:KList.KListItemBase
    {
        private StateText _txtName;
        private StateText _txtDescription;
        private IconContainer _iconContainer;

        private LimitActivityData _activityData;

        private static string template;

        protected override void Awake()
        {
            _txtName = GetChildComponent<StateText>("Label_txtName");
            _txtDescription = GetChildComponent<StateText>("Label_txtHuodong");
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            if(template == null)
            {
                template = _txtDescription.CurrentText.text;
            }
        }

        public override object Data
        {
            get
            {
                return _activityData;
            }
            set
            {
                _activityData = value as LimitActivityData;
                Refresh();
            }
        }

        public override void Dispose()
        {
            
        }

        private void Refresh()
        {
            if(_activityData!=null)
            {
                if (_activityData.openData.__name != 0)
                {
                    _txtName.CurrentText.text = MogoLanguageUtil.GetContent(_activityData.openData.__name);
                }
                else
                {
                    _txtName.CurrentText.text = MogoLanguageUtil.GetContent(_activityData.functionData.__name);
                }

                if (_activityData.openData.__icon != 0)
                {
                    _iconContainer.SetIcon(_activityData.openData.__icon);
                }
                else
                {
                    _iconContainer.SetIcon(_activityData.functionData.__icon);
                }
                int[] data = activity_helper.GetTommorrowOpenData(_activityData.openData.__id);
                _txtDescription.CurrentText.text = string.Format(template, MogoTimeUtil.ToUniversalTime(data[0], "hh:mm"), MogoTimeUtil.ToUniversalTime(data[1], "hh:mm"));
            }
            
        }

    }
}
