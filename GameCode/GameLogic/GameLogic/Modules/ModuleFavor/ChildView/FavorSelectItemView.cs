﻿using Common.Base;
using Common.Data;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleFavor
{
    public class FavorSelectItemView : KContainer
    {
        private KButton _closeBtn;
        private KButton _okBtn;
        private KScrollPage _scrollPage;
        private KList _selectItemList;
        //private StateText _npcName;
        private StateText _txtTip;
        private StateText _txtItemNum;

        private uint timerId ;

        public KComponentEvent<FavorItemData> onOkClick = new KComponentEvent<FavorItemData>();
        public KComponentEvent<FavorItemData> onSelectItemChanged = new KComponentEvent<FavorItemData>();
        //public KComponentEvent onCloseClick = new KComponentEvent();

        protected override void Awake()
        {
            _closeBtn = GetChildComponent<KButton>("Container_bg/Button_close");
            _okBtn = GetChildComponent<KButton>("Button_queding");
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_xuanzewupin");
            _selectItemList = GetChildComponent<KList>("ScrollPage_xuanzewupin/mask/content");
            //_npcName = GetChildComponent<StateText>("Label_txtXuanzeliwu");
            _txtTip = GetChildComponent<StateText>("Label_txtZengsongwupin");
            _txtItemNum = GetChildComponent<StateText>("Label_txtshengyushuliang");

            InitSelectItemView();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventsListener();
        }

        protected override void OnDisable()
        {
            RemoveEventsListener();
            base.OnDisable();
        }

        public void ShowSelectItem(List<KeyValuePair<int, int>> selectItemList)
        {
            Visible = true;
            _txtTip.Visible = true;
            _txtItemNum.Visible = false;
            _selectItemList.RemoveAll();
            if (selectItemList == null)
            {            
                return;
            }

            BagData bagData = PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag);
            for(int i = 0; i < selectItemList.Count; i++)
            {
                KeyValuePair<int, int> kvp = selectItemList[i];
                int count = bagData.GetItemNum(kvp.Key);

                ItemData itemData = new ItemData(kvp.Key);
                itemData.StackCount = count;

                FavorItemData favorItemData = new FavorItemData(itemData, kvp.Value);
                _selectItemList.AddItem<FavorSelectItem>(favorItemData, false);

            }
            _selectItemList.DoLayout();
        }

        private void AddEventsListener()
        {
            _closeBtn.onClick.AddListener(OnCloseBtnClick);
            _okBtn.onClick.AddListener(OnOkBtnClick);
            _okBtn.onPointerDown.AddListener(OnOKBtnDown);
            _okBtn.onPointerUp.AddListener(OnOkBtnUp);
            _selectItemList.onSelectedIndexChanged.AddListener(OnSelectedItemChanged);
        }

        private void RemoveEventsListener()
        {
            _closeBtn.onClick.RemoveListener(OnCloseBtnClick);
            _okBtn.onClick.RemoveListener(OnOkBtnClick);
            _okBtn.onPointerDown.RemoveListener(OnOKBtnDown);
            _okBtn.onPointerUp.RemoveListener(OnOkBtnUp);
            _selectItemList.onSelectedIndexChanged.RemoveListener(OnSelectedItemChanged);
        }

        private void OnOkBtnUp(KButton arg0, PointerEventData arg1)
        {
            TimerHeap.DelTimer(timerId);
        }

        private void OnOKBtnDown(KButton button, PointerEventData eventData)
        {
            if (eventData.dragging == true)
            {
                return;
            }
            timerId = TimerHeap.AddTimer(1000, 200, OnOkBtnClick);
        }

        private void OnOkBtnClick()
        {
            if (_selectItemList.SelectedIndex != -1)
            {
                FavorItemData selectedItemData = _selectItemList[_selectItemList.SelectedIndex].Data as FavorItemData;
                onOkClick.Invoke(selectedItemData);
                selectedItemData.ItemData.StackCount--;
                RefreshItemData(_selectItemList.SelectedIndex);
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(57024), PanelIdEnum.Favor);
            }
        }

        private void OnSelectedItemChanged(KList list, int index)
        {
            RefreshItemData(index);
        }

        private void OnCloseBtnClick()
        {
            //onCloseClick.Invoke();
            UIManager.Instance.ClosePanel(PanelIdEnum.Favor);
        }

        private void RefreshItemData(int index)
        {
            FavorItemData itemData = _selectItemList[index].Data as FavorItemData;
            if (itemData.ItemData.StackCount == 0)
            {
                _txtTip.Visible = true;
                _txtItemNum.Visible = false;
                _selectItemList[index].Data = itemData;
                _selectItemList.SelectedIndex = -1;
            }
            else
            {
                _txtTip.Visible = false;
                _txtItemNum.Visible = true;
                _selectItemList[index].Data = itemData;
                _txtItemNum.CurrentText.text = MogoLanguageUtil.GetContent(82, itemData.ItemData.StackCount);
                onSelectItemChanged.Invoke(_selectItemList[index].Data as FavorItemData);
            }
        }

        private void InitSelectItemView()
        {
            _scrollPage.SetDirection(KScrollPage.KScrollPageDirection.TopToDown);
            _scrollPage.TotalPage = 1;
            _selectItemList.SetDirection(KList.KListDirection.LeftToRight, 4, 3);
            _selectItemList.SetGap(5, 10);
            _selectItemList.SetPadding(0, 0, 0, 0);
        }
    }
}
