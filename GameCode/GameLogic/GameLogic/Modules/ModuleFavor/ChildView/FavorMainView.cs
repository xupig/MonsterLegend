﻿using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleFavor
{
    public class FavorMainView:KContainer
    {
        private FavorSelectItemView _selectItemView;
        private TweenProgressBar _tweenProgressBar;

        private StateText _favorPointTxt;
        private StateText _dialogueTxt;
        private StateText _addFavorPoint;
        private StateText _npcName;
        private StateText _favorLabel;
        private StateText _giveLabel;
        private KContainer _favorIcon;

        private Vector2 _mainViewSize;
        private Vector2 _selectViewSize;

        private int _npcId;
        private int _preUplevelFavorPoint;
        private int _preFavorPoint;
        private int _preFavorLevel;

        protected override void Awake()
        {
            _selectItemView = AddChildComponent<FavorSelectItemView>("Container_wupinxuanze");
            _tweenProgressBar = AddChildComponent<TweenProgressBar>("ProgressBar_jindu");
            _favorPointTxt = GetChildComponent<StateText>("Label_txtHaogandulabel");
            _dialogueTxt = GetChildComponent<StateText>("Label_txtDazi");
            _addFavorPoint = GetChildComponent<StateText>("Label_txtZengjia");
            _npcName = GetChildComponent<StateText>("Label_txtbiaoti");
            _favorLabel = GetChildComponent<StateText>("Label_txtHaogandu");
            _giveLabel = GetChildComponent<StateText>("Label_txtZengyu");

            _favorIcon = GetChildComponent<KContainer>("Container_haoyouduicon");

            InitSizeInfo();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventsListener();

            TweenViewMove.Begin(gameObject, MoveType.Show, MoveDirection.Left);
            TweenViewMove.Begin(_selectItemView.gameObject, MoveType.Show, MoveDirection.Right);
        }

        protected override void OnDisable()
        {
            RemoveEventsListener();
            base.OnDisable();
        }

        public void ShowNpc(int npcId)
        {
            _npcId = npcId;
            FavorInfo favorInfo = PlayerDataManager.Instance.FavorData.GetFavorData(_npcId);

            _preFavorLevel = favorInfo.Level;
            _preFavorPoint = favorInfo.CurrentFavorPoint;
            _preUplevelFavorPoint = favorInfo.UpgradeFavorPoint;

            InitFavorMainView();
            FillFavorConfig();
            ShowFavorPointIcon();
            RefreshFavorPoint();
            _selectItemView.ShowSelectItem(favorInfo.AcceptGift);
        }

        private void AddEventsListener()
        {
            _selectItemView.onSelectItemChanged.AddListener(OnSelectItem);
            _selectItemView.onOkClick.AddListener(OnRequestGive);
            EventDispatcher.AddEventListener<int>(FavorEvents.UpdateNpcFavor, FillContent);
        }

        private void RemoveEventsListener()
        {
            _selectItemView.onSelectItemChanged.RemoveListener(OnSelectItem);
            _selectItemView.onOkClick.RemoveListener(OnRequestGive);
            EventDispatcher.RemoveEventListener<int>(FavorEvents.UpdateNpcFavor, FillContent);
        }

        private void OnRequestGive(FavorItemData itemData)
        {
            _addFavorPoint.Clear();
            FavorManager.Instance.RequestGiveItem(_npcId, itemData.ItemData);
        }

        private void OnSelectItem(FavorItemData itemData)
        {

            _addFavorPoint.CurrentText.text = string.Format("+{0}", itemData.FavorValue);

        }

        private void FillContent(int npcId)
        {
            _npcId = npcId;
            FavorInfo favorInfo = PlayerDataManager.Instance.FavorData.GetFavorData(_npcId);

            ShowFavorPointIcon();
            FavorPointChange();
            if (favorInfo.Level != _preFavorLevel)
            {
                _addFavorPoint.Clear();
                _selectItemView.ShowSelectItem(favorInfo.AcceptGift);
            }

            _preUplevelFavorPoint = favorInfo.UpgradeFavorPoint;
            _preFavorPoint = favorInfo.CurrentFavorPoint;
            _preFavorLevel = favorInfo.Level;
        }

        private void FillFavorConfig()
        {
            FavorInfo favorInfo = PlayerDataManager.Instance.FavorData.GetFavorData(_npcId);
            _npcName.CurrentText.text = favorInfo.NpcName;
            _dialogueTxt.CurrentText.text = MogoLanguageUtil.GetContent(favorInfo.Dialogue);
        }

        private void InitFavorMainView()
        {
            _addFavorPoint.Clear();
            _favorPointTxt.Clear();
            _dialogueTxt.Clear();
        }

        private void RefreshFavorPoint()
        {
            //达到好感度上限
            FavorInfo favorInfo = PlayerDataManager.Instance.FavorData.GetFavorData(_npcId);
            _tweenProgressBar.Clear();
            if (favorInfo.IsPromote == false)
            {
                _tweenProgressBar.SetProgress(favorInfo.CurrentFavorPoint, favorInfo.UpgradeFavorPoint);
                _favorPointTxt.CurrentText.text = string.Format("{0}/0", favorInfo.CurrentFavorPoint); ;
            }
            else
            {
                _tweenProgressBar.SetProgress(favorInfo.CurrentFavorPoint, favorInfo.UpgradeFavorPoint);
                _favorPointTxt.CurrentText.text = string.Format("{0}/{1}", favorInfo.CurrentFavorPoint, favorInfo.UpgradeFavorPoint);
            }
        }

        private void FavorPointChange()
        {
            FavorInfo favorInfo = PlayerDataManager.Instance.FavorData.GetFavorData(_npcId);

            _favorPointTxt.CurrentText.text = string.Format("{0}/{1}", favorInfo.CurrentFavorPoint, favorInfo.UpgradeFavorPoint);
            if (_preFavorLevel != favorInfo.Level)
            {
                _tweenProgressBar.SetProgress(favorInfo.CurrentFavorPoint, favorInfo.UpgradeFavorPoint, favorInfo.UpgradeFavorPoint + 1);
            }
            else
            {
                _tweenProgressBar.SetProgress(favorInfo.CurrentFavorPoint, favorInfo.UpgradeFavorPoint);
            }
        }

        private void ShowFavorPointIcon()
        {
            FavorInfo favorInfo = PlayerDataManager.Instance.FavorData.GetFavorData(_npcId);
            for (int i = 0; i < _favorIcon.transform.childCount; i++)
            {
                _favorIcon.transform.GetChild(i).gameObject.SetActive(false);
            }
            _favorIcon.transform.GetChild(favorInfo.Level - 1).gameObject.SetActive(true);
        }

        private void InitSizeInfo()
        {
            _mainViewSize = GetComponent<RectTransform>().sizeDelta;
            _selectViewSize = _selectItemView.GetComponent<RectTransform>().sizeDelta;

        }

    }
}
