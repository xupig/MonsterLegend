﻿
using Common.Base;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleFavor
{
    public class FavorPanel:BasePanel
    {
        private FavorMainView _favorMainView;

        protected override void Awake()
        {
            _favorMainView = AddChildComponent<FavorMainView>("Container_main");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Favor; }
        }

        public override void OnShow(object data)
        {
            if (data != null)
            {
                _favorMainView.ShowNpc((int)data);
            }
            
        }

        public override void OnClose()
        {

        }
    }
}
