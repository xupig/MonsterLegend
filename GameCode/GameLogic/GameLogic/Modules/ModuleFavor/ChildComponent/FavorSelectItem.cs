﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using ModuleReward;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleFavor
{
    public class FavorSelectItem:KList.KListItemBase
    {
        private string favorValueContent;

        private KToggle _toggle;
        private ItemGrid _itemGrid;
        private FavorItemData _data;

        private StateText _txtItemNum;
        private StateText _txtfavorValue;

        protected override void Awake()
        {
            _toggle = GetChildComponent<KToggle>("Toggle_Xuanze");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _txtItemNum = GetChildComponent<StateText>("Label_txtShuliang");
            _txtfavorValue = GetChildComponent<StateText>("Label_txtShuzhi");
            favorValueContent = _txtfavorValue.CurrentText.text;
            AddEventListener();
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as FavorItemData;
                    Refresh();
                }
            }
        }

        public override bool IsSelected
        {
            get
            {
                return _toggle.isOn;
            }
            set
            {
                _toggle.isOn = value;
                if (_toggle.isOn == true)
                {
                    _toggle.interactable = false;
                }
                else
                {
                    _toggle.interactable = true;
                }
            }
        }

        public override void Dispose()
        {
            RemoveEventListener();
            _data = null;
        }

        private void AddEventListener()
        {
            _toggle.onValueChanged.AddListener(OnToggleClick);
        }

        private void RemoveEventListener()
        {
            _toggle.onValueChanged.RemoveListener(OnToggleClick);
        }

        private void OnToggleClick(KToggle target, bool isOn)
        {
            onClick.Invoke(this, Index);
        }

        private void HandlerClick()
        {
            if (_data.ItemData.StackCount <= 0)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.ItemChannel, _data.ItemData.Id);
            }
            else
            {
                onClick.Invoke(this, Index);
            }
        }

        private void Refresh()
        {
            _itemGrid.SetItemData(0);
            _itemGrid.SetItemData(_data.ItemData.Id, RefreshIcon);
            _txtItemNum.Clear();
            if (_data.ItemData.StackCount != 0)
            {
                _txtItemNum.CurrentText.text = _data.ItemData.StackCount.ToString();
            }
            _txtfavorValue.CurrentText.text = string.Format(favorValueContent, _data.FavorValue);
            _itemGrid.onClick = HandlerClick;

        }

        private void RefreshIcon(GameObject go)
        {
            if (_data.ItemData.StackCount <= 0)
            {
                SetGridDisabled(0.1f);
            }

        }
        private void SetGridDisabled(float grayValue)
        {
            KContainer container = GetChildComponent<KContainer>("Container_wupin");
            ImageWrapper[] wrappers = container.GetComponentsInChildren<ImageWrapper>();
            if (wrappers.Length > 0)
            {
                wrappers[0].SetGray(grayValue);
            }
            RectTransform rectTrans = container.GetChildComponent<RectTransform>("Container_yanse/holder");
            if (rectTrans != null)
            {
                wrappers = rectTrans.GetComponentsInChildren<ImageWrapper>();
                if (wrappers.Length > 0)
                {
                    wrappers[0].SetGray(grayValue);
                }
            }            
            rectTrans = container.GetChildComponent<RectTransform>("Container_icon/holder");
            wrappers = rectTrans.GetComponentsInChildren<ImageWrapper>();
            if (wrappers.Length > 0)
            {
                wrappers[0].SetGray(grayValue);
            }
            
            _toggle.Visible = false;
        }
    }

    public class FavorItemData
    {
        public ItemData ItemData;

        public int FavorValue;

        public FavorItemData(ItemData itemData, int favorValue)
        {
            ItemData = itemData;
            FavorValue = favorValue;
        }
    }
}
