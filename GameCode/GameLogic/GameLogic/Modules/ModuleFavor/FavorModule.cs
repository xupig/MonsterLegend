﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleFavor
{
    public class FavorModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.Favor, "Container_FavorPanel", MogoUILayer.LayerUIPanel, "ModuleFavor.FavorPanel", BlurUnderlay.Have, HideMainUI.Hide);
        }
    }
}
