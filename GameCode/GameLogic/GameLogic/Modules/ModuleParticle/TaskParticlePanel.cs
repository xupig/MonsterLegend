﻿#region 模块信息
/*==========================================
// 文件名：TaskParticlePanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleParticle
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/14 21:03:40
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleParticle
{

    public enum TaskParticleType
    {
        Accept = 0,
        Finish = 1,
    }

    public class TaskParticlePanel:BasePanel
    {
        //private KContainer _acceptTask;
        //private KParticle _acceptParticle;

        private KContainer _finishTask;
        private KParticle _finishParticle;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TaskParticle; }
        }

        protected override void Awake()
        {

            _finishTask = GetChildComponent<KContainer>("Container_wanchengrenwu");
            
            _finishParticle = AddChildComponent<KParticle>("fx_ui_3_3_wanchengrenwu");
            _finishTask.Visible = false;
            _finishParticle.Stop();

            base.Awake();
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            if (data != null)
            {
                TaskParticleType type = (TaskParticleType)data;
                if (type == TaskParticleType.Accept)
                {
                    HideFinish();
                }
                else if(type == TaskParticleType.Finish)
                {
                    ShowFinish();
                }
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _finishParticle.onComplete.AddListener(OnFinishParticleComplete);
        }

        private void RemoveEventListener()
        {
            _finishParticle.onComplete.RemoveListener(OnFinishParticleComplete);
        }

        private void OnFinishParticleComplete()
        {
            _finishTask.Visible = false;
            ClosePanel();
        }

        private void ShowFinish()
        {
            _finishTask.Visible = true;
            _finishParticle.Play();
            _finishParticle.Position = new Vector3(-311, 119, 0);
        }

        private void HideFinish()
        {
            _finishTask.Visible = false;
            _finishParticle.Stop();
        }

        private void ShowRuneWish()
        {
            _finishTask.Visible = true;
            _finishParticle.Play();
            _finishParticle.Position = new Vector3(-311, 119, 0);
        }

        private void HideRuneWish()
        {
            _finishTask.Visible = false;
            _finishParticle.Stop();
        }

    }
}
