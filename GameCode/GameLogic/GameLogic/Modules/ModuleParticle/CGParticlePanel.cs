﻿using Common.Base;
using Common.ExtendComponent;
using Game.Asset;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleParticle
{
    public class CGParticlePanel : BasePanel
    {
        private GameObject _particleGo;
        private RectTransform _maskRect;

        private float x;
        private float y;
        private uint time;

        private uint _timerId;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.CGParticle; }
        }

        protected override void Awake()
        {
            base.Awake();
            ModalMask = GetChildComponent<StateImage>("Container_zhezhao/ScaleImage_sharedZhezhao");
            ModalMask.Alpha = 0;
            _maskRect = GetChildComponent<RectTransform>("Container_zhezhao/ScaleImage_sharedZhezhao");
            gameObject.AddComponent<RaycastComponent>();
        }

        public override void OnShow(object data)
        {
            if (data is string)
            {
                ShowParticle(data);
            }
        }

        private void ShowParticle(object data)
        {
            string param = data as string;
            string[] paramList = param.Split(',');
            if (paramList.Length >= 4)
            {
                x = float.Parse(paramList[1]);
                y = float.Parse(paramList[2]);
                time = uint.Parse(paramList[3]);
                ObjectPool.Instance.GetGameObject(paramList[0], (obj) =>
                {
                    OnParticleLoaded(obj);
                });
            }
        }

        private void OnParticleLoaded(UnityEngine.Object obj)
        {
            if (obj == null) { return; }
            ClearGameObject();
            _particleGo = obj as GameObject;
            _particleGo.transform.SetParent(_maskRect);
            _particleGo.transform.localPosition = new Vector3(x * _maskRect.sizeDelta.x, -y * _maskRect.sizeDelta.y);
            _particleGo.transform.localScale = Vector3.one;
            _particleGo.SetActive(true);
            ResetTimer();
            _timerId = TimerHeap.AddTimer(time, 0, OnPlayEnd);
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private void OnPlayEnd()
        {
            _particleGo.SetActive(false);
            ResetTimer();
            ClosePanel();
        }

        private void ClearGameObject()
        {
            if (_particleGo != null)
            {
                DestroyImmediate(_particleGo);
                _particleGo = null;
            }
        }

        public override void OnClose()
        {
            ResetTimer();
        }

    }
}
