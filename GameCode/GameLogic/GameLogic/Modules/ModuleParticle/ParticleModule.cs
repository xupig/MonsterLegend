﻿#region 模块信息
/*==========================================
// 文件名：ParticleModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleParticle
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/14 21:02:19
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleParticle
{
    public class ParticleModule:BaseModule
    {
        public override void Init()
        {
            AddPanel(PanelIdEnum.TaskParticle, "Container_TaskEffect", MogoUILayer.LayerEffect, "ModuleParticle.TaskParticlePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.CGParticle, "Container_MaskPanel", MogoUILayer.LayerEffect, "ModuleParticle.CGParticlePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

    }
}
