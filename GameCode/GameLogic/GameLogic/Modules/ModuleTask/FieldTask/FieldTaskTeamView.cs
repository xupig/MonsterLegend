﻿using Common.Base;
using Common.Chat;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using Mogo.Util;
using System.Collections.Generic;

namespace ModuleTask.ChildView
{
    public class FieldTaskTeamView : KContainer
    {
        private StateText _descTxt;
        private StateText _matchTxt;
        private KButton _callBtn;
        private KButton _matchBtn;
        private CopyTeamView _teamView;

        protected override void Awake()
        {
            base.Awake();
            _descTxt = GetChildComponent<StateText>("Label_txtJiequtiaojian");
            _matchTxt = GetChildComponent<StateText>("Label_txtXitongpipei");
            _callBtn = GetChildComponent<KButton>("Button_shijiehanren");
            _matchBtn = GetChildComponent<KButton>("Button_pipeiduiyuan");
            _teamView = AddChildComponent<CopyTeamView>("Container_duiyou");
            _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(6018048);
        }

        private void RefreshContent()
        {
            _matchTxt.Visible = false;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            RefreshContent();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }


        private void AddEventListener()
        {
            _callBtn.onClick.AddListener(OnCallBtnClick);
            _matchBtn.onClick.AddListener(OnMatchBtnClick);
        }

        private void RemoveEventListener()
        {
            _callBtn.onClick.RemoveListener(OnCallBtnClick);
            _matchBtn.onClick.RemoveListener(OnMatchBtnClick);
        }

        private void OnCallBtnClick()
        {
            if (PlayerDataManager.Instance.TeamData.TeammateDic.Count >= 4)
            {
                MogoUtils.FloatTips(83031);
                return;
            }
            //int instId = 0;
            //if (PlayerDataManager.Instance.TaskData.WildTaskInfo.wild_task_id == 0)
            //{
            //    wild_task wildTaskData = wild_task_helper.GetWildTask();
            //    instId = wildTaskData.__instance_id;
            //}
            //else
            //{
            //    wild_task wildTaskData = wild_task_helper.GetWildTask(PlayerDataManager.Instance.TaskData.WildTaskInfo.wild_task_id);
            //    instId = wildTaskData.__instance_id;
            //}
            TeamInstanceManager.Instance.CallPeople(200);
        }

        private void OnMatchBtnClick()
        {

            if (PlayerDataManager.Instance.TeamData.TeamId != 0)
            {
                PanelIdEnum.TeamMatch.Show(200);
            }
            else
            {
                PanelIdEnum.Interactive.Show(new int[] { 2, 1, 200+1 });
            }
        }

    }
}
