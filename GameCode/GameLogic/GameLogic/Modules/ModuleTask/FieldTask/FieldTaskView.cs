﻿using Common.Base;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleTask.ChildComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTask.ChildView
{
    public class FieldTaskView : KContainer
    {
        private FieldTaskDetailView _detailView;
        private FieldTaskTeamView _teamView;
        private FieldTaskRewardView _rewardView;

        protected override void Awake()
        {
            base.Awake();
            _detailView = AddChildComponent<FieldTaskDetailView>("Container_renwuxinxi");
            _teamView = AddChildComponent<FieldTaskTeamView>("Container_duiwu");
            _rewardView = AddChildComponent<FieldTaskRewardView>("Container_jiangli");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

      
    }
}
