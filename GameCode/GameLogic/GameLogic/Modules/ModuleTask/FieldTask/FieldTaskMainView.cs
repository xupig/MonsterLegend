﻿using Common.Base;
using Common.Events;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{

    public class FieldTaskMainView : KContainer
    {
        private StateText _txtContent;
        private KButton _openBtn;

        protected override void Awake()
        {
            base.Awake();
            _openBtn = GetChildComponent<KButton>("Button_zhankai");
            _txtContent = GetChildComponent<StateText>("Label_txtXinxi");
            _txtContent.CurrentText.text = MogoLanguageUtil.GetContent(31146);
            _openBtn.onClick.AddListener(OnOpenBtnClick);
        }

        private void OnOpenBtnClick()
        {
            FieldTaskManager.Instance.ShowAutoTaskMessageBox();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            EventDispatcher.AddEventListener<FightType>(PlayerEvents.ON_FIGHT_TYPE_CHANGED, OnChangeFightType);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            EventDispatcher.RemoveEventListener<FightType>(PlayerEvents.ON_FIGHT_TYPE_CHANGED, OnChangeFightType);
        }

        private void OnChangeFightType(FightType type)
        {
            if (type == FightType.FOLLOW_FIGHT)
            {
                PlayerDataManager.Instance.TaskData.IsAutoWildTask = false;
                this.Visible = false;
            }
        }

        //private float elapsed = 0f;
        //private const float CHECK_INTERVAL = 1.0f;
        //void Update()
        //{
        //    elapsed += Time.deltaTime;
        //    if (elapsed > CHECK_INTERVAL)
        //    {
        //        elapsed -= CHECK_INTERVAL;
        //        if (PlayerActionManager.GetInstance().GetWaitingQueue().Count == 0)
        //        {
        //            FieldTaskManager.Instance.GotoWildTask();
        //        }
        //    }
        //}

    }
}
