﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using ModuleTask.ChildComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTask.ChildView
{
    public class FieldTaskProgressView : KContainer
    {
        private StateText _targetTxt;
        private StateText _progressTxt;
        private StateText _rewardTxt;

        protected override void Awake()
        {
            base.Awake();
            _targetTxt = GetChildComponent<StateText>("Label_txtDuiwumubiaoxinxi");
            _progressTxt = GetChildComponent<StateText>("Label_txtDuiwujinduxinxi");
            _rewardTxt = GetChildComponent<StateText>("Label_txtDuiwujingyan");
        }

        private void RefreshContent()
        {
            if (wild_task_helper.HasWildTask() == false) { return; }
            PbWildTaskInfo wildTaskInfo = PlayerDataManager.Instance.TaskData.WildTaskInfo;
            task_data taskData = task_data_helper.GetTaskData(wildTaskInfo.task_id);
            _targetTxt.CurrentText.text = task_data_helper.GetTaskSummary(taskData);
            _progressTxt.CurrentText.text = MogoLanguageUtil.GetContent(115033, wildTaskInfo.task_count, 1 + wildTaskInfo.task_count % 10);
            int rewardId = GetReward(wild_task_reward_helper.GetWildTaskReward(wildTaskInfo.wild_task_id).__reward, wildTaskInfo.task_count + 1);
            _rewardTxt.CurrentText.text = GetRewardContent(rewardId);
        }

        private string GetRewardContent(int rewardId)
        {
            string content = string.Empty;
            if (rewardId == 0)
            {
                return content;
            }
            List<RewardData> rewardDataList = item_reward_helper.GetRewardDataList(rewardId, PlayerAvatar.Player.vocation);
            for (int i = 0; i < rewardDataList.Count; i++)
            {
                content = string.Concat(content, " ", string.Format("{0}x{1}", item_helper.GetName(rewardDataList[i].id), rewardDataList[i].num));
            }
            return content;
        }

        private int GetReward(Dictionary<string, string> rewardDic, int value)
        {
            int result = 0;
            foreach (KeyValuePair<string, string> kvp in rewardDic)
            {
                int now = int.Parse(kvp.Key);
                if (now > result && now <= value)
                {
                    result = now;
                }
            }
            return int.Parse(rewardDic[result.ToString()]);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            RefreshContent();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

      
    }
}
