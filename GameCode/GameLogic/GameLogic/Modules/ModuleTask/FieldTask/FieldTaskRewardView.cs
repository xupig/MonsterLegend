﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using ModuleTask.ChildComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTask.ChildView
{
    public class FieldTaskRewardView : KContainer
    {
        private KScrollView _scrollView;
        private KList _list;

        protected override void Awake()
        {
            base.Awake();
            _scrollView = GetChildComponent<KScrollView>("ScrollView_guize");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        private void RefreshContent()
        {
            List<List<string>> dataList = new List<List<string>>();
            wild_task_reward rewardData = wild_task_reward_helper.GetWildTaskReward();
            dataList.Add(rewardData.__reward_icon);
            dataList.Add(rewardData.__first_ten_reward_icon);
            dataList.Add(rewardData.__repeat_ten_reward_icon);
            _list.SetDataList<FieldTaskRewardItem>(dataList, 2);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            RefreshContent();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }
      
    }

    public class FieldTaskRewardItem : KList.KListItemBase
    {
        private StateText _descTxt;
        private StateImage _getImage;
        private List<RewardGrid> _rewardItemList;

        private List<string> _data;

        protected override void Awake()
        {
            _descTxt = GetChildComponent<StateText>("Label_txtJianglixinxi");
            _getImage = GetChildComponent<StateImage>("Image_sharedyilingqu");
            _rewardItemList = new List<RewardGrid>();
            _rewardItemList.Add(AddChildComponent<RewardGrid>("Container_rewardGrid0"));
            _rewardItemList.Add(AddChildComponent<RewardGrid>("Container_rewardGrid1"));
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as List<string>;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            _descTxt.CurrentText.text = GetDescContent();
            _getImage.Visible = false;
            RefreshGetView();
        }

        private string GetDescContent()
        {
            return MogoLanguageUtil.GetContent(6018049 + Index);
        }

        private void RefreshGetView()
        {
            List<RewardData> rewardDataList = new List<RewardData>();
            for (int i = 0; i < _data.Count; i++)
            {
                rewardDataList.Add(new RewardData(int.Parse(_data[i]), 1));
            }
            rewardDataList = RewardData.FilterSameItemId(rewardDataList);
            for (int i = 0; i < _rewardItemList.Count; i++)
            {
                if (i < rewardDataList.Count)
                {
                    _rewardItemList[i].Visible = true;
                    _rewardItemList[i].Data = rewardDataList[i];
                }
                else
                {
                    _rewardItemList[i].Visible = false;
                }
            }
        }

        public override void Dispose()
        {
            if (_data != null)
            {
                _data.Clear();
            }
            _data = null;
        }
    }
}
