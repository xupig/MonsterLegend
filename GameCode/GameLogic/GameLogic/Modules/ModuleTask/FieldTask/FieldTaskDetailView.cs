﻿using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using ModuleTask.ChildComponent;
using ModuleTask.Handle;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTask.ChildView
{
    public class FieldTaskDetailView : KContainer
    {
        private StateText _titleTxt;
        private StateText _regionTxt;
        private StateText _tipsTxt;
        private KButton _receiveBtn;
        private KButton _gotoBtn;
        private StateText _descTxt;
        private FieldTaskProgressView _progressView;

        protected override void Awake()
        {
            base.Awake();
            _titleTxt = GetChildComponent<StateText>("Label_txtBiaoti");
            _regionTxt = GetChildComponent<StateText>("Label_txtRenwuquyu");
            _tipsTxt = GetChildComponent<StateText>("Label_txtTishi");
            _receiveBtn = GetChildComponent<KButton>("Button_jieshourenwu");
            _gotoBtn = GetChildComponent<KButton>("Button_qianwang");
            _descTxt = GetChildComponent<StateText>("Label_txtJieshi");
            _progressView = AddChildComponent<FieldTaskProgressView>("Container_renwujilu");
            _tipsTxt.CurrentText.text = MogoLanguageUtil.GetContent(6018052);
            _tipsTxt.Visible = false;
        }

        private void RefreshContent()
        {
            if (wild_task_helper.HasWildTask() == false)
            {
                RefreshNoneTaskView();
            }
            else
            {
                RefreshCurrentTaskView();
            }
        }

        private void RefreshCurrentTaskView()
        {
            PbWildTaskInfo wildTaskInfo = PlayerDataManager.Instance.TaskData.WildTaskInfo;
            wild_task wildTaskData = wild_task_helper.GetWildTask(PlayerDataManager.Instance.TaskData.WildTaskInfo.wild_task_id);
            task_data taskData = task_data_helper.GetTaskData(wildTaskInfo.task_id);
            _regionTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(6018047), MogoLanguageUtil.GetContent(instance_helper.GetName(wildTaskData.__instance_id)));
            _titleTxt.CurrentText.text = task_data_helper.GetTaskName(taskData);
            _descTxt.CurrentText.text = task_data_helper.GetTaskDesc(taskData);
            _descTxt.Visible = false;
            _receiveBtn.Visible = false;
            _gotoBtn.Visible = true;
            _progressView.Visible = true;
        }

        private void RefreshNoneTaskView()
        {
            wild_task wildTaskData = wild_task_helper.GetWildTask();
            task_data taskData = task_data_helper.GetTaskData(int.Parse(wildTaskData.__task_id[0]));
            _regionTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(6018047), MogoLanguageUtil.GetContent(instance_helper.GetName(wildTaskData.__instance_id)));
            _titleTxt.CurrentText.text = task_data_helper.GetTaskName(taskData);
            _descTxt.CurrentText.text = task_data_helper.GetTaskDesc(taskData);
            _descTxt.Visible = true;
            _receiveBtn.Visible = true;
            _gotoBtn.Visible = false;
            _progressView.Visible = false;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            RefreshContent();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }


        private void AddEventListener()
        {
            _receiveBtn.onClick.AddListener(OnReceiveBtnClick);
            _gotoBtn.onClick.AddListener(OnGotoBtnClick);
            EventDispatcher.AddEventListener(FieldTaskEvents.ON_GET_FIELD_TASK_INFO, RefreshContent);
        }

        private void RemoveEventListener()
        {
            _receiveBtn.onClick.RemoveListener(OnReceiveBtnClick);
            _gotoBtn.onClick.RemoveListener(OnGotoBtnClick);
            EventDispatcher.RemoveEventListener(FieldTaskEvents.ON_GET_FIELD_TASK_INFO, RefreshContent);
        }

        private void OnGotoBtnClick()
        {
            FieldTaskManager.Instance.ShowAutoTaskMessageBox();
        }

        private void OnReceiveBtnClick()
        {
            FieldTaskManager.Instance.AcceptTask();
        }

    }
}
