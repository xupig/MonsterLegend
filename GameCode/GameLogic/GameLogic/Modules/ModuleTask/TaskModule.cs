﻿#region 模块信息
/*==========================================
// 文件名：TaskModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleTask
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/20 20:48:10
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using GameData;
using GameLoader.Utils;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleTask.Handle;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTask
{
    public class TaskModule:BaseModule
    {
        public static int npcId;

        public TaskModule()
        {

        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            RegisterPopPanelBunchPanel(PanelIdEnum.TeamTaskBox, PanelIdEnum.Task);
            RegisterPopPanelBunchPanel(PanelIdEnum.GuildTask, PanelIdEnum.Task);
            EventDispatcher.AddEventListener<int, bool>(TaskEvent.TALK_TO_NPC, OnNpcDialogChange);
            AddPanel(PanelIdEnum.NpcDailog, "Container_NpcDialogPanel", MogoUILayer.LayerUIPanel, "ModuleTask.NpcDialogPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);
            AddPanel(PanelIdEnum.Task, "Container_TaskPanel", MogoUILayer.LayerUIPanel, "ModuleTask.TaskPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.TeamTaskTip, "Container_TeamTaskTipPanel", MogoUILayer.LayerUIToolTip, "ModuleTask.TeamTaskTipsPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.TeamTaskOfflineReward, "Container_TeamTaskOfflineRewardPanel", MogoUILayer.LayerUIToolTip, "ModuleTask.TeamTaskOfflineRewardPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.TeamTaskBox, "Container_TeamTaskBoxPanel", MogoUILayer.LayerUIPopPanel, "ModuleTask.TeamTaskBoxPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.GuildTask, "Container_GuildTaskPanel", MogoUILayer.LayerUIPopPanel, "ModuleTask.GuildTaskPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.GuildTaskTips, "Container_GuildTaskTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleTask.GuildTaskTipsPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }

        public static void EnterTeamTaskMission()
        { 
            Dictionary<string, string[]> taskFollow = task_data_helper.GetTaskFollow(PlayerDataManager.Instance.TaskData.teamTaskInfo.task_id);
            if (taskFollow != null)
            {
                foreach (KeyValuePair<string, string[]> kvp in taskFollow)
                {
                    Dictionary<TaskEventEnum, TaskEventData> dic = task_data_helper.GetTaskEventData(PlayerDataManager.Instance.TaskData.teamTaskInfo.task_id);
                    if (dic.ContainsKey(TaskEventEnum.InstId))
                    {
                        int instId = (int)dic[TaskEventEnum.InstId].value;
                        LoggerHelper.Info("时空裂隙进什么副本：" + instId + "," + PlayerDataManager.Instance.TaskData.teamTaskInfo.task_id);
                        MissionManager.Instance.RequsetEnterMission(instId);
                    }
                    return;
                }
            }
        }

        private void OnNpcDialogChange(int npcId, bool checkTask)
        {
            if (UIManager.GetLayerVisibility(MogoUILayer.LayerUIPanel) == false)
            {
                return;
            }
           if(npcId == PlayerDataManager.Instance.TaskData.npcId)
           {
               EnterTeamTaskMission();
               return;
           }
           //TaskManager.Instance.StopAutoTask();
            int taskId = NpcTaskHandle.GetNpcTask(npcId);
            int dialog = npc_helper.GetRandomDialogID(npcId);
            if(taskId!=0)
            {
                TaskModule.npcId = npcId;

                if (PlayerDataManager.Instance.TaskData.ContainsDialog(taskId))
                {
                    task_data taskData = task_data_helper.GetTaskData(taskId);
                    if (taskData.__task_type == public_config.TASK_TYPE_TEAM)
                    {
                        PanelIdEnum.Task.Show(2);
                    }
                    else
                    {
                        PanelIdEnum.Task.Show();
                    }
                }
                else if (dialog!=-1)
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.NpcDailog);
                }
            }
            else if (!checkTask && dialog != -1)
            {
                TaskModule.npcId = npcId;
                PanelIdEnum.NpcDailog.Show();
            }
        }

    }
}
