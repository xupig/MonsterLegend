﻿#region 模块信息
/*==========================================
// 文件名：TeamTaskMemberItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleTask.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/12/2 10:18:26
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTask
{
    public class TeamTaskMemberItem:KContainer
    {
        private StateText _txtName;
        private StateText _txtLevel;
        private IconContainer _iconContainer;
        private PbTeamMember _member;

        protected override void Awake()
        {
            _txtLevel = GetChildComponent<StateText>("txtDengji");
            _iconContainer = AddChildComponent<IconContainer>("icon");
            base.Awake();
        }

        public void UpdateContent(PbTeamMember member)
        {
            _member = member;
            _txtLevel.CurrentText.text = member.level.ToString();
            _iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)member.vocation), OnIconLoaded);
        }

        private void OnIconLoaded(GameObject go)
        {
            RefreshImageState();
        }

        private void RefreshOnlineContent(ulong dbid, int online)
        {
            RefreshImageState();
        }

        private void RefreshImageState()
        {
            if (PlayerDataManager.Instance.TeamData.TeammateDic.ContainsKey(_member.dbid))
            {
                PbTeamMember memberInfo = PlayerDataManager.Instance.TeamData.TeammateDic[_member.dbid];
                ImageWrapper[] wrappers = _iconContainer.gameObject.GetComponentsInChildren<ImageWrapper>();
                for (int i = 0; i < wrappers.Length; i++)
                {
                    wrappers[i].SetGray(memberInfo.online);
                }
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        public void AddEventListener()
        {
            EventDispatcher.AddEventListener<UInt64, int>(TeamEvent.TEAM_MATE_ONLINE_REFRESH, RefreshOnlineContent);
        }

        public void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<UInt64, int>(TeamEvent.TEAM_MATE_ONLINE_REFRESH, RefreshOnlineContent);
        }

    }
}
