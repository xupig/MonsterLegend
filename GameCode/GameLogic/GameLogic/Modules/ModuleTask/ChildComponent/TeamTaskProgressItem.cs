﻿#region 模块信息
/*==========================================
// 文件名：TeamTaskRewardItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleTask.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/12/2 9:58:49
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTask
{
    public class TeamTaskProgressItem:KContainer
    {
        private KContainer _haveGet;
        private KButton _btnGet;
        private StateText _txtNum;
        private spacetime_rift_times_reward _reward;
        private static string _template;

        protected override void Awake()
        {
            _haveGet = GetChildComponent<KContainer>("Container_haveGet");
            _btnGet = GetChildComponent<KButton>("Button_get");
            _txtNum = GetChildComponent<StateText>("Label_txtShuliang");
            _template = _txtNum.CurrentText.text;
            _btnGet.onClick.AddListener(OnClickGet);
            base.Awake();
        }

        public void UpdateContent(spacetime_rift_times_reward reward)
        {
            _reward = reward;
            _txtNum.CurrentText.text = string.Format(_template, reward.__task_times);
            UpdateRewardTimes();
        }

        public void UpdateRewardTimes()
        {
            _haveGet.Visible = PlayerDataManager.Instance.TaskData.hasReceivedTeamTaskTimeReward.Contains(_reward.__extra_reward_id);
            _btnGet.Visible = !_haveGet.Visible;
            DoTween(PlayerDataManager.Instance.TaskData.team_task_times >= _reward.__task_times && _btnGet.Visible);
        }

        protected override void OnDestroy()
        {
            _btnGet.onClick.AddListener(OnClickGet);
            base.OnDestroy();
        }

        private void OnClickGet()
        {
            //if (PlayerDataManager.Instance.TaskData.team_task_times >= _reward.__task_times)
            //{
            //    if (PlayerDataManager.Instance.TaskData.hasReceivedTeamTaskTimeReward.Contains(_reward.__extra_reward_id) == false)
            //    {
            //        TaskManager.Instance.ReceiveTeamTaskTimeReward(_reward.__extra_reward_id);
            //    }
            //}
            //else
            //{

            //}
            PanelIdEnum.TeamTaskBox.Show(_reward);
        }

        public void DoTween(bool isTween)
        {
            TweenTreasureBox.ResetPosition(_btnGet.gameObject);
            if (isTween)
            {
                TweenTreasureBox.Begin(_btnGet.gameObject);
            }
            else
            {
                TweenTreasureBox.Stop(_btnGet.gameObject);
            }
        }
    }
}
