﻿#region 模块信息
/*==========================================
// 文件名：BranchTaskItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleTask.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/27 14:01:36
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleTask.Handle;
using System.Collections.Generic;
using UnityEngine;
using Common.ExtendComponent;
using Common.Data;
using Common.Global;
using ModuleCommonUI;
using System;
using MogoEngine.Events;
using Common.Events;

namespace ModuleTask.ChildComponent
{

    public class BranchTaskItem : KList.KListItemBase
    {
        private PbTaskInfo taskInfo;
        private StateText _txtName;
        private StateText _txtTaskTarget;
        private KButton _btnReceive;
        private KButton _btnGoAhead;
        private KButton _btnAbandon;
        // private StateText _txtGold;
        //private StateText _txtExp;
        private List<RewardGrid> rewardList;
        private KParticle _particle;

        // private static string goldTemplate;
        // private static string expTemplate;

        private IconContainer _iconContainer;

        private static string targetTemplate;

        private MaskEffectItem _effectItem;

        protected override void Awake()
        {
            _btnReceive = GetChildComponent<KButton>("Container_lingqu/Button_queding");
            _particle = AddChildComponent<KParticle>("Container_lingqu/fx_ui_3_2_lingqu");
            _btnGoAhead = GetChildComponent<KButton>("Container_qianwang/Button_qianwang");
            _btnAbandon = GetChildComponent<KButton>("Container_fangqi/Button_fangqi");
            _txtName = GetChildComponent<StateText>("Container_wupinmingcheng/Label_txtRenwumingcheng");
            _txtTaskTarget = GetChildComponent<StateText>("Container_wupinmingcheng/Label_txtTaskTarget");
            if (targetTemplate == null)
            {
                targetTemplate = _txtTaskTarget.CurrentText.text;
            }
            //_txtGold = GetChildComponent<StateText>("Container_wupinmingcheng/Label_txtGold");
            // _txtExp = GetChildComponent<StateText>("Container_wupinmingcheng/Label_txtExp");
            //if (goldTemplate == null)
            //{
            //    goldTemplate = _txtGold.CurrentText.text;
            //    expTemplate = _txtExp.CurrentText.text;
            //}

            rewardList = new List<RewardGrid>();
            for (int i = 0; i < 3; i++)
            {
                RewardGrid grid = AddChildComponent<RewardGrid>("Container_wupinmingcheng/Container_jiangli/Container_rewardGrid" + i);
                rewardList.Add(grid);
            }
            _iconContainer = AddChildComponent<IconContainer>("Container_taskGrid/Container_icon");
            _effectItem = gameObject.AddComponent<MaskEffectItem>();
            _effectItem.Init(_particle.transform.parent.gameObject, IsParticleShow);
            _particle.Stop();
        }

        private bool IsParticleShow()
        {
            return _btnReceive.Visible;
        }

        public override void Dispose()
        {
            RemoveEvents();
            base.Data = null;
        }

        private void AddEvents()
        {
            _btnReceive.onClick.AddListener(OnClickReceiveHandler);
            _btnGoAhead.onClick.AddListener(OnClickGoAheadHandler);
            _btnAbandon.onClick.AddListener(OnClickAbandonHandler);
            EventDispatcher.AddEventListener<int>(TaskEvent.TASK_PROGRESS_CHANGE, OnTaskProgressChange);
        }

        private void RemoveEvents()
        {
            _btnReceive.onClick.RemoveListener(OnClickReceiveHandler);
            _btnGoAhead.onClick.RemoveListener(OnClickGoAheadHandler);
            _btnAbandon.onClick.RemoveListener(OnClickAbandonHandler);
            EventDispatcher.RemoveEventListener<int>(TaskEvent.TASK_PROGRESS_CHANGE, OnTaskProgressChange);
        }

        private void OnTaskProgressChange(int taskId)
        {
             if (taskInfo != null && taskInfo.task_id == taskId)
             {
                 if(PlayerDataManager.Instance.TaskData.mainTaskInfo!=null && PlayerDataManager.Instance.TaskData.mainTaskInfo.task_id == taskId)
                 {
                     taskInfo = PlayerDataManager.Instance.TaskData.mainTaskInfo;
                 }
                 else if (PlayerDataManager.Instance.TaskData.branchTaskDic.ContainsKey(taskId))
                 {
                     taskInfo = PlayerDataManager.Instance.TaskData.branchTaskDic[taskId];
                 }
                 else
                 {
                     taskInfo = null;
                 }
                 UpdateContent();
             }
        }

        public override void Show()
        {
            AddEvents();
        }

        public override void Hide()
        {
            RemoveEvents();
        }

        public override object Data
        {
            get
            {
                return taskInfo;
            }
            set
            {
                taskInfo = value as PbTaskInfo;
                UpdateContent();
            }
        }

        private void UpdateContent()
        {
            ID = 0;
            if (taskInfo != null)
            {
                ID = taskInfo.task_id;
                task_data _data = task_data_helper.GetTaskData(taskInfo.task_id);
                _iconContainer.SetIcon(_data.__task_icon);
                _txtName.CurrentText.text = task_data_helper.GetTaskName(_data);
                string taskTargetProgress = string.Empty;
                int total = task_data_helper.getTaskTargetNum(_data);
                if (total > 1 && taskInfo.target != null && taskInfo.target.Count > 0)
                {
                    taskTargetProgress = string.Format("({0}/{1})", taskInfo.target[0].value, total);
                    font_style fontStyle = style_helper.GetStyle(6);
                    taskTargetProgress = MogoStringUtils.AppendStyle(taskTargetProgress, fontStyle);
                }
                _txtTaskTarget.CurrentText.text = string.Format(targetTemplate, string.Format("{0}{1}", task_data_helper.GetTaskSummary(_data), taskTargetProgress));
                UpdateReward(_data);
                if (_data.__target != null && _data.__target.Count > 0)
                {
                    if (taskInfo.status == public_config.TASK_STATE_ACCOMPLISH)
                    {
                        EntityNPC npc = NPCManager.GetInstance().FindNPCByNPCID(_data.__npc_id_report);
                        if (_data.__npc_id_report == 0 || _data.__npc_id_report == 999 || (npc != null && npc.IsNearbyPlayer() == true))
                        {
                            _btnReceive.Visible = true;
                            _btnGoAhead.Visible = false;
                        }
                        else
                        {
                            _btnReceive.Visible = false;
                            _btnGoAhead.Visible = true;
                        }
                    }
                    else
                    {
                        _btnReceive.Visible = false;
                        _btnGoAhead.gameObject.SetActive(taskInfo.status == public_config.TASK_STATE_NEW || taskInfo.status == public_config.TASK_STATE_ACCEPT);
                    }
                }
                else
                {
                    bool dialogFinish = PlayerDataManager.Instance.TaskData.ContainsDialog(_data.__id);
                    if (dialogFinish)
                    {
                        EntityNPC npc = NPCManager.GetInstance().FindNPCByNPCID(_data.__npc_id_report);
                        if (_data.__npc_id_report == 0 || _data.__npc_id_report == 999 || (npc != null && npc.IsNearbyPlayer() == true))
                        {
                            _btnReceive.Visible = true;
                            _btnGoAhead.Visible = false;
                        }
                        else
                        {
                            _btnReceive.Visible = false;
                            _btnGoAhead.Visible = true;
                        }
                    }
                    else
                    {
                        _btnReceive.gameObject.SetActive(dialogFinish);
                        _btnGoAhead.gameObject.SetActive(dialogFinish == false);
                    }
                }
                if (_btnReceive.Visible)
                {
                    _particle.Play(true);
                    _particle.Position = new Vector3(0, 0, -1);
                }
                else
                {
                    _particle.Stop();
                }
                _btnAbandon.Visible = false;
                if (_btnGoAhead.Visible == true && (task_data_helper.GetTaskType(taskInfo.task_id) == public_config.TASK_TYPE_WILD || task_data_helper.GetTaskType(taskInfo.task_id) == public_config.TASK_TYPE_GUILD))
                {
                    _btnAbandon.Visible = true;
                }
            }
            else
            {
                _btnAbandon.Visible = false;
                _particle.Stop();

            }
            this.gameObject.SetActive(taskInfo != null);
            _effectItem.UpdateMaskEffect();
        }

        private void UpdateReward(task_data data)
        {
            List<BaseItemData> baseItemDataList = task_data_helper.GetReward(data, PlayerAvatar.Player.vocation);
            ClearAllItems();
            for (int i = 0; i < baseItemDataList.Count; i++)
            {
                BaseItemData baseItemData = baseItemDataList[i];
                AddItem(i, baseItemData.Id, baseItemData.StackCount);
            }
        }

        private void ClearAllItems()
        {
            foreach (RewardGrid grid in rewardList)
            {
                grid.Visible = false;
            }
        }

        private void AddItem(int index, int itemId, int num)
        {
            if (index >= 3)
            {
                PbTaskInfo taskInfo = base.Data as PbTaskInfo;
                Debug.LogWarning(string.Format("任务{0}的奖励配了超过5个物品（非金币经验）", taskInfo.task_id));
                return;
            }
            RewardData data = RewardData.GetRewardData();
            data.id = itemId;
            data.num = num;
            rewardList[index].gameObject.SetActive(true);
            rewardList[index].Data = data;
        }

        private void OnClickReceiveHandler()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.Task);
            TaskManager.Instance.FinishTask((uint)(Data as PbTaskInfo).task_id);
        }

        private void OnClickGoAheadHandler()
        {
            task_data _data = task_data_helper.GetTaskData((Data as PbTaskInfo).task_id);
            UIManager.Instance.ClosePanel(PanelIdEnum.Task);
            if (_data.__task_type == public_config.TASK_TYPE_WILD)
            {
                FieldTaskManager.Instance.ShowAutoTaskMessageBox();
            }
            else
            {
                TaskFollowHandle.TaskFollow(_data);
            }
        }

        private void OnClickAbandonHandler()
        {
            string content = MogoLanguageUtil.GetContent(6018058);
            task_data _data = task_data_helper.GetTaskData((Data as PbTaskInfo).task_id);
            Action abandonAction = null;
            if (_data.__task_type == public_config.TASK_TYPE_GUILD)
            {
                abandonAction = OnEnsureAbandonGuildTask;
            }
            else if (_data.__task_type == public_config.TASK_TYPE_WILD)
            {
                abandonAction = OnEnsureAbandonFieldTask;
            }
            MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, () => { }, abandonAction, MogoLanguageUtil.GetContent(6018060), MogoLanguageUtil.GetContent(6018059), null, true);
        }

        private void OnEnsureAbandonGuildTask()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.Task);
            GuildTaskManager.Instance.AbandonGuildTask(guild_task_helper.GetTaskGuildId(guild_task_helper.GetCurrentTaskId()));
        }

        private void OnEnsureAbandonFieldTask()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.Task);
            FieldTaskManager.Instance.AbandonTask();
        }

    }
}
