﻿#region 模块信息
/*==========================================
// 文件名：TeamTaskOfflineRewardItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleTask.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/12/8 14:40:05
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTask
{
    public class TeamTaskOfflineRewardItem:KList.KListItemBase
    {

        private PbOfflineRecord _record;

        private IconContainer _icon;
        private StateText _txtName;
        private StateText _txtLevel;
        private StateText _txtCount;
        private KButton _btnAdd;
        private KButton _btnGiveAward;

        private static string _template;

        private PbFriendAvatarInfo _avatarInfo;

        protected override void Awake()
        {
            _icon = AddChildComponent<IconContainer>("Container_icon");
            _txtName = GetChildComponent<StateText>("Label_txtDuiangmingcheng");
            _txtLevel = GetChildComponent<StateText>("Label_txtDengji");
            _txtCount = GetChildComponent<StateText>("Label_txtDaiduicishu");
            _template = _txtCount.CurrentText.text;
            _btnAdd = GetChildComponent<KButton>("Button_tianjia");
            _btnGiveAward = GetChildComponent<KButton>("Button_jiangli");
            _avatarInfo = new PbFriendAvatarInfo();
        }

        public override object Data
        {
            get
            {
                return _record;
            }
            set
            {
                _record = value as PbOfflineRecord;
                Refresh();
            }
        }

        private void Refresh()
        {
            string name = MogoProtoUtils.ParseByteArrToString(_record.captain_name);
            _avatarInfo.dbid = _record.captain_dbid;
            _avatarInfo.name = name;
            _icon.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_record.captain_vocation));
            _txtName.CurrentText.text = name;
            _txtLevel.CurrentText.text = _record.captain_level.ToString();
            _txtCount.CurrentText.text = (1).ToLanguage(_record.task_times);
            _btnAdd.Visible = PlayerDataManager.Instance.FriendData.HasFriend(_record.captain_dbid) == false;
        }

        public override void Dispose()
        {
            _btnAdd.onClick.RemoveListener(OnClickAdd);
            _btnGiveAward.onClick.RemoveListener(OnClickGiveAward);
        }

        public override void Show()
        {
            _btnAdd.onClick.AddListener(OnClickAdd);
            _btnGiveAward.onClick.AddListener(OnClickGiveAward);
            base.Show();
        }

        public override void Hide()
        {
            _btnAdd.onClick.RemoveListener(OnClickAdd);
            _btnGiveAward.onClick.RemoveListener(OnClickGiveAward);
            base.Hide();
        }

        private void OnClickAdd()
        {
            FriendManager.Instance.AddFriend(_record.captain_dbid);
        }

        private void OnClickGiveAward()
        {
            PanelIdEnum.FriendSend.Show(_avatarInfo);
        }

    }
}
