﻿#region 模块信息
/*==========================================
// 文件名：TeamTaskBoxGrid
// 命名空间: GameLogic.GameLogic.Modules.ModuleTask.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/12/22 11:17:14
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTask
{
    public class TeamTaskBoxGrid : RewardItem
    {
        private KButton _btnTips;
        private StateImage _imageHaveGot;

        protected override void Awake()
        {
            base.Awake();
            _imageHaveGot = GetChildComponent<StateImage>("Image_sharedyilingqu");
            _imageHaveGot.SetContinuousDimensionsDirty(true);
            MogoLayoutUtils.SetMiddleCenter(_imageHaveGot.gameObject);
            _btnTips = GetChildComponent<KButton>("Button_tips");

            AddListener();
        }

        public override void Dispose()
        {
            base.Dispose();
            RemoveListener();
        }

        private void AddListener()
        {
            _btnTips.onClick.AddListener(OnShowTips);
        }

        private void RemoveListener()
        {
            _btnTips.onClick.RemoveListener(OnShowTips);
        }

        private void OnShowTips()
        {
            ToolTipsManager.Instance.ShowItemTip(_data.Id, PanelIdEnum.TeamTaskBox);
        }

        public override object Data
        {
            set
            {
                TeamTaskBoxData wrapper = value as TeamTaskBoxData;
                base.Data = wrapper.BaseItemData;
                RefreshBoxState(wrapper.hasGet);
            }
        }

        private void RefreshBoxState(bool hasGet)
        {
            _imageHaveGot.Visible = hasGet;
        }

        public void DoTween()
        {
            TweenScale tween = TweenScale.Begin(_imageHaveGot.gameObject, 0.2f, Vector3.one);
            tween.from = new Vector3(5, 5, 1);
        }
    }
}
