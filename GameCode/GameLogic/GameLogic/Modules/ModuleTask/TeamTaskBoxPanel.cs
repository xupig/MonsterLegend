﻿#region 模块信息
/*==========================================
// 文件名：TeamTaskBoxPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleTask
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/12/22 10:46:40
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCopy;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTask
{
    public class TeamTaskBoxPanel:BasePanel
    {
        private spacetime_rift_times_reward _reward;
        private int _starNum;
        private bool _boxState;

        private KList _list;
        private GameObject _goCanNotGet;
        private KButton _btnCanGet;
        private StateText _textStarNum;

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _goCanNotGet = GetChild("Button_weidacheng");
            _btnCanGet = GetChildComponent<KButton>("Button_lingqu");
            _textStarNum = GetChildComponent<StateText>("Label_txtStarNum");
            InitList();
            base.Awake();
        }

        private void InitList()
        {
            _list = GetChildComponent<KList>("List_content");
            _list.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _list.SetGap(0, 27);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TeamTaskBox; }
        }

        public override void OnShow(object data)
        {
            AddListener();
            _reward = data as spacetime_rift_times_reward;
            _starNum = _reward.__task_times;
            _boxState = PlayerDataManager.Instance.TaskData.team_task_times >= _reward.__task_times && PlayerDataManager.Instance.TaskData.hasReceivedTeamTaskTimeReward.Contains(_reward.__extra_reward_id) == false;
            Refresh();
        }

        public override void OnClose()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnCanGet.onClick.AddListener(OnClick);
            EventDispatcher.AddEventListener(TaskEvent.WEEK_TIMES_CHANGE, OnWeekTimesChange);
            EventDispatcher.AddEventListener(TaskEvent.WEEK_REWARD_CHANGE, OnWeekRewardChange);
        }

        private void RemoveListener()
        {
            _btnCanGet.onClick.RemoveListener(OnClick);
            EventDispatcher.RemoveEventListener(TaskEvent.WEEK_TIMES_CHANGE, OnWeekTimesChange);
            EventDispatcher.RemoveEventListener(TaskEvent.WEEK_REWARD_CHANGE, OnWeekRewardChange);
        }

        private void OnWeekTimesChange()
        {
            _boxState = PlayerDataManager.Instance.TaskData.team_task_times >= _reward.__task_times && PlayerDataManager.Instance.TaskData.hasReceivedTeamTaskTimeReward.Contains(_reward.__extra_reward_id) == false;
            RefreshState();
        }

        private void OnWeekRewardChange()
        {
            _boxState = PlayerDataManager.Instance.TaskData.team_task_times >= _reward.__task_times && PlayerDataManager.Instance.TaskData.hasReceivedTeamTaskTimeReward.Contains(_reward.__extra_reward_id) == false;
            Refresh();
            GetBoxReward();
        }

        private void OnClick()
        {
            TaskManager.Instance.ReceiveTeamTaskTimeReward(_reward.__extra_reward_id);
        }

        private void GetBoxReward()
        {
            List<KList.KListItemBase> itemList = _list.ItemList;
            for (int i = 0; i < itemList.Count; i++)
            {
                TeamTaskBoxGrid rewardGrid = itemList[i] as TeamTaskBoxGrid;
                rewardGrid.DoTween();
            }
        }

        private void Refresh()
        {
            RefreshReward();
            RefreshState();
        }

        private void RefreshState()
        {
            HideAllState();
             _btnCanGet.Visible = _boxState;
            _goCanNotGet.SetActive(!_boxState);
            _textStarNum.CurrentText.text = _starNum.ToString();

        }

        private void HideAllState()
        {
            _btnCanGet.Visible = false;
            _goCanNotGet.SetActive(false);
        }

        //要求策划奖励不随机
        //客户端图标不用刷新
        private void RefreshReward()
        {
            List<TeamTaskBoxData> wrapperList = GetWrapperList();
            _list.RemoveAll();
            _list.SetDataList<TeamTaskBoxGrid>(wrapperList);
            MogoLayoutUtils.SetCenter(_list.gameObject);
        }

        private List<TeamTaskBoxData> GetWrapperList()
        {
            item_reward itemReward = item_reward_helper.GetItemReward(_reward.__extra_reward_id);
             List<BaseItemData> baseItemDataList = item_reward_helper.GetItemDataList(itemReward, PlayerAvatar.Player.vocation);
             List<TeamTaskBoxData> wrapperList = new List<TeamTaskBoxData>();
             bool hasGet = PlayerDataManager.Instance.TaskData.hasReceivedTeamTaskTimeReward.Contains(_reward.__extra_reward_id);
            for (int i = 0; i < baseItemDataList.Count; i++)
            {
                TeamTaskBoxData wrapper = new TeamTaskBoxData(baseItemDataList[i], hasGet);
                wrapperList.Add(wrapper);
            }
            return wrapperList;
        }

    }
}
