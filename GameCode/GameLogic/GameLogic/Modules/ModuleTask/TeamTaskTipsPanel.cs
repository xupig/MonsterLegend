﻿#region 模块信息
/*==========================================
// 文件名：TeamTaskTipsPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleTask
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/12/3 11:00:01
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTask
{
    public class TeamTaskTipsPanel:BasePanel
    {
        private StateText _txtTodayRemainRewardTimes;
        private StateText _txtTodayGetExp;
        private StateText _txtTodayGetCoin;
        private StateText _txtDescription;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_BG/Button_close");
            ModalMask = GetChildComponent<StateImage>("Container_BG/ScaleImage_sharedZhezhao");
            _txtTodayRemainRewardTimes = GetChildComponent<StateText>("Label_txtShouyicishu");
            _txtTodayGetExp = GetChildComponent<StateText>("Label_txtJinbi");
            _txtTodayGetCoin = GetChildComponent<StateText>("Label_txtCishu");
            _txtDescription = GetChildComponent<StateText>("Label_txtShuoming");
            _txtDescription.CurrentText.text = (115011).ToLanguage();
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TeamTaskTip; }
        }

        public override void OnShow(object data)
        {
            SetData(data);
            EventDispatcher.AddEventListener(TaskEvent.DAY_REWARD_CHANGE, OnDayRewardChange);
            EventDispatcher.AddEventListener(TaskEvent.REMAIN_TIMES_CHANGE, OnRemainTimeChange);
        }

        private void OnRemainTimeChange()
        {
            TaskData taskData = PlayerDataManager.Instance.TaskData;
            _txtTodayRemainRewardTimes.CurrentText.text = (115008).ToLanguage(taskData.remain_times);
        }

        public override void SetData(object data)
        {
            OnRemainTimeChange();
            OnDayRewardChange();
            base.SetData(data);
        }

        private void OnDayRewardChange()
        {
            TaskData taskData = PlayerDataManager.Instance.TaskData;
            int exp = 0;
            if (taskData.day_reward_list.ContainsKey(public_config.ITEM_SPECIAL_TYPE_LEVEL_EXP))
            {
                exp = taskData.day_reward_list[public_config.ITEM_SPECIAL_TYPE_LEVEL_EXP];
            }
            int gold = 0;
            if (taskData.day_reward_list.ContainsKey(public_config.ITEM_SPECIAL_TYPE_LEVEL_GOLD))
            {
                gold = taskData.day_reward_list[public_config.ITEM_SPECIAL_TYPE_LEVEL_GOLD];
            }
            _txtTodayGetExp.CurrentText.text = (115009).ToLanguage(exp);
            _txtTodayGetCoin.CurrentText.text = (115010).ToLanguage(gold);
        }

        public override void OnClose()
        {
            EventDispatcher.RemoveEventListener(TaskEvent.DAY_REWARD_CHANGE, OnDayRewardChange);
            EventDispatcher.RemoveEventListener(TaskEvent.REMAIN_TIMES_CHANGE, OnRemainTimeChange);
        }
    }
}
