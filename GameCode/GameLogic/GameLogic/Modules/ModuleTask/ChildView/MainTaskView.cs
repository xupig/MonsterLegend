﻿#region 模块信息
/*==========================================
// 文件名：MainTaskView
// 命名空间: GameLogic.GameLogic.Modules.ModuleTask.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/26 16:15:04
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleTask.Handle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTask.ChildView
{
    public class MainTaskView:KContainer
    {

        private StateText _txtTaskName;
        private StateText _txtTaskDescription;
        private StateText _txtTaskTarget;
        private TaskRewardView _taskRewardView;
        private KContainer _npcIconContainer;
        private KButton _btnGo;
        private KButton _btnReceive;
        private int _taskId;
        private ModelComponent _npcModel;
        private StateImage _imgNpcName;
        private StateText _txtNpcName;
        private KParticle _particle;

        #region 基础接口实现

        protected override void Awake()
        {
            _txtTaskName = GetChildComponent<StateText>("Label_txtzhuxianrenwumingcheng");
            _txtTaskDescription = GetChildComponent<StateText>("Container_Contents01/Label_txtTaskDescription");
            _txtTaskTarget = GetChildComponent<StateText>("Container_Contents02/Label_txtTaskTarget");
            _taskRewardView = KComponentUtil.AddChildComponent<TaskRewardView>(this.gameObject, "Container_Contents03");
            _npcIconContainer = GetChildComponent<KContainer>("Container_icon");
            _npcModel = AddChildComponent<ModelComponent>("Container_icon");
            _npcModel.DragComponent.Dragable = false;
            _btnGo = GetChildComponent<KButton>("Button_qianwang");
            _btnReceive = GetChildComponent<KButton>("Container_lingqu/Button_lingqu");
            _particle = AddChildComponent<KParticle>("Container_lingqu/fx_ui_3_2_lingqu");
            _particle.Stop();
            _imgNpcName = GetChildComponent<StateImage>("Image_NPCmingziditu");
            _txtNpcName = GetChildComponent<StateText>("Label_txtMingcheng");
            _btnGo.gameObject.SetActive(false);
            _btnReceive.gameObject.SetActive(false);
            AddEventListener();
            SetComponentsLayer();
        }

        protected void AddEventListener()
        {
            _btnGo.onClick.AddListener(OnClickGoHandler);
            _btnReceive.onClick.AddListener(OnClickReceiveHandler);
        }

        private void SetComponentsLayer()
        {
            SetComponentLayer(_imgNpcName);
            SetComponentLayer(_txtNpcName);
        }

        private void SetComponentLayer(MonoBehaviour component)
        {
            var originalLocalPosition = component.transform.localPosition;
            component.transform.localPosition = new Vector3(originalLocalPosition.x, originalLocalPosition.y, -2000f);
        }

        protected override void OnDestroy()
        {
            _btnGo.onClick.RemoveListener(OnClickGoHandler);
            _btnReceive.onClick.RemoveListener(OnClickReceiveHandler);
            base.OnDestroy();
        }

        public void UpdateMainTask()
        {
            PbTaskInfo taskInfo = PlayerDataManager.Instance.TaskData.mainTaskInfo;
            if (taskInfo!=null)
            {
                _taskId = taskInfo.task_id;
                task_data taskData = task_data_helper.GetTaskData(taskInfo.task_id);
                if (taskData != null)
                {
                    _txtTaskName.CurrentText.text = MogoLanguageUtil.GetContent(taskData.__task_name);
                    //_txtTaskDescription.CurrentText.text = MogoLanguageUtil.GetContent(taskData.__task_desc);
                    _txtTaskTarget.CurrentText.text = task_data_helper.GetTaskSummary(taskData); 
                    if (taskData.__target != null && taskData.__target.Count > 0)
                    {
                        _btnGo.gameObject.SetActive(taskInfo.status == public_config.TASK_STATE_NEW || taskInfo.status == public_config.TASK_STATE_ACCEPT);
                        _btnReceive.gameObject.SetActive(taskInfo.status == public_config.TASK_STATE_ACCOMPLISH);
                    }
                    else
                    {
                        bool dialogFinish = PlayerDataManager.Instance.TaskData.ContainsDialog(taskData.__id);
                        _btnGo.gameObject.SetActive(dialogFinish == false);
                        _btnReceive.gameObject.SetActive(dialogFinish);
                    }
                    if(_btnReceive.Visible)
                    {
                        _particle.Play(true);
                    }
                    else
                    {
                        _particle.Stop();
                    }
                    int npcId = 0;
                    if (taskInfo.status == public_config.TASK_STATE_ACCOMPLISH)
                    {
                        npcId = taskData.__npc_id_report;
                    }
                    else
                    {
                        npcId = taskData.__npc_id_accept;
                    }
                    if(npcId == 999)
                    {
                        int petId = PlayerDataManager.Instance.PetData.FightingPet != null ? PlayerDataManager.Instance.PetData.FightingPet.Id : 1;
                        _txtNpcName.CurrentText.text = pet_helper.GetName(petId);
                        _npcModel.LoadPetModel(petId, (actor) =>
                        {
                            actor.localEulerAngles = new Vector3(0, 160f, 0);
                        });
                    }
                    else
                    {
                        _npcModel.LoadNPCModel(npcId, (actor) =>
                        {
                            actor.localEulerAngles = new Vector3(0, 160f, 0);
                        });
                        _txtNpcName.CurrentText.text = npc_helper.GetNpcName(npcId);
                    }
                    
                    
                    _taskRewardView.UpdateReward(taskData);
                }
            }
        }

        #endregion

        private void OnClickGoHandler()
        {
             task_data taskData = task_data_helper.GetTaskData(_taskId);
             UIManager.Instance.ClosePanel(PanelIdEnum.Task);
             TaskFollowHandle.TaskFollow(taskData);
        }

        private void OnClickReceiveHandler()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.Task);
            TaskManager.Instance.FinishTask((uint)_taskId);

        }

    }
}
