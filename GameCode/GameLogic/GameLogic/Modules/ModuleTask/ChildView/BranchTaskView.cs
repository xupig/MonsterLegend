﻿#region 模块信息
/*==========================================
// 文件名：BranchTaskView
// 命名空间: GameLogic.GameLogic.Modules.ModuleTask.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/26 16:25:18
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using ModuleTask.ChildComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTask.ChildView
{
    public class BranchTaskView:KContainer
    {
        private KScrollView _scrollView;
        private KList _list;
        protected override void Awake()
        {
            KContainer _container = this.GetComponent<KContainer>();
            _scrollView = _container.GetChildComponent<KScrollView>("ScrollView_zhixianrenwu");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            InitScrollView();
        }

        public void OnShow()
        {
            Visible = true;
            RectTransform rect = (_scrollView.Content.transform as RectTransform);
            _scrollView.ScrollRect.verticalNormalizedPosition = 0;
            rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, 0);
        }

        public void OnClose()
        {
            Visible = false;
        }

        public void InitScrollView()
        {
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _list.SetGap(-5, 0);
        }

        public void UpdateBranchTask()
        {
            List<PbTaskInfo> taskList = new List<PbTaskInfo>();
           
            Dictionary<int,PbTaskInfo> branchDict = PlayerDataManager.Instance.TaskData.branchTaskDic;
            foreach(KeyValuePair<int,PbTaskInfo> kvp in branchDict)
            {
                task_data taskData = task_data_helper.GetTaskData(kvp.Key);
                if(taskData.__task_type != public_config.TASK_TYPE_TEAM)
                {
                    taskList.Add(kvp.Value);
                }
            }
            _list.RemoveAll();
            taskList.Sort(SortFunc);
            if (PlayerDataManager.Instance.TaskData.mainTaskInfo != null)
            {
                taskList.Insert(0,PlayerDataManager.Instance.TaskData.mainTaskInfo);
            }
            _list.SetDataList<BranchTaskItem>(taskList);
            TweenListEntry.Begin(_list.gameObject);
        }

        private int SortFunc(PbTaskInfo task1,PbTaskInfo task2)
        {
            if(task2.status == public_config.TASK_STATE_ACCOMPLISH)
            {
                return 1;
            }
            if (task1.status == public_config.TASK_STATE_ACCOMPLISH)
            {
                return -1;
            }
            return task2.task_id - task1.task_id;
        }
    }
}
