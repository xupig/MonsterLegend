﻿#region 模块信息
/*==========================================
// 文件名：TeamTaskView
// 命名空间: GameLogic.GameLogic.Modules.ModuleTask.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/12/2 9:48:59
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Chat;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTask
{
    public class TeamTaskView:KContainer
    {
        private TeamTaskProgressView _progressView;
        private StateText _txtNoTaskDescription;
        private KContainer _description;
        private StateText _txtName;
        private StateText _txtDescription;
        private KContainer _target;
        private StateText _txtTarget;
        private StateText _txtNoTeam;
        private KScrollView _teamTaskScrollView;
        private KPageableList _teamTaskRewardList;
        private KScrollView _teamCaptainScrollView;
        private KPageableList _teamCaptainRewardList;
        private List<TeamTaskMemberItem> _memberList;
        private KButton _btnInvite;
        private KShrinkableButton _btnCall;
        private KButton _btnQuit;
        private KButton _btnStart;
        private StateText _txtTask;
        private RectTransform _callRect;

        private Vector2 _posCall;
        private Vector2 _orignalPosCall;

        protected override void Awake()
        {
            _progressView = AddChildComponent<TeamTaskProgressView>("Container_wanchengcishu");
            _description = GetChildComponent<KContainer>("Container_renwumiaoshu");
            _txtName = _description.GetChildComponent<StateText>("Label_txtMiaoshu");
            _txtDescription = _description.GetChildComponent<StateText>("Label_txtMiaoshuneirong");
            _target = GetChildComponent<KContainer>("Container_renwumubiao");
            _txtTarget = _target.GetChildComponent<StateText>("Label_txtMubiaoneirong");
            _txtNoTaskDescription = GetChildComponent<StateText>("Label_txtMiaoshuneirong");
            _txtNoTaskDescription.CurrentText.text = (115013).ToLanguage();
            _txtNoTeam = GetChildComponent<StateText>("Container_duiwuqingkuang/Label_txtDangqianweiyou");
            _teamTaskScrollView = GetChildComponent<KScrollView>("Container_duiwujiangli/ScrollView_wupinliebiao02");
            _teamTaskRewardList = _teamTaskScrollView.GetChildComponent<KPageableList>("mask/content");
            _teamCaptainScrollView = GetChildComponent<KScrollView>("Container_duiwujiangli/ScrollView_wupinliebiao01");
            _teamCaptainRewardList = _teamCaptainScrollView.GetChildComponent<KPageableList>("mask/content");

            _memberList = new List<TeamTaskMemberItem>();
            for (int i = 0; i < 4;i++ )
            {
                TeamTaskMemberItem memberItem = AddChildComponent<TeamTaskMemberItem>("Container_duiwuqingkuang/Container_duiwu/Button_duizhang"+i);
                _memberList.Add(memberItem);
            }
            _btnInvite = GetChildComponent<KButton>("Container_bottomBtn/Button_yaoqing");
            _btnCall = GetChildComponent<KShrinkableButton>("Container_bottomBtn/Button_hanren");
            _callRect = _btnCall.GetComponent<RectTransform>();
            _btnQuit = GetChildComponent<KButton>("Container_bottomBtn/Button_tuichu");
            _btnStart = GetChildComponent<KButton>("Container_bottomBtn/Button_tiaozhan");
            _txtTask = _btnStart.GetChildComponent<StateText>("label");
            _orignalPosCall = _callRect.anchoredPosition;
            RectTransform rect = _btnQuit.GetComponent<RectTransform>();
            _posCall = new Vector2((rect.anchoredPosition.x + _orignalPosCall.x)/2,rect.anchoredPosition.y);
            base.Awake();
        }

        public void OnShow()
        {
            Visible = true;
            AddEventListener();
            UpdateContent();
        }

        public void OnHide()
        {
            Visible = false;
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _btnInvite.onClick.AddListener(OnClickInvite);
            _btnCall.onClick.AddListener(OnClickCall);
            _btnQuit.onClick.AddListener(OnClickQuit);
            _btnStart.onClick.AddListener(OnClickStart);
            EventDispatcher.AddEventListener(TaskEvent.RING_CHANGE, OnUpdateReward);
            EventDispatcher.AddEventListener(TaskEvent.TEAM_TASK_STATE_CHANGE, OnUpdateReward);
            EventDispatcher.AddEventListener(TeamEvent.TEAM_REFRESH, OnTeammateChange);
            EventDispatcher.AddEventListener(TeamInstanceEvents.On_Call_People_Succeed, OnCallPeopelSucceed);
        }

        private void RemoveEventListener()
        {
            if(_btnInvite!=null)
            {
                _btnInvite.onClick.RemoveListener(OnClickInvite);
                _btnCall.onClick.RemoveListener(OnClickCall);
                _btnQuit.onClick.RemoveListener(OnClickQuit);
                _btnStart.onClick.RemoveListener(OnClickStart);
            }
            EventDispatcher.RemoveEventListener(TaskEvent.RING_CHANGE, OnUpdateReward);
            EventDispatcher.RemoveEventListener(TaskEvent.TEAM_TASK_STATE_CHANGE, OnUpdateReward);
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_REFRESH, OnTeammateChange);
            EventDispatcher.RemoveEventListener(TeamInstanceEvents.On_Call_People_Succeed, OnCallPeopelSucceed);
        }

        private void OnCallPeopelSucceed()
        {
            MogoUtils.FloatTips(115025);
        }

        public void UpdateContent()
        {
            OnUpdateReward();
           OnTeammateChange();
        }

        private void OnUpdateReward()
        {
            TaskData TaskData = PlayerDataManager.Instance.TaskData;
            spacetime_rift_task_reward reward;

            int rewardType = TaskData.remain_times > 0 ? 1 : 2;
            Debug.Log("" + TaskData.remain_times + "," + TaskData.min_level + "," + TaskData.teamTaskInfo + "," + TaskData.task_id + "," + TaskData.ring);
            if (TaskData.ring == 0)
            {
                _txtTask.ChangeAllStateText((115015).ToLanguage());
                reward = task_data_helper.GetTeamTaskReward(PlayerAvatar.Player.level, 1, rewardType);
                _description.Visible = false;
                _target.Visible = false;
                _txtNoTaskDescription.Visible = true;
                _btnQuit.Visible = false;
                _callRect.anchoredPosition = _posCall;
                _btnCall.RefreshRectTransform();
            }
            else
            {
               
                _btnQuit.Visible = true;
                _callRect.anchoredPosition = _orignalPosCall;
                _btnCall.RefreshRectTransform();
                reward = task_data_helper.GetTeamTaskReward(TaskData.min_level, TaskData.ring, rewardType);
                _description.Visible = true;
                _target.Visible = true;
                _txtNoTaskDescription.Visible = false;
                if (TaskData.teamTaskInfo == null || TaskData.teamTaskInfo.status == public_config.TASK_STATE_NEW)
                {
                    _txtTask.ChangeAllStateText((115016).ToLanguage());
                }
                else if (TaskData.teamTaskInfo.status == public_config.TASK_STATE_ACCOMPLISH)//领奖
                {
                    _txtTask.ChangeAllStateText((115018).ToLanguage());
                }
                else
                {
                    _txtTask.ChangeAllStateText((115017).ToLanguage());
                }
                task_data taskData = task_data_helper.GetTaskData(TaskData.task_id);
                _txtDescription.CurrentText.text = taskData.__task_desc.ToLanguage();
                string taskTargetProgress = string.Empty;
                int total = task_data_helper.getTaskTargetNum(taskData);
                if (TaskData.teamTaskInfo != null && total > 1 && TaskData.teamTaskInfo.target != null && TaskData.teamTaskInfo.target.Count > 0)
                {
                    taskTargetProgress = string.Format("({0}/{1})", TaskData.teamTaskInfo.target[0].value, total);
                }
                _txtName.CurrentText.text = (115027).ToLanguage(taskData.__task_name.ToLanguage(),TaskData.ring);
                _txtTarget.CurrentText.text = string.Format("{0}{1}", task_data_helper.GetTaskSummary(taskData), taskTargetProgress);
            }
            List<RewardData> teamRewardList = item_reward_helper.GetRewardDataList(reward.__task_reward_id, PlayerAvatar.Player.vocation);
            List<RewardData> captainRewardList = item_reward_helper.GetRewardDataList(reward.__captain_reward_id, PlayerAvatar.Player.vocation);
            _teamTaskRewardList.SetDataList<RewardGrid>(teamRewardList);
            _teamCaptainRewardList.SetDataList<RewardGrid>(captainRewardList);
        }

        private void OnTeammateChange()
        {
            for (int i = 0; i < _memberList.Count; i++)
            {
                _memberList[i].Visible = false;
            }
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            if (teamData.TeammateDic.ContainsKey(teamData.CaptainId))
            {
                PbTeamMember captain = teamData.TeammateDic[teamData.CaptainId];
                _memberList[0].Visible = true;
                _memberList[0].UpdateContent(captain);
                int count = 1;
                foreach (PbTeamMember member in teamData.TeammateDic.Values)
                {
                    if (member.dbid != captain.dbid)
                    {
                        _memberList[count].Visible = true;
                        _memberList[count].UpdateContent(member);
                        count++;
                    }
                }
                _txtNoTeam.Visible = false;
            }
            else
            {
                _txtNoTeam.Visible = true;
            }           
        }

        private void OnClickInvite()
        {
            PanelIdEnum.Interactive.Show(new int[] { 2, 2 });//打开邀请玩家标签
        }

        private void OnClickCall()
        {
            if(PlayerDataManager.Instance.TeamData.TeammateDic.Count>=4)
            {
                MogoUtils.FloatTips(83031);
                return;
            }
            TeamInstanceManager.Instance.CallPeople(10000000);
        }


        private void OnClickQuit()
        {
            if (PlayerDataManager.Instance.TaskData.teamTaskInfo!= null)
            {
                if (PlayerDataManager.Instance.TaskData.teamTaskInfo.status == public_config.TASK_STATE_ACCOMPLISH)//已完成
                {
                    MessageBox.Show(true, string.Empty, (115004).ToLanguage(), LeaveAndGetReward, null);//退出队伍并领奖
                }
                else
                {
                    MessageBox.Show(true, string.Empty, (115014).ToLanguage(), LeaveAndGetReward, null);//退出队伍
                }
            }
            else if(PlayerDataManager.Instance.TeamData.CaptainId!=0)
            {
                MessageBox.Show(true, string.Empty, (115014).ToLanguage(), LeaveAndGetReward, null);//退出队伍
            }
        }

        private void LeaveAndGetReward()
        {
            TaskManager.Instance.QuitTeamTask();
        }

        private void OnClickStart()
        {
            if (PlayerDataManager.Instance.TeamData.TeamId == 0)//没有队伍
            {
                MogoUtils.FloatTips(115021);//没有队伍
                return;
            }
            if (PlayerDataManager.Instance.TeamData.CaptainId != PlayerAvatar.Player.dbid)//不是队长
            {
                MogoUtils.FloatTips(115000);
                return;
            }
            if (PlayerDataManager.Instance.TaskData.ring==0)
             {
                 TaskManager.Instance.StartTeamTask();
             }
            else if (PlayerDataManager.Instance.TaskData.teamTaskInfo == null || PlayerDataManager.Instance.TaskData.teamTaskInfo.status == public_config.TASK_STATE_NEW)
             {
                 TaskManager.Instance.ReceiveTeamTask(PlayerDataManager.Instance.TaskData.task_id);
             }
            else if (PlayerDataManager.Instance.TaskData.teamTaskInfo.status == public_config.TASK_STATE_ACCOMPLISH)
             {
                 task_data taskData = task_data_helper.GetTaskData(PlayerDataManager.Instance.TaskData.teamTaskInfo.task_id);
                 EntityNPC npc = NPCManager.GetInstance().FindNPCByNPCID(taskData.__npc_id_report);
                 if (taskData.__npc_id_report == 0 || taskData.__npc_id_report == 999 || (npc != null && npc.IsNearbyPlayer() == true))
                 {
                     TaskManager.Instance.ReceiveTeamTaskReward();
                 }
                 else
                 {
                     EventDispatcher.TriggerEvent(PlayerCommandEvents.FIND_NPC, taskData.__npc_id_report, true);
                 }
                
             }
             else
             {
                //if(PlayerDataManager.Instance.TaskData.isTeammateAllReady == false)
                //{
                //    //还没准备好
                //    MogoUtils.FloatTips(115022);//
                //    return;
                //}
               if(GameSceneManager.GetInstance().curInstanceID == PlayerDataManager.Instance.TaskData.task_place_Id)
               {
                   Vector3 pos = PlayerDataManager.Instance.TaskData.task_place_pos;
                   var dis = Vector3.Distance(PlayerAvatar.Player.position, pos);

                   if (dis < 0.5f)
                   {
                       TaskModule.EnterTeamTaskMission();
                       return;
                   }
                   EventDispatcher.TriggerEvent(PlayerCommandEvents.FIND_POSITION, PlayerDataManager.Instance.TaskData.task_place_pos);
                   LoggerHelper.Info("FIND_POSITION:" + pos.x + "," + pos.y + "," + pos.z);
               }
               else
               {
                   SharePostionData sharedData = new SharePostionData();
                   sharedData.mapId = PlayerDataManager.Instance.TaskData.task_place_Id;
                   sharedData.type = 1;
                   sharedData.targetPosition = PlayerDataManager.Instance.TaskData.task_place_pos;
                   sharedData.mapLine = -1;
                   EventDispatcher.TriggerEvent<SharePostionData>(PlayerCommandEvents.FIND_SHARE_POSITION, sharedData);
                   LoggerHelper.Info("FIND_SHARE_POSITION:" + sharedData.targetPosition.x + "," + sharedData.targetPosition.y + "," + sharedData.targetPosition.z + "," + sharedData.mapId);
               }
               PanelIdEnum.Task.Close();
             }
        }

    }
}
