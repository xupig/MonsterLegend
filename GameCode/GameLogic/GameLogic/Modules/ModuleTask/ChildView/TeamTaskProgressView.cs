﻿#region 模块信息
/*==========================================
// 文件名：TeamTaskRewardView
// 命名空间: GameLogic.GameLogic.Modules.ModuleTask.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/12/2 9:58:12
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTask
{
    public class TeamTaskProgressView: KContainer
    {
        private List<TeamTaskProgressItem> progressItemList;
        private KProgressBar _preogressBar;
        private List<int> progress = new List<int>();
        protected override void Awake()
        {
            base.Awake();
            _preogressBar = GetChildComponent<KProgressBar>("ProgressBar_jindu");
            progressItemList = new List<TeamTaskProgressItem>();
            List<spacetime_rift_times_reward> timeRewardList = task_data_helper.GetTeamTaskTimeRewardList();
            for(int i=0;i<3;i++)
            {
                TeamTaskProgressItem item = AddChildComponent<TeamTaskProgressItem>("Container_xiangzi0" + (i + 1));
                progressItemList.Add(item);
                item.UpdateContent(timeRewardList[i]);
                progress.Add(timeRewardList[i].__task_times);
            }
        }

        protected override void OnEnable()
        {
            UpdateContent();
            EventDispatcher.AddEventListener(TaskEvent.WEEK_TIMES_CHANGE, UpdateContent);
            EventDispatcher.AddEventListener(TaskEvent.WEEK_REWARD_CHANGE, UpdateContent);
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            EventDispatcher.RemoveEventListener(TaskEvent.WEEK_TIMES_CHANGE, UpdateContent);
            EventDispatcher.RemoveEventListener(TaskEvent.WEEK_REWARD_CHANGE, UpdateContent);
            base.OnDisable();
        }

        private void UpdateContent()
        {
            TaskData TaskData = PlayerDataManager.Instance.TaskData;
            if (TaskData.team_task_times <= progress[0])
            {
                _preogressBar.Value = TaskData.team_task_times / (3.0f * progress[0]);
            }
            else if (TaskData.team_task_times <= progress[1])
            {
                _preogressBar.Value = 1.0f / 3.0f + (TaskData.team_task_times - progress[0]) / (3.0f * (progress[1] - progress[0]));
            }
            else
            {
                _preogressBar.Value = Mathf.Min(1, 2.0f / 3.0f + (TaskData.team_task_times - progress[1]) / (3.0f * (progress[2] - progress[1])));
            }
            for (int i = 0; i < 3; i++)
            {
                progressItemList[i].UpdateRewardTimes();
            }
        }

    }
}
