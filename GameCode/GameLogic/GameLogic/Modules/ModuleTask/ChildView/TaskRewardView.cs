﻿#region 模块信息
/*==========================================
// 文件名：TaskRewardView
// 命名空间: GameLogic.GameLogic.Modules.ModuleTask.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/26 16:29:22
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using System.Collections.Generic;
using Common.ExtendComponent;
using Common.Data;
using UnityEngine;

namespace ModuleTask.ChildView
{
    public class TaskRewardView:KContainer
    {

        #region 私有变量
        private KScrollPage _scrollPage;
        private KList _list;
        private List<RewardData> _dataList;
        #endregion

        #region 基础接口实现

        protected override void Awake()
        {
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_renwujiangli");
            _list = _scrollPage.GetChildComponent<KList>("mask/content");
            _dataList = new List<RewardData>();
            _list.SetGap(0, 22);
            _list.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _list.SetPadding(0, 0, 0, 0);
        }

        #endregion

        public void UpdateReward(task_data taskData)
        {
            List<BaseItemData> bseItemDataList = task_data_helper.GetReward(taskData, PlayerAvatar.Player.vocation);
            _dataList.Clear();
            if (bseItemDataList!=null)
            {
                for (int i = 0; i < bseItemDataList.Count; i++)
                {
                    RewardData rewardData = RewardData.GetRewardData();
                    rewardData.id = bseItemDataList[i].Id;
                    rewardData.num = bseItemDataList[i].StackCount;
                    _dataList.Add(rewardData);
                }
            }
            _list.RemoveAll();
            _list.SetDataList<RewardGrid>(_dataList);
        }


        public object List { get; set; }
    }
}
