﻿using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTask
{
    public class GuildTaskPanel : BasePanel
    {
        private CategoryList _categoryList;
        private GuildTaskListView _listView;
        private GuildTaskRewardView _rewardView;
        private GuildTaskRankingView _rankingView;
        private KButton _tipsButton;

        protected override void Awake()
        {
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            _listView = AddChildComponent<GuildTaskListView>("Container_renwuliebiao");
            _rewardView = AddChildComponent<GuildTaskRewardView>("Container_renwujingli");
            _rankingView = AddChildComponent<GuildTaskRankingView>("Container_zongbang");
            _tipsButton = GetChildComponent<KButton>("Button_Gantang");
            InitCategoryList();
        }

        private void InitCategoryList()
        {
            List<CategoryItemData> listToggleData = new List<CategoryItemData>();
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(6018018)));
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(6018019)));
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(123708)));
            _categoryList.RemoveAll();
            _categoryList.SetDataList<CategoryListItem>(listToggleData);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildTask; }
        }

        public override void OnShow(System.Object data)
        {
            _categoryList.SelectedIndex = 0;
            if (data is int)
            {
                int index = (int)data - 1;
                if (index >= 0)
                {
                    _categoryList.SelectedIndex = index;
                }
            }
            OnCategoryListChange(_categoryList, _categoryList.SelectedIndex);
            AddEventListener();
            RefreshGreenPoint();
        }

        public override void OnClose()
        {
            RemoveEventListener();
            PanelIdEnum.GuildTaskTips.Close();
        }

        public override void SetData(object data)
        {
            
        }

        private void RefreshGreenPoint()
        {
            (_categoryList.ItemList[1] as CategoryListItem).SetPoint(GuildTaskManager.Instance.canReward);
        }

        private void OnCategoryListChange(CategoryList arg0, int index)
        {
            if (index == 0)
            {
                _listView.Visible = true;
                _rewardView.Visible = false;
                _rankingView.Visible = false;
            }
            else if(index == 1)
            {
                _listView.Visible = false;
                _rewardView.Visible = true;
                _rankingView.Visible = false;
            }
            else if (index == 2)
            {
                _listView.Visible = false;
                _rewardView.Visible = false;
                _rankingView.Visible = true;
            }
        }

        private void AddEventListener()
        {
            _tipsButton.onClick.AddListener(OnTipsBtnClick);
            _categoryList.onSelectedIndexChanged.AddListener(OnCategoryListChange);
            EventDispatcher.AddEventListener(GuildTaskEvents.ON_GET_GUILD_TASK_SCORE, RefreshGreenPoint);
        }

        private void RemoveEventListener()
        {
            _tipsButton.onClick.RemoveListener(OnTipsBtnClick);
            _categoryList.onSelectedIndexChanged.RemoveListener(OnCategoryListChange);
            EventDispatcher.RemoveEventListener(GuildTaskEvents.ON_GET_GUILD_TASK_SCORE, RefreshGreenPoint);
        }

        private void OnTipsBtnClick()
        {
            string result = string.Empty;
            result = string.Concat(result, MogoLanguageUtil.GetContent(123702));
            UIManager.Instance.ShowPanel(PanelIdEnum.ItemStory, new ItemStoryData(MogoLanguageUtil.GetContent(77), result));
        }


    }
}