﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using ModuleTask.ChildView;
using ModuleTask.Handle;
using MogoEngine.Events;
using MogoEngine.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTask
{
    public class GuildTaskTipsPanel : BasePanel
    {
        private KContainer _titleContainer;
        private IconContainer _icon;
        private List<StateImage> _starList;
        private StateText _nameTxt;
        private StateText _finshTimeTxt;
        private StateText _remainTxt;
        private KButton _acceptBtn;
        private KButton _abandonBtn;
        private KButton _goBtn;
        private KButton _checkBtn;
        //private StateText _descTxt;
        private List<GuildTaskDetailItem> _detailItemList;
        private List<Locater> _locaterList;
        private List<RewardContainer> _rewardItemList;

        private PbGuildTaskInfo _data;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_Bg/Button_close");
            InitTitleContainer();
            InitRewardContainer();
            _acceptBtn = GetChildComponent<KButton>("Button_jiequrenwu");
            _abandonBtn = GetChildComponent<KButton>("Button_fangqirenwu");
            _goBtn = GetChildComponent<KButton>("Button_qianwang");
            _checkBtn = GetChildComponent<KButton>("ScrollView_content/mask/content/Container_item01/Button_jiequrenwu");
            _checkBtn.Visible = false;
            //_descTxt = GetChildComponent<StateText>("Label_txtTishi");
            _detailItemList = new List<GuildTaskDetailItem>();
            _locaterList = new List<Locater>();
            for (int i = 0; i < 4; i++)
            {
                _detailItemList.Add(AddChildComponent<GuildTaskDetailItem>(string.Format("ScrollView_content/mask/content/Container_item0{0}", i + 1)));
                _locaterList.Add(_detailItemList[i].gameObject.AddComponent<Locater>());
                _detailItemList[i].Visible = true;
            }
        }

        private void InitRewardContainer()
        {
            _rewardItemList = new List<RewardContainer>();
            _rewardItemList.Add(AddChildComponent<RewardContainer>("ScrollView_content/mask/content/Container_item02/Container_jiangli/Container_jiangli1"));
            _rewardItemList.Add(AddChildComponent<RewardContainer>("ScrollView_content/mask/content/Container_item02/Container_jiangli/Container_jiangli2"));
        }

        private void InitTitleContainer()
        {
            _titleContainer = GetChildComponent<KContainer>("Container_renwubiaoti");
            _icon = _titleContainer.AddChildComponent<IconContainer>("Container_icon");
            _starList = new List<StateImage>();
            for (int i = 0; i < 5; i++)
            {
                _starList.Add(_titleContainer.GetChildComponent<StateImage>(string.Format("Container_xingxing/Image_xingxing{0}", i)));
            }
            _finshTimeTxt = _titleContainer.GetChildComponent<StateText>("Label_txtJinriyiwancheng");
            _nameTxt = _titleContainer.GetChildComponent<StateText>("Label_txtName");
            _remainTxt = _titleContainer.GetChildComponent<StateText>("Label_txtShengyuCishu");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildTaskTips; }
        }

        public override void OnShow(System.Object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void OnClose()
        {
            RemoveEventListener();
            _data = null;
        }

        public override void SetData(object data)
        {
            _data = data as PbGuildTaskInfo;
            RefreshContent();
        }

        private void RefreshContent()
        {
            RefreshTitleConent();
            //_descTxt.CurrentText.text = MogoLanguageUtil.GetContent(6018046);
            task_data taskData = task_data_helper.GetTaskData(guild_task_helper.GetTaskId(_data.guild_task_id));
            _detailItemList[0].Tilte = MogoLanguageUtil.GetContent(6018041);
            _detailItemList[0].Detail = GetProgressConent(taskData);
            _detailItemList[1].Tilte = MogoLanguageUtil.GetContent(6018042);
            _detailItemList[1].Detail = GetRewarContent();
            _detailItemList[2].Tilte = MogoLanguageUtil.GetContent(6018044);
            _detailItemList[2].Detail = GetPlayerContent(public_config.GTS_ACCOMPLISH);
            _detailItemList[3].Tilte = MogoLanguageUtil.GetContent(6018045);
            _detailItemList[3].Detail = GetPlayerContent(public_config.GTS_ACCEPT);
            RefreshGetView();
            RefreshStatus();
            DoLayoput();
        }

        private static string GetProgressConent(task_data taskData)
        {
            string progressContent = string.Empty;
            if (guild_task_helper.GetCurrentTaskId() == taskData.__id)
            {
                progressContent = task_data_helper.GetTargetProgressConent(taskData.__id, "({0}/{1})");
            }
            return string.Concat(task_data_helper.GetTaskSummary(taskData), progressContent);
        }

        private void RefreshTitleConent()
        {
            task_data taskData = task_data_helper.GetTaskData(guild_task_helper.GetTaskId(_data.guild_task_id));
            _icon.SetIcon(taskData.__task_icon);
            SetStarLevel(guild_task_helper.GetTaskDifficulty(_data.guild_task_id));
            _nameTxt.CurrentText.text = task_data_helper.GetTaskName(taskData);
            _finshTimeTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(123701), ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_TEXT_GOLDEN, guild_task_helper.GetMyFinshTimes(_data).ToString()));
            string remainContent = guild_task_helper.HasTimesLimit(_data.guild_task_id) ? _data.task_num.ToString() : MogoLanguageUtil.GetContent(123721);
            _remainTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(123707), ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_TEXT_GOLDEN, remainContent));
        }

        private void SetStarLevel(int level)
        {
            for (int i = 0; i < _starList.Count; i++)
            {
                _starList[i].Visible = i < level;
            }
        }

        private void DoLayoput()
        {
            for (int i = 0; i < _detailItemList.Count; i++)
            {
                _detailItemList[i].RecalculateSize();
            }
            for (int i = 1; i < _locaterList.Count; i++)
            {
                _locaterList[i].Y = _locaterList[i - 1].Y - _locaterList[i - 1].Height - 12;
            }
        }

        private void RefreshStatus()
        {
            _acceptBtn.Visible = guild_task_helper.CanAccept(_data);
            _abandonBtn.Visible = false;
            _goBtn.Visible = false;
            if (guild_task_helper.GetCurrentTaskId() == guild_task_helper.GetTaskId(_data.guild_task_id))
            {
                _acceptBtn.Visible = false;
                _abandonBtn.Visible = true;
                _goBtn.Visible = true;
            }
        }

        private string GetPlayerContent(int status)
        {
            _data.player_info_list.Sort(SortFun);
            string content = string.Empty;
            int count = 0;
            for (int i = _data.player_info_list.Count - 1; i >= 0; i--)
            {
                if (_data.player_info_list[i].task_status == status)
                {
                    if (count < 2)
                    {
                        string interval = string.IsNullOrEmpty(content) ? "" : MogoLanguageUtil.GetContent(102514);
                        content = string.Concat(content, interval, MogoProtoUtils.ParseByteArrToString(_data.player_info_list[i].avatar_name_b));
                    }
                    count++;
                }
            }
            return count <= 2 ? content : MogoLanguageUtil.GetContent(123706, content, count);
        }

        private int SortFun(PbGuildTaskPlayerInfo x, PbGuildTaskPlayerInfo y)
        {
            return (int)(x.time_stamp - y.time_stamp);
        }

        private string GetRewarContent()
        {
            return string.Format("{0} x{1}", MogoLanguageUtil.GetContent(6018043), guild_task_helper.GetRewardScore(_data.guild_task_id));
        }

        private void RefreshGetView()
        {
            List<RewardData> rewardDataList = new List<RewardData>();
            rewardDataList.Add(RewardData.GetRewardData(public_config.MONEY_TYPE_GUILD_CONTRIBUTION, guild_task_helper.GetRewardContribution(_data.guild_task_id)));
            rewardDataList.Add(RewardData.GetRewardData(public_config.MONEY_TYPE_GUILD_FUND, guild_task_helper.GetRewardFund(_data.guild_task_id)));
            for (int i = 0; i < _rewardItemList.Count; i++)
            {
                if (i < rewardDataList.Count)
                {
                    _rewardItemList[i].Visible = true;
                    _rewardItemList[i].SetData(rewardDataList[i]);
                }
                else
                {
                    _rewardItemList[i].Visible = false;
                }
            }
        }

        private void AddEventListener()
        {
            _acceptBtn.onClick.AddListener(OnAcceptBtnClick);
            _abandonBtn.onClick.AddListener(OnAbandonBtnClick);
            _goBtn.onClick.AddListener(OnGotoBtnClick);
            _checkBtn.onClick.AddListener(OnCheckBtnClick);
        }

        private void RemoveEventListener()
        {
            _acceptBtn.onClick.RemoveListener(OnAcceptBtnClick);
            _abandonBtn.onClick.RemoveListener(OnAbandonBtnClick);
            _goBtn.onClick.RemoveListener(OnGotoBtnClick);
            _checkBtn.onClick.RemoveListener(OnCheckBtnClick);
        }

        private void OnCheckBtnClick()
        {
            //view_helper.OpenView(401);
        }

        private void OnGotoBtnClick()
        {
            task_data taskData = task_data_helper.GetTaskData(guild_task_helper.GetTaskId(_data.guild_task_id));
            UIManager.Instance.ClosePanel(PanelIdEnum.Task);
            TaskFollowHandle.TaskFollow(taskData);
        }

        private void OnAbandonBtnClick()
        {
            GuildTaskManager.Instance.AbandonGuildTask(_data.guild_task_id);
            ClosePanel();
        }

        private void OnAcceptBtnClick()
        {
            if (guild_task_helper.HasGuildTask())
            {
                string content = MogoLanguageUtil.GetContent(6018061);
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, EnsureAcceptTask, () => { }, MogoLanguageUtil.GetContent(6018062), MogoLanguageUtil.GetContent(6018063), null, true);
            }
            else
            {
                EnsureAcceptTask();
            }
        }

        private void EnsureAcceptTask()
        {
            GuildTaskManager.Instance.AcceptGuildTask(_data.guild_task_id);
            ClosePanel();
        }


        private class GuildTaskDetailItem : KContainer
        {
            private StateText _titleTxt;
            private StateText _detailTxt;

            protected override void Awake()
            {
                base.Awake();
                _titleTxt = GetChildComponent<StateText>("Label_txtBiaoti");
                _detailTxt = GetChildComponent<StateText>("Label_txtXinxi");
            }

            public string Tilte
            {
                set
                {
                    _titleTxt.CurrentText.text = value;
                    _titleTxt.RecalculateAllStateHeight();
                }
            }

            public string Detail
            {
                set
                {
                    _detailTxt.CurrentText.text = value;
                    _detailTxt.RecalculateAllStateHeight();
                }
            }

        }
    }

}