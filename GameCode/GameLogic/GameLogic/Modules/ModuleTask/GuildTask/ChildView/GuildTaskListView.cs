﻿using Common.Base;
using Common.ClientConfig;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTask
{
    public class GuildTaskListView : KContainer
    {
        private StateText _noneTxt;
        private KButton _addBtn;
        private KScrollView _scrollView;
        private KList _list;

        protected override void Awake()
        {
            base.Awake();
            _noneTxt = GetChildComponent<StateText>("Container_meiyougonghui/Label_txtMeiyougonghui");
            _addBtn = GetChildComponent<KButton>("Container_meiyougonghui/Button_tianjia");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_youjiangoumai");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.LeftToRight, 2, int.MaxValue);
            _list.SetGap(-22, 0);
        }

        private void RefreshContent()
        {
            if(PlayerAvatar.Player.guild_id == 0)
            {
                _noneTxt.Visible = true;
                _noneTxt.CurrentText.text = MogoLanguageUtil.GetContent(74995);
                _addBtn.Visible = true;
            }
            else
            {
                _noneTxt.Visible = true;
                _noneTxt.CurrentText.text = MogoLanguageUtil.GetContent(2000000);
                _addBtn.Visible = false;
            }
            _scrollView.Visible = false;
            GuildTaskManager.Instance.GetGuildTaskInfo();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            RefreshContent();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _addBtn.onClick.AddListener(OnAddBtnClick);
            _list.onItemClicked.AddListener(OnItemClick);
            EventDispatcher.AddEventListener<PbGuildTaskInfoList>(GuildTaskEvents.ON_GET_GUILD_TASK_LIST, RefreshGuildTaskList);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.guild_id, RefreshContent);
        }

        private void RemoveEventListener()
        {
            _addBtn.onClick.RemoveListener(OnAddBtnClick);
            _list.onItemClicked.RemoveListener(OnItemClick);
            EventDispatcher.RemoveEventListener<PbGuildTaskInfoList>(GuildTaskEvents.ON_GET_GUILD_TASK_LIST, RefreshGuildTaskList);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.guild_id, RefreshContent);
        }

        private void OnAddBtnClick()
        {
            PanelIdEnum.GuildCreatePanel.Show();
        }

        private int _myTaskId;
        private void RefreshGuildTaskList(PbGuildTaskInfoList list)
        {
            if (list.task_list.Count > 0)
            {
                _scrollView.Visible = true;
                _noneTxt.Visible = false;
                _addBtn.Visible = false;
                _myTaskId = guild_task_helper.GetCurrentTaskId();
                list.task_list.Sort(SortFun);
                _list.SetDataList<GuildTaskListItem>(list.task_list, 2);
            }
        }

        private int SortFun(PbGuildTaskInfo x, PbGuildTaskInfo y)
        {
            guild_task xTaskData = guild_task_helper.GetGuildTask(x.guild_task_id);
            guild_task yTaskData = guild_task_helper.GetGuildTask(y.guild_task_id);
            if (xTaskData.__task_id == _myTaskId && yTaskData.__task_id != _myTaskId)
            {
                return -1;
            }
            if (xTaskData.__task_id != _myTaskId && yTaskData.__task_id == _myTaskId)
            {
                return 1;
            }
            if (xTaskData.__difficulty != yTaskData.__difficulty)
            {
                return xTaskData.__difficulty - yTaskData.__difficulty;
            }
            return x.guild_task_id - y.guild_task_id;
        }

        private void OnItemClick(KList arg0, KList.KListItemBase item)
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildTaskTips, (item as GuildTaskListItem).Data);
        }
    }

    public class GuildTaskListItem : KList.KListItemBase
    {
        private IconContainer _icon;
        private List<StateImage> _starList;
        private StateText _nameTxt;
        private StateImage _getImage;
        private StateImage _endImage;
        private StateText _finshTimeTxt;
        private StateText _statusTxt;
        private StateText _progressTxt;

        private PbGuildTaskInfo _data;

        protected override void Awake()
        {
            _icon = AddChildComponent<IconContainer>("Container_icon");
            _starList = new List<StateImage>();
            for (int i = 0; i < 5; i++)
            {
                _starList.Add(GetChildComponent<StateImage>(string.Format("Container_xingxing/Image_xingxing{0}", i)));
            }
            _statusTxt = GetChildComponent<StateText>("Label_txtZhuangtai");
            _progressTxt = GetChildComponent<StateText>("Label_txtJindu");
            _finshTimeTxt = GetChildComponent<StateText>("Label_txtJinriyiwancheng");
            _nameTxt = GetChildComponent<StateText>("Label_txtName");
            _getImage = GetChildComponent<StateImage>("Container_zhuangtai/Image_jinxingzhong");
            _endImage = GetChildComponent<StateImage>("Container_zhuangtai/Image_yizhongjie");
        }

        private void SetStarLevel(int level)
        {
            for (int i = 0; i < _starList.Count; i++)
            {
                _starList[i].Visible = i < level;
            }
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbGuildTaskInfo;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            task_data taskData = task_data_helper.GetTaskData(guild_task_helper.GetTaskId(_data.guild_task_id));
            _icon.SetIcon(taskData.__task_icon);
            SetStarLevel(guild_task_helper.GetTaskDifficulty(_data.guild_task_id));
            _nameTxt.CurrentText.text = task_data_helper.GetTaskName(taskData);
            _finshTimeTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(123701), ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_TEXT_GOLDEN, guild_task_helper.GetMyFinshTimes(_data).ToString()));
            if (guild_task_helper.GetCurrentTaskId() == guild_task_helper.GetTaskId(_data.guild_task_id))
            {
                _statusTxt.CurrentText.text = string.Empty;
                string progessContent = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_TEXT_GOLDEN, task_data_helper.GetTargetProgressConent(guild_task_helper.GetTaskId(_data.guild_task_id)));
                _progressTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(123700), progessContent);
            }
            else
            {
                string statusConent = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_TEXT_GOLDEN, guild_task_helper.CanAccept(_data) ? MogoLanguageUtil.GetContent(6018022) : MogoLanguageUtil.GetContent(6018023));
                _statusTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(6018021), statusConent);
                _progressTxt.CurrentText.text = string.Empty;
            }
            RefreshImageStatus();
        }

        private void RefreshImageStatus()
        {
            if (_endImage != null)
            {
                _endImage.Visible = guild_task_helper.IsEnd(_data);
            }
            if (_getImage != null)
            {
                _getImage.Visible = false;
                if (guild_task_helper.GetCurrentTaskId() == guild_task_helper.GetTaskId(_data.guild_task_id))
                {
                    _getImage.Visible = true;
                }
            }
        }

        public override void Dispose()
        {
            _data = null;
        }
    }
}
