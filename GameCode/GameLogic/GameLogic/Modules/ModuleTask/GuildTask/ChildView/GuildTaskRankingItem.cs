﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Utils;

namespace ModuleTask
{
    public class GuildTaskRankingItem : KList.KListItemBase
    {
        private RankContainer _rankContainer;
        private KContainer _backgroundContainer;
        private StateText _secondColumnTxt;
        private StateText _thirdColumnTxt;
        private StateText _fourthColumnTxt;
        private StateText _fifthColumnTxt;

        private PbGuildTaskPlayerScoreInfo _data;

        protected override void Awake()
        {
            _rankContainer = AddChildComponent<RankContainer>("Container_mingci");
            _backgroundContainer = GetChildComponent<KContainer>("Container_bg");
            _secondColumnTxt = GetChildComponent<StateText>("Label_txtWanjia");
            _thirdColumnTxt = GetChildComponent<StateText>("Label_txtDengji");
            _fourthColumnTxt = GetChildComponent<StateText>("Label_txtZhandouli");
            _fifthColumnTxt = GetChildComponent<StateText>("Label_txtFensi");
        }

        //private void OnItemClicked(KList.KListItemBase target, int index)
        //{
        //    UIManager.Instance.ShowPanel(PanelIdEnum.RankingListDetail, _data.dbid);
        //}

        //private void AddEventListener()
        //{
        //    onClick.AddListener(OnItemClicked);
        //}

        //private void RemoveEventListener()
        //{
        //    onClick.RemoveListener(OnItemClicked);
        //}

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbGuildTaskPlayerScoreInfo;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            RefreshBackground((int)_data.index);
            RefreshRankIcon();
            RefreshItemContent();
        }

        public void RefreshBackground(int rank)
        {
            StateImage bgImage1 = _backgroundContainer.GetChildComponent<StateImage>("ScaleImage_sharedGridBg");
            StateImage bgImage2 = _backgroundContainer.GetChildComponent<StateImage>("ScaleImage_sharedGridBg02");
            bgImage1.gameObject.SetActive(rank % 2 == 1);
            bgImage2.gameObject.SetActive(rank % 2 == 0);
        }

        private void RefreshItemContent()
        {
            _secondColumnTxt.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.name_b);
            _thirdColumnTxt.CurrentText.text = _data.score.ToString();
            _fourthColumnTxt.CurrentText.text = guild_helper.GetGuildPositionContent((int)_data.position);
            _fifthColumnTxt.CurrentText.text = _data.fight.ToString();
        }

        private void RefreshRankIcon()
        {
            _rankContainer.SetRank((int)_data.index);
        }

        public override void Dispose()
        {
            //RemoveEventListener();
            _data = null;
        }
    }
}