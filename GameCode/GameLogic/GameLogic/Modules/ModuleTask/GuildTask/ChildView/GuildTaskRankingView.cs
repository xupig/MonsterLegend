﻿using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using GameData;
using MogoEngine.Events;
using GameLoader.Utils;

namespace ModuleTask
{
    public class GuildTaskRankingView : KContainer
    {
        private GuildTaskRankingListView _listView;
        private GuildTaskRankingAwardView _awardView;

        protected override void Awake()
        {
            base.Awake();
            _listView = AddChildComponent<GuildTaskRankingListView>("Container_paihangbang");
            _awardView = AddChildComponent<GuildTaskRankingAwardView>("Container_jiangli");
        }

        private void AddEventListener()
        {
            _listView.showAwardClick.AddListener(OpenAwardView);
            _awardView.backClick.AddListener(OpenRankView);
        }

        private void RemoveEventListener()
        {
            _listView.showAwardClick.RemoveListener(OpenAwardView);
            _awardView.backClick.RemoveListener(OpenRankView);
        }

        private void OpenAwardView(int rank)
        {
            _awardView.Visible = true;
            _listView.Visible = false;
            _awardView.RefreshRankView(rank);
        }

        private void OpenRankView()
        {
            _listView.Visible = true;
            _awardView.Visible = false;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            OpenRankView();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }
    }
}