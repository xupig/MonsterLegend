﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ExtendComponent.GeneralRankingList;
using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;

namespace ModuleTask
{
    public class GuildTaskRankingAwardItem : KList.KListItemBase
    {
        private GuildTaskRankContaner _rankContainer;
        private RankBackgroundContainer _backgroundContainer;
        private KList _detailList;
        private StateText _hasGainedTxt;
        private StateText _NotGainedTxt;
        private StateText _rankNumTxt;

        private guild_score_rank_reward _data;

        protected override void Awake()
        {
            _rankContainer = AddChildComponent<GuildTaskRankContaner>("Container_mingci");
            _backgroundContainer = AddChildComponent<RankBackgroundContainer>("Container_bg");
            _detailList = GetChildComponent<KList>("List_jiangli");
            _hasGainedTxt = _rankContainer.GetChildComponent<StateText>("Label_txtHuode");
            _NotGainedTxt = _rankContainer.GetChildComponent<StateText>("Label_txtWeiHuode");
            _rankNumTxt = _rankContainer.GetChildComponent<StateText>("Label_txtPaiming");
            _detailList.SetGap(0, 15);
            _hasGainedTxt.Visible = false;
            _NotGainedTxt.Visible = false;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as guild_score_rank_reward;
                SetDataDirty();
            }
        }

        public override void Dispose()
        {
            _data = null;
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            _backgroundContainer.RefreshBackground(_data.__id);
            _rankContainer.SetRank(int.Parse(_data.__rank[0]), int.Parse(_data.__rank[1]));
            _detailList.RemoveAll();
            List<BaseItemData> awardDataList = item_reward_helper.GetItemDataList(_data.__reward);
            _detailList.SetDataList<AwardDetailListItem>(awardDataList, 3);
        }
    }

    public class GuildTaskRankContaner : KContainer
    {
        private StateImage _firstImg;
        private StateImage _secondImg;
        private StateImage _thridImg;
        private StateText _rankingTxt;

        protected override void Awake()
        {
            _firstImg = GetChildComponent<StateImage>("Image_firstLcon");
            _secondImg = GetChildComponent<StateImage>("Image_secondLcon");
            _thridImg = GetChildComponent<StateImage>("Image_thirdLcon");
            _rankingTxt = GetChildComponent<StateText>("Label_txtPaiming");
        }


        public void SetRank(int rankMin, int rankMax)
        {
            if (rankMin == rankMax)
            {
                _firstImg.Visible = rankMin == 1;
                _secondImg.Visible = rankMin == 2;
                _thridImg.Visible = rankMin == 3;
                _rankingTxt.Visible = rankMin >= 4;
                _rankingTxt.CurrentText.text = rankMin.ToString();
            }
            else
            {
                _firstImg.Visible = false;
                _secondImg.Visible = false;
                _thridImg.Visible = false;
                _rankingTxt.Visible = true;
                _rankingTxt.CurrentText.text = string.Format("{0}-{1}", rankMin, rankMax);
            }
        }
    }
}
