﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ExtendComponent.GeneralRankingList;
using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;

namespace ModuleTask
{
    public class GuildTaskRankingAwardView : KContainer
    {
        public KComponentEvent backClick = new KComponentEvent();

        private StateText _descTxt;
        private StateText _myRankTxt;
        private KList _myAwardList;
        private KScrollView _scrollView;
        private KList _list;
        private KButton _backBtn;

        protected override void Awake()
        {
            _descTxt = GetChildComponent<StateText>("Label_txtGengxinshijian");
            _myRankTxt = GetChildComponent<StateText>("Container_wodeshuju/Label_txtWodepaiming");
            _myAwardList = GetChildComponent<KList>("Container_wodeshuju/List_wodejiangpin");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_zhanlibang");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _backBtn = GetChildComponent<KButton>("Button_fanhui");
            InitAwardList();
            _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(123719);
        }

        private void InitAwardList()
        {
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _list.SetGap(0, 5);
            _myAwardList.SetGap(0, 15);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _backBtn.onClick.AddListener(OnBackButtonClicked);
        }

        private void RemoveEventListener()
        {
            _backBtn.onClick.RemoveListener(OnBackButtonClicked);
        }

        private void OnBackButtonClicked()
        {
            backClick.Invoke();
        }

        public void RefreshRankView(int rank)
        {
            _myRankTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(123709), rank == 0 ? MogoLanguageUtil.GetContent(123710) : rank.ToString());
            _myAwardList.RemoveAll();
            if (rank > 0)
            {
                int rewardId = guild_score_rank_reward_helper.GetRewardIdByRank(rank);
                List<BaseItemData> awardDataList = item_reward_helper.GetItemDataList(rewardId);
                _myAwardList.SetDataList<AwardDetailListItem>(awardDataList, 3);
            }
            List<guild_score_rank_reward> rewardList = guild_score_rank_reward_helper.GetScoreRankRewardList();
            _list.SetDataList<GuildTaskRankingAwardItem>(rewardList, 2);
        }


    }
}
