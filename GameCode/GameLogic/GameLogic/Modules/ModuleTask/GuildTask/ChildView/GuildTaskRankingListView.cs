﻿using Common.Data;
using Common.Events;
using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTask
{
    public class GuildTaskRankingListView : KContainer
    {
        private KScrollView _scrollView;
        private KList _list;
        private StateText _rankDescTxt;
        private StateText _noDataTxt;
        private List<StateText> _titleTxtList;
        private StateText _myRankTxt;
        private StateText _myScoreTxt;
        private KButton _showAwardBtn;
        private int _myRank;
        public KComponentEvent<int> showAwardClick = new KComponentEvent<int>();

        private List<PbGuildTaskPlayerScoreInfo> _dataList = new List<PbGuildTaskPlayerScoreInfo>();

        protected override void Awake()
        {
            _scrollView = GetChildComponent<KScrollView>("ScrollView_zhanlibang");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _rankDescTxt = GetChildComponent<StateText>("Label_txtMiaoshu");
            _noDataTxt = GetChildComponent<StateText>("Label_txtmeiyoushuju");
            _showAwardBtn = GetChildComponent<KButton>("Container_wodepaiming/Button_chakanjiangli");
            _myScoreTxt = GetChildComponent<StateText>("Container_wodepaiming/Container_wodeshuju/Label_txtWodepaihangbang");
            _myRankTxt = GetChildComponent<StateText>("Container_wodepaiming/Container_paiming/Label_txtWodepaiming");
            InitRankList();
            InitTitleList();
            InitLabel();
        }

        private void InitLabel()
        {
            _rankDescTxt.CurrentText.text = MogoLanguageUtil.GetContent(123719);
            _showAwardBtn.Label.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
            _showAwardBtn.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(123712));
            _myRankTxt.CurrentText.text = MogoLanguageUtil.GetContent(123709);
            _myScoreTxt.CurrentText.text = MogoLanguageUtil.GetContent(123711);
            _noDataTxt.CurrentText.text = MogoLanguageUtil.GetContent(123718);
        }

        private void InitTitleList()
        {
            _titleTxtList = new List<StateText>();
            for (int i = 0; i < 5; i++)
            {
                _titleTxtList.Add(GetChildComponent<StateText>("Container_txtTitle/Label_text0" + (i + 1)));
                _titleTxtList[i].ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);
            }
            _titleTxtList[0].CurrentText.text = MogoLanguageUtil.GetContent(123713);
            _titleTxtList[1].CurrentText.text = MogoLanguageUtil.GetContent(123714);
            _titleTxtList[2].CurrentText.text = MogoLanguageUtil.GetContent(123715);
            _titleTxtList[3].CurrentText.text = MogoLanguageUtil.GetContent(123716);
            _titleTxtList[4].CurrentText.text = MogoLanguageUtil.GetContent(123717);
        }

        private void InitRankList()
        {
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _list.SetGap(0, 0);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            _myRank = 0;
            OnReRequest(_scrollView.ScrollRect);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _showAwardBtn.onClick.AddListener(OnShowAwardBtnClicked);
            _scrollView.ScrollRect.onReRequest.AddListener(OnReRequest);
            _scrollView.ScrollRect.onRequestNext.AddListener(OnRequestNext);
            EventDispatcher.AddEventListener<PbGuildTaskPlayerScoreInfoList>(GuildTaskEvents.ON_GET_GUILD_TASK_RANK, RefreshListContent);
        }

        private void RemoveEventListener()
        {
            _showAwardBtn.onClick.RemoveListener(OnShowAwardBtnClicked);
            _scrollView.ScrollRect.onReRequest.RemoveListener(OnReRequest);
            _scrollView.ScrollRect.onRequestNext.RemoveListener(OnRequestNext);
            EventDispatcher.RemoveEventListener<PbGuildTaskPlayerScoreInfoList>(GuildTaskEvents.ON_GET_GUILD_TASK_RANK, RefreshListContent);
        }

        private void OnShowAwardBtnClicked()
        {
            showAwardClick.Invoke(_myRank);
        }

        private void OnRequestNext(KScrollRect arg0)
        {
            GuildTaskManager.Instance.GetGuildTaskRank(_dataList.Count + 1, 10);
        }

        private void OnReRequest(KScrollRect arg0)
        {
            _dataList.Clear();
            GuildTaskManager.Instance.GetGuildTaskRank(1, 10);
        }

        private void RefreshListContent(PbGuildTaskPlayerScoreInfoList list)
        {
            _myRank = (int)list.my_index;
            _myRankTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(123709), list.my_index == 0 ? MogoLanguageUtil.GetContent(123710) : list.my_index.ToString());
            _myScoreTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(123711), list.my_score);
            _dataList.AddRange(list.list);
            if (_dataList.Count == 0)
            {
                _noDataTxt.Visible = true;
            }
            else
            {
                _noDataTxt.Visible = false;
            }
            _scrollView.ScrollRect.StopMovement();
            _list.SetDataList<GuildTaskRankingItem>(_dataList, 4);
        }
    }
}
