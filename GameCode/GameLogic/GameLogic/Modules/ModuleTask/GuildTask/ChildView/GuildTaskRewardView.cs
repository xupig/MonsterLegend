﻿using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTask
{
    public class GuildTaskRewardView : KContainer
    {
        private StateText _scoreTxt;
        private StateText _countTxt;
        private KScrollPage _scrollPage;
        private KList _list;

        protected override void Awake()
        {
            base.Awake();
            _scoreTxt = GetChildComponent<StateText>("Label_txtJifen");
            _countTxt = GetChildComponent<StateText>("Label_txtWanchengrenwucishu");
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_scrollpage");
            _list = _scrollPage.GetChildComponent<KList>("mask/content");
            _list.SetGap(0, 11);
            _list.SetPadding(20, 0, 0, 5);
        }

        private void RefreshContent()
        {
            PbGuildTaskScoreInfo scoreInfo = PlayerDataManager.Instance.TaskData.GuildTaskInfo;
            int score = 0;
            int num = 0;
            if (scoreInfo != null)
            {
                score = scoreInfo.task_score;
                num = scoreInfo.task_num;
            }
            _scoreTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(6018027), score);
            _countTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(6018028), num);
            List<guild_score_reward> dataList = guild_score_reward_helper.GetGuildScoreRewardList();
            _list.SetDataList<GuildTaskRewardItem>(dataList, 2);
            int totalPage = Math.Max(1, Mathf.CeilToInt((float)dataList.Count / 4));
            _scrollPage.TotalPage = totalPage;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            RefreshContent();
            AddEventListener();
            GuildTaskManager.Instance.GetMyGuildScore();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(GuildTaskEvents.ON_GET_GUILD_TASK_SCORE, RefreshContent);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(GuildTaskEvents.ON_GET_GUILD_TASK_SCORE, RefreshContent);
        }
    }

    public class GuildTaskRewardItem : KList.KListItemBase
    {
        //private StateText _nameTxt;
        private KContainer _rewardContainer;
        private List<RewardContainer> _rewardItemList;
        private KButton _buyBtn;
        private StateText _conditionTxt;
        private StateText _scoreTxt;
        private KContainer _conditionContainer;

        private guild_score_reward _data;

        protected override void Awake()
        {
            //_nameTxt = GetChildComponent<StateText>("Label_txtName");
            _rewardContainer = GetChildComponent<KContainer>("Container_jiangli");
            _rewardItemList = new List<RewardContainer>();
            _rewardItemList.Add(AddChildComponent<RewardContainer>("Container_jiangli/Container_jiangli1"));
            _rewardItemList.Add(AddChildComponent<RewardContainer>("Container_jiangli/Container_jiangli2"));
            _buyBtn = GetChildComponent<KButton>("Button_goumai");
            _conditionContainer = GetChildComponent<KContainer>("Container_tiaojian");
            _conditionTxt = GetChildComponent<StateText>("Container_tiaojian/Label_txtRenwutiaojian");
            _scoreTxt = GetChildComponent<StateText>("Label_txtName");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as guild_score_reward;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            _buyBtn.Visible = false;
            if (guild_score_reward_helper.IsSatisfy(_data))
            {
                _conditionContainer.Visible = false;
                _buyBtn.Visible = true;
                if (guild_score_reward_helper.IsRewarded(_data))
                {
                    _buyBtn.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(6018056));
                    _buyBtn.SetButtonDisable();
                }
                else
                {
                    _buyBtn.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(6018026));
                    _buyBtn.SetButtonActive();
                }
            }
            else
            {
                _conditionContainer.Visible = true;
                _buyBtn.Visible = false;
                string numStr = string.Empty;
                PbGuildTaskScoreInfo scoreInfo = PlayerDataManager.Instance.TaskData.GuildTaskInfo;
                int num = 0;
                if (scoreInfo != null)
                {
                    num = scoreInfo.task_num;
                }
                if (num < _data.__need_task_num)
                {
                    numStr = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, _data.__need_task_num.ToString());
                }
                else
                {
                    numStr = _data.__need_task_num.ToString();
                }
                _conditionTxt.CurrentText.text = MogoLanguageUtil.GetContent(6018025, numStr);
            }
            RefreshScoreView();
            RefreshGetView();
        }

        private void RefreshScoreView()
        {
            string numStr = string.Empty;
            PbGuildTaskScoreInfo scoreInfo = PlayerDataManager.Instance.TaskData.GuildTaskInfo;
            int score = 0;
            if (scoreInfo != null)
            {
                score = scoreInfo.task_score;
            }
            if (score < _data.__guild_score)
            {
                numStr = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, _data.__guild_score.ToString());
            }
            else
            {
                numStr = _data.__guild_score.ToString();
            }
            _scoreTxt.CurrentText.text = string.Concat(numStr, MogoLanguageUtil.GetContent(6018043));
        }

        private void RefreshGetView()
        {
            List<RewardData> rewardDataList = item_reward_helper.GetRewardDataList(_data.__reward_id, PlayerAvatar.Player.vocation);
            rewardDataList = RewardData.FilterSameItemId(rewardDataList);
            for (int i = 0; i < _rewardItemList.Count; i++)
            { 
                if (i < rewardDataList.Count)
                {
                    _rewardItemList[i].Visible = true;
                    _rewardItemList[i].SetData(rewardDataList[i]);
                }
                else
                {
                    _rewardItemList[i].Visible = false;
                }
            }
            RecalculatePosition();
        }

        private void RecalculatePosition()
        {
            _rewardContainer.RecalculateSize();
            RectTransform rect = _rewardContainer.transform as RectTransform;
            RectTransform parentRect = _rewardContainer.transform.parent as RectTransform;
            rect.anchoredPosition = new Vector2(parentRect.sizeDelta.x / 2 - rect.sizeDelta.x / 2, rect.anchoredPosition.y);
        }

        public override void Dispose()
        {
            _data = null;
        }

        public override void Show()
        {
            base.Show();
            _buyBtn.onClick.AddListener(OnBuyBtnClick);
        }

        public override void Hide()
        {
            base.Hide();
            _buyBtn.onClick.RemoveListener(OnBuyBtnClick);
        }

        private void OnBuyBtnClick()
        {
            GuildTaskManager.Instance.GetGuildScoreReward(_data.__id);
        }
    }

}
