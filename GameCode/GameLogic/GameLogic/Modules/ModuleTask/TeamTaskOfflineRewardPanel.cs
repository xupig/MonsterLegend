﻿#region 模块信息
/*==========================================
// 文件名：TeamTaskOfflineRewardPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleTask
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/12/8 14:17:47
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTask
{
    public class TeamTaskOfflineRewardPanel:BasePanel
    {

        private StateText _txtContent;
        private KScrollView _scrollView;
        private KList _list;
        private string _template;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _txtContent = GetChildComponent<StateText>("Label_txtNr");
            _template = _txtContent.CurrentText.text;
            _scrollView = GetChildComponent<KScrollView>("ScrollView_zhuangbei");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TeamTaskOfflineReward; }
        }

        public override void OnShow(object data)
        {
            SetData(data);
        }

        public override void SetData(object data)
        {
            TaskData taskData = PlayerDataManager.Instance.TaskData;
             uint exp = 0;
             uint gold = 0;
            if(taskData.offlineReward!=null)
            {
                for(int i=0;i<taskData.offlineReward.Count;i++)
                {
                    if (taskData.offlineReward[i].item_id == public_config.ITEM_SPECIAL_TYPE_LEVEL_EXP)
                    {
                        exp += taskData.offlineReward[i].item_num;
                    }
                    else if (taskData.offlineReward[i].item_id == public_config.ITEM_SPECIAL_TYPE_LEVEL_GOLD)
                    {
                         gold = taskData.offlineReward[i].item_num;
                    }
                }
            }
            _txtContent.CurrentText.text = string.Format(_template, taskData.offlineTimes, exp, gold);
            _list.RemoveAll();
            _list.SetDataList<TeamTaskOfflineRewardItem>(PlayerDataManager.Instance.TaskData.offlineRecord);
            base.SetData(data);
        }

        public override void OnClose()
        {
            
        }


    }
}
