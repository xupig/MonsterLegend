﻿#region 模块信息
/*==========================================
// 文件名：NpcDialogViewcs
// 命名空间: GameLogic.GameLogic.Modules.ModuleTask
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/22 15:13:12
// 描述说明：
// 其他：
//==========================================*/
#endregion

using ACTSystem;
using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleTask.Handle;
using Mogo.Util;
using MogoEngine;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleTask
{
    public class NpcDialogPanel:BasePanel
    {

        private KContainer _myContainer;
        private KContainer _npcContainer;

        private StateText _txtMyName;
        private StateText _txtNpcName;

        private KContainer _imageMyName;
        private KContainer _imageNpcName;

        private StateText _txtMyContent;
        private StateText _txtNpcContent;

        private KButton _btnClose;
        private KButton _btnMyNext;
        private KButton _btnNpcNext;
        private KButton _btnSkip;

        private int direction = 1;
        private int moveCount = 10;

        private ClickableComponent _btnMyTxt;
        private ClickableComponent _btnNpcTxt;

        private ModelComponent _selfModel;
        private ModelComponent _npcModel;

        private int _curNpcId = 0;

        private PbTaskInfo _taskInfo;
       
       // private int _status = 0;

        private static Dictionary<int, Dictionary<int, int>> _taskStepDict = new Dictionary<int, Dictionary<int, int>>();

        private DialogComponent _dialogComponent;

        public static void OnZeroClockResp()
        {
            _taskStepDict.Clear();
        }

        private int CurTaskId
        {
            get 
            {
                if(_taskInfo == null)
                {
                    return 0;
                }
                else
                {
                    return _taskInfo.task_id; 
                }
            }
        }

        private static string ME = "0";
        private static string PET = "999";


        #region 基础接口实现

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("Container_bottomLeft/Button_close/image");
            _myContainer = GetChildComponent<KContainer>("Container_bottomLeft/Container_my");
            _npcContainer = GetChildComponent<KContainer>("Container_bottomLeft/Container_npc");
            _txtMyName = _myContainer.GetChildComponent<StateText>("Label_txtMingcheng");
            _txtNpcName = _npcContainer.GetChildComponent<StateText>("Label_txtMingcheng");
            _imageMyName = _myContainer.GetChildComponent<KContainer>("Container_NPCmingziditu");
            _imageNpcName = _npcContainer.GetChildComponent<KContainer>("Container_NPCmingziditu");
            _txtMyContent = _myContainer.GetChildComponent<StateText>("Label_txtxiangximiaoshu");
            _txtNpcContent = _npcContainer.GetChildComponent<StateText>("Label_txtxiangximiaoshu");
            _btnMyTxt = _txtMyContent.gameObject.AddComponent<ClickableComponent>();
            _btnNpcTxt = _txtNpcContent.gameObject.AddComponent<ClickableComponent>();
            _btnMyNext = GetChildComponent<KButton>("Container_bottomLeft/Button_xiangxiamy");
            _btnNpcNext = GetChildComponent<KButton>("Container_bottomLeft/Button_xiangxianpc");
            _btnClose = GetChildComponent<KButton>("Container_bottomLeft/Button_close");
            _btnSkip = GetChildComponent<KButton>("Container_topRight/Button_btn3");

            _selfModel = _myContainer.AddChildComponent<ModelComponent>("Container_icon");
            _npcModel = _npcContainer.AddChildComponent<ModelComponent>("Container_icon");
            _selfModel.DragComponent.Dragable = false;
            _npcModel.DragComponent.Dragable = false;
            LoadSelfModel();
            SetComponentsLayer();
            _dialogComponent = gameObject.AddComponent<DialogComponent>();
        }


        public void Update()
        {
            _btnMyNext.transform.localPosition += new Vector3(0, direction, 0);
            _btnNpcNext.transform.localPosition += new Vector3(0, direction, 0);
            moveCount--;
            if(moveCount == 0)
            {
                moveCount = 10;
                direction = 0 - direction;
            }
        }

        private void SetComponentsLayer()
        {
            SetComponentLayer(_selfModel);
            SetComponentLayer(_npcModel);
        }

        private void SetComponentLayer(MonoBehaviour component)
        {
            var originalLocalPosition = component.transform.localPosition;
            component.transform.localPosition = new Vector3(originalLocalPosition.x, originalLocalPosition.y, 2000f);
        }

        private void LoadSelfModel()
        {
            _selfModel.LoadPlayerModel(OnLoadAvatarModelFinished);
        }

        private void OnLoadAvatarModelFinished(ACTActor actor)
        {
            actor.localEulerAngles = new Vector3(0, 200f, 0);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.NpcDailog; }
        }

        public override void OnShow(System.Object data)
        {
            SetData(data);
            AddEventListener();
        }

        public override void SetData(object data)
        {
            UIManager.Instance.HideOtherLayer(MogoUILayer.LayerUIPanel);
            HideBtnSkip();
            if (data != null)
            {
                if (data is int)
                {
                    _curNpcId = (int)data;
                    OnTaskFinishHandler();
                }
                else if (data is int[])
                {
                    int[] dataList = data as int[];
                    _curNpcId = dataList[0];
                    UpdateNpcDialog(dataList[1]);
                }
            }
            else
            {
                OnNpcDialogChange();
            }
            base.SetData(data);
        }

        private void AddEventListener()
        {
            _btnClose.onClick.AddListener(OnNextStep);
            _btnNpcNext.onClick.AddListener(OnNextStep);
            _btnMyNext.onClick.AddListener(OnNextStep);
            _btnMyTxt.onClick.AddListener(OnNextStep);
            _btnNpcTxt.onClick.AddListener(OnNextStep);
            //EventDispatcher.AddEventListener<int>(TaskEvent.TASK_ACCEPTED, OnTaskAcceptedHandler);
           // EventDispatcher.AddEventListener<int>(TaskEvent.TASK_FINISH, OnTaskFinishHandler);
        }

        private void RemoveEventListener()
        {
            _btnClose.onClick.RemoveListener(OnNextStep);
            _btnNpcNext.onClick.RemoveListener(OnNextStep);
            _btnMyNext.onClick.RemoveListener(OnNextStep);
            _btnMyTxt.onClick.RemoveListener(OnNextStep);
            _btnNpcTxt.onClick.RemoveListener(OnNextStep);
            //EventDispatcher.RemoveEventListener<int>(TaskEvent.TASK_ACCEPTED, OnTaskAcceptedHandler);
        }

        

        public override void OnClose()
        {
            UIManager.Instance.ShowOtherLayer(MogoUILayer.LayerUIPanel);
            RemoveEventListener();
            if(_taskInfo!=null)
            {
                task_data data = task_data_helper.GetTaskData(_taskInfo.task_id);
                if(data.__task_type == public_config.TASK_TYPE_MAIN)
                {
                    TaskManager.Instance.ContinueAutoTask();
                }
            }
            _taskInfo = null;
            _curNpcId = 0;
           
        }

        #endregion

        #region UI事件回调处理

        private void HideBtnSkip()
        {
            _btnSkip.Visible = false;
        }

        private void OnNextStep(PointerEventData data)
        {
            OnNextStep();
        }

        private void OnNextStep()
        {
            if(_dialogComponent.IsPaused())
            {
                _dialogComponent.ContinuePlay();
            }
            else
            {
                _dialogComponent.Clear();
                AddTaskInfoStep();
                DoNextDialog();
            }
           
        }


        public void OnTaskFinishHandler()
        {
            if (CheckNpcNextTask())
            {
                return;
            }
           // GoToNextTaskDialog(taskId);
        }

        private bool CheckNpcNextTask()
        {
            int taskId = NpcTaskHandle.GetNpcTask(_curNpcId);
            _taskInfo = PlayerDataManager.Instance.TaskData.GetTaskInfo(taskId);
            if (_taskInfo != null)
            {
                InitTaskInfoStep(_taskInfo);
                DoNextDialog();
                return true;
            }
            return false;
        }

        private void DoNextDialog()
        {
            if(_taskInfo!=null)
            {
                if (Status == public_config.TASK_STATE_NEW)//可接
                {
                    Dictionary<string, string[]> data = XMLManager.task_data[CurTaskId].__npc_chat_accept;
                    DoUpdateDialog(data);

                }
                else if (Status == public_config.TASK_STATE_ACCOMPLISH)//可完成
                {
                    Dictionary<string, string[]> data = XMLManager.task_data[CurTaskId].__npc_chat_report;
                    DoUpdateDialog(data);
                }
                else
                {
                    ClosePanel();
                }
            }
            else
            {
                ClosePanel();
            }
        }

        private void DoUpdateDialog(Dictionary<string, string[]> dialogDic)
        {
            string key = Step.ToString();
            if (dialogDic!=null && dialogDic.ContainsKey(key))//还有下一步对话
            {
                string[] dialog = dialogDic[key];
                UpdateDialogContent(dialog);
            }
            else//对话到结尾
            {

                DialogComplete();
            }
        }

        private void DialogComplete()
        {
            if (Status == public_config.TASK_STATE_NEW)
            {
                TaskManager.Instance.AcceptTask((uint)CurTaskId);
                ClosePanel();
            }
            else if (Status == public_config.TASK_STATE_ACCOMPLISH)
            {
                PlayerDataManager.Instance.TaskData.AddDialog(CurTaskId);
                //TaskManager.Instance.FinishTask((uint)(CurTaskId));
                task_data taskData = task_data_helper.GetTaskData(CurTaskId);
                _taskInfo = null;
                if (taskData.__task_type == public_config.TASK_TYPE_TEAM)
                {
                    PanelIdEnum.Task.Show(2);
                }
                else
                {
                    PanelIdEnum.Task.Show();
                }
                ClosePanel();
            }
            else
            {
                ClosePanel();
            }
        }

        private void UpdateDialogContent(string[] dialog)
        {
            int contentId = 0;
            if(dialog[0] == ME)//自己的对话
            {
                _myContainer.gameObject.SetActive(true);
                _npcContainer.gameObject.SetActive(false);
                _btnMyNext.gameObject.SetActive(true);
                _btnNpcNext.gameObject.SetActive(false);
                _txtMyName.CurrentText.text = (MogoWorld.Player as PlayerAvatar).name;
                if (dialog.Length >= 2) contentId = int.Parse(dialog[1]);
                _dialogComponent.SetTxtContent(_txtMyContent, GetContent(contentId));
            }
            else//NPC
            {
                _npcContainer.gameObject.SetActive(true);
                _myContainer.gameObject.SetActive(false);
                _btnMyNext.gameObject.SetActive(false);
                _btnNpcNext.gameObject.SetActive(true);
                if(dialog[0] == PET)
                {
                    int petId = PlayerDataManager.Instance.PetData.FightingPet != null ? PlayerDataManager.Instance.PetData.FightingPet.Id : 1;
                    _txtNpcName.CurrentText.text = pet_helper.GetName(petId);
                    _npcModel.LoadPetModel(petId, (actor) =>
                    {
                        actor.localEulerAngles = new Vector3(0, 160f, 0);
                    });
                }
                else
                {
                    npc _npc = XMLManager.npc[int.Parse(dialog[0])];
                    _txtNpcName.CurrentText.text = MogoLanguageUtil.GetContent(_npc.__name);
                    
                    _npcModel.LoadNPCModel(_npc.__id, (actor) =>
                    {
                        actor.localEulerAngles = new Vector3(0, 160f, 0);
                    });
                    PlayRandomSound(_npc.__id);
                }
                if (dialog.Length >= 2) contentId = int.Parse(dialog[1]);
                _dialogComponent.SetTxtContent(_txtNpcContent, GetContent(contentId));
            }
            if (dialog.Length >= 3 && dialog[2] != null && dialog[2] != string.Empty)
            {
                PlaySound(int.Parse(dialog[2]));
            }
        }

        private void PlayRandomSound(int npcID)
        {
            int soundID = npc_helper.GetRandomNPCVoice(npcID);
            if (soundID != -1)
            {
                SoundInfoManager.Instance.PlayClickVoice(soundID);
            }
        }

        private void PlaySound(int soundId)
        {
            SoundInfoManager.Instance.PlaySound(soundId);
        }

        private string GetContent(int dialogId)
        {
            return SystemInfoUtil.GenerateContent(MogoLanguageUtil.GetContent(dialogId), PlayerAvatar.Player.name);
        }

        public void OnNpcDialogChange()
        {
            _curNpcId = TaskModule.npcId;
            int taskId = NpcTaskHandle.GetNpcTask(_curNpcId);
            UpdateNpcDialog(taskId);
        }

        private void InitTaskInfoStep(PbTaskInfo taskInfo)
        {
           
            if (_taskStepDict.ContainsKey(taskInfo.task_id) == false)
            {
                _taskStepDict.Add(taskInfo.task_id, new Dictionary<int, int>());
            }
            if (_taskStepDict[taskInfo.task_id].ContainsKey(taskInfo.status) == false)
            {
                _taskStepDict[taskInfo.task_id].Add(taskInfo.status, 1);
            }
        }

        private void AddTaskInfoStep()
        {
            if (_taskInfo!=null && _taskStepDict.ContainsKey(_taskInfo.task_id) && _taskStepDict[_taskInfo.task_id].ContainsKey(_taskInfo.status))
            {
                _taskStepDict[_taskInfo.task_id][_taskInfo.status] += 1;
            }
        }

        private int Step
        {
            get 
            {
                if(_taskInfo == null)
                {
                    return 1;
                }
                if (_taskStepDict.ContainsKey(_taskInfo.task_id) == false)
                {
                    _taskStepDict.Add(_taskInfo.task_id, new Dictionary<int, int>());
                }
                if (_taskStepDict[_taskInfo.task_id].ContainsKey(_taskInfo.status) == false)
                {
                    _taskStepDict[_taskInfo.task_id].Add(_taskInfo.status, 1);
                }
                return _taskStepDict[_taskInfo.task_id][_taskInfo.status];
            }
        }

        public int Status
        {
            get
            {
                if (_taskInfo == null)
                {
                    return public_config.TASK_STATE_NEW;
                }
                return _taskInfo.status;
            }
        }
        
        private void UpdateNpcDialog(int taskId)
        {
            _taskInfo = PlayerDataManager.Instance.TaskData.GetTaskInfo(taskId);
            //Debug.Log("UpdateNpcDialog:" + taskId + "," + _taskInfo+","+Step);
            if (_taskInfo != null)
            {
                InitTaskInfoStep(_taskInfo);
                DoNextDialog();
            }
            else
            {
                ShowNpcDefaultDialog();
            }
        }


        private void ShowNpcDefaultDialog()
        {
            npc _npc = XMLManager.npc[_curNpcId];
            _myContainer.gameObject.SetActive(false);
            _npcContainer.gameObject.SetActive(true);
            _btnMyNext.Visible = false;
            _btnNpcNext.Visible = true;
            _txtNpcName.CurrentText.text = MogoLanguageUtil.GetContent(_npc.__name);
            //_txtNpcContent.CurrentText.text = SystemInfoUtil.GenerateContent(MogoLanguageUtil.GetContent(_npc.__dialog_txt),PlayerAvatar.Player.name);
            if (_curNpcId.ToString() == PET)
            {
                int petId = PlayerDataManager.Instance.PetData.FightingPet != null ? PlayerDataManager.Instance.PetData.FightingPet.Id : 1;
                _txtNpcName.CurrentText.text = pet_helper.GetName(petId);
                _npcModel.LoadPetModel(petId, (actor) =>
                {
                    actor.localEulerAngles = new Vector3(0, 160f, 0);
                });
            }
            else
            {
                _txtNpcName.CurrentText.text = MogoLanguageUtil.GetContent(_npc.__name);

                _npcModel.LoadNPCModel(_npc.__id, (actor) =>
                {
                    actor.localEulerAngles = new Vector3(0, 160f, 0);
                });
            }
           // _dialogComponent.SetTxtContent(_txtNpcContent, SystemInfoUtil.GenerateContent((102545).ToLanguage() + (102545).ToLanguage(), PlayerAvatar.Player.name));
            _dialogComponent.SetTxtContent(_txtNpcContent, SystemInfoUtil.GenerateContent(MogoLanguageUtil.GetContent(npc_helper.GetRandomDialogID(_curNpcId)), PlayerAvatar.Player.name));
        }

        #endregion
    }
}
