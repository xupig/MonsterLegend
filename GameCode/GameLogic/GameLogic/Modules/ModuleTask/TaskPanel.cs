﻿#region 模块信息
/*==========================================
// 文件名：TaskView
// 命名空间: GameLogic.GameLogic.Modules.ModuleTask
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/20 20:49:00
// 描述说明：
// 其他：
//==========================================*/
#endregion


using Common.Base;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleTask.ChildView;
using ModuleTask.Handle;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;
namespace ModuleTask
{

   
    public class TaskPanel:BasePanel
    {
        public const int TEAM_TASK = 2;

        private KButton _btnQuestion;
        private KToggleGroup _toggleGroup;
        //private MainTaskView _mainTaskView;
        private BranchTaskView _branchTaskView;
        //private TeamTaskView _teamTaskView;
        private FieldTaskView _fieldTaskView;
        //private StateText _txtNewBranchTask;
        //private KContainer _tishi;

        protected override void Awake()
        {
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            //_txtNewBranchTask = GetChildComponent<StateText>("Container_tishi/Label_txtNoticeCount");
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close02");
            _btnQuestion = GetChildComponent<KButton>("Container_panelBg/Button_wenhao");
           // _mainTaskView = AddChildComponent<MainTaskView>("Container_zhuxianrenwu");
            _branchTaskView = AddChildComponent<BranchTaskView>( "Container_zhixianrenwu");
            //_teamTaskView = AddChildComponent<TeamTaskView>("Container_shikongliexi");
            _fieldTaskView = AddChildComponent<FieldTaskView>("Container_yewairenwu");
            //_tishi = GetChildComponent<KContainer>("Container_tishi");
            //_txtNewBranchTask.CurrentText.text = "0";

            SetTabList((int)FunctionId.task, _toggleGroup);
        }

        private void RefreshWildTaskGreenPoint()
        {
            _toggleGroup[3].SetGreenPoint(wild_task_helper.HasWildTask() == false);
        }

        private void RefreshGuildTaskGreendPoint()
        {
            _toggleGroup[2].SetGreenPoint(GuildTaskManager.Instance.canReward);
        }

        public override void OnShow(System.Object data)
        {
            TaskManager.Instance.IdleAutoTask();
            EventDispatcher.AddEventListener<bool>(TaskEvent.TASK_MAIN_TASK_CHANGE, OnTaskChange);
            EventDispatcher.AddEventListener<bool>(TaskEvent.TASK_BRANCH_TASK_CHANGE, OnTaskChange);
            EventDispatcher.AddEventListener(GuildTaskEvents.ON_GET_GUILD_TASK_SCORE, RefreshGuildTaskGreendPoint);
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            _btnQuestion.onClick.AddListener(OnClickQuestion);
            RefreshWildTaskGreenPoint();
            RefreshGuildTaskGreendPoint();
            SetData(data);
        }

        public override void SetData(object data)
        {
            int tabIndex = 0;
            int categoryIndex = 0;
            if (data != null)
            {
                if (data is int[])
                {
                    tabIndex = (data as int[])[0] - 1;
                    categoryIndex = (data as int[])[1];
                }
                else if (data is int)
                {
                    tabIndex = (int)data - 1;
                }
            }
            _toggleGroup.SelectIndex = tabIndex;
            SetSelectedIndex(tabIndex, categoryIndex);
        }

        public override void OnClose()
        {
            EventDispatcher.RemoveEventListener<bool>(TaskEvent.TASK_MAIN_TASK_CHANGE, OnTaskChange);
            EventDispatcher.RemoveEventListener<bool>(TaskEvent.TASK_BRANCH_TASK_CHANGE, OnTaskChange);
            EventDispatcher.RemoveEventListener(GuildTaskEvents.ON_GET_GUILD_TASK_SCORE, RefreshGuildTaskGreendPoint);
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            _btnQuestion.onClick.RemoveListener(OnClickQuestion);
            PanelIdEnum.GuildTask.Close();
            //TaskManager.Instance.StopAutoTask();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Task; }
        }

        public void OnTaskChange(bool isProgressChange)
        {
            if(isProgressChange == false)
            {
                return;
            }
            if(_branchTaskView.Visible)
            {
                _branchTaskView.UpdateBranchTask();
            }
            else
            {
                //_teamTaskView.UpdateContent();
            }
        }

        private void OnSelectedIndexChanged(KToggleGroup group,int index)
        {
            SetSelectedIndex(index);
        }

        private void SetSelectedIndex(int tabIndex, int categoryIndex = 0)
        {
            //_teamTaskView.OnHide();
            _branchTaskView.OnClose();
            _btnQuestion.Visible = false;
            _fieldTaskView.Visible = false;
            PanelIdEnum.GuildTask.Close();
            if (tabIndex == 0)
            {
                _branchTaskView.OnShow();
                _branchTaskView.UpdateBranchTask();
            }
            else if (tabIndex == 1)
            {
                _btnQuestion.Visible = true;
                //_teamTaskView.OnShow();
            }
            else if (tabIndex == 2)
            {
                PanelIdEnum.GuildTask.Show(categoryIndex);
            }
            else if (tabIndex == 3)
            {
                _fieldTaskView.Visible = true;
            }
        }

        private void OnClickQuestion()
        {
            PanelIdEnum.TeamTaskTip.Show();
        }

    }
}