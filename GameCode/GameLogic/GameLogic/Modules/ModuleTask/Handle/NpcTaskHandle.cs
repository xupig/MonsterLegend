﻿#region 模块信息
/*==========================================
// 文件名：NpcTaskHandle
// 命名空间: ModuleTask.Handle
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/21 10:18:18
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.ServerConfig;
using GameData;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTask.Handle
{
    /// <summary>
    /// NPC身上任务列表
    /// </summary>
    public class NpcTaskHandle
    {
        private static Dictionary<int, Dictionary<int, int>> npcTaskDic = new Dictionary<int, Dictionary<int, int>>();//NPC身上任务列表

        /// <summary>
        /// NPC当前优先级最高的任务
        /// </summary>
        /// <param name="npcId"></param>
        /// <returns></returns>
        public static int GetNpcTask(int npcId)
        {
            if(npcTaskDic.ContainsKey(npcId))
            {
                Dictionary<int, int> taskDic = npcTaskDic[npcId];
                int taskState = 0;
                int taskId = 0;
                foreach(KeyValuePair<int,int> kvp in taskDic)
                {
                    if(kvp.Value > taskState)
                    {
                        taskId = kvp.Key;
                        taskState = kvp.Value;
                    }
                }
                return taskId;
            }
            else
            {
                return 0;
            }
        }

        private static void NpcTaskStateChange(int npcId)
        {
            int taskState = 0;
            int taskId = 0;
            if (npcTaskDic.ContainsKey(npcId))
            {
                Dictionary<int, int> taskDic = npcTaskDic[npcId];
                foreach (KeyValuePair<int, int> kvp in taskDic)
                {
                    if (kvp.Value > taskState)
                    {
                        taskState = kvp.Value;
                        taskId = kvp.Key;
                    }
                }
            }
            EventDispatcher.TriggerEvent<int,int, int>(TaskEvent.NPC_TASK_STATE_CHANGE, npcId,task_data_helper.GetTaskType(taskId), taskState);
        }

        public static void OnZeroClockResp()
        {
            foreach(KeyValuePair<int,Dictionary<int,int>> kvp in npcTaskDic)
            {
                List<int> removeList = new List<int>();
                foreach(KeyValuePair<int,int> kk in kvp.Value)
                {
                    task_data data = task_data_helper.GetTaskData(kk.Key);
                    if(data.__task_type == public_config.TASK_TYPE_GUILD)
                    {
                        removeList.Add(kk.Key);
                    }
                }
                for(int i=0;i<removeList.Count;i++)
                {
                    kvp.Value.Remove(removeList[i]);
                }
            }
        }

        public static void RemoveTaskFromNpc(int npcId, int taskId)
        {
            if (npcTaskDic.ContainsKey(npcId))
            {
                if (npcTaskDic[npcId].ContainsKey(taskId))
                {
                    npcTaskDic[npcId].Remove(taskId);
                }
            }
            NpcTaskStateChange(npcId);
        }

        public static void AddTaskToNpc(int npcId, int tasdkId, int state)
        {
            if (npcTaskDic.ContainsKey(npcId) == false)
            {
                npcTaskDic.Add(npcId, new Dictionary<int, int>());
            }
            if (npcTaskDic[npcId].ContainsKey(tasdkId) == false)
            {
                npcTaskDic[npcId].Add(tasdkId, state);
            }
            else
            {
                npcTaskDic[npcId][tasdkId] = state;
            }
            NpcTaskStateChange(npcId);
        }
    }
}
