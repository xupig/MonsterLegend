﻿#region 模块信息
/*==========================================
// 文件名：TaskParser
// 命名空间: GameLogic.GameLogic.Modules.ModuleTask.Handle
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/2/9 11:17:22
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Global;
using GameData;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using GameMain.GlobalManager.SubSystem;
using UnityEngine;

namespace ModuleTask.Handle
{
    public class TaskParser
    {
        private static Regex s_regNum = new Regex(@"\{(\d+),([a-zA-Z0-9]+)\}");   //"{1,Num}"

        private const string InstId = "instid";
        private const string NpcId = "npcid";
        private const string ItemId = "itemid";
        private const string Loop = "loop";//多少次
        private const string MonsterId = "monsterid";
        public const string Level = "level";
        private const string SkillId = "skillid";
        private const string HpRate = "hprate";


        public static string ParseAllNumMatch(string _desc, int taskId)
        {
            MatchCollection _matchCollection = Regex.Matches(_desc, s_regNum.ToString(), RegexOptions.IgnoreCase);
            Dictionary<TaskEventEnum, TaskEventData> data = task_data_helper.GetTaskEventData(taskId);
            //MatchCollection _matchCollection = s_regNum.Matches( _desc );
            for (int _index = 0; _index < _matchCollection.Count; _index++)
            {
                string _matchContent = _matchCollection[_index].ToString();

                try
                {
                    _desc = _desc.Replace(_matchContent, ParseAMatch(_matchContent, data));
                }
                catch(Exception ex)
                {
                    Debug.LogError(string.Format("任务{0}的任务目标配置有问题：{1}",taskId,_matchContent));
                }
            }
            return _desc;
        }

        private static string ParseAMatch(string _match, Dictionary<TaskEventEnum, TaskEventData> data)
        {
            int _index = _match.IndexOf(",");
            string key = _match.Substring(_index + 1, _match.Length - _index - 2).ToLower();
            return ParseContent(key, data);
        }

        private static string ParseContent(string _key, Dictionary<TaskEventEnum, TaskEventData> _dic)
        {
            switch (_key)
            {
                case InstId:
                    return SystemInfoUtil.ParseInstId(_dic[TaskEventEnum.InstId].value);
                case NpcId:
                    return SystemInfoUtil.ParseNpcId(_dic[TaskEventEnum.NpcId].value);
                case ItemId:
                    return SystemInfoUtil.ParseItemId(_dic[TaskEventEnum.ItemId].value);
                case Level:
                    return SystemInfoUtil.ParseNum(_dic[TaskEventEnum.Level].value);
                case Loop:
                    return SystemInfoUtil.ParseNum(_dic[TaskEventEnum.Loop].value);
                case MonsterId:
                    return SystemInfoUtil.ParseMonsterId(_dic[TaskEventEnum.MonsterId].value);
                default:
                    MessageBox.Show(false, MessageBox.DEFAULT_TITLE, "任务目标配置发现未定义的参数类型：" + _key + "，如需新增类型，请联系RTX：" + RTXDefine.GJ);
                    return string.Empty;
            }
        }

    }
}
