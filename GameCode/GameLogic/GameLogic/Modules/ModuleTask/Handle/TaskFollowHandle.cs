﻿#region 模块信息
/*==========================================
// 文件名：TaskFollowHandle
// 命名空间: ModuleTask.Handle
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/21 10:18:38
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTask.Handle
{
    public class Condition
    {
        public int id;
        public int value;
        public int operation;
        public bool isAnd;
        public int taskId;

        public bool Check(BaseItemInfo itemInfo)
        {
            if(id == 23)
            {
                return CheckValue(itemInfo.Quality);
            }
            else if(id == 37)
            {
                if(itemInfo is EquipItemInfo)
                {
                    return CheckValue((itemInfo as EquipItemInfo).Star);
                }
                return false;
            }
            else if(id == 133)
            {
                if (itemInfo is EquipItemInfo)
                {
                    return CheckValue((itemInfo as EquipItemInfo).Level);
                }
                return false;
            }
            else if(id == 141)
            {
                return (int)itemInfo.Type == value;
            }
            else if(id == 3)
            {
                return CheckValue(itemInfo.Id);
            }
            else
            {
                Debug.LogError(string.Format("@策划，任务{0}的事件存在未定义的条件{1}", taskId, id));
                return false;
            }
        }

        private bool CheckValue(int num)
        {
            if(operation == 0)
            {
                return num == value;
            }
            else if(operation == 1)
            {
                return num >= value;
            }
            else
            {
                return num < value;
            }
        }

    }

    /// <summary>
    /// 任务追踪
    /// </summary>
    public class TaskFollowHandle
    {
        public const string OPEN_VIEW = "1";
        public const string OPEN_MAP = "2";
        public const string GOTO_MISSION = "3";
        public const string PLAY_CG_OR_GUIDE = "4";
        public const string GOTO_NPC = "5";

        public static Dictionary<int, List<Condition>> taskConditionDict = new Dictionary<int,List<Condition>>();
 
        private static void GotoCommitItems(int taskId,event_cnds cnds)
        {
            if (taskConditionDict.ContainsKey(taskId) == false)
            {
                if (cnds.__conds != null)
                {
                    HandleConds(cnds.__conds, taskId, true);
                }
                else if (cnds.__conds_or != null)
                {
                    HandleConds(cnds.__conds_or, taskId, false);
                }
            }
            PanelIdEnum.ItemCommit.Show(taskId);
        }

        private static char[] split = new char[] { ';'};
        private static char[] split1 = new char[] { '=' };
        private static char[] split2 = new char[] { '}' };
        private static char[] split3 = new char[] { '{' };

        private static void HandleConds(string cnds,int taskId,bool isAnd)
        {
            
            string[] splitResult = cnds.Split(split);
            for(int i=0;i<splitResult.Length;i++)
            {
                AddCondition(splitResult[i], isAnd, taskId);
            }
        }

        private static void AddCondition(string result,bool isAnd,int taskId)
        {
            
            if(taskConditionDict.ContainsKey(taskId) == false)
            {
                taskConditionDict.Add(taskId, new List<Condition>());
            }
            
            string[] splitResult = null;
            int operation = 0;
            if (result.Contains("="))
            {
                splitResult = result.Split(split1);
                operation = 0;
            }
            else if(result.Contains("}"))
            {
                splitResult = result.Split(split2);
                operation = 1;
            }
            else if(result.Contains("{"))
            {
                splitResult = result.Split(split3);
                operation = -1;
            }
            if(splitResult!=null)
            {
                Condition conds = new Condition();
                conds.isAnd = isAnd;
                conds.operation = operation;
                conds.id = int.Parse(splitResult[0]);
                conds.value = int.Parse(splitResult[1]);
                conds.taskId = taskId;
                taskConditionDict[taskId].Add(conds);
            }
        }

        
        public static void TaskFollow(task_data data,bool isAutoTask = false)
        {
            if(isAutoTask == false)
            {
                TaskManager.Instance.IdleAutoTask();
            }
           if(data == null)
           {
               PlayerAvatar.Player.canAutoTask = false; 
               return;
           }
           PlayerAvatar.Player.canAutoTask = data.__task_type == public_config.TASK_TYPE_MAIN;
            PbTaskInfo info = PlayerDataManager.Instance.TaskData.GetTaskInfo(data.__id);
            if (info != null && info.status == public_config.TASK_STATE_ACCOMPLISH)
            {
                if(data.__npc_id_report == 0)
                {
                    PlayerDataManager.Instance.TaskData.AddDialog(data.__id);
                    PanelIdEnum.Task.Show();
                    return;
                }
                FollowToNpc(data.__npc_id_report, data.__id);
                return;
            }
            if (info != null && info.status == public_config.TASK_STATE_NEW)
            {
                FollowToNpc(data.__npc_id_accept, data.__id);
                return;
            }
            if(data.__target == null)
            {
                return;
            }
            Dictionary<string, string[]> taskFollow = task_data_helper.GetTaskFollow(data.__id);
            event_cnds cnds = task_data_helper.GetTaskEvent(data.__id);
           if (cnds.__atom_id == 79)
           {
               GotoCommitItems(data.__id,cnds);
               return;
           }
            if (taskFollow != null)
            {
                foreach(KeyValuePair<string,string[]> kvp in taskFollow)
                {
                    switch(kvp.Key)
                    {
                        case OPEN_VIEW:
                            view_helper.OpenView(int.Parse(kvp.Value[0]));
                            break;
                        case OPEN_MAP:
                            int instId = 0;
                            int monsterId = 0;
                            Dictionary<TaskEventEnum,TaskEventData> dic = task_data_helper.GetTaskEventData(data.__id);
                            if (dic.ContainsKey(TaskEventEnum.MonsterId))
                            {
                                monsterId = (int)dic[TaskEventEnum.MonsterId].value;
                                instId = (int)dic[TaskEventEnum.InstId].value;
                                EventDispatcher.TriggerEvent(TaskEvent.FIND_MONSTER, instId, monsterId,true);
                            }
                            else if (dic.ContainsKey(TaskEventEnum.InstId))
                            {
                                instId = (int)dic[TaskEventEnum.InstId].value;
                                EventDispatcher.TriggerEvent(CopyEvents.SHOW_COPY, instId,0);
                            }
                            else if(dic.ContainsKey(TaskEventEnum.Activity))
                            {
                                open activity = activity_helper.GetOpenData((int)dic[TaskEventEnum.Activity].value);
                                function_helper.ActivityFollow(activity.__fid, activity.__id, activity.__fid_arg);
                            }
                            else
                            {
                                foreach (KeyValuePair<TaskEventEnum, TaskEventData> kkk in dic)
                                {
                                    Debug.LogError("任务ID:"+info.task_id+"的事件配置有问题");
                                }
                            }
                            break;
                        case GOTO_MISSION:
                            Dictionary<TaskEventEnum, TaskEventData> dict = task_data_helper.GetTaskEventData(data.__id);
                            if (dict.ContainsKey(TaskEventEnum.InstId))
                            {
                                instId = (int)dict[TaskEventEnum.InstId].value;
                                _instanceId = instId;
                                CopyLogicManager.Instance.RequestEnterCopy(ChapterType.EveningActivity, RequesetEnterCopy, instId);
                                //EventDispatcher.TriggerEvent(CopyEvents.SHOW_COPY, instId, 0);
                            }
                            break;
                        case PLAY_CG_OR_GUIDE:
                            DramaManager.GetInstance().Trigger(kvp.Value[0], kvp.Value[1]);
                            TaskManager.Instance.FinishClientTask(10001,data.__id);
                            break;
                        case GOTO_NPC:
                            EventDispatcher.TriggerEvent(PlayerCommandEvents.FIND_NPC_AND_ENTER_FB, kvp.Value, data.__id, true);
                            break;
                    }
                    return;
                }
            }
            else
            {

            }
        }

        private static int _instanceId;
        private static void RequesetEnterCopy()
        {
            MissionManager.Instance.RequsetEnterMission(_instanceId);
        }


        private static void FollowToMap(uint mapId)
        {
            LoggerHelper.Debug("FollowToMap:"+mapId);
            //EventDispatcher.TriggerEvent( MissionEvents.UI_SHOW_STORY_COPY_CHAPTER, mapId );
        }

        public static void FollowToNpc(int npcId,int taskId)
        {
            LoggerHelper.Debug("Ari FollowToNpc:" + npcId);
            if(npcId == 0 || npcId == 999)
            {
                TaskModule.npcId = npcId;
                UIManager.Instance.ShowPanel(PanelIdEnum.NpcDailog,  new int[] { npcId, taskId });
            }
            else
            {
                EventDispatcher.TriggerEvent(PlayerCommandEvents.FIND_NPC, npcId, true);
            }
           
            //EventDispatcher.TriggerEvent(TaskEvent.TALK_TO_NPC, npcId);
        }

        private static void ShowPanel(int panelId,int tab)
        {
            UIManager.Instance.ShowPanel((PanelIdEnum)panelId,  tab);
        }

        public static void ClosePanel(int panelId)
        {
            UIManager.Instance.ClosePanel((PanelIdEnum)panelId);
        }
    }
}
