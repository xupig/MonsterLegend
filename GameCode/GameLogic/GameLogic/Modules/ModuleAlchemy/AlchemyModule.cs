﻿#region 模块信息
/*==========================================
// 文件名：AlchemyModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleAlchemy
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/14 15:45:37
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleAlchemy
{
    public class AlchemyModule:BaseModule
    {
        public override void Init()
        {
            RegisterPopPanelBunchPanel(PanelIdEnum.AlchemyInvite, PanelIdEnum.Alchemy);
            AddPanel(PanelIdEnum.Alchemy, "Container_AlchemyPanel", MogoUILayer.LayerUIPanel, "ModuleAlchemy.AlchemyPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.AlchemyInvite, "Container_AlchemyInvitePopPanel", MogoUILayer.LayerUIPopPanel, "ModuleAlchemy.AlchemyInvitePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }
    }
}
