﻿#region 模块信息
/*==========================================
// 文件名：AlchemyInvitePanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleAlchemy
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/13 14:55:34
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleFriend;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleAlchemy
{

    public enum AlchemyInviteType
    {
        Invite_Friend = 1,
        Invite_Guild = 2,
    }

    public class AlchemyInvitePanel:BasePanel
    {

        private StateText _txtTitle;
        private StateText _txtEmpty;
        private KScrollView _scrollView;
        private KPageableList _pageableList;
        private AlchemyInviteType type;

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            CloseBtn = GetChildComponent<KButton>("Container_biaoti/Button_close");
            _txtTitle = GetChildComponent<StateText>("Container_biaoti/Label_txtBiaoti");
            _txtEmpty = GetChildComponent<StateText>("Label_txtTishi");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_xuanzegonghui");
            _pageableList = _scrollView.GetChildComponent<KPageableList>("mask/content");

            _pageableList.SetPadding(0, 0, 0, 0);
            _pageableList.SetGap(0, 0);
            _pageableList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);

            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.AlchemyInvite; }
        }

        public override void OnShow(object data)
        {
            EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_LIST,OnFriendListChange);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Member_List, OnGuildListChange);
            SetData(data);
        }

        private void OnFriendListChange()
        {
            if (type == AlchemyInviteType.Invite_Friend)
            {
                UpdateFriendList();
            }
        }

        private void UpdateFriendList()
        {
          
            List<PbFriendAvatarInfo> resultList = new List<PbFriendAvatarInfo>();
            List<PbFriendAvatarInfo> list = PlayerDataManager.Instance.FriendData.FriendAvatarList;
            Dictionary<UInt64, PbAlchemyAssist> dict = PlayerDataManager.Instance.AlchemyData.hadInviteAssistDict;
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].online == 1 && dict.ContainsKey(list[i].dbid) == false && list[i].dbid != PlayerAvatar.Player.dbid && list[i].remain_assist_cnt > 0)
                {
                    resultList.Add(list[i]);
                }
            }
            if (list.Count == 0)
            {
                _txtEmpty.CurrentText.text = (57030).ToLanguage();
            }
            else
            {
                 _txtEmpty.CurrentText.text = (110524).ToLanguage();
            }
            
            _txtEmpty.Visible = resultList.Count == 0;
            resultList.Sort(SortFriendAlchemy);
            _pageableList.SetDataList<AlchemyInviteItem>(resultList, 1);
        }

        private int SortFriendAlchemy(PbFriendAvatarInfo first, PbFriendAvatarInfo second)
        {
            if (first.degree != second.degree)
            {
                return (int)(second.degree - first.degree);
            }
            else if(first.remain_assist_cnt != second.remain_assist_cnt)
            {
                return (int)(second.remain_assist_cnt > 0 ? 1 : -1f);
            }
            else
            {
                return 0;
            }
        }

        private void OnGuildListChange()
        {
            if (type == AlchemyInviteType.Invite_Guild)
            {
                UpdateGuildList();
            }
        }

        private void UpdateGuildList()
        {
            List<PbGuildMemberInfo> memberList = PlayerDataManager.Instance.GuildData.GetMemberList();
            List<PbGuildMemberInfo> resultList = new List<PbGuildMemberInfo>();
            Dictionary<UInt64, PbAlchemyAssist> dict = PlayerDataManager.Instance.AlchemyData.hadInviteAssistDict;
            for (int i = 0; i < memberList.Count; i++)
            {
                if (memberList[i].online == 1 && memberList[i].remain_assist_cnt>0 && dict.ContainsKey(memberList[i].dbid) == false && memberList[i].dbid != PlayerAvatar.Player.dbid)
                {
                    resultList.Add(memberList[i]);
                }
            }
            if (memberList.Count <= 1)
            {
                 _txtEmpty.CurrentText.text = (63509).ToLanguage();
            }
            else
            {
                 _txtEmpty.CurrentText.text = (110525).ToLanguage();
            }
            
            _txtEmpty.Visible = resultList.Count == 0;
            resultList.Sort(SortGuildAlchemy);
            _pageableList.SetDataList<AlchemyInviteItem>(resultList, 1);
        }


        private int SortGuildAlchemy(PbGuildMemberInfo first, PbGuildMemberInfo second)
        {
            if (first.guild_position != second.guild_position)
            {
                return (int)(second.guild_position - first.guild_position);
            }
            else if (first.remain_assist_cnt != second.remain_assist_cnt)
            {
                return (int)(second.remain_assist_cnt > 0 ? 1 : -1f);
            }
            else
            {
                return 0;
            }
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            type = (AlchemyInviteType)data;
            if(type == AlchemyInviteType.Invite_Friend)
            {
                FriendManager.Instance.SendGetFriendListMsg();
                _txtTitle.CurrentText.text = (110515).ToLanguage();
                
                UpdateFriendList();
            }
            else
            {
                _txtTitle.CurrentText.text = (110516).ToLanguage();
               
                GuildManager.Instance.GetMemberList();
                UpdateGuildList();
            }
        }

        public override void OnClose()
        {
            EventDispatcher.RemoveEventListener(FriendEvents.REFRESH_FRIEND_LIST, OnFriendListChange);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Member_List, OnGuildListChange);
        }
    }
}
