﻿#region 模块信息
/*==========================================
// 文件名：AlchemyPlayerView
// 命名空间: GameLogic.GameLogic.Modules.ModuleAlchemy.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/14 10:40:15
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleAlchemy
{
    public class AlchemyPlayerView:KContainer
    {

        private List<AlchemyPlayerItem> _playerList;
        private AlchemyPlayerItem _me;

        private KContainer _invitePopView;
        private KButton _btnInviteFriend;
        private KButton _btnInviteGuild;
        private KDummyButton _btnCloseInvite;
        private RectTransform _rectTrans;

        private Vector3 _pos;
        //private KParticle _particle;
        private AlchemyPlayerItem _selectedItem;
        

        protected override void Awake()
        {
            //_particle = AddChildComponent<KParticle>("fx_ui_lianjin_lianjinlu");
           // _particle.Stop();
            _pos = GetChildComponent<RectTransform>("Image_lianjinlu").anchoredPosition;
            _me = AddChildComponent<AlchemyPlayerItem>("Container_me");

            _invitePopView = GetChildComponent<KContainer>("Container_xiaotanchuang");
            _invitePopView.Visible = false;
            _btnCloseInvite = AddChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
            _btnCloseInvite.gameObject.SetActive(false);
            _rectTrans = _invitePopView.GetComponent<RectTransform>();
            _btnInviteGuild = _invitePopView.GetChildComponent<KButton>("Button_gonghuihaoyou");
            _btnInviteFriend = _invitePopView.GetChildComponent<KButton>("Button_haoyou");

            

            InitPlayerList();

            _btnInviteGuild.onClick.AddListener(OnClickInviteGuild);
            _btnInviteFriend.onClick.AddListener(OnClickInviteFriend);
            _btnCloseInvite.onClick.AddListener(OnCloseInviteView);
           
            base.Awake();
        }


        protected override void OnEnable()
        {
            base.OnEnable();
        }

        private void InitPlayerList()
        {
            _playerList = new List<AlchemyPlayerItem>();

            for(int i=0;i<4;i++)
            {
                AlchemyPlayerItem playerItem = AddChildComponent<AlchemyPlayerItem>(string.Format("Container_player{0}",i+1));
                playerItem.pos = i + 1;
                _playerList.Add(playerItem);
                playerItem.onClick.AddListener(onClickPlayerItem);
            }
        }

        public void UpdatePlayerList()
        {
            foreach(KeyValuePair<int,PbAlchemyAssist> kvp in PlayerDataManager.Instance.AlchemyData.alchemyAssistDict)
            {
                _playerList[kvp.Key - 1].UpdateContent(kvp.Value);
            }
            _me.UpdateMeContent();
        }

        public void UpdatePlayerItem(int pos,UInt64 dbId)
        {
            if(pos>0 && pos<=_playerList.Count)
            {
                if(PlayerDataManager.Instance.AlchemyData.alchemyAssistDict.ContainsKey(pos))
                {
                     _playerList[pos - 1].UpdateContent(PlayerDataManager.Instance.AlchemyData.alchemyAssistDict[pos]);
                }
                else
                {
                     _playerList[pos - 1].UpdateContent(null);
                }
            }
        }

        protected override void OnDisable()
        {
            if (_selectedItem != null)
            {
                _selectedItem.SetSelected(false);
            }
            _selectedItem = null;
            base.OnDisable();
        }

        private void onClickPlayerItem(AlchemyPlayerItem playerItem)
        {
            if (_selectedItem!=null)
            {
                _selectedItem.SetSelected(false);
            }
            _selectedItem = playerItem;
            _selectedItem.SetSelected(true);
            PlayerDataManager.Instance.AlchemyData.currentSelectPos = playerItem.pos;
            _rectTrans.anchoredPosition = playerItem.Position + new Vector2(169, 3);
            _invitePopView.Visible = true;
            _btnCloseInvite.gameObject.SetActive(true);
        }

        private void OnClickInviteGuild()
        {
            if(function_helper.IsFunctionOpen(FunctionId.guild))
            {
                PanelIdEnum.AlchemyInvite.Show(AlchemyInviteType.Invite_Guild);
                _invitePopView.Visible = false;
                _btnCloseInvite.gameObject.SetActive(false);
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, function_helper.GetConditionDesc(FunctionId.guild));   
            }
        }

        private void OnClickInviteFriend()
        {
            if (function_helper.IsFunctionOpen(FunctionId.friend))
            {
                PanelIdEnum.AlchemyInvite.Show(AlchemyInviteType.Invite_Friend); 
                _invitePopView.Visible = false;
                _btnCloseInvite.gameObject.SetActive(false);
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, function_helper.GetConditionDesc(FunctionId.friend));   
            }
        }

        private void OnCloseInviteView()
        {
            _invitePopView.Visible = false;
            _btnCloseInvite.gameObject.SetActive(false);
            if (_selectedItem != null)
            {
                _selectedItem.SetSelected(false);
            }
            _selectedItem = null;
        }

       

        protected override void OnDestroy()
        {
            base.OnDestroy();
            _btnInviteGuild.onClick.RemoveListener(OnClickInviteGuild);
            _btnInviteFriend.onClick.RemoveListener(OnClickInviteFriend);
            _btnCloseInvite.onClick.RemoveListener(OnCloseInviteView);
           
        }


    }
}
