﻿#region 模块信息
/*==========================================
// 文件名：AlchmyRewardPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleAlchemy
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/14 15:54:49
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Mogo.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleAlchemy
{
    public class AlchemyRewardView : KContainer
    {

        private StateText _txtTotal;
        private StateText _txtBase;
        private StateText _txtAddtion;

        private string totalTemplate;
        private string baseTemplate;
        private string additionTemplate;

        private StateImage _bg;

        private KButton _btnReceive;

        private KParticle _particle;

        private KParticle _getGoldParticle;

        private Vector3 _pos;
        public KComponentEvent onClick = new KComponentEvent();

        protected override void Awake()
        {
            _particle = AddChildComponent<KParticle>("fx_ui_lianjin_linqushouyi");
            _particle.Stop();

            _getGoldParticle = AddChildComponent<KParticle>("fx_ui_17_jinbi_01");
            _getGoldParticle.Stop();
            _pos = GetChildComponent<RectTransform>("Image_jinbidui").anchoredPosition;
            _txtTotal = GetChildComponent<StateText>("Label_txtGongji");
            totalTemplate = _txtTotal.CurrentText.text;
            _txtBase = GetChildComponent<StateText>("Label_txtChanggui");
            baseTemplate = _txtBase.CurrentText.text;
            _txtAddtion = GetChildComponent<StateText>("Label_txtXiezhu");
            additionTemplate = _txtAddtion.CurrentText.text;
            _btnReceive = GetChildComponent<KButton>("Button_lingqushouyi");
           
            _bg = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            MogoUtils.AdaptScreen(_bg);
            base.Awake();
        }

        protected override void OnEnable()
        {
            _getGoldParticle.Stop();
            _getGoldParticle.onComplete.AddListener(OnComplete);
            _btnReceive.onClick.AddListener(OnRecieveReward);
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            _btnReceive.onClick.RemoveListener(OnRecieveReward);
            _getGoldParticle.onComplete.RemoveListener(OnComplete);
            base.OnDisable();
        }

        private void OnComplete()
        {
            AlchemyManager.Instance.ReceiveAlchemyReward();
        }

        private void OnRecieveReward()
        {
            UIManager.Instance.PlayAudio("Sound/UI/money_get.mp3");
            _getGoldParticle.Play();
            _getGoldParticle.Position = new Vector3(100, -50, 0);
            Visible = false;
            onClick.Invoke();
        }

        public void UpdateContent()
        {
            int num = PlayerDataManager.Instance.AlchemyData.GetAssistNum();
            alchemy alchemy = alchemy_helper.GetAlchemyById(PlayerAvatar.Player.alchemy_cur_cnt);
            int baseReward = alchemy_helper.GetAlchemyBasicReward(alchemy);
            int additionReward = alchemy_helper.GetAlchemyAdditionReward(alchemy, num);
            _txtTotal.CurrentText.text = string.Format("{0}{1}", (110520).ToLanguage(),(110522).ToLanguage(baseReward + additionReward));
            _txtBase.CurrentText.text = string.Format("{0}{1}", (110518).ToLanguage(), (110522).ToLanguage(baseReward));
            _txtAddtion.CurrentText.text = string.Format("{0}{1}", (110519).ToLanguage(), (110522).ToLanguage(additionReward));
            _particle.Play(true);
            _particle.Position = _pos;
        }

        protected override void OnDestroy()
        {
            _btnReceive.onClick.RemoveListener(OnRecieveReward);
            _getGoldParticle.onComplete.RemoveListener(OnComplete);
            base.OnDestroy();
        }

    }
}
