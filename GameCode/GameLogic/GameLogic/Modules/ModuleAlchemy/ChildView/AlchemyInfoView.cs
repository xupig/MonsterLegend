﻿#region 模块信息
/*==========================================
// 文件名：AlchemyInfoView
// 命名空间: GameLogic.GameLogic.Modules.ModuleAlchemy.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/16 9:39:27
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Mogo.Util;
using UnityEngine;

namespace ModuleAlchemy
{
    public class AlchemyInfoView:KContainer
    {

        private AlchemyCurrencyView _currencyView;

        private AlchemyInfoItem _baseItem;
        private AlchemyInfoItem _additionItem;
        private AlchemyInfoItem _totalItem;
        private StateText _txtCanAlchemyTimes;
        private string _alchemyTemplate;
        private StateText _txtCanAssistAlchemyTimes;
        private string _assistAlchemyTemplate;

        private KButton _btnAlchemy;
        private StateIcon _alchemyCostIcon;
        private StateText _txtAlchemyCost;
        private StateText _txtLabel;
        private StateText _txtCostNothing;
        private string _alchemtCostTemplate;

        private StateText _txtComplete;

        private StateText _txtTitle;

        protected override void Awake()
        {
            _currencyView = AddChildComponent<AlchemyCurrencyView>("Container_currency");

            KContainer container = GetChildComponent<KContainer>("Container_neirong");
            _baseItem = container.AddChildComponent<AlchemyInfoItem>("Container_jiben");
            _additionItem = container.AddChildComponent<AlchemyInfoItem>("Container_xiezhu");
            _totalItem = container.AddChildComponent<AlchemyInfoItem>("Container_gongji");

            _txtCanAlchemyTimes = container.GetChildComponent<StateText>("Label_txtKelianjicishu");
            _alchemyTemplate = _txtCanAlchemyTimes.CurrentText.text;

            _txtCanAssistAlchemyTimes = container.GetChildComponent<StateText>("Label_txtKexiezhulianjicishu");
            _assistAlchemyTemplate = _txtCanAssistAlchemyTimes.CurrentText.text;

            _txtComplete = container.GetChildComponent<StateText>("Label_txtWancheng");

            _btnAlchemy = GetChildComponent<KButton>("Button_lianjin");
            _alchemyCostIcon = _btnAlchemy.GetChildComponent<StateIcon>("stateIcon");
            _txtAlchemyCost = _btnAlchemy.GetChildComponent<StateText>("cost");
            _alchemtCostTemplate = _txtAlchemyCost.CurrentText.text;
            _txtLabel = _btnAlchemy.GetChildComponent<StateText>("label");
            _txtCostNothing = _btnAlchemy.GetChildComponent<StateText>("costNothing");
            _btnAlchemy.onClick.AddListener(OnAlchemy);

            _txtTitle = container.GetChildComponent<StateText>("Label_txtBiaoti");
            base.Awake();
        }

        private void OnAlchemy()
        {
            alchemy alchemy = alchemy_helper.GetAlchemyById(PlayerAvatar.Player.alchemy_cur_cnt + 1);
            BaseItemData costItem = price_list_helper.GetCost((int)PriceListId.alchemy, alchemy.__id);
            int result = PlayerAvatar.Player.CheckCostLimit(costItem.Id, costItem.StackCount, true);
            if(result <=0 )
            {
                 if(result == -1)
                 {
                     MogoUtils.UseDiamondReplaceBindDiamond(costItem.StackCount, Alchemy, null);
                 }
                 else
                 {
                     Alchemy();
                 }

            }
        }

        private void Alchemy()
        {
            AlchemyManager.Instance.Alchemy(PlayerDataManager.Instance.AlchemyData.alchemyAssistDict);
        }

        protected override void OnDestroy()
        {
            _btnAlchemy.onClick.RemoveListener(OnAlchemy);
            base.OnDestroy();
        }

       

        public void UpdateContent()
        {
            int num = PlayerDataManager.Instance.AlchemyData.GetAssistNum();
            int alchemyTimes = int.Parse(vip_rights_helper.GetVipRights(VipRightType.Vipmakegold_times_limit, PlayerAvatar.Player.vip_level)) - PlayerAvatar.Player.alchemy_cnt;
            alchemy alchemy = alchemy_helper.GetAlchemyById(PlayerAvatar.Player.alchemy_cur_cnt + 1);
            if(alchemyTimes <= 0)
            {
                SetAlchemyState(true);
            }
            else
            {
                SetAlchemyState(false);
                _txtCanAlchemyTimes.CurrentText.text = string.Format(_alchemyTemplate,alchemyTimes);
                int baseReward = alchemy_helper.GetAlchemyBasicReward(alchemy);
                int additionReward = alchemy_helper.GetAlchemyAdditionReward(alchemy, num);
                _baseItem.UpdateContent(string.Format("{0}{1}", (110518).ToLanguage(), (110522).ToLanguage(baseReward)));
                _additionItem.UpdateContent(string.Format("{0}{1}", (110519).ToLanguage(), (110522).ToLanguage(additionReward)));
                _totalItem.UpdateContent(string.Format("{0}{1}", (110520).ToLanguage(), (110522).ToLanguage(baseReward + additionReward)));
                BaseItemData costItem = price_list_helper.GetCost((int)PriceListId.alchemy,alchemy.__id);
                _txtAlchemyCost.ChangeAllStateText(string.Format(_alchemtCostTemplate, costItem.StackCount));

                _alchemyCostIcon.Visible = costItem.StackCount > 0;
                _txtAlchemyCost.Visible = costItem.StackCount > 0;
                _txtLabel.Visible = costItem.StackCount > 0;
                _txtCostNothing.Visible = costItem.StackCount == 0;

                _alchemyCostIcon.SetIcon(costItem.Icon);
            }
            if(alchemy ==null)
            {
                
            }
            else
            {
                long deltaTime = (long)Global.serverTimeStampSecond - (long)PlayerAvatar.Player.alchemy_time_stamp;
                if (deltaTime < alchemy.__time)
                {
                    _btnAlchemy.Visible = false;
                    _txtTitle.CurrentText.text = (110509).ToLanguage();
                }
                else
                {
                    _txtTitle.CurrentText.text = (110508).ToLanguage();
                }
            }
        }

        public void UpdateAsistAlchemyInfo()
        {
            int vipAssistAlchemyTimes = int.Parse(vip_rights_helper.GetVipRights(VipRightType.assistant_makegold_times, PlayerAvatar.Player.vip_level)) - (int)PlayerAvatar.Player.assist_alchemy_cnt;
            _txtCanAssistAlchemyTimes.CurrentText.text = string.Format(_assistAlchemyTemplate,vipAssistAlchemyTimes);
        }

        private void SetAlchemyState(bool isCompleted)
        {
            _txtComplete.Visible = isCompleted;
            _txtCanAlchemyTimes.Visible = !isCompleted;
            _txtCanAssistAlchemyTimes.Visible = !isCompleted;
            _baseItem.Visible = !isCompleted;
            _additionItem.Visible = !isCompleted;
            _totalItem.Visible = !isCompleted;
            _btnAlchemy.Visible = !isCompleted;
            
        }
    }
}
