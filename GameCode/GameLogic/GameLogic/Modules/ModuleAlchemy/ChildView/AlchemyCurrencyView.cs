﻿#region 模块信息
/*==========================================
// 文件名：AlchemyCurrencyView
// 命名空间: GameLogic.GameLogic.Modules.ModuleAlchemy.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/14 10:26:05
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Common.ServerConfig;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleAlchemy
{
    public class AlchemyCurrencyView:KContainer
    {

        private MoneyItem _goldItem;
        private MoneyItem _alchemyItem;

        protected override void Awake()
        {
            base.Awake();
            _goldItem = AddChildComponent<MoneyItem>("Container_buyGold");
            _goldItem.HideBtn();
            _alchemyItem = AddChildComponent<MoneyItem>("Container_buyDiamond");
            InitCurrencyIcon();
        }


        private void InitCurrencyIcon()
        {
            _goldItem.SetMoneyType(public_config.MONEY_TYPE_GOLD);
            _alchemyItem.SetMoneyType(public_config.MONEY_TYPE_BIND_DIAMOND);
        }


    }
}
