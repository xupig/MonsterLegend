﻿#region 模块信息
/*==========================================
// 文件名：AlchemyPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleAlchemy
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/12 11:29:02
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleAlchemy
{
    public class AlchemyPanel:BasePanel
    {
        private AlchemyPlayerView _playerView;
        private AlchemyInfoView _infoView;
        private AlchemyRewardView _rewardView;
        private KToggleGroup _toggleGroup;
        private KContainer _panelBg;
        private KButton _btnQuestion;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _rewardView = AddChildComponent<AlchemyRewardView>("Container_reward");
            _playerView = AddChildComponent<AlchemyPlayerView>("Container_left");
            _infoView = AddChildComponent<AlchemyInfoView>("Container_right");
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            _panelBg = GetChildComponent<KContainer>("Container_panelBg");
            ModalMask = _playerView.GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _btnQuestion = GetChildComponent<KButton>("Button_Gantang");
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Alchemy; }
        }

        public override void OnShow(object data)
        {
            SetData(data);
            AddEventListener();
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            RefreshContent();
        }

        private void RefreshContent()
        {
            AlchemyManager.Instance.RequestAlchemyAssitCnt();
            _rewardView.Visible = false;
            _playerView.Visible = true;
            _infoView.Visible = true;
            _toggleGroup.Visible = true;
            _panelBg.Visible = true;
            _playerView.UpdatePlayerList();
            _infoView.UpdateContent();
            _infoView.UpdateAsistAlchemyInfo();
        }

        private void OnUpdateAlchemyReward()
        {
            _rewardView.Visible = true;
            _rewardView.UpdateContent();
            _playerView.Visible = false;
            _infoView.Visible = false;
            _toggleGroup.Visible = false;
            _panelBg.Visible = false;
        }

        public override void OnClose()
        {
            RemoveEventListener();
            PlayerDataManager.Instance.AlchemyData.ClearInvite();
            PlayerDataManager.Instance.AlchemyData.ClearPlayer();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<UInt64,int>(AlchemyEvents.UPDATE_ALCHEMY_INVITE_PLAYER, OnAlchemyPlayerChange);
            EventDispatcher.AddEventListener(AlchemyEvents.UPDATE_ALCHEMY_ASSIST_CNT, OnAlchemyAssistCntChange);
            EventDispatcher.AddEventListener(AlchemyEvents.UPDATE_ALCHEMY_ASSIST_LIST, OnAlchemyAssistListChange);

            EventDispatcher.AddEventListener(AlchemyEvents.UPDATE_ALCHEMY_REWARD, OnUpdateAlchemyReward);
            _btnQuestion.onClick.AddListener(OnClickQuestion);
            _rewardView.onClick.AddListener(RefreshContent);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<UInt64,int>(AlchemyEvents.UPDATE_ALCHEMY_INVITE_PLAYER, OnAlchemyPlayerChange);
            EventDispatcher.RemoveEventListener(AlchemyEvents.UPDATE_ALCHEMY_ASSIST_CNT, OnAlchemyAssistCntChange);
            EventDispatcher.RemoveEventListener(AlchemyEvents.UPDATE_ALCHEMY_ASSIST_LIST, OnAlchemyAssistListChange);
            EventDispatcher.RemoveEventListener(AlchemyEvents.UPDATE_ALCHEMY_REWARD, OnUpdateAlchemyReward);
            _btnQuestion.onClick.RemoveListener(OnClickQuestion);
            _rewardView.onClick.RemoveListener(RefreshContent);
        }

        private void OnClickQuestion()
        {
            Vector2 pos = new Vector2(640, 270 - UIManager.CANVAS_HEIGHT);
            PanelIdEnum.CommonTips.Show(new object[] { (110511).ToLanguage(), pos });
        }


        private void OnAlchemyPlayerChange(UInt64 dbid,int pos)
        {
            _playerView.UpdatePlayerItem(pos, dbid);
            _infoView.UpdateContent();
        }

        private void OnAlchemyAssistCntChange()
        {
            _infoView.UpdateAsistAlchemyInfo();
        }

        private void OnAlchemyAssistListChange()
        {
            _playerView.UpdatePlayerList();
            _infoView.UpdateContent();
        }

    }
}
