﻿#region 模块信息
/*==========================================
// 文件名：AlchemyFriendItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleAlchemy.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/13 16:15:07
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleAlchemy
{
    public class AlchemyFriendItem:KContainer
    {

        private StateText _txtProgress;
        private KProgressBar _progressBar;
        private LoveStarItemList _loveStarItemList;

        private StateText _txtFightForce;
        private StateText _txtLevel;
        private StateText _txtVipLevel;
        private StateText _txtName;
        private KButton _btnInvite;
        private IconContainer _iconContaienr;

        private PbFriendAvatarInfo _friendAvatarInfo;

        protected override void Awake()
        {
            _loveStarItemList = gameObject.AddComponent<LoveStarItemList>();
            _txtProgress = GetChildComponent<StateText>("Label_txtJindu");
            _progressBar = GetChildComponent<KProgressBar>("ProgressBar_jindu");

            _txtFightForce = GetChildComponent<StateText>("Label_txtZhanli02");
            // 隐藏VIP等级显示
            _txtVipLevel = GetChildComponent<StateText>("Label_txtDengji");
            _txtVipLevel.Visible = false;

            

            _txtLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            _txtName = GetChildComponent<StateText>("Label_txtwanjiamingzi");

            _btnInvite = GetChildComponent<KButton>("Button_yaoqing");
            _iconContaienr = AddChildComponent<IconContainer>("Container_icon");
            _btnInvite.onClick.AddListener(OnClickInvite);
        }

        private void OnClickInvite()
        {
            PbAlchemyAssist assist = new PbAlchemyAssist();
            assist.dbid = _friendAvatarInfo.dbid;
            assist.name = _friendAvatarInfo.name;
            assist.vocation = _friendAvatarInfo.vocation;
            assist.level = _friendAvatarInfo.level;
            assist.name_bytes = _friendAvatarInfo.name_bytes;
            PlayerDataManager.Instance.AlchemyData.UpdateCurrentSelectAlchemyAssit(assist);
            AlchemyManager.Instance.InviteAlchemy(_friendAvatarInfo.dbid, PlayerDataManager.Instance.AlchemyData.currentSelectPos);
        }


        public void RefreshContent(PbFriendAvatarInfo data)
        {
            _friendAvatarInfo = data;

            _txtName.CurrentText.text = _friendAvatarInfo.name;
            _txtLevel.CurrentText.text = _friendAvatarInfo.level.ToString();
            _txtFightForce.CurrentText.text = _friendAvatarInfo.fight.ToString();
            _iconContaienr.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_friendAvatarInfo.vocation), UpdateOnlineState);

            float level = intimate_helper.GetIntimateLevel((int)_friendAvatarInfo.degree);
            int max = intimate_helper.GetIntimateLevelMax((int)_friendAvatarInfo.degree);
            _loveStarItemList.SetLevel(level);
            _progressBar.Value = (_friendAvatarInfo.degree) / (float)max;
            _txtProgress.CurrentText.text = string.Format("{0}/{1}", _friendAvatarInfo.degree, max);
        }

        private void UpdateOnlineState(GameObject go)
        {
            ImageWrapper[] wrappers = _iconContaienr.GetComponentsInChildren<ImageWrapper>();
            for (int i = 0; i < wrappers.Length; i++)
            {
                if (_friendAvatarInfo.online == 1)
                {
                    wrappers[i].SetGray(1);
                }
                else
                {
                    wrappers[i].SetGray(0);
                }
            }
        }


        protected override void OnDestroy()
        {
            _btnInvite.onClick.RemoveListener(OnClickInvite);
            base.OnDestroy();
        }


    }
}
