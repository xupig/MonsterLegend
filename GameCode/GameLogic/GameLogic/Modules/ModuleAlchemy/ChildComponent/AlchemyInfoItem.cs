﻿#region 模块信息
/*==========================================
// 文件名：AlchemyInfoItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleAlchemy.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/16 9:41:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleAlchemy
{
    public class AlchemyInfoItem:KContainer
    {

        private StateText _txtContent;
        private string template;
        protected override void Awake()
        {
            _txtContent = GetChildComponent<StateText>("Label_txtContent");
            template = _txtContent.CurrentText.text;
            base.Awake();
        }

        public void UpdateContent(int value)
        {
            _txtContent.CurrentText.text = string.Format(template,value);
        }

        public void UpdateContent(string content)
        {
            _txtContent.CurrentText.text = content;
        }
    }
}
