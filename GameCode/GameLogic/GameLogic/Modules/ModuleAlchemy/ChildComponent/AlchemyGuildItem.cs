﻿#region 模块信息
/*==========================================
// 文件名：AlchemyGuildItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleAlchemy.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/13 16:47:10
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleAlchemy
{
    public class AlchemyGuildItem:KContainer
    {
        private PbGuildMemberInfo _data;
        private StateText _nameText;
        private StateText _levelText;
        private StateText _fightForceText;
        private StateText _positionText;
        private StateText _vipLevelText;
        private KButton _btnInvite;
        private IconContainer _iconContainer;
        protected override void Awake()
        {
            _nameText = GetChildComponent<StateText>("Label_txtwanjiamingzi");
            _levelText = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            _vipLevelText = GetChildComponent<StateText>("Label_txtDengji");
            _vipLevelText.Visible = false;
            _fightForceText = GetChildComponent<StateText>("Label_txtZhanli02");
            _positionText = GetChildComponent<StateText>("Label_txtzhiwei");
            _btnInvite = GetChildComponent<KButton>("Button_yaoqing");
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _btnInvite.onClick.AddListener(OnClickInvite);
        }

        public void RefreshContent(PbGuildMemberInfo data)
        {

            _data = data;
            _nameText.CurrentText.text = _data.name;
            _levelText.CurrentText.text = _data.level.ToString();
            _fightForceText.CurrentText.text = _data.fight.ToString();
            _iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_data.vocation), UpdateOnlineState);
            _positionText.CurrentText.text = MogoLanguageUtil.GetContent(74626 + (int)_data.guild_position - 1);
        }

        private void UpdateOnlineState(GameObject go)
        {
            ImageWrapper[] wrappers = _iconContainer.GetComponentsInChildren<ImageWrapper>();
            for (int i = 0; i < wrappers.Length; i++)
            {
                if (_data.online == 1)
                {
                    wrappers[i].SetGray(1);
                }
                else
                {
                    wrappers[i].SetGray(0);
                }
            }
        }

        protected override void OnDestroy()
        {
            _btnInvite.onClick.RemoveListener(OnClickInvite);
            base.OnDestroy();
        }

        private void OnClickInvite()
        {
            PbAlchemyAssist assist = new PbAlchemyAssist();
            assist.dbid = _data.dbid;
            assist.name = _data.name;
            assist.vocation = _data.vocation;
            assist.level = _data.level;
            assist.name_bytes = _data.name_bytes;
            PlayerDataManager.Instance.AlchemyData.UpdateCurrentSelectAlchemyAssit(assist);
            AlchemyManager.Instance.InviteAlchemy(_data.dbid, PlayerDataManager.Instance.AlchemyData.currentSelectPos);
        }
    }
}
