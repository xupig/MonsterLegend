﻿#region 模块信息
/*==========================================
// 文件名：AlchemyPlayerItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleAlchemy.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/14 10:44:08
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.Entities;
using Mogo.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleAlchemy
{
    public class AlchemyPlayerItem:KContainer
    {

        public int pos
        {
            get;
            set;
        }

        public Vector2 Position
        {
            get;
            set;
        }
        private KComponentEvent<AlchemyPlayerItem> _onClick;

        public KComponentEvent<AlchemyPlayerItem> onClick
        {
            get
            {
                if(_onClick == null)
                {
                    _onClick = new KComponentEvent<AlchemyPlayerItem>();
                }
                return _onClick;
            }
        }

        private KContainer _playerInfo;

        private IconContainer _iconContainer;
        private StateText _txtName;
        private StateText _txtLevel;

        private KButton _btnCheckmark;
        private KButton _btnEmpty;

        private PbAlchemyAssist _assist;

        private StateImage _selected;

        protected override void Awake()
        {
            Position = GetComponent<RectTransform>().anchoredPosition;
            _playerInfo = GetChildComponent<KContainer>("Container_haoyou");
            _playerInfo.gameObject.AddComponent<RaycastComponent>().RayCast = false;
            _iconContainer = _playerInfo.AddChildComponent<IconContainer>("Container_icon");
            _txtName = _playerInfo.GetChildComponent<StateText>("Label_txtName");
            _txtLevel = _playerInfo.GetChildComponent<StateText>("Container_dengji/Label_txtDengji");

            _btnCheckmark = GetChildComponent<KButton>("Button_checkmark");
            _btnEmpty = GetChildComponent<KButton>("Button_empty");
            _btnEmpty.onClick.AddListener(OnClickAdd);
            _btnCheckmark.onClick.AddListener(OnClickAdd);
            _selected = GetChildComponent<StateImage>("Image_lianjinxuanzhong");
            _selected.Raycast = false;
            _selected.Visible = false;
            base.Awake();
        }

        public void SetSelected(bool isSelected)
        {
            _selected.Visible = isSelected;
        }

        protected override void OnDestroy()
        {
            _btnEmpty.onClick.RemoveListener(OnClickAdd);
            _btnCheckmark.onClick.RemoveListener(OnClickAdd);
            base.OnDestroy();
        }

        public void UpdateContent(PbAlchemyAssist assist)
        {
            _assist = assist;
            if (assist == null)
            {
                _playerInfo.Visible = false;
                _btnCheckmark.Visible = false;
            }
            else
            {
                _playerInfo.Visible = true;
                _btnCheckmark.Visible = true;
                _iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)assist.vocation));
                _txtName.CurrentText.text = assist.name;
                _txtLevel.CurrentText.text = assist.level.ToString();
            }
        }

        public void UpdateMeContent()
        {
            _playerInfo.Visible = true;
            _btnCheckmark.Visible = true;
            _iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)PlayerAvatar.Player.vocation));
            _txtName.CurrentText.text = PlayerAvatar.Player.name;
            _txtLevel.CurrentText.text = PlayerAvatar.Player.level.ToString();
        }

        private void OnClickAdd()
        {
            onClick.Invoke(this);
        }

        private void OnClickRemove()
        {
        }
    }
}
