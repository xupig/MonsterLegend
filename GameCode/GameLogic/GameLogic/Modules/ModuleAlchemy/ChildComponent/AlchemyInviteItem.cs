﻿#region 模块信息
/*==========================================
// 文件名：AlchemyInviteItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleAlchemy.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/13 17:18:43
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleAlchemy
{
    public class AlchemyInviteItem:KList.KListItemBase
    {

        private AlchemyFriendItem _friendItem;
        private AlchemyGuildItem _guildItem;
        protected override void Awake()
        {
            _friendItem = AddChildComponent<AlchemyFriendItem>("Container_friend");
            _guildItem = AddChildComponent<AlchemyGuildItem>("Container_guild");
        }

        public override void Dispose()
        {
            
        }

        public override object Data
        {
            get
            {
                return base.Data;
            }
            set
            {
               
                if(value != null)
                {
                    Visible = true;
                    if(value!=base.Data)
                    {
                        if(value is PbFriendAvatarInfo)
                        {
                            _friendItem.Visible = true;
                            _guildItem.Visible = false;
                            _friendItem.RefreshContent(value as PbFriendAvatarInfo);
                        }
                        else
                        {
                            _guildItem.Visible = true;
                            _friendItem.Visible = false;
                            _guildItem.RefreshContent(value as PbGuildMemberInfo);
                        }
                    }
                }
                else
                {
                    Visible = false;
                }
            }
        }



    }
}
