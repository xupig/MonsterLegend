﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/22 17:24:57
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using GameData;

namespace ModulePet
{
    public class PetModule : BaseModule
    {
        public override void Init()
        {
            AddPanel(PanelIdEnum.Pet, "Container_PetPanel", MogoUILayer.LayerUIPanel, "ModulePet.PetPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.PetStrengthen, "Container_PetStrengthen", MogoUILayer.LayerUIPanel, "ModulePet.PetStrengthenPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.PetPop, "Container_PetPopPanel", MogoUILayer.LayerUIPanel, "ModulePet.PetPopPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.PetGet, "Container_PetGetPanel", MogoUILayer.LayerUINotice, "ModulePet.PetGetPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.PetSkillTips, "Container_PetSkillTips", MogoUILayer.LayerUIToolTip, "ModulePet.PetSkillTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.PetAttribute, "Container_PetAttributePanel", MogoUILayer.LayerUIToolTip, "ModulePet.PetAttributePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        protected override void OnBeforeShowPanel(PanelIdEnum panelId)
        {
            if (panelId == PanelIdEnum.Pet)
            {
                pet_hole_helper.Init();
                pet_level_helper.Init();
                pet_star_helper.Init();
                pet_quality_helper.Init();
                pet_skill_helper.Init();
            }
            else if (panelId == PanelIdEnum.PetStrengthen)
            {
                pet_refresh_helper.Init();
                pet_refresh_cost_helper.Init();
                pet_refresh_limit_helper.Init();
            }
        }
    }
}
