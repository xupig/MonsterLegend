﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using GameLoader.Utils.Timer;
using MogoEngine.Events;
using Common.Events;
using Common.Utils;

namespace ModulePet
{
    public enum PetAddWay
    {
        call = 1,
        petCard = 2,
        petCardIntoPiece = 3,
    }

    public class PetGetPanel : BasePanel
    {
        private pet _pet;
        private uint _timerId = TimerHeap.INVALID_ID;

        private StateText _textTitle;
        private StateText _textName;
        private StateText _textPiece;
        private PetModelComponent _model;
        private KParticle _particle;
        private GameObject _goModel;
        private KButton _btnGuide;

        protected override void Awake()
        {
            _textTitle = GetChildComponent<StateText>("Label_txtBiaoti");
            _textName = GetChildComponent<StateText>("Label_txtPetName");
            _textPiece = GetChildComponent<StateText>("Label_txtBreakInfoPieces");

            CloseBtn = GetChildComponent<KButton>("Button_zhezhao");
            _btnGuide = GetChildComponent<KButton>("Button_zhiyin");
            _model = AddChildComponent<PetModelComponent>("Container_pet/Container_model");
            _model.Position = new Vector3(155f, -356f, -200f);
            _model.Scale = 300f;
            _goModel = GetChild("Container_pet/Container_model");
            _particle = AddChildComponent<KParticle>("Container_pet/fx_ui_21_5_zhaohuan_01");
            _particle.transform.localPosition = new Vector3(30f, -80f, 0f);

            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.PetGet; }
        }

        public override void OnShow(object data)
        {
            PbCreatePetInfo pbCreatePetInfo = data as PbCreatePetInfo;
            int petId = pbCreatePetInfo.pet_id;
            pet pet = pet_helper.GetConfig(petId);
            _pet = pet;
            RefreshTitle(pet, (PetAddWay)pbCreatePetInfo.add_way);
            RefreshName(pet);
            RefreshPiece(pet, (PetAddWay)pbCreatePetInfo.add_way);
            _goModel.SetActive(false);
            _particle.Play();
            _timerId = TimerHeap.AddTimer(1200, 0, ShowModel);
            _btnGuide.onClick.AddListener(ClosePanel);
        }

        private void ShowModel()
        {
            _timerId = TimerHeap.INVALID_ID;
            _goModel.SetActive(true);
            _model.Refresh(pet_quality_helper.GetModelID(_pet.__id, _pet.__init_quality));
            EventDispatcher.TriggerEvent(PetEvents.GetPanelShowPetModel);
        }

        private void RefreshTitle(pet pet, PetAddWay petAddWay)
        {
            string name = "";
            if (PetAddWay.petCardIntoPiece == petAddWay)
            {
                ItemData itemData = pet_helper.GetExchangeItemData(pet);
                name = itemData.Name;
            }
            else
            {
                name = pet_helper.GetName(pet);
            }
            _textTitle.CurrentText.text = MogoLanguageUtil.GetContent(43460, name);
        }

        private void RefreshName(pet pet)
        {
            _textName.CurrentText.text = pet_helper.GetName(pet);
        }

        private void RefreshPiece(pet pet, PetAddWay petAddWay)
        {
            if (PetAddWay.petCardIntoPiece == petAddWay)
            {
                ItemData itemData = pet_helper.GetExchangeItemData(pet);
                _textPiece.CurrentText.text = MogoLanguageUtil.GetContent(43461,
                    ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GOLDEN, itemData.StackCount.ToString()),
                    ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GOLDEN, itemData.Name));
            }
            else
            {
                _textPiece.Clear();
            }
        }

        public override void OnClose()
        {
            if (_timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timerId);
            }
            _btnGuide.onClick.RemoveListener(ClosePanel);
            EventDispatcher.TriggerEvent(PetEvents.OnPetGetPanelClose);
        }
    }
}
