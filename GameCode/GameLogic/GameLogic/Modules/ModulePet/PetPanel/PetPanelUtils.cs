﻿using Common.Data;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public static class PetPanelUtils
    {
        public static PetFightingView.RightComponent GetRightComponent(PetFightingGridDataWrapper wrapper)
        {
            if (wrapper.PetInfo != null)
            {
                return PetFightingView.RightComponent.detail;
            }
            else
            {
                if (PlayerDataManager.Instance.PetData.IdlePetList.Count > 0)
                {
                    return PetFightingView.RightComponent.optionalList;
                }
            }
            return PetFightingView.RightComponent.getWay;
        }
    }
}
