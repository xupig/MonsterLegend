﻿using ACTSystem;
using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.Asset;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetModelComponent : ModelComponent
    {
        public Vector3 Position;
        public float Scale = 200f;

        protected override void Awake()
        {
            base.Awake();
            DragComponent.Dragable = false;
        }

        public void Refresh(int actorId)
        {
            LoadModel(actorId, (obj) =>
            {
                Transform transform = obj.gameObject.transform;
                transform.localPosition = Position;
                transform.localEulerAngles = new Vector3(0, 180f, 0f);
                transform.localScale = new Vector3(Scale, Scale, Scale);
            });
        }
    }
}
