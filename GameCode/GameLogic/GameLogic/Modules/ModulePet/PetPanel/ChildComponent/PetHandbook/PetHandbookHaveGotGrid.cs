﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetHandbookHaveGotGrid : PetBaseGrid
    {
        private PetHaveGotGridDataWrapper _wrapper;

        private PetHandbookHaveGotComponent _haveGot;

        protected override void Awake()
        {
            _haveGot = AddGridComponent<PetHandbookHaveGotComponent>();
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                _wrapper = value as PetHaveGotGridDataWrapper;
                _haveGot.Data = _wrapper;
                RefreshComponentList();
            }
        }
    }
}
