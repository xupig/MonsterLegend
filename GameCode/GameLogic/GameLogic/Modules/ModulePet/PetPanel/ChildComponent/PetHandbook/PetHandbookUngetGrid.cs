﻿using Common.Global;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetHandbookUngetGrid : PetBaseGrid
    {
        private PetHandbookUngetComponent _unget;

        protected override void Awake()
        {
            _unget = AddGridComponent<PetHandbookUngetComponent>();
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                pet pet = value as pet;
                _unget.Data = pet;
                RefreshComponentList();
            }
        }

    }
}
