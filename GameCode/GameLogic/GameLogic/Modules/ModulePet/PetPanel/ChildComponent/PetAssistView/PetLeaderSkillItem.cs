﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/5 23:34:46
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using UnityEngine;
using Common.ExtendComponent;

namespace ModulePet
{
    public class PetLeaderSkillItem : KList.KListItemBase
    {
        private StateText _textSkillName;
        private StateText _textSkillDesc;
        private IconContainer _containerIcon;

        protected override void Awake()
        {
            _textSkillName = GetChildComponent<StateText>("Label_txtJinengmingcheng");
            _textSkillDesc = GetChildComponent<StateText>("Label_txtJinengmiaoshu");
            _containerIcon = AddChildComponent<IconContainer>("Container_icon");
        }

        public override object Data
        {
            set
            {
                int buffId = (int) value;
                Refresh(buffId);
            }
        }

        private void Refresh(int buffId)
        {
            _textSkillName.CurrentText.text = buff_helper.GetName(buffId);
            KeyValuePair<int, float> kvp = buff_helper.GetAttriKvp(buffId);
            _textSkillDesc.CurrentText.text = MogoLanguageUtil.GetContent(buff_helper.GetDescId(buffId), attri_config_helper.GetAttributeName(kvp.Key), kvp.Value * 100);
            _containerIcon.SetIcon(buff_helper.GetIcon(buffId));
        }

        public override void Dispose()
        {
        }
    }
}
