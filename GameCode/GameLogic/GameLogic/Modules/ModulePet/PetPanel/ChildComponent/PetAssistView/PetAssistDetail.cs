﻿using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetAssistDetail : KContainer
    {
        private GameObject _goAttribute;
        private KList _attributeList;

        private GameObject _goLeaderSkill;
        private KScrollView _leaderSkillScrollView;
        private KList _leaderSkillList;

        protected override void Awake()
        {
            InitAttributeList();
            InitLeaderSkill();
        }

        private void InitAttributeList()
        {
            _goAttribute = GetChild("Container_shuxing");
            _attributeList = GetChildComponent<KList>("Container_shuxing/List_content");
            _attributeList.SetDirection(KList.KListDirection.LeftToRight, 2, int.MaxValue);
            _attributeList.SetGap(22, 0);
        }

        private void InitLeaderSkill()
        {
            _goLeaderSkill = GetChild("Container_duizhangjineng");
            _leaderSkillScrollView = GetChildComponent<KScrollView>("Container_duizhangjineng/ScrollView_duizhangjineng");
            _leaderSkillList = GetChildComponent<KList>("Container_duizhangjineng/ScrollView_duizhangjineng/mask/content");
            _leaderSkillList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _leaderSkillList.SetGap(2, 0);
            _leaderSkillList.SetPadding(1, 0, 1, 0);
        }

        public void Refresh(PetInfo petInfo)
        {
            RefreshSkill(petInfo);
            RefreshAttri();
        }

        private void RefreshSkill(PetInfo petInfo)
        {
            _leaderSkillList.SetDataList<PetLeaderSkillItem>(pet_quality_helper.GetLeaderSkill(petInfo.Id, petInfo.Quality));
        }

        private void RefreshAttri()
        {
            PetData petData = PlayerDataManager.Instance.PetData;
            List<RoleAttributeData> list = PetUtils.GetSumAttributeDataList(petData.AssistPetList);
            List<IAttribute> ilist = pet_star_helper.ParseUpLowbound(list);
            _attributeList.SetDataList<AttributeItem>(ilist);
        }
    }
}
