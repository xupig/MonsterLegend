﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/27 10:34:06
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using GameData;

namespace ModulePet
{
    public class PetAssistGrid : PetBaseGrid
    {
        private PetInfoComponent _info;
        private PetFightingAssistShareComponent _share;
        private PetAssistComponent _assist;

        protected override void Awake()
        {
            _info = AddGridComponent<PetInfoComponent>();
            _share = AddGridComponent<PetFightingAssistShareComponent>();
            _assist = AddGridComponent<PetAssistComponent>();
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                PetAssistGridDataWrapper wrapper = value as PetAssistGridDataWrapper;
                PetInfo petInfo = value as PetInfo;
                _info.Data = wrapper.PetInfo;
                _share.Data = wrapper;
                _assist.Data = wrapper;
                RefreshComponentList();
            }
        }

        public void ShowSelectFrame()
        {
            _share.ShowSelectFrame();
        }

        public void HideSelectFrame()
        {
            _share.HideSelectFrame();
        }
    }
}
