﻿using Common.Data;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetAssistOptionalGrid : PetBaseGrid
    {
        private PetInfoComponent _info;
        private PetAssistOptionalComponent _optional;

        protected override void Awake()
        {
            _info = AddGridComponent<PetInfoComponent>();
            _optional = AddGridComponent<PetAssistOptionalComponent>();
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                PetInfo petInfo = value as PetInfo;
                _info.Data = petInfo;
                _optional.Data = petInfo;
                RefreshComponentList();
            }
        }
    }
}
