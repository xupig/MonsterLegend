﻿using Common.Data;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetAssistOptionalList : KContainer
    {
        private KScrollView _scrollView;
        private KList _list;

        protected override void Awake()
        {
            _scrollView = GetChildComponent<KScrollView>("ScrollView_daimingliebiao");
            _list = GetChildComponent<KList>("ScrollView_daimingliebiao/mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _list.SetGap(-1, 0);
        }

        public void Refresh()
        {
            _scrollView.ResetContentPosition();
            PetData petData = PlayerDataManager.Instance.PetData;
            _list.SetDataList<PetAssistOptionalGrid>(petData.GetSortedIdlePetInfoList());
        }
    }
}
