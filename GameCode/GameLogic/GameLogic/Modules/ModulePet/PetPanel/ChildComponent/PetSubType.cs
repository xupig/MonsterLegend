﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/27 10:45:54
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Game.UI.UIComponent;
using UnityEngine;

namespace ModulePet
{
    public class PetSubType : KContainer
    {
        private StateImage _goDefence;
        private StateImage _goMagicAttack;
        private StateImage _goPhysicsAttack;
        private StateImage _goAssist;

        protected override void Awake()
        {
            _goDefence = GetChildComponent<StateImage>("Image_fangyu");
            _goMagicAttack = GetChildComponent<StateImage>("Image_mogong");
            _goPhysicsAttack = GetChildComponent<StateImage>("Image_wugong");
            _goAssist = GetChildComponent<StateImage>("Image_fuzhu");
        }

        public void SetSubType(PetSubTypeDefine subType)
        {
            HideAll();
            switch (subType)
            {
            case PetSubTypeDefine.defence:
                _goDefence.Alpha = 1;
                break;
            case PetSubTypeDefine.magicAttack:
                _goMagicAttack.Alpha = 1;
                break;
            case PetSubTypeDefine.physicsAttack:
                _goPhysicsAttack.Alpha = 1;
                break;
            case PetSubTypeDefine.assist:
                _goAssist.Alpha = 1;
                break;
            }
        }

        public void HideAll()
        {
            _goDefence.Alpha = 0;
            _goMagicAttack.Alpha = 0;
            _goPhysicsAttack.Alpha = 0;
            _goAssist.Alpha = 0;
        }
    }
}
