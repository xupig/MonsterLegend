﻿using Common.Data;

namespace ModulePet
{
    public class PetFightingGrid : PetBaseGrid
    {
        private PetInfoComponent _info;
        private PetFightingAssistShareComponent _share;
        private PetFightingComponent _fighting;

        protected override void Awake()
        {
            _info = AddGridComponent<PetInfoComponent>();
            _share = AddGridComponent<PetFightingAssistShareComponent>();
            _fighting = AddGridComponent<PetFightingComponent>();
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                PetFightingGridDataWrapper wrapper = value as PetFightingGridDataWrapper;
                PetInfo petInfo = value as PetInfo;
                _info.Data = wrapper.PetInfo;
                _share.Data = wrapper;
                _fighting.SetData(Index, wrapper.PetInfo);
                RefreshComponentList();
            }
        }

        public void ShowSelectFrame()
        {
            _share.ShowSelectFrame();
        }

        public void HideSelectFrame()
        {
            _share.HideSelectFrame();
        }
    }
}
