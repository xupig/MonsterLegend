﻿using Common.ExtendComponent;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetFightingDetailButton : KContainer
    {
        private KButton _btnAdd;
        private KButton _btnMask;
        private KParticle _particle;

        public KComponentEvent onClick = new KComponentEvent();

        protected override void Awake()
        {
            _btnAdd = GetChildComponent<KButton>("Button_tianjia");
            _btnMask = GetChildComponent<KButton>("Button_zhezhao");
            _particle = AddChildComponent<KParticle>("Button_tianjia/fx_ui_21_1_fangxinghuanrao");
            _particle.Stop();

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnAdd.onClick.AddListener(OnClick);
            _btnMask.onClick.AddListener(OnClick);
        }

        private void RemoveListener()
        {
            _btnAdd.onClick.RemoveListener(OnClick);
            _btnMask.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            onClick.Invoke();
        }

        public void PlayParticle()
        {
            _particle.Play(true);
        }

        public void StopParticle()
        {
            _particle.Stop();
        }
    }
}
