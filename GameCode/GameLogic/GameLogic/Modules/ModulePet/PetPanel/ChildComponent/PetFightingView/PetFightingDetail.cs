﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetFightingDetail : KContainer
    {
        private PetInfo _petInfo;

        private PetData _petData
        {
            get
            {
                return PlayerDataManager.Instance.PetData;
            }
        }

        private PetModelComponent _model;

        private PetFightingDetailButton _btnLevel;
        private PetFightingDetailButton _btnQuality;
        private PetFightingDetailButton _btnStar;
        private PetFightingDetailButton _btnWash;
        private KButton _btnSetFirstPosition;
        private KButton _btnRest;
        private KButton _btnShowAttribute;
        private KButton _btnShowFightDetail;
        private KDummyButton _btnShowFightDetail1;

        private StateText _textName;
        private StateImage _imageFrame;
        private ArtNumberList _artNumberList;
        private KList _listAttribute;
        private StateText _textLevel;
        private StateText _textQuality;
        private StarList _starList;
        private StateText _textWash;

        public KComponentEvent onShowFightDetail = new KComponentEvent();

        protected override void Awake()
        {
            InitPetAttribute();
            InitButton();
            InitModel();

            AddListener();
        }

        private void InitPetAttribute()
        {
            _listAttribute = GetChildComponent<KList>("List_content");
            _listAttribute.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _listAttribute.SetGap(8, 0);
            _textName = GetChildComponent<StateText>("Label_txtNianshouchongwu");
            _imageFrame = GetChildComponent<StateImage>("Image_shangchengpinzhikuang");
            _artNumberList = AddChildComponent<ArtNumberList>("List_zhandouli");
            _textLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengjishuzi");
            _textQuality = GetChildComponent<StateText>("Container_pinzhi/Label_txtpinzhise");
            _starList = AddChildComponent<StarList>("Container_xingxing");
            _starList.SetImageData(5, "Image_xingxing");
            _textWash = GetChildComponent<StateText>("Container_xilian/Label_txtpinzhise");
        }

        private void InitButton()
        {
            _btnLevel = AddChildComponent<PetFightingDetailButton>("Container_dengji");
            _btnQuality = AddChildComponent<PetFightingDetailButton>("Container_pinzhi");
            _btnStar = AddChildComponent<PetFightingDetailButton>("Container_xingxing");
            _btnWash = AddChildComponent<PetFightingDetailButton>("Container_xilian");
            _btnRest = GetChildComponent<KButton>("Button_xiuxi");
            _btnShowAttribute = GetChildComponent<KButton>("Button_shuxing");
            _btnSetFirstPosition = GetChildComponent<KButton>("Button_xianfeng");
            _btnShowFightDetail = GetChildComponent<KButton>("Button_Gantang");
            _btnShowFightDetail1 = AddChildComponent<KDummyButton>("Label_txtZhuanghua");
            //TODO
            _btnSetFirstPosition.Visible = false;
        }

        private void InitModel()
        {
            _model = gameObject.AddComponent<PetModelComponent>();
            _model.Scale = 200f;
            _model.Position = new Vector3(150f, -315f, -500f);
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnLevel.onClick.AddListener(OnLevelUp);
            _btnQuality.onClick.AddListener(OnQualityUp);
            _btnStar.onClick.AddListener(OnStarUp);
            _btnWash.onClick.AddListener(OnWash);
            _btnRest.onClick.AddListener(OnRest);
            _btnShowAttribute.onClick.AddListener(OnShowAttribute);
            _btnShowFightDetail.onClick.AddListener(OnShowFightDetail);
            _btnShowFightDetail1.onClick.AddListener(OnShowFightDetail);
        }

        private void RemoveListener()
        {
            _btnLevel.onClick.RemoveListener(OnLevelUp);
            _btnQuality.onClick.RemoveListener(OnQualityUp);
            _btnStar.onClick.RemoveListener(OnStarUp);
            _btnWash.onClick.RemoveListener(OnWash);
            _btnRest.onClick.RemoveListener(OnRest);
            _btnShowAttribute.onClick.RemoveListener(OnShowAttribute);
            _btnShowFightDetail.onClick.RemoveListener(OnShowFightDetail);
            _btnShowFightDetail1.onClick.RemoveListener(OnShowFightDetail);
        }

        private void OnShowFightDetail()
        {
            onShowFightDetail.Invoke();
        }

        private void OnLevelUp()
        {
            PetStrengthenPanelDataWrapper wrapper = new PetStrengthenPanelDataWrapper(PetStrengthen.levelUp, _petInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.PetStrengthen, wrapper);
        }

        private void OnQualityUp()
        {
            PetStrengthenPanelDataWrapper wrapper = new PetStrengthenPanelDataWrapper(PetStrengthen.qualityUp, _petInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.PetStrengthen, wrapper);
        }

        private void OnStarUp()
        {
            PetStrengthenPanelDataWrapper wrapper = new PetStrengthenPanelDataWrapper(PetStrengthen.starUp, _petInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.PetStrengthen, wrapper);
        }

        private void OnWash()
        {
            if (!pet_refresh_limit_helper.IsPetWashUnlock(_petInfo))
            {
                ShowWashLockCondition();
                return;
            }
            PetStrengthenPanelDataWrapper wrapper = new PetStrengthenPanelDataWrapper(PetStrengthen.wash, _petInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.PetStrengthen, wrapper);
        }

        private void ShowWashLockCondition()
        {
            pet_refresh_limit refreshLimit = pet_refresh_limit_helper.GetPetRefreshLimit(PetWashType.total);
            string content = MogoLanguageUtil.GetContent(43463);
            string condition = string.Empty;
            if (PlayerAvatar.Player.vip_level < refreshLimit.__vip)
            {
                condition = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, MogoLanguageUtil.GetContent(43464, refreshLimit.__vip));
                content = string.Concat(content, "\n", condition);
            }
            int minStar = pet_refresh_limit_helper.GetMinStar();
            if (_petInfo.Star < minStar)
            {
                condition = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, MogoLanguageUtil.GetContent(43465, minStar));
                content = string.Concat(content, "\n", condition);
            }
            if (_petInfo.Quality < refreshLimit.__quality)
            {
                condition = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, 
                    MogoLanguageUtil.GetContent(43466, pet_quality_helper.GetPetQualityString(_petInfo.Id, refreshLimit.__quality)));
                content = string.Concat(content, "\n", condition);
            }
            MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, true);
        }

        private void OnRest()
        {
            PetManager.Instance.RequestPetIdle(_petInfo.Id);
        }

        private void OnShowAttribute()
        {
            PetPopPanelDataWrapper wrapper = new PetPopPanelDataWrapper(PetPopType.gotPetInfo, PetContext.fighting, _petInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.PetPop, wrapper);
        }

        public void SetData(PetInfo petInfo)
        {
            _petInfo = petInfo;
            RefreshName();
            RefreshFrame();
            RefreshFightValue();
            RefreshModel();
            RefreshLevel();
            RefreshQuality();
            RefreshStar();
            RefreshWash();
            RefreshAttributeList();
        }

        private void RefreshName()
        {
            _textName.CurrentText.text = pet_quality_helper.GetPetQualityName(_petInfo);
        }

        private void RefreshFrame()
        {
            MogoShaderUtils.ChangeShader(_imageFrame.CurrentImage, MogoShaderUtils.IconShaderPath);
            _imageFrame.CurrentImage.color = ColorDefine.GetColorById(pet_quality_helper.GetNameColorId(_petInfo.Id, _petInfo.Quality));
        }

        private void RefreshFightValue()
        {
            List<RoleAttributeData> list = _petData.GetAttributeDataList(_petInfo);
            //宠物用战士职业来计算战斗力
            _artNumberList.SetNumber((int)fight_force_helper.GetFightForce(list, Vocation.Warrior));
        }

        private void RefreshModel()
        {
            _model.Refresh(pet_quality_helper.GetModelID(_petInfo));
        }

        private void RefreshLevel()
        {
            _textLevel.CurrentText.text = _petInfo.Level.ToString();
            if (_petInfo.CanLevelUp())
            {
                _btnLevel.PlayParticle();
            }
            else
            {
                _btnLevel.StopParticle();
            }
        }

        private void RefreshQuality()
        {
            _textQuality.CurrentText.text = pet_quality_helper.GetPetQualityString(_petInfo.Id, _petInfo.Quality);
            if (_petInfo.CanQualityUp())
            {
                _btnQuality.PlayParticle();
            }
            else
            {
                _btnQuality.StopParticle();
            }
        }

        private void RefreshStar()
        {
            _starList.SetImageNum(_petInfo.Star);
            if (_petInfo.CanStarUp())
            {
                _btnStar.PlayParticle();
            }
            else
            {
                _btnStar.StopParticle();
            }
        }

        private void RefreshWash()
        {
            _textWash.CurrentText.text = _petInfo.WashLevel.ToString();
            if (_petInfo.CanWash())
            {
                _btnWash.PlayParticle();
            }
            else
            {
                _btnWash.StopParticle();
            }
        }

        private void RefreshAttributeList()
        {
            List<RoleAttributeData> list = _petData.GetAttributeDataList(_petInfo);
            List<IAttribute> ilist = pet_star_helper.ParseUpLowbound(list);
            _listAttribute.SetDataList<PetStrengthAttributeItem>(ilist, 1);
        }
    }
}
