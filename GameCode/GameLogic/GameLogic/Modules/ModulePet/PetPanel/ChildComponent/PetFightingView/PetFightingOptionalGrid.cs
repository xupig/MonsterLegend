﻿using Common.Data;

namespace ModulePet
{
    public class PetFightingOptionalGrid : PetBaseGrid
    {
        private PetInfoComponent _info;
        private PetFightingOptionalComponent _optional;

        protected override void Awake()
        {
            _info = AddGridComponent<PetInfoComponent>();
            _optional = AddGridComponent<PetFightingOptionalComponent>();
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                PetInfo petInfo = value as PetInfo;
                _info.Data = petInfo;
                _optional.Data = petInfo;
                RefreshComponentList();
            }
        }
    }
}
