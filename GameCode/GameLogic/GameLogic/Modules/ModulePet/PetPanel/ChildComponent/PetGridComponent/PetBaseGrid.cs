﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public abstract class PetBaseGrid : KList.KListItemBase
    {
        private List<PetBaseGridComponent> _componentList = new List<PetBaseGridComponent>();

        protected T AddGridComponent<T>() where T : PetBaseGridComponent
        {
            T component = gameObject.AddComponent<T>();
            _componentList.Add(component);
            return component;
        }

        protected void RefreshComponentList()
        {
            for (int i = 0; i < _componentList.Count; i++)
            {
                _componentList[i].Refresh();
            }
        }
    }
}
