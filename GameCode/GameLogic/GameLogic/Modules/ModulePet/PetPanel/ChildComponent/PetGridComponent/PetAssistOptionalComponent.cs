﻿using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetAssistOptionalComponent : PetBaseGridComponent
    {
        private PetInfo _petInfo;

        private KButton _btnFight;

        protected override void Awake()
        {
            _btnFight = GetChildComponent<KButton>("Button_shangzhen");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnFight.onClick.AddListener(OnFight);
        }

        private void RemoveListener()
        {
            _btnFight.onClick.RemoveListener(OnFight);
        }

        public override object Data
        {
            set
            {
                _petInfo = value as PetInfo;
            }
        }

        protected virtual void OnFight()
        {
            EventDispatcher.TriggerEvent<PetInfo>(PetEvents.RequestIdleToAssist, _petInfo);
        }

    }
}
