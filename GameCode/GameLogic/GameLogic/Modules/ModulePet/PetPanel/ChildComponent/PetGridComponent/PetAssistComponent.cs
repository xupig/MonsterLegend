﻿using Common.Data;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager.SubSystem;

namespace ModulePet
{
    public class PetAssistComponent : PetBaseGridComponent
    {
        private PetInfo _petInfo;
        private pet_hole _petHole;

        private StateImage _imagePosition;
        private StateText _textPosition;
        private KButton _btnPutDown;

        protected override void Awake()
        {
            _imagePosition = GetChildComponent<StateImage>("Image_duizhang");
            _textPosition = GetChildComponent<StateText>("Label_txtDuizhang");
            _btnPutDown = GetChildComponent<KButton>("Button_xiexia");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnPutDown.onClick.AddListener(OnPutDown);
        }

        private void RemoveListener()
        {
            _btnPutDown.onClick.RemoveListener(OnPutDown);
        }

        private void OnPutDown()
        {
            PetManager.Instance.RequestAssistPetIdle(_petInfo.Id);
        }

        public override object Data
        {
            set
            {
                PetAssistGridDataWrapper wrapper = value as PetAssistGridDataWrapper;
                _petInfo = wrapper.PetInfo;
                _petHole = wrapper.PetHole;
            }
        }

        public override void Refresh()
        {
            RefreshPosition();
            RefreshBtn();
        }

        private void RefreshPosition()
        {
            if (_petInfo != null)
            {
                _textPosition.CurrentText.text = pet_hole_helper.GetDesc(_petHole);
                _imagePosition.Alpha = 1;
                return;
            }

            _textPosition.Clear();
            _imagePosition.Alpha = 0;
        }

        private void RefreshBtn()
        {
            if (_petInfo != null)
            {
                _btnPutDown.Visible = true;
            }
            else
            {
                _btnPutDown.Visible = false;
            }
        }
    }
}
