﻿using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetInfoComponent : PetConfigComponent
    {
        protected PetInfo _petInfo;

        protected StateText _textLevel;
        protected StarList _starList;

        protected override void Awake()
        {
            base.Awake();
            _textLevel = GetChildComponent<StateText>("Label_txtDengji");
            _starList = AddChildComponent<StarList>("Container_xingxing");
            _starList.SetImageData(5, "Image_xingxing");
        }

        protected override void OnDestroy()
        {
            _petInfo = null;
            base.OnDestroy();
        }

        public override object Data
        {
            set
            {
                _petInfo = value as PetInfo;
                base.Data = _petInfo != null ? pet_helper.GetConfig(_petInfo.Id) : null;
            }
        }

        public override void Refresh()
        {
            if (_petInfo != null)
            {
                RefreshIcon(pet_helper.GetIcon(_petInfo.Id), pet_quality_helper.GetNameColorId(_petInfo.Id, _petInfo.Quality));
                RefreshName(_petInfo.Id, _petInfo.Quality);
                RefreshSubType(_petInfo.Id);
                RefreshLevel(_petInfo.Level);
                RefreshStar(_petInfo.Star);
            }
            else
            {
                ClearAll();
            }
        }

        protected override void ClearAll()
        {
            base.ClearAll();
            ClearLevel();
            ClearStar();
        }

        protected virtual void RefreshLevel(int level)
        {
            _textLevel.CurrentText.text = string.Format("LV{0}", level);
        }

        protected void ClearLevel()
        {
            _textLevel.Clear();
        }

        protected virtual void RefreshStar(int star)
        {
            _starList.Visible = true;
            _starList.SetImageNum(star);
        }

        protected void ClearStar()
        {
            _starList.Visible = false;
        }
    }
}
