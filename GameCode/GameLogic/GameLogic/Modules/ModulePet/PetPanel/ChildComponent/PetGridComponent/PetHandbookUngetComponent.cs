﻿using Common.Data;
using Common.Global;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetHandbookUngetComponent : PetConfigComponent
    {
        private KProgressBar _progressBar;
        private StateText _textProgressBar;
        private StateText _textLevel;

        protected override void Awake()
        {
            base.Awake();
            _progressBar = GetChildComponent<KProgressBar>("ProgressBar_jindu");
            _textProgressBar = GetChildComponent<StateText>("Label_txtJingyan");
            _textLevel = GetChildComponent<StateText>("Label_txtDengji");
            SetGray();
        }

        private void SetGray()
        {
            _textProgressBar.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_GRAY);
            (GetChildComponent<StateImage>("Container_leixing/Image_fuzhu").CurrentImage as ImageWrapper).SetGray(0f);
            (GetChildComponent<StateImage>("Container_leixing/Image_wugong").CurrentImage as ImageWrapper).SetGray(0f);
            (GetChildComponent<StateImage>("Container_leixing/Image_mogong").CurrentImage as ImageWrapper).SetGray(0f);
            (GetChildComponent<StateImage>("Container_leixing/Image_fangyu").CurrentImage as ImageWrapper).SetGray(0f);
        }

        public override void Refresh()
        {
            base.Refresh();
            RefreshProgress();
        }

        private void RefreshProgress()
        {
            ItemData itemData = pet_helper.GetItemCost(_pet);
            int nowItemNum = PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemNum(itemData.Id);
            _progressBar.Value = nowItemNum / (float)itemData.StackCount;
            _textProgressBar.CurrentText.text = string.Format("{0}/{1}", nowItemNum, itemData.StackCount);
            _progressBar.Visible = true;
            _textLevel.Clear();
        }

        protected override void RefreshName(int id, int quality)
        {
            _goBackground.SetActive(true);
            _textName.CurrentText.text = pet_quality_helper.GetPetQualityName(id, quality);
            _textName.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_GRAY);
        }

        protected override void IconLoadCallback(GameObject obj)
        {
            ImageWrapper imageWrapper = obj.GetComponent<ImageWrapper>();
            imageWrapper.SetGray(0f);
        }
    }
}
