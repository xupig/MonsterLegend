﻿using Common.Data;
using Common.Utils;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetFightingComponent : PetBaseGridComponent
    {
        private int _index;
        private PetInfo _petInfo;

        private StateText _textOrder;
        private StateImage _imagePioneer;
        private StateImage _imageUpgrade;

        protected override void Awake()
        {
            _textOrder = GetChildComponent<StateText>("Label_txtDuizhang");
            _imagePioneer = GetChildComponent<StateImage>("Image_xianfeng");
            _imageUpgrade = GetChildComponent<StateImage>("Image_tisheng");
        }

        public void SetData(int Index, PetInfo petInfo)
        {
            _index = Index;
            _petInfo = petInfo;
        }

        public override void Refresh()
        {
            RefreshPioneer();
            RefreshOrder();
            RefreshUpgrade();
        }

        private void RefreshPioneer()
        {
            if (_index == 0)
            {
                _imagePioneer.Alpha = 1f;
            }
            else
            {
                _imagePioneer.Alpha = 0f;
            }
        }

        private void RefreshOrder()
        {
            if (_petInfo != null)
            {
                _textOrder.CurrentText.text = MogoLanguageUtil.GetContent(43462, _index + 1);
                return;
            }
            _textOrder.Clear();
        }

        private void RefreshUpgrade()
        {
            if (_petInfo == null)
            {
                _imageUpgrade.Alpha = 0f;
                return;
            }
            if (_petInfo.CanStrength())
            {
                _imageUpgrade.Alpha = 1f;
            }
            else
            {
                _imageUpgrade.Alpha = 0f;
            }
        }
    }
}
