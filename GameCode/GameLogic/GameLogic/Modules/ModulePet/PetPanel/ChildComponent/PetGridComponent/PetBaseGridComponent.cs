﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public abstract class PetBaseGridComponent : KContainer
    {
        public virtual void Refresh() {}

        public virtual System.Object Data { get; set; }
    }
}
