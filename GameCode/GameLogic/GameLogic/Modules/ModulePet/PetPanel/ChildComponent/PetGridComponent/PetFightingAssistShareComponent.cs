﻿using Common.Data;
using Common.Global;
using Game.UI.UIComponent;
using GameData;
using UnityEngine;

namespace ModulePet
{
    public class PetFightingAssistShareComponent : PetBaseGridComponent
    {
        private pet_hole _petHole;
        private PetInfo _petInfo;

        private GameObject _goLock;
        private StateImage _imageAdd;
        private StateText _textUnlockLevel;
        private StateImage _imageSelectedFrame;

        protected override void Awake()
        {
            _goLock = GetChild("Static_sharedGridLock");
            _imageAdd = GetChildComponent<StateImage>("Image_sharedTianjia");
            _textUnlockLevel = GetChildComponent<StateText>("Label_txtjiesuo");
            _imageSelectedFrame = GetChildComponent<StateImage>("Image_xuanzhongkuang");
            _imageSelectedFrame.Visible = false;
        }

        public override object Data
        {
            set
            {
                PetFightingGridDataWrapper wrapper = value as PetFightingGridDataWrapper;
                _petInfo = wrapper.PetInfo;
                _petHole = wrapper.PetHole;
            }
        }

        public override void Refresh()
        {
            RefreshLock();
            RefreshAdd();
        }

        private void RefreshLock()
        {
            if (pet_hole_helper.IsLock(_petHole))
            {
                _goLock.SetActive(true);
                _textUnlockLevel.CurrentText.text = string.Format(LangEnum.PET_UNLOCK_LEVEL.ToLanguage(), _petHole.__avatar_level);
            }
            else
            {
                _goLock.SetActive(false);
                _textUnlockLevel.Clear();
            }
        }

        private void RefreshAdd()
        {
            if (pet_hole_helper.IsLock(_petHole))
            {
                _imageAdd.Visible = false;
            }
            else
            {
                if (_petInfo != null)
                {
                    _imageAdd.Visible = false;
                }
                else
                {
                    _imageAdd.Visible = true;
                }
            }
        }

        public void ShowSelectFrame()
        {
            _imageSelectedFrame.Visible = true;
        }

        public void HideSelectFrame()
        {
            _imageSelectedFrame.Visible = false;
        }
    }
}
