﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetHandbookHaveGotComponent : PetInfoComponent
    {
        private PetHaveGotGridDataWrapper _wrapper;

        private StateText _textState;
        private KButton _btnCall;
        private KParticle _particleCall;
        private KButton _btnStrength;
        private KParticle _particleStrength;

        protected override void Awake()
        {
            base.Awake();
            _textState = GetChildComponent<StateText>("Label_txtZhuangtai");
            _btnCall = GetChildComponent<KButton>("Button_zhaohuan");
            _particleCall = AddChildComponent<KParticle>("Button_zhaohuan/fx_ui_3_2_lingqu");
            _particleCall.transform.localScale = new Vector3(0.74f, 0.85f, 1f);
            _particleCall.transform.localPosition = new Vector3(0f, 0f, -1f);
            MaskEffectItem effectItem1 = gameObject.AddComponent<MaskEffectItem>();
            effectItem1.Init(_btnCall.gameObject);

            _btnStrength = GetChildComponent<KButton>("Button_peiyang");
            _particleStrength = AddChildComponent<KParticle>("Button_peiyang/fx_ui_3_2_lingqu");
            _particleStrength.transform.localScale = new Vector3(0.74f, 0.9f, 1f);
            _particleStrength.transform.localPosition = new Vector3(0f, 0f, -1f);
            MaskEffectItem effectItem2 = gameObject.AddComponent<MaskEffectItem>();
            effectItem2.Init(_btnStrength.gameObject, CanShowParticleStrength);
            _particleStrength.Stop();

            AddListener();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            RemoveListener();
        }

        private void AddListener()
        {
            _btnCall.onClick.AddListener(OnCall);
            _btnStrength.onClick.AddListener(OnStrength);
        }

        private void RemoveListener()
        {
            _btnCall.onClick.RemoveListener(OnCall);
            _btnStrength.onClick.RemoveListener(OnStrength);
        }

        private void OnCall()
        {
            PetManager.Instance.RequestCallPet(_wrapper.Pet.__id);
        }

        private void OnStrength()
        {
            PetStrengthenPanelDataWrapper wrapper = new PetStrengthenPanelDataWrapper(PetStrengthen.levelUp, _wrapper.PetInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.PetStrengthen, wrapper);
        }

        public override object Data
        {
            set
            {
                _wrapper = value as PetHaveGotGridDataWrapper;
            }
        }

        public override void Refresh()
        {
            RefreshIcon(pet_helper.GetIcon(_wrapper.GetId()), pet_quality_helper.GetNameColorId(_wrapper.GetId(), _wrapper.GetQuality()));
            RefreshName(_wrapper.GetId(), _wrapper.GetQuality());
            RefreshSubType(_wrapper.GetId());
            RefreshLevel(_wrapper.GetLevel());
            if (_wrapper.PetInfo != null)
            {
                RefreshStar(_wrapper.GetStar());
            }
            else
            {
                ClearStar();
            }
            RefreshState();
            RefreshCallBtn();
            RefreshStrengthBtn();
        }

        private void RefreshState()
        {
            PetInfo petInfo = _wrapper.PetInfo;
            if (petInfo != null && petInfo.State == PetState.fighting)
            {
                _textState.CurrentText.text = LangEnum.PET_STATE_FIGHT.ToLanguage();
            }
            else if (petInfo != null && petInfo.State == PetState.assist)
            {
                _textState.CurrentText.text = string.Format(LangEnum.PET_STATE.ToLanguage(), pet_hole_helper.GetDesc(petInfo.Position + 1));
            }
            else
            {
                _textState.Clear();
            }
        }

        private void RefreshCallBtn()
        {
            _btnCall.Visible = _wrapper.PetInfo == null;
        }

        private void RefreshStrengthBtn()
        {
            if (CanShowParticleStrength())
            {
                _particleStrength.Play(true);
            }
            else
            {
                _particleStrength.Stop();
            }
        }

        private bool CanShowParticleStrength()
        {
            PetInfo petInfo = _wrapper.PetInfo;
            if (petInfo != null && petInfo.CanStrength())
            {
                return true;
            }
            return false;
        }
    }
}
