﻿using Common.Data;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetFightingOptionalComponent : PetBaseGridComponent
    {
        private const int ADD_IN_END = -1;

        private PetInfo _petInfo;

        private KButton _btnFight;

        protected override void Awake()
        {
            _btnFight = GetChildComponent<KButton>("Button_shangzhen");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnFight.onClick.AddListener(OnFight);
        }

        private void RemoveListener()
        {
            _btnFight.onClick.RemoveListener(OnFight);
        }

        private void OnFight()
        {
            PetManager.Instance.RequestPetFight(_petInfo.Id, ADD_IN_END);
        }

        public override object Data
        {
            set
            {
                _petInfo = value as PetInfo;
            }
        }
    }
}
