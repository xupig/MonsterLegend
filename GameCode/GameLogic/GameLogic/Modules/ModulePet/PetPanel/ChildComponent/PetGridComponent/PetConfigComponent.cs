﻿using Common.ExtendComponent;
using Common.Global;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetConfigComponent : PetBaseGridComponent
    {
        protected pet _pet;

        protected ItemGrid _itemGrid;
        protected StateText _textName;
        protected PetSubType _subType;
        protected GameObject _goBackground;

        protected override void Awake()
        {
            _goBackground = GetChild("ScaleImage_txtBg");
            _textName = GetChildComponent<StateText>("Label_txtCongwumingcheng");
            _subType = AddChildComponent<PetSubType>("Container_leixing");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _itemGrid.SetGridBtnEnable(false);
        }

        protected override void OnDestroy()
        {
            _pet = null;
        }

        public override object Data
        {
            set
            {
                _pet = value as pet;
            }
        }

        public override void Refresh()
        {
            if (_pet != null)
            {
                RefreshIcon(pet_helper.GetIcon(_pet.__id), pet_quality_helper.GetNameColorId(_pet.__id, _pet.__init_quality));
                RefreshName(_pet.__id, _pet.__init_quality);
                RefreshSubType(_pet.__id);
            }
            else
            {
                ClearAll();
            }
        }

        protected virtual void ClearAll()
        {
            ClearIcon();
            ClearName();
            ClearSubType();
        }

        protected virtual void RefreshIcon(int id, int quality)
        {
            _itemGrid.SetLoadCallback(IconLoadCallback);
            _itemGrid.SetItemData(id, quality);
        }

        protected virtual void IconLoadCallback(GameObject obj)
        {
        }

        protected void ClearIcon()
        {
            _itemGrid.Clear();
        }

        protected virtual void RefreshName(int id, int quality)
        {
            _goBackground.SetActive(true);
            Color color = ColorDefine.GetColorById(pet_quality_helper.GetNameColorId(id, quality));
            _textName.CurrentText.text = pet_quality_helper.GetPetQualityName(id, quality);
            _textName.CurrentText.color = color;
        }

        protected void ClearName()
        {
            _goBackground.SetActive(false);
            _textName.Clear();
        }

        protected virtual void RefreshSubType(int id)
        {
            _subType.SetSubType(pet_helper.GetSubType(id));
        }

        protected void ClearSubType()
        {
            _subType.HideAll();
        }
    }
}
