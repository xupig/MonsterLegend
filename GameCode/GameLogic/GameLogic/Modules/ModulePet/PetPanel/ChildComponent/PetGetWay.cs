﻿using Common.Base;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetGetWay : KContainer
    {
        private const int GET_WAY_NUM = 3;

        private int[] _functionIdArray;

        private List<GetWayButton> _btnList;

        protected override void Awake()
        {
            _btnList = new List<GetWayButton>();
            for (int i = 0; i < GET_WAY_NUM; i++)
            {
                _btnList.Add(AddChildComponent<GetWayButton>(string.Format("Button_anniu{0}", i)));
            }

            InitIcon();
        }

        private void InitIcon()
        {
            _functionIdArray = global_params_helper.StringToArray<int>(
                global_params_helper.GetGlobalParam(GlobalParamId.petGetWay), int.Parse);

            for (int i = 0; i < GET_WAY_NUM; i++)
            {
                if (i < _functionIdArray.Length)
                {
                    _btnList[i].Visible = true;
                    _btnList[i].SetFunctionId(_functionIdArray[i]);
                }
                else
                {
                    _btnList[i].Visible = false;
                }
            }
        }



        class GetWayButton : KContainer
        {
            private int _functionId;

            private StateIcon _stateIcon;
            private KButton _btn;

            protected override void Awake()
            {
                _btn = gameObject.GetComponent<KButton>();
                _stateIcon = GetChildComponent<StateIcon>("stateIcon");

                AddListener();
            }

            protected override void OnDestroy()
            {
                RemoveListener();
            }

            private void AddListener()
            {
                _btn.onClick.AddListener(OnClick);
            }

            private void RemoveListener()
            {
                _btn.onClick.RemoveListener(OnClick);
            }

            public void SetFunctionId(int functionId)
            {
                _functionId = functionId;
                _stateIcon.SetIcon(function_helper.GetIconId(functionId));
            }

            private void OnClick()
            {
                function functionData = function_helper.GetFunction(_functionId);
                if (activity_helper.IsOpen(functionData))
                {
                    function_helper.Follow(_functionId, null);
                }
                else
                {
                    MessageBox.Show(true, MessageBox.DEFAULT_TITLE, function_helper.GetConditionDesc(_functionId));
                }
            }
        }
    }
}
