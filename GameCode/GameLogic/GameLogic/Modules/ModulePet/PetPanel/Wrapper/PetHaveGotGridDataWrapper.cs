﻿using Common.Data;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetHaveGotGridDataWrapper
    {
        public PetInfo PetInfo;
        public pet Pet;

        public PetHaveGotGridDataWrapper(PetInfo petInfo)
        {
            this.PetInfo = petInfo;
            this.Pet = pet_helper.GetConfig(petInfo.Id);
        }

        public PetHaveGotGridDataWrapper(pet pet)
        {
            this.Pet = pet;
        }

        public int GetId()
        {
            return Pet.__id;
        }

        public int GetQuality()
        {
            if (PetInfo != null)
            {
                return PetInfo.Quality;
            }
            return Pet.__init_quality;
        }

        public int GetLevel()
        {
            if (PetInfo != null)
            {
                return PetInfo.Level;
            }
            return Pet.__init_level;
        }

        public int GetStar()
        {
            if (PetInfo != null)
            {
                return PetInfo.Star;
            }
            return Pet.__init_star;
        }
    }
}
