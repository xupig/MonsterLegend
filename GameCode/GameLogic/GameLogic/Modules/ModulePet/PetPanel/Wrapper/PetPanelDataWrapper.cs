﻿using Common.Base;
using Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetPanelDataWrapper
    {
        public PanelIdEnum PanelId;
        public int PetPanelTabIndex;
        public int PetId;
        public PetStrengthen PetStrengthen;

        public PetPanelDataWrapper(int petPanelTabIndex)
        {
            this.PanelId = PanelIdEnum.Pet;
            this.PetPanelTabIndex = petPanelTabIndex;
        }

        public PetPanelDataWrapper(PanelIdEnum panelId, int petPanelTabIndex, int petId)
        {
            this.PanelId = panelId;
            this.PetPanelTabIndex = petPanelTabIndex;
            this.PetId = petId;
        }

        public PetPanelDataWrapper(PanelIdEnum panelId, PetStrengthen petStrengthen, int petId)
        {
            this.PanelId = panelId;
            this.PetStrengthen = petStrengthen;
            this.PetId = petId;
            this.PetPanelTabIndex = PetPanel.TAB_FIGHTING;
        }
    }
}
