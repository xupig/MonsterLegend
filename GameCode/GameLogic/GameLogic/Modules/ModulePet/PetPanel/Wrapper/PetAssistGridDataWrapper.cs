﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/27 11:10:32
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameData;
using Common.Data;

namespace ModulePet
{
    public class PetAssistGridDataWrapper : PetFightingGridDataWrapper
    {
        public PetAssistGridDataWrapper(pet_hole petHole, PetInfo petInfo)
            : base(petHole, petInfo) {}
    }
}
