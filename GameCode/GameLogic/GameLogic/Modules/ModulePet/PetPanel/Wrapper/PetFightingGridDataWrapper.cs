﻿using Common.Data;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetFightingGridDataWrapper
    {
        public pet_hole PetHole;
        public PetInfo PetInfo;

        public PetFightingGridDataWrapper(pet_hole petHole, PetInfo petInfo)
        {
            this.PetHole = petHole;
            this.PetInfo = petInfo;
        }
    }
}
