﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetFightingView : KContainer
    {
        //右侧组件
        public enum RightComponent
        {
            optionalList,
            detail,
            getWay
        }

        private int _currentIndex = 0;
        private List<PetFightingGridDataWrapper> _wrapperList;

        private GameObject _goFightingList;
        private KScrollView _scrollView;
        private KList _list;
        private PetGetWay _getWay;
        private PetFightingDetail _detail;
        private PetFightingOptionalList _optionalList;

        private PetData _petData
        {
            get { return PlayerDataManager.Instance.PetData; }
        }

        protected override void Awake()
        {
            InitFightingList();
            _getWay = AddChildComponent<PetGetWay>("Container_huoderuko");
            _detail = AddChildComponent<PetFightingDetail>("Container_chongwuzhanshi");
            _optionalList = AddChildComponent<PetFightingOptionalList>("Container_xuanzeliebiao");

            AddListener();
        }

        private void InitFightingList()
        {
            _goFightingList = GetChild("Container_congwuduizhang");
            _scrollView = GetChildComponent<KScrollView>("Container_congwuduizhang/ScrollView_zhuzhenmoling");
            _list = GetChildComponent<KList>("Container_congwuduizhang/ScrollView_zhuzhenmoling/mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _list.SetGap(-14, 0);
            _list.SetPadding(-2, 0, 0, 0);
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _list.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            _list.onAllItemCreated.AddListener(OnCoroutineEnd);
            _detail.onShowFightDetail.AddListener(OnShowFightDetail);
        }

        private void RemoveListener()
        {
            _list.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            _list.onAllItemCreated.RemoveListener(OnCoroutineEnd);
            _detail.onShowFightDetail.RemoveListener(OnShowFightDetail);
        }

        private void OnSelectedIndexChanged(KList list, int index)
        {
            _currentIndex = index;
            PetFightingGridDataWrapper wrapper = _wrapperList[_currentIndex];
            RefreshRightComponent(wrapper);
            RefreshSelectFrame();
        }

        private void OnShowFightDetail()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.PetAttribute);
        }

        public void SetData(List<PetInfo> petInfoList, bool doTween)
        {
            List<PetFightingGridDataWrapper> wrapperList = GetFightingGridDataWrapperList(petInfoList);
            _wrapperList = wrapperList;
            _list.SetDataList<PetFightingGrid>(wrapperList, 1);
            PetFightingGridDataWrapper wrapper = wrapperList[_currentIndex];
            RefreshRightComponent(wrapper);
            if (doTween)
            {
                DoTween();
            }
        }

        private void OnCoroutineEnd()
        {
            RefreshSelectFrame();
        }

        private void RefreshSelectFrame()
        {
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                PetFightingGrid grid = _list.ItemList[i] as PetFightingGrid;
                if (i == _currentIndex)
                {
                    grid.ShowSelectFrame();
                }
                else
                {
                    grid.HideSelectFrame();
                }
            }
        }

        private void RefreshRightComponent(PetFightingGridDataWrapper wrapper)
        {
            HideAllComponentType();
            RightComponent componentType = PetPanelUtils.GetRightComponent(wrapper);
            switch (componentType)
            {
                case RightComponent.detail:
                    _detail.Visible = true;
                    _detail.SetData(wrapper.PetInfo);
                    break;
                case RightComponent.getWay:
                    _getWay.Visible = true;
                    break;
                case RightComponent.optionalList:
                    _optionalList.Visible = true;
                    _optionalList.Refresh();
                    break;
            }
        }

        private void HideAllComponentType()
        {
            _detail.Visible = false;
            _getWay.Visible = false;
            _optionalList.Visible = false;
        }

        private List<PetFightingGridDataWrapper> GetFightingGridDataWrapperList(List<PetInfo> petInfoList)
        {
            List<PetFightingGridDataWrapper> wrapperList = new List<PetFightingGridDataWrapper>();
            List<pet_hole> petHoleList = pet_hole_helper.GetFightingPetHoldList();
            for (int i = 0; i < petHoleList.Count; i++)
            {
                pet_hole petHole = petHoleList[i];
                PetInfo petInfo = petInfoList.Find((p) => { return p.Position == (petHole.__index - 1); });
                PetFightingGridDataWrapper wrapper = new PetFightingGridDataWrapper(petHole, petInfo);
                wrapperList.Add(wrapper);
                if (petInfo == null)
                {
                    break;
                }
            }
            return wrapperList;
        }

        private void DoTween()
        {
            if (_goFightingList.activeInHierarchy) TweenViewMove.Begin(_goFightingList.gameObject, MoveType.Show, MoveDirection.Left);
            if (_getWay.gameObject.activeInHierarchy) TweenViewMove.Begin(_getWay.gameObject, MoveType.Show, MoveDirection.Right);
            if (_detail.gameObject.activeInHierarchy) TweenViewMove.Begin(_detail.gameObject, MoveType.Show, MoveDirection.Right);
            if (_optionalList.gameObject.activeInHierarchy) TweenViewMove.Begin(_optionalList.gameObject, MoveType.Show, MoveDirection.Right);
        }
    }
}
