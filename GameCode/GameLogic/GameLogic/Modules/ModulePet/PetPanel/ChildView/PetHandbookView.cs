﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/22 17:51:12
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;

namespace ModulePet
{
    public class PetHandbookView : KContainer
    {
        List<PetHaveGotGridDataWrapper> _haveGotDataWrapperList;
        List<pet> _ungetPetList;
        private int _currentIndex = 0;

        private CategoryList _toggleGroup;

        private KScrollView _scrollview;
        private VerticalLocater _locaterRoot;

        private KList _haveGotList;
        private KList _ungetList;

        private VerticalLocater _haveGotListLocater;
        private VerticalLocater _ungetListLocater;
        private VerticalLocater _haveGotTitleLocater;
        private VerticalLocater _ungetTitleLocater;
        

        protected override void Awake()
        {
            InitToggleGroup();
            InitList();
            InitLocater();
        }

        private void InitToggleGroup()
        {
            _toggleGroup = GetChildComponent<CategoryList>("Container_molingleixing/List_categoryList");
            _toggleGroup.RemoveAll();
            _toggleGroup.SetDataList<CategoryListItem>(pet_helper.GetSubTypeNameList());
        }

        private void InitList()
        {
            _scrollview = GetChildComponent<KScrollView>("ScrollView_huodemoling");
            _locaterRoot = AddChildComponent<VerticalLocater>("ScrollView_huodemoling/mask/content");
            InitGotList();
            InitUngetList();
        }

        private void InitGotList()
        {
            _haveGotList = GetChildComponent<KList>("ScrollView_huodemoling/mask/content/List_yihuodeliebiao");
            _haveGotList.SetDirection(KList.KListDirection.LeftToRight, 2, int.MaxValue);
            _haveGotList.SetGap(31, 25);
        }

        private void InitUngetList()
        {
            _ungetList = GetChildComponent<KList>("ScrollView_huodemoling/mask/content/List_weihuodeliebiao");
            _ungetList.SetDirection(KList.KListDirection.LeftToRight, 2, int.MaxValue);
            _ungetList.SetGap(31, 25);
        }

        private void InitLocater()
        {
            _haveGotListLocater = _haveGotList.gameObject.AddComponent<VerticalLocater>();
            _haveGotListLocater.NeedRecalculateSize = false;
            _ungetListLocater = _ungetList.gameObject.AddComponent<VerticalLocater>();
            _ungetListLocater.NeedRecalculateSize = false;
            _haveGotTitleLocater = AddChildComponent<VerticalLocater>("ScrollView_huodemoling/mask/content/Container_yihuodebiaoti");
            _ungetTitleLocater = AddChildComponent<VerticalLocater>("ScrollView_huodemoling/mask/content/Container_weihuodebiaoti");
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            _haveGotList.onItemClicked.AddListener(OnSelectHaveGotPet);
            _ungetList.onItemClicked.AddListener(OnSelectUngetPet);
        }

        private void RemoveListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            _haveGotList.onItemClicked.RemoveListener(OnSelectHaveGotPet);
            _ungetList.onItemClicked.RemoveListener(OnSelectUngetPet);
        }

        private void OnSelectHaveGotPet(KList list, KList.KListItemBase itemBase)
        {
            PetHaveGotGridDataWrapper haveGotDataWrapper = _haveGotDataWrapperList[itemBase.Index];
            if (haveGotDataWrapper.PetInfo != null)
            {
                PetPopPanelDataWrapper wrapper = new PetPopPanelDataWrapper(PetPopType.gotPetInfo, PetContext.handbook, haveGotDataWrapper.PetInfo);
                UIManager.Instance.ShowPanel(PanelIdEnum.PetPop, wrapper);
            }
            else
            {
                PetPopPanelDataWrapper wrapper = new PetPopPanelDataWrapper(PetPopType.ungetPetInfo, PetContext.handbook, haveGotDataWrapper.Pet);
                UIManager.Instance.ShowPanel(PanelIdEnum.PetPop, wrapper);
            }
        }

        private void OnSelectUngetPet(KList list, KList.KListItemBase itemBase)
        {
            PetPopPanelDataWrapper wrapper = new PetPopPanelDataWrapper(PetPopType.ungetPetInfo, PetContext.handbook, _ungetPetList[itemBase.Index]);
            UIManager.Instance.ShowPanel(PanelIdEnum.PetPop, wrapper);
        }

        public void OnShow(bool doTween)
        {
            _toggleGroup.SelectedIndex = 0;
            _currentIndex = 0;
            Refresh(doTween);
        }

        private void OnSelectedIndexChanged(CategoryList toggleGroup, int index)
        {
            _currentIndex = index;
            Refresh(true);
        }

        private void Refresh(bool doTween)
        {
            RefreshData();
            RefreshVisible();
            RefreshList();
            RefreshVerticalLayout();
            _scrollview.UpdateArrow();
            if (doTween)
            {
                DoTween();
            }
        }

        public List<pet> GetPetListInUngetHandBook()
        {
            PetData petData = PlayerDataManager.Instance.PetData;
            List<pet> petList = new List<pet>();
            foreach (pet pet in XMLManager.pet.Values)
            {
                if (!petData.PetDict.ContainsKey(pet.__id) && !petData.CanCall(pet.__id) && pet_helper.GetCanShow(pet.__id))
                {
                    petList.Add(pet);
                }
            }
            return petList;
        }

        public List<pet> GetPetListInUngetHandBookBySubType(PetSubTypeDefine subType)
        {
            List<pet> petList = new List<pet>();
            List<pet> allPetList = GetPetListInUngetHandBook();
            for (int i = 0; i < allPetList.Count; i++)
            {
                pet pet = allPetList[i];
                if (pet_helper.GetSubType(pet.__id) == subType)
                {
                    petList.Add(pet);
                }
            }
            return petList;
        }

        public List<PetHaveGotGridDataWrapper> GetWrapperListInGotHandBook()
        {
            PetData petData = PlayerDataManager.Instance.PetData;
            List<PetHaveGotGridDataWrapper> wrapperList = new List<PetHaveGotGridDataWrapper>();
            foreach (pet pet in XMLManager.pet.Values)
            {
                if (petData.PetDict.ContainsKey(pet.__id))
                {
                    wrapperList.Add(new PetHaveGotGridDataWrapper(petData.PetDict[pet.__id]));
                }
                else if (!petData.PetDict.ContainsKey(pet.__id) && petData.CanCall(pet.__id))
                {
                    wrapperList.Add(new PetHaveGotGridDataWrapper(pet));
                }
            }
            return wrapperList;
        }

        public List<PetHaveGotGridDataWrapper> GetWrapperListInGotHandBookBySubType(PetSubTypeDefine subType)
        {
            List<PetHaveGotGridDataWrapper> resultWrapperList = new List<PetHaveGotGridDataWrapper>();
            List<PetHaveGotGridDataWrapper> allWrapperList = GetWrapperListInGotHandBook();
            for (int i = 0; i < allWrapperList.Count; i++)
            {
                PetHaveGotGridDataWrapper wrapper = allWrapperList[i];
                int petId = wrapper.GetId();
                if (pet_helper.GetSubType(petId) == subType)
                {
                    resultWrapperList.Add(wrapper);
                }
            }
            return resultWrapperList;
        }

        private void RefreshData()
        {
            if (_currentIndex == 0)
            {
                _haveGotDataWrapperList = GetWrapperListInGotHandBook();
                _ungetPetList = GetPetListInUngetHandBook();
            }
            else
            {
                _haveGotDataWrapperList = GetWrapperListInGotHandBookBySubType((PetSubTypeDefine)(_currentIndex));
                _ungetPetList = GetPetListInUngetHandBookBySubType((PetSubTypeDefine)(_currentIndex));
            }
            _haveGotDataWrapperList.Sort(SortByHaveGotRule);
            _ungetPetList.Sort(SortByUngetRule);
        }

        private int SortByHaveGotRule(PetHaveGotGridDataWrapper x, PetHaveGotGridDataWrapper y)
        {
            if ((x.PetInfo == null) && (y.PetInfo != null))
            {
                return -1;
            }
            else if ((x.PetInfo != null) && (y.PetInfo != null))
            {
                PetInfo xPetInfo = x.PetInfo;
                PetInfo yPetInfo = y.PetInfo;
                if (xPetInfo.State == PetState.fighting && yPetInfo.State != PetState.fighting)
                {
                    return -1;
                }
                else if (xPetInfo.State == PetState.assist && yPetInfo.State != PetState.fighting && yPetInfo.State != PetState.assist)
                {
                    return -1;
                }
                else if (xPetInfo.State == yPetInfo.State)
                {
                    if (xPetInfo.Position < yPetInfo.Position)
                    {
                        return -1;
                    }
                    else if (xPetInfo.Position == yPetInfo.Position)
                    {
                        if (xPetInfo.Level > yPetInfo.Level)
                        {
                            return -1;
                        }
                        else if (xPetInfo.Level == yPetInfo.Level)
                        {
                            if (xPetInfo.Id < yPetInfo.Id)
                            {
                                return -1;
                            }
                        }
                    }
                }
            }
            else if ((x.PetInfo == null) && (y.PetInfo == null))
            {
                if (x.Pet.__id < y.Pet.__id)
                {
                    return -1;
                }
            }
            return 1;
        }

        private int SortByUngetRule(pet x, pet y)
        {
            ItemData xItemData = pet_helper.GetItemCost(x.__id);
            int xNum = PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemNum(xItemData.Id);
            ItemData yItemData = pet_helper.GetItemCost(y.__id);
            int yNum = PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemNum(yItemData.Id);
            if (xNum > yNum)
            {
                return -1;
            }
            else if (xNum == yNum)
            {
                if (x.__id < y.__id)
                {
                    return -1;
                }
            }
            return 1;
        }

        private void RefreshList()
        {
            RefreshHaveGotPetList();
            RefreshUngetPetList();
        }

        private void RefreshHaveGotPetList()
        {
            if (_haveGotDataWrapperList.Count > 0)
            {
                _haveGotList.SetDataList<PetHandbookHaveGotGrid>(_haveGotDataWrapperList);
            }
        }

        private void RefreshUngetPetList()
        {
            if (_ungetPetList.Count > 0)
            {
                _ungetList.SetDataList<PetHandbookUngetGrid>(_ungetPetList);
            }
        }

        private void RefreshVisible()
        {
            if (_haveGotDataWrapperList.Count == 0)
            {
                _haveGotListLocater.gameObject.SetActive(false);
                _haveGotTitleLocater.gameObject.SetActive(false);
            }
            else
            {
                _haveGotListLocater.gameObject.SetActive(true);
                _haveGotTitleLocater.gameObject.SetActive(true);
            }

            if (_ungetPetList.Count == 0)
            {
                _ungetListLocater.gameObject.SetActive(false);
                _ungetTitleLocater.gameObject.SetActive(false);
            }
            else
            {
                _ungetListLocater.gameObject.SetActive(true);
                _ungetTitleLocater.gameObject.SetActive(true);
            }
        }

        private void RefreshVerticalLayout()
        {
            _locaterRoot.RemoveAll();
            if (_haveGotDataWrapperList.Count != 0)
            {
                _locaterRoot.AddLocater(_haveGotTitleLocater);
                _locaterRoot.AddLocater(_haveGotListLocater);
            }
            if (_ungetPetList.Count != 0)
            {
                _locaterRoot.AddLocater(_ungetTitleLocater);
                _locaterRoot.AddLocater(_ungetListLocater);
            }
            _locaterRoot.Locate(0, new List<int>() {10});
        }

        private void DoTween()
        {
            TweenViewMove.Begin(_scrollview.gameObject, MoveType.Show, MoveDirection.Right);
        }
    }
}
