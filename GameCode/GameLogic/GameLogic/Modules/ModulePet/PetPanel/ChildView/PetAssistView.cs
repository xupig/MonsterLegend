﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/22 17:52:58
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Data;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using UnityEngine;
using Common.ExtendComponent;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager.SubSystem;

namespace ModulePet
{
    public class PetAssistView : KContainer
    {
        private List<PetAssistGridDataWrapper> _wrapperList;
        private int _currentIndex = 0;

        private KScrollView _scrollView;
        private KList _list;
        private GameObject _goLeft;
        private PetGetWay _getWay;
        private PetAssistDetail _detail;
        private PetAssistOptionalList _optionalList;

        private PetData _petData
        {
            get { return PlayerDataManager.Instance.PetData; }
        }

        protected override void Awake()
        {
            InitAssistList();
            _detail = AddChildComponent<PetAssistDetail>("Container_molingxinxi");
            _getWay = AddChildComponent<PetGetWay>("Container_huoderuko");
            _optionalList = AddChildComponent<PetAssistOptionalList>("Container_xuanzechongwu");

            AddListener();
        }

        private void InitAssistList()
        {
            _goLeft = GetChild("Container_congwuduizhang");
            _scrollView = GetChildComponent<KScrollView>("Container_congwuduizhang/ScrollView_zhuzhenmoling");
            _list = GetChildComponent<KList>("Container_congwuduizhang/ScrollView_zhuzhenmoling/mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _list.SetGap(-14, 0);
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _list.onSelectedIndexChanged.AddListener(OnSelectedIndexChange);
            EventDispatcher.AddEventListener<PetInfo>(PetEvents.RequestIdleToAssist, OnRequestIdleToAssist);
        }

        private void RemoveListener()
        {
            _list.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChange);
            EventDispatcher.RemoveEventListener<PetInfo>(PetEvents.RequestIdleToAssist, OnRequestIdleToAssist);
        }

        private void OnRequestIdleToAssist(PetInfo petInfo)
        {
            PetManager.Instance.RequestPetAssist(petInfo.Id, _currentIndex);
        }

        public void SetData(List<PetInfo> petInfoList, bool doTween)
        {
            List<PetAssistGridDataWrapper> wrapperList = GetWrapperList(petInfoList);
            _wrapperList = wrapperList;
            _list.SetDataList<PetAssistGrid>(wrapperList);
            PetAssistGridDataWrapper wrapper = wrapperList[_currentIndex];
            RefreshRightComponent(wrapper);
            RefreshSelectFrame();
            if (doTween)
            {
                DoTween();
            }
        }

        private void RefreshRightComponent(PetAssistGridDataWrapper wrapper)
        {
            PetFightingView.RightComponent componentType = PetPanelUtils.GetRightComponent(wrapper);
            HideAllComponentType();
            switch (componentType)
            {
                case PetFightingView.RightComponent.detail:
                    _detail.Visible = true;
                    _detail.Refresh(wrapper.PetInfo);
                    break;
                case PetFightingView.RightComponent.getWay:
                    _getWay.Visible = true;
                    break;
                case PetFightingView.RightComponent.optionalList:
                    _optionalList.Visible = true;
                    _optionalList.Refresh();
                    break;
            }
        }

        private void RefreshSelectFrame()
        {
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                PetAssistGrid grid = _list.ItemList[i] as PetAssistGrid;
                if (i == _currentIndex)
                {
                    grid.ShowSelectFrame();
                }
                else
                {
                    grid.HideSelectFrame();
                }
            }
        }

        private void HideAllComponentType()
        {
            _detail.Visible = false;
            _getWay.Visible = false;
            _optionalList.Visible = false;
        }

        private List<PetAssistGridDataWrapper> GetWrapperList(List<PetInfo> petInfoList)
        {
            List<PetAssistGridDataWrapper> wrapperList = new List<PetAssistGridDataWrapper>();
            List<pet_hole> petHoleList = pet_hole_helper.GetAssistPetHoldList();
            for (int i = 0; i < petHoleList.Count; i++)
            {
                pet_hole petHole = petHoleList[i];
                PetInfo petInfo = petInfoList.Find((p) => { return p.Position == (petHole.__index - 1); });
                PetAssistGridDataWrapper wrapper = new PetAssistGridDataWrapper(petHole, petInfo);
                wrapperList.Add(wrapper);
            }
            return wrapperList;
        }

        private void OnSelectedIndexChange(KList list, int index)
        {
            _currentIndex = index;
            PetAssistGridDataWrapper wrapper = _wrapperList[_currentIndex];
            RefreshRightComponent(wrapper);
            RefreshSelectFrame();
        }

        private void DoTween()
        {
            if (_goLeft.activeInHierarchy) TweenViewMove.Begin(_goLeft, MoveType.Show, MoveDirection.Left);
            if (_getWay.gameObject.activeInHierarchy) TweenViewMove.Begin(_getWay.gameObject, MoveType.Show, MoveDirection.Right);
            if (_detail.gameObject.activeInHierarchy) TweenViewMove.Begin(_detail.gameObject, MoveType.Show, MoveDirection.Right);
            if (_optionalList.gameObject.activeInHierarchy) TweenViewMove.Begin(_optionalList.gameObject, MoveType.Show, MoveDirection.Right);
        }
    }
}
