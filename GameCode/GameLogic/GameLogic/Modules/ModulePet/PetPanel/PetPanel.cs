﻿using Common.Base;
using Common.Data;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using Common.Events;
using MogoEngine.Events;
using GameData;
using System.Collections.Generic;

namespace ModulePet
{
    public class PetPanel : BasePanel
    {
        public const int TAB_FIGHTING = 0;
        public const int TAB_ASSIST = 1;
        public const int TAB_HANDBOOK = 2;

        private PetData _petData
        {
            get
            {
                return PlayerDataManager.Instance.PetData;
            }
        }

        private int _currentIndex = -1;

        private PetFightingView _fightView;
        private PetAssistView _assistView;
        private PetHandbookView _handbook;
        private KToggleGroup _toggleGroup;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Pet; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _fightView = AddChildComponent<PetFightingView>("Container_chuzhanmoling");
            _assistView = AddChildComponent<PetAssistView>("Container_zhuzhenmoling");
            _handbook = AddChildComponent<PetHandbookView>("Container_huodemoling");
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
        }

        private void AddListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            EventDispatcher.AddEventListener(PetEvents.UpdatePetInfo, OnUpdatePetInfo);
            EventDispatcher.AddEventListener(PetEvents.AddExp, OnUpdatePetInfo);
            EventDispatcher.AddEventListener(PetEvents.ReceiveFightToIdle, OnUpdatePetInfo);
            EventDispatcher.AddEventListener(PetEvents.ReceiveIdleToFight, OnUpdatePetInfo);
            string closeEventType = BaseModule.GetCloseEventName(PanelIdEnum.PetStrengthen);
            EventDispatcher.AddEventListener<PanelIdEnum,bool>(closeEventType, OnForceUpdatePetInfo);
            EventDispatcher.AddEventListener<PetInfo>(PetEvents.IdleToFight, GoToFight);
        }

        private void RemoveListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            EventDispatcher.RemoveEventListener(PetEvents.UpdatePetInfo, OnUpdatePetInfo);
            EventDispatcher.RemoveEventListener(PetEvents.AddExp, OnUpdatePetInfo);
            EventDispatcher.RemoveEventListener(PetEvents.ReceiveFightToIdle, OnUpdatePetInfo);
            EventDispatcher.RemoveEventListener(PetEvents.ReceiveIdleToFight, OnUpdatePetInfo);
            string closeEventType = BaseModule.GetCloseEventName(PanelIdEnum.PetStrengthen);
            EventDispatcher.RemoveEventListener<PanelIdEnum,bool>(closeEventType, OnForceUpdatePetInfo);
            EventDispatcher.RemoveEventListener<PetInfo>(PetEvents.IdleToFight, GoToFight);
        }

        private void GoToFight(PetInfo petInfo)
        {
            SoundInfoManager.Instance.PlaySound(pet_helper.GetSpeakVoiceId(petInfo.Id));
        }

        public override void OnShow(object param)
        {
            AddListener();
            int index = ParseIndex(param);
            ShowSecondView(param);
            _toggleGroup.SelectIndex = index;
            OnSelectedIndexChanged(_toggleGroup, index);
        }

        private int ParseIndex(object param)
        {
            int index = 0;
            if (param is int)
            {
                index = (int)param;
            }
            else if (param is PetPanelDataWrapper)
            {
                PetPanelDataWrapper PetPanelDataWrapper = param as PetPanelDataWrapper;
                index = PetPanelDataWrapper.PetPanelTabIndex;
            }
            return index;
        }

        private void ShowSecondView(object param)
        {
            if (param is PetPanelDataWrapper)
            {
                PetPanelDataWrapper PetPanelDataWrapper = param as PetPanelDataWrapper;
                if (PetPanelDataWrapper.PanelId == PanelIdEnum.PetPop)
                {
                    PetPopPanelDataWrapper petPopDataWrapper = new PetPopPanelDataWrapper(PetPopType.ungetPetInfo, PetContext.handbook, pet_helper.GetConfig(PetPanelDataWrapper.PetId));
                    UIManager.Instance.ShowPanel(PanelIdEnum.PetPop, petPopDataWrapper);
                }
                else if (PetPanelDataWrapper.PanelId == PanelIdEnum.PetStrengthen)
                {
                    PetData petData = PlayerDataManager.Instance.PetData;
                    PetStrengthenPanelDataWrapper petStrengthenDataWrapper = new PetStrengthenPanelDataWrapper(PetPanelDataWrapper.PetStrengthen, petData.PetDict[PetPanelDataWrapper.PetId]);
                    UIManager.Instance.ShowPanel(PanelIdEnum.PetStrengthen, petStrengthenDataWrapper);
                }
            }
        }

        public override void OnClose()
        {
            RemoveListener();
        }

        private void OnUpdatePetInfo()
        {
            if (BaseModule.IsPanelShowing(PanelIdEnum.PetStrengthen))
            {
                return;
            }
            OnForceUpdatePetInfo();
        }

        private void OnForceUpdatePetInfo(PanelIdEnum panelId = PanelIdEnum.Empty, bool isExclusive = false)
        {
            RefreshCurrentView(false);
            RefreshTips();
        }

        private void OnSelectedIndexChanged(KToggleGroup toggleGroup, int index)
        {
            HidePreView();
            _currentIndex = index;
            RefreshCurrentView(true);
            RefreshTips();
        }

        private void HidePreView()
        {
            switch (_currentIndex)
            {
                case TAB_FIGHTING:
                    _fightView.Visible = false;
                    break;
                case TAB_ASSIST:
                    _assistView.Visible = false;
                    break;
                case TAB_HANDBOOK:
                    _handbook.Visible = false;
                    break;
            }
        }

        private void RefreshCurrentView(bool doTween)
        {
            PetData petData = PlayerDataManager.Instance.PetData;
            switch (_currentIndex)
            {
                case TAB_FIGHTING:
                    _fightView.Visible = true;
                    _fightView.SetData(petData.FightingPetList, doTween);
                    break;
                case TAB_ASSIST:
                    _assistView.Visible = true;
                    _assistView.SetData(petData.AssistPetList, doTween);
                    break;
                case TAB_HANDBOOK:
                    _handbook.Visible = true;
                    _handbook.OnShow(doTween);
                    break;
            }
        }

        private void RefreshTips()
        {
            List<KToggle> toggleList = _toggleGroup.GetToggleList();
            toggleList[TAB_FIGHTING].SetGreenPoint(NeedShowFightingGreenPoint());
            toggleList[TAB_ASSIST].SetGreenPoint(_petData.HaveEmptyAssistHolder() && (_petData.IdlePetList.Count > 0));
            toggleList[TAB_HANDBOOK].SetGreenPoint(NeedShowHandbookGreenPoint());
        }

        public bool NeedShowFightingGreenPoint()
        {
            foreach (PetInfo petInfo in _petData.FightingPetList)
            {
                if (petInfo.CanStrength())
                {
                    return true;
                }
            }

            return _petData.HaveEmptyFightingHolder() && _petData.IdlePetList.Count > 0;
        }

        public bool NeedShowHandbookGreenPoint()
        {
            if (_petData.HaveCanCallPet())
            {
                return true;
            }
            if (_petData.HaveCanStrengthPet())
            {
                return true;
            }
            return false;
        }
    }
}
