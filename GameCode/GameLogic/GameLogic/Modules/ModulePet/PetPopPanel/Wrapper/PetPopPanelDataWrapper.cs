﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/28 11:32:53
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using GameData;

namespace ModulePet
{
    public class PetPopPanelDataWrapper
    {
        public PetPopType PopType;
        public PetContext Context;
        public PetInfo PetInfo;
        public pet Pet;

        public PetPopPanelDataWrapper(PetPopType popType, PetContext context, PetInfo petInfo)
        {
            this.PopType = popType;
            this.Context = context;
            this.PetInfo = petInfo;
        }

        public PetPopPanelDataWrapper(PetPopType popType, PetContext context, pet pet)
        {
            this.PopType = popType;
            this.Context = context;
            this.Pet = pet;
        }

    }
}
