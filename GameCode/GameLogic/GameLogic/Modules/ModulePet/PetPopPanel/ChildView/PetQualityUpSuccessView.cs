﻿using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetQualityUpSuccessView : KContainer
    {
        private PetInfo _petInfo;
        private Vector3 _oldModelInitPosition;
        private Vector3 _newModelInitPosition;

        private KDummyButton _btnClose;
        private StateText _textDesc;
        private KParticle _particle;
        private GameObject _goDetail;


        private ModelComponent _oldModel;
        private StateText _textOldName;
        private ModelComponent _newModel;
        private StateText _textNewName;
        private StateText _textQualityUping;
        private StateText _textDesc1;
        private GameObject _goArrow;

        public KComponentEvent onClose = new KComponentEvent();

        protected override void Awake()
        {
            _oldModel = AddChildComponent<ModelComponent>("Container_oldModel");
            _oldModelInitPosition = _oldModel.transform.localPosition;
            _newModel = AddChildComponent<ModelComponent>("Container_newModel");
            _newModelInitPosition = _newModel.transform.localPosition;

            _textOldName = GetChildComponent<StateText>("Container_oldModel/Label_txtChongwumingcheng");
            _textOldName.Clear();
            _textNewName = GetChildComponent<StateText>("Container_newModel/Label_txtChongwumingcheng");
            _textNewName.Clear();

            _textQualityUping = GetChildComponent<StateText>("Label_txtChongwumingcheng");
            _goArrow = GetChild("Image_qianjin");
            
            _goDetail = GetChild("Container_detail");
            _btnClose = AddChildComponent<KDummyButton>("Container_detail/ScaleImage_sharedzhezhao");
            _textDesc = GetChildComponent<StateText>("Container_detail/Label_txttupo");
            _textDesc1 = GetChildComponent<StateText>("Container_detail/Label_txtGongxi");
            
            _particle = AddChildComponent<KParticle>("fx_ui_21_3_tupo_02");
            _particle.Stop();

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(OnClose);
            _particle.onComplete.AddListener(OnComplete);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(OnClose);
            _particle.onComplete.RemoveListener(OnComplete);
        }

        private void OnClose()
        {
            onClose.Invoke();
        }

        public PetInfo Data
        {
            set
            {
                _petInfo = value;
                Refresh();
            }
        }

        private void Refresh()
        {
            _oldModel.Visible = true;
            _oldModel.LoadPetModel(_petInfo.Id, _petInfo.Quality - 1);
            _newModel.LoadPetModel(_petInfo.Id, _petInfo.Quality);
            _textOldName.CurrentText.text = ColorDefine.GetColorHtmlString(pet_quality_helper.GetPetNameColor(_petInfo.Id, _petInfo.Quality - 1), 
                pet_quality_helper.GetPetQualityName(_petInfo.Id, _petInfo.Quality - 1));
            _textNewName.CurrentText.text = ColorDefine.GetColorHtmlString(pet_quality_helper.GetPetNameColor(_petInfo.Id, _petInfo.Quality), 
                pet_quality_helper.GetPetQualityName(_petInfo));
            _textQualityUping.Clear();
            _textDesc.Clear();
            _goDetail.SetActive(false);
            Vector3 position = new Vector3(444, _oldModel.transform.localPosition.y, 0f);
            TweenPosition tween = TweenPosition.Begin(_oldModel.gameObject, 0.8f, position);
            tween.from = _oldModelInitPosition;
            tween = TweenPosition.Begin(_newModel.gameObject, 0.8f, position, OnFinish);
            tween.from = _newModelInitPosition;
        }

        private void OnFinish(UITweener tween)
        {
            _oldModel.Visible = false;
            _textNewName.Clear();
            _textQualityUping.CurrentText.text = ColorDefine.GetColorHtmlString(pet_quality_helper.GetPetNameColor(_petInfo.Id, _petInfo.Quality), 
                MogoLanguageUtil.GetContent(43475, pet_quality_helper.GetPetQualityName(_petInfo.Id, _petInfo.Quality - 1)));
            _particle.Play();
            _particle.Position = new Vector3(533f, -205f, -600f);
            UIManager.Instance.PlayAudio("Sound/UI/pet_inhance.mp3");
        }

        private void OnComplete()
        {
            _textQualityUping.Clear();
            _textDesc1.CurrentText.color = ColorDefine.GetColorById(pet_quality_helper.GetPetNameColor(_petInfo.Id, _petInfo.Quality));
            _textDesc.CurrentText.text = ColorDefine.GetColorHtmlString(pet_quality_helper.GetPetNameColor(_petInfo.Id, _petInfo.Quality),
                MogoLanguageUtil.GetContent(43459, pet_helper.GetName(_petInfo.Id),
                pet_quality_helper.GetPetQualityString(_petInfo.Id, _petInfo.Quality)));
            _goDetail.SetActive(true);
        }
    }
}
