﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/28 11:14:41
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using UnityEngine;

namespace ModulePet
{
    public class UngetPetInfoView : KContainer
    {
        private pet _pet;

        private StateText _textName;
        private PetSubType _petSubType;
        private KButton _btnClose;
        private KScrollView _scrollView;
        private KContainer _containerContent;
        private VerticalLocater _verticalLocaterRoot;
        private PetGetWay _petInfoGetWay;
        private VerticalLocater _getWayLocater;
        private PetInfoIcon _petInfoIcon;
        private PetInfoQuality _petInfoQuality;
        private PetInfoSkill _petInfoSkill;
        private PetInfoLeaderSkill _petInfoLeaderSkill;

        public KComponentEvent onClose = new KComponentEvent();

        protected override void Awake()
        {
            _textName = GetChildComponent<StateText>("Label_txtBiaoti");
            _petSubType = AddChildComponent<PetSubType>("Container_leixing");
            _btnClose = GetChildComponent<KButton>("Container_jianglibaoxiangBG/Button_close");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_huadong");
            _containerContent = GetChildComponent<KContainer>("ScrollView_huadong/mask/content");
            _petInfoGetWay = AddChildComponent<PetGetWay>("ScrollView_huadong/mask/content/Container_xinxi"); 
            _petInfoIcon = AddChildComponent<PetInfoIcon>("ScrollView_huadong/mask/content/Container_zhanshi");
            _petInfoQuality = AddChildComponent<PetInfoQuality>("ScrollView_huadong/mask/content/Container_molingzizhi");
            _petInfoSkill = AddChildComponent<PetInfoSkill>("ScrollView_huadong/mask/content/Container_jinengtianfu");
            _petInfoLeaderSkill = AddChildComponent<PetInfoLeaderSkill>("ScrollView_huadong/mask/content/Container_duizhangjineng");

            AddLocater();
            AddListener();
        }

        private void AddLocater()
        {
            _verticalLocaterRoot = _containerContent.gameObject.AddComponent<VerticalLocater>();
            VerticalLocater verticalLocater = _petInfoGetWay.gameObject.AddComponent<VerticalLocater>();
            _getWayLocater = verticalLocater;
            _verticalLocaterRoot.AddLocater(verticalLocater);
            verticalLocater.NeedRecalculateSize = false;
            verticalLocater.Gap = 60;
            _verticalLocaterRoot.AddLocater(_petInfoQuality.gameObject.AddComponent<VerticalLocater>());
            _verticalLocaterRoot.AddLocater(_petInfoSkill.gameObject.AddComponent<VerticalLocater>());
            _verticalLocaterRoot.AddLocater(_petInfoLeaderSkill.gameObject.AddComponent<VerticalLocater>());
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(OnClose);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(OnClose);
        }

        private void OnClose()
        {
            onClose.Invoke();
        }

        public pet Data
        {
            set
            {
                _pet = value;
                _scrollView.ResetContentPosition();
                Refresh();
            }
        }

        private void Refresh()
        {
            _textName.CurrentText.text = pet_helper.GetName(_pet);
            _petSubType.SetSubType(pet_helper.GetSubType(_pet));
            _petInfoIcon.Refresh(_pet);
            _petInfoQuality.Refresh(_pet);
            _petInfoSkill.Refresh(_pet);
            _petInfoLeaderSkill.Refresh(_pet);
            _getWayLocater.StartY = -20;
            _verticalLocaterRoot.Locate(0f, new List<int>() { 20 });
        }
    }
}
