﻿using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetStarUpSuccessView : KContainer
    {
        private PetInfo _petInfo;

        private StarList _starList;
        private KDummyButton _btnClose;
        private StateText _textDesc;
        private GameObject _goDetail;
        private ItemGrid _itemGrid;
        private KParticle _particleStar;
        private KParticle _particleFrame;

        public KComponentEvent onClose = new KComponentEvent();

        protected override void Awake()
        {
            _starList = AddChildComponent<StarList>("Container_xingxing");
            _starList.SetStarData(5, "Container_star", "Image_xingxingIcon");
            _goDetail = GetChild("Container_detail");
            _btnClose = AddChildComponent<KDummyButton>("Container_detail/ScaleImage_sharedzhezhao");
            _textDesc = GetChildComponent<StateText>("Container_detail/Label_txtShengxing");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _particleFrame = AddChildComponent<KParticle>("Container_wupin/fx_ui_21_4_star_02_wupin");
            _particleFrame.Stop();
            _particleStar = AddChildComponent<KParticle>("Container_xingxing/fx_ui_21_4_star_02");
            _particleStar.Stop();

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(OnClose);
            _particleStar.onComplete.AddListener(OnComplete);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(OnClose);
            _particleStar.onComplete.RemoveListener(OnComplete);
        }

        private void OnClose()
        {
            onClose.Invoke();
        }

        public PetInfo Data
        {
            set
            {
                _petInfo = value;
                Refresh();
            }
        }

        private void Refresh()
        {
            _textDesc.CurrentText.text = MogoLanguageUtil.GetContent(43458, 
                pet_helper.GetName(_petInfo.Id), _petInfo.Star);
            _itemGrid.SetItemData(pet_helper.GetIcon(_petInfo.Id), pet_quality_helper.GetNameColorId(_petInfo.Id, _petInfo.Quality));
            _starList.SetStarNum(_petInfo.Star);
            _goDetail.SetActive(false);
            _particleStar.Play();
            TimerHeap.AddTimer(1000, 0, PlayFrameParticle);
            _particleStar.transform.localPosition = _starList.GetStarPosition(_petInfo.Star);
            UIManager.Instance.PlayAudio("Sound/UI/mission_grade.mp3");
        }

        private void PlayFrameParticle()
        {
            _particleFrame.Play();
        }

        private void OnComplete()
        {
            _goDetail.SetActive(true);
        }
    }
}
