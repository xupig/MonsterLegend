﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/28 11:14:24
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;

namespace ModulePet
{
    public class GotPetInfoView : KContainer
    {
        private PetInfo _petInfo;

        private StateText _textName;
        private KButton _btnClose;
        private KScrollView _scrollView;
        private KContainer _containerContent;
        private VerticalLocater _verticalLocaterRoot;
        private KButton _btnPutOff;
        private KButton _btnChange;
        private PetSubType _petSubType;
        private PetInfoAttribute _petInfoAttribute;
        private PetInfoIcon _petInfoIcon;
        private PetInfoQuality _petInfoQuality;
        private PetInfoSkill _petInfoSkill;
        private PetInfoLeaderSkill _petInfoLeaderSkill;

        public KComponentEvent onClose = new KComponentEvent();

        protected override void Awake()
        {
            _btnClose = GetChildComponent<KButton>("Container_jianglibaoxiangBG/Button_close");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_huadong");
            _btnPutOff = GetChildComponent<KButton>("Button_xiexia");
            _btnChange = GetChildComponent<KButton>("Button_genghuan");
            _textName = GetChildComponent<StateText>("Label_txtBiaoti");
            _petSubType = AddChildComponent<PetSubType>("Container_leixing");
            _containerContent = GetChildComponent<KContainer>("ScrollView_huadong/mask/content");
            _petInfoAttribute = AddChildComponent<PetInfoAttribute>("ScrollView_huadong/mask/content/Container_xinxi");
            _petInfoIcon = AddChildComponent<PetInfoIcon>("ScrollView_huadong/mask/content/Container_zhanshi");
            _petInfoQuality = AddChildComponent<PetInfoQuality>("ScrollView_huadong/mask/content/Container_molingzizhi");
            _petInfoSkill = AddChildComponent<PetInfoSkill>("ScrollView_huadong/mask/content/Container_jinengtianfu");
            _petInfoLeaderSkill = AddChildComponent<PetInfoLeaderSkill>("ScrollView_huadong/mask/content/Container_duizhangjineng");

            AddLocater();
            AddListener();
        }

        private void AddLocater()
        {
            _verticalLocaterRoot = _containerContent.gameObject.AddComponent<VerticalLocater>();
            VerticalLocater locater = _petInfoAttribute.gameObject.AddComponent<VerticalLocater>();
            locater.Gap = 35;
            _verticalLocaterRoot.AddLocater(locater);
            _verticalLocaterRoot.AddLocater(_petInfoQuality.gameObject.AddComponent<VerticalLocater>());
            _verticalLocaterRoot.AddLocater(_petInfoSkill.gameObject.AddComponent<VerticalLocater>());
            _verticalLocaterRoot.AddLocater(_petInfoLeaderSkill.gameObject.AddComponent<VerticalLocater>());
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(OnClose);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(OnClose);
        }

        private void OnClose()
        {
            onClose.Invoke();
        }

        public PetInfo Data
        {
            set
            {
                _petInfo = value;
                _scrollView.ResetContentPosition();
                Refresh();
            }
        }

        private void Refresh()
        {
            _textName.CurrentText.text = pet_helper.GetName(_petInfo.Id);
            _petSubType.SetSubType(pet_helper.GetSubType(_petInfo.Id));
            _petInfoAttribute.Data = _petInfo;
            _petInfoIcon.Refresh(_petInfo);
            _petInfoQuality.Refresh(_petInfo);
            _petInfoSkill.Refresh(_petInfo);
            _petInfoLeaderSkill.Refresh(_petInfo);
            _verticalLocaterRoot.Locate(0f, new List<int>() { 20 });
        }
    }
}
