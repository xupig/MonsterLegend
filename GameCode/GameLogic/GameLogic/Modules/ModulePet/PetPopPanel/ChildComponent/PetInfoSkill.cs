﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/28 22:01:39
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Game.UI.UIComponent;
using GameData;
using UnityEngine;
using Common.ExtendComponent;

namespace ModulePet
{
    public class PetInfoSkill : KContainer
    {
        private float _initY;

        private KList _list;
        private VerticalLocater _locater;

        protected override void Awake()
        {
            _list = GetChildComponent<KList>("List_jineng");
            _initY = _list.transform.localPosition.y;
            _locater = AddChildComponent<VerticalLocater>("List_jineng");
            InitList();
        }

        private void InitList()
        {
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
        }

        public void Refresh(PetInfo petInfo)
        {
            List<PetSkill> petSkillList = petInfo.GetAllSkillList();
            List<PetSkillDataWrapper> wrapperList = pet_quality_helper.ToPetSkillDataWrapperList(petInfo.Id, petSkillList, petInfo.Quality);
            _list.SetDataList<PetInfoSkillItem>(wrapperList);
            AddVerticalLocater();
        }

        public void Refresh(pet _pet)
        {
            List<PetSkill> petSkillList = pet_quality_helper.GetPetSkillList(_pet.__id);
            List<PetSkillDataWrapper> wrapperList = pet_quality_helper.ToPetSkillDataWrapperList(_pet.__id, petSkillList, _pet.__init_quality);
            _list.SetDataList<PetInfoSkillItem>(wrapperList);
            AddVerticalLocater();
        }

        private void AddVerticalLocater()
        {
            _locater.RemoveAll();
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                VerticalLocater locater = _list.ItemList[i].gameObject.AddComponent<VerticalLocater>();
                _locater.AddLocater(locater);
            }
            _locater.Locate(_initY);
        }
    }
}
