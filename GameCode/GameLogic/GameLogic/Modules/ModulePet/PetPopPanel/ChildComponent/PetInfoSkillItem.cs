﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/28 23:02:16
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;

namespace ModulePet
{
    public class PetInfoSkillItem : KList.KListItemBase
    {
        private StateText _textName;
        private StateText _textDesc;
        private PetSkillIcon _icon;

        protected override void Awake()
        {
            _textName = GetChildComponent<StateText>("Label_txtName");
            _textDesc = GetChildComponent<StateText>("Label_txtTianfu");
            _icon = gameObject.AddComponent<PetSkillIcon>();
            _icon.CanShowTips = false;
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                PetSkillDataWrapper wrapper = value as PetSkillDataWrapper;
                Refresh(wrapper.PetId, wrapper.PetSkill, wrapper.Quality);
            }
        }

        private void Refresh(int petId, PetSkill petSkill, int quality)
        {
            int skillId = petSkill.skillId;
            _textName.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(pet_skill_helper.GetName(skillId)), "：");
            string desc = pet_skill_helper.GetDescStr(skillId);
            if (petSkill.quality > quality)
            {
                string unlockDesc = string.Format(LangEnum.PET_UNLOCK_QUALITY.ToLanguage(), pet_quality_helper.GetPetQualityString(petId, petSkill.quality));
                _textDesc.CurrentText.text = string.Format("{0}  {1}", desc, unlockDesc);
            }
            else
            {
                _textDesc.CurrentText.text = desc;
            }
            _icon.RefreshIcon(skillId);
            _textDesc.RecalculateCurrentStateHeight();
        }
    }
}
