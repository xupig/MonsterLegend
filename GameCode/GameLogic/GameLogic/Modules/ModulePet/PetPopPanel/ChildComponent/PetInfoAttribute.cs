﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/28 22:00:46
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;

namespace ModulePet
{
    public class PetInfoAttribute : KContainer
    {
        private StateText _textQuality;
        private StateText _textLevel;
        private KList _attributeList;

        protected override void Awake()
        {
            _textQuality = GetChildComponent<StateText>("Container_pinzhi/Label_txtPinzhizhi");
            _textLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengjizhi");
            _attributeList = GetChildComponent<KList>("List_content");
            InitList();
        }

        private void InitList()
        {
            _attributeList.SetDirection(KList.KListDirection.LeftToRight, 2, 3);
            _attributeList.SetGap(3, 98);
        }

        public PetInfo Data
        {
            set
            {
                Refresh(value);
            }
        }

        private void Refresh(PetInfo petInfo)
        {
            _textQuality.CurrentText.text = pet_quality_helper.GetPetQualityString(petInfo);
            _textLevel.CurrentText.text = petInfo.Level.ToString();
            PetData petData = PlayerDataManager.Instance.PetData;
            List<RoleAttributeData> list = petData.GetAttributeDataList(petInfo, true);
            List<IAttribute> ilist = pet_star_helper.ParseUpLowbound(list);
            _attributeList.SetDataList<AttributeItem>(ilist);
        }
    }
}
