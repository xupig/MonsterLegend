﻿using Common.ExtendComponent;
using Common.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetPopAttributeItem : AttributeItem
    {
        protected override string GetNameTemplate()
        {
            return string.Concat("{0}", LangEnum.PET_QUALITY1.ToLanguage(), "：");
        }
    }
}
