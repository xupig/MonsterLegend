﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/28 22:07:21
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using UnityEngine;

namespace ModulePet
{
    public class PetInfoIcon : KContainer
    {
        private ItemGrid _itemGrid;
        private StarList _starList;

        protected override void Awake()
        {
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _starList = AddChildComponent<StarList>("Container_xingxing");
            _starList.SetStarData(5, "Container_xingxing0", "Image_xingxingIcon");
        }

        public void Refresh(pet pet)
        {
            _itemGrid.SetItemData(pet_helper.GetIcon(pet), pet.__init_quality);
            _starList.SetStarNum(pet.__init_star);
        }

        public void Refresh(PetInfo petInfo)
        {
            _itemGrid.SetItemData(pet_helper.GetIcon(petInfo.Id), petInfo.Quality);
            _starList.SetStarNum(petInfo.Star);
        }
    }
}
