﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/7 14:01:10
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using Common.ExtendComponent;

namespace ModulePet
{
    public class PetInfoLeaderSkillItem : KList.KListItemBase
    {
        private StateText _textName;
        private StateText _textDesc;
        private IconContainer _icon;

        protected override void Awake()
        {
            _textName = GetChildComponent<StateText>("Label_txtName");
            _textDesc = GetChildComponent<StateText>("Label_txtTianfu");
            _icon = AddChildComponent<IconContainer>("Container_icon");
        }

        public override object Data
        {
            set
            {
                int buffId = (int) value;
                Refresh(buffId);
            }
        }

        private void Refresh(int buffId)
        {
            _textName.CurrentText.text = buff_helper.GetName(buffId);
            KeyValuePair<int, float> kvp = buff_helper.GetAttriKvp(buffId);
            _textDesc.CurrentText.text = MogoLanguageUtil.GetContent(buff_helper.GetDescId(buffId), attri_config_helper.GetAttributeName(kvp.Key), kvp.Value * 100);
            _icon.SetIcon(buff_helper.GetIcon(buffId));
        }

        public override void Dispose()
        {
        }
    }
}
