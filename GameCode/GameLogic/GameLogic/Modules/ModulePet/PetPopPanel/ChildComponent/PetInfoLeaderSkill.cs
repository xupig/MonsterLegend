﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/7 13:58:10
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Game.UI.UIComponent;
using GameData;

namespace ModulePet
{
    public class PetInfoLeaderSkill : KContainer
    {
        private KList _list;

        protected override void Awake()
        {
            _list = GetChildComponent<KList>("List_jineng");
            InitList();
        }

        private void InitList()
        {
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
        }

        public void Refresh(PetInfo petInfo)
        {
            _list.SetDataList<PetInfoLeaderSkillItem>(pet_quality_helper.GetLeaderSkill(petInfo.Id, petInfo.Quality));
        }

        public void Refresh(pet _pet)
        {
            _list.SetDataList<PetInfoLeaderSkillItem>(pet_quality_helper.GetLeaderSkill(_pet.__id, _pet.__init_quality));
        }
    }
}
