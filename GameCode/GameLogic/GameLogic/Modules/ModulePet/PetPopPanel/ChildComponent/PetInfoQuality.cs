﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/28 22:01:11
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Game.UI.UIComponent;
using GameData;

namespace ModulePet
{
    public class PetInfoQuality : KContainer
    {
        private KList _attributeList;

        protected override void Awake()
        {
            _attributeList = GetChildComponent<KList>("List_attri");
            InitList();
        }

        private void InitList()
        {
            _attributeList.SetDirection(KList.KListDirection.LeftToRight, 2, 3);
            _attributeList.SetGap(0, -106);
        }

        public void Refresh(PetInfo petInfo)
        {
            List<RoleAttributeData> list = petInfo.StarAttributeList;
            SetDataList(list);
        }

        public void Refresh(pet pet)
        {
            List<RoleAttributeData> list = pet_star_helper.GetPetStarAttributeDataList(pet.__id, pet.__init_star);
            SetDataList(list);
        }

        private void SetDataList(List<RoleAttributeData> list)
        {
            List<IAttribute> ilist = pet_star_helper.ParseUpLowbound(list);
            _attributeList.SetDataList<PetPopAttributeItem>(ilist);
        }
    }
}
