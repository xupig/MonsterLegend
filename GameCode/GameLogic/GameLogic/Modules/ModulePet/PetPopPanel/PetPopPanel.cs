﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/28 11:07:37
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Data;
using Common.Events;
using MogoEngine.Events;
using UnityEngine;
using GameMain.GlobalManager;
using Game.UI.UIComponent;

namespace ModulePet
{
    public class PetPopPanel : BasePanel
    {
        private GotPetInfoView _gotPetInfoView;
        private UngetPetInfoView _ungetPetInfoView;
        private PetQualityUpSuccessView _qualityUpSuccessView;
        private PetStarUpSuccessView _starUpSuccessView;

        protected override void Awake()
        {
            _gotPetInfoView = AddChildComponent<GotPetInfoView>("Container_yihuodemoling");
            _ungetPetInfoView = AddChildComponent<UngetPetInfoView>("Container_weihuodemoling");
            _qualityUpSuccessView = AddChildComponent<PetQualityUpSuccessView>("Container_tupochenggong");
            _starUpSuccessView = AddChildComponent<PetStarUpSuccessView>("Container_shengxingchenggong");

            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedzhezhao");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _gotPetInfoView.onClose.AddListener(ClosePanel);
            _ungetPetInfoView.onClose.AddListener(ClosePanel);
            _qualityUpSuccessView.onClose.AddListener(ClosePanel);
            _starUpSuccessView.onClose.AddListener(ClosePanel);
        }

        private void RemoveListener()
        {
            _gotPetInfoView.onClose.RemoveListener(ClosePanel);
            _ungetPetInfoView.onClose.RemoveListener(ClosePanel);
            _qualityUpSuccessView.onClose.RemoveListener(ClosePanel);
            _starUpSuccessView.onClose.RemoveListener(ClosePanel);
        }

        public override void OnShow(object data)
        {
            if (BaseModule.IsPanelShowing(PanelIdEnum.PetStrengthen))
            {
                UIManager.Instance.DisablePanelCanvas(PanelIdEnum.PetStrengthen);
            }
            else
            {
                UIManager.Instance.DisablePanelCanvas(PanelIdEnum.Pet);
            }
            PetPopPanelDataWrapper wrapper = data as PetPopPanelDataWrapper;
            Refresh(wrapper);
        }

        public override void OnClose()
        {
            if (BaseModule.IsPanelShowing(PanelIdEnum.PetStrengthen))
            {
                UIManager.Instance.EnablePanelCanvas(PanelIdEnum.PetStrengthen);
            }
            else
            {
                UIManager.Instance.EnablePanelCanvas(PanelIdEnum.Pet);
            }
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.PetPop; }
        }

        private void Refresh(PetPopPanelDataWrapper wrapper)
        {
            HideAllView();
            switch (wrapper.PopType)
            {
                case PetPopType.gotPetInfo:
                    _gotPetInfoView.Visible = true;
                    _gotPetInfoView.Data = wrapper.PetInfo;
                    break;
                case PetPopType.ungetPetInfo:
                    _ungetPetInfoView.Visible = true;
                    _ungetPetInfoView.Data = wrapper.Pet;
                    break;
                case PetPopType.qualityUpSuccess:
                    _qualityUpSuccessView.Visible = true;
                    _qualityUpSuccessView.Data = wrapper.PetInfo;
                    break;
                case PetPopType.starUpSuccess:
                    _starUpSuccessView.Visible = true;
                    _starUpSuccessView.Data = wrapper.PetInfo;
                    break;
            }
        }

        private void HideAllView()
        {
            _gotPetInfoView.Visible = false;
            _ungetPetInfoView.Visible = false;
            _starUpSuccessView.Visible = false;
            _qualityUpSuccessView.Visible = false;
        }
    }
}
