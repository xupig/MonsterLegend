﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/4 14:37:35
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;

namespace ModulePet
{
    public class PetStrengthenPanelDataWrapper
    {
        public PetStrengthen PetStrengthen;
        public PetInfo PetInfo;

        public PetStrengthenPanelDataWrapper(PetStrengthen petStrengthen, PetInfo petInfo)
        {
            this.PetStrengthen = petStrengthen;
            this.PetInfo = petInfo;
        }
    }
}
