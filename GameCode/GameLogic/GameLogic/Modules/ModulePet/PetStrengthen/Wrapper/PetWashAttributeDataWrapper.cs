﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetWashAttributeDataWrapper
    {
        public string Name;
        public int AttributeValue;
        public int LowBound;
        public int UpBound;
        public int RefreshAttributeValue;

        public PetWashAttributeDataWrapper(string name, int attributeValue, int lowBound, int upBound)
        {
            this.Name = name;
            this.AttributeValue = attributeValue;
            this.LowBound = lowBound;
            this.UpBound = upBound;
        }

        public PetWashAttributeDataWrapper(string name, int attributeValue, int lowBound, int upBound, int refreshAttributeValue) : 
            this(name, attributeValue, lowBound, upBound)
        {
            this.RefreshAttributeValue = refreshAttributeValue;
        }
    }
}
