﻿using Common.Data;
using Common.Structs.ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetWashDataWrapper
    {
        public PetInfo PetInfo;
        public PbPetInfoRefresh PbPetInfoRefresh;

        public PetWashDataWrapper(PetInfo petInfo, PbPetInfoRefresh pbPetInfoRefresh)
        {
            this.PetInfo = petInfo;
            this.PbPetInfoRefresh = pbPetInfoRefresh;
        }
    }
}
