﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Data;
using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager;
using UnityEngine;
using Common.Utils;
using GameData;

namespace ModulePet
{
    public class PetStrengthenPanel : BasePanel
    {
        private PetInfo _petInfo;

        private KToggleGroup _toggleGroup;
        private RectTransform _rectToggleGroup;
        private PetLevelUp _levelUp;
        private PetQualityUp _qualityUp;
        private PetStarUp _starUp;
        private PetWash _wash;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_fanhui");
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            _rectToggleGroup = GetChildComponent<RectTransform>("ToggleGroup_biaoti");
            _levelUp = AddChildComponent<PetLevelUp>("Container_molingshengji");
            _qualityUp = AddChildComponent<PetQualityUp>("Container_molingtupo");
            _starUp = AddChildComponent<PetStarUp>("Container_molingshengxing");
            _wash = AddChildComponent<PetWash>("Container_molingxilian");
            ModalMask = GetChildComponent<StateImage>("Container_panelBg/ScaleImage_sharedzhezhao");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.PetStrengthen; }
        }

        public override void OnShow(object data)
        {
            AddListener();
            UIManager.Instance.DisablePanelCanvas(PanelIdEnum.Pet);
            PetStrengthenPanelDataWrapper wrapper = data as PetStrengthenPanelDataWrapper;
            _petInfo = wrapper.PetInfo;
            _toggleGroup.SelectIndex = (int) wrapper.PetStrengthen;
            OnSelectedIndexChanged(_toggleGroup, (int)wrapper.PetStrengthen);
            RefreshToggleGroup(wrapper.PetInfo);
        }

        public override void OnClose()
        {
            RemoveListener();
            UIManager.Instance.EnablePanelCanvas(PanelIdEnum.Pet);
        }

        private void AddListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            EventDispatcher.AddEventListener(PetEvents.UpdatePetInfo, OnRefreshToggleGroup);
            EventDispatcher.AddEventListener(PetEvents.AddExp, OnRefreshToggleGroup);
            EventDispatcher.AddEventListener<PetStrengthen>(PetEvents.ChangeStrengthPanelTab, OnChangeStrengthPanelTab);
        }

        private void RemoveListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            EventDispatcher.RemoveEventListener(PetEvents.UpdatePetInfo, OnRefreshToggleGroup);
            EventDispatcher.RemoveEventListener(PetEvents.AddExp, OnRefreshToggleGroup);
            EventDispatcher.RemoveEventListener<PetStrengthen>(PetEvents.ChangeStrengthPanelTab, OnChangeStrengthPanelTab);
        }

        private void OnChangeStrengthPanelTab(PetStrengthen index)
        {
            _toggleGroup.SelectIndex = (int)index;
            HillAll();
            switch (index)
            {
                case PetStrengthen.levelUp:
                    _levelUp.SetData(_petInfo, true);
                    break;
                case PetStrengthen.qualityUp:
                    _qualityUp.SetData(_petInfo, true);
                    break;
                case PetStrengthen.starUp:
                    _starUp.SetData(_petInfo, true);
                    break;
                case PetStrengthen.wash:
                    _wash.SetData(_petInfo, true);
                    break;
            }
        }

        private void OnSelectedIndexChanged(KToggleGroup toggleGroup, int index)
        {
            OnChangeStrengthPanelTab((PetStrengthen)index);
        }

        private void HillAll()
        {
            _levelUp.Visible = false;
            _qualityUp.Visible = false;
            _starUp.Visible = false;
            _wash.Visible = false;
        }

        private void OnRefreshToggleGroup()
        {
            RefreshToggleGroup(_petInfo);
        }

        private void RefreshToggleGroup(PetInfo petInfo)
        {
            PetData petData = PlayerDataManager.Instance.PetData;
            List<KToggle> toggleList = _toggleGroup.GetToggleList();
            toggleList[(int)PetStrengthen.levelUp].SetGreenPoint(petInfo.CanLevelUp());
            toggleList[(int)PetStrengthen.qualityUp].SetGreenPoint(petInfo.CanQualityUp());
            toggleList[(int)PetStrengthen.starUp].SetGreenPoint(petInfo.CanStarUp());
            Vector3 position = _rectToggleGroup.anchoredPosition;
            if (pet_refresh_limit_helper.IsPetWashUnlock(petInfo))
            {
                _rectToggleGroup.anchoredPosition = new Vector3(71, position.y, position.z);
                toggleList[(int)PetStrengthen.wash].Visible = true;
                toggleList[(int)PetStrengthen.wash].SetGreenPoint(petInfo.CanWash());
            }
            else
            {
                _rectToggleGroup.anchoredPosition = new Vector3(215, position.y, position.z);
                toggleList[(int)PetStrengthen.wash].Visible = false;
            }
        }
    }
}
