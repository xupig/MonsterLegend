﻿using Common.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetLevelUpAttributeItem : PetStrengthAttributeItem
    {
        protected override string GetShowTemplate()
        {
            return string.Concat("{0}", ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_NEW_DARK_GOLDEN, "：{1}"));
        }
    }
}
