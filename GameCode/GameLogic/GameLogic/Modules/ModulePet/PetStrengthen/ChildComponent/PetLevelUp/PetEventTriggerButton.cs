﻿using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetEventTriggerButton : KContainer
    {
        private int _itemId;
        private PetInfo _petInfo;

        private EventTriggerButton _button;
        private StateText _textNum;

        protected override void Awake()
        {
            _button = AddChildComponent<EventTriggerButton>("Container_EventTriggerButton");
            _button.Init(PetEvents.AddExp, GetLeftNumFunc);
            _textNum = GetChildComponent<StateText>("Label_txtXinxi");

            AddListener();
        }

        private int GetLeftNumFunc()
        {
            return PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemNum(_itemId);
        }

        public void Init(int itemId)
        {
            _itemId = itemId;
            int addExp = item_helper.GetPetAddExp(itemId);
            MogoGameObjectHelper.SetButtonLabel(_button, MogoLanguageUtil.GetContent(43410, addExp));
            GetChildComponent<StateIcon>("Container_EventTriggerButton/Container_stateIcon").SetIcon(item_helper.GetIcon(itemId));
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _button.onClick.AddListener(OnClick);
            _button.ResponseEvent.AddListener(OnResponseEvent);
        }

        private void RemoveListener()
        {
            _button.onClick.RemoveListener(OnClick);
            _button.ResponseEvent.RemoveListener(OnResponseEvent);
        }

        private void OnPetAddExp(PetInfo petInfo)
        {
            _button.OnReceiveEvent();
        }

        private void OnClick()
        {
            if (CurrentLevelReachMax()) return;
            PetManager.Instance.RequestAddExp(_petInfo.Id, _itemId, 1);
        }

        private bool CurrentLevelReachMax()
        {
            if (pet_quality_helper.CurrentLevelReachMax(_petInfo.Id, _petInfo.Quality, _petInfo.Level))
            {
                string content = string.Empty;
                if(pet_level_helper.IsMaxLevel(_petInfo.Id, _petInfo.Level))
                {
                    content = MogoLanguageUtil.GetContent(43467);
                    MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, true);
                }
                else
                {
                    content = MogoLanguageUtil.GetContent(43468);
                    MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, QualityUpAction, CancelAction,
                        MogoLanguageUtil.GetContent(43469), MogoLanguageUtil.GetContent(43470));
                }
                
                return true;
            }
            return false;
        }

        private void QualityUpAction()
        {
            EventDispatcher.TriggerEvent<PetStrengthen>(PetEvents.ChangeStrengthPanelTab, PetStrengthen.qualityUp);
        }

        private void CancelAction()
        {
        }

        private void OnResponseEvent(int num)
        {
            if (CurrentLevelReachMax()) return;
            PetManager.Instance.RequestAddExp(_petInfo.Id, _itemId, num);
        }

        public void Refresh(PetInfo petInfo)
        {
            _petInfo = petInfo;
            int num = PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemNum(_itemId);
            _textNum.CurrentText.text = MogoLanguageUtil.GetContent(43449, num);
        }
    }
}