﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetSkillItem : PetSkillIconWithLock
    {
        private KButton _btnTips;
        private KButton _btnLock;
        private KButton _btnUnlock;
        private StateText _textName;
        private StateText _textDesc;
        private StateText _textUnlock;


        protected override void Awake()
        {
            _textName = GetChildComponent<StateText>("Label_txtJinengmingcheng");
            _textDesc = GetChildComponent<StateText>("Label_txtJinengmiaoshu");
            _textUnlock = GetChildComponent<StateText>("Label_txtJinengjiesuo");
            _btnTips = GetChildComponent<KButton>("Button_wenhao");
            _btnLock = GetChildComponent<KButton>("Button_unlock");//图标是unlock 点击后lock
            _btnUnlock = GetChildComponent<KButton>("Button_lock");

            AddListener();
        }

        public override void Dispose()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnLock.onClick.AddListener(OnLock);
            _btnUnlock.onClick.AddListener(OnUnlock);
            _btnTips.onClick.AddListener(OnShowTips);
        }

        private void RemoveListener()
        {
            _btnLock.onClick.RemoveListener(OnLock);
            _btnUnlock.onClick.RemoveListener(OnUnlock);
            _btnTips.onClick.RemoveListener(OnShowTips);
        }

        private void OnLock()
        {
        }

        private void OnUnlock()
        {
        }

        private void OnShowTips()
        {
        }

        public override object Data
        {
            set
            {
                base.Data = value;
            }
        }
    }
}

