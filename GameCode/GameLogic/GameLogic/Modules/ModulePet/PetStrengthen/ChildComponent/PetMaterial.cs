﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/4 16:14:59
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using Common.Global;
using UnityEngine;
using Game.UI;
using UnityEngine.UI;
using Common.Base;
using Common.Utils;

namespace ModulePet
{
    public class PetMaterial : KList.KListItemBase
    {
        private ItemGrid _itemGrid;
        private StateText _textCost;
        private Resizer _resizer;

        protected override void Awake()
        {
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _itemGrid.Context = PanelIdEnum.PetStrengthen;
            _textCost = GetChildComponent<StateText>("Label_txtShuliang");
            _resizer = _textCost.gameObject.AddComponent<Resizer>();
        }

        public override object Data
        {
            set
            {
                Refresh(value as ItemData);
            }
        }

        public override void Dispose()
        {
        }

        private void Refresh(ItemData itemData)
        {
            int itemId = itemData.Id;
            int needNum = itemData.StackCount;
            int nowNum = PlayerAvatar.Player.GetItemNum(itemId);
            string nowNumStr = NumberCovertToAbbreviations(nowNum);
            string needNumStr = NumberCovertToAbbreviations(needNum);
            //needNumStr = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_NEW_DARK_GOLDEN, needNumStr);
            if (nowNum < needNum)
            {
                nowNumStr = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, nowNumStr);
                _textCost.CurrentText.text = string.Format("{0}/{1}", nowNumStr, needNumStr);
            }
            else
            {
                //nowNumStr = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_NEW_DARK_GOLDEN, nowNumStr);
                _textCost.CurrentText.text = string.Format("{0}/{1}", nowNumStr, needNumStr);
            }
            _itemGrid.SetItemData(itemId);
            _resizer.Width = _textCost.CurrentText.preferredWidth;
            RecalculateSize();
        }

        private string NumberCovertToAbbreviations(int sourceNumber)
        {
            string abbreviation = sourceNumber.ToString();
            if (sourceNumber > 10000)
            {
                float num1 = sourceNumber / 1000f;
                num1 = (int)num1;
                if (num1 % 10 == 0)
                {
                    abbreviation = MogoLanguageUtil.GetContent(1446, (num1 / 10f));
                }
                else
                {
                    abbreviation = MogoLanguageUtil.GetContent(1452, (num1 / 10f));
                }
            }
            return abbreviation;
        }
    }
}
