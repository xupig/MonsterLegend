﻿using Common.Data;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetStrengthAttributeItem : KList.KListItemBase
    {
        private const string ATTRIBUTE_NAME_PREFIX = "Label_attributeName";

        private StateText _textName;

        protected virtual string GetShowTemplate()
        {
            return "{0}：{1}";
        }

        protected override void Awake()
        {
            _textName = GetChildComponent<StateText>(ATTRIBUTE_NAME_PREFIX);
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                IAttribute data = value as IAttribute;
                _textName.CurrentText.text = string.Format(GetShowTemplate(), data.GetAttributeName(), data.GetAttributeValue());
            }
        }
    }
}
