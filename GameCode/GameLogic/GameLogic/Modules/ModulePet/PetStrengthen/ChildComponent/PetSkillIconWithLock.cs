﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModulePet
{
    public class PetSkillIconWithLock : KList.KListItemBase
    {
        public const int InvalidSkillId = -1;

        protected PetSkillDataWrapper wrapper;
        private int _lastSkillId = InvalidSkillId;
        private int _nowSkillId = InvalidSkillId;
        private int nowSkillId
        {
            get
            {
                return _nowSkillId;
            }
            set
            {
                _lastSkillId = _nowSkillId;
                _nowSkillId = value;
            }
        }

        protected PetSkillIcon skillIcon;
        protected KParticle particle;
        private GameObject _goLock;

        protected override void Awake()
        {
            skillIcon = gameObject.AddComponent<PetSkillIcon>();
            _goLock = GetChild("Image_petGridLock");
            particle = AddChildComponent<KParticle>("fx_ui_4_1_1_CD_01");
            if (particle != null)
            {
                particle.Stop();
            }
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                PetSkillDataWrapper wrapper = value as PetSkillDataWrapper;
                this.wrapper = wrapper;
                RefreshLock();
                RefreshSkill();
            }
        }

        protected virtual void RefreshLock()
        {
            _goLock.SetActive(IsLock());
        }

        protected bool IsLock()
        {
            return wrapper.PetSkill.quality > wrapper.Quality;
        }

        private void RefreshSkill()
        {
            if (!IsLock())
            {
                nowSkillId = wrapper.PetSkill.skillId;
                skillIcon.RefreshIcon(wrapper.PetSkill.skillId);
                skillIcon.CanShowTips = true;
            }
            else
            {
                skillIcon.Hide();
                skillIcon.CanShowTips = false;
            }
        }

        protected bool SkillIdChanged()
        {
            return nowSkillId != _lastSkillId;
        }

        public virtual void PlayParticle()
        {
            if (!IsLock() && SkillIdChanged())
            {
                particle.Play();
                particle.Position = new Vector3(-13f, 11f, 0);
            }
        }
    }
}
