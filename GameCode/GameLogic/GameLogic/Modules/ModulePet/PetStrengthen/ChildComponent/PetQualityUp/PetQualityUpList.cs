﻿using Common.ExtendComponent;
using Game.UI;
using Game.UI.UIComponent;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetQualityUpList : KContainer
    {
        private KList _list;
        private VerticalLocater _locaterRoot;

        protected override void Awake()
        {
            _list = gameObject.GetComponent<KList>();
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _locaterRoot = gameObject.AddComponent<VerticalLocater>();
        }

        public void SetDataList(List<string> dataList)
        {
            _list.SetDataList<PetQualityUpItem>(dataList);
            AddComponent();
            UpdateItemListLayout();
        }

        private void AddComponent()
        {
            _locaterRoot.RemoveAll();
            List<KList.KListItemBase> itemList = _list.ItemList;
            for (int i = 0; i < itemList.Count; i++)
            {
                KList.KListItemBase item = itemList[i];
                VerticalLocater locater = itemList[i].gameObject.GetComponent<VerticalLocater>();
                _locaterRoot.AddLocater(locater);
            }
        }

        protected void UpdateItemListLayout()
        {
            _locaterRoot.Locate();
        }

        private class PetQualityUpItem : KList.KListItemBase
        {
            private StateText _text;
            private VerticalLocater _verticalLocaterRoot;

            protected override void Awake()
            {
                _text = GetChildComponent<StateText>("Label_txtqualityUpContent");
                _verticalLocaterRoot = gameObject.AddComponent<VerticalLocater>();
                _verticalLocaterRoot.AddLocater(_text.gameObject.AddComponent<VerticalLocater>());
            }

            public override void Dispose()
            {
            }

            public override object Data
            {
                set
                {
                    _text.CurrentText.text = (string)value;
                }
            }
        }
    }
}
