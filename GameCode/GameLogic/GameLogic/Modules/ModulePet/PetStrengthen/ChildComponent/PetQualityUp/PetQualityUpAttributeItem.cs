﻿using Common.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetQualityUpAttributeItem : PetStrengthAttributeItem
    {
        protected override string GetShowTemplate()
        {
            return string.Concat("{0}", LangEnum.PET_QUALITY1.ToLanguage(), ":{1}");
        }
    }
}
