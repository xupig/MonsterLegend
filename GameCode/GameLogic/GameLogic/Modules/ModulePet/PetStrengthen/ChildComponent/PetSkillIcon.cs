﻿using Common.Base;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModulePet
{
    public class PetSkillIcon : KContainer, IPointerClickHandler
    {
        private int _skillId;
        public bool CanShowTips = true;

        private IconContainer _goIcon;
        private IconContainer _goQualityIcon;
        private IconContainer _goLevelIcon;

        protected override void Awake()
        {
            _goIcon = AddChildComponent<IconContainer>("Container_icon");
            _goQualityIcon = AddChildComponent<IconContainer>("Container_leibieicon");
            _goLevelIcon = AddChildComponent<IconContainer>("Container_dengjiicon");
        }

        public void RefreshIcon(int skillId)
        {
            _skillId = skillId;
            _goIcon.Visible = true;
            _goIcon.SetIcon(pet_skill_helper.GetIconId(skillId));
            if (pet_skill_helper.GetLevelIconId(skillId) != 0)
            {
                _goLevelIcon.Visible = true;
                _goLevelIcon.SetIcon(pet_skill_helper.GetLevelIconId(skillId));
            }
            else
            {
                _goLevelIcon.Visible = false;
            }
            if (pet_skill_helper.GetQualityIconId(skillId) != 0)
            {
                _goQualityIcon.Visible = true;
                _goQualityIcon.SetIcon(pet_skill_helper.GetQualityIconId(skillId));
            }
            else
            {
                _goQualityIcon.Visible = false;
            }
        }

        public void Hide()
        {
            _goIcon.Visible = false;
            _goQualityIcon.Visible = false;
            _goLevelIcon.Visible = false;
        }

        public override void OnPointerClick(PointerEventData evtData)
        {
            if (CanShowTips)
            {
                PanelIdEnum.PetSkillTips.Show(_skillId);
            }
        }
    }
}
