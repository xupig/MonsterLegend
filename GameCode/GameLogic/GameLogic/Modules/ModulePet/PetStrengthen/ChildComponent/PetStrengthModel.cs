﻿using Common.Base;
using Common.Data;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetStrengthModel : KContainer
    {
        private PetInfo _petInfo;

        private StateText _textName;
        private KDummyButton _btnTips;
        private PetModelComponent _petModel;
        private StateImage _imageFrame;

        protected override void Awake()
        {
            _textName = GetChildComponent<StateText>("Label_txtNianshouchongwu");
            _imageFrame = GetChildComponent<StateImage>("Image_shangchengpinzhikuang");
            _btnTips = AddChildComponent<KDummyButton>("ScaleImage_sharedzhezhao");
            _petModel = gameObject.AddComponent<PetModelComponent>();

            AddListener();
        }

        public Vector3 ModelPosition
        {
            set
            {
                _petModel.Position = value;
            }
        }

        public float ModelScale
        {
            set
            {
                _petModel.Scale = value;
            }
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnTips.onClick.AddListener(ShowTips);
        }

        private void RemoveListener()
        {
            _btnTips.onClick.RemoveListener(ShowTips);
        }

        private void ShowTips()
        {
            PetPopPanelDataWrapper wrapper = new PetPopPanelDataWrapper(PetPopType.gotPetInfo, PetContext.strengthen, _petInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.PetPop, wrapper);
        }

        public void Refresh(PetInfo petInfo)
        {
            _petInfo = petInfo;
            RefreshName(petInfo);
            RefreshQualityFrame(petInfo);
            RefreshModel(petInfo);
        }

        private void RefreshName(PetInfo petInfo)
        {
            _textName.CurrentText.text = pet_quality_helper.GetPetQualityName(_petInfo);
        }

        private void RefreshQualityFrame(PetInfo petInfo)
        {
            MogoShaderUtils.ChangeShader(_imageFrame.CurrentImage, MogoShaderUtils.IconShaderPath);
            _imageFrame.CurrentImage.color = ColorDefine.GetColorById(pet_quality_helper.GetNameColorId(petInfo.Id, petInfo.Quality));
        }

        private void RefreshModel(PetInfo petInfo)
        {
            _petModel.Refresh(pet_quality_helper.GetModelID(_petInfo));
        }
    }
}
