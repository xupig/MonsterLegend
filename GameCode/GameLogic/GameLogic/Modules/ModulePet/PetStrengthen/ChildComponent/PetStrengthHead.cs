﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetStrengthHead : KContainer
    {
        private PetInfo _petInfo;

        private StateText _textName;
        private KDummyButton _btnTips;
        private ItemGrid _itemGrid;
        private StarList _starList;

        protected override void Awake()
        {
            _textName = GetChildComponent<StateText>("Label_txtNianshouchongwu");
            _btnTips = AddChildComponent<KDummyButton>("ScaleImage_sharedzhezhao");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _starList = AddChildComponent<StarList>("Container_starList");
            _starList.SetStarData(5, "Container_star", "Image_xingxingBigIcon");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnTips.onClick.AddListener(OnShowTips);
        }

        private void RemoveListener()
        {
            _btnTips.onClick.RemoveListener(OnShowTips);
        }

        private void OnShowTips()
        {
            PetPopPanelDataWrapper wrapper = new PetPopPanelDataWrapper(PetPopType.gotPetInfo, PetContext.strengthen, _petInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.PetPop, wrapper);
        }

        public void Refresh(PetInfo petInfo)
        {
            _petInfo = petInfo;

            RefreshName(petInfo);
            RefreshItemGrid(petInfo);
            RefreshStarList(petInfo);
        }

        private void RefreshName(PetInfo petInfo)
        {
            _textName.CurrentText.text = pet_quality_helper.GetPetQualityName(_petInfo);
        }

        private void RefreshItemGrid(PetInfo petInfo)
        {
            _itemGrid.SetItemData(pet_helper.GetIcon(petInfo.Id), pet_quality_helper.GetNameColorId(petInfo.Id, petInfo.Quality));
        }

        private void RefreshStarList(PetInfo petInfo)
        {
            _starList.SetStarNum(petInfo.Star);
        }

        public Vector3 GetStarPosition(int star)
        {
            return _starList.GetStarPosition(star);
        }
    }
}
