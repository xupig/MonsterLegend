﻿using Common.Data;
using Common.Events;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetWashSkillWashItem : PetSkillIconWithLock
    {
        public bool IsLockHole = false;

        private KButton _btnTips;
        private KButton _btnLock;
        private KButton _btnUnlock;
        private StateText _textName;
        private StateText _textDesc;
        private StateText _textUnlock;

        protected override void Awake()
        {
            base.Awake();
            _textName = GetChildComponent<StateText>("Label_txtJinengmingcheng");
            _textDesc = GetChildComponent<StateText>("Label_txtJinengmiaoshu");
            _textUnlock = GetChildComponent<StateText>("Label_txtJinengjiesuo");
            _btnTips = GetChildComponent<KButton>("Button_wenhao");
            _btnLock = GetChildComponent<KButton>("Button_unlock");//图标是unlock 点击后lock
            _btnUnlock = GetChildComponent<KButton>("Button_lock");

            AddListener();
        }

        public override void Dispose()
        {
            base.Dispose();
            RemoveListener();
        }

        private void AddListener()
        {
            _btnLock.onClick.AddListener(OnLock);
            _btnUnlock.onClick.AddListener(OnUnlock);
            _btnTips.onClick.AddListener(OnShowTips);
        }

        private void RemoveListener()
        {
            _btnLock.onClick.RemoveListener(OnLock);
            _btnUnlock.onClick.RemoveListener(OnUnlock);
            _btnTips.onClick.RemoveListener(OnShowTips);
        }

        private void OnLock()
        {
            EventDispatcher.TriggerEvent<int>(PetEvents.RefreshLockHole, Index);
        }

        public void SetLock()
        {
            IsLockHole = true;
            RefreshLockHole();
        }

        private void OnUnlock()
        {
            IsLockHole = false;
            RefreshLockHole();
        }

        private void OnShowTips()
        {
            EventDispatcher.TriggerEvent<PetSkillDataWrapper>(PetEvents.ShowWashPossibleSkill, wrapper);
        }

        public override object Data
        {
            set
            {
                base.Data = value;
                IsLockHole = false;
                skillIcon.CanShowTips = false;
                RefreshDesc();
                RefreshLockHole();
                RefreshBtn();
            }
        }

        private void RefreshBtn()
        {
            if (IsLock())
            {
                _btnTips.Visible = false;
            }
            else
            {
                _btnTips.Visible = true;
            }
        }

        private void RefreshLockHole()
        {
            if (IsLock())
            {
                _btnUnlock.Visible = false;
                _btnLock.Visible = false;
                return;
            }
            if (IsLockHole)
            {
                _btnUnlock.Visible = true;
                _btnLock.Visible = false;
            }
            else
            {
                _btnUnlock.Visible = false;
                _btnLock.Visible = true;
            }
        }

        private void RefreshDesc()
        {
            if (IsLock())
            {
                _textUnlock.CurrentText.text = string.Format(LangEnum.PET_UNLOCK_QUALITY.ToLanguage(),
                    pet_quality_helper.GetPetQualityString(wrapper.PetId, wrapper.PetSkill.quality));
                _textName.Clear();
                _textDesc.Clear();
            }
            else
            {
                _textName.CurrentText.text = MogoLanguageUtil.GetContent(pet_skill_helper.GetName(wrapper.PetSkill.skillId));
                _textDesc.CurrentText.text = pet_skill_helper.GetDescStr(wrapper.PetSkill.skillId);
                _textDesc.RecalculateCurrentStateHeight();
                _textUnlock.Clear();
            }
        }

        public override void PlayParticle()
        {
            if (!IsLock() && SkillIdChanged())
            {
                if (InMask())
                {
                    particle.Play();
                    particle.Position = new Vector3(69f, -4f, 0f);
                }
            }
        }

        private bool InMask()
        {
            Camera uiCamera = UIManager.Instance.UICamera;
            return RectTransformUtility.RectangleContainsScreenPoint(transform.parent.parent.GetComponent<RectTransform>(),
                uiCamera.WorldToScreenPoint(transform.position), uiCamera);
        }
    }
}
