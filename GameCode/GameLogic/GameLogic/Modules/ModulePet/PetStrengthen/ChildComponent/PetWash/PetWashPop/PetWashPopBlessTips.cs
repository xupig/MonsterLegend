﻿using Common.Data;
using Common.Utils;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetWashPopBlessTips : KContainer
    {
        private KButton _btnClose;
        private StateText _textDesc0;
        private StateText _textDesc1;

        public KComponentEvent OnClose = new KComponentEvent();

        protected override void Awake()
        {
            _btnClose = GetChildComponent<KButton>("Button_close");
            _textDesc0 = GetChildComponent<StateText>("Label_txtXilian");
            _textDesc1 = GetChildComponent<StateText>("Label_txtXiangxineirong");

            _textDesc0.CurrentText.text = MogoLanguageUtil.GetContent(43454);
            _textDesc1.CurrentText.text = MogoLanguageUtil.GetContent(43455);

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(OnClickClose);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(OnClickClose);
        }

        private void OnClickClose()
        {
            OnClose.Invoke();
        }
    }
}
