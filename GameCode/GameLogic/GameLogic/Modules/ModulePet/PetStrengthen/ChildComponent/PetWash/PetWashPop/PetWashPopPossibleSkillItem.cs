﻿using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetWashPopPossibleSkillItem : KList.KListItemBase
    {
        private PetSkillIcon _skillIcon;
        private StateText _textName;
        private StateText _textDesc;

        protected override void Awake()
        {
            _skillIcon = gameObject.AddComponent<PetSkillIcon>();
            _skillIcon.CanShowTips = false;
            _textName = GetChildComponent<StateText>("Label_txtJinengmingcheng");
            _textDesc = GetChildComponent<StateText>("Label_txtJinengmiaoshu");
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                int skillId = (int)value;
                _skillIcon.RefreshIcon(skillId);
                _textName.CurrentText.text = MogoLanguageUtil.GetContent(pet_skill_helper.GetName(skillId));
                _textDesc.CurrentText.text = pet_skill_helper.GetDescStr(skillId);
                _textDesc.RecalculateCurrentStateHeight();
            }
        }
    }
}
