﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetWashSkillWash : KContainer
    {
        private PetInfo _petInfo;

        private KList _list;

        protected override void Awake()
        {
            _list = GetChildComponent<KList>("ScrollView_molingjineng/mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _list.SetGap(0, 0);

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            EventDispatcher.AddEventListener<int>(PetEvents.RefreshLockHole, OnLockHole);
        }

        private void RemoveListener()
        {
            EventDispatcher.RemoveEventListener<int>(PetEvents.RefreshLockHole, OnLockHole);
        }

        private void OnLockHole(int index)
        {
            int lockNum = GetLockHoleList().Count;
            if (pet_refresh_limit_helper.IsPetRefreshLimitExist(PetWashType.skill, lockNum + 1)
                && _petInfo.IsWashTypeUnlock(PetWashType.skill, lockNum + 1))
            {
                PetWashSkillWashItem item = _list.ItemList[index] as PetWashSkillWashItem;
                item.SetLock();
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(43480));
            }
        }

        public void SetData(PetInfo petInfo, bool playEffect)
        {
            _petInfo = petInfo;
            List<PetSkill> petSkillList = petInfo.GetAllSkillList();
            List<PetSkillDataWrapper> wrapperList = pet_quality_helper.ToPetSkillDataWrapperList(petInfo.Id, petSkillList, petInfo.Quality, petInfo.WashLevel);
            _list.SetDataList<PetWashSkillWashItem>(wrapperList);
            if (playEffect)
            {
                PlayEffect();
            }
        }

        public List<int> GetLockHoleList()
        {
            List<int> lockHoleList = new List<int>();
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                PetWashSkillWashItem item = _list.ItemList[i] as PetWashSkillWashItem;
                if(item.IsLockHole)
                {
                    lockHoleList.Add(item.Index + 1);
                }
            }
            return lockHoleList;
        }

        private void PlayEffect()
        {
            List<KList.KListItemBase> itemList = _list.ItemList;
            for (int i = 0; i < itemList.Count; i++)
            {
                (itemList[i] as PetWashSkillWashItem).PlayParticle();
            }
        }
    }
}
