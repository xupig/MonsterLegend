﻿using Common.ExtendComponent;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetWashQualityItem : KList.KListItemBase
    {
        private TweenProgressBar _progressBar;
        private StateText _textName;
        private StateText _textNow;
        private StateText _textMax;

        protected override void Awake()
        {
            _progressBar = AddChildComponent<TweenProgressBar>("ProgressBar_jindu");
            _textName = GetChildComponent<StateText>("Label_txtDangqianzizhi");
            _textNow = GetChildComponent<StateText>("Label_txtjingyanzhi");
            _textMax = GetChildComponent<StateText>("Label_txtXilianfanwei");
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                PetWashAttributeDataWrapper wrapper = value as PetWashAttributeDataWrapper;
                _textName.CurrentText.text = wrapper.Name;
                _textNow.CurrentText.text = wrapper.AttributeValue.ToString();
                _textMax.CurrentText.text = wrapper.UpBound.ToString();
                int now = wrapper.AttributeValue;
                if (now < wrapper.LowBound)
                {
                    _progressBar.SetProgress(0, 1);
                }
                else if (now > wrapper.UpBound)
                {
                    _progressBar.SetProgress(1, 1);
                }
                else
                {
                    _progressBar.SetProgress(now - wrapper.LowBound, wrapper.UpBound - wrapper.LowBound);
                }
            }
        }

        public void ClearProgress()
        {
            _progressBar.Clear();
        }
    }
}
