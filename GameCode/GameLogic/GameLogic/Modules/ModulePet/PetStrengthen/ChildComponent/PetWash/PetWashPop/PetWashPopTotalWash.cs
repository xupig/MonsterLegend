﻿using Common.Data;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using Mogo.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetWashPopTotalWash : KContainer
    {
        private KButton _btnReplace;
        private KButton _btnCalcel;
        private KList _oldSkillList;
        private KList _newSkillList;
        private KList _attributeList;

        public KComponentEvent OnReplace = new KComponentEvent();
        public KComponentEvent OnCancel = new KComponentEvent();

        protected override void Awake()
        {
            _btnReplace = GetChildComponent<KButton>("Button_tihuan");
            _btnCalcel = GetChildComponent<KButton>("Button_quxiao");
            _oldSkillList = GetChildComponent<KList>("List_yuanjineng");
            _oldSkillList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _oldSkillList.SetGap(0, 1);
            _newSkillList = GetChildComponent<KList>("List_xilianyuanjineng");
            _newSkillList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _newSkillList.SetGap(0, 1);

            _attributeList = GetChildComponent<KList>("List_zizhixilian");
            _attributeList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _attributeList.SetGap(7, 0);

            MogoUtils.AdaptScreen(GetChildComponent<StateImage>("ScaleImage_sharedZhezhao"));

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnReplace.onClick.AddListener(OnClickReplace);
            _btnCalcel.onClick.AddListener(OnClickCancel);
        }

        private void RemoveListener()
        {
            _btnReplace.onClick.RemoveListener(OnClickReplace);
            _btnCalcel.onClick.RemoveListener(OnClickCancel);
        }

        private void OnClickReplace()
        {
            OnReplace.Invoke();
        }

        private void OnClickCancel()
        {
            OnCancel.Invoke();
        }

        public object Data
        {
            set
            {
                Visible = true;
                PetWashDataWrapper wrapper = value as PetWashDataWrapper;
                RefreshOldSkill(wrapper.PetInfo);
                RefreshNewSkill(wrapper.PetInfo, wrapper.PbPetInfoRefresh);
                RefreshAttributeList(wrapper.PetInfo, wrapper.PbPetInfoRefresh);
            }
        }

        private void RefreshOldSkill(PetInfo petInfo)
        {
            List<PetSkill> petSkillList = petInfo.GetAllSkillList();
            List<PetSkillDataWrapper> wrapperList = pet_quality_helper.ToPetSkillDataWrapperList(petInfo.Id, petSkillList, petInfo.Quality);
            _oldSkillList.SetDataList<PetSkillIconWithLock>(wrapperList);
        }

        private void RefreshNewSkill(PetInfo petInfo, PbPetInfoRefresh pbPetInfoRefresh)
        {
            List<PetSkill> petSkillList = petInfo.GetAllSkillList(pbPetInfoRefresh);
            List<PetSkillDataWrapper> wrapperList = pet_quality_helper.ToPetSkillDataWrapperList(petInfo.Id, petSkillList, petInfo.Quality);
            _newSkillList.SetDataList<PetSkillIconWithLock>(wrapperList);
        }

        private void RefreshAttributeList(PetInfo petInfo, PbPetInfoRefresh pbPetInfoRefresh)
        {
            List<PetWashAttributeDataWrapper> wrapperList = PetWashUtils.GetWashAttributeDataWrapperList(petInfo, pbPetInfoRefresh);
            _attributeList.SetDataList<PetWashPopAttributeItem>(wrapperList);
        }
    }
}
