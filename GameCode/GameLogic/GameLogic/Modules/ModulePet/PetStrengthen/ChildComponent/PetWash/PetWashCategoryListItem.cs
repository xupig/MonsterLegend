﻿using Common.ExtendComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetWashCategoryListItem : CategoryListItem
    {
        private GameObject _goLock;
        private GameObject _goCheckmarkLock;

        protected override void Awake()
        {
            base.Awake();
            _goLock = GetChild("Button_back/suo");
            _goCheckmarkLock = GetChild("Button_checkmark/suo");
        }

        public void SetLock(bool isLock)
        {
            _goLock.SetActive(isLock);
            _goCheckmarkLock.SetActive(isLock);
        }
    }
}
