﻿using Common.Global;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetWashPopAttributeItem : KList.KListItemBase
    {
        private StateText _textName;
        private StateText _textWashValue;
        private StateText _textRange;
        
        protected override void Awake()
        {
            _textName = GetChildComponent<StateText>("Label_txtdangqianzizhi");
            _textWashValue = GetChildComponent<StateText>("Label_txtxilianzizhi");
            _textRange = GetChildComponent<StateText>("Label_txtfanwei");
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                PetWashAttributeDataWrapper wrapper = value as PetWashAttributeDataWrapper;
                _textName.CurrentText.text = string.Format("{0}+{1}", wrapper.Name, wrapper.AttributeValue);
                if (wrapper.AttributeValue > wrapper.RefreshAttributeValue)
                {
                    _textWashValue.CurrentText.text = string.Format("-{0}", wrapper.AttributeValue - wrapper.RefreshAttributeValue);
                    _textWashValue.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_RED);
                }
                else
                {
                    _textWashValue.CurrentText.text = string.Format("+{0}", wrapper.RefreshAttributeValue - wrapper.AttributeValue);
                    _textWashValue.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_GREEN);
                }
                
                _textRange.CurrentText.text = string.Format("{0}~{1}", wrapper.LowBound, wrapper.UpBound);
            }
        }
    }
}
