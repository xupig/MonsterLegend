﻿using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetWashTotalWash : KContainer
    {
        private PetWashQualityWash _qualityWash;

        private GameObject _goSkill;
        private KList _listSkill;

        public KComponentEvent OnClickTips = new KComponentEvent();

        protected override void Awake()
        {
            _qualityWash = gameObject.AddComponent<PetWashQualityWash>();
            InitSkill();

            AddListener();
        }

        private void InitSkill()
        {
            _goSkill = GetChild("Container_jinengxilian");
            _listSkill = GetChildComponent<KList>("Container_jinengxilian/List_jinengxilian");
            _listSkill.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _listSkill.SetGap(0, 5);
        }

        private void AddListener()
        {
            _qualityWash.OnClickTips.AddListener(OnClickBtnTips);
        }
        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void RemoveListener()
        {
            _qualityWash.OnClickTips.RemoveListener(OnClickBtnTips);
        }

        private void OnClickBtnTips()
        {
            OnClickTips.Invoke();
        }

        public void SetData(PetInfo petInfo, bool playEffect)
        {
            RefreshSkill(petInfo);
            _qualityWash.SetData(petInfo, playEffect);
            if (playEffect)
            {
                PlayEffect();
            }
        }

        private void RefreshSkill(PetInfo petInfo)
        {
            _goSkill.SetActive(true);
            List<PetSkill> petSkillList = petInfo.GetAllSkillList();
            List<PetSkillDataWrapper> wrapperList = pet_quality_helper.ToPetSkillDataWrapperList(petInfo.Id, petSkillList, petInfo.Quality);
            _listSkill.SetDataList<PetSkillIconWithLock>(wrapperList);
        }

        private void PlayEffect()
        {
            List<KList.KListItemBase> itemList = _listSkill.ItemList;
            for (int i = 0; i < itemList.Count; i++)
            {
                (itemList[i] as PetSkillIconWithLock).PlayParticle();
            }
        }
    }
}
