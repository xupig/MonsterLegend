﻿using Common.Data;
using Game.UI.UIComponent;
using GameData;
using Mogo.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetWashPopPossibleSkill : KContainer
    {
        private KButton _btnSure;
        private KScrollView _scrollView;
        private KList _skillList;

        public KComponentEvent OnClose = new KComponentEvent();

        protected override void Awake()
        {
            _btnSure = GetChildComponent<KButton>("Button_queding");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_jinengliebiao");
            _skillList = GetChildComponent<KList>("ScrollView_jinengliebiao/mask/content");
            _skillList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnSure.onClick.AddListener(OnSure);
        }

        private void RemoveListener()
        {
            _btnSure.onClick.RemoveListener(OnSure);
        }

        private void OnSure()
        {
            OnClose.Invoke();
        }

        public object Data
        {
            set
            {
                Visible = true;
                PetSkillDataWrapper wrapper = value as PetSkillDataWrapper;
                List<int> skillIdList = pet_refresh_helper.GetSkillIdList(wrapper.PetId, wrapper.WashLevel, wrapper.PetSkill.index);
                _skillList.SetDataList<PetWashPopPossibleSkillItem>(skillIdList);
            }
        }
    }
}
