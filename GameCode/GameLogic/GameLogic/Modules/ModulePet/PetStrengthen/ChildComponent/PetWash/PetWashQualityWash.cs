﻿using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetWashQualityWash : KContainer
    {
        private KList _listAttribute;
        private KButton _btnTips;
        private StateText _textExp;
        private TweenProgressBar _progressBar;
        private StateText _textLevel;

        public KComponentEvent OnClickTips = new KComponentEvent();

        protected override void Awake()
        {
            InitAttribute();
            _btnTips = GetChildComponent<KButton>("Button_wenhao");
            _progressBar = AddChildComponent<TweenProgressBar>("ProgressBar_jindu");
            _textExp = GetChildComponent<StateText>("Label_txtjingyanzhi");
            _textLevel = GetChildComponent<StateText>("Label_txtXilian");

            AddListener();
        }

        private void InitAttribute()
        {
            _listAttribute = GetChildComponent<KList>("List_attribute");
            _listAttribute.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _listAttribute.SetGap(2, 0);
        }

        private void AddListener()
        {
            _btnTips.onClick.AddListener(OnClickBtnTips);
        }
        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void RemoveListener()
        {
            _btnTips.onClick.RemoveListener(OnClickBtnTips);
        }

        private void OnClickBtnTips()
        {
            OnClickTips.Invoke();
        }

        protected override void OnEnable()
        {
            _progressBar.Clear();
        }

        public void SetData(PetInfo petInfo, bool playEffect)
        {
            RefreshTitle(petInfo);
            RefreshProgress(petInfo);
            RefreshAttribute(petInfo, playEffect);
        }

        private void RefreshTitle(PetInfo petInfo)
        {
            _textLevel.CurrentText.text = MogoLanguageUtil.GetContent(43457, petInfo.WashLevel);
        }

        private void RefreshProgress(PetInfo petInfo)
        {
            int nextExp = pet_refresh_helper.GetNextExp(petInfo.Id, petInfo.WashLevel);
            _textExp.CurrentText.text = string.Format("{0}/{1}", petInfo.WashExp, nextExp);
            if (nextExp == 0)
            {
                //满级 TODO
                _progressBar.SetProgress(1, 1);
            }
            else
            {
                _progressBar.SetProgress(petInfo.WashExp, nextExp);
            }
        }

        private void RefreshAttribute(PetInfo petInfo, bool playEffect)
        {
            List<PetWashAttributeDataWrapper> wrapperList = PetWashUtils.GetWashAttributeDataWrapperList(petInfo.Id, petInfo.WashLevel, petInfo.StarAttributeList);
            if (!playEffect)
            {
                for (int i = 0; i < _listAttribute.ItemList.Count; i++)
                {
                    PetWashQualityItem item = _listAttribute.ItemList[i] as PetWashQualityItem;
                    item.ClearProgress();
                }
            }
            _listAttribute.SetDataList<PetWashQualityItem>(wrapperList);
        }
    }
}
