﻿using Common.Data;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetWashUtils
    {
        public static List<PetWashAttributeDataWrapper> GetWashAttributeDataWrapperList(int petId, int washLevel, List<RoleAttributeData> attributeDataList)
        {
            List<PetWashAttributeDataWrapper> wrapperList = new List<PetWashAttributeDataWrapper>();
            Dictionary<string, string[]> attributeRange = pet_refresh_helper.GetAttributeValueRange(petId, washLevel);
            for(int i = 0;i < attributeDataList.Count;i++)
            {
                RoleAttributeData attributeData = attributeDataList[i];
                attributeData.attriName = attri_config_helper.GetAttributeName(attributeData.attriId);
                string attributeIdStr = attributeData.attriId.ToString();
                PetWashAttributeDataWrapper wrapper = new PetWashAttributeDataWrapper(
                    MogoLanguageUtil.GetContent(1447, attributeData.attriName),
                    attributeData.AttriValue, int.Parse(attributeRange[attributeIdStr][0]), int.Parse(attributeRange[attributeIdStr][1]));
                wrapperList.Add(wrapper);
            }
            return wrapperList;
        }

        public static List<PetWashAttributeDataWrapper> GetWashAttributeDataWrapperList(PetInfo petInfo, PbPetInfoRefresh pbPetInfoRefresh)
        {
            List<RoleAttributeData> attributeList = petInfo.StarAttributeList;
            List<PetWashAttributeDataWrapper> wrapperList = new List<PetWashAttributeDataWrapper>();
            Dictionary<string, string[]> attributeRange = pet_refresh_helper.GetAttributeValueRange(petInfo.Id, petInfo.WashLevel);
            for (int i = 0; i < attributeList.Count; i++)
            {
                RoleAttributeData attributeData = attributeList[i];
                attributeData.attriName = attri_config_helper.GetAttributeName(attributeData.attriId);
                string attributeIdStr = attributeData.attriId.ToString();
                int refreshAttributeValue = GetRefreshAttributeValue(attributeData.attriId, pbPetInfoRefresh.petStarAttriList);
                PetWashAttributeDataWrapper wrapper = new PetWashAttributeDataWrapper(string.Format("{0}{1}", attributeData.attriName, MogoLanguageUtil.GetContent(43476)), attributeData.AttriValue, int.Parse(attributeRange[attributeIdStr][0]), int.Parse(attributeRange[attributeIdStr][1]), refreshAttributeValue);
                wrapperList.Add(wrapper);
            }
            return wrapperList;
        }

        private static int GetRefreshAttributeValue(int attriId, List<PbPetStarAttri> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].attri_id == attriId)
                {
                    return list[i].attri_value;
                }
            }
            return 0;
        }
    }
}
