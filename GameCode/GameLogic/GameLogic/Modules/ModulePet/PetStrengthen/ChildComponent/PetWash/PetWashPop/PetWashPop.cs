﻿using Common.Data;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using Mogo.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetWashPop : KContainer
    {
        public enum Type
        {
            possibleSkill,
            blessTip,
            qualityWash,
            skillWash,
            totalWash,
        }

        public PetInfo PetInfo;

        private PetWashPopPossibleSkill _possibleSkill;
        private PetWashPopBlessTips _blessTips;
        private PetWashPopQualityWash _qualityWash;
        private PetWashPopSkillWash _skillWash;
        private PetWashPopTotalWash _totalWash;

        protected override void Awake()
        {
            _possibleSkill = AddChildComponent<PetWashPopPossibleSkill>("Container_juanxian");
            _blessTips = AddChildComponent<PetWashPopBlessTips>("Container_xiaotanchuang");
            _qualityWash = AddChildComponent<PetWashPopQualityWash>("Container_zizhixilian");
            _skillWash = AddChildComponent<PetWashPopSkillWash>("Container_jinengxilian");
            _totalWash = AddChildComponent<PetWashPopTotalWash>("Container_quanbuxilian");

            Vector3 position = transform.localPosition;
            transform.localPosition = new Vector3(position.x, position.y, -1000);

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _blessTips.OnClose.AddListener(OnClose);
            _possibleSkill.OnClose.AddListener(OnClose);
            _totalWash.OnReplace.AddListener(OnReplace);
            _totalWash.OnCancel.AddListener(OnCancel);
            _skillWash.OnReplace.AddListener(OnReplace);
            _skillWash.OnCancel.AddListener(OnCancel);
            _qualityWash.OnReplace.AddListener(OnReplace);
            _qualityWash.OnCancel.AddListener(OnCancel);
        }

        private void RemoveListener()
        {
            _blessTips.OnClose.RemoveListener(OnClose);
            _possibleSkill.OnClose.RemoveListener(OnClose);
            _totalWash.OnReplace.RemoveListener(OnReplace);
            _totalWash.OnCancel.RemoveListener(OnCancel);
            _skillWash.OnReplace.RemoveListener(OnReplace);
            _skillWash.OnCancel.RemoveListener(OnCancel);
            _qualityWash.OnReplace.RemoveListener(OnReplace);
            _qualityWash.OnCancel.RemoveListener(OnCancel);
        }

        private void OnReplace()
        {
            PetManager.Instance.RequestWashConfirm(PetInfo.Id, true);
        }

        private void OnCancel()
        {
            PetManager.Instance.RequestWashConfirm(PetInfo.Id, false);
        }

        private void OnClose()
        {
            Visible = false;
        }

        public void Show(Type type, object data = null)
        {
            Visible = true;
            HideAll();
            switch (type)
            {
                case Type.possibleSkill:
                    _possibleSkill.Data = data;
                    break;
                case Type.blessTip:
                    _blessTips.Visible = true;
                    break;
                case Type.qualityWash:
                    _qualityWash.Data = data;
                    break;
                case Type.skillWash:
                    _skillWash.Data = data;
                    break;
                case Type.totalWash:
                    _totalWash.Data = data;
                    break;
            }
        }

        private void HideAll()
        {
            _possibleSkill.Visible = false;
            _blessTips.Visible = false;
            _qualityWash.Visible = false;
            _skillWash.Visible = false;
            _totalWash.Visible = false;
        }
    }
}
