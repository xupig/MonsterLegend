﻿using Common.Data;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using Mogo.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetWashPopQualityWash : KContainer
    {
        private KButton _btnReplace;
        private KButton _btnCalcel;
        private KList _attributeList;

        public KComponentEvent OnReplace = new KComponentEvent();
        public KComponentEvent OnCancel = new KComponentEvent();

        protected override void Awake()
        {
            _btnReplace = GetChildComponent<KButton>("Button_tihuan");
            _btnCalcel = GetChildComponent<KButton>("Button_quxiao");

            _attributeList = GetChildComponent<KList>("List_zizhixilian");
            _attributeList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _attributeList.SetGap(7, 0);

            MogoUtils.AdaptScreen(GetChildComponent<StateImage>("ScaleImage_sharedZhezhao"));
            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnReplace.onClick.AddListener(OnClickReplace);
            _btnCalcel.onClick.AddListener(OnClickCancel);
        }

        private void RemoveListener()
        {
            _btnReplace.onClick.RemoveListener(OnClickReplace);
            _btnCalcel.onClick.RemoveListener(OnClickCancel);
        }

        private void OnClickReplace()
        {
            OnReplace.Invoke();
        }

        private void OnClickCancel()
        {
            OnCancel.Invoke();
        }

        public object Data
        {
            set
            {
                Visible = true;
                PetWashDataWrapper wrapper = value as PetWashDataWrapper;
                RefreshAttributeList(wrapper.PetInfo, wrapper.PbPetInfoRefresh);
            }
        }

        private void RefreshAttributeList(PetInfo petInfo, PbPetInfoRefresh pbPetInfoRefresh)
        {
            List<PetWashAttributeDataWrapper> wrapperList = PetWashUtils.GetWashAttributeDataWrapperList(petInfo, pbPetInfoRefresh);
            _attributeList.SetDataList<PetWashPopAttributeItem>(wrapperList);
        }
    }
}
