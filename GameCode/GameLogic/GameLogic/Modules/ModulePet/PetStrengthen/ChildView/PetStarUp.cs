﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/4 14:07:08
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using UnityEngine;

namespace ModulePet
{
    public class PetStarUp : KContainer
    {
        private PetInfo _petInfo;
        private Vector3 _nowPosition;

        private PetStrengthHead _headNow;
        private KList _nowList;

        private PetStrengthHead _headNext;
        private KList _nextList;

        private PetMaterial _material;
        private KContainer _containerArrow;
        private KButton _btnStarUp;
        private GameObject _goFullStar;
        private KParticle _particleBtn;

        protected override void Awake()
        {
            InitNow();
            InitNext();
            InitCost();
            _btnStarUp = GetChildComponent<KButton>("Button_tupo");
            _containerArrow = GetChildComponent<KContainer>("Container_arrow");
            _goFullStar = GetChild("Label_txtmanji");
            _particleBtn = AddChildComponent<KParticle>("Button_tupo/fx_ui_3_2_lingqu");
            _particleBtn.transform.localScale = new Vector3(1.43f, 1.12f, 1f);
        }

        private void InitNow()
        {
            _headNow = AddChildComponent<PetStrengthHead>("Container_chongwuleft");
            _nowPosition = _headNow.transform.localPosition;
            _nowList = GetChildComponent<KList>("Container_chongwuleft/ScrollView_shuxing/mask/content");
            _nowList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _nowList.SetGap(7, 0);
        }

        private void InitNext()
        {
            _headNext = AddChildComponent<PetStrengthHead>("Container_chongwuright");
            _nextList = GetChildComponent<KList>("Container_chongwuright/ScrollView_shuxing/mask/content");
            _nextList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _nextList.SetGap(7, 0);
        }

        private void InitCost()
        {
            _material = AddChildComponent<PetMaterial>("Container_cailiao");
        }

        protected override void OnDestroy()
        {
            _petInfo = null;
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnStarUp.onClick.AddListener(OnClick);
            EventDispatcher.AddEventListener<PetInfo>(PetEvents.StarUp, OnStarUp);
            EventDispatcher.AddEventListener(PetEvents.UpdatePetInfo, OnRefresh);
        }

        private void RemoveListener()
        {
            _btnStarUp.onClick.RemoveListener(OnClick);
            EventDispatcher.RemoveEventListener<PetInfo>(PetEvents.StarUp, OnStarUp);
            EventDispatcher.RemoveEventListener(PetEvents.UpdatePetInfo, OnRefresh);
        }

        private void OnStarUp(PetInfo petInfo)
        {
            PetPopPanelDataWrapper wrapper = new PetPopPanelDataWrapper(PetPopType.starUpSuccess, PetContext.strengthen, _petInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.PetPop, wrapper);
        }

        private void OnClick()
        {
            PetManager.Instance.RequestAddStar(_petInfo.Id);
        }

        private void OnShowNowTips()
        {
            PetPopPanelDataWrapper wrapper = new PetPopPanelDataWrapper(PetPopType.gotPetInfo, PetContext.strengthen, _petInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.PetPop, wrapper);
        }

        private void OnShowNextTips()
        {
            PetInfo nextPetInfo = _petInfo.Copy();
            nextPetInfo.Star += 1;
            PetPopPanelDataWrapper wrapper = new PetPopPanelDataWrapper(PetPopType.gotPetInfo, PetContext.strengthen, nextPetInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.PetPop, wrapper);
        }

        public void SetData(PetInfo petInfo, bool doTween)
        {
            Visible = true;
            _petInfo = petInfo;
            Refresh(petInfo, doTween);
        }

        private void OnRefresh()
        {
            Refresh(_petInfo, false);
        }

        private void Refresh(PetInfo petInfo, bool doTween)
        {
            RefreshFullLevel(petInfo);
            RefreshNow(petInfo);
            RefreshNext(petInfo);
            RefreshCost(petInfo);
            RefreshBtn(petInfo);
            if (doTween)
            {
                DoTween(petInfo);
            }
        }

        private void RefreshNow(PetInfo petInfo)
        {
            _headNow.Refresh(petInfo);
            List<RoleAttributeData> dataList = petInfo.StarAttributeList;
            List<IAttribute> ilist = pet_star_helper.ParseUpLowbound(dataList);
            _nowList.SetDataList<PetQualityUpAttributeItem>(ilist);
        }

        private void RefreshNext(PetInfo petInfo)
        {
            if(!pet_star_helper.ReachMaxStar(petInfo.Id, petInfo.Star))
            {
                PetInfo nextPetInfo = petInfo.Copy();
                nextPetInfo.Star += 1;
                _headNext.Refresh(nextPetInfo);
                List<RoleAttributeData> dataList = pet_star_helper.GetPetStarAttributeDataList(petInfo.Id, petInfo.Star + 1);
                List<IAttribute> ilist = pet_star_helper.ParseUpLowbound(dataList);
                _nextList.SetDataList<PetQualityUpAttributeItem>(ilist);
            }
        }

        private void RefreshBtn(PetInfo petInfo)
        {
            PetData petData = PlayerDataManager.Instance.PetData;
            if (petInfo.CanStarUp())
            {
                _particleBtn.Play(true);
            }
            else
            {
                _particleBtn.Stop();
            }
        }

        private void RefreshCost(PetInfo petInfo)
        {
            ItemData itemData = pet_star_helper.GetStarUpCost(petInfo.Id, petInfo.Star);
            if (itemData != null)
            {
                _material.Data = itemData;
                _material.Visible = true;
            }
            else
            {
                _material.Visible = false;
            }
        }

        private void RefreshFullLevel(PetInfo petInfo)
        {
            if (!pet_star_helper.ReachMaxStar(petInfo.Id, petInfo.Star))
            {
                _headNow.transform.localPosition = _nowPosition;
                _headNext.Visible = true;
                _containerArrow.Visible = true;
                _material.Visible = true;
                _btnStarUp.Visible = true;
                _goFullStar.SetActive(false);
            }
            else
            {
                _headNow.transform.localPosition = new Vector3(_nowPosition.x + 250, _nowPosition.y, _nowPosition.z);
                _headNext.Visible = false;
                _containerArrow.Visible = false;
                _material.Visible = false;
                _btnStarUp.Visible = false;
                _goFullStar.SetActive(true);
            }
        }

        private void DoTween(PetInfo petInfo)
        {
            if (!pet_star_helper.ReachMaxStar(petInfo.Id, petInfo.Star))
            {
                TweenViewMove.Begin(_headNow.gameObject, MoveType.Show, MoveDirection.Left, true);
                TweenViewMove.Begin(_headNext.gameObject, MoveType.Show, MoveDirection.Right, true);
            }
            else
            {
                TweenViewMove.Begin(_headNow.gameObject, MoveType.Show, MoveDirection.Right, true);
            }
        }
    }
}
