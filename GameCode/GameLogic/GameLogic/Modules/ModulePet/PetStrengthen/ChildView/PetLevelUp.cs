﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/4 14:06:26
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using UnityEngine;

namespace ModulePet
{
    public class PetLevelUp : KContainer
    {
        private int[] PetExpItemArray = new int[3] { PetData.SmallPetExpItemId, PetData.MiddlePetExpItemId, PetData.LargePetExpItemId };
        private float InitHeadX;
        private float InitNowX;

        private PetInfo _petInfo;
        private PetData _petData
        {
            get
            {
                return PlayerDataManager.Instance.PetData;
            }
        }

        private List<PetEventTriggerButton> _eventTriggerButtonList;
        private Locater _locaterHead;
        private PetStrengthModel _petModel;

        private StateText _textLevel;
        private StateText _textExp;
        private TweenProgressBar _progressBar;

        private KList _listNow;
        private StateText _textNowLevel;
        private Locater _locaterNow;
        private KParticle _particleLevelUp1;
        private KParticle _particleLevelUp2;

        private KList _listNext;
        private StateText _textNextLevel;
        private GameObject _goNext;
        private GameObject _goFullLevel;

        protected override void Awake()
        {
            InitBtn();
            InitHead();
            InitExp();
            InitLeft();
            InitRight();
        }

        private void InitBtn()
        {
            _eventTriggerButtonList = new List<PetEventTriggerButton>();
            for (int i = 0; i < PetExpItemArray.Length; i++)
            {
                PetEventTriggerButton eventTriggerButton = AddChildComponent<PetEventTriggerButton>(string.Format("Container_jinyan{0}", i));
                eventTriggerButton.Init(PetExpItemArray[i]);
                _eventTriggerButtonList.Add(eventTriggerButton);
            }
        }

        private void InitHead()
        {
            _locaterHead = AddChildComponent<Locater>("Container_chongwuleft");
            InitHeadX = _locaterHead.X;
            _petModel = AddChildComponent<PetStrengthModel>("Container_chongwuleft");
            _petModel.ModelPosition = new Vector3(155f, -387f, -200f);
            _particleLevelUp2 = AddChildComponent<KParticle>("Container_chongwuleft/fx_ui_6_3_1_tisheng");
            _particleLevelUp2.transform.localPosition = new Vector3(93f, -27f, -300f);
            _particleLevelUp2.transform.localScale = Vector3.one * 3;
            _particleLevelUp2.Stop();
        }

        private void InitExp()
        {
            _textLevel = GetChildComponent<StateText>("Label_txtdengji");
            _textExp = GetChildComponent<StateText>("Label_txtjingyan");
            _progressBar = AddChildComponent<TweenProgressBar>("ProgressBar_jindu");
        }

        private void InitLeft()
        {
            _locaterNow = AddChildComponent<Locater>("Container_left");
            InitNowX = _locaterNow.X;
            _textNowLevel = GetChildComponent<StateText>("Container_left/Label_txtBenjibiaoti");
            _listNow = GetChildComponent<KList>("Container_left/List_content");
            _listNow.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _listNow.SetGap(19, 0);
            _particleLevelUp1 = AddChildComponent<KParticle>("Container_left/fx_ui_6_3_1_tisheng");
            _particleLevelUp1.transform.localPosition = new Vector3(180f, -12f, -20);
            _particleLevelUp1.Stop();
        }

        private void InitRight()
        {
            _goNext = GetChild("Container_right");
            _textNextLevel = GetChildComponent<StateText>("Container_right/Label_txtXiajibiaoti");
            _listNext = GetChildComponent<KList>("Container_right/List_content");
            _listNext.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _listNext.SetGap(19, 0);
            _goFullLevel = GetChild("Container_right/Label_txtShangxian");
        }

        private void InitIcon()
        {
            for (int i = 0; i < PetExpItemArray.Length; i++)
            {
                GetChildComponent<StateIcon>(string.Format("Container_jinyan{0}/Button_TimerButton/stateIcon", i)).SetIcon(item_helper.GetIcon(PetExpItemArray[i]));
            }
        }

        protected override void OnDestroy()
        {
            _petInfo = null;
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            EventDispatcher.AddEventListener(PetEvents.AddExp, OnRefresh);
            EventDispatcher.AddEventListener<PetInfo>(PetEvents.LevelUp, OnLevelUp);
        }

        private void RemoveListener()
        {
            EventDispatcher.RemoveEventListener(PetEvents.AddExp, OnRefresh);
            EventDispatcher.RemoveEventListener<PetInfo>(PetEvents.LevelUp, OnLevelUp);
        }

        private void OnLevelUp(PetInfo petInfo)
        {
            _particleLevelUp1.Play();
            _particleLevelUp2.Play();
            UIManager.Instance.PlayAudio("Sound/UI/pet_update.mp3");
        }

        private void OnShowTips()
        {
            PetPopPanelDataWrapper wrapper = new PetPopPanelDataWrapper(PetPopType.gotPetInfo, PetContext.strengthen, _petInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.PetPop, wrapper);
        }

        public void SetData(PetInfo petInfo, bool doTween)
        {
            Visible = true;
            _petInfo = petInfo;
            _progressBar.Clear();
            Refresh(petInfo, doTween);
        }

        private void OnRefresh()
        {
            Refresh(_petInfo, false);
        }

        private void Refresh(PetInfo petInfo, bool doTween)
        {
            RefreshLayout(petInfo);
            RefreshExp(petInfo);
            RefreshBtn(petInfo);
            RefreshAttribute(petInfo);
            RefreshPetModel();
            if (doTween)
            {
                DoTween();
            }
        }

        private void RefreshExp(PetInfo petInfo)
        {
            _textLevel.CurrentText.text = string.Format("{0}", petInfo.Level);
            int nowExp = petInfo.Exp;
            int nextExp = pet_level_helper.GetNextLevelExp(petInfo.Id, petInfo.Level);
            float rate = pet_level_helper.GetExpPercent(petInfo.Id, nowExp, petInfo.Level);
            _textExp.CurrentText.text = string.Format("{0}/{1} ({2}%)", petInfo.Exp, nextExp, (int)(rate * 100));
            _progressBar.SetProgress(petInfo.Exp, nextExp, nextExp == 0);
        }

        private void RefreshBtn(PetInfo petInfo)
        {
            for (int i = 0; i < _eventTriggerButtonList.Count; i++)
            {
                _eventTriggerButtonList[i].Refresh(petInfo);
            }
        }

        private void RefreshAttribute(PetInfo petInfo)
        {
            RefreshNow(petInfo);
            if(!pet_level_helper.IsMaxLevel(petInfo.Id, petInfo.Level))
            {
                RefreshNext(petInfo);
            }
        }

        private void RefreshNow(PetInfo petInfo)
        {
            _textNowLevel.CurrentText.text = MogoLanguageUtil.GetContent(43473, petInfo.Level);
            List<RoleAttributeData> nowList = _petData.GetAttributeDataList(petInfo, true);
            List<IAttribute> iNowList = pet_star_helper.ParseUpLowbound(nowList);
            _listNow.SetDataList<PetStrengthAttributeItem>(iNowList);
        }

        private void RefreshNext(PetInfo petInfo)
        {
            _textNextLevel.CurrentText.text = MogoLanguageUtil.GetContent(43474, petInfo.Level + 1);
            List<RoleAttributeData> nextList = _petData.GetAttributeDataList(petInfo.Id, petInfo.Level + 1, petInfo.Star, petInfo.Quality, petInfo.PetSkillIdList, petInfo.StarAttributeList, true, petInfo.State, petInfo.Position);
            List<IAttribute> iNextList = pet_star_helper.ParseUpLowbound(nextList);
            if (pet_quality_helper.CurrentLevelReachMax(petInfo.Id, petInfo.Quality, petInfo.Level))
            {
                _listNext.Visible = false;
                _goFullLevel.SetActive(true);
            }
            else
            {
                _listNext.Visible = true;
                _listNext.SetDataList<PetLevelUpAttributeItem>(iNextList);
                _goFullLevel.SetActive(false);
            }
        }

        private void RefreshPetModel()
        {
            _petModel.Refresh(_petInfo);
        }

        private void RefreshLayout(PetInfo petInfo)
        {
            if (pet_level_helper.IsMaxLevel(petInfo.Id, petInfo.Level))
            {
                _locaterHead.X = InitHeadX + 200;
                _locaterNow.X = InitNowX + 200;
                _goNext.SetActive(false);
            }
            else
            {
                _locaterHead.X = InitHeadX;
                _locaterNow.X = InitNowX;
                _goNext.SetActive(true);
            }
        }

        private void DoTween()
        {
            DoTween(_locaterHead.gameObject, MoveDirection.Left);
            DoTween(_locaterNow.gameObject, MoveDirection.Right);
            DoTween(_goNext, MoveDirection.Right);
        }

        private void DoTween(GameObject gameObject, MoveDirection moveDirection)
        {
            if (gameObject.activeInHierarchy)
            {
                TweenViewMove.Begin(gameObject, MoveType.Show, moveDirection, true);
            }
        }
    }
}
