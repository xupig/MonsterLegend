﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/4 14:07:00
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using UnityEngine;
using Common.ExtendComponent;

namespace ModulePet
{
    public class PetQualityUp : KContainer
    {
        private PetInfo _petInfo;
        private Vector3 _nowPosition;

        private PetStrengthModel _petModelNow;
        private StateText _textNowLevel;

        private PetStrengthModel _petModelNext;
        private PetQualityUpList _qualityUpList;

        private StateText _textNeedLevel;
        private KList _costList;
        private KButton _btnQualityUp;
        private KContainer _containerMaterial;
        private KContainer _containerArrow;
        private GameObject _goFullQuality;
        private KParticle _particleBtn;

        protected override void Awake()
        {
            InitNow();
            InitNext();
            InitCost();
            _btnQualityUp = GetChildComponent<KButton>("Container_material/Button_tupo");
            _containerArrow = GetChildComponent<KContainer>("Container_arrow");
            _goFullQuality = GetChild("Label_txtmanji");
            _particleBtn = AddChildComponent<KParticle>("Container_material/Button_tupo/fx_ui_3_2_lingqu");
            _particleBtn.transform.localScale = new Vector3(1.43f, 1.12f, 1f);
        }

        protected override void OnDestroy()
        {
            _petInfo = null;
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnQualityUp.onClick.AddListener(OnClick);
            EventDispatcher.AddEventListener(PetEvents.UpdatePetInfo, OnRefresh);
            EventDispatcher.AddEventListener<PetInfo>(PetEvents.QualityUp, OnQualityUp);
        }

        private void RemoveListener()
        {
            _btnQualityUp.onClick.RemoveListener(OnClick);
            EventDispatcher.RemoveEventListener(PetEvents.UpdatePetInfo, OnRefresh);
            EventDispatcher.RemoveEventListener<PetInfo>(PetEvents.QualityUp, OnQualityUp);
        }

        private void OnQualityUp(PetInfo petInfo)
        {
            PetPopPanelDataWrapper wrapper = new PetPopPanelDataWrapper(PetPopType.qualityUpSuccess, PetContext.strengthen, _petInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.PetPop, wrapper);
        }

        private void OnClick()
        {
            PetManager.Instance.RequestAddQuality(_petInfo.Id);
        }

        private void OnShowNowTips()
        {
            PetPopPanelDataWrapper wrapper = new PetPopPanelDataWrapper(PetPopType.gotPetInfo, PetContext.strengthen, _petInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.PetPop, wrapper);
        }

        private void OnShowNextTips()
        {
            PetInfo nextPetInfo = _petInfo.Copy();
            nextPetInfo.Quality += 1;
            PetPopPanelDataWrapper wrapper = new PetPopPanelDataWrapper(PetPopType.gotPetInfo, PetContext.strengthen, nextPetInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.PetPop, wrapper);
        }

        private void InitNow()
        {
            _petModelNow = AddChildComponent<PetStrengthModel>("Container_chongwuleft");
            _petModelNow.ModelPosition = new Vector3(123f, -234.6f, -200f);
            _petModelNow.ModelScale = 150f;
            _textNowLevel = GetChildComponent<StateText>("Container_chongwuleft/Label_txtqualityUpContent");
            _nowPosition = _petModelNow.transform.localPosition;
        }

        private void InitNext()
        {
            _petModelNext = AddChildComponent<PetStrengthModel>("Container_chongwuright");
            _petModelNext.ModelPosition = new Vector3(123f, -234.6f, -200f);
            _petModelNext.ModelScale = 150f;
            _qualityUpList = AddChildComponent<PetQualityUpList>("Container_chongwuright/ScrollView_tupojieguo/mask/content");
        }

        private void InitCost()
        {
            _containerMaterial = GetChildComponent<KContainer>("Container_material");
            _textNeedLevel = GetChildComponent<StateText>("Container_material/Label_txtXuyaodengjizhi");
            _costList = GetChildComponent<KList>("Container_material/List_content");
            _costList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _costList.SetGap(0, 10);
        }

        public void SetData(PetInfo petInfo, bool doTween)
        {
            Visible = true;
            _petInfo = petInfo;
            Refresh(petInfo, doTween);
        }

        private void OnRefresh()
        {
            Refresh(_petInfo, false);
        }

        private void Refresh(PetInfo petInfo, bool doTween)
        {
            RefreshFullQuality(petInfo);
            RefreshNow(petInfo);
            RefreshNext(petInfo);
            RefreshCost(petInfo);
            RefreshBtn(petInfo);
            if (doTween)
            {
                DoTween(petInfo);
            }
        }

        private void RefreshNow(PetInfo petInfo)
        {
            _textNowLevel.CurrentText.text = MogoLanguageUtil.GetContent(43448, pet_quality_helper.GetLevelLimit(petInfo.Id, petInfo.Quality));
            _petModelNow.Refresh(petInfo);
        }

        private void RefreshNext(PetInfo petInfo)
        {
            if (!pet_quality_helper.QualityReachMax(petInfo.Id, petInfo.Quality))
            {
                PetInfo nextPetInfo = petInfo.Copy();
                nextPetInfo.Quality += 1;
                _qualityUpList.SetDataList(GetQualityUpEffectList(nextPetInfo));
                _petModelNext.Refresh(nextPetInfo);
            }
        }

        private List<string> GetQualityUpEffectList(PetInfo petInfo)
        {
            List<string> effectList = new List<string>();
            effectList.Add(string.Format(LangEnum.PET_LEVEL_LIMIT.ToLanguage(), pet_quality_helper.GetLevelLimit(petInfo.Id, petInfo.Quality)));
            List<int> skillIdList = pet_quality_helper.GetPetSkillIdListByQuality(petInfo.Id, petInfo.Quality);
            for (int i = 0; i < skillIdList.Count; i++)
            {
                int skillId = skillIdList[i];
                string skillName = string.Format(LangEnum.PET_UNLOCK_SKILL.ToLanguage(), MogoLanguageUtil.GetContent(pet_skill_helper.GetName(skillId)));
                string skillDesc = pet_skill_helper.GetDescStr(skillId);
                string skillContent = string.Format("{0}——{1}", skillName, skillDesc);
                effectList.Add(skillContent);
            }
            return effectList;
        }

        private void RefreshCost(PetInfo petInfo)
        {
            if (!pet_quality_helper.QualityReachMax(petInfo.Id, petInfo.Quality))
            {
                pet_quality petQuality = pet_quality_helper.GetPetQualityConf(petInfo.Id, petInfo.Quality);
                if(petQuality.__level_limit > petInfo.Level)
                {
                    string levelLimitStr = MogoLanguageUtil.GetContent(1449, petInfo.Level);
                    string needLevelStr = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, petQuality.__level_limit.ToString());
                    _textNeedLevel.CurrentText.text = string.Format("{0}{1}", needLevelStr, levelLimitStr);
                }
                else
                {
                    _textNeedLevel.CurrentText.text = petQuality.__level_limit.ToString();
                }
                List<BaseItemData> baseItemDataList = pet_quality_helper.GetQualityUpCost(petInfo.Id, petInfo.Quality);
                _costList.SetDataList<PetMaterial>(baseItemDataList);
                UpdateCostLayout();
            }
        }

        private void RefreshBtn(PetInfo petInfo)
        {
            PetData petData = PlayerDataManager.Instance.PetData;
            if (petInfo.CanQualityUp())
            {
                _particleBtn.Play(true);
            }
            else
            {
                _particleBtn.Stop();
            }
        }

        private void UpdateCostLayout()
        {
            List<KList.KListItemBase> itemList = _costList.ItemList;
            float x = 0;
            for (int i = 0; i < itemList.Count; i++)
            {
                Vector3 position = itemList[i].transform.localPosition;
                RectTransform rect = itemList[i].GetComponent<RectTransform>();
                rect.localPosition = new Vector3(x, position.y, position.z);
                x += rect.sizeDelta.x + _costList.LeftToRightGap;
            }
        }

        private void RefreshFullQuality(PetInfo petInfo)
        {
            if (!pet_quality_helper.QualityReachMax(petInfo.Id, petInfo.Quality))
            {
                _petModelNow.transform.localPosition = _nowPosition;
                _petModelNext.Visible = true;
                _containerArrow.Visible = true;
                _containerMaterial.Visible = true;
                _btnQualityUp.Visible = true;
                _goFullQuality.SetActive(false);
            }
            else
            {
                _petModelNow.transform.localPosition = new Vector3(_nowPosition.x + 250, _nowPosition.y, _nowPosition.z);
                _petModelNext.Visible = false;
                _containerArrow.Visible = false;
                _containerMaterial.Visible = false;
                _btnQualityUp.Visible = false;
                _goFullQuality.SetActive(true);
            }
        }

        private void DoTween(PetInfo petInfo)
        {
            if (!pet_quality_helper.QualityReachMax(petInfo.Id, petInfo.Quality))
            {
                TweenViewMove.Begin(_petModelNow.gameObject, MoveType.Show, MoveDirection.Left, true);
                TweenViewMove.Begin(_petModelNext.gameObject, MoveType.Show, MoveDirection.Right, true);
            }
            else
            {
                TweenViewMove.Begin(_petModelNow.gameObject, MoveType.Show, MoveDirection.Right, true);
            }
        }
    }
}
