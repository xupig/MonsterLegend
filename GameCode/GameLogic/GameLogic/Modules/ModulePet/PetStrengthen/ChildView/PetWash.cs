﻿using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.CustomType;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModulePet
{
    public class PetWash : KContainer
    {
        public enum Tab
        {
            skill,
            quality,
            total,
        }

        private PetInfo _petInfo;
        private Tab _currentIndex;

        private CategoryList _categoryList;
        private KButton _btnWash;
        private KList _materialList;
        private PetStrengthModel _petModel;
        private PetWashTotalWash _totalWash;
        private PetWashQualityWash _qualityWash;
        private PetWashSkillWash _skillWash;
        private PetWashPop _popView;
        private GameObject _goLeft;
        private GameObject _goRight;

        protected override void Awake()
        {
            InitList();
            InitCategoryList();
            InitPetModel();
            _goLeft = GetChild("Container_molingxilian/Container_chongwuleft");
            _goRight = GetChild("Container_molingxilian/Container_chongwuright");
            _totalWash = AddChildComponent<PetWashTotalWash>("Container_molingxilian/Container_chongwuright/Container_zizhixilian");
            _qualityWash = AddChildComponent<PetWashQualityWash>("Container_molingxilian/Container_chongwuright/Container_zizhixilianxiangxi");
            _skillWash = AddChildComponent<PetWashSkillWash>("Container_molingxilian/Container_chongwuright/Container_molingjinengxilian");
            _btnWash = GetChildComponent<KButton>("Container_molingxilian/Button_xilian");
            _popView = AddChildComponent<PetWashPop>("Container_tanchuang");

            AddAwakeListener();
        }

        private void InitList()
        {
            _materialList = GetChildComponent<KPageableList>("Container_molingxilian/Container_cailiao/List_content");
            _materialList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _materialList.SetGap(0, 10);
        }

        private void InitPetModel()
        {
            _petModel = AddChildComponent<PetStrengthModel>("Container_molingxilian/Container_chongwuleft");
            _petModel.ModelPosition = new Vector3(155f, -387f, -200f);
        }

        private void InitCategoryList()
        {
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            _categoryList.RemoveAll();
            List<CategoryItemData> nameList = new List<CategoryItemData>();
            nameList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(43453)));
            nameList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(43452)));
            nameList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(43451)));
            _categoryList.SetDataList<PetWashCategoryListItem>(nameList);
        }

        protected override void OnDestroy()
        {
            RemoveAwakeListener();
        }

        private void AddAwakeListener()
        {
            _categoryList.onSelectedIndexChanged.AddListener(OnSelectIndexChanged);
            _btnWash.onClick.AddListener(OnRequestWash);
            _qualityWash.OnClickTips.AddListener(OnClickBlessTips);
            _totalWash.OnClickTips.AddListener(OnClickBlessTips);
        }

        private void RemoveAwakeListener()
        {
            _categoryList.onSelectedIndexChanged.RemoveListener(OnSelectIndexChanged);
            _btnWash.onClick.RemoveListener(OnRequestWash);
            _qualityWash.OnClickTips.RemoveListener(OnClickBlessTips);
            _totalWash.OnClickTips.RemoveListener(OnClickBlessTips);
        }

        protected override void OnEnable()
        {
            AddEnableListener();
        }

        protected override void OnDisable()
        {
            RemoveEnableListener();
        }

        private void AddEnableListener()
        {
            EventDispatcher.AddEventListener<PetSkillDataWrapper>(PetEvents.ShowWashPossibleSkill, OnShowPossibleSkill);
            EventDispatcher.AddEventListener<PetInfo, PbPetInfoRefresh>(PetEvents.ResponsePetWash, OnResponsePetWash);
            EventDispatcher.AddEventListener<PetInfo, PbPetInfoRefreshConfirm>(PetEvents.ResponsePetWashConfirm, OnResponsePetWashConfirm);
        }

        private void RemoveEnableListener()
        {
            EventDispatcher.RemoveEventListener<PetSkillDataWrapper>(PetEvents.ShowWashPossibleSkill, OnShowPossibleSkill);
            EventDispatcher.RemoveEventListener<PetInfo, PbPetInfoRefresh>(PetEvents.ResponsePetWash, OnResponsePetWash);
            EventDispatcher.RemoveEventListener<PetInfo, PbPetInfoRefreshConfirm>(PetEvents.ResponsePetWashConfirm, OnResponsePetWashConfirm);
        }

        private void OnShowPossibleSkill(PetSkillDataWrapper wrapper)
        {
            _popView.Show(PetWashPop.Type.possibleSkill, wrapper);
        }

        private void OnClickBlessTips()
        {
            _popView.Show(PetWashPop.Type.blessTip);
        }

        private void OnResponsePetWash(PetInfo petInfo ,PbPetInfoRefresh pbPetInfoRefresh)
        {
            Refresh(petInfo, false, false);
            switch ((PetWashType)pbPetInfoRefresh.refresh_type)
            {
                case PetWashType.total:
                    _popView.Show(PetWashPop.Type.totalWash, new PetWashDataWrapper(petInfo, pbPetInfoRefresh));
                    break;
                case PetWashType.skill:
                    _popView.Show(PetWashPop.Type.skillWash, new PetWashDataWrapper(petInfo, pbPetInfoRefresh));
                    break;
                case PetWashType.quality:
                    _popView.Show(PetWashPop.Type.qualityWash, new PetWashDataWrapper(petInfo, pbPetInfoRefresh));
                    break;
            }
        }

        private void OnResponsePetWashConfirm(PetInfo petInfo, PbPetInfoRefreshConfirm refreshConfirm)
        {
            _popView.Visible = false;
            Refresh(petInfo, false, refreshConfirm.is_confirm == 1);
        }

        private void OnSelectIndexChanged(CategoryList categoryList, int index)
        {
            _currentIndex = (Tab)index;
            Refresh(_petInfo, true, false);
        }

        private void OnRequestWash()
        {
            PetWashType type = TabToType(_currentIndex);
            LuaTable lockTable = new LuaTable();
            if (type == PetWashType.skill)
            {
                List<int> holeList = _skillWash.GetLockHoleList();
                for(int i = 0;i < holeList.Count;i++)
                {
                    lockTable.Add(i + 1, holeList[i]);
                }
            }
            else
            {
                lockTable = new LuaTable();
            }
            PetManager.Instance.RequestWash(_petInfo.Id, (int)TabToType(_currentIndex), lockTable);
        }

        public void SetData(PetInfo petInfo, bool doTween)
        {
            Visible = true;
            _popView.Visible = false;
            _petInfo = petInfo;
            _currentIndex = (Tab)0;
            _categoryList.SelectedIndex = (int)_currentIndex;
            Refresh(petInfo, doTween, false);
        }

        private void Refresh(PetInfo petInfo, bool doTween, bool playEffect)
        {
            _popView.PetInfo = petInfo;
            _petModel.Refresh(petInfo);
            RefreshCategoryList(petInfo);
            RefreshMaterialList(petInfo);
            HideAll();
            switch(_currentIndex)
            {
                case Tab.total:
                    _totalWash.Visible = true;
                    _totalWash.SetData(petInfo, playEffect);
                    break;
                case Tab.quality:
                    _qualityWash.Visible = true;
                    _qualityWash.SetData(petInfo, playEffect);
                    break;
                case Tab.skill:
                    _skillWash.Visible = true;
                    _skillWash.SetData(petInfo, playEffect);
                    break;
            }
            if (doTween)
            {
                DoTween();
            }
        }

        private void RefreshCategoryList(PetInfo petInfo)
        {
            for (int i = 0; i < _categoryList.ItemList.Count; i++)
            {
                PetWashCategoryListItem item = _categoryList.ItemList[i] as PetWashCategoryListItem;
                PetWashType type = TabToType((Tab)i);
                if (petInfo.IsWashTypeUnlock(type))
                {
                    item.SetLock(false);
                }
                else
                {
                    item.SetLock(true);
                }
                item.SetPoint(petInfo.CanWash(type));
            }
        }

        private void RefreshMaterialList(PetInfo petInfo)
        {
            PetWashType type = TabToType(_currentIndex);
            List<BaseItemData> baseItemDataList = pet_refresh_cost_helper.GetCostList(petInfo.Id, petInfo.WashLevel, (int)type);
            _materialList.SetDataList<PetMaterial>(baseItemDataList);
        }

        private void HideAll()
        {
            _skillWash.Visible = false;
            _qualityWash.Visible = false;
            _totalWash.Visible = false;
        }

        private PetWashType TabToType(Tab tab)
        {
            switch (tab)
            {
                case Tab.quality:
                    return PetWashType.quality;
                case Tab.skill:
                    return PetWashType.skill;
                default:
                    return PetWashType.total;
            }
        }

        private void DoTween()
        {
            DoTween(_goLeft.gameObject, MoveDirection.Right);
            DoTween(_goRight.gameObject, MoveDirection.Right);
        }

        private void DoTween(GameObject gameObject, MoveDirection moveDirection)
        {
            if (gameObject.activeInHierarchy)
            {
                TweenViewMove.Begin(gameObject, MoveType.Show, moveDirection, true);
            }
        }
    }
}
