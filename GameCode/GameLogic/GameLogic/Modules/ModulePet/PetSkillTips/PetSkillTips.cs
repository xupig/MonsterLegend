﻿using Common.Base;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModulePet
{
    public class PetSkillTips : BasePanel
    {
        private KDummyButton _btnClose;
        private PetSkillIcon _icon;
        private StateText _textName;
        private StateText _textDesc;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_Bg/Button_close");
            _btnClose = AddChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
            _icon = AddChildComponent<PetSkillIcon>("Container_chongwujineng");
            _icon.CanShowTips = false;
            _textName = GetChildComponent<StateText>("Container_mingzi/Label_txtName");
            _textDesc = GetChildComponent<StateText>("Container_miaoshu/Label_txtDesc");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(ClosePanel);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(ClosePanel);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.PetSkillTips; }
        }

        public override void OnShow(object data)
        {
            int petSkillId = (int)data;
            RefreshName(petSkillId);
            RefreshDesc(petSkillId);
            RefreshIcon(petSkillId);
        }

        private void RefreshName(int petSkillId)
        {
            _textName.CurrentText.text = MogoLanguageUtil.GetContent(pet_skill_helper.GetName(petSkillId));
        }

        private void RefreshDesc(int petSkillId)
        {
            _textDesc.CurrentText.text = pet_skill_helper.GetDescStr(petSkillId);
        }

        private void RefreshIcon(int petSkillId)
        {
            _icon.RefreshIcon(petSkillId);
        }

        public override void OnClose()
        {
        }
    }
}
