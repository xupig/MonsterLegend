﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModulePet
{
    public class PetAttributePanel : BasePanel
    {
        private ArtNumberList _artNumberList;
        private KList _listAttribute;

        private PetData _petData
        {
            get
            {
                return PlayerDataManager.Instance.PetData;
            }
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.PetAttribute; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_content/Button_close");
            _artNumberList = AddChildComponent<ArtNumberList>("Container_content/List_zhandouli");
            _listAttribute = GetChildComponent<KList>("Container_content/List_content");
            _listAttribute.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _listAttribute.SetGap(5, 0);
        }

        public override void OnShow(object data)
        {
            List<RoleAttributeData> attributeDataList = GetAttributeDataList();
            List<IAttribute> ilist = pet_star_helper.ParseUpLowbound(attributeDataList);
            _listAttribute.SetDataList<AttributeItem>(ilist);
            //宠物用战士职业来计算战斗力
            _artNumberList.SetNumber((int)fight_force_helper.GetFightForce(attributeDataList, Vocation.Warrior));
        }

        public override void OnClose()
        {
        }

        public List<RoleAttributeData> GetAttributeDataList()
        {
            List<PetInfo> petInfoList = _petData.FightingPetList;
            List<List<RoleAttributeData>> totalAttributeList = new List<List<RoleAttributeData>>();
            for (int i = 0; i < petInfoList.Count; i++)
            {
                PetInfo petInfo = petInfoList[i];
                List<RoleAttributeData> attributeDataList = _petData.GetAttributeDataList(petInfoList[i], false);
                ConvertFightRate(petInfo, attributeDataList);
                totalAttributeList.Add(attributeDataList);
            }
            return PetUtils.SumAttriList(totalAttributeList);
        }

        private void ConvertFightRate(PetInfo petInfo, List<RoleAttributeData> attributeDataList)
        {
            Dictionary<int, float> dict = pet_helper.GetFightPetConvertRate(petInfo.Id);
            for (int i = 0; i < attributeDataList.Count; i++)
            {
                RoleAttributeData attributeData = attributeDataList[i];
                attributeData.AttriValue = Mathf.CeilToInt(attributeData.AttriValue * dict[attributeData.attriId]);
                
            }
        }
    }
}
