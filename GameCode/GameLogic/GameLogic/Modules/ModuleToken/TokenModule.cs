﻿#region 模块信息
/*==========================================
// 文件名：TokenModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleToken
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/16 20:29:50
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleToken
{
    public class TokenModule:BaseModule
    {
        public override void Init()
        {
            RegisterPopPanelBunchPanel(PanelIdEnum.QuickBuy, PanelIdEnum.Token);
            AddPanel(PanelIdEnum.Token, "Container_TokenPanel", MogoUILayer.LayerUIPanel, "ModuleToken.TokenPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.QuickBuy, "Container_QuickBuyPanel", MogoUILayer.LayerUIPopPanel, "ModuleQuickBuy.QuickBuyPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }
    }
}
