﻿#region 模块信息
/*==========================================
// 文件名：TokenPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleToken
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/16 20:06:57
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using ModuleCommonUI.Token;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleToken
{
    public class TokenPanel : BasePanel
    {

        //private KToggleGroup _toggleGroup;
        private TokenView _tokenView;
        private StateText _txtName;
        //private KContainer _bgContainer; 
        protected override void Awake()
        {
            base.Awake();
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _txtName = GetChildComponent<StateText>("ToggleGroup_biaoti/Toggle_shilianshangcheng/checkmark/label");
            _tokenView = AddChildComponent<TokenView>("Container_shilianshangcheng");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Token; }
        }

        public override void OnShow(object data)
        {
            TokenType type = TokenType.ERROR;
            int index = 0;
            if (data is int[])
            {
                int[] dataList = data as int[];
                type = (TokenType)dataList[0];
                index = dataList[1] - 1;
            }
            else if(data is TokenType)
            {
                type = (TokenType)data;
            }
            else
            {
                type = TokenType.ERROR;
            }
            List<token_shop> shopList = token_shop_helper.GetTokenShopData(type);
            if (shopList.Count > 0)
            {
                _txtName.ChangeAllStateText(MogoLanguageUtil.GetContent(shopList[0].__title));
            }
            _tokenView.SetTokenType(type, index);
        }

        public override void OnClose()
        {
            // UIManager.Instance.SetPanelActive(_context, true);
        }

    }
}
