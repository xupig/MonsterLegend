﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;
using MogoEngine.Utils;
using UnityEngine.EventSystems;
using UnityEngine;
using Common.Data;
using UnityEngine.Events;
using GameMain.GlobalManager.SubSystem;
using Common.Utils;
using GameData;
using Common.ExtendComponent;
using ModuleCommonUI;
using Common.Base;

namespace ModuleWing
{
    public class WingListItem : KList.KListItemBase
    {
        private WingItemData _data;
        private StateText _nameText;
        private StateText _descriptionText;
        private ItemGrid _itemGrid;
        private WingListItemOwnedView _ownedView;
        private WingListItemNotOwnView _notOwnView;
        private KContainer _selectContainer;

        protected override void Awake( )
        {
            _nameText = GetChildComponent<StateText>("Label_txtChibangmingcheng");
            _descriptionText = GetChildComponent<StateText>("Label_txtMiaoshu");
            _ownedView = AddChildComponent<WingListItemOwnedView>("Container_owned");
            _notOwnView = AddChildComponent<WingListItemNotOwnView>("Container_notOwned");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _selectContainer = GetChildComponent<KContainer>("Container_xuanzhong");
            _selectContainer.Visible = false;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as WingItemData;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            RefreshItemView();
        }

        public override void Dispose()
        {
            _data = null;
        }

        public override void Show()
        {
            AddEventListener();
            RefreshItemView();
        }

        public override void Hide()
        {
            RemoveEventListener();
        }

        protected void AddEventListener()
        {
            EventDispatcher.AddEventListener(WingEvents.UPDATE_ITEM_VIEW, UpdateItemView);
        }

        protected void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(WingEvents.UPDATE_ITEM_VIEW, UpdateItemView);
        }

        private void UpdateItemView()
        {
            SetDataDirty();
        }

        private void RefreshItemView()
        {
            if (gameObject.activeInHierarchy == false) return;
            if (_data == null) return;
            _ownedView.Index = this.Index;
            _nameText.CurrentText.text = _data.name;
            _itemGrid.SetItemData(_data.icon, 1);
            _itemGrid.onClick = OnItemGridClick;
            if (_data.isOwned == true)
            {
                RefreshOwnedDescription();
                _ownedView.Visible = true;
                _notOwnView.Visible = false;
                _ownedView.SetWingItemData(_data);
            }
            else
            {
                RefreshNotOwnDescription();
                _ownedView.Visible = false;
                _notOwnView.Visible = true;
                _notOwnView.SetWingItemData(_data);
            }
        }

        private void OnItemGridClick()
        {
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.WingToolTips, _data.wingId, PanelIdEnum.Wing);
        }

        public void RefreshSelectImage(int wingId)
        {
            if (_selectContainer != null)
            {
                _selectContainer.Visible = (wingId == _data.wingId);
            }
        }

        private void RefreshNotOwnDescription()
        {
            _descriptionText.gameObject.SetActive(true);
            string description = "";
            if (_data.canBuy == 1)
            {
                description = _data.description;
            }
            else
            {
                description = _data.getMethod;
            }
            _descriptionText.CurrentText.text = description;
        }

        private void RefreshOwnedDescription()
        {
            if (_data.canTrain == true)
            {
                _descriptionText.gameObject.SetActive(false);
            }
            else
            {
                _descriptionText.gameObject.SetActive(true);
                _descriptionText.CurrentText.text = _data.description;
            }
        }

    }
}
