﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;
using MogoEngine.Utils;
using UnityEngine.EventSystems;
using UnityEngine;
using Common.Data;
using UnityEngine.Events;
using GameMain.GlobalManager.SubSystem;
using Common.Utils;
using GameData;
using Common.ExtendComponent;
using Common.Base;

namespace ModuleWing
{
    public class WingListItemNotOwnView : KContainer
    {
        private WingItemData _data;
        private StateText _notOwnText;
        private StateText _costText;
        private KButton _buyButton;
        private KButton _vipButton;
        private KButton _composeButton;
        private StateImage _blueSpot;
        private KContainer _buyContainer;
        private IconContainer _diamondIcon;
        private KContainer _grayContainer;

        protected override void Awake( )
        {
            _notOwnText = GetChildComponent<StateText>("Label_txtWeiyongyou");
            _costText = GetChildComponent<StateText>("Container_buy/Label_txtFeiyong");
            _buyButton = GetChildComponent<KButton>("Container_buy/Button_goumai");
            _vipButton = GetChildComponent<KButton>("Button_chongzhi");
            _composeButton = GetChildComponent<KButton>("Button_hecheng");
            _buyContainer = GetChildComponent<KContainer>("Container_buy");
            _blueSpot = GetChildComponent<StateImage>("Button_hecheng/xiaodian");
            _diamondIcon = AddChildComponent<IconContainer>("Container_buy/Container_icon");
            _grayContainer = GetChildComponent<KContainer>("Container_huizhao");
        }

        protected void AddEventListener()
        {
            _buyButton.onClick.AddListener(OnBuyClick);
            _vipButton.onClick.AddListener(OnVipButtonClick);
            _composeButton.onClick.AddListener(OnComposeButtonClick);
        }

        protected void RemoveEventListener()
        {
            _buyButton.onClick.RemoveListener(OnBuyClick);
            _vipButton.onClick.RemoveListener(OnVipButtonClick);
            _composeButton.onClick.RemoveListener(OnComposeButtonClick);
        }

        private void OnComposeButtonClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.WingPop, new WingToolTipsDataWrapper(4, _data));
        }

        private void OnVipButtonClick()
        {
            view_helper.OpenView(16);
        }

        private void OnBuyClick()
        {
            view_helper.OpenView(64);
        }

        public void SetWingItemData(WingItemData data)
        {
            _data = data;
            RefreshView();
        }

        private void RefreshView()
        {
            _grayContainer.gameObject.SetActive(false);
            RefreshBuyView();
        }

        private void RefreshBuyView()
        {
            if (_data.canBuy == 1)
            {
                _buyContainer.gameObject.SetActive(true);
                _diamondIcon.SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_DIAMOND));
                int cost = mall_helper.GetSellPrice(mall_helper.GetConfig(_data.mallId));
                _costText.CurrentText.text = "x" + cost;
                _composeButton.Visible = false;
                _vipButton.Visible = false;
            }
            else if (_data.canBuy == 2)
            {
                _buyContainer.gameObject.SetActive(false);
                _composeButton.Visible = false;
                _vipButton.Visible = true;
            }
            else if (_data.canBuy == 3)
            {
                _buyContainer.gameObject.SetActive(false);
                _vipButton.Visible = false;
                _composeButton.Visible = true;
                _blueSpot.Visible = PlayerDataManager.Instance.WingData.canComposeList.Contains(_data.wingId);
            }
            else
            {
                _buyContainer.gameObject.SetActive(false);
                _vipButton.Visible = false;
                _composeButton.Visible = false;
            }
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }
    }
}
