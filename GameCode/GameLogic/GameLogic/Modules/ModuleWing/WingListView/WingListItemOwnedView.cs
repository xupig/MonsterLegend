﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;
using MogoEngine.Utils;
using UnityEngine.EventSystems;
using UnityEngine;
using Common.Data;
using UnityEngine.Events;
using GameMain.GlobalManager.SubSystem;
using Common.Utils;
using GameData;
using Common.ExtendComponent;
using Common.Base;
using GameMain.Entities;
using ModuleCommonUI;

namespace ModuleWing
{
    public class WingListItemOwnedView : KContainer
    {
        private WingItemData _data;
        private StateText _levelText;
        private StateImage _equipedIconImage;
        private KButton _checkButton;
        private StateImage _pointImage;
        private KContainer _trainContainer;
        private WingStarLevelBar _wingStarLevelBar;
        private StateText _expText;
        private KProgressBar _expProgressBar;

        public int Index { get; set; }

        protected override void Awake( )
        {
            _levelText = GetChildComponent<StateText>("Label_txtDengji");
            _equipedIconImage = GetChildComponent<StateImage>("Image_sharedyizhuangbei");
            _checkButton = GetChildComponent<KButton>("Button_chakan");
            _wingStarLevelBar = AddChildComponent<WingStarLevelBar>("Container_peiyang/Container_xingxing");
            _trainContainer = GetChildComponent<KContainer>("Container_peiyang");
            _expProgressBar = GetChildComponent<KProgressBar>("Container_peiyang/ProgressBar_exp");
            _expText = GetChildComponent<StateText>("Container_peiyang/Container_txt/Label_shuju");
            _pointImage = GetChildComponent<StateImage>("Image_xiaodian");
            _checkButton.Label.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
            _checkButton.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(6172018));
        }

        private void RefreshWingPoint()
        {
            _pointImage.Visible = PlayerDataManager.Instance.WingData.CanTrain(_data.wingId);
        }

        protected void AddEventListener()
        {
            _checkButton.onClick.AddListener(OnTrainClick);
            EventDispatcher.AddEventListener(WingEvents.REFRESH_NOTICE_POINT, RefreshWingPoint);
        }

        protected void RemoveEventListener()
        {
            _checkButton.onClick.RemoveListener(OnTrainClick);
            EventDispatcher.RemoveEventListener(WingEvents.REFRESH_NOTICE_POINT, RefreshWingPoint);
        }

        private void OnTrainClick()
        {
            EventDispatcher.TriggerEvent<int>(WingEvents.SHOW_DETAIL_VIEW, Index);
        }

        public void SetWingItemData(WingItemData data)
        {
            _data = data;
            RefreshView();
        }

        private void RefreshView()
        {
            _levelText.CurrentText.text = "LV" + _data.level;
            RefreshTrainView();
            RefreshPutOnView();
            RefreshWingPoint();
        }

        private void RefreshTrainView()
        {
            if (_data.canTrain == true)
            {
                _trainContainer.gameObject.SetActive(true);
                _wingStarLevelBar.RefreshStarLevelBar(_data.level, _data.maxLevel, 1.0f);
                _expText.CurrentText.text = _data.currentExp.ToString() + "/" + _data.totalExp.ToString();
                _expProgressBar.Value = (float)_data.currentExp / _data.totalExp;
            }
            else
            {
                _trainContainer.gameObject.SetActive(false);
            }
        }

        private void RefreshPutOnView()
        {
            if (_data.isPutOn == true)
            {
                _equipedIconImage.gameObject.SetActive(true);
            }
            else
            {
                _equipedIconImage.gameObject.SetActive(false);
            }
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

    }
}
