﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using MogoEngine.Events;
using Common.Events;
using Common.Data;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using UnityEngine.Events;
using Common.ExtendComponent;

namespace ModuleWing
{
    public class WingListScrollView : KContainer
    {
        private KScrollView _scrollView;
        private KList _list;
        private List<WingItemData> _dataList;
        public KComponentEvent<WingListScrollView, int> onWingItemSelected = new KComponentEvent<WingListScrollView, int>();

        protected override void Awake()
        {
            KContainer container = this.gameObject.GetComponent<KContainer>();
            _scrollView = container.GetComponent<KScrollView>();
            _list = container.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.LeftToRight, 1);
            _list.SetGap(-23, 0);
        }

        public void Show()
        {
            this.Visible = true;
            TweenListEntry.Begin(_list.gameObject);
        }

        public void SetWingDataList(List<WingItemData> dataList)
        {
            _dataList = dataList;
            _list.SetDataList<WingListItem>(_dataList, 2);
            RefreshSelectedItem();
            _scrollView.ResetContentPosition();
            _scrollView.UpdateArrow();
        }

        protected void AddEventListener()
        {
            _list.onItemClicked.AddListener(OnScrollViewSelect);
            EventDispatcher.AddEventListener(WingEvents.REFRESH_SELECT_ID, RefreshSelectedItem);
        }

        protected void RemoveEventListener()
        {
            _list.onItemClicked.RemoveListener(OnScrollViewSelect);
            EventDispatcher.RemoveEventListener(WingEvents.REFRESH_SELECT_ID, RefreshSelectedItem);
        }

        private void RefreshSelectedItem()
        {
            if (gameObject.activeInHierarchy == false) return;
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                WingListItem item = _list.ItemList[i] as WingListItem;
                item.RefreshSelectImage(WingModelView.currentWingId);
                
            }
        }

        private void OnScrollViewSelect(KList group, KList.KListItemBase item)
        {
            onWingItemSelected.Invoke(this, item.Index);
        }

        protected override void OnEnable()
        {
            AddEventListener();
            RefreshSelectedItem();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }
    }
}
