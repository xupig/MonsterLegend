﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;
using MogoEngine.Utils;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.Events;
using Common.Data;
using GameMain.GlobalManager.SubSystem;
using Common.Utils;
using GameData;
using GameMain.Entities;
using Common.ExtendComponent;
using ModuleCommonUI;
using Common.Base;
using Common.Chat;

namespace ModuleWing
{
    public class WingDetailItem : KList.KListItemBase
    {
        private WingItemData _data;

        private KButton _closeButton;
        private StateText _nameText;
        private StateText _levelText;
        private StateText _notOwnText;
        private KContainer _suitContainer;
        private KButton _composeButton;
        private KButton _showButton;
        private KButton _putOnButton;
        private KButton _putDownButton;
        private KButton _trainButton;
        private KContainer _buttonContainer;
        private KContainer _notOwnedContainer;
        private KList _wingAttributeBarList;
        private StateText _suitTitleTxt;
        private KContainer _attributeContainer;
        private KContainer _titleContainer;
        private KList _wingSuitAttributeBarList;
        private ArtNumberList _fightForceContainer;

        private static readonly float s_attributeListGap = 32f;
        private static readonly float s_attributeListBottom = 18f;

        protected override void Awake( )
        {
            _closeButton = GetChildComponent<KButton>("Container_biaoti/Button_fanhui");
            _nameText = GetChildComponent<StateText>("Container_biaoti/Label_txtJiaosemingcheng");
            _levelText = GetChildComponent<StateText>("Container_biaoti/Label_txtDengji");
            _notOwnText = GetChildComponent<StateText>("Container_biaoti/Label_txtWYY");
            _composeButton = GetChildComponent<KButton>("Container_anniu/Button_hecheng");
            _showButton = GetChildComponent<KButton>("Container_anniu/Button_zhanshi");
            _putOnButton = GetChildComponent<KButton>("Container_anniu/Button_chuandai");
            _putDownButton = GetChildComponent<KButton>("Container_anniu/Button_xiexia");
            _trainButton = GetChildComponent<KButton>("Container_anniu/Button_peiyang");
            _suitContainer = GetChildComponent<KContainer>("Container_neirong/Container_jinglingtaozhuang");
            _notOwnedContainer = GetChildComponent<KContainer>("Container_neirong/Container_WYY");
            _wingAttributeBarList = GetChildComponent<KList>("Container_neirong/List_shuxing");
            _attributeContainer = GetChildComponent<KContainer>("Container_neirong");
            _buttonContainer = GetChildComponent<KContainer>("Container_anniu");
            _titleContainer = GetChildComponent<KContainer>("Container_biaoti");
            _wingSuitAttributeBarList = GetChildComponent<KList>("Container_neirong/Container_jinglingtaozhuang/List_taozhuangshuxing");
            _suitTitleTxt = GetChildComponent<StateText>("Container_neirong/Container_jinglingtaozhuang/Container_biaoti/Label_txtJinglingtaozhuang");
            _fightForceContainer = AddChildComponent<ArtNumberList>("Container_neirong/Container_ZDL/Container_shengming/List_zhandouli");
            InitKList();
        }

        private void InitKList()
        {
            _wingAttributeBarList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _wingAttributeBarList.SetGap(5, 0);
            _wingSuitAttributeBarList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _wingSuitAttributeBarList.SetPadding(0, -7, 0, -7);
            _wingSuitAttributeBarList.SetGap(8, 0);
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as WingItemData;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            RefreshItemView();
        }

        public override void Dispose()
        {
            _data = null;
        }

        public override void Hide()
        {
            RemoveEventListener();
        }


        public override void Show()
        {
            AddEventListener();
            RefreshItemView();
        }


        private void RefreshItemView()
        {
            if (_data == null) return;
            _nameText.CurrentText.text = _data.name;
            _levelText.CurrentText.text = "LV" + _data.level;
            _suitTitleTxt.CurrentText.text = MogoLanguageUtil.GetContent(35784 - 1 + (int)_data.type);
            Vector3 position = MogoGameObjectHelper.GetRectPosition(_notOwnedContainer.gameObject);
            position = RefreshOwnState(position);
            position = RefreshAttributeView(position);
            RefreshSuitAttributeList();
            RefreshFightForce();
        }

        private Vector3 RefreshOwnState(Vector3 position)
        {
            if (_data.isOwned == true)
            {
                _notOwnedContainer.gameObject.SetActive(false);
                MogoGameObjectHelper.SetRectPosition(_wingAttributeBarList.gameObject, position);
                _showButton.gameObject.SetActive(true);
                _levelText.gameObject.SetActive(true);
                _notOwnText.gameObject.SetActive(false);
                _composeButton.Visible = false;
                _trainButton.Visible = _data.maxLevel > 1;
                RefreshEquipState();
            }
            else
            {
                _notOwnedContainer.gameObject.SetActive(false);
                _levelText.gameObject.SetActive(false);
                _notOwnText.gameObject.SetActive(true);
                //position.y -= s_attributeListGap;
                MogoGameObjectHelper.SetRectPosition(_wingAttributeBarList.gameObject, position);
                _showButton.gameObject.SetActive(false);
                _putOnButton.gameObject.SetActive(false);
                _putDownButton.gameObject.SetActive(false);
                if (_data.canBuy == 3)
                {
                    _composeButton.Visible = true;
                }
                else
                {
                    _composeButton.Visible = false;
                }
                _trainButton.Visible = false;
            }
            RefreshButtonLayout();
            return position;
        }

        private void RefreshButtonLayout()
        {
            List<object> objectList = new List<object>();
            if (_showButton.Visible == true)
            {
                objectList.Add(_showButton);
            }
            if (_putOnButton.Visible == true)
            {
                objectList.Add(_putOnButton);
            }
            if (_putDownButton.Visible == true)
            {
                objectList.Add(_putDownButton);
            }
            if (_composeButton.Visible == true)
            {
                objectList.Add(_composeButton);
            }
            if (_trainButton.Visible == true)
            {
                objectList.Add(_trainButton);
            }

            MogoLayoutUtils.ComponentHorizontalLayout(20, objectList.ToArray());
            _buttonContainer.RecalculateSize();
            MogoLayoutUtils.SetCenter(_buttonContainer.gameObject, 2);
        }

        private Vector3 RefreshAttributeView(Vector3 position)
        {
            _wingAttributeBarList.RemoveAll();
            _wingAttributeBarList.SetDataList<WingAttributeBar>(_data.ArrtibuteList);

            position = MogoGameObjectHelper.GetRectPosition(_wingAttributeBarList.gameObject);
            position.y -= s_attributeListGap * _data.ArrtibuteList.Count;
            position.y -= s_attributeListBottom;
            MogoGameObjectHelper.SetRectPosition(_suitContainer.gameObject, position);
            return position;
        }

        private void RefreshEquipState()
        {
            if (_data.isPutOn == true)
            {
                _putOnButton.gameObject.SetActive(false);
                _putDownButton.gameObject.SetActive(true);
            }
            else
            {
                _putOnButton.gameObject.SetActive(true);
                _putDownButton.gameObject.SetActive(false);
            }
        }

        private void RefreshSuitAttributeList()
        {
            _wingSuitAttributeBarList.RemoveAll();
            List<WingSuitAttributeData> dataList = PlayerDataManager.Instance.WingData.GetWingSuitDataList(_data.type);
            _wingSuitAttributeBarList.SetDataList<WingSuitAttributeBar>(dataList);
        }

        private void RefreshFightForce()
        {
            _fightForceContainer.SetNumber(_data.fightForce);
        }

        protected void AddEventListener()
        {
            _composeButton.onClick.AddListener(OnComposeButtonClick);
            _closeButton.onClick.AddListener(OnCloseClick);
            _putOnButton.onClick.AddListener(OnPutOnClick);
            _showButton.onClick.AddListener(OnShowWingClick);
            _putDownButton.onClick.AddListener(OnPutDownClick);
            _trainButton.onClick.AddListener(OnTrainButtonClick);
            EventDispatcher.AddEventListener(WingEvents.UPDATE_ITEM_VIEW, RefreshItemView);
        }

        protected void RemoveEventListener()
        {
            _composeButton.onClick.RemoveListener(OnComposeButtonClick);
            _closeButton.onClick.RemoveListener(OnCloseClick);
            _putOnButton.onClick.RemoveListener(OnPutOnClick);
            _showButton.onClick.RemoveListener(OnShowWingClick);
            _putDownButton.onClick.RemoveListener(OnPutDownClick);
            _trainButton.onClick.RemoveListener(OnTrainButtonClick);
            EventDispatcher.RemoveEventListener(WingEvents.UPDATE_ITEM_VIEW, RefreshItemView);
        }

        private void OnTrainButtonClick()
        {
            if (_data.level >= _data.maxLevel)
            {
                SystemInfoManager.Instance.Show(Common.ServerConfig.error_code.ERR_WING_TRAIN_LEVEL_LIMIT);
                return;
            }
            UIManager.Instance.ShowPanel(PanelIdEnum.WingPop, new WingToolTipsDataWrapper(1, _data));
            EventDispatcher.TriggerEvent<int>(WingEvents.CHANGE_WING_MODEL, _data.wingId);
        }

        private void OnCloseClick()
        {
            EventDispatcher.TriggerEvent(WingEvents.CLOSE_DETAIL_ITEM);
        }

        private void OnPutOnClick()
        {
            WingManager.Instance.PutOnWing(_data.wingId);
        }

        private void OnPutDownClick()
        {
            WingManager.Instance.PutOnWing(0);
        }

        private void OnShowWingClick()
        {
            wing wingInfo = wing_helper.GetWing(_data.wingId);
            string name = MogoLanguageUtil.GetContent(wingInfo.__name);
            UIManager.Instance.ShowPanel(PanelIdEnum.Chat, new ChatItemLinkWrapper(name, _data.wingId));
            //ToolTipsManager.Instance.ShowTip(PanelIdEnum.WingToolTips, _data.wingId, PanelIdEnum.Wing);
        }

        private void OnComposeButtonClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.WingPop, new WingToolTipsDataWrapper(4, _data));
        }
    }
}
