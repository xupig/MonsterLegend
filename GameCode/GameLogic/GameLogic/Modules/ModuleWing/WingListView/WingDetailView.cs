﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager;
using Common.Data;

namespace ModuleWing
{
    public class WingDetailView : KContainer
    {
        private KScrollPage _scrollPage;
        private KPageableList _list;
        private List<WingItemData> _dataList;

        protected override void Awake()
        {
            base.Awake();
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_detail");
            _list = _scrollPage.GetChildComponent<KPageableList>("mask/content");
            InitScrollPage();
        }

        public void SetWingDataList(List<WingItemData> dataList)
        {
            _dataList = dataList;
            _list.SetDataList<WingDetailItem>(_dataList);
            _scrollPage.TotalPage = _dataList.Count;
            _scrollPage.CurrentPage = 0;
        }

        public void ShowPageByIndex(int index)
        {
            _scrollPage.CurrentPage = index;
            ChangeWingModeByInde(index);
        }

        private void ChangeWingModeByInde(int index)
        {
            WingItemData itemData = _dataList[index];
            EventDispatcher.TriggerEvent<int>(WingEvents.CHANGE_WING_MODEL, itemData.wingId);
        }

        private void InitScrollPage()
        {
            _list.SetDirection(KList.KListDirection.LeftToRight, 1, 1);
            _list.SetPadding(0, 0, 0, 0);
            _list.SetGap(0, 0);
            _scrollPage.onCurrentPageChanged.AddListener(OnPageChanged);
        }

        private void OnPageChanged(KScrollPage scrollPage, int page)
        {
            ChangeWingModeByInde(page);
        }

    }
}
