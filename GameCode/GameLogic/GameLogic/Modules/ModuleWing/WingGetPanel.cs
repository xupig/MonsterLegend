﻿using ACTSystem;
using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleWing
{
    public class WingGetPanel:BasePanel
    {

        private ModelComponent _model;
        private StateText _txtWingName;
        private StateText _txtConvertTips;
        private StateText _txtWingGetTitle;

        private KParticle _particle;

        private int _wingId;
        private bool _isShowConvertTips;

        protected override void Awake()
        {
 	        base.Awake();
            _model = AddChildComponent<ModelComponent>("Container_chibanghuode/Container_pet/Container_model");

            _txtWingName = GetChildComponent<StateText>("Container_chibanghuode/Label_txtPetName");
            _txtConvertTips = GetChildComponent<StateText>("Container_chibanghuode/Label_txtBreakInfoPieces");
            _txtWingGetTitle = GetChildComponent<StateText>("Container_chibanghuode/Label_txtBiaoti");

            _particle = AddChildComponent<KParticle>("Container_chibanghuode/Container_pet/fx_ui_21_5_zhaohuan_01");
            _particle.transform.localPosition = new Vector3(30f, -80f, 0f);

            CloseBtn = GetChildComponent<KButton>("Container_chibanghuode/Button_zhezhao");
        }

        public override void OnShow(object data)
        {
            int[] list = data as int[];
            int wingId = list[0];
            _isShowConvertTips = list[1] == 1;
            RefreshContent(wingId);
            _particle.Play();
        }

        public override void OnClose()
        {
            ;
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.WingGet; }
        }

        private void RefreshContent(int wingId)
        {
            _wingId = wingId;
            _txtWingName.CurrentText.text = wing_helper.GetWingName(wingId);
            _txtWingGetTitle.CurrentText.text = MogoLanguageUtil.GetContent(35761, wing_helper.GetWingName(wingId));

            KeyValuePair<int, int> exchange = wing_helper.GetWingConvert(wingId);
            _txtConvertTips.Visible = _isShowConvertTips;
            _txtConvertTips.CurrentText.text = MogoLanguageUtil.GetContent(35762, exchange.Value, item_helper.GetName(exchange.Key));
            LoadWingModel();
        }

        public void LoadWingModel()
        {
            WingItemData wingItemData = PlayerDataManager.Instance.WingData.GetItemDataById(_wingId);
            var equipData = ACTRunTimeData.GetEquipmentData(wingItemData.ModelId);
            if (equipData.EquipType != ACTEquipmentType.Wing) { ACTSystemDebug.LogError("equipID is not a Wing:" + equipData.ID); }
            if (string.IsNullOrEmpty(equipData.PrefabPath))
            {
                ACTSystemDebug.LogError("ACTDataEquipment Data error:" + equipData.ID);
                return;
            }
            var assetLoader = ACTSystemDriver.GetInstance().ObjectPool.assetLoader;
            string prefabPath = equipData.PrefabPath;
            assetLoader.GetGameObject(prefabPath, delegate(UnityEngine.Object obj)
            {
                var wingGameObject = obj as GameObject;
                wingGameObject.transform.SetParent(_model.transform, false);
                ACTSystemTools.SetLayer(wingGameObject.transform, gameObject.layer, true);
                int quality = (int)ACTSystemDriver.GetInstance().CurVisualQuality;
                if (wingGameObject == null) return;
                ACTWingQuality.UpdateWingQuality(wingGameObject);
                wingGameObject.AddComponent<SortOrderedRenderAgent>();
                ComputeWingPosition(wingGameObject);
            });
        }

        private void ComputeWingPosition(GameObject go)
        {
            RectTransform rectContainer = _model.GetComponent<RectTransform>();
            //RectTransform rectModel = go.GetComponent<RectTransform>();

            go.transform.localPosition = new Vector3(rectContainer.sizeDelta.x / 2, -rectContainer.sizeDelta.y/2, -300);
            go.transform.localRotation = Quaternion.Euler(90, 270, 0);
            go.transform.localScale = new Vector3(200, 200, 200);

            ModelComponentUtil.ChangeUIModelParam(go);
        }

    }
}
