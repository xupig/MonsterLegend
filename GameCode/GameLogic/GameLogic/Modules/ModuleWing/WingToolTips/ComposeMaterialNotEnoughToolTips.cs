﻿using Common.Base;
using Common.Data;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleWing
{
    public class ComposeMaterialNotEnoughToolTips : KContainer
    {
        private static string[] s_tipContent = new string[3];

        private KButton _composeButton;
        private KToggle _notPromptToggle;
        private StateText[] _tipText = new StateText[3];
        private StateImage _diamondNoImage;

        private WingItemData _data;

        protected override void Awake()
        {
            _composeButton = GetChildComponent<KButton>("Button_queding");
            _notPromptToggle = GetChildComponent<KToggle>("Toggle_Xuanze");
            _diamondNoImage = GetChildComponent<StateImage>("Container_buzutishi/Container_Xiaohaopin02/Image_biaodian");
            for (int i = 0; i < 3; i++)
            {
                string index = (i + 1).ToString();
                _tipText[i] = GetChildComponent<StateText>("Container_buzutishi/Container_Xiaohaopin0" + index + "/Label_txtNr");

                if (s_tipContent[i] == null)
                {
                    s_tipContent[i] = _tipText[i].CurrentText.text;
                }
            }
            AddEventListener();
        }

        public void UpdateToolTipsView(WingItemData data)
        {
            _data = data;
            int materialId = data.ComposeMaterial[0].Key;
            int needNum = data.ComposeMaterial[0].Value;
            int currentNum = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(materialId);
            string materialName = item_helper.GetName(materialId);
            if (wing_helper.CanUseDiamondReplaceFeather(_data.wingId, materialId))
            {
                _tipText[0].CurrentText.text = string.Format(s_tipContent[0], materialName, currentNum);
                int lackNum = needNum - currentNum;
                _tipText[1].Visible = true;
                _diamondNoImage.Visible = true;
                _tipText[1].CurrentText.text = string.Format(s_tipContent[1], lackNum * wing_helper.GetWingPerFeatherDiamond(data.wingId, materialId));
                _tipText[2].GetComponent<TextWrapper>().alignment = UnityEngine.TextAnchor.UpperLeft;
                _tipText[2].CurrentText.text = MogoLanguageUtil.GetContent(6172006, materialName, needNum);
            }
            else
            {
                _tipText[0].CurrentText.text = string.Format(s_tipContent[0], materialName, needNum);
                _tipText[1].Visible = false;
                _diamondNoImage.Visible = false;
                _tipText[2].GetComponent<TextWrapper>().alignment = UnityEngine.TextAnchor.UpperCenter;
                _tipText[2].CurrentText.text = MogoLanguageUtil.GetContent(6172019, materialName, needNum);
            }

        }

        protected void AddEventListener()
        {
            _composeButton.onClick.AddListener(OnComposeClick);
            _notPromptToggle.onValueChanged.AddListener(OnToggleChange);
        }

        protected void RemoveEventListener()
        {
            _composeButton.onClick.RemoveListener(OnComposeClick);
            _notPromptToggle.onValueChanged.RemoveListener(OnToggleChange); ;
        }

        private void OnComposeClick()
        {
            //UIManager.Instance.ShowPanel(PanelIdEnum.WingPop, new WingToolTipsDataWrapper(1, null));
            //EventDispatcher.TriggerEvent(WingEvents.ENSURE_TRAIN_TEN);
            WingManager.Instance.ComposeWing(_data.wingId);
            UIManager.Instance.ClosePanel(PanelIdEnum.WingPop);
        }

        private void OnToggleChange(KToggle target, bool check)
        {
            WingComposeToolTips.IsNotPromptToggle = check;
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }
    }
}
