﻿#region 模块信息
/*==========================================
// 模块名：WingTrainToolTips
// 命名空间: ModuleWing.ChildView
// 创建者：XYM
// 修改者列表：
// 创建日期：2015/2/4 15:52:14
// 描述说明：翅膀培养提示界面
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using Common.Data;
using UnityEngine.Events;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Common.Utils;
using Common.Global;
using GameData;
using ModuleCommonUI;
using Common.ExtendComponent;
using Common.ServerConfig;
using Game.UI;
using GameMain.Entities;

namespace ModuleWing
{
    public class WingTrainToolTips : KContainer
    {
        private static string s_expGetContent;

        private KContainer _trainContainer;
        private KContainer _trainAgainContainer;
        private StateText _nameText;
        private StateText _levelText;
        private WingStarLevelBar _wingStarLevelBar;
        private KButton _trainOneButton;
        private KButton _trainTenButton;
        private StateText _expText;
        private StateText _txtNextLevelEffect;
        private StateText _trainExplainTips;
        private TweenProgressBar _ExpProgressBar;
        private StateText _descriptionText;
        private KContainer[] _attributeContainer = new KContainer[1];
        private StateText[] _valueBeforeText = new StateText[1];
        private StateText[] _valueAfterText = new StateText[1];
        private StateText _expGetText;
        private KContainer[] _costContainer = new KContainer[2];
        private IconContainer[] _iconContainer = new IconContainer[2];
        private StateText[] _costAmountText = new StateText[2];
        private ItemGrid _itemGrid;
        private WingTrainFloatTips _floatTips;

        private WingTrainType _currentType;
        private bool _isInit = true;
        private WingItemData _data;

        public static bool s_IsShowFeatherNotEnough = true;

        protected override void Awake()
        {
            _itemGrid = GetChildComponent<ItemGrid>("Container_chibangmingcheng/Container_wupin");
            _trainContainer = GetChildComponent<KContainer>("Container_peiyangqian");
            _trainAgainContainer = GetChildComponent<KContainer>("Container_peiyanghou");
            _nameText = GetChildComponent<StateText>("Container_chibangmingcheng/Label_txtMingchen");
            _levelText = GetChildComponent<StateText>("Container_chibangmingcheng/Label_txtDengji");
            _wingStarLevelBar = AddChildComponent<WingStarLevelBar>("Container_chibangmingcheng/Container_xingxing");
            _trainOneButton = GetChildComponent<KButton>("Container_peiyangqian/Button_peiyangyicii");
            _trainTenButton = GetChildComponent<KButton>("Container_peiyangqian/Button_peiyangshici");
            _txtNextLevelEffect = GetChildComponent<StateText>("Label_txtXiayiji");
            _trainExplainTips = GetChildComponent<StateText>("Label_txtTishi");
            _trainExplainTips.CurrentText.text = MogoLanguageUtil.GetContent(35788);
            _ExpProgressBar = AddChildComponent<TweenProgressBar>("Container_shuxing01/Container_jindu/ProgressBar_exp");
            _expText = GetChildComponent<StateText>("Container_shuxing01/Container_jindu/Label_txtShuju");
            _descriptionText = GetChildComponent<StateText>("Container_peiyanghou/Container_jixianchengzhang/Label_txtJixianchengzhang");
            _expGetText = GetChildComponent<StateText>("Container_peiyangqian/Container_suoxuxiaohao/Label_txtKehuojingyan");
            _floatTips = AddChildComponent<WingTrainFloatTips>("Container_peiyanghou");
            _costContainer[0] = GetChildComponent<KContainer>("Container_peiyangqian/Container_suoxuxiaohao/Container_xiaohaopin01");
            _costContainer[1] = GetChildComponent<KContainer>("Container_peiyangqian/Container_suoxuxiaohao/Container_xiaohaopin02");
            _iconContainer[0] = AddChildComponent<IconContainer>("Container_peiyangqian/Container_suoxuxiaohao/Container_xiaohaopin01/Container_icon");
            _iconContainer[1] = AddChildComponent<IconContainer>("Container_peiyangqian/Container_suoxuxiaohao/Container_xiaohaopin02/Container_icon");
            _costAmountText[0] = GetChildComponent<StateText>("Container_peiyangqian/Container_suoxuxiaohao/Container_xiaohaopin01/Label_txtshuliang");
            _costAmountText[1] = GetChildComponent<StateText>("Container_peiyangqian/Container_suoxuxiaohao/Container_xiaohaopin02/Label_txtshuliang");

            for(int i = 0; i < 1; i++)
            {
                string index = (i + 1).ToString();
                _attributeContainer[i] = GetChildComponent<KContainer>("Container_shuxing0" + index);
                _valueBeforeText[i] = GetChildComponent<StateText>("Container_shuxing0" + index + "/Label_txtValue1");
                _valueAfterText[i] = GetChildComponent<StateText>("Container_shuxing0" + index + "/Label_txtValue2");
            }

            if(s_expGetContent == null)
            {
                s_expGetContent = _expGetText.CurrentText.text;
            }

            _currentType = WingTrainType.One;
            ShowWingTrain();
        }

        public void Show(WingItemData data)
        {

            _isInit = false;
            if (data != null)
            {
                _data = data;
                _isInit = true;
            }
            this.Visible = true;
            RefreshStarLevelBar();
            RefreshToolTipsView();
        }

        private void RefreshToolTipsView()
        {
            ///如果达到满级，直接关闭
            List<string> effectDesc = wing_helper.GetAttriEffectDesc(_data.wingId, _data.Exp);
            if (effectDesc.Count == 0)
            {
                UIManager.Instance.ClosePanel(PanelIdEnum.WingPop);
                return;
            }
            _nameText.CurrentText.text = _data.name;
            _itemGrid.SetItemData(_data.icon, 1);
            _itemGrid.onClick = OnItemGridClick;
            _levelText.CurrentText.text = "LV" + _data.level.ToString();
            _expText.CurrentText.text = _data.currentExp.ToString() + "/" + _data.totalExp.ToString();
            if (_isInit == false)
            {
                _ExpProgressBar.SetProgress(_data.currentExp, _data.totalExp);
            }
            else
            {
                _ExpProgressBar.Clear();
                _ExpProgressBar.SetProgress(_data.currentExp, _data.totalExp);
            }
            _wingStarLevelBar.RefreshStarLevelBar(_data.level, _data.maxLevel, 1.0f);
            RefreshNextLevelEffect();
            RefreshCompareView();
            RefreshTrainView();
            _isInit = false;

        }

        private void OnItemGridClick()
        {
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.WingToolTips, _data.wingId, PanelIdEnum.Wing);
        }

        private void RefreshStarLevelBar()
        {
            if (_isInit == false)
            {
                _wingStarLevelBar.ShowEffect = true;
            }
            else
            {
                _wingStarLevelBar.ShowEffect = false;
            }
        }

        private void RefreshNextLevelEffect()
        {
            List<string> effectDesc = wing_helper.GetAttriEffectDesc(_data.wingId, _data.Exp);
            if (effectDesc.Count > 0)
            {
                _txtNextLevelEffect.CurrentText.text = MogoLanguageUtil.GetContent(35787, effectDesc[0]);
            }
            else
            {
                _txtNextLevelEffect.CurrentText.text = MogoLanguageUtil.GetContent(35786);
            }
        }

        public void RefreshCompareView()
        {
            int index = 0;
            wing _wing = XMLManager.wing[_data.wingId];
            WingItemData itemData = PlayerDataManager.Instance.WingData.GetItemDataById(_data.wingId);
            foreach (WingAttributeData attributeData in itemData.ArrtibuteList)
            {
                _attributeContainer[index].gameObject.SetActive(true);
                string name = attri_config_helper.GetAttributeName(attributeData.attriId);
                string value = string.Empty;
                if ((fight_attri_config)attributeData.attriId == fight_attri_config.DMG_MIN)
                {
                    value += name.Substring(0, 2);
                    value += string.Format("+{0}~{1}", attributeData.value, attributeData.maxValue);
                }
                else
                {
                    value += name;
                    value += string.Format("+{0}", attributeData.value);
                }
                _valueBeforeText[index].CurrentText.text = value;
                MogoGameObjectHelper.RecalculateTextSize(_valueBeforeText[index].CurrentText);
                index++;
            }
            for (; index < 1; index++)
            {
                _attributeContainer[index].gameObject.SetActive(false);
            }
            if (_data.level < _data.maxLevel)
            {
                WingFeatherCostData costData = PlayerDataManager.Instance.WingData.GetWingCostData(_data.wingId, _data.level);
                int addExp = costData.AddExp;
                RefreshAfterView(addExp, _wing, itemData);
            }
        }

        private void RefreshAfterView(int addExp, wing _wing, WingItemData itemData)
        {
            int currentExp = itemData.Exp + addExp;
            foreach (KeyValuePair<string, string[]> kvp in _wing.__exp_effect_id)
            {
                int startExp = int.Parse(kvp.Key);
                int endExp = int.Parse(kvp.Value[0]);
                int division = int.Parse(kvp.Value[1]);
                int startEffectId = int.Parse(kvp.Value[2]);
                if (startExp <= currentExp && currentExp < endExp)
                {
                    int relativeExp = currentExp - startExp;
                    int relativeId = relativeExp / division;
                    int effectId = startEffectId + relativeId;
                    attri_effect effect = XMLManager.attri_effect[effectId];
                    int index = 0;
                    for (int i = 0; i < effect.__attri_ids.Count; i++)
                    {
                        int attriId = int.Parse(effect.__attri_ids[i]);
                        string value = string.Empty;
                        if ((fight_attri_config)attriId == fight_attri_config.DMG_MIN)
                        {
                            value = string.Format("+{0}~{1}", effect.__attri_values[i], effect.__attri_values[i+1]);
                            i++;
                        }
                        else if ((fight_attri_config)attriId == fight_attri_config.DMG_MAX)
                        {
                            value = string.Format("+{0}~{1}", effect.__attri_values[i + 1], effect.__attri_values[i]);
                            i++;
                        }
                        else
                        {
                            value = string.Format("+{0}", effect.__attri_values[i]);
                        }
                        _valueAfterText[index].CurrentText.text = value;
                        MogoGameObjectHelper.RecalculateTextSize(_valueAfterText[index].CurrentText);
                        index++;
                    }
                }
            }
        }

        private void RefreshTrainView()
        {
            WingFeatherCostData costData = PlayerDataManager.Instance.WingData.GetWingCostData(_data.wingId, _data.level);
            int myFeatherNum = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(costData.FeatherId);
            int needDiamond = (costData.FeatherNum - myFeatherNum) * costData.FeatherDiamond;
            if(myFeatherNum >= costData.FeatherNum)
            {
                _costContainer[0].gameObject.SetActive(true);
                _costContainer[1].gameObject.SetActive(false);
                _iconContainer[0].SetIcon( costData.FeatherIcon);
                _costAmountText[0].CurrentText.text = "x" + costData.FeatherNum.ToString();
            }
            else if (myFeatherNum > 0)
            {
                _costContainer[0].gameObject.SetActive(true);
                _costContainer[1].gameObject.SetActive(true);
                _iconContainer[0].SetIcon(costData.FeatherIcon);
                _iconContainer[1].SetIcon(costData.DiamondIcon);
                _costAmountText[0].CurrentText.text = "x" + myFeatherNum.ToString();
                _costAmountText[1].CurrentText.text = "x" + needDiamond.ToString();
            }
            else
            {
                _costContainer[0].gameObject.SetActive(true);
                _costContainer[1].gameObject.SetActive(false);
                _iconContainer[0].SetIcon(costData.DiamondIcon);
                _costAmountText[0].CurrentText.text = "x" + needDiamond.ToString();
            }
            _expGetText.CurrentText.text = string.Format(s_expGetContent, costData.AddExp);
        }

        private void AddEventListener()
        {
            _trainOneButton.onClick.AddListener(OnButtonTrainOne);
            _trainTenButton.onClick.AddListener(OnButtonTrainTen);
            EventDispatcher.AddEventListener<int, int, int>(WingEvents.SHOW_TRAIN_RESULT, ShowTrainResult);
            EventDispatcher.AddEventListener(WingEvents.ENSURE_TRAIN_TEN, EnsureTrainTen);
            EventDispatcher.AddEventListener(WingEvents.REFRESH_TRAIN_TOOLTIP, RefreshToolTipsView);
        }

        private void RemoveEventListener()
        {
            _trainOneButton.onClick.RemoveListener(OnButtonTrainOne);
            _trainTenButton.onClick.RemoveListener(OnButtonTrainTen);
            EventDispatcher.RemoveEventListener<int, int, int>(WingEvents.SHOW_TRAIN_RESULT, ShowTrainResult);
            EventDispatcher.RemoveEventListener(WingEvents.ENSURE_TRAIN_TEN, EnsureTrainTen);
            EventDispatcher.RemoveEventListener(WingEvents.REFRESH_TRAIN_TOOLTIP, RefreshToolTipsView);
        }

        private void OnButtonTrainOne()
        {
            if (_data.level >= _data.maxLevel)
            {
                SystemInfoManager.Instance.Show(Common.ServerConfig.error_code.ERR_WING_TRAIN_LEVEL_LIMIT);
                return;
            }
            int needVipLevel = wing_helper.GetWingTrainNeedVipLevel(_data.wingId, _data.level);
            if (wing_helper.IsIgnoreVip == false && needVipLevel > PlayerAvatar.Player.vip_level)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(35782, needVipLevel), PanelIdEnum.Information);
                return;
            }
            WingManager.Instance.UpdateWing(_data.wingId, WingTrainType.One);
            _currentType = WingTrainType.One;
        }

        private void OnButtonTrainTen()
        {
            if (_data.level >= _data.maxLevel)
            {
                SystemInfoManager.Instance.Show(Common.ServerConfig.error_code.ERR_WING_TRAIN_LEVEL_LIMIT);
                return;
            }
            WingFeatherCostData costData = PlayerDataManager.Instance.WingData.GetWingCostData(_data.wingId, _data.level);
            int myFeatherNum = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(costData.FeatherId);
            if(myFeatherNum < costData.FeatherNum * 10 && s_IsShowFeatherNotEnough == true)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.WingPop, new WingToolTipsDataWrapper(3, _data));
                return;
            }
            EnsureTrainTen();
        }

        private void EnsureTrainTen()
        {
            WingItemData info = _data as WingItemData;
            int needVipLevel = wing_helper.GetWingTrainNeedVipLevel(_data.wingId, _data.level);
            if (wing_helper.IsIgnoreVip == false && needVipLevel > PlayerAvatar.Player.vip_level)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(35782, needVipLevel), PanelIdEnum.Information);
                return;
            }
            WingManager.Instance.UpdateWing(info.wingId, WingTrainType.Ten);
            _currentType = WingTrainType.Ten;
        }

        private void ShowTrainResult(int sCount, int mCount, int lCount)
        {
            switch(_currentType)
            {
                case WingTrainType.One:
                    ShowWingTrainResult(sCount, mCount, lCount);
                    break;
                case WingTrainType.Ten:
                    UIManager.Instance.ShowPanel(PanelIdEnum.WingPop, new WingToolTipsDataWrapper(2, new TrainDataWrapper(_data, sCount, mCount, lCount)));
                    break;
            }
        }

        private void ShowWingTrain()
        {
            _trainContainer.gameObject.SetActive(true);
            _trainAgainContainer.gameObject.SetActive(false);
        }

        private void ShowWingTrainResult(int smallCount, int middleCount, int largeCount)
        {
            string content = "";
            WingFeatherCostData costData = PlayerDataManager.Instance.WingData.GetWingCostData(_data.wingId, _data.level);
            if(smallCount > 0)
            {
                content = MogoLanguageUtil.GetString(LangEnum.WING_TIP_TRAIN_SMALL_EXP, costData.AddExp);
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, content, PanelIdEnum.Information);
            }
            else if(middleCount > 0)
            {
                int addValue = wing_helper.GetWingTrainAddValue(_data.wingId, 1);
                content = MogoLanguageUtil.GetString(LangEnum.WING_TIP_TRAIN_EXP, costData.AddExp, costData.AddExp * addValue - costData.AddExp);
                _floatTips.Show(1, content);
            }
            else
            {
                int addValue = wing_helper.GetWingTrainAddValue(_data.wingId, 2);
                content = MogoLanguageUtil.GetString(LangEnum.WING_TIP_TRAIN_EXP, costData.AddExp, costData.AddExp * addValue - costData.AddExp);
                _floatTips.Show(2, content);
            }
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

    }
}
