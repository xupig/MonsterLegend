﻿#region 模块信息
/*==========================================
// 模块名：FeatherNotEnough
// 命名空间: ModuleWing.ChildView
// 创建者：XYM
// 修改者列表：
// 创建日期：2015/2/4 16:04:15
// 描述说明：羽毛不足提示界面
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine.Events;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using Common.Data;
using GameMain.GlobalManager;

namespace ModuleWing
{
    public class FeatherNotEnoughToolTips : KContainer
    {
        private static string[] s_tipContent = new string[3];

        private KButton _ensureButton;
        private KToggle _showTipToggle;
        private StateText[] _tipText = new StateText[3];

        protected override void Awake()
        {
            _ensureButton = GetChildComponent<KButton>("Button_queding");
            _showTipToggle = GetChildComponent<KToggle>("Toggle_Xuanze");

            for (int i = 0; i < 3; i++)
            {
                string index = (i + 1).ToString();
                _tipText[i] = GetChildComponent<StateText>("Container_buzutishi/Container_Xiaohaopin0" + index + "/Label_txtNr");

                if (s_tipContent[i] == null)
                {
                    s_tipContent[i] = _tipText[i].CurrentText.text;
                }
            }
            AddEventListener();
        }

        public void UpdateToolTipsView(WingItemData data)
        {
            WingFeatherCostData costData = PlayerDataManager.Instance.WingData.GetWingCostData(data.wingId, data.level);
            int needFeatherNum = costData.FeatherNum * 10;
            int myFeatherNum = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(costData.FeatherId);
            _tipText[0].CurrentText.text = string.Format(s_tipContent[0], costData.FeatherName, myFeatherNum);
            int lackFeatherNum = needFeatherNum - myFeatherNum;
            _tipText[1].CurrentText.text = string.Format(s_tipContent[1], lackFeatherNum * costData.FeatherDiamond);
            _tipText[2].CurrentText.text = string.Format(s_tipContent[2], costData.FeatherName, needFeatherNum);
        }

        protected void AddEventListener()
        {
            _ensureButton.onClick.AddListener(OnEnsureClick);
            _showTipToggle.onValueChanged.AddListener(OnToggleChange);
        }

        protected void RemoveEventListener()
        {
            _ensureButton.onClick.RemoveListener(OnEnsureClick);
            _showTipToggle.onValueChanged.RemoveListener(OnToggleChange); ;
        }

        private void OnEnsureClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.WingPop, new WingToolTipsDataWrapper(1, null));
            EventDispatcher.TriggerEvent(WingEvents.ENSURE_TRAIN_TEN);
        }

        private void OnToggleChange(KToggle target, bool check)
        {
            WingTrainToolTips.s_IsShowFeatherNotEnough = check == false;
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }
    }
}
