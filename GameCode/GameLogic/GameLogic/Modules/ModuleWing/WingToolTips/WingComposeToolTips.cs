﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using ModuleItemChannel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleWing
{
    public class WingComposeToolTips : KContainer
    {
        private KButton _itemChannelButton;
        private KButton _composeButton;
        private KButton _closeButton;

        private KList _attributeList;

        private ItemGrid _wingIcon;

        private StateText _wingLevel;
        private StateText _wingName;
        private StateText _txtFeatherProgress;
        private KProgressBar _featherProgress;
        private ItemGrid _featherIcon;
        private WingItemData _data;

        public static bool IsNotPromptToggle = false;

        public KComponentEvent onCloseClick = new KComponentEvent();

        protected override void Awake()
        {
            base.Awake();

            _itemChannelButton = GetChildComponent<KButton>("Button_wenhao");
            _composeButton = GetChildComponent<KButton>("Button_hecheng");
            _closeButton = GetChildComponent<KButton>("Button_close");
            _attributeList = GetChildComponent<KList>("Container_shuxing/List_shuxing");
            _wingIcon = GetChildComponent<ItemGrid>("Container_xinxi/Container_wupin");
            _wingLevel = GetChildComponent<StateText>("Container_xinxi/Label_txtDengji");
            _wingName = GetChildComponent<StateText>("Container_xinxi/Label_txtMingchen");
            _txtFeatherProgress = GetChildComponent<StateText>("Container_jindu/Label_txtShuju");
            _featherProgress = GetChildComponent<KProgressBar>("Container_jindu/ProgressBar_exp");
            _featherIcon = GetChildComponent<ItemGrid>("Container_wupin");

            _attributeList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _attributeList.SetGap(-10, 0);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        public void Show(WingItemData data)
        {
            this.Visible = true;
            _data = data;
            RefreshToolTipsView();
        }

        private void AddEventListener()
        {
            _itemChannelButton.onClick.AddListener(ShowItemChannel);
            _composeButton.onClick.AddListener(ComposeButtonClick);
            _closeButton.onClick.AddListener(CloseButtonClick);
        }

        private void RemoveEventListener()
        {
            _itemChannelButton.onClick.RemoveListener(ShowItemChannel);
            _composeButton.onClick.RemoveListener(ComposeButtonClick);
            _closeButton.onClick.RemoveListener(CloseButtonClick);
        }

        private void ShowItemChannel()
        {
            ItemChannelWrapper wrapper = new ItemChannelWrapper();
            wrapper.type = 1;
            wrapper.id = _data.ComposeMaterial[0].Key;
            wrapper.context = PanelIdEnum.Information;
            UIManager.Instance.ShowPanel(PanelIdEnum.ItemChannel, wrapper);
            onCloseClick.Invoke();

        }

        private void ComposeButtonClick()
        {
            int needNum = _data.ComposeMaterial[0].Value;
            int currentNum = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(_data.ComposeMaterial[0].Key);

            if (needNum > currentNum && IsNotPromptToggle == false)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.WingPop, new WingToolTipsDataWrapper(5, _data));
                return;
            }

            ///Todo:请求合成翅膀
            ///       
            WingManager.Instance.ComposeWing(_data.wingId);
            onCloseClick.Invoke();
        }

        private void CloseButtonClick()
        {
            onCloseClick.Invoke();
        }

        private void RefreshToolTipsView()
        {
            _wingName.CurrentText.text = _data.name;
            _wingLevel.CurrentText.text = string.Format("LV:{0}", 1); //翅膀合成时显示翅膀等级为1
            _wingIcon.SetItemData(_data.icon, 1);
            _wingIcon.onClick = OnItemGridClick;
            _attributeList.RemoveAll();
            _attributeList.SetDataList<WingAttributeBar>(_data.ArrtibuteList);
            int needNum = _data.ComposeMaterial[0].Value;
            int currentNum = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(_data.ComposeMaterial[0].Key);

            _txtFeatherProgress.CurrentText.text = string.Format("{0}/{1}", currentNum, needNum);
            _featherProgress.Value = (float)currentNum / needNum;

            _featherIcon.SetItemData(_data.ComposeMaterial[0].Key);
            _featherIcon.Context = PanelIdEnum.WingPop;
        }

        private void OnItemGridClick()
        {
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.WingToolTips, _data.wingId, PanelIdEnum.Wing);
        }
    }
}
