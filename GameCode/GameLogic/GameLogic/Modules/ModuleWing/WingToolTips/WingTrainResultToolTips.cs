﻿#region 模块信息
/*==========================================
// 模块名：WingTrainResult
// 命名空间: ModuleWing.ChildView
// 创建者：XYM
// 修改者列表：
// 创建日期：2015/2/4 16:34:19
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine.Events;
using MogoEngine.Events;
using Common.Events;
using Common.Data;
using Common.Utils;
using Common.Global;
using GameData;
using GameMain.GlobalManager;

namespace ModuleWing
{
    public class WingTrainResultToolTips : KContainer
    {
        private KButton _continueButton;
        private StateText[] _tipText = new StateText[4];
        private static string tipContent;
        private int _wingId;

        protected override void Awake()
        {
            _continueButton = GetChildComponent<KButton>("Button_jixupeiyang");
            for (int i = 0; i < 4; i++)
            {
                string index = (i+1).ToString();
                _tipText[i] = GetChildComponent<StateText>("Container_huodetishi/Container_Huodepin0" + index + "/Label_txtNr");
            }
            if (tipContent == null)
            {
                tipContent = _tipText[3].CurrentText.text;
            }
            AddEventListener();
        }

        protected void AddEventListener()
        {
            _continueButton.onClick.AddListener(OnCloseClick);
        }

        protected void RemoveEventListener()
        {
            _continueButton.onClick.RemoveListener(OnCloseClick);
        }

        private void OnCloseClick()
        {
            WingItemData data = PlayerDataManager.Instance.WingData.GetItemDataById(_wingId);
            if (data!=null && data.level >= data.maxLevel)
            {
                SystemInfoManager.Instance.Show(Common.ServerConfig.error_code.ERR_WING_TRAIN_LEVEL_LIMIT);
                UIManager.Instance.ClosePanel(PanelIdEnum.WingPop);
                return;
            }
            UIManager.Instance.ShowPanel(PanelIdEnum.WingPop, new WingToolTipsDataWrapper(1, null));
        }

        public void ShowResultView(WingItemData data, int smallCount, int middleCount, int largeCount)
        {
            this.Visible = true;
            _wingId = data.wingId;
            string[] tips = new string[3];
            tips[0] = MogoLanguageUtil.GetString(LangEnum.WING_TIP_TRAIN_SMALL);
            tips[0] = string.Concat(tips[0], "x" + smallCount);
            tips[1] = MogoLanguageUtil.GetString(LangEnum.WING_TIP_TRAIN_MIDDLE);
            tips[1] = string.Concat(tips[1], "x" + middleCount);
            tips[2] = MogoLanguageUtil.GetString(LangEnum.WING_TIP_TRAIN_LARGE);
            tips[2] = string.Concat(tips[2], "x" + largeCount);
            for (int i = 0; i < 3; i++)
            {
                _tipText[i].CurrentText.text = tips[i];
            }
            WingFeatherCostData costData = PlayerDataManager.Instance.WingData.GetWingCostData(data.wingId, data.level);
            int smallRation = wing_helper.GetWingTrainAddValue(data.wingId, 0);
            int middleRatio = wing_helper.GetWingTrainAddValue(data.wingId, 1);
            int largeRatio = wing_helper.GetWingTrainAddValue(data.wingId, 2);
            int perAddExp = costData.AddExp;
            int totalExp = perAddExp * (smallCount + middleCount + largeCount);
            int totalRatio = smallCount * smallRation + middleCount * middleRatio + largeCount * largeRatio;
            _tipText[3].CurrentText.text = string.Format(tipContent, totalExp, totalRatio * perAddExp - totalExp);
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }
    }
}
