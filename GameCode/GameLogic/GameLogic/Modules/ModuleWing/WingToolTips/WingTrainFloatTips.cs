﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using Common.Data;
using UnityEngine.Events;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Common.Utils;
using Common.Global;
using GameData;
using ModuleCommonUI;
using Common.ExtendComponent;
using GameLoader.Utils.Timer;

namespace ModuleWing
{
    public class WingTrainFloatTips : KContainer
    {
        private KContainer _container;
        private StateImage _bigImage;
        private StateImage _smallImage;
        private StateText _expText;
        private uint _timerId;

        protected override void Awake()
        {
            _container = GetChildComponent<KContainer>("Container_jixianchengzhang");
            _bigImage = _container.GetChildComponent<StateImage>("Image_dabaoj");
            _smallImage = _container.GetChildComponent<StateImage>("Image_xiaobaoj");
            _expText = _container.GetChildComponent<StateText>("Label_txtHuodejingyan");
            _container.transform.localPosition = new Vector3(_container.transform.localPosition.x, _container.transform.localPosition.y + 100, 0);
            _container.Visible = true;
            _expText.Visible = true;
        }

        public void Show(int type, string content)
        {
            this.Visible = true;
            if (type == 2)
            {
                _bigImage.Visible = true;
                _smallImage.Visible = false;
                UIManager.Instance.PlayAudio("Sound/UI/surprise_huge.mp3");
            }
            else
            {
                _bigImage.Visible = false;
                _smallImage.Visible = true;
                UIManager.Instance.PlayAudio("Sound/UI/surprise_sall.mp3");
            }
            _expText.CurrentText.text = content;
            TweenViewMove tween = TweenViewMove.Begin(_container.gameObject, MoveType.Show, MoveDirection.Down, 0.5f);
            tween.Tweener.method = UITweener.Method.BounceIn;
            ResetTimer();
            _timerId = TimerHeap.AddTimer(1500, 0, OnRiseEnd);
        }

        private void OnRiseEnd()
        {
            _container.Visible = false;
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        protected override void OnDisable()
        {
            ResetTimer();
        }

    }
}
