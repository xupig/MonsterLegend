﻿#region 模块信息
/*==========================================
// 模块名：WingPanel
// 命名空间: ModuleWing
// 创建者：XYM
// 修改者列表：
// 创建日期：2015/01/23
// 描述说明：翅膀模块Panel
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;

using Common.Base;
using MogoEngine.Events;
using Common.Events;

using Game.UI.UIComponent;
using GameMain.GlobalManager;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;

namespace ModuleWing
{
    public class WingPanel : BasePanel
    {
        private WingCurrencyView _wingCurrencyView;
        private WingListView _wingListView;
        private CategoryList _wingCategory;
        private WingModelView _wingModelView;
        private KButton _btnProperity;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Wing; }
        }

        public override void OnShow(object data)
        {
            ResetWingList();
            RefreshWingPoint();
            AddEventListener();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        protected override void Awake()
        {
            _btnProperity = GetChildComponent<KButton>("Button_btn3");
            _wingCurrencyView = AddChildComponent<WingCurrencyView>("Container_currency");
            _wingListView = AddChildComponent<WingListView>("Container_wingView");
            _wingCategory = GetChildComponent<CategoryList>("Container_category/List_categoryList");
            _wingModelView = AddChildComponent<WingModelView>("Container_wingModelView");
            RefreshCategoryList();
        }

        private void RefreshCategoryList()
        {
            List<CategoryItemData> listToggleData = new List<CategoryItemData>();
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(35765)));
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(35766)));
            _wingCategory.RemoveAll();
            _wingCategory.SetDataList<CategoryListItem>(listToggleData);
        }

        public void AddEventListener()
        {
            _btnProperity.onClick.AddListener(ShowWingAttributeView);
            _wingCategory.onSelectedIndexChanged.AddListener(OnListToggleGroupSelect);
            _wingModelView.onResetWingModel.AddListener(ResetWingList);
            EventDispatcher.AddEventListener(WingEvents.CLOSE_WING_ATTRIBUTE_VIEW, ShowWingListView);
            EventDispatcher.AddEventListener(WingEvents.REFRESH_NOTICE_POINT, RefreshWingPoint);
        }

        public void RemoveEventListener()
        {
            _btnProperity.onClick.RemoveListener(ShowWingAttributeView);
            _wingCategory.onSelectedIndexChanged.RemoveListener(OnListToggleGroupSelect);
            _wingModelView.onResetWingModel.RemoveListener(ResetWingList);
            EventDispatcher.RemoveEventListener(WingEvents.CLOSE_WING_ATTRIBUTE_VIEW, ShowWingListView);
            EventDispatcher.RemoveEventListener(WingEvents.REFRESH_NOTICE_POINT, RefreshWingPoint);
        }

        private void RefreshWingPoint()
        {
            WingData wingData = PlayerDataManager.Instance.WingData;
            (_wingCategory.ItemList[0] as CategoryListItem).SetPoint(false);
            (_wingCategory.ItemList[1] as CategoryListItem).SetPoint(false);
            if (wingData.HasNewWingByType(WingType.SPIRIT) || wingData.CanTrainByType(WingType.SPIRIT))
            {
                (_wingCategory.ItemList[0] as CategoryListItem).SetPoint(true);
            }
            if (wingData.HasNewWingByType(WingType.PHANTOM) || wingData.CanTrainByType(WingType.PHANTOM))
            {
                (_wingCategory.ItemList[1] as CategoryListItem).SetPoint(true);
            }
        }

        private void OnListToggleGroupSelect(CategoryList group, int index)
        {
            _wingListView.ChangeWingType(index);
            ShowWingListView();
        }

        private void ResetWingList()
        {
            WingItemData data = PlayerDataManager.Instance.WingData.GetPutOnWingData();
            int index = 0;
            if (data != null)
            {
                index = (int)data.type - 1;
            }
            _wingCategory.SelectedIndex = index;
            _wingListView.ChangeWingType(index);
            ShowWingListView();
        }

        private void ShowWingListView()
        {
            _wingCategory.gameObject.SetActive(true);
            _wingListView.gameObject.SetActive(true);
        }

        private void ShowWingAttributeView()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.WingAttribute);
            _wingCategory.gameObject.SetActive(false);
            _wingListView.gameObject.SetActive(false);
        }

    }
}
