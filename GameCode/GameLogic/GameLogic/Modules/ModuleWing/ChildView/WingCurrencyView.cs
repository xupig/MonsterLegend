﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Common.Base;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils;

using UnityEngine.UI;
using Game.UI.UIComponent;
using GameMain.Entities;
using UnityEngine.Events;
using ModuleCommonUI;
using Common.Utils;
using MogoEngine;
using Common.ClientConfig;
using Common.ExtendComponent;
using Common.ServerConfig;
using GameData;

namespace ModuleWing
{
    public class WingCurrencyView : KContainer
    {
        private MoneyItem _diamondItem;
        private MoneyItem _bindDiamondItem;

        protected override void Awake()
        {
            _diamondItem = AddChildComponent<MoneyItem>("Container_buyDiamond");
            _bindDiamondItem = AddChildComponent<MoneyItem>("Container_buyTicket");
            _diamondItem.SetMoneyType(public_config.MONEY_TYPE_DIAMOND);
            _bindDiamondItem.SetMoneyType(public_config.MONEY_TYPE_BIND_DIAMOND);
        }

    }
}

