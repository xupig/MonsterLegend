﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Common.Base;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils;

using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using GameMain.GlobalManager;

namespace ModuleWing
{
    public class WingListView : KContainer
    {
        private WingListScrollView _wingListScrollView;
        private WingDetailView _wingDetailView;
        private bool _isWingDetailDataChanged = true;
        private List<WingItemData> _dataList = new List<WingItemData>();
        private WingType _currentType;

        protected override void Awake()
        {
            _wingListScrollView = AddChildComponent<WingListScrollView>("ScrollView_wingList");
            _wingDetailView = AddChildComponent<WingDetailView>("Container_detail");
            _currentType = WingType.NONE;
        }

        private void AddEventListener()
        {
            _wingListScrollView.onWingItemSelected.AddListener(ShowDetailPage);
            EventDispatcher.AddEventListener(WingEvents.CLOSE_DETAIL_ITEM, ShowListScrollView);
            EventDispatcher.AddEventListener<int>(WingEvents.SHOW_DETAIL_VIEW, ShowWingDetailPage);
        }

        private void RemoveEventListener( )
        {
            _wingListScrollView.onWingItemSelected.RemoveListener(ShowDetailPage);
            EventDispatcher.RemoveEventListener(WingEvents.CLOSE_DETAIL_ITEM, ShowListScrollView);
            EventDispatcher.RemoveEventListener<int>(WingEvents.SHOW_DETAIL_VIEW, ShowWingDetailPage);
        }

        private void ShowWingDetailPage(int index)
        {
            _wingDetailView.gameObject.SetActive(true);
            _wingListScrollView.gameObject.SetActive(false);
            if (_isWingDetailDataChanged == true)
            {
                _isWingDetailDataChanged = false;
                _wingDetailView.SetWingDataList(_dataList);
            }
            _wingDetailView.ShowPageByIndex(index);
        }

        protected override void OnEnable()
        {
            AddEventListener();
            UpdateWingDataList();
            ShowListScrollView();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void ShowDetailPage(WingListScrollView target, int index)
        {
            ShowWingDetailPage(index);
        }

        private void UpdateWingDataList()
        {
            Dictionary<int, WingItemData> wingItem = PlayerDataManager.Instance.WingData.GetAllItemData();
            _dataList.Clear();
            foreach (WingItemData itemData in wingItem.Values)
            {
                if (itemData.type == _currentType)
                {
                    _dataList.Add(itemData);
                }
            }
            _dataList.Sort(SortFun);
            _wingListScrollView.SetWingDataList(_dataList);
            _isWingDetailDataChanged = true;
        }

        private static int SortFun(WingItemData a, WingItemData b)
        {
            if (a.isPutOn == true && b.isPutOn == false)
            {
                return -1;
            }
            if (a.isPutOn == false && b.isPutOn == true)
            {
                return 1;
            }
            if (a.isOwned == true && b.isOwned == false)
            {
                return -1;
            }
            if (a.isOwned == false && b.isOwned == true)
            {
                return 1;
            }
            return a.wingId - b.wingId;
        }

        private void ShowListScrollView()
        {
            _wingDetailView.gameObject.SetActive(false);
            _wingListScrollView.Show();
        }

        public void ChangeWingType(int index)
        {
            WingType type = WingType.NONE;
            switch (index)
            {
                case 0:
                    type = WingType.SPIRIT;
                    break;
                case 1:
                    type = WingType.PHANTOM;
                    break;
            }
            if (_currentType != type)
            {
                _currentType = type;
                UpdateWingDataList();
            }
            PlayerDataManager.Instance.WingData.RemoveNewWingByType(_currentType);
            ShowListScrollView();
        }

    }
}
 

