﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Common.Base;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils;

using Game.UI.UIComponent;
using GameMain.GlobalManager;
using Common.Data;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using Common.Utils;
using Common.ExtendComponent;

namespace ModuleWing
{
    public class WingAttributePanel : BasePanel
    {
        private StateText _fightingCapacityText;
        private StateText _descriptionText;
        private KList _wingAttributeBarList;
        private KContainer _wingAttributeContainer;

        private static string s_rankContent;

        protected override void Awake()
        {
            _wingAttributeContainer = GetChildComponent<KContainer>("Container_property");
            CloseBtn = GetChildComponent<KButton>("Container_property/Button_close");
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _fightingCapacityText = GetChildComponent<StateText>("Container_property/Container_biaoti/Label_txtZhandouli");
            _descriptionText = GetChildComponent<StateText>("Container_property/Container_tishi/Label_txtTishi");
            _wingAttributeBarList = GetChildComponent<KList>("Container_property/List_suoyoushuxing");
            _wingAttributeBarList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _wingAttributeBarList.SetGap(5, 0);
            if (s_rankContent == null)
            {
                s_rankContent = _descriptionText.CurrentText.text;
            }
            _descriptionText.CurrentText.text = string.Empty;
        }

        protected override void OnCloseBtnClick()
        {
            TweenViewMove.Begin(_wingAttributeContainer.gameObject, MoveType.Hide, MoveDirection.Right).Tweener.onFinished = OnTweenFinshed;
        }

        private void OnTweenFinshed(UITweener tween)
        {
            ClosePanel();
        }


        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.WingAttribute; }
        }

        private void UpdateAttributeView()
        {
            _wingAttributeBarList.RemoveAll();
            List<WingAttributeData> dataList = PlayerDataManager.Instance.WingData.GetAllArrtibuteList();
            _wingAttributeBarList.SetDataList<WingAttributeBar>(dataList);

            Dictionary<int, float> attriDict = new Dictionary<int, float>();
            for (int i = 0; i < dataList.Count; i++)
            {
                attriDict.Add(dataList[i].attriId, dataList[i].value);
            }
            int fightForce = fight_force_helper.GetFightForce(attriDict, PlayerAvatar.Player.vocation);
            _fightingCapacityText.CurrentText.text = fightForce.ToString();

            WingManager.Instance.GetWingRank();
        }

        private void RefreshWingRank()
        {
            _descriptionText.CurrentText.text = string.Format(s_rankContent, PlayerDataManager.Instance.WingData.Rank);
        }

        public override void OnShow(object data)
        {
            TweenViewMove.Begin(_wingAttributeContainer.gameObject, MoveType.Show, MoveDirection.Right);
            UpdateAttributeView();
            RefreshWingRank();
            EventDispatcher.AddEventListener(WingEvents.REFRESH_WING_RANK, RefreshWingRank);
        }

        public override void OnClose()
        {
            EventDispatcher.RemoveEventListener(WingEvents.REFRESH_WING_RANK, RefreshWingRank);
            EventDispatcher.TriggerEvent(WingEvents.CLOSE_WING_ATTRIBUTE_VIEW);
        }

    }
}
 

