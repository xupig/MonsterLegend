﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Common.Base;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils;

using Game.UI.UIComponent;
using Common.Data;

namespace ModuleWing
{
    public class TrainDataWrapper
    {
        public WingItemData data;
        public int smallCount;
        public int middleCount;
        public int largeCount;

        public TrainDataWrapper(WingItemData data, int smallCount, int middleCount, int largeCount)
        {
            this.data = data;
            this.smallCount = smallCount;
            this.middleCount = middleCount;
            this.largeCount = largeCount;
        }
    }

    public class WingToolTipsDataWrapper
    {
        public int type;
        public object data;

        public WingToolTipsDataWrapper(int type, object data)
        {
            this.type = type;
            this.data = data;
        }
    }

    public class WingPopPanel : BasePanel
    {
        private WingTrainToolTips _wingTrainToolTips;
        private WingTrainResultToolTips _wingTrainResultToolTips;
        private FeatherNotEnoughToolTips _featherNotEnoughToolTips;
        private WingComposeToolTips _wingComposeToolTips;
        private ComposeMaterialNotEnoughToolTips _composeMaterialNotEnoughToolTips;
        private StateImage _imageMask;
        private KContainer _backgroundContainer;

        //private KButton _closeButton;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_shilianzhiluBG/Button_close");
            _wingTrainToolTips = AddChildComponent<WingTrainToolTips>("Container_chibangpeiyang");
            _wingTrainResultToolTips = AddChildComponent<WingTrainResultToolTips>("Container_bencipeiyang");
            _featherNotEnoughToolTips = AddChildComponent<FeatherNotEnoughToolTips>("Container_yumaobuzu");
            _wingComposeToolTips = AddChildComponent<WingComposeToolTips>("Container_chibanghecheng");
            _composeMaterialNotEnoughToolTips = AddChildComponent<ComposeMaterialNotEnoughToolTips>("Container_cailiaobuzu");
            _imageMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _backgroundContainer = GetChildComponent<KContainer>("Container_shilianzhiluBG");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.WingPop; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void SetData(object data)
        {
            WingToolTipsDataWrapper wrapper = data as WingToolTipsDataWrapper;
            int type = wrapper.type;
            switch (type)
            {
                case 1:
                    ShowTrainToolTips(wrapper.data as WingItemData);
                    break;
                case 2:
                    ShowResultToolTips(wrapper.data as TrainDataWrapper);
                    break;
                case 3:
                    ShowFeatherToolTips(wrapper.data as WingItemData);
                    break;
                case 4:
                    ShowComposeToolTips(wrapper.data as WingItemData);
                    break;
                case 5:
                    ShowComposeMaterialNotEnoughToolTips(wrapper.data as WingItemData);
                    break;
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
            EventDispatcher.TriggerEvent(WingEvents.REFRESH_NOTICE_POINT);
        }

        private void AddEventListener()
        {
            _wingComposeToolTips.onCloseClick.AddListener(ClosePanel);
        }

        private void RemoveEventListener()
        {
            _wingComposeToolTips.onCloseClick.RemoveListener(ClosePanel);
        }

        private void ShowComposeToolTips(WingItemData data)
        {
            _imageMask.gameObject.SetActive(true);
            _backgroundContainer.gameObject.SetActive(false);
            _wingTrainResultToolTips.gameObject.SetActive(false);
            _featherNotEnoughToolTips.gameObject.SetActive(false);
            _wingTrainToolTips.gameObject.SetActive(false);
            _composeMaterialNotEnoughToolTips.gameObject.SetActive(false);
            _wingComposeToolTips.Show(data);
        }

        private void ShowTrainToolTips(WingItemData data)
        {
            _imageMask.gameObject.SetActive(true);
            _backgroundContainer.gameObject.SetActive(true);
            _wingTrainResultToolTips.gameObject.SetActive(false);
            _featherNotEnoughToolTips.gameObject.SetActive(false);
            _wingComposeToolTips.gameObject.SetActive(false);
            _composeMaterialNotEnoughToolTips.gameObject.SetActive(false);
            _wingTrainToolTips.Show(data);
        }

        private void ShowResultToolTips(TrainDataWrapper trainData)
        {
            _imageMask.gameObject.SetActive(true);
            _backgroundContainer.gameObject.SetActive(true);
            _wingTrainToolTips.gameObject.SetActive(false);
            _featherNotEnoughToolTips.gameObject.SetActive(false);
            _wingComposeToolTips.gameObject.SetActive(false);
            _composeMaterialNotEnoughToolTips.gameObject.SetActive(false);
            _wingTrainResultToolTips.ShowResultView(trainData.data, trainData.smallCount, trainData.middleCount, trainData.largeCount);

        }

        private void ShowFeatherToolTips(WingItemData data)
        {
            _imageMask.gameObject.SetActive(true);
            _backgroundContainer.gameObject.SetActive(true);
            _wingTrainToolTips.gameObject.SetActive(false);
            _wingTrainResultToolTips.gameObject.SetActive(false);
            _wingComposeToolTips.gameObject.SetActive(false);
            _composeMaterialNotEnoughToolTips.gameObject.SetActive(false);
            _featherNotEnoughToolTips.gameObject.SetActive(true);
            _featherNotEnoughToolTips.UpdateToolTipsView(data);
        }

        private void ShowComposeMaterialNotEnoughToolTips(WingItemData data)
        {
            _imageMask.gameObject.SetActive(true);
            _backgroundContainer.gameObject.SetActive(true);
            _wingTrainToolTips.gameObject.SetActive(false);
            _wingTrainResultToolTips.gameObject.SetActive(false);
            _wingComposeToolTips.gameObject.SetActive(false);
            _composeMaterialNotEnoughToolTips.gameObject.SetActive(true);
            _featherNotEnoughToolTips.gameObject.SetActive(false);
            _composeMaterialNotEnoughToolTips.UpdateToolTipsView(data);
        }

    }
}
 

