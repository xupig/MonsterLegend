﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using UnityEngine;
using Common.Base;
using GameMain.Entities;
using Common.Utils;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Game.Asset;
using GameMain.GlobalManager;
using Common.Data;
using MogoEngine.Events;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using ACTSystem;

namespace ModuleWing
{
    public class WingModelView : KContainer
    {
        private int _currentModelId;
        private ModelComponent _model;
        private ACTActor _actor;
        private KButton _resetButton;

        public KComponentEvent onResetWingModel = new KComponentEvent();

        private readonly Vector3 WING_POSITION = new Vector3(0, 1.5f, -0.1f);

        public static int currentWingId = 0;

        protected override void Awake()
        {
            _model = AddChildComponent<ModelComponent>("Container_wingModel");
            _resetButton = GetChildComponent<KButton>("Button_shuaxin");
        }

        private void OnAvatarModelLoaded(ACTActor actor)
        {
            _actor = actor;
            _model.PlayRandomIdleAction();
            _actor.localPosition = new Vector3(_actor.localPosition.x, _actor.localPosition.y, 4000);
        }

        protected void AddEventListener()
        {
            EventDispatcher.AddEventListener<int>(WingEvents.CHANGE_WING_MODEL, AddWingModel);
            _resetButton.onClick.AddListener(ResetModelView);
        }

        protected void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<int>(WingEvents.CHANGE_WING_MODEL, AddWingModel);
            _resetButton.onClick.RemoveListener(ResetModelView);
        }


        private void ResetModelView()
        {
            WingItemData putOnData = PlayerDataManager.Instance.WingData.GetPutOnWingData();
            if (putOnData != null)
            {
                AddWingModel(putOnData.wingId);
                onResetWingModel.Invoke();
            }
            else
            {
                AddWingModel(0);
            }
        }

        private void AddWingModel(int wingId)
        {
            currentWingId = wingId;
            int modelId = 0;
            if(wingId != 0)
            {
                modelId = PlayerDataManager.Instance.WingData.GetItemDataById(wingId).ModelId;
            }
            EventDispatcher.TriggerEvent(WingEvents.REFRESH_SELECT_ID);
            RefreshTip(modelId);
            RefreshModel(modelId);
        }

        private void RefreshModel(int modelId)
        {
            if (_currentModelId == modelId)
            {
                return;
            }
            _currentModelId = modelId;
            if (_actor == null) return;
            _actor.equipController.equipWing.onLoadEquipFinished = OnWingGoLoaded;
            _actor.equipController.equipWing.PutOn(modelId);
        }

        private void OnWingGoLoaded()
        {
            ModelComponentUtil.ChangeUIModelParam(_actor.gameObject);
        }

        private void RefreshTip(int modelId)
        {
            int putOnModeId = 0;
            WingItemData putOnData = PlayerDataManager.Instance.WingData.GetPutOnWingData();
            if (putOnData != null)
            {
                putOnModeId = putOnData.ModelId;
            }
        }

        protected override void OnEnable()
        {
            ResetModelView();
            AddEventListener();
            _model.LoadPlayerModel(OnAvatarModelLoaded);
        }

        protected override void OnDisable()
        {
            _model.RemoveAnimationTimer();
            RemoveEventListener();
        }

    }
}
