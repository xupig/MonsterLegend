﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Common.Base;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils;
using UnityEngine.Events;

using Game.UI.UIComponent;
using GameMain.GlobalManager;
using Common.Data;
using GameData;

namespace ModuleWing
{
    class WingSuitAttributeBar : KList.KListItemBase
    {
        private static string s_activeContent;
        private static string s_notActiveContent;

        private StateText _activeAttributeText;
        private StateText _notActiveAttributeText;

        protected override void Awake()
        {
            KContainer container = this.gameObject.GetComponent<KContainer>();
            _activeAttributeText = container.GetChildComponent<StateText>("Label_txtNameActive");
            _notActiveAttributeText = container.GetChildComponent<StateText>("Label_txtNameNotActive");

            if (s_activeContent == null)
            {
                s_activeContent = _activeAttributeText.CurrentText.text;
            }
            if (s_notActiveContent == null)
            {
                s_notActiveContent = _notActiveAttributeText.CurrentText.text;
            }
        }

        public override object Data
        {
            get
            {
                return base.Data;
            }
            set
            {
                base.Data = value;
                Refresh(base.Data as WingSuitAttributeData);
            }
        }

        private void Refresh(WingSuitAttributeData info)
        {
            if (info.isActive == true)
            {
                _activeAttributeText.gameObject.SetActive(true);
                _notActiveAttributeText.gameObject.SetActive(false);
                string name = attri_config_helper.GetAttributeName(info.attriId);
                _activeAttributeText.CurrentText.text = string.Format(s_activeContent, name, info.value, info.description);
            }
            else
            {
                _activeAttributeText.gameObject.SetActive(false);
                _notActiveAttributeText.gameObject.SetActive(true);
                string name = attri_config_helper.GetAttributeName(info.attriId);
                _notActiveAttributeText.CurrentText.text = string.Format(s_notActiveContent, name, info.value, info.description); ;
            }
        }

        public override void Dispose()
        {
            base.Data = null;
        }

    }
}
