﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Common.Base;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils;
using UnityEngine.Events;

using Game.UI.UIComponent;
using GameMain.GlobalManager;
using Common.Data;
using GameData;
using Common.ServerConfig;

namespace ModuleWing
{
    public class WingAttributeBar : KList.KListItemBase
    {
        private static string s_attributeContent;

        private StateText _attributeText;

        protected override void Awake()
        {
            KContainer container = this.gameObject.GetComponent<KContainer>();
            _attributeText = container.GetChildComponent<StateText>("Label_txtName");

            if (s_attributeContent == null)
            {
                s_attributeContent = _attributeText.CurrentText.text;
            }
        }

        public override object Data
        {
            get
            {
                return base.Data;
            }
            set
            {
                base.Data = value;
                this.Refresh(base.Data as WingAttributeData);
            }
        }

        private void Refresh(WingAttributeData info)
        {
            string name = attri_config_helper.GetAttributeName(info.attriId);
            if ((fight_attri_config)info.attriId == fight_attri_config.DMG_MIN)
            {
                name = name.Substring(0, 2);
                string value = string.Format("{0}~{1}", info.value, info.maxValue);
                _attributeText.CurrentText.text = string.Format(s_attributeContent, name, value);
            }
            else
            {
                _attributeText.CurrentText.text = string.Format(s_attributeContent, name, info.value);
            }
        }

        public override void Dispose()
        {
            base.Data = null;
        }

    }
}
