﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Common.Base;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils;
using UnityEngine.Events;

using Game.UI.UIComponent;
using GameMain.GlobalManager;

namespace ModuleWing
{
    public class WingStarLevelBar : KContainer
    {
        private int _maxLevel = 5;
        private int _currentLevel = 1;
        private float _division = 1.0f; //分度值

        private KContainer[] _starContainer = new KContainer[5];
        private StateImage[] _starImage = new StateImage[5];
        private GameObject[] _effectGo = new GameObject[5];
        private GameObject _effectTemplate;

        private bool _showEffect = false;
        public bool ShowEffect
        {
            get
            {
                return _showEffect;
            }
            set
            {
                _showEffect = value;
                ResetEffect();
            }
        }

        private void ResetEffect()
        {
            for (int i = 0; i < 5; i++)
            {
                if (_effectGo[i] != null)
                {
                    _effectGo[i].SetActive(false);
                }
            }
        }

        protected override void Awake()
        {
            for (int i = 0; i < 5; i++)
            {
                string index = (i + 1).ToString();
                _starContainer[i] = GetChildComponent<KContainer>("Container_xingxing0"+index);
                _starImage[i] = GetChildComponent<StateImage>("Container_xingxing0" + index + "/Image_xingxingIcon");
                _starImage[i].CurrentImage.type = UnityEngine.UI.Image.Type.Filled;
                _starImage[i].CurrentImage.fillMethod = UnityEngine.UI.Image.FillMethod.Horizontal;
            }
            _effectTemplate = GetChild("fx_ui_21_4_star_01");
            if (_effectTemplate != null)
            {
                _effectTemplate.SetActive(false);
            }
        }


        public void RefreshStarLevelBar(int level, int maxLevel, float division)
        {
            _currentLevel = level;
            _maxLevel = maxLevel;
            _division = division;
            UpdataStarCount();
            UpdataLightStar();
        }

        public int MaxLevel
        {
            get { return _maxLevel; }
            set 
            { 
                _maxLevel = value;
                UpdataStarCount();
                UpdataLightStar();
            }
        }

        public int Level
        {
            get { return _currentLevel; }
            set
            {
                _currentLevel = value;
                UpdataStarCount();
                UpdataLightStar();
            }
        }

        public float Division
        {
            get { return _division; }
            set 
            { 
                _division = value;
                UpdataStarCount();
                UpdataLightStar();
            }
        }

        private void UpdataStarCount()
        {
            int count = Mathf.CeilToInt(_maxLevel / _division);
            for (int i = 0; i < count; i++)
            {
                _starContainer[i].gameObject.SetActive(true);
            }
            for (int i = count; i < 5; i++)
            {
                _starContainer[i].gameObject.SetActive(false);
            }
        }

        private void UpdataLightStar()
        {
            float x = (float)_currentLevel / _division;
            int count = Mathf.CeilToInt(_maxLevel / _division);
            int i;
            for (i = 0; i < Mathf.FloorToInt(x); i++)
            {
                SetStarAmount(i,1);
            }
            if (i < count)
            {
                SetStarAmount(i, x - Mathf.Floor(x));
                i++;
            }
            while (i < count)
            {
                SetStarAmount(i, 0);
                i++;
            }
        }

        private void SetStarAmount(int index, float value)
        {
            float current = _starImage[index].CurrentImage.fillAmount;
            _starImage[index].CurrentImage.fillAmount = value;
            if (_showEffect == true && current < value && value > 0)
            {
                ShowStarEffect(index);
            }
        }

        private void HideStarEffect(int index)
        {
            if (_effectGo[index] != null)
            {
                _effectGo[index].SetActive(false);
            }
        }

        private void ShowStarEffect(int index)
        {
            if (_effectGo[index] == null)
            {
                _effectGo[index] = Instantiate(_effectTemplate) as GameObject;
                _effectGo[index].transform.SetParent(_starContainer[index].transform, false);
                _effectGo[index].transform.localPosition = Vector3.zero;
            }
            _effectGo[index].SetActive(false);
            _effectGo[index].SetActive(true);
        }

    }
}
