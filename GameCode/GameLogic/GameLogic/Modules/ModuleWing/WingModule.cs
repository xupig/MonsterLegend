﻿#region 模块信息
/*==========================================
// 模块名：WingModule
// 命名空间: ModuleWing
// 创建者：XYM
// 修改者列表：
// 创建日期：2015/01/23
// 描述说明：翅膀模块Module
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;

using System.Text;

using UnityEngine;
using Common.Base;
using Common;
using Common.Events;
using MogoEngine.RPC;
using MogoEngine.Events;
using GameLoader.Config;

using GameMain.Entities;
using MogoEngine;
using Common.ServerConfig;

using GameLoader.Utils.CustomType;

namespace ModuleWing
{
    public class WingModule : BaseModule
    {

        public WingModule() : base()
        {
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init( )
        {
            RegisterPopPanelBunchPanel(PanelIdEnum.WingPop, PanelIdEnum.Wing);
            AddPanel(PanelIdEnum.WingPop, "Container_WingPopPanel", MogoUILayer.LayerUIPopPanel, "ModuleWing.WingPopPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.WingGet, "Container_WingGetPanel", MogoUILayer.LayerUINotice, "ModuleWing.WingGetPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.WingAttribute, "Container_WingPropertyPanel", MogoUILayer.LayerUIToolTip, "ModuleWing.WingAttributePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }


    }
}
