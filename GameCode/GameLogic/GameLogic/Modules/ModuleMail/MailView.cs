﻿#region 模块信息
/*==========================================
// 文件名：MailPanel
// 命名空间: ModuleMail
// 创建者：李晓帅
// 修改者列表：
// 创建日期：2015/1/24
// 描述说明：邮箱面板Panel
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using GameLoader.Utils;

using Common.Base;
using Common.ServerConfig;
using MogoEngine.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using GameMain.GlobalManager;
using Common.Events;
using Common.Structs;
using Common.Data;
using Common.ExtendComponent;

namespace ModuleMail
{
    public class MailPanel : BasePanel
    {
        private const int INBOX_INDEX = 0;
        private const int OUTBOX_INDEX = 1;

        private MailInboxView _leftInbox;
        private MailOutboxView _leftOutbox;
        private KToggleGroup _mailboxNavigation;
        private KContainer _leftNewInboxNotice;

        private KContainer _rightBgContainer;
        private KContainer _leftBgContainer;

        /// <summary>
        /// 以下变量暂时为动画服务
        /// </summary>
        private GameObject navigationObj;
        private GameObject mailInboxLeftObj;

        protected override void Awake()
        {
            _leftInbox = AddChildComponent<MailInboxView>("Container_inboxview");
            _mailboxNavigation = GetChildComponent<KToggleGroup>("Container_mailtoggle/ToggleGroup_inboxoutbox");
            _leftNewInboxNotice = GetChildComponent<KContainer>("Container_mailtoggle/Container_newInboxNotice");
            _leftOutbox = AddChildComponent<MailOutboxView>("Container_outboxview");

            _rightBgContainer = GetChildComponent<KContainer>("Container_panelBgright");
            _leftBgContainer = GetChildComponent<KContainer>("Container_panelBgleft");

            navigationObj = this.GetChild("Container_mailtoggle");
            mailInboxLeftObj = this.GetChild("Container_inboxview/Container_left");

        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Mail; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            OnTweenLeftBg();
            OnTweenRightBg();
            _mailboxNavigation.onSelectedIndexChanged.AddListener(OnMailboxIndexChanged);
            if (data != null)
            {
                OnMailboxIndexChanged(_mailboxNavigation, (int)data - 1);
                _mailboxNavigation.SelectIndex = (int)data - 1;
            }
            else
            {
                OnMailboxIndexChanged(_mailboxNavigation, INBOX_INDEX);
                _mailboxNavigation.SelectIndex = INBOX_INDEX;

            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
            if (_mailboxNavigation!=null)
            {
                _mailboxNavigation.onSelectedIndexChanged.RemoveListener(OnMailboxIndexChanged);
            }
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(MailEvents.TWEEN_RIGHT_BG, OnTweenRightBg);
            EventDispatcher.AddEventListener(MailEvents.TWEEN_LEFT_BG, OnTweenLeftBg);
            EventDispatcher.AddEventListener<int>(MailEvents.REFRESH_MAIL_LIST, RefreshPage);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(MailEvents.TWEEN_RIGHT_BG, OnTweenRightBg);
            EventDispatcher.RemoveEventListener(MailEvents.TWEEN_LEFT_BG, OnTweenLeftBg);
            EventDispatcher.RemoveEventListener<int>(MailEvents.REFRESH_MAIL_LIST, RefreshPage);

        }

        private void OnMailboxIndexChanged(KToggleGroup _toggleGroup, int _index)
        {
            _mailboxNavigation.gameObject.SetActive(true);
            if(INBOX_INDEX == _index)
            {
                _leftInbox.gameObject.SetActive(true);
                _leftOutbox.gameObject.SetActive(false);
            }
            else if(OUTBOX_INDEX == _index)
            {
                _leftInbox.gameObject.SetActive(false);
                _leftOutbox.gameObject.SetActive(true);
            }
        }

        private void OnTweenRightBg()
        {
            TweenViewMove.Begin(_rightBgContainer.gameObject, MoveType.Show, MoveDirection.Right);
        }

        private void OnTweenLeftBg()
        {
            TweenViewMove.Begin(navigationObj, MoveType.Show, MoveDirection.Left);
            TweenViewMove.Begin(mailInboxLeftObj, MoveType.Show, MoveDirection.Left);
            TweenViewMove.Begin(_leftBgContainer.gameObject, MoveType.Show, MoveDirection.Left);
        }

        private void RefreshPage(int page)
        {
            _leftInbox.RefreshPage(page);
        }
    }
}
