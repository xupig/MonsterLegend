﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.UIComponent;
using Common.Structs;
using GameData;
using Common.Utils;
using Common.Data;
using UnityEngine;
using Common.ServerConfig;
using Common.ExtendComponent;
using Common.Base;

namespace ModuleMail
{
    public class AttachmentItemGrid : KList.KListItemBase
    {
        private StateText _txtNum;
        private ItemGrid _iconContainer;
        private AttachmentItemData _data;
        private GameObject _isDrawIcon;

        protected override void Awake()
        {
            _isDrawIcon = this.GetChild("Image_idDraw");
            _iconContainer = GetChildComponent<ItemGrid>("Container_wupin");
            _iconContainer.Context = PanelIdEnum.Interactive;
            _txtNum = GetChildComponent<StateText>("Label_txtNum");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                RemoveSprite();
                if (value != null)
                {
                    _data = value as AttachmentItemData;
                    Refresh();
                }
            }
        }

        private void ItemClickHandler()
        {
            onClick.Invoke(this, Index);
        }

        private void RemoveSprite()
        {
            _iconContainer.Clear();
            _data = null;
        }

        public void Refresh()
        {
            if (_data.IsNotShowTip == false)
            {
                _iconContainer.onClick = null;
            }
            else
            {
                _iconContainer.onClick = ItemClickHandler;
            }
            _txtNum.CurrentText.text = _data.BaseItemData.StackCount.ToString();
            _iconContainer.SetItemData(_data.BaseItemData);

            if (_isDrawIcon != null)
            {
                if (_data != null)
                {
                    if (_data.AttachmentState == public_config.MAIL_NOT_GET)
                    {
                        _isDrawIcon.SetActive(false);
                    }
                    else if (_data.AttachmentState == public_config.MAIL_GET)
                    {
                        _isDrawIcon.SetActive(true);
                    }
                }
            }
        }

        public override void Dispose()
        {
            _data = null;
        }
    }
}
