﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using MogoEngine.Events;
using UnityEngine;
using UnityEngine.UI;
using GameLoader.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Common.ServerConfig;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.ExtendComponent;
namespace ModuleMail
{
    public class MailFriendListView : KContainer
    {
        private const int DEFAULT_PAGE_INDEX = 0;

        private KContainer m_addFriendContainer;
        private KScrollView m_ScrollView;
        private KPageableList _friendList;
        private KButton m_addFriendButton;

        private int _currentItemCount = 6;
        private const int PER_PAGE_COUNT = 6;

        public KComponentEvent<KList, int> onSelectedFriendChanged = new KComponentEvent<KList, int>();

        protected override void Awake()
        {
            KContainer _container = this.gameObject.GetComponent<KContainer>();
            m_ScrollView = GetChildComponent<KScrollView>("ScrollView_playerFriends");
            _friendList = m_ScrollView.GetChildComponent<KPageableList>("mask/content");
            if (_container.GetChild("Container_addPal") != null)
            {
                m_addFriendContainer = _container.GetChildComponent<KContainer>("Container_addPal");
            }
            if (m_addFriendContainer != null)
            {
                m_addFriendButton = m_addFriendContainer.GetChildComponent<KButton>("Button_addPal");
            }

            this.InitMailFriendListView();
        }

        protected override void OnEnable()
        {
            AddEventListener();
            Refresh();
            SelectedIndex = MailFriendItem.SelectedIndex;
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        public int SelectedIndex
        {
            get
            {
                return MailFriendItem.SelectedIndex;
            }
            set
            {
                List<KList.KListItemBase> list = _friendList.ItemList;
                _friendList.SelectedIndex = -1;
                MailFriendItem.SelectedIndex = value;
                if( MailFriendItem.SelectedIndex >= 0 &&  MailFriendItem.SelectedIndex <= _friendList.ItemList.Count)
                {
                    _friendList.SelectedIndex = MailFriendItem.SelectedIndex;
                }
            }
        }

        private void AddEventListener()
        {
            if (m_addFriendButton != null)
            {
                m_addFriendButton.onClick.AddListener(OnAddFriendButtonClick);
            }
            m_ScrollView.ScrollRect.onRequestNext.AddListener(ShowNextPage);
            _friendList.onSelectedIndexChanged.AddListener(OnSelectedReceiverChanged);
            EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_LIST, OnRefreshFriendList);
        }

        private void RemoveEventListener()
        {
            if (m_addFriendButton != null)
            {
                m_addFriendButton.onClick.RemoveListener(OnAddFriendButtonClick);
            }
            m_ScrollView.ScrollRect.onRequestNext.RemoveListener(ShowNextPage);
            _friendList.onSelectedIndexChanged.RemoveListener(OnSelectedReceiverChanged);
            EventDispatcher.RemoveEventListener(FriendEvents.REFRESH_FRIEND_LIST, OnRefreshFriendList);
        }

        public void Refresh()
        {
            if (PlayerDataManager.Instance.FriendData.GetMyFriendCount() == 0)
            {
                FriendManager.Instance.SendGetFriendListMsg();
            }
            OnRefreshFriendList();
        }

        private void ShowNextPage(KScrollRect scrollRect)
        {
            _currentItemCount += PER_PAGE_COUNT;
            OnRefreshFriendList();
        }

        private void OnRefreshFriendList()
        {
            List<PbFriendAvatarInfo> friendListData = PlayerDataManager.Instance.FriendData.GetFriendInfoByDefault();
            if (friendListData.Count == 0)
            {
                if (m_addFriendContainer != null)
                {
                    m_addFriendContainer.gameObject.SetActive(true);
                    return;
                }
            }
            else
            {
                if (m_addFriendContainer != null)
                {
                    m_addFriendContainer.gameObject.SetActive(false);
                }
            }
            List<PbFriendAvatarInfo> showFriendList = GetShowFriendList();
            _friendList.SetDataList<MailFriendItem>(showFriendList);
            SelectedIndex = MailFriendItem.SelectedIndex;
        }

        private List<PbFriendAvatarInfo> GetShowFriendList()
        {
            List<PbFriendAvatarInfo> friendListData = PlayerDataManager.Instance.FriendData.GetFriendInfoByDefault();
            int friendListCount = friendListData.Count;
            int showFriendListCount = _currentItemCount > friendListCount ? friendListCount : _currentItemCount;

            return friendListData.GetRange(0, showFriendListCount);
        }

        private void OnSelectedReceiverChanged(KList list, int index)
        {
            onSelectedFriendChanged.Invoke(list, index);
        }

        private void OnAddFriendButtonClick()
        {
            EventDispatcher.TriggerEvent(MailEvents.MAIL_ADD_FRIEND);
        }

        private void InitMailFriendListView()
        {
            _friendList.SetPadding(0, -5, 0, -5);
            _friendList.SetGap(-15, 0);
            _friendList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);

            if (m_addFriendContainer != null)
            {
                m_addFriendContainer.gameObject.SetActive(false);
            }
        }
    }
}
