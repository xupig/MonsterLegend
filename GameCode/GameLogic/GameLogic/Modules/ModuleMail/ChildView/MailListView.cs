﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Common.Base;
using Common.Events;
using Common.Global;
using Common.Utils;
using Common.Structs;
using ModuleCommonUI;
using Game.UI.UIComponent;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using GameMain.GlobalManager;
using MogoEngine.Events;


namespace ModuleMail
{
    public class MailListView : KContainer
    {
        private KScrollView _scrollView;
        private KPageableList _mailList;
        private int _currentItemCount = 5;
        private int PER_PAGE_NUM = 5;

        public KComponentEvent<KList, int> onSelectedIndexChanged = new KComponentEvent<KList, int>();

        protected override void Awake()
        {
            base.Awake();
            _scrollView = GetChildComponent<KScrollView>("ScrollView_mailInfoList");
            _mailList = _scrollView.GetChildComponent<KPageableList>("mask/content");
            _mailList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _mailList.SetPadding(0, -3, 0, -3);
            _mailList.SetGap(-10, 0);

        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _scrollView.ScrollRect.onRequestNext.AddListener(OnRequestPage);
            _mailList.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
        }


        private void RemoveEventListener()
        {
            _scrollView.ScrollRect.onRequestNext.RemoveListener(OnRequestPage);
            _mailList.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
        }

        private void OnRequestPage(KScrollRect list)
        {
            _currentItemCount += PER_PAGE_NUM;

            List<PbMail> mailDataList = PlayerDataManager.Instance.MailData.MailDataList;
            int mailTotalNum = PlayerDataManager.Instance.MailData.MailTotalNum;
            if (mailTotalNum > mailDataList.Count)
            {
                MailManager.Instance.RequestMailList((uint)(_currentItemCount / PER_PAGE_NUM + 1));
            }
            else
            {
                ShowMailList(1);
            }
        }

        private void OnSelectedIndexChanged(KList list, int index)
        {
            onSelectedIndexChanged.Invoke(list, index);
            PbMail pbMail = list[index].Data as PbMail;
            MailManager.Instance.RequestReadMail(pbMail.id);
        }

        private void ShowMailList(int pageIndex)
        {
            List<PbMail> mailDataList = PlayerDataManager.Instance.MailData.MailDataList;
            int mailListCount = mailDataList.Count;
            int showMailListCount = _currentItemCount > mailListCount ? mailListCount : _currentItemCount;
            List<PbMail> showMailList = mailDataList.GetRange(0, showMailListCount);
            _mailList.SetDataList<MailItem>(showMailList);
        }

        public int SelectedIndex
        {
            get { return _mailList.SelectedIndex; }
            set { _mailList.SelectedIndex = value; }
        }

        public void ShowMailListPage(int pageIndex)
        {
            List<PbMail> mailDataList = PlayerDataManager.Instance.MailData.MailDataList;
            int mailTotalNum = PlayerDataManager.Instance.MailData.MailTotalNum;
            if (mailDataList.Count < 5 && mailTotalNum > mailDataList.Count)
            {
                MailManager.Instance.RequestMailList(1);
            }
            else
            {
                ShowMailList(pageIndex);
            }

        }

        public void RefreshSeletedItem(ulong mailId)
        {
            int seletedIndex = _mailList.SelectedIndex;
            _mailList[seletedIndex].Data = PlayerDataManager.Instance.MailData.GetMailById(mailId);
            (_mailList[seletedIndex] as MailItem).IsSelected = true;
        }

    }
}
