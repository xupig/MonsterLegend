﻿using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMail
{
    public class InboxAttachmentInfoView : KContainer
    {
        private KContainer _drawAllAttachment;
        private StateText _txtNoneAttachment;
        private StateText _txtUndraw;

        private KButton _drawAllAttachmentBtn;

        public KComponentEvent onDrawAllButtonClick = new KComponentEvent();

        protected override void Awake()
        {
 	        base.Awake();
            _drawAllAttachment = GetChildComponent<KContainer>("Container_quanbulingqu");
            _txtNoneAttachment = GetChildComponent<StateText>("Label_wufujiankelingqu");
            _drawAllAttachmentBtn = _drawAllAttachment.GetChildComponent<KButton>("Button_quanbulingqu");
            _txtUndraw = _drawAllAttachment.GetChildComponent<StateText>("Label_fujiantishilabel");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();

            TweenViewMove.Begin(this.gameObject, MoveType.Show, MoveDirection.Right);
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        public void RefreshAttachmentInfo()
        {
            uint unDrawAttachmentNum = PlayerDataManager.Instance.MailData.AttachmentNum;
            if (unDrawAttachmentNum == 0)
            {
                _drawAllAttachment.Visible = false;
                _txtNoneAttachment.Visible = true;
                _txtNoneAttachment.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.MAIL_TIP_NONE_ATTACHMENT);
            }
            else
            {
                _drawAllAttachment.Visible = true;
                _txtNoneAttachment.Visible = false;
                _txtUndraw.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.MAIL_TIP_NONE_DRAWATTACH, unDrawAttachmentNum);
            }
        }


        private void AddEventListener()
        {
            _drawAllAttachmentBtn.onClick.AddListener(OnDrawAllButtonClick);

        }

        private void RemoveEventListener()
        {
            _drawAllAttachmentBtn.onClick.RemoveListener(OnDrawAllButtonClick);

        }



        private void OnDrawAllButtonClick()
        {
            onDrawAllButtonClick.Invoke();
        }
    }
}
