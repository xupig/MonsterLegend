﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Structs.ProtoBuf;
using Common.Base;
using Game.UI.UIComponent;
using UnityEngine.UI;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager;
using Common.ExtendComponent;

namespace ModuleMail
{
    class MailAttachmentListView : KContainer
    {
        private KList m_list;

        protected override void Awake()
        {
            m_list = GetChildComponent<KList>("mask/content");

            this.InitScrollView();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

        }

        protected override void OnDisable()
        {
            m_list.RemoveAll();
            base.OnDisable();
        }

        public void AddItem(ulong mailId)
        {
            PbMail pbMail = PlayerDataManager.Instance.MailData.GetMailById(mailId);
            m_list.AddItem<AttachmentDrawItem>(pbMail);
        }

        private void InitScrollView()
        {
            m_list.SetPadding(0, 7, 0, 7);
            m_list.SetDirection(KList.KListDirection.LeftToRight,1,int.MaxValue);
            m_list.SetGap(3, 0);
        }

    }
}
