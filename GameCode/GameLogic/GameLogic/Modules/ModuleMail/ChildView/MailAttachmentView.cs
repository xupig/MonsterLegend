﻿using Common.Base;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.Enum;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMail
{
    public class MailAttachmentView : KContainer
    {
        private KScrollPage _scrollPage;
        private KList _attachmentKList;
        private KButton _drawBtn;

        private List<AttachmentItemData> _attachmentList = new List<AttachmentItemData>();

        private PbMail _data;

        protected override void Awake()
        {
            base.Awake();
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_fujian");
            _attachmentKList = GetChildComponent<KList>("ScrollPage_fujian/mask/content");
            _drawBtn = GetChildComponent<KButton>("Button_lingqufujian");

            _attachmentKList.SetPadding(0, 0, 0, 0);
            _attachmentKList.SetGap(5, 7);
            _attachmentKList.SetDirection(KList.KListDirection.LeftToRight, 5, 1);
            _attachmentKList.RemoveAll();
            _attachmentKList.SetDataList<AttachmentItemGrid>(_attachmentList);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        private void AddEventListener()
        {
            _drawBtn.onClick.AddListener(OnDrawBtnClick);
        }

        private void RemoveEventListener()
        {
            _drawBtn.onClick.RemoveListener(OnDrawBtnClick);
        }

        public void ShowAttachment(PbMail pbMail)
        {
            _data = pbMail;
            InitUI();
            FillAttachment();
        }

        private void InitUI()
        {
            _drawBtn.Visible = false;
            if (_data.get_state == public_config.MAIL_NOT_GET)
            {
                _drawBtn.Visible = true;
            }
        }

        private void OnDrawBtnClick()
        {
            //if (IsBagEnough(_data.attachment) == true)
            //{
            MailManager.Instance.RequestDrawAttachment(_data.id);
            //}
            //else
            //{
            //    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.BAG_NOT_ENOUGH), PanelIdEnum.Interactive);
            //}
        }

        private void FillAttachment()
        {
            ReOrganizetionData();
            _scrollPage.TotalPage = (_attachmentList.Count - 1) / 5 + 1;
            _attachmentKList.RemoveAll();
            _attachmentKList.SetDataList<AttachmentItemGrid>(_attachmentList);
        }

        private void ReOrganizetionData()
        {
            List<PbItemInfo> itemInfoList = _data.attachment;
            int count = itemInfoList.Count;

            _attachmentList.Clear();

            for (int i = 0; i < count; ++i)
            {
                AttachmentItemData attachmentData = new AttachmentItemData((int)itemInfoList[i].id);
                attachmentData.AttachmentState = (int)_data.get_state;
                attachmentData.BaseItemData.StackCount = (int)itemInfoList[i].count;
                _attachmentList.Add(attachmentData);
            }
        }

        //private bool IsBagEnough(List<PbItemInfo> attachmentList)
        //{
        //    Dictionary<int, BaseItemInfo> m_BagData;
        //    m_BagData = PlayerDataManager.Instance.BagData.GetItemBagData().GetAllItemInfo();
        //    uint curBagCount = PlayerAvatar.Player.cur_bag_count;
        //    uint needNewGridCount = 0;
        //    int count = attachmentList.Count;
        //    for (int i = 0; i < count; ++i)
        //    {
        //        if (attachmentList[i].id == SpecialItemType.GOLD
        //            || attachmentList[i].id == SpecialItemType.DIAMOND
        //            || attachmentList[i].id == SpecialItemType.EXP_PLAYER)
        //        {
        //            continue;
        //        }

        //        if (m_BagData.ContainsKey((int)attachmentList[i].id) == true)
        //        {
        //            if (m_BagData[(int)attachmentList[i].id].StackCount + attachmentList[i].count > item_helper.GetItemConfig((int)attachmentList[i].id).__stack)
        //            {
        //                needNewGridCount++;
        //            }
        //        }
        //        else
        //        {
        //            needNewGridCount++;
        //        }
        //    }

        //    if (m_BagData.Count + needNewGridCount > curBagCount)
        //    {
        //        return false;
        //    }
        //    return true;
        //}
    }
}
