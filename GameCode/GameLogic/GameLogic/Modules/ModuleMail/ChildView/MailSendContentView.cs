﻿using System;
using System.Collections.Generic;
using Common.Base;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using Common.Structs;
using UnityEngine.UI;
using GameMain.Entities;
using Common.ServerConfig;
using GameLoader.Utils.CustomType;
using ModuleCommonUI;
using Common.Utils;
using Common.Global;
using MogoEngine.Events;
using Common.Events;
using GameData;
using UnityEngine;
using Common.ExtendComponent;
using UnityEngine.EventSystems;

namespace ModuleMail
{
    public class MailSendContentView : KContainer
    {
        private StateText m_receiverName;
        private KButton m_addReceiverButton;
        private KButton _addAttachmentButton;
        private KButton m_sendlButton;
        private KDummyButton _hintBtn;
        private KInputField _mailInputContent;
        private StateText m_mailText;
        private KScrollPage _mailSelectedScrollPage;
        private KList _mailSelectedList;

        private PbFriendAvatarInfo m_currentReceiver;

        public KComponentEvent onAddReceiverBtnClick = new KComponentEvent();
        public KComponentEvent onAddAttachmentBtnClick = new KComponentEvent();

        protected override void Awake()
        {
            m_receiverName = GetChildComponent<StateText>("Label_receivername");
            m_addReceiverButton = GetChildComponent<KButton>("Button_addreceiver");
            _addAttachmentButton = GetChildComponent<KButton>("Button_tianjia");
            m_sendlButton = GetChildComponent<KButton>("Button_sendbtn");
            _mailInputContent = GetChildComponent<KInputField>("Input_content");
            _mailSelectedScrollPage = GetChildComponent<KScrollPage>("ScrollPage_fujian");
            _mailSelectedList = _mailSelectedScrollPage.GetChildComponent<KList>("mask/content");

            _hintBtn = AddChildComponent<KDummyButton>("Container_inputHint");
            m_mailText = GetChildComponent<StateText>("Container_inputHint/Label_label");
            InitSendContentView();
        }

        protected override void OnEnable()
        {
            AddEventListener();
            ResetMailSendContentView();
            EventDispatcher.TriggerEvent(MailEvents.TWEEN_RIGHT_BG);
            TweenViewMove.Begin(gameObject, MoveType.Show, Common.ExtendComponent.MoveDirection.Right);
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }
        private void AddEventListener()
        {
            m_sendlButton.onClick.AddListener(OnSendMailButtonClick);
            m_addReceiverButton.onClick.AddListener(OnAddReceiverButtonClick);
            _addAttachmentButton.onClick.AddListener(OnAddAttachmentButtonClick);
            _mailInputContent.onValueChange.AddListener(OnContentChange);
            _hintBtn.onClick.AddListener(OnHintClick);
        }

        private void RemoveEventListener()
        {
            m_sendlButton.onClick.RemoveListener(OnSendMailButtonClick);
            m_addReceiverButton.onClick.RemoveListener(OnAddReceiverButtonClick);
            _addAttachmentButton.onClick.RemoveListener(OnAddAttachmentButtonClick);
            _mailInputContent.onValueChange.RemoveListener(OnContentChange);
            _hintBtn.onClick.RemoveListener(OnHintClick);
        }

        public void AddReceiver(PbFriendAvatarInfo _friendInfo)
        {
            m_currentReceiver = _friendInfo;
            m_receiverName.ChangeAllStateText(_friendInfo.name);
            EventDispatcher.TriggerEvent(MailEvents.TWEEN_RIGHT_BG);
            TweenViewMove.Begin(gameObject, MoveType.Show, Common.ExtendComponent.MoveDirection.Right);
        }


        public void AddAttachmentToMail(List<AttachmentItemData> selectedAttachemnet)
        {
            _mailSelectedScrollPage.TotalPage = (selectedAttachemnet.Count - 1) / 5 + 1;
            _mailSelectedList.RemoveAll();
            _mailSelectedList.SetDataList<AttachmentItemGrid>(selectedAttachemnet);
        }

        private void OnAddAttachmentButtonClick()
        {
            onAddAttachmentBtnClick.Invoke();
        }

        private void OnHintClick()
        {
            _hintBtn.gameObject.SetActive(false);
            EventSystem.current.SetSelectedGameObject(_mailInputContent.gameObject);
            _mailInputContent.OnPointerClick(new PointerEventData(EventSystem.current));
        }

        private void OnAddReceiverButtonClick()
        {
            onAddReceiverBtnClick.Invoke();
        }

        private void OnContentChange(string _content)
        {
            if(_content.Length == _mailInputContent.characterLimit)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.MAIL_TIP_CONTENT_LIMIT), PanelIdEnum.Interactive);
                _mailInputContent.text = _content.Substring(0, _content.Length - 1);
            }
        }

        private void OnSendMailButtonClick()
        {
            uint mailDayCount = PlayerAvatar.Player.mail_daily_count;
            int mailDayLimit = int.Parse(GameData.XMLManager.global_params[49].__value);

            if (mailDayCount >= mailDayLimit)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.MAIL_TIP_SEND_DAY_MAIL_LIMIT), PanelIdEnum.Interactive);
                return;
            }
            if (_mailInputContent.text.Length == 0 && _mailSelectedList.ItemList.Count == 0)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.MAIL_TIP_EMPTY_MAIL, int.Parse(XMLManager.global_params[62].__value)), PanelIdEnum.Interactive);
                return;
            }
            if (_mailInputContent.text.Length < int.Parse(XMLManager.global_params[62].__value) && _mailSelectedList.ItemList.Count == 0)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(37718), PanelIdEnum.Interactive);
                return;
            }
            if (m_currentReceiver == null)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.MAIL_TIP_ADD_RECEIVER), PanelIdEnum.Interactive);
                return;
            }

            MailInfo _mailInfo = FillSendMailInfo();
            MailManager.Instance.RequestSendMail(_mailInfo);
            ResetMailSendContentView();
        }

        private MailInfo FillSendMailInfo()
        {
            MailInfo _mailInfo = new MailInfo();
            _mailInfo.title = MogoLanguageUtil.GetString(LangEnum.MAIL_TIP_COMEFROM,PlayerAvatar.Player.name);
            _mailInfo.avatar_dbid = m_currentReceiver.dbid;
            _mailInfo.text = ChatManager.Instance.FilterContent(_mailInputContent.text);
            _mailInfo.from = PlayerAvatar.Player.name;
            _mailInfo.mail_type = (uint)public_config.MAIL_TYPE_PLAYER;
            ulong timeStamp = (ulong)(Common.Global.Global.serverTimeStamp * 0.001);
            _mailInfo.time = (uint)timeStamp;

            LuaTable attachment = new LuaTable();
            int count = _mailSelectedList.ItemList.Count;
            for (int i = 0; i < count; i++)
            {
                AttachmentItemData _itemInfo = _mailSelectedList.ItemList[i].Data as AttachmentItemData;
                attachment.Add((int)_itemInfo.GridPosition, _itemInfo.BaseItemData.StackCount);
            }
            _mailInfo.attachment = attachment;
            return _mailInfo;
        }

        private void InitSendContentView()
        {
            _hintBtn.gameObject.SetActive(true);
            m_mailText.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.MAIL_TIP_CLICK_INPUT_TEXT);
            _mailInputContent.characterLimit = int.Parse(XMLManager.global_params[63].__value) + 1;
            _mailInputContent.lineType = InputField.LineType.MultiLineNewline;
            _mailSelectedList.SetDirection(KList.KListDirection.LeftToRight, 5, 1);
            _mailSelectedList.SetPadding(0, 0, 0, 0);
            _mailSelectedList.SetGap(0, 5);

            ///表示未开放发送附件功能
            if (int.Parse(global_params_helper.GetGlobalParam(123)) == 0)
            {
                GetChild("Container_fujianbiaoqian").gameObject.SetActive(false);
                _mailSelectedScrollPage.Visible = false;
                _addAttachmentButton.Visible = false;
            }

            ResetMailSendContentView();
        }

        private void ResetMailSendContentView()
        {
            _mailInputContent.text = "";
            m_currentReceiver = null;
            m_receiverName.CurrentText.text = "";
            _hintBtn.gameObject.SetActive(true);

            _mailSelectedList.RemoveAll();
            _mailSelectedScrollPage.TotalPage = 1;

            EventDispatcher.TriggerEvent(MailEvents.RESET_OUTBOX_VIEW);
        }
    }
}
