﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using MogoEngine.Utils;
using Common.Structs.ProtoBuf;
using Common.ServerConfig;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using Common.Events;
using GameMain.Entities;
using ModuleCommonUI;
using Common.Base;
using Common.Utils;
using Common.Global;
using GameData;
using Game.UI;
using UnityEngine;
using Common.Structs;
using GameMain.GlobalManager;

namespace ModuleMail
{
    public class MailContentView : KContainer
    {
        private KContainer _mailTitleInfoContainer;
        private KContainer _mailContentContainer;

        private StateText _senderNameTxt;
        private StateText _txtSendDate;
        private StateText _txtSendTime;
        private StateText _txtPlayerName;

        private KList _contentList;

        private KButton _deleteMailButton;
        private KButton _returnButton;

        private MailAttachmentView _attachmentView;

        private PbMail _pbMail;

        public KComponentEvent onReturnClick = new KComponentEvent();

        protected override void Awake()
        {
            _mailTitleInfoContainer = GetChildComponent<KContainer>("Container_mailtitleinfo");
            _mailContentContainer = GetChildComponent<KContainer>("Container_mailcontent");

            _senderNameTxt = _mailTitleInfoContainer.GetChildComponent<StateText>("Label_txtplayername");
            _txtSendDate = _mailTitleInfoContainer.GetChildComponent<StateText>("Label_txtdate");
            _txtSendTime = _mailTitleInfoContainer.GetChildComponent<StateText>("Label_txttime");
            _txtPlayerName = _mailContentContainer.GetChildComponent<StateText>("Label_txtplayername");

            _contentList = _mailContentContainer.GetChildComponent<KList>("ScrollView_chatView/mask/content");
            _deleteMailButton = GetChildComponent<KButton>("Button_deletefujian");
            _returnButton = GetChildComponent<KButton>("Button_return");


            _attachmentView = AddChildComponent<MailAttachmentView>("Container_fujian");

            _contentList.SetPadding(5, 30, 5, 30);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        private void AddEventListener()
        {
            _deleteMailButton.onClick.AddListener(OnDeleteButtonClick);
            _returnButton.onClick.AddListener(OnReturnBtnClick);
        }

        private void RemoveEventListener()
        {
            _deleteMailButton.onClick.RemoveListener(OnDeleteButtonClick);
            _returnButton.onClick.RemoveListener(OnReturnBtnClick);
        }

        public void ShowMailContent(ulong mailId)
        {
            _pbMail = PlayerDataManager.Instance.MailData.GetMailById(mailId);
            FillMailContent();
            UpdateAttachmentView();
        }

        private void OnDeleteButtonClick()
        {
            if (_pbMail.get_state == public_config.MAIL_NOT_GET)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(37704), PanelIdEnum.Interactive);
                return;
            }

            MailManager.Instance.RequestDeleleMail(_pbMail.id);
        }

        private void OnReturnBtnClick()
        {
            onReturnClick.Invoke();
        }

        private void UpdateAttachmentView()
        {
            if (_pbMail.get_state == public_config.MAIL_NONE)
            {
                _attachmentView.Visible = false;
                return;
            }

            _attachmentView.Visible = true;
            _attachmentView.ShowAttachment(_pbMail);
        }

        private void FillMailContent()
        {
            ShowSendTime();

            _txtPlayerName.CurrentText.text = PlayerAvatar.Player.name;

            if (_pbMail.mail_type == (uint)public_config.MAIL_TYPE_PLAYER)
            {
                ShowPrivateMail();
            }
            else
            {
                ShowSystemMail();
            }
        }

        private void ShowSendTime()
        {
            DateTime _dateTime = ParseTimestamp(_pbMail.time);

            _txtSendDate.CurrentText.text = _dateTime.ToString("yyyy-MM-dd");
            _txtSendTime.CurrentText.text = _dateTime.ToString("HH:mm:ss");
        }

        private void ShowPrivateMail()
        {
            _senderNameTxt.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_pbMail.from);
            _contentList.RemoveAll();
            string content = MogoProtoUtils.ParseByteArrToString(_pbMail.text);
            if (content != null)
            {
                _contentList.AddItem<MailContentItem>(content);
            }
        }

        private void ShowSystemMail()
        {
            string textParam = MogoProtoUtils.ParseByteArrToString(_pbMail.text);
            object[] mailParamList = ParseMailParam(textParam);
            _senderNameTxt.CurrentText.text = sys_mail_helper.GetSysMailFrom((int)_pbMail.mail_type);
            string content = sys_mail_helper.GetSysMailContent((int)_pbMail.mail_type, mailParamList);
            if(content == null)
            {
                content = string.Empty;
            }
            else
            {
                content = MogoStringUtils.ConvertStyle(content.Replace("\\n", "\n"));
            }
            _contentList.RemoveAll();
            if (content != null)
            {
                _contentList.AddItem<MailContentItem>(content);
            }
        }

        private DateTime ParseTimestamp(long time)
        {
            DateTime _dateTime = DateTime.MinValue;
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            _dateTime = startTime.AddSeconds(time);

            return _dateTime;
        }

        private object[] ParseMailParam(string str)
        {
            string[] itemList = str.Split(',');
            object[] param = new object[itemList.Length];
            for (int i = 0; i < itemList.Length; i++)
            {
                param[i] = itemList[i];
            }
            return param;
        }
    }
}
