﻿
using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.Enum;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.CustomType;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMail
{
    public class MailInboxView:KContainer
    {
        private KContainer _leftNewInboxNotice;

        private KButton _delAllReadMailBtn;

        private StateText _mailNumLabel;
        private StateText _noneMailTip;
        private StateText _leftNewMailNumTxt;

        private InboxAttachmentInfoView _attachmentInfoView;
        private MailAttachmentListView _attachmentListView;
        private MailContentView _mailContentView;
        private MailListView _mailListView;

        protected override void Awake()
        {
            KContainer _parentContainer = transform.parent.gameObject.GetComponent<KContainer>();
            _leftNewInboxNotice = _parentContainer.GetChildComponent<KContainer>("Container_mailtoggle/Container_newInboxNotice");
            _leftNewMailNumTxt = _leftNewInboxNotice.GetChildComponent<StateText>("Label_NoticeCount");
            _delAllReadMailBtn = GetChildComponent<KButton>("Container_left/Button_deletealreadyread");
            _noneMailTip = GetChildComponent<StateText>("Container_left/Label_txtNoneMailTip");
            _noneMailTip.Visible = false;
            _mailNumLabel = GetChildComponent<StateText>("Container_left/Label_capacity");


            _mailListView = AddChildComponent<MailListView>("Container_left");
            _mailContentView = AddChildComponent<MailContentView>("Container_rightreceiveUI");
            _attachmentListView = AddChildComponent<MailAttachmentListView>("ScrollView_youjianliebiao");
            _attachmentInfoView = AddChildComponent<InboxAttachmentInfoView>("Container_rightdefault");

            InitScrollView();
        }

        protected override void OnEnable()
        {
            Refresh();
            AddEventListener();
            EventDispatcher.TriggerEvent(MailEvents.TWEEN_RIGHT_BG);
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }
        private void AddEventListener()
        {
            _mailListView.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            _delAllReadMailBtn.onClick.AddListener(OnDeleteAllReadMail);
            _attachmentInfoView.onDrawAllButtonClick.AddListener(OnDrawAllAttachment);
            _mailContentView.onReturnClick.AddListener(OnReturnBtnClick);

            //EventDispatcher.AddEventListener(MailEvents.DRAW_MAIL_ATTACHMENT, RefreshUnDrawLabel);
            EventDispatcher.AddEventListener<ulong>(MailEvents.DISPLAY_MAIL_CONTENT, OnDisplayMailContent);
            EventDispatcher.AddEventListener(MailEvents.DELETE_MAIL, Refresh);
            EventDispatcher.AddEventListener<ulong>(MailEvents.DRAW_MAIL_ATTACHMENT, RefreshInboxView);
            EventDispatcher.AddEventListener(MailEvents.BATCH_DELETE_MAIL, OnBatchDelete);


        }

        private void RemoveEventListener()
        {
            _mailListView.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            _delAllReadMailBtn.onClick.RemoveListener(OnDeleteAllReadMail);
            _attachmentInfoView.onDrawAllButtonClick.RemoveListener(OnDrawAllAttachment);
            _mailContentView.onReturnClick.RemoveListener(OnReturnBtnClick);

            //EventDispatcher.RemoveEventListener(MailEvents.DRAW_MAIL_ATTACHMENT, RefreshUnDrawLabel);
            EventDispatcher.RemoveEventListener<ulong>(MailEvents.DISPLAY_MAIL_CONTENT, OnDisplayMailContent);
            EventDispatcher.RemoveEventListener(MailEvents.DELETE_MAIL, Refresh);
            EventDispatcher.RemoveEventListener<ulong>(MailEvents.DRAW_MAIL_ATTACHMENT, RefreshInboxView);
            EventDispatcher.RemoveEventListener(MailEvents.BATCH_DELETE_MAIL, OnBatchDelete);


        }

        private void OnBatchDelete()
        {
            RefreshPage(1);
            EventDispatcher.TriggerEvent(MailEvents.TWEEN_LEFT_BG);
        }

        public void RefreshPage(int pageIndex)
        {
            _mailContentView.Visible = false;
            _leftNewInboxNotice.Visible = false;
            _noneMailTip.Visible = false;
            _attachmentListView.Visible = false;
            _attachmentInfoView.Visible = false;

            _mailListView.ShowMailListPage(pageIndex);
            RefreshUnDrawLabel();
            RefreshMailCapacity();
            RefreshUnReadMailNum();
        }

        public void Refresh()
        {
            _mailListView.SelectedIndex = -1;
            RefreshPage(1);
        }

        public void RefreshUnReadMailNum()
        {
            uint unReadMailNum = PlayerDataManager.Instance.MailData.NewMailCount;

            if (0 == unReadMailNum)
            {
                _leftNewInboxNotice.Visible = false;
            }
            else
            {
                _leftNewInboxNotice.Visible = true;
                _leftNewMailNumTxt.CurrentText.text = unReadMailNum.ToString();
            }
        }

        private void RefreshInboxView(ulong mailId)
        {
            if (_mailContentView.Visible == false)
            {
                _attachmentInfoView.Visible = false;
                _attachmentListView.Visible = true;
                _attachmentListView.AddItem(mailId);
                RefreshUnReadMailNum();
                _mailListView.ShowMailListPage(1);
                TweenViewMove.Begin(_attachmentListView.gameObject, MoveType.Show, MoveDirection.Right);
                EventDispatcher.TriggerEvent(MailEvents.TWEEN_RIGHT_BG);
            }
            else
            {
                _mailContentView.ShowMailContent(mailId);
            }
        }

        private void OnReturnBtnClick()
        {
            _mailContentView.Visible = false;

            RefreshUnDrawLabel();
            _mailListView.SelectedIndex = -1;

        }

        private void OnDisplayMailContent(ulong mailId)
        {
            if (_mailContentView.Visible == true)
            {
                _mailContentView.ShowMailContent(mailId);
                _attachmentListView.Visible = false;
            }
            _mailListView.RefreshSeletedItem(mailId);
            RefreshUnReadMailNum();

            TweenViewMove.Begin(_mailContentView.gameObject, MoveType.Show, MoveDirection.Right);
            EventDispatcher.TriggerEvent(MailEvents.TWEEN_RIGHT_BG);
        }

        private void OnSelectedIndexChanged(KList list, int index)
        {
            _attachmentInfoView.Visible = false;
            _mailContentView.Visible = true;
        }

        private void OnDeleteAllReadMail()
        {
            string content = "\n                 " + MogoLanguageUtil.GetString(LangEnum.MAIL_TIP_DELETE_MAIL);
            MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, OnConfirmDelete, OnCancel);
        }

        private void OnDrawAllAttachment()
        {
            MailManager.Instance.RequestBatchDrawAttachment();
        }

        private void InitScrollView()
        {
            _mailContentView.Visible = false;
            _leftNewInboxNotice.gameObject.SetActive(false);
            _noneMailTip.Visible = false;
            _attachmentListView.gameObject.SetActive(false);

            _noneMailTip.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.MAIL_TIP_NONE_MAIL);
            _noneMailTip.CurrentText.alignment = TextAnchor.MiddleCenter;
            _leftNewMailNumTxt.CurrentText.alignment = TextAnchor.UpperCenter;
        }

        private void OnCancel()
        {

        }

        private void OnConfirmDelete()
        {
            LuaTable deleteMailList = new LuaTable();
            int i = 0;
            List<PbMail> mailDataList = PlayerDataManager.Instance.MailData.MailDataList;
            int count = mailDataList.Count;
            for (int j = 0; j < count; j++)
            {
                if (mailDataList[j].read_state == public_config.MAIL_READ
                    && (mailDataList[j].get_state == public_config.MAIL_NONE
                    || mailDataList[j].get_state == public_config.MAIL_GET))
                {
                    deleteMailList.Add(++i, mailDataList[j].id);
                }
            }
            if (deleteMailList.Count == 0)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(37723), PanelIdEnum.Interactive);
            }
            else
            {
                MailManager.Instance.RequestDeleteList(deleteMailList);
            }
        }

        private void RefreshUnDrawLabel()
        {
            _attachmentInfoView.Visible = true;
            _attachmentInfoView.RefreshAttachmentInfo();

            if (0 == PlayerDataManager.Instance.MailData.MailTotalNum)
            {
                _noneMailTip.Visible = true;
            }
            else
            {
                _noneMailTip.Visible = false;
            }
        }

        private void RefreshMailCapacity()
        {
            int _mailCount = PlayerDataManager.Instance.MailData.MailTotalNum;

            int _mailCapacity = PlayerDataManager.Instance.MailData.MailMaxCount;
            _mailNumLabel.CurrentText.text = _mailCount + "/" + _mailCapacity;
        }

        private bool IsBagEnough(List<PbItemInfo> list)
        {
            Dictionary<int, BaseItemInfo> m_BagData;
            m_BagData = PlayerDataManager.Instance.BagData.GetItemBagData().GetAllItemInfo();
            uint curBagCount = PlayerAvatar.Player.cur_bag_count;
            uint needNewGridCount = 0;
            foreach (PbItemInfo _item in list)
            {
                if (_item.id == SpecialItemType.GOLD
                    || _item.id == SpecialItemType.DIAMOND
                    || _item.id == SpecialItemType.EXP_PLAYER)
                {
                    continue;
                }
                if (m_BagData.ContainsKey((int)_item.id) == true)
                {
                    if (m_BagData[(int)_item.id].StackCount + _item.count > item_helper.GetItemConfig((int)_item.id).__stack)
                    {
                        needNewGridCount++;
                    }
                }
                else
                {
                    needNewGridCount++;
                }
            }
            if (m_BagData.Count + needNewGridCount > curBagCount)
            {
                return false;
            }
            return true;
        }
    }
}
