﻿using System.Collections.Generic;
using System.Linq;
using Game.UI.UIComponent;
using Common.Structs;
using GameMain.GlobalManager;
using UnityEngine;

namespace ModuleMail
{
    public class MailSelectAttachmentView : KContainer
    {
        private KButton _returnButton;
        private KList _unselectedKList;
        private KList _selectedKList;

        private KScrollView _unselectedScrollView;
        private KScrollView _selectedScrollView;

        private Dictionary<int, AttachmentItemData> _selectedAttachmentDic;
        private Dictionary<int, AttachmentItemData> _unselectedAttachmentDic;
        private KButton _addAttachmentButton;

        public KComponentEvent<List<AttachmentItemData>> onAddAttachmentBtnClick = new KComponentEvent<List<AttachmentItemData>>();

        protected override void Awake()
        {
            _returnButton = GetChildComponent<KButton>("Button_return");

            _unselectedScrollView = GetChildComponent<KScrollView>("ScrollView_fujian");
            _selectedScrollView = GetChildComponent<KScrollView>("ScrollView_wupin");

            _unselectedKList = GetChildComponent<KList>("ScrollView_fujian/mask/content");
            _selectedKList = GetChildComponent<KList>("ScrollView_wupin/mask/content");
            _addAttachmentButton = GetChildComponent<KButton>("Button_addfujian");

            InitView();
        }

        protected override void OnEnable()
        {
            ReSetSelectedItem();
            RefreshUnselItemData();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _returnButton.onClick.AddListener(OnReturnButtonClick);
            _addAttachmentButton.onClick.AddListener(OnAddAttachmentButtonClick);
            _unselectedKList.onSelectedIndexChanged.AddListener(OnClickAddItem);
            _selectedKList.onSelectedIndexChanged.AddListener(OnClickDelItem);
        }


        private void RemoveEventListener()
        {
            _returnButton.onClick.RemoveListener(OnReturnButtonClick);
            _addAttachmentButton.onClick.RemoveListener(OnAddAttachmentButtonClick);
            _unselectedKList.onSelectedIndexChanged.RemoveListener(OnClickAddItem);
            _selectedKList.onSelectedIndexChanged.RemoveListener(OnClickDelItem);
        }


        private void OnReturnButtonClick()
        {
            this.Visible = false;
        }

        private void OnAddAttachmentButtonClick()
        {
            onAddAttachmentBtnClick.Invoke(_selectedAttachmentDic.Values.ToList());
        }

        private void OnClickDelItem(KList list, int index)
        {
            PlusOneUnselectedItem(list, index);
            MinusOneSelectedItem(list, index);

            if (_selectedKList.ItemList.Count <= 1 * 3)
            {
                _selectedScrollView.ResetContentPosition();
                _selectedScrollView.UpdateArrow();
            }
            _selectedKList.SelectedIndex = -1;
        }

        private void OnClickAddItem(KList list, int index)
        {
            PlusOneSelectedItem(list, index);
            MinusOneUnselectedItem(list, index);

            if(_unselectedKList.ItemList.Count <= 4 * 4)
            {
                _unselectedScrollView.ResetContentPosition();
                _unselectedScrollView.UpdateArrow();
            }

            _unselectedKList.SelectedIndex = -1;
        }

        private void PlusOneUnselectedItem(KList list, int index)
        {
            AttachmentItemData itemData = _selectedKList.ItemList[index].Data as AttachmentItemData;
            if (_unselectedAttachmentDic.ContainsKey(itemData.GridPosition) == false)
            {
                AttachmentItemData unselectedItemData = new AttachmentItemData(itemData.BaseItemData.Id);
                unselectedItemData.BaseItemData.StackCount = 1;
                unselectedItemData.GridPosition = itemData.GridPosition;
                unselectedItemData.IsNotShowTip = true;
                _unselectedAttachmentDic[itemData.GridPosition] = unselectedItemData;
                _unselectedKList.AddItem<AttachmentItemGrid>(unselectedItemData, true);
            }
            else
            {
                _unselectedAttachmentDic[itemData.GridPosition].BaseItemData.StackCount++;

                int count = _unselectedKList.ItemList.Count;
                for (int i = 0; i < count; i++)
                {
                    (_unselectedKList[i] as AttachmentItemGrid).Refresh();
                }
            }
        }

        private void PlusOneSelectedItem(KList list, int index)
        {
            AttachmentItemData itemData = _unselectedKList[index].Data as AttachmentItemData;
            if (_selectedAttachmentDic.ContainsKey(itemData.GridPosition) == false)
            {

                AttachmentItemData selectedItemData = new AttachmentItemData(itemData.BaseItemData.Id);
                selectedItemData.BaseItemData.StackCount = 1;
                selectedItemData.GridPosition = itemData.GridPosition;
                selectedItemData.IsNotShowTip = true;
                _selectedAttachmentDic[itemData.GridPosition] = selectedItemData;
                _selectedKList.AddItem<AttachmentSelectionItem>(selectedItemData, true);
            }
            else
            {
                _selectedAttachmentDic[itemData.GridPosition].BaseItemData.StackCount++;
                int count = _selectedKList.ItemList.Count;
                for (int i = 0; i < count; i++)
                {
                    (_selectedKList[i] as AttachmentSelectionItem).Refresh();
                }
            }
        }

        private void MinusOneUnselectedItem(KList list, int index)
        {
            AttachmentItemData itemData = _unselectedKList[index].Data as AttachmentItemData;
            itemData.BaseItemData.StackCount--;
            if (itemData.BaseItemData.StackCount <= 0)
            {
                _unselectedKList.RemoveItemAt(index);
                _unselectedAttachmentDic.Remove(itemData.GridPosition);
            }
            else
            {
                _unselectedKList[index].Data = itemData;
            }
        }

        private void MinusOneSelectedItem(KList list, int index)
        {
            AttachmentItemData itemData = _selectedKList.ItemList[index].Data as AttachmentItemData;
            itemData.BaseItemData.StackCount--;
            if (itemData.BaseItemData.StackCount <= 0)
            {
                _selectedKList.RemoveItemAt(index);
                _selectedAttachmentDic.Remove(itemData.GridPosition);
            }
            else
            {
                _selectedKList.ItemList[index].Data = itemData;
            }
        }

        private void ReSetSelectedItem()
        {
            _selectedAttachmentDic.Clear();
            _selectedKList.RemoveAll();
        }

        private void RefreshUnselItemData()
        {
            Dictionary<int, BaseItemInfo> bagData;
            bagData = PlayerDataManager.Instance.BagData.GetItemBagData().GetAllItemInfo();

            foreach (KeyValuePair<int, BaseItemInfo> kvp in bagData)
            {
                if (kvp.Value.IsBind == false)
                {
                    AttachmentItemData itemData = new AttachmentItemData(kvp.Value.Id);
                    itemData.GridPosition = kvp.Key;
                    itemData.BaseItemData.StackCount = kvp.Value.StackCount;
                    itemData.IsNotShowTip = true;
                    _unselectedAttachmentDic[kvp.Key] = itemData;
                }
            }

            _unselectedKList.RemoveAll();
            _unselectedKList.SetDataList<AttachmentItemGrid>(_unselectedAttachmentDic.Values.ToList());
        }

        private void InitView()
        {
            _unselectedKList.SetDirection(KList.KListDirection.LeftToRight, 4);
            _unselectedKList.SetPadding(10, 0, 10, 0);
            _unselectedKList.SetGap(20, 25);

            _selectedKList.SetDirection(KList.KListDirection.LeftToRight, 1);
            _selectedKList.SetPadding(10, 0, 10, 0);
            _selectedKList.SetGap(0, 0);

            _selectedAttachmentDic = new Dictionary<int, AttachmentItemData>();
            _unselectedAttachmentDic = new Dictionary<int, AttachmentItemData>();
        }
    }
}
