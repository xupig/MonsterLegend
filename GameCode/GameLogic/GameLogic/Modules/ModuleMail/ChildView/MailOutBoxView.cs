﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using Common.Structs.ProtoBuf;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;

namespace ModuleMail
{
    class MailOutboxView : KContainer
    {
        private MailFriendListView _friendListView;
        private MailSendContentView _sendContentView;
        private MailFriendPopupView _friendPopupView;
        private MailSelectAttachmentView _selectAttachmentView;

        protected override void Awake()
        {
            _sendContentView = AddChildComponent<MailSendContentView>("Container_rightsendUI");
            _friendListView = AddChildComponent<MailFriendListView>("Container_playerFriends");
            _friendPopupView = AddChildComponent<MailFriendPopupView>("Container_friendlistPopupPanel");
            _selectAttachmentView = AddChildComponent<MailSelectAttachmentView>("Container_selectedfujian");

            InitOutboxView();
        }

        protected override void OnEnable()
        {
            AddEventsListener();
        }

        protected override void OnDisable()
        {
            RemoveEventsListener();
        }

        private void AddEventsListener()
        {
            _sendContentView.onAddAttachmentBtnClick.AddListener(OnShowSelectView);
            _sendContentView.onAddReceiverBtnClick.AddListener(OnShowFriendPopupView);
            _friendListView.onSelectedFriendChanged.AddListener(OnAddReceiverToMail);
            _friendPopupView.onSelectedFriendChanged.AddListener(OnAddReceiverToMail);
            _selectAttachmentView.onAddAttachmentBtnClick.AddListener(OnAddAttachmentToMail);

            EventDispatcher.AddEventListener(MailEvents.RESET_OUTBOX_VIEW, ResetOutboxView);
            EventDispatcher.AddEventListener(MailEvents.MAIL_CLOSE_POPUP_VIEW, ClosePopupView);
        }

        private void RemoveEventsListener()
        {
            _sendContentView.onAddAttachmentBtnClick.RemoveListener(OnShowSelectView);
            _sendContentView.onAddReceiverBtnClick.RemoveListener(OnShowFriendPopupView);
            _friendListView.onSelectedFriendChanged.RemoveListener(OnAddReceiverToMail);
            _friendPopupView.onSelectedFriendChanged.RemoveListener(OnAddReceiverToMail);
            _selectAttachmentView.onAddAttachmentBtnClick.RemoveListener(OnAddAttachmentToMail);

            EventDispatcher.RemoveEventListener(MailEvents.RESET_OUTBOX_VIEW, ResetOutboxView);
            EventDispatcher.RemoveEventListener(MailEvents.MAIL_CLOSE_POPUP_VIEW, ClosePopupView);
        }

        private void ResetOutboxView()
        {
            MailFriendItem.SelectedIndex = 0;
			if(_friendPopupView.Visible == true)
			{
            	_friendPopupView.SelectedIndex = -1;
			}
            _friendListView.SelectedIndex = -1;
        }

        private void OnAddAttachmentToMail(List<AttachmentItemData> selectedAttachemnet)
        {
            _selectAttachmentView.Visible = false;
            _sendContentView.AddAttachmentToMail(selectedAttachemnet);
        }

        private void OnAddReceiverToMail(KList list, int index)
        {
            _friendListView.SelectedIndex = index;
            _sendContentView.AddReceiver(list[index].Data as PbFriendAvatarInfo);
        }

        private void ClosePopupView()
        {
            _friendPopupView.Visible = false;
        }

        private void OnShowSelectView()
        {
            _selectAttachmentView.Visible = true;
        }

        private void OnShowFriendPopupView()
        {
            _friendPopupView.Visible = true;
        }

        private void InitOutboxView()
        {
            _selectAttachmentView.Visible = false;
            _friendPopupView.Visible = false;
        }
    }
}
