﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Structs.ProtoBuf;
using Common.Events;
using UnityEngine;
using GameMain.GlobalManager;

namespace ModuleMail
{
    public class MailFriendPopupView : KContainer
    {

        private MailFriendListView _friendListView;
        private KButton _closeButton;
        private StateText _noFriendTipsTxt;

        public KComponentEvent<KList, int> onSelectedFriendChanged = new KComponentEvent<KList, int>();

        protected override void Awake()
        {
            _closeButton = GetChildComponent<KButton>("Container_friendlistpanelBg/Button_close");
            _noFriendTipsTxt = GetChildComponent<StateText>("Label_txtNoneFriendTip");
            _friendListView = AddChildComponent<MailFriendListView>("Container_playerFriends");
        }

        public int SelectedIndex
        {
            get
            {
                return _friendListView.SelectedIndex;
            }
            set
            {
                _friendListView.SelectedIndex = value;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            OnRefreshContent();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        protected void AddEventListener()
        {
            _closeButton.onClick.AddListener(OnCloseButtonClick);
            _friendListView.onSelectedFriendChanged.AddListener(OnSelectedFriendChanged);
            EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_LIST, OnRefreshContent);
        }

        protected void RemoveEventListener()
        {
            _closeButton.onClick.RemoveListener(OnCloseButtonClick);
            _friendListView.onSelectedFriendChanged.RemoveListener(OnSelectedFriendChanged);
            EventDispatcher.RemoveEventListener(FriendEvents.REFRESH_FRIEND_LIST, OnRefreshContent);
        }

        private void OnRefreshContent()
        {
            List<PbFriendAvatarInfo> friendListData = PlayerDataManager.Instance.FriendData.GetFriendInfoByDefault();
            DisableNoFriendTip(friendListData.Count == 0);
        }

        private void OnSelectedFriendChanged(KList list, int index)
        {
            onSelectedFriendChanged.Invoke(list, index);
        }

        public void DisableNoFriendTip(bool isVisible)
        {
            _noFriendTipsTxt.Visible = isVisible;
        }

        private void OnCloseButtonClick()
        {
            this.Visible = false;
        }
    }
}
