﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;

namespace ModuleMail
{
    class MailModule : BaseModule
    {
        public MailModule()
            : base()
        {
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init( )
        {
            AddPanel(PanelIdEnum.Mail, "Container_InteractiveEmailPanel", MogoUILayer.LayerUIPanel, "ModuleMail.MailPanel", BlurUnderlay.Have, HideMainUI.Hide);
            RegisterPanelModule(PanelIdEnum.Mail, PanelIdEnum.Interactive);
        }

    }
}
