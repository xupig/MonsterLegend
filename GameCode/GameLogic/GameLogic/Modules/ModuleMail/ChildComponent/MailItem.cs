﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using Common.Structs.ProtoBuf;
using Common.ServerConfig;
using MogoEngine;
using MogoEngine.Utils;
using Common.Structs;
using GameMain.GlobalManager;
using GameMain.Entities;
using UnityEngine;
using GameData;
using Common.Utils;

namespace ModuleMail
{
    public class MailItem : KList.KListItemBase
    {
        private StateText _mailTitle;
        private StateText _senderNameTxt;
        private StateText _expireTimeTxt;

        private KContainer _readAttachmentIcon;
        private KContainer _unreadAttachmentIcon;
        private KContainer _readMailIcon;
        private KContainer _unreadMailIcon;

        private KDummyButton _dummyButton;

        private KContainer _checkmark;
       // private KToggle _toggle;
        private PbMail _data;

        private bool _isInteractive = true;
        private const int PERMANENT = 0;

        protected override void Awake()
        {
            KContainer _container = this.GetChildComponent<KContainer>("Container_mailInfo");
            _dummyButton = _container.gameObject.AddComponent<KDummyButton>();
            _checkmark = GetChildComponent<KContainer>("Container_selected");
            
            _mailTitle = _container.GetChildComponent<StateText>("Label_mailtitle");
            _senderNameTxt = _container.GetChildComponent<StateText>("Label_sendername");
            _expireTimeTxt = _container.GetChildComponent<StateText>("Label_deadline");
            
            _readAttachmentIcon = _container.GetChildComponent<KContainer>("Container_youjianfujiankai");
            _unreadAttachmentIcon = _container.GetChildComponent<KContainer>("Container_youjianfujian");
            _readMailIcon = _container.GetChildComponent<KContainer>("Container_youjiankai");
            _unreadMailIcon = _container.GetChildComponent<KContainer>("Container_youjian");
            
            AddListener();
        }

        public override void Dispose()
        {
            RemoveListener();
            _data = null;
        }

        private void AddListener()
        {
            _dummyButton.onClick.AddListener(OnItemClick);
        }

        private void RemoveListener()
        {
            _dummyButton.onClick.RemoveListener(OnItemClick);
        }

        private void OnItemClick()
        {
            if (_isInteractive)
            {
                onClick.Invoke(this, Index);
            }
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as PbMail;
                    _checkmark.Visible = false;
                    Refresh();
                }
            }
        }

        public override bool IsSelected
        {
            get
            {
                return _checkmark.Visible;
            }
            set
            {
                _checkmark.Visible = value;
                _isInteractive = !value;
            }
        }

        public void Refresh()
        {
            if (_data == null) return;
            
            FillMailItemContent();
            
            ResetMailItemIcon();
            UpdateMailItemIcon();
        }

        private void UpdateMailItemIcon()
        {
            if(_data.get_state == public_config.MAIL_NONE
                && _data.read_state == public_config.MAIL_NOT_READ)
            {
                _unreadMailIcon.Visible = true;
            }
            else if(_data.get_state == public_config.MAIL_NONE
                && _data.read_state == public_config.MAIL_READ)
            {
                _readMailIcon.Visible = true;
            }
            else if(_data.get_state != public_config.MAIL_NONE
                && _data.read_state == public_config.MAIL_NOT_READ)
            {
                _unreadAttachmentIcon.Visible = true;
            }
            else if (_data.get_state != public_config.MAIL_NONE
                && _data.read_state == public_config.MAIL_READ)
            {
                _readAttachmentIcon.Visible = true;
            }
        }

        private void FillMailItemContent()
        {
            DateTime _dateTime = ParseTimestamp(_data.time);
            if (XMLManager.sys_mail.ContainsKey((int)_data.mail_type) == false)
            {
                _dateTime = _dateTime.AddSeconds(int.Parse(XMLManager.global_params[59].__value));
                _expireTimeTxt.CurrentText.text = MogoLanguageUtil.GetContent(37715, _dateTime.Year, _dateTime.Month, _dateTime.Day);
            }
            else
            {
                int effectiveTime = XMLManager.sys_mail[(int)_data.mail_type].__effective_time;
                _dateTime = _dateTime.AddDays(effectiveTime);
                if (effectiveTime == PERMANENT)
                {
                    _expireTimeTxt.CurrentText.text = MogoLanguageUtil.GetContent(37716);
                }
                else
                {
                    _expireTimeTxt.CurrentText.text = MogoLanguageUtil.GetContent(37715, _dateTime.Year, _dateTime.Month, _dateTime.Day);
                }
            }
            if (_data.mail_type == (uint)public_config.MAIL_TYPE_PLAYER)
            {
                _mailTitle.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.title);
                _senderNameTxt.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.from);

            }
            else
            {
                _mailTitle.CurrentText.text = sys_mail_helper.GetSysMailTitle((int)_data.mail_type);
                _senderNameTxt.CurrentText.text = sys_mail_helper.GetSysMailFrom((int)_data.mail_type);
            }
        }

        private void ResetMailItemIcon()
        {
            _readAttachmentIcon.Visible = false;
            _unreadAttachmentIcon.Visible = false;
            _readMailIcon.Visible = false;
            _unreadMailIcon.Visible = false;
        }

        private DateTime ParseTimestamp(long time)
        {
            DateTime _dateTime = DateTime.MinValue;
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            _dateTime = startTime.AddSeconds(time);

            return _dateTime;
        }
    }
}
