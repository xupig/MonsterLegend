﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Game.UI.UIComponent;
using Common.Structs;
using Common.Utils;
using GameData;
using MogoEngine.Events;
using Common.Events;
using UnityEngine.EventSystems;
using Common.Data;

namespace ModuleMail
{
    public class AttachmentSelectionItem : KList.KListItemBase
    {
        private StateText _itemName;
        private StateText _itemDescribe;
        private AttachmentItemGrid mailAttachmentInfoGrid;
        private AttachmentItemData _data;

        protected override void Awake()
        {
            _itemName = GetChildComponent<StateText>("Label_thingname");
            _itemDescribe = GetChildComponent<StateText>("Label_describe");

            mailAttachmentInfoGrid = this.gameObject.AddComponent<AttachmentItemGrid>();
        }

        public override void Dispose()
        {
            _data = null;
        }
        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as AttachmentItemData;
                    Refresh();
                }
            }
        }

        public void Refresh()
        {
            mailAttachmentInfoGrid.Data = _data;
            _itemName.ChangeAllStateText(_data.BaseItemData.Name);
            _itemDescribe.ChangeAllStateText(_data.BaseItemData.Description);
        }
    }
}
