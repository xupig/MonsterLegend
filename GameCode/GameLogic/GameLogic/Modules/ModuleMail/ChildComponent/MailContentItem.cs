﻿
using Common.Chat;
using Game.Asset;
using Game.UI.TextEngine;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace ModuleMail
{
    public class MailContentItem : KList.KListItemBase
    {


        //private float MAX_WIDTH = 500.0f;
        //public const string FONT_ASSET_KEY = "Font$MicrosoftYaHei.ttf.u";
        //private TextBlock _block = null;

        private StateText _txtContent;
        private string _content;

        protected override void Awake()
        {
            _txtContent = GetChildComponent<StateText>("Label_txtcontent");
            _txtContent.Visible = true;
        }

        public override void Dispose()
        {

        }

        public override object Data
        {
            get
            {
                return _content;
            }
            set
            {
                _content = value.ToString();
                //ShowMailContent(_content);
                RefreshContent();
            }
        }

        private void RefreshContent()
        {
            _txtContent.CurrentText.text = _content;
            RecalculateItemSize();
        }

        private void RecalculateItemSize()
        {
            _txtContent.RecalculateAllStateHeight();
            RecalculateSize();
        }


        //public void ShowMailContent(string content)
        //{
        //    RemoveTextBlock();
        //    string contents = "每年生日都很让人期待，因为生日当天可以收到很多很多\n祝福，还可以收到<color=#ff0000>礼物</color>，还可以当天我最大，还可以";
        //    AddTextBlock(contents);
        //    AdjustLayout();
        //}

        //private void AddTextBlock(string content)
        //{
        //    _block = CreateTextBlock(content);
        //    _block.GameObject.transform.SetParent(gameObject.transform);
        //    RectTransform rect = _block.GameObject.GetComponent<RectTransform>();
        //    rect.localScale = Vector3.one;
        //    rect.localPosition = Vector3.zero;
        //}

        //protected virtual void AdjustLayout()
        //{
        //    RectTransform rect = _block.GameObject.GetComponent<RectTransform>();

        //    float height = _block.Height + 40.0f;
        //    rect.anchoredPosition = new Vector2(20.0f, -6.9f);
        //    RecalculateSize();
        //}

        //private TextBlock CreateTextBlock(string content)
        //{
        //    FontData fontData = new FontData();
        //    fontData.alignment = TextAnchor.UpperLeft;
        //    Font font = ObjectPool.Instance.GetAssemblyObject(FONT_ASSET_KEY) as Font;
        //    fontData.font = font;
        //    fontData.fontSize = 22;
        //    fontData.fontStyle = FontStyle.Normal;
        //    fontData.lineSpacing = 1.0f;
        //    fontData.richText = true;

        //    TextBlock block = new TextBlock(content,new Regex(@"\s+"), MAX_WIDTH, fontData, Color.white);
        //    return block;
        //}


        //private void RemoveTextBlock()
        //{
        //    if (_block != null)
        //    {
        //        _block.GameObject.transform.SetParent(null);
        //        Object.Destroy(_block.GameObject);
        //        _block = null;
        //    }
        //}
    }
}
