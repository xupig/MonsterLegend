﻿using System;
using Game.UI.UIComponent;
using Common.Structs.ProtoBuf;
using Common.ExtendComponent;
using MogoEngine.Events;
using Common.Events;
using Common.Utils;
using GameData;
using Game.UI;
using GameMain.Entities;
using UnityEngine;

namespace ModuleMail
{
    public class MailFriendItem : KList.KListItemBase
    {
        public static int SelectedIndex = -1; //记录当前选中的
        private KProgressBar _progressBar;
        private StateText _txtProgress;
        private StateText _txtFightForce;
        private StateText _txtLevel;
        private StateText _txtVipLevel;
        private StateText _txtName;
        private KContainer _checkmark;
        private IconContainer _iconContaienr;
        private LoveStarItemList _loveStarItemList;

        private PbFriendAvatarInfo _friendAvatarInfo;

        protected override void Awake()
        {
            _loveStarItemList = gameObject.AddComponent<LoveStarItemList>();
            _txtProgress = GetChildComponent<StateText>("Label_txtJindu");
            _progressBar = GetChildComponent<KProgressBar>("ProgressBar_jindu");
            _txtFightForce = GetChildComponent<StateText>("Label_txtZhanli02");
            _txtLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            _txtVipLevel = GetChildComponent<StateText>("Label_txtDengji");
            _txtName = GetChildComponent<StateText>("Label_txtwanjiamingzi");
            _iconContaienr = AddChildComponent<IconContainer>("Container_icon");
            _checkmark = GetChildComponent<KContainer>("Container_selected");
            _checkmark.Visible = false;
        }

        public override void Show()
        {
            EventDispatcher.AddEventListener<UInt64>(FriendEvents.FRIEND_INTIMATE_CHANGE, OnIntimateChange);
            EventDispatcher.AddEventListener<UInt64>(FriendEvents.FRIEND_ONLINE_STATE_CHANGE, OnFriendOnlineStateChange);

        }

        public override void Hide()
        {
            EventDispatcher.RemoveEventListener<UInt64>(FriendEvents.FRIEND_INTIMATE_CHANGE, OnIntimateChange);
            EventDispatcher.RemoveEventListener<UInt64>(FriendEvents.FRIEND_ONLINE_STATE_CHANGE, OnFriendOnlineStateChange);
        }

        private void OnIntimateChange(UInt64 dbId)
        {
            if (_friendAvatarInfo != null && _friendAvatarInfo.dbid == dbId)
            {
                UpdateInitmate();
            }
        }

        public override void Dispose()
        {
            _friendAvatarInfo = null;
        }

        public override void OnPointerClick(UnityEngine.EventSystems.PointerEventData evtData)
        {
            base.OnPointerClick(evtData);
            onClick.Invoke(this, Index);
            EventDispatcher.TriggerEvent(MailEvents.MAIL_CLOSE_POPUP_VIEW);
        }

        public override object Data
        {
            get
            {
                return _friendAvatarInfo;
            }
            set
            {
                _friendAvatarInfo = value as PbFriendAvatarInfo;
                Refresh();
            }
        }

        public override bool IsSelected
        {
            get
            {
                return _checkmark.Visible;
            }
            set
            {
                _checkmark.Visible = value;
            }
        }

        private void Refresh()
        {
            if (_friendAvatarInfo != null)
            {
                _txtName.CurrentText.text = _friendAvatarInfo.name;
                _txtLevel.CurrentText.text = _friendAvatarInfo.level.ToString();
                _txtFightForce.CurrentText.text = _friendAvatarInfo.fight.ToString();
                _txtVipLevel.CurrentText.text = string.Empty;
                _iconContaienr.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_friendAvatarInfo.vocation), UpdateOnlineState);

                UpdateInitmate();
            }
        }

        private void OnFriendOnlineStateChange(UInt64 dbId)
        {
            if (_friendAvatarInfo != null && dbId == _friendAvatarInfo.dbid)
            {
                UpdateOnlineState();
            }
        }

        public void UpdateOnlineState()
        {
            if (_friendAvatarInfo.online == 1)
            {
                SetItemGray(1);
            }
            else
            {
                SetItemGray(0);
            }
        }

        private void UpdateOnlineState(GameObject go)
        {
            UpdateOnlineState();
        }

        private void UpdateInitmate()
        {
            float level = intimate_helper.GetIntimateLevel((int)_friendAvatarInfo.degree);
            int max = intimate_helper.GetIntimateLevelMax((int)_friendAvatarInfo.degree);
            _loveStarItemList.SetLevel(level);
            _progressBar.Value = (_friendAvatarInfo.degree) / (float)max;
            _txtProgress.CurrentText.text = string.Format("{0}/{1}", _friendAvatarInfo.degree, max);
        }

        private void SetItemGray(float gray)
        {
            ImageWrapper[] imgWrappers = _iconContaienr.GetComponentsInChildren<ImageWrapper>();
            for (int i = 0; i < imgWrappers.Length; i++)
            {
                imgWrappers[i].SetGray(gray);
            }
        }
    }
}
