﻿using System;
using System.Collections.Generic;

using MogoEngine.Utils;
using Game.UI.UIComponent;
using Common.Structs.ProtoBuf;
using Common.ServerConfig;
using GameData;
using Common.ExtendComponent;

namespace ModuleMail
{
    public class AttachmentDrawItem : KList.KListItemBase
    {
        private StateText _senderNameTxt;
        private StateText _sendDateTxt;
        private StateText _sendTimeTxt;
        private KList _attachmentList;
        private KScrollPage attachmentScrollPage;
        private PbMail _data;

        static readonly int[] s_Padding = new int[] { 0, 0, 0, 0 };
        static readonly int[] s_Gap = new int[] { 0, 12 };

        protected override void Awake()
        {
            _senderNameTxt = GetChildComponent<StateText>("Label_txtWanjiamingzi");
            _sendDateTxt = GetChildComponent<StateText>("Label_txtRiqi");
            _sendTimeTxt = GetChildComponent<StateText>("Label_txtShijian");
            attachmentScrollPage = GetChildComponent<KScrollPage>("ScrollPage_fujian");
            _attachmentList = attachmentScrollPage.GetChildComponent<KList>("mask/content");

            this.InitScrollPage();
        }

        public override void Dispose()
        {
            _data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as PbMail;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            FillMailContent();
            DisplayMailAttachment();
        }

        private void FillMailContent()
        {
            DateTime _dateTime = ParseTimeStamp(_data.time);

            if (_data.mail_type == (uint)public_config.MAIL_TYPE_PLAYER)
            {
                _senderNameTxt.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.from);
            }
            else
            {
                _senderNameTxt.CurrentText.text = sys_mail_helper.GetSysMailFrom((int)_data.mail_type);
            }
            _sendDateTxt.CurrentText.text = _dateTime.ToString("yyyy-MM-dd");
            _sendTimeTxt.CurrentText.text = _dateTime.ToString("HH:mm:ss");
        }

        private DateTime ParseTimeStamp(long time)
        {
            DateTime _dateTime = DateTime.MinValue;
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            _dateTime = startTime.AddSeconds(time);

            return _dateTime;
        }

        private void DisplayMailAttachment()
        {
            List<AttachmentItemData> attachmentList = new List<AttachmentItemData>();
            foreach (PbItemInfo pbItem in _data.attachment)
            {
                AttachmentItemData itemData = new AttachmentItemData((int)pbItem.id);
                itemData.BaseItemData.StackCount = (int)pbItem.count;
                attachmentList.Add(itemData);
            }
            attachmentScrollPage.TotalPage = (attachmentList.Count - 1) / 4 + 1;
            if (attachmentScrollPage.TotalPage <= 1)
            {
                AddChildComponent<RaycastComponent>("ScrollPage_fujian");
            }
            _attachmentList.RemoveAll();
            _attachmentList.SetDataList<AttachmentItemGrid>(attachmentList);
        }

        private void InitScrollPage()
        {
            _attachmentList.SetDirection(KList.KListDirection.LeftToRight, 5, 1);
            _attachmentList.SetPadding(s_Padding[0], s_Padding[1], s_Padding[2], s_Padding[3]);
            _attachmentList.SetGap(s_Gap[0], s_Gap[1]);
        }
    }
}
