﻿using Common.Data;
using Common.Structs;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMail
{
    public class AttachmentItemData
    {
        private BaseItemData _baseItemData;

        public AttachmentItemData(int id)
        {
            BagItemType itemType = item_helper.GetItemType(id);
             if (itemType == BagItemType.Equip)
            {
                _baseItemData = new EquipData(id);
            }
            else
            {
                _baseItemData = new ItemData(id);
            }
        }

        public BaseItemData BaseItemData
        {
            get
            {
                return _baseItemData;
            }
        }

        public int GridPosition
        {
            get;
            set;
        }

        public int AttachmentState
        {
            get;
            set;
        }

        public bool IsNotShowTip
        {
            get;
            set;
        }
    }
}
