﻿using Common.Base;
using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTradeMarket
{
    public class TradeWantBuyPanel :BasePanel
    {
        private KList _wantList;
        private StateText _noneItemToWant;

        protected override void Awake()
        {
            base.Awake();
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _wantList = GetChildComponent<KList>("ScrollView_youjiangoumai/mask/content");
            _noneItemToWant = GetChildComponent<StateText>("Label_txtWujilv");

            _wantList.SetDirection(KList.KListDirection.LeftToRight, 2, int.MaxValue);
            _wantList.SetGap(0, 11);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TradeWantBuy; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            TradeMarketManager.Instance.RequestWantToBuyList();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(TradeMarketEvents.UpdateWantBuyList, RefreshWantList);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(TradeMarketEvents.UpdateWantBuyList, RefreshWantList);
        }

        private void RefreshWantList()
        {
            List<TradeItemData> wantBuyList = PlayerDataManager.Instance.AuctionData.FilterAndFillWantItemInfo();
            _wantList.SetDataList<CanToBuyItem>(wantBuyList, 2);
            _noneItemToWant.Visible = wantBuyList.Count == 0;
        }
    }
}
