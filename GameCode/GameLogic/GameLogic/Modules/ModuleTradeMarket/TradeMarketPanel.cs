﻿using System;
using System.Collections.Generic;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager.SubSystem;
using UnityEngine;
using GameMain.GlobalManager;


namespace ModuleTradeMarket
{
    public class TradeMarketPanel : BasePanel
    {
        private const int BUY = 0;
        private const int SELL = 1;
        private const int RECORD = 2;

        private KToggleGroup _categoryToggleGroup; 
        private TradeMarketBuyView _buyView;       //购买中心主面板
        private TradeMarketSellView _sellView;     //寄售主面板
        private TradeMarketRecordView _recordView;
        
        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _categoryToggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_title");
            _buyView = AddChildComponent<TradeMarketBuyView>("Container_goumaixitong");
            _sellView = AddChildComponent<TradeMarketSellView>("Container_jishou");
            _recordView = AddChildComponent<TradeMarketRecordView>("Container_jiaoyijilu");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TradeMarket; }
        }

        public override void OnShow(object data)
        {
            SetData(data);
            AddEventListener();
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            if (data is int)
            {
                RefreshView((int)data);
                _categoryToggleGroup.SelectIndex = (int)data;
            }
            else
            {
                RefreshView(BUY);
                _categoryToggleGroup.SelectIndex = BUY;
            }

            RefreshPoint();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

        private void AddEventListener()
        {
            _categoryToggleGroup.onSelectedIndexChanged.AddListener(OnCategoryChanged);
            EventDispatcher.AddEventListener(TradeMarketEvents.NoticeCountChanged, RefreshPoint);
        }

        private void RemoveEventListener()
        {
            _categoryToggleGroup.onSelectedIndexChanged.RemoveListener(OnCategoryChanged);
            EventDispatcher.RemoveEventListener(TradeMarketEvents.NoticeCountChanged, RefreshPoint);   
        }

        private void OnCategoryChanged(KToggleGroup toggleGroup, int index)
        {
            RefreshView(index);
        }

        private void RefreshView(int index)
        {
            switch(index)
            {
                case BUY:
                    _buyView.Visible = true;
                    _sellView.Visible = false;
                    _recordView.Visible = false;
                    break;
                case SELL:
                    _sellView.Visible = true;
                    _buyView.Visible = false;
                    _recordView.Visible = false;
                    break;
                case RECORD:
                    _sellView.Visible = false;
                    _buyView.Visible = false;
                    _recordView.Visible = true;
                    break;
                default:
                    break;
            }
        }

        private void RefreshPoint()
        {
            _categoryToggleGroup[SELL].SetGreenPoint(PlayerDataManager.Instance.AuctionData.NoticeCount + PlayerDataManager.Instance.PutOutData.NoticeCount > 0);
            _categoryToggleGroup[RECORD].SetGreenPoint(false);
        }
    }
}
