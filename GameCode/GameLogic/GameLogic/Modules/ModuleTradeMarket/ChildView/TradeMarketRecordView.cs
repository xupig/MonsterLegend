﻿
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTradeMarket
{
    public class TradeMarketRecordView:KContainer
    {
        private const int BUY_RECORD = 0;
        private const int SELL_RECORD = 1;

        private StateText _txtNoneRecord;
        private CategoryList _categoryList;
        private KList _recordList;

        protected override void Awake()
        {
            base.Awake();
            _txtNoneRecord = GetChildComponent<StateText>("Label_txtEmpty");
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            _recordList = GetChildComponent<KList>("ScrollView_qinmizhi/mask/content");

            _recordList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            InitCategory();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            _categoryList.SelectedIndex = BUY_RECORD;
            TradeMarketManager.Instance.RequestBuyRecord();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        private void AddEventListener()
        {
            _categoryList.onSelectedIndexChanged.AddListener(OnSelectedRecordTypeChanged);
            EventDispatcher.AddEventListener(TradeMarketEvents.RefreshBuyRecord, RefreshBuyRecord);
            EventDispatcher.AddEventListener(TradeMarketEvents.RefreshSellRecord, RefreshSellRecord);
        }

        private void RemoveEventListener()
        {
            _categoryList.onSelectedIndexChanged.RemoveListener(OnSelectedRecordTypeChanged);
            EventDispatcher.RemoveEventListener(TradeMarketEvents.RefreshBuyRecord, RefreshBuyRecord);
            EventDispatcher.RemoveEventListener(TradeMarketEvents.RefreshSellRecord, RefreshSellRecord);
        }

        private void OnSelectedRecordTypeChanged(CategoryList list, int index)
        {
            if (_categoryList.SelectedIndex == BUY_RECORD)
            {
                TradeMarketManager.Instance.RequestBuyRecord();
            }
            else if (_categoryList.SelectedIndex == SELL_RECORD)
            {
                TradeMarketManager.Instance.RequestSellRecord();
            }
        }

        private void RefreshBuyRecord()
        {
            if (_categoryList.SelectedIndex == BUY_RECORD)
            {
                List<AuctionRecord> buyRecordList = PlayerDataManager.Instance.AuctionData.GetBuyRecordList();
                _recordList.SetDataList<TradeRecordItem>(buyRecordList, 10);
                _txtNoneRecord.Visible = buyRecordList.Count == 0;
                _txtNoneRecord.CurrentText.text = MogoLanguageUtil.GetContent(56228);
            }
        }

        private void RefreshSellRecord()
        {
            if (_categoryList.SelectedIndex == SELL_RECORD)
            {
                List<AuctionRecord> sellRecordList = PlayerDataManager.Instance.AuctionData.GetSellRecordList();
                _recordList.SetDataList<TradeRecordItem>(sellRecordList, 10);
                _txtNoneRecord.Visible = sellRecordList.Count == 0;
                _txtNoneRecord.CurrentText.text = MogoLanguageUtil.GetContent(56229);
            }
        }

        private void InitCategory()
        {
            List<CategoryItemData> listToggleData = new List<CategoryItemData>();
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(56223)));//购买记录
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(56222))); //售出记录

            _categoryList.SetDataList<CategoryListItem>(listToggleData);
        }
    }
}
