﻿using System;
using System.Collections.Generic;
using System.Text;

using Game.UI.UIComponent;
using UnityEngine.UI;
using Common.Global;
using UnityEngine;
using MogoEngine.Events;
using Common.Events;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Data;

namespace ModuleTradeMarket
{
    public class TradeMarketSellView : KContainer
    {
        private PutOutListView _putOutListView;   //显示已上架物品的主panel
        private TradeSelectItemView _selectItemView;     //物品上架主界面
        private ItemPutOutFixedView _putOutItemView;   //单个物品上架弹出子界面

        private KContainer _moneyContainer;

        protected override void Awake()
        {
            _putOutListView = AddChildComponent<PutOutListView>("Container_mainPanel");
            _selectItemView = AddChildComponent<TradeSelectItemView>("Container_shangjia");
            _moneyContainer = _putOutListView.AddChildComponent<SellCurrencyView>("Container_minemoney");
            _putOutItemView = AddChildComponent<ItemPutOutFixedView>("Container_shangjiawupin02");
        }

        protected override void OnEnable()
        {
            InitTradeMarketSellView();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        protected void AddEventListener()
        {
            _selectItemView.onCloseClick.AddListener(OnClosePutawayFix);
            _putOutItemView.onCloseClick.AddListener(OnClosePutawayPopup);
            EventDispatcher.AddEventListener<int>(TradeMarketEvents.OpenPutOutPanel, OnOpenPutOutPanel);
            EventDispatcher.AddEventListener(TradeMarketEvents.ClosePutOutPanel, OnClosePutawayFix);
            EventDispatcher.AddEventListener<PbPutawayItem, int>(TradeMarketEvents.OpenPutOutPopupView, OnOpenPutOutPopup);
        }

        protected void RemoveEventListener()
        {
            _selectItemView.onCloseClick.RemoveListener(OnClosePutawayFix);
            _putOutItemView.onCloseClick.RemoveListener(OnClosePutawayPopup);
            EventDispatcher.RemoveEventListener<int>(TradeMarketEvents.OpenPutOutPanel, OnOpenPutOutPanel);
            EventDispatcher.RemoveEventListener(TradeMarketEvents.ClosePutOutPanel, OnClosePutawayFix);
            EventDispatcher.RemoveEventListener<PbPutawayItem, int>(TradeMarketEvents.OpenPutOutPopupView, OnOpenPutOutPopup);
        }

        /// <summary>
        /// 打开物品上架弹出框
        /// </summary>
        private void OnOpenPutOutPanel(int index)
        {
            _selectItemView.gameObject.SetActive(true);

            _putOutItemView.gameObject.SetActive(false);
            _putOutListView.gameObject.SetActive(false);

            _selectItemView.SetSelectedIndex(index);
        }

        /// <summary>
        /// 关闭上架物品列表面板
        /// </summary>
        private void OnClosePutawayFix()
        {
            _selectItemView.gameObject.SetActive(false);
            _putOutItemView.gameObject.SetActive(false);
            _putOutListView.gameObject.SetActive(true);
        }

        /// <summary>
        /// 关闭弹出上架窗口
        /// </summary>
        private void OnClosePutawayPopup()
        {
            _putOutItemView.gameObject.SetActive(false);
            _putOutListView.gameObject.SetActive(true);
        }

        private void OnOpenPutOutPopup(PbPutawayItem putawayItem, int index)
        {
            _putOutItemView.gameObject.SetActive(true);
            _putOutListView.gameObject.SetActive(false);

            TradeItemData sellItemData = new TradeItemData((int)putawayItem.item.id);
            sellItemData.GridPosition = -1;
            sellItemData.BaseItemData.StackCount = (int)putawayItem.item.count;
            _putOutItemView.SelectedIndex = index;
            _putOutItemView.CurrentItemData = sellItemData;
        }

        private void InitTradeMarketSellView()
        {
            _selectItemView.gameObject.SetActive(false);
            _putOutItemView.gameObject.SetActive(false);
            _putOutListView.gameObject.SetActive(true);
        }


    }
}
