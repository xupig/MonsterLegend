﻿using Common.ClientConfig;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTradeMarket
{
    public class SellCurrencyView : KContainer
    {
        private MoneyItem _goldItem;
        private MoneyItem _rmbItem;
        private StateText _extractableMoney;
        private KButton _extractBtn;
        private KContainer _iconContainer;

        private KParticle _extractParticle;
        protected override void Awake()
        {
            _extractableMoney = GetChildComponent<StateText>("Container_tixian/Label_txtGold");
            _extractBtn = GetChildComponent<KButton>("Container_tixian/Button_yijiantixian");
            _iconContainer = GetChildComponent<KContainer>("Container_tixian/Container_icon");
            _goldItem = AddChildComponent<MoneyItem>("Container_gold");
            _rmbItem = AddChildComponent<MoneyItem>("Container_zuanshi");
            _extractParticle = _extractBtn.AddChildComponent<KParticle>("fx_ui_3_2_lingqu");
            _goldItem.SetMoneyType(public_config.MONEY_TYPE_GOLD);
            _rmbItem.SetMoneyType(public_config.MONEY_TYPE_BIND_DIAMOND);
            SetGetMoneyType(trademarket_helper.getMoneyType);
        }

        protected override void OnEnable()
        {
            RefreshMoney();
            AddEventsListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }


        private void AddEventsListener()
        {
            _extractBtn.onClick.AddListener(OnExtractMoney);
            EventDispatcher.AddEventListener(TradeMarketEvents.RefreshLeftMoney, OnRefreshExtractMoney);
        }

        private void RemoveEventListener()
        {
            _extractBtn.onClick.RemoveListener(OnExtractMoney);
            EventDispatcher.RemoveEventListener(TradeMarketEvents.RefreshLeftMoney, OnRefreshExtractMoney);
        }

        private void RefreshMoney()
        {
            OnRefreshExtractMoney();
        }

        private void OnExtractMoney()
        {
            TradeMarketManager.Instance.RequestTakeOutMoney();
        }

        private void OnRefreshExtractMoney()
        {
            _extractParticle.Visible = PlayerDataManager.Instance.AuctionData.LeftBindDiamond > 0;

            _extractableMoney.ChangeAllStateText(PlayerDataManager.Instance.AuctionData.LeftBindDiamond.ToString());
        }

        private void SetGetMoneyType(int moneyType)
        {
            MogoAtlasUtils.AddIcon(_iconContainer.gameObject, item_helper.GetIcon(moneyType));
        }
    }

}
