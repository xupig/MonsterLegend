﻿using System;
using System.Collections.Generic;
using System.Text;

using Game.UI.UIComponent;
using UnityEngine.UI;
using Common.Global;
using MogoEngine.Events;
using Common.Structs.ProtoBuf;
using Common.Events;
using GameMain.GlobalManager.SubSystem;
using UnityEngine;
using GameMain.GlobalManager;
using Common.ExtendComponent;
using Common.Data;

namespace ModuleTradeMarket
{
    public class PutOutListView : KContainer
    {
        private KScrollView _itemsScrollView;  //滚动已上架物品的scrollView
        private ScrollRect _itemsScrollRect;   //滚动已上架物品的scrollView的遮罩区域
        private KList _itemsList;              //显示已上架物品的列表

        private KParticle _particle;

        protected override void Awake()
        {
            _itemsScrollView = GetChildComponent<KScrollView>("ScrollView_youjianjishou");
            _particle = AddChildComponent<KParticle>("ScrollView_youjianjishou/fx_ui_17_jinbi_01");
            _particle.Stop();
            _itemsScrollRect = _itemsScrollView.GetChildComponent<ScrollRect>("mask");
            _itemsList = _itemsScrollView.GetChildComponent<KList>("mask/content");
            InitScrollView();
            InitContentList();
        }

        protected override void OnEnable()
        {
            AddEventListener();
            RefreshPutawayedItems();
            TweenListEntry.Begin(_itemsList.gameObject); 
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        protected void AddEventListener()
        {
            EventDispatcher.AddEventListener(TradeMarketEvents.RefreshSellView,RefreshPutawayedItems);
            EventDispatcher.AddEventListener<int>(TradeMarketEvents.UpdateSellGrid, RefreshPutawayedItem);
            EventDispatcher.AddEventListener<int>(TradeMarketEvents.CancelSellItem, OnCancelItem);
            EventDispatcher.AddEventListener(TradeMarketEvents.WithdrawCash, WithdrawCash);
        }

        protected void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(TradeMarketEvents.RefreshSellView, RefreshPutawayedItems);
            EventDispatcher.RemoveEventListener<int>(TradeMarketEvents.UpdateSellGrid, RefreshPutawayedItem);
            EventDispatcher.RemoveEventListener<int>(TradeMarketEvents.CancelSellItem, OnCancelItem);
            EventDispatcher.RemoveEventListener(TradeMarketEvents.WithdrawCash, WithdrawCash);
        }

        private void WithdrawCash()
        {
            _particle.Play();
            UIManager.Instance.PlayAudio("Sound/UI/money_get.mp3");
        }

        private void InitScrollView()
        {
            _itemsList.SetDirection(KList.KListDirection.LeftToRight, 1);
            _itemsList.SetPadding(0, 0, 0, 0);
            _itemsList.SetGap(-5, 0);
        }

        private void InitContentList()
        {
            for (int i = 0; i < GlobalParams.GetTradeMarketMaxGrid(); ++i)
            {
                _itemsList.AddItem<PutOutItemGrid>(null, false);
            }
            _itemsList.DoLayout();

        }

        private void RefreshPutawayedItem(int index)
        {
            PutOutItemData putOutItem = PlayerDataManager.Instance.PutOutData.GetPutOutItemDic()[index];
            _itemsList[putOutItem.PbPutawayItem.grid - 1].Data = putOutItem;
        }

        private void OnCancelItem(int gridId)
        {
            _itemsList.ItemList[gridId - 1].Data = null;
        }

        private void RefreshPutawayedItems()
        {
            Dictionary<int, PutOutItemData> putawayDic = PlayerDataManager.Instance.PutOutData.GetPutOutItemDic();

            for (int i = 0; i < GlobalParams.GetTradeMarketMaxGrid(); ++i)
            {
                if (putawayDic.ContainsKey(i + 1) == true)
                {
                    _itemsList[i].Data = putawayDic[i + 1];
                }
                else
                {
                    _itemsList[i].Data = null;
                }
            }
        }
    }
}
