﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTradeMarket
{
    public class ItemPutOutFixedView : KContainer
    {
        private ItemIconGrid _itemIconGrid;
        private StateText _itemName;
        private StateText _itemPrice;
        private StateText _unitPrice;
        private StateText _unitPricePercent;
        private StateText _itemNum;
        private StateText _consignmentTime;
        private StateText _totalPrices;
        private StateText _storageFee;
        private StateText _txtNote;

        private KButton _increasePriceBtn;
        private KButton _decreasePriceBtn;
        private KButton _increaseNumBtn;
        private KButton _decreaseNumBtn;
        private KButton _increaseTimeBtn;
        private KButton _decreaseTimeBtn;
        private KButton _putawayBtn;
        private KButton _showRuleBtn;

        private KButton _closeButton;

        private KContainer _totalDiamondIcon;
        private KContainer _storageFeeIcon;
        private KContainer _priceIcon;

        private TradeItemData _currentItemData;

        private int _selectedIndex;
        private int _storageNum = 1;//1表示24小时，2表示48小时

        public KComponentEvent onCloseClick = new KComponentEvent();

        protected override void Awake()
        {
            _itemIconGrid = AddChildComponent<ItemIconGrid>("Container_wupingmingcheng/Container_zhuangbei");
            _itemName = GetChildComponent<StateText>("Container_wupingmingcheng/Container_zhuangbeiInfo/Label_txtWupinmingcheng");
            _itemPrice = GetChildComponent<StateText>("Container_wupingmingcheng/Container_zhuangbeiInfo/Label_txtJiage");
            _unitPrice = GetChildComponent<StateText>("Container_wupingmingcheng/Container_danjia/Container_neirong/Label_txtdanjiapercent");
            _unitPricePercent = GetChildComponent<StateText>("Container_wupingmingcheng/Container_danjia/Container_neirong/Label_txtdanjia");
            _itemNum = GetChildComponent<StateText>("Container_wupingmingcheng/Container_shuliang/Container_neirong/Label_txtNum");
            _consignmentTime = GetChildComponent<StateText>("Container_wupingmingcheng/Container_jishoushijian/Container_neirong/Label_txtJianyijiage");
            _totalPrices = GetChildComponent<StateText>("Container_wupingmingcheng/Container_zongjia/Container_neirong/Label_txtJianyijiage");
            _storageFee = GetChildComponent<StateText>("Container_wupingmingcheng/Container_baoguanfei/Container_neirong/Label_txtNum");
            _txtNote = GetChildComponent<StateText>("Container_wupingmingcheng/Label_txtZhu");

            _increasePriceBtn = GetChildComponent<KButton>("Container_wupingmingcheng/Container_danjia/Button_tianjia");
            _decreasePriceBtn = GetChildComponent<KButton>("Container_wupingmingcheng/Container_danjia/Button_jianshao");
            _increaseNumBtn = GetChildComponent<KButton>("Container_wupingmingcheng/Container_shuliang/Button_tianjia");
            _decreaseNumBtn = GetChildComponent<KButton>("Container_wupingmingcheng/Container_shuliang/Button_jianshao");
            _increaseTimeBtn = GetChildComponent<KButton>("Container_wupingmingcheng/Container_jishoushijian/Button_tianjia");
            _decreaseTimeBtn = GetChildComponent<KButton>("Container_wupingmingcheng/Container_jishoushijian/Button_jianshao");
            _putawayBtn = GetChildComponent<KButton>("Container_wupingmingcheng/Button_shangjia");
            _showRuleBtn = GetChildComponent<KButton>("Container_wupingmingcheng/Container_baoguanfei/Button_Gantang");

            _closeButton = GetChildComponent<KButton>("Container_wupingmingcheng/Button_fanhui");

            _totalDiamondIcon = GetChildComponent<KContainer>("Container_wupingmingcheng/Container_zongjia/Container_neirong/Container_icon");
            _storageFeeIcon = GetChildComponent<KContainer>("Container_wupingmingcheng/Container_baoguanfei/Container_neirong/Container_icon");
            _priceIcon = GetChildComponent<KContainer>("Container_wupingmingcheng/Container_zhuangbeiInfo/Container_icon");
            InitView();

        }

        private void InitView()
        {
            AuctionData auctionData = PlayerDataManager.Instance.AuctionData;
            MogoAtlasUtils.AddIcon(_totalDiamondIcon.gameObject, item_helper.GetIcon(trademarket_helper.payMoneyType));
            MogoAtlasUtils.AddIcon(_storageFeeIcon.gameObject, item_helper.GetIcon(public_config.MONEY_TYPE_GOLD));
            MogoAtlasUtils.AddIcon(_priceIcon.gameObject, item_helper.GetIcon(trademarket_helper.payMoneyType));

            int tradeTax = trademarket_helper.TradeTax;
            if (tradeTax == 0)
            {
                _txtNote.Visible = false;
            }
            else
            {
                _txtNote.Visible = true;
                _txtNote.CurrentText.text = MogoLanguageUtil.GetContent(56217, tradeTax);
            }
        }

        public TradeItemData CurrentItemData
        {
            get
            {
                return _currentItemData;
            }
            set
            {
                ResetView();
                if (value != null)
                {
                    _currentItemData = value;

                    InitPutOutItem();
                }
            }
        }

        public int SelectedIndex
        {
            get
            {
                return _selectedIndex;
            }
            set
            {
                _selectedIndex = value;
            }
        }
        protected override void OnEnable()
        {
            ResetView();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _increasePriceBtn.onClick.AddListener(OnIncreasePriceBtnClick);
            _decreasePriceBtn.onClick.AddListener(OnDecreasePriceBtnClick);
            _increaseNumBtn.onClick.AddListener(OnIncreaseNumBtnClick);
            _decreaseNumBtn.onClick.AddListener(OnDecreaseNumBtnClick);
            _increaseTimeBtn.onClick.AddListener(OnIncreaseTimeBtnClick);
            _decreaseTimeBtn.onClick.AddListener(OnDecreaseTimeBtnClick);
            _putawayBtn.onClick.AddListener(OnPutOutBtnClick);
            _showRuleBtn.onClick.AddListener(OnShowRuleBtnClick);

            _closeButton.onClick.AddListener(OnCloseButtonClick);

        }

        private void RemoveEventListener()
        {
            _increasePriceBtn.onClick.RemoveListener(OnIncreasePriceBtnClick);
            _decreasePriceBtn.onClick.RemoveListener(OnDecreasePriceBtnClick);
            _increaseNumBtn.onClick.RemoveListener(OnIncreaseNumBtnClick);
            _decreaseNumBtn.onClick.RemoveListener(OnDecreaseNumBtnClick);
            _increaseTimeBtn.onClick.RemoveListener(OnIncreaseTimeBtnClick);
            _decreaseTimeBtn.onClick.RemoveListener(OnDecreaseTimeBtnClick);
            _putawayBtn.onClick.RemoveListener(OnPutOutBtnClick);
            _showRuleBtn.onClick.RemoveListener(OnShowRuleBtnClick);

            _closeButton.onClick.RemoveListener(OnCloseButtonClick);
        }

        private void OnCloseButtonClick()
        {
            onCloseClick.Invoke();
        }

        private void OnShowRuleBtnClick()
        {
            RuleTipsDataWrapper wrapper = new RuleTipsDataWrapper(MogoLanguageUtil.GetContent(56220), _showRuleBtn.GetComponent<RectTransform>(), RuleTipsDirection.Bottom);
            UIManager.Instance.ShowPanel(PanelIdEnum.RuleTips, wrapper);
        }

        private void OnIncreasePriceBtnClick()
        {

            if (_currentItemData == null)
            {
                return;
            }
            int price = int.Parse(_unitPrice.CurrentText.text);
            price += 5;
            if (price > _currentItemData.PriceUpLimit)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.TRADE_PRICE_HIGHEST_TIP), PanelIdEnum.TradeMarket);
                price = _currentItemData.PriceUpLimit;
            }
            _unitPrice.ChangeAllStateText(price.ToString());

            RefreshUnitPrice();
            RefreshTotalPrice();
            RefreshStorageFee();
        }

        private void OnDecreasePriceBtnClick()
        {

            if (_currentItemData == null)
            {
                return;
            }

            int price = int.Parse(_unitPrice.CurrentText.text);
            price -= 5;
            if (price < _currentItemData.PriceDownLimit)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.TRADE_PRICE_CHEAPEST_TIP), PanelIdEnum.TradeMarket);
                price = _currentItemData.PriceDownLimit;
            }
            _unitPrice.ChangeAllStateText(price.ToString());

            RefreshUnitPrice();
            RefreshTotalPrice();
            RefreshStorageFee();
        }

        private void OnIncreaseNumBtnClick()
        {
            int num = int.Parse(_itemNum.CurrentText.text);

            ///当未选中物品，当前数量等于背包最大数量，重新上架。以上这三种情况均不做任何处理
            if (_currentItemData == null || _currentItemData.BaseItemData.StackCount <= num || _currentItemData.GridPosition == -1)
            {
                return;
            }
            else if (_currentItemData.SellCount + num >= _currentItemData.SellUpLimit)
            {
                _itemNum.ChangeAllStateText((_currentItemData.SellUpLimit - _currentItemData.SellCount).ToString());
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.TRADE_SELL_UPLIMIT), PanelIdEnum.TradeMarket);
            }
            else
            {
                _itemNum.ChangeAllStateText((num + 1).ToString());
                BaseItemData itemData = _itemIconGrid.Data;
                itemData.StackCount++;
                _itemIconGrid.UpdateNum();
            }
            RefreshNum();
            RefreshTotalPrice();
            RefreshStorageFee();
        }

        private void OnDecreaseNumBtnClick()
        {
            int num = int.Parse(_itemNum.CurrentText.text);
            if (_currentItemData == null || num <= 1 || _currentItemData.GridPosition == -1)
            {
                return;
            }

            num--;

            _itemNum.ChangeAllStateText(num.ToString());
            BaseItemData itemData = _itemIconGrid.Data;
            itemData.StackCount--;
            _itemIconGrid.UpdateNum();

            RefreshNum();
            RefreshTotalPrice();
            RefreshStorageFee();
        }

        private void OnIncreaseTimeBtnClick()
        {
            if (_currentItemData == null)
            {
                return;
            }
            _storageNum = 2;
            _consignmentTime.ChangeAllStateText(MogoLanguageUtil.GetContent((int)LangEnum.TRADE_CONSIGNMENT_TIME_48));

            SetButtonGray(_increaseTimeBtn, 0.0f, false);
            SetButtonGray(_decreaseTimeBtn, 1.0f, true);

            Refresh();
        }

        private void OnDecreaseTimeBtnClick()
        {
            if (_currentItemData == null)
            {
                return;
            }
            _storageNum = 1;
            _consignmentTime.ChangeAllStateText(MogoLanguageUtil.GetContent((int)LangEnum.TRADE_CONSIGNMENT_TIME_24));
            SetButtonGray(_increaseTimeBtn, 1.0f, true);
            SetButtonGray(_decreaseTimeBtn, 0.0f, false);

            Refresh();
        }

        private void OnPutOutBtnClick()
        {
            if(_currentItemData == null)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.TRADE_SELECT_PUTAWAY_ITEM), PanelIdEnum.TradeMarket);
                return;
            }
            int unitPrice = int.Parse(_unitPrice.CurrentText.text);
            int num = int.Parse(_itemNum.CurrentText.text);
            int totalPrices = num * unitPrice;
            int storageFee = _storageNum * totalPrices * trademarket_helper.StorageFeeRate;

            if (storageFee > PlayerAvatar.Player.money_gold)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.TRADE_GOLD_NOT_ENOUGHT), PanelIdEnum.TradeMarket);
                return;
            }
            if (_currentItemData.GridPosition == -1)
            {
                TradeMarketManager.Instance.RequestRePutOutItem(_selectedIndex + 1, unitPrice, _storageNum * 24, _currentItemData.BaseItemData.Id, num);
                onCloseClick.Invoke();
                return;
            }

            TradeMarketManager.Instance.RequestPutOutItem(_currentItemData.GridPosition, unitPrice, num, _selectedIndex + 1, _storageNum * 24);
            onCloseClick.Invoke();
        }


        private void InitPutOutItem()
        {
            TradeItemData itemData = new TradeItemData(_currentItemData.BaseItemData.Id);
            itemData.BaseItemData.StackCount = 1;
            _itemIconGrid.Data = itemData.BaseItemData;
            _itemName.ChangeAllStateText(_currentItemData.BaseItemData.Name);
            _itemPrice.ChangeAllStateText("x" + _currentItemData.StandPrice.ToString());
            _unitPrice.ChangeAllStateText(_currentItemData.StandPrice.ToString());
            _unitPricePercent.ChangeAllStateText("+0%");
            _itemNum.ChangeAllStateText("1");
            if (_currentItemData.GridPosition == -1)
            {
                _itemNum.ChangeAllStateText(_currentItemData.BaseItemData.StackCount.ToString());
                itemData.BaseItemData.StackCount = _currentItemData.BaseItemData.StackCount;
                _itemIconGrid.Data = itemData.BaseItemData;
            }

            _storageNum = 1;
            _consignmentTime.ChangeAllStateText(MogoLanguageUtil.GetContent((int)LangEnum.TRADE_CONSIGNMENT_TIME_24));
            _totalPrices.ChangeAllStateText(_currentItemData.StandPrice.ToString());

            int storageFeeRatio = _currentItemData.StandPrice * trademarket_helper.StorageFeeRate;
            int storageFee = ((storageFeeRatio / 100 == 0) ? 1 : (storageFeeRatio / 100));
            _storageFee.ChangeAllStateText("x" + storageFee.ToString());

            SetButtonGray(_increaseTimeBtn, 1.0f, true);
            SetButtonGray(_decreaseTimeBtn, 0.0f, false);

            Refresh();
        }

        private void ResetView()
        {
            _currentItemData = null;
            _itemIconGrid.Data = null;
            _itemName.ChangeAllStateText(string.Empty);
            _itemPrice.ChangeAllStateText("0");
            _unitPrice.ChangeAllStateText("0");
            _unitPricePercent.ChangeAllStateText("+0%");
            _itemNum.ChangeAllStateText("0");
            _consignmentTime.ChangeAllStateText(MogoLanguageUtil.GetContent((int)LangEnum.TRADE_CONSIGNMENT_TIME_24));
            _totalPrices.ChangeAllStateText("x0");
            _storageFee.ChangeAllStateText("x0");
        }

        private void Refresh()
        {
            //刷新单价相关数据
            if (_currentItemData != null)
            {
                RefreshUnitPrice();
                RefreshNum();
                RefreshTotalPrice();
                RefreshStorageFee();
            }
        }

        private void RefreshUnitPrice()
        {
            int price = int.Parse(_unitPrice.CurrentText.text);
            if (price >= _currentItemData.StandPrice)
            {
                float percent = ((float)((price - _currentItemData.StandPrice) * 100)) / (float)(_currentItemData.StandPrice);
                _unitPricePercent.CurrentText.color = Color.red;
                _unitPricePercent.ChangeAllStateText("+" + Math.Round(percent, 1).ToString() + "%");
            }
            else
            {
                float percent = ((float)((_currentItemData.StandPrice - price) * 100)) / (float)(_currentItemData.StandPrice);
                _unitPricePercent.CurrentText.color = Color.green;
                _unitPricePercent.ChangeAllStateText("-" + Math.Round(percent, 1).ToString() + "%");
            }
            if (price >= _currentItemData.PriceUpLimit)
            {
                SetButtonGray(_increasePriceBtn, 0.0f, true);
            }
            else
            {
                SetButtonGray(_increasePriceBtn, 1.0f, true);
            }
            if (price <= _currentItemData.PriceDownLimit)
            {
                SetButtonGray(_decreasePriceBtn, 0.0f, false);
            }
            else
            {
                SetButtonGray(_decreasePriceBtn, 1.0f, true);
            }
        }

        private void RefreshNum()
        {
            int num = int.Parse(_itemNum.CurrentText.text);
            if (num >= _currentItemData.BaseItemData.StackCount || _currentItemData.GridPosition == -1
                || _currentItemData.SellCount + num >= _currentItemData.SellUpLimit)
            {
                SetButtonGray(_increaseNumBtn, 0.0f, false);
            }
            else
            {
                SetButtonGray(_increaseNumBtn, 1.0f, true);
            }

            if (num <= 1 || _currentItemData.GridPosition == -1)
            {
                SetButtonGray(_decreaseNumBtn, 0.0f, false);
            }
            else
            {
                SetButtonGray(_decreaseNumBtn, 1.0f, true);
            }
        }

        private void RefreshTotalPrice()
        {
            int unitPrice = int.Parse(_unitPrice.CurrentText.text);
            int num = int.Parse(_itemNum.CurrentText.text);

            _totalPrices.ChangeAllStateText("x" + (unitPrice * num).ToString());
        }

        private void RefreshStorageFee()
        {
            int unitPrice = int.Parse(_unitPrice.CurrentText.text);
            int num = int.Parse(_itemNum.CurrentText.text);
            int totalPrices = num * unitPrice;
            int storageFeeRatio = _storageNum * totalPrices * trademarket_helper.StorageFeeRate;
            int storageFee = ((storageFeeRatio / 100 == 0) ? 1 : (storageFeeRatio / 100));
            _storageFee.ChangeAllStateText("x" + storageFee.ToString());
        }

        private void SetButtonGray(KButton button, float grayValue, bool isInteractable)
        {
            ImageWrapper[] wrappers = button.gameObject.GetComponentsInChildren<ImageWrapper>(true);
            for (int i = 0; i < wrappers.Length; i++)
            {
                wrappers[i].SetGray(grayValue);
            }
        }
    }
}
