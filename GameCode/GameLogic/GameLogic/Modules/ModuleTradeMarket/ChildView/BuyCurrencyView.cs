﻿using Common.ClientConfig;
using Common.ExtendComponent;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTradeMarket
{
    public class BuyCurrencyView:KContainer
    {
        private MoneyItem _goldItem;
        private MoneyItem _rmbItem;

        protected override void Awake()
        {
            _goldItem = AddChildComponent<MoneyItem>("Container_gold");
            _rmbItem = AddChildComponent<MoneyItem>("Container_zuanshi");

            _goldItem.SetMoneyType(public_config.MONEY_TYPE_GOLD);
            int buyCurrencyType = trademarket_helper.getMoneyType;
            _rmbItem.SetMoneyType(buyCurrencyType);
        }

        protected override void OnEnable()
        {
            AddEventsListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventsListener()
        {

        }

        private void RemoveEventListener()
        {

        }


    }
}
