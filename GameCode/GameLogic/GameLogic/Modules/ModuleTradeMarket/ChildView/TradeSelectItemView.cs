﻿using System;
using System.Collections.Generic;
using System.Text;

using Game.UI.UIComponent;
using Common.Data;
using GameMain.GlobalManager;
using Common.Structs;
using GameData;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using Common.Structs.ProtoBuf;
using Common.Events;
using UnityEngine;
using ModuleCommonUI;
using Common.Base;
using Common.Utils;
using Common.Global;
using Common.ExtendComponent;

namespace ModuleTradeMarket
{
    public class TradeSelectItemView : KContainer
    {
        private KContainer _validItemsList;    //显示能上架物品的列表
        private ItemPutOutFixedView _itemPutawayFixed;  //单个物品上架信息面板
        private KList _itemList;
        private List<TradeItemData> _selectableItemList;
        private TradeItemData _currentItemData;
        private KContainer _noneItemContainer;
        private StateText _noneItemLabel;

        public KComponentEvent onCloseClick = new KComponentEvent();

        protected override void Awake()
        {
            _validItemsList = GetChildComponent<KContainer>("Container_jishouliebiao");
            _noneItemContainer = GetChildComponent<KContainer>("Container_wuwupin");
            _noneItemLabel = _noneItemContainer.GetChildComponent<StateText>("Label_txtZhu");
            _itemPutawayFixed = AddChildComponent<ItemPutOutFixedView>("Container_shangjiawupin");

            _itemList = _validItemsList.GetChildComponent<KList>("ScrollView_jishouwupin/mask/content");

            InitSelectItemView();

        }

        protected override void OnEnable()
        {
            RefreshSelectableItem();
            AddEventListener();

            TweenViewMove.Begin(_validItemsList.gameObject, MoveType.Show, MoveDirection.Left);
            TweenViewMove.Begin(_itemPutawayFixed.gameObject, MoveType.Show, MoveDirection.Right);
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        protected void AddEventListener()
        {
            _itemPutawayFixed.onCloseClick.AddListener(OnCloseButtonClick);
            _itemList.onSelectedIndexChanged.AddListener(OnSelectedItemChanged);
            EventDispatcher.AddEventListener<PbItemCount>(TradeMarketEvents.UpSellItemInfo, OnRefreshSellItem);
        }

        public void SetSelectedIndex(int index)
        {
            _itemPutawayFixed.SelectedIndex = index;
        }

        protected void RemoveEventListener()
        {
            _itemPutawayFixed.onCloseClick.RemoveListener(OnCloseButtonClick);
            _itemList.onSelectedIndexChanged.RemoveListener(OnSelectedItemChanged);

            EventDispatcher.RemoveEventListener<PbItemCount>(TradeMarketEvents.UpSellItemInfo, OnRefreshSellItem);
        }

        private void OnCloseButtonClick()
        {
            onCloseClick.Invoke();
        }

        private void InitSelectItemView()
        {
            _itemList.SetDirection(KList.KListDirection.LeftToRight, 4);
            _itemList.SetPadding(5, 15, 5, 15);
            _itemList.SetGap(15, 10);

            _selectableItemList = new List<TradeItemData>();
            _noneItemLabel.ChangeAllStateText(MogoLanguageUtil.GetContent((int)LangEnum.TRADE_NONE_ITEM_SELL));
        }

        private void RefreshSelectableItem()
        {
            _selectableItemList.Clear();

            Dictionary<int,BaseItemInfo> bagData = PlayerDataManager.Instance.BagData.GetItemBagData().GetAllItemInfo();
            foreach (KeyValuePair<int, BaseItemInfo> kvp in bagData)
            {
                if(XMLManager.trade_items.ContainsKey(kvp.Value.Id) == true && kvp.Value.IsBind != true)
                {
                    TradeItemData itemData = new TradeItemData(kvp.Value.Id);
                    itemData.BaseItemData.StackCount = kvp.Value.StackCount;
                    itemData.GridPosition = kvp.Value.GridPosition;
                    _selectableItemList.Add(itemData);
                }
            }
            _itemList.RemoveAll();

            _noneItemContainer.gameObject.SetActive(false);
            if(_selectableItemList.Count == 0)
            {
                _noneItemContainer.gameObject.SetActive(true);
                return;
            }

            _itemList.SetDataList<SalableItem>(_selectableItemList);
        }

        private void OnSelectedItemChanged(KList list, int index)
        {
            _currentItemData = _itemList.ItemList[index].Data as TradeItemData;
            TradeMarketManager.Instance.RequestSelledCount(_currentItemData.BaseItemData.Id);
        }

        private void OnRefreshSellItem(PbItemCount itemCount)
        {
            TradeItemData sellItemData = new TradeItemData((int)itemCount.type_id);
            sellItemData.SellCount = itemCount.count;
            if (sellItemData.SellCount >= sellItemData.SellUpLimit)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.TRADE_SELL_UPLIMIT), PanelIdEnum.TradeMarket);
                return;
            }
            sellItemData.BaseItemData.StackCount = _currentItemData.BaseItemData.StackCount;
            sellItemData.GridPosition = _currentItemData.GridPosition;
            _itemPutawayFixed.CurrentItemData = sellItemData;
        }

    }
}
