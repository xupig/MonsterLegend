﻿using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.CustomType;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTradeMarket
{
    public class FilterView : KContainer
    {
        private StateText _refreshTime;
        private StateText _txtRefreshTime;

        private KButton _maskBtn;

        private ConditionTypeItem[] _filtTypeList;
        private ConditionTypeItem _selectedConditionTypeItem;

        private DateTime _dateTime;
        private int _itemType;
        private string timeFormat = "{0:00}:{1:00}:{2:00}";
        private bool _isRefresh = true;

        private uint _timerId = TimerHeap.INVALID_ID;

        public KComponentEvent onFilterChanged = new KComponentEvent();

        protected override void Awake()
        {
            _filtTypeList = new ConditionTypeItem[3];
            _refreshTime = GetChildComponent<StateText>("Label_txtTime");
            _txtRefreshTime = GetChildComponent<StateText>("Label_txtRenwumudi");
            _maskBtn = GetChildComponent<KButton>("Button_biaoji");

            for (int i = 0; i < 3; i++)
            {
                _filtTypeList[i] = AddChildComponent<ConditionTypeItem>("Container_shaixuan" + (i + 1));
            }

            InitFilterView();
        }


        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventsListener();
            _isRefresh = true;
            OnRefreshLeftTime();
        }

        protected override void OnDisable()
        {
            _isRefresh = false;
            RemoveTimer();
            RemoveEventsListener();
            base.OnDisable();

        }

        public List<FilterData> GetFilterDataList()
        {
            List<FilterData> result = new List<FilterData>();
            int count = _filtTypeList.Length;
            for (int i = 0; i < count; i++)
            {
                TradeFilterItem filtConditionData = _filtTypeList[i].CurrentFilterCondition;
                TradeFilterType conditionTypeData = _filtTypeList[i].Data as TradeFilterType;
                if (filtConditionData != null && filtConditionData.Value != 0)
                {
                    result.Add(new FilterData(conditionTypeData.Type, filtConditionData.Value));
                }
            }
            return result;
        }

        public void ShowFiltCondition(int itemType)
        {
            _itemType = itemType;

            List<TradeFilterType> filterTypeList = trademarket_helper.GetTradeFilterList(itemType);
            ResetConditionList();
            for (int i = 0; i < filterTypeList.Count; i++)
            {
                _filtTypeList[i].gameObject.SetActive(true);
                _filtTypeList[i].Data = filterTypeList[i];
            }
        }

        private void ResetConditionList()
        {
            for (int i = 0; i < 3; i++)
            {
                _filtTypeList[i].Data = null;
                _filtTypeList[i].gameObject.SetActive(false);
            }
        }

        //public void RefreshPageButton(PbMarketItems marketItems)
        //{
        //    _nextPage.gameObject.SetActive(false);
        //    _prePage.gameObject.SetActive(false);
        //    if (marketItems.cur_page < marketItems.total_page)
        //    {
        //        _nextPage.gameObject.SetActive(true);
        //    }
        //    if (marketItems.cur_page > 1)
        //    {
        //        _nextPage.gameObject.SetActive(true);
        //    }
        //}

        private void AddEventsListener()
        {
            //_prePage.onClick.AddListener(OnPrePageBtnClick);
            //_nextPage.onClick.AddListener(OnNextPageBtnClick);
            _maskBtn.onClick.AddListener(OnMaskBtnClick);

            for (int i = 0; i < 3; i++)
            {
                _filtTypeList[i].onConditionTypeChanged.AddListener(OnConditionTypeChange);
            }

            EventDispatcher.AddEventListener(TradeMarketEvents.UpdateGlobalInfo, OnRefreshLeftTime);
        }

        private void RemoveEventsListener()
        {
            //_prePage.onClick.RemoveListener(OnPrePageBtnClick);
            //_nextPage.onClick.RemoveListener(OnNextPageBtnClick);
            _maskBtn.onClick.RemoveListener(OnMaskBtnClick);

            for (int i = 0; i < 3; i++)
            {
                _filtTypeList[i].onConditionTypeChanged.RemoveListener(OnConditionTypeChange);
            }
            EventDispatcher.RemoveEventListener(TradeMarketEvents.UpdateGlobalInfo, OnRefreshLeftTime);
        }

        private void InitFilterView()
        {
            _txtRefreshTime.ChangeAllStateText(MogoLanguageUtil.GetContent((int)LangEnum.TRADE_REFRESH_TIME_TXT));

            _maskBtn.gameObject.SetActive(false);
        }

        //private void OnNextPageBtnClick()
        //{
        //    RequestSearch();
        //}

        //private void OnPrePageBtnClick()
        //{
        //    RequestSearch();
        //}


        private void OnMaskBtnClick()
        {
            _selectedConditionTypeItem.IsSelected = false;
            _selectedConditionTypeItem = null;
            _maskBtn.Visible = false;
        }

        private void OnRefreshLeftTime()
        {
            RemoveTimer();
            ComputeNextRefreshDateTime();
            _timerId = TimerHeap.AddTimer(0, 1000, RefreshTime);
        }

        private void ComputeNextRefreshDateTime()
        {
            DateTime serverTimeWithGainDateTime = PlayerDataManager.Instance.AuctionData.DateTime;
            long serverTimeWithGainTimeStamp = MogoTimeUtil.UtcTime2ServerTimeStamp(serverTimeWithGainDateTime);
            DateTime nowServerDateTime = PlayerTimerManager.GetInstance().GetNowServerDateTime();
            long nowServerTimeStamp = MogoTimeUtil.UtcTime2ServerTimeStamp(nowServerDateTime);
            int refreshInteralTime = trademarket_helper.RefreshInteral;
            int leftRefreshTime = PlayerDataManager.Instance.AuctionData.LeftRefreshTime;

            long nextRefreshGap = refreshInteralTime - (((nowServerTimeStamp + refreshInteralTime) - (serverTimeWithGainTimeStamp + leftRefreshTime))%refreshInteralTime);

            _dateTime = nowServerDateTime.AddSeconds(nextRefreshGap);
        }

        private void RemoveTimer()
        {
            if (_timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = TimerHeap.INVALID_ID;
            }
        }

        private void OnConditionTypeChange(ConditionTypeItem conditionTypeItem, int index)
        { 
            if (conditionTypeItem == _selectedConditionTypeItem)
            {
                _selectedConditionTypeItem.IsSelected = false;
                _selectedConditionTypeItem = null;
                _maskBtn.Visible = false;
                return;
            }
            else if (_selectedConditionTypeItem != null)
            {
                _selectedConditionTypeItem.IsSelected = false;
            }

            _selectedConditionTypeItem = conditionTypeItem;
            _selectedConditionTypeItem.IsSelected = true;
            _maskBtn.Visible = true;
        }

        private void RefreshTime()
        {
            DateTime dateTime = PlayerTimerManager.GetInstance().GetNowServerDateTime();
            if (dateTime > _dateTime)
            {
                _dateTime = dateTime.AddSeconds(trademarket_helper.RefreshInteral);
                if (_isRefresh == true)
                {
                    onFilterChanged.Invoke();
                }
            }

            TimeSpan deltaTime = _dateTime.Subtract(dateTime);
            _refreshTime.ChangeAllStateText(string.Format(timeFormat, deltaTime.TotalHours, deltaTime.Minutes, deltaTime.Seconds));
        }
    }

}
