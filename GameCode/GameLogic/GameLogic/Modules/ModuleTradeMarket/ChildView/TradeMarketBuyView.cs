﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Game.UI.UIComponent;
using UnityEngine.UI;
using Common.ExtendComponent;
using Common.Utils;
using GameData;
using GameMain.GlobalManager.SubSystem;
using GameLoader.Utils.CustomType;
using MogoEngine.Events;
using Common.Structs.ProtoBuf;
using Common.Events;
using Common.ServerConfig;
using Common.Global;
using UnityEngine;
using GameMain.GlobalManager;
using Common.Base;
using Common.Data;
using GameMain.Entities;
using MogoEngine.Mgrs;
using Common.ClientConfig;



namespace ModuleTradeMarket
{
    public class TradeMarketBuyView : KContainer
    {
        private KScrollView _scrollView;
        private KList _buyList;
        private CategoryList _categoryToggleGroup;
        private BuyCurrencyView _buyCurrencyView;
        private FilterView _filterView;
        private StateText _noneItemToBuy;
        private StateText _txtWantBuyNum;
        private KButton _wantBuyButton;

        private Dictionary<int, int> _indexToTypeDic;
        public static int CurrentPage;
        private int _totalPage;

        protected override void Awake()
        {
            _scrollView = GetChildComponent<KScrollView>("ScrollView_youjiangoumai");
            _buyList = GetChildComponent<KList>("ScrollView_youjiangoumai/mask/content");
            _noneItemToBuy = GetChildComponent<StateText>("Container_wugoumai/Label_txtRenwumudi");
            _txtWantBuyNum = GetChildComponent<StateText>("Label_txtjinrishengyuqiugoucishu");
            _categoryToggleGroup = GetChildComponent<CategoryList>("List_categoryList");
            _buyCurrencyView = AddChildComponent<BuyCurrencyView>("Container_minemoney");
            _filterView = AddChildComponent<FilterView>("Container_shaixuan");
            _wantBuyButton = GetChildComponent<KButton>("Button_wodeqiugou");

            _buyList.SetDirection(KList.KListDirection.LeftToRight, 2, int.MaxValue);
            _buyList.SetGap(0, 11);
            InitScrollView();
            InitContentList();
        }

        protected override void OnEnable()
        {
            OnItemTypeChanged(_categoryToggleGroup, 0);
            _categoryToggleGroup.SelectedIndex = 0;
            _filterView.ShowFiltCondition(_indexToTypeDic[0]);
            RefreshWantBuyNum();
            AddEventsListener();
            TradeMarketManager.Instance.RequestWantToBuyList();
        }

        protected override void OnDisable()
        {
            RemoveEventsListener();
        }

        private void AddEventsListener()
        {
            _categoryToggleGroup.onSelectedIndexChanged.AddListener(OnItemTypeChanged);
            _scrollView.ScrollRect.onRequestNext.AddListener(RequestNextPageData);
            //_scrollView.ScrollRect.onReRequest.AddListener(RequestPrePageData);
            _wantBuyButton.onClick.AddListener(ShowWantBuyPanel);
            _filterView.onFilterChanged.AddListener(OnFilterChanged);
            EventDispatcher.AddEventListener(TradeMarketEvents.SendSearch, OnFilterChanged);
            EventDispatcher.AddEventListener(TradeMarketEvents.RefreshBuyView, OnRefreshBuyView);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.auction_want_buy_times, RefreshWantBuyNum);
        }

        private void RemoveEventsListener()
        {
            _categoryToggleGroup.onSelectedIndexChanged.RemoveListener(OnItemTypeChanged);
            _scrollView.ScrollRect.onRequestNext.RemoveListener(RequestNextPageData);
            //_scrollView.ScrollRect.onReRequest.RemoveListener(RequestPrePageData);
            _wantBuyButton.onClick.RemoveListener(ShowWantBuyPanel);
            _filterView.onFilterChanged.RemoveListener(OnFilterChanged);
            EventDispatcher.RemoveEventListener(TradeMarketEvents.SendSearch, OnFilterChanged);
            EventDispatcher.RemoveEventListener(TradeMarketEvents.RefreshBuyView, OnRefreshBuyView);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.auction_want_buy_times, RefreshWantBuyNum);
        }

        private void RequestNextPageData(KScrollRect scrollRect)
        {
            CurrentPage++;
            PlayerDataManager.Instance.AuctionData.FilterAndFillTradeItemInfo(_indexToTypeDic[_categoryToggleGroup.SelectedIndex], _filterView.GetFilterDataList(), Math.Max(CurrentPage, 1));
            if (CurrentPage > PlayerDataManager.Instance.AuctionData.CurPage) { return; }
            if (PlayerDataManager.Instance.AuctionData.CurPage < PlayerDataManager.Instance.AuctionData.TotalPage)
            {

                TradeMarketManager.Instance.RequestSearchItemByPage(_indexToTypeDic[_categoryToggleGroup.SelectedIndex], _filterView.GetFilterDataList(), Math.Min(CurrentPage, PlayerDataManager.Instance.AuctionData.TotalPage));
            }
            else
            {
                OnRefreshBuyView();
            }
        }

        //private void RequestPrePageData(KScrollRect scrollRect)
        //{
        //    CurrentPage--;
        //    if (PlayerDataManager.Instance.AuctionData.CurPage <= PlayerDataManager.Instance.AuctionData.TotalPage)
        //    {
        //        TradeMarketManager.Instance.RequestSearchItemByPage(_indexToTypeDic[_categoryToggleGroup.SelectedIndex], _filterView.GetFilterDataList(), Math.Max(CurrentPage, 1));
        //    }
        //    else
        //    {
        //        OnRefreshBuyView();
        //    }
        //}

        private void ShowWantBuyPanel()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.TradeWantBuy);
        }

        private void RefreshWantBuyNum()
        {
            _txtWantBuyNum.CurrentText.text = MogoLanguageUtil.GetContent(56239, trademarket_helper.GetWantBuyUpLimit() - PlayerAvatar.Player.auction_want_buy_times);
        }

        private void OnRefreshBuyView()
        {
            List<TradeItemData> tradeItemList = PlayerDataManager.Instance.AuctionData.FilterAndFillTradeItemInfo(_indexToTypeDic[_categoryToggleGroup.SelectedIndex], _filterView.GetFilterDataList(), Math.Max(CurrentPage, 1));
            CurrentPage = PlayerDataManager.Instance.AuctionData.CurPage;
            _totalPage = PlayerDataManager.Instance.AuctionData.TotalPage;
            _buyList.SetDataList<CanToBuyItem>(tradeItemList, 2);
            _noneItemToBuy.Visible = tradeItemList.Count == 0;
        }

        private void OnItemTypeChanged(CategoryList _toggleGroup, int index)
        {
            CurrentPage = 1;
            _buyList.RemoveAll();
            _filterView.ShowFiltCondition(_indexToTypeDic[index]);
            TradeMarketManager.Instance.RequestSearchItems(_indexToTypeDic[index],null);
        }

        private void OnFilterChanged()
        {
            CurrentPage = 1;
            _buyList.RemoveAll();
            TradeMarketManager.Instance.RequestSearchItems(_indexToTypeDic[_categoryToggleGroup.SelectedIndex], _filterView.GetFilterDataList());
        }

        private void InitScrollView()
        {
            _indexToTypeDic = new Dictionary<int, int>();
            List<CategoryItemData> _toggleNameList = new List<CategoryItemData>();
            List<string> typeList = trademarket_helper.TradeItemTypeList;
            int count = typeList.Count;
            for (int i = 0; i < count; i++)
            {
                if (int.Parse(typeList[i]) != 0)
                {
                    string typeName = trademarket_helper.GetTradeTypeDesc(int.Parse(typeList[i]));
                    _toggleNameList.Add(CategoryItemData.GetCategoryItemData(typeName));
                }
                else
                {
                    _toggleNameList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetString(LangEnum.TRADE_OTHER_CHINESE)));
                }
                _indexToTypeDic[i] = int.Parse(typeList[i]);
            }

            _categoryToggleGroup.RemoveAll();
            _categoryToggleGroup.SetDataList<CategoryListItem>(_toggleNameList);
            _categoryToggleGroup.SelectedIndex = 0;

            _noneItemToBuy.ChangeAllStateText(MogoLanguageUtil.GetContent((int)LangEnum.TRADE_NONE_ITEM_TO_BUY));

            HideRedPoints();
        }

        private void HideRedPoints()
        {
            int count = _categoryToggleGroup.ItemList.Count;
            for (int i = 0; i < count; i++)
            {
                (_categoryToggleGroup[i] as CategoryListItem).SetPoint(false);
            }
        }

        private void InitContentList()
        {
            
        }

    }
}
