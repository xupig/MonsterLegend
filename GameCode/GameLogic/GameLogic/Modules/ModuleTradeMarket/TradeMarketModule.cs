﻿#region 模块信息
/*==========================================
// 创建者：andy
// 修改者列表：
// 创建日期：2015/3/11
// 描述说明：交易市场主模块
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Text;

using Common.Base;

namespace ModuleTradeMarket
{
    public class TradeMarketModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.TradeMarket, "Container_TradePanel", MogoUILayer.LayerUIPanel, "ModuleTradeMarket.TradeMarketPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.TradeWantBuy, "Container_TradeWantBuyPanel", MogoUILayer.LayerUIPanel, "ModuleTradeMarket.TradeWantBuyPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.TradeBuy, "Container_TradeBuyPanel", MogoUILayer.LayerUIToolTip, "ModuleTradeMarket.TradeBuyPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }
    }
}
