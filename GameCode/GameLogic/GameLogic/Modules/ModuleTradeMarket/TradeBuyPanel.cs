﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTradeMarket
{
    public class TradeBuyPanel : BasePanel
    {
        private KButton _increaseItemBtn;
        private KButton _decreaseItemBtn;
        private KButton _increaseTenItemBtn;
        private KButton _decreaseTenItemBtn;
        private KButton _buyButton;

        private StateText _itemNum;
        private StateText _totalPrice;
        private StateText _itemName;

        private KContainer _iconKContainer;

        private TradeItemData _currentSellItem;
        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_BG/Button_close");
            _increaseItemBtn = GetChildComponent<KButton>("Button_tianjia");
            _decreaseItemBtn = GetChildComponent<KButton>("Button_jianshao");

            _increaseTenItemBtn = GetChildComponent<KButton>("Button_shuangjia");
            _decreaseTenItemBtn = GetChildComponent<KButton>("Button_shuangjian");

            _buyButton = GetChildComponent<KButton>("Button_zhong");

            _itemNum = GetChildComponent<StateText>("Label_txtShuliang");
            _totalPrice = GetChildComponent<StateText>("Label_txtSjine");
            _itemName = GetChildComponent<StateText>("Label_txtbiaoti");
            _iconKContainer = GetChildComponent<KContainer>("Container_icon");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TradeBuy; }
        }

        public override void OnShow(object data)
        {
            AddEventsListener();
            TradeMarketManager.Instance.RequestWantToBuyList();
            _currentSellItem = data as TradeItemData;
            _itemNum.ChangeAllStateText("1");
            _itemName.CurrentText.text = _currentSellItem.BaseItemData.Name;
            int currencyType = trademarket_helper.payMoneyType;
            MogoAtlasUtils.AddIcon(_iconKContainer.gameObject, item_helper.GetIcon(currencyType));
            Refresh();
        }

        public override void OnClose()
        {
            RemoveEventsListener();
        }

        private void AddEventsListener()
        {
            _increaseItemBtn.onClick.AddListener(OnIncreaseBtnClick);
            _decreaseItemBtn.onClick.AddListener(OnDecreaseBtnClick);
            _increaseTenItemBtn.onClick.AddListener(OnIncreaseTenBtnClick);
            _decreaseTenItemBtn.onClick.AddListener(OnDecreaseTenBtnClick);
            _buyButton.onClick.AddListener(OnBuyBtnClick);

        }

        private void RemoveEventsListener()
        {  
            _increaseItemBtn.onClick.RemoveListener(OnIncreaseBtnClick);
            _decreaseItemBtn.onClick.RemoveListener(OnDecreaseBtnClick);
            _increaseTenItemBtn.onClick.RemoveListener(OnIncreaseTenBtnClick);
            _decreaseTenItemBtn.onClick.RemoveListener(OnDecreaseTenBtnClick);
            _buyButton.onClick.RemoveListener(OnBuyBtnClick);

        }

        private void OnBuyBtnClick()
        {
            int count = int.Parse(_itemNum.CurrentText.text);

            if (CheckMoneyIsEnough() == false)
            {
                ClosePanel();
                return;
            }
            if (IsBagEnough())
            {
                TradeMarketManager.Instance.RequestBuyItem(_currentSellItem.BaseItemData.Id, count, _currentSellItem.Price);
                OnCloseBtnClick();
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.TRADE_BAG_NOT_ENOUGHT), PanelIdEnum.TradeMarket);
            }
        }

        private bool CheckMoneyIsEnough()
        {
            int count = int.Parse(_itemNum.CurrentText.text);
            BagData bagData = PlayerDataManager.Instance.BagData.GetItemBagData();
            int payMoneyType = trademarket_helper.payMoneyType;
            int moneyCount = _currentSellItem.Price * count;
            int myMoneyCount = item_helper.GetMyMoneyCountByItemId(payMoneyType);
            if (moneyCount > myMoneyCount)
            {
                if (payMoneyType == public_config.MONEY_TYPE_BIND_DIAMOND)
                {
                    string content = MogoLanguageUtil.GetContent(56231, moneyCount - myMoneyCount, item_helper.GetName(public_config.MONEY_TYPE_DIAMOND));
                    Action confirmAction = new Action(() =>
                    {
                        if (moneyCount - myMoneyCount > PlayerAvatar.Player.money_diamond)
                        {
                            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.FRIEND_SEND_DIAMOND), PanelIdEnum.TradeMarket);
                        }
                        else if (IsBagEnough())
                        {
                            TradeMarketManager.Instance.RequestBuyItem(_currentSellItem.BaseItemData.Id, count, _currentSellItem.Price);
                            OnCloseBtnClick();
                        }
                        else
                        {
                            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.TRADE_BAG_NOT_ENOUGHT), PanelIdEnum.TradeMarket);
                        }
                    });
                    Action cancelAction = new Action(() =>
                    {
                        MessageBox.CloseMessageBox();
                    });
                    MessageBox.Show(false, MessageBox.DEFAULT_TITLE, content, confirmAction, cancelAction, false, true, true);
                }
                return false;
            }
            return true;
        }

        private bool IsBagEnough()
        {
            Dictionary<int, BaseItemInfo> bagData;
            bagData = PlayerDataManager.Instance.BagData.GetItemBagData().GetAllItemInfo();
            uint curBagCount = PlayerAvatar.Player.cur_bag_count;
            int count = int.Parse(_itemNum.CurrentText.text);

            int maxStackCount = ((int)curBagCount - bagData.Count) * _currentSellItem.BaseItemData.MaxStackCount;

            foreach (KeyValuePair<int, BaseItemInfo> kvp in bagData)
            {
                if (kvp.Value.Id == _currentSellItem.BaseItemData.Id)
                {
                    maxStackCount += (_currentSellItem.BaseItemData.MaxStackCount - kvp.Value.StackCount);
                }
            }

            if (maxStackCount < count)
            {
                return false;
            }
            return true;
        }


        private void OnIncreaseTenBtnClick()
        {
            IncreaseBuyItem(10);
        }

        private void OnDecreaseTenBtnClick()
        {
            DecreaseBuyItem(10);
        }

        private void OnDecreaseBtnClick()
        {
            DecreaseBuyItem(1);
        }

        private void OnIncreaseBtnClick()
        {
            IncreaseBuyItem(1);
        }

        private void IncreaseBuyItem(int itemNum)
        {
            int count = int.Parse(_itemNum.CurrentText.text) + itemNum;

            if (count + _currentSellItem.BuyCount > _currentSellItem.BuyUpLimit)
            {
                count = _currentSellItem.BuyUpLimit - _currentSellItem.BuyCount;
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.TRADE_BUY_UPLIMIT), PanelIdEnum.TradeMarket);
            }

            int minLimit = Mathf.Min(_currentSellItem.BaseItemData.StackCount, trademarket_helper.PerBuyUpLimit, _currentSellItem.GetPerBuyLimitNum());
            if (count <= minLimit)
            {
                _itemNum.ChangeAllStateText(count.ToString());
            }
            else
            {
                _itemNum.ChangeAllStateText(minLimit.ToString());
            }
            Refresh();

        }

        private void DecreaseBuyItem(int itemNum)
        {
            int count = int.Parse(_itemNum.CurrentText.text);

            _itemNum.ChangeAllStateText((count > itemNum ? count - itemNum : 1).ToString());
            Refresh();
        }

        private void Refresh()
        {
            if (_currentSellItem != null)
            {
                int num = int.Parse(_itemNum.CurrentText.text);

                SetButtonGray(_increaseItemBtn, 0.0f, true);
                SetButtonGray(_decreaseItemBtn, 0.0f, true);
                SetButtonGray(_increaseTenItemBtn, 0.0f, true);
                SetButtonGray(_decreaseTenItemBtn, 0.0f, true);

                if (num < _currentSellItem.BaseItemData.StackCount
                    && num < trademarket_helper.PerBuyUpLimit
                    && num + _currentSellItem.BuyCount < _currentSellItem.BuyUpLimit
                    && num < _currentSellItem.GetPerBuyLimitNum())
                {
                    SetButtonGray(_increaseItemBtn, 1.0f, true);
                    SetButtonGray(_increaseTenItemBtn, 1.0f, true);
                }

                if (num > 1)
                {
                    SetButtonGray(_decreaseItemBtn, 1.0f, true);
                    SetButtonGray(_decreaseTenItemBtn, 1.0f, true);
                }

                _totalPrice.ChangeAllStateText("x" + (num * _currentSellItem.Price).ToString());
            }
        }

        private void SetButtonGray(KButton button, float grayValue, bool isInteractable)
        {
            ImageWrapper[] wrappers = button.gameObject.GetComponentsInChildren<ImageWrapper>(true);
            for (int i = 0; i < wrappers.Length; i++)
            {
                wrappers[i].SetGray(grayValue);
            }
        }
    }
}
