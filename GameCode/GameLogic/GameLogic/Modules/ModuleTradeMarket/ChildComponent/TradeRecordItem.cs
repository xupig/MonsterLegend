﻿
using Common.Data;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTradeMarket
{
    public class TradeRecordItem:KList.KListItemBase
    {
        private StateText _txtRecord;

        private AuctionRecord _record;

        protected override void Awake()
        {
            _txtRecord = GetChildComponent<StateText>("Label_txtQinmizhi");
        }

        public override void Dispose()
        {
            _record = null;
        }

        public override object Data
        {
            get
            {
                return _record;
            }
            set
            {
                _record = value as AuctionRecord;
                Refresh();
            }
        }

        private void Refresh()
        {
            int moneyCount = (int)_record.ItemRecord.money;
            string moneyName = item_helper.GetName((int)_record.ItemRecord.money_type);
            int itemCount = (int)_record.ItemRecord.count;
            string itemName = item_helper.GetName((int)_record.ItemRecord.type_id);
            DateTime tradeTime = MogoTimeUtil.ServerTimeStamp2UtcTime(_record.ItemRecord.time);
            if (_record.RecordType == AuctionRecordType.BuyRecord)
            {
                //你花费$(23,{0}){1}购买了$(23,{2})个{3}    {4}-{5}-{6}
                _txtRecord.CurrentText.text = MogoLanguageUtil.GetContent(56225, moneyCount, moneyName, itemCount, itemName, tradeTime.Year, tradeTime.Month, tradeTime.Day);
            }
            else if(_record.RecordType == AuctionRecordType.SellRecord)
            {
                //你寄售的{0}已成功售出，获得$(23,{1}){2}收益    {3}-{4}-{5}
                _txtRecord.CurrentText.text = MogoLanguageUtil.GetContent(56226, itemName, moneyCount, moneyName, tradeTime.Year, tradeTime.Month, tradeTime.Day);
            }
        }
    }
}
