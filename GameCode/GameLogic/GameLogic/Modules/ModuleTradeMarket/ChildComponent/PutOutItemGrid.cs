﻿using System;
using System.Collections.Generic;
using System.Text;

using Game.UI.UIComponent;
using Common.Structs.ProtoBuf;
using GameData;
using ModuleCommonUI;
using UnityEngine;
using GameMain.Entities;
using Common.Global;
using Common.Utils;
using Common.Data;
using Common.Base;
using Common.ServerConfig;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager.SubSystem;
using GameLoader.Utils.Timer;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using Common.ExtendComponent;


namespace ModuleTradeMarket
{
    class PutOutItemGrid : KList.KListItemBase
    {
        private PutOutItemData _data; //该单元的数据结构

        private KContainer _containerItemPanel;  //显示物品信息的面板
        private KContainer _containerEmptyPanel;
        private KContainer _containerLockedPanel;
        private KContainer _unlockPriceIcon;
        private ItemIconGrid _containerItemIcon;

        private KDummyButton _containerPopMsgBtn;
        private KDummyButton _iconButton;

        private StateText _textItemName; //物品名称
        private StateText _textPrice;    //物品价格
        private StateText _textLeftTime;   //剩余时间
        private StateText _textLeftCount;  //显示已售出数量和总数量
        private StateText _textUnlockPriceLabel;

        private StateImage _outOfDateImg;
        private StateImage _sellOutImg;
        private KButton _pulledBtn;
        private KButton _rePutawayBtn;
        private KButton _unlockBtn;
        private KButton _drawButton;
        private KButton _withdrawCashButton;
        private KButton _putOutBtn;
        private DateTime _outOfDate;
        private KContainer _moneyIcon;
        private KParticle _withdrawCashParticle;
        private uint _timerId;
        private string timeFormat = "{0:00}:{1:00}:{2:00}";

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = null;
                if (value != null)
                {
                    _data = value as PutOutItemData;
                    
                }
                InitData();
                RefreshUI();
            }
        }

        public override void Dispose()
        {
            if (_data != null)
            {
                TimerHeap.DelTimer(_timerId);
            }
            _data = null;
        }


        protected override void Awake()
        {
            _containerItemPanel = this.GetChildComponent<KContainer>("Container_ItemInfo");
            _containerEmptyPanel = this.GetChildComponent<KContainer>("Container_Empty");
            _containerLockedPanel = this.GetChildComponent<KContainer>("Container_Unlock");
            _containerPopMsgBtn = this.gameObject.AddComponent<KDummyButton>();
            _iconButton = _containerItemPanel.AddChildComponent<KDummyButton>("Container_ItemIcon");

            _unlockBtn = _containerLockedPanel.GetChildComponent<KButton>("Button_jiesuo");
            _containerItemIcon = _containerItemPanel.AddChildComponent<ItemIconGrid>("Container_ItemIcon");
            _drawButton = _containerItemPanel.GetChildComponent<KButton>("Button_linghui");
            _putOutBtn = _containerItemPanel.GetChildComponent<KButton>("Button_shangjia1");
            _withdrawCashButton = _containerItemPanel.GetChildComponent<KButton>("Button_tixian");
            _withdrawCashParticle = _withdrawCashButton.AddChildComponent<KParticle>("fx_ui_3_2_lingqu");
            _textItemName = _containerItemPanel.GetChildComponent<StateText>("Label_txtItemName");
            _textPrice = _containerItemPanel.GetChildComponent<StateText>("Label_txtItemPrice");
            _textLeftTime = _containerItemPanel.GetChildComponent<StateText>("Label_txtLeftTime");
            _textLeftCount = _containerItemPanel.GetChildComponent<StateText>("Label_txtLeftItemCount");
            _outOfDateImg = _containerItemPanel.GetChildComponent<StateImage>("Image_sharedguoqi");
            _sellOutImg = _containerItemPanel.GetChildComponent<StateImage>("Image_sharedshouqing");
            _pulledBtn = _containerItemPanel.GetChildComponent<KButton>("Button_mianfeixiajia");
            _rePutawayBtn = _containerItemPanel.GetChildComponent<KButton>("Button_shangjia");
            _moneyIcon = _containerItemPanel.GetChildComponent<KContainer>("Container_icon");
            
            _textUnlockPriceLabel = _containerLockedPanel.GetChildComponent<StateText>("Button_jiesuo/label");
            _unlockPriceIcon = _containerLockedPanel.GetChildComponent<KContainer>("Button_jiesuo/icon");
            _textUnlockPriceLabel.ChangeAllStateText(GlobalParams.GetTradeMarketExpandPrice().ToString());
            int moneyType = GlobalParams.GetTradeMarketExpandMoneyType();
            MogoAtlasUtils.AddIcon(_unlockPriceIcon.gameObject, item_helper.GetIcon(moneyType));

            SetPayMoneyType();
        }

        public override void Show()
        {
            AddEventListener();
        }

        public override void Hide()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _containerPopMsgBtn.onClick.AddListener(OnGridClick);
            _pulledBtn.onClick.AddListener(OnPulledBtnClick);
            _rePutawayBtn.onClick.AddListener(OnRePutawayBtnClick);
            _unlockBtn.onClick.AddListener(UnlockGrid);
            _iconButton.onClick.AddListener(ShowItemTip);
            _withdrawCashButton.onClick.AddListener(WithdrawCash);
            _drawButton.onClick.AddListener(OnPulledBtnClick);
            _putOutBtn.onClick.AddListener(PutOutItem);
            EventDispatcher.AddEventListener(TradeMarketEvents.UpdateUnlockGrid, RefreshUI);
        }

        protected void RemoveEventListener()
        {
            _containerPopMsgBtn.onClick.RemoveListener(OnGridClick);
            _pulledBtn.onClick.RemoveListener(OnPulledBtnClick);
            _rePutawayBtn.onClick.RemoveListener(OnRePutawayBtnClick);
            _unlockBtn.onClick.RemoveListener(UnlockGrid);
            _iconButton.onClick.RemoveListener(ShowItemTip);
            _withdrawCashButton.onClick.RemoveListener(WithdrawCash);
            _drawButton.onClick.RemoveListener(OnPulledBtnClick);
            _putOutBtn.onClick.RemoveListener(PutOutItem);
            EventDispatcher.RemoveEventListener(TradeMarketEvents.UpdateUnlockGrid, RefreshUI);
        }

        private void InitData()
        {
            if (_data != null)
            {
                _outOfDate = _data.DateTime.AddSeconds(_data.PbPutawayItem.left_time);
            }
        }

        /// <summary>
        /// 刷新该格子上架物品信息的显示
        /// </summary>
        protected void RefreshUI()
        {
            switch(GetGridState())
            {
                case ItemGridState.Locked:
                    SetLockState();
                    break;
                case ItemGridState.Empty:
                    SetEmptyState();
                    break;
                case ItemGridState.OutOfDate:
                    SetOutOfDateState();
                    break;
                case ItemGridState.Selling:
                    SetSellingState();
                    break;
                case ItemGridState.SellOut:
                    SetSellOutState();
                    break;
            }
        }

        private int GetGridPosition()
        {
            return this.Index + 1;
        }

        private void OnPulledBtnClick()
        {
            TradeMarketManager.Instance.RequestPullItem(Index + 1);
            onClick.Invoke(this, Index);
        }
        /// <summary>
        /// 获取已开启格子数量
        /// </summary>
        /// <returns></returns>
        private int GetOpenGridCount()
        {
            return PlayerDataManager.Instance.AuctionData.UnlockGridNum +GlobalParams.GetTradeMarketFreeGrid();
        }

        /// <summary>
        /// 格子当前数据状态
        /// </summary>
        /// <returns></returns>
        private ItemGridState GetGridState()
        {
            if(GetGridPosition() > GetOpenGridCount())
            {
                return ItemGridState.Locked;
            }
            else if(this._data == null)
            {
                return ItemGridState.Empty;
            }
            else if (_data.PbPutawayItem.item.count <= 0)
            {
                return ItemGridState.SellOut;
            }
            else if (PlayerTimerManager.GetInstance().GetNowServerDateTime() > _outOfDate)
            {
                return ItemGridState.OutOfDate;
            }
            return ItemGridState.Selling;
        }

        /// <summary>
        /// 处理格子为空的情况
        /// </summary>
        private void SetEmptyState()
        {
            _containerEmptyPanel.gameObject.SetActive(true);
            _containerItemPanel.gameObject.SetActive(false);
            _containerLockedPanel.gameObject.SetActive(false);
            RemoveTimer();
        }

        /// <summary>
        /// 处理格式加锁的情况
        /// </summary>
        private void SetLockState()
        {
            _containerLockedPanel.gameObject.SetActive(true);
            _containerEmptyPanel.gameObject.SetActive(false);
            _containerItemPanel.gameObject.SetActive(false);
            RemoveTimer();
        }

        private void SetSellOutState()
        {
            RemoveTimer();
            SetOccupiedState();
            HideIcons();

            Decimal hours = 0;
            Decimal minutes = 0;
            Decimal seconds = 0;
            if (_data.PbPutawayItem.left_time > 0)
            {
                hours = Math.Floor(Convert.ToDecimal(_data.PbPutawayItem.left_time / 3600));
                minutes = Math.Floor(Convert.ToDecimal((_data.PbPutawayItem.left_time % 3600) / 60));
                seconds = Math.Floor(_data.PbPutawayItem.left_time - hours * 3600 - minutes * 60);
            }

            _textLeftTime.ChangeAllStateText(string.Format(timeFormat, hours, minutes, seconds));
            _putOutBtn.Visible = true;
            _sellOutImg.Visible = true;
        }

        private void SetWithdrawState()
        {
            RemoveTimer();
            SetOccupiedState();
            HideIcons();

            switch(GetGridState())
            {
                case ItemGridState.OutOfDate:
                    _textLeftTime.ChangeAllStateText(string.Format(timeFormat, 0, 0, 0));
                    break;
                case ItemGridState.Selling:
                    _timerId = TimerHeap.AddTimer(0, 1000, RefreshTime);
                    break;
                case ItemGridState.SellOut:                    
                    Decimal hours = 0;
                    Decimal minutes = 0;
                    Decimal seconds = 0;
                    if (_data.PbPutawayItem.left_time > 0)
                    {
                        hours = Math.Floor(Convert.ToDecimal(_data.PbPutawayItem.left_time / 3600));
                        minutes = Math.Floor(Convert.ToDecimal((_data.PbPutawayItem.left_time % 3600) / 60));
                        seconds = Math.Floor(_data.PbPutawayItem.left_time - hours * 3600 - minutes * 60);
                    }
                    break;
            }
            _withdrawCashButton.Visible = true;
        }

        private void SetOutOfDateState()
        {
            RemoveTimer();
            SetOccupiedState();
            HideIcons();

            _textLeftTime.ChangeAllStateText(string.Format(timeFormat, 0, 0, 0));

            _drawButton.Visible = true;
            _outOfDateImg.Visible = true;
            _rePutawayBtn.Visible = true;
        }

        private void SetSellingState()
        {
            RemoveTimer();
            SetOccupiedState();
            _timerId = TimerHeap.AddTimer(0, 1000, RefreshTime);
            HideIcons();

            _pulledBtn.Visible = true;
        }

        private void SetOccupiedState()
        {
            _containerLockedPanel.Visible = false;
            _containerEmptyPanel.Visible = false;
            _containerItemPanel.Visible = true;

            TradeItemData itemData = new TradeItemData((int)_data.PbPutawayItem.item.id);
            itemData.BaseItemData.StackCount = (int)_data.PbPutawayItem.item.count;
            _containerItemIcon.Data = itemData.BaseItemData;
            _textItemName.ChangeAllStateText(itemData.BaseItemData.Name);
            _textPrice.ChangeAllStateText("x" + _data.PbPutawayItem.price.ToString());
            _textLeftCount.ChangeAllStateText(string.Format("{0}/{1}", _data.PbPutawayItem.total_count - _data.PbPutawayItem.item.count, _data.PbPutawayItem.total_count));
        }

        private void RefreshTime()
        {
            DateTime dateTime = PlayerTimerManager.GetInstance().GetNowServerDateTime(); ;
            if (dateTime > _outOfDate)
            {
                RefreshUI();
            }
            TimeSpan timeSpan = _outOfDate.Subtract(dateTime);
            _textLeftTime.ChangeAllStateText(string.Format(timeFormat, timeSpan.Days * 24 + timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds));
        }

        private void RemoveTimer()
        {
            if (_timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = TimerHeap.INVALID_ID;
            }
        }

        private void OnRePutawayBtnClick()
        {
            EventDispatcher.TriggerEvent<PbPutawayItem, int>(TradeMarketEvents.OpenPutOutPopupView, _data.PbPutawayItem, Index);
        }

        private void OnGridClick()
        {
            Debug.Log("PutawyItemGrid::OnGridClick");
            switch (GetGridState())
            {
                case ItemGridState.OutOfDate:
                case ItemGridState.Selling:
                case ItemGridState.SellOut:
                    
                    break;
                case ItemGridState.Locked:
                    UnlockGrid();
                    break;
                case ItemGridState.Empty:
                    PutOutItem();
                    break;
                default:
                    break;
            }
        }

        private void HideIcons()
        {
            _outOfDateImg.Visible = false;
            _sellOutImg.Visible = false;
            _pulledBtn.Visible = false;
            _drawButton.Visible = false;
            _putOutBtn.Visible = false;
            _rePutawayBtn.Visible = false;
            _withdrawCashButton.Visible = false;
        }

        private void WithdrawCash()
        {
            //领取当前格子上的现金
        }

        /// <summary>
        /// 显示物品的tooltips
        /// </summary>
        private void ShowItemTip()
        {
            BagItemType itemType = (BagItemType)item_helper.GetItemType((int)_data.PbPutawayItem.item.id);
            ItemToolTipsData itemToolData = new ItemToolTipsData((int)_data.PbPutawayItem.item.id);
            if(itemType == BagItemType.Equip)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.EquipToolTips, itemToolData, PanelIdEnum.TradeMarket);
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.ItemToolTips, itemToolData, PanelIdEnum.TradeMarket);
            }
        }

        /// <summary>
        /// 弹出上架物品面板
        /// </summary>
        private void PutOutItem()
        {
            EventDispatcher.TriggerEvent(TradeMarketEvents.OpenPutOutPanel, Index);
        }

        /// <summary>
        /// 解锁某个格子
        /// </summary>
        private void UnlockGrid()
        {
            int count = GetGridPosition() - GetOpenGridCount();
            int money = GlobalParams.GetTradeMarketExpandPrice() * count;
            int moneyType = GlobalParams.GetTradeMarketExpandMoneyType();
            int myMoney = item_helper.GetMyMoneyCountByItemId(moneyType);

            if(myMoney >= money)
            {
                string content = MogoLanguageUtil.GetContent(56240, money, item_helper.GetName(moneyType), count);
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, OnConfirm);
            }
            else if (moneyType == public_config.MONEY_TYPE_BIND_DIAMOND && money - myMoney <= PlayerAvatar.Player.money_diamond)
            {
                UseDiamondReplaceBindDiamond(count, money, myMoney);
            }
            else
            {
                SystemInfoManager.Instance.Show(error_code.ERR_NOT_ENOUGH_DIAMOND);
            }
        }

        private static void UseDiamondReplaceBindDiamond(int count, int money, int myMoney)
        {
            string content = MogoLanguageUtil.GetContent(56241, money - myMoney);
            Action confirmAction = new Action(() =>
            {
                TradeMarketManager.Instance.RequestExtendGrid(count);
            });
            Action cancelAction = new Action(() =>
            {
                MessageBox.CloseMessageBox();
            });
            MessageBox.Show(false, MessageBox.DEFAULT_TITLE, content, confirmAction, cancelAction, false, true, true);
        }

        /// <summary>
        /// 确认解锁格子
        /// </summary>
        private void OnConfirm()
        {
            int count = GetGridPosition() - GetOpenGridCount();
            TradeMarketManager.Instance.RequestExtendGrid(count);
        }

        private void SetPayMoneyType()
        {
            int payMoneyType = trademarket_helper.payMoneyType;
            MogoAtlasUtils.AddIcon(_moneyIcon.gameObject, item_helper.GetIcon(payMoneyType));
        }

    }

    /**格子当前数据状态**/
    public enum ItemGridState
    {
        Empty,    //没有物品
        Locked,   //未解锁的格子
        WithdrawCash, //提现状态
        OutOfDate, //过期，需要重新上架，或下架
        Selling,  //正在出售状态
        SellOut,  //售罄
    }
}
