﻿using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTradeMarket
{
    public class SalableItem : KList.KListItemBase
    {
        private ItemIconGrid _itemIconGrid;
        private ItemGrid _itemGrid;

        private TradeItemData _data;

        protected override void Awake()
        {
            _itemIconGrid = this.gameObject.AddComponent<ItemIconGrid>();
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
        }

        public override void Dispose()
        {
            _data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as TradeItemData;
                    _itemIconGrid.Data = _data.BaseItemData;
                    Refresh();
                }
            }
        }

        private void OnClick()
        {
            onClick.Invoke(this, Index);
        }

        public void UpdateNum()
        {
            _itemIconGrid.UpdateNum();
        }

        private void Refresh()
        {
            _itemGrid.onClick = OnClick;
        }
    }
}
