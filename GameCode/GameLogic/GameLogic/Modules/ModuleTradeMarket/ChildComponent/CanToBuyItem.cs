﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTradeMarket
{
    public class CanToBuyItem : KList.KListItemBase
    {
        private ItemIconGrid _itemIconGrid;
        private StateText _itemName;
        private StateText _itemNum;
        private StateText _itemPrice;
        private StateText _txtItemPriceLabel;
        private StateText _txtWantBuy;
        private StateImage _sellOutImg;
        private StateImage _shortSupplyImg;
        private StateImage _outOfDateImg;
        private KButton _buyButton;
        private KButton _wantBuyButton;
        private KButton _cancelBuyButton;
        private IconContainer _moneyIcon;
        private TradeItemData _data;

        protected override void Awake()
        {
            _itemIconGrid = AddChildComponent<ItemIconGrid>("Container_zhuangbei");

            _itemName = GetChildComponent<StateText>("Label_txtName");
            _itemNum = GetChildComponent<StateText>("Label_txtNum");
            _txtItemPriceLabel = GetChildComponent<StateText>("Label_txtdanjialabel");
            _itemPrice = GetChildComponent<StateText>("Label_txtdanjia");
            _txtWantBuy = GetChildComponent<StateText>("Label_txtQiugourenshu");
            _sellOutImg = GetChildComponent<StateImage>("Image_sharedshouqing");
            _sellOutImg.AddAllStateComponent<RaycastComponent>();
            _sellOutImg.Visible = false;
            _shortSupplyImg = GetChildComponent<StateImage>("Image_quehuo");
            _shortSupplyImg.AddAllStateComponent<RaycastComponent>();
            _outOfDateImg = GetChildComponent<StateImage>("Image_yiguoqi");
            _outOfDateImg.AddAllStateComponent<RaycastComponent>();
            _outOfDateImg.Visible = false;
            _buyButton = GetChildComponent<KButton>("Button_goumai");
            _wantBuyButton = GetChildComponent<KButton>("Button_qiugou");
            _cancelBuyButton = GetChildComponent<KButton>("Button_quxiao");
            _moneyIcon = AddChildComponent<IconContainer>("Container_icon");
            AddEventListener();
        }

        public override void Dispose()
        {
            RemoveEventListener();
            _data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as TradeItemData;
                Refresh();
            }
        }

        private void AddEventListener()
        {
            _buyButton.onClick.AddListener(OnBuyBtnClick);
            _wantBuyButton.onClick.AddListener(OnWantBuyBtnClick);
            _cancelBuyButton.onClick.AddListener(OnCancelBuyBtnClick);
            GetChildComponent<ItemGrid>("Container_zhuangbei/Container_wupin").onClick = OnShowItemInfo;

            EventDispatcher.AddEventListener<int>(TradeMarketEvents.UpdateBuyItemInfo, OnRefreshBuyItemInfo);
            EventDispatcher.AddEventListener<int>(TradeMarketEvents.UpdateBuyItem, OnRefreshBuyItem);
        }

        private void RemoveEventListener()
        {
            _buyButton.onClick.RemoveListener(OnBuyBtnClick);
            _wantBuyButton.onClick.RemoveListener(OnWantBuyBtnClick);
            _cancelBuyButton.onClick.RemoveListener(OnCancelBuyBtnClick);
            GetChildComponent<ItemGrid>("Container_zhuangbei/Container_wupin").onClick = null;
            EventDispatcher.RemoveEventListener<int>(TradeMarketEvents.UpdateBuyItemInfo, OnRefreshBuyItemInfo);
            EventDispatcher.RemoveEventListener<int>(TradeMarketEvents.UpdateBuyItem, OnRefreshBuyItem);
        }

        private void OnRefreshBuyItem(int itemId)
        {
            if (_data != null && _data.BaseItemData.Id == itemId)
            {
                Refresh();
            }
        }

        private void OnRefreshBuyItemInfo(int itemId)
        {
            if (_data != null && _data.BaseItemData.Id == itemId)
            {
                if (_data.BaseItemData.StackCount <= 0)
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(56711), PanelIdEnum.TradeMarket);
                    return;
                }
                if (_data.BuyCount >= _data.BuyUpLimit)
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.TRADE_BUY_UPLIMIT), PanelIdEnum.TradeMarket);
                    return;
                }
                if (_data.BaseItemData.MaxStackCount <= 1)
                {
                    if (CheckMoneyIsEnough() == false) return;
                    if (IsBagEnough())
                    {
                        onClick.Invoke(this, Index);
                        BuyItem();
                    }
                    else
                    {
                        ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.TRADE_BAG_NOT_ENOUGHT), PanelIdEnum.TradeMarket);
                    }
                    return;
                }
                onClick.Invoke(this, Index);
                UIManager.Instance.ShowPanel(PanelIdEnum.TradeBuy, _data);
            }
        }

        private bool CheckMoneyIsEnough()
        {
            BagData bagData = PlayerDataManager.Instance.BagData.GetItemBagData();
            int payMoneyType = trademarket_helper.payMoneyType;
            int moneyCount = _data.Price;
            int myMoneyCount = item_helper.GetMyMoneyCountByItemId(payMoneyType);
            if (moneyCount > myMoneyCount)
            {
                if (payMoneyType == public_config.MONEY_TYPE_BIND_DIAMOND && moneyCount - myMoneyCount <= PlayerAvatar.Player.money_diamond)
                {
                    string content = MogoLanguageUtil.GetContent(56231, moneyCount - myMoneyCount, item_helper.GetName(public_config.MONEY_TYPE_DIAMOND));
                    Action confirmAction = new Action(() =>
                    {
                        if (IsBagEnough())
                        {
                            onClick.Invoke(this, Index);
                            BuyItem();
                        }
                        else
                        {
                            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.TRADE_BAG_NOT_ENOUGHT), PanelIdEnum.TradeMarket);
                        }
                    });
                    Action cancelAction = new Action(() =>
                    {
                        MessageBox.CloseMessageBox();
                    });
                    MessageBox.Show(false, MessageBox.DEFAULT_TITLE, content, confirmAction, cancelAction, false, true, true);
                }
                else
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.FRIEND_SEND_DIAMOND), PanelIdEnum.TradeMarket);
                }
                return false;
            }
            return true;
        }

        private bool IsBagEnough()
        {
            Dictionary<int, BaseItemInfo> bagData;
            bagData = PlayerDataManager.Instance.BagData.GetItemBagData().GetAllItemInfo();
            uint curBagCount = PlayerAvatar.Player.cur_bag_count;
            if (bagData.Count >= curBagCount)
            {
                return false;
            }
            return true;
        }


        private void BuyItem()
        {
            TradeMarketManager.Instance.RequestBuyItem(_data.BaseItemData.Id, 1, _data.Price);
        }


        private void OnShowItemInfo()
        {
            BagItemType itemType = (BagItemType)item_helper.GetItemType((int)_data.BaseItemData.Id);
            ItemToolTipsData itemToolData = new ItemToolTipsData((int)_data.BaseItemData.Id);
            if (itemType == BagItemType.Equip)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.EquipToolTips, itemToolData, PanelIdEnum.TradeMarket);
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.ItemToolTips, itemToolData, PanelIdEnum.TradeMarket);
            }
        }

        private void OnBuyBtnClick()
        {
            TradeMarketManager.Instance.RequestBuyedCount(_data.BaseItemData.Id);
        }

        private void OnWantBuyBtnClick()
        {
            TradeMarketManager.Instance.ChangeWantToBuyState(_data.BaseItemData.Id, 1);
        }

        private void OnCancelBuyBtnClick()
        {
            TradeMarketManager.Instance.ChangeWantToBuyState(_data.BaseItemData.Id, 0);
        }

        private void Refresh()
        {
            TradeItemData itemData = new TradeItemData(_data.BaseItemData.Id);
            itemData.BaseItemData.StackCount = _data.BaseItemData.StackCount;
            _itemIconGrid.Data = itemData.BaseItemData;
            _itemName.ChangeAllStateText(itemData.BaseItemData.Name);
            _itemNum.ChangeAllStateText(_data.BaseItemData.StackCount.ToString());
            if (_data.BaseItemData.StackCount > 0)
            {
                ///56108 均价
                _txtItemPriceLabel.CurrentText.text = MogoLanguageUtil.GetContent(56108);
                _itemPrice.ChangeAllStateText(string.Format("x{0}", _data.Price.ToString()));
            }
            else
            {
                ///56242 价格
                _txtItemPriceLabel.CurrentText.text = MogoLanguageUtil.GetContent(56242);
                ///56243 {0}~{1}
                _itemPrice.ChangeAllStateText(string.Format("x{0}", MogoLanguageUtil.GetContent(56243, _data.PriceDownLimit, _data.PriceUpLimit)));
            }
            int _buyCurrencyType = trademarket_helper.payMoneyType;
            MogoAtlasUtils.AddIcon(_moneyIcon.gameObject, item_helper.GetIcon(_buyCurrencyType));

            if (_data.BaseItemData.StackCount == 0)
            {
                _shortSupplyImg.Visible = true;
                _buyButton.Visible = false;
                _wantBuyButton.Visible = _data.IsWantBuy == false;
                _cancelBuyButton.Visible = _data.IsWantBuy;
                _txtWantBuy.CurrentText.text = MogoLanguageUtil.GetContent(56238, _data.WantAvatarNum);
            }
            else
            {
                _shortSupplyImg.Visible = false;
                _buyButton.Visible = true;
                _wantBuyButton.Visible = false;
                _cancelBuyButton.Visible = false;
                _txtWantBuy.Clear();
            }
        }
    }
}
