﻿
using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using GameData;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTradeMarket
{
    public class ConditionTypeItem : KContainer
    {
        private StateText _conditionTypeName;
        private KButton _button;

        private KList _conditionList;
        private KContainer _selectView;
        private TradeFilterType _data;

        public KComponentEvent<ConditionTypeItem, int> onConditionTypeChanged = new KComponentEvent<ConditionTypeItem,int>();

        protected override void Awake()
        {
            _conditionTypeName = GetChildComponent<StateText>("Button_dengjishaixuan/label");
            _button = GetChildComponent<KButton>("Button_dengjishaixuan");
            _selectView = GetChildComponent<KContainer>("Container_xuanzeliebiao");
            _conditionList = _selectView.GetChildComponent<KList>("ScrollView_xuanzeliebiao/mask/content");

            InitView();

        }

        protected override void OnEnable()
        {
            AddEventsListener();
        }

        protected override void OnDisable()
        {
            RemoveEventsListener();
        }

        protected override void OnDestroy()
        {
            _data = null;
        }

        public TradeFilterType Data
        {
            get
            {
                return _data;
            }
            set
            {
                _conditionList.SelectedIndex = -1;
                if (value != null)
                {
                    _data = value;
                    Refresh();
                }
            }
        }

        public bool IsSelected
        {
            get
            {
                return _selectView.IsActive();
            }
            set
            {
                _selectView.Visible = value;
                if (value == true)
                {
                    _conditionList.RemoveAll();
                    _conditionList.SetDataList<FilterConditionItem>(_data.TradeFilterItemList);
                }
            }
        }

        public TradeFilterItem CurrentFilterCondition
        {
            get
            {
                if (_conditionList.SelectedIndex != -1)
                {
                    return _conditionList[_conditionList.SelectedIndex].Data as TradeFilterItem;
                }
                return null;
            }
        }

        public int Index
        {
            get;
            set;
        }

        private void AddEventsListener()
        {
            _button.onClick.AddListener(OnBtnClick);
            _conditionList.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
        }

        private void RemoveEventsListener()
        {
            _button.onClick.RemoveListener(OnBtnClick);
            _conditionList.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
        }

        private void OnSelectedIndexChanged(KList list, int index)
        {
            TradeFilterItem tradeFilterItem = list[index].Data as TradeFilterItem;
            if (tradeFilterItem.Value == 0)
            {
                _conditionTypeName.ChangeAllStateText(_data.Name);
            }
            else
            {
                _conditionTypeName.ChangeAllStateText(tradeFilterItem.Content);
            }
            onConditionTypeChanged.Invoke(this, Index);
            EventDispatcher.TriggerEvent(TradeMarketEvents.SendSearch);
        }

        private void OnBtnClick()
        {
            onConditionTypeChanged.Invoke(this,Index);
        }

        private void InitView()
        {
            _selectView.Visible = false;
            _conditionList.SetDirection(KList.KListDirection.LeftToRight, 1);
            _conditionList.SetPadding(0, 0, 0, 0);
            _conditionList.SetGap(0,0);
        }

        private void Refresh()
        {
            _conditionTypeName.ChangeAllStateText(_data.Name);
        }
    }
}
