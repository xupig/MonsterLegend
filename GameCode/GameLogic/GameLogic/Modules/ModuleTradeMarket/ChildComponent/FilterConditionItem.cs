﻿using Common.Data;
using Game.UI.UIComponent;

namespace ModuleTradeMarket
{
    public class FilterConditionItem : KList.KListItemBase
    {
        private StateText _filtConditionName;

        private TradeFilterItem _data;
        private KToggle _toggle;

        protected override void Awake()
        {
            _filtConditionName = GetChildComponent<StateText>("Toggle_xuanze/label");
            _toggle = GetChildComponent<KToggle>("Toggle_xuanze");
            _toggle.onValueChanged.AddListener(OnSelectedCondition);
        }

        public override void Dispose()
        {
            _toggle.onValueChanged.RemoveListener(OnSelectedCondition);
            _data = null;
        }

        private void OnSelectedCondition(KToggle toggle, bool isOn)
        {
            onClick.Invoke(this, Index);
        }
 
        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as TradeFilterItem;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            _filtConditionName.ChangeAllStateText(_data.Content);
        }
    }

    
}
