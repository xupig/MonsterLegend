﻿using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTradeMarket
{
    public class ItemIconGrid : KContainer
    {
        private ItemGrid _itemGrid;
        private StateText _txtNum;

        private BaseItemData _data;

        protected override void Awake()
        {
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            if (GetChild("Label_txtNum") != null)
            {
                _txtNum = GetChildComponent<StateText>("Label_txtNum");
                _txtNum.Clear();
            }
        }

        protected override void OnDestroy()
        {
            _data = null;
        }

        public BaseItemData Data
        {
            get
            {
                return _data;
            }
            set
            {
                RemoveSprite();
                if (value != null)
                {
                    _data = value;
                    Refresh();
                }
            }
        }

        public void UpdateNum()
        {
            if (_txtNum != null && _data.StackCount > 1)
            {
                _txtNum.CurrentText.text = _data.StackCount.ToString();
            }
        }

        private void Refresh()
        {
            _itemGrid.SetItemData(_data);
            UpdateNum();
        }

        private void RemoveSprite()
        {
            _itemGrid.Clear();
            if (_txtNum != null)
            {
                _txtNum.Clear();
            }
        }
    }
}
