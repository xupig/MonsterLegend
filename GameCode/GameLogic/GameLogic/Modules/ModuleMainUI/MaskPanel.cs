﻿using Common.Base;
using Common.ClientConfig;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using Mogo.Util;
using MogoEngine.Mgrs;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleMainUI
{
    public class MaskPanel : BasePanel
    {
        private float _fastTime;
        private float _slowTime;
        private float _startAlpha;
        private float _middleAlpha;
        private float _endAlpha;

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("Container_zhezhao/ScaleImage_sharedZhezhao");
            gameObject.AddComponent<RaycastComponent>();
            ModalMask.Alpha = 0;
        }

        private void InitParam(string[] paramList)
        {
            if (paramList.Length >= 5)
            {
                _fastTime = float.Parse(paramList[0]);
                _slowTime = float.Parse(paramList[1]);
                _startAlpha = float.Parse(paramList[2]);
                _middleAlpha = float.Parse(paramList[3]);
                _endAlpha = float.Parse(paramList[4]);
            }
        }

        private void ShowMask()
        {
            ModalMask.Alpha = _startAlpha;
            TweenStateChangeableAlpha.Begin(ModalMask.gameObject, _fastTime, _middleAlpha, OnTweenEnd);
        }

        private void OnTweenEnd(UITweener tween)
        {
            TweenStateChangeableAlpha.Begin(ModalMask.gameObject, _slowTime, _endAlpha, OnHide);
        }

        private void OnHide(UITweener tween)
        {
            ClosePanel();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.MaskPanel; }
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            string[] paramList = null;
            if (data is string)
            {
                string param = data as string;
                paramList = param.Split(',');
            }
            if (paramList == null)
            {
                paramList = global_params_helper.GetGlobalParam(GlobalParamId.scene_change_mask).Split(',');
            }
            InitParam(paramList);
            ShowMask();
        }

        public override void OnShow(object data)
        {
            SetData(data);
        }

        public override void OnClose()
        {
            ModalMask.Alpha = 0;
        }
    }
}
