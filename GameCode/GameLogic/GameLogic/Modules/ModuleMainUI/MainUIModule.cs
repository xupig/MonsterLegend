﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MogoEngine.Events;
using Common.Structs.ProtoBuf;
using Common.Events;


namespace ModuleMainUI
{
    public class MainUIModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.MainUIField, "Container_MainFieldPanel", MogoUILayer.LayerUIMain, "ModuleMainUI.MainUIFieldPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);
            AddPanel(PanelIdEnum.MainUIBattle, "Container_MainBattlePanel", MogoUILayer.LayerUIMain, "ModuleMainUI.MainUIBattlePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);
            AddPanel(PanelIdEnum.BattleElement, "Container_BattleElementPanel", MogoUILayer.LayerUIMain, "ModuleMainUI.BattleElementPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);
            AddPanel(PanelIdEnum.FieldChangeLine, "Container_FieldChangeLinePanel", MogoUILayer.LayerUIPanel, "ModuleMainUI.FieldChangeLinePanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.BossPartSelect, "Container_BossPartSelectPanel", MogoUILayer.LayerUIMain, "ModuleMainUI.BossPartSelectPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, true);
            AddPanel(PanelIdEnum.BossPartPop, "Container_BossPartPopPanel", MogoUILayer.LayerUIToolTip, "ModuleMainUI.BossPartPopPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.MaskPanel, "Container_MaskPanel", MogoUILayer.LayerUITop, "ModuleMainUI.MaskPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);
            AddPanel(PanelIdEnum.SmallMap, "Container_SmallMapPanel", MogoUILayer.LayerUIPanel, "ModuleSmallMap.SmallMapPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.ChatEntry, "Container_ChatEntry", MogoUILayer.LayerUIMain, "ModuleMainUI.ChatEntryPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);
            AddPanel(PanelIdEnum.MainBattleControl, "Container_MainBattleControlPanel", MogoUILayer.LayerUIMain, "ModuleMainUI.MainBattleControlPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);
        }
    }
}
