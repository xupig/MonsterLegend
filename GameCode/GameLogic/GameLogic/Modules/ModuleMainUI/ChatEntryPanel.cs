﻿using System;
using System.Collections.Generic;
using Common.Base;
using Game.UI.UIComponent;
using UnityEngine.EventSystems;
using ModuleChat;
using Common.ServerConfig;
using GameMain.Entities;
using UnityEngine;
using Common.Data;
using GameMain.GlobalManager;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using MogoEngine.Events;
using Common.Events;
using ModuleFastNotice;
using GameMain.GlobalManager.SubSystem;

namespace ModuleMainUI
{
    public class ChatEntryPanel : BasePanel
    {
        private TownMiniChatView _miniChatView;
        private ChatVoiceNoticeView _voiceNoticeView;
        private List<ChatVoiceInputView> _voiceInputViewList;
        private KButton _worldVoiceBtn;
        private KButton _teamVoiceBtn;
        private KButton _guildVoiceBtn;
        private ChatVoiceAgent _chatVoiceAgent;
        protected KContainer _bottomContainer;

        private FastNoticeView _fastNoticeView;

        private bool _isInitVoiceInputViews = false;
        private Vector2[] _channelBtnPosList = new Vector2[3];

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.ChatEntry; }
        }

        protected override void Awake()
        {
           
            _bottomContainer = GetChildComponent<KContainer>("Container_bottomCenter");
            _fastNoticeView = _bottomContainer.AddChildComponent<FastNoticeView>("Container_fastNotice");
            _fastNoticeView.Hide();
            _miniChatView = AddChildComponent<TownMiniChatView>("Container_bottomCenter/Button_Duihua");
            _voiceNoticeView = AddChildComponent<ChatVoiceNoticeView>("Container_bottomCenter/Container_yuyintishi");
            _worldVoiceBtn = GetChildComponent<KButton>("Container_bottomCenter/Button_shijieyuyin");
            _teamVoiceBtn = GetChildComponent<KButton>("Container_bottomCenter/Button_duiwuyuyin");
            _guildVoiceBtn = GetChildComponent<KButton>("Container_bottomCenter/Button_gonghuiyuyin");
            List<ChatVoiceInputView> voiceInputList = new List<ChatVoiceInputView>();
            MiniChatVoiceInputView voiceInputView = _worldVoiceBtn.gameObject.AddComponent<MiniChatVoiceInputView>();
            voiceInputList.Add(voiceInputView);
            voiceInputView = _teamVoiceBtn.gameObject.AddComponent<MiniChatVoiceInputView>();
            voiceInputList.Add(voiceInputView);
            voiceInputView = _guildVoiceBtn.gameObject.AddComponent<MiniChatVoiceInputView>();
            voiceInputList.Add(voiceInputView);
            _voiceInputViewList = voiceInputList;

            _chatVoiceAgent = gameObject.AddComponent<ChatVoiceAgent>();
            _chatVoiceAgent.InitView(_voiceNoticeView, voiceInputList);

            for (int i = 0; i < _voiceInputViewList.Count; i++)
            {
                RectTransform btnRect = _voiceInputViewList[i].gameObject.GetComponent<RectTransform>();
                _channelBtnPosList[i] = btnRect.anchoredPosition;
            }
            StateImage btnBg = GetChildComponent<StateImage>("Container_bottomCenter/ScaleImage_Golddi");
            btnBg.Visible = false;

            if (ChatManager.Instance.IsCancelChatVoiceFunction())
            {
                _worldVoiceBtn.Visible = false;
                _teamVoiceBtn.Visible = false;
                _guildVoiceBtn.Visible = false;
            }
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(MainUIEvents.FAST_NOTICE_LIST_CHANGE, OnFastNoticeListChange);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.guild_id, OnPlayerGuildChanged);
            EventDispatcher.AddEventListener(SceneEvents.SCENE_LOADED, OnMapChanged);
            EventDispatcher.AddEventListener(TeamEvent.TEAM_REFRESH, OnTeamChanged);
            EventDispatcher.AddEventListener(TeamInstanceEvents.Teammate_Refresh, OnTeamChanged);
            EventDispatcher.AddEventListener(ChatEvents.OPEN_CHAT_PANEL, OnOpenChatPanel);
            EventDispatcher.AddEventListener(ChatEvents.CLOSE_CHAT_PANEL, OnCloseChatPanel);
            EventDispatcher.AddEventListener(MainUIEvents.ON_SHOW_MAIN_UI, OnShowMainUI);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(MainUIEvents.FAST_NOTICE_LIST_CHANGE, OnFastNoticeListChange);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.guild_id, OnPlayerGuildChanged);
            EventDispatcher.RemoveEventListener(SceneEvents.SCENE_LOADED, OnMapChanged);
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_REFRESH, OnTeamChanged);
            EventDispatcher.RemoveEventListener(TeamInstanceEvents.Teammate_Refresh, OnTeamChanged);
            EventDispatcher.RemoveEventListener(ChatEvents.OPEN_CHAT_PANEL, OnOpenChatPanel);
            EventDispatcher.RemoveEventListener(ChatEvents.CLOSE_CHAT_PANEL, OnCloseChatPanel);
            EventDispatcher.RemoveEventListener(MainUIEvents.ON_SHOW_MAIN_UI, OnShowMainUI);
        }

        private void OnFastNoticeListChange()
        {
            if (_bottomContainer.Visible  == false)
            {
                return;
            }
            List<FastNoticeData> dataList = PlayerDataManager.Instance.NoticeData.dataList;
            for (int i = 0; i < dataList.Count; i++)
            {
                _fastNoticeView.Show(dataList[i]);
            }
            if (dataList.Count == 0)
            {
                if (_fastNoticeView.HasNotice())
                {
                    _fastNoticeView.Show(null);
                }
            }
            PlayerDataManager.Instance.NoticeData.dataList.Clear();

        }

        private void OnPlayerGuildChanged()
        {
            DoLayoutChannelBtns();
        }

        private void OnMapChanged()
        {
            DoLayoutChannelBtns();
        }

        private void OnTeamChanged()
        {
            DoLayoutChannelBtns();
        }

        private void OnOpenChatPanel()
        {
            if (_bottomContainer.Visible)
            {
                _bottomContainer.Visible = false;
            }
        }

        private void OnCloseChatPanel()
        {
            ShowBottomContainer();
            OnFastNoticeListChange();
        }

        private void OnShowMainUI()
        {
            ShowBottomContainer();
            OnFastNoticeListChange();
        }

        private void ShowBottomContainer()
        {
            if (!_bottomContainer.Visible)
            {
                _bottomContainer.Visible = true;
            }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
            ShowBottomContainer();
            Refresh();
            OnFastNoticeListChange();
        }

        public override void OnEnterMap(int mapType)
        {
            base.OnEnterMap(mapType);
            OnFastNoticeListChange();
        }

        private void Refresh()
        {
            if (!_isInitVoiceInputViews)
            {
                _voiceInputViewList[0].Refresh(public_config.CHANNEL_ID_WORLD);
                _voiceInputViewList[1].Refresh(public_config.CHANNEL_ID_TEAM);
                _voiceInputViewList[2].Refresh(public_config.CHANNEL_ID_GUILD);
                _isInitVoiceInputViews = true;
            }
            DoLayoutChannelBtns();
        }

        private void DoLayoutChannelBtns()
        {
            if (!Visible)
                return;
            ChatData chatData = PlayerDataManager.Instance.ChatData;
            List<KButton> channelBtnOrderList = new List<KButton>();
            List<KButton> invisibleBtnList = new List<KButton>();
            //顺序：公会、世界、队伍
            if (PlayerAvatar.Player.guild_id != 0)
            {
                channelBtnOrderList.Add(_guildVoiceBtn);
            }
            else
            {
                invisibleBtnList.Add(_guildVoiceBtn);
            }
            channelBtnOrderList.Add(_worldVoiceBtn);
            if (chatData.IsPlayerInTeam())
            {
                channelBtnOrderList.Add(_teamVoiceBtn);
            }
            else
            {
                invisibleBtnList.Add(_teamVoiceBtn);
            }
            for (int i = 0; i < channelBtnOrderList.Count; i++)
            {
                if (ChatManager.Instance.IsCancelChatVoiceFunction())
                {
                    channelBtnOrderList[i].Visible = false;
                }
                else
                {
                    channelBtnOrderList[i].Visible = true;
                }
                RectTransform rect = channelBtnOrderList[i].gameObject.GetComponent<RectTransform>();
                rect.anchoredPosition = _channelBtnPosList[i];
            }
            for (int i = 0; i < invisibleBtnList.Count; i++)
            {
                invisibleBtnList[i].Visible = false;
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        protected override void OnDestroy()
        {
            _voiceInputViewList.Clear();
        }
    }
}
