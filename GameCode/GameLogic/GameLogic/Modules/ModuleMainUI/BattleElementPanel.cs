using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Game.UI.UIComponent;
using Common.Global;
using GameLoader.Utils.Timer;
using Common.Utils;
using Common.Base;
using MogoEngine.Events;
using Common.Events;
using GameMain.CombatSystem;
using GameMain.Entities;
using GameMain.GlobalManager;
using Common.Data;
using GameMain.PlayerAction;
using Common.Structs.ProtoBuf;
using Common.ServerConfig;
using Common.ExtendComponent;
using GameMain.GlobalManager.SubSystem;

namespace ModuleMainUI
{
    public class BattleElementPanel : BasePanel
    {
        private LifeBarList _lifeBarList;
        private DamageList _damageList;
        private CreatureInfoList _creatrueInfoList;
        private SpeechInfoList _speechInfoList;
        private AutoTextList _autoTextList;
        private LowHpView _lowHpView;
        private WayDirectionView _directionView;
        private LockTargetView _lockView;
        private BattleFloatTipList _floatTips;
        private IconContainer _teamDutyIcon;

        protected override void Awake()
        {
            _creatrueInfoList = AddChildComponent<CreatureInfoList>("Container_creatureInfoList");
            _speechInfoList = AddChildComponent<SpeechInfoList>("Container_speechInfoList");
            _lifeBarList = AddChildComponent<LifeBarList>("Container_lifeBarList");
            _damageList = AddChildComponent<DamageList>("Container_damageList");
            _autoTextList = AddChildComponent<AutoTextList>("Container_autoTextList");
            _lowHpView = AddChildComponent<LowHpView>("Container_lowHp");
            _directionView = AddChildComponent<WayDirectionView>("Container_direction");
            _lockView = AddChildComponent<LockTargetView>("Container_suoding");
            _floatTips = AddChildComponent<BattleFloatTipList>("Container_floatTip");
            _teamDutyIcon = AddChildComponent<IconContainer>("Container_teamDuty");
            InitListView();
            AddEventListener();
        }

        private void InitListView()
        {
            MogoGameObjectHelper.SetFullScreenSize(_creatrueInfoList.gameObject);
            MogoGameObjectHelper.SetFullScreenSize(_speechInfoList.gameObject);
            MogoGameObjectHelper.SetFullScreenSize(_lifeBarList.gameObject);
            MogoGameObjectHelper.SetFullScreenSize(_damageList.gameObject);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.BattleElement; }
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<uint>(BattleUIEvents.CREATE_LIFE_BAR_FOR_ENTITY, CreateLifeBar);
            EventDispatcher.AddEventListener<uint>(BattleUIEvents.REMOVE_LIFE_BAR_FOR_ENTITY, RemoveLifeBar);
            EventDispatcher.AddEventListener<Vector3, int, AttackType, bool>(BattleUIEvents.CREATE_DAMAGE_NUMBER_FOR_ENTITY, CreateDamageNumber);
            EventDispatcher.AddEventListener<uint, int>(BattleUIEvents.SHOW_SPEECH, CreateSpeechInfo);
            EventDispatcher.AddEventListener(BattleUIEvents.SHOW_FIND_PATH, ShowFindPath);
            EventDispatcher.AddEventListener(BattleUIEvents.HIDE_FIND_PATH, HideFindPath);
            EventDispatcher.AddEventListener<FightType>(PlayerEvents.ON_FIGHT_TYPE_CHANGED, OnChangeFightType);
            EventDispatcher.AddEventListener<List<PortalItemData>>(BattleUIEvents.SHOW_PORTAL_PANEL, ShowPortalPanel);
            EventDispatcher.AddEventListener<int,int>(BattleUIEvents.SHOW_FLOAT_TIP, ShowBattleFloatTip);
            EventDispatcher.AddEventListener<uint, uint>(EntityEvents.ON_MANUAL_LOCK_ID_CHANGED, ShowLockView);
            EventDispatcher.AddEventListener(TaskEvent.RING_CHANGE, OnRingChange);
            EventDispatcher.AddEventListener(TaskEvent.TEAM_TASK_STATE_CHANGE, OnRingChange);
        }

        

        private void ShowLockView(uint attackerId, uint lockedEntityId)
        {
            if (attackerId != PlayerAvatar.Player.id)
            {
                return;
            }
            if (lockedEntityId == 0)
            {
                _lockView.Visible = false;
            }
            else
            {
                _lockView.Visible = true;
            }
            _lockView.SetEntityId(lockedEntityId);
        }

        private void ShowPortalPanel(List<PortalItemData> dataList)
        {
            if (_findPathFlag == false || PlayerActionManager.GetInstance().currentAction is FindTransferPoint)
            {
                if(PlayerDataManager.Instance.TaskData.isTeammateInTeamTask() == false)
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.PortalPanel, dataList);
                }
            }
        }

        private void CreateLifeBar(uint entityId)
        {
            LifeBarManager.Instance.CreateLifeBar(entityId);
        }

        private void RemoveLifeBar(uint entityId)
        {
            LifeBarManager.Instance.RemoveLifeBar(entityId);
        }

        private void CreateDamageNumber(Vector3 position, int damage, AttackType type, bool isPlayer)
        {
            DamageManager.Instance.CreateDamageNumber(position, damage, type, isPlayer);
        }

        public void CreateSpeechInfo(uint entityId, int chineseId)
        {
            SpeechInfoManager.Instance.CreateSpeechInfo(entityId, chineseId);
        }

        private void ShowBattleFloatTip(int type, int value)
        {
            DamageManager.Instance.CreateVariableNumber(PlayerAvatar.Player.position, value, type);
        }

        private bool _findPathFlag = false;
        private void ShowFindPath()
        {
            _findPathFlag = true;
            RefreshAutoTextView();
        }

        private void HideFindPath()
        {
            _findPathFlag = false;
            RefreshAutoTextView();
        }

        private void OnChangeFightType(FightType obj)
        {
            RefreshAutoTextView();
        }

        private void OnRingChange()
        {
            RefreshAutoTextView();
        }

        private void RefreshAutoTextView()
        {
            PbTaskInfo taskData =  PlayerDataManager.Instance.TaskData.teamTaskInfo;
            int mapType = GameSceneManager.GetInstance().curMapType;
            bool isInShowTeamTaskRing = false;
            if(mapType == public_config.MAP_TYPE_WILD || mapType == public_config.MAP_TYPE_CITY || mapType == public_config.MAP_TYPE_SPACETIME_RIFT)
            {
                isInShowTeamTaskRing = true;
            }
            if (isInShowTeamTaskRing && PlayerDataManager.Instance.TaskData.ring > 0 && taskData != null && (taskData.status == public_config.TASK_STATE_ACCEPT || taskData.status == public_config.TASK_STATE_ACCOMPLISH))
            {
                AutoTextManager.Instance.ShowAutoText(AutoTextType.TeamTaskRing);
            }
            else if (_findPathFlag == true)
            {
                AutoTextManager.Instance.ShowAutoText(AutoTextType.AutoFindPath);
            }
            else
            {
                RefreshFightTypeText();
            }
        }

        private void RefreshFightTypeText()
        {

            if (PlayerAutoFightManager.GetInstance().fightType == FightType.AUTO_FIGHT)
            {
                AutoTextManager.Instance.ShowAutoText(AutoTextType.AutoBattle);
            }
            else if (PlayerAutoFightManager.GetInstance().fightType == FightType.FOLLOW_FIGHT)
            {
                AutoTextManager.Instance.ShowAutoText(AutoTextType.FollowBattle);
            }
            else
            {
                AutoTextManager.Instance.HideAutoText();
            }
        }

        public override void OnShow(object data)
        {
            RefreshAutoTextView();
            CreatureInfoManager.Instance.IsUpdate = true;
            RefresheBattleElementVisibility();
        }

        public override void OnClose()
        {
            CreatureInfoManager.Instance.IsUpdate = false;
        }

        public override void OnEnterMap(int mapId)
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.MaskPanel);
            RefresheBattleElementVisibility();
            RefreshAutoTextView();
        }

        private void RefresheBattleElementVisibility()
        {
            _lowHpView.Visible = GameSceneManager.GetInstance().inCombatScene;
            _directionView.Visible = GameSceneManager.GetInstance().inCombatScene;
            _lifeBarList.Visible = GameSceneManager.GetInstance().inCombatScene;
            _damageList.Visible = GameSceneManager.GetInstance().inCombatScene;
            _speechInfoList.Visible = GameSceneManager.GetInstance().inCombatScene;
        }

        public override void OnLeaveMap(int mapId)
        {
            LifeBarManager.Instance.LeaveMap();
            SpeechInfoManager.Instance.LeaveMap();
            CreatureInfoManager.Instance.LeaveMap();
        }

    }
}