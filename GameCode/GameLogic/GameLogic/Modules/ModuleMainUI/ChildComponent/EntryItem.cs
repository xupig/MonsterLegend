﻿#region 模块信息
/*==========================================
// 文件名：EntryItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleMainUI.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/27 14:27:21
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleMainUI
{
    public class EntryItem : KContainer
    {

       // private KButton _btnButton;
        protected StateIcon _iconContainer;

        protected function _function;

        protected RectTransform _transform;
        protected KShrinkableButton _shrinkableButton;

        protected static Quaternion quaternion = new Quaternion();
        protected static Vector3 vector = Vector3.one;

        protected int _num;

        private TweenPosition3D _tweener;

        private CanOperationEffect _effect;


        protected override void Awake()
        {
            _shrinkableButton = GetComponent<KShrinkableButton>();
            _iconContainer = GetChildComponent<StateIcon>("stateIcon");
            _transform = GetComponent<RectTransform>();
            _effect = gameObject.AddComponent<CanOperationEffect>();
            //GetChild("tishi").SetActive(false);
            _shrinkableButton.onClick.AddListener(OnClickHandler);
            base.Awake();
        }

        public virtual void SetInfo(function function,int num)
        {
            _num = num;
            _function = function;
            _transform.localRotation = quaternion;
            _transform.localScale = vector;
            _iconContainer.SetIcon(function.__icon);
            _transform.anchoredPosition3D = function_helper.GetPosition(function.__type, num);
            _shrinkableButton.RefreshRectTransform();
            gameObject.name = string.Format("Button_{0}", function.__id);
            _effect.SetPoint(PlayerDataManager.Instance.FunctionData.IsFunctionPoint((FunctionId)function.__id));
        }

        public void SetPoint(bool isPoint)
        {
            _effect.SetPoint(isPoint);
        }



        public void MoveTo(int num,bool isTween,float time)
        {
            _num = num;
            if(isTween)
            {
                if (_tweener!=null)
                {
                    _tweener.enabled = false;
                    
                    _shrinkableButton.RefreshRectTransform();
                    _shrinkableButton.IsShrinkable = true;
                    _tweener = null;
                }
                _shrinkableButton.IsShrinkable = false;
                _tweener = TweenPosition3D.Begin(gameObject, time, function_helper.GetPosition(_function.__type, num),delegate(UITweener tween)
                    {
                        _shrinkableButton.RefreshRectTransform();
                        _shrinkableButton.IsShrinkable = true;
                        _tweener = null;
                        EventDispatcher.TriggerEvent(PanelEvents.PANEL_LAYER_RESET_POS);
                    });
            }
            else
            {
                _transform.anchoredPosition3D = function_helper.GetPosition(_function.__type, num);
            }
        }


        private void OnClickHandler()
        {
            if (_function != null)
            {
                function_helper.Follow(_function.__id, null);
            }
        }


        public void TweenHide(float duration, int num, UITweener.OnFinished onFinished)
        {
            _shrinkableButton.IsShrinkable = false;
            if (Mathf.Abs(duration) < 0.01f)
            {
                _transform.localPosition = function_helper.GetStartPosition(_transform, _function.__type, num);
                onFinished(null);
                return;
            }
            TweenPosition3D.Begin(gameObject, duration, function_helper.GetStartPosition(_transform, _function.__type, num), delegate(UITweener tween)
            {
                onFinished(tween);
                _shrinkableButton.RefreshRectTransform();
                _shrinkableButton.IsShrinkable = true;
                EventDispatcher.TriggerEvent(PanelEvents.PANEL_LAYER_RESET_POS);
            });
        }

        public void TweenShow(float duration, UITweener.OnFinished onFinished)
        {
            _shrinkableButton.IsShrinkable = false;
            TweenPosition3D.Begin(gameObject, duration, function_helper.GetPosition(_function.__type, _num), delegate(UITweener tween)
            {
                onFinished(tween);
                _shrinkableButton.RefreshRectTransform();
                _shrinkableButton.IsShrinkable = true;
                EventDispatcher.TriggerEvent(PanelEvents.PANEL_LAYER_RESET_POS);
            });
        }
    }
}
