﻿using Common.ClientConfig;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleMainUI
{
    public class BattleMemberItem : KList.KListItemBase, IPointerEnterHandler, IPointerExitHandler
    {
        private KProgressBar _hpProgress;
        private StateText _avatarLevel;
        private StateText _avatarName;
        private KContainer _captainIcon;
        private IconContainer _avatarHeadIcon;
        private IconContainer _teamDutyIcon;
        private StateImage _selectImg;

        private PbHpMember _data;

        protected override void Awake()
        {
            _hpProgress = GetChildComponent<KProgressBar>("ProgressBar_hp");
            _avatarLevel = GetChildComponent<StateText>("Label_txtZuduixinxi1");
            _avatarName = GetChildComponent<StateText>("Label_txtZuduixinxi0");
            _captainIcon = GetChildComponent<KContainer>("Container_duizhang");
            _avatarHeadIcon = AddChildComponent<IconContainer>("Container_icon");
            _teamDutyIcon = AddChildComponent<IconContainer>("Container_teamDuty");
            _selectImg = GetChildComponent<StateImage>("Image_renwudiBg");
            _selectImg.Visible = false;
            AddEventListener();
        }

        public override void Dispose()
        {
            RemoveEventListener();
            _data = null;
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(TeamInstanceEvents.Teammate_Hp_Refresh, RefreshHpProgress);
            EventDispatcher.AddEventListener<uint, int>(TeamInstanceEvents.Teammate_Online_Refresh, RefreshOnlineContent);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(TeamInstanceEvents.Teammate_Hp_Refresh, RefreshHpProgress);
            EventDispatcher.RemoveEventListener<uint, int>(TeamInstanceEvents.Teammate_Online_Refresh, RefreshOnlineContent);
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as PbHpMember;
                    RefreshMemberInfo();
                }
            }
        }

        private void RefreshMemberInfo()
        {
            _avatarLevel.CurrentText.text = "LV" + _data.level.ToString();
            _avatarName.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.name);
            RefreshTeamDutyIcon();
            if (PlayerDataManager.Instance.TeamData.TeammateDic.Count > 0 && _data.dbid == PlayerDataManager.Instance.TeamData.CaptainId)
            {
                _captainIcon.Visible = true;
            }
            else
            {
                _captainIcon.Visible = false;
            }
            _avatarHeadIcon.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_data.vocation));
            RefreshHpProgress();
        }

        private void RefreshTeamDutyIcon()
        {
            if (_data.spell_proficient != 0)
            {
                _teamDutyIcon.SetIcon(skill_helper.GetTeamDutyIconId((Vocation)_data.vocation, (int)_data.spell_proficient));
            }
            else
            {
                _teamDutyIcon.RemoveSprite();
            }
        }

        private void RefreshHpProgress()
        {
            int hp = (int)_data.hp;
            int max_hp = (int)_data.max_hp;
            float value = 1;
            if (max_hp != 0)
            {
                value = (float)hp / max_hp;
            }
            _hpProgress.Value = value;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _selectImg.Visible = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _selectImg.Visible = false;
        }

        private void OnIconLoaded(GameObject go)
        {
            RefreshImageState();
        }

        private void RefreshOnlineContent(uint dbid, int online)
        {
            RefreshImageState();
        }

        private void RefreshImageState()
        {
            if (PlayerDataManager.Instance.TeamInstanceData.HpMemberDic.ContainsKey(_data.avatar_id))
            {
                PbHpMember memberInfo = PlayerDataManager.Instance.TeamInstanceData.HpMemberDic[_data.avatar_id];
                ImageWrapper[] wrappers = _avatarHeadIcon.gameObject.GetComponentsInChildren<ImageWrapper>();
                for (int i = 0; i < wrappers.Length; i++)
                {
                    wrappers[i].SetGray(memberInfo.online);
                }
            }
        }

    }
}
