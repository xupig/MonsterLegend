﻿using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleMainUI
{
    public class TownMemberItem : KList.KListItemBase, IPointerEnterHandler, IPointerExitHandler
    {
        private KProgressBar _hpProgress;
        private StateText _avatarLevel;
        private StateText _avatarName;
        private KContainer _captainIcon;
        private KContainer _avatarHeadIcon;
        private StateImage _selectImg;
        private StateImage _farImage;
        private IconContainer _teamDutyIcon;
        private PbTeamMember _data;

        protected override void Awake()
        {
            _hpProgress = GetChildComponent<KProgressBar>("ProgressBar_hp");
            _avatarLevel = GetChildComponent<StateText>("Label_txtZuduixinxi1");
            _avatarName = GetChildComponent<StateText>("Label_txtZuduixinxi0");
            _captainIcon = GetChildComponent<KContainer>("Container_duizhang");
            _avatarHeadIcon = GetChildComponent<KContainer>("Container_icon");
            _selectImg = GetChildComponent<StateImage>("Image_renwudiBg");
            _farImage = GetChildComponent<StateImage>("Image_imgyuan");
            _teamDutyIcon = AddChildComponent<IconContainer>("Container_teamDuty");
            //gameObject.AddComponent<KDummyButton>();
            _selectImg.Visible = false;
            AddEventListener();
        }

        public override void Dispose()
        {
            RemoveEventListener();
            _data = null;
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(TeamEvent.TEAM_MATE_HP_REFRESH, RefreshHpProgress);
            EventDispatcher.AddEventListener<ulong, int>(TeamEvent.TEAM_MATE_ONLINE_REFRESH, RefreshOnlineContent);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_MATE_HP_REFRESH, RefreshHpProgress);
            EventDispatcher.RemoveEventListener<ulong, int>(TeamEvent.TEAM_MATE_ONLINE_REFRESH, RefreshOnlineContent);
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as PbTeamMember;
                    RefreshMemberInfo();
                }
            }
        }

        private void RefreshMemberInfo()
        {
            _avatarLevel.CurrentText.text = "LV" + _data.level.ToString();
            _avatarName.CurrentText.text = _data.name;
            _farImage.Visible = PlayerDataManager.Instance.TeamData.IsTeammateInSameMap(_data.dbid) == false;
            if (PlayerDataManager.Instance.TeamData.TeammateDic.Count > 0 && _data.dbid == PlayerDataManager.Instance.TeamData.CaptainId)
            {
                _captainIcon.Visible = true;
            }
            else
            {
                _captainIcon.Visible = false;
            }
            MogoAtlasUtils.AddIcon(_avatarHeadIcon.gameObject, MogoPlayerUtils.GetSmallIcon((int)_data.vocation), OnIconLoaded);
            RefreshHpProgress();
            RefreshTeamDuty();
        }

        private int currentTeamDutyIcon = 0;
        public void RefreshTeamDuty()
        {
            if(_data.spell_proficient != 0)
            {
                _teamDutyIcon.Visible = true;
                if (skill_helper.GetTeamDutyIconId((Vocation)_data.vocation, (int)_data.spell_proficient) != currentTeamDutyIcon)
                {
                    currentTeamDutyIcon = skill_helper.GetTeamDutyIconId((Vocation)_data.vocation, (int)_data.spell_proficient);
                    _teamDutyIcon.RemoveSprite();
                    _teamDutyIcon.SetIcon(currentTeamDutyIcon);
                }
            }
            else
            {
                _teamDutyIcon.Visible = false;
            }
        }

        private void OnIconLoaded(GameObject go)
        {
            RefreshImageState();
        }

        private void RefreshOnlineContent(ulong dbid, int online)
        {
            RefreshImageState();
        }

        private void RefreshImageState()
        {
            if (_data == null) { return; }
            if (PlayerDataManager.Instance.TeamData.TeammateDic.ContainsKey(_data.dbid))
            {
                PbTeamMember memberInfo = PlayerDataManager.Instance.TeamData.TeammateDic[_data.dbid];
                ImageWrapper[] wrappers = _avatarHeadIcon.gameObject.GetComponentsInChildren<ImageWrapper>();
                for (int i = 0; i < wrappers.Length; i++)
                {
                    wrappers[i].SetGray(memberInfo.online);
                }
            }
        }

        private void RefreshHpProgress()
        {
            int hp = (int)_data.hp;
            int max_hp = (int)_data.max_hp;
            float value = 1;
            if (max_hp != 0)
            {
                value = (float)hp / max_hp;
            }
            _hpProgress.Value = value;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _selectImg.Visible = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _selectImg.Visible = false;
        }
    }
}
