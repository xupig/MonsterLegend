﻿#region 模块信息
/*==========================================
// 文件名：FunctionEntryItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleMainUI.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/12/28 14:58:11
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    public class FunctionEntryItem : KContainer
    {
        private StateIcon _iconContainer;
        //private StateImage _imgBg;
        private function _function;
        private KShrinkableButton _shrinkableButton;
        //private StateImage _imgLock;
        private CanOperationEffect _effect;
        private RectTransform _transform;
        public int type;
        public string Name;
        public int num;
        public int sequence;
        protected override void Awake()
        {
            _shrinkableButton = GetComponent<KShrinkableButton>();
            _iconContainer = GetChildComponent<StateIcon>("stateIcon");
            //_imgBg = GetChildComponent<StateImage>("zhuIcondi");
            _effect = gameObject.AddComponent<CanOperationEffect>();
            //_imgLock = GetChildComponent<StateImage>("suo03");
            _transform = GetComponent<RectTransform>();
            _shrinkableButton.onClick.AddListener(OnClickHandler);
            base.Awake();
        }

        public void SetInfo(function function, int num)
        {
            _function = function;
            gameObject.name = string.Format("Button_{0}", function.__id);
            Name = function.__name.ToLanguage();
            type = function.__type;
            sequence = function.__button_sequence;
            UpdateContent(num);
        }

        public void SetPoint(bool isPoint)
        {
            if (PlayerDataManager.Instance.FunctionData.IsFunctionOpen(_function.__id))
            {
                _effect.SetPoint(isPoint);
            }
        }

        public void UpdateContent(int num)
        {
            this.num = num;
            _transform.anchoredPosition = function_helper.GetPosition(_function.__type, num);
            _shrinkableButton.RefreshRectTransform();
            if (PlayerDataManager.Instance.FunctionData.IsFunctionOpen(_function.__id))
            {
               // _imgBg.Visible = true;
                //_iconContainer.Visible = true;
                _iconContainer.SetIcon(_function.__icon);
                _effect.SetPoint(PlayerDataManager.Instance.FunctionData.IsFunctionPoint((FunctionId)_function.__id));
                //_imgLock.Visible = false;
            }
            else
            {
               // _imgBg.Visible = false;
                //_iconContainer.Visible = false;
                _effect.SetPoint(false);
                _iconContainer.SetIcon(12138);
               // _imgLock.Visible = true;
            }
           
        }

        private void OnClickHandler()
        {
            if (_function != null && PlayerDataManager.Instance.FunctionData.IsFunctionOpen(_function.__id))
            {
                function_helper.Follow(_function.__id, null);
                PlayerDataManager.Instance.FunctionData.RemoveFunctionPoint(_function.__id);
            }
        }

    }
}
