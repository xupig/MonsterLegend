﻿#region 模块信息
/*==========================================
// 文件名：CanOperationEffect
// 命名空间: GameLogic.GameLogic.Modules.ModuleMainUI.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/9/14 21:26:42
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    public class CanOperationEffect:MonoBehaviour
    {
        private KParticle _noticeEffectGo;

        public void SetPoint(bool isPoint)
        {
            if (isPoint)
            {
                ShowEffect();
            }
            else
            {
                HideEffect();
            }
        }

        private void ShowEffect()
        {
            if (_noticeEffectGo == null)
            {
                GameObject template = FunctionComponent.pointGo;
                if (template == null)
                {
                    Debug.LogError("EntryItem光圈特效不存在");
                    return;
                }
                GameObject go = Instantiate(template) as GameObject;
                go.transform.SetParent(transform, false);
                go.transform.localScale = Vector3.one;
                _noticeEffectGo = go.AddComponent<KParticle>();
                _noticeEffectGo.Position = new Vector3(0, 0, -50);
                _noticeEffectGo.Stop();
            }
            if(_noticeEffectGo.Visible == false)
            {
                _noticeEffectGo.Play(true);
            }
           
        }

        private void HideEffect()
        {
            if (_noticeEffectGo != null)
            {
                _noticeEffectGo.Stop();
            }
        }
    }
}
