﻿using Game.UI.UIComponent;
using GameLoader.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    /// <summary>
    /// 用于显示vip等级
    /// </summary>
    public class VipLevelItem : KContainer
    {
        private List<GameObject> _stateImageNumList1;
        private List<GameObject> _stateImageNumList2;
        private int _currentVipLevel = -1;
        private KContainer _singleNumContainer;
        private KContainer _tenNumContainer;
        private RectTransform _vipTextImageRectTransform;

        private Vector3 vipImageLocalPosition;

        protected override void Awake()
        {
            _stateImageNumList1 = new List<GameObject>();
            _stateImageNumList2 = new List<GameObject>();
            GetAllNumStateImage("sharedlianji1", _stateImageNumList1);
            GetAllNumStateImage("sharedlianji2", _stateImageNumList2);
            _tenNumContainer = GetChildComponent<KContainer>("sharedlianji1");
            _singleNumContainer = GetChildComponent<KContainer>("sharedlianji2");
            vipImageLocalPosition = GetChild("imageVip").transform.localPosition;
            _vipTextImageRectTransform = GetChildComponent<RectTransform>("imageVip");
        }
        /// <summary>
        /// 获取所有数字图片,并隐藏
        /// </summary>
        private void GetAllNumStateImage(string prePath, List<GameObject> stateImageNumList)
        {
            string imageName;
            for (int index = 0; index < 10; index++)
            {
                imageName = prePath + "/Image_VIP0"+index.ToString();
                stateImageNumList.Add(transform.FindChild(imageName).gameObject);
            }
        }

        private void HideAllStateImage(List<GameObject> stateImageNumList)
        {
            for (int index = 0; index < 10; index++)
            {
                stateImageNumList[index].SetActive(false);
            }
        }

        public void SetVipLevel(int levelToSet)
        {
            if (levelToSet == _currentVipLevel)
                return;
            HideAllStateImage(_stateImageNumList1);
            HideAllStateImage(_stateImageNumList2);
            _currentVipLevel = levelToSet;
            RefreshGraphic();
        }

        /// <summary>
        /// 此处假设Vip不超过99级
        /// </summary>
        private void RefreshGraphic()
        {
            RectTransform imageList1Rect = GetChildComponent<RectTransform>("sharedlianji1");
            RectTransform imageList2Rect = GetChildComponent<RectTransform>("sharedlianji2");
           
            if (_currentVipLevel < 0)
                return;
            int singleNum = _currentVipLevel % 10;
            int tensNum = _currentVipLevel / 10;
            if (tensNum > 0)
            {
                _stateImageNumList1[tensNum].SetActive(true);
                _stateImageNumList2[singleNum].SetActive(true);
                RefreshLayout();
            }
            else
            {
                _stateImageNumList2[singleNum].SetActive(true);
                RefreshLayout();
            }
        }

        private void RefreshLayout()
        {
            if (_currentVipLevel >= 10)
            {
                _singleNumContainer.transform.localPosition = new Vector3(30, -11, 0);
                _tenNumContainer.transform.localPosition = new Vector3(19, -11, 0);
            }
            else
            {
                _singleNumContainer.transform.localPosition = new Vector3(24, -11, 0);
            }
        }

    }
}
