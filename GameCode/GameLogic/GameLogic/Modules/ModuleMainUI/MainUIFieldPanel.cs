﻿using Common.Base;
using Common.Events;
using Common.Global;
using Common.Utils;
using GameMain.GlobalManager;
using ModuleCommonUI;
using MogoEngine.Events;
using GameMain.Entities;
using Common.ServerConfig;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using MogoEngine.RPC;
using MogoEngine;
using GameData;
using ModuleTeam;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using UnityEngine;

namespace ModuleMainUI
{
    public class MainUIFieldPanel : BasePanel
    {
        private TeamEntryMainView _teamEntryView;
        private FieldTaskMainView _fieldTaskView;
        private FieldBattleView _battleView;
        private FieldTownView _townView;
        private FieldCommonView _commonView;
        private InformationView _infomationView;

        private MainUIFunctionEntryView _entryView;
        private PetView _petView;
        private KContainer _antiAddictionContainer;
        private int playerEnergy;

        private uint addiction_time = 0;

        protected override void Awake()
        {
            base.Awake();
            _teamEntryView = AddChildComponent<TeamEntryMainView>("Container_topRight/Container_pipeichuangkou");
            _fieldTaskView = AddChildComponent<FieldTaskMainView>("Container_topRight/Container_yewairenwu");
            _battleView = AddChildComponent<FieldBattleView>("Container_mainBattle");
            _townView = AddChildComponent<FieldTownView>("Container_mainTown");
            _commonView = AddChildComponent<FieldCommonView>("Container_common");
            _infomationView = AddChildComponent<InformationView>("Container_information");
            _entryView = AddChildComponent<MainUIFunctionEntryView>("Container_bottomRight");
            _petView = AddChildComponent<PetView>("Container_topCenter");
            _antiAddictionContainer = GetChildComponent<KContainer>("Container_fangchenmi");
            InitAntiContainer();
            playerEnergy = PlayerAvatar.Player.energy;
            WelfareActiveManager.Instance.GetBindMobileInfoReq();  //请求获取验证码信息
        }

        private StateText _titleTxt;
        private StateText _alreadyPlayTime;
        private KButton _antiBtn;
        private void InitAntiContainer()
        {
            _antiAddictionContainer.GetComponent<RectTransform>().anchoredPosition = new Vector2(1043, -346);
            _titleTxt = _antiAddictionContainer.GetChildComponent<StateText>("Label_txtGold");
            _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(123511);//MogoLanguageUtil.GetContent();
            _alreadyPlayTime = _antiAddictionContainer.GetChildComponent<StateText>("Label_txtShengyushijian");
            _antiBtn = _antiAddictionContainer.GetChildComponent<KButton>("Button_tianjia");
            _antiAddictionContainer.Visible = false;
        }

        public override void EnableCanvas()
        {
            base.EnableCanvas();
            if (_entryView.Visible)
            {
                _entryView.resizer.canResize = true;
            }
            if(isEntryViewShow)
            {
                _townView.CloseFunction();
                OnOpenFucntionEntryWithoutTween();
                isEntryViewShow = false;
            }
        }

        public override void DisableCanvas()
        {
            base.DisableCanvas();
            if (_entryView.Visible)
            {
                _entryView.resizer.canResize = false;
            }
            OnCloseFunctionEntry();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.MainUIField; }
        }

        public override void OnShow(object data)
        {
            _teamEntryView.Visible = false;
            RefreshFieldTaskView();
            AddEventListener();
            _infomationView.Show();
            _commonView.Visible = true;
            _townView.ShowPlayerTownView();
            _petView.Show();
            InitFieldUI();
            CheckAntiContainer();
            EventDispatcher.TriggerEvent(MainUIEvents.ON_SHOW_MAIN_UI);
        }

        private void CheckAntiContainer()
        {
            if (platform_helper.InPCPlatform())
            {
                if (AntiAddictionManager.Instance.NeedShowMessage == true)
                {
                    _antiAddictionContainer.Visible = true;
                }
                else
                {
                    _antiAddictionContainer.Visible = false;
                }
            }
            else
            {
                _antiAddictionContainer.Visible = false;
            }
        }

        public void InitFieldUI()
        {
            int mapType = GameSceneManager.GetInstance().curMapType;
            _townView.ShowActivityView();
            _commonView.ShowTaskView();
            _entryView.Visible = false;
            switch (mapType)
            {
                case public_config.MAP_TYPE_WILD:
                    RefreshFieldUI(FieldUIIdEnum.FieldBattleUI);
                    break;
                case public_config.MAP_TYPE_GUILD_WAIT_SINGLE_COPY:
                case public_config.MAP_TYPE_GUILD_WAIT_DOUBLE_COPY:
                    _townView.HideActivityView();
                    _commonView.HideTaskView();
                    RefreshFieldUI(FieldUIIdEnum.FieldTownUI);
                    break;
                case public_config.MAP_TYPE_DUEL:
                    _commonView.HideTaskView();
                    RefreshFieldUI(FieldUIIdEnum.FieldTownUI);
                    break;
                default:
                    RefreshFieldUI(FieldUIIdEnum.FieldTownUI);
                    break;
            }
            ActivityNoticeManager.Instance.CheckActivityNotice();
        }


        public override void OnClose()
        {
            _infomationView.Hide();
            RemoveEventListener();
            PanelIdEnum.MainUINotice.Close();
            PanelIdEnum.EquipUpgradeTips.Close();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<PanelIdEnum>(MainUIEvents.SHOW_TEAM_ENTRY_VIEW, OnShowTeamEntryView);
            EventDispatcher.AddEventListener(MainUIEvents.HIDE_TEAM_ENTRY_VIEW, OnHideTeamEntryView);
            EventDispatcher.AddEventListener<int>(TreasureEvent.ON_SHARE_SUCCEED, OnShareSucceed);
            EventDispatcher.AddEventListener(RPCEvents.RPCWait, OnRpcWait);
            EventDispatcher.AddEventListener(ChatEvents.OPEN_CHAT_PANEL, OnOpenChatPanel);
            EventDispatcher.AddEventListener(ChatEvents.CLOSE_CHAT_PANEL, OnCloseChatPanel);
            EventDispatcher.AddEventListener<uint>(TouchEvents.ON_TOUCH_ENTITY, OnShowPlayerInfo);
            EventDispatcher.AddEventListener(FieldTaskEvents.ON_AUTO_FIELD_TASK_STATE_CHANGE, RefreshFieldTaskView);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.owner_id, this.OnTeamTaskHangUp);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.energy, OnEnergyChanged);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.anti_addiction, OnAddictionStateChanged);
            _townView.onOpenFunctionEntry.AddListener(OnOpenFunctionEntry);
            _entryView.onCloseFunctionEntry.AddListener(OnCloseFunctionEntry);
            EventDispatcher.AddEventListener<int, int>(MainUIEvents.SHOWORHIDE_MAINUI_PART, ShowOrHideMainUIPart);
            EventDispatcher.AddEventListener<bool>(MainUIEvents.NEED_SHOW_ANTIADDICTION, ShowAntiAddictionTip);
            EventDispatcher.AddEventListener<int>(AntiAddictionEvents.ANTI_TIME_REFRESH, RefreshAlreadyPlayTime);
            _antiBtn.onClick.AddListener(OnAntiButtonClick);
        }

        private void OnAddictionStateChanged()
        {
            if (platform_helper.InPCPlatform())
            {
                if (PlayerAvatar.Player.anti_addiction == public_config.ANTI_ADDICTION_HAS_AGREE)
                {
                    _antiAddictionContainer.Visible = false;
                }
                else
                {
                    _antiAddictionContainer.Visible = true;
                }
            }
        }

        private void OnAntiButtonClick()
        {
            AntiDataWrapper wrapper = new AntiDataWrapper();
            wrapper.type = AntiPanelType.WriteMessage;
            UIManager.Instance.ShowPanel(PanelIdEnum.AntiAddiction, wrapper);
        }

        private void ShowAntiAddictionTip(bool needShowAntiTip)
        {
            if (platform_helper.InPCPlatform())
            {
                if (needShowAntiTip)
                {
                    _antiAddictionContainer.Visible = true;
                }
                else
                {
                    _antiAddictionContainer.Visible = false;
                }
            }
        }

        private void RefreshFieldTaskView()
        {
            _fieldTaskView.Visible = PlayerDataManager.Instance.TaskData.IsAutoWildTask;
        }

        private void RefreshAlreadyPlayTime(int playTime)
        {
            if (_antiAddictionContainer.Visible == true)
            {
                _alreadyPlayTime.CurrentText.text = string.Format("{0:D2}:{1:D2}:{2:D2}", playTime / 3600, (playTime / 60) % 60, playTime % 60);
                addiction_time++;
            }
        }

        private void ShowOrHideMainUIPart(int part, int showOrHide)
        {

        }

        private void OnEnergyChanged()
        {
            // 当用户的能量大于90且等级大于24的时候，获得体力时，提醒玩家去副本
            if (PlayerAvatar.Player.energy >= 90
                && PlayerAvatar.Player.energy > playerEnergy
                && PlayerAvatar.Player.level >= 24)
            {
                SystemInfoManager.Instance.Show(100019);
            }
            playerEnergy = PlayerAvatar.Player.energy;
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<PanelIdEnum>(MainUIEvents.SHOW_TEAM_ENTRY_VIEW, OnShowTeamEntryView);
            EventDispatcher.RemoveEventListener(MainUIEvents.HIDE_TEAM_ENTRY_VIEW, OnHideTeamEntryView);
            EventDispatcher.RemoveEventListener<int>(TreasureEvent.ON_SHARE_SUCCEED, OnShareSucceed);
            EventDispatcher.RemoveEventListener(RPCEvents.RPCWait, OnRpcWait);
            EventDispatcher.RemoveEventListener(ChatEvents.OPEN_CHAT_PANEL, OnOpenChatPanel);
            EventDispatcher.RemoveEventListener(ChatEvents.CLOSE_CHAT_PANEL, OnCloseChatPanel);
            EventDispatcher.RemoveEventListener<uint>(TouchEvents.ON_TOUCH_ENTITY, OnShowPlayerInfo);
            EventDispatcher.RemoveEventListener(FieldTaskEvents.ON_AUTO_FIELD_TASK_STATE_CHANGE, RefreshFieldTaskView);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.owner_id, this.OnTeamTaskHangUp);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.energy, OnEnergyChanged);
            _townView.onOpenFunctionEntry.RemoveListener(OnOpenFunctionEntry);
            _entryView.onCloseFunctionEntry.RemoveListener(OnCloseFunctionEntry);
            EventDispatcher.RemoveEventListener<int, int>(MainUIEvents.SHOWORHIDE_MAINUI_PART, ShowOrHideMainUIPart);
            EventDispatcher.RemoveEventListener<bool>(MainUIEvents.NEED_SHOW_ANTIADDICTION, ShowAntiAddictionTip);
            EventDispatcher.RemoveEventListener<int>(AntiAddictionEvents.ANTI_TIME_REFRESH, RefreshAlreadyPlayTime);
            _antiBtn.onClick.RemoveListener(OnAntiButtonClick);
        }

        private void OnShowTeamEntryView(PanelIdEnum panelId)
        {
            _teamEntryView.SetData(panelId);
        }

        private void OnHideTeamEntryView()
        {
            _teamEntryView.OnClose();
        }

        private void OnOpenFunctionEntry()
        {
            _entryView.Visible = true;
            _battleView.Visible = false;
            _petView.Visible = false;
            EventDispatcher.TriggerEvent<bool>(MainUIEvents.ON_FUNCTION_ENTRY_VIEW_OPEN, true);
            UIManager.Instance.ClosePanel(PanelIdEnum.WorldBossRanking);
            WorldBossRankManager.Instance.HideTheRankingPanel = true;
        }

        private void OnOpenFucntionEntryWithoutTween()
        {
            _entryView.OnShowWithOutTween();
            _battleView.Visible = false;
            _petView.Visible = false;
            EventDispatcher.TriggerEvent<bool>(MainUIEvents.ON_FUNCTION_ENTRY_VIEW_OPEN, true);
            UIManager.Instance.ClosePanel(PanelIdEnum.WorldBossRanking);
            WorldBossRankManager.Instance.HideTheRankingPanel = true;
        }

        private bool isEntryViewShow = false;

        private void OnCloseFunctionEntry()
        {
            if (isEntryViewShow == false)
            {
                isEntryViewShow = _entryView.Visible;
            }
            else
            {
                isEntryViewShow = false;
            }
            _entryView.Visible = false;
            _battleView.Visible = true;
            _petView.Visible = true;
            _petView.FieldChange();
            _townView.CloseFunctionEntry();
            EventDispatcher.TriggerEvent<bool>(MainUIEvents.ON_FUNCTION_ENTRY_VIEW_OPEN, false);
            WorldBossRankManager.Instance.HideTheRankingPanel = false;
        }

        private float _beforeOwnId = -1;
        private void OnTeamTaskHangUp()
        {
            if (_beforeOwnId == 0 && PlayerAvatar.Player.owner_id > 0)
            {
                PlayerActionManager.GetInstance().StopAllAction();
            }
            _beforeOwnId = (float)PlayerAvatar.Player.owner_id;
        }

        private void OnRpcWait()
        {
            ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.OPERATION_CD), PanelIdEnum.MainUIField);
        }

        private void OnShareSucceed(int reqShareCount)
        {
            if (reqShareCount == 0)
            {
                ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.SHARE_TREASURE_POSITION_SUCCEED));
            }
        }

        private void OnOpenChatPanel()
        {
            if (_commonView.Visible)
            {
                _commonView.Visible = false;
            }
            _townView.HidePlayerTownView();
        }

        private void OnCloseChatPanel()
        {
            if (!_commonView.Visible)
            {
                _commonView.Visible = true;
            }
            _townView.ShowPlayerTownView();
        }

        public void RefreshContent()
        {
            if (_townView.Visible)
            {
                _townView.Show();
            }
            if (_battleView.Visible)
            {
                _battleView.Show();
            }
        }

        private void RefreshFieldUI(FieldUIIdEnum fieldUIId)
        {
            if (fieldUIId == FieldUIIdEnum.FieldBattleUI)
            {
                _townView.Show();
                _battleView.Show();
            }
            else if (fieldUIId == FieldUIIdEnum.FieldTownUI)
            {
                _battleView.Hide();
                _townView.Show();
            }
        }

        public override void OnEnterMap(int mapId)
        {
            isEntryViewShow = false;
            InitFieldUI();
            if (GameSceneManager.GetInstance().CanShowNotice())
            {
                PlayerDataManager.Instance.FunctionData.CheckFunctionWaitList();
            }
        }

        public override void OnLeaveMap(int mapId)
        {
            isEntryViewShow = false;
            base.OnLeaveMap(mapId);
            _petView.OnLevelMap(mapId);
        }

        public void OnShowPlayerInfo(uint entityId)
        {
            if (entityId != PlayerAvatar.Player.dbid)
            {
                Entity entity = MogoWorld.GetEntity(entityId);
                if (entity == null) return;
                if (entity.entityType == EntityConfig.ENTITY_TYPE_AVATAR)
                {
                    EntityAvatar avatar = entity as EntityAvatar;
                    FriendManager.Instance.GetPlayerDetailInfo(avatar.name);
                }
                else if (entity.entityType == EntityConfig.ENTITY_TYPE_NAME_NPC)
                {
                    EntityNPC npc = entity as EntityNPC;
                    if (PlayerDataManager.Instance.FavorData.GetFavorData(npc.npc_id) != null && PlayerDataManager.Instance.FunctionData.IsFunctionOpen(FunctionId.favor) == true)
                    {
                        UIManager.Instance.ShowPanel(PanelIdEnum.Favor, npc.npc_id);
                    }
                    else
                    {
                        int dialog = npc_helper.GetRandomDialogID(npc.npc_id);
                        if (dialog != 0 && dialog != -1)
                        {
                            UIManager.Instance.ShowPanel(PanelIdEnum.NpcDailog, new int[] { npc.npc_id, 0 });
                        }
                    }
                }
            }
        }
    }

    public enum FieldUIIdEnum
    {
        FieldBattleUI = 1,
        FieldTownUI = 2,
    }
}
