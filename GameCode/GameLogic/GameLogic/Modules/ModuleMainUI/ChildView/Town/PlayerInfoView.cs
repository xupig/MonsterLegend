﻿using Common.Base;
using Common.ClientConfig;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using MogoEngine.RPC;
using UnityEngine;

namespace ModuleMainUI
{
    public class PlayerInfoView : KContainer
    {
        private KDummyButton _playerInfoHeadBtn;
        private KDummyButton _playerInfoHeadCloseBtn;
        private KContainer _playerInfoIcon;
        private StateText _txtPlayerInfoLevel;

        protected override void Awake()
        {
            _playerInfoIcon = GetChildComponent<KContainer>("Container_icon");
            _txtPlayerInfoLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            _playerInfoHeadBtn = _playerInfoIcon.gameObject.AddComponent<KDummyButton>();
            _playerInfoHeadCloseBtn = AddChildComponent<KDummyButton>("Container_hotArea");
            this.Visible = false;
        }

        private void AddEventListener()
        {
            _playerInfoHeadBtn.onClick.AddListener(OnPlayerInfoHeadClick);
            _playerInfoHeadCloseBtn.onClick.AddListener(OnClosePlayerInfo);
        }

        private void RemoveEventListener( )
        {
            _playerInfoHeadBtn.onClick.RemoveListener(OnPlayerInfoHeadClick);
            _playerInfoHeadCloseBtn.onClick.RemoveListener(OnClosePlayerInfo);
        }

        public void OnClosePlayerInfo()
        {
            this.Visible = false;
        }

        private string _queryName;
        private int _npcId = -1;
        public void OnShowPlayerInfo(uint entityId)
        {
            if(entityId != PlayerAvatar.Player.dbid)
            {
                Entity entity = MogoWorld.GetEntity(entityId);
                if (entity == null) return;
                if(entity.entityType == EntityConfig.ENTITY_TYPE_AVATAR)
                {
                    EntityAvatar avatar = entity as EntityAvatar;
                    _queryName = avatar.name;
                    _npcId = -1;
                    //MogoAtlasUtils.AddIcon(_playerInfoIcon.gameObject, MogoPlayerUtils.GetSmallIcon(avatar.vocation.ToInt()));
                    //_txtPlayerInfoLevel.CurrentText.text = avatar.level.ToString();
                    //this.Visible = true;
                    OnPlayerInfoHeadClick();

                }
                else if (entity.entityType == EntityConfig.ENTITY_TYPE_NAME_NPC)
                {
                    EntityNPC npc = entity as EntityNPC;
                    _npcId = npc.npc_id;
                    _queryName = null;
                    OnPlayerInfoHeadClick();

                    //if (npc_helper.GetNpcHeadIcon(npc.npc_id) != 0)
                    //{
                    //    //MogoAtlasUtils.AddIcon(_playerInfoIcon.gameObject, npc_helper.GetNpcHeadIcon(npc.npc_id));
                    //    //_txtPlayerInfoLevel.CurrentText.text = npc.level.ToString();
                    //    //this.Visible = true;
                    //}
                }

            }
        }

        private void OnPlayerInfoHeadClick()
        {
            if (_queryName != null)
            {
                FriendManager.Instance.GetPlayerDetailInfo(_queryName);
            }

            if (_npcId != -1)
            {
                if (PlayerDataManager.Instance.FavorData.GetFavorData(_npcId) != null && PlayerDataManager.Instance.FunctionData.IsFunctionOpen(FunctionId.favor) == true)
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.Favor, _npcId);
                }
                else
                {
                    int dialog = npc_helper.GetRandomDialogID(_npcId) ;
                    if (dialog != 0 && dialog!=-1)
                    {
                        UIManager.Instance.ShowPanel(PanelIdEnum.NpcDailog, new int[] { _npcId, 0 });
                    }
                }
            }
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }
      
    }
}
