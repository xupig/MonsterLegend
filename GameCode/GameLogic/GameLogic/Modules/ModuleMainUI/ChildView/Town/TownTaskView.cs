﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using UnityEngine;
using MogoEngine.Events;
using GameLoader.Utils;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using Common.Structs.ProtoBuf;
using GameData;
using ModuleTask.Handle;
using Common.Utils;
using Common.Global;
using Common.ServerConfig;
using GameMain.Entities;
using Common.ExtendComponent;
using Common.Data;

namespace ModuleMainUI
{
    public class TownTaskView : KContainer
    {

       // private KScrollView _scrollView;
        private KPageableList _pageableList;
        private PbTaskInfo _dailyTask;
        protected override void Awake()
        {
          //  _scrollView = GetChildComponent<KScrollView>("ScrollView_renwu");

            //野外psd中mask宽度太小，导致任务特效不能显示。
            //为了不修改资源，在代码中修改此mask大小，
           // _scrollView.Mask.GetComponent<RectTransform>().sizeDelta = new Vector2(272, 196);
           // _pageableList = _scrollView.GetChildComponent<KPageableList>("mask/content");
            _dailyTask = new PbTaskInfo();
            _dailyTask.task_id = -1;
            _pageableList = GetChildComponent<KPageableList>("List_content");
            _pageableList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _pageableList.SetPadding(3, 0, 0, 0);
           // _scrollView.ScrollRect.enabled = false;
        }

        protected override void OnDestroy( )
        {
            RemoveEventListener();
            base.OnDestroy();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            UpdateContent(true);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        
        public void UpdateContent(bool isProgressChange)
        {
            if(Visible == false)
            {
                return;
            }
            List<PbTaskInfo> taskfinalList = new List<PbTaskInfo>();
            List<PbTaskInfo> taskList = new List<PbTaskInfo>();
            
            Dictionary<int, PbTaskInfo> branchDict = PlayerDataManager.Instance.TaskData.branchTaskDic;
            foreach (KeyValuePair<int, PbTaskInfo> kvp in branchDict)
            {
                taskList.Add(kvp.Value);
            }
            taskList.Sort(SortFunc);

            
            if(wild_task_helper.HasWildTask() == false)
            {
                if (PlayerDataManager.Instance.FunctionData.IsFunctionOpen(FunctionId.dailyactive))
                {
                    int dailyTaskCount = GetDailyTask();
                    if (dailyTaskCount < PlayerDataManager.Instance.RewardData.GetDailyActiveData().DailyActiveList.Count)
                    {
                        _dailyTask.status = dailyTaskCount;
                        taskList.Insert(0, _dailyTask);
                    }
                }
            }

            if (PlayerDataManager.Instance.TaskData.mainTaskInfo != null)
            {
                taskList.Insert(0,PlayerDataManager.Instance.TaskData.mainTaskInfo);
            }
            int count = Math.Min(3, taskList.Count);
            for(int i=0;i<count;i++)
            {
                taskfinalList.Add(taskList[i]);
            }
            //Debug.LogError(PlayerDataManager.Instance.TaskData.mainTaskInfo + "," + wild_task_helper.HasWildTask() + "," + branchDict.Count+","+taskfinalList.Count);
            _pageableList.SetDataList<TownTaskItem>(taskfinalList);
        }

        private int GetDailyTask()
        {
            int count = 0;
            List<BaseDailyTaskItemData> dataList = PlayerDataManager.Instance.RewardData.GetDailyActiveData().DailyActiveList;
            for(int i=0;i<dataList.Count;i++)
            {
                DailyActiveItemData dailyActiveItemData = dataList[i] as DailyActiveItemData;
                if(dailyActiveItemData.Status == (int)DailyActiveStatus.Complete)
                {
                    count++;
                }
            }
            return count;
        }

        private int SortFunc(PbTaskInfo task1, PbTaskInfo task2)
        {
            if (task2.status == public_config.TASK_STATE_ACCOMPLISH)
            {
                return 1;
            }
            if (task1.status == public_config.TASK_STATE_ACCOMPLISH)
            {
                return -1;
            }
            task_data data1 = task_data_helper.GetTaskData(task1.task_id);
            task_data data2 = task_data_helper.GetTaskData(task2.task_id);
            if(data2.__task_type == public_config.TASK_TYPE_GUILD)
            {
                return 1;
            }
            if (data1.__task_type == public_config.TASK_TYPE_GUILD)
            {
                return -1;
            }
            return task2.task_id - task1.task_id;
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<bool>(TaskEvent.TASK_MAIN_TASK_CHANGE, UpdateContent);
            EventDispatcher.AddEventListener<bool>(TaskEvent.TASK_BRANCH_TASK_CHANGE, UpdateContent);
        }

        private void RemoveEventListener( )
        {
            EventDispatcher.RemoveEventListener<bool>(TaskEvent.TASK_MAIN_TASK_CHANGE, UpdateContent);
            EventDispatcher.RemoveEventListener<bool>(TaskEvent.TASK_BRANCH_TASK_CHANGE, UpdateContent);
        }

    }
}
