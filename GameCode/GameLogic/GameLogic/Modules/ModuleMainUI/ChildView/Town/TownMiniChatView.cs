﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using UnityEngine;
using MogoEngine.Events;
using GameLoader.Utils;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using Common.Structs.ProtoBuf;
using Common.ServerConfig;
using Common.Structs;
using GameMain.GlobalManager.SubSystem;
using Common.ExtendComponent;
using GameLoader.Utils.Timer;
using Common.Utils;
using GameMain.Entities;
using Common.Chat;
using UnityEngine.UI;
using Game.UI;
using Common.Data;

namespace ModuleMainUI
{
    public class TownMiniChatView : KContainer
    {
        private KButton _buttonOpenChat;
        private StateText _stateTextContent;
        private StateImage _stateImagePoint;
        private GameObject _maskGo;
        private MiniChatTextBlock _newTextBlock;
        private MiniChatTextBlock _oldTextBlock;

        private ChatData _chatData;
        private float _bgImgHeight = 0f;
        private Queue<PbChatInfoResp> _chatInfoQueue = new Queue<PbChatInfoResp>();  //等待显示的消息
        private uint _timerId = 0;
        private bool _isTweeningMsg = false;
        
        private const float MOVE_SPEED = 60.0f;
        private const int NEW_MSG_STAY_TIME = 2000;
        private const int CHAT_MSG_LIMIT_LENGTH = 12;
        private const float TEXT_LINE_MAX_WIDTH = 450;

        protected override void Awake()
        {
            _chatData = PlayerDataManager.Instance.ChatData;
            _buttonOpenChat = gameObject.GetComponent<KButton>();
            _stateTextContent = GetChildComponent<StateText>("txtDuihua");
            _stateTextContent.gameObject.AddComponent<RaycastComponent>();
            _stateImagePoint = GetChildComponent<StateImage>("Spotlight");
            _stateImagePoint.gameObject.SetActive(false);
            _stateTextContent.RecalculateAllStateHeight();
            GameObject maskGo = GetChild("mask");
            _stateTextContent.transform.SetParent(maskGo.transform);
            _maskGo = maskGo;
            StateImage bgImg = GetChildComponent<StateImage>("ltXianshikuang");
            _bgImgHeight = bgImg.GetComponent<RectTransform>().sizeDelta.y;
            _stateTextContent.ChangeAllStateText(MogoLanguageUtil.GetContent(32543));
            ResizeTextContent();
            RectTransform textContentRect = _stateTextContent.GetComponent<RectTransform>();
            textContentRect.anchoredPosition = new Vector2(textContentRect.anchoredPosition.x, -23f);
            AddEventListener();
        }

        private void ResizeTextContent()
        {
            Text[] texts = _stateTextContent.GetAllStateTextComponent();
            RectTransform textRect = _stateTextContent.GetComponent<RectTransform>();
            float width = 0;
            for (int i = 0; i < texts.Length; i++)
            {
                RectTransform rect = texts[i].GetComponent<RectTransform>();
                if (rect.sizeDelta.x < texts[i].preferredWidth)
                {
                    float x = Mathf.CeilToInt(texts[i].preferredWidth);
                    TextWrapper wrapper = texts[i] as TextWrapper;
                    wrapper.SetDimensionsDirty();
                    rect.sizeDelta = new Vector2(x, rect.sizeDelta.y);
                }
                if (width < rect.sizeDelta.x)
                {
                    width = rect.sizeDelta.x;
                }
            }
            textRect.sizeDelta = new Vector2(width, textRect.sizeDelta.y);
        }

        protected void AddEventListener()
        {
            _buttonOpenChat.onClick.AddListener(OnOpenChatClick);
            EventDispatcher.AddEventListener<int, PbChatInfoResp>(ChatEvents.RECEIVE_CONTENT, OnReceiveContent);
            EventDispatcher.AddEventListener<int>(ChatEvents.ReadContent, OnReadContent);
            EventDispatcher.AddEventListener<int, int>(MainUIEvents.SHOWORHIDE_MAIN_CHAT_PART, OnShowOrHide);
            EventDispatcher.AddEventListener(ChatEvents.QUIT_TEAM, OnUpdateNewMsgFlagQuitTeam);
            EventDispatcher.AddEventListener(ChatEvents.LEAVE_GUILD, OnUpdateNewMsgFlagLeaveGuild);
        }

        protected void RemoveEventListener()
        {
            _buttonOpenChat.onClick.RemoveListener(OnOpenChatClick);
            EventDispatcher.RemoveEventListener<int, PbChatInfoResp>(ChatEvents.RECEIVE_CONTENT, OnReceiveContent);
            EventDispatcher.RemoveEventListener<int>(ChatEvents.ReadContent, OnReadContent);
            EventDispatcher.RemoveEventListener<int, int>(MainUIEvents.SHOWORHIDE_MAIN_CHAT_PART, OnShowOrHide);
            EventDispatcher.RemoveEventListener(ChatEvents.QUIT_TEAM, OnUpdateNewMsgFlagQuitTeam);
            EventDispatcher.RemoveEventListener(ChatEvents.LEAVE_GUILD, OnUpdateNewMsgFlagLeaveGuild);
        }

        private void OnOpenChatClick()
        {
            if(MainUIFunctionEntryView.isVisible)
            {
                EventDispatcher.AddEventListener(MainUIFunctionEntryView.CLOSE_END,OnCallBack);
                EventDispatcher.TriggerEvent(MainUIFunctionEntryView.CLOSE_FUNCTION_ENTRY_VIEW);
            }
            else
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.Chat);
            }
            
        }

        private void OnCallBack()
        {
            EventDispatcher.RemoveEventListener(MainUIFunctionEntryView.CLOSE_END, OnCallBack);
            UIManager.Instance.ShowPanel(PanelIdEnum.Chat);
        }

        private void OnReceiveContent(int channelId, PbChatInfoResp chatInfo)
        {
            ShowAContent(chatInfo);
        }

        private void OnReadContent(int channelId)
        {
            CheckHasNewInfo();
        }

        private void OnShowOrHide(int param1, int param2)
        {
            bool isShow = param2 == 1 ? true : false;
            switch (param1)
            {
                case 1:
                    _stateTextContent.Visible = isShow;
                    break;
                case 2:
                    _maskGo.SetActive(isShow);
                    break;
                case 3:
                    if (_newTextBlock != null && _newTextBlock.GameObject != null)
                    {
                        _newTextBlock.GameObject.SetActive(isShow);
                    }
                    break;
                default:
                    break;
            }
        }

        private void OnUpdateNewMsgFlagQuitTeam()
        {
            _chatData.ProcessNewMsgFlagNotInTeam();
        }

        private void OnUpdateNewMsgFlagLeaveGuild()
        {
            _chatData.ProcessNewMsgFlagNotInGuild();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            if (!_isTweeningMsg && _chatInfoQueue.Count > 0)
            {
                ShowNextMsg();
            }
            _chatData.ProcessNewMsgFlagNotInTeam();
            _chatData.ProcessNewMsgFlagNotInGuild();
            CheckHasNewInfo();
        }

        public void CheckHasNewInfo()
        {
            _stateImagePoint.gameObject.SetActive(ChatManager.Instance.HasNewChatInfo());
        }

        public void HideNewMsgPoint()
        {
            _stateImagePoint.Visible = false;
        }

        public void ShowAContent(PbChatInfoResp chatInfo)
        {
            if (chatInfo.msg_type == (int)ChatContentType.RedEnvelope)
                return;
            _stateTextContent.Visible = false;
            //如果隐藏，就不再进行动画，直接设置为聊天内容
            if (gameObject.activeInHierarchy == false || Common.States.GMState.openMainChatAnim == false)
            {
                SetMsgWithoutAnimation(chatInfo);
                if (_chatInfoQueue.Count > 0)
                {
                    _chatInfoQueue.Clear();
                }
                _chatInfoQueue.Enqueue(chatInfo);
                return;
            }
            _chatInfoQueue.Enqueue(chatInfo);
            if (!_isTweeningMsg)
            {
                PbChatInfoResp oneChatInfo = _chatInfoQueue.Peek();
                StartMsgSwitchAnimation(oneChatInfo);
                _chatInfoQueue.Dequeue();
            }
            CheckHasNewInfo();
            if (!_isTweeningMsg)
            {
                ShowNextMsg();
            }
        }

        private void StartMsgSwitchAnimation(PbChatInfoResp chatInfo)
        {
            _isTweeningMsg = true;
            //将以前的新消息作为旧消息
            if (_newTextBlock != null)
            {
                _oldTextBlock = _newTextBlock;
                _newTextBlock = null;
            }
            else
            {
                _newTextBlock = CreateMiniTextBlock(chatInfo);
                SetTextPosition(_newTextBlock.GameObject, GetTextShowPosition(_newTextBlock.GameObject));
                _isTweeningMsg = false;
                return;
            }

            //移动新的消息
            _newTextBlock = CreateMiniTextBlock(chatInfo);
            SetTextPosition(_newTextBlock.GameObject, GetNewTextMoveSrcPos(_newTextBlock.GameObject));
            TweenTextPosition(_newTextBlock.GameObject, GetTextShowPosition(_newTextBlock.GameObject), OnNewTextMoveFinished);

            //移动旧的消息
            TweenTextPosition(_oldTextBlock.GameObject, GetOldTextMoveDestPos(_oldTextBlock.GameObject), OnOldTextMoveFinished);
        }

        private void SetMsgWithoutAnimation(PbChatInfoResp chatInfo)
        {
            if (_isTweeningMsg)
            {
                ResetTimer();
                _isTweeningMsg = false;
            }
            if (_oldTextBlock != null)
            {
                if (_oldTextBlock.GameObject != null)
                {
                    Destroy(_oldTextBlock.GameObject);
                }
                _oldTextBlock = null;
            }
            if (_newTextBlock != null)
            {
                if (_newTextBlock.GameObject != null)
                {
                    Destroy(_newTextBlock.GameObject);
                }
                _newTextBlock = null;
            }
        }

        private void TweenTextPosition(GameObject textGo, Vector2 destPos, TweenPosition.OnFinished moveFinished)
        {
            TweenPosition textTweenPos = textGo.AddComponent<TweenPosition>();
            Vector2 textSrcPos = textGo.GetComponent<RectTransform>().anchoredPosition;
            Vector2 textDestPos = destPos;
            textTweenPos.from = textSrcPos;
            textTweenPos.to = textDestPos;
            float deltaY = textDestPos.y - textSrcPos.y;
            textTweenPos.duration = deltaY / MOVE_SPEED;
            textTweenPos.onFinished = moveFinished;
            textTweenPos.Play(true);
        }

        private void SetTextPosition(GameObject textGo, Vector2 pos)
        {
            RectTransform textRect = textGo.GetComponent<RectTransform>();
            textRect.anchoredPosition = pos;
        }

        private Vector2 GetTextShowPosition(GameObject textGo)
        {
            Vector2 pos = Vector2.zero;
            RectTransform rect = textGo.GetComponent<RectTransform>();
            if (textGo.transform.childCount <= 1)
            {
                pos = new Vector2(rect.anchoredPosition.x, -23f);
            }
            else
            {
                pos = new Vector2(rect.anchoredPosition.x, -11f);
            }
            return pos;
        }

        private Vector2 GetNewTextMoveSrcPos(GameObject textGo)
        {
            Vector2 srcPos = Vector2.zero;
            RectTransform rect = textGo.GetComponent<RectTransform>();
            srcPos.x = rect.anchoredPosition.x;
            srcPos.y = -_bgImgHeight;
            return srcPos;
        }

        private Vector2 GetOldTextMoveDestPos(GameObject textGo)
        {
            Vector2 destPos = Vector2.zero;
            RectTransform rect = textGo.GetComponent<RectTransform>();
            destPos.x = rect.anchoredPosition.x;
            destPos.y = rect.sizeDelta.y;
            return destPos;
        }

        private MiniChatTextBlock CreateMiniTextBlock(PbChatInfoResp chatInfo)
        {
            MiniChatTextBlock miniTextBlock = null;
            float maxWidth = 22.0f * TEXT_LINE_MAX_WIDTH / 18.0f;
            if (ChatInfoHelper.isSystem(chatInfo))
            {
                miniTextBlock = MiniChatTextBlock.CreateMiniBlock(chatInfo, maxWidth, ChatTextBlock.COLOR_SYSTEM_WORLD_CANNEL);
            }
            else
            {
                if (ChatManager.Instance.IsSystemTypeChannelMsg(chatInfo))
                {
                    miniTextBlock = MiniChatTextBlock.CreateMiniBlock(chatInfo, maxWidth, ChatTextBlock.COLOR_SYSTEM_WORLD_CANNEL);
                }
                else
                {
                    miniTextBlock = MiniChatTextBlock.CreateMiniBlock(chatInfo, maxWidth, ChatTextBlock.GetChannelFontColor((int)chatInfo.channel_id));
                }
            }
            miniTextBlock.GameObject.transform.SetParent(_maskGo.transform);
            float scale = 1.0f * 18 / 22.0f;
            miniTextBlock.GameObject.transform.localScale = new Vector3(scale, scale, 1);
            miniTextBlock.GameObject.transform.localPosition = new Vector3(16f, 0f, 0f);
            RectTransform rect = _maskGo.GetComponent<RectTransform>();
            RectTransform rectBlock = miniTextBlock.GameObject.GetComponent<RectTransform>();
            return miniTextBlock;
        }

        private GameObject CloneTextGameObject()
        {
            GameObject textGo = GameObject.Instantiate(_stateTextContent.gameObject) as GameObject;
            textGo.transform.SetParent(_stateTextContent.transform.parent);
            textGo.name = _stateTextContent.gameObject.name + "_old";
            textGo.transform.localScale = Vector3.one;
            Vector3 anchorPos = _stateTextContent.GetComponent<RectTransform>().anchoredPosition;
            textGo.transform.localPosition = new Vector3(anchorPos.x, anchorPos.y, 0f);
            return textGo;
        }

        private void OnNewTextMoveFinished(UITweener tween)
        {
            Destroy(tween);
            ResetTimer();
            if (_timerId == 0)
            {
                _timerId = TimerHeap.AddTimer(NEW_MSG_STAY_TIME, 0, ShowNextMsg);
            }
        }

        private void OnOldTextMoveFinished(UITweener tween)
        {
            _oldTextBlock = null;
            Destroy(tween.gameObject);
        }

        private void ShowNextMsg()
        {
            _isTweeningMsg = false;
            if (_chatInfoQueue.Count > 0)
            {
                PbChatInfoResp oneChatInfo = _chatInfoQueue.Peek();
                StartMsgSwitchAnimation(oneChatInfo);
                _chatInfoQueue.Dequeue();
            }
            ResetTimer();
            CheckHasNewInfo();
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            _isTweeningMsg = false;
            _chatInfoQueue.Clear();
            ResetTimer();
        }
    }
}
