﻿#region 模块信息
/*==========================================
// 文件名：TownTaskItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleMainUI.ChildView.Town
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/9/9 20:43:36
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleReward;
using ModuleTask;
using ModuleTask.Handle;
using Mogo.Util;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleMainUI
{
    public class TownTaskItem : KList.KListItemBase, IPointerEnterHandler, IPointerExitHandler
    {
        private static string _taskTargetContent;
        private const string _progressContent = "({0}/{1})";

        private KParticle _taskParticle;

        private StateImage _taskFinished;

        private StateText _txtTaskName;
        private StateText _txtTaskContent;

        private StateImage _selectImg;

        private PbTaskInfo _taskInfo;

        //private KDummyButton _btnClick;
        //private MaskEffectItem effect;

        private int _levelLimit;
        private static HtmlUtils _htmlUtils;

        protected override void Awake()
        {
            if (_htmlUtils == null)
            {
                _htmlUtils = new HtmlUtils();
            }
            _txtTaskName = GetChildComponent<StateText>("Label_title");
            _txtTaskContent = GetChildComponent<StateText>("Label_content");
            _taskFinished = GetChildComponent<StateImage>("Image_mainSharedxuanze02");

            _selectImg = GetChildComponent<StateImage>("Image_renwudiBg");
            _selectImg.Visible = false;


            _taskParticle = AddChildComponent<KParticle>("fx_ui_3_1");
            //effect = gameObject.AddComponent<MaskEffectItem>();
            //effect.Init(gameObject, Callback, true);
            _taskParticle.Stop();
            if (_taskTargetContent == null)
            {
                _taskTargetContent = _txtTaskContent.CurrentText.text;
            }
            //_btnClick = gameObject.AddComponent<KDummyButton>();
            // _btnClick.onClick.AddListener(OnClick);
        }

        private bool Callback()
        {
            if (_taskInfo == null)
            {
                return false;
            }
            task_data data = task_data_helper.GetTaskData(_taskInfo.task_id);
            if (_taskInfo.status == public_config.TASK_STATE_NEW)
            {
                return true;
            }
            else if (_taskInfo.status == public_config.TASK_STATE_ACCOMPLISH || (_taskInfo.status == public_config.TASK_STATE_ACCEPT && (data.__target == null || data.__target.Count == 0)))
            {
                return true;
            }
            else
            {
                if (PlayerAvatar.Player.level < 40 && data.__task_type == public_config.TASK_TYPE_MAIN)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private void OnClickStart()
        {
            if (PlayerDataManager.Instance.TeamData.TeamId == 0)//没有队伍
            {
                MogoUtils.FloatTips(115021);//没有队伍
                return;
            }
            if (PlayerDataManager.Instance.TeamData.CaptainId != PlayerAvatar.Player.dbid)//不是队长
            {
                MogoUtils.FloatTips(115000);
                return;
            }
            if (PlayerDataManager.Instance.TaskData.ring == 0)
            {
                TaskManager.Instance.StartTeamTask();
            }
            else if (PlayerDataManager.Instance.TaskData.teamTaskInfo == null || PlayerDataManager.Instance.TaskData.teamTaskInfo.status == public_config.TASK_STATE_NEW)
            {
                TaskManager.Instance.ReceiveTeamTask(PlayerDataManager.Instance.TaskData.task_id);
            }
            else if (PlayerDataManager.Instance.TaskData.teamTaskInfo.status == public_config.TASK_STATE_ACCOMPLISH)
            {
                task_data taskData = task_data_helper.GetTaskData(PlayerDataManager.Instance.TaskData.teamTaskInfo.task_id);
                EntityNPC npc = NPCManager.GetInstance().FindNPCByNPCID(taskData.__npc_id_report);
                if (taskData.__npc_id_report == 0 || taskData.__npc_id_report == 999 || (npc != null && npc.IsNearbyPlayer() == true))
                {
                    TaskManager.Instance.ReceiveTeamTaskReward();
                }
                else
                {
                    EventDispatcher.TriggerEvent(PlayerCommandEvents.FIND_NPC, taskData.__npc_id_report, true);
                }
            }
            else
            {
                //if (PlayerDataManager.Instance.TaskData.isTeammateAllReady == false)
                //{
                //    //还没准备好
                //    MogoUtils.FloatTips(115022);//
                //    return;
                //}
                if (GameSceneManager.GetInstance().curInstanceID == PlayerDataManager.Instance.TaskData.task_place_Id)
                {
                    Vector3 pos = PlayerDataManager.Instance.TaskData.task_place_pos;
                    var dis = Vector3.Distance(PlayerAvatar.Player.position, pos);

                    if (dis < 0.5f)
                    {
                        TaskModule.EnterTeamTaskMission();
                        return;
                    }
                    EventDispatcher.TriggerEvent(PlayerCommandEvents.FIND_POSITION, PlayerDataManager.Instance.TaskData.task_place_pos);
                }
                else
                {
                    SharePostionData sharedData = new SharePostionData();
                    sharedData.mapId = PlayerDataManager.Instance.TaskData.task_place_Id;
                    sharedData.type = 1;
                    sharedData.targetPosition = PlayerDataManager.Instance.TaskData.task_place_pos;
                    sharedData.mapLine = -1;
                    EventDispatcher.TriggerEvent<SharePostionData>(PlayerCommandEvents.FIND_SHARE_POSITION, sharedData);
                }
            }
        }

        public override void OnPointerClick(UnityEngine.EventSystems.PointerEventData evtData)
        {
            base.OnPointerClick(evtData);
            if (_taskInfo != null)
            {
                if (_taskInfo.task_id == -1)
                {
                    PanelIdEnum.Reward.Show(RewardViewType.DAILY_ACTIVE_TASK_VIEW);
                    //
                }
                else
                {
                    task_data data = task_data_helper.GetTaskData(_taskInfo.task_id);
                    if (data.__task_type == public_config.TASK_TYPE_TEAM)
                    {
                        OnClickStart();
                    }
                    else if (data.__task_type == public_config.TASK_TYPE_WILD)
                    {
                        FieldTaskManager.Instance.ShowAutoTaskMessageBox();
                    }
                    else
                    {
                        if (PlayerDataManager.Instance.TaskData.isTeammateInTeamTask())
                        {
                            MogoUtils.FloatTips(115032);
                            return;
                        }
                        if (PlayerAvatar.Player.level < _levelLimit)
                        {
                            UIManager.Instance.ShowPanel(PanelIdEnum.LevelUpChannel);
                            return;
                        }
                        TaskFollowHandle.TaskFollow(data);
                    }
                }
            }
        }

        public override void Hide()
        {
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, OnLevelChange);
            base.Hide();
        }

        public override void Show()
        {
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, OnLevelChange);
            base.Show();
        }

        private void OnLevelChange()
        {
            if (_levelLimit > 0 && PlayerAvatar.Player.level >= _levelLimit)
            {
                Refresh();
            }
        }


        public override object Data
        {
            get
            {
                return _taskInfo;
            }
            set
            {
                _taskInfo = value as PbTaskInfo;
                Refresh();
            }
        }

        private void OnRingChange()
        {
            Refresh();
        }

        private void Refresh()
        {
            if (_taskInfo != null)
            {
                if(_taskInfo.task_id == -1)
                {
                    RefreshDailyTask();
                }
                else
                {
                    RefreshTask();
                }
            }
            Visible = _taskInfo != null;
           // effect.UpdateMaskEffect();
        }

        private void RefreshDailyTask()
        {
            _taskFinished.Visible = false;
            _txtTaskName.CurrentText.text = (106603).ToLanguage();
            _txtTaskContent.ChangeAllStateText((106604).ToLanguage(_taskInfo.status, PlayerDataManager.Instance.RewardData.GetDailyActiveData().DailyActiveList.Count));
        }

        private void RefreshTask()
        {
            EventDispatcher.RemoveEventListener(TaskEvent.RING_CHANGE, OnRingChange);
            _taskFinished.Visible = false;
            task_data data = task_data_helper.GetTaskData(_taskInfo.task_id);
            string _taskContent = string.Empty;
            if (data.__task_type == public_config.TASK_TYPE_MAIN)
            {
                _taskContent = (88).ToLanguage();
            }
            else if (data.__task_type == public_config.TASK_TYPE_BRANCH)
            {
                _taskContent = (89).ToLanguage();
            }
            else if (data.__task_type == public_config.TASK_TYPE_GUILD)
            {
                _taskContent = (90).ToLanguage();
            }
            else if (data.__task_type == public_config.TASK_TYPE_TEAM)
            {
                _taskContent = (6018016).ToLanguage();
            }
            else if (data.__task_type == public_config.TASK_TYPE_WILD)
            {
                _taskContent = (31145).ToLanguage();
            }
            if (data.__task_type == public_config.TASK_TYPE_TEAM)
            {
                EventDispatcher.AddEventListener(TaskEvent.RING_CHANGE, OnRingChange);
                _txtTaskName.CurrentText.text = string.Format(_taskContent, task_data_helper.GetTaskName(data), PlayerDataManager.Instance.TaskData.ring);
            }
            else
            {
                _txtTaskName.CurrentText.text = string.Format(_taskContent, task_data_helper.GetTaskName(data));
            }
            string taskTargetProgress = string.Empty;
            string content = string.Empty;

            if (_taskInfo.status == public_config.TASK_STATE_NEW)
            {

                int total = task_data_helper.getTaskTargetNum(data);
                if (total > 1 && _taskInfo.target != null && _taskInfo.target.Count > 0)
                {
                    taskTargetProgress = string.Format(_progressContent, _taskInfo.target[0].value, total);
                }
                content = string.Format(_taskTargetContent, task_data_helper.GetTaskSummary(data), taskTargetProgress);

                _taskParticle.Play(true);
                _taskParticle.Position = new Vector3(0, 0, -1);
            }
            else if (_taskInfo.status == public_config.TASK_STATE_ACCOMPLISH || (_taskInfo.status == public_config.TASK_STATE_ACCEPT && (data.__target == null || data.__target.Count == 0)))
            {
                content = string.Format(_taskTargetContent, task_data_helper.GetTaskSummary(data), taskTargetProgress);
                content = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, content);
                _taskFinished.Visible = true;
                if (data.__task_type == public_config.TASK_TYPE_TEAM)
                {
                    _txtTaskName.CurrentText.text = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, string.Format(_taskContent, task_data_helper.GetTaskName(data), PlayerDataManager.Instance.TaskData.ring));
                }
                else
                {
                    _txtTaskName.CurrentText.text = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, (string.Format(_taskContent, task_data_helper.GetTaskName(data))));
                }

                _taskParticle.Play(true);
                _taskParticle.Position = new Vector3(0, 0, -1);
            }
            else
            {
                int instanceId = task_data_helper.GetTaskInstanceId(_taskInfo.task_id);

                if (instanceId != 0 && data.__task_type != public_config.TASK_TYPE_WILD)
                {
                    _levelLimit = instance_helper.GetMinLevel(instanceId);
                }
                else
                {
                    _levelLimit = 0;
                }

                if (PlayerAvatar.Player.level >= _levelLimit)
                {
                    _levelLimit = 0;
                    int total = task_data_helper.getTaskTargetNum(data);
                    if (total > 1 && _taskInfo.target != null && _taskInfo.target.Count > 0)
                    {
                        taskTargetProgress = string.Format(_progressContent, _taskInfo.target[0].value, total);
                    }
                    content = string.Format(_taskTargetContent, task_data_helper.GetTaskSummary(data), taskTargetProgress);
                }
                else
                {
                    string summary = task_data_helper.GetTaskSummary(data);
                    _htmlUtils.ClearContent();
                    _htmlUtils.Clear();
                    _htmlUtils.GetContentStack(summary);
                    if (_htmlUtils.GetLength() > 7)
                    {

                        content = string.Format(_taskTargetContent, _htmlUtils.GetString(7), (95147).ToLanguage(_levelLimit));
                    }
                    else
                    {
                        content = string.Format(_taskTargetContent, summary, (95146).ToLanguage(_levelLimit));
                    }
                }

                content = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_WHITE, content);
                if (PlayerAvatar.Player.level < 40 && data.__task_type == public_config.TASK_TYPE_MAIN)
                {
                    _taskParticle.Play(true);
                    _taskParticle.Position = new Vector3(0, 0, -1);
                }
                else
                {
                    _taskParticle.Stop();
                }
            }
            _txtTaskContent.ChangeAllStateText(content);
        }

        public override void Dispose()
        {
            _taskInfo = null;
            // _btnClick.onClick.RemoveListener(OnClick);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _selectImg.Visible = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _selectImg.Visible = false;
        }

    }
}
