﻿using UnityEngine;
using Game.UI.UIComponent;

namespace ModuleMainUI
{
    public class TownEntryView : KContainer
    {
        private KContainer m_toggleShousuo;

        private KButton _btnCheckmark;
        private KButton _btnDefault;

        private KContainer _baseFunctionContainer;
        private KContainer _extendFunctionContainer;

        public KComponentEvent<bool> onToggleClick = new KComponentEvent<bool>();

        public GameObject BaseFunction
        {
            get { return _baseFunctionContainer.gameObject; }
        }

        public GameObject ExtendFunction
        {
            get { return _extendFunctionContainer.gameObject; }
        }

        protected override void Awake()
        {
            m_toggleShousuo = GetChildComponent<KContainer>("Container_Shousuo");

            _btnDefault = m_toggleShousuo.GetChildComponent<KButton>("Button_image");
            _btnCheckmark = m_toggleShousuo.GetChildComponent<KButton>("Button_checkmark");

            _baseFunctionContainer = GetChildComponent<KContainer>("Container_zhankai");
            _extendFunctionContainer = GetChildComponent<KContainer>("Container_shousuo");
            _extendFunctionContainer.Visible = false;
            _btnCheckmark.Visible = false;
            //ToggleIcons(false);
            AddEventListener();
        }


        private void AddEventListener()
        {
            _btnDefault.onClick.AddListener(OnClickDefault);
            _btnCheckmark.onClick.AddListener(OnClickCheckmark);
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }
        private void RemoveEventListener()
        {
            _btnDefault.onClick.RemoveListener(OnClickDefault);
            _btnCheckmark.onClick.RemoveListener(OnClickCheckmark);
        }


        private void OnClickDefault()
        {
            ToggleIcons(true);
        }

        private void OnClickCheckmark()
        {
            ToggleIcons(false);
        }

        public void HideEntryBtn()
        {
            ToggleIcons(false);
        }

        private bool isTween = false;
        private void ToggleIcons(bool isShow)
        {
            if(isTween)
            {
                return;
            }
            _btnCheckmark.Visible = isShow;
            _btnDefault.Visible = !isShow;
            isTween = true;
            onToggleClick.Invoke(isShow);
        }

        public void TweenEnd()
        {
            isTween = false;
        }

        public void ShowBaseFunction()
        {
            _baseFunctionContainer.Visible = true;
        }

        public void ShowExtendFunction()
        {
            _extendFunctionContainer.Visible = true;
        }

        public void HideBaseFunction()
        {
            _baseFunctionContainer.Visible = false;
        }

        public void HideExtendFunction()
        {
            _extendFunctionContainer.Visible = false;
        }

    }
}
