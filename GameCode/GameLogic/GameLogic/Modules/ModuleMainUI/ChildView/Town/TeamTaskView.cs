﻿#region 模块信息
/*==========================================
// 文件名：MainUITeamTaskView
// 命名空间: GameLogic.GameLogic.Modules.ModuleMainUI.ChildView.Town
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/6/3 16:24:19
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleMainUI
{
    public class TeamTaskView : KContainer
    {
        public const int TEAM_GUIDE_ID = 999999;
        private TownTaskView _taskView;
        private TownTeamView _teamView;
        private KContainer _taskGuide;
        private StateText _txtGuideContent;
        private KButton _btnButton;

        private KToggleGroup _toggleGroup;

        private KContainer _contentContainer;
        private StateImage _teamInfoSpotLight;
        private KContainer _teamPromptContainer;
        private RectTransform _teamPromptRect;
        private KParticle _teamToggleParticle;
        private TweenPosition _teamPromptTweenPosition;
        private Vector3 _teamPromptOriginalPosition;
        private RectTransform _contentRect;
        private RectTransform _buttonRect;
        private RectTransform _toggleGroupRect;

        private Vector3 _contentPos;
        private Vector3 _buttonPos;
        private Vector3 _toggleGroupPos;
        private Quaternion _buttonRotation;

        private Vector3 _contentEndPos;
        private Vector3 _buttonEndPos;
        private Vector3 _toggleGroupEndPos;
        private Quaternion _buttonEndRotation;

        private bool _isHide;

        private const int TASK_INDEX = 0;
        private const int TEAM_INDEX = 1;
        private int preSelectIndex = TASK_INDEX;
        private RectTransform _rect;
        private Vector2 _pos;
        private int taskGuideLevel;

        private StateImage _bgImage;

        private KButton _showBuffButton;
        private FieldRePlayKitView _replayKitView;    //IOS9 视频录制开关视图
        private int _levelLimit;
        private int _guideCount;
        private int _langId;

        private StateImage _guideImage;
        private RectTransform _guideImageTrans;
        private RectTransform _txtGuideTrans;

        protected override void Awake()
        {
            taskGuideLevel = int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.task_guide_level));
            _bgImage = GetChildComponent<StateImage>("ScaleImage_sharedzhezhao");
            _btnButton = GetChildComponent<KButton>("Button_zhankai");

            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_group");
            _teamInfoSpotLight = GetChildComponent<StateImage>("ToggleGroup_group/Toggle_zudui/renwu/Spotlight");
            _teamInfoSpotLight.Visible = false;
            _contentContainer = GetChildComponent<KContainer>("Container_content");
            _teamPromptContainer = GetChildComponent<KContainer>("Container_tishixinxi");
            _teamPromptContainer.Visible = false;
            _teamPromptTweenPosition = AddChildComponent<TweenPosition>("Container_tishixinxi");
            AddChildComponent<RaycastComponent>("Container_tishixinxi");
            _teamPromptRect = GetChildComponent<RectTransform>("Container_tishixinxi");
            _teamPromptOriginalPosition = _teamPromptRect.localPosition;
            _taskView = _contentContainer.AddChildComponent<TownTaskView>("Container_Task");
            _teamView = _contentContainer.AddChildComponent<TownTeamView>("Container_Team");
            _taskGuide = _contentContainer.GetChildComponent<KContainer>("Container_dianjirenwu");
            _guideImageTrans = _taskGuide.GetChildComponent<RectTransform>("ScaleImage_dianjirenwu");
            _rect = _taskGuide.GetComponent<RectTransform>();
            _pos = _rect.anchoredPosition;
            _txtGuideContent = _taskGuide.GetChildComponent<StateText>("Label_txtDianjirenwu");
            _txtGuideTrans = _txtGuideContent.gameObject.GetComponent<RectTransform>();
            _guideImage = _guideImageTrans.GetComponent<StateImage>();

            string content = global_params_helper.GetGlobalParam(GlobalParamId.team_guide);
            int[] dataList = global_params_helper.StringToArray<int>(content,int.Parse);
            _levelLimit = dataList[0];
            _guideCount = dataList[1];
            _langId = dataList[2];

            _teamToggleParticle = AddChildComponent<KParticle>("ToggleGroup_group/Toggle_zudui/fx_ui_3_1");
            _teamToggleParticle.transform.localScale = new Vector3(0.41f, 0.6f, 1);
            _teamToggleParticle.transform.localPosition = new Vector3(0, -7, 0);
            _teamToggleParticle.Stop();
            _teamView.Visible = false;
            vect = Vector2.zero;
            InitPromptTweenSetting();
            InitShowBuffButton();
            InitReplayKitView();
            base.Awake();
        }

        private void InitReplayKitView()
        {
            _replayKitView = AddChildComponent<FieldRePlayKitView>("Container_luxiang");
            if (Application.platform != RuntimePlatform.IPhonePlayer)
            {
                _replayKitView.Visible = false;
            }
            else
            {
                _replayKitView.VisiableWithIosVersion(); // .Visible = true;
                _replayKitView.GetComponent<RectTransform>().anchoredPosition = iosRecorderViewVector;
            }
        }

        private Vector2 iosRecorderViewVector = new Vector2(298, -12);
        private Vector2 iosShrinkedRecorderViewVector = new Vector2(58, -12);
        private Vector2 iosPlatformBuffBtnPosition = new Vector2(357, -12);
        private Vector2 iosPlatformShrinkBtnPosition = new Vector2(110, -12);

        private void InitShowBuffButton()
        {
            _showBuffButton = GetChildComponent<KButton>("Button_qianghuaBuff");
            if (Application.platform != RuntimePlatform.IPhonePlayer)
            {
                _showBuffButton.GetComponent<RectTransform>().anchoredPosition = iosRecorderViewVector;
            }
            else
            {
                _showBuffButton.GetComponent<RectTransform>().anchoredPosition = iosPlatformBuffBtnPosition;
            }
            CheckBuffNeedShow();
            if (_showBuffButton.Visible == false)
            {
                return;
            }
            
        }

        private void CheckBuffNeedShow()
        {
            _showBuffButton.Visible = NeedShowBuff();
        }

        private bool NeedShowBuff()
        {
            List<int> buffList = PlayerAvatar.Player.bufferManager.GetCurBufferIDList();
            for (int i = 0; i < buffList.Count; i++)
            {
                if (buff_helper.NeedShowInMainUI(buffList[i]))
                {
                    return true;
                }
            }
            return false;
        }

        private Vector2 vect;
        public int count = 0;
        public int dir = 0;

        private void UpdateGuide()
        {
            if (_toggleGroup.SelectIndex == TASK_INDEX)
            {
                _taskGuide.Visible = PlayerAvatar.Player.level <= taskGuideLevel;
                _txtGuideContent.CurrentText.text = (6260003).ToLanguage();
            }
            else if(PlayerDataManager.Instance.TeamData.TeamId!=0)
            {
                _taskGuide.Visible = PlayerDataManager.Instance.TeamData.teamGuideFlag && PlayerAvatar.Player.level <= _levelLimit && DramaManager.GetInstance().GetTriggerCount(TEAM_GUIDE_ID) <= _guideCount;
                _txtGuideContent.CurrentText.text = _langId.ToLanguage();
            }
            else
            {
                _taskGuide.Visible = false;
                return;
            }
            _guideImage.SetDimensionsDirty();
            _guideImageTrans.sizeDelta = new Vector2(_txtGuideContent.CurrentText.preferredWidth + 49, _guideImageTrans.sizeDelta.y);
            _txtGuideTrans.localPosition = _guideImageTrans.localPosition + new Vector3(32, -9, 0);
        }

        private void UpdateGuidePos()
        {
            if (_taskGuide.Visible == true)
            {
                if (dir == 0)
                {
                    count++;
                    if (count >= 30)
                    {
                        dir = 1;
                    }
                }
                else
                {
                    count--;
                    if (count <= 0)
                    {
                        dir = 0;
                    }
                }
                vect.x = count;
                _rect.anchoredPosition = _pos + vect;
            }
        }

        public void OnShow()
        {
            _taskView.UpdateContent(true);
            UpdateGuide();
            AddEventListener();
        }

        private void AddEventListener()
        {
            _btnButton.onClick.AddListener(OnClickButton);
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            _toggleGroup.GetToggleList()[0].onValueChanged.AddListener(OnToggleValueChanged1);
            _toggleGroup.GetToggleList()[1].onValueChanged.AddListener(OnToggleValueChanged2);
            EventDispatcher.AddEventListener(TeamEvent.TEAM_ID_CHANGED, OnTeamIdChanged);
            EventDispatcher.AddEventListener(TeamEvent.TEAM_GUIDE_FLAG_CHANGED, OnTeamIdChanged);
            EventDispatcher.AddEventListener(TeamEvent.TEAM_APPLY_NOTICE, OnRefreshApplyNotice);
            EventDispatcher.AddEventListener(BuffEvents.BUFF_REFRESH, CheckBuffNeedShow);
            _showBuffButton.onClick.AddListener(OnBuffBtnClick);
        }


        private void OnToggleValueChanged1(KToggle toggle, bool isOn)
        {
            if (_toggleGroup.SelectIndex == preSelectIndex)
            {
                PanelIdEnum.Task.Show();
            }
            preSelectIndex = _toggleGroup.SelectIndex;
            UpdateGuide();
        }

        private void OnToggleValueChanged2(KToggle toggle, bool isOn)
        {
            if (_teamInfoSpotLight.Visible == true)
            {
                PanelIdEnum.Interactive.Show(new int[] { 2, 3 });//打开申请消息标签
            }
            else if (PlayerDataManager.Instance.TeamData.TeammateDic.Count <= 0)
            {
                PanelIdEnum.Interactive.Show(new int[] { 2, 1 });//打开邀请玩家标签
            }
            else if (_toggleGroup.SelectIndex == preSelectIndex)
            {
                PanelIdEnum.Interactive.Show(new int[] { 2, 1 }); //打开我的队伍
            }
            else
            {

            }
            _teamInfoSpotLight.Visible = false;
            _teamPromptContainer.Visible = false;
            _teamToggleParticle.Stop();
            preSelectIndex = _toggleGroup.SelectIndex;
            UpdateGuide();
        }

        private void RemoveEventListener()
        {
            _btnButton.onClick.RemoveListener(OnClickButton);
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            _toggleGroup.GetToggleList()[0].onValueChanged.RemoveListener(OnToggleValueChanged1);
            _toggleGroup.GetToggleList()[1].onValueChanged.RemoveListener(OnToggleValueChanged2);
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_ID_CHANGED, OnTeamIdChanged);
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_GUIDE_FLAG_CHANGED, OnTeamIdChanged);
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_APPLY_NOTICE, OnRefreshApplyNotice);
            EventDispatcher.RemoveEventListener(BuffEvents.BUFF_REFRESH, CheckBuffNeedShow);
            _showBuffButton.onClick.RemoveListener(OnBuffBtnClick);
        }

        private void OnTeamIdChanged()
        {
            UpdateGuide();
        }

        private void OnBuffBtnClick()
        {
            if (BaseModule.IsPanelShowing(PanelIdEnum.BuffDetail))
            {
                UIManager.Instance.ClosePanel(PanelIdEnum.BuffDetail);
            }
            else
            {
                UIManager.Instance.ClosePanel(PanelIdEnum.RewardBuffDetail);
                UIManager.Instance.ShowPanel(PanelIdEnum.BuffDetail);
            }
        }

        private void OnRefreshApplyNotice()
        {
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            if (teamData.CaptainId == MogoWorld.Player.dbid)
            {
                _teamInfoSpotLight.Visible = true;
                _teamPromptContainer.Visible = true;
                if (_contentContainer.Visible == true)
                {
                    OpenStateAnimation();
                }
                else
                {
                    CloseStateAnimation();
                }
                _teamToggleParticle.Play(true);
            }
        }

        private void OnClickButton()
        {
            _isHide = !_isHide;
            if (_isHide)
            {
                TweenHide();
                _bgImage.Visible = false;
            }
            else
            {
                TweenShow();
                _bgImage.Visible = true;
            }
        }


        private void InitPromptTweenSetting()
        {
            _teamPromptTweenPosition.duration = 2.0f;
            _teamPromptTweenPosition.from = _teamPromptOriginalPosition;
            _teamPromptTweenPosition.to = _teamPromptOriginalPosition - new Vector3(15, 0, 0);
            _teamPromptTweenPosition.style = UITweener.Style.PingPong;
            _teamPromptTweenPosition.Play(true);
        }

        private void TweenHide()
        {
            if (_buttonRect == null)
            {
                _buttonRect = _btnButton.GetComponent<RectTransform>();
                _buttonRect.pivot = new Vector2(0.5f, 0.5f);
                Vector2 size = _buttonRect.sizeDelta;
                Vector3 position = _buttonRect.anchoredPosition3D;
                _buttonRect.anchoredPosition3D = new Vector3(position.x + size.x * 0.5f, position.y - size.y * 0.5f);
                _buttonPos = _buttonRect.anchoredPosition3D;
                _buttonRotation = _buttonRect.localRotation;

                _buttonEndRotation = new Quaternion();
                _buttonEndRotation.eulerAngles = new Vector3(0, 0, 180);

                _toggleGroupRect = _toggleGroup.GetComponent<RectTransform>();
                _toggleGroupPos = _toggleGroupRect.anchoredPosition3D;
                _toggleGroupEndPos = _toggleGroupPos - new Vector3(_toggleGroupRect.sizeDelta.x, 0, 0);
                _buttonEndPos = _buttonPos - new Vector3(_toggleGroupRect.sizeDelta.x + 8, 0, 0);

                _contentRect = _contentContainer.GetComponent<RectTransform>();
                _contentPos = _contentRect.anchoredPosition3D;
                _contentEndPos = _contentPos - new Vector3(_contentRect.sizeDelta.x, 0, 0);
            }

            TweenPosition3D.Begin(_btnButton.gameObject, 0.2f, _buttonEndPos);
            TweenPosition3D.Begin(_toggleGroup.gameObject, 0.2f, _toggleGroupEndPos);
            TweenPosition3D.Begin(_contentContainer.gameObject, 0.2f, _contentEndPos);
            TweenRotation.Begin(_btnButton.gameObject, 0.2f, _buttonEndRotation, HideEnd);
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                TweenPosition.Begin(_replayKitView.gameObject, 0.2f, iosShrinkedRecorderViewVector);
                TweenPosition.Begin(_showBuffButton.gameObject, 0.2f, iosPlatformShrinkBtnPosition);
            }
            else
            {
                TweenPosition.Begin(_showBuffButton.gameObject, 0.2f, iosShrinkedRecorderViewVector);
            }
            OpenToCloseAnimation();
        }

        private void HideEnd(UITweener tweener)
        {
            _contentContainer.Visible = false;
            _toggleGroup.Visible = false;
        }

        private void TweenShow()
        {
            _contentContainer.Visible = true;
            _toggleGroup.Visible = true;
            TweenPosition3D.Begin(_btnButton.gameObject, 0.2f, _buttonPos);
            TweenPosition3D.Begin(_toggleGroup.gameObject, 0.2f, _toggleGroupPos);
            TweenPosition3D.Begin(_contentContainer.gameObject, 0.2f, _contentPos);
            TweenRotation.Begin(_btnButton.gameObject, 0.2f, _buttonRotation, ShowEnd);
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                TweenPosition.Begin(_replayKitView.gameObject, 0.2f, iosRecorderViewVector);
                TweenPosition.Begin(_showBuffButton.gameObject, 0.2f, iosPlatformBuffBtnPosition);
            }
            else
            {
                TweenPosition.Begin(_showBuffButton.gameObject, 0.2f, iosRecorderViewVector);
            }
            CloseToOpenAnimation();
        }

        private void ShowEnd(UITweener tweener)
        {
        }

        private void OnSelectedIndexChanged(KToggleGroup toggle, int index)
        {
            _taskView.Visible = index == 0;
            _teamView.Visible = index == 1;
        }

        public void OnHide()
        {
            RemoveEventListener();
        }

        #region 提示框的四个动画

        //任务点开时的动画
        private void OpenStateAnimation()
        {
            _teamPromptTweenPosition.Stop();
            _teamPromptTweenPosition.duration = 2.0f;
            _teamPromptTweenPosition.from = _teamPromptOriginalPosition;
            _teamPromptTweenPosition.to = _teamPromptOriginalPosition - new Vector3(15, 0, 0);
            _teamPromptTweenPosition.style = UITweener.Style.PingPong;
            _isPlayTween = true;
        }

        //任务关闭时的动画
        private void CloseStateAnimation()
        {
            _teamPromptTweenPosition.Stop();
            _teamPromptTweenPosition.duration = 2.0f;
            _teamPromptTweenPosition.from = _teamPromptOriginalPosition - _buttonPos + _buttonEndPos;
            _teamPromptTweenPosition.to = _teamPromptOriginalPosition - _buttonPos + _buttonEndPos - new Vector3(15, 0, 0);
            _teamPromptTweenPosition.style = UITweener.Style.PingPong;
            _isPlayTween = true;
        }

        //由开到关的动画
        private void OpenToCloseAnimation()
        {
            _teamPromptTweenPosition.Stop();
            _teamPromptTweenPosition.duration = 0.2f;
            _teamPromptTweenPosition.from = _teamPromptOriginalPosition;
            _teamPromptTweenPosition.to = _teamPromptOriginalPosition - _buttonPos + _buttonEndPos;
            _teamPromptTweenPosition.style = UITweener.Style.Once;
            _teamPromptTweenPosition.onFinished = CloseStateAnimation;
            _isPlayTween = true;
        }

        private void CloseStateAnimation(UITweener tween)
        {
            CloseStateAnimation();
        }

        //由关到开的动画
        private void CloseToOpenAnimation()
        {
            _teamPromptTweenPosition.Stop();
            _teamPromptTweenPosition.duration = 0.2f;
            _teamPromptTweenPosition.from = _teamPromptOriginalPosition - _buttonPos + _buttonEndPos;
            _teamPromptTweenPosition.to = _teamPromptOriginalPosition;
            _teamPromptTweenPosition.style = UITweener.Style.Once;
            _teamPromptTweenPosition.onFinished = OpenStateAnimation;
            _isPlayTween = true;
        }

        //为了实现下一帧播放
        bool _isPlayTween = false;
        void Update()
        {
            if (_isPlayTween == true && _teamPromptContainer.Visible == true)
            {
                _teamPromptTweenPosition.Play(true);
                _isPlayTween = false;
            }
            UpdateGuidePos();
        }

        private void OpenStateAnimation(UITweener tween)
        {
            OpenStateAnimation();
        }
        #endregion
    }
}
