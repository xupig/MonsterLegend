﻿using Common.Base;
using MogoEngine.Events;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.Entities;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using Common.Global;
using Common.Utils;
using GameMain.GlobalManager.SubSystem;
using Common.ExtendComponent;
using GameData;
using Common.Data;
using UnityEngine;

namespace ModuleMainUI
{
    public class PlayerTownView : KContainer
    {
        private StateText _levelTxt;

        private IconContainer _headIcon;
        private KDummyButton _headBtn;

        private ArtNumberList _fightForceList;
        //private KButton _addEnergyBtn;
        private KButton _addFightForceBtn;
        private StateImage _captainImg;
        private KButton _rewardBuffButton;

        //private KButton _vipButton;

        private const int GAP = -5;
        private const int DIGITAL_COUNT = 8;
        //private KButton _bangButton;   //主界面左上角--榜按钮
        //private KButton _shareButton;  //主界面左上角--分享按钮

        //private VipLevelItem _vipLevelItem;

        // private CanOperationEffect _effect;
        private StateText _hpText;
        private KProgressBar _hpProgressBar;
        private PlayerEpBar _epProgressBar;
        private KProgressBar _powerProgressBar;
        private KDummyButton _addEnergyBtn;
        private IconContainer _teamDutyIcon;

        protected override void Awake()
        {
            KContainer container = GetChildComponent<KContainer>("Container_HeadInfo");
            _headIcon = container.AddChildComponent<IconContainer>("Container_icon");
            _captainImg = container.GetChildComponent<StateImage>("Image_duizhang");
            _headBtn = _headIcon.gameObject.AddComponent<KDummyButton>();
            _levelTxt = container.GetChildComponent<StateText>("Container_dengji/Label_txtDengji");

            _fightForceList = container.AddChildComponent<ArtNumberList>("Container_shengming/List_zhandouli");

            _hpProgressBar = container.GetChildComponent<KProgressBar>("ProgressBar_xueliang");
            _powerProgressBar = container.GetChildComponent<KProgressBar>("ProgressBar_tili");
            _addEnergyBtn = _powerProgressBar.gameObject.AddComponent<KDummyButton>();
            _hpText = _hpProgressBar.GetChildComponent<StateText>("txtShengmingzhi");
            _epProgressBar = container.AddChildComponent<PlayerEpBar>("Container_nuqi");

            _addFightForceBtn = GetChildComponent<KButton>("Button_tigao");
            _rewardBuffButton = GetChildComponent<KButton>("Button_yingfu");

            _teamDutyIcon = container.AddChildComponent<IconContainer>("Container_teamDuty");
            _rewardBuffButton.Visible = false;
        }

        private void RefreshContent()
        {
            RefreshLevel();
            RefreshFightForce();
            RefleshVipLevel();
            RefreshRewardBuffButton();
            RefreshCaptainImage();
            RefreshAddFightForceBtn();
            RefreshHp();
            RefreshPower();
            RefreshDutyIcon();
            _headIcon.SetIcon(MogoPlayerUtils.GetSmallIcon(PlayerAvatar.Player.vocation.ToInt()));
        }

        private void RefreshDutyIcon()
        {
            if (PlayerDataManager.Instance.TeamData.IsInTeam())
            {
                int iconId = skill_helper.GetTeamDutyIconId(PlayerAvatar.Player.vocation,PlayerAvatar.Player.spell_proficient);
                if (iconId != 0)
                {
                    _teamDutyIcon.Visible = true;
                    _teamDutyIcon.SetIcon(iconId);
                }
                else
                {
                    _teamDutyIcon.Visible = false;
                }
            }
            else
            {
                _teamDutyIcon.Visible = false;
            }
        }

        private void RefreshPower()
        {
            _powerProgressBar.Value = Mathf.Min(1, (float)PlayerAvatar.Player.energy / GlobalParams.GetFullEnergy());
        }

        private void RefreshHp()
        {
            PlayerAvatar avatar = PlayerAvatar.Player;
            _hpText.CurrentText.text = string.Concat(avatar.cur_hp_string, "/", avatar.max_hp_string);
            if (avatar.max_hp != 0)
            {
                _hpProgressBar.Value = (float)avatar.cur_hp / avatar.max_hp;
            }
        }

        private void RefreshAddFightForceBtn()
        {
            if (function_helper.IsFunctionOpen((int)FunctionId.fightForceIncrease))
            {
                _addFightForceBtn.Visible = true;
            }
            else
            {
                _addFightForceBtn.Visible = false;
            }
        }

        private void RefreshCaptainImage()
        {
            _captainImg.Visible = PlayerDataManager.Instance.TeamData.IsCaptain();
            RefreshDutyIcon();
        }

        private void AddEventListener()
        {
            _headBtn.onClick.AddListener(OnHeadClick);
            //_vipButton.onClick.AddListener(OnVipButtonClicked);
            //_bangButton.onClick.AddListener(OnBangButtonClick);
            _addEnergyBtn.onClick.AddListener(OnAddEnergyClick);
            _addFightForceBtn.onClick.AddListener(OnAddFightForceClick);
            _rewardBuffButton.onClick.AddListener(OnShowRewardBuffDetail);
            // EventDispatcher.AddEventListener(MainUIEvents.INFORMATION_OPERATION_STATE_CHANGE, OnInformationOperationStateChange);
            //_shareButton.onClick.AddListener(OnShareButtonClick);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, RefreshLevel);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.fight_force, RefreshFightForce);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.vip_level, RefleshVipLevel);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.cur_hp, RefreshHp);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.max_hp, RefreshHp);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.energy, RefreshPower);
            EventDispatcher.AddEventListener<int>(FunctionEvents.ADD_FUNTION, SetAddFightForceBtn);
            EventDispatcher.AddEventListener(TeamEvent.TEAM_REFRESH, RefreshCaptainImage);
            EventDispatcher.AddEventListener(RewardBuffEvents.ShowRewardBuffIcon, RefreshRewardBuffButton);
            EventDispatcher.AddEventListener(RewardBuffEvents.HideRewardBuffIcon, RefreshRewardBuffButton);
        }

        private void SetAddFightForceBtn(int functionId)
        {
            if (functionId == (int)FunctionId.fightForceIncrease)
            {
                _addFightForceBtn.Visible = true;
            }
        }

        private void RemoveEventListener()
        {
            _headBtn.onClick.RemoveListener(OnHeadClick);
            //_vipButton.onClick.RemoveListener(OnVipButtonClicked);
            //_bangButton.onClick.RemoveListener(OnBangButtonClick);
            _addEnergyBtn.onClick.RemoveListener(OnAddEnergyClick);
            _addFightForceBtn.onClick.RemoveListener(OnAddFightForceClick);
            _rewardBuffButton.onClick.RemoveListener(OnShowRewardBuffDetail);
            //EventDispatcher.RemoveEventListener(MainUIEvents.INFORMATION_OPERATION_STATE_CHANGE, OnInformationOperationStateChange);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, RefreshLevel);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.fight_force, RefreshFightForce);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.vip_level, RefleshVipLevel);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.cur_hp, RefreshHp);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.max_hp, RefreshHp);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.energy, RefreshPower);
            EventDispatcher.RemoveEventListener<int>(FunctionEvents.ADD_FUNTION, SetAddFightForceBtn);
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_REFRESH, RefreshCaptainImage);
            EventDispatcher.RemoveEventListener(RewardBuffEvents.ShowRewardBuffIcon, RefreshRewardBuffButton);
            EventDispatcher.RemoveEventListener(RewardBuffEvents.HideRewardBuffIcon, RefreshRewardBuffButton);
        }

        private void OnShowRewardBuffDetail()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.BuffDetail);
            UIManager.Instance.ShowPanel(PanelIdEnum.RewardBuffDetail, _rewardBuffButton.GetComponent<RectTransform>());
        }

        private void RefreshRewardBuffButton()
        {
            _rewardBuffButton.Visible = RewardBuffManager.Instance.HaveRewardBuff;
        }

        private void OnAddFightForceClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.FightForceChannel, 1);
        }

        private void OnAddEnergyClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.EnergyBuying);
        }

        private void OnHeadClick()
        {
            PanelIdEnum.Information.Show(1);
        }

        private void OnVipButtonClicked()
        {
            //UIManager.Instance.ShowPanel(PanelIdEnum.Charge, 3); //1表示充值标签，2表示代充标签，3表示vip
            //List<string> list = new List<string>();
            //list.Add("32");
            //UIManager.Instance.ShowPanel(PanelIdEnum.SingleInstance, list);
        }

        private void OnBangButtonClick()
        {

        }

        /// <summary>
        /// 点击“分享”按钮
        /// </summary>
        private void OnShareButtonClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GM);
        }

        private void RefreshLevel()
        {
            PlayerAvatar.Player.RecordUpLevelLogToSdk();
            _levelTxt.CurrentText.text = PlayerAvatar.Player.level.ToString();
        }

        private void RefreshFightForce()
        {
            _fightForceList.SetNumber((int)PlayerAvatar.Player.fight_force);
        }

        private void RefleshVipLevel()
        {
            //if (PlayerAvatar.Player.vip_level <= 0)
            //{
            //    _vipButton.Visible = false;
            //}
            //else
            //{
            //    _vipButton.Visible = true;
            //    _vipLevelItem.SetVipLevel(PlayerAvatar.Player.vip_level);
            //}
        }

        protected override void OnEnable()
        {
            RefreshContent();
            AddEventListener();
            ProcessFunctionPoint();
            SetUIButtonAudioVolume();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        protected override void OnDestroy()
        {
            SavePrivateChatInfo();
        }

        private void ProcessFunctionPoint()
        {
            ProcessRewardSystemPoint();
        }

        private void ProcessRewardSystemPoint()
        {
            //由于vip等级会影响VipGift中是否有可领取的奖励，监听vip等级变化的事件只是在其改变时才会触发，刚开始登陆游戏时，
            //即使有vip等级，也是不会触发的，因此在主界面打开时做Vip礼包tips提示的更新
            RewardManager.Instance.ProcessRewardPoint(RewardSystemType.VipGift);
            RewardManager.Instance.ProcessRewardPoint(RewardSystemType.DailyTask);
        }

        private void SetUIButtonAudioVolume()
        {
            int settingVolume = PlayerDataManager.Instance.SettingData.GetSettingValue(SettingType.Sound);
            KComponentAudioCollective.audioVolume = settingVolume / 100f;
        }

        private void SavePrivateChatInfo()
        {
            ChatPrivateData privateData = PlayerDataManager.Instance.ChatData.ChatPrivateData;
            privateData.SavePrivateChatInfo();
        }
    }
}
