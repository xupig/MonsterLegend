﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using UnityEngine;
using MogoEngine.Events;
using GameLoader.Utils;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using Common.Structs.ProtoBuf;
using MogoEngine;
using MogoEngine.RPC;
using GameMain.Entities;
using GameData;
using Common.ServerConfig;
using GameMain.GlobalManager.SubSystem;
using Common.Utils;
using MogoEngine.Utils;
using Common.ExtendComponent;
using GameLoader.Utils.Timer;

namespace ModuleMainUI
{
    public class LuckNoticeView : KContainer
    {
        private KContainer _container;
        private StateText _content;
        private bool _hasNotice;
        private uint _timerId;

        protected override void Awake()
        {
            _container = GetChildComponent<KContainer>("Container_xinxi");
            _content = GetChildComponent<StateText>("Container_xinxi/Label_content");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            _container.Visible = false;
            _hasNotice = false;
            FriendManager.Instance.SendGetFriendListMsg();
            RefreshContent();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
            ResetTimer();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(TeamInstanceEvents.Teammate_Refresh, RefreshContent);
            EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_LIST, RefreshContent);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(TeamInstanceEvents.Teammate_Refresh, RefreshContent);
            EventDispatcher.RemoveEventListener(FriendEvents.REFRESH_FRIEND_LIST, RefreshContent);
        }

        private void RefreshContent()
        {
            if (_hasNotice == true) return;
            foreach (PbHpMember teamMember in PlayerDataManager.Instance.TeamInstanceData.HpMemberDic.Values)
            {
                if (PlayerDataManager.Instance.FriendData.HasFriend(teamMember.dbid))
                {
                    _hasNotice = true;
                    _container.Visible = true;
                    _container.transform.localScale = Vector3.one;
                    TweenViewMove.Begin(_container.gameObject, MoveType.Show, MoveDirection.Right);
                    _content.CurrentText.text = MogoLanguageUtil.GetContent(55033, MogoProtoUtils.ParseByteArrToString(teamMember.name));
                    ResetTimer();
                    _timerId = TimerHeap.AddTimer(15000, 0, CloseView);
                }
            }
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private void CloseView()
        {
            ResetTimer();
            PlayMoveAnimation();
        }

        private void PlayMoveAnimation()
        {
            TweenPosition.Begin(_container.gameObject, 1.0f, new Vector2(0, 100), 0, UITweener.Method.EaseIn, OnMoveEnd);
            TweenScale.Begin(_container.gameObject, 1.0f, Vector2.zero, UITweener.Method.EaseIn);
        }

        private void OnMoveEnd(UITweener tween)
        {
            _container.Visible = false;
            TweenScale tweenScale = _container.GetComponent<TweenScale>();
            if (tweenScale != null)
            {
                tweenScale.Stop();
                tweenScale.enabled = false;
            }
        }

    }
}
