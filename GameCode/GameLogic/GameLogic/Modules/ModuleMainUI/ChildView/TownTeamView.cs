﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using UnityEngine;
using MogoEngine.Events;
using GameLoader.Utils;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using Common.Structs.ProtoBuf;
using MogoEngine;
using MogoEngine.RPC;
using GameMain.Entities;
using GameData;
using Common.ServerConfig;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using Common.Utils;
using ModulePlayerInfo;

namespace ModuleMainUI
{
    public class TownTeamView : KContainer
    {
        private KList _teamMemberList;
        private KContainer _noneTeam;

        protected override void Awake()
        {
            base.Awake();
            _teamMemberList = GetChildComponent<KList>("List_zudui");
            _noneTeam = GetChildComponent<KContainer>("Container_noneTeam");
            _noneTeam.Visible = false;
            _teamMemberList.SetDirection(KList.KListDirection.LeftToRight, 1);
        }

        protected override void OnEnable()
        {
            AddEventListener();
            RefreshMemberList();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(TeamEvent.TEAM_REFRESH, RefreshMemberList);
            _teamMemberList.onItemClicked.AddListener(OnItemClick);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_REFRESH, RefreshMemberList);
            _teamMemberList.onItemClicked.RemoveListener(OnItemClick);
        }

        private void OnItemClick(KList arg0, KList.KListItemBase item)
        {
            ulong dbid = ((item as TownMemberItem).Data as PbTeamMember).dbid;
            RectTransform rect = item.transform as RectTransform;
            Vector2 screenPosition = RectTransformUtility.WorldToScreenPoint(UIManager.Instance.UICamera, item.transform.position);
            TeammateInfoData data = new TeammateInfoData();
            data.dbid = dbid;
            data.position = screenPosition;
            data.name = ((item as TownMemberItem).Data as PbTeamMember).name;
            UIManager.Instance.ShowPanel(PanelIdEnum.TeammateInfo, data);
            PlayerDataManager.Instance.TeamData.teamGuideFlag = false;
            EventDispatcher.TriggerEvent(TeamEvent.TEAM_GUIDE_FLAG_CHANGED);
        }

        private void RefreshMemberList()
        {
            Dictionary<ulong, PbTeamMember> teamMateDic = PlayerDataManager.Instance.TeamData.TeammateDic;
            RefreshNoneTeamView(teamMateDic.Count);
            ulong captainId = PlayerDataManager.Instance.TeamData.CaptainId;
            _teamMemberList.RemoveAll();
            foreach (PbTeamMember member in teamMateDic.Values)
            {
                if (member.dbid == PlayerAvatar.Player.dbid)
                {
                    continue;
                }
                if (member.dbid == captainId)
                {
                    _teamMemberList.AddItemAt<TownMemberItem>(member, 0, false);
                    continue;
                }
                _teamMemberList.AddItem<TownMemberItem>(member, false);
            }
            _teamMemberList.DoLayout();
        }

        private void RefreshNoneTeamView(int count)
        {
            if (count == 0)
            {
                _noneTeam.Visible = true;
            }
            else
            {
                _noneTeam.Visible = false;
            }
        }
    }
}
