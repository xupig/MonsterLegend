﻿#region 模块信息
/*==========================================
// 文件名：MainUIFunctionEntryView
// 命名空间: GameLogic.GameLogic.Modules.ModuleMainUI.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/12/28 10:20:35
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Game.UI.Builder;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    public class MainUIFunctionEntryView:KContainer
    {
        public static bool isVisible = false;

        public const string CLOSE_FUNCTION_ENTRY_VIEW = "MainUIFunctionEntryView_CLOSE_FUNCTION_ENTRY_VIEW";
        public const string CLOSE_END = "MainUIFunctionEntryView_CLOSE_END";

        private RectTransform _transform;
        private KContainer _content;
        private Dictionary<int,FunctionEntryItem> _entryItemDict;
        private StateImage _imgBg;
        private KDummyButton _btnClickOtherAndClose;
        private KButton _btnClose;
        private RectTransform _rect;
        private Vector2 _orignalPos;

        private GameObject _template;
        public static GameObject pointGo;
        private UIAnchorResizer _resizer;
        public UIImageResizer resizer;

        private KComponentEvent _onCloseFunctionEntry;
        public KComponentEvent onCloseFunctionEntry
        {
            get
            {
                if(_onCloseFunctionEntry == null)
                {
                    _onCloseFunctionEntry = new KComponentEvent();
                }
                return _onCloseFunctionEntry;
            }
        }

        protected override void Awake()
        {
            _transform = GetComponent<RectTransform>();
            _resizer = GetComponent<UIAnchorResizer>();
            _entryItemDict = new Dictionary<int, FunctionEntryItem>();

            _content = GetChildComponent<KContainer>("Container_content");
            _rect = _content.GetComponent<RectTransform>();
            _orignalPos = _rect.anchoredPosition;
            _template = _content.transform.FindChild("Container_itemTemplate/Button_itemTemplate").gameObject;

            _imgBg = _content.GetChildComponent<StateImage>("ScaleImage_miaoshudi");
            resizer = _imgBg.gameObject.AddComponent<UIImageResizer>();
            resizer.isHorizontal = false;
            resizer.OnResize();
            StateImage _img = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            MogoUtils.AdaptScreen(_img);
            _btnClickOtherAndClose = _img.gameObject.AddComponent<KDummyButton>();
            _btnClose = _content.GetChildComponent<KButton>("Button_shouqi");
            CreateEntryItems(1);
            CreateEntryItems(5);
            CreateEntryItems(4);
            CreateEntryItems(6);

            EventDispatcher.AddEventListener(FunctionEvents.INIT, OnFunctionRefresh);
            EventDispatcher.AddEventListener<int, bool>(FunctionEvents.SET_FUNTION_POINT, OnSetFunctionPoint);
            EventDispatcher.AddEventListener<int>(FunctionEvents.UPDATE_FUNCTION, OnUpdateFunction);
            _btnClose.onClick.AddListener(OnClickClose);
            _btnClickOtherAndClose.onClick.AddListener(OnClickClose);
            _resizer.onChnage.AddListener(OnChange);
            base.Awake();
        }

        private void OnChange()
        {
           // _transform.anchoredPosition += new Vector2(0, 11);
        }

        protected override void OnEnable()
        {
            EventDispatcher.AddEventListener(CLOSE_FUNCTION_ENTRY_VIEW, OnClickClose);
            isVisible = true;
            OnFunctionRefresh();
            if(needTween)
            {
                _isTween = true;
                resizer.canResize = false;
                _rect.anchoredPosition = _orignalPos + new Vector2(0, UIManager.CANVAS_HEIGHT);
                TweenPosition.Begin(_content.gameObject, 0.3f, _orignalPos, OnTweenShowEnd);
            }
            else
            {
                _isTween = false;
                _rect.anchoredPosition = _orignalPos;
                resizer.canResize = true;
                EventDispatcher.TriggerEvent(PanelEvents.PANEL_LAYER_RESET_POS);
            }
            needTween = true;
            base.OnEnable();
        }

        private bool needTween = true;
        public void OnShowWithOutTween()
        {
            needTween = false;
            Visible = true;
        }

        protected override void OnDisable()
        {
            EventDispatcher.RemoveEventListener(CLOSE_FUNCTION_ENTRY_VIEW, OnClickClose);
            isVisible = false;
            base.OnDisable();
        }

        private void OnTweenShowEnd(UITweener tweener)
        {
            _isTween = false;
            resizer.canResize = true;
            EventDispatcher.TriggerEvent(PanelEvents.PANEL_LAYER_RESET_POS);
        }

        private bool _isTween = false;
        private void OnClickClose()
        {
            if(_isTween == false)
            {
                _isTween = true;
                resizer.canResize = false;
                TweenPosition.Begin(_content.gameObject, 0.3f, _orignalPos + new Vector2(0, UIManager.CANVAS_HEIGHT), OnTweenEnd);
            }
        }

        private void OnTweenEnd(UITweener tweener)
        {
            _isTween = false;
            onCloseFunctionEntry.Invoke();
            EventDispatcher.TriggerEvent(PanelEvents.PANEL_LAYER_RESET_POS);
            EventDispatcher.TriggerEvent(CLOSE_END);
        }

        private void OnSetFunctionPoint(int funId, bool isPoint)
        {
            if (_entryItemDict.ContainsKey(funId) == true)
            {
                _entryItemDict[funId].SetPoint(isPoint);
            }
        }

        private void CreateEntryItems(int type)
        {
            List<function> functionList = function_helper.GetFunctionList(type);
            for(int i=0;i<functionList.Count;i++)
            {
                FunctionEntryItem item = CreateItem(type, functionList[i], i + 1);
                _entryItemDict.Add(functionList[i].__id,item);
            }
        }

        private FunctionEntryItem CreateItem(int type,function function,int num)
        {
            GameObject gameObject = GameObject.Instantiate(_template) as GameObject;
            FunctionEntryItem item = gameObject.AddComponent<FunctionEntryItem>();
            item.transform.SetParent(_content.transform);
            item.SetInfo(function, num);
            return item;
        }
        

        private void InitEffectGo()
        {
            Transform child = transform.FindChild("Container_itemTemplate/fx_ui_23_zhiyin_01");
            if (child != null)
            {
                pointGo = child.gameObject;
                pointGo.SetActive(false);
            }
        }

        private void OnUpdateFunction(int functionId)
        {
            if (_entryItemDict.ContainsKey(functionId) && Visible)
            {
                OnFunctionRefresh();
            }
        }

        public void OnFunctionRefresh()
        {
            UpdateEntryItems(1);
            UpdateEntryItems(5);
            UpdateEntryItems(4);
            UpdateEntryItems(6);
        }

        private void UpdateEntryItems(int type)
        {
            List<function> functionList = function_helper.GetFunctionList(type);
            functionList.Sort(SortFunc);
            for (int i = 0; i < functionList.Count; i++)
            {
                if(_entryItemDict.ContainsKey(functionList[i].__id))
                {
                    _entryItemDict[functionList[i].__id].UpdateContent(i+1);
                }
            }
        }

        private static int SortFunc(function func1, function func2)
        {
            bool isFunc1 = PlayerDataManager.Instance.FunctionData.IsFunctionOpen(func1.__id);
            bool isFunc2 = PlayerDataManager.Instance.FunctionData.IsFunctionOpen(func2.__id);
            if(isFunc1!=isFunc2)
            {
                return isFunc1?-1:1;
            }
            return func1.__button_sequence - func2.__button_sequence;
        }

    }
}
