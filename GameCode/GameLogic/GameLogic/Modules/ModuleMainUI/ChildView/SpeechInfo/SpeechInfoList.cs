﻿using Common.Utils;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    public class SpeechInfoListBase : KContainer
    {
        public static readonly Vector3 HIDE_POSITION = new Vector3(-1000, -2000, 0);
        public static readonly Vector3 TEXT_HIDE_POSITION = new Vector3(-1000, -2500, 0);
        public static Vector3 TEXT_SHOW_POSITION;
    }

    public class SpeechInfoList : SpeechInfoListBase
    {
        private static GameObject _speechInfoItem;

        protected override void Awake()
        {
            base.Awake();
            _speechInfoItem = GetChild("Container_speechItem");
            //_speechInfoItem.SetActive(false);
            _speechInfoItem.transform.localPosition = HIDE_POSITION;
            TEXT_SHOW_POSITION = _speechInfoItem.transform.FindChild("Label_txtduihua").localPosition;
            _speechInfoItem.transform.FindChild("Label_txtduihua").localPosition = TEXT_HIDE_POSITION;
        }

        public static SpeechInfoItem AddSpeechInfoItem()
        {
            SpeechInfoItem item = MogoGameObjectHelper.AddByTemplate<SpeechInfoItem>(_speechInfoItem);
            return item;
        }
    }

    public class SpeechInfoCGList : SpeechInfoListBase
    {
        private static GameObject _speechInfoItem;

        protected override void Awake()
        {
            base.Awake();
            _speechInfoItem = GetChild("Container_speechItem");
            _speechInfoItem.gameObject.SetActive(true);
            //_speechInfoItem.SetActive(false);
            _speechInfoItem.transform.localPosition = HIDE_POSITION;
            TEXT_SHOW_POSITION = _speechInfoItem.transform.FindChild("Label_txtduihua").localPosition;
            _speechInfoItem.transform.FindChild("Label_txtduihua").localPosition = TEXT_HIDE_POSITION;
        }

        public static SpeechInfoItem AddSpeechInfoItem()
        {
            SpeechInfoCGItem item = MogoGameObjectHelper.AddByTemplate<SpeechInfoCGItem>(_speechInfoItem);
            return item;
        }
    }
 
}
