﻿using Common.Events;
using Common.ServerConfig;
using Common.Utils;
using GameData;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.RPC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    public class SpeechInfoCGManager : SpeechInfoManagerBase
    {
        private static SpeechInfoCGManager s_Instance;
        public static SpeechInfoCGManager Instance
        {
            get
            {
                if (s_Instance == null)
                    s_Instance = new SpeechInfoCGManager();
                return s_Instance;
            }
        }

        protected override SpeechInfoItem CloneSpeechInfoItem()
        {
            return SpeechInfoCGList.AddSpeechInfoItem();
        }

        public override int MaxSpeechNum
        {
            get
            {
                return int.MaxValue;
            }
        }
    }

    public class SpeechInfoManager : SpeechInfoManagerBase
    {
        private static SpeechInfoManager s_Instance;
        public static SpeechInfoManager Instance
        {
            get
            {
                if (s_Instance == null)
                    s_Instance = new SpeechInfoManager();
                return s_Instance;
            }
        }

        protected override SpeechInfoItem CloneSpeechInfoItem()
        {
            return SpeechInfoList.AddSpeechInfoItem();
        }

        public void LeaveMap()
        {
            RemoveAllSpeechInfoItem();
        }
    }

    public abstract class SpeechInfoManagerBase
    {
        private static int INVALID_VALUE = -1;
        private int _maxSpeechNum = INVALID_VALUE;
        private int _displayTime = INVALID_VALUE;
        private int _fadeOutTime = INVALID_VALUE;

        protected Stack<SpeechInfoItem> _speechInfoItemStack;
        protected Dictionary<uint, SpeechInfoItem> _speechInfoDic;

        public SpeechInfoManagerBase()
        {
            _speechInfoItemStack = new Stack<SpeechInfoItem>();
            _speechInfoDic = new Dictionary<uint, SpeechInfoItem>();
        }

        public void CreateSpeechInfo(uint entityId, int chineseId)
        {
            ///野外不显示冒泡
            if (GameSceneManager.GetInstance().curMapType == public_config.MAP_TYPE_WILD) return;
            if (_speechInfoDic.Count >= MaxSpeechNum) return;
            if (GameData.XMLManager.chinese.ContainsKey(chineseId) == false) return;
            Entity entity = MogoWorld.GetEntity(entityId);
            if (entity != null)
            {
                if (_speechInfoDic.ContainsKey(entityId) == false)
                {
                    SpeechInfoItem speechInfoItem = CreateSpeechInfoItem(entityId);
                    _speechInfoDic.Add(entityId, speechInfoItem);
                    speechInfoItem.Show(entityId, chineseId);
                }
            }
        }

        public void RemoveSpeechInfo(uint entityId)
        {
            if (_speechInfoDic.ContainsKey(entityId) == true)
            {
                SpeechInfoItem speechInfoItem = _speechInfoDic[entityId];
                _speechInfoItemStack.Push(speechInfoItem);
                _speechInfoDic.Remove(entityId);
            }
        }

        private SpeechInfoItem CreateSpeechInfoItem(uint entityId)
        {
            SpeechInfoItem result;
            if (_speechInfoItemStack.Count != 0)
            {
                result = _speechInfoItemStack.Pop();
            }
            else
            {
                result = CloneSpeechInfoItem();
            }
            return result;
        }

        protected abstract SpeechInfoItem CloneSpeechInfoItem();

        protected void RemoveAllSpeechInfoItem()
        {
            foreach (KeyValuePair<uint, SpeechInfoItem> kvp in _speechInfoDic)
            {
                kvp.Value.Hide();
                if (_speechInfoDic.ContainsKey(kvp.Key) == true)
                {
                    SpeechInfoItem speechInfoItem = _speechInfoDic[kvp.Key];
                    _speechInfoItemStack.Push(speechInfoItem);
                }
            }
            _speechInfoDic.Clear();
        }

        public virtual int MaxSpeechNum
        {
            get
            {
                if (_maxSpeechNum == INVALID_VALUE)
                {
                    InitGlobalParams();
                }
                return _maxSpeechNum;
            }
        }

        public int SpeechDisplayTime
        {
            get
            {
                if (_displayTime == INVALID_VALUE)
                {
                    InitGlobalParams();
                }
                return _displayTime;
            }
        }

        public int FadeOutTime
        {
            get
            {
                if (_fadeOutTime == INVALID_VALUE)
                {
                    InitGlobalParams();
                }
                return _fadeOutTime;
            }
        }

        private void InitGlobalParams()
        {
            string str = global_params_helper.GetGlobalParam(111);
            List<int> paramList = MogoStringUtils.Convert2List(str);
            _maxSpeechNum = paramList[0];
            _displayTime = paramList[1];
            _fadeOutTime = paramList[2];
        }
    }
}
