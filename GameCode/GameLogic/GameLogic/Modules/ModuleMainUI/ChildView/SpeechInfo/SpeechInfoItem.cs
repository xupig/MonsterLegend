﻿using Common.Chat;
using Common.Events;
using Common.Global;
using Common.Utils;
using Game.Asset;
using Game.UI;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Object = UnityEngine.Object;
using UnityEngine.UI;

namespace ModuleMainUI
{
    public class SpeechInfoItem : KContainer
    {
        protected uint _entityId = 0;
        private const float INTERVAL = 0.02f;
        private const float BGALPHAVALUE = 178.0f / 255.0f;
        private const float TXTALPHAVALUE = 1.0f;
        private const float VERTICAL_PADDING = 25f;
        private const float HORIZONTAL_PADDING = 17f;
        private const float ARROW_HEIGHT = 26f;
        private float _height = 50f;
        private Vector3 _billboardPosition = Vector3.zero;
        private Vector3 _rectSize;
        private RectTransform _rectTransform;
        private StateImage _bgImage;
        private StateText _txtDialog;

        private float txtMaxWidth ;
        private const float OFFSET = 80.0f;
        private bool isFadeOut = true;
        private float delayFadeOutTimer = 0.0f;  // 用于淡出计时
        private float fadeOutTimer = 0.0f;

        protected override void Awake()
        {
            base.Awake();
            _rectTransform = this.gameObject.GetComponent<RectTransform>();

            _txtDialog = GetChildComponent<StateText>("Label_txtduihua");
            txtMaxWidth = _txtDialog.GetComponent<RectTransform>().sizeDelta.x;
            _bgImage = GetChildComponent<StateImage>("ScaleImage_duihuakuang");
            _bgImage.AddAllStateComponent<Resizer>();
            _rectSize = GetChildComponent<RectTransform>("ScaleImage_duihuakuang").sizeDelta;

        }

        public void Show(uint entityId, int chineseId)
        {
            _entityId = entityId;
            _txtDialog.transform.localPosition = SpeechInfoListBase.TEXT_SHOW_POSITION;
            InitSpeechInfo(chineseId);
            delayFadeOutTimer = (float)SpeechInfoManager.Instance.SpeechDisplayTime / 1000;
            fadeOutTimer = (float)SpeechInfoManager.Instance.FadeOutTime / 1000;
            enabled = true;
        }

        public void Hide()
        {
            _rectTransform.localPosition = SpeechInfoListBase.HIDE_POSITION;
            _txtDialog.transform.localPosition = SpeechInfoListBase.TEXT_HIDE_POSITION;
            _entityId = 0;
            enabled = false;
        }

        void Update()
        {
            delayFadeOutTimer -= Time.deltaTime;

            ///一系列的判断是否需要更新位置和透明度
            if (Camera.main == null) { return; }
            if (_entityId == 0) return;

            UpdateBillboardPosition();

            Vector2 screenPosition = Camera.main.WorldToScreenPoint(_billboardPosition);

            if (delayFadeOutTimer <= 0)
            {
                FadeOut();
            }
            if (_entityId != 0)
            {
                UpdateSpeechInfoPos();
            }
        }

        private void Retrieve()
        {
            RemoveItem();
            Hide();
        }

        protected virtual void RemoveItem()
        {
            SpeechInfoManager.Instance.RemoveSpeechInfo(_entityId);
        }

        private void InitSpeechInfo(int chineseId)
        {
            _txtDialog.CurrentText.text = MogoLanguageUtil.GetContent(chineseId);
            _txtDialog.RecalculateCurrentStateHeight();
            _bgImage.Width = Mathf.Min(txtMaxWidth, _txtDialog.CurrentText.preferredWidth) + 2 * VERTICAL_PADDING;
            _bgImage.Height = _txtDialog.CurrentText.preferredHeight + 2 * HORIZONTAL_PADDING + ARROW_HEIGHT;

            RecalculateSize();

            if (isFadeOut == true)
            {
                SetAlpha2Zero();
            }
        }

        private void UpdateBillboardPosition()
        {
            EntityCreature entity = MogoWorld.GetEntity(_entityId) as EntityCreature;
            if (entity != null && entity.actor != null)
            {
                Transform billboard = entity.controlActor.boneController.GetBoneByName("slot_billboard");
                if (billboard != null)
                {
                    _billboardPosition = billboard.position;
                }
            }
        }

        private void UpdateSpeechInfoPos()
        {
            Vector2 screenPosition = Camera.main.WorldToScreenPoint(_billboardPosition);

            Vector3 localPosition = screenPosition / Global.Scale;
            localPosition.y -= Screen.height / Global.Scale;
            localPosition.x -= (0.5f * _rectSize.x - OFFSET);
            localPosition.y += 0.5f * _rectSize.y;
            localPosition.y += _height;
            _rectTransform.localPosition = localPosition;
        }

        private void SetAlpha2Zero()
        {
            StateImage[] imgWrapper = GetComponentsInChildren<StateImage>();
            for (int i = 0; i < imgWrapper.Length; i++)
            {
                imgWrapper[i].Alpha = BGALPHAVALUE;
            }

            TextWrapper[] txtWrapper = GetComponentsInChildren<TextWrapper>();
            for (int i = 0; i < txtWrapper.Length; i++)
            {
                Color stateColor = txtWrapper[i].color;
                Color color = new Color(stateColor.r, stateColor.g, stateColor.b, TXTALPHAVALUE);
                txtWrapper[i].color = color;
            }
        }

        private void FadeOut()
        {
            fadeOutTimer -= Time.deltaTime;
            if (fadeOutTimer <= 0 || isFadeOut == false)
            {
                Retrieve();
                return;
            }
            float alphaPercent = fadeOutTimer / (SpeechInfoManager.Instance.FadeOutTime / 1000);
            TextWrapper[] txtWrapper = GetComponentsInChildren<TextWrapper>();
            for (int i = 0; i < txtWrapper.Length; i++)
            {
                Color stateColor = txtWrapper[i].color;

                Color color = new Color(stateColor.r, stateColor.g, stateColor.b, TXTALPHAVALUE * alphaPercent);
                txtWrapper[i].color = color;
            }

            StateImage[] imgWrapper = GetComponentsInChildren<StateImage>();
            for (int i = 0; i < imgWrapper.Length; i++)
            {
                imgWrapper[i].Alpha = BGALPHAVALUE * alphaPercent;
            }

        }
    }

    public class SpeechInfoCGItem : SpeechInfoItem
    {
        protected override void RemoveItem()
        {
            SpeechInfoCGManager.Instance.RemoveSpeechInfo(_entityId);
        }
    }
}
