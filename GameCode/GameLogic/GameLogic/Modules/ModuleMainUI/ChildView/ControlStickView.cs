﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Game.UI.UIComponent;
using Common.Global;
using MogoEngine.Events;
using Common.Events;
using GameMain.CombatSystem;
using GameMain.Entities;
using Common.States;
using GameMain.GlobalManager;
using GameLoader.Utils;

namespace ModuleMainUI
{
    public class ControlStickView : KContainer, IPointerDownHandler, IPointerUpHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        private ControlStick _controlStick;
        private Locater _stickLocater;
        private Vector2 _originalPosition;
        private RectTransform _rectTransform;
        private RectTransform _stickRect;

        private bool _isDraging = false;
        private int _fingerId = -1;

        protected override void Awake()
        {
            _controlStick = AddChildComponent<ControlStick>("Container_ControlStick/Image_ControlStick");
            _stickRect = GetChildComponent<RectTransform>("Container_ControlStick/Image_ControlStick");
            _stickLocater = AddChildComponent<Locater>("Container_ControlStick");
            _rectTransform = GetComponent<RectTransform>();
            _originalPosition = _stickLocater.Position;
            //_stickCenterPosition = _stickLocater.Position + new Vector2(_stickLocater.Width / 2, -_stickLocater.Height / 2);
            this.gameObject.AddComponent<KButtonHitArea>();

            EventDispatcher.AddEventListener(MainUIEvents.RESET_CONTROL_STICK, ResetControlStick);
            EventDispatcher.AddEventListener<MogoUILayer>(LayerEvents.HideLayer, OnHideLayer);
        }

        private void OnHideLayer(MogoUILayer layer)
        {
            if (layer == MogoUILayer.LayerUIMain)
            {
                ResetControlStick();
            }
        }

        void LateUpdate()
        {
            //由于EventSystem的响应顺序在Update之前，所以需要在LateUpdate的时候处理
            ControlStickState.isDragging = _isDraging;
            ControlStickState.fingerId = _fingerId;
        }

        public override void OnPointerClick(PointerEventData evtData)
        {
            base.OnPointerClick(evtData);
            OnBeginDrag(evtData);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (_isDraging == true) return;
            _isDraging = true;
            _fingerId = eventData.pointerId;
            ControlStickState.isDragging = _isDraging;
            ControlStickState.fingerId = _fingerId;
            if (eventData.pointerId >= 0 || eventData.pointerId == PointerInputModule.kMouseLeftId)
            {
                Vector2 position;
                if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform,
        eventData.position, eventData.pressEventCamera, out position))
                {
                    position.x -= _stickLocater.Width / 2;
                    position.y += _stickLocater.Height / 2;
                    _stickLocater.Position = position;
                }
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            ResetControlStick();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (eventData.pointerId >= 0 || eventData.pointerId == PointerInputModule.kMouseLeftId)
            {
                EventSystem.current.SetSelectedGameObject(gameObject, eventData);
                EventDispatcher.TriggerEvent(BattleUIEvents.BATTLE_CONTROL_STICK_DOWN);
                PlayerAvatar.Player.moveManager.Stop();
                _controlStick.OnBeginDrag(eventData);
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (eventData.pointerId >= 0 || eventData.pointerId == PointerInputModule.kMouseLeftId)
            {
                if (UIManager.GetLayerVisibility(MogoUILayer.LayerUIMain) == false || _isDraging == false)
                {
                    ResetControlStick();
                    return;
                }
                _controlStick.OnDrag(eventData);
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _controlStick.OnEndDrag(eventData);
        }

        protected override void OnEnable()
        {
            ResetControlStick();
        }

        protected override void OnDisable()
        {
            ResetControlStick();
        }

        private void ResetControlStick()
        {
            _isDraging = false;
            _fingerId = -1;
            _stickLocater.Position = _originalPosition;
            _controlStick.ResetControlStick();
        }

    }
}