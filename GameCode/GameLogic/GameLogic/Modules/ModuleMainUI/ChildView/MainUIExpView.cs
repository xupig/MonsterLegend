﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using UnityEngine;
using MogoEngine.Events;
using GameLoader.Utils;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Mgrs;
using GameMain.Entities;
using Common.ClientConfig;
using GameData;
using UnityEngine.UI;

namespace ModuleMainUI
{
    public class MainUIExpView : KContainer
    {
        private StateText _expTxt;
        private StateImage _expImgLeft;
        private StateImage _expImgRight;

        protected override void Awake()
        {
            _expTxt = this.GetChildComponent<StateText>("Label_txtNr");
            _expImgLeft = GetChildComponent<StateImage>("Container_jingyantiao/Image_shengmingtiaolanse01");
            _expImgRight = GetChildComponent<StateImage>("Container_jingyantiao/Image_shengmingtiaolanse02");
            AddEventListener();
            UpdateExp();
            InitImage();
        }

        private void InitImage()
        {
            _expImgLeft.CurrentImage.type = Image.Type.Filled;
            _expImgLeft.CurrentImage.fillMethod = Image.FillMethod.Horizontal;
            _expImgLeft.CurrentImage.fillOrigin = (int)Image.OriginHorizontal.Left;
            _expImgRight.CurrentImage.type = Image.Type.Filled;
            _expImgRight.CurrentImage.fillMethod = Image.FillMethod.Horizontal;
            _expImgRight.CurrentImage.fillOrigin = (int)Image.OriginHorizontal.Left;
        }

        private void AddEventListener( )
        {
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.exp, UpdateExp);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, UpdateExp);
        }

        private void RemoveEventListener( )
        {
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.exp, UpdateExp);
			EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, UpdateExp);
        }

        private void UpdateExp()
        {
            if (PlayerAvatar.Player == null) return;
            PlayerAvatar avatar = PlayerAvatar.Player;
            int curExp = avatar.exp;
            int level = Math.Max( (int) avatar.level, 1 );
            float percent = avatar_level_helper.GetExpPercent(curExp, level);
            int nextLevelExp = avatar_level_helper.GetNextLevelExp(level);
            if (nextLevelExp == 0) nextLevelExp = curExp;
            _expTxt.CurrentText.text = string.Format("{0}/{1}", curExp, nextLevelExp);
            if (percent <= 0.5)
            {
                _expImgLeft.CurrentImage.fillAmount = percent * 2;
                _expImgRight.CurrentImage.fillAmount = 0;
            }
            else
            {
                _expImgLeft.CurrentImage.fillAmount = 1;
                _expImgRight.CurrentImage.fillAmount = (percent - 0.5f) * 2;
            }
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }
      
    }
}
