﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using UnityEngine;
using MogoEngine.Events;
using GameLoader.Utils;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using Common.Structs.ProtoBuf;
using MogoEngine;
using MogoEngine.RPC;
using GameMain.Entities;
using GameData;
using Common.ServerConfig;
using GameMain.GlobalManager.SubSystem;

namespace ModuleMainUI
{
    public class BattleTeamView : KContainer
    {
        private KList _teamMemberList;
		private KButton _buffButton;
        protected override void Awake()
        {
            _teamMemberList = GetChildComponent<KList>("List_zudui");
            _teamMemberList.SetDirection(KList.KListDirection.LeftToRight, 1);
            _buffButton = GetChildComponent<KButton>("Button_qianghuaBuff");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            RefreshMemberList();
            FriendManager.Instance.SendGetFriendListMsg();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(TeamInstanceEvents.Teammate_Refresh, RefreshMemberList);
            EventDispatcher.AddEventListener(BuffEvents.BUFF_REFRESH, RefreshBuffBtnState);
            _buffButton.onClick.AddListener(OnBuffBtnClick);
        }

        private bool NeedShowBuff()
        {
            List<int> buffList = PlayerAvatar.Player.bufferManager.GetCurBufferIDList();
            for (int i = 0; i < buffList.Count; i++)
            {
                if (buff_helper.NeedShowInMainUI(buffList[i]))
                {
                    return true;
                }
            }
            return false;
        }

        private void OnBuffBtnClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.BuffDetail);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(TeamInstanceEvents.Teammate_Refresh, RefreshMemberList);
            EventDispatcher.RemoveEventListener(BuffEvents.BUFF_REFRESH, RefreshBuffBtnState);
            _buffButton.onClick.RemoveListener(OnBuffBtnClick);
        }

        private void RefreshBuffBtnState()
        {
            _buffButton.Visible = NeedShowBuff();
        }

        private void RefreshMemberList()
        {
            Dictionary<uint, PbHpMember> teamMateDic = PlayerDataManager.Instance.TeamInstanceData.HpMemberDic;
            List<PbHpMember> memberList = new List<PbHpMember>(teamMateDic.Values);
            ulong captainId = PlayerDataManager.Instance.TeamData.CaptainId;
            _teamMemberList.RemoveAll();
            foreach (PbHpMember member in teamMateDic.Values)
            {
                if (member.dbid == PlayerAvatar.Player.dbid)
                {
                    continue;
                }
                if (member.dbid == captainId)
                {
                    _teamMemberList.AddItemAt<BattleMemberItem>(member, 0, false);
                    continue;
                }
                _teamMemberList.AddItem<BattleMemberItem>(member, false);
            }
            _teamMemberList.DoLayout();
        }

    }
}
