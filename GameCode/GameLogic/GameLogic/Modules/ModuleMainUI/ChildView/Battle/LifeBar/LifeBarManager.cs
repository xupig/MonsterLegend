﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MogoEngine.Events;
using UnityEngine;
using Common.Data;
using Common.Base;

using MogoEngine.RPC;
using Common.Events;
using Common.Global;
using GameMain.GlobalManager;
using MogoEngine;
using GameData;
using GameMain.Entities;
using GameMain.CombatSystem;
using Common.ServerConfig;

namespace ModuleMainUI
{
    public class LifeBarTypeComparer : IEqualityComparer<LifeBarType>
    {
        public bool Equals(LifeBarType x, LifeBarType y)
        {
            return (int)x == (int)y;
        }

        public int GetHashCode(LifeBarType obj)
        {
            return (int)obj;
        }
    }

    public class LifeBarManager
    {
        private List<uint> _bossEntityIdList;
        private Dictionary<LifeBarType, Stack<LifeBarItem>> _lifeBarItemStackDict;  //保存当前空闲的生命条，以重复利用
        private Stack<PointingArrow> _pointingArrowStack;  //保存当前空闲的箭头指示，以重复利用
        private Dictionary<uint, LifeBarView> _lifeBarViewDic;
        private Stack<LifeBarView> _lifeBarViewStack;

        public LifeBarManager()
        {
            _lifeBarItemStackDict = new Dictionary<LifeBarType, Stack<LifeBarItem>>(new LifeBarTypeComparer());
            foreach (LifeBarType type in Enum.GetValues(typeof(LifeBarType)))
            {
                _lifeBarItemStackDict.Add(type, new Stack<LifeBarItem>());
            }
            _pointingArrowStack = new Stack<PointingArrow>();
            _lifeBarViewDic = new Dictionary<uint, LifeBarView>();
            _lifeBarViewStack = new Stack<LifeBarView>();
            _bossEntityIdList = new List<uint>();
        }

        private static LifeBarManager s_Instance;
        public static LifeBarManager Instance
        {
            get
            {
                if (s_Instance == null)
                    s_Instance = new LifeBarManager();
                return s_Instance;
            }
        }


        public void CreateLifeBar(uint entityId)
        {
            if (Common.States.GMState.showLifeBar == false) return;
            Entity entity = MogoWorld.GetEntity(entityId);
            if (entity != null)
            {
                if (GetLifeBarType(entityId) == LifeBarType.NULL) return;
                else if (GetLifeBarType(entityId) == LifeBarType.BOSS)
                {
                    _bossEntityIdList.Add(entityId);
                    EventDispatcher.TriggerEvent(BattleUIEvents.REFRESH_BOSS_VIEW);
                    return;
                }
                if (_lifeBarViewDic.ContainsKey(entityId) == false)
                {
                    LifeBarView lifeBarView;
                    if (_lifeBarViewStack.Count != 0)
                    {
                        lifeBarView = _lifeBarViewStack.Pop();
                    }
                    else
                    {
                        lifeBarView = LifeBarList.AddLifeBarView();
                    }
                    _lifeBarViewDic.Add(entityId, lifeBarView);
                    lifeBarView.Show(entityId);
                }
            }
        }

        public void RemoveLifeBar(uint entityId)
        {
            if (Common.States.GMState.showLifeBar == false) return;
            Entity entity = MogoWorld.GetEntity(entityId);
            if (entity != null)
            {
                if (GetLifeBarType(entityId) == LifeBarType.NULL) return;
                else if (GetLifeBarType(entityId) == LifeBarType.BOSS)
                {
                    _bossEntityIdList.Remove(entityId);
                    EventDispatcher.TriggerEvent(BattleUIEvents.REFRESH_BOSS_VIEW);
                    return;
                }
                if (_lifeBarViewDic.ContainsKey(entityId))
                {
                    LifeBarView lifeBarView = _lifeBarViewDic[entityId];
                    lifeBarView.Hide();
                    _lifeBarViewStack.Push(lifeBarView);
                    _lifeBarViewDic.Remove(entityId);
                }
            }
        }

        public LifeBarItem CreateLifeBarItem(uint entityId)
        {
            LifeBarItem lifeBarItem;
            LifeBarType type = GetLifeBarType(entityId);
            if (_lifeBarItemStackDict[type].Count != 0)
            {
                lifeBarItem = _lifeBarItemStackDict[type].Pop();
            }
            else
            {
                lifeBarItem = LifeBarList.AddLifeBarItem(type);
            }
            lifeBarItem.Show(entityId);
            return lifeBarItem;
        }

        public LifeBarType GetLifeBarType(uint entityId)
        {
            if (entityId == PlayerAvatar.Player.id) return LifeBarType.NULL;
            Entity entity = MogoWorld.GetEntity(entityId);
            if (TargetFilter.IsEnemy(PlayerAvatar.Player, entity as EntityCreature))
            {
                if (entity is EntityMonsterBase)
                {
                    List<int> paramList = monster_helper.GetMonsterHpShow((int)(entity as EntityMonsterBase).monster_id);
                    if (paramList == null || paramList.Count == 0 || paramList[0] == 0)
                    {
                        return LifeBarType.RED;
                    }
                    else if (paramList[0] == -1)
                    {
                        return LifeBarType.NULL;
                    }
                    else if (paramList[0] == 1)
                    {
                        return LifeBarType.BOSS;
                    }
                    else if (paramList[0] == 2)
                    {
                        return LifeBarType.ELITE_RED;
                    }
                    else if (paramList[0] == 3)
                    {
                        return LifeBarType.FIELDBOSS;
                    }
                    return LifeBarType.RED;
                }
                else if (entity is EntityAvatar)
                {
                    return LifeBarType.ELITE_RED;
                }
                return LifeBarType.RED;
            }
            else
            {
                if (entity is EntityAvatar)
                {
                    
                    return LifeBarType.ELITE_BLUE;
                }
                return LifeBarType.BLUE;
            }
        }

        public void RemoveLifeBarItem(LifeBarItem lifeBarItem)
        {
            lifeBarItem.Hide();
            _lifeBarItemStackDict[lifeBarItem.Type].Push(lifeBarItem);
        }

        public bool IsShowPointingWayDirection()
        {
            int lifeBarItemCount = GetLifeBarItemCount();
            return lifeBarItemCount == 0;
        }

        private int GetLifeBarItemCount()
        {
            int result = 0;
            foreach (KeyValuePair<uint, LifeBarView> kvp in _lifeBarViewDic)
            {
                if (kvp.Value.HasLifeBar)
                {
                    result++;
                }
            }
            if (_bossEntityIdList.Count != 0)
            {
                result++;
            }
            return result;
        }

        public PointingArrow CreatePointingArrow(uint id)
        {
            PointingArrow pointingArrow;
            if (_pointingArrowStack.Count != 0)
            {
                pointingArrow = _pointingArrowStack.Pop();
            }
            else
            {
                pointingArrow = LifeBarList.AddPointingArrowItem();
            }
            pointingArrow.Show(id);
            return pointingArrow;
        }

        public void RemovePointingArrow(PointingArrow pointingArrow)
        {
            pointingArrow.Hide();
            _pointingArrowStack.Push(pointingArrow);
        }

        public void RemoveAllLifeBarView()
        {
            foreach (KeyValuePair<uint, LifeBarView> kvp in _lifeBarViewDic)
            {
                kvp.Value.Hide();
                _lifeBarViewStack.Push(kvp.Value);
            }
            _lifeBarViewDic.Clear();
        }

        public void LeaveMap()
        {
            RemoveAllLifeBarView();
            _bossEntityIdList.Clear();
        }

        public uint GetBossEntityId()
        {
            if (_bossEntityIdList.Count != 0)
            {
                return _bossEntityIdList[0];
            }
            return 0;
        }

    }
}
