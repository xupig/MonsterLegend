﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using UnityEngine.EventSystems;
using UnityEngine;
using MogoEngine.Events;
using Common.Data;
using Game.UI.UIComponent;
using GameMain.Entities;

using MogoEngine.RPC;
using MogoEngine;
using Common.Events;
using GameMain.GlobalManager;
using Common.Global;
using GameLoader.Utils.Timer;

namespace ModuleMainUI
{
    public class PointingArrow : KContainer
    {
        private uint _entityId;

        private RectTransform _rectTransform;
        private RectTransform _imgRect;
        private const float BOTTOM_PADDING = 0;
        private Vector2[] _rectPoint = new Vector2[5];
        private Vector2 _centerPosition = new Vector2(Screen.width / 2, Screen.height / 2);

        protected override void Awake()
        {
            KContainer container = this.gameObject.GetComponent<KContainer>();
            _rectTransform = container.GetComponent<RectTransform>();
            _imgRect = container.GetChildComponent<RectTransform>("Image_gwsjzy");
            _imgRect.pivot = new Vector2(0.5f, 0f);

            _rectPoint[0] = new Vector2(0, BOTTOM_PADDING);
            _rectPoint[1] = new Vector2(Screen.width, BOTTOM_PADDING);
            _rectPoint[2] = new Vector2(Screen.width, Screen.height);
            _rectPoint[3] = new Vector2(0, Screen.height);
            _rectPoint[4] = new Vector2(0, BOTTOM_PADDING);
        }

        public void Show(uint id)
        {
            _entityId = id;
            enabled = true;
        }

        public void Hide()
        {
            _entityId = 0;
            _rectTransform.localPosition = LifeBarList.HIDE_POSITION;
            enabled = false;
        }

        void Update()
        {
            if (Camera.main == null) { return; }
            EntityCreature entity = MogoWorld.GetEntity(_entityId) as EntityCreature;
            if (entity == null || entity.controlActor == null) { return; }
            Vector2 vector = GeometryUtil.GetTargetVector(entity.controlActor.position);
            Vector2 endPosition = _centerPosition + vector * 10000;
            Vector2 intersection;
            for (int i = 0; i < 4; i++)
            {
                if (GeometryUtil.TryGetIntersection(_centerPosition, endPosition, _rectPoint[i], _rectPoint[i + 1], out intersection))
                {
                    SetPosition(intersection);
                    SetRotation(vector);
                    break;
                }
            }
        }

        private void SetRotation(Vector2 vector)
        {
            float angle = GeometryUtil.CalculateRotation(vector);
            Vector3 eulerAngles = _imgRect.eulerAngles;
            eulerAngles.z = 180 - angle;
            if (_imgRect.eulerAngles != eulerAngles)
            {
                _imgRect.eulerAngles = eulerAngles;
            }
        }

        private void SetPosition(Vector2 intersection)
        {
            Vector3 localPosition = new Vector3(intersection.x, intersection.y, 0);
            localPosition /= Global.Scale;
            localPosition.y -= Screen.height / Global.Scale;
            if (_rectTransform.localPosition != localPosition)
            {
                _rectTransform.localPosition = localPosition;
            }
        }


    }
}
