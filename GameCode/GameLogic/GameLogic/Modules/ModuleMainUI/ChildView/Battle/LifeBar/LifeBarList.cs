﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Game.UI.UIComponent;
using Common.Utils;
using GameMain.GlobalManager;
using Common.ExtendComponent;

namespace ModuleMainUI
{
    public class LifeBarList : KContainer
    {
        private static GameObject _lifeBarItemGo1;
        private static GameObject _lifeBarItemGo2;
        private static GameObject _lifeBarItemGo3;
        private static GameObject _lifeBarItemGo4;
        private static GameObject _lifeBarBoss;
        private static GameObject _pointingArrowGo;
        private static GameObject _lifeBarViewGo;
        private IconContainer _teamDutyIcon;

        public static readonly Vector3 HIDE_POSITION = new Vector3(-1000, -1200, 0);

        protected override void Awake()
        {
            KContainer container = this.GetComponent<KContainer>();
            _lifeBarItemGo1 = container.GetChild("Container_lifeBarItem1");
            _lifeBarItemGo2 = container.GetChild("Container_lifeBarItem2");
            _lifeBarItemGo3 = container.GetChild("Container_lifeBarItem3");
            _lifeBarItemGo4 = container.GetChild("Container_lifeBarItem4");
            _lifeBarBoss = container.GetChild("Container_lifeBarBoss");
            _pointingArrowGo = container.GetChild("Container_pointing");
            _lifeBarViewGo = container.GetChild("Container_lifeBar");
            _lifeBarItemGo1.transform.localPosition = HIDE_POSITION;
            _lifeBarItemGo2.transform.localPosition = HIDE_POSITION;
            _lifeBarItemGo3.transform.localPosition = HIDE_POSITION;
            _lifeBarItemGo4.transform.localPosition = HIDE_POSITION;
            _lifeBarBoss.transform.localPosition = HIDE_POSITION;
            _pointingArrowGo.transform.localPosition = HIDE_POSITION;
            _lifeBarViewGo.transform.localPosition = HIDE_POSITION;
        }

        public static LifeBarItem AddLifeBarItem(LifeBarType type)
        {
            LifeBarItem item = null;
            switch (type)
            {
                case LifeBarType.RED:
                    item = MogoGameObjectHelper.AddByTemplate<LifeBarItem>(_lifeBarItemGo1);
                    item.Type = LifeBarType.RED;
                    break;
                case LifeBarType.BLUE:
                    item = MogoGameObjectHelper.AddByTemplate<LifeBarItem>(_lifeBarItemGo2);
                    item.Type = LifeBarType.BLUE;
                    break;
                case LifeBarType.ELITE_RED:
                    item = MogoGameObjectHelper.AddByTemplate<LifeBarItem>(_lifeBarItemGo3);
                    item.Type = LifeBarType.ELITE_RED;
                    break;
                case LifeBarType.ELITE_BLUE:
                    item = MogoGameObjectHelper.AddByTemplate<LifeBarItem>(_lifeBarItemGo4);
                    item.Type = LifeBarType.ELITE_BLUE;
                    break;
                case LifeBarType.FIELDBOSS:
                    item = MogoGameObjectHelper.AddByTemplate<FieldBossLifeBar>(_lifeBarBoss);
                    item.Type = LifeBarType.FIELDBOSS;
                    break;
            }
            return item;
        }

        public static PointingArrow AddPointingArrowItem()
        {
            PointingArrow item = MogoGameObjectHelper.AddByTemplate<PointingArrow>(_pointingArrowGo);
            return item;
        }

        public static LifeBarView AddLifeBarView()
        {
            LifeBarView item = MogoGameObjectHelper.AddByTemplate<LifeBarView>(_lifeBarViewGo);
            return item;
        }

    }

    public enum LifeBarType
    {
        NULL = -1,
        RED = 0,
        BOSS = 1,
        ELITE_RED = 2,
        FIELDBOSS = 3,
        BLUE = 10,
        ELITE_BLUE = 11,
    }
}
