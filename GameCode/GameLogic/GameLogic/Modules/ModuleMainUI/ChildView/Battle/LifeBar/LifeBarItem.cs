﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using UnityEngine.EventSystems;
using UnityEngine;
using MogoEngine.Events;
using Common.Data;
using Game.UI.UIComponent;
using GameMain.Entities;

using MogoEngine.RPC;
using MogoEngine;
using Common.Events;
using GameMain.GlobalManager;
using Common.Global;
using Common.ExtendComponent;

namespace ModuleMainUI
{
    public class LifeBarItem : KContainer
    {
        public LifeBarType Type { get; set; }
        private float _grayPercent = 1f;
        public float Current { get { return _grayPercent; } }
        protected uint _entityId;

        private float _height = 0f;
        private const float _damping = 0.5f;
        private float _ref = 0f;
        private RectTransform _rectTransform;
        private Vector3 _rectSize;

        private KProgressBar _red;
        private KProgressBar _gray;

        private bool _firstShow = false;
        private IconContainer _teamDutyIcon;
        private float teamDutyIconWidth;

        protected override void Awake()
        {
            _rectSize = GetComponent<RectTransform>().sizeDelta;
            _red = GetChildComponent<KProgressBar>("ProgressBar_redBar");
            _gray = GetChildComponent<KProgressBar>("ProgressBar_grayBar");
            _teamDutyIcon = AddChildComponent<IconContainer>("Container_teamDuty");
            _rectTransform = this.gameObject.GetComponent<RectTransform>();
        }

        public void Show(uint id)
        {
            _entityId = id;
            RefreshDutyIcon();
            _ref = 0f;
            _grayPercent = 1f;
            _firstShow = true;
            enabled = true;
        }

        private void RefreshDutyIcon()
        {
            int iconId = PlayerDataManager.Instance.TeamData.GetPlayerDutyIconByEntityId(_entityId);
            if (iconId != 0)
            {
                if (_teamDutyIcon != null)
                {
                    _teamDutyIcon.Visible = true;
                    _teamDutyIcon.SetIcon(iconId);
                }
            }
            else
            {
                if (_teamDutyIcon != null)
                {
                    _teamDutyIcon.Visible = false;
                }
            }
        }

        public void Hide()
        {
            _entityId = 0;
            _rectTransform.localPosition = LifeBarList.HIDE_POSITION;
            enabled = false;
        }

        private bool IsActorExist(EntityCreature entity)
        {
            if (entity == null || entity.controlActor == null)
            {
                Hide();
                return false;
            }
            return true;
        }

        private bool IsBillboardExist(Transform billboard)
        {
            if (billboard == null)
            {
                Hide();
                return false;
            }
            return true;
        }

        void Update()
        {
            if (Camera.main == null) { return; }
            EntityCreature entity = MogoWorld.GetEntity(_entityId) as EntityCreature;
            if (IsActorExist(entity) == false) return;
            Transform billboard = entity.controlActor.boneController.GetBoneByName("slot_billboard");
            if (IsBillboardExist(billboard) == false) return;
            Vector2 screenPosition = Camera.main.WorldToScreenPoint(billboard.position);
            RefreshContent(screenPosition, entity.cur_hp, entity.max_hp);
        }

        private void RefreshContent(Vector3 screenPosition, int currentHp, int maxHp)
        {
            float percent = (float)currentHp / maxHp;
            _red.Value = percent;
            if (_firstShow)
            {
                _firstShow = false;
                _grayPercent = percent;
            }
            _grayPercent = Mathf.SmoothDamp(_grayPercent, percent, ref _ref, _damping);
            _gray.Value = _grayPercent;
            SetPosition(screenPosition);
        }

        private float TeamDutyWidth
        {
            get
            {
                if (_teamDutyIcon == null || _teamDutyIcon.Visible == false)
                {
                    return 38;
                }
                else
                {
                    return 0;
                }
            }
        }
        private void SetPosition(Vector3 screenPosition)
        {
            Vector3 localPosition = screenPosition / Global.Scale;
            localPosition.y += _height;
            localPosition.y -= Screen.height / Global.Scale;
            localPosition.x -= 0.5f * (_rectSize.x + TeamDutyWidth);
            localPosition.y += 0.5f * (_rectSize.y - 10);
            if (_rectTransform.localPosition != localPosition)
            {
                _rectTransform.localPosition = localPosition;
            }
        }
    }
}
