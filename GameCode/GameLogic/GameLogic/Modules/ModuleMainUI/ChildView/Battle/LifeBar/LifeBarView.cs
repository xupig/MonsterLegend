﻿using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.CombatSystem;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine;
using UnityEngine;

namespace ModuleMainUI
{
    public class LifeBarView : KContainer
    {
        private uint _entityId;
        private LifeBarItem _lifeBarItem;
        private PointingArrow _pointingArrow;

        private float elapsed = 0f;
        private const float CHECK_INTERVAL = 1.0f;

        public void Show(uint id)
        {
            _entityId = id;
            enabled = true;
        }

        public void Hide()
        {
            _entityId = 0;
            RemoveLifeBarItem();
            RemovePointingArrow();
            enabled = false;
        }

        private bool IsActorExist(EntityCreature entity)
        {
            if (entity == null || entity.controlActor == null)
            {
                Hide();
                return false;
            }
            return true;
        }

        private bool IsBillboardExist(Transform billboard)
        {
            if (billboard == null)
            {
                Hide();
                return false;
            }
            return true;
        }

        void Update()
        {
            elapsed += Time.deltaTime;
            if (elapsed > CHECK_INTERVAL)
            {
                elapsed -= CHECK_INTERVAL;
                Check();
            }
        }

        private void Check()
        {
            if (Camera.main == null) { return; }
            EntityCreature entity = MogoWorld.GetEntity(_entityId) as EntityCreature;
            if (IsActorExist(entity) == false) return;
            CheckPointingArrow(entity);
            Transform billboard = entity.controlActor.boneController.GetBoneByName("slot_billboard");
            if (IsBillboardExist(billboard) == false) return;
            CheckLifeBar(entity, billboard);
        }

        private void CheckPointingArrow(EntityCreature entity)
        {
            if (TargetFilter.IsEnemy(PlayerAvatar.Player, entity) == false) return;
            Vector2 screenPosition = Camera.main.WorldToScreenPoint(entity.actor.position);
            if (MogoGameObjectHelper.CheckPositionIsInScreen(screenPosition) == false && Common.States.GMState.showPointingArrow == true)
            {
                if (entity.cur_hp <= 0)
                {
                    RemovePointingArrow();
                    return;
                }
                ShowPointingArrow();
            }
            else
            {
                RemovePointingArrow();
            }
        }

        private void CheckLifeBar(EntityCreature entity, Transform billboard)
        {
            Vector2 screenPosition = Camera.main.WorldToScreenPoint(billboard.position);
            if (MogoGameObjectHelper.CheckPositionIsInScreen(screenPosition) == true)
            {
                if (entity.cur_hp ==0)
                {
                    RemoveLifeBarItem();
                    return;
                }
                bool isEnemy = TargetFilter.IsEnemy(PlayerAvatar.Player, entity as EntityCreature);
                if (IsHideFullHp(entity))
                {
                    if (isEnemy && _hasRedLifeBar == false)
                    {
                        _hasRedLifeBar = true;
                    }
                    if (_lifeBarItem != null)
                    {
                        LifeBarManager.Instance.RemoveLifeBarItem(_lifeBarItem);
                        _lifeBarItem = null;
                    }
                    return;
                }
                ShowLifeBarItem();
                if (isEnemy && _hasRedLifeBar == false)
                {
                    _hasRedLifeBar = true;
                }
            }
            else
            {
                RemoveLifeBarItem();
            }
        }

        private static bool IsHideFullHp(EntityCreature entity)
        {
            if (map_helper.ShowFullBlood(GameSceneManager.GetInstance().curMapID))
            {
                return false;
            }
            return entity.cur_hp == entity.max_hp;
        }

        private void ShowLifeBarItem()
        {
            if (_lifeBarItem == null)
            {
                _lifeBarItem = LifeBarManager.Instance.CreateLifeBarItem(_entityId);
            }
        }

        private void RemoveLifeBarItem()
        {
            if (_hasRedLifeBar == true)
            {
                _hasRedLifeBar = false;
            }
            if (_lifeBarItem != null)
            {
                LifeBarManager.Instance.RemoveLifeBarItem(_lifeBarItem);
                _lifeBarItem = null;
            }
        }

        private void ShowPointingArrow()
        {
            if (_pointingArrow == null)
            {
                _pointingArrow = LifeBarManager.Instance.CreatePointingArrow(_entityId);
            }
        }

        private void RemovePointingArrow()
        {
            if (_pointingArrow != null)
            {
                LifeBarManager.Instance.RemovePointingArrow(_pointingArrow);
                _pointingArrow = null;
            }
        }

        private bool _hasRedLifeBar = false;
        public bool HasLifeBar
        {
            get
            {
                return _hasRedLifeBar;
            }
        }

    }
}
