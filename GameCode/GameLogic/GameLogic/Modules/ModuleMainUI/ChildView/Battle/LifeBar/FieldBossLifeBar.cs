﻿using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;

namespace ModuleMainUI
{
    public class FieldBossLifeBar : LifeBarItem
    {
        private KButton _shareButton;

        protected override void Awake()
        {
            base.Awake();
            _shareButton = GetChildComponent<KButton>("Button_fenxiang");
            _shareButton.onClick.AddListener(OnShareButtonClick);
        }

        private void OnShareButtonClick()
        {
            TreasureManager.Instance.ShareBossPosition(_entityId, GameSceneManager.GetInstance().curMapID, (int)PlayerAvatar.Player.curLine, PlayerAvatar.Player.position, 2);
        }

    }
}
