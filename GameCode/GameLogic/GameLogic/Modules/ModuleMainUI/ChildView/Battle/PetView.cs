﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleMainUI
{
    public class PetView : KContainer
    {
        private BattlePetData battlePetData
        {
            get {return PlayerDataManager.Instance.BattlePetData;}
        }

        private PetInfo _currentPetInfo;
        private bool _inited = false;
        private bool _isShowingList = false;

        private KList _list;
        private KDummyButton _btnHotArea;
        private GameObject _goInactiveList;
        private KDummyButton _btnBg;
        private KButton _btnRevive;

        private KProgressBar _progress;
        private StateText _textLevel;
        private KDummyButton _btnHead;
        private BattlePetStarList _starList;
        private ItemGrid _itemGrid;
        private StateText _textNoPet;
        private BattlePetCountdown _countdown;

        protected override void Awake()
        {
            InitList();
            InitHead();
        }

        private void InitList()
        {
            _list = GetChildComponent<KList>("Container_dengdailiebiao/List_content");
            _list.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _list.SetGap(0, 10);
            _btnHotArea = AddChildComponent<KDummyButton>("Container_dengdailiebiao/Container_hotArea");
            _btnBg = AddChildComponent<KDummyButton>("Container_dengdailiebiao/ScaleImage_chongwudi");
            _goInactiveList = GetChild("Container_dengdailiebiao");
            _textNoPet = GetChildComponent<StateText>("Container_dengdailiebiao/Label_txtNoPet");
        }

        private void InitHead()
        {
            _progress = GetChildComponent<KProgressBar>("Container_chongwutouxiang/ProgressBar_jindu");
            _textLevel = GetChildComponent<StateText>("Container_chongwutouxiang/Label_txtDengji");
            _btnHead = AddChildComponent<KDummyButton>("Container_chongwutouxiang");
            _btnRevive = GetChildComponent<KButton>("Container_chongwutouxiang/Button_fuhuo");
            _itemGrid = GetChildComponent<ItemGrid>("Container_chongwutouxiang/Container_wupin");
            _itemGrid.SetGridBtnEnable(false);
            _starList = AddChildComponent<BattlePetStarList>("Container_chongwutouxiang/Container_xingxing");
            _countdown = AddChildComponent<BattlePetCountdown>("Container_chongwutouxiang/Container_meishuzi");
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnHead.onClick.AddListener(OnClickHead);
            _btnHotArea.onClick.AddListener(OnHotAreaClick);
            _btnBg.onClick.AddListener(OnHotAreaClick);
            _btnRevive.onClick.AddListener(OnRevive);
            EventDispatcher.AddEventListener(BattleUIEvents.UPDATE_PET_FIGHT_STATE, Refresh);
            EventDispatcher.AddEventListener(BattleUIEvents.BATTLE_CONTROL_STICK_DOWN, OnHotAreaClick);
            EventDispatcher.AddEventListener<float>(BattleUIEvents.BATTLE_PET_REFRESH_HP, OnPetRefreshHp);
            EventDispatcher.AddEventListener(BattleUIEvents.BATTLE_PET_DATA_INITED, Show);
            EventDispatcher.AddEventListener<int>(BattleUIEvents.BATTLE_PET_REPLACE_CLICK, OnPetReplaceClick);
            EventDispatcher.AddEventListener(PetEvents.ReceiveIdleToFight, Refresh);
            EventDispatcher.AddEventListener(PetEvents.ReceiveFightToIdle, Refresh);
        }

        private void RemoveListener()
        {
            _btnHead.onClick.RemoveListener(OnClickHead);
            _btnHotArea.onClick.RemoveListener(OnHotAreaClick);
            _btnBg.onClick.RemoveListener(OnHotAreaClick);
            _btnRevive.onClick.RemoveListener(OnRevive);
            EventDispatcher.RemoveEventListener(BattleUIEvents.UPDATE_PET_FIGHT_STATE, Refresh);
            EventDispatcher.RemoveEventListener(BattleUIEvents.BATTLE_CONTROL_STICK_DOWN, OnHotAreaClick);
            EventDispatcher.RemoveEventListener(BattleUIEvents.BATTLE_PET_DATA_INITED, Show);
            EventDispatcher.RemoveEventListener<float>(BattleUIEvents.BATTLE_PET_REFRESH_HP, OnPetRefreshHp);
            EventDispatcher.RemoveEventListener<int>(BattleUIEvents.BATTLE_PET_REPLACE_CLICK, OnPetReplaceClick);
            EventDispatcher.RemoveEventListener(PetEvents.ReceiveIdleToFight, Refresh);
            EventDispatcher.RemoveEventListener(PetEvents.ReceiveFightToIdle, Refresh);
        }

        private void OnHotAreaClick()
        {
            IsShowingList = false;
        }

        private void OnClickHead()
        {
            IsShowingList = !IsShowingList;
            if (IsShowingList)
            {
                RefreshList(battlePetData.GetInactivePetInfoList());
            }
        }

        private void OnPetReplaceClick(int petId)
        {
            IsShowingList = false;
            battlePetData.RequestReplacePetInBattle(petId);
        }

        private void OnPetRefreshHp(float rate)
        {
            _progress.Value = rate;
        }

        private void OnRevive()
        {
            BaseItemData baseItemData = GetCost();
            MessageBox.Show(true, MessageBox.DEFAULT_TITLE,
                MogoLanguageUtil.GetContent(43456, baseItemData.StackCount, baseItemData.Name), OnSure);
        }

        private BaseItemData GetCost()
        {
            BaseItemData totalBaseItemData = ItemDataCreator.Create(public_config.MONEY_TYPE_BIND_DIAMOND);
            totalBaseItemData.StackCount = 0;
            PetInfo petInfo;
            List<PetInfo> inactivePetInfoList = battlePetData.GetInactivePetInfoList();
            for (int i = 0; i < inactivePetInfoList.Count; i++)
            {
                petInfo = inactivePetInfoList[i];
                if (petInfo.FightState == PetFightState.dead)
                {
                    BaseItemData baseItemData = global_params_helper.GetPetReviveCost(petInfo.Level);
                    totalBaseItemData.StackCount += baseItemData.StackCount;
                }
            }
            petInfo = battlePetData.GetCurrentPetInfo();
            if (petInfo != null && petInfo.FightState == PetFightState.dead)
            {
                BaseItemData baseItemData = global_params_helper.GetPetReviveCost(petInfo.Level);
                totalBaseItemData.StackCount += baseItemData.StackCount;
            }
            return totalBaseItemData;
        }

        private void OnSure()
        {
            PetManager.Instance.RequestRevive();
        }

        public void Show()
        {
            int instanceId = GameSceneManager.GetInstance().curInstanceID;
            ChapterType chapterType = instance_helper.GetChapterType(instanceId);
            if ((chapterType == ChapterType.MainTown)||
                (chapterType == ChapterType.DuelWait))
            {
                HideAll();
                return;
            }

            //确保界面与数据都ok了再初始化
            //Debug.LogError(string.Format("inited:{0}  GetInit:{1}", _inited, battlePetData.GetInit()));
            if (_inited == false && battlePetData.GetInit())
            {
                _inited = true;
                IsShowingList = false;
                _currentPetInfo = null;
                Refresh();
            }
            else
            {
                HideAll();
            }
        }

        public void FieldChange()
        {
            if (_inited == true)
            {
                IsShowingList = false;
                Refresh();
            }
        }

        private void HideAll()
        {
            _btnHead.gameObject.SetActive(false);
            IsShowingList = false;
        }

        private bool IsShowingList
        {
            get
            {
                return _isShowingList;
            }
            set
            {
                _isShowingList = value;
                _goInactiveList.SetActive(value);
            }
        }

        private void RefreshList(List<PetInfo> petInfoList)
        {
            if (petInfoList.Count == 0)
            {
                _textNoPet.Visible = true;
            }
            else
            {
                _textNoPet.Visible = false;
            }
            _list.SetDataList<BattlePetHead>(petInfoList);
        }

        private void Refresh()
        {
            if (_inited)
            {
                RefreshHead();
                RefreshReviveBtn();
            }
        }

        private void RefreshHead()
        {
            PetInfo petInfo = battlePetData.GetCurrentPetInfo();
            _currentPetInfo = petInfo;
            if (petInfo == null)
            {
                _btnHead.gameObject.SetActive(false);
                return;
            }
            _btnHead.gameObject.SetActive(true);
            _itemGrid.Clear();
            _itemGrid.SetLoadCallback(LoadIconCallback);
            _itemGrid.SetItemData(pet_helper.GetIcon(petInfo.Id), pet_quality_helper.GetNameColorId(petInfo.Id, petInfo.Quality));
            _countdown.Refresh(petInfo);
            RefreshLevelAndStar(petInfo);
            RefreshHeadList();
        }

        private void RefreshLevelAndStar(PetInfo petInfo)
        {
            _textLevel.ChangeAllStateText(petInfo.Level.ToString());
            _starList.SetNum(petInfo.Star);
        }

        private void RefreshHeadList()
        {
            if (IsShowingList)
            {
                RefreshList(battlePetData.GetInactivePetInfoList());
            }
        }

        private void LoadIconCallback(GameObject obj)
        {
            if (_currentPetInfo != null && _currentPetInfo.FightState == PetFightState.dead)
            {
                obj.GetComponent<ImageWrapper>().SetGray(0f);
            }
            else
            {
                obj.GetComponent<ImageWrapper>().SetGray(1f);
            }
        }

        private void RefreshReviveBtn()
        {
            _btnRevive.Visible = GameSceneManager.GetInstance().curMapType == public_config.MAP_TYPE_WILD && GetDeadPetCount() > 0;
        }

        public void OnLevelMap(int mapId)
        {
            _inited = false;
            battlePetData.LevelCombatScene();
        }

        public int GetDeadPetCount()
        {
            int count = 0;
            List<PetInfo> inactivePetInfoList = battlePetData.GetInactivePetInfoList();
            for (int i = 0; i < inactivePetInfoList.Count; i++)
            {
                if (inactivePetInfoList[i].FightState == PetFightState.dead)
                {
                    count++;
                }
            }
            return count;
        }
    }
}
