﻿using System;
using System.Collections.Generic;
using System.Text;

using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using UnityEngine.UI;
using GameLoader.Utils.Timer;
using Common.Utils;
using GameData;
using Game.UI;
using Common.ExtendComponent;

namespace ModuleMainUI
{
    public class SkillIconPool 
    {
        public static SkillIconPool pool;
        public static void InitPool(GameObject mainPoolObj)
        {
            pool = new SkillIconPool(mainPoolObj);
        }

        GameObject _poolObj;
        Dictionary<int, Dictionary<int, Queue<SkillIcon>>> _iconIdGameObjectMap = new Dictionary<int, Dictionary<int, Queue<SkillIcon>>>();
        public SkillIconPool(GameObject mainPoolObj)
        {
            _poolObj = new GameObject("SkillIconPool");
            _poolObj.SetActive(false);
            _poolObj.transform.SetParent(mainPoolObj.transform, false);
        }

        Queue<SkillIcon> GetGameObjectQueue(int iconId, int width)
        {
            if (!_iconIdGameObjectMap.ContainsKey(iconId))
            {
                _iconIdGameObjectMap[iconId] = new Dictionary<int, Queue<SkillIcon>>();
            }
            if (!_iconIdGameObjectMap[iconId].ContainsKey(width))
            {
                _iconIdGameObjectMap[iconId][width] = new Queue<SkillIcon>();
            }
            return _iconIdGameObjectMap[iconId][width];
        }

        public SkillIcon CreateIconGameObject(int iconId, GameObject parent)
        {
            SkillIcon icon = null;
            RectTransform rect = parent.GetComponent<RectTransform>();
            int width = (int)rect.sizeDelta.x;
            var queue = GetGameObjectQueue(iconId, width);
            if (queue.Count > 0)
            {
                icon = queue.Dequeue();
                return icon;
            }
            icon = CreateIconGameObject(iconId, width);
            return icon;
        }

        private SkillIcon CreateIconGameObject(int iconId, int width)
        {
            SkillIcon icon = null;
            GameObject iconGameObject = new GameObject();
            icon = iconGameObject.AddComponent<SkillIcon>();
            icon.Reset(iconId, width, width);
            iconGameObject.transform.SetParent(_poolObj.transform, false);
            return icon;
        }

        public void ReleaseActorGameObject(SkillIcon skillIcon)
        {
            var queue = GetGameObjectQueue(skillIcon.iconId, skillIcon.width);
            skillIcon.transform.SetParent(_poolObj.transform, false);
            queue.Enqueue(skillIcon);
        }

        public void Clear()
        {
            var childCount = _poolObj.transform.childCount;
            for (int i = childCount - 1; i >= 0; i--)
            {
                var go = _poolObj.transform.GetChild(i);
                GameObject.Destroy(go.gameObject);
            }
            _iconIdGameObjectMap.Clear();
        }
    }
}
