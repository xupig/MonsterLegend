﻿
using Common.ClientConfig;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using UnityEngine;
using UnityEngine.UI;

namespace ModuleMainUI
{
    public class PlayerEpBar : KContainer
    {
        private StateImage _epImage;
        private StateImage _grayImage;
        private float _percent;
        private float _slowPercent;
        private float _addToPercent;
        private uint _timerId;
        private float _damping = 0.5f;
        private float _ref = 0f;
        private float _width;
        private bool _needEffect;
        private StateText _txtNum;

        protected override void Awake()
        {
            _epImage = GetChildComponent<StateImage>("Image_nuqi");
            _grayImage = GetChildComponent<StateImage>("Image_nuqidi");
            _txtNum = GetChildComponent<StateText>("Label_txtNnengliang");
            _width = _epImage.GetComponent<RectTransform>().sizeDelta.x;
            InitImage();
        }

        private void InitImage()
        {
            _epImage.CurrentImage.type = Image.Type.Filled;
            _epImage.CurrentImage.fillMethod = Image.FillMethod.Horizontal;
            _epImage.CurrentImage.fillOrigin = (int)Image.OriginHorizontal.Left;
            _grayImage.CurrentImage.type = Image.Type.Filled;
            _grayImage.CurrentImage.fillMethod = Image.FillMethod.Horizontal;
            _grayImage.CurrentImage.fillOrigin = (int)Image.OriginHorizontal.Left;
        }

        private void RefreshEp()
        {
            PlayerAvatar avatar = PlayerAvatar.Player;
            _percent = Mathf.Clamp01((float)avatar.cur_ep / avatar.max_ep);
            _txtNum.CurrentText.text = string.Concat(avatar.cur_ep_show_string, "/", avatar.max_ep_show_string);
            if (_timerId == 0)
            {
                _timerId = TimerHeap.AddTimer(0, 20, OnEpTimerStep);
            }
        }

        private void AddEp(int oldEp, int curEp)
        {
            PlayerAvatar avatar = PlayerAvatar.Player;
            _addToPercent = Mathf.Clamp01((float)curEp / avatar.max_ep);
            int value = Mathf.RoundToInt((curEp - oldEp) / 100.0f);
            if (value > 0)
            {
                EventDispatcher.TriggerEvent<int, int>(BattleUIEvents.SHOW_FLOAT_TIP, 1, value);
            }
            _needEffect = true;
            if (_timerId == 0)
            {
                _timerId = TimerHeap.AddTimer(0, 20, OnEpTimerStep);
            }
        }

        private void OnEpTimerStep()
        {
            _slowPercent = Mathf.SmoothDamp(_slowPercent, _percent, ref _ref, _damping);
            if (Mathf.Abs(_slowPercent - _percent) < 0.001f)
            {
                _needEffect = false;
                ResetTimer();
                _slowPercent = _percent;
            }
            if (_needEffect == false)
            {
                RefreshEpImage(_slowPercent, _slowPercent);
            }
            else
            {
                if (_addToPercent <= _slowPercent)
                {
                    _needEffect = false;
                }
                RefreshEpImage(_slowPercent, Mathf.Min(_addToPercent, _percent));
            }
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        protected void AddEventListener()
        {
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.cur_ep, RefreshEp);
            EventDispatcher.AddEventListener<int, int>(BattleUIEvents.ADD_EP, AddEp);
        }

        protected void RemoveEventListener()
        {
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.cur_ep, RefreshEp);
            EventDispatcher.RemoveEventListener<int, int>(BattleUIEvents.ADD_EP, AddEp);
        }

        private void RefreshEpImage(float value1, float value2)
        {
            _epImage.CurrentImage.fillAmount = value1;
            _grayImage.CurrentImage.fillAmount = value2;
        }

        protected override void OnEnable()
        {
            AddEventListener();
            _ref = 0;
            PlayerAvatar avatar = PlayerAvatar.Player;
            _percent = _slowPercent = Mathf.Clamp01((float)avatar.cur_ep / avatar.max_ep);
            _txtNum.CurrentText.text = string.Concat(avatar.cur_ep_show_string, "/", avatar.max_ep_show_string);
            _needEffect = false;
            RefreshEpImage(_slowPercent, _percent);
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            ResetTimer();
        }

      
    }
}
