﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using MogoEngine.Mgrs;
using GameMain.Entities;
using Common.ClientConfig;
using GameLoader.Utils.Timer;
using Common.Global;
using GameMain.GlobalManager;
using Common.Utils;
using Common.States;

namespace ModuleMainUI
{
    public class BattleFloatTipList : KContainer
    {
        private BattleFloatTip _epFloatTip;
        private BattleFloatTip _integralFloatTip;

        private const int EP_FLOAT_TIP = 1;
        private const int INTEGRAL_FLOAT_TIP = 2;

        protected override void Awake()
        {
            _epFloatTip = AddChildComponent<BattleFloatTip>("Label_txtNengliang");
            _integralFloatTip = AddChildComponent<BattleFloatTip>("Label_txtJifen");
        }

        public void Show(int type, int value)
        {
            switch (type)
            {
                case EP_FLOAT_TIP:
                    _epFloatTip.ShowFloatTip(value);
                    break;
                case INTEGRAL_FLOAT_TIP:
                    _integralFloatTip.ShowFloatTip(value);
                    break;
            }
        }

    }
}
