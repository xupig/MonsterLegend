﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using MogoEngine.Mgrs;
using GameMain.Entities;
using Common.ClientConfig;
using GameLoader.Utils.Timer;
using Common.Global;
using GameMain.GlobalManager;
using Common.Utils;
using Common.States;

namespace ModuleMainUI
{
    public class WayDirectionView : KContainer
    {
        private WayDirectionItem _item;
        private float elapsed = 0f;
        private const float CHECK_INTERVAL = 1.0f;

        protected override void Awake()
        {
            _item = AddChildComponent<WayDirectionItem>("Container_img");
        }

        private void Show()
        {
            if (_item.Visible == false)
            {
                _item.Visible = true;
            }
        }

        private void Hide()
        {
            if (_item.Visible == true)
            {
                _item.Visible = false;
            }
        }

        void Update()
        {
            elapsed += Time.deltaTime;
            if (elapsed > CHECK_INTERVAL)
            {
                elapsed -= CHECK_INTERVAL;
                Check();
            }
        }

        private void Check()
        {
            if (Camera.main == null)
            {
                Hide();
                return;
            }
            if (UIManager.GetLayerVisibility(MogoUILayer.LayerUIMain) == false)
            {
                Hide();
                return;
            }
            Vector3 autoMovePos = GameSceneManager.GetInstance().GetAutoMovePoint();
            float dist = GetDistToTarget(autoMovePos);
            if (dist < 2 || autoMovePos == Vector3.zero)
            {
                Hide();
                return;
            }
            if (LifeBarManager.Instance.IsShowPointingWayDirection() == false)
            {
                Hide();
                return;
            }
            Show();
        }

        private float GetDistToTarget(Vector3 autoMovePos)
        {
            Vector3 playerPosition = PlayerAvatar.Player.position;
            Vector3 targetPosition = autoMovePos;
            playerPosition.y = 0;
            targetPosition.y = 0;
            float dist = (playerPosition - targetPosition).magnitude;
            return dist;
        }

    }
}
