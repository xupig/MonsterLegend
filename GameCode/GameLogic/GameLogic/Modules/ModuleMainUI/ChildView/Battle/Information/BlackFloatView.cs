﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using MogoEngine.Mgrs;
using GameMain.Entities;
using Common.ClientConfig;
using GameLoader.Utils.Timer;
using Common.Data;
using Common.Utils;
using Common.ExtendComponent;

namespace ModuleMainUI
{
    public class BlackFloatView : SystemNoticeDisplayer
    {
        private StateText _txt;

        protected override void Awake()
        {
            base.Awake();
            _txt = GetChildComponent<StateText>("Label_txtTip");
            this.Visible = false;
        }

        protected override void Refresh(SystemNoticeDisplayData notice)
        {
            _txt.CurrentText.text = MogoLanguageUtil.GetContent(notice.content, notice.objArr);
        }

        protected override void ShowView()
        {
            TweenViewMove.Begin(gameObject, MoveType.Show, MoveDirection.Down, 0.1f);
        }

        protected override void HideView()
        {
            Visible = false;
        }

    }
}
