﻿using Common.Events;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using UnityEngine;

namespace ModuleMainUI
{
    public class ClockView : KContainer
    {
        private StateText _timeText;
        private float elapsed = 0f;
        private const float CHECK_INTERVAL = 0.5f;
        private bool _isEnd = false;

        protected override void Awake()
        {
            _timeText = GetChildComponent<StateText>("Label_txtShijian");
        }

        void Update()
        {
            if (_isEnd) { return; }
            elapsed += Time.deltaTime;
            if (elapsed > CHECK_INTERVAL)
            {
                elapsed -= CHECK_INTERVAL;
                RefreshContent();
            }
        }

        private void RefreshContent()
        {
            uint endTime = SpaceLeftTimeManager.Instance.endTime;
            uint currentTime = Global.serverTimeStampSecond;
            if (GameSceneManager.GetInstance().IsInClientMap())
            {
                currentTime = (uint)Time.realtimeSinceStartup;
                if (currentTime >= endTime)
                {
                    var _scene = GameSceneManager.GetInstance().scene;
                    if (_scene is CombatScene)
                    {
                        (_scene as CombatScene).OnCombatComplete();
                    }
                }
            }
            int leftSecond = Mathf.RoundToInt(endTime - currentTime);
            leftSecond = Math.Max(0, leftSecond);
            _timeText.CurrentText.text = MogoTimeUtil.ToUniversalTime(leftSecond, "mm:ss");
            if (currentTime >= endTime)
            {
                _isEnd = true;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            StartCountDown();
            EventDispatcher.AddEventListener<int>(SceneEvents.ENTER_SCENE, OnEnterMap);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            EventDispatcher.RemoveEventListener<int>(SceneEvents.ENTER_SCENE, OnEnterMap);
        }

        private void OnEnterMap(int mapId)
        {
            StartCountDown();
        }

        private void StartCountDown()
        {
            _isEnd = false;
        }

    }
}