﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using MogoEngine.Mgrs;
using GameMain.Entities;
using Common.ClientConfig;
using GameLoader.Utils.Timer;
using Common.Data;
using GameMain.GlobalManager;
using Common;
using Common.Utils;
using Common.ServerConfig;
using GameData;
using GameMain.GlobalManager.SubSystem;

namespace ModuleMainUI
{
    public class InformationView : KContainer
    {
        private ProgressiveView _progressView;
        private BlackFloatView _floatView;
        private RedWarningView _warningView;
        private RightUpNoticeView _rightUpView;
        private ClockView _clockView;

        protected override void Awake()
        {
            _clockView = AddChildComponent<ClockView>("Container_bottomCenter/Container_shijian");
            _progressView = AddChildComponent<ProgressiveView>("Container_middleCenter/Container_caiji");
            _progressView.Visible = false;
            _floatView = AddChildComponent<BlackFloatView>("Container_middleCenter/Container_boss");
            _floatView.Visible = false;
            _warningView = AddChildComponent<RedWarningView>("Container_middleCenter/Container_jineng");
            _warningView.Visible = false;
            _rightUpView = AddChildComponent<RightUpNoticeView>("Container_topRight/Container_jindu");
            if (_rightUpView != null)
            {
                _rightUpView.Visible = false;
            }
        }

        protected override void OnEnable()
        {
            AddEventListener();
            SpaceNoticeActionManager.Instance.ShowSystemNotice();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<SystemNoticeData>(BattleUIEvents.SHOW_INFOMATION, OnShowSystemNotice);
            EventDispatcher.AddEventListener<int, int>(BattleUIEvents.HIDE_SYSTEM_NOTICE_DISPLAY, RemoveSystemNoticeDisplay);
            EventDispatcher.AddEventListener(BattleUIEvents.HIDE_PROGRESSBAR_INFOMATION, OnHideCollectingView);
            EventDispatcher.AddEventListener<int>(BattleUIEvents.HIDE_RIGHT_UP_INFOMATION, RemoveProgressiveItem);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<SystemNoticeData>(BattleUIEvents.SHOW_INFOMATION, OnShowSystemNotice);
            EventDispatcher.RemoveEventListener<int, int>(BattleUIEvents.HIDE_SYSTEM_NOTICE_DISPLAY, RemoveSystemNoticeDisplay);
            EventDispatcher.RemoveEventListener(BattleUIEvents.HIDE_PROGRESSBAR_INFOMATION, OnHideCollectingView);
            EventDispatcher.RemoveEventListener<int>(BattleUIEvents.HIDE_RIGHT_UP_INFOMATION, RemoveProgressiveItem);
        }

        private void OnShowSystemNotice(SystemNoticeData notice)
        {
            if (notice.type == InstanceDefine.COUNTER_NOTICE_ACTION || notice.type == InstanceDefine.COUNTER_NOTICE_ACTION_INIT) { return; }
            SpaceNoticeActionManager.Instance.RemoveSystemNotice(notice);
            switch (notice.type)
            {
                case InstanceDefine.SYSTEM_NOTICE_ACTION_UP_TYPE:
                    _floatView.Show(notice as SystemNoticeDisplayData);
                    break;
                case InstanceDefine.SYSTEM_NOTICE_ACTION_MIDDLE_TYPE:
                    _warningView.Show(notice as SystemNoticeDisplayData);
                    break;
                case InstanceDefine.SYSTEM_NOTICE_ACTION_PROGRESSBAR_TYPE:
                    _progressView.Show(notice as SystemNoticeProgressData);
                    break;
                case InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_TYPE:
                    if (_rightUpView != null)
                    {
                        _rightUpView.AddSystemNotice(notice as SystemNoticeRightUpData);
                    }
                    break;
            }
        }

        private void OnHideCollectingView()
        {
            _progressView.Hide();
        }

        private void RemoveSystemNoticeDisplay(int type, int content)
        {
            switch (type)
            {
                case InstanceDefine.SYSTEM_NOTICE_ACTION_UP_TYPE:
                    _floatView.RemoveNotice(content);
                    break;
                case InstanceDefine.SYSTEM_NOTICE_ACTION_MIDDLE_TYPE:
                    _warningView.RemoveNotice(content);
                    break;
            }
        }

        private void RemoveProgressiveItem(int targetId)
        {
            if (_rightUpView != null)
            {
                _rightUpView.RemoveItemByNoticeId(targetId);
            }
        }

        public void Show()
        {
            int mapId = GameSceneManager.GetInstance().curMapID;
            int instId = map_helper.GetInstanceIDByMapID(mapId);
            if (_clockView != null)
            {
                _clockView.Visible = instance_helper.GetTimeLeft(instId) > 0;
            }
        }

        public void Hide()
        {
            _progressView.Visible = false;
            _floatView.Visible = false;
            _warningView.Visible = false;
        }

    }
}
