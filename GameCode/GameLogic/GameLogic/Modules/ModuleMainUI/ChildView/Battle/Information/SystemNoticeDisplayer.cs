﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using MogoEngine.Mgrs;
using GameMain.Entities;
using Common.ClientConfig;
using GameLoader.Utils.Timer;
using Common.Data;
using Common.ExtendComponent;

namespace ModuleMainUI
{
    public class SystemNoticeDisplayer : KContainer
    {
        protected List<SystemNoticeDisplayData> _noticeList = new List<SystemNoticeDisplayData>();
        protected uint _timerId;
        protected int _currentContent;

        public void Show(SystemNoticeDisplayData notice)
        {
            ShowView();
            AddSystemNotice(notice);
            Invoke("TryShowNotice", 0.01f);
        }

        public void RemoveNotice(int content)
        {
            for (int i = _noticeList.Count - 1; i >= 0; i--)
            {
                if (_noticeList[i].content == content)
                {
                    _noticeList.RemoveAt(i);
                }
            }
            if (_currentContent == content)
            {
                OnTimerEnd();
            }
        }

        protected virtual void ShowView()
        {
            Visible = true;
        }

        protected virtual void HideView()
        {
            Visible = false;
        }

        protected virtual void AddSystemNotice(SystemNoticeDisplayData notice)
        {
            _noticeList.Add(notice);
            _noticeList.Sort(SortByPriority);
        }

        private int SortByPriority(SystemNoticeDisplayData x, SystemNoticeDisplayData y)
        {
            return x.priority - y.priority;
        }

        protected virtual bool TryShowNotice()
        {
            if(_noticeList.Count > 0 && _timerId == 0)
            {
                SystemNoticeDisplayData notice = _noticeList[0];
                _noticeList.RemoveAt(0);
                Refresh(notice);
                _currentContent = notice.content;
                _timerId = TimerHeap.AddTimer(notice.time, 0, OnTimerEnd);
                return true;
            }
            return false;
        }

        protected void OnTimerEnd()
        {
            TimerHeap.DelTimer(_timerId);
            _timerId = 0;
            _currentContent = 0;
            if (TryShowNotice() == false)
            {
                HideView();
            }
        }

        protected virtual void Refresh(SystemNoticeDisplayData notice)
        {

        }
    }
}
