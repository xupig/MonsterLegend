﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using MogoEngine.Mgrs;
using GameMain.Entities;
using Common.ClientConfig;
using GameLoader.Utils.Timer;
using Common.Data;
using Game.UI;
using Common.Utils;
using Common.ExtendComponent;

namespace ModuleMainUI
{
    public class RightUpNoticeView : KContainer
    {
        private KList _list;
        private KDummyButton _closeButton;
        private KDummyButton _openButton;

        protected override void Awake()
        {
            _list = GetChildComponent<KList>("Container_open/ScrollView_xianshineirong/mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _list.SetPadding(5, 0, 0, 20);
            _closeButton = AddChildComponent<KDummyButton>("Image_zhandoutishiSH");
            _openButton = AddChildComponent<KDummyButton>("Container_open");
            _closeButton.onClick.AddListener(OpenView);
            _openButton.onClick.AddListener(CloseView);
            CloseView();
        }

        private void CloseView()
        {
            TweenViewMove.Begin(_openButton.gameObject, MoveType.Hide, MoveDirection.Right).Tweener.onFinished = OnCloseTweenEnd;
        }

        private void OpenView()
        {
            TweenViewMove.Begin(_openButton.gameObject, MoveType.Show, MoveDirection.Right);
            _closeButton.gameObject.SetActive(false);
        }

        private void OnCloseTweenEnd(UITweener tween)
        {
            _closeButton.gameObject.SetActive(true);
            if (_list.ItemList.Count == 0)
            {
                this.Visible = false;
            }
        }

        public void AddSystemNotice(SystemNoticeRightUpData notice)
        {
            if (this.Visible == false)
            {
                this.Visible = true;
                OpenView();
            }
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                SystemNoticeRightUpData itemData = _list.ItemList[i].Data as SystemNoticeRightUpData;
                if (itemData.rightUpNoticeId == notice.rightUpNoticeId)
                {
                    if (itemData.rightUpType == InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_PROGRESSBAR_TYPE && itemData.rightUpTargetId == notice.rightUpTargetId)
                    {
                        _list.ItemList[i].Data = notice;
                        return;
                    }
                    if (itemData.rightUpType != InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_PROGRESSBAR_TYPE)
                    {
                        _list.ItemList[i].Data = notice;
                        return;
                    }
                }
            }
            int index = GetItemIndexByPriority(notice);
            _list.AddItemAt<RightUpNoticeItem>(notice, index);
            RightUpNoticeItem item = _list.ItemList[index] as RightUpNoticeItem;
            item.onRemove.AddListener(OnRemoveItem);
        }


        private int GetItemIndexByPriority(SystemNoticeRightUpData notice)
        {
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                SystemNoticeRightUpData data = _list.ItemList[i].Data as SystemNoticeRightUpData;
                if (data.priority > notice.priority)
                {
                    return i;
                }
            }
            return _list.ItemList.Count;
        }

        public void RemoveItemByNoticeId(int noticeId)
        {
            for (int i = _list.ItemList.Count - 1; i >= 0; i--)
            {
                SystemNoticeRightUpData itemData = _list.ItemList[i].Data as SystemNoticeRightUpData;
                if (itemData.rightUpNoticeId == noticeId)
                {
                    _list.RemoveItemAt(i);
                }
            }
            _list.DoLayout();
            if (_list.ItemList.Count == 0)
            {
                CloseView();
            }
        }

        private void OnRemoveItem(RightUpNoticeItem target)
        {
            _list.RemoveItemAt(target.Index);
            _list.DoLayout();
            if (_list.ItemList.Count == 0)
            {
                CloseView();
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            CloseView();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            _list.RemoveAll();
        }

    }

    public class RightUpNoticeItem : KList.KListItemBase
    {
        private const int INTERVAL = 100;

        private KProgressBar _bar;
        private StateText _contentTxt;
        private StateText _numTxt;

        private SystemNoticeRightUpData _data;
        private uint _barTimerId;
        private uint _removeTimerId;
        private uint _leftTimeInMillisceond;

        public KComponentEvent<RightUpNoticeItem> onRemove = new KComponentEvent<RightUpNoticeItem>();

        protected override void Awake()
        {
            _bar = GetChildComponent<KProgressBar>("ProgressBar_jindu");
            _contentTxt = GetChildComponent<StateText>("Label_txtMiaoshu");
            _contentTxt.CurrentText.lineSpacing = 0.75f;
            _numTxt = GetChildComponent<StateText>("Label_txtShuliang");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as SystemNoticeRightUpData;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            if (_data.time > 0)
            {
                ResetRemoveTimer();
                _removeTimerId = TimerHeap.AddTimer(_data.time, 0, OnTimeEnd);
            }
            switch (_data.rightUpType)
            {
                case InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_CONTENT_TYPE:
                    _contentTxt.CurrentText.text = MogoLanguageUtil.GetContent(_data.content, _data.objArr);
                    _numTxt.Visible = false;
                    _bar.Visible = false;
                    break;
                case InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_PROGRESSBAR_TYPE:
                    _contentTxt.CurrentText.text = MogoLanguageUtil.GetContent(_data.content, _data.objArr);
                    _numTxt.CurrentText.text = string.Format("{0}/{1}", _data.killMonsterNum, _data.monsterTotalNum);
                    _bar.Value = (float)((float)_data.killMonsterNum / (float)_data.monsterTotalNum);
                    if (_data.killMonsterNum == _data.monsterTotalNum)
                    {
                        onRemove.Invoke(this);
                    }
                    break;
                case InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_TIME_TYPE:
                    _contentTxt.CurrentText.text = MogoLanguageUtil.GetContent(_data.content, _data.objArr);
                    _leftTimeInMillisceond = _data.time;
                    _bar.Value = 1.0f;
                    ResetTimer();
                    _barTimerId = TimerHeap.AddTimer(0, INTERVAL, OnTimerStep);
                    break;
            }
        }

        private void OnTimeEnd()
        {
            onRemove.Invoke(this);
        }

        private void OnTimerStep()
        {
            if (_data == null)
            {
                ResetTimer();
                return;
            }
            _bar.Value -= (float)((float)INTERVAL / (float)_data.time);
            _leftTimeInMillisceond -= INTERVAL;
            _numTxt.CurrentText.text = Mathf.CeilToInt(_leftTimeInMillisceond / 1000f).ToString();
            if (_bar.Value <= 0)
            {
                onRemove.Invoke(this);
            }
        }

        private void ResetTimer()
        {
            if (_barTimerId != 0)
            {
                TimerHeap.DelTimer(_barTimerId);
                _barTimerId = 0;
            }
        }

        private void ResetRemoveTimer()
        {
            if (_removeTimerId != 0)
            {
                TimerHeap.DelTimer(_removeTimerId);
                _removeTimerId = 0;
            }
        }

        public override void Dispose()
        {
            ResetTimer();
            ResetRemoveTimer();
            onRemove.RemoveAllListeners();
            _data = null;
        }
    }

}

