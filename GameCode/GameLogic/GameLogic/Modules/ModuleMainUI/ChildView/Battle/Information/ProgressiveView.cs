﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using MogoEngine.Mgrs;
using GameMain.Entities;
using Common.ClientConfig;
using GameLoader.Utils.Timer;
using Common.Data;
using Common.Utils;

namespace ModuleMainUI
{
    public class ProgressiveView : KContainer
    {
        private const int INTERVAL = 20;

        private KProgressBar _bar;
        private StateText _label;

        private uint _timerId;
        private SystemNoticeProgressData _data;

        protected override void Awake()
        {
            base.Awake();
            _bar = GetChildComponent<KProgressBar>("ProgressBar_jindu");
            _label = GetChildComponent<StateText>("Label_txtCaijizhong");
            this.Visible = false;
        }

        public void Show(SystemNoticeProgressData notice)
        {
            this.Visible = true;
            _data = notice;
            _label.CurrentText.text = MogoLanguageUtil.GetContent(_data.content);
            if (_data.progressbarPlayType == 0)
            {
                Hide();
            }
            else if (_data.progressbarPlayType > 0)
            {
                _bar.Value = 0f;
            }
            else
            {
                _bar.Value = 1.0f;
            }
            ResetTimer();
            _timerId = TimerHeap.AddTimer(0, INTERVAL, OnTimerStep);
        }

        private void OnTimerStep()
        {
            if (_data.progressbarPlayType > 0)
            {
                _bar.Value += (float)((float)INTERVAL / (float)_data.time);
            }
            else
            {
                _bar.Value -= (float)((float)INTERVAL / (float)_data.time);
            }

            if (_bar.Value >= 1 || _bar.Value <= 0)
            {
                Hide();
            }
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        public void Hide()
        {
            this.Visible = false;
            _data = null;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            ResetTimer();
        }

    }
}
