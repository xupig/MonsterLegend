﻿using Common.Data;
using Common.Events;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleMainUI
{
    public class StarView : KContainer
    {
        List<StateText> _textList;
        List<StateImage> _imageList;

        protected override void Awake()
        {
            _textList = new List<StateText>();
            _imageList = new List<StateImage>();
            for (int i = 1; i <= 3; i++)
            {
                _textList.Add(GetChildComponent<StateText>(string.Format("Container_xingxing0{0}/Label_txtxingjitiaozhan", i)));
                _imageList.Add(GetChildComponent<StateImage>(string.Format("Container_xingxing0{0}/Container_diyixingxing/Image_xingxingBigIcon", i)));
            }
        }

        protected override void OnEnable()
        {
            RefreshStarView();
        }


        public void Show(SystemNoticeCopyStarData notice)
        {
            PlayerDataManager.Instance.PlayingCopyStarData.UpdateCopyStar(notice.actionId, notice.state);
            if (this.Visible == true)
            {
                RefreshStarView();
            }
        }

        private void RefreshStarView()
        {
            Dictionary<int, int> actionIdToStateDic = PlayerDataManager.Instance.PlayingCopyStarData.ActionIdToStateDic;

            int index = 0;
            int mapId = GameSceneManager.GetInstance().curMapID;
            Dictionary<string, string[]> dicScoreRule = map_helper.GetScoreRule(mapId);
            foreach (KeyValuePair<int, int> pair in actionIdToStateDic)
            {
                string desc = map_helper.GetDesc(mapId, pair.Key);
                _imageList[index].Visible = pair.Value == 1;
                _textList[index++].CurrentText.text = string.Format(desc, ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_CHAT_RUMOR, map_helper.ParseScoreRuleParam(dicScoreRule[pair.Key.ToString()][0])));
                
            }
        }

    }
}
