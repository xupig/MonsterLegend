﻿using Common;
using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleMainUI
{
    public class AutoBattleContainer : KContainer
    {
        private KButton _battleButton;
        private KParticle _effectGo;

        protected override void Awake()
        {
            _battleButton = gameObject.GetComponent<KButton>();
            _effectGo = AddChildComponent<KParticle>("fx_ui_4_3_zidongzhandou");
            _effectGo.Stop();
        }

        protected void AddEventListener()
        {
            _battleButton.onClick.AddListener(OnBattleButtonClick);
            EventDispatcher.AddEventListener<FightType>(PlayerEvents.ON_FIGHT_TYPE_CHANGED, OnChangeAutoFight);
        }

        protected void RemoveEventListener()
        {
            _battleButton.onClick.RemoveListener(OnBattleButtonClick);
            EventDispatcher.RemoveEventListener<FightType>(PlayerEvents.ON_FIGHT_TYPE_CHANGED, OnChangeAutoFight);
        }


        private void OnChangeAutoFight(FightType fightType)
        {
            RefreshAutoView();
        }

        private bool _isAutoFight = false;
        private void OnBattleButtonClick()
        {
            if (_isAutoFight == false && PlayerAutoFightManager.GetInstance().fightType == FightType.FOLLOW_FIGHT)
            {
                _effectGo.Stop();
                MessageBox.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.ENSURE_CANCEL_FOLLOW_FIGHT), () =>
                {
                    _isAutoFight = true;
                    PlayerAutoFightManager.GetInstance().Start();
                    _effectGo.Play(true);
                    TeamManager.Instance.CancelFollowCaptain();
                });
                return;
            }
            if (_isAutoFight == false)
            {
                _isAutoFight = true;
                _effectGo.Play(true);
                PlayerAutoFightManager.GetInstance().Start();
            }
            else
            {
                _isAutoFight = false;
                _effectGo.Stop();
                PlayerAutoFightManager.GetInstance().Stop();
            }
        }

        private void RefreshAutoView()
        {
            FightType fightType = PlayerAutoFightManager.GetInstance().fightType;
            if (fightType == FightType.AUTO_FIGHT)
            {
                _isAutoFight = true;
                _effectGo.Play(true);
            }
            else
            {
                _isAutoFight = false;
                _effectGo.Stop();
            }
        }

        protected override void OnEnable()
        {
            AddEventListener();
            RefreshAutoView();
        }
        protected override void OnDisable()
        {
            RemoveEventListener();
        }

    }
}
