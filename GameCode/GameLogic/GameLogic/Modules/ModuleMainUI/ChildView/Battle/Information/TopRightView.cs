﻿using Common;
using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.States;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.CombatSystem;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    public class TopRightView : KContainer
    {
        private KButton _starButton;
        private KButton _joyStickBtn;
        private KButton _clickControlBtn;
        private StarView _starView;
        private AutoBattleContainer _battleContainer;
        private int _openAutoFightTakId;

        protected override void Awake()
        {
            _starButton = GetChildComponent<KButton>("Button_zhandouxingji");
            _starView = AddChildComponent<StarView>("Container_xingjixinxi");
            _joyStickBtn = GetChildComponent<KButton>("Button_yaogan");
            _clickControlBtn = GetChildComponent<KButton>("Button_dianchu");
            _battleContainer = AddChildComponent<AutoBattleContainer>("Button_zhandou");
            _starView.Visible = false;
            if (platform_helper.InPCPlatform())
            {
                _openAutoFightTakId = int.Parse(global_params_helper.GetGlobalParam(247).Split(',')[0]);
            }
            else
            {
                _openAutoFightTakId = int.Parse(global_params_helper.GetGlobalParam(127).Split(',')[0]);
            }
        }

        protected void AddEventListener()
        {
            _starButton.onClick.AddListener(OnStarToggleChange);
            _joyStickBtn.onClick.AddListener(OnJoyStickBtnClick);
            _clickControlBtn.onClick.AddListener(OnClickControlBtnClick);
            EventDispatcher.AddEventListener<SystemNoticeData>(BattleUIEvents.SHOW_INFOMATION, OnShowSystemNotice);
            EventDispatcher.AddEventListener<ControlVehicle>(VehicleEvents.DRIVER_VEHICLE, OnDriverVehicle);
            EventDispatcher.AddEventListener<ControlVehicle>(VehicleEvents.DIVORCE_VEHICLE, OnDivorceVehicle);
            EventDispatcher.AddEventListener<bool>(PlayerEvents.ON_IN_TOUCH_MODE_STATE_CHANGED, OnBattleModeChange);
            EventDispatcher.AddEventListener<int>(TaskEvent.TASK_FINISH, OnTaskComplete);
        }

        protected void RemoveEventListener()
        {
            _starButton.onClick.RemoveListener(OnStarToggleChange);
            _joyStickBtn.onClick.RemoveListener(OnJoyStickBtnClick);
            _clickControlBtn.onClick.RemoveListener(OnClickControlBtnClick);
            EventDispatcher.RemoveEventListener<SystemNoticeData>(BattleUIEvents.SHOW_INFOMATION, OnShowSystemNotice);
            EventDispatcher.RemoveEventListener<ControlVehicle>(VehicleEvents.DRIVER_VEHICLE, OnDriverVehicle);
            EventDispatcher.RemoveEventListener<ControlVehicle>(VehicleEvents.DIVORCE_VEHICLE, OnDivorceVehicle);
            EventDispatcher.RemoveEventListener<bool>(PlayerEvents.ON_IN_TOUCH_MODE_STATE_CHANGED, OnBattleModeChange);
            EventDispatcher.RemoveEventListener<int>(TaskEvent.TASK_FINISH, OnTaskComplete);
        }

        private void OnBattleModeChange(bool obj)
        {
            RefreshControlBtnView();
        }

        private void OnClickControlBtnClick()
        {
            PlayerTouchBattleModeManager.GetInstance().Enter();
        }

        private void OnJoyStickBtnClick()
        {
            PlayerTouchBattleModeManager.GetInstance().Exit();
        }

        private void RefreshControlBtnView()
        {
            if (PlayerTouchBattleModeManager.GetInstance().inTouchModeState == false)
            {
                _joyStickBtn.Visible = false;
                _clickControlBtn.Visible = true;
            }
            else
            {
                _joyStickBtn.Visible = true;
                _clickControlBtn.Visible = false;
            }
            if (PlayerAvatar.Player.vehicleManager.isDrivering)
            {
                _joyStickBtn.Visible = false;
                _clickControlBtn.Visible = false;
            }
            if (platform_helper.InPCPlatform())
            {
                _joyStickBtn.Visible = false;
                _clickControlBtn.Visible = false;
            }
        }
            
        private void OnStarToggleChange()
        {
            _starView.Visible = !_starView.Visible;
        }
        
        private void RefreshButtonView()
        {
            RefreshBattleBtuttonVisibility();
            RefreshControlBtnView();
        }

        private void OnDriverVehicle(ControlVehicle obj)
        {
            RefreshBattleBtuttonVisibility();
            RefreshControlBtnView();
        }

        private void OnDivorceVehicle(ControlVehicle obj)
        {
            RefreshBattleBtuttonVisibility();
            RefreshControlBtnView();
        }

        private void OnTaskComplete(int taskId)
        {
            RefreshBattleBtuttonVisibility();
        }

        private void RefreshBattleBtuttonVisibility()
        {
            int mapId = GameSceneManager.GetInstance().curMapID;
            _battleContainer.Visible = map_helper.GetPlayerAI(mapId) != 0;
            if (PlayerAvatar.Player.vehicleManager.isDrivering)
            {
                _battleContainer.Visible = false;
            }
            bool isOpenAutoFight = PlayerDataManager.Instance.TaskData.IsCompleted(_openAutoFightTakId);
            if (isOpenAutoFight == false)
            {
                _battleContainer.Visible = false;
            }
        }

        private void RefreshContent()
        {
            _starView.Visible = false;
            int mapId = GameSceneManager.GetInstance().curMapID;
            int instId = map_helper.GetInstanceIDByMapID(mapId);
            _starButton.Visible = map_helper.GetStandardWay(mapId) == MapStandardWay.Star;
            switch (GameSceneManager.GetInstance().curMapType)
            {
                case public_config.MAP_TYPE_WILD:
                    _starButton.Visible = false;
                    break;
                case public_config.MAP_TYPE_NIGHT_ACTIVITY:
                    _starButton.Visible = false;
                    break;
            }
            if (_starButton.Visible == true)
            {
                RefreshStarView();
            }
        }

        protected override void OnEnable()
        {
            AddEventListener();
            RefreshContent();
            RefreshButtonView();
        }
        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void OnShowSystemNotice(SystemNoticeData notice)
        {
            if (notice is SystemNoticeCopyStarData)
            {
                SystemNoticeCopyStarData copyStarNotice = notice as SystemNoticeCopyStarData;
                _starView.Show(copyStarNotice);
                RefreshStarView();
            }

        }

        private void RefreshStarView()
        {
            int starCount = PlayerDataManager.Instance.PlayingCopyStarData.GetCopyStarCount();
            for (int i = 0; i < starCount; i++)
            {
                SetStarGrayValue(_starButton.transform.GetChild(i + 1).GetComponent<StateImage>(), 1.0f);        
            }
            for (int i = starCount + 1; i < _starButton.transform.childCount; i++)
            {
                SetStarGrayValue(_starButton.transform.GetChild(i).GetComponent<StateImage>(), 0.0f);
            }
        }

        private void SetStarGrayValue(StateImage image, float grayValue)
        {
            ImageWrapper[] wrapper = image.GetComponentsInChildren<ImageWrapper>(true);
            for (int i = 0; i < wrapper.Length; i++)
            {
                wrapper[i].SetGray(grayValue);
            }
        }
    }
}
