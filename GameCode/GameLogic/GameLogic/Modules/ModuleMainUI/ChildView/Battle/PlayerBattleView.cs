﻿
using Game.UI.UIComponent;
using GameMain.Entities;
using Common.Utils;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using ModuleCommonUI;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager;
using Common.ServerConfig;
using Common.ExtendComponent;
using GameMain.CombatSystem;
using UnityEngine;
using GameData;
using GameMain.GlobalManager.SubSystem;

namespace ModuleMainUI
{
    public class PlayerBattleView : KContainer
    {
        private IconContainer _containerIcon;
        private KDummyButton _headButton;
        private StateText _hpText;
        private StateText _playerName;
        private StateText _levelText;
        private KProgressBar _hpProgressBar;
        private PlayerEpBar _epProgressBar;
        private KButton _separateButton;
        private StateImage _captainImg;
        private IconContainer _teamDutyIcon;

        protected override void Awake()
        {
            _containerIcon = AddChildComponent<IconContainer>("Container_PlayerInfo/Container_HeadInfo/Container_icon");
            _headButton = AddChildComponent<KDummyButton>("Container_PlayerInfo/Container_HeadInfo");
            _captainImg = GetChildComponent<StateImage>("Container_PlayerInfo/Container_HeadInfo/Image_duizhang");
            _hpText = GetChildComponent<StateText>("Container_PlayerInfo/Container_HeadInfo/Container_shengming/Label_txtTili");
            _playerName = GetChildComponent<StateText>("Container_PlayerInfo/Container_HeadInfo/Label_txtWanjiamingzi");
            _levelText = GetChildComponent<StateText>("Container_PlayerInfo/Container_HeadInfo/Container_dengji/Label_txtDengji");
            _hpProgressBar = GetChildComponent<KProgressBar>("Container_PlayerInfo/Container_HeadInfo/ProgressBar_xueliang");
            _epProgressBar = AddChildComponent<PlayerEpBar>("Container_PlayerInfo/Container_HeadInfo/Container_nuqi");
            _separateButton = GetChildComponent<KButton>("Container_PlayerInfo/Button_tuolizaijv");
            _teamDutyIcon = AddChildComponent<IconContainer>("Container_PlayerInfo/Container_HeadInfo/Container_teamDuty");
            if (_separateButton != null)
            {
                _separateButton.Visible = false;
            }
        }

        private void RefreshHp()
        {
            PlayerAvatar avatar = PlayerAvatar.Player;
            _hpText.CurrentText.text = string.Concat(avatar.cur_hp_string, "/", avatar.max_hp_string);
            if (avatar.max_hp != 0)
            {
                _hpProgressBar.Value = (float)avatar.cur_hp / avatar.max_hp;
            }
        }

        private void RefreshContent()
        {
            RefreshHp();
            RefreshLevel();
            RefreshPlayerName();
            RefreshCaptainImage();
            RefreshSeparateView();
            RefreshTeamDutyIcon();
            PlayerAvatar avatar = PlayerAvatar.Player;
            _containerIcon.SetIcon(MogoPlayerUtils.GetSmallIcon((int)avatar.vocation));
        }

        private void RefreshTeamDutyIcon()
        {
            int proficientId = PlayerAvatar.Player.spell_proficient;
            if ( proficientId != 0 && PlayerDataManager.Instance.TeamData.IsInTeam())
            {
                _teamDutyIcon.Visible = true;
                _teamDutyIcon.SetIcon(skill_helper.GetTeamDutyIconId(PlayerAvatar.Player.vocation, proficientId));
            }
            else
            {
                _teamDutyIcon.Visible = false;
            }
        }

        private void RefreshCaptainImage()
        {
            _captainImg.Visible = PlayerDataManager.Instance.TeamData.IsCaptain();
        }

        private void RefreshPlayerName()
        {
            _playerName.CurrentText.text = PlayerAvatar.Player.name;
        }

        protected void AddEventListener()
        {
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, RefreshLevel);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.cur_hp, RefreshHp);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.max_hp, RefreshHp);
            EventDispatcher.AddEventListener(TeamEvent.TEAM_REFRESH, RefreshCaptainImage);
            _headButton.onClick.AddListener(OnHeadButtonClick);
            if (_separateButton != null)
            {
                _separateButton.onClick.AddListener(OnSeparateButtonClick);
            }
            EventDispatcher.AddEventListener<ControlVehicle>(VehicleEvents.DRIVER_VEHICLE, OnDriverVehicle);
            EventDispatcher.AddEventListener<ControlVehicle>(VehicleEvents.DIVORCE_VEHICLE, OnDivorceVehicle);
        }

        protected void RemoveEventListener()
        {
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, RefreshLevel);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.cur_hp, RefreshHp);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.max_hp, RefreshHp);
            _headButton.onClick.RemoveListener(OnHeadButtonClick);
            if (_separateButton != null)
            {
                _separateButton.onClick.RemoveListener(OnSeparateButtonClick);
            }
            EventDispatcher.RemoveEventListener<ControlVehicle>(VehicleEvents.DRIVER_VEHICLE, OnDriverVehicle);
            EventDispatcher.RemoveEventListener<ControlVehicle>(VehicleEvents.DIVORCE_VEHICLE, OnDivorceVehicle);
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_REFRESH, RefreshCaptainImage);
        }

        private void RefreshLevel()
        {
            PlayerAvatar avatar = PlayerAvatar.Player;
            _levelText.CurrentText.text = avatar.level.ToString();
        }

        private void OnHeadButtonClick()
        {
            if (GameSceneManager.GetInstance().curMapType == public_config.MAP_TYPE_EXPERIENCE_COPY || GameSceneManager.GetInstance().curMapType == public_config.MAP_TYPE_NO_REWARD_CLIENT_COPY)
            {
                return;
            }
            SystemAlert.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(1409), LeaveMission);
        }

        private void OnSeparateButtonClick()
        {
            EventDispatcher.TriggerEvent(VehicleEvents.ONREQUEST_DIVORCE_VEHICLE);
        }

        private void OnDriverVehicle(ControlVehicle obj)
        {
            RefreshSeparateView();
        }

        private void OnDivorceVehicle(ControlVehicle obj)
        {
            RefreshSeparateView();
        }

        private void RefreshSeparateView()
        {
            SetSeparateButtonVisibility(PlayerAvatar.Player.vehicleManager.showDriveringButton);
        }

        private void SetSeparateButtonVisibility(bool visible)
        {
            if (_separateButton != null)
            {
                _separateButton.Visible = visible;
            }
        }

        private void LeaveMission()
        {
            //MissionManager.Instance.RequsetQuitMisstion();
            if (PlayerAutoFightManager.GetInstance().fightType == FightType.FOLLOW_FIGHT)
            {
                PlayerAutoFightManager.GetInstance().Stop();
                TeamManager.Instance.CancelFollowCaptain();
            }
            GameSceneManager.GetInstance().LeaveCombatScene();
        }

        protected override void OnEnable()
        {
            AddEventListener();
            RefreshContent();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }
    }
}
