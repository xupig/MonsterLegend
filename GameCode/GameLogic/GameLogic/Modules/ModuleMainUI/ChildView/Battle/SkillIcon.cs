﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using Common.Data;
using Common.ClientConfig;
using GameMain.GlobalManager;
using Common.Base;
using GameMain.Entities;
using Common.Utils;
using Common.ExtendComponent;

namespace ModuleMainUI
{
    public class SkillIcon : KContainer
    {
        private IconContainer _iconContainer;
        protected override void Awake()
        {
            _iconContainer = gameObject.AddComponent<IconContainer>();
            base.Awake();
        }

        private int _width = 0;
        public int width
        {
            get { return _width; }
        }

        private int _height = 0;
        public int height
        {
            get { return _height; }
        }

        private int _iconId = 0;
        public int iconId 
        {
            get{return _iconId;}
            set{
                _iconId = value;
            }
        }

        private void ResetRectTransform(int width, int height)
        {
            var rect = this.gameObject.GetComponent<RectTransform>();
            if (rect == null)
            {
                rect = this.gameObject.AddComponent<RectTransform>();
                rect.anchoredPosition3D = Vector3.zero;
                rect.anchorMin = new Vector2(0, 1);
                rect.anchorMax = new Vector2(0, 1);
                rect.pivot = new Vector2(0, 1);
                rect.localScale = Vector3.one;
            }
            rect.sizeDelta = new Vector2(width, height);
        }

        public void Reset(int iconId, int width, int height)
        {
            ResetRectTransform(width, height);
            _iconId = iconId;
            _width = width;
            _height = height;
            this.name = _iconId.ToString();
            _iconContainer.SetIcon(_iconId);
        }
    }
}
