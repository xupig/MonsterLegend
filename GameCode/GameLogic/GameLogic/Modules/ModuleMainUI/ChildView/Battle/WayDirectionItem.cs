﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using MogoEngine.Mgrs;
using GameMain.Entities;
using Common.ClientConfig;
using GameLoader.Utils.Timer;
using Common.Global;
using GameMain.GlobalManager;
using Common.Utils;
using Common.States;

namespace ModuleMainUI
{
    public class WayDirectionItem : KContainer
    {
        private StateImage _directionImage;
        private StateImage _goImgOne;
        private StateImage _goImgTwo;
        private RectTransform _goRect;
        private RectTransform _imgRect;
        private RectTransform _parentRect;
        private float _height;
        private Vector2 _centerPosition = new Vector2(Screen.width * 0.5f, Screen.height * 0.5f);

        protected override void Awake()
        {
            _goRect = GetChildComponent<RectTransform>("Container_go");
            _goImgOne = GetChildComponent<StateImage>("Container_go/Image_imgGo1");
            _goImgTwo = GetChildComponent<StateImage>("Container_go/Image_imgGo2");
            _directionImage = GetChildComponent<StateImage>("Image_imgDirection");
            _parentRect = transform.parent as RectTransform;
            _imgRect = _directionImage.GetComponent<RectTransform>();
            _imgRect.pivot = new Vector2(0.5f, 0.5f);
            _goRect.pivot = new Vector2(0.5f, 0.5f);
            _height = 0.15f * Screen.height;
        }
        
        void Update()
        {
            if (Camera.main == null) { return; }
            PlayGoAnimation();
            Vector3 autoMovePos = GameSceneManager.GetInstance().GetAutoMovePoint();
            Vector2 vector = GeometryUtil.GetTargetVector(autoMovePos);
            if (vector == Vector2.zero) return;
            Vector2 xVector = new Vector2(1, 0);
            float radian = GeometryUtil.CalculateRadianBetweenTwoVector(xVector, vector);
            if (vector.y < 0) radian = 2 * Mathf.PI - radian;
            Vector2 screenPosition = GetScreenPosition(vector, radian);
            UpdateGoPosition(screenPosition);
            UpdateArrowPosition(screenPosition, radian);
            UpdateArrowRotation(vector);
        }

        private void UpdateGoPosition(Vector2 screenPosition)
        {
            Vector3 localPosition = MogoGameObjectHelper.ScreenPointToLocalPointInRectangle(_parentRect, screenPosition);
            if (_goRect.localPosition != localPosition)
            {
                _goRect.localPosition = localPosition;
            }
        }

        private Vector2 GetScreenPosition(Vector3 vector, float radian)
        {
            float b = _height;
            float a = b * 1.2f;
            float x = _centerPosition.x + a * Mathf.Cos(radian);
            float y = _centerPosition.y + b * Mathf.Sin(radian);
            return  new Vector2(x, y);
        }

        private float _goTick;
        private void PlayGoAnimation()
        {
            _goTick += Time.deltaTime;
            if (_goTick > 0.5f)
            {
                _goTick -= 0.5f;
                OnGoStep();
            }
        }

        private int frame = 0;
        private void OnGoStep()
        {
            if (frame == 0)
            {
                frame = 1;
                _goImgOne.Visible = false;
                _goImgTwo.Visible = true;
            }
            else
            {
                frame = 0;
                _goImgOne.Visible = true;
                _goImgTwo.Visible = false;
            }
        }

        private void UpdateArrowRotation(Vector2 vector)
        {
            float angle = GeometryUtil.CalculateRotation(vector);
            Vector3 eulerAngles = _imgRect.eulerAngles;
            eulerAngles.z = -angle;
            if (_imgRect.eulerAngles != eulerAngles)
            {
                _imgRect.eulerAngles = eulerAngles;
            }
        }

        private void UpdateArrowPosition(Vector2 pivotPosition, float radian)
        {
            float b = 35 * Global.Scale;
            float a = b * 1.5f;
            float x = pivotPosition.x + a * Mathf.Cos(radian);
            float y = pivotPosition.y + b * Mathf.Sin(radian);
            Vector2 screenPosition = new Vector2(x, y);
            Vector3 localPosition = MogoGameObjectHelper.ScreenPointToLocalPointInRectangle(_parentRect, screenPosition);
            if (_imgRect.localPosition != localPosition)
            {
                _imgRect.localPosition = localPosition;
            }

        }

    }
}
