﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using UnityEngine.EventSystems;
using UnityEngine;
using MogoEngine.Events;
using Common.Data;
using Game.UI.UIComponent;
using GameMain.Entities;
using UnityEngine.UI;
using Game.UI;
using Common.Utils;

namespace ModuleMainUI
{
    public class DamageItem : KContainer
    {
        private RectTransform _rectTransform;

        public string Name { get; set;  }

        public Vector2 Size
        {
            get
            {
                return _rectTransform.sizeDelta;
            }
        }

        public Vector3 Position
        {
            get
            {
                return _rectTransform.localPosition;
            }
            set
            {
                _rectTransform.localPosition = value;
            }
        }

        public Vector3 Scale
        {
            get
            {
                return _rectTransform.localScale;
            }
            set
            {
                _rectTransform.localScale = value;
            }
        }

        public Vector2 Pivot
        {
            get
            {
                return _rectTransform.pivot;
            }
            set
            {
                _rectTransform.pivot = value;
            }
        }

        //透明度设置，0~1, 0为全透明
        public float Alpha 
        {
            get { return _alpha; }
            set 
            {
                _alpha = value;
                for (int i = 0; i < _image.Length; i++)
                {
                    _image[i].alpha = _alpha;
                }
            }
        }

        private ImageWrapper[] _image;
        private float _alpha = 255.0f;

        protected override void Awake()
        {
            _image = this.gameObject.GetComponentsInChildren<ImageWrapper>();
            _rectTransform = transform as RectTransform;
        }

        public void Show()
        {
        }

        public void Hide()
        {
            _rectTransform.localPosition = DamageList.HIDE_POSITION;
        }

    }
}
