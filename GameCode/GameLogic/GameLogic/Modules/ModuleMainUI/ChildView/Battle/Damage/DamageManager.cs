﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MogoEngine.Events;
using UnityEngine;
using Common.Data;
using Common.Base;

using MogoEngine.RPC;
using Common.Events;
using Common.ServerConfig;
using GameMain.CombatSystem;
using Common.Global;
using GameMain.GlobalManager;
using MogoEngine;
using Common.Utils;

namespace ModuleMainUI
{
    public class AttackTypeComparer : IEqualityComparer<AttackType>
    {

        public bool Equals(AttackType x, AttackType y)
        {
            return (int)x == (int)y;
        }

        public int GetHashCode(AttackType obj)
        {
            return (int)obj;
        }
    }

    public class DamageManager
    {
        private Dictionary<string, Stack<DamageItem>> _damageItemStackDic;   //保存各种空闲美术字的物体，以重复利用
        private Stack<DamageGrid> _damageGridStack;     //保存空闲伤害数字物体，以重复利用
        private Dictionary<AttackType, string> _hitTypeArtNumberTypeMap;
        private Dictionary<AttackType, string> _hitTypeArtWordTypeMap;

        /// <summary>
        /// 伤害物体（DamageGrid）最大上限个数
        /// </summary>
        public int maxCount = 10;

        public DamageManager()
        {
            _damageItemStackDic = new Dictionary<string, Stack<DamageItem>>();
            _damageGridStack = new Stack<DamageGrid>();
            _hitTypeArtNumberTypeMap = new Dictionary<AttackType, string>(new AttackTypeComparer());
            _hitTypeArtWordTypeMap = new Dictionary<AttackType, string>(new AttackTypeComparer());
            InitHitType2ArtNumberType();
            InitHitType2ArtWordType();
            EventDispatcher.AddEventListener<int>(SceneEvents.LEAVE_SCENE, OnLeaveMap);
        }

        private static DamageManager s_Instance;
        public static DamageManager Instance
        {
            get
            {
                if (s_Instance == null)
                    s_Instance = new DamageManager();
                return s_Instance;
            }
        }

        private void InitHitType2ArtNumberType()
        {
            _hitTypeArtNumberTypeMap.Add(AttackType.ATTACK_HIT, ArtNumberType.HIT);
            _hitTypeArtNumberTypeMap.Add(AttackType.ATTACK_CRITICAL, ArtNumberType.CRITICAL);
            _hitTypeArtNumberTypeMap.Add(AttackType.ATTACK_STRIKE, ArtNumberType.STRIKE);        }

        private void InitHitType2ArtWordType()
        {
            _hitTypeArtWordTypeMap.Add(AttackType.ATTACK_CRITICAL, ArtWord.CRITICAL);
            _hitTypeArtWordTypeMap.Add(AttackType.ATTACK_STRIKE, ArtWord.STRIKE);
            _hitTypeArtWordTypeMap.Add(AttackType.ATTACK_MISS, ArtWord.MISS);
        }

        private string HitType2ArtNumberType(AttackType type, bool isPlayer)
        {
            if (isPlayer == true)
            {
                return ArtNumberType.ATTACK;
            }
            if (_hitTypeArtNumberTypeMap.ContainsKey(type))
            {
                return _hitTypeArtNumberTypeMap[type];
            }
            return ArtNumberType.ATTACK;
        }

        private string HitType2ArtWordType(AttackType type, bool isPlayer)
        {
            if (isPlayer == true)
            {
                return string.Empty;
            }
            if (_hitTypeArtWordTypeMap.ContainsKey(type))
            {
                return _hitTypeArtWordTypeMap[type];
            }
            return string.Empty;
        }

        private int _currentCount = 0;
        #region 对外接口
        /**生成受伤时候的伤害数字, type所有类型已经在类DamageType中声明**/
        public void CreateDamageNumber(Vector3 position, int damage, AttackType type, bool isPlayer)
        {
            if (_currentCount > maxCount) return;
            if (Common.States.GMState.showBlood == false) return;
            if (Camera.main == null) return;
            Vector3 screenPosition = Camera.main.WorldToScreenPoint(position);
            if (MogoGameObjectHelper.CheckPositionIsInScreen(screenPosition))
            {
                DamageGrid damageGrid = GetDamageGrid();
                string word = HitType2ArtWordType(type, isPlayer);
                string colorType = HitType2ArtNumberType(type, isPlayer);
                if (damage > 0)
                {
                    word = ArtWord.ADD_GREEN;
                    colorType = ArtNumberType.HEAL;
                }
                else
                {
                    damage = -damage;
                }
                if (word != string.Empty)
                {
                    damageGrid = CreateDamageWord(damageGrid, word);
                }
                if (word != ArtWord.MISS)
                {
                    damageGrid = CreateDamageNumber(damageGrid, colorType, damage);
                }
                damageGrid.Show(position);
                _currentCount++;
            }
        }

        public void CreateVariableNumber(Vector3 position, int value, int type)
        {
            if (Common.States.GMState.showBlood == false) return;
            if (Camera.main == null) return;
            Vector3 screenPosition = Camera.main.WorldToScreenPoint(position);
            if (MogoGameObjectHelper.CheckPositionIsInScreen(screenPosition))
            {
                DamageGrid damageGrid = GetDamageGrid();
                DamageItem damageItem = GetDamageItem(type == 1 ? ArtWord.ADD_YELLOW : ArtWord.ADD_BLUE, true);
                damageGrid.AddNumberItem(damageItem);
                string str = value.ToString();
                for (int i = 0; i < str.Length; i++)
                {
                    string name = (type == 1 ? ArtNumberType.Energy : ArtNumberType.INTEGRAL) + str[i];
                    damageItem = GetDamageItem(name, true);
                    damageGrid.AddNumberItem(damageItem);
                }
                damageItem = GetDamageItem(type == 1 ? ArtWord.Energy : ArtWord.INTEGRAL, true);
                damageGrid.AddNumberItem(damageItem);
                damageGrid.Show(position, DamageStyle.Flow);
            }
        }

        private void OnLeaveMap(int mapId)
        {
            this.ClearPools();
        }

        public void ClearPools()
        {
            while (_damageGridStack.Count > 0)
            {
                var obj = _damageGridStack.Pop();
                GameObject.Destroy(obj.gameObject);
            }
            _damageGridStack.Clear();
            foreach (var pair in _damageItemStackDic)
            {
                var stack = pair.Value;
                while (stack.Count > 0)
                {
                    var item = stack.Pop();
                    GameObject.Destroy(item.gameObject);
                }
                stack.Clear();
            }
        }

        /**生成Hits的美术字效果**/
        public void Hits()
        {
            //HitsCountView hitsCountView = DamageList.AddHits();
            //hitsCountView.Show();
        }
        #endregion

        /**把Hits需要的数字加到物体中**/
        public void CreateHitsNumber(HitsCountView hitsCountView)
        {
            //string type = ArtNumberType.COMBO;
            //string str = hitsCountView.Count.ToString();
            //for (int i = 0; i < str.Length; i++)
            //{
            //    string name = type + str[i];
            //    DamageItem damageItem = GetDamageItem(name);
            //    hitsCountView.AddNumberItem(damageItem);
            //}
        }

        public void RemoveItemFormList(List<DamageItem> damageItemList)
        {
            foreach (DamageItem item in damageItemList)
            {
                Stack<DamageItem> damageItemStack;
                if (_damageItemStackDic.ContainsKey(item.Name))
                {
                    damageItemStack = _damageItemStackDic[item.Name];
                }
                else
                {
                    damageItemStack = new Stack<DamageItem>();
                    _damageItemStackDic.Add(item.Name, damageItemStack);
                }
                if (damageItemStack.Count < 7)
                {
                    damageItemStack.Push(item);
                    item.Hide();
                    //DamageList.SetItemToParent(item);
                }
                else {
                    GameObject.Destroy(item.gameObject);
                }
            }
        }

        public void RemoveDamageGrid(DamageGrid damageGrid)
        {
            damageGrid.Hide();
            _currentCount--;
            _damageGridStack.Push(damageGrid);
            //damageGrid.gameObject.SetActive(false);
        }

        private int _damageGridNumber = 0;
        private DamageGrid GetDamageGrid()
        {
            DamageGrid damageGrid;
            if (_damageGridStack.Count > 0)
            {
                damageGrid = _damageGridStack.Pop();
            }
            else
            {
                damageGrid = DamageList.AddDamageGrid();
                damageGrid.gameObject.name = "DamageGrid" + ++_damageGridNumber;
            }
            damageGrid.gameObject.SetActive(true);
            return damageGrid;
        }

        private DamageGrid CreateDamageNumber(DamageGrid damageGrid, string colorType, int value)
        {
            string str = value.ToString();
            for (int i = 0; i < str.Length; i++)
            {
                string name = colorType + str[i];
                DamageItem damageItem = GetDamageItem(name);
                damageGrid.AddNumberItem(damageItem);
            }
            return damageGrid;
        }

        private DamageGrid CreateDamageWord(DamageGrid damageGrid, string word)
        {
            DamageItem damageItem = GetDamageItem(word);
            damageGrid.AddNumberItem(damageItem);
            return damageGrid;
        }
        
        private DamageItem GetDamageItem(string name, bool isTop = false)
        {
            DamageItem damageItem;
            if (_damageItemStackDic.ContainsKey(name))
            {
                Stack<DamageItem>  damageItemStack = _damageItemStackDic[name];
                if (damageItemStack.Count > 0)
                {
                    damageItem = damageItemStack.Pop();
                    return damageItem;
                }
            }
            damageItem = DamageList.AddItem(name, isTop);
            return damageItem;
        }

    }
}
