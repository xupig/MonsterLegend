﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    public class ArtNumberType
    {
        /// <summary>
        /// 攻击小怪
        /// </summary>
        public const string HIT = "lianji";

        /// <summary>
        /// 受到攻击
        /// </summary>
        public const string ATTACK = "baojihong";

        /// <summary>
        /// 暴击
        /// </summary>
        public const string CRITICAL = "baojilan";

        /// <summary>
        /// 破击
        /// </summary>
        public const string STRIKE = "pojizi";

        /// <summary>
        /// 能量
        /// </summary>
        public const string Energy = "nengliang";

        /// <summary>
        /// 积分
        /// </summary>
        public const string INTEGRAL = "jifen";

        /// <summary>
        /// 回血
        /// </summary>
        public const string HEAL = "sharedhuixuei";

    }

    public class ArtWord
    {
        /// <summary>
        /// 暴击
        /// </summary>
        public const string CRITICAL = "baojilan";

        /// <summary>
        /// 破击
        /// </summary>
        public const string STRIKE = "pojizi";

        /// <summary>
        /// 闪避
        /// </summary>
        public const string MISS = "shanbibai";

        /// <summary>
        /// 能量
        /// </summary>
        public const string Energy = "nengliang";

        /// <summary>
        /// 积分
        /// </summary>
        public const string INTEGRAL = "jifen";

        /// <summary>
        /// 加号黄
        /// </summary>
        public const string ADD_YELLOW = "jiahaohuang";

        /// <summary>
        /// 加号蓝
        /// </summary>
        public const string ADD_BLUE = "jiahaolan";

        /// <summary>
        /// 加号绿
        /// </summary>
        public const string ADD_GREEN = "huixue";

    }

}
