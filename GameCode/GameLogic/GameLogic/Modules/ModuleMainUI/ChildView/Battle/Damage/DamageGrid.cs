﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using UnityEngine.EventSystems;
using UnityEngine;
using MogoEngine.Events;
using Common.Data;
using Game.UI.UIComponent;
using GameMain.Entities;

using MogoEngine.RPC;
using Common.Global;
using GameMain.GlobalManager;
using MogoEngine;
using GameData;

namespace ModuleMainUI
{
    public enum DamageStyle
    {
        Expand = 1,
        Flow = 2,
    }

    public class DamageGrid : KContainer
    {
        private float _height = 30f;
        private float _range = 30f;
        private float _expandTime = 0.1f;
        private float _expandHeight = 0f;
        private float _expandScale = 1.5f;
        private float _expandAlpha = 1f;
        private float _shrinkTime = 0.4f;
        private float _shrinkHeight = 20f;
        private float _shrinkRight = 0;
        private float _shrinkScale = 0.9f;
        private float _shrinkAlpha = 0.38f;

        private Vector3 _floatPosition;
        private List<DamageItem> _damageItemList = new List<DamageItem>();

        private Vector3 _size = Vector3.zero;
        private float _randOffX;
        private float _randOffY;

        private UITweener.OnFinished _onExpandFinished;
        private UITweener.OnFinished _onShrinkFinished;

        protected override void Awake()
        {
            _height = float.Parse(global_params_helper.GetGlobalParam(101));
        }

        private void ExpandParam()
        {
            _range = 30f;
            _expandTime = 0.1f;
            _expandHeight = 0f;
            _expandScale = 1.5f;
            _expandAlpha = 1f;
            _shrinkTime = 0.4f;
            _shrinkHeight = 20f;
            _shrinkRight = 0;
            _shrinkScale = 0.9f;
            _shrinkAlpha = 0.38f;
        }

        private void FlowParam()
        {
            _range = 5f;
            _expandTime = 0.1f;
            _expandHeight = 0f;
            _expandScale = 1f;
            _expandAlpha = 1f;
            _shrinkTime = 0.3f;
            _shrinkHeight = 5f;
            _shrinkRight = 30f;
            _shrinkScale = 1f;
            _shrinkAlpha = 0.8f;
        }

        private void InitAnimation()
        {
            _randOffX = UnityEngine.Random.Range(-_range, _range);
            _randOffY = UnityEngine.Random.Range(-_range, _range);
        }

        private void SetStartPosition(Vector3 localPosition)
        {
            localPosition.y -= Screen.height / Global.Scale;
            localPosition.x += _randOffX;
            localPosition.y += _randOffY;
            localPosition.z = 0;
            float _startX = localPosition.x - _size.x / 2;
            foreach (DamageItem item in _damageItemList)
            {
                item.Pivot = new Vector2(0f, 0.5f);
                item.Position = new Vector3(_startX, localPosition.y, 0);
                _startX += item.Size.x;
            }
        }

        private void SetStartScale()
        {
            float sizeX = _size.x;
            foreach (DamageItem item in _damageItemList)
            {
                item.Scale = Vector3.one;
                item.Pivot = new Vector2(sizeX / item.Size.x * 0.5f, 0.5f);
                sizeX -= item.Size.x * 2;
                item.Position += new Vector3(item.Size.x * item.Pivot.x, 0, 0);
            }
        }

        private void SetStartAlpha()
        {
            foreach (DamageItem item in _damageItemList)
            {
                item.Alpha = 1;
            }
        }

        private void BeginExpand()
        {
            Vector3 damagePosition = _floatPosition + new Vector3(0, _height, 0);
            Vector3 screenPosition = Camera.main.WorldToScreenPoint(damagePosition);
            Vector3 localPosition = screenPosition / Global.Scale;
            InitAnimation();
            SetStartPosition(localPosition);
            SetStartScale();
            SetStartAlpha();
            if(_onExpandFinished == null)
            {
                _onExpandFinished = BeginShrink;
            } 
            foreach (DamageItem item in _damageItemList)
            {
                Vector3 targetPosition = item.Position + new Vector3(0, _expandHeight, 0);
                TweenPosition.Begin(item.gameObject, _expandTime, targetPosition, _onExpandFinished);
                Vector3 targetScale = new Vector3(_expandScale, _expandScale, 1.0f);
                TweenScale.Begin(item.gameObject, _expandTime, targetScale, UITweener.Method.EaseIn);
                TweenDamageAlpha.Begin(item.gameObject, _expandTime, _expandAlpha);
            }
        }

        private void BeginShrink(UITweener tween)
        {
            if(_onShrinkFinished == null)
            {
                _onShrinkFinished = OnTweenEnd;
            }
            foreach (DamageItem item in _damageItemList)
            {
                Vector3 targetPosition = item.Position + new Vector3(_shrinkRight, _shrinkHeight, 0);
                TweenPosition.Begin(item.gameObject, _shrinkTime, targetPosition, _onShrinkFinished);
                Vector3 targetScale = new Vector3(_shrinkScale, _shrinkScale, 1.0f);
                TweenScale.Begin(item.gameObject, _shrinkTime, targetScale, UITweener.Method.EaseOut);
                TweenDamageAlpha.Begin(item.gameObject, _shrinkTime, _shrinkAlpha);
            }
        }

        private void OnTweenEnd(UITweener tween)
        {
            foreach (DamageItem item in _damageItemList)
            {
                item.GetComponent<TweenPosition>().enabled = false;
                item.GetComponent<TweenScale>().enabled = false;
                item.GetComponent<TweenDamageAlpha>().enabled = false;
            }
            RemoveDamage();
        }

        private void RemoveDamage()
        {
            DamageManager.Instance.RemoveItemFormList(_damageItemList);
            RemoveAllItem();
            DamageManager.Instance.RemoveDamageGrid(this);
        }

        public void Show(Vector3 position, DamageStyle style = DamageStyle.Expand)
        {
            _floatPosition = position;
            SetShowParam(style);
            BeginExpand();
        }

        private void SetShowParam(DamageStyle style)
        {
            switch (style)
            {
                case DamageStyle.Expand:
                    ExpandParam();
                    break;
                case DamageStyle.Flow:
                    FlowParam();
                    break;
            }
        }

        public void Hide()
        {
            _floatPosition = Vector3.zero;
        }

        public void AddNumberItem(DamageItem damageItem)
        {
            _damageItemList.Add(damageItem);
            damageItem.Show(); ;
            _size.x += damageItem.Size.x;
            _size.y = Mathf.Max(damageItem.Size.y, _size.y);
        }

        private void RemoveAllItem()
        {
            _damageItemList.Clear();
            _size = Vector2.zero;
        }

    }
}
