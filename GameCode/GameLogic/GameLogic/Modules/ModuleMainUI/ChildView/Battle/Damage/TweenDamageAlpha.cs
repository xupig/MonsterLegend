using ModuleMainUI;
using UnityEngine;

namespace ModuleMainUI
{
    public class TweenDamageAlpha : UITweener
    {
        public float from = 1f;
        public float to = 1f;

        DamageItem damageItem;

        public float Alpha
        {
            get
            {
                if (damageItem != null) return damageItem.Alpha;
                return 0f;
            }
            set
            {
                if (damageItem != null) damageItem.Alpha = value;
            }
        }

        void Awake()
        {
            damageItem = GetComponent<DamageItem>();
        }

        override protected void OnUpdate(float factor, bool isFinished) { Alpha = Mathf.Lerp(from, to, factor); }

        static public TweenDamageAlpha Begin(GameObject go, float duration, float alpha)
        {
            TweenDamageAlpha comp = UITweener.Begin<TweenDamageAlpha>(go, duration);
            comp.from = comp.Alpha;
            comp.to = alpha;

            if (duration <= 0f)
            {
                comp.Sample(1f, true);
                comp.enabled = false;
            }
            return comp;
        }
    }
}
