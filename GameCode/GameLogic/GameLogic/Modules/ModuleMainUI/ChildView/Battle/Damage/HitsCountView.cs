﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using UnityEngine.EventSystems;
using UnityEngine;
using MogoEngine.Events;
using Common.Data;
using Game.UI.UIComponent;

namespace ModuleMainUI
{
    public class HitsCountView : KContainer
    {
        private float _hitTime = 2.0f;    //每次Hit持续的时间
        private float _animationTime = 0.3f;   //动画播放时间
        private float _startScale = 1.5f; //动画开始缩放的大小
        private float _targetScale = 1.0f; //动画结束缩放的大小

        public int Count { get; set; }

        private RectTransform _transform;
        private KContainer _holder;
        private int _showCount = 0;
        private List<DamageItem> _damageItemList = new List<DamageItem>();
        private Vector3 _size = Vector3.zero;
        private Vector3 _center;

        private float _startTime;
        private float _curScale;
        private float _speedScale;
        private Vector2 _pivot = new Vector2(0.4f, 0.25f);

        protected override void Awake()
        {
            Count = 0;

            AdjustNumberAnchor();
            InitTemplate();
        }

        private void AdjustNumberAnchor()
        {
            _transform = this.GetComponent<RectTransform>();
            _transform.pivot = _pivot;
            Vector3 localPosition = _transform.localPosition;
            localPosition.x += _pivot.x * _transform.sizeDelta.x;
            localPosition.y -= (1.0f - _pivot.y) * _transform.sizeDelta.y;
            _transform.localPosition = localPosition;
        }

        private void InitTemplate()
        {
            KContainer container = this.GetComponent<KContainer>();
            _holder = container.GetChildComponent<KContainer>("Container_holder");
            _holder.gameObject.SetActive(false);
            RectTransform rect = _holder.GetComponent<RectTransform>();
            _center = rect.localPosition;
            _center.x += 0.5f * rect.sizeDelta.x;
            _center.y -= 0.5f * rect.sizeDelta.y;
        }

        protected override void Start()
        {

        }

        void Update()
        {
            if (_showCount != Count)
            {
                _showCount = Count;
                UpdateNumber();
            }

            float currentTime = Time.time;
            _curScale = Mathf.MoveTowards(_curScale, _targetScale, _speedScale * Time.deltaTime);
            _transform.localScale = new Vector3(_curScale, _curScale, 1.0f);

            if (currentTime > _startTime + _hitTime)
            {
                DamageManager.Instance.RemoveItemFormList(_damageItemList);
                RemoveAllItem();
                Count = 0;
                _showCount = Count;
                Hide();
            }

        }

        private void Init()
        {
            _startTime = Time.time;
            _curScale = _startScale;
            _speedScale = Mathf.Abs(_targetScale - _curScale) / _animationTime;

            CalculatePosition();
        }

        private void CalculatePosition()
        {
            foreach (DamageItem item in _damageItemList)
            {
                item.Alpha = 255.0f;
                Vector3 localPosition = item.gameObject.transform.localPosition;
                localPosition.x -= _size.x * 0.5f;
                localPosition.y += _size.y * 0.5f;
                localPosition += _center;
                item.gameObject.transform.localPosition = localPosition;
            }
        }

        private void UpdateNumber()
        {
            DamageManager.Instance.RemoveItemFormList(_damageItemList);
            RemoveAllItem();
            DamageManager.Instance.CreateHitsNumber(this);
            Init();
        }

        public void Show()
        {
            this.gameObject.SetActive(true);
        }

        public void Hide()
        {
            this.gameObject.SetActive(false);
        }

        public void AddNumberItem(DamageItem artNumberItem)
        {
            RectTransform rectTransform = artNumberItem.GetComponent<RectTransform>();
            _damageItemList.Add(artNumberItem);
            rectTransform.SetParent(this.transform, false);
            rectTransform.localScale = Vector3.one;
            artNumberItem.gameObject.SetActive(true);
            rectTransform.localPosition = new Vector3(_size.x, 0, 0);
            _size.x += rectTransform.sizeDelta.x;
            _size.y = Mathf.Max(rectTransform.sizeDelta.y, _size.y);
        }

        private void RemoveAllItem()
        {
            foreach (DamageItem item in _damageItemList)
            {
                item.transform.SetParent(this.transform.parent, false);
            }
            _damageItemList.Clear();
            _size = Vector3.zero;
        }


    }
}
