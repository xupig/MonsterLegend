﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Game.UI.UIComponent;
using Common.Utils;

namespace ModuleMainUI
{
    public class DamageList : KContainer
    {
        private static GameObject _numberGo;
        private static GameObject _damageGo;
        private static GameObject _wordGo;
        private static Dictionary<string, GameObject> _itemDict;
        private static Transform _underTransform;
        private static Transform _topTransform;
        //private HitsCountView _hitsCountView;

        public static readonly Vector3 HIDE_POSITION = new Vector3(-1000, -1400, 0);

        protected override void Awake()
        {
            KContainer container = this.GetComponent<KContainer>();
            //_hitsCountView = container.AddChildComponent<HitsCountView>("Container_hits");
            _numberGo = container.GetChild("Container_number");
            _wordGo = container.GetChild("Container_word");
            _damageGo = container.GetChild("Container_damage");
            _numberGo.transform.localPosition = HIDE_POSITION;
            _wordGo.transform.localPosition = HIDE_POSITION;
            _damageGo.transform.localPosition = HIDE_POSITION;
            _underTransform = GetChild("Container_layer1").transform;
            _topTransform = GetChild("Container_layer2").transform;
            InitItemDict();
            RecalculateWordGoSize();
            //_hitsCountView.Hide();
        }

        private void InitItemDict()
        {
            _itemDict = new Dictionary<string, GameObject>();
            for (int i = 0; i < _numberGo.transform.childCount; i++)
            {
                GameObject item = _numberGo.transform.GetChild(i).gameObject;
                _itemDict.Add(item.name.Substring(7), item);
            }
            for (int i = 0; i < _wordGo.transform.childCount; i++)
            {
                GameObject item = _wordGo.transform.GetChild(i).gameObject;
                _itemDict.Add(item.name.Substring(10), item);
            }
        }

        private void RecalculateWordGoSize()
        {
            for (int i = 0; i < _wordGo.transform.childCount; i++)
            {
                RectTransform rect = _wordGo.transform.GetChild(i).GetComponent<RectTransform>();
                RectTransform wordRect = rect.GetChild(rect.childCount-1).GetComponent<RectTransform>();
                rect.sizeDelta = wordRect.sizeDelta;
                Vector3 rectPosition = wordRect.localPosition;
                wordRect.localPosition = Vector3.zero;
                for (int j = rect.childCount - 2; j >= 0; j--)
                {
                    RectTransform backRect = rect.GetChild(j).GetComponent<RectTransform>();
                    backRect.localPosition = new Vector3(-rectPosition.x, -rectPosition.y, 0);
                }
            }
        }

        public static DamageItem AddItem(string name, bool isTop)
        {
            GameObject itemGo = null;
            itemGo = _itemDict[name];
            DamageItem item = MogoGameObjectHelper.AddByTemplate<DamageItem>(itemGo, isTop ? _topTransform : _underTransform);
            item.Name = name;
            return item;
        }

        public static DamageGrid AddDamageGrid()
        {
            DamageGrid grid = MogoGameObjectHelper.AddByTemplate<DamageGrid>(_damageGo, _underTransform);
            return grid;
        }
        

        //public static void SetItemToParent(DamageItem item)
        //{
        //    item.transform.SetParent(_damageItemPool.transform, false);
        //}

        //public HitsCountView AddHits()
        //{
        //    _hitsCountView.Count++;
        //    return _hitsCountView;
        //}

    }
}
