﻿using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using UnityEngine;

namespace ModuleMainUI
{
    public class BattleFloatTip : KContainer
    {
        private StateText _text;
        private string _content;
        private RectTransform _rect;
        private uint _timerId = 0;

        protected override void Awake()
        {
            _text = gameObject.GetComponent<StateText>();
            _content = _text.CurrentText.text;
            _rect = transform as RectTransform;
            _text.Visible = false;
        }

        public void ShowFloatTip(int value)
        {
            _text.CurrentText.text = string.Format(_content, value);
            LocateText();
            _text.Visible = true;
            ResetTimer();
            TweenPosition.Begin(gameObject, 0.3f, GetStopPosition(),0,UITweener.Method.EaseOut,OnTweenEnd);
        }

        private void OnTweenEnd(UITweener tween)
        {
            ResetTimer();
            _timerId = TimerHeap.AddTimer(1000, 0, Hide);
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private void Hide()
        {
            _text.Visible = false;
        }

        private void LocateText()
        {
            if (UIManager.GetLayerVisibility(MogoUILayer.LayerUIMain) == false) return;
            if (Camera.main == null || PlayerAvatar.Player == null || PlayerAvatar.Player.actor == null) return;
            Vector3 screenPosition = Camera.main.WorldToScreenPoint(PlayerAvatar.Player.actor.position + new Vector3(0, 2f, 0));
            Vector3 localPosition = MogoGameObjectHelper.ScreenPointToLocalPointInRectangle(_rect.parent.GetComponent<RectTransform>(), screenPosition);
            localPosition += new Vector3(-0.5f * _rect.sizeDelta.x, 0.5f * _rect.sizeDelta.y, -100);
            _rect.localPosition = localPosition;
        }

        private Vector3 GetStopPosition()
        {
            return _rect.localPosition + new Vector3(75, 10, 0);
        }

    }
}
