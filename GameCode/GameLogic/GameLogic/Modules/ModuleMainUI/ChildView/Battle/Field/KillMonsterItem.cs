﻿using Common.Data;
using Common.Events;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    public class KillMonsterItem:KList.KListItemBase
    {
        private string _taskNameTemplate;
        private float _taskNameLength;
        private float _underlineLength;

        private StateText _txtTaskName;
        private StateText _txtKillNum;
        private StateImage _underlineImg;
        private KButton _button;

        private SystemNoticeRightUpData _data;

        public KComponentEvent<KillMonsterItem> onRemove = new KComponentEvent<KillMonsterItem>();

        protected override void Awake()
        {
            _txtTaskName = GetChildComponent<StateText>("Button_txtrenwuming/txtrenwuming");
            _taskNameTemplate = _txtTaskName.CurrentText.text;
            _taskNameLength = _txtTaskName.GetComponent<RectTransform>().sizeDelta.x;
            _underlineImg = GetChildComponent<StateImage>("Button_txtrenwuming/xiahuaxian");
            _underlineLength = _underlineImg.GetComponent<RectTransform>().sizeDelta.x;
            _underlineImg.AddAllStateComponent<Resizer>();
            _txtKillNum = GetChildComponent<StateText>("Label_txtShuliang");
            _button = GetChildComponent<KButton>("Button_txtrenwuming");
            _button.onClick.AddListener(OnTaskClick);
        }

        public override void Dispose()
        {
            _data = null;
        }
        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as SystemNoticeRightUpData;
                Refresh();
            }
        }

        private void Refresh()
        {
            _txtTaskName.ChangeAllStateText(string.Format(_taskNameTemplate,monster_helper.GetMonsterName(_data.rightUpTargetId)));
            int killNum = _data.killMonsterNum > _data.monsterTotalNum?_data.monsterTotalNum:_data.killMonsterNum;
            _txtKillNum.CurrentText.text = string.Format("{0}/{1}", killNum, _data.monsterTotalNum);

            RefreshUnderline();
        }

        private void RefreshUnderline()
        {
            TextWrapper wrapper = _txtTaskName.GetComponentInChildren<TextWrapper>();
            _underlineImg.ScaleX = wrapper.preferredWidth / _taskNameLength;
        }

        private void OnTaskClick()
        {
            int mapId = GameSceneManager.GetInstance().curMapID;
            int insId = map_helper.GetInstanceIDByMapID(mapId);
            int monsterId = _data.rightUpTargetId;

            EventDispatcher.TriggerEvent(TaskEvent.FIND_MONSTER, insId, monsterId, true);
        }
    }
}
