﻿using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMainUI
{
    public class FieldChangeLinePanel : BasePanel
    {
        private KToggleGroup _lineTypeGroup;
        private FieldLineView _lineView;
        private LineItem _rencentLoginLine;

        private const int RECOMMENDED_LINE = 1;
        private const int ALL_LINE = 0;

        protected override void Awake()
        {
            base.Awake();

            CloseBtn = GetChildComponent<KButton>("Container_xuanzefenxian/Button_close");
            _lineTypeGroup = GetChildComponent<KToggleGroup>("Container_xuanzefenxian/ToggleGroup_LineType");
            _lineView = AddChildComponent<FieldLineView>("Container_xuanzefenxian/Container_tuijianfengxian");
            _rencentLoginLine = AddChildComponent<LineItem>("Container_xuanzefenxian/Container_zuijindenglu");
        }

        protected override PanelIdEnum Id
        {
            get
            {
                return  PanelIdEnum.FieldChangeLine; 
            }
        }

        public override void OnShow(object data)
        {
            EventDispatcher.TriggerEvent(MainUIEvents.RESET_CONTROL_STICK);
            AddListener();
            MissionManager.Instance.RequestLineInfo(GameSceneManager.GetInstance().curMapID);
        }

        public override void OnClose()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _lineTypeGroup.onSelectedIndexChanged.AddListener(OnLineTypeChanged);
            EventDispatcher.AddEventListener<PbMapLinesState>(CopyEvents.FieldShowAllLine, RefreshLineInfo);
        }

        private void RemoveListener()
        {
            _lineTypeGroup.onSelectedIndexChanged.RemoveListener(OnLineTypeChanged);
            EventDispatcher.RemoveEventListener<PbMapLinesState>(CopyEvents.FieldShowAllLine, RefreshLineInfo);
        }


        private void RefreshLineInfo(PbMapLinesState lineState)
        {
            List<PbMapOneLineState> showLineList;
            int count = lineState.lines_state.Count;
            for (int i = 0; i < count; i++)
            {
                if(lineState.lines_state[i].line_num == PlayerAvatar.Player.curLine)
                {
                    _rencentLoginLine.Data = lineState.lines_state[i];
                }
            }
            if (_lineTypeGroup.SelectIndex == RECOMMENDED_LINE)
            {
                showLineList = new List<PbMapOneLineState>();
                for (int i = 0; i < count; i++)
                {
                    if (lineState.lines_state[i].state == 1)
                    {
                        showLineList.Add(lineState.lines_state[i]);
                    }
                }
            }
            else
            {
                showLineList = lineState.lines_state;
            }

            _lineView.ShowLineInfo(showLineList);
        }

        private void OnLineTypeChanged(KToggleGroup toggleGroup, int index)
        {
            MissionManager.Instance.RequestLineInfo(GameSceneManager.GetInstance().curMapID);
        }


    }
}
