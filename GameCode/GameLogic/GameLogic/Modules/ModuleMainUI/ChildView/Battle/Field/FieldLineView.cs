﻿using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMainUI
{
    public class FieldLineView : KContainer
    {
        private KScrollPage _scrollPage;
        private KList _lineList;

        protected override void Awake()
        {
            base.Awake();
            _lineList = GetChildComponent<KList>("ScrollPage_tuijianfenxian/mask/content");
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_tuijianfenxian");
            InitLineList();
        }

        private void InitLineList()
        {
            _scrollPage.SetDirection(KScrollPage.KScrollPageDirection.LeftToRight);

            _lineList.SetDirection(KList.KListDirection.LeftToRight, 3, 3);
            _lineList.SetPadding(5, 10, 5, 10);
            _lineList.SetGap(20, 20);
        }
        public void ShowLineInfo(List<PbMapOneLineState> lineStateList)
        {
            Visible = true;
            _lineList.RemoveAll();
            _lineList.SetDataList<LineItem>(lineStateList);
            _scrollPage.CurrentPage = 0;
            _scrollPage.TotalPage = (lineStateList.Count - 1) / 9 + 1;
            _scrollPage.UpdateArrow(_scrollPage.CurrentPage, _scrollPage.TotalPage);
        }

        public void Hide()
        {
            Visible = false;
        }

        //private void RefreshLineInfo(PbMapLinesState lineState)
        //{
        //    List<PbMapOneLineState> lineStateList = lineState.lines_state;
        //    _lineList.RemoveAll();
        //    _lineList.SetDataList<LineItem>(lineStateList);
        //}
    }
}
