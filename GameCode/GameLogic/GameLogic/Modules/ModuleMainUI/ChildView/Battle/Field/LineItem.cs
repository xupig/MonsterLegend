﻿using Common.Base;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMainUI
{
    public class LineItem:KList.KListItemBase
    {
        private KButton _button;
        private StateImage _fullLineImg;
        private StateImage _smoothLineImg;
        private StateText _lineName;

        private PbMapOneLineState _data;

        protected override void Awake()
        {
            _button = GetChildComponent<KButton>("Button_server");
            _fullLineImg = _button.GetChildComponent<StateImage>("baomantuoyuanIcon");
            _smoothLineImg = _button.GetChildComponent<StateImage>("liuchangtuoyuanIcon");
            _button.GetChildComponent<StateImage>("weihutuoyuanIcon").Visible = false;
            _button.GetChildComponent<StateImage>("fanmangtuoyuanIcon").Visible = false;
            _lineName = _button.GetChildComponent<StateText>("label");
            _button.onClick.AddListener(OnLineClick);
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as PbMapOneLineState;
                    Refresh();
                }
            }
        }

        public override void Dispose()
        {
            _data = null;
            _button.onClick.RemoveListener(OnLineClick);
        }


        private void Refresh()
        {
            if (_data.state == 1)
            {
                _fullLineImg.Visible = false;
                _smoothLineImg.Visible = true;
            }
            else if (_data.state >= 2)
            {
                _fullLineImg.Visible = false;
                _smoothLineImg.Visible = true;
            }
            _lineName.ChangeAllStateText(string.Format("{0}线", _data.line_num));
        }


        private void OnLineClick()
        {
            MissionManager.Instance.RequestSwitchLine(GameSceneManager.GetInstance().curMapID, (int)_data.line_num);
            UIManager.Instance.ClosePanel(PanelIdEnum.FieldChangeLine);
        }

    }
}
