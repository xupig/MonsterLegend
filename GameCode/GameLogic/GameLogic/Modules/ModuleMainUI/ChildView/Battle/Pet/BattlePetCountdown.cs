﻿using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    public class BattlePetCountdown : KContainer
    {
        private ArtisticNumberList _artisticNumberList;
        private uint _timerId = TimerHeap.INVALID_ID;

        protected override void Awake()
        {
            _artisticNumberList = gameObject.GetComponent<ArtisticNumberList>();
            _artisticNumberList.Init("Container_sharedxiaoshu0", "Static_sharedmiao");
        }

        protected override void OnDisable()
        {
            RemoveTimer();
        }

        private void RemoveTimer()
        {
            if (_timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = TimerHeap.INVALID_ID;
            }
        }

        private void AddTimer(PetInfo petInfo)
        {
            if (_timerId == TimerHeap.INVALID_ID)
            {
                _timerId = TimerHeap.AddTimer<PetInfo>(0, 500, OnTick, petInfo);
            }
        }

        private void OnTick(PetInfo petInfo)
        {
            int second = petInfo.GetLeftReviveSecond();
            if (second <= 0)
            {
                RemoveTimer();
                Visible = false;
            }
            else
            {
                _artisticNumberList.Refresh(second, true);
            }
        }

        public void Refresh(PetInfo petInfo)
        {
            RemoveTimer();
            if (petInfo.FightState == PetFightState.dead && GameSceneManager.GetInstance().curMapType == public_config.MAP_TYPE_WILD)
            {
                Visible = true;
                AddTimer(petInfo);
            }
            else
            {
                Visible = false;
            }
        }
    }
}
