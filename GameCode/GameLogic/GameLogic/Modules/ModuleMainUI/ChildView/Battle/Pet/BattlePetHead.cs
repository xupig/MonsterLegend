﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleMainUI
{
    public class BattlePetHead : KList.KListItemBase
    {
        private PetInfo _petInfo;

        private KDummyButton _btn;
        private ItemQualityDisplayer _qualityFrame;
        private IconContainer _iconContainer;
        private BattlePetStarList _starList;
        private StateText _textLevel;
        private BattlePetCountdown _countdown;

        protected override void Awake()
        {
            _btn = gameObject.AddComponent<KDummyButton>();
            _qualityFrame = AddChildComponent<ItemQualityDisplayer>("Container_pinzhi");
            _iconContainer = AddChildComponent<IconContainer>("Container_head");
            _starList = AddChildComponent<BattlePetStarList>("Container_xingxing");
            _textLevel = GetChildComponent<StateText>("Label_txtDengji");
            _countdown = AddChildComponent<BattlePetCountdown>("Container_meishuzi");

            AddListener();
        }

        public override void Dispose()
        {
            _petInfo = null;
            RemoveListener();
        }

        private void AddListener()
        {
            _btn.onClick.AddListener(OnClick);
        }

        private void RemoveListener()
        {
            _btn.onClick.RemoveListener(OnClick);
        }

        public override object Data
        {
            set
            {
                _petInfo = value as PetInfo;
                _iconContainer.SetIcon(pet_helper.GetIcon(_petInfo.Id), LoadCallback);
                _qualityFrame.SetLoadCallback(LoadCallback);
                _qualityFrame.prefix = ItemQualityDisplayer.LARGE_PREFIX;
                _qualityFrame.Quality = pet_quality_helper.GetNameColorId(_petInfo.Id, _petInfo.Quality);
                _starList.SetNum(_petInfo.Star);
                _textLevel.ChangeAllStateText(_petInfo.Level.ToString());
                _countdown.Refresh(_petInfo);
                RefreshBtn();
            }
        }

        private void LoadCallback(GameObject go)
        {
            ImageWrapper wrapper = go.GetComponent<ImageWrapper>();
            if (_petInfo.FightState != PetFightState.idle)
            {
                wrapper.SetGray(0f);
            }
            else
            {
                wrapper.SetGray(1f);
            }
        }

        private void RefreshBtn()
        {
            if (_petInfo.FightState == PetFightState.idle)
            {
                _btn.interactable = true;
            }
            else
            {
                _btn.interactable = false;
            }
        }

        private void OnClick()
        {
            if (_petInfo.FightState == PetFightState.idle)
            {
                EventDispatcher.TriggerEvent<int>(BattleUIEvents.BATTLE_PET_REPLACE_CLICK, _petInfo.Id);
            }
        }
    }
}
