﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    public class BattlePetStarList : KContainer
    {
        private const int MAX_STAR_NUM = 5;

        private Vector3 _initPosition;
        private RectTransform _rect;

        private List<StateImage> _imageList;

        protected override void Awake()
        {
            _initPosition = transform.localPosition;
            _rect = gameObject.GetComponent<RectTransform>();
            _imageList = new List<StateImage>();
            for (int i = 0; i < 5; i++)
            {
                _imageList.Add(GetChildComponent<StateImage>(string.Format("Image_xingxing{0}", i)));
            }
        }

        public void SetNum(int num)
        {
            for (int i = 0; i < _imageList.Count; i++)
            {
                if (i < num)
                {
                    _imageList[i].Alpha = 1f;
                }
                else
                {
                    _imageList[i].Alpha = 0f;
                }
            }
            float deltaX = (MAX_STAR_NUM - num) / (2f * MAX_STAR_NUM) * _rect.sizeDelta.x;
            transform.localPosition = new Vector3(_initPosition.x + deltaX, _initPosition.y, _initPosition.z);
        }
    }
}
