﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using MogoEngine.Mgrs;
using GameMain.Entities;
using Common.ClientConfig;
using GameLoader.Utils.Timer;
using Common.Global;
using GameMain.GlobalManager;
using Common.Utils;
using Common.States;
using MogoEngine;
using Common.ExtendComponent;

namespace ModuleMainUI
{
    public class LockTargetView : KContainer
    {
        private StateImage _arrowImg;
        private StateImage _lockImg;
        private KContainer _container;
        private RectTransform _parentRect;
        private RectTransform _arrowRect;
        private RectTransform _lockRect;

        private uint _entityId;
        private readonly Vector2 _centerPosition = new Vector2(Screen.width / 2, Screen.height / 2);
        private Vector2[] _rectPoint = new Vector2[5];

        private float _height;
        private bool _isRise;
        private const float SPEED = 0.02f;

        protected override void Awake()
        {
            _lockImg = GetChildComponent<StateImage>("Container_img/Image_suoding");
            _arrowImg = GetChildComponent<StateImage>("Container_img/Image_gwsjzyHuang");
            _container = GetChildComponent<KContainer>("Container_img");
            _parentRect = GetChildComponent<RectTransform>("Container_img");
            _arrowRect = _arrowImg.transform as RectTransform;
            _lockRect = _lockImg.transform as RectTransform;
            _lockRect.pivot = new Vector2(0.5f, 0.5f);
            _arrowRect.pivot = new Vector2(0.5f, 0f);
            _rectPoint[0] = new Vector2(0, 0);
            _rectPoint[1] = new Vector2(Screen.width, 0);
            _rectPoint[2] = new Vector2(Screen.width, Screen.height);
            _rectPoint[3] = new Vector2(0, Screen.height);
            _rectPoint[4] = new Vector2(0, 0);
            gameObject.AddComponent<RaycastComponent>();
            Visible = false;
        }

        void Update()
        {
            if (Camera.main == null) { return; }
            if (_entityId == 0)
            {
                HideView();
                return;
            }
            EntityCreature entity = MogoWorld.GetEntity(_entityId) as EntityCreature;
            if (entity == null || entity.actor == null)
            {
                HideView();
                return;
            }
            ShowView();
            Vector2 screenPosition = Camera.main.WorldToScreenPoint(entity.actor.position);
            if (MogoGameObjectHelper.CheckPositionIsInScreen(screenPosition))
            {
                ShowLockInScreen(entity.actor.position);
            }
            else
            {
                ShowLockOutScreen(entity.actor.position);
            }
        }

        private void ShowLockOutScreen(Vector3 position)
        {
            UpdateShow(position);
        }

        private void ReclaculateHeight()
        {
            if (_isRise)
            {
                _height += SPEED;
            }
            else
            {
                _height -= SPEED;
            }
            if (_height >= 0.75 || _height <= 0.25)
            {
                _isRise = !_isRise;
            }
        }

        public void UpdateShow(Vector3 position)
        {
            Vector2 vector = GeometryUtil.GetTargetVector(position);
            Vector2 endPosition = _centerPosition + vector * 10000;
            Vector2 intersection;
            for (int i = 0; i < 4; i++)
            {
                if (GeometryUtil.TryGetIntersection(_centerPosition, endPosition, _rectPoint[i], _rectPoint[i + 1], out intersection))
                {
                    Vector3 screenPosition = new Vector3(intersection.x, intersection.y, 0);
                    Vector3 localPosition = MogoGameObjectHelper.ScreenPointToLocalPointInRectangle(_parentRect, screenPosition);
                    if (_arrowImg.Visible == false)
                    {
                        _arrowImg.Visible = true;
                    }
                    if (_arrowRect.transform.localPosition != localPosition)
                    {
                        _arrowRect.transform.localPosition = localPosition;
                    }
                    float angle = GeometryUtil.CalculateRotation(vector);
                    Vector3 eulerAngles = _arrowRect.eulerAngles;
                    eulerAngles.z = 180 - angle;
                    if (_arrowRect.eulerAngles != eulerAngles)
                    {
                        _arrowRect.eulerAngles = eulerAngles;
                    }
                    Vector2 xVector = new Vector2(1, 0);
                    float radian = GeometryUtil.CalculateRadianBetweenTwoVector(xVector, -vector);
                    if (-vector.y < 0)
                    {
                        radian = 2 * Mathf.PI - radian;
                    }
                    UpdateLockPosition(screenPosition, radian);
                    break;
                }
            }
        }

        private void UpdateLockPosition(Vector2 pivotPosition, float radian)
        {
            float b = 35 * Global.Scale;
            float a = b * 1.5f;
            float x = pivotPosition.x + a * Mathf.Cos(radian);
            float y = pivotPosition.y + b * Mathf.Sin(radian);
            Vector2 screenPosition = new Vector2(x, y);
            Vector3 localPosition = MogoGameObjectHelper.ScreenPointToLocalPointInRectangle(_parentRect, screenPosition);
            if (_lockRect.localPosition != localPosition)
            {
                _lockRect.localPosition = localPosition;
            }

        }

        private void ShowLockInScreen(Vector3 position)
        {
            if (_arrowImg.Visible == true)
            {
                _arrowImg.Visible = false;
            }
            position.y += _height;
            Vector2 screenPosition = Camera.main.WorldToScreenPoint(position);
            Vector3 localPosition = MogoGameObjectHelper.ScreenPointToLocalPointInRectangle(_parentRect, screenPosition);
            if (_lockImg.transform.localPosition != localPosition)
            {
                _lockImg.transform.localPosition = localPosition;
            }
            ReclaculateHeight();
        }

        private void ShowView()
        {
            if (_container.Visible == false)
            {
                _container.Visible = true;
            }
        }

        private void HideView()
        {
            if (_container.Visible == true)
            {
                _container.Visible = false;
            }
        }


        public void SetEntityId(uint entityId)
        {
            _entityId = entityId;
            _height = 0.25f;
            _isRise = true;
        }
    }
}
