﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using GameMain.Entities;
using Common.Utils;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using ModuleCommonUI;
using UnityEngine;
using MogoEngine;
using GameData;
using UnityEngine.UI;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils.Timer;

namespace ModuleMainUI
{
    public class BossView : KContainer
    {
        private KDummyButton _iconButton;
        private StateText _hpText;
        private BossPartView _bossPartView;
        private StateImage[] _hpImage = new StateImage[7];
        private StateImage[] _grayHpImage = new StateImage[7];
        private bool[] _isVisible = new bool[7];

        private uint _entityId;
        private int _lifeBarCount;
        private float _grayPercent = 1f;
        private float _damping = 0.5f;
        private float _ref = 0f;
        private const int MAX_COUNT = 7;
        private const float INTERVAL = 0.02f;

        private uint _hpTimerId = 0;
        private float _percent = 1f;

        private bool _isRemove = false;

        protected override void Awake()
        {
            _iconButton = AddChildComponent<KDummyButton>("Container_hp");
            _hpText = GetChildComponent<StateText>("Container_hp/Label_txtShengming");
            InitImage();
            _bossPartView = AddChildComponent<BossPartView>("Container_buwei");
            RefreshLifeBarVisibility(MAX_COUNT);
            RefreshGrayLifeBarVisibility(MAX_COUNT);
            _iconButton.onClick.AddListener(OnIconButtonClick);
            this.Visible = false;
        }

        private void InitImage()
        {
            KContainer imageContainer = GetChildComponent<KContainer>("Container_hp/Container_shengmingtiao");
            for (int i = 0; i < MAX_COUNT; i++)
            {
                _hpImage[i] = imageContainer.GetChild(2 * i + 1).GetComponent<StateImage>();
                _grayHpImage[i] = imageContainer.GetChild(2 * i).GetComponent<StateImage>();
                _isVisible[i] = false;
                _hpImage[i].CurrentImage.type = Image.Type.Filled;
                _hpImage[i].CurrentImage.fillMethod = Image.FillMethod.Horizontal;
                _hpImage[i].CurrentImage.fillOrigin = (int)Image.OriginHorizontal.Left;
                _grayHpImage[i].CurrentImage.type = Image.Type.Filled;
                _grayHpImage[i].CurrentImage.fillMethod = Image.FillMethod.Horizontal;
                _grayHpImage[i].CurrentImage.fillOrigin = (int)Image.OriginHorizontal.Left;
            }
        }

        private void OnIconButtonClick()
        {
            EntityMonsterBase entityMonster = MogoWorld.GetEntity(_entityId) as EntityMonsterBase;
            if (entityMonster.partManager.CancelCurrentSelectedPartManually())
            {
                EventDispatcher.TriggerEvent<int>(SpaceActionEvents.SpaceActionEvents_Boss_Part_Selected_Changed, 0);
            }
        }

        private void ResetHpTimer()
        {
            if (_hpTimerId != 0)
            {
                TimerHeap.DelTimer(_hpTimerId);
                _hpTimerId = 0;
            }
        }

        public void Show()
        {
            _entityId = LifeBarManager.Instance.GetBossEntityId();
            _grayPercent = 1f;
            _percent = 1f;
            _ref = 0f;
            _damping = 0.5f;
            _isRemove = false;
            InitLifeCount();
            this.Visible = true;
            _bossPartView.Show(_entityId);
            RefreshContent();
            EntityMonsterBase entityMonster = MogoWorld.GetEntity(_entityId) as EntityMonsterBase;
            EntityPropertyManager.Instance.AddEventListener(entityMonster, EntityPropertyDefine.cur_hp, RefreshContent);
        }
        
        public void Hide()
        {
            EntityMonsterBase entityMonster = MogoWorld.GetEntity(_entityId) as EntityMonsterBase;
            EntityPropertyManager.Instance.RemoveEventListener(entityMonster, EntityPropertyDefine.cur_hp, RefreshContent);
            _entityId = 0;
            _bossPartView.Hide();
            if (_hpTimerId != 0)
            {
                _damping = 0.3f;
                _isRemove = true;
            }
            else
            {
                this.Visible = false;
            }
        }

        private void InitLifeCount()
        {
            EntityMonsterBase entityMonster = MogoWorld.GetEntity(_entityId) as EntityMonsterBase;
            List<int> list = monster_helper.GetMonsterHpShow((int)entityMonster.monster_id);
            _lifeBarCount = 1;
            if (list != null && list.Count > 1 && list[0] == 1)
            {
                _lifeBarCount = list[1];
            }
            for (int i = 0; i < MAX_COUNT; i++)
            {
                _isVisible[i] = false;
                _hpImage[i].Visible = _isVisible[i];
                _grayHpImage[i].Visible = _isVisible[i];
            }
        }

        private void RefreshContent()
        {
            EntityMonsterBase entity = MogoWorld.GetEntity(_entityId) as EntityMonsterBase;
            if (entity == null) return;
            _hpText.CurrentText.text = string.Concat(entity.cur_hp_string, "/", entity.max_hp_string);
            _percent = Mathf.Clamp01((float)entity.cur_hp / entity.max_hp);
            RefreshLifeBarShow();
            if (_hpTimerId == 0)
            {
                _hpTimerId = TimerHeap.AddTimer(0, 20, OnHpTimerStep);
            }
        }

        private void RefreshLifeBarShow()
        {
            RecalculateBloodVolume(_percent);
            RefreshLifeBarVisibility(_lifeBarCount);
        }

        private void OnHpTimerStep()
        {
            RefreshGrayLifeBar(_percent);
            RefreshGrayLifeBarVisibility(_lifeBarCount);
        }

        private void RefreshGrayLifeBar(float percent)
        {
            for (int i = 0; i < _lifeBarCount; i++)
            {
                _isVisible[i] = false;
            }
            _grayPercent = Mathf.SmoothDamp(_grayPercent, percent, ref _ref, _damping);
            if (Mathf.Approximately(_grayPercent, percent) || _grayPercent <= 0.001)
            {
                ResetHpTimer();
                _grayPercent = percent;
                if (_isRemove)
                {
                    this.Visible = false;
                    return;
                }
            }
            int index = GetBloodIndex(_grayPercent);
            float intervalEnd = (float)index / _lifeBarCount;
            float value = (_grayPercent - intervalEnd) * _lifeBarCount;
            _grayHpImage[index].CurrentImage.fillAmount = value;
            _isVisible[index] = true;
        }

        private void RecalculateBloodVolume(float percent)
        {
            for (int i = 0; i < _lifeBarCount; i++)
            {
                _isVisible[i] = false;
            }
            int index = GetBloodIndex(percent);
            float intervalEnd = (float)index / _lifeBarCount;
            float value = (percent - intervalEnd) * _lifeBarCount;
            _hpImage[index].CurrentImage.fillAmount = value;
            _isVisible[index] = true;
            if (index - 1 >= 0)
            {
                _hpImage[index - 1].CurrentImage.fillAmount = 1;
                _isVisible[index - 1] = true;
            }
        }

        private int GetBloodIndex(float percent)
        {
            for (int i = _lifeBarCount - 1; i >= 0; i--)
            {
                float intervalEnd = (float)i / _lifeBarCount;
                if (percent >= intervalEnd)
                {
                    return i;
                }
            }
            return 0;
        }

        private void RefreshLifeBarVisibility(int count)
        {
            for (int i = 0; i < count; i++)
            {
                if (_hpImage[i].Visible != _isVisible[i])
                {
                    _hpImage[i].Visible = _isVisible[i];
                }
            }
        }

        private void RefreshGrayLifeBarVisibility(int count)
        {
            for (int i = 0; i < count; i++)
            {
                if (_grayHpImage[i].Visible != _isVisible[i])
                {
                    _grayHpImage[i].Visible = _isVisible[i];
                }
            }
        }
    }
}
