﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using GameMain.Entities;
using Common.Utils;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using ModuleCommonUI;
using UnityEngine;
using MogoEngine;
using GameData;
using UnityEngine.UI;
using MogoEngine.Events;
using Common.Events;
using GameMain.CombatSystem;
using Common.Data;
using Common.Base;
using Common.ExtendComponent;
using Common.ServerConfig;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using Common.Global;

namespace ModuleMainUI
{
    public class BossPartPopData
    {
        public uint entityId;
        public int partId;
        public RectTransform rectTransform;
    }

    public class BossPartPopPanel : BasePanel
    {
        private KContainer _tipContainer;
        private Locater _tipLocater;
        private StateText _nameText;
        private StateText _descriptText;
        private KContainer _iconContainer;

        protected override void Awake()
        {
            _tipContainer = GetChildComponent<KContainer>("Container_buwei/Container_buweiTip");
            _tipLocater = AddChildComponent<Locater>("Container_buwei/Container_buweiTip");
            _nameText = GetChildComponent<StateText>("Container_buwei/Container_buweiTip/Container_toubuzhuangtai/Label_txtBuweimingcheng");
            _descriptText = GetChildComponent<StateText>("Container_buwei/Container_buweiTip/Label_txtMiaoshu");
            _iconContainer = GetChildComponent<KContainer>("Container_buwei/Container_buweiTip/Container_toubuzhuangtai/Container_buweitubiao/Container_icon");
            _tipContainer.Visible = false;
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.BossPartPop; }
        }

        public override void OnShow(object data)
        {
            RefreshContent(data as BossPartPopData);
        }

        public override void OnClose()
        {

        }

        private void RefreshContent(BossPartPopData bossPartData)
        {
            EntityMonsterBase entityMonster = MogoWorld.GetEntity(bossPartData.entityId) as EntityMonsterBase;
            PartData data = entityMonster.partManager.PartSubjects[bossPartData.partId].Data;
            _tipContainer.Visible = true;
            _nameText.CurrentText.text = MogoLanguageUtil.GetContent(data.name);
            _descriptText.CurrentText.text = MogoLanguageUtil.GetContent(data.desc);
            _descriptText.RecalculateCurrentStateHeight();
            MogoAtlasUtils.AddIcon(_iconContainer.gameObject, data.icons[0]);
            RecalculateLocation(bossPartData.rectTransform);
        }

        private void RecalculateLocation(RectTransform rectTransform)
        {
            Vector2 size = rectTransform.sizeDelta;
            Vector2 screenPosition = RectTransformUtility.WorldToScreenPoint(UIManager.Instance.UICamera, rectTransform.position);
            Vector3 localPosition = MogoGameObjectHelper.ScreenPointToLocalPointInRectangle(_tipLocater.transform.parent as RectTransform, screenPosition);
            _tipLocater.Position = new Vector2(localPosition.x - _tipLocater.Width * 0.5f, localPosition.y - rectTransform.sizeDelta.y * 0.5f);
        }

        
    }
}
