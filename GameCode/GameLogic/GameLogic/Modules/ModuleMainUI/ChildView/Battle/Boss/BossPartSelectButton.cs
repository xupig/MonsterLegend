﻿using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.CombatSystem;
using GameMain.Entities;
using MogoEngine;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ModuleMainUI
{
    public class BossPartSelectButton : BossPartButton
    {
        private StateText _partNameText;

        protected override void Awake()
        {
            base.Awake();
            _partNameText = GetChildComponent<StateText>("Label_mingcheng");
        }

        protected override void RefreshName()
        {
            base.RefreshName();
            EntityMonsterBase entityMonster = MogoWorld.GetEntity(_entityId) as EntityMonsterBase;
            PartSubject subject = entityMonster.partManager.PartSubjects[_partId];
            string name = MogoLanguageUtil.GetContent(subject.Data.name);
            _partNameText.CurrentText.text = name.Split('·')[0];
        }

        protected override void AutoSelectPart()
        {
            
        }

        protected override void ShowUnLockEffect()
        {
            
        }

    }
}
