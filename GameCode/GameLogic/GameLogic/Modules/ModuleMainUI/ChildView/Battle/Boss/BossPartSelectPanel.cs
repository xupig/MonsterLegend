﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using GameMain.Entities;
using Common.Utils;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using ModuleCommonUI;
using UnityEngine;
using MogoEngine;
using GameData;
using UnityEngine.UI;
using MogoEngine.Events;
using Common.Events;
using GameMain.CombatSystem;
using Common.Data;
using Common.Base;
using Common.ExtendComponent;
using Common.ServerConfig;
using GameLoader.Utils.Timer;

namespace ModuleMainUI
{
    public class BossPartSelectPanel : BasePanel
    {
        private BossPartSelectButton[] _partButton = new BossPartSelectButton[4];

        private List<int> _partList;
        private uint _entityId;

        private KButton _closeButton;
        private KDummyButton _hideButton;
        private TweenViewMove _tweener;
        private bool _isShow = false;
        private uint _checkTimerId;
        private uint _hideTimerId;

        private Vector3 _centerPosition;
        private Vector3 _size;
        private float _gap;

        protected override void Awake()
        {
            for (int i = 0; i < 4; i++)
            {
                _partButton[i] = AddChildComponent<BossPartSelectButton>("Container_buwei/Container_anniu" + i);
            }
            _closeButton = GetChildComponent<KButton>("Container_buwei/Button_close");
            _hideButton = AddChildComponent<KDummyButton>("Container_hideBtn");
            _tweener = AddChildComponent<TweenViewMove>("Container_buwei");
            _tweener.transform.localPosition = new Vector3(_tweener.transform.localPosition.x, -1000, 0);
            _hideButton.gameObject.SetActive(false);
            _isShow = false;
            _size = (_partButton[0].transform as RectTransform).sizeDelta;
            _gap = _partButton[1].transform.localPosition.x - _partButton[0].transform.localPosition.x - _size.x;
            _centerPosition = _partButton[0].transform.localPosition + new Vector3(_size.x * 2 + _gap * 1.5f, 0, 0);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.BossPartSelect; }
        }

        public override void OnShow(object data)
        {
            _entityId = (uint)data;
            AddEventListener();
            InitBossPartList();
        }

        public override void OnClose()
        {
            for (int i = 0; i < 4; i++)
            {
                _partButton[i].Hide();
            }
            RemoveEventListener();
            ResetCheckTimer();
            ResetHideTimer();
            CloseSelectView();
        }

        private void ShowSelectView()
        {
            ResetCheckTimer();
            ResetHideTimer();
            if (_isShow == false)
            {
                _isShow = true;
                _hideButton.gameObject.SetActive(false);
                _tweener = TweenViewMove.Begin(_tweener.gameObject, MoveType.Show, MoveDirection.Up, 0.8f);
                _hideTimerId = TimerHeap.AddTimer(3000, 0, ShowHideButton);
            }
        }

        private void ShowHideButton()
        {
            _hideButton.gameObject.SetActive(true);
        }

        private void CloseSelectView()
        {
            ResetCheckTimer();
            ResetHideTimer();
            if (_isShow == true)
            {
                _isShow = false;
                _hideButton.gameObject.SetActive(false);
                _tweener = TweenViewMove.Begin(_tweener.gameObject, MoveType.Hide, MoveDirection.Up, 0.8f);
                _tweener.Tweener.onFinished = OnTweenFinish;
            }
        }

        private void OnTweenFinish(UITweener tween)
        {
            _tweener.transform.localPosition = new Vector3(_tweener.transform.localPosition.x, -1000, 0);
        }

        private void CheckShowPartView()
        {
            ResetCheckTimer();
            if (_isShow == false)
            {
                EntityMonsterBase entityMonster = MogoWorld.GetEntity(_entityId) as EntityMonsterBase;
                if (entityMonster.partManager.CurrentSelectedSubject == null)
                {
                    foreach (var subject in entityMonster.partManager.PartSubjects.Values)
                    {
                        if (subject.CanBeSelected() == true)
                        {
                            ShowSelectView();
                            return;
                        }
                    }
                }
            }
        }


        protected void AddEventListener()
        {
            for (int i = 0; i < 4; i++)
            {
                _partButton[i].onClick.AddListener(OnPartClick);
                _partButton[i].onLongClickBegin.AddListener(OnShowPartTip);
                _partButton[i].onLongClickEnd.AddListener(OnHidePartTip);
            }
            EventDispatcher.AddEventListener<int, int>(SpaceActionEvents.SpaceActionEvents_Change_Boss_Part, ChangePartStatus);
            EventDispatcher.AddEventListener<int, float>(SpaceActionEvents.SpaceActionEvents_Boss_Part_Hp_Changed, UpdatePartHp);
            EventDispatcher.AddEventListener<int>(SpaceActionEvents.SpaceActionEvents_Boss_Part_Selected_Changed, OnPartSelected);
            _closeButton.onClick.AddListener(CloseSelectView);
            _hideButton.onClick.AddListener(CloseSelectView);
        }

        protected void RemoveEventListener()
        {
            for (int i = 0; i < 4; i++)
            {
                _partButton[i].onClick.RemoveListener(OnPartClick);
                _partButton[i].onLongClickBegin.RemoveListener(OnShowPartTip);
                _partButton[i].onLongClickEnd.RemoveListener(OnHidePartTip);
            }
            EventDispatcher.RemoveEventListener<int, int>(SpaceActionEvents.SpaceActionEvents_Change_Boss_Part, ChangePartStatus);
            EventDispatcher.RemoveEventListener<int, float>(SpaceActionEvents.SpaceActionEvents_Boss_Part_Hp_Changed, UpdatePartHp);
            EventDispatcher.RemoveEventListener<int>(SpaceActionEvents.SpaceActionEvents_Boss_Part_Selected_Changed, OnPartSelected);
            _closeButton.onClick.RemoveListener(CloseSelectView);
            _hideButton.onClick.RemoveListener(CloseSelectView);
        }

        private void UpdatePartHp(int partId, float percent)
        {
            int index = GetPartButtonIndexById(partId);
            _partButton[index].SetPartHP(percent);
        }

        private void ChangePartStatus(int partId, int status)
        {
            int index = GetPartButtonIndexById(partId);
            _partButton[index].SetStatusType(status);
            ResetCheckTimer();

            if (status == public_config.BODY_PART_STATE_NORMAL)
            {
                EntityMonsterBase entityMonster = MogoWorld.GetEntity(_entityId) as EntityMonsterBase;
                PartData data = entityMonster.partManager.PartSubjects[partId].Data;
                if (data.autoSelect == false)
                {
                    _checkTimerId = TimerHeap.AddTimer(500, 0, CheckShowPartView);
                }
            }
            else if (status == public_config.BODY_PART_STATE_DESTROY)
            {
                _checkTimerId = TimerHeap.AddTimer(5000, 0, CheckShowPartView);
            }
        }

        private void ResetCheckTimer()
        {
            if (_checkTimerId != 0)
            {
                TimerHeap.DelTimer(_checkTimerId);
                _checkTimerId = 0;
            }
        }

        private void ResetHideTimer()
        {
            if (_hideTimerId != 0)
            {
                TimerHeap.DelTimer(_hideTimerId);
                _hideTimerId = 0;
            }
        }

        private void OnPartClick(int partId)
        {
            EntityMonsterBase entityMonster = MogoWorld.GetEntity(_entityId) as EntityMonsterBase;
            if (partId == 0)
            {
                if (entityMonster.partManager.CancelCurrentSelectedPartManually() == false)
                {
                    return;
                }
            }
            else
            {
                if (entityMonster.partManager.SelectPart(partId) == false)
                {
                    return;
                }
            }
        }

        private void OnPartSelected(int partId)
        {
            for (int i = 0; i < 4; i++)
            {
                _partButton[i].SetSelect(false);
            }
            int index = GetPartButtonIndexById(partId);
            if (index != -1)
            {
                _partButton[index].SetSelect(true);
                _partButton[index].transform.SetAsLastSibling();
                CloseSelectView();
            }
        }

        private void OnShowPartTip(int partId)
        {

        }


        private void OnHidePartTip(int partId)
        {

        }


        private void InitBossPartList()
        {
            EntityMonsterBase entityMonster = MogoWorld.GetEntity(_entityId) as EntityMonsterBase;
            if (entityMonster.partManager.HasPart() == false)
            {
                ClosePanel();
                return;
            }
            for (int i = 0; i < 4; i++)
            {
                _partButton[i].Hide();
            }
            _partList = entityMonster.partManager.PartIdList;
            int start = 4 - _partList.Count;
            for (int i = start; i < 4; i++)
            {
                int id = _partList[i - start];
                _partButton[i].Show(_entityId, id);
            }
            RecalculatePosition();
            _checkTimerId = TimerHeap.AddTimer(500, 0, CheckShowPartView);
        }

        private void RecalculatePosition()
        {
            Vector3 startPosition = _centerPosition;
            startPosition.x -= (_partList.Count * _size.x + (_partList.Count) * _gap) / 2.0f;
            for (int i = 0; i < 4; i++)
            {
                if (_partButton[i].Visible == true)
                {
                    _partButton[i].transform.localPosition = startPosition;
                    _partButton[i].ResetOrginalPosition();
                    startPosition.x += _size.x + _gap;
                }
            }
        }

        private int GetPartButtonIndexById(int partId)
        {
            int start = 4 - _partList.Count;
            for (int i = start; i < 4; i++)
            {
                if (_partList[i-start] == partId)
                {
                    return i;
                }
            }
            return -1;
        }

    }
}
