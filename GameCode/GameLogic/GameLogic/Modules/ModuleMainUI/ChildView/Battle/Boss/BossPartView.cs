﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using GameMain.Entities;
using Common.Utils;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using ModuleCommonUI;
using UnityEngine;
using MogoEngine;
using GameData;
using UnityEngine.UI;
using MogoEngine.Events;
using Common.Events;
using GameMain.CombatSystem;
using Common.Data;
using GameMain.GlobalManager;
using Common.Base;

namespace ModuleMainUI
{
    public class BossPartView : KContainer
    {
        private BossPartButton[] _partButton = new BossPartButton[4];

        private List<int> _partList;
        private uint _entityId;

        private Vector3 _centerPosition;
        private Vector3 _size;
        private float _gap;

        protected override void Awake()
        {
            for (int i = 0; i < 4; i++)
            {
                _partButton[i] = AddChildComponent<BossPartButton>("Container_anniu" + i);
            }
            this.Visible = false;
            _size = (_partButton[0].transform as RectTransform).sizeDelta;
            _gap = _partButton[1].transform.localPosition.x - _partButton[0].transform.localPosition.x - _size.x;
            _centerPosition = _partButton[0].transform.localPosition + new Vector3(_size.x * 2 + _gap * 1.5f, 0, 0);
        }

        protected void AddEventListener()
        {
            for (int i = 0; i < 4; i++)
            {
                _partButton[i].onClick.AddListener(OnPartClick);
                _partButton[i].onLongClickBegin.AddListener(OnShowPartTip);
                _partButton[i].onLongClickEnd.AddListener(OnHidePartTip);
            }
            EventDispatcher.AddEventListener<int, int>(SpaceActionEvents.SpaceActionEvents_Change_Boss_Part, ChangePartStatus);
            EventDispatcher.AddEventListener<int, float>(SpaceActionEvents.SpaceActionEvents_Boss_Part_Hp_Changed, UpdatePartHp);
            EventDispatcher.AddEventListener<int>(SpaceActionEvents.SpaceActionEvents_Boss_Part_Selected_Changed, OnPartSelected);
        }

        protected void RemoveEventListener()
        {
            for (int i = 0; i < 4; i++)
            {
                _partButton[i].onClick.RemoveListener(OnPartClick);
                _partButton[i].onLongClickBegin.RemoveListener(OnShowPartTip);
                _partButton[i].onLongClickEnd.RemoveListener(OnHidePartTip);
            }
            EventDispatcher.RemoveEventListener<int, int>(SpaceActionEvents.SpaceActionEvents_Change_Boss_Part, ChangePartStatus);
            EventDispatcher.RemoveEventListener<int, float>(SpaceActionEvents.SpaceActionEvents_Boss_Part_Hp_Changed, UpdatePartHp);
            EventDispatcher.RemoveEventListener<int>(SpaceActionEvents.SpaceActionEvents_Boss_Part_Selected_Changed, OnPartSelected);
        }

        private void UpdatePartHp(int partId, float percent)
        {
            int index = GetPartButtonIndexById(partId);
            _partButton[index].SetPartHP(percent);
        }

        private void ChangePartStatus(int partId, int status)
        {
            int index = GetPartButtonIndexById(partId);
            _partButton[index].SetStatusType(status);
        }

        private void OnPartClick(int partId)
        {
            EntityMonsterBase entityMonster = MogoWorld.GetEntity(_entityId) as EntityMonsterBase;
            if (partId == 0)
            {
                if (entityMonster.partManager.CancelCurrentSelectedPartManually() == false)
                {
                    return;
                }
            }
            else
            {
                if (entityMonster.partManager.SelectPart(partId) == false)
                {
                    return;
                }
            }
        }

        private void OnPartSelected(int partId)
        {
            for (int i = 0; i < 4; i++)
            {
                _partButton[i].SetSelect(false);
            }
            int index = GetPartButtonIndexById(partId);
            if (index != -1)
            {
                _partButton[index].SetSelect(true);
                _partButton[index].transform.SetAsLastSibling();
            }
        }

        private void OnShowPartTip(int partId)
        {
            int index = GetPartButtonIndexById(partId);
            RectTransform rect = _partButton[index].GetComponent<RectTransform>();
            BossPartPopData bossPartData = new BossPartPopData();
            bossPartData.entityId = _entityId;
            bossPartData.partId = partId;
            bossPartData.rectTransform = rect;
            UIManager.Instance.ShowPanel(PanelIdEnum.BossPartPop, bossPartData);
        }


        private void OnHidePartTip(int partId)
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.BossPartPop);
        }

        public void Show(uint entityId)
        {
            _entityId = entityId;
            AddEventListener();
            InitBossPartList();
            if (PlayerDataManager.Instance.CopyData.IsInstancePass(int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.boss_part_select_show))))
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.BossPartSelect, entityId);
            }
        }

        public void Hide()
        {
            this.Visible = false;
            for (int i = 0; i < 4; i++)
            {
                _partButton[i].Hide();
            }
            RemoveEventListener();
            UIManager.Instance.ClosePanel(PanelIdEnum.BossPartSelect);
            UIManager.Instance.ClosePanel(PanelIdEnum.BossPartPop);
        }

        private void InitBossPartList()
        {
            EntityMonsterBase entityMonster = MogoWorld.GetEntity(_entityId) as EntityMonsterBase;
            if (entityMonster.partManager.HasPart() == false)
            {
                Hide();
                return;
            }
            this.Visible = true;

            for (int i = 0; i < 4; i++)
            {
                _partButton[i].Hide();
            }
            _partList = entityMonster.partManager.PartIdList;
            int start = 4 - _partList.Count;
            for (int i = start; i < 4; i++)
            {
                int id = _partList[i - start];
                _partButton[i].Show(_entityId, id);
            }
            RecalculatePosition();
        }

        private void RecalculatePosition()
        {
            Vector3 startPosition = _centerPosition;
            startPosition.x -= (_partList.Count * _size.x + (_partList.Count) * _gap) / 2.0f;
            for (int i = 0; i < 4; i++)
            {
                if (_partButton[i].Visible == true)
                {
                    _partButton[i].transform.localPosition = startPosition;
                    _partButton[i].ResetOrginalPosition();
                    startPosition.x += _size.x + _gap;
                }
            }
        }

        private int GetPartButtonIndexById(int partId)
        {
            int start = 4 - _partList.Count;
            for (int i = start; i < 4; i++)
            {
                if (_partList[i-start] == partId)
                {
                    return i;
                }
            }
            return -1;
        }

      
    }
}
