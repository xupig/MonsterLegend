﻿using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.CombatSystem;
using GameMain.Entities;
using MogoEngine;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ModuleMainUI
{
    public class BossPartButton : KContainer
    {
        private RectTransform _rect;
        private StateImage _lockImage;
        private KContainer _iconContainer;
        private Image _iconImage;
        private KContainer _backIconContainer;
        private KContainer _selectContainer;
        private Vector3 _orginalPosition;
        private StateImage _slotImage;
        private StateImage _breakImage;
        private LongClickButton _longClickButton;
        private KParticle _recoverEffect;
        private KParticle _destroyEffect;
        private KParticle _unLockEffect;
        private KParticle _hurtEffect;
        private KParticle _lineEffect;

        private bool _isLock;
        private bool _isDestroy;
        private bool _isRecovering;
        protected uint _entityId;
        protected int _partId;
        private int _statusType;
        private uint _recoverTimerId;
        private const int INTERVAL = 20;
        private ulong _recoverTime;
        private float _iconRadius;
        private float _iconY;
        private uint _hpTimerId;
        private float _current;
        private float _percent;
        private const float _damping = 0.1f;
        private float _ref = 0f;

        public KComponentEvent<int> onClick = new KComponentEvent<int>();
        public KComponentEvent<int> onLongClickBegin = new KComponentEvent<int>();
        public KComponentEvent<int> onLongClickEnd = new KComponentEvent<int>();

        protected override void Awake()
        {
            _lockImage = GetChildComponent<StateImage>("Image_lock");
            _iconContainer = GetChildComponent<KContainer>("Container_icon");
            _backIconContainer = GetChildComponent<KContainer>("Container_backicon");
            _selectContainer = GetChildComponent<KContainer>("Container_checkmark");
            _breakImage = GetChildComponent<StateImage>("Image_pohuai");
            _breakImage.Visible = false;
            _slotImage = GetChildComponent<StateImage>("Image_xuecao");
            _slotImage.Visible = false;
            _rect = transform as RectTransform;
            _longClickButton = gameObject.AddComponent<LongClickButton>();
            _longClickButton.onClick.AddListener(OnButtonClick);
            _longClickButton.onLongClickBegin.AddListener(OnButtonLongClickBegin);
            _longClickButton.onLongClickEnd.AddListener(OnButtonLongClickEnd);
            InitPivot();
        }

        private void InitPivot()
        {
            _rect.pivot = new Vector2(0.5f, 0.5f);
            Vector3 localposition = _rect.localPosition;
            localposition.x += _rect.sizeDelta.x * 0.5f;
            localposition.y -= _rect.sizeDelta.y * 0.5f;
            _rect.localPosition = localposition;
            _orginalPosition = localposition;
        }

        public void ResetOrginalPosition()
        {
            _orginalPosition = _rect.localPosition;
        }

        public override void OnPointerClick(PointerEventData evtData)
        {
            base.OnPointerClick(evtData);
            OnButtonClick();
        }

        private void OnButtonLongClickEnd()
        {
            onLongClickEnd.Invoke(_partId);
        }

        private void OnButtonLongClickBegin()
        {
            onLongClickBegin.Invoke(_partId);
        }

        private void OnButtonClick()
        {
            onClick.Invoke(_partId);
        }

        public void SetStatusType(int statusType)
        {
            if (gameObject.activeInHierarchy == false) { return; }
            if (_statusType != statusType)
            {
                _statusType = statusType;
                RefreshButtonStatus();
                AutoSelectPart();
            }
        }

        public void SetSelect(bool select)
        {
            if (gameObject.activeInHierarchy == false) { return; }
            if (select)
            {
                this.transform.localScale = new Vector3(1.2f, 1.2f, 1.0f);
                _selectContainer.Visible = true;
            }
            else
            {
                this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                _selectContainer.Visible = false;
            }
        }

        public void SetPartHP(float percent, bool isAnimation = true)
        {
            if (gameObject.activeInHierarchy == false) { return; }
            _percent = percent;
            if (isAnimation)
            {
                if (_hpTimerId == 0)
                {
                    _hpTimerId = TimerHeap.AddTimer(0, INTERVAL, OnHpTimerStep);

                }
            }
            else
            {
                _current = _percent;
                if (_iconImage != null)
                {
                    _iconImage.fillAmount = _current;
                }
                RefreshLineLayout(_current);
            }
        }

        protected virtual void AutoSelectPart()
        {
            if (_statusType == public_config.BODY_PART_STATE_NORMAL)
            {
                EntityMonsterBase entityMonster = MogoWorld.GetEntity(_entityId) as EntityMonsterBase;
                PartData data = entityMonster.partManager.PartSubjects[_partId].Data;
                if (data.autoSelect == true && entityMonster.partManager.CurrentSelectedSubject == null)
                {
                    onClick.Invoke(_partId);
                }
            }
        }

        private void RefreshButtonStatus()
        {
            switch (_statusType)
            {
                case public_config.BODY_PART_STATE_NORMAL:
                    SetLock(false);
                    SetDestroy(false);
                    SetPartHP(1, false);
                    EndRecover();
                    SetRecovering(false);
                    break;
                case public_config.BODY_PART_STATE_LOCK:
                    SetDestroy(false);
                    SetLock(true);
                    EndRecover();
                    SetSelect(false);
                    SetPartHP(0, false);
                    break;
                case public_config.BODY_PART_STATE_RECOVER:
                    SetLock(false);
                    SetDestroy(true);
                    SetPartHP(0, false);
                    SetRecovering(true);
                    BeginRecover();
                    SetSelect(false);
                    break;
                case public_config.BODY_PART_STATE_DESTROY:
                    SetLock(false);
                    SetDestroy(true);
                    EndRecover();
                    SetSelect(false);
                    break;
            }
        }

        private void SetLock(bool isLock)
        {
            if (_isLock == true && isLock == false)
            {
                ShowUnLockEffect();
            }
            _isLock = isLock;
            if (isLock == true)
            {
                _lockImage.gameObject.SetActive(true);
                if (_iconImage != null)
                {
                    _iconImage.gameObject.SetActive(false);
                }
            }
            else
            {
                _lockImage.gameObject.SetActive(false);
                if (_iconImage != null)
                {
                    _iconImage.gameObject.SetActive(true);
                }
            }
        }

        private void SetDestroy(bool isDestroy)
        {
            if (isDestroy == true)
            {
                if (_iconImage != null)
                {
                    _iconImage.gameObject.SetActive(false);
                }
                _breakImage.Visible = true;
                HideLineEffect();
                if (_isDestroy == false)
                {
                    ShowDestroyEffect();
                }
            }
            else
            {
                if (_iconImage != null)
                {
                    _iconImage.gameObject.SetActive(true);
                }
                _breakImage.Visible = false;
                HideHurtEffect();
                ShowLineEffect();
            }
            _isDestroy = isDestroy;
        }

        private void OnHpTimerStep()
        {
            _rect.localPosition = _orginalPosition + new Vector3(UnityEngine.Random.Range(-5, 5), UnityEngine.Random.Range(-5, 5), 0);
            _current = Mathf.SmoothDamp(_current, _percent, ref _ref, _damping);
            bool equal = false;
            if (Mathf.Abs(_current - _percent) < 0.001f)
            {
                equal = true;
                ResetHpTimer();
                _rect.localPosition = _orginalPosition;
                _current = _percent;
            }
            if (_current < _slotImage.CurrentImage.fillAmount)
            {
                ShowHurtEffect();
            }
            if (_iconImage != null)
            {
                float scale = (_iconImage.transform as RectTransform).sizeDelta.y / (_slotImage.transform as RectTransform).sizeDelta.y;
                float start = (1 - scale) / 2;
                _iconImage.fillAmount = Mathf.Clamp01((_current - start) / scale);
            }
            _slotImage.CurrentImage.fillAmount = _current;
            RefreshLineLayout(_current);
            _slotImage.Visible = equal == false;
        }

        private void ResetHpTimer()
        {
            if (_hpTimerId != 0)
            {
                TimerHeap.DelTimer(_hpTimerId);
                _hpTimerId = 0;
            }
        }

        private void RefreshLineLayout(float percent)
        {
            if (percent >= 1 || percent <= 0)
            {
                HideLineEffect();
            }
            else
            {
                ShowLineEffect();
                float y = 6 + percent * (95 - 6); //85~15
                _lineEffect.Position = new Vector3(_lineEffect.Position.x, y, _lineEffect.Position.z);
            }
        }

        private void SetRecovering(bool isRecovering)
        {
            if (_isRecovering == isRecovering) return;
            _isRecovering = isRecovering;
            ShowLineEffect();
            if (_iconImage != null)
            {
                _iconImage.gameObject.SetActive(true);
                ImageWrapper wrapper = _iconImage as ImageWrapper;
                if (_isRecovering)
                {
                    wrapper.color = Color.gray;
                }
                else
                {
                    wrapper.color = Color.white;
                }
            }
        }

        private void BeginRecover()
        {
            ResetRecoverTimer();
            _recoverTime = Global.serverTimeStamp;
            SetRecovering(true);
            _recoverTimerId = TimerHeap.AddTimer(INTERVAL, INTERVAL, DoingRecover);
        }

        private void ResetRecoverTimer()
        {
            if (_recoverTimerId != 0)
            {
                TimerHeap.DelTimer(_recoverTimerId);
                _recoverTimerId = 0;
            }
        }

        private void DoingRecover()
        {
            float timeSpan = Global.serverTimeStamp - _recoverTime;
            EntityMonsterBase entityMonster = MogoWorld.GetEntity(_entityId) as EntityMonsterBase;
            PartData data = entityMonster.partManager.PartSubjects[_partId].Data;
            float percent = timeSpan / (data.recoverDuration * 1000f);
            if (percent > 1) percent = 1;
            SetPartHP(percent);
        }

        private void ShowRecoverEffect()
        {
            if (_recoverEffect == null)
            {
                Transform trans = Instantiate(transform.parent.FindChild("Container_effect/fx_ui_4_2_2_shanguang_01")) as Transform;
                trans.SetParent(transform, false);
                trans.localScale = Vector3.one;
                _recoverEffect = trans.gameObject.AddComponent<KParticle>();
                _recoverEffect.Visible = true;
                _recoverEffect.Position = new Vector3(-52, 50, -50);
            }
            _recoverEffect.Play();
        }

        private void ShowDestroyEffect()
        {
            if (_destroyEffect == null)
            {
                Transform trans = Instantiate(transform.parent.FindChild("Container_effect/fx_ui_4_2_1_posun_01")) as Transform;
                trans.SetParent(transform, false);
                trans.localScale = Vector3.one;
                _destroyEffect = trans.gameObject.AddComponent<KParticle>();
                _destroyEffect.Visible = true;
                _destroyEffect.Position = new Vector3(-50, 50, -50);
            }
            _destroyEffect.Play();
        }

        private void ShowHurtEffect()
        {
            if (_hurtEffect == null)
            {
                Transform trans = Instantiate(transform.parent.FindChild("Container_effect/fx_ui_4_2_3_shoushang")) as Transform;
                trans.SetParent(transform, false);
                trans.localScale = Vector3.one;
                _hurtEffect = trans.gameObject.AddComponent<KParticle>();
                _hurtEffect.Visible = true;
                _hurtEffect.Position = new Vector3(-51, 56, -50);
            }
            _hurtEffect.Play(false, false);
        }

        private void HideHurtEffect()
        {
            if (_hurtEffect != null)
            {
                _hurtEffect.Stop();
            }
        }

        private void ShowLineEffect()
        {
            if (_lineEffect == null)
            {
                Transform trans = Instantiate(transform.parent.FindChild("Container_effect/fx_ui_4_2_4_xuecao")) as Transform;
                trans.SetParent(transform, false);
                trans.localScale = Vector3.one;
                _lineEffect = trans.gameObject.AddComponent<KParticle>();
                _lineEffect.Visible = true;
                _lineEffect.Position = new Vector3(-52, 12, -50);
            }
            _lineEffect.Play(true);
        }

        private void HideLineEffect()
        {
            if (_lineEffect != null)
            {
                _lineEffect.Stop();
            }
        }

        protected virtual void ShowUnLockEffect()
        {
            if (_unLockEffect == null)
            {
                Transform trans = Instantiate(transform.parent.FindChild("Container_effect/fx_ui_4_2_3_jiesuo")) as Transform;
                trans.SetParent(transform, false);
                trans.localScale = Vector3.one;
                _unLockEffect = trans.gameObject.AddComponent<KParticle>();
                _unLockEffect.Visible = true;
                _unLockEffect.Position = new Vector3(-52, 55f, -50);
            }
            _unLockEffect.Play();
        }

        private void EndRecover()
        {
            ResetRecoverTimer();
            if (_isRecovering == true)
            {
                ShowRecoverEffect();
            }
            _isRecovering = false;
        }

        public void Show(uint entityId, int partId)
        {
            this.Visible = true;
            _entityId = entityId;
            _partId = partId;
            _statusType = -1;
            _isDestroy = true;
            _isLock = true;
            SetSelect(false);
            ResetEffect();
            SetIcon();
            EndRecover();
            ResetHpTimer();
            _percent = 1;
            _current = 1;
            _ref = 0;
            RefreshName();
        }

        public void Hide()
        {
            this.Visible = false;
            ResetRecoverTimer();
            ResetHpTimer();
        }

        protected virtual void RefreshName()
        {
        }

        private void ResetEffect()
        {
            HideHurtEffect();
            HideLineEffect();
        }
        
        private void SetIcon()
        {
            _iconRadius = _iconContainer.GetComponent<RectTransform>().sizeDelta.y * 0.5f;
            _iconY = _iconContainer.GetComponent<RectTransform>().anchoredPosition.y;
            EntityMonsterBase entityMonster = MogoWorld.GetEntity(_entityId) as EntityMonsterBase;
            PartSubject subject = entityMonster.partManager.PartSubjects[_partId];
            MogoAtlasUtils.AddSprite(_iconContainer.gameObject, icon_item_helper.GetIcon(subject.Data.icons[0]), OnIconLoaded);
            if (subject.Data.icons.Count > 1)
            {
                MogoAtlasUtils.AddSprite(_backIconContainer.gameObject, icon_item_helper.GetIcon(subject.Data.icons[1]));
            }
        }

        private void OnIconLoaded(GameObject go)
        {
            _iconImage = go.GetComponent<Image>();
            _iconImage.type = Image.Type.Filled;
            _iconImage.fillMethod = Image.FillMethod.Vertical;
            _iconImage.fillOrigin = (int)Image.OriginVertical.Bottom;
            _slotImage.CurrentImage.type = Image.Type.Filled;
            _slotImage.CurrentImage.fillMethod = Image.FillMethod.Vertical;
            _slotImage.CurrentImage.fillOrigin = (int)Image.OriginVertical.Bottom;
            EntityMonsterBase entityMonster = MogoWorld.GetEntity(_entityId) as EntityMonsterBase;
            if (entityMonster == null) return;
            PartSubject subject = entityMonster.partManager.PartSubjects[_partId];
            if (subject == null) return;
            SetStatusType(subject.Status);
        }

        protected override void OnEnable()
        {
            _rect.localPosition = _orginalPosition;
            _slotImage.Visible = false;
        }

        protected override void OnDisable()
        {
            ResetRecoverTimer();
            ResetHpTimer();
        }

    }
}
