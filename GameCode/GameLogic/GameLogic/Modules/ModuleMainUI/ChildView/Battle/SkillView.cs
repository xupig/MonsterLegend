﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using Common.Data;
using Common.ClientConfig;
using GameMain.GlobalManager;
using Common.Base;
using GameMain.Entities;
using Common.States;
using GameData;
using MogoEngine.Mgrs;

namespace ModuleMainUI
{
    public class SkillView : KContainer
    {
        private List<SkillButton> _btnSkillList;
        private List<Vector3> _btnLocalPosition;

        protected override void Awake()
        {
            _btnSkillList = new List<SkillButton>();
            _btnLocalPosition = new List<Vector3>();
            _btnSkillList.Add(AddChildComponent<SkillButton>("Container_Skill1"));
            _btnSkillList.Add(AddChildComponent<SkillButton>("Container_Skill2"));
            _btnSkillList.Add(AddChildComponent<SkillButton>("Container_Skill3"));
            _btnSkillList.Add(AddChildComponent<SkillButton>("Container_Skill4"));
            for (int i = 0; i < _btnSkillList.Count; i++)
            {
                _btnSkillList[i].position = i + 1;
            }
            SkillIconPool.InitPool(this.gameObject);
            InitPosition();
        }

        private void InitPosition()
        {
            for (int i = 0; i < 4; i++)
            {
                _btnLocalPosition.Add(_btnSkillList[i].transform.localPosition);
            }
            _btnLocalPosition.Add(new Vector3(-2000, -2000, 0));
            for (int i = 1; i <= 3; i++)
            {
                Vector3 localposition = GetChild(string.Format("Container_SkillPos{0}", i)).transform.localPosition;
                localposition.x += (_btnSkillList[i].transform as RectTransform).sizeDelta.x * 0.5f;
                localposition.y -= (_btnSkillList[i].transform as RectTransform).sizeDelta.y * 0.5f;
                _btnLocalPosition.Add(localposition);
            }
        }

        protected void AddEventListener()
        {
            _btnSkillList[0].onClick.AddListener(OnBtnSkill1Click);
            _btnSkillList[1].onClick.AddListener(OnBtnSkill2Click);
            _btnSkillList[2].onClick.AddListener(OnBtnSkill3Click);
            _btnSkillList[3].onClick.AddListener(OnBtnSkill4Click);
            EventDispatcher.AddEventListener<int, float>(BattleUIEvents.SKILL_COOL_DOWN, SkillCoolDown);
            EventDispatcher.AddEventListener<int, float>(BattleUIEvents.SET_SKILL_EP, SkillEpPercent);
            EventDispatcher.AddEventListener<int, bool>(BattleUIEvents.SET_SKILL_BEFORE_NOTICE, SetSkillButtonBeforeNotice);
            EventDispatcher.AddEventListener<int>(BattleUIEvents.ADD_SKILL, AddSkillNotice);
            EventDispatcher.AddEventListener<int, int>(BattleUIEvents.SET_SKILL_ID, SetSkillId);
            EventDispatcher.AddEventListener<bool>(PlayerEvents.ON_IN_TOUCH_MODE_STATE_CHANGED, OnBattleModeChange);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.control_setting_lol_move, OnSkillShortcutKeyChange);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.control_setting_web_player, OnSkillShortcutKeyChange);
        }

        protected void RemoveEventListener()
        {
            _btnSkillList[0].onClick.RemoveListener(OnBtnSkill1Click);
            _btnSkillList[1].onClick.RemoveListener(OnBtnSkill2Click);
            _btnSkillList[2].onClick.RemoveListener(OnBtnSkill3Click);
            _btnSkillList[3].onClick.RemoveListener(OnBtnSkill4Click);
            EventDispatcher.RemoveEventListener<int, float>(BattleUIEvents.SKILL_COOL_DOWN, SkillCoolDown);
            EventDispatcher.RemoveEventListener<int, float>(BattleUIEvents.SET_SKILL_EP, SkillEpPercent);
            EventDispatcher.RemoveEventListener<int, bool>(BattleUIEvents.SET_SKILL_BEFORE_NOTICE, SetSkillButtonBeforeNotice);
            EventDispatcher.RemoveEventListener<int>(BattleUIEvents.ADD_SKILL, AddSkillNotice);
            EventDispatcher.RemoveEventListener<int, int>(BattleUIEvents.SET_SKILL_ID, SetSkillId);
            EventDispatcher.RemoveEventListener<bool>(PlayerEvents.ON_IN_TOUCH_MODE_STATE_CHANGED, OnBattleModeChange);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.control_setting_lol_move, OnSkillShortcutKeyChange);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.control_setting_web_player, OnSkillShortcutKeyChange);
        }

        private void OnSkillShortcutKeyChange()
        {
            for (int i = 0; i < _btnSkillList.Count; i++)
            {
                _btnSkillList[i].RefreshShortKeyView();
            }
        }

        private void OnBattleModeChange(bool obj)
        {
            RefreshControlStyle();
        }

        private void RefreshControlStyle()
        {
            if (PlayerTouchBattleModeManager.GetInstance().inTouchModeState == false)
            {
                for (int i = 0; i < _btnSkillList.Count; i++)
                {
                    _btnSkillList[i].transform.localPosition = _btnLocalPosition[i];
                }
            }
            else
            {
                for (int i = 0; i < _btnSkillList.Count; i++)
                {
                    _btnSkillList[i].transform.localPosition = _btnLocalPosition[i + 4];
                }
            }
        }

        private void SetSkillButtonBeforeNotice(int position, bool needNotice)
        {
            if (needNotice == true)
            {
                GetSkillButtonByPosition(position).Visible = false;
            }
            else
            {
                GetSkillButtonByPosition(position).Visible = true;
            }
        }

        private void AddSkillNotice(int position)
        {
            SkillNoticeData data = new SkillNoticeData();
            data.Index = position;
            RectTransform target = GetSkillButtonByPosition(position).transform as RectTransform;
            Vector3 localposition = _btnLocalPosition[position - 1];
            localposition.x -= target.sizeDelta.x * 0.5f;
            localposition.y += target.sizeDelta.y * 0.5f;
            if (BaseModule.IsPanelCanvasEnable(PanelIdEnum.MainBattleControl) == false)
            {
                localposition.y += 2000;
            }
            data.targetPosition = transform.TransformPoint(localposition);
            UIManager.Instance.ShowPanel(PanelIdEnum.SkillNotice, data);
        }

        private void SetSkillId(int position, int skillId)
        {
            GetSkillButtonByPosition(position).SetSkillId(skillId);
            if (skill_guide_helper.IsHide(position))
            {
                SetSkillButtonBeforeNotice(position, true);
            }
        }

        private void RefreshSkillId(int position)
        {
            int skillId = skill_play_helper.GetCurrentSkillId(position);
            SetSkillId(position, skillId);
        }

        private void SkillCoolDown(int position, float tick)
        {
            GetSkillButtonByPosition(position).SkillCoolDown(tick);
        }

        private void SkillEpPercent(int position, float percent)
        {
            GetSkillButtonByPosition(position).SetEpPercent(percent);
        }

        private void RefreshSkillView()
        {
            for (int i = 1; i <= 4; i++)
            {
                RefreshSkillId(i);
            }
        }

        private void RefreshSkillBeforeNotice()
        {
            for (int i = 1; i <=4 ; i++)
            {
                if (skill_guide_helper.IsHide(i))
                {
                    SetSkillButtonBeforeNotice(i, true);
                }
                else
                {
                    SetSkillButtonBeforeNotice(i, false);
                }
            }
        }

        private SkillButton GetSkillButtonByPosition(int position)
        {
            return _btnSkillList[position - 1];
        }

        private void OnBtnSkill1Click()
        {
            skill_play_helper.PlaySkill(SkillData.SKILL_POS_ONE);
        }

        private void OnBtnSkill2Click()
        {
            skill_play_helper.PlaySkill(SkillData.SKILL_POS_TWO);
        }

        private void OnBtnSkill3Click()
        {
            skill_play_helper.PlaySkill(SkillData.SKILL_POS_THREE);
        }

        private void OnBtnSkill4Click()
        {
            skill_play_helper.PlaySkill(SkillData.SKILL_POS_FOUR);
        }

        protected override void OnEnable()
        {
            RefreshSkillView();
            RefreshSkillBeforeNotice();
            RefreshControlStyle();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

    }
}
