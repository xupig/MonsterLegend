﻿using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ModuleMainUI
{
    public class SkillButton : KContainer, IPointerDownHandler
    {
        private KContainer _cdContainer;
        private StateImage _lockImage;
        private StateImage _timeImage;
        private KContainer _iconContainer;
        private StateImage _epImage;
        private Locater _cdLocater;
        private RectTransform _cdRect;
        private KParticle _cdRestEffect;
        private KParticle _epFullEffect;
        private KParticle _rotateEffect;
        private SkillIcon _skillIcon;
        private KContainer _shortcutKeyContaner;
        private StateText _shortcutKeyTxt;

        private bool _isLock;
        private float _cdTick;
        private float _totalTick;
        private uint _timerId;
        private int _second;
        private uint _commonCdTimerId;
        private bool _iscommonCd = false;
        private bool _isFullEp = true;
        private int _curSkillID = 0;

        private const int INTERVAL = 20;

        public KComponentEvent onClick = new KComponentEvent();

        private int _position;
        public int position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }

        protected override void Awake()
        {
            _cdContainer = GetChildComponent<KContainer>("Container_tick");
            _lockImage = GetChildComponent<StateImage>("Image_lock");
            _timeImage = GetChildComponent<StateImage>("Image_time");
            _epImage = GetChildComponent<StateImage>("Image_nenglicao");
            _iconContainer = GetChildComponent<KContainer>("Container_icon");
            if (_cdContainer != null)
            {
                _cdLocater = _cdContainer.gameObject.AddComponent<Locater>();
                _cdRect = _cdContainer.gameObject.GetComponent<RectTransform>();
                _cdRect.anchorMin = new Vector2(0.5f, 0.5f);
                _cdRect.anchorMax = new Vector2(0.5f, 0.5f);
                _cdRect.pivot = new Vector2(0.5f, 0.5f);
                _cdLocater.Y = 0;
            }
            InitImageType();
            gameObject.AddComponent<TweenButtonEnlarge>();
            _shortcutKeyContaner = GetChildComponent<KContainer>("Container_kuaijiejian");
            _shortcutKeyContaner.Visible = platform_helper.InPCPlatform() || platform_helper.InEditorPlatform();
            _shortcutKeyTxt = _shortcutKeyContaner.GetChildComponent<StateText>("Label_jianwei");
        }

        public void RefreshShortKeyView()
        {
            if (_position == 0) { return; }
            string shortcutKeyString = KeyboardManager.GetInstance().GetCurrentSkillString();
            int charIndex = (_position - 1) * 2;
            _shortcutKeyTxt.CurrentText.text = shortcutKeyString[charIndex].ToString();
        }

        private void InitImageType()
        {
            _timeImage.CurrentImage.type = Image.Type.Filled;
            _timeImage.CurrentImage.fillMethod = Image.FillMethod.Radial360;
            _timeImage.CurrentImage.fillOrigin = (int)Image.Origin360.Top;
            _timeImage.CurrentImage.fillClockwise = false;
            if (_epImage != null)
            {
                _epImage.CurrentImage.type = Image.Type.Filled;
                _epImage.CurrentImage.fillMethod = Image.FillMethod.Radial360;
                _epImage.CurrentImage.fillOrigin = (int)Image.Origin360.Top;
            }
        }

        private void SetLock(bool isLock)
        {
            if (_lockImage != null)
            {
                _isLock = isLock;
                if (isLock == true)
                {
                    _lockImage.gameObject.SetActive(true);
                }
                else
                {
                    _lockImage.gameObject.SetActive(false);
                }
            }
        }

        public void SetSkillId(int skillId)
        {
            _curSkillID = skillId;
            RefreshSkillButtonVisibility(skillId);
            if (gameObject.activeInHierarchy == false) return;
            if (skillId == -1)
            {
                SetLock(true);
            }
            else
            {
                SetLock(false);
                int iconId = spell_helper.GetIconId(skillId);
                //MogoAtlasUtils.AddIcon(_iconContainer.gameObject, iconId);
                if (_skillIcon != null) { SkillIconPool.pool.ReleaseActorGameObject(_skillIcon); }
                _skillIcon = SkillIconPool.pool.CreateIconGameObject(iconId, _iconContainer.gameObject);
                _skillIcon.transform.SetParent(_iconContainer.transform, false);
                SkillCoolDown(0);
                if (_cdContainer != null)
                {
                    _cdContainer.Visible = false;
                }
                RefreshEnableState();
            }
        }

        private void RefreshSkillButtonVisibility(int skillId)
        {
            if (skillId == 0)
            {
                if (this.Visible == true)
                {
                    this.Visible = false;
                }
            }
            else
            {
                if (this.Visible == false)
                {
                    this.Visible = true;
                }
            }
        }

        private void OnBtnSkillClick()
        {
            onClick.Invoke();
        }

        public void SkillCoolDown(float tick)
        {
            if (gameObject.activeInHierarchy == false) return;
            //小于0为公共CD
            if (tick < 0)
            {
                SetCommonCd(tick);
                return;
            }
            if (tick == 0)
            {
                SkillCDRest();
                return;
            }
            if (tick < 1000) return;
            RefreshCDView(tick);
        }

        private void RefreshCDView(float tick)
        {
            SkillCDRest();
            _cdTick = tick;
            if (tick > _totalTick)
            {
                _totalTick = tick;
            }
            if (_cdContainer != null)
            {
                _cdContainer.Visible = true;
            }
            _timerId = TimerHeap.AddTimer(0, INTERVAL, SkillCDTick);
        }

        private void SkillCDTick()
        {
            _cdTick -= INTERVAL;
            if (_cdTick <= 0)
            {
                ShowCDRestEffect();
                SkillCDRest();
                RefreshEnableState();
                if (_cdContainer != null)
                {
                    _cdContainer.Visible = false;
                }
                return;
            }
            int second = Mathf.CeilToInt(_cdTick / 1000.0f);
            if (_second != second)
            {
                _second = second;
                RefreshCdTick(_second);
            }
            _timeImage.CurrentImage.fillAmount = _cdTick / _totalTick;
        }

        private void SkillCDRest()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
            _cdTick = 0;
            _totalTick = 0;
        }

        private void SetCommonCd(float tick)
        {
            if (_commonCdTimerId != 0)
            {
                TimerHeap.DelTimer(_commonCdTimerId);
            }
            _commonCdTimerId = TimerHeap.AddTimer((uint)-tick, 0, ResetCommonCD);
            _iscommonCd = true;
            RefreshEnableState();
        }


        private void ResetCommonCD()
        {
            if (_commonCdTimerId != 0)
            {
                TimerHeap.DelTimer(_commonCdTimerId);
            }
            _commonCdTimerId = 0;
            _iscommonCd = false;
            RefreshEnableState();
        }

        private void RefreshCdTick(int tick)
        {
            if (_cdContainer != null)
            {
                int digita1 = tick / 10;
                KContainer num1 = _cdContainer.GetChildComponent<KContainer>("Container_sharedxiaoshu00");
                if (digita1 == 0)
                {
                    _cdLocater.X = -12.5f;
                    num1.Visible = false;
                }
                else
                {
                    _cdLocater.X = 0;
                    num1.Visible = true;
                    SetDigitalImage(digita1, num1);
                }

                int digita2 = tick % 10;
                KContainer num2 = _cdContainer.GetChildComponent<KContainer>("Container_sharedxiaoshu01");
                SetDigitalImage(digita2, num2);
            }
        }

        private static void SetDigitalImage(int digita, KContainer numContainer)
        {
            for (int i = 0; i < 10; i++)
            {
                GameObject go = numContainer.GetChild("Image_sharedmiao" + i);
                if (i == digita)
                {
                    go.SetActive(true);
                }
                else
                {
                    go.SetActive(false);
                }
            }
        }

        public void SetEpPercent(float percent)
        {
            if (gameObject.activeInHierarchy == false) return;
            if (_epImage != null)
            {
                _epImage.CurrentImage.fillAmount = percent;
                if (percent < 1)
                {
                    _isFullEp = false;
                    RefreshEnableState();
                }
                else
                {
                    if (_isFullEp == false)
                    {
                        ShowEpFullEffect();
                        _isFullEp = true;
                        RefreshEnableState();
                    }
                }
            }
        }

        private void ShowEpFullEffect()
        {
            if (_position == 1) return;
            if (_isLock) return;
            if (_epFullEffect == null)
            {
                Transform trans = Instantiate(transform.parent.FindChild("Container_effect/fx_ui_4_1_2_nengliangcao_01")) as Transform;
                trans.SetParent(transform, false);
                trans.localPosition = Vector3.zero;
                trans.localScale = Vector3.one;
                _epFullEffect = trans.gameObject.AddComponent<KParticle>();
                _epFullEffect.Visible = true;
                _epFullEffect.Position = new Vector3(-53.5f, 53.5f, -50);
                _epFullEffect.IsSortWhenEnable = false;
            }
            _epFullEffect.Play();
        }

        private void ShowCDRestEffect()
        {
            if (_position == 1) return;
            if (_isLock) return;
            if (_cdRestEffect == null)
            {
                Transform trans = Instantiate(transform.parent.FindChild("Container_effect/fx_ui_4_1_1_CD_01")) as Transform;
                trans.SetParent(transform, false);
                trans.localPosition = Vector3.zero;
                trans.localScale = Vector3.one;
                _cdRestEffect = trans.gameObject.AddComponent<KParticle>();
                _cdRestEffect.Visible = true;
                _cdRestEffect.Position = new Vector3(-53.5f, 53.5f, -50);
                _cdRestEffect.IsSortWhenEnable = false;
            }
            _cdRestEffect.Play();
        }

        private void ShowRotateEffect()
        {
            if (_position == 1) return;
            if (_rotateEffect == null)
            {
                Transform trans = Instantiate(transform.parent.FindChild("Container_effect/fx_ui_4_4_nengliangcao")) as Transform;
                trans.SetParent(transform, false);
                trans.localPosition = Vector3.zero;
                trans.localScale = Vector3.one;
                _rotateEffect = trans.gameObject.AddComponent<KParticle>();
                _rotateEffect.Visible = true;
                _rotateEffect.Position = new Vector3(-53.5f, 53.5f, -50);
                _rotateEffect.IsSortWhenEnable = false;
            }
            _rotateEffect.Play(true);
        }

        private void HideRotateEffect()
        {
            if (_rotateEffect != null)
            {
                _rotateEffect.Stop();
            }
        }

        bool enableState = true;
        private void RefreshEnableState()
        {
            bool state = false;
            if (_iscommonCd == false && _isFullEp == true)
            {
                state = true;
            }
            enableState = state;
            RefreshRotateEffect();
            RefreshImageState();
        }

        private void RefreshRotateEffect()
        {
            if (enableState && spell_helper.GetShowType(_curSkillID) != 0 && spell_helper.GetPos(_curSkillID) > 1)
            {
                ShowRotateEffect();
            }
            else
            {
                HideRotateEffect();
            }
        }

        private void RefreshImageState()
        {
            if (_cdTick > 0) return;
            if (enableState)
            {
                _timeImage.CurrentImage.fillAmount = 0;
            }
            else
            {
                _timeImage.CurrentImage.fillAmount = 1;
            }
        }

        protected override void OnEnable()
        {
            SkillCoolDown(0);
            SetEpPercent(1);
            SetSkillId(_curSkillID);
            if (_cdContainer != null)
            {
                _cdContainer.Visible = false;
            }
            _timeImage.CurrentImage.fillAmount = 0;
            RefreshShortKeyView();
        }

        protected override void OnDisable()
        {
            SkillCDRest();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            EventSystem.current.SetSelectedGameObject(gameObject, eventData);
            onClick.Invoke();
        }
    }
}
