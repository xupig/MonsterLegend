﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using MogoEngine.Mgrs;
using GameMain.Entities;
using Common.ClientConfig;
using GameLoader.Utils.Timer;
using Common.Global;
using GameMain.GlobalManager;
using Common.Utils;
using Game.UI;
using Common.ExtendComponent;

namespace ModuleMainUI
{
    public class LowHpView : KContainer
    {
        private KContainer _imageContainer;
        private List<ImageWrapper> _warningImageList;
        private uint _timerId;
        private bool _isShow = false;
        private float _currentTransparency;
        private float delta;

        private float fadeOut;
        private int lastTime;

        private const int INTERVAL = 20;
        private const float FADE_OUT = 100;
        private const int LAST_TIME = 500;

        protected override void Awake()
        {
            _warningImageList = new List<ImageWrapper>();
            _imageContainer = GetChildComponent<KContainer>("Container_dixueliang");
            for (int i = 0; i < _imageContainer.transform.childCount; i++)
            {
                _warningImageList.Add(GetChildComponent<ImageWrapper>("Container_dixueliang/Static_dishenming" + i));
            }
            MogoGameObjectHelper.AttachFullScreen(gameObject);
            gameObject.AddComponent<RaycastComponent>();
        }

        protected void AddEventListener()
        {
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.cur_hp, ShowWarningImage);
        }

        protected void RemoveEventListener()
        {
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.cur_hp, ShowWarningImage);
        }

        private void ShowWarningImage()
        {
            int cur_hp = PlayerAvatar.Player.cur_hp;
            int max_hp = PlayerAvatar.Player.max_hp;
            float percent = (float)cur_hp / max_hp;
            if (percent <= 0.3)
            {
                if (_timerId == 0)
                {
                    ToggleWarningImage(true);
                    _currentTransparency = fadeOut;
                    SetAlpha(_currentTransparency);
                    _isShow = true;
                    _timerId = TimerHeap.AddTimer(0, INTERVAL, Twinkle);
                }
            }
            else
            {
                if (_timerId != 0)
                {
                    TimerHeap.DelTimer(_timerId);
                    ToggleWarningImage(false);
                    _timerId = 0;
                }
            }
        }

        private void ToggleWarningImage(bool visible)
        {
            _imageContainer.Visible = visible;
        }

        private void Twinkle()
        {
            if(_isShow == true)
            {
                _currentTransparency += delta;
                if (_currentTransparency >= 255)
                {
                    _currentTransparency = 255;
                    _isShow = false;
                }
            }
            else
            {
                _currentTransparency -= delta;
                if (_currentTransparency <= fadeOut)
                {
                    _currentTransparency = fadeOut;
                    _isShow = true;
                }
            }
            SetAlpha(_currentTransparency);
        }

        private void SetAlpha(float value)
        {
            for (int i = 0; i < _warningImageList.Count; i++)
            {
                _warningImageList[i].alpha = value / 255.0f;
            }
        }

        private void InitView()
        {
            fadeOut = FADE_OUT;
            lastTime = LAST_TIME;
            ToggleWarningImage(false);
            delta = (255 - fadeOut) * INTERVAL / lastTime;
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        protected override void OnEnable()
        {
            InitView();
            AddEventListener();
            ShowWarningImage();
        }

        protected override void OnDisable()
        {
            ResetTimer();
            RemoveEventListener();
        }

    }
}
