﻿using Common.Events;
using Common.ServerConfig;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.RPC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using Game.UI.UIComponent;
using Object = UnityEngine.Object;
using Common.Structs.ProtoBuf;
using Common.Data;

namespace ModuleMainUI
{
    public class CreatureInfoManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }
    public class CreatureInfoManager
    {
        static private float SHOW_INFO_DISTANCE = 10f;
        static private int SHOW_INFO_MAX_NUM = 20;
        static private float IDLE_INFO_MAX_TIME = 15;
        static private int IDLE_INFO_MAX_NUM = 10;

        private Stack<CreatureInfoItem> _creatureInfoItemStack;
        private Dictionary<uint, CreatureInfoItem> _creatureInfoDic;
        public bool IsUpdate = true;

        public CreatureInfoManager()
        {
            _creatureInfoItemStack = new Stack<CreatureInfoItem>();
            _creatureInfoDic = new Dictionary<uint, CreatureInfoItem>();

            MogoWorld.RegisterUpdate<CreatureInfoManagerUpdateDelegate>("CreatureInfoManager.Update", Update);
        }

        private static CreatureInfoManager s_Instance;
        public static CreatureInfoManager Instance
        {
            get
            {
                if (s_Instance == null)
                    s_Instance = new CreatureInfoManager();
                return s_Instance;
            }
        }

        int _spaceTime = 0;
        float _spareTime = 0;
        void Update()
        {
            _spareTime += Time.deltaTime;
            if (_spareTime > IDLE_INFO_MAX_TIME || _creatureInfoItemStack.Count >= IDLE_INFO_MAX_NUM)
            {
                DestoryOneItem();
                _spareTime = 0;
            }

            if (_spaceTime > 0) { _spaceTime--; return; }
            _spaceTime = 30;

            //若战斗元素界面隐藏则直接return
            if (IsUpdate == false) return;
            UpdateCreatureInfos();
        }

        void UpdateCreatureInfos()
        {
            if (PlayerAvatar.Player == null) return;
            var playerPos = PlayerAvatar.Player.position;
            List<uint> entityIdList = CreatureDataManager.GetInstance().GetEntityIDList();
            List<uint> outofMonsterIdList = CreatureDataManager.GetInstance().GetOutofEntityIdList();

            for (int j = 0; j < outofMonsterIdList.Count; j++)
            {
                RemoveCreatureInfo(outofMonsterIdList[j]);
            }
            CreatureDataManager.GetInstance().ClearOutofEntityIdList();
            for (int i = 0; i < entityIdList.Count; i++)
            {
                var entityId = entityIdList[i];
                Entity entity = MogoWorld.GetEntity(entityId);
                var entityPos = entity.position;
                entityPos.y = playerPos.y;
                if (entity != null && (playerPos - entityPos).magnitude < SHOW_INFO_DISTANCE)
                {
                    CreateCreatureInfo(entityId);
                }
                else {
                    RemoveCreatureInfo(entityId);
                }
            }
        }

        public void CreateCreatureInfo(uint entityId)
        {
            if (Common.States.GMState.showName == false) return;
            if (_creatureInfoDic.Count >= SHOW_INFO_MAX_NUM) return;
            if (_creatureInfoDic.ContainsKey(entityId) == false)
            {
                CreatureInfoItem creatureInfoItem = CreateCreatureInfoItem(entityId);
                creatureInfoItem.gameObject.name = "CreatureInfoItem_" + entityId;
                _creatureInfoDic.Add(entityId, creatureInfoItem);
                creatureInfoItem.Show(entityId);
            }
        }

        public void RemoveCreatureInfo(uint entityId)
        {
            if (_creatureInfoDic.ContainsKey(entityId) == true)
            {
                CreatureInfoItem creatureInfoItem = _creatureInfoDic[entityId];
                creatureInfoItem.Hide();
                _creatureInfoDic.Remove(entityId);
                _creatureInfoItemStack.Push(creatureInfoItem);
            }
        }

        private void DestoryOneItem()
        {
            if (_creatureInfoItemStack.Count != 0)
            {
                CreatureInfoItem creatureInfoItem = _creatureInfoItemStack.Pop();
                Object.Destroy(creatureInfoItem.gameObject);
                creatureInfoItem = null;
            }
        }

        private CreatureInfoItem CreateCreatureInfoItem(uint entityId)
        {
            CreatureInfoItem result;
            if (_creatureInfoItemStack.Count != 0)
            {
                result = _creatureInfoItemStack.Pop();
            }
            else
            {
                _spareTime = 0;
                result = CreatureInfoList.AddCreatureInfoItem();
            }
            return result;
        }

        private void RemoveAllCreatureInfoItem()
        {
            foreach (KeyValuePair<uint, CreatureInfoItem> kvp in _creatureInfoDic)
            {
                kvp.Value.Hide();
                Object.Destroy(kvp.Value.gameObject);
            }
            _creatureInfoDic.Clear();
            CreatureDataManager.GetInstance().ClearEntityIdList();
        }

        public void LeaveMap()
        {
            RemoveAllCreatureInfoItem();
        }
    }
}
