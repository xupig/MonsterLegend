﻿using Common.ClientConfig;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.CombatSystem;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using MogoEngine.RPC;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace ModuleMainUI
{
    public class CreatureInfoItem : KContainer
    {
        private static Vector3 CHEST_HEAD_POS = new Vector3(0, 2.5f, 0);
        private static Vector3 COMBATPORTAL_HEAD_POS = new Vector3(0, 2.5f, 0);
        private static Vector3 PORTAL_HEAD_POS = new Vector3(0, 3.0f, 0);
        private static Vector2 ART_TITLE_SIZE = new Vector2(191, 48);

        private uint _entityId = 0;
       
        private Vector3 _rectSize;
        private RectTransform _rectTransform;
        private StateText _creatureName;
        private StateText _creatureGuildInfo;
        private StateText _creatureTitle;
        private GameObject _guildIcon;
        private GameObject _titleIcon;
        private KContainer _containerGuild;
        private KContainer _containerTitle;
        private KShrinkableButton _btnStake;
        private Vector3 _lastCameraPos = Vector3.zero;
        private Vector3 _lastEntityPos = Vector3.zero;

        private enum Priority
        {
            guild,
            name,
            title,
            stake,//下注
        }

        private VerticalLocater _locaterRoot;

        private bool _isShowGuildInfo = false;
        private bool _isShowTitleInfo = false;
        private bool _isShowTitleIcon = false;
        private bool _isShowBtnInfo = false;

        protected override void Awake()
        {
            _rectSize = GetComponent<RectTransform>().sizeDelta;
            _rectTransform = this.gameObject.GetComponent<RectTransform>();
            _rectTransform.pivot = Vector2.zero;

            _creatureName = GetChildComponent<StateText>("Label_txtcreatureInfo1");
            _locaterRoot = gameObject.AddComponent<VerticalLocater>();
            AddVerticalLocater(_creatureName.gameObject, Priority.name);
        }

        public void Show(uint entityId)
        {
            _entityId = entityId;
            AddEntityPropertyEventsListener();
            AddEventsListener();
            InitCreatureInfo();
            enabled = true;
        }

        public void Hide()
        {
            RemoveEntityPropertyEventsListener();
            RemoveEventsListener();
            _entityId = 0;
            _rectTransform.localPosition = CreatureInfoList.HIDE_POSITION;
            enabled = false;
        }

        void Update()
        {
			if (Camera.main == null)	return;
            var cameraTransform = CameraManager.GetInstance().CameraTransform;
            if (cameraTransform == null) { return; }
            if (_entityId == 0) return;
            Entity entity = MogoWorld.GetEntity(_entityId);
            if (IsEntityExist(entity) == false) return;
            if (entity is EntityCreature)
            {
                EntityCreature creature = entity as EntityCreature;
                Vector3 pos = creature.position;
                pos.y = MogoWorld.Player.position.y + 2f;
                if (creature.controlActor != null)
                {
                    Transform billBoard = creature.controlActor.boneController.GetBoneByName("slot_billboard");
                    if (IsBillboardExist(billBoard) == true)
                    {
                        pos = billBoard.position;
                    }
                }
                if (_lastCameraPos == Camera.main.transform.position
                    && _lastEntityPos == pos) return;
                _lastEntityPos = pos;
                _lastCameraPos = Camera.main.transform.position;
                UpdateCreatureInfoPos(entity, pos);
            }
            else
            {
                if (_lastCameraPos == Camera.main.transform.position
                    && _lastEntityPos == entity.position) return;
                _lastCameraPos = Camera.main.transform.position;
                _lastEntityPos = entity.position;
                if (entity is EntityChestBase)
                {
                    UpdateCreatureInfoPos(entity, _lastEntityPos + CHEST_HEAD_POS);
                }
                else if(entity is EntityCombatPortal)
                {
                    UpdateCreatureInfoPos(entity, _lastEntityPos + COMBATPORTAL_HEAD_POS);
                }
                else if (entity is EntityPortal)
                {
                    UpdateCreatureInfoPos(entity, _lastEntityPos + PORTAL_HEAD_POS);
                }
            }
        }

        private void UpdateCreatureInfoPos(Entity entity, Vector3 position)
        {
            Vector2 screenPosition = Camera.main.WorldToScreenPoint(position);
            if (MogoGameObjectHelper.CheckPositionIsInScreen(screenPosition) == true)
            {
                CalculatePosition(screenPosition);
            }
            else
            {
                _rectTransform.localPosition = CreatureInfoList.HIDE_POSITION;
            }
        }

        /// <summary>
        /// 实体属性相关
        /// </summary>
        private void AddEntityPropertyEventsListener()
        {
            Entity entity = MogoWorld.GetEntity(_entityId);
            if (entity != null && entity is EntityAvatar)
            {
                EntityPropertyManager.Instance.AddEventListener(entity as EntityAvatar, EntityPropertyDefine.guild_id, RefreshGuildInfoAndLayout);
                EntityPropertyManager.Instance.AddEventListener(entity as EntityAvatar, EntityPropertyDefine.level, InitCreatureInfo);
                EntityPropertyManager.Instance.AddEventListener(entity as EntityAvatar, EntityPropertyDefine.name, InitCreatureInfo);
                EntityPropertyManager.Instance.AddEventListener(entity as EntityAvatar, EntityPropertyDefine.title, RefreshTitleInfoAndLayout);
            }
            if (entity != null && entity is EntityCombatPortal)
            {
                EntityPropertyManager.Instance.AddEventListener(entity, EntityPropertyDefine.enter_count, RefreshCombatPortalInfo);
            }
        }

        /// <summary>
        /// 如果执行此函数时，实体已经销毁，不用担心事件没有删除，在实体销毁时会将其所有属性的监听事件删除
        /// </summary>
        private void RemoveEntityPropertyEventsListener()
        {
            Entity entity = MogoWorld.GetEntity(_entityId);
            if (entity != null && entity is EntityAvatar)
            {
                ///销毁实体时会把监听移除
                EntityPropertyManager.Instance.RemoveEventListener(entity as EntityAvatar, EntityPropertyDefine.guild_id, RefreshGuildInfoAndLayout);
                EntityPropertyManager.Instance.RemoveEventListener(entity as EntityAvatar, EntityPropertyDefine.level, InitCreatureInfo);
                EntityPropertyManager.Instance.RemoveEventListener(entity as EntityAvatar, EntityPropertyDefine.name, InitCreatureInfo);
                EntityPropertyManager.Instance.RemoveEventListener(entity as EntityAvatar, EntityPropertyDefine.title, RefreshTitleInfoAndLayout);
            }
            if (entity != null && entity is EntityCombatPortal)
            {
                EntityPropertyManager.Instance.RemoveEventListener(entity, EntityPropertyDefine.enter_count, RefreshCombatPortalInfo);
            }
        }

        private void AddEventsListener()
        {
            EventDispatcher.AddEventListener<ulong>(GuildEvents.Refresh_Guild_Simple_Info, RefreshGuildInfo);
            EventDispatcher.AddEventListener<uint>(EntityEvents.ON_DUEL_STAKE, RefreshDuelBtnAndLayout);
            EventDispatcher.AddEventListener<uint>(EntityEvents.ON_DUEL_FIGHT, RefreshDuelBuff);
        }

        private void RemoveEventsListener()
        {
            EventDispatcher.RemoveEventListener<ulong>(GuildEvents.Refresh_Guild_Simple_Info, RefreshGuildInfo);
            EventDispatcher.RemoveEventListener<uint>(EntityEvents.ON_DUEL_STAKE, RefreshDuelBtnAndLayout);
            EventDispatcher.RemoveEventListener<uint>(EntityEvents.ON_DUEL_FIGHT, RefreshDuelBuff);
        }

        private void RefreshGuildInfoAndLayout()
        {
            RefreshGuildInfo();
            RemoveFreeGameObject();
            RefreshLayout();
        }

        private void RefreshTitleInfoAndLayout()
        {
            RefreshTitleInfo();
            RemoveFreeGameObject();
            RefreshLayout();
        }

        private void RefreshDuelBtnAndLayout(uint entityId)
        {
            if (_entityId == entityId)
            {
                RefreshDuelBtn();
                RemoveFreeGameObject();
                RefreshLayout();
            }
        }

        private void RefreshDuelBuff(uint entityId)
        {
            if (_entityId == entityId)
            {
                EntityAgent entityAgent = MogoWorld.GetEntity(_entityId) as EntityAgent;
                if (entityAgent != null)
                {
                    if (entityAgent.stateManager.InState(state_config.AVATAR_STATE_DUEL_FIGHT))
                    {
                        entityAgent.bufferManager.AddBuffer(4957);
                    }
                    else
                    {
                        entityAgent.bufferManager.AddBuffer(4957, -2);
                    }
                }
            }
        }

        private void OnStake()
        {
            EntityAgent entityAgent = MogoWorld.GetEntity(_entityId) as EntityAgent;
            if (entityAgent != null)
            {
                DuelManager.Instance.RequestStakeInfo(entityAgent.name);
            }
            else
            {
                EntityAvatar entityAvatar = MogoWorld.GetEntity(_entityId) as EntityAvatar;
                DuelManager.Instance.RequestStakeInfo(entityAvatar.name);
            }
        }

        private void CalculatePosition(Vector3 screenPosition)
        {
            Vector3 localPosition = screenPosition / Global.Scale;
            localPosition.y -= Screen.height / Global.Scale;
            localPosition.x -= 0.5f * _rectSize.x;
            localPosition.y += 0.5f * _rectSize.y;
            _rectTransform.localPosition = localPosition;
        }

        private void InitCreatureInfo()
        {
            _isShowGuildInfo = false;
            _isShowTitleInfo = false;
            _isShowTitleIcon = false;
            _isShowBtnInfo = false;
            Entity entity = MogoWorld.GetEntity(_entityId);
            if (IsEntityExist(entity) == false) return;
            if (entity is EntityMonsterBase)
            {
                ShowMonsterInfo();
            }
            else if (entity is EntityNPC)
            {
                ShowNPCInfo();
            }
            else if (entity is PlayerAvatar)
            {
                ShowPlayAvatarInfo();
            }
            else if ((entity as EntityAvatar) != null)
            {
                ShowEntityAvatarInfo();
            }
            else if (entity is EntityPet)
            {
                ShowEntityPet();
            }
            else
            {
                ShowEntityOther();
            }
            RemoveFreeGameObject();
            RefreshLayout();
        }

        private void ShowMonsterInfo()
        {
            EntityMonsterBase entityMonster = MogoWorld.GetEntity(_entityId) as EntityMonsterBase;
            _creatureName.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_RED);
            _creatureName.CurrentText.text = monster_helper.GetMonsterName((int)entityMonster.monster_id);
        }

        private void ShowNPCInfo()
        {
            EntityNPC entityNPC = MogoWorld.GetEntity(_entityId) as EntityNPC;
            _creatureName.CurrentText.color = ColorDefine.GetColorById(ColorDefine.ENTITY_NPC_INFO_COLOR);
            _creatureName.CurrentText.text = npc_helper.GetNpcName(entityNPC.npc_id);

            if (npc_helper.GetNpc(entityNPC.npc_id).__function_honor != 0)
            {
                _isShowTitleInfo = true;
                CreateTitleBoardGameObject();
                _isShowTitleIcon = false;
                _creatureTitle.CurrentText.text = npc_helper.GetNpcFunctionHonor(entityNPC.npc_id);
            }
            else
            {
                _isShowTitleIcon = false;
                _isShowTitleInfo = false;
            }
        }

        private void ShowEntityAvatarInfo()
        {
            EntityAvatar entityAvatar = MogoWorld.GetEntity(_entityId) as EntityAvatar;
            if (TargetFilter.IsEnemy(PlayerAvatar.Player, entityAvatar as EntityCreature) && GameSceneManager.GetInstance().inCombatScene)
            {
                _creatureName.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_RED);
            }
            else
            {
                _creatureName.CurrentText.color = ColorDefine.GetColorById(ColorDefine.ENTITY_AVATAR_INFO_COLOR);
            }
            if (entityAvatar is EntityPuppet && GameSceneManager.GetInstance().chapterType == ChapterType.GuildWarGroup)
            {
                ///{0}的影像
                _creatureName.CurrentText.text = MogoLanguageUtil.GetContent(111650,entityAvatar.name);
            }
            else
            {
                _creatureName.CurrentText.text = entityAvatar.name;
            }


            EntityAgent entityAgent = MogoWorld.GetEntity(_entityId) as EntityAgent;
            if (entityAgent != null)
            {
                GuildDataManager.GetInstance().RequestGuildDataById(entityAgent.guild_id);
            }
            else if (entityAvatar.guild_id != 0)
            {
                GuildDataManager.GetInstance().RequestGuildDataById(entityAvatar.guild_id);
            }

            RefreshDuelBtn();
            RefreshTitleInfo();
        }

        private void ShowPlayAvatarInfo()
        {
            EntityAvatar playerAvatar = MogoWorld.GetEntity(_entityId) as EntityAvatar;
            _creatureName.CurrentText.color = ColorDefine.GetColorById(ColorDefine.PLAYER_AVATAR_INFO_COLOR);
            _creatureName.CurrentText.text = playerAvatar.name;

            if (playerAvatar.guild_id != 0)
            {
                GuildDataManager.GetInstance().RequestGuildDataById(playerAvatar.guild_id);
            }
            RefreshDuelBtn();
            RefreshTitleInfo();
        }

        private void RefreshGuildInfo()
        {
            EntityCreature entity = MogoWorld.GetEntity(_entityId) as EntityCreature;
            if (entity is EntityAvatar && (entity as EntityAvatar).guild_id != 0)
            {
                GuildDataManager.GetInstance().RequestGuildDataById((entity as EntityAvatar).guild_id);
            }
            else
            {
                _isShowGuildInfo = false;
                RemoveFreeGameObject();
                RefreshLayout();
            }
        }

        private void RefreshTitleInfo()
        {
            EntityAvatar entityAvatar = MogoWorld.GetEntity(_entityId) as EntityAvatar;
            if (entityAvatar == null) return;
            _isShowTitleIcon = false;
            _isShowTitleInfo = false;
            if (entityAvatar.title != 0)
            {
                int titleId = Convert.ToInt32(entityAvatar.title);
                
                int titleIconId = title_helper.GetTitleIcon(titleId);
                if (titleIconId != 0)
                {
                    _isShowTitleIcon = true;
                    CreateTitleIconGameObject();
                    MogoAtlasUtils.AddIcon(_titleIcon, titleIconId, IconLoadCallback);
                }
                else
                {
                    _isShowTitleInfo = true;
                    CreateTitleBoardGameObject();
                    _creatureTitle.CurrentText.color = title_helper.GetTitleColor(titleId);
                    _creatureTitle.CurrentText.text = title_helper.GetTitleName(titleId);
                }

            }
        }

        private void RefreshDuelBtn()
        {
            EntityAgent entityAgent = MogoWorld.GetEntity(_entityId) as EntityAgent;
            if (entityAgent != null)
            {
                if (entityAgent.stateManager.InState(state_config.AVATAR_STATE_DUEL_BET))
                {
                    _isShowBtnInfo = true;
                    CreateButtonGameObject();
                }
                else
                {
                    _isShowBtnInfo = false;
                }
            }
            else
            {
                EntityAvatar entityAvatar = MogoWorld.GetEntity(_entityId) as EntityAvatar;
                if (entityAvatar.stateManager.InState(state_config.AVATAR_STATE_DUEL_BET))
                {
                    _isShowBtnInfo = true;
                    CreateButtonGameObject();
                }
                else
                {
                    _isShowBtnInfo = false;
                }
            }
        }

        private void ShowEntityPet()
        {
            EntityPet entityPet = MogoWorld.GetEntity(_entityId) as EntityPet;
            _creatureName.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_WHITE);
            _creatureName.CurrentText.text = pet_helper.GetName(entityPet.pet_id);
        }

        private void ShowEntityOther()
        {
            Entity entity = MogoWorld.GetEntity(_entityId) as Entity;
            if (entity is EntityChestBase)
            {
                RefreshChestName();
            }
            else if (entity is EntityCombatPortal)
            {
                RefreshCombatPortalInfo();
            }
            else if (entity is EntityPortal)
            {
                RefreshPortalName();
            }
        }

        private void RefreshChestName()
        {
            Entity entity = MogoWorld.GetEntity(_entityId) as Entity;
            EntityChestBase chest = entity as EntityChestBase;
            _creatureName.CurrentText.text = chest_data_helper.GetChestName(chest.chest_id);
        }

        private void RefreshPortalName()
        {
            Entity entity = MogoWorld.GetEntity(_entityId) as Entity;
            EntityPortal portal = entity as EntityPortal;
            _creatureName.CurrentText.text = instance_helper.GetInstanceName((int)portal.mission_id);
        }

        private void RefreshCombatPortalInfo()
        {
            Entity entity = MogoWorld.GetEntity(_entityId) as Entity;
            EntityCombatPortal portal = entity as EntityCombatPortal;
            string name = transmit_portal_helper.GetPortalName(portal.portal_id);
            instance ins = instance_helper.GetInstanceCfg((int)portal.inst_id);
            if (ins != null)
            {
                int maxEnterCount = instance_helper.GetMaxPlayerNum(ins);
                name = string.Concat(name, "\n", portal.enter_count < maxEnterCount ? MogoLanguageUtil.GetContent(102411) : MogoLanguageUtil.GetContent(102412));
            }
            _creatureName.CurrentText.text = name;
        }

        public void RefreshGuildInfo(ulong guildId)
        {
            EntityAvatar entityAvatar = MogoWorld.GetEntity(_entityId) as EntityAvatar;
            if(guildId == 0 || entityAvatar == null)
            {
                return;
            }
            if (entityAvatar.guild_id != guildId) return;
            _isShowGuildInfo = true;
            CreateGuildBoardGameObject();

            PbGuildSimpleInfo guildSimpleInfo = GuildDataManager.GetInstance().GetGuildSimpleInfo(guildId);
            FillExtendInfo(guildSimpleInfo);

            RefreshLayout();
        }

        private void FillExtendInfo(PbGuildSimpleInfo guildInfo)
        {
            List<int> iconList = MogoStringUtils.Convert2List(global_params_helper.GetGlobalParam(87));
            MogoAtlasUtils.AddIcon(_guildIcon, iconList[(int)guildInfo.badge_type], IconLoadCallback);
            EntityCreature entity = MogoWorld.GetEntity(_entityId) as EntityCreature;
            Dictionary<ulong, PbHpMember> enemyMemberDic = PlayerDataManager.Instance.InstanceAvatarData.HpEnemyMemberDic;

            _creatureGuildInfo.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_GUILD_NAME);
            _creatureGuildInfo.CurrentText.text = MogoProtoUtils.ParseByteArrToString(guildInfo.guild_name_bytes);
        }

        private void IconLoadCallback(GameObject obj)
        {
            RefreshLayout();
        }

        private void CreateGuildBoardGameObject()
        {
            if (_containerGuild == null)
            {
                _containerGuild = CreatureInfoList.CloneGameObject<KContainer>("Container_container", gameObject);
                AddVerticalLocater(_containerGuild.gameObject, Priority.guild);
            }
            if (_creatureGuildInfo == null)
            {
                _creatureGuildInfo = CreatureInfoList.CloneGameObject<StateText>("Label_txtcreatureInfo2", _containerGuild.gameObject);
            }
            if (_guildIcon == null)
            {
                _guildIcon = CreatureInfoList.CloneGameObject("Container_icon", _containerGuild.gameObject);
            }
        }

        private void CreateTitleBoardGameObject()
        {
            if (_containerTitle == null)
            {
                _containerTitle = CreatureInfoList.CloneGameObject<KContainer>("Container_container", gameObject);
                AddVerticalLocater(_containerTitle.gameObject, Priority.title);
            }
            if (_creatureTitle == null)
            {
                _creatureTitle = CreatureInfoList.CloneGameObject<StateText>("Label_txtcreatureInfo2", _containerTitle.gameObject);
            }
        }

        private void CreateTitleIconGameObject()
        {
            if (_containerTitle == null)
            {
                _containerTitle = CreatureInfoList.CloneGameObject<KContainer>("Container_container", gameObject);
                AddVerticalLocater(_containerTitle.gameObject, Priority.title);
            }

            if (_titleIcon == null)
            {
                _titleIcon = CreatureInfoList.CloneGameObject("Container_icon", _containerTitle.gameObject);
                _titleIcon.GetComponent<RectTransform>().sizeDelta = ART_TITLE_SIZE;
            }
        }

        private void CreateButtonGameObject()
        {
            if (_btnStake == null)
            {
                _btnStake = CreatureInfoList.CloneGameObject("Button_xiazhu", gameObject).GetComponent<KShrinkableButton>();
                _btnStake.onClick.AddListener(OnStake);
                AddVerticalLocater(_btnStake.gameObject, Priority.stake);
            }
        }

        private void RemoveFreeGameObject()
        {
            if (_isShowGuildInfo == false)
            {
                if (_containerGuild != null)
                {
                    _locaterRoot.Remove(_containerGuild.GetComponent<VerticalLocater>());
                    _containerGuild.Visible = false; //重新计算大小，不将其计算在内
                    Destroy(_containerGuild.gameObject);
                    _containerGuild = null;
                }
                if (_creatureGuildInfo != null)
                {
                    _creatureGuildInfo.Visible = false;
                    Destroy(_creatureGuildInfo.gameObject);
                    _creatureGuildInfo = null;
                }
                if (_guildIcon != null)
                {
                    _guildIcon.SetActive(false);
                    MogoAtlasUtils.RemoveSprite(_guildIcon);
                    Destroy(_guildIcon);
                    _guildIcon = null;
                }
            }

            if (_isShowBtnInfo == false)
            {
                if (_btnStake != null)
                {
                    _locaterRoot.Remove(_btnStake.GetComponent<VerticalLocater>());
                    _btnStake.onClick.RemoveListener(OnStake);
                    _btnStake.Visible = false;
                    Destroy(_btnStake.gameObject);
                    _btnStake = null;
                }
            }

            if(_isShowTitleInfo == false)
            {
                if (_creatureTitle != null)
                {
                    _creatureTitle.Visible = false;
                    Destroy(_creatureTitle.gameObject);
                    _creatureTitle = null;
                }
            }

            if (_isShowTitleIcon == false)
            {
                if (_titleIcon != null)
                {
                    _titleIcon.SetActive(false);
                    Destroy(_titleIcon);
                    _titleIcon = null;
                }
            }

            if (_isShowTitleInfo == false && _isShowTitleIcon == false)
            {
                if (_containerTitle != null)
                {
                    _locaterRoot.Remove(_containerTitle.GetComponent<VerticalLocater>());
                    _containerTitle.Visible = false;
                    Destroy(_containerTitle.gameObject);
                    _containerTitle = null;
                }
            }
        }

        private void RefreshLayout()
        {
            AdjustPositionBeforeLayout();
            _locaterRoot.Sort();
            _locaterRoot.Locate(_rectTransform.localPosition.y);
            AdjustPositionAfterLayout();
        }

        private void AdjustPositionBeforeLayout()
        {
            if (_containerGuild != null)
            {
                Text text = _creatureGuildInfo.CurrentText;
                MogoGameObjectHelper.RecalculateTextSize(_creatureGuildInfo.CurrentText);
                Transform tranform = text.transform;
                tranform.localPosition = new Vector3(tranform.localPosition.x, -6);
                MogoLayoutUtils.ImageTextMixHorizontalLayout(_guildIcon.GetComponent<KContainer>(), _creatureGuildInfo);
            }

            if (_creatureTitle != null)
            {
                Text text = _creatureTitle.CurrentText;
                MogoGameObjectHelper.RecalculateTextSize(_creatureTitle.CurrentText);
                Transform tranform = text.transform;
                tranform.localPosition = new Vector3(tranform.localPosition.x, -6);
            }
        }

        private void AdjustPositionAfterLayout()
        {
            if (_containerGuild != null)
            {
                _containerGuild.RecalculateSize();
                MogoLayoutUtils.SetCenter(_containerGuild.gameObject, 2);
            }

            if (_containerTitle != null)
            {
                _containerTitle.RecalculateSize();
                MogoLayoutUtils.SetCenter(_containerTitle.gameObject, 2);
            }

            if (_btnStake != null)
            {
                MogoLayoutUtils.SetCenter(_btnStake.gameObject);
                _btnStake.RefreshRectTransform();
            }
        }

        private void AddVerticalLocater(GameObject gameObject, Priority priority)
        {
            VerticalLocater locater = gameObject.AddComponent<VerticalLocater>();
            locater.Priority = (int)priority;
            _locaterRoot.AddLocater(locater);
        }

        private bool IsEntityExist(Entity entity)
        {
            if (entity == null)
            {
                CreatureInfoManager.Instance.RemoveCreatureInfo(_entityId);
                return false;
            }
            return true;
        }

        private bool IsBillboardExist(Transform billboard)
        {
            if (billboard == null)
            {
                CreatureInfoManager.Instance.RemoveCreatureInfo(_entityId);
                return false;
            }
            return true;
        }
    }
}
