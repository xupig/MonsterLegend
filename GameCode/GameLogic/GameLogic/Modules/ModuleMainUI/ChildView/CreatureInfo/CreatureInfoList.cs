﻿using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    public class CreatureInfoList:KContainer
    {
        private static GameObject _creatureInfoItem;
        private static GameObject _template;

        public static readonly Vector3 HIDE_POSITION = new Vector3(-1000, -1600, 0);

        private static Dictionary<string, GameObject> _goDict;

        protected override void Awake()
        {
            _template = GetChild("Container_template");
            _goDict = new Dictionary<string, GameObject>();
            AddIntoDict("Container_icon", true);
            AddIntoDict("Label_txtcreatureInfo2", true);
            AddIntoDict("Button_xiazhu", false);
            AddIntoDict("Container_container", true);
            _creatureInfoItem = GetChild("Container_creatureInforItem");
            _creatureInfoItem.SetActive(false);
            _creatureInfoItem.transform.localPosition = HIDE_POSITION;
        }

        private void AddIntoDict(string name, bool addRaycast)
        {
            GameObject go = GetChild("Container_template/" + name);
            _goDict.Add(name, go);
            if (addRaycast)
            {
                go.AddComponent<RaycastComponent>();
            }
            go.SetActive(false);
        }

        public static CreatureInfoItem AddCreatureInfoItem()
        {
            CreatureInfoItem item = MogoGameObjectHelper.AddByTemplate<CreatureInfoItem>(_creatureInfoItem);
            item.Visible = true;
            return item;
        }

        public static GameObject CloneGameObject(string name, GameObject parent)
        {
            GameObject go = Instantiate(_goDict[name]) as GameObject;
            go.SetActive(true);
            go.transform.SetParent(parent.transform);
            go.transform.localScale = Vector3.one;
            go.transform.localPosition = Vector3.zero;
            return go;
        }

        public static T CloneGameObject<T>(string name, GameObject parent) where T : Component
        {
            GameObject go = Instantiate(_goDict[name]) as GameObject;
            go.SetActive(true);
            go.transform.SetParent(parent.transform);
            go.transform.localScale = Vector3.one;
            go.transform.localPosition = Vector3.zero;
            return go.GetComponent<T>();
        }
    }
}
