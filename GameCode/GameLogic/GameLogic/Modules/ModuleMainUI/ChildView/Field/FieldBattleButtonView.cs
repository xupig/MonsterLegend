﻿using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.States;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using ModuleSmallMap;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ModuleMainUI
{
    public class FieldBattleButtonView : KContainer
    {
       // private KButton _worldMapBtn;
       // private KButton _changeLineBtn;
       // private KButton _transferPointBtn;
        private int cameraRotation = 135;
        private AutoBattleContainer _battleContainer;

        private Mask _mask;
        private SmallMapElementView _elementView;
        private RectTransform _rect;

        private KDummyButton _btnButton;

        private StateText _txtName;
        private StateText _txtPos;
        private StateText _txtLine;
        private string posTemplate;
        private string lineTemplate;

        private int _curMap = -1;

        private int _openAutoFightTakId;

        protected override void Awake()
        {
            base.Awake();
            _battleContainer = AddChildComponent<AutoBattleContainer>("Button_zhandou");
            KContainer _smallMap = GetChildComponent<KContainer>("Container_xiaoditu");
            _mask = _smallMap.GetChildComponent<Mask>("Container_hechengbaoshi/ScaleImage_mask");
            GameObject content = _smallMap.transform.FindChild("Container_hechengbaoshi/Container_content").gameObject;
            content.transform.SetParent(_mask.transform);
            
            _elementView = content.AddComponent<SmallMapElementView>();
            _elementView.HideTemplate();
            _rect = _elementView.GetComponent<RectTransform>();
            _rect.anchoredPosition = Vector2.zero;
            _btnButton = _mask.gameObject.AddComponent<KDummyButton>();
            bound = _btnButton.GetComponent<RectTransform>().sizeDelta;

            _txtName = _smallMap.GetChildComponent<StateText>("Label_txtditumingcheng");
            _txtLine = _smallMap.GetChildComponent<StateText>("Label_txtXian");
            lineTemplate = _txtLine.CurrentText.text;
            _txtPos = _smallMap.GetChildComponent<StateText>("Label_txtZuobiao");
            posTemplate = _txtPos.CurrentText.text;

            if (platform_helper.InPCPlatform())
            {
                _openAutoFightTakId = int.Parse(global_params_helper.GetGlobalParam(247).Split(',')[0]);
            }
            else
            {
                _openAutoFightTakId = int.Parse(global_params_helper.GetGlobalParam(127).Split(',')[0]);
            }
        }

        protected override void OnDisable()
        {
            leftBottomPos = new Vector2(-1, -1);
            base.OnDestroy();
            RemoveListener();
            count = 0;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            ResetMapInfo();
            RefreshBattleButtonView();
            AddListener();
        }

        public void UpdateContent()
        {
            _txtLine.CurrentText.text = string.Format(lineTemplate, PlayerAvatar.Player.curLine);
            if(_curMap!=GameSceneManager.GetInstance().curMapID)
            {
                _curMap = GameSceneManager.GetInstance().curMapID;
                int instanceId = map_helper.GetInstanceIDByMapID(GameSceneManager.GetInstance().curMapID);
                instance instance = instance_helper.GetInstanceCfg(instanceId);
                map map = map_helper.GetMap(GameSceneManager.GetInstance().curMapID);
                _txtName.CurrentText.text = instance_helper.GetInstanceName(instanceId);
                _elementView.UpdateContent(map);
                minimap minimap = minimap_helper.GetMinimapByMapId(map.__space_name);
                leftBottomPos = ConvertToPos(minimap.__left_bottom_pos);
                rightTopPos = ConvertToPos(minimap.__right_top_pos);
                float mainAngle = cameraRotation;
                float centerX = (leftBottomPos.x + rightTopPos.x) / 2;
                float centerY = (leftBottomPos.y + rightTopPos.y) / 2;
                float radius = Mathf.Sqrt(Mathf.Pow(centerX - leftBottomPos.x, 2) + Mathf.Pow(centerY - leftBottomPos.y, 2));
                float angle = Mathf.Atan2(leftBottomPos.y - centerY, leftBottomPos.x - centerX);
                float angle2 = Mathf.Atan2(rightTopPos.y - centerY, rightTopPos.x - centerX);
                leftBottomPos.x = radius * Mathf.Cos(angle + mainAngle * Mathf.PI / 180.0f) + centerX;
                leftBottomPos.y = radius * Mathf.Sin(angle + mainAngle * Mathf.PI / 180.0f) + centerY;
                rightTopPos.x = radius * Mathf.Cos(angle2 + mainAngle * Mathf.PI / 180.0f) + centerX;
                rightTopPos.y = radius * Mathf.Sin(angle2 + mainAngle * Mathf.PI / 180.0f) + centerY;
            }
            UpdateMePos();
        }

        private void ResetMapInfo()
        {
            if(_curMap==-1)
            {
                return;
            }
            map map = map_helper.GetMap(_curMap);
            minimap minimap = minimap_helper.GetMinimapByMapId(map.__space_name);
            leftBottomPos = ConvertToPos(minimap.__left_bottom_pos);
            rightTopPos = ConvertToPos(minimap.__right_top_pos);
            float mainAngle = cameraRotation;
            float centerX = (leftBottomPos.x + rightTopPos.x) / 2;
            float centerY = (leftBottomPos.y + rightTopPos.y) / 2;
            float radius = Mathf.Sqrt(Mathf.Pow(centerX - leftBottomPos.x, 2) + Mathf.Pow(centerY - leftBottomPos.y, 2));
            float angle = Mathf.Atan2(leftBottomPos.y - centerY, leftBottomPos.x - centerX);
            float angle2 = Mathf.Atan2(rightTopPos.y - centerY, rightTopPos.x - centerX);
            leftBottomPos.x = radius * Mathf.Cos(angle + mainAngle * Mathf.PI / 180.0f) + centerX;
            leftBottomPos.y = radius * Mathf.Sin(angle + mainAngle * Mathf.PI / 180.0f) + centerY;
            rightTopPos.x = radius * Mathf.Cos(angle2 + mainAngle * Mathf.PI / 180.0f) + centerX;
            rightTopPos.y = radius * Mathf.Sin(angle2 + mainAngle * Mathf.PI / 180.0f) + centerY;
        }

        private Vector2 ConvertToPos(List<string> posList)
        {
            Vector2 pos = new Vector2();
            if (posList != null && posList.Count >= 2)
            {
                pos.x = float.Parse(posList[0]);
                pos.y = float.Parse(posList[1]);
            }
            return pos;
        }

        private void RefreshBattleButtonView()
        {
            int mapId = GameSceneManager.GetInstance().curMapID;
            _battleContainer.Visible = map_helper.GetPlayerAI(mapId) != 0;
            bool isOpenAutoFight = PlayerDataManager.Instance.TaskData.IsCompleted(_openAutoFightTakId);
            if (isOpenAutoFight == false)
            {
                _battleContainer.Visible = false;
            }
        }

        private void AddListener()
        {
            _btnButton.onClick.AddListener(OnClickShowSmallMap);
            EventDispatcher.AddEventListener<int>(TaskEvent.TASK_FINISH, OnTaskComplete);
            EventDispatcher.AddEventListener(MainUIEvents.MINI_MAP_GATE_CHANGE, OnGateChange);
        }

        private void RemoveListener()
        {
            _btnButton.onClick.RemoveListener(OnClickShowSmallMap);
            EventDispatcher.RemoveEventListener<int>(TaskEvent.TASK_FINISH, OnTaskComplete);
            EventDispatcher.RemoveEventListener(MainUIEvents.MINI_MAP_GATE_CHANGE, OnGateChange);
        }

        private void OnGateChange()
        {
            _elementView.OnUpdateMiniInfo();
        }

        private void OnTaskComplete(int taskId)
        {
            RefreshBattleButtonView();
        }

        private void OnClickShowSmallMap()
        {
            PanelIdEnum.SmallMap.Show();
        }

        private void OnShowWorldMap()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.FieldWorldMap);
        }

        private void OnShowChangeLine()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.FieldChangeLine);
        }

        private void OnComeTransferPoint()
        {
            if (PlayerAutoFightManager.GetInstance().fightType == FightType.FOLLOW_FIGHT) return;
            List<Vector3> posList = wild_helper.GetTransferPoint(GameSceneManager.GetInstance().curSpaceName);
            int count = posList.Count;

            Vector3 latestPos = posList[0];
            for (int i = 1; i < count; i++)
            {
                if ((posList[i] - PlayerAvatar.Player.position).magnitude < (latestPos - PlayerAvatar.Player.position).magnitude)
                {
                    latestPos = posList[i];
                }
            }
            EventDispatcher.TriggerEvent<Vector3>(PlayerCommandEvents.FIND_TRANSFER_POINT, latestPos);
        }

        

        private int count = 0;
        void Update()
        {
            count++;
            if (count == 5)
            {
               UpdateMePos();
                count = 0;
            }
        }

        private int oldPosX = -1;
        private int oldPosY = -1;

        private void UpdateMePos()
        {
            _elementView.UpdateMe();
            _elementView.UpdateBoss();
            int newPosX = Mathf.RoundToInt(PlayerAvatar.Player.position.x);
            int newPosY = Mathf.RoundToInt(PlayerAvatar.Player.position.z);
            if(oldPosX != newPosX || oldPosY != newPosY)
            {
                oldPosX = newPosX;
                oldPosY = newPosY;
                _txtPos.CurrentText.text = string.Format(posTemplate, newPosX, newPosY);
            }
            
            UpdateRect();
        }

        private Vector2 vect = new Vector2();
        private Vector2 leftBottomPos = new Vector2(-1,-1);
        private Vector2 rightTopPos;
        private Vector2 bound;

        private void UpdateRect()
        {
            //Debug.LogError("CameraManager.GetInstance().mainCamera.rotationY = " + PlayerAvatar.Player.position.x + "," + PlayerAvatar.Player.position.z + "," + CameraManager.GetInstance().mainCamera.rotationY + "," + leftBottomPos.x + "," + leftBottomPos.y + "," + rightTopPos.x + "," + rightTopPos.y + "," + bound.x + "," + bound.y);
            if(leftBottomPos.x == -1 && leftBottomPos.y == -1)
            {
                return;
            }
            vect.x = (PlayerAvatar.Player.position.x - leftBottomPos.x) * 488 / (rightTopPos.x - leftBottomPos.x);
            vect.y = (PlayerAvatar.Player.position.z - leftBottomPos.y) * 488 / (rightTopPos.y - leftBottomPos.y) -488;
            float mainAngle = cameraRotation;
            
            float radius = Mathf.Sqrt(Mathf.Pow(244 - vect.x, 2) + Mathf.Pow(244 + vect.y, 2));
            float angle = Mathf.Atan2(vect.y + 244, vect.x - 244);
            vect.x = radius * Mathf.Cos(angle + mainAngle * Mathf.PI / 180.0f) + 244;
            vect.y = radius * Mathf.Sin(angle + mainAngle * Mathf.PI / 180.0f) - 244;
            vect.x = Mathf.Clamp(vect.x, bound.x / 2, 488 - bound.x/2);
            vect.y = Mathf.Clamp(vect.y, -488 + bound.y/2, -bound.y / 2);

            _rect.anchoredPosition = new Vector2(-vect.x + bound.x / 2, -vect.y - bound.y / 2);
        }

    }
}
