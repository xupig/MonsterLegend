﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Events;
using Game.UI.UIComponent;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using MogoEngine.Events;
using UnityEngine;

namespace ModuleMainUI
{
    /// <summary>
    /// IOS9视频录制开关--视图类
    /// </summary>
    public class FieldRePlayKitView : KContainer
    {
        private StateImage _imageBg;
        private StateText _txtTime;
        private KShrinkableButton _btn;
        private StateImage _imageStopRecord;      //录制停止图标
        private StateImage _imageRecording1;      //录制中图标1
        private StateImage _imageRecording2;      //录制中图标2
        private bool isMoreThanIos9 = false;      

        protected override void Awake()
        {
            _imageBg = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _txtTime = GetChildComponent<StateText>("Label_txtShijian");
            _btn = GetChildComponent<KShrinkableButton>("Button_tongzhi");
            _imageStopRecord = _btn.GetChildComponent<StateImage>("Image");
            _imageRecording1 = _btn.GetChildComponent<StateImage>("yuanxing");
            _imageRecording2 = _btn.GetChildComponent<StateImage>("yuanxing2");
            _txtTime.ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);

            //在IOS9及以上版本，允许视频录制
            #if UNITY_IPHONE && !UNITY_EDITOR
			     isMoreThanIos9 =IOSPlugins.isSupportRePlayKit();
			     LoggerHelper.Info ("isSupportRePlayKit isMoreThanIos9:"+isMoreThanIos9);
			     if(isMoreThanIos9) 
			     {
                     Show();
                     return;
                 }
            #endif
            Hide();
        }

        public void VisiableWithIosVersion()
        {
            Visible = isMoreThanIos9;
        }

        public void Show()
        {
            AddEventListener();
            ChangeStatusImage(RePlayKitManager.Instance.isRecording);
            Visible = true;
        }

        public void Hide()
        {
            RemoveEventListener();
            ChangeStatusImage(false);
            Visible = false;
        }

        private void AddEventListener()
        {
            _btn.onClick.AddListener(OnRePlayKit);
            EventDispatcher.AddEventListener<int, bool>(MainUIEvents.REFRESH_MV_RECORD_TIME, OnRefreshTime);
        }

        private void RemoveEventListener()
        {
            _btn.onClick.RemoveListener(OnRePlayKit);
            EventDispatcher.RemoveEventListener<int, bool>(MainUIEvents.REFRESH_MV_RECORD_TIME, OnRefreshTime);
        }

        //录制处理
        private void OnRePlayKit()
        {
            if (RePlayKitManager.Instance.isRecording)
            {//录制中，将停止录制
                RePlayKitManager.Instance.StopRecording();
            }
            else
            {//已停止，将开启录制
                RePlayKitManager.Instance.StartRecording();
            }
        }

        /// <summary>
        /// 刷新录制时间(从RePlayKitManager.cs中抛出)
        /// </summary>
        /// <param name="lastTime">已录制时间,单位：秒</param>
        private void OnRefreshTime(int lastTime, bool isStop)
        {
            if (Visible)
            {
                if (isStop)
                {//已停止
                    ChangeStatusImage(false);
                }
                else
                {//录制中
                    if (lastTime < 1) ChangeStatusImage(true);
                    _imageRecording1.Visible = !_imageRecording1.Visible;
                    int minute = lastTime / 60;
                    int secord = lastTime % 60;
                    _txtTime.CurrentText.text = string.Format("{0}:{1}", minute > 9 ? minute.ToString() : "0" + minute, secord > 9 ? secord.ToString() : "0" + secord);
                }
            }
        }

        //切换状态图
        private void ChangeStatusImage(bool isPlay)
        {
            if (isPlay)
            {//录制中
                _imageBg.Visible = true;
                _imageRecording1.Visible = true;
                _imageRecording2.Visible = true;
                _imageStopRecord.Visible = false;
            }
            else
            {//已停止录制
                _imageBg.Visible = false;
                _imageRecording1.Visible = false;
                _imageRecording2.Visible = false;
                _imageStopRecord.Visible = true;
                _txtTime.CurrentText.text = string.Empty;
            }
        }
    }
}
