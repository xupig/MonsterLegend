﻿using Common;
using Common.Events;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    public class FieldBattleView : KContainer
    {
        private FieldBattleButtonView _fieldBattleButtonView;
        private RectTransform _rect;
        private Vector3 _startPosition;

        protected override void Awake()
        {
            base.Awake();
            _fieldBattleButtonView = AddChildComponent<FieldBattleButtonView>("Container_topRight");
            _rect = _fieldBattleButtonView.GetComponent<RectTransform>();
            _startPosition = _rect.localPosition;
        }

        public void Show()
        {
            TweenTopRightButton();
            
            _fieldBattleButtonView.Visible = true;
            _fieldBattleButtonView.UpdateContent();
        }

        public void Hide()
        {
            _fieldBattleButtonView.Visible = false;
        }

        public void TweenTopRightButton()
        {
            Vector3 position = _rect.localPosition;
            position.x = MogoGameObjectHelper.ScreenPointToLocalPointInRectangle(_rect.parent as RectTransform, new Vector2(Screen.width, 0)).x;
            _rect.localPosition = position;

            TweenPosition.Begin(_rect.gameObject, 0.35f, _startPosition);
        }
    }
}
