﻿using Common.Base;
using Common.Events;
using Game.UI.UIComponent;
using GameData;
using GameMain.CombatSystem;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.RPC;
using UnityEngine;

namespace ModuleMainUI
{
    public class FieldCommonView:KContainer
    {
        private MainUIExpView _expView;
        private TeamTaskView _teamTaskView;
        private KButton _joyStickBtn;
        private KButton _clickControlBtn;

        protected override void Awake()
        {
 	         base.Awake();
             _expView = AddChildComponent<MainUIExpView>("Container_bottomCenter/Container_EXP");
             _teamTaskView = AddChildComponent<TeamTaskView>("Container_zuduirenwu");
             _joyStickBtn = GetChildComponent<KButton>("Container_topRight/Button_yaogan");
             _clickControlBtn = GetChildComponent<KButton>("Container_topRight/Button_dianchu");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            _expView.Visible = !SpotlightPanel.isBottomSpotLightRunning;
            AddEventListener();
            _teamTaskView.OnShow();
            RefreshControlBtnView();
        }

        protected override void OnDisable()
        {
            _teamTaskView.OnHide();
            RemoveEventListener();
            base.OnDisable();
        }

        public void ShowTaskView()
        {
            _teamTaskView.Visible = true;
        }

        public void HideTaskView()
        {
            _teamTaskView.Visible = false;
        }

        private void AddEventListener()
        {
            _joyStickBtn.onClick.AddListener(OnJoyStickBtnClick);
            _clickControlBtn.onClick.AddListener(OnClickControlBtnClick);
            EventDispatcher.AddEventListener<uint>(TouchEvents.ON_TOUCH_ENTITY, ShowPlayerInfo);
            EventDispatcher.AddEventListener<bool>(PlayerEvents.ON_IN_TOUCH_MODE_STATE_CHANGED, OnBattleModeChange);
            EventDispatcher.AddEventListener<ControlVehicle>(VehicleEvents.DRIVER_VEHICLE, OnDriverVehicle);
            EventDispatcher.AddEventListener<ControlVehicle>(VehicleEvents.DIVORCE_VEHICLE, OnDivorceVehicle);
            EventDispatcher.AddEventListener<bool>(CommonUIEvents.ON_BOTTOM_SPOTLIGHT_VISIBILITY_CHANGE, OnBottomSpotLightVisibleChange);
        }

        private void RemoveEventListener()
        {
            _joyStickBtn.onClick.RemoveListener(OnJoyStickBtnClick);
            _clickControlBtn.onClick.RemoveListener(OnClickControlBtnClick);
            EventDispatcher.RemoveEventListener<uint>(TouchEvents.ON_TOUCH_ENTITY, ShowPlayerInfo);
            EventDispatcher.RemoveEventListener<bool>(PlayerEvents.ON_IN_TOUCH_MODE_STATE_CHANGED, OnBattleModeChange);
            EventDispatcher.RemoveEventListener<ControlVehicle>(VehicleEvents.DRIVER_VEHICLE, OnDriverVehicle);
            EventDispatcher.RemoveEventListener<ControlVehicle>(VehicleEvents.DIVORCE_VEHICLE, OnDivorceVehicle);
            EventDispatcher.RemoveEventListener<bool>(CommonUIEvents.ON_BOTTOM_SPOTLIGHT_VISIBILITY_CHANGE, OnBottomSpotLightVisibleChange);
        }

       

        private void OnBottomSpotLightVisibleChange(bool visible)
        {
            _expView.Visible = visible == false;
        }

        private void OnBattleModeChange(bool obj)
        {
            RefreshControlBtnView();
        }

        private void OnDriverVehicle(ControlVehicle obj)
        {
            RefreshControlBtnView();
        }

        private void OnDivorceVehicle(ControlVehicle obj)
        {
            RefreshControlBtnView();
        }

        private void OnClickControlBtnClick()
        {
            PlayerTouchBattleModeManager.GetInstance().Enter();
        }

        private void OnJoyStickBtnClick()
        {
            PlayerTouchBattleModeManager.GetInstance().Exit();
        }

        private void RefreshControlBtnView()
        {
            if (PlayerTouchBattleModeManager.GetInstance().inTouchModeState == false)
            {
                _joyStickBtn.Visible = false;
                _clickControlBtn.Visible = true;
            }
            else
            {
                _joyStickBtn.Visible = true;
                _clickControlBtn.Visible = false;
            }
            if (PlayerAvatar.Player.vehicleManager.isDrivering)
            {
                _joyStickBtn.Visible = false;
                _clickControlBtn.Visible = false;
            }
            if (platform_helper.InPCPlatform())
            {
                _joyStickBtn.Visible = false;
                _clickControlBtn.Visible = false;
            }
        }

        private string _queryName;
        private int _npcId = -1;

        public void ShowPlayerInfo(uint entityId)
        {
            if (BaseModule.IsPanelCanvasEnable(PanelIdEnum.MainUIField))
            {
                if (entityId != PlayerAvatar.Player.dbid)
                {
                    Entity entity = MogoWorld.GetEntity(entityId);
                    if (entity == null) return;
                    if (entity.entityType == EntityConfig.ENTITY_TYPE_AVATAR)
                    {
                        EntityAvatar avatar = entity as EntityAvatar;
                        _queryName = avatar.name;
                        _npcId = -1;
                        OnPlayerInfoHeadClick();

                    }
                    else if (entity.entityType == EntityConfig.ENTITY_TYPE_NAME_NPC)
                    {
                        EntityNPC npc = entity as EntityNPC;
                        _npcId = npc.npc_id;
                        _queryName = null;
                        OnPlayerInfoHeadClick();
                    }

                }
            }
        }

        private void OnPlayerInfoHeadClick()
        {
            if (_queryName != null)
            {
                FriendManager.Instance.GetPlayerDetailInfo(_queryName);
            }

            if (_npcId != -1)
            {
                if (PlayerDataManager.Instance.FavorData.GetFavorData(_npcId) != null && PlayerDataManager.Instance.FunctionData.IsFunctionOpen(FunctionId.favor) == true)
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.Favor, _npcId);
                }
                else
                {
                    int dialog = npc_helper.GetRandomDialogID(_npcId);
                    if (dialog != 0 && dialog != -1)
                    {
                        UIManager.Instance.ShowPanel(PanelIdEnum.NpcDailog, new int[] { _npcId, 0 });
                    }
                }
            }
        }
    }
}
