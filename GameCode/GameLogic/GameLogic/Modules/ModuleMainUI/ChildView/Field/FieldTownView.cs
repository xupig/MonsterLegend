﻿using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    public class FieldTownView : KContainer
    {
        private KContainer _topRight;
        private FunctionComponent _functionComponent;
        private PlayerTownView _playerTownView;

        private KContainer _openFunctionEntry;
        private KButton _btnOpenFunctionEntry;
        private KContainer _functionTips;
        private RectTransform _openFunctionEntryRect;
        private Vector2 _openFunctionEntryPos;
        private RectTransform _functionTipsRect;
        private Vector2 _pos;
        private StateText _txtFunctionTips;
        private KComponentEvent _onOpenFunctionEntry;
        public KComponentEvent onOpenFunctionEntry
        {
            get
            {
                if (_onOpenFunctionEntry == null)
                {
                    _onOpenFunctionEntry = new KComponentEvent();
                }
                return _onOpenFunctionEntry;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            vect = Vector2.zero;
            _functionComponent = gameObject.AddComponent<FunctionComponent>();
            _playerTownView = AddChildComponent<PlayerTownView>("Container_topLeft/Container_Player");
            _topRight = GetChildComponent<KContainer>("Container_topRight");
            _openFunctionEntry = _topRight.GetChildComponent<KContainer>("Container_Shousuo");
            _btnOpenFunctionEntry = _openFunctionEntry.GetChildComponent<KButton>("Button_image");
            _functionTips = _openFunctionEntry.GetChildComponent<KContainer>("Container_tishixinxi");
            _openFunctionEntryRect = _openFunctionEntry.GetComponent<RectTransform>();
            _openFunctionEntryPos = _openFunctionEntryRect.anchoredPosition;
            _functionTipsRect = _functionTips.GetComponent<RectTransform>();
            _pos = _functionTipsRect.anchoredPosition;
            _txtFunctionTips = _functionTips.GetChildComponent<StateText>("Label_txtNinyouxindetishi");
        }

        public void ShowActivityView()
        {
            _functionComponent.ShowActivityView();
        }

        public void HideActivityView()
        {
            _functionComponent.HideActivityView();
        }

        protected override void OnEnable()
        {
            _btnOpenFunctionEntry.onClick.AddListener(OnClickOpenFunctionEntry);
            EventDispatcher.AddEventListener<int, GameObject, Vector3>(FunctionEvents.OPEN, OnFunctionOpen);
            EventDispatcher.AddEventListener(FunctionEvents.UPDATE_NOTICE, UpdateNotice);
            UpdateNotice();
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            _btnOpenFunctionEntry.onClick.RemoveListener(OnClickOpenFunctionEntry);
            EventDispatcher.RemoveEventListener<int, GameObject, Vector3>(FunctionEvents.OPEN, OnFunctionOpen);
            EventDispatcher.RemoveEventListener(FunctionEvents.UPDATE_NOTICE, UpdateNotice);
            base.OnDisable();
        }

        private void UpdateNotice()
        {
            if (PlayerDataManager.Instance.FunctionData.noticeList.Count > 0)
            {
                foreach (int functionId in PlayerDataManager.Instance.FunctionData.noticeList)
                {
                    if (function_helper.NeedPushNoticeToMainUI(functionId))
                    {
                        _functionTips.Visible = true;
                        function function = function_helper.GetFunction(functionId);
                        _txtFunctionTips.CurrentText.text = (2683).ToLanguage(function.__name.ToLanguage());
                        return;
                    }
                }
                _functionTips.Visible = false;
            }
            else
            {
                _functionTips.Visible = false;
            }
        }

        private void OnFunctionOpen(int openId, GameObject go, Vector3 offset)
        {
            function function = function_helper.GetFunction(openId);
            if (function.__type == 1 || function.__type == 4 || function.__type == 5 || function.__type == 6)
            {
                UIManager.Instance.EnablePanelCanvas(PanelIdEnum.MainUIField);
                TweenAbsolutePosition.Begin(go, 1f, _btnOpenFunctionEntry.transform.position, delegate(UITweener tween)
                {
                    if (UIManager.GetLayerVisibility(MogoUILayer.LayerUIMain) == false)
                    {
                        UIManager.Instance.DisablePanelCanvas(PanelIdEnum.MainUIField);
                    }
                    PanelIdEnum.Function.Close();
                    PlayerDataManager.Instance.FunctionData.TweenEnd(function.__id);
                });
            }
        }

        private Vector2 vect;
        private int count = 0;
        private int dir = 0;

        void Update()
        {
            if (_functionTips.Visible)
            {
                if (dir == 0)
                {
                    count++;
                    if (count >= 30)
                    {
                        dir = 1;
                    }
                }
                else
                {
                    count--;
                    if (count <= 0)
                    {
                        dir = 0;
                    }
                }
                vect.x = count;
                _functionTipsRect.anchoredPosition = _pos + vect;
            }
        }

        private void OnClickOpenFunctionEntry()
        {
            _topRight.Visible = false;
            _openFunctionEntry.Visible = false;
            onOpenFunctionEntry.Invoke();
        }

        public void CloseFunction()
        {
            _topRight.Visible = false;
            _openFunctionEntry.Visible = false;
        }

        public void CloseFunctionEntry()
        {
            _topRight.Visible = true;
            _openFunctionEntry.Visible = true;
            ResetPos();
        }

        public void Show()
        {
            _topRight.Visible = true;
            _openFunctionEntry.Visible = true;
            ResetPos();
            RefreshFunctionEntryPos();
            _functionComponent.OnFunctionRefresh();
        }

        private bool HasSmallMap()
        {
            int mapType = GameSceneManager.GetInstance().curMapType;
            if (GameSceneManager.GetInstance().IsInCity() ||
                mapType == public_config.MAP_TYPE_GUILD_WAIT_SINGLE_COPY ||
                mapType == public_config.MAP_TYPE_GUILD_WAIT_DOUBLE_COPY ||
                mapType == public_config.MAP_TYPE_DUEL)
            {
                return false;
            }
            return true;
        }

        private void RefreshFunctionEntryPos()
        {
            _openFunctionEntryRect.anchoredPosition = HasSmallMap() ? _openFunctionEntryPos : _openFunctionEntryPos + new Vector2(160, 0);
        }

        private void ResetPos()
        {
            dir = 0;
            count = 0;
            _functionTipsRect.anchoredPosition = _pos;
        }

        public void Hide()
        {
            Visible = false;
        }

        public void ShowPlayerTownView()
        {
            if (!_playerTownView.Visible)
            {
                _playerTownView.Visible = true;
            }
        }

        public void HidePlayerTownView()
        {
            if (_playerTownView.Visible)
            {
                _playerTownView.Visible = false;
            }
        }
    }
}
