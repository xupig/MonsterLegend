using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Game.UI.UIComponent;
using Common.Global;
using GameLoader.Utils.Timer;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager;

namespace ModuleMainUI
{
    public class AutoTextList : KContainer
    {
        private AutoTextView _findPathView;
        private AutoTextView _autoBattleView;
        private AutoTextView _followBattleView;
        private AutoTextView _teamTaskView;
        private List<KContainer> _containerList;
        private RectTransform _rectEnd;
        private Vector2 _orignalVect;


        protected override void Awake()
        {
            _autoBattleView = AddChildComponent<AutoTextView>("Container_zidongzhandoutext");
            _findPathView = AddChildComponent<AutoTextView>("Container_zidongxunlutext");
            _followBattleView = AddChildComponent<AutoTextView>("Container_gensuizhandoutext");
            _teamTaskView = AddChildComponent<AutoTextView>("Container_shikongliexi");

            _containerList = new List<KContainer>();
            for (int i = 1; i < 11;i++ )
            {
                KContainer container = _teamTaskView.GetChildComponent<KContainer>(string.Format("Container_num/Container_num{0}",i));
                _containerList.Add(container);
            }
            _rectEnd = _teamTaskView.GetChildComponent<RectTransform>("Container_end");
            _orignalVect = _rectEnd.anchoredPosition;
            EventDispatcher.AddEventListener<AutoTextType>(BattleUIEvents.SHOW_AUTO_TEXT, Show);
            EventDispatcher.AddEventListener(BattleUIEvents.HIDE_AUTO_TEXT, Hide);
           
        }

        protected override void OnDestroy()
        {
            EventDispatcher.RemoveEventListener<AutoTextType>(BattleUIEvents.SHOW_AUTO_TEXT, Show);
            EventDispatcher.RemoveEventListener(BattleUIEvents.HIDE_AUTO_TEXT, Hide);
           
            base.OnDestroy();
        }


        private void Show(AutoTextType type)
        {
            Hide();
            switch (type)
            {
                case AutoTextType.AutoBattle:
                    _autoBattleView.Show();
                    break;
                case AutoTextType.AutoFindPath:
                    _findPathView.Show();
                    break;
                case AutoTextType.FollowBattle:
                    _followBattleView.Show();
                    break;
                case AutoTextType.TeamTaskRing:
                    _teamTaskView.Show();
                    UpdateTeamTaskRingNum();
                    break;
            }
        }

        private void UpdateTeamTaskRingNum()
        {
            int ring = PlayerDataManager.Instance.TaskData.ring;
            
            for(int i=1;i<11;i++)
            {
                _containerList[i-1].Visible = (ring == i);
            }
            if(ring == 10)
            {
                _rectEnd.anchoredPosition = _orignalVect + new Vector2(8, 0);
            }
            else
            {
                _rectEnd.anchoredPosition = _orignalVect;
            }
        }

        public void Hide()
        {
            _findPathView.Hide();
            _autoBattleView.Hide();
            _followBattleView.Hide();
            _teamTaskView.Hide();
        }

    }
}