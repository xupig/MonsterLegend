using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Game.UI.UIComponent;
using Common.Global;
using GameLoader.Utils.Timer;
using Common.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;

namespace ModuleMainUI
{
    public class AutoTextView : KContainer
    {
        private RectTransform _rect;

        private int _count;
        private int _index;
        private int _height;
        private List<Locater> _locaterList = new List<Locater>();
        private List<float> _originalYList = new List<float>();

        private uint _timerId;
        private int _pauseTime;

        private const int INTERVAL = 20;
        private const int MAX_HEIGHT = 20;

        protected override void Awake()
        {
            _rect = gameObject.GetComponent<RectTransform>();
            _count = this.transform.childCount;
            for (int i = 0; i < _count; i++)
            {
                _locaterList.Add(this.GetChild(i).AddComponent<Locater>());
                _originalYList.Add(_locaterList[i].Y);
            }
            _locaterList.Reverse();
            _originalYList.Reverse();
            Hide();
        }

        public void Show()
        {
            this.Visible = true;
            AutoTextLocate();
            _timerId = TimerHeap.AddTimer(0, INTERVAL, OnTimerStep);
        }

        public void Hide()
        {
            this.Visible = false;
            ResetAutoText();
        }

        private bool _isRise = false;
        private void OnTimerStep()
        {
            if (_pauseTime > 0)
            {
                _pauseTime -= INTERVAL;
                return;
            }
            if (_isRise) _height += 5;
            else _height -= 5;
            if (_height >= MAX_HEIGHT || _height < 0)
            {
                _isRise = !_isRise;
                if (_isRise)
                {
                    _height = 0;
                    _index++;
                    ResetIndex();
                }
            }
            _locaterList[_index].Y = _originalYList[_index] + _height;
        }

        private void ResetIndex()
        {
            if (_index >= _count)
            {
                _index = 0;
                _pauseTime = 2000;
            }
        }

        private void ResetAutoText()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
            }
            _timerId = 0;
            _height = 0;
            _index = 0;
            _pauseTime = 0;
            _isRise = true;
            for (int i = 0; i < _locaterList.Count; i++)
            {
                _locaterList[i].Y = _originalYList[i];
            }
        }

        private void AutoTextLocate()
        {
            Vector2 screenPosition = new Vector2(Screen.width / 2, Screen.height / 2) + new Vector2(0, 0.1f * Screen.height);
            Vector3 localPosition = MogoGameObjectHelper.ScreenPointToLocalPointInRectangle(_rect.parent.GetComponent<RectTransform>(), screenPosition);
            localPosition += new Vector3(-0.5f * _rect.sizeDelta.x, 0.5f * _rect.sizeDelta.y, -100);
            localPosition.y += 90;
            if (UIManager.GetLayerVisibility(MogoUILayer.LayerUIMain) == false)
            {
                localPosition.y -= 2000;
            }
            _rect.localPosition = localPosition;
        }

    }
}