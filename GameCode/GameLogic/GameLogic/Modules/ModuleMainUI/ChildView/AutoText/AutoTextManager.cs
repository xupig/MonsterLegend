using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Game.UI.UIComponent;
using Common.Global;
using GameLoader.Utils.Timer;
using MogoEngine.Events;
using Common.Events;

namespace ModuleMainUI
{
    public enum AutoTextType
    {
		AutoBattle = 1,
        FollowBattle = 2,
        AutoFindPath = 3,
        TeamTaskRing = 4,
    }
	
    public class AutoTextManager
    {
        public AutoTextManager()
        {
			
        }

        private static AutoTextManager s_Instance;
        public static AutoTextManager Instance
        {
            get
            {
                if (s_Instance == null)
                    s_Instance = new AutoTextManager();
                return s_Instance;
            }
        }

        public void ShowAutoText(AutoTextType type)
        {
            EventDispatcher.TriggerEvent<AutoTextType>(BattleUIEvents.SHOW_AUTO_TEXT, type);
        }

        public void HideAutoText()
        {
            EventDispatcher.TriggerEvent(BattleUIEvents.HIDE_AUTO_TEXT);
        }

    }
}