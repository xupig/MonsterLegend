﻿using Common.Base;
using UnityEngine;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager;
using Common.ServerConfig;
using GameData;
using Common.Data;
using Common.Global;
using GameMain.Entities;
using ModuleCommonUI;
using Common.Utils;
using Game.UI.UIComponent;

namespace ModuleMainUI
{
    public class MainUIBattlePanel : BasePanel
    {
        private PlayerBattleView _playerView;
        private BossView _bossView;
        private InformationView _infomationView;
        private PetView _petView;
        private TopRightView _topRightView;
        private BattleTeamView _teamView;
        private LuckNoticeView _luckView;
        private FieldRePlayKitView _replayKitView1;  //IOS9视频录制开关视图
        private FieldRePlayKitView _replayKitView2;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.MainUIBattle; }
        }

        protected override void Awake()
        {
            _bossView = AddChildComponent<BossView>("Container_topCenter/Container_boss");
            _playerView = AddChildComponent<PlayerBattleView>("Container_topLeft");
            _infomationView = AddChildComponent<InformationView>("Container_information");
            _petView = AddChildComponent<PetView>("Container_chongwu");
            _teamView = AddChildComponent<BattleTeamView>("Container_duiwu");
            _topRightView = AddChildComponent<TopRightView>("Container_topRight");
            _luckView = AddChildComponent<LuckNoticeView>("Container_topCenter/Container_xingyun");
            _replayKitView1 = AddChildComponent<FieldRePlayKitView>("Container_luxiang");
            _replayKitView2 = AddChildComponent<FieldRePlayKitView>("Container_luxiang2");
            IOSRePlayKitIcon();
        }

        //显示ios下录制图标
        private void IOSRePlayKitIcon()
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                int cardId = GameSceneManager.GetInstance().scene.GetInstanceID();
                instance data = XMLManager.instance.ContainsKey(cardId) ? XMLManager.instance[cardId] : null;
                if (data != null && data.__type == 1)
                {//野外场景，显示_replayKitView2录制图标
                    _replayKitView1.Visible = false;
                    _replayKitView2.VisiableWithIosVersion();
                }
                else
                {//其他场景,显示_replayKitView1录制图标
                    _replayKitView1.VisiableWithIosVersion();
                    _replayKitView2.Visible = false;
                }
            }
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(BattleUIEvents.REFRESH_BOSS_VIEW, RefreshBossView);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(BattleUIEvents.REFRESH_BOSS_VIEW, RefreshBossView);
        }

        public override void OnShow(object data)
        {
            _infomationView.Show();
            _petView.Show();
            gameObject.AddComponent<TimerManager>();
            InitBattleUI();
            InitTeamUI();
            CheckCanEquipProficient();
            AddEventListener();
        }

        public override void OnClose()
        {
            RemoveEventListener();
            _infomationView.Hide();
            UIManager.Instance.ClosePanel(PanelIdEnum.EveningActivityStatistic);
            _bossView.Hide();
        }

        public override void OnEnterMap(int mapId)
        {
            base.OnEnterMap(mapId);
            InitBattleUI();
            InitTeamUI();
            CheckCanEquipProficient();
        }

        public override void OnLeaveMap(int mapId)
        {
            _petView.OnLevelMap(mapId);
            _bossView.Hide();
        }

        private void CheckCanEquipProficient()
        {
            int changeProficientType = inst_type_operate_helper.GetChangeProficientType(GameSceneManager.GetInstance().chapterType);
            if (changeProficientType != 0)
            {
                if (PlayerAvatar.Player.level >= GlobalParams.GetSkillOpenLevel() && PlayerAvatar.Player.spell_proficient == 0)
                {
                    MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(71487), OnConfirm, true, false);
                }
            }
        }

        private void OnConfirm()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.ProficientSkill);
        }

        private void RefreshBossView()
        {
            if (LifeBarManager.Instance.GetBossEntityId() == 0)
            {
                _bossView.Hide();
                return;
            }
            if (GameSceneManager.GetInstance().inCombatScene)
            {
                _bossView.Show();
            }
        }

        private void InitBattleUI()
        {
            int mapType = GameSceneManager.GetInstance().curMapType;
            switch (mapType)
            {
                case public_config.MAP_TYPE_NIGHT_ACTIVITY:
                    break;
                case public_config.MAP_TYPE_COMBAT_PORTAL:
                    UIManager.Instance.ShowPanel(PanelIdEnum.PortalFight);
                    break;
                case public_config.MAP_TYPE_GUILD_PK_COPY:
                    if (PlayerDataManager.Instance.GuildWarData.EnemyData != null)
                    {
                        CopyLogicManager.Instance.ShowPreviewPanel((ChapterType)mapType);
                    }
                    break;
                case public_config.MAP_TYPE_DUEL_MATCH:
                    CopyLogicManager.Instance.ShowPreviewPanel((ChapterType)mapType);
                    break;
            }
            UIManager.Instance.ShowPanel(PanelIdEnum.EveningActivityStatistic);
        }

        private void InitTeamUI()
        {
            int teamType = instance_helper.GetTeamTypeByMapId(GameSceneManager.GetInstance().curMapID);
            _teamView.Visible = teamType == public_config.MAP_TEAM_TYPE_TEAM;
        }

    }
}
