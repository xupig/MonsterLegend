using Common.States;
using UnityEngine;
using UnityEngine.EventSystems;

public class ControlStick : MonoBehaviour
{
    private int MAX_DIS = 100;
    
    private Vector2 m_currentDragDetla = Vector2.zero;

    private Vector2 m_ControlStickSourcePosition = Vector2.zero;

    private Vector2 m_ControlStickCenterOffset = Vector2.zero;

    private RectTransform m_thisRectTransform = null;


    void Awake()
    {
        m_thisRectTransform = this.transform as RectTransform;
        m_ControlStickSourcePosition.x = m_thisRectTransform.localPosition.x;
        m_ControlStickSourcePosition.y = m_thisRectTransform.localPosition.y;

        m_ControlStickCenterOffset.x = m_thisRectTransform.rect.width / 2;
        m_ControlStickCenterOffset.y = -m_thisRectTransform.rect.height / 2;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
       
    }

    public void OnDrag(PointerEventData eventData)
    {
        //if (GMState.keyboardCtrl)
        //{
        //    return;
        //}
        Vector2 fingerPos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(m_thisRectTransform.parent as RectTransform,
            eventData.position, eventData.pressEventCamera, out fingerPos))
        {
            var center = m_ControlStickSourcePosition + m_ControlStickCenterOffset;
            var direction = (fingerPos - center);
            if (direction.magnitude < MAX_DIS)
            {
                m_thisRectTransform.localPosition =
                    new Vector3(fingerPos.x - m_ControlStickCenterOffset.x, fingerPos.y - m_ControlStickCenterOffset.y, 0);
            }
            else
            {
                direction = direction.normalized;
                direction = direction * MAX_DIS;
                var stickPos = center + direction;
                m_thisRectTransform.localPosition =
                    new Vector3(stickPos.x - m_ControlStickCenterOffset.x, stickPos.y - m_ControlStickCenterOffset.y, 0);
            }
            ControlStickState.direction = direction;
            ControlStickState.strength = direction.magnitude / MAX_DIS;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        ResetControlStick();
    }

    public void ResetControlStick()
    {
        //if (GMState.keyboardCtrl)
        //{
        //    return;
        //}
        m_thisRectTransform.localPosition = new Vector3(m_ControlStickSourcePosition.x, m_ControlStickSourcePosition.y, 0);
        ControlStickState.direction = Vector2.zero;
        ControlStickState.strength = 0;
    }
}