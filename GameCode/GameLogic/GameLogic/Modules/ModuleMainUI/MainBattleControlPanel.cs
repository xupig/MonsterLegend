﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using UnityEngine;
using MogoEngine.Events;
using GameLoader.Utils;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.Entities;
using Common.States;
using UnityEngine.UI;
using Common.Utils;

using ModuleCommonUI;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using Common.ServerConfig;
using Common;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.CombatSystem;

namespace ModuleMainUI
{
    public class MainBattleControlPanel : BasePanel
    {
        private SkillView _skillView;
        private ControlStickView _stickView;
        private KButton _resetBtn;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.MainBattleControl; }
        }

        protected override void Awake()
        {
            _skillView = AddChildComponent<SkillView>("Container_bottomRight/Container_SkillButton");
            _stickView = AddChildComponent<ControlStickView>("Container_bottomLeft/Container_wheel");
            if (platform_helper.InPCPlatform())
            {
                _resetBtn = GetChildComponent<KButton>("Container_topRight/Container_shuaxin/Button_qiehuan");
            }
            else
            {
                _resetBtn = GetChildComponent<KButton>("Container_bottomRight/Container_shuaxin/Button_qiehuan");
            }
        }

        private void AddEventListener()
        {
            _resetBtn.onClick.AddListener(OnResetBtnClick);
            EventDispatcher.AddEventListener<ControlVehicle>(VehicleEvents.DRIVER_VEHICLE, OnDriverVehicle);
            EventDispatcher.AddEventListener<ControlVehicle>(VehicleEvents.DIVORCE_VEHICLE, OnDivorceVehicle);
            EventDispatcher.AddEventListener<bool>(PlayerEvents.ON_IN_TOUCH_MODE_STATE_CHANGED, OnBattleModeChange);
        }

        private void RemoveEventListener()
        {
            _resetBtn.onClick.RemoveListener(OnResetBtnClick);
            EventDispatcher.RemoveEventListener<ControlVehicle>(VehicleEvents.DRIVER_VEHICLE, OnDriverVehicle);
            EventDispatcher.RemoveEventListener<ControlVehicle>(VehicleEvents.DIVORCE_VEHICLE, OnDivorceVehicle);
            EventDispatcher.RemoveEventListener<bool>(PlayerEvents.ON_IN_TOUCH_MODE_STATE_CHANGED, OnBattleModeChange);
        }

        private void OnResetBtnClick()
        {
            if (platform_helper.InPCPlatform())
            {
                DramaTrigger.ClickButton("LayerUIMain/Container_MainBattleControlPanel/Container_topRight/Container_shuaxin/Button_qiehuan,Button");
            }
            else
            {
                DramaTrigger.ClickButton("LayerUIMain/Container_MainBattleControlPanel/Container_bottomRight/Container_shuaxin/Button_qiehuan,Button");
            }
            if (PlayerAvatar.Player.skillLockManager.ManualUnlockEntity() == false)
            {
                CameraManager.GetInstance().OnRecoverTouchesCamera();
            }
        }

        private void OnBattleModeChange(bool obj)
        {
            RefreshControlStyle();
        }

        private void OnDriverVehicle(ControlVehicle obj)
        {
            RefreshControlStyle();
        }

        private void OnDivorceVehicle(ControlVehicle obj)
        {
            RefreshControlStyle();
        }

        private void RefreshControlStyle()
        {
            RefreshResetBtnView();
            RefreshStickView();
        }

        private void RefreshStickView()
        {
            _stickView.Visible = true;
            if (PlayerTouchBattleModeManager.GetInstance().inTouchModeState)
            {
                _stickView.Visible = false;
            }
            if (PlayerAvatar.Player.vehicleManager.isDrivering && PlayerAvatar.Player.vehicleManager.vehicleType == GameMain.CombatSystem.VehicleType.AIRVEHICLE)
            {
                _stickView.Visible = false;
            }
            if (platform_helper.InPCPlatform())
            {
                _stickView.Visible = false;
            }
        }

        private void RefreshResetBtnView()
        {
            _resetBtn.Visible = true;
            if (platform_helper.InPCPlatform() == false && PlayerTouchBattleModeManager.GetInstance().inTouchModeState == false)
            {
                _resetBtn.Visible = false;
            }
        }

        public override void OnShow(object data)
        {
            InitControlUI();
            AddEventListener();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        public override void OnEnterMap(int mapId)
        {
            base.OnEnterMap(mapId);
            InitControlUI();
            skill_guide_helper.OnEnterMap();
        }

        public override void OnLeaveMap(int mapId)
        {

        }

        private void InitControlUI()
        {
            this.transform.SetSiblingIndex(1);
            int mapType = GameSceneManager.GetInstance().curMapType;
            _skillView.Visible = true;
            switch (mapType)
            {
                case public_config.MAP_TYPE_CITY:
                case public_config.MAP_TYPE_GUILD_WAIT_SINGLE_COPY:
                case public_config.MAP_TYPE_GUILD_WAIT_DOUBLE_COPY:
                case public_config.MAP_TYPE_DUEL:
                    _skillView.Visible = false;
                    break;
            }
            RefreshControlStyle();
        }

    }
}
