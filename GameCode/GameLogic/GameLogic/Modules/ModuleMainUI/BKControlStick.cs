// 模块名   :  ControlStick
// 创建者   :  莫卓豪
// 创建日期 :  2013-1-18
// 描    述 :  摇杆

using UnityEngine;
using System.Collections;
using Mogo.Util;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class BKControlStick : MonoBehaviour
{
    public float rangeStick;//stick可以移动范围半径

    /// <summary>
    /// 是否使用键盘操作
    /// </summary>
    private bool m_isKeyboard = false;
    public bool IsKeyboard
    {
        get { return m_isKeyboard; }
        set
        {
            m_isKeyboard = value;
            if (IsKeyboard)
            {
                m_tranControllerBg.gameObject.SetActive(false);
            }
            else
            {
                m_tranControllerBg.gameObject.SetActive(true);
            }
        }
    }

    private Rect rectCanTouch;
    private float actualRangeStick;
    private Vector2 actualCenterBg;
    private Vector2 actualCenterStick;
    private Rect actualRectBg;
    private Rect actualRectStick;
    public float canMoveRange = 0.1f;

    private bool m_isDraging = false;
    public bool isDraging
    {
        get
        {
            return m_isDraging;
        }
        set
        {
            m_isDraging = value;
        }
    }

    public bool IsDraging
    {
        get
        {
            return isDraging && (strength > canMoveRange);
        }
    }

    public bool IsTuring
    {
        get
        {
            return isDraging && (strength <= canMoveRange);
        }
    }

    public Vector2 direction = Vector2.zero;
    public float strength = 0f;
    private int fingerId = -100;


    private int screenWidth = Screen.width;
    private int screenHeight = Screen.height;

    private RectTransform m_tranControllerButton;
    private RectTransform m_tranControllerBg;
    private RectTransform m_tranControllerOriginalPos;

    private Image m_spControllerBG;
    private Image m_spControllerButtonBG;
    private Image m_spControllerButtonArrow;

    public Action<bool, Vector2> stateInfoUpdate;
    public Action onDragBegin;

    void Awake()
    {
        //确保手机设备上获取正确的屏幕分辨率
        Invoke("InitSize", 0.1f);

        m_tranControllerButton = transform.FindChild("JoyStickBtn").GetComponent<RectTransform>();
        m_tranControllerBg = transform.FindChild("JoyStickBg").GetComponent<RectTransform>();
        m_tranControllerOriginalPos = transform.FindChild("JoyStickPosInit").GetComponent<RectTransform>();

        m_spControllerBG = m_tranControllerBg.GetComponent<Image>();
        m_spControllerButtonBG = m_tranControllerButton.FindChild("JoyStickBtnBg").GetComponent<Image>();
        m_spControllerButtonArrow = m_tranControllerButton.FindChild("JoyStickBtnArrow").GetComponent<Image>();

        
        SetControllerAlpha(false);

        //Utils.AddUIEvent(gameObject, EventTriggerType.PointerDown, OnPointDown);
        //Utils.AddUIEvent(gameObject, EventTriggerType.PointerUp, OnPointUp);
    }

    void InitSize()
    {
        actualRangeStick = m_tranControllerBg.sizeDelta.x * UIResize.scale * 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        //direction = Vector2.zero;
        //strength = 0;
        if (!IsKeyboard)
        {
            if (isDraging)
            {
                Vector2 touchPosition = GetTouchPosition();
                ChangeStickPositon(touchPosition);

                direction = (actualCenterStick - actualCenterBg);
                strength = direction.magnitude / actualRangeStick;
                direction = direction.normalized;
                
                m_tranControllerButton.anchoredPosition = new Vector2(actualCenterStick.x,Screen.height - actualCenterStick.y)/ UIResize.scale;
                m_tranControllerBg.anchoredPosition = new Vector2(actualCenterBg.x, Screen.height - actualCenterBg.y) / UIResize.scale;
            }

            if (stateInfoUpdate != null)
            {
                stateInfoUpdate(isDraging, direction);
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                curKeyCode |= responseKeys[KeyCode.W];
                isPress();
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                curKeyCode |= responseKeys[KeyCode.S];
                isPress();
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                curKeyCode |= responseKeys[KeyCode.A];
                isPress();
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                curKeyCode |= responseKeys[KeyCode.D];
                isPress();
            }
            if (Input.GetKeyUp(KeyCode.W))
            {
                curKeyCode &= ~responseKeys[KeyCode.W];
                isPress();
            }
            if (Input.GetKeyUp(KeyCode.S))
            {
                curKeyCode &= ~responseKeys[KeyCode.S];
                isPress();
            }
            if (Input.GetKeyUp(KeyCode.A))
            {
                curKeyCode &= ~responseKeys[KeyCode.A];
                isPress();
            }
            if (Input.GetKeyUp(KeyCode.D))
            {
                curKeyCode &= ~responseKeys[KeyCode.D];
                isPress();
            }
        }
    }

    private void isPress()
    {
        if (curKeyCode > 0)
        {
            direction = (curKeyCode & responseKeys[KeyCode.W]) * new Vector2(0, -1)
                + ((curKeyCode & responseKeys[KeyCode.S]) >> 1) * new Vector2(0, 1)
                + ((curKeyCode & responseKeys[KeyCode.A]) >> 2) * new Vector2(-1, 0)
                + ((curKeyCode & responseKeys[KeyCode.D]) >> 3) * new Vector2(1, 0);

            m_isDraging = true;
            strength = 1;
        }
        else
        {
            direction = Vector2.one;
            m_isDraging = false;
            strength = 0;
        }
    }

    private int curKeyCode = 0x00;
    private static Dictionary<KeyCode, int> responseKeys
        = new Dictionary<KeyCode, int>() 
        { 
            {KeyCode.W , 0x01},
            {KeyCode.S , 0x02},
            {KeyCode.A , 0x04},
            {KeyCode.D , 0x08}
        };

    private void TouchBegin(Vector2 touchPosition)
    {
        isDraging = true;

        actualRectBg.center = touchPosition;
        actualCenterBg = touchPosition;

        actualRectStick.center = touchPosition;
        actualCenterStick = touchPosition;

        m_tranControllerButton.gameObject.SetActive(true);
        m_tranControllerBg.gameObject.SetActive(true);
        SetControllerAlpha(true);
    }

    private void ChangeStickPositon(Vector2 touchPosition)
    {
        Vector2 v = touchPosition - actualRectBg.center;
        if (v.magnitude > actualRangeStick)
        {
            v = v.normalized;
            v = v * actualRangeStick;
            v = actualRectBg.center + v;
            actualRectStick.center = v;
            actualCenterStick = v;
        }
        else
        {
            actualRectStick.center = touchPosition;
            actualCenterStick = touchPosition;
        }
    }

    private Vector2 GetTouchPosition(bool touchBegin = false)
    {
        Vector2 touchPosition = Vector2.zero;
        switch (Application.platform)
        {
            case RuntimePlatform.IPhonePlayer:
            case RuntimePlatform.Android:
                {
                    if (touchBegin)
                    {
                        for (int i = 0; i < Input.touchCount; i++)
                        {
                            Touch touch = Input.GetTouch(i);

                            if (touch.phase == TouchPhase.Began)
                            {
                                //转成左上角
                                Vector2 temp = new Vector2(touch.position.x, screenHeight - touch.position.y);
                                if (rectCanTouch.Contains(temp))
                                {
                                    touchPosition = temp;
                                    fingerId = touch.fingerId;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < Input.touchCount; i++)
                        {
                            Touch touch = Input.GetTouch(i);

                            if (touch.fingerId == fingerId)
                            {
                                Vector2 temp = new Vector2(touch.position.x, screenHeight - touch.position.y);
                                touchPosition = temp;
                                break;
                            }
                        }
                    }

                    break;
                }
            case RuntimePlatform.WindowsEditor:
            case RuntimePlatform.WindowsPlayer:
            case RuntimePlatform.OSXEditor:
            case RuntimePlatform.OSXPlayer:
                {
                    touchPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                    touchPosition.y = Screen.height - touchPosition.y;
                    break;
                }
            default:
                break;
        }
        return touchPosition;
    }

    public void Reset()
    {
        actualRectStick.center = actualRectBg.center;
        actualCenterStick = actualRectBg.center;
        isDraging = false;
        direction = Vector2.zero;
        curKeyCode = 0x00;
        fingerId = -100;
        strength = 0;

        m_tranControllerBg.anchoredPosition = m_tranControllerOriginalPos.anchoredPosition;
        m_tranControllerButton.anchoredPosition = m_tranControllerOriginalPos.anchoredPosition;
        SetControllerAlpha(false);
    }

    void OnPointDown(BaseEventData eventData)
    {
        PointerEventData data = eventData as PointerEventData;

        //Debug.LogError("data.pointerId:" + data.pointerId);
        //Debug.LogError("data.position:" + data.position);
        //Debug.LogError("data.pressPositon:" + data.pressPositon);

        for (int i = 0; i < Input.touchCount; i++)
        {
            //Touch touch = Input.GetTouch(i);
            //Debug.LogError("touch.position:" + touch.position);
            //Debug.LogError("touch.position:" + touch.fingerId);
        }

        if (!IsKeyboard)
        {
            fingerId = data.pointerId;
            Vector2 touchPosition = new Vector2(data.pressPosition.x, Screen.height - data.pressPosition.y);
            TouchBegin(touchPosition);
            if (onDragBegin != null) onDragBegin();
        }
    }

    private void OnPointUp(BaseEventData arg0)
    {
        if (!IsKeyboard)
        {
            Reset();
        }
    }

    public void FakePress()
    {
        Vector2 touchPosition = GetTouchPosition(true);
        TouchBegin(touchPosition);
    }

    private void SetControllerAlpha(bool isPress)
    {
        if (isPress)
        {
            ChangeImgAlpha(m_spControllerBG, 1);
            ChangeImgAlpha(m_spControllerButtonBG, 1);
            ChangeImgAlpha(m_spControllerButtonArrow, 1);
        }
        else
        {
            ChangeImgAlpha(m_spControllerBG, 0.6f);
            ChangeImgAlpha(m_spControllerButtonBG, 0.6f);
            ChangeImgAlpha(m_spControllerButtonArrow, 0.6f);
        }
    }

    private void ChangeImgAlpha(Image img, float a)
    {
        img.color = new Color(img.color.r, img.color.g, img.color.b, a);
    }
}