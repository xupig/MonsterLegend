/*==========================================
// Copyright (C) 2013 广州，爱游
//
// 模块名：BillboardView
// 创建者：Matako
// 修改者列表：
// 创建日期：
// 模块描述：
//==========================================*/

/*
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Mogo.Util;
using Common;
using Common.Base;
using ModuleBillboard;

public class BillboardView : BasePanel
{
    //BillboardModule m_module;
    Camera m_rootCamera;

    public BillboardView(BillboardModule module)
    {
        //m_module = module;
    }

    public override void OnShow(object data)
    {

    }

    public override void OnClose()
    {

    }

    protected override PanelIdEnum Id
    {
        get { return PanelIdEnum.Billboard; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="pos">屏幕坐标</param>
    /// <param name="blood"></param>
    /// <param name="type"></param>
    public void AddSplitBattleBillboard(Vector2 pos, int blood, SplitBattleBillboardType type)
    {
        SuperBattleBillboard ssb = null;

        switch (type)
        {
            case SplitBattleBillboardType.CriticalMonster:
                ssb = new CriticalMonster(blood);
                break;

            case SplitBattleBillboardType.CriticalPlayer:
                ssb = new CriticalPlayer(blood);
                break;

            case SplitBattleBillboardType.BrokenAttack:
                ssb = new BrokenAttack(blood);
                break;

            case SplitBattleBillboardType.NormalMonster:
                ssb = new NormalMonster(blood);
                break;

            case SplitBattleBillboardType.NormalPlayer:
                ssb = new NormalPlayer(blood);
                break;

            case SplitBattleBillboardType.Miss:
            default:
                ssb = new Miss(blood);
                break;
        }

        ssb.SetBillboardPos(m_rootCamera, pos);
    }

    protected override void Awake()
    {
    }
}
*/