/*==========================================
// Copyright (C) 2013 广州，爱游
//
// 模块名：BillboardModule
// 创建者：Matako
// 修改者列表：
// 创建日期：
// 模块描述：
//==========================================*/

using System.Collections;
using System.Collections.Generic;
using System;
using Common;
using Common.Base;
using UnityEngine;
using GameMain.GlobalManager;
using GameLoader.Utils; 
using MogoEngine.Events;

namespace ModuleBillboard
{
    public class BillboardModule : BaseModule
    {
        //BillboardView m_billboardView;

        public BillboardModule() : base()
        {
            //EventDispatcher.AddEventListener<Vector3, int, SplitBattleBillboardType>(BillboardEvents.AddSplitBillboard, AddSplitBattleBillboard);
        }

        /*
        ~BillboardModule()
        {
            EventDispatcher.RemoveEventListener<Vector3, int, SplitBattleBillboardType>(BillboardEvents.AddSplitBillboard, AddSplitBattleBillboard);
        }
         */
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pos">screen position</param>
        /// <param name="blood"></param>
        /// <param name="type"></param>
        public void AddSplitBattleBillboard(Vector3 pos, int blood, SplitBattleBillboardType type)
        {
            Debug.LogError("pos:" + pos + ",blood:" + blood + ",type:" + type);
            if (Camera.main == null)
            {
                LoggerHelper.Error("camera.main == null when AddSplitBattleBillBoard");
            }

            //Vector2 temp = new Vector2(UnityEngine.Random.Range(0f, (float)Screen.width), UnityEngine.Random.Range(0f, (float)Screen.height));
            //m_billboardView.AddSplitBattleBillboard(Camera.main.WorldToScreenPoint(pos), blood, type);
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init( )
        {
            //AddPanel(PanelIdEnum.Billboard, "Container_LifeBar", MogoUILayer.LayerUIPanel, "ModuleBillboard.BillboardView");
        }
    }
}
