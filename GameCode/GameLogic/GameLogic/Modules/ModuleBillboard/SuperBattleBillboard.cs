/*==========================================
// Copyright (C) 2013 广州，爱游
//
// 模块名：SuperBattleBillboard
// 创建者：Matako
// 修改者列表：
// 创建日期：
// 模块描述：
//==========================================*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Mogo.Util;
using UnityEngine.UI;


public class NormalPlayer : SuperBattleBillboard
{
    public NormalPlayer(int blood)
    {
        AddBattleBillboadSign(null);
        AddBattleBillboardNum(blood.ToString(), "red", 1.3f);
        PlayAnim();
    }
}

public class NormalMonster : SuperBattleBillboard
{
    public NormalMonster(int blood)
    {
        AddBattleBillboadSign(null);
        AddBattleBillboardNum(blood.ToString(), "yellow", 1.3f);
        PlayAnim();
    }
}

public class Miss : SuperBattleBillboard
{
    public Miss(int blood)
    {
        AddBattleBillboadSign("miss");
        AddBattleBillboardNum(null, null);
        PlayAnim();
    }
}

public class CriticalMonster : SuperBattleBillboard
{
    public CriticalMonster(int blood)
    {
        AddBattleBillboadSign("critical_guaiwu", 1.2f);
        AddBattleBillboardNum(blood.ToString(), "blue_", 1.2f);
        PlayAnim();
    }
}

public class CriticalPlayer : SuperBattleBillboard
{
    public CriticalPlayer(int blood)
    {
        AddBattleBillboadSign("critical_juese", 1.2f);
        AddBattleBillboardNum(blood.ToString(), "Crimson_", 1.2f);
        PlayAnim();
    }
}

public class BrokenAttack : SuperBattleBillboard
{
    public BrokenAttack(int blood)
    {
        AddBattleBillboadSign(null);
        AddBattleBillboardNum(blood.ToString(), "zi_", 1.8f);
        PlayAnim();
    }
}

public class SuperBattleBillboard
{
    protected GameObject m_goBillboard;
    protected RectTransform m_rt;
    protected GameObject m_goBillboardList;
    protected float m_fBillboardLength;

    string m_num;
    string m_numHeadImg;
    float m_numScale = 1;
    bool m_bIsLoaded = false;
    Vector3 m_vec3Pos = new Vector3(10000, 10000, 10000);
    int m_iNumCount = 0;


    protected void AddBattleBillboadSign(string imgName, float scale = 1)
    {

        if (imgName != null)
        {
            m_bIsNeedSign = true;
            //sp.sprite = imgName;
            m_sign.gameObject.SetActive(true);
        }
        else
        {
            m_bIsNeedSign = false;
            m_sign.gameObject.SetActive(false);
        }
    }

    Image m_sign;
    private List<Image> m_listSprite = new List<Image>();
    protected Dictionary<string,List<Sprite>> m_nameToSpritesDic;
    private TweenAlpha m_ta;
    private TweenScale m_ts;
    private bool m_bIsNeedSign = false;

    public int GetBillboardSpriteList()
    {
        if (m_listSprite.Count == 0)
        {
            Image[] spList = m_goBillboard.GetComponentsInChildren<Image>(true);


            for (int i = 0; i < spList.Length; ++i)
            {
                if (spList[i].name != "BattleBillboardSign")
                {
                    m_listSprite.Add(spList[i]);
                }
                else
                {
                    m_sign = spList[i];
                }
            }
        }

        return m_listSprite.Count;
    }

    protected void AddBattleBillboardNum(string num, string imgName, float scale = 1)
    {
        if (m_bIsLoaded)
        {
            int i = 0;
            for ( i = 0; i < num.Length && i < m_listSprite.Count - 1; i++)
            {
                m_listSprite[i].gameObject.SetActive(true);
                m_listSprite[i].sprite = m_nameToSpritesDic[imgName][int.Parse(num[i].ToString())];
            }
            for (; i < m_listSprite.Count; i++)
            {
                m_listSprite[i].gameObject.SetActive(false);
            }
        }
        else
        {
            m_num = num;
            m_numHeadImg = imgName;
            m_numScale = scale;
        }
    }

    public void PlayAnim()
    {
        m_ta.Reset();
        m_ts.Reset();

        m_ta.enabled = true;
        m_ts.enabled = true;

        m_ta.Play(true);
        m_ts.Play(true);
    }


    public SuperBattleBillboard()
    {
    }

    public void SetBillboardPos(Camera c, Vector2 pos)
    {
        if (m_goBillboard != null)
        {
            m_goBillboard.transform.position = c.ScreenToWorldPoint(pos);
            m_goBillboard.transform.position = new Vector3(m_goBillboard.transform.position.x, m_goBillboard.transform.position.y, 0);

        }
        else
        {
            m_vec3Pos = pos;
        }
    }
}
