﻿#region 模块信息
/*==========================================
// 文件名：ReconnectPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleReconnect
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/23 10:17:19
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using UnityEngine;
namespace ModuleReconnect
{
    public class ReconnectPanel : BasePanel
    {
        private StateText _descTxt;
        private StateImage _image;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Reconnect; }
        }

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("Container_bg/ScaleImage_sharedZhezhao");
            KContainer _container = GetChildComponent<KContainer>("Container_duanxianchonglianzhong");
            _descTxt = _container.GetChildComponent<StateText>("Label_txtjiazaizhong");
            _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(103024);
            _descTxt.CurrentText.alignment = TextAnchor.UpperCenter;
            _image = _container.GetChildComponent<StateImage>("Image_jiazai");
            _image.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 1.0f / 3.0f);
            _image.GetComponent<RectTransform>().localPosition = new Vector2(22.5f, -18.0f);
            _image.Visible = false;
        }

        public override void OnShow(object data)
        {
            _image.Visible = true;
        }

        public void Update()
        {
            RotateImage();
        }

        private void RotateImage()
        {
            _image.transform.Rotate(0, 0, -360 * Time.deltaTime, Space.Self);
        }

        public override void OnClose()
        {
        }
    }

}