﻿#region 模块信息
/*==========================================
// 文件名：ReconnnectModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleReconnect
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/23 10:20:21
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
namespace ModuleReconnect
{
    public class ReconnectModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.Reconnect, "Container_ReconnectingPanel", MogoUILayer.LayerUITop, "ModuleReconnect.ReconnectPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }
    }

}