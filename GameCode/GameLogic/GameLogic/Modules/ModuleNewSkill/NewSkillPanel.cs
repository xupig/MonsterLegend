﻿#region 模块信息
/*==========================================
// 文件名：NewSkillView
// 命名空间: GameLogic.GameLogic.Modules.ModuleNewSkill
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/13 10:54:13
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ClientConfig;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleNewSkill
{
    public class NewSkillPanel : BasePanel
    {

        private const int LEARN_SKILL = 1;
        private const int PROFICIENT_SKILL = 2;

        private CategoryList _categoryList;
       
        private LearnSkillView _learnSkillView;
        private ProficientSkillView _proficientSkillView;
        private StateImage _imgBg;

        protected override void Awake()
        {
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            _learnSkillView = AddChildComponent<LearnSkillView>("Container_jinengxuexi");
            _proficientSkillView = AddChildComponent<ProficientSkillView>("Container_jinengfenzhi");

            _imgBg = GetChildComponent<StateImage>("ScaleImage_sharedKuangDT02");
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Skill; }
        }

        private void SetCategoryList()
        {
            int[] categoryNameList = new int[] { 71431, 71433 };
            List<int> categoryList = new List<int>() { 3297, 3299 };
            List<CategoryItemData> dataList = new List<CategoryItemData>();
            for (int i = 0; i < categoryList.Count; i++)
            {
                if (function_helper.IsFunctionOpen(categoryList[i]))
                {
                    dataList.Add(CategoryItemData.GetCategoryItemData(categoryNameList[i].ToLanguage()));
                }
            }
            _categoryList.RemoveAll();
            _categoryList.SetDataList<CategoryListItem>(dataList);
        }

        public override void OnShow(object data)
        {
            SetCategoryList();
            EventDispatcher.AddEventListener(SpellEvents.SPELL_OPERATION_CHANGE, OnSpellOperationChnage);
            if(data!=null)
            {
                if(data is int)
                {
                    int index = int.Parse(data.ToString()) - 1;
                    if (index >= 0)
                    {
                        _categoryList.SelectedIndex = index;
                        OnSelectedIndexChanged(_categoryList, index);
                    }
                    else
                    {
                        _categoryList.SelectedIndex = LEARN_SKILL - 1;
                        OnSelectedIndexChanged(_categoryList, LEARN_SKILL - 1);
                    }
                }
                else
                {
                    int[] list = data as int[];
                    _categoryList.SelectedIndex = list[1] - 1;
                    
                    OnSelected(list[1]-1, list);
                }
            }
            else
            {
                _categoryList.SelectedIndex = LEARN_SKILL - 1;
                OnSelectedIndexChanged(_categoryList, LEARN_SKILL - 1);
            }
            _categoryList.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            OnSpellOperationChnage();
        }

        public override void OnClose()
        {
            if(_categoryList!=null)
            {
                _categoryList.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
                _learnSkillView.OnClose();
                _proficientSkillView.OnClose();
            }
            EventDispatcher.RemoveEventListener(SpellEvents.SPELL_OPERATION_CHANGE, OnSpellOperationChnage);
        }

        private void OnSpellOperationChnage()
        {
            List<KList.KListItemBase> itemList = _categoryList.ItemList;
            for (int i = 0; i < itemList.Count;i++ )
            {
                CategoryListItem item = itemList[i] as CategoryListItem;
                if(i == 0)
                {
                    item.SetPoint(PlayerDataManager.Instance.SpellData.hasSkillCanUpgradeDict.Count > 0);
                }
                else if(i == 1)
                {
                    item.SetPoint(false);
                }
                else
                {
                    item.SetPoint(PlayerDataManager.Instance.SpellData.isSkillProficientUnLock == 1);
                }
            }
               
            _learnSkillView.UpdateSpellOperation();
        }

        private void OnSelected(int index,object tab)
        {
            switch (index + 1)
            {
                case LEARN_SKILL:
                    _learnSkillView.OnShow(tab);
                    _proficientSkillView.OnClose();
                    _imgBg.Visible = true;
                    break;
                case PROFICIENT_SKILL:
                    PlayerDataManager.Instance.SpellData.isSkillProficientUnLock = 0;
                    _learnSkillView.OnClose();
                    _proficientSkillView.OnShow();
                    _imgBg.Visible = true;
                    break;
                default:
                    break;
            }
        }

        private void OnSelectedIndexChanged(CategoryList list,int index)
        {
            OnSelected(index,0);
        }
    }
}
