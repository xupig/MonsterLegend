﻿#region 模块信息
/*==========================================
// 文件名：NewSkillItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleNewSkill.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/14 21:08:32
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleNewSkill
{
    public class NewSkillItem : KContainer
    {
        private RectTransform _rect;
        private StateIcon _icon;
        private spell spell;
        private KToggle _toggle;

        public bool isProficient;

        public spell Data
        {
            get { return spell; }
        }

        protected override void Awake()
        {
            _toggle = GetComponent<KToggle>();
            _rect = GetComponent<RectTransform>();
            _icon = GetChildComponent<StateIcon>("jinengi/stateIcon");
        }

        public void OnShow()
        {
            Visible = true;
            _toggle.onValueChanged.RemoveListener(OnClickToggle);
            _toggle.onValueChanged.AddListener(OnClickToggle);
        }

        public void OnClose()
        {
            Visible = false;
            _toggle.onValueChanged.RemoveListener(OnClickToggle);
        }

        private void OnClickToggle(KToggle toggle, bool isOn)
        {
            if (spell != null)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.SkillToolTips, new object[] { spell, !isProficient }, PanelIdEnum.Information);
            }
        }

        public void SetData(spell_sys sys)
        {
            spell = PlayerDataManager.Instance.SpellData.GetPositionSpell(sys);
            _icon.SetIcon(spell.__icon);

        }

        public void UpdateSpell(spell_sys sys)
        {
            spell = PlayerDataManager.Instance.SpellData.GetPositionSpell(sys);
        }

        public float StarPlayTime = 0.15f;
        public float StarScale = 3f;
      
        private void OnScaleFinish(UITweener tween)
        {
            tween = null;
        }

        public void SetData(spell spell)
        {
            if (spell == null)
            {
                _icon.Visible = false;
                this.spell = null;
            }
            else
            {
                spell_sys sys = skill_helper.GetSpellSysyByGroupId(spell.__group);
                _icon.Visible = true;
                this.spell = spell;
                _icon.SetIcon(spell.__icon);
            }
        }

        public Vector3 Position
        {
            get { return _rect.anchoredPosition3D; }
            set { _rect.anchoredPosition3D = value; }
        }

    }
}
