﻿#region 模块信息
/*==========================================
// 文件名：EquipSkillItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleNewSkill.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/15 11:37:17
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleNewSkill
{
    public class EquipSkillItem:KList.KListItemBase
    {

        private EquipSkillInfoItem _one;
        private KButton _btnOneReplace;

        private StateText _txtOneReplace;

        private KContainer _two;
        private KButton _btnTwoReplace;
        private StateText _txtTwoReplace;

        private EquipSkillInfoItem _twoJunior;
        private EquipSkillInfoItem _twoSenior;

        private spell _spell;

        private RectTransform _rect;

        private RectTransform _oneRect;
        private RectTransform _twoRect;

        protected override void Awake()
        {
            _one = AddChildComponent<EquipSkillInfoItem>("Container_one");
            _btnOneReplace = _one.GetChildComponent<KButton>("Button_tihuan");

            _txtOneReplace = _btnOneReplace.GetChildComponent<StateText>("label");
            
            _two = GetChildComponent<KContainer>("Container_two");
            _twoJunior = _two.AddChildComponent<EquipSkillInfoItem>("Container_junior"); 
            _twoSenior = _two.AddChildComponent<EquipSkillInfoItem>("Container_seinor");
            _btnTwoReplace = _two.GetChildComponent<KButton>("Button_tihuan");
            _txtTwoReplace = _btnTwoReplace.GetChildComponent<StateText>("label");

            _oneRect = _one.GetComponent<RectTransform>();
            _twoRect = _two.GetComponent<RectTransform>();
            Height = _twoRect.sizeDelta.y;

            _rect = GetComponent<RectTransform>();
            _btnOneReplace.onClick.AddListener(OnClickReplace);
            _btnTwoReplace.onClick.AddListener(OnClickReplace);
        }

        private void OnClickReplace()
        {
            if (PlayerDataManager.Instance.SpellData.GetSlotSpell(_spell.__pos) == null)
            {
                SpellManager.Instance.SpellEquip(_spell.__id);
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.SkillToolTips, new object[]{_spell,true}, PanelIdEnum.Information);
            }
        }

        public Vector3 Position
        {
            get { return _rect.anchoredPosition3D; }
            set { _rect.anchoredPosition3D = value; }
        }


        public float Height
        {
            get;
            set;
        }

        public override void Dispose()
        {
            _btnOneReplace.onClick.RemoveListener(OnClickReplace);
            _btnTwoReplace.onClick.RemoveListener(OnClickReplace);
        }

        public override object Data
        {
            get
            {
                return _spell;
            }
            set
            {
                _spell = value as spell;
                Refresh();
            }
        }

        private void SetBtnState(StateText txt)
        {
            if(PlayerDataManager.Instance.SpellData.GetSlotSpell(_spell.__pos) == null)
            {
                 txt.ChangeAllStateText((71315).ToLanguage());//装备
            }
            else
            {
                txt.ChangeAllStateText((71316).ToLanguage());//替换
            }
        }

        private void Refresh()
        {
            spell_sys sys = skill_helper.GetSpellSysyByGroupId(_spell.__group);
            spell spell = PlayerDataManager.Instance.SpellData.GetPositionSpell(sys);
            spell_sys nextSys = null;
            spell nextSpell = null;
            if(sys.__position%2 == 1)
            {
                nextSys = skill_helper.GetSpellSysByPosition(PlayerAvatar.Player.vocation.ToInt(), sys.__proficient_group, sys.__position + 1);
                if (nextSys!=null)
                {
                    nextSpell = PlayerDataManager.Instance.SpellData.GetPositionSpell(nextSys);
                }
            }

            if(nextSys!=null)
            {
                _one.Visible = false;
                _two.Visible = true;
                _twoJunior.SetData(spell);
                _twoSenior.SetData(nextSpell);
                Height = -_twoRect.sizeDelta.y;
                _txtTwoReplace.ChangeAllStateText((71315).ToLanguage());
                SetBtnState(_txtTwoReplace);
            }
            else
            {
                _one.Visible = true;
                _two.Visible = false;
                _one.SetData(spell);
                Height = -_oneRect.sizeDelta.y;
                SetBtnState(_txtOneReplace);
            }
           
        }
    }
}
