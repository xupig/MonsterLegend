﻿#region 模块信息
/*==========================================
// 文件名：SkillEffectItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleNewSkill.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/13 16:45:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleNewSkill
{
    public class SkillEffectItem:KContainer
    {
        private StateText _txtTitle;
        private StateText _txtEffect;
        private static string effectTemplate;

        private RectTransform _rect;
        private RectTransform _txtRect;

        protected override void Awake()
        {
            _txtTitle = GetChildComponent<StateText>("Label_txtTitle");
            _txtEffect = GetChildComponent<StateText>("Label_txtXiaoguo1");
            if (effectTemplate == null)
            {
                effectTemplate = _txtEffect.CurrentText.text;
            }
            _rect = GetComponent<RectTransform>();
            _txtRect = _txtEffect.GetComponent<RectTransform>();
            base.Awake();
        }


        public void SetData(spell spell)
        {
            int[] attri = skill_helper.GetEffectAttri(spell);
            _txtEffect.CurrentText.text = (71343).ToLanguage(attri[0], attri[1].ToLanguage(), attri[2]);
        }


        public float Y
        {
            set { _rect.anchoredPosition3D = new Vector3(_rect.anchoredPosition3D.x, value, _rect.anchoredPosition3D.z); }
            get { return _rect.anchoredPosition3D.y; }
        }

        public float Height
        {
            get
            {
                return _txtRect.anchoredPosition3D.y - _txtEffect.CurrentText.preferredHeight;
            }
        }

    }
}
