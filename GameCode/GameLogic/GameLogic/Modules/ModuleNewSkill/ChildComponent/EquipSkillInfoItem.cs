﻿#region 模块信息
/*==========================================
// 文件名：EquipSkillInfoItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleNewSkill.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/15 14:26:25
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleNewSkill
{
    public class EquipSkillInfoItem:KContainer
    {
        private StateIcon _stateIcon;
        private StateText _txtName;
        private StateText _txtLevel;
        private StateText _txtType;
        private StateText _txtEffect;

        private StateText _txtCd;
        private StateText _txtOutput;
       // private StateImage _imgProficient;

        private static string levelTemplate;
        private static string typeTemplate;
        private static string effectTemplate;
        private static string cdTemplate;
        private static string outputTemplate;

        protected override void Awake()
        {
            _txtCd = GetChildComponent<StateText>("Label_txtCD");
            _txtOutput = GetChildComponent<StateText>("Label_txtNengliangshuchu");

            if(cdTemplate == null)
            {
                cdTemplate = _txtCd.CurrentText.text + "s";
                outputTemplate = _txtOutput.CurrentText.text;
            }

            _stateIcon = GetChildComponent<StateIcon>("Button_jinengi/stateIcon");
            _txtName = GetChildComponent<StateText>("Label_txtJinengmingcheng");
            _txtLevel = GetChildComponent<StateText>("Label_txtDengji");
            if (levelTemplate == null)
            {
                levelTemplate = _txtLevel.CurrentText.text;
            }
            _txtType = GetChildComponent<StateText>("Label_txtLeixing");
            if (typeTemplate == null)
            {
                typeTemplate = _txtType.CurrentText.text;
            }
            _txtEffect = GetChildComponent<StateText>("Label_txtDangqianxiaoguoNR");
            if (effectTemplate == null)
            {
                effectTemplate = _txtEffect.CurrentText.text;
            }
           // _imgProficient = GetChildComponent<StateImage>("Image_zhuanjing");
        }

        public void SetData(spell spell)
        {
            _stateIcon.SetIcon(spell.__icon);
            _txtName.CurrentText.text = spell.__name.ToLanguage();
            spell_sys sys = skill_helper.GetSpellSysyByGroupId(spell.__group);
            if (PlayerDataManager.Instance.SpellData.HasSkillLearned(sys.__id))
            {
                _txtLevel.CurrentText.text = string.Format(levelTemplate, spell.__sp_level);
            }
            else
            {
                _txtLevel.CurrentText.text = string.Format(levelTemplate, 0);
            }            
            _txtType.CurrentText.text = string.Format(typeTemplate,skill_helper.GetPosName(spell.__pos).ToLanguage());
             int[] attri = skill_helper.GetEffectAttri(spell);
            _txtEffect.CurrentText.text = string.Format(effectTemplate, (71343).ToLanguage(attri[0], attri[1].ToLanguage(), attri[2]));
           
            //_imgProficient.Visible = PlayerDataManager.Instance.SpellData.HasProficient(sys.__proficient_group);
            _txtCd.CurrentText.text = (71882).ToLanguage(skill_helper.GetCd(spell));
            _txtOutput.CurrentText.text = (71881).ToLanguage(skill_helper.GetOutput(spell));
        }

    }
}
