﻿#region 模块信息
/*==========================================
// 文件名：LearnSkillItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleNewSkill.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/13 14:59:40
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace ModuleNewSkill
{
    public class LearnSkillItem : KContainer
    {
        private KButton _btnUpgrade;
        private KToggle _toggle;
        public KComponentEvent<spell_sys, bool> onClick = new KComponentEvent<spell_sys, bool>();
        private spell_sys _sys;
        private StateIcon _icon;

        private KParticle _canUpgrade;
        private KParticle _upgrade;

        public spell_sys Data
        {
            get { return _sys; }
        }

        public void RegisterToggle(ToggleGroup toggleGroup)
        {
            _toggle.group = toggleGroup;
            toggleGroup.RegisterToggle(_toggle);
        }

        public void SetSelected(bool isOn)
        {
            _toggle.isOn = isOn;
        }

        protected override void Awake()
        {
            _btnUpgrade = GetChildComponent<KButton>("Button_shengji");
            _toggle = GetChildComponent<KToggle>("Toggle_jineng");
            _icon = _toggle.GetChildComponent<StateIcon>("jinengi/stateIcon");
            _canUpgrade = _toggle.AddChildComponent<KParticle>("fx_ui_5_1_xuanzhuanguangquan");
            _canUpgrade.Stop();
            _upgrade = _toggle.AddChildComponent<KParticle>("fx_ui_5_2_shengji");
            _upgrade.Stop();
            base.Awake();
        }

        public void OnShow()
        {
            _upgrade.Stop();
            Visible = true;
        }

        protected override void OnEnable()
        {
            if (_btnUpgrade != null)
            {
                _btnUpgrade.onClick.AddListener(OnClickUpgrade);
            }
            if (_toggle != null)
            {
                _toggle.onValueChanged.AddListener(OnValueChanged);
            }
            base.OnEnable();
        }

        public void SetData(spell_sys spellSys)
        {
            _canUpgrade.Stop();

            _sys = spellSys;
            spell spell = PlayerDataManager.Instance.SpellData.GetPositionSpell(_sys);
            bool isFullLevel = spell.__sp_level >= skill_helper.GetSpellMaxLevel(spell.__group);
            _icon.SetIcon(spell.__icon);
            spell nextSpell = skill_helper.GetSpell(spell.__group, spell.__sp_level + 1);
            if (isFullLevel == false)
            {
                if (PlayerAvatar.Player.CheckLimit<string>(nextSpell.__study_cnd) == LimitEnum.NO_LIMIT && PlayerAvatar.Player.CheckCostLimit(nextSpell.__study_costs, false) == 0)
                {
                    _btnUpgrade.Visible = true;
                    if (_canUpgrade.Visible == false)
                    {
                        _canUpgrade.Play(true);
                    }
                }
                else
                {
                    _btnUpgrade.Visible = false;
                    _canUpgrade.Stop();
                }
            }
            else
            {
                _btnUpgrade.Visible = false;
                _canUpgrade.Stop();
            }
        }

        public bool CanUpgrade
        {
            get { return _btnUpgrade.Visible; }
        }

        public void OnClose()
        {
            Visible = false;
        }

        protected override void OnDisable()
        {
            if (_btnUpgrade != null)
            {
                _btnUpgrade.onClick.RemoveListener(OnClickUpgrade);
            }
            if (_toggle != null)
            {
                _toggle.onValueChanged.RemoveListener(OnValueChanged);
            }
            base.OnDisable();
        }

        private void OnClickUpgrade()
        {

        }

        private void OnValueChanged(KToggle toggle, bool isOn)
        {
            if (isOn)
            {
                onClick.Invoke(_sys, CanUpgrade);
            }
        }


        public void Upgrade(spell spell, bool hasLearn)
        {
            spell nextSpell = skill_helper.GetSpell(spell.__group, spell.__sp_level + 1);
            if (PlayerAvatar.Player.CheckCostLimit(nextSpell.__study_costs, true) == 0)
            {
                LimitEnum result = PlayerAvatar.Player.CheckLimit<string>(nextSpell.__study_cnd);
                if (hasLearn == false)
                {
                    if (result == LimitEnum.NO_LIMIT)
                    {
                        _upgrade.Play();
                        UIManager.Instance.PlayAudio("Sound/UI/skill_update.mp3");
                        SpellManager.Instance.SpellLearn(spell.__id);
                    }
                    if (result == LimitEnum.LIMIT_LEVEL)
                    {
                        ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, (71324).ToLanguage(nextSpell.__study_cnd[((int)LimitEnum.LIMIT_LEVEL).ToString()]), PanelIdEnum.MainUIField);
                    }
                    else if (result == LimitEnum.LIMIT_TASK)
                    {
                        task_data data = task_data_helper.GetTaskData(int.Parse(nextSpell.__study_cnd[((int)LimitEnum.LIMIT_TASK).ToString()]));
                        if (data.__task_type == public_config.TASK_TYPE_MAIN)
                        {
                            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, (71326).ToLanguage((71332).ToLanguage(), data.__task_name.ToLanguage()), PanelIdEnum.MainUIField);
                        }
                        else
                        {
                            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, (71326).ToLanguage((71332).ToLanguage(), data.__task_name.ToLanguage()), PanelIdEnum.MainUIField);
                        }
                    }
                }
                else
                {
                    if (result == LimitEnum.NO_LIMIT)
                    {
                        _upgrade.Play();
                        UIManager.Instance.PlayAudio("Sound/UI/skill_update.mp3");
                        SpellManager.Instance.SpellUpgrade(spell.__id);
                    }
                    if (result == LimitEnum.LIMIT_LEVEL)
                    {
                        ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, (71325).ToLanguage(nextSpell.__study_cnd[((int)LimitEnum.LIMIT_LEVEL).ToString()]), PanelIdEnum.MainUIField);
                    }
                    else if (result == LimitEnum.LIMIT_TASK)
                    {
                        task_data data = task_data_helper.GetTaskData(int.Parse(nextSpell.__study_cnd[((int)LimitEnum.LIMIT_TASK).ToString()]));
                        if (data.__task_type == public_config.TASK_TYPE_MAIN)
                        {
                            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, (71327).ToLanguage((71332).ToLanguage(), data.__task_name.ToLanguage()), PanelIdEnum.MainUIField);

                        }
                        else
                        {
                            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, (71327).ToLanguage((71332).ToLanguage(), data.__task_name.ToLanguage()), PanelIdEnum.MainUIField);
                        }

                    }

                }
            }
        }

    }
}
