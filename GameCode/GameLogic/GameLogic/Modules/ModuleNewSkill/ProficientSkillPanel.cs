﻿using Common.Base;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleNewSkill
{
    public class ProficientSkillPanel : BasePanel
    {
        public string eventName = "PLAY_CG";
        public string eventValue = "199901";

        private ProficientSkillView _proficientSkillView;

        protected override void Awake()
        {
            base.Awake();
            CloseBtn = GetChildComponent<KButton>("Container_jineng/Button_close");
            _proficientSkillView = AddChildComponent<ProficientSkillView>("Container_jineng/Container_jinengfenzhi");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.ProficientSkill; }
        }

        public override void OnShow(object data)
        {
            _proficientSkillView.OnShow();
            DramaManager.GetInstance().Trigger(eventName, eventValue);
        }

        public override void OnClose()
        {
            _proficientSkillView.OnClose();
        }


    }
}
