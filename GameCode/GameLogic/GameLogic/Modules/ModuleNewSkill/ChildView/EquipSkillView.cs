﻿#region 模块信息
/*==========================================
// 文件名：EquipSkillView
// 命名空间: GameLogic.GameLogic.Modules.ModuleNewSkill.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/13 10:55:49
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleNewSkill
{
    public class EquipSkillView:KContainer
    {

        private StateText _txt;
        private KScrollView _scrollView;
        private KPageableList _pageableList;
        private KToggleGroup _toggleGroup;
        private List<NewSkillItem> _skillList;
        private List<spell> spellList;
        private StateText _txtNormalSpell;
        private KContainer _slotContainer;

        protected override void Awake()
        {
            _slotContainer = GetChildComponent<KContainer>("Container_slot");
            _txtNormalSpell = _slotContainer.GetChildComponent<StateText>("Label_txtBuketihuan");
            _txtNormalSpell.CurrentText.text = (71877).ToLanguage();
            _scrollView = GetChildComponent<KScrollView>("ScrollView_jinengtihuan");
            _txt = GetChildComponent<StateText>("Label_txtDangqianmeiyouketihuanjineng");
            _pageableList = _scrollView.GetChildComponent<KPageableList>("mask/content");
            _pageableList.SetDirection(KList.KListDirection.TopToDown, 1,5);
            _pageableList.SetGap(10, 0);
            _toggleGroup = _slotContainer.GetChildComponent<KToggleGroup>("ToggleGroup_jinengcao");
            spellList = new List<spell>();
            _skillList = new List<NewSkillItem>();
            for (int i = 0; i < 4;i++ )
            {
                NewSkillItem item = _toggleGroup.AddChildComponent<NewSkillItem>("Toggle_jineng"+i);
                _skillList.Add(item);
            }

            base.Awake();
        }

        public void OnShow()
        {
            Visible = true;
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            EventDispatcher.AddEventListener(SpellEvents.SPELL_UPDATE_EQUIPED_SKILL, OnSpellUpdateEquipedSkill);
            UpdateEquipedSkill();
            TweenViewMove.Begin(_slotContainer.gameObject, MoveType.Show, MoveDirection.Right);
        }

        private void OnSpellUpdateEquipedSkill()
        {
            if(_toggleGroup!=null)
            {
                SpellData spellData = PlayerDataManager.Instance.SpellData;
                for (int i = 0; i < _skillList.Count; i++)
                {
                    spell spell = spellData.GetSlotSpell(i + 1);
                    _skillList[i].SetData(spell);
                }
                OnSelectedIndexChanged(_toggleGroup, _toggleGroup.SelectIndex);
            }
        }

        private void UpdateEquipedSkill()
        {
            SpellData spellData = PlayerDataManager.Instance.SpellData;
            for (int i = 0; i < _skillList.Count; i++)
            {
                spell spell = spellData.GetSlotSpell(i + 1);
                _skillList[i].SetData(spell);
            }
            _toggleGroup.SelectIndex = 3;
            OnSelectedIndexChanged(_toggleGroup,3);
        }

        public void OnClose()
        {
            Visible = false;
            if(_toggleGroup!=null)
            {
                _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            }
            EventDispatcher.RemoveEventListener(SpellEvents.SPELL_UPDATE_EQUIPED_SKILL, OnSpellUpdateEquipedSkill);
        }

        private void OnSelectedIndexChanged(KToggleGroup toggleGroup,int index)
        {
            SpellData spellData = PlayerDataManager.Instance.SpellData;
            Dictionary<int, Dictionary<int,int>> spellDict = spellData.SpellDict;
            spellList.Clear();
            int slotedSpellId = _skillList[index].Data != null ? _skillList[index].Data.__id : 0;
            foreach (KeyValuePair<int, Dictionary<int, int>> kk in spellDict)
            {
                foreach (KeyValuePair<int, int> kvp in kk.Value)
                {
                    spell spell = spellData.GetSpell(kvp.Value);
                    spell_sys spellSys = skill_helper.GetSpellSysyByGroupId(spell.__group);
                    
                    if (spell.__pos == index + 1 && spell.__id != slotedSpellId && spellSys.__is_equip == 1)
                    {
                        spellList.Add(spell);
                    }
                }
            }
            _pageableList.SetDataList<EquipSkillItem>(spellList);
            _txt.Visible = spellList.Count == 0;
            float offset = 0;
            Vector3 vect = Vector3.zero;
            List<KList.KListItemBase> itemList = _pageableList.ItemList;
            for(int i=0;i<itemList.Count;i++)
            {
                EquipSkillItem item = itemList[i] as EquipSkillItem;
                vect.x = item.Position.x;
                vect.y =  offset;
                item.Position = vect;
                offset += -10 + item.Height;
            }
            _scrollView.ScrollRect.content.sizeDelta = new Vector2(_scrollView.ScrollRect.content.sizeDelta.x, -offset);
            _scrollView.ResetContentPosition();
            TweenListEntry.Begin(_pageableList.gameObject, TweenListEntryMoveDirection.LeftToRight);
        }

    }
}
