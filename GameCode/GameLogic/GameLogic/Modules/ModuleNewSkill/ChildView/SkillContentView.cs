﻿#region 模块信息
/*==========================================
// 文件名：SkillContentView
// 命名空间: GameLogic.GameLogic.Modules.ModuleNewSkill.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/13 16:40:27
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleNewSkill
{
    public class SkillContentView:KContainer
    {
        private StateText _txtName;
        private StateText _txtDescription;
        private RectTransform _otherRect;
        private RectTransform _descriptionRect;
        //private StateImage _imgProficient;
        private StateText _txtType;
        private StateText _txtLevel;
        private SkillEffectItem _currentEffect;
        private SkillEffectItem _nextEffect;
        private KContainer _upgradeNeed;
        private StateText _txtNeed1;
        private StateText _txtNeed2;
        private IconContainer _iconContainer;
        private RectTransform _rect;
        private StateText _txtUpgrade;

        private StateText _txtCd;
        private StateText _txtOutput;

       // private string levelTemplate;
        private string posTemplate;
        private string upgradeTemplate;
        private string needTemplate;

        private string cdTemplate;
        private string outputTemplate;

        private KScrollView _scrollView;
        
        protected override void Awake()
        {
            _txtName = GetChildComponent<StateText>("Label_txtJinengmingcheng");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_jinengxinxi");
            KContainer container = _scrollView.GetChildComponent<KContainer>("mask/content");
            
            _txtDescription = container.GetChildComponent<StateText>("Label_txtMiaoshu");
            _descriptionRect = _txtDescription.GetComponent<RectTransform>();
            KContainer other = container.GetChildComponent<KContainer>("Container_other");
            _otherRect = container.GetChildComponent<RectTransform>("Container_other");
            _txtCd = other.GetChildComponent<StateText>("Label_txtCd");
            _txtOutput = other.GetChildComponent<StateText>("Label_txtNengliangshuchu");

            cdTemplate = _txtCd.CurrentText.text+"s";
            outputTemplate = _txtOutput.CurrentText.text;
            //_imgProficient = GetChildComponent<StateImage>("Image_zhuanjing");
            _txtType = other.GetChildComponent<StateText>("Label_txtLeixing");
            posTemplate = _txtType.CurrentText.text;
            _txtLevel = GetChildComponent<StateText>("Label_txtDengji");
            _currentEffect = other.AddChildComponent<SkillEffectItem>("Container_currentEffect");
            _nextEffect = other.AddChildComponent<SkillEffectItem>("Container_nextEffect");
            _upgradeNeed = other.GetChildComponent<KContainer>("Container_upgradeNeed");
            _txtNeed1 = _upgradeNeed.GetChildComponent<StateText>("Label_txtReneudengji");
            _txtNeed2 = _upgradeNeed.GetChildComponent<StateText>("Label_txtGold");
            _iconContainer = _upgradeNeed.AddChildComponent<IconContainer>("Container_icon");
            _iconContainer.SetIcon( item_helper.GetIcon(public_config.MONEY_TYPE_GOLD));
            _txtUpgrade = _upgradeNeed.GetChildComponent<StateText>("Label_txtShengjixuyao");
            upgradeTemplate = _txtUpgrade.CurrentText.text;
            needTemplate = _txtNeed1.CurrentText.text;
            _rect = _upgradeNeed.GetComponent<RectTransform>();
            base.Awake();
        }

       

        public void SetData(spell spell, bool isFullLevel,bool hasLearned)
        {
            int posName = skill_helper.GetPosName(spell.__pos);
            _txtName.CurrentText.text = spell.__name.ToLanguage();
            _txtDescription.CurrentText.text = (6013028).ToLanguage(spell.__desc.ToLanguage());
            _otherRect.anchoredPosition = new Vector2(_otherRect.anchoredPosition.x, _descriptionRect.anchoredPosition.y - _txtDescription.CurrentText.preferredHeight - 12);
            _txtCd.CurrentText.text = (71882).ToLanguage(skill_helper.GetCd(spell));
            _txtOutput.CurrentText.text = (71881).ToLanguage(skill_helper.GetOutput(spell));
            _txtLevel.CurrentText.text = string.Format("LV{0}", hasLearned ? spell.__sp_level : 0);
            _txtType.CurrentText.text = string.Format(posTemplate,posName.ToLanguage());
            spell_sys sys = skill_helper.GetSpellSysyByGroupId(spell.__group);
            //_imgProficient.Visible = PlayerDataManager.Instance.SpellData.HasProficient(sys.__proficient_group);
             _currentEffect.SetData(spell);
            if (!isFullLevel)
            {
                spell nextSpell = skill_helper.GetSpell(spell.__group, spell.__sp_level + 1);
                _nextEffect.SetData(nextSpell);
                _nextEffect.Y = _currentEffect.Y + _currentEffect.Height - 2f;
                
                _nextEffect.Visible = true;
                _rect.anchoredPosition3D = new Vector3(_rect.anchoredPosition3D.x,_nextEffect.Y+_nextEffect.Height-2f,_rect.anchoredPosition3D.z);
                _txtUpgrade.CurrentText.text = string.Format(upgradeTemplate, (71309).ToLanguage());
                _iconContainer.Visible = true;
                _txtNeed1.Visible = true;
                _txtNeed2.Visible = true;


                _txtNeed1.CurrentText.text = string.Format(needTemplate, GetLevelLimit(nextSpell), GetTaskLimit(nextSpell));
                string cost = "0";
                if (nextSpell.__study_costs.ContainsKey(public_config.MONEY_TYPE_GOLD.ToString()))
                {
                    cost = nextSpell.__study_costs[public_config.MONEY_TYPE_GOLD.ToString()];
                }
                if (PlayerAvatar.Player.money_gold >= int.Parse(cost))
                {
                    _txtNeed2.CurrentText.text = (32019).ToLanguage(cost);
                }
                else
                {
                    _txtNeed2.CurrentText.text = (32022).ToLanguage(cost);
                }

            }
            else
            {
                _nextEffect.Visible = false;
                _rect.anchoredPosition3D = new Vector3(_rect.anchoredPosition3D.x, _currentEffect.Y + _currentEffect.Height - 2f, _rect.anchoredPosition3D.z);
                _txtUpgrade.CurrentText.text = (71311).ToLanguage();//技能等级已达上限
                _iconContainer.Visible = false;
                _txtNeed1.Visible = false;
                _txtNeed2.Visible = false;
            }
            RectTransform rect = (_scrollView.Content.transform as RectTransform);
            _scrollView.ScrollRect.verticalNormalizedPosition = 0;
            rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, 0);
            rect.sizeDelta = new Vector2(rect.sizeDelta.x, _otherRect.sizeDelta.y - _otherRect.anchoredPosition.y);
        }

         private string GetLevelLimit(spell spell)
         {
             if (spell.__study_cnd.ContainsKey(LimitEnum.LIMIT_LEVEL.ToIntString()))
             {
                 int value = int.Parse(spell.__study_cnd[LimitEnum.LIMIT_LEVEL.ToIntString()]);
                 int lang = PlayerAvatar.Player.level >= value ? 71330 : 71312;
                 return lang.ToLanguage(value);
             }
             return string.Empty;
         }

         private string GetTaskLimit(spell spell)
         {
             if (spell.__study_cnd.ContainsKey(LimitEnum.LIMIT_TASK.ToIntString()))
             {
                 int value = int.Parse(spell.__study_cnd[LimitEnum.LIMIT_TASK.ToIntString()]);
                 int lang = PlayerDataManager.Instance.TaskData.IsCompleted(value) ? 71331 : 71313;
                 return lang.ToLanguage(value);
             }
             return string.Empty;
         }
    }
}
