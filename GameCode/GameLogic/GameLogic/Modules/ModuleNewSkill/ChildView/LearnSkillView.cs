﻿#region 模块信息
/*==========================================
// 文件名：LearnSkillView
// 命名空间: GameLogic.GameLogic.Modules.ModuleNewSkill.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/13 10:55:04
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace ModuleNewSkill
{
    public class LearnSkillView:KContainer
    {
        private KToggleGroup _toggleGroup;
        private SkillContentView _skillContentView;
        private KButton _btnPreview;
        private KButton _btnLearn;
        private StateText _txtLearnName;
        private KToggleGroup _toggleRegister;
        private List<LearnSkillItem> _skillItemList;
        private List<StateText> checkmarkList;
        private List<StateText> backList;
        private List<StateImage> checkmarkImgList;
        private List<StateImage> backImgList;

        private KParticle _particle;

        private Dictionary<int, spell_proficient> dict;
        private spell spell;

        private bool hasLearn;
        private bool isFullLevel;

        private static int selectedIndex = -1;

        protected override void Awake()
        {
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_jinengxuexi");
            _skillContentView = AddChildComponent<SkillContentView>("Container_jinengxinxi");
            _btnPreview = GetChildComponent<KButton>("Button_jinengyulani");
            _btnLearn = GetChildComponent<KButton>("Button_xuexi");
            _particle = _btnLearn.AddChildComponent<KParticle>("fx_ui_3_2_lingqu");
            _particle.Stop();
            _txtLearnName = _btnLearn.GetChildComponent<StateText>("label");
            _toggleRegister = _skillContentView.gameObject.AddComponent<KToggleGroup>();
            _skillItemList = new List<LearnSkillItem>();
            for (int i = 0; i < 8;i++ )
            {
                LearnSkillItem item = AddChildComponent<LearnSkillItem>("Container_jinengxuanze/Container_jineng0"+i);
                item.RegisterToggle(_toggleRegister);
                item.onClick.AddListener(OnClickLearnSkillItem);
                _skillItemList.Add(item);
            }
            base.Awake();
        }

        public void UpdateSpellOperation()
        {
            if(Visible)
            {
                List<KToggle> toggleList = _toggleGroup.GetToggleList();
                Dictionary<int, bool> hasSkillCanUpgradeDict = PlayerDataManager.Instance.SpellData.hasSkillCanUpgradeDict;
                for (int i = 0; i < toggleList.Count; i++)
                {
                    toggleList[i].SetGreenPoint(hasSkillCanUpgradeDict.ContainsKey(i + 1) && hasSkillCanUpgradeDict[i + 1]);
                }
            }
        }

        public void OnShow(object data)
        {
            Visible = true;
            dict = skill_helper.GetVocationSpellPrificientDict(PlayerAvatar.Player.vocation.ToInt());
            List<KToggle> toggleList = _toggleGroup.GetToggleList();
            if(checkmarkList == null)
            {
                checkmarkList = new List<StateText>();
                checkmarkImgList = new List<StateImage>();
                backList = new List<StateText>();
                backImgList = new List<StateImage>();
               
                for(int i=0;i<toggleList.Count;i++)
                {
                    checkmarkList.Add(toggleList[i].GetChildComponent<StateText>("checkmark/label"));
                    backList.Add(toggleList[i].GetChildComponent<StateText>("image/label"));
                    checkmarkImgList.Add(toggleList[i].GetChildComponent<StateImage>("checkmark/icon"));
                    backImgList.Add(toggleList[i].GetChildComponent<StateImage>("image/icon"));
                }
            }
            for (int i = 0; i < checkmarkList.Count;i++ )
            {
                if (dict.ContainsKey(i + 1))
                {
                    spell_proficient proficient = dict[i + 1];
                    toggleList[i].Visible = true;
                    if (GlobalParams.GetProficientOpenLevel(i + 1) <= PlayerAvatar.Player.level)
                    {
                        string name = (71879).ToLanguage(proficient.__name.ToLanguage());
                        checkmarkList[i].ChangeAllStateText(name);
                        backList[i].ChangeAllStateText(name);
                        bool hasProficient = PlayerDataManager.Instance.SpellData.HasProficient(proficient.__group);
                        checkmarkImgList[i].Visible = hasProficient;
                        backImgList[i].Visible = hasProficient;
                        toggleList[i].enabled = true;
                    }
                    else
                    {
                        string name = (71880).ToLanguage(proficient.__name.ToLanguage(), GlobalParams.GetProficientOpenLevel(i + 1));
                        toggleList[i].enabled = false;
                        checkmarkList[i].ChangeAllStateText(name);
                        backList[i].ChangeAllStateText(name);
                        checkmarkImgList[i].Visible = false;
                        backImgList[i].Visible = false; ;
                    }
                }
                else
                {
                    string name = (83001).ToLanguage();
                    toggleList[i].enabled = false;
                    checkmarkList[i].ChangeAllStateText(name);
                    backList[i].ChangeAllStateText(name);
                    checkmarkImgList[i].Visible = false;
                    backImgList[i].Visible = false;
                }
            }


            
            if(data == null)
            {
                _toggleGroup.SelectIndex = 0;
                OnSelectedIndexChanged(_toggleGroup, 0);
            }
            else if(data is int[])
            {
                int[] dataList = data as int[];
                _toggleGroup.SelectIndex = dataList[2];
                RefreshLearnSkillItemList(dataList[2]);
                SelectLearnSkillItem(dataList[3]);
            }
            else
            {
                _toggleGroup.SelectIndex = (int)data;
                OnSelectedIndexChanged(_toggleGroup, (int)data);
            }

            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            _btnLearn.onClick.AddListener(OnClickLearn);
            _btnPreview.onClick.AddListener(OnClickPreview);
            EventDispatcher.AddEventListener<int>(SpellEvents.SPELL_CHANGE, OnSpellChange);
            UpdateSpellOperation();
        }

        public void OnClose()
        {
            Visible = false;
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            _btnLearn.onClick.RemoveListener(OnClickLearn);
            _btnPreview.onClick.RemoveListener(OnClickPreview);
            EventDispatcher.RemoveEventListener<int>(SpellEvents.SPELL_CHANGE, OnSpellChange);
        }

        private void OnClickLearn()
        {
            
            _skillItemList[selectedIndex].Upgrade(spell, hasLearn);
        }

        private void OnSpellChange(int position)
        {
            if(position < 1)
            {
                return;
            }
            spell_sys sys = skill_helper.GetSpellSysByPosition(PlayerAvatar.Player.vocation.ToInt(), _toggleGroup.SelectIndex+1,position);
            _skillItemList[position - 1].SetData(sys);
            OnClickLearnSkillItem(sys, _skillItemList[position - 1].CanUpgrade);
            if (position % 2 == 1 && _skillItemList[position].Visible)
            {
                spell_sys nextSys = skill_helper.GetSpellSysByPosition(PlayerAvatar.Player.vocation.ToInt(), _toggleGroup.SelectIndex + 1, position + 1);
                _skillItemList[position].SetData(nextSys);
            }
            if(_skillItemList[position-1].CanUpgrade == false)
            {
                SelectedFirstCanUpgradeItem();
            }
        }

       
        private void OnSelectedIndexChanged(KToggleGroup toggleGroup,int index)
        {
            RefreshLearnSkillItemList(index);
            SelectedFirstCanUpgradeItem();
        }

        private void SelectedFirstCanUpgradeItem()
        {
            for (int i = 0; i < _skillItemList.Count; i++)
            {
                if (_skillItemList[i].Visible && _skillItemList[i].CanUpgrade)
                {
                    SelectLearnSkillItem(i);
                    return;
                }
            }
            SelectLearnSkillItem(0);
        }

        private void RefreshLearnSkillItemList(int index)
        {
            if (dict != null && dict.ContainsKey(index + 1))
            {
                Dictionary<int, spell_sys> dataDict = skill_helper.GetPlayerSpellListByGroup(index + 1);
                for (int i = 0; i < _skillItemList.Count; i++)
                {
                    if (dataDict.ContainsKey(i + 1) == false)
                    {
                        _skillItemList[i].OnClose();
                    }
                    else
                    {
                        _skillItemList[i].OnShow();
                        _skillItemList[i].SetData(dataDict[i + 1]);
                    }
                }
            }
        }

        private void SelectLearnSkillItem(int index)
        {
            if (selectedIndex != -1)
            {
                _skillItemList[selectedIndex].SetSelected(false);
            }
            selectedIndex = index;
            _skillItemList[selectedIndex].SetSelected(true);
            OnClickLearnSkillItem(_skillItemList[selectedIndex].Data, _skillItemList[selectedIndex].CanUpgrade);
        }

        private void OnClickLearnSkillItem(spell_sys sys,bool CanUpgrade)
        {
            selectedIndex = sys.__position-1;
            spell = PlayerDataManager.Instance.SpellData.GetPositionSpell(sys);
            isFullLevel = spell.__sp_level>=skill_helper.GetSpellMaxLevel(spell.__group);
            
            hasLearn = PlayerDataManager.Instance.SpellData.HasSkillLearned(sys.__id);
            //Debug.LogError("选中技能:" + spell.__id);
            _skillContentView.SetData(spell, isFullLevel, hasLearn);
            if (hasLearn == false)
            {
                _btnLearn.Visible = true;
                _txtLearnName.ChangeAllStateText((71314).ToLanguage());//学习
            }
            else if(isFullLevel == false)
            {
                _btnLearn.Visible = true;
                
                _txtLearnName.ChangeAllStateText((74624).ToLanguage());//升级
            }
            else
            {
                _btnLearn.Visible = false;
               
            }
            if(CanUpgrade)
            {
                if(_particle.Visible == false)
                {
                    _particle.Play(true);
                    _particle.Position = new Vector3(0, 3, 0);
                }
            }
            else
            {
                _particle.Stop();
            }
        }

        private void OnClickPreview()
        {
            PanelIdEnum.SkillCg.Show(spell);
           // PanelIdEnum.SkillCg.Show( spell);
            SkillPreviewManager.GetInstance().Show(spell);
           // UILayerManager.HideLowerLayer(MogoUILayer.LayerSecondUIPanel);

        }

    }
}
