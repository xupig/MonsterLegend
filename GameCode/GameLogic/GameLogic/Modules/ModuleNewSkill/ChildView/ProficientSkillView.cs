﻿#region 模块信息
/*==========================================
// 文件名：ProficientSkillView
// 命名空间: GameLogic.GameLogic.Modules.ModuleNewSkill.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/13 10:56:27
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ClientConfig;
using Common.Events;
using Common.Global;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleNewSkill
{
    public class ProficientSkillView : KContainer
    {
        private KContainer _imgEffect;
        private KToggleGroup _toggleGroup;
       // private StateText _txtName;
        private StateText _txtDescription;
        private StateText _txtSkillDesc;
        private PentagonImage _pentagonImage;
        private StateText _txtDetailDescription;
        private KButton _btnUse;
        private KButton _btnUsed;

        private KToggleGroup _skillToggleGroup;

        //private string nameTemplate;

        private List<StateText> checkmarkList;
        private List<StateText> backList;
        private List<StateImage> checkmarkImgList;
        private List<StateImage> backImgList;

        private Dictionary<int, spell_proficient> dict;
        private List<NewSkillItem> skillList;
        private StateText _txtCantReplace;
      
        protected override void Awake()
        {
            _imgEffect = GetChildComponent<KContainer>("Container_zhuangjing");
            _imgEffect.Visible = false;
            startPos = _imgEffect.GetComponent<RectTransform>().anchoredPosition;
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_jinengxuexi");
            _txtDescription = GetChildComponent<StateText>("Label_txtJieshaoxiangxi");
            _txtSkillDesc = GetChildComponent<StateText>("Label_txtJinengpinglun");
            _txtDetailDescription = GetChildComponent<StateText>("Label_txtJieshaoxiangxi");
            _btnUse = GetChildComponent<KButton>("Button_xuexi");
            _btnUsed = GetChildComponent<KButton>("Button_shiyongzhong");
            _pentagonImage = GetChildComponent<StateImage>("Image_pentagon").CurrentImage as PentagonImage;
            _skillToggleGroup = GetChildComponent<KToggleGroup>("Container_slot/ToggleGroup_jinengcao");
            List<KToggle> list = _skillToggleGroup.GetToggleList();
            skillList = new List<NewSkillItem>();
            for (int i = 0; i < list.Count; i++)
            {
                NewSkillItem item = list[i].gameObject.AddComponent<NewSkillItem>();
                item.isProficient = true;
                skillList.Add(item);
            }
            _txtCantReplace = GetChildComponent<StateText>("Container_slot/Label_txtBuketihuan");
            _txtCantReplace.CurrentText.text = (6013012).ToLanguage();
        }


        public void OnShow()
        {
            Visible = true;
            dict = skill_helper.GetVocationSpellPrificientDict(PlayerAvatar.Player.vocation.ToInt());

            if (checkmarkList == null)
            {
                checkmarkList = new List<StateText>();
                checkmarkImgList = new List<StateImage>();
                backList = new List<StateText>();
                backImgList = new List<StateImage>();
                List<KToggle> toggleList = _toggleGroup.GetToggleList();
                for (int i = 0; i < toggleList.Count; i++)
                {
                    toggleList[i].SetGreenPoint(false);
                    checkmarkList.Add(toggleList[i].GetChildComponent<StateText>("checkmark/label"));
                    backList.Add(toggleList[i].GetChildComponent<StateText>("image/label"));
                    checkmarkImgList.Add(toggleList[i].GetChildComponent<StateImage>("checkmark/icon"));
                    backImgList.Add(toggleList[i].GetChildComponent<StateImage>("image/icon"));
                }
            }
            RefreshProficient();
            _btnUse.onClick.AddListener(OnUseProficient);
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            EventDispatcher.AddEventListener<int>(SpellEvents.SPELL_CHANGE, OnSpellChanged);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.spell_proficient, UpdateProficients);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.spell_proficient_factor1, UpdateProficients);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.spell_proficient_factor2, UpdateProficients);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.spell_proficient_factor3, UpdateProficients);
            _toggleGroup.SelectIndex = 0;
            OnSelectedIndexChanged(_toggleGroup, 0);
        }


        private Vector2 startPos;

        public void Tween()
        {
            _imgEffect.Visible = true;
            Vector3 endPosition = startPos;
            _imgEffect.transform.localPosition = new Vector3(startPos.x, startPos.y - 570, 0);
            TweenPosition.Begin(_imgEffect.gameObject, 0.5f, endPosition, 0.0f, UITweener.Method.BounceIn, OnComplete);
        }

        private void OnComplete(UITweener tween)
        {
            _imgEffect.Visible = false;
        }

        private void UpdateToggle()
        {
            List<KToggle> toggleList = _toggleGroup.GetToggleList();
            for (int i = 0; i < checkmarkList.Count; i++)
            {
                if (dict.ContainsKey(i + 1))
                {
                    spell_proficient proficient = dict[i + 1];
                    if (GlobalParams.GetProficientOpenLevel(i + 1) <= PlayerAvatar.Player.level)
                    {
                        string name = (71879).ToLanguage(proficient.__name.ToLanguage());
                        checkmarkList[i].ChangeAllStateText(name);
                        backList[i].ChangeAllStateText(name);
                        bool hasProficient = PlayerDataManager.Instance.SpellData.HasProficient(proficient.__group);
                        checkmarkImgList[i].Visible = hasProficient;
                        backImgList[i].Visible = hasProficient;
                        toggleList[i].enabled = true;
                    }
                    else
                    {
                        string name = (71880).ToLanguage(proficient.__name.ToLanguage(), GlobalParams.GetProficientOpenLevel(i + 1));
                        toggleList[i].enabled = false;
                        checkmarkList[i].ChangeAllStateText(name);
                        backList[i].ChangeAllStateText(name);
                        checkmarkImgList[i].Visible = false;
                        backImgList[i].Visible = false;
                    }
                }
                else
                {
                    string name = (83001).ToLanguage();
                    toggleList[i].enabled = false;
                    checkmarkList[i].ChangeAllStateText(name);
                    backList[i].ChangeAllStateText(name);
                    checkmarkImgList[i].Visible = false;
                    backImgList[i].Visible = false;
                }
            }
            SetButtonState();
        }

        private void SetButtonState()
        {
            if (PlayerDataManager.Instance.SpellData.HasProficient(_toggleGroup.SelectIndex + 1))
            {
                _btnUsed.Visible = true;
                _btnUse.Visible = false;
            }
            else
            {
                _btnUsed.Visible = false;
                _btnUse.Visible = true;
            }
        }

        private void RefreshProficient()
        {
            UpdateToggle();
            Dictionary<int, spell_sys> dataDict = skill_helper.GetPlayerSpellListByGroup(_toggleGroup.SelectIndex + 1);
            for (int i = 0; i < skillList.Count; i++)
            {
                if (dataDict.ContainsKey(i * 2 + 1))
                {
                    skillList[i].UpdateSpell(dataDict[i * 2 + 1]);
                }
            }
        }

        private void UpdateValue()
        {
            Dictionary<int, spell_sys> dataDict = skill_helper.GetPlayerSpellListByGroup(_toggleGroup.SelectIndex + 1);
            for (int i = 0; i < skillList.Count; i++)
            {
                if (dataDict.ContainsKey(i * 2 + 1))
                {
                    skillList[i].UpdateSpell(dataDict[i * 2 + 1]);
                }
            }
        }

        private void UpdateProficients()
        {
            if (PlayerAvatar.Player.spell_proficient == 1 && PlayerAvatar.Player.spell_proficient_factor1 <= 0)
            {
                return;
            }
            if (PlayerAvatar.Player.spell_proficient == 2 && PlayerAvatar.Player.spell_proficient_factor2 <= 0)
            {
                return;
            }
            if (PlayerAvatar.Player.spell_proficient == 3 && PlayerAvatar.Player.spell_proficient_factor3 <= 0)
            {
                return;
            }
            Tween();
            UpdateToggle();
            Dictionary<int, spell_sys> dataDict = skill_helper.GetPlayerSpellListByGroup(_toggleGroup.SelectIndex + 1);
            for (int i = 0; i < skillList.Count; i++)
            {
                if (dataDict.ContainsKey(i * 2 + 1))
                {
                    skillList[i].UpdateSpell(dataDict[i * 2 + 1]);
                }
            }
            UpdateValue();
        }

        public void OnClose()
        {
            Visible = false;
            if (_toggleGroup != null)
            {
                _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            }
            EventDispatcher.RemoveEventListener<int>(SpellEvents.SPELL_CHANGE, OnSpellChanged);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.spell_proficient, UpdateProficients);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.spell_proficient_factor1, UpdateProficients);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.spell_proficient_factor2, UpdateProficients);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.spell_proficient_factor3, UpdateProficients);
            if (_btnUse != null)
            {
                _btnUse.onClick.RemoveListener(OnUseProficient);
            }
        }

        private void OnUseProficient()
        {
            if (dict != null)
            {
                spell_proficient current = skill_helper.GetSpellProficient(PlayerAvatar.Player.vocation.ToInt(), _toggleGroup.SelectIndex + 1);
                spell_proficient before = skill_helper.GetSpellProficient(PlayerAvatar.Player.vocation.ToInt(), PlayerDataManager.Instance.SpellData.GetCurrentProficient());
                if (before != null && current.__id != before.__id)
                {
                    MessageBox.Show(true, MessageBox.DEFAULT_TITLE, (71321).ToLanguage(before.__name.ToLanguage(), current.__name.ToLanguage()), OnProficient);
                }
                else if (before == null)
                {
                    MessageBox.Show(true, MessageBox.DEFAULT_TITLE, (71320).ToLanguage(current.__name.ToLanguage()), OnProficient);
                }
                else
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, (71438).ToLanguage(), PanelIdEnum.MainUIField);
                }
            }
        }

        private void OnProficient()
        {
            SpellManager.Instance.SpellProficient(_toggleGroup.SelectIndex + 1);
        }

        private void OnSpellChanged(int position)
        {
            OnSelectedIndexChanged(_toggleGroup, position - 1);
        }

        private void OnSelectedIndexChanged(KToggleGroup toggleGroup, int index)
        {
            SetButtonState();
            if (dict != null && dict.ContainsKey(index + 1))
            {
                spell_proficient proficient = dict[index + 1];
                float[] dataList = new float[5];
                for (int i = 0; i < proficient.__evaluation.Count; i++)
                {
                    dataList[i] = float.Parse(proficient.__evaluation[i]) / 10.0f;
                }
                _pentagonImage.SetValues(dataList);
                _txtSkillDesc.CurrentText.text = proficient.__effect_id.ToLanguage();
                _txtDetailDescription.CurrentText.text = proficient.__description.ToLanguage();
                //_txtName.CurrentText.text = string.Format(nameTemplate, proficient.__name.ToLanguage());

                Dictionary<int, spell_sys> dataDict = skill_helper.GetPlayerSpellListByGroup(index + 1);
                List<NewSkillItem> sortList = new List<NewSkillItem>();
                for (int i = 0; i < skillList.Count; i++)
                {
                    if (dataDict.ContainsKey(i * 2 + 1) == false)
                    {
                        skillList[i].OnClose();
                    }
                    else
                    {
                        sortList.Add(skillList[i]);
                        skillList[i].OnShow();
                        skillList[i].SetData(dataDict[i * 2 + 1]);
                    }
                }
            }
            UpdateValue();
        }

    }
}
