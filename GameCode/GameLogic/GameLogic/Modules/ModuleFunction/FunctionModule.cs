﻿#region 模块信息
/*==========================================
// 文件名：FunctionModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleFunction
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/25 16:55:43
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleFunction
{
    public class FunctionModule:BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.Function, "Container_FunctionPanel", MogoUILayer.LayerHideOther, "ModuleFunction.FunctionPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.SkillNotice, "Container_SkillNoticePanel", MogoUILayer.LayerHideOther, "ModuleFunction.SkillNoticePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, true);
        }
    }
}
