﻿#region 模块信息
/*==========================================
// 文件名：FunctionPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleFunction
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/25 17:18:00
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleFunction
{
    public class FunctionPanel:BasePanel
    {
        private StateIcon _iconContainer;
        private StateText _txtTitle;
        //private StateText _iconName;
        private KButton _btnIcon;
        private KContainer _content;
        private KDummyButton _btnClose;
        private int functionId;
        private string template;
        private Vector2 _positions;
        private Quaternion _rotation;
        private Vector3 _scale;
        private StateText _txtCount;

        private Vector3 _offset;

        private bool _hasInitail = false;

        //private uint _timerId = 0;
        private float count = 0;

       // private StateImage _imgBg;

        public static bool isShow;
        private RectTransform rect;

        private StateImage _lockImg;

        private KParticle _boomParticle;

        private bool isOver = true;

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _lockImg = GetChildComponent<StateImage>("Image_suo02");
            _btnIcon = GetChildComponent<KButton>("Button_chongzhijiangli");
            _content = _btnIcon.GetChildComponent<KContainer>("content");
            _txtTitle = _content.GetChildComponent<StateText>("Label_txtdianjipingmujixu");
            //_iconName = _btnIcon.GetChildComponent<StateText>("Label_label");
            template = _txtTitle.CurrentText.text;
            _iconContainer = _btnIcon.GetChildComponent<StateIcon>("stateIcon");
            _txtCount = _content.GetChildComponent<StateText>("Label_txtDaojishi");
            _boomParticle = _btnIcon.AddChildComponent<KParticle>("stateIcon/fx_ui_2_1_jiesuojineng");
            _boomParticle.Stop();
           
            _btnClose = gameObject.AddComponent<KDummyButton>();
             rect = _btnIcon.GetComponent<RectTransform>();
            _offset = rect.sizeDelta/2 - _iconContainer.GetComponent<RectTransform>().sizeDelta/2;
            _offset.y = -_offset.y;
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Function; }
        }
        
        public override void OnClose()
        {
            isOver = false;
            UIManager.Instance.ShowOtherLayer(MogoUILayer.LayerHideOther);
            isShow = false;
            _btnClose.onClick.RemoveListener(OnClickClose);
            _btnIcon.onClick.RemoveListener(OnClickClose);

        }

        public override void OnShow(object data)
        {
            TaskManager.Instance.ContinueAutoTask();
            isOver = true;
            UIManager.Instance.HideOtherLayer(MogoUILayer.LayerHideOther);
            isShow = true;
            count = Time.realtimeSinceStartup;
            _txtCount.CurrentText.text = string.Format("{0}s",10);
            _lockImg.Visible = true;
            _btnClose.onClick.AddListener(OnClickClose);
            _btnIcon.onClick.AddListener(OnClickClose);
            SetData(data);
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            functionId = (int)data;
            ResetPosition();
            function function = function_helper.GetFunction(functionId);
            _txtTitle.CurrentText.text = string.Format(template, function.__name.ToLanguage());
            _iconContainer.SetIcon(function.__icon);
            _boomParticle.Play(true);
            _boomParticle.transform.localPosition = new Vector3(25, -25, 0);
        }

        private void ResetPosition()
        {
            if (_hasInitail == true)
            {
                _content.Visible = true;
                rect.anchoredPosition = _positions;
                _iconContainer.transform.rotation = _rotation;
                _iconContainer.transform.localScale = _scale;
            }
        }

        private void OnClickClose()
        {
            if (count == 0 || Time.realtimeSinceStartup - count < 1)
            {
                return;
            }
            _boomParticle.Stop();
            _content.Visible = false;
            if (_hasInitail == false)
            {
                _hasInitail = true;
                _positions = rect.anchoredPosition;
                _rotation = _iconContainer.transform.rotation;
                _scale = _iconContainer.transform.localScale;
            }
            _btnClose.onClick.RemoveListener(OnClickClose);
            _btnIcon.onClick.RemoveListener(OnClickClose);
            EventDispatcher.TriggerEvent(FunctionEvents.OPEN, functionId, _iconContainer.gameObject, _offset);
        }

        public void Update()
        {
            if (_lockImg.Visible == true && Time.realtimeSinceStartup - count > 1)
            {
                UIManager.Instance.PlayAudio("Sound/UI/new_fuction.mp3");
                _lockImg.Visible = false;
            }
            _txtCount.CurrentText.text = string.Format("{0}s", 10-Mathf.RoundToInt(Time.realtimeSinceStartup - count));
            if (Time.realtimeSinceStartup - count >= 10 && isOver)
            {
                isOver = false;
                OnClickClose();
            }
        }

    }
}
