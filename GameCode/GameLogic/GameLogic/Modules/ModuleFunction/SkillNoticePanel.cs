﻿using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using ModuleMainUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleFunction
{
    public class SkillNoticePanel : BasePanel
    {
        private IconContainer _iconContainer;
        private StateText _txtTitle;
        private KButton _btnIcon;
        private KContainer _content;
        private KDummyButton _btnClose;
        private KContainer _skillContainer;
        private SkillNoticeData _data;
        private uint _timerId;
        private Vector3 _originalPosition;
        private KParticle _particle;

        protected override void Awake()
        {
            _btnIcon = GetChildComponent<KButton>("Button_chongzhijiangli");
            _content = _btnIcon.GetChildComponent<KContainer>("content");
            _txtTitle = _content.GetChildComponent<StateText>("Label_txtWeizhi");
            _skillContainer = _btnIcon.GetChildComponent<KContainer>("Skill");
            _iconContainer = _btnIcon.AddChildComponent<IconContainer>("Skill/Container_icon");
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _btnClose = AddChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
            _particle = _skillContainer.AddChildComponent<KParticle>("fx_ui_2_1_jiesuojineng");
            _skillContainer.transform.localPosition = new Vector3(_skillContainer.transform.localPosition.x, _skillContainer.transform.localPosition.y, -300);
            _iconContainer.transform.localPosition = new Vector3(_iconContainer.transform.localPosition.x, _iconContainer.transform.localPosition.y, -5);
            _particle.Position = new Vector3(18, -18, 200);
            _particle.Stop();
            _originalPosition = _skillContainer.transform.localPosition;
        }

        //private void ChangeImageShader(GameObject go)
        //{
        //    ImageWrapper[] imageWrapperArray = go.transform.GetComponentsInChildren<ImageWrapper>();
        //    for (int i = 0; i < imageWrapperArray.Length; i++)
        //    {
        //        MogoShaderUtils.ChangeShader(imageWrapperArray[i], MogoShaderUtils.IconCutShaderPath);
        //    }
        //}

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.SkillNotice; }
        }

        public override void OnClose()
        {
            UIManager.Instance.ShowOtherLayer(MogoUILayer.LayerHideOther);
            ResetTimer();
            _particle.Stop();
        }

        public override void OnShow(object data)
        {
            UIManager.Instance.HideOtherLayer(MogoUILayer.LayerHideOther);
            //ChangeImageShader(_skillContainer.gameObject);
            Begin(data);
        }

        public void Begin(object data)
        {
            _data = data as SkillNoticeData;
            int skillId = 0;
            Dictionary<int, int> posSkillIdMap;
            if (PlayerAvatar.Player.vehicleManager.vehicle != null)
            {
                posSkillIdMap = PlayerAvatar.Player.vehicleManager.vehicle.skillManager.GetCurPosSkillIDMap();
            }
            else
            {
                posSkillIdMap = PlayerAvatar.Player.skillManager.GetCurPosSkillIDMap();
            }
            if (posSkillIdMap.ContainsKey(_data.Index))
            {
                skillId = posSkillIdMap[_data.Index];
            }
            _txtTitle.CurrentText.text = MogoLanguageUtil.GetContent(spell_helper.GetName(skillId));
            int iconId = spell_helper.GetIconId(skillId);
            _iconContainer.SetIcon(iconId);
            _skillContainer.transform.localPosition = _originalPosition;
            _content.Visible = true;
            ResetTimer();
            _timerId = TimerHeap.AddTimer(1000, 0, StartTween);
            _particle.Play(true);
        }

        //private void OnIconLoaded(GameObject go)
        //{
        //    ChangeImageShader(go);
        //}

        private void StartTween()
        {
            ResetTimer();
            _content.Visible = false;
            Vector3 targetPosition = _skillContainer.transform.parent.InverseTransformPoint(_data.targetPosition);
            TweenPosition tween = TweenPosition.Begin(_skillContainer.gameObject, 1.0f, targetPosition);
            tween.onFinished = OnTweenEnd;
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private void OnTweenEnd(UITweener tween)
        {
            EventDispatcher.TriggerEvent<int, bool>(BattleUIEvents.SET_SKILL_BEFORE_NOTICE, _data.Index, false);
            ClosePanel();
            EventDispatcher.TriggerEvent(PanelEvents.PANEL_LAYER_RESET_POS);
        }

    }
}
