﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTeamReward
{
    public class EquipAssignTips : BasePanel
    {
        private KScrollView _scrollView;
        private KList _list;
        private KDummyButton _dummyBtn;
        private KButton _closeBtn;
        private KContainer _equipContainer;
        private int _gridPosition;

        private readonly Vector2 POSITION = new Vector3(152, -62);

        protected override void Awake()
        {
            _scrollView = GetChildComponent<KScrollView>("Container_equip/ScrollView_duiyou");
            _list = GetChildComponent<KList>("Container_equip/ScrollView_duiyou/mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _closeBtn = GetChildComponent<KButton>("Container_equip/Container_Bg/Button_close");
            _equipContainer = GetChildComponent<KContainer>("Container_equip");
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _dummyBtn = ModalMask.gameObject.AddComponent<KDummyButton>();
            ModalMask.Alpha = 1f / 255f;
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.EquipAssignTips; }
        }

        public override void OnShow(object data)
        {
            SetData(data);
            AddEventListener();
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            ShowView();
            RefreshContent(data as EquipToolTipsData);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void RefreshContent(EquipToolTipsData data)
        {
            _gridPosition = data.GridPosition;
            _list.SetDataList<EquipAssignItem>(data.UserList);
            foreach (EquipAssignItem item in _list.ItemList)
            {
                item.gridPosition = _gridPosition;
            }
        }

        private void AddEventListener()
        {
            _closeBtn.onClick.AddListener(HideView);
            _dummyBtn.onClick.AddListener(HideView);
        }

        private void RemoveEventListener()
        {
            _closeBtn.onClick.RemoveListener(HideView);
            _dummyBtn.onClick.RemoveListener(HideView);
        }

        private void ShowView()
        {
            (_equipContainer.transform as RectTransform).anchoredPosition = POSITION;
            TweenViewMove.Begin(_equipContainer.gameObject, MoveType.Show, MoveDirection.Left);
        }

        private void HideView()
        {
            TweenViewMove tween = TweenViewMove.Begin(_equipContainer.gameObject, MoveType.Hide, MoveDirection.Left);
            tween.Tweener.onFinished = OnTweenFinish;  
        }

        private void OnTweenFinish(UITweener tween)
        {
            ClosePanel();
        }

    }
}