﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTeamReward
{
    public class TeamRewardAssignPanel : BasePanel
    {
        private TeamRewardLeftView _leftView;
        private TeamRewardRightView _rightView;
        private TeamRewardStarView _starView;
        private TeamRewardDiceView _diceView;
        private KButton _ensureBtn;
        private KButton _nextBtn;
        private StateText _descTxt;

        private uint _endTime;
        private ulong _giveId;
        private uint _turnId;
        private uint _mode;

        private uint _timeId;
        private bool _isOnShow;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TeamRewardAssign; }
        }

        protected override void Awake()
        {
            _starView = AddChildComponent<TeamRewardStarView>("Container_biaoti");
            _leftView = AddChildComponent<TeamRewardLeftView>("Container_left");
            _rightView = AddChildComponent<TeamRewardRightView>("Container_right");
            _diceView = AddChildComponent<TeamRewardDiceView>("Container_daojishi");
            _ensureBtn = GetChildComponent<KButton>("Button_queding");
            _nextBtn = GetChildComponent<KButton>("Button_fenpei");
            _descTxt = GetChildComponent<StateText>("Label_txtXitongfenpei");
            _ensureBtn.Label.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
            _nextBtn.Label.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
        }

        public override void OnShow(object data)
        {
            PlayerDataManager.Instance.TeamInstanceData.IsAssigningTeamReward = true;
            AddEventListener();
            _isOnShow = true;
            SetData(data);
            PanelIdEnum.TeamRewardChatEntry.Show();
        }

        public override void OnClose()
        {
            ResetTimer();
            RemoveEventListener();
            PanelIdEnum.TeamRewardChatEntry.Close();
            PlayerDataManager.Instance.TeamInstanceData.IsAssigningTeamReward = false;
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            PbCaptainDistInfo info = PlayerDataManager.Instance.TeamInstanceData.PbCaptainDistInfo;
            PlayerDataManager.Instance.TeamInstanceData.PbCaptainDistInfo = null;
            if (_isOnShow)
            {
                _starView.SetStarNum((int)info.group_score);
            }
            RefreshReward(info);
            bool hasGain = CheckHasGain(info);
            if (!_isOnShow && hasGain && _endTime > Global.serverTimeStampSecond)
            {
                StartCoroutine(WaitAndRefresh(2.0f, info));
            }
            else
            {
                RefreshContent(info);
            }
            _isOnShow = false;
        }

        private void RefreshReward(PbCaptainDistInfo info)
        {
            if (info.temp_dbid != 0)
            {
                _giveId = info.temp_dbid;
            }
            if (info.dist_datas.Count > 0)
            {
                _turnId = info.dist_datas[0].turn_id;
                _mode = info.dist_datas[0].dist_mode;
                if (info.dist_datas[0].wait_time > 0)
                {
                    _endTime = info.dist_datas[0].wait_time;
                }
            }
        }

        private IEnumerator WaitAndRefresh(float waitTime, PbCaptainDistInfo info)
        {
            yield return new WaitForSeconds(waitTime);
            RefreshContent(info);
        }


        private void RefreshContent(PbCaptainDistInfo info)
        {
            if (info.dist_datas.Count > 0)
            {
                _leftView.SetWantBtnStatus((int)_mode);
                _leftView.RefreshContent(info.dist_datas[0].item_info, (int)_turnId);
                _rightView.RefreshContent(info.dist_datas);
                int waitTime = int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.captain_auto_distribution_time));
                _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(110012, waitTime);
                RefreshDiceView(info.dist_datas);
            }
            _leftView.RefreshBelongContent(_giveId);
            RefreshBtnView(info);
        }

        private void RefreshDiceView(List<PbDistItem> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].role_dbid == PlayerAvatar.Player.dbid)
                {
                    if (list[i].dist_mode == public_config.CPATAIN_MODE_DICE && list[i].gain_status == public_config.CAPTAIN_GAIN_WANT && list[i].rand_point != 0)
                    {
                        _diceView.RefreshContent((int)list[i].rand_point);
                    }
                    break;
                }
            }
        }

        private void RefreshBtnView(PbCaptainDistInfo info)
        {
            ResetTimer();
            bool hasGain = CheckHasGain(info);
            if (hasGain == true)
            {
                _nextBtn.Visible = true;
                _ensureBtn.Visible = false;
                _nextBtn.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(110005));
                _leftView.HideButton();
                _descTxt.Visible = false;
            }
            else
            {
                _nextBtn.Visible = false;
                _ensureBtn.Visible = true;
                _descTxt.Visible = true;
                if (_mode == public_config.CAPTAIN_MODE_MASTER)
                {
                    if (PlayerDataManager.Instance.TeamData.IsCaptain() || PlayerDataManager.Instance.TeamData.TeammateDic.Count == 0)
                    {
                        _ensureBtn.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(110003));
                        _ensureBtn.SetButtonActive();
                    }
                    else
                    {
                        _ensureBtn.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(110004));
                        _ensureBtn.SetButtonDisable();
                    }
                }
                else
                {
                    _ensureBtn.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(110021));
                    _ensureBtn.SetButtonDisable();
                }
                _timeId = TimerHeap.AddTimer(0, 100, OnStep);
            }
        }

        private bool CheckHasGain(PbCaptainDistInfo info)
        {
            bool hasGain = false;
            for (int i = 0; i < info.dist_datas.Count; i++)
            {
                if (info.dist_datas[i].gain_status == public_config.CAPTAIN_GAIN_SUCCESS || info.dist_datas[i].gain_status == public_config.CAPTAIN_GAIN_FAILURE)
                {
                    hasGain = true;
                }
            }
            return hasGain;
        }

        private void ResetTimer()
        {
            if (_timeId != 0)
            {
                TimerHeap.DelTimer(_timeId);
                _timeId = 0;
            }
        }

        private void OnStep()
        {
            ulong leftTime = 0;
            if (_endTime > Global.serverTimeStampSecond)
            {
                leftTime = _endTime - Global.serverTimeStampSecond;
            }
            if (_mode == public_config.CAPTAIN_MODE_MASTER)
            {
                if (PlayerDataManager.Instance.TeamData.IsCaptain() || PlayerDataManager.Instance.TeamData.TeammateDic.Count == 0)
                {
                    _ensureBtn.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(110003, leftTime));
                }
            }
            _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(110012, leftTime);
        }

        private void OnEnsureBtnClick()
        {
            TeamInstanceManager.Instance.CaptainItemEnsure(_giveId, (byte)_turnId);
        }

        private void OnNextBtnClick()
        {
            ClosePanel();
            CopyLogicManager.Instance.ShowTeamLogicPanel();
        }

        private void AddEventListener()
        {
            _ensureBtn.onClick.AddListener(OnEnsureBtnClick);
            _nextBtn.onClick.AddListener(OnNextBtnClick);
        }

        private void RemoveEventListener()
        {
            _ensureBtn.onClick.RemoveListener(OnEnsureBtnClick);
            _nextBtn.onClick.RemoveListener(OnNextBtnClick);
        }
    }
}