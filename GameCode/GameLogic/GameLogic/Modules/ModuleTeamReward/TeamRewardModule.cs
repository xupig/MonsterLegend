﻿using Common.Base;

namespace ModuleTeamReward
{
    public class TeamRewardModule : BaseModule
    {

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.TeamRewardAssign, "Container_TeamRewardAssignPanel", MogoUILayer.LayerUIPanel, "ModuleTeamReward.TeamRewardAssignPanel", BlurUnderlay.Have, HideMainUI.Hide, true);
            AddPanel(PanelIdEnum.EquipAssignTips, "Container_EquipAssignTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleTeamReward.EquipAssignTips", BlurUnderlay.Empty, HideMainUI.Not_Hide, true);
            AddPanel(PanelIdEnum.TeamRewardChatEntry, "Container_ChatEntry", MogoUILayer.LayerUIToolTip, "ModuleTeamReward.TeamRewardChatEntryPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }
    }
}