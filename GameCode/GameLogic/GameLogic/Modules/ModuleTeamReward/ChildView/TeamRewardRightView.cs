﻿using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTeamReward
{
    class TeamRewardRightView : KContainer
    {
        private KList _teammateList;
        private List<PbDistItem> _dataList;

        protected override void Awake()
        {
            base.Awake();
            _teammateList = GetChildComponent<KList>("List_content");
            _teammateList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _teammateList.SetGap(-10, 0);
            _dataList = new List<PbDistItem>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            EventDispatcher.AddEventListener(TeamInstanceEvents.Teammate_Refresh, RefreshMemberList);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            EventDispatcher.RemoveEventListener(TeamInstanceEvents.Teammate_Refresh, RefreshMemberList);
        }

        public void RefreshContent(List<PbDistItem> info)
        {
            List<PbDistItem> dataList = new List<PbDistItem>();
            dataList.AddRange(info);
            for (int i = dataList.Count - 1; i >= 0; i--)
            {
                for (int j = 0; j < _dataList.Count; j++)
                {
                    if (_dataList[j].role_dbid == dataList[i].role_dbid)
                    {
                        _dataList[j] = dataList[i];
                        dataList.RemoveAt(i);
                        break;
                    }
                }
            }
            _dataList.AddRange(dataList);
            RefreshMemberList();
        }

        private void RefreshMemberList()
        {
            _dataList.Sort(SortFun);
            _teammateList.SetDataList<TeamRewardItem>(_dataList);
        }

        private int SortFun(PbDistItem x, PbDistItem y)
        {
            if (x.gain_status == public_config.CAPTAIN_GAIN_SUCCESS && y.gain_status != public_config.CAPTAIN_GAIN_SUCCESS)
            {
                return -1;
            }
            if (x.gain_status != public_config.CAPTAIN_GAIN_SUCCESS && y.gain_status == public_config.CAPTAIN_GAIN_SUCCESS)
            {
                return 1;
            }
            if (x.role_dbid == PlayerDataManager.Instance.TeamData.CaptainId && y.role_dbid != PlayerDataManager.Instance.TeamData.CaptainId)
            {
                return -1;
            }
            if (x.role_dbid != PlayerDataManager.Instance.TeamData.CaptainId && y.role_dbid == PlayerDataManager.Instance.TeamData.CaptainId)
            {
                return 1;
            }
            return (int)x.role_dbid - (int)y.role_dbid;
        }
    }
}
