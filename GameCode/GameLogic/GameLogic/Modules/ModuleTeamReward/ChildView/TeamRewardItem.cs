﻿using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTeamReward
{
    class TeamRewardItem : KList.KListItemBase
    {
        private KButton _giveBtn;
        private IconContainer _icon;
        private StateText _levelTxt;
        private StateText _guildTxt;
        private StateText _fightTxt;
        private StateText _nameTxt;
        private StateText _wantTxt;
        private StateText _waitTxt;
        private StateImage _captainImg;
        private StateText _finalTxt;
        private KParticle _particle;

        private PbDistItem _data;
        private ulong _guildId;

        private ArtNumberList _pointList;
        private bool _hasRoll = false;

        protected override void Awake()
        {
            _giveBtn = GetChildComponent<KButton>("Button_sousuo");
            _icon = AddChildComponent<IconContainer>("Container_icon");
            _levelTxt = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            _guildTxt = GetChildComponent<StateText>("Label_txtGonghui");
            _nameTxt = GetChildComponent<StateText>("Label_txtWanjiamingcheng");
            _fightTxt = GetChildComponent<StateText>("Label_txtZhanli");
            _captainImg = GetChildComponent<StateImage>("Image_duizhangDB");
            _wantTxt = GetChildComponent<StateText>("Label_txtyiyuan");
            _waitTxt = GetChildComponent<StateText>("Label_txtDuizhangfenpei");
            _finalTxt = GetChildComponent<StateText>("Label_txtZuizhong");
            _pointList = AddChildComponent<ArtNumberList>("List_shaizi");
            _particle = AddChildComponent<KParticle>("fx_ui_TeamReward_huode");
            _particle.Stop();
        }

        public override void Dispose()
        {
            _data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbDistItem;
                RefreshContent();
            }
        }

        private void RefreshStatusView()
        {
            if (_data.dist_mode == public_config.CAPTAIN_MODE_MASTER)
            {
                RefreshMasterStatus();
            }
            else
            {
                RefreshDiceStatus();
            }
        }

        private void RefreshDiceStatus()
        {
            _giveBtn.Visible = false;
            _waitTxt.Visible = false;
            switch (_data.gain_status)
            {
                case public_config.CAPTAIN_GAIN_WAIT:
                    _finalTxt.Visible = false;
                    _wantTxt.Visible = false;
                    _pointList.Visible = false;
                    _hasRoll = false;
                    break;
                case public_config.CAPTAIN_GAIN_WANT:
                    _finalTxt.Visible = false;
                    _wantTxt.Visible = true;
                    _wantTxt.CurrentText.text = string.Empty;
                    _pointList.Visible = true;
                    StartRollPoint();
                    break;
                case public_config.CAPTAIN_GAIN_GIVEUP:
                    _finalTxt.Visible = false;
                    _wantTxt.Visible = true;
                    _wantTxt.CurrentText.text = MogoLanguageUtil.GetContent(110011);
                    _pointList.Visible = false;
                    break;
                case public_config.CAPTAIN_GAIN_SUCCESS:
                    _finalTxt.Visible = true;
                    _wantTxt.Visible = false;
                    ResetTimer();
                    _pointList.Visible = _data.rand_point != 0;
                    if (_pointList.Visible)
                    {
                        _pointList.SetNumber((int)_data.rand_point);
                    }
                    _particle.Play(true);
                    break;
                case public_config.CAPTAIN_GAIN_FAILURE:
                    _finalTxt.Visible = false;
                    _wantTxt.Visible = false;
                    ResetTimer();
                    _pointList.Visible = _data.rand_point != 0;
                    if (_pointList.Visible)
                    {
                        _pointList.SetNumber((int)_data.rand_point);
                    }
                    SetImageGray(true);
                    break;
            }
        }

        private void RefreshMasterStatus()
        {
            _pointList.Visible = false;
            switch (_data.gain_status)
            {
                case public_config.CAPTAIN_GAIN_WAIT:
                    _giveBtn.Visible = PlayerDataManager.Instance.TeamData.IsCaptain() || PlayerDataManager.Instance.TeamData.TeammateDic.Count == 0;
                    _waitTxt.Visible = !PlayerDataManager.Instance.TeamData.IsCaptain();
                    _finalTxt.Visible = false;
                    _wantTxt.Visible = false;
                    break;
                case public_config.CAPTAIN_GAIN_WANT:
                    _giveBtn.Visible = PlayerDataManager.Instance.TeamData.IsCaptain() || PlayerDataManager.Instance.TeamData.TeammateDic.Count == 0;
                    _waitTxt.Visible = !PlayerDataManager.Instance.TeamData.IsCaptain();
                    _finalTxt.Visible = false;
                    _wantTxt.Visible = true;
                    _wantTxt.CurrentText.text = MogoLanguageUtil.GetContent(110010);
                    break;
                case public_config.CAPTAIN_GAIN_GIVEUP:
                    _giveBtn.Visible = PlayerDataManager.Instance.TeamData.IsCaptain() || PlayerDataManager.Instance.TeamData.TeammateDic.Count == 0;
                    _waitTxt.Visible = !PlayerDataManager.Instance.TeamData.IsCaptain();
                    _finalTxt.Visible = false;
                    _wantTxt.Visible = true;
                    _wantTxt.CurrentText.text = MogoLanguageUtil.GetContent(110011);
                    break;
                case public_config.CAPTAIN_GAIN_SUCCESS:
                    _giveBtn.Visible = false;
                    _waitTxt.Visible = false;
                    _finalTxt.Visible = true;
                    _wantTxt.Visible = false;
                    _particle.Play(true);
                    break;
                case public_config.CAPTAIN_GAIN_FAILURE:
                    _giveBtn.Visible = false;
                    _waitTxt.Visible = false;
                    _finalTxt.Visible = false;
                    _wantTxt.Visible = false;
                    SetImageGray(true);
                    break;
            }
        }

        private void SetImageGray(bool isGray)
        {
            ImageWrapper[] wrappers = gameObject.GetComponentsInChildren<ImageWrapper>();
            for (int i = 0; i < wrappers.Length; i++)
            {
                wrappers[i].SetGray(isGray ? 0 : 1);
            }
        }

        private void RefreshContent()
        {
            SetImageGray(false);
            _particle.Stop();
            RefreshStatusView();
            RefreshMyView();
            RefreshTeammateView();
        }

        private void RefreshMyView()
        {
            if (_data.role_dbid == PlayerAvatar.Player.dbid)
            {
                _levelTxt.CurrentText.text = PlayerAvatar.Player.level.ToString();
                _nameTxt.CurrentText.text = PlayerAvatar.Player.name;
                _fightTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(110020), PlayerAvatar.Player.fight_force);
                _captainImg.Visible = PlayerDataManager.Instance.TeamData.IsCaptain() || PlayerDataManager.Instance.TeamData.TeammateDic.Count == 0;
                _icon.SetIcon(MogoPlayerUtils.GetSmallIcon((int)PlayerAvatar.Player.vocation), OnIconLoaded);
                _guildTxt.CurrentText.text = MogoLanguageUtil.GetContent(110019);
                _guildId = PlayerAvatar.Player.guild_id;
                if (PlayerAvatar.Player.guild_id != 0)
                {
                    GuildDataManager.GetInstance().RequestGuildDataById(PlayerAvatar.Player.guild_id);
                }
                else
                {
                    _guildTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(110019), MogoLanguageUtil.GetContent(102508));
                }
            }
        }

        private void RefreshTeammateView()
        {
            if (_data.role_dbid != PlayerAvatar.Player.dbid)
            {
                if (IsTeamMember(_data.role_dbid))
                {
                    PbTeamMember memberInfo = PlayerDataManager.Instance.TeamData.TeammateDic[_data.role_dbid];
                    _levelTxt.CurrentText.text = memberInfo.level.ToString();
                    _nameTxt.CurrentText.text = memberInfo.name;
                    _fightTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(110020), memberInfo.fight_force);
                    _captainImg.Visible = PlayerDataManager.Instance.TeamData.CaptainId == memberInfo.dbid;
                    _icon.SetIcon(MogoPlayerUtils.GetSmallIcon((int)memberInfo.vocation), OnIconLoaded);
                    _guildTxt.CurrentText.text = MogoLanguageUtil.GetContent(110019);
                    _guildId = memberInfo.guild_id;
                    if (memberInfo.guild_id != 0)
                    {
                        GuildDataManager.GetInstance().RequestGuildDataById(memberInfo.guild_id);
                    }
                    else
                    {
                        _guildTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(110019), MogoLanguageUtil.GetContent(102508));
                    }
                }
                else
                {
                    PbHpMember memberInfo = PlayerDataManager.Instance.TeamInstanceData.GetHpMemberInfo(_data.role_dbid);
                    if (memberInfo == null)
                    {
                        memberInfo = new PbHpMember();
                    }
                    _levelTxt.CurrentText.text = memberInfo.level.ToString();
                    _nameTxt.CurrentText.text = MogoProtoUtils.ParseByteArrToString(memberInfo.name);
                    _fightTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(110020));
                    _captainImg.Visible = PlayerDataManager.Instance.TeamData.CaptainId == memberInfo.dbid;
                    _icon.SetIcon(MogoPlayerUtils.GetSmallIcon((int)memberInfo.vocation), OnIconLoaded);
                    _guildTxt.CurrentText.text = MogoLanguageUtil.GetContent(110019);
                    _guildTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(110019), MogoLanguageUtil.GetContent(102508));
                }
            }
        }

        private bool IsTeamMember(ulong dbid)
        {
            return PlayerDataManager.Instance.TeamData.TeammateDic.ContainsKey(_data.role_dbid);
        }

        private void OnIconLoaded(GameObject go)
        {
            ImageWrapper[] wrappers = go.GetComponentsInChildren<ImageWrapper>();
            for (int i = 0; i < wrappers.Length; i++)
            {
                wrappers[i].SetGray(1);
            }
        }

        private void RefreshGuildInfo(ulong guildId)
        {
            if (_guildId != guildId) { return; }
            PbGuildSimpleInfo info = GuildDataManager.GetInstance().GetGuildSimpleInfo(guildId);
            if (info != null)
            {
                _guildTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(110019), info.guild_name);
            }
        }

        private uint _timerId = 0;
        private int _step;
        private void StartRollPoint()
        {
            if (_hasRoll == false)
            {
                _hasRoll = true;
                ResetTimer();
                _step = 24;
                _timerId = TimerHeap.AddTimer(0, 50, OnStep);
            }
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private void OnStep()
        {
            if (_step <= 0)
            {
                ResetTimer();
                _pointList.SetNumber((int)_data.rand_point);
            }
            else
            {
                int range = UnityEngine.Random.Range(1, 101);
                _pointList.SetNumber(range);
            }
            _step--;
        }

        public override void Show()
        {
            base.Show();
            AddEventListener();
        }

        public override void Hide()
        {
            base.Hide();
            RemoveEventListener();
            ResetTimer();
        }

        private void OnGiveBtnClick()
        {
            TeamInstanceManager.Instance.CaptainItemAssign(_data.role_dbid, (byte)_data.turn_id);
        }

        private void AddEventListener()
        {
            _giveBtn.onClick.AddListener(OnGiveBtnClick);
            EventDispatcher.AddEventListener<ulong>(GuildEvents.Refresh_Guild_Simple_Info, RefreshGuildInfo);
        }

        private void RemoveEventListener()
        {
            _giveBtn.onClick.RemoveListener(OnGiveBtnClick);
            EventDispatcher.RemoveEventListener<ulong>(GuildEvents.Refresh_Guild_Simple_Info, RefreshGuildInfo);
        }

    }
}
