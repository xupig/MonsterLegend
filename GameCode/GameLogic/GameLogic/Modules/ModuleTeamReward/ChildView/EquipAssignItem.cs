﻿using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Utils;
using System.Collections.Generic;

namespace ModuleTeamReward
{
    public class EquipAssignItem : KList.KListItemBase
    {
        private KButton _giveBtn;
        private IconContainer _icon;
        private StateText _levelTxt;
        private StateText _nameTxt;
        private StateText _guildTxt;
        private StateText _fightTxt;

        private ulong _dbid;
        private ulong _guildId;

        public int gridPosition;

        protected override void Awake()
        {
            _giveBtn = GetChildComponent<KButton>("Button_sousuo");
            _icon = AddChildComponent<IconContainer>("Container_icon");
            _levelTxt = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            _guildTxt = GetChildComponent<StateText>("Label_txtGonghui");
            _nameTxt = GetChildComponent<StateText>("Label_txtWanjiamingcheng");
            _fightTxt = GetChildComponent<StateText>("Label_txtZhanli");
        }

        public override object Data
        {
            get
            {
                return _dbid;
            }
            set
            {
                _dbid = (ulong)value;
                RequestPlayerInfo();
            }
        }

        private void RequestPlayerInfo()
        {
            RankingListManager.Instance.GetRankPlayerGuildData(_dbid);
        }

        private void RefreshContent(PbOfflineData data)
        {
            if ((ulong)data.dbid != _dbid) return;
            _icon.SetIcon(MogoPlayerUtils.GetSmallIcon(data.vocation));
            _levelTxt.CurrentText.text = data.level.ToString();
            _nameTxt.CurrentText.text = MogoProtoUtils.ParseByteArrToString(data.name_bytes);
            _fightTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(110020), data.fight_force);
            _guildTxt.CurrentText.text = MogoLanguageUtil.GetContent(110019);
            _guildId = (ulong)data.guild_id;
            if (data.guild_id != 0)
            {
                GuildDataManager.GetInstance().RequestGuildDataById((ulong)data.guild_id);
            }
            else
            {
                _guildTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(110019), MogoLanguageUtil.GetContent(102508));
            }
        }

        private void RefreshGuildInfo(ulong guildId)
        {
            if (_guildId != guildId) { return; }
            PbGuildSimpleInfo info = GuildDataManager.GetInstance().GetGuildSimpleInfo(guildId);
            if (info != null)
            {
                _guildTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(110019), info.guild_name);
            }
        }

        public override void Dispose()
        {
            
        }

        public override void Show()
        {
            base.Show();
            AddEventListener();
        }

        public override void Hide()
        {
            base.Hide();
            RemoveEventListener();
        }

        private void OnGiveBtnClick()
        {
            TeamInstanceManager.Instance.TransferItemToPlayer(_dbid, gridPosition);
            UIManager.Instance.ClosePanel(PanelIdEnum.EquipAssignTips);
            UIManager.Instance.ClosePanel(PanelIdEnum.EquipToolTips);
        }

        private void AddEventListener()
        {
            _giveBtn.onClick.AddListener(OnGiveBtnClick);
            EventDispatcher.AddEventListener<PbOfflineData>(RankingListEvents.GET_RANK_PLAYER_GUILD_INFO, RefreshContent);
            EventDispatcher.AddEventListener<ulong>(GuildEvents.Refresh_Guild_Simple_Info, RefreshGuildInfo);
        }

        private void RemoveEventListener()
        {
            _giveBtn.onClick.RemoveListener(OnGiveBtnClick);
            EventDispatcher.RemoveEventListener<PbOfflineData>(RankingListEvents.GET_RANK_PLAYER_GUILD_INFO, RefreshContent);
            EventDispatcher.RemoveEventListener<ulong>(GuildEvents.Refresh_Guild_Simple_Info, RefreshGuildInfo);
        }

    }
}