﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTeamReward
{
    class TeamRewardStarView : KContainer
    {
        private List<StateImage> _starList;
        private const float PLAY_TIME = 0.15f;

        protected override void Awake()
        {
            base.Awake();
            _starList = new List<StateImage>();
            _starList.Add(GetChildComponent<StateImage>("Container_xingxing/Container_star0/Image_xingxingIcon"));
            _starList.Add(GetChildComponent<StateImage>("Container_xingxing/Container_star1/Image_xingxingIcon"));
            _starList.Add(GetChildComponent<StateImage>("Container_xingxing/Container_star2/Image_xingxingIcon"));
        }

        public void SetStarNum(int num)
        {
            if (num > _starList.Count) num = _starList.Count;
            for (int i = 0; i < _starList.Count; i++)
            {
                _starList[i].transform.localScale = new Vector3(0, 0, 0);
            }
            for (int i = 0; i < num; i++)
            {
                TweenScale tween = TweenScale.Begin(_starList[i].gameObject, 0.5f + i * PLAY_TIME, PLAY_TIME, Vector3.one);
                tween.from = new Vector3(3, 3, 1);
                tween.method = UITweener.Method.EaseOut;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

    }
}
