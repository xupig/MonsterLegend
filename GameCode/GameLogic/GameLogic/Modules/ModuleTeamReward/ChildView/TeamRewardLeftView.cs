﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.Enum;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTeamReward
{
    public class TeamRewardLeftView : KContainer
    {
        private ItemGrid _itemGrid;
        private List<StateImage> _starList;
        private StateImage _upImg;
        private StateImage _downImg;
        private StateText _nameTxt;
        private StateText _vocationTxt;
        private StateText _levelTxt;
        private StateText _fightTxt;
        private StateText _belongTxt;
        private KButton _abandonBtn;
        private KButton _wantBtn;
        private KButton _rollBtn;

        private int _turn;

        protected override void Awake()
        {
            base.Awake();
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _starList = new List<StateImage>();
            _starList.Add(GetChildComponent<StateImage>("Container_xingxing/Container_xingxing00/Image_xingxingIcon"));
            _starList.Add(GetChildComponent<StateImage>("Container_xingxing/Container_xingxing01/Image_xingxingIcon"));
            _starList.Add(GetChildComponent<StateImage>("Container_xingxing/Container_xingxing02/Image_xingxingIcon"));
            _starList.Add(GetChildComponent<StateImage>("Container_xingxing/Container_xingxing03/Image_xingxingIcon"));
            _starList.Add(GetChildComponent<StateImage>("Container_xingxing/Container_xingxing04/Image_xingxingIcon"));
            _downImg = GetChildComponent<StateImage>("Image_xiajiang");
            _upImg = GetChildComponent<StateImage>("Image_tisheng");
            _nameTxt = GetChildComponent<StateText>("Label_txtZhuangbeimingcheng");
            _vocationTxt = GetChildComponent<StateText>("Label_txtSuoshuzhiye");
            _levelTxt = GetChildComponent<StateText>("Label_txtsuoshudengji");
            _fightTxt = GetChildComponent<StateText>("Label_txtZhanliduibi");
            _belongTxt = GetChildComponent<StateText>("Label_txtDangqianguishu");
            _abandonBtn = GetChildComponent<KButton>("Button_fangqi");
            _wantBtn = GetChildComponent<KButton>("Button_xiangyao");
            _rollBtn = GetChildComponent<KButton>("Button_touzi");
        }

        public void RefreshContent(List<PbItemInfo> itemList, int turn)
        {
            if (itemList.Count >= 0)
            {
                _turn = turn;
                _itemGrid.IsShowStar = false;
                BaseItemInfo data = ItemInfoCreator.CreateItemInfo(itemList[0]);
                _itemGrid.SetItemData(data);
                _itemGrid.Context = PanelIdEnum.Chat;
                SetStarNum(0);
                _fightTxt.CurrentText.text = MogoLanguageUtil.GetContent(110008);
                _upImg.Visible = false;
                _downImg.Visible = false;
                if (data != null)
                {
                    _nameTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(110018), data.Name);
                    string vocation = MogoLanguageUtil.GetContent((int)data.UsageVocationRequired);
                    if (PlayerAvatar.Player.vocation != data.UsageVocationRequired)
                    {
                        vocation = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, vocation);
                    }
                    _vocationTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(110006), vocation);
                    _levelTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(110007), data.UsageLevelRequired);
                    if (data.Type == BagItemType.Equip)
                    {
                        EquipItemInfo equipData = data as EquipItemInfo;
                        _fightTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(110008), equipData.FightForce);
                        SetStarNum(equipData.Star);
                        _upImg.Visible = equipData.FightForce > GetMyEquipFightForce(equipData);
                        _downImg.Visible = equipData.FightForce < GetMyEquipFightForce(equipData);
                        if (PlayerAvatar.Player.vocation != data.UsageVocationRequired)
                        {
                            HideButton();
                        }
                    }
                }
                //_fightTxt.CurrentText.text = MogoLanguageUtil.GetContent(110008);
                //_upImg.Visible = false;
                //_downImg.Visible = false;
            }
        }

        private int GetMyEquipFightForce(EquipItemInfo equipData)
        {
            BagData bodyEquipData = PlayerDataManager.Instance.BagData.GetBodyEquipBagData();
            EquipItemInfo leftInfo;
            if (equipData.SubType == EquipType.ring)
            {
                EquipItemInfo rightRing = bodyEquipData.GetItemInfo(public_config.EQUIP_POS_RIGHTRING) as EquipItemInfo;
                EquipItemInfo leftRing = bodyEquipData.GetItemInfo(public_config.EQUIP_POS_LEFTRING) as EquipItemInfo;
                if (leftRing == null || rightRing == null)
                {
                    leftInfo = leftRing == null ? leftRing : rightRing;
                }
                else
                {
                    leftInfo = rightRing.FightForce < leftRing.FightForce ? rightRing : leftRing;
                }
            }
            else
            {
                leftInfo = bodyEquipData.GetItemInfo((int)equipData.SubType) as EquipItemInfo;
            }
            if (leftInfo != null)
            {
                return leftInfo.FightForce;
            }
            return 0;
        }

        public void RefreshBelongContent(ulong giveId)
        {
            if (PlayerDataManager.Instance.TeamData.TeammateDic.ContainsKey(giveId))
            {
                PbTeamMember memberInfo = PlayerDataManager.Instance.TeamData.TeammateDic[giveId];
                _belongTxt.CurrentText.text = MogoLanguageUtil.GetContent(110009, memberInfo.name);
            }
            else
            {
                _belongTxt.CurrentText.text = MogoLanguageUtil.GetContent(110009, "");
            }
        }

        private void SetStarNum(int num)
        {
            if (num > _starList.Count) num = _starList.Count;
            for (int i = 0; i < _starList.Count; i++)
            {
                _starList[i].Visible = i < num;
            }
        }

        public void SetWantBtnStatus(int mode)
        {
            _abandonBtn.Visible = true;
            if (mode == public_config.CAPTAIN_MODE_MASTER)
            {
                _wantBtn.Visible = true;
                _rollBtn.Visible = false;
            }
            else
            {
                _wantBtn.Visible = false;
                _rollBtn.Visible = true;
            }
        }

        public void HideButton()
        {
            _abandonBtn.Visible = false;
            _wantBtn.Visible = false;
            _rollBtn.Visible = false;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _abandonBtn.onClick.AddListener(OnAbandonClick);
            _wantBtn.onClick.AddListener(OnWantClick);
            _rollBtn.onClick.AddListener(OnWantClick);
        }

        private void RemoveEventListener()
        {
            _abandonBtn.onClick.RemoveListener(OnAbandonClick);
            _wantBtn.onClick.RemoveListener(OnWantClick);
            _rollBtn.onClick.RemoveListener(OnWantClick);
        }

        private void OnWantClick()
        {
            TeamInstanceManager.Instance.CaptainItemApply(PlayerAvatar.Player.dbid, (byte)_turn, public_config.CAPTAIN_ITEM_APPLY_WANT);
        }

        private void OnAbandonClick()
        {
            TeamInstanceManager.Instance.CaptainItemApply(PlayerAvatar.Player.dbid, (byte)_turn, public_config.CAPTAIN_ITEM_APPLY_GIVEUP);
        }

    }
}
