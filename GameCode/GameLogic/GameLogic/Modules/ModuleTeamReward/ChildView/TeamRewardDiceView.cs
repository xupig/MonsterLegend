﻿using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using Mogo.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTeamReward
{
    class TeamRewardDiceView : KContainer
    {
        private StateImage _maskImage;
        private ArtNumberList _pointList;
        private Locater _numberLocater;
        private List<StateImage> _diceList;

        private List<int> _diceOrder;
        private int _currentStep;
        private uint _timerId;

        private int _point;

        protected override void Awake()
        {
            base.Awake();
            _maskImage = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            MogoUtils.AdaptScreen(_maskImage);
            _pointList = AddChildComponent<ArtNumberList>("Container_shuzi/List_shaizi");
            _numberLocater = AddChildComponent<Locater>("Container_shuzi");
            _diceList = new List<StateImage>();
            for (int i = 1; i <= 6; i++)
            {
                _diceList.Add(GetChildComponent<StateImage>(string.Format("Container_shaizi/Image_shaizi{0}", i)));
            }
            _diceOrder = new List<int>() { 1, 4, 5, 6, 2, 4, 5, 6, 3, 4, 5, 6 };
        }

        public void RefreshContent(int dicePoint)
        {
            this.Visible = true;
            _point = dicePoint;
            HideAllImage();
            _currentStep = 0;
            _diceList[_diceOrder[_currentStep] - 1].Visible = true;
            ResetTimer();
            _timerId = TimerHeap.AddTimer(50, 50, OnStep);
        }

        private void OnStep()
        {
            int currentDice = _currentStep % _diceOrder.Count;
            _diceList[_diceOrder[currentDice] - 1].Visible = false;
            _currentStep++;
            if (_currentStep >= 24)
            {
                ResetTimer();
                ShowDicePoint();
                return;
            }
            currentDice = _currentStep % _diceOrder.Count;
            _diceList[_diceOrder[currentDice] - 1].Visible = true;
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private void HideAllImage()
        {
            for (int i = 0; i < _diceList.Count; i++)
            {
                _diceList[i].Visible = false;
            }
        }

        private void ShowDicePoint()
        {
            _pointList.SetNumber(_point);
            _pointList.RecalculateSize();
            RectTransform rect = _pointList.transform as RectTransform;
            rect.anchoredPosition = new Vector2(_numberLocater.Width / 2 - rect.sizeDelta.x / 2, rect.anchoredPosition.y);
            StartCoroutine(WaitAndClose(1.0f));
        }

        private IEnumerator WaitAndClose(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            this.Visible = false;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            ResetTimer();
        }

    }
}
