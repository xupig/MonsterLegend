﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleMainUI;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleTeamReward
{
    public class TeamRewardChatEntryPanel : ChatEntryPanel
    {
        private Locater _locater;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TeamRewardChatEntry; }
        }

        protected override void Awake()
        {
            base.Awake();
            _locater = _bottomContainer.gameObject.AddComponent<Locater>();
            _locater.X = 0;
            _locater.Y = -((_locater.transform.parent as RectTransform).sizeDelta.y - _locater.Height);
        }
    }
}