﻿using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMall
{
    public class MallTotalCost : KContainer
    {
        private int _buyNum;
        private int _couponItemId;
        private market_data _marketData;

        private StateText _textTotalCost;

        protected override void Awake()
        {
            _textTotalCost = GetChildComponent<StateText>("Label_txtSjine");
            InitIcon();
        }

        private void InitIcon()
        {
            AddChildComponent<IconContainer>("Container_icon").SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_DIAMOND));
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            EventDispatcher.AddEventListener<int>(MallEvent.SelectCoupon, OnSelectCoupon);
        }

        private void RemoveListener()
        {
            EventDispatcher.RemoveEventListener<int>(MallEvent.SelectCoupon, OnSelectCoupon);
        }

        private void OnSelectCoupon(int couponItemId)
        {
            _couponItemId = couponItemId;
            RefreshCost();
        }

        public void SetData(market_data marketData, int buyNum, int couponItemId)
        {
            _marketData = marketData;
            _buyNum = buyNum;
            _couponItemId = couponItemId;
            RefreshCost();
        }

        private void RefreshCost()
        {
            int cost = GetBuyItemCost();
            _textTotalCost.CurrentText.text = string.Format("x{0}", cost);
            if (cost > PlayerAvatar.Player.money_diamond)
            {
                _textTotalCost.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_RED);
            }
            else
            {
                _textTotalCost.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_WHITE);
            }
        }

        private int GetBuyItemCost()
        {
            int perCost = mall_helper.GetSellPrice(_marketData);
            int totalCost = perCost * _buyNum;
            if (_couponItemId != MallCouponItem.INVALID_ID)
            {
                market_coupon marketCouponData = XMLManager.market_coupon[_couponItemId];
                if (totalCost >= marketCouponData.__price_limit)
                {
                    totalCost -= marketCouponData.__coupon_effect;
                }
            }
            return totalCost;
        }
    }
}
