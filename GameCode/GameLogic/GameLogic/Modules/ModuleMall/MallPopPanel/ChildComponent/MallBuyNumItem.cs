﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/3/17 14:32:09
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using Common.Data;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using UnityEngine;

namespace ModuleMall
{
    public class MallBuyNumItem : KContainer
    {
        private const int BUY_MAX = 999;
        private const int BUY_MIN = 1;
        private const int DOUBLE_ADD_VALUE = 10;
        private const int DOUBLE_MINUS_VALUE = 10;

        private int _buyNum = BUY_MIN;
        public market_data _marketData;

        private KButton _btnAdd;
        private KButton _btnDoubleAdd;
        private KButton _btnMinus;
        private KButton _btnDoubleMinus;
        private StateText _textBuyNum;

        public KComponentEvent<MallBuyNumItem, int> onNumChange = new KComponentEvent<MallBuyNumItem, int>();

        protected override void Awake()
        {
            _btnAdd = GetChildComponent<KButton>("Button_tianjia");
            _btnDoubleAdd = GetChildComponent<KButton>("Button_shuangjia");
            _btnMinus = GetChildComponent<KButton>("Button_jianshao");
            _btnDoubleMinus = GetChildComponent<KButton>("Button_shuangjian");
            _textBuyNum = GetChildComponent<StateText>("Label_txtShuliang");

            AddEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _btnAdd.onClick.AddListener(OnAdd);
            _btnDoubleAdd.onClick.AddListener(OnDoubleAdd);
            _btnMinus.onClick.AddListener(OnMinus);
            _btnDoubleMinus.onClick.AddListener(OnDoubleMinus);
        }

        private void RemoveEventListener()
        {
            _btnAdd.onClick.RemoveListener(OnAdd);
            _btnDoubleAdd.onClick.RemoveListener(OnDoubleAdd);
            _btnMinus.onClick.RemoveListener(OnMinus);
            _btnDoubleMinus.onClick.RemoveListener(OnDoubleMinus);
        }

        public void SetData(market_data marketData)
        {
            _marketData = marketData;
            _buyNum = BUY_MIN;
            Refresh();
        }

        public int BuyNum
        {
            get
            {
                return _buyNum;
            }
            set
            {
                _buyNum = value;
                Refresh();
                onNumChange.Invoke(this, _buyNum);
            }
        }

        private void OnAdd()
        {
            BuyNum = BuyNum + 1;
        }

        private void OnDoubleAdd()
        {
            if (BuyNum == 1)
            {
                BuyNum = Math.Min(10, GetBuyMax());
            }
            else
            {
                BuyNum = Math.Min(BuyNum + DOUBLE_ADD_VALUE, GetBuyMax());
            }
        }

        private void OnMinus()
        {
            BuyNum = BuyNum - 1;
        }

        private void OnDoubleMinus()
        {
            BuyNum = Math.Max(BuyNum - DOUBLE_MINUS_VALUE, 1);
        }

        public void Refresh()
        {
            RefreshBtn();
            RefreshText();
        }

        private void RefreshBtn()
        {
            _btnMinus.interactable = BuyNum > 1;
            _btnDoubleMinus.interactable = BuyNum > 1;
            _btnAdd.interactable = BuyNum < GetBuyMax();
            _btnDoubleAdd.interactable = BuyNum < GetBuyMax();
        }

        private void RefreshText()
        {
            _textBuyNum.CurrentText.text = BuyNum.ToString();
        }

        private int GetBuyMax()
        {
            MallDataManager mallDataManager = PlayerDataManager.Instance.MallDataManager;
            int dailyCanBuyNum = mallDataManager.GetDailyCanBuyNum(_marketData);
            dailyCanBuyNum = dailyCanBuyNum < BUY_MIN ? BUY_MIN : dailyCanBuyNum;
            int totalCanBuyNum = mallDataManager.GetTotalCanBuyNum(_marketData);
            totalCanBuyNum = totalCanBuyNum < BUY_MIN ? BUY_MIN : totalCanBuyNum;
            int flashSaleCanBuyNum = BUY_MAX;
            if (_marketData.__sell_mode == (int) MallSellMode.limitNum)
            {
                flashSaleCanBuyNum = 1;
            }
            return Mathf.Min(dailyCanBuyNum, BUY_MAX, totalCanBuyNum, flashSaleCanBuyNum);
        }
    }
}
