﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/3/23 19:09:29
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleMall
{
    public class MallFriendItem : KList.KListItemBase
    {
        private PbFriendAvatarInfo _data;

        private StateText _textName;
        private StateText _textLv;
        private StateText _textLv1;
        private StateText _textFight;
        private StateText _textProgress;
        private KProgressBar _progressBar;
        private LoveStarItemList _loveStarItemList;
        private IconContainer goHead;

        protected override void Awake()
        {
            _textName = GetChildComponent<StateText>("Label_txtwanjiamingzi");
            _textLv = GetChildComponent<StateText>("Label_txtDengji");
            _textLv1 = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            _textFight = GetChildComponent<StateText>("Label_txtZhanli02");
            goHead = AddChildComponent<IconContainer>("Container_icon");
            _loveStarItemList = gameObject.AddComponent<LoveStarItemList>();
            InitProgress();
        }

        private void InitProgress()
        {
            _textProgress = GetChildComponent<StateText>("Label_txtJindu");
            _progressBar = GetChildComponent<KProgressBar>("ProgressBar_jindu");
        }

        public override void Dispose()
        {
            _data = null;
        }

        public override object Data
        {
            set
            {
                _data = value as PbFriendAvatarInfo;
                Refresh();
            }
        }

        private void Refresh()
        {
            RefreshFriendInfo();
            RefreshHead();
            RefreshDegree();
            RefreshProgressDegree();
        }

        private void RefreshFriendInfo()
        {
            _textName.CurrentText.text = _data.name;
            _textLv.CurrentText.text = string.Format("Lv:{0}", _data.level);
            _textLv1.CurrentText.text = _data.level.ToString();
            _textFight.CurrentText.text = _data.fight.ToString();
        }

        private void RefreshHead()
        {
            goHead.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_data.vocation));
        }

        private void RefreshDegree()
        {
            float level = intimate_helper.GetIntimateLevel((int) _data.degree);
            _loveStarItemList.SetLevel(level);
        }

        private void RefreshProgressDegree()
        {
            int max = intimate_helper.GetIntimateLevelMax((int) _data.degree);
            _progressBar.Value = (_data.degree) / (float) max;
            _textProgress.CurrentText.text = string.Format("{0}/{1}", _data.degree, max);
        }

        public override void OnPointerClick(PointerEventData evtData)
        {
            if (evtData.dragging == true)
            {
                return;
            }
            UIManager.Instance.ClosePanel(PanelIdEnum.MallPop);
            EventDispatcher.TriggerEvent<PbFriendAvatarInfo>(MallEvent.SendFriend, _data);
        }
    }
}
