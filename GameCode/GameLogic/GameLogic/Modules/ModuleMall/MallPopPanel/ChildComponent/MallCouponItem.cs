﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/3/17 14:34:59
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleMall
{
    public class MallCouponItem : KContainer
    {
        public const int INVALID_ID = -1;
        private bool _isShowingSelectableBox = false;
        private int _couponItemId = INVALID_ID;
        public int CouponItemId
        {
            get
            {
                return _couponItemId;
            }
        }
        private market_data _marketData;
        
        private KButton _btnSelectableBox;
        private KScrollView _scrollView;
        private KList _list;
        private GameObject _goCoupon;
        private StateText _textCouponName;
        private KButton _btnMask;

        protected override void Awake()
        {
            _btnSelectableBox = GetChildComponent<KButton>("Button_xiala");
            _scrollView = GetChildComponent<KScrollView>("Container_youhuijuan/ScrollView_youhuiquan");
            _btnMask = GetChildComponent<KButton>("Container_youhuijuan/ScrollView_youhuiquan/maskBtn");
            _goCoupon = GetChild("Container_youhuijuan");
            _textCouponName = GetChildComponent<StateText>("Label_txtKeyong");
            InitList();
        }

        private void InitList()
        {
            _list = GetChildComponent<KList>("Container_youhuijuan/ScrollView_youhuiquan/mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _list.SetPadding(15, 0, 5, 0);
            _list.SetGap(22, 0);
        }

        protected override void OnEnable()
        {
            _isShowingSelectableBox = false;
            _couponItemId = INVALID_ID;
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _btnSelectableBox.onClick.AddListener(OnShowSelectableBox);
            _btnMask.onClick.AddListener(OnMaskClick);
            EventDispatcher.AddEventListener<int>(MallEvent.SelectCoupon, OnSelectCoupon);
        }

        private void RemoveEventListener()
        {
            _btnSelectableBox.onClick.RemoveListener(OnShowSelectableBox);
            _btnMask.onClick.RemoveListener(OnMaskClick);
            EventDispatcher.RemoveEventListener<int>(MallEvent.SelectCoupon, OnSelectCoupon);
        }

        private void OnShowSelectableBox()
        {
            _isShowingSelectableBox = !_isShowingSelectableBox;
            Refresh();
        }

        private void OnSelectCoupon(int mallCouponId)
        {
            _isShowingSelectableBox = false;
            _couponItemId = mallCouponId;
            Refresh();
        }

        private void OnMaskClick()
        {
            _isShowingSelectableBox = false;
            Refresh();
        }

        public void SetData(market_data marketData)
        {
            _marketData = marketData;
            Refresh();
        }

        private void Refresh()
        {
            RefreshSelectableBox();
            RefreshLabel();
        }

        private void RefreshSelectableBox()
        {
            _goCoupon.SetActive(_isShowingSelectableBox);
            if (_isShowingSelectableBox)
            {
                _list.RemoveAll();
                List<int> couponList = GetCouponListByMallType((MallType)_marketData.__item_page);
                _list.SetDataList<Coupon>(couponList);
                _scrollView.ResetContentPosition();
            }
        }

        private void RefreshLabel()
        {
            if (IsMallCouponIdValid())
            {
                RefreshLabelWhenCouponIdValid();
            }
            else
            {
                RefreshLabelWhenCouponIdInvalid();
            }
        }

        private void RefreshLabelWhenCouponIdValid()
        {
            _textCouponName.CurrentText.text = item_helper.GetName(_couponItemId);
            _btnSelectableBox.interactable = true;
        }

        private void RefreshLabelWhenCouponIdInvalid()
        {
            int count = GetCurrentValidCouponCount();
            if (count == 0)
            {
                _textCouponName.ChangeAllStateText(MogoLanguageUtil.GetString(LangEnum.MALL_NO_DISCOUNT_COUPON));
                _btnSelectableBox.interactable = false;
            }
            else
            {
                _textCouponName.ChangeAllStateText(string.Format(MogoLanguageUtil.GetString(LangEnum.MALL_COUPON_COUNT), count.ToString()));
                _btnSelectableBox.interactable = true;
            }
        }

        public bool IsMallCouponIdValid()
        {
            return _couponItemId != INVALID_ID;
        }

        private List<int> GetCouponListByMallType(MallType mallType)
        {
            List<int> couponList = new List<int>();
            BagData itemBagData = PlayerDataManager.Instance.BagData.GetItemBagData();
            MallDataManager mallDataManager = PlayerDataManager.Instance.MallDataManager;
            foreach (int itemId in mallDataManager.MallCouponDict[mallType])
            {
                int num = itemBagData.GetItemNum(itemId);
                if ((couponList.Count == 0) && (num > 0))
                {
                    //第一项是清除按钮
                    couponList.Add(INVALID_ID);
                }
                for (int i = 0; i < num; i++)
                {
                    couponList.Add(itemId);
                }
            }
            return couponList;
        }

        public int GetCurrentValidCouponCount()
        {
            List<int> couponList = GetCouponListByMallType((MallType) _marketData.__item_page);
            //有一个清除字段
            return couponList.Count > 1 ? (couponList.Count - 1) : 0;
        }

        private class Coupon : KList.KListItemBase
        {
            private int _itemId;

            private StateText _textName;

            protected override void Awake()
            {
                _textName = GetChildComponent<StateText>("Label_label");
            }

            public override void Dispose()
            {
            }

            public override void OnPointerClick(PointerEventData evtData)
            {
                if (!evtData.dragging)
                {
                    EventDispatcher.TriggerEvent<int>(MallEvent.SelectCoupon, _itemId);
                }
            }

            public override object Data
            {
                set
                {
                    _itemId = (int)value;
                    UpdateView();
                }
            }

            private void UpdateView()
            {
                if (_itemId == MallCouponItem.INVALID_ID)
                {
                    _textName.ChangeAllStateText(MogoLanguageUtil.GetString(LangEnum.MALL_CLEAR_COUPON));
                }
                else
                {
                    string name = item_helper.GetName(_itemId);
                    _textName.ChangeAllStateText(name);
                }
            }
        }
    }
}
