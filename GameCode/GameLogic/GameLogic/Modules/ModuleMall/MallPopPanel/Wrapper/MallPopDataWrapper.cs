﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/21 20:00:44
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs.ProtoBuf;
using GameData;

namespace ModuleMall
{
    public class MallPopDataWrapper
    {
        public MallPopViewType PopViewType;
        public market_data MarketData;
        public PbFriendAvatarInfo FriendAvatarInfo;

        public MallPopDataWrapper(MallPopViewType mallPopViewType, market_data marketData)
        {
            this.PopViewType = mallPopViewType;
            this.MarketData = marketData;
        }

        public MallPopDataWrapper(MallPopViewType mallPopViewType, market_data marketData, PbFriendAvatarInfo friendAvatarInfo)
            : this(mallPopViewType, marketData)
        {
            this.FriendAvatarInfo = friendAvatarInfo;
        }
    }
}
