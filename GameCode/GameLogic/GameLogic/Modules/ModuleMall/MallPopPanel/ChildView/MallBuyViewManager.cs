﻿using Common.Base;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMall
{
    public class MallBuyViewManager : KContainer
    {
        private MallBuyNumItem _buyNumItem;
        private GameObject _goBuyGroup;
        private KButton _btnBuy;
        private KButton _btnClose;
        private GameObject _goFriendInfo;
        private GameObject _goSelectNum;

        private MallBaseBuyView _currentBuyView;

        private MallBuyView _buyView;
        private MallBuyGroupView _buyGroupView;
        private MallGiveView _giveView;

        protected override void Awake()
        {
            InitChildComponent();
            _buyView = gameObject.AddComponent<MallBuyView>();
            _buyGroupView = gameObject.AddComponent<MallBuyGroupView>();
            _giveView = gameObject.AddComponent<MallGiveView>();

            AddListener();
        }

        private void InitChildComponent()
        {
            //不在MallBaseBuyView添加组件，避免重复添加
            AddChildComponent<MallCouponItem>("Container_coupon");
            AddChildComponent<MallTotalCost>("Container_totalCost");
            AddChildComponent<Locater>("Container_buyNum");
            AddChildComponent<Locater>("Container_buyInfo");
            AddChildComponent<Locater>("Container_friendInfo");
            _buyNumItem = AddChildComponent<MallBuyNumItem>("Container_buyNum");
            _goBuyGroup = GetChild("Container_selectTime");
            _goFriendInfo = GetChild("Container_friendInfo");
            _goSelectNum = GetChild("Container_buyInfo");
            _btnBuy = GetChildComponent<KButton>("Button_zhong");
            _btnClose = GetChildComponent<KButton>("Container_BG/Button_close");
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnBuy.onClick.AddListener(OnBuy);
            _btnClose.onClick.AddListener(OnClose);
        }

        private void RemoveListener()
        {
            _btnBuy.onClick.RemoveListener(OnBuy);
            _btnClose.onClick.RemoveListener(OnClose);
        }

        private void OnBuy()
        {
            _currentBuyView.OnBuy();
        }

        private void OnClose()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.MallPop);
        }

        public void Show(MallPopDataWrapper wrapper)
        {
            Visible = true;
            HideComponent();
            switch (wrapper.PopViewType)
            {
                case MallPopViewType.buy:
                    _currentBuyView = _buyView;
                    break;
                case MallPopViewType.buyGroupItem:
                    _currentBuyView = _buyGroupView;
                    break;
                case MallPopViewType.give:
                    _currentBuyView = _giveView;
                    break;
            }
            _currentBuyView.enabled = true;
            _currentBuyView.OnShow();
            _currentBuyView.SetData(wrapper);
        }

        private void HideComponent()
        {
            _buyView.enabled = false;
            _buyGroupView.enabled = false;
            _giveView.enabled = false;
            _buyNumItem.Visible = false;
            _goBuyGroup.SetActive(false);
            _goFriendInfo.SetActive(false);
            _goSelectNum.SetActive(false);
        }
    }
}
