﻿using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMall
{
    public class MallGiveView : MallBuyView
    {
        private float InitFriendInfoY;

        private PbFriendAvatarInfo _friendAvatarInfo;
        private GameObject _goFriendInfo;
        private Locater _locaterFriendInfo;
        private StateText _textFriendName;

        protected override void Awake()
        {
            base.Awake();
            _goFriendInfo = GetChild("Container_friendInfo");
            _locaterFriendInfo = GetChildComponent<Locater>("Container_friendInfo");
            InitFriendInfoY = _locaterFriendInfo.Y;
            _textFriendName = GetChildComponent<StateText>("Container_friendInfo/Label_txtwanjiamingzi");
        }

        public override void OnShow()
        {
            _goFriendInfo.SetActive(true);
            buyNumItem.Visible = true;
        }

        public override void OnBuy()
        {
            MallManager.Instance.RequestGive(marketData.__id, buyNumItem.BuyNum, _friendAvatarInfo.dbid, couponItem.CouponItemId);
        }

        public override void SetData(MallPopDataWrapper wrapper)
        {
            _friendAvatarInfo = wrapper.FriendAvatarInfo;
            base.SetData(wrapper);
            _textFriendName.CurrentText.text = _friendAvatarInfo.name;
        }

        protected override void RefreshLayout(market_data marketData)
        {
            base.RefreshLayout(marketData);
            if (couponItem.GetCurrentValidCouponCount() == 0)
            {
                couponItem.Visible = false;
                _locaterFriendInfo.Y = InitFriendInfoY - BUY_NUM_OFFSET;
                locaterBuyNum.Y = InitY - BUY_NUM_OFFSET;
            }
            else
            {
                couponItem.Visible = true;
                locaterBuyNum.Y = InitY;
                _locaterFriendInfo.Y = InitFriendInfoY;
            }
        }
    }
}
