﻿using Common.Base;
using Common.Data;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMall
{
    public class MallBuyGroupView : MallBaseBuyView
    {
        private float InitBuyGoupY;

        private MallGroupData _mallGroupData;

        private Locater _locaterBuyGroup;
        private KToggleGroup _toggleGroup;

        protected override void Awake()
        {
            base.Awake();
            _locaterBuyGroup = AddChildComponent<Locater>("Container_selectTime");
            _toggleGroup = GetChildComponent<KToggleGroup>("Container_selectTime/ToggleGroup_qixian");
        }

        protected override void OnEnable()
        {
            AddListener();
        }


        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnCategoryChanged);
        }

        private void RemoveListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnCategoryChanged);
        }

        private MallGroupData GetMallGroupData(market_data marketData)
        {
            MallDataManager mallDataManager = PlayerDataManager.Instance.MallDataManager;
            return mallDataManager.MallGroupDict[marketData.__show_item_id];
        }

        private void OnCategoryChanged(KToggleGroup toggleGroup, int index)
        {
            _mallGroupData = GetMallGroupData(marketData);
            marketData = _mallGroupData.GetMarketDataByIndex(index);
            Refresh();
        }

        public override void OnBuy()
        {
            switch ((MallType)marketData.__item_page)
            {
                case MallType.fashion:
                    int fashionId = item_helper.GetFashionId(marketData.__item_id);
                    if (PlayerDataManager.Instance.FashionData.HaveFashion(fashionId))
                    {
                        MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(36514), ConfirmAction, CancelAction);
                        return;
                    }
                    break;
                case MallType.mount:
                    int mountId = item_helper.GetMountId(marketData.__item_id);
                    if (PlayerDataManager.Instance.mountData.HasMount(mountId))
                    {
                        MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(112517), ConfirmAction, CancelAction);
                        return;
                    }
                    break;
            }
            MallManager.Instance.RequestBuy(marketData.__id, 1, couponItem.CouponItemId);
        }

        private void ConfirmAction()
        {
            MallManager.Instance.RequestBuy(marketData.__id, 1, couponItem.CouponItemId);
        }

        private void CancelAction()
        {
        }

        private bool once = true;
        public override void OnShow()
        {
            _locaterBuyGroup.gameObject.SetActive(true);
            if (once)
            {
                once = false;
                InitBuyGoupY = _locaterBuyGroup.Y;
            }
        }

        public override void SetData(MallPopDataWrapper wrapper)
        {
            marketData = wrapper.MarketData;
            _toggleGroup.SelectIndex = 0;
            OnCategoryChanged(_toggleGroup, 0);
        }

        private void Refresh()
        {
            couponItem.SetData(marketData);
            totalCost.SetData(marketData, 1, couponItem.CouponItemId);
            RefreshName(marketData);
            RefreshLayout(marketData);
        }

        protected override void RefreshLayout(market_data marketData)
        {
            if (couponItem.GetCurrentValidCouponCount() == 0)
            {
                couponItem.Visible = false;
                _locaterBuyGroup.Y = InitBuyGoupY - BUY_NUM_OFFSET;
            }
            else
            {
                couponItem.Visible = true;
                _locaterBuyGroup.Y = InitBuyGoupY;
            }
        }
    }
}
