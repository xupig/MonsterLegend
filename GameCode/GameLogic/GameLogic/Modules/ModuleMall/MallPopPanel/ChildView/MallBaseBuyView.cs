﻿using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMall
{
    abstract public class MallBaseBuyView : KContainer
    {
        protected const float BUY_NUM_OFFSET = 66;
        protected float InitY;
        protected market_data marketData;

        private StateText _textName;
        protected Locater locaterBuyNum;
        protected MallCouponItem couponItem;
        protected MallTotalCost totalCost;

        protected override void Awake()
        {
            couponItem = GetChildComponent<MallCouponItem>("Container_coupon");
            totalCost = GetChildComponent<MallTotalCost>("Container_totalCost");
            _textName = GetChildComponent<StateText>("Label_txtbiaoti");
            locaterBuyNum = GetChildComponent<Locater>("Container_buyNum");
            InitY = locaterBuyNum.Y;
        }

        protected void RefreshName(market_data marketData)
        {
            _textName.CurrentText.text = item_helper.GetName(marketData.__item_id);
        }

        protected virtual void RefreshLayout(market_data marketData)
        {
            if (couponItem.GetCurrentValidCouponCount() == 0)
            {
                couponItem.Visible = false;
                locaterBuyNum.Y = InitY - BUY_NUM_OFFSET;
            }
            else
            {
                couponItem.Visible = true;
                locaterBuyNum.Y = InitY;
            }
        }

        public abstract void OnShow();

        public abstract void OnBuy();

        public abstract void SetData(MallPopDataWrapper wrapper);
    }
}
