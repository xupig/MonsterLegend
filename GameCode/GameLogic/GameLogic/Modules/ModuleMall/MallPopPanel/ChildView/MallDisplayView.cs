﻿#region 模块信息
/*==========================================
// 文件名：MallDisplayView
// 创建者：陈铨
// 创建日期：2015/3/12 16:14:47
// 描述说明：
// 其他：
//==========================================*/
#endregion

using ACTSystem;
using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using Mogo.Util;
using UnityEngine;
using ShaderUtils;

namespace ModuleMall
{
    public class MallDisplayView : KContainer
    {
        private market_data _marketData;
        private ACTActor _actor;
        private ACTActor _mount;

        private KButton _btnClose;
        private KButton _btnBuy;
        private ModelComponent _model;
        private int currentMountId;

        protected override void Awake()
        {
            _btnClose = GetChildComponent<KButton>("Button_fanhui");
            _btnBuy = GetChildComponent<KButton>("Button_zhong");
            _model = AddChildComponent<ModelComponent>("Container_model");
            MogoUtils.AdaptScreen(GetChildComponent<StateImage>("ScaleImage_sharedZhezhao"));
            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(OnClose);
            _btnBuy.onClick.AddListener(OnBuy);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(OnClose);
            _btnBuy.onClick.RemoveListener(OnBuy);
        }

        protected override void OnEnable()
        {
            UIManager.Instance.DisablePanelCanvas(PanelIdEnum.Mall);
        }

        protected override void OnDisable()
        {
            // 关闭界面的时候清除人物模型和坐骑模型
            if (_mount != null)
            {
                _model.ClearModelGameObject();
                Destroy(_mount.gameObject);
                _actor = null;
                _mount = null;
            }
            UIManager.Instance.EnablePanelCanvas(PanelIdEnum.Mall);
        }

        public void SetData(market_data marketData)
        {
            Visible = true;
            _marketData = marketData;
            _model.LoadPlayerModel(ModelLoadCallback);
            RefreshBtn();
        }

        public void RefreshModel()
        {
            switch ((MallType)_marketData.__item_page)
            {
                case MallType.wing:
                    ShowModelWing();
                    break;
                case MallType.fashion:
                    ShowModelFasion();
                    break;
                case MallType.mount:
                    ShowModelMount();
                    break;
            }
        }

        private void ModelLoadCallback(ACTActor actor)
        {
            _actor = actor;
            _actor.gameObject.transform.localRotation = Quaternion.Euler(0, 180f, 0);
            RefreshModel();
        }

        private void SetModelEquipShader()
        {
            const string shaderName = "MOGO2/Character/Player";
            Shader shader = ShaderRuntime.loader.Find(shaderName);
            _actor.equipController.equipWeapon.ChangeEquipWeaponShader(shader);
        }

        private void SetModelShader(Transform tran)
        {
            const string shaderName = "MOGO2/Character/Player";
            SkinnedMeshRenderer renderer = tran.GetComponent<SkinnedMeshRenderer>();
            Material material = renderer.material;
            Shader shader = ShaderRuntime.loader.Find(shaderName);
            if (shader != null)
            {
                material.shader = shader;
            }
        }

        private void OnBuy()
        {
            OnClose();
            if (mall_helper.IsGroup(_marketData))
            {
                MallPopDataWrapper wrapper = new MallPopDataWrapper(MallPopViewType.buyGroupItem, _marketData);
                UIManager.Instance.ShowPanel(PanelIdEnum.MallPop, wrapper);
            }
            else
            {
                MallPopDataWrapper wrapper = new MallPopDataWrapper(MallPopViewType.buy, _marketData);
                UIManager.Instance.ShowPanel(PanelIdEnum.MallPop, wrapper);
            }
        }

        private void OnClose()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.MallPop);
        }

        private void ShowModelWing()
        {
            int wingId = item_helper.GetWingId(_marketData.__item_id);
            int wingModelId = PlayerDataManager.Instance.WingData.GetItemDataById(wingId).ModelId;
            _actor.equipController.equipWing.onLoadEquipFinished = OnWingGoLoaded;
            _actor.equipController.equipWing.PutOn(wingModelId);
        }

        private void ShowModelFasion()
        {
            int fashionId = item_helper.GetFashionId(_marketData.__item_id);
            int clothId = fashion_helper.GetClothId(fashion_helper.GetFashionCfg(fashionId), PlayerAvatar.Player.vocation);
            if (clothId != 0)
            {
                _actor.equipController.equipCloth.onLoadEquipFinished = OnClothLoaded;
                _actor.equipController.equipCloth.PutOn(clothId);
            }
            int weaponId = fashion_helper.GetWeaponId(fashion_helper.GetFashionCfg(fashionId), PlayerAvatar.Player.vocation);
            if (weaponId != 0)
            {
                _actor.equipController.equipWeapon.onLoadEquipFinished = OnClothLoaded;
                _actor.equipController.equipWeapon.PutOn(weaponId);
            }
        }

        private void ShowModelMount()
        {
            int mountId = item_helper.GetMountId(_marketData.__item_id);
            if (mountId != currentMountId || _mount == null)
            {
                _model.LoadMountModel(mountId, OnMountLoaded);
                currentMountId = mountId;
            }
            else
            {
                _mount.visible = true;
                _mount.transform.localEulerAngles = new Vector3(0, 100, 0);
                _actor.animationController.PlayRidingAction();
            }
        }

        private void OnMountLoaded(ACTActor mountActor)
        {
            _mount = mountActor;
            if (_actor != null)
            {
                _model.IsOnMount = true;
                _actor.rideController.Mount(_mount, true);
                _actor.transform.localScale = Vector3.one;
                _mount.transform.localEulerAngles = new Vector3(0, 100, 0);
                SetModelShader(_actor.boneController.GetHumanBody());   // 更换人物Shader,防止坐骑和人物出现相互遮挡的情况
                SetModelEquipShader();
            }
        }

        private void OnClothLoaded()
        {
            SortOrderedRenderAgent.RebuildAll();
        }

        private void OnWeaponLoaded()
        {
            SortOrderedRenderAgent.RebuildAll();
        }

        private void RefreshBtn()
        {
            if (PlayerDataManager.Instance.MallDataManager.CanBuy(_marketData) == 0)
            {
                _btnBuy.Visible = true;
            }
            else
            {
                _btnBuy.Visible = false;
            }
        }

        private void OnWingGoLoaded()
        {
            ModelComponentUtil.ChangeUIModelParam(_actor.gameObject);
        }
    }
}
