﻿using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMall
{
    public class MallBuyView : MallBaseBuyView
    {
        private float InitSelectNumY;

        protected MallBuyNumItem buyNumItem;
        private Locater _locaterSelectNum;

        protected override void Awake()
        {
            base.Awake();
            buyNumItem = GetChildComponent<MallBuyNumItem>("Container_buyNum");
            _locaterSelectNum = GetChildComponent<Locater>("Container_buyInfo");
            InitSelectNumY = _locaterSelectNum.Y;
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            buyNumItem.onNumChange.AddListener(OnNumChange);
        }

        private void RemoveListener()
        {
            buyNumItem.onNumChange.RemoveListener(OnNumChange);
        }

        public override void OnBuy()
        {
            MallManager.Instance.RequestBuy(marketData.__id, buyNumItem.BuyNum, couponItem.CouponItemId);
        }

        private void OnNumChange(MallBuyNumItem buyNumItem, int buyNum)
        {
            totalCost.SetData(marketData, buyNumItem.BuyNum, couponItem.CouponItemId);
        }

        public override void OnShow()
        {
            buyNumItem.Visible = true;
            _locaterSelectNum.gameObject.SetActive(true);
        }

        public override void SetData(MallPopDataWrapper wrapper)
        {
            marketData = wrapper.MarketData;
            buyNumItem.SetData(marketData);
            couponItem.SetData(marketData);
            RefreshName(marketData);
            totalCost.SetData(marketData, buyNumItem.BuyNum, couponItem.CouponItemId);
            RefreshLayout(marketData);
        }

        protected override void RefreshLayout(market_data marketData)
        {
            base.RefreshLayout(marketData);
            if (couponItem.GetCurrentValidCouponCount() == 0)
            {
                _locaterSelectNum.Y = InitSelectNumY - BUY_NUM_OFFSET;
            }
            else
            {
                _locaterSelectNum.Y = InitSelectNumY;
            }
        }
    }
}
