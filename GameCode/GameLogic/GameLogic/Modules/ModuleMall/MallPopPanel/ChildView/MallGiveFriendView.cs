﻿#region 模块信息
/*==========================================
// 文件名：MallGiveFriendView
// 创建者：陈铨
// 创建日期：2015/3/12 16:14:30
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Utils;
using UnityEngine;
using System.Text.RegularExpressions;
using Common.ExtendComponent;

namespace ModuleMall
{
    public class MallGiveFriendView : KContainer
    {
        private const int NUM_PER_PAGE = 6;
        private market_data _marketData;

        private KButton _btnClose;
        private KButton _btnHelp;
        private KScrollView _scrollView;
        private KList _list;
        private KContainer _containerTips;

        protected override void Awake()
        {
            _btnClose = GetChildComponent<KButton>("Button_close");
            _btnHelp = GetChildComponent<KButton>("Button_wenhao");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_haoyouliebiao");
            _list = GetChildComponent<KList>("ScrollView_haoyouliebiao/mask/content");
            _containerTips = GetChildComponent<KContainer>("Container_tishi");

            InitList();
            InitTips();
            AddEventListener();
        }

        private void InitList()
        {
            _list.SetPadding(8, 0, 0, -12);
            _list.SetGap(0, 0);
            _list.SetDirection(KList.KListDirection.TopToDown, 1, NUM_PER_PAGE);
        }

        private void InitTips()
        {
            AddChildComponent<IconContainer>("Container_tishi/Container_zuanshi0").SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_DIAMOND));
            AddChildComponent<IconContainer>("Container_tishi/Container_zuanshi1").SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_DIAMOND));
            AddChildComponent<IconContainer>("Container_tishi/Container_zuanshi2").SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_DIAMOND));

            InitLine0();
            InitLine1();
            InitLine2();
        }

        private void InitLine0()
        {
            int degreeLevel = int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.minFriendDegreeLevelToGiveFriend));
            int degree = intimate_helper.GetMaxDegreeByDegreeLevel(degreeLevel);
            string content = MogoLanguageUtil.GetContent(56744, degreeLevel, degree);
            MatchCollection matchCollection = Regex.Matches(content, @"[^ ]+");
            StateText text0 = GetChildComponent<StateText>("Container_tishi/Label_txttishi00");
            text0.CurrentText.text = matchCollection[0].ToString();
            StateText text1 = GetChildComponent<StateText>("Container_tishi/Label_txttishi01");
            text1.CurrentText.text = matchCollection[1].ToString();
            StateImage image = GetChildComponent<StateImage>("Container_tishi/Image_xinxin2");
            MogoLayoutUtils.ImageTextMixHorizontalLayout(text0, image, text1);
        }

        private void InitLine1()
        {
            string content = MogoLanguageUtil.GetContent(56745,
                int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.dailyMaxWorthToGiveFriend)));
            MatchCollection matchCollection = Regex.Matches(content, @"[^ ]+");
            StateText text0 = GetChildComponent<StateText>("Container_tishi/Label_txttishi10");
            text0.CurrentText.text = matchCollection[0].ToString();
            StateText text1 = GetChildComponent<StateText>("Container_tishi/Label_txttishi11");
            text1.CurrentText.text = matchCollection[1].ToString();
            KContainer container = GetChildComponent<KContainer>("Container_tishi/Container_zuanshi2");
            MogoLayoutUtils.ImageTextMixHorizontalLayout(text0, container, text1);
        }

        private void InitLine2()
        {
            string content = MogoLanguageUtil.GetContent(56746,
                int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.minAccumulateCostToGiveFriend)));
            MatchCollection matchCollection = Regex.Matches(content, @"[^ ]+");
            StateText text0 = GetChildComponent<StateText>("Container_tishi/Label_txttishi20");
            text0.CurrentText.text = matchCollection[0].ToString();
            StateText text1 = GetChildComponent<StateText>("Container_tishi/Label_txttishi21");
            text1.CurrentText.text = matchCollection[1].ToString();
            StateText text2 = GetChildComponent<StateText>("Container_tishi/Label_txttishi22");
            text2.CurrentText.text = matchCollection[2].ToString();
            KContainer container0 = GetChildComponent<KContainer>("Container_tishi/Container_zuanshi0");
            KContainer container1 = GetChildComponent<KContainer>("Container_tishi/Container_zuanshi1");
            MogoLayoutUtils.ImageTextMixHorizontalLayout(text0, container0, text1, container1, text2);
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _btnClose.onClick.AddListener(OnClose);
            _btnHelp.onClick.AddListener(OnHelp);
            EventDispatcher.AddEventListener<PbFriendAvatarInfo>(MallEvent.SendFriend, OnSendFriend);
        }

        private void RemoveEventListener()
        {
            _btnClose.onClick.RemoveListener(OnClose);
            _btnHelp.onClick.RemoveListener(OnHelp);
            EventDispatcher.RemoveEventListener<PbFriendAvatarInfo>(MallEvent.SendFriend, OnSendFriend);
        }

        public virtual void SetData(market_data marketData)
        {
            Visible = true;
            _marketData = marketData;
            Refresh();
        }

        private void OnClose()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.MallPop);
        }

        private void OnHelp()
        {
        }

        private void OnSendFriend(PbFriendAvatarInfo friendAvatarInfo)
        {
            MallPopDataWrapper wrapper = new MallPopDataWrapper(MallPopViewType.give, _marketData, friendAvatarInfo);
            UIManager.Instance.ShowPanel(PanelIdEnum.MallPop, wrapper);
        }

        public void Refresh()
        {
            RefreshList();
        }

        private void RefreshList()
        {
            _list.RemoveAll();
            if (MeetSendFriendRule())
            {
                _containerTips.Visible = false;
                FriendData friendData = PlayerDataManager.Instance.FriendData;
                List<PbFriendAvatarInfo> canGiveFriendDataList = friendData.FriendAvatarList.FindAll(MeetIntimateNeeded);
                _list.SetDataList<MallFriendItem>(canGiveFriendDataList);
            }
            else
            {
                _containerTips.Visible = true;
            }
        }

        private bool MeetIntimateNeeded(PbFriendAvatarInfo friendData)
        {
            float level = intimate_helper.GetIntimateLevel((int) friendData.degree);
            if (level >= int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.minFriendDegreeLevelToGiveFriend)))
            {
                return true;
            }
            return false;
        }

        private bool MeetSendFriendRule()
        {
            if (PlayerAvatar.Player.market_daily_give_price >= int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.dailyMaxWorthToGiveFriend)))
            {
                return false;
            }
            if (PlayerAvatar.Player.market_total_consume_price < int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.minAccumulateCostToGiveFriend)))
            {
                return false;
            }

            FriendData friendData = PlayerDataManager.Instance.FriendData;
            List<PbFriendAvatarInfo> canGiveFriendDataList = friendData.FriendAvatarList.FindAll(MeetIntimateNeeded);
            return canGiveFriendDataList.Count > 0;
        }
    }
}
