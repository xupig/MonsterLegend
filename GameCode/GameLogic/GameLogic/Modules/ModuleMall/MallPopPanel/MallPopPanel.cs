﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/21 19:45:09
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using MogoEngine.Events;

namespace ModuleMall
{
    public enum MallPopViewType
    {
        buy,//售卖弹窗
        giveFriend,//赠送朋友弹窗
        give,//赠送弹窗
        display,//展示弹窗
        buyGroupItem,//购买组物品弹窗
    }

    public class MallPopPanel : BasePanel
    {
        private MallDisplayView _displayView;
        private MallGiveFriendView _giveFriendView;
        private MallBuyViewManager _buyViewManager;

        protected override void Awake()
        {
            _displayView = AddChildComponent<MallDisplayView>("Container_zhuangbeizhanshi");
            _giveFriendView = AddChildComponent<MallGiveFriendView>("Container_zengsongduixiang");
            _buyViewManager = AddChildComponent<MallBuyViewManager>("Container_goumai");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.MallPop; }
        }

        public override void OnShow(object data)
        {
            HideAll();
            AddListener();
            MallPopDataWrapper wrapper = data as MallPopDataWrapper;
            switch (wrapper.PopViewType)
            {
            case MallPopViewType.display:
                _displayView.SetData(wrapper.MarketData);
                break;
            case MallPopViewType.giveFriend:
                _giveFriendView.SetData(wrapper.MarketData);
                break;
            default:
                _buyViewManager.Show(wrapper);
                break;
            }
        }

        private void HideAll()
        {
            _displayView.Visible = false;
            _giveFriendView.Visible = false;
            _buyViewManager.Visible = false;
        }

        public override void OnClose()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            EventDispatcher.AddEventListener(MallEvent.BuySuccess, ClosePanel);
        }

        private void RemoveListener()
        {
            EventDispatcher.RemoveEventListener(MallEvent.BuySuccess, ClosePanel);
        }
    }
}
