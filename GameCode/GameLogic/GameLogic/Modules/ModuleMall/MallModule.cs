﻿#region 模块信息
/*==========================================
// 文件名：MallModule
// 创建者：陈铨
// 创建日期：2015/3/12 15:20:41
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using GameData;
using Common.Utils;
using Common.Global;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;

namespace ModuleMall
{
    public class MallModule: BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init( )
        {
            RegisterPopPanelBunchPanel(PanelIdEnum.MallPop, PanelIdEnum.Mall);
            AddPanel(PanelIdEnum.Mall, "Container_MallPanel", MogoUILayer.LayerUIPanel, "ModuleMall.MallPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.MallPop, "Container_MallPopPanel", MogoUILayer.LayerUIPopPanel, "ModuleMall.MallPopPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
           
        }

        protected override void OnBeforeShowPanel(PanelIdEnum panelId)
        {
            if (panelId == PanelIdEnum.Mall)
            {
                PlayerDataManager.Instance.MallDataManager.Init();
                FriendManager.Instance.SendGetFriendListMsg();
                mall_helper.Initialize();
                mall_page_helper.Initialize();
                MogoLanguageUtil.GetContent(56747);
                MogoLanguageUtil.GetString(LangEnum.MALL_HAD_BUY);
                MogoLanguageUtil.GetString(LangEnum.MALL_BUY);
            }
        }
    }
}
