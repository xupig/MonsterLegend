﻿#region 模块信息
/*==========================================
// 文件名：TokenItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleMall.MallPanel.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/3/30 15:20:22
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using ModuleCommonUI.Token;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMall
{
    public class TokenItem:KList.KListItemBase
    {
        private token_shop _shop;
        private StateText _txtName;
        private IconContainer _icon;
        private StateImage _img;
        protected override void Awake()
        {
            _txtName = GetChildComponent<StateText>("Label_txtShangdianmingcheng");
            _icon = AddChildComponent<IconContainer>("Container_icon");
            _img = GetChildComponent<StateImage>("Image_xiaodian");
        }

        public override object Data
        {
            get
            {
                return _shop;
            }
            set
            {
                _shop = value as token_shop;
                Refresh();
            }
        }

        public override void Show()
        {
            EventDispatcher.AddEventListener(MallEvent.TOKEN_ITEM_REFRESH, RefreshItem);
            base.Show();
        }

        public override void Hide()
        {
            EventDispatcher.RemoveEventListener(MallEvent.TOKEN_ITEM_REFRESH, RefreshItem);
            base.Hide();
        }

        private void RefreshItem()
        {
            _img.Visible = HasItem((TokenType)_shop.__type);
        }

        private void Refresh()
        {
            _txtName.CurrentText.text = int.Parse(_shop.__title).ToLanguage();
            _icon.SetIcon(_shop.__icon);
            _img.Visible = HasItem((TokenType)_shop.__type);
        }

        private bool HasItem(TokenType type)
        {
            List<token_shop> shopList = token_shop_helper.GetTokenShopData(type);
            for(int i=0;i<shopList.Count;i++)
            {
                token_shop shop = shopList[i];
                List<token_shop_item> shopItemList = token_shop_helper.GetShopItemData(shop.__id, PlayerAvatar.Player.vocation);
                for(int j=0;j<shopItemList.Count;j++)
                {
                    token_shop_item item = shopItemList[j];
                    if(PlayerAvatar.Player.CheckCostLimit(item.__cost,false) == 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public override void OnPointerClick(UnityEngine.EventSystems.PointerEventData evtData)
        {
            PanelIdEnum.Token.Show((TokenType)_shop.__type);
            _img.Visible = false;
            base.OnPointerClick(evtData);
        }

        public override void Dispose()
        {
            
        }

    }
}
