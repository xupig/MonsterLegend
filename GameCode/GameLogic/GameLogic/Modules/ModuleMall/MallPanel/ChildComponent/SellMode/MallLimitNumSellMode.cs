﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMall
{
    public class MallLimitNumSellMode : MallBaseSellMode
    {
        private StateText _textPriceLimitNumSellMode;
        private StateText _textNumTextLimitNumSellMode;
        private StateText _textNumLimitNumSellMode;

        protected override void Awake()
        {
            _textPriceLimitNumSellMode = GetChildComponent<StateText>("Label_txtJiage");
            _textNumTextLimitNumSellMode = GetChildComponent<StateText>("Label_txtShengyuShuliang");
            _textNumLimitNumSellMode = GetChildComponent<StateText>("Label_txtNum");

            AddChildComponent<IconContainer>("Container_icon").SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_DIAMOND));
        }

        public override void Refresh(market_data marketData)
        {
            _textPriceLimitNumSellMode.CurrentText.text = string.Concat("x", mall_helper.GetSellPrice(marketData).ToString());
            MallDataManager mallDataManager = PlayerDataManager.Instance.MallDataManager;
            int num = mallDataManager.GetFlashSaleNum(marketData);
            if (num == 0)
            {
                _textNumTextLimitNumSellMode.gameObject.SetActive(false);
                _textNumLimitNumSellMode.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.MALL_SELL_OUT);
            }
            else
            {
                _textNumTextLimitNumSellMode.gameObject.SetActive(true);
                _textNumLimitNumSellMode.CurrentText.text = num.ToString();
            }
        }
    }
}
