﻿#region 模块信息
/*==========================================
// 文件名：MallMountSellMode
// 命名空间: GameLogic.GameLogic.Modules.ModuleMall.MallPanel.ChildComponent.SellMode
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/1/5 10:37:58
// 描述说明：
// 其他：
//==========================================*/
#endregion

using ACTSystem;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using UnityEngine;
namespace ModuleMall
{
    public class MallMountSellMode : MallBaseSellMode
    {
        private StateImage _mountBg;
        private ModelComponent _mountModel;

        protected override void Awake()
        {
            _mountModel = AddChildComponent<ModelComponent>("Container_mountModel");
            _mountBg = GetChildComponent<StateImage>("Image_congwudi");
        }

        public override void Refresh(market_data data)
        {
            if (data.__item_page == (int)MallType.mount)
            {
                // 加载模型
                AddMountModel(data.__item_id);
            }
            else
            {
                RemoveMountModel();
            }
        }

        private void AddMountModel(int itemId)
        {
            _mountBg.Visible = true;
            _mountModel.Visible = true;
            _mountModel.LoadMountModel(GetMountId(itemId), OnMountModelLoaded);
        }

        private void RemoveMountModel()
        {
            if (_mountActor != null)
            {
                Destroy(_mountActor);
                _mountActor = null;
            }
        }

        private ACTActor _mountActor;
        private void OnMountModelLoaded(ACTActor actor)
        {
            _mountActor = actor;
            _mountActor.transform.localEulerAngles = new Vector3(0, 210, 0);
        }

        private int GetMountId(int itemId)
        {
            if (XMLManager.item_commom.ContainsKey(itemId) && XMLManager.item_commom[itemId].__use_effect.ContainsKey("20"))
            {
                return int.Parse(XMLManager.item_commom[itemId].__use_effect["20"]);
            }
            UnityEngine.Debug.LogError("道具总表中ID为" + itemId + "项配置错误，缺少使用效果20");
            return 0;
        }

        public override void ShowMountModel()
        {
            if (_mountActor != null)
            {
                _mountActor.gameObject.SetActive(true);
            }
        }

        public override void HideMountModel()
        {
            if (_mountActor != null)
            {
                _mountActor.gameObject.SetActive(false);
            }
        }
    }
}