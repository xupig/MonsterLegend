﻿using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMall
{
    public abstract class MallBaseSellMode : KContainer
    {
        public abstract void Refresh(market_data data);

        public virtual void Clear()
        {
        }

        public virtual void ShowMountModel()
        {
        }

        public virtual void HideMountModel()
        {
        }
    }
}
