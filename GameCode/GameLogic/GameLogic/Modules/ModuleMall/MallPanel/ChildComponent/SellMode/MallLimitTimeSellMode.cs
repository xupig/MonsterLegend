﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMall
{
    public class MallLimitTimeSellMode : MallBaseSellMode
    {
        private uint timerId = TimerHeap.INVALID_ID;
        private market_data _marketData;

        private StateText _textPriceLimitTimeSellMode;
        private StateText _textTimeLimitTimeSellMode;

        protected override void Awake()
        {
            _textPriceLimitTimeSellMode = GetChildComponent<StateText>("Label_txtJiage");
            _textTimeLimitTimeSellMode = GetChildComponent<StateText>("Label_txtTime");

            AddChildComponent<IconContainer>("Container_icon").SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_DIAMOND));
        }

        public override void Refresh(market_data marketData)
        {
            _marketData = marketData;
            _textPriceLimitTimeSellMode.CurrentText.text = string.Concat("x", mall_helper.GetSellPrice(marketData).ToString());
            if (MallSellModeUtils.IsExpired(marketData))
            {
                _textTimeLimitTimeSellMode.CurrentText.text = MogoLanguageUtil.GetContent(56736);
            }
            else
            {
                AddTimer();
            }
        }
        private void AddTimer()
        {
            if (timerId == TimerHeap.INVALID_ID)
            {
                timerId = TimerHeap.AddTimer(0, 1000, OnTick);
            }
        }

        public void RemoveTimer()
        {
            if (timerId != TimerHeap.INVALID_ID)
            {
                TimerHeap.DelTimer(timerId);
                timerId = TimerHeap.INVALID_ID;
            }
        }

        private void OnTick()
        {
            if (MallSellModeUtils.IsExpired(_marketData))
            {
                RemoveTimer();
                _textTimeLimitTimeSellMode.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.MALL_ITEM_HAD_EXPIRED);
            }
            else
            {
                UInt64 endMilliseconds = mall_helper.GetEndMilliseconds(_marketData);

                string content = MogoTimeUtil.ToUniversalTime(Convert.ToInt32((endMilliseconds - Global.serverTimeStamp) / 1000), MogoLanguageUtil.GetString(LangEnum.MALL_LEFT_TIME));
                _textTimeLimitTimeSellMode.CurrentText.text = content;
            }
        }

        public override void Clear()
        {
            RemoveTimer();
        }
    }
}
