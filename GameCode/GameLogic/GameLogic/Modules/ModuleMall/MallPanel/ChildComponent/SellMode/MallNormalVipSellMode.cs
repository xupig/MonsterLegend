﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMall
{
    public class MallNormalVipSellMode : MallBaseSellMode
    {
        private StateText _textNormalPriceNormalVipSellMode;
        private StateText _textVipPriceNormalVipSellMode;

        protected override void Awake()
        {
            _textNormalPriceNormalVipSellMode = GetChildComponent<StateText>("Label_txtOldJiage");
            _textVipPriceNormalVipSellMode = GetChildComponent<StateText>("Label_txtVipJiage");

            AddChildComponent<IconContainer>("Container_vipIcon").SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_DIAMOND));
            AddChildComponent<IconContainer>("Container_oldIcon").SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_DIAMOND));
        }

        public override void Refresh(market_data marketData)
        {
            _textNormalPriceNormalVipSellMode.CurrentText.text = string.Concat("x", marketData.__price_org);
            _textVipPriceNormalVipSellMode.CurrentText.text = string.Concat("x", marketData.__price_vip);
        }
    }
}
