﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMall
{
    public class MallVipSellMode : MallBaseSellMode
    {
        private StateText _textPriceNormalSellMode;

        protected override void Awake()
        {
            _textPriceNormalSellMode = GetChildComponent<StateText>("Label_txtJiage");

            AddChildComponent<IconContainer>("Container_icon").SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_DIAMOND));
        }

        public override void Refresh(market_data marketData)
        {
            _textPriceNormalSellMode.CurrentText.text = string.Concat("x", mall_helper.GetSellPrice(marketData).ToString());
        }
    }
}
