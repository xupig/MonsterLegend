﻿using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMall
{
    public class MallDiscountSellMode : MallBaseSellMode
    {
        private StateText _textNewPriceDiscountSellMode;
        private StateText _textOldPriceDiscountSellMode;

        protected override void Awake()
        {
            _textNewPriceDiscountSellMode = GetChildComponent<StateText>("Label_txtNewJiage");
            _textOldPriceDiscountSellMode = GetChildComponent<StateText>("Label_txtOldJiage");

            AddChildComponent<IconContainer>("Container_oldIcon").SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_DIAMOND));
            AddChildComponent<IconContainer>("Container_newIcon").SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_DIAMOND));
        }

        public override void Refresh(market_data marketData)
        {
            _textOldPriceDiscountSellMode.CurrentText.text = string.Concat("x", marketData.__price_org.ToString());
            _textNewPriceDiscountSellMode.CurrentText.text = string.Concat("x", mall_helper.GetSellPrice(marketData).ToString());
        }
    }
}
