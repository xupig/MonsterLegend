﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/3/27 16:58:44
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Game.UI.UIComponent;
using GameData;
using UnityEngine;
using System.Collections.Generic;

namespace ModuleMall
{
    public class MallItemGridSellMode : KContainer
    {
        private market_data _data;
        private MallItemGridTemplate _template;
        private MallBaseSellMode _currentSellMode;
        private Dictionary<string, MallBaseSellMode> _sellModeDict;

        private Transform _transformParent;

        protected override void Awake()
        {
            _transformParent = GetChild("Container_sellMode").transform;
            _sellModeDict = new Dictionary<string, MallBaseSellMode>();
        }

        private T GetSellMode<T>(string name) where T : MallBaseSellMode
        {
            if (_sellModeDict.ContainsKey(name))
            {
                return _sellModeDict[name] as T;
            }
            T sellMode;
            _template.GetSellMode<T>(name, _transformParent, out sellMode);
            _sellModeDict.Add(name, sellMode);
            return sellMode;
        }

        private MallBaseSellMode GetCurrentSellMode()
        {
            if (_data.__item_page == (int)MallType.mount)
            {
                MallMountSellMode mountSellMode;
                _template.GetSellMode<MallMountSellMode>("Container_zuoqi", this.transform,out mountSellMode);
                mountSellMode.transform.localPosition = Vector3.zero;
                mountSellMode.transform.SetParent(this.transform);
                mountSellMode.transform.SetSiblingIndex(2);
                return mountSellMode;
            }
            switch ((MallSellMode)_data.__sell_mode)
            {
                case MallSellMode.normal:
                    if (_data.__price_vip > 0)
                    {
                        return GetSellMode<MallNormalVipSellMode>("Container_putongVip");
                    }
                    else
                    {
                        return GetSellMode<MallNormalSellMode>("Container_putong");
                    }
                case MallSellMode.vip:
                    return GetSellMode<MallVipSellMode>("Container_vip");
                case MallSellMode.discount:
                    return GetSellMode<MallDiscountSellMode>("Container_gaijiage");
                case MallSellMode.limitNum:
                    return GetSellMode<MallLimitNumSellMode>("Container_shengyushuliang");
                case MallSellMode.limitTime:
                    return GetSellMode<MallLimitTimeSellMode>("Container_shengyushijian");
            }
            return null;
        }

        public void SetData(market_data data, MallItemGridTemplate template)
        {
            Clear();
            _data = data;
            _template = template;
        }

        public void Refresh()
        {
            if (_currentSellMode != null)
            {
                _currentSellMode.Visible = false;
            }
            if (mall_helper.IsGroup(_data) && _data.__item_page != (int)MallType.mount)
            {
                return;
            }
            _currentSellMode = GetCurrentSellMode();
            _currentSellMode.Visible = true;
            _currentSellMode.Refresh(_data);
        }

        public void Clear()
        {
            foreach (MallBaseSellMode sellMode in _sellModeDict.Values)
            {
                sellMode.Clear();
            }
        }

        public void ShowMountModel()
        {
            _currentSellMode.ShowMountModel();
        }

        public void HideMountModel()
        {
            _currentSellMode.HideMountModel();
        }
    }
}
