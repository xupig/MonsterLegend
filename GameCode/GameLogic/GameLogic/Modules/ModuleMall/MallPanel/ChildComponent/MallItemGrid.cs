﻿#region 模块信息
/*==========================================
// 文件名：MallItemGrid
// 创建者：陈铨
// 创建日期：2015/3/12 16:08:47
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using ModuleCommonUI;
using MogoEngine.Events;
using UnityEngine;
using Game.UI;
using System.Collections.Generic;
using ACTSystem;

namespace ModuleMall
{
    public class MallItemGrid : KList.KListItemBase
    {
        private float QUALITY_FRAME_WIDTH;

        private static Vector3 ButtonCostInitPosition1;
        private static Vector3 ButtonCostInitPosition2;
        public Material _mallMaterial;

        private market_data _data;
        private MallItemGridTemplate _template;
        private Dictionary<string, GameObject> _labelBgDict;
        private Dictionary<string, GameObject> _labelDict;

        private KButton _btnSend;
        private KButton _btnCost;
        private ImageWrapper _btnCostNormal;
        private ImageWrapper _btnCostOver;
        private StateText _textCost;
        private KButton _btnDisplay;
        private KButton _btnShowTips;

        private Transform _transformLabelParent;
        private Transform _transformLabelBgParent;
        private Transform _transformSendParent;
        private Transform _transformDisplayParent;

        private StateText _textName;
        private IconContainer _goIcon;
        private StateImage _imageFrame;

        private MallItemGridSellMode _sellMode;

        private MallDataManager _dataManager
        {
            get
            {
                return PlayerDataManager.Instance.MallDataManager;
            }
        }

        protected override void Awake()
        {
            InitBtn();
            InitMallItem();
            _sellMode = gameObject.AddComponent<MallItemGridSellMode>();
            _transformLabelParent = GetChildComponent<Transform>("Container_biaoqian");
            _transformLabelBgParent = GetChildComponent<Transform>("Container_biaoqianbeijing");
            _transformSendParent = GetChildComponent<Transform>("Container_zengsong");
            _transformDisplayParent = GetChildComponent<Transform>("Container_yulan");
            _labelBgDict = new Dictionary<string, GameObject>();
            _labelDict = new Dictionary<string, GameObject>();
        }

        private void InitBtn()
        {
            _btnSend = GetChildComponent<KButton>("Button_zengsong");
            _btnCost = GetChildComponent<KButton>("Button_zhong");
            _textCost = GetChildComponent<StateText>("Button_zhong/label");
            _btnShowTips = GetChildComponent<KButton>("Button_showTips");
            if (ButtonCostInitPosition1 == Vector3.zero)
            {
                ButtonCostInitPosition1 = _btnCost.transform.localPosition;
            }
            if (ButtonCostInitPosition2 == Vector3.zero)
            {
                ButtonCostInitPosition2 = GetChild("Container_goumaijuzhong").transform.localPosition;
            }
            _btnCostNormal = GetChildComponent<ImageWrapper>("Button_zhong/image/normal");
            _btnCostOver = GetChildComponent<ImageWrapper>("Button_zhong/image/over");
        }

        private void InitMallItem()
        {
            _textName = GetChildComponent<StateText>("Label_txtName");
            _goIcon = AddChildComponent<IconContainer>("Container_icon");
            _imageFrame = GetChildComponent<StateImage>("Image_shangchengpinzhikuang02");
            QUALITY_FRAME_WIDTH = _imageFrame.Width;
            //修改图片的shader
            //这里不能直接调用MogoShaderUtils.ChangeShader
            //因为初始化时Mask会改变当前面板的material，这时候复制会导致面板不显示
            MogoMaterialUtils.CreateMaterialAndChangeShader(_imageFrame.CurrentImage, MogoMaterialUtils.MallPanelMaterialPath, MogoShaderUtils.IconShaderPath);
        }

        private GameObject GetLabelBg(string name)
        {
            if (_labelBgDict.ContainsKey(name))
            {
                return _labelBgDict[name];
            }
            GameObject go = _template.CloneGameObject(name, _transformLabelBgParent);
            _labelBgDict.Add(name, go);
            return go;
        }

        private GameObject GetLabel(string name)
        {
            if (_labelDict.ContainsKey(name))
            {
                return _labelDict[name];
            }
            GameObject go = _template.CloneGameObject(name, _transformLabelParent);
            _labelDict.Add(name, go);
            return go;
        }

        public override void Dispose()
        {
            _data = null;
            _template = null;
            _mallMaterial = null;
        }

        public override void Show()
        {
            AddEventListener();
        }

        public override void Hide()
        {
            RemoveEventListener();
            _sellMode.Clear();
        }

        private void AddEventListener()
        {
            _btnCost.onClick.AddListener(OnBuy);
            if (_btnSend != null) _btnSend.onClick.AddListener(OnGiveFriend);
            if (_btnDisplay != null) _btnDisplay.onClick.AddListener(OnDisplay);
            _btnShowTips.onClick.AddListener(OnShowTips);
            EventDispatcher.AddEventListener<int>(MallEvent.RefreshMallItemGrid, RefreshMallItemGrid);
        }

        private void RemoveEventListener()
        {
            _btnCost.onClick.RemoveListener(OnBuy);
            if (_btnSend != null) _btnSend.onClick.RemoveListener(OnGiveFriend);
            if (_btnDisplay != null) _btnDisplay.onClick.RemoveListener(OnDisplay);
            _btnShowTips.onClick.RemoveListener(OnShowTips);
            EventDispatcher.RemoveEventListener<int>(MallEvent.RefreshMallItemGrid, RefreshMallItemGrid);
        }

        public override object Data
        {
            set
            {
                MallItemGridDataWrapper wrapper = value as MallItemGridDataWrapper;
                _data = wrapper.MarketData as market_data;
                _template = wrapper.Template;
                _sellMode.SetData(_data, _template);
                Refresh();
            }
        }

        private void OnBuy()
        {
            int errorCode = _dataManager.CanBuy(_data);
            if (errorCode != 0)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(errorCode), PanelIdEnum.Mall);
                return;
            }
            if (mall_helper.IsGroup(_data))
            {
                MallPopDataWrapper wrapper = new MallPopDataWrapper(MallPopViewType.buyGroupItem, _data);
                UIManager.Instance.ShowPanel(PanelIdEnum.MallPop, wrapper);
            }
            else
            {
                MallPopDataWrapper wrapper = new MallPopDataWrapper(MallPopViewType.buy, _data);
                UIManager.Instance.ShowPanel(PanelIdEnum.MallPop, wrapper);
            }
        }

        private void OnGiveFriend()
        {
            int errorCode = _dataManager.CanBuy(_data);
            if (errorCode != 0)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(errorCode), PanelIdEnum.Mall);
                return;
            }
            MallPopDataWrapper wrapper = new MallPopDataWrapper(MallPopViewType.giveFriend, _data);
            UIManager.Instance.ShowPanel(PanelIdEnum.MallPop, wrapper);
        }


        private void OnShowTips()
        {
            ToolTipsManager.Instance.ShowItemTip(_data.__item_id, PanelIdEnum.Mall);
        }

        private void OnDisplay()
        {
            MallPopDataWrapper wrapper = new MallPopDataWrapper(MallPopViewType.display, _data);
            UIManager.Instance.ShowPanel(PanelIdEnum.MallPop, wrapper);
        }

        private void RefreshMallItemGrid(int mallId)
        {
            if ((_data.__id == mallId) || (_data.__show_item_id == mall_helper.GetConfig(mallId).__show_item_id))
            {
                Refresh();
            }
        }

        private void Refresh()
        {
            RefreshBtn();
            RefreshBtnContent();
            RefreshDisplayBtn();
            RefreshLabel();
            _sellMode.Refresh();
            RefreshMallItemInfo();
            RefreshQualityState();
        }

        private void RefreshQualityState()
        {
            if (_data.__item_page == (int)MallType.mount)
            {
                _imageFrame.Visible = false;
                ShowMountContainer();
            }
            else
            {
                _imageFrame.Visible = true;
                HideMountContainer();
            }
        }

        private GameObject _mountContainer;
        private void InitMountContainer()
        {
            if (_mountContainer == null)
            {
                if (transform.Find("Container_zuoqi") != null)
                {
                    _mountContainer = transform.Find("Container_zuoqi").gameObject;
                }
            }
        }

        private void ShowMountContainer()
        {
            InitMountContainer();
            if (_mountContainer != null)
            {
                _mountContainer.SetActive(true);
            }
        }

        private void HideMountContainer()
        {
            InitMountContainer();
            if (_mountContainer != null)
            {
                _mountContainer.SetActive(false);
            }
        }

        private void RefreshBtn()
        {
            if (mall_helper.CanSend(_data))
            {
                _btnCost.transform.localPosition = ButtonCostInitPosition1;
                if (_btnSend == null)
                {
                    _btnSend = _template.CloneGameObject("Button_zengsong", _transformSendParent).GetComponent<KButton>();
                    _btnSend.onClick.AddListener(OnGiveFriend);
                }
                _btnSend.Visible = true;
            }
            else
            {
                _btnCost.transform.localPosition = ButtonCostInitPosition2;
                if (_btnSend != null) _btnSend.Visible = false;
            }
            (_btnCost as KShrinkableButton).RefreshRectTransform();
        }

        private void RefreshBtnContent()
        {
            if (_dataManager.CheckMallType(_data) != 0)
            {
                _btnCostNormal.SetGray(0f);
                _btnCostOver.SetGray(0f);
                _textCost.ChangeAllStateText(MogoLanguageUtil.GetContent(56747));
                return;
            }

            if (_dataManager.IsTotalSellOut(_data) ||
                _dataManager.IsDailySellOut(_data))
            {
                _textCost.ChangeAllStateText(MogoLanguageUtil.GetString(LangEnum.MALL_HAD_BUY));
                _btnCostNormal.SetGray(0f);
                _btnCostOver.SetGray(0f);
            }
            else
            {
                _textCost.ChangeAllStateText(MogoLanguageUtil.GetString(LangEnum.MALL_BUY));
                _btnCostNormal.SetGray(1f);
                _btnCostOver.SetGray(1f);
            }
        }

        private void RefreshDisplayBtn()
        {
            if (mall_helper.CanDisplay(_data))
            {
                if (_btnDisplay == null)
                {
                    _btnDisplay = _template.CloneGameObject("Button_yulan", _transformDisplayParent).GetComponent<KButton>();
                    _btnDisplay.onClick.AddListener(OnDisplay);
                }
                _btnDisplay.Visible = true;
            }
            else
            {
                if (_btnDisplay != null) _btnDisplay.Visible = false;
            }
        }

        private void RefreshLabel()
        {
            HideLabel();
            switch ((MallSellLabel)_data.__sell_label)
            {
                case MallSellLabel.hot:
                    GetLabel("Static_rexiao").SetActive(true);
                    GetLabelBg("Static_hotBg").SetActive(true);
                    break;
                case MallSellLabel.discount:
                    GetLabel("Static_zhekou").SetActive(true);
                    GetLabelBg("Static_newpinBg").SetActive(true);
                    break;
                case MallSellLabel.flashSale:
                    GetLabel("Static_qianggou").SetActive(true);
                    GetLabelBg("Static_hotBg").SetActive(true);
                    break;
                case MallSellLabel.limitTime:
                    GetLabel("Static_xianshi").SetActive(true);
                    GetLabelBg("Static_hotBg").SetActive(true);
                    break;
                case MallSellLabel.newThing:
                    GetLabel("Static_xinpin").SetActive(true);
                    GetLabelBg("Static_newpinBg").SetActive(true);
                    break;
                case MallSellLabel.vip:
                    GetLabel("Static_vip").SetActive(true);
                    GetLabelBg("Static_vipBg").SetActive(true);
                    break;
                case MallSellLabel.limitNum:
                    GetLabel("Static_xiangou").SetActive(true);
                    GetLabelBg("Static_hotBg").SetActive(true);
                    break;
            }
        }

        private void HideLabel()
        {
            foreach (GameObject go in _labelDict.Values)
            {
                go.SetActive(false);
            }

            foreach (GameObject go in _labelBgDict.Values)
            {
                go.SetActive(false);
            }
        }

        private void RefreshMallItemInfo()
        {
            if (mall_helper.IsGroup(_data))
            {
                int showItemId = _data.__show_item_id;
                _textName.CurrentText.text = item_helper.GetName(mall_helper.GetConfig(showItemId).__item_id);
            }
            else
            {
                _textName.CurrentText.text = item_helper.GetName(_data.__item_id);
            }
            if (_data.__item_page == (int)MallType.mount)
            {
                _goIcon.Visible = false;
                return;
            }
            _goIcon.Visible = true;
            if (_data.__large_icon != 0)
            {
                _goIcon.SetIcon(_data.__large_icon, LoadLargeIconCallback);
            }
            else
            {
                if ((MallType)_data.__item_page == MallType.fashion)
                {
                    int fashionId = item_helper.GetFashionId(_data.__item_id);
                    _goIcon.SetIcon(fashion_helper.GetIconId(fashionId, (int)PlayerAvatar.Player.vocation));
                }
                else
                {
                    _goIcon.SetIcon(item_helper.GetIcon(_data.__item_id));
                }
            }
            _imageFrame.CurrentImage.color = item_helper.GetColor(_data.__item_id);
        }

        private void LoadLargeIconCallback(GameObject go)
        {
            RectTransform rect = MogoLayoutUtils.SetMiddleCenter(go);
            ImageWrapper imageWrapper = go.GetComponent<ImageWrapper>();
            imageWrapper.SetDimensionsDirty();
            float scale = (QUALITY_FRAME_WIDTH) / imageWrapper.preferredWidth;
            rect.sizeDelta = new Vector2(imageWrapper.preferredWidth * scale, imageWrapper.preferredHeight * scale);
        }

        public void ShowMountModel()
        {
            _sellMode.ShowMountModel();
        }

        public void HideMountModel()
        {
            _sellMode.HideMountModel();
        }
    }
}
