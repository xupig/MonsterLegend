﻿using Common.ExtendComponent;
using Game.UI;
using Game.UI.UIComponent;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleMall
{
    public class MallItemGridTemplate : KContainer
    {
        private Dictionary<string, GameObject> _nameDict;
        private Dictionary<string, MallBaseSellMode> _sellModeDict;

        protected override void Awake()
        {
            _nameDict = new Dictionary<string, GameObject>();
            InitLabel();
            InitButton();
            InitLabelBackground();
            InitMountContainer();
            InitSellMode();
        }

        private GameObject AddNameDict(string name)
        {
            GameObject go = GetChild(name);
            _nameDict.Add(name, go);
            return go;
        }

        private void InitButton()
        {
            AddNameDict("Button_zengsong");
            GetChildComponent<ImageWrapper>("Button_zengsong/image/disable").SetGray(0f);
            AddNameDict("Button_yulan");
        }

        private void InitLabel()
        {
            AddNameDict("Static_newpinBg");
            AddNameDict("Static_hotBg");
            AddNameDict("Static_vipBg");
        }

        private void InitLabelBackground()
        {
            AddNameDict("Static_vip");
            AddNameDict("Static_zhekou");
            AddNameDict("Static_xinpin");
            AddNameDict("Static_rexiao");
            AddNameDict("Static_qianggou");
            AddNameDict("Static_xiangou");
            AddNameDict("Static_xianshi");
        }

        private void InitMountContainer()
        {
            AddNameDict("Container_zuoqi");
        }

        private T AddSellMode<T>(string name) where T : MallBaseSellMode
        {
            T component = AddChildComponent<T>(name);
            _sellModeDict.Add(name, component);
            return component as T;
        }

        private void InitSellMode()
        {
            _sellModeDict = new Dictionary<string, MallBaseSellMode>();
            AddSellMode<MallNormalSellMode>("Container_putong");
            AddSellMode<MallNormalVipSellMode>("Container_putongVip");
            AddSellMode<MallVipSellMode>("Container_vip");
            AddSellMode<MallDiscountSellMode>("Container_gaijiage");
            AddSellMode<MallLimitNumSellMode>("Container_shengyushuliang");
            AddSellMode<MallLimitTimeSellMode>("Container_shengyushijian");
            AddSellMode<MallMountSellMode>("Container_zuoqi");
        }

        public GameObject CloneGameObject(string name, Transform parent)
        {
            GameObject go = Instantiate(_nameDict[name]) as GameObject;
            go.SetActive(true);
            go.transform.SetParent(parent, false);
            return go;
        }

        public void GetSellMode<T>(string name, Transform parent, out T sellMode) where T : MallBaseSellMode
        {
            GameObject go = Instantiate(_sellModeDict[name].gameObject) as GameObject;
            go.SetActive(true);
            go.transform.SetParent(parent, false);
            sellMode = go.GetComponent<T>();
        }
    }
}
