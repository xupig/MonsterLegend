﻿using GameData;

namespace ModuleMall
{
    public class MallItemGridDataWrapper
    {
        public market_data MarketData;
        public MallItemGridTemplate Template;

        public MallItemGridDataWrapper(market_data marketData, MallItemGridTemplate template)
        {
            this.MarketData = marketData;
            this.Template = template;
        }
    }
}
