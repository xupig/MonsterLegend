﻿#region 模块信息
/*==========================================
// 文件名：MallPanel
// 创建者：陈铨
// 创建日期：2015/3/12 15:20:14
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using UnityEngine;
using GameMain.Entities;
using ModuleCommonUI.Token;
using MogoEngine.Events;
using Common.Events;

namespace ModuleMall
{
    public class MallPanel : BasePanel
    {
        private const int MALL = 0;
        private const int TOKEN = 1;
        private MallView _mallView;
        private TokenView _tokenView;
        private KToggleGroup _toggleGroup;
        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _mallView = AddChildComponent<MallView>("Container_mall");
            _tokenView = AddChildComponent<TokenView>("Container_daibishangdian");
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Mall; }
        }

        public override void OnShow(object param)
        {
            SetTabList(6,_toggleGroup);
            _toggleGroup.GetToggleList()[1].SetGreenPoint(IsTokenHasItem());
            if(param is int[])
            {
                int[] list = param as int[];
                if(list[0] == 0)
                {
                    _toggleGroup.SelectIndex = MALL;
                    _mallView.OnShow(param);
                    _tokenView.OnClose();
                }
                else
                {
                    _toggleGroup.SelectIndex = TOKEN;
                    _mallView.OnClose();
                    _tokenView.OnShow();
                }
            }
            else
            {
                _toggleGroup.SelectIndex = MALL;
                _mallView.OnShow(null);
                _tokenView.OnClose();
            }
            AddEventListener();
        }

        public override void EnableCanvas()
        {
            base.EnableCanvas();
            if(_tokenView.Visible)
            {
                EventDispatcher.TriggerEvent(MallEvent.TOKEN_ITEM_REFRESH);
            }
        }

        private bool IsTokenHasItem()
        {
            List<token_shop> list = token_shop_helper.GetMallTokenShopList();
            for(int k=0;k<list.Count;k++)
            {
                List<token_shop> shopList = token_shop_helper.GetTokenShopData((TokenType)list[k].__type);
                for (int i = 0; i < shopList.Count; i++)
                {
                    token_shop shop = shopList[i];
                    List<token_shop_item> shopItemList = token_shop_helper.GetShopItemData(shop.__id, PlayerAvatar.Player.vocation);
                    for (int j = 0; j < shopItemList.Count; j++)
                    {
                        token_shop_item item = shopItemList[j];
                        if (PlayerAvatar.Player.CheckCostLimit(item.__cost, false) == 0)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChange);
        }

        private void RemoveEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChange);
        }

        private void OnSelectedIndexChange(KToggleGroup toggleGroup, int index)
        {
            if(index == MALL)
            {
                _mallView.OnShow(null);
                _tokenView.OnClose();
            }
            else
            {
                _mallView.OnClose();
                _tokenView.OnShow();
            }
        }
    }
}
