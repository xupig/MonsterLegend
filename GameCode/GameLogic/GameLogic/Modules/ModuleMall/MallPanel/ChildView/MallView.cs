﻿#region 模块信息
/*==========================================
// 文件名：MallView
// 命名空间: GameLogic.GameLogic.Modules.ModuleMall.MallPanel.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/3/30 10:54:42
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMall
{
    public class MallView:KContainer
    {
        private MallGridView _mallGridView;
        private CategoryList _categoryToggleGroup;
        private List<MallType> _mallTypeList;

        private int _currentIndex;
        private int _selectMallId;
        protected override void Awake()
        {
            _mallGridView = AddChildComponent<MallGridView>("Container_shangchang");
            InitToggleGroup();
        }

        private void InitToggleGroup()
        {
            _categoryToggleGroup = GetChildComponent<CategoryList>("List_categoryList");
            _categoryToggleGroup.SetDataList<CategoryListItem>(GetCategoryItemDataList(), 1);
            _categoryToggleGroup.onCoroutineEnd.AddListener(SetCategoryIndex);
        }

        private void SetCategoryIndex()
        {
            _categoryToggleGroup.SelectedIndex = _currentIndex;
        }

        private List<CategoryItemData> GetCategoryItemDataList()
        {
            MallDataManager mallDataManager = PlayerDataManager.Instance.MallDataManager;
            List<CategoryItemData> dataList = new List<CategoryItemData>();
            _mallTypeList = new List<MallType>();
            foreach (MallType mallType in mallDataManager.MallDataDict.Keys)
            {
                if (mallDataManager.MallDataDict[mallType].GetCount() > 0)
                {
                    _mallTypeList.Add(mallType);
                    dataList.Add(CategoryItemData.GetCategoryItemData(
                        mall_page_helper.GetName((int)mallType),
                        mall_page_helper.GetIcon((int)mallType))
                        );
                }
            }
            return dataList;
        }

        public void OnShow(object param)
        {
            Visible = true;
            ParseParam(param);
            _categoryToggleGroup.SelectedIndex = _currentIndex;
            OnCategoryChanged(_categoryToggleGroup, _currentIndex);
            AddEventListener();
        }

        private void ParseParam(object param)
        {
            int index = 0;
            _selectMallId = 0;
            if (param is int[])
            {
                int[] list = param as int[];
                for (int i = 0; i < _mallTypeList.Count; i++)
                {
                    if ((MallType)list[1] == _mallTypeList[i])
                    {
                        index = i;
                        break;
                    }
                }
                if (list.Length >= 3)
                {
                    _selectMallId = list[2];
                }
            }
            _currentIndex = index;
        }

        public void OnClose()
        {
            Visible = false;
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _categoryToggleGroup.onSelectedIndexChanged.AddListener(OnCategoryChanged);
        }

        private void RemoveEventListener()
        {
            _categoryToggleGroup.onSelectedIndexChanged.RemoveListener(OnCategoryChanged);
        }

        private void OnCategoryChanged(CategoryList toggleGroup, int index)
        {
            MallType mallType = _mallTypeList[index];
            _mallGridView.SetData(mallType, _selectMallId);
        }
    }
}
