﻿#region 模块信息
/*==========================================
// 文件名：TokenView
// 命名空间: GameLogic.GameLogic.Modules.ModuleMall.MallPanel.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/3/30 10:59:36
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Game.UI.UIComponent;
using GameData;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMall
{
    public class TokenView:KContainer
    {
        private KScrollPage _page;
        private KPageableList _list;

        protected override void Awake()
        {
            _page = GetChildComponent<KScrollPage>("ScrollPage_scrollpage");
            _list = _page.GetChildComponent<KPageableList>("mask/content");
            _list.SetDirection(KList.KListDirection.LeftToRight, 5, 2);
            _list.SetGap(83,26);
            base.Awake();
        }

        public void OnShow()
        {
            Visible = true;
            List<token_shop> list = token_shop_helper.GetMallTokenShopList();
           
            _list.SetDataList<TokenItem>(list);
            _page.TotalPage = Mathf.CeilToInt(list.Count / 10f);
            _page.CurrentPage = 0;
        }


        public void OnClose()
        {
            Visible = false;
        }

    }
}
