﻿#region 模块信息
/*==========================================
// 文件名：MallGridView
// 创建者：陈铨
// 创建日期：2015/3/12 16:02:47
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Data;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using UnityEngine;
using Common.ServerConfig;
using Common.ExtendComponent;
using Common.Base;
using ModuleCommonUI;

namespace ModuleMall
{
    public class MallGridView : KContainer
    {
        private const int PAGE_GRID_ITEM_NUM = 4;

        private MallType _mallType;
        private MallDataManager _mallData
        {
            get
            {
                return PlayerDataManager.Instance.MallDataManager;
            }
        }

        private KScrollPage _scrollPage;
        private KPageableList _contentList;
        private StateText _textPage;
        private MoneyItem _moneyItem;
        private KButton _btnBuyDiamond;
        private MallItemGridTemplate _template;

        protected override void Awake()
        {
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_scrollpage");
            _contentList = GetChildComponent<KPageableList>("ScrollPage_scrollpage/mask/content");
            _template = AddChildComponent<MallItemGridTemplate>("ScrollPage_scrollpage/mask/content/template");
            _textPage = GetChildComponent<StateText>("Container_bottom/Label_txtYeshu");
            _moneyItem = AddChildComponent<MoneyItem>("Container_bottom/Container_buyDiamond");
            _moneyItem.SetMoneyType(public_config.MONEY_TYPE_DIAMOND);
            _btnBuyDiamond = GetChildComponent<KButton>("Container_bottom/Button_chongzhi");
            KParticle particle = AddChildComponent<KParticle>("Container_bottom/fx_ui_12_shangcheng_01");
            particle.transform.localPosition = new Vector3(759f, -43.5f, 0);

            InitList();
            AddListener();
        }

        private void InitList()
        {
            _contentList.SetPadding(0, 4, 0, 4);
            _contentList.SetGap(0, 8);
            _contentList.SetDirection(KList.KListDirection.LeftToRight, PAGE_GRID_ITEM_NUM, 1);
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _scrollPage.onCurrentPageChanged.AddListener(OnPageChange);
            _btnBuyDiamond.onClick.AddListener(OnBuyDiamond);
        }

        private void RemoveListener()
        {
            _scrollPage.onCurrentPageChanged.RemoveListener(OnPageChange);
            _btnBuyDiamond.onClick.RemoveListener(OnBuyDiamond);
        }

        public void Update()
        {
            GameObject maskGo = _scrollPage.transform.Find("mask").gameObject;
            if (_scrollPage.IsDragging || _scrollPage.IsMoving)
            {
                for (int i = 0; i < _contentList.ItemList.Count; i++)
                {
                    MallItemGrid item = _contentList.ItemList[i] as MallItemGrid;
                    if (item.Visible == true && ItemInMask(item, maskGo))
                    {
                        item.ShowMountModel();
                    }
                    else
                    {
                        item.HideMountModel();
                    }
                }
            }
        }
        

        private bool ItemInMask(MallItemGrid item, GameObject maskGo)
        {
            RectTransform itemRect = item.GetComponent<RectTransform>();
            RectTransform maskRect = maskGo.GetComponent<RectTransform>();
            Vector3[] itemWorldCorners = new Vector3[4];
            Vector3[] maskWorldCorners = new Vector3[4];
            itemRect.GetWorldCorners(itemWorldCorners);
            maskRect.GetWorldCorners(maskWorldCorners);
            Vector2 itemLeftUpVector = new Vector2(itemWorldCorners[0].x, itemWorldCorners[0].y);
            Vector2 itemRightDownVector = new Vector2(itemWorldCorners[2].x, itemWorldCorners[2].y);
            Vector2 maskLeftUpVector = new Vector2(maskWorldCorners[0].x, maskWorldCorners[0].y);
            Vector2 maskRightDownVector = new Vector2(maskWorldCorners[2].x, maskWorldCorners[2].y);
            return itemLeftUpVector.x >= maskLeftUpVector.x && itemLeftUpVector.y >= maskLeftUpVector.y
                && itemRightDownVector.x <= maskRightDownVector.x && itemRightDownVector.y <= maskRightDownVector.y;
        }

        private void OnBuyDiamond()
        {
            if (function_helper.IsFunctionOpen(FunctionId.charge))
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.Charge);
            }
            else
            {
                MessageBox.ShowFunctionUnrealized();
            }
        }

        private void OnPageChange(KScrollPage scrollPage, int index)
        {
            RefreshPage();
        }

        public void SetData(MallType mallType, int selectMallId)
        {
            _mallType = mallType;
            Refresh(selectMallId);
        }

        private void Refresh(int selectMallId)
        {
            RefreshContentList(selectMallId);
            RefreshPage();
        }

        private void RefreshContentList(int selectMallId)
        {
            List<MallItemGridDataWrapper> wrapperList = GetWrapperList();
            int selectIndex = GetSelectIndex(wrapperList, selectMallId);
            _contentList.SetDataList(selectIndex, typeof(MallItemGrid), wrapperList, PAGE_GRID_ITEM_NUM);
            int totalPage = (int)Mathf.Floor((wrapperList.Count - 1) / PAGE_GRID_ITEM_NUM) + 1;
            _scrollPage.TotalPage = totalPage;
            _scrollPage.CurrentPage = selectIndex;
        }

        private int GetSelectIndex(List<MallItemGridDataWrapper> wrapperList, int selectMallId)
        {
            int index = 0;
            if(selectMallId != 0)
            {
                for (int i = 0; i < wrapperList.Count; i++)
                {
                    if (wrapperList[i].MarketData.__id == selectMallId)
                    {
                        index = Mathf.FloorToInt(((float)i / PAGE_GRID_ITEM_NUM));
                        break;
                    }
                }
            }
            return index;
        }

        private List<MallItemGridDataWrapper> GetWrapperList()
        {
            List<MallItemGridDataWrapper> wrapperList = new List<MallItemGridDataWrapper>();
            List<market_data> mallItemList = _mallData.MallDataDict[_mallType].MallItemList;
            mallItemList.Sort(MarketDataSort);
            for (int i = 0; i < mallItemList.Count; i++)
            {
                MallItemGridDataWrapper wrapper = new MallItemGridDataWrapper(mallItemList[i], _template);
                wrapperList.Add(wrapper);
            }
            return wrapperList;
        }

        private int MarketDataSort(market_data x, market_data y)
        {
            bool xIsSellOut = IsSellOut(x);
            bool yIsSellOut = IsSellOut(y);
            if (!xIsSellOut && yIsSellOut)
            {
                return -1;
            }
            else if (xIsSellOut == yIsSellOut)
            {
                if ((x.__is_hot == 1) && (y.__is_hot == 1))
                {
                    if (x.__hot_sort > y.__hot_sort)
                    {
                        return -1;
                    }
                    else if (x.__hot_sort == y.__hot_sort)
                    {
                        if (x.__id < y.__id)
                        {
                            return -1;
                        }
                    }
                }
                else
                {
                    if (x.__item_sort > y.__item_sort)
                    {
                        return -1;
                    }
                    else if ((x.__item_sort == y.__item_sort))
                    {
                        if (x.__id < y.__id)
                        {
                            return -1;
                        }
                    }
                }
            }
            return 1;
        }

        private bool IsSellOut(market_data marketData)
        {
            //排序用
            if (_mallData.IsDailySellOut(marketData))
            {
                return true;
            }
            if (_mallData.IsTotalSellOut(marketData))
            {
                return true;
            }
            if (_mallData.CheckMallType(marketData) != 0)
            {
                return true;
            }

            //if (((MallSellMode)marketData.__sell_mode == MallSellMode.limitNum) &&(mallDataManager.GetFlashSaleNum(marketData) == 0))
            //{
            //    return true;
            //}
            return false;
        }

        private void RefreshPage()
        {
            _textPage.ChangeAllStateText(string.Format("{0}/{1}", (_scrollPage.CurrentPage + 1), _scrollPage.TotalPage));
        }
    }
}
