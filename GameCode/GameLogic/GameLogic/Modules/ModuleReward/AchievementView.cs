﻿#region 模块信息
/*==========================================
// 文件名：AchievementView
// 命名空间: GameLogic.GameLogic.Modules.ModuleReward
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/2 14:34:49
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ClientConfig;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System.Collections.Generic;
namespace ModuleReward
{
    public class AchievementView : KContainer
    {
        private const int ACHIEVEMNET_POINT_ITEM = 15;
        private const int NONE = 0;
        private CategoryList _categoryList;
        private AchievementContentView _contentView;
        private List<string> _categoryListNames;
        private List<int> _achievementGroups;
        private List<int> _categoryListIcons;
        private StateText _currentAchievementValueTxt;
        private IconContainer _achievementIcon;
        private KDummyButton _achievementIconBtn;
        private int currentCategoryIndex = 0;
        private bool needShowHasGained = false;

        protected override void Awake()
        {
            _contentView = AddChildComponent<AchievementContentView>("Container_chengjiu");
            _currentAchievementValueTxt = GetChildComponent<StateText>("Container_chengjiuzhi/Label_txtGold");
            _achievementIcon = AddChildComponent<IconContainer>("Container_chengjiuzhi/Container_icon");
            _achievementIconBtn = AddChildComponent<KDummyButton>("Container_chengjiuzhi/Container_icon");
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
        }

        public void Refresh(bool showAchievementGainedd = false)
        {
            this.Visible = true;
            needShowHasGained = showAchievementGainedd;
            InitCategoryList();
            _categoryList.SelectedIndex = currentCategoryIndex;
            OnCategoryIndexChanged(_categoryList, currentCategoryIndex);
            SetCurrentAchievement();
            AddEventListener();
        }

        private void SetCurrentAchievement()
        {
            _achievementIcon.SetMoneyIcon(ACHIEVEMNET_POINT_ITEM);
            SetAchievementValue();
        }

        private void InitCategoryList()
        {
            bool hasGainedAchievement = false;
            _categoryList.RemoveAll();
            _achievementGroups = achievement_helper.GetCategoryGroups();
            _categoryListNames = new List<string>();
            _categoryListIcons = new List<int>();
            for (int i = 0; i < _achievementGroups.Count; i++)
            {
                if (_achievementGroups[i] == achievement_helper.HASREWARDED)
                {
                    hasGainedAchievement = true;
                    _categoryListNames.Add(MogoLanguageUtil.GetContent(9500004));
                    _categoryListIcons.Add(NONE);
                }
                else
                {
                    _categoryListNames.Add(MogoLanguageUtil.GetContent(_achievementGroups[i]));
                    _categoryListIcons.Add(NONE);
                }

            }
            _categoryList.SetDataList(_categoryListNames, _categoryListIcons);
            currentCategoryIndex = SetCategoryPoint();
            if (needShowHasGained && hasGainedAchievement)
            {
                currentCategoryIndex = _achievementGroups.Count - 1;
            }
        }

        private int SetCategoryPoint()
        {
            int index = -1;
            for (int i = 0; i < _achievementGroups.Count; i++)
            {
                int groupId = _achievementGroups[i];
                if (groupId != achievement_helper.HASREWARDED)
                {
                    bool hasRedPoint = PlayerDataManager.Instance.AchievementData.GroupContainsCompletedAchievement(groupId);
                    if (index == -1 && hasRedPoint == true)
                    {
                        index = i;
                    }
                    (_categoryList.ItemList[i] as CategoryListItem).SetPoint(hasRedPoint);
                }
            }
            return index > 0 ? index : 0;
        }

        private void AddEventListener()
        {
            _categoryList.onSelectedIndexChanged.AddListener(OnCategoryIndexChanged);
            EventDispatcher.AddEventListener(AchievementEvents.REFRESH_ACHIEVEMENT_LIST, RefreshContent);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.achievement_point, SetAchievementValue);
            _achievementIconBtn.onClick.AddListener(OnAchievementIconClick);
        }

        private void RemoveEventListener()
        {
            _categoryList.onSelectedIndexChanged.RemoveListener(OnCategoryIndexChanged);
            EventDispatcher.RemoveEventListener(AchievementEvents.REFRESH_ACHIEVEMENT_LIST, RefreshContent);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.achievement_point, SetAchievementValue);
            _achievementIconBtn.onClick.RemoveListener(OnAchievementIconClick);
        }

        private void OnAchievementIconClick()
        {
            ToolTipsManager.Instance.ShowItemTip(ACHIEVEMNET_POINT_ITEM, PanelIdEnum.Reward);
        }

        private void SetAchievementValue()
        {
            _currentAchievementValueTxt.CurrentText.text = PlayerAvatar.Player.achievement_point.ToString();
        }

        private void RefreshContent()
        {
            SetCategoryPoint();
            if (achievement_helper.hasRewardAchievement == false && PlayerDataManager.Instance.AchievementData.HasRewardedTask() == true)
            {
                _achievementGroups.Add(achievement_helper.HASREWARDED);
                CategoryItemData data = new CategoryItemData();
                data.name = MogoLanguageUtil.GetContent(9500004);
                _categoryList.AddItem<CategoryListItem>(data, true);
                achievement_helper.hasRewardAchievement = true;
            }
            int groupId = _achievementGroups[currentCategoryIndex];
            _contentView.Refresh(groupId);
        }

        private void OnCategoryIndexChanged(CategoryList category, int index)
        {
            category.SelectedIndex = index;
            currentCategoryIndex = index;
            _contentView.Refresh(_achievementGroups[index]);
        }

        public void Hide()
        {
            if (this.Visible == true)
            {
                RemoveEventListener();
                this.Visible = false;
            }
        }

    }
}