﻿using System;
using System.Collections;
using System.Collections.Generic;
using Common.Data;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using UnityEngine;

namespace ModuleReward
{
    public class MonthLoginRewardView : KContainer
    {
        private KList _contentList;
        private StateText _monthTitleText;
        private KScrollView _scrollView;

        private MonthLoginData _monthLoginData;
        private bool _isStartSetPosCoroutine = false;
        
        private const int ROW_ITEM_NUM = 4;

        protected override void Awake()
        {
            _monthLoginData = PlayerDataManager.Instance.RewardData.GetMonthLoginData();
            _contentList = GetChildComponent<KList>("ScrollView_denglujiangli/mask/content");
            _monthTitleText = GetChildComponent<StateText>("Label_txtShuliang");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_denglujiangli");
            InitScrollView();
        }

        private void InitScrollView()
        {
            _contentList.SetPadding(8, 10, 15, 10);
            _contentList.SetGap(15, 39);
            _contentList.SetDirection(KList.KListDirection.LeftToRight, ROW_ITEM_NUM, int.MaxValue);
        }

        public void RefreshContent()
        {
            RefreshContentList();
            RefreshMonthTitleText();
        }

        private void RefreshContentList()
        {
            int addStepCount = 4;
            _contentList.SetDataList<MonthLoginRewardItem>(_monthLoginData.MonthLoginList, addStepCount);
            if (_contentList.ItemList.Count >= Mathf.Min(_monthLoginData.SignDayNum + 4, _monthLoginData.MonthLoginList.Count))
            {
                SetPositionToCurSignRow();
            }
            else
            {
                StopSetPositionCoroutine();
                StartCoroutine(DoWaitSetContentPosition());
            }
        }

        private void SetPositionToCurSignRow()
        {
            if (_monthLoginData.SignDayNum <= ROW_ITEM_NUM)
            {
                _scrollView.ResetContentPosition();
                return;
            }
            RectTransform contentRect = _scrollView.Content.GetComponent<RectTransform>();
            RectTransform itemRect = _contentList.ItemList[_monthLoginData.SignDayNum - 1].GetComponent<RectTransform>();
            contentRect.localPosition = new Vector3(contentRect.localPosition.x, -itemRect.anchoredPosition.y, 0);
        }

        private IEnumerator DoWaitSetContentPosition()
        {
            _isStartSetPosCoroutine = true;
            int waitCount = Mathf.Min(_monthLoginData.SignDayNum + 4, _monthLoginData.MonthLoginList.Count);
            while (_contentList.ItemList.Count < waitCount)
            {
                yield return 0;
            }
            SetPositionToCurSignRow();
            _isStartSetPosCoroutine = false;
        }

        private void RefreshMonthTitleText()
        {
            MonthLoginData monthLoginData = PlayerDataManager.Instance.RewardData.GetMonthLoginData();
            _monthTitleText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent((int)LangEnum.REWARD_LOGIN_MONTH_TITLE), monthLoginData.Month);
        }

        protected void AddEventListener()
        {

        }

        protected void RemoveEventListener()
        {

        }

        private void StopSetPositionCoroutine()
        {
            if (_isStartSetPosCoroutine)
            {
                StopCoroutine(DoWaitSetContentPosition());
                _isStartSetPosCoroutine = false;
            }
        }

        protected override void OnDisable()
        {
            StopSetPositionCoroutine();
        }

        protected override void OnDestroy()
        {
            StopSetPositionCoroutine();
            base.OnDestroy();
        }
    }
}
