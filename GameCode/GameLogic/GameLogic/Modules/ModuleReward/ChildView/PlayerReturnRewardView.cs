﻿#region 模块信息
/*==========================================
// 文件名：PlayerReturnRewardView
// 命名空间: GameLogic.GameLogic.Modules.ModuleReward.ChildView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/3/19 15:48:59
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using Game.UI.UIComponent;
using Common.Structs.ProtoBuf;
using Common.ExtendComponent;
using GameData;
using GameMain.Entities;
using Game.UI;
using GameMain.GlobalManager.SubSystem;
using Common.ServerConfig;
using MogoEngine.Events;
using Common.Events;

namespace ModuleReward
{
    public class ReturnRewardItem : KList.KListItemBase
    {
        private PbOldReturnItem _data;
        private StateText _dayIndexTxt;
        private KList _rewardList;
        private KButton _getRewardBtn;
        private KParticle _getRewardBtnParticle;
        private MaskEffectItem _maskEffectItem;
        private bool ShowParticle = false;

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbOldReturnItem;
                Refresh();
            }
        }

        protected override void Awake()
        {
            _rewardList = GetChildComponent<KList>("List_jiangli");
            _getRewardBtn = GetChildComponent<KButton>("Button_lingqu");
            _dayIndexTxt = GetChildComponent<StateText>("Label_txtTianshu");
            _getRewardBtnParticle = _getRewardBtn.AddChildComponent<KParticle>("fx_ui_3_2_lingqu");
            _maskEffectItem = gameObject.AddComponent<MaskEffectItem>();
            _maskEffectItem.Init(_getRewardBtnParticle.transform.parent.gameObject, IsParticleShow);
            AddEventListener();
        }

        private bool IsParticleShow()
        {
            return ShowParticle;
        }

        private void AddEventListener()
        {
            _getRewardBtn.onClick.AddListener(GetPlayerReturnReward);
        }

        private void RemoveEventListener()
        {
            _getRewardBtn.onClick.RemoveListener(GetPlayerReturnReward);
        }

        private void GetPlayerReturnReward()
        {
            PlayerReturnManager.Instance.RequestToGetReward((int)_data.reward_day);
        }

        private void Refresh()
        {
            _dayIndexTxt.CurrentText.text = MogoLanguageUtil.GetContent((int)_data.reward_name);
            _rewardList.RemoveAll();
            _rewardList.SetDataList<RewardGrid>(item_reward_helper.GetRewardDataList((int)_data.reward_id, PlayerAvatar.Player.vocation));
            if (_data.reward_flag == public_config.OLD_RETURN_REWAED_NO)  // 未领取
            {
                HideAllGetImage();
                SetButtonActive();
                ShowParticle = true;
                _getRewardBtnParticle.Play(true);
            }
            else if (_data.reward_flag == public_config.OLD_RETURN_REWAED_OK)   // 已经领取
            {
                ShowAllGetImage();
                SetButtonHasGetReward();
                ShowParticle = false;
                _getRewardBtnParticle.Stop();
            }
            else if(_data.reward_flag == public_config.OLD_RETURN_REWAED_CANNOT) // 不能领取
            {
                HideAllGetImage();
                SetButtonCannotGet();
                ShowParticle = false;
                _getRewardBtnParticle.Stop();
            }
        }

        private void HideAllGetImage()
        {
            for (int i = 0; i < _rewardList.ItemList.Count; i++)
            {
                _rewardList[i].transform.FindChild("Image_sharedyilingqu").gameObject.SetActive(false);
            }
        }

        private void ShowAllGetImage()
        {
            for (int i = 0; i < _rewardList.ItemList.Count; i++)
            {
                _rewardList[i].transform.FindChild("Image_sharedyilingqu").gameObject.SetActive(true);
            }
        }

        private void SetButtonCannotGet()
        {
            _getRewardBtn.SetButtonDisable();
            SetButtonText(_getRewardBtn, MogoLanguageUtil.GetContent(124018));
        }

        private void SetButtonHasGetReward()
        {
            _getRewardBtn.SetButtonDisable();
            SetButtonText(_getRewardBtn, MogoLanguageUtil.GetContent(65305));
        }

        private void SetButtonActive()
        {
            _getRewardBtn.Visible = true;
            SetButtonText(_getRewardBtn,MogoLanguageUtil.GetContent(124018));
        }

        private void SetButtonText(KButton button, string text)
        {
            TextWrapper[] textArray = button.GetComponentsInChildren<TextWrapper>(true);
            for (int i = 0; i < textArray.Length; i++)
            {
                textArray[i].text = text;
            }
        }

        public override void Dispose()
        {
            RemoveEventListener();
        }
    }

    public class PlayerReturnRewardView : KContainer
    {
        private StateText _titleTxt;
        private KPageableList _returnRewardList;

        protected override void Awake()
        {
            _titleTxt = GetChildComponent<StateText>("Label_txtBiaoti");
            _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(124021);
            _returnRewardList = GetChildComponent<KPageableList>("ScrollView_laowanjiajingli/mask/content");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        public void AddEventListener()
        {
            EventDispatcher.AddEventListener(OldPlayerReturnEvent.UpdateData, Refresh);
        }

        public void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(OldPlayerReturnEvent.UpdateData, Refresh);
        }

        public void Refresh()
        {
            _returnRewardList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _returnRewardList.RemoveAll();
            _returnRewardList.SetDataList<ReturnRewardItem>(PlayerReturnManager.Instance.ReturnData.GetOldReturnRewardList());
            TweenListEntry.Begin(_returnRewardList.gameObject);
        }
    }
}