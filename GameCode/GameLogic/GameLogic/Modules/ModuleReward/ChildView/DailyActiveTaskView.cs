﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.Data;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using Common.Events;
using Common.ExtendComponent;
using GameLoader.Utils.Timer;
using GameLoader.Utils;
using UnityEngine.UI;
using UnityEngine;
using Game.UI;
using ModuleCommonUI;
using Common.Base;
using Common.Utils;
using System.Collections;

namespace ModuleReward
{
    public class DailyActiveTaskView : KContainer
    {
        private KList _contentList;
        private KScrollView _scrollView;
        private KProgressBar _activeProgressBar;
        private List<KContainer> _activeBoxList = new List<KContainer>();
        private List<DailyActiveBoxItem> _activeBoxItemList = new List<DailyActiveBoxItem>();
        private bool _isStartContentCoroutine = false;
        private bool _isTweenList = false;
        public bool IsTweenList
        {
            get { return _isTweenList; }
            set { _isTweenList = value; }
        }

        private DailyActiveTaskData _dailyActiveData;

        protected override void Awake()
        {
            _dailyActiveData = PlayerDataManager.Instance.RewardData.GetDailyActiveData();
            _contentList = GetChildComponent<KList>("ScrollView_jiangli/mask/content");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_jiangli");
            InitActiveBoxWidgets();
            InitScrollView();
        }

        private void InitScrollView()
        {
            _contentList.SetPadding(5, 1, 1, 5);
            _contentList.SetGap(-5, 1);
            _contentList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        private void InitActiveBoxWidgets()
        {
            _activeProgressBar = GetChildComponent<KProgressBar>("Container_huoyuedu/ProgressBar_jindu");
            SetActiveProgressBarScaleDir();
            for (int i = 0; i < _dailyActiveData.DailyActiveBoxList.Count; i++)
            {
                KContainer box = GetChildComponent<KContainer>("Container_huoyuedu/Container_xiangzi0" + (i + 1));
                DailyActiveBoxItem boxItem = AddChildComponent<DailyActiveBoxItem>("Container_huoyuedu/Container_xiangzi0" + (i + 1));
                _activeBoxList.Add(box);
                _activeBoxItemList.Add(boxItem);
            }
        }

        private void SetActiveProgressBarScaleDir()
        {
            UIUtils.SetProgressBarScaleDir(_activeProgressBar, UVScaleImage.UVScaleDirection.Forward);
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(RewardEvents.UPDATE_ACTIVE_BOX_REWARD_LV_INFOS, OnRefreshActiveBoxContent);
            EventDispatcher.AddEventListener(RewardEvents.UPDATE_ACTIVE_BOX_INFO, OnRefreshActiveBoxContent);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(RewardEvents.UPDATE_ACTIVE_BOX_REWARD_LV_INFOS, OnRefreshActiveBoxContent);
            EventDispatcher.RemoveEventListener(RewardEvents.UPDATE_ACTIVE_BOX_INFO, OnRefreshActiveBoxContent);
        }

        public void RefreshContent()
        {
            RefreshContentList();
            RefreshActiveBoxContent();
        }

        private void RefreshContentList()
        {
            if (_dailyActiveData.DailyActiveList.Count == 0)
            {
                LoggerHelper.Error("每日活跃任务中没有可接的任务，可能是由于配置的问题！");
            }
            if (_contentList.ItemList.Count < _dailyActiveData.DailyActiveList.Count)
            {
                StopContentCoroutine();
                //由于要灵活控制执行TweenList效果的时机，因此就用自定义的加载协程
                StartCoroutine(SetContentList());
                if (_isTweenList)
                {
                    _isTweenList = false;
                }
            }
            else
            {
                _contentList.SetDataList<DailyActiveTaskItem>(_dailyActiveData.DailyActiveList);
                if (_isTweenList)
                {
                    _scrollView.ResetContentPosition();
                    TweenListEntry.Begin(_contentList.gameObject);
                    _isTweenList = false;
                }
            }
        }

        private IEnumerator SetContentList()
        {
            _isStartContentCoroutine = true;
            bool isTween = _isTweenList;
            bool isTweenAgain = true;
            int curItemCount = _contentList.ItemList.Count;
            while (_contentList.ItemList.Count < _dailyActiveData.DailyActiveList.Count)
            {
                if (curItemCount > 0 && curItemCount <= 3 && isTween)
                {
                    _contentList.DoLayout();
                    TweenListEntry.Begin(_contentList.gameObject);
                    isTweenAgain = false;
                }
                int addStepCount = 3;
                int addCount = Mathf.Min(addStepCount, _dailyActiveData.DailyActiveList.Count - _contentList.ItemList.Count);
                int index = 0;
                int startIndex = _contentList.ItemList.Count;
                while (index < addCount)
                {
                    _contentList.AddItem(typeof(DailyActiveTaskItem), _dailyActiveData.DailyActiveList[startIndex + index], false);
                    index++;
                }
                yield return 0;
            }
            _contentList.DoLayout();
            if (isTween && isTweenAgain)
            {
                TweenListEntry.Begin(_contentList.gameObject);
            }
            _isStartContentCoroutine = false;
        }

        private void RefreshActiveBoxContent()
        {
            List<DailyActiveBoxData> activeBoxInfoList = _dailyActiveData.DailyActiveBoxList;
            _activeProgressBar.Value = (float)_dailyActiveData.CurActivePoint / (_dailyActiveData.MaxActivePoint * 1.0f);
            for (int i = 0; i < activeBoxInfoList.Count; i++)
            {
                _activeBoxItemList[i].SetItemData(activeBoxInfoList[i]);
            }
        }

        private void OnRefreshActiveBoxContent()
        {
            RefreshActiveBoxContent();
        }

        private void StopContentCoroutine()
        {
            if (_isStartContentCoroutine)
            {
                StopCoroutine(SetContentList());
                _isStartContentCoroutine = false;
            }
        }

        protected override void OnDestroy()
        {
            StopContentCoroutine();
            base.OnDestroy();
        }
    }
}
