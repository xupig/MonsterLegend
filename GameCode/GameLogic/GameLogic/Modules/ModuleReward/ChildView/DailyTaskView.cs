﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using Common.Data;
using MogoEngine.Mgrs;
using GameMain.Entities;
using Common.ClientConfig;
using Common.ExtendComponent;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils.Timer;
using GameLoader.Utils;

namespace ModuleReward
{
    public class DailyTaskView : KContainer
    {
        private KList _contentList;
        private KScrollView _scrollView;
        private bool _isTweenList = false;
        public bool IsTweenList
        {
            get { return _isTweenList; }
            set { _isTweenList = value; }
        }

        protected override void Awake()
        {
            _contentList = GetChildComponent<KList>("mask/content");
            _scrollView = GetComponent<KScrollView>();
            InitScrollView();
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void InitScrollView()
        {
            _contentList.SetPadding(5, 1, 1, 5);
            _contentList.SetGap(-5, 1);
            _contentList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        public void RefreshContent()
        {
            RefreshContentList();
        }

        private void RefreshContentList()
        {
            DailyTaskData dailyTaskData = PlayerDataManager.Instance.RewardData.GetDailyTaskData();
            if (dailyTaskData.DailyTaskCombineList.Count == 0)
            {
                LoggerHelper.Error("每日任务中没有可接的任务，可能是由于配置的问题！");
            }
            int coroutineCreateCount = dailyTaskData.DailyTaskCombineList.Count > 5 ? 5 : dailyTaskData.DailyTaskCombineList.Count;
            _contentList.SetDataList<DailyTaskItem>(dailyTaskData.DailyTaskCombineList, coroutineCreateCount);
            OnRefreshMonthCardTimeInfo();
            if (_isTweenList)
            {
                _scrollView.ResetContentPosition();
                TweenListEntry.Begin(_contentList.gameObject);
                _isTweenList = false;
            }
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(RewardEvents.UPDATE_CARD_INFO, OnRefreshMonthCardTimeInfo);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(RewardEvents.UPDATE_CARD_INFO, OnRefreshMonthCardTimeInfo);
        }

        private void OnRefreshMonthCardTimeInfo()
        {
            DailyTaskData dailyTaskData = PlayerDataManager.Instance.RewardData.GetDailyTaskData();
            for (int i = 0; i < _contentList.ItemList.Count; i++)
            {
                DailyTaskItemData itemData = (_contentList.ItemList[i] as DailyTaskItem).Data as DailyTaskItemData;
                if (itemData.TaskType == (int)DailyTaskType.MonthCard)
                {
                    DailyTaskItem item = _contentList.ItemList[i] as DailyTaskItem;
                    item.RefreshMonthCardTimeInfo(dailyTaskData.TaskCardInfo.leftSeconds, dailyTaskData.IsMonthCardValid());
                    break;
                }
            }
        }
    }
}
