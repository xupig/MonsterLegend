﻿using System;
using System.Collections.Generic;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using Common.Data;
using GameMain.GlobalManager;
using UnityEngine;
using Common.Base;
using Common.Utils;
using MogoEngine.Events;
using Common.Events;
using ModuleChat;

namespace ModuleReward
{
    public class OpenServerLoginRewardView : KContainer
    {
        private KContainer _dayRewardsContainer;
        private List<ItemGrid> _rewardItemList = new List<ItemGrid>();
        private List<StateText> _rewardItemNumList = new List<StateText>();
        private List<StateImage> _haveGetRewardTagList = new List<StateImage>();
        private KScrollPage _scrollPage;
        private KList _loginRewardItemList;
        private KButton _toGoBtn;
        private CenterAlignComponent _loginDayRewardsCenterAlign;

        private OpenServerLoginRewardData _loginRewardData;
        private int _curSelectIndex = 0;
        private int _totalPage = 0;

        protected override void Awake()
        {
            _loginRewardData = PlayerDataManager.Instance.RewardData.GetOpenServerLoginData();
            _dayRewardsContainer = GetChildComponent<KContainer>("Container_jiangli");
            for (int i = 0; i < 4; i++)
            {
                KContainer rewardContainer = GetChildComponent<KContainer>("Container_jiangli/Container_item" + (i + 1).ToString());
                ItemGrid itemGrid = rewardContainer.GetChildComponent<ItemGrid>("Container_wupin");
                StateText itemNumText = rewardContainer.GetChildComponent<StateText>("Label_txtNum");
                StateImage getRewardTag = rewardContainer.GetChildComponent<StateImage>("Image_sharedyilingqu");
                _rewardItemList.Add(itemGrid);
                _rewardItemNumList.Add(itemNumText);
                _haveGetRewardTagList.Add(getRewardTag);
                rewardContainer.Visible = false;
            }
            InitDayRewardsCenterAlignment();
            InitLoginRewardScrollPage();
            SetRewardsContainerParentToMask();
            _toGoBtn = GetChildComponent<KButton>("Button_qianwang");
        }

        private void InitLoginRewardScrollPage()
        {
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_kaifujiangliLiebiao");
            _loginRewardItemList = _scrollPage.GetChildComponent<KList>("mask/content");
            _loginRewardItemList.SetDirection(KList.KListDirection.LeftToRight, 4, 1);
            _loginRewardItemList.SetPadding(5, 0, 5, 0);
            _loginRewardItemList.SetGap(1, 0);
        }

        private void SetRewardsContainerParentToMask()
        {
            GameObject maskGo = GetChild("ScaleImage_mask");
            KContainer rewardsContainer = GetChildComponent<KContainer>("Container_jiangli");
            UIUtils.SetWidgetParentWithOriginalPos(rewardsContainer.gameObject, maskGo, UIManager.Instance.UICamera);
        }

        private void InitDayRewardsCenterAlignment()
        {
            _loginDayRewardsCenterAlign = _dayRewardsContainer.gameObject.AddComponent<CenterAlignComponent>();
            List<GameObject> rewardGoList = new List<GameObject>();
            for (int i = 0;i < _rewardItemList.Count;i++)
            {
                rewardGoList.Add(_rewardItemList[i].transform.parent.gameObject);
            }
            StateImage bgImg = GetChildComponent<StateImage>("Image_shouchongBg");
            _loginDayRewardsCenterAlign.Init(rewardGoList, _dayRewardsContainer.gameObject, bgImg.gameObject, UIManager.Instance.UICamera);
            _loginDayRewardsCenterAlign.SetGap(9.0f, 0);
        }

        protected override void OnEnable()
        {
            AddEventListener();            
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _toGoBtn.onClick.AddListener(OnClickToGoBtn);
            _loginRewardItemList.onItemClicked.AddListener(OnClickLoginRewardItem);
            EventDispatcher.AddEventListener<OpenServerLoginItemData, int>(RewardEvents.GET_SERVER_OPENED_LOGIN_REWARD, OnGetServerOpenedLoginReward);
        }

        private void RemoveEventListener()
        {
            _toGoBtn.onClick.RemoveListener(OnClickToGoBtn);
            _loginRewardItemList.onItemClicked.RemoveListener(OnClickLoginRewardItem);
            EventDispatcher.RemoveEventListener<OpenServerLoginItemData, int>(RewardEvents.GET_SERVER_OPENED_LOGIN_REWARD, OnGetServerOpenedLoginReward);
        }

        private void OnClickToGoBtn()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.Task);
        }

        private void OnClickLoginRewardItem(KList list, KList.KListItemBase item)
        {
            for (int i = 0; i < _loginRewardItemList.ItemList.Count; i++)
            {
                if (_loginRewardItemList.ItemList[i] == item)
                {
                    if (i != _curSelectIndex)
                    {
                        HideLoginRewardItemHighlight(_curSelectIndex);
                        RefreshLoginDayRewards(i);
                        _curSelectIndex = i;
                        break;
                    }
                }
            }
        }

        private void OnGetServerOpenedLoginReward(OpenServerLoginItemData itemData, int itemDataIndex)
        {
            _loginRewardItemList[itemDataIndex].Data = itemData;
            if (itemDataIndex == _curSelectIndex)
            {
                RefreshLoginDayRewards(_curSelectIndex);
            }
        }

        public void Refresh()
        {
            int lastSelectIndex = _curSelectIndex;
            _curSelectIndex = 0;
            if (_loginRewardData.LoginRewardInfoList.Count == 0)
            {
                if (lastSelectIndex != 0)
                {
                    HideLoginRewardItemHighlight(lastSelectIndex);
                }
                _scrollPage.Visible = false;
                return;
            }
            if (!_scrollPage.Visible)
            {
                _scrollPage.Visible = true;
            }
            if (_totalPage == 0)
            {
                int totalPage = Mathf.CeilToInt((float)_loginRewardData.LoginRewardInfoList.Count / 4.0f);
                _scrollPage.TotalPage = totalPage;
                _totalPage = totalPage;
            }
            RefreshLoginRewardList();
            if (lastSelectIndex != 0)
            {
                HideLoginRewardItemHighlight(lastSelectIndex);
            }
            ShowLoginRewardItemHighlight(_curSelectIndex);
            RefreshLoginDayRewards(_curSelectIndex);
        }

        private void RefreshLoginRewardList()
        {
            _loginRewardItemList.SetDataList<OpenServerLoginRewardItem>(_loginRewardData.LoginRewardInfoList);
        }

        private void RefreshLoginDayRewards(int listIndex)
        {
            OpenServerLoginItemData itemData = _loginRewardData.LoginRewardInfoList[listIndex];
            if (itemData == null)
                return;
            for (int i = 0; i < itemData.RewardList.Count; i++)
            {
                _rewardItemList[i].transform.parent.gameObject.SetActive(true);
                RewardData rewardItemData = itemData.RewardList[i];
                _rewardItemList[i].SetItemData(rewardItemData.id);
                _rewardItemNumList[i].CurrentText.text = rewardItemData.num.ToString();
                _haveGetRewardTagList[i].Visible = itemData.IsReceived;
            }
            for (int i = itemData.RewardList.Count; i < _rewardItemList.Count; i++)
            {
                _rewardItemList[i].transform.parent.gameObject.SetActive(false);
            }
            _dayRewardsContainer.RecalculateSize();
            _loginDayRewardsCenterAlign.DoLayout();
            RectTransform maskRect = GetChildComponent<RectTransform>("ScaleImage_mask");
            ChatPopupViewFadeIn.BeginShow(_dayRewardsContainer.gameObject, 0f, maskRect.sizeDelta.y, _rewardItemList[0].transform.parent.gameObject);
        }

        private void ShowLoginRewardItemHighlight(int listIndex)
        {
            OpenServerLoginRewardItem loginRewardItem = _loginRewardItemList[listIndex] as OpenServerLoginRewardItem;
            loginRewardItem.ShowHighlight();
        }

        private void HideLoginRewardItemHighlight(int listIndex)
        {
            OpenServerLoginRewardItem loginRewardItem = _loginRewardItemList[listIndex] as OpenServerLoginRewardItem;
            loginRewardItem.HideHighlight();
        }

        protected override void OnDestroy()
        {
            
        }
    }
}
