﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.Data;
using GameMain.GlobalManager;
using GameMain.Entities;
using UnityEngine;
using Common.ExtendComponent;
using ModuleCommonUI;
using Common.Base;
using Common.Utils;
using Common.Global;

namespace ModuleReward
{
    public class VIPGiftView : KContainer
    {
        private KScrollPage _scrollPage;
        private KList _contentList;
        private StateText _chargeTitleText;
        private KButton _chargeBtn;
        private KParticle _chargeEffect;

        protected override void Awake()
        {
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_VIPjiangli");
            _contentList = GetChildComponent<KList>("ScrollPage_VIPjiangli/mask/content");
            _chargeTitleText = GetChildComponent<StateText>("Label_txtChongzhineirong");
            _chargeBtn = GetChildComponent<KButton>("Button_chongzhi");
            RectTransform chargeBtnRect = _chargeBtn.gameObject.GetComponent<RectTransform>();
            chargeBtnRect.anchoredPosition = new Vector2(701.0f, chargeBtnRect.anchoredPosition.y);
            (_chargeBtn as KShrinkableButton).RefreshRectTransform();
            _chargeEffect = _chargeBtn.AddChildComponent<KParticle>("fx_ui_12_shangcheng_01");
            _chargeEffect.transform.localPosition = new Vector3(1f, 0, 0);
            InitScrollPage();
            AddEventListener();
        }

        private void InitScrollPage()
        {
            _scrollPage.TotalPage = 3;
            _contentList.SetPadding(1, 10, 1, 10);
            _contentList.SetGap(1, 20);
            _contentList.SetDirection(KList.KListDirection.LeftToRight, 3, 1);
        }

        private void AddEventListener()
        {
            _chargeBtn.onClick.AddListener(OnClickChargeBtn);
        }

        private void RemoveEventListener()
        {
            _chargeBtn.onClick.RemoveListener(OnClickChargeBtn);
        }

        public void RefreshContent()
        {
            RefreshContentList();
            RefreshChargeTitleInfo();
        }

        private void RefreshContentList()
        {
            VipGiftData vipGiftData = PlayerDataManager.Instance.RewardData.GetVipGiftData();
            //根据玩家当前的vip等级显示
            List<VipGiftItemData> vipGiftItemList = null;
            int curVip = PlayerAvatar.Player.vip_level;
            if (curVip < 8)
            {
                vipGiftItemList = vipGiftData.GetGiftItemListByCount(8);
            }
            else if (curVip >= 8)
            {
                int count = curVip + 1;
                if (count > vipGiftData.VipGiftList.Count)
                {
                    count = curVip;
                }
                vipGiftItemList = vipGiftData.GetGiftItemListByCount(count);
            }
            int totalPage = Mathf.CeilToInt((float)vipGiftItemList.Count / 3.0f);
            if (_scrollPage.TotalPage != totalPage)
            {
                _scrollPage.TotalPage = totalPage;
            }
            _contentList.SetDataList<VIPGiftItem>(vipGiftItemList);
        }

        private void RefreshChargeTitleInfo()
        {
            VipGiftData vipGiftData = PlayerDataManager.Instance.RewardData.GetVipGiftData();
            int curVip = PlayerAvatar.Player.vip_level;
            int nextVip = curVip + 1;
            if (nextVip <= vipGiftData.VipGiftList.Count)
            {
                uint needCost = vipGiftData.GetReachVipNeedCost(nextVip);
                _chargeTitleText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent((int)LangEnum.REWARD_UPGRADE_NEXT_VIP_NEED_CHARGE), nextVip, needCost);
            }
            else
            {
                _chargeTitleText.CurrentText.text = MogoLanguageUtil.GetContent((int)LangEnum.REWARD_REACH_MAX_VIP);
            }
        }

        private void OnClickChargeBtn()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.Charge);
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }
    }
}
