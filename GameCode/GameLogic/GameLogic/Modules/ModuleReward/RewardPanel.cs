﻿using System.Collections.Generic;
using Common.Base;
using Common.Events;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Game.UI.UIComponent;
using ModuleTimeAltar;
using GameData;
using Common.Utils;
using GameLoader.Utils;

namespace ModuleReward
{
    public class RewardPanel : BasePanel
    {
        private KToggleGroup _toggleGroup;
        private KToggle _achievementToggle;
        private KToggle _timeAltarToggle;
        private RewardView _rewardView;
        private AchievementView _achievementView;
        private TimeAltarView _timeAltarView;

        private const int REWARD_INDEX = 0;
        private const int ACHIEVEMENT_INDEX = 1;
        private const int TIME_ALTAR_INDEX = 2;

        private List<int> toggleList = new List<int>();

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            _achievementToggle = _toggleGroup.GetChildComponent<KToggle>("Toggle_chengjiu");
            _timeAltarToggle = _toggleGroup.GetChildComponent<KToggle>("Toggle_shiguangjitan");
            GetChildComponent<StateText>("ToggleGroup_biaoti/Toggle_shiguangjitan/image/label").ChangeAllStateText(MogoLanguageUtil.GetContent(78030));
            GetChildComponent<StateText>("ToggleGroup_biaoti/Toggle_shiguangjitan/checkmark/label").ChangeAllStateText(MogoLanguageUtil.GetContent(78030));
            _rewardView = AddChildComponent<RewardView>("Container_jiangli");
            _achievementView = AddChildComponent<AchievementView>("Container_chengjiu");
            _timeAltarView = AddChildComponent<TimeAltarView>("Container_shiguangjitan");

            toggleList.Add((int)FunctionId.reward);
            toggleList.Add((int)FunctionId.Achievement);
            toggleList.Add((int)FunctionId.TimeAltar);
            SetTabList(toggleList, _toggleGroup);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Reward; }
        }

        public override void OnShow(object data)
        {
            SetData(data);
        }

        public override void SetData(object data)
        {
            int toggleIndex = 0;
            int rewardViewIndex = -1;
            bool showAwardGained = false;
            if (data is RewardViewType)
            {
                toggleIndex = REWARD_INDEX;
                rewardViewIndex = (int)((RewardViewType)data);
            }
            else if (data is int)
            {
                toggleIndex = (int)data;
            }
            // 特殊规则，当传入的参数为Bool,表示跳转到成就界面，并且直接跳转到已完成的成就之上
            else if (data is bool)
            {
                toggleIndex = ACHIEVEMENT_INDEX;
                showAwardGained = true;
            }
            else if (data is int[])
            {
                int[] tabs = data as int[];
                //tab开始的索引值为1
                int tab = tabs[0];
                int secondTab = tabs[1];
                if (secondTab > 0 && tab <= 0)
                {
                    LoggerHelper.Error("Tab value is 0, but second tab is larger than 0 ! If you set second tab not equal 0, please set tab value larger than 0 !!!");
                    return;
                }
                toggleIndex = tab - 1;
                rewardViewIndex = secondTab - 1;
            }
            switch (toggleIndex)
            {
                case REWARD_INDEX:
                    _achievementView.Visible = false;
                    _rewardView.Refresh(rewardViewIndex);
                    _timeAltarView.OnClose();
                    break;
                case ACHIEVEMENT_INDEX:
                    _rewardView.Visible = false;
                    _achievementView.Refresh(showAwardGained);
                    _timeAltarView.OnClose();
                    break;
                case TIME_ALTAR_INDEX:
                    _rewardView.Visible = false;
                    _achievementView.Visible = false;
                    _timeAltarView.OnShow();
                    break;
            }
            _achievementToggle.SetGreenPoint(PlayerDataManager.Instance.AchievementData.HasCompletedAchievement());
            _timeAltarToggle.SetGreenPoint(PlayerDataManager.Instance.TimeAltarData.IsHaveNotice);
            _toggleGroup.SelectIndex = toggleIndex;
            AddEventListener();
        }

        private void AddEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnToggleGroupChanged);
            EventDispatcher.AddEventListener(AchievementEvents.REFRESH_ACHIEVEMENT_TOGGLE_STATE, SetAchievementToggleState);
            EventDispatcher.AddEventListener(TimeAltarEvents.NoticeChanged, SetTimeAltarToggleState);
        }

        private void RemoveEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnToggleGroupChanged);
            EventDispatcher.RemoveEventListener(AchievementEvents.REFRESH_ACHIEVEMENT_TOGGLE_STATE, SetAchievementToggleState);
            EventDispatcher.RemoveEventListener(TimeAltarEvents.NoticeChanged, SetTimeAltarToggleState);
        }

        private void SetAchievementToggleState()
        {
            _achievementToggle.SetGreenPoint(PlayerDataManager.Instance.AchievementData.HasCompletedAchievement());
        }

        private void SetTimeAltarToggleState()
        {
            _timeAltarToggle.SetGreenPoint(PlayerDataManager.Instance.TimeAltarData.IsHaveNotice);
        }

        private void OnToggleGroupChanged(KToggleGroup group, int index)
        {
            switch (index)
            {
                case REWARD_INDEX:
                    _achievementView.Hide();
                    _timeAltarView.OnClose();
                    _rewardView.Refresh();
                    break;
                case ACHIEVEMENT_INDEX:
                    _rewardView.Hide();
                    _timeAltarView.OnClose();
                    _achievementView.Refresh();
                    break;
                case TIME_ALTAR_INDEX:
                    _rewardView.Hide();
                    _achievementView.Hide();
                    _timeAltarView.OnShow();
                    break;
            }
        }

        public override void OnClose()
        {
            _rewardView.Hide();
            _achievementView.Hide();
            _timeAltarView.OnClose();
            RemoveEventListener();
        }

    }
}
