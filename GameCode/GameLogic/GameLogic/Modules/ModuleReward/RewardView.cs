﻿#region 模块信息
/*==========================================
// 文件名：RewardView
// 命名空间: GameLogic.GameLogic.Modules.ModuleReward
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/2 14:35:06
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System.Collections.Generic;
using GameLoader.Utils;
using Common.Utils;
using Common.Global;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Mgrs;
using Common.ClientConfig;
namespace ModuleReward
{
    public enum RewardViewType
    {
        OPEN_SERVER_LOGIN_REWARD_VIEW = 0,
        DAILY_ACTIVE_TASK_VIEW = 1,
        DAILY_TASK_VIEW = 2,
        MONTH_LOGIN_REWARD_VIEW = 3,
        VIP_GIFT_VIEW = 4,
        PLAYER_RETURN_REWARD_VIEW = 5,
        REWARD_COMPENSATION = 6,
    }

    public class RewardView : KContainer
    {
        private DailyTaskView _dailyTaskView;
        private MonthLoginRewardView _monthLoginRewardView;
        private DailyActiveTaskView _dailyActiveTaskView;
        private PlayerReturnRewardView _playerReturnRewardView;
        private VIPGiftView _vipGiftView;
        private CategoryList _categoryToggleGroup;

        private bool _isInitCategoryList = false;
        private List<int> _rewardViewIndexList = new List<int>();

        private int currentCategoryIndex = int.MaxValue;

        protected override void Awake()
        {
            PlayerDataManager.Instance.RewardData.UpdateOpenServerLoginRewardData();
            _dailyTaskView = AddChildComponent<DailyTaskView>("ScrollView_jiangli");
            _monthLoginRewardView = AddChildComponent<MonthLoginRewardView>("Container_denglujiangli");
            _dailyActiveTaskView = AddChildComponent<DailyActiveTaskView>("Container_huoyuejiangli");
            _vipGiftView = AddChildComponent<VIPGiftView>("Container_VIPjiangli");
            _playerReturnRewardView = AddChildComponent<PlayerReturnRewardView>("Container_laowanjiajingli");
            _categoryToggleGroup = GetChildComponent<CategoryList>("List_categoryList");
        }

        private void AddEventListner()
        {
            _categoryToggleGroup.onSelectedIndexChanged.AddListener(OnCategoryChanged);
            EventDispatcher.AddEventListener(RewardEvents.UPDATE_DAILY_TASK, OnRefreshDailyTaskView);
            EventDispatcher.AddEventListener(RewardEvents.UPDATE_MONTH_LOGIN, OnRefreshMonthLoginView);
            EventDispatcher.AddEventListener(RewardEvents.UPDATE_ACTIVE_TASK, OnRefreshActiveTaskView);
            EventDispatcher.AddEventListener(RewardEvents.UPDATE_VIP_GIFT, OnRefreshCategoryVipGift);
            EventDispatcher.AddEventListener(RewardEvents.UPDATE_ACTIVE_BOX_INFO, OnRefreshCategoryDailyActive);
            EventDispatcher.AddEventListener(OldPlayerReturnEvent.UpdateData, RefreshPlayerReturnRedPoint);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.old_role_status, CheckOldPlayerReturn);
        }

        private void RemoveEventListener()
        {
            _categoryToggleGroup.onSelectedIndexChanged.RemoveListener(OnCategoryChanged);
            EventDispatcher.RemoveEventListener(RewardEvents.UPDATE_DAILY_TASK, OnRefreshDailyTaskView);
            EventDispatcher.RemoveEventListener(RewardEvents.UPDATE_MONTH_LOGIN, OnRefreshMonthLoginView);
            EventDispatcher.RemoveEventListener(RewardEvents.UPDATE_ACTIVE_TASK, OnRefreshActiveTaskView);
            EventDispatcher.RemoveEventListener(RewardEvents.UPDATE_VIP_GIFT, OnRefreshCategoryVipGift);
            EventDispatcher.RemoveEventListener(RewardEvents.UPDATE_ACTIVE_BOX_INFO, OnRefreshCategoryDailyActive);
            EventDispatcher.RemoveEventListener(OldPlayerReturnEvent.UpdateData, RefreshPlayerReturnRedPoint);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.old_role_status, CheckOldPlayerReturn);
        }

        protected override void OnEnable()
        {
            AddEventListner();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        // 不传入参数的条件下，选择有奖励条件的展示
        public void Refresh()
        {
            this.Visible = true;
            RefreshCategory();
            RefreshSelectView(currentCategoryIndex);
        }

        // 传入参数，按照参数传入的选择
        public void Refresh(int rewardViewIndex)
        {
            if (rewardViewIndex == -1)
            {
                Refresh();
            }
            else
            {
                this.Visible = true;
                RefreshCategory();
                int categoryIndex = GetCategoryIndex(rewardViewIndex);
                RefreshSelectView(categoryIndex);
            }
        }

        private void OnCategoryChanged(CategoryList toggleGroup, int index)
        {
            ShowView(index);
        }

        private void OnRefreshDailyTaskView()
        {
            RefreshCategoryItemRedPoint(GetCategoryIndex((int)RewardViewType.DAILY_TASK_VIEW));
            if (_dailyTaskView.Visible == true)
            {
                _dailyTaskView.RefreshContent();
            }
        }

        private void OnRefreshMonthLoginView()
        {
            RefreshCategoryItemRedPoint(GetCategoryIndex((int)RewardViewType.MONTH_LOGIN_REWARD_VIEW));
            if (_monthLoginRewardView.Visible == true)
            {
                _monthLoginRewardView.RefreshContent();
            }
        }

        private void OnRefreshActiveTaskView()
        {
            RefreshCategoryItemRedPoint(GetCategoryIndex((int)RewardViewType.DAILY_ACTIVE_TASK_VIEW));
            if (_dailyActiveTaskView.Visible == true)
            {
                _dailyActiveTaskView.RefreshContent();
            }
        }

        private void OnRefreshCategoryVipGift()
        {
            RefreshCategoryItemRedPoint(GetCategoryIndex((int)RewardViewType.VIP_GIFT_VIEW));
        }

        private void OnRefreshCategoryDailyActive()
        {
            RefreshCategoryItemRedPoint(GetCategoryIndex((int)RewardViewType.DAILY_ACTIVE_TASK_VIEW));
        }

        private int GetCategoryIndex(int viewIndex)
        {
            int index = -1;
            for (int i = 0; i < _rewardViewIndexList.Count; i++)
            {
                if (_rewardViewIndexList[i] == viewIndex)
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

        private void RefreshPlayerReturnRedPoint()
        {
            if (_rewardViewIndexList.Contains((int)RewardViewType.PLAYER_RETURN_REWARD_VIEW))
            {
                RefreshCategoryItemRedPoint(GetCategoryIndex((int)RewardViewType.PLAYER_RETURN_REWARD_VIEW));
            }
        }

        private void RefreshCategoryItemRedPoint(int categoryIndex)
        {
            if (categoryIndex < 0)
            {
                return;
            }
            CategoryListItem item = _categoryToggleGroup[categoryIndex] as CategoryListItem;
            int viewIndex = _rewardViewIndexList[categoryIndex];
            bool isShowRedPoint = false;
            if (viewIndex == (int)RewardViewType.DAILY_ACTIVE_TASK_VIEW)
            {
                DailyActiveTaskData activeTaskData = PlayerDataManager.Instance.RewardData.GetDailyActiveData();
                isShowRedPoint = activeTaskData.IsShowTips();

            }
            else if (viewIndex == (int)RewardViewType.DAILY_TASK_VIEW)
            {
                DailyTaskData dailyTaskData = PlayerDataManager.Instance.RewardData.GetDailyTaskData();
                isShowRedPoint = dailyTaskData.IsShowTips();
            }
            else if (viewIndex == (int)RewardViewType.MONTH_LOGIN_REWARD_VIEW)
            {
                MonthLoginData monthLoginData = PlayerDataManager.Instance.RewardData.GetMonthLoginData();
                isShowRedPoint = monthLoginData.IsShowTips();
            }
            else if (viewIndex == (int)RewardViewType.VIP_GIFT_VIEW)
            {
                VipGiftData vipGiftData = PlayerDataManager.Instance.RewardData.GetVipGiftData();
                isShowRedPoint = vipGiftData.IsShowTips();
            }
            else if (viewIndex == (int)RewardViewType.PLAYER_RETURN_REWARD_VIEW)
            {
                isShowRedPoint = PlayerReturnManager.Instance.HasRewardToGet();
            }
            item.SetPoint(isShowRedPoint);
            if (isShowRedPoint == true && categoryIndex < currentCategoryIndex)
            {
                currentCategoryIndex = categoryIndex;
            }
        }

        private void CheckOldPlayerReturn()
        {
            // 是否需要显示老玩家的信息
            if (PlayerReturnManager.Instance.IsOldPlayer == false && _rewardViewIndexList.Contains((int)RewardViewType.PLAYER_RETURN_REWARD_VIEW) == true)
            {
                _isInitCategoryList = false;
                RefreshCategory();
                ShowView(0);
            }
            else if (PlayerReturnManager.Instance.IsOldPlayer == true && _rewardViewIndexList.Contains((int)RewardViewType.PLAYER_RETURN_REWARD_VIEW) == false)
            {
                _isInitCategoryList = false;
                RefreshCategory();
                ShowView(currentCategoryIndex);
            }
        }

        private void RefreshCategory()
        {
            RewardDataManager rewardData = PlayerDataManager.Instance.RewardData;
            //判断是否有变动，有变动再更新；或是界面初始化时也需更新
            if (rewardData.IsCategoryListChange() || !_isInitCategoryList || PlayerDataManager.Instance.playerReturnData.StatusChanged == true)
            {
                _rewardViewIndexList.Clear();
                List<CategoryItemData> toggleNameList = new List<CategoryItemData>();
                string title = string.Empty;
                if(rewardData.IsRewardTypeOpen(RewardSystemType.RewardCompensation))
                {
                    title = MogoLanguageUtil.GetContent((int)LangEnum.REWARD_CATEGORY_REWARD_COMPENSATION);
                    toggleNameList.Add(CategoryItemData.GetCategoryItemData(title));
                    _rewardViewIndexList.Add((int)RewardViewType.REWARD_COMPENSATION);
                }
                if (rewardData.IsRewardTypeOpen(RewardSystemType.ServerOpendLoginReward))
                {
                    title = MogoLanguageUtil.GetContent(33216);
                    toggleNameList.Add(CategoryItemData.GetCategoryItemData(title));
                    _rewardViewIndexList.Add((int)RewardViewType.OPEN_SERVER_LOGIN_REWARD_VIEW);
                }
                if (rewardData.IsRewardTypeOpen(RewardSystemType.DailyActiveTask))
                {
                    title = MogoLanguageUtil.GetContent((int)LangEnum.REWARD_CATEGORY_DAILY_ACTIVE);
                    toggleNameList.Add(CategoryItemData.GetCategoryItemData(title));
                    _rewardViewIndexList.Add((int)RewardViewType.DAILY_ACTIVE_TASK_VIEW);
                }
                if (rewardData.IsRewardTypeOpen(RewardSystemType.DailyTask))
                {
                    title = MogoLanguageUtil.GetContent((int)LangEnum.REWARD_CATEGORY_DAILY_TASK);
                    toggleNameList.Add(CategoryItemData.GetCategoryItemData(title));
                    _rewardViewIndexList.Add((int)RewardViewType.DAILY_TASK_VIEW);
                }
                if (rewardData.IsRewardTypeOpen(RewardSystemType.MonthLoginReward))
                {
                    title = MogoLanguageUtil.GetContent((int)LangEnum.REWARD_CATEGORY_MONTH_LOGIN);
                    toggleNameList.Add(CategoryItemData.GetCategoryItemData(title));
                    _rewardViewIndexList.Add((int)RewardViewType.MONTH_LOGIN_REWARD_VIEW);
                }
                // 是否需要显示老玩家的信息
                if (PlayerReturnManager.Instance.IsOldPlayer == true && PlayerDataManager.Instance.playerReturnData.GetOldReturnRewardList().Count > 0)
                {
                    title = MogoLanguageUtil.GetContent((int)LangEnum.REWARD_PLAYER_RETURN);
                    toggleNameList.Add(CategoryItemData.GetCategoryItemData(title));
                    _rewardViewIndexList.Add((int)RewardViewType.PLAYER_RETURN_REWARD_VIEW);
                }
                // VIP系统暂时被屏蔽【只有VIP等级大于等于1才会显示该叶签，否则不予显示】
                if (rewardData.IsRewardTypeOpen(RewardSystemType.VipGift) && PlayerAvatar.Player.vip_level > 0)
                {
                    title = MogoLanguageUtil.GetContent((int)LangEnum.REWARD_CATEGORY_VIP_GIFT);
                    toggleNameList.Add(CategoryItemData.GetCategoryItemData(title));
                    _rewardViewIndexList.Add((int)RewardViewType.VIP_GIFT_VIEW);
                }
                _categoryToggleGroup.RemoveAll();
                _categoryToggleGroup.SetDataList<CategoryListItem>(toggleNameList);
                _isInitCategoryList = true;
            }
            currentCategoryIndex = int.MaxValue;
            for (int i = 0; i < _categoryToggleGroup.ItemList.Count; i++)
            {
                RefreshCategoryItemRedPoint(i);
            }
            if (currentCategoryIndex == int.MaxValue)
            {
                currentCategoryIndex = 0;
            }
        }

        private void RefreshSelectView(int categoryIndex)
        {
            if (_categoryToggleGroup.ItemList.Count == 0)
            {
                return;
            }
            if (categoryIndex < _categoryToggleGroup.ItemList.Count)
            {
                if (categoryIndex >= 0)
                {
                    _categoryToggleGroup.SelectedIndex = categoryIndex;
                }
                else
                {
                    _categoryToggleGroup.SelectedIndex = 0;
                }
            }
            else
            {
                _categoryToggleGroup.SelectedIndex = 0;
                LoggerHelper.Error("Refresh Reward Category, the category index is larger than contains item count !!!");
            }
            ShowView(_categoryToggleGroup.SelectedIndex);
        }

        public void ShowView(int index)
        {
            int viewIndex = _rewardViewIndexList[index];
            if (viewIndex == (int)RewardViewType.REWARD_COMPENSATION)
            {
                _dailyTaskView.Visible = false;
                _monthLoginRewardView.Visible = false;
                _dailyActiveTaskView.Visible = false;
                _vipGiftView.Visible = false;
                _playerReturnRewardView.Visible = false;
                PanelIdEnum.OpenServerLoginReward.Close();
                PanelIdEnum.RewardCompensation.Show();
            }
            else if (viewIndex == (int)RewardViewType.DAILY_TASK_VIEW)
            {
                _dailyTaskView.Visible = true;
                _monthLoginRewardView.Visible = false;
                _dailyActiveTaskView.Visible = false;
                _vipGiftView.Visible = false;
                _dailyTaskView.IsTweenList = true;
                PanelIdEnum.OpenServerLoginReward.Close();
                PanelIdEnum.RewardCompensation.Close();
                _playerReturnRewardView.Visible = false;
                _dailyTaskView.RefreshContent();
            }
            else if (viewIndex == (int)RewardViewType.MONTH_LOGIN_REWARD_VIEW)
            {
                _dailyTaskView.Visible = false;
                _monthLoginRewardView.Visible = true;
                _dailyActiveTaskView.Visible = false;
                _vipGiftView.Visible = false;
                PanelIdEnum.OpenServerLoginReward.Close();
                PanelIdEnum.RewardCompensation.Close();
                _playerReturnRewardView.Visible = false;
                _monthLoginRewardView.RefreshContent();
            }
            else if (viewIndex == (int)RewardViewType.DAILY_ACTIVE_TASK_VIEW)
            {
                _dailyTaskView.Visible = false;
                _monthLoginRewardView.Visible = false;
                _dailyActiveTaskView.Visible = true;
                _playerReturnRewardView.Visible = false;
                _vipGiftView.Visible = false;
                _dailyActiveTaskView.IsTweenList = true;
                PanelIdEnum.OpenServerLoginReward.Close();
                PanelIdEnum.RewardCompensation.Close();
                _dailyActiveTaskView.RefreshContent();
            }
            else if (viewIndex == (int)RewardViewType.VIP_GIFT_VIEW)
            {
                _dailyTaskView.Visible = false;
                _monthLoginRewardView.Visible = false;
                _dailyActiveTaskView.Visible = false;
                _playerReturnRewardView.Visible = false;
                _vipGiftView.Visible = true;
                PanelIdEnum.OpenServerLoginReward.Close();
                PanelIdEnum.RewardCompensation.Close();
                _vipGiftView.RefreshContent();
            }
            else if (viewIndex == (int)RewardViewType.OPEN_SERVER_LOGIN_REWARD_VIEW)
            {
                _dailyTaskView.Visible = false;
                _monthLoginRewardView.Visible = false;
                _dailyActiveTaskView.Visible = false;
                _vipGiftView.Visible = false;
                _playerReturnRewardView.Visible = false;
                PanelIdEnum.OpenServerLoginReward.Show();
                PanelIdEnum.RewardCompensation.Close();
            }
            else if (viewIndex == (int)RewardViewType.PLAYER_RETURN_REWARD_VIEW)
            {
                _dailyTaskView.Visible = false;
                _monthLoginRewardView.Visible = false;
                _dailyActiveTaskView.Visible = false;
                _vipGiftView.Visible = false;
                PanelIdEnum.OpenServerLoginReward.Close();
                PanelIdEnum.RewardCompensation.Close();
                _playerReturnRewardView.Visible = true;
                _playerReturnRewardView.Refresh();
            }

        }

        public void Hide()
        {
            if (this.Visible == true)
            {
                this.Visible = false;
            }
            PanelIdEnum.OpenServerLoginReward.Close();
            PanelIdEnum.RewardCompensation.Close();
        }
    }
}