﻿#region 模块信息
/*==========================================
// 文件名：AchievementItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleAchievement
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/10/30 13:36:40
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Chat;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System.Collections.Generic;
using UnityEngine;
namespace ModuleReward
{
    public class AchievementItem : KList.KListItemBase
    {
        private StateText _achievementNameTxt;
        private StateText _achievementDescTxt;
        private KProgressBar _achievementBar;
        private StateText _barTxt;

        private RewardGrid _rewardOne;
        private RewardGrid _rewardTwo;

        private KButton _showBtn;
        private KButton _getRewardBtn;
        private KParticle _getRewardParticle;
        private bool isShowParticle;
        private StateText _processingTxt;
        private SingleAchievementData _data;

        private SortOrderedRenderAgent _particleSortAgent;

        protected override void Awake()
        {
            KContainer _descContainer = GetChildComponent<KContainer>("Container_miaoshu");
            _achievementNameTxt = _descContainer.GetChildComponent<StateText>("Label_txtChengjiu");
            _achievementDescTxt = _descContainer.GetChildComponent<StateText>("Label_txtMiaoshu");
            _achievementBar = _descContainer.GetChildComponent<KProgressBar>("ProgressBar_jindu");
            _barTxt = _descContainer.GetChildComponent<StateText>("Label_txtJindu");
            _rewardOne = AddChildComponent<RewardGrid>("Container_jiangli01");
            _rewardTwo = AddChildComponent<RewardGrid>("Container_jiangli02");
            KContainer _stateContainer = GetChildComponent<KContainer>("Container_zhuangtai");
            _showBtn = _stateContainer.GetChildComponent<KButton>("Button_zhanshi");
            _getRewardBtn = _stateContainer.GetChildComponent<KButton>("Button_lingqu");
            _getRewardParticle = _getRewardBtn.AddChildComponent<KParticle>("fx_ui_3_2_lingqu");
            _particleSortAgent = _getRewardParticle.GetComponent<SortOrderedRenderAgent>();
            _getRewardParticle.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
            MaskEffectItem _effectItem = gameObject.AddComponent<MaskEffectItem>();
            _effectItem.Init(_getRewardParticle.transform.parent.gameObject, IsParticleShow);
            _getRewardParticle.Stop();
            isShowParticle = false;
            _processingTxt = _stateContainer.GetChildComponent<StateText>("Label_txtJinxingzhong");
            AddEventListener();
        }

        private bool IsParticleShow()
        {
            return isShowParticle;
        }

        private void AddEventListener()
        {
            _showBtn.onClick.AddListener(OnShowBtnClicked);
            _getRewardBtn.onClick.AddListener(OnGetRewardBtnClicked);
        }

        private void RemoveEventListener()
        {
            _showBtn.onClick.RemoveListener(OnShowBtnClicked);
            _getRewardBtn.onClick.RemoveListener(OnGetRewardBtnClicked);
        }

        private void OnGetRewardBtnClicked()
        {
            AchievementManager.Instance.RequestGetAchievementReward(_data.id);
        }

        private void OnShowBtnClicked()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.Reward);
            ChatItemLinkWrapper wrapper = new ChatItemLinkWrapper(_data.id);
            UIManager.Instance.ShowPanel(PanelIdEnum.Chat, wrapper);
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as SingleAchievementData;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            RefreshAchievementDesc();
            RefreshAchievementReward();
            RefreshAchievementState();
            RefreshAchievementProgress();
        }

        private void RefreshAchievementProgress()
        {
            _getRewardParticle.Stop();
            switch (_data.state)
            {
                case AchievementState.NotStarted:
                    _barTxt.CurrentText.text = string.Format("{0}/{1}", _data.currentNum, _data.targetNum);
                    _achievementBar.Percent = 0;
                    break;
                case AchievementState.Processing:
                    _barTxt.CurrentText.text = string.Format("{0}/{1}", _data.currentNum, _data.targetNum);
                    if (_data.targetNum != 0)
                    {
                        _achievementBar.Percent = Mathf.FloorToInt(_data.currentNum * 100.0f / _data.targetNum);
                    }
                    else
                    {
                        _achievementBar.Percent = 0;
                    }
                    break;
                case AchievementState.Completed:
                    _getRewardParticle.Play(true);
                    SortOrderedRenderAgent.RebuildAll();
                    isShowParticle = true;
                    _barTxt.CurrentText.text = string.Format("{0}/{1}", _data.targetNum, _data.targetNum);
                    _achievementBar.Percent = 100;
                    break;
                case AchievementState.Rewarded:
                    _barTxt.CurrentText.text = string.Format("{0}/{1}", _data.targetNum, _data.targetNum);
                    _achievementBar.Percent = 100;
                    break;
            }
        }

        private void RefreshAchievementState()
        {
            _getRewardBtn.Visible = false;
            _showBtn.Visible = false;
            _processingTxt.Visible = false;
            switch (_data.state)
            {
                case AchievementState.NotStarted:
                case AchievementState.Processing:
                    _processingTxt.Visible = true;
                    break;
                case AchievementState.Completed:
                    _getRewardBtn.Visible = true;
                    break;
                case AchievementState.Rewarded:
                    _showBtn.Visible = true;
                    break;
            }
        }

        private void RefreshAchievementReward()
        {
            List<RewardData> rewardDataList = achievement_helper.GetAchievementReward(_data.id);
            if (rewardDataList != null && rewardDataList.Count > 0)
            {
                _rewardOne.Visible = true;
                _rewardOne.Data = rewardDataList[0];
            }
            else
            {
                _rewardOne.Visible = false;
            }
            if (rewardDataList != null && rewardDataList.Count > 1)
            {
                _rewardTwo.Visible = true;
                _rewardTwo.Data = rewardDataList[1];
            }
            else
            {
                _rewardTwo.Visible = false;
            }
        }

        private void RefreshAchievementDesc()
        {
            _achievementNameTxt.CurrentText.text = achievement_helper.GetAchievementName(_data.id);
            _achievementDescTxt.CurrentText.text = achievement_helper.GetAchievementDesc(_data.id);
        }

        public override void Dispose()
        {
            RemoveEventListener();
        }
    }
}