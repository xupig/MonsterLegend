﻿#region 模块信息
/*==========================================
// 文件名：AchievementContentView
// 命名空间: GameLogic.GameLogic.Modules.ModuleAchievement
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/10/30 11:17:03
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System.Collections.Generic;
namespace ModuleReward
{
    public class AchievementContentView : KContainer
    {
        private StateText _txtHasGetAllAchievement;
        private KPageableList _achievementList;
        private KScrollView _scrollView;
        private List<SingleAchievementData> list;
        private int beginIndex = 0;
        private const int stepCount = 5;

        protected override void Awake()
        {
            _txtHasGetAllAchievement = GetChildComponent<StateText>("Label_txtDacheng");
            _txtHasGetAllAchievement.Visible = false;
            _scrollView = GetChildComponent<KScrollView>("ScrollView_chengjiu");
            _achievementList = GetChildComponent<KPageableList>("ScrollView_chengjiu/mask/content");
            _achievementList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _achievementList.SetGap(-5, 0);
            _scrollView.ScrollRect.onRequestNext.AddListener(OnDragNextDown);
        }


        public void Refresh(int groupId)
        {
            _scrollView.ResetContentPosition();
            beginIndex = 0;
            if (groupId != achievement_helper.HASREWARDED)
            {
                list = PlayerDataManager.Instance.AchievementData.GetAchievementList(groupId);
                if (list.Count == 0)
                {
                    _txtHasGetAllAchievement.Visible = true;
                }
                else
                {
                    _txtHasGetAllAchievement.Visible = false;
                }
            }
            else
            {
                list = PlayerDataManager.Instance.AchievementData.GetRewardedAchievementList();
            }
            List<SingleAchievementData> dataList = GetRangeOfList(beginIndex, stepCount);
            _achievementList.SetDataList<AchievementItem>(dataList);
            TweenListEntry.Begin(_achievementList.gameObject, TweenListEntryMoveDirection.RightToLeft);
            beginIndex = dataList.Count;
        }

        private void OnDragNextDown(KScrollRect scrollRect)
        {
            List<SingleAchievementData> addedList = GetRangeOfList(beginIndex, stepCount);
            for (int i = 0; i < addedList.Count; i++)
            {
                if (_achievementList.ItemList.Count <= beginIndex + i)
                {
                    _achievementList.AddItem<AchievementItem>(addedList[i]);
                }
                else
                {
                    _achievementList.ItemList[beginIndex+i].Visible = true;
                    _achievementList.ItemList[beginIndex+i].Data = addedList[i];
                }
            }
            _achievementList.DoLayout();
            beginIndex += addedList.Count;
        }

        private List<SingleAchievementData> GetRangeOfList(int beginIndex, int count)
        {
            List<SingleAchievementData> result = new List<SingleAchievementData>();
            if (beginIndex < list.Count)
            {
                for (int i = beginIndex; i < beginIndex + count && i < list.Count; i++)
                {
                    result.Add(list[i]);
                }
            }
            return result;
        }

    }
}