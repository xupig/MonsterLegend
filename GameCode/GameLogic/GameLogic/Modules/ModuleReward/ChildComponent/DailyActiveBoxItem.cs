﻿using Game.UI.UIComponent;
using Common.Data;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using GameData;
using UnityEngine;
using Common.ExtendComponent;
using GameMain.Entities;
using Common.Utils;
using Common.Global;

namespace ModuleReward
{
    public class DailyActiveBoxItem : KContainer
    {
        private DailyActiveBoxData _boxData;
        private StateText _activePointTxt;
        private ItemGrid _rewardGrid;
        private StateText _rewardNumText;
        private StateImage _haveRecievedImg;
        private Vector2 _boxOriginPos = Vector2.zero;
        private KParticle _boxEffect;

        private string _activePointTemplate;

        public object Data
        {
            get
            {
                return _boxData;
            }
            set
            {
                _boxData = value as DailyActiveBoxData;
                Refresh();
            }
        }

        public void SetItemData(DailyActiveBoxData boxData)
        {
            Data = boxData;
        }

        protected override void Awake()
        {
            _activePointTxt = GetChildComponent<StateText>("Label_txtShuliang");
            _rewardGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _rewardNumText = _rewardGrid.GetChildComponent<StateText>("Label_txtNum");
            _haveRecievedImg = GetChildComponent<StateImage>("Image_sharedyilingqu");
            _boxOriginPos = _rewardGrid.gameObject.GetComponent<RectTransform>().anchoredPosition;
            _haveRecievedImg.Visible = false;
            _activePointTemplate = _activePointTxt.CurrentText.text;
            AddEventListener();
        }

        private void AddEventListener()
        {
            _rewardGrid.onClick += OnClickBoxBtn;
        }

        private void RemoveEventListener()
        {
            _rewardGrid.onClick -= OnClickBoxBtn;
        }

        private void Refresh()
        {
            RefreshReward();
            _activePointTxt.CurrentText.text = string.Format(_activePointTemplate, _boxData.Point.ToString());
            if (_boxEffect == null)
            {
                _boxEffect = _rewardGrid.GetChild("fx_ui_24_2_baoxianglingqu").AddComponent<KParticle>();
            }
            if (_boxData.Status == (int)ActiveBoxStatus.CanGetReward)
            {
                if (_boxData.RewardFlag == reward_system_helper.HAVE_REWARD_FLAG)
                {
                    TweenBoxPosition();
                    ShowBoxEffect();
                    HideHaveRecievedTag();
                }
                else
                {
                    StopTweenPosition();
                    HideBoxEffect();
                    ResetRewardPos();
                    ShowHaveRecievedTag();
                }
            }
            else
            {
                StopTweenPosition();
                HideBoxEffect();
                ResetRewardPos();
                HideHaveRecievedTag();
            }
        }

        private void RefreshReward()
        {
            if (_boxData.RewardList.Count > 0)
            {
                _rewardGrid.SetItemData(_boxData.RewardList[0].id);
                int num = _boxData.RewardList[0].num;
                if (num > 1)
                {
                    _rewardNumText.Visible = true;
                    _rewardNumText.CurrentText.text = num.ToString();
                }
                else
                {
                    _rewardNumText.Visible = false;
                }
            }
        }

        private void TweenBoxPosition()
        {
            TweenPosition tweenPos = _rewardGrid.gameObject.GetComponent<TweenPosition>();
            if (tweenPos == null)
            {
                Vector3 destPos = new Vector3(_boxOriginPos.x, _boxOriginPos.y, 0);
                destPos.y += 5f;
                tweenPos = TweenPosition.Begin(_rewardGrid.gameObject, 0.5f, destPos, 0f, UITweener.Method.Linear);
                tweenPos.style = UITweener.Style.PingPong;
            }
        }

        private void StopTweenPosition()
        {
            TweenPosition tweenPos = _rewardGrid.gameObject.GetComponent<TweenPosition>();
            if (tweenPos != null)
            {
                GameObject.Destroy(tweenPos);
            }
        }

        private void ResetRewardPos()
        {
            RectTransform rect = _rewardGrid.gameObject.GetComponent<RectTransform>();
            if (rect.anchoredPosition.x != _boxOriginPos.x || rect.anchoredPosition.y != _boxOriginPos.y)
            {
                rect.anchoredPosition = _boxOriginPos;
            }
        }

        private void ShowBoxEffect()
        {
            if (_boxEffect.Visible == false)
            {
                _boxEffect.Visible = true;
            }
            _boxEffect.transform.localPosition = new Vector3(7f, 7.12f, 0f);
        }

        private void HideBoxEffect()
        {
            if (_boxEffect.Visible == true)
            {
                _boxEffect.Visible = false;
            }
        }

        private void ShowHaveRecievedTag()
        {
            if (_haveRecievedImg.Visible == false)
            {
                _haveRecievedImg.Visible = true;
            }
        }

        private void HideHaveRecievedTag()
        {
            if (_haveRecievedImg.Visible == true)
            {
                _haveRecievedImg.Visible = false;
            }
        }

        private void OnClickBoxBtn()
        {
            if (_boxData.Status == (int)ActiveBoxStatus.CanGetReward)
            {
                if (_boxData.RewardFlag == reward_system_helper.HAVE_REWARD_FLAG)
                {
                    RewardManager.Instance.RequestGetDailyActiveBoxReward(_boxData.Id);
                }
                else
                {
                    if (_boxData.VipRewardFlag == reward_system_helper.HAVE_REWARD_FLAG)
                    {
                        MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent((int)LangEnum.REWARD_STILL_HAVE_VIP_REWARD));
                    }
                    else
                    {
                        MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent((int)LangEnum.REWARD_NOT_HAVE_REWARD));
                    }
                }
            }
            else
            {
                if (_rewardGrid.ItemId != 0)
                {
                    ToolTipsManager.Instance.ShowItemTip(_rewardGrid.ItemId, _rewardGrid.Context);
                }
            }
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }
    }
}
