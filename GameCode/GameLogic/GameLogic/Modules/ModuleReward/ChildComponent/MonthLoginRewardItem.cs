﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.ExtendComponent;
using Common.Data;
using UnityEngine;
using GameMain.GlobalManager;
using ModuleCommonUI;
using GameMain.GlobalManager.SubSystem;
using GameMain.Entities;
using Common.Base;
using GameData;
using Common.Structs.ProtoBuf;
using Common.Structs;
using Common.Utils;
using Common.Global;

namespace ModuleReward
{
    public class MonthLoginRewardItem : KList.KListItemBase
    {
        private MonthLoginItemData _itemData;
        private StateText _dayText;
        private StateImage _haveGetRewardTag;
        private KContainer _vipTagContainer;
        private StateText _vipText;
        private KButton _getRewardBtn;
        private StateImage _rewardHighlight;
        private StateImage _itemMask;
        private KParticle _getRewardEffect;
        private KContainer _rewardIcon;
        private StateImage _borderImg;

        private int _rewardIconId;
        private const int MALL_QUALITY_ICON_ID = 15700;

        public override object Data
        {
            get
            {
                return _itemData;
            }
            set
            {
                _itemData = value as MonthLoginItemData;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            Refresh();
        }

        protected override void Awake()
        {
            _rewardIcon = GetChildComponent<KContainer>("Container_icon");
            _dayText = GetChildComponent<StateText>("Label_tianshu");
            _haveGetRewardTag = GetChildComponent<StateImage>("Image_sharedyilingqu");
            _vipTagContainer = GetChildComponent<KContainer>("Container_vipTag");
            _vipText = _vipTagContainer.GetChildComponent<StateText>("Label_txtVip");
            Vector3 anchorPos = _vipText.CurrentText.rectTransform.anchoredPosition;
            anchorPos.x = 0f;
            anchorPos.y = -59.3f;
            _vipText.CurrentText.rectTransform.anchoredPosition = anchorPos;
            Vector3 eulerAngles = _vipText.CurrentText.rectTransform.localEulerAngles;
            eulerAngles.z = 45f;
            _vipText.CurrentText.rectTransform.localEulerAngles = eulerAngles;
            _getRewardBtn = GetChildComponent<KButton>("Button_jiangli");
            _rewardHighlight = _getRewardBtn.GetChildComponent<StateImage>("checkmark");
            _itemMask = GetChildComponent<StateImage>("Image_blackMask");
            _itemMask.Visible = false;
            _borderImg = GetChildComponent<StateImage>("Container_pinzhikuang/Image_pinzhikuang");
            _getRewardEffect = _getRewardBtn.GetChild("fx_ui_27_qiandaojiangli").AddComponent<KParticle>();
            MaskEffectItem maskEffectItem = gameObject.AddComponent<MaskEffectItem>();
            maskEffectItem.Init(_getRewardBtn.gameObject, CanShowEffect);
            AddEventListener();
        }

        private void AddEventListener()
        {
            _getRewardBtn.onClick.AddListener(OnClickGetRewardBtn);
            this.onClick.AddListener(OnClickItemRewardBtn);
        }

        private void RemoveEventListener()
        {
            _getRewardBtn.onClick.RemoveListener(OnClickGetRewardBtn);
            this.onClick.RemoveListener(OnClickItemRewardBtn);
        }

        public override void Dispose()
        {
            RemoveEventListener();
        }

        private void Refresh()
        {
            _dayText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent((int)LangEnum.REWARD_LOGIN_DAY), _itemData.Day);
            if (_itemData.Status == (int)MonthLoginStatus.HaveReceived)
            {
                _haveGetRewardTag.Visible = true;
            }
            else
            {
                _haveGetRewardTag.Visible = false;
            }

            //当天可领取的奖励高亮显示
            MonthLoginData monthLoginData = PlayerDataManager.Instance.RewardData.GetMonthLoginData();
            if (_itemData.Day == monthLoginData.SignDayNum)
            {
                _rewardHighlight.Visible = true;
                if (_itemData.Status == (int)MonthLoginStatus.HaveReceived)
                {
                    HideGetRewardEffect();
                }
                else
                {
                    ShowGetRewardEffect();
                }
            }
            else
            {
                _rewardHighlight.Visible = false;
                HideGetRewardEffect();
            }

            if (_itemData.Day != monthLoginData.SignDayNum && _itemData.Status == (int)MonthLoginStatus.HaveReceived)
            {
                _itemMask.Visible = true;
            }
            else
            {
                _itemMask.Visible = false;
            }

            if (_itemData.VipNeed > 0 && PlayerAvatar.Player.vip_level > 0)
            {
                _vipTagContainer.Visible = true;
                _vipText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent((int)LangEnum.REWARD_VIP_NUM_DOUBLE_REWARD), _itemData.VipNeed);
            }
            else
            {
                _vipTagContainer.Visible = false;
            }
            RefreshItemGrid();
        }

        private void RefreshItemGrid()
        {
            List<RewardData> rewardList = _itemData.RewardList;
            if (rewardList == null || rewardList.Count == 0)
                return;

            int iconId = item_helper.GetIcon(rewardList[0].id);
            if (_rewardIconId != iconId)
            {
                _rewardIconId = iconId;
                MogoAtlasUtils.AddIcon(_rewardIcon.gameObject, iconId);
                _borderImg.CurrentImage.color = item_helper.GetColor(rewardList[0].id);
            }
            MogoShaderUtils.ChangeShader(_borderImg.CurrentImage, MogoShaderUtils.IconShaderPath);
            StateText rewardCountText = GetChildComponent<StateText>("Label_txtShuliang");
            rewardCountText.CurrentText.text = rewardList[0].num > 1 ? rewardList[0].num.ToString() : "";
        }

        private void ShowGetRewardEffect()
        {
            if (_getRewardEffect.Visible == false)
            {
                _getRewardEffect.Visible = true;
            }
            _getRewardEffect.transform.localPosition = new Vector3(-3f, 5f, -5f);
        }

        private void HideGetRewardEffect()
        {
            if (_getRewardEffect.Visible == true)
            {
                _getRewardEffect.Visible = false;
            }
        }

        private bool CanShowEffect()
        {
            if (_itemData != null)
            {
                MonthLoginData monthLoginData = PlayerDataManager.Instance.RewardData.GetMonthLoginData();
                return (_itemData.Day == monthLoginData.SignDayNum && _itemData.Status == (int)MonthLoginStatus.UnReceive);
            }
            else
            {
                return false;
            }
        }

        private void OnClickGetRewardBtn()
        {
            OnClickItemRewardBtn(null, 0);
        }

        private void OnClickItemRewardBtn(KList.KListItemBase target, int index)
        {
            //如果当前未领取，请求领取
            //如果已领取，弹提示，看是否可领取双倍奖励
            MonthLoginData monthLoginData = PlayerDataManager.Instance.RewardData.GetMonthLoginData();
            if (_itemData.Day == monthLoginData.SignDayNum)
            {
                if (_itemData.Status == (int)MonthLoginStatus.UnReceive)
                {
                    RewardManager.Instance.RequestGetMonthLoginReward();
                }
                else
                {
                    int rewardId = _itemData.RewardList[0].id;
                    ShowItemRewardTip(rewardId);
                }
            }
            else
            {
                ShowItemRewardTip(_itemData.RewardList[0].id);
            }
        }

        private void ShowItemRewardTip(int rewardId)
        {
            if (item_helper.IsEquip(rewardId))
            {
                BaseItemInfo itemInfo = ItemInfoCreator.CreateItemInfo(rewardId, _itemData.RewardList[0].num);
                ToolTipsManager.Instance.ShowItemTip(itemInfo, PanelIdEnum.Reward, EquipState.NotHave, false);
            }
            else
            {
                ToolTipsManager.Instance.ShowItemTip(rewardId, PanelIdEnum.Reward);
            }
        }
    }
}
