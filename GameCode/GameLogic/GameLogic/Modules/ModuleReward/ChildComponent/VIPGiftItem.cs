﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.Data;
using Common.ExtendComponent;
using GameMain.GlobalManager;
using Common.Base;
using ModuleCommonUI;
using GameData;
using UnityEngine;
using Common.Utils;
using Common.Global;

namespace ModuleReward
{
    public class VIPGiftItem : KList.KListItemBase
    {
        private VipGiftItemData _itemData;
        private List<KContainer> _rewardContainerList = new List<KContainer>();
        private List<ItemGrid> _rewardGridList = new List<ItemGrid>();
        private StateText _titleText;
        private KButton _vipDetailInfoBtn;
        private KButton _receiveVipGiftBtn;
        private StateImage _haveReceivedGiftTag;

        private const int MAX_REWARD_NUM = 4;
        private const int VIP_DETAIL_INFO_BTN = 0;
        private const int RECEIVE_VIP_GIFT_BTN = 1;
        private const int HAVE_RECEIVED_GIFT_TAG = 2;

        public override object Data
        {
            get
            {
                return _itemData;
            }
            set
            {
                _itemData = value as VipGiftItemData;
                Refresh();
            }
        }

        protected override void Awake()
        {
            for (int i = 0; i < MAX_REWARD_NUM; i++)
            {
                KContainer reward = GetChildComponent<KContainer>("Container_xiaobiaoti/Container_reward" + (i + 1));
                _rewardContainerList.Add(reward);
                ItemGrid rewardGrid = reward.GetChildComponent<ItemGrid>("Container_wupin");
                _rewardGridList.Add(rewardGrid);
            }
            _titleText = GetChildComponent<StateText>("Container_xiaobiaoti/Label_txtLibao");
            _vipDetailInfoBtn = GetChildComponent<KButton>("Container_xiaobiaoti/Button_chakanVIP");
            _receiveVipGiftBtn = GetChildComponent<KButton>("Container_xiaobiaoti/Button_qulingqu");
            _haveReceivedGiftTag = GetChildComponent<StateImage>("Container_xiaobiaoti/Image_sharedyilingqu");

            AddEventListener();
        }

        private void AddEventListener()
        {
            _vipDetailInfoBtn.onClick.AddListener(OnClickVipDetailInfoBtn);
            _receiveVipGiftBtn.onClick.AddListener(OnClickReceiveVipGiftBtn);
           
        }

        private void RemoveEventListener()
        {
            _vipDetailInfoBtn.onClick.RemoveListener(OnClickVipDetailInfoBtn);
            _receiveVipGiftBtn.onClick.RemoveListener(OnClickReceiveVipGiftBtn);
        }

        public override void Dispose()
        {
            RemoveEventListener();
        }

        private void Refresh()
        {
            RefreshRewardList();
            RefreshTitleText();
            RefreshStatusWidgets();
        }

        private void RefreshRewardList()
        {
            for (int i = 0; i < _itemData.RewardList.Count; i++)
            {
                _rewardContainerList[i].Visible = true;
                ItemGrid rewardGrid = _rewardGridList[i];
                rewardGrid.SetItemData(_itemData.RewardList[i].id);
                StateText rewardNumText = _rewardContainerList[i].GetChildComponent<StateText>("Label_txtShuliang");
                rewardNumText.CurrentText.text = _itemData.RewardList[i].num > 1 ? _itemData.RewardList[i].num.ToString() : "";
            }
            for (int i = _itemData.RewardList.Count; i < MAX_REWARD_NUM; i++)
            {
                _rewardContainerList[i].Visible = false;
            }
        }

        private void RefreshTitleText()
        {
            _titleText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent((int)LangEnum.REWARD_VIP_NUM_GIFT), _itemData.VipLv);
        }

        private void RefreshStatusWidgets()
        {
            if (_itemData.IsReachVipNeed())
            {
                if (_itemData.RewardFlag == reward_system_helper.HAVE_REWARD_FLAG)
                {
                    EnableStatusWidget(RECEIVE_VIP_GIFT_BTN);
                }
                else
                {
                    EnableStatusWidget(HAVE_RECEIVED_GIFT_TAG);
                }
            }
            else
            {
                EnableStatusWidget(VIP_DETAIL_INFO_BTN);
            }
        }

        private void EnableStatusWidget(int widgetIndex)
        {
            if (widgetIndex == VIP_DETAIL_INFO_BTN)
            {
                _vipDetailInfoBtn.Visible = true;
                _receiveVipGiftBtn.Visible = false;
                _haveReceivedGiftTag.Visible = false;
            }
            else if (widgetIndex == RECEIVE_VIP_GIFT_BTN)
            {
                _vipDetailInfoBtn.Visible = false;
                _receiveVipGiftBtn.Visible = true;
                _haveReceivedGiftTag.Visible = false;
            }
            else if (widgetIndex == HAVE_RECEIVED_GIFT_TAG)
            {
                _vipDetailInfoBtn.Visible = false;
                _receiveVipGiftBtn.Visible = false;
                _haveReceivedGiftTag.Visible = true;
            }

        }

        private void OnClickVipDetailInfoBtn()
        {
            PanelIdEnum.Charge.Show(new int[] { 3, _itemData.VipLv });
        }

        private void OnClickReceiveVipGiftBtn()
        {
            PanelIdEnum.Charge.Show(new int[] { 3, _itemData.VipLv });
        }

    }
}
