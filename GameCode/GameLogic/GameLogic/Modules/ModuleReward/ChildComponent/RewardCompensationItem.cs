﻿using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleReward
{
    public class RewardCompensationItem:KList.KListItemBase
    {
        private ItemGrid _itemGrid;
        private StateText _txtNum;

        private BaseItemData _data;

        protected override void Awake()
        {
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _txtNum = GetChildComponent<StateText>("Label_txtShuliang");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as BaseItemData;
                SetDataDirty();
            }
        }

        public override void Dispose()
        {
            _data = null;
        }

        public override void DoRefreshData()
        {
            Refresh();
        }

        private void Refresh()
        {
            _itemGrid.SetItemData(_data);
            _txtNum.CurrentText.text = _data.StackCount.ToString();
        }


    }
}
