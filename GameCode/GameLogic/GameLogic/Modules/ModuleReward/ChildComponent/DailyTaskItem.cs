﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.Data;
using Common.ExtendComponent;
using GameMain.GlobalManager.SubSystem;
using Common.Utils;
using GameData;
using GameMain.Entities;
using UnityEngine;
using ModuleCommonUI;
using Common.Base;
using Game.UI;
using GameMain.GlobalManager;
using Common.Global;

namespace ModuleReward
{
    public class DailyTaskItem : KList.KListItemBase
    {
        private DailyTaskItemData _dailyItemData;
        private int _iconId = 0;
        private KButton _goBtn;
        private KButton _getRewardBtn;
        private StateImage _haveGetRewardTag;
        private StateText _notReachTimeText;
        private StateText _nameText;
        private StateText _descText;
        private ItemGrid _rewardGrid;
        private StateText _remainTimeText;
        private KContainer _taskIcon;
        private Color _nameColor = Color.white;
        private Color _descColor = Color.white;
        private readonly Color _grayTextColor = new Color(188f / 255f, 188f / 255f, 188f / 255f, 1.0f);

        private Dictionary<int, IKContainer> _statusContainers = new Dictionary<int, IKContainer>();
        private Dictionary<int, StateChangeable> _statusChangeables = new Dictionary<int, StateChangeable>();

        private KParticle _getRewardEffect;

        private const int GO_BTN = 0;
        private const int GET_REWARD_BTN = 1;
        private const int HAVE_GET_REWARD_TAG = 2;
        private const int NOT_REACH_TIME_TEXT = 3;

        private int _id = 0;
        private bool _isInfoGray = false;
        private bool _canRefreshInfoColor = false;

        public override object Data
        {
            get
            {
                return _dailyItemData;
            }
            set
            {
                _dailyItemData = value as DailyTaskItemData;
                if (_id != _dailyItemData.Id)
                {
                    _canRefreshInfoColor = true;
                    _id = _dailyItemData.Id;
                }
                
                ID = _dailyItemData.Id;
                Refresh();
            }
        }

        protected override void Awake()
        {
            _goBtn = GetChildComponent<KButton>("Button_qianwang");
            _getRewardBtn = GetChildComponent<KButton>("Button_lingqu");
            _haveGetRewardTag = GetChildComponent<StateImage>("Container_reward1/Image_sharedyilingqu");
            _notReachTimeText = GetChildComponent<StateText>("Label_txtShijianweidao");
            _statusContainers.Add(GO_BTN, _goBtn);
            _statusContainers.Add(GET_REWARD_BTN, _getRewardBtn);
            _statusChangeables.Add(HAVE_GET_REWARD_TAG, _haveGetRewardTag);
            _statusChangeables.Add(NOT_REACH_TIME_TEXT, _notReachTimeText);
            _rewardGrid = GetChildComponent<ItemGrid>("Container_reward1/Container_wupin");
            _remainTimeText = GetChildComponent<StateText>("Label_txtRemainTime");
            _taskIcon = GetChildComponent<KContainer>("Container_taskIcon/Container_icon");
            _nameText = GetChildComponent<StateText>("Label_txtName");
            _descText = GetChildComponent<StateText>("Label_txtDetailDesc");
            _nameColor = _nameText.CurrentText.color;
            _descColor = _descText.CurrentText.color;
            _getRewardEffect = _getRewardBtn.GetChild("fx_ui_3_2_lingqu").AddComponent<KParticle>();
            MaskEffectItem maskEffectItem = gameObject.AddComponent<MaskEffectItem>();
            maskEffectItem.Init(_getRewardBtn.gameObject, CanShowEffect);
            AddEventListener();
        }

        private void AddEventListener()
        {
            _goBtn.onClick.AddListener(OnClickGoBtn);
            _getRewardBtn.onClick.AddListener(OnClickGetRewardBtn);
        }

        private void RemoveEventListener()
        {
            _goBtn.onClick.RemoveListener(OnClickGoBtn);
            _getRewardBtn.onClick.RemoveListener(OnClickGetRewardBtn);
        }

        public override void Dispose()
        {
            RemoveEventListener();
            _statusContainers.Clear();
            _statusChangeables.Clear();
        }

        private void Refresh()
        {
            RefreshStatusWidgets();
            RefreshTaskBaseInfo();
            //更新奖励物品图标等信息
            RefreshItemRewardInfo();
            RefreshRemainTimeText();
            RefreshInfoColor();
        }

        private void RefreshStatusWidgets()
        {
            if (_dailyItemData.TimeStatus == (int)DailyTaskTimeStatus.Valid)
            {
                if (_dailyItemData.Status == (int)DailyTaskStatus.Accept)
                {
                    if (_dailyItemData.EventId != 0)
                    {
                        Dictionary<string, string[]> evtFollow = event_cnds_helper.GetEventFollow(_dailyItemData.EventId);
                        if (evtFollow != null && evtFollow.Count > 0)
                        {
                            EnableStatusWidgetsVisible(GO_BTN);
                        }
                        else
                        {
                            DisableAllStatusWidgets();
                        }
                    }
                    else
                    {
                        DisableAllStatusWidgets();
                    }
                }
                else if (_dailyItemData.Status == (int)DailyTaskStatus.CanCommit)
                {
                    EnableStatusWidgetsVisible(GET_REWARD_BTN);
                    ShowGetRewardEffect();
                }
                else if (_dailyItemData.Status == (int)DailyTaskStatus.Complete)
                {
                    EnableStatusWidgetsVisible(HAVE_GET_REWARD_TAG);
                }
            }
            else if (_dailyItemData.TimeStatus == (int)DailyTaskTimeStatus.NotStarted)
            {
                EnableStatusWidgetsVisible(NOT_REACH_TIME_TEXT);
                _notReachTimeText.CurrentText.text = MogoLanguageUtil.GetContent((int)LangEnum.REWARD_DAILY_TASK_NOT_REACH_TIME);
            }
            else if (_dailyItemData.TimeStatus == (int)DailyTaskTimeStatus.HaveEnded)
            {
                EnableStatusWidgetsVisible(NOT_REACH_TIME_TEXT);
                _notReachTimeText.CurrentText.text = MogoLanguageUtil.GetContent((int)LangEnum.REWARD_DAILY_TASK_TIME_END);
            }
        }

        private void RefreshTaskBaseInfo()
        {
            _nameText.CurrentText.text = _dailyItemData.Name;
            _descText.CurrentText.text = _dailyItemData.Desc;
            if (_dailyItemData.TaskType == (int)DailyTaskType.MonthCard)
            {
                _remainTimeText.Visible = false;
            }
            else
            {
                _remainTimeText.Visible = false;
            }
            RefreshIcon();
        }

        private void RefreshIcon()
        {
            if (_iconId != _dailyItemData.IconId)
            {
                _iconId = _dailyItemData.IconId;
                MogoAtlasUtils.AddIcon(_taskIcon.gameObject, _iconId, OnTaskIconLoaded);
            }
        }

        private void OnTaskIconLoaded(GameObject spriteGo)
        {
            RefreshIconSpriteColor(spriteGo);
        }

        private void OnRewardSpritesLoaded(GameObject spriteGo)
        {
            RefreshIconSpriteColor(spriteGo);
        }

        private void RefreshItemRewardInfo()
        {
            List<RewardData> rewardList = _dailyItemData.RewardList;
            if (rewardList == null || rewardList.Count == 0)
                return;

            _rewardGrid.SetItemData(rewardList[0].id, OnRewardSpritesLoaded);
            StateText rewardCountText = GetChildComponent<StateText>("Container_reward1/Label_txtNum");
            rewardCountText.CurrentText.text = rewardList[0].num > 1 ? rewardList[0].num.ToString() : "";
        }

        private void EnableStatusWidgetsVisible(int widgetIndex)
        {
            foreach (var item in _statusContainers)
            {
                if (item.Key == widgetIndex)
                {
                    item.Value.Visible = true;
                }
                else
                {
                    item.Value.Visible = false;
                }
            }
            foreach (var item in _statusChangeables)
            {
                if (item.Key == widgetIndex)
                {
                    item.Value.Visible = true;
                }
                else
                {
                    item.Value.Visible = false;
                }
            }
        }

        private void DisableAllStatusWidgets()
        {
            foreach (var item in _statusContainers)
            {
                item.Value.Visible = false;
            }
            foreach (var item in _statusChangeables)
            {
                item.Value.Visible = false;
            }
        }

        private void ShowGetRewardEffect()
        {
            if (_getRewardEffect.Visible == false)
            {
                _getRewardEffect.Visible = true;
            }
            _getRewardEffect.transform.localPosition = new Vector3(0, 0.54f, -2);
            _getRewardEffect.transform.localScale = new Vector3(1f, 1f, 1f);
        }

        private void RefreshRemainTimeText()
        {
            if (reward_system_helper.IsOnlineTimeEvent(_dailyItemData.EventId))
            {
                RefreshTimeInfo();
            }
            else if (_dailyItemData.IsShowTaskTypeNumProgress())
            {
                if (reward_system_helper.IsDailyAccumulationEvent(_dailyItemData.EvtAtomId))
                {
                    RefreshDailyAccumulationInfo();
                }
                else
                {
                    RefreshProgressInfo();
                }
            }
            else if (reward_system_helper.IsDailyAccumulationEvent(_dailyItemData.EvtAtomId))
            {
                RefreshDailyAccumulationInfo();
            }
            else
            {
                if (_dailyItemData.TaskType != (int)DailyTaskType.MonthCard)
                {
                    _remainTimeText.Visible = false;
                }
            }
        }

        private void RefreshTimeInfo()
        {
            if (_dailyItemData.Status != (int)DailyTaskStatus.Complete)
            {
                _remainTimeText.Visible = true;
                uint onlineMinute = GetOnlineTimeMinute();
                if (onlineMinute < 60)
                {
                    _remainTimeText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent((int)LangEnum.REWARD_ONLINE_MINUTE), onlineMinute);
                }
                else
                {
                    uint onlineHour = (uint)Mathf.FloorToInt((float)onlineMinute / 60.0f);
                    _remainTimeText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent((int)LangEnum.REWARD_ONLINE_HOUR), onlineHour);
                }
            }
        }

        public void RefreshMonthCardTimeInfo(uint leftSeconds, bool isCardValid)
        {
            if (_dailyItemData.TaskType != (int)DailyTaskType.MonthCard)
            {
                return;
            }
            if (!isCardValid)
            {
                _remainTimeText.Visible = true;
                _remainTimeText.CurrentText.text = MogoLanguageUtil.GetContent(81);
                return;
            }
            _remainTimeText.Visible = true;
            if (leftSeconds >= 24 * 3600)
            {
                int day = Mathf.CeilToInt((float)leftSeconds / (24 * 3600 * 1.0f));
                _remainTimeText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent((int)LangEnum.REWARD_CARD_LEFT_DAY), day);
            }
            else if (leftSeconds >= 3600)
            {
                int hour = Mathf.CeilToInt((float)leftSeconds / 3600.0f);
                _remainTimeText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent((int)LangEnum.REWARD_CARD_LEFT_HOUR), hour);
            }
            else
            {
                int minute = Mathf.CeilToInt((float)leftSeconds / 60.0f);
                _remainTimeText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent((int)LangEnum.REWARD_CARD_LEFT_MINUTE), minute);
            }
        }

        private void RefreshProgressInfo()
        {
            _remainTimeText.Visible = true;
            if (_dailyItemData.EvtAtomId == (int)EventAtomId.DemonGate)
            {
                int maxDemonGateScore = PlayerDataManager.Instance.DemonGateData.SelfRankInfo.max_score;
                _remainTimeText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(33111), maxDemonGateScore);
            }
        }

        private void RefreshDailyAccumulationInfo()
        {
            _remainTimeText.Visible = true;
            if (_dailyItemData.EvtAtomId == (int)EventAtomId.LimitTimeTotalCharge)
            {
                _remainTimeText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(33144), _dailyItemData.MaxTriggerNum.ToString());
            }
            else if (_dailyItemData.EvtAtomId == (int)EventAtomId.DayLogin)
            {
                _remainTimeText.CurrentText.text = MogoLanguageUtil.GetContent(33145);
            }
        }

        private void RefreshInfoColor()
        {
            if (_dailyItemData.Status == (int)DailyTaskStatus.Complete || _dailyItemData.TimeStatus == (int)DailyTaskTimeStatus.HaveEnded)
            {
                if (!_isInfoGray || _canRefreshInfoColor)
                {
                    _isInfoGray = true;
                    _canRefreshInfoColor = false;
                    SetWidgetsGray(0.1f);
                    SetTextsColor(_grayTextColor);
                }
            }
            else
            {
                if (_isInfoGray || _canRefreshInfoColor)
                {
                    _isInfoGray = false;
                    _canRefreshInfoColor = false;
                    SetWidgetsGray(1f);
                    _notReachTimeText.CurrentText.color = _nameColor;
                    _nameText.CurrentText.color = _nameColor;
                    _descText.CurrentText.color = _descColor;
                }
            }
        }

        private void RefreshIconSpriteColor(GameObject spriteGo)
        {
            if (_dailyItemData.Status == (int)DailyTaskStatus.Complete || _dailyItemData.TimeStatus == (int)DailyTaskTimeStatus.HaveEnded)
            {
                DailyItemGrayHandler.SetIconSpriteGray(spriteGo, 0.1f);
            }
            else
            {
                DailyItemGrayHandler.SetIconSpriteGray(spriteGo, 1.0f);
            }
        }

        private void SetWidgetsNormalColor()
        {
            SetWidgetsGray(1f);
        }

        private void SetWidgetsGray(float grayValue)
        {
            ImageWrapper wrapper = _haveGetRewardTag.CurrentImage as ImageWrapper;
            wrapper.SetGray(grayValue);
            DailyItemGrayHandler.SetItemBgGray(this, grayValue);
            SetTaskIconGray(grayValue);
            SetRewardGridGray(grayValue);
        }

        private void SetTaskIconGray(float grayValue)
        {
            DailyItemGrayHandler.SetTaskIconGray(_taskIcon, grayValue);
        }

        private void SetRewardGridGray(float grayValue)
        {
            DailyItemGrayHandler.SetRewardGridGray(_rewardGrid, grayValue);
        }

        private void SetTextsColor(Color color)
        {
            _notReachTimeText.CurrentText.color = color;
            _nameText.CurrentText.color = color;
            _descText.CurrentText.color = color;
        }

        private bool CanShowEffect()
        {
            if (_dailyItemData == null)
            {
                return false;
            }
            return (_dailyItemData.Status == (int)DailyTaskStatus.CanCommit);
        }

        public uint GetOnlineTimeMinute()
        {
            uint minute = 0;
            uint onlineTime = PlayerAvatar.Player.online_time_day;  //单位为s
            minute = (uint)Mathf.CeilToInt((float)onlineTime / 60.0f);
            return minute;
        }

        private void OnClickGoBtn()
        {
            //引导
            if (_dailyItemData.EvtAtomId == (int)EventAtomId.RightsCard)
            {
                if (function_helper.IsFunctionOpen(FunctionId.charge))
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.Charge);
                }
                else
                {
                    MessageBox.Show(true, string.Empty, MogoLanguageUtil.GetContent(114982));
                }
            }
            else
            {
                RewardTaskFollowHandler.TaskFollow(_dailyItemData.EventId);
            }
        }

        private void OnClickGetRewardBtn()
        {
            RewardManager.Instance.RequestGetDailyTaskReward(_dailyItemData.Id);
        }

        private void OnClickRewardBtn()
        {
            ToolTipsManager.Instance.ShowItemTip(_dailyItemData.RewardList[0].id, PanelIdEnum.Reward);
        }
    }
}
