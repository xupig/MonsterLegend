﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.Data;
using ACTSystem;
using UnityEngine;
using GameLoader.Utils;
using Common.ExtendComponent;
using Common.Utils;

namespace ModuleReward
{
    public class OpenServerLoginRewardItem : KList.KListItemBase
    {
        private KDummyButton _selectBtn;
        private StateImage _highlightImg;
        private KDummyButton _highlightSelectBtn;
        private StateText _loginDayText;
        private StateImage _haveGetRewardTag;
        private GameObject _modelGo;
        private KContainer _modelContainer;
        private MaskModelItem _maskModelItem;

        private ModelType _modelType = ModelType.Pet;
        private OpenServerLoginItemData _data;
        private bool _isReceived = false;

        private static readonly string TITLE_DESC = MogoLanguageUtil.GetContent(33217);

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                OpenServerLoginItemData itemData = value as OpenServerLoginItemData;
                if (_data != null && ID == _data.Id && _isReceived == _data.IsReceived)
                    return;
                _data = itemData;
                ID = _data.Id;
                _isReceived = _data.IsReceived;
                Refresh();
            }
        }

        protected override void Awake()
        {
            _selectBtn = AddChildComponent<KDummyButton>("Image_congwudi");
            _highlightImg = GetChildComponent<StateImage>("ScaleImage_xuanzhongkuang");
            _highlightSelectBtn = _highlightImg.gameObject.AddComponent<KDummyButton>();
            _highlightImg.Visible = false;
            _loginDayText = GetChildComponent<StateText>("Label_txtDenglu");
            _haveGetRewardTag = GetChildComponent<StateImage>("Image_sharedyilingqu");
            _haveGetRewardTag.Visible = false;
            _modelContainer = GetChildComponent<KContainer>("Container_model");
            RectTransform modelContainerRect = _modelContainer.gameObject.GetComponent<RectTransform>();
            modelContainerRect.anchoredPosition3D = new Vector3(modelContainerRect.anchoredPosition3D.x, modelContainerRect.anchoredPosition3D.y, -500f);
            _maskModelItem = this.gameObject.AddComponent<MaskModelItem>();
            AddEventListener();
        }

        private void AddEventListener()
        {
            _selectBtn.onClick.AddListener(OnClickSelectBtn);
            _highlightSelectBtn.onClick.AddListener(OnClickHighlightSelectBtn);
        }

        private void RemoveEventListener()
        {
            _selectBtn.onClick.RemoveListener(OnClickSelectBtn);
            _highlightSelectBtn.onClick.RemoveListener(OnClickHighlightSelectBtn);
        }

        private void OnClickSelectBtn()
        {
            ShowHighlight();
            if (_onClick != null)
            {
                _onClick.Invoke(this, this.Index);
            }
        }

        private void OnClickHighlightSelectBtn()
        {
            ShowHighlight();
            if (_onClick != null)
            {
                _onClick.Invoke(this, this.Index);
            }
        }

        public void ShowHighlight()
        {
            _highlightImg.Visible = true;
        }

        public void HideHighlight()
        {
            _highlightImg.Visible = false;
        }

        private void Refresh()
        {
            _loginDayText.CurrentText.text = string.Format(TITLE_DESC, _data.OpenServerLoginConfig.__login_day.ToString());
            _haveGetRewardTag.Visible = _data.IsReceived;
            ShowModel();
        }

        private void ShowModel()
        {
            if (_modelGo == null)
            {
                LoadModel(_data.OpenServerLoginConfig.__model_id);
            }
            else
            {
                if (!_modelGo.activeSelf)
                {
                    _modelGo.SetActive(true);
                }
            }
        }

        private string GetModelPath(int modelId)
        {
            string modelPath = string.Empty;
            if (modelId == 0)
            {
                LoggerHelper.Error("OpenServerLoginRewardItem GetModelPath model id invalid ! model id = " + modelId);
                return modelPath;
            }
            ACTDataActor actorData = null;
            if (modelId != 2033)  //特殊处理，由于要加载翅膀模型，有个怪物的id也为2033
            {
                actorData = ACTRunTimeData.GetActorData(modelId);
            }
            if (actorData != null)
            {
                modelPath = actorData.GetModelAssetPath();
                _modelType = ModelType.Pet;
            }
            else
            {
                var equipData = ACTRunTimeData.GetEquipmentData(modelId);
                if (equipData != null)
                {
                    modelPath = equipData.GetModelAssetPath();
                    _modelType = ModelType.Wing;
                }
            }
            return modelPath;
        }

        private void LoadModel(int modelId)
        {
            Game.Asset.ObjectPool.Instance.GetGameObject(GetModelPath(modelId), OnModelLoaded);
        }

        private void OnModelLoaded(GameObject modelGo)
        {
            if (modelGo == null)
                return;
            if (_modelGo != null)
            {
                DestroyImmediate(_modelGo);
                _modelGo = null;
            }
            _modelGo = modelGo;
            _modelGo.transform.SetParent(_modelContainer.transform);
            if (_modelType == ModelType.Pet)
            {
                _modelGo.transform.localScale = new Vector3(200, 200, 200);
                _modelGo.transform.localPosition = new Vector3(110f, -275f, 0f);
                _modelGo.transform.localEulerAngles = new Vector3(0f, 180f, 0f);
            }
            else if (_modelType == ModelType.Wing)
            {
                if (_data.OpenServerLoginConfig.__model_id == 2043)
                {
                    _modelGo.transform.localScale = new Vector3(90, 90, 90);
                }
                else
                {
                    _modelGo.transform.localScale = new Vector3(70, 70, 70);
                }
                _modelGo.transform.localPosition = new Vector3(108f, -160f, 0f);
                _modelGo.transform.localEulerAngles = new Vector3(_modelGo.transform.localEulerAngles.x, 90f, 0f);
            }
            _modelGo.SetActive(true);
            SetModelLayerToUI();
            _maskModelItem.Init(_modelContainer.gameObject, _modelGo, true);
        }

        private void SetModelLayerToUI()
        {
            int layer = UnityEngine.LayerMask.NameToLayer("UI");
            _modelGo.layer = layer;
            for (int i = 0; i < _modelGo.transform.childCount; i++)
            {
                Transform child = _modelGo.transform.GetChild(i);
                child.gameObject.layer = layer;
            }
        }

        public override void Show()
        {
            if (_modelGo != null && !_modelGo.activeSelf)
            {
                _modelGo.SetActive(true);
            }
        }

        public override void Hide()
        {
            if (_modelGo != null && _modelGo.activeSelf)
            {
                _modelGo.SetActive(false);
            }
        }

        public override void Dispose()
        {
            RemoveEventListener();
        }

        public enum ModelType
        {
            Pet = 0,
            Wing
        }
    }
}
