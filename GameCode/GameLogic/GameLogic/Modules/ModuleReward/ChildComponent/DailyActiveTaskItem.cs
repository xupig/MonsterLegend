﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.Data;
using Common.Utils;
using GameMain.GlobalManager.SubSystem;
using GameData;
using Common.ExtendComponent;
using Game.UI;
using UnityEngine;
using ModuleCommonUI;
using Common.Base;
using Common.Global;

namespace ModuleReward
{
    public class DailyActiveTaskItem : KList.KListItemBase
    {
        private DailyActiveItemData _itemData;
        private KButton _goBtn;
        private KButton _receiveRewardBtn;
        private StateImage _haveReceiveTag;
        private StateText _nameText;
        private StateText _rewardTitleText;
        private StateText _progressText;
        private KContainer _taskIcon;
        private List<ItemGrid> _rewardGridList = new List<ItemGrid>();
        private List<StateText> _rewardNumList = new List<StateText>();
        private StateImage _activeIcon;
        private Color _nameColor = Color.white;
        private Color _progressColor = Color.white;
        private Color _rewardNumColor = Color.white;
        private readonly Color _grayTextColor = new Color(188f / 255f, 188f / 255f, 188f / 255f, 1.0f);

        private KParticle _getRewardEffect;

        private int _iconId = 0;
        private List<int> _rewardIconIdList = new List<int>();

        private const int GO_BTN = 0;
        private const int RECEIVE_REWARD_BTN = 1;
        private const int HAVE_RECEIVE_TAG = 2;

        private int _id = 0;
        private bool _isInfoGray = false;
        private bool _canRefreshInfoColor = false;
        private string _activePointRewardTemplate;

        public override object Data
        {
            get
            {
                return _itemData;
            }
            set
            {
                _itemData = value as DailyActiveItemData;
                if (_id != _itemData.Id)
                {
                    _canRefreshInfoColor = true;
                    _id = _itemData.Id;
                }
                Refresh();
            }
        }

        protected override void Awake()
        {
            _goBtn = GetChildComponent<KButton>("Button_qianwang");
            _receiveRewardBtn = GetChildComponent<KButton>("Button_lingqu");
            _haveReceiveTag = GetChildComponent<StateImage>("Image_sharedyilingqu");
            _nameText = GetChildComponent<StateText>("Label_txtJianglixiangmu");
            _progressText = GetChildComponent<StateText>("Label_txtJindu");
            _rewardTitleText = GetChildComponent<StateText>("Label_txtJiangli");
            _taskIcon = GetChildComponent<KContainer>("Container_taskIcon/Container_icon");
            _activeIcon = GetChildComponent<StateImage>("Container_reward1/Container_wupin/Container_icon/Image_huoyuedu");
            _activeIcon.Visible = false;
            InitRewardWidgets();
            _rewardNumColor = _rewardNumList[0].CurrentText.color;
            _getRewardEffect = _receiveRewardBtn.GetChild("fx_ui_3_2_lingqu").AddComponent<KParticle>();
            MaskEffectItem maskEffectItem = gameObject.AddComponent<MaskEffectItem>();
            maskEffectItem.Init(_receiveRewardBtn.gameObject, CanShowEffect);
            _activePointRewardTemplate = _rewardNumList[0].CurrentText.text;
            AddEventListener();
        }

        private void InitRewardWidgets()
        {
            KContainer rewardIcon = null;
            ItemGrid rewardGrid = null;
            StateText rewardNum = null;
            for (int i = 0; i < 3; i++)
            {
                rewardIcon = GetChildComponent<KContainer>("Container_reward" + (i + 1));
                rewardGrid = rewardIcon.GetChildComponent<ItemGrid>("Container_wupin");
                _rewardGridList.Add(rewardGrid);
                rewardNum = GetChildComponent<StateText>("Label_txtRewardCount" + (i + 1));
                _rewardNumList.Add(rewardNum);
            }
        }

        private void AddEventListener()
        {
            _goBtn.onClick.AddListener(OnClickGoBtn);
            _receiveRewardBtn.onClick.AddListener(OnClickReceiveRewardBtn);
        }

        private void RemoveEventListener()
        {
            _goBtn.onClick.RemoveListener(OnClickGoBtn);
            _receiveRewardBtn.onClick.RemoveListener(OnClickReceiveRewardBtn);
        }

        public override void Dispose()
        {
            RemoveEventListener();
        }

        private void OnClickGoBtn()
        {
            RewardTaskFollowHandler.TaskFollow(_itemData.EventId);
        }

        private void OnClickReceiveRewardBtn()
        {
            RewardManager.Instance.RequestGetDailyActiveReward(_itemData.Id);
        }

        private void Refresh()
        {
            RefreshStatusWidgets();
            RefreshBaseInfo();
            RefreshRewardInfo();
            RefreshInfoColor();
        }

        private void RefreshStatusWidgets()
        {
            if (_itemData.Status == (int)DailyActiveStatus.Accept)
            {
                if (_itemData.IsFollowEvent)
                {
                    EnableStatusWidget(GO_BTN);
                }
                else
                {
                    HideStatusWidgets();
                }
            }
            else if (_itemData.Status == (int)DailyActiveStatus.CanCommit)
            {
                EnableStatusWidget(RECEIVE_REWARD_BTN);
                ShowGetRewardEffect();
            }
            else if (_itemData.Status == (int)DailyActiveStatus.Complete)
            {
                EnableStatusWidget(HAVE_RECEIVE_TAG);
            }
        }

        private void EnableStatusWidget(int widgetIndex)
        {
            if (widgetIndex == GO_BTN)
            {
                _goBtn.Visible = true;
                _receiveRewardBtn.Visible = false;
                _haveReceiveTag.Visible = false;
            }
            else if (widgetIndex == RECEIVE_REWARD_BTN)
            {
                _goBtn.Visible = false;
                _receiveRewardBtn.Visible = true;
                _haveReceiveTag.Visible = false;
            }
            else if (widgetIndex == HAVE_RECEIVE_TAG)
            {
                _goBtn.Visible = false;
                _receiveRewardBtn.Visible = false;
                _haveReceiveTag.Visible = true;
            }
        }

        private void HideStatusWidgets()
        {
            _goBtn.Visible = false;
            _receiveRewardBtn.Visible = false;
            _haveReceiveTag.Visible = false;
        }

        private void RefreshBaseInfo()
        {
            _nameText.CurrentText.text = _itemData.Name;
            _progressText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent((int)LangEnum.REWARD_PROGRESS), _itemData.CurTriggerNum, _itemData.MaxTriggerNum);
            RefreshTaskIcon();
        }

        private void RefreshTaskIcon()
        {
            if (_iconId != _itemData.IconId)
            {
                _iconId = _itemData.IconId;
                MogoAtlasUtils.AddIcon(_taskIcon.gameObject, _iconId, OnTaskIconLoaded);
            }
        }

        private void RefreshRewardInfo()
        {
            List<RewardData> rewardList = _itemData.RewardList;
            if (rewardList == null || rewardList.Count == 0)
                return;

            int index = 0;
            _rewardNumList[index].CurrentText.text = string.Format(_activePointRewardTemplate, _itemData.ActivePoint.ToString());
            for (int i = 1; i < _rewardGridList.Count; i++)
            {
                if (index < rewardList.Count)
                {
                    _rewardGridList[i].Visible = true;
                    _rewardNumList[i].Visible = true;
                    _rewardGridList[i].SetItemData(rewardList[index].id, OnRewardSpritesLoaded);
                    _rewardNumList[i].CurrentText.text = string.Format("x {0}", rewardList[index].num);
                }
                else
                {
                    _rewardGridList[i].Visible = false;
                    _rewardNumList[i].Visible = false;
                }
                index++;
            }
        }

        private void ShowGetRewardEffect()
        {
            if (_getRewardEffect.Visible == false)
            {
                _getRewardEffect.Visible = true;
            }
            _getRewardEffect.transform.localPosition = new Vector3(0, 0.54f, -2);
            _getRewardEffect.transform.localScale = new Vector3(1f, 1f, 1f);
        }

        private void HideGetRewardEffect()
        {
            if (_getRewardEffect != null && _getRewardEffect.Visible == true)
            {
                _getRewardEffect.Visible = false;
            }
        }

        private void OnTaskIconLoaded(GameObject go)
        {
            RefreshIconSpriteColor(go);
        }

        private void OnRewardSpritesLoaded(GameObject go)
        {
            RefreshIconSpriteColor(go);
        }

        private void RefreshInfoColor()
        {
            if (_itemData.Status == (int)DailyActiveStatus.Complete)
            {
                if (!_isInfoGray || _canRefreshInfoColor)
                {
                    _isInfoGray = true;
                    _canRefreshInfoColor = false;
                    SetWidgetsGray(0.1f);
                    SetTextsColor(_grayTextColor, _grayTextColor, _grayTextColor);
                }
            }
            else
            {
                if (_isInfoGray || _canRefreshInfoColor)
                {
                    _isInfoGray = false;
                    _canRefreshInfoColor = false;
                    SetWidgetsGray(1f);
                    SetTextsColor(_nameColor, _progressColor, _rewardNumColor);
                }
            }
        }

        private void RefreshIconSpriteColor(GameObject spriteGo)
        {
            if (_itemData.Status == (int)DailyActiveStatus.Complete)
            {
                DailyItemGrayHandler.SetIconSpriteGray(spriteGo, 0.1f);
            }
            else
            {
                DailyItemGrayHandler.SetIconSpriteGray(spriteGo, 1.0f);
            }
        }

        private void SetWidgetsGray(float grayValue)
        {
            ImageWrapper wrapper = _haveReceiveTag.CurrentImage as ImageWrapper;
            wrapper.SetGray(grayValue);
            DailyItemGrayHandler.SetItemBgGray(this, grayValue);
            SetTaskIconGray(grayValue);
            SetRewardGridGray(grayValue);
        }

        private void SetTaskIconGray(float grayValue)
        {
            DailyItemGrayHandler.SetTaskIconGray(_taskIcon, grayValue);
        }

        private void SetRewardGridGray(float grayValue)
        {
            for (int i = 0; i < _rewardGridList.Count; i++)
            {
                if (_rewardGridList[i].Visible)
                {
                    DailyItemGrayHandler.SetRewardGridGray(_rewardGridList[i], grayValue);
                }
            }
        }

        private void SetTextsColor(Color nameColor, Color progressColor, Color rewardNumColor)
        {
            _nameText.CurrentText.color = nameColor;
            _progressText.CurrentText.color = progressColor;
            _rewardTitleText.CurrentText.color = progressColor;
            for (int i = 0; i < _rewardNumList.Count; i++)
            {
                if (_rewardNumList[i].Visible)
                {
                    _rewardNumList[i].CurrentText.color = rewardNumColor;
                }
            }
        }

        private bool CanShowEffect()
        {
            if (_itemData == null)
            {
                return false;
            }
            return (_itemData.Status == (int)DailyActiveStatus.CanCommit);
        }
        
    }
}
