﻿using Common.Base;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleReward
{
    public class RewardCompensationPanel:BasePanel
    {
        private KList _rewardList;

        protected override void Awake()
        {
            base.Awake();
            _rewardList = GetChildComponent<KList>("Container_yingfujiangli/ScrollView_yingfujiangli/mask/content");
            _rewardList.SetGap(18, 35);
            _rewardList.SetDirection(KList.KListDirection.LeftToRight, 3);
        }
        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.RewardCompensation; }
        }

        public override void OnShow(object data)
        {
            _rewardList.SetDataList<RewardCompensationItem>(avatar_level_helper.GetUpLevelCompensationReward(PlayerAvatar.Player.level));
        }

        public override void OnClose()
        {
            ;
        }
    }
}
