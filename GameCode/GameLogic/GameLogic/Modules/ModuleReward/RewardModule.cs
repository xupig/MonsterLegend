﻿using System;
using System.Collections.Generic;
using Common.Base;

namespace ModuleReward
{
    public class RewardModule : BaseModule
    {
        public override ModuleType ModuleType
        { 
            get { return ModuleType.Permanent; } 
        }

        public override void Init()
        {
            RegisterPanelModule(PanelIdEnum.Reward, PanelIdEnum.Reward);
            RegisterPanelModule(PanelIdEnum.OpenServerLoginReward, PanelIdEnum.Reward);
            RegisterPopPanelBunchPanel(PanelIdEnum.OpenServerLoginReward, PanelIdEnum.Reward);
            RegisterPanelModule(PanelIdEnum.RewardCompensation, PanelIdEnum.Reward);
            RegisterPopPanelBunchPanel(PanelIdEnum.RewardCompensation, PanelIdEnum.Reward);
            AddPanel(PanelIdEnum.Reward, "Container_TheRewardSystemPanel", MogoUILayer.LayerUIPanel, "ModuleReward.RewardPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.OpenServerLoginReward, "Container_OpenServerLoginRewardPanel", MogoUILayer.LayerUIPopPanel, "ModuleReward.OpenServerLoginRewardPanel", 
                BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.RewardCompensation, "Container_RewardCompensationPanel", MogoUILayer.LayerUIPopPanel, "ModuleReward.RewardCompensationPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }
    }
}
