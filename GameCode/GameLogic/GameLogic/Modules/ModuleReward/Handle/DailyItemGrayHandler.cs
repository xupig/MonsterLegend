﻿using System;
using System.Collections.Generic;
using Common.ExtendComponent;
using Game.UI;
using Game.UI.UIComponent;
using UnityEngine;

namespace ModuleReward
{
    public class DailyItemGrayHandler
    {
        public static void SetTaskIconGray(KContainer taskIcon, float grayValue)
        {
            RectTransform rectTrans = taskIcon.GetChildComponent<RectTransform>("bg_holder");
            ImageWrapper[] wrappers = null;
            if (rectTrans != null)
            {
                wrappers = rectTrans.GetComponentsInChildren<ImageWrapper>();
                if (wrappers.Length > 0)
                {
                    wrappers[0].SetGray(grayValue);
                }
            }
            rectTrans = taskIcon.GetChildComponent<RectTransform>("holder");
            if (rectTrans != null)
            {
                wrappers = rectTrans.GetComponentsInChildren<ImageWrapper>();
                if (wrappers.Length > 0)
                {
                    wrappers[0].SetGray(grayValue);
                }
            }
        }

        public static void SetRewardGridGray(ItemGrid rewardGrid, float grayValue)
        {
            RectTransform rectTrans = rewardGrid.GetChildComponent<RectTransform>("Container_yanse/holder");
            ImageWrapper[] wrappers = null;
            if (rectTrans != null)
            {
                wrappers = rectTrans.GetComponentsInChildren<ImageWrapper>();
                if (wrappers.Length > 0)
                {
                    wrappers[0].SetGray(grayValue);
                }
            }
            rectTrans = rewardGrid.GetChildComponent<RectTransform>("Container_icon/holder");
            if (rectTrans != null)
            {
                wrappers = rectTrans.GetComponentsInChildren<ImageWrapper>();
                if (wrappers.Length > 0)
                {
                    wrappers[0].SetGray(grayValue);
                }
            }
        }

        public static void SetItemBgGray(KContainer item, float grayValue)
        {
            StateImage bgImg = item.GetChildComponent<StateImage>("Container_Bg/Image_tiaokuang");
            ImageWrapper wrapper = bgImg.CurrentImage as ImageWrapper;
            wrapper.SetGray(grayValue);
        }

        public static void SetIconSpriteGray(GameObject spriteGo, float grayValue)
        {
            ImageWrapper iconWrapper = spriteGo.GetComponent<ImageWrapper>();
            if (iconWrapper == null)
                return;
            iconWrapper.SetGray(grayValue);
        }
    }
}
