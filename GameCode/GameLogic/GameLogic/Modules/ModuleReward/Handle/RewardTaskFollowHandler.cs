﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using GameData;
using GameMain.GlobalManager;

namespace ModuleReward
{
    public class RewardTaskFollowHandler
    {
        public const string OPEN_MAP = "2";
        public const string OPEN_VIEW = "1";

        public static void TaskFollow(int eventId)
        {
            if (eventId == 0)
                return;
            Dictionary<string, string[]> evtFollow = event_cnds_helper.GetEventFollow(eventId);
            if (evtFollow != null)
            {
                foreach (var item in evtFollow)
                {
                    string strFollowType = item.Key;
                    if (strFollowType == OPEN_VIEW)
                    {
                        int viewId = int.Parse(item.Value[0]);
                        view_helper.OpenView(viewId);
                    }
                    else if (strFollowType == OPEN_MAP)
                    {
                        PanelIdEnum.Copy.Show(ChapterType.Copy);
                    }
                    break;
                }
            }
        }

        private static void ShowPanel(int panelId, int tab)
        {
            if (tab == 0)
            {
                ((PanelIdEnum)panelId).Show();
            }
            else
            {
                 ((PanelIdEnum)panelId).Show(tab);
            }
        }
    }
}
