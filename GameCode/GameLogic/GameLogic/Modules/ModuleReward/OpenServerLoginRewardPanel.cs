﻿using System;
using System.Collections.Generic;
using Common.Base;

namespace ModuleReward
{
    public class OpenServerLoginRewardPanel : BasePanel
    {
        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.OpenServerLoginReward; }
        }

        private OpenServerLoginRewardView _loginRewardView;

        protected override void Awake()
        {
            _loginRewardView = AddChildComponent<OpenServerLoginRewardView>("Container_kaifudenglujiangli");
        }

        public override void OnShow(object data)
        {
            _loginRewardView.Refresh();
        }

        public override void OnClose()
        {
            
        }
    }
}
