﻿using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleFriend
{
    public class RecommendFriendView:KContainer
    {
        private KList _recommendList;
        protected override void Awake()
        {
            base.Awake();

            _recommendList = GetChildComponent<KList>("ScrollView_haoyou/mask/content");
            _recommendList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();

        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        public void OnShow()
        {
            Visible = true;
            if (PlayerDataManager.Instance.FriendData.RecommendFriendList.Count == 0)
            {
                FriendManager.Instance.RequestRecomendFriend();
            }
            RecomendRefresh();
        }

        public void OnClose()
        {
            Visible = false;
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(FriendEvents.FRIEND_RECOMMEND_CHANGE, RecomendRefresh);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(FriendEvents.FRIEND_RECOMMEND_CHANGE, RecomendRefresh);
        }

        private void RecomendRefresh()
        {
            List<PbAvatarData> recommendList = PlayerDataManager.Instance.FriendData.RecommendFriendList;
            _recommendList.SetDataList<FriendRecommendItem>(recommendList);
        }

    }
}
