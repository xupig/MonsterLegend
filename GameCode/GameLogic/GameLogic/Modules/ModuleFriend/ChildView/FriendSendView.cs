﻿#region 模块信息
/*==========================================
// 文件名：FriendSendView
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/27 20:57:23
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;

namespace ModuleFriend
{
    public class FriendSendView:KContainer
    {

        private const int DIRECT = 0;
        private const int DIAMOND = 1;

        private KToggleGroup _toggleGroup;
        private KButton _btnSend;
        private KButton _btnSend2;
        private StateText _txtCost;
        private PageableScrollView _scrollView;

        private IntimateItemInfo _itemInfo;

        private List<IntimateItemInfo> _totalList;
        private List<IntimateItemInfo> _directList;
        private HashSet<int> _directSet;
       // private List<IntimateItemInfo> _diamondList;

        public UInt64 sendPlayer;
        public string sendPlayerName = string.Empty;

        private StateText _txtEmpty;
        private StateText _txtSendTime;
       // private StateText _txtTips;

        private StateIcon _stateIcon;

        protected override void Awake()
        {
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_toggleGroup");
            _btnSend = GetChildComponent<KButton>("Button_zengsonganjian");
            _txtCost = _btnSend.GetChildComponent<StateText>("label02");
            _btnSend2 = GetChildComponent<KButton>("Button_zengsonganjian2");
            _scrollView = AddChildComponent<PageableScrollView>("ScrollView_baoshizengsong");
            _scrollView.SetDirection(3, int.MaxValue);
            _scrollView.SetGap(11,8);
            _scrollView.IsPageable = false;
            _scrollView.SetCapacity(15);
           
            _directList = new List<IntimateItemInfo>();
            _directSet = new HashSet<int>();

            _stateIcon = _btnSend.GetChildComponent<StateIcon>("stateIcon");

            _btnSend.gameObject.SetActive(false);

            _txtEmpty = GetChildComponent<StateText>("Label_txtMeiyoudaoju");
            _txtSendTime = GetChildComponent<StateText>("Label_txtZengsongcishu");
            //_txtTips = GetChildComponent<StateText>("Label_txtTishi");

            _txtEmpty.Visible = false;
            _txtSendTime.Visible = false;
            //_txtTips.Visible = false;

            InitTotalData();
            base.Awake();
            ClearBtns();
        }

        private void ClearBtns()
        {
            _btnSend.gameObject.SetActive(false);
            _btnSend2.gameObject.SetActive(false);
        }

        protected override void OnEnable()
        {
            SetData();
            AddEventListener();
            base.OnEnable();
            _toggleGroup.SelectIndex = DIRECT;
            OnToggleGroupChanged(_toggleGroup, DIRECT);
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        private void InitTotalData()
        {
            _totalList = new List<IntimateItemInfo>();
            foreach(KeyValuePair<int,intimate_item> kvp in XMLManager.intimate_item)
            {
                if(kvp.Value.__price !=null && kvp.Value.__price.Count> 0)
                {
                    IntimateItemInfo itemInfo = new IntimateItemInfo();
                    itemInfo.IsDirect = false;
                    itemInfo.item = kvp.Value;
                    _totalList.Add(itemInfo);
                }
            }
        }

        private void SetData()
        {
            OnVipLevelChange();
            UpdateDirectList();
            //UpdateDiamondList();
        }

        public void UpdateDirectList()
        {
            BagData bagData = PlayerDataManager.Instance.BagData.GetItemBagData();
            Dictionary<int, BaseItemInfo> dataDict = bagData.GetAllItemInfo();
            _directList.Clear();
            _directSet.Clear();
            foreach (KeyValuePair<int, BaseItemInfo> kvp in dataDict)
            {
                if (kvp.Value.Type == BagItemType.Intimate)
                {
                    IntimateItemInfo itemInfo = new IntimateItemInfo();
                    itemInfo.IsDirect = true;
                    itemInfo.item = intimate_helper.GetIntimateItem(kvp.Value.Id);
                    _directList.Add(itemInfo);
                    if (_directSet.Contains(kvp.Value.Id) == false)
                    {
                        _directSet.Add(kvp.Value.Id);
                    }
                }
            }
        }

        //private void UpdateDiamondList()
        //{
        //    _diamondList.Clear();
        //    for (int i = 0; i < _totalList.Count; i++)
        //    {
        //        if (_directSet.Contains(_totalList[i].item.__id) == false)
        //        {
        //            _diamondList.Add(_totalList[i]);
        //        }
        //    }
        //}

        private void AddEventListener()
        {
            _scrollView.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            _toggleGroup.onSelectedIndexChanged.AddListener(OnToggleGroupChanged);
            _btnSend.onClick.AddListener(OnClickSend);
            _btnSend2.onClick.AddListener(OnClickSend);
            EventDispatcher.AddEventListener<int>(FriendEvents.FRIEND_SEND_LIST_CHANGE, OnItemChange);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.vip_level, OnVipLevelChange);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.friend_give_count, OnVipLevelChange);
        }

        private void RemoveEventListener()
        {
            _scrollView.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnToggleGroupChanged);
            _btnSend.onClick.RemoveListener(OnClickSend);
            _btnSend2.onClick.RemoveListener(OnClickSend);
            EventDispatcher.RemoveEventListener<int>(FriendEvents.FRIEND_SEND_LIST_CHANGE, OnItemChange);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.vip_level, OnVipLevelChange);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.friend_give_count, OnVipLevelChange);
        }

        private void OnVipLevelChange()
        {
            int limit = int.Parse(vip_rights_helper.GetVipRights(9, PlayerAvatar.Player.vip_level));
            _txtSendTime.CurrentText.text = (57106).ToLanguage(limit-PlayerAvatar.Player.friend_give_count, limit);
            //UpdateDirectList();
        }

        private void OnItemChange(int itemId)
        {
            
           // UpdateDiamondList();
            if(_toggleGroup.SelectIndex == DIRECT)
            {
                UpdateDirectList();
                _scrollView.ClearSelected();
                _scrollView.SetDataList<FriendSendItem>(_directList, 1, 1, false);
                if(_itemInfo!=null)
                {
                    List<KList.KListItemBase> list = _scrollView.List.ItemList;
                    for (int i = 0; i < list.Count; i++)
                    {
                        FriendSendItem item = list[i] as FriendSendItem;
                        if (item.Data != null && (item.Data as IntimateItemInfo).item.__id == _itemInfo.item.__id)
                        {
                            _scrollView.List.SelectedIndex = item.Index;
                        }
                    }
                }
            }
        }

        private void OnClickSend()
        {
            if(sendPlayer == 0)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.FRIEND_SELECT_SEND_PLAYER), PanelIdEnum.MainUIField);
                //MessageBox.Show(true, string.Empty, MogoLanguageUtil.GetString(LangEnum.FRIEND_SELECT_SEND_PLAYER));
                return;
            }
            if (_itemInfo == null)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.FRIEND_SELECT_SEND_ITEM), PanelIdEnum.MainUIField);
                //MessageBox.Show(true, string.Empty, MogoLanguageUtil.GetString(LangEnum.FRIEND_SELECT_SEND_ITEM));
                return;
            }

            PbFriendAvatarInfo friendInfo = PlayerDataManager.Instance.FriendData.GetFriendAvatarInfoByDbid(sendPlayer);
            if (intimate_helper.IsReachUpLimit((int)friendInfo.degree) == true)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.FRIEND_INTIMATE_LIMIT), PanelIdEnum.MainUIField);
                return;
            }

            Dictionary<int,int> sendItemDict = PlayerDataManager.Instance.FriendData.SendItemDict;
            if (sendItemDict.ContainsKey(_itemInfo.item.__id))
            {
                if(_btnSend2.Visible)
                {
                    int limit = int.Parse(vip_rights_helper.GetVipRights(9,PlayerAvatar.Player.vip_level));
                    if (sendItemDict[_itemInfo.item.__id] >= limit)
                    {
                        ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.FRIEND_SEND_LIMIT), PanelIdEnum.MainUIField);
                        //MessageBox.Show(true, string.Empty, MogoLanguageUtil.GetString(LangEnum.FRIEND_SEND_LIMIT));
                        return;
                    }
                }
                else
                {
                    if(sendItemDict[_itemInfo.item.__id] >= _itemInfo.item.__limit)
                    {
                        ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.FRIEND_SEND_LIMIT), PanelIdEnum.MainUIField);
                        //MessageBox.Show(true, string.Empty, MogoLanguageUtil.GetString(LangEnum.FRIEND_SEND_LIMIT));
                        return;
                    }
                }
            }
           
            if(_itemInfo.IsDirect)
            {
                FriendManager.Instance.GiveGoods(sendPlayer,_itemInfo.item.__id,1);
            }
            else
            {
                int costLimit = PlayerAvatar.Player.CheckCostLimit(_itemInfo.item.__price,false);
                string itemName = item_helper.GetName(_itemInfo.item.__id);
                string content = string.Empty;
                if (costLimit != 0)
                {
                    if(costLimit == -1)
                    {
                        content = MogoLanguageUtil.GetString(LangEnum.FRIEND_SEND_COST_ALL_DIAMOND, _itemInfo.item.__price[public_config.MONEY_TYPE_BIND_DIAMOND.ToString()], itemName, sendPlayerName);//是否花费{0}钻石赠送{1}给{2}?
                    }
                    else
                    {
                        ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.FRIEND_SEND_DIAMOND), PanelIdEnum.MainUIField);
                        //MessageBox.Show(true, string.Empty, MogoLanguageUtil.GetString(LangEnum.FRIEND_SEND_DIAMOND));
                        return;
                    }
                   
                }
                else
                {
                    content = MogoLanguageUtil.GetString(LangEnum.FRIEND_SEND_COST_BIND_DIAMOND, _itemInfo.item.__price[public_config.MONEY_TYPE_BIND_DIAMOND.ToString()], itemName, sendPlayerName);//是否花费{0}钻石赠送{1}给{2}?
                }
                //content = MogoLanguageUtil.GetString(LangEnum.FRIEND_SEND_COST_DIAMOND, _itemInfo.item.__price, itemName, sendPlayerName);//是否花费{0}钻石赠送{1}给{2}?
                MessageBox.Show(true, string.Empty, content, SendConfirm);
            }
        }

        private void SendConfirm()
        {
            if(_itemInfo.IsDirect)
            {
                FriendManager.Instance.GiveGoods(sendPlayer, _itemInfo.item.__id, 1);
            }
            else
            {
                FriendManager.Instance.BuyAndGiveGoods(sendPlayer, _itemInfo.item.__id, 1);
            }
        }

        private void OnToggleGroupChanged(KToggleGroup toggleGroup,int index)
        {
           switch(index)
           {
               case DIRECT:
                   _scrollView.SetDataList<FriendSendItem>(_directList, 1, 1, true);
                   _txtEmpty.Visible = _directList.Count == 0;
                   _txtSendTime.Visible = true;
                   // 只有VIP等级大于0的玩家才会显示提示信息
                   //_txtTips.Visible = (PlayerAvatar.Player.vip_level > 0);
                   break;
               case DIAMOND:
                   _scrollView.SetDataList<FriendSendItem>(_totalList, 1, 1, true);
                   _txtEmpty.Visible = _totalList.Count == 0;
                    _txtSendTime.Visible = false;
                   //_txtTips.Visible = false;
                   break;
               default:
                   break;
           }
           
           _itemInfo = null;
           ClearBtns();
        }

        private void OnSelectedIndexChanged(PageableScrollView scrollView,int index)
        {
            if(_toggleGroup.SelectIndex == DIRECT)
            {
                _itemInfo = _directList[index];
                _btnSend2.gameObject.SetActive(true);
                
            }
            else
            {
                _itemInfo = _totalList[index];
                _btnSend.gameObject.SetActive(true);

                foreach (KeyValuePair<string, string> kvp in _itemInfo.item.__price)
                {
                    _stateIcon.SetIcon(item_helper.GetIcon(int.Parse(kvp.Key)));
                    _txtCost.ChangeAllStateText(kvp.Value);
                }
               
            }
            
        }

    }
}
