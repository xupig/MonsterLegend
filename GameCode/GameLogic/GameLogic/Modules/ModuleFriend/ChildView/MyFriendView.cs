﻿#region 模块信息
/*==========================================
// 文件名：MyFriendView
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/11 21:03:24
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleFriend
{
    public class MyFriendView:KContainer
    {
        private KScrollView _scrollView;
        private KPageableList _myFriendList;
        private StateText _txtBlessing;
        private StateText _txtBlessed;
        private KButton _btnGetReward;
        private StateText _txtEmpty;

        protected override void Awake()
        {
            _scrollView = GetChildComponent<KScrollView>("ScrollView_haoyou");
            _myFriendList = _scrollView.GetChildComponent<KPageableList>("mask/content");
            _txtBlessing = GetChildComponent<StateText>("Container_btnContainer01/Label_txtNR");
            _txtBlessed = GetChildComponent<StateText>("Container_btnContainer01/Label_txtNR2");
            
            _btnGetReward = GetChildComponent<KButton>("Container_btnContainer01/Button_chongzhi");
            _myFriendList.SetPadding(5, -4, 5, -4);
            _myFriendList.SetGap(-8, 0);
            _myFriendList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _txtEmpty = GetChildComponent<StateText>("Label_txtEmpty");
            int emptyContent = 57030;
            _txtEmpty.CurrentText.text = emptyContent.ToLanguage();
            base.Awake();
            
        }

        
        public void  OnShow()
        {
            Visible = true;
            AddEventListener();
            if (PlayerDataManager.Instance.FriendData.hasRequestData == false)
            {
                FriendManager.Instance.SendGetFriendListMsg();
                FriendManager.Instance.GetBlessedList();
                FriendManager.Instance.GetBlessingList();
                FriendManager.Instance.GetReceivedList();
            }
            else
            {
                OnFriendListChange();
            }
        }

       

        public void OnClose()
        {
            if (Visible)
            {
                RemoveEventListner();
            }
            Visible = false;
        }

        private void AddEventListener()
        {
            _btnGetReward.onClick.AddListener(OnClickGetReward);
            EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_LIST, OnFriendListChange);
            EventDispatcher.AddEventListener<UInt64>(FriendEvents.FRIEND_BLESS_STATE_CHANGE, OnBlessStateChange);
            EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_RECEIVED_LIST, OnRefreshReceiveNum);
            EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_SEND_LIST, OnRefreshBlessingNum);
        }


        private void RemoveEventListner()
        {
            _btnGetReward.onClick.RemoveListener(OnClickGetReward);
            EventDispatcher.RemoveEventListener(FriendEvents.REFRESH_FRIEND_LIST, OnFriendListChange);
            EventDispatcher.RemoveEventListener<UInt64>(FriendEvents.FRIEND_BLESS_STATE_CHANGE, OnBlessStateChange);
            EventDispatcher.RemoveEventListener(FriendEvents.REFRESH_FRIEND_RECEIVED_LIST, OnRefreshReceiveNum);
            EventDispatcher.RemoveEventListener(FriendEvents.REFRESH_FRIEND_SEND_LIST, OnRefreshBlessingNum);
        }

        private void OnBlessStateChange(UInt64 dbId)
        {
            OnRefreshBlessingNum();
            OnRefreshReceiveNum();
        }

        private void OnRefreshBlessingNum()
        {
            int blessingLimit = GlobalParams.GetFriendBlessingLimit();
            _txtBlessing.CurrentText.text = (blessingLimit - PlayerDataManager.Instance.FriendData.BlessingNum).ToString();
        }

        private void OnRefreshReceiveNum()
        {
            int blessedLimit = GlobalParams.GetFriendBlessedLimit();
            _txtBlessed.CurrentText.text = (blessedLimit - PlayerDataManager.Instance.FriendData.ReceivedNum).ToString();
        }
        

        private void OnClickGetReward()
        {
            if (PlayerDataManager.Instance.FriendData.BlessedNum <= 0)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.FRIEND_NOTHING_TO_RECEIVE), PanelIdEnum.Interactive);
            }
            else
            {
                FriendManager.Instance.ReceiveAllBless();
            }
        }

        private void OnFriendListChange()
        {
            OnBlessStateChange(0);
            FriendData friendData = PlayerDataManager.Instance.FriendData;
            _txtEmpty.Visible = friendData.FriendAvatarList.Count == 0;
            List<PbFriendAvatarInfo> friendAvatarList = friendData.GetFriendInfoByDefault();
            _myFriendList.SetDataList<FriendItem>(friendAvatarList, 4);
            _scrollView.ResetContentPosition();
            TweenListEntry.Begin(_myFriendList.gameObject);
        }

    }
}
