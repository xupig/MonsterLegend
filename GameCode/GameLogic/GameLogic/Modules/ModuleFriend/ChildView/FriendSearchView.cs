﻿#region 模块信息
/*==========================================
// 文件名：FriendSearchView
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/11 20:56:55
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.ExtendComponent.PageableComponent;
using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ModuleFriend
{
    public class FriendSearchView:KContainer
    {
        private KInputField _inputSearch;
        private KButton _btnSearch;
        private FriendSearchResultItem _resultItem;
        private string defaultInputMessage;
        protected override void Awake()
        {
            defaultInputMessage = MogoLanguageUtil.GetString(LangEnum.FRIEND_SEARCH);//输入昵称查找
            _inputSearch = GetChildComponent<KInputField>("Input_txtShuruneirong");
            Text text = _inputSearch.GetComponentInChildren<Text>();
            text.lineSpacing = 1;
            _inputSearch.text = defaultInputMessage;
            _btnSearch = GetChildComponent<KButton>("Button_sousuo");
            _resultItem = AddChildComponent<FriendSearchResultItem>("Container_itemTemplate");
            _resultItem.gameObject.SetActive(false);
            base.Awake();
        }

        private void AddEventListener()
        {
            _btnSearch.onClick.AddListener(OnClickSearch);
            _inputSearch.onPointerDown.AddListener(OnInputPointerDown);
            EventDispatcher.AddEventListener<PbFriendAvatarInfo>(FriendEvents.GET_PLAYERINFO, OnGetSearchResult);
           // _inputSearch.onEndEdit.AddListener(OnEndEdit);
           
        }

        public void OnClose()
        {
            Visible = false;
            if(_inputSearch!=null)
            {
                _inputSearch.text = defaultInputMessage;
                _resultItem.gameObject.SetActive(false);
                RemoveEventListener();
            }

        }

        public void OnShow()
        {
            Visible = true;
            AddEventListener();
        }

        protected override void OnDestroy()
        {

        }

        private void RemoveEventListener()
        {
            _btnSearch.onClick.RemoveListener(OnClickSearch);
            _inputSearch.onPointerDown.RemoveListener(OnInputPointerDown);
           // _inputSearch.onEndEdit.RemoveListener(OnEndEdit);
            EventDispatcher.RemoveEventListener<PbFriendAvatarInfo>(FriendEvents.GET_PLAYERINFO, OnGetSearchResult);
        }

        private void OnInputPointerDown(KInputField inputField,PointerEventData evtData)
        {
            if (_inputSearch.text == defaultInputMessage)
            {
                //_inputSearch.text = string.Empty;
                
            }
        }

        private void OnEndEdit(string content)
        {
            if (_inputSearch.text == string.Empty)
            {
                _inputSearch.text = defaultInputMessage;
            }
        }

        private void OnClickSearch()
        {
            if (_inputSearch.text != string.Empty && _inputSearch.text != defaultInputMessage)
            {
                if (_inputSearch.text == PlayerAvatar.Player.name)
                {
                    MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.FRIEND_CANT_SEARCH_MYSELF));
                }
                else
                {
                    FriendManager.Instance.SendSearchPlayerMsg(_inputSearch.text);
                }
            }
        }

        private void OnGetSearchResult(PbFriendAvatarInfo avatarInfo)
        {
            if(_resultItem!=null)
            {
                _resultItem.gameObject.SetActive(true);
                _resultItem.SetInfo(avatarInfo);
            }
           
        }

    }
}
