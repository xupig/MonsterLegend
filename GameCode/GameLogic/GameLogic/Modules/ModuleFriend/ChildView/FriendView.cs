﻿#region 模块信息
/*==========================================
// 文件名：FriendView
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/11 21:05:42
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleFriend
{
    public class FriendView : KContainer
    {

        private const int MY_FRIEND = 0;
        private const int APPLICATION = 1;
        private const int INTIMATE = 2;
        private const int FIND_FRIEND = 3;
        private const int BLACK_LIST = 4;

        private BlackListView _blackListView;

        private FriendSearchView _searchFriendView;
        private FriendIntimateView _intimateView;
        private FriendApplicationView _applicationView;
        private MyFriendView _myFriendView;
        private RecommendFriendView _recommendFriendView;
        private StateText _txtNum;
        private string template;

        private CategoryList _toggleGroup;
        protected override void Awake()
        {
            _searchFriendView = AddChildComponent<FriendSearchView>( "Container_zhaopengyou");
            _intimateView = AddChildComponent<FriendIntimateView>( "Container_qinmizhi");
            _applicationView = AddChildComponent<FriendApplicationView>( "Container_haoyoushenqing");
            _myFriendView = AddChildComponent<MyFriendView>( "Container_wodehaoyou");
            _recommendFriendView = AddChildComponent<RecommendFriendView>("Container_tuijianhaoyou");
            _blackListView = AddChildComponent<BlackListView>("Container_pingbi");

            _toggleGroup = GetChildComponent<CategoryList>("List_categoryList");
            _txtNum = GetChildComponent<StateText>("Label_txtNum");
            template = _txtNum.CurrentText.text;
            base.Awake();
           
        }

        private void OnFriendNumChange()
        {
            _txtNum.CurrentText.text = string.Format(template, PlayerDataManager.Instance.FriendData.FriendAvatarList.Count, GlobalParams.GetFriendLimit());
        }

        public void ShowApplication()
        {
            OnSelectedIndexChanged(_toggleGroup, APPLICATION);
            _toggleGroup.SelectedIndex = APPLICATION;
        }

        public void ShowMyFriend()
        {
            OnSelectedIndexChanged(_toggleGroup,MY_FRIEND);
            _toggleGroup.SelectedIndex = MY_FRIEND;
        }

        public void ShowSearchFriend()
        {
            OnSelectedIndexChanged(_toggleGroup, FIND_FRIEND);
            _toggleGroup.SelectedIndex = FIND_FRIEND;
        }

        public void OnShow(object data)
        {
            Visible = true;

            AddEventListener();
            CheckAddRequestData();
            OnFriendNumChange();
            if(data !=null)
            {
                OnSelectedIndexChanged(_toggleGroup, (int)data  - 1);
                _toggleGroup.SelectedIndex = (int)data - 1;
            }
            else
            {
                ShowMyFriend();
            }
            RefreshPoint();


        }

        public void OnHide()
        {
            Visible = false;
            if(_applicationView != null)
            {
                _applicationView.OnClose();
                _intimateView.OnClose();
                _searchFriendView.OnClose();
                _myFriendView.OnClose();
                _recommendFriendView.OnClose();
                _blackListView.OnHide();
                RemoveEventListener();
            }
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_LIST, OnFriendNumChange);
            EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_LIST, RefreshFriendList);
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);

            EventDispatcher.AddEventListener(FriendEvents.FRIEND_POINT_CHANGE, RefreshPoint);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(FriendEvents.REFRESH_FRIEND_LIST, OnFriendNumChange);
            EventDispatcher.RemoveEventListener(FriendEvents.REFRESH_FRIEND_LIST, RefreshFriendList);
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);

            EventDispatcher.RemoveEventListener(FriendEvents.FRIEND_POINT_CHANGE, RefreshPoint);
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

        private void CheckAddRequestData()
        {
            if (PlayerDataManager.Instance.FriendData.hasRequestData == false)
            {
                FriendManager.Instance.SendGetFriendListMsg();
                FriendManager.Instance.GetBlessedList();
                FriendManager.Instance.GetBlessingList();
                FriendManager.Instance.GetReceivedList();
            }
        }

        private void OnSelectedIndexChanged(CategoryList toggleGroup,int index)
        {
            _applicationView.OnClose();
            _intimateView.OnClose();
            _searchFriendView.OnClose();
            _myFriendView.OnClose();
            _recommendFriendView.OnClose();
            _blackListView.OnHide();
            switch(index)
            {
                case MY_FRIEND:
                    ShowFriendList();
                    break;
                case APPLICATION:
                    _applicationView.OnShow();
                    break;
                case INTIMATE:
                    _intimateView.OnShow();
                    break;
                case FIND_FRIEND:
                    _searchFriendView.OnShow();
                    break;
                case BLACK_LIST:
                    _blackListView.OnShow();
                    break;
                default:
                    break;
            }
        }

        private void RefreshFriendList()
        {
            if (_toggleGroup.SelectedIndex != MY_FRIEND)
            {
                return;
            }

            ShowFriendList();
        }

        private void ShowFriendList()
        {
            if (PlayerDataManager.Instance.FriendData.FriendAvatarList.Count == 0)
            {
                _myFriendView.OnClose();
                _recommendFriendView.OnShow();
            }
            else
            {
                _recommendFriendView.OnClose();
                _myFriendView.OnShow();
            }
        }

        private void RefreshPoint()
        {
            if (_toggleGroup!=null)
            {
                (_toggleGroup[MY_FRIEND] as CategoryListItem).SetPoint(PlayerDataManager.Instance.FriendData.BlessedNum > PlayerDataManager.Instance.FriendData.ReceivedNum);
                (_toggleGroup[APPLICATION] as CategoryListItem).SetPoint(PlayerDataManager.Instance.FriendData.FriendApplicationList.Count > 0);
            }  
        }
    }
}
