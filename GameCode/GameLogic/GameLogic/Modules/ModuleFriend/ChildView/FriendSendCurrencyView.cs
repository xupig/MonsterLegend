﻿#region 模块信息
/*==========================================
// 文件名：FriendSendCurrencyView
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/8/17 14:44:01
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ClientConfig;
using Common.ExtendComponent;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameMain.Entities;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleFriend
{
    public class FriendSendCurrencyView:KContainer
    {

        private MoneyItem _bindDiamond;
        private MoneyItem _diamond;
        protected override void Awake()
        {
            _bindDiamond = AddChildComponent<MoneyItem>("Container_buyGold");
            _bindDiamond.SetMoneyType(public_config.MONEY_TYPE_BIND_DIAMOND);
            _diamond = AddChildComponent<MoneyItem>("Container_buyDiamond");
            _diamond.SetMoneyType(public_config.MONEY_TYPE_DIAMOND);
            base.Awake();
        }


        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

    }
}
