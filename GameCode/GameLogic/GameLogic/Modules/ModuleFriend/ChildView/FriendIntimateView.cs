﻿#region 模块信息
/*==========================================
// 文件名：FriendIntimateView
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/11 20:59:32
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.ExtendComponent;
using MogoEngine.Events;
using Common.Events;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Common.Structs.ProtoBuf;
using UnityEngine;

namespace ModuleFriend
{
    public class FriendIntimateView:KContainer
    {
        private KScrollView _scrollView;
        private KPageableList _intimateEventsList;
        private StateText _txtEmpty;

        private int _currentItemCount = 10;
        private const int PER_PAGE_COUNT = 10;

        protected override void Awake()
        {
            _scrollView = GetChildComponent<KScrollView>("ScrollView_haoyou");
            _intimateEventsList = _scrollView.GetChildComponent<KPageableList>("mask/content");
            _intimateEventsList.SetPadding(5, 0, 5, 0);
            _intimateEventsList.SetGap(-8, 0);
            _intimateEventsList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);

            _txtEmpty = GetChildComponent<StateText>("Label_txtEmpty");
            int emptyContent = 57032;
            _txtEmpty.CurrentText.text = emptyContent.ToLanguage();

            base.Awake();
            FriendManager.Instance.GetIntimateEventList();
            
        }


        public void OnShow()
        {
            Visible = true;
            AddEventListener();
            OnIntimateEventListChange();
        }

        public void OnClose()
        {
            Visible = false;
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(FriendEvents.INTIMATE_EVENT_LIST_CHANGE, OnIntimateEventListChange);
            _scrollView.ScrollRect.onRequestNext.AddListener(ShowNextPage);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(FriendEvents.INTIMATE_EVENT_LIST_CHANGE, OnIntimateEventListChange);
            if (_scrollView != null)
            {
                _scrollView.ScrollRect.onRequestNext.RemoveListener(ShowNextPage);
            }
        }

        private void ShowNextPage(KScrollRect scrollRect)
        {
            _currentItemCount += PER_PAGE_COUNT;
            OnIntimateEventListChange();
        }

        private void OnIntimateEventListChange()
        {
            _txtEmpty.Visible = PlayerDataManager.Instance.FriendData.IntimateEventInfoList.Count == 0;

            List<PbIntimateEventInfo> showIntimateEventList = GetShowIntimateEventList();
            _intimateEventsList.SetDataList<FriendIntimateItem>(showIntimateEventList);
        }

        private List<PbIntimateEventInfo> GetShowIntimateEventList()
        {
            List<PbIntimateEventInfo> intimateEventList = PlayerDataManager.Instance.FriendData.IntimateEventInfoList;
            int intimateEventCount = intimateEventList.Count;
            int showIntimateEventCount = _currentItemCount > intimateEventCount ? intimateEventCount : _currentItemCount;

            return intimateEventList.GetRange(0, showIntimateEventCount);
        }

    }
}
