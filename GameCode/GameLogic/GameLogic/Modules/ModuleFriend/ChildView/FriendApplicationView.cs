﻿#region 模块信息
/*==========================================
// 文件名：FriendApplicationView
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/11 21:01:29
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleFriend
{
    public class FriendApplicationView:KContainer
    {

        private KScrollView _scrollView;
        private KPageableList _applicationList;
        private StateText _txtEmpty;

        private int _currentItemCount = 6;
        private const int PER_PAGE_COUNT = 6;

        protected override void Awake()
        {
            _scrollView = GetChildComponent<KScrollView>("ScrollView_haoyou");

            _applicationList = _scrollView.GetChildComponent<KPageableList>("mask/content");
            _applicationList.SetPadding(5, -4, 5, -4);
            _applicationList.SetGap(-5, 0);
            _applicationList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);

            _txtEmpty = GetChildComponent<StateText>("Label_txtEmpty");
            int emptyContent = 57031;
            _txtEmpty.CurrentText.text = emptyContent.ToLanguage();
            base.Awake();
           
        }

        public void OnClose()
        {
            Visible = false;
            RemoveEventListener();
        }

        public void OnShow()
        {
            Visible = true;
            AddEventListener();
            OnAddFriendApplication();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(FriendEvents.FRIEND_APPLICATION_CHANGE, OnAddFriendApplication);
            _scrollView.ScrollRect.onRequestNext.AddListener(ShowNextPage);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(FriendEvents.FRIEND_APPLICATION_CHANGE, OnAddFriendApplication);
            if (_scrollView != null)
            {
                _scrollView.ScrollRect.onRequestNext.RemoveListener(ShowNextPage);
            }

        }

        private void ShowNextPage(KScrollRect scrollRect)
        {
            _currentItemCount += PER_PAGE_COUNT;
            OnAddFriendApplication();
        }

        private void OnAddFriendApplication()
        {
            _txtEmpty.Visible = PlayerDataManager.Instance.FriendData.FriendApplicationList.Count == 0;
            List<PbFriendAvatarInfo> showApplicationList = GetShowApplicationList();
            _applicationList.SetDataList<FriendApplicationItem>(showApplicationList);
        }

        private List<PbFriendAvatarInfo> GetShowApplicationList()
        {
            List<PbFriendAvatarInfo> applicationList = PlayerDataManager.Instance.FriendData.FriendApplicationList;
            int applicationListCount = applicationList.Count;
            int showApplicationCount = _currentItemCount > applicationListCount ? applicationListCount : _currentItemCount;

            return applicationList.GetRange(0, showApplicationCount);
        }


    }
}
