﻿#region 模块信息
/*==========================================
// 文件名：BlackListView
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/11 21:25:29
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleFriend
{
    public class BlackListView:KContainer
    {

        private KScrollView _scrollView;
        private KPageableList _blackList;
        private StateText _txtEmpty;

        private int _currentItemCount = 6;
        private const int PER_PAGE_COUNT = 6;
        protected override void Awake()
        {
            _scrollView = GetChildComponent<KScrollView>("ScrollView_haoyou");
            _blackList = _scrollView.GetChildComponent<KPageableList>("mask/content");
            _txtEmpty = GetChildComponent<StateText>("Label_txtEmpty");

            int emptyContent = 57051;
            _txtEmpty.CurrentText.text = emptyContent.ToLanguage();

            _blackList.SetPadding(5, 0, 5, 0);
            _blackList.SetGap(-8, 0);
            _blackList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);

            base.Awake();
           
        }

        public void OnShow()
        {
            this.gameObject.SetActive(true);
            AddEventListener();
            OnBlackListChange();
        }

        public void OnHide()
        {
            this.gameObject.SetActive(false);
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(ChatEvents.BLACK_LIST_CHANGE, OnBlackListChange);
            _scrollView.ScrollRect.onRequestNext.AddListener(ShowNextPage);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(ChatEvents.BLACK_LIST_CHANGE, OnBlackListChange);
            if (_scrollView != null)
            {
                _scrollView.ScrollRect.onRequestNext.RemoveListener(ShowNextPage);
            }
        }

        private void ShowNextPage(KScrollRect scrollRect)
        {
            _currentItemCount += PER_PAGE_COUNT;
            OnBlackListChange();
        }

        private void OnBlackListChange()
        {
            if(_scrollView!=null)
            {
                _txtEmpty.Visible = PlayerDataManager.Instance.ChatData.BlackList.Count == 0;
                List<PbChatAvatarInfo> showBlackList = GetShowBlackList();

                _blackList.SetDataList<BlackListItem>(showBlackList);
            }
            
        }

        private List<PbChatAvatarInfo> GetShowBlackList()
        {
            List<PbChatAvatarInfo> blackList = PlayerDataManager.Instance.ChatData.BlackList;
            int blackListCount = blackList.Count;
            int showBlackCount = _currentItemCount > blackListCount ? blackListCount : _currentItemCount;

            return blackList.GetRange(0, showBlackCount);
        }

    }
}
