﻿#region 模块信息
/*==========================================
// 文件名：FriendSendFriendListView
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/27 17:44:00
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleFriend
{
    public class FriendSendFriendListView:KContainer
    {
        private PageableScrollView _scrollView;
        private StateText _txtEmpty;

        public KComponentEvent<UInt64,string> onSelectedIndexChanged = new KComponentEvent<ulong,string>();
        private UInt64 _playerDbId;
        protected override void Awake()
        {
            _scrollView = AddChildComponent<PageableScrollView>("ScrollView_xuanzehaoyouzengsong");
            _scrollView.IsPageable = false;
            _scrollView.SetCapacity(12);
            _scrollView.SetGap(-15, 0);
            _txtEmpty = GetChildComponent<StateText>("Label_txtMeiyouhaoyou");
            _txtEmpty.Visible = false;
            base.Awake();

        }

        protected override void OnEnable()
        {
            AddEventListner();
            if (PlayerDataManager.Instance.FriendData.hasFriendSendDataRequest)
            {
                FriendManager.Instance.GetBlessedList();
                FriendManager.Instance.GetBlessingList();
                FriendManager.Instance.GetReceivedList();
                FriendManager.Instance.SendGetFriendListMsg();
            }
           
            PlayerDataManager.Instance.FriendData.hasFriendSendDataRequest = false;
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            _scrollView.ClearSelected();
            base.OnDisable();
        }

        public void SetSelectPlayer(UInt64 playerDbId)
        {
            _playerDbId = playerDbId;
            OnFriendListChange();
            SetSelected();
        }

        private void AddEventListner()
        {
            _scrollView.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
             EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_LIST,OnFriendListChange);
        }

        private void RemoveEventListener()
        {
            _scrollView.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            EventDispatcher.RemoveEventListener(FriendEvents.REFRESH_FRIEND_LIST,OnFriendListChange);
        }

        private void OnSelectedIndexChanged(PageableScrollView scrollPage,int index)
        {
            onSelectedIndexChanged.Invoke(FriendSendFriendItem.selectedPlayer,FriendSendFriendItem.selectedPlayerName);
        }

        private void  OnFriendListChange()
        {
            FriendData friendData = PlayerDataManager.Instance.FriendData;
            List<PbFriendAvatarInfo> list = friendData.GetFriendInfoByDefault();
            PbFriendAvatarInfo info = null;
            for (int i = 0; i < list.Count;i++ )
            {
                if(list[i].dbid == _playerDbId)
                {
                    info = list[i];
                    list.RemoveAt(i);
                    break;
                }
            }
            if(info!=null)
            {
                list.Insert(0, info);
                FriendSendFriendItem.selectedPlayer = info.dbid;
                FriendSendFriendItem.selectedPlayerName = info.name;
            }
            _scrollView.SetDataList<FriendSendFriendItem>(list, 1, 1, true);
            _txtEmpty.Visible = friendData.FriendAvatarList.Count == 0;
            SetSelected();
        }

        private void SetSelected()
        {
            int count = _scrollView.List.ItemList.Count;
            for (int i = 0; i < count; i++)
            {
                FriendSendFriendItem item = _scrollView.List.ItemList[i] as FriendSendFriendItem;
                if (item.Data != null)
                {
                    PbFriendAvatarInfo avatarInfo = item.Data as PbFriendAvatarInfo;
                    if (avatarInfo.dbid == _playerDbId)
                    {
                        _scrollView.List.SelectedIndex = item.Index;
                        return;
                    }
                }
            }
        }

    }
}
