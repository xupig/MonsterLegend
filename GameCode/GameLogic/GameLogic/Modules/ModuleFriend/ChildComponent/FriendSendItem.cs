﻿#region 模块信息
/*==========================================
// 文件名：FriendSendItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/28 9:53:16
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleFriend
{
    public class FriendSendItem:KList.KListItemBase
    {

        private StateImage _imgDiamond;
        private StateText _txtIntimate;
        private ItemGrid _iconContainer;
        public static string intimateTemplate;
        private StateImage _selectedImg;

        private IntimateItemInfo _itemInfo;
        protected override void Awake()
        {
            _imgDiamond = GetChildComponent<StateImage>("Image_biaoshiIcon");
            _txtIntimate = GetChildComponent<StateText>("Label_txtWanjiamingzi");
            _iconContainer = GetChildComponent<ItemGrid>("Container_wupin");
            if(intimateTemplate == null)
            {
                intimateTemplate = _txtIntimate.CurrentText.text;
            }
            _selectedImg = GetChildComponent<StateImage>("Image_EquipSelectedImageguang02");
            _selectedImg.Visible = false;
            _iconContainer.onClick = OnClick;
        }

        private void OnClick()
        {
            onClick.Invoke(this, this.Index);
        }

        public override object Data
        {
            get
            {
                return _itemInfo;
            }
            set
            {
                _itemInfo = value as IntimateItemInfo;
                Refresh();
            }
        }

        private void Refresh()
        {
            _selectedImg.Visible = false;
            if (_itemInfo != null)
            {
                _txtIntimate.CurrentText.text = string.Format(intimateTemplate, _itemInfo.item.__value);
                _iconContainer.SetItemData(_itemInfo.item.__id);
               // MogoAtlasUtils.AddIcon(_iconContainer.gameObject, item_helper.GetIcon(_itemInfo.item.__id));
                _imgDiamond.gameObject.SetActive(_itemInfo.IsDirect == false);
            }
        }

        public override bool IsSelected
        {
            get
            {
                return base.IsSelected;
            }
            set
            {
                base.IsSelected = value;
                _selectedImg.Visible = value;
                //选中状态处理
            }
        }

        public override void Dispose()
        {
            _iconContainer.onClick = null;
        }

    }
}
