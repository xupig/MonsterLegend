﻿#region 模块信息
/*==========================================
// 文件名：FriendItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/16 20:26:02
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using UnityEngine;
using Common.ExtendComponent;
using UnityEngine.EventSystems;
using Game.UI;

namespace ModuleFriend
{
    public class FriendItem:KList.KListItemBase
    {
        private StateText _txtProgress;
        private KProgressBar _progressBar;
        private StateText _txtBlessed;
        private StateText _txtFightForce;
        private StateText _txtLevel;
        private StateText _txtVipLevel;
        private StateText _txtName;
        private KButton _btnBless;
        private KButton _btnChat;
        private StateImage _imgBlessed;
        private IconContainer _iconContainer;
        private LoveStarItemList _loveStarItemList;
        private KParticle _blessParticle;

        private PbFriendAvatarInfo _friendAvatarInfo;
        private PositionInfo posInfo;

        private StateImage _imgOnline;

        private static string _template;
        protected override void Awake()
        {
            _imgOnline = GetChildComponent<StateImage>("Image_image02");
            _loveStarItemList = gameObject.AddComponent<LoveStarItemList>();
            _txtProgress = GetChildComponent<StateText>("Label_txtJindu");
            _progressBar = GetChildComponent<KProgressBar>("ProgressBar_jindu");
            _txtBlessed = GetChildComponent<StateText>("Label_txtYizhufu");
            _txtFightForce = GetChildComponent<StateText>("Label_txtZhanli02");
            // 隐藏VIP等级显示
            _txtVipLevel = GetChildComponent<StateText>("Label_txtDengji");
            _txtVipLevel.Visible = false;
            //if(_template==null)
            //{
            //    _template = _txtVipLevel.CurrentText.text;
            //}
            _txtLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            _txtName = GetChildComponent<StateText>("Label_txtwanjiamingzi");
            _btnBless = GetChildComponent<KButton>("Button_tili");
            _blessParticle = _btnBless.GetChild("fx_ui_23_zhiyin_01").AddComponent<KParticle>();

            _btnChat = GetChildComponent<KButton>("Button_duihua");
            _imgBlessed = _btnBless.GetChildComponent<StateImage>("xiaodian");
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            MaskEffectItem maskEffectItem = gameObject.AddComponent<MaskEffectItem>();
            maskEffectItem.Init(_btnBless.gameObject, CanShowEffect);

            posInfo = new PositionInfo();
            posInfo.pos = new Vector2(120, 0);
        }

        public override void Show()
        {
            _btnBless.onClick.AddListener(OnClickBless);
            _btnChat.onClick.AddListener(OnClickChat);
            EventDispatcher.AddEventListener<UInt64>(FriendEvents.FRIEND_BLESS_STATE_CHANGE, OnBlessStateChange);
            EventDispatcher.AddEventListener<UInt64>(FriendEvents.FRIEND_INTIMATE_CHANGE,OnIntimateChange);
            EventDispatcher.AddEventListener<UInt64>(FriendEvents.FRIEND_ONLINE_STATE_CHANGE, OnFriendOnlineStateChange);
            EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_RECEIVED_LIST, OnFriendListChange);
            EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_SEND_LIST, OnFriendListChange);
            EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_RECV_LIST, OnFriendListChange);
            base.Show();
        }

        public override void Hide()
        {
            _btnBless.onClick.RemoveListener(OnClickBless);
            _btnChat.onClick.RemoveListener(OnClickChat);
            EventDispatcher.RemoveEventListener<UInt64>(FriendEvents.FRIEND_BLESS_STATE_CHANGE, OnBlessStateChange);
            EventDispatcher.RemoveEventListener<UInt64>(FriendEvents.FRIEND_INTIMATE_CHANGE, OnIntimateChange);
            EventDispatcher.RemoveEventListener(FriendEvents.REFRESH_FRIEND_RECEIVED_LIST, OnFriendListChange);
            EventDispatcher.RemoveEventListener<UInt64>(FriendEvents.FRIEND_ONLINE_STATE_CHANGE, OnFriendOnlineStateChange);
            EventDispatcher.RemoveEventListener(FriendEvents.REFRESH_FRIEND_SEND_LIST, OnFriendListChange);
            EventDispatcher.RemoveEventListener(FriendEvents.REFRESH_FRIEND_RECV_LIST, OnFriendListChange);
            base.Hide();
        }

        private void InitBlessParticle()
        {
            _blessParticle.transform.localPosition = new Vector3(-8, 17, 0);
            ParticleSystem particleSystem = _blessParticle.transform.Find("root/Particle_jiantou1").GetComponent<ParticleSystem>();
            particleSystem.startSize = 1.0f;
            _blessParticle.transform.Find("root/Particle_jiantou3").localScale = new Vector3(0.5f, 0.5f, 0);
        }

        private bool CanShowEffect()
        {
            if(_friendAvatarInfo == null)
            {
                return false;
            }
            FriendData friendData = PlayerDataManager.Instance.FriendData;
            return friendData.BlessedDict.ContainsKey(_friendAvatarInfo.dbid) == true;
        }

        private void OnFriendOnlineStateChange(UInt64 dbId)
        {
            if(_friendAvatarInfo!=null && dbId == _friendAvatarInfo.dbid)
            {
                UpdateOnlineState();
            }
        }

        public override void OnPointerClick(PointerEventData evtData)
        {
            base.OnPointerClick(evtData);
            posInfo.avatarInfo = _friendAvatarInfo;
            _friendAvatarInfo.is_friend = 1;
            UIManager.Instance.ShowPanel(PanelIdEnum.PlayerInfo, posInfo);
        }

        private void OnIntimateChange(UInt64 dbId)
        {
            if (_friendAvatarInfo != null && _friendAvatarInfo.dbid == dbId)
            {
                UpdateInitmate();
            }
        }

        private void OnBlessStateChange(UInt64 dbId)
        {
            if (_friendAvatarInfo != null && _friendAvatarInfo.dbid == dbId)
            {
                UpdateBlessState();
            }
        }

        public override void Dispose()
        {
            
        }

        private void OnClickBless()
        {
            FriendData friendData = PlayerDataManager.Instance.FriendData;
            if (friendData.BlessingDict.ContainsKey(_friendAvatarInfo.dbid) == false)//未赠送
            {
                if (friendData.BlessedDict.ContainsKey(_friendAvatarInfo.dbid))//收到赠送
                {
                    FriendManager.Instance.ReplyBless(_friendAvatarInfo.dbid);
                }
                else//未收到赠送
                {
                    if (PlayerDataManager.Instance.FriendData.BlessingNum < GlobalParams.GetFriendBlessingLimit())
                    {
                        FriendManager.Instance.BlessFriend(_friendAvatarInfo.dbid);
                    }
                    else
                    {
                        ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.FRIEND_BLESSING_NUM_LIMIT), PanelIdEnum.MainUIField);
                    }
                }
            }
            else//已赠送
            {
                 if (friendData.BlessedDict.ContainsKey(_friendAvatarInfo.dbid) && friendData.ReceivedDict.ContainsKey(_friendAvatarInfo.dbid) == false)
                 {
                     FriendManager.Instance.ReceiveBless(_friendAvatarInfo.dbid);
                 }
            }
        }


        private void OnClickChat()
        {
            PanelIdEnum.Chat.Show(_friendAvatarInfo);
        }

        public override object Data
        {
            get
            {
                return _friendAvatarInfo;
            }
            set
            {
                _friendAvatarInfo = value as PbFriendAvatarInfo;
                Refresh();
            }
        }

        private void OnFriendListChange()
        {
            if(_friendAvatarInfo!=null)
            {
                UpdateBlessState();
            }
        }

        private void Refresh()
        {
            if(_friendAvatarInfo != null)
            {
                _txtName.CurrentText.text = _friendAvatarInfo.name;
                _txtLevel.CurrentText.text = _friendAvatarInfo.level.ToString();
                _txtFightForce.CurrentText.text = _friendAvatarInfo.fight.ToString();
                ////_txtVipLevel.CurrentText.text = string.Format(_template,_friendAvatarInfo.vip_level);
                UpdateBlessState();
                _iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_friendAvatarInfo.vocation), UpdateOnlineState);
                UpdateInitmate();
                UpdateOnlineState();
            }
           
            //_txtBlessed.gameObject.SetActive(_friendAvatarInfo.)
        }

        public void UpdateOnlineState()
        {
            ImageWrapper[] wrappers = _iconContainer.GetComponentsInChildren<ImageWrapper>();
            for(int i = 0; i < wrappers.Length; i++)
            {
                if (_friendAvatarInfo.online == 1)
                {
                    wrappers[i].SetGray(1);
                }
                else
                {
                    wrappers[i].SetGray(0);
                }
            }
        }

        private void UpdateOnlineState(GameObject go)
        {
            UpdateOnlineState();
        }

        private void UpdateInitmate()
        {
            float level = intimate_helper.GetIntimateLevel((int)_friendAvatarInfo.degree);
            int max = intimate_helper.GetIntimateLevelMax((int)_friendAvatarInfo.degree);
            _loveStarItemList.SetLevel(level);
            _progressBar.Value = (_friendAvatarInfo.degree) / (float)max;
            _txtProgress.CurrentText.text = string.Format("{0}/{1}", _friendAvatarInfo.degree, max);
        }

        private void UpdateBlessState()
        {
            FriendData friendData = PlayerDataManager.Instance.FriendData;
            _blessParticle.Stop();
            if (friendData.ReceivedDict.ContainsKey(_friendAvatarInfo.dbid))
            {
                _btnBless.gameObject.SetActive(false);
                _imgBlessed.Alpha = 0.0f;
                _txtBlessed.Visible = true;
                _txtBlessed.CurrentText.text = MogoLanguageUtil.GetContent(57038);
            }
            else if (friendData.BlessedDict.ContainsKey(_friendAvatarInfo.dbid))
            {
                _btnBless.gameObject.SetActive(true);
                _txtBlessed.Visible = false;
                _imgBlessed.Alpha = 1.0f;

                _blessParticle.Play(true);
                InitBlessParticle();
            }
            else if (friendData.BlessingDict.ContainsKey(_friendAvatarInfo.dbid))
            {
                _btnBless.gameObject.SetActive(false);
                _imgBlessed.Alpha = 0.0f;
                _txtBlessed.Visible = true;
                _txtBlessed.CurrentText.text = MogoLanguageUtil.GetContent(57037);
            }
            else
            {
                _btnBless.gameObject.SetActive(true);
                _txtBlessed.Visible = false;
                _imgBlessed.Alpha = 0.0f;
            }
        }
    }
}
