﻿#region 模块信息
/*==========================================
// 文件名：FriendApplicationItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/17 16:04:46
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleFriend
{
    public class FriendApplicationItem:KList.KListItemBase
    {

        private KButton _btnAccept;
        private KButton _btnReject;
        private IconContainer _iconContainer;
        private StateText _txtLevel;
        private StateText _txtName;
        private StateText _txtFightForce;
        private StateText _txtVipLevel;
        private PbFriendAvatarInfo _avatarInfo;

        private static string _template;
        protected override void Awake()
        {
            _btnAccept = GetChildComponent<KButton>("Button_xuanze");
            _btnReject = GetChildComponent<KButton>("Button_cuowu");
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _txtVipLevel = GetChildComponent<StateText>("Label_txtDengji");
            if(_template == null)
            {
                _template = _txtVipLevel.CurrentText.text;
            }
            _txtName = GetChildComponent<StateText>("Label_txtwanjiamingzi");
            _txtFightForce = GetChildComponent<StateText>("Label_txtZhanli02");
            _txtLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
        }

        public override void Show()
        {
            _btnAccept.onClick.AddListener(OnClickAccept);
            _btnReject.onClick.AddListener(OnClickReject);
            base.Show();
        }

        public override void Hide()
        {
            _btnAccept.onClick.RemoveListener(OnClickAccept);
            _btnReject.onClick.RemoveListener(OnClickReject);
            base.Hide();
        }

        private void OnClickAccept()
        {
            FriendManager.Instance.AcceptFriend(_avatarInfo);
        }

        private void OnClickReject()
        {
            FriendManager.Instance.RejectFriend(_avatarInfo.dbid);
        }

        public override object Data
        {
            get
            {
                return _avatarInfo;
            }
            set
            {
                _avatarInfo = value as PbFriendAvatarInfo;
                Refresh();
            }
        }

        private void Refresh()
        {
            this.gameObject.SetActive(_avatarInfo != null);
            if(_avatarInfo != null)
            {
                _txtName.CurrentText.text = _avatarInfo.name;
                _txtLevel.CurrentText.text =  _avatarInfo.level.ToString();
                _txtFightForce.CurrentText.text = _avatarInfo.fight.ToString();
                _txtVipLevel.CurrentText.text = string.Empty;
                _iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_avatarInfo.vocation));
            }
            
        }

        public override void Dispose()
        {
            
        }
    }
}
