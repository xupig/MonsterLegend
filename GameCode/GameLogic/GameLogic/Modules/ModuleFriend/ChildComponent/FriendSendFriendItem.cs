﻿#region 模块信息
/*==========================================
// 文件名：FriendSendFriendItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/27 19:51:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using MogoEngine.Events;
using System;

namespace ModuleFriend
{
    public class FriendSendFriendItem:KList.KListItemBase
    {

        private PbFriendAvatarInfo _avatarInfo;

        private StateText _txtProgress;
        private KProgressBar _progressBar;
        private StateText _txtFightForce;
        private StateText _txtLevel;
        private StateText _txtName;
        private StateText _txtVipLevel;
        private IconContainer _iconContaienr;
        private LoveStarItemList _loveStarItemList;

        private StateImage _selectedContainer;
        private StateImage _unSelectedContainer;

        public static UInt64 selectedPlayer = 0;
        public static string selectedPlayerName = string.Empty;

        private static string progressTemplate;

        protected override void Awake()
        {
            _loveStarItemList = gameObject.AddComponent<LoveStarItemList>();
            _txtProgress = GetChildComponent<StateText>("Label_txtJindu");
            _progressBar = GetChildComponent<KProgressBar>("ProgressBar_jindu");
            _txtFightForce = GetChildComponent<StateText>("Label_txtZhanli02");
            _txtVipLevel = GetChildComponent<StateText>("Label_txtDengji");
            _txtVipLevel.Visible = false;
            progressTemplate = _txtProgress.CurrentText.text;
            _txtName = GetChildComponent<StateText>("Label_txtwanjiamingzi");
            _iconContaienr = AddChildComponent<IconContainer>("Container_icon");
            _txtLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            _selectedContainer = GetChildComponent<StateImage>("Image_checkmark");
            _unSelectedContainer = GetChildComponent<StateImage>("Image_image");
            _selectedContainer.gameObject.SetActive(false);
        }

        public override object Data
        {
            get
            {
                return _avatarInfo;
            }
            set
            {
                _avatarInfo = value as PbFriendAvatarInfo;
                Refresh();
            }
        }

        public override void Show()
        {
            
            EventDispatcher.AddEventListener<UInt64>(FriendEvents.FRIEND_INTIMATE_CHANGE, OnIntimateChange);
            EventDispatcher.AddEventListener<UInt64>(FriendEvents.FRIEND_ONLINE_STATE_CHANGE, OnFriendOnlineStateChange);
            base.Show();
        }

        public override void Hide()
        {
            EventDispatcher.RemoveEventListener<UInt64>(FriendEvents.FRIEND_INTIMATE_CHANGE, OnIntimateChange);
            EventDispatcher.RemoveEventListener<UInt64>(FriendEvents.FRIEND_ONLINE_STATE_CHANGE, OnFriendOnlineStateChange);
            base.Hide();
        }

        private void OnIntimateChange(UInt64 dbId)
        {
            if (_avatarInfo != null && _avatarInfo.dbid == dbId)
            {
                UpdateInitmate();
            }
        }

        private void UpdateInitmate()
        {
            float level = intimate_helper.GetIntimateLevel((int)_avatarInfo.degree);
            int max = intimate_helper.GetIntimateLevelMax((int)_avatarInfo.degree);
            _loveStarItemList.SetLevel(level);
            _progressBar.Value = (_avatarInfo.degree) / (float)max;
            _txtProgress.CurrentText.text = string.Format(progressTemplate, _avatarInfo.degree, max);
        }

        private void Refresh()
        {
            this.gameObject.SetActive(_avatarInfo != null);

            if (_avatarInfo != null)
            {
                _txtName.CurrentText.text = _avatarInfo.name;
                _txtLevel.CurrentText.text = _avatarInfo.level.ToString();
                _txtFightForce.CurrentText.text = _avatarInfo.fight.ToString();
                //_txtVipLevel.CurrentText.text = string.Format(_template,_avatarInfo.vip_level);
                _iconContaienr.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_avatarInfo.vocation));
                UpdateInitmate();
                _selectedContainer.gameObject.SetActive(_avatarInfo.dbid == selectedPlayer);
                _unSelectedContainer.gameObject.SetActive(_avatarInfo.dbid != selectedPlayer);
                UpdateOnlineState();
            }
        }

        private void OnFriendOnlineStateChange(UInt64 dbId)
        {
            if (_avatarInfo != null && dbId == _avatarInfo.dbid)
            {
                UpdateOnlineState();
            }
        }

        public void UpdateOnlineState()
        {
            ImageWrapper[] wrappers = _iconContaienr.GetComponentsInChildren<ImageWrapper>();
            for (int i = 0; i < wrappers.Length; i++)
            {
                if (_avatarInfo.online == 1)
                {
                    wrappers[i].SetGray(1);
                }
                else
                {
                    wrappers[i].SetGray(0);
                }
            }
        }


        public override void OnPointerClick(UnityEngine.EventSystems.PointerEventData evtData)
        {
            if (_avatarInfo!=null)
            {
                selectedPlayer = _avatarInfo.dbid;
                selectedPlayerName = _avatarInfo.name;
            }
            base.OnPointerClick(evtData);
        }

        public override bool IsSelected
        {
            get
            {
                return base.IsSelected;
            }
            set
            {
                base.IsSelected = value;
                _selectedContainer.gameObject.SetActive(value);
                _unSelectedContainer.gameObject.SetActive(!value);
            }
        }

        public override void Dispose()
        {
            
        }
    }
}
