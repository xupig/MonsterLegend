﻿#region 模块信息
/*==========================================
// 文件名：FriendSearchResultItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/17 16:11:33
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleFriend
{
    public class FriendSearchResultItem:KContainer
    {
        private KButton _btnAdd;
        private IconContainer _iconContainer;
        private StateText _txtLevel;
        private StateText _txtName;
        private StateText _txtFightForce;
        private StateText _txtVipLevel;
        private PbFriendAvatarInfo _avatarInfo;

        private static string _template;
        protected override void Awake()
        {
            _btnAdd = GetChildComponent<KButton>("Button_jiaweihaoyou");
            base.Awake();
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _txtVipLevel = GetChildComponent<StateText>("Label_txtDengji");
            _txtVipLevel.Visible = false;
            _txtName = GetChildComponent<StateText>("Label_txtwanjiamingzi");
            _txtFightForce = GetChildComponent<StateText>("Label_txtZhanli02");
            _txtLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
        }

        protected override void OnEnable()
        {
            _btnAdd.onClick.AddListener(OnClickAdd);
            EventDispatcher.AddEventListener<UInt64>(FriendEvents.FRIEND_ADD_APPLY_SUCCESS, OnAddApplySuccess);
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            _btnAdd.onClick.RemoveListener(OnClickAdd);
            EventDispatcher.RemoveEventListener<UInt64>(FriendEvents.FRIEND_ADD_APPLY_SUCCESS, OnAddApplySuccess);
            base.OnDisable();
        }

        private void OnAddApplySuccess(UInt64 dbId)
        {
            if (_avatarInfo != null && gameObject.activeSelf && dbId == _avatarInfo.dbid)
            {
                this.gameObject.SetActive(false);
            }
        }

        private void OnClickAdd()
        {
            if(PlayerDataManager.Instance.FriendData.GetFriendAvatarInfoByDbid(_avatarInfo.dbid)!=null)
            {
                MessageBox.Show(true, string.Empty, MogoLanguageUtil.GetString(LangEnum.FRIEND_ALREADY_FRIEND));
            }
            else
            {
                FriendManager.Instance.AddFriend(_avatarInfo);
            }
            
        }

        public void SetInfo(PbFriendAvatarInfo avatarInfo)
        {
            _avatarInfo = avatarInfo;
            _txtName.CurrentText.text = avatarInfo.name;
            _txtLevel.CurrentText.text = _avatarInfo.level.ToString();
            _txtFightForce.CurrentText.text = avatarInfo.fight.ToString();
            _iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)avatarInfo.vocation));
        }
    }
}
