﻿#region 模块信息
/*==========================================
// 文件名：BlackListItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/18 17:05:02
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleFriend
{
    public class BlackListItem:KList.KListItemBase
    {

        private KButton _btnCancel;
        private IconContainer _iconContainer;
        private StateText _txtLevel;
        private StateText _txtName;
        private StateText _txtFightForce;
        private StateText _txtVipLevel;
        private PbChatAvatarInfo _avatarInfo;
        private static string _template;
        protected override void Awake()
        {
            _btnCancel = GetChildComponent<KButton>("Button_quxiaopingbi");
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _txtVipLevel = GetChildComponent<StateText>("Label_txtDengji");
            if (_template == null)
            {
                _template = _txtVipLevel.CurrentText.text;
            }
            _txtName = GetChildComponent<StateText>("Label_txtwanjiamingzi");
            _txtFightForce = GetChildComponent<StateText>("Label_txtZhanli02");
            _txtLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
        }

        public override void Show()
        {
            AddEventListener();
            base.Show();
        }
        public override void Hide()
        {
            RemoveEventListener();
            base.Hide();
        }

        private void AddEventListener()
        {
            _btnCancel.onClick.AddListener(OnClickCancel);
        }

        private void RemoveEventListener()
        {
            _btnCancel.onClick.RemoveListener(OnClickCancel);
        }

        private void OnClickCancel()
        {
            ChatManager.Instance.RemoveFromBlackList(_avatarInfo.dbid);
        }

        public override object Data
        {
            get
            {
                return _avatarInfo;
            }
            set
            {
                _avatarInfo = value as PbChatAvatarInfo;
                Refresh();
            }
        }

        private void Refresh()
        {
            if(_avatarInfo != null)
            {
                _txtName.CurrentText.text = _avatarInfo.name;
                _txtLevel.CurrentText.text = _avatarInfo.level.ToString();
                _txtFightForce.CurrentText.text = _avatarInfo.fight.ToString();
                _txtVipLevel.CurrentText.text = string.Empty;
                _iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_avatarInfo.vocation));
            }
        }

        public override void Dispose()
        {
            
        }

    }
}
