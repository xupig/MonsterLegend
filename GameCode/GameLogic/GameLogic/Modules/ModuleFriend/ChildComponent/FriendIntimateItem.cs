﻿#region 模块信息
/*==========================================
// 文件名：FriendIntimateItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/18 16:12:57
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleFriend
{
    public class FriendIntimateItem:KList.KListItemBase
    {

        private StateText _txtIntimate;
        private StateImage _imgBg1;
        private StateImage _imgBg2;
        private PbIntimateEventInfo _eventInfo;

        protected override void Awake()
        {
            _txtIntimate = GetChildComponent<StateText>("Label_txtQinmizhi");
            _imgBg1 = GetChildComponent<StateImage>("Container_bg/ScaleImage_sharedGridBg");
            _imgBg2 = GetChildComponent<StateImage>("Container_bg/ScaleImage_sharedGridBg02");
        }


        public override void Dispose()
        {
            
        }

        public override object Data
        {
            get
            {
                return _eventInfo;
            }
            set
            {
                _eventInfo = value as PbIntimateEventInfo;
                Refresh();
            }
        }

        private void UpdateBgState()
        {
            if (Index % 2 == 0)
            {
                _imgBg2.gameObject.SetActive(true);
                _imgBg1.gameObject.SetActive(false);
            }
            else
            {
                _imgBg1.gameObject.SetActive(true);
                _imgBg2.gameObject.SetActive(false);
            }
        }

        private void Refresh()
        {
            if (_eventInfo == null)
            {
                return;
            }
            UpdateBgState();
            DateTime now = MogoTimeUtil.ServerTimeStamp2LocalTime((long)_eventInfo.record_time);
            switch(_eventInfo.add_type)
            {
                case public_config.FREIDN_DEGREE_GIVE_GOODS:
                    string itemName = item_helper.GetName((int)_eventInfo.item_id);
                    if(_eventInfo.send_dbid == PlayerAvatar.Player.dbid)
                    {
                        _txtIntimate.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.FRIEND_SNED_SOMETHING, _eventInfo.recv_name, itemName, _eventInfo.degree, now.Year, now.Month, now.Day);
                    }
                    else
                    {
                        _txtIntimate.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.FRIEND_RECV_SOMETHING, _eventInfo.send_name, itemName, _eventInfo.degree, now.Year, now.Month, now.Day);
                    }
                    break;
                case public_config.FREIDN_DEGREE_MISSION:
                    string instanceName = instance_helper.GetInstanceName((int)_eventInfo.item_id);
                    string name = (_eventInfo.send_dbid == PlayerAvatar.Player.dbid) ? _eventInfo.recv_name : _eventInfo.send_name;
                    _txtIntimate.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.FRIEND_FINISH_SOMETHING, name, instanceName, _eventInfo.degree, now.Year, now.Month, now.Day);
                    break;
                case public_config.FREIDN_DEGREE_KILL:
                     string monsterName = monster_helper.GetMonsterName((int)_eventInfo.item_id);
                    string nname = (_eventInfo.send_dbid == PlayerAvatar.Player.dbid) ? _eventInfo.recv_name : _eventInfo.send_name;
                    _txtIntimate.CurrentText.text = (57105).ToLanguage(nname, monsterName, _eventInfo.degree, now.Year, now.Month, now.Day);
                    break;
                case public_config.FREIDN_DEGREE_TIME:
                    string pname = (_eventInfo.send_dbid == PlayerAvatar.Player.dbid) ? _eventInfo.recv_name : _eventInfo.send_name;
                    _txtIntimate.CurrentText.text = (57104).ToLanguage(pname, _eventInfo.item_num, _eventInfo.degree, now.Year, now.Month, now.Day);
                    break;
                case public_config.FREIDN_DEGREE_ALCHEMY:
                    if (_eventInfo.send_name == PlayerAvatar.Player.name)
                    {
                        //你和$(1,{0})共同炼金，你们的亲密值增加了$(23,{1})
                        _txtIntimate.CurrentText.text = (110506).ToLanguage(_eventInfo.recv_name, _eventInfo.degree, now.Year, now.Month, now.Day);
                    }
                    else if (_eventInfo.recv_name == PlayerAvatar.Player.name)
                    {
                        //$(1,{0})和你共同炼金，你们的亲密值增加了$(23,{1})
                        _txtIntimate.CurrentText.text = (110507).ToLanguage(_eventInfo.send_name, _eventInfo.degree, now.Year, now.Month, now.Day);
                    }
                    break;
                default:
                    _txtIntimate.CurrentText.text = string.Empty;
                    break;

            }
        }

    }
}
