﻿using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleFriend
{
    public class FriendRecommendItem:KList.KListItemBase
    {
        private IconContainer _iconContainer;
        private StateText _txtLevel;
        private KButton _addButton;
        private StateText _txtName;
        private StateText _txtFightValue;
        private StateText _txtApplying;

        private PbAvatarData _data;

        protected override void Awake()
        {
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _txtLevel = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            _addButton = GetChildComponent<KButton>("Button_tianjia");
            _txtName = GetChildComponent<StateText>("Label_txtwanjiamingzi");
            _txtFightValue = GetChildComponent<StateText>("Label_txtZhanli02");
            _txtApplying = GetChildComponent<StateText>("Label_txtyishenqing");
            AddEventListener();
        }

        public override void Dispose()
        {
            RemoveEventListener();
            _data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbAvatarData;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            Refresh();
        }

        private void Refresh()
        {
            _iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_data.vocation));
            _txtLevel.CurrentText.text = _data.level.ToString();
            _txtFightValue.CurrentText.text = _data.fight_force.ToString();
            _txtName.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.name_bytes);
            _addButton.Visible = true;
            _txtApplying.Visible = false;
        }

        private void AddEventListener()
        {
            _addButton.onClick.AddListener(AddFriend);
        }

        private void RemoveEventListener()
        {
            _addButton.onClick.RemoveListener(AddFriend);
        }

        private void AddFriend()
        {
            _addButton.Visible = false;
            _txtApplying.Visible = true;
            FriendManager.Instance.AddFriend(_data.dbid);
        }
    }
}
