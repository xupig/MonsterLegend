﻿#region 模块信息
/*==========================================
// 文件名：FriendSendPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/27 17:01:50
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleFriend
{
    public class FriendSendPanel:BasePanel
    {
        //private KButton _btnReturn;
        private FriendSendFriendListView _friendListView;
        private FriendSendView _sendView;
        private FriendSendCurrencyView _currencyView;
        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_fanhui");
            _friendListView = AddChildComponent<FriendSendFriendListView>("Container_friend");
            _sendView = AddChildComponent<FriendSendView>("Container_send");
            _currencyView = AddChildComponent<FriendSendCurrencyView>("Container_dibu");
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.FriendSend; }
        }

        public override void OnShow(object data)
        {
            _friendListView.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            SetData(data);
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            if(data!=null)
            {
                PbFriendAvatarInfo avatarInfo = data as PbFriendAvatarInfo;
                _sendView.sendPlayer = avatarInfo.dbid;
                _sendView.sendPlayerName = avatarInfo.name;
                _friendListView.SetSelectPlayer(avatarInfo.dbid);
            }
        }

        public override void OnClose()
        {
            _friendListView.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
        }

        private void OnSelectedIndexChanged(UInt64 dbId,string name)
        {
            _sendView.sendPlayer = dbId;
            _sendView.sendPlayerName = name;
        }

    }
}
