﻿#region 模块信息
/*==========================================
// 文件名：FriendModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleFriend
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/11 20:40:17
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleFriend
{
    public class FriendModule:BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
           // AddPanel(PanelIdEnum.Friend, "Container_FriendsPanel", MogoUILayer.LayerUIPanel, "ModuleFriend.FriendPanel");
            AddPanel(PanelIdEnum.Interactive, "Container_InteractiveFriendPanel", MogoUILayer.LayerSpecialUIPanel, "ModuleInteractive.InteractivePanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.FriendSend, "Container_FriendSendPanel", MogoUILayer.LayerUIPanel, "ModuleFriend.FriendSendPanel", BlurUnderlay.Have, HideMainUI.Hide);

            RegisterPanelModule(PanelIdEnum.Interactive, PanelIdEnum.Interactive);
        }
    }
}
