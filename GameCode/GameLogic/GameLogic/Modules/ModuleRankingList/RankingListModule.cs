﻿#region 模块信息
/*==========================================
// 文件名：RankingListModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleRankingList
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/4/15 10:10:24
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;

namespace ModuleRankingList
{
    public class RankingListModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.RankingList, "Container_RankingListPanel", MogoUILayer.LayerUIPanel, "ModuleRankingList.RankingListPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.RankingListDetail, "Container_RankingListDetail", MogoUILayer.LayerUIPanel, "ModuleRankingList.RankingListDetailPanel", BlurUnderlay.Have, HideMainUI.Hide);
        }

    }
}