﻿using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System.Collections.Generic;
using System.Text;


namespace ModuleRankingList
{
    public class RankPlayerRuneItem : KList.KListItemBase
    {
        private const int RUNE_ICE_ICON_ID = 11404;
        private const int RUNE_POISON_ICON_ID = 11405;
        private const int RUNE_LIGHT_ICON_ID = 11406;
        private const int RUNE_FIRE_ICON_ID = 11407;
        private const int RUNE_LIGHTNING_ICON_ID = 11408;

        private static Dictionary<int, int> RuneIconDict = new Dictionary<int, int>()
            {
                {1, RUNE_FIRE_ICON_ID}, {2, RUNE_ICE_ICON_ID},
                {3, RUNE_LIGHTNING_ICON_ID}, {4, RUNE_POISON_ICON_ID},
                {5, RUNE_LIGHT_ICON_ID}
            };

        private StateText _itemListTxt;
        private ItemGrid _itemGrid;

        private PlayerRuneItemData runeItemData;

        protected override void Awake()
        {
            _itemListTxt = GetChildComponent<StateText>("Label_txtbaoshixinxi");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
        }

        public override void Dispose()
        {

        }

        public override object Data
        {
            get
            {
                return runeItemData;
            }
            set
            {
                if (value != null)
                {
                    runeItemData = value as PlayerRuneItemData;
                    RefreshItemView();
                }
            }
        }

        private void RefreshItemView()
        {
            if (RuneIconDict.ContainsKey(runeItemData.runePageIndex))
            {
                _itemGrid.SetItemData(RuneIconDict[runeItemData.runePageIndex], 0);
            }
            SetItemListTxt();
        }

        private void SetItemListTxt()
        {
            List<RuneItemInfo> runeItemInfoList = runeItemData.runeItemInfoList;
            StringBuilder itemListDescTxt = new StringBuilder();
            if (runeItemInfoList.Count == 0)
            {
                _itemListTxt.CurrentText.text = "   " + MogoLanguageUtil.GetString(LangEnum.RANKINGLIST_NOT_RUNE_EQUIPED);
                return;
            }
            for (int i = 0; i < runeItemInfoList.Count; i++)
            {
                if (i == 5)
                {
                    itemListDescTxt.Append("\n");
                }
                itemListDescTxt.Append("   ");
                itemListDescTxt.Append(ColorDefine.GetQualityWithColorHtmlString(item_helper.GetQuality(runeItemInfoList[i].Id), GetNameWithLevel(runeItemInfoList[i].Id, runeItemInfoList[i].Level)));
            }
            _itemListTxt.CurrentText.text = itemListDescTxt.ToString();
        }

        private string GetNameWithLevel(int runeItemId, int level)
        {
            string levelDesc = string.Format(MogoLanguageUtil.GetContent(6052023),level);
            return string.Format("{0}{1}", levelDesc, item_helper.GetName(runeItemId));
        }
    }
}
