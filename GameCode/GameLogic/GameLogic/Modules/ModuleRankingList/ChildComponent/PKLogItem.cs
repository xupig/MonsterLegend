﻿#region 模块信息
/*==========================================
// 文件名：PKLogItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleRankingList.ChildComponent
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/12/10 17:12:32
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using MogoEngine.Utils;
namespace ModuleRankingList
{
    public class PKLogItem : KList.KListItemBase
    {
        private PbPkLog _data;

        private StateText _timeTxt;
        private StateText _txtOpponentName;
        private StateText _fightForceTxt;
        private StateImage _winImage;
        private StateImage _loseImage;

        protected override void Awake()
        {
            _timeTxt = GetChildComponent<StateText>("Label_txtShijian");
            _txtOpponentName = GetChildComponent<StateText>("Label_txtDuishou");
            _fightForceTxt = GetChildComponent<StateText>("Label_txtZhanli");
            _winImage = GetChildComponent<StateImage>("Image_sheng");
            _loseImage = GetChildComponent<StateImage>("Image_fu");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as PbPkLog;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            _timeTxt.CurrentText.text = MogoTimeUtil.ServerTimeStamp2LocalTime(_data.date).ToString("yyyy-MM-dd");
            _txtOpponentName.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.other_name_bytes);
            _fightForceTxt.CurrentText.text = string.Format("{0}:<color=#0096FF>{1}</color>", _data.fight, _data.other_fight);
            _winImage.Visible = _data.is_win >= 0;
            _loseImage.Visible = _data.is_win == -1;
        }

       
        public override void Dispose()
        {

        }
    }
}