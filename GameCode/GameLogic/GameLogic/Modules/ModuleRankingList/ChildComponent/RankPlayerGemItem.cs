﻿#region 模块信息
/*==========================================
// 文件名：RankPlayerItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleRankingList.ChildComponent
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/4/21 16:29:10
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Common.Global;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System.Collections.Generic;
using System.Text;
namespace ModuleRankingList
{
    public class RankPlayerGemItem : KList.KListItemBase
    {
        private StateText _itemListTxt;
        private ItemGrid _itemGrid;

        private PbGridItem gridItem;

        protected override void Awake()
        {
            _itemListTxt = GetChildComponent<StateText>("Label_txtbaoshixinxi");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
        }

        public override void Dispose()
        {
            gridItem = null;
        }

        public override object Data
        {
            get
            {
                return gridItem;
            }
            set
            {
                if (value != null)
                {
                    gridItem = value as PbGridItem;
                    RefreshItemView();
                }
            }
        }

        private void RefreshItemView()
        {
            EquipItemInfo equipItemInfo = new EquipItemInfo(gridItem);
            _itemGrid.SetItemData(equipItemInfo);
            SetItemListTxt();
        }

        private void SetItemListTxt()
        {
            StringBuilder itemListDescTxt = new StringBuilder();
            List<PbEquipGems> pbEquipGemList = gridItem.item.gems;
            if (pbEquipGemList.Count == 0)
            {
                _itemListTxt.CurrentText.text = " " + MogoLanguageUtil.GetString(LangEnum.RANKINGLIST_NOT_GEM_INLAYED);
                return;
            }
            for (int i = 0; i < pbEquipGemList.Count; i++)
            {
                itemListDescTxt.Append(" ");
                itemListDescTxt.Append(item_helper.GetName((int)pbEquipGemList[i].gem_id));
                if (i % 6 == 5)
                {
                    itemListDescTxt.Append("\n");
                }
            }
            _itemListTxt.CurrentText.text = itemListDescTxt.ToString();
        }

        private void SetIcon()
        {
            _itemGrid.Clear();
            _itemGrid.SetItemData(0, 0);
        }
    }
}