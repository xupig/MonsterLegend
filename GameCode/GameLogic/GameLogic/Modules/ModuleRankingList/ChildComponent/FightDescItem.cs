﻿#region 模块信息
/*==========================================
// 文件名：FightDescItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleRankingList.ChildComponent
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/4/11 9:35:51
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using GameData;
namespace ModuleRankingList
{
    public class FightDescItem : KContainer
    {
        private StateText _descTxt;
        private StateText _otherFightTxt;
        private StateText _myFightTxt;
        private StateImage _upImage;
        private StateImage _downImage;
        private StateText _differValue;
        private StateText _getFightForceTxt;
        private StateImage _bg02;

        protected override void Awake()
        {
            _descTxt = GetChildComponent<StateText>("Label_txtJichuzhuangbeizhanli");
            _otherFightTxt = GetChildComponent<StateText>("Label_txtZhandouli");
            _myFightTxt = GetChildComponent<StateText>("Label_txtZhandouli02");
            _upImage = GetChildComponent<StateImage>("Image_tisheng");
            _downImage = GetChildComponent<StateImage>("Image_xiajiang");
            _differValue = GetChildComponent<StateText>("Label_txtTisheng");
            _getFightForceTxt = GetChildComponent<StateText>("Label_txtTishengfangshi");
            _bg02 = GetChildComponent<StateImage>("Container_bg/ScaleImage_sharedGridBg02");
        }

        public void Refresh(int id, int myValue, int playerValue)
        {
            _descTxt.CurrentText.text = rank_helper.GetEquipDesc(id);
            if (myValue == -1)
            {
                _otherFightTxt.CurrentText.text = playerValue.ToString();
                _myFightTxt.CurrentText.text = string.Empty;
                _upImage.Visible = false;
                _downImage.Visible = false;
                _differValue.CurrentText.text = string.Empty;
            }
            else
            {
                _otherFightTxt.CurrentText.text = playerValue.ToString();
                _myFightTxt.CurrentText.text = myValue.ToString();
                if (playerValue > myValue)
                {
                    _upImage.Visible = false;
                    _downImage.Visible = true;
                    _differValue.CurrentText.text = (playerValue - myValue).ToString();
                }
                else if (playerValue < myValue)
                {
                    _upImage.Visible = true;
                    _downImage.Visible = false;
                    _differValue.CurrentText.text = (myValue - playerValue).ToString();
                }
                else
                {
                    _upImage.Visible = false;
                    _downImage.Visible = false;
                    _differValue.CurrentText.text = string.Empty;
                }
            }
            _getFightForceTxt.CurrentText.text = rank_helper.GetGetFightForceDesc(id);
            _bg02.Visible = (id % 2) == 0;
        }
    }
}