﻿#region 模块信息
/*==========================================
// 文件名：AttributeListItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleRankingList.ChildComponent
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/4/25 9:29:28
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System.Collections.Generic;
namespace ModuleRankingList
{
    public class AttributeListItem : KList.KListItemBase
    {
        private StateText _attributeName;
        private StateText _attributeValue;
        
        private KeyValuePair<string, string> nameValueData;

        protected override void Awake()
        {
            _attributeName = GetChildComponent<StateText>("Label_attributeName");
            _attributeValue = GetChildComponent<StateText>("Label_txtFightValue");
        }

        public override void Dispose()
        {
            
        }

        public override object Data
        {
            get
            {
                return nameValueData;
            }
            set
            {
                nameValueData = (KeyValuePair<string,string>)value;
                RefreshItem();
            }
        }

        private void RefreshItem()
        {
            _attributeName.CurrentText.text = nameValueData.Key;
            _attributeValue.CurrentText.text = nameValueData.Value;
        }
    }
}