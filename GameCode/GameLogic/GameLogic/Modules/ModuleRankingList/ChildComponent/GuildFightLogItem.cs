﻿#region 模块信息
/*==========================================
// 文件名：GuildFightLogItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleRankingList.ChildComponent
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/12/10 17:12:19
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using MogoEngine.Utils;
namespace ModuleRankingList
{
    public class GuildFightLogItem : KList.KListItemBase
    {
        private PbGuildPkLog _data;

        private StateText _txtTime;
        private StateText _txtType;
        private StateText _txtOpponentName;
        private StateText _txtMilitaryRank;
        private StateText _fightForceTxt;
        private StateImage _winImage;
        private StateImage _loseImage;

        protected override void Awake()
        {
            _txtTime = GetChildComponent<StateText>("Label_txtShijian");
            _txtType = GetChildComponent<StateText>("Label_txtSaishi");
            _txtOpponentName = GetChildComponent<StateText>("Label_txtDuishouMingcheng");
            _txtMilitaryRank = GetChildComponent<StateText>("Label_txtDuishou");
            _fightForceTxt = GetChildComponent<StateText>("Label_txtZhanli");
            _winImage = GetChildComponent<StateImage>("Image_sheng");
            _loseImage = GetChildComponent<StateImage>("Image_fu");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as PbGuildPkLog;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            _txtTime.CurrentText.text = MogoTimeUtil.ServerTimeStamp2LocalTime(_data.date).ToString("yyyy-MM-dd");
            _txtType.CurrentText.text = MogoLanguageUtil.GetContent((int)_data.battle_type + 111507);
            string nameString = MogoProtoUtils.ParseByteArrToString(_data.other_name_bytes);
            if(nameString.Length == 0)
            {
                _txtOpponentName.CurrentText.text = MogoLanguageUtil.GetContent(0);
            }
            else
            {
                _txtOpponentName.CurrentText.text = nameString;
            }
            string militaryString;
            if (_data.military == 0)
            {
                militaryString = MogoLanguageUtil.GetContent(0);
            }
            else
            {
                militaryString = MogoLanguageUtil.GetContent(111516 + (int)_data.military);
            }
            string otherMiltaryString;
            if (_data.other_military == 0)
            {
                otherMiltaryString = MogoLanguageUtil.GetContent(0);
            }
            else
            {
                otherMiltaryString = MogoLanguageUtil.GetContent(111516 + (int)_data.other_military);
            }
            _txtMilitaryRank.CurrentText.text = string.Format("{0}:<color=#0096FF>{1}</color>",
                militaryString,
                otherMiltaryString);
            _fightForceTxt.CurrentText.text = string.Format("{0}:<color=#0096FF>{1}</color>", _data.fight, _data.other_fight);
            _winImage.Visible = _data.is_win >= 0;
            _loseImage.Visible = _data.is_win == -1;
        }

        public override void Dispose()
        {

        }
    }
}