﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs;
using Common.Structs.Enum;
using Game.UI.UIComponent;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleRankingList
{
    public class RoleEquipItem : KContainer
    {
        private StateImage _imgSelected;
        private KButton _backgroundBtn;
        private ItemGrid _itemGrid;
        public int EquipPosition { get; set; }

        public KComponentEvent<int> onClick = new KComponentEvent<int>();
        private EquipItemInfo equipInfo;
        private WingItemData wingData;

        protected override void Awake()
        {
            _imgSelected = GetChildComponent<StateImage>("Image_selected");
            _backgroundBtn = GetChildComponent<KButton>("Button_background");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _itemGrid.gameObject.AddComponent<RaycastComponent>();
            _imgSelected.gameObject.AddComponent<RaycastComponent>();
            _imgSelected.gameObject.SetActive(false);
            AddEventListener();
        }

        private void AddEventListener()
        {
            _backgroundBtn.onClick.AddListener(onBgBtnClicked);
        }

        private void RemoveEventListener()
        {
            _backgroundBtn.onClick.RemoveListener(onBgBtnClicked);
        }

        public void ClearEquip()
        {
            _itemGrid.Clear();
        }

        public void SetEquip(EquipItemInfo equipInfo)
        {
            if(EquipPosition == (int)EquipType.love)
            {
                return;
            }
            if (EquipPosition == (int)EquipType.wing)
            {
                _itemGrid.SetItemData(wingData.icon, (int)ItemQualityDefine.WHITE);
                return;
            }
            this.equipInfo = equipInfo;
            if (equipInfo != null)
            {
                _itemGrid.SetItemData(equipInfo);
            }
            else
            {
                _itemGrid.Clear();
            }
        }

        public void SetSelected(bool isSelected)
        {
            _imgSelected.gameObject.SetActive(isSelected);
            if (isSelected == true)
            {
                ToolTipsManager.Instance.ShowItemTip(equipInfo, PanelIdEnum.RankingListDetail);
            }
        }

        public void SetWing(WingItemData wingData)
        {
            if (EquipPosition == (int)EquipType.wing)
            {
                this.wingData = wingData;
                if (wingData != null)
                {
                    _itemGrid.SetItemData(wingData.icon, (int)ItemQualityDefine.WHITE);
                }
                else
                {
                    _itemGrid.Clear();
                }
            }
        }

        private void onBgBtnClicked()
        {
            if (equipInfo != null || EquipPosition == (int)EquipType.love || EquipPosition == (int)EquipType.wing)
            {
                onClick.Invoke(EquipPosition);
            }
        }

        protected override void OnDestroy()
        {

        }
    }
}
