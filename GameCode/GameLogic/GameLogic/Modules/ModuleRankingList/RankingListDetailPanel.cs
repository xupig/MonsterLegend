﻿#region 模块信息
/*==========================================
// 文件名：RankingListDetailPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleRankingList
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/4/18 13:14:43
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System.Collections.Generic;
namespace ModuleRankingList
{
    public class RankingListDetailPanel : BasePanel
    {
        private CategoryList _playerInfoCategory;
        private RankPlayerRoleInfoView _playerRoleInfoView;
        private RankPlayerFightInfoView _playerFightInfoView;
        private RankPlayerGemView _playerGemView;
        private RankPlayerRuneView _playerRuneView;
        private RankPlayerFightForceView _rankPlayerFightCompareView;

        private ulong playerDbId;
        public static int index = 1;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_fanhui");
            _playerInfoCategory = GetChildComponent<CategoryList>("List_categoryList");
            _playerRoleInfoView = AddChildComponent<RankPlayerRoleInfoView>("Container_roleInfoView");
            _playerGemView = AddChildComponent<RankPlayerGemView>("Container_baoshi");
            _playerRuneView = AddChildComponent<RankPlayerRuneView>("Container_fuwen");
            _playerFightInfoView = AddChildComponent<RankPlayerFightInfoView>("Container_gonghuizhan");
            _rankPlayerFightCompareView = AddChildComponent<RankPlayerFightForceView>("Container_zhanliduibi");
            InitCategory();
        }

        private void InitDetailView()
        {
            _playerInfoCategory.SelectedIndex = 0;
            OnSelectedInfoList(_playerInfoCategory, 0);
        }

        private void AddEventListener()
        {
            _playerInfoCategory.onSelectedIndexChanged.AddListener(OnSelectedInfoList);
        }

        private void RemoveEventListener()
        {
            _playerInfoCategory.onSelectedIndexChanged.RemoveListener(OnSelectedInfoList);
        }

        private void InitCategory()
        {
            List<CategoryItemData> listToggleData = rank_helper.GetRankingDetailCategoryNames();
            _playerInfoCategory.RemoveAll();
            _playerInfoCategory.SetDataList<CategoryListItem>(listToggleData);
        }

        private void OnSelectedInfoList(CategoryList target, int index)
        {
            _playerRoleInfoView.Hide();
            _playerGemView.Hide();
            _playerRuneView.Hide();
            _playerFightInfoView.Hide();
            _rankPlayerFightCompareView.Visible = false;
            switch (index)
            {
                case 0:
                    _playerRoleInfoView.Show(playerDbId);
                    break;
                case 1:
                    _rankPlayerFightCompareView.Visible = true;
                    _rankPlayerFightCompareView.Show(playerDbId);
                    break;
                case 2:
                    _playerGemView.Show(playerDbId);
                    break;
                case 3:
                    _playerRuneView.Show(playerDbId);
                    break;
                case 4:
                    _playerFightInfoView.Show(playerDbId);
                    break;
            }
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.RankingListDetail; }
        }

        public override void OnShow(object data)
        {
            this.playerDbId = (ulong)data;
            UIManager.Instance.DisablePanelCanvas(PanelIdEnum.RankingList);
            UIManager.Instance.DisablePanelCanvas(PanelIdEnum.DemonGate);
            InitDetailView();
            AddEventListener();
        }

        public override void OnClose()
        {
            CloseAllSubView();
            UIManager.Instance.EnablePanelCanvas(PanelIdEnum.RankingList);
            UIManager.Instance.EnablePanelCanvas(PanelIdEnum.DemonGate);
            RemoveEventListener();
        }

        private void CloseAllSubView()
        {
            _playerRuneView.Hide();
            _playerGemView.Hide();
            _playerRoleInfoView.Hide();
            _playerFightInfoView.Hide();
        }
    }
}