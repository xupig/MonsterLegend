﻿#region 模块信息
/*==========================================
// 文件名：RankingListPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleRankingList
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/4/15 10:10:03
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.ExtendComponent.GeneralRankingList;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;

namespace ModuleRankingList
{
    public class RankingListPanel : BasePanel
    {
        private CategoryList _rankingTypeCategory;
        private GeneralRankingList _rankingListContainer;

        // 存储排行榜类型的列表
        private List<int> _categoryList;
        
        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _rankingTypeCategory = GetChildComponent<CategoryList>("Container_panghangbangxianshi/List_categoryList");
            _rankingListContainer = AddChildComponent<GeneralRankingList>("Container_panghangbangxianshi/Container_zongbang");
        }

        private void AddEventListener()
        {
            _rankingTypeCategory.onSelectedIndexChanged.AddListener(OnRankingTypeChanged);
        }

        private void RemoveEventListener()
        {
            _rankingTypeCategory.onSelectedIndexChanged.RemoveListener(OnRankingTypeChanged);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.RankingList; }
        }

        public override void OnShow(object data)
        {
            int index = 0;
            if (data != null && data is int)
            {
                index = Convert.ToInt32(data);
            }
            if (data is int[])
            {
                index = (data as int[])[1] - 1;
            }
            SetCategoryList();
            SetCategoryName();
            Refresh(index);
            AddEventListener();
        }

        private void SetCategoryList()
        {
            _categoryList = rank_helper.GetRankCategory();
        }

        private void SetCategoryName()
        {
            List<CategoryItemData> listToggleData = new List<CategoryItemData>();
            List<int> hideList = new List<int>();
            for (int i = 0; i < _categoryList.Count; i++)
            {
                listToggleData.Add(CategoryItemData.GetCategoryItemData(rank_helper.GetRankingListName(_categoryList[i])));
                if (PlayerAvatar.Player.level < rank_helper.GetRankingListLevel(_categoryList[i]))
                {
                    hideList.Add(_categoryList[i] - 1);
                }
            }
            _rankingTypeCategory.RemoveAll();
            _rankingTypeCategory.SetDataList<CategoryListItem>(listToggleData);
            _rankingTypeCategory.SetHiddenList(hideList);
        }

        private void Refresh(int index)
        {
            _rankingListContainer.Refresh((RankType)_categoryList[index]);
            _rankingTypeCategory.SelectedIndex = index;
        }

        private void OnRankingTypeChanged(CategoryList target, int index)
        {
            RankItem.rankingListType = (RankType)_categoryList[index];
            _rankingListContainer.Refresh((RankType)_categoryList[index]);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }
    }
}