﻿#region 模块信息
/*==========================================
// 文件名：PlayerRuneItemData
// 命名空间: GameLogic.GameLogic.Modules.ModuleRankingList.Data
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/5/15 16:51:54
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs;
using System.Collections.Generic;
namespace Common.Data
{
    public class PlayerRuneItemData
    {
        public int runePageIndex;

        public List<RuneItemInfo> runeItemInfoList;

        public PlayerRuneItemData(int runePageIndex)
        {
            this.runePageIndex = runePageIndex;
            this.runeItemInfoList = new List<RuneItemInfo>();
        }
    }
}