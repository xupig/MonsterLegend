﻿#region 模块信息
/*==========================================
// 文件名：PlayerRuneView
// 命名空间: GameLogic.GameLogic.Modules.ModuleRankingList.ChildView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/4/18 13:53:14
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using System;

namespace ModuleRankingList
{
    public class RankPlayerRuneView : KContainer
    {
        private KScrollView _runeScrollView;
        private KList _runeList;

        private UInt64 playerDbId;

        protected override void Awake()
        {
            _runeScrollView = GetChildComponent<KScrollView>("ScrollView_fuwen");
            _runeList = _runeScrollView.GetChildComponent<KList>("mask/content");
            InitRuneListView();
        }

        private void InitRuneListView()
        {
            _runeList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _runeList.SetGap(-10, 0);
        }

        public void Hide()
        {
            if (this.Visible == true)
            {
                this.Visible = false;
            }
        }

        public void Show(ulong playerDbId)
        {
            Visible = true;
            this.playerDbId = playerDbId;
            InitRuneViewData();
        }

        private void InitRuneViewData()
        {
            _runeScrollView.ResetContentPosition();
            _runeList.RemoveAll();
            _runeList.SetDataList<RankPlayerRuneItem>(RankingListManager.Instance.rankInfo.playerRuneList);
            TweenListEntry.Begin(_runeList.gameObject);
        }

    }
}