﻿using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleRankingList
{
    public class GuildFightView : KContainer
    {
        private ulong currentPlayerId;
        private KScrollView _guildFightScrollView;
        private KPageableList _guildFightList;
        private StateText _noDataTxt;
        private List<PbGuildPkLog> dataList; 

        protected override void Awake()
        {
            _guildFightScrollView = GetChildComponent<KScrollView>("ScrollView_zhanlibang");
            _guildFightScrollView.ScrollRect.onRequestNext.AddListener(OnShowMoreData);
            _guildFightList = _guildFightScrollView.GetChildComponent<KPageableList>("mask/content");
            _guildFightList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _noDataTxt = GetChildComponent<StateText>("Label_txtZanwujilu");
            InitTitleTxt();
        }

        private void InitTitleTxt()
        {
            _noDataTxt.CurrentText.text = MogoLanguageUtil.GetContent(65323);
            KContainer titleContainer = GetChildComponent<KContainer>("Container_txtTitle");
            StateText txtTime = titleContainer.GetChildComponent<StateText>("Label_text01");
            StateText txtType = titleContainer.GetChildComponent<StateText>("Label_text02");
            StateText txtOpponentName = titleContainer.GetChildComponent<StateText>("Label_text03");
            StateText txtMilitaryRank = titleContainer.GetChildComponent<StateText>("Label_text04");
            StateText fightForceTxt = titleContainer.GetChildComponent<StateText>("Label_text05");
            StateText fightResult = titleContainer.GetChildComponent<StateText>("Label_text06");
            txtTime.CurrentText.text = MogoLanguageUtil.GetContent(65329);
            txtType.CurrentText.text = MogoLanguageUtil.GetContent(65330);
            txtOpponentName.CurrentText.text = MogoLanguageUtil.GetContent(65331);
            txtMilitaryRank.CurrentText.text = MogoLanguageUtil.GetContent(65332);
            fightForceTxt.CurrentText.text = MogoLanguageUtil.GetContent(65333);
            fightResult.CurrentText.text = MogoLanguageUtil.GetContent(65334);
        }

        private void OnShowMoreData(KScrollRect scrollRect)
        {
            if (_guildFightList.ItemList.Count >= dataList.Count)
            {
                return;
            }
            for (int i = _guildFightList.ItemList.Count; i < dataList.Count && i < _guildFightList.ItemList.Count + 10; i++)
            {
                _guildFightList.AddItem<GuildFightLogItem>(dataList[i]);
            }
        }

        private void RefreshDataList(PbGuildPkLogList guildPkLogData)
        {
            currentPlayerId = guildPkLogData.dbid;
            dataList = guildPkLogData.list;
            dataList.Sort(SortFunction);
            if (dataList == null || dataList.Count == 0)
            {
                _guildFightList.Visible = false;
                _noDataTxt.CurrentText.text = MogoLanguageUtil.GetContent(65323);
            }
            else
            {
                _noDataTxt.CurrentText.text = string.Empty;
                _guildFightList.Visible = true;
                int min = Mathf.Min(dataList.Count, 10);
                _guildFightList.SetDataList<GuildFightLogItem>(dataList.GetRange(0, min), 2);
            }
        }

        private int SortFunction(PbGuildPkLog x, PbGuildPkLog y)
        {
            return (int)y.date - (int)x.date;
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<PbGuildPkLogList>(RankingListEvents.GET_RANK_PLAYER_GUILD_DATA, RefreshDataList);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<PbGuildPkLogList>(RankingListEvents.GET_RANK_PLAYER_GUILD_DATA, RefreshDataList);
        }

        public void Show(ulong currentPlayerDbId)
        {
            Visible = true;
            RankingListManager.Instance.RequestPlayerGuildData(currentPlayerDbId);
        }
    }
}
