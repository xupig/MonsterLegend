﻿using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleRankingList
{
    public class DuelRecordView : KContainer
    {
        private KScrollView _scrollView;
        private KList _list;
        private StateText _textNoRecord;

        protected override void Awake()
        {
            _scrollView = GetChildComponent<KScrollView>("ScrollView_zhanlibang");
            _list = GetChildComponent<KList>("ScrollView_zhanlibang/mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _list.SetGap(0, 0);

            _textNoRecord = GetChildComponent<StateText>("Label_txtZanwujilu");
        }

        protected override void OnEnable()
        {
            EventDispatcher.AddEventListener<PbDuelReport>(DuelEvents.ResponseRecord, OnRefresh);
        }

        protected override void OnDisable()
        {
            EventDispatcher.RemoveEventListener<PbDuelReport>(DuelEvents.ResponseRecord, OnRefresh);
        }


        public void Show(ulong currentPlayerDbId)
        {
            Visible = true;
            DuelManager.Instance.RequestDuelReport(currentPlayerDbId);
        }

        private void OnRefresh(PbDuelReport pb)
        {
            if (pb.record.Count == 0)
            {
                _textNoRecord.Visible = true;
            }
            else
            {
                _textNoRecord.Visible = false;
            }
            _list.SetDataList<RecordItem>(pb.record, 1);
        }

        class RecordItem : KList.KListItemBase
        {
            private StateText _textTime;
            private StateText _textName;
            private StateText _textDivision;
            private StateText _textFight;
            private StateImage _imageWin;
            private StateImage _imageLose;
            private StateImage _imageDraw;


            protected override void Awake()
            {
                _textTime = GetChildComponent<StateText>("Label_txtShijian");
                _textName = GetChildComponent<StateText>("Label_txtDuishouMingcheng");
                _textFight = GetChildComponent<StateText>("Label_txtDuanwei");
                _textDivision = GetChildComponent<StateText>("Label_txtZhanli");
                _imageWin = GetChildComponent<StateImage>("Image_sheng");
                _imageLose = GetChildComponent<StateImage>("Image_fu");
                _imageDraw = GetChildComponent<StateImage>("Image_ping");
            }

            public override object Data
            {
                set
                {
                    PbDuelMatchRecord pb = value as PbDuelMatchRecord;
                    _textTime.CurrentText.text = MogoTimeUtil.ServerTimeStamp2LocalTime(pb.duel_time).ToString("MM-dd hh:mm");
                    _textName.CurrentText.text = MogoProtoUtils.ParseByteArrToString(pb.other_name_bytes);
                    _textDivision.CurrentText.text = string.Format("{0}：{1}", 
                        duel_rank_reward_helper.GetNameInReward((int)pb.self_stage_id), duel_rank_reward_helper.GetNameInReward((int)pb.other_stage_id));
                    _textFight.CurrentText.text = string.Format("{0}：{1}", pb.self_fight_force, pb.other_fight_force);
                    _imageWin.Visible = false;
                    _imageLose.Visible = false;
                    _imageDraw.Visible = false;
                    switch (pb.match_result)
                    {
                        case 0:
                            _imageLose.Visible = true;
                            break;
                        case 1:
                            _imageWin.Visible = true;
                            break;
                        case 2:
                            _imageDraw.Visible = true;
                            break;
                    }
                }
            }

            public override void Dispose()
            {
            }
        }
    }
}
