﻿using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
#region 模块信息
/*==========================================
// 文件名：RankPlayerFightView
// 命名空间: GameLogic.GameLogic.Modules.ModuleRankingList.ChildView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/4/11 9:32:56
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace ModuleRankingList
{
    public class RankPlayerFightForceView : KContainer
    {
        private StateText _myFightForceTxt;
        private StateText _otherFightForceTxt;

        FightDescItem[] fightDescArray = new FightDescItem[4];

        protected override void Awake()
        {
            _myFightForceTxt = GetChildComponent<StateText>("Label_wodezhanli");
            _otherFightForceTxt = GetChildComponent<StateText>("Label_tadezhanli");

            KContainer _fightForceContainer = GetChildComponent<KContainer>("Container_duibi");
            for (int i = 0; i < fightDescArray.Length; i++)
            {
                fightDescArray[i] = _fightForceContainer.AddChildComponent<FightDescItem>("Container_item0" + i);
            }
        }

        private void RefreshCompareView(List<PbSysFightForce> pbFightForceInfoList)
        {
            if (pbFightForceInfoList == null)
            {
                return;
            }
            fightDescArray[0].Refresh(1, pbFightForceInfoList[0].equipment_fight_force, pbFightForceInfoList[1].equipment_fight_force);
            fightDescArray[1].Refresh(2, pbFightForceInfoList[0].strengthen_fight_force, pbFightForceInfoList[1].strengthen_fight_force);
            fightDescArray[2].Refresh(3, pbFightForceInfoList[0].enchant_fight_force, pbFightForceInfoList[1].enchant_fight_force);
            fightDescArray[3].Refresh(4, pbFightForceInfoList[0].equip_suit_fight_force, pbFightForceInfoList[1].equip_suit_fight_force);
        }

        public void Show(ulong playerDbId)
        {
            RefreshCompareView(PlayerDataManager.Instance.rankInfo.FightForceCompareInfo);
        }
    }
}