﻿#region 模块信息
/*==========================================
// 文件名：PlayerGemView
// 命名空间: GameLogic.GameLogic.Modules.ModuleRankingList.ChildView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/4/18 13:52:59
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
namespace ModuleRankingList
{
    public class RankPlayerGemView : KContainer
    {
        private KScrollView _gemScrollView;
        private KList _gemList;

        private UInt64 playerDbId;

        protected override void Awake()
        {
            _gemScrollView = GetChildComponent<KScrollView>("ScrollView_baoshi");
            _gemList = _gemScrollView.GetChildComponent<KList>("mask/content");
            InitGemList();
        }

        private void InitGemList()
        {
            _gemList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _gemList.SetGap(-10, 0);
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(RankingListEvents.GET_RANK_PLAYER_GEM_INFO, RefreshRankPlayerGemList);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(RankingListEvents.GET_RANK_PLAYER_GEM_INFO, RefreshRankPlayerGemList);
        }

        private void RefreshRankPlayerGemList()
        {
            _gemScrollView.ResetContentPosition();
            _gemList.RemoveAll();
            _gemList.SetDataList<RankPlayerGemItem>(RankingListManager.Instance.rankInfo.playerEquipBagList.items);
            TweenListEntry.Begin(_gemList.gameObject);
        }

        public void Hide()
        {
            if (this.Visible == true)
            {
                this.Visible = false;
            }
        }

        public void Show(ulong playerDbId)
        {
            Visible = true;
            this.playerDbId = playerDbId;
            RefreshRankPlayerGemList();
        }
      
    }
}