﻿#region 模块信息
/*==========================================
// 文件名：RankPlayerFightInfoView
// 命名空间: GameLogic.GameLogic.Modules.ModuleRankingList.ChildView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/12/7 11:06:04
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using UnityEngine;

namespace ModuleRankingList
{
    public class RankPlayerFightInfoView : KContainer
    {
        private KToggleGroup _toggleGroup;
        private PKFightView _pkFightView;
        private GuildFightView _guildFightView;
        private DuelRecordView _duelRecordView;
        private const int GUILD_INDEX = 0;
        private const int DUEL_INDEX = 1;
        private const int PK_INDEX = 2;
        private ulong currentPlayerDbId;

        protected override void Awake()
        {
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_zhanji");
            _pkFightView = AddChildComponent<PKFightView>("Container_PKjilu");
            _guildFightView = AddChildComponent<GuildFightView>("Container_gonghuizhanzhanji");
            _duelRecordView = AddChildComponent<DuelRecordView>("Container_juedou");
            AddEventListener();
        }

        private void AddEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChange);
        }

        public void Show(ulong playerDbId)
        {
            Visible = true;
            currentPlayerDbId = playerDbId;
            int defaultIndex = 0;
            _toggleGroup.SelectIndex = defaultIndex;
            OnSelectedIndexChange(_toggleGroup, defaultIndex);
        }

        private void OnSelectedIndexChange(KToggleGroup toggleGroup, int index)
        {
            _duelRecordView.Visible = false;
            _pkFightView.Visible = false;
            _guildFightView.Visible = false;
            switch (index)
            {
                case GUILD_INDEX:
                    _guildFightView.Show(currentPlayerDbId);
                    break;
                case DUEL_INDEX:
                    _duelRecordView.Show(currentPlayerDbId);
                    break;
                case PK_INDEX:
                    _pkFightView.Show(currentPlayerDbId);
                    break;
            }
        }

        public void Hide()
        {
            Visible = false;
        }
    }
}
