﻿#region 模块信息
/*==========================================
// 文件名：PlayerRuneView
// 命名空间: GameLogic.GameLogic.Modules.ModuleRankingList.ChildView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/4/18 13:53:14
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Structs.Enum;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleRankingList
{
    public class RoleEquipView : KContainer
    {
        private Dictionary<int, RoleEquipItem> _equipItemDic;
        public int _prevSelectIndex;

        private ModelComponent _avatarModel;
        private KContainer _iconContainer;
        private RoleEquipItem _wingItem;
        private WingItemData wingData;

        protected override void Awake()
        {
            _iconContainer = GetChildComponent<KContainer>("Container_icons/Container_equipItem");
            _avatarModel = AddChildComponent<ModelComponent>("Container_roleModel");
            InitEquipItemDic();
        }

        private void InitEquipItemDic()
        {
            _equipItemDic = new Dictionary<int, RoleEquipItem>();
            Type enumType = typeof(EquipType);
            List<string> equipNames = GetEquipNamesList(Enum.GetNames(enumType));
            RoleEquipItem item;
            for (int i = 0; i < equipNames.Count; i++)
            {
                item = _iconContainer.AddChildComponent<RoleEquipItem>("Container_" + equipNames[i]);
                if (equipNames[i].Equals("wing"))
                {
                    _wingItem = item;
                }
                _equipItemDic.Add(ToEquipPosition(i), item);
                item.EquipPosition = ToEquipPosition(i);
            }
        }

        private List<string> GetEquipNamesList(string[] equipNames)
        {
            List<string> result = new List<string>();
            for (int i = 0; i < equipNames.Length; i++)
            {
                if (equipNames[i] == "ring")
                {
                    result.Add("leftRing");
                    result.Add("rightRing");
                }
                else
                {
                    result.Add(equipNames[i]);
                }
            }
            return result;
        }

        public void SetAvatarModule(Vocation vocation, string equipInfo)
        {
            _avatarModel.SetStartPosition(new Vector3(0, 0, 1000));
            _avatarModel.LoadAvatarModel(vocation, equipInfo);
        }

        private void OnEquipItemClicked(int index)
        {
            if (index == (int)EquipType.wing)
            {
                ToolTipsDataWrapper wrapper = new ToolTipsDataWrapper(wingData.wingId, PanelIdEnum.RankingListDetail);
                UIManager.Instance.ShowPanel(PanelIdEnum.WingToolTips, wrapper);
            }
            else if (index == (int)EquipType.love)
            {

            }
            else if (_equipItemDic.ContainsKey(index))
            {
                SelectEquipItem(index);
            }
        }

        private void SelectEquipItem(int index)
        {
            if (_prevSelectIndex != 0)
            {
                _equipItemDic[_prevSelectIndex].SetSelected(false);
            }
            _prevSelectIndex = index;
            _equipItemDic[_prevSelectIndex].SetSelected(true);
        }

        private int ToEquipPosition(int index)
        {
            return index + 1;
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<PbODWingBody>(RankingListEvents.GET_RANK_PLAYER_WING_DATA, RefreshWingItem);
            EventDispatcher.AddEventListener<PbBagList>(RankingListEvents.GET_RANK_PLAYER_EQUIP_INFO, RefreshEquip);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<PbODWingBody>(RankingListEvents.GET_RANK_PLAYER_WING_DATA, RefreshWingItem);
            EventDispatcher.RemoveEventListener<PbBagList>(RankingListEvents.GET_RANK_PLAYER_EQUIP_INFO, RefreshEquip);
        }

        private void RefreshWingItem(PbODWingBody pbOdWingBody)
        {
            if (pbOdWingBody.wing_id == 0)
            {
                return;
            }
            _wingItem.ClearEquip();
            wingData = new WingItemData(pbOdWingBody.wing_id);
            _wingItem.SetWing(wingData);
            _wingItem.onClick.AddListener(OnEquipItemClicked);
        }
        private void RefreshEquip(PbBagList pbBagList)
        {
            foreach (KeyValuePair<int, RoleEquipItem> pair in _equipItemDic)
            {
                pair.Value.ClearEquip();
                pair.Value.onClick.RemoveListener(OnEquipItemClicked);
            }

            if (pbBagList.bag_id == (int)BagType.BodyEquipBag)
            {
                OnBodyEquipBagUpdate(BagType.BodyEquipBag, pbBagList.items);
            }
        }

        protected override void OnDestroy()
        {
            foreach (KeyValuePair<int, RoleEquipItem> pair in _equipItemDic)
            {
                pair.Value.onClick.RemoveListener(OnEquipItemClicked);
            }
        }

        private void OnBodyEquipBagUpdate(BagType type, List<PbGridItem> pbGridItemList)
        {
            for (int i = 0; i < pbGridItemList.Count; i++)
            {
                PbGridItem pbGridItem = pbGridItemList[i];
                _equipItemDic[(int)pbGridItem.grid_index].SetEquip(new EquipItemInfo(pbGridItem));
                _equipItemDic[(int)pbGridItem.grid_index].onClick.AddListener(OnEquipItemClicked);
            }
        }

        public void Hide()
        {
            if (this.Visible == true)
            {
                RemoveEventListener();
                this.Visible = false;
            }
        }

        public void Show(ulong playerDbId)
        {
            this.Visible = true;
            RankingListManager.Instance.GetRankPlayerEquipInfo(playerDbId);
            RankingListManager.Instance.GetRankPlayerWingData(playerDbId);
            AddEventListener();
        }
    }
}
