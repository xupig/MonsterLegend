﻿using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleRankingList
{
    public class PKFightView : KContainer
    {
        private ulong currentPlayerId;
        private KScrollView _pkFightScrollView;
        private KPageableList _pkFightList;
        private StateText _noDataTxt;
        private List<PbPkLog> dataList;

        protected override void Awake()
        {
            _pkFightScrollView = GetChildComponent<KScrollView>("ScrollView_zhanlibang");
            _pkFightScrollView.ScrollRect.onRequestNext.AddListener(OnShowMoreData);
            _pkFightList = _pkFightScrollView.GetChildComponent<KPageableList>("mask/content");
            _pkFightList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _noDataTxt = GetChildComponent<StateText>("Label_txtZanwuPK");
            InitTitleTxt();
        }

        private void InitTitleTxt()
        {
            _noDataTxt.CurrentText.text = MogoLanguageUtil.GetContent(65326);
            KContainer titleContainer = GetChildComponent<KContainer>("Container_txtTitle");
            StateText timeTxt = titleContainer.GetChildComponent<StateText>("Label_text01");
            StateText txtOpponentName = titleContainer.GetChildComponent<StateText>("Label_text02");
            StateText fightForceTxt = titleContainer.GetChildComponent<StateText>("Label_text03");
            StateText fightResult = titleContainer.GetChildComponent<StateText>("Label_text04");
            timeTxt.CurrentText.text = MogoLanguageUtil.GetContent(65336);
            txtOpponentName.CurrentText.text = MogoLanguageUtil.GetContent(65337);
            fightForceTxt.CurrentText.text = MogoLanguageUtil.GetContent(65338);
            fightResult.CurrentText.text = MogoLanguageUtil.GetContent(65339);
        }

        private void OnShowMoreData(KScrollRect scrollRect)
        {
            if (_pkFightList.ItemList.Count >= dataList.Count)
            {
                return;
            }
            for (int i = _pkFightList.ItemList.Count; i < dataList.Count && i < _pkFightList.ItemList.Count + 10; i++)
            {
                _pkFightList.AddItem<PKLogItem>(dataList[i]);
            }
        }

        private void RefreshDataList(PbPkLogList pkLogData)
        {
            currentPlayerId = pkLogData.dbid;
            dataList = pkLogData.list;
            dataList.Sort(SortFunction);
            if (dataList == null && dataList.Count == 0)
            {
                _noDataTxt.CurrentText.text = MogoLanguageUtil.GetContent(65326);
                _pkFightList.Visible = false;
            }
            else
            {
                
                _noDataTxt.CurrentText.text = string.Empty;
                _pkFightList.Visible = true;
                int min = Mathf.Min(dataList.Count, 10);
                _pkFightList.SetDataList<PKLogItem>(dataList.GetRange(0, min), 2);
            }
        }

        private int SortFunction(PbPkLog x, PbPkLog y)
        {
            return (int)y.date - (int)x.date;
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<PbPkLogList>(RankingListEvents.GET_RANK_PLAYER_PK_DATA, RefreshDataList);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<PbPkLogList>(RankingListEvents.GET_RANK_PLAYER_PK_DATA, RefreshDataList);
        }

        public void Show(ulong currentPlayerDbId)
        {
            Visible = true;
            RankingListManager.Instance.RequestPlayerPKData(currentPlayerDbId);
        }
    }
}
