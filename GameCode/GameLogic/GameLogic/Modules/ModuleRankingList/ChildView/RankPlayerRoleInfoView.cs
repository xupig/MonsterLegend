﻿#region 模块信息
/*==========================================
// 文件名：RoleInfoView
// 命名空间: GameLogic.GameLogic.Modules.ModuleRankingList.ChildView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/4/18 13:22:57
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.ExtendComponent.GeneralRankingList;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Utils;
using System.Collections.Generic;
using Common.ExtendComponent;
using UnityEngine;
using Game.UI;
using GameData;

namespace ModuleRankingList
{
    public class AttributeContainer : KContainer
    {
        private StateText _attributeName;
        private StateText _attributeValue;

        protected override void Awake()
        {
            _attributeName = GetChildComponent<StateText>("Label_txtName");
            _attributeValue = GetChildComponent<StateText>("Label_txtValue");
        }

        public void SetValue(int attributeId, int value)
        {
            _attributeName.CurrentText.text = attri_config_helper.GetAttributeName(attributeId);
            _attributeValue.CurrentText.text = value.ToString();
        }

        public void SetValue(string attributeName, string value)
        {
            _attributeName.CurrentText.text = attributeName;
            _attributeValue.CurrentText.text = value;
        }
    }


    public class RankPlayerRoleInfoView : KContainer
    {
        private RoleEquipView _roleEquipView;
        private RoleAttributeDetailView _roleAttributeDetailView;
        private GameObject maskGo;
        private KButton _worshipBtn;
        private StateText _worshipBtnTxt;
        private KButton _addAsFriendBtn;
        private KButton _detailInfoBtn;
        private KButton _backBtn;
        private KContainer _roleInfoContainer;
        private KContainer _roleAttributeContainer;
        private StateText _worshipTxt;

        private AttributeContainer[] _AttrubuteContainer = new AttributeContainer[6];
        private StateText _nameTxt;
        private StateText _titleTxt;
        private string titleTemplate;
        private ArtNumberList _fightForceContainer;
        private KParticle _worshipParticle;

        private Vector3 startPosition = new Vector3(0, -10, 0);
        private Vector3 endPosition = new Vector3(600, -10, 0);

        public string worshipTemplate;
        public ulong playerDbId;

        protected override void Awake()
        {
            _roleEquipView = AddChildComponent<RoleEquipView>("Container_equipView");
            _roleAttributeDetailView = AddChildComponent<RoleAttributeDetailView>("Container_xiangxixinxi");
            _worshipBtn = GetChildComponent<KButton>("Button_chongbaita");
            _addAsFriendBtn = GetChildComponent<KButton>("Button_jiaweihaoyou");
            _detailInfoBtn = GetChildComponent<KButton>("Container_wanjiaxinxi/Button_xiangxixinxi");
            _backBtn = GetChildComponent<KButton>("Container_wanjiaxinxi/Button_fanhui");
            _backBtn.Visible = false;
            _roleInfoContainer = GetChildComponent<KContainer>("Container_jiaosexinxi");
            _roleAttributeContainer = GetChildComponent<KContainer>("Container_wanjiaxinxi");
            // 玩家相关属性
            for (int i = 0; i < _AttrubuteContainer.Length; i++)
            {
                _AttrubuteContainer[i] = _roleAttributeContainer.AddChildComponent<AttributeContainer>("Container_attr" + i);
            }
            _worshipTxt = GetChildComponent<StateText>("Label_txtChongbaizhi");
            _worshipBtnTxt = GetChildComponent<StateText>("Button_chongbaita/label");
            _nameTxt = _roleInfoContainer.GetChildComponent<StateText>("Container_miaoshu/Label_txtName");
            _titleTxt = _roleInfoContainer.GetChildComponent<StateText>("Container_miaoshu/Label_txtchenghao");
            titleTemplate = _titleTxt.CurrentText.text;
            worshipTemplate = _worshipTxt.CurrentText.text;
            _fightForceContainer = AddChildComponent<ArtNumberList>("Container_jiaosexinxi/Container_miaoshu/Container_shengming/List_zhandouli");
            maskGo = GetChild("ScaleImage_mask");
            _worshipParticle = AddChildComponent<KParticle>("fx_ui_29_chongbaita");
            _worshipParticle.transform.localPosition = new Vector3(648, -504, 0);
            InitView();
        }

        public void OnPlayerInfoDetailPanelHide()
        {
            _worshipTxt.Visible = false;
            _worshipBtn.Visible = false;
            _addAsFriendBtn.Visible = false;
        }

        private void InitView()
        {
            _roleAttributeDetailView.Hide();
            _roleAttributeDetailView.transform.SetParent(maskGo.transform);
            _worshipTxt.CurrentText.text = string.Format(worshipTemplate, RankingListManager.Instance.rankInfo.hasWorshipTimes, RankingListManager.Instance.rankInfo.MaxWorshipCount);
            _worshipParticle.Stop();
        }

        private void AddEventListener()
        {
            _worshipBtn.onClick.AddListener(OnWorshipBtnClicked);
            _detailInfoBtn.onClick.AddListener(OnDetailInfoBtnClicked);
            _backBtn.onClick.AddListener(OnBackBtnClicked);
            _addAsFriendBtn.onClick.AddListener(OnAddAsFriendBtnClicked);
            EventDispatcher.AddEventListener<PbOfflineData>(RankingListEvents.GET_RANK_PLAYER_ATTRIBUTE_INFO, RefreshRankPlayerAttribute);
            EventDispatcher.AddEventListener<PbOfflineData>(RankingListEvents.GET_RANK_PLAYER_GENERAL_INFO, RefreshRankPlayerGeneralInfo);
            EventDispatcher.AddEventListener<PbRankInfo>(RankingListEvents.GET_RANK_WORSHIP_RESULT, RefreshWorshipTxt);
        }

        private void OnBackBtnClicked()
        {
            TweenPosition.Begin(_roleAttributeDetailView.gameObject, 0.3f, endPosition, TweenEnd);
            _backBtn.Visible = false;
        }

        private void RefreshWorshipTxt(PbRankInfo pbRankInfo)
        {
            _worshipTxt.CurrentText.text = string.Format(worshipTemplate, pbRankInfo.worship_count, RankingListManager.Instance.rankInfo.MaxWorshipCount);
            _worshipParticle.Play();
        }

        private void RemoveEventListener()
        {
            _worshipBtn.onClick.RemoveListener(OnWorshipBtnClicked);
            _addAsFriendBtn.onClick.RemoveListener(OnAddAsFriendBtnClicked);
            _backBtn.onClick.RemoveListener(OnBackBtnClicked);
            _detailInfoBtn.onClick.RemoveListener(OnDetailInfoBtnClicked);
            EventDispatcher.RemoveEventListener<PbOfflineData>(RankingListEvents.GET_RANK_PLAYER_ATTRIBUTE_INFO, RefreshRankPlayerAttribute);
            EventDispatcher.RemoveEventListener<PbOfflineData>(RankingListEvents.GET_RANK_PLAYER_GENERAL_INFO, RefreshRankPlayerGeneralInfo);
            EventDispatcher.RemoveEventListener<PbRankInfo>(RankingListEvents.GET_RANK_WORSHIP_RESULT, RefreshWorshipTxt);
        }

        private void RefreshRankPlayerGeneralInfo(PbOfflineData pbOfflineData)
        {
            if (pbOfflineData.title_id != 0)
            {
                _titleTxt.CurrentText.text = string.Format(titleTemplate, title_helper.GetTitleName(pbOfflineData.title_id));
            }
            else
            {
                _titleTxt.CurrentText.text = string.Format(titleTemplate, MogoLanguageUtil.GetContent(63508));
            }
            string levelString = string.Concat("LV", pbOfflineData.level.ToString());
            _nameTxt.CurrentText.text = string.Format("{0} {1}", MogoProtoUtils.ParseByteArrToString(pbOfflineData.name_bytes), levelString);
            _fightForceContainer.SetNumber(pbOfflineData.fight_force);
            pbOfflineData.facade = MogoProtoUtils.ParseByteArrToString(pbOfflineData.facade_bytes);
            _roleEquipView.SetAvatarModule((Vocation)pbOfflineData.vocation, pbOfflineData.facade);
        }

        private void RefreshRankPlayerAttribute(PbOfflineData pbOfflineData)
        {
            Dictionary<int, PbAttri> attriDict = new Dictionary<int, PbAttri>();
            int dmgMax = 0, dmgMin = 0;
            List<PbAttri> pbAttriList = pbOfflineData.battle_attries;
            for (int i = 0; i < pbOfflineData.battle_attries.Count; i++)
            {
                attriDict.Add((int)pbOfflineData.battle_attries[i].atrri_id, pbOfflineData.battle_attries[i]);
            }
            PbAttri pbAttri;
            if (attriDict.ContainsKey((int)fight_attri_config.STAMINA))
            {
                pbAttri = attriDict[(int)fight_attri_config.STAMINA];
                _AttrubuteContainer[1].SetValue((int)pbAttri.atrri_id, pbAttri.attri_value);
            }
            else
            {
                _AttrubuteContainer[1].SetValue((int)fight_attri_config.STAMINA, 0);
            }
            if (attriDict.ContainsKey((int)fight_attri_config.HP_MAX))
            {
                pbAttri = attriDict[(int)fight_attri_config.HP_MAX];
                _AttrubuteContainer[2].SetValue(MogoLanguageUtil.GetContent(3101), pbAttri.attri_value.ToString());
            }
            else
            {
                _AttrubuteContainer[2].SetValue((int)fight_attri_config.HP_MAX, 0);
            }
            if (attriDict.ContainsKey((int)fight_attri_config.DMG_MAX))
            {
                pbAttri = attriDict[(int)fight_attri_config.DMG_MAX];
                dmgMax = pbAttri.attri_value;
            }
            if (attriDict.ContainsKey((int)fight_attri_config.DMG_MIN))
            {
                pbAttri = attriDict[(int)fight_attri_config.DMG_MIN];
                dmgMin = pbAttri.attri_value;
            }
            if (attriDict.ContainsKey((int)fight_attri_config.PHY_DEFENSE))
            {
                pbAttri = attriDict[(int)fight_attri_config.PHY_DEFENSE];
                _AttrubuteContainer[4].SetValue((int)pbAttri.atrri_id, pbAttri.attri_value);
            }
            else
            {
                _AttrubuteContainer[4].SetValue((int)fight_attri_config.PHY_DEFENSE, 0);
            }
            if (attriDict.ContainsKey((int)fight_attri_config.MAG_DEFENSE))
            {
                pbAttri = attriDict[(int)fight_attri_config.MAG_DEFENSE];
                _AttrubuteContainer[5].SetValue((int)pbAttri.atrri_id, pbAttri.attri_value);
            }
            else
            {
                _AttrubuteContainer[5].SetValue((int)fight_attri_config.MAG_DEFENSE, 0);
            }
            switch (pbOfflineData.vocation)
            {
                case 1:
                    if (attriDict.ContainsKey((int)fight_attri_config.STRENGTH))
                    {
                        pbAttri = attriDict[(int)fight_attri_config.STRENGTH];
                        _AttrubuteContainer[0].SetValue((int)pbAttri.atrri_id, pbAttri.attri_value);
                    }
                    else
                    {
                        _AttrubuteContainer[0].SetValue((int)fight_attri_config.STRENGTH, 0);
                    }
                    break;
                case 2:
                    if (attriDict.ContainsKey((int)fight_attri_config.DEXTERITY))
                    {
                        pbAttri = attriDict[(int)fight_attri_config.DEXTERITY];
                        _AttrubuteContainer[0].SetValue((int)pbAttri.atrri_id, pbAttri.attri_value);
                    }
                    else
                    {
                        _AttrubuteContainer[0].SetValue((int)fight_attri_config.DEXTERITY, 0);
                    }
                    break;
                case 3:
                    if (attriDict.ContainsKey((int)fight_attri_config.INTELLIGENC))
                    {
                        pbAttri = attriDict[(int)fight_attri_config.INTELLIGENC];
                        _AttrubuteContainer[0].SetValue((int)pbAttri.atrri_id, pbAttri.attri_value);
                    }
                    else
                    {
                        _AttrubuteContainer[0].SetValue((int)fight_attri_config.INTELLIGENC, 0);
                    }
                    break;
                case 4:
                    if (attriDict.ContainsKey((int)fight_attri_config.CONCENTRATE))
                    {
                        pbAttri = attriDict[(int)fight_attri_config.CONCENTRATE];
                        _AttrubuteContainer[0].SetValue((int)pbAttri.atrri_id, pbAttri.attri_value);
                    }
                    else
                    {
                        _AttrubuteContainer[0].SetValue((int)fight_attri_config.CONCENTRATE, 0);
                    }
                    break;
            }
            string value = string.Concat(dmgMin, "-", dmgMax);
            _AttrubuteContainer[3].SetValue(MogoLanguageUtil.GetContent(3104), value);
        }

        private void OnDetailInfoBtnClicked()
        {
            _roleAttributeDetailView.Show();
            _roleEquipView.Hide();
            _roleAttributeDetailView.transform.localPosition = endPosition;
            TweenPosition.Begin(_roleAttributeDetailView.gameObject, 0.3f, startPosition);
            _backBtn.Visible = true;
        }

        private void TweenEnd(UITweener tween)
        {
            _roleAttributeDetailView.Hide();
            _roleEquipView.Show(playerDbId);
        }

        private void OnAddAsFriendBtnClicked()
        {
            PbFriendAvatarInfo friendAvatarInfo = new PbFriendAvatarInfo();
            friendAvatarInfo.dbid = playerDbId;
            FriendManager.Instance.AddFriend(friendAvatarInfo);
        }

        private void OnWorshipBtnClicked()
        {
            RankingListManager.Instance.RequestWorshipToPlayer((int)RankItem.rankingListType, playerDbId);
        }

        public void Hide()
        {
            if (this.Visible == true)
            {
                this.Visible = false;
                HideAllSubView();
                RemoveEventListener();
            }
        }

        private void HideAllSubView()
        {
            _roleEquipView.Hide();
            _roleAttributeDetailView.Hide();
        }

        public void Show(ulong playerDbId)
        {
            this.playerDbId = playerDbId;
            Visible = true;
            AddEventListener();
            RefreshView();
            if (MogoWorld.Player.dbid == playerDbId)
            {
                _addAsFriendBtn.SetButtonDisable();
            }
            else
            {
                _addAsFriendBtn.SetButtonActive();
            }
            RankingListManager.Instance.GetRankPlayerRoleInfo(playerDbId);
            RankingListManager.Instance.GetRankPlayerFightInfo(playerDbId);
        }

        private void SetButtonGray(GameObject go, float grayValue)
        {
            ImageWrapper[] imageWrappers = go.GetComponentsInChildren<ImageWrapper>();
            for (int i = 0; i < imageWrappers.Length; i++)
            {
                imageWrappers[i].SetGray(0f);
            }
        }

        private void RefreshView()
        {
            _backBtn.Visible = false;
            _roleEquipView.Show(playerDbId);
            _roleAttributeDetailView.Hide();
        }

    }
}