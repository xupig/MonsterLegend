﻿using Common.Data;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleRankingList
{
    class RoleAttributeDetailView : KContainer
    {
        private const int DETAIL_ATTRI_INDEX = 0;
        private const int SPECIAL_ATTRI_INDEX = 1;
        private const int ELEMENT_ATTRI_INDEX = 2;

        private KToggleGroup _tab;
        private KScrollView _infoScrollView;
        private KList _attributeList;

        protected override void Awake()
        {
            _tab = GetChildComponent<KToggleGroup>("ToggleGroup_category");
            _infoScrollView = GetChildComponent<KScrollView>("ScrollView_xinxi");
            _attributeList = _infoScrollView.GetChildComponent<KList>("mask/content");
            _attributeList.SetGap(5, 0);
            InitAttributeList();
            AddEventListener();
        }

        private void InitAttributeList()
        {
            _attributeList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        private void AddEventListener()
        {
            _tab.onSelectedIndexChanged.AddListener(OnTabIndexChanged);
        }

        private void OnTabIndexChanged(KToggleGroup target, int index)
        {
            _attributeList.RemoveAll();
            _infoScrollView.ResetContentPosition();
            switch (index)
            {
                case DETAIL_ATTRI_INDEX:
                    _attributeList.SetDataList<AttributeListItem>(RankingListManager.Instance.rankInfo.playerDetailAttributeList);
                    break;
                case SPECIAL_ATTRI_INDEX:
                    _attributeList.SetDataList<AttributeListItem>(RankingListManager.Instance.rankInfo.playerSpecialAttributeList);
                    break;
                case ELEMENT_ATTRI_INDEX:
                    _attributeList.SetDataList<AttributeListItem>(RankingListManager.Instance.rankInfo.playerElementAttributeList);
                    break;
            }
        }

        private void RemoveEventListener()
        {
            _tab.onSelectedIndexChanged.RemoveListener(OnTabIndexChanged);
        }

        public void Hide()
        {
            if (this.Visible == true)
            {
                Visible = false;
                RemoveEventListener();
            }
        }

        private void End(UITweener tween)
        {
            Visible = false;
        }

        public void Show()
        {
            Visible = true;
            AddEventListener();
            _tab.SelectIndex = DETAIL_ATTRI_INDEX;
            _attributeList.RemoveAll();
            _infoScrollView.ResetContentPosition();
            _attributeList.SetDataList<AttributeListItem>(RankingListManager.Instance.rankInfo.playerDetailAttributeList);
        }
    }
}
