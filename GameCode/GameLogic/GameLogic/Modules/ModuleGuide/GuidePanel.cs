﻿#region 模块信息
/*==========================================
// 文件名：GuideArrow
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/6/6 10:07:13
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleMainUI;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ModuleGuide
{
    public class GuidePanelInspector
    {
        public string url;
        public int pos;
    }

    public class GuidePanel : BasePanel
    {
        public static GuidePanelInspector inspector = new GuidePanelInspector();
        private GuideArrow _arrow;
        private GuideFrame _frame;
        private GuideContent _content;

        private string _url;
        private string url
        {
            get
            {
                return _url;
            }
            set
            {
                _url = value;
            }
        }
        private MethodInfo info;
        private object target;

       // private Vector3 _offset;
       // private Vector3 _offset2;
        private GameObject go;
        private int direction;

        public static bool isShow;

        private RectTransform _rect;
        private Vector2 _orignal;
        private Vector2 offsetVect = new Vector2(0, 2000);

        public static bool clickAndHide = true;
        private UIImageResizer _resizer;

        protected override void Awake()
        {
            base.Awake();
            _rect = GetComponent<RectTransform>();
            _orignal = _rect.anchoredPosition;
            _arrow = AddChildComponent<GuideArrow>("Container_guideArrow");
            _frame = AddChildComponent<GuideFrame>("Container_guideFrame");

            ModalMask = GetChildComponent<StateImage>("ScaleImage_guidesharedZhezhao");
            ModalMask.gameObject.AddComponent<KDummyButton>();
            _resizer = ModalMask.gameObject.GetComponent<UIImageResizer>();
            _frame.SetMask(this);
            _content = AddChildComponent<GuideContent>("Container_guideContent");
           
            //_offset = _content.Position - _frame.Position;
            _frame.onClick.AddListener(OnClick);
            _frame.onCallFunction.AddListener(OnCall);
            _frame.onLongClick.AddListener(OnLongClick);
        }


        private void OnClick(PointerEventData eventData)
        {
            try
            {
                if (info != null)
                {
                    //Debug.LogError("Before GuidePanel.Invoke:" + url + "," + info + "," + info.Name + "," + target.GetType().Name);
                    info.Invoke(target, new object[] { eventData });
                }
                if (clickAndHide)
                {
                    DoClose();
                }
            }
            catch(Exception ex)
            {
                DoClose();
            }
        }

        private void DoClose()
        {
            info = null;
            target = null;
            PanelIdEnum.GuideArrow.Close();
            DramaTrigger.ClickButton(url);
            url = null;
        }


        private void OnCall(string functionName,PointerEventData eventData)
        {
            if (functionName == "OnPointerDown")
            {
                _arrow.Visible = false;
                _content.Visible = false;
            }
            ControlStickView view = go.transform.parent.parent.GetComponent<ControlStickView>();
            MethodInfo callMethod = view.GetType().GetMethod(functionName);
            if (callMethod != null)
            {
                callMethod.Invoke(view, new object[] { eventData });
            }
        }

        private void OnLongClick(string functionName,PointerEventData evtData)
        {
            LongClickButton btn = go.GetComponent<LongClickButton>();
            MethodInfo callMethod = btn.GetType().GetMethod(functionName);
            if (callMethod != null)
            {
                callMethod.Invoke(btn, new object[] { evtData });
            }
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuideArrow; }
        }

        private Vector3 offset;

        private string[] waitingObject;
        private int count = 150;

        public override void OnShow(object data)
        {
            if (BaseModule.IsPanelShowing(PanelIdEnum.RewardBuffTip))
            {
                UIManager.Instance.ClosePanel(PanelIdEnum.RewardBuffTip);
            }
            string[] dataList = data as string[];
            ModalMask.Visible = true;

            isShow = true;
            if(waitingObject != null)
            {
                return;
            }
            clickAndHide = true;
            SetData(data);
            EventDispatcher.AddEventListener(PanelEvents.PANEL_LAYER_RESET_POS,OnResetPos);
        }

        private void UpdatePosition()
        {
            if(BaseModule.IsPanelCanvasEnable(BaseModule.currentGuidePanel))
            {
                _rect.anchoredPosition = _orignal;
                hasMovePos = false;
                inspector.pos = 0;
                _resizer.canResize = true;
            }
            else
            {
                _rect.anchoredPosition = _orignal - offsetVect;
                inspector.pos = -2000;
                hasMovePos = true;
                _resizer.canResize = false;
            }
        }

        private void OnResetPos()
        {
            if(go!=null)
            {
                UpdatePosition();
                RectTransform goRect = go.GetComponent<RectTransform>();

                offset = Vector3.zero;

                Vector3 v = MogoUtils.GetPosition3D(goRect.gameObject);
                _frame.SetPosition(v, goRect.sizeDelta, goRect.sizeDelta);
                Vector2 center = new Vector2(_frame.Position.x + _frame.Width / 2, _frame.Position.y - _frame.Height / 2);
                _arrow.Position = center + new Vector2(0, 10);
                if (_content.Visible == true)
                {
                    //_content.Position = _offset + _frame.Position;
                    _content.SetScale(direction, _frame.Position, goRect.sizeDelta);
                    _content.StartTween();
                }
            }
        }

        public override void SetData(object data)
        {
            //改为默认材质，让底下层次的特效可以显示
            //_mask.CurrentImage.material = null;
            //this.Visible = false;

            _arrow.Visible = false;
            _frame.Hide();
            _content.Visible = false;

            waitingObject = data as string[];
            bool notResetControlType = waitingObject[1].Contains("timeout") || waitingObject[1].Contains("auto");
            if (notResetControlType == false)
            {
                EventDispatcher.TriggerEvent(MainUIEvents.RESET_CONTROL_STICK);
            }
            count = 0;
        }

        public static PanelIdEnum GetPanelId(GameObject go)
        {
            GameObject root = UIManager.Instance.UIRoot;
            GameObject child = null;
            GameObject childchild = null;
            while (go != root)
            {
                childchild = child;
                child = go;
                go = go.transform.parent.gameObject;
            }
            if (childchild!=null)
            {
                PanelIdInspector inspector = childchild.GetComponent<PanelIdInspector>();
                if (inspector!=null)
                {
                    return (PanelIdEnum)inspector.PanelId;
                }
            }
            return PanelIdEnum.Empty;
        }

        private void SetContent(string[] list)
        {
            GameObject host = UIManager.Instance.UIRoot;
            Transform childTransform = host.transform.Find(list[2]);
            if (childTransform == null)
            {
                return;
            }
            TaskManager.Instance.ContinueAutoTask();
            inspector.url = list[2];
           // MogoUtils.PrintLog("guide url:"+inspector.url);
            waitingObject = null;
            count = 0;
            BasePanel[] panel = childTransform.GetComponentsInParent<BasePanel>(true);
            if(panel!=null && panel.Length!=0)
            {
                BaseModule.currentGuidePanel = panel[0].ID;
            }
            else
            {
                BaseModule.currentGuidePanel = PanelIdEnum.Empty;
            }
           //UILayerManager.AddGuideToLayer(childTransform);
           
            if(list[1].Contains("list"))
            {
                char[] split = new char[] { '|'};
                string[] resultList = list[1].Split(split);
                int Id = int.Parse(resultList[0].Substring(4, resultList[0].Length - 4));
                KList dataList = childTransform.gameObject.GetComponent<KList>();
                List<KList.KListItemBase> itemList = dataList.ItemList;
                for (int i = 0; i < itemList.Count; i++)
                {
                    if (itemList[i].ID  == Id)
                    {
                        go = itemList[i].gameObject;
                        if(resultList.Length>1)
                        {
                            Transform trans = go.transform.FindChild(resultList[1]);
                            go = trans.gameObject;
                        }
                    }
                }
            }
            else
            {
                go = childTransform.gameObject;
            }
            UpdatePosition();
            RectTransform goRect = go.GetComponent<RectTransform>();

            offset = Vector3.zero;
            FindPointerClickMethod(go, list[3]);
            if(list[1] == "longClick")
            {
                info = null;
            }
            SetVisible(list);
           
            direction = int.Parse(list[0]);
            SetDirection(goRect, list, direction);
            if (list[3] == "IPointerDownHandler")
            {
                _frame.OnPointerDown();
            }
            if(list[1].Contains("nothide"))
            {
                clickAndHide = false;
            }
            else
            {
                clickAndHide = true;
            }
            if (list[1].IndexOf("activity") != -1)
            {
                url = list[1];
            }
            else
            {
                url = list[2];
            }
            if(list[1] == "tween")
            {
                _arrow.Tween();
            }
        }


        private void SetVisible(string[] list)
        {
            _arrow.Visible = true;
            _frame.Show();
            _content.Visible = true;
            if (!list[1].Contains("all"))
            {
                _arrow.Show();
            }
            else
            {
                _arrow.Hide();
            }
            isTimeoutCallback = true;
            
            if (list[1].IndexOf("nomaskauto") != -1)
            {
                isTimeoutCallback = false;
                countDown = int.Parse(list[1].Substring(10, list[1].Length - 10)) * 30 / 1000;
                _frame.HideMask();
                ModalMask.Visible = false;
            }
            else if (list[1].IndexOf("nothidetimeout") != -1)
            {
                _frame.HideMask();
                ModalMask.Visible = false;
            }
            else if(list[1] == "mask")
            {
                countDown = 0;
                _frame.ShowMask();
            }
            else if (list[1].IndexOf("auto") != -1)
            {
                countDown = int.Parse(list[1].Substring(4, list[1].Length - 4)) * 30 / 1000;
                _frame.NoMask();
            }
            else
            {
                countDown = 0;
                _frame.NoMask();
            }
        }

        private void SetDirection(RectTransform goRect, string[] list, int direction)
        {
            Vector3 v = MogoUtils.GetPosition3D(goRect.gameObject);
            _frame.SetPosition(v, goRect.sizeDelta, goRect.sizeDelta);
            _frame.SetType(list[1]);
            Vector2 center = new Vector2(_frame.Position.x + _frame.Width / 2, _frame.Position.y - _frame.Height / 2);
            _arrow.Position = center + new Vector2(0,10);
            if (list.Length > 4)
            {
                _content.SetData(list[4]);
                _content.Visible = true;
                _content.SetScale(direction, _frame.Position, goRect.sizeDelta);
                _content.StartTween();
            }
            else
            {
                _content.Visible = false;
            }
        }

        private void FindPointerClickMethod(GameObject go,string type)
        {
            if(type == "Button")
            {
                target = go.GetComponent<Button>();
            }
            else if(type == "Toggle")
            {
                target = go.GetComponent<Toggle>();
            }
            else if(type == "IPointerDownHandler")
            {
                GetIPointerDownHandler(go.GetComponents<KContainer>());
            }
            else
            {
                KContainer[] components = go.GetComponents<KContainer>();
                GetContainerTarget(components);
            }
           
           
            if (type == "IPointerDownHandler")
            {
                if (target != null)
                {
                    info = target.GetType().GetMethod("OnPointerDown");
                }
                else
                {
                    target = null;
                }
            }
            else
            {
                if (target != null)
                {
                    info = target.GetType().GetMethod("OnPointerClick");
                }
                else
                {
                    target = null;
                }
            }
        }

        private void GetIPointerDownHandler(KContainer[] components)
        {
            for (int i = 0; i < components.Length; i++)
            {
                Type type = components[i].GetType();
                target = components[i];
                MethodInfo methodInfo = type.GetMethod("OnPointerDown");
                if (methodInfo != null && methodInfo.DeclaringType == type)
                {
                    return;
                }
            }
        }

        private void GetContainerTarget(KContainer[] components)
        {
            for(int i=0;i<components.Length;i++)
            {
                Type type = components[i].GetType();
                if (type.Name != "KContainer")
                {
                    target = components[i];
                    MethodInfo methodInfo = type.GetMethod("OnPointerClick");
                    if (methodInfo != null && methodInfo.DeclaringType == type)
                    {
                        return;
                    }
                }
            }
        }
        
        public override void OnClose()
        {
            BaseModule.currentGuidePanel = PanelIdEnum.Empty;
            inspector.url = string.Empty;
            waitingObject = null;
            isShow = false;
            _frame.Hide();
            _arrow.Hide();
            _content.StopTween();
            _frame.Stop();
            countDown = 0;
            isTimeoutCallback = true;
            info = null;
            go = null;
            target = null;
            EventDispatcher.RemoveEventListener(PanelEvents.PANEL_LAYER_RESET_POS, OnResetPos);
            //UILayerManager.RemoveGuideDependLayer();
        }

        private int countDown;
        private bool isTimeoutCallback = true;
        private char[] split = new char[] { ','};
        void Update()
        {
            if(waitingObject!=null)
            {
                if(count < 150)
                {
                    if(count%5==0)
                    {
                        SetContent(waitingObject);
                    }
                    count++;
                }
                else
                {
                    LoggerHelper.Error("GuidePanel:Not found:" + waitingObject[2]);
                    if(waitingObject[1].IndexOf("activity")!=-1)
                    {
                        url = waitingObject[1];
                    }
                    else
                    {
                        url = waitingObject[2];
                    }
                    
                    waitingObject = null;
                    count = 0;
                    PanelIdEnum.GuideArrow.Close();
                    DramaTrigger.ClickButton(url);
                }
            }
            if(countDown>0)
            {
                countDown--;
                if (countDown == 0)
                {
                    if (isTimeoutCallback)
                    {
                        OnClick(new PointerEventData(EventSystem.current));
                    }
                    else
                    {
                        PanelIdEnum.GuideArrow.Close();
                        url = null;
                    }
                }
            }
        }

    }
}
