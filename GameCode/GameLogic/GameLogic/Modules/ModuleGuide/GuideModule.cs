﻿#region 模块信息
/*==========================================
// 文件名：GuideModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/6/6 10:06:45
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuide
{
    public class GuideModule:BaseModule
    {
        public override void Init()
        {
            AddPanel(PanelIdEnum.GuideArrow, "Container_GuideArrowPanel", MogoUILayer.LayerGuide, "ModuleGuide.GuidePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);
            AddPanel(PanelIdEnum.SpecialGuide, "Container_SpecialGuidePanel", MogoUILayer.LayerUIMain, "ModuleGuide.SpecialGuidePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.FingerGuide, "Container_FingerGuidePanel", MogoUILayer.LayerGuide, "ModuleGuide.FingerGuidePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.PCFingerGuide, "Container_PCFingerGuidePanel", MogoUILayer.LayerGuide, "ModuleGuide.PCFingerGuidePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.TouchBattleMode, "Container_TouchBattleModePanel", MogoUILayer.LayerGuide, "ModuleGuide.TouchBattleModePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.PCChooseMouseMode, "Container_PCChooseMouseMode", MogoUILayer.LayerGuide, "ModuleGuide.PCChooseMouseModePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

    }
}
