﻿#region 模块信息
/*==========================================
// 文件名：PCChooseMouseMode
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/4/13 17:21:59
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
namespace ModuleGuide
{
    public class PCChooseMouseModePanel : BasePanel
    {
        private KButton _ensureButton;
        private KToggleGroup _toggleGroup;

        protected override PanelIdEnum Id
        {
            get
            {
                return PanelIdEnum.PCChooseMouseMode;
            }
        }

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            KContainer container = GetChildComponent<KContainer>("Container_caozuoshezhi");
            _ensureButton = container.GetChildComponent<KButton>("Button_queding");
            _toggleGroup = container.GetChildComponent<KToggleGroup>("ToggleGroup_xuanze");
        }

        public override void OnShow(object data)
        {
            _toggleGroup.SelectIndex = 0;
            AddEventListener();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _ensureButton.onClick.AddListener(OnEnsureBtnClick);
        }

        private void RemoveEventListener()
        {
            _ensureButton.onClick.RemoveListener(OnEnsureBtnClick);
        }

        private void OnEnsureBtnClick()
        {
            DramaTrigger.ClickButton("LayerGuide/Container_PCChooseMouseMode/Container_caozuoshezhi/Button_queding,Button");
            PlayerAvatar.Player.RpcCall("control_setting_save", (byte)_toggleGroup.SelectIndex + 1, 
                PlayerAvatar.Player.control_setting_lol_move, PlayerAvatar.Player.control_setting_web_player);
            ClosePanel();
        }
    }

}