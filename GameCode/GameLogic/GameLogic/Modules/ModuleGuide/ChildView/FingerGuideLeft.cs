﻿#region 模块信息
/*==========================================
// 文件名：FingerGuideLeft
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/1/5 14:12:46
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuide
{
    public class FingerGuideLeft:KContainer
    {
        private StateImage _guideArrow;
        private StateImage _img;
        private RectTransform _guideArrowRect;
        private Vector2 _pos;

        private int count = 0;
        private int wait = 0;
        private Vector2 _offset;

        protected override void Awake()
        {
            _guideArrow = GetChildComponent<StateImage>("Image_shouzhi01");
            _guideArrowRect = _guideArrow.transform as RectTransform;
            _guideArrowRect.localScale = new Vector3(-1, 1, 1);
            _guideArrowRect.anchoredPosition = _guideArrowRect.anchoredPosition + new Vector2(_guideArrowRect.sizeDelta.x , 0);
            _img = GetChildComponent<StateImage>("Image_fangda");
            RectTransform _rect = _img.GetComponent<RectTransform>();
            _rect.localScale = new Vector3(-1, 1, 1);
            _rect.anchoredPosition = _rect.anchoredPosition + new Vector2(_rect.sizeDelta.x, 0);
            _pos = _guideArrowRect.anchoredPosition;
            _offset = Vector2.zero;
            base.Awake();
        }

        protected override void OnEnable()
        {
            count = 0;
            wait = 0;
            _guideArrowRect.anchoredPosition = _pos;
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        void Update()
        {
            if (count <= -240)
            {
                wait++;
                if (wait == 10)
                {
                    count = 0;
                    wait = 0;
                }
            }
            else
            {
                count -= 8;
            }
            _offset.x = count;
            _guideArrowRect.anchoredPosition = _pos + _offset;
        }

    }
}
