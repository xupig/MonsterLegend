﻿#region 模块信息
/*==========================================
// 文件名：GuideFrame
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/6/16 15:55:16
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using Mogo.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ModuleGuide
{
    public class GuideFrame : KContainer
    {
        private RectTransform _rect;
        //private KParticle _particle;
        public KComponentEvent<PointerEventData> onClick = new KComponentEvent<PointerEventData>();

        public KComponentEvent<string,PointerEventData> onCallFunction = new KComponentEvent<string,PointerEventData>();
        public KComponentEvent<string, PointerEventData> onLongClick = new KComponentEvent<string, PointerEventData>();

        private float _width;
        private float _height;
        private StateImage _img;
        private RectTransform _imgRect;

        private RectTransform _topLeft;
        private RectTransform _topRight;
        private RectTransform _bottomLeft;
        private RectTransform _bottomRight;

        private RectTransform _maskTop;
        private RectTransform _maskBottom;
        private RectTransform _maskLeft;
        private RectTransform _maskRight;


        private StateImage _maskTopImg;
        private StateImage _maskBottomImg;
        private StateImage _maskLeftImg;
        private StateImage _maskRightImg;

        private ClickableComponent allClickComponentTop;
        private ClickableComponent allClickComponentBottom;
        private ClickableComponent allClickComponentLeft;
        private ClickableComponent allClickComponentRight;

        private PointerDownComponent pointerDownComponent;
        private ClickableComponent clickComponent;
        private DragableComponent dragableComponent;
        
        private LongClickComponent longClickButton;

        private bool _isTween = false;
        private int count = 0;
        private bool isOut = true;


        private StateImage _topLeftImg;
        private StateImage _topRightImg;
        private StateImage _bottomLeftImg;
        private StateImage _bottomRightImg;

        protected override void Awake()
        {
            base.Awake();
           // _particle = AddChildComponent<KParticle>("fx_ui_23_zhiyin_01");
            _img = GetChildComponent<StateImage>("Container_content/ScaleImage_guidesharedZhezhao");
            _img.CurrentImage.alpha = 0f;
            _img.gameObject.AddComponent<KDummyButton>();
            dragableComponent = _img.gameObject.AddComponent<DragableComponent>();
            pointerDownComponent = _img.gameObject.AddComponent<PointerDownComponent>();
            clickComponent = _img.gameObject.AddComponent<ClickableComponent>();
            longClickButton = _img.gameObject.AddComponent<LongClickComponent>();
            _imgRect = _img.CurrentImage.GetComponent<RectTransform>();
            _imgRect.anchoredPosition = Vector2.zero;
            _rect = GetComponent<RectTransform>();
            _width = _rect.sizeDelta.x;
            _height = _rect.sizeDelta.y;


            _topLeftImg = GetChildComponent<StateImage>("Image_guideFrame01");
            _topRightImg = GetChildComponent<StateImage>("Image_guideFrame02");
            _bottomLeftImg = GetChildComponent<StateImage>("Image_guideFrame03");
            _bottomRightImg = GetChildComponent<StateImage>("Image_guideFrame04");

            _topLeftImg.Raycast = false;
            _topRightImg.Raycast = false;
            _bottomLeftImg.Raycast = false;
            _bottomRightImg.Raycast = false;

            _topLeft = GetChildComponent<RectTransform>("Image_guideFrame01");
            _topRight = GetChildComponent<RectTransform>("Image_guideFrame02");
            _bottomLeft = GetChildComponent<RectTransform>("Image_guideFrame03");
            _bottomRight = GetChildComponent<RectTransform>("Image_guideFrame04");
            dragableComponent.onBeginDrag.AddListener(OnBeginDrag);
            dragableComponent.onDrag.AddListener(OnDrag);
            dragableComponent.onEndDrag.AddListener(OnEndDrag);
            clickComponent.onClick.AddListener(OnPointerClick);
            pointerDownComponent.onClick.AddListener(OnPointerClick);
            longClickButton.onPointerDown.AddListener(OnLongPointerDown);
            longClickButton.onPointerUp.AddListener(OnLongPointerUp);
        }

        private void OnLongPointerDown(PointerEventData eventData)
        {
            onLongClick.Invoke("OnPointerDown", eventData);
        }

        private void OnLongPointerUp(PointerEventData eventData)
        {
            onLongClick.Invoke("OnPointerUp", eventData);
            onClick.Invoke(eventData);
        }

        public void SetType(string type)
        {
            timeout = 0;
            isTimeOutCallBack = true;
            
            if(type == "drag")
            {
                dragableComponent.enabled = true;
                clickComponent.enabled = false;
                allClickComponentTop.enabled = false;
                allClickComponentBottom.enabled = false;
                allClickComponentLeft.enabled = false;
                allClickComponentRight.enabled = false;
                pointerDownComponent.enabled = false;
                longClickButton.enabled = false;
            }
            else if(type == "longClick")
            {
                longClickButton.enabled = true;
                dragableComponent.enabled = false;
                clickComponent.enabled = false;
                allClickComponentTop.enabled = false;
                allClickComponentBottom.enabled = false;
                allClickComponentLeft.enabled = false;
                allClickComponentRight.enabled = false;
                pointerDownComponent.enabled = false;
            }
            else if (type.Contains("clicktimeout"))
            {
                dragableComponent.enabled = false;
                clickComponent.enabled = true;
                allClickComponentTop.enabled = false;
                allClickComponentBottom.enabled = false;
                allClickComponentLeft.enabled = false;
                allClickComponentRight.enabled = false;
                longClickButton.enabled = false;
                pointerDownComponent.enabled = false;
                timeout = int.Parse(type.Substring(12, type.Length - 12)) * 30 / 1000;
                isTimeOutCallBack = false;
            }
            else if (type.Contains("alltimeout"))
            {
                dragableComponent.enabled = false;
                clickComponent.enabled = false;
                allClickComponentTop.enabled = false;
                allClickComponentBottom.enabled = false;
                allClickComponentLeft.enabled = false;
                allClickComponentRight.enabled = false;
                longClickButton.enabled = false;
                pointerDownComponent.enabled = false;
                timeout = int.Parse(type.Substring(10, type.Length - 10)) * 30 / 1000;
            }
            else if(type == "all")
            {
                dragableComponent.enabled = false;
                clickComponent.enabled = true;
                allClickComponentTop.enabled = true;
                allClickComponentBottom.enabled = true;
                allClickComponentLeft.enabled = true;
                allClickComponentRight.enabled = true;
                longClickButton.enabled = false;
                pointerDownComponent.enabled = false;
            }
            else if (type.Contains("nothidetimeout"))
            {
                dragableComponent.enabled = false;
                clickComponent.enabled = true;
                allClickComponentTop.enabled = false;
                allClickComponentBottom.enabled = false;
                allClickComponentLeft.enabled = false;
                allClickComponentRight.enabled = false;
                longClickButton.enabled = false;
                pointerDownComponent.enabled = false;
                timeout = int.Parse(type.Substring(14, type.Length - 14)) * 30 / 1000;
            }
            else if(type.Contains("timeout"))
            {
                dragableComponent.enabled = false;
                clickComponent.enabled = false;
                allClickComponentTop.enabled = false;
                allClickComponentBottom.enabled = false;
                allClickComponentLeft.enabled = false;
                allClickComponentRight.enabled = false;
                longClickButton.enabled = false;
                pointerDownComponent.enabled = false;
                timeout = int.Parse(type.Substring(7, type.Length - 7)) * 30/1000;
            }
            else
            {
                dragableComponent.enabled = false;
                clickComponent.enabled = true;
                allClickComponentTop.enabled = false;
                allClickComponentBottom.enabled = false;
                allClickComponentLeft.enabled = false;
                allClickComponentRight.enabled = false;
                longClickButton.enabled = false;
                pointerDownComponent.enabled = false;
            }
        }

        public void OnPointerDown()
        {
            dragableComponent.enabled = false;
            clickComponent.enabled = false;
            allClickComponentTop.enabled = false;
            allClickComponentBottom.enabled = false;
            allClickComponentLeft.enabled = false;
            allClickComponentRight.enabled = false;
            longClickButton.enabled = false;
            pointerDownComponent.enabled = true;
        }

        public void Show()
        {
            this.Visible = true;
        }

        public void Hide()
        {
            this.Visible = false;
            timeout = 0;
            isTimeOutCallBack = true;
        }

        public void SetMask(GuidePanel root)
        {
            for (int i = 1; i < 5; i++)
            {
                RectTransform rect = root.GetChildComponent<RectTransform>("Container_mask" + i);
                rect.anchoredPosition = Vector3.zero;
            }

            _maskTopImg = root.GetChildComponent<StateImage>("Container_mask1/ScaleImage_guidesharedZhezhao");
            _maskTopImg.AddAllStateComponent<Resizer>();
            _maskTopImg.gameObject.AddComponent<KDummyButton>();
            _maskBottomImg = root.GetChildComponent<StateImage>("Container_mask2/ScaleImage_guidesharedZhezhao");
            _maskBottomImg.AddAllStateComponent<Resizer>();
            _maskBottomImg.gameObject.AddComponent<KDummyButton>();
            _maskLeftImg = root.GetChildComponent<StateImage>("Container_mask3/ScaleImage_guidesharedZhezhao");
            _maskLeftImg.AddAllStateComponent<Resizer>();
            _maskLeftImg.gameObject.AddComponent<KDummyButton>();
            _maskRightImg = root.GetChildComponent<StateImage>("Container_mask4/ScaleImage_guidesharedZhezhao");
            _maskRightImg.AddAllStateComponent<Resizer>();
            _maskRightImg.gameObject.AddComponent<KDummyButton>();

            _maskTop = _maskTopImg.CurrentImage.GetComponent<RectTransform>();
            _maskBottom = _maskBottomImg.CurrentImage.GetComponent<RectTransform>();
            _maskLeft = _maskLeftImg.CurrentImage.GetComponent<RectTransform>();
            _maskRight = _maskRightImg.CurrentImage.GetComponent<RectTransform>();


            NoMask();

            allClickComponentTop = _maskTopImg.gameObject.AddComponent<ClickableComponent>();
            allClickComponentBottom = _maskBottomImg.gameObject.AddComponent<ClickableComponent>();
            allClickComponentLeft = _maskLeftImg.gameObject.AddComponent<ClickableComponent>();
            allClickComponentRight = _maskRightImg.gameObject.AddComponent<ClickableComponent>();

            allClickComponentTop.onClick.AddListener(OnPointerClick);
            allClickComponentBottom.onClick.AddListener(OnPointerClick);
            allClickComponentLeft.onClick.AddListener(OnPointerClick);
            allClickComponentRight.onClick.AddListener(OnPointerClick);

        }

        public void HideMask()
        {
            _maskTopImg.Visible = false;
            _maskBottomImg.Visible = false;
            _maskLeftImg.Visible = false;
            _maskRightImg.Visible = false;

            
        }

        public void NoMask()
        {
            _maskTopImg.Visible = true;
            _maskBottomImg.Visible = true;
            _maskLeftImg.Visible = true;
            _maskRightImg.Visible = true;

            _maskTopImg.CurrentImage.alpha = 0;
            _maskBottomImg.CurrentImage.alpha = 0;
            _maskLeftImg.CurrentImage.alpha = 0;
            _maskRightImg.CurrentImage.alpha = 0;
        }

        public void ShowMask()
        {
            _maskTopImg.Visible = true;
            _maskBottomImg.Visible = true;
            _maskLeftImg.Visible = true;
            _maskRightImg.Visible = true;

            _maskTopImg.CurrentImage.alpha = 0.6f;
            _maskBottomImg.CurrentImage.alpha = 0.6f;
            _maskLeftImg.CurrentImage.alpha = 0.6f;
            _maskRightImg.CurrentImage.alpha = 0.6f;
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
           // Debug.LogError("OnPointerClick");
            onClick.Invoke(eventData);
        }

        public void OnDrag(PointerEventData eventData)
        {
            onCallFunction.Invoke("OnDrag", eventData);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            onCallFunction.Invoke("OnPointerDown", eventData);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            onCallFunction.Invoke("OnEndDrag", eventData);
            onClick.Invoke(eventData);
        }

        public float Width
        {
            get { return _width; }
        }

        public float Height
        {
            get { return _height; }
        }

        public Vector3 Position
        {
            get
            {
                return _rect.anchoredPosition;
            }
            set
            {
                _rect.anchoredPosition = value;
                _rect.transform.localPosition = new Vector3(_rect.transform.localPosition.x, _rect.transform.localPosition.y, -2);
            }
        }


        public void SetPosition(Vector3 pos, Vector3 offset, Vector2 size)
        {
            _width = size.x;
            _height = size.y;

            offset.x = offset.x / 2 - _width / 2;
            offset.y = _height / 2 - offset.y / 2;
            offset.z = 0;
            _rect.anchoredPosition = pos + offset;
            _rect.transform.localPosition = new Vector3(_rect.transform.localPosition.x, _rect.transform.localPosition.y, -2);
            _rect.sizeDelta = size;

            _img.SetDimensionsDirty();
            _imgRect.sizeDelta = size;
            _rect.sizeDelta = size;
            _imgRect.anchoredPosition = new Vector3((_width - _imgRect.sizeDelta.x) / 2, -(_height - _imgRect.sizeDelta.y) / 2, 0);

            _topLeft.anchoredPosition = new Vector2(-3, 3);
            _topRight.anchoredPosition = new Vector2(_topLeft.anchoredPosition.x + size.x - _topRight.sizeDelta.x + 6, _topLeft.anchoredPosition.y);
            _bottomLeft.anchoredPosition = new Vector2(_topLeft.anchoredPosition.x, _topLeft.anchoredPosition.y - size.y + _bottomLeft.sizeDelta.y-6);
            _bottomRight.anchoredPosition = new Vector2(_topRight.anchoredPosition.x, _bottomLeft.anchoredPosition.y);

            ResizeImage(_maskTopImg, new Vector2(UIManager.CANVAS_WIDTH, Mathf.Floor(-_rect.anchoredPosition.y)));
            float topHeight = _maskTop.sizeDelta.y * _maskTop.localScale.y;
            _maskTop.anchoredPosition = Vector2.zero;

            ResizeImage(_maskLeftImg, new Vector2(Mathf.Floor(_rect.anchoredPosition.x), Mathf.Floor(size.y)));
            float leftWidth = _maskLeft.sizeDelta.x * _maskLeft.localScale.x;
            float leftHeight = _maskLeft.sizeDelta.y * _maskLeft.localScale.y;
            _maskLeft.anchoredPosition = new Vector2(0, Mathf.Floor(-topHeight));

            ResizeImage(_maskRightImg, new Vector2(Mathf.Floor(UIManager.CANVAS_WIDTH - leftWidth - size.x), Mathf.Floor(size.y)));
            _maskRight.anchoredPosition = new Vector2(Mathf.Floor(leftWidth + size.x), Mathf.Floor(-topHeight));

            ResizeImage(_maskBottomImg, new Vector2(UIManager.CANVAS_WIDTH, Mathf.Floor(UIManager.CANVAS_HEIGHT - topHeight - leftHeight)));
            _maskBottom.anchoredPosition = new Vector2(0, Mathf.Floor(-topHeight - leftHeight));
            _isTween = true;
        }

        private void ResizeImage(StateImage image, Vector2 size)
        {
            image.CurrentImage.SetAllDirty();
            Vector2 originalSize = image.CurrentImage.GetComponent<Resizer>().OriginalSize;
            RectTransform imageRect = image.CurrentImage.GetComponent<RectTransform>();
            imageRect.sizeDelta = size;
            
        }

        public void Stop()
        {
            _isTween = false;
            count = 0;
            isOut = true;
        }

        private int timeout = 0;
        private bool isTimeOutCallBack = true;

        void Update()
        {
            if(_isTween)
            {
                if(isOut)
                {
                    _topLeft.anchoredPosition += new Vector2(-1,1 );
                    _topRight.anchoredPosition += new Vector2(1,1);
                    _bottomLeft.anchoredPosition += new Vector2(-1,-1);
                    _bottomRight.anchoredPosition += new Vector2(1,-1);
                }
                else
                {
                    _topLeft.anchoredPosition -= new Vector2(-1, 1);
                    _topRight.anchoredPosition -= new Vector2(1, 1);
                    _bottomLeft.anchoredPosition -= new Vector2(-1, -1);
                    _bottomRight.anchoredPosition -= new Vector2(1, -1);
                }
                if(count>=10)
                {
                    isOut = !isOut;
                    count = 0;
                }
                count++;
            }
            if(timeout>0)
            {
                timeout--;
                if (timeout == 0 && isTimeOutCallBack)
                {
                    GuidePanel.clickAndHide = true;
                    onClick.Invoke(new PointerEventData(EventSystem.current));
                }
            }
        }

    }
}
