﻿#region 模块信息
/*==========================================
// 文件名：FingerReset
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/1/6 14:10:41
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuide
{
    public class FingerReset:KContainer
    {
        private DoubleClickGuide _arrow;
        private KParticle _particle;
        private RectTransform _tishi;
        protected override void Awake()
        {
            _arrow = AddChildComponent<DoubleClickGuide>("Container_guideArrow");
            _particle = AddChildComponent<KParticle>("fx_ui_FingerGuide");
            _particle.Position = (_arrow.transform as RectTransform).anchoredPosition;
            _tishi = GetChildComponent<RectTransform>("Container_tishi");
            _tishi.anchoredPosition = _tishi.anchoredPosition + new Vector2(240,0);
            base.Awake();
        }
    }
}
