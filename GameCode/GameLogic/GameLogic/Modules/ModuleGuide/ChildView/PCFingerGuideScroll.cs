﻿#region 模块信息
/*==========================================
// 文件名：PCFingerGuideScroll
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/3/3 20:14:28
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuide
{
    public class PCFingerGuideScroll:KContainer
    {
        private StateImage _imgUp;
        private Vector2 _posUp;
        private RectTransform _rectUp;
        private int countUp = 0;
        private int waitUp = 0;
        private Vector2 _offsetUp;

        private StateImage _imgDown;
        private Vector2 _posDown;
        private RectTransform _rectDown;
        private int countDown = 0;
        private int waitDown = 0;
        private Vector2 _offsetDown;

        protected override void Awake()
        {
            _imgUp = GetChildComponent<StateImage>("Container_left/Container_huadong/Image_xiangxia");
            _rectUp = _imgUp.GetComponent<RectTransform>();
            _posUp = _rectUp.anchoredPosition;
            _offsetUp = Vector2.zero;

            _imgDown = GetChildComponent<StateImage>("Container_left/Container_huadong/Image_xiangshang");
            _rectDown = _imgDown.GetComponent<RectTransform>();
            _posDown = _rectDown.anchoredPosition;
            _offsetDown = Vector2.zero;
            base.Awake();
        }

        protected override void OnEnable()
        {
            countUp = 0;
            waitUp = 0;
            _rectUp.anchoredPosition = _posUp;

            countDown = 0;
            waitDown = 0;
            _rectDown.anchoredPosition = _posDown;
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        void Update()
        {
            if (countDown <= -30)
            {
                waitDown++;
                if (waitDown == 10)
                {
                    countDown = 0;
                    waitDown = 0;
                }
            }
            else
            {
                countDown -= 3;
            }
            _offsetDown.y = countDown;
            _rectDown.anchoredPosition = _posDown - _offsetDown;


            if (countUp >= 30)
            {
                waitUp++;
                if (waitUp == 10)
                {
                    countUp = 0;
                    waitUp = 0;
                }
            }
            else
            {
                countUp += 3;
            }
            _offsetUp.y = countUp;
            _rectUp.anchoredPosition = _posUp - _offsetUp;
        }
    }
}
