﻿#region 模块信息
/*==========================================
// 文件名：FingerGuideRight
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/1/5 14:13:21
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using ModuleGuide;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuide
{
    public class FingerGuideRight:KContainer
    {
        private StateImage _guideArrow;
        private StateImage _img;
        private RectTransform _rect;
        private Vector2 _pos;

        private int count = 0;
        private int wait = 0;
        private Vector2 _offset;

        protected override void Awake()
        {
            _guideArrow = GetChildComponent<StateImage>("Image_shouzhi01");
            _img = GetChildComponent<StateImage>("Image_fangda");
            _rect = _guideArrow.GetComponent<RectTransform>();
            _pos = _rect.anchoredPosition;
            _offset = Vector2.zero;
            base.Awake();
        }

        protected override void OnEnable()
        {
            count = 0;
            wait = 0;
            _rect.anchoredPosition = _pos;
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        void Update()
        {
            if (count >= 240)
            {
                wait++;
                if(wait == 10)
                {
                    count = 0;
                    wait = 0;
                }
            }
            else
            {
                count += 8;
            }
            _offset.x = count;
            _rect.anchoredPosition = _pos + _offset;
        }



    }
}
