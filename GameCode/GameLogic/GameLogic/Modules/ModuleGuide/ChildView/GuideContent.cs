﻿#region 模块信息
/*==========================================
// 文件名：GuideContent
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/7 10:12:31
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuide
{
    public class GuideContent : KContainer
    {
        private StateText _txtContent;
        private RectTransform _rect;
        private RectTransform _txtRect;
        private StateImage _imgFrame;
        private StateImage _imgArrow;
        private RectTransform _iconRect;
        private RectTransform _txtContentRect;
        //private RectTransform _frameContentRect;
        private RectTransform _frameRect;
        private RectTransform _arrowRect;
        private string result;
        private string orignal;
        protected override void Awake()
        {
            _txtContent = GetChildComponent<StateText>("Label_txtZhiyinneirong");
            _txtContent.Raycast = false;
            orignal = _txtContent.CurrentText.text;
            _iconRect = GetChildComponent<RectTransform>("Image_tairuier");
            GetChildComponent<StateImage>("Image_tairuier").Raycast = false;
            _imgFrame = GetChildComponent<StateImage>("ScaleImage_zyFrame");
            _imgFrame.Raycast = false;
            _imgArrow = GetChildComponent<StateImage>("Image_sharedArrow_down");
            _imgArrow.Raycast = false;
            //_frameContentRect = _imgFrame.GetComponent<RectTransform>();
            _frameRect = _imgFrame.CurrentImage.GetComponent<RectTransform>();
            _arrowRect = _imgArrow.GetComponent<RectTransform>();
            _rect = gameObject.GetComponent<RectTransform>();
            _txtRect = _txtContent.CurrentText.GetComponent<RectTransform>();
            _txtContentRect = _txtContent.GetComponent<RectTransform>();
            _txtContentRect.anchoredPosition = _frameRect.anchoredPosition;
            base.Awake();
        }

        public void SetData(string contentId)
        {
            string content = int.Parse(contentId).ToLanguage();
            result = string.Empty;
            if (content.Length <= 20)
            {
                result = content;
            }
            else
            {
                int len3 = Mathf.CeilToInt(content.Length / 2.0f);
                result = content.Substring(0, len3) + "\n" + content.Substring(len3, content.Length - len3);
            }
            Refresh();
            _txtContent.Visible = false;
            _imgFrame.Visible = false;
            _imgArrow.Visible = false;
            lateUpdate = 5;
           
        }

        private int lateUpdate = 0;


        public Vector2 Position
        {
            set { _rect.anchoredPosition = value; }
            get { return _rect.anchoredPosition; }
        }


        public void SetScale(int dir, Vector2 pos,Vector2 size)
        {
            this.dir = dir;   
            _pos = pos;
            _size = size;
            lateUpdate = 5;
            Refresh();
        }

        private Vector2 _pos;
        private Vector2 _size;

        public void StartTween()
        {
            _isTween = true;
            
        }

        public void StopTween()
        {
            _isTween = false;
            direction = -1;
            count = 30;
        }

        private bool _isTween = false;
        private const float _tweenStep = 0.8f;
        private int direction = -1;
        private int count = 30;

        private int dir = 0;

        void Update()
        {
            if(_isTween)
            {
                if(count % 2 == 0)
                {
                    _rect.anchoredPosition = new Vector2(_rect.anchoredPosition.x, _rect.anchoredPosition.y + direction * _tweenStep);
                }
                count--;
                if(count == 0)
                {
                    direction = -direction;
                    count = 30;
                }
            }
            if (lateUpdate>0)
            {
                lateUpdate--;
                _txtContent.Visible = true;
                _imgFrame.Visible = true;
                _imgArrow.Visible = true;
                Refresh();
            }
        }


        private void Refresh()
        {
            _txtContent.SetDimensionsDirty();
            _txtContent.CurrentText.text = result;
            int txtWidth = Mathf.CeilToInt(_txtContent.CurrentText.preferredWidth + 1);
            int txtHeight = Mathf.CeilToInt(_txtContent.CurrentText.preferredHeight + 1);
            _txtRect.sizeDelta = new Vector2(txtWidth, txtHeight);

            _imgFrame.SetDimensionsDirty();
            _frameRect.sizeDelta = new Vector2(Math.Max(300,_txtRect.sizeDelta.x + 32),Math.Max(80, _txtRect.sizeDelta.y + 35));
            _txtRect.anchoredPosition = _frameRect.anchoredPosition + new Vector2(_frameRect.sizeDelta.x / 2 - _txtRect.sizeDelta.x / 2, _txtRect.sizeDelta.y / 2 - _frameRect.sizeDelta.y / 2);
            int arrowWidth = Mathf.CeilToInt(_frameRect.sizeDelta.x - (8 * _frameRect.sizeDelta.x / 174 + 11));
            int arrowHeight = Mathf.CeilToInt((_arrowRect.sizeDelta.y - _frameRect.sizeDelta.y) / 2);
            _arrowRect.anchoredPosition = new Vector2(arrowWidth, arrowHeight);

           // _frameContentRect.anchoredPosition = new Vector2(0, _arrowRect.anchoredPosition.y + (_frameRect.sizeDelta.y - _arrowRect.sizeDelta.y) / 2);
           // _iconRect.anchoredPosition = new Vector2(-6, _frameRect.anchoredPosition.y + 54);
           // _txtContentRect.anchoredPosition = new Vector2(12, _frameRect.anchoredPosition.y - 31);
            //float width = _arrowRect.anchoredPosition.x + _arrowRect.sizeDelta.x;
           // float height = _frameRect.sizeDelta.y;

            if (dir == 1)//左
            {

                _arrowRect.localScale = Vector3.one;
                _arrowRect.anchoredPosition = _frameRect.anchoredPosition + new Vector2(_arrowRect.sizeDelta.x / 2,_arrowRect.sizeDelta.y / 2);
                _iconRect.anchoredPosition = new Vector2(_frameRect.anchoredPosition.x + _frameRect.sizeDelta.x - _iconRect.sizeDelta.x, _iconRect.anchoredPosition.y);
                _rect.anchoredPosition = _pos + new Vector2(_size.x/2 -_arrowRect.sizeDelta.x/2, _arrowRect.sizeDelta.y + _arrowRect.anchoredPosition.y - _iconRect.anchoredPosition.y);
            }
            else if (dir == 2)//右
            {
                _arrowRect.localScale = Vector3.one;
                _arrowRect.anchoredPosition = _frameRect.anchoredPosition + new Vector2(_frameRect.sizeDelta.x / 2, _arrowRect.sizeDelta.y / 2);
                _iconRect.anchoredPosition = new Vector2(_frameRect.anchoredPosition.x, _iconRect.anchoredPosition.y);
                _rect.anchoredPosition = _pos + new Vector2(_size.x / 2 - _frameRect.sizeDelta.x / 2, _arrowRect.sizeDelta.y + _arrowRect.anchoredPosition.y - _iconRect.anchoredPosition.y);
            }
            else if (dir == 3)//上
            {
                _arrowRect.localScale = Vector3.one;
                _arrowRect.anchoredPosition = _frameRect.anchoredPosition + new Vector2(_frameRect.sizeDelta.x - _arrowRect.sizeDelta.x / 2, _arrowRect.sizeDelta.y / 2);
                _iconRect.anchoredPosition = new Vector2(_frameRect.anchoredPosition.x, _iconRect.anchoredPosition.y);
                _rect.anchoredPosition = _pos + new Vector2(_size.x / 2 - _frameRect.sizeDelta.x + _arrowRect.sizeDelta.x / 2, _arrowRect.sizeDelta.y + _arrowRect.anchoredPosition.y - _iconRect.anchoredPosition.y);
            }
            else if(dir == 4)
            {
                _arrowRect.localScale = new Vector3(1, -1, 1);
                _arrowRect.anchoredPosition = _frameRect.anchoredPosition + new Vector2(_arrowRect.sizeDelta.x / 2, -_frameRect.sizeDelta.y - _arrowRect.sizeDelta.y / 2);
                _iconRect.anchoredPosition = new Vector2(_frameRect.anchoredPosition.x + _frameRect.sizeDelta.x - _iconRect.sizeDelta.x, _iconRect.anchoredPosition.y);
                _rect.anchoredPosition = _pos + new Vector2(_size.x / 2 - _arrowRect.sizeDelta.x / 2, _arrowRect.sizeDelta.y/2 - _arrowRect.anchoredPosition.y);
            }
            else if(dir == 5)
            {
                _arrowRect.localScale = new Vector3(1, -1, 1);
                _arrowRect.anchoredPosition = _frameRect.anchoredPosition + new Vector2(_frameRect.sizeDelta.x / 2, -_frameRect.sizeDelta.y - _arrowRect.sizeDelta.y / 2);
                _iconRect.anchoredPosition = new Vector2(_frameRect.anchoredPosition.x, _iconRect.anchoredPosition.y);
                _rect.anchoredPosition = _pos + new Vector2(_size.x / 2 - _frameRect.sizeDelta.x / 2, _arrowRect.sizeDelta.y/2 - _arrowRect.anchoredPosition.y);
            }
            else
            {
                _arrowRect.localScale = new Vector3(1, -1, 1);
                _arrowRect.anchoredPosition = _frameRect.anchoredPosition + new Vector2(_frameRect.sizeDelta.x - _arrowRect.sizeDelta.x/2, -_frameRect.sizeDelta.y - _arrowRect.sizeDelta.y/2);
                _iconRect.anchoredPosition = new Vector2(_frameRect.anchoredPosition.x, _iconRect.anchoredPosition.y);
                _rect.anchoredPosition = _pos + new Vector2(_size.x / 2 - _frameRect.sizeDelta.x + _arrowRect.sizeDelta.x / 2, _arrowRect.sizeDelta.y/2 - _arrowRect.anchoredPosition.y);
            }
        }

    }
}
