﻿#region 模块信息
/*==========================================
// 文件名：PCFingerReset
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/3/7 14:04:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuide
{
    public class PCFingerReset:KContainer
    {
        private StateImage _click;
        public int index = 0;
        public float _timerId;
        public int count = 2;
        protected override void Awake()
        {
            _click = GetChildComponent<StateImage>("Container_guideArrow/Image_mouse03");
            _click.Raycast = false;
            time2 = Time.realtimeSinceStartup;
            base.Awake();
        }

        void Update()
        {
            if (count == 0)
            {
                if (Time.realtimeSinceStartup - _timerId >= 1)
                {
                    count = 2;
                }
                else
                {
                    return;
                }
            }
            OnTimeStep();
        }

        public float time2 = 0;

        private void OnTimeStep()
        {
            if (Time.realtimeSinceStartup - time2 >= 0.03f)
            {
                index++;
                if (index == 2)
                {
                    _click.Visible = true;
                    index = 0;
                    count--;
                    if (count == 0)
                    {
                        _timerId = Time.realtimeSinceStartup;
                    }
                }
                else
                {
                    _click.Visible = false;
                }
                time2 = Time.realtimeSinceStartup;
            }

        }
    }
}
