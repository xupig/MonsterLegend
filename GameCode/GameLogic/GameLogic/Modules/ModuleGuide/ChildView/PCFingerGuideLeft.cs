﻿#region 模块信息
/*==========================================
// 文件名：PCFingerGuideLeft
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/3/3 20:12:29
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuide
{
    public class PCFingerGuideLeft:KContainer
    {
        private Vector2 _pos;
        private RectTransform _rect;
        private int count = 0;
        private int wait = 0;
        private Vector2 _offset;

        protected override void Awake()
        {
            _rect = GetChildComponent<RectTransform>("Container_tishi/Container_arrowGuide");
            _pos = _rect.anchoredPosition;
            _offset = Vector2.zero;
            base.Awake();
        }

        protected override void OnEnable()
        {
            count = 0;
            wait = 0;
            _rect.anchoredPosition = _pos;
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        void Update()
        {
            if (count <= -240)
            {
                wait++;
                if (wait == 10)
                {
                    count = 0;
                    wait = 0;
                }
            }
            else
            {
                count -= 8;
            }
            _offset.x = count;
            _rect.anchoredPosition = _pos + _offset;



        }
    }
}
