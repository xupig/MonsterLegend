﻿#region 模块信息
/*==========================================
// 文件名：FingerGuideLeftRight
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/1/5 14:30:47
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuide
{
    public class FingerGuideLeftRight:KContainer
    {
        private FingerGuideLeft _left;
        private FingerGuideRight _right;

        protected override void Awake()
        {
            _left = AddChildComponent<FingerGuideLeft>("Container_left");
            _right = AddChildComponent<FingerGuideRight>("Container_right");
            base.Awake();
        }

    }
}
