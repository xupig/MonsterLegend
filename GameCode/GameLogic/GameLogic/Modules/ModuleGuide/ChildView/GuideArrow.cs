﻿#region 模块信息
/*==========================================
// 文件名：GuideArrow
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/6/16 15:25:26
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuide
{
    public class GuideArrow:KContainer
    {
        private RectTransform _rect;
        private List<StateImage> _imageList;
        private int index = 0;
        private uint _timerId;
        public float width;
        public float height;
        protected override void Awake()
        {
            _rect = gameObject.GetComponent<RectTransform>();
            width = _rect.sizeDelta.x;
            height = _rect.sizeDelta.y;
            _imageList = new List<StateImage>();
            //transform.FindChild("").gameObject.SetActive(false);
            for (int i = 0; i < 4;i++ )
            {
                StateImage image = GetChildComponent<StateImage>(string.Format("Image_shouzhi0{0}",i+1));
                image.Raycast = false;
                _imageList.Add(image);
            }
            base.Awake();
        }

        public Vector2 Position
        {
            get { return _rect.anchoredPosition; }
            set { _rect.anchoredPosition = value; _rect.transform.localPosition = new Vector3(_rect.transform.localPosition.x, _rect.transform.localPosition.y, -5); }
        }

        //public void SetScale(int scale,float offsetX,float offsetY)
        //{
        //    _rect.localScale = new Vector3(scale, 1, 1);
        //    if (scale == -1)
        //    {
        //        _rect.anchoredPosition3D = new Vector3(_rect.anchoredPosition3D.x - offsetX, _rect.anchoredPosition3D.y+offsetY, _rect.anchoredPosition3D.z);
        //    }
        //}

        private TweenPosition tween;
        private Vector2 _orignalPos;

        public void Tween()
        {
            _orignalPos = _rect.anchoredPosition;
            Vector2 v = _rect.anchoredPosition + new Vector2(200,100);
            if (tween) tween.Stop();
            tween = TweenPosition.Begin(this.gameObject, 0.5f, v, OnFinish);
        }

        private void OnFinish(UITweener tweener)
        {
            _rect.anchoredPosition3D = _orignalPos;
            _orignalPos = _rect.anchoredPosition;
            Vector2 v = _rect.anchoredPosition + new Vector2(200, 100);
            if(tween)tween.Stop();
            tween = TweenPosition.Begin(this.gameObject, 0.5f, v, OnFinish2);
        }

        private void OnFinish2(UITweener tweener)
        {
            Position = _orignalPos;
        }

        public void Show()
        {
            Visible = true;
            
            if (_timerId == 0)
            {
                _timerId = TimerHeap.AddTimer(0, 180, OnTimeStep);
            }
        }

        public void Hide()
        {
            Visible = false;
            for (int i = 0; i < 4; i++)
            {
                _imageList[i].Visible = false;
            }
            if(_timerId!=0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private void OnTimeStep()
        {
            for (int i = 0; i < 4;i++ )
            {
                _imageList[i].Visible = false;
            }
            _imageList[index].Visible = true;
            index++;
            if(index == 4)
            {
                index = 0;
            }
        }

    }
}
