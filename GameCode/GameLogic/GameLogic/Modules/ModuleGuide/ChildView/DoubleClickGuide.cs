﻿#region 模块信息
/*==========================================
// 文件名：DoubleClickComponent
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/1/6 15:23:41
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuide
{
    public class DoubleClickGuide:KContainer
    {
        private List<StateImage> _imageList;
        public int index = 0;
        public float _timerId;
        public int count = 2;
        protected override void Awake()
        {
            _imageList = new List<StateImage>();
            for (int i = 0; i < 4; i++)
            {
                StateImage image = GetChildComponent<StateImage>(string.Format("Image_shouzhi0{0}", i + 1));
                image.Raycast = false;
                _imageList.Add(image);
            }
            time2 = Time.realtimeSinceStartup;
            base.Awake();
        }

        void Update()
        {
            if (count == 0)
            {
                if (Time.realtimeSinceStartup - _timerId >= 1)
                {
                    count = 2;
                }
                else
                {
                    return;
                }
            }
            OnTimeStep();
        }

        public float time2 = 0;

        private void OnTimeStep()
        {
            if (Time.realtimeSinceStartup - time2 >= 0.03f)
            {
                for (int i = 0; i < 4; i++)
                {
                    _imageList[i].Visible = false;
                }
                _imageList[index].Visible = true;
                index++;
                if (index == 4)
                {
                    index = 0;
                    count--;
                    if (count == 0)
                    {
                        _timerId = Time.realtimeSinceStartup;
                    }
                }
                time2 = Time.realtimeSinceStartup;
            }

        }
    }
}
