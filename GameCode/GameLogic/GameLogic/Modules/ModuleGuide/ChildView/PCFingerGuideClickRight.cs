﻿#region 模块信息
/*==========================================
// 文件名：PCFingerGuideClickRight
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/3/3 20:03:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuide
{
    public class PCFingerGuideClickRight:KContainer
    {
        private StateImage _imgRight;
        private RectTransform _rect;
        private Vector2 _offset;
        private Vector2 _pos; 
        private int count = 0;
        private int wait = 0;
        private int waitClick = 10;

        protected override void Awake()
        {
            _rect = GetChildComponent<RectTransform>("Container_guideArrow");
            _imgRight = GetChildComponent<StateImage>("Container_guideArrow/Image_mouse02");
            _pos = _rect.anchoredPosition;
            _offset = Vector2.zero;
            base.Awake();
        }

        protected override void OnEnable()
        {
            count = 0;
            wait = 0;
            _rect.anchoredPosition = _pos;
            waitClick = 10;
            _imgRight.Visible = false;
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        public void OnClickRight()
        {
            //_imgRight.
        }


        void Update()
        {
            if(waitClick>0)
            {
                waitClick--;
                return;
            }
            _imgRight.Visible = true;
            if (count <= -240)
            {
                wait++;
                if (wait == 10)
                {
                    count = 0;
                    wait = 0;
                    waitClick = 10;
                    _imgRight.Visible = false;
                }
            }
            else
            {
                count -= 8;
            }
            _offset.x = count;
            _rect.anchoredPosition = _pos - _offset;
        }

    }
}
