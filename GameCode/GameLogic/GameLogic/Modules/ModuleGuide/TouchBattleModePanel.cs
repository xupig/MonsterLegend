﻿using Common.Base;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuide
{
    public class TouchBattleModePanel : BasePanel
    {
        private KButton _ensureBtn;
        private KToggleGroup _toggleGroup;

        protected override void Awake()
        {
            base.Awake();
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _ensureBtn = GetChildComponent<KButton>("Button_queding");
            _toggleGroup = GetChildComponent<KToggleGroup>("Container_xuanzecaozuo/ToggleGroup_xuanze");
        }


        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.TouchBattleMode; }
        }

        public override void OnShow(object data)
        {
            _toggleGroup.SelectIndex = 0;
            AddEventListener();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        protected void AddEventListener()
        {
            _ensureBtn.onClick.AddListener(OnEnsureBtnClick);
        }

        protected void RemoveEventListener()
        {
            _ensureBtn.onClick.RemoveListener(OnEnsureBtnClick);
        }

        private void OnEnsureBtnClick()
        {
            DramaTrigger.ClickButton("LayerGuide/Container_TouchBattleModePanel/Button_queding,Button");
            if (_toggleGroup.SelectIndex == 0)
            {
                PlayerTouchBattleModeManager.GetInstance().Exit();
            }
            else
            {
                PlayerTouchBattleModeManager.GetInstance().Enter();
            }
            ClosePanel();
        }


    }
}
