﻿#region 模块信息
/*==========================================
// 文件名：PCFingerGuidePanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/3/3 19:56:29
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuide
{
    public class PCFingerGuidePanel:BasePanel
    {
        public static bool isShow = false;
        private PCFingerGuideClickRight _clickRight;
        private PCFingerGuideLeft _left;
        private PCFingerGuideRight _right;
        private PCFingerGuideScroll _leftRight;
        private PCFingerReset _reset;
        private int timeout = 3000;
        private string type;
        private string guideName;
        private float _touchTimeStamp = 0;
        private Vector3 _touchPosition;
        private const float DOUBLE_CLICK_TIME_SPAN = 0.4f;

        protected override void Awake()
        {
            _reset = AddChildComponent<PCFingerReset>("Container_shuangji");
            _clickRight = AddChildComponent<PCFingerGuideClickRight>("Container_clickright");
            _left = AddChildComponent<PCFingerGuideLeft>("Container_fangdaleft");
            _right = AddChildComponent<PCFingerGuideRight>("Container_fangdaright");
            _leftRight = AddChildComponent<PCFingerGuideScroll>("Container_fangda");
            OnClearData();
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            ModalMask.gameObject.AddComponent<KDummyButton>();
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.PCFingerGuide; }
        }

        public override void OnShow(object data)
        {
            isShow = true;
            TaskManager.Instance.ContinueAutoTask();
            SetData(data);
        }

        public override void SetData(object data)
        {
            OnClearData();
            base.SetData(data);
            string[] dataList = data as string[];
            type = dataList[0];
            if (dataList[0] == "left")
            {
                _left.Visible = true;
            }
            else if (dataList[0] == "right")
            {
                _right.Visible = true;
            }
            else if (dataList[0] == "leftright")
            {
                _leftRight.Visible = true;
            }
            else if(dataList[0] == "clickright")
            {
                _clickRight.Visible = true;
            }
            else
            {
                _reset.Visible = true;
            }
            timeout = int.Parse(dataList[1]) * 30 / 1000;
            guideName = dataList[2];
        }

        private void OnClearData()
        {
            _left.Visible = false;
            _right.Visible = false;
            _leftRight.Visible = false;
            _clickRight.Visible = false;
            _reset.Visible = false;
            _touchTimeStamp = 0;
            _touchPosition = Vector3.zero;
        }

        public override void OnClose()
        {
            isShow = false;
            OnClearData();
        }

        private bool CheckTouchPosition(Vector3 touchPosition)
        {
            return Vector3.Distance(touchPosition, _touchPosition) < 100f;
        }

        private void CheckClick()
        {
            if (type == "click")
            {
                if (Input.GetMouseButtonUp(0))//左键
                {
                    var now = Time.realtimeSinceStartup;
                    Vector3 touchPosition = Input.mousePosition;
                    if (_touchTimeStamp > 0)
                    {
                        if (now - _touchTimeStamp <= DOUBLE_CLICK_TIME_SPAN)
                        {
                            if (CheckTouchPosition(touchPosition))
                            {
                                CameraManager.GetInstance().OnDoubleTouchBlankSpace();
                                ID.Close();
                                DramaTrigger.FingerGuideEnd(guideName);
                            }
                            _touchTimeStamp = 0;
                            _touchPosition = Vector3.zero;
                            return;
                        }
                    }
                    _touchTimeStamp = now;
                    _touchPosition = touchPosition;
                }
            }
            else if(type == "clickright")
            {
               
            }
            else if(type == "left")
            {

            }
        }

        void Update()
        {
            if (timeout > 0)
            {
                timeout--;
                if (timeout == 0)
                {
                    ID.Close();
                    DramaTrigger.FingerGuideEnd(guideName);
                }
            }
            CheckClick();
        }

    }
}
