﻿#region 模块信息
/*==========================================
// 文件名：SpecialGuidePanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/8/27 15:07:25
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuide
{
    public class SpecialGuidePanel:BasePanel
    {
        private KButton _btn;
        protected override void Awake()
        {
            _btn = GetChildComponent<KButton>("Button_btn");
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.SpecialGuide; }
        }

        public override void OnShow(object data)
        {
            _btn.onClick.AddListener(OnClickHandler);
        }

        public override void OnClose()
        {
            _btn.onClick.RemoveListener(OnClickHandler);
        }

        private void OnClickHandler()
        {
            Id.Close();
        }
    }
}
