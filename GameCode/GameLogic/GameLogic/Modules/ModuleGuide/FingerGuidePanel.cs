﻿#region 模块信息
/*==========================================
// 文件名：FingerGuidePanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleGuide
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/1/5 14:03:42
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuide
{
    public class FingerGuidePanel:BasePanel
    {
        public static bool isShow = false;
        private FingerGuideLeft _left;
        private FingerGuideRight _right;
        private FingerGuideLeftRight _leftRight;
        private FingerReset _reset;
        private int timeout = 3000;
        private string type;
        private string guideName;
        private float _touchTimeStamp = 0;
        private Vector3 _touchPosition;
        private const float DOUBLE_CLICK_TIME_SPAN = 0.4f;

        protected override void Awake()
        {
            _left = AddChildComponent<FingerGuideLeft>("Container_fangdaleft");
            _right = AddChildComponent<FingerGuideRight>("Container_fangdaright");
            _leftRight = AddChildComponent<FingerGuideLeftRight>("Container_fangda");
            _reset = AddChildComponent<FingerReset>("Container_shuangji");
            OnClearData();
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            ModalMask.gameObject.AddComponent<KDummyButton>();
            base.Awake();
        }


        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.FingerGuide; }
        }

        public override void OnShow(object data)
        {
            isShow = true;
            TaskManager.Instance.ContinueAutoTask();
            SetData(data);
        }

        private void CheckClick()
        {
            if (type == "click")
            {
                if (Input.GetMouseButtonUp(0))
                {
                    var now = Time.realtimeSinceStartup;
                    Vector3 touchPosition = Input.mousePosition;
                    if (_touchTimeStamp > 0)
                    {
                        if (now - _touchTimeStamp <= DOUBLE_CLICK_TIME_SPAN)
                        {
                            if (CheckTouchPosition(touchPosition))
                            {
                                CameraManager.GetInstance().OnDoubleTouchBlankSpace();
                                ID.Close();
                                DramaTrigger.FingerGuideEnd(guideName);
                            }
                            _touchTimeStamp = 0;
                            _touchPosition = Vector3.zero;
                            return;
                        }
                    }
                    _touchTimeStamp = now;
                    _touchPosition = touchPosition;
                }
            }
        }

        private bool CheckTouchPosition(Vector3 touchPosition)
        {
            return Vector3.Distance(touchPosition, _touchPosition) < 100f;
        }

        public override void SetData(object data)
        {
            OnClearData();
            base.SetData(data);
            string[] dataList = data as string[];
            type = dataList[0];
            if(dataList[0] == "left")
            {
                _left.Visible = true;
            }
            else if(dataList[0] == "right")
            {
                _right.Visible = true;
            }
            else if(dataList[0] == "leftright")
            {
                _leftRight.Visible = true;
            }
            else
            {
                _reset.Visible = true;
            }
            timeout = int.Parse(dataList[1]) * 30 / 1000;
            guideName = dataList[2];
        }

        private void OnClearData()
        {
            _left.Visible = false;
            _right.Visible = false;
            _leftRight.Visible = false;
            _reset.Visible = false;
            _touchTimeStamp = 0;
            _touchPosition = Vector3.zero;
        }
        public override void OnClose()
        {
            isShow = false;
            OnClearData();
        }

        void Update()
        {
            if (timeout > 0)
            {
                timeout--;
                if (timeout == 0)
                {
                    ID.Close();
                    DramaTrigger.FingerGuideEnd(guideName);
                }
            }
            CheckClick();
        }

    }
}
