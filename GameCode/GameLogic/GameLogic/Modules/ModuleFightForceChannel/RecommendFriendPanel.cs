﻿using Common.Base;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;

namespace ModuleFightForceChannel
{
    public class RecommendFriendPanel : BasePanel
    {
        private KButton _addFriendButton;
        private KScrollView _scrollView;
        private KList _list;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.RecommendFriend; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_jujue");
            _addFriendButton = GetChildComponent<KButton>("Button_tianjiahaoyou");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_haoyou");
            _list = GetChildComponent<KList>("ScrollView_haoyou/mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        private void OnAddFriendBtnClick()
        {
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                RecommendFriendItem item = _list.ItemList[i] as RecommendFriendItem;
                if (item.GetCheck() == true)
                {
                    FriendManager.Instance.AddFriend(item.GetDbid());
                }
            }
            ClosePanel();
        }

        private void RefreshContent(List<PbHpMember> dataList)
        {
            _list.SetDataList<RecommendFriendItem>(dataList);
        }

        protected void AddEventListener()
        {
            _addFriendButton.onClick.AddListener(OnAddFriendBtnClick);
        }

        protected void RemoveEventListener()
        {
            _addFriendButton.onClick.RemoveListener(OnAddFriendBtnClick);
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void SetData(object data)
        {
            if (data is List<PbHpMember>)
            {
                RefreshContent(data as List<PbHpMember>);
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }
        
    }

    public class RecommendFriendItem : KList.KListItemBase
    {

        private IconContainer _iconContainer;
        private KToggle _check;
        private StateText _nameText;
        private PbHpMember _data;

        protected override void Awake()
        {
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _check = GetChildComponent<KToggle>("Toggle_Xuanze");
            _nameText = GetChildComponent<StateText>("Label_txtWanjiamingzi");
            _check.isOn = true;
        }

        public override void Dispose()
        {
            _data = null;
        }

        public bool GetCheck()
        {
            return _check.isOn;
        }

        public UInt64 GetDbid()
        {
            return _data.dbid;
        }

        private void RefreshContent()
        {
            _iconContainer.SetIcon(MogoPlayerUtils.GetSmallIcon((int)_data.vocation));
            _nameText.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.name);
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbHpMember;
                RefreshContent();
            }
        }

    }
}