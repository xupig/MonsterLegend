﻿#region 模块信息
/*==========================================
// 文件名：FightForceChannelPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleFightForceChannel
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/8 10:44:24
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager.SubSystem;
using System.Collections.Generic;
namespace ModuleFightForceChannel
{
    public class FightForceChannelPanel : BasePanel
    {
        private StateText _title;
        private StateText _equipTitle;
        private StateText _otherTitle;
        private KScrollView _equipScrollView;
        private KList _equipList;
        private KScrollView _otherScrollView;
        private KList _otherList;
        private KContainer _notShowAgainContainer;
        private KToggle _notShowAgainToggle;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.FightForceChannel; }
        }

        protected override void Awake()
        {
            _title = GetChildComponent<StateText>("Label_txtBiaoti");
            _title.CurrentText.text = MogoLanguageUtil.GetContent(100006);
            _equipTitle = GetChildComponent<StateText>("Label_txtZhuangbei");
            _equipTitle.CurrentText.text = MogoLanguageUtil.GetContent(100034);
            _otherTitle = GetChildComponent<StateText>("Label_txtQita");
            _otherTitle.CurrentText.text = MogoLanguageUtil.GetContent(100035);
            _equipScrollView = GetChildComponent<KScrollView>("ScrollView_zhuangbei");
            _equipScrollView.ScrollRect.Draggable = false; 
            _equipList = _equipScrollView.GetChildComponent<KList>("mask/content");
            _otherScrollView = GetChildComponent<KScrollView>("ScrollView_qita");
            _otherScrollView.ScrollRect.Draggable = false; 
            _otherList = _otherScrollView.GetChildComponent<KList>("mask/content");
            _notShowAgainContainer = GetChildComponent<KContainer>("Container_butishi");
            _notShowAgainToggle = _notShowAgainContainer.GetChildComponent<KToggle>("Toggle_Xuanze");
            // 隐藏背景遮罩
            GetChild("Container_panelBg/ScaleImage_sharedZhezhao").SetActive(false);
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            InitScrollList();
        }

        private void InitScrollList()
        {
            _equipList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _equipList.SetGap(13, 0);
            _otherList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _otherList.SetGap(13, 0);
        }

        public override void OnShow(object data)
        {
            _equipList.SetDataList<ChannelItem>(FightForceUtil.GetChannelData(FightForceChannelType.FightForceEquip));
            _otherList.SetDataList<ChannelItem>(FightForceUtil.GetChannelData(FightForceChannelType.FightForceOther));
        }

        public override void OnClose()
        {
            _equipList.RemoveAll();
            _otherList.RemoveAll();
            FightForceChannelManager.Instance.IsShowFightForceView = !_notShowAgainToggle.isOn;
        }
    }
}