﻿#region 模块信息
/*==========================================
// 文件名：LevelUpChannelPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleFightForceChannel
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/1/18 10:27:57
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Utils;
using Game.UI.UIComponent;
using ModuleFightForceChannel;
using System.Collections.Generic;

namespace ModuleFightForceChannel
{
    public class LevelUpChannelPanel : BasePanel
    {
        private StateText _title;
        private KScrollView _scrollView;
        private KList _levelUpChannelList;
        private KContainer _notShowAgainContainer;
        private KToggle _notShowAgainToggle;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.LevelUpChannel; }
        }

        protected override void Awake()
        {
            _title = GetChildComponent<StateText>("Label_txtBiaoti");
            _title.CurrentText.text = MogoLanguageUtil.GetContent(100007);
            _scrollView = GetChildComponent<KScrollView>("ScrollView_tujing");
            _levelUpChannelList = _scrollView.GetChildComponent<KList>("mask/content");
            _scrollView.ScrollRect.vertical = false;
            _scrollView.ScrollRect.horizontal = true;
            InitList();
            _notShowAgainContainer = GetChildComponent<KContainer>("Container_butishi");
            _notShowAgainToggle = _notShowAgainContainer.GetChildComponent<KToggle>("Toggle_Xuanze");
            _notShowAgainContainer.Visible = false;
            // 隐藏背景遮罩
            GetChild("Container_panelBg/ScaleImage_sharedZhezhao").SetActive(false);
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
        }

        private void InitList()
        {
            _levelUpChannelList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue);
        }

        public override void OnShow(object data)
        {
            List<ChannelItemData> channelList = FightForceUtil.GetChannelData(FightForceChannelType.LevelUp);
            _levelUpChannelList.SetDataList<ChannelItem>(channelList);
        }

        public override void OnClose()
        {

        }
    }
}
