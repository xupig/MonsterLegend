﻿#region 模块信息
/*==========================================
// 文件名：FightForceUtil
// 命名空间: GameLogic.GameLogic.Modules.ModuleFightForceChannel
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/1/18 10:38:35
// 描述说明：
// 其他：
//==========================================*/
#endregion

using GameData;
using System.Collections.Generic;
namespace ModuleFightForceChannel
{
    public enum FightForceChannelType
    {
        FightForceEquip = 1,
        FightForceOther = 2,
        LevelUp = 3,
    }

    public static class FightForceUtil
    {
        const int FIGHT_FORCE_EQUIP_CHANNEL = 1;
        const int FIGHT_FORCE_OTHER_CHANNEL = 2;
        const int LEVEL_UP_CHANNEL = 3;

        public static List<ChannelItemData> GetChannelData(FightForceChannelType channelType)
        {
            int type = (int)channelType;
            List<ChannelItemData> result = new List<ChannelItemData>();
            List<int> channelIdList = fightforce_level_channel_helper.GetFunctionIdList(type);
            List<int> nameIdList = fightforce_level_channel_helper.GetChannelName(type);
            List<int> viewIdList = fightforce_level_channel_helper.GetViewIdList(type);
            List<int> descIdList = fightforce_level_channel_helper.GetDescribeIdList(type);

            for (int i = 0; i < channelIdList.Count; i++)
            {
                ChannelItemData data = new ChannelItemData();
                data.functionID = channelIdList[i];
                data.iconId = function_helper.GetIconId(channelIdList[i]);
                data.buttonChineseId = nameIdList[i];
                data.viewID = viewIdList[i];
                data.describeId = descIdList[i];
                data.isOpen = function_helper.IsFunctionOpen(data.functionID);
                result.Add(data);
            }
            SortListByOpenState(result);
            return result;
        }

        private static void SortListByOpenState(List<ChannelItemData> result)
        {
            List<ChannelItemData> notOpenList = new List<ChannelItemData>();
            for (int i = result.Count - 1; i >= 0; i--)
            {
                if (result[i].isOpen == false)
                {
                    notOpenList.Add(result[i]);
                    result.RemoveAt(i);
                }
            }
            result.AddRange(notOpenList);
        }
    }
}