﻿#region 模块信息
/*==========================================
// 文件名：ChannelItemData
// 命名空间: GameLogic.GameLogic.Modules.ModuleFightForceChannel.ChannelItem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/8 13:50:15
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace ModuleFightForceChannel
{
    public class ChannelItemData
    {
        public int functionID;
        public int iconId;
        public int buttonChineseId;
        public int describeId;
        public int viewID;

        public bool isOpen;
    }
}