﻿#region 模块信息
/*==========================================
// 文件名：ChannelItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleFightForceChannel.ChannelItem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/8 10:55:34
// 描述说明：
// 其他：
//==========================================*/
#endregion
using Common.Base;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
namespace ModuleFightForceChannel
{
    public class ChannelItem : KList.KListItemBase
    {
        private KButton _goButton;
        private StateText _descTxt;
        private StateText _buttonTxt;
        private IconContainer _iconContainer;
        private ChannelItemData _channelData;

        public override object Data
        {
            get
            {
                return _channelData;
            }
            set
            {
                if (value != null)
                {
                    _channelData = value as ChannelItemData;
                    Refresh();
                }
            }
        }

        protected override void Awake()
        {
            _goButton = GetChildComponent<KButton>("Button_anjian");
            _descTxt = GetChildComponent<StateText>("Label_txtMiaoshu");
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _buttonTxt = GetChildComponent<StateText>("Button_anjian/label");
            AddEventListener();
        }

        private void Refresh()
        {
            RefreshIcon();
            RefreshDesc();
            RefreshButtonTxt();
        }

        private void RefreshIcon()
        {
            _iconContainer.SetIcon(_channelData.iconId);
            if (function_helper.IsFunctionOpen(_channelData.functionID) == false)
            {
                ImageWrapper wrapper = _iconContainer.transform.GetComponentInChildren<ImageWrapper>();
                if (wrapper != null)
                {
                    wrapper.SetGray(0.0f);
                }
            }
        }

        private void RefreshDesc()
        {
            if (function_helper.IsFunctionOpen(_channelData.functionID) == false)
            {
                _descTxt.CurrentText.text = function_helper.GetConditionDesc(_channelData.functionID);
                return;
            }
            if (_channelData.describeId != 0)
            {
                _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(_channelData.describeId);
            }
        }

        private void RefreshButtonTxt()
        {
            _buttonTxt.ChangeAllStateText(MogoLanguageUtil.GetContent(_channelData.buttonChineseId));
            if (function_helper.IsFunctionOpen(_channelData.functionID) == false)
            {
                SetButtonGray();
            }
        }

        private void GoPanel()
        {
            view_helper.OpenView(_channelData.viewID);
            UIManager.Instance.ClosePanel(PanelIdEnum.FightForceChannel);
            UIManager.Instance.ClosePanel(PanelIdEnum.LevelUpChannel);
        }

        private void SetButtonGray()
        {
            ImageWrapper[] images = transform.GetComponentsInChildren<ImageWrapper>();

            for (int i = 0; i < images.Length; i++)
            {
                images[i].SetGray(0.0f);
            }
            _goButton.interactable = false;
        }

        public override void Dispose()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _goButton.onClick.AddListener(GoPanel);
        }

       
        private void RemoveEventListener()
        {
            _goButton.onClick.RemoveListener(GoPanel);
        }

    }

}