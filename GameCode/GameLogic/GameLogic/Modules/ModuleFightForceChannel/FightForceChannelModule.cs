﻿#region 模块信息
/*==========================================
// 文件名：FightForceChannelModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleFightForceChannel
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/8 10:43:34
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
namespace ModuleFightForceChannel
{
    public class FightForceChannelModule : BaseModule
    {

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.FightForceChannel, "Container_FightForceChannel", MogoUILayer.LayerUIPanel, "ModuleFightForceChannel.FightForceChannelPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.LevelUpChannel, "Container_LevelUpChannel", MogoUILayer.LayerUIPanel, "ModuleFightForceChannel.LevelUpChannelPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.RecommendFriend, "Container_TeamRecommendFriend", MogoUILayer.LayerAlert, "ModuleFightForceChannel.RecommendFriendPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }
    }

}