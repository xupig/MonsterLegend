﻿#region 模块信息
/*==========================================
// 文件名：DetailBuffListPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleRewardBuff
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/3/2 13:21:01
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;
namespace ModuleRewardBuff
{
    public class BuffItem : KList.KListItemBase
    {
        private int buffId;
        private StateText _descTxt;
        private int leftTime;
        private uint timeId;
        private RectTransform _rectTran;

        public override object Data
        {
            get
            {
                return buffId;
            }
            set
            {
                buffId = (int)value;
                Refresh();
            }
        }

        private void Refresh()
        {
            // 毫秒转化成秒
            leftTime = Mathf.CeilToInt(PlayerAvatar.Player.bufferManager.GetBufferRemainTime(buffId));
            if (leftTime <= 0)
            {
                _descTxt.CurrentText.text = buff_helper.GetName(buffId) + "\n" + buff_helper.GetDesc(buffId);
            }
            else
            {
                RefreshTxt();
                timeId = TimerHeap.AddTimer(0, 1000, RefreshTxt);
            }
        }

        private void RefreshTxt()
        {
            if (leftTime <= 0)
            {
                TimerHeap.DelTimer(timeId);
            }
            _descTxt.CurrentText.text = buff_helper.GetName(buffId) + " " + GetBuffLeftTime() + "\n" + buff_helper.GetDesc(buffId);
            leftTime--;
        }

        private string GetBuffLeftTime()
        {
            if (leftTime == 0)
            {
                return string.Empty;
            }
            int days = leftTime / (3600 * 24);
            int secondsInDay = leftTime % (3600 * 24);
            int hours = secondsInDay / 3600;
            int secondsInHour = secondsInDay % 3600;
            int minutes = secondsInHour / 60;
            int seconds = secondsInHour % 60;
            if (days > 0)
            {
                return string.Format("{0}{1} {2:D2}:{3:D2}:{4:D2}", days, MogoLanguageUtil.GetContent(32547), hours, minutes, seconds);
            }
            else
            {
                return string.Format("{0:D2}:{1:D2}:{2:D2}", hours, minutes, seconds);
            }
        }

        protected override void Awake()
        {
            _descTxt = GetChildComponent<StateText>("Label_txtBuff");
            _rectTran = GetComponent<RectTransform>();
        }

        public override void Dispose()
        {
            TimerHeap.DelTimer(timeId);
        }

        public void DoLayout()
        {
            _rectTran.sizeDelta = new Vector2(_rectTran.sizeDelta.x, _descTxt.CurrentText.preferredHeight);
        }
    }

    public class DetailBuffListPanel : BasePanel
    {
        private StateText _titleTxt;
        private KList _buffList;
        private KContainer _container;
        private Vector3 small = new Vector3(1, 0.1f, 1);
        private KDummyButton _closeBtn;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.BuffDetail; }
        }

        protected override void Awake()
        {
            _container = GetChildComponent<KContainer>("Container_neirong");
            _container.GetComponent<RectTransform>().anchoredPosition = new Vector2(350.0f, -150.0f);
            _titleTxt = _container.GetChildComponent<StateText>("Label_txtBiaoti");
            _buffList = _container.GetChildComponent<KList>("ScrollView_content/mask/content");
            _buffList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _closeBtn = gameObject.AddComponent<KDummyButton>();
            AddEventListener();
        }

        private void AddEventListener()
        {
            _closeBtn.onClick.AddListener(HidePanel);
            EventDispatcher.AddEventListener(BuffEvents.BUFF_REFRESH, Refresh);
        }

        private void RemoveEventListener()
        {
            _closeBtn.onClick.RemoveListener(HidePanel);
            EventDispatcher.RemoveEventListener(BuffEvents.BUFF_REFRESH, Refresh);
        }

        public override void OnShow(object data)
        {
            _container.transform.localScale = small;
            TweenScale.Begin(_container.gameObject, 0.3f, Vector3.one);
            _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(71484);
            Refresh();
        }

        private List<int> cacheList = null;
        public void Refresh()
        {
            List<int> buffIdList = PlayerAvatar.Player.bufferManager.GetCurBufferIDList();
            List<int> needShowBuffList = new List<int>();
            for (int i = 0; i < buffIdList.Count; i++)
            {
                if (buff_helper.NeedShowInMainUI(buffIdList[i]) == true)
                {
                    needShowBuffList.Add(buffIdList[i]);
                }
            }
            if (cacheList == null || cacheList.Count != cacheList.Count)
            {
                cacheList = needShowBuffList;
            }
            else
            {
                return;
            }
            if (needShowBuffList.Count > 0)
            {
                _buffList.RemoveAll();
                _buffList.SetDataList<BuffItem>(needShowBuffList);
            }
            DoListLayout();
        }

        private void DoListLayout()
        {
            Vector2 startPosition = new Vector2(0, 0);
            float totalHeight = 0;
            for (int i = 0; i < _buffList.ItemList.Count; i++)
            {
                _buffList.ItemList[i].GetComponent<RectTransform>().anchoredPosition = startPosition;
                (_buffList.ItemList[i] as BuffItem).DoLayout();
                float textPreferHeight = (_buffList.ItemList[i] as BuffItem).GetComponent<RectTransform>().rect.size.y;
                totalHeight += textPreferHeight;
                startPosition += new Vector2(0, -textPreferHeight);
            }
            Vector2 size = _buffList.GetComponent<RectTransform>().rect.size;
            _buffList.GetComponent<RectTransform>().sizeDelta = new Vector2(size.x, totalHeight);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void HidePanel()
        {
            TweenScale tween = TweenScale.Begin(_container.gameObject, 0.3f, small, UITweener.Method.Linear);
            tween.onFinished = ClosePanel;
        }

        private void ClosePanel(UITweener tween)
        {
            ClosePanel();
        }

    }

}