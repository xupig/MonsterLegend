﻿using Common.Base;
using Common.Events;
using Common.Global;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;
using Common.Utils;

namespace ModuleRewardBuff
{
    public class RewardBuffDetailPanel:BasePanel
    {
        private RectTransform _rectContent;
        private KDummyButton _closeButton;
        private KList _buffList;

        protected override void Awake()
        {
            base.Awake();
            _rectContent = GetChildComponent<RectTransform>("Container_neirong");
            _buffList = GetChildComponent<KList>("Container_neirong/ScrollView_content/mask/content");
            _closeButton = gameObject.AddComponent<KDummyButton>();
            _buffList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _closeButton.onClick.AddListener(PlayHideAnimator);
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            if (data != null)
            {
                RectTransform rectTransform = data as RectTransform;
                RecalculateLocation(rectTransform);
            }
            RewardBuffManager.Instance.RequestRewardBuff();
            PlayShowAnimator();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.RewardBuffDetail; }
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<PbItemCompensateList>(RewardBuffEvents.RefreshRewardBuffList, ShowBuffList);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<PbItemCompensateList>(RewardBuffEvents.RefreshRewardBuffList, ShowBuffList);
        }

        private void ShowBuffList(PbItemCompensateList compensateList)
        {
            List<string> contentList = GetShowBuffDesc(compensateList);
            _buffList.SetDataList<RewardBuffItem>(contentList);
            AdjustProperityList();
        }

        private List<string> GetShowBuffDesc(PbItemCompensateList compensateList)
        {
            List<string> result = new List<string>();
            for (int i = 0; i < compensateList.items.Count; i++)
            {
                string content = MogoLanguageUtil.GetContent(2682, item_helper.GetName((int)compensateList.items[i].item_id), string.Format("{0}%", (int)(compensateList.items[i].rate * 100)));
                result.Add(content);
            }
            return result;
        }

        private void RecalculateLocation(RectTransform rectTransform)
        {
            Vector2 size = rectTransform.sizeDelta;
            Vector2 screenPosition = RectTransformUtility.WorldToScreenPoint(UIManager.Instance.UICamera, rectTransform.position) / Global.Scale;
            screenPosition.y -= Screen.height / Global.Scale;
            RectTransform rectPanel = gameObject.GetComponent<RectTransform>();
            Vector2 sizeDelta = rectPanel.sizeDelta;
            Vector2 pivot = rectPanel.pivot;
            Vector2 parentOffect = rectPanel.anchoredPosition - new Vector2(sizeDelta.x * pivot.x, -(rectPanel.sizeDelta.y * pivot.y));
            Vector2 topLeftPosition = screenPosition - parentOffect;
            _rectContent.pivot = new Vector2(0, 1);
            _rectContent.anchoredPosition = topLeftPosition + new Vector2(size.x/2, -size.y);
        }

        private void AdjustProperityList()
        {
            Vector2 offsetVector = new Vector2(_buffList.Padding.left, _buffList.Padding.top);
            for (int i = 0; i < _buffList.ItemList.Count; i++)
            {
                RewardBuffItem item = _buffList.ItemList[i] as RewardBuffItem;
                StateText[] texts = item.transform.GetComponentsInChildren<StateText>();
                for (int j = 0; j < texts.Length; j++)
                {
                    texts[j].RecalculateAllStateHeight();
                }
                item.RecalculateSize();
                RectTransform rectTransform = item.GetComponent<RectTransform>();
                rectTransform.anchoredPosition = offsetVector;
                offsetVector.y -= Mathf.CeilToInt(rectTransform.rect.height);
            }

            _buffList.RecalculateSize();
        }

        private void PlayShowAnimator()
        {
            _rectContent.localScale = new Vector3(1, 0);
            TweenScale.Begin(_rectContent.gameObject, 0.3f, Vector3.one);
        }

        private void PlayHideAnimator()
        {
            TweenScale.Begin(_rectContent.gameObject, 0.3f, new Vector3(1, 0), UITweener.Method.Linear, HidePanel);
        }

        private void HidePanel(UITweener tween)
        {
            ClosePanel();
        }
    }

    
}
