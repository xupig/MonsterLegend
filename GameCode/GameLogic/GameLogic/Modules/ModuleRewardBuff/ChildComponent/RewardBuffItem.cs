﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleRewardBuff
{
    public class RewardBuffItem:KList.KListItemBase
    {
        private StateText _txt;
        private string _data;
        protected override void Awake()
        {
            _txt = GetChildComponent<StateText>("Label_txtBuff");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as string;
                _txt.ChangeAllStateText(_data);
            }
        }

        public StateText Label
        {
            get
            {
                return _txt;
            }
        }

        public override void Dispose()
        {
            _data = null;
        }
    }
}
