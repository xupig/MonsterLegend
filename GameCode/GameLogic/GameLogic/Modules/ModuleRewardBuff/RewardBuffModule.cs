﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleRewardBuff
{
    public class RewardBuffModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.RewardBuffTip, "Container_RewardBuffTip", MogoUILayer.LayerAlert, "ModuleRewardBuff.RewardBuffTipPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.RewardBuffDetail, "Container_RewardBuffDetailPanel", MogoUILayer.LayerUIToolTip, "ModuleRewardBuff.RewardBuffDetailPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.BuffDetail, "Container_RewardBuffDetailPanel", MogoUILayer.LayerUIToolTip, "ModuleRewardBuff.DetailBuffListPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }
    }
}
