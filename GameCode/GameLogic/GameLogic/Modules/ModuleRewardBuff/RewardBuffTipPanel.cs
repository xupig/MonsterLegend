﻿using Common.Base;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using GameData;
using ModuleReward;

namespace ModuleRewardBuff
{
    public enum PanelType
    {
        RewardBuff = 1,     // 盈福奖励
        ReturnReward = 2,   // 回归奖励
        Both = 3,           // 盈福奖励和回归奖励同时显示
    }

    public class RewardBuffTipPanel : BasePanel
    {
        private StateText _contentTxt;
        private StateText _titleTxt;
        private KButton _getReturnRewardBtn;
        private KButton _lookBuffEffectBtn;
        private Locater _btnLocaterOne;
        private Locater _btnLocaterTwo;
        private Vector2 _btnVectorOne;
        private Vector2 _btnVectorTwo;

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("Container_panelBg/ScaleImage_sharedZhezhao");
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _titleTxt = GetChildComponent<StateText>("Label_txtBiaoti");
            _contentTxt = GetChildComponent<StateText>("Label_txtNr");
            _lookBuffEffectBtn = GetChildComponent<KButton>("Button_chakan");
            _btnLocaterOne = _lookBuffEffectBtn.gameObject.AddComponent<Locater>();
            _btnVectorOne = _btnLocaterOne.Position;
            _getReturnRewardBtn = GetChildComponent<KButton>("Button_jiangli");
            _btnLocaterTwo = _getReturnRewardBtn.gameObject.AddComponent<Locater>();
            _btnVectorTwo = _btnLocaterTwo.Position;
        }

        public override void OnShow(object data)
        {
            if (data is int)
            {
                PanelType type = (PanelType)data;
                switch (type)
                {
                    case PanelType.RewardBuff:
                        ShowRewardBuffContent();
                        break;
                    case PanelType.ReturnReward:
                        ShowReturnRewardContent();
                        break;
                    case PanelType.Both:
                        ShowRewardBuffAndReturnReward();
                        break;
                }
            }
            AddEventListener();
        }

        private void ShowRewardBuffContent()
        {
            _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(6308004);
            _contentTxt.CurrentText.text = string.Format("{0}\n\n{1}", MogoLanguageUtil.GetContent(6308003), MogoLanguageUtil.GetContent(6308002));
            _lookBuffEffectBtn.Visible = true;
            _btnLocaterOne.Position = (_btnVectorOne + _btnVectorTwo) * 0.5f;
            (_lookBuffEffectBtn as KShrinkableButton).RefreshRectTransform();
            _getReturnRewardBtn.Visible = false;
        }

        private void ShowReturnRewardContent()
        {
            _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(124011);
            _contentTxt.CurrentText.text = MogoLanguageUtil.GetContent(124012);
            _getReturnRewardBtn.Visible = true;
            _btnLocaterTwo.Position = (_btnVectorOne + _btnVectorTwo) * 0.5f;
            (_getReturnRewardBtn as KShrinkableButton).RefreshRectTransform();
            _lookBuffEffectBtn.Visible = false;
        }

        private void ShowRewardBuffAndReturnReward()
        {
            _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(124014);
            _contentTxt.CurrentText.text = MogoLanguageUtil.GetContent(124015);
            _lookBuffEffectBtn.Visible = true;
            _btnLocaterOne.Position = _btnVectorOne;
            (_lookBuffEffectBtn as KShrinkableButton).RefreshRectTransform();
            _getReturnRewardBtn.Visible = true;
            _btnLocaterTwo.Position = _btnVectorTwo;
            (_getReturnRewardBtn as KShrinkableButton).RefreshRectTransform();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.RewardBuffTip; }
        }

        private void AddEventListener()
        {
            _lookBuffEffectBtn.onClick.AddListener(ShowRewardBuffDetail);
            _getReturnRewardBtn.onClick.AddListener(ShowReturnReward);
        }

        private void RemoveEventListener()
        {
            _lookBuffEffectBtn.onClick.RemoveListener(ShowRewardBuffDetail);
            _getReturnRewardBtn.onClick.RemoveListener(ShowReturnReward);
        }

        private void ShowReturnReward()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.Reward, RewardViewType.PLAYER_RETURN_REWARD_VIEW);
            ClosePanel();
        }

        private void ShowRewardBuffDetail()
        {
            ClosePanel();
            UIManager.Instance.ShowPanel(PanelIdEnum.RewardBuffDetail);
        }
    }
}
