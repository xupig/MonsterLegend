﻿#region 模块信息
/*==========================================
// 文件名：AchievementObtainTips
// 命名空间: GameLogic.GameLogic.Modules.ModuleAchievement
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/10/29 15:02:25
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using UnityEngine;
namespace ModuleAchievement
{
    public class AchievementObtainTips : BasePanel
    {
        private StateText _achievementNameTxt;
        private string _achievementNameTemplate;
        private KContainer _contentContainer;
        Vector3 startPosition;
        private Vector3 endPosition;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.AchievementObtainTips; }
        }

        protected override void Awake()
        {
            _contentContainer = GetChildComponent<KContainer>("Container_chengjiutishi");
            _achievementNameTxt = GetChildComponent<StateText>("Container_chengjiutishi/Label_txtHuodechengjiu");
            _achievementNameTemplate = _achievementNameTxt.CurrentText.text;
            InitShowPosition();
        }

        private void InitShowPosition()
        {
            float width = _contentContainer.GetComponent<RectTransform>().rect.width;
            float parentWidth = GetComponent<RectTransform>().rect.width;
            float startX = (parentWidth - width) * 0.5f;
            startPosition = new Vector3(startX, -640, 0);
            endPosition = new Vector3(startX, -500, 0);
            _contentContainer.transform.GetComponent<RectTransform>().anchoredPosition = startPosition;

        }

        public override void OnShow(object data)
        {
            if (data is int)
            {
                _achievementNameTxt.CurrentText.text = string.Format(_achievementNameTemplate, achievement_helper.GetAchievementName((int)data));
            }
            TweenPosition tweenPosition = TweenPosition.Begin(_contentContainer.gameObject, 0.5f, endPosition, UITweener.Method.BounceIn);
            tweenPosition.onFinished = OnFinished;
        }

        private void OnFinished(UITweener tween)
        {
            TimerHeap.AddTimer(3000, 0, ClosePanel);
        }

        public override void OnClose()
        {

        }
    }

}