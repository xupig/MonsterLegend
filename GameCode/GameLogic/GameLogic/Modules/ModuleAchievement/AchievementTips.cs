﻿#region 模块信息
/*==========================================
// 文件名：AchievementTips
// 命名空间: GameLogic.GameLogic.Modules.ModuleAchievement
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/10/29 15:02:07
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using System.Collections.Generic;
using UnityEngine;
using GameMain.GlobalManager.SubSystem;
using GameMain.GlobalManager;
namespace ModuleAchievement
{
    public class AchievementTips : BasePanel
    {
        private StateText _achievementName;
        private StateText _achievementDesc;
        private RewardGrid _rewardOne;
        private RewardGrid _rewardTwo;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.AchievementTips; }
        }

        protected override void Awake()
        {
            _achievementName = GetChildComponent<StateText>("Container_chengjiumingcheng/Label_chengjiuMingzi");
            _achievementDesc = GetChildComponent<StateText>("Container_chengjiushuoming/Label_xiangxishuoming");
            _rewardOne = AddChildComponent<RewardGrid>("Container_jiangli/Container_jiangli01");
            _rewardTwo = AddChildComponent<RewardGrid>("Container_jiangli/Container_jiangli02");
            CloseBtn = GetChildComponent<KButton>("Button_close");
        }

        public override void OnShow(object data)
        {
            if (data is int)
            {
                Refresh((int)data);
            }
            else if (data is AchievementTipsParam)
            {
                AchievementTipsParam info = data as AchievementTipsParam;
                SetPosition(info);
                Refresh(info.achieveId);
            }
        }

        private void SetPosition(AchievementTipsParam info)
        {
            Vector2 screenPos = RectTransformUtility.WorldToScreenPoint(UIManager.Instance.UICamera, info.targetPos);
            Vector2 destPos = Vector2.zero;
            RectTransform parentRect = transform.parent.gameObject.GetComponent<RectTransform>();
            RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRect, screenPos, UIManager.Instance.UICamera, out destPos);
            RectTransform rect = gameObject.GetComponent<RectTransform>();
            RectTransform bgRect = GetChildComponent<RectTransform>("Container_background/ScaleImage_miaoshudi");
            RectTransform closeBtnRect = GetChildComponent<RectTransform>("Button_close");
            Vector2 size = new Vector2(bgRect.sizeDelta.x + closeBtnRect.sizeDelta.x, bgRect.sizeDelta.y);
            destPos.x += size.x * rect.pivot.x;
            destPos.y -= size.y * rect.pivot.y;
            destPos.y -= info.targetSizeDelta.y;
            //如果界面超出了屏幕底部
            float bgPivotHeight = bgRect.sizeDelta.y * (1 - rect.pivot.y);
            float canvasHeight = UIManager.CANVAS_HEIGHT;
            if (Mathf.Abs(destPos.y) + bgPivotHeight > canvasHeight)
            {
                float deltaHeight = Mathf.Abs(destPos.y) + bgPivotHeight - canvasHeight;
                destPos.y += deltaHeight;
                destPos.x += info.targetSizeDelta.x;
            }
            rect.anchoredPosition = destPos;
        }

        private void Refresh(int achieveId)
        {
            _achievementName.CurrentText.text = achievement_helper.GetAchievementName(achieveId);
            _achievementDesc.CurrentText.text = achievement_helper.GetAchievementDesc(achieveId);
            RefreshReward(achieveId);
        }

        private void RefreshReward(int achieveId)
        {
            List<RewardData> rewardDataList = achievement_helper.GetAchievementReward(achieveId);
            SetRewardItem(rewardDataList, _rewardOne, 1);
            SetRewardItem(rewardDataList, _rewardTwo, 2);
        }

        private void SetRewardItem(List<RewardData> rewardDataList, RewardGrid _rewardGrid, int count)
        {
            if (rewardDataList.Count >= count)
            {
                _rewardGrid.Visible = true;
                _rewardGrid.Data = rewardDataList[count - 1];
            }
            else
            {
                _rewardGrid.Visible = false;
            }
        }

        public override void OnClose()
        {
        }
    }
}