﻿#region 模块信息
/*==========================================
// 文件名：AchievementModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleAchievement
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/10/29 15:02:42
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
namespace ModuleAchievement
{
    public class AchievementModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.AchievementObtainTips, "Container_AchievementGetTips", MogoUILayer.LayerAlert, "ModuleAchievement.AchievementObtainTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.AchievementTips, "Container_AchievementTips", MogoUILayer.LayerUINotice, "ModuleAchievement.AchievementTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }
    }
}