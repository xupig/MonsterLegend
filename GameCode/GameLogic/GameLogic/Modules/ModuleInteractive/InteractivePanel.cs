﻿#region 模块信息
/*==========================================
// 文件名：InteractivePanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleInteractive
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/6/12 11:11:00
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using ModuleFriend;
using ModuleMail;
using ModuleTeam;
using ModuleTeam.ChildView;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleInteractive
{
    public class InteractivePanel:BasePanel
    {
        private const int FRIEND = 0;
        private const int TEAM = 1;
        private const int EMAIL = 2;

        //private TeamView _teamView;
        //private MailView _mailView;
        private FriendView _friendView;
        private KToggleGroup _toggleGroup;

        protected override void Awake()
        {
            //_teamView = AddChildComponent<TeamView>("Container_team");
            //_mailView = AddChildComponent<MailView>("Container_mail");
            _friendView = AddChildComponent<FriendView>("Container_friend");
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");

            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            SetTabList(17, _toggleGroup);
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Interactive; }
        }


        public override void OnShow(object data)
        {
            SetData(data);
            AddEventListener();
            RefreshFriendPoint();
            RefreshTeamPoint();
            RefreshMailPoint();
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            if(data != null)
            {
                if(data is int)
                {
                    _toggleGroup.SelectIndex = (int)data - 1;
                    ChangeSelectedIndex( (int)data - 1,null);
                }
                else if(data is int[])
                {
                    int[] list = data as int[];
                    if(list.Length == 1)
                    {
                        _toggleGroup.SelectIndex = list[0] - 1;
                        ChangeSelectedIndex(list[0] - 1,null);
                    }
                    else if (list.Length == 2)
                    {
                        _toggleGroup.SelectIndex = list[0] - 1;
                        ChangeSelectedIndex(list[0] - 1, list[1]);
                    }
                    else
                    {
                        _toggleGroup.SelectIndex = list[0] - 1;
                        ChangeSelectedIndex(list[0] - 1, list.ToList().GetRange(1, list.Length - 1).ToArray());
                    }
                }
                else if (data is InteractivePanelParameter)
                {
                    InteractivePanelParameter panelParameter = data as InteractivePanelParameter;
                    _toggleGroup.SelectIndex = panelParameter.FirstTab - 1;
                    ChangeSelectedIndex(panelParameter.FirstTab - 1, panelParameter);
                }
            }
            else
            {
                _toggleGroup.SelectIndex = 0;
                ChangeSelectedIndex(_toggleGroup.SelectIndex, null);
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
            PanelIdEnum.Mail.Close();
            PanelIdEnum.Team.Close();
            _friendView.OnHide();
        }

        private void AddEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            EventDispatcher.AddEventListener(MailEvents.MAIL_ADD_FRIEND, ShowFriendView);

            EventDispatcher.AddEventListener(FriendEvents.FRIEND_POINT_CHANGE, RefreshFriendPoint);
            EventDispatcher.AddEventListener(TeamEvent.TEAM_POINT_REFRESH, RefreshTeamPoint);
            EventDispatcher.AddEventListener(MailEvents.MAIL_POINT_REFRESH, RefreshMailPoint);
        }

        private void RemoveEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            EventDispatcher.RemoveEventListener(MailEvents.MAIL_ADD_FRIEND, ShowFriendView);

            EventDispatcher.RemoveEventListener(FriendEvents.FRIEND_POINT_CHANGE, RefreshFriendPoint);
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_POINT_REFRESH, RefreshTeamPoint);
            EventDispatcher.RemoveEventListener(MailEvents.MAIL_POINT_REFRESH, RefreshMailPoint);
        }

        private void ShowFriendView()
        {
            SetData(new int[] { 1, 4 });
        }

        private void OnSelectedIndexChanged(KToggleGroup toggleGroup,int index)
        {
            ChangeSelectedIndex(index, null);
        }

        private void RefreshFriendPoint()
        {
            _toggleGroup[FRIEND].SetGreenPoint(PlayerDataManager.Instance.InteractivePointManager.IsHaveNewFriendInfo);
        }

        private void RefreshTeamPoint()
        {
            _toggleGroup[TEAM].SetGreenPoint(PlayerDataManager.Instance.InteractivePointManager.IsHaveNewTeamInfo);
        }

        private void RefreshMailPoint()
        {
            _toggleGroup[EMAIL].SetGreenPoint(PlayerDataManager.Instance.InteractivePointManager.IsHaveNewMailInfo);
        }

        private void ChangeSelectedIndex(int index,object data)
        {
            switch (index)
            {
                case FRIEND:
                    PanelIdEnum.Team.Close();
                    PanelIdEnum.Mail.Close();
                    _friendView.OnShow(data);
                    break;
                case TEAM:
                    PanelIdEnum.Mail.Close();
                    _friendView.OnHide();
                    if (data is InteractivePanelParameter)
                    {
                        InteractivePanelParameter interactivePanelParameter = data as InteractivePanelParameter;
                        TeamPanelParamter teamPanelParamter = new TeamPanelParamter(interactivePanelParameter.SecondTab, 
                            interactivePanelParameter.ThirdTab, interactivePanelParameter.InstanceId);
                        PanelIdEnum.Team.Show(teamPanelParamter);
                    }
                    else
                    {
                        PanelIdEnum.Team.Show(data);
                    }
                    break;
                case EMAIL:
                    _friendView.OnHide();
                    PanelIdEnum.Team.Close();
                    PanelIdEnum.Mail.Show(data);
                    break;
                default:
                    break;
            }
        }
    }
}
