﻿#region 模块信息
/*==========================================
// 文件名：InteractiveModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleInteractive
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/6/12 11:09:29
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleInteractive
{
    public class InteractiveModule:BaseModule
    {
        public override void Init()
        {
            
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

    }
}
