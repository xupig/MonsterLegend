﻿using ACTSystem;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine;
using MogoEngine.Events;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace ModuleField
{
    public class FieldBossIntroducePanel : BasePanel
    {
        private StateText _nameText;
        private KContainer _nameContainer;
        private KContainer _nameBgContainer;

        private const float SCALE_TIME = 0.15f;
        private const float MOVE_TIME = 0.25f;
        private const float INIT_ROTATE = 90;
        private const float ROTATE_SPEED = 0.7f;

        private RectTransform _nameLeftBgRect;
        private RectTransform _nameRightBgRect;
        private KContainer _warningContainer;
        private StateImage _warningImage1;
        private StateImage _warningImage2;

        private ModelComponent _model;

        private uint _rotateTimer;
        private uint _shrinkTimer;

        protected override void Awake()
        {
            _nameLeftBgRect = GetChildComponent<RectTransform>("Container_bottomLeft/Container_desc/Image_Bossmodian01");
            _nameRightBgRect = GetChildComponent<RectTransform>("Container_bottomLeft/Container_desc/Image_Bossmodian02");
            _nameBgContainer = GetChildComponent<KContainer>("Container_bottomLeft/Container_desc");
            _nameContainer = GetChildComponent<KContainer>("Container_bottomLeft/Container_desc/Container_name");
            _nameText = GetChildComponent<StateText>("Container_bottomLeft/Container_desc/Container_name/Label_txtBossmingcheng");
            _warningContainer = GetChildComponent<KContainer>("Container_bottomLeft/Container_jinggao");
            _warningImage1 = GetChildComponent<StateImage>("Container_bottomLeft/Container_jinggao/Image_jinggao01");
            _warningImage2 = GetChildComponent<StateImage>("Container_bottomLeft/Container_jinggao/Image_jinggao02");
            _warningImage2.Visible = false;
            _model = AddChildComponent<ModelComponent>("Container_bottomLeft/Container_model");
            ModalMask = GetChildComponent<StateImage>("Container_zhezhao/ScaleImage_sharedZhezhao");
            InitBgView();
        }

        private void InitBgView()
        {
            _nameLeftBgRect.pivot = new Vector2(0.5f, 0.5f);
            _nameRightBgRect.pivot = new Vector2(0.5f, 0.5f);
            Vector3 localposition = _nameLeftBgRect.localPosition;
            localposition.x += _nameLeftBgRect.sizeDelta.x * 0.5f;
            localposition.y -= _nameLeftBgRect.sizeDelta.y * 0.5f;
            _nameLeftBgRect.localPosition = localposition;
            localposition = _nameRightBgRect.localPosition;
            localposition.x += _nameRightBgRect.sizeDelta.x * 0.5f;
            localposition.y -= _nameRightBgRect.sizeDelta.y * 0.5f;
            _nameRightBgRect.localPosition = localposition;
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.FieldBossIntroduce; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            uint _entityId = (uint)data;
            EntityMonster _entity = MogoWorld.GetEntity(_entityId) as EntityMonster;
            if (_entity == null)
            {
                ClosePanel();
                return;
            }
            RefreshContent((int)_entity.monster_id);
        }


        public override void OnClose()
        {
            ResetTimer();
            RemoveEventListener();
        }

        private void CloseBossIntroduce()
        {
            TweenViewMove.Begin(_nameContainer.gameObject, MoveType.Hide, MoveDirection.Right);
            TimerHeap.AddTimer(300, 0, ClosePanel);
        }

        private void RefreshContent(int monsterId)
        {
            RefreshMonsterName(monsterId);
            _model.DragComponent.Dragable = false;
            _model.LoadMonsterModel(monsterId, OnMosterLoaded);
            PlayAnimation();
        }

        private void OnMosterLoaded(ACTActor actor)
        {
            ResetTimer();
            actor.gameObject.transform.eulerAngles = new Vector3(0, INIT_ROTATE, 0);
            _rotateTimer = TimerHeap.AddTimer(0, 20, OnRotateStep, actor.gameObject);
            _shrinkTimer = TimerHeap.AddTimer(0, 200, OnShrinkStep);
        }

        private void ResetTimer()
        {
            if (_rotateTimer != 0)
            {
                TimerHeap.DelTimer(_rotateTimer);
                _rotateTimer = 0;
            }
            if (_shrinkTimer != 0)
            {
                TimerHeap.DelTimer(_shrinkTimer);
                _shrinkTimer = 0;
            }
        }

        private void OnRotateStep(GameObject go)
        {
            go.transform.Rotate(new Vector3(0, ROTATE_SPEED, 0));
        }

        private int frame = 0;
        private void OnShrinkStep()
        {
            if (frame == 0)
            {
                frame = 1;
                _warningImage1.Visible = false;
                _warningImage2.Visible = true;
            }
            else
            {
                frame = 0;
                _warningImage1.Visible = true;
                _warningImage2.Visible = false;
            }
        }

        private void PlayAnimation()
        {
            _nameLeftBgRect.gameObject.SetActive(false);
            _nameLeftBgRect.transform.localScale = new Vector3(3, 3, 1);
            _nameRightBgRect.gameObject.SetActive(false);
            _nameRightBgRect.transform.localScale = new Vector3(3, 3, 1);
            PlayLeftBgAnimation();
        }

        private void PlayLeftBgAnimation()
        {
            _nameLeftBgRect.gameObject.SetActive(true);
            TweenScale.Begin(_nameLeftBgRect.gameObject, SCALE_TIME, Vector3.one, UITweener.Method.EaseOut, PlayRightBgAnimation);
        }

        private void PlayRightBgAnimation(UITweener tween)
        {
            _nameRightBgRect.gameObject.SetActive(true);
            TweenScale.Begin(_nameRightBgRect.gameObject, SCALE_TIME, Vector3.one, UITweener.Method.EaseOut, PlayMoveAnimation);
        }

        private void PlayMoveAnimation(UITweener tween)
        {
            TweenViewMove.Begin(_nameContainer.gameObject, MoveType.Show, MoveDirection.Right);
            TimerHeap.AddTimer(3000, 0, CloseBossIntroduce);
        }

        private void RefreshMonsterName(int monsterId)
        {
            _nameText.CurrentText.text = monster_helper.GetMonsterName(monsterId);
        }

        private void AddEventListener()
        {
            
        }

        private void RemoveEventListener()
        {

        }

    }
}
