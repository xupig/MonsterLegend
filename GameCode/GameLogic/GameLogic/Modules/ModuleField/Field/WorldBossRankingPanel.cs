﻿#region 模块信息
/*==========================================
// 文件名：WorldBossRankingPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleField
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/2/18 15:03:39
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Utils;
namespace ModuleField
{
    public class WorldBossRankItem : KList.KListItemBase
    {
        private StateText _rank;
        private StateText _playerName;
        private StateText _score;

        private PbWorldBossRankInfo _data;

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbWorldBossRankInfo;
                Refresh();
            }
        }

        private void Refresh()
        {
            string score = _data.hit.ToString();
            string playerName = MogoProtoUtils.ParseByteArrToString(_data.name_bytes);
            string ranking = _data.ranking.ToString();
            if (_data.is_team == 1)
            {
                playerName += MogoLanguageUtil.GetContent(72824);
            }
            if (_data.is_self == 1)
            {
                _score.CurrentText.text = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, score);
                _playerName.CurrentText.text = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, playerName);
                _rank.CurrentText.text = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, ranking);
            }
            else
            {
                _rank.CurrentText.text = _data.ranking.ToString();
                _playerName.CurrentText.text = playerName;
                _score.CurrentText.text = _data.hit.ToString();
            }
        }

        protected override void Awake()
        {
            _rank = GetChildComponent<StateText>("Label_txtPaiming");
            _playerName = GetChildComponent<StateText>("Label_txtWanjimingzi");
            _score = GetChildComponent<StateText>("Label_txtShuzhi");
        }

        public override void Dispose()
        {

        }
    }

    public class WorldBossRankingPanel : BasePanel
    {
        private KPageableList _rankList;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.WorldBossRanking; }
        }

        protected override void Awake()
        {
            _rankList = GetChildComponent<KPageableList>("List_paimingxinxi");
            _rankList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _rankList.SetGap(5, 0);
        }

        public override void OnShow(object data)
        {
            RefreshRankingData();
        }

        public override void SetData(object data)
        {
            RefreshRankingData();
        }

        private void RefreshRankingData()
        {
            PbWorldBossRankInfoList pbInfo = WorldBossRankManager.Instance.GetLastestWorldInfo();
            if(pbInfo == null || pbInfo.rank_list == null)
            {
                return;
            }
            _rankList.SetDataList<WorldBossRankItem>(pbInfo.rank_list);
        }

        public override void OnClose()
        {

        }
    }

}