﻿using Common.Base;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleField
{
    public class FieldWorldMapPanel:BasePanel
    {
        private FieldWorldMapView _mapView;

        protected override void Awake()
        {
            base.Awake();
            CloseBtn = GetChildComponent<KButton>("Container_bg/Button_close");
            _mapView = gameObject.AddComponent<FieldWorldMapView>();

        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.FieldWorldMap; }
        }

        public override void OnShow(object data)
        {
            AddListener();
        }

        public override void OnClose()
        {
            RemoveListener();
        }

        private void AddListener()
        {

        }

        private void RemoveListener()
        {

        }

    }
}
