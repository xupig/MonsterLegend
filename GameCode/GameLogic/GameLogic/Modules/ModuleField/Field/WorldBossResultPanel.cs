﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using MogoEngine.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleField
{
    public class WorldBossResultPanel : BasePanel
    {
        private StateText _titleTxt;
        private KButton _ensureBtn;
        // 个人MVP
        private IconContainer _mvpIcon;
        private StateText _mvpLevel;
        private StateText _mvpName;
        private StateText _mvpDamage;
        private ItemGrid _mvpRewardItem;
        private StateText _mvpRewardCount;
        // 补刀奖
        private IconContainer _lastIcon;
        private StateText _lastLevel;
        private StateText _lastName;
        private StateText _lastDamage;
        private ItemGrid _lastRewardItem;
        private StateText _lastRewardCount;
        //// 队伍MVP奖
        //private IconContainer _teamMvpIcon;
        //private StateText _teamLeaderLevel;
        //private StateText _teamLeaderName;
        //private StateText _teamLeaderDesc;
        //private ItemGrid _teamMVPRewardItem;
        //private StateText _teamMVPRewardCount;

        private KPageableList _myRewardList;
        private KScrollView _scrollView;

        private string _titleContent;
        private float _maskWidth;
        private float _itemWidth;
        private const int GAP = 50;

        protected override void Awake()
        {
            _titleTxt = GetChildComponent<StateText>("Label_txtbiaoti");
            _ensureBtn = GetChildComponent<KButton>("Button_queding");
            _mvpIcon = AddChildComponent<IconContainer>("Container_gerenMVP/Container_touxiang/Container_icon");
            _mvpLevel = GetChildComponent<StateText>("Container_gerenMVP/Container_touxiang/Container_dengji/Label_txtDengji");
            _mvpName = GetChildComponent<StateText>("Container_gerenMVP/Label_txtWanjiamingzizi");
            _mvpDamage = GetChildComponent<StateText>("Container_gerenMVP/Label_txtShanghai");
            _mvpRewardItem = GetChildComponent<ItemGrid>("Container_gerenMVP/Container_wupin");
            _mvpRewardCount = GetChildComponent<StateText>("Container_gerenMVP/Label_txtShuliang");
            _lastIcon = AddChildComponent<IconContainer>("Container_budao/Container_touxiang/Container_icon");
            _lastLevel = GetChildComponent<StateText>("Container_budao/Container_touxiang/Container_dengji/Label_txtDengji");
            _lastName = GetChildComponent<StateText>("Container_budao/Label_txtWanjiamingzizi");
            _lastDamage = GetChildComponent<StateText>("Container_budao/Label_txtShanghai");
            _lastRewardItem = GetChildComponent<ItemGrid>("Container_budao/Container_wupin");
            _lastRewardCount = GetChildComponent<StateText>("Container_budao/Label_txtShuliang");
            //KContainer _teamMVPContainer = GetChildComponent<KContainer>("Container_zuduimvp");
            //_teamMvpIcon = _teamMVPContainer.AddChildComponent<IconContainer>("Container_touxiang/Container_icon");
            //_teamLeaderLevel = _teamMVPContainer.GetChildComponent<StateText>("Container_touxiang/Container_dengji/Label_txtDengji");
            //_teamLeaderName = _teamMVPContainer.GetChildComponent<StateText>("Label_txtWanjiamingzizi");
            //_teamLeaderDesc = _teamMVPContainer.GetChildComponent<StateText>("Label_txtShanghai");
            //_teamMVPRewardItem = _teamMVPContainer.GetChildComponent<ItemGrid>("Container_wupin");
            //_teamMVPRewardCount = _teamMVPContainer.GetChildComponent<StateText>("Label_txtShuliang");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_wupinliebiao");
            _myRewardList = GetChildComponent<KPageableList>("ScrollView_wupinliebiao/mask/content");
            _titleContent = _titleTxt.CurrentText.text;
            _maskWidth = (GetChild("ScrollView_wupinliebiao/mask").transform as RectTransform).sizeDelta.x;
            _itemWidth = (GetChild("ScrollView_wupinliebiao/mask/content/item").transform as RectTransform).sizeDelta.x;
            InitScrollView();
        }

        private void InitScrollView()
        {
            _myRewardList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue);
            _scrollView.ScrollRect.horizontal = true;
            _scrollView.ScrollRect.vertical = false;
            _myRewardList.SetGap(0, GAP);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.WorldBossResult; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            if (data is PbWorldBossInfo)
            {
                RefreshContent(data as PbWorldBossInfo);
            }
            else
            {
                ClosePanel();
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void OnEnsureBtnClick()
        {
            ClosePanel();
        }

        private void RefreshContent(PbWorldBossInfo data)
        {
            _mvpDamage.CurrentText.text = MogoLanguageUtil.GetContent(72813);
            _lastDamage.CurrentText.text = MogoLanguageUtil.GetContent(72813);
            if (data.mvp_level > 0)
            {
                _mvpDamage.CurrentText.text = MogoLanguageUtil.GetContent(72811, data.mvp_damage);
            }
            if (data.level > 0)
            {
                _lastDamage.CurrentText.text = MogoLanguageUtil.GetContent(72812);
            }
            _mvpIcon.SetIcon(MogoPlayerUtils.GetSmallIcon((int)data.mvp_vocation));
            _mvpLevel.CurrentText.text = data.mvp_level.ToString();
            _mvpName.CurrentText.text = MogoProtoUtils.ParseByteArrToString(data.mvp_name_bytes);
            _titleTxt.CurrentText.text = string.Format(_titleContent, monster_helper.GetMonsterName((int)data.monster_id));
            _lastIcon.SetIcon(MogoPlayerUtils.GetSmallIcon((int)data.vocation));
            _lastLevel.CurrentText.text = data.level.ToString();
            _lastName.CurrentText.text = MogoProtoUtils.ParseByteArrToString(data.name_bytes);
            //_teamMvpIcon.SetIcon(MogoPlayerUtils.GetSmallIcon((int)data.team_mvp_vocation));
            //_teamLeaderLevel.CurrentText.text = data.team_mvp_level.ToString();
            //_teamLeaderName.CurrentText.text = MogoProtoUtils.ParseByteArrToString(data.team_mvp_name_bytes);
            //if (data.team_mvp_damage > 0)
            //{
            //    _teamLeaderDesc.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(72823), data.team_mvp_damage);
            //}
            //else
            //{
            //    _teamLeaderDesc.CurrentText.text = string.Empty;
            //}

            RefreshReward(data);
            RefreshMyReward(data);
        }

        private void RefreshMyReward(PbWorldBossInfo data)
        {
            List<RewardData> dataList = new List<RewardData>();
            for (int i = 0; i < data.my_item_list.Count; i++)
            {
                for (int j = 0; j < dataList.Count; j++)
                {
                    if (dataList[j].id == (int)data.my_item_list[i].item_id)
                    dataList.Add(RewardData.GetRewardData((int)data.my_item_list[i].item_id, (int)data.my_item_list[i].item_count));
                }
            }
            RecalculatePosition(dataList.Count);
            _myRewardList.SetDataList<RewardGrid>(dataList);
        }

        private void RefreshReward(PbWorldBossInfo data)
        {
            _mvpRewardItem.Clear();
            _mvpRewardCount.CurrentText.text = string.Empty;
            if (data.mvp_item_list != null && data.mvp_item_list.Count > 0)
            {
                _mvpRewardItem.SetItemData((int)data.mvp_item_list[0].item_id);
                if (data.mvp_item_list[0].item_count > 1)
                {
                    _mvpRewardCount.CurrentText.text = data.mvp_item_list[0].item_count.ToString();
                }
            }
            _lastRewardItem.Clear();
            _lastRewardCount.CurrentText.text = string.Empty;
            if (data.item_list != null && data.item_list.Count > 0)
            {
                _lastRewardItem.SetItemData((int)data.item_list[0].item_id);
                if (data.item_list[0].item_count > 1)
                {
                    _lastRewardCount.CurrentText.text = data.item_list[0].item_count.ToString();
                }
            }
            //_teamMVPRewardItem.Clear();
            //if (data.team_mvp_item_list != null && data.team_mvp_item_list.Count > 0)
            //{
            //    _teamMVPRewardItem.Visible = true;
            //    _teamMVPRewardItem.SetItemData((int)data.team_mvp_item_list[0].item_id);
            //    if (data.team_mvp_item_list[0].item_count > 1)
            //    {
            //        _teamMVPRewardCount.CurrentText.text = data.team_mvp_item_list[0].item_count.ToString();
            //    }
            //    else
            //    {
            //        _teamMVPRewardCount.CurrentText.text = string.Empty;
            //    }
            //}
            //else
            //{
            //    _teamMVPRewardItem.Visible = false;
            //    _teamMVPRewardCount.CurrentText.text = string.Empty;
            //}
        }

        private void RecalculatePosition(int count)
        {
            if (count > 4) count = 4;
            float contentWidth = _itemWidth * count + GAP * (count - 1);
            int padding = Mathf.RoundToInt(_maskWidth / 2 - contentWidth / 2);
            _myRewardList.SetPadding(0, padding, 0, padding);
        }

        private void AddEventListener()
        {
            _ensureBtn.onClick.AddListener(OnEnsureBtnClick);
        }

        private void RemoveEventListener()
        {
            _ensureBtn.onClick.RemoveListener(OnEnsureBtnClick);
        }
    }
}
