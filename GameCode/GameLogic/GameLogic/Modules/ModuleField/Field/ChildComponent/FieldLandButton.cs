﻿using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleField
{
    public class FieldLandButton:KContainer
    {
        private instance _instance;

        private KContainer _bgMapImg;
        private StateImage _lock;
        private StateText _name;
        private KButton _button;

        protected override void Awake()
        {
            base.Awake();
            _bgMapImg = GetChildComponent<KContainer>("imageMap");
            _lock = GetChildComponent<StateImage>("imageLock");
            _name = GetChildComponent<StateText>("labelName");
            _button = GetComponent<KButton>();
            AddListener();
            gameObject.AddComponent<TweenButtonEnlarge>();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            RemoveListener();
        }

        public instance Data
        {
            get
            {
                return _instance; 
            }
            set
            {
                _instance = value;
                Refresh();
            }
        }

        private void AddListener()
        {
            _button.onClick.AddListener(OnClick);
        }

        private void RemoveListener()
        {
            _button.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            if (PlayerAutoFightManager.GetInstance().fightType == FightType.FOLLOW_FIGHT)
            {
                MessageBox.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.ENSURE_CANCEL_FOLLOW_FIGHT), () =>
                {
                    PlayerAutoFightManager.GetInstance().Stop();
                    TeamManager.Instance.CancelFollowCaptain();
                    MissionManager.Instance.RequsetEnterMission(_instance.__id);
                });
                return;
            }
            MissionManager.Instance.RequsetEnterMission(_instance.__id);
        }

        private void Refresh()
        {
            ImageWrapper[] wrappers = _bgMapImg.GetComponentsInChildren<ImageWrapper>();
            _name.ChangeAllStateText(MogoLanguageUtil.GetContent(42002,_instance.__level_limit[0],MogoLanguageUtil.GetContent(_instance.__inst_name)));

            ///野外世界地图按钮解锁规则
            ///1、如果玩家等级小于全局配置表中配置的野外世界地图开放等级243，根据玩家等级是否大于该副本等级限制来判断是否解锁
            ///2、如果玩家等级大于等于该配置，首先取玩家世界最高等级以及自己等级（防止服务器未同步玩家世界最高等级）的最大值，根据该值是否大于该副本等级限制来判断是否解锁
            int wildOpenLevel = GlobalParams.GetWildOpenLevel();
            if ((PlayerAvatar.Player.level >= wildOpenLevel && Math.Max(PlayerAvatar.Player.avatarMostLevel,PlayerAvatar.Player.level) >= int.Parse(_instance.__level_limit[0]))
                || (PlayerAvatar.Player.level < wildOpenLevel && PlayerAvatar.Player.level >= int.Parse(_instance.__level_limit[0])))
            {
                RefreshIsOpen(true);
            }
            else
            {
                RefreshIsOpen(false);
            }
        }

        private void RefreshIsOpen(bool isShow)
        {
            ImageWrapper[] wrappers = _bgMapImg.GetComponentsInChildren<ImageWrapper>();

            for (int i = 0; i < wrappers.Length; i++)
            {
                if (wrappers[i] != null)
                {
                    wrappers[i].SetGray(isShow ? 1 : 0);
                }
            }
            _lock.Visible = !isShow;
        }
    }
}
