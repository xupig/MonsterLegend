﻿using Common.ExtendComponent;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleField
{
    public class FieldLandButtonView : KContainer
    {
        private FieldLandButton[] _fieldLandButtonArray = new FieldLandButton[6];
        private FieldLandButton _mainTownButton;
        private KContainer[] _bossArray = new KContainer[6];
        private KContainer[] _dialogueArray = new KContainer[6];
        private TweenTreasureBox[] _triangleArray = new TweenTreasureBox[6];

        private TweenTreasureBox _currentTriangle;

        protected override void Awake()
        {
            base.Awake();
            for (int i = 0; i < 6; i++)
            {
                _fieldLandButtonArray[i] = AddChildComponent<FieldLandButton>(string.Format("Container_button/Button_world{0}0", i + 2));
                _bossArray[i] = GetChildComponent<KContainer>(string.Format("Container_boss/Container_boss{0}0", i + 2));
                _triangleArray[i] = _bossArray[i].AddChildComponent<TweenTreasureBox>("Image_gwsjzy");
                _triangleArray[i].Visible = false;
                _bossArray[i].gameObject.AddComponent<RaycastComponent>();
                _dialogueArray[i] = GetChildComponent<KContainer>(string.Format("Container_dialogue/Container_dialogue{0}0", i + 2));
            }
            _mainTownButton = AddChildComponent<FieldLandButton>("Container_button/Button_mainTown");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            FillFieldLandInfo();
            ShowCurrentMapTriangle();
        }

        private void FillFieldLandInfo()
        {
            List<int> fieldCopyList = data_parse_helper.ParseListInt(chapters_helper.GetChapterList(ChapterType.Field)[0].__instance_ids);
            int count = fieldCopyList.Count;
            for (int i = 0; i < count; i++)
            {
                instance instance = instance_helper.GetInstanceCfg(fieldCopyList[i]);
                _fieldLandButtonArray[i].Data = instance;

                if ((int)(PlayerAvatar.Player.level) >= int.Parse(instance.__level_limit[0]))
                {
                    _bossArray[i].Visible = true;
                }
                else
                {
                    _bossArray[i].Visible = false;
                }
                _dialogueArray[i].Visible = false;
            }

            _mainTownButton.Data = instance_helper.GetInstanceCfg(instance_helper.MainTownInstanceId);
        }

        private void ShowCurrentMapTriangle()
        {
            int instanceId = map_helper.GetInstanceIDByMapID(GameSceneManager.GetInstance().curMapID);
            List<int> fieldCopyList = data_parse_helper.ParseListInt(chapters_helper.GetChapterList(ChapterType.Field)[0].__instance_ids);
            if (_currentTriangle != null)
            {
                _currentTriangle.StopTween();
                _currentTriangle.Visible = false;
                _currentTriangle = null;
            }

            if (fieldCopyList.Contains(instanceId) == true)
            {
                _currentTriangle = _triangleArray[fieldCopyList.IndexOf(instanceId)];
                _currentTriangle.Visible = true;
                _currentTriangle.StartTween();
            }

        }

    }
}
