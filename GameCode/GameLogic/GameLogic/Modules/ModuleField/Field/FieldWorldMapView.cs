﻿using Common.Base;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleField
{
    public class FieldWorldMapView : KContainer
    {
        private FieldLandButtonView _fieldLandButton;
        private KContainer[] _bgContainer;

        protected override void Awake()
        {
            base.Awake();
            _bgContainer = new KContainer[4];
            _fieldLandButton = AddChildComponent<FieldLandButtonView>("Container_worldModule");

            for (int i = 0; i < 4; i++)
            {
                _bgContainer[i] = GetChildComponent<KContainer>(string.Format("Container_bg/Container_bg{0}", i+1));
            }
            LoadMapBg();
        }

        private void LoadMapBg()
        {
            for(int i =0; i < 4; i++)
            {
                MogoAtlasUtils.AddSprite(_bgContainer[i].gameObject, string.Format("yewaiditubg{0}", i));
            }
        }

    }
}
