﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleField
{
    public class FieldModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.FieldWorldMap, "Container_FieldWorldMapPanel", MogoUILayer.LayerUIPanel, "ModuleField.FieldWorldMapPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.FieldBossIntroduce, "Container_FieldBossPanel", MogoUILayer.LayerUIToolTip, "ModuleField.FieldBossIntroducePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);
            AddPanel(PanelIdEnum.WorldBossResult, "Container_WorldBOSSResultPanel", MogoUILayer.LayerUIPanel, "ModuleField.WorldBossResultPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.WorldBossRanking, "Container_WorldBossRanking", MogoUILayer.LayerUnderPanel, "ModuleField.WorldBossRankingPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);
        }
    }
}
