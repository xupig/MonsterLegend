﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/20 10:29:17
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Data;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ModuleRole
{
    public class RoleAttributeItem : KList.KListItemBase
    {
        private KContainer _basicInfo;
        private KContainer _detailInfo;
        private StateImage _upperImage;

        private StateText nameTxt;
        private StateText valueTxt;
        private RoleAttributeData _data;
        private bool isShowDetailInfo = false;

        private StateText _descTxt;

        protected override void Awake()
        {
            _basicInfo = GetChildComponent<KContainer>("Container_shousuo");
            _upperImage = _basicInfo.GetChildComponent<StateImage>("Image_zhankaiUp");
            _detailInfo = GetChildComponent<KContainer>("Container_zhankai");
            _detailInfo.Visible = false;
            RecalculateSize();
            nameTxt = _basicInfo.GetChildComponent<StateText>("Label_attributeName");
            valueTxt = _basicInfo.GetChildComponent<StateText>("Label_txtFightValue");
            nameTxt.CurrentText.text = string.Empty;
            valueTxt.CurrentText.text = string.Empty;
        }

        public override void Dispose()
        {
            _data = null;
        }

        //ItemInfo类型 
        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as RoleAttributeData;
                if (_data == null)
                {
                    nameTxt.CurrentText.text = string.Empty;
                    valueTxt.CurrentText.text = string.Empty;
                    //_descTxt.CurrentText.text = string.Empty;
                }
                else
                {
                    nameTxt.CurrentText.text = _data.attriName;
                    valueTxt.CurrentText.text = _data.strAttriValue;
                }
            }
        }

        public override void OnPointerClick(PointerEventData evtData)
        {
            isShowDetailInfo = !isShowDetailInfo;
            if (isShowDetailInfo)
            {
                _detailInfo.Visible = true;
                _descTxt = _detailInfo.GetChildComponent<StateText>("Label_txtShuxingjieshao");
                _descTxt.CurrentText.text = attri_config_helper.GetRecommendTxt(_data.attriId);
                _upperImage.Visible = false;
            }
            else
            {
                _detailInfo.Visible = false;
                _upperImage.Visible = true;
            }
            RecalculateSize();
            onClick.Invoke(this, this.Index);
        }
    }
}
