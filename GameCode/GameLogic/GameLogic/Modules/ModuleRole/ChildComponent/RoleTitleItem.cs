﻿#region 模块信息
/*==========================================
// 文件名：RoleTitleItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleRole.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/6 15:15:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent.ListToggleGroup;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleRole
{
    public class RoleTitleItem:ListToggleItem
    {
        private StateText _txtTitleName;
        private StateImage _imgEquiped;
        private StateImage _imgLocked;
        protected override void Awake()
        {
            m_toggle = GetChildComponent<KToggle>("Toggle_item");
            _txtTitleName = GetChildComponent<StateText>("Label_txtDafuhao");
            _imgEquiped = GetChildComponent<StateImage>("Image_yizhuangbei");
            _imgLocked = GetChildComponent<StateImage>("Image_suo");
        }

        public override void Show()
        {
            base.Show();
        }

        public override void Hide()
        {
            base.Hide();
        }

        public override void Dispose()
        {
            
        }

        public override object Data
        {
            get
            {
                return base.Data;
            }
            set
            {
                base.Data = value;
            }
        }
    }
}
