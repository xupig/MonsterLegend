﻿#region 模块信息
/*==========================================
// 文件名：RoleEquipItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleRole.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/6 11:27:30
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Structs;
using Common.Structs.Enum;
using Game.UI.UIComponent;
using ModuleCommonUI;
using Common.Global;

namespace ModuleRole
{
    public class RoleEquipItem : KContainer
    {
        private StateImage _imgSelected;
        private KButton _backgroundButton;
        private ItemGrid _itemGrid;
        public int EquipPostion { get; set; }

        public KComponentEvent<int> onClick = new KComponentEvent<int>();
        private EquipItemInfo _equipInfo;
        private WingItemData _wingData;
        private KParticle _particle;

        protected override void Awake()
        {
            _particle = AddChildComponent<KParticle>("fx_ui_5_2_shengji");
            if(_particle!=null)
            {
                _particle.Stop();
            }
            _imgSelected = GetChildComponent<StateImage>("Image_selected");
            _backgroundButton = GetChildComponent<KButton>("Button_background");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _itemGrid.gameObject.AddComponent<RaycastComponent>();
            _imgSelected.gameObject.AddComponent<RaycastComponent>();
            _imgSelected.gameObject.SetActive(false);
            AddEventListener();
        }

        public void ShowParticle()
        {
            if(_particle!=null)
            {
                _particle.Play(true);
            }
        }

        public void HideParticle()
        {
            if(_particle!=null)
            {
                _particle.Stop();
            }
        }

        public void SetEquip(EquipItemInfo equipInfo)
        {
            if(EquipPostion == (int)EquipType.wing || EquipPostion == (int)EquipType.love)
            {
                return;
            }

            _equipInfo = equipInfo;
            if(_equipInfo != null)
            {
                _itemGrid.SetItemData(_equipInfo);
                _itemGrid.equipState = EquipState.Wearing;
            }
            else
            {
                _itemGrid.Clear();
            }
        }

        public void SetWing(WingItemData wingData)
        {
            if(EquipPostion == (int)EquipType.wing)
            {
                _wingData = wingData;
                if(_wingData != null)
                {
                    _itemGrid.SetItemData(_wingData.icon, (int)ItemQualityDefine.WHITE);
                }
                else
                {
                    _itemGrid.Clear();
                }
            }
        }

        public void SetSelectd(bool isSelect)
        {
            _imgSelected.gameObject.SetActive(isSelect);
            if(isSelect)
            {
                ToolTipsManager.Instance.ShowItemTip(_equipInfo, PanelIdEnum.Information, EquipState.Wearing);
            }
        }

        private void AddEventListener()
        {
            _backgroundButton.onClick.AddListener(OnButtonClick);
        }

        protected override void OnDestroy()
        {
            _backgroundButton.onClick.RemoveListener(OnButtonClick);
        }

        private void OnButtonClick()
        {
            if(_equipInfo != null || EquipPostion == (int)EquipType.wing || EquipPostion == (int)EquipType.love)
            {
                onClick.Invoke(EquipPostion);
            }
        }

    }
}
