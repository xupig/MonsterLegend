﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/9 11:35:18
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.ExtendComponent.ListToggleGroup;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using ModuleBag;
using MogoEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleRole
{
    public class EquipAppearanceItem : ListToggleItem
    {
        private KButton m_btnPreView;
        private KButton m_btnActive;
        private KButton m_btnPutOn;
        private KButton m_btnPutOff;
        private StateText m_textProgress;
        private KProgressBar m_progress;
        private StateText m_textDesc;
        private StateText m_textDescDetial;
        private StateImage equipedImg;
        private StateImage activedImg;
        private KContainer _equipContainer;

        protected override void Awake()
        {
            m_toggle = GetChildComponent<KToggle>("Toggle_item");
            KContainer _container = GetChildComponent<KContainer>("Container_xiangqian");
            m_btnActive = _container.GetChildComponent<KButton>("Button_jihuo");
            m_btnPreView = _container.GetChildComponent<KButton>("Button_yulan");
            m_btnPutOff = _container.GetChildComponent<KButton>("Button_xiexia");
            m_btnPutOn = _container.GetChildComponent<KButton>("Button_chuanshang");

            m_textProgress = _container.GetChildComponent<StateText>("Label_txtjindu");
            m_textDesc = _container.GetChildComponent<StateText>("Label_txtDesc");
            m_textDescDetial = _container.GetChildComponent<StateText>("Label_txtXiangqianxinxi");
            m_progress = _container.GetChildComponent<KProgressBar>("ProgressBar_jindu");

            equipedImg = _container.GetChildComponent<StateImage>("Image_yizhuangbei");
            activedImg = _container.GetChildComponent<StateImage>("Image_yijihuoIcon");
            _equipContainer = _container.GetChildComponent<KContainer>("Container_equipIcon");


           // itemGrid = this.gameObject.AddComponent<ItemGrid>();
            //itemGrid.transform.SetParent(this.gameObject.transform);
           // itemGrid.transform.position = equipImg.transform.position;

            m_btnActive.gameObject.SetActive(false);
            m_btnPreView.gameObject.SetActive(false);
            m_btnPutOff.gameObject.SetActive(false);
            m_textDescDetial.gameObject.SetActive(false);
            equipedImg.gameObject.SetActive(false);
            activedImg.gameObject.SetActive(false);
           // itemGrid.gameObject.SetActive(false);

        }


        public override void Dispose()
        {
            RemoveEvents();
            base.Data = null;
        }

        private void AddEvents()
        {
            m_btnActive.onClick.AddListener(OnClickActiveBtn);
            m_btnPreView.onClick.AddListener(OnClickPreviewBtn);
            m_btnPutOff.onClick.AddListener(OnClickPutOffBtn);
            m_btnPutOn.onClick.AddListener(OnClickPutOnBtn);
        }

        private void RemoveEvents()
        {
            m_btnActive.onClick.RemoveListener(OnClickActiveBtn);
            m_btnPreView.onClick.RemoveListener(OnClickPreviewBtn);
            m_btnPutOff.onClick.RemoveListener(OnClickPutOffBtn);
            m_btnPutOn.onClick.RemoveListener(OnClickPutOnBtn);
        }

        public override void Show()
        {
            AddEvents();
        }

        public override void Hide()
        {
            RemoveEvents();
        }

        public override object Data
        {
            get
            {
                return base.Data;
            }
            set
            {
                base.Data = value;
                UpdateContent();
            }
        }

        private void UpdateContent()
        {
        }

        public void SetInfo(item_commom itemInfo)
        {
            //PlayerAvatar player = MogoWorld.Player as PlayerAvatar;

            //MogoAtlasUtils.AddSprite(_equipContainer.gameObject, item_helper.GetSpriteName(itemInfo.__id), item_helper.GetIconColor(itemInfo.__id));
        }

        private void OnClickActiveBtn()
        {

        }

        private void OnClickPreviewBtn()
        {

        }

        private void OnClickPutOnBtn()
        {

        }

        private void OnClickPutOffBtn()
        {

        }

    }
}
