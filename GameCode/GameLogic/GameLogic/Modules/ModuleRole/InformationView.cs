﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/8 11:00:08
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Events;
using Game.UI.UIComponent;
using MogoEngine.Events;
using UnityEngine;
using MogoEngine;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using Common.ExtendComponent;
using ModuleMainUI;
namespace ModuleRole
{
    public class InformationView:KContainer
    {
        private RoleEquipView m_roleEquipView;//装备面板
        private RoleInfoView m_roleInfoView;//角色信息面板
        private RoleAttrDetialView m_roleAttrDetialView;//角色属性详细信息面板
        private MainUIExpView _expView;

        public void Show()
        {
            Visible = true;
            AddEventListener();
            ShowEquipView();
        }

        public void Close()
        {
            Visible = false;
            RemoveEventListener();
        }


        /**提取UI子级组件[UI资源加载完成时调用]**/
        protected override void Awake()
        {
            
            m_roleEquipView = AddChildComponent<RoleEquipView>("Container_equipView");
            m_roleInfoView = AddChildComponent<RoleInfoView>("Container_roleInfoView");
            m_roleAttrDetialView = AddChildComponent<RoleAttrDetialView>("Container_xiangxixinxi");
            _expView = AddChildComponent<MainUIExpView>("Container_EXP");
           // m_roleTitleView = AddChildComponent<RoleTitleView>("Container_chenghao");
            //m_equipAppearceView = AddChildComponent<EquipAppearceView>("Container_zhuangbeiwaiguanzhanshi");

           // m_toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            //m_roleTitleView.gameObject.SetActive(false);
            //m_equipAppearceView.gameObject.SetActive(false);
            m_roleInfoView.ShowDetialInfoBtn();
        }

        /**统一注册事件监听**/
        private void AddEventListener()
        {
            //m_toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            m_roleInfoView.onReturnClick.AddListener(OnReturnEquipViewHandler);
            m_roleInfoView.onShowRoleDetailClick.AddListener(OnShowRoleDetialViewHandler);
        }

        /**统一移除事件监听**/
        private void RemoveEventListener()
        {
            //m_toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            if(m_roleInfoView!=null)
            {
                m_roleInfoView.onReturnClick.RemoveListener(OnReturnEquipViewHandler);
                m_roleInfoView.onShowRoleDetailClick.RemoveListener(OnShowRoleDetialViewHandler);
            }
        }

        /// <summary>
        /// 切换分类
        /// </summary>
        /// <param name="index"></param>
        private void OnSelectedIndexChanged(KToggleGroup toggleGroup, int index)
        {
            m_roleEquipView.gameObject.SetActive(false);//装备面板
            m_roleInfoView.gameObject.SetActive(false);//角色信息面板
            m_roleAttrDetialView.gameObject.SetActive(false);//角色属性详细信息面板
           // m_roleTitleView.gameObject.SetActive(false);//称号面板
            //m_equipAppearceView.gameObject.SetActive(false);
            switch(index)
            {
                case 0:
                    m_roleEquipView.gameObject.SetActive(true);
                    m_roleInfoView.gameObject.SetActive(true);//角色信息面板
                    m_roleInfoView.ShowDetialInfoBtn();
                   break;
                case 1:
                   //m_equipAppearceView.gameObject.SetActive(true);
                   break;
                case 2:
                    //m_roleTitleView.gameObject.SetActive(true);//称号面板
                   break;
                default:
                   break;
            }
        }

        private void ShowEquipView()
        {
            m_roleEquipView.gameObject.SetActive(true);
            m_roleAttrDetialView.gameObject.SetActive(false);
            m_roleInfoView.ShowDetialInfoBtn();
        }

        /// <summary>
        /// 返回装备面板
        /// </summary>
        private void OnReturnEquipViewHandler()
        {
            m_roleEquipView.gameObject.SetActive(true);
            TweenViewMove.Begin(m_roleAttrDetialView.gameObject, MoveType.Hide, MoveDirection.Left);
            m_roleInfoView.ShowDetialInfoBtn();
        }

        private void OnShowRoleDetialViewHandler()
        {
            m_roleEquipView.gameObject.SetActive(false);
            TweenViewMove.Begin(m_roleAttrDetialView.gameObject, MoveType.Show, MoveDirection.Left);
            m_roleAttrDetialView.UpdateContent();
            m_roleInfoView.HideDetialInfoBtn();
        }

    }
}
