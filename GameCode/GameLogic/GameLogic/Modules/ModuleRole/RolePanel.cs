﻿#region 模块信息
/*==========================================
// 文件名：InformationPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleRole
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/6/15 10:30:31
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using ModuleMainUI;
using MogoEngine.Events;
using UnityEngine;

namespace ModuleRole
{
    public class RolePanel:BasePanel
    {
        private const int ROLE = 0;
        private const int SKILL = 1;
        private const int WING = 2;
        private const int EQUIP_FACADE = 3;

        private InformationView _roleView;

        private KToggleGroup _toggleGroup;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Information; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            _roleView = AddChildComponent<InformationView>("Container_role");
            SetTabList(32, _toggleGroup);
        }

        private void AttachToTopCenter(GameObject go)
        {
            RectTransform rectTransform = go.GetComponent<RectTransform>();
            Vector3 position = rectTransform.localPosition;
            rectTransform.localPosition = UIManager.GetUIStartPosition();
        }

        private void RefreshEquipEffectPoint()
        {
            _toggleGroup[EQUIP_FACADE].SetGreenPoint(PlayerDataManager.Instance.EquipEffectData.HasCanActivateEffect());
        }

        private void RefreshWingPoint()
        {
            WingData wingData = PlayerDataManager.Instance.WingData;
            _toggleGroup[WING].SetGreenPoint(false);
            if (wingData.newWingList.Count > 0)
            {
                _toggleGroup[WING].SetGreenPoint(true);
            }
            if (wingData.canTrainList.Count > 0)
            {
                _toggleGroup[WING].SetGreenPoint(true);
            }
            if (wingData.canComposeList.Count > 0)
            {
                _toggleGroup[WING].SetGreenPoint(true);
            }
        }

        private void AddEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            EventDispatcher.AddEventListener(WingEvents.REFRESH_NOTICE_POINT, RefreshWingPoint);
            EventDispatcher.AddEventListener(SpellEvents.SPELL_OPERATION_CHANGE, OnSpellOperationChnage);
            EventDispatcher.AddEventListener(FashionEvents.ACTIVATE_FASHION_CHANGE, RefreshEquipEffectPoint);
        }

        private void RemoveEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            EventDispatcher.RemoveEventListener(WingEvents.REFRESH_NOTICE_POINT, RefreshWingPoint);
            EventDispatcher.RemoveEventListener(SpellEvents.SPELL_OPERATION_CHANGE, OnSpellOperationChnage);
            EventDispatcher.RemoveEventListener(FashionEvents.ACTIVATE_FASHION_CHANGE, RefreshEquipEffectPoint);
        }

        public override void OnShow(object data)
        {
            SetData(data);
            RefreshWingPoint();
            RefreshEquipEffectPoint();
            AddEventListener();
            OnSpellOperationChnage();
        }

        public override void OnClose()
        {
            RemoveEventListener();
            UIManager.Instance.ClosePanel(PanelIdEnum.Skill);
            UIManager.Instance.ClosePanel(PanelIdEnum.Wing);
            UIManager.Instance.ClosePanel(PanelIdEnum.EquipFacade);
        }

        private void OnSpellOperationChnage()
        {
            _toggleGroup.GetToggleList()[SKILL].SetGreenPoint(PlayerDataManager.Instance.SpellData.isSkillProficientUnLock == 1 || PlayerDataManager.Instance.SpellData.hasSkillCanUpgradeDict.Count > 0);
            
        }

        private void OnSelectedIndexChanged(KToggleGroup toggleGroup,int index)
        {
            ChangeSelectedIndex(index, null);
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            if (data != null)
            {
                if (data is int)
                {
                    _toggleGroup.SelectIndex = (int)data - 1;
                    ChangeSelectedIndex((int)data - 1, null);
                }
                else if (data is int[])
                {
                    int[] list = data as int[];
                    if (list.Length == 1)
                    {
                        _toggleGroup.SelectIndex = list[0] - 1;
                        ChangeSelectedIndex(list[0] - 1, null);
                    }
                    else if (list.Length == 2)
                    {
                        _toggleGroup.SelectIndex = list[0] - 1;
                        ChangeSelectedIndex(list[0] - 1, list[1] - 1);
                    }
                    else
                    {
                        _toggleGroup.SelectIndex = list[0] - 1;
                        ChangeSelectedIndex(list[0] - 1, list);
                    }
                }
            }
            else
            {
                _toggleGroup.SelectIndex = ROLE;
                ChangeSelectedIndex(_toggleGroup.SelectIndex, null);
            }
        }


        private void ChangeSelectedIndex(int index, object data)
        {
            switch (index)
            {
                case ROLE:
                    _roleView.Show();
                    PanelIdEnum.Skill.Close();
                    PanelIdEnum.Wing.Close();
                    PanelIdEnum.EquipFacade.Close();
                    break;
                case SKILL:
                    _roleView.Close();
                    PanelIdEnum.Wing.Close();
                    PanelIdEnum.Skill.Show(data);
                    PanelIdEnum.EquipFacade.Close();
                    break;
                case WING:
                    _roleView.Close();
                    PanelIdEnum.Skill.Close();
                    PanelIdEnum.Wing.Show();
                    PanelIdEnum.EquipFacade.Close();
                    break;
                case EQUIP_FACADE:
                    _roleView.Close();
                    PanelIdEnum.Skill.Close();
                    PanelIdEnum.Wing.Close();
                    PanelIdEnum.EquipFacade.Show(data);
                    break;
                default:
                    break;
            }
        }

    }
}
