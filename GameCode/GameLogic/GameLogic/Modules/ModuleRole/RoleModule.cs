﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/8 10:51:17
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Data;
using Common.ServerConfig;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine;

namespace ModuleRole
{
    public class RoleModule:BaseModule
    {
        public RoleModule() 
        {
            
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.ProficientSkill, "Container_ProficientSkillPanel", MogoUILayer.LayerUIPanel, "ModuleNewSkill.ProficientSkillPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.Information, "Container_InformationPanel", MogoUILayer.LayerSpecialUIPanel, "ModuleRole.RolePanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.Skill, "Container_RoleSkillPanel", MogoUILayer.LayerUIPanel, "ModuleNewSkill.NewSkillPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.Wing, "Container_RoleWingPanel", MogoUILayer.LayerUIPanel, "ModuleWing.WingPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.EquipFacade, "Container_RoleEquipFacadePanel", MogoUILayer.LayerUIPanel, "ModuleEquipFacade.EquipFacadePanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.EquipFacadeTip, "Container_RoleEquipFacadeTip", MogoUILayer.LayerUIToolTip, "ModuleEquipFacade.EquipFacadeTip", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            RegisterPanelModule(PanelIdEnum.Information, PanelIdEnum.Information);
            RegisterPanelModule(PanelIdEnum.Skill,PanelIdEnum.Information);
            RegisterPanelModule(PanelIdEnum.Wing,PanelIdEnum.Information);
            RegisterPanelModule(PanelIdEnum.EquipFacade, PanelIdEnum.Information);
            RegisterPanelModule(PanelIdEnum.EquipFacadeTip, PanelIdEnum.Information);
        }

        protected override void OnBeforeShowPanel(PanelIdEnum panelId)
        {
            //RoleManager.Instance.ReqGetFashionInfo();
        }
    }
}
