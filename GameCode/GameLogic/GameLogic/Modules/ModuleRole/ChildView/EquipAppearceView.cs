﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/9 9:59:51
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.ExtendComponent;
using Common.ExtendComponent.ListToggleGroup;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleRole
{
    public class EquipAppearceView:KContainer
    {
        private int sortType = 0;

        private CategoryList toggleGroup;

        private KButton refreshBtn;
        private ModelComponent _avatarModel;

        protected override void Awake()
        {
            toggleGroup = GetChildComponent<CategoryList>("List_categoryList");
            refreshBtn = GetChildComponent<KButton>("Button_shuaxin");
            _avatarModel = AddChildComponent<ModelComponent>("Container_roleModel");
            AddEventListener();
        }

        protected void AddEventListener()
        {
            toggleGroup.onSelectedIndexChanged.AddListener(OnToggleGroupSelectChange);
            refreshBtn.onClick.AddListener(onRefreshBtnClick);
        }

        protected override void OnDestroy()
        {
            toggleGroup.onSelectedIndexChanged.RemoveListener(OnToggleGroupSelectChange);
            refreshBtn.onClick.RemoveListener(onRefreshBtnClick);
        }

        private void OnToggleGroupSelectChange(CategoryList toggle, int index)
        {
            sortType = index;
        }

        private void onRefreshBtnClick()
        {

        }

    }
}
