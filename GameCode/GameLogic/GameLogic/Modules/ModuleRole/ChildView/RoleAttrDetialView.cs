﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/8 14:19:52
 * 描述说明：装备属性详细信息
 * *******************************************************/

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using ModuleCommonUI;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;
namespace ModuleRole
{
    public class RoleAttrDetialView : KContainer
    {
        private int m_index;

        private KScrollView m_attributeView;
        private KList m_contentList;
        private KToggleGroup m_toggleGroup;
        private RectTransform _rect;
        private KContainer _attributeDescContainer;
        private StateText _descTxt;
        private Locater _descLocater;

        private List<int> levelList;//三个标签页的开放等级限制
        static readonly int[] s_Padding = new int[] { 5, 10, 5, 10 };
        static readonly int[] s_Gap = new int[] { 13, 0 };

        protected override void Awake()
        {
            m_attributeView = GetChildComponent<KScrollView>("ScrollView_xinxi");
            m_attributeView.ScrollRect.vertical = true;
            m_attributeView.ScrollRect.movementType = UnityEngine.UI.ScrollRect.MovementType.Elastic;
            m_contentList = m_attributeView.GetChildComponent<KList>("mask/content");
            m_toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_category");

            string levelString = XMLManager.global_params[65].__value;
            levelList = MogoStringUtils.Convert2List(levelString);

            InitScrollPage();
            //InitAttributeDescContainer();
            AddEventListener();
            _rect = GetComponent<RectTransform>();
        }

        private void InitAttributeDescContainer()
        {
            GameObject descGO = m_contentList.transform.Find("xiangxixinxi").gameObject;
            _attributeDescContainer = descGO.AddComponent<KContainer>();
            _attributeDescContainer.Visible = true;
            _descTxt = _attributeDescContainer.GetChildComponent<StateText>("Label_txtShuxingjieshao");
            _descLocater = _attributeDescContainer.gameObject.AddComponent<Locater>();
            _attributeDescContainer.Visible = false;
        }

        private void InitScrollPage()
        {
            m_contentList.SetPadding(s_Padding[0], s_Padding[1], s_Padding[2], s_Padding[3]);
            m_contentList.SetGap(s_Gap[0], s_Gap[1]);
            m_contentList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
        }

        protected void AddEventListener()
        {
            m_toggleGroup.onSelectedIndexChanged.AddListener(OnSelectIndexChange);
            m_contentList.onItemClicked.AddListener(OnAttributeItemClicked);
        }

        private void OnAttributeItemClicked(KList list, KList.KListItemBase baseItem)
        {
            DoContentListLayout();
        }

        private void ShowCurrentAttributeIntroduction(RoleAttributeItem item)
        {
            _attributeDescContainer.Visible = true;
            _descTxt.CurrentText.text = attri_config_helper.GetAttributeDesc((item.Data as RoleAttributeData).attriId);
            Locater itemLocater = GetOrAddLocater(item);
            _descLocater.Position = itemLocater.Position - new Vector2(0, itemLocater.Height);
            Vector2 startVector = _descLocater.Position - new Vector2(0, _descLocater.Height + s_Gap[0]);
            for (int i = item.Index + 1; i < m_contentList.ItemList.Count; i++)
            {
                Locater locater = GetOrAddLocater(m_contentList.ItemList[i]);
                startVector -= new Vector2(0, locater.Height + s_Gap[0]);
                locater.Position = startVector;
            }
        }

        private void CloseCurrentAttributeIntroduction()
        {
            _attributeDescContainer.Visible = false;
            Vector2 startVector = new Vector2(0, 0);
            for (int i = 0; i < m_contentList.ItemList.Count; i++)
            {
                Locater locater = GetOrAddLocater(m_contentList.ItemList[i]);
                startVector -= new Vector2(0, locater.Height + s_Gap[0]);
                locater.Position = startVector;
            }
        }

        private Locater GetOrAddLocater(KList.KListItemBase item)
        {
            if (item.gameObject.GetComponent<Locater>() != null)
            {
                return item.gameObject.GetComponent<Locater>();
            }
            return item.gameObject.AddComponent<Locater>();
        }

        protected override void OnDestroy()
        {

            m_toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectIndexChange);
            m_contentList.onItemClicked.RemoveListener(OnAttributeItemClicked);
            base.OnDestroy();
        }

        public void UpdateContent()
        {
            List<RoleAttributeData> _list = new List<RoleAttributeData>();
            PlayerAvatar player = MogoWorld.Player as PlayerAvatar;
            foreach (KeyValuePair<int, GameData.attri_config> kvp in XMLManager.attri_config)
            {
                float value = player.GetAttribute((Common.ServerConfig.fight_attri_config)kvp.Value.__id);
                string key = ((int)player.vocation).ToString();
                if (m_index + 1 == kvp.Value.__tab && kvp.Value.__show.ContainsKey(key) && kvp.Value.__show[key] == "1" && value != 0)
                {
                    if ((fight_attri_config)kvp.Value.__id == fight_attri_config.DMG_MAX)
                    {
                        continue;
                    }
                    else if ((fight_attri_config)kvp.Value.__id == fight_attri_config.DMG_MIN)
                    {
                        RoleAttributeData info = new RoleAttributeData();
                        info.attriId = kvp.Value.__id;
                        info.attriName = MogoLanguageUtil.GetContent(kvp.Value.__name_id).Substring(0, 2);
                        info.strAttriValue = player.GetAttribute2String(fight_attri_config.DMG_MIN, false) + "-" + player.GetAttribute2String(fight_attri_config.DMG_MAX, false);
                        info.priority = kvp.Value.__priority;
                        _list.Add(info);
                    }
                    else
                    {
                        RoleAttributeData info = new RoleAttributeData();
                        info.attriId = kvp.Value.__id;
                        info.attriName = MogoLanguageUtil.GetContent(kvp.Value.__name_id);
                        info.strAttriValue = kvp.Value.__is_float == 1 ? string.Concat((value * 100).ToString("0.00"), "%") : value.ToString();
                        info.priority = kvp.Value.__priority;
                        if ((fight_attri_config)kvp.Value.__id == fight_attri_config.EP_LIMIT)
                        {
                            info.strAttriValue = (value / 100).ToString(); ;
                        }
                        _list.Add(info);
                    }
                }
            }
            _list.Sort(sortFunc);
            m_contentList.SetDataList<RoleAttributeItem>(_list);
            DoContentListLayout();
        }

        private void DoContentListLayout()
        {
            float height = 0;
            height += s_Padding[0];
            Vector2 startVector = new Vector2(s_Padding[1], -s_Padding[0]);
            for (int i = 0; i < m_contentList.ItemList.Count; i++)
            {
                Locater locater = GetOrAddLocater(m_contentList.ItemList[i]);
                locater.Position = startVector;
                height += (locater.Height + s_Gap[0]);
                startVector += new Vector2(0, -(locater.Height + s_Gap[0]));
            }
            height += s_Padding[3];
            RectTransform rectTran = m_contentList.GetComponent<RectTransform>();
            rectTran.sizeDelta = new Vector2(rectTran.rect.size.x, height);
        }

        private int sortFunc(RoleAttributeData info1, RoleAttributeData info2)
        {
            if (info1.priority == info2.priority)
            {
                return info1.attriId - info2.attriId;
            }
            else
            {
                return info1.priority - info2.priority;
            }
        }

        private void OnSelectIndexChange(KToggleGroup toggle, int index)
        {
            if (PlayerAvatar.Player.level < levelList[index])
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(600, levelList[index]), PanelIdEnum.MainUIField);
                m_toggleGroup.SelectIndex = m_index;
                return;
            }
            m_index = index;
            UpdateContent();
        }

        protected override void OnEnable()
        {
            m_toggleGroup.SelectIndex = 0;
            m_index = 0;
            UpdateContent();
            m_attributeView.ResetContentPosition();
        }

    }
}
