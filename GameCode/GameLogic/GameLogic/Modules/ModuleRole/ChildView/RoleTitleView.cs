﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/8 11:12:38
 * 描述说明：称号子界面
 * *******************************************************/

using Common.Base;
using Common.ExtendComponent;
using Common.ExtendComponent.ListToggleGroup;
using Game.UI.UIComponent;
using System.Collections.Generic;

namespace ModuleRole
{
    public class RoleTitleView:KContainer
    {

        private CategoryList toggleGroup;
        private KScrollPage _scrollPage;
        private KList _list;
        private ListToggleGroup _listToggleGroup;

        protected void AddEventListener()
        {
            toggleGroup.onSelectedIndexChanged.AddListener(OnToggleGroupSelectedChangeHandler);
        }

        protected override void Awake()
        {
            KContainer _container = this.gameObject.GetComponent<KContainer>();
            toggleGroup = _container.GetChildComponent<CategoryList>("List_categoryList");
            _scrollPage = _container.GetChildComponent<KScrollPage>("ScrollPage_chenghao");
            _list = _scrollPage.GetChildComponent<KList>("mask/content");
            _listToggleGroup = _list.gameObject.AddComponent<ListToggleGroup>();
            _listToggleGroup.SetGap(0, -19);
            _listToggleGroup.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);

            AddEventListener();

        }

        protected override void OnDestroy()
        {
            toggleGroup.onSelectedIndexChanged.RemoveListener(OnToggleGroupSelectedChangeHandler);
            base.OnDestroy();
        }
        
        /// <summary>
        /// 类型选择切换
        /// </summary>
        /// <param name="toggleGroup"></param>
        /// <param name="index"></param>
        private void OnToggleGroupSelectedChangeHandler(CategoryList toggleGroup, int index)
        {

        }
    }
}
