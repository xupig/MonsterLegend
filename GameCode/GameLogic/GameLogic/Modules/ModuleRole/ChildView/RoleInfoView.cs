﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/8 11:04:06
 * 描述说明：角色信息子界面
 * *******************************************************/

using Common.Base;
using Common.ClientConfig;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using UnityEngine;
using UnityEngine.UI;

namespace ModuleRole
{
    public class RoleInfoView:KContainer
    {
       // private KProgressBar m_expProgress;//经验条
        //private KProgressBar m_physicalProgress;//体力条
        private KButton m_btnDetialInfo;
        private KButton m_btnReturn;
        private KButton _btnTitleEntry;

        private StateText m_textCareer;
        private StateText m_textName;
        private ArtNumberList m_fightPowerContainer;
        private StateText m_textHp;
        private StateText m_textAttack;
        private StateText m_textPhysicalDefence;
        private StateText m_textMagicDefence;
        //private StateText m_textExp;
        private StateText m_textStamina;
        private StateText m_textXName;
        private StateText m_textXValue;
        private StateText _textTitleName;
        private Locater _locater;
        private Locater _fightForceLocater;

       // private KContainer m_titleBg;
        //private Locater m_nameLocater;

        public KComponentEvent onShowRoleDetailClick = new KComponentEvent();
        public KComponentEvent onReturnClick = new KComponentEvent();

        protected override void Awake()
        {
            m_btnDetialInfo = GetChildComponent<KButton>("Button_xiangxishuxing");
            m_btnReturn = GetChildComponent<KButton>("Button_btnReturn");
            m_btnDetialInfo.Label.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
            m_btnReturn.Label.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
            _btnTitleEntry = GetChildComponent<KButton>("Container_mingcheng/Button_chenghaoruko");
            m_textPhysicalDefence = GetChildComponent<StateText>("Label_txtPhysicalDefence");
            m_textMagicDefence = GetChildComponent<StateText>("Label_txtMagicDefence");
            m_textAttack = GetChildComponent<StateText>("Label_txtDamage");
            m_textHp = GetChildComponent<StateText>("Label_txtHp");
            //m_textFightValue = GetChildComponent<StateText>("Label_txtFightValue");
            m_textCareer = GetChildComponent<StateText>("Label_txtZhiye");
            m_textName = GetChildComponent<StateText>("Container_mingcheng/Label_txtName");
            //m_textExp = GetChildComponent<StateText>("Label_txtExp");
            _textTitleName = GetChildComponent<StateText>("Container_mingcheng/Label_txtchenghao");
            m_textStamina = GetChildComponent<StateText>("Label_txtNaili");
            m_textXName = GetChildComponent<StateText>("Label_text05");
            m_textXValue = GetChildComponent<StateText>("Label_txtX");
            //m_expProgress = GetChildComponent<KProgressBar>("ProgressBar_exp");
            //m_titleBg = GetChildComponent<KContainer>("Container_mingchengbg");
            //m_nameLocater = AddChildComponent<Locater>("Container_mingcheng");
            _fightForceLocater = AddChildComponent<Locater>("Container_mingcheng/Container_shengming");
            m_fightPowerContainer = AddChildComponent<ArtNumberList>("Container_mingcheng/Container_shengming/List_zhandouli");
            _locater = gameObject.AddComponent<Locater>();
            AddEventListener();
        }

        public void ShowDetialInfoBtn()
        {
            m_btnDetialInfo.gameObject.SetActive(true);
            m_btnReturn.gameObject.SetActive(false);
        }

        public void HideDetialInfoBtn()
        {
            m_btnDetialInfo.gameObject.SetActive(false);
            m_btnReturn.gameObject.SetActive(true);
        }

        protected void AddEventListener()
        {
            m_btnReturn.onClick.AddListener(OnReturnClick);
            m_btnDetialInfo.onClick.AddListener(OnDetialInfoClick);
            _btnTitleEntry.onClick.AddListener(OnShowTitlePanel);

            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player as EntityAvatar, EntityPropertyDefine.title, UpdateContent);
        }

        protected void RemoveEventListener()
        {
            m_btnReturn.onClick.RemoveListener(OnReturnClick);
            m_btnDetialInfo.onClick.RemoveListener(OnDetialInfoClick);
            _btnTitleEntry.onClick.RemoveListener(OnShowTitlePanel);

            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player as EntityAvatar, EntityPropertyDefine.title, UpdateContent);
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

      
        public void UpdateContent()
        {
            PlayerAvatar player = PlayerAvatar.Player;
             m_textPhysicalDefence.CurrentText.text = player.GetAttribute2String(fight_attri_config.PHY_DEFENSE, false);
            m_textMagicDefence.CurrentText.text = player.GetAttribute2String(fight_attri_config.MAG_DEFENSE, false);
            m_textAttack.CurrentText.text = player.GetAttribute2String(fight_attri_config.DMG_MIN, false) + "-" + player.GetAttribute2String(fight_attri_config.DMG_MAX,false);
            m_textStamina.CurrentText.text = player.GetAttribute2String(fight_attri_config.STAMINA, false);
            m_textHp.CurrentText.text = player.max_hp.ToString();
           // m_textFightValue.CurrentText.text = player.fight_force.ToString();
            m_textCareer.CurrentText.text = GameData.XMLManager.chinese[(int)player.vocation].__content;
            m_textName.CurrentText.text = string.Format("{0} LV{1}", player.name, player.level);
            m_textXName.CurrentText.text = attri_config_helper.GetAttributeName(GetAttributeId());
            m_textXValue.CurrentText.text = player.GetAttribute2String((fight_attri_config)GetAttributeId(), false);
            //m_textExp.CurrentText.text = player.exp.ToString() + "/" + GameData.XMLManager.avatar_level[player.level].__next_level_exp.ToString();
            if (PlayerAvatar.Player.title != 0)
            {
                _textTitleName.CurrentText.text = title_helper.GetTitleName(PlayerAvatar.Player.title);
            }
            else
            {
                _textTitleName.CurrentText.text = MogoLanguageUtil.GetContent(9500006);
            }
            _btnTitleEntry.Visible = function_helper.IsFunctionOpen(FunctionId.title);
            // m_expProgress.Percent = player.exp * 100 / GameData.XMLManager.avatar_level[player.level].__next_level_exp;
            //m_physicalProgress.Percent = player.energy * 100 / GameData.XMLManager.avatar_level[player.level].__energy_limit;
            RefreshFightForce();
        }

        private int GetAttributeId()
        {
            switch (PlayerAvatar.Player.vocation)
            {
                case Vocation.Warrior:
                    return (int)fight_attri_config.STRENGTH;
                case Vocation.Assassin:
                    return (int)fight_attri_config.DEXTERITY;
                case Vocation.Wizard:
                    return (int)fight_attri_config.INTELLIGENC;
                case Vocation.Archer:
                    return (int)fight_attri_config.CONCENTRATE;
            }
            return 0;
        }

        private void RefreshFightForce()
        {
            PlayerAvatar player = PlayerAvatar.Player;
            m_fightPowerContainer.SetNumber((int)player.fight_force);
            //RectTransform _rect = _fightForceLocater.GetComponent<RectTransform>();
            //_rect.sizeDelta = new Vector2(m_fightPowerContainer.transform.localPosition.x + m_fightPowerContainer.Width, _rect.sizeDelta.y);
            //_fightForceLocater.X = _locater.Width / 2 - _fightForceLocater.Width / 2 - _fightForceLocater.transform.parent.localPosition.x;
        }

        private void OnDetialInfoClick()
        {
            onShowRoleDetailClick.Invoke();

        }

        private void OnReturnClick()
        {
            onReturnClick.Invoke();
        }

        private void OnShowTitlePanel()
        {
            view_helper.OpenView(706);
        }

        protected override void OnEnable()
        {
            UpdateContent();
            TweenViewMove.Begin(gameObject, MoveType.Show, MoveDirection.Right);
        }

    }
}
