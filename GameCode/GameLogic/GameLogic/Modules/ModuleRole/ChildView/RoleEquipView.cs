﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/8 11:11:59
 * 描述说明：装备子界面
 * *******************************************************/

using Common.Base;
using Common.Events;
using Common.Structs;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;
using Common.Data;
using Common.Structs.Enum;
using Common.ExtendComponent;
using GameData;

namespace ModuleRole
{
    public class RoleEquipView : KContainer
    {
        //Key为装备部位值，Value为部位上格子
        private Dictionary<int, RoleEquipItem> _equipItemDic;
        private int _currentSelectIndex;

        private ModelComponent _avatarModel;//半身像
        private KButton _spiritButton;
        private StateText _spiritLevelText;
        private KContainer _iconContainer;

        private const int MAX_EQUIP_POSITION = 13;

        protected override void Awake()
        {
            _iconContainer = GetChildComponent<KContainer>("Container_icons");
            _spiritButton = _iconContainer.GetChildComponent<KButton>("Button_fengling");
            _spiritLevelText = _iconContainer.GetChildComponent<StateText>("Label_n12");
            _equipItemDic = new Dictionary<int, RoleEquipItem>();
            for (int i = 0; i < MAX_EQUIP_POSITION; i++)
            {
                GameObject go = _iconContainer.GetChildComponent<KContainer>("Container_equipItem").GetChild(i); ;
                RoleEquipItem item = go.AddComponent<RoleEquipItem>();
                item.EquipPostion = ToEquipPosistion(i);
                item.onClick.AddListener(OnClickItem);
                _equipItemDic.Add(ToEquipPosistion(i), item);
            }
            _avatarModel = AddChildComponent<ModelComponent>("Container_roleModel");
            AddEventListener();
        }

        public void ShowEquipParticle(int position)
        {
            foreach(KeyValuePair<int,RoleEquipItem> kvp in _equipItemDic)
            {
                kvp.Value.HideParticle();
            }
            if(_equipItemDic.ContainsKey(position))
            {
                _equipItemDic[position].ShowParticle();
            }
        }

        private int ToEquipPosistion(int index)
        {
            return index + 1;
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<BagType>(BagEvents.Init, OnBodyEquipBagUpdate);
            if (_spiritButton!=null)
            {
                _spiritButton.onClick.AddListener(OnSpiritButtonClick);
            }
           
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<BagType>(BagEvents.Init, OnBodyEquipBagUpdate);
            if (_spiritButton!=null)
            {
                _spiritButton.onClick.RemoveListener(OnSpiritButtonClick);
            } 
        }

        protected override void OnDestroy()
        {
            foreach(KeyValuePair<int, RoleEquipItem> kvp in _equipItemDic)
            {
                kvp.Value.onClick.RemoveListener(OnClickItem);
            }
            RemoveEventListener();
        }

        public void UpdateContent()
        {
            if (_spiritButton!=null)
            {
                _spiritButton.Visible = PlayerDataManager.Instance.FunctionData.IsFunctionOpen(FunctionId.gemSpirit);
            }
            if (_spiritLevelText!=null)
            {
                _spiritLevelText.Visible = PlayerDataManager.Instance.FunctionData.IsFunctionOpen(FunctionId.gemSpirit);
                _spiritLevelText.CurrentText.text = PlayerAvatar.Player.spirit_level.ToString();
            }
           
            UpdateBodyEquip();
            UpdateWing();
            _avatarModel.SetStartRotationY(177.6f);
            _avatarModel.LoadPlayerModel(OnActorLoaded);
        }

        private void OnActorLoaded(ACTSystem.ACTActor actor)
        {
            actor.localPosition = new Vector3(actor.localPosition.x, actor.localPosition.y, 2000);
            _avatarModel.PlayRandomIdleAction();
        }

        private void UpdateBodyEquip()
        {
            foreach(KeyValuePair<int, RoleEquipItem> kvp in _equipItemDic)
            {
                EquipItemInfo equipInfo = PlayerDataManager.Instance.BagData.GetBodyEquipBagData().GetItemInfo(kvp.Key) as EquipItemInfo;
                if(kvp.Key == _currentSelectIndex && equipInfo == null)
                {
                    SelectEquipItem(0);
                }
                kvp.Value.SetEquip(equipInfo);
            }

        }

        private void UpdateWing()
        {
            _equipItemDic[(int)EquipType.wing].SetWing(PlayerDataManager.Instance.WingData.GetPutOnWingData());
        }

        private void OnSpiritButtonClick()
        {
            view_helper.OpenView(473);
        }

        private void OnBodyEquipBagUpdate(BagType bagType)
        {
            if(bagType == BagType.BodyEquipBag)
            {
                UpdateBodyEquip();
            }
        }

        private void OnClickItem(int index)
        {

            if(index == (int)EquipType.wing)//打开翅膀界面
            {
                PanelIdEnum.Information.Show(3);
            }
            else if (index == (int)EquipType.love)//打开情缘界面
            {
                //MessageBox.Show(false, MessageBox.DEFAULT_TITLE, "");
            }
            else if(_equipItemDic.ContainsKey(index))
            {
                SelectEquipItem(index);
            }
        }

        private void SelectEquipItem(int index)
        {
            if (_currentSelectIndex != 0)
            {
                _equipItemDic[_currentSelectIndex].SetSelectd(false);
            }
            if (index != 0)
            {
                _equipItemDic[index].SetSelectd(true);
            }
            _currentSelectIndex = index;
        }

        protected override void OnEnable()
        {
            UpdateContent();
        }

        protected override void OnDisable()
        {
            _avatarModel.RemoveAnimationTimer();
        }

    }
}
