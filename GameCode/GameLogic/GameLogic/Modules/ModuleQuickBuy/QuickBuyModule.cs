﻿#region 模块信息
/*==========================================
// 文件名：QuickBuyModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleQuickBuy
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/11 16:02:00
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleQuickBuy
{
    public class QuickBuyModule:BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            
        }
    }
}
