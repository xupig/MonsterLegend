﻿#region 模块信息
/*==========================================
// 文件名：QuickBuyPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleQuickBuy
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/10 20:42:02
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using Common.ExtendComponent;
using Common.Data;
using UnityEngine;
using ModuleCommonUI;
using MogoEngine.Events;
using Common.Events;
using ModuleCommonUI.Token;
using ModuleGuild;

namespace ModuleQuickBuy
{
    public class QuickBuyPanel:BasePanel
    {

        private KButton _btnBuy;
        private KButton _btnMinus;
        private KButton _btnMinimum;
        private KButton _btnAdd;
        private KButton _btnMaximum;
        private StateText _txtName;
        private StateText _txtNum;
        private StateText _txtBuyNum;
        //private StateText _txtDescription;
       // private KContainer _itemListContainer;
       // private KScrollPage _scrollPage;
       // private KList _list;
        private IconContainer _iconContainer;

        private int _buyNum;
        private int _maximum;
        private int _leftBuyCount = 0;

        private TokenItemData _buyData = null;
        private int _cost = 0;

        //private static int COMMON_ITEM = 1;
        //private static int REWARD_ITEM = 2;

        protected override void Awake()
        {
            _btnBuy = GetChildComponent<KButton>("Button_zhong");
            CloseBtn = GetChildComponent<KButton>("Container_BG/Button_close");
            _btnAdd = GetChildComponent<KButton>("Button_tianjia");
            _btnMaximum = GetChildComponent<KButton>("Button_shuangjia");
            _btnMinus = GetChildComponent<KButton>("Button_jianshao");
            _btnMinimum = GetChildComponent<KButton>("Button_shuangjian");

            _txtName = GetChildComponent<StateText>("Label_txtbiaoti");
            //_txtDescription = GetChildComponent<StateText>("Label_txtMiaoshu");
            _txtNum = GetChildComponent<StateText>("Label_txtSjine");
            _iconContainer = AddChildComponent<IconContainer>("Container_icon");
            _txtBuyNum = GetChildComponent<StateText>("Label_txtShuliang");

            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.QuickBuy; }
        }

        public override void OnShow(object data)
        {
            SetData(data);
            AddEventListener();
        }

        public override void OnClose()
        {
            RemoveEventListener();
            _buyData = null;
        }


        //private void OnTokenShopItemChange(int id)
        //{
        //    if (_buyData != null && _buyData.Id == id)
        //    {
        //        SetData(_buyData);
        //    }
        //}


        private void AddEventListener()
        {
            _btnBuy.onClick.AddListener(OnBuy);
            _btnAdd.onClick.AddListener(OnAdd);
            _btnMaximum.onClick.AddListener(OnMaximum);
            _btnMinus.onClick.AddListener(OnMinus);
            _btnMinimum.onClick.AddListener(OnMinimum);
           // EventDispatcher.AddEventListener<int>(TokenEvents.UPDATE, OnTokenShopItemChange);
        }

        private void RemoveEventListener()
        {
            _btnBuy.onClick.RemoveListener(OnBuy);
            _btnAdd.onClick.RemoveListener(OnAdd);
            _btnMaximum.onClick.RemoveListener(OnMaximum);
            _btnMinus.onClick.RemoveListener(OnMinus);
            _btnMinimum.onClick.RemoveListener(OnMinimum);
            //EventDispatcher.RemoveEventListener<int>(TokenEvents.UPDATE, OnTokenShopItemChange);
        }

        private void OnBuy()
        {
            if (_buyNum <= 0 || _buyNum > _leftBuyCount)
            {
                Tips();
            }
            else
            {
                if (PlayerAvatar.Player.CheckCostLimit(_buyData.Cost, _buyNum, true) == 0)
                {
                    TokenManager.Instance.TokenExchange(_buyData.Id, _buyNum);
                }
            }
        }

        private void OnAdd()
        {
            if(_buyNum<_maximum)
            {
                _buyNum++;
                UpdateCost();
            }
            else
            {
                Tips();
            }
        }

        private void Tips()
        {
            if (_buyNum >= _leftBuyCount)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, (60033).ToLanguage(), PanelIdEnum.MainUIField);
            }
            else if (_buyNum >= _buyData.TotalLeftCount)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, (120115).ToLanguage(), PanelIdEnum.MainUIField);
            }
            else
            {
                if (PlayerAvatar.Player.CheckCostLimit(_buyData.Cost, _buyNum + 1, true) == 0)
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, (60039).ToLanguage(_leftBuyCount), PanelIdEnum.MainUIField);
                }
            }
        }

        private void OnMaximum()
        {
            if (_buyNum < _maximum)
            {
                _buyNum = (_buyNum + 10) / 10 * 10;
                _buyNum = Math.Min(_maximum, _buyNum);
                UpdateCost();
            }
            else
            {
                Tips();
            }
        }

        private void OnMinus()
        {
            if(_buyNum>1)
            {
                _buyNum--;
                UpdateCost();
            }
        }

        private void OnMinimum()
        {
            _buyNum = (_buyNum - 10) / 10 * 10;
            _buyNum = Math.Max(1, _buyNum);
            UpdateCost();
        }

        private void UpdateCost()
        {
            //_btnAdd.enabled = _buyNum < _maximum;
            _btnMinus.enabled = _buyNum > 1;
            _btnMinimum.enabled = _buyNum > 1;
            _txtBuyNum.CurrentText.text = _buyNum.ToString();
            _txtNum.CurrentText.text = string.Format("x{0}", _buyNum * _cost);
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            _buyData = data as TokenItemData;
            BuyTokenShopItem();
        }

        private void BuyTokenShopItem()
        {
            _buyNum = 1;
            //_txtDescription.gameObject.SetActive(tokenItem.__good_type == COMMON_ITEM);
            _txtName.CurrentText.text = item_helper.GetName(_buyData.ItemId);
            _btnBuy.Label.ChangeAllStateText((60037).ToLanguage());
            _btnBuy.Label.ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);
            InitTokenCost();
            UpdateCost();
        }

        private void InitTokenCost()
        {
            foreach (KeyValuePair<string, string> kvp in _buyData.Cost)
            {
                _cost = int.Parse(kvp.Value);
                int costId = int.Parse(kvp.Key);
                int num = item_helper.GetMyMoneyCountByItemId(costId);
                int canBuyNum = num / _cost;
                _leftBuyCount = _buyData.DailyLeftCount;
                _maximum = Math.Min(canBuyNum, Math.Min(_leftBuyCount, _buyData.TotalLeftCount));
                 _iconContainer.SetIcon(item_helper.GetIcon(costId));
                return;
            }
        }

    }
}
