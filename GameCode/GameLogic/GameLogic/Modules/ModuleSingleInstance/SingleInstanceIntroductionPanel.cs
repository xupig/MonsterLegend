﻿#region 模块信息
/*==========================================
// 文件名：SingleInstanceIntroductionPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleSingleInstance
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/11 14:38:34
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Game.UI.UIComponent;
using GameData;
using System.Collections.Generic;
namespace ModuleSingleInstance
{
    public class SingleInstanceIntroductionPanel : BasePanel
    {
        private KScrollView _petScrollView;
        private KList _petList;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.SingleInstanceIntroduction; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _petScrollView = GetChildComponent<KScrollView>("ScrollView_pets");
            _petScrollView.ScrollRect.horizontal = true;
            _petScrollView.ScrollRect.vertical = false;
            _petList = _petScrollView.GetChildComponent<KList>("mask/content");
            _petList.SetPadding(0, 0, 0, 23);
            _petList.SetGap(0, 23);
        }

        public override void OnShow(object data)
        {
            List<PetInstanceItemData> list = GetPetItemData(global_params_helper.GetInstancePetDict());
            _petList.SetDataList<PetInstanceItem>(list);
        }

        private List<PetInstanceItemData> GetPetItemData(Dictionary<int, int> dictionary)
        {
            List<PetInstanceItemData> result = new List<PetInstanceItemData>();
            foreach (var pair in dictionary)
            {
                PetInstanceItemData data = new PetInstanceItemData();
                data.chapterId = pair.Key;
                data.petId = pair.Value;
                data.SetLockChapterId();
                result.Add(data);
            }
            return result;
        }

        public override void OnClose()
        {

        }
    }
}