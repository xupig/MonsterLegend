﻿#region 模块信息
/*==========================================
// 文件名：PetInstanceItemData
// 命名空间: GameLogic.GameLogic.Modules.ModuleSingleInstance.Item
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/11 17:08:03
// 描述说明：
// 其他：
//==========================================*/
#endregion

using GameData;
using System.Collections.Generic;
namespace ModuleSingleInstance
{
    public class PetInstanceItemData
    {
        //PetID
        public int petId;

        // 对应宠物本的章节ID
        public int chapterId;

        // 需要解锁的章节ID
        public int lockChapterId;

        public void SetLockChapterId()
        {
            int result = 0;
            int instanceId = chapters_helper.GetInstanceIdList(chapterId)[0];
            Dictionary<string, string> unlockDict = instance_helper.GetUnlockConditions(instanceId);
            if (unlockDict.ContainsKey("10"))
            {
                result = int.Parse(unlockDict["10"]);
            }
            lockChapterId = result;
        }
    }
}
