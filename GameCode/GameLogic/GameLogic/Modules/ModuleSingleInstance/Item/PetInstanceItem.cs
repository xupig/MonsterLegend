﻿#region 模块信息
/*==========================================
// 文件名：PetInstanceItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleSingleInstance.Item
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/11 17:05:06
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using UnityEngine;
namespace ModuleSingleInstance
{
    public class PetInstanceItem : KList.KListItemBase
    {
        private StateText _petName;
        private ModelComponent _petContainer;
        private KButton _enterBtn;
        private StateText _unlockConditionUp;
        private StateText _unlockConditionDown;
        private string unlockTemplate;
        
        private PetInstanceItemData _data;

        protected override void Awake()
        {
            _petName = GetChildComponent<StateText>("Label_petName");
            _petContainer = AddChildComponent<ModelComponent>("Container_petModel");
            _enterBtn = GetChildComponent<KButton>("Button_jinru");
            _unlockConditionUp = GetChildComponent<StateText>("Label_txt1");
            _unlockConditionDown = GetChildComponent<StateText>("Label_txtXianzhitiaojian");
            _unlockConditionDown.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
            unlockTemplate = _unlockConditionDown.CurrentText.text;
            AddEventListener();
        }

        public override void Dispose()
        {
            RemoveEventListener();
        }


        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as PetInstanceItemData;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            RefreshPetName();
            RefreshPetModel();
            RefreshBtnState();
        }

        private void RefreshPetName()
        {
            _petName.CurrentText.text = pet_helper.GetName(_data.petId);
        }

        private void RefreshPetModel()
        {
            const int default_Quality = 2;
            int modelId = pet_quality_helper.GetModelID(_data.petId, default_Quality);
            _petContainer.LoadModel(modelId);
        }

        private void RefreshBtnState()
        {
            if (PlayerDataManager.Instance.CopyData.IsChapterPass(_data.lockChapterId) == true)
            {
                _enterBtn.Visible = true;
                _unlockConditionUp.Visible = false;
                _unlockConditionDown.Visible = false;
            }
            else
            {
                _enterBtn.Visible = false;
                _unlockConditionUp.Visible = true;
                _unlockConditionDown.Visible = true;
                _unlockConditionDown.CurrentText.text = string.Format(unlockTemplate, chapters_helper.GetPage(_data.lockChapterId));
            }
        }

        private void AddEventListener()
        {
            _enterBtn.onClick.AddListener(OnEnterBtnClick);
        }

        private void RemoveEventListener()
        {
            _enterBtn.onClick.RemoveListener(OnEnterBtnClick);
        }

        private void OnEnterBtnClick()
        {
            PanelIdEnum.SingleInstance.Show(_data.chapterId);
        }
    }

}