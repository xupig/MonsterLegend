﻿#region 模块信息
/*==========================================
// 文件名：SingleInstanceModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleSingleInstance
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/7/9 18:57:28
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
namespace ModuleSingleInstance
{
    public class SingleInstanceModule : BaseModule
    {

        public SingleInstanceModule()
        {

        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.SingleInstanceIntroduction, "Container_SingleInstanceIntroduction", MogoUILayer.LayerUIPanel, "ModuleSingleInstance.SingleInstanceIntroductionPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.SingleInstance, "Container_SingleInstancePanel", MogoUILayer.LayerUIPanel, "ModuleSingleInstance.SingleInstancePanel", BlurUnderlay.Have, HideMainUI.Hide);
        }
    }
}