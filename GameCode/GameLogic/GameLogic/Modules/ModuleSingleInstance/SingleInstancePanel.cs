﻿#region 模块信息
/*==========================================
// 文件名：SingleInstancePanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleSingleInstance
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/7/9 18:57:04
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCopy;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System.Collections.Generic;
namespace ModuleSingleInstance
{
    public class SingleInstancePanelData
    {
        public int chapterId;
        public int currentIndex;

        public SingleInstancePanelData(int chapterId, int index)
        {
            this.chapterId = chapterId;
            this.currentIndex = index;
        }
    }

    public class SingleInstancePanel : BasePanel
    {
        public static int chapterId;
        private CategoryList _singleCopyTypeCategoryList;
        private List<int> _instanceIdList;
        private InstanceContentView _contentView;
        private MoneyItem _eneryItem;
        private StateText _currentEnergy;
        private int currentIndex = -1;

        private int sum;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.SingleInstance; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _singleCopyTypeCategoryList = GetChildComponent<CategoryList>("List_categoryList");
            _contentView = AddChildComponent<InstanceContentView>("Container_fubenNeiRong");
            _eneryItem = AddChildComponent<MoneyItem>("Container_fubenNeiRong/Container_tili");
            _eneryItem.SetMoneyType(public_config.MONEY_TYPE_ENERGY);
            _eneryItem.onAddClick = OnAddMoneyClick;
            _currentEnergy = GetChildComponent<StateText>("Container_fubenNeiRong/Container_tili/Label_txtGold");
        }

        private void OnAddMoneyClick()
        {
            PanelIdEnum.EnergyBuying.Show();
        }

        public override void OnShow(object data)
        {
            if (data is List<string>)
            {
                List<string> list = data as List<string>;
                if (list.Count > 0)
                {
                    chapterId = int.Parse(list[0]);
                }
            }
            else if (data is int)
            {
                chapterId = (int)data;
            }
            else if (data is SingleInstancePanelData)
            {
                chapterId = (data as SingleInstancePanelData).chapterId;
                currentIndex = (data as SingleInstancePanelData).currentIndex;
            }
            else if (data is int[])
            {
                int[] dataArray = data as int[];
                dataArray[1] = dataArray[1] > 0 ? dataArray[1] - 1 : 0;
                SingleInstancePanelData panelData = new SingleInstancePanelData(dataArray[0], dataArray[1]);
                chapterId = panelData.chapterId;
                currentIndex = panelData.currentIndex;
            }
            _contentView.Visible = false;
            _instanceIdList = chapters_helper.GetInstanceIdList(chapterId);
            if (currentIndex == -1)
            {
                currentIndex = CanFightMaxInstanceId(_instanceIdList);
            }
            RefreshCategoryList();
            AddEventListener();
            SingleInstanceManager.Instance.RequesetGetInstanceState(chapterId);
            SingleInstanceManager.Instance.GetChapterInfo();
        }

        public override void OnClose()
        {
            _contentView.Hide();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _singleCopyTypeCategoryList.onSelectedIndexChanged.AddListener(OnIndexChanged);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.energy, OnEnergyChanged);
            EventDispatcher.AddEventListener<int>(SingleInstanceEvents.BEGIN_REFRESH, Refresh);
            EventDispatcher.AddEventListener<PbMopUpInfo>(SingleInstanceEvents.OPEN_SWEEP_PANEL, SweepFillInstance);
        }

        private void RemoveEventListener()
        {
            _singleCopyTypeCategoryList.onSelectedIndexChanged.RemoveListener(OnIndexChanged);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.energy, OnEnergyChanged);
            EventDispatcher.RemoveEventListener<int>(SingleInstanceEvents.BEGIN_REFRESH, Refresh);
            EventDispatcher.RemoveEventListener<PbMopUpInfo>(SingleInstanceEvents.OPEN_SWEEP_PANEL, SweepFillInstance);
        }

        public void SweepFillInstance(PbMopUpInfo pbMopUpInfo)
        {
            SingleInstancePanelData data = new SingleInstancePanelData(chapterId, currentIndex);
            CopySweepDataWrapper wrapper = new CopySweepDataWrapper(1, GetBaseItemDataList(pbMopUpInfo.mop_up_reward), 3);
            PanelIdEnum.CopySweep.Show(wrapper);
        }

        public List<List<BaseItemData>> GetBaseItemDataList(List<PbMopUpRewards> list)
        {
            List<List<BaseItemData>> resultList = new List<List<BaseItemData>>();
            for (int i = 0; i < list.Count; i++)
            {
                PbMopUpRewards pbMopUpRewards = list[i];
                resultList.Add(PbItemHelper.ToBaseItemDataList(pbMopUpRewards.reward));
            }
            return resultList;
        }

        private void OnEnergyChanged()
        {
            _currentEnergy.CurrentText.text = PlayerAvatar.Player.energy.ToString();
        }

        private void Refresh(int p)
        {
            sum += p;
            if (sum == 3)
            {
                sum = 0;
                if (currentIndex >= _instanceIdList.Count)
                {
                    currentIndex = _instanceIdList.Count - 1;
                }
                _singleCopyTypeCategoryList.SelectedIndex = currentIndex;
                _contentView.Show(_instanceIdList[currentIndex]);
                _currentEnergy.CurrentText.text = PlayerAvatar.Player.energy.ToString();
            }
        }

        private int CanFightMaxInstanceId(List<int> _instanceIdList)
        {
            int result = 0;
            for (int i = 0; i < _instanceIdList.Count; i++)
            {
                if (player_condition_helper.JudgePlayerCondition(instance_helper.GetUnlockConditions(_instanceIdList[i])) == true)
                {
                    result = i;
                }
            }
            return result;
        }

        private void RefreshCategoryList()
        {
            _singleCopyTypeCategoryList.RemoveAll();
            List<CategoryItemData> categoryDataList = GetCategoryDataList(_instanceIdList);
            _singleCopyTypeCategoryList.SetDataList<CategoryListItem>(categoryDataList);
            _singleCopyTypeCategoryList.SelectedIndex = currentIndex;
        }

        private List<CategoryItemData> GetCategoryDataList(List<int> _instanceIdList)
        {
            List<CategoryItemData> result = new List<CategoryItemData>();
            for (int i = 0; i < _instanceIdList.Count; i++)
            {
                CategoryItemData data = CategoryItemData.GetCategoryItemData(instance_helper.GetInstanceCategoryName(_instanceIdList[i]));
                result.Add(data);
            }
            return result;
        }

        private void OnIndexChanged(CategoryList categoryList, int index)
        {
            currentIndex = index;
            _contentView.Refresh(_instanceIdList[index]);
        }

    }
}