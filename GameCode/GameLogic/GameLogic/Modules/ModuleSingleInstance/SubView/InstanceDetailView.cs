﻿using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using Common.Data;
using Common.Utils;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;

namespace ModuleSingleInstance
{
    public class InstanceDetailView : KContainer
    {
        private int currentInstanceId;

        private StateText _enterCountTxt;
        private string _enterCountStringTemplate;
        private StateText _enterDescribeTxt;
        private KButton _questionMarkBtn;
        private StateText _vipDescTxt;

        private ConditionListView _conditionListView;
        private RewardView _rewardView;
        private InstanceStateView _stateView;
        private Vector3 _initPosition;

        protected override void Awake()
        {
            _enterCountTxt = GetChildComponent<StateText>("Container_tiaozhanyaoqiu/Label_txtJinrucishu");
            _enterCountStringTemplate = _enterCountTxt.CurrentText.text;
            _enterDescribeTxt = GetChildComponent<StateText>("Container_tiaozhanyaoqiu/Label_txtTiaojian");
            _enterDescribeTxt.CurrentText.text = string.Empty;
            _vipDescTxt = GetChildComponent<StateText>("Container_tiaozhanyaoqiu/Label_txtTiaojian");
            _vipDescTxt.Visible = false;
            _conditionListView = AddChildComponent<ConditionListView>("Container_xuyaoxiaohao");
            _rewardView = AddChildComponent<RewardView>("Container_jiangpin");
            _stateView = AddChildComponent<InstanceStateView>("Container_bottomBtn");
            _questionMarkBtn = GetChildComponent<KButton>("Container_upperBtn/Button_wenhao");
            _questionMarkBtn.Visible = false;
            _initPosition = transform.localPosition;
        }

        public void Show(int instanceId)
        {
            AddEventListener();
            Refresh(instanceId);
        }

        public void Refresh(int instanceId)
        {
            currentInstanceId = instanceId;
            _conditionListView.Refresh(instanceId);
            _rewardView.Refresh(instanceId);
            _stateView.Refresh(instanceId);
            RefreshEnterTimes(SingleInstancePanel.chapterId);
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(SingleInstanceEvents.REFRESH_CHPATER_COUNTS, RefreshState);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(SingleInstanceEvents.REFRESH_CHPATER_COUNTS, RefreshState);
        }

        private void RefreshState()
        {
            if (Visible == false || currentInstanceId == 0)
            {
                return;
            }
            else
            {
                RefreshEnterTimes(SingleInstancePanel.chapterId);
                _stateView.RefreshState();
            }
        }

        private void RefreshEnterTimes(int chapterId)
        {
            int totalTime = SingleInstanceManager.Instance._singleInstanceData.GetChpaterTotalTimes(chapterId);
            int canEnterTime = totalTime - SingleInstanceManager.Instance._singleInstanceData.GetChapterEnterTimes(chapterId);
            if (canEnterTime <= 0)
            {
                _enterCountTxt.CurrentText.text = string.Format(_enterCountStringTemplate, "<color=#ff0000>" + canEnterTime + "</color>", totalTime);
            }
            else
            {
                _enterCountTxt.CurrentText.text = string.Format(_enterCountStringTemplate, canEnterTime, totalTime);
            }
        }

        public void Hide()
        {
            _stateView.Hide();
            if (Visible == true)
            {
                RemoveEventListener();
                Visible = false;
            }
        }

    }
}
