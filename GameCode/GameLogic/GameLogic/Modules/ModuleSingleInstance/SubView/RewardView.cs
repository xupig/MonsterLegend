﻿using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager.SubSystem;
using System.Collections.Generic;

namespace ModuleSingleInstance
{
    public class RewardView : KContainer
    {
        private KPageableList _rewardList;
        private KScrollPage _rewardPage;

        private const int NUM_PER_PAGE = 6;

        protected override void Awake()
        {
            base.Awake();
            _rewardPage = GetChildComponent<KScrollPage>("ScrollPage_jiangpin");
            _rewardList = GetChildComponent<KPageableList>("ScrollPage_jiangpin/mask/content");
            InitRewardList();
        }

        private void InitRewardList()
        {
            _rewardList.SetGap(0, 10);
            _rewardList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue);
        }

        public void Refresh(int instanceId)
        {
            _rewardList.RemoveAll();
            List<RewardData> rewardData = SingleInstanceManager.Instance.GetRewardDataListByInstanceId(instanceId);
            _rewardPage.TotalPage = rewardData.Count % NUM_PER_PAGE > 0 ? rewardData.Count / NUM_PER_PAGE + 1 : rewardData.Count / NUM_PER_PAGE;
            _rewardList.SetDataList<RewardGrid>(rewardData);
        }

    }
}
