﻿#region 模块信息
/*==========================================
// 文件名：InstanceContentView
// 命名空间: GameLogic.GameLogic.Modules.ModuleSingleInstance
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/7/9 19:26:25
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using GameMain.Entities;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using Common.ExtendComponent;

namespace ModuleSingleInstance
{
    public class InstanceContentView : KContainer
    {
        private InstanceOutLineView _outlineView;
        private InstanceDetailView _detailView;
        
        protected override void Awake()
        {
            _outlineView = AddChildComponent<InstanceOutLineView>("Container_left");
            _detailView = AddChildComponent<InstanceDetailView>("Container_right");
        }

        public void Show(int instanceId)
        {
            this.Visible = true;
            TweenViewMove.Begin(_outlineView.gameObject, MoveType.Show, MoveDirection.Left);
            TweenViewMove.Begin(_detailView.gameObject, MoveType.Show, MoveDirection.Right);
            _outlineView.Refresh(instanceId);
            _detailView.Show(instanceId);
        }

        public void Refresh(int instanceId)
        {
            this.Visible = true;
            TweenViewMove.Begin(_outlineView.gameObject, MoveType.Show, MoveDirection.Left);
            TweenViewMove.Begin(_detailView.gameObject, MoveType.Show, MoveDirection.Right);
            _outlineView.Refresh(instanceId);
            _detailView.Refresh(instanceId);
        }

        public void Hide()
        {
            _detailView.Hide();
            Visible = false;
        }


    }
}