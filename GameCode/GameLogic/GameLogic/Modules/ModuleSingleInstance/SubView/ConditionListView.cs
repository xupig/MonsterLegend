﻿using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System.Collections.Generic;

namespace ModuleSingleInstance
{
    public class ConditionListView : KContainer
    {
        private const int MAX_CROSS_CONDITION_MAX = 3;

        private CopyPassCondition[] _crossConditionArray;

        protected override void Awake()
        {
            _crossConditionArray = new CopyPassCondition[MAX_CROSS_CONDITION_MAX];
            for (int i = 0; i < MAX_CROSS_CONDITION_MAX; i++)
            {
                _crossConditionArray[i] = AddChildComponent<CopyPassCondition>(string.Format("Container_NR0{0}", i));
            }
        }

        public void Refresh(int instanceId)
        {
            instance cfg = instance_helper.GetInstanceCfg(instanceId);
            Dictionary<string, string[]> dicScoreRule = XMLManager.map[instance_helper.GetMapId(cfg)].__score_rule;
            int count = 0;
            foreach (KeyValuePair<string, string[]> pair in dicScoreRule)
            {
                float param = float.Parse(pair.Value[0]);
                string strParam = "";
                if (param < 0)
                {
                    param = param * -100;
                    strParam = param.ToString() + "%";
                }
                else
                {
                    strParam = param.ToString();
                }

                _crossConditionArray[count].SetDescribe(MogoLanguageUtil.GetContent(pair.Value[2], strParam));
                count++;
            }
            for (int i = 0; i < _crossConditionArray.Length; i++)
            {
                _crossConditionArray[i].Visible = i < dicScoreRule.Count;
            }
        }
    }
}
