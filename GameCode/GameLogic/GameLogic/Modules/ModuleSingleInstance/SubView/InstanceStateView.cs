﻿using Common.Data;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using ModuleCommonUI;
using GameMain.Entities;
#region 模块信息
/*==========================================
// 文件名：InstanceStateView
// 命名空间: GameLogic.GameLogic.Modules.ModuleSingleInstance
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/7/30 21:01:44
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace ModuleSingleInstance
{
    public class InstanceStateView : KContainer
    {
        private const int NEED_ENERGY_ID = 83009;
        private const int NEED_DIAMOND_ID = 80034;
        private int currentInstanceId;

        private StateText _lockTxt;
        private string lockStringTemplate;
        private StateText _cannotSweepTxt;
        private string cannotSweepString;
        private StateText _descTxt;

        private KButton _sweepBtn;
        private KButton _fightBtn;
        private KButton _buyChapterTimesBtn;

        private int currentBuyCostDiamond;

        protected override void Awake()
        {
            _lockTxt = GetChildComponent<StateText>("Label_txtqianti");
            lockStringTemplate = _lockTxt.CurrentText.text;
            _cannotSweepTxt = GetChildComponent<StateText>("Label_txtMmeirishouci");
            cannotSweepString = _cannotSweepTxt.CurrentText.text;
            _descTxt = GetChildComponent<StateText>("Label_txtXuyaotili");

            _sweepBtn = GetChildComponent<KButton>("Button_saodang");
            _fightBtn = GetChildComponent<KButton>("Button_tiaozhan");
            _buyChapterTimesBtn = GetChildComponent<KButton>("Button_goumai");
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _fightBtn.onClick.AddListener(OnFightBtnClicked);
            _sweepBtn.onClick.AddListener(onSweepBtnClicked);
            _buyChapterTimesBtn.onClick.AddListener(OnBuyCountBtnClicked);
            EventDispatcher.AddEventListener(SingleInstanceEvents.REFRESH_BUY_COUNTS, RefreshBuyCounts);
        }

        private void RemoveEventListener()
        {
            _fightBtn.onClick.RemoveListener(OnFightBtnClicked);
            _sweepBtn.onClick.RemoveListener(onSweepBtnClicked);
            _buyChapterTimesBtn.onClick.RemoveListener(OnBuyCountBtnClicked);
            EventDispatcher.RemoveEventListener(SingleInstanceEvents.REFRESH_BUY_COUNTS, RefreshBuyCounts);
        }

        private void OnBuyCountBtnClicked()
        {
            string content;
            if (PlayerAvatar.Player.money_bind_diamond >= currentBuyCostDiamond)
            {
                content = string.Format(MogoLanguageUtil.GetContent(55024), currentBuyCostDiamond);
                MessageBox.Show(true, string.Empty, content, OnConfirmClick, OnCancelClick, null, null, null, true, false);
            }
            else if (PlayerAvatar.Player.money_diamond >= currentBuyCostDiamond)
            {
                content = string.Format(MogoLanguageUtil.GetContent(55025), currentBuyCostDiamond - PlayerAvatar.Player.money_bind_diamond);
                MessageBox.Show(true, string.Empty, content, OnConfirmClick, OnCancelClick, null, null, null, true, false);
            }
            else
            {
                content = string.Format(MogoLanguageUtil.GetContent(55026), currentBuyCostDiamond);
                MessageBox.Show(true, string.Empty, content, OnConfirmClick, OnCancelClick, null, null, null, true, false);
            }
        }

        private void OnCancelClick()
        {
            MessageBox.CloseMessageBox();
        }

        private void OnConfirmClick()
        {
            SingleInstanceManager.Instance.RequesetBuyChapterCount(chapters_helper.GetChapterId(currentInstanceId));
        }

        private void onSweepBtnClicked()
        {
            SingleInstanceManager.Instance.RequestSweepInstance(currentInstanceId);
        }

        private void OnFightBtnClicked()
        {
            SingleInstanceManager.Instance.RequestEnterInstance(currentInstanceId);
        }

        public void Refresh(int instanceId)
        {
            currentInstanceId = instanceId;
            RefreshState();
        }

        public void RefreshState()
        {
            this.Visible = true;
            SingleInstanceState state = GetSingleInstanceState(currentInstanceId);
            RefreshLabelState(state, currentInstanceId);
        }

        private void RefreshLabelState(SingleInstanceState state, int instanceId)
        {
            HideAllChildren();
            switch (state)
            {
                case SingleInstanceState.noFightRights:
                    ShowNoFightRights(instanceId);
                    break;
                case SingleInstanceState.neverEnter:
                    ShowNeverEnterView(instanceId);
                    break;
                case SingleInstanceState.firstPass:
                    ShowFirstPassView(instanceId);
                    break;
                case SingleInstanceState.firstNotPass:
                    ShowNeverEnterView(instanceId);
                    break;
                case SingleInstanceState.NoCounts:
                    ShowBuyCountsView(instanceId);
                    break;
            }
        }

        private void ShowBuyCountsView(int instanceId)
        {
            int chapterId = chapters_helper.GetChapterId(instanceId);
            int priceId = SingleInstanceManager.Instance._singleInstanceData.GetPriceId(chapterId);
            SingleInstanceManager.RequestGetBuyCount(priceId);
        }

        private void RefreshBuyCounts()
        {
            int chapterId = chapters_helper.GetChapterId(currentInstanceId);
            int priceId = SingleInstanceManager.Instance._singleInstanceData.GetPriceId(chapterId);
            _buyChapterTimesBtn.Visible = true;
            _descTxt.Visible = true;
            int hasBuyCount = SingleInstanceManager.Instance._singleInstanceData.GetBuyCounts(priceId);
            if (hasBuyCount > vip_rights_helper.GetVIPMaxBuyCounts(PlayerAvatar.Player.vip_level))
            {
                _descTxt.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(50000));
            }
            else
            {
                currentBuyCostDiamond = price_list_helper.GetPrice(priceId, hasBuyCount);
                _descTxt.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(NEED_DIAMOND_ID), currentBuyCostDiamond);
            }
        }

        private void ShowFirstPassView(int instanceId)
        {
            _fightBtn.Visible = true;
            _sweepBtn.Visible = true;
            _descTxt.Visible = true;
            _descTxt.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(NEED_ENERGY_ID), instance_helper.GetCostEnergy(instanceId));
        }

        private void ShowNeverEnterView(int instanceId)
        {
            _cannotSweepTxt.Visible = true;
            _fightBtn.Visible = true;
            _descTxt.Visible = true;
            _descTxt.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(NEED_ENERGY_ID), instance_helper.GetCostEnergy(instanceId));
        }

        private void ShowNoFightRights(int instanceId)
        {
            _lockTxt.Visible = true;
            string content = player_condition_helper.GetConditionDescribe(instance_helper.GetUnlockConditions(instanceId));
            _lockTxt.CurrentText.text = string.Format(lockStringTemplate, content);
        }

        private SingleInstanceState GetSingleInstanceState(int instanceId)
        {
            int chapterId = chapters_helper.GetChapterId(instanceId);
            if (player_condition_helper.JudgePlayerCondition(instance_helper.GetUnlockConditions(instanceId)) == false)
            {
                return SingleInstanceState.noFightRights;
            }
            SingleInstanceState state = SingleInstanceManager.Instance._singleInstanceData.GetInstanceState(instanceId);
            int enterCount = SingleInstanceManager.Instance._singleInstanceData.GetChapterEnterTimes(chapterId);
            int totalCount = SingleInstanceManager.Instance._singleInstanceData.GetChpaterTotalTimes(chapterId);
            if (enterCount >= totalCount)
            {
                return SingleInstanceState.NoCounts;
            }
            return state;
        }

        private void HideAllChildren()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
        }

        public void Hide()
        {
            Visible = false;
        }

    }
}