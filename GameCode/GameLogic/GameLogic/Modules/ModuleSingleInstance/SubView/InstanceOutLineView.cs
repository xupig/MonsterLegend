﻿using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleSingleInstance
{
    public class InstanceOutLineView : KContainer
    {
        private StateText _instanceTitle;
        private KContainer _starContainer;
        private KContainer _instanceImageContainer;
        private Vector3 _initPosition;

        protected override void Awake()
        {
            _instanceTitle = GetChildComponent<StateText>("Container_biaoti/Label_txtFuben");
            _starContainer = GetChildComponent<KContainer>("Container_starxingxing");
            _instanceImageContainer = GetChildComponent<KContainer>("Container_fuben");
            _initPosition = transform.localPosition;
        }

        public void Refresh(int instanceId)
        {
            RefreshInstanceTitle(instanceId);
            RefrehInstanceImage(instanceId);
            RefreshInstanceStar(instanceId);
        }

        private void RefreshInstanceStar(int instanceId)
        {
            int starNums = PlayerDataManager.Instance.CopyData.GetHistoryMaxStar(instanceId);
            for (int i = 0; i <= 2; i++)
            {
                KContainer container = GetChildComponent<KContainer>("Container_xingxing/Container_star" + i);
                if (i >= starNums)
                {
                    HideStar(container);
                }
                else
                {
                    LightStar(container);
                }
            }
            
        }

        private void LightStar(KContainer container)
        {
            StateImage image1 = container.GetChildComponent<StateImage>("Image_xingxingIcon");
            StateImage image2 = container.GetChildComponent<StateImage>("Image_jiesuanxingxing02Icon");
            image1.Visible = true;
            image2.Visible = false;
        }

        private void HideStar(KContainer container)
        {
            StateImage image1 = container.GetChildComponent<StateImage>("Image_xingxingIcon");
            StateImage image2 = container.GetChildComponent<StateImage>("Image_jiesuanxingxing02Icon");
            image1.Visible = false;
            image2.Visible = true;
        }

        private void RefrehInstanceImage(int instanceId)
        {
            MogoAtlasUtils.AddIcon(_instanceImageContainer.gameObject, instance_helper.GetIcon(instanceId));
        }

        private void RefreshInstanceTitle(int instanceId)
        {
            _instanceTitle.CurrentText.text = instance_helper.GetInstanceName(instanceId);
        }

    }
}
