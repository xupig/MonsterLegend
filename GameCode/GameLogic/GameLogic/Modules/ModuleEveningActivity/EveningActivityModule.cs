﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEveningActivity
{
    public class EveningActivityModule:BaseModule
    {
        public override void Init()
        {
            AddPanel(PanelIdEnum.PVEPreview, "Container_PVEPreviewPanel", MogoUILayer.LayerUIPanel, "ModuleEveningActivity.PVEPreviewPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.PVEResult, "Container_PVEResultPanel", MogoUILayer.LayerUIPanel, "ModuleEveningActivity.CombatResultPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.BranchPreview, "Container_PVEPreviewPanel", MogoUILayer.LayerUIPanel, "ModuleEveningActivity.BranchPreviewPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.EveningActivityStatistic, "Container_EveningActivityStatisticPanel", MogoUILayer.LayerUnderPanel, "ModuleMainUI.EveningActivityStatisticPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }
    }
}
