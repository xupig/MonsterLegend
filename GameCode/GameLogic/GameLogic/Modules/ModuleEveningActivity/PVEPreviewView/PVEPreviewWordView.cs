﻿using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEveningActivity
{
    public class PVEPreviewWordView:KContainer
    {
        private KContainer _introduction;
        private KScrollView _itemScrollView;
        private KList _itemList;

        private int _activityId = 0;

        protected override void Awake()
        {
            base.Awake();
            _introduction = GetChildComponent<KContainer>("Container_tiaozhanyaoqiu/Container_command/Container_tiaozhan");
            _itemScrollView = GetChildComponent<KScrollView>("Container_zhuangbei/ScrollView_wupinliebiao");

            InitScrollView();
        }

        private void InitScrollView()
        {
            _itemList = _itemScrollView.GetChildComponent<KList>("mask/content");
            _itemList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue);
            _itemScrollView.ScrollRect.horizontal = true;
            _itemScrollView.ScrollRect.vertical = false;
            _itemScrollView.UpdateArrow();
            _itemList.SetGap(0, 28);
        }

        public void RefreshWord(int activityId)
        {
            _activityId = activityId;
            RefreshIntroduction();

        }

        private void RefreshIntroduction()
        {
            RefreshOpenTime();
            RefreshLevelLimit();
            RefreshAvatarNumLimit();
            RefreshEnterTimes();
            RefreshGetView();
        }

        private void RefreshOpenTime()
        {
            LimitActivityData activityData = activity_helper.GetActivity(_activityId);
            string str = GetOpenContent(activityData);
            str = string.Concat(MogoLanguageUtil.GetContent(97003), " : ", str);
            GameObject child = _introduction.GetChild(0);
            child.SetActive(true);
            StateText stateText = child.transform.GetChild(1).GetComponent<StateText>();
            stateText.CurrentText.text = str; 
        }

        private static string GetOpenContent(LimitActivityData activityData)
        {
            string content = string.Empty;
            for (int i = 0; i < activityData.openTimeStart.Count; i++)
            {
                string time = string.Format("{0}-{1}", MogoTimeUtil.ToUniversalTime(activityData.openTimeStart[i], "hh:mm"), MogoTimeUtil.ToUniversalTime(activityData.openTimeEnd[i], "hh:mm"));
                if (i != 0)
                {
                    content = string.Concat(content, MogoLanguageUtil.GetContent(102514));
                }
                content = string.Concat(content, time);
            }
            List<int> days = new List<int>();
            for (int i = 0; i < activityData.openWeekOfDay.Count; i++)
            {
                if (activityData.openWeekOfDay[i] == 0)
                {
                    days.Add(7);
                }
                else
                {
                    days.Add(activityData.openWeekOfDay[i]);
                }
            }
            days.Sort();
            string zhou = string.Empty;
            for (int i = 0; i < days.Count; i++)
            {
                if (i == 0)
                {
                    zhou = string.Concat(zhou, MogoLanguageUtil.GetContent(102513), MogoLanguageUtil.GetContent(102514 + days[i]));
                }
                else
                {
                    zhou = string.Concat(zhou, MogoLanguageUtil.GetContent(102514), MogoLanguageUtil.GetContent(102514 + days[i]));
                }
            }
            string str = string.Format("{0}{1}", zhou, content);
            return str;
        }

        private void RefreshLevelLimit()
        {
            LimitActivityData activityData = activity_helper.GetActivity(_activityId);
            instance ins = instance_helper.GetInstanceCfg(activityData.openData.__fid);

            int minLevel = instance_helper.GetMinLevel(ins);
            int maxLevel = instance_helper.GetMaxLevel(ins);

            GameObject child = _introduction.GetChild(1);
            child.SetActive(true);
            StateText stateText = child.transform.GetChild(1).GetComponent<StateText>();
            stateText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(97004), " : ",MogoLanguageUtil.GetContent(97014, minLevel));
        }

        private void RefreshAvatarNumLimit()
        {
            LimitActivityData activityData = activity_helper.GetActivity(_activityId);
            instance ins = instance_helper.GetInstanceCfg(activityData.openData.__fid);

            int minPlayerNum = instance_helper.GetMinPlayerNum(ins);
            int maxPlayerNum = instance_helper.GetMaxPlayerNum(ins);

            GameObject child = _introduction.GetChild(2);
            child.SetActive(true);
            StateText stateText = child.transform.GetChild(1).GetComponent<StateText>();
            if (minPlayerNum == maxPlayerNum)
            {
                if (minPlayerNum > PlayerDataManager.Instance.TeamData.TeammateDic.Count)
                {
                    stateText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(97005), " : ", MogoLanguageUtil.GetContent(97017, minPlayerNum));
                }
                else
                {
                    stateText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(97005), " : ", MogoLanguageUtil.GetContent(97016, minPlayerNum));
                }
            }
            else
            {
                stateText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(97005), " : ", MogoLanguageUtil.GetContent(83018, minPlayerNum, maxPlayerNum));
            }
        }

        private void RefreshEnterTimes()
        {
            LimitActivityData activityData = activity_helper.GetActivity(_activityId);
            int instId = activityData.openData.__fid;
            int instanceTime =  inst_type_operate_helper.GetDailyTimes(instance_helper.GetChapterType(instId));
            int leftCount = 0;
            if (instanceTime > 0)
            {
                leftCount = instanceTime - PlayerDataManager.Instance.CopyData.GetDailyTimesByChapterType(instance_helper.GetChapterType(instId));
            }
            else
            {
                instanceTime = instance_helper.GetDailyTimes(instId);
                leftCount = PlayerDataManager.Instance.CopyData.GetDailyLeftTimes(activityData.openData.__fid);
            }

            GameObject child = _introduction.GetChild(3);
            if (instanceTime > 0)
            {
                child.SetActive(true);
                StateText stateText = child.transform.GetChild(1).GetComponent<StateText>();
                if (leftCount > 0)
                {
                    stateText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(97006), " : ", MogoLanguageUtil.GetContent(97018, leftCount, instanceTime));
                }
                else
                {
                    stateText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(97006), " : ", MogoLanguageUtil.GetContent(97019, leftCount, instanceTime));
                }
            }
            else
            {
                child.SetActive(false);
            }
        }

        private void RefreshGetView()
        {
            LimitActivityData activityData = activity_helper.GetActivity(_activityId);
            instance ins = instance_helper.GetInstanceCfg(activityData.openData.__fid);
            List<RewardData> rewardDataList;
            if (instance_helper.GetChapterType(ins.__id) == ChapterType.GuildCopy)
            {
                List<int> rewardIdList = instance_reward_helper.GetRewardIdListByRewardLevel(ins.__id, RewardLevel.S);
                rewardDataList = item_reward_helper.GetRewardDataList(rewardIdList, PlayerAvatar.Player.vocation);
            }
            else
            {
                rewardDataList = night_activity_reward_helper.GetRewardList(ins.__id, PlayerAvatar.Player.level, PlayerAvatar.Player.vocation);
            }
            _itemList.RemoveAll();
            rewardDataList = RewardData.FilterSameItemId(rewardDataList);
            _itemList.SetDataList<RewardGrid>(rewardDataList);
        }

    }
}
