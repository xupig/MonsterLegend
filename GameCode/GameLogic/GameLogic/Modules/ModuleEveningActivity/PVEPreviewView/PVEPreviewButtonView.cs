﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEveningActivity
{
    public class PVEPreviewButtonView:KContainer
    {
        private KShrinkableButton _callButton;
        private KShrinkableButton _entryButton;
        private KShrinkableButton _matchButton;
        private KShrinkableButton _cancelButton;

        private StateText _matchText;
        private StateText _costText;

        private Vector3 _callBtnPosition;
        private Vector3 _entryButtonPosition;
        private Vector3 _matchButtonPosition;
        private Vector3 _cancelButtonposition;
        private Vector3 _txtMatchPosition;
        private Vector3 _txtCostPosition;

        private int _instanceId = 0;
        private int _activityId = 0;
        private uint _matchTimerId = 0;

        private bool _requestEnter = false;

        protected override void Awake()
        {
            base.Awake();
            _callButton = GetChildComponent<KShrinkableButton>("Container_saodang/Button_saodang");
            _entryButton = GetChildComponent<KShrinkableButton>("Container_saodang/Button_tiaozhanSaodang");
            _matchButton = GetChildComponent<KShrinkableButton>("Container_saodang/Button_saodangNci");
            _cancelButton = GetChildComponent<KShrinkableButton>("Container_saodang/Button_quxiao");
            _cancelButton.Visible = false;

            _matchText = GetChildComponent<StateText>("Label_txtPipei");
            _costText = GetChildComponent<StateText>("Label_txtXuyaotili");

            InitButtonView();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            ResetMatchTimer();
            RemoveEventListener();
            base.OnDisable();
        }

        public void RefreshButton(int activityId)
        {
            _activityId = activityId;
            LimitActivityData activityData = activity_helper.GetLimitActivity(activityId);
            _instanceId = activityData.openData.__fid;
            RefreshMatchView();
            RefreshCostView();
            RefreshButtonLayout();
        }

        private void AddEventListener()
        {
            _callButton.onClick.AddListener(OnCallButtonClick);
            _matchButton.onClick.AddListener(OnMatchButtonClick);
            _entryButton.onClick.AddListener(OnEnterButtonClick);
            _cancelButton.onClick.AddListener(OnCancelButtonClick);
            EventDispatcher.AddEventListener(CopyEvents.ON_MATCH_MISSION_CHANGE, RefreshMatchView);
            EventDispatcher.AddEventListener(TeamInstanceEvents.On_Call_People_Succeed, OnCallPeopelSucceed);
            EventDispatcher.AddEventListener(TeamEvent.TEAM_QUIT, ExitTeamEnterMission);
        }

        private void RemoveEventListener()
        {
            _callButton.onClick.RemoveListener(OnCallButtonClick);
            _matchButton.onClick.RemoveListener(OnMatchButtonClick);
            _entryButton.onClick.RemoveListener(OnEnterButtonClick);
            _cancelButton.onClick.RemoveListener(OnCancelButtonClick);
            EventDispatcher.RemoveEventListener(CopyEvents.ON_MATCH_MISSION_CHANGE, RefreshMatchView);
            EventDispatcher.RemoveEventListener(TeamInstanceEvents.On_Call_People_Succeed, OnCallPeopelSucceed);
            EventDispatcher.RemoveEventListener(TeamEvent.TEAM_QUIT, ExitTeamEnterMission);
        }

        private void OnCallPeopelSucceed()
        {
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83026), PanelIdEnum.MainUIField);
        }

        private void OnCallButtonClick()
        {
            if (_isMatching)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.COPY_MATCHING_CAN_NOT_CALL));
                return;
            }
            if (activity_helper.IsOpen(_activityId) == false)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(44317), PanelIdEnum.MainUIField);
                return;
            }

            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int maxNum = instance_helper.GetMaxPlayerNum(ins);
            if (PlayerDataManager.Instance.TeamData.TeammateDic.Count >= maxNum)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83033), PanelIdEnum.MainUIField);
                return;
            }
            if (CheckMeetCondition())
            {
                TeamInstanceManager.Instance.CallPeople(team_helper.GetGameIdByInstanceId(_instanceId));
            }
        }

        private void OnMatchButtonClick()
        {
            if (CheckMeetCondition())
            {
                instance ins = instance_helper.GetInstanceCfg(_instanceId);
                inst_match_rule matchRule = inst_match_rule_helper.GetMatchRule(ins.__auto_team_match);
                //单人PVP,需要检查队伍信息
                if (ins.__auto_team_match != 0 && matchRule.__group_player_num <= 1)
                {
                    CopyLogicManager.Instance.RequestEnterCopy(ChapterType.EveningActivity, IgnoreTeamEnterMission, _instanceId);
                }
                else
                {
                    MissionManager.Instance.RequsetEnterMission(_instanceId, 1);
                }
            }
        }

        private void ExitTeamEnterMission()
        {
            if (_requestEnter == true)
            {
                _requestEnter = false;
                MissionManager.Instance.RequsetEnterMission(_instanceId, 1);
            }
        }

        private void IgnoreTeamEnterMission()
        {
            //如果在队伍中则退出队伍
            if (PlayerDataManager.Instance.TeamData.IsInTeam())
            {
                _requestEnter = true;
                TeamManager.Instance.LeaveTeam();
            }
            else
            {
                _requestEnter = false;
                MissionManager.Instance.RequsetEnterMission(_instanceId, 1);
            }
        }

        private void OnEnterButtonClick()
        {
            if (_isMatching)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.COPY_MATCHING_CAN_NOT_ENTRY));
                return;
            }
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int minNum = instance_helper.GetMinPlayerNum(ins);

            inst_match_rule matchRule = inst_match_rule_helper.GetMatchRule(ins.__auto_team_match);
            if (matchRule.__group_player_num > 1 && PlayerDataManager.Instance.TeamData.TeammateDic.Count < minNum)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83029, minNum), PanelIdEnum.MainUIField);
                return;
            }
            else if (CheckMeetCondition())
            {
                CopyLogicManager.Instance.RequestEnterCopy(ChapterType.EveningActivity, EnterCopy, _instanceId);
            }
        }

        private void EnterCopy()
        {
            MissionManager.Instance.RequsetEnterMission(_instanceId);
        }

        private void OnCancelButtonClick()
        {
            _requestEnter = false;
            if (PlayerDataManager.Instance.TeamData.TeammateDic.Count > 0 && PlayerAvatar.Player.dbid != PlayerDataManager.Instance.TeamData.CaptainId)
            {
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(97024));
                return;
            }
            MissionManager.Instance.RequsetCancelMatchMission(_instanceId);
        }

        private void InitButtonView()
        {
            StateText txt = _matchButton.GetChildComponent<StateText>("label");
            txt.ChangeAllStateText(MogoLanguageUtil.GetContent(83005));
            txt = _callButton.GetChildComponent<StateText>("label");
            txt.ChangeAllStateText(MogoLanguageUtil.GetContent(83006));
            txt = _entryButton.GetChildComponent<StateText>("label");
            txt.ChangeAllStateText(MogoLanguageUtil.GetContent(83007));
            txt = _cancelButton.GetChildComponent<StateText>("label");
            txt.ChangeAllStateText(MogoLanguageUtil.GetContent(83022));

            _callBtnPosition = _callButton.transform.localPosition;
            _matchButtonPosition = _matchButton.transform.localPosition;
            _entryButtonPosition = _entryButton.transform.localPosition;
            _cancelButtonposition = _cancelButton.transform.localPosition;
            _txtMatchPosition = _matchText.transform.localPosition;
            _txtCostPosition = _costText.transform.localPosition;
        }

        private bool CheckMeetCondition()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int minLevel = instance_helper.GetMinLevel(ins);
            if (PlayerAvatar.Player.level < minLevel)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83027, minLevel), PanelIdEnum.MainUIField);
                return false;
            }
            int currCount = PlayerDataManager.Instance.CopyData.GetDailyTimes(_instanceId);
            int instanceTimes = instance_helper.GetDailyTimes(_instanceId);
            int maxCount = 0;
            if (instanceTimes == 0)
            {
                maxCount = PlayerDataManager.Instance.TeamInstanceData.MaxEnterTimes;
            }
            else
            {
                maxCount = Math.Min(instance_helper.GetDailyTimes(_instanceId), PlayerDataManager.Instance.TeamInstanceData.MaxEnterTimes);
            }
            if (currCount >= maxCount)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83028), PanelIdEnum.MainUIField);
                return false;
            }
            return true;
        }

        private bool _isMatching = false;
        private void RefreshMatchView()
        {
            ulong matchTime = PlayerDataManager.Instance.CopyData.GetMatchTime(_instanceId);
            ResetMatchTimer();
            if (matchTime > 0)
            {
                _matchTimerId = TimerHeap.AddTimer(0, 1000, OnMatchTextShowStep);
                _matchText.Visible = true;
                _matchButton.Visible = false;
                _cancelButton.Visible = true;
                _isMatching = true;
            }
            else
            {
                _matchText.Visible = false;
                _matchButton.Visible = true;
                _cancelButton.Visible = false;
                _isMatching = false;
            }
        }

        private void RefreshCostView()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int cost = instance_helper.GetCostEnergy(ins);
            if (cost != 0)
            {
                _costText.Visible = true;
                _costText.CurrentText.text = MogoLanguageUtil.GetContent(83009, instance_helper.GetCostEnergy(ins));
            }
            else
            {
                _costText.Visible = false;
            }
        }

        private void ResetMatchTimer()
        {
            if (_matchTimerId != 0)
            {
                TimerHeap.DelTimer(_matchTimerId);
                _matchTimerId = 0;
            }
        }

        private void RefreshButtonLayout()
        {
            LimitActivityData activityData = activity_helper.GetActivity(_activityId);
            instance ins = instance_helper.GetInstanceCfg(activityData.openData.__fid);
            int minPlayerNum = instance_helper.GetMinPlayerNum(ins);
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            int mapId = instance_helper.GetMapId(ins);
            _callButton.transform.localPosition = _callBtnPosition;
            _matchButton.transform.localPosition = _matchButtonPosition;
            _entryButton.transform.localPosition = _entryButtonPosition;
            _cancelButton.transform.localPosition = _cancelButtonposition;

            inst_match_rule matchRule = inst_match_rule_helper.GetMatchRule(ins.__auto_team_match);
            if (matchRule.__group_player_num <= 1)
            {
                _entryButton.Visible = false;
                _matchButton.Visible = true;
                _callButton.Visible = false;
            }
            else if(map_helper.GetMap(mapId).__group < 2)
            {
                _callButton.Visible = true;
                if (teamData.TeammateDic.Count >= minPlayerNum)
                {
                    _entryButton.Visible = true;
                    _matchButton.Visible = false;
                }
                else
                {
                    _entryButton.Visible = false;
                    _matchButton.Visible = true;
                }
            }
            else
            {
                _callButton.Visible = true;
                _matchButton.Visible = true;
                _entryButton.Visible = false;
            }
            RefreshShrinkButton();
        }

        private void RefreshShrinkButton()
        {
            _callButton.RefreshRectTransform();
            _matchButton.RefreshRectTransform();
            _entryButton.RefreshRectTransform();
            _cancelButton.RefreshRectTransform();
        }

        private void OnMatchTextShowStep()
        {
            uint waitTime = uint.Parse(global_params_helper.GetConfig(104).__value) * 1000;
            ulong currentTime = Global.serverTimeStamp;
            ulong matchTime = PlayerDataManager.Instance.CopyData.GetMatchTime(_instanceId);
            ulong leftTime = (waitTime > (currentTime - matchTime)) ? (waitTime - (currentTime - matchTime)) : 0;
            int tick = Mathf.RoundToInt((float)leftTime / 1000);
            tick = tick > 0 ? tick : 0;
            _matchText.CurrentText.text = MogoLanguageUtil.GetContent(83000, tick);
        }
    }
}
