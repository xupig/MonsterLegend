﻿using Common.Data;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEveningActivity
{
    public class PVEPreviewRightView : KContainer
    {
        private KButton _ruleButton;
        private PVEPreviewButtonView _buttonView;
        private PVEPreviewWordView _wordView;

        public KComponentEvent onRuleButtonClick = new KComponentEvent();

        protected override void Awake()
        {
            base.Awake();
            _ruleButton = GetChildComponent<KButton>("Container_upperBtn/Button_fubenjilv");

            _wordView = AddChildComponent<PVEPreviewWordView>("Container_word");
            _buttonView = AddChildComponent<PVEPreviewButtonView>("Container_bottomBtn");

            AddEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

        public void RefreshRightView(int activityId)
        {
            _wordView.RefreshWord(activityId);
            _buttonView.RefreshButton(activityId);

        }

        private void AddEventListener()
        {
            _ruleButton.onClick.AddListener(OnRuleButtonClick);
        }

        private void RemoveEventListener()
        {
            _ruleButton.onClick.RemoveListener(OnRuleButtonClick);
        }

        private void OnRuleButtonClick()
        {
            onRuleButtonClick.Invoke();
        }
    }
}
