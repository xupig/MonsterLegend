﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEveningActivity
{
    public class PVEPreviewRuleView:KContainer
    {
        private KButton _closeBtn;

        protected override void Awake()
        {
            base.Awake();
            _closeBtn = GetChildComponent<KButton>("Container_tanchuanglBg/Button_close");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        private void AddEventListener()
        {
            _closeBtn.onClick.AddListener(OnCloseView);
        }

        private void RemoveEventListener()
        {
            _closeBtn.onClick.RemoveListener(OnCloseView);
        }

        private void OnCloseView()
        {
            this.Visible = false;
        }
    }
}
