﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using ModuleActivity;
using ModuleCommonUI;
using ModuleTeam.ChildView;
using UnityEngine;

namespace ModuleEveningActivity
{
    public class PVEPreviewLeftView : KContainer
    {
        private int _instanceId;
        private StateText _txtPVEName;
        private StateImage _teamBgImg;
        private KButton _inviteButton;
        private IconContainer _copyImageContainer;

        private CopyTeamView _copyTeamView;

        private object _data;

        protected override void Awake()
        {
            base.Awake();
            _txtPVEName = GetChildComponent<StateText>("Container_biaoti/Label_txtzuduimijing");
            _teamBgImg = GetChildComponent<StateImage>("Image_zuduidi");
            _copyImageContainer = AddChildComponent<IconContainer>("Container_mijing");
            _inviteButton = GetChildComponent<KButton>("Button_yaoqing");
            _copyTeamView = AddChildComponent<CopyTeamView>("Container_duiyou");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        public void Refresh(PanelIdEnum context, object data)
        {
            _instanceId = (data as int[])[0];
            _txtPVEName.CurrentText.text = instance_helper.GetInstanceName(_instanceId);
            _data = data;
            _copyTeamView.RefreshContent(_instanceId);
            _copyImageContainer.SetIcon(instance_helper.GetIcon(_instanceId));
            ShowOrHideTeamInfo();
        }

        private void AddEventListener()
        {
            _inviteButton.onClick.AddListener(OnInviteButtonClick);
        }

        private void RemoveEventListener()
        {
            _inviteButton.onClick.RemoveListener(OnInviteButtonClick);
        }

        private void OnInviteButtonClick()
        {
            if (_data is int[])
            {
                int[] list = _data as int[];
                if (list.Length >= 2)
                {
                    int activityId = (_data as int[])[1];
                    if (activity_helper.IsOpen(activityId) == false)
                    {
                        ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.ACTIVITY_NOT_OPEN));
                        return;
                    }
                    LimitActivityData activityData = activity_helper.GetActivity(activityId);
                    instance ins = instance_helper.GetInstanceCfg(activityData.openData.__fid);
                    if (instance_helper.GetChapterType(ins.__id) == ChapterType.GuildCopy)
                    {
                        view_helper.OpenView(112, new InteractivePanelParameter(3, _instanceId));
                        return;
                    }
                }
            }
            view_helper.OpenView(112, new InteractivePanelParameter(_instanceId));
        }

        private void ShowOrHideTeamInfo()
        {
            int maxPlayerNum = instance_helper.GetMaxPlayerNum(_instanceId);
            if (maxPlayerNum <= 1)
            {
                _teamBgImg.Visible = false;
                _inviteButton.Visible = false;
                _copyTeamView.Visible = false;
            }
            else
            {
                _teamBgImg.Visible = true;
                _inviteButton.Visible = true;
                _copyTeamView.Visible = true;
            }
        
        }
    }
}
