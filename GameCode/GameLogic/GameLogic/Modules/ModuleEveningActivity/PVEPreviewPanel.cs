﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEveningActivity
{
    public class PVEPreviewPanel:BasePanel
    {
        private PVEPreviewLeftView _leftView;
        private PVEPreviewRightView _rightView;
        private PVEPreviewRuleView _ruleView;

        protected override void Awake()
        {
            base.Awake();
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _leftView = AddChildComponent<PVEPreviewLeftView>("Container_left");
            _rightView = AddChildComponent<PVEPreviewRightView>("Container_right");
            _ruleView = AddChildComponent<PVEPreviewRuleView>("Container_tanchuang");
        }

        protected override PanelIdEnum Id
        {
            get 
            {
                return PanelIdEnum.PVEPreview;
            }
        }

        public override void OnShow(object data)
        {
            SetData(data);
            AddEventListener();
            TweenViewMove.Begin(_leftView.gameObject, MoveType.Show, MoveDirection.Left);
            TweenViewMove.Begin(_rightView.gameObject, MoveType.Show, MoveDirection.Right);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            if (data is int[])
            {
                int[] list = data as int[];
                if (list.Length == 2)
                {
                    _leftView.Refresh(PanelIdEnum.PVEPreview, data);
                    _rightView.RefreshRightView(list[1]);

                }
            }
            
        }

        private void AddEventListener()
        {
            _rightView.onRuleButtonClick.AddListener(OnShowRuleView);
        }

        private void RemoveEventListener()
        {
            _rightView.onRuleButtonClick.RemoveListener(OnShowRuleView);
        }

        private void OnShowRuleView()
        {
            //_ruleView.Visible = true;
        }
    }
}
