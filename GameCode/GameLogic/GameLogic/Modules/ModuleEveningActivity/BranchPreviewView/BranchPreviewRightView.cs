﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEveningActivity
{
    public class BranchPreviewRightView:KContainer
    {
        private KButton _ruleButton;
        private BranchPreviewButtonView _buttonView;
        private BranchPreviewWordView _wordView;

        protected override void Awake()
        {
            base.Awake();
            _ruleButton = GetChildComponent<KButton>("Container_upperBtn/Button_fubenjilv");

            _wordView = AddChildComponent<BranchPreviewWordView>("Container_word");
            _buttonView = AddChildComponent<BranchPreviewButtonView>("Container_bottomBtn");

            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        public void RefreshRightView(int instanceId)
        {
            _wordView.RefreshIntroduction(instanceId);
            _buttonView.Refresh(instanceId);
        }

        private void AddEventListener()
        {
            _ruleButton.onClick.AddListener(OnRuleButtonClick);
        }

        private void RemoveEventListener()
        {
            _ruleButton.onClick.RemoveListener(OnRuleButtonClick);
        }

        private void OnRuleButtonClick()
        {

        }
    }
}
