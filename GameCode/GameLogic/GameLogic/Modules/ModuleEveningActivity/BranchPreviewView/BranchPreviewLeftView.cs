﻿
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEveningActivity
{
    public class BranchPreviewLeftView:KContainer
    {
        private int _instanceId;
        private StateText _branchName;
        private KButton _inviteButton;
        private IconContainer _copyImageContainer;

        private CopyTeamView _copyTeamView;

        protected override void Awake()
        {
            base.Awake();
            _branchName = GetChildComponent<StateText>("Container_biaoti/Label_txtzuduimijing");
            _copyImageContainer = AddChildComponent<IconContainer>("Container_mijing");
            _inviteButton = GetChildComponent<KButton>("Button_yaoqing");
            _copyTeamView = AddChildComponent<CopyTeamView>("Container_duiyou");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        public void Refresh(int instanceId)
        {
            _instanceId = instanceId;
            _branchName.CurrentText.text = instance_helper.GetInstanceName(_instanceId);
            _copyTeamView.RefreshContent(_instanceId);
            _copyImageContainer.SetIcon(instance_helper.GetIcon(_instanceId));
        }

        private void AddEventListener()
        {
            _inviteButton.onClick.AddListener(OnInviteButtonClick);
        }

        private void RemoveEventListener()
        {
            _inviteButton.onClick.RemoveListener(OnInviteButtonClick);
        }

        private void OnInviteButtonClick()
        {
            view_helper.OpenView(111, new InteractivePanelParameter(_instanceId));
        }
    }
}
