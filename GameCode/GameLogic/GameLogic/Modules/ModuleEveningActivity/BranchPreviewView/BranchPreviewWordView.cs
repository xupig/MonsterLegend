﻿using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEveningActivity
{
    public class BranchPreviewWordView:KContainer
    {
        private KContainer _introduction;
        private KScrollView _itemScrollView;
        private KList _itemList;

        private int _instanceId;

        protected override void Awake()
        {
            base.Awake();
            _introduction = GetChildComponent<KContainer>("Container_tiaozhanyaoqiu/Container_command/Container_tiaozhan");
            _itemScrollView = GetChildComponent<KScrollView>("Container_zhuangbei/ScrollView_wupinliebiao");

            InitScrollView();
        }

        public void RefreshIntroduction(int instanceId)
        {
            _instanceId = instanceId;
            RefreshLevelLimit();
            RefreshAvatarNumLimit();
            RefreshRecommendFightForce();
            RefreshGetView();
        }

        private void InitScrollView()
        {
            _itemList = _itemScrollView.GetChildComponent<KList>("mask/content");
            _itemList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue);
            _itemScrollView.ScrollRect.horizontal = true;
            _itemScrollView.ScrollRect.vertical = false;
            _itemList.SetGap(0, 28);
        }

        private void RefreshLevelLimit()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);

            int minLevel = instance_helper.GetMinLevel(ins);

            GameObject child = _introduction.GetChild(0);
            child.SetActive(true);
            StateText stateText = child.transform.GetChild(1).GetComponent<StateText>();
            stateText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(97004), " : ", MogoLanguageUtil.GetContent(97014, minLevel));
        }

        private void RefreshAvatarNumLimit()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);

            int minPlayerNum = instance_helper.GetMinPlayerNum(ins);
            int maxPlayerNum = instance_helper.GetMaxPlayerNum(ins);

            GameObject child = _introduction.GetChild(1);
            child.SetActive(true);
            StateText stateText = child.transform.GetChild(1).GetComponent<StateText>();

            stateText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(97005), " : ", MogoLanguageUtil.GetContent(83018, minPlayerNum, maxPlayerNum));
        }

        private void RefreshRecommendFightForce()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int recommendFightForce = instance_helper.GetRecommendFightForce(ins);

            GameObject child = _introduction.GetChild(2);
            child.SetActive(true);
            StateText stateText = child.transform.GetChild(1).GetComponent<StateText>();

            stateText.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(1421, recommendFightForce));
        }

        private void RefreshGetView()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            List<RewardData> rewardDataList = night_activity_reward_helper.GetRewardList(ins.__id, PlayerAvatar.Player.level, PlayerAvatar.Player.vocation);

            _itemList.RemoveAll();
            rewardDataList = RewardData.FilterSameItemId(rewardDataList);
            _itemList.SetDataList<RewardGrid>(rewardDataList);
        }
    }
}
