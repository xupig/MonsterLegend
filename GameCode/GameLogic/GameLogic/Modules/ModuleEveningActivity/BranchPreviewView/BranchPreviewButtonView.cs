﻿
using Common.Base;
using Common.Data;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEveningActivity
{
    public class BranchPreviewButtonView:KContainer
    {
        private KButton _callButton;
        private KButton _entryButton;
        private KButton _matchButton;
        private KButton _cancelButton;

        private StateText _matchText;
        private StateText _costText;

        private int _instanceId;

        protected override void Awake()
        {
            base.Awake();
            _callButton = GetChildComponent<KButton>("Container_saodang/Button_saodang");
            _entryButton = GetChildComponent<KButton>("Container_saodang/Button_tiaozhanSaodang");
            _matchButton = GetChildComponent<KButton>("Container_saodang/Button_saodangNci");
            _cancelButton = GetChildComponent<KButton>("Container_saodang/Button_quxiao");

            _matchText = GetChildComponent<StateText>("Label_txtPipei");
            _costText = GetChildComponent<StateText>("Label_txtXuyaotili");

            InitButtonView();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        public void Refresh(int instanceId)
        {
            _instanceId = instanceId;
            _matchText.Visible = false;
            _costText.Visible = false;
            _matchButton.Visible = false;
            _cancelButton.Visible = false;
        }

        private void AddEventListener()
        {
            _callButton.onClick.AddListener(OnCallButtonClick);
            _entryButton.onClick.AddListener(OnEnterButtonClick);
            EventDispatcher.AddEventListener(TeamInstanceEvents.On_Call_People_Succeed, OnCallPeopleSucceed);
        }

        private void RemoveEventListener()
        {
            _callButton .onClick.RemoveListener(OnCallButtonClick);
            _entryButton.onClick.RemoveListener(OnEnterButtonClick);
            EventDispatcher.RemoveEventListener(TeamInstanceEvents.On_Call_People_Succeed, OnCallPeopleSucceed);
        }

        private void OnCallButtonClick()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int maxNum = instance_helper.GetMaxPlayerNum(ins);

            if (PlayerDataManager.Instance.TeamData.TeammateDic.Count >= maxNum)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83033), PanelIdEnum.BranchPreview);
                return;
            }
            if (CheckMeetCondition())
            {
                TeamInstanceManager.Instance.CallPeople(team_helper.GetGameIdByInstanceId(_instanceId));
            }
        }

        private void OnEnterButtonClick()
        {
            if (CheckMeetCondition())
            {
                CopyLogicManager.Instance.RequestEnterCopy(ChapterType.Branch, EnterCopy, _instanceId);
            }
        }

        private void EnterCopy()
        {
            MissionManager.Instance.RequsetEnterMission(_instanceId);
        }

        private void OnCallPeopleSucceed()
        {
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83026), PanelIdEnum.BranchPreview);
        }

        private void InitButtonView()
        {
            StateText txt = _matchButton.GetChildComponent<StateText>("label");
            txt.ChangeAllStateText(MogoLanguageUtil.GetContent(83005));
            txt = _callButton.GetChildComponent<StateText>("label");
            txt.ChangeAllStateText(MogoLanguageUtil.GetContent(83006));
            txt = _entryButton.GetChildComponent<StateText>("label");
            txt.ChangeAllStateText(MogoLanguageUtil.GetContent(83007));
            txt = _cancelButton.GetChildComponent<StateText>("label");
            txt.ChangeAllStateText(MogoLanguageUtil.GetContent(83022));
        }

        private bool CheckMeetCondition()
        {
            instance ins = instance_helper.GetInstanceCfg(_instanceId);
            int minLevel = instance_helper.GetMinLevel(ins);
            if (PlayerAvatar.Player.level < minLevel)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83027, minLevel), PanelIdEnum.MainUIField);
                return false;
            }
            return true;
        }
    }
}
