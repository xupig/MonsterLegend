﻿using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEveningActivity
{
    public class AvatarResultItem : KList.KListItemBase
    {
        private KContainer _rankContainer;
        private KContainer _rewardIcon;
        private StateText _txtavatarName;
        private StateText _txtLevel;
        private StateText _txtFightForce;
        private StateText[] _txtConditionList;
        private StateText[] _txtRewardList;
        private IconContainer[] _iconContainer;
        private KButton _addFriendButton;

        private Vector2 _startPosition;
        private Vector2 _endPosition;

        private MultiCopyResultData _data;

        protected override void Awake()
        {
            _txtConditionList = new StateText[4];
            _txtRewardList = new StateText[3];
            _iconContainer = new IconContainer[3];
            _rankContainer = GetChildComponent<KContainer>("Container_mingci");
            _rewardIcon = GetChildComponent<KContainer>("Container_icon");
            _txtavatarName = GetChildComponent<StateText>("Label_txtwanjiamingcheng");
            _txtLevel = GetChildComponent<StateText>("Label_txtdengji");
            _txtFightForce = GetChildComponent<StateText>("Label_txtzhanli");
            for (int i = 0; i < 4; i++)
            {
                _txtConditionList[i] = GetChildComponent<StateText>(string.Format("Label_txtTiaojian{0}" ,i + 1));
            }
            for (int i = 0; i < 3; i++)
            {
                _txtRewardList[i] = GetChildComponent<StateText>(string.Format("Label_txtShuliang{0}", i + 1));
                _iconContainer[i] = AddChildComponent<IconContainer>(string.Format("Container_icon{0}", i+1));
            }

            _startPosition = _txtConditionList[0].GetComponent<RectTransform>().anchoredPosition;
            _endPosition = _txtRewardList[0].GetComponent<RectTransform>().anchoredPosition;

            _addFriendButton = GetChildComponent<KButton>("Button_tianijiahaoyou");
            _addFriendButton.onClick.AddListener(RequestAddFriend);
        }

        public override void Dispose()
        {
            _data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as MultiCopyResultData;
                    Refresh();
                }
            }
        }

        private void RefreshConditionInfo()
        {
            ResetConditionInfo();
            int conditionCount = 0;
            List<PbActionInfo> actionList = _data.GetActionInfo();
            Dictionary<int, int> actionId2ConditionId = _data.Logic.GetActionId2ConditionIdDic();
            conditionCount = actionList.Count;
            for (int i = 0; i < actionList.Count; i++)
            {
                if (actionId2ConditionId.ContainsKey((int)actionList[i].action_id) == false)
                {
                    continue;
                }
                int conditionIndex = actionId2ConditionId[(int)actionList[i].action_id];
                _txtConditionList[conditionIndex].Visible = true;
                _txtConditionList[conditionIndex].CurrentText.text = actionList[i].val.ToString();
            }
            int maxRewardCount = _data.Logic.GetMaxNotSpecialRewardCount();

            RefreshConditionNameLayout(conditionCount, maxRewardCount);
        }

        private void RefreshConditionNameLayout(int conditionNum, int maxRewardCount)
        {
            Vector2 gap = (_endPosition - _startPosition) / (conditionNum + maxRewardCount - 1);

            for (int i = 1; i < conditionNum; i++)
            {
                _txtConditionList[i].GetComponent<RectTransform>().anchoredPosition = _startPosition + gap * i;
            }

            for (int i = 0; i < maxRewardCount; i++)
            {
                _txtRewardList[i].GetComponent<RectTransform>().anchoredPosition = _startPosition + (conditionNum + i) * gap;
                _iconContainer[i].GetComponent<RectTransform>().anchoredPosition = _startPosition + (conditionNum + i) * gap - new Vector2(25, -9);
            }

        }

        private void ResetConditionInfo()
        {
            for (int i = 0; i < 4; i++)
            {
                _txtConditionList[i].Visible = false;
                _txtConditionList[i].CurrentText.text = "0";
            }
        }

        private void Refresh()
        {
            RefreshRankNum();
            RefreshAvatarInfo();
            RefreshRewardInfo();
            RefreshAddFriendButton();

            RefreshConditionInfo();
        }

        private void RefreshRankNum()
        {
            ResetRankNum();
            //Debug.LogError("rank" + _data.RankNum);
            if (_data.RankNum < 4)
            {
                _rankContainer.transform.GetChild((int)(4 - _data.RankNum)).gameObject.SetActive(true);
            }
            else
            {
                _rankContainer.transform.GetChild(0).gameObject.SetActive(true);
                _rankContainer.transform.GetChild(4).gameObject.SetActive(true);
                _rankContainer.transform.GetChild(4).gameObject.GetComponent<StateText>().CurrentText.text = _data.RankNum.ToString();
            }
        }

        private void ResetRankNum()
        {
            int childCount = _rankContainer.transform.childCount;
            for (int i = 0; i < childCount; i++)
            {
                _rankContainer.transform.GetChild(i).gameObject.SetActive(false);
            }
        }

        private void RefreshAvatarInfo()
        {
            _txtavatarName.CurrentText.text = _data.AvatarName;
            _txtLevel.CurrentText.text = _data.AvatarLevel.ToString();
            _txtFightForce.CurrentText.text = _data.FightForce.ToString();
        }

        private void RefreshRewardInfo()
        {
            List<RewardData> rewardDataList = _data.GetTotalNormalRewardList();
            for (int i = 0; i < rewardDataList.Count; i++)
            {
                _iconContainer[i].Visible = true;
                _iconContainer[i].SetIcon(item_helper.GetIcon(rewardDataList[i].id));
                _txtRewardList[i].Visible = true;
                _txtRewardList[i].CurrentText.text = string.Format("x{0}", rewardDataList[i].num);
            }
            for (int i = rewardDataList.Count; i < 3; i++)
            {
                _iconContainer[i].Visible = false;
                _txtRewardList[i].Visible = false;
            }

        }

        private void RefreshAddFriendButton()
        {
            if (_data.AvatarName == PlayerAvatar.Player.name || _data is TransPortalResultPersonData)
            {
                _addFriendButton.Visible = false;
            }
            else
            {
                _addFriendButton.Visible = true;
            }
        }

        private void RequestAddFriend()
        {
            FriendManager.Instance.AddFriend((_data as EveningActivityResultPersonData).dbid);
        }
    }
}
