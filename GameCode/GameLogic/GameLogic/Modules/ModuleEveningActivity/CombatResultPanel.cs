﻿using Common.Base;
using Common.Data;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEveningActivity
{
    public class CombatResultPanel : BasePanel
    {
        private CombatResultView _combatResultView;
        private PVEResultView _pveResultView;
        private PVPResultView _pvpResultView;

        private KDummyButton _button;
        private CopyMultiPlayerLogic _logic;

        protected override void Awake()
        {
            base.Awake();
            _button = this.gameObject.AddComponent<KDummyButton>();
            _combatResultView = AddChildComponent<CombatResultView>("Container_fubenjiesuan");
            _pveResultView = AddChildComponent<PVEResultView>("Container_PVEzhankuang");
            _pvpResultView = AddChildComponent<PVPResultView>("Container_PVPzhankuang");

        }
        
        public override void OnShow(object data)
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.RelivePop);
            UIManager.Instance.ClosePanel(PanelIdEnum.PortalFight);
            AddEventListener();
            _logic = data as CopyMultiPlayerLogic;
            _combatResultView.ShowResultInfo(_logic);
        }

        public override void OnClose()
        {
            _combatResultView.Hide();
            _pveResultView.Hide();
            _pvpResultView.Hide();
            RemoveEventListener();
        }

        protected override PanelIdEnum Id
        {
            get 
            {
                return PanelIdEnum.PVEResult; 
            }
        }

        private void AddEventListener()
        {
            _button.onClick.AddListener(NextUIShow);
        }

        private void RemoveEventListener()
        {
            _button.onClick.RemoveListener(NextUIShow);
        }

        private void NextUIShow()
        {
            if (_logic == null) return;
            if (_combatResultView.IsPlayingTween == true) return;
            if (_combatResultView.Visible == true)
            {
                _combatResultView.Hide();
                int mapId = GameSceneManager.GetInstance().curMapID;
                if (_logic is CopyCombatPortalLogic)
                {
                    _pveResultView.RefreshResultView(_logic);
                }
                else if (map_helper.GetMap(mapId).__group == 2)
                {
                    _pvpResultView.RefreshResultView(_logic);
                }
                else
                {
                    _pveResultView.RefreshResultView(_logic);
                }
            }
        }
    }
}
