﻿using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    public class CampIntegralView : KContainer
    {
        private KProgressBar _ourProgressBar;
        private KProgressBar _enemyProgressBar;

        private StateText _txtLeftTeamScore;
        private StateText _txtRightTeamScore;

        private StateImage _enemyYellowLabel;
        private StateImage _enemyPurpleLabel;

        private StateImage _ourselfYellowLabel;
        private StateImage _ourselfPurpleLabel;

        private SystemNoticeTopData _initNotice = null; //用来表示客户端已经初始化
        private Dictionary<int, int> _campId2ScoreDic; //用来记录各个阵营的积分

        protected override void Awake()
        {
            base.Awake();
            _campId2ScoreDic = new Dictionary<int, int>();
            _ourProgressBar = GetChildComponent<KProgressBar>("Container_left/ProgressBar_wofang");
            _enemyProgressBar = GetChildComponent<KProgressBar>("Container_right/ProgressBar_difang");

            _txtLeftTeamScore = GetChildComponent<StateText>("Container_left/Label_txtjindushuzhi");
            _txtRightTeamScore = GetChildComponent<StateText>("Container_right/Label_txtjindushuzhi");

            _enemyYellowLabel = GetChildComponent<StateImage>("Container_right/Image_difang1");
            _ourselfYellowLabel = GetChildComponent<StateImage>("Container_right/Image_wofang1");

            _enemyPurpleLabel = GetChildComponent<StateImage>("Container_left/Image_difang");
            _ourselfPurpleLabel = GetChildComponent<StateImage>("Container_left/Image_wofang");

            AddEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

        private void AddEventListener()
        {
            //EventDispatcher.AddEventListener<uint>(EveningActivityEvents.Avatar_Enter_Instance, TeamMemberInitFinish);
        }

        private void RemoveEventListener()
        {
            //EventDispatcher.RemoveEventListener<uint>(EveningActivityEvents.Avatar_Enter_Instance, TeamMemberInitFinish);
        }

        public void Show(SystemNoticeTopData notice)
        {
            this.Visible = true;
            float progressValue = (float)notice.timerCount / notice.maxCount;
            if (notice.type == InstanceDefine.COUNTER_NOTICE_ACTION_INIT)
            {
                InitScoreInfo(notice);

            }
            else if (notice.rightUpTargetId == 1)
            {
                _campId2ScoreDic[notice.rightUpTargetId] = notice.timerCount;
                _enemyProgressBar.Value = progressValue;
                _txtRightTeamScore.Visible = true;
                _txtRightTeamScore.CurrentText.text = string.Format("{0}/{1}", notice.timerCount, notice.maxCount);
            }
            else
            {
                _campId2ScoreDic[notice.rightUpTargetId] = notice.timerCount;
                _ourProgressBar.Value = progressValue;
                _txtLeftTeamScore.Visible = true;
                _txtLeftTeamScore.CurrentText.text = string.Format("{0}/{1}", notice.timerCount, notice.maxCount);
            }
        }

        public void InitProgressContainer()
        {
            this.Visible = true;
        }

        public void Hide()
        {
            this.Visible = false;
            _initNotice = null;
            _campId2ScoreDic.Clear();
        }

        private void InitCampScore()
        {
            if (_initNotice != null)
            {
                float progressValue = (float)_initNotice.timerCount / _initNotice.maxCount;

                _ourProgressBar.Value = progressValue;
                if (_campId2ScoreDic.ContainsKey(2) == false) //如果服务器已经发送阵营2的积分信息则不去初始化
                {
                    _txtLeftTeamScore.Visible = true;
                    _txtLeftTeamScore.CurrentText.text = string.Format("{0}/{1}", _initNotice.timerCount, _initNotice.maxCount);
                }
                _enemyProgressBar.Value = progressValue;
                if (_campId2ScoreDic.ContainsKey(1) == false) //如果服务器已经发送阵营1的积分信息则不去初始化
                {
                    _txtRightTeamScore.Visible = true;
                    _txtRightTeamScore.CurrentText.text = string.Format("{0}/{1}", _initNotice.timerCount, _initNotice.maxCount);
                }

                InitScoreDisplayStyle();
            }
        }

        private void InitScoreDisplayStyle()
        {
            ///阵营2显示在左边，阵营1显示在右边
            if ((int)PlayerDataManager.Instance.InstanceAvatarData.OurCampId == 1)
            {
                _ourselfPurpleLabel.Visible = false;
                _enemyPurpleLabel.Visible = true;
                _ourselfYellowLabel.Visible = true;
                _enemyYellowLabel.Visible = false;
            }
            else if ((int)PlayerDataManager.Instance.InstanceAvatarData.OurCampId == 2)
            {
                _ourselfPurpleLabel.Visible = true;
                _enemyPurpleLabel.Visible = false;
                _ourselfYellowLabel.Visible = false;
                _enemyYellowLabel.Visible = true;
            }
        }

        private void InitScoreInfo(SystemNoticeTopData notice)
        {
            _initNotice = notice;
            InitCampScore();
        }
    }
}
