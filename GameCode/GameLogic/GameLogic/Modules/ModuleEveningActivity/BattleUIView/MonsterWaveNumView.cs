﻿using Common.ClientConfig;
using Common.Data;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMainUI
{
    public class MonsterWaveNumView : KContainer
    {
        private StateText _txtWaveNum;
        private KProgressBar _progressBar;

        private string _waveNumContent;
        private int _waveNum = 0;
        private int _supplyNum = 0;

        protected override void Awake()
        {
            base.Awake();
            _txtWaveNum = GetChildComponent<StateText>("Label_txtXinxi");
            _progressBar = GetChildComponent<KProgressBar>("ProgressBar_jindu");

            _waveNumContent = _txtWaveNum.CurrentText.text;
        }

        public void Show(SystemNoticeTopData notice)
        {
            this.Visible = true;
            if (notice.notice_type == InstanceDefine.NOTICE_MONSTER_WAVE)
            {
                _waveNum = notice.timerCount;
                _txtWaveNum.CurrentText.text = string.Format(_waveNumContent, _waveNum, _supplyNum);
            }
            else if (notice.notice_type == InstanceDefine.NOTICE_SUPPLY)
            {
                _supplyNum = notice.timerCount;
                _txtWaveNum.CurrentText.text = string.Format(_waveNumContent, _waveNum, _supplyNum);
                _progressBar.Value = (float)_supplyNum / notice.maxCount;
            }
        }

        public void Hide()
        {
            this.Visible = false;
        }
    }
}
