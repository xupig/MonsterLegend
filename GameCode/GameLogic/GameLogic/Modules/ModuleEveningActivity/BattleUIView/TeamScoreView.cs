﻿using Common.Data;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMainUI
{
    public class TeamScoreView:KContainer
    {
        private string _teamScoreContent;

        private StateText _txtTeamScore;

        protected override void Awake()
        {
            base.Awake();
            _txtTeamScore = GetChildComponent<StateText>("Label_txtjifen");
            _teamScoreContent = _txtTeamScore.CurrentText.text;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }

        public void Show(SystemNoticeTopData notice)
        {
            this.Visible = true;
            _txtTeamScore.CurrentText.text = string.Format(_teamScoreContent, notice.timerCount);
        }

        public void Hide()
        {
            this.Visible = false;
        }
    }
}
