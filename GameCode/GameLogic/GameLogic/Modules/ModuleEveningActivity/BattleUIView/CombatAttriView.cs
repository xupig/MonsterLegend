﻿
using Common.Data;
using Common.Global;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.Entities;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    public class CombatAttriView:KContainer
    {
        private CombatAttriItem[] _txtAvatarCombatAttriList;
        private StateImage _imgBg;
        private KButton _combatAttriDetailBtn;
        private KContainer _combatAttriDetailContainer;

        private const float PADDING_Y = 28;
        private const float GAP_Y = 2;
        private float heightOfCombatAttriItem;

        protected override void Awake()
        {
            base.Awake();
            _txtAvatarCombatAttriList = new CombatAttriItem[8];
            for (int i = 0; i < 8; i++)
            {
                _txtAvatarCombatAttriList[i] = AddChildComponent<CombatAttriItem>("Container_tishixinxi/Container_wanjiajifen" + (i + 1));
            }
            heightOfCombatAttriItem = _txtAvatarCombatAttriList[0].GetComponent<RectTransform>().sizeDelta.y;

            _combatAttriDetailBtn = GetChildComponent<KButton>("Button_Gantang");
            _combatAttriDetailContainer = GetChildComponent<KContainer>("Container_tishixinxi");
            _imgBg = GetChildComponent<StateImage>("Container_tishixinxi/ScaleImage_zhankaixinxi");
            _imgBg.AddAllStateComponent<Resizer>();
        }

        public void Show(SystemCombatAttriData notice)
        {
            if (this.Visible == false)
            {
                this.Visible = true;
                _combatAttriDetailContainer.Visible = true;
            }
            notice.combat_attri.Sort(CombatAttriSort);
            RefreshAllCombatAttriInfo(notice);
            RefreshBgImgSize(notice.combat_attri.Count);

        }

        public void Hide()
        {
            this.Visible = false;
            for (int j = 0; j < 8; j++)
            {
                _txtAvatarCombatAttriList[j].Visible = false;
            }
        }

        private void AddEventListener()
        {
            _combatAttriDetailBtn.onClick.AddListener(ShowCombatDetail);

        }

        private void RemoveEventListener()
        {
            _combatAttriDetailBtn.onClick.AddListener(ShowCombatDetail);
        }

        private void RefreshAllCombatAttriInfo(SystemCombatAttriData notice)
        {
            for (int i = 0; i < notice.combat_attri.Count && i < 8; i++)
            {
                _txtAvatarCombatAttriList[i].Visible = true;
                _txtAvatarCombatAttriList[i].RefreshCombatAttriInfo(notice.combat_attri[i], i + 1);
            }
            for (int i = notice.combat_attri.Count; i < 8; i++)
            {
                _txtAvatarCombatAttriList[i].Visible = false;
            }
        }

        private void RefreshBgImgSize(int combatAttriNum)
        {
            _imgBg.Height = PADDING_Y * 2 + combatAttriNum * heightOfCombatAttriItem;
        }

        private int CombatAttriSort(PbCombatAttri first, PbCombatAttri second)
        {
            return (int)(second.attri_value - first.attri_value);
        }

        private void ShowCombatDetail()
        {
            _combatAttriDetailContainer.Visible = !_combatAttriDetailContainer.Visible;
        }
    }

    public class CombatAttriItem : KContainer
    {
        private StateText _txtRankNum;
        private StateText _txtAvatarName;
        private StateText _txtCombatAttriValue;

        private PbCombatAttri _combatAttri;

        protected override void Awake()
        {
            base.Awake();
            _txtRankNum = GetChildComponent<StateText>("Label_txtmingci");
            _txtAvatarName = GetChildComponent<StateText>("Label_txtWanjiamingzi");
            _txtCombatAttriValue = GetChildComponent<StateText>("Label_txtjifen");
        }

        public void RefreshCombatAttriInfo(PbCombatAttri combatAttri, int rankNum = 0)
        {
            _combatAttri = combatAttri;
            SetLabelColor();

            _txtAvatarName.CurrentText.text = MogoProtoUtils.ParseByteArrToString(combatAttri.name_bytes);
            _txtRankNum.CurrentText.text = string.Format("{0}、", rankNum);
            _txtCombatAttriValue.CurrentText.text = combatAttri.attri_value.ToString();
        }

        private void SetLabelColor()
        {
            if (_combatAttri.map_camp_id == PlayerAvatar.Player.map_camp_id)
            {
                _txtRankNum.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_NEW_DARK_GOLDEN);
                _txtAvatarName.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_NEW_DARK_GOLDEN);
                _txtCombatAttriValue.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_NEW_DARK_GOLDEN);
            }
            else
            {
                _txtRankNum.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_NEW_PURPLE);
                _txtAvatarName.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_NEW_PURPLE);
                _txtCombatAttriValue.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_NEW_PURPLE);
            }
        }
    }
}
