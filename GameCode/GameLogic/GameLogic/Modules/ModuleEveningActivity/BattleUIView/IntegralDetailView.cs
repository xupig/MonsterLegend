﻿using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.RPC;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    public class IntegralDetailView:KContainer
    {
        private IntegralItem[] _txtAvatarIntegralList;
        private StateImage _imgAvatarIntegral;
        private KButton _integralDetailBtn;
        private List<KeyValuePair<uint, int>> _avatarIntegralList; //KeyValuePair里的key为0时表示阵营积分
        private KContainer _integralDetailContainer;
        private SystemNoticeTopData _initNotice;

        private const uint CAMP_SCORE_KEY = 0;

        private const float PADDING_Y = 28;
        private const float GAP_Y = 9;
        private float heightOfIntegralItem;

        protected override void Awake()
        {
            base.Awake();
            _txtAvatarIntegralList = new IntegralItem[8];
            _avatarIntegralList = new List<KeyValuePair<uint,int>>();
            for (int i = 0; i < 8; i++)
            {
                _txtAvatarIntegralList[i] = AddChildComponent<IntegralItem>("Container_tishixinxi/Container_wanjiajifen" + (i + 1));
            }
            heightOfIntegralItem = _txtAvatarIntegralList[0].GetComponent<RectTransform>().sizeDelta.y;
            _integralDetailBtn = GetChildComponent<KButton>("Button_Gantang");
            _integralDetailContainer = GetChildComponent<KContainer>("Container_tishixinxi");
            _integralDetailContainer.transform.localPosition = new Vector3(_integralDetailContainer.transform.localPosition.x, _integralDetailContainer.transform.localPosition.y, -100);
            _imgAvatarIntegral = GetChildComponent<StateImage>("Container_tishixinxi/ScaleImage_zhankaixinxi");
            _imgAvatarIntegral.AddAllStateComponent<Resizer>();
            AddEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

        private void AddEventListener()
        {
            _integralDetailBtn.onClick.AddListener(OnIntegralDetail);
            EventDispatcher.AddEventListener<uint>(EveningActivityEvents.Avatar_Enter_Instance, InitAvatarScore);
            EventDispatcher.AddEventListener<uint>(EveningActivityEvents.Avatar_Leave_Instance, RemoveAvatarScore);
        }

        private void RemoveEventListener()
        {
            _integralDetailBtn.onClick.AddListener(OnIntegralDetail);
            EventDispatcher.RemoveEventListener<uint>(EveningActivityEvents.Avatar_Enter_Instance, InitAvatarScore);
            EventDispatcher.RemoveEventListener<uint>(EveningActivityEvents.Avatar_Leave_Instance, RemoveAvatarScore);
        }

        private void InitAvatarScore(uint avatarId)
        {
            if (_initNotice != null)
            {
                AddAvatarIntegral(avatarId, _initNotice.timerCount, false);
                RefreshIntegralInfo();
                RefreshBgImgSize();
            }
        }

        private void RemoveAvatarScore(uint avatarId)
        {
            int count = _avatarIntegralList.Count;
            for (int i = 0; i < count; i++)
            {
                if (_avatarIntegralList[i].Key == avatarId)
                {
                    _avatarIntegralList.RemoveAt(i);
                    break;
                }
            }
            RefreshIntegralInfo();
            RefreshBgImgSize();
        }

        private void OnIntegralDetail()
        {
            if (_integralDetailContainer.Visible == true)
            {
                _integralDetailContainer.Visible = false;
            }
            else
            {
                _integralDetailContainer.Visible = true;
            }
        }

        public void Show(SystemNoticeTopData notice)
        {
            if (this.Visible == false)
            {
                this.Visible = true;
                _integralDetailContainer.Visible = false;
            }
            if (notice.type == InstanceDefine.COUNTER_NOTICE_ACTION)
            {
                AddAvatarIntegral((uint)notice.rightUpTargetId, notice.timerCount);
            }
            else
            {
                _initNotice = notice;
                List<PbHpMember> ourMemberList = PlayerDataManager.Instance.InstanceAvatarData.HpOurMemberDic.Values.ToList();
                List<PbHpMember> enemyMemberList = PlayerDataManager.Instance.InstanceAvatarData.HpEnemyMemberDic.Values.ToList();
                for (int i = 0; i < ourMemberList.Count; i++)
                {
                    AddAvatarIntegral(ourMemberList[i].avatar_id, notice.timerCount, false);
                }
                for (int i = 0; i < enemyMemberList.Count; i++)
                {
                    AddAvatarIntegral(enemyMemberList[i].avatar_id, notice.timerCount, false);
                }
            }
            RefreshIntegralInfo();
            RefreshBgImgSize();
        }

        private void AddAvatarIntegral(uint avatarId, int avatarIntegral, bool replace = true)
        {
            int count = _avatarIntegralList.Count;
            for (int i = 0; i < count; i++)
            {
                if (_avatarIntegralList[i].Key == avatarId)
                {
                    if (replace == true)
                    {
                        _avatarIntegralList.RemoveAt(i);
                        break;
                    }
                    else
                    {
                        return;
                    }
                }
            }
            _avatarIntegralList.Add(new KeyValuePair<uint,int>(avatarId,avatarIntegral));
        }

        public void Hide()
        {
            this.Visible = false;
            _initNotice = null;
            _avatarIntegralList.Clear();
            for (int j = 0; j < 8; j++)
            {
                _txtAvatarIntegralList[j].Visible = false;
            }
        }

        private void RefreshIntegralInfo()
        {
            int index = 0;
            _avatarIntegralList.Sort(ListCompareSort);
            for (int i = index; i < _avatarIntegralList.Count && i < 8; i++)
            {
                _txtAvatarIntegralList[index].Visible = true;
                _txtAvatarIntegralList[index].RefreshIntegralInfo(_avatarIntegralList[i], i + 1);
                index++;
            }

            for (int j = _avatarIntegralList.Count; j < 8; j++)
            {
                _txtAvatarIntegralList[j].Visible = false;
            }
        }

        private void RefreshBgImgSize()
        {
            _imgAvatarIntegral.Height = PADDING_Y * 2 + _avatarIntegralList.Count * heightOfIntegralItem + (_avatarIntegralList.Count - 1) * GAP_Y;
        }

        private int ListCompareSort(KeyValuePair<uint, int> first, KeyValuePair<uint, int> second)
        {
            return second.Value - first.Value;
        }
    }

    public class IntegralItem : KContainer
    {
        private StateText _txtRankNum;
        private StateText _txtAvatarName;
        private StateText _txtIntegralNum;

        protected override void Awake()
        {
            base.Awake();
            _txtRankNum = GetChildComponent<StateText>("Label_txtmingci");
            _txtAvatarName = GetChildComponent<StateText>("Label_txtWanjiamingzi");
            _txtIntegralNum = GetChildComponent<StateText>("Label_txtjifen");
        }

        public void RefreshIntegralInfo(KeyValuePair<uint, int> avatarInfo, int rankNum = 0)
        {
            SetLabelColor(avatarInfo.Key);
            _txtRankNum.CurrentText.text = string.Format("{0}、", rankNum);

            Dictionary<UInt64, PbHpMember> ourMemberDic = PlayerDataManager.Instance.InstanceAvatarData.HpOurMemberDic;
            Dictionary<UInt64, PbHpMember> enemyMemberDic = PlayerDataManager.Instance.InstanceAvatarData.HpEnemyMemberDic;

            PbHpMember member = null;
            if (ourMemberDic.ContainsKey((ulong)avatarInfo.Key) == true)
            {
                member = ourMemberDic[(ulong)avatarInfo.Key];
            }
            else if(enemyMemberDic.ContainsKey((ulong)avatarInfo.Key) == true)
            {
                member = enemyMemberDic[(ulong)avatarInfo.Key];
            }

            if (member != null)
            {
                _txtAvatarName.CurrentText.text = MogoProtoUtils.ParseByteArrToString(member.name);
            }
            else
            {
                _txtAvatarName.CurrentText.text = string.Empty;
            }
            _txtIntegralNum.CurrentText.text = avatarInfo.Value.ToString();
        }

        private void SetLabelColor(uint avatarId)
        {
            uint campId = PlayerDataManager.Instance.InstanceAvatarData.GetCampId(avatarId);
            if (campId == 0)
            {
                return;
            }
            if (campId == 1)
            {
                _txtRankNum.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_NEW_DARK_GOLDEN);
                _txtAvatarName.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_NEW_DARK_GOLDEN);
                _txtIntegralNum.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_NEW_DARK_GOLDEN);
            }
            else
            {
                _txtRankNum.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_NEW_PURPLE);
                _txtAvatarName.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_NEW_PURPLE);
                _txtIntegralNum.CurrentText.color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_NEW_PURPLE);
            }
        }
    }
}
