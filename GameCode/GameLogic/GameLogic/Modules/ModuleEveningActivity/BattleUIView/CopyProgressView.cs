﻿using Common.Data;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMainUI
{
    public class CopyProgressView:KContainer
    {
        private string _progressContent;

        private StateText _txtProgressLabel;
        private KProgressBar _progress;

        protected override void Awake()
        {
            base.Awake();
            _txtProgressLabel = GetChildComponent<StateText>("Label_txtFubenjindu");
            _progressContent = _txtProgressLabel.CurrentText.text;
            _progress = GetChildComponent<KProgressBar>("ProgressBar_jindu");
        }

        public void Show(SystemNoticeTopData notice)
        {
            this.Visible = true;
            int percent = notice.timerCount * 100 / notice.maxCount;
            _txtProgressLabel.CurrentText.text = string.Format(_progressContent, percent);
            _progress.Percent = percent;
        }

        public void Hide()
        {
            this.Visible = false;
        }
    }
}
