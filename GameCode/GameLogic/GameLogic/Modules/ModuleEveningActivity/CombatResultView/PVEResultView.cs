﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEveningActivity
{
    public class PVEResultView : KContainer
    {
        private StateText _teamScore;
        private StateText _teamScoreLabel;
        private StateText[] _conditionNameList;
        private StateText[] _rewardNameList;
        private KButton _okButton;
        private KButton _statisticBtn;
        private KList _avatarResultList;
        private Vector2 _startPosition;
        private Vector2 _endPosition;
        private CopyMultiPlayerLogic _logic;

        protected override void Awake()
        {
            base.Awake();
            _conditionNameList = new StateText[4];
            _rewardNameList = new StateText[3];
            _teamScore = GetChildComponent<StateText>("Container_zhankuang/Label_txtDuiwuzongjifenNR");
            _teamScoreLabel = GetChildComponent<StateText>("Container_zhankuang/Label_txtDuiwuzongjifen");
            for (int i = 0; i < 4; i++)
            {
                _conditionNameList[i] = GetChildComponent<StateText>(string.Format("Container_biaoti/Label_txttiaojian0{0}", i + 1));
            }

            for (int i = 0; i < 3; i++)
            {
                _rewardNameList[i] = GetChildComponent<StateText>(string.Format("Container_biaoti/Label_txtjiangli{0}", i + 1));
            }

            _startPosition = _conditionNameList[0].GetComponent<RectTransform>().anchoredPosition;
            _endPosition = _rewardNameList[0].GetComponent<RectTransform>().anchoredPosition;
            _okButton = GetChildComponent<KButton>("Button_queding");
            _statisticBtn = GetChildComponent<KButton>("Button_zhandoutongji");
            _avatarResultList = GetChildComponent<KList>("ScrollView_haoyou/mask/content");
            _avatarResultList.SetDirection(KList.KListDirection.LeftToRight, 1);

            AddEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

        private void AddEventListener()
        {
            _okButton.onClick.AddListener(OnCloseView);
            _statisticBtn.onClick.AddListener(ShowStatisticPanel);
        }

        private void RemoveEventListener()
        {
            _okButton.onClick.RemoveListener(OnCloseView);
            _statisticBtn.onClick.RemoveListener(ShowStatisticPanel);
        }

        public void RefreshResultView(CopyMultiPlayerLogic logic)
        {
            this.Visible = true;
            _logic = logic;
            RefreshTotalScore();
            RefreshConditionName();
            RefreshAvatarResultInfo();
            RefreshStatisticButton();
        }

        public void Hide()
        {
            this.Visible = false;
        }

        private void RefreshTotalScore()
        {
            if (_logic is CopyCombatPortalLogic)
            {
                _teamScore.CurrentText.fontSize = 25;
            }
            else
            {
                _teamScore.CurrentText.fontSize = 34;
            }

            _teamScore.CurrentText.text = _logic.GetTeamScore();
            _teamScoreLabel.CurrentText.text = _logic.GetTeamLabel();
        }

        private void RefreshConditionName()
        {
            Dictionary<int, string> conditionNameDic = _logic.GetConditionchinese();
            List<int> actionIdList = conditionNameDic.Keys.ToList();
            for (int i = 0; i < actionIdList.Count; i++)
            {
                _conditionNameList[i].Visible = true;
                _conditionNameList[i].CurrentText.text = conditionNameDic[actionIdList[i]];
            }
            for (int i = actionIdList.Count; i < 4; i++)
            {
                _conditionNameList[i].Visible = false;
            }
            int maxRewardItemCount = _logic.GetMaxNotSpecialRewardCount();
            for (int i = 0; i < maxRewardItemCount; i++)
            {
                _rewardNameList[i].Visible = true;
            }
            for (int i = maxRewardItemCount; i < 3; i++)
            {
                _rewardNameList[i].Visible = false;
            }

            RefreshConditionNameLayout(actionIdList.Count, maxRewardItemCount);
        }

        private void RefreshConditionNameLayout(int conditionNum, int rewardItemCount)
        {

            Vector2 gap = (_endPosition - _startPosition) / (conditionNum + rewardItemCount - 1);
            for (int i = 1; i < conditionNum; i++)
            {
                _conditionNameList[i].GetComponent<RectTransform>().anchoredPosition = _startPosition + gap * i;
            }
            for (int i = 0; i < rewardItemCount; i++)
            {
                _rewardNameList[i].GetComponent<RectTransform>().anchoredPosition = _startPosition + (conditionNum + i) * gap;
            }
        }

        private void RefreshAvatarResultInfo()
        {
            _avatarResultList.RemoveAll();

            List<MultiCopyResultData> resultPersonDataList = _logic.GetResultPersonDataList();
            _avatarResultList.SetDataList<AvatarResultItem>(resultPersonDataList);

            TweenListEntry.Begin(_avatarResultList.gameObject);
        }

        private void ShowStatisticPanel()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.BattleDataStatistic);
        }

        private void OnCloseView()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.PVEResult);
            GameSceneManager.GetInstance().LeaveCombatScene();
        }

        private void RefreshStatisticButton()
        {
            int insId = GameSceneManager.GetInstance().curInstanceID;
            instance ins = instance_helper.GetInstanceCfg(insId);
            _statisticBtn.Visible = instance_helper.GetStatisticType(ins) != 0;
        }
    }
}
