﻿using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEveningActivity
{
    public class CombatResultView : KContainer
    {
        private const float TWEEN_SCALE = 5.0f;
        private string _rankString;
        private KContainer _loseIcon;
        private KContainer _winIcon;

        private KContainer _combatIntegralContaienr;
        private StateText _integral;
        private StateText _enemyIntegral;
        private StateText _selfIntegralLabel;
        private StateText _otherIntegralLabel;
        private KContainer _winOrFailReward;
        private KContainer _rankReward;
        private KContainer _integralReward;
        private StateText _txtRankNum;
        private StateText _txtContinue;

        private StateText _txtRankName;
        private StateText[] _rankRewardList;
        private IconContainer[] _rankRewardIconList;

        private GameObject _resultLeftIconObj;
        private GameObject _resultRightInfoBg;

        private CombatResultExpView _expView;

        private CopyMultiPlayerLogic _logic;

        private int _playingTweenNum = 0;
        private bool _isPlayingTween = false;

        protected override void Awake()
        {
            base.Awake();

            _rankRewardList = new StateText[3];
            _rankRewardIconList = new IconContainer[3];

            _winIcon = GetChildComponent<KContainer>("Container_left/Container_shengli");
            RectTransform rect = _winIcon.GetComponent<RectTransform>();
            rect.pivot = new Vector2(0.5f, 0.5f);
            Vector2 sizeDelta = rect.sizeDelta;
            rect.localPosition = new Vector3(rect.localPosition.x + sizeDelta.x / 2f, rect.localPosition.y - sizeDelta.y / 2f, 0);
            _winIcon.Visible = false;


            _loseIcon = GetChildComponent<KContainer>("Container_left/Container_shibai");
            rect = _loseIcon.GetComponent<RectTransform>();
            rect.pivot = new Vector2(0.5f, 0.5f);
            sizeDelta = rect.sizeDelta;
            rect.localPosition = new Vector3(rect.localPosition.x + sizeDelta.x / 2f, rect.localPosition.y - sizeDelta.y / 2f, 0);
            _loseIcon.Visible = false;


            _combatIntegralContaienr = GetChildComponent<KContainer>("Container_left/Container_zhandouxinxi");

            _integral = GetChildComponent<StateText>("Container_left/Container_zhandouxinxi/Label_txtWofangNR");
            _enemyIntegral = GetChildComponent<StateText>("Container_left/Container_zhandouxinxi/Label_txtDifangNR");
            _selfIntegralLabel = GetChildComponent<StateText>("Container_left/Container_zhandouxinxi/Label_txtWofang");
            _otherIntegralLabel = GetChildComponent<StateText>("Container_left/Container_zhandouxinxi/Label_txtDifang");

            _txtContinue = GetChildComponent<StateText>("Container_bottom/Label_txtDianjipingmujixu");
            _winOrFailReward = GetChildComponent<KContainer>("Container_right/Container_PVEshengliUP/Container_huode/Container_item1");
            _rankReward = GetChildComponent<KContainer>("Container_right/Container_PVEshengliUP/Container_huode/Container_item2");
            _txtRankNum = _rankReward.GetChildComponent<StateText>("Label_txtShenglmingci");
            _rankString = _txtRankNum.GetStateComponent(0).text;
            _txtRankName = _rankReward.GetChildComponent<StateText>("Label_txtShengli");
            for (int i = 0; i < 3; i++)
            {
                _rankRewardIconList[i] = _rankReward.AddChildComponent<IconContainer>(string.Format("Container_icon{0}", i+1));
                _rankRewardList[i] = _rankReward.GetChildComponent<StateText>(string.Format("Label_txtShengliNr{0}", i+1));
            }

            _integralReward = GetChildComponent<KContainer>("Container_right/Container_PVEshengliUP/Container_zonggonghuode");
            _expView = AddChildComponent<CombatResultExpView>("Container_right/Container_PVEshengliDOWN");
            _resultLeftIconObj = GetChild("Container_left/Container_jieguo");
            _resultRightInfoBg = GetChild("Container_right/Container_bg");

            LoadResultIcon();
        }

        public bool IsPlayingTween
        {
            get
            {
                return _isPlayingTween;
            }
        }

        public void ShowResultInfo(CopyMultiPlayerLogic logic)
        {
            this.Visible = true;
            _logic = logic;
            AddEventListener();
            ResetAllContainer(); //隐藏所有组件

            TweenFirstStep();
        }

        public void Hide()
        {
            RemoveEventListener();
            this.Visible = false;
        }

        private void AddEventListener()
        {
            _expView.onTweenFinish.AddListener(OnEndTween);
        }

        private void RemoveEventListener()
        {
            _expView.onTweenFinish.RemoveListener(OnEndTween);
        }

        private void LoadResultIcon(Action<GameObject> callback = null)
        {
            MogoAtlasUtils.AddSprite(GetChild("Container_left/Container_jieguo"), "shengliIcon", callback);
        }

        private void ShowRewardInfo()
        {
            MultiCopyResultData selfResultData =  _logic.GetCopyResultPersonData(PlayerAvatar.Player.id);
            ShowRewardResult(_winOrFailReward, selfResultData.GetWinFailReward(), selfResultData.GetWinFailRewardChineseDesc());
            ShowRankReward(selfResultData.GetRankNumReward(), selfResultData.GetRankNumRewardChineseDesc());
            ShowRewardResult(_integralReward, selfResultData.GetTotalReward(), selfResultData.GetTotalRewardChineseDesc());

            _txtRankNum.CurrentText.text = string.Format(_rankString, selfResultData.RankNum);

        }

        private void ShowRankReward(List<RewardData> rewardDataList, string desc)
        {
            if (rewardDataList.Count > 0)
            {
                int index = 0;
                _rankReward.Visible = true;
                _txtRankName.CurrentText.text = desc;
                for (int i = 0; i < rewardDataList.Count; i++)
                {
                    _rankRewardList[index].Visible = true;
                    _rankRewardList[index].CurrentText.text = string.Format("x{0}", rewardDataList[i].num);
                    _rankRewardIconList[index].Visible = true;
                    _rankRewardIconList[index].SetIcon(item_helper.GetIcon(rewardDataList[i].id));
                    index++;
                }
                for (int i = index; i < 3; i++)
                {
                    _rankRewardList[i].Visible = false;
                    _rankRewardIconList[i].Visible = false;
                }
            }
            else
            {
                _rankReward.Visible = false;
            }
        }

        private void ShowCombatResult()
        {
            if (_logic.IsWin() == true)
            {
                UIManager.Instance.PlayAudio("Sound/UI/fight_victory.mp3");
                _loseIcon.Visible = false;
                _winIcon.Visible = true;
            }
            else
            {
                UIManager.Instance.PlayAudio("Sound/UI/fight_fail.mp3");
                _loseIcon.Visible = true;
                _winIcon.Visible = false;
            }

        }

        private void ShowCombatIntegral()
        {

            if (_logic.IsShowCombatIntegral() == true)
            {
                _combatIntegralContaienr.Visible = true;
                _integral.CurrentText.text = _logic.GetLeftScoreValue().ToString();
                _enemyIntegral.CurrentText.text = _logic.GetRightScoreValue().ToString();
                _selfIntegralLabel.CurrentText.text = _logic.GetLeftScoreName();
                _otherIntegralLabel.CurrentText.text = _logic.GetRightScoreName();
            }
            else
            {
                _combatIntegralContaienr.Visible = false;
            }
        }

        private void ShowRewardResult(KContainer container, List<RewardData> pbItemList, string rewardName)
        {
            KContainer iconContainer = container.GetChildComponent<KContainer>("Container_icon");
            StateText txtRewardName = container.GetChildComponent<StateText>("Label_txtShengli");
            StateText txtRewardNum = container.GetChildComponent<StateText>("Label_txtShengliNr");
            if (pbItemList.Count > 0)
            {
                container.Visible = true;
                txtRewardName.CurrentText.text = rewardName;
                MogoAtlasUtils.AddIcon(iconContainer.gameObject, item_helper.GetIcon((int)pbItemList[0].id));
                txtRewardNum.CurrentText.text = string.Format("x{0}", (int)pbItemList[0].num);
            }
            else
            {
                container.Visible = false;
            }
        }

        private void ResetAllContainer()
        {
            _resultLeftIconObj.SetActive(false);
            _resultRightInfoBg.SetActive(false);
            _combatIntegralContaienr.Visible = false;
            _loseIcon.Visible = false;
            _winIcon.Visible = false;
            _winOrFailReward.Visible = false;
            _rankReward.Visible = false;
            _expView.Visible = false;
            _txtContinue.Visible = false;
        }

        /// <summary>
        /// 动画第一步
        /// </summary>
        private void TweenFirstStep()
        {
            _isPlayingTween = true;

            //防止什么原因导致卡在此界面
            TimerHeap.AddTimer(5000, 0, OnEndTween);
            _resultLeftIconObj.SetActive(true);
            LoadResultIcon((obj) =>
                {
                    TweenViewMove.Begin(_resultLeftIconObj, MoveType.Show, MoveDirection.Down).Tweener.onFinished = TweenFirstStepEnd;
                });
            _resultRightInfoBg.SetActive(true);
            TweenViewMove.Begin(_resultRightInfoBg, MoveType.Show, MoveDirection.Down).Tweener.onFinished = TweenFirstStepEnd;
            ShowCombatIntegral();
            if (_combatIntegralContaienr.Visible == true)
            {
                TweenViewMove.Begin(_combatIntegralContaienr.gameObject, MoveType.Show, MoveDirection.Down).Tweener.onFinished = TweenFirstStepEnd;
                _playingTweenNum = 3;
            }
            else
            {
                _playingTweenNum = 2;
            }
        }

        private void OnEndTween()
        {
            _isPlayingTween = false;
        }

        /// <summary>
        /// 动画第一步结束
        /// </summary>
        /// <param name="tweer"></param>
        public void TweenFirstStepEnd(UITweener tweer)
        {
            _playingTweenNum--;
            if (_playingTweenNum <= 0)
            {
                TimerHeap.AddTimer(200,0, TweenSecondStep);
                
            }
        }

        /// <summary>
        /// 动画第二步
        /// </summary>
        private void TweenSecondStep()
        {
            int playingTweenNum = 0;
            ShowCombatResult();
            if (_loseIcon.Visible == true)
            {
                _loseIcon.transform.localScale = Vector3.one * TWEEN_SCALE;
                TweenScale.Begin(_loseIcon.gameObject, 0.2f, Vector3.one, UITweener.Method.EaseOut, TweenSecondStepEnd);
            }
            else
            {
                _winIcon.transform.localScale = Vector3.one * TWEEN_SCALE;
                TweenScale.Begin(_winIcon.gameObject, 0.2f, Vector3.one, UITweener.Method.EaseOut, TweenSecondStepEnd);
            }
            playingTweenNum++;
            ShowRewardInfo();
            if (_winOrFailReward.Visible == true)
            {
                TweenViewMove.Begin(_winOrFailReward.gameObject, MoveType.Show, MoveDirection.Right).Tweener.onFinished = TweenSecondStepEnd;
                playingTweenNum++;
            }
            if (_rankReward.Visible == true)
            {
                TweenViewMove.Begin(_rankReward.gameObject, MoveType.Show, MoveDirection.Right).Tweener.onFinished = TweenSecondStepEnd;
                playingTweenNum++;
            }
            if (_integralReward.Visible == true)
            {
                TweenViewMove.Begin(_integralReward.gameObject, MoveType.Show, MoveDirection.Right).Tweener.onFinished = TweenSecondStepEnd;
                playingTweenNum++;
            }
            _playingTweenNum = playingTweenNum;
        }

        /// <summary>
        /// 动画第2步结束
        /// </summary>
        /// <param name="tweener"></param>
        private void TweenSecondStepEnd(UITweener tweener)
        {
            _playingTweenNum--;
            if (_playingTweenNum <= 0)
            {
                TimerHeap.AddTimer(200, 0, TweenThirdStep);
            }
        }

        /// <summary>
        /// 动画第3步
        /// </summary>
        private void TweenThirdStep()
        {
            _expView.Visible = true;
            _expView.ShowSpecialReward(_logic);
            _txtContinue.Visible = true;
            if (this.Visible == true)
            {
                TweenStateChangeableAlpha.Begin(_txtContinue.gameObject, 2, 0.3f, FadeInContinueText);
            }
        }

        private void FadeInContinueText(UITweener tweener)
        {
            if (this.Visible == true)
            {
                TweenStateChangeableAlpha.Begin(_txtContinue.gameObject, 2, 0.7f, FadeOutContinueText);
            }
        }

        private void FadeOutContinueText(UITweener tweener)
        {
            if (this.Visible == true)
            {
                TweenStateChangeableAlpha.Begin(_txtContinue.gameObject, 2, 0.3f, FadeInContinueText);
            }
        }

    }
}
