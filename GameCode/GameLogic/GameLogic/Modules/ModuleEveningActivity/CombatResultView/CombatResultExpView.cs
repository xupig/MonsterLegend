﻿using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.Enum;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEveningActivity
{
    public class CombatResultExpView:KContainer
    {
        private StateText _playerName;
        private StateText _level;
        private StateText _txtExpNumLabel;
        private StateText _txtExpNum;
        private StateText _txtCoinNumLabel;
        private StateText _txtCoinNum;
        private KContainer _upgradeContainer;
        private CopyProgressBar _expProgress;
        private CopyBaseLogic _logic;

        private int change = 0;

        public KComponentEvent onTweenFinish = new KComponentEvent();

        protected override void Awake()
        {
            base.Awake();
            _playerName = GetChildComponent<StateText>("Label_txtWanjiamingzi");
            _level = GetChildComponent<StateText>("Label_txtdengji");
            _txtExpNum = GetChildComponent<StateText>("Label_txtJingyan");
            _txtExpNumLabel = GetChildComponent<StateText>("Label_txtJingyanLabel");
            _txtCoinNum = GetChildComponent<StateText>("Label_txtJinbi");
            _txtCoinNumLabel = GetChildComponent<StateText>("Label_txtJinbiLabel");
            _upgradeContainer = GetChildComponent<KContainer>("Container_shengji");
            _expProgress = AddChildComponent<CopyProgressBar>("ProgressBar_jindu");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            _upgradeContainer.Visible = false;
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        public void ShowSpecialReward(CopyBaseLogic logic)
        {
            _logic = logic;
            ItemData expItemData = GetItemData(public_config.ITEM_SPECIAL_TYPE_EXP);
            ItemData goldItemData = GetItemData(public_config.MONEY_TYPE_GOLD);
            ItemData levelExpItemData = GetItemData(public_config.ITEM_SPECIAL_TYPE_LEVEL_EXP);
            ItemData levelGoldItemData = GetItemData(public_config.ITEM_SPECIAL_TYPE_LEVEL_GOLD);
            _playerName.CurrentText.text = PlayerAvatar.Player.name;
            _txtExpNumLabel.CurrentText.text = string.Format("{0}:", MogoLanguageUtil.GetContent(20));
            _txtExpNum.CurrentText.text = string.Format("+{0}", expItemData.StackCount + levelExpItemData.StackCount);
            _txtCoinNumLabel.CurrentText.text = string.Format("{0}:", MogoLanguageUtil.GetContent(14));
            _txtCoinNum.CurrentText.text = string.Format("+{0}", goldItemData.StackCount + levelGoldItemData.StackCount);
            TweenViewMove.Begin(gameObject, MoveType.Show, MoveDirection.Right).Tweener.onFinished = TweenEnd;
        }

        private void TweenEnd(UITweener tweener)
        {
            ItemData expItemData = GetItemData(public_config.ITEM_SPECIAL_TYPE_EXP);
            ItemData goldItemData = GetItemData(public_config.MONEY_TYPE_GOLD);
            ItemData levelExpItemData = GetItemData(public_config.ITEM_SPECIAL_TYPE_LEVEL_EXP);
            ItemData levelGoldItemData = GetItemData(public_config.ITEM_SPECIAL_TYPE_LEVEL_GOLD);
            TweenNumIncrease(_txtExpNum, 0, expItemData.StackCount + levelExpItemData.StackCount);
            TweenNumIncrease(_txtCoinNum, 0, goldItemData.StackCount + levelGoldItemData.StackCount);
            ShowExpProgress(expItemData.StackCount + levelExpItemData.StackCount);
        }

        private void AddEventListener()
        {
            _expProgress.Over100PercentOnce.AddListener(OnLevelUp);
        }

        private void RemoveEventListener()
        {
            _expProgress.Over100PercentOnce.RemoveListener(OnLevelUp);
        }

        private void ShowExpProgress(int exp)
        {
            LevelConfig lastLevel = avatar_level_helper.GetLastLevelConfig(PlayerAvatar.Player.level, PlayerAvatar.Player.exp, exp);
            float nowRate = avatar_level_helper.GetExpPercent(PlayerAvatar.Player.exp, PlayerAvatar.Player.level);
            float lastRate = avatar_level_helper.GetExpPercent(lastLevel.Exp, lastLevel.Level);
            _level.CurrentText.text = lastLevel.Level.ToString();
            _expProgress.SetInitLevel(lastLevel.Level);
            _expProgress.SetProgress(lastRate, nowRate + (PlayerAvatar.Player.level - lastLevel.Level));
        }

        private ItemData GetItemData(int itemId)
        {
            List<RewardData> totalRewardData = null;
            if (_logic is CopyEveningActivityLogic)
            {
                CopyEveningActivityLogic logic = _logic as CopyEveningActivityLogic;
                EveningActivityResultPersonData personResultData = logic.EveningActivityResultPersonDataDic[PlayerAvatar.Player.id];
                totalRewardData = personResultData.GetRewardList();

            }
            else if (_logic is CopyCombatPortalLogic)
            {
                CopyCombatPortalLogic portalLogic = _logic as CopyCombatPortalLogic;
                totalRewardData = portalLogic.TransPortalResultPersonDataDic[PlayerAvatar.Player.id].GetRewardList();
            }

            Dictionary<int, int> rewardDic = item_reward_helper.ParseRewardListToDic(totalRewardData);

            ItemData itemData = new ItemData(itemId);
            if (rewardDic.ContainsKey(itemId) == true)
            {
                if (itemId == public_config.ITEM_SPECIAL_TYPE_LEVEL_EXP)
                {
                    itemData.StackCount = Mathf.CeilToInt(rewardDic[itemId] * avatar_level_helper.GetExpStandard((int)PlayerAvatar.Player.level) * 0.0001f);
                }
                else if (itemId == public_config.ITEM_SPECIAL_TYPE_LEVEL_GOLD)
                {
                    itemData.StackCount = Mathf.CeilToInt(rewardDic[itemId] * avatar_level_helper.GetGoldStandard((int)PlayerAvatar.Player.level) * 0.0001f);
                }
                else
                {
                    itemData.StackCount = rewardDic[itemId];
                }
            }
            return itemData;
        }

        private void OnLevelUp(int level)
        {
            _upgradeContainer.Visible = true;
            _level.CurrentText.text = level.ToString();
        }


        private void TweenNumIncrease(StateText txtLabel, int startNum, int endNum)
        {
            change = endNum - startNum;
            TimerHeap.AddTimer<StateText, int, int>(50, 0, TweenRefresh, txtLabel,startNum, endNum);
        }

        private void TweenRefresh(StateText txtLabel, int startNum, int endNum)
        {
            if (startNum < endNum)
            {
                startNum += Mathf.CeilToInt(change / 19.0f);
                txtLabel.CurrentText.text = string.Format("+{0}", startNum);
                TimerHeap.AddTimer<StateText, int, int>(50, 0, TweenRefresh, txtLabel, startNum, endNum);
            }
            else
            {
                onTweenFinish.Invoke();
            }
        }
    }
}
