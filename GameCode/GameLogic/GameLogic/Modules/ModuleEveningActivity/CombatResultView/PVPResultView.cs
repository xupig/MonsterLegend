﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEveningActivity
{
    public class PVPResultView:KContainer
    {
        private StateText _txtOurScore;
        private StateText _txtEnemyScore;
        private StateText _txtOurCombatResult;
        private StateText _txtEnemyCombatResult;
        private StateText[] _conditionNameList;
        private StateText[] _rewardNameList;
        private KButton _okButton;
        private KButton _statisticBtn;
        private KList _ourGroupAvatarResultList;
        private KList _EnemyAvatarResultList;

        private Vector2 _startPosition;
        private Vector2 _endPosition;
        private CopyMultiPlayerLogic _logic;

        protected override void Awake()
        {
            base.Awake();
            _conditionNameList = new StateText[4];
            _txtOurScore = GetChildComponent<StateText>("Container_zhankuang/Container_zhandouxinxi/Label_txtWofangNR");
            _txtEnemyScore = GetChildComponent<StateText>("Container_zhankuang/Container_zhandouxinxi/Label_txtDifangNR");
            for (int i = 0; i < 4; i++)
            {
                _conditionNameList[i] = GetChildComponent<StateText>(string.Format("Container_biaoti/Label_txttiaojian0{0}", i + 1));
            }
            _rewardNameList = new StateText[3];
            for (int i = 0; i < 3; i++)
            {
                _rewardNameList[i] = GetChildComponent<StateText>(string.Format("Container_biaoti/Label_txtjiangli{0}", i + 1));
            }
            _startPosition = _conditionNameList[0].GetComponent<RectTransform>().anchoredPosition;
            _endPosition = _rewardNameList[0].GetComponent<RectTransform>().anchoredPosition;
            _okButton = GetChildComponent<KButton>("Button_queding");
            _statisticBtn = GetChildComponent<KButton>("Button_zhandoutongji");
            _ourGroupAvatarResultList = GetChildComponent<KList>("Container_shengli/List_jiesuanxinxi");
            _txtOurCombatResult = GetChildComponent<StateText>("Container_shengli/Container_shengli/Label_txtShengli");
            _txtEnemyCombatResult = GetChildComponent<StateText>("Container_shibai/Container_shengli/Label_txtShengli");
            _EnemyAvatarResultList = GetChildComponent<KList>("Container_shibai/List_jiesuanxinxi");
            _ourGroupAvatarResultList.SetDirection(KList.KListDirection.LeftToRight, 1);
            _EnemyAvatarResultList.SetDirection(KList.KListDirection.LeftToRight, 1);
            AddEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

        private void AddEventListener()
        {
            _okButton.onClick.AddListener(OnCloseView);
            _statisticBtn.onClick.AddListener(ShowStatisticPanel);
        }

        private void RemoveEventListener()
        {
            _okButton.onClick.RemoveListener(OnCloseView);
            _statisticBtn.onClick.RemoveListener(ShowStatisticPanel);
        }

        public void RefreshResultView(CopyMultiPlayerLogic logic)
        {
            this.Visible = true;
            _logic = logic;
            RefreshTotalScore();
            RefreshConditionName();
            RefreshAvatarResultInfo();
            RefreshStatisticButton();
        }

        public void Hide()
        {
            this.Visible = false;
        }

        private void RefreshTotalScore()
        {
            if (_logic is CopyEveningActivityLogic)
            {
                CopyEveningActivityLogic logic = _logic as CopyEveningActivityLogic;
                int selfCampId = logic.SelfCampId;
                int enemyCampId = logic.EnemyCampId;
                EveningActivityResultCampData selfResultCampData = logic.GetResultCampInfoByCampId(selfCampId);
                EveningActivityResultCampData enemyResultCampData = logic.GetResultCampInfoByCampId(enemyCampId);

                _txtOurScore.CurrentText.text = selfResultCampData.CampScore.ToString();
                _txtEnemyScore.CurrentText.text = enemyResultCampData.CampScore.ToString();
            }
        }

        private void RefreshConditionName()
        {
            Dictionary<int, string> conditionNameDic = _logic.GetConditionchinese();
            List<int> actionIdList = conditionNameDic.Keys.ToList();
            for (int i = 0; i < actionIdList.Count; i++)
            {
                _conditionNameList[i].Visible = true;
                _conditionNameList[i].CurrentText.text = conditionNameDic[actionIdList[i]];
            }
            for (int i = actionIdList.Count; i < 4; i++)
            {
                _conditionNameList[i].Visible = false;
            }

            int maxRewardItemCount = _logic.GetMaxNotSpecialRewardCount();

            for (int i = 0; i < maxRewardItemCount; i++)
            {
                _rewardNameList[i].Visible = true;
            }
            for (int i = maxRewardItemCount; i < 3; i++)
            {
                _rewardNameList[i].Visible = false;
            }

            RefreshConditionNameLayout(actionIdList.Count, maxRewardItemCount);
        }

        private void RefreshConditionNameLayout(int conditionNum, int rewardItemCount)
        {

            Vector2 gap = (_endPosition - _startPosition) / (conditionNum + rewardItemCount - 1);
            for (int i = 1; i < conditionNum; i++)
            {
                _conditionNameList[i].GetComponent<RectTransform>().anchoredPosition = _startPosition + gap * i;
            }
            for (int i = 0; i < rewardItemCount; i++)
            {
                _rewardNameList[i].GetComponent<RectTransform>().anchoredPosition = _startPosition + (conditionNum + i) * gap;
            }
        }

        private void RefreshAvatarResultInfo()
        {
            _ourGroupAvatarResultList.RemoveAll();
            _EnemyAvatarResultList.RemoveAll();
            if (_logic is CopyEveningActivityLogic)
            {
                CopyEveningActivityLogic logic = _logic as CopyEveningActivityLogic;
                List<EveningActivityResultPersonData> avatarResultList = logic.OurGoupPersonResultDataList;
                if (avatarResultList[0].IsWin == 1)
                {
                    _txtOurCombatResult.CurrentText.text = MogoLanguageUtil.GetContent(65324);
                }
                else
                {
                    _txtOurCombatResult.CurrentText.text = MogoLanguageUtil.GetContent(65325);
                }
                avatarResultList.Sort(ListCompareSort);
                _ourGroupAvatarResultList.SetDataList<AvatarResultItem>(avatarResultList);
                TweenViewMove.Begin(_ourGroupAvatarResultList.gameObject, MoveType.Show, MoveDirection.Right);

                avatarResultList = logic.EnemyPersonResultDataList;
                avatarResultList.Sort(ListCompareSort);
                if (avatarResultList[0].IsWin == 1)
                {
                    _txtEnemyCombatResult .CurrentText.text = MogoLanguageUtil.GetContent(65324);
                }
                else
                {
                    _txtEnemyCombatResult.CurrentText.text = MogoLanguageUtil.GetContent(65325);
                }
                _EnemyAvatarResultList.SetDataList<AvatarResultItem>(avatarResultList);
                TweenViewMove.Begin(_EnemyAvatarResultList.gameObject, MoveType.Show, MoveDirection.Right);
            }
        }

        private void OnCloseView()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.PVEResult);
            GameSceneManager.GetInstance().LeaveCombatScene();
        }

        private void ShowStatisticPanel()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.BattleDataStatistic);
        }

        private int ListCompareSort(EveningActivityResultPersonData first, EveningActivityResultPersonData second)
        {
            return (int)first.RankNum - (int)second.RankNum;
        }

        private void RefreshStatisticButton()
        {
            int insId = GameSceneManager.GetInstance().curInstanceID;
            instance ins = instance_helper.GetInstanceCfg(insId);
            _statisticBtn.Visible = instance_helper.GetStatisticType(ins) != 0;
        }
    }
}
