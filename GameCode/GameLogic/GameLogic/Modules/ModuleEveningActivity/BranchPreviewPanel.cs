﻿using Common.Base;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEveningActivity
{
    public class BranchPreviewPanel:BasePanel
    {
        private BranchPreviewLeftView _leftView;
        private BranchPreviewRightView _rightView;

        protected override void Awake()
        {
            base.Awake();
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _leftView = AddChildComponent<BranchPreviewLeftView>("Container_left");
            _rightView = AddChildComponent<BranchPreviewRightView>("Container_right");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.BranchPreview; }
        }

        public override void OnShow(object data)
        {
            SetData(data);
            TweenViewMove.Begin(_leftView.gameObject, MoveType.Show, MoveDirection.Left);
            TweenViewMove.Begin(_rightView.gameObject, MoveType.Show, MoveDirection.Right);
        }

        public override void OnClose()
        {
            ;
        }

        public override void SetData(object data)
        {
            _leftView.Refresh((int)data);
            _rightView.RefreshRightView((int)data);

        }


    }
}
