﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleMainUI
{
    public class EveningActivityStatisticPanel : BasePanel
    {
        private CopyProgressView _copyProgressView;
        private CopyProgressView _copyProgressDownView;
        private MonsterWaveNumView _monsterWaveNumView;
        private CampIntegralView _campIntegralView;
        private IntegralDetailView _integralDetailView;
        private TeamScoreView _teamScoreView;
        private CombatAttriView _combatAttriView;

        protected override void Awake()
        {
            base.Awake();
            _copyProgressView = AddChildComponent<CopyProgressView>("Container_topCenter/Container_fubenjindushang");
            _copyProgressDownView = AddChildComponent<CopyProgressView>("Container_bottomCenter/Container_fubenjinduxia");
            _monsterWaveNumView = AddChildComponent<MonsterWaveNumView>("Container_topCenter/Container_guaiwutitle");
            _campIntegralView = AddChildComponent<CampIntegralView>("Container_topCenter/Container_zhenyingxinxi");
            _integralDetailView = AddChildComponent<IntegralDetailView>("Container_topRight/Container_zhenyingxinxidetail");
            _teamScoreView = AddChildComponent<TeamScoreView>("Container_bottomCenter/Container_duiwuzongjifen");
            _combatAttriView = AddChildComponent<CombatAttriView>("Container_topRight/Container_zhenyingxinxidetail");

            Hide();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.EveningActivityStatistic; }
        }

        public override void OnShow(object data)
        {
            PlayerDataManager.Instance.InstanceAvatarData.ScoreDict.Clear();
            AddEventListener();
            SpaceNoticeActionManager.Instance.ShowSystemNotice();
            Show();
        }

        public override void OnClose()
        {
            RemoveEventListener();
            Hide();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<SystemNoticeData>(BattleUIEvents.SHOW_INFOMATION, OnShowSystemNotice);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<SystemNoticeData>(BattleUIEvents.SHOW_INFOMATION, OnShowSystemNotice);
        }

        private void OnShowSystemNotice(SystemNoticeData notice)
        {
            if (notice.type == InstanceDefine.COUNTER_NOTICE_ACTION || notice.type == InstanceDefine.COUNTER_NOTICE_ACTION_INIT)
            {
                SpaceNoticeActionManager.Instance.RemoveSystemNotice(notice);
                SystemNoticeTopData topNotice = notice as SystemNoticeTopData;
                switch (topNotice.notice_type)
                {
                    case InstanceDefine.NOTICE_MAX_SCORE:
                        UIManager.Instance.ShowPanel(PanelIdEnum.PortalFight);
                        break;
                    case InstanceDefine.NOTICE_MONSTER_WAVE:
                    case InstanceDefine.NOTICE_SUPPLY:
                        _monsterWaveNumView.Show(topNotice);
                        break;
                    case InstanceDefine.NOTICE_COPY_PROGRESS_DOWN:
                        _copyProgressDownView.Show(topNotice);
                        break;
                    case InstanceDefine.NOTICE_COPY_PROGRESS:
                        _copyProgressView.Show(topNotice);
                        break;
                    case InstanceDefine.NOTICE_TEAM_SCORE:
                        _teamScoreView.Show(topNotice);
                        break;
                    case InstanceDefine.NOTICE_CAMP_SCORE:
                        map map = map_helper.GetMap(GameSceneManager.GetInstance().curMapID);
                        if (map.__group >= 2) //是pvp才显示
                        {
                            _campIntegralView.Show(topNotice);
                        }
                        break;
                    case InstanceDefine.NOTICE_COMBAT_ATTRI:
                        _combatAttriView.Show(topNotice as SystemCombatAttriData);
                        break;
                    case InstanceDefine.NOTICE_PLAYER_SCORE:
                        _integralDetailView.Show(topNotice);
                        SaveScore(topNotice);
                        break;
                }
            }

        }

        private static void SaveScore(SystemNoticeTopData topNotice)
        {
            var scoreDict = PlayerDataManager.Instance.InstanceAvatarData.ScoreDict;
            int addValue = 0;
            if (scoreDict.ContainsKey(topNotice.rightUpTargetId) == false)
            {
                addValue = topNotice.timerCount;
                scoreDict.Add(topNotice.rightUpTargetId, topNotice.timerCount);
            }
            else
            {
                addValue = topNotice.timerCount - scoreDict[topNotice.rightUpTargetId];
                scoreDict[topNotice.rightUpTargetId] = topNotice.timerCount;
            }
            if (addValue > 0 && topNotice.rightUpTargetId == PlayerAvatar.Player.id)
            {
                EventDispatcher.TriggerEvent<int, int>(BattleUIEvents.SHOW_FLOAT_TIP, 2, addValue);
            }
            EventDispatcher.TriggerEvent(BattleUIEvents.REFRESH_AVATAR_SCORE);
        }

        private void Hide()
        {
            _copyProgressView.Hide();
            _copyProgressDownView.Hide();
            _monsterWaveNumView.Hide();
            _campIntegralView.Hide();
            _integralDetailView.Hide();
            _teamScoreView.Hide();
            _combatAttriView.Hide();
        }

        private void Show()
        {
            int mapId = GameSceneManager.GetInstance().curMapID;
            if (GameSceneManager.GetInstance().curMapType == public_config.MAP_TYPE_NIGHT_ACTIVITY && map_helper.GetMap(mapId).__group == 2)
            {
                _campIntegralView.InitProgressContainer();
            }
        }
    }
}
