﻿#region 模块信息
/*==========================================
// 模块名：BagModule
// 命名空间: ModuleBag
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/08
// 描述说明：背包模块Module
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;

using System.Text;

using UnityEngine;
using Common.Base;
using Common;
using Common.Events;
using MogoEngine.RPC;
using MogoEngine.Events;
using GameLoader.Config;

using GameMain.Entities;
using MogoEngine;
using Common.ServerConfig;

using GameLoader.Utils.CustomType;
using Common.Structs;
using MogoEngine.Mgrs;
using Common.ClientConfig;

namespace ModuleBag
{
    public class BagModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.Bag, "Container_BagPanel", MogoUILayer.LayerUIPanel, "ModuleBag.BagPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.BagTips, "Container_BagTipPanel", MogoUILayer.LayerUIToolTip, "ModuleBag.BagTipsPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }

    }
}
