﻿using System;
using System.Collections.Generic;
using System.Text;

using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Base;
using Common.ClientConfig;
using Common.Events;
using Common.Structs;
using Game.UI.UIComponent;
using GameMain.Entities;
using Common.Data;
using Common.Structs.ProtoBuf;
using Common.ExtendComponent;
using Common.Utils;
using UnityEngine;
using GameMain.GlobalManager;

namespace ModuleBag
{
    public class BagTipsData
    {
        public string title;
        public string content;
        public Action<bool> confirmAction;
    }

    public class BagTipsPanel : BasePanel
    {
        private KToggle _notShowToggle;
        private KButton _ensureBtn;
        private KButton _cancelBtn;
        private StateText _titleTxt;
        private StateText _contentTxt;

        private BagTipsData _data;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_tanchuang/Button_close");
            ModalMask = GetChildComponent<StateImage>("Container_tanchuang/ScaleImage_sharedZhezhao");
            _notShowToggle = GetChildComponent<KToggle>("Container_tanchuang/Toggle_Xuanze");
            _ensureBtn = GetChildComponent<KButton>("Container_tanchuang/Button_queding");
            _cancelBtn = GetChildComponent<KButton>("Container_tanchuang/Button_quxiao");
            _titleTxt = GetChildComponent<StateText>("Container_tanchuang/Label_txtBiaoti");
            _contentTxt = GetChildComponent<StateText>("Container_tanchuang/Label_txtNR");
            _titleTxt.CurrentText.alignment = TextAnchor.MiddleCenter;
            _contentTxt.CurrentText.alignment = TextAnchor.MiddleCenter;
        }


        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.BagTips; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            _data = data as BagTipsData;
            _notShowToggle.isOn = false;
            RefreshContent();
        }

        public override void OnClose()
        {
            RemoveEventListener();
            _data = null;
        }

        private void RefreshContent()
        {
            _titleTxt.CurrentText.text = _data.title;
            _contentTxt.CurrentText.text = _data.content;
        }

        private void AddEventListener()
        {
            _ensureBtn.onClick.AddListener(OnEnsureBtnClick);
            _cancelBtn.onClick.AddListener(OnCanceBtnClick);
        }

        private void RemoveEventListener()
        {
            _ensureBtn.onClick.RemoveListener(OnEnsureBtnClick);
            _cancelBtn.onClick.RemoveListener(OnCanceBtnClick);
        }

        private void OnCanceBtnClick()
        {
            ClosePanel();
        }

        private void OnEnsureBtnClick()
        {
            if (_data.confirmAction != null)
            {
                _data.confirmAction.Invoke(_notShowToggle.isOn);
            }
            ClosePanel();
        }

        public static void Show(string title, string content, Action<bool> confirmAction)
        {
            BagTipsData data = new BagTipsData();
            data.title = title;
            data.content = content;
            data.confirmAction = confirmAction;
            UIManager.Instance.ShowPanel(PanelIdEnum.BagTips, data);
        }


    }
}
