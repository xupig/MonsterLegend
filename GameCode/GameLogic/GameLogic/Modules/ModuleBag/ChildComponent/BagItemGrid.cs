﻿#region 模块信息
/*==========================================
// 模块名：ItemGrid
// 命名空间: ModuleBag
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/08
// 描述说明：背包模块格子单元
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using Game.UI.UIComponent;

using Common.Structs;
using UnityEngine;
using UnityEngine.EventSystems;
using MogoEngine;
using ModuleCommonUI;
using GameMain.Entities;
using Common.Utils;
using GameMain.GlobalManager.SubSystem;
using GameMain.GlobalManager;
using Common.ServerConfig;
using GameData;
using Common.Global;
using Common.Structs.Enum;
using Common.Data;
using Common.Base;
using Common.ExtendComponent;

namespace ModuleBag
{

    public class BagItemGrid : KList.KListItemBase
    {
        private BaseItemInfo _data;
        private ItemGrid _itemGrid;

        /// <summary>
        ///  用数字显示可堆叠道具的实际堆叠数（大于1时才显示）
        /// </summary>
        private StateText _textNum;
        /// <summary>
        ///  未开启的格子上显示被锁住状态
        /// </summary>
        private GameObject _gridLock;
        /// <summary>
        ///  当装备与使用等级条件不符时，该装备图标上显示被锁住的效果
        /// </summary>
        private GameObject _itemLock;
        private KButton _fastReplaceBtn;

        private KList _parent;

        private static bool _showUnlockTips = true;

        protected override void Awake()
        {
            _parent = transform.parent.GetComponent<KList>();
            _textNum = GetChildComponent<StateText>("Label_txtNum");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _itemGrid.Context = PanelIdEnum.Bag;
            onClick.AddListener(OnGridClick);
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as BaseItemInfo;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            Refresh();
        }

        protected void Refresh()
        {
            ID = 0;
            switch(GetGridState())
            {
                case BagGridState.Locked:
                    SetLockState();
                    break;
                case BagGridState.Empty:
                    SetEmptyState();
                    break;
                case BagGridState.Occupied:
                    SetOccupiedState();
                    break;
            }
        }

        /// <summary>
        /// 格子当前数据状态
        /// </summary>
        /// <returns></returns>
        private BagGridState GetGridState()
        {
            if(GetGridPosition() > PlayerAvatar.Player.cur_bag_count)
            {
                return BagGridState.Locked;
            }
            if(_data == null)
            {
                return BagGridState.Empty;
            }
            return BagGridState.Occupied;
        }

        private void OnGridClick(KList.KListItemBase item, int Index)
        {
            OnGridClick();
        }

        private void OnGridClick()
        {
            switch(GetGridState())
            {
                case BagGridState.Occupied:
                    ShowItemTip();
                    break;
                case BagGridState.Locked:
                    UnlockGrid();
                    break;
            }
        }

        private void ShowItemTip()
        {
            ToolTipsManager.Instance.ShowItemTip(_data as BaseItemInfo, PanelIdEnum.Bag);
        }

        private void UnlockGrid()
        {
            int count = GetGridPosition() - (int)PlayerAvatar.Player.cur_bag_count;
            int needMoney = GetBagExpandPrice() * count;
            int moneyType = GetBagExpandMoney();
            int myMoney = item_helper.GetMyMoneyCountByItemId(moneyType);
            if (myMoney >= needMoney)
            {
                if (_showUnlockTips)
                {
                    string content = string.Concat("\n", MogoLanguageUtil.GetContent(5161, needMoney, item_helper.GetName(GetBagExpandMoney()), count));
                    //MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, OnConfirm);
                    BagTipsPanel.Show(MessageBox.DEFAULT_TITLE, content, OnUnlockConfirm);
                }
                else
                {
                    OnConfirm();
                }
            }
            else if (moneyType == public_config.MONEY_TYPE_BIND_DIAMOND)
            {
                UseDiamondReplaceBindDiamond(count, needMoney, myMoney);
            }
            else
            {
                string content = string.Concat("\n", MogoLanguageUtil.GetContent(5161, needMoney, item_helper.GetName(moneyType), count));
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, ShowNotEmoughTips);
            }

        }

        private static void UseDiamondReplaceBindDiamond(int count, int needMoney, int myMoney)
        {
            string content = string.Empty;
            if (myMoney == 0)
            {
                content = MogoLanguageUtil.GetContent(5163, needMoney - myMoney, item_helper.GetName(public_config.MONEY_TYPE_DIAMOND), count);
            }
            else
            {
                content = MogoLanguageUtil.GetContent(5162, myMoney, item_helper.GetName(public_config.MONEY_TYPE_BIND_DIAMOND), needMoney - myMoney, item_helper.GetName(public_config.MONEY_TYPE_DIAMOND), count);
            }
            Action confirmAction = new Action(() =>
            {
                if (needMoney - myMoney <= PlayerAvatar.Player.money_diamond)
                {
                    BagManager.Instance.RequestUnlockGrid(count);
                }
                else
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(5160), PanelIdEnum.MainUIField);
                }
            });
            Action cancelAction = new Action(() =>
            {
                MessageBox.CloseMessageBox();
            });
            MessageBox.Show(false, MessageBox.DEFAULT_TITLE, content, confirmAction, cancelAction, false, true, true);
        }

        private void OnUnlockConfirm(bool notShowUnlockTips)
        {
            _showUnlockTips = !notShowUnlockTips;
            OnConfirm();
        }

        private void OnConfirm()
        {
            int count = GetGridPosition() - (int)PlayerAvatar.Player.cur_bag_count;
            BagManager.Instance.RequestUnlockGrid(count);
        }

        private void ShowNotEmoughTips()
        {
            int moneyType = GetBagExpandMoney();
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(56743, item_helper.GetName(moneyType)), PanelIdEnum.MainUIField);
        }

        private int GetBagExpandMoney()
        {
            Dictionary<int, int> dict = MogoStringUtils.Convert2Dic(GameData.XMLManager.global_params[36].__value);
            Dictionary<int, int>.KeyCollection.Enumerator enumerator = dict.Keys.GetEnumerator();
            if (enumerator.MoveNext())
            {
                return enumerator.Current;
            }
            return 0;
        }

        private int GetBagExpandPrice()
        {
            Dictionary<int, int> dict = MogoStringUtils.Convert2Dic(GameData.XMLManager.global_params[36].__value);
            Dictionary<int, int>.ValueCollection.Enumerator enumerator = dict.Values.GetEnumerator();
            if (enumerator.MoveNext())
            {
                return enumerator.Current;
            }
            return 0;
        }

        private int GetGridPosition()
        {
            return this.Index + 1;
        }

        private void SetEmptyState()
        {
            HideGridLock();
            HideItemLock();
            HideFastReplaceBtn();
            RefreshTextNum(string.Empty);
            _itemGrid.Clear();
            ShowItemGrid();
        }

        private void SetLockState()
        {
            ShowGridLock();
            HideItemLock();
            HideItemGrid();
            HideFastReplaceBtn();
            RefreshTextNum(string.Empty);
        }

        private void SetOccupiedState()
        {
            HideGridLock();
            HideItemLock();
            HideFastReplaceBtn();
            PlayerAvatar player = PlayerAvatar.Player;
            bool isVocationLock = _data.UsageVocationRequired == Vocation.None ? false : _data.UsageVocationRequired != player.vocation;
            bool isLevelLock = _data.UsageLevelRequired == 0 ? false : _data.UsageLevelRequired > player.level;
            if(isVocationLock || isLevelLock)
            {
                ShowItemLock();
            }
            string content = _data.StackCount > 1 ? _data.StackCount.ToString() : string.Empty;
            RefreshTextNum(content);
            RefreshItemGrid(_data);
            RefreshFastReplaceBtn();
            ShowItemGrid();
        }

        private void RefreshItemGrid(BaseItemInfo itemInfo)
        {
            ID = itemInfo.Id;
            _itemGrid.SetItemData(itemInfo);
            _itemGrid.onClick = OnGridClick;
        }

        private void RefreshFastReplaceBtn()
        {
            if (_data.Type == BagItemType.Equip && CanWearEquip == true)
            {
                EquipItemInfo equipInfo = _data as EquipItemInfo;
                BagData bodyEquipData = PlayerDataManager.Instance.BagData.GetBodyEquipBagData();
                EquipItemInfo leftInfo;
                if(equipInfo.SubType == EquipType.ring)
                {
                    EquipItemInfo rightRing = bodyEquipData.GetItemInfo(public_config.EQUIP_POS_RIGHTRING) as EquipItemInfo;
                    EquipItemInfo leftRing = bodyEquipData.GetItemInfo(public_config.EQUIP_POS_LEFTRING) as EquipItemInfo;
                    if (leftRing == null || rightRing == null)
                    {
                        leftInfo = leftRing == null ? leftRing : rightRing;
                    }
                    else
                    {
                        leftInfo = rightRing.FightForce < leftRing.FightForce ? rightRing : leftRing;
                    }
                }
                else
                {
                    leftInfo = bodyEquipData.GetItemInfo((int)equipInfo.SubType) as EquipItemInfo;
                }
                if(leftInfo == null || leftInfo.FightForce < equipInfo.FightForce)
                {
                    ShowFastReplaceBtn();
                }
            }
        }

        private void OnFastReplaceBtnClick()
        {
            EquipItemInfo equipInfo = _data as EquipItemInfo;
            BagManager.Instance.RequestWearEquip(equipInfo.GridPosition, PlayerDataManager.Instance.BagData.GetBodyEquipPosition(equipInfo), PlayerDataManager.Instance.BagData.IsBodyEquipPositionOccupied(equipInfo));
        }

        private bool CanWearEquip
        {
            get
            {
                return PlayerAvatar.Player.vocation == _data.UsageVocationRequired && PlayerAvatar.Player.level >= _data.UsageLevelRequired;
            }
        }

        private void ShowGridLock()
        {
            if(_gridLock == null)
            {
                _gridLock = CloneGameObject("template/Image_sharedGridLock");
            }
            _gridLock.SetActive(true);
        }

        private void HideGridLock()
        {
            if(_gridLock != null)
            {
                _gridLock.SetActive(false);
            }
        }

        private void ShowItemLock()
        {
            if(_itemLock == null)
            {
                _itemLock = CloneGameObject("template/Image_ItemLocked", 6.0f, -6.0f);
            }
            _itemLock.SetActive(true);
        }

        private void HideItemLock()
        {
            if(_itemLock != null)
            {
                _itemLock.SetActive(false);
            }
        }

        private void ShowFastReplaceBtn()
        {
            if(_fastReplaceBtn == null)
            {
                GameObject go = CloneGameObject("template/Button_shengji", 86.0f, 8.0f);
                _fastReplaceBtn = go.GetComponent<KButton>();
                _fastReplaceBtn.onClick.AddListener(OnFastReplaceBtnClick);
            }
            _fastReplaceBtn.Visible = true;
        }

        private GameObject CloneGameObject(string path, float x = 0.0f, float y = 0.0f)
        {
            GameObject go = Instantiate(_parent.GetChild(path)) as GameObject;
            go.transform.SetParent(transform);
            go.transform.localScale = Vector3.one;
            go.transform.localPosition = new Vector3(x, y, 0);
            return go;
        }

        private void HideFastReplaceBtn()
        {
            if(_fastReplaceBtn != null)
            {
                _fastReplaceBtn.Visible = false;
            }
        }

        private void ShowItemGrid()
        {
            _itemGrid.Visible = true;
        }

        private void HideItemGrid()
        {
            _itemGrid.Visible = false;
        }

        private void RefreshTextNum(string content)
        {
            if(string.IsNullOrEmpty(content) == false)
            {
                _textNum.Visible = true;
                _textNum.CurrentText.text = content;
            }
            else
            {
                _textNum.Visible = false;
            }
        }

        public override void Dispose()
        {

        }
    }

    /**格子当前数据状态**/
    public enum BagGridState
    {
        /// <summary>
        /// 空格子
        /// </summary>
        Empty,
        /// <summary>
        /// 有物品的格子
        /// </summary>
        Occupied,
        /// <summary>
        /// 未解锁的格子
        /// </summary>
        Locked,
    }
}
