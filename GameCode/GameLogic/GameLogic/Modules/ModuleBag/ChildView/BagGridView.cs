﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Common.Base;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils;

using Game.UI.UIComponent;
using UnityEngine.Events;
using ModuleCommonUI;
using Common.Data;
using Common.Structs;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Common.ServerConfig;
using MogoEngine;
using GameMain.Entities;
using Common.Structs.Enum;

namespace ModuleBag
{
    public class BagGridView : KContainer
    {
        private KScrollPage _scrollPage;
        private StateText _textCapacity;
        private string _capacityTemplate;
        private KPageableList _contentList;

        private BagItemType _currentShowType = BagItemType.All;
        public BagItemType CurrentShowType { get { return _currentShowType; } }

        protected override void Awake()
        {
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_itemBag");
            _contentList = GetChildComponent<KPageableList>("ScrollPage_itemBag/mask/content");
            _textCapacity = _scrollPage.GetChildComponent<StateText>("capacity");
            _capacityTemplate = _textCapacity.CurrentText.text;

            InitScrollPage();
        }

        private void InitScrollPage()
        {
            //_scrollPage.CreatePagerByCoroutine = true;
            _contentList.SetPadding(15, 15, 15, 15);
            _contentList.SetGap(30, 32);
            _contentList.SetDirection(KList.KListDirection.LeftToRight, 7, 3);
        }

        public void RefreshContent()
        {
            int nowPage = _scrollPage.CurrentPage;
            ShowItemType();
            RefreshCapacity();
            _scrollPage.CurrentPage = nowPage;
        }

        public void ChangeItemCategory(int category)
        {
            _currentShowType = BagItemType.All;
            switch(category)
            {
                case 1:
                    _currentShowType = BagItemType.Equip;
                    break;
                case 2:
                    _currentShowType = BagItemType.Gem;
                    break;
                case 3:
                    _currentShowType = BagItemType.Token;
                    break;
                case 4:
                    _currentShowType = BagItemType.Material;
                    break;
            }
            RefreshContent();
            _scrollPage.CurrentPage = 0;
        }

        private void ShowItemType()
        {
            BagData bagData = PlayerDataManager.Instance.BagData.GetItemBagData();
            BaseItemInfo[] itemInfoArray = new BaseItemInfo[BagDataManager.MaxItemNum];
            if(_currentShowType == BagItemType.All)
            {
                Dictionary<int, BaseItemInfo> dict = bagData.GetAllItemInfo();
                foreach(KeyValuePair<int, BaseItemInfo> kvp in dict)
                {
                    //key是格子编号，服务器从1开始
                    itemInfoArray[kvp.Key - 1] = kvp.Value;
                }
            }
            else
            {
                List<BaseItemInfo> subList = bagData.GetTypeItemInfo(_currentShowType);
                for(int i = 0; i < subList.Count; i++)
                {
                    itemInfoArray[i] = subList[i];
                }
            }
            RefreshContentList(itemInfoArray);
        }

        private void RefreshContentList(BaseItemInfo[] array)
        {
            _scrollPage.TotalPage = Mathf.CeilToInt(array.Length / 21f);
            _contentList.SetDataList<BagItemGrid>(array, 7);
        }

        private void RefreshCapacity()
        {
            _textCapacity.CurrentText.text = string.Format(_capacityTemplate, PlayerDataManager.Instance.BagData.GetItemBagData().GetAllItemCount(), PlayerAvatar.Player.cur_bag_count);
        }

    }
}
