﻿

using Game.UI.UIComponent;
using Common.ServerConfig;
using Common.ExtendComponent;

namespace ModuleBag
{
    public class BagCurrencyView : KContainer
    {
        private MoneyItem _goldItem;
        private MoneyItem _diamondItem;
        private MoneyItem _ticketItem;


        protected override void Awake()
        {
            _goldItem = AddChildComponent<MoneyItem>("Container_buyGold");
            _diamondItem = AddChildComponent<MoneyItem>("Container_buyDiamond");
            _ticketItem = AddChildComponent<MoneyItem>("Container_buyTicket");

            InitCurrencyIcon();
        }

        private void InitCurrencyIcon()
        {
            _goldItem.SetMoneyType(public_config.MONEY_TYPE_GOLD);
            _diamondItem.SetMoneyType(public_config.MONEY_TYPE_DIAMOND);
            _ticketItem.SetMoneyType(public_config.MONEY_TYPE_BIND_DIAMOND);
        }

    }
}

