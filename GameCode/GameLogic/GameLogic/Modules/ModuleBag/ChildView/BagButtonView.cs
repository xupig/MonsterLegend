﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Common.Base;
using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils;

using Game.UI.UIComponent;
using UnityEngine.Events;

using ModuleCommonUI;
using Common.Utils;
using Common.Data;
using Common.ServerConfig;
using GameMain.GlobalManager.SubSystem;
using GameMain.GlobalManager;
using GameData;
using Common.ExtendComponent;

namespace ModuleBag
{
    public class BagButtonView : KContainer
    {
        private KButton _buttonDivide;
        private KShrinkableButton _buttonPolish;
        private KButton _buttonArrange;
        private KParticle _particle;

        protected override void Awake()
        {
            _buttonDivide = GetChildComponent<KButton>("Button_divide");
            _buttonPolish = GetChildComponent<KShrinkableButton>("Button_polish");
            _buttonArrange = GetChildComponent<KButton>("Button_arrane");
            _particle = _buttonPolish.AddChildComponent<KParticle>("fx_ui_lianjin_anniu");
            _particle.Stop();
            AddEventListener();
        }

        protected void AddEventListener()
        {
            _buttonDivide.onClick.AddListener(OnClickDivide);
            _buttonPolish.onClick.AddListener(OnClickPolish);
            _buttonArrange.onClick.AddListener(OnClickArrange);
            EventDispatcher.AddEventListener<int>(FunctionEvents.ADD_FUNTION, OnFunctionOpen);
        }

        protected void RemoveEventListener()
        {
            _buttonDivide.onClick.RemoveListener(OnClickDivide);
            _buttonPolish.onClick.RemoveListener(OnClickPolish);
            _buttonArrange.onClick.RemoveListener(OnClickArrange);
            EventDispatcher.RemoveEventListener<int>(FunctionEvents.ADD_FUNTION, OnFunctionOpen);
        }


        protected void OnClickDivide()
        {
            PanelIdEnum.EquipDecomposition.Show();
        }

        protected void OnClickPolish()
        {
            PanelIdEnum.Alchemy.Show();
        }

        protected void OnClickArrange()
        {
            BagManager.Instance.RequestSortBag(BagType.ItemBag);
        }

        private void RefreshButtonVisibility()
        {
            _buttonDivide.Visible = PlayerDataManager.Instance.FunctionData.IsFunctionOpen(FunctionId.equip);
        }

        protected override void OnEnable()
        {
            RefreshButtonVisibility();
            _buttonPolish.Visible = function_helper.IsFunctionOpen((int)FunctionId.alchemy);
            base.OnEnable();
        }

        private void OnFunctionOpen(int functionId)
        {
            if(functionId == (int)FunctionId.alchemy)
            {
                _buttonPolish.Visible = true;
            }
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            base.OnDestroy();
        }

    }
}
