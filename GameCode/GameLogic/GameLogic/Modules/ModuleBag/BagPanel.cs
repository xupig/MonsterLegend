﻿using System;
using System.Collections.Generic;
using System.Text;

using MogoEngine.Mgrs;
using MogoEngine.Events;
using Common.Base;
using Common.ClientConfig;
using Common.Events;
using Common.Structs;
using Game.UI.UIComponent;
using GameMain.Entities;
using Common.Data;
using Common.Structs.ProtoBuf;
using Common.ExtendComponent;
using Common.Utils;
using UnityEngine;
using GameMain.GlobalManager;
using Common.Global;
using GameMain.GlobalManager.SubSystem;

namespace ModuleBag
{
    public class BagPanel : BasePanel
    {
        private BagButtonView _buttonView;
        private CategoryList _categoryList;
        private BagGridView _gridView;
        private BagCurrencyView _currencyView;
        private KParticle _goldParticle;
        private ulong lastSortTimeStamp;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _buttonView = AddChildComponent<BagButtonView>("Container_btnContainer");
            _gridView = AddChildComponent<BagGridView>("Container_content");
            _currencyView = AddChildComponent<BagCurrencyView>("Container_currency");
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            _goldParticle = AddChildComponent<KParticle>("Container_panelBg/Container_xiaoniankaBG/fx_ui_17_jinbi_01");
            InitCategoryList();
        }

        private void InitCategoryList()
        {
            //TODO:中文表
            List<string> labelList = new List<string>() { MogoLanguageUtil.GetContent(9022), MogoLanguageUtil.GetContent(5002), MogoLanguageUtil.GetContent(5103), MogoLanguageUtil.GetContent(5116), MogoLanguageUtil.GetContent(5111) };
            List<int> iconIdList = new List<int>() { 11400, 11402, 11401, 11439, 11403 };
            _categoryList.SetDataList(labelList, iconIdList, 1);
            _categoryList.onSelectedIndexChanged.AddListener(OnCategoryChanged);
        }

        private void OnCategoryChanged(CategoryList toggleGroup, int index)
        {
            RefreshGridView(index);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Bag; }
        }

        public override void OnShow(object data)
        {
            _categoryList.SelectedIndex = 0;
            RefreshGridView(0);
            AddEventListener();
            _goldParticle.Stop();
            if(Global.serverTimeStamp - lastSortTimeStamp > 5000)
            {
                lastSortTimeStamp = Global.serverTimeStamp;
                //BagManager.Instance.RequestSortBag(BagType.ItemBag);
            }
        }

        private void PlaySellParticle()
        {
            _goldParticle.Play();
            UIManager.Instance.PlayAudio("Sound/UI/money_get.mp3");
        }

        private void RefreshGridView(int category)
        {
            _gridView.ChangeItemCategory(category);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(BagEvents.SellItem, PlaySellParticle);
            EventDispatcher.AddEventListener<BagType>(BagEvents.Init, this.InitBag);
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, this.UpdateItem);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.cur_bag_count, UpdateBagCount);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, OnPlayerLevelChange);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(BagEvents.SellItem, PlaySellParticle);
            EventDispatcher.RemoveEventListener<BagType>(BagEvents.Init, this.InitBag);
            EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, this.UpdateItem);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.cur_bag_count, UpdateBagCount);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, OnPlayerLevelChange);
        }

        private void InitBag(BagType bagType)
        {
            if(bagType == BagType.ItemBag)
            {
                _gridView.RefreshContent();
            }
        }

        private void UpdateItem(BagType bagType, uint position)
        {
            if (bagType == BagType.ItemBag || bagType == BagType.BodyEquipBag)
            {
                _gridView.RefreshContent();
            }
        }

        private void UpdateBagCount()
        {
            _gridView.RefreshContent();
        }

        private void OnPlayerLevelChange()
        {
            _gridView.RefreshContent();
        }
    }
}
