﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEquipUpgradeTips
{
    public class EquipUpgradeTipsModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.EquipUpgradeTips, "Container_EquipUpgradeTipsPanel", MogoUILayer.LayerUnderPanel, "ModuleEquipUpgradeTips.EquipUpgradeTipsPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }
    }
}
