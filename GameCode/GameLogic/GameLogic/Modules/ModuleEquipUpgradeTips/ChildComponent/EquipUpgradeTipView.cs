﻿#region 模块信息
/*==========================================
// 文件名：EquipUpgradeTip
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.EquipUpgradeTips
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/5/27 14:20:01
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.Enum;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using UnityEngine;

namespace ModuleEquipUpgradeTips
{
    public class EquipUpgradeTipView : KContainer
    {
        //面板存在时间
        private const uint LIVE_TIME = 305000;

        private uint timeId;
        private ItemGrid _itemGrid;
        private KButton _equipBtn;
        private KButton _useBtn;
        private StateText _name;
        private StateText _zhanli;
        private StateText _fightForceValue;
        private StateImage _arrowUpImage;
        private KButton _closeBtn;
        private KContainer _fightForceContainer;

        private EquipItemInfo _equipInfo;
        private CommonItemInfo _commonInfo;
        private string zhanliString;

        private Vector3 zhanliContainerLocalposition;

        protected override void Awake()
        {
            base.Awake();
            _fightForceContainer = GetChildComponent<KContainer>("Container_zhuangbeiXinxi/Container_zhanli");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _equipBtn = GetChildComponent<KButton>("Button_zhaungbei");
            _useBtn = GetChildComponent<KButton>("Button_shiyong");
            _name = GetChildComponent<StateText>("Container_zhuangbeiXinxi/Label_txtWupinmingcheng");
            _name.CurrentText.alignment = TextAnchor.UpperCenter;
            _fightForceValue = _fightForceContainer.GetChildComponent<StateText>("Label_txtZhandoulizhi");
            _zhanli = _fightForceContainer.GetChildComponent<StateText>("Label_txtZhanlimingzi");
            zhanliContainerLocalposition = _fightForceContainer.transform.localPosition;
            _arrowUpImage = _fightForceContainer.GetChildComponent<StateImage>("Image_jiangtouIcon");
            _closeBtn = GetChildComponent<KButton>("Button_close");
            zhanliString = _zhanli.CurrentText.text;
            AddEventListener();
        }

        private void AddEventListener()
        {
            _equipBtn.onClick.AddListener(PutOnEquip);
            _useBtn.onClick.AddListener(UseItem);
            _closeBtn.onClick.AddListener(CloseTips);
        }

        private void RemoveEventListener()
        {
            _equipBtn.onClick.RemoveListener(PutOnEquip);
            _useBtn.onClick.RemoveListener(UseItem);
            _closeBtn.onClick.RemoveListener(CloseTips);
        }

        private void CloseTips()
        {
            gameObject.SetActive(false);
            if (_equipInfo != null)
            {
                EventDispatcher.TriggerEvent(EquipUpgradeTipEvents.Close_Or_Use_Item);
            }
            if (_commonInfo != null)
            {
                EventDispatcher.TriggerEvent(EquipUpgradeTipEvents.Remove_Same_Item);
            }
            Refresh();
        }

        public void Refresh()
        {
            if (GameSceneManager.GetInstance().curMapType == public_config.MAP_TYPE_CITY || GameSceneManager.GetInstance().curMapType == public_config.MAP_TYPE_WILD)
            {
                BaseItemInfo baseItemInfo = PlayerDataManager.Instance.EquipUpgradeTipManager.GetCurrentItemInfo();
                if (baseItemInfo == null)
                {
                    if (timeId != 0)
                    {
                        TimerHeap.DelTimer(timeId);
                    }
                    this.Visible = false;
                }
                else
                {
                    this.Visible = true;
                    if (baseItemInfo is EquipItemInfo)
                    {
                        RefreshEquipView(baseItemInfo as EquipItemInfo);
                    }
                    else if (baseItemInfo is CommonItemInfo)
                    {
                        RefreshItemView(baseItemInfo as CommonItemInfo);
                    }
                    timeId = TimerHeap.AddTimer(LIVE_TIME, 0, Refresh);
                }
            }
        }

        private void RefreshEquipView(EquipItemInfo equipItemInfo)
        {
            DramaTrigger.EquipUpgrade(0);
            _equipInfo = equipItemInfo;
            int addedFightForce = 0;
            _fightForceContainer.Visible = true;
            _equipBtn.gameObject.SetActive(true);
            _useBtn.gameObject.SetActive(false);
            _zhanli.CurrentText.text = zhanliString;
            _name.CurrentText.text = equipItemInfo.Name;
            _itemGrid.Clear();
            _itemGrid.SetItemData(equipItemInfo);
            if (equipItemInfo.SubType == EquipType.ring)
            {
                addedFightForce = GetAddedFightForceCompareWithRing(equipItemInfo);
            }
            else
            {
                if (PlayerDataManager.Instance.BagData.GetEquipedItem(equipItemInfo.SubType) == null)
                {
                    addedFightForce = equipItemInfo.FightForce;
                }
                else
                {
                    addedFightForce = equipItemInfo.FightForce - PlayerDataManager.Instance.BagData.GetEquipedItem(equipItemInfo.SubType).FightForce;
                }
            }
            _fightForceValue.CurrentText.text = addedFightForce.ToString();
            AdjustFightForce(addedFightForce);
        }

        private int GetAddedFightForceCompareWithRing(EquipItemInfo equipItemInfo)
        {
            EquipItemInfo leftRing = PlayerDataManager.Instance.BagData.GetEquipedItem(public_config.EQUIP_POS_LEFTRING);
            EquipItemInfo rightRing = PlayerDataManager.Instance.BagData.GetEquipedItem(public_config.EQUIP_POS_RIGHTRING);

            if (leftRing == null)
            {
                return equipItemInfo.FightForce;
            }
            else if (rightRing == null)
            {
                return equipItemInfo.FightForce;
            }
            else
            {
                return equipItemInfo.FightForce - Mathf.Min(leftRing.FightForce, rightRing.FightForce);
            }
        }

        private void AdjustFightForce(int value)
        {
            float width = GetWidth(value);
            _arrowUpImage.GetComponent<RectTransform>().anchoredPosition = _fightForceValue.GetComponent<RectTransform>().anchoredPosition + new Vector2(width + 10.0f, 0);
            float totalWidth = width + 70f;
            _fightForceContainer.transform.localPosition = zhanliContainerLocalposition + new Vector3(80.0f - totalWidth / 2, 0, 0);
        }

        private float GetWidth(int value)
        {
            int digitNum = 0;
            while (value > 0)
            {
                value /= 10;
                digitNum++;
            }
            return digitNum * 12.0f;
        }

        private void RefreshItemView(CommonItemInfo commonItemInfo)
        {
            _commonInfo = commonItemInfo;
            _equipBtn.gameObject.SetActive(false);
            _useBtn.gameObject.SetActive(true);
            _name.CurrentText.text = commonItemInfo.Name;
            _fightForceContainer.Visible = false;
            _itemGrid.Clear();
            _itemGrid.SetItemData(commonItemInfo);
            _itemGrid.Context = PanelIdEnum.EquipUpgradeTips;
        }

        private void PutOnEquip()
        {
            int equipIndex = GetEquipIndex();
            BagManager.Instance.RequestWearEquip(_equipInfo.GridPosition, equipIndex, IsBodyEquipPositionOccupied(_equipInfo));
            if (timeId != 0)
            {
                TimerHeap.DelTimer(timeId);
            }
            EventDispatcher.TriggerEvent(EquipUpgradeTipEvents.Close_Or_Use_Item);
            Refresh();
        }

        private int GetEquipIndex()
        {
            if (_equipInfo.SubType != EquipType.ring)
            {
                return (int)_equipInfo.SubType;
            }
            else
            {
                EquipItemInfo leftRing = PlayerDataManager.Instance.BagData.GetEquipedItem(public_config.EQUIP_POS_LEFTRING);
                EquipItemInfo rightRing = PlayerDataManager.Instance.BagData.GetEquipedItem(public_config.EQUIP_POS_RIGHTRING);

                if (leftRing == null)
                {
                    return public_config.EQUIP_POS_LEFTRING;
                }
                else if (rightRing == null)
                {
                    return public_config.EQUIP_POS_RIGHTRING;
                }
                else
                {
                    return leftRing.FightForce <= rightRing.FightForce ? public_config.EQUIP_POS_LEFTRING : public_config.EQUIP_POS_RIGHTRING;
                }
            }
        }

        private void UseItem()
        {
            if (_commonInfo.Type == BagItemType.TreasureMap)
            {
                TreasureManager.Instance.UseTreasureMap(_commonInfo.Id);
                EventDispatcher.TriggerEvent(EquipUpgradeTipEvents.Remove_Same_Item);
            }
            else
            {
                int gridPosition = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemPosition(_commonInfo.Id);
                BagManager.Instance.RequestUseItem(gridPosition, 1);
                EventDispatcher.TriggerEvent(EquipUpgradeTipEvents.Close_Or_Use_Item);
            }
            if (timeId != 0)
            {
                TimerHeap.DelTimer(timeId);
            }
            Refresh();
        }

        private bool IsBodyEquipPositionOccupied(EquipItemInfo info)
        {
            BagData bodyEquipData = PlayerDataManager.Instance.BagData.GetBodyEquipBagData();
            if (info.SubType == EquipType.ring)
            {
                return bodyEquipData.GetItemInfo(public_config.EQUIP_POS_LEFTRING) != null && bodyEquipData.GetItemInfo(public_config.EQUIP_POS_RIGHTRING) != null;
            }
            return bodyEquipData.GetItemInfo((int)info.SubType) != null;
        }

        public void Hide()
        {
            if (Visible == true)
            {
                Visible = false;
            }
        }
    }
}