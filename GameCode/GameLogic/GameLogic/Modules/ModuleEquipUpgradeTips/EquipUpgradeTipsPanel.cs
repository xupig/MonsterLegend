﻿using Common.Base;
using Common.Events;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquipUpgradeTips
{
    public class EquipUpgradeTipsPanel : BasePanel
    {
        private EquipUpgradeTipView _equipUpgradeTip;


        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.EquipUpgradeTips; }
        }

        protected override void Awake()
        {
            _equipUpgradeTip = AddChildComponent<EquipUpgradeTipView>("Container_zhuangbeiTips");
            _equipUpgradeTip.Visible = false;
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            ShowEquipView();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(EquipUpgradeTipEvents.Hide_Equip, HideEquipUpgradeTips);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(EquipUpgradeTipEvents.Hide_Equip, HideEquipUpgradeTips);
        }

        private void HideEquipUpgradeTips()
        {
            _equipUpgradeTip.Visible = false;
        }

        private void ShowEquipView()
        {
            _equipUpgradeTip.Visible = true;
            _equipUpgradeTip.Refresh();
        }

        public override void OnLeaveMap(int mapType)
        {
            ClosePanel();
        }
    }
}
