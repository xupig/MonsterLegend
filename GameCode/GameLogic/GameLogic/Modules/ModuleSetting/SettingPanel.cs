﻿using System;
using System.Collections.Generic;
using Common.Base;
using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Events;
using UnityEngine;
using GameData;

namespace ModuleSetting
{
    public class SettingPanel : BasePanel
    {
        private SystemSettingView _systemSettingView;
        private PushSettingView _pushSettingView;
        private ControlSettingView _controlSettingView;
        private KToggleGroup _titleToggleGroup;
        private KToggle _controlSettingToggle;

        private const int SYSTEM_SETTING = 0;
        private const int PUSH_SETTING = 1;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close02");
            _systemSettingView = AddChildComponent<SystemSettingView>("Container_xitongshezhi");
            _pushSettingView = AddChildComponent<PushSettingView>("Container_tuisongshezhi");
            _controlSettingView = AddChildComponent<ControlSettingView>("Container_caozuoshezhi");
            _titleToggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            _controlSettingToggle = _titleToggleGroup.GetChildComponent<KToggle>("Toggle_caozuoshezhi");
            InitTitleToggleGroup();
        }

        private void InitTitleToggleGroup()
        {
            _controlSettingToggle.Visible = platform_helper.InPCPlatform();
            DoTabLayout(_titleToggleGroup);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Setting; }
        }

        public override void OnShow(object data)
        {
            _systemSettingView.Visible = true;
            _pushSettingView.Visible = false;
            _controlSettingView.Visible = false;
            _titleToggleGroup.SelectIndex = SYSTEM_SETTING;
            AddEventListener();
        }

        public override void OnClose()
        {
            SettingData settingData = PlayerDataManager.Instance.SettingData;
            settingData.SaveSettingData();
            if (settingData.IsPushSettingChange)
            {
                settingData.SetupNotificationData();
                settingData.IsPushSettingChange = false;
            }
            _pushSettingView.DoRequestServerPushChanged();
            SetUIButtonAudioVolume();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _titleToggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
        }

        private void RemoveEventListener()
        {
            _titleToggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
        }

        private void OnSelectedIndexChanged(KToggleGroup toggleGroup, int index)
        {
            if (index == SYSTEM_SETTING)
            {
                _systemSettingView.Visible = true;
                _pushSettingView.Visible = false;
                _controlSettingView.Visible = false;
            }
            else if (index == PUSH_SETTING)
            {
                _systemSettingView.Visible = false;
                _pushSettingView.Visible = true;
                _controlSettingView.Visible = false;
            }
            else
            {
                _systemSettingView.Visible = false;
                _pushSettingView.Visible = false;
                _controlSettingView.Visible = true;
                _controlSettingView.Refresh();
            }
        }

        private void SetUIButtonAudioVolume()
        {
            int settingVolume = PlayerDataManager.Instance.SettingData.GetSettingValue(SettingType.Sound);
            KComponentAudioCollective.audioVolume = settingVolume / 100f;
        }
    }
}
