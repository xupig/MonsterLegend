﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Game.UI.UIComponent;
using Common.ExtendComponent;
using Common.Data;
using Game.UI;
using GameMain.GlobalManager;

namespace ModuleSetting
{
    public class PushSettingItem : KList.KListItemBase
    {
        private PushItemData _data;
        private StateText _nameText;
        private StateText _descText;
        private KToggle _toggleSelect;

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PushItemData;
                Refresh();
            }
        }

        protected override void Awake()
        {
            _nameText = GetChildComponent<StateText>("Label_txtHuodongmingcheng");
            _descText = GetChildComponent<StateText>("Label_txtHuodongshijian");
            _toggleSelect = GetChildComponent<KToggle>("Toggle_Xuanze");
            _toggleSelect.onValueChanged.AddListener(OnSelectValueChanged);
        }

        public override void Dispose()
        {
            _toggleSelect.onValueChanged.RemoveListener(OnSelectValueChanged);
            _data = null;
        }

        protected void Refresh()
        {
            _nameText.CurrentText.text = _data.Name;
            _descText.CurrentText.text = _data.Desc;
            bool isSelected = PlayerDataManager.Instance.SettingData.IsPushTypeSelected(_data.Id);
            if (_toggleSelect.isOn != isSelected)
            {
                _toggleSelect.isOn = isSelected;
            }
        }

        private void OnSelectValueChanged(KToggle toggle, bool isSelected)
        {
            //设置值
            PlayerDataManager.Instance.SettingData.SetPushValue(_data.Id, isSelected);
            //Debug.LogError("push item type id = " + _data.Id.ToString() + "  is selected = " + PlayerDataManager.Instance.SettingData.IsPushTypeSelected(_data.Id));
        }
    }
}
