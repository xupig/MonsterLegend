﻿#region 模块信息
/*==========================================
// 文件名：ControlItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleSetting.ChildComponent
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/4/11 20:28:01
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
namespace ModuleSetting
{
    public class ControlItem : KList.KListItemBase
    {
        private int _mode;
        private KButton _button;
        private StateText _showTxt;

        public override object Data
        {
            get
            {
                return _mode;
            }
            set
            {
                _mode = (int)value;
                Refresh();
            }
        }

        private void Refresh()
        {
            switch(_mode)
            {
                case 1:
                    _showTxt.CurrentText.text = "1、2、3、4";
                    break;
                case 2:
                    _showTxt.CurrentText.text = "Q、W、E、R";
                    break;
            }
        }

        protected override void Awake()
        {
            _showTxt = GetChildComponent<StateText>("Button_item/label");
            _button = GetChildComponent<KButton>("Button_item");
            _button.onClick.AddListener(OnButtonClick);
        }

        private void OnButtonClick()
        {
            onClick.Invoke(this, Index);
        }

        public override void Dispose()
        {

        }

        public string ShowString
        {
            get
            {
                return _showTxt.CurrentText.text;
            }
        }

    }
}