﻿using System;
using System.Collections.Generic;
using Common.Base;

namespace ModuleSetting
{
    public class SettingModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.Setting, "Container_SettingPanel", MogoUILayer.LayerUIPanel, "ModuleSetting.SettingPanel", BlurUnderlay.Have, HideMainUI.Hide);
        }
    }
}
