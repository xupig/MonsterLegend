﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using Common.Data;
using GameMain.GlobalManager;
using Common.ExtendComponent;
using GameMain.GlobalManager.SubSystem;
using Common.ServerConfig;
using GameData;

namespace ModuleSetting
{
    public class PushSettingView : KContainer
    {
        private KList _contentList;
        private Dictionary<int, bool> _curPushSettingValueDict;

        protected override void Awake()
        {
            _contentList = GetChildComponent<KList>("ScrollView_tuisongshezhi/mask/content");
            InitScrollView();
            InitPushValueDict();
            RefreshContentList();
        }

        private void InitScrollView()
        {
            _contentList.SetPadding(5, 6, 5, 15);
            _contentList.SetGap(-5, 1);
            _contentList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        private void InitPushValueDict()
        {
            _curPushSettingValueDict = new Dictionary<int, bool>();
            Dictionary<int, bool> pushSettingDict = PlayerDataManager.Instance.SettingData.PushSettingDict;
            foreach (var item in pushSettingDict)
            {
                _curPushSettingValueDict.Add(item.Key, item.Value);
            }
        }

        private void RefreshContentList()
        {
            List<PushItemData> pushItemDatas = new List<PushItemData>();
            Dictionary<int, bool> pushSettingDict = PlayerDataManager.Instance.SettingData.PushSettingDict;
            foreach (var item in pushSettingDict)
            {
                PushItemData data = new PushItemData(item.Key);
                pushItemDatas.Add(data);
            }
            _contentList.SetDataList<PushSettingItem>(pushItemDatas);
        }

        protected override void OnEnable()
        {
            TweenListEntry.Begin(_contentList.gameObject);
        }

        public void DoRequestServerPushChanged()
        {
            //如果为null，也就是还没有初始化该数据，即没有切换到该分页，数据也不会改变
            if (_curPushSettingValueDict == null)
            {
                return;
            }
            Dictionary<int, bool> pushSettingDict = PlayerDataManager.Instance.SettingData.PushSettingDict;
            foreach (var item in _curPushSettingValueDict)
            {
                int pushId = item.Key;
                bool isPush = item.Value;
                if (push_helper.IsServerPush(pushId) && isPush != pushSettingDict[pushId])
                {
                    int serverPushType = push_helper.GetServerPushType(pushId);
                    SettingManager.Instance.RequestPushSettingChanged(serverPushType, pushSettingDict[pushId]);
                }
            }
        }

        protected void AddEventListener()
        {

        }

        protected void RemoveEventListener()
        {

        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }
    }
}
