﻿#region 模块信息
/*==========================================
// 文件名：ControlSettingView
// 命名空间: GameLogic.GameLogic.Modules.ModuleSetting.ChildView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/4/11 19:37:46
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
namespace ModuleSetting
{
    public class ControlSettingView : KContainer
    {
        private KToggleGroup _settingToggleGroup;
        private SettingComponent _leftComponent;
        private SettingComponent _rightComponent;

        protected override void Awake()
        {
            _settingToggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_xuanze");
            _leftComponent = AddChildComponent<SettingComponent>("Container_left");
            _rightComponent = AddChildComponent<SettingComponent>("Container_right");
        }

        public void Refresh()
        {
            _leftComponent.SetControlMode(1);
            _rightComponent.SetControlMode(2);
            _settingToggleGroup.SelectIndex = KeyboardManager.GetInstance().GetChosenMode();
        }

        protected override void OnDisable()
        {
            PlayerAvatar.Player.RpcCall("control_setting_save", 
                (byte)_settingToggleGroup.SelectIndex + 1,  // 1 是 页游， 2 是 LOL
                (byte)_rightComponent.GetControlMode(),
                (byte)_leftComponent.GetControlMode());
        }
    }
}