﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Game.UI.UIComponent;
using Common.Data;
using GameMain.GlobalManager;
using Common.Utils;
using Common.ExtendComponent;
using Game.UI;
using ModuleCommonUI;
using GameLoader.PlatformSdk;

namespace ModuleSetting
{
    public class SystemSettingView : KContainer
    {
        private KSlider _musicSlider;
        private KSlider _soundSlider;
        private KSlider _speechSlider;
        private KSlider _playerCountSlider;
        private StateText _playerCountText;
        private KToggleGroup _renderQualityToggleGroup;
        private Dictionary<ChatSpeechSettingType, KToggle> _chatSpeechToggleDict = null;
        private KButton _reloginBtn;  //重新登录
        //private KButton _returnLoginBtn;  //返回登陆
        private KButton _customServiceBtn;  //联系客服
        private KContainer _leftContainer;
        private KContainer _rightContainer;

        private SettingData _settingData = null;

        protected override void Awake()
        {
            _settingData = PlayerDataManager.Instance.SettingData;

            _musicSlider = GetChildComponent<KSlider>("Container_left/Slider_yinyue");
            _soundSlider = GetChildComponent<KSlider>("Container_left/Slider_yinxiao");
            _speechSlider = GetChildComponent<KSlider>("Container_left/Slider_yuyin");
            _playerCountSlider = GetChildComponent<KSlider>("Container_left/Slider_wanjiashuliang");
            _renderQualityToggleGroup = GetChildComponent<KToggleGroup>("Container_left/ToggleGroup_huazhi");
            _playerCountText = GetChildComponent<StateText>("Container_left/Slider_wanjiashuliang/txtCount");
            _reloginBtn = GetChildComponent<KButton>("Button_chongxindenglu");
            _customServiceBtn = GetChildComponent<KButton>("Button_lianxikefu");
            _leftContainer = GetChildComponent<KContainer>("Container_left");
            _rightContainer = GetChildComponent<KContainer>("Container_right");
            SetReloginBtnVisible();
            InitChatSpeechToggles();
            SetSlidersScaleDir();
            InitWidgetsValue();
            
            AddEventListener();
        }

        private void SetReloginBtnVisible()
        {
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WindowsPlayer)
            {
                _reloginBtn.Visible = true;
            }
            else if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                _reloginBtn.Visible = false;
            }
            else
            {
                _reloginBtn.Visible = true;
            }
        }

        private void InitChatSpeechToggles()
        {
            _chatSpeechToggleDict = new Dictionary<ChatSpeechSettingType, KToggle>();
            KToggle toggle = GetChildComponent<KToggle>("Container_right/Toggle_gonghui");
            _chatSpeechToggleDict.Add(ChatSpeechSettingType.Guild, toggle);
            toggle = GetChildComponent<KToggle>("Container_right/Toggle_duiwu");
            _chatSpeechToggleDict.Add(ChatSpeechSettingType.Team, toggle);
            toggle = GetChildComponent<KToggle>("Container_right/Toggle_shijie");
            _chatSpeechToggleDict.Add(ChatSpeechSettingType.World, toggle);
        }

        private void InitWidgetsValue()
        {
            _musicSlider.value = CalculateSoundVolume(SettingType.Music);
            _soundSlider.value = CalculateSoundVolume(SettingType.Sound);
            _speechSlider.value = CalculateSoundVolume(SettingType.Speech);
            _playerCountSlider.value = (float)_settingData.GetSettingValue(SettingType.PlayerCount) / (float)SettingData.MAX_SHOW_PLAYER_COUNT;
            int curPlayerCount = _settingData.GetSettingValue(SettingType.PlayerCount);
            RefreshPlayerCountText(curPlayerCount);
            _renderQualityToggleGroup.SelectIndex = _settingData.GetSettingValue(SettingType.RenderQuality) - 1;
            foreach (var item in _chatSpeechToggleDict)
            {
                KToggle speechToggle = item.Value;
                speechToggle.isOn = _settingData.IsChatSpeechTypeSelected(item.Key);
            }
        }

        private void SetSlidersScaleDir()
        {
            UIUtils.SetSliderScaleDir(_musicSlider, UVScaleImage.UVScaleDirection.Forward);
            UIUtils.SetSliderScaleDir(_soundSlider, UVScaleImage.UVScaleDirection.Forward);
            UIUtils.SetSliderScaleDir(_speechSlider, UVScaleImage.UVScaleDirection.Forward);
            UIUtils.SetSliderScaleDir(_playerCountSlider, UVScaleImage.UVScaleDirection.Forward);
        }

        protected void AddEventListener()
        {
            _musicSlider.onValueChanged.AddListener(OnMusicSliderChange);
            _soundSlider.onValueChanged.AddListener(OnSoundSliderChange);
            _speechSlider.onValueChanged.AddListener(OnSpeechSliderChange);
            _playerCountSlider.onValueChanged.AddListener(OnPlayerCountSliderChange);
            _renderQualityToggleGroup.onSelectedIndexChanged.AddListener(OnRenderQualitySelectedChanged);
            _reloginBtn.onClick.AddListener(OnClickReloginBtn);
            _customServiceBtn.onClick.AddListener(OnClickCustomServiceBtn);
            foreach (var item in _chatSpeechToggleDict)
            {
                KToggle toggle = item.Value;
                toggle.onValueChanged.AddListener(OnChatSpeechValueChanged);
            }
        }

        protected void RemoveEventListener()
        {
            _musicSlider.onValueChanged.RemoveListener(OnMusicSliderChange);
            _soundSlider.onValueChanged.RemoveListener(OnSoundSliderChange);
            _speechSlider.onValueChanged.RemoveListener(OnSpeechSliderChange);
            _playerCountSlider.onValueChanged.RemoveListener(OnPlayerCountSliderChange);
            _renderQualityToggleGroup.onSelectedIndexChanged.RemoveListener(OnRenderQualitySelectedChanged);
            _reloginBtn.onClick.RemoveListener(OnClickReloginBtn);
            _customServiceBtn.onClick.RemoveListener(OnClickCustomServiceBtn);
            foreach (var item in _chatSpeechToggleDict)
            {
                KToggle toggle = item.Value;
                toggle.onValueChanged.RemoveListener(OnChatSpeechValueChanged);
            }
        }

        protected override void OnEnable()
        {
            TweenViewMove.Begin(_leftContainer.gameObject, MoveType.Show, MoveDirection.Left);
            TweenViewMove.Begin(_rightContainer.gameObject, MoveType.Show, MoveDirection.Right);
        }

        private void OnMusicSliderChange(float value)
        {
            MogoEngine.Events.EventDispatcher.TriggerEvent<float>(Common.Events.BgMusicEvent.CHANGE_MUSIC_VOLUME, value);
            SetSoundVolume(SettingType.Music, value);
        }

        private void OnSoundSliderChange(float value)
        {
            ACTSystem.ACTSystemDriver.GetInstance().soundVolume = value;
            SetSoundVolume(SettingType.Sound, value);
        }

        private void OnSpeechSliderChange(float value)
        {
            SetSoundVolume(SettingType.Speech, value);
        }

        private void OnPlayerCountSliderChange(float value)
        {
            int sliderValue = (int)(value * SettingData.MAX_SHOW_PLAYER_COUNT);
            _settingData.SetSettingValue(SettingType.PlayerCount, sliderValue);
            RefreshPlayerCountText(sliderValue);
        }

        private void OnRenderQualitySelectedChanged(KToggleGroup toggleGroup, int index)
        {
            _settingData.SetSettingValue(SettingType.RenderQuality, index + 1);
        }

        private void OnChatSpeechValueChanged(KToggle toggle, bool isSelected)
        {
            foreach (var item in _chatSpeechToggleDict)
            {
                if (item.Value == toggle)
                {
                    _settingData.SetChatSpeechValue(item.Key, isSelected);
                    break;
                }
            }
        }

        private void OnClickReloginBtn()
        {
            MessageBox.Show(true, MogoLanguageUtil.GetContent(6085008), MogoLanguageUtil.GetContent(73067), OnConfirmRelogin, OnCancelRelogin, "", "");
        }

        private void OnConfirmRelogin()
        {
            PlatformSdkMgr.Instance.RestartGame();
        }

        private void OnCancelRelogin()
        {

        }

        private void OnClickCustomServiceBtn()
        {
            _settingData.ShowCustomServiceBox();
        }

        private void RefreshPlayerCountText(int curPlayerCount)
        {
            string strAll = MogoLanguageUtil.GetContent(56122);
            _playerCountText.CurrentText.text = curPlayerCount < SettingData.MAX_SHOW_PLAYER_COUNT ? curPlayerCount.ToString() : strAll;
        }

        private float CalculateSoundVolume(SettingType settingType)
        {
            float result = _settingData.GetSettingValue(settingType);
            result = result / 100f;
            return result;
        }

        private void SetSoundVolume(SettingType settingType, float volume)
        {
            int value = (int)(volume * 100);
            _settingData.SetSettingValue(settingType, value);
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
            _chatSpeechToggleDict.Clear();
            _settingData = null;
            base.OnDestroy();
        }
    }
}
