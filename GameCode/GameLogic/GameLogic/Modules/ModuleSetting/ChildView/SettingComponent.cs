﻿#region 模块信息
/*==========================================
// 文件名：SettingComponent
// 命名空间: GameLogic.GameLogic.Modules.ModuleSetting.ChildView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/4/11 19:40:26
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using System.Collections.Generic;
namespace ModuleSetting
{
    public class SettingComponent : KContainer
    {
        private KContainer _showContainer;
        private StateImage _upArrowImage;
        private StateImage _downArrowImage;
        private StateText _currentChooseTxt;
        private KContainer _chooseKeyContainer;
        private KList _chooseKeyList;
        private ControlItem _currentChosenItem = null;
        private KDummyButton _dummyBtn;
        private int _mode;

        private bool toggleState = false;

        protected override void Awake()
        {
            _showContainer = GetChildComponent<KContainer>("Container_chooseKey");
            _dummyBtn = _showContainer.gameObject.AddComponent<KDummyButton>();
            _upArrowImage = _showContainer.GetChildComponent<StateImage>("Image_shangjiantou");
            _upArrowImage.Visible = false;
            _downArrowImage = _showContainer.GetChildComponent<StateImage>("Image_xiajiantou");
            _currentChooseTxt = _showContainer.GetChildComponent<StateText>("Label_anjianTxt");
            _chooseKeyContainer = GetChildComponent<KContainer>("Container_shaixuanxiang");
            _chooseKeyList = _chooseKeyContainer.GetChildComponent<KList>("List_content");
            InitChooseKeyList();
        }

        private void InitChooseKeyList()
        {
            _chooseKeyList.Visible = true;
            _chooseKeyList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _chooseKeyList.SetDataList<ControlItem>(new List<int> { 1, 2 });
            _chooseKeyContainer.Visible = false;
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _dummyBtn.onClick.AddListener(OpenChooseKeyList);
            _chooseKeyList.onItemClicked.AddListener(OnKeyListClicked);
        }

        private void RemoveEventListener()
        {
            _dummyBtn.onClick.AddListener(OpenChooseKeyList);
            _chooseKeyList.onItemClicked.RemoveListener(OnKeyListClicked);
        }

        private void OnKeyListClicked(KList arg0, KList.KListItemBase item)
        {
            ControlItem controlItem = item as ControlItem;
            if (item == _currentChosenItem)
            {

            }
            else
            {
                _currentChosenItem = controlItem;
                _mode = (int)_currentChosenItem.Data;
                _currentChooseTxt.CurrentText.text = _currentChosenItem.ShowString;
            }
            OpenChooseKeyList();
        }

        private void OpenChooseKeyList()
        {
            toggleState = !toggleState;
            _upArrowImage.Visible = toggleState;
            _downArrowImage.Visible = !toggleState;
            _chooseKeyContainer.Visible = toggleState;
        }

        public void SetControlMode(int mode)
        {
            switch (mode)
            {
                case 1:
                    _mode = PlayerAvatar.Player.control_setting_web_player == 0 ? 1 : (int)PlayerAvatar.Player.control_setting_web_player;
                    _currentChooseTxt.CurrentText.text = KeyboardManager.GetInstance().GetWebPlayerControlMode();
                    break;
                case 2:
                    _mode = PlayerAvatar.Player.control_setting_lol_move == 0 ? 1 : (int)PlayerAvatar.Player.control_setting_lol_move;
                    _currentChooseTxt.CurrentText.text = KeyboardManager.GetInstance().GetLOLPlayerControlMode();
                    break;
            }
        }

        public int GetControlMode()
        {
            return _mode;
        }
    }
}