﻿using System;
using System.Collections.Generic;
using Common.Base;

namespace ModuleEnergyBuying
{
    public class EnergyBuyingModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.EnergyBuying, "Container_EnergyBuyingPanel", MogoUILayer.LayerUIPanel, "ModuleEnergyBuying.EnergyBuyingPanel", BlurUnderlay.Have, HideMainUI.Hide);
        }
    }
}
