﻿using Game.UI.UIComponent;
using GameMain.Entities;
using Common.Data;
using GameMain.GlobalManager;
using Common.Global;
using GameData;
using Common.Utils;
using MogoEngine.Events;
using Common.Events;
using UnityEngine.UI;
using Common.ClientConfig;
using MogoEngine.Mgrs;
using Common.ServerConfig;
using UnityEngine;
using ModuleCommonUI;
using Common.Base;
using Common.ExtendComponent;
using Game.UI;

namespace ModuleEnergyBuying
{
    public class EnergyBuyingMainView : KContainer
    {
        private StateText _remainBuyTimes;
        private KProgressBar _energyProgBar;
        private StateText _energyNumText;
        private StateText _resetTimesTips;
        private KButton _buyOnceBtn;
        private KButton _buyAllBtn;
        private MoneyItem _diamondItem;
        private MoneyItem _bindDiamondItem;

        private EnergyBuyingData _energyBuyData;
        private int _costMoneyId = 0;
        private BaseItemData _buyOnceCostItem;
        private BaseItemData _buyAllCostItem;
        private Vector2 _originBuyOnceBtnPos = Vector2.zero;

        protected override void Awake()
        {
            _energyBuyData = PlayerDataManager.Instance.EnergyBuyingData;
            InitResetTimesTips();
            _remainBuyTimes = GetChildComponent<StateText>("Label_txtGoumaicishu");
            _energyProgBar = GetChildComponent<KProgressBar>("ProgressBar_jindu");
            _energyNumText = GetChildComponent<StateText>("Label_txtTili");
            _buyOnceBtn = GetChildComponent<KButton>("Button_goumai");
            _buyAllBtn = GetChildComponent<KButton>("Button_goumaiTenTimes");
            _diamondItem = AddChildComponent<MoneyItem>("Container_buyGold");
            _bindDiamondItem = AddChildComponent<MoneyItem>("Container_buyDiamond");
            _diamondItem.SetMoneyType(public_config.MONEY_TYPE_DIAMOND);
            _bindDiamondItem.SetMoneyType(public_config.MONEY_TYPE_BIND_DIAMOND);
            _costMoneyId = _energyBuyData.GetCostItem(0).Id;
            RectTransform rect = _buyOnceBtn.GetComponent<RectTransform>();
            _originBuyOnceBtnPos = rect.anchoredPosition;
            SetProgressBarScaleDir();
        }

        private void InitResetTimesTips()
        {
            _resetTimesTips = GetChildComponent<StateText>("Label_txtResetTips");
            _resetTimesTips.CurrentText.text = MogoLanguageUtil.GetContent(98001);
        }

        private void SetProgressBarScaleDir()
        {
            UIUtils.SetProgressBarScaleDir(_energyProgBar, UVScaleImage.UVScaleDirection.Forward);
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _buyOnceBtn.onClick.AddListener(OnClickBuyOnceBtn);
            _buyAllBtn.onClick.AddListener(OnClickBuyAllBtn);
            EventDispatcher.AddEventListener(EnergyBuyingEvents.UPDATE_BUY_TIMES, OnRefreshContent);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.energy, OnRefreshEnergyInfo);
            if (_costMoneyId == public_config.MONEY_TYPE_DIAMOND || _costMoneyId == public_config.MONEY_TYPE_BIND_DIAMOND)
            {
                EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_diamond, OnRefreshBuyBtnsInfo);
                EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_bind_diamond, OnRefreshBuyBtnsInfo);
            }
            else
            {
                EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_gold, OnRefreshBuyBtnsInfo);
            }
        }

        private void RemoveEventListener()
        {
            _buyOnceBtn.onClick.RemoveListener(OnClickBuyOnceBtn);
            _buyAllBtn.onClick.RemoveListener(OnClickBuyAllBtn);
            EventDispatcher.RemoveEventListener(EnergyBuyingEvents.UPDATE_BUY_TIMES, OnRefreshContent);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.energy, OnRefreshEnergyInfo);
            if (_costMoneyId == public_config.MONEY_TYPE_DIAMOND || _costMoneyId == public_config.MONEY_TYPE_BIND_DIAMOND)
            {
                EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_diamond, OnRefreshBuyBtnsInfo);
                EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_bind_diamond, OnRefreshBuyBtnsInfo);
            }
            else
            {
                EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.money_gold, OnRefreshBuyBtnsInfo);
            }
        }

        protected override void OnDestroy()
        {
        }

        private void OnClickBuyOnceBtn()
        {
            if (_energyBuyData.BuyOnceStatus == (int)BuyEnergyStatus.ReachEnergyLimit)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(98009), PanelIdEnum.MainUIField);
            }
            else
            {
                EventDispatcher.TriggerEvent<int, BaseItemData, int>(EnergyBuyingEvents.SHOW_BUY_ENERGY_CONFIRM_VIEW, EnergyBuyingData.BUY_TYPE_ONCE,
                _buyOnceCostItem, GlobalParams.GetBuyOnceEnergyNum());
            }
        }

        private void OnClickBuyAllBtn()
        {
            EventDispatcher.TriggerEvent<int, BaseItemData, int>(EnergyBuyingEvents.SHOW_BUY_ENERGY_CONFIRM_VIEW, EnergyBuyingData.BUY_TYPE_ALL,
                _buyAllCostItem, _energyBuyData.GetBuyAllEnergyNum());
        }

        private void OnClickChargeBtn()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.Charge);
        }

        private void OnRefreshContent()
        {
            Refresh();
        }

        private void OnRefreshEnergyInfo()
        {
            RefreshEnergyInfo();
            if (_energyBuyData.IsCurEnergyChanged())
            {
                RefreshBuyButtonsInfo();
            }
        }

        private void OnRefreshBuyBtnsInfo()
        {
            if (_energyBuyData.IsCurOwnMoneyChanged())
            {
                RefreshBuyButtonsInfo();
            }
        }

        public void Refresh()
        {
            _buyOnceCostItem = _energyBuyData.GetBuyOnceCostItem();
            _buyAllCostItem = _energyBuyData.GetBuyAllCostItem();
            RefreshRemainBuyTimes();
            RefreshEnergyInfo();
            RefreshBuyButtonsInfo();
        }
      
        private void RefreshRemainBuyTimes()
        {
            int curVip = PlayerAvatar.Player.vip_level;
            int nextVip = curVip + 1;
            int maxVip = XMLManager.vip.Count;
            if (nextVip > maxVip)
            {
                nextVip = maxVip;
            }
            string strBuyTimes = vip_rights_helper.GetVipRights(VipRightType.EnergyBuyTimes, nextVip);
            _remainBuyTimes.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(98000), _energyBuyData.RemainBuyTimes, strBuyTimes);
        }

        private void RefreshEnergyInfo()
        {
            _energyNumText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(98015), PlayerAvatar.Player.energy, GlobalParams.GetFullEnergy());
            _energyProgBar.Value = (float)PlayerAvatar.Player.energy / GlobalParams.GetFullEnergy();
        }

        private void RefreshBuyButtonsInfo()
        {
            StateText buyOnceNumText = _buyOnceBtn.GetChildComponent<StateText>("labelEnergyNum");
            StateText buyAllNumText = _buyAllBtn.GetChildComponent<StateText>("labelEnergyNum");
            StateText buyAllTitle = _buyAllBtn.GetChildComponent<StateText>("label");
            int energyNum = _energyBuyData.GetBuyOnceEnergyNum();
            RefreshBuyBtnText(buyOnceNumText, GetBuyBtnNumStrText(_energyBuyData.BuyOnceStatus, energyNum));
            energyNum = _energyBuyData.GetBuyAllEnergyNum();
            RectTransform buyOnceBtnRect = _buyOnceBtn.GetComponent<RectTransform>();
            KShrinkableButton shrinkBuyOnceBtn = _buyOnceBtn as KShrinkableButton;
            //当仅能买一次体力或不够条件买时，就不显示全部购买按钮
            if (energyNum / GlobalParams.GetBuyOnceEnergyNum() <= 1)
            {
                _buyAllBtn.Visible = false;
                buyOnceBtnRect.anchoredPosition = new Vector2(529f, _originBuyOnceBtnPos.y);
                shrinkBuyOnceBtn.RefreshRectTransform();
            }
            else
            {
                _buyAllBtn.Visible = true;
                RefreshBuyBtnText(buyAllNumText, GetBuyBtnNumStrText(_energyBuyData.BuyAllStatus, energyNum));
                RefreshBuyBtnText(buyAllTitle, MogoLanguageUtil.GetContent(98002));
                buyOnceBtnRect.anchoredPosition = _originBuyOnceBtnPos;
                shrinkBuyOnceBtn.RefreshRectTransform();
            }
        }

        private void RefreshBuyBtnText(StateText buyBtnText, string strText)
        {
            Text[] texts = buyBtnText.GetAllStateTextComponent();
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].text = strText;
            }
        }

        private string GetBuyBtnNumStrText(int buyStatus, int energyNum)
        {
            string strText = "";
            if (buyStatus == (int)BuyEnergyStatus.CanBuy)
            {
                strText = string.Format(MogoLanguageUtil.GetContent(98003), energyNum);
            }
            else if (buyStatus == (int)BuyEnergyStatus.NotHaveBuyTimes)
            {
                strText = MogoLanguageUtil.GetContent(98004);
            }
            else if (buyStatus == (int)BuyEnergyStatus.ReachEnergyLimit)
            {
                strText = MogoLanguageUtil.GetContent(98005);
            }
            else if (buyStatus == (int)BuyEnergyStatus.NotEnoughMoney)
            {
                strText = MogoLanguageUtil.GetContent(98006);
            }
            return strText;
        }

    }
}
