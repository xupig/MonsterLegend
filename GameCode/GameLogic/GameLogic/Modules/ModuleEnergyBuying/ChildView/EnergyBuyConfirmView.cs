﻿using System;
using System.Collections.Generic;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using GameData;
using Common.Utils;
using GameMain.GlobalManager;
using UnityEngine.UI;
using UnityEngine;
using Common.ServerConfig;
using Mogo.Util;
using Common.Base;

namespace ModuleEnergyBuying
{
    public class EnergyBuyConfirmView : KContainer
    {
        private StateText _costTipsText;
        private KButton _okBtn;
        private KButton _cancelBtn;
        private KContainer _costItemIcon;

        private int _costItemIconId = 0;

        private int _buyType = 0;
        private BaseItemData _baseItemData;
        private int _energyNum = 0;

        private EnergyBuyingData _energyBuyData;

        private Vector2 _maskBgSize = Vector2.zero;

        private KComponentEvent _onClose;
        public KComponentEvent onClose
        {
            get
            {
                if (_onClose == null)
                {
                    _onClose = new KComponentEvent();
                }
                return _onClose;
            }
        }

        protected override void Awake()
        {
            _energyBuyData = PlayerDataManager.Instance.EnergyBuyingData;
            _costTipsText = GetChildComponent<StateText>("Label_txtHuafei");
            _okBtn = GetChildComponent<KButton>("Button_queding");
            _cancelBtn = GetChildComponent<KButton>("Button_quxiao");
            _costItemIcon = GetChildComponent<KContainer>("Container_icon");
            StateImage maskImg = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            maskImg.gameObject.AddComponent<KDummyButton>();
            RectTransform rect = this.GetComponent<RectTransform>();
            Vector2 anchorPos = rect.anchoredPosition;
            rect.anchoredPosition3D = new Vector3(anchorPos.x, anchorPos.y, -35f);
            _costItemIcon.Visible = false;
            AddEventListener();
        }

        private void AddEventListener()
        {
            _okBtn.onClick.AddListener(OnClickOKBtn);
            _cancelBtn.onClick.AddListener(OnClickCancelBtn);
        }

        private void RemoveEventListener()
        {
            _okBtn.onClick.RemoveListener(OnClickOKBtn);
            _cancelBtn.onClick.RemoveListener(OnClickCancelBtn);
        }

        private void OnClickOKBtn()
        {
            int buyStatus = (_buyType == EnergyBuyingData.BUY_TYPE_ONCE) ? _energyBuyData.BuyOnceStatus : _energyBuyData.BuyAllStatus;
            if (buyStatus == (int)BuyEnergyStatus.NotEnoughMoney)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.Charge);
                return;
            }
            if (_buyType == EnergyBuyingData.BUY_TYPE_ONCE)
            {
                EnergyBuyingManager.Instance.RequestBuyOnceEnergy();
            }
            else if (_buyType == EnergyBuyingData.BUY_TYPE_ALL)
            {
                EnergyBuyingManager.Instance.RequestBuyAllEnergy();
            }
            this.Visible = false;
        }

        private void OnClickCancelBtn()
        {
            this.Visible = false;
            if (_onClose != null)
            {
                _onClose.Invoke();
            }
        }

        protected override void OnEnable()
        {
            if (_maskBgSize.x != UIManager.CANVAS_WIDTH || _maskBgSize.y != UIManager.CANVAS_HEIGHT)
            {
                StateImage maskImg = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
                MogoUtils.AdaptScreen(maskImg);
                _maskBgSize.x = UIManager.CANVAS_WIDTH;
                _maskBgSize.y = UIManager.CANVAS_HEIGHT;
            }
        }

        public void Refresh(int buyType, BaseItemData itemData, int energyNum)
        {
            _buyType = buyType;
            _baseItemData = itemData;
            _energyNum = energyNum;
            StateText okBtnText = _okBtn.GetChildComponent<StateText>("label");
            StateText cancelBtnText = _cancelBtn.GetChildComponent<StateText>("label");
            int buyStatus = (_buyType == EnergyBuyingData.BUY_TYPE_ONCE) ? _energyBuyData.BuyOnceStatus : _energyBuyData.BuyAllStatus;
            if (buyStatus == (int)BuyEnergyStatus.CanBuy)
            {
                string moneyName = item_helper.GetName(_baseItemData.Id);
                _costTipsText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(98007), moneyName,
                    _baseItemData.StackCount, _energyNum);
                RefreshBtnText(okBtnText, MogoLanguageUtil.GetContent(98011));
                RefreshBtnText(cancelBtnText, MogoLanguageUtil.GetContent(98012));
            }
            else if (buyStatus == (int)BuyEnergyStatus.NotHaveBuyTimes)
            {
                _costTipsText.CurrentText.text = MogoLanguageUtil.GetContent(98008);
                _costItemIcon.Visible = false;
                RefreshBtnText(okBtnText, MogoLanguageUtil.GetContent(98013));
                RefreshBtnText(cancelBtnText, MogoLanguageUtil.GetContent(98012));
            }
            else if (buyStatus == (int)BuyEnergyStatus.ReachEnergyLimit)
            {
                _costTipsText.CurrentText.text = MogoLanguageUtil.GetContent(98009);
                _costItemIcon.Visible = false;
                RefreshBtnText(okBtnText, MogoLanguageUtil.GetContent(98011));
                RefreshBtnText(cancelBtnText, MogoLanguageUtil.GetContent(98012));
            }
            else if (buyStatus == (int)BuyEnergyStatus.NotEnoughMoney)
            {
                _costTipsText.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(98010), _baseItemData.StackCount);
                _costItemIcon.Visible = false;
                RefreshBtnText(okBtnText, MogoLanguageUtil.GetContent(98013));
                RefreshBtnText(cancelBtnText, MogoLanguageUtil.GetContent(98012));
            }
        }

        private void RefreshCostItemIcon()
        {
            if (_costItemIcon.Visible == false)
            {
                _costItemIcon.Visible = true;
            }
            int iconId = item_helper.GetIcon(_baseItemData.Id);
            if (_costItemIconId != iconId)
            {
                _costItemIconId = iconId;
                MogoAtlasUtils.AddIcon(_costItemIcon.gameObject, _costItemIconId);
            }
            RectTransform iconRect = _costItemIcon.gameObject.GetComponent<RectTransform>();
            if (_baseItemData.Id == public_config.MONEY_TYPE_GOLD || _baseItemData.Id == public_config.MONEY_TYPE_DIAMOND)
            {
                iconRect.anchoredPosition = new Vector2(552.0f, iconRect.anchoredPosition.y);
            }
            else if (_baseItemData.Id == public_config.MONEY_TYPE_BIND_DIAMOND)
            {
                iconRect.anchoredPosition = new Vector2(618.7f, iconRect.anchoredPosition.y);
            }
        }

        private void RefreshBtnText(StateText btnText, string strText)
        {
            btnText.ChangeAllStateText(strText);
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }
    }
}
