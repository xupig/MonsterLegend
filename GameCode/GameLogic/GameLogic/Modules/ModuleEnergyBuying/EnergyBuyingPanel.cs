﻿using System;
using System.Collections.Generic;
using Common.Base;
using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Data;
using Common.Events;

namespace ModuleEnergyBuying
{
    public class EnergyBuyingPanel : BasePanel
    {
        private EnergyBuyingMainView _energyBuyMainView;
        private EnergyBuyConfirmView _energyBuyConfirmView;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_tilihuode/Container_panelBg/Button_close");
            _energyBuyMainView = AddChildComponent<EnergyBuyingMainView>("Container_tilihuode");
            _energyBuyConfirmView = AddChildComponent<EnergyBuyConfirmView>("Container_tanchuang");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.EnergyBuying; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            _energyBuyMainView.Refresh();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<int, BaseItemData, int>(EnergyBuyingEvents.SHOW_BUY_ENERGY_CONFIRM_VIEW, OnShowConfirmView);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<int, BaseItemData, int>(EnergyBuyingEvents.SHOW_BUY_ENERGY_CONFIRM_VIEW, OnShowConfirmView);
        }

        private void OnShowConfirmView(int buyType, BaseItemData itemData, int energyNum)
        {
            _energyBuyConfirmView.Visible = true;
            _energyBuyConfirmView.Refresh(buyType, itemData, energyNum);
        }

    }
}
