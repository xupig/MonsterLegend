﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleSuitScroll
{
    public class SuitScrollData
    {
        public int itemId { get; set; } //物品ID
        public int equipId { get; set; } //装备ID
        public int icon { get; set; } //图标
        public string equipName { get; set; } //装备名字
        public int suitId { get; set; } //套装名字
        public int postion { get; set; } //套装名字
    }
}

