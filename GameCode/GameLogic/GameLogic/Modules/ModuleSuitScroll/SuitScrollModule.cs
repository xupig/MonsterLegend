﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;

namespace ModuleSuitScroll
{
    public class SuitScrollModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.SuitEquip, "Container_SuitScrollPanel", MogoUILayer.LayerUIToolTip, "ModuleSuitScroll.EquipSuitScrollPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
        }
    }
}

