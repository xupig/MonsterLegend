﻿using System.Collections.Generic;
using Game.UI.UIComponent;



namespace ModuleSuitScroll
{
    public class SuitScrollListView : KContainer
    {
        public bool IsRefreshByProto;

        private KScrollView _scrollPageReward;
        private KList _contentList;

        private KContainer _containerArrow;

        protected int _onePageNum = 6;

        protected override void Awake()
        {
            _scrollPageReward = this.gameObject.GetComponent<KScrollView>();
            _contentList = this.GetChildComponent<KList>("mask/content");

            if (this.GetChild("arrow") != null)
            {
                _containerArrow = this.GetChildComponent<KContainer>("arrow");
            }

            _contentList.SetPadding(0, 0, 0, 0);
            _contentList.SetGap(20, 0);
            _contentList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }
        
        public void RefreshData(List<SuitScrollData> suitScrollDataList)
        {
            //suitScrollDataList.Clear();
            if (suitScrollDataList != null)
            {
                _contentList.RemoveAll();
                _contentList.SetDataList<SuitScroll>(suitScrollDataList);
            }
            if (_containerArrow != null)
            {
                _containerArrow.gameObject.SetActive(suitScrollDataList != null && suitScrollDataList.Count > _onePageNum);
            }
        }
    }

    public class SelectBox : KContainer
    {
        //private KContainer m_containerXiangzi;
        //private KContainer m_containerSelect;
        
        protected override void Awake( )
        {
            
        }
    }
}
