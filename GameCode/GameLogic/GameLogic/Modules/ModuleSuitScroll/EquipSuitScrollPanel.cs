﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleSuitScroll
{
    public class EquipSuitScrollPanel : BasePanel
    {
        private object _data;

        private SuitScrollListView _suitScrollListView;
        private KContainer _hasNoTarget;
        private List<SuitScrollData> suitScrollData = new List<SuitScrollData>();

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_xuanzeshiyong/Container_Bg/Button_fanhui");
            _suitScrollListView = AddChildComponent<SuitScrollListView>("Container_xuanzeshiyong/ScrollView_zhuangbei");
            _hasNoTarget = GetChildComponent<KContainer>("Container_xuanzeshiyong/Container_zanbumanzu");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.SuitEquip; }
        }

        public override void OnShow(object data)
        {
            GetChildComponent<RectTransform>("Container_xuanzeshiyong").anchoredPosition = new Vector2(645, -64);
            SetData(data);
            AddListener();
            EventDispatcher.TriggerEvent(EquipSuitScrollEvents.ShowEquipSuitScrollPanel);
        }

        public override void OnClose()
        {
            EventDispatcher.TriggerEvent(EquipSuitScrollEvents.HideEquipSuitScrollPanel);
            RemoveListener();
        }

        private void AddListener()
        {
            EventDispatcher.AddEventListener(EquipSuitScrollEvents.Refresh, OnRefresh);
        }

        private void RemoveListener()
        {
            EventDispatcher.RemoveEventListener(EquipSuitScrollEvents.Refresh, OnRefresh);
        }

        private void OnRefresh()
        {
            SetData(_data);
        }

        public override void SetData(object data)
        {
            _data = data;
            if(data != null)
            {
                suitScrollData = new List<SuitScrollData>();
                ItemToolTipsData itemdata = data as ItemToolTipsData;
                var scrollItemInfo = equip_suit_scroll_helper.GetConfig(itemdata.Id);
                item_equipment _baseConfig;
                BagData bodyEquipData = PlayerDataManager.Instance.BagData.GetBodyEquipBagData();
                Dictionary<int, BaseItemInfo> itemInfo = bodyEquipData.GetAllItemInfo();
                bool posFlag = false;
                bool qualityFlag = false;
                foreach(KeyValuePair<int, BaseItemInfo> kv in itemInfo)
                {
                    var equipInfo = kv.Value as EquipItemInfo;
                    _baseConfig = item_helper.GetEquipConfig(equipInfo.Id);

                    for(int i = 0; i < scrollItemInfo.__position.Count; i++)
                    {
                        posFlag = false;
                        int pos = System.Convert.ToInt32(scrollItemInfo.__position[i]);
                        if(_baseConfig.__subtype == pos)
                        {
                            posFlag = true;
                            break;
                        }
                    }
                    for(int j = 0; j < scrollItemInfo.__quality.Count; j++)
                    {
                        qualityFlag = false;
                        int quality = System.Convert.ToInt32(scrollItemInfo.__quality[j]);
                        if(equipInfo.Quality == quality)
                        {
                            qualityFlag = true;
                            break;
                        }
                    }
                    int minLevel = System.Convert.ToInt32(scrollItemInfo.__level_range[0]);
                    int maxLevel = System.Convert.ToInt32(scrollItemInfo.__level_range[1]);
                    if(posFlag && qualityFlag && _baseConfig.__level_need >= minLevel && _baseConfig.__level_need <= maxLevel)
                    {
                        SuitScrollData suitdata = new SuitScrollData();
                        suitdata.itemId = itemdata.Id;
                        suitdata.equipId = _baseConfig.__id;
                        suitdata.icon = _baseConfig.__icon;
                        suitdata.equipName = MogoLanguageUtil.GetContent(_baseConfig.__name);
                        suitdata.suitId = equipInfo.SuitId;
                        suitdata.postion = equipInfo.GridPosition;
                        suitScrollData.Add(suitdata);
                    }
                }

                if(suitScrollData != null && suitScrollData.Count > 0)
                {
                    _suitScrollListView.gameObject.SetActive(true);
                    _suitScrollListView.RefreshData(suitScrollData);
                    _hasNoTarget.gameObject.SetActive(false);
                }
                else
                {
                    _suitScrollListView.gameObject.SetActive(false);
                    _hasNoTarget.gameObject.SetActive(true);
                }
            }
        }
    }
}
