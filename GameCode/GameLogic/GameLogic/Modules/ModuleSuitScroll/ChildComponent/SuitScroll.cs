﻿using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using System;
using Common.ExtendComponent;

namespace ModuleSuitScroll
{
    public class SuitScroll : KList.KListItemBase
    {
        private StateText _labEquipName;
        private StateText _labEquipSuitName;
        private StateText _labUnactuated;
        private StateText _count;
        private ItemGrid _itemGrid;
        private KContainer _selectState;
        private KButton _usebtn;

        private SuitScrollData _data;

        protected override void Awake()
        {
            _labEquipName = GetChildComponent<StateText>("Label_txtZhuangbei");
            _labEquipSuitName = GetChildComponent<StateText>("Label_txtZhuangbeiScroll");
            _labUnactuated = GetChildComponent<StateText>("Label_txtWeikaiqi");
            _count = GetChildComponent<StateText>("Label_txtYizhuangbsuitnum");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _selectState = GetChildComponent<KContainer>("Container_xuanzhongkuang");
            _usebtn = GetChildComponent<KButton>("Button_queding");
            _usebtn.onClick.AddListener(OnUseBtnHandler);
            this.onClick.AddListener(OnItemClick);
        }

        public override void Dispose()
        {
            _usebtn.onClick.RemoveListener(OnUseBtnHandler);
            this.onClick.RemoveListener(OnItemClick);
            _data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if(value != null)
                {
                    _data = value as SuitScrollData;
                    Refresh();
                }
            }
        }

        private void OnItemClick(KList.KListItemBase ItemBase, int index)
        {
            
        }

        private void OnUseBtnHandler()
        {
            PlayerAvatar.Player.RpcCall("suit_req", (UInt32)_data.itemId, (UInt32)_data.postion);
        }

        private void Refresh()
        {
            _labEquipName.CurrentText.text = _data.equipName.ToString();
            if (_data.suitId > 0)
            {
                string suitCurrCount = equip_suit_helper.GetEquipedSuitPartCount(_data.suitId).ToString();
                string suitTotalCount = equip_suit_helper.GetSuitPartCount(_data.suitId).ToString();
                _labEquipSuitName.CurrentText.text = equip_suit_helper.GetName(_data.suitId);
                _count.CurrentText.text = suitCurrCount + "/" + suitTotalCount;
                _labEquipSuitName.gameObject.SetActive(true);
                _count.gameObject.SetActive(true);
                _labUnactuated.gameObject.SetActive(false);
            }
            else
            {
                _labEquipSuitName.gameObject.SetActive(false);
                _count.gameObject.SetActive(false);
                _labUnactuated.gameObject.SetActive(true);
            }
            _selectState.gameObject.SetActive(false);
            _itemGrid.SetItemData(_data.equipId);
        }
    }
}
