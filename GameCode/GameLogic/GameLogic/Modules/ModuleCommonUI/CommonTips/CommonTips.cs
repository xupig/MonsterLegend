﻿#region 模块信息
/*==========================================
// 文件名：CommonTips
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.CommonTips
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/8/11 22:05:38
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCommonUI
{
    public class CommonTips:BasePanel
    {
        private Vector2 _pos;
        private RectTransform _rect;
        private StateText _txtContent;
        private KDummyButton _btn;
        protected override void Awake()
        {
            _rect = GetChildComponent<RectTransform>("Container_content");
            _pos = _rect.anchoredPosition;
            _txtContent = GetChildComponent<StateText>("Container_content/Label_txtContent");
            _btn = gameObject.AddComponent<KDummyButton>();
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.CommonTips; }
        }

        public override void OnShow(object data)
        {
            if(data is string)
            {
                _txtContent.CurrentText.text = (data).ToString();
                _rect.anchoredPosition = _pos;
            }
            else
            {
                object[] list = data as object[];
                _txtContent.CurrentText.text = (list[0]).ToString();
                _rect.anchoredPosition = (Vector2)list[1];
            }
            _btn.onClick.AddListener(OnClick);
        }

        public override void OnClose()
        {
            _btn.onClick.RemoveListener(OnClick);
        }


        private void OnClick()
        {
            Id.Close();
        }

    }
}
