﻿#region 模块信息
/*==========================================
// 文件名：CommonRulePanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.CommonTips
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/10/10 11:25:33
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCommonUI
{
    public class CommonRulePanel:BasePanel
    {
        private StateText _txtContent;
        private StateText _txtTitle;
        private KButton _btnClose;
        private KDummyButton _btnCloseAll;

        protected override void Awake()
        {
            _txtContent = GetChildComponent<StateText>("Label_txtNR01");
            _txtTitle = GetChildComponent<StateText>("Label_txtbiaoti");
            _btnClose = GetChildComponent<KButton>("Button_btn3");
            CloseBtn = GetChildComponent<KButton>("Button_close");
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _btnCloseAll = ModalMask.gameObject.AddComponent<KDummyButton>();

            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.CommonRule; }
        }

        public override void OnShow(object data)
        {
            string[] dataList = data as string[];
            _txtTitle.CurrentText.text = dataList[0];
            _txtContent.CurrentText.text = dataList[1];
            _btnClose.onClick.AddListener(OnClickClose);
            _btnCloseAll.onClick.AddListener(OnClickClose);
        }

        public override void OnClose()
        {
            _btnClose.onClick.RemoveListener(OnClickClose);
            _btnCloseAll.onClick.RemoveListener(OnClickClose);
        }

        private void OnClickClose()
        {
            Id.Close();
        }
    }
}
