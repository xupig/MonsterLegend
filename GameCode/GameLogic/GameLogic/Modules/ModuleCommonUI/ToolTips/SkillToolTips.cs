﻿#region 模块信息
/*==========================================
// 文件名：SkillToolTips
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.ToolTips
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/16 10:05:49
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using Mogo.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCommonUI
{
    public class SkillToolTips : BasePanel
    {
        private SkillOneContainer _one;
        private SkillTwoContainer _two;
        private SkillOneContainer _oneSloted;
        private SkillTwoContainer _twoSloted;
        private KButton _btnReplace;

        private spell _spell;

        private RectTransform _rectOne;
        private RectTransform _rectTwo;
        private float _orignalX = 0;
        private KDummyButton _btnClose;

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            CloseBtn = GetChildComponent<KButton>("Button_quxiao");
            _btnReplace = GetChildComponent<KButton>("Button_tihuan");
            GameObject one = transform.FindChild("Container_one").gameObject;
            GameObject two = transform.FindChild("Container_two").gameObject;
            GameObject oneSloted = GameObject.Instantiate(one) as GameObject;
            GameObject twoSloted = GameObject.Instantiate(two) as GameObject;
            ResetTransform(oneSloted,one);
            ResetTransform(twoSloted,one);
            ResetTransform(one, two);
            one.SetActive(true);
            two.SetActive(true);
            oneSloted.SetActive(true);
            twoSloted.SetActive(true);
            _one = one.AddComponent<SkillOneContainer>();
            _oneSloted = oneSloted.AddComponent<SkillOneContainer>();
            _two = two.AddComponent<SkillTwoContainer>();
            _twoSloted = twoSloted.AddComponent<SkillTwoContainer>();
            _rectOne = _one.GetComponent<RectTransform>();
            _rectTwo = _two.GetComponent<RectTransform>();
            _orignalX = _rectOne.anchoredPosition3D.x;
            _one.Visible = false;
            _oneSloted.Visible = false;
            _two.Visible = false;
            _twoSloted.Visible = false;
            _btnClose = AddChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
            
            base.Awake();
        }



        private void ResetTransform(GameObject clone,GameObject go)
        {
            clone.transform.SetParent(transform,false);
            clone.transform.localPosition = go.transform.localPosition;
            clone.transform.localRotation = go.transform.localRotation;
            clone.transform.localScale = go.transform.localScale;
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.SkillToolTips; }
        }

        public override void OnShow(object data)
        {
            _btnClose.onClick.AddListener(ClosePanel);
            _btnReplace.onClick.AddListener(OnClickReplace);
            SetData(data);
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            _one.Visible = false;
            _oneSloted.Visible = false;
            _two.Visible = false;
            _twoSloted.Visible = false;
            object[] dataList = (data as ToolTipsDataWrapper).data as object[];
            _spell = dataList[0] as spell;
            
            SpellData spellData = PlayerDataManager.Instance.SpellData;
            spell slotedSpell = null;
            if ((bool)dataList[1] == true)
            {
                slotedSpell = spellData.GetSlotSpell(_spell.__pos);
            }
            spell seniorSpell = skill_helper.GetSeniorSpell(_spell);
            if (slotedSpell != null && slotedSpell.__id != _spell.__id)
            {
                CloseBtn.Visible = true;
                _btnReplace.Visible = true;
            }
            else
            {
                CloseBtn.Visible = false;
                _btnReplace.Visible = false;
            }
            if (slotedSpell != null && slotedSpell.__id == _spell.__id)
            {
                slotedSpell = null;
            }
            if (seniorSpell != null)
            {
                _two.Visible = true;
                _two.SetData(_spell, slotedSpell, seniorSpell);
                if (slotedSpell!=null)
                {
                    _rectTwo.anchoredPosition3D = new Vector3(_orignalX, _rectTwo.anchoredPosition3D.y, _rectTwo.anchoredPosition3D.z);
                }
                else
                {
                    _rectTwo.anchoredPosition3D = new Vector3((UIManager.PANEL_WIDTH-_rectTwo.sizeDelta.x)/2, _rectTwo.anchoredPosition3D.y, _rectTwo.anchoredPosition3D.z);
                }
            }
            else
            {
                _one.Visible = true;
                _one.SetData(_spell, slotedSpell);
                if(slotedSpell!=null)
                {
                    _rectOne.anchoredPosition3D = new Vector3(_orignalX, _rectOne.anchoredPosition3D.y, _rectOne.anchoredPosition3D.z);
                }
                else
                {
                    _rectOne.anchoredPosition3D = new Vector3((UIManager.PANEL_WIDTH - _rectOne.sizeDelta.x) / 2, _rectOne.anchoredPosition3D.y, _rectOne.anchoredPosition3D.z);
                }
            }
            if(slotedSpell != null)
            {
                spell slotedSeniorSpell = skill_helper.GetSeniorSpell(slotedSpell);
                if (slotedSeniorSpell != null)
                {
                    _twoSloted.Visible = true;
                    _twoSloted.SetData(slotedSpell, slotedSpell, slotedSeniorSpell);
                }
                else
                {
                    _oneSloted.Visible = true;
                    _oneSloted.SetData(slotedSpell, slotedSpell);
                }
            }
        }

        public override void OnClose()
        {
            _btnClose.onClick.RemoveListener(ClosePanel);
            _btnReplace.onClick.RemoveListener(OnClickReplace);
        }

        private void OnClickReplace()
        {
            Debug.Log("OnClickReplace:"+_spell.__id+","+_spell.__name.ToLanguage());
            SpellManager.Instance.SpellEquip(_spell.__id);
            ClosePanel();
        }

    }
}
