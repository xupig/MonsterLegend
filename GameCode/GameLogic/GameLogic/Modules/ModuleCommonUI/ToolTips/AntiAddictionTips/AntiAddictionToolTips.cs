﻿#region 模块信息
/*==========================================
// 文件名：AntiAddiction
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.ToolTips
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/2/24 13:57:07
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using UnityEngine;
using GameMain.GlobalManager.SubSystem;

namespace ModuleCommonUI
{
    public class AntiAddictionToolTips : BasePanel
    {
        private LoginAntiAddictionToolTip _loginAntiAddictionTip;
        private WriteOwnMessageTip _writeOwnMessageTip;
        private WarnPlayerTimeTip _warnPlayerTimeTip;

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _loginAntiAddictionTip = AddChildComponent<LoginAntiAddictionToolTip>("Container_tishi");
            _writeOwnMessageTip = AddChildComponent<WriteOwnMessageTip>("Container_tianxiexinxi");
            _warnPlayerTimeTip = AddChildComponent<WarnPlayerTimeTip>("Container_antitishi");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.AntiAddiction; }
        }

        private void AddEventListener()
        {
            _loginAntiAddictionTip.writeMessageBtnClick.AddListener(WriteMessage);
        }

        private void RemoveEventListener()
        {
            _loginAntiAddictionTip.writeMessageBtnClick.RemoveListener(WriteMessage);
        }

        private void WriteMessage()
        {
            HideAllSubTips();
            GetChild("Container_tianxiexinxi").SetActive(true);
            _writeOwnMessageTip.Refresh();
        }

        private void HideAllSubTips()
        {
            _loginAntiAddictionTip.Visible = false;
            _warnPlayerTimeTip.Visible = false;
            _writeOwnMessageTip.Visible = false;
        }

        public override void OnShow(object data)
        {
            HideAllSubTips();
            if (data is AntiDataWrapper)
            {
                AntiDataWrapper wrapper = data as AntiDataWrapper;
                AntiPanelType type = wrapper.type;
                switch (type)
                {
                    case AntiPanelType.Login:
                        GetChild("Container_tishi").SetActive(true);
                        _loginAntiAddictionTip.Refresh();
                        break;
                    case AntiPanelType.WriteMessage:
                        GetChild("Container_tianxiexinxi").SetActive(true);
                        _writeOwnMessageTip.Refresh();
                        break;
                    case AntiPanelType.KickOff:
                        GetChild("Container_antitishi").SetActive(true);
                        _warnPlayerTimeTip.Refresh(wrapper);
                        break;
                }
            }
            AddEventListener();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }
    }
}