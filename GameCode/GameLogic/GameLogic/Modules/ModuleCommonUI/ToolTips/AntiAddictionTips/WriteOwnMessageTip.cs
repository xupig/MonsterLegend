﻿using Common.Base;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

namespace ModuleCommonUI
{
    public class WriteOwnMessageTip : KContainer
    {
        private KInputField _nameField;
        private KInputField _idCardField;
        private KButton _commitBtn;
        private KButton _cancelBtn;
        private KButton _closeBtn;

        protected override void Awake()
        {
            _nameField = GetChildComponent<KInputField>("Input_realName");
            _idCardField = GetChildComponent<KInputField>("Input_idCardNum");
            _commitBtn = GetChildComponent<KButton>("Button_tijiao");
            _cancelBtn = GetChildComponent<KButton>("Button_quxiao");
            _closeBtn = GetChildComponent<KButton>("Button_close");
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _cancelBtn.onClick.AddListener(CancelCommit);
            _commitBtn.onClick.AddListener(OnCommitInfo);
            _closeBtn.onClick.AddListener(CancelCommit);
        }

        private void RemoveEventListener()
        {
            _cancelBtn.onClick.RemoveListener(CancelCommit);
            _commitBtn.onClick.RemoveListener(OnCommitInfo);
            _closeBtn.onClick.RemoveListener(CancelCommit);
        }

        private void CancelCommit()
        {
            AntiAddictionManager.Instance.SetMainUIShowAntiMessage(true);
            ClosePanel();
        }

        private void ClosePanel()
        {
            AntiAddictionManager.Instance.SetMainUIShowAntiMessage(true);
            UIManager.Instance.ClosePanel(PanelIdEnum.AntiAddiction);
        }

        private void OnCommitInfo()
        {
            bool checkResult = IDCardCheckManager.GetInstance().CheckIDCard(_idCardField.text);
            bool checkName = IsChinaese(_nameField.text);
            if (!checkName)
            {
                MessageBox.Show(true, "", "请输入正确的中文名！");
                return;
            }
            if (checkResult)
            {
                string username = "";
                if (Application.platform == RuntimePlatform.WindowsPlayer)
                {
                    username = LoginManager.Instance.GetWindowsPlatformLoginData().name;
                }
                else if (Application.platform == RuntimePlatform.WindowsWebPlayer)
                {
                    username = WebLoginInfoManager.GetInstance().GetWebPlatformLoginData().name;
                }
                string url = string.Empty;
                string _url = "http://web.4399.com/api/reg/fcm_api.php";
                string key = _nameField.text + username + "shjdysuwei32*&DSdooiew" + _idCardField.text;
                //string key = _nameField.text + "test" + "shjdysuwei32*&DSdooiew" + _idCardField.text;
                MD5 md5 = MD5.Create();
                byte[] _md5Key = md5.ComputeHash(Encoding.UTF8.GetBytes(key));
                string LOGIN_URLX = "{0}?account={1}&card={2}&sign={3}&truename={4}";
                url = string.Format(LOGIN_URLX,
                                            _url,
                                    Uri.EscapeDataString(username),
                                            _idCardField.text,
                                            CryptoUtils.FormatMD5(_md5Key),
                                            Uri.EscapeDataString(_nameField.text));
                LoggerHelper.Info(string.Format("[CheckCard] HttpGetUrl:{0}", url));
                Driver.Instance.StartCoroutine(WebLoginInfoManager.GetInstance().GetHttpByWWW(url, OnCheckDone, OnCheckFail));
                //HttpUtils.Get(url, OnCheckDone, OnCheckFail);
            }
            else
            {
                MessageBox.Show(true, "", "请输入正确的身份证号！");
            }

        }

        //Web请求验证返回
        private void OnCheckDone(string result)
        {
            LoggerHelper.Info("CheckCard Done：" + result);
            //string[] baseappInfo = result.Split(',');
            int errorId = int.Parse(result);
            if (errorId < 0)
            {
                switch (errorId)
                {
                    case -1:
                        MessageBox.Show(true, "", "参数缺失！");
                        break;
                    case -2:
                        MessageBox.Show(true, "", "验证失败！");
                        break;
                    case -3:
                        MessageBox.Show(true, "", "身份证号码无效！");
                        break;
                    case -4:
                        MessageBox.Show(true, "", "不允许重复登记,每个玩家账户登记成功不允许变动！");
                        break;
                    case -5:
                        MessageBox.Show(true, "", "登记失败！");
                        break;
                    case -6:
                        MessageBox.Show(true, "", "用户不存在！");
                        break;
                    default:
                        MessageBox.Show(true, "", "验证失败！");
                        break;
                }
                ClosePanel();
                return;

            }
            int cm = 2;
            if (errorId == 1)
            {
                cm = 1;
            }
            else if (errorId == 2)
            {
                cm = 0;
            }
            else
            {
                cm = 2;
            }
            LoggerHelper.Info("CM Done：" + cm);

            if (cm < 2 && cm >= 0)
            {
                MessageBox.Show(true, "", "验证成功！");
            }
            else if (cm == 2)
            {
                MessageBox.Show(true, "", "验证失败！");
            }
            ClosePanel();
            AntiAddictionManager.Instance.SendTheAntiAddictionCode((UInt16)cm, 0);
            if (cm == 1)
            {
                AntiAddictionManager.Instance.SetMainUIShowAntiMessage(false);
            }
            else
            {
                AntiAddictionManager.Instance.SetMainUIShowAntiMessage(true);
            }
            ClosePanel();
        }

        private bool IsChinaese(string chinaese)
        {
            return Regex.IsMatch(chinaese, @"^[\u4e00-\u9fa5]+$");
        }

        //Web请求验证失败
        private void OnCheckFail(HttpStatusCode errorCode)
        {
            LoggerHelper.Info("CheckCard Fail");
        }

        public void Refresh()
        {
            _nameField.text = MogoLanguageUtil.GetContent(6338007);
            _nameField.characterLimit = 10;
            _idCardField.text = MogoLanguageUtil.GetContent(6338008);
            _idCardField.characterLimit = 18;
        }
    }
}
