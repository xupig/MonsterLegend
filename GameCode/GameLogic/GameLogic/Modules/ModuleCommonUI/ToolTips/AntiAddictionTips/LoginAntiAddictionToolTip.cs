﻿using Common.Base;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCommonUI
{
    public class LoginAntiAddictionToolTip : KContainer
    {
        private StateText _titleTxt;
        private StateText _content;
        private KButton _writeMessageBtn;
        private KButton _underageBtn;
        private KButton _closeBtn;

        public KComponentEvent writeMessageBtnClick = new KComponentEvent();

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _writeMessageBtn.onClick.AddListener(OnWriteOwnMessageBtnClick);
            _underageBtn.onClick.AddListener(OnUnderageBtnClick);
            _closeBtn.onClick.AddListener(OnUnderageBtnClick);
        }

        private void RemoveEventListener()
        {
            _writeMessageBtn.onClick.RemoveListener(OnWriteOwnMessageBtnClick);
            _underageBtn.onClick.RemoveListener(OnUnderageBtnClick);
            _closeBtn.onClick.RemoveListener(OnUnderageBtnClick);
        }

        private void OnWriteOwnMessageBtnClick()
        {
            writeMessageBtnClick.Invoke();
        }

        private void OnUnderageBtnClick()
        {
            AntiAddictionManager.Instance.SetMainUIShowAntiMessage(true);
            UIManager.Instance.ClosePanel(PanelIdEnum.AntiAddiction);
        }

        protected override void Awake()
        {
            _titleTxt = GetChildComponent<StateText>("Label_txtTitle");
            _content = GetChildComponent<StateText>("Container_Content/Label_txtContent");
            _writeMessageBtn = GetChildComponent<KButton>("Button_tianxiexinxi");
            _underageBtn = GetChildComponent<KButton>("Button_weichengnian");
            SetButtonTxt(_underageBtn, MogoLanguageUtil.GetContent(123502));
            _closeBtn = GetChildComponent<KButton>("Container_Bg/Button_close");
        }

        private void SetButtonTxt(KButton button, string text)
        {
            TextWrapper[] textArray = button.GetComponentsInChildren<TextWrapper>(true);
            for (int i = 0; i < textArray.Length; i++)
            {
                textArray[i].text = text;
            }
        }

        public void Refresh()
        {
            _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(24);
            _content.CurrentText.text = MogoLanguageUtil.GetContent(123500);
        }
    }
}
