﻿using Common.Base;
using Common.Utils;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCommonUI
{
    public class WarnPlayerTimeTip : KContainer
    {
        private StateText _titleTxt;
        private StateText _content;
        private StateText _leftTimeTxt;
        private KButton _closeButton;
        private KButton _sureButton;
        private KButton _quitButton;

        private int leftTime = 10;
        private uint timerId = 0;

        protected override void Awake()
        {
            _titleTxt = GetChildComponent<StateText>("Label_txtTitle");
            _content = GetChildComponent<StateText>("Container_Content/Label_txtContent");
            _leftTimeTxt = GetChildComponent<StateText>("Label_txtshengyushijian");
            _sureButton = GetChildComponent<KButton>("Button_tianxiexinxi");
            _closeButton = GetChildComponent<KButton>("Container_Bg/Button_close");
            _quitButton = GetChildComponent<KButton>("Button_tuichuyouxi");
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _sureButton.onClick.AddListener(ClosePanel);
            _closeButton.onClick.AddListener(ClosePanel);
            _quitButton.onClick.AddListener(QuitGame);
        }

        private void RemoveEventListener()
        {
            _sureButton.onClick.RemoveListener(ClosePanel);
            _closeButton.onClick.RemoveListener(ClosePanel);
            _quitButton.onClick.RemoveListener(QuitGame);
        }

        private void QuitGame()
        {
            Application.Quit();
        }

        private void ClosePanel()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.AntiAddiction);
        }

        public void Refresh(AntiDataWrapper wrapper)
        {
            _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(24);
            if (wrapper.isKickOff == true)
            {
                timerId = TimerHeap.AddTimer(1000, 1000, ResetTime);
                _leftTimeTxt.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(123510), leftTime);
                _content.CurrentText.text = MogoLanguageUtil.GetContent(wrapper.tipsId);
                _quitButton.Visible = true;
                _sureButton.Visible = false;
            }
            else
            {
                _leftTimeTxt.CurrentText.text = string.Empty;
                _content.CurrentText.text = MogoLanguageUtil.GetContent(wrapper.tipsId);
                _quitButton.Visible = true;
                _sureButton.Visible = false;
            }
        }

        private void ResetTime()
        {
            leftTime--;
            if (leftTime <= 0)
            {
                TimerHeap.DelTimer(timerId);
                Application.Quit();
            }
            else
            {
                _leftTimeTxt.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(123510), leftTime);
            }
        }
    }
}
