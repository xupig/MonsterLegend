﻿#region 模块信息
/*==========================================
// 文件名：RuneExpTipContainer
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.ToolTips.RuneToolTips
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/8 9:48:03
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ServerConfig;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using ModuleRune.ChildComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCommonUI
{
    public class RuneExpTipContainer:KContainer
    {

        private KContainer _levelContainer;
        private KContainer _getContainer;
        private KContainer _descriptionContainer;

        private StateText _txtLevel;
        private StateText _txtGet;
        private StateText _txtDescription;

        private string levelTemplate;
        private string getTemplate;
        private string descriptionTemplate;

        private List<Locater> _locaterList;
        protected override void Awake()
        {
            _locaterList = new List<Locater>();
            _levelContainer = GetChildComponent<KContainer>("Container_dengji");
            _locaterList.Add(_levelContainer.gameObject.AddComponent<Locater>());
            _txtLevel = _levelContainer.GetChildComponent<StateText>("Label_text");
            levelTemplate = _txtLevel.CurrentText.text;

            _getContainer = GetChildComponent<KContainer>("Container_huode");
            _locaterList.Add(_getContainer.gameObject.AddComponent<Locater>());
            _txtGet = _getContainer.GetChildComponent<StateText>("Label_text");
            getTemplate = _txtGet.CurrentText.text;

            _descriptionContainer = GetChildComponent<KContainer>("Container_miaoshu");
            _locaterList.Add(_descriptionContainer.gameObject.AddComponent<Locater>());
            _txtDescription = _descriptionContainer.GetChildComponent<StateText>("Label_text");
            descriptionTemplate = _txtDescription.CurrentText.text;

            base.Awake();
        }

        public void SetContent(RuneItemInfo itemInfo)
        {
            _txtLevel.CurrentText.text = string.Format(levelTemplate,itemInfo.Level);
            _txtDescription.CurrentText.text = string.Format(descriptionTemplate,itemInfo.Description);
            string rewardContent = GetRewardContent(itemInfo);
            _txtGet.CurrentText.text = string.Format(getTemplate,rewardContent);
            RefreshLayout();
        }

        private string GetRewardContent(RuneItemInfo itemInfo)
        {
            Dictionary<string, string> useEffect = itemInfo.UseEffect;
            string key = public_config.USE_EFFECT_CFG_ITEM_REWARD.ToString();
            List<string> itemList = null;
            List<string> itemValue = null;
            if (useEffect.ContainsKey(key))
            {
                item_reward reward = item_reward_helper.GetItemReward(int.Parse(useEffect[key]));
                itemValue = reward.__item_value;
                switch (PlayerAvatar.Player.vocation)
                {
                    case Vocation.Warrior:
                        itemList = reward.__item1;
                        break;
                    case Vocation.Assassin:
                        itemList = reward.__item2;
                        break;
                    case Vocation.Wizard:
                        itemList = reward.__item3;
                        break;
                    case Vocation.Archer:
                        itemList = reward.__item4;
                        break;
                    default:
                        break;
                }
            }
            string rewardContent = string.Empty;
            if (itemList != null)
            {
                for (int i = 0; i < itemList.Count; i++)
                {
                    int id = int.Parse(itemList[i]);
                    if (i != 0)
                    {
                        rewardContent = string.Concat("\n", rewardContent);
                    }
                    rewardContent = string.Concat(rewardContent, item_helper.GetName(id), "x", itemValue[i]);
                }
            }
            return rewardContent;
        }

        private void RefreshLayout()
        {
            float startY = _locaterList[0].Y;
            for (int i = 0; i < _locaterList.Count; i++)
            {
                Locater locater = _locaterList[i];
                if (locater.gameObject.activeSelf == true)
                {
                    locater.Y = startY;
                    startY -= locater.Height;
                }
            }
        }
    }
}
