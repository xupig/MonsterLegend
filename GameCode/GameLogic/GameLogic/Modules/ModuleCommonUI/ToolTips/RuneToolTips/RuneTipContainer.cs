﻿#region 模块信息
/*==========================================
// 文件名：RuneTipContainercs
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.ToolTips.RuneToolTips
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/8 9:47:18
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleRune.ChildComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCommonUI
{
    public class RuneTipContainer:KContainer
    {

        private StateText _txtLevel;
        private StateText _txtCurrentLevel;
        private StateText _txtExp;
        private KProgressBar _progressBar;

        //private Locater _active;
        //private Locater _notActive;
       // private KContainer activeContainer;
        //private KContainer notActiveContainer;
        private StateText _txtActive;
        private StateText _txtNotActive;

        private string levelTemplate;
        private string currentLevelTemplate;
        private string expTemplate;
        private string activeTemplate;
        private string nextTemplate;


        protected override void Awake()
        {
            _txtLevel = GetChildComponent<StateText>("Container_suoxudengji/Label_text");
            levelTemplate = _txtLevel.CurrentText.text;
            _txtCurrentLevel = GetChildComponent<StateText>("Container_dengji/Label_text");
            currentLevelTemplate = _txtCurrentLevel.CurrentText.text;
            _txtExp = GetChildComponent<StateText>("Container_bangding/Label_txtExp");
            expTemplate = _txtExp.CurrentText.text;
            _txtActive = GetChildComponent<StateText>("Container_yijihuo/Label_txtAttri");
            _txtNotActive = GetChildComponent<StateText>("Container_yijihuo/Label_txtAttriNext");
            _progressBar = GetChildComponent<KProgressBar>("Container_bangding/ProgressBar_jindu");
            activeTemplate = _txtActive.CurrentText.text;
            nextTemplate = _txtNotActive.CurrentText.text;
            base.Awake();
        }

        public void SetRuneContent(rune rune)
        {
            _txtLevel.CurrentText.text = string.Format(levelTemplate, rune.__level_need);
            _txtCurrentLevel.CurrentText.text = string.Format(currentLevelTemplate, 1);
            int currentExp = 0;
            int totalExp = int.Parse(rune.__exp_need["1"]);
            _txtExp.CurrentText.text = string.Format(expTemplate, currentExp, totalExp );
            _progressBar.Percent = (currentExp) * 100 / (totalExp);

            Dictionary<int, List<int[]>> attributeLevelValueDict = rune_helper.GetRuneAttributeData(rune);


            SetTxtAttribute(1, attributeLevelValueDict, _txtActive,activeTemplate);
            SetTxtAttribute(2, attributeLevelValueDict, _txtNotActive, nextTemplate);

        }


        private void SetTxtAttribute(int level, Dictionary<int, List<int[]>> attributeLevelValueDict,StateText txt,string template)
        {
            if(attributeLevelValueDict.ContainsKey(level))
            {
                txt.Visible = true;
                List<int[]> dataList = attributeLevelValueDict[level];
                GameData.attri_config attri1 = XMLManager.attri_config[dataList[0][0]];
                string attri2Content;
                string name = string.Empty;
                if (dataList.Count > 1)
                {
                    attri2Content = string.Format("-{0}", dataList[1][1]);
                    name = attri1.__name_id.ToLanguage().Substring(0, 2);
                }
                else
                {
                    attri2Content = string.Empty;
                    name = attri1.__name_id.ToLanguage();
                }
                txt.CurrentText.text = string.Format(template,name , dataList[0][1], attri2Content);
            }
            else
            {
                txt.Visible = false ;
            }
        }


        public void SetContent(RuneItemInfo itemInfo)
        {
            _txtLevel.CurrentText.text = string.Format(levelTemplate, itemInfo.RuneConfig.__level_need);
            _txtCurrentLevel.CurrentText.text = string.Format(currentLevelTemplate, itemInfo.Level);
            if(itemInfo.Level >= itemInfo.MaxLevel)
            {
                _txtExp.CurrentText.text = (4334).ToLanguage();//已满级
                _progressBar.Percent = 100;
            }
            else
            {
                int currentExp = itemInfo.CurExp;
                int totalExp = itemInfo.LevelMaxExp(itemInfo.CurExp);
                int currentLevelMinExp = itemInfo.LevelMinExp(itemInfo.CurExp);
                _txtExp.CurrentText.text = string.Format("{0}/{1}", currentExp - currentLevelMinExp, totalExp - currentLevelMinExp);
                _progressBar.Percent = (currentExp - currentLevelMinExp) * 100 / (totalExp - currentLevelMinExp);
            }
           
            Dictionary<int, List<int[]>> attributeLevelValueDict = rune_helper.GetRuneAttributeData(itemInfo.RuneConfig);

            SetTxtAttribute(itemInfo.Level, attributeLevelValueDict, _txtActive, activeTemplate);
            SetTxtAttribute(itemInfo.Level+1, attributeLevelValueDict, _txtNotActive,nextTemplate);
        }

    }
}
