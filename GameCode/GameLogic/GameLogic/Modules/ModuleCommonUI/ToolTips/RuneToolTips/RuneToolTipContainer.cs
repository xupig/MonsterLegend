﻿#region 模块信息
/*==========================================
// 文件名：RuneToolTipContainer
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.ToolTips.RuneToolTips
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/2 15:07:25
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleRune.ChildComponent;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCommonUI
{
    public class RuneToolTipContainer : KContainer
    {
        private KScrollView _scrollView;
        private RuneTipContainer _runeTipContainer;
        private StateText _txtName;
        private ItemGrid _itemGrid;
        private StateImage _equipedImg;
        private StateImage _lockImg;
        private RecommendContainer _recommendContainer;

        protected override void Awake()
        {
            _lockImg = GetChildComponent<StateImage>("Image_suo");
            _equipedImg = GetChildComponent<StateImage>("Image_sharedyizhuangbei");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_scrollView");
            _runeTipContainer = _scrollView.AddChildComponent<RuneTipContainer>("mask/content");
            _txtName = GetChildComponent<StateText>("Container_xiaobiaoti/Label_txtName");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _itemGrid.IsClickable = false;
            _equipedImg.Visible = false;
            InitRecommendContainer();
        }

        private void InitRecommendContainer()
        {
            GameObject childGo = GetChild("ScrollView_scrollView/mask/content/Container_tuijian");
            childGo.SetActive(true);
            _recommendContainer = childGo.AddComponent<RecommendContainer>();
        }

        public void ShowEquiped()
        {
            _equipedImg.Visible = true;
        }


        public void SetRuneData(rune rune)
        {
            _runeTipContainer.SetRuneContent(rune);
            _txtName.CurrentText.text = item_helper.GetName(rune.__id);
            _txtName.CurrentText.color = item_helper.GetColor(rune.__id);
            _itemGrid.SetItemData(rune.__id);
            _lockImg.Visible = false;
            int attriId = attri_effect_helper.GetAttributeIdList(int.Parse(rune.__level_effect["1"]))[0];
            _recommendContainer.SetStarNum(attri_config_helper.GetRecommendStar(attriId, PlayerAvatar.Player.vocation));
            _recommendContainer.SetStarTxt(attri_config_helper.GetRecommendTxt(attriId));
        }

        public void SetRuneItemInfo(RuneItemInfo _itemInfo)
        {
            _runeTipContainer.SetContent(_itemInfo);
            _txtName.CurrentText.text = _itemInfo.Name;
            _txtName.CurrentText.color = item_helper.GetColor(_itemInfo.Id);
            _itemGrid.SetItemData(_itemInfo);
            _lockImg.Visible = _itemInfo.RuneLock == 1;
            int attriId = attri_effect_helper.GetAttributeIdList(_itemInfo.EffectId)[0];
            _recommendContainer.SetStarNum(attri_config_helper.GetRecommendStar(attriId, PlayerAvatar.Player.vocation));
            _recommendContainer.SetStarTxt(attri_config_helper.GetRecommendTxt(attriId));
        }

        public void UpdateLockState(RuneItemInfo _itemInfo)
        {
            _lockImg.Visible = _itemInfo.RuneLock == 1;
        }

    }
}
