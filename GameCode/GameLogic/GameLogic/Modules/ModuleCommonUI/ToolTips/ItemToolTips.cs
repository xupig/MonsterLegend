﻿using System.Collections.Generic;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Data;
using GameMain.GlobalManager;
using Common.ExtendComponent;
using MogoEngine.Events;
using Common.Events;
using Common.Utils;
using Common.Global;

namespace ModuleCommonUI
{
    public class ItemToolTips : BasePanel
    {
        protected KContainer _contentContainer;
        protected Locater _contentLocater;
        protected KButton _storyBtn;
        protected StateText _nameTxt;
        protected StateText _expireTimeTxt;
        protected ItemGrid _itemGrid;
        protected StateImage _imagezheZhao;

        protected CommonFunctionContainer _commonBtnContainer;
        protected StartPropertyContainer _startContainer;
        protected EndPropertyContainer _endContaner;

        protected List<Locater> _locaterList = new List<Locater>();
        protected List<KContainer> _containerList = new List<KContainer>();

        protected ItemToolTipsData _itemData;
        protected PanelIdEnum _context;

        private KDummyButton _dummyBtn;
        protected bool showBtn;
        private RectTransform _contentRect;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.ItemToolTips; }
        }

        protected override void Awake()
        {
            _contentContainer = GetChildComponent<KContainer>("Container_content/ScrollView_content/mask/content");
            _contentLocater = AddChildComponent<Locater>("Container_content/ScrollView_content/mask/content");
            _dummyBtn = AddChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
            CloseBtn = GetChildComponent<KButton>("Container_content/Container_Bg/Button_close");
            _storyBtn = GetChildComponent<KButton>("Container_content/Button_story");
            _storyBtn.gameObject.SetActive(false);
            _nameTxt = GetChildComponent<StateText>("Container_content/Container_mingzi/Label_txtName");
            _expireTimeTxt = GetChildComponent<StateText>("Container_content/Label_shixiaoshijian");
            _itemGrid = GetChildComponent<ItemGrid>("Container_content/Container_wupin");
            _imagezheZhao = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _imagezheZhao.Alpha = 195.0f / 255.0f;
            _contentRect = GetChildComponent<RectTransform>("Container_content");
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            InitChildren();
            transform.SetAsLastSibling();
        }

        protected virtual void InitChildren()
        {
            AddStartContainer();
            AddEndContainer();
            AddCommonBtnContainer();
        }

        protected void AddStartContainer()
        {
            GameObject child = GetChild("Container_content/ScrollView_content/mask/content/Container_jichu");
            child.SetActive(true);
            _locaterList.Add(child.AddComponent<Locater>());
            _startContainer = child.AddComponent<StartPropertyContainer>();
            _containerList.Add(_startContainer);
        }

        protected void AddEndContainer()
        {
            GameObject child = GetChild("Container_content/ScrollView_content/mask/content/Container_jiewei");
            child.SetActive(true);
            _locaterList.Add(child.AddComponent<Locater>());
            _endContaner = child.AddComponent<EndPropertyContainer>();
            _containerList.Add(_endContaner);
        }

        protected void AddCommonBtnContainer()
        {
            GameObject child = GetChild("Container_content/Container_Buttons/Container_Common");
            child.SetActive(true);
            _commonBtnContainer = child.AddComponent<CommonFunctionContainer>();
        }

        public override void OnShow(object data)
        {
            ToolTipsDataWrapper wrapper = data as ToolTipsDataWrapper;
            _itemData = wrapper.data as ItemToolTipsData;
            _context = wrapper.context;
            AddEventListener();
            Refresh();
        }

        public override void OnClose()
        {
            ResetPosition();
            UIManager.Instance.ClosePanel(PanelIdEnum.ItemChannel);
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _dummyBtn.onClick.AddListener(OnDummyClick);
            _storyBtn.onClick.AddListener(OnStoryClick);
            EventDispatcher.AddEventListener(ItemChannelEvents.Close_Item_Channel_Tips, ResetPosition);
            EventDispatcher.AddEventListener(ItemChannelEvents.Open_Item_Channel_Tips, SetPosition);
            EventDispatcher.AddEventListener(ItemChannelEvents.Go_Item_Get_Panel, CloseTipsPanel);
        }

        private void SetPosition()
        {
            _contentRect.anchoredPosition = new Vector2(166, -62);
        }

        private void ResetPosition()
        {
            _contentRect.anchoredPosition = new Vector2(397, -62);
        }

        private void RemoveEventListener()
        {
            _dummyBtn.onClick.RemoveListener(OnDummyClick);
            _storyBtn.onClick.RemoveListener(OnStoryClick);
            EventDispatcher.RemoveEventListener(ItemChannelEvents.Close_Item_Channel_Tips, ResetPosition);
            EventDispatcher.RemoveEventListener(ItemChannelEvents.Open_Item_Channel_Tips, SetPosition);
            EventDispatcher.RemoveEventListener(ItemChannelEvents.Go_Item_Get_Panel, CloseTipsPanel);
        }
        
        // 从道具获得渠道点击前往，不仅需要关闭Tips界面，而且要关闭Tips的上层界面
        private void CloseTipsPanel()
        {
            ClosePanel();
        }

        private void OnStoryClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.ItemStory, _itemData.Story);
        }

        private void OnDummyClick()
        {
            ClosePanel();
        }

        protected virtual void Refresh()
        {
            ResetContentPosition();
            RefreshName();
            RefreshStoryBtn();
            RefreshIcon();
            RefreshExpireTime();
            RefreshContent();
            RefreshBtnContainer();
            RefreshLayout();
        }

        private void RefreshExpireTime()
        {
            // 该道具不会失效
            if(_itemData.ExpireTime == 0)
            {
                _expireTimeTxt.CurrentText.text = string.Empty;
                return;
            }

            if (_itemData.IsExpire == true)
            {
                _expireTimeTxt.CurrentText.text = string.Format("<color=#FF0000>{0}</color>", MogoLanguageUtil.GetContent(33220));
            }
            else
            {
                long leftSeconds = (long)_itemData.ExpireTime - Global.serverTimeStampSecond;
                if (leftSeconds > 24 * 3600)
                {
                    long leftDays = leftSeconds / (24 * 3600);
                    long leftHours = (leftSeconds % (24 * 3600)) / 3600;
                    _expireTimeTxt.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(33218), leftDays, leftHours);
                }
                else
                {
                    long leftHours = leftSeconds / 3600;
                    long leftMinutes = (leftSeconds % 3600) / 60;
                    _expireTimeTxt.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(33219), leftHours, leftMinutes);
                }
            }
        }

        private void ResetContentPosition()
        {
            _contentLocater.Position = Vector2.zero;
        }

        private void RefreshName()
        {
            if (Application.isEditor == true)
            {
                _nameTxt.CurrentText.text = _itemData.ColorName + " " + _itemData.Id;
            }
            else
            {
                _nameTxt.CurrentText.text = _itemData.ColorName;
            }
        }

        private void RefreshStoryBtn()
        {
            if (string.IsNullOrEmpty(_itemData.Story) == false)
            {
                _storyBtn.gameObject.SetActive(true);
            }
            else
            {
                _storyBtn.gameObject.SetActive(false);
            }
        }

        private void RefreshIcon()
        {
            _itemGrid.SetItemData(_itemData);
        }

        protected virtual void RefreshContent()
        {
            RefreshStartContainer();
            RefreshEndContainer();
        }

        protected void RefreshStartContainer()
        {
            _startContainer.Refresh(_itemData, _context);
        }

        protected void RefreshEndContainer()
        {
            _endContaner.Refresh(_itemData, _context);
        }

        protected virtual void RefreshBtnContainer()
        {
            _commonBtnContainer.Refresh(_itemData, _context);
        }

        private void RefreshLayout()
        {
            float startY = _locaterList[0].Y;
            for (int i = 0; i < _locaterList.Count; i++)
            {
                Locater locater = _locaterList[i];
                if (locater.gameObject.activeSelf == true)
                {
                    locater.Y = startY;
                    startY -= locater.Height + PropertyContainer.LAYOUT_GAP;
                }
            }
            _contentContainer.RecalculateSize();
        }
    }
}
