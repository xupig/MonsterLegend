﻿#region 模块信息
/*==========================================
// 文件名：SkillToolTipsContainer
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.ToolTips.SkillToolTips
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/16 10:52:24
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCommonUI
{
    public class SkillToolTipsContainer : KContainer
    {
        private StateImage _proficient;
        private StateImage _imgEquiped;
        private StateIcon _icon;
        private StateText _txtName;
        private StateText _txtType;
        private StateText _txtLevel;
        private StateText _txtEffect;
        private KContainer _skillPosContainer;
        private StateText _txtCd;
        private StateText _txtOutput;

        private List<StateImage> _imgPosList;

        private static string typeTemplate;
        private static string levelTemplate;
        private static string effectTemplate;
        protected override void Awake()
        {
            _txtCd = GetChildComponent<StateText>("Label_txtCd");
            _txtOutput = GetChildComponent<StateText>("Label_txtNengliangshuchu");
            _proficient = GetChildComponent<StateImage>("Image_zhuanjing");
            _imgEquiped = GetChildComponent<StateImage>("Image_sharedyizhuangbei");
            _icon = GetChildComponent<StateIcon>("Button_jinengi/stateIcon");
            _txtName = GetChildComponent<StateText>("Label_txtJinengmingcheng");
            _txtType = GetChildComponent<StateText>("Label_txtLeixing");
            _txtLevel = GetChildComponent<StateText>("Label_txtDengji");
            _txtEffect = GetChildComponent<StateText>("Label_txtXiaoguo");
            _skillPosContainer = GetChildComponent<KContainer>("Container_jinengfenzhi");
            if (_skillPosContainer != null)
            {
                _imgPosList = new List<StateImage>();
                for (int i = 0; i < 3; i++)
                {
                    _imgPosList.Add(_skillPosContainer.GetChildComponent<StateImage>("Container_pos" + i + "/Image_jinengweizhixuanzhong"));
                }
            }

            if (typeTemplate == null)
            {
                typeTemplate = _txtType.CurrentText.text;
                levelTemplate = _txtLevel.CurrentText.text;
                effectTemplate = _txtEffect.CurrentText.text;
            }

            base.Awake();
        }

        public void SetData(spell spell, spell slotedSpell)
        {
            SpellData spellData = PlayerDataManager.Instance.SpellData;
            if (_proficient != null)
            {
                _proficient.Visible = spellData.HasProficient(spell.__group);
            }
            if (_imgEquiped != null)
            {
                _imgEquiped.Visible = slotedSpell != null && slotedSpell.__id == spell.__id;
            }

            spell_sys _sys = skill_helper.GetSpellSysyByGroupId(spell.__group);

            _icon.SetIcon(spell.__icon);
            _txtName.CurrentText.text = spell.__name.ToLanguage();
            if (_imgPosList != null)
            {
                for (int i = 0; i < _imgPosList.Count; i++)
                {
                    if (spell.__pos == i + 2)
                    {
                        _imgPosList[i].Visible = true;
                    }
                    else
                    {
                        _imgPosList[i].Visible = false;
                    }
                }
            }
            int[] attri = skill_helper.GetEffectAttri(spell);

            _txtCd.CurrentText.text = (71882).ToLanguage(skill_helper.GetCd(spell));
            _txtOutput.CurrentText.text = (71881).ToLanguage(skill_helper.GetOutput(spell));
            _txtEffect.CurrentText.text = (71343).ToLanguage(attri[0], attri[1].ToLanguage(), attri[2]);
            if (PlayerDataManager.Instance.SpellData.HasSkillLearned(_sys.__id))
            {
                _txtLevel.CurrentText.text = string.Format("LV{0}", spell.__sp_level);
            }
            else
            {
                _txtLevel.CurrentText.text = string.Format("LV{0}", 0);
            }
            _txtType.CurrentText.text = skill_helper.GetPosName(spell.__pos).ToLanguage();
        }
    }
}
