﻿#region 模块信息
/*==========================================
// 文件名：SkillOneContainer
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.ToolTips.SkillToolTips
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/16 15:06:19
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCommonUI
{
    public class SkillOneContainer:KContainer
    {
        private SkillToolTipsContainer _one;
        private KButton _btnOneClose;

        protected override void Awake()
        {
            _one = gameObject.AddComponent<SkillToolTipsContainer>();
            _btnOneClose = _one.GetChildComponent<KButton>("Button_close");
            _btnOneClose.onClick.AddListener(OnClickClose);
            base.Awake();
        }

        private void OnClickClose()
        {
            PanelIdEnum.SkillToolTips.Close();
        }

        public void SetData(spell spell,spell spellSloted)
        {
            _btnOneClose.Visible = spellSloted == null;
            _one.SetData(spell, spellSloted);
        }

    }
}
