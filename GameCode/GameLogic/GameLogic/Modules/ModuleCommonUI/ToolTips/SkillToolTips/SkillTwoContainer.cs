﻿#region 模块信息
/*==========================================
// 文件名：SkillTwoContainer
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.ToolTips.SkillToolTips
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/7/16 15:12:19
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCommonUI
{
    public class SkillTwoContainer:KContainer
    {
        private KButton _btnTwoClose;
        private KScrollView _scrollView;
        private SkillToolTipsContainer _twoJunior;
        private SkillToolTipsContainer _twoSenior;

        protected override void Awake()
        {
            _btnTwoClose = GetChildComponent<KButton>("Button_close");

            _scrollView = GetChildComponent<KScrollView>("ScrollView_jinengneirong");
            _twoJunior = _scrollView.AddChildComponent<SkillToolTipsContainer>("mask/content/Container_junior");
            _twoSenior = _scrollView.AddChildComponent<SkillToolTipsContainer>("mask/content/Container_senior");
            _btnTwoClose.onClick.AddListener(OnClosePanel);
            base.Awake();
        }

        private void OnClosePanel()
        {
            PanelIdEnum.SkillToolTips.Close();
        }

        public void SetData(spell spell,spell spellSloted,spell senior)
        {
            _btnTwoClose.Visible = spellSloted == null;
            _twoJunior.SetData(spell, spellSloted);
            _twoSenior.SetData(senior, spellSloted);
        }
    }
}
