﻿using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using GameMain.GlobalManager;

namespace ModuleCommonUI
{
    public class UseItemToolTips : BasePanel
    {
        private KButton _cancelBtn;
        private KButton _useBtn;
        private KButton _addBtn;
        private KButton _reduceBtn;
        private KButton _superAddBtn;
        private KButton _superReduceBtn;
        private StateText _countTxt;
        private ItemToolTipsData _itemData;
        private int _count = 0;

        private StateImage _imagezheZhao;

        protected override PanelIdEnum Id
        {
            get 
            {
                return PanelIdEnum.UseItemToolTips;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            _cancelBtn = GetChildComponent<KButton>("Container_Buttons/Button_quxiao");
            _useBtn = GetChildComponent<KButton>("Container_Buttons/Button_shiyong");
            _addBtn = GetChildComponent<KButton>("Container_Buttons/Button_tianjia");
            _reduceBtn = GetChildComponent<KButton>("Container_Buttons/Button_jianshao");
            _superAddBtn = GetChildComponent<KButton>("Container_Buttons/Button_shuangjia");
            _superReduceBtn = GetChildComponent<KButton>("Container_Buttons/Button_shuangjian");
            _countTxt = GetChildComponent<StateText>("Container_shuliang/Label_textCount");
            _imagezheZhao = GetChildComponent<StateImage>("Container_Bg/ScaleImage_sharedZhezhao");
            _imagezheZhao.Alpha = 195.0f / 255.0f;
            AddEventListener();
        }

        private void AddEventListener()
        {
            _cancelBtn.onClick.AddListener(OnCancelClick);
            _useBtn.onClick.AddListener(OnUseClick);
            _addBtn.onClick.AddListener(OnAddClick);
            _reduceBtn.onClick.AddListener(OnReduceClick);
            _superAddBtn.onClick.AddListener(OnSuperAddClick);
            _superReduceBtn.onClick.AddListener(OnSuperReduceClick);
        }

        private void OnCancelClick()
        {
            ClosePanel();
        }

        private void OnUseClick()
        {
            ClosePanel();
            BagManager.Instance.RequestUseItem(_itemData.GridPosition, _count);
            UIManager.Instance.ClosePanel(PanelIdEnum.ItemToolTips);
        }

        private void OnAddClick()
        {
            _count = Mathf.Min(_count + 1, _itemData.StackCount);
            Refresh();
        }

        private void OnReduceClick()
        {
            _count = Mathf.Max(1, _count - 1);
            Refresh();
        }

        private void OnSuperAddClick()
        {
            _count = Mathf.Min((_count + 10) / 10 * 10, _itemData.StackCount);
            Refresh();
        }

        private void OnSuperReduceClick()
        {
            _count = Mathf.Max((_count - 1) / 10 * 10, 1);
            Refresh();
        }

        public override void OnShow(object data)
        {
            if(data is ItemToolTipsData)
            {
                _itemData = data as ItemToolTipsData;
            }
            _count = 1;
            Refresh();
        }

        private void Refresh()
        {
            _countTxt.CurrentText.text = _count.ToString();
        }

        public override void OnClose()
        {
            
        }

    }
}
