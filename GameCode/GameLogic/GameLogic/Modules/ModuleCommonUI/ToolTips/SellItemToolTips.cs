﻿#region 模块信息
/*==========================================
// 模块名：ItemToolTips
// 命名空间: ModuleCommonUI.ToolTips
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/08
// 描述说明：道具tip
// 其他：拼接信息未实现
//==========================================*/
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Structs;
using Common.Utils;
using Common.ServerConfig;
using UnityEngine.UI;
using GameMain.GlobalManager.SubSystem;
using GameLoader.Utils;
using Common.Data;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using GameData;

namespace ModuleCommonUI
{
    public class SellItemToolTips : BasePanel
    {
        private KButton _sellBtn;
        private KButton _addBtn;
        private KButton _reduceBtn;
        private KButton _tenAddBtn;
        private KButton _tenReduceBtn;
        private KButton _cancelBtn;
        private KDummyButton _dummyBtn;
        private StateText _moneyTxt;
        private StateText _countTxt;

        private ItemToolTipsData _itemData;
        private PanelIdEnum _context;
        private int _currentCount = 1;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.SellItemToolTips; }
        }

        protected override void Awake()
        {
            _sellBtn = GetChildComponent<KButton>("Container_Buttons/Button_Sell");
            _cancelBtn = GetChildComponent<KButton>("Container_Buttons/Button_quxiao");
            _dummyBtn = AddChildComponent<KDummyButton>("Container_Bg/ScaleImage_sharedZhezhao");
            _addBtn = GetChildComponent<KButton>("Container_Buttons/Button_tianjia");
            _tenAddBtn = GetChildComponent<KButton>("Container_Buttons/Button_shuangjia");
            _reduceBtn = GetChildComponent<KButton>("Container_Buttons/Button_jianshao");
            _tenReduceBtn = GetChildComponent<KButton>("Container_Buttons/Button_shuangjian");
            _moneyTxt = GetChildComponent<StateText>("Container_Contents/Container_huodejinbi/Label_textMoney");
            _moneyTxt.CurrentText.text = "0";
            _countTxt = GetChildComponent<StateText>("Container_Contents/Container_chushoushuliang/Label_textCount");
            _countTxt.CurrentText.text = "0";
        }

        public override void OnShow(object data)
        {
            ToolTipsDataWrapper wrapper = data as ToolTipsDataWrapper;
            _itemData = wrapper.data as ItemToolTipsData;
            _context = wrapper.context;
            AddEventListener();
            _currentCount = 1;
            RefreshTxt();
            transform.SetAsLastSibling();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _sellBtn.onClick.AddListener(OnClickSell);
            _cancelBtn.onClick.AddListener(ClosePanel);
            _dummyBtn.onClick.AddListener(ClosePanel);
            _addBtn.onClick.AddListener(OnAddClick);
            _tenAddBtn.onClick.AddListener(OnTenAddClick);
            _reduceBtn.onClick.AddListener(OnReduceClick);
            _tenReduceBtn.onClick.AddListener(OnTenReduceClick);
        }

        private void RemoveEventListener()
        {
            _sellBtn.onClick.RemoveListener(OnClickSell);
            _cancelBtn.onClick.RemoveListener(ClosePanel);
            _dummyBtn.onClick.RemoveListener(ClosePanel);
            _addBtn.onClick.RemoveListener(OnAddClick);
            _reduceBtn.onClick.RemoveListener(OnReduceClick);
            _tenAddBtn.onClick.RemoveListener(OnTenAddClick);
            _tenReduceBtn.onClick.RemoveListener(OnTenReduceClick);
        }

        private void RefreshTxt()
        {
            _countTxt.CurrentText.text = _currentCount.ToString();
            int sellPriceInGold = 0;
            if (_itemData.SellPrice.ContainsKey(1))
            {
                sellPriceInGold = _itemData.SellPrice[1];
            }
            _moneyTxt.CurrentText.text = (_currentCount * sellPriceInGold).ToString();
        }

        private void OnAddClick()
        {
            _currentCount = Math.Min(_itemData.StackCount, _currentCount + 1);
            RefreshTxt();
        }
        private void OnReduceClick()
        {
            _currentCount = Math.Max(1, _currentCount - 1);
            RefreshTxt();
        }

        private void OnTenAddClick()
        {
            int addCount = GetAddCount();
            if (_itemData.StackCount > 100)
            {
                addCount = 100;
            }
            _currentCount = Math.Min(_itemData.StackCount, (_currentCount + addCount) / addCount * addCount);
            RefreshTxt();
        }

        private void OnTenReduceClick()
        {
            int addCount = GetAddCount();
            _currentCount = Math.Max(1, (_currentCount - 1) / addCount * addCount);
            RefreshTxt();
        }

        private int GetAddCount()
        {
            int addCount = 10;
            if (_itemData.StackCount > 100)
            {
                addCount = 100;
            }
            return addCount;
        }

        private void OnClickSell()
        {
            if (_itemData.Quality >= 4)
            {
                string content = string.Format(MogoLanguageUtil.GetContent(5031), item_helper.GetItemQualityDesc(_itemData.Quality));
                MessageBox.Show(false, MessageBox.DEFAULT_TITLE, content, SellItem, ClosePanel, string.Empty, string.Empty);
                return;
            }
            BagManager.Instance.RequestSellItem(BagType.ItemBag, _itemData.GridPosition, _currentCount);
            ClosePanel();
            CloseContextPanel();
            EventDispatcher.TriggerEvent(BagEvents.SellItem);
        }

        private void SellItem()
        {
            BagManager.Instance.RequestSellItem(BagType.ItemBag, _itemData.GridPosition, _currentCount);
            ClosePanel();
            CloseContextPanel();
            EventDispatcher.TriggerEvent(BagEvents.SellItem);
        }

        private void CloseContextPanel()
        {
            UIManager.Instance.ClosePanel(_context);
        }
    }

}
