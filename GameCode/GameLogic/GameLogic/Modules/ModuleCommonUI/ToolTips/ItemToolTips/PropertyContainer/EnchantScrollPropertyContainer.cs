﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Structs;
using Common.Utils;
using Common.ServerConfig;
using UnityEngine.UI;
using GameMain.GlobalManager.SubSystem;
using Common.Global;
using Common.Data;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using UnityEngine.EventSystems;
using GameMain.Entities;

namespace ModuleCommonUI
{
    public class EnchantScrollPropertyContainer : StartPropertyContainer
    {
        protected override void RefreshLimit()
        {
            ClearLimitContent();
            AppendQuality();
            AppendLevel();
            AppendPosition();
            RecalculateLimitListSize();
        }

        private void AppendQuality()
        {
            //装备品质
            int startQuality = enchant_scroll_helper.GetMinEquipQuality(_itemData.Id);
            string content = string.Format("{0}：{1}", MogoLanguageUtil.GetContent(5093), item_helper.GetItemQualityDesc(startQuality));
            _limitContentList.Add(content);
        }

        private void AppendLevel()
        {
            string startLevel = enchant_scroll_helper.GetMinEquipLevel(_itemData.Id).ToString();
            string endLevel = enchant_scroll_helper.GetMaxEquipLevel(_itemData.Id).ToString();
            //装备等级
            string content = string.Format("{0}：{1}~{2}级", MogoLanguageUtil.GetContent(5094), startLevel, endLevel);
            _limitContentList.Add(content);
        }

        private void AppendPosition()
        {
            List<int> positionList = enchant_scroll_helper.GetEquipPositionList(_itemData.Id);
            //可用部位
            string content = MogoLanguageUtil.GetContent(5096) + "：";
            for(int i = 0; i < positionList.Count; i++)
            {
                content = string.Concat(content, item_helper.GetEquipTypeDesc(positionList[i]));
                if(i < positionList.Count - 1)
                {
                    content = string.Concat(content, "、");
                }
            }
            _limitContentList.Add(content);
        }
    }
}
