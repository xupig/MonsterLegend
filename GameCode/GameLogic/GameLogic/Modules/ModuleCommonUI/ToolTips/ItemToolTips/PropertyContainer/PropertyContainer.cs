﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Structs;
using Common.Utils;
using Common.ServerConfig;
using UnityEngine.UI;
using GameMain.GlobalManager.SubSystem;
using Common.Global;
using Common.Data;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using UnityEngine.EventSystems;

namespace ModuleCommonUI
{
    public abstract class PropertyContainer : KContainer
    {
        public const int LAYOUT_GAP = 7;
        protected List<Locater> _locaterList = new List<Locater>();
        protected ItemToolTipsData _itemData;
        protected PanelIdEnum _context;

        protected override void Awake()
        {
        }

        public virtual void Refresh(ItemToolTipsData itemData, PanelIdEnum context)
        {
            _itemData = itemData;
            _context = context;
            RefreshContent();
            RefreshLayout();
        }

        protected abstract void RefreshContent();

        protected virtual void RefreshLayout()
        {
            float startY = _locaterList[0].Y;
            for(int i = 0; i < _locaterList.Count; i++)
            {
                Locater locater = _locaterList[i];
                if(locater.gameObject.activeSelf == true)
                {
                    locater.Y = startY;
                    startY -= locater.Height + LAYOUT_GAP;
                }
            }
            RecalculateSize();
        }
    }

    public class PropertyItem : KList.KListItemBase
    {
        private StateText _txt;
        private string _data;
        protected override void Awake()
        {
            _txt = GetChildComponent<StateText>("Label_text");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as string;
                _txt.CurrentText.text = _data;
                _txt.RecalculateCurrentStateHeight();
                RecalculateSize();
            }
        }

        public override void Dispose()
        {

        }
    }
}
