﻿using System;
using System.Collections.Generic;

using Game.UI.UIComponent;
using UnityEngine;
using Common.Structs;
using Common.Utils;
using Common.Global;
using GameData;
using GameMain.Entities;
using Common.Data;
using Common.ExtendComponent;

namespace ModuleCommonUI
{
    public class StartPropertyContainer : PropertyContainer
    {
        /// <summary>
        /// 已绑定
        /// </summary>
        public const int LANG_BIND = 5077;
        /// <summary>
        /// 未绑定
        /// </summary>
        public const int LANG_UNBIND = 5078;
        /// <summary>
        /// Key为限制条件的类型，Value为限制条件的中文描述
        /// 1：需要等级
        /// 2：需要职业
        /// 3：需要VIP等级
        /// 4：需要制造等级
        /// </summary>
        private static Dictionary<string, int> LIMIT_LANG_DICT = new Dictionary<string,int>(){{"1", 5074}, {"2", 5075}, {"3", 5098}, {"4", 5097}};
        protected StateText _typeTxt;
        protected StateText _bindTxt;
        protected KContainer _limitContaner;
        protected KList _limitList;
        protected List<string> _limitContentList = new List<string>();
        private KContainer _priceContainer;

        private string _typeTemplate;


        protected override void Awake()
        {
            _typeTxt = GetChildComponent<StateText>("Container_leixing/Label_text");
            _typeTemplate = _typeTxt.CurrentText.text;
            _locaterList.Add(_typeTxt.transform.parent.gameObject.AddComponent<Locater>());

            _limitContaner = GetChildComponent<KContainer>("Container_xianzhi");
            _locaterList.Add(_limitContaner.gameObject.AddComponent<Locater>());
            _limitList = _limitContaner.GetChildComponent<KList>("List_tiaojian");
            _limitList.SetGap(0, 0);
            _limitList.SetPadding(0, 0, 0, 0);
            _limitList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _bindTxt = GetChildComponent<StateText>("Container_bangding/Label_text");
            _locaterList.Add(_bindTxt.transform.parent.gameObject.AddComponent<Locater>());
            base.Awake();
        }

        protected override void RefreshContent()
        {
            RefreshType();
            RefreshLimit();
            RefreshBind();
            RefreshTrade();
        }

        private void RefreshTrade()
        {
            if (_itemData.IsBind == false && trademarket_helper.ContainsItem(_itemData.Id))
            {
                InitTradeContainer();
                _priceContainer.Visible = true;
                StateText _priceTxt = _priceContainer.GetChildComponent<StateText>("Label_jiage");
                IconContainer _costIcon = _priceContainer.AddChildComponent<IconContainer>("Container_icon");
                _costIcon.SetIcon(trademarket_helper.GetTradeMoneyIcon(_itemData.Id));
                _priceTxt.CurrentText.text = string.Format("X {0}~{1}", trademarket_helper.GetTradeMinPrice(_itemData.Id), trademarket_helper.GetTradeMaxPrice(_itemData.Id));
            }
            else
            {
                if (_priceContainer != null)
                {
                    _priceContainer.Visible = false;
                }
            }
        }

        private void InitTradeContainer()
        {
            if (_priceContainer == null)
            {
                GetChild("Container_jiaoyixinxi").SetActive(true);
                _priceContainer = AddChildComponent<KContainer>("Container_jiaoyixinxi");
                _locaterList.Add(_priceContainer.gameObject.AddComponent<Locater>());
            }
        }

        protected virtual void RefreshType()
        {
            _typeTxt.CurrentText.text = string.Format(_typeTemplate, item_helper.GetItemTypeDesc((int)_itemData.Type));
        }

        protected virtual void RefreshLimit()
        {
            if (_itemData.Type == BagItemType.TreasureMap)
            {
                _limitContaner.gameObject.SetActive(false);
                return;
            }
            ClearLimitContent();
            Dictionary<string, string> limitDict = _itemData.BaseConfig.__use_limit;
            if(limitDict == null || limitDict.Count == 0)
            {
                _limitContaner.Visible = false;
                return;
            }
            _limitContaner.Visible = true;
            foreach(KeyValuePair<string, string> kvp in limitDict)
            {
                string key = kvp.Key;
                switch(key)
                {
                    case ItemUseLimit.LIMIT_LEVEL:
                        AddLevelLimit(limitDict[key], _itemData.BaseConfig.__use_effect.ContainsKey(ItemUseEffect.COMSPOSITE_GET_VALUE));
                        break;
                    case ItemUseLimit.LIMIT_VOCATION:
                        AddVocationLimit(limitDict[key]);
                        break;
                    case ItemUseLimit.LIMIT_VIP:
                        AddVipLimit(limitDict[key]);
                        break;
                    case ItemUseLimit.LIMIT_MANUFACTURE:
                        AddManufactureLimit(limitDict[key]);
                        break;
                }
            }
            RecalculateLimitListSize();
        }

        protected void ClearLimitContent()
        {
            _limitContentList.Clear();
        }

        protected void RecalculateLimitListSize()
        {
            if (_limitContentList.Count == 0)
            {
                _limitContaner.Visible = false;
                return;
            }
            _limitList.SetDataList<PropertyItem>(_limitContentList);
            _limitList.DoLayout();
            RelayoutLimitList();
            _limitList.RecalculateSize();
            _limitContaner.RecalculateSize();
        }

        private void RelayoutLimitList()
        {
            float itemHeight = 0;
            for(int i = 0; i < _limitList.ItemList.Count; i++)
            {
                StateText txt = _limitList.ItemList[i].GetComponentInChildren<StateText>();
                if (txt != null)
                {
                    txt.RecalculateAllStateHeight();
                }
                RectTransform itemRect = _limitList.ItemList[i].gameObject.GetComponent<RectTransform>();
                itemRect.anchoredPosition = new Vector2(0, -itemHeight);
                itemHeight += itemRect.rect.height;
            }
        }

        private void AddLevelLimit(string value,bool isCompose)
        {
            string content = string.Empty;
            if(isCompose)
            {
                if (PlayerAvatar.Player.level < _itemData.UsageLevelRequired)
                {
                    content = (126009).ToLanguage(value);
                }
                else
                {
                    content = (126008).ToLanguage(value);
                }
            }
            else
            {
                if (PlayerAvatar.Player.level < _itemData.UsageLevelRequired)
                {
                    content = string.Format("{0}<color={1}>{2}</color>", MogoLanguageUtil.GetContent(LIMIT_LANG_DICT[ItemUseLimit.LIMIT_LEVEL]), ColorDefine.GetColorHexToken(ColorDefine.COLOR_ID_RED), value);
                }
                else
                {
                    content = MogoLanguageUtil.GetContent(LIMIT_LANG_DICT[ItemUseLimit.LIMIT_LEVEL]) + value;
                }
            }
            AppendLimitTxt(content);
        }


        private void AddVocationLimit(string value)
        {
            int vocation = Convert.ToInt32(value);
            string content = MogoLanguageUtil.GetContent(LIMIT_LANG_DICT[ItemUseLimit.LIMIT_VOCATION]) + MogoLanguageUtil.GetContent(vocation);
            if(PlayerAvatar.Player.vocation != (Vocation)vocation)
            {
                content = string.Format("{0}<color={1}>{2}</color>", MogoLanguageUtil.GetContent(LIMIT_LANG_DICT[ItemUseLimit.LIMIT_VOCATION]), ColorDefine.GetColorHexToken(ColorDefine.COLOR_ID_RED), MogoLanguageUtil.GetContent(vocation));
            }
            AppendLimitTxt(content);
        }

        private void AddVipLimit(string value)
        {
            int vip = Convert.ToInt32(value);
            string content = MogoLanguageUtil.GetContent(LIMIT_LANG_DICT[ItemUseLimit.LIMIT_VIP]) + vip;
            if(PlayerAvatar.Player.vip_level < vip)
            {
                content = string.Format("{0}<color={1}>{2}</color>", MogoLanguageUtil.GetContent(LIMIT_LANG_DICT[ItemUseLimit.LIMIT_VIP]), ColorDefine.GetColorHexToken(ColorDefine.COLOR_ID_RED), vip);
            }
            AppendLimitTxt(content);
        }

        private void AddManufactureLimit(string value)
        {
            int manufactureLevel = Convert.ToInt32(value);
            string content = MogoLanguageUtil.GetContent(LIMIT_LANG_DICT[ItemUseLimit.LIMIT_MANUFACTURE]) + manufactureLevel;
            if(PlayerAvatar.Player.mfg_skill_level < manufactureLevel)
            {
                content = string.Format("{0}<color={1}>{2}</color>", MogoLanguageUtil.GetContent(LIMIT_LANG_DICT[ItemUseLimit.LIMIT_MANUFACTURE]), ColorDefine.GetColorHexToken(ColorDefine.COLOR_ID_RED), manufactureLevel);
            }
            AppendLimitTxt(content);
        }

        private void AppendLimitTxt(string content)
        {
            _limitContentList.Add(content);
        }

        protected virtual void RefreshBind()
        {
            int langId = 0;
            if(_itemData.IsBind == true)
            {
                langId = LANG_BIND;
                string colorToken = ColorDefine.GetColorHexToken(ColorDefine.COLOR_ID_RED);
                _bindTxt.CurrentText.text = string.Format("<color={0}>{1}</color>", colorToken, MogoLanguageUtil.GetContent(langId));
            }
            else
            {
                langId = LANG_UNBIND;
                _bindTxt.CurrentText.text = MogoLanguageUtil.GetContent(langId);
            }
            
        }
    }
}
