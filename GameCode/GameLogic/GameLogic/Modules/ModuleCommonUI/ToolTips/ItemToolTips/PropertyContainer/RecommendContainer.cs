﻿#region 模块信息
/*==========================================
// 文件名：RecommendContainer
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.ToolTips.ItemToolTips.PropertyContainer
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/1/26 20:52:15
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System.Collections.Generic;
namespace ModuleCommonUI
{
    public class RecommendContainer : KContainer
    {
        private KList _starList;
        private StateText _descTxt;

        protected override void Awake()
        {
            _starList = GetChildComponent<KList>("List_xingxing");
            _descTxt = GetChildComponent<StateText>("Label_txtjieshi");
            InitList();
        }

        private void InitList()
        {
            _starList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue);
        }

        public void SetStarNum(int num)
        {
            _starList.RemoveAll();
            List<int> starList = new List<int>();
            while (starList.Count < num)
            {
                starList.Add(1);
            }
            _starList.SetDataList<StarItem>(starList);
        }

        public void SetStarTxt(string text)
        {
            _descTxt.CurrentText.text = text;
        }
    }
}