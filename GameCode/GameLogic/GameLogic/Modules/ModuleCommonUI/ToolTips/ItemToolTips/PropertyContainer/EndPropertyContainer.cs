﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Structs;
using Common.Utils;
using Common.ServerConfig;
using UnityEngine.UI;
using GameMain.GlobalManager.SubSystem;
using Common.Global;
using Common.Data;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using UnityEngine.EventSystems;
using Common.ExtendComponent;

namespace ModuleCommonUI
{
    public class EndPropertyContainer : PropertyContainer
    {
        private KContainer _descContainer;
        private StateText _descTxt;
        private StateText _countTxt;
        private StateText _sellPriceTxt;
        private StateTextLocater _sellLocater;

        private string _countTemplate;
        private string _sellPriceTemplate;

        private IconContainer _goldIcon;
        private Locater _iconLocater;

        protected override void Awake()
        {
            base.Awake();
            _descContainer = GetChildComponent<KContainer>("Container_miaoshu");
            _descTxt = GetChildComponent<StateText>("Container_miaoshu/Label_text");
            _descTxt.CurrentText.lineSpacing = 1.0f;
            _locaterList.Add(_descTxt.transform.parent.gameObject.AddComponent<Locater>());

            _countTxt = GetChildComponent<StateText>("Container_duidie/Label_text");
            _locaterList.Add(_countTxt.transform.parent.gameObject.AddComponent<Locater>());
            _countTemplate = _countTxt.CurrentText.text;

            _sellPriceTxt = GetChildComponent<StateText>("Container_chushou/Label_text");
            _locaterList.Add(_sellPriceTxt.transform.parent.gameObject.AddComponent<Locater>());
            _sellPriceTemplate = _sellPriceTxt.CurrentText.text;
            _sellLocater = AddChildComponent<StateTextLocater>("Container_chushou/Label_text");

            _goldIcon = AddChildComponent<IconContainer>("Container_chushou/Container_currencyIcon");
            _goldIcon.SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_GOLD));
            _iconLocater = AddChildComponent<Locater>("Container_chushou/Container_currencyIcon");
        }

        protected override void RefreshContent()
        {
            RefreshDesc();
            RefreshStack();
            RefreshSellPrice();
        }

        private void RefreshDesc()
        {
            if(string.IsNullOrEmpty(_itemData.Description) == true)
            {
                _descContainer.gameObject.SetActive(false);
            }
            else
            {
                _descContainer.gameObject.SetActive(true);
                string desc = _itemData.Description.Replace("\\n", "\n");
                if (_itemData.Type == BagItemType.PetExperience)
                {
                    desc = string.Format(desc, item_helper.GetPetAddExp(_itemData.Id));
                }
                if (_itemData.Type == BagItemType.Intimate)
                {
                    desc = string.Format(desc, intimate_item_helper.GetIntimate(_itemData.Id));
                }
                _descTxt.CurrentText.text = desc;
                _descTxt.RecalculateCurrentStateHeight();
                _descContainer.RecalculateSize();
            }
        }

        private void RefreshStack()
        {
            if(_itemData.StackCount == 0)
            {
                _countTxt.CurrentText.text = string.Format(_countTemplate, _itemData.MaxStackCount.ToString());
            }
            else
            {
                _countTxt.CurrentText.text = string.Format(_countTemplate, string.Format("{0}/{1}", _itemData.StackCount.ToString(), _itemData.MaxStackCount.ToString()));
            }
        }

        private void RefreshSellPrice()
        {
            if(_itemData.SellPrice.Count > 0)
            {
                _goldIcon.gameObject.SetActive(true);
                int moneyType = 0;
                int moneyCount = 0;
                foreach (var pair in _itemData.SellPrice)
                {
                    moneyType = pair.Key;
                    moneyCount = pair.Value;
                }
                _goldIcon.SetIcon(item_helper.GetIcon(moneyType));
                _sellPriceTxt.CurrentText.text = string.Format(_sellPriceTemplate, moneyCount.ToString());
                _iconLocater.X = _sellLocater.X + _sellLocater.Width + 5.0f;
            }
            else
            {
                _goldIcon.gameObject.SetActive(false);
                //该物品不可出售
                _sellPriceTxt.CurrentText.text = MogoLanguageUtil.GetContent(5042);
            }
        }
    }
}
