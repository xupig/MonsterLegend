﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Structs;
using Common.Utils;
using Common.ServerConfig;
using UnityEngine.UI;
using GameMain.GlobalManager.SubSystem;
using Common.Global;
using Common.Data;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using UnityEngine.EventSystems;

namespace ModuleCommonUI
{
    public class SpecialPropertyContainer : PropertyContainer
    {
        private KList _propertyList;
        private KContainer _propertyContaner;
        private List<string> _propertyContentList = new List<string>();

        protected override void Awake()
        {
            _propertyContaner = GetChildComponent<KContainer>("Container_shuxing");
            _locaterList.Add(_propertyContaner.gameObject.AddComponent<Locater>());
            _propertyList = GetChildComponent<KList>("Container_shuxing/List_shuxing");
            _propertyList.SetGap(0, 0);
            _propertyList.SetPadding(0, 0, 0, 0);
            _propertyList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            base.Awake();
        }

        protected override void RefreshContent()
        {
            _propertyContentList.Clear();
            int effectId = item_gem_helper.GetAttriEffectId(_itemData.Id);
            List<string> descList = attri_effect_helper.GetAttributeDescList(effectId);
            for(int i = 0; i < descList.Count; i++)
            {
                _propertyContentList.Add(descList[i]);
            }
            _propertyList.SetDataList<PropertyItem>(_propertyContentList);
            _propertyList.RecalculateSize();
            _propertyContaner.RecalculateSize();
        }
    }
}
