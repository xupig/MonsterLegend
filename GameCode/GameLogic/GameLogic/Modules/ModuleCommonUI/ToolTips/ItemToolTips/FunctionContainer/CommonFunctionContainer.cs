﻿using System.Collections.Generic;
using Common.Base;
using Game.UI.UIComponent;
using Common.Utils;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using Common.Chat;
using ModuleCommonUI.Token;
using Common.Structs.ProtoBuf;
using GameMain.Entities;

namespace ModuleCommonUI
{
    public class CommonFunctionContainer : FunctionContainer
    {
        private KButton _displayBtn;
        private KButton _useBtn;
        private KButton _tradeBtn;
        private StateText _txtUse;

        private int currentCraftId;

        protected override void Awake()
        {
            base.Awake();
            _displayBtn = GetChildComponent<KButton>("Button_zhanshi");
            _useBtn = GetChildComponent<KButton>("Button_shiyong");
            _txtUse = _useBtn.GetChildComponent<StateText>("label");
            _tradeBtn = GetChildComponent<KButton>("Button_jiaoyi");
            AddBtnPosition(_displayBtn);
            AddBtnPosition(_tradeBtn);
            AddBtnPosition(_useBtn);
            AddBtnPosition(_getBtn);

            AddBtnVisibility(_getBtn);
            AddBtnVisibility(_sellBtn);
            AddBtnVisibility(_displayBtn);
            AddBtnVisibility(_tradeBtn);
            AddBtnVisibility(_useBtn);

            AddEventListener();
        }

        protected override void RefreshBtnVisibility()
        {
            HideAllBtn();
            switch (_context)
            {
                case PanelIdEnum.Bag:
                    ShowAllBtn();
                    RefreshUseBtnVisibility();
                    RefreshGetBtnVisibility();
                    RefreshTradeBtnVisibility();
                    break;
                case PanelIdEnum.Empty:
                case PanelIdEnum.Pet:
                    RefreshGetBtnVisibility();
                    break;
                default:
                    RefreshGetBtnVisibility();
                    break;
            }
            if (_itemData.ExpireTime != 0 && _itemData.IsExpire == true)
            {
                bool sellBtnVisible = _visibilityList[_visibilityBtnList.IndexOf(_sellBtn)];
                HideAllBtn();
                SetBtnVisibility(_sellBtn, sellBtnVisible);
            }
            ApplyBtnListVisibility();
        }

        private void RefreshTradeBtnVisibility()
        {
            SetBtnVisibility(_tradeBtn, XMLManager.trade_items.ContainsKey(_itemData.Id) && !_itemData.IsBind && function_helper.IsFunctionOpen(FunctionId.tradeMarket));
        }

        private void RefreshGetBtnVisibility()
        {
            SetBtnVisibility(_getBtn, true);
        }

        protected void RefreshUseBtnVisibility()
        {
            if (string.IsNullOrEmpty(_itemData.UseEffect.Key) == true)
            {
                SetBtnVisibility(_useBtn, false);
            }
            else
            {
                if (_itemData.UseEffectDict.ContainsKey(ItemUseEffect.COMSPOSITE_GET_VALUE))
                {
                    _txtUse.ChangeAllStateText((6031003).ToLanguage());
                }
                else
                {
                    _txtUse.ChangeAllStateText((6031007).ToLanguage());
                }
            }
        }

        private void AddEventListener()
        {
            _displayBtn.onClick.AddListener(OnDisplayClick);
            _useBtn.onClick.AddListener(OnUseClick);
            _tradeBtn.onClick.AddListener(OnTradeClick);
            EventDispatcher.AddEventListener<PbMfgFormulaInfo>(MakeEvents.FORMULA_BAG_CHANGE, LearnedFormula);
        }

        private void OnTradeClick()
        {
            // 打开交易界面
            view_helper.OpenView(135);
            ClosePanel();
        }

        private void RemoveEventListener()
        {
            _displayBtn.onClick.RemoveListener(OnDisplayClick);
            _useBtn.onClick.RemoveListener(OnUseClick);
            _tradeBtn.onClick.RemoveListener(OnTradeClick);
            EventDispatcher.RemoveEventListener<PbMfgFormulaInfo>(MakeEvents.FORMULA_BAG_CHANGE, LearnedFormula);
        }

        private void LearnedFormula(PbMfgFormulaInfo pbMfg)
        {
            if (pbMfg == null)
            {
                return;
            }
            currentCraftId = (int)pbMfg.formula_id;
            mfg_formula mfg = mfg_formula_helper.GetFormulaCfg(currentCraftId);
            string mfg_name = item_helper.GetName(int.Parse(mfg.__product["1"]));
            string info_message = string.Format(MogoLanguageUtil.GetContent(32007), mfg_name);
            MessageBox.Show(false, string.Empty, info_message, ClosePanel, null, MogoLanguageUtil.GetContent(43), string.Empty);
        }

        private void ClosePanel()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.ItemToolTips);
        }

        private void OnDisplayClick()
        {
            CloseItemToolTips(true);
            UIManager.Instance.ShowPanel(PanelIdEnum.Chat, new ChatItemLinkWrapper(_itemData.Name, GetBagType(), _itemData.GridPosition, _itemData.Quality));
        }

        private int GetBagType()
        {
            if (_context == PanelIdEnum.Information)
            {
                return (int)BagType.BodyEquipBag;
            }
            return (int)BagType.ItemBag;
        }

        private void OnUseClick()
        {
            if (string.IsNullOrEmpty(_itemData.UseEffect.Key) == false)
            {
                Dictionary<string, string> useEffectDict = _itemData.UseEffectDict;
                //如果有打开面板，优先打开面板
                if (useEffectDict.ContainsKey(ItemUseEffect.OPEN_FUNCTION_PANEL))
                {
                    OpenFunctionPanel(useEffectDict[ItemUseEffect.OPEN_FUNCTION_PANEL]);
                }
                else if (useEffectDict.ContainsKey(ItemUseEffect.LEARN_CRAFT))
                {
                    OpenCraftPopUp(int.Parse(useEffectDict[ItemUseEffect.LEARN_CRAFT]));
                }
                else if (useEffectDict.ContainsKey(ItemUseEffect.SUIT_SCROLL))
                {
                    OpenSuitScrollPanel();
                }
                else if (useEffectDict.ContainsKey(ItemUseEffect.TOKEN_EXCHANGE))
                {
                    OpenToken(useEffectDict[ItemUseEffect.TOKEN_EXCHANGE]);
                }
                else if (useEffectDict.ContainsKey(ItemUseEffect.FUNCTION_ID) && useEffectDict.ContainsKey(ItemUseEffect.VIEW_ID))
                {
                    int functionId = int.Parse(useEffectDict[ItemUseEffect.FUNCTION_ID]);
                    int viewId = int.Parse(useEffectDict[ItemUseEffect.VIEW_ID]);
                    OpenFunctionView(functionId, viewId);
                }
                else if (useEffectDict.ContainsKey(ItemUseEffect.OBTAIN_ITEM))
                {
                    UseItem();
                }
                else if (useEffectDict.ContainsKey(ItemUseEffect.OBTAIN_BUFF))
                {
                    UseItem();
                }
                else if (useEffectDict.ContainsKey(ItemUseEffect.COMSPOSITE_GET_VALUE))
                {
                    CompositeItem();
                }
                else
                {
                    UseItem();
                }
            }
        }

        private void CompositeItem()
        {
            PanelIdEnum.ItemCompose.Show(_itemData);
            CloseItemToolTips();
        }

        private void OpenFunctionView(int functionId, int viewId)
        {
            if (function_helper.IsFunctionOpen(functionId))
            {
                view_helper.OpenView(viewId);
                ClosePanel();
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(32100));
            }
        }

        private void OpenCraftPopUp(int craftId)
        {
            currentCraftId = craftId;
            if (PlayerDataManager.Instance.MakeData.learnDict.Contains(craftId))
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(5614), PanelIdEnum.ItemToolTips);
            }
            else
            {
                MakeManager.Instance.LearnFormula(craftId);
            }
        }

        private void OpenFunctionPanel(string useEffectValue)
        {
            int functionId = int.Parse(useEffectValue);
            function functionData = function_helper.GetFunction(functionId);
            if (activity_helper.IsOpen(functionData))
            {
                function_helper.Follow(functionId, null);
                CloseItemToolTips();
            }
            else
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, function_helper.GetConditionDesc(functionId));
            }
        }

        private void OpenSuitScrollPanel()
        {
            if (function_helper.IsFunctionOpen(204))
            {
                view_helper.OpenView(444);
            }
        }

        private void OpenToken(string useEffectValue)
        {
            CloseItemToolTips();
            PanelIdEnum.Token.Show((TokenType)int.Parse(useEffectValue));
        }

        private void UseItem()
        {
            if (CheckCostEnough() == false)
            {
                return;
            }
            if (CheckLimit2Enough() == false)
            {
                return;
            }
            CloseItemToolTips();
            if (_itemData.Type == BagItemType.TreasureMap)
            {
                TreasureManager.Instance.UseTreasureMap(_itemData.Id);
                UIManager.Instance.ClosePanel(PanelIdEnum.Bag);
                return;
            }
            if (_itemData.StackCount == 1)
            {
                BagManager.Instance.RequestUseItem(_itemData.GridPosition, 1);
            }
            else
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.UseItemToolTips, _itemData);
            }
        }

        private int needItemId;
        private bool CheckCostEnough()
        {
            Dictionary<int, int> cost = item_helper.GetItemUseCost(_itemData.Id);
            if (cost == null || cost.Count <= 0)
            {
                return true;
            }
            else
            {
                foreach (var pair in cost)
                {
                    if (CheckBagHasEnoughItem(pair.Key, pair.Value) == false)
                    {
                        needItemId = pair.Key;
                        string content = string.Format(MogoLanguageUtil.GetContent(1453), pair.Value, item_helper.GetName(pair.Key));
                        MessageBox.Show(true, string.Empty, content, GoAhead, CloseMessageBox, MogoLanguageUtil.GetContent(1454), MogoLanguageUtil.GetContent(1455));
                        return false;
                    }
                }
            }
            return true;
        }

        private bool CheckLimit2Enough()
        {
            Dictionary<int, List<int>> limit = item_helper.GetItemUseLimit2(_itemData.Id);
            if (limit == null || limit.Count <= 0)
            {
                return true;
            }
            else
            {
                int limitBuffKey = int.Parse(ItemUseLimit2.LIMIT_BUFF);
                if (limit.ContainsKey(limitBuffKey))
                {
                    List<int> buffIdList = limit[limitBuffKey];
                    for (int i = 0; i < buffIdList.Count; ++i)
                    {
                        if (PlayerAvatar.Player.bufferManager.ContainsBuffer(buffIdList[i]))
                        {
                            MessageBox.Show(true, string.Empty, MogoLanguageUtil.GetContent(71491), OnLimit2MsgBoxGoAhead);
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        private void OnLimit2MsgBoxGoAhead()
        {
            MessageBox.CloseMessageBox();
            UIManager.Instance.ClosePanel(PanelIdEnum.ItemToolTips);
        }

        private void CloseMessageBox()
        {
            MessageBox.CloseMessageBox();
        }

        private void GoAhead()
        {
            MessageBox.CloseMessageBox();
            UIManager.Instance.ClosePanel(PanelIdEnum.ItemToolTips);
            UIManager.Instance.ShowPanel(PanelIdEnum.ItemChannel, needItemId);
        }

        private bool CheckBagHasEnoughItem(int itemId, int itemNeedCount)
        {
            return PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(itemId) >= itemNeedCount;
        }

    }
}
