﻿using System.Collections.Generic;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Utils;
using GameMain.GlobalManager.SubSystem;
using Common.Data;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using ModuleItemChannel;

namespace ModuleCommonUI
{
    public class FunctionContainer : KContainer
    {
        protected ItemToolTipsData _itemData;
        protected PanelIdEnum _context;
        protected PanelIdEnum _openedOtherPanel = PanelIdEnum.Empty;

        protected List<KButton> _positionBtnList = new List<KButton>();
        protected List<Vector2> _positionList = new List<Vector2>();

        protected List<KButton> _visibilityBtnList = new List<KButton>();
        protected List<bool> _visibilityList = new List<bool>();

        protected KButton _sellBtn;
        protected KButton _getBtn;

        protected override void Awake()
        {
            _sellBtn = GetChildComponent<KButton>("Button_Sell");
            _sellBtn.onClick.AddListener(OnSellClick);
            _getBtn = GetChildComponent<KButton>("Button_huode");
            _getBtn.onClick.AddListener(OnGetClick);
            // 修改按钮文字,对应中文表字段
            SetBtnStateTxt(_getBtn, MogoLanguageUtil.GetContent(98173));
        }

        private void SetBtnStateTxt(KButton _getBtn, string content)
        {
            StateText[] texts = _getBtn.GetComponentsInChildren<StateText>();
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].ChangeAllStateText(content);
            }
        }

        private void OnGetClick()
        {
            ItemChannelWrapper wrapper = new ItemChannelWrapper();
            wrapper.type = 2;
            wrapper.id = _itemData.Id;
            wrapper.context = _context;
            _openedOtherPanel = PanelIdEnum.ItemChannel;
            UIManager.Instance.ShowPanel(PanelIdEnum.ItemChannel, wrapper);
        }

        protected void AddBtnPosition(KButton btn)
        {
            _positionBtnList.Add(btn);
            _positionList.Add(btn.gameObject.GetComponent<RectTransform>().anchoredPosition);
        }

        protected void SetBtnPosition(KButton btn, int index)
        {
            btn.gameObject.GetComponent<RectTransform>().anchoredPosition = _positionList[index];
            if (btn is KShrinkableButton)
            {
                (btn as KShrinkableButton).RefreshRectTransform();
            }
        }

        protected void AddBtnVisibility(KButton btn)
        {
            _visibilityBtnList.Add(btn);
            _visibilityList.Add(true);
        }

        protected void SetBtnVisibility(KButton btn, bool visibility)
        {
            if (btn == _sellBtn && _itemData.SellPrice.Count <= 0)
            {
                visibility = false;
            }
            if (btn == _getBtn && visibility == true)
            {
                if (item_channel_helper.ContainId(_itemData.Id) == true)
                {
                    visibility = true;
                }
                else
                {
                    visibility = false;
                }
            }
            _visibilityList[_visibilityBtnList.IndexOf(btn)] = visibility;
            btn.Visible = visibility;
        }

        protected void ShowAllBtn()
        {
            for (int i = 0; i < _visibilityBtnList.Count; i++)
            {
                SetBtnVisibility(_visibilityBtnList[i], true);
            }
        }

        protected void HideAllBtn()
        {
            for (int i = 0; i < _visibilityBtnList.Count; i++)
            {
                SetBtnVisibility(_visibilityBtnList[i], false);
            }
        }

        protected void ApplyBtnListVisibility()
        {
            for (int i = 0; i < _visibilityBtnList.Count; i++)
            {
                _visibilityBtnList[i].Visible = _visibilityList[i];
            }
        }

        public virtual void Refresh(ItemToolTipsData itemData, PanelIdEnum context)
        {
            _itemData = itemData;
            _context = context;
            RefreshBtnVisibility();
            RefreshBtnLayout();
            ApplyBtnListVisibility();
        }

        protected virtual void RefreshBtnVisibility()
        {

        }

        protected virtual void RefreshBtnLayout()
        {
            int index = 0;
            for (int i = 0; i < _positionBtnList.Count; i++)
            {
                if (_positionBtnList[i].Visible == true)
                {
                    SetBtnPosition(_positionBtnList[i], index);
                    index++;
                }
            }
        }

        protected virtual void CloseItemToolTips(bool isCloseContext = false)
        {
            if (_openedOtherPanel != PanelIdEnum.Empty)
            {
                UIManager.Instance.ClosePanel(_openedOtherPanel);
            }
            ToolTipsManager.Instance.CloseTip(PanelIdEnum.ItemToolTips, isCloseContext);
        }

        protected virtual void OnSellClick()
        {
            if (_openedOtherPanel != PanelIdEnum.Empty)
            {
                UIManager.Instance.ClosePanel(_openedOtherPanel);
                EventDispatcher.TriggerEvent(ItemChannelEvents.Close_Item_Channel_Tips);
            }
            if (_itemData.Quality >= 4 && _itemData.StackCount == 1)
            {
                CloseItemToolTips();
                string content = string.Format(MogoLanguageUtil.GetContent(5031), item_helper.GetItemQualityDesc(_itemData.Quality));
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, SellItem, CloseTips, string.Empty, string.Empty);
                return;
            }
            else if (_itemData.StackCount == 1)
            {
                CloseItemToolTips();
                BagManager.Instance.RequestSellItem(BagType.ItemBag, _itemData.GridPosition, 1);
                EventDispatcher.TriggerEvent(BagEvents.SellItem);
            }
            else
            {
                OpenSellItemToolTips();
            }
        }

        private void CloseTips()
        {
            MessageBox.CloseMessageBox();
        }

        private void SellItem()
        {
            BagManager.Instance.RequestSellItem(BagType.ItemBag, _itemData.GridPosition, 1);
            EventDispatcher.TriggerEvent(BagEvents.SellItem);
        }

        protected void OpenSellItemToolTips()
        {
            _openedOtherPanel = PanelIdEnum.SellItemToolTips;
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.SellItemToolTips, _itemData, GetOpenSellPanelContext());
        }

        protected virtual PanelIdEnum GetOpenSellPanelContext()
        {
            return PanelIdEnum.ItemToolTips;
        }
    }
}
