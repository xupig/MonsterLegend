﻿
using Common.Base;
using Game.UI.UIComponent;
using Common.Data;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using Common.Chat;

namespace ModuleCommonUI
{
    public class GemFunctionContainer : FunctionContainer
    {
        private KButton _displayBtn;
        private KButton _inlayBtn;
        private KButton _compositeBtn;

        protected override void Awake()
        {
            base.Awake();
            _displayBtn = GetChildComponent<KButton>("Button_zhanshi");
            _compositeBtn = GetChildComponent<KButton>("Button_Hecheng");
            _inlayBtn = GetChildComponent<KButton>("Button_xiangqian");

            AddBtnPosition(_displayBtn);
            AddBtnPosition(_compositeBtn);
            AddBtnPosition(_inlayBtn);
            AddBtnPosition(_getBtn);

            AddBtnVisibility(_getBtn);
            AddBtnVisibility(_sellBtn);
            AddBtnVisibility(_displayBtn);
            AddBtnVisibility(_compositeBtn);
            AddBtnVisibility(_inlayBtn);

            AddEventListener();
        }

        protected override void RefreshBtnVisibility()
        {
            HideAllBtn();
            switch (_context)
            {
                case PanelIdEnum.Bag:
                    RefreshBagBtnVisibility();
                    break;
                case PanelIdEnum.Gem:
                    RefreshGemBtnVisibility();
                    break;
            }
            ApplyBtnListVisibility();
        }

        private void RefreshBagBtnVisibility()
        {
            FunctionData functionData = PlayerDataManager.Instance.FunctionData;
            if (functionData.IsFunctionOpen(FunctionId.gem))
            {
                if (functionData.IsFunctionOpen(FunctionId.gemInlay))
                {
                    SetBtnVisibility(_inlayBtn, true);
                }
                if (functionData.IsFunctionOpen(FunctionId.gemComposite))
                {
                    SetBtnVisibility(_compositeBtn, true);
                }
            }
            SetBtnVisibility(_displayBtn, true);
            SetBtnVisibility(_sellBtn, true);
            SetBtnVisibility(_getBtn, true);
        }

        private void RefreshGemBtnVisibility()
        {
            SetBtnVisibility(_inlayBtn, true);
        }

        private void AddEventListener()
        {
            _displayBtn.onClick.AddListener(OnDisplayClick);
            _compositeBtn.onClick.AddListener(OnCompositeClick);
            _inlayBtn.onClick.AddListener(OnInlayClick);
        }

        private void OnDisplayClick()
        {
            CloseItemToolTips();
            UIManager.Instance.ShowPanel(PanelIdEnum.Chat, new ChatItemLinkWrapper(_itemData.Name, GetBagType(), _itemData.GridPosition, _itemData.Quality));
        }

        private int GetBagType()
        {
            return (int)BagType.ItemBag;
        }

        private void OnInlayClick()
        {
            if (_context == PanelIdEnum.Gem)
            {
                CloseItemToolTips();
                EventDispatcher.TriggerEvent<int>(GemEvents.INLAY, _itemData.GridPosition);
            }
            else
            {
                CloseItemToolTips();
                UIManager.Instance.ShowPanel(PanelIdEnum.Gem, 0);
            }
        }

        private void OnCompositeClick()
        {
            if (_context != PanelIdEnum.Gem)
            {
                CloseItemToolTips();
                UIManager.Instance.ShowPanel(PanelIdEnum.Gem, 1);
            }
            else
            {
                CloseItemToolTips();
            }
        }

        protected override void CloseItemToolTips(bool isCloseContext = false)
        {
            ToolTipsManager.Instance.CloseTip(PanelIdEnum.GemItemToolTips);
        }

        protected override PanelIdEnum GetOpenSellPanelContext()
        {
            return PanelIdEnum.GemItemToolTips;
        }
    }
}
