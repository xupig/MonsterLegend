﻿using Common.Base;
using Game.UI.UIComponent;
using GameData;

namespace ModuleCommonUI
{
    public class TokenFunctionContainer : FunctionContainer
    {
        private KButton _exchangeBtn;

        protected override void Awake()
        {
            base.Awake();
            _exchangeBtn = GetChildComponent<KButton>("Button_duihuan");
            AddBtnPosition(_exchangeBtn);
            AddBtnPosition(_getBtn);

            AddBtnVisibility(_exchangeBtn);
            AddBtnVisibility(_sellBtn);
            AddBtnVisibility(_getBtn);

            AddEventListener();
        }

        protected override void RefreshBtnVisibility()
        {
            HideAllBtn();
            switch(_context)
            {
                case PanelIdEnum.Bag:
                    ShowAllBtn();
                    break;
            }
            ApplyBtnListVisibility();
        }

        private void AddEventListener()
        {
            _exchangeBtn.onClick.AddListener(OnExchangeClick);
            _sellBtn.onClick.AddListener(OnSellClick);
        }

        private void OnExchangeClick()
        {
            CloseItemToolTips();
            PanelIdEnum.Token.Show(token_shop_helper.GetTokenTypeByItemId(_itemData.Id));
            //UIManager.Instance.ShowPanel(PanelIdEnum.Token, token_shop_helper.GetTokenTypeByItemId(_itemData.Id));
        }

        protected override void CloseItemToolTips(bool isCloseContext = false)
        {
            ToolTipsManager.Instance.CloseTip(PanelIdEnum.TokenItemToolTips);
        }

        protected override PanelIdEnum GetOpenSellPanelContext()
        {
            return PanelIdEnum.TokenItemToolTips;
        }
    }
}
