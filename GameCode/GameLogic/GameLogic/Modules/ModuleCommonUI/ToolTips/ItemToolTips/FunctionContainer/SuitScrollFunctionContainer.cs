﻿
using Common.Base;
using GameMain.GlobalManager;

namespace ModuleCommonUI
{
    public class SuitScrollFunctionContainer : CommonFunctionContainer
    {
        protected override void CloseItemToolTips(bool isCloseContext = false)
        {
            if (_openedOtherPanel != PanelIdEnum.Empty)
            {
                UIManager.Instance.ClosePanel(_openedOtherPanel);
            }
            ToolTipsManager.Instance.CloseTip(PanelIdEnum.SuitScrollToolTips);
        }

        protected override PanelIdEnum GetOpenSellPanelContext()
        {
            return PanelIdEnum.SuitScrollToolTips;
        }
    }
}
