﻿
using Common.Base;

namespace ModuleCommonUI
{
    public class EnchantScrollFunctionContainer : CommonFunctionContainer
    {
        protected override void CloseItemToolTips(bool isCloseContext = false)
        {
            ToolTipsManager.Instance.CloseTip(PanelIdEnum.EnchantScrollToolTips);
        }

        protected override PanelIdEnum GetOpenSellPanelContext()
        {
            return PanelIdEnum.EnchantScrollToolTips;
        }
    }
}
