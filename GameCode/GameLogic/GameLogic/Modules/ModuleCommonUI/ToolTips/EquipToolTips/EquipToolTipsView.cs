﻿#region 模块信息
/*==========================================
// 模块名：EquipToolTips
// 命名空间: ModuleCommonUI.ToolTips
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/08
// 描述说明：装备tip 
// 其他：拼接信息未实现
//==========================================*/
#endregion

using System.Collections.Generic;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Structs;
using Common.Utils;
using Common.ServerConfig;
using Common.Global;
using GameMain.GlobalManager;
using GameMain.Entities;
using Common.Data;
using GameData;

namespace ModuleCommonUI
{
    public class EquipToolTipsView : KContainer
    {
        public const int LANG_BIND = 5077;
        public const int LANG_UNBIND = 5078;
        private StateText _nameTxt;
        private KButton _closeBtn;
        private KButton _storyBtn;

        private StateText _vocationTxt;
        private string _vocationTemplate;
        private StateText _levelTxt;
        private string _levelTemplate;
        private StateText _bindTxt;

        private FightForceContainer _fightForceContainer;
        private RecastDescribeContainer _recastDescribeContainer;
        private EquipIconContainer _iconContainer;

        private BasisPropertyContainer _basisContainer;
        private RarityPropertyContainer _rarityContainer;
        private AttachMagicPropertyContainer _attachMagicContainer;
        private SuitPropertyContainer _suitContainer;
        private GemPropertyContainer _gemContainer;

        private Locater _contentLocater;
        private KContainer _contentContainer;
        private EquipFunctionContainer _functionContainer;
        private KButton _switchLeftBtn;
        private KButton _switchRightBtn;

        private StateText _descTxt;
        private KContainer _descContainer;
        private StateText _priceTxt;

        public KComponentEvent onWearEquip = new KComponentEvent();
        public KComponentEvent onExchange = new KComponentEvent();

        private EquipToolTipsData _equipData;
        private PanelIdEnum _context;
        private EquipToolTipsViewPosition _position;

        private List<Locater> _locaterList = new List<Locater>();


        protected override void Awake()
        {
            _nameTxt = GetChildComponent<StateText>("Container_mingzi/Label_txtName");
            _closeBtn = GetChildComponent<KButton>("Container_Bg/Button_close");
            _storyBtn = GetChildComponent<KButton>("Button_story");

            _functionContainer = AddChildComponent<EquipFunctionContainer>("Container_btns/Container_zhuangbei");
            _switchLeftBtn = GetChildComponent<KButton>("Container_btns/Container_qiehuan/Button_zuoshou");
            _switchRightBtn = GetChildComponent<KButton>("Container_btns/Container_qiehuan/Button_youshou");

            _contentContainer = GetChildComponent<KContainer>("ScrollView_content/mask/content");
            _contentLocater = AddChildComponent<Locater>("ScrollView_content/mask/content");

            _fightForceContainer = _contentContainer.AddChildComponent<FightForceContainer>("Container_zhandouli");
            _locaterList.Add(_fightForceContainer.gameObject.AddComponent<Locater>());

            _recastDescribeContainer = _contentContainer.AddChildComponent<RecastDescribeContainer>("Container_chongzhu");
            _locaterList.Add(_recastDescribeContainer.transform.gameObject.AddComponent<Locater>());

            _vocationTxt = _contentContainer.GetChildComponent<StateText>("Container_zhiye/Label_text");
            _locaterList.Add(_vocationTxt.transform.parent.gameObject.AddComponent<Locater>());
            _vocationTemplate = _vocationTxt.CurrentText.text;

            _levelTxt = _contentContainer.GetChildComponent<StateText>("Container_dengji/Label_text");
            _locaterList.Add(_levelTxt.transform.parent.gameObject.AddComponent<Locater>());
            _levelTemplate = _levelTxt.CurrentText.text;

            _bindTxt = _contentContainer.GetChildComponent<StateText>("Container_bangding/Label_text");
            _locaterList.Add(_bindTxt.transform.parent.gameObject.AddComponent<Locater>());

            _basisContainer = _contentContainer.AddChildComponent<BasisPropertyContainer>("Container_jichushuxing");
            _locaterList.Add(_basisContainer.gameObject.AddComponent<Locater>());

            _rarityContainer = _contentContainer.AddChildComponent<RarityPropertyContainer>("Container_xiyoushuxing");
            _locaterList.Add(_rarityContainer.gameObject.AddComponent<Locater>());

            _attachMagicContainer = _contentContainer.AddChildComponent<AttachMagicPropertyContainer>("Container_fumoshuxing");
            _locaterList.Add(_attachMagicContainer.gameObject.AddComponent<Locater>());

            _suitContainer = _contentContainer.AddChildComponent<SuitPropertyContainer>("Container_taozhuangshuxing");
            _locaterList.Add(_suitContainer.gameObject.AddComponent<Locater>());

            _gemContainer = _contentContainer.AddChildComponent<GemPropertyContainer>("Container_baoshishuxing");
            _locaterList.Add(_gemContainer.gameObject.AddComponent<Locater>());

            _descTxt = _contentContainer.GetChildComponent<StateText>("Container_miaoshu/Label_text");
            _descContainer = _contentContainer.GetChildComponent<KContainer>("Container_miaoshu");
            _locaterList.Add(_descContainer.gameObject.AddComponent<Locater>());
            _priceTxt = _contentContainer.GetChildComponent<StateText>("Container_chushou/Label_text");
            //该物品不可出售
            _priceTxt.CurrentText.text = MogoLanguageUtil.GetContent(5042);
            _locaterList.Add(_priceTxt.transform.parent.gameObject.AddComponent<Locater>());
            _iconContainer = AddChildComponent<EquipIconContainer>("Container_equipIcon");

            AddEventListener();
        }

        public void Refresh(EquipToolTipsData equipData, PanelIdEnum context, EquipToolTipsViewPosition position)
        {
            _equipData = equipData;
            _context = context;
            _position = position;
            ResetContentPosition();
            RefreshName();
            RefreshIcon();
            RefreshFunctionContainer();
            RefreshFightForceContainer();
            RefreshRecastContainer();
            RefreshVocation();
            RefreshLevel();
            RefreshBind();
            RefreshPropertyContainer();
            RefreshStoryBtn();
            RefreshDesc();
            RefreshSwitchBtn();
            RefreshLayout();
        }

        private void RefreshRecastContainer()
        {
            if (_equipData.EquipState != EquipState.NotHave && equip_recast_helper.CanRecast(_equipData.Id))
            {
                _recastDescribeContainer.Visible = true;
                _recastDescribeContainer.Refresh(_equipData);
            }
            else
            {
                _recastDescribeContainer.Visible = false;
            }
        }

        private void RefreshFightForceContainer()
        {
            _fightForceContainer.HideComparisionFlag();
            if (_equipData.EquipDataType == EquipDataType.FromServer)
            {
                _fightForceContainer.SetFightForce(_equipData.FightForce);
            }
            else if (_equipData.EquipDataType == EquipDataType.ClientCalculate)
            {
                _fightForceContainer.SetFightForce(_equipData.BaseFightForce + equip_random_helper.GetExtraMinFightForce(_equipData.Id), _equipData.BaseFightForce + equip_random_helper.GetExtraMaxFightForce(_equipData.Id, true));
            }
        }

        public void SetFightForceComparision(int fightForce)
        {
            if (_equipData.EquipDataType == EquipDataType.FromServer)
            {
                _fightForceContainer.HideComparisionFlag();
                if (_equipData.FightForce < fightForce)
                {
                    _fightForceContainer.SetComparisionDownArrow();
                }
                if (_equipData.FightForce > fightForce)
                {
                    _fightForceContainer.SetComparisionUpArrow();
                }
            }
        }

        private void ResetContentPosition()
        {
            _contentLocater.Position = Vector2.zero;
        }

        private void RefreshName()
        {
            if (Application.isEditor == true)
            {
                _nameTxt.CurrentText.text = _equipData.ColorName + " " + _equipData.Id;
            }
            else
            {
                _nameTxt.CurrentText.text = _equipData.ColorName;
            }
        }

        private void RefreshIcon()
        {
            _iconContainer.Refresh(_equipData, _context, _position);
        }

        private void RefreshFunctionContainer()
        {
            _functionContainer.Refresh(_equipData, _context);
            if ((_context == PanelIdEnum.EquipSmelter || _context == PanelIdEnum.Bag) && _position == EquipToolTipsViewPosition.Left)
            {
                _functionContainer.gameObject.SetActive(false);
            }
        }

        private void RefreshLevel()
        {
            string content = _equipData.UsageLevelRequired.ToString();
            if (PlayerAvatar.Player.level < _equipData.UsageLevelRequired)
            {
                content = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, content);
            }
            _levelTxt.CurrentText.text = string.Format(_levelTemplate, content);
        }

        private void RefreshVocation()
        {
            string content = MogoLanguageUtil.GetContent((int)_equipData.UsageVocationRequired);
            if (PlayerAvatar.Player.vocation != _equipData.UsageVocationRequired)
            {
                content = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, content);
            }
            _vocationTxt.CurrentText.text = string.Format(_vocationTemplate, content);
        }

        private void RefreshBind()
        {
            string content = MogoLanguageUtil.GetContent(LANG_UNBIND);
            if (_equipData.IsBind == true)
            {
                content = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, MogoLanguageUtil.GetContent(LANG_BIND));
            }
            _bindTxt.CurrentText.text = content;
        }

        private void RefreshPropertyContainer()
        {
            _basisContainer.Refresh(_equipData, _context);
            _rarityContainer.Refresh(_equipData, _context);
            _attachMagicContainer.Refresh(_equipData, _context);
            _suitContainer.Refresh(_equipData.SuitId);
            _gemContainer.Refresh(_equipData);
        }

        private void RefreshStoryBtn()
        {
            if (string.IsNullOrEmpty(_equipData.Story) == false)
            {
                _storyBtn.Visible = true;
            }
            else
            {
                _storyBtn.Visible = false;
            }
        }

        private void RefreshDesc()
        {
            _descTxt.transform.parent.gameObject.SetActive(false);
            if (string.IsNullOrEmpty(_equipData.Description) == false)
            {
                _descTxt.transform.parent.gameObject.SetActive(true);
                _descTxt.CurrentText.text = _equipData.Description;
                _descTxt.RecalculateCurrentStateHeight();
                _descContainer.RecalculateSize();
            }
        }

        private void RefreshSwitchBtn()
        {
            _switchLeftBtn.gameObject.SetActive(false);
            _switchRightBtn.gameObject.SetActive(false);
            if ((_context == PanelIdEnum.Bag || _context == PanelIdEnum.EquipSmelter) && _position == EquipToolTipsViewPosition.Left)
            {
                BagData bodyEquipData = PlayerDataManager.Instance.BagData.GetBodyEquipBagData();
                if (_equipData.GridPosition == public_config.EQUIP_POS_LEFTRING)
                {
                    if (bodyEquipData.GetItemInfo(public_config.EQUIP_POS_RIGHTRING) != null)
                    {
                        _switchRightBtn.gameObject.SetActive(true);
                    }
                }
                else if (_equipData.GridPosition == public_config.EQUIP_POS_RIGHTRING)
                {
                    if (bodyEquipData.GetItemInfo(public_config.EQUIP_POS_LEFTRING) != null)
                    {
                        _switchLeftBtn.gameObject.SetActive(true);
                    }
                }
            }
        }

        private void RefreshLayout()
        {
            float startY = _locaterList[0].Y;
            for (int i = 0; i < _locaterList.Count; i++)
            {
                Locater locater = _locaterList[i];
                if (locater.gameObject.activeSelf == true)
                {
                    locater.Y = startY;
                    startY -= (locater.Height + 7);
                }
            }
            _contentContainer.RecalculateSize();
        }

        private void AddEventListener()
        {
            _closeBtn.onClick.AddListener(OnCloseClick);
            _storyBtn.onClick.AddListener(OnStoryClick);
            _switchLeftBtn.onClick.AddListener(OnSwitchClick);
            _switchRightBtn.onClick.AddListener(OnSwitchClick);
            _functionContainer.onWearEquip.AddListener(OnWearEquip);
        }

        private void OnCloseClick()
        {
            Close();
        }

        private void OnStoryClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.ItemStory, _equipData.Story);
        }

        private void OnSwitchClick()
        {
            BagData bodyEquipBag = PlayerDataManager.Instance.BagData.GetBodyEquipBagData();
            EquipItemInfo equipInfo = null;
            if (_equipData.GridPosition == public_config.EQUIP_POS_LEFTRING)
            {
                equipInfo = bodyEquipBag.GetItemInfo(public_config.EQUIP_POS_RIGHTRING) as EquipItemInfo;
            }
            else if (_equipData.GridPosition == public_config.EQUIP_POS_RIGHTRING)
            {
                equipInfo = bodyEquipBag.GetItemInfo(public_config.EQUIP_POS_LEFTRING) as EquipItemInfo;
            }
            if (equipInfo != null)
            {
                EquipToolTipsData equipData = EquipToolTipsData.CopyFromEquipInfo(equipInfo);
                equipData.EquipState = EquipState.Wearing;
                Refresh(equipData, _context, _position);
            }
            onExchange.Invoke();
        }

        private void OnWearEquip()
        {
            onWearEquip.Invoke();
        }

        protected override void OnDestroy()
        {

        }

        private void Close()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.EquipToolTips);
        }

        public EquipToolTipsData EquipData
        {
            get
            {
                return _equipData;
            }
        }

        public void HideCloseBtn()
        {
            _closeBtn.gameObject.SetActive(false);
        }

        public int GetEquipFightForce()
        {
            return _equipData != null ? _equipData.FightForce : 0;
        }
    }
}
