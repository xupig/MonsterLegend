﻿using System.Collections.Generic;

using Common.Base;
using Game.UI.UIComponent;
using Common.Data;

namespace ModuleCommonUI
{
    public class EquipPropertyContainer : KContainer
    {
        protected KList _propertyList;
        protected List<string> _propertyContentList = new List<string>();
        protected EquipToolTipsData _equipData;
        protected PanelIdEnum _context;

        protected override void Awake()
        {
            _propertyList = GetChildComponent<KList>("List_shuxing");
            _propertyList.SetPadding(0, 0, 0, 0);
            _propertyList.SetGap(5, 0);
            _propertyList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        public void Refresh(EquipToolTipsData equipData, PanelIdEnum context)
        {
            _equipData = equipData;
            _context = context;
            RefreshContent();
            RecalculateSize();
        }

        protected virtual void RefreshContent()
        {

        }

        protected void Hide()
        {
            gameObject.SetActive(false);
        }

        protected void Show()
        {
            gameObject.SetActive(true);
        }

    }

    public class EquipPropertyItem : KList.KListItemBase
    {
        private StateText _txt;
        private string _data;
        protected override void Awake()
        {
            _txt = GetChildComponent<StateText>("Label_textValue");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as string;
                _txt.ChangeAllStateText(_data);
            }
        }

        public StateText Label
        {
            get
            {
                return _txt;
            }
        }

        public override void Dispose()
        {
            _data = null;
        }
    }


}
