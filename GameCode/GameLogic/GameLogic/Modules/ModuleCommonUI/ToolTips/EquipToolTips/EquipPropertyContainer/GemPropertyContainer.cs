﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Structs;
using Common.Utils;
using Common.ServerConfig;
using UnityEngine.UI;
using GameMain.GlobalManager.SubSystem;
using Common.Global;
using Common.Data;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using UnityEngine.EventSystems;
using Common.Structs.ProtoBuf;

namespace ModuleCommonUI
{
    public class GemPropertyContainer : KContainer
    {
        private KList _propertyList;

        private EquipToolTipsData _equipData;
        private List<GemPropertyItemData> _propertyDataList = new List<GemPropertyItemData>();

        protected override void Awake()
        {
            _propertyList = GetChildComponent<KList>("List_baoshi");
            _propertyList.SetPadding(0, 0, 0, 0);
            _propertyList.SetGap(5, 0);
            _propertyList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        public void Refresh(EquipToolTipsData equipData)
        {
            _equipData = equipData;
            _propertyDataList.Clear();
            AddSlotItem();
            AddNewSlotItem();
            _propertyList.SetDataList<GemPropertyItem>(_propertyDataList);
            _propertyList.RecalculateSize();
            RecalculateSize();
        }

        private void AddSlotItem()
        {
            int slotCount = _equipData.SlotList.Count;
            for(int i = 0; i < slotCount; i++)
            {
                GemPropertyItemData data = new GemPropertyItemData();
                data.slotType = Convert.ToInt32(_equipData.SlotList[i]);
                data.state = GemSlotState.Empty;
                PbEquipGems gem = GetGem(i + GemManager.SLOT_INDEX_START);
                if(gem != null)
                {
                    data.gem = gem;
                    data.state = GemSlotState.Occupied;
                }
                _propertyDataList.Add(data);
            }
        }

        private void AddNewSlotItem()
        {
            int newSlotCount = item_helper.GetEquipOpenableNewSlotCount(_equipData.Id);
            int openedNewSlotCount = _equipData.OpenedNewSlotCount;
            for(int i = 0; i < newSlotCount; i++)
            {
                GemPropertyItemData data = new GemPropertyItemData();
                data.slotType = Convert.ToInt32(_equipData.NewSlotList[i]);
                if(i < openedNewSlotCount)
                {
                    data.state = GemSlotState.Empty;
                }
                else
                {
                    data.state = GemSlotState.Locked;
                }
                PbEquipGems gem = GetGem(i + GemManager.NEW_SLOT_INDEX_START);
                if(gem != null)
                {
                    data.gem = gem;
                    data.state = GemSlotState.Occupied;
                }
                _propertyDataList.Add(data);
            }
        }

        private PbEquipGems GetGem(int gemIndex)
        {
            for(int i = 0; i < _equipData.GemList.Count; i++)
            {
                PbEquipGems gem = _equipData.GemList[i];
                if(gem.index == gemIndex)
                {
                    return gem;
                }
            }
            return null;
        }

    }

    public class GemPropertyItem : KList.KListItemBase
    {
        private GemPropertyItemData _data;

        private StateText _txt;
        private KContainer _slotIconContainer;

        protected override void Awake()
        {
            _txt = GetChildComponent<StateText>("Label_text");
            _slotIconContainer = GetChildComponent<KContainer>("Container_baoshicao");
        }

        private void HideAllSlotIcon()
        {
            for(int i = 0; i < _slotIconContainer.transform.childCount; i++)
            {
                _slotIconContainer.transform.GetChild(i).gameObject.SetActive(false);
            }
        }

        private void RefreshSlotIcon()
        {
            if(_data.state == GemSlotState.Locked)
            {
                GetIconContainer(0).gameObject.SetActive(true);
                return;
            }
            KContainer iconContainer = GetIconContainer(_data.slotType);
            iconContainer.gameObject.SetActive(true);
            if(_data.state == GemSlotState.Empty)
            {
                iconContainer.GetChild(1).gameObject.SetActive(false);
            }
            else if(_data.state == GemSlotState.Occupied)
            {
                iconContainer.GetChild(1).gameObject.SetActive(true);
            }
        }

        private KContainer GetIconContainer(int slotType)
        {
            return _slotIconContainer.transform.FindChild("Container_t" + slotType).GetComponent<KContainer>();
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if(value != null)
                {
                    _data = value as GemPropertyItemData;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            HideAllSlotIcon();
            RefreshSlotIcon();
            RefreshSlotDesc();
        }

        private void RefreshSlotDesc()
        {
            if(_data.state == GemSlotState.Locked)
            {
                //未开启
                _txt.CurrentText.text = MogoLanguageUtil.GetContent(5092);
            }
            else if(_data.state == GemSlotState.Empty)
            {
                //未镶嵌
                _txt.CurrentText.text = MogoLanguageUtil.GetContent(5091);
            }
            else
            {
                ShowGemDesc();
            }
        }

        private void ShowSlotDesc()
        {
            _txt.CurrentText.text = item_gem_helper.GetGemSlotDesc(_data.slotType);
        }

        private void ShowGemDesc()
        {
            int gemId = (int)_data.gem.gem_id;
            List<string> descList = attri_effect_helper.GetAttributeDescList(item_gem_helper.GetAttriEffectId(gemId));
            string content = string.Empty;
            for(int i = 0; i < descList.Count; i++)
            {
                content += descList[i];
            }
            _txt.CurrentText.text = content;
        }

        public override void Dispose()
        {
            _data = null;
        }
    }

    public class GemPropertyItemData
    {
        public GemSlotState state;
        public PbEquipGems gem;
        public int slotType;

    }

    public enum GemSlotState
    {
        Empty,
        Occupied,
        Locked,
    }
}
