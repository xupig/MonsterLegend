﻿using System.Collections.Generic;

using Game.UI.UIComponent;
using UnityEngine;
using Common.Utils;
using GameData;
using Common.Data;

namespace ModuleCommonUI
{
    public class RarityPropertyContainer : EquipPropertyContainer
    {
        protected override void RefreshContent()
        {
            _propertyContentList.Clear();
            RefreshRarityPropertyContent();
            RefreshBuffContent();
            if (_propertyContentList.Count <= 0 && _equipData.EquipState == EquipState.NotHave && equip_recast_helper.CanRecast(_equipData.Id))
            {
                _propertyContentList.Add(string.Format("<color=#FF0000>{0}</color>", MogoLanguageUtil.GetContent(9154)));
                _propertyContentList.Add(string.Format("<color=#FF0000>{0}</color>", MogoLanguageUtil.GetContent(9155)));
            }
            if (_propertyContentList.Count <= 0)
            {
                Hide();
                return;
            }
            Show();
            _propertyList.SetDataList<EquipPropertyItem>(_propertyContentList);
            AdjustProperityList();
            _propertyList.SetGap(5, 0);
            _propertyList.RecalculateSize();
        }

        private void AdjustProperityList()
        {
            Vector2 offsetVector = new Vector2(0,0);
            for (int i = 0; i < _propertyList.ItemList.Count; i++)
            {
                EquipPropertyItem item = _propertyList.ItemList[i] as EquipPropertyItem;
                StateText[] texts = item.transform.GetComponentsInChildren<StateText>();
                for (int j = 0; j < texts.Length; j++)
                {
                    texts[j].RecalculateAllStateHeight();
                }
                item.RecalculateSize();
                RectTransform rectTransform = item.GetComponent<RectTransform>();
                rectTransform.anchoredPosition = offsetVector;
                offsetVector.y -= Mathf.CeilToInt(rectTransform.rect.height);
            }
        }

        private void RefreshRarityPropertyContent()
        {
            List<int> rarityPropertyIdList = _equipData.GetRarityPropertyIdList();
            List<int> rarityPropertyValueList = _equipData.GetRarityPropertyValueList();
            for (int i = 0; i < rarityPropertyIdList.Count; i++)
            {
                string content = string.Format("{0}  +{1}", attri_config_helper.GetAttributeName(rarityPropertyIdList[i]), rarityPropertyValueList[i].ToString());
                _propertyContentList.Add(content);
            }
        }

        private void RefreshBuffContent()
        {
            List<uint> buffList = _equipData.BuffList;
            if (buffList == null || buffList.Count <= 0)
            {
                return;
            }
            for (int i = 0; i < buffList.Count; i++)
            {
                if (buff_helper.CheckBuffId((int)buffList[i]) == false)
                {
                    continue;
                }
                string buffName = buff_helper.GetName((int)buffList[i]);
                string buffDescibe = buff_helper.GetDesc((int)buffList[i]);
                string content = string.Format("{0}:{1}", buffName, buffDescibe);
                _propertyContentList.Add(content);
            }
        }
    }
}
