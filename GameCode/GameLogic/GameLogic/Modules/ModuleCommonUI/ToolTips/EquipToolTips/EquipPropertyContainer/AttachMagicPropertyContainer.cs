﻿using System.Collections.Generic;
using GameData;
using Common.Structs.ProtoBuf;

namespace ModuleCommonUI
{
    public class AttachMagicPropertyContainer : EquipPropertyContainer
    {
        protected override void RefreshContent()
        {
            this.gameObject.SetActive(true);
            List<PbEquipEnchantAttri> EquipEnchantList = _equipData.EnchantAttri;
            if (EquipEnchantList == null || EquipEnchantList.Count == 0)
            {
                this.Visible = false;
                return;
            }
            _propertyContentList.Clear();
            this.Visible = true;
            for (int i = 0; i < _equipData.EnchantAttri.Count; i++)
            {
                PbEquipEnchantAttri pbEquipEnchant = _equipData.EnchantAttri[i];
                string content = string.Format("{0}   +{1}", attri_config_helper.GetAttributeName((int)pbEquipEnchant.enchant_attri_id), pbEquipEnchant.enchant_attri_value);
                _propertyContentList.Add(content);
            }
            _propertyList.SetDataList<EquipPropertyItem>(_propertyContentList);
            _propertyList.SetGap(5, 0);
            _propertyList.RecalculateSize();
        }
    }
}
