﻿#region 模块信息
/*==========================================
// 文件名：RecastDescribeContainer
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.ToolTips.EquipToolTips.EquipPropertyContainer
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/3/30 19:08:44
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
namespace ModuleCommonUI
{
    public class RecastDescribeContainer : KContainer
    {
        private StateText _descTxt;
        private EquipToolTipsData _data;

        protected override void Awake()
        {
            _descTxt = GetChildComponent<StateText>("Label_chongzhuzhanli");
        }

        public void Refresh(EquipToolTipsData data)
        {
            _data = data;
            int minFightForce = GetBaseFightForce() + GetExtraMinFightForce();
            int maxFightForce = GetBaseFightForce() + GetExtraMaxFightForce();
            _descTxt.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(9153), minFightForce, maxFightForce);
        }

        private int GetBaseFightForce()
        {
            return _data.BaseFightForce;
        }

        private int GetExtraMaxFightForce()
        {
            if (equip_random_helper.ContainsId(_data.Id))
            {
                return equip_random_helper.GetExtraMaxFightForce(_data.Id);
            }
            return 0;
        }

        private int GetExtraMinFightForce()
        {
            if (equip_random_helper.ContainsId(_data.Id))
            {
                return equip_random_helper.GetExtraMinFightForce(_data.Id);
            }
            return 0;
        }
    }
}
