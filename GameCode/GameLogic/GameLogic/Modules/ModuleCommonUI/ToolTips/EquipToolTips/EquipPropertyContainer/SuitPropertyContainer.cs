﻿using System.Collections.Generic;

using Game.UI.UIComponent;
using GameData;
using MogoEngine.Events;
using Common.Events;

namespace ModuleCommonUI
{
    public class SuitPropertyContainer : KContainer
    {
        private KPageableList _propertyList;
        private StateText _nameTxt;
        private StateText _countTxt;
        private string _countTemplate;
        private int _suitId;
        private List<string> _propertyContentList = new List<string>();

        protected override void Awake()
        {
            base.Awake();
            _propertyList = GetChildComponent<KPageableList>("List_shuxing");
            _propertyList.SetPadding(0, 0, 0, 0);
            _propertyList.SetGap(5, 0);
            _propertyList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _nameTxt = GetChildComponent<StateText>("Container_taozhuang/Label_txtName");
            _countTxt = GetChildComponent<StateText>("Container_taozhuang/Label_txtCount");
            _countTemplate = _countTxt.CurrentText.text;
            EventDispatcher.AddEventListener(EquipSuitScrollEvents.Refresh, Refresh);
        }

        public void Refresh(int suitId)
        {
            _suitId = suitId;
            if(_suitId != 0)
            {
                gameObject.SetActive(true);
                _suitId = suitId;
                _nameTxt.CurrentText.text = equip_suit_helper.GetSuitName(_suitId);
                _countTxt.CurrentText.text = string.Format(_countTemplate, equip_suit_helper.GetEquipedSuitPartCount(_suitId), equip_suit_helper.GetSuitPartCount(_suitId));
                RefreshList();
                RecalculateSize();
            }
            else
            {
                gameObject.SetActive(false);
            }
        }

        public void Refresh()
        {
            if (_suitId != 0)
            {
                _nameTxt.CurrentText.text = equip_suit_helper.GetSuitName(_suitId);
                _countTxt.CurrentText.text = string.Format(_countTemplate, equip_suit_helper.GetEquipedSuitPartCount(_suitId), equip_suit_helper.GetSuitPartCount(_suitId));
                RefreshList();
                RecalculateSize();
            }
        }

        private void RefreshList()
        {
            int showNum = 0;
            _propertyContentList.Clear();
            int equipedCount = equip_suit_helper.GetEquipedSuitPartCount(_suitId);
            Dictionary<int, int> equipAttriDict = equip_suit_helper.GetSuitEffectDict(_suitId);
            foreach (var pair in equipAttriDict)
            {
                int equipedNum = pair.Key;
                int attriId = pair.Value;
                List<string> contentList = attri_effect_helper.GetAttributeDescList(attriId);
                bool isShow = equipedCount >= equipedNum;
                showNum += isShow ? contentList.Count : 0;
                for (int i = 0; i < contentList.Count; i++)
                {
                    string content;
                    if (i == 0)
                    {
                        content = equipedNum.ToString() + "件： " + contentList[i];
                        _propertyContentList.Add(content);
                        
                    }
                    else
                    {
                        content = "    " + contentList[i];
                        _propertyContentList.Add(content);
                    }
                }
            }
            _propertyList.SetDataList<EquipPropertyItem>(_propertyContentList);
            for(int i = 0; i < _propertyContentList.Count; i++)
            {
                if(showNum > i)
                {
                    (_propertyList.ItemList[i] as EquipPropertyItem).Label.State = "meet";
                }
                else
                {
                    (_propertyList.ItemList[i] as EquipPropertyItem).Label.State = "normal";
                }
            }
        }
    }
}
