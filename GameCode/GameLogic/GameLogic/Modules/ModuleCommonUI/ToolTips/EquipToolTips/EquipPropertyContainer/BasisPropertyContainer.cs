﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Structs;
using Common.Utils;
using Common.ServerConfig;
using UnityEngine.UI;
using GameMain.GlobalManager.SubSystem;
using Common.Global;
using Common.Data;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using UnityEngine.EventSystems;

namespace ModuleCommonUI
{
    public class BasisPropertyContainer : EquipPropertyContainer
    {
        // “伤害”的中文ID
        private const int HURT_CHINESE_ID = 4318;
        private const int StrengthenLevel_CHINESE_ID = 9043;

        private const int HURT_MAX = 160;
        private const int HURT_MIN = 161;

        private StateText _basisTitle;
        private string _basisTitleTemplate;

        protected override void Awake()
        {
            base.Awake();
            _basisTitle = GetChildComponent<StateText>("Label_txtName");
            _basisTitleTemplate = _basisTitle.CurrentText.text;
        }

        protected override void RefreshContent()
        {
            Dictionary<int, int> strengthenAtrriDict = new Dictionary<int, int>();
            int strengthenLevel = 0;
            if (_equipData.StrengthenLevel > 0)
            {
                strengthenLevel = _equipData.StrengthenLevel;
                strengthenAtrriDict = strengthen_helper.GetStrengthenAttribute(_equipData.SubType, _equipData.StrengthenLevel);
            }
            if (strengthenLevel > 0 && strengthenLevel < strengthen_helper.MaxStrengthenLevel)
            {
                int startCounts = (strengthenLevel - 1) % 10 + 1;
                int step = (strengthenLevel - 1) / 10 + 1;
                string content = string.Format(MogoLanguageUtil.GetContent(11017), step, startCounts);
                _basisTitle.CurrentText.text = string.Format(_basisTitleTemplate, content);
            }
            else if (strengthenLevel == strengthen_helper.MaxStrengthenLevel)
            {
                int step = strengthenLevel / 10 + 1;
                string content = string.Format(MogoLanguageUtil.GetContent(11018), step);
                _basisTitle.CurrentText.text = string.Format(_basisTitleTemplate, content);
            }
            else
            {
                _basisTitle.CurrentText.text = MogoLanguageUtil.GetContent(5065);
            }
            _propertyContentList.Clear();
            Dictionary<int, int> basisPropertyDict = new Dictionary<int, int>();
            List<int> basisPropertyIdList = _equipData.GetBasisPropertyIdList();
            List<int> basisPropertyValueList = _equipData.GetBasisPropertyValueList();
            if (basisPropertyIdList.Count != basisPropertyValueList.Count)
            {
                throw new Exception("装备属性配置Id和值长度不一致:  " + _equipData.Id + " " + _equipData.ValueConfig.__id);
            }
            if (basisPropertyIdList.Count == 0)
            {
                Hide();
                return;
            }
            Show();
            for (int i = 0; i < basisPropertyIdList.Count && i < basisPropertyValueList.Count; i++)
            {
                basisPropertyDict.Add(basisPropertyIdList[i], basisPropertyValueList[i]);
            }
            if (basisPropertyIdList.Contains(HURT_MIN) && basisPropertyIdList.Contains(HURT_MAX))
            {
                string content = string.Format("{0}  +{1}~{2}", MogoLanguageUtil.GetContent(HURT_CHINESE_ID), basisPropertyDict[HURT_MIN], basisPropertyDict[HURT_MAX]);
                int addedHurtMin = 0;
                int addedHurtMax = 0;
                if (strengthenAtrriDict.ContainsKey(HURT_MIN))
                {
                    addedHurtMin = strengthenAtrriDict[HURT_MIN];
                }
                if (strengthenAtrriDict.ContainsKey(HURT_MAX))
                {
                    addedHurtMax = strengthenAtrriDict[HURT_MAX];
                }
                if (addedHurtMin != 0 || addedHurtMax != 0)
                {
                    content += string.Format("   <color=#4feb01>+{0}~{1}</color>", addedHurtMin, addedHurtMax);
                }
                basisPropertyIdList.Remove(HURT_MAX);
                basisPropertyIdList.Remove(HURT_MIN);
                _propertyContentList.Add(content);
            }
            for (int i = 0; i < basisPropertyIdList.Count; i++)
            {
                string content = string.Format("{0}  +{1}",
                    attri_config_helper.GetAttributeName(basisPropertyIdList[i]),
                    basisPropertyDict[basisPropertyIdList[i]].ToString());
                if (strengthenAtrriDict != null && strengthenAtrriDict.ContainsKey(basisPropertyIdList[i]))
                {
                    content += string.Format("   <color=#4feb01>+{0}</color>", strengthenAtrriDict[basisPropertyIdList[i]]);
                }
                _propertyContentList.Add(content);
            }
            _propertyList.SetDataList<EquipPropertyItem>(_propertyContentList);
            _propertyList.SetGap(5, 0);
            _propertyList.RecalculateSize();
        }
    }
}
