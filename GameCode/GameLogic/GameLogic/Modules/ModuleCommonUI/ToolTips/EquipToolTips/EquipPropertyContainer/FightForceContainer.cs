﻿#region 模块信息
/*==========================================
// 文件名：FightForceContainer
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.ToolTips.EquipToolTips.EquipPropertyContainer
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/3/31 16:59:53
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Game.UI.UIComponent;
using UnityEngine;
namespace ModuleCommonUI
{
    public class FightForceContainer : KContainer
    {
        private ArtNumberList _numberList;
        private StateImage _upImage;
        private StateImage _downImage;

        protected override void Awake()
        {
            _numberList = AddChildComponent<ArtNumberList>("List_numbers");
            _upImage = GetChildComponent<StateImage>("Image_tisheng");
            _downImage = GetChildComponent<StateImage>("Image_xiajiang");
        }

        public void SetFightForce(int fightForce)
        {
            _numberList.SetNumber(fightForce);
        }

        public void SetFightForce(int minFightForce, int maxFightForce)
        {
            _numberList.SetNumber(string.Format("{0}~{1}", minFightForce, maxFightForce));
        }

        public void HideComparisionFlag()
        {
            _upImage.Visible = false;
            _downImage.Visible = false;
        }

        public void SetComparisionUpArrow()
        {
            RectTransform fightPowerRect = _numberList.GetComponent<RectTransform>();
            Vector2 fightForceNumAnchorPosition = fightPowerRect.anchoredPosition;
            _upImage.Visible = true;
            _downImage.Visible = false;
            _upImage.GetComponent<RectTransform>().anchoredPosition = new Vector2(fightForceNumAnchorPosition.x + fightPowerRect.rect.width + 5, -4);
        }

        public void SetComparisionDownArrow()
        {
            RectTransform fightPowerRect = _numberList.GetComponent<RectTransform>();
            Vector2 fightForceNumAnchorPosition = fightPowerRect.anchoredPosition;
            _upImage.Visible = false;
            _downImage.Visible = true;
            _downImage.GetComponent<RectTransform>().anchoredPosition = new Vector2(fightForceNumAnchorPosition.x + fightPowerRect.rect.width + 5, -4);
        }
    }
}