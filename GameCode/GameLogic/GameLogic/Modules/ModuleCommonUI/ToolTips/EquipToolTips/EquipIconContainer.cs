﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Structs;
using Common.Utils;
using Common.ServerConfig;
using UnityEngine.UI;
using GameMain.GlobalManager.SubSystem;
using Common.Global;
using Common.Data;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using UnityEngine.EventSystems;
using Common.ExtendComponent;

namespace ModuleCommonUI
{
    public class EquipIconContainer : KContainer
    {
        private ItemGrid _itemGrid;
        private GameObject _equipedMarkGo;

        private EquipToolTipsData _equipData;
        private PanelIdEnum _context;
        private EquipToolTipsViewPosition _position;

        protected override void Awake()
        {
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _equipedMarkGo = GetChild("Image_sharedyizhuangbei");
        }

        public void Refresh(EquipToolTipsData equipData, PanelIdEnum context, EquipToolTipsViewPosition position)
        {
            _equipData = equipData;
            _context = context;
            _position = position;

            RefreshIcon();
            RefreshEquipedMark();
        }

        private void RefreshIcon()
        {
            _itemGrid.SetItemData(_equipData);
        }

        private void RefreshEquipedMark()
        {
            if (_equipData.EquipState == EquipState.Wearing)
            {
                _equipedMarkGo.SetActive(true);
            }
            else
            {
                _equipedMarkGo.SetActive(false);
            }
        }
    }
}
