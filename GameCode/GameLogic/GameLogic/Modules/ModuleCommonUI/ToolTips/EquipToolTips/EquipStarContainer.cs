﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Structs;
using Common.Utils;
using Common.ServerConfig;
using UnityEngine.UI;
using GameMain.GlobalManager.SubSystem;
using Common.Global;
using Common.Data;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using UnityEngine.EventSystems;

namespace ModuleCommonUI
{
    public class EquipStarContainer : KContainer
    {
        private const int STAR_COUNT = 5;

        private KList _starList;
        //整个Container的宽度
        private float _width;

        protected override void Awake()
        {
            _starList = GetComponent<KList>();
            _starList.SetDirection(KList.KListDirection.LeftToRight, STAR_COUNT);
            _starList.SetGap(0, 0);
            _starList.SetPadding(0, 0, 0, 0);
        }

        public void Refresh(EquipToolTipsData equipData)
        {
            ShowStarCount(equipData.Star);
        }

        private void ShowStarCount(int count)
        {
            object[] dataList = new object[count];
            _starList.RemoveAll();
            _starList.SetDataList<StarItem>(dataList, 1);
            _width = CalculateWidth(count);
        }

        private float CalculateWidth(int count)
        {
            RectTransform rect = _starList.ItemList[0].GetComponent<RectTransform>();
            return rect.sizeDelta.x * count;
        }

        public float Width
        {
            get
            {
                return _width;
            }
        }
    }

    public class StarItem : KList.KListItemBase
    {

        protected override void Awake()
        {
            
        }

        public override void Dispose()
        {
            
        }
    }
}
