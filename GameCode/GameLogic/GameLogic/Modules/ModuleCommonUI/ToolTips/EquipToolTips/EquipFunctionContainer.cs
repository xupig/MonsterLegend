﻿using System.Collections.Generic;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Structs;
using Common.Structs.Enum;
using Common.ServerConfig;
using Common.Data;
using GameData;
using GameMain.GlobalManager;
using GameMain.Entities;
using Common.Chat;
using ModuleItemChannel;
using Common.Global;
using GameMain.GlobalManager.SubSystem;
using GameLoader.Utils.Timer;
using Common.Utils;
using System;

namespace ModuleCommonUI
{
    public class EquipFunctionContainer : KContainer
    {
        /// <summary>
        /// 根据玩家是否已经装备相同部位装备而指向装备和替换按钮
        /// </summary>
        private KButton _actionBtn;

        private KShrinkableButton _useBtn;
        private KShrinkableButton _exchangeBtn;
        private KShrinkableButton _strengthenBtn;
        private KShrinkableButton _attachMagicBtn;
        private KShrinkableButton _showBtn;
        private KShrinkableButton _decomposeBtn;
        private KShrinkableButton _sourceBtn;
        private KShrinkableButton _recastBtn;
        private KShrinkableButton _assignBtn;
        private StateText _assignTimeTxt;
        private uint _assignTimerId;

        private List<Vector2> _positionList = new List<Vector2>();
        private List<KButton> _positionBtnList = new List<KButton>();
        private List<bool> _visibilityList = new List<bool>();
        private List<KButton> _visibilityBtnList = new List<KButton>();

        private EquipToolTipsData _equipData;
        private PanelIdEnum _context;

        public KComponentEvent onWearEquip = new KComponentEvent();

        protected override void Awake()
        {
            _useBtn = GetChildComponent<KShrinkableButton>("Button_zhuangbei");
            _exchangeBtn = GetChildComponent<KShrinkableButton>("Button_tihuan");
            _strengthenBtn = GetChildComponent<KShrinkableButton>("Button_qianghua");
            _attachMagicBtn = GetChildComponent<KShrinkableButton>("Button_fumo");
            _showBtn = GetChildComponent<KShrinkableButton>("Button_zhanshi");
            _decomposeBtn = GetChildComponent<KShrinkableButton>("Button_fenjie");
            _sourceBtn = GetChildComponent<KShrinkableButton>("Button_laiyuan");
            _recastBtn = GetChildComponent<KShrinkableButton>("Button_chongzhu");
            _assignBtn = GetChildComponent<KShrinkableButton>("Button_zhuanjiao");
            _assignTimeTxt = GetChildComponent<StateText>("Label_txtShijian");

            AddBtnPosition(_showBtn);
            AddBtnPosition(_attachMagicBtn);
            AddBtnPosition(_strengthenBtn);
            AddBtnPosition(_exchangeBtn);
            AddBtnPosition(_useBtn);
            AddBtnPosition(_recastBtn);
            AddBtnPosition(_sourceBtn);

            AddBtnVisibility(_decomposeBtn);
            AddBtnVisibility(_showBtn);
            AddBtnVisibility(_attachMagicBtn);
            AddBtnVisibility(_strengthenBtn);
            AddBtnVisibility(_useBtn);
            AddBtnVisibility(_exchangeBtn);
            AddBtnVisibility(_recastBtn);
            AddBtnVisibility(_sourceBtn);
            AddBtnVisibility(_assignBtn);
            AddEventListener();
        }

        public void Refresh(EquipToolTipsData equipData, PanelIdEnum context)
        {
            _equipData = equipData;
            _context = context;
            RefreshActionBtn();
            RefreshBtnVisiblility();
            RefreshBtnLayout();
        }

        private void RefreshBtnVisiblility()
        {
            HideAllBtn();
            switch (_context)
            {
                case PanelIdEnum.Bag:
                    RefreshBagBtnVisibility();
                    break;
                case PanelIdEnum.Information:
                    RefreshRoleBtnVisibility();
                    break;
                case PanelIdEnum.EquipSmelter:
                    RefreshSmelterBtnVisibility();
                    break;
            }
            ApplyBtnListVisibility();
        }

        private void RefreshBagBtnVisibility()
        {
            SetBtnVisibility(_actionBtn, true);
            FunctionData functionData = PlayerDataManager.Instance.FunctionData;
            if (functionData.IsFunctionOpen(FunctionId.equip))
            {
                SetBtnVisibility(_strengthenBtn, true);
                SetBtnVisibility(_decomposeBtn, true);
            }
            if (functionData.IsFunctionOpen(FunctionId.enchant))
            {
                SetBtnVisibility(_attachMagicBtn, true);
            }
            if (functionData.IsFunctionOpen(FunctionId.EquipRecast)
                && equip_recast_helper.CanRecast(_equipData.EquipInfo))
            {
                SetBtnVisibility(_recastBtn, true);
            }
            SetBtnVisibility(_showBtn, true);
            SetBtnVisibility(_sourceBtn, true);
            RefreshAssignBtnVisibility();
        }

        private void RefreshAssignBtnVisibility()
        {
            uint canAssignTime = 60 * 60 * uint.Parse(global_params_helper.GetGlobalParam(GlobalParamId.transfer_item_to_player_time));
            uint spanTime = Global.serverTimeStampSecond > _equipData.GetTime ? Global.serverTimeStampSecond - _equipData.GetTime : 0;
            if (_equipData.UserList != null && _equipData.UserList.Count > 0 && spanTime < canAssignTime)
            {
                SetBtnVisibility(_assignBtn, true);
                _assignTimeTxt.Visible = true;
                _assignTimeTxt.CurrentText.text = MogoTimeUtil.ToUniversalTime(Convert.ToInt32(canAssignTime - spanTime));
                ResetAssignTimer();
                _assignTimerId = TimerHeap.AddTimer(0, 500, OnAssignTimeStep);
            }
        }

        private void ResetAssignTimer()
        {
            if (_assignTimerId != 0)
            {
                TimerHeap.DelTimer(_assignTimerId);
                _assignTimerId = 0;
            }
        }

        private void OnAssignTimeStep()
        {
            uint canAssignTime = 60 * 60 * uint.Parse(global_params_helper.GetGlobalParam(GlobalParamId.transfer_item_to_player_time));
            uint spanTime = Global.serverTimeStampSecond > _equipData.GetTime ? Global.serverTimeStampSecond - _equipData.GetTime : 0;
            if (_equipData.UserList != null && _equipData.UserList.Count > 0 && spanTime < canAssignTime)
            {
                _assignTimeTxt.CurrentText.text = MogoTimeUtil.ToUniversalTime(Convert.ToInt32(canAssignTime - spanTime));
            }
            else
            {
                SetBtnVisibility(_assignBtn, false);
                _assignTimeTxt.Visible = false;
                ResetAssignTimer();
            }
        }

        private void RefreshRoleBtnVisibility()
        {
            SetBtnVisibility(_showBtn, true);
            SetBtnVisibility(_sourceBtn, true);
            FunctionData functionData = PlayerDataManager.Instance.FunctionData;
            if (functionData.IsFunctionOpen(FunctionId.EquipRecast)
               && equip_recast_helper.CanRecast(_equipData.EquipInfo))
            {
                SetBtnVisibility(_recastBtn, true);
            }
        }

        private void RefreshSmelterBtnVisibility()
        {
            SetBtnVisibility(_actionBtn, true);
        }

        private void RefreshActionBtn()
        {
            SetBtnVisibility(_useBtn, false);
            SetBtnVisibility(_exchangeBtn, false);
            if (IsBodyEquipPositionOccupied == true)
            {
                SetBtnVisibility(_exchangeBtn, true);
                _actionBtn = _exchangeBtn;
            }
            else
            {
                SetBtnVisibility(_useBtn, true);
                _actionBtn = _useBtn;
            }
        }

        private void RefreshBtnLayout()
        {
            int index = 0;
            for (int i = 0; i < _positionBtnList.Count; i++)
            {
                if (_positionBtnList[i].gameObject.activeSelf == true)
                {
                    SetBtnPosition(_positionBtnList[i], index);
                    index++;
                }
            }
        }

        protected void AddEventListener()
        {
            _useBtn.onClick.AddListener(OnUseClick);
            _exchangeBtn.onClick.AddListener(OnUseClick);
            _strengthenBtn.onClick.AddListener(OnStrengthenClick);
            _attachMagicBtn.onClick.AddListener(OnAttachMagicClick);
            _showBtn.onClick.AddListener(OnShowClick);
            _decomposeBtn.onClick.AddListener(OnDecomposeClick);
            _recastBtn.onClick.AddListener(OnRecastClick);
            _sourceBtn.onClick.AddListener(OnSourceBtnClick);
            _assignBtn.onClick.AddListener(OnAssignClick);
        }

        private void OnRecastClick()
        {
            view_helper.OpenView(446);
            UIManager.Instance.ClosePanel(PanelIdEnum.EquipToolTips);
        }

        private void OnSourceBtnClick()
        {
            List<PanelIdEnum> previousPanelList = new List<PanelIdEnum>();
            previousPanelList.Add(_context);
            UIManager.Instance.ClosePanel(PanelIdEnum.EquipToolTips);
            UIManager.Instance.ShowPanel(PanelIdEnum.EquipChannel, previousPanelList);
        }

        private void OnAssignClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.EquipAssignTips, _equipData);
        }

        private void OnUseClick()
        {
            WearEquip();
            if (CanWearEquip == true)
            {
                CloseEquipToolTips();
            }
        }

        private void WearEquip()
        {
            onWearEquip.Invoke();
        }

        private bool IsBodyEquipPositionOccupied
        {
            get
            {
                BagData bodyEquipData = PlayerDataManager.Instance.BagData.GetBodyEquipBagData();
                if (_equipData.SubType == EquipType.ring)
                {
                    return bodyEquipData.GetItemInfo(public_config.EQUIP_POS_LEFTRING) != null && bodyEquipData.GetItemInfo(public_config.EQUIP_POS_RIGHTRING) != null;
                }
                return bodyEquipData.GetItemInfo((int)_equipData.SubType) != null;
            }
        }

        private bool CanWearEquip
        {
            get
            {
                return PlayerAvatar.Player.vocation == _equipData.UsageVocationRequired && PlayerAvatar.Player.level >= _equipData.UsageLevelRequired;
            }
        }

        private void OnStrengthenClick()
        {
            EquipPanelOption equipPanelData;
            BagType bagType = (BagType)GetBagType();
            if (bagType == BagType.BodyEquipBag)
            {
                equipPanelData = new EquipPanelOption(0, _equipData.GridPosition);
            }
            else
            {
                equipPanelData = new EquipPanelOption(0, (int)_equipData.SubType);
            }
            UIManager.Instance.ShowPanel(PanelIdEnum.Equip, equipPanelData);
            CloseEquipToolTips();
        }

        private void OnAttachMagicClick()
        {
            EquipPanelOption equipPanelData = new EquipPanelOption(1);
            UIManager.Instance.ShowPanel(PanelIdEnum.Equip, equipPanelData);
            CloseEquipToolTips();
        }

        private void OnShowClick()
        {
            CloseEquipToolTips();
            UIManager.Instance.ShowPanel(PanelIdEnum.Chat, new ChatItemLinkWrapper(_equipData.Name, GetBagType(), _equipData.GridPosition, _equipData.Quality));
        }

        private int GetBagType()
        {
            if (_context == PanelIdEnum.Information)
            {
                return (int)BagType.BodyEquipBag;
            }
            return (int)BagType.ItemBag;
        }

        private void OnDecomposeClick()
        {
            EquipManager.Instance.RequestDecompose(_equipData.GridPosition);
            CloseEquipToolTips();
        }

        private void AddBtnPosition(KButton btn)
        {
            _positionBtnList.Add(btn);
            _positionList.Add(btn.gameObject.GetComponent<RectTransform>().anchoredPosition);
        }

        private void SetBtnPosition(KButton btn, int index)
        {
            btn.gameObject.GetComponent<RectTransform>().anchoredPosition = _positionList[index];
            KShrinkableButton shrinkableBtn = btn as KShrinkableButton;
            shrinkableBtn.RefreshRectTransform();
        }

        private void AddBtnVisibility(KButton btn)
        {
            _visibilityBtnList.Add(btn);
            _visibilityList.Add(true);
        }

        private void SetBtnVisibility(KButton btn, bool visibility)
        {
            _visibilityList[_visibilityBtnList.IndexOf(btn)] = visibility;
        }

        protected void ShowAllBtn()
        {
            for (int i = 0; i < _visibilityList.Count; i++)
            {
                _visibilityList[i] = true;
            }
        }

        protected void HideAllBtn()
        {
            for (int i = 0; i < _visibilityList.Count; i++)
            {
                _visibilityList[i] = false;
            }
            _assignTimeTxt.Visible = false;
        }

        protected void ApplyBtnListVisibility()
        {
            for (int i = 0; i < _visibilityBtnList.Count; i++)
            {
                _visibilityBtnList[i].Visible = _visibilityList[i];
            }
        }

        private void CloseEquipToolTips()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.EquipToolTips);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            ResetAssignTimer();
        }
    }
}
