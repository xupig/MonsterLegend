﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Structs;
using Common.Utils;
using Common.ServerConfig;
using UnityEngine.UI;
using GameMain.GlobalManager.SubSystem;
using Common.Global;
using Common.Data;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using GameMain.Entities;
using UnityEngine.EventSystems;

namespace ModuleCommonUI
{
    public class EnchantScrollItemToolTips : ItemToolTips
    {
        protected EnchantScrollPropertyContainer _scrollContainer;

        protected override PanelIdEnum Id
        {
            get
            {
                return PanelIdEnum.EnchantScrollToolTips;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            gameObject.name = "Container_EnchantScrollItemToolTips";
        }

        protected override void InitChildren()
        {
            AddScrollContainer();
            AddEndContainer();
            AddScrollBtnContainer();
        }

        private void AddScrollContainer()
        {
            GameObject child = GetChild("Container_content/ScrollView_content/mask/content/Container_taozhuangjuanzhou");
            child.SetActive(true);
            _locaterList.Add(child.AddComponent<Locater>());
            _scrollContainer = child.AddComponent<EnchantScrollPropertyContainer>();
            _containerList.Add(_scrollContainer);
        }

        protected void AddScrollBtnContainer()
        {
            GameObject child = GetChild("Container_content/Container_Buttons/Container_Common");
            child.SetActive(true);
            _commonBtnContainer = child.AddComponent<EnchantScrollFunctionContainer>();
        }

        protected override void RefreshContent()
        {
            RefreshScrollPropertyContainer();
            RefreshEndContainer();
        }

        private void RefreshScrollPropertyContainer()
        {
            _scrollContainer.Refresh(_itemData, _context);
        }

        protected override void RefreshBtnContainer()
        {
            _commonBtnContainer.Refresh(_itemData, _context);
        }
        
    }
}
