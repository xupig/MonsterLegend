﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Structs;
using Common.Utils;
using Common.ServerConfig;
using UnityEngine.UI;
using GameMain.GlobalManager.SubSystem;
using Common.Global;
using Common.Data;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;
using GameMain.Entities;
using UnityEngine.EventSystems;

namespace ModuleCommonUI
{
    public class TokenItemToolTips : ItemToolTips
    {
        protected TokenFunctionContainer _tokenBtnContainer;

        protected override PanelIdEnum Id
        {
            get
            {
                return PanelIdEnum.TokenItemToolTips;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            gameObject.name = "Container_TokenItemToolTips";
        }

        protected override void InitChildren()
        {
            AddStartContainer();
            AddEndContainer();
            AddTokenBtnContainer();
        }

        private void AddTokenBtnContainer()
        {
            GameObject child = GetChild("Container_content/Container_Buttons/Container_currency");
            child.SetActive(true);
            _tokenBtnContainer = child.AddComponent<TokenFunctionContainer>();
        }

        protected override void RefreshContent()
        {
            RefreshStartContainer();
            RefreshEndContainer();
        }

        protected override void RefreshBtnContainer()
        {
            _tokenBtnContainer.Refresh(_itemData, _context);
        }
    }
}
