﻿#region 模块信息
/*==========================================
// 文件名：RuneToolTips
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.ToolTips
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/9 17:07:51
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleRune.ChildComponent;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleCommonUI
{
    public class RuneToolTips : BasePanel
    {

        private KButton _btnLock;
        private StateText _txtLock;
        private KButton _btnLevelUp;
        private KButton _btnEquip;
        private StateText _txtEquip;

        private KButton _btnClose;

        private RuneTipsData _tipsData;
        private RuneItemInfo _itemInfo;

        private RuneToolTipContainer _equiped;
        private RuneToolTipContainer _notEquiped;

        private RectTransform _equipedRect;
        private RectTransform _notEquipedRect;

        private Vector3 _center = Vector3.zero;
        private Vector3 _left = Vector3.zero;
        private Vector3 _right = Vector3.zero;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.RuneToolTips; }
        }

        protected override void Awake()
        {
            _equiped = AddChildComponent<RuneToolTipContainer>("Container_equiped");
            _notEquiped = AddChildComponent<RuneToolTipContainer>("Container_notEquiped");
            CloseBtn = _notEquiped.GetChildComponent<KButton>("Button_close");
            _equiped.GetChildComponent<KButton>("Button_close").Visible = false;
            _equipedRect = _equiped.GetComponent<RectTransform>();
            _equiped.ShowEquiped();
            _notEquipedRect = _notEquiped.GetComponent<RectTransform>();
            _center = _equipedRect.anchoredPosition;
            _left = _center - new Vector3(250, 0, 0);
            _right = _center + new Vector3(250, 0, 0);
            _equipedRect.anchoredPosition = _left;
            _btnLock = _notEquiped.GetChildComponent<KButton>("Button_suoding");
            _txtLock = _btnLock.GetChildComponent<StateText>("label");

            _btnEquip = _notEquiped.GetChildComponent<KButton>("Button_zhuangbei");
            _txtEquip = _btnEquip.GetChildComponent<StateText>("label");

            _btnLevelUp = _notEquiped.GetChildComponent<KButton>("Button_shengji");

            _btnClose = GetChildComponent<KButton>("Button_close");
            ModalMask = _btnClose.GetChildComponent<StateImage>("image");
        }

        public override void OnShow(object data)
        {
            SetData(data);
            AddEventListener();
        }

        public override void SetData(object data)
        {
            if (data is ToolTipsDataWrapper)
            {
                _tipsData = (data as ToolTipsDataWrapper).data as RuneTipsData;
                Refresh();
            }
            else if (data is int)
            {
                _equiped.Visible = false;
                _notEquiped.SetRuneData(rune_helper.GetRune((int)data));
                _btnEquip.Visible = false;
                _btnLevelUp.Visible = false;
                _btnLock.Visible = false;
            }
        }

        private void Refresh()
        {
            if (_tipsData == null) return;
            if (_tipsData.bagType == BagType.Undefined)
            {
                _btnLock.Visible = false;
                _btnLevelUp.Visible = false;
                _btnEquip.Visible = false;
                _notEquiped.SetRuneData(rune_helper.GetRune(_tipsData.pos));
                _equiped.Visible = false;
                _notEquipedRect.anchoredPosition = _center;
            }
            else
            {
                _btnLock.Visible = true;
                _btnLevelUp.Visible = true;
                _btnEquip.Visible = true;
                if (_tipsData.bagType == BagType.BodyRuneBag)
                {
                    _txtEquip.ChangeAllStateText(MogoLanguageUtil.GetContent(4320));
                }
                else
                {
                    _txtEquip.ChangeAllStateText(MogoLanguageUtil.GetContent(4319));
                }
               
                _itemInfo = PlayerDataManager.Instance.BagData.GetBagData(_tipsData.bagType).GetItemInfo(_tipsData.pos) as RuneItemInfo;
                UpdateLockState();
                _btnLevelUp.gameObject.SetActive(_itemInfo.Level < rune_helper.GetMaxLevel(_itemInfo.RuneConfig));
                _notEquiped.SetRuneItemInfo(_itemInfo);
                ShowEquipedRuneTips(_itemInfo);
            }
        }

        private void ShowEquipedRuneTips(RuneItemInfo _itemInfo)
        {
            if(_tipsData.bagType != BagType.BodyRuneBag)
            {
                Dictionary<int, BaseItemInfo> dic = PlayerDataManager.Instance.BagData.GetBagData(BagType.BodyRuneBag).GetAllItemInfo();
                foreach (KeyValuePair<int, BaseItemInfo> kvp in dic)
                {
                    RuneItemInfo itemInfo = kvp.Value as RuneItemInfo;
                    if (itemInfo.RuneConfig.__subtype == _itemInfo.RuneConfig.__subtype && itemInfo.RuneConfig.__page == _itemInfo.RuneConfig.__page)
                    {
                        _equiped.Visible = true;
                        _equiped.SetRuneItemInfo(itemInfo);
                        _notEquipedRect.anchoredPosition = _right;
                        _txtEquip.ChangeAllStateText(MogoLanguageUtil.GetContent(4335));
                        return;
                    }
                }
            }
            
            _equiped.Visible = false;
            _notEquipedRect.anchoredPosition = _center;
        }

        private void UpdateLockState()
        {
            _notEquiped.UpdateLockState(_itemInfo);
            if (_itemInfo.RuneLock == 1)
            {
                _txtLock.ChangeAllStateText(MogoLanguageUtil.GetContent(4322));
            }
            else
            {
                _txtLock.ChangeAllStateText(MogoLanguageUtil.GetContent(4321));
            }
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _btnLevelUp.onClick.AddListener(OnLevelUp);
            _btnEquip.onClick.AddListener(PutOn);
            _btnLock.onClick.AddListener(OnLock);
            _btnClose.onClick.AddListener(ClosePanel);
            EventDispatcher.AddEventListener<BagType, int, RuneItemInfo>(RuneEvent.RUNE_BAG_ITEM_CHANGE, OnRuneBagItemChange);
        }


        private void RemoveEventListener()
        {
            _btnLevelUp.onClick.RemoveListener(OnLevelUp);
            _btnEquip.onClick.RemoveListener(PutOn);
            _btnLock.onClick.RemoveListener(OnLock);
            _btnClose.onClick.RemoveListener(ClosePanel);
            EventDispatcher.RemoveEventListener<BagType, int, RuneItemInfo>(RuneEvent.RUNE_BAG_ITEM_CHANGE, OnRuneBagItemChange);
        }

        private void OnRuneBagItemChange(BagType bagType, int pos, RuneItemInfo info)
        {
            if (_tipsData == null || _tipsData.pos != pos) return;
            _itemInfo = info;
            UpdateLockState();
        }

        private void OnLevelUp()
        {
            if (_tipsData == null)
            {
                return;
            }
            RuneManager.levelUpData.bagType = _tipsData.bagType;
            RuneManager.levelUpData.pos = _tipsData.pos;
            UIManager.Instance.ShowPanel(PanelIdEnum.RuneLevelUp);
            ClosePanel();
        }

        private void OnLock()
        {
            if (_itemInfo == null)
            {
                return;
            }
            if (_itemInfo.RuneLock == 1)
            {
                RuneManager.Instance.OnRuneUnlock(_itemInfo.GridPosition, _tipsData.bagType);
            }
            else
            {
                RuneManager.Instance.OnRuneLock(_itemInfo.GridPosition, _tipsData.bagType);
            }
        }

        private void PutOn()
        {
            int toPos = _tipsData.toPos;
            if (_tipsData.bagType == BagType.RuneBag)
            {
                if (toPos == -1)
                {
                    toPos = GetPutOnPos();
                }
                if (toPos != -1)
                {
                    UIManager.Instance.PlayAudio("Sound/UI/rune_geton.mp3");
                    RuneManager.Instance.OnRunePutOn(_tipsData.pos, toPos);
                }
            }
            else
            {
                if (toPos == -1)
                {
                    toPos = GetPutOffPos();
                }
                if (toPos != -1)
                {
                    RuneManager.Instance.OnRunePutDown(_tipsData.pos, toPos);
                }
            }
            ClosePanel();
        }

        private int GetPutOnPos()
        {
            Dictionary<int, BaseItemInfo> equipDict = PlayerDataManager.Instance.BagData.GetBodyRuneBagData().GetAllItemInfo();
            Dictionary<int, int> holeDict = rune_helper.GetPageHoleIndex(_itemInfo.RuneConfig.__page);
            int levelLimit = int.MaxValue;
            int emptyCellCount = 0;
            foreach (KeyValuePair<int, int> kvp in holeDict)
            {
                rune_hole hole = rune_helper.GetRuneHole(kvp.Value);
                if (equipDict.ContainsKey(kvp.Value))
                {
                    RuneItemInfo itemInfo = equipDict[kvp.Value] as RuneItemInfo;
                    if (itemInfo.RuneConfig.__subtype == _itemInfo.RuneConfig.__subtype)
                    {
                        return hole.__id;
                    }
                }
            }

            bool isUnlock = false;
            foreach (KeyValuePair<int, int> kvp in holeDict)
            {
                rune_hole hole = rune_helper.GetRuneHole(kvp.Value);
                if (equipDict.ContainsKey(kvp.Value) == false)
                {
                    if (PlayerAvatar.Player.level >= hole.__unlock_level)//等级检测
                    {
                        if (rune_helper.CheckSubType(hole.__subtypes, _itemInfo.RuneConfig.__subtype))
                        {
                            return hole.__id;
                        }
                        emptyCellCount++;
                    }
                    else
                    {
                        levelLimit = Math.Min(levelLimit, hole.__unlock_level);
                        isUnlock = true;
                    }
                }
            }

            if (emptyCellCount == 0)
            {
                if(isUnlock)
                {
                    ShowTips(MogoLanguageUtil.GetContent(4351, rune_helper.GetRuneTypeName(_itemInfo.RuneConfig.__page)));//符文装备栏已满
                }
                else
                {
                    ShowTips(MogoLanguageUtil.GetContent(4329, rune_helper.GetRuneTypeName(_itemInfo.RuneConfig.__page)));//符文装备栏已满
                }
                return -1;
            }
            if (levelLimit > PlayerAvatar.Player.level)
            {
                ShowTips(string.Format(MogoLanguageUtil.GetContent(4327), levelLimit));//等级不足
            }
            return -1;
        }

        private int GetPutOffPos()
        {
            Dictionary<int, BaseItemInfo> bagDict = PlayerDataManager.Instance.BagData.GetRuneBagData().GetAllItemInfo();
            for (int i = 0; i < public_config.MAX_COUNT_PKG_RUNE; i++)
            {
                if (bagDict.ContainsKey(i + 1) == false)
                {
                    return i + 1;
                }
            }
            ShowTips(MogoLanguageUtil.GetContent(4325));//符文背包满，不能脱下
            return -1;
        }

        private void ShowTips(object data)
        {
            ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, data, PanelIdEnum.RuneToolTips);
        }
    }
}
