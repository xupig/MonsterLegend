﻿using Common.Base;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using UnityEngine;

namespace ModuleCommonUI
{
    public class GemItemToolTips : ItemToolTips
    {
        protected GemFunctionContainer _gemBtnContainer;
        protected SpecialPropertyContainer _specialContainer;
        protected RecommendContainer _recommendContainer;

        protected override PanelIdEnum Id
        {
            get
            {
                return PanelIdEnum.GemItemToolTips;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            gameObject.name = "Container_GemItemToolTips";
        }

        protected override void InitChildren()
        {
            AddStartContainer();
            AddSpecialContainer();
            AddEndContainer();
            AddRecommendContainer();
            AddGemBtnContainer();
        }

        private void AddRecommendContainer()
        {
            GameObject child = GetChild("Container_content/ScrollView_content/mask/content/Container_tuijian");
            child.SetActive(true);
            _locaterList.Add(child.AddComponent<Locater>());
            _recommendContainer = child.AddComponent<RecommendContainer>();
            _containerList.Add(_recommendContainer);
        }

        private void AddSpecialContainer()
        {
            GameObject child = GetChild("Container_content/ScrollView_content/mask/content/Container_teshu");
            child.SetActive(true);
            _locaterList.Add(child.AddComponent<Locater>());
            _specialContainer = child.AddComponent<SpecialPropertyContainer>();
            _containerList.Add(_startContainer);
        }

        private void AddGemBtnContainer()
        {
            GameObject child = GetChild("Container_content/Container_Buttons/Container_Gem");
            child.SetActive(true);
            _gemBtnContainer = child.AddComponent<GemFunctionContainer>();
        }

        protected override void RefreshContent()
        {
            RefreshStartContainer();
            RefreshSpecialContainer();
            RefreshEndContainer();
            RefreshRecommendContainer();
        }

        private void RefreshRecommendContainer()
        {
            int gemAttriEffectId = item_gem_helper.GetAttriEffectId(_itemData.Id);
            int attriId = attri_effect_helper.GetAttributeIdList(gemAttriEffectId)[0];
            int starNum = attri_config_helper.GetRecommendStar(attriId, PlayerAvatar.Player.vocation);
            string recommendTxt = attri_config_helper.GetRecommendTxt(attriId);
            _recommendContainer.SetStarNum(starNum);
            _recommendContainer.SetStarTxt(recommendTxt);
        }

        protected override void RefreshBtnContainer()
        {
            _gemBtnContainer.Refresh(_itemData, _context);
        }

        private void RefreshSpecialContainer()
        {
            _specialContainer.Refresh(_itemData, _context);
        }
    }
}
