﻿#region 模块信息
/*==========================================
// 模块名：EquipToolTips
// 命名空间: ModuleCommonUI.ToolTips
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/08
// 描述说明：装备tip 
// 其他：拼接信息未实现
//==========================================*/
#endregion

using System.Collections.Generic;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using Common.ServerConfig;
using GameMain.GlobalManager.SubSystem;
using GameMain.GlobalManager;
using Common.Data;
using Common.Structs.Enum;
using MogoEngine.Events;
using Common.Events;

namespace ModuleCommonUI
{
    public enum EquipToolTipsViewPosition
    {
        Right = 0,
        Left = 1,
    }

    public class EquipToolTips : BasePanel
    {
        private EquipToolTipsView _rightTipsView;
        private EquipToolTipsView _leftTipsView;

        private GameObject _rightTipsGo;
        private GameObject _leftTipsGo;

        private List<Locater> _locaterList = new List<Locater>();
        private KDummyButton _dummyBtn;
        
        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            ModalMask.Alpha = 195.0f / 255.0f;
            _dummyBtn = AddChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
            _rightTipsGo = GetChild("Container_equip");
            _leftTipsGo = GameObject.Instantiate(_rightTipsGo) as GameObject;
            _leftTipsGo.transform.SetParent(this.transform);
            _leftTipsGo.transform.localScale = Vector3.one;
            _leftTipsGo.transform.localPosition = _rightTipsGo.transform.localPosition;

            _locaterList.Add(_leftTipsGo.AddComponent<Locater>());
            _locaterList.Add(_rightTipsGo.AddComponent<Locater>());

            _rightTipsView = _rightTipsGo.AddComponent<EquipToolTipsView>();
            _leftTipsView = _leftTipsGo.AddComponent<EquipToolTipsView>();
           
            AddEventListener();
        }

        protected override void OnDestroy()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _dummyBtn.onClick.AddListener(OnDummyClick);
            _leftTipsView.onExchange.AddListener(OnExchangeHand);
            _rightTipsView.onWearEquip.AddListener(OnWearEquip);
        }

        private void RemoveEventListener()
        {
            _dummyBtn.onClick.RemoveListener(OnDummyClick);
            _leftTipsView.onExchange.RemoveListener(OnExchangeHand);
            _rightTipsView.onWearEquip.RemoveListener(OnWearEquip);
        }

        private void OnExchangeHand()
        {
            SetFightForceComparision();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.EquipToolTips; }
        }

        public override void OnShow(object data)
        {
            ToolTipsDataWrapper wrapper = data as ToolTipsDataWrapper;
            EquipToolTipsDataWrapper equipTipsDataWrapper = wrapper.data as EquipToolTipsDataWrapper;
            if(equipTipsDataWrapper.rightEquipData != null)
            {
                _rightTipsGo.SetActive(true);
                _rightTipsView.Refresh(equipTipsDataWrapper.rightEquipData, wrapper.context, EquipToolTipsViewPosition.Right);
            }
            else
            {
                _rightTipsGo.SetActive(false);
            }

            if(equipTipsDataWrapper.leftEquipData != null)
            {
                _leftTipsGo.SetActive(true);
                _leftTipsView.HideCloseBtn();
                _leftTipsView.Refresh(equipTipsDataWrapper.leftEquipData, wrapper.context, EquipToolTipsViewPosition.Left);
                SetFightForceComparision();
            }
            else
            {
                _leftTipsGo.SetActive(false);
            }
            RefreshLayout();
            EventDispatcher.TriggerEvent<PanelIdEnum>(EquipEvents.EQUIP_TIPS_SHOW_FINISH, wrapper.context);
        }

        private void SetFightForceComparision()
        {
            if (_leftTipsView != null)
            {
                _rightTipsView.SetFightForceComparision(_leftTipsView.GetEquipFightForce());
            }
        }

        private void OnDummyClick()
        {
            ClosePanel();
        }

        public override void OnClose()
        {
            
        }

        private void OnWearEquip()
        {
            BagManager.Instance.RequestWearEquip(_rightTipsView.EquipData.GridPosition, GetBodyEquipPosition(), IsBodyEquipPositionOccupied);
        }

        private int GetBodyEquipPosition()
        {
            BagData bodyEquipData = PlayerDataManager.Instance.BagData.GetBodyEquipBagData();
            if(_rightTipsView.EquipData.SubType == EquipType.ring)
            {
                if(bodyEquipData.GetItemInfo(public_config.EQUIP_POS_LEFTRING) == null)
                {
                    return public_config.EQUIP_POS_LEFTRING;
                }
                if(bodyEquipData.GetItemInfo(public_config.EQUIP_POS_RIGHTRING) == null)
                {
                    return public_config.EQUIP_POS_RIGHTRING;
                }
                if(_leftTipsView.EquipData != null)
                {
                    return _leftTipsView.EquipData.GridPosition;
                }
                return public_config.EQUIP_POS_LEFTRING;
            }
            return (int)_rightTipsView.EquipData.SubType;
        }

        private bool IsBodyEquipPositionOccupied
        {
            get
            {
                BagData bodyEquipData = PlayerDataManager.Instance.BagData.GetBodyEquipBagData();
                if(_rightTipsView.EquipData.SubType == EquipType.ring)
                {
                    return bodyEquipData.GetItemInfo(public_config.EQUIP_POS_LEFTRING) != null && bodyEquipData.GetItemInfo(public_config.EQUIP_POS_RIGHTRING) != null;
                }
                return bodyEquipData.GetItemInfo((int)_rightTipsView.EquipData.SubType) != null;
            }
        }

        private void RefreshLayout()
        {
            float startX = UIManager.PANEL_WIDTH;
            for(int i = 0; i < _locaterList.Count; i++)
            {
                if(_locaterList[i].gameObject.activeSelf == true)
                {
                    startX -= _locaterList[i].Width;
                }
            }
            startX *= 0.5f;
            for(int i = 0; i < _locaterList.Count; i++)
            {
                if(_locaterList[i].gameObject.activeSelf == true)
                {
                    _locaterList[i].X = startX;
                    startX += _locaterList[i].Width;
                }
            }
        }
    }

  

}
