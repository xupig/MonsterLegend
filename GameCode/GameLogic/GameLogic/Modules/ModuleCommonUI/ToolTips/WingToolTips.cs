﻿using System.Collections.Generic;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using Common.Data;
using GameMain.GlobalManager;
using Common.ExtendComponent;
using Game.UI;
using GameData;
using System;
using Common.Utils;

namespace ModuleCommonUI
{
    public class WingToolTips : BasePanel
    {
        protected KContainer _contentContainer;
        protected Locater _contentLocater;
        protected StateText _nameTxt;
        protected StateText _typeText;
        protected StateText _description;
        protected ItemGrid _itemGrid;
        protected StateImage _imagezheZhao;

        protected List<Locater> _locaterList = new List<Locater>();
        protected List<KContainer> _containerList = new List<KContainer>();
        protected WingItemData _itemData;

        private KDummyButton _dummyBtn;
        private Vector3 _position;
        protected bool showBtn;
        private string _typeContent;
        private ArtNumberList _fightForceContainer;
        protected KPageableList _propertyList;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.WingToolTips; }
        }

        protected override void Awake()
        {
            _contentContainer = GetChildComponent<KContainer>("ScrollView_content/mask/content");
            _contentLocater = AddChildComponent<Locater>("ScrollView_content/mask/content");
            _dummyBtn = AddChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
            CloseBtn = GetChildComponent<KButton>("Container_Bg/Button_close");
            _nameTxt = GetChildComponent<StateText>("Container_mingzi/Label_txtName");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _position = gameObject.transform.localPosition;
            _imagezheZhao = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _imagezheZhao.Alpha = 195.0f / 255.0f;
            _fightForceContainer = _contentContainer.AddChildComponent<ArtNumberList>("Container_zhandouli/List_zhandouli");
            _description = _contentContainer.GetChildComponent<StateText>("Container_miaoshu/Label_text");
            _typeText = _contentContainer.GetChildComponent<StateText>("Container_leixing/Label_text");
            _typeContent = _typeText.CurrentText.text;
            _locaterList.Add(_contentContainer.AddChildComponent<Locater>("Container_jichushuxing"));
            _locaterList.Add(_contentContainer.AddChildComponent<Locater>("Container_miaoshu"));
            _containerList.Add(_contentContainer.GetChildComponent<KContainer>("Container_jichushuxing"));
            _containerList.Add(_contentContainer.GetChildComponent<KContainer>("Container_miaoshu"));
            InitPropertyList();
        }

        private void InitPropertyList()
        {
            _propertyList = _contentContainer.GetChildComponent<KPageableList>("Container_jichushuxing/List_shuxing");
            _propertyList.SetPadding(0, 0, 5, 0);
            _propertyList.SetGap(5, 0);
            _propertyList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        public override void OnShow(object data)
        {
            int wingId = 0;
            ToolTipsDataWrapper wrapper = data as ToolTipsDataWrapper;
            if (wrapper.data is int)
            {
                wingId = (int)wrapper.data;
                _itemData = PlayerDataManager.Instance.WingData.GetItemDataById(wingId);
                Refresh();
            }
            AddEventListener();
        }

        public override void OnClose()
        {
            gameObject.transform.localPosition = _position;
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _dummyBtn.onClick.AddListener(OnDummyClick);
        }

        private void RemoveEventListener()
        {
            _dummyBtn.onClick.RemoveListener(OnDummyClick);
        }

        private void OnDummyClick()
        {
            ClosePanel();
        }

        protected virtual void Refresh()
        {
            ResetContentPosition();
            RefreshName();
            RefreshIcon();
            RefreshFightForce();
            RefreshDescription();
            RefreshType();
            RefreshProperty();
            RefreshLayout();
        }

        private void RefreshType()
        {
            _typeText.CurrentText.text = string.Format(_typeContent, MogoLanguageUtil.GetContent(35764 + (int)_itemData.type));
        }

        private void RefreshProperty()
        {
            List<string> _contentList = attri_effect_helper.GetAttributeDescList(_itemData.effectId);
            _propertyList.SetDataList<PropertyItem>(_contentList);
            _propertyList.RecalculateSize();
        }

        private void RefreshDescription()
        {
            _description.CurrentText.text = _itemData.description;
            _description.RecalculateCurrentStateHeight();
        }

        private void RefreshFightForce()
        {
            _fightForceContainer.SetNumber(_itemData.fightForce);
        }

        private void ResetContentPosition()
        {
            _contentLocater.Position = Vector2.zero;
        }

        private void RefreshName()
        {
            _nameTxt.CurrentText.text = _itemData.name;
        }

        private void RefreshIcon()
        {
            _itemGrid.SetItemData(_itemData.icon, 0);
        }

        private void RefreshLayout()
        {
            for (int i = 0; i < _containerList.Count; i++)
            {
                _containerList[i].RecalculateSize();
            }
            float startY = _locaterList[0].Y;
            for (int i = 0; i < _locaterList.Count; i++)
            {
                Locater locater = _locaterList[i];
                if (locater.gameObject.activeSelf == true)
                {
                    locater.Y = startY;
                    startY -= locater.Height + PropertyContainer.LAYOUT_GAP;
                }
            }
            _contentContainer.RecalculateSize();
        }

    }
}
