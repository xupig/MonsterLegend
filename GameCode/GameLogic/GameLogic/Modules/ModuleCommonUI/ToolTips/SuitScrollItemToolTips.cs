﻿
using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using Common.Events;

namespace ModuleCommonUI
{
    public class SuitScrollItemToolTips : ItemToolTips
    {
        protected SuiteScrollProperyContainer _scrollContainer;
        protected SuitPropertyContainer _suitContainer;
        private Vector2 _beginPosition;
        protected override PanelIdEnum Id
        {
            get
            {
                return PanelIdEnum.SuitScrollToolTips;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            _beginPosition = GetChildComponent<RectTransform>("Container_content").anchoredPosition;
            gameObject.name = "Container_SuitScrollItemToolTips";
        }

        public override void OnShow(object data)
        {
            base.OnShow(data);
            AddEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener(EquipSuitScrollEvents.ShowEquipSuitScrollPanel, OnEquipSuitScrollPanelShow);
            EventDispatcher.AddEventListener(EquipSuitScrollEvents.HideEquipSuitScrollPanel, OnEquipSuitScrollPanelHide);
        }

        private void OnEquipSuitScrollPanelShow()
        {
            GetChildComponent<RectTransform>("Container_content").anchoredPosition = new Vector3(162f, -62f, 0);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener(EquipSuitScrollEvents.ShowEquipSuitScrollPanel, OnEquipSuitScrollPanelShow);
            EventDispatcher.RemoveEventListener(EquipSuitScrollEvents.HideEquipSuitScrollPanel, OnEquipSuitScrollPanelHide);
        }

        private void OnEquipSuitScrollPanelHide()
        {
            GetChildComponent<RectTransform>("Container_content").anchoredPosition = _beginPosition;
        }

        protected override void InitChildren()
        {
            AddScrollContainer();
            AddSuitContainer();
            AddEndContainer();
            AddSuitScrollBtnContainer();
        }

        private void AddScrollContainer()
        {
            GameObject child = GetChild("Container_content/ScrollView_content/mask/content/Container_taozhuangjuanzhou");
            child.SetActive(true);
            _locaterList.Add(child.AddComponent<Locater>());
            _scrollContainer = child.AddComponent<SuiteScrollProperyContainer>();
            _containerList.Add(_scrollContainer);
        }

        private void AddSuitContainer()
        {
            GameObject child = GetChild("Container_content/ScrollView_content/mask/content/Container_taozhuangshuxing");
            child.SetActive(true);
            _locaterList.Add(child.AddComponent<Locater>());
            _suitContainer = child.AddComponent<SuitPropertyContainer>();
            _containerList.Add(_suitContainer);
        }

        protected void AddSuitScrollBtnContainer()
        {
            GameObject child = GetChild("Container_content/Container_Buttons/Container_Common");
            child.SetActive(true);
            _commonBtnContainer = child.AddComponent<SuitScrollFunctionContainer>();
        }

        protected override void RefreshContent()
        {
            RefreshScrollPropertyContainer();
            RefreshSuitPropertyContainer();
            RefreshEndContainer();
        }

        protected override void RefreshBtnContainer()
        {
            _commonBtnContainer.Refresh(_itemData, _context);
        }

        private void RefreshScrollPropertyContainer()
        {
            _scrollContainer.Refresh(_itemData, _context);
        }

        private void RefreshSuitPropertyContainer()
        {
            _suitContainer.Refresh(equip_suit_scroll_helper.GetSuitId(_itemData.Id));
        }

        public override void OnClose()
        {
            GetChildComponent<RectTransform>("Container_content").anchoredPosition = _beginPosition;
            UIManager.Instance.ClosePanel(PanelIdEnum.SuitEquip);
            RemoveEventListener();
            base.OnClose();
        }
    }
}
