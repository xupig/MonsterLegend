﻿#region 模块信息
/*==========================================
// 文件名：ExpRuneToolTips
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.ToolTips
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/10 11:59:02
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleRune.ChildComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Data;
using GameMain.Entities;
using Common.ExtendComponent;
using Mogo.Util;

namespace ModuleCommonUI
{
    public class ExpRuneToolTips : BasePanel
    {
        private KContainer _levelContainer;
        private KContainer _getContainer;
        private KContainer _descriptionContainer;

        private KButton _btnUse;

        private StateText _txtLevel;
        private StateText _txtGet;
        private StateText _txtDescription;

        private StateText _txtName;
        private ItemGrid _itemGrid;
       // private KContainer _iconContainer;

        private string levelTemplate;
        private string getTemplate;
        private string descriptionTemplate;

        private List<Locater> _locaterList;

        private RuneTipsData _tipsData;

        private KButton _btnClose;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.ExpRuneToolTips; }
        }

        protected override void Awake()
        {

            _txtName = GetChildComponent<StateText>("Label_txtName");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _itemGrid.IsClickable = false;
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _locaterList = new List<Locater>();
            _levelContainer = GetChildComponent<KContainer>("Container_dengji");
            
            _locaterList.Add(_levelContainer.gameObject.AddComponent<Locater>());
            _txtLevel = _levelContainer.GetChildComponent<StateText>("Label_text");
            levelTemplate = _txtLevel.CurrentText.text;

            _getContainer = GetChildComponent<KContainer>("Container_huode");
            _locaterList.Add(_getContainer.gameObject.AddComponent<Locater>());
            _txtGet = _getContainer.GetChildComponent<StateText>("Label_text");
            getTemplate = _txtGet.CurrentText.text;

            _descriptionContainer = GetChildComponent<KContainer>("Container_miaoshu");
            _locaterList.Add(_descriptionContainer.gameObject.AddComponent<Locater>());
            _txtDescription = _descriptionContainer.GetChildComponent<StateText>("Label_text");
            descriptionTemplate = _txtDescription.CurrentText.text;

            _btnUse = GetChildComponent<KButton>("Button_shiyong");

            _btnClose = GetChildComponent<KButton>("Button_bigClose");
            ModalMask = _btnClose.GetChildComponent<StateImage>("image");
        }

        public override void OnShow(object data)
        {
            ToolTipsDataWrapper wrapper = data as ToolTipsDataWrapper;
            _tipsData = wrapper.data as RuneTipsData;
            AddEventListener();
            Refresh();
        }


        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _btnUse.onClick.AddListener(OnUse);
            _btnClose.onClick.AddListener(ClosePanel);
        }

        private void RemoveEventListener()
        {
            _btnUse.onClick.RemoveListener(OnUse);
            _btnClose.onClick.RemoveListener(ClosePanel);
        }

        private void Refresh()
        {
            if(_tipsData == null) return;
             if (_tipsData.bagType == BagType.Undefined)
             {
                 rune rune = rune_helper.GetRune(_tipsData.pos);
                 SetContent(rune);
                 _btnUse.Visible = false;
             }
             else
             {
                 RuneItemInfo itemInfo = PlayerDataManager.Instance.BagData.GetBagData(_tipsData.bagType).GetItemInfo(_tipsData.pos) as RuneItemInfo;
                 SetContent(itemInfo);
                 _btnUse.Visible = itemInfo.RuneConfig.__subtype != public_config.RUNE_EXP;
             }
        }

        private void OnUse()
        {
            RuneManager.Instance.OnRuneUse(_tipsData.pos);
            ClosePanel();
        }

        private string GetDescription(rune rune)
        {
            item_commom itemCommon = item_helper.GetItemConfig(rune.__id);
            if (itemCommon.__desc != null && itemCommon.__desc.ContainsKey("1") == true)
            {
                return MogoLanguageUtil.GetContent(itemCommon.__desc["1"]);
            }
            return string.Empty;
        }

        public void SetContent(rune rune)
        {
            _btnUse.Visible = rune.__subtype == public_config.RUNE_MONEY;
            _txtName.CurrentText.text = item_helper.GetName(rune.__id);
            _txtName.CurrentText.color = item_helper.GetColor(rune.__id);
            _itemGrid.SetItemData(rune.__id);

            _txtLevel.CurrentText.text = string.Format(levelTemplate, 1);



            _txtDescription.CurrentText.text = string.Format(descriptionTemplate, GetDescription(rune));
            _txtDescription.RecalculateAllStateHeight();
            string rewardContent = GetRewardContent(rune);
            _txtGet.CurrentText.text = string.Format(getTemplate, rune.__subtype == public_config.RUNE_MONEY ? (4333).ToLanguage() : (4332).ToLanguage(), rewardContent);
            _txtGet.RecalculateAllStateHeight();
            RefreshLayout();
        }

        public void SetContent(RuneItemInfo itemInfo)
        {
            _btnUse.Visible = itemInfo.RuneConfig.__subtype == public_config.RUNE_MONEY;
            _txtName.CurrentText.text = itemInfo.Name;
            _txtName.CurrentText.color = item_helper.GetColor(itemInfo.Id);
            _itemGrid.SetItemData(itemInfo);

            _txtLevel.CurrentText.text = string.Format(levelTemplate, itemInfo.Level);
            _txtDescription.CurrentText.text = string.Format(descriptionTemplate,  itemInfo.Description);
            _txtDescription.RecalculateAllStateHeight();
            string rewardContent = GetRewardContent(itemInfo.RuneConfig);
            _txtGet.CurrentText.text = string.Format(getTemplate, itemInfo.RuneConfig.__subtype == public_config.RUNE_MONEY ? (4333).ToLanguage() : (4332).ToLanguage(), rewardContent);
            _txtGet.RecalculateAllStateHeight();
            RefreshLayout();
        }

        private string GetRewardContent(rune rune)
        {
            if (rune.__subtype == public_config.RUNE_MONEY)
            {
                Dictionary<string, string> useEffect = rune.__item_value;
                string rewardContent = string.Empty;
                int i = 0;
                foreach (KeyValuePair<string, string> kvp in useEffect)
                {
                    if (i != 0)
                    {
                        rewardContent = string.Concat(rewardContent, "\n");
                    }
                    rewardContent = string.Concat(rewardContent, item_helper.GetName(int.Parse(kvp.Key)), "x", kvp.Value);
                }
                return rewardContent;
            }
            else
            {
                return rune.__exp_value + item_helper.GetName(public_config.ITEM_SPECIAL_TYPE_EXP);
            }
        }

        private void RefreshLayout()
        {
            _getContainer.RecalculateSize();
            _descriptionContainer.RecalculateSize();
            float startY = _locaterList[0].Y;
            for (int i = 0; i < _locaterList.Count; i++)
            {
                Locater locater = _locaterList[i];
                if (locater.gameObject.activeSelf == true)
                {
                    locater.Y = startY;
                    startY -= locater.Height;
                    startY -= 10;
                }
            }
        }

    }
}
