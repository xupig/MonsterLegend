﻿#region 模块信息
/*==========================================
// 模块名：ToolTipsManager
// 命名空间: ModuleCommonUI
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/08
// 描述说明：ToolTips控制都用该管理类ShowTip
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MogoEngine.Events;
using UnityEngine;
using Common.Base;
using GameMain.GlobalManager;
using GameData;
using Common.Data;
using Common.Structs;
using Common.Structs.Enum;
using Common.ServerConfig;
using MogoEngine.Mgrs;
using GameMain.Entities;

namespace ModuleCommonUI
{
    public class ToolTipsManager
    {
        private static ToolTipsManager _instance;
        private Dictionary<PanelIdEnum, PanelIdEnum> _tipsContextDict = new Dictionary<PanelIdEnum, PanelIdEnum>();

        static ToolTipsManager()
        {
            _instance = new ToolTipsManager();
        }

        public static ToolTipsManager Instance
        {
            get
            {
                return _instance;
            }
        }

        private void AddTipsContext(PanelIdEnum toolTipsId, PanelIdEnum context)
        {
            if (!_tipsContextDict.ContainsKey(toolTipsId))
            {
                _tipsContextDict.Add(toolTipsId, context);
            }
            else
            {
                _tipsContextDict[toolTipsId] = context;
            }
        }

        /// <summary>
        /// panelId:显示Tips面板Id
        /// data:Tips面板需要展示的数据
        /// context:开启Tips面板的所处的面板
        /// </summary>
        /// <param name="panelId"></param>
        /// <param name="data"></param>
        /// <param name="context"></param>
        public void ShowTip(PanelIdEnum panelId, System.Object data, PanelIdEnum context = PanelIdEnum.Empty)
        {
            AddTipsContext(panelId, context);
            UIManager.Instance.ShowPanel(panelId, new ToolTipsDataWrapper(data, context));
        }

        /// <summary>
        /// 根据道具的ID显示信息，数据来自客户端计算
        /// </summary>
        /// <param name="id"></param>
        /// <param name="context"></param>
        /// <param name="isShowLeftEquip"></param>
        public void ShowItemTip(int id, PanelIdEnum context, bool isShowLeftEquip = true)
        {
            if (item_helper.IsEquip(id) == true)
            {
                EquipToolTipsDataWrapper equipDataWrapper = new EquipToolTipsDataWrapper();
                EquipToolTipsData rightEquipData = new EquipToolTipsData(id);
                equipDataWrapper.rightEquipData = rightEquipData;
                if (isShowLeftEquip == true)
                {
                    equipDataWrapper.leftEquipData = GetLeftEquipData(rightEquipData, context);
                }
                ShowTip(PanelIdEnum.EquipToolTips, equipDataWrapper, context);
                return;
            }
            id = item_reward_helper.ProcessLvAdaptItemId(id);
            PanelIdEnum panel = GetItemPanelId(id, context);
            System.Object itemData = GetItemPanelData(id);
            ShowTip(panel, itemData, context);
        }

        /// <summary>
        /// 用于背包，人物装备等面板，数据来自服务器
        /// </summary>
        /// <param name="data"></param>
        /// <param name="context"></param>
        public void ShowItemTip(BaseItemInfo itemInfo, PanelIdEnum context,EquipState state = EquipState.InBag, bool isShowLeftEquip = true)
        {
            if(itemInfo.Type == BagItemType.Equip)
            {
                EquipToolTipsDataWrapper equipDataWrapper = new EquipToolTipsDataWrapper();
                EquipToolTipsData rightEquipData = EquipToolTipsData.CopyFromEquipInfo(itemInfo as EquipItemInfo);
                rightEquipData.EquipState = state;
                equipDataWrapper.rightEquipData = rightEquipData;
                if (isShowLeftEquip == true)
                {
                    equipDataWrapper.leftEquipData = GetLeftEquipData(rightEquipData, context);
                }
                ShowTip(PanelIdEnum.EquipToolTips, equipDataWrapper, context);
                return;
            }
            PanelIdEnum panel = GetItemPanelId(itemInfo.Id, context);
            ItemToolTipsData itemData = ItemToolTipsData.CopyFromItemInfo(itemInfo as CommonItemInfo);
            ShowTip(panel, itemData, context);
        }

        //Todo:看情况对Tips展示的数据做池化处理
        private EquipToolTipsData GetLeftEquipData(EquipToolTipsData rightEquipData, PanelIdEnum context)
        {
            if (context == PanelIdEnum.Information)
            {
                return null;
            }
            BagData bodyData = PlayerDataManager.Instance.BagData.GetBodyEquipBagData();
            EquipItemInfo equipInfo = bodyData.GetItemInfo((int)rightEquipData.SubType) as EquipItemInfo;
            if(equipInfo == null && rightEquipData.SubType == EquipType.ring)
            {
                equipInfo = bodyData.GetItemInfo(public_config.EQUIP_POS_RIGHTRING) as EquipItemInfo;
            }
            if(equipInfo == null)
            {
                return null;
            }
            EquipToolTipsData equipData = EquipToolTipsData.CopyFromEquipInfo(equipInfo);
            equipData.EquipState = EquipState.Wearing;
            return equipData;
        }

        private PanelIdEnum GetItemPanelId(int itemId, PanelIdEnum context)
        {
            PanelIdEnum panel = PanelIdEnum.ItemToolTips;
            BagItemType type = item_helper.GetItemType(itemId);
            if (type == BagItemType.Equip)
            {
                panel = PanelIdEnum.EquipToolTips;
            }
            if(type == BagItemType.Gem)
            {
                panel = PanelIdEnum.GemItemToolTips;
            }
            else if(type == BagItemType.Token)
            {
                panel = PanelIdEnum.TokenItemToolTips;
            }
            else if(type == BagItemType.SuitScroll)
            {
                panel = PanelIdEnum.SuitScrollToolTips;
            }
            else if(type == BagItemType.EnchantScroll)
            {
                panel = PanelIdEnum.EnchantScrollToolTips;
            }
            else if (type == BagItemType.Wing)
            {
                if (context != PanelIdEnum.Bag && context != PanelIdEnum.EquipUpgradeTips)
                {
                    panel = PanelIdEnum.WingToolTips;
                }
            }
            return panel;
        }

        private System.Object GetItemPanelData(int itemId)
        {
            System.Object itemData;
            BagItemType type = item_helper.GetItemType(itemId);
            if (type == BagItemType.Wing)
            {
                itemData = item_helper.GetWingId(itemId);
            }
            else
            {
                itemData = new ItemToolTipsData(itemId);
            }
            return itemData;
        }

        public void CloseTip(PanelIdEnum panelId, bool isCloseContext = false)
        {
            UIManager.Instance.ClosePanel(panelId);
            if (isCloseContext)
            {
                if (_tipsContextDict.ContainsKey(panelId))
                {
                    UIManager.Instance.ClosePanel(_tipsContextDict[panelId]);
                }
            }
        }
    }

}