﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using Mogo.Util;
using MogoEngine.Events;
using System.Collections.Generic;

namespace ModuleCommonUI
{
    public class PortalPanel : BasePanel
    {
        private KScrollView _scrollView;
        private KList _list;

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_fubenchuangsong");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _list.SetPadding(0, 20, 0, 20);
            CloseBtn = GetChildComponent<KButton>("Container_Bg/Button_close02");
            this.Visible = false;
        }

        private void SetPortalViewList(List<PortalItemData> dataList)
        {
            this.Visible = true;
            _list.RemoveAll();
            _list.SetDataList<PortalItem>(dataList);
        }


        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.PortalPanel; }
        }

        public override void OnShow(object data)
        {

            EventDispatcher.TriggerEvent(MainUIEvents.RESET_CONTROL_STICK);
            List<PortalItemData> list = data as List<PortalItemData>;
            SetPortalViewList(list);
        }

        public override void OnClose()
        {

        }
    }

    public class PortalItem : KList.KListItemBase
    {
        private PortalItemData _data;
        private StateText _titleText;
        private KButton _button;

        protected override void Awake()
        {
            _button = GetChildComponent<KButton>("Button_btn");
            _titleText = _button.GetChildComponent<StateText>("label");
            _titleText.CurrentText.alignment = UnityEngine.TextAnchor.UpperCenter;
            _button.onClick.AddListener(OnItemClick);
        }

        private void OnItemClick()
        {
            if (PlayerDataManager.Instance.TaskData.isTeammateInTeamTask())
            {
                MogoUtils.FloatTips(115024);
                return;
            }
            if (_data.choiceType == InstanceDefine.CHOICE_CLIENT_ACTION)
            {
                GameMain.GlobalManager.GameSceneManager.GetInstance().CreateActions(_data.choiceActionId, PlayerAvatar.Player.position);
            }
            else
            {
                PlayerAvatar.Player.RpcCall("choice_action_req", _data.choiceId, _data.choiceActionId);
            }

            UIManager.Instance.ClosePanel(PanelIdEnum.PortalPanel);
        }

        private void RefreshContent()
        {
            _titleText.CurrentText.text = MogoLanguageUtil.GetContent(_data.contentId);
        }

        public override void Dispose()
        {
            _button.onClick.RemoveListener(OnItemClick);
            base.Data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null && value != base.Data)
                {
                    _data = value as PortalItemData;
                    RefreshContent();
                }
            }
        }

    }
}
