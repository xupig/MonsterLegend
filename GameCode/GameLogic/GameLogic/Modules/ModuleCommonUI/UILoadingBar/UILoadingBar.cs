﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Game.UI.UIComponent;
using GameLoader;
using GameLoader.Utils;
using GameMain.GlobalManager;
using UnityEngine;

namespace ModuleCommonUI
{
    /// <summary>
    /// UI加载进度条
    /// </summary>
    public class UILoadingBar : BasePanel//, IProgressBar
    {
        private GameObject goGengXin;
        private GameObject goZaiRuZhong;
        private GameObject goNuLiJiaZai;
        private GameObject goJiaZaiZhong;
        private GameObject goImageJiaZai;
        private StateText labLoading;

        private bool isRotate = false;
        private const string txt1 = "加载中...";
        //public static UILoadingBar Instance;

        public static string GetPrefabPath()
        {
            return string.Format("{0}/{1}{2}", BaseModule.UI_FOLDER, "Container_UILoadingPanel", ".prefab");
        }

        void Update()
        {
            if (isRotate)
            {//旋转图片
                if (goImageJiaZai != null) goImageJiaZai.transform.Rotate(0, 0, -Time.deltaTime * 360, Space.Self);
            }
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.UILoadingBar; }
        }

        protected override void Awake()
        {
            //Instance = this;
            Visible = false;
            GameObject.DontDestroyOnLoad(gameObject);
            goGengXin = GetChild("Container_gengxin");
            goZaiRuZhong = GetChild("Container_zairuzhong");
            goNuLiJiaZai = GetChild("Container_nglijiazai");
            goJiaZaiZhong = GetChild("Container_jiazaizhong");
            goImageJiaZai = GetChild("Container_jiazaizhong/Image_jiazai");   //normal
            labLoading = GetChildComponent<StateText>("Container_jiazaizhong/Label_txtjiazaizhong");

            RectTransform rect = goImageJiaZai.GetComponent<RectTransform>();
            rect.pivot = new Vector2(0.5f, 1 / 3.0f);                         //为了围绕自身中心点旋转，必需把图片描点调整在图片中心
            rect.localPosition = new Vector3(110, -28, 0);

            goGengXin.SetActive(false);
            goZaiRuZhong.SetActive(false);
            goNuLiJiaZai.SetActive(false);
            labLoading.CurrentText.text = txt1;
            ModalMask = GetChildComponent<StateImage>("Container_Bg/ScaleImage_sharedZhezhao");
        }

        public void Close()
        {
            OnClose();
        }

        public void Dispose()
        { 
        }

        public void Show()
        {
            isRotate = true;
        }

        public void UpdateFloatTip(IList<string> floatTipList) { }

        public void UpdateFloatTipInterval(int interval) { }

        public void OnUpdate() { }

        public void UpdateProgress(float progress)
        {
            UpdateProgress((int)(progress * 100));
        }

        public void UpdateProgress(int value)
        {
            //labLoading.CurrentText.text = string.Format("{0}%", value);
        }

        public void UpdateProgress(int cur, int total)
        {
            UpdateProgress(((float)cur) / total);
        }

        public void UpdateSkin(GameObject skin)
        { 
        
        }

        public void UpdateTip(string tip)
        { 
        
        }

        public override void OnShow(object data)
        {
            
        }

        public override void OnClose()
        {
            isRotate = false;
        }
    }
}
