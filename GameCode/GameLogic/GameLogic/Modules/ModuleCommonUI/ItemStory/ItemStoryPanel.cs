﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Game.UI.UIComponent;

namespace ModuleCommonUI
{
    public class ItemStoryPanel : BasePanel
    {
        private StateText _contentTxt;
        private StateText _titleTxt;
        private string _contentTemplate;
        private KDummyButton _dummyBtn;

        protected override PanelIdEnum Id
        {
            get 
            {
                return PanelIdEnum.ItemStory;
            }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _contentTxt = GetChildComponent<StateText>("Container_content/Label_content");
            _titleTxt = GetChildComponent<StateText>("Container_content/Label_title");
            _contentTxt.CurrentText.lineSpacing = 1.0f;
            _contentTemplate = _contentTxt.CurrentText.text;
            _dummyBtn = AddChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
            _dummyBtn.onClick.AddListener(OnDummyClick);
        }

        private void OnDummyClick()
        {
            ClosePanel();
        }

        public override void OnShow(object data)
        {
            if (data is ItemStoryData)
            {
                ItemStoryData wrapper = data as ItemStoryData;
                _contentTxt.CurrentText.text = wrapper.content;
                _titleTxt.CurrentText.text = wrapper.title;
            }
            if (data is string)
            {
                _contentTxt.CurrentText.text = string.Format(_contentTemplate, data.ToString());
            }
            _contentTxt.RecalculateCurrentStateHeight();
        }

        public override void OnClose()
        {
            _contentTxt.CurrentText.text = string.Empty;
        }
    }
}
