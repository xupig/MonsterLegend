﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Game.UI.UIComponent;

namespace ModuleCommonUI
{
    public class ItemStoryData
    {
        public string title;
        public string content;

        public ItemStoryData(string title, string content)
        {
            this.title = title;
            this.content = content;
        }
    }
}
