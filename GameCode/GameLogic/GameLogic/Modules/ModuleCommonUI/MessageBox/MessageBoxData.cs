﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine.Events;

namespace ModuleCommonUI
{
    public class MessageBoxData
    {
        public string title;
        public string content;
        public string confirmLabel;
        public string cancelLabel;
        public Action confirmAction;
        public Action cancelAction;

        /// <summary>
        /// 是否隐藏关闭按钮[true:隐藏,false:显示]
        /// </summary>
        public bool isHideCloseBtn;
        /// <summary>
        /// 是否模态
        /// </summary>
        public bool isModal;
        /// <summary>
        /// 是否模态可以点击取消
        /// </summary>
        public bool modelCanCancel = true;
        /// <summary>
        /// 是否调换确定和取消按钮的位置
        /// </summary>
        public bool reverseBtn = false;
        /// <summary>
        /// 额外参数
        /// </summary>
        public object param;
    }

    public class MessageBoxCountdownWrapper
    {
        public int count;
        public int clickType;

        public MessageBoxCountdownWrapper(int count, int clickType)
        {
            this.count = count;
            this.clickType = clickType;
        }
    }
}
