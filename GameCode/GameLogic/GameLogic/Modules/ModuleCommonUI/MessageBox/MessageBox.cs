﻿#region 模块信息
/*==========================================
// 模块名：MessageBox
// 命名空间: ModuleCommonUI
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/08
// 描述说明：MessageBox.ShowMsgBox
// 其他：
//==========================================*/
#endregion
using System;
using System.Collections.Generic;
using UnityEngine;
using Common.Base;
using Game.UI.UIComponent;
using UnityEngine.Events;
using GameMain.GlobalManager;
using GameLoader.Utils;
using Common.Utils;
using Common.Global;
using GameLoader.Utils.Timer;
using Common.ServerConfig;
using MogoEngine;
using Common.ClientConfig;

namespace ModuleCommonUI
{
    public class MessageBox : BasePanel
    {
        public static string DEFAULT_TITLE = MogoLanguageUtil.GetContent((int)LangEnum.PROMPT);
        private StateText _titleTxt;
        private StateText _contentTxt;
        private StateText _confirmBtnTxt;
        private StateText _cancelBtnTxt;
        private KButton _confirmBtn;
        private KButton _cancelBtn;
        private KDummyButton _dummyBtn;

        private string _confirmLabel;
        private string _cancelLabel;
        private Action _confirmAction;
        private Action _cancelAction;

        private Locater _confirmLocater;
        private Locater _cancelLocater;
        private List<Vector2> _positionList = new List<Vector2>();

        private uint _timeId;

        private MessageBoxData _data;


        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.MessageBox; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_Bg/Button_close");
            _titleTxt = GetChildComponent<StateText>("Label_txtTitle");
            _contentTxt = GetChildComponent<StateText>("Container_Content/Label_txtContent");
            _contentTxt.ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);
            _confirmBtn = GetChildComponent<KButton>("Button_queding");
            _cancelBtn = GetChildComponent<KButton>("Button_quxiao");
            _confirmBtnTxt = _confirmBtn.GetChildComponent<StateText>("label");
            _confirmBtnTxt.ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);
            _confirmLabel = _confirmBtnTxt.CurrentText.text;
            _confirmLabel = string.Format(_confirmLabel, "");

            _cancelBtnTxt = _cancelBtn.GetChildComponent<StateText>("label");
            _cancelBtnTxt.ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);
            _cancelLabel = _cancelBtnTxt.CurrentText.text;
            _cancelLabel = string.Format(_cancelLabel, "");
            ModalMask = GetChildComponent<StateImage>("Container_Bg/ScaleImage_sharedZhezhao");
            _dummyBtn = AddChildComponent<KDummyButton>("Container_Bg/ScaleImage_sharedZhezhao");
            _confirmLocater = _confirmBtn.gameObject.AddComponent<Locater>();
            _cancelLocater = _cancelBtn.gameObject.AddComponent<Locater>();
            _positionList.Add(_confirmLocater.Position);
            _positionList.Add(_cancelLocater.Position);
        }

        private void AddEventListener()
        {
            _dummyBtn.onClick.AddListener(OnDummyClick);
            _confirmBtn.onClick.AddListener(OnConfirmClick);
            _cancelBtn.onClick.AddListener(OnCancelClick);
        }

        private void RemoveEventListener()
        {
            _dummyBtn.onClick.RemoveListener(OnDummyClick);
            _confirmBtn.onClick.RemoveListener(OnConfirmClick);
            _cancelBtn.onClick.RemoveListener(OnCancelClick);
        }

        private void OnConfirmClick()
        {
            ClearCountDown();
            ClosePanel();
            if(_confirmAction != null)
            {
                Action tempConfirmAction = _confirmAction;
                _confirmAction = null;
                tempConfirmAction.Invoke();
            }
        }

        private void OnCancelClick()
        {
            ClearCountDown();
            ClosePanel();
            if(_cancelAction != null)
            {
                Action tempCancelAction = _cancelAction;
                _cancelAction = null;
                tempCancelAction.Invoke();
            }
        }

        private void OnDummyClick()
        {
            if (_data.modelCanCancel)
            {
                ClosePanel();
            }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void SetData(object data)
        {
            _data = data as MessageBoxData;
            ClearCountDown();
            Refresh();
            ProcessParam();
        }

        public override void OnClose()
        {
            RemoveEventListener();
            _data = null;
        }

        public override void OnLeaveMap(int mapType)
        {

        }

        private void Refresh()
        {
            RefreshTitle();
            RefreshContent();
            RefreshConfirmBtn();
            RefreshCancelBtn();
            RefreshDummyBtn();
            RefreshBtnLayout();
            RefreshCloseBtn();
        }

        private void RefreshTitle()
        {
            _titleTxt.CurrentText.text = _data.title;
        }

        private void RefreshContent()
        {
            _contentTxt.CurrentText.text = _data.content;
        }

        private void RefreshConfirmBtn()
        {
            if(string.IsNullOrEmpty(_data.confirmLabel) == false)
            {
                _confirmLabel = _data.confirmLabel;
            }
            else
            {
                _confirmLabel = MogoLanguageUtil.GetString(LangEnum.CONFIRM_TEXT);
            }
            RefreshBtnLabel(_confirmBtn, _confirmLabel);
            _confirmAction = _data.confirmAction;
        }

        private void RefreshCancelBtn()
        {
            if (string.IsNullOrEmpty(_data.cancelLabel) == false)
            {
                _cancelLabel = _data.cancelLabel;
            }
            else {
                _cancelLabel = MogoLanguageUtil.GetString(LangEnum.CANCEL_TEXT);
            }
            RefreshBtnLabel(_cancelBtn, _cancelLabel);
            if(_data.cancelAction == null)
            {
                _cancelBtn.gameObject.SetActive(false);
            }
            else
            {
                _cancelBtn.gameObject.SetActive(true);
            }
            _cancelAction = _data.cancelAction;
        }

        private void RefreshBtnLabel(KButton btn, string label)
        {
            btn.GetChildComponent<StateText>("label").ChangeAllStateText(label);
        }

        private void RefreshDummyBtn()
        {
            if(_data.isModal == true)
            {
                _dummyBtn.gameObject.SetActive(true);
            }
            else
            {
                _dummyBtn.gameObject.SetActive(false);
            }
        }

        private void RefreshBtnLayout()
        {
            if(_confirmLocater.gameObject.activeSelf == true && _cancelLocater.gameObject.activeSelf == true)
            {
                if (_data.reverseBtn == true)
                {
                    _confirmLocater.Position = _positionList[1];
                    _cancelLocater.Position = _positionList[0];
                }
                else
                {
                    _confirmLocater.Position = _positionList[0];
                    _cancelLocater.Position = _positionList[1];
                }
            }
            else
            {
                _confirmLocater.X = (_positionList[0].x + _positionList[1].x + _cancelLocater.Width - _confirmLocater.Width) * 0.5f;
            }
        }

        private void RefreshCloseBtn()
        {
            CloseBtn.Visible = !_data.isHideCloseBtn;
        }

        private void ProcessParam()
        {
            if(_data.param != null)
            {
                Type type = _data.param.GetType();
                if(type == typeof(MessageBoxCountdownWrapper))
                {
                    ProcessCoundDownWrapper();
                }
                else if(type == typeof(TextAnchor))
                {
                    ProcessTextAnchor();
                }
            }
        }

        private void ProcessTextAnchor()
        {
            if (_data.param is TextAnchor)
            {
                _contentTxt.CurrentText.alignment = (TextAnchor)_data.param;
            }
            else
            {
                _contentTxt.CurrentText.alignment = TextAnchor.MiddleCenter;
            }
        }

        private void ProcessCoundDownWrapper()
        {
            MessageBoxCountdownWrapper wrapper = _data.param as MessageBoxCountdownWrapper;
            if (wrapper != null)
            {
                _confirmBtn.interactable = false;
                if (_timeId != 0) { TimerHeap.DelTimer(_timeId); }
                _timeId = TimerHeap.AddTimer(0, 1000, OnTimer);
            }
            /*else if (wrapper != null)
            {
                _confirmBtn.interactable = true;
                _timeId = TimerHeap.AddTimer(0, 1000, OnTimer);
            }*/
        }

        private void OnTimer()
        {
			if (_data == null)
			{ 
				if (_timeId != 0) { TimerHeap.DelTimer(_timeId); }
				_timeId = 0;
                return;
			}
            MessageBoxCountdownWrapper wrapper = _data.param as MessageBoxCountdownWrapper;
            string label1 = _confirmLabel;
            string label2 = _cancelLabel;
            if(wrapper.count > 0)
            {
                label1 = string.Format(_confirmLabel, wrapper.count.ToString());
                label2 = string.Format(_cancelLabel, wrapper.count.ToString());
                _contentTxt.CurrentText.text = string.Format(_data.content, wrapper.count.ToString());
            }
            RefreshBtnLabel(_confirmBtn, label1);
            RefreshBtnLabel(_cancelBtn, label2);
            if(wrapper.count <= 0)
            {
                ClearCountDown();
                if (wrapper != null && wrapper.clickType != InstanceDefine.MESSAGEBOX_AUTO_COUNTDOWN_CLICK)
                {
                    _confirmBtn.interactable = true;
                }
                else if (wrapper != null && wrapper.clickType != InstanceDefine.MESSAGEBOX_SURE_COUNTDOWN_END_CLICK)
                {
                    ClosePanel();
                }
            }
            wrapper.count--;
        }

        private void ClearCountDown()
        {
            if(_timeId != 0)
            {
                TimerHeap.DelTimer(_timeId);
                _timeId = 0;
            }
        }

        /// <summary>
        /// 完整版接口
        /// </summary>
        /// <param name="isModal"></param>
        /// <param name="title"></param>
        /// <param name="content"></param>
        /// <param name="confirmAction"></param>
        /// <param name="cancelAction"></param>
        /// <param name="confirmLabel"></param>
        /// <param name="cancelLabel"></param>
        /// <param name="param"></param>
        /// <param name="isHideCloseBtn">是否隐藏右上角关闭按钮[true:隐藏,false:显示]</param>
        public static void Show(bool isModal, string title, string content, Action confirmAction, Action cancelAction, string confirmLabel, string cancelLabel, object param = null, bool isHideCloseBtn = false, bool modelCanCancel = true, bool reverseBtn = false)
        {
            //
            MessageBoxData data = new MessageBoxData();
            data.isModal = isModal;
            data.title = title;
            data.content = content;
            data.confirmAction = confirmAction;
            data.cancelAction = cancelAction;
            data.confirmLabel = confirmLabel;
            data.cancelLabel = cancelLabel;
            data.param = param;
            data.isHideCloseBtn = isHideCloseBtn;
            data.modelCanCancel = modelCanCancel;
            data.reverseBtn = reverseBtn;
            UIManager.Instance.ShowPanel(PanelIdEnum.MessageBox, data);
        }
        /// <summary>
        /// 展示信息，显示确定按钮
        /// 点击确定按钮关闭面板
        /// </summary>
        /// <param name="isModal"></param>
        /// <param name="content"></param>
        /// <param name="isHideCloseBtn">是否隐藏右上角关闭按钮[true:隐藏,false:显示]</param>
        public static void Show(bool isModal, string title, string content, bool isHideCloseBtn = false, bool modelCanCancel = true)
        {
            Show(isModal, string.IsNullOrEmpty(title) ? DEFAULT_TITLE : title, content, null, null, null, null, null, isHideCloseBtn, modelCanCancel);
        }

        /// <summary>
        /// 展示提示，显示确定和取消按钮
        /// 点击确定按钮执行指定动作并关闭界面
        /// 点击取消按钮关闭界面
        /// </summary>
        /// <param name="isModal"></param>
        /// <param name="content"></param>
        /// <param name="confirmAction"></param>
        /// <param name="isHideCloseBtn">是否隐藏右上角关闭按钮[true:隐藏,false:显示]</param>
        /// <param name="modelCanCancel">是否允许模态时，点击模态区域产生cancel点击效果[true:隐藏,false:显示]</param>
        public static void Show(bool isModal, string title, string content, Action confirmAction, bool isHideCloseBtn = false, bool modelCanCancel = true)
        {
            Show(isModal, string.IsNullOrEmpty(title) ? DEFAULT_TITLE : title, content, confirmAction, null, null, null, null, isHideCloseBtn, modelCanCancel);
        }

        /// <summary>
        /// 展示提示，显示确定和取消按钮
        /// 点击确认按钮执行指定动作并关闭界面
        /// 点击取消按钮执行指定动作并关闭界面
        /// </summary>
        /// <param name="isModal"></param>
        /// <param name="content"></param>
        /// <param name="confirmAction"></param>
        /// <param name="isHideCloseBtn">是否隐藏右上角关闭按钮[true:隐藏,false:显示]</param>
        /// <param name="modelCanCancel">是否允许模态时，点击模态区域产生cancel点击效果[true:隐藏,false:显示]</param>
        public static void Show(bool isModal, string title, string content, Action confirmAction, Action cancelAction, bool isHideCloseBtn = false, bool modelCanCancel = true, bool reverseBtn = false)
        {
            Show(isModal, title, content, confirmAction, cancelAction, null, null, null, isHideCloseBtn, modelCanCancel, reverseBtn);
        }

        public static void CloseMessageBox()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.MessageBox);
        }

        public static void ShowFunctionUnrealized()
        {
            Show(true, DEFAULT_TITLE, MogoLanguageUtil.GetContent(122114));
        }
    }
}
