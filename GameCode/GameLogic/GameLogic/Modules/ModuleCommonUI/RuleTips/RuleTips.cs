﻿using Common.Base;
using Common.Data;
using Common.Global;
using Common.Utils;
using Game.Asset;
using Game.UI.TextEngine;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace ModuleCommonUI
{
    public class RuleTips : BasePanel
    {
        private const int PADDING = 40;
        private const float GAP = 20;

        private StateText _txtContent;
        private StateImage _imgBg;
        private KDummyButton _button;
        private RectTransform _rectContent;
        private KContainer _containerContent;
        private RectTransform _rectBackground;

        private Vector2 _parentOffect;

        protected override void Awake()
        {
            base.Awake();
            _txtContent = GetChildComponent<StateText>("Container_neirong/Label_content");
            _imgBg = GetChildComponent<StateImage>("Container_neirong/ScaleImage_bg");
            _imgBg.AddAllStateComponent<Resizer>();
            _rectBackground = _imgBg.GetComponent<RectTransform>();

            InitParentOffset();
            _rectContent = GetChildComponent<RectTransform>("Container_neirong");
            _containerContent = GetChildComponent<KContainer>("Container_neirong");

            _button = gameObject.AddComponent<KDummyButton>();
            _button.onClick.AddListener(ClosePanel);
        }

        private void InitParentOffset()
        {
            RectTransform rectPanel = gameObject.GetComponent<RectTransform>();
            Vector2 sizeDelta = rectPanel.sizeDelta;
            Vector2 pivot = rectPanel.pivot;
            _parentOffect = rectPanel.anchoredPosition - 
                new Vector2(sizeDelta.x * pivot.x, -(rectPanel.sizeDelta.y * pivot.y));
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.RuleTips; }
        }

        public override void OnShow(object data)
        {
            RuleTipsDataWrapper wrapper = data as RuleTipsDataWrapper;
            _txtContent.CurrentText.text = wrapper.Content;
            _txtContent.RecalculateAllStateHeight();
            RecalculateBg();
            _containerContent.RecalculateSize();
            RecalculateLocation(wrapper.Rect, wrapper.Direction);
        }

        private void RecalculateBg()
        {
            RectTransform contentRect = _txtContent.GetComponent<RectTransform>();
            _imgBg.Height = contentRect.sizeDelta.y + 2 * PADDING;
            _imgBg.Width = contentRect.sizeDelta.x + 2 * PADDING;
        }

        private void RecalculateLocation(RectTransform rectTransform, RuleTipsDirection direction)
        {
            Vector2 size = rectTransform.sizeDelta;
            Vector2 screenPosition = RectTransformUtility.WorldToScreenPoint(UIManager.Instance.UICamera, rectTransform.position) / Global.Scale;
            screenPosition.y -= Screen.height / Global.Scale;
            Vector2 topLeftPosition = screenPosition - _parentOffect;
            switch (direction)
            {
                case RuleTipsDirection.Top:
                    _rectContent.pivot = new Vector2(0.5f, 0f);
                    _rectContent.anchoredPosition = topLeftPosition + new Vector2(size.x / 2, 0);
                    break;
                case RuleTipsDirection.Right:
                    _rectContent.pivot = new Vector2(0f, 0.5f);
                    _rectContent.anchoredPosition = topLeftPosition + new Vector2(size.x, -size.y / 2);
                    break;
                case RuleTipsDirection.Left:
                    _rectContent.pivot = new Vector2(1, 0.5f);
                    _rectContent.anchoredPosition = topLeftPosition + new Vector2(0, -size.y / 2);
                    break;
                case RuleTipsDirection.Bottom:
                    _rectContent.pivot = new Vector2(0.5f, 1f);
                    _rectContent.anchoredPosition = topLeftPosition + new Vector2(size.x / 2, -size.y);
                    break;
                case RuleTipsDirection.TopRight:
                    _rectContent.pivot = new Vector2(0, 0);
                    _rectContent.anchoredPosition = topLeftPosition + new Vector2(size.x, 0);
                    break;
                case RuleTipsDirection.TopLeft:
                    _rectContent.pivot = new Vector2(1, 0);
                    _rectContent.anchoredPosition = topLeftPosition;
                    break;
                case RuleTipsDirection.BottomLeft:
                    _rectContent.pivot = new Vector2(1, 1);
                    _rectContent.anchoredPosition = topLeftPosition + new Vector2(0, -size.y);
                    break;
                case RuleTipsDirection.BottomRight:
                    _rectContent.pivot = new Vector2(0, 1);
                    _rectContent.anchoredPosition = topLeftPosition + new Vector2(size.x, -size.y);
                    break;
            }
        }

        public override void OnClose()
        {
        }
    }
}
