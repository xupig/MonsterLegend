﻿#region 模块信息
/*==========================================
// 文件名：ItemObtainUtils
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.ItemObtain
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/5 20:39:23
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Structs.ProtoBuf;
using System.Collections.Generic;
namespace ModuleCommonUI
{
    public class ItemObtainUtils
    {
        public static List<BaseItemData> ConvertDataToItemDataList(PbRewardList rewardList)
        {
            List<BaseItemData> result = new List<BaseItemData>();
            for (int i = 0; i < rewardList.item_list.Count; i++)
            {
                BaseItemData itemData = ItemDataCreator.Create((int)rewardList.item_list[i].id);
                itemData.StackCount = (int)rewardList.item_list[i].count;
                result.Add(itemData);
            }
            return result;
        }

        public static List<BaseItemData> ConvertDataToItemDataList(List<PbItem> itemList)
        {
            List<BaseItemData> result = new List<BaseItemData>();
            for (int i = 0; i < itemList.Count; i++)
            {
                BaseItemData itemData = ItemDataCreator.Create((int)itemList[i].item_id);
                itemData.StackCount = (int)itemList[i].item_count;
                result.Add(itemData);
            }
            return result;
        }

        public static List<BaseItemData> ConvertDataToItemDataList(PbRuneItemInfoList runeData)
        {
            List<BaseItemData> result = new List<BaseItemData>();
            List<PbRuneItemInfo> runeList = runeData.item_info;
            for (int i = 0; i < runeList.Count; i++)
            {
                BaseItemData itemData = ItemDataCreator.Create((int)runeList[i].item_id);
                itemData.StackCount = (int)runeList[i].item_num;
                result.Add(itemData);
            }
            return result;
        }

    }
}