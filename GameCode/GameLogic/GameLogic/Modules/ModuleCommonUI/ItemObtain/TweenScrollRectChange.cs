﻿#region 模块信息
/*==========================================
// 文件名：TweenScrollRectChange
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.ItemObtain
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/8/11 19:41:22
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using UnityEngine;
public class TweenScrollRectChange : UITweener
{

    public float from = 1.0f;
    public float to = 1.0f;

    private KScrollRect _scrollRect;

    public float YNormalizePosition
    {
        get
        {
            return _scrollRect.verticalNormalizedPosition;
        }
        set
        {
            _scrollRect.verticalNormalizedPosition = Mathf.Clamp01(value);
        }
    }

    void Awake()
    {
        _scrollRect = GetComponent<KScrollRect>();
    }

    protected override void OnUpdate(float factor, bool isFinished)
    {
        YNormalizePosition = Mathf.Lerp(from, to, factor);
    }

    static public TweenScrollRectChange Begin(GameObject go, float duration, float position, Method method)
    {
        TweenScrollRectChange comp = UITweener.Begin<TweenScrollRectChange>(go, duration);
        comp.from = comp.YNormalizePosition;
        comp.to = position;
        comp.method = method;

        if (duration <= 0f)
        {
            comp.Sample(1f, true);
            comp.enabled = false;
        }
        return comp;
    }

}
