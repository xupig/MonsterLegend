﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Game.UI.UIComponent;


namespace ModuleCommonUI
{
    public class ItemObtainBackView : KContainer
    {
        private StateImage _backImage;
        private Locater _backLocater;
        private StateImage _lineImageLeft;
        private Locater _lineLocaterLeft;
        private StateImage _lineImageRight;
        private Locater _lineLocaterRight;

        protected override void Awake()
        {
            _backImage = GetChildComponent<StateImage>("ScaleImage_baoxiangjiangli");
            _backImage.AddAllStateComponent<Resizer>();
            _backLocater = AddChildComponent<Locater>("ScaleImage_baoxiangjiangli");
            _lineImageLeft = GetChildComponent<StateImage>("Image_linebgDown_left");
            _lineLocaterLeft = AddChildComponent<Locater>("Image_linebgDown_left");
            _lineImageRight = GetChildComponent<StateImage>("Image_linebgDown_right");
            _lineLocaterRight = AddChildComponent<Locater>("Image_linebgDown_right");
        }

        public void SetHeight(float value)
        {
            _backImage.Height = value;
            _lineLocaterLeft.Y = _backLocater.Y - _backLocater.Height + _lineLocaterLeft.Height;
            // 由于镜像图片的左边和右边的Pivot不一致修改，Left Pivot（0，1）， Right Pivot(0.5, 0.5)
            _lineLocaterRight.Y = _lineLocaterLeft.Y - 0.5f * _lineLocaterLeft.Height;
        }
    }
}
