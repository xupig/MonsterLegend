﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Game.UI.UIComponent;
using Common.ExtendComponent;
using Common.Data;
using GameMain.GlobalManager;
using GameData;
using ModuleRune.ChildComponent;
using Common.ServerConfig;
using Game.UI;


namespace ModuleCommonUI
{
    public class ItemObtainItem : KList.KListItemBase
    {
        private ItemGrid _itemGrid;
        private BaseItemData _data;
        private StateText _nameTxt;
        private StateText _countTxt;
        private KContainer _wrapperContainer;

        protected override void Awake()
        {
            _wrapperContainer = GetChildComponent<KContainer>("Container_wrapper");
            _itemGrid = _wrapperContainer.GetChildComponent<ItemGrid>("Container_wupin");
            _nameTxt = _wrapperContainer.GetChildComponent<StateText>("Label_txtName");
            _countTxt = _wrapperContainer.GetChildComponent<StateText>("Label_txtNum");
            onClick.AddListener(ShowItemTips);
        }

        private void ShowItemTips(KList.KListItemBase arg0, int arg1)
        {
            ToolTipsManager.Instance.ShowItemTip(_data.Id, PanelIdEnum.ItemObtain);
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as BaseItemData;
                Refresh();
                BagItemType itemType = item_helper.GetItemType(_data.Id);
                if(itemType == BagItemType.Rune)
                {
                    _itemGrid.onClick = OnClick;
                }
            }
        }


        private void OnClick()
        {
            if (_data.Id != 0)
            {
                rune rune = rune_helper.GetRune(_data.Id);
                RuneTipsData bagData = new RuneTipsData();
                bagData.pos = _data.Id;
                bagData.toPos = -1;
                bagData.bagType = BagType.Undefined;
                if (rune.__subtype == public_config.RUNE_EXP || rune.__subtype == public_config.RUNE_MONEY)
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.ExpRuneToolTips, bagData, PanelIdEnum.Rune);
                }
                else
                {
                    ToolTipsManager.Instance.ShowTip(PanelIdEnum.RuneToolTips, bagData, PanelIdEnum.Rune);
                }

            }
        }

        private void Refresh()
        {
            _itemGrid.SetItemData(_data, onItemLoaded);
            _nameTxt.CurrentText.text = _data.ColorName;
            if(_data.StackCount > 1)
            {
                _countTxt.CurrentText.text = "x" + _data.StackCount;
            }
            else
            {
                _countTxt.CurrentText.text = string.Empty;
            }
            SetAllStateTextDimensionDirty(gameObject);
            SetAllStateImageDimensionDirty(gameObject);
        }

        private void onItemLoaded(GameObject obj)
        {
            SetAllStateImageDimensionDirty(obj);
        }

        public override void Dispose()
        {
            _data = null;
            _itemGrid.onClick = null;
        }

        private void SetAllStateTextDimensionDirty(GameObject go)
        {
            TextWrapper[] arr = go.GetComponentsInChildren<TextWrapper>();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i].SetContinuousDimensionDirty(true);
            }
        }

        private void SetAllStateImageDimensionDirty(GameObject go)
        {
            ImageWrapper[] arr = go.GetComponentsInChildren<ImageWrapper>();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i].SetContinuousDimensionDirty(true);
            }
        }
    }
}
