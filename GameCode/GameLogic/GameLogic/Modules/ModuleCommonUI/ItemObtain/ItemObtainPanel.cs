﻿using System.Collections.Generic;
using Common.Base;
using Game.UI.UIComponent;
using Common.Data;
using GameMain.GlobalManager;
using Common.Structs.ProtoBuf;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using GameLoader.Utils.Timer;
using System.Collections;
using Common.ExtendComponent;
using GameMain.GlobalManager.SubSystem;
using GameData;

namespace ModuleCommonUI
{
    public class ItemObtainPanel : BasePanel
    {
        public const float ItemScaleTime = 0.2f;
        public const float ItemDelayTime = 0.1f;
        public const float ScrollViewDragTime = 0.5f;

        private KContainer equipDecomposizeContainer;
        private KShrinkableButton _goStrengthenViewBtn;
        private KShrinkableButton _goMakeViewBtn;
        private KShrinkableButton _closeBtn;
        private KContainer _titleView;
        private ItemObtainBackView _backView;
        private KPageableList _itemList;
        private KScrollView _itemScrollView;
        private Locater _itemListLocater;
        private Locater _scrollViewLocater;
        private Locater _btnLocater;
        private Locater[] _btnLocaters;
        private Locater _equipDecomposizeLocater;
        private List<BaseItemData> _itemDataList = new List<BaseItemData>();
        private uint timeId;
        private int currentIndex;

        private KParticle _particle;


        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.ItemObtain; }
        }

        protected override void Awake()
        {
            base.Awake();
            equipDecomposizeContainer = GetChildComponent<KContainer>("Container_qianwangzhuangbei");
            _closeBtn = equipDecomposizeContainer.GetChildComponent<KShrinkableButton>("Button_queding");
            _goStrengthenViewBtn = equipDecomposizeContainer.GetChildComponent<KShrinkableButton>("Button_qianghua");
            _goMakeViewBtn = equipDecomposizeContainer.GetChildComponent<KShrinkableButton>("Button_zhizao");
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            CloseBtn = GetChildComponent<KButton>("Button_queding");
            InitBtnLocaters();
            _backView = AddChildComponent<ItemObtainBackView>("Container_zhuangshi");
            _itemScrollView = GetChildComponent<KScrollView>("ScrollView_wupinhuode");
            _scrollViewLocater = AddChildComponent<Locater>("ScrollView_wupinhuode");
            _itemList = GetChildComponent<KPageableList>("ScrollView_wupinhuode/mask/content");
            _itemList.SetDirection(KList.KListDirection.LeftToRight, 5);
            _itemList.SetGap(0, 0);
            _itemListLocater = AddChildComponent<Locater>("ScrollView_wupinhuode/mask/content");
            _titleView = GetChildComponent<KContainer>("Container_title");
            _particle = _titleView.AddChildComponent<KParticle>("fx_ui_26_guangshuhuanrao_01");
            _particle.Position = new Vector3(283, 0, 0);
            _particle.Stop();
            AddEventListener();
        }

        private void AddEventListener()
        {
            _closeBtn.onClick.AddListener(ClosePanel);
            _goMakeViewBtn.onClick.AddListener(OpenMakeView);
            _goStrengthenViewBtn.onClick.AddListener(OpenStrengthenView);
        }

        private void OpenStrengthenView()
        {
            ClosePanel();
            view_helper.OpenView(441);
        }

        private void OpenMakeView()
        {
            ClosePanel();
            UIManager.Instance.ShowPanel(PanelIdEnum.Make);
        }

        private void InitBtnLocaters()
        {
            _btnLocater = AddChildComponent<Locater>("Button_queding");
            _equipDecomposizeLocater = equipDecomposizeContainer.gameObject.AddComponent<Locater>();
            _btnLocaters = new Locater[3];
            _btnLocaters[0] = _goStrengthenViewBtn.gameObject.AddComponent<Locater>();
            _btnLocaters[1] = _goMakeViewBtn.gameObject.AddComponent<Locater>();
            _btnLocaters[2] = _closeBtn.gameObject.AddComponent<Locater>();
        }

        public override void OnShow(object data)
        {
            if (data == null)
            {
                ClosePanel();
                return;
            }
            HideDecomposizeBtns();
            _itemScrollView.ResetContentPosition();
            if (data is PbRewardList)
            {
                PbRewardList pbRewardList = data as PbRewardList;
                const int equip_descomposize = 22;
                if (pbRewardList.get_way == equip_descomposize)
                {
                    ShowDecomposizeBtns();
                }
                _itemDataList = ItemObtainUtils.ConvertDataToItemDataList(pbRewardList);
            }
            else if (data is List<PbItem>)
            {
                _itemDataList = ItemObtainUtils.ConvertDataToItemDataList(data as List<PbItem>);
            }
            else if (data is PbRuneItemInfoList)
            {
                DramaTrigger.RuneWishEnd();
                _itemDataList = ItemObtainUtils.ConvertDataToItemDataList(data as PbRuneItemInfoList);
            }
            else if (data is List<BaseItemData>)
            {
                _itemDataList = data as List<BaseItemData>;
            }
            UIManager.Instance.PlayAudio("Sound/UI/get_reward.mp3");
            _itemList.SetDataList<ItemObtainItem>(_itemDataList, 10);
            StartCoroutine(WaitForCreatedListEnded());
        }

        private void HideDecomposizeBtns()
        {
            CloseBtn.Visible = true;
            _closeBtn.Visible = false;
            _goMakeViewBtn.Visible = false;
            _goStrengthenViewBtn.Visible = false;
        }

        private void ShowDecomposizeBtns()
        {
            CloseBtn.Visible = false;
            _closeBtn.Visible = true;
            _goMakeViewBtn.Visible = function_helper.IsFunctionOpen(FunctionId.make);
            _goStrengthenViewBtn.Visible = function_helper.IsFunctionOpen(FunctionId.equip);
            int count = 0;
            for (int i = 0; i < _btnLocaters.Length; i++)
            {
                if (_btnLocaters[i].IsActive())
                {
                    count++;
                }
            }
            float btnWidth = _goMakeViewBtn.GetComponent<RectTransform>().rect.width;
            float totalWidth = _equipDecomposizeLocater.GetComponent<RectTransform>().rect.width;
            float lerpLength = (totalWidth - btnWidth * count) / (count + 1);
            Vector2 startVector = new Vector2(lerpLength, 0);
            for (int i = 0; i < _btnLocaters.Length; i++)
            {
                if (_btnLocaters[i].IsActive())
                {
                    _btnLocaters[i].Position = startVector;
                    startVector += new Vector2(btnWidth + lerpLength, 0);
                }
            }
            _closeBtn.RefreshRectTransform();
            _goMakeViewBtn.RefreshRectTransform();
            _goStrengthenViewBtn.RefreshRectTransform();
        }

        private IEnumerator WaitForCreatedListEnded()
        {
            while (_itemList.ItemList.Count < _itemDataList.Count)
            {
                yield return null;
            }
            RefreshScrollViewPosition();
            HideAllItems();
            RefreshBackHeight();
            ShowItemAnimation();
        }

        private void HideAllItems()
        {
            for (int i = 0; i < _itemDataList.Count && i < 10; i++)
            {
                RectTransform rectTranform = _itemList.ItemList[i].GetComponent<RectTransform>();
                rectTranform.pivot = new Vector2(0.5f, 0.5f);
                rectTranform.anchoredPosition += new Vector2(rectTranform.sizeDelta.x * 0.5f, -rectTranform.sizeDelta.y * 0.5f);
                rectTranform.localScale = new Vector3(0.01f, 0.01f, 1);
                _itemList.ItemList[i].Visible = false;
            }
        }

        private void ShowItemAnimation()
        {
            _particle.Play(true);
            float startTime = 0.0f;
            for (int i = 0; i < 10 && i < _itemDataList.Count; i++)
            {
                _itemList.ItemList[i].Visible = true;
                RectTransform rectTran = _itemList.ItemList[i].GetComponent<RectTransform>();
                TweenScale.Begin(_itemList.ItemList[i].gameObject, startTime, ItemScaleTime, new Vector3(1.0f, 1.0f, 1.0f));
                startTime += ItemDelayTime;
            }
            currentIndex = Mathf.Min(_itemDataList.Count, 10);
            uint startMilliSecs = (uint)Mathf.CeilToInt(startTime * 1000.0f + 500);
            timeId = TimerHeap.AddTimer(startMilliSecs, 800, DragScrollView);
        }

        private void DragScrollView()
        {

            GameObject mask = _itemScrollView.GetChildComponent<RectTransform>("mask").gameObject;
            if (mask == null)
            {
                TimerHeap.DelTimer(timeId);
                return;
            }
            float maskLength = _itemScrollView.GetChildComponent<RectTransform>("mask").sizeDelta.y;
            float contentLength = _itemList.GetComponent<RectTransform>().sizeDelta.y;
            float moveDistance = contentLength - maskLength > 0 ? contentLength - maskLength : 0;
            int rowNums = Mathf.CeilToInt(_itemDataList.Count * 0.2f);
            if (currentIndex >= 10 && currentIndex < _itemDataList.Count)
            {
                float height = GetItemHeight();
                float position = _itemScrollView.ScrollRect.verticalNormalizedPosition - height / moveDistance;
                TweenScrollRectChange.Begin(mask, ScrollViewDragTime, position, UITweener.Method.Linear);
                currentIndex += 5;
            }
            else
            {
                TimerHeap.DelTimer(timeId);
            }
        }

        private float GetItemHeight()
        {
            return _itemList.ItemSize.y;
        }

        private void RefreshScrollViewPosition()
        {
            _itemListLocater.X = (_scrollViewLocater.Width - _itemListLocater.Width) * 0.5f;
        }

        private void RefreshBackHeight()
        {
            if (_itemDataList.Count > 5)
            {
                _backView.SetHeight(387.0f);
                _btnLocater.Y = -596.0f;
                _equipDecomposizeLocater.Y = -596.0f;
                (CloseBtn as KShrinkableButton).RefreshRectTransform();
            }
            else
            {
                _backView.SetHeight(242.0f);
                _btnLocater.Y = -488.0f;
                _equipDecomposizeLocater.Y = -488.0f;
                (CloseBtn as KShrinkableButton).RefreshRectTransform();
            }
            EventDispatcher.TriggerEvent(PanelEvents.PANEL_LAYER_RESET_POS);
        }

        public override void OnClose()
        {
            _itemDataList.Clear();
            TimerHeap.DelTimer(timeId);
            EventDispatcher.TriggerEvent(CommonUIEvents.ON_ITEM_OBTAIN_PANEL_CLOSE);
        }
    }
}
