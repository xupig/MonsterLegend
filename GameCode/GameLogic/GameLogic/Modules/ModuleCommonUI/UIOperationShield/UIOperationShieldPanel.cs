﻿using Common.Base;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCommonUI
{
    public class UIOperationShieldPanel:BasePanel
    {
        protected override void Awake()
        {
            base.Awake();
            AddChildComponent<KDummyButton>("Container_zhezhao");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.UIOperationShield; }
        }

        public override void OnShow(object data)
        {
            ;
        }

        public override void OnClose()
        {
            ;
        }
    }
}
