﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleCommonUI
{
    public class RelivePopPanel : BasePanel
    {
        private StateText _contentTxt;
        private KButton _waitButton;
        private KButton _buyButton;
        private KButton _ensureButton;
        private StateText _reliveText;
        private StateText _buyTipText;
        private KContainer _buyTipContainer;
        private IconContainer _iconContainer;
        private StateIcon _stateIcon;
        private string _reliveContent;
        private string _buyTipContent;
        private uint _timeId;
        private ulong _startTime = 0;
        private ReliveData _data;

        private StateText _countDownText;
        private StateText _costText;
        private StateText _buyCostText;
        private int _cost = 0;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.RelivePop; }
        }

        protected override void Awake()
        {
            _contentTxt = GetChildComponent<StateText>("Container_Content/Label_txtContent");
            _waitButton = GetChildComponent<KButton>("Button_wait");
            _buyButton = GetChildComponent<KButton>("Button_buy");
            _ensureButton = GetChildComponent<KButton>("Button_queding");
            _reliveText = GetChildComponent<StateText>("Container_Content/Label_txtContent");
            _buyTipContainer = GetChildComponent<KContainer>("Container_Content/Container_goumaitishi");
            _buyTipText = _buyTipContainer.GetChildComponent<StateText>("Label_txttishi");
            _reliveContent = _reliveText.CurrentText.text;
            _buyTipContent = _buyTipText.CurrentText.text;
            _iconContainer = _buyTipContainer.AddChildComponent<IconContainer>("Container_icon");
            _stateIcon = _buyButton.GetChildComponent<StateIcon>("stateIcon");
            _countDownText = _waitButton.GetChildComponent<StateText>("label02");
            _costText = _buyTipContainer.GetChildComponent<StateText>("Label_shuliang");
            _buyCostText = _buyButton.GetChildComponent<StateText>("label02");
            _waitButton.interactable = false;
            ModalMask = GetChildComponent<StateImage>("Container_Bg/ScaleImage_sharedZhezhao");
        }

        private void AddEventListener()
        {
            _waitButton.onClick.AddListener(OnWaitButtonClick);
            _buyButton.onClick.AddListener(OnBuyButtonClick);
            _ensureButton.onClick.AddListener(OnEnsureButtonClick);
            EventDispatcher.AddEventListener(SpaceActionEvents.SpaceActionEvents_Complete, ClosePanel);
        }

        private void RemoveEventListener()
        {
            _waitButton.onClick.RemoveListener(OnWaitButtonClick);
            _buyButton.onClick.RemoveListener(OnBuyButtonClick);
            _ensureButton.onClick.RemoveListener(OnEnsureButtonClick);
            EventDispatcher.RemoveEventListener(SpaceActionEvents.SpaceActionEvents_Complete, ClosePanel);
        }

        private void OnEnsureButtonClick()
        {
            GameSceneManager.GetInstance().DoRelive();
            ClosePanel();
        }

        private void OnBuyButtonClick()
        {
            int result = PlayerAvatar.Player.CheckCostLimit(public_config.MONEY_TYPE_BIND_DIAMOND, _cost, true);
            if (result <= 0)
            {
                GameSceneManager.GetInstance().DoRelive();
                ClosePanel();
            }
        }

        private void OnWaitButtonClick()
        {

        }

        public override void OnShow(object data)
        {
            _data = data as ReliveData;
            RefreshView();
            StartCountDown();
            AddEventListener();
        }

        public override void OnClose()
        {
            RemoveEventListener();
            ResetTimer();
            _data = null;
        }

        private void RefreshView()
        {
            _reliveText.CurrentText.text = string.Format(_reliveContent, _data.currReliveCount);
            _buyTipText.CurrentText.text = string.Format(_buyTipContent, _data.currReliveCount);
            _iconContainer.SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_BIND_DIAMOND));
            _stateIcon.SetIcon(item_helper.GetIcon(public_config.MONEY_TYPE_BIND_DIAMOND));
            _buyTipContainer.Visible = false;
            _waitButton.Visible = false;
            _buyButton.Visible = false;
            _ensureButton.Visible = false;
            _reliveText.Visible = false;
        }

        private void StartCountDown()
        {
            ResetTimer();
            _buyTipContainer.Visible = true;
            _waitButton.Visible = true;
            _buyButton.Visible = true;
            _ensureButton.Visible = false;
            _reliveText.Visible = false;
            GameSceneManager.GetInstance().NoticeServePlayerDeath();
            _startTime = Global.serverTimeStamp;
            _timeId = TimerHeap.AddTimer(0, 100, OnStepCountDown);
        }

        private void OnStepCountDown()
        {
            ulong currentTime = Global.serverTimeStamp;
            if (currentTime - _startTime >= (ulong)_data.cdTime * 1000)
            {
                CountDownEnd();
                return;
            }
            RefreshCountDownView();
        }

        private void RefreshCountDownView()
        {
            ulong currentTime = Global.serverTimeStamp;
            ulong span = currentTime - _startTime;
            ulong remain = (ulong)_data.cdTime * 1000 - span;
            //Debug.LogError(span + " , " + remain + " , " + _data.cdTime);
            _countDownText.ChangeAllStateText(Mathf.RoundToInt(remain / 1000) + "s");
            _cost = (_data.price - _data.odds * Mathf.RoundToInt(span / 1000));
            _buyCostText.ChangeAllStateText("X" + _cost);
            _costText.CurrentText.text = "X" + _cost;
        }

        private void CountDownEnd()
        {
            ResetTimer();
            _buyTipContainer.Visible = false;
            _waitButton.Visible = false;
            _buyButton.Visible = false;
            _ensureButton.Visible = true;
            _reliveText.Visible = true;
            OnEnsureButtonClick();
        }

        private void ResetTimer()
        {
            if (_timeId != 0)
            {
                TimerHeap.DelTimer(_timeId);
                _timeId = 0;
            }
        }

    }
}
