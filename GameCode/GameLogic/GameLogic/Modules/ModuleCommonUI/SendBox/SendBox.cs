﻿#region 模块信息
/*==========================================
// 文件名：SendBox
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.SendBox
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/9 9:47:15
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ModuleCommonUI
{
    public class SendBox : BasePanel
    {
        private KButton _btnConfirm;
        private KButton _btnCancel;
        private KDummyButton _hintBtn;
        //private KButton _btnMask;
        private KInputField _inputMessage;
        private StateText _txtTitle;
        private StateImage _imgMask;

        private UnityAction _closeAction;

        private static SendBoxData _sendBoxData = new SendBoxData();


        public SendBox()
        {
            _closeAction = ClosePanel;
        }


        public static void ShowSendBox(string _title,string _defaultInputMessage,bool _isModal,UnityAction<string> _confirmAction,UnityAction _cancelAction)
        {
            _sendBoxData = new SendBoxData();
            _sendBoxData.defaultInputMessage = _defaultInputMessage;
            _sendBoxData.title = _title;
            _sendBoxData.isModal = _isModal;
            _sendBoxData.confirmAction = _confirmAction;
            _sendBoxData.cancelAction = _cancelAction;
            UIManager.Instance.ShowPanel(PanelIdEnum.SendBox);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.SendBox; }
        }

        protected override void Awake()
        {
            KContainer _container = this.gameObject.GetComponent<KContainer>();
            _btnConfirm = _container.GetChildComponent<KButton>("Button_fasong");
            _btnCancel = _container.GetChildComponent<KButton>("Button_quxiao");
            _inputMessage = _container.GetChildComponent<KInputField>("Input_txtMessage");
            _txtTitle = _container.GetChildComponent<StateText>("Label_txtYoujianbiaoti");
            _imgMask = _container.GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _hintBtn = AddChildComponent<KDummyButton>("Container_inputHint");

            _inputMessage.lineType = InputField.LineType.MultiLineNewline;
            //_btnMask = _container.GetChildComponent<KButton>("Button_Mask");
           // _btnMask.gameObject.SetActive(false);
        }

        public override void OnShow(object data)
        {
            this.RemoveEvents();
            this.Refresh();
            this.AddEvents();
        }

        public override void OnClose()
        {
            this.RemoveEvents();
        }

        private void AddEvents()
        {
            _inputMessage.onEndEdit.AddListener(OnEndEdit);
            _inputMessage.onPointerDown.AddListener(OnPointerDown);
            _btnConfirm.onClick.AddListener(OnClickConfirm);
            _btnCancel.onClick.AddListener(_closeAction);
            _hintBtn.onClick.AddListener(OnHintClick);
        }

        private void RemoveEvents()
        {
            _inputMessage.onEndEdit.RemoveListener(OnEndEdit);
            _inputMessage.onPointerDown.RemoveListener(OnPointerDown);
            _btnConfirm.onClick.RemoveListener(OnClickConfirm);
            _btnCancel.onClick.RemoveListener(_closeAction);
            _hintBtn.onClick.RemoveListener(OnHintClick);
        }

        private void OnHintClick()
        {
            _hintBtn.gameObject.SetActive(false);
            EventSystem.current.SetSelectedGameObject(_inputMessage.gameObject);
            _inputMessage.OnPointerClick(new PointerEventData(EventSystem.current));
        }

        private void Refresh()
        {
            _hintBtn.gameObject.SetActive(true);
            _txtTitle.CurrentText.text = _sendBoxData.title;
            _inputMessage.text = _sendBoxData.defaultInputMessage;
            _imgMask.gameObject.SetActive(_sendBoxData.isModal);
        }

        private void OnEndEdit(string content)
        {
            if (_inputMessage.text == string.Empty)
            {
                 _inputMessage.text = _sendBoxData.defaultInputMessage;
            }
        }

        private void OnPointerDown(KInputField input,PointerEventData data)
        {
            if(_inputMessage.text == _sendBoxData.defaultInputMessage)
            {
                _inputMessage.text = string.Empty;
            }
        }

        private void OnClickConfirm()
        {
            _sendBoxData.confirmAction(_inputMessage.text);
            ClosePanel();
        }


    }
}
