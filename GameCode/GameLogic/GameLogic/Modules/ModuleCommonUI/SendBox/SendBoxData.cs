﻿#region 模块信息
/*==========================================
// 文件名：SendBoxData
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.SendBox
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/9 10:12:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Events;

namespace ModuleCommonUI
{
    public class SendBoxData
    {
        public string title;
        public bool isModal;
        public string defaultInputMessage;

        public UnityAction<string> confirmAction;
        public UnityAction cancelAction;
    }
}
