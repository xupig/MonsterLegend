﻿#region 模块信息
/*==========================================
// 文件名：TokenView
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.Token
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/10 14:25:42
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCommonUI.Token
{
    public class TokenView:KContainer
    {
        private CategoryList _toggleGroup;
        //private PageableScrollPage _scrollPage;
        private KScrollPage _scrollPage;
        private KPageableList _list;
        private List<token_shop> _tokenList;
        private TokenCurrencyView _currencyView;        //private StateText _txtPageNum;

        private int _currentPage = 1;
        private int _totalPage = 1;

        protected override void Awake()
        {
            _toggleGroup = GetChildComponent<CategoryList>("List_categoryList");
            _currencyView = AddChildComponent<TokenCurrencyView>( "Container_currency");

            //_txtPageNum = GetChildComponent<StateText>("Label_txtYeshu");

            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_scrollpage");
            _list = _scrollPage.GetChildComponent<KPageableList>("mask/content");
            //_scrollPage.SetPageCapacity(8);
            _list.SetGap(0, 8);
            _list.SetPadding(0, 4, 0, 4);
            _list.SetDirection(KList.KListDirection.LeftToRight, 4, 1);
            base.Awake();
            TokenManager.Instance.GetTokenShopItemLimit();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        private void AddEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            _scrollPage.onCurrentPageChanged.AddListener(OnPageChange);
            EventDispatcher.AddEventListener(TokenEvents.INIT, OnTokenDataInit);
            EventDispatcher.AddEventListener<int>(TokenEvents.REFRESH_LIST, RefreshTokenShopItemList);
        }

        private void RemoveEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            _scrollPage.onCurrentPageChanged.RemoveListener(OnPageChange);
            EventDispatcher.RemoveEventListener(TokenEvents.INIT, OnTokenDataInit);
            EventDispatcher.RemoveEventListener<int>(TokenEvents.REFRESH_LIST, RefreshTokenShopItemList); 
        }

        private void RefreshTokenShopItemList(int itemId)
        {
            token_shop_item item = token_shop_helper.GetTokenShopItem(itemId);
            token_shop shop = token_shop_helper.GetTokenShop(item.__shop_id);
            if (shop!=null && shop.__sub_type == _toggleGroup.SelectedIndex + 1)
            {
                OnSelectedIndexChanged(_toggleGroup, _toggleGroup.SelectedIndex);
            }
        }

        private void OnTokenDataInit()
        {
            if (_tokenList != null)
            {
                OnSelectedIndexChanged(_toggleGroup, _toggleGroup.SelectedIndex);
            }
        }

        private void OnPageChange(KScrollPage scrollPage, int page)
        {
            _currentPage = page+1;
        }

        private void OnSelectedIndexChanged(CategoryList toggleGroup, int index)
        {
            List<token_shop_item> shopItemList = token_shop_helper.GetShopItemData(_tokenList[index].__id, PlayerAvatar.Player.vocation);
            List<TokenItemData> result = new List<TokenItemData>();
            for (int i = 0; i < shopItemList.Count; i++)
            {
                if (PlayerAvatar.Player.CheckLimit<string>(shopItemList[i].__rule) == LimitEnum.NO_LIMIT)
                {
                    result.Add(new TokenItemData(shopItemList[i]));
                }
            }
            // result.Sort(SortFunc);
            _currentPage = 1;

            _totalPage = Math.Max(1, Mathf.CeilToInt((float)result.Count / 4));
            SetDataList<TokenItem>(result);
            _scrollPage.TotalPage = _totalPage;
            _scrollPage.CurrentPage = 0;
           
            if(_tokenList[index].__show_source!=0)
            {
                _currencyView.SetGoldType(int.Parse(_tokenList[index].__token));
                _currencyView.SetTokenType(_tokenList[index].__show_source);
            }
            else
            {
                _currencyView.SetGoldType(public_config.MONEY_TYPE_GOLD);
                _currencyView.SetTokenType(int.Parse(_tokenList[index].__token));
            }
        }

        private int SortFunc(token_shop_item item1,token_shop_item item2)
        {
            bool isItem1Limit = item1.__good_type == 3 && PlayerDataManager.Instance.TokenData.GetTokenShopItemDailyCount(item1.__id) <= 0;
            bool isItem2Limit = item2.__good_type == 3 && PlayerDataManager.Instance.TokenData.GetTokenShopItemDailyCount(item2.__id) <= 0;
            if(isItem1Limit!=isItem2Limit)
            {
                if(isItem1Limit)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            return 0;
        }

        public void SetTokenType(TokenType type,int index)
        {
            _tokenList  = token_shop_helper.GetTokenShopData(type);
            List<CategoryItemData> tokenNameList = new List<CategoryItemData>();
            foreach (token_shop token in _tokenList)
            {
                tokenNameList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(token.__name)));
            }
            SetCategory(tokenNameList);
            _toggleGroup.SelectedIndex = index;
            OnSelectedIndexChanged(_toggleGroup, index);
            if (type == TokenType.TUTOR)
            {
                TokenManager.Instance.GetTokenShopToTalLimit();
            }
            AddEventListener();
        }

        private void SetCategory(List<CategoryItemData> dataList)
        {
            _toggleGroup.RemoveAll();
            _toggleGroup.SetDataList<CategoryListItem>(dataList);
        }

        private void SetDataList<T>(IList dataList) where T : TokenItem
        {
            _list.SetDataList<T>(dataList);
            
        }
    }
}
