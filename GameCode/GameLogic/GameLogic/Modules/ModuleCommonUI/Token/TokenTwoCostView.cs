﻿#region 模块信息
/*==========================================
// 文件名：TokenTwoCostView
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.Token
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/11 16:33:56
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ServerConfig;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCommonUI.Token
{
    public class TokenTwoCostView:KContainer
    {
        private List<TokenOneCostView> _viewList;
        protected override void Awake()
        {
            _viewList = new List<TokenOneCostView>();
            _viewList.Add(AddChildComponent<TokenOneCostView>("Container_gold"));
            _viewList.Add(AddChildComponent<TokenOneCostView>("Container_token"));
            base.Awake();
        }

        public void SetCost(Dictionary<string, string> costDict)
        {
            int i=0;
            foreach (KeyValuePair<string, string> kvp in costDict)
            {
                _viewList[i].SetCost(int.Parse(kvp.Key), int.Parse(kvp.Value));
                i++;
            }
        }

    }
}
