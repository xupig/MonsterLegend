﻿#region 模块信息
/*==========================================
// 文件名：TokenCurrencyView
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.Token
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/10 16:24:05
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using ModuleItemChannel;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using UnityEngine;

namespace ModuleCommonUI.Token
{
    public class TokenCurrencyView : KContainer
    {
        private MoneyItem _goldItem;
        private MoneyItem _tokenItem;

        private int _tokenId;
        private int _goldId;

        protected override void Awake()
        {
            _goldItem = AddChildComponent<MoneyItem>("Container_buyGold");
            _tokenItem = AddChildComponent<MoneyItem>("Container_buyDiamond");
            SetGoldType(public_config.MONEY_TYPE_GOLD);
            base.Awake();
        }

        private void OnGoldAddClick()
        {
            if(_goldId == public_config.MONEY_TYPE_DIAMOND)
            {
                if (function_helper.IsFunctionOpen(FunctionId.charge))
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.Charge);
                }
                else
                {
                    MessageBox.Show(true, string.Empty, "充值系统暂未开放，敬请期待！");
                }
            }
            else
            {
                ItemChannelWrapper wrapper = new ItemChannelWrapper();
                wrapper.type = 1;
                wrapper.id = _goldId;
                wrapper.context = PanelIdEnum.Token;
                UIManager.Instance.ShowPanel(PanelIdEnum.ItemChannel, wrapper);
            }
        }

        public void SetGoldType(int goldId)
        {
            _goldId = goldId;
            _goldItem.SetMoneyType(goldId);
            _goldItem.onAddClick = OnGoldAddClick;
        }

        public void SetTokenType(int tokenId)
        {
            _tokenId = tokenId;
            _tokenItem.SetMoneyType(tokenId);
            _tokenItem.onAddClick = OnTokenClick;
        }

        private void OnTokenClick()
        {
            if (_tokenId == public_config.MONEY_TYPE_DIAMOND)
            {
                if (function_helper.IsFunctionOpen(FunctionId.charge))
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.Charge);
                }
                else
                {
                    MessageBox.Show(true, string.Empty, "充值系统暂未开放，敬请期待！");
                }
            }
            else
            {
                ItemChannelWrapper wrapper = new ItemChannelWrapper();
                wrapper.type = 1;
                wrapper.id = _tokenId;
                wrapper.context = PanelIdEnum.Token;
                UIManager.Instance.ShowPanel(PanelIdEnum.ItemChannel, wrapper);
            }
            
        }

        protected sealed override void OnEnable()
        {
            AddEventListener();
            base.OnEnable();
        }

        protected sealed override void OnDisable()
        {
            RemoveEventListener();
            base.OnDisable();
        }

        private void AddEventListener()
        {

        }

        private void RemoveEventListener()
        {

        }

    }
}
