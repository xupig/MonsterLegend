﻿#region 模块信息
/*==========================================
// 文件名：TokenActiveType
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.Token
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/11 17:09:54
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCommonUI.Token
{
    public enum TokenActiveType
    {
        LEVEL = 1,
        CAREER = 2,
        VIP_LEVEL = 3,
        GUILD_CONTRIBUTE = 4,
    }
}
