﻿#region 模块信息
/*==========================================
// 文件名：TokenType
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.Token
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/10 14:45:18
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCommonUI.Token
{
    public enum TokenType
    {
        ERROR = 0,//错误
        //SHI_LIAN_MI_JING = 1,//试炼秘境
        //JUNIOR_DEVIL_GATE = 2,//初级恶魔之门
        //SENIOR_DEVIL_GATE = 3,//高级恶魔之门
        //FRIENDSHIP = 4,//友爱商店
        //YING_YONG = 5,//英勇商店
        RUNE_STONE = 6,//高级符文石商店
        GUILD_CONTRIBUTION = 7, //公会贡献商店
        TUTOR = 9,
    }
}
