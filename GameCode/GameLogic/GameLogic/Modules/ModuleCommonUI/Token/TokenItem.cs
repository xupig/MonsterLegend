﻿#region 模块信息
/*==========================================
// 文件名：TokenItem
// 命名空间: GameLogic.GameLogic.Common.ExtendComponent.Token
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/10 14:34:17
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Global;
using Common.Structs;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using Common.ExtendComponent;
using Common.Base;
using Game.UI;
using Common.Data;

namespace ModuleCommonUI.Token
{
    public class TokenItem : KList.KListItemBase
    {
        protected TokenItemData _itemData;

        protected static string costDescriptionTemplate;

        protected StateImage _quality;
        // private StateImage _imgQuality;
        protected StateText _txtNum;
        protected StateText _txtName;
        // private KContainer _iconContainer;
        protected ItemGrid _itemGrid;
        protected StateText _txtCostDescription;
        protected TokenTwoCostView _twoCostView;
        protected TokenOneCostView _oneCostView;
        protected StateImage _imgNotActived;
        protected KButton _btnBuy;
        protected RectTransform rect;
        protected Vector3 vect;
        protected ImageWrapper _imgLimitBuy;
        protected ImageWrapper _imgLimitBugBg;

        protected override void Awake()
        {
            _imgLimitBuy = GetChildComponent<ImageWrapper>("Container_biaoqian/Static_xiangou");
            _imgLimitBugBg = GetChildComponent<ImageWrapper>("Container_biaoqian/Static_hotBg");
            _txtNum = GetChildComponent<StateText>("Label_txtShuliang");
            _txtName = GetChildComponent<StateText>("Label_txtName");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _itemGrid.Context = PanelIdEnum.Token;
            _imgNotActived = GetChildComponent<StateImage>("Image_sharedweijihuo");
            rect = _imgNotActived.gameObject.GetComponent<RectTransform>();
            vect = rect.anchoredPosition3D;
            _quality = GetChildComponent<StateImage>("Container_kuang/Image_shangchengpinzhikuang02");
            MogoShaderUtils.ChangeShader(_quality.CurrentImage, MogoShaderUtils.IconShaderPath);
            _txtCostDescription = GetChildComponent<StateText>("Label_txtGongxianxiaohao");
            _twoCostView = AddChildComponent<TokenTwoCostView>("Container_jinbijiazhi");
            _oneCostView = AddChildComponent<TokenOneCostView>("Container_jiazhi");
            costDescriptionTemplate = _txtCostDescription.CurrentText.text;
            _btnBuy = GetChildComponent<KButton>("Button_goumai");
        }

        public override void Hide()
        {
            rect.anchoredPosition3D = vect;
            _btnBuy.onClick.RemoveListener(OnItemClick);
            //onClick.RemoveListener(OnClickHandler);
            EventDispatcher.RemoveEventListener<int>(TokenEvents.UPDATE, OnTokenLimitChange);
            EventDispatcher.RemoveEventListener(TokenEvents.REFRESH_TOTAL_COUNT, RefreshLeftCountView);
            base.Hide();
        }

        public override void Show()
        {
            rect.anchoredPosition3D = vect + new Vector3(-20, 0, 0);
            //onClick.AddListener(OnClickHandler);
            _btnBuy.onClick.AddListener(OnItemClick);
            EventDispatcher.AddEventListener<int>(TokenEvents.UPDATE, OnTokenLimitChange);
            EventDispatcher.AddEventListener(TokenEvents.REFRESH_TOTAL_COUNT, RefreshLeftCountView);
            base.Show();
        }

        //private void OnClickHandler(KList.KListItemBase item,int index)
        //{
        //    if (_itemData!=null)
        //    {
        //        ToolTipsManager.Instance.ShowItemTip(_itemData.__item_id, PanelIdEnum.Token, false);
        //    }
        //}

        protected virtual void OnTokenLimitChange(int itemId)
        {
            if (_itemData != null && gameObject.activeSelf && _itemData.Id == itemId)
            {
                Refresh();
            }
        }

        protected virtual void OnItemClick()
        {
            if (_txtCostDescription.gameObject.activeSelf == false)
            {
                if (_itemData.ShowLeftCount > 0)
                {
                    UIManager.Instance.ShowPanel(Common.Base.PanelIdEnum.QuickBuy, _itemData);
                }
            }
        }


        public override object Data
        {
            get
            {
                return _itemData;
            }
            set
            {
                _itemData = value as TokenItemData;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            Refresh();
        }

        protected virtual void Refresh()
        {
            if (_itemData == null)
            {
                return;
            }
            // _imgQuality.CurrentImage.solidFillColor = item_helper.GetColor(_itemData.__item_id);
            RefreshItemView();
            RefreshLeftCountView();
            if (HasActived())
            {
                RefreshCostView();
                RefreshActiveView();
            }
            else
            {
                SetNotActive();
            }
            RefreshGoodTypeView();
        }

        protected void RefreshGoodTypeView()
        {
            if (_itemData.GoodType == 3)
            {
                _imgLimitBuy.gameObject.SetActive(true);
                _imgLimitBugBg.gameObject.SetActive(true);
                if (_itemData.ShowLeftCount <= 0)
                {
                    _btnBuy.Label.ChangeAllStateText((56712).ToLanguage()); //已购买
                }
                else
                {
                    _btnBuy.Label.ChangeAllStateText((60037).ToLanguage()); //购买
                }
            }
            else
            {
                _imgLimitBuy.gameObject.SetActive(false);
                _imgLimitBugBg.gameObject.SetActive(false);
                _btnBuy.Label.ChangeAllStateText((60037).ToLanguage()); //购买
            }
        }

        public override void Dispose()
        {
            _itemData = null;
        }

        protected void RefreshCostView()
        {
            Dictionary<string, string> costDict = _itemData.Cost;
            if (costDict.Count == 1)
            {
                SetOneCost(costDict);
            }
            else
            {
                SetTwoCost(costDict);
            }
        }

        protected void RefreshActiveView()
        {
            _imgNotActived.gameObject.SetActive(false);
            _txtCostDescription.gameObject.SetActive(false);
        }

        private void RefreshLeftCountView()
        {
            if (HasActived())
            {
                if (_itemData.ShowLeftCount > 0)
                {
                    _btnBuy.SetButtonActive();
                }
                else
                {
                    _btnBuy.SetButtonDisable();
                }
                _txtNum.CurrentText.text = (60038).ToLanguage(_itemData.ShowLeftCount);
                _txtNum.Visible = _itemData.ShowLeftCount != int.MaxValue;
            }
            else
            {
                _btnBuy.SetButtonDisable();
                _txtNum.gameObject.SetActive(false);
            }
        }

        private int runeId;
        protected void RefreshItemView()
        {
            runeId = _itemData.ItemId;
            _txtName.CurrentText.text = item_helper.GetName(runeId);
            _itemGrid.SetItemData(runeId);
            if (item_helper.GetItemType(runeId) == BagItemType.Rune)
            {
                _itemGrid.onClick = ShowRuneTip;
            }
            else
            {
                _itemGrid.onClick = null;
            }
            _quality.CurrentImage.color = item_helper.GetColor(runeId);
        }

        private void ShowRuneTip()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.RuneToolTips, runeId);
        }

        protected void SetNotActive()
        {
            _oneCostView.gameObject.SetActive(false);
            _twoCostView.gameObject.SetActive(false);
            _imgNotActived.gameObject.SetActive(true);
            _txtCostDescription.gameObject.SetActive(true);
        }

        protected void SetOneCost(Dictionary<string, string> costDict)
        {
            _oneCostView.gameObject.SetActive(true);
            _twoCostView.gameObject.SetActive(false);
            foreach (KeyValuePair<string, string> kvp in costDict)
            {
                _oneCostView.SetCost(int.Parse(kvp.Key), int.Parse(kvp.Value));
            }
        }

        protected void SetTwoCost(Dictionary<string, string> costDict)
        {
            _oneCostView.gameObject.SetActive(false);
            _twoCostView.gameObject.SetActive(true);
            _twoCostView.SetCost(costDict);
        }

        protected bool HasActived()
        {
            Dictionary<string, string> activeDict = _itemData.Active;
            LimitEnum limit = PlayerAvatar.Player.CheckLimit<string>(activeDict);
            switch (limit)
            {
                case LimitEnum.NO_LIMIT:
                    return true;
                case LimitEnum.LIMIT_LEVEL:
                    _txtCostDescription.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.TOKEN_LEVEL_ACTIVE, activeDict[LimitEnum.LIMIT_LEVEL.ToIntString()]);
                    break;
                case LimitEnum.LIMIT_VIP_LEVEL:
                    _txtCostDescription.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.TOKEN_VIP_ACTIVE, activeDict[LimitEnum.LIMIT_VIP_LEVEL.ToIntString()]);
                    break;
                case LimitEnum.LIMIT_VOCATION:
                    _txtCostDescription.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.TOKEN_VOCATION_ACTIVE, int.Parse(activeDict[LimitEnum.LIMIT_VOCATION.ToIntString()]).ToLanguage());
                    break;
                case LimitEnum.LIMIT_TRY_FB:
                    int fb = int.Parse(activeDict[LimitEnum.LIMIT_TRY_FB.ToIntString()]);
                    _txtCostDescription.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.TOKEN_FB_ACTIVE, instance_helper.GetInstanceName(fb));
                    break;
                case LimitEnum.LIMIT_GUILD_CONTRIBUTE:
                    _txtCostDescription.CurrentText.text = MogoLanguageUtil.GetString(LangEnum.TOKEN_CONTRIBUTE_ACTIVE, activeDict[LimitEnum.LIMIT_GUILD_CONTRIBUTE.ToIntString()]);
                    break;
                case LimitEnum.LIMIT_TUTOR_LEVEL:
                    _txtCostDescription.CurrentText.text = MogoLanguageUtil.GetContent(120135, activeDict[LimitEnum.LIMIT_TUTOR_LEVEL.ToIntString()]);
                    break;
            }
            return false;
        }

    }
}
