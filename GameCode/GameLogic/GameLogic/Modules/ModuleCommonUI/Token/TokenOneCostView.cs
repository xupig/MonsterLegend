﻿#region 模块信息
/*==========================================
// 文件名：TokenOneCostView
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.Token
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/11 16:34:26
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCommonUI.Token
{
    public class TokenOneCostView:KContainer
    {
        private ItemGrid _iconContainer;
        private StateText _txtNum;
        protected override void Awake()
        {
            _txtNum = GetChildComponent<StateText>("Label_txtNum");
            _iconContainer = GetChildComponent<ItemGrid>("Container_wupin");
            base.Awake();
        }

        public void SetCost(int id,int num)
        {
            _txtNum.CurrentText.text= num.ToString();
            _iconContainer.SetItemData(id);
           // MogoAtlasUtils.AddIcon(_iconContainer.gameObject, item_helper.GetIcon(id));
        }
    }
}
