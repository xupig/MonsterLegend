﻿using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCommonUI.Token
{
    public class TokenItemData
    {
        private token_shop_item _baseConfig;

        public TokenItemData(token_shop_item cfg)
        {
            _baseConfig = cfg;
        }

        public int Id
        {
            get { return _baseConfig.__id; }
        }

        public int ItemId
        {
            get { return _baseConfig.__item_id; }
        }

        public Dictionary<string, string> Active
        {
            get { return _baseConfig.__active; }
        }

        public Dictionary<string, string> Cost
        {
            get { return _baseConfig.__cost; }
        }

        public TokenType BelongTokenType
        {
            get { return GetTokenType(); }
        }

        private TokenType GetTokenType()
        {
            token_shop shop = token_shop_helper.GetTokenShop(_baseConfig.__shop_id);
            if (shop != null)
            {
                return (TokenType)shop.__type;
            }
            return TokenType.ERROR;
        }

        public int GoodType
        {
            get { return _baseConfig.__good_type; }
        }

        /// <summary>
        /// 个人每日限购
        /// </summary>
        public int DailyLeftCount
        {
            get { return PlayerDataManager.Instance.TokenData.GetTokenShopItemDailyCount(Id); }
        }

        /// <summary>
        /// 全服每日限购
        /// </summary>
        public int TotalLeftCount
        {
            get
            {
                if (BelongTokenType == TokenType.TUTOR)
                {
                    return PlayerDataManager.Instance.TokenData.GetTokenShopItemTotalCount(Id);
                }
                return int.MaxValue;
            }
        }

        /// <summary>
        /// 代币商店显示的剩余量
        /// </summary>
        public int ShowLeftCount
        {
            get
            {
                if (BelongTokenType == TokenType.TUTOR)
                {
                    return TotalLeftCount;
                }
                else
                {
                    return DailyLeftCount;
                }
            }
        }
    }
}
