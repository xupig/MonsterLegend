﻿using Common.Base;
using Game.UI.UIComponent;
#region 模块信息
/*==========================================
// 文件名：BuffTips
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.BuffTips
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/4/7 16:24:54
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace ModuleCommonUI
{
    public class BuffTips : BasePanel
    {
        private StateText _content;
        private KDummyButton _backBtn;

        protected override PanelIdEnum Id
        {
            get
            {
                return PanelIdEnum.BuffTips;
            }
        }

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("Container_Bg/ScaleImage_sharedZhezhao");
            ModalMask.Alpha = 0;
            _backBtn = ModalMask.gameObject.AddComponent<KDummyButton>();
            _backBtn.onClick.AddListener(ClosePanel);
            CloseBtn = GetChildComponent<KButton>("Container_Bg/Button_close");
            _content = GetChildComponent<StateText>("Container_Content/Label_txtContent");
        }

        public override void OnShow(object data)
        {
            _content.CurrentText.text = data as string;
        }

        public override void OnClose()
        {

        }
    }

}
