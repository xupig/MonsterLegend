﻿#region 模块信息
/*==========================================
// 文件名：ItemComposePanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.ItemCompose
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/4/12 9:15:43
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCommonUI
{
    public class ItemComposePanel:BasePanel
    {
        private StateText _txtTitle;
        private StateText _txtNeedMaterial;
        private StateText _txtComposeMaterial;
        private StateText _txtInsufficient;
        private StateText _txtNeedNum;
        private StateText _txtHasNum;
        private StateText _txtComposeNum;
        private StateText _txtLevelLimit;
        private KButton _btnCompose;
        private ItemGrid _needItem;
        private ItemGrid _composeItem;

        private int needItemId;
        private int needItemNum;
        private int composeId;
        private ItemToolTipsData _itemData;
        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _txtTitle = GetChildComponent<StateText>("Label_txtTitle");
            _txtTitle.CurrentText.text = (126000).ToLanguage();
            _txtNeedMaterial = GetChildComponent<StateText>("Container_left/Label_txtXuyao");
            _txtNeedMaterial.CurrentText.text = (126001).ToLanguage();
            _txtComposeMaterial = GetChildComponent<StateText>("Container_right/Label_txtXuyao");
            _txtComposeMaterial.Visible = false;
            _txtInsufficient = GetChildComponent<StateText>("Container_left/Label_txtBuzu");
            _txtInsufficient.CurrentText.text = (126002).ToLanguage();
            _txtNeedNum = GetChildComponent<StateText>("Container_left/Label_txtXuyaoshuliang");
            _txtHasNum = GetChildComponent<StateText>("Container_left/Label_txtYiyoushuliang");
            _txtComposeNum = GetChildComponent<StateText>("Container_right/Label_txtHechengshuliang");
            
            _txtLevelLimit = GetChildComponent<StateText>("Label_txtTiaojian");
            _btnCompose = GetChildComponent<KButton>("Button_hecheng");
            _needItem = GetChildComponent<ItemGrid>("Container_left/Container_wupin");
            _needItem.IsClickable = false;
            _composeItem = GetChildComponent<ItemGrid>("Container_right/Container_wupin");
            _composeItem.IsClickable = false;
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.ItemCompose; }
        }

        public override void OnShow(object data)
        {
            _itemData = data as ItemToolTipsData;
            UpdateContent();
            _btnCompose.onClick.AddListener(OnClickCompose);
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, Refresh);
        }

        private void Refresh(BagType type,uint pos)
        {
            if(type == BagType.ItemBag && _itemData!=null && _itemData.GridPosition == Convert.ToInt32(pos))
            {
                UpdateContent();
                //UIManager.Instance.EnablePanelCanvas(PanelIdEnum.ItemCompose);
            }
        }

        private void UpdateContent()
        {
            item_commom item = item_helper.GetItemConfig(_itemData.Id);
            foreach (KeyValuePair<string, string> kvp in item.__use_cost)
            {
                needItemId = int.Parse(kvp.Key);
                needItemNum = int.Parse(kvp.Value) + 1;
            }
            foreach (KeyValuePair<string, string> kvp in item.__use_effect)
            {
                composeId = int.Parse(kvp.Value);
            }
            BaseItemInfo itemInfo = PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemInfo(_itemData.GridPosition);
            int currentNum = 0;
            if(itemInfo!=null)
            {
                currentNum = itemInfo.StackCount;
            }
            if (currentNum >= needItemNum)
            {
                _txtNeedNum.CurrentText.text = (126004).ToLanguage(needItemNum);
                _txtInsufficient.Visible = false;
            }
            else
            {
                _txtNeedNum.CurrentText.text = (126005).ToLanguage(needItemNum);
                _txtInsufficient.Visible = true;
            }
            int levelLimit = 0;
            if (item.__use_limit.ContainsKey(public_config.LIMIT_LEVEL.ToString()))
            {
                levelLimit = int.Parse(item.__use_limit[public_config.LIMIT_LEVEL.ToString()]);
            }
            if (PlayerAvatar.Player.level >= levelLimit)
            {
                _btnCompose.Visible = true;
                _txtLevelLimit.Visible = false;
            }
            else
            {
                _btnCompose.Visible = false;
                _txtLevelLimit.Visible = true;
                _txtLevelLimit.CurrentText.text = (126007).ToLanguage(levelLimit);
            }
            _txtComposeNum.CurrentText.text = (126003).ToLanguage(1);
            _txtHasNum.CurrentText.text = (126006).ToLanguage(currentNum);
            _needItem.SetItemData(needItemId);
            _composeItem.SetItemData(composeId);
        }


        public override void OnClose()
        {
            _btnCompose.onClick.RemoveListener(OnClickCompose);
            EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, Refresh);
        }

        private void OnClickCompose()
        {
            BagManager.Instance.RequestUseItem(_itemData.GridPosition, 1);
            //UIManager.Instance.DisablePanelCanvas(PanelIdEnum.ItemCompose);
        }
    }
}
