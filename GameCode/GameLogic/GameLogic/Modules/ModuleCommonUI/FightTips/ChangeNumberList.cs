﻿#region 模块信息
/*==========================================
// 文件名：ChangeNumberList
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.FightTips
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/10/21 10:31:38
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using Game.UI.UIComponent;
using System.Collections.Generic;
using UnityEngine;
namespace ModuleCommonUI.FightTips
{
    public class ChangeNumberList : KContainer
    {
        private KContainer _numberContainer;
        private List<StateImage> _changeNumberTemplate;
        private List<StateImage> _showList;

        private static FightNumberType type;

        protected override void Awake()
        {
            _changeNumberTemplate = new List<StateImage>();
            _showList = new List<StateImage>();
            _numberContainer = GetChildComponent<KContainer>("Container_template");
            for (int i = 0; i <= 9; i++)
            {
                StateImage numberImage = _numberContainer.GetChildComponent<StateImage>("Image_bianhuashuzi0" + i);
                _changeNumberTemplate.Add(numberImage);
            }
            _numberContainer.Visible = false;
        }

        public void SetChangeNumber(int number)
        {
            DestroyPreviousNumbers();
            type = number > 0 ? FightNumberType.UP : FightNumberType.DOWN;
            List<int> digitsList = GetDigitsInNumber(Mathf.Abs(number));
            for (int i = 0; i < digitsList.Count; i++)
            {
                int digit = digitsList[i];
                StateImage image = GameObject.Instantiate(_changeNumberTemplate[digit]) as StateImage;
                image.transform.SetParent(transform);
                _showList.Add(image);
            }
            SetNumberImageColor();
            AdjustNumberPosition();
        }

        private void DestroyPreviousNumbers()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                if (string.Equals(transform.GetChild(i).name, "Container_template") == false)
                {
                    GameObject.Destroy(transform.GetChild(i).gameObject);
                }
            }
            _showList.Clear();
        }

        private void AdjustNumberPosition()
        {
            Vector2 markPosition = Vector2.zero;
            for (int i = 0; i < _showList.Count; i++)
            {
                RectTransform rectTran = _showList[i].GetComponent<RectTransform>();
                rectTran.anchoredPosition3D = markPosition;
                markPosition += new Vector2(rectTran.rect.width, 0);
                _showList[i].transform.localScale = new Vector3(1, 1, 1);
            }
        }

        private void SetNumberImageColor()
        {
            float r, g, b;
            for (int i = 0; i < _showList.Count; i++)
            {
                if (_showList[i].gameObject.activeSelf == true)
                {
                    MogoShaderUtils.ChangeShader(_showList[i].CurrentImage, MogoShaderUtils.IconShaderPath);
                    if (type == FightNumberType.UP)
                    {
                        r = 255 / 255.0f;
                        g = 150 / 255.0f;
                        b = 0 / 255.0f;
                        _showList[i].CurrentImage.color = new Color(r, g, b);
                    }
                    else
                    {
                        r = 180 / 255.0f;
                        g = 73 / 255.0f;
                        b = 255 / 255.0f;
                        _showList[i].CurrentImage.color = new Color(r, g, b);
                    }
                }
            }
        }

        private List<int> GetDigitsInNumber(int number)
        {
            List<int> result = new List<int>();
            while (number > 0)
            {
                result.Add(number % 10);
                number /= 10;
            }
            result.Reverse();
            return result;
        }
    }
}