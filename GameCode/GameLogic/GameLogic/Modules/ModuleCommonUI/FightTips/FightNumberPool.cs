﻿#region 模块信息
/*==========================================
// 文件名：FightNumberPool
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.FightTips
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/10/21 10:25:24
// 描述说明：
// 其他：
//==========================================*/
#endregion

using ModuleCommonUI;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleCommonUI.FightTips
{
    public class FightNumberPool
    {
        private static FightNumberPool _instance;
        private Dictionary<int, Stack<GameObject>> numberObjectPool;

        public FightNumberPool()
        {
            if (_instance != null)
            {
                return;
            }
            numberObjectPool = new Dictionary<int, Stack<GameObject>>();
            _instance = this;
        }

        public static FightNumberPool Instance
        {
            get
            {
                if (_instance == null)
                {
                    new FightNumberPool();
                }
                return _instance;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="number"></param>
        /// <param name="go"></param>
        /// <param name="type">表示类型:UP / Down</param>
        /// 如果是上升的数字，索引在10-19之间
        /// 如果是下降的数字，索引在20-29之间
        public void AddNumberGameObject(FightNumberType type, int number, GameObject go)
        {
            int index = (int)type * 10 + number;
            if (numberObjectPool.ContainsKey(index) == false)
            {
                numberObjectPool.Add(index, new Stack<GameObject>());
            }
            if (go != null)
            {
                go.SetActive(false);
                numberObjectPool[index].Push(go);
            }
        }

        public GameObject GetNumberGameObject(FightNumberType type, int number)
        {
            int index = (int)type * 10 + number;
            Stack<GameObject> numberGoStack = numberObjectPool[index];
            if (numberGoStack.Count > 1)
            {
                return numberGoStack.Pop();
            }
            else
            {
                return GameObject.Instantiate(numberGoStack.Peek()) as GameObject;
            }
        }
    }
}