﻿#region 模块信息
/*==========================================
// 文件名：FightTips
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.FightTips
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/5/20 16:52:24
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using System.Collections.Generic;
using UnityEngine;
using GameMain.GlobalManager;

namespace ModuleCommonUI.FightTips
{
    public enum FightNumberType
    {
        UP = 1,
        DOWN = 2,
    }

    public class FightForceNumData
    {
        public int number;
        public FightNumberType type;

        public FightForceNumData(FightNumberType type, int number)
        {
            this.type = type;
            this.number = number;
        }
    }

    public class NumberList : KContainer
    {
        protected const int NUMBER_COUNT = 9;
        protected KContainer _templateContainer;
        protected List<GameObject> _template = new List<GameObject>();
        protected Dictionary<int, Stack<GameObject>> digitsPool;
        protected List<int> digits;
        private GameObject numContainer;
        private GameObject numPool;
        protected string prefix;

        protected override void Awake()
        {
            CreateNumberContainer();
            _templateContainer = GetChildComponent<KContainer>("Container_template");
            for (int i = 0; i <= NUMBER_COUNT; i++)
            {
                _template.Add(_templateContainer.GetChild(prefix + i).gameObject);
            }
            _templateContainer.Visible = false;
            gameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        }

        protected void CreateNumberContainer()
        {
            numContainer = new GameObject("numbersContainer");
            numContainer.AddComponent<RectTransform>();
            numContainer.AddComponent<KContainer>();
            numContainer.transform.SetParent(this.transform);
            numContainer.transform.localScale = Vector3.one;
            numContainer.GetComponent<RectTransform>().anchoredPosition = Vector2.one;
        }

        protected void CreateNumberPools(string poolName)
        {
            numPool = new GameObject(poolName);
            numPool.AddComponent<RectTransform>();
            numPool.transform.SetParent(this.transform.parent);
            numPool.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        }

        public void SetNumber(int number)
        {
            Visible = true;
            digits = GetDigits(Mathf.Abs(number));
            PutOldNumbersInPool();
            for (int i = 0; i < digits.Count; i++)
            {
                GameObject numGo = GetStateImage(digits[i]);
                numGo.transform.SetParent(numContainer.transform);
                numGo.transform.localScale = Vector3.one;
                numGo.transform.SetAsLastSibling();
            }
            AdjustPosition();
            for (int i = 0; i < numContainer.transform.childCount; i++)
            {
                numContainer.transform.GetChild(i).gameObject.SetActive(true);
            }
        }

        private void AdjustPosition()
        {
            Vector3 beginPosition = Vector3.zero;
            const int standardWidth = 40;
            const int standardHeight = 45;
            for (int i = 0; i < numContainer.transform.childCount; i++)
            {
                Transform goTran = numContainer.transform.GetChild(i);
                RectTransform rectTran = goTran.GetComponent<RectTransform>();
                rectTran.anchoredPosition3D = beginPosition + new Vector3((standardWidth - rectTran.rect.width) * 0.5f, 0, 0);
                beginPosition += new Vector3(standardWidth, 0, 0);
            }
            RectTransform parentTran = transform.parent.GetComponent<RectTransform>();
            RectTransform selfTran = numContainer.transform.GetComponent<RectTransform>();
            selfTran.sizeDelta = new Vector2(standardWidth * numContainer.transform.childCount, standardHeight);
            selfTran.pivot = new Vector2(0, 1);
            selfTran.anchorMin = new Vector2(0, 1);
            selfTran.anchorMax = new Vector2(0, 1);
            float selfWidth = selfTran.rect.width;
            float selfHeight = selfTran.rect.height;
            float parentWidth = parentTran.rect.width;
            float parentHeight = parentTran.rect.height;
            selfTran.anchoredPosition3D = new Vector3((parentWidth - selfWidth) * 0.5f, -(parentHeight - selfHeight) * 0.65f, -1);
        }

        private GameObject GetStateImage(int digit)
        {
            for (int i = 0; i < numPool.transform.childCount; i++)
            {
                GameObject imageGo = numPool.transform.GetChild(i).gameObject;
                if (imageGo.name.Contains(prefix + digit) == true)
                {
                    return imageGo;
                }
            }
            return GameObject.Instantiate(_template[digit]) as GameObject;
        }

        private void PutOldNumbersInPool()
        {
            for (int i = numContainer.transform.childCount - 1; i >= 0; i--)
            {
                numContainer.transform.GetChild(i).gameObject.SetActive(false);
                numContainer.transform.GetChild(i).SetParent(numPool.transform);
            }
        }

        private List<int> GetDigits(int number)
        {
            List<int> result = new List<int>();
            while (number > 0)
            {
                result.Add(number % 10);
                number /= 10;
            }
            result.Reverse();
            return result;
        }
    }

    public class UpNumberContainer : NumberList
    {
        protected override void Awake()
        {
            prefix = "Static_sharedlianji";
            CreateNumberPools("UpNumberPools");
            base.Awake();
        }
    }

    public class DownNumberContainer : NumberList
    {
        protected override void Awake()
        {
            prefix = "Image_sharedbaojilan";
            CreateNumberPools("DownNumberPools");
            base.Awake();
        }
    }

    public class FightChangeView:KContainer
    {
       
        private int old_FightValue;
        private int change;

        private KContainer _upImageContainer;
        private KContainer _downImageContainer;
        private UpNumberContainer _fightForceUpContainer;
        private DownNumberContainer _fightForceDownContainer;

        private ChangeNumberList _changeNumberList;

       

        private int startFightForce;
        private int endFightForce;

        protected override void Awake()
        {
            _upImageContainer = GetChildComponent<KContainer>("Container_zhandouli/Container_shangsheng");
            _downImageContainer = GetChildComponent<KContainer>("Container_zhandouli/Container_xiajiang");
            _fightForceUpContainer = AddChildComponent<UpNumberContainer>("Container_shangshengshuzi");
            _fightForceDownContainer = AddChildComponent<DownNumberContainer>("Container_xiajiangshuzi");
            _changeNumberList = AddChildComponent<ChangeNumberList>("Container_bianhuashuzi");
            
        }

        public void OnShow(object data)
        {
            if (data is int)
            {
                UIManager.Instance.PlayAudio("Sound/UI/surprise_original_11_combat_up.mp3");
                RemoveTimerHeap();
                old_FightValue = (int)data;
                change = (int)PlayerAvatar.Player.fight_force - (int)old_FightValue;
                Refresh(change);
            }
            else if (data is List<int>)
            {
                int start = (data as List<int>)[0];
                int end = (data as List<int>)[1];
                change = end - start;
                Refresh(change);
            }

        }

        private uint numberTimeId = 0;
        private void RemoveTimerHeap()
        {
            if (numberTimeId != 0)
            {
                TimerHeap.DelTimer(numberTimeId);
                numberTimeId = 0;
            }
        }

       

        private void Refresh(int change)
        {
            ShowChange(change);
            _upImageContainer.Visible = (change > 0);
            if(_downImageContainer!=null)
            {
                _downImageContainer.Visible = (change < 0);
            }
           
            
            ShowFightForce((int)PlayerAvatar.Player.fight_force, (int)old_FightValue);
           
        }

        private void ShowChange(int change)
        {
            _changeNumberList.SetChangeNumber(change);
        }

        private int step = 0;
        private void ShowFightForce(int nowFightForce, int oldFightForce)
        {
            RemoveTimerHeap();
            startFightForce = oldFightForce;
            endFightForce = nowFightForce;
            int changeFightForce = endFightForce - startFightForce;
            if (changeFightForce < 0 && changeFightForce > -20)
            {
                step = -1;
            }
            else if (changeFightForce > 0 && changeFightForce < 20)
            {
                step = 1;
            }
            else
            {
                step = Mathf.RoundToInt((endFightForce - startFightForce) * 0.05f);
            }
            numberTimeId = TimerHeap.AddTimer(200, 50, ChangeFightForce);
        }

        private void ChangeFightForce()
        {
            if (Mathf.Abs(endFightForce - startFightForce) <= Mathf.Abs(step + 6))
            {
                startFightForce = endFightForce;
                if (step > 0)
                {
                    _fightForceUpContainer.SetNumber(startFightForce);
                    _fightForceDownContainer.Visible = false;
                }
                else
                {
                    _fightForceUpContainer.Visible = false;
                    _fightForceDownContainer.SetNumber(startFightForce);
                }
            }
            if (startFightForce != endFightForce)
            {
                if (Mathf.Abs(step) > 3)
                {
                    step += Random.Range(-2, 2);
                }
                startFightForce += step;
                if (step > 0)
                {
                    _fightForceUpContainer.SetNumber(startFightForce);
                    _fightForceDownContainer.Visible = false;
                }
                else
                {
                    _fightForceUpContainer.Visible = false;
                    _fightForceDownContainer.SetNumber(startFightForce);
                }
            }
            else
            {
                RemoveTimerHeap();
            }
        }

        public void OnClose()
        {
            RemoveTimerHeap();
           
           
        }
    }

    public class FightTips : BasePanel
    {
        private Vector3 startPosition = new Vector3(372.0f, -380.0f);
        private Vector3 endPosition = new Vector3(372.0f, -200.0f);
        private KContainer _container;
        private FightChangeView _changeView;
        private const uint STAY_DURATION = 1500;
        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.FightTips; }
        }

        protected override void Awake()
        {
            this.transform.localPosition += new Vector3(0, 0, 10);
            _changeView = AddChildComponent<FightChangeView>("Container_zhandoulitishi");
            _container = GetChildComponent<KContainer>("Container_zhandoulitishi");
            _container.GetComponent<RectTransform>().anchoredPosition = startPosition;
        }

        public override void OnShow(object data)
        {
            SetData(data);
        }

        public override void SetData(object data)
        {
            if (data is int)
            {
                RemoveShowTimerId();
            }
            _changeView.OnShow(data);
            TweenPosition tween = TweenPosition.Begin(_container.gameObject, 0.2f, endPosition, UITweener.Method.BounceIn);
            showTimeId = TimerHeap.AddTimer(STAY_DURATION, 0, ClosePanel);
        }

        private uint showTimeId = 0;
        private void RemoveShowTimerId()
        {
            if (showTimeId != 0)
            {
                TimerHeap.DelTimer(showTimeId);
                showTimeId = 0;
            }
        }

        public override void OnClose()
        {
            _changeView.OnClose();
            RemoveShowTimerId();
            _container.GetComponent<RectTransform>().anchoredPosition = startPosition;
        }
    }
}