﻿#region 模块信息
/*==========================================
// 文件名：EquipFightForceTips
// 命名空间: GameLogic.GameLogic.Modules.ModuleCommonUI.FightTips
// 创建者：巩靖
// 修改者列表：
// 创建日期：2016/4/13 17:21:37
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using ModuleCommonUI.FightTips;
using ModuleRole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCommonUI
{
    public class EquipFightForceTips:BasePanel
    {
        public static bool isShow = false;
        private RoleEquipView _equipView;
        private FightChangeView _changeView;
        private KDummyButton _btnClose;
        private Vector3 startPosition = new Vector3(710, -380.0f);
        private Vector3 endPosition = new Vector3(710, -200.0f);
        private KContainer _container;
        private const uint STAY_DURATION = 1500;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _btnClose = AddChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
            _changeView = AddChildComponent<FightChangeView>("Container_zhandoulitishi");
            _changeView.gameObject.AddComponent<RaycastComponent>().RayCast = false;
            _equipView = AddChildComponent<RoleEquipView>("Container_equipView");
            _container = GetChildComponent<KContainer>("Container_zhandoulitishi");
            _container.GetComponent<RectTransform>().anchoredPosition = startPosition;
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.EquipFightForceTips; }
        }

        private void AddEventListener()
        {
            _btnClose.onClick.AddListener(OnClickCloseBtn);
        }

        private void RemoveEventListener()
        {
            _btnClose.onClick.AddListener(OnClickCloseBtn);
        }

        private void OnClickCloseBtn()
        {
            ClosePanel();
        }

        public override void OnShow(object data)
        {
            isShow = true;
            SetData(data);
            AddEventListener();
        }

        public override void SetData(object data)
        {
            base.SetData(data);
            int[] dataList = data as int[];
            _changeView.OnShow(dataList[1]);
            _equipView.ShowEquipParticle(dataList[0]);
            TweenPosition tween = TweenPosition.Begin(_container.gameObject, 0.2f, endPosition, UITweener.Method.BounceIn);
        }

        public override void OnClose()
        {
            isShow = false;
            RemoveEventListener();
            _container.GetComponent<RectTransform>().anchoredPosition = startPosition;
        }

    }
}
