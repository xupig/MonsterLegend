﻿#region 模块信息
/*==========================================
// 模块名：CommonUIModule
// 命名空间: ModuleCommonUI
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/04
// 描述说明：通用UI模块
// 其他：
//==========================================*/
#endregion



using Common.Base;


namespace ModuleCommonUI
{
    public class CommonUIModule : BaseModule
    {
        public CommonUIModule() 
        {

        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init( )
        {
            AddPanel(PanelIdEnum.MessageBox, "Container_MessageBox", MogoUILayer.LayerAlert, "ModuleCommonUI.MessageBox", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.UILoadingBar, "Container_UILoadingPanel", MogoUILayer.LayerUITop, "ModuleCommonUI.UILoadingBar", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.SystemAlert, "Container_MessageBox", MogoUILayer.LayerUITop, "ModuleCommonUI.SystemAlert", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.ExpRuneToolTips, "Container_ExpRuneToolTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.ExpRuneToolTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.ItemStory, "Container_ItemStoryPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.ItemStoryPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.ItemToolTips, "Container_ItemToolTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.ItemToolTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.GemItemToolTips, "Container_ItemToolTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.GemItemToolTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.TokenItemToolTips, "Container_ItemToolTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.TokenItemToolTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.SuitScrollToolTips, "Container_ItemToolTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.SuitScrollItemToolTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.EnchantScrollToolTips, "Container_ItemToolTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.EnchantScrollItemToolTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.WingToolTips, "Container_WingToolTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.WingToolTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.EquipToolTips, "Container_EquipToolTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.EquipToolTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.SellItemToolTips, "Container_ItemSellTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.SellItemToolTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.UseItemToolTips, "Container_ItemUseTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.UseItemToolTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.RuneToolTips, "Container_RuneToolTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.RuneToolTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.Spotlight, "Container_SpotlightPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.SpotlightPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);
            AddPanel(PanelIdEnum.FloatTips, "Container_FloatTips", MogoUILayer.LayerAlert, "ModuleCommonUI.FloatTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.SendBox, "Container_SendBoxPanel", MogoUILayer.LayerAlert, "ModuleCommonUI.SendBox", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.ItemObtain, "Container_ItemObtainPanel", MogoUILayer.LayerUIPanel, "ModuleCommonUI.ItemObtainPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.FightTips, "Container_FightForceEffect", MogoUILayer.LayerEffect, "ModuleCommonUI.FightTips.FightTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.SkillToolTips, "Container_SkillToolTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.SkillToolTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.RelivePop, "Container_RelivePopPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.RelivePopPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.PortalPanel, "Container_PortalPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.PortalPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.SelectList, "Container_PortalPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.SelectListPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.CommonTips, "Container_CommonTips", MogoUILayer.LayerAlert, "ModuleCommonUI.CommonTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.RuleTips, "Container_RuleTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.RuleTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.ChangeScene, "Container_ChangeScenePanel", MogoUILayer.LayerUIPanel, "ModuleCommonUI.ChangeScenePanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.CommonRule, "Container_CommonRulePanel", MogoUILayer.LayerAlert, "ModuleCommonUI.CommonRulePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.UIOperationShield, "Container_UIOperationShieldPanel", MogoUILayer.LayerAlert, "ModuleCommonUI.UIOperationShieldPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.AntiAddiction, "Container_AntiAddictionTips", MogoUILayer.LayerAlert, "ModuleCommonUI.AntiAddictionToolTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.RewardBox, "Container_RewardBoxPanel", MogoUILayer.LayerUINotice, "ModuleCommonUI.RewardBoxPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.BuffTips, "Container_BuffTips", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.BuffTips", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.ItemCompose, "Container_ItemComposePanel", MogoUILayer.LayerUIToolTip, "ModuleCommonUI.ItemComposePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.EquipFightForceTips, "Container_EquipFightForceEffect", MogoUILayer.LayerUIPanel, "ModuleCommonUI.EquipFightForceTips", BlurUnderlay.Have, HideMainUI.Hide);
        }

    }
}
