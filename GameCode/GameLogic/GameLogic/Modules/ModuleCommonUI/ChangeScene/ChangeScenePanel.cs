﻿using Common.Base;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using UnityEngine;

namespace ModuleCommonUI
{
    public class ChangeSceneData
    {
        public int instId;
        public Action changeAction;
        public Action cancelAction;

        public ChangeSceneData(int instId, Action changeAction, Action cancelAction)
        {
            this.instId = instId;
            this.changeAction = changeAction;
            this.cancelAction = cancelAction;
        }
    }


    public class ChangeScenePanel : BasePanel
    {
        private StateText _timeText;
        private StateText _descText;
        private KButton _cancelButton;
        private uint _timerId;
        private int _second;
        private string _timeContent;
        private string _descContent;
        private ChangeSceneData _data;
        private KParticle _particle;

        protected override void Awake()
        {
            _cancelButton = GetChildComponent<KButton>("Button_quxiao");
            _timeText = GetChildComponent<StateText>("Label_txtDaojishi");
            _descText = GetChildComponent<StateText>("Label_txtChuansong");
            _particle = AddChildComponent<KParticle>("fx_ui_25_chuansongmen_01");
            _particle.Stop();
            _timeContent = _timeText.CurrentText.text;
            _descContent = _descText.CurrentText.text;
            _cancelButton.onClick.AddListener(OnCancelBtnClick);
        }

        private void OnCancelBtnClick()
        {
            if (_data.cancelAction != null)
            {
                _data.cancelAction.Invoke();
            }
            ResetTimer();
            ClosePanel();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.ChangeScene; }
        }

        public override void OnShow(object data)
        {
            _data = data as ChangeSceneData;
            StartCountDown();
        }

        private void StartCountDown()
        {
            ResetTimer();
            _second = 2;
            _descText.CurrentText.text = string.Format(_descContent, instance_helper.GetInstanceName(_data.instId));
            _timerId = TimerHeap.AddTimer(0, 1000, OnStep);
            _particle.Play(true);
            _particle.Position = new Vector3(-560, 340, 0);
        }

        private void OnStep()
        {
            if (_second <= 0)
            {
                ResetTimer();
                _data.changeAction.Invoke();
                ClosePanel();
            }
            _timeText.CurrentText.text = string.Format(_timeContent, _second);
            _second--;
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        public override void OnClose()
        {
            _data = null;
        }

        public static void Show(int instId, Action changeAction, Action cancelAction = null)
        {
            if (map_helper.IsWild(instId) == false && PlayerAvatar.Player.level < instance_helper.GetMinLevel(instId))
            {
                SystemInfoManager.Instance.Show(15);
                return;
            }
            ChangeSceneData data = new ChangeSceneData(instId, changeAction, cancelAction);
            UIManager.Instance.ShowPanel(PanelIdEnum.ChangeScene, data);
        }

    }
}
