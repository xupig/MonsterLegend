﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleCommonUI
{
    public class RewardBoxPanel : BasePanel
    {
        private KList _list;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            GetChildComponent<StateText>("Container_biaoti/Label_txtBaoxiangjiangli").CurrentText.text = "宝箱奖励";
            InitList();
        }

        private void InitList()
        {
            _list = GetChildComponent<KList>("List_content");
            _list.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _list.SetGap(0, 27);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.RewardBox; }
        }

        public override void OnShow(object data)
        {
            Refresh((int)data);
        }

        public override void OnClose()
        {
        }

        private void Refresh(int rewardId)
        {
            List<BaseItemData> baseItemDataList = item_reward_helper.GetItemDataList(rewardId);
            _list.SetDataList<RewardItem>(baseItemDataList);
            MogoLayoutUtils.SetCenter(_list.gameObject);
        }
    }
}
