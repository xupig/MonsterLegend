﻿#region 模块信息
/*==========================================
// 模块名：FloatTips
// 命名空间: ModuleCommonUI
// 修改者列表：
// 创建日期：2014/12/26
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;

using Common.Base;
using Common.Data;
using Game.UI.UIComponent;
using UnityEngine;

using GameLoader.Utils.Timer;
using Common.ExtendComponent;

namespace ModuleCommonUI
{
    public class FloatTips : BasePanel
    {
        private const int MAX_GAMEOBJECT_COUNT = 3;
        private Vector2 StartPosition = new Vector2(264, -300);
        private GameObject _tipsTemplate;

        private Queue<GameObject> _gameObjectPool = new Queue<GameObject>();
        private Queue<string> _tipContentQueue = new Queue<string>();
        private List<SingleTipView> _showingTipsQueue = new List<SingleTipView>();

        private Vector2[] endPositionArray = new Vector2[3];

        private int _createGameObjectCount = 0;

        protected override PanelIdEnum Id
        {
            get
            {
                return PanelIdEnum.FloatTips;
            }
        }

        protected override void Awake()
        {
            AddChildComponent<Locater>("Container_tip");
            AddChildComponent<StateTextLocater>("Container_tip/Label_txtTip");
            AddChildComponent<SingleTipView>("Container_tip");
            _tipsTemplate = GetChild("Container_tip");
            _tipsTemplate.SetActive(false);
            gameObject.AddComponent<RaycastComponent>();
            InitEndPositionArray();
        }

        private void InitEndPositionArray()
        {
            endPositionArray[0] = new Vector2(264, -144);
            endPositionArray[1] = new Vector2(264, -176);
            endPositionArray[2] = new Vector2(264, -209);
        }

        public override void OnShow(object data)
        {
            SetData(data);
        }

        public override void SetData(object data)
        {
            ToolTipsDataWrapper wrapper = data as ToolTipsDataWrapper;
            string tipString = wrapper.data as string;
            AddInfoToTipQueue(tipString);
        }

        private uint timeId = 0;
        private void AddShowTipsTimer()
        {
            timeId = TimerHeap.AddTimer(0, 300, GetContentFromQueueAndShow);
        }

        private void RemoveShowTipsTimer()
        {
            TimerHeap.DelTimer(timeId);
            timeId = 0;
        }

        private void AddInfoToTipQueue(string info)
        {
            if (_tipContentQueue.Contains(info) == false)
            {
                _tipContentQueue.Enqueue(info);
            }
            if (timeId == 0 && _tipContentQueue.Count > 0)
            {
                AddShowTipsTimer();
            }
        }

        private void GetContentFromQueueAndShow()
        {
            if (_tipContentQueue.Count > 0)
            {
                if (_showingTipsQueue.Count < MAX_GAMEOBJECT_COUNT)
                {
                    string content = _tipContentQueue.Dequeue();
                    ShowTips(content);
                }
            }
            else
            {
                RemoveShowTipsTimer();
            }
        }

        public override void OnLeaveMap(int mapType)
        {

        }

        private Vector2 GetEndPosition(SingleTipView tipView)
        {
            int count = 0;
            for (int i = 0; i < _showingTipsQueue.Count; i++)
            {
                if (_showingTipsQueue[i] != tipView)
                {
                    if (_showingTipsQueue[i].State == MovingState.MOVING || _showingTipsQueue[i].State == MovingState.SHOWING)
                    {
                        count++;
                    }
                }
                else
                {
                    break;
                }
            }
            return endPositionArray[count];
        }

        private void ShowTips(string content)
        {
            GameObject go = GetGameObject();
            SingleTipView tipView = go.GetComponent<SingleTipView>();
            if (tipView == null)
            {
                tipView = go.AddComponent<SingleTipView>();
            }
            _showingTipsQueue.Add(tipView);
            tipView.Position = StartPosition;
            tipView.Content = content;
            tipView.StartTween(GetEndPosition(tipView));
            tipView.onTweenFinish.AddListener(RefreshLayout);
            TimerHeap.AddTimer<SingleTipView>(1000, 0, HideTipAndRecycle, tipView);
        }

        private void HideTipAndRecycle(SingleTipView tipView)
        {
            tipView.onTweenFinish.RemoveListener(RefreshLayout);
            tipView.MoveOutOfScreen();
            _showingTipsQueue.Remove(tipView);
            _gameObjectPool.Enqueue(tipView.gameObject);
            TimerHeap.AddTimer(100, 0, RefreshLayout);
            GetContentFromQueueAndShow();
        }

        private void RefreshLayout()
        {
            int count = 0;
            for (int i = 0; i < _showingTipsQueue.Count; i++)
            {
                if (_showingTipsQueue[i].State == MovingState.SHOWING)
                {
                    TweenPosition.Begin(_showingTipsQueue[i].gameObject, 0.2f, endPositionArray[i]).onFinished = CheckAgain;
                    count++;
                }
            }
        }

        private void CheckAgain(UITweener tween)
        {
            SingleTipView tipView = tween.gameObject.GetComponent<SingleTipView>();
            if (tipView != null && tipView.State == MovingState.Hided)
            {
                tipView.MoveOutOfScreen();
            }
        }

        private GameObject GetGameObject()
        {
            GameObject go;
            if (_gameObjectPool.Count > 0)
            {
                go = _gameObjectPool.Dequeue();
                return go;
            }
            else
            {
                _createGameObjectCount += 1;
                go = GameObject.Instantiate(_tipsTemplate) as GameObject;
                go.transform.SetParent(this.transform);
                go.transform.localScale = Vector3.one;
                go.transform.localPosition = Vector3.zero;
                go.SetActive(true);
            }
            return go;
        }

        public override void OnClose()
        {

        }
    }

    public enum MovingState
    {
        Hided = 1,
        Created = 2,
        MOVING = 3,
        SHOWING = 4,
    }

    public class SingleTipView : KContainer
    {
        private MovingState _state;
        private Locater _locater;
        private StateText _tipTxt;
        private StateTextLocater _tipLocater;
        private Vector2 HidePosition = new Vector2(0, 100);

        public KComponentEvent onTweenFinish = new KComponentEvent();

        private string _content;

        protected override void Awake()
        {
            _locater = GetComponent<Locater>();
            _tipTxt = GetChildComponent<StateText>("Label_txtTip");
            _tipLocater = GetChildComponent<StateTextLocater>("Label_txtTip");
        }

        public string Content
        {
            get
            {
                return _content;
            }
            set
            {
                _content = value;
                _tipTxt.CurrentText.text = _content;
                _state = MovingState.Created;
            }
        }

        public Vector2 Position
        {
            get
            {
                return _locater.Position;
            }
            set
            {
                _locater.Position = value;
            }
        }

        public MovingState State
        {
            get
            {
                return _state;
            }
        }

        public void StartTween(Vector2 endPosition)
        {
            _state = MovingState.MOVING;
            TweenPosition.Begin(gameObject, 0.3f, endPosition, UITweener.Method.Linear).onFinished = OnTweenFinished;
        }

        private void OnTweenFinished(UITweener tween)
        {
            _state = MovingState.SHOWING;
            onTweenFinish.Invoke();
        }

        public void MoveOutOfScreen()
        {
            _tipTxt.CurrentText.text = string.Empty;
            _state = MovingState.Hided;
        }
    }
}

