﻿using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using Mogo.Util;
using MogoEngine.Events;
using System.Collections.Generic;

namespace ModuleCommonUI
{
    public class SelectListPanel : BasePanel
    {
        private KScrollView _scrollView;
        private KList _list;

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("ScaleImage_sharedZhezhao");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_fubenchuangsong");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _list.SetPadding(0, 20, 0, 20);
            CloseBtn = GetChildComponent<KButton>("Container_Bg/Button_close02");
            this.Visible = false;
        }

        private void SetSelectist(List<SelectListItemData> dataList)
        {
            this.Visible = true;
            _list.RemoveAll();
            _list.SetDataList<SelectListItem>(dataList);
        }


        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.SelectList; }
        }

        public override void OnShow(object data)
        {
            List<SelectListItemData> list = data as List<SelectListItemData>;
            SetSelectist(list);
        }

        public override void OnClose()
        {

        }
    }

    public class SelectListItem : KList.KListItemBase
    {
        private SelectListItemData _data;
        private StateText _titleText;
        private KButton _button;

        protected override void Awake()
        {
            _button = GetChildComponent<KButton>("Button_btn");
            _titleText = _button.GetChildComponent<StateText>("label");
            _titleText.CurrentText.alignment = UnityEngine.TextAnchor.UpperCenter;
            _button.onClick.AddListener(OnItemClick);
        }

        private void OnItemClick()
        {
            if (_data.onSelect != null)
            {
                _data.onSelect.Invoke();
            }
            UIManager.Instance.ClosePanel(PanelIdEnum.SelectList);
        }

        private void RefreshContent()
        {
            _titleText.CurrentText.text = _data.content;
        }

        public override void Dispose()
        {
            _button.onClick.RemoveListener(OnItemClick);
            base.Data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null && value != base.Data)
                {
                    _data = value as SelectListItemData;
                    RefreshContent();
                }
            }
        }

    }
}
