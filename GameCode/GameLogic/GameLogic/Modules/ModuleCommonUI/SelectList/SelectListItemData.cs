﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleCommonUI
{
    public class SelectListItemData
    {
        public string content;
        public Action onSelect;

        public SelectListItemData(string content, Action onSelect)
        {
            this.content = content;
            this.onSelect = onSelect;
        }
    }
}
