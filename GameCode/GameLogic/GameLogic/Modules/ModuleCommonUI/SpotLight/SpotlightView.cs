﻿using System;
using System.Collections.Generic;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;

using Common.Utils;
using Common.Data;
using Common.ExtendComponent;

namespace ModuleCommonUI
{
    public class SpotlightView : KContainer
    {
        private static float INTERVAL_WIDTH = 100.0f;
        private static float SPEED = 4.0f;
        private static int TXT_COUNT = 2;
     
        private List<SpotlightData> _dataList = new List<SpotlightData>();
        private List<StateText> _availableTxtList = new List<StateText>();
        private List<Locater> _runningLocaterList = new List<Locater>();
        private List<SpotlightData> _runningDataList = new List<SpotlightData>();
        private GameObject _maskGo;
        private float _maskWidth;

        public KComponentEvent onFinish = new KComponentEvent();

        protected override void Awake()
        {
            _maskGo = GetChild("ScaleImage_mask");
            RectTransform maskRect = _maskGo.transform as RectTransform;
            _maskWidth = maskRect.sizeDelta.x;
            StateText label0 = GetChildComponent<StateText>("Label_content0");
            StateText label1 = GetChildComponent<StateText>("Label_content1");
            label0.transform.SetParent(_maskGo.transform, false);
            label1.transform.SetParent(_maskGo.transform, false);
            Locater locater0 = label0.gameObject.AddComponent<Locater>();
            locater0.X = _maskWidth;
            locater0.Y = 0;
            Locater locater1 = label1.gameObject.AddComponent<Locater>();
            locater1.X = _maskWidth;
            locater1.Y = 0;
            label1.gameObject.AddComponent<Locater>();
            label0.CurrentText.text = string.Empty;
            label1.CurrentText.text = string.Empty;
            _availableTxtList.Add(label0);
            _availableTxtList.Add(label1);
            gameObject.AddComponent<RaycastComponent>();
            Visible = false;
        }

        public void Show(object data)
        {
            if (Visible == false)
            {
                Visible = true;
            }
            _dataList.Add(data as SpotlightData);
            ShowData();
        }

        private void ShowData()
        {
            if(_availableTxtList.Count > 0 && _dataList.Count > 0)
            {
                SpotlightData data = _dataList[0];
                _dataList.RemoveAt(0);
                StateText txt = _availableTxtList[0];
                _availableTxtList.RemoveAt(0);
                txt.CurrentText.text = data.Content;
                txt.ChangeAllStateTextAlignment(TextAnchor.MiddleLeft);
                MogoGameObjectHelper.RecalculateTextSize(txt.CurrentText);
                Locater locater = txt.gameObject.GetComponent<Locater>();
                locater.X = GetStartX();
                _runningLocaterList.Add(locater);
                _runningDataList.Add(data);
            }
            else if(_availableTxtList.Count == TXT_COUNT)
            {
                Visible = false;
                onFinish.Invoke();
            }
        }

        private float GetStartX()
        {
            if(_runningLocaterList.Count == 0)
            {
                return _maskWidth;
            }
            return Math.Max(_maskWidth, _runningLocaterList[0].X + _runningLocaterList[0].Width + INTERVAL_WIDTH);
        }

        protected void Update()
        {
            for(int i = _runningLocaterList.Count - 1; i >= 0; i--)
            {
                Locater locater = _runningLocaterList[i];
                if((locater.X + locater.Width) < 0)
                {
                    SpotlightData data = _runningDataList[i];
                    data.ShowTime--;
                    _runningDataList.RemoveAt(i);
                    _runningLocaterList.RemoveAt(i);
                    _availableTxtList.Add(locater.gameObject.GetComponent<StateText>());
                    if(data.ShowTime > 0)
                    {
                        Show(data);
                    }
                    else
                    {
                        ShowData();
                    }
                }
            }
            for(int i = 0; i < _runningLocaterList.Count; i++)
            {
                Locater locater = _runningLocaterList[i];
                locater.X -= SPEED;
            }
        }
    }
}
