﻿using System;
using System.Collections.Generic;

using Common.Base;
using Game.UI.UIComponent;
using UnityEngine;

using Common.Utils;
using Common.Data;
using Common.ExtendComponent;
using MogoEngine.Events;
using Common.Events;

namespace ModuleCommonUI
{
    public class SpotlightPanel : BasePanel
    {
        private SpotlightView _topSpotLight;
        private SpotlightView _bottomSpotLight;

        public static bool isBottomSpotLightRunning = false;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Spotlight; }
        }

        protected override void Awake()
        {
            _topSpotLight = AddChildComponent<SpotlightView>("Container_topCenter");
            _bottomSpotLight = AddChildComponent<SpotlightView>("Container_bottomCenter");
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            SetData(data);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        public override void SetData(object data)
        {
            if (data is SpotlightData)
            {
                SpotlightData spotlightData = data as SpotlightData;
                if (spotlightData.ShowType == SpotlightData.SHOW_TYPE_BOTTOM)
                {
                    _bottomSpotLight.Show(data);
                    isBottomSpotLightRunning = true;
                    EventDispatcher.TriggerEvent<bool>(CommonUIEvents.ON_BOTTOM_SPOTLIGHT_VISIBILITY_CHANGE, _bottomSpotLight.Visible);
                }
                else if (spotlightData.ShowType == SpotlightData.SHOW_TYPE_TOP)
                {
                    _topSpotLight.Show(data);
                }
            }
        }

        public override void OnLeaveMap(int mapType)
        {

        }


        private void AddEventListener()
        {
            _bottomSpotLight.onFinish.AddListener(OnBottomSpotlightFinish);
        }

        private void RemoveEventListener()
        {
            _bottomSpotLight.onFinish.RemoveListener(OnBottomSpotlightFinish);
        }

        private void OnBottomSpotlightFinish()
        {
            isBottomSpotLightRunning = _bottomSpotLight.Visible;
            EventDispatcher.TriggerEvent<bool>(CommonUIEvents.ON_BOTTOM_SPOTLIGHT_VISIBILITY_CHANGE, _bottomSpotLight.Visible);
        }

    }
}
