﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using ModuleCopy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarResultPanel : BasePanel
    {
        private CopyGuildWarBaseLogic _logic;
        private float _scoreY;

        private CopyModelComponent _model;
        private KButton _btnSure;
        private KButton _statisticBtn;
        private GameObject _goWin;
        private GameObject _goLose;
        private GameObject _goDraw;
        private GameObject _goFinish;
        private GameObject _goRound;
        private StateText _textRound;
        private Countdown _countdown;
        private StateText _textCountdown;
        private ArtNumberList _artNumberList;
        private KContainer _containerScore;
        private RectTransform _rectScore;
        private StateText _textScoreDesc;
        private StateImage _imagePlus;
        private StateImage _imageMinus;
        private Locater _scoreLocater;
        private GameObject _goDuel;
        private RewardItem _rewardItem;
        private StateText _textTips;

        protected override void Awake()
        {
            _model = AddChildComponent<CopyModelComponent>("Container_model");
            _btnSure = GetChildComponent<KButton>("Container_result/Button_queding");
            _statisticBtn = GetChildComponent<KButton>("Container_result/Button_zhandoutongji");
            _goWin = GetChild("Container_result/Container_shengli");
            _goLose = GetChild("Container_result/Container_shibai");
            _goDraw = GetChild("Container_result/Container_pingjui");
            _goFinish = GetChild("Container_result/Container_jieshu");
            _goRound = GetChild("Container_result/Container_shenglitiaojian/Container_jushu");
            _scoreLocater = AddChildComponent<Locater>("Container_result/Container_shenglitiaojian");
            _scoreY = _scoreLocater.Y;
            _textRound = GetChildComponent<StateText>("Container_result/Container_shenglitiaojian/Container_jushu/Label_txtJushu");
            _countdown = gameObject.AddComponent<Countdown>();
            _textCountdown = GetChildComponent<StateText>("Container_result/Label_txtShijian");
            InitReward();
            InitScore();

            AddListener();
        }

        private void InitReward()
        {
            _goDuel = GetChild("Container_result/Container_juedou");
            _rewardItem = AddChildComponent<RewardItem>("Container_result/Container_juedou/Container_jiangli");
            _textTips = GetChildComponent<StateText>("Container_result/Container_juedou/Label_txtwuxiao");
        }

        private void InitScore()
        {
            _artNumberList = AddChildComponent<ArtNumberList>("Container_result/Container_shenglitiaojian/Container_jifen/Container_fenshu/List_jifen");
            _containerScore = GetChildComponent<KContainer>("Container_result/Container_shenglitiaojian/Container_jifen/Container_fenshu");
            _rectScore = GetChildComponent<RectTransform>("Container_result/Container_shenglitiaojian/Container_jifen/Container_fenshu");
            _imagePlus = GetChildComponent<StateImage>("Container_result/Container_shenglitiaojian/Container_jifen/Container_fenshu/Image_jiahao");
            _imageMinus = GetChildComponent<StateImage>("Container_result/Container_shenglitiaojian/Container_jifen/Container_fenshu/Image_jianhao");
            _rectScore.pivot = new Vector2(0.5f, 1f);
            _rectScore.anchorMin = new Vector2(0.5f, 1f);
            _rectScore.anchorMax = new Vector2(0.5f, 1f);
            _textScoreDesc = GetChildComponent<StateText>("Container_result/Container_shenglitiaojian/Container_jifen/Label_txthuodejifen");
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnSure.onClick.AddListener(OnSure);
            _statisticBtn.onClick.AddListener(ShowStatisticPanel);
            _countdown.onEnd.AddListener(OnCountdownEnd);
        }

        private void RemoveListener()
        {
            _btnSure.onClick.RemoveListener(OnSure);
            _statisticBtn.onClick.RemoveListener(ShowStatisticPanel);
            _countdown.onEnd.RemoveListener(OnCountdownEnd);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildWarResult; }
        }

        private void OnSure()
        {
            if (_logic.NeedLLeaveScene())
            {
                GameSceneManager.GetInstance().LeaveCombatScene();
            }
            else
            {
                ClosePanel();
            }
        }

        private void OnCountdownEnd()
        {
            if (_logic.NeedLLeaveScene())
            {
                GameSceneManager.GetInstance().LeaveCombatScene();
            }
            else
            {
                ClosePanel();
            }
        }

        private void ShowStatisticPanel()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.BattleDataStatistic);
        }

        public override void OnShow(object data)
        {
            SetData(data);
        }

        public override void SetData(object data)
        {
            CopyGuildWarBaseLogic logic = data as CopyGuildWarBaseLogic;
            logic.CloseOtherPanel();
            _logic = logic;
            _model.LoadPlayerModel(logic.GetModelAction());
            RefreshRound(logic);
            RefreshResult(logic.Result);
            RefreshCountDown();
            RefreshScore(logic);
            RefreshRewardItem(logic);
            RefreshStatisticButton();
        }

        private void RefreshRound(CopyGuildWarBaseLogic logic)
        {
            if (logic.ShowRound())
            {
                _goRound.SetActive(true);
                _textRound.CurrentText.text = MogoLanguageUtil.GetContent(111671, logic.Round);
            }
            else
            {
                _goRound.SetActive(false);
            }
        }

        private void RefreshResult(CopyResult Result)
        {
            _goWin.SetActive(false);
            _goLose.SetActive(false);
            _goDraw.SetActive(false);
            _goFinish.SetActive(false);
            if (_logic.ShowFinish())
            {
                _goFinish.SetActive(true);
                return;
            }
            switch (Result)
            {
                case CopyResult.Win:
                    _goWin.SetActive(true);
                    break;
                case CopyResult.Lose:
                    _goLose.SetActive(true);
                    break;
                case CopyResult.Draw:
                    _goDraw.SetActive(true);
                    break;
            }
        }

        private void RefreshCountDown()
        {
            _countdown.StartCountdown(10, _textCountdown);
        }

        private void RefreshScore(CopyGuildWarBaseLogic logic)
        {
            if (_logic.ShowRewardItem())
            {
                _scoreLocater.Y = _scoreY + 50;
            }
            else
            {
                _scoreLocater.Y = _scoreY;
            }
            _textScoreDesc.CurrentText.text = logic.GetScoreDesc();
            int Score = logic.Score;
            if (Score < 0)
            {
                _imageMinus.Visible = true;
                _imagePlus.Visible = false;
                Score = -Score;
            }
            else
            {
                _imagePlus.Visible = true;
                _imageMinus.Visible = false;
            }
            _artNumberList.SetNumber(Score);
            _containerScore.RecalculateSize();
            Vector2 position = _rectScore.anchoredPosition;
            _rectScore.anchoredPosition = new Vector2(0f, position.y);
        }

        private void RefreshRewardItem(CopyGuildWarBaseLogic logic)
        {
            if (logic.ShowRewardItem())
            {
                _goDuel.SetActive(true);
                if (logic.DuringActivity)
                {
                    _textTips.CurrentText.text = activity_helper.IsOpen(25) ? MogoLanguageUtil.GetContent(122133) : string.Empty;
                }
                else
                {
                    _textTips.CurrentText.text = MogoLanguageUtil.GetContent(122132);
                }
                _rewardItem.Visible = false;
            }
            else
            {
                _goDuel.SetActive(false);
            }
        }

        private void RefreshStatisticButton()
        {
            int insId = GameSceneManager.GetInstance().curInstanceID;
            instance ins = instance_helper.GetInstanceCfg(insId);
            _statisticBtn.Visible = instance_helper.GetStatisticType(ins) != 0;
        }

        public override void OnClose()
        {
        }
    }
}
