﻿using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuildWar
{
    public class GuildWarResultRecordItem : KList.KListItemBase
    {
        private StateText _textMyName;
        private StateText _textMyFight;
        private StateText _textMyScore;
        private StateText _textOtherName;
        private StateText _textOtherFight;
        private StateText _textOtherScore;
        private StateImage _imgMyWin;
        private StateImage _imgOtherWin;
        private PbGuildBattlePkLog _data;

        protected override void Awake()
        {
            _textMyName = GetChildComponent<StateText>("Label_txtBenfangWanjia");
            _textMyFight = GetChildComponent<StateText>("Label_txtBenfangZhandouli");
            _textMyScore = GetChildComponent<StateText>("Label_txtBenfangBifen");
            _textOtherName = GetChildComponent<StateText>("Label_txtDuifangWanjia");
            _textOtherFight = GetChildComponent<StateText>("Label_txtDuifangZhandouli");
            _textOtherScore = GetChildComponent<StateText>("Label_txtDuifangBifen");
            _imgMyWin = GetChildComponent<StateImage>("Container_benfang/Image_sheng");
            _imgOtherWin = GetChildComponent<StateImage>("Container_duifang/Image_sheng");
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                _data = value as PbGuildBattlePkLog;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            _textMyName.CurrentText.text = _data.self_avatar_name;
            _textMyFight.CurrentText.text = _data.self_avatar_fight.ToString();
            _textMyScore.CurrentText.text = _data.self_avatar_point.ToString();
            _textOtherName.CurrentText.text = _data.to_avatar_name;
            _textOtherFight.CurrentText.text = _data.to_avatar_fight.ToString();
            _textOtherScore.CurrentText.text = _data.to_avatar_point.ToString();
            RefreshWinImage();
        }

        private void RefreshWinImage()
        {
            if (_data.self_avatar_point > _data.to_avatar_point)
            {
                _imgMyWin.Visible = true;
                _imgOtherWin.Visible = false;
            }
            else if (_data.self_avatar_point < _data.to_avatar_point)
            {
                _imgMyWin.Visible = false;
                _imgOtherWin.Visible = true;
            }
            else
            {
                _imgMyWin.Visible = false;
                _imgOtherWin.Visible = false;
            }
        }
    }
}
