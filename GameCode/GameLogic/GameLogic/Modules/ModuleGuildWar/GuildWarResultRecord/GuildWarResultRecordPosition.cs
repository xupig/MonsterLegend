﻿using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarResultRecordPosition : KContainer
    {
        private KScrollView _scrollView;
        private KList _list;
        private KButton _closeBtn;

        protected override void Awake()
        {
            _scrollView = GetChildComponent<KScrollView>("ScrollView_zhanlibang");
            _list = GetChildComponent<KList>("ScrollView_zhanlibang/mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _list.SetGap(1, 0);
            _closeBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _closeBtn.onClick.AddListener(OnCloseBtnClick);
        }

        private void OnCloseBtnClick()
        {
            this.Visible = false;
        }

        public void SetData(bool isMyGuild, List<PbGuildBattlePkLog> pbList)
        {
            this.Visible = true;
            Dictionary<string, PositionData> dataDict = new Dictionary<string, PositionData>();
            for (int i = 0; i < pbList.Count; i++)
            {
                PbGuildBattlePkLog pb = pbList[i];
                if(isMyGuild)
                {

                    if (!string.IsNullOrEmpty(pb.self_avatar_name) && dataDict.ContainsKey(pb.self_avatar_name) == false)
                    {
                        dataDict.Add(pb.self_avatar_name, new PositionData(pb.self_avatar_name, pb.self_avatar_fight, pb.self_avatar_military));
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(pb.to_avatar_name) && dataDict.ContainsKey(pb.to_avatar_name) == false)
                    {
                        dataDict.Add(pb.to_avatar_name, new PositionData(pb.to_avatar_name, pb.to_avatar_fight, pb.to_avatar_military));
                    }
                }
            }
            List<PositionData> dataList = new List<PositionData>();
            dataList.AddRange(dataDict.Values);
            _list.SetDataList<PositionItem>(dataList);
        }

        class PositionData
        {
            public string Name;
            public uint FightValue;
            public uint MilitaryRank;

            public PositionData(string name, uint fightValue, uint militaryRank)
            {
                this.Name = name;
                this.FightValue = fightValue;
                this.MilitaryRank = militaryRank;
            }
        }

        class PositionItem : KList.KListItemBase
        {
            private StateText _textName;
            private StateText _textFight;
            private StateText _textPosition;

            protected override void Awake()
            {
                _textName = GetChildComponent<StateText>("Label_txtJiaosei");
                _textFight = GetChildComponent<StateText>("Label_txtZhanli");
                _textPosition = GetChildComponent<StateText>("Label_txtJunxian");
            }

            public override void Dispose()
            {
            }

            public override object Data
            {
                set
                {
                    PositionData data = value as PositionData;
                    _textName.CurrentText.text = data.Name;
                    _textFight.CurrentText.text = data.FightValue.ToString();
                    _textPosition.CurrentText.text = guild_match_helper.GetMilitaryRankName((int)data.MilitaryRank);
                }
            }
        }
    }
}
