﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarResultRecordPanel : BasePanel
    {
        private PbGuildBattlePkLogList _pb;

        private CategoryList _categoryList;
        private StateText _textMyGuildName;
        private StateText _textOtherGuildName;
        private StateText _textMyGuildScore;
        private StateText _textOtherGuildScore;
        private StateImage _imgMyGuildWin;
        private StateImage _imgOtherGuildWin;
        private StateText _textTime;
        private StateText _noneTxt;
        private KButton _btnMyAssignedPosition;
        private KButton _btnOtherAssignedPosition;
        private KButton _btnTips;
        private KList _listContent;
        private GuildWarResultRecordPosition _resultRecordPosition;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_gonghuizhanzhankuang/Button_fanhui");
            _categoryList = GetChildComponent<CategoryList>("Container_gonghuizhanzhankuang/List_categoryList");
            _textMyGuildName = GetChildComponent<StateText>("Container_gonghuizhanzhankuang/Container_zhankuang/Container_gonghuimingcheng/Label_txtDuifangGonghuimingcheng");
            _textOtherGuildName = GetChildComponent<StateText>("Container_gonghuizhanzhankuang/Container_zhankuang/Container_gonghuimingcheng/Label_txtBenGonghuimingcheng");
            _textMyGuildScore = GetChildComponent<StateText>("Container_gonghuizhanzhankuang/Container_zhankuang/Container_gonghuimingcheng/Label_txtBenfangbifen");
            _textOtherGuildScore = GetChildComponent<StateText>("Container_gonghuizhanzhankuang/Container_zhankuang/Container_gonghuimingcheng/Label_txtDuifangbifen");
            _imgMyGuildWin = GetChildComponent<StateImage>("Container_gonghuizhanzhankuang/Container_zhankuang/Container_gonghuimingcheng/Container_benfang/Image_shengli");
            _imgOtherGuildWin = GetChildComponent<StateImage>("Container_gonghuizhanzhankuang/Container_zhankuang/Container_gonghuimingcheng/Container_duifang/Image_shengli");
            _textTime = GetChildComponent<StateText>("Container_gonghuizhanzhankuang/Container_zhankuang/Container_gonghuimingcheng/Label_txtXiaozusai");
            _btnOtherAssignedPosition = GetChildComponent<KButton>("Container_gonghuizhanzhankuang/Container_zhankuang/Container_paihangbang/Container_zhankuangduibi/Button_benfangzhiwei");
             _btnMyAssignedPosition = GetChildComponent<KButton>("Container_gonghuizhanzhankuang/Container_zhankuang/Container_paihangbang/Container_zhankuangduibi/Button_duifangzhiwei");
             _btnTips = GetChildComponent<KButton>("Container_gonghuizhanzhankuang/Container_zhankuang/Container_gonghuimingcheng/Button_Gantang");
            _listContent = GetChildComponent<KList>("Container_gonghuizhanzhankuang/Container_zhankuang/Container_paihangbang/Container_zhankuangduibi/ScrollView_zhanlibang/mask/content");
            _noneTxt = GetChildComponent<StateText>("Container_gonghuizhanzhankuang/Container_zhankuang/Container_paihangbang/Label_txtmeiyoushuju");
            _noneTxt.ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);
            _listContent.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _resultRecordPosition = AddChildComponent<GuildWarResultRecordPosition>("Container_xiaotanchuang");
            InitCategoryList();
            AddListener();
        }

        private void InitCategoryList()
        {
            List<CategoryItemData> listToggleData = new List<CategoryItemData>();
            for (int i = 0; i < 5; i++)
            {
                listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(111627, i + 1)));
            }
            _categoryList.RemoveAll();
            _categoryList.SetDataList<CategoryListItem>(listToggleData);
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnMyAssignedPosition.onClick.AddListener(OnMyAssignedPosition);
            _btnOtherAssignedPosition.onClick.AddListener(OnOtherAssignedPosition);
            _categoryList.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            _btnTips.onClick.AddListener(onDescBtnClick);
        }

        private void RemoveListener()
        {
            _btnMyAssignedPosition.onClick.RemoveListener(OnMyAssignedPosition);
            _btnOtherAssignedPosition.onClick.RemoveListener(OnOtherAssignedPosition);
            _categoryList.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            _btnTips.onClick.RemoveListener(onDescBtnClick);
        }

        private void onDescBtnClick()
        {
            RuleTipsDataWrapper wrapper = new RuleTipsDataWrapper(MogoLanguageUtil.GetContent(111575), _btnTips.GetComponent<RectTransform>(), RuleTipsDirection.BottomLeft);
            UIManager.Instance.ShowPanel(PanelIdEnum.RuleTips, wrapper);
        }

        private void OnMyAssignedPosition()
        {
            _resultRecordPosition.SetData(true, _pb.list);
        }

        private void OnOtherAssignedPosition()
        {
            _resultRecordPosition.SetData(false, _pb.list);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildWarResultRecord; }
        }

        public override void OnShow(object data)
        {
            _categoryList.SelectedIndex = 0;
            PbGuildBattlePkLogList pb = data as PbGuildBattlePkLogList;
            _pb = pb;
            RefreshTitle(pb);
            RefreshList();
        }

        private void OnSelectedIndexChanged(CategoryList arg0, int index)
        {
            RefreshList();
        }

        private void RefreshTitle(PbGuildBattlePkLogList pb)
        {
            _textMyGuildName.CurrentText.text = string.IsNullOrEmpty(pb.self_guild_name) ? "--" : pb.self_guild_name;
            _textMyGuildScore.CurrentText.text = pb.self_point.ToString();
            _textOtherGuildName.CurrentText.text = string.IsNullOrEmpty(pb.to_guild_name) ? "--" : pb.to_guild_name;
            _textOtherGuildScore.CurrentText.text = pb.to_point.ToString();
            string combatString = GetCombatString(pb.battle_type);
            string timeString = pb.date == 0 ? "" : MogoTimeUtil.ToLocalTime((long)pb.date, "yyyy-MM-dd");
            _textTime.CurrentText.text = string.Concat(timeString, "  ", combatString);
            RefreshWinImage(pb);
        }

        private string GetCombatString(uint battleType)
        {
            switch (battleType)
            {
                case public_config.GUILD_MATCH:
                    return MogoLanguageUtil.GetContent(111672);
                case public_config.GUILD_MATCH_FINAL_MATCH:
                case public_config.GUILD_MATCH_FINAL_CREAM_MATCH:
                    return MogoLanguageUtil.GetContent(111673);
                case public_config.GUILD_MATCH_FRIENDLY_MATCH:
                    return MogoLanguageUtil.GetContent(111674);
            }
            return string.Empty;
        }

        private void RefreshWinImage(PbGuildBattlePkLogList pb)
        {
            _imgMyGuildWin.Visible = false;
            _imgOtherGuildWin.Visible = false;

            if (_pb.self_guild_id != 0 && _pb.to_guild_id == 0)    //轮空状态
            {
                _imgMyGuildWin.Visible = true;
                return;
            }

            if (pb.is_in_battle == 0)//不在公会战
            {
                if (pb.self_point > pb.to_point)
                {
                    _imgMyGuildWin.Visible = true;
                }
                else if (pb.self_point < pb.to_point)
                {
                    _imgOtherGuildWin.Visible = true;
                }
            }
            else
            {
                //在公会战中则不显示胜负
            }
        }

        private void RefreshList()
        {
            List<PbGuildBattlePkLog> dataList = new List<PbGuildBattlePkLog>();
            for (int i = 0; i < _pb.list.Count; i++)
            {
                if (_pb.list[i].round_count == _categoryList.SelectedIndex + 1)
                {
                    dataList.Add(_pb.list[i]);
                }
            }
            _listContent.SetDataList<GuildWarResultRecordItem>(dataList);
            _btnMyAssignedPosition.Visible = dataList.Count > 0;
            _btnOtherAssignedPosition.Visible = dataList.Count > 0;
            _noneTxt.Visible = dataList.Count == 0;
            if (_pb.self_guild_id != 0 && _pb.to_guild_id == 0)    //轮空状态
            {
                _noneTxt.CurrentText.text = MogoLanguageUtil.GetContent(111643);
            }
            else
            {
                _noneTxt.CurrentText.text = MogoLanguageUtil.GetContent(6236010);
            }
        }


        public override void OnClose()
        {
            _pb = null;
        }
    }
}
