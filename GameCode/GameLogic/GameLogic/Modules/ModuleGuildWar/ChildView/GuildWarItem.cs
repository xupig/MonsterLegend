﻿using Common.Base;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace ModuleGuildWar
{
    public class GuildWarItem : KContainer
    {
        private IconContainer _activityIcon;
        private StateText _timeTxt;
        private StateText _descTxt;
        private KButton _entryBtn;
        //private StateImage _selectImg;
        private KParticle _particle;
        private KDummyButton _dummyBtn;
        private Color _openTxtColor;

        private int _index;
        private bool _isOpen = false;

        protected override void Awake()
        {
            base.Awake();
            _activityIcon = AddChildComponent<IconContainer>("Container_icon");
            _timeTxt = GetChildComponent<StateText>("Label_txtShijian");
            _descTxt = GetChildComponent<StateText>("Label_txtYingdejihui");
            //_selectImg = GetChildComponent<StateImage>("Image_gonghuizhanxuanzhong");
            _entryBtn = GetChildComponent<KButton>("Button_jinru");
            _particle = AddChildComponent<KParticle>("fx_ui_gonghuizhan_zhuangtai");
            _particle.Position = new Vector3(0, -60, 0);
            _particle.Stop();
            _entryBtn.Label.ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);
            //_selectImg.Visible = false;
            _dummyBtn = gameObject.AddComponent<KDummyButton>();
            MaskEffectItem maskEffectItem = gameObject.AddComponent<MaskEffectItem>();
            maskEffectItem.Init(gameObject, CanShowEffect);
            _openTxtColor = _entryBtn.Label.CurrentText.color;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            _isOpen = false;
            _particle.Stop();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        public void RefreshContent(int index)
        {
            _index = index;
            _activityIcon.SetIcon(guild_match_open_ui_helper.GetIcon(_index + 1), OnIconLoaded);
            _descTxt.CurrentText.text = guild_match_open_ui_helper.GetDesc(_index + 1);
            _entryBtn.Label.ChangeAllStateText(guild_match_open_ui_helper.GetButtonText(_index + 1));
            int stage = guild_match_helper.GetBattleStageByIndex(index);
            _timeTxt.CurrentText.text = guild_match_helper.GetOpenContent(stage);
        }

        private void OnIconLoaded(GameObject go)
        {
            ImageWrapper[] imgList = go.GetComponentsInChildren<ImageWrapper>();
            for (int i = 0; i < imgList.Length; i++)
            {
                imgList[i].SetGray(!_isOpen ? 0 : 1);
            }
        }


        private void SetGray(bool isGray)
        {
            ImageWrapper[] imgList = gameObject.GetComponentsInChildren<ImageWrapper>();
            for (int i = 0; i < imgList.Length; i++)
            {
                imgList[i].SetGray(isGray ? 0 : 1);
            }
            Text[] textList = _entryBtn.Label.GetAllStateTextComponent();
            for (int i = 0; i < textList.Length; i++)
            {
                textList[i].color = isGray ? Color.black : _openTxtColor;
                textList[i].GetComponent<Outline>().enabled = isGray ? false : true;
                textList[i].GetComponent<Shadow>().enabled = isGray ? false : true;
            }
        }

        public void SetSelect(bool isOpen)
        {
            _isOpen = isOpen;
            //_selectImg.Visible = isSelect;
            if (isOpen)
            {
                _particle.Play(true);
            }
            else
            {
                _particle.Stop();
            }
            SetGray(!isOpen);
        }

        private bool CanShowEffect()
        {
            return _isOpen;
        }

        private void AddEventListener()
        {
            _entryBtn.onClick.AddListener(OnEntryBtnClick);
            _dummyBtn.onClick.AddListener(OnEntryBtnClick);
        }

        private void RemoveEventListener()
        {
            _entryBtn.onClick.RemoveListener(OnEntryBtnClick);
            _dummyBtn.onClick.RemoveListener(OnEntryBtnClick);
        }

        private void OnEntryBtnClick()
        {
            GotoActivity(_index);
        }

        public void GotoActivity(int index)
        {
            if (index == 0)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarTry);
            }
            else if (index == 1)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarSignUp);
            }
            else if (index == 2)
            {
                GuildWarManager.Instance.GetGuildWarDrawResult();
            }
            else if (index == 3)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarGroupMatch);
            }
            else if (index == 4)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarFinalMatch);
            }
            else
            {
                return;
            }
            UIManager.Instance.ClosePanel(PanelIdEnum.GuildWar);
        }

    }
}
