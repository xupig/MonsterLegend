﻿using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarTitleView : KContainer
    {
        private StateText _timeTxt;
        private List<StateImage> _imgListOne;
        private List<StateImage> _imgListTwo;

        protected override void Awake()
        {
            base.Awake();
            _timeTxt = GetChildComponent<StateText>("Label_txtShijian");
            _timeTxt.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
            _imgListOne = new List<StateImage>();
            _imgListOne.Add(GetChildComponent<StateImage>("Container_shuzi1/Image_ling"));
            _imgListOne.Add(GetChildComponent<StateImage>("Container_shuzi1/Image_yi"));
            _imgListOne.Add(GetChildComponent<StateImage>("Container_shuzi1/Image_er"));
            _imgListOne.Add(GetChildComponent<StateImage>("Container_shuzi1/Image_san"));
            _imgListOne.Add(GetChildComponent<StateImage>("Container_shuzi1/Image_si"));
            _imgListOne.Add(GetChildComponent<StateImage>("Container_shuzi1/Image_wu"));
            _imgListOne.Add(GetChildComponent<StateImage>("Container_shuzi1/Image_liu"));
            _imgListOne.Add(GetChildComponent<StateImage>("Container_shuzi1/Image_qi"));
            _imgListOne.Add(GetChildComponent<StateImage>("Container_shuzi1/Image_ba"));
            _imgListOne.Add(GetChildComponent<StateImage>("Container_shuzi1/Image_jiu"));
            _imgListTwo = new List<StateImage>();
            _imgListTwo.Add(GetChildComponent<StateImage>("Container_shuzi2/Image_ling"));
            _imgListTwo.Add(GetChildComponent<StateImage>("Container_shuzi2/Image_yi"));
            _imgListTwo.Add(GetChildComponent<StateImage>("Container_shuzi2/Image_er"));
            _imgListTwo.Add(GetChildComponent<StateImage>("Container_shuzi2/Image_san"));
            _imgListTwo.Add(GetChildComponent<StateImage>("Container_shuzi2/Image_si"));
            _imgListTwo.Add(GetChildComponent<StateImage>("Container_shuzi2/Image_wu"));
            _imgListTwo.Add(GetChildComponent<StateImage>("Container_shuzi2/Image_liu"));
            _imgListTwo.Add(GetChildComponent<StateImage>("Container_shuzi2/Image_qi"));
            _imgListTwo.Add(GetChildComponent<StateImage>("Container_shuzi2/Image_ba"));
            _imgListTwo.Add(GetChildComponent<StateImage>("Container_shuzi2/Image_jiu"));
            SetSeason(1);
        }

        public void SetSeason(int num)
        {
            if (num == 0) num = 1;
            int one = num / 10;
            int two = num % 10;
            for (int i = 0; i < 10; i++)
            {
                _imgListOne[i].Visible = (i == one);
                _imgListTwo[i].Visible = (i == two);
            }
            _timeTxt.CurrentText.text = guild_match_open_helper.GetOpenDateContent(num);
        }

    }
}
