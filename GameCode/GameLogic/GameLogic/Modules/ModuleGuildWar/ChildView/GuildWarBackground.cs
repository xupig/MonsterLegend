﻿using Common.Utils;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuildWar
{
    public class GuildWarBackground : KContainer
    {
        protected override void Awake()
        {
            MogoAtlasUtils.AddSprite(GetChild("Container_bg0"), "gonghuizhanbaoming0");
            MogoAtlasUtils.AddSprite(GetChild("Container_bg1"), "gonghuizhanbaoming1");
            MogoAtlasUtils.AddSprite(GetChild("Container_bg2"), "gonghuizhanbaoming2");
            MogoAtlasUtils.AddSprite(GetChild("Container_bg3"), "gonghuizhanbaoming3");
        }
    }
}
