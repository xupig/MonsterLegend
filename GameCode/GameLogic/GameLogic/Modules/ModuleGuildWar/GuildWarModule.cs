﻿using Common.Base;

namespace ModuleGuildWar
{
    public class GuildWarModule : BaseModule
    {

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            RegisterPopPanelBunchPanel(PanelIdEnum.GuildWar, PanelIdEnum.Activity);
            RegisterPopPanelBunchPanel(PanelIdEnum.GuildWar, PanelIdEnum.GuildInformation);
            AddPanel(PanelIdEnum.GuildWar, "Container_GuildWarPanel", MogoUILayer.LayerUIPopPanel, "ModuleGuildWar.GuildWarPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.GuildWarRankingTips, "Container_GuildWarRankingTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleGuildWar.GuildWarRankingTipsPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.GuildWarTry, "Container_GuildWarTryPanel", MogoUILayer.LayerUIPanel, "ModuleGuildWar.GuildWarTryPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.GuildWarTryTips, "Container_GuildTryRecordTipsPanel", MogoUILayer.LayerUIToolTip, "ModuleGuildWar.GuildWarTryRecordTipsPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.GuildWarFlow, "Container_GuildWarFlowPanel", MogoUILayer.LayerUIPanel, "ModuleGuildWar.GuildWarFlowPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.GuildWarResult, "Container_GuildWarResultPanel", MogoUILayer.LayerUIPanel, "ModuleGuildWar.GuildWarResultPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.GuildWarSignUp, "Container_GuildWarSignUpPanel", MogoUILayer.LayerUIPanel, "ModuleGuildWar.GuildWarSignUpPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.GuildWarReward, "Container_GuildWarRewardPanel", MogoUILayer.LayerUIPanel, "ModuleGuildWar.GuildWarRewardPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.GuildWarAssignedPosition, "Container_GuildWarAssignedPositionPanel", MogoUILayer.LayerUIPanel, "ModuleGuildWar.GuildWarAssignedPositionPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.GuildWarResultRecord, "Container_GuildWarResultRecordPanel", MogoUILayer.LayerUIPanel, "ModuleGuildWar.GuildWarResultRecordPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.GuildWarDraw, "Container_GuildWarDrawPanel", MogoUILayer.LayerUIPanel, "ModuleGuildWar.GuildWarDrawPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.GuildWarMainUI, "Container_GuildWarMainUIPanel", MogoUILayer.LayerUnderPanel, "ModuleGuildWar.GuildWarMainUIPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide, false);
            AddPanel(PanelIdEnum.GuildWarGroupMatch, "Container_GuildWarGroupMatchPanel", MogoUILayer.LayerUIPanel, "ModuleGuildWar.GuildWarGroupMatchPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.GuildWarFinalMatch, "Container_GuildWarFinalMatchPanel", MogoUILayer.LayerUIPanel, "ModuleGuildWar.GuildWarFinalMatchPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.GuildWarBattlePreview, "Container_GuildWarBattlePreviewPanel", MogoUILayer.LayerUIPanel, "ModuleGuildWar.GuildWarBattlePreviewPanel", BlurUnderlay.Have, HideMainUI.Hide);
            
        }
    }
}