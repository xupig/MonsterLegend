﻿using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarSignUpItem : KList.KListItemBase
    {
        private StateText _nameTxt;
        private StateText _rankTxt;
        private StateText _levelTxt;
        private StateText _numTxt;
        private StateText _scoreTxt;

        private PbGuildBattleSignupedInfo _data;

        protected override void Awake()
        {
            _nameTxt = GetChildComponent<StateText>("Label_txtGonghui");
            _rankTxt = GetChildComponent<StateText>("Label_txtPaiming");
            _levelTxt = GetChildComponent<StateText>("Label_txtDengji");
            _numTxt = GetChildComponent<StateText>("Label_txtRenshu");
            _scoreTxt = GetChildComponent<StateText>("Label_txtJifen");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbGuildBattleSignupedInfo;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            _nameTxt.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.name_bytes);
            _rankTxt.CurrentText.text = _data.index.ToString();
            _levelTxt.CurrentText.text = _data.level.ToString();
            _numTxt.CurrentText.text = _data.count.ToString();
            _scoreTxt.CurrentText.text = _data.week_point.ToString();
        }

        public override void Dispose()
        {
            _data = null;
        }
    }
}
