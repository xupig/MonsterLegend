﻿using Common.Base;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarSignUpPanel : BasePanel
    {
        private KButton _signUpBtn;
        private KButton _rewardBtn;
        private KButton _flowBtn;
        private StateText _timeTxt;
        private StateText _descTxt;
        private StateText _notSignUpTxt;
        private KScrollView _scrollView;
        private KList _list;

        private List<PbGuildBattleSignupedInfo> _dataList = new List<PbGuildBattleSignupedInfo>();

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildWarSignUp; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            AddChildComponent<GuildWarBackground>("Container_bg");
            _signUpBtn = GetChildComponent<KButton>("Button_baoming");
            _rewardBtn = GetChildComponent<KButton>("Button_jiangliyulan");
            _flowBtn = GetChildComponent<KButton>("Button_chakanliucheng");
            _timeTxt = GetChildComponent<StateText>("Label_txtShijian");
            _descTxt = GetChildComponent<StateText>("Container_biaoti/Label_txtTishi");
            _notSignUpTxt = GetChildComponent<StateText>("Label_txtzanwubaoming");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_zhanlibang");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _signUpBtn.Label.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
        }

        protected override void OnCloseBtnClick()
        {
            base.OnCloseBtnClick();
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWar);
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            RefreshContent();
            OnReRequest(_scrollView.ScrollRect);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void RefreshContent()
        {
            List<int> openDayList = guild_match_helper.GetOpenDay(public_config.GUILD_BATTLE_STATE_TESTING);
            string start = MogoLanguageUtil.GetContent(102514 + openDayList[0]);
            string end = MogoLanguageUtil.GetContent(102514 + openDayList[openDayList.Count - 1]);
            int position = guild_rights_helper.GetGuildRightsPositionById(GuildRightId.guildwarSignUp);
            string name = MogoLanguageUtil.GetContent(74626 + (int)position - 1);
            _timeTxt.CurrentText.text = string.Concat(guild_match_open_helper.GetOpenDateContent((int)PlayerDataManager.Instance.GuildWarData.Stage.battle_id), " ", MogoLanguageUtil.GetContent(111559, start, end, name));
            _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(111597, guild_match_helper.GetEnterMatchGuildCount((int)PlayerDataManager.Instance.GuildWarData.Stage.format));
            _notSignUpTxt.Visible = false;
            _signUpBtn.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(111644));
        }

        private void AddEventListener()
        {
            _signUpBtn.onClick.AddListener(OnSignUpBtnClick);
            _rewardBtn.onClick.AddListener(OnRewardBtnClick);
            _flowBtn.onClick.AddListener(OnFlowBtnClick);
            _scrollView.ScrollRect.onReRequest.AddListener(OnReRequest);
            _scrollView.ScrollRect.onRequestNext.AddListener(OnRequestNext);
            EventDispatcher.AddEventListener<PbGuildBattleSignupedInfoList>(GuildWarEvents.Refresh_Guild_War_Sign_List, RefreshListContent);
            EventDispatcher.AddEventListener(GuildWarEvents.Refresh_Guild_Sign_Up_Success, OnSignUpSuccess);
        }

        private void RemoveEventListener()
        {
            _signUpBtn.onClick.RemoveListener(OnSignUpBtnClick);
            _rewardBtn.onClick.RemoveListener(OnRewardBtnClick);
            _flowBtn.onClick.RemoveListener(OnFlowBtnClick);
            _scrollView.ScrollRect.onReRequest.RemoveListener(OnReRequest);
            _scrollView.ScrollRect.onRequestNext.RemoveListener(OnRequestNext);
            EventDispatcher.RemoveEventListener<PbGuildBattleSignupedInfoList>(GuildWarEvents.Refresh_Guild_War_Sign_List, RefreshListContent);
            EventDispatcher.RemoveEventListener(GuildWarEvents.Refresh_Guild_Sign_Up_Success, OnSignUpSuccess);
        }

        private void RefreshListContent(PbGuildBattleSignupedInfoList list)
        {
            _dataList.AddRange(list.list);
            if (_dataList.Count == 0)
            {
                _notSignUpTxt.Visible = true;
            }
            else
            {
                _notSignUpTxt.Visible = false;
            }
            _scrollView.ScrollRect.StopMovement();
            _list.SetDataList<GuildWarSignUpItem>(_dataList, 4);
            if (list.is_signuped == 0)
            {
                _signUpBtn.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(111644));
            }
            else
            {
                _signUpBtn.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(111645));
            }
        }

        private void OnRequestNext(KScrollRect arg0)
        {
            GuildWarManager.Instance.GetSignUpInfo(_dataList.Count + 1, 10);
        }

        private void OnReRequest(KScrollRect arg0)
        {
            _dataList.Clear();
            GuildWarManager.Instance.GetSignUpInfo(1, 10);
        }

        private void OnFlowBtnClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarFlow);
        }

        private void OnRewardBtnClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarReward, GuildWarRewardType.GroupMatch);
        }

        private void OnSignUpBtnClick()
        {
            if (guild_rights_helper.CheckHasRights(GuildRightId.guildwarSignUp, 111567))
            {
                GuildWarManager.Instance.SignUpGuildWar();
            }
        }

        private void OnSignUpSuccess()
        {
            OnReRequest(_scrollView.ScrollRect);
        }

    }
}