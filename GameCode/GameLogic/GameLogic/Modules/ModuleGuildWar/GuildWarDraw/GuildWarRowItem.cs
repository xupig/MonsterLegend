﻿using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarRowItem : KList.KListItemBase
    {
        private GuildWarDrawPanel.RowData _rowData;
        private KList _list;

        protected override void Awake()
        {
            _list = GetChildComponent<KList>("List_columnContent");
            _list.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _list.SetGap(0, 4);
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                GuildWarDrawPanel.RowData rowData = value as GuildWarDrawPanel.RowData;
                _rowData = rowData;
                List<string> columnList = new List<string>();
                for(int i = 0;i < rowData.NameList.Count;i++)
                {
                    string name = rowData.NameList[i];
                    if(name == string.Empty)
                    {
                        name = GuildWarDrawUtils.GetSeatName(i + 1, Index + 1);
                    }
                    columnList.Add(name);
                }
                _list.SetDataList<ColumnItem>(columnList);
            }
        }

        private bool _playingAnimator = false;
        private List<ColumnItem> _tempList = new List<ColumnItem>();
        private int _listCount;
        private int _currentIndex;
        private int _currentInterval;
        private int _intervalTimes;
        private int _updateTimes;
        private int _finalIndex;
        public void DoTween(int myRowIndex, int myGroupIndex)
        {
            _tempList.Clear();
            List<KList.KListItemBase> itemList = _list.ItemList;
            string myGuildName = PlayerDataManager.Instance.GuildData.MyGuildData.GuildName;
            for (int i = 0; i < itemList.Count; i++)
            {
                string guildName = _rowData.NameList[i];
                //Debug.LogError(string.Format("i:{0}  guildName:{1}", i, guildName));
                if (guildName == string.Empty || string.Equals(guildName, myGuildName))
                {
                    ColumnItem item = itemList[i] as ColumnItem;
                    item.SetName(GuildWarDrawUtils.GetSeatName(item.Index + 1, Index + 1));
                    item.HideFrame();
                    _tempList.Add(item);
                }
            }
            _listCount = _tempList.Count;
            _currentIndex = 0;
            _currentInterval = 1;
            _intervalTimes = 0;
            _updateTimes = 0;
            _finalIndex = myGroupIndex;
            _playingAnimator = true;
        }

        void Update()
        {
            if (_playingAnimator)
            {
                if (_intervalTimes % _currentInterval == 0)
                {
                    _tempList[_currentIndex % _listCount].HideFrame();
                    _currentIndex++;
                    _tempList[_currentIndex % _listCount].ShowFrame();
                    if (_updateTimes > 10)
                    {
                        if (_tempList.Count == 4)
                        {
                            _currentInterval += 1;
                        }
                        else if (_tempList.Count == 3)
                        {
                            _currentInterval += 2;
                        }
                        else if (_tempList.Count == 2)
                        {
                            _currentInterval += 3;
                        }
                        else if (_tempList.Count == 1)
                        {
                            _currentInterval += 10;
                        }
                    }
                    if (_currentInterval > 10 && _tempList[_currentIndex % _listCount].Index == _finalIndex)
                    {
                        _playingAnimator = false;
                        TimerHeap.AddTimer(500, 0, OnDrawTweenEnd);
                    }
                    _intervalTimes = 1;
                }
                _updateTimes++;
                _intervalTimes++;
            }
        }

        private void OnDrawTweenEnd()
        {
            EventDispatcher.TriggerEvent(GuildWarEvents.Draw_Tween_End);
        }

        public void ShowMyGuildFrame(int myGroupIndex)
        {
            ColumnItem item = _list.ItemList[myGroupIndex] as ColumnItem;
            item.ShowFrame();
        }

        public class ColumnItem : KList.KListItemBase
        {
            private StateImage _imageFrame;
            private StateText _textName;

            protected override void Awake()
            {
                _imageFrame = GetChildComponent<StateImage>("ScaleImage_sharedGrid1Bg");
                _textName = GetChildComponent<StateText>("Label_txtName");
                HideFrame();
            }

            public override void Dispose()
            {
            }

            public override object Data
            {
                set
                {
                    _textName.CurrentText.text = value as String;
                    HideFrame();
                }
            }

            public void ShowFrame()
            {
                _imageFrame.Alpha = 1f;
            }

            public void HideFrame()
            {
                _imageFrame.Alpha = 0f;
            }

            public void SetName(string name)
            {
                _textName.CurrentText.text = name;
            }
        }
    }
}
