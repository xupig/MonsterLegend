﻿using Common.Data;
using Common.Utils;
using GameData;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public static class GuildWarDrawUtils
    {
        public static string GetSeatName(int group, int row)
        {
            return string.Concat(GetGroupName(group), row);
        }

        public static string GetGroupName(int group)
        {
            string groupName;
            if (group == 1)
            {
                groupName = "A";
            }
            else if (group == 2)
            {
                groupName = "B";
            }
            else if (group == 3)
            {
                groupName = "C";
            }
            else
            {
                groupName = "D";
            }
            return groupName;
        }

        private static Action _sureCallback;
        public static void SendDraw(int drawedTimes, Action sureCallback = null)
        {
            _sureCallback = sureCallback;
            if (drawedTimes == 0)
            {
                ConfirmAction();
                return;
            }
            BaseItemData baseItemData = price_list_helper.GetCost(PriceListId.guildWarDraw, (int)(drawedTimes + 1));
            MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(111653, baseItemData.StackCount, baseItemData.Name), ConfirmAction, CancelAction);
        }

        private static void ConfirmAction()
        {
            GuildWarManager.Instance.GuildWarDraw();
            if (_sureCallback != null)
            {
                _sureCallback.Invoke();
                _sureCallback = null;
            }
        }

        private static void CancelAction()
        {
            if (_sureCallback != null)
            {
                _sureCallback = null;
            }
        }
    }
}
