﻿using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarDrawAgainButton : KContainer
    {
        private KButton _btnDrawAgain;
        private StateText _textNum;
        private StateIcon _stateIcon;

        public KComponentEvent onClick = new KComponentEvent();

        protected override void Awake()
        {
            _btnDrawAgain = gameObject.GetComponent<KButton>();
            _textNum = GetChildComponent<StateText>("shuliang");
            _stateIcon = GetChildComponent<StateIcon>("stateIcon");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnDrawAgain.onClick.AddListener(OnClick);
        }

        private void RemoveListener()
        {
            _btnDrawAgain.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            onClick.Invoke();
        }

        public void Refresh(int times)
        {
            BaseItemData baseItemData = price_list_helper.GetCost(PriceListId.guildWarDraw, (int)(times + 1));
            _stateIcon.SetIcon(baseItemData.Icon);
            _textNum.ChangeAllStateText(baseItemData.StackCount.ToString());
        }
    }
}
