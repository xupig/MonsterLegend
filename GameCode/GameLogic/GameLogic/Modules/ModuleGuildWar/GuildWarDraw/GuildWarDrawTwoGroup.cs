﻿using Common.Utils;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuildWar
{
    public class GuildWarDrawTwoGroup : KContainer
    {
        private KList _rowList;
        private List<StateText> _positionList;

        protected override void Awake()
        {
            _rowList = GetChildComponent<KList>("List_rowContent");
            _rowList.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _rowList.SetGap(13, 0);
            _positionList = new List<StateText>();
            for (int i = 0; i < 4; i++)
            {
                _positionList.Add(GetChildComponent<StateText>(string.Format("Label_txtKenengxiwei{0}", i)));
            }

            InitTips();
        }

        private void InitTips()
        {
            GetChildComponent<StateText>("Label_txtTishi").CurrentText.text = MogoLanguageUtil.GetContent(6238020, 8);
        }

        public void Refresh(List<GuildWarDrawPanel.RowData> rowDataList, int myRowIndex, int myGroupIndex, bool doTween)
        {
            _rowList.SetDataList<GuildWarRowItem>(rowDataList);
            for (int i = 0; i < GuildWarDrawPanel.MaxRowNum; i++)
            {
                _positionList[i].Visible = false;
                if (i == myRowIndex)
                {
                    //TODO
                    //_positionList[i].Visible = true;
                    GuildWarRowItem item = _rowList.ItemList[i] as GuildWarRowItem;
                    if (doTween)
                    {
                        item.DoTween(myRowIndex, myGroupIndex);
                    }
                    else
                    {
                        item.ShowMyGuildFrame(myGroupIndex);
                    }
                    
                }
            }
        }
    }
}
