﻿using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarGroupMatchPreview : KContainer
    {
        private KDummyButton _btnClose;
        private KList _list;
        private StateText _textNoRecord;

        protected override void Awake()
        {
            _textNoRecord = GetChildComponent<StateText>("Label_txtMeiJiLu");
            _btnClose = AddChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
            _list = GetChildComponent<KList>("ScrollView_zhanlibang/mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _list.SetGap(-5, 0);

            InitLabel();
            AddListener();
        }

        private void InitLabel()
        {
            _textNoRecord.CurrentText.text = MogoLanguageUtil.GetContent(111629);
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(OnClose);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(OnClose);
        }

        private void OnClose()
        {
            Visible = false;
        }

        public void Refresh(PbGuildBattleDrawResultList pbGuildBattleDrawResultList)
        {
            if (pbGuildBattleDrawResultList.list.Count == 0)
            {
                _textNoRecord.Visible = true;
            }
            else
            {
                _textNoRecord.Visible = false;
            }
            _list.SetDataList<Item>(pbGuildBattleDrawResultList.list, 1);
        }

        public class Item : KList.KListItemBase
        {
            private StateText _textName;
            private StateText _textSeat;
            private RankContainer _rankContainer;

            protected override void Awake()
            {
                _textName = GetChildComponent<StateText>("Label_txtGonghui");
                _textSeat = GetChildComponent<StateText>("Label_txtxiwei");
                _rankContainer = AddChildComponent<RankContainer>("Container_mingci");
            }

            public override void Dispose()
            {
            }

            public override object Data
            {
                set
                {
                    uint guildFormat = PlayerDataManager.Instance.GuildWarData.Stage.format;
                    PbGuildBattleDrawResult pb = value as PbGuildBattleDrawResult;
                    _rankContainer.SetRank(Index + 1);
                    _textName.CurrentText.text = MogoProtoUtils.ParseByteArrToString(pb.name_bytes);

                    int rank = 0;
                    if (pb.pos.Count == 0)
                    {
                        Debug.LogError("服务器数据有误 count:" + pb.pos.Count);
                        MogoDebugUtils.Print(pb);
                    }
                    int guildIndex = (int)pb.pos[0];
                    
                    string posStr = string.Empty;
                    if (guildFormat == 1)
                    {
                        rank = Index / 4 + 1;
                        posStr = GuildWarDrawUtils.GetSeatName(guildIndex, rank);
                        for (int i = 1; i < pb.pos.Count; i++)
                        {
                            posStr = string.Concat(posStr, "/", GuildWarDrawUtils.GetSeatName((int)pb.pos[i], rank));
                        }
                    }
                    else if (guildFormat == 2)
                    {
                        rank = Index / 2 + 1;
                        posStr = GuildWarDrawUtils.GetSeatName(guildIndex, rank);
                        for (int i = 1; i < pb.pos.Count; i++)
                        {
                            posStr = string.Concat(posStr, "/", GuildWarDrawUtils.GetSeatName((int)pb.pos[i], rank));
                        }
                    }
                    else if (guildFormat == 3)
                    {
                        rank = Index % 4 + 1;
                        posStr = GuildWarDrawUtils.GetSeatName(Index < 4 ? 1 : 2, rank);
                    }
                    _textSeat.CurrentText.text = posStr;
                }
            }
        }
    }
}
