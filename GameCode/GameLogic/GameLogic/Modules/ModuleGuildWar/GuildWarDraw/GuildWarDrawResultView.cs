﻿using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarDrawResultView : KContainer
    {
        private int _drawedTimes;

        private GuildWarDrawAgainButton _btnDrawAgain;
        private KButton _btnSure;
        private KButton _btnClose;
        private StateText _textGroup;
        private StateText _textSeat;
        private StateText _textNoSameGroup;
        private GameObject _goSameGroup;
        private KList _listSameGroup;

        protected override void Awake()
        {
            _btnDrawAgain = AddChildComponent<GuildWarDrawAgainButton>("Button_chouqian");
            _btnSure = GetChildComponent<KButton>("Button_chongxinchouqian");
            _btnClose = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _textGroup = GetChildComponent<StateText>("Label_txtZubei");
            _textSeat = GetChildComponent<StateText>("Label_txtXiwei");
            _textNoSameGroup = GetChildComponent<StateText>("Label_txtGonghuimingcheng");
            _goSameGroup = GetChild("Container_tongzugonghui");
            InitList();

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void InitList()
        {
            _listSameGroup = GetChildComponent<KList>("Container_tongzugonghui/List_gonghuiliebiao");
            _listSameGroup.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _listSameGroup.SetGap(7, 0);
        }

        private void AddListener()
        {
            _btnSure.onClick.AddListener(OnClose);
            _btnClose.onClick.AddListener(OnClose);
            _btnDrawAgain.onClick.AddListener(OnDraw);
        }

        private void RemoveListener()
        {
            _btnSure.onClick.RemoveListener(OnClose);
            _btnClose.onClick.RemoveListener(OnClose);
            _btnDrawAgain.onClick.RemoveListener(OnDraw);
        }

        private void OnClose()
        {
            Visible = false;
        }

        private void OnDraw()
        {
            GuildWarDrawUtils.SendDraw(_drawedTimes, OnClose);
        }

        internal void Refresh(int _myGroupIndex, int _myRowIndex, int DrawedTimes, List<PbGuildBattleDrawInfo> list)
        {
            _drawedTimes = DrawedTimes;
            _textSeat.CurrentText.text = MogoLanguageUtil.GetContent(111651, GuildWarDrawUtils.GetSeatName(_myGroupIndex + 1, _myRowIndex + 1));
            _textGroup.CurrentText.text = MogoLanguageUtil.GetContent(111652, GuildWarDrawUtils.GetGroupName(_myGroupIndex + 1));
            if (list.Count != 0)
            {
                _goSameGroup.SetActive(true);
                _textNoSameGroup.Visible = false;
                List<string> wrapperList = GetWrapperList(list);
                _listSameGroup.SetDataList<GuildWarDrawResultItem>(wrapperList);
            }
            else
            {
                _textNoSameGroup.Visible = true;
                _goSameGroup.SetActive(false);
            }
            _btnDrawAgain.Refresh(DrawedTimes);
        }

        private List<string> GetWrapperList(List<PbGuildBattleDrawInfo> list)
        {
            List<string> guildList = new List<string>();
            for (int i = 0; i < list.Count; i++)
            {
                guildList.Add(string.Format("{0}    LV{1}", list[i].name, list[i].level));
            }
            return guildList;
        }
    }
}
