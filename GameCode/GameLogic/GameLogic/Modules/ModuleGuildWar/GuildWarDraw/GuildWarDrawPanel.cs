﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarDrawPanel : BasePanel
    {
        public const int InvalidIndex = -1;
        public const int MaxRowNum = 4;

        public class RowData
        {
            public List<string> NameList;
            public RowData(int groupNum)
            {
                NameList = new List<string>();
                for (int i = 0; i < groupNum; i++)
                {
                    NameList.Add(string.Empty);
                }
            }

            public void Clear()
            {
                for (int i = 0; i < NameList.Count; i++)
                {
                    NameList[i] = string.Empty;
                }
            }
        }

        private int _drawedTimes;
        private int _myRowIndex;
        private int _myGroupIndex;
        private List<RowData> _rowDataList;

        private int _groupNum
        {
            get
            {
                return PlayerDataManager.Instance.GuildWarData.Stage.format == 1 ? 4 : 2;
            }
        }

        private int _guildNum
        {
            get
            {
                return 4 * _groupNum;
            }
        }

        private GuildWarData _data
        {
            get
            {
                return PlayerDataManager.Instance.GuildWarData;
            }
        }

        private GuildWarDrawResultView _drawResult;
        private GuildWarGroupMatchPreview _preview;
        private KButton _btnDraw;
        private GuildWarDrawAgainButton _btnDrawAgain;
        private KButton _btnPreview;
        private KButton _btnReward;
        private KButton _btnFlow;
        private StateText _textRule;
        private StateText _textUnneedDraw;
        private GuildWarDrawTwoGroup _twoGroup;
        private GuildWarDrawFourGroup _fourGroup;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildWarDraw; }
        }

        protected override void Awake()
        {
            _drawResult = AddChildComponent<GuildWarDrawResultView>("Container_chouqianjieguotanchuang");
            _preview = AddChildComponent<GuildWarGroupMatchPreview>("Container_xiaotanchuang");
            _textRule = GetChildComponent<StateText>("Label_txtShijian");
            _twoGroup = AddChildComponent<GuildWarDrawTwoGroup>("Container_draw02");
            _fourGroup = AddChildComponent<GuildWarDrawFourGroup>("Container_draw");
            _textUnneedDraw = GetChildComponent<StateText>("Label_txtUnavailable");
            InitBtn();
            AddChildComponent<GuildWarBackground>("Container_bg");
            InitTips();

            AddListener();
        }

        private void InitBtn()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _btnDraw = GetChildComponent<KButton>("Button_chouqian");
            _btnDrawAgain = AddChildComponent<GuildWarDrawAgainButton>("Button_chongxinchouqian");
            _btnPreview = GetChildComponent<KButton>("Button_xiwei");
            _btnReward = GetChildComponent<KButton>("Button_jiangliyulan");
            _btnFlow = GetChildComponent<KButton>("Button_chakanliucheng");
        }

        protected override void OnCloseBtnClick()
        {
            base.OnCloseBtnClick();
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWar);
        }

        private void InitTips()
        {
            int position = guild_rights_helper.GetGuildRightsPositionById(GuildRightId.guildwarDraw);
            string positionStr = MogoLanguageUtil.GetContent(74626 + position - 1);
            _textRule.CurrentText.text = string.Concat(guild_match_open_helper.GetOpenDateContent((int)_data.Stage.battle_id), " ", guild_match_helper.GetOpenContent(public_config.GUILD_BATTLE_STATE_DRAWING), " ", MogoLanguageUtil.GetContent(111634, positionStr));
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnDraw.onClick.AddListener(OnDraw);
            _btnDrawAgain.onClick.AddListener(OnDrawAgain);
            _btnPreview.onClick.AddListener(OnShowPreview);
            _btnReward.onClick.AddListener(OnShowReward);
            _btnFlow.onClick.AddListener(OnShowFlow);
            EventDispatcher.AddEventListener<PbGuildBattleDrawInfoList>(GuildWarEvents.Draw, OnResponseDraw);
            EventDispatcher.AddEventListener(GuildWarEvents.Draw_Tween_End, OnDrawTweenEnd);
            EventDispatcher.AddEventListener<PbGuildBattleDrawResultList>(GuildWarEvents.Response_Draw_Info, OnResponseDrawInfo);
        }

        private void RemoveListener()
        {
            _btnDraw.onClick.RemoveListener(OnDraw);
            _btnDrawAgain.onClick.RemoveListener(OnDrawAgain);
            _btnPreview.onClick.RemoveListener(OnShowPreview);
            _btnReward.onClick.RemoveListener(OnShowReward);
            _btnFlow.onClick.RemoveListener(OnShowFlow);
            EventDispatcher.RemoveEventListener<PbGuildBattleDrawInfoList>(GuildWarEvents.Draw, OnResponseDraw);
            EventDispatcher.RemoveEventListener(GuildWarEvents.Draw_Tween_End, OnDrawTweenEnd);
            EventDispatcher.RemoveEventListener<PbGuildBattleDrawResultList>(GuildWarEvents.Response_Draw_Info, OnResponseDrawInfo);
        }

        private void OnDraw()
        {
            GuildWarDrawUtils.SendDraw(_drawedTimes);
        }

        private void OnDrawAgain()
        {
            GuildWarDrawUtils.SendDraw(_drawedTimes);
        }

        private void OnShowPreview()
        {
            GuildWarManager.Instance.GetGuildWarDrawInfo();
        }

        private void OnShowReward()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarReward, GuildWarRewardType.GroupMatch);
        }

        private void OnShowFlow()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarFlow);
        }

        private void OnDrawTweenEnd()
        {
            _drawResult.Visible = true;
            _drawResult.Refresh(_myGroupIndex, _myRowIndex, _drawedTimes, _pbGuildBattleDrawInfoList.list);
            Refresh(false);
        }

        private void OnResponseDrawInfo(PbGuildBattleDrawResultList pbGuildBattleDrawResultList)
        {
            _preview.Visible = true;
            _preview.Refresh(pbGuildBattleDrawResultList);
        }

        private PbGuildBattleDrawInfoList _pbGuildBattleDrawInfoList;
        private void OnResponseDraw(PbGuildBattleDrawInfoList pb)
        {
            //Debug.LogError("OnResponseDraw");
            _pbGuildBattleDrawInfoList = pb;
            FillGuildWarDrawResult(pb);
            Refresh(true);
        }

        public override void OnShow(object data)
        {
            FillGuildWarDrawResult(data as PbGuildBattleDrawResultInfoList);
            Refresh(false);
        }

        public override void SetData(object data)
        {
            //Debug.LogError("SetData:");
            //抽签协议返回前会收到所有公会信息的协议
            FillGuildWarDrawResult(data as PbGuildBattleDrawResultInfoList);
            RefreshRowList(false);
        }

        private void Refresh(bool doTween)
        {
            RefreshRowList(doTween);
            RefreshBtn(_drawedTimes);
        }

        private void RefreshRowList(bool doTween)
        {
            _twoGroup.Visible = false;
            _fourGroup.Visible = false;
            if (_groupNum == 2)
            {
                _twoGroup.Visible = true;
                _twoGroup.Refresh(_rowDataList, _myRowIndex, _myGroupIndex, doTween);
            }
            else
            {
                _fourGroup.Visible = true;
                _fourGroup.Refresh(_rowDataList, _myRowIndex, _myGroupIndex, doTween);
            }
        }

        private void RefreshBtn(int times)
        {
            _btnDraw.Visible = false;
            _btnDrawAgain.Visible = false;
            _textUnneedDraw.Visible = false;
            if (PlayerDataManager.Instance.GuildWarData.Stage.format == 3)
            {
                _textUnneedDraw.Visible = true;
            }
            else
            {
                if (times == 0)
                {
                    _btnDraw.Visible = true;
                }
                else
                {
                    _btnDrawAgain.Visible = true;
                    _btnDrawAgain.Refresh(_drawedTimes);
                }
            }
        }

        public override void OnClose()
        {
        }


        private void FillGuildWarDrawResult(PbGuildBattleDrawResultInfoList pb)
        {
            //MogoDebugUtils.Print(pb);
            //Debug.LogError("format:" + PlayerDataManager.Instance.GuildWarData.Stage.format);
            InitData();
            _drawedTimes = (int)pb.drawed_times;
            for (int i = 0; i < _groupNum; i++)
            {
                PbGuildBattleDrawResultInfo pbGuildBattleDrawResultInfo = pb.list[i];
                FillName((int)pbGuildBattleDrawResultInfo.group, pbGuildBattleDrawResultInfo);
            }
            SetMyGuildData();
        }

        private void InitData()
        {
            _myRowIndex = InvalidIndex;
            _myGroupIndex = InvalidIndex;
            _rowDataList = new List<RowData>();
            for (int i = 0; i < MaxRowNum; i++)
            {
                _rowDataList.Add(new RowData(_groupNum));
            }
        }

        private void FillName(int group, PbGuildBattleDrawResultInfo pbGuildBattleDrawResultInfo)
        {
            int groupIndex = group - 1;
            _rowDataList[0].NameList[groupIndex] = pbGuildBattleDrawResultInfo.guild1_name;
            _rowDataList[1].NameList[groupIndex] = pbGuildBattleDrawResultInfo.guild2_name;
            _rowDataList[2].NameList[groupIndex] = pbGuildBattleDrawResultInfo.guild3_name;
            _rowDataList[3].NameList[groupIndex] = pbGuildBattleDrawResultInfo.guild4_name;
        }

        private void SetMyGuildData()
        {
            string myGuildName = PlayerDataManager.Instance.GuildData.MyGuildData.GuildName;
            for (int i = 0; i < _rowDataList.Count; i++)
            {
                RowData row = _rowDataList[i];
                for (int j = 0; j < row.NameList.Count; j++)
                {
                    if (row.NameList[j] == myGuildName)
                    {
                        _myRowIndex = i;
                        _myGroupIndex = j;
                        break;
                    }
                }
            }
        }

        private void FillGuildWarDrawResult(PbGuildBattleDrawInfoList pb)
        {
            _drawedTimes = (int)pb.times;
            int rowIndex = (int)pb.level - 1;
            int groupIndex = (int)(pb.group - 1);
            _myRowIndex = rowIndex;
            _myGroupIndex = groupIndex;
            //因为抽签会返回当前所有公会的抽签情况，这里不需要再对其他数据进行处理
        }
    }
}
