﻿using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuildWar
{
    public class GuildWarDrawResultItem : KList.KListItemBase
    {
        private StateText _textGuild;

        protected override void Awake()
        {
            _textGuild = GetChildComponent<StateText>("Label_txtGonghuimingcheng");
        }

        public override void Dispose()
        {
        }

        public override object Data
        {
            set
            {
                _textGuild.CurrentText.text = value as String;
            }
        }
    }
}
