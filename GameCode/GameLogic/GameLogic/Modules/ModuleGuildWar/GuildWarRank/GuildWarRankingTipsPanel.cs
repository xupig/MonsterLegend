﻿using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;

namespace ModuleGuildWar
{
    public class GuildWarRankingTipsPanel : BasePanel
    {
        private StateText _titleTxt;
        private KScrollView _scrollView;
        private KList _list;
        private KDummyButton _dummyBtn;

        protected override void Awake()
        {
            _titleTxt = GetChildComponent<StateText>("Container_xiaotanchuang/Label_txtBiaoti");
            _scrollView = GetChildComponent<KScrollView>("Container_xiaotanchuang/ScrollView_zhanlibang");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _dummyBtn = AddChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildWarRankingTips; }
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            int season = (int)data;
            _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(111632, season);
            GuildWarManager.Instance.GetGuildWarRankList(season);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void RefreshList(PbGuildBattleResultInfoList list)
        {
            _list.SetDataList<GuildWarRankingItem>(list.list);
        }

        private void AddEventListener()
        {
            _dummyBtn.onClick.AddListener(OnDummyBtnClick);
            EventDispatcher.AddEventListener<PbGuildBattleResultInfoList>(GuildWarEvents.On_Get_Guild_War_Rank_List, RefreshList);
        }

        private void RemoveEventListener()
        {
            _dummyBtn.onClick.RemoveListener(OnDummyBtnClick);
            EventDispatcher.RemoveEventListener<PbGuildBattleResultInfoList>(GuildWarEvents.On_Get_Guild_War_Rank_List, RefreshList);
        }

        private void OnDummyBtnClick()
        {
            ClosePanel();
        }
    }
}