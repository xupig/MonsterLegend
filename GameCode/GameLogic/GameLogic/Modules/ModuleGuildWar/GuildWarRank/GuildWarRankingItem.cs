﻿using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuildWar
{
    public class GuildWarRankingItem : KList.KListItemBase
    {
        private StateText _nameTxt;
        private RankContainer _rankContainer;

        private PbGuildBattleResultInfo _data;

        protected override void Awake()
        {
            _nameTxt = GetChildComponent<StateText>("Label_txtGonghui");
            _rankContainer = AddChildComponent<RankContainer>("Container_mingci");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbGuildBattleResultInfo;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            RefreshContent();
        }

        private void RefreshContent()
        {
            _rankContainer.SetRank((int)_data.index);
            _nameTxt.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.guild_name_bytes);
        }

        public override void Dispose()
        {
            _data = null;
        }
    }
}
