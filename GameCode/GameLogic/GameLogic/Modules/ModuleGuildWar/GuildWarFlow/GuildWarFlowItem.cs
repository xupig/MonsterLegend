﻿using Common.ExtendComponent;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarFlowItem : KContainer
    {
        private StateText _descTxt;
        private StateText _titleTxt;
        private StateImage _selectImg;
        private IconContainer _icon;

        private int _index;

        protected override void Awake()
        {
            base.Awake();
            _descTxt = GetChildComponent<StateText>("Label_txtXinxi");
            _titleTxt = GetChildComponent<StateText>("Label_txtMingchen");
            _selectImg = GetChildComponent<StateImage>("ScaleImage_liuchengdi02");
            _icon = AddChildComponent<IconContainer>("Container_icon");
            _selectImg.Visible = false;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        public void RefreshContent(int index)
        {
            _index = index;
            _descTxt.CurrentText.text = guild_match_open_ui_helper.GetDesc(_index + 1);
            if (_icon != null)
            {
                _icon.SetIcon(guild_match_open_ui_helper.GetIcon(_index + 1));
            }
        }

        public void SetSelect(bool isSelect)
        {
            _selectImg.Visible = isSelect;

        }


    }
}
