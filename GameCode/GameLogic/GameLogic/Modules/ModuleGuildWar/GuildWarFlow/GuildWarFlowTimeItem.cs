﻿using Common.ExtendComponent;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarFlowTimeItem : KContainer
    {
        private StateText _timeTxt;
        private StateImage _selectImg;
        private StateImage _selectImgBg;

        private int _index;

        protected override void Awake()
        {
            base.Awake();
            _timeTxt = GetChildComponent<StateText>("Label_txtShijian");
            _selectImg = GetChildComponent<StateImage>("Image_jvxing");
            _selectImgBg = GetChildComponent<StateImage>("Image_jvxingdi");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        public void RefreshContent(int index)
        {
            _index = index;
            int stage = guild_match_helper.GetBattleStageByIndex(index);
            _timeTxt.CurrentText.text = guild_match_helper.GetOpenContent(stage);
        }

        public void SetSelect(bool isSelect)
        {
            _selectImg.Visible = isSelect;
            _selectImgBg.Visible = isSelect;
        }

    }
}
