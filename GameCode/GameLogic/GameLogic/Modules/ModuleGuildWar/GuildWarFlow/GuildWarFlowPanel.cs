﻿using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarFlowPanel : BasePanel
    {
        private List<GuildWarFlowItem> _itemList;
        private List<GuildWarFlowTimeItem> _timeItemList;
        private StateText _getDescTxt;
        private StateText _ungetDescTxt;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildWarFlow; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _itemList = new List<GuildWarFlowItem>();
            _itemList.Add(AddChildComponent<GuildWarFlowItem>("Container_liucheng/Container_shilian"));
            _itemList.Add(AddChildComponent<GuildWarFlowItem>("Container_liucheng/Container_baoming"));
            _itemList.Add(AddChildComponent<GuildWarFlowItem>("Container_liucheng/Container_chouqian"));
            _itemList.Add(AddChildComponent<GuildWarFlowItem>("Container_liucheng/Container_xiaozusai"));
            _itemList.Add(AddChildComponent<GuildWarFlowItem>("Container_liucheng/Container_taotaisai"));
            _itemList.Add(AddChildComponent<GuildWarFlowItem>("Container_liucheng/Container_youyisai1"));
            _itemList.Add(AddChildComponent<GuildWarFlowItem>("Container_liucheng/Container_youyisai2"));
            _timeItemList = new List<GuildWarFlowTimeItem>();
            _timeItemList.Add(AddChildComponent<GuildWarFlowTimeItem>("Container_liucheng/Container_shijian01"));
            _timeItemList.Add(AddChildComponent<GuildWarFlowTimeItem>("Container_liucheng/Container_shijian02"));
            _timeItemList.Add(AddChildComponent<GuildWarFlowTimeItem>("Container_liucheng/Container_shijian03"));
            _timeItemList.Add(AddChildComponent<GuildWarFlowTimeItem>("Container_liucheng/Container_shijian04"));
            _timeItemList.Add(AddChildComponent<GuildWarFlowTimeItem>("Container_liucheng/Container_shijian05"));
            _getDescTxt = GetChildComponent<StateText>("Container_liucheng/Label_txtHuode");
            _ungetDescTxt = GetChildComponent<StateText>("Container_liucheng/Label_txtWeihuode");
            _getDescTxt.CurrentText.text = MogoLanguageUtil.GetContent(111640);
            _ungetDescTxt.CurrentText.text = MogoLanguageUtil.GetContent(111641);
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            RefreshContent();
            RefreshGuildWarStage();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void RefreshContent()
        {
            for (int i = 0; i < _itemList.Count; i++)
            {
                _itemList[i].RefreshContent(i);
            }
            for (int i = 0; i < _timeItemList.Count; i++)
            {
                _timeItemList[i].RefreshContent(i);
            }
        }

        private void AddEventListener()
        {

        }

        private void RemoveEventListener()
        {

        }

        private void RefreshGuildWarStage()
        {
            PbGuildStage stage = PlayerDataManager.Instance.GuildWarData.Stage;
            RefreshItemImage((int)stage.stage);
        }

        private void RefreshItemImage(int stage)
        {
            for (int i = 0; i < _itemList.Count; i++)
            {
                int itemStage = (int)guild_match_helper.GetBattleStageByIndex(i);
                _itemList[i].SetSelect(itemStage == stage);
            }
            for (int i = 0; i < _timeItemList.Count; i++)
            {
                int itemStage = (int)guild_match_helper.GetBattleStageByIndex(i);
                _timeItemList[i].SetSelect(itemStage == stage);
            }
        }

    }
}