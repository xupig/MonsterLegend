﻿using Common.Base;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarPanel : BasePanel
    {
        private KScrollView _scrollView;
        private Locater _pageOne;
        private Locater _pageTwo;
        private GuildWarTitleView _titleView;
        private List<GuildWarItem> _itemList;
        private KButton _rewardBtn;
        private KButton _entryBtn;
        private PbGuildStage _stage;

        private const int FAST_ENTRY_INDEX = -1;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildWar; }
        }

        protected override void Awake()
        {
            _scrollView = GetChildComponent<KScrollView>("ScrollView_xianshihuodong");
            _scrollView.ScrollRect.vertical = false;
            _scrollView.ScrollRect.horizontal = true;
            _pageOne = AddChildComponent<Locater>("ScrollView_xianshihuodong/mask/content/Container_page1");
            _pageTwo = AddChildComponent<Locater>("ScrollView_xianshihuodong/mask/content/Container_page2");
            _pageTwo.X = _pageOne.X + _pageOne.Width + 2;
            //_scrollPage.TotalPage = 2;
            _titleView = AddChildComponent<GuildWarTitleView>("Container_gonghuizhanbiaoti");
            _itemList = new List<GuildWarItem>();
            _itemList.Add(AddChildComponent<GuildWarItem>("ScrollView_xianshihuodong/mask/content/Container_page1/Container_item01"));
            _itemList.Add(AddChildComponent<GuildWarItem>("ScrollView_xianshihuodong/mask/content/Container_page1/Container_item02"));
            _itemList.Add(AddChildComponent<GuildWarItem>("ScrollView_xianshihuodong/mask/content/Container_page1/Container_item03"));
            _itemList.Add(AddChildComponent<GuildWarItem>("ScrollView_xianshihuodong/mask/content/Container_page2/Container_item04"));
            _itemList.Add(AddChildComponent<GuildWarItem>("ScrollView_xianshihuodong/mask/content/Container_page2/Container_item05"));
            _rewardBtn = GetChildComponent<KButton>("Button_jiangliyulan");
            _entryBtn = GetChildComponent<KButton>("Button_jinru");
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            RefreshContent();
            if (data is int)
            {
                int index = (int)data - 1;
                if (index >= 0)
                {
                    _itemList[index].GotoActivity(index);
                }
            }
            GuildWarManager.Instance.GetGuildWarInfo();
        }

        public override void OnClose()
        {
            RemoveEventListener();
            _stage = null;
        }

        private void RefreshContent()
        {
            for (int i = 0; i < _itemList.Count; i++)
            {
                _itemList[i].RefreshContent(i);
            }
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<PbGuildStage>(GuildWarEvents.Refresh_Guild_War_Stage, RefreshGuildWarStage);
            _rewardBtn.onClick.AddListener(OnRewardBtnClick);
            _entryBtn.onClick.AddListener(OnEntryBtnClick);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<PbGuildStage>(GuildWarEvents.Refresh_Guild_War_Stage, RefreshGuildWarStage);
            _rewardBtn.onClick.RemoveListener(OnRewardBtnClick);
            _entryBtn.onClick.RemoveListener(OnEntryBtnClick);
        }

        private void RefreshGuildWarStage(PbGuildStage stage)
        {
            _stage = stage;
            _titleView.SetSeason((int)stage.battle_id);
            RefreshItemImage((int)stage.stage);
            int openIndex = GetOpenIndex((int)stage.stage);
            RecalculatePosition(openIndex);
        }

        private void RecalculatePosition(int index)
        {
            RectTransform maskRect = _scrollView.GetChildComponent<RectTransform>("mask");
            RectTransform contentRect = _scrollView.GetChildComponent<RectTransform>("mask/content");
            RectTransform itemRect = _itemList[index].transform as RectTransform;
            float parentOffset = 0;
            parentOffset = index < 3 ? _pageOne.X : _pageTwo.X;
            float x = (maskRect.sizeDelta.x / 2) - (itemRect.sizeDelta.x / 2) - (itemRect.anchoredPosition.x + parentOffset);
            x = Mathf.Clamp(x, maskRect.sizeDelta.x - contentRect.sizeDelta.x, 0);
            contentRect.anchoredPosition = new Vector2(x, contentRect.anchoredPosition.y);
        }

        private int GetOpenIndex(int stage)
        {
            for (int i = 0; i < _itemList.Count; i++)
            {
                int itemStage = (int)guild_match_helper.GetBattleStageByIndex(i);
                if (itemStage >= stage)
                {
                    return i;
                }
            }
            return 0;
        }

        private void RefreshItemImage(int stage)
        {
            for (int i = 0; i < _itemList.Count; i++)
            {
                int itemStage = (int)guild_match_helper.GetBattleStageByIndex(i);
                _itemList[i].SetSelect(itemStage == stage);
            }
        }

        private void OnRewardBtnClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarReward, GuildWarRewardType.FinalMatch);
        }

        private void OnEntryBtnClick()
        {
            int openIndex = GetOpenIndex((int)_stage.stage);
            _itemList[openIndex].GotoActivity(openIndex);
        }
    }
}