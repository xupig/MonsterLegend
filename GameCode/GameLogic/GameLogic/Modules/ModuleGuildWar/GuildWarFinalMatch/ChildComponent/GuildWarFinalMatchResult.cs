﻿using Common.Data;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuildWar
{
    public class GuildWarFinalMatchResult : KContainer
    {
        private StateImage _imageWin;
        private StateImage _imageFail;
        private StateText _textName;

        protected override void Awake()
        {
            _imageWin = GetChildComponent<StateImage>("Image_sheng");
            if (_imageWin == null)
            {
                _imageWin = GetChildComponent<StateImage>("Image_shengli");
            }
            _imageFail = GetChildComponent<StateImage>("Image_fu");
            _textName = GetChildComponent<StateText>("Label_txtgonghuiming");
        }

        public void Refresh(CopyResult result, string guildName)
        {
            if (string.IsNullOrEmpty(guildName))
            {
                guildName = "--";
            }
            _textName.CurrentText.text = guildName;
            _imageWin.Visible = false;
            SetImageFailVisible(false);
            switch (result)
            {
                case CopyResult.Win:
                    _imageWin.Visible = true;
                    break;
                case CopyResult.Lose:
                    SetImageFailVisible(true);
                    break;
            }
        }

        private void SetImageFailVisible(bool value)
        {
            if (_imageFail != null)
            {
                _imageFail.Visible = value;
            }
        }

        public void Clear()
        {
            _textName.CurrentText.text = "--";
            _imageWin.Visible = false;
            SetImageFailVisible(false);
        }
    }
}
