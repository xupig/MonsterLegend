﻿using Common.ExtendComponent;
using Game.UI;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarFinalMatchButton : KContainer
    {
        private KButton _btn;
        private StateImage _imageNotSelect;
        private StateImage _imageSelected;
        private StateImage _imageUnbegin;
        private StateText _textLabel;
        private KParticle _particle;

        public KComponentEvent onClick = new KComponentEvent();

        protected override void Awake()
        {
            _btn = gameObject.GetComponent<KButton>();
            _imageSelected = GetChildComponent<StateImage>("image0");
            _imageNotSelect = GetChildComponent<StateImage>("image1");
            _imageUnbegin = GetChildComponent<StateImage>("image2");
            _imageUnbegin.GetComponentInChildren<ImageWrapper>().SetGray(0);
            _textLabel = GetChildComponent<StateText>("label");
            _particle = AddChildComponent<KParticle>("fx_ui_gonghuizhan_jinxingzhong");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btn.onClick.AddListener(OnClick);
        }

        private void RemoveListener()
        {
            _btn.onClick.RemoveListener(OnClick);
        }

        public void SetLabel(string content)
        {
            _textLabel.CurrentText.text = content;
        }

        public void AppendLabel(string appendContent)
        {
            _textLabel.CurrentText.text = string.Concat(_textLabel.CurrentText.text, appendContent);
        }

        public void Reset()
        {
            _imageSelected.Visible = false;
            _imageNotSelect.Visible = false;
            _imageUnbegin.Visible = false;
            _btn.interactable = false;
            _particle.Stop();
        }

        private void OnClick()
        {
            onClick.Invoke();
        }

        public void SetState(bool isCurrentRound, bool isSelectedRound, bool isBegan)
        {
            if (isCurrentRound)
            {
                _particle.Play(true);
            }
            if (isBegan)
            {
                if (isSelectedRound)
                {
                    _imageSelected.Visible = true;
                }
                else
                {
                    _imageNotSelect.Visible = true;
                }
                _btn.interactable = true;
            }
            else
            {
                if (isSelectedRound)
                {
                    _imageSelected.Visible = true;
                }
                else
                {
                    _imageUnbegin.Visible = true;
                }
            }
            
        }
    }
}
