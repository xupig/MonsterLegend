﻿using Common.Data;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarQuarterFinalView : KContainer
    {
        private StateText _textTitle;
        private List<QuarterFinalViewItem> _itemList;

        protected override void Awake()
        {
            _textTitle = GetChildComponent<StateText>("Label_txtzhongBiaoti");

            _itemList = new List<QuarterFinalViewItem>();
            _itemList.Add(AddChildComponent<QuarterFinalViewItem>("Container_item0"));
            _itemList.Add(AddChildComponent<QuarterFinalViewItem>("Container_item2"));
            _itemList.Add(AddChildComponent<QuarterFinalViewItem>("Container_item1"));
            _itemList.Add(AddChildComponent<QuarterFinalViewItem>("Container_item3"));
        }

        public void SetData(GuildWarFinalMatchPanel.Tab tab, List<PbGuildBattleFinalMatchResult> list, bool isQuarterFinalBegin)
        {
            for (int i = 0; i < 4; i++)
            {
                _itemList[i].SetData(list[i], tab, i, isQuarterFinalBegin);
            }
            _textTitle.CurrentText.text = MogoLanguageUtil.GetContent(111654, tab == GuildWarFinalMatchPanel.Tab.elite ? MogoLanguageUtil.GetContent(111655) : MogoLanguageUtil.GetContent(111656));
        }

        public class QuarterFinalViewItem : KContainer
        {
            private PbGuildBattleFinalMatchResult _pb;

            private StateText _textTitle;
            private StateText _textDesc;
            private KButton _btnDetail;
            private GuildWarFinalMatchResult _leftResult;
            private GuildWarFinalMatchResult _rightResult;
            private GameObject _goBegin;

            protected override void Awake()
            {
                _textTitle = GetChildComponent<StateText>("Container_txtTitle/Label_text01");
                _textDesc = GetChildComponent<StateText>("Label_txtDaixiaozusai");
                _goBegin = GetChild("Container_duizhanxinxi");
                _btnDetail = GetChildComponent<KButton>("Container_duizhanxinxi/Button_benfangzhiwei");
                _leftResult = AddChildComponent<GuildWarFinalMatchResult>("Container_duizhanxinxi/Container_zuo");
                _rightResult = AddChildComponent<GuildWarFinalMatchResult>("Container_duizhanxinxi/Container_you");

                AddListener();
            }

            protected override void OnDestroy()
            {
                RemoveListener();
            }

            private void AddListener()
            {
                _btnDetail.onClick.AddListener(OnDetail);
            }

            private void RemoveListener()
            {
                _btnDetail.onClick.RemoveListener(OnDetail);
            }

            private void OnDetail()
            {
                GuildWarManager.Instance.GetGuildWarResultRecord(_pb.log_dbid, _pb.guild_dbid1);
            }

            public void SetData(PbGuildBattleFinalMatchResult pb, GuildWarFinalMatchPanel.Tab tab, int index, bool isQuarterFinalBegin)
            {
                _pb = pb;
                RefreshTitle(tab, index);
                RefreshResult(pb, isQuarterFinalBegin);
            }

            private void RefreshTitle(GuildWarFinalMatchPanel.Tab tab, int index)
            {
                uint guildFormat = PlayerDataManager.Instance.GuildWarData.Stage.format;
                string content = string.Empty;
                if (guildFormat == 1)
                {
                    if (tab == GuildWarFinalMatchPanel.Tab.king)
                    {
                        if (index == 0)
                        {
                            content = MogoLanguageUtil.GetContent(111676, "A", "1", "B", "2");
                        }
                        else if (index == 1)
                        {
                            content = MogoLanguageUtil.GetContent(111676, "C", "1", "D", "2");
                        }
                        else if (index == 2)
                        {
                            content = MogoLanguageUtil.GetContent(111676, "B", "1", "A", "2");
                        }
                        else if (index == 3)
                        {
                            content = MogoLanguageUtil.GetContent(111676, "D", "1", "C", "2");
                        }
                    }
                    else
                    {
                        if (index == 0)
                        {
                            content = MogoLanguageUtil.GetContent(111676, "A", "3", "B", "4");
                        }
                        else if (index == 1)
                        {
                            content = MogoLanguageUtil.GetContent(111676, "C", "3", "D", "4");
                        }
                        else if (index == 2)
                        {
                            content = MogoLanguageUtil.GetContent(111676, "B", "3", "A", "4");
                        }
                        else if (index == 3)
                        {
                            content = MogoLanguageUtil.GetContent(111676, "D", "3", "C", "4");
                        }
                    }
                }
                else if (guildFormat == 2)
                {
                    if (index == 0)
                    {
                        content = MogoLanguageUtil.GetContent(111676, "A", "1", "B", "4");
                    }
                    else if (index == 1)
                    {
                        content = MogoLanguageUtil.GetContent(111676, "B", "2", "A", "3");
                    }
                    else if (index == 2)
                    {
                        content = MogoLanguageUtil.GetContent(111676, "B", "1", "A", "4");
                    }
                    else if (index == 3)
                    {
                        content = MogoLanguageUtil.GetContent(111676, "A", "2", "B", "3");
                    }
                }
                else if (guildFormat == 3)
                {
                    if (index == 0)
                    {
                        content = MogoLanguageUtil.GetContent(111676, "A", "1", "B", "4");
                    }
                    else if (index == 1)
                    {
                        content = MogoLanguageUtil.GetContent(111676, "A", "4", "B", "1");
                    }
                    else if (index == 2)
                    {
                        content = MogoLanguageUtil.GetContent(111676, "A", "2", "B", "3");
                    }
                    else if (index == 3)
                    {
                        content = MogoLanguageUtil.GetContent(111676, "A", "3", "B", "2");
                    }
                }
                _textTitle.CurrentText.text = content;
            }

            private void RefreshResult(PbGuildBattleFinalMatchResult pb, bool isQuarterFinalBegin)
            {
                if (isQuarterFinalBegin)
                {
                    _goBegin.SetActive(true);
                    _textDesc.Visible = false;
                    _leftResult.Clear();
                    _rightResult.Clear();
                    _btnDetail.Visible = false;
                    if (pb != null)
                    {
                        if (pb.win_dbid == 0)
                        {
                            _leftResult.Refresh(CopyResult.NoResult, pb.name1);
                            _rightResult.Refresh(CopyResult.NoResult, pb.name2);
                        }
                        else
                        {
                            _leftResult.Refresh(pb.guild_dbid1 == pb.win_dbid ? CopyResult.Win : CopyResult.Lose, pb.name1);
                            _rightResult.Refresh(pb.guild_dbid2 == pb.win_dbid ? CopyResult.Win : CopyResult.Lose, pb.name2);
                            _btnDetail.Visible = true;
                        }
                    }
                }
                else
                {
                    _goBegin.SetActive(false);
                    _textDesc.Visible = true;
                }
            }
        }
    }
}
