﻿using Common.Data;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarFinalView : KContainer
    {
        private List<FinalViewItem> _itemList;
        private StateText _textTitle;

        protected override void Awake()
        {
            _itemList = new List<FinalViewItem>();
            for (int i = 0; i < 4; i++)
            {
                FinalViewItem item = AddChildComponent<FinalViewItem>(string.Concat("Container_item", i));
                _itemList.Add(item);
            }
            GetChildComponent<StateText>("Container_item0/Label_txtChengji").Visible = false;
            _textTitle = GetChildComponent<StateText>("Label_txtBiaoti");
        }

        public void SetData(GuildWarFinalMatchPanel.Tab index, List<PbGuildBattleFinalMatchResult> list)
        {
            for (int i = 0; i < 4; i++)
            {
                _itemList[i].SetData(list[i]);
            }
        }

        private class FinalViewItem : KContainer
        {
            private PbGuildBattleFinalMatchResult _pb;

            private KButton _btnDetail;
            private GuildWarFinalMatchResult _leftResult;
            private GuildWarFinalMatchResult _rightResult;

            protected override void Awake()
            {
                _btnDetail = GetChildComponent<KButton>("Button_benfangzhiwei");
                _leftResult = AddChildComponent<GuildWarFinalMatchResult>("Container_zuo");
                _rightResult = AddChildComponent<GuildWarFinalMatchResult>("Container_you");

                AddListener();
            }

            protected override void OnDestroy()
            {
                RemoveListener();
            }

            private void AddListener()
            {
                _btnDetail.onClick.AddListener(OnDetail);
            }

            private void RemoveListener()
            {
                _btnDetail.onClick.RemoveListener(OnDetail);
            }

            private void OnDetail()
            {
                GuildWarManager.Instance.GetGuildWarResultRecord(_pb.log_dbid, _pb.guild_dbid1);
            }

            public void SetData(PbGuildBattleFinalMatchResult pb)
            {
                _pb = pb;
                _leftResult.Clear();
                _rightResult.Clear();
                _btnDetail.Visible = false;
                if (pb != null)
                {
                    if (pb.win_dbid == 0)
                    {
                        _leftResult.Refresh(CopyResult.NoResult, pb.name1);
                        _rightResult.Refresh(CopyResult.NoResult, pb.name2);
                    }
                    else
                    {
                        _leftResult.Refresh(pb.guild_dbid1 == pb.win_dbid ? CopyResult.Win : CopyResult.Lose, pb.name1);
                        _rightResult.Refresh(pb.guild_dbid2 == pb.win_dbid ? CopyResult.Win : CopyResult.Lose, pb.name2);
                    }
                }
            }
        }
    }
}
