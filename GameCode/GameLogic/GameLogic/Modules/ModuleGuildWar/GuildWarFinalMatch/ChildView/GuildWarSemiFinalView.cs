﻿using Common.Data;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarSemiFinalView : KContainer
    {
        private StateText _textLeftTitle;
        private StateText _textRightTitle;
        private List<SemiFinalItem> _itemList;

        protected override void Awake()
        {
            _textLeftTitle = GetChildComponent<StateText>("Label_txtzuoBiaoti");
            _textRightTitle = GetChildComponent<StateText>("Label_txtyouBiaoti");
            _itemList = new List<SemiFinalItem>();
            _itemList.Add(AddChildComponent<SemiFinalItem>("Container_item0"));
            _itemList.Add(AddChildComponent<SemiFinalItem>("Container_item2"));
            _itemList.Add(AddChildComponent<SemiFinalItem>("Container_item1"));
            _itemList.Add(AddChildComponent<SemiFinalItem>("Container_item3"));
        }

        public void SetData(GuildWarFinalMatchPanel.Tab _team, List<PbGuildBattleFinalMatchResult> list)
        {
            _textLeftTitle.CurrentText.text = MogoLanguageUtil.GetContent(111665);
            _textRightTitle.CurrentText.text = MogoLanguageUtil.GetContent(111666);
            for (int i = 0; i < list.Count; i++)
            {
                _itemList[i].SetData(list[i], i);
            }
        }

        class SemiFinalItem : KContainer
        {
            private PbGuildBattleFinalMatchResult _pb;

            private KButton _btnDetail;
            private GuildWarFinalMatchResult _leftResult;
            private GuildWarFinalMatchResult _rightResult;

            protected override void Awake()
            {
                _btnDetail = GetChildComponent<KButton>("Button_benfangzhiwei");
                _leftResult = AddChildComponent<GuildWarFinalMatchResult>("Container_zuo");
                _rightResult = AddChildComponent<GuildWarFinalMatchResult>("Container_you");

                AddListener();
            }

            protected override void OnDestroy()
            {
                RemoveListener();
            }

            private void AddListener()
            {
                _btnDetail.onClick.AddListener(OnDetail);
            }

            private void RemoveListener()
            {
                _btnDetail.onClick.RemoveListener(OnDetail);
            }

            private void OnDetail()
            {
                GuildWarManager.Instance.GetGuildWarResultRecord(_pb.log_dbid, _pb.guild_dbid1);
            }

            public void SetData(PbGuildBattleFinalMatchResult pb, int index)
            {
                _pb = pb;
                RefreshResult(pb);
            }

            private void RefreshResult(PbGuildBattleFinalMatchResult pb)
            {
                _leftResult.Clear();
                _rightResult.Clear();
                _btnDetail.Visible = false;
                if (pb != null)
                {
                    if (pb.win_dbid == 0)
                    {
                        _leftResult.Refresh(CopyResult.NoResult, pb.name1);
                        _rightResult.Refresh(CopyResult.NoResult, pb.name2);
                    }
                    else
                    {
                        _leftResult.Refresh(pb.guild_dbid1 == pb.win_dbid ? CopyResult.Win : CopyResult.Lose, pb.name1);
                        _rightResult.Refresh(pb.guild_dbid2 == pb.win_dbid ? CopyResult.Win : CopyResult.Lose, pb.name2);
                        _btnDetail.Visible = true;
                    }
                }
            }
        }
    }
}
