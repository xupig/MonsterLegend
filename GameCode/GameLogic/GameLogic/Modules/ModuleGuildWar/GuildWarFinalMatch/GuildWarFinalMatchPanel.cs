﻿using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarFinalMatchPanel : BasePanel
    {
        public enum Tab
        {
            king = 1,
            elite,
        }

        public enum Round
        {
            noStart,
            quarterFinal = 1,
            semiFinal,
            final,
        }

        private Tab _team;
        private Round _round;
        private Round _currentRound;

        private CategoryList _categoryList;
        private KButton _btnEnter;
        private KButton _btnAssignedPosition;
        private KButton _btnReward;
        private KButton _callBtn;
        private GuildWarFinalMatchButton _btnQuarterFinal;
        private GuildWarFinalMatchButton _btnSemiFinal;
        private GuildWarFinalMatchButton _btnFinal;
        private StateText _textNotStart;
        private StateText _textStartTime;

        private GuildWarQuarterFinalView _quarterFinalView;
        private GuildWarFinalView _finalView;
        private GuildWarSemiFinalView _semiFinalView;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildWarFinalMatch; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            InitCategoryList();
            InitButton();
            _textNotStart = GetChildComponent<StateText>("Label_txtmeiyoushuju");
            _textStartTime = GetChildComponent<StateText>("Label_txtKaishishijian");
            _quarterFinalView = AddChildComponent<GuildWarQuarterFinalView>("Container_xiangxizhankuang/Container_quarterFinal");
            _semiFinalView = AddChildComponent<GuildWarSemiFinalView>("Container_xiangxizhankuang/Container_zhankuangduibi");
            _finalView = AddChildComponent<GuildWarFinalView>("Container_xiangxizhankuang/Container_juesai");

            //_textStartTime.CurrentText.text = string.Concat(guild_match_open_helper.GetOpenDateContent((int)PlayerDataManager.Instance.GuildWarData.Stage.battle_id), " ", guild_match_helper.GetOpenContent(public_config.GUILD_BATTLE_STATE_FINAL_MATCHING));
            _textStartTime.CurrentText.text = string.Concat(guild_match_helper.GetOpenContent(public_config.GUILD_BATTLE_STATE_FINAL_MATCHING));

            AddListener();
        }

        protected override void OnCloseBtnClick()
        {
            base.OnCloseBtnClick();
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWar);
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void InitCategoryList()
        {
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
        }

        private void InitButton()
        {
            _btnEnter = GetChildComponent<KButton>("Button_jinru");
            _btnAssignedPosition = GetChildComponent<KButton>("Button_fenpei");
            _btnReward = GetChildComponent<KButton>("Button_jiangliyulan");
            _callBtn = GetChildComponent<KButton>("Button_zhaoji");
            _btnQuarterFinal = AddChildComponent<GuildWarFinalMatchButton>("Container_xiangxizhankuang/Button_sifenzhiyi");
            _btnSemiFinal = AddChildComponent<GuildWarFinalMatchButton>("Container_xiangxizhankuang/Button_banjuesai");
            _btnFinal = AddChildComponent<GuildWarFinalMatchButton>("Container_xiangxizhankuang/Button_juesai");
        }

        private void AddListener()
        {
            _btnEnter.onClick.AddListener(OnEnterMatch);
            _btnAssignedPosition.onClick.AddListener(OnShowAssignedPosition);
            _btnReward.onClick.AddListener(OnShowRewardPreview);
            _btnQuarterFinal.onClick.AddListener(OnShowQuarterFinal);
            _btnSemiFinal.onClick.AddListener(OnShowSemiFinal);
            _btnFinal.onClick.AddListener(OnShowFinal);
            _callBtn.onClick.AddListener(OnCallBtnClick);
            _categoryList.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
        }

        private void RemoveListener()
        {
            _btnEnter.onClick.RemoveListener(OnEnterMatch);
            _btnAssignedPosition.onClick.RemoveListener(OnShowAssignedPosition);
            _btnReward.onClick.RemoveListener(OnShowRewardPreview);
            _btnQuarterFinal.onClick.RemoveListener(OnShowQuarterFinal);
            _btnSemiFinal.onClick.RemoveListener(OnShowSemiFinal);
            _btnFinal.onClick.RemoveListener(OnShowFinal);
            _callBtn.onClick.RemoveListener(OnCallBtnClick);
            _categoryList.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
        }

        private void OnCallBtnClick()
        {
            GuildWarManager.Instance.CallPeople();
        }


        private void AddEnableListener()
        {
            EventDispatcher.AddEventListener<PbGuildBattleFinalMatchResultList>(GuildWarEvents.Response_Final_Match_Info, OnResponseFinalMatchInfo);
        }

        private void RemoveEnableListener()
        {
            EventDispatcher.RemoveEventListener<PbGuildBattleFinalMatchResultList>(GuildWarEvents.Response_Final_Match_Info, OnResponseFinalMatchInfo);
        }

        private void OnEnterMatch()
        {
            GuildWarManager.Instance.EnterWaitMission();
        }

        private void OnShowAssignedPosition()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarAssignedPosition);
        }

        private void OnShowRewardPreview()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarReward, GuildWarRewardType.FinalMatch);
        }

        private void OnSelectedIndexChanged(CategoryList categoryList, int index)
        {
            _team = (Tab)(categoryList.SelectedIndex + 1);
            GuildWarManager.Instance.GetFinalMatchInfo((int)_team, (int)_round);
        }

        private void OnShowQuarterFinal()
        {
            _round = Round.quarterFinal;
            GuildWarManager.Instance.GetFinalMatchInfo((int)_team, (int)_round);
        }

        private void OnShowSemiFinal()
        {
            _round = Round.semiFinal;
            GuildWarManager.Instance.GetFinalMatchInfo((int)_team, (int)_round);
        }

        private void OnShowFinal()
        {
            _round = Round.final;
            GuildWarManager.Instance.GetFinalMatchInfo((int)_team, (int)_round);
        }

        private void OnResponseFinalMatchInfo(PbGuildBattleFinalMatchResultList pb)
        {
            //Debug.LogError("OnResponseFinalMatchInfo");
            //Debug.LogError("team:" + pb.team);
            //Debug.LogError("group:" + pb.group);
            //Debug.LogError("battle_match_round:" + pb.battle_match_round);

            //pb.team = (uint)1;
            //pb.group = (uint)2;
            //pb.battle_match_round = (uint)2;
            //for (int i = 0; i < 4; i++)
            //{
            //    PbGuildBattleFinalMatchResult item = new PbGuildBattleFinalMatchResult();
            //    item.index = (uint)i + 1;
            //    item.name1 = "A" + i;
            //    item.name2 = "B" + i;
            //    item.guild_dbid1 = (ulong)i;
            //    item.guild_dbid2 = (ulong)i + 100;
            //    item.win_dbid = (uint)i;
            //    pb.list.Add(item);
            //}
            ParseProtoBuff(pb);
            Refresh(pb);
        }

        private void ParseProtoBuff(PbGuildBattleFinalMatchResultList pb)
        {
            for (int i = 0; i < pb.list.Count; i++)
            {
                PbGuildBattleFinalMatchResult item = pb.list[i];
                item.name1 = MogoProtoUtils.ParseByteArrToString(item.name1_bytes);
                item.name2 = MogoProtoUtils.ParseByteArrToString(item.name2_bytes);
            }
            _team = (Tab)pb.team;
            _round = (Round)pb.group;
            _currentRound = (Round)pb.battle_match_round;
        }
                                                                 
        public override void OnShow(object data)
        {
            AddEnableListener();
            GuildWarManager.Instance.GetFinalMatchInfo(1, 1);
        }

        private void Refresh(PbGuildBattleFinalMatchResultList pb)
        {
            List<CategoryItemData> nameList = new List<CategoryItemData>();
            nameList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(111608)));
            if (PlayerDataManager.Instance.GuildWarData.Stage.format == 1)
            {
                nameList.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(111609)));
            }
            _categoryList.RemoveAll();
            _categoryList.SetDataList<CategoryListItem>(nameList);
            _categoryList.SelectedIndex = (int)_team - 1;
            _quarterFinalView.Visible = false;
            _semiFinalView.Visible = false;
            _finalView.Visible = false;
            List<PbGuildBattleFinalMatchResult> list = SortPbList(pb);
            bool isQuarterFinalBegin = (int)_currentRound >= (int)Round.quarterFinal;
            switch (_round)
            {
                case Round.quarterFinal:
                    _quarterFinalView.Visible = true;
                    _quarterFinalView.SetData(_team, list, isQuarterFinalBegin);
                    break;
                case Round.semiFinal:
                    _semiFinalView.Visible = true;
                    _semiFinalView.SetData(_team, list);
                    break;
                case Round.final:
                    _finalView.Visible = true;
                    _finalView.SetData(_team, list);
                    break;
            }
            RefreshNoStar();
            RefreshRound();
            RefreshButtonState();
        }

        private List<PbGuildBattleFinalMatchResult> SortPbList(PbGuildBattleFinalMatchResultList pb)
        {
            List<PbGuildBattleFinalMatchResult> list = new List<PbGuildBattleFinalMatchResult>();
            for (int i = 0; i < 4; i++)
            {
                list.Add(GetIndex(pb.list, i + 1));
            }
            return list;
        }

        private PbGuildBattleFinalMatchResult GetIndex(List<PbGuildBattleFinalMatchResult> list, int index)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].index == index)
                {
                    return list[i];
                }
            }
            return null;
        }

        private void RefreshNoStar()
        {
            if(_currentRound == Round.noStart)
            {
                _textNotStart.Visible = true;
                _callBtn.Visible = false;
                _btnEnter.Visible = false;
            }
            else
            {
                _textNotStart.Visible = false;
                _callBtn.Visible = true;
                _btnEnter.Visible = true;
            }
        }

        private void RefreshRound()
        {
            _btnQuarterFinal.SetLabel(MogoLanguageUtil.GetContent(111667));
            _btnSemiFinal.SetLabel(MogoLanguageUtil.GetContent(111668));
            _btnFinal.SetLabel(MogoLanguageUtil.GetContent(111669));
            switch (_currentRound)
            {
                case Round.quarterFinal:
                    _btnQuarterFinal.AppendLabel(MogoLanguageUtil.GetContent(111670));
                    break;
                case Round.semiFinal:
                    _btnQuarterFinal.AppendLabel(MogoLanguageUtil.GetContent(111670));
                    break;
                case Round.final:
                    _btnQuarterFinal.AppendLabel(MogoLanguageUtil.GetContent(111670));
                    break;
            }
        }

        private void RefreshButtonState()
        {
            _btnQuarterFinal.Reset();
            _btnSemiFinal.Reset();
            _btnFinal.Reset();
            _btnQuarterFinal.SetState(_currentRound == Round.quarterFinal, _round == Round.quarterFinal, ((int)_currentRound >= (int)Round.quarterFinal));
            _btnSemiFinal.SetState(_currentRound == Round.semiFinal, _round == Round.semiFinal, ((int)_currentRound >= (int)Round.semiFinal));
            _btnFinal.SetState(_currentRound == Round.final, _round == Round.final, ((int)_currentRound >= (int)Round.final));
        }

        public override void OnClose()
        {
            RemoveEnableListener();
        }
    }
}
