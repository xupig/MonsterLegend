﻿using Common.Data;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarRewardItem : KList.KListItemBase
    {
        private StateImage _firstImg;
        private StateImage _secondImg;
        private StateImage _thridImg;
        private StateImage _1stImg;
        private StateImage _2ndImg;
        private StateImage _3rdImg;
        private StateText _1stTxt;
        private StateText _2ndTxt;
        private List<GuildWarRewardContainer> _positionContainerList;

        private object _data;
        private int _rank;

        protected override void Awake()
        {
            _firstImg = GetChildComponent<StateImage>("Container_paiming/Image_diyiming");
            _secondImg = GetChildComponent<StateImage>("Container_paiming/Image_dierming");
            _thridImg = GetChildComponent<StateImage>("Container_paiming/Image_disanming");
            _1stImg = GetChildComponent<StateImage>("Container_paiming/Image_diyi");
            _2ndImg = GetChildComponent<StateImage>("Container_paiming/Image_dier");
            _3rdImg = GetChildComponent<StateImage>("Container_paiming/Image_disan");
            _1stTxt = GetChildComponent<StateText>("Container_paiming/Label_txtPaimingyi");
            _2ndTxt = GetChildComponent<StateText>("Container_paiming/Label_txtPaiminger");
            _1stTxt.ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);
            _2ndTxt.ChangeAllStateTextAlignment(TextAnchor.MiddleCenter);
            _positionContainerList = new List<GuildWarRewardContainer>();
            _positionContainerList.Add(AddChildComponent<GuildWarRewardContainer>("Container_qishizhang"));
            _positionContainerList.Add(AddChildComponent<GuildWarRewardContainer>("Container_qishi"));
            _positionContainerList.Add(AddChildComponent<GuildWarRewardContainer>("Container_shibing"));
        }

        private void SetRank()
        {
            if (_data is List<guild_battle_group_match_reward>)
            {
                _rank = (_data as List<guild_battle_group_match_reward>)[0].__guild_group_rank;
                _firstImg.Visible = _rank == 1;
                _secondImg.Visible = _rank == 2;
                _thridImg.Visible = _rank == 3;
                _1stImg.Visible = _rank == 1;
                _2ndImg.Visible = _rank == 2;
                _3rdImg.Visible = _rank == 3;
                _1stTxt.Visible = _rank == 4;
                _1stTxt.CurrentText.text = MogoLanguageUtil.GetContent(111596);
                _2ndTxt.Visible = false;
            }
            else if (_data is List<guild_battle_final_match_reward>)
            {
                _rank = (_data as List<guild_battle_final_match_reward>)[0].__guild_final_rank;
                _firstImg.Visible = _rank <= 8;
                _secondImg.Visible = _rank > 8;
                _thridImg.Visible = false;
                _1stImg.Visible = false;
                _2ndImg.Visible = false;
                _3rdImg.Visible = false;
                _1stTxt.Visible = _rank <= 8;
                _2ndTxt.Visible = _rank > 8;
                _1stTxt.CurrentText.text = MogoLanguageUtil.GetContent(111580 + _rank - 1);
                _2ndTxt.CurrentText.text = MogoLanguageUtil.GetContent(111580 + _rank - 1);
            }
            else if (_data is List<guild_battle_friendly_match_reward>)
            {
                _rank = (_data as List<guild_battle_friendly_match_reward>)[0].__friend_match_point;
                _firstImg.Visible = false;
                _secondImg.Visible = false;
                _thridImg.Visible = false;
                _1stImg.Visible = false;
                _2ndImg.Visible = false;
                _3rdImg.Visible = false;
                _1stTxt.Visible = true;
                _1stTxt.CurrentText.text = _rank.ToString();
                _2ndTxt.Visible = false;
            }
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            SortList();
            SetRank();
            for (int i = 0; i < 3; i++)
            {
                int rewardId = 0;
                if (_data is List<guild_battle_group_match_reward>)
                {
                    rewardId = (_data as List<guild_battle_group_match_reward>)[i].__reward;
                }
                else if (_data is List<guild_battle_final_match_reward>)
                {
                    rewardId = (_data as List<guild_battle_final_match_reward>)[i].__reward;
                }
                else if (_data is List<guild_battle_friendly_match_reward>)
                {
                    rewardId = (_data as List<guild_battle_friendly_match_reward>)[i].__reward;
                }
                _positionContainerList[i].SetPosition(i + 1, rewardId);
            }
        }

        private void SortList()
        {
            if (_data is List<guild_battle_group_match_reward>)
            {
                (_data as List<guild_battle_group_match_reward>).Sort(SortGroupReward);
            }
            else if (_data is List<guild_battle_final_match_reward>)
            {
                (_data as List<guild_battle_final_match_reward>).Sort(SortFinalReward);
            }
            else if (_data is List<guild_battle_friendly_match_reward>)
            {
                (_data as List<guild_battle_friendly_match_reward>).Sort(SortFriendlyReward);
            }
        }

        private int SortFriendlyReward(guild_battle_friendly_match_reward x, guild_battle_friendly_match_reward y)
        {
            return x.__guild_match_player_rank - y.__guild_match_player_rank;
        }

        private int SortFinalReward(guild_battle_final_match_reward x, guild_battle_final_match_reward y)
        {
            return x.__guild_match_player_rank - y.__guild_match_player_rank;
        }

        private int SortGroupReward(guild_battle_group_match_reward x, guild_battle_group_match_reward y)
        {
            return x.__guild_match_player_rank - y.__guild_match_player_rank;
        }

        public override void Dispose()
        {
            _data = null;
        }

        private class GuildWarRewardContainer : KContainer
        {
            private StateText _positionTxt;
            private List<StateText> _numTxtList;
            private List<ItemGrid> _itemList;

            protected override void Awake()
            {
                base.Awake();
                _positionTxt = GetChildComponent<StateText>("Label_txtZhiwei");
                _numTxtList = new List<StateText>();
                _numTxtList.Add(GetChildComponent<StateText>("Container_item01/Label_txtShuliang"));
                _numTxtList.Add(GetChildComponent<StateText>("Container_item02/Label_txtShuliang"));
                _numTxtList.Add(GetChildComponent<StateText>("Container_item03/Label_txtShuliang"));
                _numTxtList.Add(GetChildComponent<StateText>("Container_item04/Label_txtShuliang"));
                _itemList = new List<ItemGrid>();
                _itemList.Add(GetChildComponent<ItemGrid>("Container_item01/Container_wupin"));
                _itemList.Add(GetChildComponent<ItemGrid>("Container_item02/Container_wupin"));
                _itemList.Add(GetChildComponent<ItemGrid>("Container_item03/Container_wupin"));
                _itemList.Add(GetChildComponent<ItemGrid>("Container_item04/Container_wupin"));
                for (int i = 0; i < 4; i++)
                {
                    _numTxtList[i].ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
                }
            }

            public void SetPosition(int position, int rewardId)
            {
                _positionTxt.CurrentText.text = MogoLanguageUtil.GetContent(111517 + position - 1);
                List<RewardData> dataList = item_reward_helper.GetRewardDataList(rewardId, PlayerAvatar.Player.vocation);
                for (int i = 0; i < 4; i++)
                {
                    _numTxtList[i].Visible = false;
                    _itemList[i].Visible = false;
                }
                for (int i = 0; i < dataList.Count && i < 4; i++)
                {
                    _numTxtList[i].Visible = true;
                    _itemList[i].Visible = true;
                    _numTxtList[i].ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
                    _numTxtList[i].CurrentText.text = string.Format("x{0}", dataList[i].num);
                    _itemList[i].SetItemData(dataList[i].id);
                }
            }
        }

    }
}
