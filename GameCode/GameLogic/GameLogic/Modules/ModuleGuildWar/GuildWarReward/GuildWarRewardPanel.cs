﻿using Common.Base;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuildWar
{
    public enum GuildWarRewardType
    {
        GroupMatch = 1,
        FinalMatch = 2,
        FriendlyMatch = 3,
    }

    public class GuildWarRewardPanel : BasePanel
    {
        private KScrollView _scrollView;
        private KList _list;
        private KToggleGroup _toggleGroup;
        private StateText _titleTxt;
        private StateText _descTxt;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildWarReward; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_biaoti");
            _scrollView = GetChildComponent<KScrollView>("Container_paihangbang/ScrollView_zhanlibang");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _titleTxt = GetChildComponent<StateText>("Container_paihangbang/Container_txtTitle/Label_text01");
            _descTxt = GetChildComponent<StateText>("Label_txtXinxi");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            _toggleGroup.SelectIndex = (int)data - 1;
            _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(111675);
            RefreshContent((GuildWarRewardType)data);
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void RefreshText(GuildWarRewardType type)
        {
            if (type == GuildWarRewardType.GroupMatch)
            {
                _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(111578);
            }
            else if (type == GuildWarRewardType.GroupMatch)
            {
                _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(111578);
            }
            else if (type == GuildWarRewardType.FriendlyMatch)
            {
                _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(111579);
            }
        }

        private void RefreshContent(GuildWarRewardType type)
        {
            int format = (int)PlayerDataManager.Instance.GuildWarData.Stage.format;
            format = 1;
            RefreshText(type);
            if (type == GuildWarRewardType.GroupMatch)
            {
                List<List<guild_battle_group_match_reward>> dataList = guild_match_group_match_helper.GetRewardList(format, PlayerAvatar.Player.level);
                _list.SetDataList<GuildWarRewardItem>(dataList, 1);
            }
            else if (type == GuildWarRewardType.FinalMatch)
            {
                List<List<guild_battle_final_match_reward>> dataList = guild_match_final_match_helper.GetRewardList(format, PlayerAvatar.Player.level);
                _list.SetDataList<GuildWarRewardItem>(dataList, 1);
            }
            else if (type == GuildWarRewardType.FriendlyMatch)
            {
                List<List<guild_battle_friendly_match_reward>> dataList = guild_match_friendly_match_helper.GetRewardList(format, PlayerAvatar.Player.level);
                _list.SetDataList<GuildWarRewardItem>(dataList, 1);
            }
            else
            {
                _list.RemoveAll();
            }
        }

        private void OnTabChange(KToggleGroup arg0, int index)
        {
            RefreshContent((GuildWarRewardType)(index + 1));
        }

        private void AddEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnTabChange);
        }

        private void RemoveEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnTabChange);
        }


    }
}