﻿using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuildWar
{
    public class GuildWarGroupMatchListView : KContainer
    {
        private KScrollView _scrollView;
        private KList _list;
        private StateText _noneTxt;

        protected override void Awake()
        {
            base.Awake();
            _scrollView = GetChildComponent<KScrollView>("Container_zhankuangduibi/ScrollView_zhanlibang");
            _noneTxt = GetChildComponent<StateText>("Label_txtMeiyoushujv");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        public void RefrshList(int index)
        {
            _noneTxt.Visible = false;
            GuildWarManager.Instance.GetGroupMatchInfo(index);
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<PbGuildBattleGroupMatchResultList>(GuildWarEvents.Refresh_Guild_Group_Match_Info, RefreshGroupMatchInfo);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<PbGuildBattleGroupMatchResultList>(GuildWarEvents.Refresh_Guild_Group_Match_Info, RefreshGroupMatchInfo);
        }

        private void RefreshGroupMatchInfo(PbGuildBattleGroupMatchResultList list)
        {
            _noneTxt.Visible = list.list.Count == 0;
            list.list.Sort(SortFun);
            _list.SetDataList<GuildWarGroupMatchItem>(list.list);
        }

        private int SortFun(PbGuildBattleGroupMatchResult x, PbGuildBattleGroupMatchResult y)
        {
            if (x.win_count != y.win_count)
            {
                return (int)y.win_count - (int)x.win_count;
            }
            if (x.point != y.point)
            {
                return (int)y.point - (int)x.point;
            }
            return (int)y.index - (int)x.index;
        }

    }
}
