﻿using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarGroupMatchInfoView : KContainer
    {
        private StateText _descTxt;
        private KButton _backBtn;
        private KButton _detailBtn;
        private GuildWarGroupMatchListView _listView;
        private GuildWarGroupMatchDetailView _detailView;
        private int _stage;
        private int _index;

        protected override void Awake()
        {
            base.Awake();
            _descTxt = GetChildComponent<StateText>("Label_txtTishi");
            _backBtn = GetChildComponent<KButton>("Button_fanhui");
            _detailBtn = GetChildComponent<KButton>("Button_xiangxizhanbao");
            _listView = AddChildComponent<GuildWarGroupMatchListView>("Container_paihangbang");
            _detailView = AddChildComponent<GuildWarGroupMatchDetailView>("Container_xiangxizhankuang");
        }

        private void RefreshView()
        {
            _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(111602, guild_match_helper.GetEnterMatchGuildCount((int)PlayerDataManager.Instance.GuildWarData.Stage.format));
            _stage = 0;
            _index = 1;
            ShowInfoView();
        }

        private void RefreshList()
        {
            if (_listView.Visible == true)
            {
                _listView.RefrshList(_index);
            }
            else if (_detailView.Visible == true)
            {
                _detailView.RefrshList(_index);
            }
        }

        public void RefreshIndex(int index)
        {
            _index = index;
            RefreshList();
        }

        public void RefreshStage(int stage)
        {
            _stage = stage;
            if (_listView.Visible == true)
            {
                _detailBtn.Visible = _stage > public_config.GUILD_BATTLE_STATE_GROUP_MATCHING;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            RefreshView();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _backBtn.onClick.AddListener(OnBackBtnClick);
            _detailBtn.onClick.AddListener(OnDetailBtnClick);
        }

        private void RemoveEventListener()
        {
            _backBtn.onClick.RemoveListener(OnBackBtnClick);
            _detailBtn.onClick.RemoveListener(OnDetailBtnClick);
        }

        public void ShowInfoView()
        {
            _backBtn.Visible = false;
            _detailBtn.Visible = _stage > public_config.GUILD_BATTLE_STATE_GROUP_MATCHING;
            _listView.Visible = true;
            _detailView.Visible = false;
            RefreshList();
        }

        public void ShowDetailView()
        {
            _backBtn.Visible = true;
            _detailBtn.Visible = false;
            _listView.Visible = false;
            _detailView.Visible = true;
            RefreshList();
        }

        private void OnDetailBtnClick()
        {
            ShowDetailView();
        }

        private void OnBackBtnClick()
        {
            ShowInfoView();
        }

    }
}
