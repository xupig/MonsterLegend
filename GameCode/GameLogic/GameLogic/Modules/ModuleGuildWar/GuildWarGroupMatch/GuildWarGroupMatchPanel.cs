﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarGroupMatchPanel : BasePanel
    {
        private CategoryList _categoryList;
        private StateText _timeTxt;
        private StateText _notOpenTxt;
        private KButton _assignBtn;
        private KButton _entryBtn;
        private KButton _flowBtn;
        private KButton _rewardBtn;
        private KButton _callBtn;
        private GuildWarGroupMatchInfoView _infoView;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildWarGroupMatch; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            AddChildComponent<GuildWarBackground>("Container_panelBg/Contianer_bg");
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            _timeTxt = GetChildComponent<StateText>("Label_txtKaishishijian");
            _notOpenTxt = GetChildComponent<StateText>("Label_txtmeiyoushuju");
            _assignBtn = GetChildComponent<KButton>("Button_fenpei");
            _entryBtn = GetChildComponent<KButton>("Button_jinru");
            _flowBtn = GetChildComponent<KButton>("Button_liucheng");
            _rewardBtn = GetChildComponent<KButton>("Button_jiangliyulan");
            _callBtn = GetChildComponent<KButton>("Button_zhaoji");
            _infoView = AddChildComponent<GuildWarGroupMatchInfoView>("Container_zhanbao");
        }

        protected override void OnCloseBtnClick()
        {
            base.OnCloseBtnClick();
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWar);
        }

        private void RefreshCategoryList()
        {
            PbGuildStage stage = PlayerDataManager.Instance.GuildWarData.Stage;
            List<CategoryItemData> listToggleData = new List<CategoryItemData>();
            if (stage.format == public_config.GUILD_BATTLE_FORMAT_1)
            {
                listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(111598)));
                listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(111599)));
                listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(111600)));
                listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(111601)));
            }
            else
            {
                listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(111598)));
                listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(111599)));
            }
            _categoryList.RemoveAll();
            _categoryList.SetDataList<CategoryListItem>(listToggleData);
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            RefreshCategoryList();
            _categoryList.SelectedIndex = 0;
            RefreshContent();
            RefreshGuildWarStage();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void RefreshContent()
        {
            //_timeTxt.CurrentText.text = string.Concat(guild_match_open_helper.GetOpenDateContent((int)PlayerDataManager.Instance.GuildWarData.Stage.battle_id), " ", guild_match_helper.GetOpenContent(public_config.GUILD_BATTLE_STATE_GROUP_MATCHING));
            _timeTxt.CurrentText.text = string.Concat(guild_match_helper.GetOpenContent(public_config.GUILD_BATTLE_STATE_GROUP_MATCHING));
            _notOpenTxt.Visible = false;
            _entryBtn.Visible = false;
            _assignBtn.Visible = false;
            _callBtn.Visible = false;
        }

        private void RefreshGuildWarStage()
        {
            PbGuildStage stage = PlayerDataManager.Instance.GuildWarData.Stage;
            _entryBtn.Visible = stage.stage == public_config.GUILD_BATTLE_STATE_GROUP_MATCHING;
            _callBtn.Visible = stage.stage == public_config.GUILD_BATTLE_STATE_GROUP_MATCHING;
            _assignBtn.Visible = stage.stage <= public_config.GUILD_BATTLE_STATE_GROUP_MATCHING;
            if (stage.stage < public_config.GUILD_BATTLE_STATE_GROUP_MATCHING)
            {
                _notOpenTxt.Visible = true;
                _notOpenTxt.CurrentText.text = MogoLanguageUtil.GetContent(111604);
            }
            else if (stage.stage > public_config.GUILD_BATTLE_STATE_GROUP_MATCHING)
            {
                _notOpenTxt.Visible = true;
                _notOpenTxt.CurrentText.text = MogoLanguageUtil.GetContent(111605);
            }
            else
            {
                _notOpenTxt.Visible = false;
            }
            _infoView.RefreshStage((int)stage.stage);
        }

        private void OnSelectedIndexChanged(CategoryList arg0, int index)
        {
            _infoView.RefreshIndex(index + 1);
        }

        private void AddEventListener()
        {
            _assignBtn.onClick.AddListener(OnAssignBtnClick);
            _entryBtn.onClick.AddListener(OnEntryBtnClick);
            _flowBtn.onClick.AddListener(OnFlowBtnClick);
            _rewardBtn.onClick.AddListener(OnRewardBtnClick);
            _callBtn.onClick.AddListener(OnCallBtnClick);
            _categoryList.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
        }

        private void RemoveEventListener()
        {
            _assignBtn.onClick.RemoveListener(OnAssignBtnClick);
            _entryBtn.onClick.RemoveListener(OnEntryBtnClick);
            _flowBtn.onClick.RemoveListener(OnFlowBtnClick);
            _rewardBtn.onClick.RemoveListener(OnRewardBtnClick);
            _callBtn.onClick.RemoveListener(OnCallBtnClick);
            _categoryList.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
        }

        private void OnCallBtnClick()
        {
            GuildWarManager.Instance.CallPeople();
        }

        private void OnRewardBtnClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarReward, GuildWarRewardType.GroupMatch);
        }

        private void OnFlowBtnClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarFlow);
        }

        private void OnEntryBtnClick()
        {
            GuildWarManager.Instance.EnterWaitMission();
        }

        private void OnAssignBtnClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarAssignedPosition);
        }

        
    }
}