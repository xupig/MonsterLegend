﻿using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarGroupMatchItem : KList.KListItemBase
    {
        private RankContainer _rankContainer;
        private StateText _nameTxt;
        private StateText _winloseTxt;
        private StateText _scoreTxt;
        private StateText _infoTxt;

        private PbGuildBattleGroupMatchResult _data;

        protected override void Awake()
        {
            _rankContainer = AddChildComponent<RankContainer>("Container_mingci");
            _nameTxt = GetChildComponent<StateText>("Label_txtGonghuimingzi");
            _winloseTxt = GetChildComponent<StateText>("Label_txtShengfu");
            _scoreTxt = GetChildComponent<StateText>("Label_txtShengfen");
            _infoTxt = GetChildComponent<StateText>("Label_txtDuifangZhandouli");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbGuildBattleGroupMatchResult;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            _rankContainer.SetRank((int)_data.index);
            _nameTxt.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.name_bytes);
            _winloseTxt.CurrentText.text = string.Format("{0}-{1}", _data.win_count, _data.lose_count);
            _scoreTxt.CurrentText.text = _data.point.ToString();
            if (PlayerDataManager.Instance.GuildWarData.Stage.format == 1)
            {
                if (_data.promotion == 1 || _data.promotion == 2)
                {
                    _infoTxt.CurrentText.text = MogoLanguageUtil.GetContent(111608);
                }
                else if (_data.promotion == 3 || _data.promotion == 4)
                {
                    _infoTxt.CurrentText.text = MogoLanguageUtil.GetContent(111609);
                }
                else
                {
                    _infoTxt.CurrentText.text = "--";
                }
            }
            else
            {
                _infoTxt.CurrentText.text = MogoLanguageUtil.GetContent(111608);
            }
        }

        public override void Dispose()
        {
            _data = null;
        }
    }
}
