﻿using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuildWar
{
    public class GuildWarGroupMatchDetailView : KContainer
    {
        private KScrollView _scrollView;
        private KList _list;
        private StateText _titleTxt;

        protected override void Awake()
        {
            base.Awake();
            _scrollView = GetChildComponent<KScrollView>("Container_zhankuangduibi/ScrollView_zhanlibang");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _titleTxt = GetChildComponent<StateText>("Container_zhankuangduibi/Label_txtBiaoti");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        public void RefrshList(int index)
        {
            GuildWarManager.Instance.GetGroupMatchLog(index);
            _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(111606, MogoLanguageUtil.GetContent(111598 + index - 1));
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<PbGuildBattleGroupMatchLogList>(GuildWarEvents.Refresh_Guild_Group_Match_Log, RefreshGroupMatchInfo);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<PbGuildBattleGroupMatchLogList>(GuildWarEvents.Refresh_Guild_Group_Match_Log, RefreshGroupMatchInfo);
        }

        private void RefreshGroupMatchInfo(PbGuildBattleGroupMatchLogList list)
        {
            _list.SetDataList<GuildWarGroupMatchDetailItem>(list.list, 2);
        }
    }
}
