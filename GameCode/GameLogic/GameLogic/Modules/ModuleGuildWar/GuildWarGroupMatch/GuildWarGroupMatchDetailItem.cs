﻿using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarGroupMatchDetailItem : KList.KListItemBase
    {
        private StateText _vsTxt;
        private StateText _scoreTxt;

        private PbGuildBattleGroupMatchLog _data;

        protected override void Awake()
        {
            _vsTxt = GetChildComponent<StateText>("Label_txtGonghuimingzi");
            _scoreTxt = GetChildComponent<StateText>("Label_txtShengfu");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbGuildBattleGroupMatchLog;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            RefreshContent();
        }

        private void RefreshContent()
        {
            _vsTxt.CurrentText.text = string.Format("{0} VS {1}", MogoProtoUtils.ParseByteArrToString(_data.name1_btypes), MogoProtoUtils.ParseByteArrToString(_data.name2_btypes));
            _scoreTxt.CurrentText.text = string.Format("{0}:{1}", _data.point1, _data.point2);
        }

        public override void Dispose()
        {
            _data = null;
        }
    }
}
