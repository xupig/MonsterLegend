﻿using Common.Data;
using Common.Events;
using Common.Global;
using Common.Structs.ProtoBuf;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarAssignedPositionItem : KList.KListItemBase
    {
        private PbGuildMemberInfo _playerInfo;

        private Color _originalColor;

        private StateText _textName;
        private StateText _textFightValue;
        private StateText _textAttendance;
        private KButton _btnAssigned;
        private StateText _textPosition;
        private StateImage _imageFrame;

        protected override void Awake()
        {
            _textName = GetChildComponent<StateText>("Label_txtWanjia");
            _originalColor = _textName.CurrentText.color;
            _textFightValue = GetChildComponent<StateText>("Label_txtZhandouli");
            _textAttendance = GetChildComponent<StateText>("Label_txtShangjiechuqinlv");
            _btnAssigned = GetChildComponent<KButton>("Button_gonghuizhanzhiwei");
            _textPosition = GetChildComponent<StateText>("Button_gonghuizhanzhiwei/label");
            _imageFrame = GetChildComponent<StateImage>("Image_xuanzhong");

            AddListener();
        }

        public override void Dispose()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnAssigned.onClick.AddListener(OnShowDropList);
        }

        private void RemoveListener()
        {
            _btnAssigned.onClick.RemoveListener(OnShowDropList);
        }

        public override object Data
        {
            get
            {
                return _playerInfo;
            }
            set
            {
                PbGuildMemberInfo playerInfo = value as PbGuildMemberInfo;
                _playerInfo = playerInfo;
                _textName.CurrentText.text = playerInfo.name;
                _textFightValue.CurrentText.text = playerInfo.fight.ToString();
                _textAttendance.CurrentText.text = string.Format("{0}%", playerInfo.attendance);
                RefreshPosition();
                RefreshTextGray(playerInfo.online == 1);
            }
        }

        private void OnShowDropList()
        {
            if (guild_rights_helper.CheckHasRights(GuildRightId.examine, 111649) == false) return;
            var screenPosition = UIManager.Instance.UICamera.WorldToScreenPoint(transform.position);
            EventDispatcher.TriggerEvent<int, Vector3>(GuildWarEvents.Show_Drop_Down_List, Index, screenPosition);
        }

        public void ShowFrame()
        {
            _imageFrame.Visible = true;
        }

        public void HideFrame()
        {
            _imageFrame.Visible = false;
        }

        public void RefreshPosition()
        {
            string position = string.Empty;
            if (_playerInfo.military == 0)
            {
                position = guild_match_helper.GetMilitaryRankName((int)GuildWarData.MilitaryRank.soldier);
            }
            else
            {
                position = guild_match_helper.GetMilitaryRankName((int)_playerInfo.military);
            }
            _textPosition.ChangeAllStateText(position);
        }

        private void RefreshTextGray(bool isOnline)
        {
            TextWrapper[] textArray = GetComponentsInChildren<TextWrapper>(true);
            for (int i = 0; i < textArray.Length; i++)
            {
                if (isOnline)
                {
                    textArray[i].color = _originalColor;
                }
                else
                {
                    textArray[i].color = ColorDefine.GetColorById(ColorDefine.COLOR_ID_GRAY);
                }
            }
        }
    }
}
