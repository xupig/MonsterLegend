﻿using Common.ExtendComponent;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.EventSystems;

namespace ModuleGuildWar
{
    public class GuildWarAssignedDropDownItem : DropDownItem
    {
        private StateText _txtName;
        private KButton _btnItem;

        protected override void Awake()
        {
            base.Awake();
            _btnItem = GetChildComponent<KButton>("Button_item");
            _txtName = _btnItem.GetChildComponent<StateText>("label");
            _btnItem.onClick.AddListener(OnClickItem);
        }

        public override void Dispose()
        {
            _btnItem.onClick.RemoveListener(OnClickItem);
        }

        public override void OnPointerClick(PointerEventData evtData)
        {

        }

        public override object Data
        {
            set
            {
                _txtName.ChangeAllStateText(value as String);
            }
        }

        private void OnClickItem()
        {
            onClick.Invoke(this, Index);
        }
    }
}
