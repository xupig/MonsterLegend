﻿using Common.ExtendComponent;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuildWar
{
    public class GuildWarAssignedDropDownList : DropDownList
    {
        private KDummyButton _btnClose;
        protected override void Awake()
        {
            base.Awake();
            _btnClose = AddChildComponent<KDummyButton>("Container_close");

            _btnClose.onClick.AddListener(OnClose);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            _btnClose.onClick.RemoveListener(OnClose);
        }

        private void OnClose()
        {
            Visible = false;
        }
    }
}
