﻿using ACTSystem;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarAssginedPlayerInfo : KContainer
    {
        private PbGuildMemberInfo _pbInfo;
        private ACTActor _actor;

        private StateText _textId;
        private StateText _textVocation;
        private StateText _textEnterTime;
        private StateText _textPosition;
        private ModelComponent _model;
        private KButton _btnAdd;

        protected override void Awake()
        {
            _textId = GetChildComponent<StateText>("Label_txtWanjia02");
            _textVocation = GetChildComponent<StateText>("Label_txtZhiye02");
            _textEnterTime = GetChildComponent<StateText>("Label_txtRuhuishijian02");
            _textPosition = GetChildComponent<StateText>("Label_txtZhiwei02");
            _btnAdd = GetChildComponent<KButton>("Button_tianjia");
            _model = AddChildComponent<ModelComponent>("Container_roleModel");

        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnAdd.onClick.AddListener(OnAdd);
            EventDispatcher.AddEventListener<PbOfflineData>(RankingListEvents.GET_RANK_PLAYER_FACADE_INFO, RefreshMemberFacade);
        }

        private void RemoveListener()
        {
            _btnAdd.onClick.RemoveListener(OnAdd);
            EventDispatcher.RemoveEventListener<PbOfflineData>(RankingListEvents.GET_RANK_PLAYER_FACADE_INFO, RefreshMemberFacade);
        }

        private void RefreshMemberFacade(PbOfflineData data)
        {
            data.facade = MogoProtoUtils.ParseByteArrToString(data.facade_bytes);
            if (_actor != null && _pbInfo.vocation == data.vocation && data.facade != null)
            {
                var avatarModelData = ModelEquipTools.CreateAvatarModelData((Vocation)data.vocation, data.facade);
                var clothInfo = avatarModelData.GetEquipInfo(ACTEquipmentType.Cloth);
                _actor.equipController.equipCloth.PutOn(clothInfo.equipID, clothInfo.particle, clothInfo.flow);
                _actor.equipController.equipWeapon.PutOn(avatarModelData.GetEquipInfo(ACTEquipmentType.Weapon).equipID);
                _actor.equipController.equipWing.onLoadEquipFinished = OnWingGoLoaded;
                _actor.equipController.equipWing.PutOn(avatarModelData.GetEquipInfo(ACTEquipmentType.Wing).equipID);
            }
        }

        private void OnWingGoLoaded()
        {
            ModelComponentUtil.ChangeUIModelParam(_actor.gameObject);
        }

        private void OnAdd()
        {
            if (_pbInfo.dbid == PlayerAvatar.Player.dbid)
            {
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(74859), PanelIdEnum.MainUIField);
                return;
            }
            PbFriendAvatarInfo avatarInfo = new PbFriendAvatarInfo();
            avatarInfo.dbid = _pbInfo.dbid;
            FriendManager.Instance.AddFriend(avatarInfo);
        }

        public void SetData(PbGuildMemberInfo pbInfo)
        {
            _pbInfo = pbInfo;
            _textId.CurrentText.text = pbInfo.dbid.ToString();
            _textVocation.CurrentText.text = MogoLanguageUtil.GetContent((int)pbInfo.vocation);
            _textEnterTime.CurrentText.text = MogoTimeUtil.ToLocalTime(pbInfo.join_time, "yyyy-MM-dd");
            _textPosition.CurrentText.text = MogoLanguageUtil.GetContent(74626 + (int)pbInfo.guild_position - 1);
            _model.LoadAvatarModel((Vocation)pbInfo.vocation, "", OnActorLoaded);
            if (PlayerAvatar.Player.dbid != pbInfo.dbid)
            {
                RankingListManager.Instance.GetRankPlayerFacadeInfo(pbInfo.dbid);
            }
        }

        private void OnActorLoaded(ACTActor actor)
        {
            _actor = actor;
        }
    }
}
