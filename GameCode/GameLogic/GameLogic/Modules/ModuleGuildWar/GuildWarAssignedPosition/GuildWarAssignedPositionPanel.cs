﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarAssignedPositionPanel : BasePanel
    {
        private int _currentIndex = 0;
        private GuildData _guildData
        {
            get
            {
                return PlayerDataManager.Instance.GuildData;
            }
        }
        private List<PbGuildMemberInfo> _memberInfoList;
        private Dictionary<GuildWarData.MilitaryRank, int> _numDict = new Dictionary<GuildWarData.MilitaryRank, int>();

        private GuildWarAssginedPlayerInfo _playerInfo;
        private GuildWarAssignedDropDownList _dropDownList;
        private KScrollView _scrollView;
        private KList _list;
        private KButton _btnTips;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_fanhui");

            _playerInfo = AddChildComponent<GuildWarAssginedPlayerInfo>("Container_chongwuleft");
            _dropDownList = AddChildComponent<GuildWarAssignedDropDownList>("Container_chongwuright/Container_xiaokuang");
            _scrollView = GetChildComponent<KScrollView>("Container_chongwuright/ScrollView_chengyuanxinxi");
            _list = GetChildComponent<KList>("Container_chongwuright/ScrollView_chengyuanxinxi/mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _btnTips = GetChildComponent<KButton>("Button_Gantang");
            GetChildComponent<StateText>("Label_txtJieshi").CurrentText.text = MogoLanguageUtil.GetContent(111574);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildWarAssignedPosition; }
        }

        public override void OnClose()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnTips.onClick.AddListener(OnShowTips);
            _list.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            _list.onAllItemCreated.AddListener(OnCoroutineEnd);
            _dropDownList.onClick.AddListener(OnDropDownListClick);
            EventDispatcher.AddEventListener<int, Vector3>(GuildWarEvents.Show_Drop_Down_List, ShowDropDownList);
            EventDispatcher.AddEventListener<PbGuildChangeMilitary>(GuildWarEvents.Assigned_Position, AssignedPosition);
            EventDispatcher.AddEventListener(GuildEvents.Refresh_Member_List, RefreshMemberList);
        }

        private void RemoveListener()
        {
            _btnTips.onClick.RemoveListener(OnShowTips);
            _list.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            _list.onAllItemCreated.RemoveListener(OnCoroutineEnd);
            _dropDownList.onClick.RemoveListener(OnDropDownListClick);
            EventDispatcher.RemoveEventListener<int, Vector3>(GuildWarEvents.Show_Drop_Down_List, ShowDropDownList);
            EventDispatcher.RemoveEventListener<PbGuildChangeMilitary>(GuildWarEvents.Assigned_Position, AssignedPosition);
            EventDispatcher.RemoveEventListener(GuildEvents.Refresh_Member_List, RefreshMemberList);
        }

        public override void OnShow(object data)
        {
            AddListener();

            _memberInfoList = _guildData.GetMemberList();
            _memberInfoList.Sort(SortByOnline);
            RefreshPlayerInfo();
            GuildManager.Instance.GetMemberList();
        }

        private int SortByOnline(PbGuildMemberInfo x, PbGuildMemberInfo y)
        {
            if (x.online == 1 && y.online != 1)
            {
                return -1;
            }
            return 1;
        }

        private void RefreshPlayerInfo()
        {
            _playerInfo.SetData(_memberInfoList[_currentIndex]);
        }

        private void RefreshPlayerList()
        {
            _list.SetDataList<GuildWarAssignedPositionItem>(_memberInfoList, 1);
        }

        private void OnCoroutineEnd()
        {
            RefreshSelectFrame();
        }

        private void ShowDropDownList(int index, Vector3 screenPosition)
        {
            _currentIndex = index;
            RefreshPlayerInfo();
            RefreshSelectFrame();
            _dropDownList.Visible = true;
            Vector2 position;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_dropDownList.transform.parent.GetComponent<RectTransform>(), screenPosition, UIManager.Instance.UICamera, out position);
            Vector2 tempPosition = _dropDownList.transform.localPosition;
            _dropDownList.transform.localPosition = new Vector3(tempPosition.x, position.y + 161);
            _dropDownList.SetDataList<GuildWarAssignedDropDownItem>(GetContentList());
        }

        private void InitNumDict()
        {
            if (_numDict.Count == 0)
            {
                _numDict.Add(GuildWarData.MilitaryRank.knightLeader, 0);
                _numDict.Add(GuildWarData.MilitaryRank.knight, 0);
                _numDict.Add(GuildWarData.MilitaryRank.soldier, 0);
            }
            else
            {
                _numDict[GuildWarData.MilitaryRank.knightLeader] = 0;
                _numDict[GuildWarData.MilitaryRank.knight] = 0;
                _numDict[GuildWarData.MilitaryRank.soldier] = 0;
            }
        }

        private List<string> GetContentList()
        {
            InitNumDict();
            for(int i = 0;i < _memberInfoList.Count;i++)
            {
                switch(_memberInfoList[i].military)
                {
                    case (int)GuildWarData.MilitaryRank.knightLeader:
                        _numDict[GuildWarData.MilitaryRank.knightLeader]++;
                        break;
                    case (int)GuildWarData.MilitaryRank.knight:
                        _numDict[GuildWarData.MilitaryRank.knight]++;
                        break;
                    case 0:
                    case (int)GuildWarData.MilitaryRank.soldier:
                        _numDict[GuildWarData.MilitaryRank.soldier]++;
                        break;
                }
            }
            List<string> contentList = new List<string>();
            foreach(GuildWarData.MilitaryRank rank in Enum.GetValues(typeof(GuildWarData.MilitaryRank)))
            {
                contentList.Add(string.Format("{0}({1}/{2})",
                    guild_match_helper.GetMilitaryRankName((int)rank), _numDict[rank], guild_match_helper.GetMilitaryRankLimitNum((int)rank)));
            }
            return contentList;
        }

        private void AssignedPosition(PbGuildChangeMilitary pb)
        {
            _dropDownList.Visible = false;
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                GuildWarAssignedPositionItem item = _list.ItemList[i] as GuildWarAssignedPositionItem;
                PbGuildMemberInfo data = item.Data as PbGuildMemberInfo;
                if (pb.dbid == data.dbid)
                {
                    item.RefreshPosition();
                }
            }
        }

        private void OnDropDownListClick(DropDownList dropDownList, DropDownItem dropDownItem)
        {
            PbGuildMemberInfo memberInfo = _memberInfoList[_currentIndex];
            GuildWarManager.Instance.AssignedPosition(memberInfo.dbid, (dropDownItem.Index + 1));
        }

        private void OnSelectedIndexChanged(KList list, int index)
        {
            _currentIndex = index;
            _playerInfo.SetData(_memberInfoList[_currentIndex]);
            RefreshSelectFrame();
        }

        private void RefreshMemberList()
        {
            _memberInfoList = _guildData.GetMemberList();
            if (_currentIndex >= _memberInfoList.Count)
            {
                _currentIndex = _memberInfoList.Count - 1;
            }
            RefreshPlayerInfo();
            RefreshPlayerList();
        }

        private void RefreshSelectFrame()
        {
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                GuildWarAssignedPositionItem item = _list.ItemList[i] as GuildWarAssignedPositionItem;
                if (i == _currentIndex)
                {
                    item.ShowFrame();
                }
                else
                {
                    item.HideFrame();
                }
            }
        }

        private void OnShowTips()
        {
            RuleTipsDataWrapper wrapper = new RuleTipsDataWrapper(MogoLanguageUtil.GetContent(111575), _btnTips.GetComponent<RectTransform>(), RuleTipsDirection.TopLeft);
            UIManager.Instance.ShowPanel(PanelIdEnum.RuleTips, wrapper);
        }
    }
}
