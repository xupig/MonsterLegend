﻿using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildTryGuildRankItem : KList.KListItemBase
    {
        private RankContainer _rankContainer;
        private StateText _nameTxt;
        private StateText _bestTxt;
        private StateText _leaderTxt;

        private PbGuildBattleWeekRankInfo _data;

        protected override void Awake()
        {
            _rankContainer = AddChildComponent<RankContainer>("Container_mingci");
            _nameTxt = GetChildComponent<StateText>("Label_txtWanjia");
            _bestTxt = GetChildComponent<StateText>("Label_txtJifen");
            _leaderTxt = GetChildComponent<StateText>("Label_txtHuizhangmingzi");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbGuildBattleWeekRankInfo;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            _rankContainer.SetRank((int)_data.index);
            _nameTxt.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.name_bytes);
            _bestTxt.CurrentText.text = _data.point.ToString();
            _leaderTxt.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.leader_name_bytes);
        }

        public override void Dispose()
        {
            _data = null;
        }
    }
}
