﻿using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildTryRecordItem : KList.KListItemBase
    {
        private RankContainer _rankContainer;
        private StateText _dateTxt;
        private StateText _totalScoreTxt;
        private List<StateText> _personTxtList;

        private PbGuildBattleLogInfo _data;

        protected override void Awake()
        {
            _rankContainer = AddChildComponent<RankContainer>("Container_mingci");
            _dateTxt = GetChildComponent<StateText>("Label_txtRiqi");
            _totalScoreTxt = GetChildComponent<StateText>("Label_txtJifen");
            _personTxtList = new List<StateText>();
            _personTxtList.Add(GetChildComponent<StateText>("Container_wanjia1/Label_txtWanjia"));
            _personTxtList.Add(GetChildComponent<StateText>("Container_wanjia1/Label_txtJifen02"));
            _personTxtList.Add(GetChildComponent<StateText>("Container_wanjia2/Label_txtWanjia"));
            _personTxtList.Add(GetChildComponent<StateText>("Container_wanjia2/Label_txtJifen02"));
            _personTxtList.Add(GetChildComponent<StateText>("Container_wanjia3/Label_txtWanjia"));
            _personTxtList.Add(GetChildComponent<StateText>("Container_wanjia3/Label_txtJifen02"));
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbGuildBattleLogInfo;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            string date = _data.date.ToString();
            _rankContainer.SetRank((int)_data.index);
            _dateTxt.CurrentText.text = string.Format("{0}/{1}/{2}", date.Substring(0, 4), date.Substring(4, 2), date.Substring(6, 2));
            _totalScoreTxt.CurrentText.text = _data.point.ToString();
            for (int i = 0; i < _personTxtList.Count; i++)
            {
                _personTxtList[i].CurrentText.text = "--";
            }
            for (int i = 0; i < _data.logs.Count; i++)
            {
                _personTxtList[2 * i].CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.logs[i].name_bytes);
                _personTxtList[2 * i + 1].CurrentText.text = _data.logs[i].point.ToString();
            }
        }

        public override void Dispose()
        {
            _data = null;
        }
    }
}
