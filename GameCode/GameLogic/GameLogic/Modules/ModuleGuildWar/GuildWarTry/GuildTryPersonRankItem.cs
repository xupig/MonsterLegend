﻿using Common.Data;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildTryPersonRankItem : KList.KListItemBase
    {
        private RankContainer _rankContainer;
        private StateText _nameTxt;
        private StateText _scoreTxt;
        private StateText _positionTxt;
        private StateText _fightTxt;
        private StateText _rewardNum;
        private IconContainer _rewardIcon;

        private PbGuildBattleRankInfo _data;

        protected override void Awake()
        {
            _rankContainer = AddChildComponent<RankContainer>("Container_mingci");
            _nameTxt = GetChildComponent<StateText>("Label_txtWanjia");
            _scoreTxt = GetChildComponent<StateText>("Label_txtJifen");
            _positionTxt = GetChildComponent<StateText>("Label_txtZhiwei");
            _fightTxt = GetChildComponent<StateText>("Label_txtZhandouli");
            _rewardNum = GetChildComponent<StateText>("Label_txtJiangli");
            _rewardIcon = AddChildComponent<IconContainer>("Container_icon");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as PbGuildBattleRankInfo;
                SetDataDirty();
            }
        }

        public override void DoRefreshData()
        {
            base.DoRefreshData();
            _rankContainer.SetRank((int)_data.index);
            _nameTxt.CurrentText.text = MogoProtoUtils.ParseByteArrToString(_data.name_bytes);
            _scoreTxt.CurrentText.text = _data.point.ToString();
            _positionTxt.CurrentText.text = MogoLanguageUtil.GetContent(74626 + (int)_data.position - 1);
            _fightTxt.CurrentText.text = _data.fight.ToString();
            int rewardId = guild_rank_reward_helper.GetRewardId((int)_data.index);
            List<RewardData> rewardDataList = item_reward_helper.GetRewardDataList(rewardId, PlayerAvatar.Player.vocation);
            if (rewardDataList == null || rewardDataList.Count == 0) { return; }
            _rewardNum.CurrentText.text = string.Format("x{0}", rewardDataList[0].num);
            _rewardIcon.SetIcon(item_helper.GetIcon(rewardDataList[0].id));
        }

        public override void Dispose()
        {
            _data = null;
        }
    }
}
