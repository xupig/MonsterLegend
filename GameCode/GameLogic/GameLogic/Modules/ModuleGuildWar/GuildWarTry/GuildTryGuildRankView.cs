﻿using Common.Base;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildTryGuildRankView : KContainer
    {
        private StateText _descTxt;
        private StateText _rankTxt;
        private StateText _notRankTxt;
        private KScrollView _scrollView;
        private KList _list;
        private List<PbGuildBattleWeekRankInfo> _dataList = new List<PbGuildBattleWeekRankInfo>();

        protected override void Awake()
        {
            base.Awake();
            _descTxt = GetChildComponent<StateText>("Label_txtXinxi");
            _rankTxt = GetChildComponent<StateText>("Container_biaotilan/Label_txtGonghuipaiming");
            _notRankTxt = GetChildComponent<StateText>("Label_txtmeiyoushuju");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_zhanlibang");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }


        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            RefreshContent();
            OnReRequest(_scrollView.ScrollRect);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void RefreshContent()
        {
            _rankTxt.CurrentText.text = MogoLanguageUtil.GetContent(111555);
            int day = int.Parse(guild_match_helper.GetGuildMatchParam(GuildMatchId.testwar_guild_score));
            List<int> openList = guild_match_helper.GetOpenDay(public_config.GUILD_BATTLE_STATE_TESTING);
            _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(111557, day, openList[openList.Count - 1], guild_match_helper.GetEnterMatchGuildCount((int)PlayerDataManager.Instance.GuildWarData.Stage.format));
            _notRankTxt.Visible = false;
        }

        private void RefreshListContent(PbGuildBattleWeekRankInfoList list)
        {
            _dataList.AddRange(list.rank);
            if (list.index != 0)
            {
                _rankTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(111555), list.index);
            }
            else
            {
                _rankTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(111555), MogoLanguageUtil.GetContent(111543));
            }
            if (_dataList.Count == 0)
            {
                _notRankTxt.Visible = true;
            }
            else
            {
                _notRankTxt.Visible = false;
            }
            _scrollView.ScrollRect.StopMovement();
            _list.SetDataList<GuildTryGuildRankItem>(_dataList, 4);
        }

        private void OnRequestNext(KScrollRect arg0)
        {
            GuildWarManager.Instance.GetGuildWarWeekRank(_dataList.Count + 1, 10);
        }

        private void OnReRequest(KScrollRect arg0)
        {
            _dataList.Clear();
            GuildWarManager.Instance.GetGuildWarWeekRank(1, 10);
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<PbGuildBattleWeekRankInfoList>(GuildWarEvents.Refresh_Guild_War_Try_Week_Rank, RefreshListContent);
            _scrollView.ScrollRect.onReRequest.AddListener(OnReRequest);
            _scrollView.ScrollRect.onRequestNext.AddListener(OnRequestNext);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<PbGuildBattleWeekRankInfoList>(GuildWarEvents.Refresh_Guild_War_Try_Week_Rank, RefreshListContent);
            _scrollView.ScrollRect.onReRequest.RemoveListener(OnReRequest);
            _scrollView.ScrollRect.onRequestNext.RemoveListener(OnRequestNext);
        }

        private void OnEntryBtnClick()
        {

        }

    }
}
