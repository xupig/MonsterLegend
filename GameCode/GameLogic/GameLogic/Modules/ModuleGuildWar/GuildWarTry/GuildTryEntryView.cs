﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuildWar
{
    public class GuildTryEntryView : KContainer
    {
        private KButton _flowBtn;
        private KButton _entryBtn;
        private StateText _openTxt;
        private StateText _countTxt;
        private StateText _scoreTxt;
        private StateText _rankTxt;
        private StateText _descTxt;
        private StateText _notRankTxt;
        private KContainer _rewardContainer;
        private StateText _rewardNameTxt;
        private StateText _rewardNumTxt;
        private ItemGrid _rewardItem;

        protected override void Awake()
        {
            base.Awake();
            _flowBtn = GetChildComponent<KButton>("Button_chakanliucheng");
            _entryBtn = GetChildComponent<KButton>("Button_jinru");
            _openTxt = GetChildComponent<StateText>("Container_left/Label_txtKaifangshijian");
            _countTxt = GetChildComponent<StateText>("Container_left/Label_txtJinrucishu");
            _scoreTxt = GetChildComponent<StateText>("Container_left/Label_txtGerenzuigaojifen");
            _rankTxt = GetChildComponent<StateText>("Container_left/Label_txtJifenpaiming");
            _descTxt = GetChildComponent<StateText>("Container_left/Label_txtGuanxi");
            _notRankTxt = GetChildComponent<StateText>("Container_right/Label_txtGonghuigongxian");
            _rewardContainer = GetChildComponent<KContainer>("Container_right/Container_item01");
            _rewardNameTxt = _rewardContainer.GetChildComponent<StateText>("Label_txtGonghuigongxian");
            _rewardNumTxt = _rewardContainer.GetChildComponent<StateText>("Label_txtShuliang");
            _rewardItem = _rewardContainer.GetChildComponent<ItemGrid>("Container_wupin");
            _entryBtn.Label.ChangeAllStateText(MogoLanguageUtil.GetContent(111544));
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            RefreshContent();
            GuildWarManager.Instance.GetGuildWarExpInfo();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void RefreshContent()
        {
            _openTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(111537), guild_match_helper.GetOpenContent(public_config.GUILD_BATTLE_STATE_TESTING));
            //_countTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(111538));
            _scoreTxt.CurrentText.text = MogoLanguageUtil.GetContent(111539);
            _rankTxt.CurrentText.text = MogoLanguageUtil.GetContent(111540);
            _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(111541);
            _notRankTxt.Visible = true;
            _rewardContainer.Visible = false;
        }

        private void RefreshInfoContent(PbGuildExpInfo info)
        {
            _scoreTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(111539), info.point);
            if (info.index != 0)
            {
                _rankTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(111540), info.index);
                _notRankTxt.Visible = false;
                RefreshRewardContent((int)info.index);
            }
            else
            {
                _rankTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(111540), MogoLanguageUtil.GetContent(111543));
            }
        }

        private void RefreshRewardContent(int rank)
        {
            _rewardContainer.Visible = true;
            int rewardId = guild_rank_reward_helper.GetRewardId(rank);
            List<RewardData> rewardDataList = item_reward_helper.GetRewardDataList(rewardId, PlayerAvatar.Player.vocation);
            if (rewardDataList == null || rewardDataList.Count == 0) { return; }
            RewardData data = rewardDataList[0];
            _rewardNameTxt.CurrentText.text = item_helper.GetName(data.id);
            _rewardNumTxt.CurrentText.text = string.Format("x{0}", data.num);
            _rewardItem.SetItemData(data.id);
            
        }

        private void AddEventListener()
        {
            _flowBtn.onClick.AddListener(OnFlowBtnClick);
            _entryBtn.onClick.AddListener(OnEntryBtnClick);
            EventDispatcher.AddEventListener<PbGuildExpInfo>(GuildWarEvents.Refresh_Guild_War_Try_Info, RefreshInfoContent);
        }

        private void RemoveEventListener()
        {
            _flowBtn.onClick.RemoveListener(OnFlowBtnClick);
            _entryBtn.onClick.RemoveListener(OnEntryBtnClick);
            EventDispatcher.RemoveEventListener<PbGuildExpInfo>(GuildWarEvents.Refresh_Guild_War_Try_Info, RefreshInfoContent);
        }

        private void OnEntryBtnClick()
        {
            CopyLogicManager.Instance.RequestEnterCopy(ChapterType.TeamCopy, () =>
            {
                MissionManager.Instance.RequsetEnterMission(GuildWarData.GUILD_TRY_MISSION_ID);
            }, GuildWarData.GUILD_TRY_MISSION_ID);
        }

        private void OnFlowBtnClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarFlow);
        }

    }
}
