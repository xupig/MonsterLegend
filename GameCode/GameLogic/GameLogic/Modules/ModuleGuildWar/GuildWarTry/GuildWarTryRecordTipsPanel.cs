﻿using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarTryRecordTipsPanel : BasePanel
    {
        private KScrollView _scrollView;
        private KList _list;
        private StateText _noneTxt;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildWarTryTips; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_zhanlibang");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _noneTxt = GetChildComponent<StateText>("Label_txtmeiyoushuju");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            _noneTxt.Visible = false;
            GuildWarManager.Instance.GetGuildWarTryLog();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void RefreshListContent(PbGuildBattleLogInfoList dataList)
        {
            _list.SetDataList<GuildTryRecordItem>(dataList.guild_battle_log_info);
            _noneTxt.Visible = dataList.guild_battle_log_info.Count == 0;
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<PbGuildBattleLogInfoList>(GuildWarEvents.Refresh_Guild_War_Try_Log, RefreshListContent);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<PbGuildBattleLogInfoList>(GuildWarEvents.Refresh_Guild_War_Try_Log, RefreshListContent);
        }

      
    }
}