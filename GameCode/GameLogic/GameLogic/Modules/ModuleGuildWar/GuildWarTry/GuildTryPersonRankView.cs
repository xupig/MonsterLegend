﻿using Common.Base;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuildWar
{
    public class GuildTryPersonRankView : KContainer
    {
        private StateText _descTxt;
        private StateText _rankTxt;
        private StateText _personScoreTxt;
        private StateText _guildScoreTxt;
        private KButton _scoreBtn;
        private StateText _notRankTxt;
        private KScrollView _scrollView;
        private KList _list;

        private List<PbGuildBattleRankInfo> _dataList = new List<PbGuildBattleRankInfo>();

        protected override void Awake()
        {
            base.Awake();
            _personScoreTxt = GetChildComponent<StateText>("Container_biaotilan/Label_txtGerendangqianjifen");
            _guildScoreTxt = GetChildComponent<StateText>("Container_biaotilan/Label_txtGonghuizongjifen");
            _descTxt = GetChildComponent<StateText>("Container_biaotilan/Label_txtJieshi");
            _rankTxt = GetChildComponent<StateText>("Container_biaotilan/Label_txtGerenpaiming");
            _notRankTxt = GetChildComponent<StateText>("Label_txtmeiyoushuju");
            _scoreBtn = GetChildComponent<KButton>("Container_biaotilan/Button_chakanjifen");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_zhanlibang");
            _list = _scrollView.GetChildComponent<KList>("mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
            RefreshContent();
            GuildWarManager.Instance.GetGuildWarExpInfo();
            OnReRequest(_scrollView.ScrollRect);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void RefreshContent()
        {
            _personScoreTxt.CurrentText.text = MogoLanguageUtil.GetContent(111546);
            _rankTxt.CurrentText.text = MogoLanguageUtil.GetContent(111548);
            _guildScoreTxt.CurrentText.text = MogoLanguageUtil.GetContent(111550);
            List<int> paramList = MogoStringUtils.Convert2List(guild_match_helper.GetConfig((int)GuildMatchId.testwar_player_score).__value);
            _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(111552, paramList[0], paramList[1]);
            _notRankTxt.Visible = false;
        }

        private void RefreshInfoContent(PbGuildExpInfo info)
        {
            _personScoreTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(111546), info.point);
            if (info.index != 0)
            {
                _rankTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(111540), info.index);
            }
            else
            {
                _rankTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(111540), MogoLanguageUtil.GetContent(111543));
            }
            _guildScoreTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(111550), info.sum_point);
        }

        private void RefreshListContent(PbGuildBattleRankInfoList list)
        {
            _dataList.AddRange(list.guild_battle_rank_info);
            if (_dataList.Count == 0)
            {
                _notRankTxt.Visible = true;
            }
            else
            {
                _notRankTxt.Visible = false;
            }
            _scrollView.ScrollRect.StopMovement();
            _list.SetDataList<GuildTryPersonRankItem>(_dataList, 4);
        }

        private void OnRequestNext(KScrollRect arg0)
        {
            GuildWarManager.Instance.GetGuildWarExpRank(_dataList.Count + 1, 10);
        }

        private void OnReRequest(KScrollRect arg0)
        {
            _dataList.Clear();
            GuildWarManager.Instance.GetGuildWarExpRank(1, 10);
        }

        private void AddEventListener()
        {
            _scoreBtn.onClick.AddListener(OnScoreBtnClick);
            EventDispatcher.AddEventListener<PbGuildExpInfo>(GuildWarEvents.Refresh_Guild_War_Try_Info, RefreshInfoContent);
            EventDispatcher.AddEventListener<PbGuildBattleRankInfoList>(GuildWarEvents.Refresh_Guild_War_Try_Person_Rank, RefreshListContent);
            _scrollView.ScrollRect.onReRequest.AddListener(OnReRequest);
            _scrollView.ScrollRect.onRequestNext.AddListener(OnRequestNext);
        }

        private void RemoveEventListener()
        {
            _scoreBtn.onClick.RemoveListener(OnScoreBtnClick);
            EventDispatcher.RemoveEventListener<PbGuildExpInfo>(GuildWarEvents.Refresh_Guild_War_Try_Info, RefreshInfoContent);
            EventDispatcher.RemoveEventListener<PbGuildBattleRankInfoList>(GuildWarEvents.Refresh_Guild_War_Try_Person_Rank, RefreshListContent);
            _scrollView.ScrollRect.onReRequest.RemoveListener(OnReRequest);
            _scrollView.ScrollRect.onRequestNext.RemoveListener(OnRequestNext);
        }

        private void OnScoreBtnClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarTryTips);
        }


    }
}
