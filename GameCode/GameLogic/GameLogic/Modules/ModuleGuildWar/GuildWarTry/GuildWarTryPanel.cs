﻿using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarTryPanel : BasePanel
    {
        private CategoryList _categoryList;
        private GuildTryEntryView _entryView;
        private GuildTryGuildRankView _guildView;
        private GuildTryPersonRankView _personView;

        private const int CATEGORY_INDEX_ENTRY = 0;
        private const int CATEGORY_INDEX_PERSON = 1;
        private const int CATEGORY_INDEX_GUILD = 2;

        private bool _needReturn = true;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildWarTry; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            InitCategoryList();
            _entryView = AddChildComponent<GuildTryEntryView>("Container_shilianchang");
            _guildView = AddChildComponent<GuildTryGuildRankView>("Container_gonghui");
            _personView = AddChildComponent<GuildTryPersonRankView>("Container_geren");
        }

        private void InitCategoryList()
        {
            List<CategoryItemData> listToggleData = new List<CategoryItemData>();
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(111534)));
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(111535)));
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(111536)));
            _categoryList.RemoveAll();
            _categoryList.SetDataList<CategoryListItem>(listToggleData);
        }

        protected override void OnCloseBtnClick()
        {
            base.OnCloseBtnClick();
            if (_needReturn == true)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.GuildWar);
            }
        }

        public override void OnShow(object data)
        {
            _needReturn = true;
            if (data != null)
            {
                _needReturn = (bool)data;
            }
            AddEventListener();
            ShowEntryView();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _categoryList.onSelectedIndexChanged.AddListener(OnCategoryChange);
        }

        private void RemoveEventListener()
        {
            _categoryList.onSelectedIndexChanged.RemoveListener(OnCategoryChange);
        }

        private void OnCategoryChange(CategoryList target, int index)
        {
            switch (index)
            {
                case CATEGORY_INDEX_ENTRY:
                    ShowEntryView();
                    break;
                case CATEGORY_INDEX_PERSON:
                    ShowPersonView();
                    break;
                case CATEGORY_INDEX_GUILD:
                    ShowGuildView();
                    break;
            }
        }

        private void ShowGuildView()
        {
            _entryView.Visible = false;
            _personView.Visible = false;
            _guildView.Visible = true;
        }

        private void ShowPersonView()
        {
            _entryView.Visible = false;
            _personView.Visible = true;
            _guildView.Visible = false;
        }

        private void ShowEntryView()
        {
            _entryView.Visible = true;
            _personView.Visible = false;
            _guildView.Visible = false;
        }

       
    }
}