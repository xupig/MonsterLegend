﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarBattlePreviewPanel : BasePanel
    {
        private StateText _titileTxt;
        private GuildWarBattlePreviewItem _leftItem;
        private GuildWarBattlePreviewItem _rightItem;

        private uint _timerId;
        private CopyGuildWarBaseLogic _logic;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildWarBattlePreview; }
        }

        protected override void Awake()
        {
            _leftItem = AddChildComponent<GuildWarBattlePreviewItem>("Container_left");
            _rightItem = AddChildComponent<GuildWarBattlePreviewItem>("Container_right");
            _titileTxt = GetChildComponent<StateText>("Label_txtLunshu");
        }

        public override void OnShow(object data)
        {
            _logic = data as CopyGuildWarBaseLogic;
            RefreshContent();
        }

        public override void OnClose()
        {
            ResetTimer();
            PlayerDataManager.Instance.GuildWarData.EnemyData = null;
        }

        private void RefreshContent()
        {
            ResetTimer();
            uint waitTime = uint.Parse(global_params_helper.GetGlobalParam(GlobalParamId.guild_match_showtime)) * 1000;
            _timerId = TimerHeap.AddTimer(waitTime, 0, HideView);
            if (_logic.NeedShowPreviewTitle())
            {
                _titileTxt.CurrentText.text = MogoLanguageUtil.GetContent(111627, PlayerDataManager.Instance.GuildWarData.EnemyData.round);
            }
            else
            {
                _titileTxt.Clear();
            }
            _leftItem.RefreshContent(_logic.LeftWrapper);
            _rightItem.RefreshContent(_logic.RightWrapper);
        }

        private void HideView()
        {
            ResetTimer();
            ClosePanel();
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

    }
}