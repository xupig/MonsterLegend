﻿using Common.Data;
using Common.ExtendComponent;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleGuildWar
{
    public class GuildWarBattlePreviewItem : KContainer
    {
        private IconContainer _icon;
        private StateText _guildTxt;
        private StateText _militaryRankTxt;
        private StateText _levelTxt;
        private StateText _nameTxt;
        private StateText _fightTxt;

        protected override void Awake()
        {
            base.Awake();
            _icon = AddChildComponent<IconContainer>("Container_icon");
            _guildTxt = GetChildComponent<StateText>("Label_txtGonghuimingcheng");
            _militaryRankTxt = GetChildComponent<StateText>("Label_txtQishizhang");
            _levelTxt = GetChildComponent<StateText>("Container_dengji/Label_txtDengji");
            _nameTxt = GetChildComponent<StateText>("Label_txtJiaosemingcheng");
            _fightTxt = GetChildComponent<StateText>("Label_txtZhandouli");
        }

        //public void RefreshContent(bool isPlayer)
        //{
        //    if (isPlayer)
        //    {
        //        RefreshPlayerContent();
        //    }
        //    else
        //    {
        //        RefreshEnemyContent();
        //    }
        //}

        //private void RefreshEnemyContent()
        //{
        //    if (PlayerDataManager.Instance.GuildWarData.EnemyData != null)
        //    {
        //        GuildWarEnemyData data = PlayerDataManager.Instance.GuildWarData.EnemyData;
        //        _fightTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(110020), data.fight);
        //        _icon.SetIcon(MogoPlayerUtils.GetSmallIcon(data.vocation));
        //        _guildTxt.CurrentText.text = data.guildName;
        //        _militaryRankTxt.CurrentText.text = MogoLanguageUtil.GetContent(111516 + (int)data.military);
        //        _levelTxt.CurrentText.text = data.level.ToString();
        //        _nameTxt.CurrentText.text = data.name.ToString();
        //    }
        //}

        //private void RefreshPlayerContent()
        //{
        //    PlayerAvatar data = PlayerAvatar.Player;
        //    _fightTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(110020), data.fight_force);
        //    _icon.SetIcon(MogoPlayerUtils.GetSmallIcon((int)data.vocation));
        //    _guildTxt.CurrentText.text = PlayerDataManager.Instance.GuildData.MyGuildData.GuildName;
        //    _militaryRankTxt.CurrentText.text = MogoLanguageUtil.GetContent(111516 + (int)PlayerDataManager.Instance.GuildData.GetMyMemberInfo().military);
        //    _levelTxt.CurrentText.text = data.level.ToString();
        //    _nameTxt.CurrentText.text = data.name.ToString();
        //}

        public void RefreshContent(CopyPreviewItemDataWrapper wrapper)
        {
            _fightTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(110020), wrapper.FightValue);
            _icon.SetIcon(MogoPlayerUtils.GetSmallIcon(wrapper.Vocation));
            _guildTxt.CurrentText.text = wrapper.GuildName;
            _militaryRankTxt.CurrentText.text = wrapper.MilitaryRank;
            _levelTxt.CurrentText.text = wrapper.Level.ToString();
            _nameTxt.CurrentText.text = wrapper.Name.ToString();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }
    }
}
