﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game.UI.UIComponent;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using MogoEngine.Mgrs;
using GameMain.Entities;
using Common.ClientConfig;
using GameLoader.Utils.Timer;
using Common.Data;
using Game.UI;
using Common.Utils;
using Common.Structs.ProtoBuf;
using GameMain.GlobalManager.SubSystem;
using Common.Global;
using Common.ServerConfig;
using Common.ExtendComponent;
using MogoEngine.Utils;

namespace ModuleGuildWar
{
    public class GuildWarRightUpView : KContainer
    {
        private KDummyButton _oppenButton;
        private KDummyButton _closeButton;
        private KContainer _openContainer;
        private StateText _descTxt;

        private PbGuildBattleMatchCountdownResult _data;
        private uint _timerId;

        protected override void Awake()
        {
            _openContainer = GetChildComponent<KContainer>("Container_open");
            _oppenButton = AddChildComponent<KDummyButton>("Image_zhandoutishiSH");
            _closeButton = _openContainer.AddChildComponent<KDummyButton>("Image_zhandoutishiGU");
            _descTxt = _openContainer.GetChildComponent<StateText>("Label_txtMiaoshu");
            AddEventListener();
            CloseView();
            _openContainer.transform.localPosition = new Vector3(220, 0, 0);
        }

        private void AddEventListener()
        {
            _oppenButton.onClick.AddListener(OpenView);
            _closeButton.onClick.AddListener(CloseView);
            EventDispatcher.AddEventListener<PbGuildBattleMatchCountdownResult>(GuildWarEvents.Refresh_Guild_Match_Count_Down_Info, RefreshCountDownInfo);
        }

        private void RemoveEventListener()
        {
            _oppenButton.onClick.RemoveListener(OpenView);
            _closeButton.onClick.RemoveListener(CloseView);
            EventDispatcher.RemoveEventListener<PbGuildBattleMatchCountdownResult>(GuildWarEvents.Refresh_Guild_Match_Count_Down_Info, RefreshCountDownInfo);
        }

        private void RefreshCountDownInfo(PbGuildBattleMatchCountdownResult data)
        {
            uint second = data.next_guild_battle_time > Global.serverTimeStampSecond ? data.next_guild_battle_time - Global.serverTimeStampSecond : 0;
            if (data.battle_countdown_state == 0 || data.next_guild_battle_time == 0 || data.next_guild_battle_time < Global.serverTimeStampSecond)
            {
                CloseView();
                return;
            }
            _data = data;
            _data.guild_name = MogoProtoUtils.ParseByteArrToString(_data.guild_name_bytes);
            OpenView();
            ResetTimer();
            if (_data != null)
            {
                _timerId = TimerHeap.AddTimer(0, 100, OnStep);
            }
        }

        private void CloseView()
        {
            //TweenPosition.Begin(_openContainer.gameObject, 0.2f, new Vector2(220, 0), 0, UITweener.Method.EaseOut);
            TweenViewMove.Begin(_openContainer.gameObject, MoveType.Hide, MoveDirection.Right);
            _oppenButton.gameObject.SetActive(true);
            _closeButton.gameObject.SetActive(false);
        }

        private void OpenView()
        {
            //TweenPosition.Begin(_openContainer.gameObject, 0.2f, new Vector2(0, 0), 0, UITweener.Method.EaseOut);
            TweenViewMove.Begin(_openContainer.gameObject, MoveType.Show, MoveDirection.Right);
            _oppenButton.gameObject.SetActive(false);
            _closeButton.gameObject.SetActive(true);
        }

        private void OnStep()
        {
            if (_data.next_guild_battle_time == 0 || _data.next_guild_battle_time + 1 < Global.serverTimeStampSecond)
            {
                ResetTimer();
                RequestCountDownInfo();
                return;
            }
            uint leftTime = _data.next_guild_battle_time > Global.serverTimeStampSecond ? _data.next_guild_battle_time - Global.serverTimeStampSecond : 0;
            if (_data.battle_countdown_state == public_config.GUILD_BATTLE_COUNTDOWN_STATE1 || _data.battle_countdown_state == public_config.GUILD_BATTLE_COUNTDOWN_STATE4)
            {
                if (string.IsNullOrEmpty(_data.guild_name))
                {
                    _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(111503, leftTime);
                }
                else
                {
                    _descTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(111648, _data.guild_name), "\n", MogoLanguageUtil.GetContent(111503, leftTime));
                }
            }
            else if (_data.battle_countdown_state == public_config.GUILD_BATTLE_COUNTDOWN_STATE2 || _data.battle_countdown_state == public_config.GUILD_BATTLE_COUNTDOWN_STATE3)
            {
                if (_data.battle_match_round_count < 5)
                {
                    _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(111504, _data.battle_match_round_count + 1, leftTime);
                }
                else //最后一轮成员匹配
                {
                    if (_data.battle_match_round < 3)
                    {
                        _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(111635, leftTime);
                    }
                    else //最后一场
                    {
                        _descTxt.CurrentText.text = MogoLanguageUtil.GetContent(111636, leftTime);
                    }
                }
            }
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }

        private void RequestCountDownInfo()
        {
            _data = null;
            GuildWarManager.Instance.GetMatchCountDownInfo();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            _descTxt.CurrentText.text = string.Empty;
            AddEventListener();
            RequestCountDownInfo();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
            ResetTimer();
            _data = null;
        }

    }

}


