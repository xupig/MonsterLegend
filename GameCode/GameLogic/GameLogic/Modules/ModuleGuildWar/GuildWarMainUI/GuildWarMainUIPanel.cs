﻿using Common.Base;
using Common.Events;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using ModuleMainUI;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarMainUIPanel : BasePanel
    {
        private KButton _backHomeBtn;
        private KButton _battleBtn;
        private KButton _recordBtn;
        private KButton _memberBtn;
        private KContainer _topRightContainer;
        private GuildWarRightUpView _rightUpView;
        private GuildWarInfoPool _infoPool;
        private CanOperationEffect _effect;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.GuildWarMainUI; }
        }

        protected override void Awake()
        {
            _topRightContainer = GetChildComponent<KContainer>("Container_topRight");
            _backHomeBtn = GetChildComponent<KButton>("Container_topRight/Button_huicheng");
            _battleBtn = GetChildComponent<KButton>("Container_topRight/Button_gonghuizhan");
            _recordBtn = GetChildComponent<KButton>("Container_topRight/Button_zhankuang");
            _memberBtn = GetChildComponent<KButton>("Container_topRight/Button_chengyuan");
            _rightUpView = AddChildComponent<GuildWarRightUpView>("Container_topRight/Container_jindu");
            _infoPool = AddChildComponent<GuildWarInfoPool>("Container_pool");
            _effect = _recordBtn.gameObject.AddComponent<CanOperationEffect>();
            _effect.SetPoint(true);
        }

        public override void OnShow(object data)
        {
            AddEventListener();
            RefreshContent();
        }

        public override void OnClose()
        {
            RemoveEventListener();
        }

        private void RefreshContent()
        {

        }

        private void AddEventListener()
        {
            _backHomeBtn.onClick.AddListener(OnBackTownBtnClick);
            _battleBtn.onClick.AddListener(OnBattleBtnClick);
            _recordBtn.onClick.AddListener(OnRecordBtnClick);
            _memberBtn.onClick.AddListener(OnMemberBtnClick);
            EventDispatcher.AddEventListener<bool>(MainUIEvents.ON_FUNCTION_ENTRY_VIEW_OPEN, OnFunctionEntryViewOpen);
        }

        private void RemoveEventListener()
        {
            _backHomeBtn.onClick.RemoveListener(OnBackTownBtnClick);
            _battleBtn.onClick.RemoveListener(OnBattleBtnClick);
            _recordBtn.onClick.RemoveListener(OnRecordBtnClick);
            _memberBtn.onClick.RemoveListener(OnMemberBtnClick);
            EventDispatcher.RemoveEventListener<bool>(MainUIEvents.ON_FUNCTION_ENTRY_VIEW_OPEN, OnFunctionEntryViewOpen);
        }

        private void OnFunctionEntryViewOpen(bool isOpen)
        {
            _topRightContainer.Visible = isOpen == false;
        }

        private void OnMemberBtnClick()
        {
            //PanelIdEnum.GuildInformation.Show(new int[] { 1, 3 });
            PanelIdEnum.GuildWarAssignedPosition.Show();
        }

        private void OnRecordBtnClick()
        {
            //PanelIdEnum.GuildInformation.Show(new int[] { 1, 6 });
            GuildWarManager.Instance.GetGuildWarResultRecord(0, PlayerDataManager.Instance.GuildData.MyGuildData.Id, 0);
        }

        private void OnBattleBtnClick()
        {
            PanelIdEnum.GuildInformation.Show(new int[] { 3, 1 });
        }

        private void OnBackTownBtnClick()
        {
            if (PlayerAutoFightManager.GetInstance().fightType == FightType.FOLLOW_FIGHT)
            {
                MessageBox.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.ENSURE_CANCEL_FOLLOW_FIGHT), () =>
                {
                    PlayerAutoFightManager.GetInstance().Stop();
                    TeamManager.Instance.CancelFollowCaptain();
                    GameSceneManager.GetInstance().LeaveCombatScene();
                });
                return;
            }
            GameSceneManager.GetInstance().LeaveCombatScene();
        }

    }
}