﻿using Common.Events;
using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarInfoItem : KContainer
    {
        private StateText _militaryTxt;
        private StateText _nameTxt;
        private StateImage _swordLeft;
        private StateImage _swordRight;
        private StateImage _fightImg;
        private StateImage _winImg;
        private StateImage _loseImg;

        private RectTransform _rectTransform;

        protected override void Awake()
        {
            base.Awake();
            _militaryTxt = GetChildComponent<StateText>("Label_txtcreatureInfo1");
            _nameTxt = GetChildComponent<StateText>("Label_txtcreatureInfo2");
            _swordLeft = GetChildComponent<StateImage>("Image_jian1");
            _swordRight = GetChildComponent<StateImage>("Image_jian2");
            _fightImg = GetChildComponent<StateImage>("Image_fenzhanzhong");
            _winImg = GetChildComponent<StateImage>("Image_sheng");
            _loseImg = GetChildComponent<StateImage>("Image_fu");
            _rectTransform = transform as RectTransform;
        }

        private uint _entityId;

        public void Show(uint id)
        {
            _entityId = id;
            enabled = true;
            ShowAvatarInfo();
        }

        public void Hide()
        {
            _entityId = 0;
            enabled = false;
        }

        private bool IsActorExist(EntityAvatar entity)
        {
            if (entity == null || entity.controlActor == null)
            {
                Hide();
                return false;
            }
            return true;
        }

        private bool IsBillboardExist(Transform billboard)
        {
            if (billboard == null)
            {
                Hide();
                return false;
            }
            return true;
        }

        void Update()
        {
            if (Camera.main == null) { return; }
            EntityAvatar entity = MogoWorld.GetEntity(_entityId) as EntityAvatar;
            if (IsActorExist(entity) == false) return;
            Transform billboard = entity.controlActor.boneController.GetBoneByName("slot_billboard");
            if (IsBillboardExist(billboard) == false) return;
            CalculatePosition(entity, billboard);
        }

        private void CalculatePosition(EntityCreature entity, Transform billboard)
        {
            Vector2 screenPosition = Camera.main.WorldToScreenPoint(billboard.position);
            if (MogoGameObjectHelper.CheckPositionIsInScreen(screenPosition) == true)
            {
                SetPosition(screenPosition);
            }
            else
            {
                if (_rectTransform.localPosition != GuildWarInfoPool.HIDE_POSITION)
                {
                    _rectTransform.localPosition = GuildWarInfoPool.HIDE_POSITION;
                }
            }
        }

        private void SetPosition(Vector3 screenPosition)
        {
            Vector3 localPosition = screenPosition / Global.Scale;
            localPosition.y -= Screen.height / Global.Scale;
            localPosition.x -= 0.5f * _rectTransform.sizeDelta.x;
            localPosition.y += _rectTransform.sizeDelta.y;
            if (_rectTransform.localPosition != localPosition)
            {
                _rectTransform.localPosition = localPosition;
            }
        }

        private void ShowAvatarInfo()
        {
            RefreshName();
            RequestMilitary();
        }

        private void RequestMilitary()
        {
            GuildWarManager.Instance.GetGuildMilitary(_entityId);
        }

        private void RefreshMilitary(PbGuildMilitaryInfo pbInfo)
        {
            if (pbInfo.military == 0) return;
            if (pbInfo.entity_eid == _entityId)
            {
                if (_militaryTxt.Visible == false)
                {
                    _militaryTxt.Visible = true;
                }
                _militaryTxt.CurrentText.text = MogoLanguageUtil.GetContent(111637 - 1 + (int)pbInfo.military);
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddEventListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            EventDispatcher.AddEventListener<PbGuildMilitaryInfo>(GuildWarEvents.On_Get_Guild_Military_Info, RefreshMilitary);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<PbGuildMilitaryInfo>(GuildWarEvents.On_Get_Guild_Military_Info, RefreshMilitary);
        }

        private void RefreshName()
        {
            EntityAvatar entityAvatar = MogoWorld.GetEntity(_entityId) as EntityAvatar;
            if (_nameTxt.Visible == false)
            {
                _nameTxt.Visible = true;
            }
            _nameTxt.CurrentText.color = entityAvatar.dbid == PlayerAvatar.Player.dbid ? ColorDefine.GetColorById(ColorDefine.PLAYER_AVATAR_INFO_COLOR) : ColorDefine.GetColorById(ColorDefine.ENTITY_AVATAR_INFO_COLOR);
            _nameTxt.CurrentText.text = entityAvatar.name;
        }

        

    }
}
