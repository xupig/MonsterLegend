﻿using Common.Utils;
using Game.UI.UIComponent;
using GameMain.Entities;
using MogoEngine;
using MogoEngine.RPC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleGuildWar
{
    public class GuildWarInfoPool : KContainer
    {
        private GameObject _template;
        private Dictionary<uint, GuildWarInfoItem> _itemDict;
        private Stack<GuildWarInfoItem> _itemStack;
        private List<uint> _notExitList;

        public static readonly Vector3 HIDE_POSITION = new Vector3(-2000, -1200, 0);

        private float elapsed = 0f;
        private const float CHECK_INTERVAL = 1.0f;

        protected override void Awake()
        {
            base.Awake();
            _template = GetChild("Container_xinxi");
            _template.transform.localPosition = HIDE_POSITION;
            _itemDict = new Dictionary<uint, GuildWarInfoItem>();
            _itemStack = new Stack<GuildWarInfoItem>();
            _notExitList = new List<uint>();
            MogoGameObjectHelper.SetFullScreenSize(gameObject);
        }

        void Update()
        {
            elapsed += Time.deltaTime;
            if (elapsed > CHECK_INTERVAL)
            {
                elapsed -= CHECK_INTERVAL;
                _notExitList.Clear();
                foreach (uint id in _itemDict.Keys)
                {
                    if (MogoWorld.Entities.ContainsKey(id) == false)
                    {
                        _notExitList.Add(id);
                    }
                }
                foreach (uint id in _notExitList)
                {
                    RemoveItem(id);
                }
                foreach (uint id in MogoWorld.Entities.Keys)
                {
                    if (_itemDict.ContainsKey(id) == false)
                    {
                        CreateItem(id);
                    }
                }
            }
        }

        private GuildWarInfoItem AddGuildWarInfoItem()
        {
            GuildWarInfoItem item = MogoGameObjectHelper.AddByTemplate<GuildWarInfoItem>(_template);
            return item;
        }

        private void CreateItem(uint entityId)
        {
            EntityAvatar entity = MogoWorld.GetEntity(entityId) as EntityAvatar;
            if (entity != null)
            {
                if (_itemDict.ContainsKey(entityId) == false)
                {
                    GuildWarInfoItem item;
                    if (_itemStack.Count != 0)
                    {
                        item = _itemStack.Pop();
                    }
                    else
                    {
                        item = AddGuildWarInfoItem();
                    }
                    _itemDict.Add(entityId, item);
                    item.Show(entityId);
                }
            }
        }

        private void RemoveItem(uint entityId)
        {
            if (_itemDict.ContainsKey(entityId))
            {
                GuildWarInfoItem lifeBarView = _itemDict[entityId];
                lifeBarView.Hide();
                _itemStack.Push(lifeBarView);
                _itemDict.Remove(entityId);
            }
        }

        private void ClearPools()
        {
            while (_itemStack.Count > 0)
            {
                GuildWarInfoItem item = _itemStack.Pop();
                GameObject.Destroy(item.gameObject);
            }
            _itemStack.Clear();
            foreach (GuildWarInfoItem item in _itemDict.Values)
            {
                GameObject.Destroy(item.gameObject);
            }
            _itemDict.Clear();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            ClearPools();
        }
    }
}
