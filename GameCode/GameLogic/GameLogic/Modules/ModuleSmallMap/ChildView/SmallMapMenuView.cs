﻿using Common.Base;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using GameData;
using GameMain.GlobalManager.SubSystem;
using GameMain.GlobalManager;
using Common.ServerConfig;
using Common.Global;
using Common.Data;
using Common.Structs;
using GameMain.Entities;

namespace ModuleSmallMap
{
    public class SmallMapMenuView : KContainer
    {
        private const int ITEM_GAP = 4;
        private const int DETAIL_ITEM_GAP = 2;
        private List<KToggle> _menuToggleList;
        private List<KList> _detailListList;
        private List<Locater> _locaterList;
        private RectTransform _contentRect;

        private List<List<SmallMapDetailData>> _itemDataLisArray;


        protected override void Awake()
        {
            _menuToggleList = new List<KToggle>();
            _detailListList = new List<KList>();
            _locaterList = new List<Locater>();

            for (int i = 0; i < 5; i++)
            {
                KToggle menuToggle = GetChildComponent<KToggle>("mask/content/Toggle_menu" + i.ToString());
                _locaterList.Add(menuToggle.gameObject.AddComponent<Locater>());
                menuToggle.Index = i;
                _menuToggleList.Add(menuToggle);
                KList detailList = GetChildComponent<KList>("mask/content/List_detail" + i.ToString());
                _locaterList.Add(detailList.gameObject.AddComponent<Locater>());
                detailList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
                detailList.SetPadding(0, 0, 0, 0);
                detailList.SetGap(DETAIL_ITEM_GAP, 0);
                detailList.RecalculateSize();
                _detailListList.Add(detailList);
                _contentRect = GetChildComponent<RectTransform>("mask/content");
            }
            AddEventListener();
        }

        private void AddEventListener()
        {
            for (int i = 0; i < _menuToggleList.Count; i++)
            {
                _menuToggleList[i].onValueChanged.AddListener(OnMenuToggle);
            }
        }

        private void OnMenuToggle(KToggle target, bool isOn)
        {
            int index = target.Index;
            if (isOn == true)
            {
                if (count != 0)
                {
                    count = 0;
                    KList detailList = _detailListList[showIndex];
                    showIndex = -1;
                    for (int i = 0; i < detailList.ItemList.Count; i++)
                    {
                        detailList.ItemList[i].transform.localPosition = new Vector3(detailList.ItemList[i].transform.localPosition.x, detailList.Padding.top - (43 + DETAIL_ITEM_GAP) * i, detailList.ItemList[i].transform.localPosition.z);
                    }
                    detailList.RecalculateSize();
                    for (int i = 0; i < showTweenList.Count;i++ )
                    {
                        showTweenList[i].enabled = false;
                    }
                    RefreshLayout();
                }
                _detailListList[index].Visible = true;
                ShowMenuAnimation(target.transform.localPosition, index);
            }
            else
            {
                if(hideCount != 0)
                {
                    hideCount = 0;
                    _detailListList[hideIndex].Visible = false;
                    for (int i = 0; i <  _detailListList[hideIndex].ItemList.Count; i++)
                    {
                        _detailListList[hideIndex].ItemList[i].Visible = false;
                    }
                    _detailListList[hideIndex].RecalculateSize();
                    hideIndex = -1;
                    RefreshLayout();
                   
                }
                ShowMenuCloseAnimation(target.transform.localPosition, index);
            }
        }

        private void ShowMenuCloseAnimation(Vector3 vector3, int index)
        {
            hideIndex = index;
            KList detailList = _detailListList[index];
            float y = _locaterList[2 * index].Y;
            y -= _locaterList[2 * index].Height + ITEM_GAP;
            for (int i = 0; i < detailList.ItemList.Count; i++)
            {
                hideCount++;
                TweenPosition.Begin(detailList.ItemList[i].gameObject, 0.2f, new Vector3(detailList.ItemList[i].transform.localPosition.x, detailList.Padding.top, detailList.ItemList[i].transform.localPosition.z), onTweenEnd);
            }
            for (int i = 2 * index + 2; i < _locaterList.Count; i++)
            {
                Locater locater = _locaterList[i];
                if (locater.gameObject.activeSelf == true)
                {
                    hideCount++;
                    TweenPosition.Begin(locater.gameObject, 0.2f, new Vector3(locater.transform.localPosition.x, y, locater.transform.localPosition.z), onTweenEnd);
                    y -= locater.Height + ITEM_GAP;
                }
            }

        }

        private int hideCount = 0;
        private int hideIndex = -1;
       

        private void onTweenEnd(UITweener tween)
        {
            hideCount -= 1;
            if (hideCount <= 0)
            {
                _detailListList[hideIndex].Visible = false;
                for (int i = 0; i < _detailListList[hideIndex].ItemList.Count; i++)
                {
                    _detailListList[hideIndex].ItemList[i].Visible = false;
                }
                _detailListList[hideIndex].RecalculateSize();
                hideIndex = -1;
                RefreshLayout();
                if(_bossList!=null)
                {
                    RefreshBoss(_bossList);
                }
                if(_gateList!=null)
                {
                    RefreshGate(_gateList);
                }
            }
        }

        protected override void OnDisable()
        {
            showIndex = -1;
            count = 0;
            hideCount = 0;
            hideIndex = -1;
            showTweenList.Clear();
            base.OnDisable();
        }

        protected override void OnDestroy()
        {
            showTweenList.Clear();
            base.OnDestroy();
        }

        private int GetListLocaterByMenuIndex(int index)
        {
            return 2*index+1;
        }

        private void ShowMenuAnimation(Vector3 startPosition, int index)
        {
            showIndex = index;
            KList detailList = _detailListList[index];
            float y = _locaterList[2 * index].Y;
            y -= _locaterList[2 * index].Height + ITEM_GAP;
            float startPosY = y;
            _locaterList[2 * index + 1].Position = new Vector2(_locaterList[2 * index + 1].X, startPosY);
            for (int i = 0; i < detailList.ItemList.Count; i++)
            {
                detailList.ItemList[i].Visible = true;
                detailList.ItemList[i].transform.localPosition = new Vector3(detailList.ItemList[i].transform.localPosition.x,detailList.Padding.top,detailList.ItemList[i].transform.localPosition.z);
                count++;
                showTweenList.Add(TweenPosition.Begin(detailList.ItemList[i].gameObject, 0.2f, new Vector3(detailList.ItemList[i].transform.localPosition.x, detailList.Padding.top - (43 + DETAIL_ITEM_GAP) * i, detailList.ItemList[i].transform.localPosition.z), OnTweenShowEnd));
                y -= 46;
            }
            y -= ITEM_GAP;
            for(int i=2*index+2;i<_locaterList.Count;i++)
            {
                Locater locater = _locaterList[i];
                if (locater.gameObject.activeSelf == true)
                {
                    count++;
                    showTweenList.Add(TweenPosition.Begin(locater.gameObject, 0.2f, new Vector3(locater.transform.localPosition.x, y, locater.transform.localPosition.z), OnTweenShowEnd));
                    y -= locater.Height + ITEM_GAP;
                }
            }
        }

        private int count = 0;
        private int showIndex = -1;
        private List<TweenPosition> showTweenList = new List<TweenPosition>();
        private void OnTweenShowEnd(UITweener tween)
        {
            count -= 1;
            if(count<=0)
            {
                _detailListList[showIndex].RecalculateSize();
                showIndex = -1;
                showTweenList.Clear();
                RefreshLayout();
                if (_bossList != null)
                {
                    RefreshBoss(_bossList);
                }
                if (_gateList != null)
                {
                    RefreshGate(_gateList);
                }
            }
        }


        public void SetMenuLabel(List<string> labelList)
        {
            for (int i = 0; i < _menuToggleList.Count; i++)
            {
                _menuToggleList[i].Visible = false;
            }
            for (int i = 0; i < labelList.Count; i++)
            {
                KToggle toggle = _menuToggleList[i];
                toggle.Visible = true;
                toggle.isOn = false;
                StateText backLabel = toggle.GetChildComponent<StateText>("image/label");
                StateText markLabel = toggle.GetChildComponent<StateText>("checkmark/label");
                backLabel.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
                backLabel.ChangeAllStateText(labelList[i]);
                markLabel.ChangeAllStateTextAlignment(TextAnchor.UpperCenter);
                markLabel.ChangeAllStateText(labelList[i]);
            }
        }


        private void Refresh()
        {
            for (int i = 0; i < _menuToggleList.Count;i++ )
            {
                if(_menuToggleList[i].isOn)
                {
                    ShowDetailList(i);
                }
                else
                {
                    HideDetailList(i);
                }
            }
            RefreshLayout();
        }

        public void SetDataList(List<List<SmallMapDetailData>> dataList)
        {
            _itemDataLisArray = dataList;
            for(int i=0;i<_itemDataLisArray.Count;i++)
            {
                _detailListList[i].Visible = true;
                _detailListList[i].RemoveAll();
                _detailListList[i].SetDataList<SmallMapDetailItem>(_itemDataLisArray[i]);
                _detailListList[i].Visible = _menuToggleList[i].isOn;
            }
            _contentRect.anchoredPosition = Vector2.zero;
            RefreshLayout();
        }

        private List<SmallMapDetailData> _bossList;
        private List<SmallMapDetailData> _gateList;
        public void RefreshBoss(List<SmallMapDetailData> bossList)
        {
            if (count != 0 || hideCount != 0)
            {
                _bossList = bossList;
            }
            else
            {
                _detailListList[3].Visible = true;
                _detailListList[3].RemoveAll();
                _detailListList[3].SetDataList<SmallMapDetailItem>(bossList);
                _detailListList[3].Visible = _menuToggleList[3].isOn;
                _bossList = null;
            }
            RefreshLayout();
        }

        public void RefreshGate(List<SmallMapDetailData> gateList)
        {
            if (count != 0 || hideCount != 0)
            {
                _gateList = gateList;
            }
            else
            {
                _detailListList[4].Visible = true;
                _detailListList[4].RemoveAll();
                _detailListList[4].SetDataList<SmallMapDetailItem>(gateList);
                _detailListList[4].Visible = _menuToggleList[4].isOn;
                _gateList = null;
            }
            RefreshLayout();
        }

        private void ShowDetailList(int index)
        {
            if (_itemDataLisArray[index].Count > 0)
            {
                _detailListList[index].Visible = true;
                _detailListList[index].SetDataList<SmallMapDetailItem>(_itemDataLisArray[index]);
            }
        }

        private void HideDetailList(int index)
        {
            _detailListList[index].Visible = false;
        }

        private void RefreshLayout()
        {
            float y = _locaterList[0].Y;
            float height = 0;
            for (int i = 0; i < _locaterList.Count; i++)
            {
                Locater locater = _locaterList[i];
                if (locater.gameObject.activeSelf == true)
                {
                    locater.Y = y;
                    y -= locater.Height + ITEM_GAP;
                    height += locater.Height + ITEM_GAP;
                }
            }
            _contentRect.sizeDelta = new Vector2(_contentRect.sizeDelta.x, height);
        }

    }
}
