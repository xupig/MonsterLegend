﻿#region 模块信息
/*==========================================
// 文件名：SmallMapElementView
// 命名空间: GameLogic.GameLogic.Modules.ModuleSmallMap.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/20 15:06:46
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using SpaceSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleSmallMap
{
    public class SmallMapElementView:KContainer
    {
        private int cameraRotation = 135;
        private SmallMapElement _me;
        private StateImage _meRotate;
        private SmallMapElement _boss;
        private List<SmallMapElement> _dummyList;
        private List<SmallMapElement> _spwanList;
        private List<SmallMapElement> _gateList;
        private List<SmallMapElement> _fieldNameList;
        private List<SmallMapElement> _spwanPoolList;
        private List<SmallMapElement> _dummyPoolList;
        private List<SmallMapElement> _gatePoolList;
        private List<SmallMapElement> _fieldNamePoolList;
        private IconContainer _iconContainer;

        private GameObject _bossTemplate;
        private GameObject _fieldTemplate;
        private GameObject _spawnTemplate;
        private GameObject _meTemplate;
        private GameObject _gateTemplate;
        public StateText _fieldNameTemplate;
        private KContainer _elements;

        private Vector2 leftBottomPos;
        private Vector2 rightTopPos;

        private IconContainer _iconBg;
        private KContainer _rotate;

        private Quaternion qua = new Quaternion();

        private bool isSmallMap = false;

        protected override void Awake()
        {
            _dummyList = new List<SmallMapElement>();
            _dummyPoolList = new List<SmallMapElement>();
            _spwanList = new List<SmallMapElement>();
            _spwanPoolList = new List<SmallMapElement>();
            _gateList = new List<SmallMapElement>();
            _gatePoolList = new List<SmallMapElement>();
            _fieldNameList = new List<SmallMapElement>();
            _fieldNamePoolList = new List<SmallMapElement>();
            _rotate = GetChildComponent<KContainer>("Container_rotate");
            _elements = _rotate.GetChildComponent<KContainer>("Container_elements");
            _bossTemplate = GetChildComponent<StateImage>("Container_template/Image_Boss").gameObject;
            _fieldTemplate = GetChildComponent<StateImage>("Container_template/Image_yeguai").gameObject;
            _gateTemplate = GetChildComponent<StateImage>("Container_template/Image_chuansongdian").gameObject;
            _spawnTemplate = GetChildComponent<StateImage>("Container_template/Image_chushengdian").gameObject;
            _meTemplate = GetChildComponent<KContainer>("Container_template/Container_suozaidi").gameObject;
            _iconContainer = _rotate.AddChildComponent<IconContainer>("Container_icon");
            _iconBg = AddChildComponent<IconContainer>("Container_iconBg");
           
            base.Awake();
        }

        public void HideTemplate()
        {
            GetChildComponent<KContainer>("Container_template").Visible = false;
        }

       
        public void UpdateContent(map map)
        {
            RectTransform rect = (_rotate.transform as RectTransform);
            rect.pivot = new Vector2(0.5f, 0.5f);
            rect.anchoredPosition = new Vector2(rect.sizeDelta.x / 2, -rect.sizeDelta.y / 2);
            qua.eulerAngles = new Vector3(0, 0, cameraRotation);
            (_rotate.transform as RectTransform).localRotation = qua;

            //_iconContainer.AddSprite(string.Format("minimap{0}", map.__space_name));
            _iconBg.AddSprite(string.Format("minimap{0}", map.__space_name));
            minimap minimap = minimap_helper.GetMinimapByMapId(map.__space_name);
            
            leftBottomPos = ConvertToPos(minimap.__left_bottom_pos);
            rightTopPos = ConvertToPos(minimap.__right_top_pos);
            float mainAngle = cameraRotation;
            float centerX = (leftBottomPos.x + rightTopPos.x) / 2;
            float centerY = (leftBottomPos.y + rightTopPos.y) / 2;
            float radius = Mathf.Sqrt(Mathf.Pow(centerX - leftBottomPos.x, 2) + Mathf.Pow(centerY - leftBottomPos.y, 2));
            float angle = Mathf.Atan2(leftBottomPos.y - centerY, leftBottomPos.x - centerX);
            float angle2 = Mathf.Atan2(rightTopPos.y - centerY, rightTopPos.x - centerX);
            leftBottomPos.x = radius * Mathf.Cos(angle + mainAngle * Mathf.PI / 180.0f) + centerX;
            leftBottomPos.y = radius * Mathf.Sin(angle + mainAngle * Mathf.PI / 180.0f) + centerY;
            rightTopPos.x = radius * Mathf.Cos(angle2 + mainAngle * Mathf.PI / 180.0f) + centerX;
            rightTopPos.y = radius * Mathf.Sin(angle2 + mainAngle * Mathf.PI / 180.0f) + centerY;

            Dictionary<Vector3, string> dummyPosDict = wild_helper.GetCurrSceneDummyPosInfo(map.__space_name);
            List<EntityData> entityList = GameSceneManager.GetInstance().curSpaceData.entities.entityList;
            UpdateMe();
            UpdateBoss();
            UpdateMiniInfo();
            UpdateDummyElements(dummyPosDict);
            UpdateSpwanElements(entityList);
            
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        public void OnUpdateMiniInfo()
        {
            UpdateMiniInfo();
        }

        private Vector2 ConvertToPos(List<string> posList)
        {
            Vector2 pos = new Vector2();
            if (posList != null && posList.Count >= 2)
            {
                pos.x = float.Parse(posList[0]);
                pos.y = float.Parse(posList[1]);
            }
            return pos;
        }

        private Quaternion q = new Quaternion();
        public void UpdateMe()
        {
            if (PlayerAvatar.Player.actor == null)
            {
                return;
            }
            if(_me == null)
            {
                _me = CreateMapElement(SmallMapItemType.Me);
                _meRotate = _me.GetComponentInChildren<StateImage>();
                RectTransform meRotateRect = _meRotate.transform as RectTransform;
                meRotateRect.pivot = new Vector2(0.5f, 0.5f);
                meRotateRect.anchoredPosition = new Vector2(0, 0);
              
            }
            _me.Position = ConvertToMap(PlayerAvatar.Player.position);
            q.eulerAngles = new Vector3(0, 0, -PlayerAvatar.Player.actor.localEulerAngles.y);
            //meRotateRect.anchoredPosition = new Vector2(0, 0);
            (_meRotate.transform as RectTransform).localRotation = q;
            //_meRotate.transform.localRotation = q;
        }

        private Vector2 vect = new Vector2();

        private Vector2 ConvertToMap(Vector3 position,SmallMapElement element)
        {
            vect.x = (position.x - leftBottomPos.x) * 488 / (rightTopPos.x - leftBottomPos.x) - element.SizeDelta.x/2;
            vect.y = (position.z - leftBottomPos.y) * 488 / (rightTopPos.y - leftBottomPos.y) + element.SizeDelta.y/2 - 488;
            return vect;
        }

        private Vector2 ConvertToMap(Vector3 position)
        {
            vect.x = (position.x - leftBottomPos.x) * 488 / (rightTopPos.x - leftBottomPos.x);
            vect.y = (position.z - leftBottomPos.y) * 488 / (rightTopPos.y - leftBottomPos.y)- 488;
            return vect;
        }

        private Vector2 ConvertToMap(Vector3 position,Vector2 offset)
        {
            vect.x = (position.x - leftBottomPos.x) * 488 / (rightTopPos.x - leftBottomPos.x) + offset.x;
            vect.y = (position.z - leftBottomPos.y) * 488 / (rightTopPos.y - leftBottomPos.y) + offset.y - 488;
            return vect;
        }

        private Vector2 ConvertToMap(float x, float y, float z, SmallMapElement element)
        {
            vect.x = (x - leftBottomPos.x) * 488 / (rightTopPos.x - leftBottomPos.x) - element.SizeDelta.x / 2;
            vect.y = (z - leftBottomPos.y) * 488 / (rightTopPos.y - leftBottomPos.y) + element.SizeDelta.y / 2 - 488;
            return vect;
        }

        public void UpdateBoss()
        {
            WorldBossInfoData bossInfo = SmallMapManager.Instance.WorldBossInfo;
            if (bossInfo != null && bossInfo.isBossLife == public_config.WORLD_BOSS_APPEAR)
            {
                if (_boss == null)
                {
                    _boss = CreateMapElement(SmallMapItemType.Boss);
                }
                _boss.gameObject.SetActive(true);
                _boss.Position = ConvertToMap(bossInfo.targetPosition, _boss);
            }
            else
            {
                if (_boss != null)
                {
                    _boss.gameObject.SetActive(false);
                }
            }
        }

        private void UpdateMiniInfo()
        {
            for (int i = 0; i < _gateList.Count; i++)
            {
                _gateList[i].gameObject.SetActive(false);
                _gatePoolList.Add(_gateList[i]);
            }
            _gateList.Clear();
            if (SmallMapManager.Instance.MiniInfoDict!=null)
            {
                foreach (MiniMapInfoData data in SmallMapManager.Instance.MiniInfoDict.Values)
                {
                    SmallMapElement element = GetGateElement();
                    element.Position = ConvertToMap(data.targetPosition, element);
                    _gateList.Add(element);
                }
            }
            
        }

        private void UpdateSpwanElements(List<EntityData> entityList)
        {
            for (int i = 0; i < _spwanList.Count;i++ )
            {
                _spwanList[i].gameObject.SetActive(false);
                _spwanPoolList.Add(_spwanList[i]);
            }
            _spwanList.Clear();
            for (int i = 0; i < entityList.Count; i++)
            {
                if (entityList[i].type == EntityType.CAMP_ACTION && (entityList[i] as EntityCampData).camp_id_i == PlayerAvatar.Player.map_camp_id)
                {
                   SmallMapElement element = GetSpwanElement();
                    EntityCampData data = (entityList[i] as EntityCampData);
                    element.Position = ConvertToMap(data.pos_x_i/100.0f, data.pos_y_i/100.0f, data.pos_z_i/100.0f,element);
                    _spwanList.Add(element);
                }
            }
        }

        private void UpdateDummyElements(Dictionary<Vector3, string> dummyPosDict)
        {
            for (int i = 0; i < _dummyList.Count; i++)
            {
                _dummyList[i].gameObject.SetActive(false);
                _dummyPoolList.Add(_dummyList[i]);
            }
            for (int i = 0; i < _fieldNameList.Count;i++ )
            {
                _fieldNameList[i].gameObject.SetActive(false);
                _fieldNamePoolList.Add(_fieldNameList[i]);
            }
                _dummyList.Clear();
                _fieldNameList.Clear();
            foreach (Vector3 vect in dummyPosDict.Keys)
            {
                SmallMapElement element = GetDummyElement();
                element.Position = ConvertToMap(vect, element);
                _dummyList.Add(element);
                if (_fieldNameTemplate != null)
                {
                    SmallMapElement nameElement = GetDummyNameElement();
                    nameElement.Position = element.Position + new Vector2(-9, 9);
                    nameElement.name = dummyPosDict[vect];
                    _fieldNameList.Add(nameElement);
                    nameElement.GetComponent<TextWrapper>().text = dummyPosDict[vect];
                }
            }
        }

        private SmallMapElement GetGateElement()
        {
            if (_gatePoolList.Count > 0)
            {
                SmallMapElement element = _gatePoolList[0];
                _gatePoolList.RemoveAt(0);
                element.gameObject.SetActive(true);
                return element;
            }
            return CreateMapElement(SmallMapItemType.Gate);
        }

        private SmallMapElement GetSpwanElement()
        {
             if(_spwanPoolList.Count > 0)
             {
                 SmallMapElement element = _spwanPoolList[0];
                 _spwanPoolList.RemoveAt(0);
                 element.gameObject.SetActive(true);
                 return element;
             }
             return CreateMapElement(SmallMapItemType.Spwan);
        }

        private SmallMapElement GetDummyElement()
        {
            if (_dummyPoolList.Count > 0)
            {
                SmallMapElement element = _dummyPoolList[0];
                _dummyPoolList.RemoveAt(0);
                element.gameObject.SetActive(true);
                return element;
            }
            return CreateMapElement(SmallMapItemType.DummyPos);
        }

        private SmallMapElement GetDummyNameElement()
        {
            if (_fieldNamePoolList.Count > 0)
            {
                SmallMapElement element = _fieldNamePoolList[0];
                _fieldNamePoolList.RemoveAt(0);
                element.gameObject.SetActive(true);
                return element;
            }
            return CreateMapElement(SmallMapItemType.FieldName);
        }

        private SmallMapElement CreateMapElement(SmallMapItemType type)
        {
            GameObject obj;
            if(type == SmallMapItemType.Me)
            {
                obj = _meTemplate;
            }
            else if(type == SmallMapItemType.Boss)
            {
                obj = _bossTemplate;
            }
            else if(type == SmallMapItemType.DummyPos)
            {
                obj = _fieldTemplate;
            }
            else if(type == SmallMapItemType.Spwan)
            {
                obj = _spawnTemplate;
            }
            else if(type == SmallMapItemType.Gate)
            {
                obj = _gateTemplate;
            }
            else if(type == SmallMapItemType.FieldName)
            {
                obj = _fieldNameTemplate.gameObject;
            }
            else
            {
                return null;
            }
            GameObject template = GameObject.Instantiate(obj) as GameObject;
            template.transform.SetParent(_elements.transform);
            template.transform.localPosition = Vector3.zero;
            template.transform.localScale = Vector3.one;
            Quaternion q = new Quaternion();
            if (type == SmallMapItemType.FieldName)
            {
                q.eulerAngles = new Vector3(0, 0, -cameraRotation);
                RectTransform rect = template.transform as RectTransform;
                rect.anchorMin = new Vector2(0.5f, 0.5f);
                rect.anchorMax = new Vector2(0.5f, 0.5f);
                rect.pivot = new Vector2(0.5f,0.5f);
            }
            template.transform.localRotation = q;
            template.SetActive(true);
            return template.AddComponent<SmallMapElement>();
        }


        public Vector3 PlayerAvaatar { get; set; }
    }
}
