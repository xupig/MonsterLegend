﻿using Common.Base;
using Game.UI.UIComponent;
using ModuleField;

namespace ModuleSmallMap
{
    public class SmallMapPanel:BasePanel
    {
        private FieldWorldSmallMapView _worldView;
        private FieldCurrentMapView _currentView;
        private KToggleGroup _toggleGroup;

        protected override void Awake()
        {
            base.Awake();
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _worldView = AddChildComponent<FieldWorldSmallMapView>("Container_shijieditu");
            _currentView = AddChildComponent<FieldCurrentMapView>("Container_dangqianditu");
            _toggleGroup = GetChildComponent<KToggleGroup>("ToggleGroup_title");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.SmallMap; }
        }

        public override void OnShow(object data)
        {
            _toggleGroup.SelectIndex = 0;
            OnToggleGroupChange(_toggleGroup, _toggleGroup.SelectIndex);
            AddListener();
        }

        public override void OnClose()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnToggleGroupChange);
        }

        private void RemoveListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnToggleGroupChange);
        }

        private void OnToggleGroupChange(KToggleGroup arg0, int index)
        {
            if (index == 0)
            {
                _worldView.Visible = false;
                _currentView.Visible = true;
            }
            else
            {
                _worldView.Visible = true;
                _currentView.Visible = false;
            }
        }

    }
}
