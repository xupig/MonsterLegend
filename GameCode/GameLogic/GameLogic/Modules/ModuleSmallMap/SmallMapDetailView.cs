﻿using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleSmallMap
{
    public class SmallMapDetailView : KContainer
    {
        private ModelComponent _model;
        private PathModelComponent _pathModel;
        private KButton _goBtn;
        private StateText _timeTxt;
        private StateText _nameTxt;

        private SmallMapDetailData _detailData;
        private bool isUpdate;
        private int count = 0;

        protected override void Awake()
        {
            base.Awake();
            _model = AddChildComponent<ModelComponent>("Container_moxingzhanshi");
            _pathModel = AddChildComponent<PathModelComponent>("Container_pathmoxingzhanshi");
            _goBtn = GetChildComponent<KButton>("Button_huicheng");
            _timeTxt = GetChildComponent<StateText>("Label_txtShuanxinshijian");
            _nameTxt = GetChildComponent<StateText>("Label_txtMingzi");
            _nameTxt.CurrentText.alignment = TextAnchor.UpperCenter;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            RefreshContent();
            AddListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveListener();
        }

        private void AddListener()
        {
            _goBtn.onClick.AddListener(OnGoBtnClick);
            EventDispatcher.AddEventListener<SmallMapDetailData>(MainUIEvents.MINI_MAP_DETAIL_CHANGE, SetData);
        }

        private void RemoveListener()
        {
            _goBtn.onClick.RemoveListener(OnGoBtnClick);
            EventDispatcher.RemoveEventListener<SmallMapDetailData>(MainUIEvents.MINI_MAP_DETAIL_CHANGE, SetData);
        }

        private void OnGoBtnClick()
        {
            if (_detailData != null)
            {
                switch (_detailData.type)
                {
                    case SmallMapItemType.NPC:
                        EventDispatcher.TriggerEvent(PlayerCommandEvents.FIND_NPC, _detailData.id, true);
                        break;
                    case SmallMapItemType.DummyPos:
                        EventDispatcher.TriggerEvent(PlayerCommandEvents.FIND_POSITION, _detailData.pos);
                        break;
                    case SmallMapItemType.TransformPoint:
                        EventDispatcher.TriggerEvent<Vector3>(PlayerCommandEvents.FIND_TRANSFER_POINT, _detailData.pos);
                        break;
                    case SmallMapItemType.Boss:
                        if (_detailData.id != 0)
                        {
                            EventDispatcher.TriggerEvent(PlayerCommandEvents.FIND_POSITION, _detailData.pos);
                        }
                        break;
                    case SmallMapItemType.Gate:
                        EventDispatcher.TriggerEvent(PlayerCommandEvents.FIND_POSITION, _detailData.pos);
                        break;
                    default:
                        break;
                }
            }
        }


        private void RefreshModel()
        {
            if (_detailData != null)
            {
                switch (_detailData.type)
                {
                    case SmallMapItemType.NPC:
                        LoadModelById(_detailData.id, SmallMapItemType.NPC);
                        break;
                    case SmallMapItemType.DummyPos:
                        LoadModelByType(SmallMapItemType.DummyPos);
                        break;
                    case SmallMapItemType.TransformPoint:
                        LoadModelByType(SmallMapItemType.TransformPoint);
                        break;
                    case SmallMapItemType.Boss:
                        if (_detailData.id != 0)
                        {
                            LoadModelById(_detailData.id, SmallMapItemType.Boss);
                        }
                        else
                        {
                            LoadModelByType(SmallMapItemType.Boss);
                        }
                        break;
                    case SmallMapItemType.Gate:
                        LoadModelByType(SmallMapItemType.Gate);
                        break;
                    default:
                        break;
                }
            }
        }

        private void LoadModelById(int id, SmallMapItemType type)
        {
            _model.Visible = true;
            _pathModel.Visible = false;
            if (type == SmallMapItemType.NPC)
            {
                _model.LoadNPCModel(id);
            }
            else if (type == SmallMapItemType.Boss)
            {
                _model.LoadMonsterModel(id);
            }
        }

        private void LoadModelByType(SmallMapItemType smallMapItemType)
        {
            _model.Visible = false;
            _pathModel.Visible = true;
            minimap_menu data = minimap_menu_helper.GetMinimapMenuData(GameSceneManager.GetInstance().curMapID, smallMapItemType);
            _pathModel.LoadModel(data.__model, data.__params);
        }

        public void SetData(SmallMapDetailData data)
        {
            _detailData = data;
            if (Visible == false)
            {
                return;
            }
            RefreshContent();
        }

        private void RefreshContent()
        {
            Clear();
            Refresh();
            RefreshModel();
        }

        private void Refresh()
        {
            if (_detailData != null)
            {
                _goBtn.Visible = true;
                _nameTxt.CurrentText.text = _detailData.name;
                if (_detailData.type == SmallMapItemType.Boss || _detailData.type == SmallMapItemType.Gate)
                {
                    if (_detailData.id == 0)
                    {
                        _goBtn.Visible = false;
                        isUpdate = true;
                        count = 0;
                        RefreshTime();
                    }
                }
            }
        }

        void Update()
        {
            if (isUpdate)
            {
                count++;
                if (count >= 15)
                {
                    count = 0;
                    RefreshTime();
                }
            }
        }


        private void RefreshTime()
        {
            if (_detailData != null)
            {
                int leftTime = _detailData.nextTime - Convert.ToInt32(Global.serverTimeStampSecond);
                leftTime = Math.Max(leftTime, 0);
                string txt = MogoTimeUtil.ToUniversalTime(leftTime);
                _timeTxt.CurrentText.text = string.Concat(MogoLanguageUtil.GetContent(6165306), txt);
            }
        }

        private void Clear()
        {
            isUpdate = false;
            _timeTxt.CurrentText.text = string.Empty;
            _nameTxt.CurrentText.text = string.Empty;
            _goBtn.Visible = false;
            _model.ClearModelGameObject();
            _pathModel.ClearModelGameObject();

        }

    }
}
