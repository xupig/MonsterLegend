﻿#region 模块信息
/*==========================================
// 文件名：SmallMapPanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleSmallMap
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/12 9:58:00
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using ModuleGem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleSmallMap
{

    public enum SmallMapItemType
    {
        NPC = 1,
        TransformPoint = 2,
        DummyPos = 3,       //怪物营地
        Boss = 4,
        Spwan = 5,      //出生点
        Me = 6,
        Gate = 7,       //争夺传送门
        FieldName = 8,
    }

    public class SmallMapView:KContainer
    {
        private StateText _txtTitle;
        private StateText _txtPosition;
        private string positionTemplate;

        private SmallMapMenuView _menuView;

        private SmallMapElementView _elementView;
        private SmallMapDetailView _detailView;


        protected override void Awake()
        {
            _txtTitle = GetChildComponent<StateText>("Label_txtBiaoti");
            _txtPosition = GetChildComponent<StateText>("Label_txtZuobiao");
            positionTemplate = _txtPosition.CurrentText.text;
            _menuView = AddChildComponent<SmallMapMenuView>("ScrollView_hechengbaoshi");
            _elementView = AddChildComponent<SmallMapElementView>("Container_elements");
            _detailView = AddChildComponent<SmallMapDetailView>("Container_didianjieshao");
            _elementView._fieldNameTemplate = GetChildComponent<StateText>("Label_yeguai");
            _elementView._fieldNameTemplate.Visible = false;
            base.Awake();
        }

        private void RefreshContent()
        {
            int instanceId = map_helper.GetInstanceIDByMapID(GameSceneManager.GetInstance().curMapID);
            instance instance = instance_helper.GetInstanceCfg(instanceId);
            map map = map_helper.GetMap(GameSceneManager.GetInstance().curMapID);
            _txtTitle.CurrentText.text = instance_helper.GetInstanceName(instanceId);
            _elementView.UpdateContent(map);
            List<minimap_menu> menuDataList = minimap_menu_helper.GetMinimapMenu(map.__id);
            _menuView.SetMenuLabel(GetMenuData(menuDataList));

            List<List<SmallMapDetailData>> detailList = new List<List<SmallMapDetailData>>();
            foreach (minimap_menu menuData in menuDataList)
            {
                if ((SmallMapItemType)menuData.__type == SmallMapItemType.NPC)
                {
                    detailList.Add(GetNPCList(map.__id));
                }
                else if((SmallMapItemType)menuData.__type == SmallMapItemType.TransformPoint)
                {
                    detailList.Add(GetTransferPointList(map.__space_name));
                }
                else if ((SmallMapItemType)menuData.__type == SmallMapItemType.DummyPos)
                {
                    detailList.Add(GetDummyPosList(map.__space_name));
                }
                else if ((SmallMapItemType)menuData.__type == SmallMapItemType.Boss)
                {
                    detailList.Add(GetBossList());
                }
                else if ((SmallMapItemType)menuData.__type == SmallMapItemType.Gate)
                {
                    detailList.Add(GetGateList());
                }
            }
            _menuView.SetDataList(detailList);
            SmallMapDetailData firstData = GetFirstData(detailList);
            _detailView.SetData(firstData);
        }

        private SmallMapDetailData GetFirstData(List<List<SmallMapDetailData>> detailList)
        {
            for (int i = 0; i < detailList.Count; i++)
            {
                for (int j = 0; j < detailList[i].Count; j++)
                {
                    return detailList[i][j];
                }
            }
            return null;
        }

        private List<SmallMapDetailData> GetNPCList(int mapId)
        {
            List<SmallMapDetailData> result = new List<SmallMapDetailData>();
            List<npc> npcList = map_helper.GetNPCList(mapId);
            if (npcList != null)
            {
                for (int i = 0; i < npcList.Count; i++)
                {
                    SmallMapDetailData detailNpcData = new SmallMapDetailData();
                    detailNpcData.id = npcList[i].__id;
                    detailNpcData.type = SmallMapItemType.NPC;
                    detailNpcData.pos = Vector3.zero;
                    detailNpcData.name = MogoLanguageUtil.GetContent(npcList[i].__name);
                    result.Add(detailNpcData);
                }
            }
            return result;
        }

        private List<SmallMapDetailData> GetTransferPointList(string spaceName)
        {
            List<SmallMapDetailData> result = new List<SmallMapDetailData>();
            Dictionary<Vector3, string> transformPosDict = wild_helper.GetTransformPointDict(spaceName);
            foreach (KeyValuePair<Vector3, string> kvp in transformPosDict)
            {
                SmallMapDetailData detailTransformData = new SmallMapDetailData();
                detailTransformData.type = SmallMapItemType.TransformPoint;
                detailTransformData.id = 0;
                detailTransformData.pos = kvp.Key;
                detailTransformData.name = kvp.Value;
                result.Add(detailTransformData);
            }
            return result;
        }

        private List<SmallMapDetailData> GetDummyPosList(string spaceName)
        {
            List<SmallMapDetailData> result = new List<SmallMapDetailData>();
            Dictionary<Vector3, string> dummyPosDict = wild_helper.GetCurrSceneDummyPosInfo(spaceName);
            foreach (KeyValuePair<Vector3, string> kvp in dummyPosDict)
            {
                SmallMapDetailData detailDummyData = new SmallMapDetailData();
                detailDummyData.type = SmallMapItemType.DummyPos;
                detailDummyData.id = 0;
                detailDummyData.pos = kvp.Key;
                detailDummyData.name = kvp.Value;
                result.Add(detailDummyData);
            }
            return result;
        }

        private List<SmallMapDetailData> GetBossList()
        {
            List<SmallMapDetailData> result = new List<SmallMapDetailData>();
            SmallMapDetailData worldBossData = new SmallMapDetailData();
            worldBossData.type = SmallMapItemType.Boss;
            if (SmallMapManager.Instance.WorldBossInfo != null && SmallMapManager.Instance.WorldBossInfo.isBossLife == public_config.WORLD_BOSS_APPEAR)
            {
                worldBossData.id = (int)SmallMapManager.Instance.WorldBossInfo.monsterId;
                worldBossData.pos = SmallMapManager.Instance.WorldBossInfo.targetPosition;

                worldBossData.name = SmallMapManager.Instance.WorldBossInfo.name;
            }
            worldBossData.nextTime = SmallMapManager.Instance.nextWorldBossTimeStamp;
            result.Add(worldBossData);
            return result;
        }

        private List<SmallMapDetailData> GetGateList()
        {
            List<SmallMapDetailData> result = new List<SmallMapDetailData>();
            if (SmallMapManager.Instance.MiniInfoDict != null && SmallMapManager.Instance.MiniInfoDict.Count > 0)
            {
                foreach (MiniMapInfoData miniMapInfo in SmallMapManager.Instance.MiniInfoDict.Values)
                {
                    SmallMapDetailData miniInfoData = new SmallMapDetailData();
                    miniInfoData.type = SmallMapItemType.Gate;
                    miniInfoData.id = (int)miniMapInfo.id;
                    miniInfoData.pos = miniMapInfo.targetPosition;
                    miniInfoData.name = miniMapInfo.name;
                    result.Add(miniInfoData);
                }
            }
            else if (SmallMapManager.Instance.nextGatTimeStamp > Convert.ToInt32(Global.serverTimeStampSecond))
            {
                SmallMapDetailData miniInfoData = new SmallMapDetailData();
                miniInfoData.type = SmallMapItemType.Gate;
                miniInfoData.id = 0;
                miniInfoData.nextTime = (int)SmallMapManager.Instance.nextGatTimeStamp;
                result.Add(miniInfoData);
            }
            return result;
        }

        protected override void OnEnable()
        {
            EventDispatcher.AddEventListener(MainUIEvents.MINI_MAP_GATE_CHANGE, OnGateChange);
            EventDispatcher.AddEventListener(MainUIEvents.WORLD_BOSS_CHANGE, OnBossChange);
            RefreshContent();
        }

        protected override void OnDisable()
        {
            EventDispatcher.RemoveEventListener(MainUIEvents.MINI_MAP_GATE_CHANGE, OnGateChange);
            EventDispatcher.RemoveEventListener(MainUIEvents.WORLD_BOSS_CHANGE, OnBossChange);
            count = 0;
        }

        private void OnGateChange()
        {
            RefreshContent();
            //List<SmallMapDetailData> dataList = new List<SmallMapDetailData>();
            //if (SmallMapManager.Instance.MiniInfoDict != null)
            //{
            //    foreach (MiniMapInfoData miniMapInfo in SmallMapManager.Instance.MiniInfoDict.Values)
            //    {
            //        SmallMapDetailData worldBossData = new SmallMapDetailData();
            //        worldBossData.type = SmallMapItemType.Gate;
            //        worldBossData.id = (int)miniMapInfo.id;
            //        worldBossData.pos = miniMapInfo.targetPosition;
            //        worldBossData.name = miniMapInfo.name;
            //        dataList.Add(worldBossData);
            //    }
            //}
            //_menuView.RefreshGate(dataList);
        }

        private void OnBossChange()
        {
            RefreshContent();
            //List<SmallMapDetailData> dataList = new List<SmallMapDetailData>();
            //SmallMapDetailData worldBossData = new SmallMapDetailData();
            //worldBossData.type = SmallMapItemType.Boss;
            //if (SmallMapManager.Instance.WorldBossInfo != null && SmallMapManager.Instance.WorldBossInfo.isBossLife == public_config.WORLD_BOSS_APPEAR)
            //{
            //    worldBossData.id = (int)SmallMapManager.Instance.WorldBossInfo.monsterId;
            //    worldBossData.pos = SmallMapManager.Instance.WorldBossInfo.targetPosition;
            //    worldBossData.name = SmallMapManager.Instance.WorldBossInfo.name;
            //}
            //if (SmallMapManager.Instance.nextWorldBossTimeStamp != 0)
            //{
            //    dataList.Add(worldBossData);
            //}
            //_menuView.RefreshBoss(dataList);
        }

        private void OnClickField()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.FieldWorldMap);
        }

        private List<string> GetMenuData(List<minimap_menu> menuDataList)
        {
            List<string> menuList = new List<string>();
            foreach (minimap_menu menuData in menuDataList)
            {
                menuList.Add(menuData.__name.ToLanguage());
            }
            return menuList;
        }

        private int count = 0;
        void Update()
        {
            count++;
            if(count == 15)
            {
                _elementView.UpdateMe();
                _elementView.UpdateBoss();
                _txtPosition.CurrentText.text = string.Format(positionTemplate, Mathf.RoundToInt(PlayerAvatar.Player.position.x), Mathf.RoundToInt(PlayerAvatar.Player.position.z));
                count = 0;
            }
        }

    }
}
