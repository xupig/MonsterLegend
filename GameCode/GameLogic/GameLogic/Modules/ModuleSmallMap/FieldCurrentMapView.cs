﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleSmallMap
{
    public class FieldCurrentMapView : KContainer
    {
        private CategoryList _categoryList;
        private SmallMapView _mapView;
        private FieldPlayIntroduceView _introduceView;
        private KButton _btnChangeLine;
        private KButton _btnBackTown;
        private KButton _btnFindTreasureRank;

        protected override void Awake()
        {
            base.Awake();
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            _btnChangeLine = GetChildComponent<KButton>("Button_huanxian");
            _btnBackTown = GetChildComponent<KButton>("Button_huicheng");
            _btnFindTreasureRank = GetChildComponent<KButton>("Button_xunbaobang");
            _mapView = AddChildComponent<SmallMapView>("Container_dangqianditu");
            _introduceView = AddChildComponent<FieldPlayIntroduceView>("Container_wanfajieshao");
            InitCategoryList();
        }

        private void InitCategoryList()
        {
            List<CategoryItemData> listToggleData = new List<CategoryItemData>();
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(6165303)));
            listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(6165304)));
            _categoryList.RemoveAll();
            _categoryList.SetDataList<CategoryListItem>(listToggleData);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            _categoryList.SelectedIndex = 0;
            OnCategoryListChange(_categoryList, _categoryList.SelectedIndex);
            _btnFindTreasureRank.Visible = rank_helper.GetRankingListLevel((int)RankType.findTreasureRank) <= PlayerAvatar.Player.level;
            AddListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveListener();
        }

        private void AddListener()
        {
            _categoryList.onSelectedIndexChanged.AddListener(OnCategoryListChange);
            _btnChangeLine.onClick.AddListener(OnClickChangeLine);
            _btnBackTown.onClick.AddListener(OnClickBackTown);
            _btnFindTreasureRank.onClick.AddListener(OnClickFindTreasure);
        }

        private void RemoveListener()
        {
            _categoryList.onSelectedIndexChanged.RemoveListener(OnCategoryListChange);
            _btnChangeLine.onClick.RemoveListener(OnClickChangeLine);
            _btnBackTown.onClick.RemoveListener(OnClickBackTown);
            _btnFindTreasureRank.onClick.RemoveListener(OnClickFindTreasure);
        }

        private void OnClickFindTreasure()
        {
            view_helper.OpenView(40);
        }

        private void OnCategoryListChange(CategoryList arg0, int index)
        {
            if (index == 0)
            {
                _mapView.Visible = true;
                _introduceView.Visible = false;
            }
            else if (index == 1)
            {
                _mapView.Visible = false;
                _introduceView.Visible = true;
            }
        }

        private void OnClickBackTown()
        {
            if (PlayerAutoFightManager.GetInstance().fightType == FightType.FOLLOW_FIGHT)
            {
                MessageBox.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.ENSURE_CANCEL_FOLLOW_FIGHT), () =>
                {
                    PlayerAutoFightManager.GetInstance().Stop();
                    TeamManager.Instance.CancelFollowCaptain();
                    GameSceneManager.GetInstance().LeaveCombatScene();
                });
                return;
            }
            GameSceneManager.GetInstance().LeaveCombatScene();
        }

        private void OnClickChangeLine()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.FieldChangeLine);
        }
    }
}
