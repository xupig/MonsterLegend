﻿#region 模块信息
/*==========================================
// 文件名：SmallMapElement
// 命名空间: GameLogic.GameLogic.Modules.ModuleSmallMap.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/20 14:56:34
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleSmallMap
{
    public class SmallMapElement:MonoBehaviour
    {
        private RectTransform _rect;
        private RectTransform _rectWidth;
        protected void Awake()
        {
            _rect = GetComponent<RectTransform>();
            _rectWidth = _rect;
        }

        public Vector2 Position
        {
            get { return _rect.anchoredPosition; }
            set { _rect.anchoredPosition = value; }
        }

        public Vector2 SizeDelta
        {
            get { return _rectWidth.sizeDelta; }
        }
    }
}
