﻿#region 模块信息
/*==========================================
// 文件名：SmallMapMenuItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleSmallMap.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/19 10:23:09
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleSmallMap
{

    public class SmallMapMenuData
    {
        public int type;
        public string name;
    }
    public class SmallMapMenuItem:KTreeMenuItem
    {
        private KContainer _selected;
        private KContainer _notSelected;
        private KButton _btnClick;

        private SmallMapMenuData menuData;

        private StateText _txtName;
        private StateText _txtName2;

        protected override void Awake()
        {
            _btnClick = GetChildComponent<KButton>("Button_item");

            _selected = GetChildComponent<KContainer>("Button_item/checkmark");
            _notSelected = GetChildComponent<KContainer>("Button_item/image");

            _txtName = _selected.GetChildComponent<StateText>("Label_label");
            _txtName2 = _notSelected.GetChildComponent<StateText>("Label_label");

            _btnClick.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            onClick.Invoke(this, Index);
        }

        public override bool IsSelected
        {
            get
            {
                return base.IsSelected;
            }
            set
            {
                base.IsSelected = value;
                _selected.Visible = value;
                _notSelected.Visible = !value;
            }
        }

        public override object Data
        {
            get
            {
                return menuData;
            }
            set
            {
                menuData = value as SmallMapMenuData;
                Refresh();
            }
        }

        private void Refresh()
        {
            if (menuData != null)
            {
                _txtName.ChangeAllStateText(menuData.name);
                _txtName2.ChangeAllStateText(menuData.name);
            }
            _selected.Visible = false;
        }


        public override void Dispose()
        {
            _btnClick.onClick.RemoveListener(OnClick);
        }


    }
}
