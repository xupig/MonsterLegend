﻿#region 模块信息
/*==========================================
// 文件名：SmallMapDetailItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleSmallMap.ChildComponent
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/19 10:36:38
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleSmallMap
{

    public class SmallMapDetailData
    {
        public SmallMapItemType type;
        public int id;
        public Vector3 pos;
        public string name;
        public int nextTime;   //刷新时间
    }

    public class SmallMapDetailItem:KList.KListItemBase
    {

        private StateText _txtTitle1;
        private StateText _txtTitle2;
        private KContainer _selected;
        private KContainer _notSelected;
        private SmallMapDetailData detailData;

        private bool isUpdate;
        private int count = 0;

        protected override void Awake()
        {
            _selected = GetChildComponent<KContainer>("Container_checkmark");
            _txtTitle1 = _selected.GetChildComponent<StateText>("Label_label");
            _notSelected = GetChildComponent<KContainer>("Container_image");
            _txtTitle2 = _notSelected.GetChildComponent<StateText>("Label_label");
            _selected.Visible = false;
        }
        public override object Data
        {
            get
            {
                return detailData;
            }
            set
            {
                detailData = value as SmallMapDetailData;
                Refresh();
            }
        }

        public override void OnPointerClick(UnityEngine.EventSystems.PointerEventData evtData)
        {
            base.OnPointerClick(evtData);
            EventDispatcher.TriggerEvent(MainUIEvents.MINI_MAP_DETAIL_CHANGE, detailData);
        }

        public override bool IsSelected
        {
            get
            {
                return base.IsSelected;
            }
            set
            {
                base.IsSelected = value;
                _selected.Visible = value;
                _notSelected.Visible = !value;
            }
        }

        private void Refresh()
        {
            this.gameObject.SetActive(detailData != null);
            isUpdate = false;
            if (detailData != null)
            {
                if ((detailData.type == SmallMapItemType.Boss && detailData.id == 0) || (detailData.type == SmallMapItemType.Gate && detailData.id == 0))
                {
                    isUpdate = true;
                    count = 0;
                    RefreshTime();
                }
                else
                {
                    _txtTitle1.ChangeAllStateText(detailData.name);
                    _txtTitle2.ChangeAllStateText(detailData.name);
                }
            }
        }

        public override void Dispose()
        {
            
        }

        void Update()
        {
            if(isUpdate)
            {
                count++;
                if(count>=15)
                {
                    count = 0;
                    RefreshTime();
                }
            }
        }


        private void RefreshTime()
        {
            if (detailData != null)
            {
                int leftTime = detailData.nextTime - Convert.ToInt32(Global.serverTimeStampSecond);
                leftTime = Math.Max(leftTime, 0);
                string txt = MogoTimeUtil.ToUniversalTime(leftTime, (39991).ToLanguage());
                _txtTitle1.ChangeAllStateText(txt);
                _txtTitle2.ChangeAllStateText(txt);
            }
        }
    }
}
