﻿using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleSmallMap
{
    public class FieldPlayIntroduceView : KContainer
    {
        private KScrollView _menuScrollView;
        private KList _menuList;
        private StateText _descriptionTxt;
        private PathModelComponent _model;
        private KScrollView _itemScrollView;
        private KList _itemList;
        private StateText _rewardTxt;

        protected override void Awake()
        {
            base.Awake();
            _menuScrollView = GetChildComponent<KScrollView>("ScrollView_caidan");
            _menuList = _menuScrollView.GetChildComponent<KList>("mask/content");
            _menuList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _menuList.SetPadding(5, 0, 0, 25);
            _menuList.SetGap(3, 0);
            _descriptionTxt = GetChildComponent<StateText>("Label_txtJianjiexinxi");
            _rewardTxt = GetChildComponent<StateText>("Label_txtChanchuyulan");
            _model = AddChildComponent<PathModelComponent>("Container_moxingzhanshi");
            _itemScrollView = GetChildComponent<KScrollView>("ScrollView_wupinliebiao");
            InitScrollView();
        }

        private void InitScrollView()
        {
            _itemList = _itemScrollView.GetChildComponent<KList>("mask/content");
            _itemList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue);
            _itemList.SetGap(0, 15);
            _itemScrollView.ScrollRect.horizontal = true;
            _itemScrollView.ScrollRect.vertical = false;
            _itemScrollView.UpdateArrow();
        }

        private void RefreshGetView(int rewardId)
        {
            if (rewardId == 0)
            {
                _rewardTxt.Visible = false;
                _itemList.RemoveAll();
                return;
            }
            _rewardTxt.Visible = true;
            List<RewardData> rewardDataList = item_reward_helper.GetRewardDataList(rewardId, PlayerAvatar.Player.vocation);
            rewardDataList = RewardData.FilterSameItemId(rewardDataList);
            _itemList.SetDataList<RewardGrid>(rewardDataList, 2);
        }

        private void RefreshContent()
        {
            List<field_play_introduce> dataList = field_play_introduce_helper.GetPlayIntroduceMenu(GameSceneManager.GetInstance().curMapID);
            _menuList.SetDataList<FieldPlayIntroduceItem>(dataList, 4);
            OnFieldPlayMenuClick(0);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            RefreshContent();
            AddListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveListener();
        }

        private void AddListener()
        {
            EventDispatcher.AddEventListener<int>(MainUIEvents.FIELD_PLAY_DETAIL_CHANGE, OnFieldPlayMenuClick);
        }

        private void RemoveListener()
        {
            EventDispatcher.RemoveEventListener<int>(MainUIEvents.FIELD_PLAY_DETAIL_CHANGE, OnFieldPlayMenuClick);
        }

        private void OnFieldPlayMenuClick(int index)
        {
            for (int i = 0; i < _menuList.ItemList.Count; i++)
            {
                (_menuList.ItemList[i] as FieldPlayIntroduceItem).IsOn = false;
            }
            (_menuList.ItemList[index] as FieldPlayIntroduceItem).IsOn = true;
            field_play_introduce data = (_menuList.ItemList[index] as FieldPlayIntroduceItem).Data as field_play_introduce;
            _descriptionTxt.CurrentText.text = MogoLanguageUtil.GetContent(data.__desc);
            RefreshGetView(data.__reward);
            _model.LoadModel(data.__model, data.__params);
        }
    }

    public class FieldPlayIntroduceItem : KList.KListItemBase
    {
        private KToggle _toggle;
        private field_play_introduce _data;

        protected override void Awake()
        {
            _toggle = GetChildComponent<KToggle>("Toggle_caidan");
            _toggle.onValueChanged.AddListener(OnValueChange);
        }

        private void OnValueChange(KToggle target, bool index)
        {
            EventDispatcher.TriggerEvent<int>(MainUIEvents.FIELD_PLAY_DETAIL_CHANGE, this.Index);
        }

        public override void Dispose()
        {
            _data = null;
        }

        public bool IsOn
        {
            set
            {
                _toggle.isOn = value;
            }
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as field_play_introduce;
                _toggle.SetLabel(MogoLanguageUtil.GetContent(_data.__name));
            }
        }
    }
}
