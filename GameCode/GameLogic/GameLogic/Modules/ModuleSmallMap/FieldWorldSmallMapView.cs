﻿using Common.Base;
using Common.Events;
using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using ModuleField;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleSmallMap
{
    public class FieldWorldSmallMapView : FieldWorldMapView
    {
        private KButton _btnChangeLine;
        private KButton _btnBackTown;

        protected override void Awake()
        {
            base.Awake();
            _btnChangeLine = GetChildComponent<KButton>("Button_huanxian");
            _btnBackTown = GetChildComponent<KButton>("Button_huicheng");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            AddListener();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RemoveListener();
        }

        private void AddListener()
        {
            _btnChangeLine.onClick.AddListener(OnClickChangeLine);
            _btnBackTown.onClick.AddListener(OnClickBackTown);
        }

        private void RemoveListener()
        {
            _btnChangeLine.onClick.RemoveListener(OnClickChangeLine);
            _btnBackTown.onClick.RemoveListener(OnClickBackTown);
        }

        private void OnClickChangeLine()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.FieldChangeLine);
        }


        private void OnClickBackTown()
        {
            if (PlayerAutoFightManager.GetInstance().fightType == FightType.FOLLOW_FIGHT)
            {
                MessageBox.Show(false, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.ENSURE_CANCEL_FOLLOW_FIGHT), () =>
                {
                    PlayerAutoFightManager.GetInstance().Stop();
                    TeamManager.Instance.CancelFollowCaptain();
                    GameSceneManager.GetInstance().LeaveCombatScene();
                });
                return;
            }
            GameSceneManager.GetInstance().LeaveCombatScene();
        }

    }
}
