﻿#region 模块信息
/*==========================================
// 文件名：MakeMessagePanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/30 10:43:08
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Game.UI.UIComponent;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMake
{
    public class MakePopPanelDataWrapper
    {
        public MakeMessageType type;
        public int giftId;

        public MakePopPanelDataWrapper(MakeMessageType type, int giftId)
        {
            this.type = type;
            this.giftId = giftId;
        }
    }

    public class MakeMessagePanel:BasePanel
    {
        private MakeOpenGiftTips _openedGiftTips;
        private MakeGiftDetailTips _makeGiftDetailTips;

        protected override void Awake()
        {
            ModalMask = GetChildComponent<StateImage>("Container_bg/ScaleImage_sharedZhezhao");
            _openedGiftTips = AddChildComponent<MakeOpenGiftTips>("Container_yikaiqizhuanjing");
            _makeGiftDetailTips = AddChildComponent<MakeGiftDetailTips>("Container_tianfuxiangqing");
            base.Awake();
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.MakeMessage; }
        }

        public override void OnShow(object data)
        {
            if (data is MakePopPanelDataWrapper)
            {
                MakePopPanelDataWrapper popData = data as MakePopPanelDataWrapper;
                MakeMessageType type = popData.type;
                switch (type)
                {
                    case MakeMessageType.SHOWOPENGIFT:
                        _openedGiftTips.Show();
                        _makeGiftDetailTips.Hide();
                        break;
                    case MakeMessageType.SHOWGIFTDETAIL:
                        _openedGiftTips.Hide();
                        _makeGiftDetailTips.Show(popData.giftId);
                        break;
                }
            }
        }

        public override void OnClose()
        {
        }

    }
}
