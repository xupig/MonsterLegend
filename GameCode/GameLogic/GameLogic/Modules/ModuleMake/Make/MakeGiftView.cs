﻿#region 模块信息
/*==========================================
// 文件名：TrainView
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/24 9:53:04
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ClientConfig;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Mgrs;

namespace ModuleMake
{
    public class MakeGiftView : KContainer
    {
        private MoneyItem _item;
        private KScrollView _giftView;
        private KPageableList _giftList;
        private KButton _resetGiftBtn;
        private int itemId;

        protected override void Awake()
        {
            itemId = price_list_helper.GetCost(PriceListId.makeGift).Id;
            _item = AddChildComponent<MoneyItem>("Container_buyGold");
            _item.SetMoneyType(itemId);
            _giftView = GetChildComponent<KScrollView>("ScrollView_jieduan");
            _giftList = _giftView.GetChildComponent<KPageableList>("mask/content");
            _resetGiftBtn = GetChildComponent<KButton>("Button_zhuanjing");
            Init();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            _resetGiftBtn.onClick.AddListener(ResetGift);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.mfg_talent_reset_cnt, RefreshResetBtn);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.mfg_talent_had_learn_cnt, RefreshResetBtn);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            _resetGiftBtn.onClick.RemoveListener(ResetGift);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.mfg_talent_reset_cnt, RefreshResetBtn);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.mfg_talent_had_learn_cnt, RefreshResetBtn);
        }

        private void ResetGift()
        {
            MessageBox.Show(true, string.Empty, MogoLanguageUtil.GetContent(32102), ResetGiftRequest);
        }

        private void ResetGiftRequest()
        {
            MakeManager.Instance.RequestResetGift();
        }

        private void Init()
        {
            _giftView.ScrollRect.Draggable = false;
            _giftList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue);
            _giftList.SetGap(0, 59);
            _giftList.SetDataList<MakeGiftItem>(PlayerDataManager.Instance.MakeData.GetAllMakeGiftItemData());
            RefreshResetBtn();
            
        }

        private void RefreshResetBtn()
        {
            if (PlayerAvatar.Player.mfg_talent_reset_cnt == 0 && PlayerAvatar.Player.mfg_talent_had_learn_cnt > 0)
            {
                _resetGiftBtn.Visible = true;
            }
            else
            {
                _resetGiftBtn.Visible = false;
            }
        }

        public void Show()
        {
            Visible = true;
        }

        public void Hide()
        {
            Visible = false;
        }

    }
}
