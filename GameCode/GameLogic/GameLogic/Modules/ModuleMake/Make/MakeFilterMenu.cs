﻿#region 模块信息
/*==========================================
// 文件名：MakeFilterMenu
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake.ChildView.Make
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/25 9:49:49
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Events;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;
namespace ModuleMake
{
    public class MakeFilterMenu : KContainer
    {
        private const int VocationFilterString = 56105;
        private const int LevelFilterString = 56104;
        private const int TradeFilterString = 32016;

        private const int VocationCondition = 0;
        private const int LevelCondition = 1;
        private const int TradeCondition = 2;

        private KToggle[] toggles = new KToggle[3];
        private Vector2[] vectorArray = new Vector2[3];
        private int currentIndex = -1;
        private KContainer _filterContainer;
        private KPageableList _filterConditionList;
        private StateImage _filterBg;

        private List<FilterCondition> vocationConditionList;
        private List<FilterCondition> levelConditionList;
        private List<FilterCondition> tradeConditionList;

        protected override void Awake()
        {
            toggles[0] = GetChildComponent<KToggle>("Toggle_zhiye");
            vectorArray[0] = toggles[0].gameObject.AddComponent<Locater>().Position;
            toggles[1] = GetChildComponent<KToggle>("Toggle_dengji");
            vectorArray[1] = toggles[1].gameObject.AddComponent<Locater>().Position;
            toggles[2] = GetChildComponent<KToggle>("Toggle_jiaoyi");
            vectorArray[2] = toggles[2].gameObject.AddComponent<Locater>().Position;

            _filterContainer = GetChildComponent<KContainer>("Container_shaixuanxiang");
            _filterConditionList = _filterContainer.GetChildComponent<KPageableList>("List_content");
            _filterConditionList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _filterBg = _filterContainer.GetChildComponent<StateImage>("ScaleImage_sharedKuangDT02");
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            for (int i = 0; i < toggles.Length; i++)
            {
                toggles[i].Index = i;
                toggles[i].onValueChanged.AddListener(OnToggleStateChanged);
            }
            _filterConditionList.onItemClicked.AddListener(OnFilterItemClicked);
        }

        private void RemoveEventListener()
        {
            for (int i = 0; i < toggles.Length; i++)
            {
                toggles[i].onValueChanged.RemoveListener(OnToggleStateChanged);
            }
            _filterConditionList.onItemClicked.RemoveListener(OnFilterItemClicked);
        }

        public void SetFilterConditionMenu(List<int> vocationList, List<int> levelList, List<int> tradeList)
        {
            if (vocationList == null || vocationList.Count <= 1)
            {
                toggles[VocationCondition].Visible = false;
            }
            else
            {
                toggles[VocationCondition].Visible = true;
                vocationConditionList = GetFilterDataList(vocationList, FilterType.Vocation);
            }
            if (levelList == null || levelList.Count <= 1)
            {
                toggles[LevelCondition].Visible = false;
            }
            else
            {
                toggles[LevelCondition].Visible = true;
                levelConditionList = GetFilterDataList(levelList, FilterType.Level);
            }
            if (tradeList == null || tradeList.Count <= 1)
            {
                toggles[TradeCondition].Visible = false;
            }
            else
            {
                toggles[TradeCondition].Visible = true;
                tradeConditionList = GetFilterDataList(tradeList, FilterType.Trade);
            }
            RefreshFilterItem();
            SetDefaultToggleName();
        }

        private List<FilterCondition> GetFilterDataList(List<int> list, FilterType filterType)
        {
            List<FilterCondition> result = new List<FilterCondition>();
            result.Add(FilterCondition.GetEmptyFilterCondition(filterType));
            for (int i = 0; i < list.Count; i++)
            {
                result.Add(new FilterCondition(list[i], filterType));
            }
            return result;
        }

        private void RefreshFilterItem()
        {
            int index = 0;
            for (int i = 0; i < toggles.Length; i++)
            {
                if (toggles[i].Visible == true)
                {
                    toggles[i].GetComponent<Locater>().Position = vectorArray[index];
                    index++;
                }
            }
        }

        private void SetDefaultToggleName()
        {
            for (int i = 0; i < toggles.Length; i++)
            {
                if (toggles[i].Visible == true)
                {
                    SetToggleDefaultName(i);
                }
            }
        }

        private void SetToggleDefaultName(int index)
        {
            TextWrapper[] texts = toggles[index].GetComponentsInChildren<TextWrapper>(true);
            int nameId = 0;
            switch (index)
            {
                case 0:
                    nameId = VocationFilterString;
                    break;
                case 1:
                    nameId = LevelFilterString;
                    break;
                case 2:
                    nameId = TradeFilterString;
                    break;
            }
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].text = MogoLanguageUtil.GetContent(nameId);
            }
        }

        private void OnToggleStateChanged(KToggle toggle, bool value)
        {
            if (currentIndex == toggle.Index)
            {
                SetToggleState(toggle, toggle.isOn);
            }
            else
            {
                if (currentIndex >= 0 && currentIndex < toggles.Length)
                {
                    SetToggleState(toggles[currentIndex], false);
                }
                SetToggleState(toggle, true);
            }
            currentIndex = toggle.Index;
        }

        private void SetToggleState(KToggle toggle, bool isSelected)
        {
            toggle.isOn = isSelected;
            if (isSelected)
            {
                toggle.transform.Find("item").gameObject.SetActive(false);
                toggle.transform.Find("checkmark").gameObject.SetActive(true);
                ShowFilterList(toggle.Index, toggle.GetComponent<Locater>());
            }
            else
            {
                toggle.transform.Find("item").gameObject.SetActive(true);
                toggle.transform.Find("checkmark").gameObject.SetActive(false);
                HideFilterList();
            }
        }

        private void OnFilterItemClicked(KList list, KList.KListItemBase item)
        {
            FilterItem filterItem = item as FilterItem;
            FilterCondition data = filterItem.Data as FilterCondition;
            if ((item.Data as FilterCondition).Value != -1)
            {
                MakeManager.Instance.makeData.AddFilterCondition(data);
                EventDispatcher.TriggerEvent(MakeEvents.FORMULA_MENU_REFRESH);
                SetToggleName(currentIndex, data.Name);
            }
            else
            {
                MakeManager.Instance.makeData.RemoveFilterCondition(data.Type);
                EventDispatcher.TriggerEvent(MakeEvents.FORMULA_MENU_REFRESH);
                SetToggleDefaultName(currentIndex);
            }
            SetToggleState(toggles[currentIndex], false);
            HideFilterList();
        }

        private void SetToggleName(int index, string toggleTxt)
        {
            TextWrapper[] texts = toggles[index].GetComponentsInChildren<TextWrapper>(true);
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].text = toggleTxt;
            }
        }

        private void ShowFilterList(int condition, Locater locater)
        {
            _filterContainer.Visible = true;
            switch (condition)
            {
                case VocationCondition:
                    _filterConditionList.SetDataList<FilterItem>(vocationConditionList);
                    break;
                case LevelCondition:
                    _filterConditionList.SetDataList<FilterItem>(levelConditionList);
                    break;
                case TradeCondition:
                    _filterConditionList.SetDataList<FilterItem>(tradeConditionList);
                    break;
            }
            AdjustFilterPosition(locater);
        }

        private void AdjustFilterPosition(Locater locater)
        {
            const float gap = 6.0f;
            RectTransform filterRect = _filterContainer.GetComponent<RectTransform>();
            RectTransform bgRect = _filterBg.GetComponent<RectTransform>();
            RectTransform listRect = _filterConditionList.GetComponent<RectTransform>();
            bgRect.sizeDelta = new Vector2(bgRect.sizeDelta.x, listRect.sizeDelta.y + gap);
            _filterContainer.RecalculateSize();
            Vector2 anchorPosition = locater.Position + new Vector2(locater.Width * 0.5f, 0);
            filterRect.anchoredPosition = new Vector2(anchorPosition.x - 0.5f * filterRect.rect.width, anchorPosition.y + filterRect.rect.height);
        }

        private void HideFilterList()
        {
            _filterContainer.Visible = false;
        }

        //玩家打开面板，默认筛选与其职业相符的套装, Vocation为职业编号，0为所有职业
        public void SetVocationFilterCondition(int vocation)
        {
            if (toggles[VocationCondition].Visible == true)
            {
                SetToggleName(VocationCondition, MogoLanguageUtil.GetContent(vocation));
                FilterCondition defaultVocationCondition = new FilterCondition((int)PlayerAvatar.Player.vocation, FilterType.Vocation);
                MakeManager.Instance.makeData.AddFilterCondition(defaultVocationCondition);
            }
        }
        
        // 设置默认的等级
        public void SetLevelFilterCondition(int level)
        {
            if (toggles[LevelCondition].Visible == true)
            {
                SetToggleName(LevelCondition, string.Format(MogoLanguageUtil.GetContent(56232), level / 10 * 10));
                FilterCondition defaultLevelCondition = new FilterCondition(level / 10 * 10, FilterType.Level);
                MakeManager.Instance.makeData.AddFilterCondition(defaultLevelCondition);
            }
        }

        public void ClearAllFilterCondition()
        {
            for (int i = 0; i < toggles.Length; i++)
            {
                SetToggleDefaultName(i);
                SetToggleState(toggles[i], false);
            }
        }

    }
}