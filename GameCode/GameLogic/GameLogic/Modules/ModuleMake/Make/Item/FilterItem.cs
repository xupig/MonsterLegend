﻿#region 模块信息
/*==========================================
// 文件名：FilterItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake.Make.Item
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/28 11:08:19
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameMain.GlobalManager.SubSystem;
namespace ModuleMake
{
    public class FilterItem : KList.KListItemBase
    {
        private FilterCondition _data;

        private KButton _button;

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as FilterCondition;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            TextWrapper[] texts = _button.GetComponentsInChildren<TextWrapper>(true);
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].text = _data.Name;
            }
        }

        protected override void Awake()
        {
            _button = GetChildComponent<KButton>("Button_item");
            _button.onClick.AddListener(OnButtonClick);
        }

        private void OnButtonClick()
        {
            onClick.Invoke(this, this.Index);
        }

        public override void Dispose()
        {

        }
    }
}