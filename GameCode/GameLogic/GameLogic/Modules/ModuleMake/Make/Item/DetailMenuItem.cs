﻿#region 模块信息
/*==========================================
// 文件名：EnchantScrollDetailMenuItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake.ChildView.Make.Item
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/25 17:16:10
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using ModuleCommonUI;
namespace ModuleMake
{
    public class DetailMenuItem : KTreeDetailItem
    {
        private CraftItemData _data;

        private ItemGrid _itemIcon;
        private StateText _itemName;
        private StateText _itemDesc;
        private StateImage _lockImage;
        private StateImage _rareImage;
        private StateImage _selectedImage;

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as CraftItemData;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            if (_data.iconId != 0)
            {
                _itemIcon.SetItemData(_data.iconId, 0);
            }
            else
            {
                _itemIcon.SetItemData(item_helper.GetIcon(_data.productId), item_helper.GetQuality(_data.productId));
            }
            _itemName.CurrentText.text = _data.Name;
            _itemDesc.CurrentText.text = _data.Describe;
            _lockImage.Visible = _data.isLocked;
            _rareImage.Visible = _data.isRare;
            _selectedImage.Visible = false;
        }

        protected override void Awake()
        {
            _itemIcon = GetChildComponent<ItemGrid>("Container_wupin");
            _itemName = GetChildComponent<StateText>("Label_txtCailiaomingcheng");
            _itemDesc = GetChildComponent<StateText>("Label_txtCailiaoShuliang");
            _lockImage = GetChildComponent<StateImage>("Image_suo");
            _rareImage = GetChildComponent<StateImage>("Image_xiyouIcon");
            _selectedImage = GetChildComponent<StateImage>("Image_xuanzhongkuang01");
            _itemIcon.onClick = OnItemIconClick;
        }

        private void OnItemIconClick()
        {
            onClick.Invoke(this, MenuIndex, Index);
            if (_data.productId != 0)
            {
                ToolTipsManager.Instance.ShowItemTip(_data.productId, PanelIdEnum.Make);
            }
            else
            {
                MessageBox.Show(true, _data.Name, _data.DescribeInTipPanel);
            }
        }

        public void SetSelectedImage()
        {
            _selectedImage.Visible = true;
        }

        public void HideSelectedImage()
        {
            _selectedImage.Visible = false;
        }

        public override void Dispose()
        {

        }
    }
}