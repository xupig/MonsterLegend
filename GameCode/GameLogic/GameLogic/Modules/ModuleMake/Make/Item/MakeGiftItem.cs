﻿#region 模块信息
/*==========================================
// 文件名：MakeGiftItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake.Make.Item
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/28 15:51:57
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Mgrs;
using UnityEngine;

namespace ModuleMake
{
    public class MakeGiftItem : KList.KListItemBase
    {
        private MakeGiftItemData _data;

        private StateText _nameTxt;
        private StateImage _selectedImage;
        private KButton _showDetailMessageBtn;
        private KContainer _notActivatedContainer;
        private KContainer _activatedContainer;
        private StateText _descTxt;
        private IconContainer _giftIcon;
        private KButton _activiteBtn;
        private IconContainer _costMoneyIcon;
        private StateText _costMoneyNum;
        private StateText _firstOpenTxt;
        private StateText _costTxt;
        private KParticle _successParticle;
        private int needCostDiamond;
        private int costItemId = price_list_helper.GetCost(PriceListId.makeGift).Id;

        protected override void Awake()
        {
            _nameTxt = GetChildComponent<StateText>("Label_txtMingcheng");
            _selectedImage = GetChildComponent<StateImage>("ScaleImage_xuanzhongkuang");
            _selectedImage.Visible = false;
            _showDetailMessageBtn = GetChildComponent<KButton>("Button_xianshixiangqing");
            _notActivatedContainer = GetChildComponent<KContainer>("Container_weizhuanjing");
            _activiteBtn = _notActivatedContainer.GetChildComponent<KButton>("Button_zhuanjing");
            _costMoneyNum = _notActivatedContainer.GetChildComponent<StateText>("Label_txtShuliang");
            _costMoneyIcon = _notActivatedContainer.AddChildComponent<IconContainer>("Container_icon");
            _firstOpenTxt = _notActivatedContainer.GetChildComponent<StateText>("Label_shoucikaiqi");
            _costTxt = _notActivatedContainer.GetChildComponent<StateText>("Label_txtXiaohao");
            _costMoneyIcon.SetMoneyIcon(costItemId);
            _activatedContainer = GetChildComponent<KContainer>("Container_yizhuangjing");
            _descTxt = GetChildComponent<StateText>("Label_txtMiaoshu");
            _giftIcon = AddChildComponent<IconContainer>("Container_icon");
            _successParticle = AddChildComponent<KParticle>("fx_ui_10_2_xuanwo_01");
            _successParticle.Stop();
            _successParticle.transform.localPosition = new Vector3(63, -95, 0);
            AddEventListener();
        }

        private void AddEventListener()
        {
            _showDetailMessageBtn.onClick.AddListener(ShowDetailMessage);
            _activiteBtn.onClick.AddListener(ActivateGift);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.mfg_talent, Refresh);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, Refresh);
        }

        private void ActivateGift()
        {
            int totalMoney = item_helper.GetMyMoneyCountByItemId(costItemId);
            int hasOpenedGifts = _data.GetOwnedGiftNum();
            needCostDiamond = hasOpenedGifts < PlayerAvatar.Player.mfg_talent_had_learn_cnt ? 0 : price_list_helper.GetPrice(19, hasOpenedGifts);
            if (totalMoney >= needCostDiamond)
            {
                OnSureClick();
            }
            else
            {
                string content = string.Format(MogoLanguageUtil.GetContent(74607), item_helper.GetName(costItemId));
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, content);
            }
        }

        private void OnSureClick()
        {
            _successParticle.Play();
            UIManager.Instance.PlayAudio("Sound/UI/mystery.mp3");
            MakeManager.Instance.ActivateGift(_data.id);
        }

        private void ShowDetailMessage()
        {
            MakePopPanelDataWrapper data = new MakePopPanelDataWrapper(MakeMessageType.SHOWGIFTDETAIL, _data.id);
            UIManager.Instance.ShowPanel(PanelIdEnum.MakeMessage, data);
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as MakeGiftItemData;
                    Refresh();
                }
            }
        }

        public void Refresh()
        {
            _nameTxt.CurrentText.text = _data.name;
            _descTxt.CurrentText.text = _data.desc;
            _giftIcon.SetIcon(_data.icon, onIconLoaded);
            _notActivatedContainer.Visible = !_data.isAchieved;
            _activatedContainer.Visible = _data.isAchieved;
            switch (_data.isAchieved)
            {
                case true:
                    RefreshActivateContainer();
                    break;
                case false:
                    RefreshNotActivateContainer();
                    break;
            }
        }

        private void onIconLoaded(GameObject iconGameObject)
        {
            ImageWrapper imageWrapper = iconGameObject.GetComponent<ImageWrapper>();
            if (_data.canLearn == true)
            {
                imageWrapper.SetGray(1.0f);
            }
            else
            {
                imageWrapper.SetGray(0.0f);
            }
        }

        private void RefreshActivateContainer()
        {
            _notActivatedContainer.Visible = false;
            _activatedContainer.Visible = true;
        }

        private void RefreshNotActivateContainer()
        {
            if (_data.canLearn == false)
            {
                SetGiftIconGray();
                _firstOpenTxt.CurrentText.text = _data.learnConditionDesc;
                _costMoneyNum.Visible = false;
                _costMoneyIcon.Visible = false;
                _costTxt.Visible = false;
                _activiteBtn.Visible = false;
                return;
            }
            SetGiftIconNormal();
            _activiteBtn.Visible = true;
            int hasOpenedGifts = _data.GetOwnedGiftNum();
            if (hasOpenedGifts == 0)
            {
                _firstOpenTxt.CurrentText.text = MogoLanguageUtil.GetContent(6066012);
                _costMoneyNum.Visible = false;
                _costMoneyIcon.Visible = false;
                _costTxt.Visible = false;
            }
            else
            {
                _firstOpenTxt.CurrentText.text = string.Empty;
                _costMoneyNum.Visible = true;
                _costMoneyIcon.Visible = true;
                _costTxt.Visible = true;
                if (hasOpenedGifts < PlayerAvatar.Player.mfg_talent_had_learn_cnt)
                {
                    needCostDiamond = 0;
                    _costMoneyNum.CurrentText.text = string.Format("X{0}", needCostDiamond);
                }
                else
                {
                    needCostDiamond = price_list_helper.GetPrice(19, hasOpenedGifts);
                    _costMoneyNum.CurrentText.text = string.Format("X{0}", needCostDiamond);
                }
            }
        }

        private void SetGiftIconNormal()
        {
            GameObject holderGo = _giftIcon.GetChild("holder");
            foreach (var imageWrapper in holderGo.GetComponentsInChildren<ImageWrapper>())
            {
                imageWrapper.SetGray(1.0f);
            }
        }

        private void SetGiftIconGray()
        {
            GameObject holderGo = _giftIcon.GetChild("holder");
            foreach (var imageWrapper in holderGo.GetComponentsInChildren<ImageWrapper>())
            {
                imageWrapper.SetGray(0.0f);
            }
        }

        public override void Dispose()
        {

        }
    }

}