﻿#region 模块信息
/*==========================================
// 文件名：SuitOrWonderMenuItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake.ChildView.Make.Item
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/26 14:16:01
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
namespace ModuleMake
{
    public class EnchantOrWonderMenuItem : KList.KListItemBase
    {
        private CraftItemData _data;

        private ItemGrid _itemIcon;
        private StateText _itemNameTxt;
        private StateText _itemDescTxt;
        private StateImage _lockImage;
        private StateImage _rareImage;
        private StateImage _selectedImage;

        protected override void Awake()
        {
            _itemIcon = GetChildComponent<ItemGrid>("Container_wupin");
            _itemNameTxt = GetChildComponent<StateText>("Label_txtCailiaomingcheng");
            _itemDescTxt = GetChildComponent<StateText>("Label_txtCailiaoShuliang");
            _lockImage = GetChildComponent<StateImage>("Image_suo");
            _rareImage = GetChildComponent<StateImage>("Image_xiyouIcon");
            _selectedImage = GetChildComponent<StateImage>("Image_xuanzhongkuang01");
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value as CraftItemData;
                Refresh();
            }
        }

        private void Refresh()
        {
            _itemIcon.SetItemData(_data.productId);
            _itemNameTxt.CurrentText.text = _data.Name;
            _itemDescTxt.CurrentText.text = _data.Describe;
            _lockImage.Visible = _data.isLocked;
            _rareImage.Visible = _data.isRare;
            _selectedImage.Visible = false;
        }

        public void SetSelected()
        {
            _selectedImage.Visible = true;
        }

        public void ClearSelectedMark()
        {
            _selectedImage.Visible = false;
        }

        public override void Dispose()
        {

        }
    }
}