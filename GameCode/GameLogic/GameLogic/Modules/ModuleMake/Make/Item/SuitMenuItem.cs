﻿using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
#region 模块信息
/*==========================================
// 文件名：EnchantScrollMenuItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake.ChildView.Make.Item
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/25 16:52:21
// 描述说明：
// 其他：
//==========================================*/
#endregion

namespace ModuleMake
{
    public class SuitMenuItem : KTreeMenuItem
    {
        private int groupId;
        private KToggle _menuToggle;

        public override object Data
        {
            get
            {
                return groupId;
            }
            set
            {
                groupId = (int)value;
                Refresh();
            }
        }

        public void SetToggleState(bool state)
        {
            _menuToggle.isOn = state;
            _menuToggle.transform.Find("checkmark").gameObject.SetActive(state);
        }

        protected override void Awake()
        {
            _menuToggle = GetChildComponent<KToggle>("Toggle_item");
            _menuToggle.onValueChanged.AddListener(OnMenuBtnClick);
        }

        private void OnMenuBtnClick(KToggle toggle, bool value)
        {
            onClick.Invoke(this, this.Index);
        }

        private void Refresh()
        {
            TextWrapper[] btnTxts = _menuToggle.transform.GetComponentsInChildren<TextWrapper>(true);
            for (int i = 0; i < btnTxts.Length; i++)
            {
                btnTxts[i].text = MogoLanguageUtil.GetContent(groupId);
            }
        }

        public override void Dispose()
        {

        }
    }
}