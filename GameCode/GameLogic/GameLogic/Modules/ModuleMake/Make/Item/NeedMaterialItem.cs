﻿#region 模块信息
/*==========================================
// 文件名：NeedMaterialItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake.Make.Item
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/26 21:00:34
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
namespace ModuleMake
{
    public class NeedMaterialItem : KContainer
    {
        private StateText _nameTxt;
        private StateText _numTxt;
        private ItemGrid _itemGrid;

        protected override void Awake()
        {
            _nameTxt = GetChildComponent<StateText>("Label_txtMingzi");
            _numTxt = GetChildComponent<StateText>("Label_txtShuliang");
            _itemGrid = GetChildComponent<ItemGrid>("Container_wupin");
        }

        public void SetNeedMaterial(int itemId, int num)
        {
            Visible = true;
            _nameTxt.CurrentText.text = item_helper.GetName(itemId);
            int ownNum = item_helper.GetMyMoneyCountByItemId(itemId);
            if (item_helper.isMoneyType(itemId))
            {
                if (ownNum < num)
                {
                    _numTxt.CurrentText.text = string.Format("<color=#FF0000>{0}</color>", num);
                }
                else
                {
                    _numTxt.CurrentText.text = string.Format("{0}", num);
                }
            }
            else
            {
                if (ownNum < num)
                {
                    _numTxt.CurrentText.text = string.Format("<color=#FF0000>{0}</color>/{1}", ownNum, num);
                }
                else
                {
                    _numTxt.CurrentText.text = string.Format("{0}/{1}", ownNum, num);
                }
            }
            _itemGrid.SetItemData(itemId);
        }
    }
}