﻿#region 模块信息
/*==========================================
// 文件名：AttatchMagicScrollMenu
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake.ChildView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/24 21:19:33
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using System.Collections.Generic;

namespace ModuleMake
{
    public class SuitScrollMenu : KContainer
    {
        private KTree _menu;
        private List<int> groupList;
        private KScrollView _menuView;
        private List<CraftItemData> _detailList;
        public static int defaultSuitId = 0;

        private SuitMenuItem currentSuitMenuItem;
        private DetailMenuItem currentDetailMenu;

        public KComponentEvent<int> onClick = new KComponentEvent<int>();

        protected override void Awake()
        {
            _menuView = GetChildComponent<KScrollView>("ScrollView_hechengbaoshi");
            _menu = _menuView.GetChildComponent<KTree>("mask/content");
            _menu.SetGap(5, -10);
            AddEventListener();
        }

        private void AddEventListener()
        {
            _menu.onSelectedMenuIndexChanged.AddListener(OpenMenu);
            _menu.onSelectedDetailIndexChanged.AddListener(ChooseSubMenu);
        }

        private void RemoveEventListener()
        {
            _menu.onSelectedMenuIndexChanged.RemoveListener(OpenMenu);
            _menu.onSelectedDetailIndexChanged.RemoveListener(ChooseSubMenu);
        }

        private void OpenMenu(KTree tree, KTreeMenuItem menuItem)
        {
            if (currentSuitMenuItem != null)
            {

            }
            SuitMenuItem item = menuItem as SuitMenuItem;
            currentSuitMenuItem = item;
            int groupId = (int)item.Data;
            _detailList = MakeManager.Instance.makeData.GetCraftItemByGroup(groupId);
            _detailList.Sort(SuitSortFunction);
            _menu.SetDetailList<DetailMenuItem>(item.Index, _detailList, 5);
        }

        private int SuitSortFunction(CraftItemData x, CraftItemData y)
        {
            if (x.isLocked != y.isLocked)
            {
                return x.isLocked ? 1 : -1;
            }
            bool xCanMake = mfg_formula_helper.CheckEnoughMaterialMake(x.id) == 0;
            bool yCanMake = mfg_formula_helper.CheckEnoughMaterialMake(y.id) == 0;
            if (xCanMake != yCanMake)
            {
                return xCanMake ? -1 : 1;
            }
            int playerLevel = PlayerAvatar.Player.level;
            if (x.MinProductLevel != y.MinProductLevel)
            {
                if (playerLevel >= x.MinProductLevel && playerLevel >= y.MinProductLevel)
                {
                    return y.MinProductLevel - x.MinProductLevel;
                }
                else if (playerLevel >= x.MinProductLevel)
                {
                    return -1;
                }
                else if (playerLevel >= y.MinProductLevel)
                {
                    return 1;
                }
                else
                {
                    return x.MinProductLevel - y.MinProductLevel;
                }
            }
            else
            {
                return x.id - y.id;
            }
        }

        private void ChooseSubMenu(KTree tree, KTreeDetailItem detailItem)
        {
            if (currentDetailMenu != null)
            {
                currentDetailMenu.HideSelectedImage();
            }
            onClick.Invoke((detailItem.Data as CraftItemData).id);
            currentDetailMenu = detailItem as DetailMenuItem;
            currentDetailMenu.SetSelectedImage();
        }

        public void SetGroupMenu(int type)
        {
            _menu.RemoveAll();
            groupList = MakeManager.Instance.makeData.GetSuitGroupList(type);
            _menu.SetMenuList<SuitMenuItem>(groupList, 5, true);
            if (groupList.Count > 0 && MakeManager.Instance.makeData.GetCraftItemByGroup(groupList[0]).Count > 0)
            {
                _detailList = MakeManager.Instance.makeData.GetCraftItemByGroup(groupList[0]);
                _detailList.Sort(SuitSortFunction);
                defaultSuitId = _detailList.Count > 0 ? _detailList[0].id : 0;
                if (groupList.Count == 1)
                {
                    OpenMenu(_menu, _menu.ItemList[0]);
                }
            }
        }
    }
}