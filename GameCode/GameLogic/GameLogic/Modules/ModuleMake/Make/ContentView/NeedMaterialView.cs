﻿#region 模块信息
/*==========================================
// 文件名：NeedMaterialView
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake.Make.ContentView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/26 16:44:23
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager.SubSystem;
using System.Collections.Generic;
using UnityEngine;
namespace ModuleMake
{
    public class NeedMaterialView : KContainer
    {
        private NeedMaterialItem[] needMaterialItems = new NeedMaterialItem[3];

        private float totalWidth;
        private float itemWidth;
        private float startYPosition;

        private int MAX_ITEM_COUNT = 3;

        protected override void Awake()
        {
            for (int i = 0; i < needMaterialItems.Length; i++)
            {
                needMaterialItems[i] = AddChildComponent<NeedMaterialItem>(string.Concat("Container_item0", i.ToString()));
            }
            totalWidth = GetComponent<RectTransform>().rect.width;
            itemWidth = needMaterialItems[0].GetComponent<RectTransform>().rect.width;
            startYPosition = needMaterialItems[0].GetComponent<RectTransform>().anchoredPosition.y;
        }

        public void SetNeedMaterial(int craftId)
        {
            int totalCount = 0;
            Dictionary<int, int> needMaterials;
            if (mfg_formula_helper.GetProductType(craftId) != 0 && MakeManager.Instance.makeData.HasOpenCraftGift(craftId))
            {
                needMaterials = mfg_formula_helper.GetExpertFormulaMakeCost(craftId, false);
            }
            else
            {
                needMaterials = mfg_formula_helper.GetFormulaMakeCost(craftId, false);
            }
            foreach (var cost in needMaterials)
            {
                needMaterialItems[totalCount].SetNeedMaterial(cost.Key, cost.Value);
                totalCount++;
            }
            if (totalCount == 0)
            {
                return;
            }
            AdjustPosition(totalCount);
        }

        public void AdjustPosition(int count)
        {
            float xGap = (totalWidth - count * itemWidth) / (count + 1);
            float startXPosition = xGap;
            for (int i = 0; i < count; i++)
            {
                needMaterialItems[i].Visible = true;
                needMaterialItems[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(startXPosition, startYPosition);
                startXPosition += (xGap + itemWidth);
            }
            for (int i = count; i < MAX_ITEM_COUNT; i++)
            {
                needMaterialItems[i].Visible = false;
            }
        }
    }
}