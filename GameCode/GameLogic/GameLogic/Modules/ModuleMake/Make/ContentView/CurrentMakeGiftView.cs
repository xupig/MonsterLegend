﻿#region 模块信息
/*==========================================
// 文件名：CurrentMakeGiftView
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake.Make.ContentView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/26 16:43:14
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ClientConfig;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Mgrs;
using System.Collections.Generic;
namespace ModuleMake
{
    public class CurrentMakeGiftView : KContainer
    {
        private const int MAXGIFTCOUNT = 3;
        private StateText[] _giftTxt = new StateText[MAXGIFTCOUNT];
        private StateText _titleTxt;
        private KButton _showBtn;
        private KButton _goGiftViewBtn;
        private int _currentGift;

        protected override void Awake()
        {
            _giftTxt[0] = GetChildComponent<StateText>("Label_txtZhuangjing00");
            _giftTxt[1] = GetChildComponent<StateText>("Label_txtZhuanjing01");
            _giftTxt[2] = GetChildComponent<StateText>("Label_txtZhuanjing02");
            _titleTxt = GetChildComponent<StateText>("Label_txtDangqianzhuangjing");
            _titleTxt.Visible = true;
            _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(32062);
            _showBtn = GetChildComponent<KButton>("Button_zhizao");
            _goGiftViewBtn = GetChildComponent<KButton>("Button_open");
            InitBtnTxt();
            AddEventListener();
        }

        public void Refresh(int craftId)
        {
            _currentGift = mfg_formula_helper.GetProductType(craftId);
            SetGiftView();
        }

        private void InitBtnTxt()
        {
            TextWrapper[] btnTxts = _goGiftViewBtn.GetComponentsInChildren<TextWrapper>(true);
            for (int i = 0; i < btnTxts.Length; i++)
            {
                btnTxts[i].text = MogoLanguageUtil.GetContent(32060);
            }
        }

        private void SetGiftView()
        {
            List<int> hasOpenedGiftList = GetOpenedGift(PlayerAvatar.Player.mfg_talent);
            if (PlayerAvatar.Player.mfg_talent > 0)
            {
                _showBtn.Visible = true;
                _goGiftViewBtn.Visible = false;
                for (int i = 0; i < hasOpenedGiftList.Count; i++)
                {
                    _giftTxt[i].Visible = true;
                    _giftTxt[i].CurrentText.text = mfg_talent_helper.GetTalentName(hasOpenedGiftList[i]);
                }
                for (int i = hasOpenedGiftList.Count; i < MAXGIFTCOUNT; i++)
                {
                    _giftTxt[i].CurrentText.text = string.Empty;
                }
            }
            else
            {
                if (mfg_formula_helper.CheckGiftCanLearn(_currentGift))
                {
                    _goGiftViewBtn.Visible = true;
                }
                _showBtn.Visible = false;
                _giftTxt[0].Visible = true;
                _giftTxt[0].CurrentText.text = MogoLanguageUtil.GetContent(32047);
                for (int i = 1; i < MAXGIFTCOUNT; i++)
                {
                    _giftTxt[i].CurrentText.text = string.Empty;
                }
            }
        }

        private List<int> GetOpenedGift(int mfgNum)
        {
            List<int> result = new List<int>();
            int giftBit = 1;
            while (mfgNum > 0)
            {
                if (mfgNum % 2 == 1)
                {
                    result.Add(giftBit);
                }
                mfgNum /= 2;
                giftBit++;
            }
            return result;
        }

        private void AddEventListener()
        {
            _showBtn.onClick.AddListener(OnShowBtnClick);
            _goGiftViewBtn.onClick.AddListener(GoToGiftView);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.mfg_talent, SetGiftView);
        }

        private void RemoveEventListener()
        {
            _showBtn.onClick.RemoveListener(OnShowBtnClick);
            _goGiftViewBtn.onClick.RemoveListener(GoToGiftView);
            EntityPropertyManager.Instance.RemoveEventListener(PlayerAvatar.Player, EntityPropertyDefine.mfg_talent, SetGiftView);
        }

        private void GoToGiftView()
        {
            // 打开制造天赋界面
            UIManager.Instance.ShowPanel(PanelIdEnum.Make, 2);
        }

        private void OnShowBtnClick()
        {
            MakePopPanelDataWrapper data = new MakePopPanelDataWrapper(MakeMessageType.SHOWOPENGIFT, 0);
            UIManager.Instance.ShowPanel(PanelIdEnum.MakeMessage, data);
        }

       
    }
}