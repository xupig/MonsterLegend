﻿#region 模块信息
/*==========================================
// 文件名：CurrentStateView
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake.Make.ContentView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/26 16:45:27
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
namespace ModuleMake
{
    public class CurrentStateView : KContainer
    {
        private KButton _showMySuitsBtn;
        private KContainer _lockedContainer;
        private StateText _lockTxt;
        private KContainer _needLearnContainer;
        private KButton _learnBtn;
        private StateText _learnTxt;
        private KContainer _needOpenGiftContainer;
        private KButton _openGiftBtn;
        private StateText _openGiftTxt;
        private KContainer _buyOrMakeContainer;
        private KButton _makeBtn;
        private KButton _buyBtn;
        private IconContainer _needMaterial;
        private StateText _numTxt;
        private int currentCraftId;
        private CraftItemData _currentCraftItemData;
        private StateText _cdTimeTxt;
        private uint timeId;
        private int leftSeconds;

        protected override void Awake()
        {
            _showMySuitsBtn = GetChildComponent<KButton>("Button_taozhuang");
            _lockedContainer = GetChildComponent<KContainer>("Container_jiesuo");
            _lockTxt = _lockedContainer.GetChildComponent<StateText>("Label_txtTishi");
            _needLearnContainer = GetChildComponent<KContainer>("Container_xuexi");
            _learnBtn = _needLearnContainer.GetChildComponent<KButton>("Button_xuexi");
            _learnTxt = _needLearnContainer.GetChildComponent<StateText>("Label_txtTishi");
            _needOpenGiftContainer = GetChildComponent<KContainer>("Container_kaiqi");
            _openGiftBtn = _needOpenGiftContainer.GetChildComponent<KButton>("Button_xuexi");
            _openGiftTxt = _needOpenGiftContainer.GetChildComponent<StateText>("Label_txtTishi");
            _buyOrMakeContainer = GetChildComponent<KContainer>("Container_qiugou");
            _buyBtn = _buyOrMakeContainer.GetChildComponent<KButton>("Button_qiugou");
            _makeBtn = _buyOrMakeContainer.GetChildComponent<KButton>("Button_zhizao");
            _needMaterial = _buyOrMakeContainer.AddChildComponent<IconContainer>("Container_icon");
            _numTxt = _buyOrMakeContainer.GetChildComponent<StateText>("Label_txtShuliang");
            _cdTimeTxt = _buyOrMakeContainer.GetChildComponent<StateText>("Label_txtCDTime");
            SetLearnBtn();
            AddEventListener();
        }

        private void SetLearnBtn()
        {
            TextWrapper[] textWrappers = _openGiftBtn.GetComponentsInChildren<TextWrapper>(true);
            for (int i = 0; i < textWrappers.Length; i++)
            {
                textWrappers[i].text = MogoLanguageUtil.GetContent(32060);
            }
        }

        public void SetState(int craftId)
        {
            TimerHeap.DelTimer(timeId);
            currentCraftId = craftId;
            CraftItemData itemData = MakeManager.Instance.makeData.GetCraftItemData(craftId);
            _currentCraftItemData = itemData;
            HideAllContainer();
            if (itemData == null)
            {
                return;
            }
            if (itemData.isLocked == true)
            {
                _lockedContainer.Visible = true;
                _lockTxt.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(32048), itemData.needPlayerLevel);
                return;
            }
            if (itemData.NeedLearn == true && MakeManager.Instance.makeData.IsFormulaLearned(craftId) == false)
            {
                _needLearnContainer.Visible = true;
                Dictionary<int, int> learnCost = mfg_formula_helper.GetFormulaLearnCost(craftId);
                int itemId = 0;
                foreach (var pair in learnCost)
                {
                    itemId = pair.Key;
                }
                _learnTxt.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(32061), item_helper.GetName(itemId));
                return;
            }
            if (itemData.needGift != 0 && MakeManager.Instance.makeData.HasOpenCraftGift(craftId) == false)
            {
                _needOpenGiftContainer.Visible = true;
                int talentId = mfg_formula_helper.GetMakeGiftByCraftId(craftId);
                _openGiftTxt.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(32049), mfg_talent_helper.GetTalentName(talentId));
                return;
            }
            _buyOrMakeContainer.Visible = true;
            if (mfg_formula_helper.GetProductType(craftId) == 2)
            {
                _showMySuitsBtn.Visible = true;
            }
            if (function_helper.IsFunctionOpen(FunctionId.tradeMarket)
                && mfg_formula_helper.GetViewIdByProductType(_currentCraftItemData.craftType) != 0
                && trademarket_helper.ContainsItem(_currentCraftItemData.productId))
            {
                _buyBtn.Visible = true;
            }
            else
            {
                _buyBtn.Visible = false;
            } 
            SetCDState();
            _needMaterial.SetIcon(item_helper.GetIcon(6));
            if (MakeManager.Instance.makeData.HasOpenCraftGift(craftId))
            {
                _numTxt.CurrentText.text = mfg_formula_helper.GetExpertFormulaMakeCostOfSpirit(craftId).ToString();
            }
            else
            {
                _numTxt.CurrentText.text = mfg_formula_helper.GetFormulaMakeCostOfSpirit(craftId).ToString();
            }
        }

        private void SetCDState()
        {
            DateTime cdTimeStamp = MakeManager.Instance.makeData.GetFormulaCDTime(currentCraftId);
            if ((cdTimeStamp - DateTime.Now).TotalSeconds <= 0.0f)
            {
                _cdTimeTxt.CurrentText.text = string.Empty;
                _makeBtn.SetButtonActive();
            }
            else
            {
                _makeBtn.SetButtonDisable();
                leftSeconds = (int)(cdTimeStamp - DateTime.Now).TotalSeconds;
                timeId = TimerHeap.AddTimer(0, 1000, SetCDTxt);
            }
        }

        private void SetCDTxt()
        {
            leftSeconds--;
            if (leftSeconds > 0)
            {
                _cdTimeTxt.CurrentText.text = GetLeftHourMinuteSecond();
            }
            else
            {
                _makeBtn.SetButtonActive();
                _cdTimeTxt.CurrentText.text = string.Empty;
                TimerHeap.DelTimer(timeId);
            }
        }

        private string descString = MogoLanguageUtil.GetContent(32103);
        private string GetLeftHourMinuteSecond()
        {
            int leftHours = leftSeconds / 3600;
            int leftMinutes = (leftSeconds % 3600) / 60;
            int leftSecond = leftSeconds % 60;
            return string.Format("{0}{1:00}:{2:00}:{3:00}", descString, leftHours, leftMinutes, leftSecond);
        }

        private void HideAllContainer()
        {
            _showMySuitsBtn.Visible = false;
            _lockedContainer.Visible = false;
            _needLearnContainer.Visible = false;
            _needOpenGiftContainer.Visible = false;
            _buyOrMakeContainer.Visible = false;
        }

        private void AddEventListener()
        {
            _learnBtn.onClick.AddListener(OnLearnBtnClick);
            _buyBtn.onClick.AddListener(OnBuyBtnClick);
            _makeBtn.onClick.AddListener(OnMakeBtnClick);
            _openGiftBtn.onClick.AddListener(OnOpenGiftClick);
            _showMySuitsBtn.onClick.AddListener(OnShowMySuitsBtnClick);
        }

        private void RemoveEventListener()
        {
            _learnBtn.onClick.RemoveListener(OnLearnBtnClick);
            _buyBtn.onClick.RemoveListener(OnBuyBtnClick);
            _makeBtn.onClick.RemoveListener(OnMakeBtnClick);
            _openGiftBtn.onClick.RemoveListener(OnOpenGiftClick);
            _showMySuitsBtn.onClick.RemoveListener(OnShowMySuitsBtnClick);
        }

        private void OnLearnBtnClick()
        {
            int needMaterialId = mfg_formula_helper.CheckEnoughMaterialLearn(currentCraftId);
            if (needMaterialId == 0)
            {
                MakeManager.Instance.LearnFormula(currentCraftId);
            }
            else
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.ItemChannel, needMaterialId);
            }
        }

        private void OnBuyBtnClick()
        {
            if (_currentCraftItemData == null)
            {
                return;
            }
            int viewId = mfg_formula_helper.GetViewIdByProductType(_currentCraftItemData.craftType);
            if (viewId != 0)
            {
                view_helper.OpenView(viewId);
            }
        }

        private bool showAgain = true;
        private void OnMakeBtnClick()
        {
            if (PlayerDataManager.Instance.MakeData.hasOpenOneMakeGift() == false && showAgain)
            {
                MessageBox.Show(true, string.Empty, MogoLanguageUtil.GetContent(32064), CloseMessageBox, GoGiftView, MogoLanguageUtil.GetContent(32066), MogoLanguageUtil.GetContent(32065), null, true);
                return;
            }
            int needMaterialId = mfg_formula_helper.CheckEnoughMaterialMake(currentCraftId);
            if (needMaterialId == 0)
            {
                MakeManager.Instance.MakeFormula(currentCraftId);
            }
            else
            {
                string warnString = string.Format(MogoLanguageUtil.GetContent(32001), item_helper.GetName(needMaterialId));
                ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, warnString, PanelIdEnum.Make);
            }
        }

        private void CloseMessageBox()
        {
            showAgain = false;
        }

        private void GoGiftView()
        {
            view_helper.OpenView(434);
        }

        private void OnOpenGiftClick()
        {
            // 打开制造天赋界面
            UIManager.Instance.ShowPanel(PanelIdEnum.Make, 2);
        }

        private void OnShowMySuitsBtnClick()
        {
            view_helper.OpenView(444);
        }
    }
}