﻿#region 模块信息
/*==========================================
// 文件名：MakeContentView
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake.ChildView.Make
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/26 14:22:24
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Game.UI.UIComponent;
using MogoEngine.Events;
namespace ModuleMake
{
    public class MakeContentView : KContainer
    {
        private CurrentMakeGiftView _currentMakeGiftView;
        private NeedMaterialView _needMaterialView;
        private CurrentStateView _currentStateView;

        protected override void Awake()
        {
            _currentMakeGiftView = AddChildComponent<CurrentMakeGiftView>("Container_dangqianzhuanjing");
            _needMaterialView = AddChildComponent<NeedMaterialView>("Container_xuyaocailiao");
            _currentStateView = AddChildComponent<CurrentStateView>("Container_zhuangtai");
            AddEventListener();
        }

        public void AddEventListener()
        {
            EventDispatcher.AddEventListener<int>(MakeEvents.FORMULA_MAKE_SUCCESS, RefreshStateView);
        }

        public void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<int>(MakeEvents.FORMULA_MAKE_SUCCESS, RefreshStateView);
        }

        private void RefreshStateView(int craftId)
        {
            _currentStateView.SetState(craftId);
            _needMaterialView.SetNeedMaterial(craftId);
        }

        public void Refresh(int craftId)
        {
            _needMaterialView.SetNeedMaterial(craftId);
            _currentStateView.SetState(craftId);
            _currentMakeGiftView.Refresh(craftId);
        }
    }
}