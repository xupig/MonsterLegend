﻿#region 模块信息
/*==========================================
// 文件名：MakeCurrencyView
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake.ChildView.Make
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/26 14:22:40
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Game.UI.UIComponent;
using Common.ExtendComponent;
using Common.ServerConfig;
namespace ModuleMake
{
    public class MakeCurrencyView : KContainer
    {
        private MoneyItem goldItem;
        private MoneyItem spiritItem;

        protected override void Awake()
        {
            goldItem = AddChildComponent<MoneyItem>("Container_buyGold");
            spiritItem = AddChildComponent<MoneyItem>("Container_buyDiamond");
            goldItem.SetMoneyType(public_config.MONEY_TYPE_GOLD);
            spiritItem.SetMoneyType(public_config.MONEY_TYPE_POWER);
            // 暂时隐藏精力的加号功能
            HideSpiritAddBtn();
        }

        private void HideSpiritAddBtn()
        {
            spiritItem.GetChildComponent<KButton>("Button_tianjia").Visible = false;
        }
    }

}