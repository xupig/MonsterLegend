﻿#region 模块信息
/*==========================================
// 文件名：MakeView
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake.ChildView
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/24 11:14:45
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;

namespace ModuleMake
{
    public class MakeView : KContainer
    {
        private CategoryList _toggleGroup;
        private SuitScrollMenu _suitScrollMenu;
        private MakeContentView _contentView;
        private MakeFilterMenu _filterMenu;
        private MakeCurrencyView _currencyView;
        private List<int> categoryList;

        protected override void Awake()
        {
            _toggleGroup = GetChildComponent<CategoryList>("List_categoryList");
            KContainer _makeProductContainer = GetChildComponent<KContainer>("Container_taozhuang");
            _suitScrollMenu = _makeProductContainer.AddChildComponent<SuitScrollMenu>("Container_fumojuanzhou");
            _contentView = _makeProductContainer.AddChildComponent<MakeContentView>("Container_zhizaoright");
            _filterMenu = _makeProductContainer.AddChildComponent<MakeFilterMenu>("Container_shaixuan");
            _currencyView = _makeProductContainer.AddChildComponent<MakeCurrencyView>("Container_huobi");
            InitCategoryMenu();
        }

        private void InitCategoryMenu()
        {
            categoryList = mfg_formula_helper.GetCategoryList();
            List<CategoryItemData> list = new List<CategoryItemData>();
            for (int i = 0; i < categoryList.Count; i++)
            {
                list.Add(CategoryItemData.GetCategoryItemData(mfg_formula_helper.GetCategoryName(categoryList[i]).ToString()));
            }
            _toggleGroup.SetDataList<CategoryListItem>(list, 1);
        }

        protected override void OnEnable()
        {
            AddEventListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        public void Show(int index)
        {
            Visible = true;
            _toggleGroup.SelectedIndex = index;
            OnSelectedIndexChange(_toggleGroup, index);
        }

        private void OnSelectedIndexChange(CategoryList toggleGroup, int index)
        {
            SetDefaultVocationFilterCondition(categoryList[index]);
            RefreshContent(index);
        }

        private void SetDefaultVocationFilterCondition(int type)
        {
            // 从配置表中读取所有可以筛选的条件项
            List<int> vocationList = mfg_formula_helper.GetVocationConditionList(type);
            List<int> levelList = mfg_formula_helper.GetLevelConditionList(type);
            List<int> tradeList = mfg_formula_helper.GetTradeConditionList(type);
            _filterMenu.SetFilterConditionMenu(vocationList, levelList, tradeList);
            _filterMenu.ClearAllFilterCondition();
            MakeManager.Instance.makeData.RemoveAllFilterConditions();
            _filterMenu.SetVocationFilterCondition((int)PlayerAvatar.Player.vocation);
            if (levelList.Contains((int)PlayerAvatar.Player.level / 10 * 10))
            {
                _filterMenu.SetLevelFilterCondition((int)PlayerAvatar.Player.level);
            }
            else
            {
                _filterMenu.SetLevelFilterCondition(levelList[0]);
            }
        }

        public void Hide()
        {
            Visible = false;
        }

        private void RefreshContent(int index)
        {
            int type = categoryList[index];
            _suitScrollMenu.SetGroupMenu(type);
            _contentView.Refresh(SuitScrollMenu.defaultSuitId);
        }

        private void AddEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChange);
            _suitScrollMenu.onClick.AddListener(OnMenuClicked);
            EventDispatcher.AddEventListener(MakeEvents.FORMULA_MENU_REFRESH, Refresh);
            EventDispatcher.AddEventListener<int>(MakeEvents.REFRESH_CONTENT_STATE, RefreshContentState);
        }

        private void Refresh()
        {
            RefreshContent(_toggleGroup.SelectedIndex);
        }

        private void RemoveEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChange);
            _suitScrollMenu.onClick.RemoveListener(OnMenuClicked);
            EventDispatcher.RemoveEventListener(MakeEvents.FORMULA_MENU_REFRESH, Refresh);
            EventDispatcher.RemoveEventListener<int>(MakeEvents.REFRESH_CONTENT_STATE, RefreshContentState);
        }

        private void RefreshContentState(int formulaId)
        {
            _contentView.Refresh(formulaId);
        }

        private void OnMenuClicked(int craftId)
        {
            _contentView.Refresh(craftId);
        }

    }
}
