﻿#region 模块信息
/*==========================================
// 文件名：MakeMessageType
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/30 19:32:25
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMake
{
    public enum MakeMessageType
    {
        SHOWOPENGIFT = 0,
        SHOWGIFTDETAIL = 1,
    }
}
