﻿#region 模块信息
/*==========================================
// 文件名：MakeOpenGiftTips
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake.MakePopView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/12/1 20:12:26
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
namespace ModuleMake
{
    public class GiftItem : KList.KListItemBase
    {
        private int id;

        private StateText _name;
        private StateText _desc;

        public override object Data
        {
            get
            {
                return id;
            }
            set
            {
                if (value != null)
                {
                    id = (int)value;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            _name.CurrentText.text = mfg_talent_helper.GetTalentName(id);
            _desc.CurrentText.text = mfg_talent_helper.GetTalentDesc(id);
        }

        protected override void Awake()
        {
            _name = GetChildComponent<StateText>("Label_txtXiaobiaoti");
            _desc = GetChildComponent<StateText>("Label_txtNr");
        }

        public override void Dispose()
        {

        }
    }

    public class MakeOpenGiftTips : KContainer
    {
        private KButton _closeBtn;
        private KList _openGiftList;

        protected override void Awake()
        {
            _closeBtn = GetChildComponent<KButton>("Button_close");
            _openGiftList = GetChildComponent<KList>("List_zhuanjing");
            _openGiftList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _closeBtn.onClick.AddListener(ClosePanel);
        }

        private void ClosePanel()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.MakeMessage);
        }

        public void Show()
        {
            Visible = true;
            _openGiftList.RemoveAll();
            _openGiftList.SetDataList<GiftItem>(MakeManager.Instance.makeData.GetOpenCraftGiftList());
        }

        public void Hide()
        {
            Visible = false;
        }
    }
}