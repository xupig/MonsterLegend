﻿#region 模块信息
/*==========================================
// 文件名：MakeGiftDetailTips
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake.MakePopView
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/12/1 20:12:43
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using System;
using System.Collections.Generic;

namespace ModuleMake
{
    public class LockCraftItem : KList.KListItemBase
    {
        public int id;
        private StateText _text;

        public override object Data
        {
            get
            {
                return id;
            }
            set
            {
                if (value != null)
                {
                    id = (int)value;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            _text.CurrentText.text = MakeManager.Instance.makeData.GetCraftItemData(id).Name;
        }

        protected override void Awake()
        {
            _text = GetChildComponent<StateText>("Label_txtNr");
        }

        public override void Dispose()
        {
        }
    }

    public class MakeGiftDetailTips : KContainer
    {
        private KButton _closeBtn;
        private StateText _titleTxt;
        private StateText _desc;
        private KScrollView _scrollView;
        private KPageableList _lockCraftList;
        private KContainer _content;
        private KContainer _giftContainer;

        protected override void Awake()
        {
            _closeBtn = GetChildComponent<KButton>("Button_close");
            _titleTxt = GetChildComponent<StateText>("Label_txtBiaoti");
            _scrollView = GetChildComponent<KScrollView>("ScrollView_xiangqing");
            _content = _scrollView.GetChildComponent<KContainer>("mask/content");
            _giftContainer = _content.GetChildComponent<KContainer>("Container_jiesuopeifang");
            _desc = _scrollView.GetChildComponent<StateText>("mask/content/Container_tianfuxiaoguo/Label_txtNr");
            _lockCraftList = _scrollView.GetChildComponent<KPageableList>("mask/content/Container_jiesuopeifang/List_content");
            _lockCraftList.SetGap(0, 10);
            _lockCraftList.SetDirection(KList.KListDirection.LeftToRight, 2);
            _closeBtn.onClick.AddListener(ClosePanel);
        }

        private void ClosePanel()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.MakeMessage);
        }

        public void Show(int GiftId)
        {
            Visible = true;
            _scrollView.ResetContentPosition();
            _desc.CurrentText.text = mfg_talent_helper.GetTalentDesc(GiftId);
            _titleTxt.CurrentText.text = MogoLanguageUtil.GetContent(mfg_talent_helper.GetTalentTipTitleTxt(GiftId));
            List<int> dataList = mfg_formula_helper.GetGiftLockFormulaIdList(GiftId);
            if (dataList != null && dataList.Count > 0)
            {
                _giftContainer.Visible = true;
                _lockCraftList.SetDataList<LockCraftItem>(dataList, 10);
            }
            else
            {
                _giftContainer.Visible = false;
            }
            TimerHeap.AddTimer(500, 0, RecalculateContentSize);
        }

        private void RecalculateContentSize()
        {
            _giftContainer.RecalculateSize();
            _content.RecalculateSize();
        }

        public void Hide()
        {
            Visible = false;
        }
    }
}