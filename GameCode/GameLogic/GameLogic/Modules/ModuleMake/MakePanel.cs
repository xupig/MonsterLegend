﻿#region 模块信息
/*==========================================
// 文件名：MakePanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/24 9:45:06
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using UnityEngine;

namespace ModuleMake
{
    public class MakePanel : BasePanel
    {
        public const int MAKE = 0;
        public const int MAKEGIFT = 1;

        private KToggleGroup _toggleGroup;
        private MakeView _makeView;
        private MakeGiftView _makeGiftView;
        private int currentIndex;
        private int currentsubMenuIndex;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _makeView = AddChildComponent<MakeView>("Container_Zhizao");
            _makeGiftView = AddChildComponent<MakeGiftView>("Container_Zhizaotianfu");
            _toggleGroup = AddChildComponent<KToggleGroup>("ToggleGroup_biaoti");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.Make; }
        }

        public override void OnShow(object data)
        {
            SetData(data);
            AddEventListener();
            RefreshTabGreenPoint();
        }

        public override void SetData(object data)
        {
            currentIndex = 0;
            currentsubMenuIndex = 0;
            if (data is int)
            {
                currentIndex = (int)data - 1;
            }
            if (data is int[])
            {
                int[] viewparams = data as int[];
                currentIndex = viewparams[0] - 1;
                currentsubMenuIndex = viewparams[1] - 1;
            }
            switch (currentIndex)
            {
                case 0:
                    OnGoToMake();
                    break;
                case 1:
                    OnGoToMakeGift();
                    break;
            }
        }

        private void RefreshTabGreenPoint()
        {
            _toggleGroup.GetToggleList()[0].SetGreenPoint(false);
            _toggleGroup.GetToggleList()[1].SetGreenPoint(false);
        }

        private void OnGoToMake()
        {
            _toggleGroup.SelectIndex = MAKE;
            OnSelextedIndexChanged(_toggleGroup, MAKE);
        }

        private void OnGoToMakeGift()
        {
            OnSelextedIndexChanged(_toggleGroup, MAKEGIFT);
            _toggleGroup.SelectIndex = MAKEGIFT;
        }

        public void HideMakeTabPoint()
        {
            _toggleGroup.GetToggleList()[0].SetGreenPoint(false);
        }

        public void HideTrainTabPoint()
        {
            _toggleGroup.GetToggleList()[1].SetGreenPoint(false);
        }

        public override void OnClose()
        {
            RemoveEventListener();
            _makeView.Hide();
        }

        private void AddEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.AddListener(OnSelextedIndexChanged);
            EventDispatcher.AddEventListener(MakeEvents.HIDE_MAKE_TAB_POINT, HideMakeTabPoint);
            EventDispatcher.AddEventListener(MakeEvents.HIDE_TRAIN_TAB_POINT, HideTrainTabPoint);
        }

        private void RemoveEventListener()
        {
            _toggleGroup.onSelectedIndexChanged.RemoveListener(OnSelextedIndexChanged);
            EventDispatcher.RemoveEventListener(MakeEvents.HIDE_MAKE_TAB_POINT, HideMakeTabPoint);
            EventDispatcher.RemoveEventListener(MakeEvents.HIDE_TRAIN_TAB_POINT, HideTrainTabPoint);
        }

        private void OnSelextedIndexChanged(KToggleGroup toggleGroup, int index)
        {
            switch (index)
            {
                case MAKE:
                    _makeGiftView.Hide();
                    _makeView.Show(currentsubMenuIndex);
                    break;
                case MAKEGIFT:
                    _makeGiftView.Show();
                    _makeView.Hide();
                    break;
                default:
                    break;
            }
        }
    }
}
