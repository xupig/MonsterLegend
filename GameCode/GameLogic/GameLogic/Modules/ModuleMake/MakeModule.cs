﻿#region 模块信息
/*==========================================
// 文件名：MakeModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleMake
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/24 9:49:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleMake
{
    public class MakeModule:BaseModule
    {

        public override ModuleType ModuleType
        {
            get { return ModuleType.Permanent; }
        }

        public override void Init()
        {
            RegisterPopPanelBunchPanel(PanelIdEnum.MakeMessage, PanelIdEnum.Make);
            AddPanel(PanelIdEnum.Make, "Container_MakePanel", MogoUILayer.LayerUIPanel, "ModuleMake.MakePanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.MakeMessage, "Container_MakeMessagePanel", MogoUILayer.LayerUIPopPanel, "ModuleMake.MakeMessagePanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            
        }

        protected override void OnBeforeShowPanel(PanelIdEnum panelId)
        {
            
        }
    }
}
