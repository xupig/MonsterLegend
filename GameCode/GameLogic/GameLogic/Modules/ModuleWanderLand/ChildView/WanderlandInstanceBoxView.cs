﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleWanderLand
{
    public class WanderlandInstanceBoxView : KContainer
    {
        private CopyInstanceBoxDataWrapper _wrapper;
        private WanderLandInstance _data;

        private KButton _btnClose;
        private StateText _textTitle;
        private KList _list;
        private KButton _btnFight;
        private KButton _btnGetReward;

        public KComponentEvent onClose = new KComponentEvent();

        protected override void Awake()
        {
            _btnClose = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _btnFight = GetChildComponent<KButton>("Button_tiaozhan");
            _btnGetReward = GetChildComponent<KButton>("Button_yijianlingqu");
            _textTitle = GetChildComponent<StateText>("Label_txtBaoxiangjiangli");
            _list = GetChildComponent<KList>("List_rewardList");
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(OnClose);
            _btnFight.onClick.AddListener(OnFight);
            _btnGetReward.onClick.AddListener(OnGetReward);
            EventDispatcher.AddEventListener(WanderLandEvents.RefreshGetBoxReward, OnGetBoxReward);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(OnClose);
            _btnFight.onClick.RemoveListener(OnFight);
            _btnGetReward.onClick.RemoveListener(OnGetReward);
            EventDispatcher.RemoveEventListener(WanderLandEvents.RefreshGetBoxReward, OnGetBoxReward);
        }

        private void OnFight()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.WanderLandDetail, 
                new WanderlandDetailDataWrapper(_wrapper.WanderLandChapter, _wrapper.WanderLandInstance.Level - 1));
            OnClose();
        }

        private void OnGetReward()
        {
            WanderLandManager.Instance.RequsetGetAllInstanceBoxReward(_wrapper.WanderLandInstance.InstanceId);
        }

        private void OnGetBoxReward()
        {
            Refresh();
        }

        private void OnClose()
        {
            onClose.Invoke();
        }

        public CopyInstanceBoxDataWrapper Data
        {
            set
            {
                _wrapper = value as CopyInstanceBoxDataWrapper;
                _data = _wrapper.WanderLandInstance;
                Refresh();
            }
        }

        private void Refresh()
        {
            if (_data.InstaceBoxState == BoxState.CAN_GET)
            {
                _btnGetReward.SetButtonActive();
            }
            else
            {
                _btnGetReward.SetButtonDisable();
            }
            _textTitle.CurrentText.text = MogoLanguageUtil.GetContent(40418, _data.Level);
            List<Wrapper> wrapperList = GetWrapperList();
            _list.SetDataList<BoxItem>(wrapperList, 1);
        }

        private List<Wrapper> GetWrapperList()
        {
            List<Wrapper> wrapperList = new List<Wrapper>();
            for (int i = 0; i < _data.RewardIdList.Count; i++)
            {
                Wrapper wrapper = new Wrapper(_data.InstanceId, _data.RewardIdList[i], _data.GetStarBoxState(i + 1));
                wrapperList.Add(wrapper);
            }
            return wrapperList;
        }

        private class Wrapper
        {
            public int InstanceId;
            public int RewardId;
            public BoxState BoxState;

            public Wrapper(int instanceId, int rewardId, BoxState boxState)
            {
                this.InstanceId = instanceId;
                this.RewardId = rewardId;
                this.BoxState = boxState;
            }
        }

        private class BoxItem : KList.KListItemBase
        {
            private Wrapper _wrapper;

            private StateText _textStarNum;
            private KButton _btnCanGet;
            private KButton _btnCanNotGet;
            private List<CopyBoxRewardGrid> _list;

            protected override void Awake()
            {
                _textStarNum = GetChildComponent<StateText>("Label_txtXingxinggeshu");
                _btnCanGet = GetChildComponent<KButton>("Button_lingqu");
                _btnCanNotGet = GetChildComponent<KButton>("Button_weidacheng");
                _list = new List<CopyBoxRewardGrid>();
                for (int i = 0; i < WanderLandInstance.MAX_STAR; i++)
                {
                    _list.Add(AddChildComponent<CopyBoxRewardGrid>(string.Format("Container_jiangli/Container_item{0}", i)));
                }
            }

            public override void Dispose()
            {
            }

            public override void Show()
            {
                _btnCanGet.onClick.AddListener(OnGetReward);
            }

            public override void Hide()
            {
                _btnCanGet.onClick.RemoveListener(OnGetReward);
            }

            private void OnGetReward()
            {
                WanderLandManager.Instance.RequsetGetInstanceBoxReward(_wrapper.InstanceId, Index + 1);
            }

            public override object Data
            {
                set
                {
                    _textStarNum.CurrentText.text = (Index + 1).ToString();
                    _wrapper = value as Wrapper;
                    Refresh();
                }
            }

            private void Refresh()
            {
                List<BaseItemData> itemDataList = item_reward_helper.GetItemDataList(_wrapper.RewardId);
                for (int i = 0; i < _list.Count; i++)
                {
                    if (i < itemDataList.Count)
                    {
                        _list[i].Visible = true;
                        _list[i].Data = new CopyBoxDataWrapper(itemDataList[i], _wrapper.BoxState);
                    }
                    else
                    {
                        _list[i].Visible = false;
                    }
                }
                _btnCanGet.Visible = false;
                _btnCanNotGet.Visible = false;
                switch (_wrapper.BoxState)
                {
                    case BoxState.CAN_GET:
                        _btnCanGet.Visible = true;
                        break;
                    case BoxState.CANNOT_GET:
                        _btnCanNotGet.Visible = true;
                        MogoGameObjectHelper.SetButtonLabel(_btnCanNotGet, MogoLanguageUtil.GetContent(40415));
                        break;
                    case BoxState.HAD_GOT:
                        _btnCanNotGet.Visible = true;
                        MogoGameObjectHelper.SetButtonLabel(_btnCanNotGet, MogoLanguageUtil.GetContent(40416));
                        break;
                }
            }
        }
    }
}
