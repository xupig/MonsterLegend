﻿using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleWanderLand
{
    public class WanderlandChapterBoxView : KContainer
    {
        private CopyProgressBoxDataWrapper _wrapper;

        private KList _list;
        private KButton _btnClose;
        private GameObject _goCanNotGet;
        private KButton _btnCanGet;
        private StateText _textStarNum;

        public KComponentEvent onClose = new KComponentEvent();

        protected override void Awake()
        {
            _btnClose = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _goCanNotGet = GetChild("Button_weidacheng");
            _btnCanGet = GetChildComponent<KButton>("Button_lingqu");
            _textStarNum = GetChildComponent<StateText>("Label_txtStarNum");
            InitList();
        }

        private void InitList()
        {
            _list = GetChildComponent<KList>("List_content");
            _list.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue, 1);
            _list.SetGap(0, 23);
        }

        protected override void OnEnable()
        {
            AddListener();
        }

        protected override void OnDisable()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnClose.onClick.AddListener(OnClose);
            _btnCanGet.onClick.AddListener(OnGetReward);
            EventDispatcher.AddEventListener(CopyEvents.GotStarRewardListChange, Refresh);
            EventDispatcher.AddEventListener<int, int>(CopyEvents.GET_BOX_REWARD, GetBoxReward);
        }

        private void RemoveListener()
        {
            _btnClose.onClick.RemoveListener(OnClose);
            _btnCanGet.onClick.RemoveListener(OnGetReward);
            EventDispatcher.RemoveEventListener(CopyEvents.GotStarRewardListChange, Refresh);
            EventDispatcher.RemoveEventListener<int, int>(CopyEvents.GET_BOX_REWARD, GetBoxReward);
        }

        private void OnClose()
        {
            onClose.Invoke();
        }

        private void OnGetReward()
        {
            MissionManager.Instance.RequsetGetStarSpiritReward(_wrapper.ChapterId, _wrapper.StarNum);
        }

        private void GetBoxReward(int chapterId, int starNum)
        {
            if ((_wrapper.ChapterId == chapterId) && (_wrapper.StarNum == starNum))
            {
                List<KList.KListItemBase> itemList = _list.ItemList;
                for (int i = 0; i < itemList.Count; i++)
                {
                    CopyBoxRewardGrid rewardGrid = itemList[i] as CopyBoxRewardGrid;
                    rewardGrid.DoTween();
                }
            }
        }

        public CopyProgressBoxDataWrapper Data
        {
            set
            {
                _wrapper = value;
                Refresh();
            }
        }

        private void Refresh()
        {
            CopyData copyData = PlayerDataManager.Instance.CopyData;
            BoxState boxState = copyData.GetBoxState(_wrapper.ChapterId, _wrapper.StarNum);
            RefreshState(boxState);
            RefreshReward(boxState);
        }

        private void RefreshState(BoxState boxState)
        {
            HideAllState();
            switch (boxState)
            {
                case BoxState.CAN_GET:
                    _btnCanGet.Visible = true;
                    break;
                case BoxState.CANNOT_GET:
                    _goCanNotGet.SetActive(true);
                    break;
            }
            _textStarNum.CurrentText.text = _wrapper.StarNum.ToString();

        }

        private void HideAllState()
        {
            _btnCanGet.Visible = false;
            _goCanNotGet.SetActive(false);
        }

        //要求策划奖励不随机
        //客户端图标不用刷新
        private void RefreshReward(BoxState boxState)
        {
            List<CopyBoxDataWrapper> wrapperList = GetWrapperList(boxState);
            _list.SetDataList<CopyBoxRewardGrid>(wrapperList, 1);
            MogoLayoutUtils.SetCenter(_list.gameObject);
        }

        private List<CopyBoxDataWrapper> GetWrapperList(BoxState boxState)
        {
            List<CopyBoxDataWrapper> wrapperList = new List<CopyBoxDataWrapper>();
            int rewardId = chapters_helper.GetStarRewardId(_wrapper.ChapterId, _wrapper.StarNum);
            item_reward itemReward = item_reward_helper.GetItemReward(rewardId);
            List<BaseItemData> baseItemDataList = item_reward_helper.GetItemDataList(itemReward, PlayerAvatar.Player.vocation);
            for (int i = 0; i < baseItemDataList.Count; i++)
            {
                CopyBoxDataWrapper wrapper = new CopyBoxDataWrapper(baseItemDataList[i], boxState);
                wrapperList.Add(wrapper);
            }
            return wrapperList;
        }
    }
}
