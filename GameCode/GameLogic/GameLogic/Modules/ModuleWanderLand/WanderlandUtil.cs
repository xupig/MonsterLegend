﻿using Common.Data;
using Common.Global;
using Common.Utils;
using GameData;
using GameMain.GlobalManager;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleWanderLand
{
    public class WanderlandUtil
    {
        public static void ShowTips(int instanceId, Action callback)
        {
            int chapterId = chapters_helper.GetChapterId(instanceId);
            if (chapterId == 1)
            {
                callback.Invoke();
                return;
            }
            int level = chapters_helper.GetMinLevel(chapterId);
            WanderLandData wanderlandData = PlayerDataManager.Instance.WanderLandData;
            if (!wanderlandData.HadFighted())
            {
                string warmContent = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, MogoLanguageUtil.GetContent(1450));
                string content = string.Format(MogoLanguageUtil.GetContent(1451), level, warmContent);
                MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, callback);
            }
            else if (wanderlandData.FightFirstChapter())
            {
                int index = chapters_helper.GetInstanceIndex(ChapterType.WanderLand, instanceId);
                if (index == 0)
                {
                    string warmContent = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, MogoLanguageUtil.GetContent(1450));
                    string content = string.Format(MogoLanguageUtil.GetContent(1451), level, warmContent);
                    MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, callback);
                }
            }
            else
            {
                callback.Invoke();
            }
        }
    }
}
