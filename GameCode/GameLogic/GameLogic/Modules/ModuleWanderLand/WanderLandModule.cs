﻿#region 模块信息
/*==========================================
// 文件名：WanderLandModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleWanderLand
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/23 10:46:45
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using GameMain.Entities;
using GameLoader.Utils.CustomType;
using MogoEngine.Events;
using Common.Events;
using GameData;
using GameMain.GlobalManager;
using Common.ServerConfig;

namespace ModuleWanderLand
{
    public class WanderLandModule : BaseModule
    {
        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            RegisterPopPanelBunchPanel(PanelIdEnum.WanderLandDetail, PanelIdEnum.WanderLand);
            RegisterPopPanelBunchPanel(PanelIdEnum.WanderLandBox, PanelIdEnum.WanderLand);
            AddPanel(PanelIdEnum.WanderLand, "Container_WanderLandPanel", MogoUILayer.LayerUIPanel, "ModuleWanderLand.WanderLandPanel", BlurUnderlay.Have, HideMainUI.Hide);
            AddPanel(PanelIdEnum.WanderLandDetail, "Container_WanderLandDetailPanel", MogoUILayer.LayerUIPopPanel, "ModuleWanderLand.WanderLandDetailPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);
            AddPanel(PanelIdEnum.WanderLandBox, "Container_WanderLandBoxPanel", MogoUILayer.LayerUIPopPanel, "ModuleWanderLand.WanderLandBoxPanel", BlurUnderlay.Empty, HideMainUI.Not_Hide);

           
            EventDispatcher.AddEventListener(SceneEvents.SCENE_LOADED, OnEnterMap);
        }

        private void OnEnterMap()
        {
            int mapType = map_helper.GetMapType(GameSceneManager.GetInstance().preMapID);
            if (mapType == public_config.MAP_TYPE_TRY_COPY)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.WanderLand);
            }
        }
    }
}
