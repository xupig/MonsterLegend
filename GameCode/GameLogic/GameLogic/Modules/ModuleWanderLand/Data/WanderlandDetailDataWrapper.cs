﻿using Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleWanderLand
{
    public class WanderlandDetailDataWrapper
    {
        public WanderLandChapter Chapter;
        public int SelectLevel;

        public WanderlandDetailDataWrapper(WanderLandChapter chapter, int selectLevel)
        {
            this.Chapter = chapter;
            this.SelectLevel = selectLevel;
        }
    }
}
