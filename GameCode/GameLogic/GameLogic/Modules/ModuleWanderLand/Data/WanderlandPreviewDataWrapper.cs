﻿using Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleWanderLand
{
    public class WanderlandPreviewDataWrapper
    {
        public WanderLandChapter WanderLandChapter;
        public WanderLandInstance WanderLandInstance;

        public WanderlandPreviewDataWrapper(WanderLandChapter wanderLandChapter, WanderLandInstance wanderLandInstance)
        {
            this.WanderLandChapter = wanderLandChapter;
            this.WanderLandInstance = wanderLandInstance;
        }
    }
}
