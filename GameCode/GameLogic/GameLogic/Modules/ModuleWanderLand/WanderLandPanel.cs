﻿using Common.Base;
using Game.UI.UIComponent;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using GameMain.GlobalManager;
using GameData;
using Common.Data;
using MogoEngine.Events;
using Common.Events;
using UnityEngine;
using Common.Utils;
using Common.ExtendComponent;

namespace ModuleWanderLand
{
    public class WanderLandPanel : BasePanel
    {
        private readonly int[] xArray = new int[] { 305, 830 };
        private readonly int[] yArray = new int[] { -446, -362, -278, -194, -110 };

        private List<WanderLandChapter> _chapterList;
        private WanderLandChapter _currentChapter;
        private int _currentIndex = 0;

        private CategoryList _categoryList;
        private WanderLandTreasureBox[] _treasureBoxArray;
        private WanderlandBottom _bottom;
        private WanderlandPopList _pop;
        private GameObject _goTitle;
        private StateText _textCurrentLevel;
        private LStarList _starList;
        private Locater _locater;
        private CopyProgress _progress;

        private WanderLandData _wanderlandData
        {
            get
            {
                return PlayerDataManager.Instance.WanderLandData;
            }
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.WanderLand; }
        }

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("Button_close");
            _goTitle = GetChild("ToggleGroup_biaoti");
            _pop = AddChildComponent<WanderlandPopList>("Container_tanchu");
            _bottom = AddChildComponent<WanderlandBottom>("Container_bottom");
            _progress = AddChildComponent<CopyProgress>("Container_progress");
            InitCurrentLevel();
            InitTreasureBox();
            InitCategory();
            MogoAtlasUtils.AddSprite(GetChild("Container_bg"), "wanderlandBg");
        }

        private void InitCurrentLevel()
        {
            _textCurrentLevel = GetChildComponent<StateText>("Container_guangkabiaoti/Label_txtGuanka");
            _locater = AddChildComponent<Locater>("Container_guangkabiaoti");
            _starList = AddChildComponent<LStarList>("Container_guangkabiaoti/Container_xingxing");
        }

        private void InitTreasureBox()
        {
            _treasureBoxArray = new WanderLandTreasureBox[WanderLandData.MAX_INSTANCE_NUM_PER_CHAPTER];
            for (int i = 0; i < WanderLandData.MAX_INSTANCE_NUM_PER_CHAPTER; i++)
            {
                _treasureBoxArray[i] = AddChildComponent<WanderLandTreasureBox>(string.Format("Container_cengshu/Container_cengshu{0}", i + 1));
            }
        }

        private void InitCategory()
        {
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            _categoryList.SetDataList<WanderlandCategoryListItem>(chapters_helper.GetNameList(ChapterType.WanderLand), 2);
        }

        private void AddListener()
        {
            _categoryList.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            _categoryList.onCoroutineEnd.AddListener(OnCoroutineEnd);
            _pop.onSelect.AddListener(SetInstanceIndex);
            EventDispatcher.AddEventListener(WanderLandEvents.RefreshWanderLandView, Refresh);
            EventDispatcher.AddEventListener(WanderLandEvents.SweepSuccess, Refresh);
            EventDispatcher.AddEventListener(WanderLandEvents.Reset, OnReset);
            EventDispatcher.AddEventListener(CopyEvents.GotStarRewardListChange, Refresh);
            EventDispatcher.AddEventListener(WanderLandEvents.BoxPanelClose, Refresh);
            EventDispatcher.AddEventListener(WanderLandEvents.RefreshGetBoxReward, OnRefreshGetBoxReward);
            EventDispatcher.AddEventListener(WanderLandEvents.PanelVisibleChange, OnPanelVisibleChange);
            EventDispatcher.AddEventListener<int>(WanderLandEvents.DetailViewSlide, OnDetailViewSlide);
        }

        private void RemoveListener()
        {
            _categoryList.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            _categoryList.onCoroutineEnd.RemoveListener(OnCoroutineEnd);
            _pop.onSelect.RemoveListener(SetInstanceIndex);
            EventDispatcher.RemoveEventListener(WanderLandEvents.RefreshWanderLandView, Refresh);
            EventDispatcher.RemoveEventListener(WanderLandEvents.SweepSuccess, Refresh);
            EventDispatcher.RemoveEventListener(WanderLandEvents.Reset, OnReset);
            EventDispatcher.RemoveEventListener(CopyEvents.GotStarRewardListChange, Refresh);
            EventDispatcher.RemoveEventListener(WanderLandEvents.BoxPanelClose, Refresh);
            EventDispatcher.RemoveEventListener(WanderLandEvents.RefreshGetBoxReward, OnRefreshGetBoxReward);
            EventDispatcher.RemoveEventListener(WanderLandEvents.PanelVisibleChange, OnPanelVisibleChange);
            EventDispatcher.RemoveEventListener<int>(WanderLandEvents.DetailViewSlide, OnDetailViewSlide);
        }

        private void OnReset()
        {
            SetInstanceIndex(0);
            Refresh();
        }

        private void OnDetailViewSlide(int index)
        {
            index = Mathf.Min(_currentChapter.GetFightLevel() - 1, index);
            SetInstanceIndex(index);
        }

        private void OnCoroutineEnd()
        {
            _categoryList.SelectedIndex = _currentIndex;
            RefreshCategoryList();
        }

        private void SetInstanceIndex(int index)
        {
            _pop.Index = index;
            _bottom.SelectIndex = index;
        }

        private void OnPanelVisibleChange()
        {
            if (BaseModule.IsPanelShowing(PanelIdEnum.WanderLandBox)
                || BaseModule.IsPanelShowing(PanelIdEnum.WanderLandDetail))
            {
                _bottom.Visible = false;
                _goTitle.SetActive(false);
            }
            else
            {
                _bottom.Visible = true;
                _goTitle.SetActive(true);
            }
            _pop.Refresh(_currentChapter);
        }

        private void OnSelectedIndexChanged(CategoryList _toggleGroup, int index)
        {
            _currentIndex = index;
            SetCategoryListIndex(index);
            _currentChapter = _chapterList[index];
            _pop.IsShowingPop = false;
            SetInstanceIndex(_currentChapter.GetFightLevel() - 1);
            Refresh();
        }

        private void SetCategoryListIndex(int index)
        {
            if (index < _categoryList.ItemList.Count)
            {
                _categoryList.SelectedIndex = index;
            }
        }

        private void OnRefreshGetBoxReward()
        {
            Refresh();
        }

        private int GetIndex(object data)
        {
            int index = 0;
            if (data is int[])
            {
                index = (data as int[])[1];
            }
            else
            {
                if (_wanderlandData.HadFighted())
                {
                    index = _wanderlandData.ChapterPage - 1;
                }
                else
                {
                    for (int i = _chapterList.Count - 1; i >= 0; i--)
                    {
                        WanderLandChapter chapter = _chapterList[i];
                        if (PlayerAvatar.Player.level >= chapters_helper.GetMinLevel(chapter.ChapterId))
                        {
                            index = i;
                            break;
                        }
                    }
                }
            }
            return index;
        }

        public override void OnShow(object data)
        {
            SetData(data);
        }

        public override void SetData(object data)
        {
            //可能通过来源跳转到这里，不关闭二级界面的话 OnPanelVisibleChange会出现问题
            UIManager.Instance.ClosePanel(PanelIdEnum.WanderLandBox);
            UIManager.Instance.ClosePanel(PanelIdEnum.WanderLandDetail);

            AddListener();
            _chapterList = _wanderlandData.ChapterList;
            int index = GetIndex(data);
            OnSelectedIndexChanged(_categoryList, index);
        }

        public override void OnClose()
        {
            RemoveListener();
        }

        private void Refresh()
        {
            RefreshCategoryList();
            _bottom.SetData(_currentChapter);
            _pop.Refresh(_currentChapter);
            RefreshTreasureBox();
            _progress.Refresh(_currentChapter.ChapterId);
            RefreshCurrentInstance();
            OnPanelVisibleChange();
        }

        private void RefreshCategoryList()
        {
            List<KList.KListItemBase> itemList = _categoryList.ItemList;
            for (int i = 0; i < itemList.Count; i++)
            {
                WanderlandCategoryListItem categoryListItem = itemList[i] as WanderlandCategoryListItem;
                categoryListItem.Refresh(_chapterList[i].ChapterId);
            }
        }

        private void RefreshTreasureBox()
        {
            List<WanderLandInstance> InstanceList = _currentChapter.InstanceList;
            int fightLevel = _currentChapter.GetFightLevel();
            for (int i = 0; i < _treasureBoxArray.Length; i++)
            {
                WanderLandTreasureBox treasureBox = _treasureBoxArray[i];
                treasureBox.Refresh(_currentChapter, InstanceList[i], Int32.Parse(_currentChapter.Chapter.__icon_get[i]), Int32.Parse(_currentChapter.Chapter.__icon_unget[i]));
                if (i == fightLevel - 1)
                {
                    treasureBox.HideStar();
                }
            }
        }

        private void RefreshCurrentInstance()
        {
            RefreshCurrentLevel();
            _starList.SetNum(GetCurrentLevelStar());
            RefreshPosition();
        }

        private void RefreshCurrentLevel()
        {
            int nowLevel = _currentChapter.GetFightLevel();
            _textCurrentLevel.CurrentText.text = MogoLanguageUtil.GetContent(1428, MogoLanguageUtil.GetChineseNum(nowLevel));
        }

        private int GetCurrentLevelStar()
        {
            CopyData copyData = PlayerDataManager.Instance.CopyData;
            int fightLevel = _currentChapter.GetFightLevel();
            WanderLandInstance wanderlandInstance = _currentChapter.GetInstanceIdByLevel(fightLevel);
            return copyData.GetHistoryMaxStar(wanderlandInstance.InstanceId);
        }

        private void RefreshPosition()
        {
            int index = _currentChapter.GetFightLevel() - 1;
            int x = xArray[index % 2];
            int y = yArray[index / 2];
            _locater.Position = new Vector2(x, y);
        }
    }
}
