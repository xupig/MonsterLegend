﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/2 0:04:15
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using UnityEngine;
using ModuleCopy;
using Common.Utils;

namespace ModuleWanderLand
{
    public class WanderLandBoxPanel : BasePanel
    {
        private WanderlandInstanceBoxView _instanceBox;
        private WanderlandChapterBoxView _chapterBox;

        protected override void Awake()
        {
            _instanceBox = AddChildComponent<WanderlandInstanceBoxView>("Container_baoxiangjiangli");
            _chapterBox = AddChildComponent<WanderlandChapterBoxView>("Container_xingjijiangli");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _instanceBox.onClose.AddListener(ClosePanel);
            _chapterBox.onClose.AddListener(ClosePanel);
        }

        private void RemoveListener()
        {
            _instanceBox.onClose.RemoveListener(ClosePanel);
            _chapterBox.onClose.RemoveListener(ClosePanel);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.WanderLandBox; }
        }

        public override void OnShow(object data)
        {
            _chapterBox.Visible = false;
            _instanceBox.Visible = false;
            if (data is CopyProgressBoxDataWrapper)
            {
                _chapterBox.Visible = true;
                _chapterBox.Data = data as CopyProgressBoxDataWrapper;
            }
            else
            {
                _instanceBox.Visible = true;
                _instanceBox.Data = data as CopyInstanceBoxDataWrapper;
            }
            EventDispatcher.TriggerEvent(WanderLandEvents.PanelVisibleChange);
        }

        public override void OnClose()
        {
            EventDispatcher.TriggerEvent(WanderLandEvents.BoxPanelClose);
        }
    }
}
