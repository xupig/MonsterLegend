﻿using Common.Base;
using Common.Data;
using Common.Utils;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleWanderLand
{
    public class WanderlandPopList : KContainer
    {
        public const int MaxItemNumPerPage = 6;
        public int Index = 0;

        private Vector3 SVOriginalPosition;
        private Vector3 BGOriginalPosition;
        private float BGOriginalHeight;
        private float ItemHeight;
        public bool IsShowingPop;

        private WanderLandChapter _chapter;

        private KDummyButton _hotArea;
        private RectTransform _rectBg;
        private KButton _btnShowPop;
        private StateText _textShowPop;
        private StateImage _imageDown;
        private StateImage _imageUp;
        private KScrollView _scrollView;
        private KList _list;
        private GameObject _goPop;

        private WanderLandData _wanderlandData
        {
            get
            {
                return PlayerDataManager.Instance.WanderLandData;
            }
        }

        public KComponentEvent onClose = new KComponentEvent();
        public KComponentEvent<int> onSelect = new KComponentEvent<int>();

        protected override void Awake()
        {
            _goPop = GetChild("Container_pop");
            _hotArea = AddChildComponent<KDummyButton>("Container_pop/Container_hotArea");
            _rectBg = GetChildComponent<RectTransform>("Container_pop/ScaleImage_sharedKuangDT02");
            _btnShowPop = GetChildComponent<KButton>("Button_saodang");
            _textShowPop = GetChildComponent<StateText>("Button_saodang/label");
            _imageDown = GetChildComponent<StateImage>("Button_saodang/down");
            _imageUp = GetChildComponent<StateImage>("Button_saodang/up");
            _scrollView = GetChildComponent<KScrollView>("Container_pop/ScrollView_fubenxuanze");
            _list = GetChildComponent<KList>("Container_pop/ScrollView_fubenxuanze/mask/content");
            _list.SetDirection(KList.KListDirection.TopToDown, 1, int.MaxValue);
            _list.SetGap(6, 0);
            _list.SetPadding(3, 0, 3, 0);

            SVOriginalPosition = _scrollView.transform.localPosition;
            BGOriginalHeight = _rectBg.sizeDelta.y;
            BGOriginalPosition = _rectBg.localPosition;
            ItemHeight = GetChildComponent<RectTransform>("Container_pop/ScrollView_fubenxuanze/mask/content/item").sizeDelta.y + 6;

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _hotArea.onClick.AddListener(OnClickHotArea);
            _btnShowPop.onClick.AddListener(OnShowPopView);
            _list.onItemClicked.AddListener(OnItemClick);
            _list.onAllItemCreated.AddListener(OnCreateItemEnd);
        }

        private void RemoveListener()
        {
            _hotArea.onClick.RemoveListener(OnClickHotArea);
            _btnShowPop.onClick.RemoveListener(OnShowPopView);
            _list.onItemClicked.RemoveListener(OnItemClick);
            _list.onAllItemCreated.RemoveListener(OnCreateItemEnd);
        }

        private void OnClickHotArea()
        {
            IsShowingPop = false;
            Refresh(_chapter);
        }

        private void OnShowPopView()
        {
            IsShowingPop = !IsShowingPop;
            Refresh(_chapter);
        }

        private void OnItemClick(KList list, KList.KListItemBase itemBase)
        {
            Index = itemBase.Index;
            IsShowingPop = false;
            Refresh(_chapter);
            onSelect.Invoke(itemBase.Index);
        }

        public void Refresh(WanderLandChapter chapter)
        {
            _chapter = chapter;
            RefreshBtn();
            RefreshPopVisible(chapter);
        }

        private void RefreshBtn()
        {
            if (BaseModule.IsPanelShowing(PanelIdEnum.WanderLandBox)
                || BaseModule.IsPanelShowing(PanelIdEnum.WanderLandDetail))
            {
                _btnShowPop.Visible = false;
            }
            else
            {
                _btnShowPop.Visible = _wanderlandData.ChapterCanFight(_chapter);
            }
            _imageDown.Visible = false;
            _imageUp.Visible = false;
            if (IsShowingPop)
            {
                _imageDown.Visible = true;
            }
            else
            {
                _imageUp.Visible = true;
            }
            _textShowPop.ChangeAllStateText(MogoLanguageUtil.GetContent(40419, MogoLanguageUtil.GetChineseNum(Index + 1)));
        }

        private void RefreshPopVisible(WanderLandChapter chapter)
        {
            _goPop.SetActive(IsShowingPop);
            if (!IsShowingPop)
            {
                return;
            }
            List<int> instanceIdList = GetInstanceIdList(chapter);
            Vector2 size = _rectBg.sizeDelta;
            int itemNum = Mathf.Min(MaxItemNumPerPage, instanceIdList.Count);
            _rectBg.sizeDelta = new Vector2(size.x, ItemHeight * itemNum);
            _rectBg.localPosition = new Vector3(BGOriginalPosition.x, BGOriginalPosition.y - ItemHeight * (MaxItemNumPerPage - itemNum));
            _scrollView.transform.localPosition = new Vector3(SVOriginalPosition.x, SVOriginalPosition.y - ItemHeight * (MaxItemNumPerPage - itemNum));
            _list.SetDataList<PopItem>(instanceIdList, 1);
        }

        private List<int> GetInstanceIdList(WanderLandChapter chapter)
        {
            List<int> instanceIdList = new List<int>();
            for (int i = 0; i < chapter.InstanceList.Count; i++)
            {
                if (chapter.InstanceList[i].Level <= chapter.GetFightLevel())
                {
                    instanceIdList.Add(chapter.InstanceList[i].InstanceId);
                }
            }
            return instanceIdList;
        }

        private void OnCreateItemEnd()
        {
            for (int i = 0; i < _list.ItemList.Count; i++)
            {
                PopItem item = _list.ItemList[i] as PopItem;
                item.SetCheckmarkVisible(item.Index == Index);
            }
        }

        class PopItem : KList.KListItemBase
        {
            private StateImage _imageCheckmark;
            private StateText _textContent;

            protected override void Awake()
            {
                _imageCheckmark = GetChildComponent<StateImage>("Image_checkmark");
                _textContent = GetChildComponent<StateText>("Label_Label");
            }

            public override void Dispose()
            {
            }

            public override object Data
            {
                set
                {
                    //传入的参数不需要用到
                    _textContent.ChangeAllStateText( MogoLanguageUtil.GetContent(40419, MogoLanguageUtil.GetChineseNum(Index + 1)));
                    _imageCheckmark.Visible = false;
                }
            }

            public void SetCheckmarkVisible(bool visible)
            {
                _imageCheckmark.Visible = visible;
            }
        }
    }
}
