﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/3/31 20:51:26
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using UnityEngine;

namespace ModuleWanderLand
{
    public class WanderLandDetailGrid : KList.KListItemBase
    {
        private const int PAGE_ICON_NUM = 8;
        private const int MAX_CROSS_CONDITION_NUM = 3;
        private const int MIN_FIGHT_VALUE_NUM = 1;

        private instance _instance;
        private WanderLandInstance _wanderlandInstance;
        private WanderLandChapter _wanderlandChapter;

        private KButton _btnRecord;
        private KButton _btnFight;
        private KButton _btnSweep;
        private GameObject _goTips;

        private CopyHead _copyHead;
        private WanderlandFightConditionList _fightConditionList;
        private CopyPassConditionList _passConditionList;
        private CopyPreviewReward _previewReward;

        protected override void Awake()
        {
            _copyHead = AddChildComponent<CopyHead>("Container_left");
            _fightConditionList = AddChildComponent<WanderlandFightConditionList>("Container_right/Container_word/Container_tiaozhanyaoqiu");
            _passConditionList = AddChildComponent<CopyPassConditionList>("Container_right/Container_word/Container_xuyaoxiaohao");
            _previewReward = AddChildComponent<CopyPreviewReward>("Container_right/Container_word/Container_zhuangbei");
            _goTips = GetChild("Container_right/Container_bottomBtn/Label_txtqianti");
            InitBtn();

            AddListener();
        }

        private void InitBtn()
        {
            _btnRecord = GetChildComponent<KButton>("Container_right/Container_upperBtn/Button_wenhao");
            _btnRecord.Visible = false;
            _btnFight = GetChildComponent<KButton>("Container_right/Button_tiaozhan");
            _btnSweep = GetChildComponent<KButton>("Container_right/Button_shilian");
        }

        public override void Dispose()
        {
            _instance = null;
            _wanderlandInstance = null;
            _wanderlandChapter = null;
            RemoveListener();
        }

        private void AddListener()
        {
            _btnFight.onClick.AddListener(OnFight);
            _btnSweep.onClick.AddListener(OnSweep);
        }

        private void RemoveListener()
        {
            _btnFight.onClick.RemoveListener(OnFight);
            _btnSweep.onClick.RemoveListener(OnSweep);
        }

        private void OnFight()
        {
            CopyLogicManager.Instance.RequestEnterCopy(ChapterType.WanderLand, RequestEnterCopy, _instance.__id);
        }

        private void RequestEnterCopy()
        {
            WanderlandUtil.ShowTips(_instance.__id, TipsCallback);
        }

        private void TipsCallback()
        {
            WanderLandManager.Instance.RequestFight(_instance.__id);
        }

        private void OnSweep()
        {
            WanderlandUtil.ShowTips(_instance.__id, SweepCallback);
        }

        private void SweepCallback()
        {
            WanderLandManager.Instance.RequestSweep(_wanderlandInstance.InstanceId);
        }

        public override object Data
        {
            set
            {
                WanderlandPreviewDataWrapper wrapper = value as WanderlandPreviewDataWrapper;
                _wanderlandChapter = wrapper.WanderLandChapter;
                _wanderlandInstance = wrapper.WanderLandInstance;
                _instance = _wanderlandInstance.Instance;
                Refresh();
            }
        }

        private void Refresh()
        {
            CopyData copyData = PlayerDataManager.Instance.CopyData;
            RefreshBtn();
            _copyHead.Refresh(_instance.__id, copyData.GetHistoryMaxStar(_instance.__id));
            _fightConditionList.Refresh(_instance.__id, copyData.GetDailyTimes(_instance.__id));
            _passConditionList.Refresh(_instance.__id);
            _previewReward.Refresh(_instance.__id);
        }

        private void RefreshBtn()
        {
            if (_wanderlandChapter.CanFight(_wanderlandInstance.Level))
            {
                _btnFight.Visible = true;
                _goTips.SetActive(false);
            }
            else
            {
                _btnFight.Visible = false;
                _goTips.SetActive(true);
            }
            _btnSweep.Visible = _wanderlandChapter.CanSweeep(_wanderlandInstance);
        }
    }
}
