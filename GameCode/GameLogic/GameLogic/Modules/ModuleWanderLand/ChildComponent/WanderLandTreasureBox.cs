﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/1 14:07:11
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Data;
using Common.Structs.ProtoBuf;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using UnityEngine;
using ModuleCopy;
using Common.ExtendComponent;

namespace ModuleWanderLand
{
    public class WanderLandTreasureBox : KContainer
    {
        private WanderLandChapter _wanderLandChapter;
        private WanderLandInstance _wanderLandInstance;
        private int _gotIconId;
        private int _ungetIconId;

        private IconContainer _imageGot;
        private IconContainer _imageUnget;
        private KButton _btn;
        private LStarList _starList;
        private KParticle _particle;

        protected override void Awake()
        {
            _btn = GetChildComponent<KButton>("Button_baoxiang");
            _starList = AddChildComponent<LStarList>("Container_xingxing");
            _imageGot = AddChildComponent<IconContainer>("Button_baoxiang/got");
            _imageUnget = AddChildComponent<IconContainer>("Button_baoxiang/unget");
            _particle = AddChildComponent<KParticle>("Button_baoxiang/fx_ui_24_2_baoxianglingqu");
            _particle.transform.localPosition = new Vector3(19f, 0f, 0f);
            _particle.Stop();

            _btn.onClick.AddListener(OnClick);
        }

        protected override void OnDestroy()
        {
            _btn.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.WanderLandBox, new CopyInstanceBoxDataWrapper(_wanderLandChapter, _wanderLandInstance));
        }

        public void Refresh(WanderLandChapter wanderLandChapter, WanderLandInstance wanderLandInstance, int gotIconId, int ungetIconId)
        {
            _wanderLandChapter = wanderLandChapter;
            _wanderLandInstance = wanderLandInstance;
            _gotIconId = gotIconId;
            _ungetIconId = ungetIconId;
            TweenTreasureBox.Stop(_btn.gameObject);
            SetGot(false);
            _particle.Stop();
            switch (wanderLandInstance.InstaceBoxState)
            {
                case BoxState.HAD_GOT:
                    SetGot(true);
                    break;
                case BoxState.CAN_GET:
                    _particle.Play(true);
                    TweenTreasureBox.Begin(_btn.gameObject);
                    break;
            }
            _starList.Visible = true;
            _starList.SetNum(PlayerDataManager.Instance.CopyData.GetHistoryMaxStar(wanderLandInstance.InstanceId));
        }

        private void SetGot(bool value)
        {
            if (value)
            {
                ShowImageGot();
                _imageUnget.Visible = false;
            }
            else
            {
                ShowImageUnget();
                _imageGot.Visible = false;
            }
        }

        private void ShowImageGot()
        {
            _imageGot.Visible = true;
            _imageGot.SetIcon(_gotIconId);
        }

        private void ShowImageUnget()
        {
            _imageUnget.Visible = true;
            _imageUnget.SetIcon(_ungetIconId);
        }

        public void HideStar()
        {
            _starList.SetNum(0);
        }
    }
}
