﻿using Common.ExtendComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleWanderLand
{
    public class WanderlandCategoryListItem : CategoryListItem
    {
        private GameObject _goLock;
        private GameObject _goCheckmarkLock;

        protected override void Awake()
        {
            base.Awake();
            _goLock = GetChild("Button_back/suo");
            _goCheckmarkLock = GetChild("Button_checkmark/suo");
        }

        public void Refresh(int chapterId)
        {
            bool isLock = PlayerAvatar.Player.level < chapters_helper.GetMinLevel(chapterId);
            _goLock.SetActive(isLock);
            _goCheckmarkLock.SetActive(isLock);
            SetPoint(PlayerDataManager.Instance.CopyData.ChapterHaveCanGetBox(chapterId) 
                || PlayerDataManager.Instance.WanderLandData.HaveInstanceTreasureBox(chapterId));
        }
    }
}
