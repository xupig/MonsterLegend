﻿using Common.Global;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleWanderLand
{
    public class WanderlandFightConditionList : KContainer
    {
        private StateText _textRecommendLevel;
        private StateText _textRecommendFight;
        private StateText _textEnterTimes;

        protected override void Awake()
        {
            _textRecommendLevel = GetChildComponent<StateText>("Container_command/Container_NR00/Label_txtXuyaodengji");
            _textRecommendFight = GetChildComponent<StateText>("Container_command/Container_NR01/Label_txtXuyaozhanli");
            _textEnterTimes = GetChildComponent<StateText>("Container_command/Container_NR02/Label_txtJinrucishu");
        }

        public void Refresh(int instanceId, int dailyTimes)
        {
            instance instance = instance_helper.GetInstanceCfg(instanceId);
            string minLevelStr = GetMinLevelStr(instance);
            _textRecommendLevel.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(1422), minLevelStr);
            string minFightForceStr = GetMinFightForceStr(instance);
            _textRecommendFight.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(1421), minFightForceStr);
            _textEnterTimes.CurrentText.text = string.Format(MogoLanguageUtil.GetContent(1431), MogoLanguageUtil.GetContent(40420));
        }

        private string GetMinLevelStr(instance instance)
        {
            int minLevel = instance_helper.GetMinLevel(instance);
            string minLevelStr = minLevel.ToString();
            if (PlayerAvatar.Player.level < minLevel)
            {
                minLevelStr = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, minLevelStr);
            }
            return minLevelStr;
        }

        private string GetMinFightForceStr(instance instance)
        {
            int recommendFightForce = instance_helper.GetRecommendFightForce(instance);
            string minFightForceStr = recommendFightForce.ToString();
            if (PlayerAvatar.Player.fight_force <= 0.9f * recommendFightForce)
            {
                minFightForceStr = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_RED, minFightForceStr);
            }
            else if (PlayerAvatar.Player.fight_force >= 1.1f * recommendFightForce)
            {
                minFightForceStr = ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, minFightForceStr);
            }
            return minFightForceStr;
        }
    }
}
