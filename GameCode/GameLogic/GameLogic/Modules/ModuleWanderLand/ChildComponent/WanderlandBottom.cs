﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleWanderLand
{
    public class WanderlandBottom : KContainer
    {
        public int SelectIndex;
        private WanderLandChapter _chapter;

        private KButton _btnFight;
        private KButton _btnSmeltMeterial;
        private KParticle _particleShop;
        private KButton _btnReset;
        private StateText _textReset;
        private StateText _textTips;

        private WanderLandData _wanderlandData
        {
            get
            {
                return PlayerDataManager.Instance.WanderLandData;
            }
        }

        protected override void Awake()
        {
            _btnFight = GetChildComponent<KButton>("Button_tiaozhan");
            _btnReset = GetChildComponent<KButton>("Button_chongzhiguanka");
            _btnSmeltMeterial = GetChildComponent<KButton>("Button_shilianshangcheng");
            _particleShop = AddChildComponent<KParticle>("Button_shilianshangcheng/fx_ui_3_2_lingqu");
            _particleShop.Stop();
            _textReset = GetChildComponent<StateText>("Label_txtDangqianguanka");
            _textTips = GetChildComponent<StateText>("Label_txtNotic");

            AddListener();
        }

        protected override void OnDestroy()
        {
            RemoveListener();
        }

        private void AddListener()
        {
            _btnFight.onClick.AddListener(OnFight);
            _btnReset.onClick.AddListener(OnReset);
            _btnSmeltMeterial.onClick.AddListener(OnOpenSmeltMeterial);
        }

        private void RemoveListener()
        {
            _btnFight.onClick.RemoveListener(OnFight);
            _btnReset.onClick.RemoveListener(OnReset);
            _btnSmeltMeterial.onClick.RemoveListener(OnOpenSmeltMeterial);
        }

        protected override void OnEnable()
        {
            EventDispatcher.AddEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshShopBtn);
        }

        protected override void OnDisable()
        {
            EventDispatcher.RemoveEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshShopBtn);
        }

        private void OnFight()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.WanderLandDetail, new WanderlandDetailDataWrapper(_chapter, SelectIndex));
        }

        private void OnOpenSmeltMeterial()
        {
            PanelIdEnum.EquipSmelter.Show();
        }

        private void OnReset()
        {
            int leftTimes = int.Parse(vip_helper.GetWanderlandMaxResetTimes(PlayerAvatar.Player.vip_level)) - (int)_wanderlandData.HaveResetTimes;
            if (leftTimes == 0)
            {
                ConfirmAction();
            }
            else
            {
                BaseItemData baseItemData  = price_list_helper.GetCost(PriceListId.wanderlandReset, (int)_wanderlandData.HaveResetTimes + 1);
                MogoTipsUtils.ShowTips(baseItemData, MogoLanguageUtil.GetContent(40412), ConfirmAction);
            }
        }

        private void ConfirmAction()
        {
            WanderLandManager.Instance.RequestResetTimes();
        }

        public void SetData(WanderLandChapter chapter)
        {
            _chapter = chapter;
            RefreshFightBtn();
            RefreshResetBtn();
            RefreshShopBtn();
            RefreshTips();
        }

        private void RefreshFightBtn()
        {
            if (_wanderlandData.ChapterCanFight(_chapter))
            {
                _btnFight.Visible = true;
            }
            else
            {
                _btnFight.Visible = false;
            }
        }

        private void RefreshResetBtn()
        {
            if (CanReset())
            {
                _textReset.CurrentText.text =
                 MogoLanguageUtil.GetContent(40411, int.Parse(vip_helper.GetWanderlandMaxResetTimes(PlayerAvatar.Player.vip_level)) - _wanderlandData.HaveResetTimes);
                _btnReset.Visible = true;
            }
            else
            {
                _textReset.Clear();
                _btnReset.Visible = false;
            }
        }

        private bool CanReset()
        {
            if (_wanderlandData.HadFighted())
            {
                if (_wanderlandData.ChapterPage == _chapter.Page)
                {
                    return true;
                }
            }
            return false;
        }

        private void RefreshShopBtn()
        {
            if (PlayerDataManager.Instance.EquipPointManager.CanSmelt())
            {
                _particleShop.Play(true);
            }
            else
            {
                _particleShop.Stop();
            }
        }

        private void RefreshTips()
        {
            if (CanShowTips())
            {
                int level = chapters_helper.GetMinLevel(_wanderlandData.GetChapterByPage(_wanderlandData.ChapterPage).ChapterId);
                _textTips.CurrentText.text = MogoLanguageUtil.GetContent(40414, level);
            }
            else
            {
                _textTips.Clear();
            }
        }

        private bool CanShowTips()
        {
            if (_wanderlandData.HadFighted() && _chapter.Page != _wanderlandData.ChapterPage)
            {
                if (!_wanderlandData.FightFirstChapter())
                {
                    return true;
                }
            }
            return false;
        }
    }
}
