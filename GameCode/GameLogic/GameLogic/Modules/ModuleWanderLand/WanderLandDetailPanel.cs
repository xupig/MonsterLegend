﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/3/31 20:25:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System.Collections.Generic;
using Common.Base;
using Common.Data;
using Common.Events;
using Game.UI.UIComponent;
using MogoEngine.Events;

namespace ModuleWanderLand
{
    public class WanderLandDetailPanel : BasePanel
    {
        private WanderLandChapter _wanderLandChapter;

        private KScrollPage _scrollPage;
        private KPageableList _pageableList;

        protected override void Awake()
        {
            CloseBtn = GetChildComponent<KButton>("ScrollPage_shilianfuben/close");
            _scrollPage = GetChildComponent<KScrollPage>("ScrollPage_shilianfuben");
            _pageableList = GetChildComponent<KPageableList>("ScrollPage_shilianfuben/mask/content");

            InitScrollPage();
        }

        private void InitScrollPage()
        {
            _pageableList.SetPadding(0, 1, 0, 1);
            _pageableList.SetGap(0, 2);
            _pageableList.SetDirection(KList.KListDirection.LeftToRight, 1, 1);
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.WanderLandDetail; }
        }

        public override void OnShow(object data)
        {
            WanderlandDetailDataWrapper wrapper = data as WanderlandDetailDataWrapper;
            _wanderLandChapter = wrapper.Chapter;
            Refresh(wrapper.SelectLevel);
            AddListener();
            EventDispatcher.TriggerEvent(WanderLandEvents.PanelVisibleChange);
        }

        public override void OnClose()
        {
            RemoveListener();
            EventDispatcher.TriggerEvent(WanderLandEvents.PanelVisibleChange);
        }

        private void AddListener()
        {
            _scrollPage.onCurrentPageChanged.AddListener(OnCurrentPageChange);
            EventDispatcher.AddEventListener(WanderLandEvents.SweepSuccess, OnSweepSuccess);
        }

        private void RemoveListener()
        {
            _scrollPage.onCurrentPageChanged.RemoveListener(OnCurrentPageChange);
            EventDispatcher.RemoveEventListener(WanderLandEvents.SweepSuccess, OnSweepSuccess);
        }

        private void OnSweepSuccess()
        {
            Refresh(_scrollPage.CurrentPage);
        }

        private void OnCurrentPageChange(KScrollPage scrollpage, int index)
        {
            EventDispatcher.TriggerEvent<int>(WanderLandEvents.DetailViewSlide, index);
        }

        private void Refresh(int selectLevel)
        {
            List<WanderlandPreviewDataWrapper> wrapperList = GetWrapperList();
            _pageableList.SetDataList(selectLevel, typeof(WanderLandDetailGrid), wrapperList, 1);
            _scrollPage.TotalPage = wrapperList.Count;
            _scrollPage.CurrentPage = selectLevel;
        }

        private List<WanderlandPreviewDataWrapper> GetWrapperList()
        {
            List<WanderlandPreviewDataWrapper> wrapperList = new List<WanderlandPreviewDataWrapper>();
            List<WanderLandInstance> instanceList = _wanderLandChapter.InstanceList;
            for (int i = 0; i < instanceList.Count; i++)
            {
                WanderlandPreviewDataWrapper wrapper = new WanderlandPreviewDataWrapper(_wanderLandChapter, instanceList[i]);
                wrapperList.Add(wrapper);
            }
            return wrapperList;
        }
    }
}
