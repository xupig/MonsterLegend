﻿using Common.Data;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTimeAltar
{
    public class TimeAltarItemView:KContainer
    {
        private KList _timeAltarItemList;
        private StateText _txtNoneRedemptionItem;

        protected override void Awake()
        {
            base.Awake();
            _timeAltarItemList = GetChildComponent<KList>("ScrollView_shuhui/mask/content");
            _txtNoneRedemptionItem = GetChildComponent<StateText>("Label_txtWeihuode");

            _timeAltarItemList.SetDirection(KList.KListDirection.LeftToRight, 1);

        }

        public void ShowRedemptionItem(int functionId)
        {
            Dictionary<int, TimeAltarFunctionData> timeAltarData = PlayerDataManager.Instance.TimeAltarData.TimeAltarDataDic;
            _timeAltarItemList.RemoveAll();
            if (timeAltarData.ContainsKey(functionId) == true && timeAltarData[functionId].TimeAltarItemDataList.Count != 0)
            {
                _txtNoneRedemptionItem.Visible = false;
                _timeAltarItemList.SetDataList<RedemptionItem>(timeAltarData[functionId].TimeAltarItemDataList);
            }
            else
            {
                _txtNoneRedemptionItem.Visible = true;
            }
        }


    }
}
