﻿
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.ServerConfig;
using Game.UI.UIComponent;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTimeAltar
{
    public class TimeAltarBottomView : KContainer
    {
        private MoneyItem[] _materialList;
        private StateText _txtActiveLabel;

        private int _functionId = -1;

        protected override void Awake()
        {
            _materialList = new MoneyItem[2];
            _txtActiveLabel = GetChildComponent<StateText>("Label_txtWeihuode");
            _txtActiveLabel.Visible = false;
            InitMaterialItem();

        }

        private void InitMaterialItem()
        {
            _materialList[0] = AddChildComponent<MoneyItem>("Container_buyGold1");
            _materialList[1] = AddChildComponent<MoneyItem>("Container_buyGold2");

        }

        protected override void OnEnable()
        {
            HideMaterialItem();
            AddEventsListener();
        }

        protected override void OnDisable()
        {
            RemoveEventListener();
        }

        public void ShowMaterialByFunctionId(int functionId)
        {
            _functionId = -1;
            TimeAltarData timeAltarData = PlayerDataManager.Instance.TimeAltarData;
            if (timeAltarData.TimeAltarDataDic.ContainsKey(functionId) == true)
            {
                _functionId = functionId;
            }
            ShowMineMaterial();
        }

        private void ShowMineMaterial()
        {
            HideMaterialItem();

            if(_functionId == -1) {return;}


            TimeAltarData timeAltarData = PlayerDataManager.Instance.TimeAltarData;
            List<TimeAltarItemData> timeAltarItemDataList = timeAltarData.TimeAltarDataDic[_functionId].TimeAltarItemDataList;

            int i = 0;
            if (timeAltarItemDataList.Count != 0)
            {
                TimeAltarItemData redemptionExample = timeAltarItemDataList[0];
                int count = redemptionExample.CostList.Count;
                if (redemptionExample.ActivePoint != 0)
                {
                    _txtActiveLabel.Visible = true;
                    ItemData itemData = new ItemData(public_config.MONEY_TYPE_ACTIVE_POINT);
                    itemData.StackCount = (int)PlayerAvatar.Player.active_point;
                    ShowMaterialItem(itemData, i);
                    i++;
                }
                else
                {
                    _txtActiveLabel.Visible = false;
                }
                for (int j = 0; j < redemptionExample.CostList.Count; j++ )
                {
                    BaseItemData ItemData = redemptionExample.CostList[j];
                    ItemData itemData = new ItemData(ItemData.Id);
                    if (ItemData.Id == public_config.MONEY_TYPE_GOLD)
                    {
                        itemData.StackCount = PlayerAvatar.Player.money_gold;
                    }
                    else
                    {
                        itemData.StackCount = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(ItemData.Id);
                    }
                    ShowMaterialItem(itemData, i);
                    i++;
                }
            }

        }

        private void ShowMaterialItem(ItemData itemData, int index)
        {
            _materialList[index].Visible = true;
            _materialList[index].SetMoneyType(itemData.Id);
        }

        public void HideMaterialItem()
        {
            for (int k = 0; k < 2; k++)
            {
                _materialList[k].gameObject.SetActive(false);
            }
        }

        private void AddEventsListener()
        {
            EventDispatcher.AddEventListener<BagType, uint>(BagEvents.Update, Refresh);
        }

        private void RemoveEventListener()
        {
            EventDispatcher.RemoveEventListener<BagType, uint>(BagEvents.Update, Refresh);
        }

        private void Refresh(BagType bagType, uint index)
        {
            ShowMineMaterial();
        }
    }
}
