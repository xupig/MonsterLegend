﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTimeAltar
{
    public class TimeAltarView : KContainer
    {
        private CategoryList _categoryToggleGroup;
        private TimeAltarItemView _timeAltarItemView;
        private TimeAltarBottomView _bottomView;
        private KButton _ruleButton;
        private StateText _txtTitle;

        private Dictionary<int, int> _categoryIndexToFunctionIdDic;

        protected override void Awake()
        {
            base.Awake();
            _categoryIndexToFunctionIdDic = new Dictionary<int, int>();
            _categoryToggleGroup = GetChildComponent<CategoryList>("List_categoryList");
            _timeAltarItemView = AddChildComponent<TimeAltarItemView>("Container_wupinqingdan");
            _bottomView = AddChildComponent<TimeAltarBottomView>("Container_bottommaterial");
            _txtTitle = GetChildComponent<StateText>("Container_biaoti/Label_txtBiaoti");
            _ruleButton = GetChildComponent<KButton>("Container_biaoti/Button_Gantang");
            _txtTitle.CurrentText.text = MogoLanguageUtil.GetContent(74028);
        }

        public void OnShow(object data = null)
        {
            if (this.Visible != true)
            {
                this.Visible = true;
                AddEventListener();
                RefreshView();
                RefreshCategoryGreenPoint();
            }
        }

        public void OnClose()
        {
            if (this.Visible == true)
            {
                this.Visible = false;
                RemoveEventListener();
            }
        }

        private void RefreshView()
        {
            RefreshFunctionCategory();
            Dictionary<int, TimeAltarFunctionData> timeAltarData = PlayerDataManager.Instance.TimeAltarData.TimeAltarDataDic;

            if (_categoryIndexToFunctionIdDic.Count != 0)
            {
                foreach (int categoryIndex in _categoryIndexToFunctionIdDic.Keys)
                {
                    if (timeAltarData.ContainsKey(_categoryIndexToFunctionIdDic[categoryIndex]) == true && timeAltarData[_categoryIndexToFunctionIdDic[categoryIndex]].TimeAltarItemDataList.Count != 0)
                    {
                        _timeAltarItemView.ShowRedemptionItem(_categoryIndexToFunctionIdDic[categoryIndex]);
                        _bottomView.ShowMaterialByFunctionId(_categoryIndexToFunctionIdDic[categoryIndex]);
                        _categoryToggleGroup.SelectedIndex = categoryIndex;
                        return;
                    }
                }

                _timeAltarItemView.ShowRedemptionItem(_categoryIndexToFunctionIdDic[0]);
                _bottomView.ShowMaterialByFunctionId(_categoryIndexToFunctionIdDic[0]);
                _categoryToggleGroup.SelectedIndex = 0;
            }
            else
            {
                //清空数据
                _timeAltarItemView.ShowRedemptionItem(0);
                _bottomView.ShowMaterialByFunctionId(0);
            }
        }

        private void AddEventListener()
        {
            _categoryToggleGroup.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            _ruleButton.onClick.AddListener(ShowRule);
            EventDispatcher.AddEventListener(TimeAltarEvents.InitTimeAltar, RefreshView);
            EventDispatcher.AddEventListener<int>(TimeAltarEvents.UpdateTimeAltar, RefreshRedemptionList);
        }

        private void RemoveEventListener()
        {
            _categoryToggleGroup.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            _ruleButton.onClick.RemoveListener(ShowRule);
            EventDispatcher.RemoveEventListener(TimeAltarEvents.InitTimeAltar, RefreshView); 
            EventDispatcher.RemoveEventListener<int>(TimeAltarEvents.UpdateTimeAltar, RefreshRedemptionList);
        }

        private void RefreshRedemptionList(int functionId)
        {
            if(_categoryIndexToFunctionIdDic[_categoryToggleGroup.SelectedIndex] == functionId)
            {
                Dictionary<int, TimeAltarFunctionData> timeAltarData = PlayerDataManager.Instance.TimeAltarData.TimeAltarDataDic;
                if (timeAltarData.ContainsKey(functionId) == false)
                {
                    RefreshView(); //全部刷新
                }
                else
                {
                    _timeAltarItemView.ShowRedemptionItem(functionId);
                    _bottomView.ShowMaterialByFunctionId(functionId);
                }
            }
            RefreshCategoryGreenPoint();
        }

        private void OnSelectedIndexChanged(CategoryList categoryList, int index)
        {
            _timeAltarItemView.ShowRedemptionItem(_categoryIndexToFunctionIdDic[index]);
            _bottomView.ShowMaterialByFunctionId(_categoryIndexToFunctionIdDic[index]);
        }

        private void RefreshFunctionCategory()
        {
            List<int> timeAltarFuntionIdList = time_alter_helper.GetFunctionIdList();
            List<CategoryItemData> listToggleData = new List<CategoryItemData>();
            _categoryIndexToFunctionIdDic.Clear();
            for (int i = 0; i < timeAltarFuntionIdList.Count; i++)
            {
                int functionNameId = function_helper.GetFunction(timeAltarFuntionIdList[i]).__name;
                listToggleData.Add(CategoryItemData.GetCategoryItemData(MogoLanguageUtil.GetContent(functionNameId)));
                _categoryIndexToFunctionIdDic.Add(i, timeAltarFuntionIdList[i]);
            }
            _categoryToggleGroup.RemoveAll();
            if (listToggleData.Count != 0)
            {
                _categoryToggleGroup.SetDataList<CategoryListItem>(listToggleData);
            }
        }

        private void RefreshCategoryGreenPoint()
        {
            foreach (int categoryIndex in _categoryIndexToFunctionIdDic.Keys)
            {
                int functionId =_categoryIndexToFunctionIdDic[categoryIndex];

                Dictionary<int, TimeAltarFunctionData> timeAltarData = PlayerDataManager.Instance.TimeAltarData.TimeAltarDataDic;
                if (timeAltarData.ContainsKey(functionId) == true && timeAltarData[functionId].TimeAltarItemDataList.Count != 0)
                {
                    (_categoryToggleGroup[categoryIndex] as CategoryListItem).SetPoint(true);
                }
                else
                {
                    (_categoryToggleGroup[categoryIndex] as CategoryListItem).SetPoint(false);
                }
            }
        }

        private void ShowRule()
        {
            RuleTipsDataWrapper wrapper = new RuleTipsDataWrapper(MogoLanguageUtil.GetContent(102914), _ruleButton.GetComponent<RectTransform>(), RuleTipsDirection.Bottom);
            UIManager.Instance.ShowPanel(PanelIdEnum.RuleTips, wrapper);
        }

    }
}
