﻿using Common.Base;
using Common.Data;
using Common.ExtendComponent;
using Common.ServerConfig;
using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleTimeAltar
{
    public class RedemptionItem:KList.KListItemBase
    {
        private const int MAX_NEED_MATERIAL = 2;
        private string _functionNameContent;

        private KContainer _iconContainer;
        private KContainer[] _needMaterial;
        private StateText[] _txtMaterialNum;
        private StateImage[] _imgActivePoint;
        private StateText _txtFunctionName;
        private StateText _txtRedemptionNum;
        private KButton _redemptionButton;
        private KList _rewardKList;


        private TimeAltarItemData _data;

        protected override void Awake()
        {
            _iconContainer = GetChildComponent<KContainer>("Container_icon");
            _needMaterial = new KContainer[MAX_NEED_MATERIAL];
            _txtMaterialNum = new StateText[MAX_NEED_MATERIAL];
            _imgActivePoint = new StateImage[MAX_NEED_MATERIAL];
            for (int i = 0; i < MAX_NEED_MATERIAL; i++)
            {
                _needMaterial[i] = GetChildComponent<KContainer>(string.Format("Container_xiaohao/Container_xiaohao{0}", i+1));
                _imgActivePoint[i] = _needMaterial[i].GetChildComponent<StateImage>("Container_icon/Image_huoyuedu");
                _imgActivePoint[i].Visible = false;
                _txtMaterialNum[i] = GetChildComponent<StateText>(string.Format("Container_xiaohao/Container_xiaohao{0}/Label_txtShuliang", i + 1));
            }
            _txtFunctionName = GetChildComponent<StateText>("Label_txtCengshu");
            _functionNameContent = _txtFunctionName.CurrentText.text;
            _txtRedemptionNum = GetChildComponent<StateText>("Label_txtXiaohao");
            _redemptionButton = GetChildComponent<KButton>("Button_shuhui");
            _rewardKList = GetChildComponent<KList>("List_jiangli");
            _rewardKList.SetDirection(KList.KListDirection.LeftToRight, int.MaxValue);

            _redemptionButton.onClick.AddListener(OnClickRedemption);
        }

        public override void Dispose()
        {
            _redemptionButton.onClick.RemoveListener(OnClickRedemption);
            _data = null;
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as TimeAltarItemData;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            MogoAtlasUtils.AddIcon(_iconContainer.gameObject, _data.Icon);
            List<BaseItemData> constList = _data.CostList;
            int needItemCount = 0;
            if (_data.ActivePoint != 0)
            {
                MogoAtlasUtils.AddIcon(_needMaterial[needItemCount].GetChild(1), item_helper.GetIcon(14));
                _txtMaterialNum[needItemCount].CurrentText.text = string.Format("x{0}", _data.ActivePoint);
                needItemCount++;

            }
            for(int i = 0; i < constList.Count; i++,needItemCount++)
            {
                MogoAtlasUtils.AddIcon(_needMaterial[needItemCount].GetChild(1), item_helper.GetIcon(constList[i].Id));
                _txtMaterialNum[needItemCount].CurrentText.text = string.Format("x{0}",constList[i].StackCount);
            }
            for (int i = needItemCount; i < MAX_NEED_MATERIAL; i++)
            {
                MogoAtlasUtils.RemoveSprite(_needMaterial[i].GetChild(1));
                _txtMaterialNum[i].Clear();
            }

            _rewardKList.SetDataList<RewardGrid>(_data.RewardList);
            _txtFunctionName.CurrentText.text = _data.Name;
            ///当前可赎回{0}次
            _txtRedemptionNum.CurrentText.text = MogoLanguageUtil.GetContent(102919, _data.Count);
        }

        private void OnClickRedemption()
        {
            ///TODO点击赎回按钮的操作
            for (int i = 0; i < _data.CostList.Count; i++)
            {
                if (_data.CostList[i].Id == public_config.MONEY_TYPE_ACTIVE_POINT && _data.CostList[i].StackCount > PlayerAvatar.Player.active_point)
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.ItemChannel, public_config.MONEY_TYPE_ACTIVE_POINT);
                }
            }

            for (int i = 0; i < _data.CostList.Count; i++)
            {
                if (PlayerAvatar.Player.CheckCostLimit(_data.CostList[i].Id, _data.CostList[i].StackCount, false) == -1)
                {
                    string content = MogoLanguageUtil.GetContent(102918, item_helper.GetName(public_config.MONEY_TYPE_DIAMOND), _data.CostList[i].StackCount - PlayerAvatar.Player.money_bind_diamond);
                    MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, OnConfirm);
                    return;
                }
            }
            TimeAltarManager.Instance.RequestRedemption(_data.FunctionId, _data.TimeAltarLevelStruct.date, _data.TimeAltarLevelStruct.count);

        }

        private void OnConfirm()
        {
            TimeAltarManager.Instance.RequestRedemption(_data.FunctionId, _data.TimeAltarLevelStruct.date, _data.TimeAltarLevelStruct.count);
        }
    }
}
