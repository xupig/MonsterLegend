﻿using Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleTimeAltar
{
    public class TimeAltarModule : BaseModule
    {

        public TimeAltarModule()
            : base()
        {
        }

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {
            AddPanel(PanelIdEnum.TimeAltar, "Container_TimeAltarPanel", MogoUILayer.LayerUIPanel, "ModuleTimeAltar.TimeAltarPanel", BlurUnderlay.Have, HideMainUI.Hide);
        }
    }
}
