﻿using Common.Base;
using Common.Data;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquipFacade
{
    public class FashionTip : KContainer
    {
        private KContainer _contentContainer;
        private StateText _nameTxt;
        private KContainer _fightForceContainer;
        private StateText _fightForceTxt;
        private string _fightForceTemplate;
        private KList _attriList;
        private KContainer _rideEffectContainer;
        private StateText _rideEffectStateText;
        private KContainer _descContainer;
        private StateText _descTxt;
        private KContainer _expireContainer;
        private StateText _expireTxt;
        private KDummyButton _dummyBtn;
        private List<Locater> _locaters;

        protected override void Awake()
        {
            _contentContainer = GetChildComponent<KContainer>("Container_content");
            _nameTxt = _contentContainer.GetChildComponent<StateText>("Label_txtmingcheng");
            _fightForceContainer = _contentContainer.GetChildComponent<KContainer>("Container_zhandouli");
            _fightForceTxt = _fightForceContainer.GetChildComponent<StateText>("Label_txtZhanli");
            _fightForceTemplate = _fightForceTxt.CurrentText.text;
            _rideEffectContainer = _contentContainer.GetChildComponent<KContainer>("Container_qichengxiaoguo");
            _rideEffectStateText = _rideEffectContainer.GetChildComponent<StateText>("Label_Label");
            _attriList = _contentContainer.GetChildComponent<KList>("List_attributes");
            _attriList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _descContainer = _contentContainer.GetChildComponent<KContainer>("Container_miaoshu");
            _descTxt = _descContainer.GetChildComponent<StateText>("Label_txtNeirong");
            _expireContainer = _contentContainer.GetChildComponent<KContainer>("Container_shengyushijian");
            _expireTxt = _expireContainer.GetChildComponent<StateText>("Label_txtShengming");
            _dummyBtn = AddChildComponent<KDummyButton>("ScaleImage_sharedZhezhao");
            _dummyBtn.onClick.AddListener(HideTip);
            InitLocaters();
        }

        private void InitLocaters()
        {
            _locaters = new List<Locater>();
            _locaters.Add(_fightForceContainer.gameObject.AddComponent<Locater>());
            _locaters.Add(_attriList.gameObject.AddComponent<Locater>());
            _locaters.Add(_rideEffectContainer.gameObject.AddComponent<Locater>());
            _locaters.Add(_descContainer.gameObject.AddComponent<Locater>());
            _locaters.Add(_expireContainer.gameObject.AddComponent<Locater>());
        }

        public void Refresh(FashionItemData fashionItemData, Vector2 position)
        {
            Vector2 localPosition;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(gameObject.GetComponent<RectTransform>(), position, UIManager.Instance.UICamera, out localPosition);
            _contentContainer.GetComponent<RectTransform>().anchoredPosition = localPosition + new Vector2(-260, 0);
            Visible = true;
            _rideEffectContainer.Visible = false;
            RefreshName(fashionItemData.Name);
            RefreshFightForce(fashionItemData.FightForce);
            RefreshAttributes(fashionItemData.EffectId);
            RefreshDescription(fashionItemData.Description);
            RefreshExpireTime(fashionItemData.ValidDescStringInTips);
            DoContainerLayout();
        }

        public void Refresh(MountItemData mountItemData, Vector2 position)
        {
            Vector2 localPosition;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(gameObject.GetComponent<RectTransform>(), position, UIManager.Instance.UICamera, out localPosition);
            _contentContainer.GetComponent<RectTransform>().anchoredPosition = localPosition + new Vector2(-260, 0);
            Visible = true;
            _rideEffectContainer.Visible = true;
            RefreshName(mountItemData.Name);
            RefreshFightForce(mountItemData.FightForce);
            RefreshAttributes(mountItemData.EffectId);
            RefreshRideEffect(mountItemData.RideEffect);
            RefreshDescription(mountItemData.Description);
            RefreshExpireTime(mountItemData.ValidDescStringInTips);
            DoContainerLayout();
        }

        private void RefreshRideEffect(int rideEffectId)
        {
            _rideEffectStateText.CurrentText.text = string.Format("{0}", buff_helper.GetName(rideEffectId));
        }

        private void RefreshFightForce(int fightForce)
        {
            _fightForceTxt.CurrentText.text = string.Format(_fightForceTemplate, fightForce);
        }

        private void DoContainerLayout()
        {
            Vector2 startPosition = _locaters[0].Position;
            for (int i = 0; i < _locaters.Count; i++)
            {
                if (_locaters[i].gameObject.activeSelf == true)
                {
                    _locaters[i].Position = startPosition;
                    startPosition.y -= _locaters[i].Height;
                }
            }
        }

        private void RefreshExpireTime(string value)
        {
            _expireTxt.CurrentText.text = value;
        }

        private void RefreshDescription(string description)
        {
            _descTxt.CurrentText.text = description;
        }

        private void RefreshAttributes(int effectId)
        {
            _attriList.RemoveAll();
            _attriList.SetDataList<FashionAttributeItem>(attri_effect_helper.GetAttributeDescList(effectId));
            _attriList.DoLayout();
        }

        private void RefreshName(string name)
        {
            _nameTxt.CurrentText.text = name;
        }

        private void HideTip()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.EquipFacadeTip);
        }
    }
}
