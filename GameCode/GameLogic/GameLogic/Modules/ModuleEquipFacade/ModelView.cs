﻿using ACTSystem;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameMain.Entities;
using UnityEngine;
using MogoEngine.Events;
using Common.Events;
using GameData;
using GameMain.GlobalManager;
using Common.Data;
using Common.Structs;
using ShaderUtils;

namespace ModuleEquipFacade
{
    public enum FacadeType
    {
        Effect = 1,
        Fashion = 2,
        Mount = 3,
    }

    public class ModelView : KContainer
    {
        private KButton _resetBtn;
        private KContainer _showTxtContainer;
        private FashionAttributeContainer _fashionAttributeContainer;
        private ModelComponent _avatarModel;
        private ACTActor _actor;
        private ACTActor _mountActor;
        private StateText _warnTxt;
        private int currentGroupId;
        private int currentEffectId;
        private int currentFashionWeaponId;
        private int currentFashionClothId;
        private int currentMountId;
        public static FacadeType mode = FacadeType.Effect;

        protected override void Awake()
        {
            _resetBtn = GetChildComponent<KButton>("Button_shuaxin");
            _showTxtContainer = GetChildComponent<KContainer>("Container_tip");
            _fashionAttributeContainer = AddChildComponent<FashionAttributeContainer>("Container_xinxi");
            _avatarModel = AddChildComponent<ModelComponent>("Container_wingModel");
            _warnTxt = GetChildComponent<StateText>("Label_txtTishi");
            HideTheWarnMessage();
            Vector3 position = _avatarModel.transform.localPosition;
            _avatarModel.transform.localPosition = new Vector3(position.x, position.y, position.z + 1000);
            AddEventListener();
        }

        private void AddEventListener()
        {
            _resetBtn.onClick.AddListener(ResetModel);
            EventDispatcher.AddEventListener<int, int>(FashionEvents.WEAR_EQUIP_EFFECT, ChangeEquipEffect);
            EventDispatcher.AddEventListener<int, int>(FashionEvents.TAKE_OFF_EQUIP_EFFECT, TakeOffEquipEffect);
        }

        private void RemoveEventListener()
        {
            _resetBtn.onClick.RemoveListener(ResetModel);
            EventDispatcher.RemoveEventListener<int, int>(FashionEvents.WEAR_EQUIP_EFFECT, ChangeEquipEffect);
            EventDispatcher.RemoveEventListener<int, int>(FashionEvents.TAKE_OFF_EQUIP_EFFECT, TakeOffEquipEffect);
        }

        private void TakeOffEquipEffect(int groupId, int effectId)
        {
            currentGroupId = 0;
            currentEffectId = 0;
            switch (groupId)
            {
                case (int)EffectGroup.WeaponParticle:
                    TakeOffWeaponEffect();
                    break;
                case (int)EffectGroup.WeaponFlow:
                    TakeOffWeaponEffect();
                    break;
                case (int)EffectGroup.Armor:
                    TakeOffClothEffect();
                    break;
            }
        }

        private void TakeOffClothEffect()
        {
            _actor.equipController.equipCloth.ChangeParticle(0);
            _actor.equipController.equipCloth.ChangeFlow(0);
        }

        private void TakeOffWeaponEffect()
        {
            _actor.equipController.equipWeapon.ChangeFlow(0);
            _actor.equipController.equipWeapon.ChangeParticle(0);
        }

        public void ChangeEquipEffect(int groupId, int equipEffectId)
        {
            _warnTxt.Visible = true;
            if (_actor == null)
            {
                LoadPlayerModel(true);
                return;
            }
            currentGroupId = groupId;
            currentEffectId = equipEffectId;
            switch (groupId)
            {
                case (int)EffectGroup.WeaponParticle:
                    PutOnNewWeaponEffect(equipEffectId);
                    break;
                case (int)EffectGroup.WeaponFlow:
                    PutOnNewWeaponEffect(equipEffectId);
                    break;
                case (int)EffectGroup.Armor:
                    PutOnNewClothEffect(equipEffectId);
                    break;
            }
        }

        public void PutOnFashion(int fashionId, int clothId, int weaponId)
        {
            _warnTxt.Visible = false;
            currentFashionClothId = clothId;
            currentFashionWeaponId = weaponId;
            if (clothId != 0)
            {
                _actor.equipController.equipCloth.onLoadEquipFinished = OnClothLoaded;
                _actor.equipController.equipCloth.PutOn(clothId);
            }
            else
            {
                int defaultClothId = _actor.equipController.equipCloth.defaultEquipID;
                _actor.equipController.equipCloth.onLoadEquipFinished = OnClothLoaded;
                _actor.equipController.equipCloth.PutOn(defaultClothId);
            }
            if (weaponId != 0)
            {
                _actor.equipController.equipWeapon.onLoadEquipFinished = OnWeaponLoaded;
                _actor.equipController.equipWeapon.PutOn(weaponId);
            }
            else
            {
                int defaultEquipId = _actor.equipController.equipWeapon.defaultEquipID;
                _actor.equipController.equipWeapon.onLoadEquipFinished = OnWeaponLoaded;
                _actor.equipController.equipWeapon.PutOn(defaultEquipId);
            }
        }

        private void OnClothLoaded()
        {
            SortOrderedRenderAgent.RebuildAll();
        }

        private void OnWeaponLoaded()
        {
            SortOrderedRenderAgent.RebuildAll();
        }

        private void PutOnNewWeaponEffect(int effectId)
        {
            var equipClothInfo = PlayerDataManager.Instance.BagData.GetEquipedItem(Common.Structs.Enum.EquipType.weapon);
            var equipInfo = PlayerDataManager.Instance.BagData.GetEquipedItem(Common.Structs.Enum.EquipType.breastPlate);
            int curWeapon = equipClothInfo != null ? item_helper.GetEquipModelID(equipClothInfo.Id) : 0;
            int equipCloth = equipInfo != null ? item_helper.GetEquipModelID(equipInfo.Id) : 0;
            if (equipCloth != _actor.equipController.equipCloth.curEquipData.ID)
            {
                _actor.equipController.equipCloth.PutOn(equipCloth);
            }
            if (curWeapon != _actor.equipController.equipWeapon.curEquipData.ID)
            {
                int flowId = equip_effect_helper.GetFlowId(effectId, curWeapon);
                int particleId = equip_effect_helper.GetParticleId(effectId, curWeapon);
                _actor.equipController.equipWeapon.PutOn(curWeapon, particleId, flowId);
            }
            else
            {
                int flowId = equip_effect_helper.GetFlowId(effectId, _actor.equipController.equipWeapon.curEquipData.ID);
                int particleId = equip_effect_helper.GetParticleId(effectId, _actor.equipController.equipWeapon.curEquipData.ID);
                if (particleId != 0)
                {
                    _actor.equipController.equipWeapon.ChangeParticle(particleId);
                }
                if (flowId != 0)
                {
                    _actor.equipController.equipWeapon.ChangeFlow(flowId);
                }
            }
        }

        private void OnParticleLoaded()
        {
            ModelComponentUtil.ChangeUIModelParam(_actor.gameObject);
        }

        private void PutOnNewClothEffect(int effectId)
        {
            var equipClothInfo = PlayerDataManager.Instance.BagData.GetEquipedItem(Common.Structs.Enum.EquipType.weapon);
            var equipInfo = PlayerDataManager.Instance.BagData.GetEquipedItem(Common.Structs.Enum.EquipType.breastPlate);
            int curWeapon = equipClothInfo != null ? item_helper.GetEquipModelID(equipClothInfo.Id) : 0;
            int equipCloth = equipInfo != null ? item_helper.GetEquipModelID(equipInfo.Id) : 0;
            if (curWeapon != _actor.equipController.equipWeapon.curEquipData.ID)
            {
                _actor.equipController.equipWeapon.PutOn(curWeapon);
            }
            if (equipCloth != _actor.equipController.equipCloth.curEquipData.ID)
            {
                int flowId = equip_effect_helper.GetFlowId(effectId, equipCloth);
                int particleId = equip_effect_helper.GetParticleId(effectId, equipCloth);
                _actor.equipController.equipCloth.PutOn(equipCloth, particleId, flowId);
            }
            else
            {
                int flowId = equip_effect_helper.GetFlowId(effectId, _actor.equipController.equipCloth.curEquipData.ID);
                int particleId = equip_effect_helper.GetParticleId(effectId, _actor.equipController.equipCloth.curEquipData.ID);
                if (particleId != 0)
                {
                    _actor.equipController.equipCloth.ChangeParticle(particleId);

                }
                if (flowId != 0)
                {
                    _actor.equipController.equipCloth.ChangeFlow(flowId);
                }
            }
        }

        public void Refresh()
        {
            LoadPlayerModel();
        }

        public void HideTheWarnMessage()
        {
            _warnTxt.Visible = false;
        }

        public void ShowTheWarnMessage()
        {
            if (PlayerDataManager.Instance.FashionData.CurrentFashionId != 0)
            {
                _warnTxt.Visible = true;
            }
        }

        public void LoadPlayerModel(bool forceReload = false)
        {
            currentEffectId = 0;
            currentGroupId = 0;
            if (_actor == null || forceReload == true)
            {
                _avatarModel.ClearModelGameObject();
                _avatarModel.LoadPlayerModel(OnPlayerModelLoaded);
            }
        }

        private void ChangePlayerShader(ACTActor actor)
        {
            SetModelShader(actor);
            SetModelEquipShader(actor);
        }

        private void AddEquipedEffect()
        {
            Dictionary<int, int> _equipedEffectDict = PlayerDataManager.Instance.EquipEffectData.GetEquipedEffectIdList();
            foreach (var pair in _equipedEffectDict)
            {
                ChangeEquipEffect(pair.Key, pair.Value);
            }
        }

        public void ResetModel()
        {
            switch (mode)
            {
                case FacadeType.Effect:
                    PutOnScenePlayerFacade();
                    break;
                case FacadeType.Fashion:
                    PutOnScenePlayerFacade();
                    break;
                case FacadeType.Mount:
                    RefreshPlayerMountState();
                    break;
            }
        }

        private void PutOnScenePlayerFacade()
        {
            if (_actor == null)
            {
                LoadPlayerModel(true);
                return;
            }
            if (PlayerAvatar.Player.actor.equipController.equipCloth.curEquipData != null)
            {
                int playerClothId = PlayerAvatar.Player.actor.equipController.equipCloth.curEquipData.ID;
                int playerClothParticleId = PlayerAvatar.Player.actor.equipController.equipCloth.curEquipParticleID;
                int playerClothFlowId = PlayerAvatar.Player.actor.equipController.equipCloth.curEquipFlowID;
                _actor.equipController.equipCloth.PutOn(playerClothId, playerClothParticleId, playerClothFlowId);
            }
            if (PlayerAvatar.Player.actor.equipController.equipWeapon.curEquipData != null)
            {
                int playerWeaponId = PlayerAvatar.Player.actor.equipController.equipWeapon.curEquipData.ID;
                int playerWeaponParticleId = PlayerAvatar.Player.actor.equipController.equipWeapon.curEquipParticleID;
                int playerWeaponFlowId = PlayerAvatar.Player.actor.equipController.equipWeapon.curEquipFlowID;
                _actor.equipController.equipWeapon.PutOn(playerWeaponId, playerWeaponParticleId, playerWeaponFlowId);
            }
            if (PlayerAvatar.Player.actor.equipController.equipWing.curEquipData != null)
            {
                int playerWingId = PlayerAvatar.Player.actor.equipController.equipWing.curEquipData.ID;
                int playerWingParticleId = PlayerAvatar.Player.actor.equipController.equipWing.curEquipParticleID;
                int playerWingFlowId = PlayerAvatar.Player.actor.equipController.equipWing.curEquipFlowID;
                _actor.equipController.equipWing.PutOn(playerWingId, playerWingParticleId, playerWingFlowId);
            }
            else
            {
                _actor.equipController.equipWing.PutOn(0);
            }
        }

        private void RefreshPlayerMountState()
        {
            int mountId = PlayerDataManager.Instance.mountData.CurrentMountId;
            if (mountId == 0)
            {
                RemoveMount();
            }
            else
            {
                Ride(mountId);
            }
        }

        public void HideFashionAttributeContainer()
        {
            _fashionAttributeContainer.Visible = false;
        }

        public void ShowFashionAttributeContainer(int type)
        {
            _fashionAttributeContainer.Show(type);
        }

        public void LoadDefaultPlayerModel()
        {
            AvatarModelData avatarModelData = ModelEquipTools.CreateAvatarModelData(PlayerAvatar.Player.vocation);
            AvatarEquipInfo avatarEquipInfo = new AvatarEquipInfo();
            EquipItemInfo equipInfo = PlayerDataManager.Instance.BagData.GetEquipedItem(Common.Structs.Enum.EquipType.breastPlate);
            avatarEquipInfo.equipID = equipInfo != null ? item_helper.GetEquipModelID(equipInfo.Id) : 0;
            avatarModelData.equips[ACTEquipmentType.Cloth] = avatarEquipInfo;
            string defaultEquipString = ModelEquipTools.CalculateEquipInfoStr(avatarModelData);
            _avatarModel.LoadAvatarModel(PlayerAvatar.Player.vocation, defaultEquipString, OnPlayerModelLoaded);
        }

        private ACTActor tempMountActor;
        public void Ride(int mountId)
        {
            _avatarModel.IsOnMount = true;
            if (mountId == currentMountId)
            {
                return;
            }
            currentMountId = mountId;
            if (_mountActor != null)
            {
                tempMountActor = _mountActor;
            }
            _avatarModel.LoadMountModel(mountId, OnMountLoaded);
        }

        private void OnMountLoaded(ACTActor mountActor)
        {
            _mountActor = mountActor;
            if (_actor != null)
            {
                _actor.rideController.Mount(mountActor, true);
                _actor.transform.localScale = Vector3.one;
                if (tempMountActor != null)
                {
                    Destroy(tempMountActor);
                    tempMountActor = null;
                }
                ChangePlayerShader(_actor);
            }
            _mountActor.transform.localEulerAngles = new Vector3(0, 240, 0);
        }

        public void RemoveMount()
        {
            currentMountId = 0;
            _avatarModel.IsOnMount = false;
            if (_actor != null && _mountActor != null)
            {
                _actor.rideController.Dismount();
                Destroy(_mountActor.gameObject);
                _mountActor = null;
            }
            ResetActor();
        }

        private const float MODE_SCALE_RATE = 0.8f;
        private const float ROTE_SPEED = 1.0f;
        private Vector3 _startPosition = new Vector3(0, 0, -500);
        private float _rotationY = 180f;
        private void ResetActor()
        {
            if (_actor != null)
            {
                _actor.actorController.isStatic = true;
                GameObject go = _actor.gameObject;
                _avatarModel.DragComponent.SetModel(go);
                var characterController = go.GetComponent<CharacterController>();
                RectTransform _rectTransform = _avatarModel.GetComponent<RectTransform>();
                characterController.enabled = false;
                float Scale = MODE_SCALE_RATE * _rectTransform.sizeDelta.y / characterController.height;
                go.transform.localScale = new Vector3(Scale, Scale, Scale);
                go.transform.localRotation = Quaternion.Euler(0, _rotationY, 0);
                go.transform.localPosition = new Vector3(0.5f * _rectTransform.sizeDelta.x, -0.92f * _rectTransform.sizeDelta.y, 0) + _startPosition;
                go.transform.SetParent(_avatarModel.transform, false);
            }
            
        }

        private void SetModelEquipShader(ACTActor actor)
        {
            const string shaderName = "MOGO2/Character/Player";
            Shader shader = ShaderRuntime.loader.Find(shaderName);
            actor.equipController.equipWeapon.ChangeEquipWeaponShader(shader);
        }

        private void SetModelShader(ACTActor actor)
        {
            Transform tran = actor.boneController.GetHumanBody();
            const string shaderName = "MOGO2/Character/Player";
            SkinnedMeshRenderer renderer = tran.GetComponent<SkinnedMeshRenderer>();
            Material material = renderer.material;
            Shader shader = ShaderRuntime.loader.Find(shaderName);
            if (shader != null)
            {
                material.shader = shader;
            }
        }

        public void ReloadPlayerAndMount()
        {
            if (_mountActor != null)
            {
                RemoveMount();
            }
            if (_actor == null)
            {
                _avatarModel.LoadPlayerModel(OnPlayerModelLoaded);
            }
            else
            {
                if (mode == FacadeType.Mount && PlayerDataManager.Instance.mountData.CurrentMountId > 0)
                {
                    SetModelEquipShader(_actor);
                    Ride(PlayerDataManager.Instance.mountData.CurrentMountId);
                }
                PutOnScenePlayerFacade();
            }
        }

        private void OnPlayerModelLoaded(ACTActor actor)
        {
            _actor = actor;
            _avatarModel.PlayRandomIdleAction();
            ChangePlayerShader(actor);
            ModelComponentUtil.ChangeUIModelParam(_actor.gameObject);
            SortOrderedRenderAgent.RebuildAll();
            AddEquipedEffect();
            if (mode == FacadeType.Mount && PlayerDataManager.Instance.mountData.CurrentMountId > 0)
            {
                SetModelEquipShader(_actor);
                Ride(PlayerDataManager.Instance.mountData.CurrentMountId);
            }
            PutOnScenePlayerFacade();
        }

        protected override void OnDisable()
        {
            _avatarModel.RemoveAnimationTimer();
        }
    }
}
