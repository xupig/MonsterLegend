﻿using Common.Utils;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using ModuleEquipFacade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModuleEquipFacade
{
    public class FashionAttributeItem : KList.KListItemBase
    {
        private string _data;
        private StateText _attriTxt;

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as string;
                    Refresh();
                }
            }
        }

        private void Refresh()
        {
            _attriTxt.CurrentText.text = _data;
        }

        protected override void Awake()
        {
            _attriTxt = GetChildComponent<StateText>("Label_txtShengming");
        }

        public override void Dispose()
        {

        }
    }

    public class FashionAttributeContainer : KContainer
    {
        private KContainer _unfoldContainer;
        private KDummyButton _foldBtn;
        private KContainer _foldContainer;
        private KDummyButton _unfoldBtn;
        private KList _attributeList;
        private StateText _fightForceTxt;
        private string _fightForceTemplate;
        private List<string> attributeDescList;
        private int currentType;
        private StateText _titleTxt1;
        private StateText _titleTxt2;

        protected override void Awake()
        {
            _foldContainer = GetChildComponent<KContainer>("Container_shousuo");
            _unfoldContainer = GetChildComponent<KContainer>("Container_zhankai");
            _titleTxt1 = _foldContainer.GetChildComponent<StateText>("Label_txtZongshuxing");
            _titleTxt1.Raycast = false;
            _titleTxt2 = _unfoldContainer.GetChildComponent<StateText>("Label_txtZongshuxing");
            _titleTxt2.Raycast = false;
            _foldBtn = _unfoldContainer.AddChildComponent<KDummyButton>("Container_open");
            _unfoldBtn = _foldContainer.AddChildComponent<KDummyButton>("Container_open");
            _attributeList = _unfoldContainer.GetChildComponent<KList>("List_attribute");
            _attributeList.SetDirection(KList.KListDirection.LeftToRight, 2);
            _fightForceTxt = _unfoldContainer.GetChildComponent<StateText>("Container_zhandouli/Label_txtZhanli");
            _fightForceTemplate = _fightForceTxt.CurrentText.text;
            attributeDescList = new List<string>();
            AddEventListener();
        }

        private void AddEventListener()
        {
            _foldBtn.onClick.AddListener(OnFoldBtnClick);
            _unfoldBtn.onClick.AddListener(OnUnfoldBtnClick);
        }

        private void OnFoldBtnClick()
        {
            _foldContainer.Visible = true;
            _unfoldContainer.Visible = false;
        }

        private void OnUnfoldBtnClick()
        {
            _unfoldContainer.Visible = true;
            _foldContainer.Visible = false;
            SetFashionAttribute(currentType);
        }

        public void Show(int type)
        {
            Visible = true;
            _foldContainer.Visible = true;
            _unfoldContainer.Visible = false;
            currentType = type;
            SetTitleTxt(type);
        }

        private void SetTitleTxt(int type)
        {
            switch (type)
            {
                case 1:
                    _titleTxt1.CurrentText.text = MogoLanguageUtil.GetContent(36518);
                    _titleTxt2.CurrentText.text = MogoLanguageUtil.GetContent(36518);
                    break;
                case 2:
                    _titleTxt1.CurrentText.text = MogoLanguageUtil.GetContent(112519);
                    _titleTxt2.CurrentText.text = MogoLanguageUtil.GetContent(112519);
                    break;
            }
        }

        private void SetFashionAttribute(int type)
        {
            attributeDescList.Clear();
            switch (type)
            {
                case 1:
                    Dictionary<int, int> attributeDict = PlayerDataManager.Instance.FashionData.GetTotalFashionAttributes();
                    foreach (var pair in attributeDict)
                    {
                        attributeDescList.Add(string.Format("{0}:+{1}", attri_config_helper.GetAttributeName(pair.Key), pair.Value));
                    }
                    _attributeList.SetDataList<FashionAttributeItem>(attributeDescList);
                    _fightForceTxt.CurrentText.text = string.Format(_fightForceTemplate, PlayerDataManager.Instance.FashionData.TotalFightForce);
                    break;
                case 2:
                    Dictionary<int, int> mountAttributeDict = PlayerDataManager.Instance.mountData.GetTotalMountAttributes();
                    foreach (var pair in mountAttributeDict)
                    {
                        attributeDescList.Add(string.Format("{0}:+{1}", attri_config_helper.GetAttributeName(pair.Key), pair.Value));
                    }
                    _attributeList.SetDataList<FashionAttributeItem>(attributeDescList);
                    _fightForceTxt.CurrentText.text = string.Format(_fightForceTemplate, PlayerDataManager.Instance.mountData.TotalFightForce);
                    break;
            }

        }
    }
}
