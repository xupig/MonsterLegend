﻿#region 模块信息
/*==========================================
// 文件名：EquipEffectItem
// 命名空间: ModuleEquipFacade
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/12 21:50:04
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
using UnityEngine.EventSystems;
namespace ModuleEquipFacade
{
    public class EquipEffectItem : KList.KListItemBase
    {
        private EquipEffectItemData _data;

        private StateImage _newImage;
        private ItemGrid _effectGrid;
        private KProgressBar _progressBar;
        private KContainer _selectedContainer;
        private StateText _txtUnLockCondition;
        private StateText _txtNeedUnlockLastEffect;
        private StateText _currentProgressTxt;
        private KButton _activateBtn;
        private KButton _equipBtn;
        private KButton _takeOffBtn;

        protected override void Awake()
        {
            _newImage = GetChildComponent<StateImage>("Image_NEWIcon");
            _effectGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _progressBar = GetChildComponent<KProgressBar>("ProgressBar_exp");
            _selectedContainer = GetChildComponent<KContainer>("Container_xuanzhong");
            _selectedContainer.Visible = false;
            _txtNeedUnlockLastEffect = GetChildComponent<StateText>("Label_txtXuyaojiesuoshangji");
            _txtUnLockCondition = GetChildComponent<StateText>("Label_txtShuliangxianzhi");
            _currentProgressTxt = GetChildComponent<StateText>("Label_txtJindu");
            _activateBtn = GetChildComponent<KButton>("Button_jihuo");
            _equipBtn = GetChildComponent<KButton>("Button_chuandai");
            _takeOffBtn = GetChildComponent<KButton>("Button_xiexia");
            AddEventListener();
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as EquipEffectItemData;
                    Refresh();
                }
            }
        }

        private void AddEventListener()
        {
            _activateBtn.onClick.AddListener(OnActivateBtnClick);
            _equipBtn.onClick.AddListener(OnEquipBtnClick);
            _takeOffBtn.onClick.AddListener(OnTakeOffBtnClick);
        }

        private void OnActivateBtnClick()
        {
            EquipEffectManager.Instance.RequestActiviteEffect(_data.id);
        }

        // 穿戴装备特效
        private void OnEquipBtnClick()
        {
            EventDispatcher.TriggerEvent(FashionEvents.WEAR_EQUIP_EFFECT, _data.groupId, _data.id);
            EquipEffectManager.Instance.SetEquipEffect(_data.id);
        }

        private void OnTakeOffBtnClick()
        {
            EventDispatcher.TriggerEvent(FashionEvents.TAKE_OFF_EQUIP_EFFECT, _data.groupId, _data.id);
            EquipEffectManager.Instance.RequestTakeOffEffect(_data.id);
        }

        private void Refresh()
        {
            _newImage.Visible = false;
            _effectGrid.SetItemData(_data.iconId, 0);
            HideAllBtns();
            _progressBar.Visible = true;
            switch (_data.effectState)
            {
                case EffectState.Lock:
                    RefreshLockState();
                    break;
                case EffectState.Processing:
                    RefreshProcessingState();
                    break;
                case EffectState.CanActivate:
                    RefreshCanActivateState();
                    break;
                case EffectState.Activated:
                    RefreshAcitvatedState();
                    break;
                case EffectState.Equiped:
                    RefreshEquipedState();
                    break;
            }
        }

        private void HideAllBtns()
        {
            _activateBtn.Visible = false;
            _equipBtn.Visible = false;
            _takeOffBtn.Visible = false;
        }

        private void RefreshLockState()
        {
            _txtNeedUnlockLastEffect.Visible = true;
            _txtUnLockCondition.CurrentText.text = string.Empty;
            _currentProgressTxt.CurrentText.text = string.Empty;
            _progressBar.Visible = false;
        }

        private void RefreshProcessingState()
        {
            _txtNeedUnlockLastEffect.Visible = false;
            _txtUnLockCondition.CurrentText.text = _data.unlockDesc;
            SetCurrentProgressTxt();
            _progressBar.Percent = PlayerDataManager.Instance.AchievementData.GetAchievementPercent(_data.achivementId);
        }

        private void SetCurrentProgressTxt(bool isAchieved = false)
        {
            int currentNumber = PlayerDataManager.Instance.AchievementData.GetAchievementCurrentNum(_data.achivementId);
            int targetNumber = PlayerDataManager.Instance.AchievementData.GetAchievementTargetNum(_data.achivementId);
            if (isAchieved == false)
            {
                _currentProgressTxt.CurrentText.text = string.Format("{0}/{1}", currentNumber, targetNumber);
            }
            else
            {
                _currentProgressTxt.CurrentText.text = string.Format("{0}/{1}", targetNumber, targetNumber);
            }
        }

        private void RefreshCanActivateState()
        {
            _newImage.Visible = true;
            _txtNeedUnlockLastEffect.Visible = false;
            _txtUnLockCondition.CurrentText.text = _data.unlockDesc;
            _progressBar.Percent = 100;
            SetCurrentProgressTxt(true);
            _activateBtn.Visible = true;
        }

        private void RefreshAcitvatedState()
        {
            _txtNeedUnlockLastEffect.Visible = false;
            _txtUnLockCondition.CurrentText.text = _data.unlockDesc;
            _progressBar.Percent = 100;
            SetCurrentProgressTxt(true);
            _equipBtn.Visible = true;
        }

        private void RefreshEquipedState()
        {
            _txtNeedUnlockLastEffect.Visible = false;
            _txtUnLockCondition.CurrentText.text = _data.unlockDesc;
            _progressBar.Percent = 100;
            SetCurrentProgressTxt(true);
            _takeOffBtn.Visible = true;
        }

        public void OnSelected()
        {
            _selectedContainer.Visible = true;
        }

        public void ClearSelectedMark()
        {
            _selectedContainer.Visible = false;
        }

        public override void Dispose()
        {

        }
    }
}