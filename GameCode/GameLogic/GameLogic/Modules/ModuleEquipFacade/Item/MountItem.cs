﻿#region 模块信息
/*==========================================
// 文件名：MountItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleEquipFacade.Item
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/12/4 15:09:55
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
namespace ModuleEquipFacade
{
    public class MountItem : KList.KListItemBase
    {
        private MountItemData _data;

        private StateImage _hasEquipedImage;
        private StateImage _newImage;
        private ItemGrid _mountGrid;
        private KContainer _selectedContainer;
        private KButton _buyBtn;
        private KButton _equipBtn;
        private KButton _takeOffBtn;
        private KButton _infoBtn;
        private KButton _previewBtn;
        private StateText _txtName;
        private StateText _txtDeadLine;
        private uint timeId;

        protected override void Awake()
        {
            _hasEquipedImage = GetChildComponent<StateImage>("Image_sharedyizhuangbei");
            _newImage = GetChildComponent<StateImage>("Image_NEWIcon");
            _mountGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _selectedContainer = GetChildComponent<KContainer>("Container_xuanzhong");
            _selectedContainer.Visible = false;
            _buyBtn = GetChildComponent<KButton>("Button_goumai");
            _equipBtn = GetChildComponent<KButton>("Button_zhuangbei");
            _takeOffBtn = GetChildComponent<KButton>("Button_xiexia");
            _infoBtn = GetChildComponent<KButton>("Button_Gantang");
            _previewBtn = GetChildComponent<KButton>("Button_yulan");
            _previewBtn.Visible = false;
            _txtName = GetChildComponent<StateText>("Label_txtShizhuang");
            _txtDeadLine = GetChildComponent<StateText>("Label_txtShizhuangmiaoshu");
            AddEventListener();
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as MountItemData;
                    Refresh();
                }
            }
        }

        private void AddEventListener()
        {
            _buyBtn.onClick.AddListener(OnBuyButtonClick);
            _equipBtn.onClick.AddListener(OnEquipBtnClick);
            _takeOffBtn.onClick.AddListener(OnTakeOffBtnClick);
            _infoBtn.onClick.AddListener(OnInfoBtnClick);
        }

        private void OnBuyButtonClick()
        {
            const int shopMountViewId = 68;
            view_helper.OpenView(shopMountViewId);
        }

        private void OnEquipBtnClick()
        {
            MountManager.Instance.SetCurrentReq((uint)_data.id);
            SelectThisItem();
        }

        private void OnTakeOffBtnClick()
        {
            MountManager.Instance.SetCurrentReq(0);
            SelectThisItem();
        }

        private void OnInfoBtnClick()
        {
            EventDispatcher.TriggerEvent<int>(MountEvents.SHOW_MOUNT_INFO, Index);
            SelectThisItem();
        }

        private void Refresh()
        {
            _mountGrid.SetItemData(_data.iconId, 0);
            _txtName.CurrentText.text = _data.Name;
            _newImage.Visible = _data.IsNew;
            switch (_data.state)
            {
                case MountState.NotHave:
                    RefreshNotHaveState();
                    break;
                case MountState.NotRide:
                    RefreshNotEquipState();
                    break;
                case MountState.Riding:
                    RefreshHasEquipedState();
                    break;
                default:
                    break;
            }
        }

        private void RefreshNotHaveState()
        {
            _buyBtn.Visible = true && (_data.source == 1);
            _equipBtn.Visible = false;
            _takeOffBtn.Visible = false;
            _hasEquipedImage.Visible = false;
            _txtDeadLine.CurrentText.text = _data.Description;
        }

        private void RefreshNotEquipState()
        {
            _buyBtn.Visible = false;
            _equipBtn.Visible = true;
            _takeOffBtn.Visible = false;
            _hasEquipedImage.Visible = false;
            timeId = TimerHeap.AddTimer(0, 1000, SetTimeDeadLine);
        }

        private void SetTimeDeadLine()
        {
            _txtDeadLine.CurrentText.text = _data.ValidDescString;
        }

        private void RefreshHasEquipedState()
        {
            _newImage.Visible = false;
            _buyBtn.Visible = false;
            _equipBtn.Visible = false;
            _takeOffBtn.Visible = true;
            _hasEquipedImage.Visible = true;
            timeId = TimerHeap.AddTimer(0, 1000, SetTimeDeadLine);
        }

        private void SelectThisItem()
        {
            //EventDispatcher.TriggerEvent<int>(FashionEvents.SELECT_ONE_FASHION, Index);
        }

        public void MarkSelected()
        {
            _selectedContainer.Visible = true;
        }

        public void ClearSelectedMark()
        {
            _selectedContainer.Visible = false;
        }

        public override void Dispose()
        {
            TimerHeap.DelTimer(timeId);
        }
    }
}