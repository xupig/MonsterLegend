﻿#region 模块信息
/*==========================================
// 文件名：FacadeItem
// 命名空间: GameLogic.GameLogic.Modules.ModuleEquipFacade
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/12 21:50:16
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.GlobalManager.SubSystem;
using MogoEngine.Events;
namespace ModuleEquipFacade
{
    public class FashionItem : KList.KListItemBase
    {
        private FashionItemData _data;

        private StateImage _hasEquipedImage;
        private StateImage _newImage;
        private ItemGrid _fashionGrid;
        private KContainer _selectedContainer;
        private KButton _buyBtn;
        private KButton _equipBtn;
        private KButton _takeOffBtn;
        private KButton _infoBtn;
        private StateText _txtName;
        private StateText _txtDeadLine;
        private uint timeId;

        protected override void Awake()
        {
            _hasEquipedImage = GetChildComponent<StateImage>("Image_sharedyizhuangbei");
            _newImage = GetChildComponent<StateImage>("Image_NEWIcon");
            _fashionGrid = GetChildComponent<ItemGrid>("Container_wupin");
            _selectedContainer = GetChildComponent<KContainer>("Container_xuanzhong");
            _selectedContainer.Visible = false;
            _buyBtn = GetChildComponent<KButton>("Button_goumai");
            _equipBtn = GetChildComponent<KButton>("Button_zhuangbei");
            _takeOffBtn = GetChildComponent<KButton>("Button_xiexia");
            _infoBtn = GetChildComponent<KButton>("Button_Gantang");
            _txtName = GetChildComponent<StateText>("Label_txtShizhuang");
            _txtDeadLine = GetChildComponent<StateText>("Label_txtShizhuangmiaoshu");
            AddEventListener();
        }

        public override object Data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != null)
                {
                    _data = value as FashionItemData;
                    Refresh();
                }
            }
        }

        private void AddEventListener()
        {
            _buyBtn.onClick.AddListener(OnBuyButtonClick);
            _equipBtn.onClick.AddListener(OnEquipBtnClick);
            _takeOffBtn.onClick.AddListener(OnTakeOffBtnClick);
            _infoBtn.onClick.AddListener(OnInfoBtnClick);
        }

        private void OnBuyButtonClick()
        {
            const int shopFashionViewId = 63;
            view_helper.OpenView(shopFashionViewId);
        }

        private void OnEquipBtnClick()
        {
            RoleManager.Instance.ReqSetCurrentFashion(_data.Id);
            SelectThisItem();
        }

        private void OnTakeOffBtnClick()
        {
            RoleManager.Instance.ReqSetCurrentFashion(0);
            SelectThisItem();
        }

        private void OnInfoBtnClick()
        {
            EventDispatcher.TriggerEvent<int>(FashionEvents.SHOW_FASHION_INFO, Index);
            SelectThisItem();
        }

        private void Refresh()
        {
            _fashionGrid.SetItemData(_data.Icon, 0);
            _txtName.CurrentText.text = _data.Name;
            _newImage.Visible = _data.IsNew;
            switch (_data.state)
            {
                case FashionState.NotHave:
                    RefreshNotHaveState();
                    break;
                case FashionState.NotEquiped:
                    RefreshNotEquipState();
                    break;
                case FashionState.Equiped:
                    RefreshHasEquipedState();
                    break;
                default:
                    break;
            }
        }

        private void RefreshNotHaveState()
        {
            _buyBtn.Visible = true;
            _equipBtn.Visible = false;
            _takeOffBtn.Visible = false;
            _hasEquipedImage.Visible = false;
            _txtDeadLine.CurrentText.text = _data.Description;
        }

        private void RefreshNotEquipState()
        {
            _buyBtn.Visible = false;
            _equipBtn.Visible = true;
            _takeOffBtn.Visible = false;
            _hasEquipedImage.Visible = false;
            timeId = TimerHeap.AddTimer(0, 1000, SetTimeDeadLine);
        }

        private void SetTimeDeadLine()
        {
            _txtDeadLine.CurrentText.text = _data.ValidDescString;
        }

        private void RefreshHasEquipedState()
        {
            _newImage.Visible = false;
            _buyBtn.Visible = false;
            _equipBtn.Visible = false;
            _takeOffBtn.Visible = true;
            _hasEquipedImage.Visible = true;
            timeId = TimerHeap.AddTimer(0, 1000, SetTimeDeadLine);
        }

        private void SelectThisItem()
        {
            EventDispatcher.TriggerEvent<int>(FashionEvents.SELECT_ONE_FASHION, Index);
        }

        public void MarkSelected()
        {
            _selectedContainer.Visible = true;
        }

        public void ClearSelectedMark()
        {
            _selectedContainer.Visible = false;
        }

        public override void Dispose()
        {
            TimerHeap.DelTimer(timeId);
        }
    }
}