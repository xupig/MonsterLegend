﻿#region 模块信息
/*==========================================
// 文件名：EquipFacadeTip
// 命名空间: GameLogic.GameLogic.Modules.ModuleEquipFacade
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/12/11 17:52:01
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using UnityEngine;
namespace ModuleEquipFacade
{
    public enum EquipTipType
    {
        EffectType = 1,
        FashionType = 2,
        MountType = 3,
    }

    public class EquipFacadeTipDataWrapper
    {
        public EquipTipType type;
        public int effectId;
        public Vector2 position;
        public FashionItemData _fashionItemData;
        public MountItemData _mountItemData;
    }

    public class EquipFacadeTip : BasePanel
    {
        private GetNewEffectTip _newEffectTip;
        private FashionTip _fashionTip;

        protected override void Awake()
        {
            _newEffectTip = AddChildComponent<GetNewEffectTip>("Container_tanchuang");
            _fashionTip = AddChildComponent<FashionTip>("Container_shizhuangtanchuang");
        }

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.EquipFacadeTip; }
        }

        public override void OnShow(object data)
        {
            EquipFacadeTipDataWrapper wrapper = data as EquipFacadeTipDataWrapper;
            if (wrapper.type == EquipTipType.EffectType)
            {
                _fashionTip.Visible = false;
                _newEffectTip.Visible = true;
                _newEffectTip.SetEffectIcon(wrapper.effectId);
            }
            else if (wrapper.type == EquipTipType.FashionType)
            {
                _newEffectTip.Visible = false;
                _fashionTip.Visible = true;
                _fashionTip.Refresh(wrapper._fashionItemData, wrapper.position);
            }
            else if (wrapper.type == EquipTipType.MountType)
            {
                _newEffectTip.Visible = false;
                _fashionTip.Visible = true;
                _fashionTip.Refresh(wrapper._mountItemData, wrapper.position);
            }
        }

        public override void OnClose()
        {

        }
    }
}