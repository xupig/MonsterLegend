﻿using Common.Global;
using Common.Utils;
using GameData;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquipFacade
{
    public enum FashionState
    {
        NotHave = 1,    // 未拥有
        NotEquiped = 2, // 购买之后未穿戴
        Equiped = 3,    // 穿戴状态
    }

    public enum LimitType
    {
        // 未拥有该时装的时候为空（无法确定）
        Empty = 0,
        // 有时限
        TimeLimited = 1,
        // 永久套装类型
        NoTimeLimited = 2,
    }

    public class FashionItemData
    {
        private fashion _fashionConfig;

        public FashionItemData(int id)
        {
            Id = id;
            _fashionConfig = fashion_helper.GetFashionCfg(id);
            state = FashionState.NotHave;
            type = LimitType.Empty;
        }

        /// <summary>
        /// 时装ID
        /// </summary>
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        /// 时装的状态
        /// </summary>
        public FashionState state
        {
            get;
            set;
        }

        public LimitType type
        {
            get;
            set;
        }

        /// <summary>
        /// 时装名字
        /// </summary>
        public string Name
        {
            get
            {
                return fashion_helper.GetName(_fashionConfig);
            }
        }

        /// <summary>
        /// 时装描述
        /// </summary>
        public string Description
        {
            get
            {
                return fashion_helper.GetDiscription(_fashionConfig);
            }
        }

        /// <summary>
        /// 时装来源：1商城 2活动
        /// </summary>
        public int Source
        {
            get
            {
                return fashion_helper.GetSource(_fashionConfig);
            }
        }

        /// <summary>
        /// 时装图标
        /// </summary>
        public int Icon
        {
            get
            {
                int vocationIndex = (int)PlayerAvatar.Player.vocation;
                if (_fashionConfig.__icon.ContainsKey(vocationIndex.ToString()))
                {
                    return int.Parse(_fashionConfig.__icon[vocationIndex.ToString()]);
                }
                return 0;
            }
        }

        /// <summary>
        /// 时装ID
        /// </summary>
        public int ClothId
        {
            get
            {
                int vocationIndex = (int)PlayerAvatar.Player.vocation;
                if (_fashionConfig.__cloth != null && _fashionConfig.__cloth.ContainsKey(vocationIndex.ToString()))
                {
                    return int.Parse(_fashionConfig.__cloth[vocationIndex.ToString()]);
                }
                return 0;
            }
        }

        /// <summary>
        /// 武器Id
        /// </summary>
        public int WeaponId
        {
            get
            {
                int vocationIndex = (int)PlayerAvatar.Player.vocation;
                if (_fashionConfig.__weapon != null && _fashionConfig.__weapon.ContainsKey(vocationIndex.ToString()))
                {
                    return int.Parse(_fashionConfig.__weapon[vocationIndex.ToString()]);
                }
                return 0;
            }
        }

        /// <summary>
        /// 时装显示效果
        /// </summary>
        public int DisplayEffect
        {
            get
            {
                return fashion_helper.GetDisplayEffect(_fashionConfig);
            }
        }

        /// <summary>
        /// 属性效果ID
        /// </summary>
        public int EffectId
        {
            get
            {
                return fashion_helper.GetEffectId(_fashionConfig, PlayerAvatar.Player.vocation);
            }
        }

        /// <summary>
        /// 隐藏方式：1不隐藏 2隐藏 3置灰
        /// </summary>
        public int HideType
        {
            get
            {
                return _fashionConfig.__hide;
            }
        }

        /// <summary>
        /// 时装组别（表示投放时间顺序）
        /// </summary>
        public int GorupId
        {
            get
            {
                return _fashionConfig.__group_id;
            }
        }

        /// <summary>
        /// 是否新投入的时装
        /// </summary>
        public bool IsNew
        {
            get;
            set;
        }

        /// <summary>
        /// 剩余时间
        /// 如果是永久套装，则该字段为0
        /// </summary>
        private uint validTime;
        public uint ValidTime
        {
            get { return validTime; }
            set
            {
                if (value == 0)
                {
                    type = LimitType.NoTimeLimited;
                    validTime = value;
                }
                else
                {
                    type = LimitType.TimeLimited;
                    validTime = value;
                }
            }
        }

        public string ValidDescString
        {
            get
            {
                if (state == FashionState.NotHave)
                {
                    return Description;
                }
                if (type == LimitType.NoTimeLimited)
                {
                    return MogoLanguageUtil.GetContent(36508);
                }
                long ValidSeconds = (long)ValidTime;
                int currentTimeStamp = (int)Global.serverTimeStampSecond;
                long leftSeconds = ValidSeconds - (long)currentTimeStamp;
                if (leftSeconds > 3600 * 24)
                {
                    string descString = MogoLanguageUtil.GetContent(36506);
                    int leftDays = (int)(leftSeconds / (3600 * 24));
                    leftDays += leftSeconds % (3600 * 24) > 0 ? 1 : 0;
                    return string.Format(descString, leftDays);
                }
                else if (leftSeconds > 3600)
                {
                    string descString2 = MogoLanguageUtil.GetContent(36507);
                    int leftHours = (int)(leftSeconds / 3600);
                    leftHours += leftSeconds % 3600 > 0 ? 1 : 0;
                    return string.Format(descString2, leftHours);
                }
                else
                {
                    string descString3 = MogoLanguageUtil.GetContent(36517);
                    int leftMinutes = (int)(leftSeconds / 60);
                    leftMinutes += (leftSeconds % 60) > 0 ? 1 : 0;
                    return string.Format(descString3, leftMinutes);
                }
            }
        }

        public string ValidDescStringInTips
        {
            get
            {
                if (state == FashionState.NotHave)
                {
                    return MogoLanguageUtil.GetContent(36512);
                }
                if (type == LimitType.NoTimeLimited)
                {
                    return MogoLanguageUtil.GetContent(36508);
                }
                int ValidSeconds = (int)ValidTime;
                int currentTimeStamp = (int)Global.serverTimeStampSecond;
                int leftSeconds = ValidSeconds - currentTimeStamp;
                if (leftSeconds > 3600 * 24)
                {
                    string descString = string.Concat(MogoLanguageUtil.GetContent(36511), MogoLanguageUtil.GetContent(36509));
                    int leftDays = leftSeconds / (3600 * 24);
                    leftDays += leftSeconds % (3600 * 24) > 0 ? 1 : 0;
                    return string.Format(descString, leftDays);
                }
                else if (leftSeconds > 3600)
                {
                    string descString2 = string.Concat(MogoLanguageUtil.GetContent(36511), MogoLanguageUtil.GetContent(36510));
                    int leftHours = leftSeconds / 3600;
                    leftHours += (leftSeconds % 3600) > 0 ? 1 : 0;
                    return string.Format(descString2, leftHours);
                }
                else
                {
                    string descString3 = string.Concat(MogoLanguageUtil.GetContent(36511), MogoLanguageUtil.GetContent(36516));
                    int leftMinutes = leftSeconds / 60;
                    leftMinutes += (leftSeconds % 60) > 0 ? 1 : 0;
                    return string.Format(descString3, leftMinutes);
                }
            }
        }

        public int FightForce
        {
            get
            {
                int vocationIndex = (int)PlayerAvatar.Player.vocation;
                int effectId = int.Parse(_fashionConfig.__effect_id[vocationIndex.ToString()]);
                return fight_force_helper.GetFightForce(effectId, PlayerAvatar.Player.vocation);
            }
            set { }
        }

    }
}
