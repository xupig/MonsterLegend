﻿#region 模块信息
/*==========================================
// 文件名：EquipEffectData
// 命名空间: GameLogic.GameLogic.Modules.ModuleEquipFacade.ItemData
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/12 21:51:07
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using GameData;
namespace ModuleEquipFacade
{
    public enum EffectState
    {
        Lock = 0,
        Processing = 1,
        CanActivate = 2,
        Activated = 3,
        Equiped = 4,
    }

    public class EquipEffectItemData
    {
        public int id;
        public string name;
        public int groupId;
        public string groupName;
        public int level;
        public int iconId;
        public int achivementId;    // 对应需要完成的成就内容
        public string unlockDesc;  //解锁条件

        public EffectState effectState;

        public EquipEffectItemData(equip_facade cfg)
        {
            id = cfg.__id;
            name = MogoLanguageUtil.GetContent(cfg.__name);
            groupId = cfg.__group;
            groupName = MogoLanguageUtil.GetContent(cfg.__group_name);
            level = cfg.__level;
            iconId = cfg.__icon;
            achivementId = cfg.__achievement_id;
            effectState = EffectState.Lock;
            unlockDesc = MogoLanguageUtil.GetContent(cfg.__describe);
        }
    }
}