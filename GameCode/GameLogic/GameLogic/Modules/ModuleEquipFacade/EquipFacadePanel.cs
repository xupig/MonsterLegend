﻿#region 模块信息
/*==========================================
// 文件名：EquipFacadePanel
// 命名空间: GameLogic.GameLogic.Modules.ModuleEquipFacade
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/12 13:30:18
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;

namespace ModuleEquipFacade
{
    public class EquipFacadePanel : BasePanel
    {
        private CategoryList _categoryList;
        private ModelView _modelView;
        private KScrollView _facadeScrollView;
        private KScrollView _equipEffectView;
        private KScrollView _mountScrollView;
        private KPageableList _fashionList;
        private KPageableList _equipEffectList;
        private KPageableList _mountList;
        private List<int> groupList = new List<int>();
        private List<string> groupNameList = new List<string>();
        private const int FACADE = 9999;
        private const int MOUNT = 10000;
        private int lastSelectedCategoryIndex = -1;
        private int lastSelectedEffectIndex = -1;
        private int lastSelectedFashionIndex = -1;
        private int lastSelectedMountIndex = -1;

        protected override PanelIdEnum Id
        {
            get { return PanelIdEnum.EquipFacade; }
        }

        protected override void Awake()
        {
            _categoryList = GetChildComponent<CategoryList>("List_categoryList");
            _modelView = AddChildComponent<ModelView>("Container_wingModelView");
            _modelView.LoadDefaultPlayerModel();
            _facadeScrollView = GetChildComponent<KScrollView>("ScrollView_shizhuang");
            _fashionList = _facadeScrollView.GetChildComponent<KPageableList>("mask/content");
            _equipEffectView = GetChildComponent<KScrollView>("ScrollView_effectList");
            _equipEffectList = _equipEffectView.GetChildComponent<KPageableList>("mask/content");
            _mountScrollView = GetChildComponent<KScrollView>("ScrollView_zuoqi");
            _mountList = _mountScrollView.GetChildComponent<KPageableList>("mask/content");
            InitCategoryList();
            SetListProperty();
        }

        private void InitCategoryList()
        {
            Dictionary<int, string> groupNameDict = equip_facade_helper.GetFacadeGroupList();
            List<CategoryItemData> categoryDataList = new List<CategoryItemData>();
            foreach (var pair in groupNameDict)
            {
                groupList.Add(pair.Key);
                groupNameList.Add(pair.Value);
                categoryDataList.Add(CategoryItemData.GetCategoryItemData(pair.Value));
            }
            _categoryList.SetDataList<CategoryListItem>(categoryDataList);
            SetCategoryListPoint();
        }

        private void SetCategoryListPoint()
        {
            for (int i = 0; i < groupList.Count; i++)
            {
                if (groupList[i] != FACADE && groupList[i] != MOUNT)
                {
                    (_categoryList.ItemList[i] as CategoryListItem).SetPoint(PlayerDataManager.Instance.EquipEffectData.HasCanActivateEffectByGroup(groupList[i]));
                }
            }
        }

        private void SetListProperty()
        {
            _fashionList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _fashionList.SetGap(-23, 0);
            _equipEffectList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _equipEffectList.SetGap(-16, 0);
            _mountList.SetDirection(KList.KListDirection.TopToDown, int.MaxValue);
            _mountList.SetGap(-23, 0);

        }

        public override void OnShow(object data)
        {
            int defaultIndex = 0;
            if (data != null && data is int)
            {
                defaultIndex = (int)data;
            }
            _categoryList.SelectedIndex = defaultIndex;
            OnSelectedIndexChanged(_categoryList, defaultIndex);
            lastSelectedCategoryIndex = -1;
            AddEventListener();
            SetCategoryListPoint();
        }

        public override void OnClose()
        {
            lastSelectedCategoryIndex = -1;
            lastSelectedEffectIndex = -1;
            lastSelectedFashionIndex = -1;
            lastSelectedMountIndex = -1;
            RemoveEventListener();
        }

        private void AddEventListener()
        {
            _categoryList.onSelectedIndexChanged.AddListener(OnSelectedIndexChanged);
            _equipEffectList.onItemClicked.AddListener(OnEquipEffectClick);
            _fashionList.onItemClicked.AddListener(OnFashionItemClick);
            _mountList.onItemClicked.AddListener(OnMountItemClick);
            EventDispatcher.AddEventListener<int>(FashionEvents.SHOW_FASHION_INFO, ShowFashionInfo);
            EventDispatcher.AddEventListener(FashionEvents.REFRESH_FASHION_LIST, RefreshFashionStateInList);
            EventDispatcher.AddEventListener<int>(FashionEvents.PUT_ON_FASHION, ReloadModel);
            EventDispatcher.AddEventListener<int>(FashionEvents.SELECT_ONE_FASHION, SelectOneFashionItem);
            EventDispatcher.AddEventListener(FashionEvents.REFRESH_EFFECT_LIST, RefreshEffectStateInList);
            EventDispatcher.AddEventListener(FashionEvents.ACTIVATE_FASHION_CHANGE, SetCategoryListPoint);
            EventDispatcher.AddEventListener(MountEvents.REFRESH_MOUNT_LIST, RefreshMountStateInList);
            EventDispatcher.AddEventListener<int>(MountEvents.RIDE_ON_MOUNT, ReloadMountModel);
            EventDispatcher.AddEventListener<int>(MountEvents.SHOW_MOUNT_INFO, ShowMountInfo);
        }

        private void ShowFashionInfo(int fashionIndex)
        {
            if (fashionIndex >= 0 && fashionIndex < _fashionList.ItemList.Count)
            {
                Vector2 position = RectTransformUtility.WorldToScreenPoint(UIManager.Instance.UICamera, _fashionList.ItemList[fashionIndex].transform.position);
                EquipFacadeTipDataWrapper wrapper = new EquipFacadeTipDataWrapper();
                wrapper.type = EquipTipType.FashionType;
                wrapper._fashionItemData = _fashionList.ItemList[fashionIndex].Data as FashionItemData;
                wrapper.position = position;
                UIManager.Instance.ShowPanel(PanelIdEnum.EquipFacadeTip, wrapper);
            }
        }

        private void ShowMountInfo(int mountIndex)
        {
            if (mountIndex >= 0 && mountIndex < _mountList.ItemList.Count)
            {
                Vector2 position = RectTransformUtility.WorldToScreenPoint(UIManager.Instance.UICamera, _mountList.ItemList[mountIndex].transform.position);
                EquipFacadeTipDataWrapper wrapper = new EquipFacadeTipDataWrapper();
                wrapper.type = EquipTipType.MountType;
                wrapper._mountItemData = _mountList.ItemList[mountIndex].Data as MountItemData;
                wrapper.position = position;
                UIManager.Instance.ShowPanel(PanelIdEnum.EquipFacadeTip, wrapper);
            }
        }

        private void RemoveEventListener()
        {
            _categoryList.onSelectedIndexChanged.RemoveListener(OnSelectedIndexChanged);
            _equipEffectList.onItemClicked.RemoveListener(OnEquipEffectClick);
            _fashionList.onItemClicked.RemoveListener(OnFashionItemClick);
            _mountList.onItemClicked.RemoveListener(OnMountItemClick);
            EventDispatcher.RemoveEventListener<int>(FashionEvents.SHOW_FASHION_INFO, ShowFashionInfo);
            EventDispatcher.RemoveEventListener(FashionEvents.REFRESH_FASHION_LIST, RefreshFashionStateInList);
            EventDispatcher.RemoveEventListener<int>(FashionEvents.PUT_ON_FASHION, ReloadModel);
            EventDispatcher.RemoveEventListener<int>(FashionEvents.SELECT_ONE_FASHION, SelectOneFashionItem);
            EventDispatcher.RemoveEventListener(FashionEvents.REFRESH_EFFECT_LIST, RefreshEffectStateInList);
            EventDispatcher.RemoveEventListener(FashionEvents.ACTIVATE_FASHION_CHANGE, SetCategoryListPoint);
            EventDispatcher.RemoveEventListener(MountEvents.REFRESH_MOUNT_LIST, RefreshMountStateInList);
            EventDispatcher.RemoveEventListener<int>(MountEvents.RIDE_ON_MOUNT, ReloadMountModel);
            EventDispatcher.RemoveEventListener<int>(MountEvents.SHOW_MOUNT_INFO, ShowMountInfo);
        }

        private void ReloadMountModel(int mountId)
        {
            if (mountId == 0)
            {
                _modelView.RemoveMount();
            }
            else
            {
                _modelView.Ride(mountId);
            }
        }

        private void RefreshMountStateInList()
        {
            for (int i = 0; i < _mountList.ItemList.Count; i++)
            {
                MountItemData data = _mountList.ItemList[i].Data as MountItemData;
                _mountList.ItemList[i].Data = PlayerDataManager.Instance.mountData.GetMountItemData(data.id);
            }
        }

        private void ReloadEffectModel()
        {
            //_modelView.ResetModel();
        }

        private void RefreshEffectStateInList()
        {
            SetCategoryListPoint();
            for (int i = 0; i < _equipEffectList.ItemList.Count; i++)
            {
                EquipEffectItemData data = _equipEffectList.ItemList[i].Data as EquipEffectItemData;
                _equipEffectList.ItemList[i].Data = PlayerDataManager.Instance.EquipEffectData.GetEquipEffectItemData(data.id);
            }
        }

        private void SelectOneFashionItem(int index)
        {
            ClearAllItemMark(_fashionList);
            (_fashionList.ItemList[index] as FashionItem).MarkSelected();
        }

        private void ReloadModel(int fashionId)
        {
            _modelView.ResetModel();
        }

        private void RefreshFashionStateInList()
        {
            for (int i = 0; i < _fashionList.ItemList.Count; i++)
            {
                FashionItemData data = _fashionList.ItemList[i].Data as FashionItemData;
                _fashionList.ItemList[i].Data = PlayerDataManager.Instance.FashionData.GetFashionItemData(data.Id);
            }
        }

        private void ClearAllItemMark(KList list)
        {
            if (list == _fashionList)
            {
                for (int i = 0; i < list.ItemList.Count; i++)
                {
                    (list.ItemList[i] as FashionItem).ClearSelectedMark();
                }
            }
            if (list == _equipEffectList)
            {
                for (int i = 0; i < list.ItemList.Count; i++)
                {
                    (list.ItemList[i] as EquipEffectItem).ClearSelectedMark();
                }
            }
            if (list == _mountList)
            {
                for (int i = 0; i < list.ItemList.Count; i++)
                {
                    (list.ItemList[i] as MountItem).ClearSelectedMark();
                }
            }
        }

        private void OnFashionItemClick(KList list, KList.KListItemBase item)
        {
            if (item.Index == lastSelectedFashionIndex)
            {
                return;
            }
            ClearAllItemMark(list);
            lastSelectedFashionIndex = item.Index;
            FashionItem fashionItem = item as FashionItem;
            fashionItem.MarkSelected();
            FashionItemData fashionItemData = item.Data as FashionItemData;
            _modelView.PutOnFashion(fashionItemData.Id, fashionItemData.ClothId, fashionItemData.WeaponId);
        }

        private void OnEquipEffectClick(KList list, KList.KListItemBase item)
        {
            if (item.Index == lastSelectedEffectIndex)
            {
                return;
            }
            ClearAllItemMark(list);
            lastSelectedEffectIndex = item.Index;
            EquipEffectItem effectItem = item as EquipEffectItem;
            effectItem.OnSelected();
            EquipEffectItemData effectItemData = effectItem.Data as EquipEffectItemData;
            _modelView.ChangeEquipEffect(effectItemData.groupId, effectItemData.id);
        }

        private void OnMountItemClick(KList list, KList.KListItemBase item)
        {
            if (item.Index == lastSelectedMountIndex)
            {
                return;
            }
            ClearAllItemMark(list);
            lastSelectedMountIndex = item.Index;
            MountItem mountItem = item as MountItem;
            mountItem.MarkSelected();
            MountItemData mountItemData = item.Data as MountItemData;
            _modelView.Ride(mountItemData.id);
        }

        private void OnSelectedIndexChanged(CategoryList categoryList, int index)
        {
            if (index == lastSelectedCategoryIndex)
            {
                return;
            }
            if (lastSelectedFashionIndex != -1)
            {
                (_fashionList[lastSelectedFashionIndex] as FashionItem).ClearSelectedMark();
            }
            if (lastSelectedEffectIndex != -1)
            {
                (_equipEffectList[lastSelectedEffectIndex] as EquipEffectItem).ClearSelectedMark();
            }
            if (lastSelectedMountIndex != -1)
            {
                (_mountList[lastSelectedMountIndex] as MountItem).ClearSelectedMark();
            }
            if (groupList[index] == FACADE)
            {
                ModelView.mode = FacadeType.Fashion;
                _modelView.ReloadPlayerAndMount();
                _modelView.HideTheWarnMessage();
                _modelView.ShowFashionAttributeContainer(1);
                _facadeScrollView.Visible = true;
                _equipEffectView.Visible = false;
                _mountScrollView.Visible = false;
                _fashionList.SetDataList<FashionItem>(PlayerDataManager.Instance.FashionData.GetFashionInfoList());
            }
            else if (groupList[index] == MOUNT)
            {
                ModelView.mode = FacadeType.Mount;
                _modelView.ReloadPlayerAndMount();
                _modelView.ShowFashionAttributeContainer(2);
                _modelView.HideTheWarnMessage();
                _facadeScrollView.Visible = false;
                _equipEffectView.Visible = false;
                _mountScrollView.Visible = true;
                _mountList.SetDataList<MountItem>(PlayerDataManager.Instance.mountData.GetMountInfoList());
            }
            else  // 当切换的标签页为装备外观的时候
            {
                ModelView.mode = FacadeType.Effect;
                _modelView.ReloadPlayerAndMount();
                _modelView.ShowTheWarnMessage();
                _modelView.HideFashionAttributeContainer();
                _facadeScrollView.Visible = false;
                _equipEffectView.Visible = true;
                _mountScrollView.Visible = false;
                _equipEffectList.SetDataList<EquipEffectItem>(PlayerDataManager.Instance.EquipEffectData.GetEquipEffectItemDataByGroup(groupList[index]));
            }
            lastSelectedCategoryIndex = index;
            lastSelectedFashionIndex = -1;
            lastSelectedEffectIndex = -1;
            lastSelectedMountIndex = -1;
        }
    }

}