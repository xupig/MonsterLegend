﻿using Common.Base;
using Common.ExtendComponent;
using Game.UI.UIComponent;
using GameData;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ModuleEquipFacade
{
    public class GetNewEffectTip : KContainer
    {
        private ItemGrid _newEffectIcon;
        private KButton _closeBtn;
        private KButton _sureBtn;
        private KParticle _particle;

        protected override void Awake()
        {
            _newEffectIcon = GetChildComponent<ItemGrid>("Container_wupin");
            _particle = _newEffectIcon.AddChildComponent<KParticle>("fx_ui_9_4_xuanzhuanguangsu");
            _particle.transform.localPosition = new Vector3(-44, 74, 0);
            _particle.Stop();
            _closeBtn = GetChildComponent<KButton>("Container_panelBg/Button_close");
            _sureBtn = GetChildComponent<KButton>("Button_queding");
            AddEventListener();
        }

        private void AddEventListener()
        {
            _closeBtn.onClick.AddListener(CloseTip);
            _sureBtn.onClick.AddListener(CloseTip);
        }

        public void SetEffectIcon(int effectId)
        {
            Visible = true;
            _newEffectIcon.SetItemData(equip_facade_helper.GetEffectIcon(effectId), 0);
            _particle.Play(true);
        }

        private void CloseTip()
        {
            UIManager.Instance.ClosePanel(PanelIdEnum.EquipFacadeTip);
            _particle.Stop();
        }
    }
}
