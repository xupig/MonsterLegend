﻿#region 模块信息
/*==========================================
// 文件名：EquipFacadeModule
// 命名空间: GameLogic.GameLogic.Modules.ModuleEquipFacade
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/16 10:25:43
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
namespace ModuleEquipFacade
{
    public class EquipFacadeModule : BaseModule
    {

        public override ModuleType ModuleType
        {
            get { return ModuleType.OneOff; }
        }

        public override void Init()
        {

        }
    }
}