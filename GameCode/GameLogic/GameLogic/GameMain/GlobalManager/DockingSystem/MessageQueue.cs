﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameMain.GlobalManager
{
    public struct CMessage
    {
        public int nResponseID;
        public int nRequestID;
        public int nRetCode;
        public int nResponseType; //0字符串 1图片
        public byte[] strInfo;
        public string strInfo2;//拓展字段 log的各种子类型
        public uint nLength;
    };

    public class MessageQueue
    {
        private static MessageQueue s_instance = null;
        public static MessageQueue GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new MessageQueue();
            }
            return s_instance;
        }

        private const int MAX_MSG_PER_FRAME = 1000;
        public Queue<CMessage> m_dqMessageQueue = new Queue<CMessage>();
        public void AppendMessage(CMessage pMsg)
        {
            lock (m_dqMessageQueue)
            {
                m_dqMessageQueue.Enqueue(pMsg);
            }
        }

        public void RunMessages()
        {
            int nMsgCount = 0;
            while (true)
            {
                CMessage pMsg = new CMessage();
                bool isGetMessag = false;
                lock(m_dqMessageQueue)
                {
                    if(m_dqMessageQueue.Count > 0)
                    {
                        pMsg = m_dqMessageQueue.Dequeue();
                        isGetMessag = true;
                    }
                }
                if (isGetMessag)
                {
                    DockingService.ExecuteMessage(pMsg);
                    if (MAX_MSG_PER_FRAME < ++nMsgCount)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
    }
}
