﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
#if UNITY_WEBPLAYER
using GameLoader.Utils;
#endif

namespace GameMain.GlobalManager
{
    public class DockingPlugin
    {
#if UNITY_WEBPLAYER
        private static DockingPlugin s_instance = null;
        public static DockingPlugin GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new DockingPlugin();
            }
            return s_instance;
        }

        /*
        public static IntPtr CreateDocking(ResponseFunc callback, string szInfo)
        {
            return Docking.CreateDocking(callback, szInfo);
        }*/

        public static bool Request(IntPtr pContext, int reqID, string szInfo1, string szInfo2, string szInfo3)
		{
            return false;//Ari Docking.Request(pContext, reqID, szInfo1, szInfo2, szInfo3);
		}
        /*
        public static void DestroyDocking(IntPtr pContext)
        {
            Docking.DestroyDocking(pContext);
        }*/
#else
        //private  const string PluginPath = @"E:\work\Launcher\Debug\Docking_d.dll";
        //private const string PluginPath = @"D:\4399\qjphs\launcher\Docking.dll";
        private const string PluginPath = "../../Docking.dll";
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void ResponseFunc(int nRespID, int retCode, int nReqID, int nResponseType, IntPtr szInfo, uint len, string szInfo2);

        [DllImport(PluginPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CreateDocking(ResponseFunc callback, string szInfo);

        [DllImport(PluginPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Request(IntPtr pContext, int reqID, string szInfo1, string szInfo2, string szInfo3);

        [DllImport(PluginPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void DestroyDocking(IntPtr pContext);
#endif
    }
}
