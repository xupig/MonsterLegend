﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using LitJson;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using GameMain.Entities;
using GameLoader.Utils;
using UnityEngine;
using GameLoader.Config;
using Common.Data;
using MogoEngine;
//using System.Net.NetworkInformation;
using MogoEngine.Events;
using Common.Events;

namespace GameMain.GlobalManager
{
   
    public enum enumResponseID
    {
        ResponseID_Ready = 0,
        ResponseID_Request,
    }

    public enum enumRequestID
    {
        RequestID_None = 0,
        RequestID_GetData,
        RequestID_Login,
        RequestID_Charge,
        RequestID_Log,
        RequestID_PostData,
        RequestID_CheckPic,
        RequestID_MAX,
    }

    public enum enumResponseType
    {
        ResponseType_String = 0,
        ResponseType_Texture,
    }

    public class DockingService
    {
        public static bool isReady = false;
        protected static IntPtr m_pContext = IntPtr.Zero;
        private static MessageQueue sMessageQueue = new MessageQueue();
        private static JsonData m_jsonValue = new JsonData();
        private static JsonData m_jsonInitValue = new JsonData();
        private const string LogOpenGame = "OpenGame";
        private const string LogOpenLoginView = "OpenLoginView";
        private const string LogLogin = "Login";
        private const string LogCreateRole = "CreateRole";
        private const string LogSelectServer = "SelectServer";
        private const string LogLevelUp = "LevelUp";
        private const string LogOnline = "Online";
        private static string _uid = "";
        private static string _device_type = "";
        private static string _fngid = "";

        public static bool Initialize()
        {

            if (IntPtr.Zero != m_pContext)
            {
                return true;
            }
            //EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, RoleLevelChangeLog);
            InitJsonValue();
            if (Application.platform == RuntimePlatform.WindowsPlayer)
            {
                _uid = LoginManager.Instance.GetWindowsPlatformLoginData().username;
                _device_type = LoginManager.Instance.GetWindowsPlatformLoginData().device_type;
                _fngid = LoginManager.Instance.GetWindowsPlatformLoginData().fngid;
            }
            else
            {
                _uid = WebLoginInfoManager.GetInstance().GetWebPlatformLoginData().username;
                _device_type = WebLoginInfoManager.GetInstance().GetWebPlatformLoginData().device_type;
                _fngid = WebLoginInfoManager.GetInstance().GetWebPlatformLoginData().fngid;
            }
#if UNITY_WEBPLAYER
            //string json = "{'cid':'','oid':'','aid':'','fnpid':'','fnppid':'','uid':'" + _uid + "','fngid':'1441964431398340'}";
            
            SetInitJson();
            string strJson = JsonMapper.ToJson(m_jsonInitValue);
            LoggerHelper.Info("UNITY_WEBPLAYER:" + strJson);
            //Ari m_pContext = DockingPlugin.CreateDocking(OnResponse, strJson);
#else
            //Ari m_pContext = DockingPlugin.CreateDocking(OnResponse, "../../qjphs/game/");
#endif
            LoggerHelper.Info("Intialize success!!!");
            return m_pContext != null;
        }

        public static void RoleLevelChangeLog()
        {
            //LoggerHelper.Info("RoleLevelChangeLog");
            
            //string json = "{\"eventname\":\"role_level_change\",\"eventkey\":\"level\",\"eventval\":\"" + PlayerAvatar.Player.level.ToString() + "\"uid\":\"" + uid + "\",\",\"server\":\"\",\"screen\":\"\"}";
            //Request((int)enumRequestID.RequestID_Log, "LogLevelUp", json);
            SendRoleLevelChangeLog(PlayerAvatar.Player.level.ToString());
        }

        public static bool Request(int nReqID, string szInfo1, string szInfo2, string szInfo3)
        {
            //LoggerHelper.Info("DockingService:" + nReqID + "--" + szInfo1 + "--" + szInfo2);
            if (IntPtr.Zero == m_pContext)
            {
                LoggerHelper.Info("not init");
                return false;
            }

            return DockingPlugin.Request(m_pContext, nReqID, szInfo1, szInfo2, szInfo3);
        }

#if UNITY_WEBPLAYER
        public static void OnResponse(int nRespID, int nRetCode, int nReqID, int nResponseType, string szInfo, uint len, string szInfo2)
        
#else 
        public static void OnResponse(int nRespID, int nRetCode, int nReqID, int nResponseType, IntPtr szInfo, uint len, string szInfo2)
#endif
        {
            //LoggerHelper.Info("OnResponse");
            CMessage pMsg = new CMessage();
            pMsg.nResponseID = nRespID;
            pMsg.nRequestID = nReqID;
            pMsg.nRetCode = nRetCode;
            pMsg.nResponseType = nResponseType;
#if UNITY_WEBPLAYER
            string str = szInfo;
#else
            string str = Marshal.PtrToStringAnsi(szInfo);
            
#endif
            byte[] buffer;
            if (len > 0)
            {
                buffer = new byte[(int)len];
#if UNITY_WEBPLAYER
                buffer = System.Text.Encoding.Default.GetBytes(szInfo);
#else
                Marshal.Copy(szInfo, buffer, 0, (int)len);
#endif
                
            }
            else
            {
                buffer = new byte[1];
            }
            //LoggerHelper.Info("OnResponse:" + nRespID + ":" + nRetCode + ":" + nReqID + ":" + nResponseType + ":" + szInfo2);
            pMsg.strInfo = buffer;
            pMsg.nLength = len;
            pMsg.strInfo2 = szInfo2;
            sMessageQueue.AppendMessage(pMsg);
        }

        public static void UpdateMes()
        {
            //LoggerHelper.Info("Update：" + sMessageQueue.m_dqMessageQueue.Count);
            if (sMessageQueue.m_dqMessageQueue.Count > 0)
            {
                sMessageQueue.RunMessages();
            }
        }

        public static void ExecuteMessage(CMessage pMsg)
        {
            OnMessage(pMsg);
        }

        public static void OnMessage(CMessage pMsg)
        {
            if (pMsg.nResponseID == (int)enumResponseID.ResponseID_Ready)
            {
                isReady = true;

            }
            else if (pMsg.nResponseID == (int)enumResponseID.ResponseID_Request)
            {
                if (pMsg.nRetCode != 0)
                {
                    LoggerHelper.Info("返回信息有误，错误码为：" + pMsg.nRetCode);
                }
                if (pMsg.strInfo2 == "GetCaptcha" && pMsg.nResponseType == (int)enumResponseType.ResponseType_Texture)
                {
                    FileStream fs = new FileStream("D://test.png", FileMode.Create);
                    fs.Write(pMsg.strInfo, 0, pMsg.strInfo.Length);
                    fs.Close();
                    LoggerHelper.Info("Write picture");
                }
                else
                {
                    string str = System.Text.Encoding.Default.GetString(pMsg.strInfo);
                    //Main.log += str + "\n";
                    if (pMsg.nRequestID == 1)
                    {
                        EventDispatcher.TriggerEvent<string>(WelfareActiveEvent.DOCKING_SHARE, str);
                    }
                    LoggerHelper.Info("ResponseID_Request callback:, id = " + pMsg.nRequestID + ", strInfo = " + str);
                }

            }
        }

        private static void SetInitJson()
        {
            WindowsWebPlatformLoginData webLoginDta = WebLoginInfoManager.GetInstance().GetWebPlatformLoginData();
             SetInitJsonValue("cid", webLoginDta.cid);
             SetInitJsonValue("oid", webLoginDta.oid);
             SetInitJsonValue("aid", webLoginDta.aid);
             SetInitJsonValue("fnpid", webLoginDta.fnpid);
             SetInitJsonValue("fnppid", webLoginDta.fnppid);
             SetInitJsonValue("uid", webLoginDta.username);
             SetInitJsonValue("fngid", webLoginDta.fngid);
        }

        private static void SetInitJsonValue(string key, string value)
        {
            m_jsonInitValue[key] = value;
        }

        public static void InitJsonValue()
        {
            m_jsonValue["eventname"] = "";
            m_jsonValue["eventkey"] = "";
            m_jsonValue["eventval"] = "";
            m_jsonValue["uid"] = "";
            m_jsonValue["server"] = "";
            m_jsonValue["screen"] = "";
        }

        public static void ClearJson()
        {
            m_jsonValue.Clear();
        }

        private static void SetJsonValue(string key, string value)
        {
            m_jsonValue[key] = value;
        }

        public static void SendOpenGameLog()
        {
            SetJsonValue("eventname", "open_game");
            SendLog();
        }

        public static void SendOpenLoginViewLog()
        {
            SetJsonValue("eventname", "open_login");
            SendLog();
        }

        public static void SendLoginedLog()
        {
            SetJsonValue("eventname", "logined");
            SendLog();
        }

        public static void SendSelectServerLog()
        {
            SetJsonValue("eventname", "select_server");
            SetJsonValue("server", LocalSetting.settingData.SelectedServerID.ToString());
            SetJsonValue("uid", _uid);
            SetJsonValue("fngid", _fngid);
            SendLog();
        }

        public static void SendCreateRoleLog(string eventVal)
        {
            SetJsonValue("eventname", "create_role");
            SetJsonValue("eventkey", "role");
            SetJsonValue("eventval", eventVal);
            SetJsonValue("uid", _uid);
            SetJsonValue("fngid", _fngid);
            SendLog();
        }

        public static void SendRoleLevelChangeLog(string eventVal)
        {
            SetJsonValue("eventname", "role_level_change");
            SetJsonValue("eventkey", "level");
            SetJsonValue("eventval", eventVal);
            SetJsonValue("uid", _uid);
            SetJsonValue("fngid", _fngid);
            SendLog();
        }

        public static void SendOnlineLog(string eventVal)
        {
            LoggerHelper.Info("SendOnlineLog");
            SetJsonValue("eventname", "send_online");
            SetJsonValue("eventkey", "onlne_status");
            SetJsonValue("eventval", eventVal);
            SetJsonValue("uid", _uid);
            SetJsonValue("fngid", _fngid);
            SendLog();
        }

        public static string GetMacAddress()
        {
            string mac = "";
            /*Ari
            NetworkInterface[] nis = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface ni in nis)
            {
                mac = ni.GetPhysicalAddress().ToString();
                if (!string.IsNullOrEmpty(mac))
                {
                    break;
                }
            }
            LoggerHelper.Info("mac:" + mac);
             * */
            return mac;
        }

        public static void GetShareData()
        {
            PlayerAvatar player = (PlayerAvatar)MogoWorld.Player;
            ServerInfo serverInfo = ServerListManager.GetServerInfoByID(LocalSetting.settingData.SelectedServerID);
            ClearJson();
            //必填
            SetJsonValue("roleId", player.dbid.ToString());
            SetJsonValue("roleName", player.name);
            SetJsonValue("uid", _uid);
            SetJsonValue("serverName", serverInfo.name);
            SetJsonValue("callback_info", "1");
            SetJsonValue("serverId", serverInfo.id.ToString());
            SetJsonValue("device_id", GetMacAddress());
            //选填
            SetJsonValue("appVersion", "");
            SetJsonValue("sdkVersion", "");
            SetJsonValue("device", "");
            SetJsonValue("osVersion", "");
            SetJsonValue("nm", "");
            SetJsonValue("mno", "");
            SetJsonValue("areaId", "");
            SetJsonValue("screen", "");
            SetJsonValue("client_id", _fngid);
            SetJsonValue("deviceType", _device_type);

            SendLog((int)enumRequestID.RequestID_GetData, "Share");
        }

        public static void SendShareEnterLog()
        {
            PlayerAvatar player = (PlayerAvatar)MogoWorld.Player;
            ServerInfo serverInfo = ServerListManager.GetServerInfoByID(LocalSetting.settingData.SelectedServerID);
            ClearJson();
            SetJsonValue("roleId", player.dbid.ToString());
            SetJsonValue("roleName", player.name);
            SetJsonValue("uid", _uid);
            SetJsonValue("serverName", serverInfo.name);
            SetJsonValue("serverId", serverInfo.id.ToString());
            SetJsonValue("roleLevel", player.level.ToString());
            SetJsonValue("gameId", _fngid);
            SetJsonValue("did", "1");
            SetJsonValue("appVersion", "");
            SetJsonValue("sdkVersion", "");
            SetJsonValue("device", "");
            SetJsonValue("osVersion", "");
            SetJsonValue("nm", "");
            SetJsonValue("mno", "");
            SetJsonValue("areaId", "");
            SetJsonValue("screen", "");
            SetJsonValue("deviceType", "");
            SetJsonValue("device_id", GetMacAddress());
            SendLog("ShareEnterGameLog");
        }

        public static void PostPicData(string imageData)
        {
            ClearJson();
            SetJsonValue("client_id", _fngid);
            SetJsonValue("use_type", "1");//1代表头像，2代表朋友圈图片，默认为1
            SetJsonValue("upload_type", "1");//1代表图片文件数据使用字符串形式， 2代表采用multipart/form-data 表单提交, 默认为1
            SetJsonValue("image_type", "1");//约定的图片格式，其中1代表原图， 2代表128x128		注意：原图不要超过1M， 原则上应该在客户端控制在600K以内；
            SetJsonValue("client_key", "615059b3e67a89d8e719fc67caf308ed");
            SendLog((int)enumRequestID.RequestID_PostData, "", imageData);
        }

        public static void CheckPicData()
        {
            ClearJson();
            SetJsonValue("url", @"http:\/\/f1.img4399.com\/syavatar~2016\/03\/10\/13_UKA_EP3EWu.248x248.png");
            SendLog((int)enumRequestID.RequestID_CheckPic, "");
        }

        /*private static void SendLog()
        {
            string strJson = JsonMapper.ToJson(m_jsonValue);
            DockingService.Request((int)enumRequestID.RequestID_Log, m_jsonValue["eventname"].ToString(), strJson);
        }*/

        private static void SendLog()
        {
            SendLog(m_jsonValue["eventname"].ToString());
        }

        private static void SendLog(string eventName)
        {
            SendLog((int)enumRequestID.RequestID_Log, eventName);
        }

        private static void SendLog(int reqID, string eventname, string szInfo = "")
        {            
            if (!isReady)
            {
                LoggerHelper.Info("Docking 还没有准备好");
                return;
            }
            string strJson = JsonMapper.ToJson(m_jsonValue);
            LoggerHelper.Info("reqID..." + reqID + ",eventname..." + eventname + "strJson..." + strJson);
            DockingService.Request(reqID, eventname, strJson, szInfo);
        }

        public static void DestroyDockingSocket()
        {
            //Ari DockingPlugin.DestroyDocking(m_pContext);
        }
    }
}

