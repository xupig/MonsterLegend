using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using GameMain.Entities;
using GameData;
using GameMain.CombatSystem;
namespace GameMain.GlobalManager
{
    public struct PerformaceDamageData
    { 
        public Vector3 position;
        public int damage;
        public AttackType type;
        public bool isPlayer;
    }
}