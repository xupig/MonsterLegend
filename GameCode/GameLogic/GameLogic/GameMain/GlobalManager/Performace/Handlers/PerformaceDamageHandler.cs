using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using GameMain.Entities;
using GameData;
using GameMain.CombatSystem;
using Common.Events;
using MogoEngine.Events;
using Common.Data;
namespace GameMain.GlobalManager
{
    public class PerformaceDamageHandler : PerformaceBaseHandler
    {
        Queue<PerformaceDamageData> _damageDataQueue;
        Dictionary<int, int> _qualityQueueMax;
        int _handleNumPerFrame = 3;
        int _handled = 0;
        public PerformaceDamageHandler()
        {
            _damageDataQueue = new Queue<PerformaceDamageData>();
            _qualityQueueMax = new Dictionary<int, int>();
            _qualityQueueMax[SettingData.RENDER_QUALITY_LOW] = 3;
            _qualityQueueMax[SettingData.RENDER_QUALITY_MIDDLE] = 4;
            _qualityQueueMax[SettingData.RENDER_QUALITY_HIGH] = 7;
        }

        override public void Update()
        {
            _handled = 0;
            while (_damageDataQueue.Count > 0)
            {
                var data = _damageDataQueue.Dequeue();
                EventDispatcher.TriggerEvent<Vector3, int, AttackType, bool>(
                    BattleUIEvents.CREATE_DAMAGE_NUMBER_FOR_ENTITY, data.position, data.damage, data.type, data.isPlayer);
                _handled++;
                if (_handled >= _handleNumPerFrame) break;
            }
        }

        public void CreateDamageNumber(Vector3 position, int damage, AttackType type, bool isPlayer)
        {
            var data = new PerformaceDamageData();
            data.position = position;
            data.damage = damage;
            data.type = type;
            data.isPlayer = isPlayer;
            _damageDataQueue.Enqueue(data);
            int max = _qualityQueueMax[GameRenderManager.curRenderQuality];
            if (_damageDataQueue.Count > max)
            {
                _damageDataQueue.Dequeue();
            }
        }
    }
}