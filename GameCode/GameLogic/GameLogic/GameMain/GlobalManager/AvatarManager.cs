using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using MogoEngine.RPC;
using GameMain.Entities;
using MogoEngine.Events;
using GameLoader.Utils.Timer;
using Common.Events;
using Common.States;
using GameData;
using Common.ServerConfig;
using Common.ClientConfig;
using GameLoader.Utils;
using MogoEngine.Mgrs;
using Common.Base;


namespace GameMain.GlobalManager
{

    public class AvatarManager
    {
        private static AvatarManager s_instance = null;
        public static AvatarManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new AvatarManager();
            }
            return s_instance;
        }

        Dictionary<string, uint> _avatarNameEntityIDMap = new Dictionary<string, uint>();
        private AvatarManager()
        {
            
        }

        public void OnAvatarEnterWorld(EntityAvatar avatar)
        {
            if (string.IsNullOrEmpty(avatar.name)) return;
            //LoggerHelper.Error("1____________avatar:    " + avatar + ",avatar.id:   " + avatar.id + ",avatar.name:  " + avatar.name);
            _avatarNameEntityIDMap[avatar.name] = avatar.id;
        }

        public void OnAvatarLeaveWorld(EntityAvatar avatar)
        {
            
        }

        public uint GetEntityIDByName(string name)
        {
            uint result = 0;
            if (_avatarNameEntityIDMap.ContainsKey(name))
            {
                result = _avatarNameEntityIDMap[name];
            }
            return result;
        }
    }

}