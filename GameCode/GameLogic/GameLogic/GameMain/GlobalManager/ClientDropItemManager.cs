﻿using System;
using System.Collections.Generic;
using System.Text;

using Common;
using Common.IManager;
using Common.Events;
using GameLoader.Utils;
using MogoEngine.Events;
using GameLoader.Utils.Timer;
using MogoEngine.Mgrs;
using UnityEngine;
using GameLoader.Config;

using Common.Base;
using MogoEngine.RPC;
using MogoEngine;
using GameMain.Entities;
using Common.ServerConfig;
using MogoEngine.Utils;
using Common.Structs.ProtoBuf;
using Common.ClientConfig;

namespace GameMain.GlobalManager
{
    class ClientDropItemManager
    {
        private static ClientDropItemManager s_Instance;
        public static ClientDropItemManager Instance
        {
            get
            {
                if (s_Instance == null)
                {
                    s_Instance = new ClientDropItemManager();
                }
                return s_Instance;
            }
        }

        private List<PbDropItemInfo> _curDropItemInfoList;
        private HashSet<int> _notDropItemIds;
        ClientDropItemManager()
        {
            EventDispatcher.AddEventListener<uint, int>(SpaceActionEvents.SpaceActionEvents_MonsterDie, OnDummyDie);
            InitNotDropItemIds();
        }

        void InitNotDropItemIds()
        {
            _notDropItemIds = new HashSet<int>();
            _notDropItemIds.Add(public_config.ITEM_SPECIAL_TYPE_EXP);
            _notDropItemIds.Add(public_config.ITEM_SPECIAL_TYPE_LEVEL_EXP);
            _notDropItemIds.Add(public_config.ITEM_SPECIAL_TYPE_PET_EXP);
        }

        public void OnDummyDie(uint entityId, int type)
        {
            var dummy = MogoWorld.GetEntity(entityId) as EntityDummy;
            if (type == InstanceDefine.DESTROY_SPAWN_COLLECT_MOSTER_TYPE) return;
            if (dummy != null && dummy.isNoDrop != 1)
            {
                DropItemRandom(dummy.position);
            }
            if (dummy.isServerDummy)
            {
                PlayerAvatar.Player.RpcCall("entity_action_req", entityId, action_config.ACTION_CLIENT_MISSION_INFO);
            }
        }

        public void SetDropItemList(PbDropItems data)
        {
            _curDropItemInfoList = data.drop_items;
            for (int i = _curDropItemInfoList.Count - 1; i >= 0; i--)
            {
                if (_notDropItemIds.Contains((int)_curDropItemInfoList[i].item_id))
                {
                    _curDropItemInfoList.RemoveAt(i);
                }
            }
        }

        public void ClearDropItemList()
        {
            _curDropItemInfoList = null;
        }

        public void DropItemRandom(Vector3 position, int count = 1)
        {
            for (int i = 0; i < count; i++)
            {
                if (_curDropItemInfoList == null || _curDropItemInfoList.Count == 0) return;
                var idx = UnityEngine.Random.Range(0, _curDropItemInfoList.Count - 1);
                var info = _curDropItemInfoList[idx];
                _curDropItemInfoList.RemoveAt(idx);
                position.x = position.x + UnityEngine.Random.Range(-1f, 1f); ;
                position.z = position.z + UnityEngine.Random.Range(-1f, 1f); ;
                ClientEntityManager.Instance.CreateDropItemByClient((int)info.item_id, (int)info.item_cnt, PlayerAvatar.Player.id, position);
            }
        }
    }
}
