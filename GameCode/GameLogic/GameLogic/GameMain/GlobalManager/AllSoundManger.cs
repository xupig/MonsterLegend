﻿using GameMain.Entities;
using MogoEngine;
using System;
using System.Collections.Generic;


namespace GameMain.GlobalManager
{
    public class AllSoundManger : Common.Utils.Singleton<AllSoundManger>
    {
        private bool _isTempLoseVolume = false;
        public bool isTempLoseVolume
        {
            get { return _isTempLoseVolume; }
            
        }

        public void TempCloseVolume()
        {
            BgMusicAudioSource.Instance.TempCloseVolume();
            SoundPool.GetInstance().TempCloseVolume();
            TempCloseEntityVolume();
            _isTempLoseVolume = true;
        }
        public void ResetTempVolume()
        {
            BgMusicAudioSource.Instance.ResetTempVolume();
            SoundPool.GetInstance().ResetTempVolume();
            ResetTempEntityVolume();
            _isTempLoseVolume = false;
        }

        private void TempCloseEntityVolume()
        {
            EntityCreature entity;
            foreach (var item in MogoWorld.Entities)
            {
                if (!(item.Value is EntityCreature)) continue;
                entity = (item.Value as EntityCreature);
                if (entity.actor == null) continue;
                if (entity.actor.soundController == null) continue;
                entity.actor.soundController.TempCloseVolume();
            }
        }

        private void ResetTempEntityVolume()
        {
            EntityCreature entity;
            foreach (var item in MogoWorld.Entities)
            {
                if (!(item.Value is EntityCreature)) continue;
                entity = (item.Value as EntityCreature);
                if (entity.actor == null) continue;
                if (entity.actor.soundController == null) continue;
                entity.actor.soundController.ResetTempVolume();
            }
        }

    }
}
