﻿using Common.Events;
using Common.Structs.ProtoBuf;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using ModuleMainUI;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class GuildDataManager
    {
        private Dictionary<ulong, PbGuildSimpleInfo> _guildDataDic;

        public GuildDataManager()
        {
            _guildDataDic = new Dictionary<ulong, PbGuildSimpleInfo>();
        }

        private static GuildDataManager s_instance = null;
        public static GuildDataManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new GuildDataManager();
            }
            return s_instance;
        }

        public void AddGuild(PbGuildSimpleInfo pbGuild)
        {
            pbGuild.guild_name = MogoProtoUtils.ParseByteArrToString(pbGuild.guild_name_bytes);
            _guildDataDic[pbGuild.guild_id] = pbGuild;
            EventDispatcher.TriggerEvent<ulong>(GuildEvents.Refresh_Guild_Simple_Info, pbGuild.guild_id);

        }

        public void RequestGuildDataById(ulong guildId)
        {
            if (guildId == 0) return;
            if (_guildDataDic.ContainsKey(guildId) == true)
            {
                EventDispatcher.TriggerEvent<ulong>(GuildEvents.Refresh_Guild_Simple_Info, guildId);
                return;
            }
            GuildManager.Instance.SearchGuildSimpleInfo(guildId);
            
        }

        public PbGuildSimpleInfo GetGuildSimpleInfo(ulong guildId)
        {
            if (_guildDataDic.ContainsKey(guildId) == true)
            {
                return _guildDataDic[guildId];
            }
            return null;
        }
    }
}
