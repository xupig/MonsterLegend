﻿#region 模块信息
/*==========================================
// 文件名：EditorTestKeyboardManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.KeyboardManager
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/1/4 20:44:32
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ExtendComponent;
using Common.Structs;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager;
using GameMain.GlobalManager.SubSystem;

using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;
public class EditorTestKeyboardManager
{

    /// <summary>
    /// 测试载具
    /// </summary>
    private void TestEntityVehicle()
    {
        if (Input.GetKeyUp(KeyCode.U) && Input.GetKey(KeyCode.LeftControl))
        {
            PlayerAvatar.Player.vehicleManager.RequestSummon();
            //GameMain.CombatSystem.EntityFilter.ShowDebugRange = !GameMain.CombatSystem.EntityFilter.ShowDebugRange;
            //LoggerHelper.Error("[KeyboardManager:TestEntityVehicle]=>30___________ShowDebugRange:    " + GameMain.CombatSystem.EntityFilter.ShowDebugRange);
        }

        if (Input.GetKeyUp(KeyCode.I) && Input.GetKey(KeyCode.LeftControl))
        {
            PlayerAvatar.Player.vehicleManager.RequestDivorce();
        }
    }

    public void CreateEntityVehicle(string[] args)
    {
        MogoEngine.MogoWorld.CreateEntity(GameMain.Entities.EntityConfig.ENTITY_TYPE_GROUND_VEHICLE, (entity) =>
        {
            var vehicle = entity as GameMain.Entities.EntityGroundVehicle;
            vehicle.monster_id = uint.Parse(args[0]);
            //dummy.GetTransform().position = PlayerAvatar.Player.position;
            vehicle.SetPosition(PlayerAvatar.Player.position);
            vehicle.SetRotation(Quaternion.Euler(0, 0, 0));
            vehicle.speed = 400;
            vehicle.cur_hp = vehicle.max_hp;
            //控制主角消失
        });
    }

    private void TestSort()
    {
        List<int> list = new List<int>() { 3,1,4,5,2,6};
        list.Sort(SortFunction);
        for(int i=0;i<list.Count;i++)
        {
            Debug.LogError(list[i]);
        }
    }

    private int SortFunction(int d1,int d2)
    {
        return d1 - d2;
    }

    public void FunctionControl()
    {
        if ((Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.Space)) && Input.GetKeyUp(KeyCode.Keypad0))
        {
            platform_helper.SetPlatformMode(PlatformMode.PC);
            LoggerHelper.Error(string.Format("设置操作平台成功，当前操作平台为：{0}", platform_helper.GetPlatformMode()));
        }

        if ((Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.Space)) && Input.GetKeyUp(KeyCode.Keypad1))
        {
            platform_helper.SetPlatformMode(PlatformMode.MOBILE);
            LoggerHelper.Error(string.Format("设置操作平台成功，当前操作平台为：{0}", platform_helper.GetPlatformMode()));
        }

        if ((Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.Space)) && Input.GetKeyUp(KeyCode.Keypad2))
        {
            platform_helper.SetPlatformMode(PlatformMode.EDITOR);
            LoggerHelper.Error(string.Format("设置操作平台成功，当前操作平台为：{0}", platform_helper.GetPlatformMode()));
        }

        if (Input.GetKey(KeyCode.C) && Input.GetKeyUp(KeyCode.P))
        {
            EventDispatcher.TriggerEvent(CloudEvents.CloseCloudPanel);
        }

        if(Input.GetKey(KeyCode.S) && Input.GetKeyUp(KeyCode.P))
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.CloudPanel);
        }

        if (Input.GetKeyUp(KeyCode.F5))
        {
            PlayerTimerManager.GetInstance().RefreshLocalTimeStamp();
            Debug.LogError("Refresh TimeStamp");
        }

        if (Input.GetKeyUp(KeyCode.F6))
        {
            SortOrderedRenderAgent.RebuildAll();
            Debug.LogError("Rebuild SortOrder");
        }

        if (Input.GetKeyUp(KeyCode.F9))
        {
            /*
            if (!string.IsNullOrEmpty(ModuleCGDialog.CGDialogPanel.dramaKey))
            {
                DramaManager.GetInstance().SkipDrama(ModuleCGDialog.CGDialogPanel.dramaKey);
            }*/
            //PanelIdEnum.GuideArrow.Show(new string[] { "4", "nomask", "LayerUIMain/Container_MainTownUI/Container_zuduirenwu/Container_content/Container_Task/ScrollView_renwu/mask/content/item_0", "Button", "95116" });
        }

        if (Input.GetKeyUp(KeyCode.F10))
        {
            UILayerManager.GetUILayerTransform(MogoUILayer.LayerGuide).gameObject.SetActive(false);
            //PanelIdEnum.SmallMap.Show();
            //PanelIdEnum.GuideArrow.Show(new string[] { "4", "nomask", "LayerUIMain/Container_MainTownUI/Container_zuduirenwu/Container_content/Container_Task/ScrollView_renwu/mask/content/item_0", "Button", "95116" });
            // ChatManager.Instance.SendClientChannelSystemMsg(public_config.CHANNEL_ID_WORLD, "争夺型传送门即将开始，快来看看呦，呀呀呀呀哈哈哈哈【去野外看看】", public_config.CHAT_MSG_TYPE_SYSTEM);
        }

        if (Input.GetKeyUp(KeyCode.F11))
        {
           // TaskManager.Instance.Print();
            Debug.LogError("公会战排名:" + PlayerDataManager.Instance.GuildData.MyGuildData.GuildWarRank);
            //TestSort();
            //PanelIdEnum.TeamMatch.Show();
            //PanelIdEnum.TeamEntryMainPanel.Show(PanelIdEnum.MainUIField);
           // PanelIdEnum.FingerGuide.Show(new string[] { "click", "400000000", "name" });
            //view_helper.OpenView(752);
            // PanelIdEnum.TeamTaskOfflineReward.Show();
            //isHang = !isHang;
            //PlayerDataManager.Instance.TaskData.isHangUp = isHang;
            //EventDispatcher.TriggerEvent(TaskEvent.TEAM_TASK_HANG_UP);
            //Debug.LogError(string.Format("炼金数据   assist_alchemy_cnt:{0},alchemy_cur_cnt:{1},alchemy_time_stamp:{2},alchemy_cnt:{3}", PlayerAvatar.Player.assist_alchemy_cnt, PlayerAvatar.Player.alchemy_cur_cnt, PlayerAvatar.Player.alchemy_time_stamp,PlayerAvatar.Player.alchemy_cnt));
            //SortOrderedRenderAgent.RebuildAll();
            //PanelIdEnum.GuideArrow.Show(new string[] { "4","drag","LayerUIMain/Container_MainBattleControlPanel/Container_bottomLeft/Container_wheel/Container_ControlStick/Image_ControlStick","StageImage","81042"});
            //PanelIdEnum.SpecialGuide.Show();
            //PanelIdEnum.Function.Show(1);
            //EventDispatcher.TriggerEvent(FunctionEvents.SET_FUNTION_POINT, 7,true);
            // PlayerDataManager.Instance.FunctionData.AddFunction(8);
            // PanelIdEnum.TradeMarket.Show();
            //PanelIdEnum.Interactive.Show();
            // PanelIdEnum.WanderLand.Show();
            //PanelIdEnum.NpcDailog.Show(1);
            //SystemInfoManager.Instance.Show(100001, MogoTimeUtil.ToUniversalTime(3600*15+21*60+15));
            //UIManager.Instance.ShowPanel(PanelIdEnum.TaskParticle, TaskParticleType.Accept);
            // PanelIdEnum.ItemObtain.Show(new List<PbItem>());
            //UIManager.Instance.ShowPanel(PanelIdEnum.TaskParticle, TaskParticleType.Finish);
            // PanelIdEnum.Token.Show(new TokenOpenDataWrapper(TokenType.SHI_LIAN_MI_JING));
            //FastNoticeData data = new FastNoticeData(SystemInfoUtil.GenerateContent(60317, null),system_info_helper.GetPanelViewId(60317));
            //UIManager.Instance.ShowPanel(PanelIdEnum.FastNotice, data);
            //EventDispatcher.TriggerEvent(TaskEvent.TASK_REACHED, 2);
            //EventDispatcher.TriggerEvent(ActivityEvents.SELECT_CHALLENGE, 3, "1", "95018");

            //
            //PanelIdEnum.Copy.Show();
           /// PanelIdEnum.GuideArrow.Show(new string[] { "4", "url", "LayerUIPanel/Container_CopyPanel/Container_progress/Button_fubenxuanze", "Button", "95116" });
            //PanelIdEnum.GuideArrow.Show(new string[] { "4", "nothidetimeout3000", "LayerUIMain/Container_MainFieldPanel/Container_common/Container_zuduirenwu/Container_content/Container_Task/List_content/item_0", "Container", "95116" });
            //PanelIdEnum.GuideArrow.Show(new string[] { "1", "url", "LayerUIMain/Container_MainFieldPanel/Container_common/Container_bottomLeft/Container_wheel/Container_ControlStick/Image_ControlStick", "Button", "95116" });
            //ChatManager.Instance.RequestSendChat(public_config.CHANNEL_ID_WORLD, "@get_time", 0, (byte)ChatContentType.Text, null);//(PlayerAvatar.Player.level + 1)
            //ChatManager.Instance.SendClientChannelSystemMsg(public_config.CHANNEL_ID_WORLD, "争夺型传送门即将开始，快来看看呦【去野外看看】");
            //string filterContent = ChatManager.Instance.FilterContent(ChatManager.Instance.voiceTestText);
            //UnityEngine.Debug.LogError("filter content = " + filterContent);
            //string content = "[哇哈哈我在10级野外晨星山谷(215,20,50)找到了宝箱,";
            //System.Text.RegularExpressions.Regex DESC_PATTERN = new System.Text.RegularExpressions.Regex(@"(?<=\[).*?(?=\,)");
            //UnityEngine.Debug.LogError("value = " + DESC_PATTERN.Match(content).Value);
            System.Text.RegularExpressions.Regex ENVELOPE_ID_PATTERN = new System.Text.RegularExpressions.Regex(@"(?<=\[.*?,\d{1,3},)\d{1,20}(?=\,)");
            string content = "[测试内容_风痕德里克,7,1012,70001,123456]";
            UnityEngine.Debug.LogError("value = " + ENVELOPE_ID_PATTERN.Match(content).Value);
        }

        if (Input.GetKeyUp(KeyCode.F12))
        {
            ReloadXmlManager.GetInstance().ReloadFromLocal();
            Debug.LogError("Reload Data");
        }

        if (Input.GetKeyUp(KeyCode.F1))
        {
            EventDispatcher.TriggerEvent(ChatEvents.CLOSE_CHAT_PANEL);
        }

        if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && Input.GetKeyUp(KeyCode.Keypad0))
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GM);
        }

        if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && Input.GetKeyUp(KeyCode.Keypad1))
        {
            string content = "异界宝箱活动已开启，各级野外地图将会出现异界宝箱，速速去开启吧！";
            SpotlightData data = new SpotlightData(content, 0, 1);
            UIManager.Instance.ShowPanel(PanelIdEnum.Spotlight, data);
        }

        if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && Input.GetKeyUp(KeyCode.Keypad2))
        {
            SystemInfoManager.Instance.Show(100018);
        }

        if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && Input.GetKeyUp(KeyCode.Keypad3))
        {
            DuelManager.Instance.RequestQueryInfo();
        }

        if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && Input.GetKeyUp(KeyCode.Keypad5))
        {
        }
    }
}