﻿using Common;
using Common.Base;
using Common.Utils;
using GameData;
using GameMain.Entities;
using UnityEngine;
namespace GameMain.GlobalManager
{
    public class KeyboardManagerUpdateDelegate : MogoEngine.UpdateDelegateBase
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public enum MouseClickMode
    {
        LoLMode = 0, // LOL操作模式 左键转镜头、右键移动
        WebPlayerMode = 1 // 页游操作方式 左键移动、右键转镜头
    }

    public enum SkillKeyMode
    {
        Number = 1,
        QWER = 2
    }

    public class KeyboardManager
    {
        private EditorTestKeyboardManager _editorTestKeyboardManager;
        private PCControlManager _pcControlManager;

        private static KeyboardManager s_instance = null;
        public static KeyboardManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new KeyboardManager();
            }
            return s_instance;
        }

        private KeyboardManager()
        {
            MogoEngine.MogoWorld.RegisterUpdate<KeyboardManagerUpdateDelegate>("KeyboardManager.Update", Update);
            _editorTestKeyboardManager = new EditorTestKeyboardManager();
            _pcControlManager = new PCControlManager();
        }

        public MouseClickMode MouseClickMode
        {
            get
            {
                int mouseMode = PlayerAvatar.Player.control_setting_chosen;
                if (mouseMode == 0)
                {
                    if (Application.platform == RuntimePlatform.WindowsPlayer || 
                        Application.platform == RuntimePlatform.WindowsEditor)
                    {
                        return MouseClickMode.LoLMode;
                    }
                    else
                    {
                        return MouseClickMode.WebPlayerMode;
                    }
                }
                else if (mouseMode == 1)
                {
                    return MouseClickMode.WebPlayerMode;
                }
                else
                {
                    return MouseClickMode.LoLMode;
                }
            }
        }

        private SkillKeyMode defaultWebPlayerKeyCode = SkillKeyMode.Number;
        private SkillKeyMode defaultLoLPlayerKeyCode = SkillKeyMode.Number;
        public SkillKeyMode SkillKeyMode
        {
            get
            {
                MouseClickMode mode = MouseClickMode;
                switch (mode)
                {
                    case MouseClickMode.WebPlayerMode:
                        if (PlayerAvatar.Player.control_setting_web_player == 0)
                        {
                            return defaultWebPlayerKeyCode;
                        }
                        else
                        {
                            return (SkillKeyMode)PlayerAvatar.Player.control_setting_web_player;
                        }
                    case MouseClickMode.LoLMode:
                        if (PlayerAvatar.Player.control_setting_lol_move == 0)
                        {
                            return defaultLoLPlayerKeyCode;
                        }
                        else
                        {
                            return (SkillKeyMode)PlayerAvatar.Player.control_setting_lol_move;
                        }
                }
                return SkillKeyMode.Number;
            }
        }

        public void Release()
        {
            MogoEngine.MogoWorld.UnregisterUpdate("KeyboardManager.Update", Update);
        }

        private void Update()
        {
            if (platform_helper.InPCPlatform() || platform_helper.InEditorPlatform())
            {
                _pcControlManager.PlayerKeyboardCtrl();
            }
            if (platform_helper.InEditorPlatform())
            {
                _editorTestKeyboardManager.FunctionControl();
            }
        }

        public int GetChosenMode()
        {
            return 1 - (int)MouseClickMode;
        }

        private const int defaultChosenControlMode = 1;
        private int GetControlMode()
        {
            int mode = PlayerAvatar.Player.control_setting_chosen;
            if (mode == 0)
            {
                return defaultChosenControlMode;
            }
            else
            {
                return mode;
            }
        }

        private const string modeOneString = "1、2、3、4";
        private const string modeTwoString = "Q、W、E、R";
        public string GetWebPlayerControlMode()
        {
            int webMode = PlayerAvatar.Player.control_setting_web_player;
            if (webMode == 0)
            {
                return modeOneString;
            }
            if (webMode == 1)
            {
                return modeOneString;
            }
            else
            {
                return modeTwoString;
            }
        }

        public string GetLOLPlayerControlMode()
        {
            int lolMode = PlayerAvatar.Player.control_setting_lol_move;
            if (lolMode == 0)
            {
                return modeOneString;
            }
            else if (lolMode == 1)
            {
                return modeOneString;
            }
            else
            {
                return modeTwoString;
            }
        }

        public string GetCurrentSkillString()
        {
            switch (MouseClickMode)
            {
                case MouseClickMode.WebPlayerMode:
                    return GetWebPlayerControlMode();
                case MouseClickMode.LoLMode:
                    return GetLOLPlayerControlMode();
            }
            return modeOneString;
        }
    }
}