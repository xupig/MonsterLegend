﻿#region 模块信息
/*==========================================
// 文件名：PCControlManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.KeyboardManager
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/1/4 20:44:48
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Events;
using Common.States;
using Game.UI.UIComponent;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class PCControlManager
{
    private float rightSign = 0;
    private float upSign = 0;
    private List<bool> _skillKeyStateList = new List<bool>();

    private const int SKILL_COUNT = 4;

    public PCControlManager()
    {
        for (int i = 0; i < SKILL_COUNT; ++i)
        {
            _skillKeyStateList.Add(false);
        }
    }

    public void PlayerKeyboardCtrl()
    {
        if (GMState.keyboardCtrl 
            && PlayerAvatar.Player != null
            && JudgeFocusOnInputField() == false
            && DramaManager.GetInstance().IsInViewingMode() == false)
        {
            PlayerSkillControl();
            PlayerMoveControl();
        }
    }

    private GameObject hasSelectedGameObject;
    private bool hasInputFieldComponent = false;
    private bool JudgeFocusOnInputField()
    {
        GameObject currentSelectedGO = EventSystem.current.currentSelectedGameObject;
        if (currentSelectedGO != null)
        {
            if(hasSelectedGameObject != currentSelectedGO)
            {
                hasSelectedGameObject = currentSelectedGO;
                hasInputFieldComponent = hasSelectedGameObject.GetComponent<KInputField>();
            }
            return hasInputFieldComponent;
        }
        return false;
    }

    private void PlayerMoveControl()
    {
        // 当玩家操作遥感时，按键默认无效
        if (ControlStickState.isDragging == true)
        {
            return;
        }
        upSign = Input.GetAxis("Vertical");
        rightSign = Input.GetAxis("Horizontal");
        ControlStickState.direction.x = rightSign * 100;
        ControlStickState.direction.y = upSign * 100;
        ControlStickState.strength = ControlStickState.direction.magnitude / 100;
    }

    private void PlayerSkillControl()
    {
        HandleKey(KeyCode.Q, KeyCode.Alpha1, SkillData.SKILL_POS_ONE);
        HandleKey(KeyCode.W, KeyCode.Alpha2, SkillData.SKILL_POS_TWO);
        HandleKey(KeyCode.E, KeyCode.Alpha3, SkillData.SKILL_POS_THREE);
        HandleKey(KeyCode.R, KeyCode.Alpha4, SkillData.SKILL_POS_FOUR);
    }

    private void HandleKey(KeyCode keyCode1, KeyCode keyCode2, int skillPos)
    {
        KeyCode validKey = KeyboardManager.GetInstance().SkillKeyMode == SkillKeyMode.QWER ? keyCode1 : keyCode2;
        if (Input.GetKeyDown(validKey))
        {
            UpdateKeyState(validKey);
            skill_play_helper.PlaySkill(skillPos);
        }
        else if(Input.GetKey(validKey))
        {
            if (CheckCanTrigger(validKey))
            {
                skill_play_helper.PlaySkill(skillPos);
            }
        }
    }

    private bool CheckCanTrigger(KeyCode validKey)
    {
        if (keyCodeDict.ContainsKey(validKey) && Time.realtimeSinceStartup > keyCodeDict[validKey] + 0.2f)
        {
            return true;
        }
        return false;
    }

    private Dictionary<KeyCode, float> keyCodeDict = new Dictionary<KeyCode, float>();
    private void UpdateKeyState(KeyCode validKey)
    {
        if (keyCodeDict.ContainsKey(validKey))
        {
            keyCodeDict[validKey] = Time.realtimeSinceStartup;
        }
        else
        {
            keyCodeDict.Add(validKey, Time.realtimeSinceStartup);
        }
    }
}