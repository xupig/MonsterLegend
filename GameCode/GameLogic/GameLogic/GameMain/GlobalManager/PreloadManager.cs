﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;

using ACTSystem;
using MogoEngine.Core;
using Common.ServerConfig;
using Common.Base;
using Game.Asset;
using MogoEngine;
using MogoEngine.Events;
using Common.Events;
using GameData;
using GameMain.Entities;
using Common.ClientConfig;
using SpaceSystem;

namespace GameMain.GlobalManager
{
    public class PreloadManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }
    //游戏预加载管理器
    //针对某些特定场合对资源进行预加载
    public class PreloadManager
    {
        private static PreloadManager s_instance = null;
        public static PreloadManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new PreloadManager();
            }
            return s_instance;
        }

        List<PreloadStrategyBase> _preloadStrategys = new List<PreloadStrategyBase>();
        PreloadManager()
        {
            if (Application.isEditor) InspectorSetting.InspectingTool.Inspect(new Inspectors.InspectingObjectPool(ObjectPool.Instance), GameObject.Find("_ObjectPoolProxy"));
        }

        public void OnPlayerEnterWorld()
        {
            MogoWorld.RegisterUpdate<PreloadManagerUpdateDelegate>("PreloadManager.Update", Update);
            EventDispatcher.AddEventListener<int>(SceneEvents.ENTER_MAP, OnPlayerEnterMap);
            EventDispatcher.AddEventListener<int>(SceneEvents.LEAVE_SCENE, OnPlayerLeaveMap);
            PreloadForPlayer();
        }

        public void OnPlayerLeaveWorld()
        {
            MogoWorld.UnregisterUpdate("PreloadManager.Update", Update);
            EventDispatcher.RemoveEventListener<int>(SceneEvents.LEAVE_SCENE, OnPlayerLeaveMap);
            EventDispatcher.RemoveEventListener<int>(SceneEvents.ENTER_MAP, OnPlayerEnterMap);
        }

        void PreloadForPlayer()
        {
            try
            {
                var paths = ACTResource.GetVisualFXResourcePaths(InstanceDefine.LEVEL_UP_VISUAL_FX_ID);
                ObjectPool.Instance.DontDestroy(paths.ToArray());
                ObjectPool.Instance.PreloadAsset(paths.ToArray());
                paths = ACTResource.GetVisualFXResourcePaths(InstanceDefine.BORN_VISUAL_FX_ID);
                ObjectPool.Instance.DontDestroy(paths.ToArray());
                ObjectPool.Instance.PreloadAsset(paths.ToArray());
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message);
            }
        }

        void ClearPreloadStrategys()
        {
            for (int i = 0; i < _preloadStrategys.Count; i++)
            {
                _preloadStrategys[i].Stop();
            }
            _preloadStrategys.Clear();
        }

        void OnPlayerEnterMap(int mapID)
        {
            int mapType = map_helper.GetMapType(mapID);
            switch (mapType)
            {
                case public_config.MAP_TYPE_CITY:
                    break;
                case public_config.MAP_TYPE_WILD:
                    AddStrategy(new PreloadStrategyWild(mapID));
                    break;
                default:
                    AddStrategy(new PreloadStrategyCombat(mapID));
                    break;
            }
        }

        void OnPlayerLeaveMap(int mapID)
        {
            ClearPreloadStrategys();
        }

        void OnPlayerEnterLogin()
        {
            List<string> paths = new List<string>();
            AddPreloadUIPaths(paths);
            ObjectPool.Instance.PreloadAsset(paths.ToArray());
        }

        void AddStrategy(PreloadStrategyBase strategy)
        {
            _preloadStrategys.Add(strategy);
        }

        int _spaceTime = 0;
        int DEFAULT_SPACE_TIME = 5; //加载间隔时间
        void Update()
        {
            if (_spaceTime > 0) { _spaceTime--; return; }
            _spaceTime = DEFAULT_SPACE_TIME;
            if (_preloadStrategys.Count == 0) return;
            if (!_preloadStrategys[0].Update())
            {
                _preloadStrategys[0].Stop();
                _preloadStrategys.RemoveAt(0);
            }
        }

        public void BeforeEnterScene(Action callback)
        {
            List<string> paths = new List<string>();
            PreloadSettingData();
            if (GameSceneManager.GetInstance().curMapID != 0)
            {
                AddPreloadUIPaths(paths);
            }
            ObjectPool.Instance.DontDestroy(paths.ToArray());
            AddPreloadSpaceResourcePaths(paths);
            //AddPreloadSoundPaths(paths);
            ObjectPool.Instance.PreloadAsset(paths.ToArray(), callback);
            /*string temp = "BeforeEnterScene: \n";
            for (int i = 0; i < paths.Count; i++)
            {
                temp += paths[i] + "\n";
            }
            Debug.LogError(temp);*/
        }

        void PreloadSettingData()
        {
            var monster = XMLManager.monster;
            var monster_value = XMLManager.monster_value;
            var spell = XMLManager.spell;
            var spell_action = XMLManager.spell_action;
            var buff = XMLManager.buff;
            var item_equiment = XMLManager.item_equipment;
            var equip_value = XMLManager.equip_value;
        }

        void AddPreloadUIPaths(List<string> paths)
        {
            var curMapType = GameSceneManager.GetInstance().curMapType;
            switch (curMapType)
            {
                case public_config.MAP_TYPE_CITY:
                    paths.Add(UIManager.Instance.GetPanelPrefabPath(PanelIdEnum.MainUIField));
                    break;
                case public_config.MAP_TYPE_WILD:
                    paths.Add(UIManager.Instance.GetPanelPrefabPath(PanelIdEnum.MainUIField));
                    break;
                default:
                    paths.Add(UIManager.Instance.GetPanelPrefabPath(PanelIdEnum.MainUIBattle));
                    break;
            }
            paths.Add(CameraManager.MAIN_CAMERA_PREFAB);
            paths.Add(UIManager.Instance.GetPanelPrefabPath(PanelIdEnum.CGDialog));
            paths.Add(UIManager.Instance.GetPanelPrefabPath(PanelIdEnum.BattleElement));
            paths.Add(UIManager.Instance.GetPanelPrefabPath(PanelIdEnum.GuideArrow));
            paths.Add(UIManager.Instance.GetPanelPrefabPath(PanelIdEnum.CloudPanel));
            paths.Add(UIManager.Instance.GetPanelPrefabPath(PanelIdEnum.SceneChange));
        }

        void AddPreloadSpaceResourcePaths(List<string> paths)
        {
            var spaceData = GameSceneManager.GetInstance().curSpaceData;
            var listData = spaceData.entities.entityList;
            EntitySetStateData beginData = null;
            for (int i = 0; i < listData.Count; i++)
            {
                var data = listData[i];
                var type = data.type;
                if (type == EntityType.SET_BEGIN_STATE_ACTION)
                {
                    beginData = data as EntitySetStateData;
                }
            }
            if (beginData != null && !string.IsNullOrEmpty(beginData.next_actions_l))
            {
                var actionIDList = ActionEntitiesUtils.ParseListInt(beginData.next_actions_l);
                for (int i = 0; i < actionIDList.Count; i++)
                {
                    AddEntityActionPaths(paths, actionIDList[i]);
                }
            }
        }

        void AddPreloadSoundPaths(List<string> paths)
        {
            int curMapID = GameSceneManager.GetInstance().curMapID;
            music musicData = music_helper.GetInstance().GetBgMusicData(curMapID);
            if (musicData != null && !string.IsNullOrEmpty(musicData.__path)) { paths.Add(musicData.__path); }
        }

        void AddEntityActionPaths(List<string> paths, int actionID)
        {
            var spaceData = GameSceneManager.GetInstance().curSpaceData;
            var entityDic = spaceData.entities.entityDic;
            if (!entityDic.ContainsKey(actionID)) return;
            var data = entityDic[actionID];
            //if (data.type == EntityType.PLAY_CG_ACTION)
            //{
            //    var playCGActionData = data as EntityPlayCGActionData;
            //    DramaResource.GetDramaPreloadResource(paths, playCGActionData.cg_event_name, playCGActionData.cg_event_value);
            //}
        }

        public void AfterEnterScene()
        {
            
        }
    }

}
