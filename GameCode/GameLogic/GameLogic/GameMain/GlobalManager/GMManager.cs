#region ????????
/*==========================================
// ????????GMManager
// ????????: GameMain.GlobalManager
// ????????LYX
// ????????????
// ??????????2014/12/08
// ??????????GM????????
// ??????RPC: {"chat_req"}
//==========================================*/
#endregion

using ACTSystem;
using Common;
using Common.Base;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.States;
using Common.Structs;
using Common.Utils;
using Game.Asset;
using Game.UI;
using GameData;
using GameLoader.Config;
using GameLoader.Utils;
using GameLoader.Utils.Network;
using GameLoader.Utils.Timer;
using GameMain.CombatSystem;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using Mogo.AI;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.RPC;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public class GMManager
    {
        private static PlayerAvatar m_playerAvatar;

        private static GMManager s_instance = null;
        public static GMManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new GMManager();
            }
            return s_instance;
        }

        static readonly string s_ChatReq = "chat_req";

        private GMManager()
        {
            m_playerAvatar = (MogoWorld.Player as PlayerAvatar);
        }

        public void OnPlayerEnterWorld()
        {

        }

        public void OnPlayerLeaveWorld()
        {

        }

        public void ProcessCommand(string cmd)
        {
            if (cmd.EndsWith("|"))
            {
                cmd = cmd.Substring(0, cmd.Length - 1);
            }
            //cmd = cmd.ToLower();
            string[] cmds = cmd.Split(' ');
            var cmdMethod = this.GetType().GetMethod(cmds[0].Replace("@", ""));
            this.ExcuteMethod(cmds, cmdMethod, cmd);
        }

        private void ExcuteMethod(string[] cmds, System.Reflection.MethodInfo cmdMethod, string content)
        {
            if (cmdMethod != null)
            {
                LoggerHelper.Debug("GM cmds:" + cmds);
                cmdMethod.Invoke(this, cmds);
            }
            else
            {
                //????????????????????  todo
                //m_playerAvatar.RpcCall(s_ChatReq, 1, content, 0);
            }
        }

        public bool CheckHasCommand(string content)
        {
            int commandTagIndex = content.IndexOf("@");
            return commandTagIndex != -1 && commandTagIndex == 0;
        }

        #region GM???????????? ??????????

		public void rpc(string cmd, string rpcName, string rpcArgs)
        {
            ProtocolTestManager.GetInstance().RPCCall(rpcName, rpcArgs);
        }

        public void print_log(string cmd, string param)
        {
            BaseModule.PrintLog();
        }

        public void reloadxml(string cmd)
        {
            LoggerHelper.Debug("reloadxml");
            GlobalManager.ReloadXmlManager.GetInstance().ReloadFromLocal();
        }

        public void task_accept(string cmd, string param)
        {
            TaskManager.Instance.Clear();
        }

        public void task_accomplish(string cmd, string param)
        {
            TaskManager.Instance.Clear();
        }

        public void ai(string cmd, string param1)
        {
            EntityAI.aiState = param1 == "0" ? false : true;
        }

        public void feilong(string cmd)
        {
            EventDispatcher.TriggerEvent(DreamlandEvents.ShowScene);
        }

        public void nettimeout(string cmd, string param1)
        {
            ServerProxy.Instance.enableTimeout(false);
        }

        public void visible(string cmd, string param1)
        {
            PlayerAvatar.Player.actor.visible = param1 == "0" ? false : true;
        }

        public void showfightforce(string cmd)
        {
            ChatManager.Instance.SendClientChatMsg(public_config.CHANNEL_ID_WORLD, FightTipsManager.Instance.GetFightForceHistory());
        }

        public void showcopystar(string cmd)
        {
            string starmsg = string.Empty;
            foreach (KeyValuePair<int, int> kvp in PlayerDataManager.Instance.PlayingCopyStarData.ActionIdToStateDic)
            {
                starmsg += string.Format(",{0}:{1}", kvp.Key, kvp.Value);
            }
            ChatManager.Instance.SendClientChatMsg(public_config.CHANNEL_ID_WORLD, starmsg);
        }

        public void sendmail(string cmd, string param1, string param2, string param3)
        {
            MailInfo _mailInfo = new MailInfo();
            _mailInfo.title = "GM";
            _mailInfo.avatar_dbid = ulong.Parse(param1);
            _mailInfo.text = param2;
            _mailInfo.from = "GM";
            _mailInfo.mail_type = (uint)public_config.MAIL_TYPE_PLAYER;
            _mailInfo.time = 0;
            _mailInfo.attachment = new GameLoader.Utils.CustomType.LuaTable();
            for (int i = 0; i < int.Parse(param3); i++)
            {
                MailManager.Instance.RequestSendMail(_mailInfo);
            }
        }

        public void showassigninfo(string cmd, string param1)
        {
            EquipItemInfo equipItemInfo = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemInfo(int.Parse(param1)) as EquipItemInfo;
            uint canAssignTime = 60 * 60 * uint.Parse(global_params_helper.GetGlobalParam(GlobalParamId.transfer_item_to_player_time));
            uint spanTime = Global.serverTimeStampSecond > equipItemInfo.GetTime ? Global.serverTimeStampSecond - equipItemInfo.GetTime : 0;
            string content = string.Format("??????????{0},????????????{1},??????????????????????{2}", MogoTimeUtil.ServerTimeStamp2UtcTime(equipItemInfo.GetTime).ToString(), canAssignTime, spanTime);
            LoggerHelper.Error(content);
        }

        public void set_client_charge_check_state(string cmd, string param1)
        {
            ChargeManager.IgnoreClientChargeCheck = param1 == "0";
        }

        public void client_ignore_wing_train_vip_level(string cmd, string param1)
        {
            wing_helper.IsIgnoreVip = param1 == "0";
        }

        public void addwingss(string cmd, string param1)
        {
            WingManager.Instance.AddWingResp(byte.Parse(param1));
        }

        public void showpanel(string cmd, string param1)
        {
            int panelId = int.Parse(param1);
            PanelIdEnum panel = (PanelIdEnum)panelId;
            panel.Show();
        }

        //?????@?????????????????????????????????F??????
        private GameObject _testC = null;
        public void testitemgrid(string cmd)
        {
            if (_testC == null)
            {
                Transform parentTransform = UILayerManager.GetUILayerTransform(MogoUILayer.LayerUITop);
                _testC = new GameObject();
                _testC.AddComponent<RectTransform>();
                _testC.name = "_testC";
                _testC.transform.SetParent(parentTransform);
                _testC.SetActive(false);//?@?e?????????P?I?c???????????c???[?????B??
            }
            ObjectPool.Instance.GetGameObject("Atlas/Equip/warrior/000/0.prefab", delegate(GameObject gox)
            {
                for (int i = 0; i < 10; i++)
                {
                    ObjectPool.Instance.GetGameObject("Atlas/Equip/warrior/000/0.prefab", delegate(GameObject go)
                    {
                        var holder = new GameObject();
                        holder.name = "holder";
                        holder.AddComponent<RectTransform>();
                        holder.transform.SetParent(_testC.transform);

                        go.transform.SetParent(holder.transform);
                        go.SetActive(true);
                        //holder.SetActive(holder.activeInHierarchy);//?????@??????????????
                        holder.transform.SetParent(null);
                        GameObject.Destroy(holder);
                    });
                }
            });

        }

        public void resetshader(string cmd)
        {
            ObjectPool.Instance.GetGameObject("Atlas/Equip/warrior/000/0.prefab", delegate(GameObject go)
            {
                for (int i = 0; i < 2; i++)
                {
                    ImageWrapper image = go.GetComponent<ImageWrapper>();
                    string spriteKey = "Atlas$Equip$warrior$000$1501.u";
                    image.sprite = ObjectPool.Instance.GetAssemblyObject(spriteKey) as Sprite;
                    go.transform.SetParent(_testC.transform, false);
                    go.SetActive(true);
                }
            });
        }

        public void playaction(string cmd, string param1)
        {
            PlayerAvatar.Player.actor.animationController.PlayAction(int.Parse(param1));
        }

        public void set_wait_resp(string cmd)
        {
            PlayerAvatar.Player.isOpenWait = !PlayerAvatar.Player.isOpenWait;
        }

        public void disconnect(string cmd)
        {
            Debug.LogError("GM_DISCONNECT!");
            MogoEngine.RPC.ServerProxy.Instance.Disconnect();
        }

        public void KickOut(string cmd)
        {
            PlayerAvatar.Player.TickOut();
        }

        public void findpos(string cmd, string param1, string param2, string param3)
        {
            Vector3 position = new Vector3(float.Parse(param1), float.Parse(param2), float.Parse(param3));
            EventDispatcher.TriggerEvent(PlayerCommandEvents.FIND_POSITION, position);
        }

        public void show_range(string cmd)
        {
            EntityFilter.ShowDebugRange = !EntityFilter.ShowDebugRange;
        }

        public void blockmap(string cmd)
        {
            var h = PlayerAvatar.Player.position.y;
            Vector3 start = new Vector3(0, h, 0);
            Vector3 end = new Vector3(0, h, 0);
            var data = GameSceneManager.GetInstance().GetBlockMapData();
            for (int x = data.GetLowerBound(0); x <= data.GetUpperBound(0); x++)
            {
                for (int z = data.GetLowerBound(1); z <= data.GetUpperBound(1); z++)
                {
                    var subData = data[x, z];
                    if (subData > 0)
                    {
                        start.x = x;
                        start.z = z;
                        end.x = x + 0.5f;
                        end.z = z;
                        Debug.DrawLine(start, end, Color.blue, 10);
                        end.x = x - 0.5f;
                        end.z = z;
                        Debug.DrawLine(start, end, Color.blue, 10);
                        end.x = x;
                        end.z = z + 0.5f;
                        Debug.DrawLine(start, end, Color.blue, 10);
                        end.x = x;
                        end.z = z - 0.5f;
                        Debug.DrawLine(start, end, Color.blue, 10);
                    }
                }
            }
        }

        public void testdrop(string cmd, string param1)
        {
            LoggerHelper.Debug(string.Concat("", "testdrop", cmd));
            uint entityID = PlayerAvatar.Player.id;
            var x = PlayerAvatar.Player.position.x;
            var y = PlayerAvatar.Player.position.y;
            var z = PlayerAvatar.Player.position.z;
            int count = int.Parse(param1);
            for (int i = 0; i < count; i++)
            {
                var nx = x + UnityEngine.Random.Range(0f, 5f);
                var nz = z + UnityEngine.Random.Range(0f, 5f);
                ClientEntityManager.Instance.CreateDropItemByClient(1, 1, entityID, new Vector3(nx, y, nz));
            }
        }

        public void changeskill(string cmd, string param1, string param2)
        {

            List<int> learnedSkills = null;
            int vocation = int.Parse(param1);
            int idx = int.Parse(param2);
            if (vocation == Vocation.Warrior.ToInt())
            {
                if (idx == 1)
                {
                    learnedSkills = new List<int>() { 21, 41, 61, 81, 101, 121, 141, 161, 181, 201, 221 };
                }
                else
                {
                    learnedSkills = new List<int>() { 341, 361, 381, 401, 421, 441, 241, 261, 281, 301, 321 };
                }
            }
            else if (vocation == Vocation.Archer.ToInt())
            {
                if (idx == 1)
                {
                    learnedSkills = new List<int>() { 6001, 6021, 6041, 6061, 6081, 6101, 6121, 6141, 6161, 6181, 6201, 6221 };
                }
                else
                {
                    learnedSkills = new List<int>() { 6361, 6381, 6401, 6421, 6441, 6461, 6241, 6261, 6281, 6301, 6321, 6341 };
                }
            }
            else if (vocation == (int)Vocation.Wizard)
            {
                if (idx == 1)
                {
                    learnedSkills = new List<int>() { 4001, 4021, 4041, 4061, 4081, 4101,
                                                                    4121, 4141, 4161, 4181, 4201};
                }
                else
                {
                    learnedSkills = new List<int>() { 4001, 4021, 4041, 4061, 4081, 4101,
                                                                    4241, 4261, 4281, 4301};
                }
            }
            else if (vocation == (int)Vocation.Assassin)
            {
                if (idx == 1)
                {
                    learnedSkills = new List<int>() { 4001, 4021, 4041, 4061, 4081, 4101,
                                                                    4121, 4141, 4161, 4181, 4201};
                }
                else
                {
                    learnedSkills = new List<int>() { 4001, 4021, 4041, 4061, 4081, 4101,
                                                                    4241, 4261, 4281, 4301};
                }
            }
            PlayerAvatar.Player.skillManager.SetLearnedSkillList(learnedSkills);
        }

        public void testclient(string cmd)
        {
            LoggerHelper.Debug(string.Concat("", "testclient", cmd));
        }

        public void testclient1(string cmd, string param1)
        {
            LoggerHelper.Debug(string.Concat("", "testclient", cmd, param1));
        }

        public void testclient2(string cmd, string param1, string param2)
        {
            LoggerHelper.Debug(string.Concat("", "testclient", cmd, param1, param2));
        }

        public void setaction(string cmd, string param1)
        {
            GameMain.GlobalManager.GameSceneManager.GetInstance().CreateActions(param1, Vector3.zero);
        }

        public void countbox(string cmd)
        {
            Action action1 = () => { Debug.LogError("countbox::action1"); };
            string content = "????????????200??????????????";
            //ari MessageBoxCountdownWrapper wrapper = new MessageBoxCountdownWrapper(10, 1);
            //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, action1, null, null, null, wrapper);
        }

        public void changescreencamera(string cmd, string param1)
        {
            string[] str = param1.Split(',');
            int cameraType = System.Convert.ToInt32(str[0]);
            Vector3 targetPostion = new Vector3(System.Convert.ToInt32(str[1]), System.Convert.ToInt32(str[2]), System.Convert.ToInt32(str[3]));
            float moveTime = float.Parse(str[4]);
            CameraManager.GetInstance().SetScreenCamera(cameraType, targetPostion, moveTime);
        }

        public void changeplayercamera(string cmd, string param1)
        {
            string[] str = param1.Split(',');
            int cameraType = System.Convert.ToInt32(str[0]);
            float rotationX = float.Parse(str[1]);
            float rotationY = float.Parse(str[2]);
            float distance = float.Parse(str[3]);
            float speed = float.Parse(str[4]);
            float odds = float.Parse(str[5]);
            CameraManager.GetInstance().SetPlayerCamera(cameraType, rotationX, rotationY, distance, speed, odds);
        }

        public void changeshakecamera(string cmd, string param1)
        {
            string[] str = param1.Split(',');
            int id = System.Convert.ToInt32(str[0]);
            float time = float.Parse(str[1]);
            CameraManager.GetInstance().ShakeCamera(id, time);
        }

        public void changecamera(string cmd, string param1)
        {
            string[] str = param1.Split(',');
            int cameraType = System.Convert.ToInt32(str[0]);
            float rotationX = float.Parse(str[1]);
            float rotationY = float.Parse(str[2]);
            float rotateTime = float.Parse(str[3]);
            float distance = float.Parse(str[4]);
            Vector3 targetPostion = new Vector3(System.Convert.ToInt32(str[5]), System.Convert.ToInt32(str[6]), System.Convert.ToInt32(str[7]));
            float moveTime = float.Parse(str[8]);
            float speed = float.Parse(str[9]);
            float odds = float.Parse(str[10]);
            string concuss = str[11] + "," + str[12];
            //, float rotationX, float rotationY, float rotateTime, float distance, Vector3 targetPostion, float moveTime, float speed, float odds
            CameraManager.GetInstance().ChangeCamera(cameraType, rotationX, rotationY, rotateTime, distance, targetPostion, moveTime, speed, odds, concuss);
        }

        public void addpet(string cmd)
        {
            Debug.LogError(cmd);
            ChatManager.Instance.RequestSendChat(1, "@su admin", 0);
            ChatManager.Instance.RequestSendChat(1, "@add_item 28000 10", 0);
            PetManager.Instance.RequestCallPet(1);
            PetManager.Instance.RequestPetFight(1, 0);
        }

        public void enterscreen(string cmd, string param1)
        {
            int missionId = System.Convert.ToInt32(param1);
            MissionManager.Instance.RequsetEnterMission(missionId);
        }

        public void selectpart(string cmd, string param1)
        {
            var enumerator = MogoWorld.Entities.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Current.Value is EntityDummy)
                {
                    EntityDummy entity = enumerator.Current.Value as EntityDummy;
                    entity.partManager.SelectPart(int.Parse(param1));
                }
            }
        }

        public void addequip(string cmd, string param1)
        {
            string[] paramArr = param1.Split(',');
            PlayerAvatar.Player.equipManager.TestChangeCloth(int.Parse(paramArr[0]), int.Parse(paramArr[1]), int.Parse(paramArr[2]));
        }

        public void addweapon(string cmd, string param1)
        {
            string[] paramArr = param1.Split(',');
            PlayerAvatar.Player.equipManager.TestChangeWeapon(int.Parse(paramArr[0]), int.Parse(paramArr[1]), int.Parse(paramArr[2]));
        }

        public void addwing(string cmd, string param1)
        {
            string[] paramArr = param1.Split(',');
            PlayerAvatar.Player.equipManager.TestChangeWing(int.Parse(paramArr[0]), int.Parse(paramArr[1]), int.Parse(paramArr[2]));
        }

        public void addbuff(string cmd, string param1)
        {
            string[] paramArr = param1.Split(',');
            PlayerAvatar.Player.bufferManager.AddBuffer(int.Parse(paramArr[0]), float.Parse(paramArr[1]), true);
        }

        public void adddummy(string cmd, string param1)
        {
            string[] paramArr = param1.Split(',');
            MogoWorld.CreateEntity(EntityConfig.ENTITY_TYPE_NAME_DUMMY, (entity) =>
            {
                var monster = entity as EntityMonsterBase;
                monster.monster_id = uint.Parse(paramArr[0]);
                //entity.Teleport(new Vector3(float.Parse(paramArr[1]), float.Parse(paramArr[2]), float.Parse(paramArr[3])));
                monster.aiID = -1;
            });
        }

        public void showgm(string cmd)
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.GM);
        }

        public void unloadunused(string cmd)
        {
            Debug.LogError("UnloadUnusedAssets");
            Resources.UnloadUnusedAssets();
        }

        public void clearmemory(string cmd)
        {
            Debug.LogError("un");
            Resources.UnloadUnusedAssets();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            ChatManager.Instance.SendClientChatMsg(public_config.CHANNEL_ID_WORLD,
                "un!!" + PlayerTimerManager.GetInstance().GetNowServerDateTime());
        }

        public void staticbatching(string cmd, string param1)
        {
            GMState.staticBatching = param1 == "0" ? false : true;
        }

        public void keyboardctrl(string cmd, string param1)
        {
            GMState.keyboardCtrl = param1 == "0" ? false : true;
        }

        public void enterspace(string cmd, string param1)
        {
            PlayerAvatar.Player.OnEnterSpaceResp(uint.Parse(param1), 1, 0, 0, 0);
        }

        public void fps(string cmd)
        {
            if (LoaderDriver.Instance.gameObject.GetComponent<FPSCounter>() == null)
            {
                LoaderDriver.Instance.gameObject.AddComponent<FPSCounter>();
            }
        }

        public void printfps(string cmd)
        {
            fps("");
            FPSCounter.printFPS = true;
        }

        public void shelter(string cmd)
        {
            PlayerAvatar.Player.SetShelter();
        }

        public void good(string cmd)
        {
            PlayerAvatarState.isPowerfully = true;
            PlayerAvatarState.isWeakly = false;
        }

        public void bad(string cmd)
        {
            PlayerAvatarState.isWeakly = true;
            PlayerAvatarState.isPowerfully = false;
        }

        public void sblood(string cmd, string param1)
        {
            GMState.showBlood = param1 == "0" ? false : true;
        }

        public void sheart(string cmd, string param1)
        {
            GMState.showheartbeatinfo = param1 == "0" ? false : true;
        }

        public void slifebar(string cmd, string param1)
        {
            GMState.showLifeBar = param1 == "0" ? false : true;
        }

        public void sname(string cmd, string param1)
        {
            GMState.showName = param1 == "0" ? false : true;
        }

        public void sspeech(string cmd, string param1)
        {
            GMState.showSpeech = param1 == "0" ? false : true;
        }

        public void shead(string cmd, string param1)
        {
            sblood(cmd, param1);
            slifebar(cmd, param1);
            sname(cmd, param1);
            sspeech(cmd, param1);
        }

        public void smainchat(string cmd, string param1)
        {
            GMState.openMainChatAnim = param1 == "1" ? true : false;
        }

        public void bgdpause(string cmd, string param1)
        {
            BackgroundDownloadManager.GetInstance().isPause = param1 == "1" ? true : false;
        }

        public void saveloaded(string cmd, string param1)
        {
            Dictionary<string, int> assetLoadedCountSet = ObjectPool.Instance.GetAssetLoadedCountSet();
            string content = FormatDictString(assetLoadedCountSet);
            //string content = LitJson.JsonMapper.ToJson(assetLoadedCountSet);
            var targetPath = Path.Combine(SystemConfig.RuntimeResourcePath, param1 + ".json");
            LoggerHelper.Error("saveloaded:" + targetPath);
            if (File.Exists(targetPath) == true)
            {
                File.Delete(targetPath);
            }
            using (FileStream fs = File.OpenWrite(targetPath))
            {
                char[] charData = content.ToCharArray();
                byte[] byteData = new byte[charData.Length];
                System.Text.Encoder encoder = System.Text.Encoding.UTF8.GetEncoder();
                encoder.GetBytes(charData, 0, charData.Length, byteData, 0, true);
                fs.Write(byteData, 0, byteData.Length);
            }
        }

        public static string[] GetSortedArray(ICollection collection)
        {
            string[] result = new string[collection.Count];
            collection.CopyTo(result, 0);
            Array.Sort<string>(result);
            return result;
        }

        //TODO:Log??????????????????
        public static string FormatDictString(Dictionary<string, int> data)
        {
            StringBuilder result = new StringBuilder(500000);
            result.Append("{\r\n");
            string[] memoryDataArray = GetSortedArray(data.Keys);
            for (int i = 0; i < memoryDataArray.Length; i++)
            {
                string key = memoryDataArray[i];
                result.AppendFormat("\t\"{0}\":{1},\r\n", key, data[key]);
            }
            result.Remove(result.Length - 3, 3);
            result.Append("\r\n}");
            return result.ToString();
        }

        public void testfile(string cmd, string param1)
        {
            //Debug.LogError("testfile1:" + Time.realtimeSinceStartup);
            float begin = Time.realtimeSinceStartup;
            var targetPath = Path.Combine(SystemConfig.RuntimeResourcePath, "test1111");
            int count = int.Parse(param1);
            for (int i = 0; i < count; i++)
            {
                File.Exists(targetPath);
            }
            Debug.LogError("count and time:" + count + ":" + (Time.realtimeSinceStartup - begin));
        }


        public void gotologin(string cmd)
        {
            GameStateManager.GetInstance().SetState(GameDefine.STATE_LOGIN);
        }

        public void findmonster(string cmd, string param1, string param2)
        {
            int insID = int.Parse(param1);
            int monsterID = int.Parse(param2);
            EventDispatcher.TriggerEvent(TaskEvent.FIND_MONSTER, insID, monsterID, true);
        }

        public void findnpc(string cmd, string param1)
        {
            int npcID = int.Parse(param1);
            EventDispatcher.TriggerEvent(PlayerCommandEvents.FIND_NPC, npcID, true);
        }

        public void findpath(string cmd, string param1, string param2, string param3, string param4)
        {
            //int x1 = int.Parse(param1);
            //int y1 = int.Parse(param2);
            //int x2 = int.Parse(param3);
            //int y2 = int.Parse(param4);
            //var path = GameSceneManager.GetInstance().GetPath(x1, y1, x2, y2);
        }

        public void beginsimple(string cmd, string param1)
        {
            SystemConfig.canProfilerBeginSample = param1 == "0" ? false : true;
            Debug.LogError("Profiler.enabled =" + Profiler.enabled);
            Profiler.enabled = param1 == "0" ? false : true;
        }

        public void setpetdie(string cmd)
        {
            uint curPetEntID = PlayerPetManager.GetInstance().curPetEntityID;
            if (curPetEntID == 0)
            {
                return;
            }
            EntityCreature pet = MogoWorld.GetEntity(curPetEntID) as EntityCreature;
            if (pet.entityAI != null)
            {
                pet.entityAI.blackBoard.aiState = AIState.CD_STATE;
                pet.entityAI.Destroy();
            }
            pet.cur_hp = 0;
            pet.stateManager.SetState(state_config.AVATAR_STATE_DEATH, true);
        }

        public void showcgdialog(string cmd, string param1)
        {
            string entityType = "NPC";
            string content = param1 + "," + param1;
            float duration = 30000f;
            bool isWaitingEvent = false;
            Common.Data.CGDialogData data = new Common.Data.CGDialogData("aa1", entityType, content, duration, isWaitingEvent);
            UIManager.Instance.ShowPanel(PanelIdEnum.CGDialog, data);
        }

        public void pass(string cmd)
        {
            CombatScene scene = GameSceneManager.GetInstance().scene as CombatScene;
            MissionManager.Instance.RequsetLeaveMission(scene.GetBattleResultData());
        }

        public void creategobj(string cmd)
        {
            ObjectPool.Instance.GetGameObject("Fx/fx_prefab/pvp_buff/pvp_jiasu.prefab", (obj) => { });
        }

        public void sscene(string cmd, string param1, string param2)
        {
            GameSceneManager.GetInstance().SetSceneObjVisible(param1, param2 != "0");
        }

        public void sterrain(string cmd, string param1)
        {
            GameSceneManager.GetInstance().SetTerrainEnabled(param1 != "0");
        }

        public void resolution(string cmd, string param1, string param2, string param3, string param4)
        {
            Screen.SetResolution(int.Parse(param1), int.Parse(param2), param3 != "0", int.Parse(param4));
        }

        public void clearblood(string cmd)
        {
            //ari ModuleMainUI.DamageManager.Instance.ClearPools();
        }

        public void aspect(string cmd, string param1)
        {
            Camera.main.aspect = float.Parse(param1);
        }

        public void sobj(string cmd, string param1, string param2)
        {
            var gobj = GameObject.Find(param1);
            if (gobj)
            {
                gobj.SetActive(param2 != "0");
            }
        }

        public void spanel(string cmd, string param1, string param2)
        {
            var panelId = (PanelIdEnum)int.Parse(param1);
            LoggerHelper.Error("showpanel:" + param1 + ":" + param2);
            if (param2 != "0")
            {
                UIManager.Instance.ShowPanel(panelId);
            }
            else
            {
                UIManager.Instance.ClosePanel(panelId);
            }
        }

        public void mainui(string cmd, string param1, string param2)
        {
            EventDispatcher.TriggerEvent(MainUIEvents.SHOWORHIDE_MAINUI_PART, int.Parse(param1), int.Parse(param2));
        }

        public void mainchat(string cmd, string param1, string param2)
        {
            EventDispatcher.TriggerEvent(MainUIEvents.SHOWORHIDE_MAIN_CHAT_PART, int.Parse(param1), int.Parse(param2));
        }

        public void clayer(string cmd, string param1, string param2)
        {
            var layer = 1 << UnityEngine.LayerMask.NameToLayer(param1);
            if (param2 != "0")
            {
                CameraManager.GetInstance().Camera.cullingMask |= layer;
            }
            else
            {
                CameraManager.GetInstance().Camera.cullingMask ^= layer;
            }
        }

        public void desoe(string cmd)
        {
            MogoEngine.Mgrs.EntityMgr.Instance.DestroyAllOtherEntities();
        }

        public void frate(string cmd, string param1)
        {
            Application.targetFrameRate = int.Parse(param1);
        }

        public void facade(string cmd)
        {
            Debug.LogError("PlayerAvatar.Player.facade: " + PlayerAvatar.Player.facade);
        }

        public void usetimeout(string cmd, string param1)
        {
            Main.Instance.curACTSystemDriver.ObjectPool.useTimeout = param1 != "0";
        }

        public void cachescene(string cmd)
        {
            /*
            List<string> paths = new List<string>();
            paths.Add(GameSceneManager.GetInstance().curSpaceData.MapPath);
            var lightMapPaths = GameSceneManager.GetInstance().curSpaceData.LightMapPaths;
            for (int i = 0; i < lightMapPaths.Length; i++)
            {
                paths.Add(lightMapPaths[i]);
            }
            ObjectPool.Instance.DontDestroy(paths.ToArray());*/
        }

        public void playskill(string cmd, string param1)
        {
            string[] paramArr = param1.Split(',');
            Entity entity = MogoWorld.GetEntity(uint.Parse(paramArr[0]));
            if (entity == null || !(entity is EntityCreature)) return;

            EntityCreature creature = entity as EntityCreature;
            creature.skillManager.PlaySkill(int.Parse(paramArr[1]));
        }

        public void autofight(string cmd, string param1)
        {
            PlayerAutoFightManager.GetInstance().AutoFight(int.Parse(param1));
        }

        public void showbuff(string cmd)
        {
            List<int> buffList = PlayerAvatar.Player.bufferManager.GetCurBufferIDList();
            string buffs = string.Empty;
            for (int i = 0; i < buffList.Count; ++i)
            {
                buffs += buffList[i].ToString();
                if (i < buffList.Count - 1)
                {
                    buffs += ",";
                }
            }
            if (!string.IsNullOrEmpty(buffs))
            {
                ChatManager.Instance.SendClientChatMsg(public_config.CHANNEL_ID_ALL, buffs);
            }
        }

        public void statecell(string cmd)
        {
            ChatManager.Instance.SendClientChatMsg(public_config.CHANNEL_ID_ALL, Convert.ToString(PlayerAvatar.Player.state_cell, 2));
        }

        public void clientstatecell(string cmd)
        {
            ChatManager.Instance.SendClientChatMsg(public_config.CHANNEL_ID_ALL, Convert.ToString(PlayerAvatar.Player.stateManager.currentState, 2));
        }

        public void setstate(string cmd, string param1)
        {
            string[] paramArr = param1.Split(',');
            PlayerAvatar.Player.stateManager.AccumulateState(int.Parse(paramArr[0]), int.Parse(paramArr[1]));
        }

        public void showattr(string cmd, string param1)
        {
            float attrValue = PlayerAvatar.Player.GetAttribute((fight_attri_config)int.Parse(param1));
            ChatManager.Instance.SendClientChatMsg(public_config.CHANNEL_ID_ALL, param1 + "," + attrValue.ToString());
        }

        public void rps(string cmd, string param1)
        {
            string[] paramArr = param1.Split(',');
            int param = int.Parse(paramArr[0]);
            TestManager.Instance.airVehicle.ReplaceScene("10002(Clone)", @"Scenes/10002/10002_test.prefab", param);
        }

        public void closecp(string cmd)
        {
            TimerHeap.AddTimer(5000, 0, OnCloseCp);
        }

        public void enablesystemchat(string cmd, string param1)
        {
            int param = int.Parse(param1);
            if (param == 1)
            {
                //ari SystemChatMsgManager.Instance.IsShowMsg = true;
            }
            else
            {
                //ari SystemChatMsgManager.Instance.IsShowMsg = false;
            }
        }

        public void sortordered(string cmd, string param1)
        {
            int param = int.Parse(param1);
            GMState.enableSortOrderedRender = param == 1 ? true : false;
        }

        private void OnCloseCp()
        {
            UIManager.Instance.DisablePanelCanvas(PanelIdEnum.Copy);
            //UILayerManager.GetRectTransform(MogoUILayer.LayerUIPanel).FindChild("Container_CopyPanel").gameObject.SetActive(false);
        }

        public void dpanel(string cmd, string param)
        {
            UIManager.Instance.DestroyPanel((PanelIdEnum)int.Parse(param));
        }

        public void triggercount(string cmd, string param1)
        {
            ChatManager.Instance.SendClientChatMsg(public_config.CHANNEL_ID_ALL, DramaManager.GetInstance().GetTriggerCount(int.Parse(param1)).ToString());
        }

        public void sound(string cmd, string param1)
        {
            SoundInfoManager.Instance.PlaySound(int.Parse(param1));
        }

        public void voicetest(string cmd, string param1, string param2)
        {
            int isEnable = int.Parse(param1);
            GMState.enableVoiceTest = isEnable == 1 ? true : false;
            ChatManager.Instance.voiceTestText = param2;
        }

        public void systemmsg(string cmd, string param1, string param2, string param3)
        {
            int channelId = int.Parse(param1);
            int msgType = int.Parse(param3);
            ChatManager.Instance.SendClientChannelSystemMsg(channelId, param2, msgType);
        }

        public void systemchat(string cmd, string param1)
        {
            ChatManager.Instance.SendClientTipsChatMsg(public_config.CHANNEL_ID_WORLD, param1);
        }

        public void ride(string cmd, string param1)
        {
            int isMount = int.Parse(param1);
            if (isMount == 1)
            {
                //PlayerAvatar.Player.rideManager.Mount(1);
                MountManager.Instance.SetCurrentReq(1);
            }
            else
            {
                //PlayerAvatar.Player.rideManager.Dismount();
                MountManager.Instance.SetCurrentReq(0);
            }
        }

        public void returnready(string cmd, string param1)
        {
            if (Convert.ToBoolean(int.Parse(param1)))
            {
                PlayerAvatar.Player.actor.animationController.ReturnReady();
                PlayerAvatar.Player.actor.animationController.SetRidingState(false);
            }
            else
            {
                PlayerAvatar.Player.actor.animationController.SetRidingState(true);
            }
        }

        public void stopanim(string cmd, string param1)
        {
            if (Convert.ToBoolean(int.Parse(param1)))
            {
                PlayerAvatar.Player.actor.animationController.Pause();
                PlayerAvatar.Player.controlActor.animationController.Pause();
            }
            else
            {
                PlayerAvatar.Player.actor.animationController.Resume();
                PlayerAvatar.Player.controlActor.animationController.Resume();
            }
        }

        public void hideplayer(string cmd, string param1)
        {
            PlayerAvatar.Player.actor.visible = Convert.ToBoolean(int.Parse(param1));
        }

        public void hideride(string cmd, string param1)
        {
            PlayerAvatar.Player.controlActor.visible = Convert.ToBoolean(int.Parse(param1));
        }

        public void changeslot(string cmd, string param1)
        {
            Transform trans;
            if (Convert.ToBoolean(int.Parse(param1)))
            {
                trans = PlayerAvatar.Player.controlActor.boneController.GetBoneByName("slot_camera");
            }
            else
            {
                trans = PlayerAvatar.Player.actor.boneController.GetBoneByName("slot_camera");
            }
            CameraManager.GetInstance().ChangeToRotationTargetMotion(trans, -Vector3.one, 0, 0, -1f, 0);
        }

        public void lerp(string cmd)
        {
            Debug.LogError(Mathf.Lerp(0, 100, 0));
            Debug.LogError(Mathf.Lerp(0, 100, 0.1f));
            Debug.LogError(Mathf.Lerp(0, 100, 0.5f));
            Debug.LogError(Mathf.Lerp(0, 100, 0.8f));
            Debug.LogError(Mathf.Lerp(0, 100, 1f));

            Vector3 from = new Vector3(0, 0, 1);
            Vector3 to = new Vector3(1, 0, 0);
            Vector3 lerp;
            lerp = Vector3.Lerp(from, to, 0);
            Debug.LogError(lerp.x + " " + lerp.y + " " + lerp.z);
            lerp = Vector3.Lerp(from, to, 0.1f);
            Debug.LogError(lerp.x + " " + lerp.y + " " + lerp.z);
            lerp = Vector3.Lerp(from, to, 0.5f);
            Debug.LogError(lerp.x + " " + lerp.y + " " + lerp.z);
            lerp = Vector3.Lerp(from, to, 0.8f);
            Debug.LogError(lerp.x + " " + lerp.y + " " + lerp.z);
            lerp = Vector3.Lerp(from, to, 1f);
            Debug.LogError(lerp.x + " " + lerp.y + " " + lerp.z);

            lerp = Vector3.Slerp(from, to, 0);
            Debug.LogError(lerp.x + " " + lerp.y + " " + lerp.z);
            lerp = Vector3.Slerp(from, to, 0.1f);
            Debug.LogError(lerp.x + " " + lerp.y + " " + lerp.z);
            lerp = Vector3.Slerp(from, to, 0.5f);
            Debug.LogError(lerp.x + " " + lerp.y + " " + lerp.z);
            lerp = Vector3.Slerp(from, to, 0.8f);
            Debug.LogError(lerp.x + " " + lerp.y + " " + lerp.z);
            lerp = Vector3.Slerp(from, to, 1f);
            Debug.LogError(lerp.x + " " + lerp.y + " " + lerp.z);
        }

        public void touches(string cmd, string param1)
        {
            string[] args = param1.Split(',');
            CameraManager.GetInstance().mainCamera.UpdateTouchesArgs(float.Parse(args[0]), float.Parse(args[1]), float.Parse(args[2]), float.Parse(args[3]));
        }

        public void touchrange(string cmd, string param1)
        {
            string[] args = param1.Split(',');
            CameraManager.GetInstance().mainCamera.UpdateTouchesRange(float.Parse(args[0]), float.Parse(args[1]), float.Parse(args[2]), float.Parse(args[3]));
        }

        public void touchmode(string cmd, string param1)
        {
            if (Convert.ToBoolean(int.Parse(param1)))
            {
                PlayerTouchBattleModeManager.GetInstance().Enter();
            }
            else
            {
                PlayerTouchBattleModeManager.GetInstance().Exit();
            }
        }

        public void platform(string cmd, string param1)
        {
            platform_helper.SetPlatformMode((PlatformMode)int.Parse(param1));
        }

        public void showweblog(string cmd, string param1)
        {
            WebLoginInfoManager.GetInstance().showWebLogFlag = int.Parse(param1);
        }

        public void fly(string cmd, string param1)
        {
            PlayerAvatar.Player.RpcCall("fly_req", byte.Parse(param1));
        }
        #endregion


        #region GM???????????? ??????????
        /**??????????????AutoTest????**/
        public void autotest(string param)
        {
            m_playerAvatar.RpcCall(s_ChatReq, 1, "@excute_lua_test auto_tests 1", 0);
        }

        /**??????????????AutoTest????**/
        public void clearbag(string param, string param1)
        {
            m_playerAvatar.RpcCall(s_ChatReq, 1, "@clear_bag " + param1, 0);
        }

        #endregion


    }
}
