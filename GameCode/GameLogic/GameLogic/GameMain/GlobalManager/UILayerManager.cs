﻿#region 模块信息
/*==========================================
// 模块名：UILayerManager
// 命名空间: GameMain.GlobalManager
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/12
// 描述说明：UI层级控制
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using GameLoader.Utils;
using GameMain.GlobalManager;
using Common.ExtendComponent;
using Game.UI.TextEngine;
using MogoEngine.Events;
using Common.Events;

public class MogoUILayerComparer : IEqualityComparer<MogoUILayer>
{
    public bool Equals(MogoUILayer x, MogoUILayer y)
    {
        return (int)x == (int)y;
    }

    public int GetHashCode(MogoUILayer obj)
    {
        return (int)obj;
    }
}

public enum MogoUILayer
{
    /// <summary>
    /// 主UI 活动图标等
    /// </summary>
    LayerUIMain = 1000,

    LayerUnderPanel = 1500,

    /// <summary>
    /// 各个子功能
    /// </summary>
    LayerUIPanel = 2000,

    LayerSpecialUIPanel = 3000,

    LayerUIPopPanel = 3500,

    /// <summary>
    /// 二级界面
    /// </summary>
    // LayerSecondUIPanel = 3000,

    /// <summary>
    /// 三级界面
    /// </summary>
    //LayerThirdUIPanel = 3500, 

    /// <summary>
    /// 通知信息
    /// </summary>
    LayerUINotice = 4000,
    /// <summary>
    /// ToolTip
    /// </summary>
    LayerUIToolTip = 4500,

    LayerEffect = 5000,


    /// <summary>
    /// 弹出框层
    /// </summary>
    LayerAlert = 7000,

    LayerGuide = 7500,

    LayerHideOther = 8000,

    /// <summary>
    /// 最上面预留
    /// </summary>
    LayerUITop = 9000,

}

namespace GameMain.GlobalManager
{
    public class UILayerManager
    {
        private const float LAYER_Z_INTERVAL = -500;
        private static UILayerManager _instance;

        public static UILayerManager Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new UILayerManager();
                }
                return _instance;
            }
        }

        private static Dictionary<MogoUILayer, Transform> layerTransformDict;

        public static void CreateUILayers(GameObject uiroot)
        {
            layerTransformDict = new Dictionary<MogoUILayer, Transform>();
            Array arr = Enum.GetValues(typeof(MogoUILayer));
            for(int i = 0; i < arr.Length; i++)
            {
                MogoUILayer layer = (MogoUILayer)(arr.GetValue(i));
                createLayer(uiroot, layer, i * LAYER_Z_INTERVAL);
            }
        }

        private static void createLayer(GameObject parent, MogoUILayer layer, float zPosition)
        {
            string layerName = layer.ToString();
            GameObject layerGo = new GameObject(layerName);
            RectTransform rect = GameObjectHelper.AddBottomLeftRectTransform(layerGo, Vector2.zero, new Vector2(UIManager.CANVAS_WIDTH, UIManager.CANVAS_HEIGHT));
            layerGo.transform.SetParent(parent.transform, false);
            rect.localPosition = new Vector3(rect.localPosition.x, rect.localPosition.y, zPosition);
            layerTransformDict.Add(layer, layerGo.transform);
        }

        public static Transform GetUILayerTransform(MogoUILayer layer)
        {
            if(layerTransformDict.ContainsKey(layer))
            {
                return layerTransformDict[layer];
            }
            return null;
        }
    }
}
