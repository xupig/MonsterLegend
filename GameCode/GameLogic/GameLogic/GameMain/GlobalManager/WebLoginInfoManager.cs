﻿using Common;
using Common.Data;
using GameLoader.Config;
using GameLoader.Utils;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Net;
using System.Text.RegularExpressions;
using UnityEngine;
namespace GameMain.GlobalManager
{
    public class WebLoginInfoManager
    {
        private static WebLoginInfoManager s_instance = null;
        private WindowsWebPlatformLoginData _webPlatformLoginData;
        public static WebLoginInfoManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new WebLoginInfoManager();
            }
            return s_instance;
        }

        public  bool GetLoginInfo()
        {
            var _webPlatformLoginData = new WindowsWebPlatformLoginData();
            //string webUrl = "http://www.qjphsweb.com/ResourcesWeb/mian_2016022901/qjphs.unity3d?v=000003&username=123&time=39832&server=1&cm=1&flag=kljewofwefwo&game=&name=chen321yg";
            string webUrl = Application.absoluteURL;
            LoggerHelper.Info("webUrl:" + webUrl);
            if (string.IsNullOrEmpty(webUrl))
            {
                LoggerHelper.Error("webUrl is nullOrEmpty");
                return false;
            }
            int temp = webUrl.IndexOf("?");
            if (temp == -1)
            {
                LoggerHelper.Error("webUrl can't find ?");
                return false;
            }
            string command = webUrl.Substring(temp + 1);
            LoggerHelper.Info("command:" + command);

            string[] values = command.Split('&');
            for (int i = 0; i < values.Length; ++i)
            {
                if (values[i].StartsWith("username=") && values[i].Length > 9)
                {
                    _webPlatformLoginData.username = values[i].Substring(9);
                }
                else if (values[i].StartsWith("server=") && values[i].Length > 7)
                {
                    _webPlatformLoginData.server = values[i].Substring(7);
                }
                else if (values[i].StartsWith("name=") && values[i].Length > 5)
                {
                    _webPlatformLoginData.name = values[i].Substring(5);
                }
                else if (values[i].StartsWith("time=") && values[i].Length > 5)
                {
                    _webPlatformLoginData.time = values[i].Substring(5);
                }
                else if (values[i].StartsWith("cm=") && values[i].Length > 3)
                {
                    _webPlatformLoginData.cm = values[i].Substring(3);
                }
                else if (values[i].StartsWith("flag=") && values[i].Length > 5)
                {
                    _webPlatformLoginData.flag = values[i].Substring(5);
                }
            }

            LoggerHelper.Info("[WebSDK]username:" + _webPlatformLoginData.username
                + " time:" + _webPlatformLoginData.time
                + " cm:" + _webPlatformLoginData.cm
                + " flag:" + _webPlatformLoginData.flag
                + " server:" + _webPlatformLoginData.server
                + " platformid:" + _webPlatformLoginData.platform_id
                + " name:" + _webPlatformLoginData.name);

            return true;
        }

        public void Parseurl()
        {
            string url = Application.absoluteURL;
            LoggerHelper.Info("webUrl:" + url);
            if (string.IsNullOrEmpty(url))
            {
                LoggerHelper.Error("url is nullOrEmpty");
            }
            _webPlatformLoginData = new WindowsWebPlatformLoginData();
            //string url = "http://www.qjphsweb.com/ResourcesWeb/mian_2016022901/qjphs.unity3d?v=000003&username=123&time=39832&server=1&cm=1&flag=kljewofwefwo&game=&name=chen321yg";
            NameValueCollection cPar = new NameValueCollection();
            url = url.StartsWith("?") ? url.Substring(1) : url;
            string[] pairs = url.Split(new char[] { '&' });
            int tempIndex;
            foreach(string pair in pairs)
            {
                if(string.IsNullOrEmpty(pair))
                {
                    continue;
                }
                tempIndex = pair.IndexOf('=');
                if(tempIndex == -1)
                {
                    continue;
                }
                cPar.Add(pair.Substring(0, tempIndex), pair.Substring(tempIndex + 1));
            }
            if (!string.IsNullOrEmpty(cPar["username"]))
            {
                _webPlatformLoginData.username = cPar["username"];
            }
            if (!string.IsNullOrEmpty(cPar["name"]))
            {
                _webPlatformLoginData.name = cPar["name"];
            }
            if (!string.IsNullOrEmpty(cPar["time"]))
            {
                _webPlatformLoginData.time = cPar["time"];
            }
            if (!string.IsNullOrEmpty(cPar["cm"]))
            {
                _webPlatformLoginData.cm = cPar["cm"];
            }
            if (!string.IsNullOrEmpty(cPar["flag"]))
            {
                _webPlatformLoginData.flag = cPar["flag"];
            }
            if (!string.IsNullOrEmpty(cPar["server"]))
            {
                _webPlatformLoginData.server = cPar["server"];
            }
            if (!string.IsNullOrEmpty(cPar["platform_id"]))
            {
                _webPlatformLoginData.platform_id = cPar["platform_id"];
            }
            if (!string.IsNullOrEmpty(cPar["cid"]))
            {
                _webPlatformLoginData.cid = cPar["cid"];
            }
            if (!string.IsNullOrEmpty(cPar["oid"]))
            {
                _webPlatformLoginData.oid = cPar["oid"];
            }
            if (!string.IsNullOrEmpty(cPar["aid"]))
            {
                _webPlatformLoginData.aid = cPar["aid"];
            }
            if (!string.IsNullOrEmpty(cPar["fnpid"]))
            {
                _webPlatformLoginData.fnpid = cPar["fnpid"];
            }
            if (!string.IsNullOrEmpty(cPar["fnppid"]))
            {
                _webPlatformLoginData.fnppid = cPar["fnppid"];
            }
            if (!string.IsNullOrEmpty(cPar["fngid"]))
            {
                _webPlatformLoginData.fngid = cPar["fngid"];
            }
            LoggerHelper.Info("[WebSDK]username:" + _webPlatformLoginData.username
               + " time:" + _webPlatformLoginData.time
               + " cm:" + _webPlatformLoginData.cm
               + " flag:" + _webPlatformLoginData.flag
               + " server:" + _webPlatformLoginData.server
               + " platformid:" + _webPlatformLoginData.platform_id
               + " name:" + _webPlatformLoginData.name
               + " cid:" + _webPlatformLoginData.cid
               + " oid:" + _webPlatformLoginData.oid
               + " aid:" + _webPlatformLoginData.aid
               + " fnpid:" + _webPlatformLoginData.fnpid
               + " fnppid:" + _webPlatformLoginData.fnppid
               + " fngid:" + _webPlatformLoginData.fngid);
            LocalSetting.settingData.UserAccount = _webPlatformLoginData.name;
            LoginInfo.platformAccount = _webPlatformLoginData.username;
            LocalSetting.settingData.SelectedServerID = int.Parse(_webPlatformLoginData.server.Split('S')[1]);
        }

        public IEnumerator GetHttpByWWW(string url, Action<string> onDone, Action<HttpStatusCode> onFail)
        {
            WWW d = new WWW(url);
            //LoggerHelper.Info(string.Format("[www] :", url));
            yield return d;
            if(string.IsNullOrEmpty(d.error))
            {
                LoggerHelper.Info(string.Format("[www] HttpGetUrl:{0}", url + "---"));
                onDone(d.text);
            }
            else
            {
                onFail(HttpStatusCode.Accepted);
            }
        }

        public WindowsWebPlatformLoginData GetWebPlatformLoginData()
        {
            return _webPlatformLoginData;
        }

        private string _onLineFlag = "1";
        public string cuuOnlineFlag
        {
            set
            {
                _onLineFlag = value;
            }
            get
            {
                return _onLineFlag;
            }
        }

        private int _showWebLogFlag = 0;
        public int showWebLogFlag
        {
            set
            {
                _showWebLogFlag = value;
            }
            get
            {
                return _showWebLogFlag;
            }
        }
    }
}

