﻿#region 模块信息
/*==========================================
// 文件名：ActionRespManager
// 命名空间: GameMain.GlobalManager
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/19 8:33:32
// 描述说明：主要用于对服务器的返回码最初响应(已滤过错误码)
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;


using GameMain.GlobalManager.SubSystem;
using Common.ServerConfig;
using Common.Utils;

namespace GameMain.GlobalManager
{
    public class ActionRespManager : Singleton<ActionRespManager> 
    {

        public static bool showDefault = true;


        /// <summary>
        ///  分批处理消息
        /// </summary>
        /// <param name="_actionId">操作码</param>
        /// <param name="_errorId">错误码</param>
        /// <param name="_param">参数</param>
        public void parse_action(int _actionId, int _errorId, object _param)
        {
            Debug.Log("ActionRespManager::parse_action::" + _actionId + _errorId + _param);
            // 如果出现错误码，优先处理
            //SoulManager.GetInstance().ActionResp(action_id, err_id);
            //ShopManager.GetInstance().ActionResp(action_id, err_id);
            //DragManager.GetInstance().ActionResp(action_id, err_id);
            //CombinManager.GetInstance().ActionResp(action_id, err_id);
            //HeroInstanceManager.GetInstance().ResetInsResp(action_id, err_id);
            //InsResultManager.GetInstance().ExitInsResp(action_id, err_id);
            //DlgSweep.GetInstance().ActionResp(action_id, err_id);
            //StrengthenEquipManager.GetInstance().ActionResp(action_id, err_id);
            //OtherRoleInfoManager.ActionResp(action_id, err_id);
            //FengShenTaManager.ActionResp(action_id, err_id);
            //PKManager.getInstance().err_resp(action_id, err_id);
            //BaoWuDaiManager.GetInstance().ActionResp(action_id, err_id);
            //EscortManager.GetInstance().ActionResp(action_id, err_id);
            //DlgLiBaoCengCengDie.GetInstance().actionResp(action_id, err_id);
            //FourGodsManager.GetInstance().ActionResp(action_id, err_id);
            //GuildWarManager.GetInstance().ActionResp(action_id, err_id);// 帮派战处理消息
            if(_errorId != 0)
            {
                this.DoErrorLogic(_actionId, _errorId, _param);
            }
            else
            {
                this.DoNoErrorLogic(_actionId, _errorId, _param);
            }

        }

        private void DoErrorLogic(int _actionId, int _errorId, object _param)
        {
            switch((error_code)_errorId)
            {
                case error_code.ERR_ITEM_NO_THIS_ITEM_IN_BAG:
                    //    WorldWarManager.GetInstance().HandlerErrorAction(err_id);// 处理错误码操作
                    break;
                default:
                    break;
            }
            this.system_info(_errorId, _param);
        }

        private void DoNoErrorLogic(int _actionId, int _errorId, object _param)
        {
            switch(_actionId)
            {
                case 60000:

                    break;

                //case svr_action_config.GEM_COMPOUND:
                //    GemCombinManager.GetInstance().CombinResp(action_id, err_id);
                //    break;

                default:
                    Debug.Log("还没有定义操作的服务器返回码  " + _actionId);
                    break;

            }
            action_config actionConfig = (action_config)_actionId;
            Debug.Log("ActionRespManager::system_info::" + actionConfig.ToInt32());
            // 执行完逻辑后表现
            this.system_info(actionConfig.ToInt32(), _param);
        }

        public string get_system_info_str(int _id, object _param = null)
        {
            string _content = "";
            //_content = HtmlTextManager.GetInstance().GetTheHtmlText(content);
            return _content;
        }

        /**对特定信息做检查返回false就不走正常的提示流程* */
        public bool per_check(int _id, string _content)
        {
            return (_id != 30071);
        }

        /// <summary>
        ///  根据系统提示信息表显示提示信息
        ///  其中融合了action_id, error_id, system_id
        ///  error_id: 1 ~ 29999
        ///  sys_info_server: 30000 ~ 49999
        ///  sys_info_client: 50000 ~ 59999
        ///  action_id: 60000+
        /// </summary>
        /// <param name="_id">信息id</param>
        /// <param name="_param">参数字典</param>
        public void system_info(int _id, object _param = null)
        {
            if(_id == 0) return;
            int[] _typeArr = new int[] { 20, 21 };
            string content = "";
            foreach(int _type in _typeArr)
            {
                if(_type < 20) // 聊天框聊天
                {
                    system_chat(content, _type);
                    continue;
                }
                if(_type > 1000)
                {
                    MsgBtn(_type, content);
                    continue;
                }
                switch(_type)
                {
                    case 22:        // 对话框
                        Dialog(content);
                        break;
                    case 23:        // 跑马灯

                        //spotlight(content, _priority);
                        break;
                    case 24:        // 浮动tips

                        //floattips(content);
                        FloatText(content);
                        break;
                    case 25:       // 按钮+对话框消息

                        BtnToDialog(_id, content);
                        break;
                    case 27:      // 某个操作成功的浮动tips

                        FloatSuccessTips(content);
                        break;
                    case 28:
                        FloatTips(content);
                        break;
                    case 29:
                        //LeftBottomTipsManager.getInstance().AddTips(content);
                        break;
                    case 30:
                        CountDownMessage(_id, content);
                        break;
                    default:

                        break;
                }
            }
            special_handler(_id, content, _param);
        }

        /**特殊ID分发**/
        public void special_handler(int id, string content, object param = null)
        {
            //BossInstanceManager.getInstance().onSystemInfo(id, content);
            //PKManager.getInstance().onSystemInfo(id, content);
            //QuestionManager.getInstance().onSystemInfo(id, content);
            //BathManager.getInstance().onSystemInfo(id, content);
            //GiftManager.getInstance().opendDlgDollarRechargeByID(id);
            //NewbieGuideManager.getInstance().onSystemInfo(id, content);
            //BudoManager.getInstance().ResUpdatanews(id,param);
            //BudoManager.getInstance().prompt(id, content);
            //MarrySystemManager.GetInstance().updataHallStep(id);
            //ActivityNPCManager.getInstance().onSystemInfo(id, content, param);
            //PlayerActionManager.getInstance().onSystemInfo(id);
        }


        #region 显示功能

        /**系统聊天**/
        public void system_chat(string content, int channel = 11)
        {
            //ChatWindowManager.getInstance().AddOneMsgNew(channel, "", content);
        }

        /**聊天框普通提示**/
        public void common_chat(string content)
        {
            //ChatWindowManager.getInstance().AddOneMsgNew(chat_config.CHANNEL_COMMON, "", content);
        }

        /**对话框**/
        public void Dialog(string content)
        {
      //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content);
        }

        /**跑马灯**/
        public void SpotLight(string content, int priority)
        {
            //SpotLightManager.getInstance().addInfo(content, priority);
        }

        public void FloatText(string text)
        {

        }

        /**浮动tips * */
        public void FloatTips(string content)
        {
            //FloatTipsManager.getInstance().showTips(content);
        }

        /**成功操作浮动tips**/
        public void FloatSuccessTips(string content)
        {
            //FloatSuccessTipsManager.getInstance().showTips(content);
        }

        /**按钮提示，再弹出确认框**/
        public void BtnToDialog(int id, string content)
        {
            //BtnToDialogManager.GetInstance().AddMsg(id, content);
        }

        public void MsgBtn(int id, string content)
        {
            //UserMSGBtnArea.GetInstance().addMsgBtn(id, content);
        }

        public void CountDownMessage(int id, string content)
        {

        }

        #endregion

    }
}
