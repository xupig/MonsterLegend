﻿#region 模块信息
/*==========================================
// 模块名：UIManager
// 命名空间: GameMain.GlobalManager
// 创建者：Ari
// 修改者列表：YZ, LYX
// 创建日期：2014/12/08
// 描述说明：UI控制
// 其他：
//==========================================*/
#endregion

using ACTSystem;
using Common.Audio;
using Common.Base;
using Common.Events;
using Common.ExtendComponent;
using Common.Global;
using Common.Utils;
using Game.Asset;
using Game.UI;
using Game.UI.UIComponent;
using GameLoader.Utils;
using Mogo.Util;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameMain.GlobalManager
{
    public class UIResizer:MonoBehaviour
    {
        private RectTransform _rect;
        private int delay = 5;
        private int lastScreenWidth = 0;
        private int lastScreenHeight = 0;

        void Awake()
        {
            _rect = gameObject.GetComponent<RectTransform>();
        }

        void Update()
        {
            if (lastScreenHeight != Screen.height || lastScreenWidth != Screen.width)
            {
                lastScreenHeight = Screen.height;
                lastScreenWidth = Screen.width;
                UIManager.OnResize();
                //UIManager.Instance.OnResizeAndRefreshBlurPanel();
                BaseModule.OnResize();
                delay = 3;
            }
            if(delay > 0)
            {
                delay--;
                if(delay == 0)
                {
                    EventDispatcher.TriggerEvent(PanelEvents.PANEL_LAYER_RESET_POS);
                }
            }
        }
    }

    public class UIImageResizer : MonoBehaviour
    {
        public bool isHorizontal = true;
        public bool isVertical = true;
        private int lastScreenWidth = 0;
        private int lastScreenHeight = 0;
        private StateImage img;
        private RectTransform rect;
        private Vector2 pos;
        private Vector2 size;
        public bool canResize = true;

        void Awake()
        {
            img = GetComponent<StateImage>();
            rect = img.CurrentImage.GetComponent<RectTransform>();
            pos = Vector2.zero;
            size = Vector2.zero;
        }

        public void OnResize()
        {
            if (canResize == false)
            {
                return;
            }
            img.SetDimensionsDirty();
            Vector2 parentPos = MogoUtils.GetPosition2D(img.transform.parent.gameObject);
            if (isHorizontal)
            {
                pos.x = -1 - parentPos.x;
                size.x = UIManager.CANVAS_WIDTH + 2;
            }
            else
            {
                pos.x = rect.anchoredPosition.x;
                size.x = rect.sizeDelta.x;
            }
            if (isVertical)
            {
                pos.y = 1 - parentPos.y;
                size.y = UIManager.CANVAS_HEIGHT + 2;
            }
            else
            {
                pos.y = rect.anchoredPosition.y;
                size.y = rect.sizeDelta.y;
            }
            rect.anchoredPosition = pos;
            rect.sizeDelta = size;
        }

        void Update()
        {
            if (canResize == false)
            {
                return;
            }
            if (lastScreenHeight != Screen.height || lastScreenWidth != Screen.width)
            {
                lastScreenHeight = Screen.height;
                lastScreenWidth = Screen.width;
                OnResize();
            }
        }
    }

    public class UIContainerResizer : MonoBehaviour
    {
        private int lastScreenWidth = 0;
        private int lastScreenHeight = 0;

        private RectTransform rect;

        void Awake()
        {
            rect = transform as RectTransform;
        }

        public void OnResize()
        {
            rect.anchoredPosition = -MogoUtils.GetPosition2D(rect.parent.gameObject);
            rect.sizeDelta = new Vector2(UIManager.CANVAS_WIDTH, UIManager.CANVAS_HEIGHT);
        }

        void Update()
        {
            if (lastScreenHeight != Screen.height || lastScreenWidth != Screen.width)
            {
                lastScreenHeight = Screen.height;
                lastScreenWidth = Screen.width;
                OnResize();
            }
        }
    }

    public class UIContainerScaler : MonoBehaviour
    {
        private int lastScreenWidth = 0;
        private int lastScreenHeight = 0;

        private RectTransform rect;

        void Awake()
        {
            rect = transform as RectTransform;
        }

        public void OnResize()
        {
            rect.anchoredPosition = -MogoUtils.GetPosition2D(rect.parent.gameObject);
            rect.localScale = new Vector3(UIManager.CANVAS_WIDTH / UIManager.PANEL_WIDTH, UIManager.CANVAS_HEIGHT / UIManager.PANEL_HEIGHT, 1);
        }

        void Update()
        {
            if (lastScreenHeight != Screen.height || lastScreenWidth != Screen.width)
            {
                lastScreenHeight = Screen.height;
                lastScreenWidth = Screen.width;
                OnResize();
            }
        }
    }
}


