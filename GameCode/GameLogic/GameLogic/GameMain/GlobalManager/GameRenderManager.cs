using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using MogoEngine.RPC;
using GameMain.Entities;
using MogoEngine.Events;
using GameLoader.Utils.Timer;
using Common.Events;
using Common.States;
using GameData;
using Common.ServerConfig;
using Common.ClientConfig;
using GameLoader.Utils;
using MogoEngine.Mgrs;
using Common.Data;
using ACTSystem;


namespace GameMain.GlobalManager
{
    public class GameRenderManager
    {
        private static GameRenderManager s_instance = null;
        public static GameRenderManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new GameRenderManager();
            }
            return s_instance;
        }

        private static int _curRenderQuality = SettingData.RENDER_QUALITY_LOW;
        public static int curRenderQuality { get { return _curRenderQuality; } }

        private GameRenderManager()
        {
            OnInitSettings();
            EventDispatcher.AddEventListener<SettingType>(SettingEvents.UPDATE_SYSTEM_SETTING, OnSettingChange);
            EventDispatcher.AddEventListener<int>(SceneEvents.ENTER_MAP, OnPlayerEnterMap);
        }

        void OnInitSettings()
        {
            OnSettingChange(SettingType.RenderQuality);
            OnSettingChange(SettingType.PlayerCount);
            OnSettingChange(SettingType.Sound);
        }

        void OnSettingChange(SettingType settingType)
        {
            switch (settingType)
            { 
                case SettingType.RenderQuality:
                    _curRenderQuality = PlayerDataManager.Instance.SettingData.GetSettingValue(SettingType.RenderQuality);
                    OnRefreshFXQuality();
                    OnRefreshSceneLevel();
                    EventDispatcher.TriggerEvent<int>(SettingEvents.UPDATE_RENDER_QUALITY_SETTING, _curRenderQuality);
                    break;
                case SettingType.PlayerCount:
                    _showAvatarMaxCount = PlayerDataManager.Instance.SettingData.GetSettingValue(SettingType.PlayerCount);
                    OnRefreshAvatarVisible();
                    OnRefreshPetVisible();
                    break;
                case SettingType.Sound:
                    ACTSystemDriver.GetInstance().soundVolume = PlayerDataManager.Instance.SettingData.GetSettingValue(SettingType.Sound);
                    break;
            }  
        }

        //****************************************************���|***************************************************

        static List<string> _sceneObjectNames = new List<string>() { "Anim", "BlockTransparent", "Mesh", "FX"};
        static List<string> _sceneLevels = new List<string>() { "Level_H", "Level_M", "Level_L"};        
        static Dictionary<int, bool[]> _sceneLevelFlags 
            = new Dictionary<int, bool[]>() {
                {SettingData.RENDER_QUALITY_LOW, new bool[]{false, false, true}},
                {SettingData.RENDER_QUALITY_MIDDLE, new bool[]{false, true, true}},
                {SettingData.RENDER_QUALITY_HIGH, new bool[]{true, true, true}}
            };

        private Dictionary<int, Action> _qualityFunctionMap = null;
        private Dictionary<int, Action> qualityFunctionMap
        {
            get
            {
                if (_qualityFunctionMap == null)
                {
                    _qualityFunctionMap = new Dictionary<int, Action>();
                    _qualityFunctionMap[SettingData.RENDER_QUALITY_LOW] = HandleLowQuality;
                    _qualityFunctionMap[SettingData.RENDER_QUALITY_MIDDLE] = HandleMiddleQuality;
                    _qualityFunctionMap[SettingData.RENDER_QUALITY_HIGH] = HandleHighQuality;
                }
                return _qualityFunctionMap;
            }
        }

        void HandleLowQuality()
        {
            ACTSystemDriver.GetInstance().CurVisualQuality = VisualFXQuality.L;
            //ari ModuleMainUI.DamageManager.Instance.maxCount = 3;
        }

        void HandleMiddleQuality()
        {
            ACTSystemDriver.GetInstance().CurVisualQuality = VisualFXQuality.M;
            //ari ModuleMainUI.DamageManager.Instance.maxCount = 7;
        }

        void HandleHighQuality()
        {
            ACTSystemDriver.GetInstance().CurVisualQuality = VisualFXQuality.H;
            //ari ModuleMainUI.DamageManager.Instance.maxCount = 10;
        }

        void OnRefreshFXQuality()
        {
            qualityFunctionMap[_curRenderQuality]();
        }

        void OnRefreshSceneLevel()
        {
            var sceneGameObjectList = GameSceneManager.GetInstance().sceneGameObjects;
            if (sceneGameObjectList == null) return;
            var levelFlag = _sceneLevelFlags[_curRenderQuality];

            for (int i = 0; i < sceneGameObjectList.Length; i++)
            {
                var obj = sceneGameObjectList[i];
                if (obj == null) continue;
                for (int j = 0; j < _sceneObjectNames.Count; j++)
                {
                    string name = _sceneObjectNames[j];
                    var child = obj.transform.FindChild(name);
                    if (!child) continue;
                    for (int t = 0; t < _sceneLevels.Count; t++)
                    {
                        var levelName = _sceneLevels[t];
                        var level = child.FindChild(levelName);
                        if (!level) continue;
                        level.gameObject.SetActive(levelFlag[t]);
                    }
                }
            }
        }

        public void OnPlayerEnterMap(int mapID)
        {
            //OnStaticBatching();
            OnRefreshSceneLevel();
        }

        //****************************************************��Ҕ���***************************************************

        private static int MAX_COUNT = 999;
        private int _showAvatarMaxCount = MAX_COUNT;
        private int _curShowAvatarCount = 0;
        private Dictionary<uint, bool> _curAvatarStates = new Dictionary<uint, bool>();
        
        private int _curShowPetCount = 0;
        private Dictionary<uint, bool> _curPetStates = new Dictionary<uint, bool>();

        int GetShowAvatarMaxCount()
        {
            int mapType = GameSceneManager.GetInstance().curMapType;
            if (mapType != public_config.MAP_TYPE_CITY && mapType != public_config.MAP_TYPE_WILD)
            {
                return MAX_COUNT;
            }
            return _showAvatarMaxCount;
        }

        public void OnAvatarEnterWorld(EntityAvatar avatar)
        {
            if (avatar == PlayerAvatar.Player) return ;
            if (!avatar.canBeHiddenInCamera) return;
            _curAvatarStates.Add(avatar.id, false);
            if (_curShowAvatarCount < GetShowAvatarMaxCount())
            {
                _curAvatarStates[avatar.id] = true;
                _curShowAvatarCount++;
            }
            avatar.actorVisible = _curAvatarStates[avatar.id];
        }

        public void OnAvatarLeaveWorld(EntityAvatar avatar)
        {
            if (avatar == PlayerAvatar.Player) return;
            if (!avatar.canBeHiddenInCamera) return;
            if (_curAvatarStates[avatar.id]) {
                _curShowAvatarCount--;
            }
            _curAvatarStates.Remove(avatar.id);
        }

        void OnRefreshAvatarVisible()
        {
            List<uint> tempList = new List<uint>();
            foreach (var id in _curAvatarStates.Keys)
            {
                tempList.Add(id);
            }

            for (int i = 0; i < tempList.Count; i++)
            {
                var entityID = tempList[i];
                EntityAvatar avatar = MogoWorld.GetEntity(entityID) as EntityAvatar;
                if (avatar == PlayerAvatar.Player) continue;
                int maxCount = GetShowAvatarMaxCount();
                if ((_curShowAvatarCount < maxCount) && !avatar.actorVisible)
                {
                    _curAvatarStates[entityID] = true;
                    avatar.actorVisible = _curAvatarStates[entityID];
                    _curShowAvatarCount++;
                }
                else if ((_curShowAvatarCount > maxCount) && avatar.actorVisible)
                {
                    _curAvatarStates[entityID] = false;
                    avatar.actorVisible = _curAvatarStates[entityID];
                    _curShowAvatarCount--;
                }
            }
        }

        public void OnPetEnterWorld(EntityPet pet)
        {
            if (pet.MasterIsPlayer()) return;
            if (!pet.canBeHiddenInCamera) return;
            _curPetStates.Add(pet.id, false);
            if (_curShowPetCount < GetShowAvatarMaxCount())
            {
                _curPetStates[pet.id] = true;
                _curShowPetCount++;
            }
            pet.actorVisible = _curPetStates[pet.id];
        }

        public void OnPetLeaveWorld(EntityPet pet)
        {
            if (pet.MasterIsPlayer()) return;
            if (!pet.canBeHiddenInCamera) return;
            if (_curPetStates[pet.id])
            {
                _curShowPetCount--;
            }
            _curPetStates.Remove(pet.id);
        }

        void OnRefreshPetVisible()
        {
            List<uint> tempList = new List<uint>();
            foreach (var id in _curPetStates.Keys)
            {
                tempList.Add(id);
            }

            for (int i = 0; i < tempList.Count; i++)
            {
                var entityID = tempList[i];
                EntityPet pet = MogoWorld.GetEntity(entityID) as EntityPet;
                int maxCount = GetShowAvatarMaxCount();
                if ((_curShowPetCount < maxCount) && !pet.actorVisible)
                {
                    _curPetStates[entityID] = true;
                    pet.actorVisible = _curPetStates[entityID];
                    _curShowPetCount++;
                }
                else if ((_curShowPetCount > maxCount) && pet.actorVisible)
                {
                    _curPetStates[entityID] = false;
                    pet.actorVisible = _curPetStates[entityID];
                    _curShowPetCount--;
                }
            }
        }
    }

}