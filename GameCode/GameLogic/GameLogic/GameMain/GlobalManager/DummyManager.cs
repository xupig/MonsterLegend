using System;
using System.Collections.Generic;
using UnityEngine;

using MogoEngine;
using MogoEngine.RPC;
using GameMain.Entities;
using MogoEngine.Events;
using GameLoader.Utils.Timer;
using Common.Events;
using Common.States;
using GameData;
using Common.ServerConfig;
using Common.ClientConfig;
using GameLoader.Utils;
using MogoEngine.Mgrs;


namespace GameMain.GlobalManager
{
    public class DummyManagerUpdateDelegate : MogoEngine.UpdateDelegateBase 
    {
        override protected void Update()
        {
            if (onUpdate != null) onUpdate();
        }
    }

    public class DummyManager
    {
        private static DummyManager s_instance = null;
        public static DummyManager GetInstance()
        {
            if (s_instance == null)
            {
                s_instance = new DummyManager();
            }
            return s_instance;
        }

        private List<uint> _dummys = new List<uint>();
        private HashSet<uint> _deadDummys = new HashSet<uint>();
        private DummyManager()
        {
            
        }

        public void OnPlayerEnterWorld()
        {
            MogoWorld.RegisterUpdate<DummyManagerUpdateDelegate>("DummyManager.Update", Update);
        }

        public void OnPlayerLeaveWorld()
        {
            MogoWorld.UnregisterUpdate("DummyManager.Update", Update);
        }

        public void OnDummyEnterWorld(EntityDummy dummy)
        {
            if (!_dummys.Contains(dummy.id))
            {
                _dummys.Add(dummy.id);
            }
        }

        public void OnDummyLeaveWorld(EntityDummy dummy)
        {
            if (_dummys.Contains(dummy.id))
            {
                _dummys.Remove(dummy.id);
            }
        }

        int _spaceTime = 0;
        void Update()
        {
            if (_spaceTime > 0) { _spaceTime--; return; }
            _spaceTime = 10;
            CheckDeadDummy();
        }

        void CheckDeadDummy()
        {
            for (int i = _dummys.Count - 1; i >=0; i--)
            {
                uint entityID = _dummys[i];
                if (_deadDummys.Contains(entityID)) continue;
                var dummy = MogoWorld.GetEntity(entityID) as EntityDummy;
                if (dummy == null) continue;
                if (dummy.stateManager.InState(state_config.AVATAR_STATE_DEATH))
                {
                    _deadDummys.Add(entityID);
                    EventDispatcher.TriggerEvent<uint, int>(SpaceActionEvents.SpaceActionEvents_MonsterDie, 
                        entityID, InstanceDefine.DESTROY_SPAWN_DIE_MOSTER_TYPE);
                }
            }
        }
    }

}