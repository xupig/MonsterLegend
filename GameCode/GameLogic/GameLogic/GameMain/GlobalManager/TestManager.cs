﻿using Common.Utils;
using System;
using System.Collections.Generic;

namespace GameMain.GlobalManager
{
    public class TestManager : Singleton<TestManager>
    {
        private TestAirVehiceGate _airVehicle = null;
        public TestAirVehiceGate airVehicle
        {
            get 
            {
                if (_airVehicle == null)
                {
                    _airVehicle = new TestAirVehiceGate();
                }
                return _airVehicle;
            }
        }


    }
}
