﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameLoader.Utils;
using MogoEngine.Events;
using MogoEngine.RPC;
using UnityEngine;
using GameLoader.Utils.Timer;
using Common.ClientConfig;
using GameLoader.PlatformSdk;
using GameMain.GlobalManager.SubSystem;
using Common.ServerConfig;
using Common.Events;
using GameData;
using Common.Base;
using GameMain.Entities;

namespace GameMain.GlobalManager
{
    /// <summary>
    /// 应用层网络--管理类
    /// </summary>
    public class NetSocketManager
    {
        public static uint RECONNECT_LIMIT_TIME = 5;
        /// <summary>
        /// 是否出在创建，登录服
        /// true:是,false:游戏服
        /// </summary>
        public bool isLoginNet = true;
        private static NetSocketManager _instance;

        /// <summary>
        /// 允许下一次重练的时间（单位：秒）
        /// </summary>
        private static uint _tryReConnectDelayTime = 10;

        /// <summary>
        /// 重练次数限制值（单位：秒）
        /// </summary>
        private static uint _tryReConnectLimitTime = RECONNECT_LIMIT_TIME;

        /// <summary>
        /// 最后一次重连时间
        /// </summary>
        private static float _lastReConnectTime = 0;
        public static float lastReConnectTime
        {
            get { return _lastReConnectTime; }
        }

        private static uint _reconnectTimerID = 0;

        static NetSocketManager()
        {
            _instance = new NetSocketManager();
            AddEventListener();
        }

        /// <summary>
        /// 单例
        /// </summary>
        public static NetSocketManager GetInstance()
        {
            return _instance;
        }

        private static void AddEventListener()
        {
            EventDispatcher.AddEventListener(RPCEvents.NetStatusEventClose, OnNetClose);      //与服务器断开处理
            EventDispatcher.AddEventListener(RPCEvents.ReConnectFailed, OnReconnectFailed);   //重连失败
            EventDispatcher.AddEventListener(RPCEvents.ReConnectRejected, OnReConnectRejected);   //重连失败
            EventDispatcher.AddEventListener(RPCEvents.ReConnectSucceeded, OnReConnectSucceeded);   //重连成功
            EventDispatcher.AddEventListener(LoginEvents.ON_KICK_OUT, OnKickOut);   //被踢下线
            EventDispatcher.AddEventListener(RPCEvents.NetStatusEventBegin, OnBeginReconnect);
            EventDispatcher.AddEventListener(RPCEvents.NetStatusEventEnd, OnEndReconnect);
        }

        private static void OnEndReconnect()
        {
        }

        private static void OnBeginReconnect()
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.Reconnect);
        }

        private static void OnNetClose()
        {
            LoggerHelper.Info("与服务器断开连接!,isLoginNet:" + _instance.isLoginNet);
            ShowReconnectTips();
        }

        private static void OnReconnectFailed()
        {
            if (_tryReConnectLimitTime > 0)
            {
                _tryReConnectLimitTime--;
                TimerHeap.AddTimer(1000, 0, ShowReconnectTips);
            }
            else
            {
                TimerHeap.AddTimer(1000, 0, ShowQuitTips);
            }
        }

        private static void OnReConnectRejected()
        {
            ShowQuitTips();
        }

        private static void OnKickOut()
        {
            ShowQuitTips();
        }

        private static void OnReConnectSucceeded()
        {
            _tryReConnectLimitTime = RECONNECT_LIMIT_TIME;
            _lastReConnectTime = Time.realtimeSinceStartup;
            UIManager.Instance.ClosePanel(PanelIdEnum.Reconnect);
            if (PlayerAvatar.Player != null)
            {
                string txt7 = XMLManager.chinese[103026].__content;
                ChatManager.Instance.SendClientTipsChatMsg(public_config.CHANNEL_ID_WORLD, string.Format("{0} {1}", txt7, PlayerTimerManager.GetInstance().GetNowServerDateTime().ToString("yyyy-MM-dd HH:mm:ss")));
                PlayerTimerManager.GetInstance().RefreshLocalTimeStamp();
            }
        }
        
        private static void ShowQuitTips()
        {
            string txt1 = XMLManager.chinese[103020].__content;
            string txt4 = XMLManager.chinese[103023].__content;
            //Ari SystemAlert.CloseMessageBox();
            //Ari SystemAlert.Show(true, txt1, txt4, OnExitGame, true, false);
        }

        private static void ShowReconnectTips()
        {
            //Ari SystemAlert.CloseMessageBox();
            //MessageBoxCountdownWrapper wrapper = new MessageBoxCountdownWrapper((int)_tryReConnectDelayTime, InstanceDefine.MESSAGEBOX_SURE_COUNTDOWN_END_CLICK);
            string txt1 = XMLManager.chinese[103020].__content;
            string txt2 = XMLManager.chinese[103021].__content;
            string txt5 = XMLManager.chinese[103024].__content;
            string txt6 = XMLManager.chinese[103025].__content;
            //Ari MessageBoxCountdownWrapper wrapper = new MessageBoxCountdownWrapper(10, 0);
            //Ari SystemAlert.Show(true, txt1, txt2, OnTryLink, OnExitGame, txt5, txt6, wrapper, true, false);
            _reconnectTimerID = TimerHeap.AddTimer(10000, 0, OnTryLink);
        }

        /// <summary>
        /// 重连
        /// </summary>
        private static void OnTryLink()
        {
            if (_reconnectTimerID > 0)
            {
                TimerHeap.DelTimer(_reconnectTimerID);
                _reconnectTimerID = 0;
            }
            //Ari SystemAlert.CloseMessageBox();
            ServerProxy.Instance.ReConnect();
        }

        /// <summary>
        /// 确定退出游戏
        /// </summary>
        private static void OnExitGame()
        {
            if (Application.platform == RuntimePlatform.WindowsPlayer)
            {
                PlatformWindows.allowQuitting = true;
                LoggerHelper.Info("OnExitGame");
                Application.Quit();
                return;
            }
            //如果接入SDK：重启游戏，否则退出应用
            PlatformSdkMgr.Instance.RestartGame();
        }
    }
}
