﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using MogoEngine.Mgrs;
using GameLoader.Utils;
using GameData;
using ACTSystem;
using GameMain.Entities;
using MogoEngine;
using GameLoader.IO;
using GameLoader.Config;
using ShaderUtils;
using Game.Asset;

namespace GameMain.GlobalManager
{
    public class ShaderRuntimeLoader : IShaderLoader
    {
        public Shader Find(string name)
        {
            Shader shader = null;
            name = name.Replace("\\", "/");
            name = "Shader$" + Path.GetFileNameWithoutExtension(name) + ".u";
            shader = ObjectPool.Instance.GetShader(name);
            if (shader == null)
            {
                LoggerHelper.Error("Shader Find Error:" + name);
            }
            if (Application.isEditor && Shader.Find(shader.name) != null)
            {
                shader = Shader.Find(shader.name);
                LoggerHelper.Warning("Shader replace in editor:" + shader.name);
            }
            return shader;   
        }
    }


}
