﻿using System;
using System.Collections.Generic;
using System.Text;

using Common;
using Common.IManager;
using Common.Events;
using GameLoader.Utils;
using MogoEngine.Events;
using GameLoader.Utils.Timer;
using MogoEngine.Mgrs;
using UnityEngine;
using GameLoader.Config;

using Common.Base;
using MogoEngine.RPC;
using MogoEngine;
using GameMain.Entities;
using Common.ServerConfig;
using MogoEngine.Utils;
using Common.Structs.ProtoBuf;
using GameLoader.Utils.CustomType;

namespace GameMain.GlobalManager
{
    class ClientEntityManager
    {
        private static ClientEntityManager s_Instance;
        public static ClientEntityManager Instance
        {
            get
            {
                if (s_Instance == null)
                {
                    s_Instance = new ClientEntityManager();
                }
                return s_Instance;
            }
        }

        ClientEntityManager()
        {

        }

        private void CreateClientEntityByPbData(PbClientEntity pbClientEntityData)
        {
            switch (pbClientEntityData.entity_type)
            {
                case 1:  //CLIENT_ENTITY_TYPE_DUMMY
                    CreateDummyByPbData(pbClientEntityData);
                    break;
                case 2:  //CLIENT_ENTITY_TYPE_DROP
                    CreateDropItemByPbData(pbClientEntityData);
                    break;
                default:
                    break;
            }
        }

        private void CreateDummyByPbData(PbClientEntity pbClientEntityData)
        {
            MogoEngine.MogoWorld.CreateEntity(pbClientEntityData.entity_id, EntityConfig.ENTITY_TYPE_NAME_DUMMY, (entity) =>
                                                {
                                                    var dummy = entity as GameMain.Entities.EntityDummy;
                                                    dummy.isServerDummy = true;
                                                    dummy.SetPosition(new Vector3(pbClientEntityData.entity_x * 0.01f,
                                                                                    PlayerAvatar.Player.position.y,
                                                                                    pbClientEntityData.entity_y * 0.01f));    
                                                });
        }

        private void CreateDropItemByPbData(PbClientEntity pbClientEntityData)
        {
            var belongAvatar = pbClientEntityData.entity_param3;
            if (belongAvatar != 0 && belongAvatar != PlayerAvatar.Player.id) return;
            MogoEngine.MogoWorld.CreateEntity(pbClientEntityData.entity_id, EntityConfig.ENTITY_TYPE_NAME_CLIENT_DROP_ITEM, (entity) =>
                                                {
                                                    var dropItem = entity as EntityClientDropItem;
                                                    dropItem.type_id = (UInt32)pbClientEntityData.entity_param1;
                                                    dropItem.itemCount = (int)pbClientEntityData.entity_param2;            
                                                    dropItem.belongAvatar = pbClientEntityData.entity_param3;
                                                    dropItem.isServerItem = true;
                                                    dropItem.SetPosition(new Vector3(pbClientEntityData.entity_x * 0.01f + UnityEngine.Random.Range(-1f, 1f), 
                                                                                    PlayerAvatar.Player.position.y,
                                                                                    pbClientEntityData.entity_y * 0.01f + UnityEngine.Random.Range(-1f, 1f)));
                                                    //Debug.LogError(string.Format("CreateDropItemByPbData, id = {0}, type_id = {1}, Position = {2}, {3}", dropItem.id, dropItem.type_id, (pbClientEntityData.entity_x * 0.01f), (pbClientEntityData.entity_y * 0.01f)));
                                                });
        }

        public uint CreateDropItemByClient(int itemId, int itemCount, uint belongAvatar, Vector3 dropPosition, int pickType =0)
        {
            var newEntity = MogoEngine.MogoWorld.CreateEntity(EntityConfig.ENTITY_TYPE_NAME_CLIENT_DROP_ITEM, (entity) =>
            {
                var dropItem = entity as EntityClientDropItem;
                dropItem.type_id = (UInt32)itemId;
                dropItem.itemCount = itemCount;
                dropItem.belongAvatar = belongAvatar;
                dropItem.isServerItem = false;
                dropItem.pickType = pickType;
                dropItem.SetPosition(dropPosition);
            });
            return newEntity.id;
        }

        //以下是服务器回调接口
        public void CreateClientEntityResp(byte[] bytes)
        {
            PbClientEntityList data = MogoProtoUtils.ParseProto<PbClientEntityList>(bytes);
            List<PbClientEntity> clientEntityList = data.clientEntityList;
            for (int i = 0; i < clientEntityList.Count; i++)
            {
                CreateClientEntityByPbData(clientEntityList[i]);
            }
        }

        public void DestroyClientEntityResp(byte[] bytes)
        {
            PbClientEntityRemoveList data = MogoProtoUtils.ParseProto<PbClientEntityRemoveList>(bytes);
            List<PbClientEntityRemove> clientEntityList = data.clientEntityRemoveList;
            for (int i = 0; i < clientEntityList.Count; i++)
            {
                var entity = MogoWorld.GetEntity(clientEntityList[i].entity_id);
                if (entity is EntityClientDropItem && (entity as EntityClientDropItem).picked)//被拾取的掉落物不在这里删除。 
                {
                    continue;
                }
                MogoWorld.DestroyEntity(clientEntityList[i].entity_id);
                //Debug.LogError(string.Format("DestroyDropItemByPbData, id = {0}", clientEntityList[i].entity_id));
            }
        }

        public void PickDropResp(UInt16 errorCode, UInt32 entityId, UInt32 itemID, UInt32 ItemCount)
        {
            if (errorCode > 0)
            {
                PlayerAvatar.Player.OnActionResp(0, errorCode, new LuaTable());
            }
            else
            {
                EntityClientDropItem dropItem = MogoWorld.GetEntity(entityId) as EntityClientDropItem;
                if (dropItem != null)
                {
                    dropItem.PickConfirm();    
                }
            }
        }

        public void SetFaceResp(UInt32 entityID, byte face)
        {
            var entity = MogoWorld.GetEntity(entityID);
            if (entity == null || !(entity is EntityCreature)) return;
            (entity as EntityCreature).face = face;
        }
    }
}
