﻿#region 模块信息
/*==========================================
// 模块名：BgMusicManager
// 命名空间: GameMain.GlobalManager
// 创建者：HANK
// 修改者列表：
// 创建日期：2015/06/30
// 描述说明：背景音乐管理器
// 其他：
//==========================================*/
#endregion

using ACTSystem;
using Common.Data;
using Common.Utils;
using Game.Asset;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    /// <summary>
    /// 背景音乐管理器
    /// </summary>
    public class BgMusicManager : Singleton<BgMusicManager>
    {
        /// <summary>
        /// 背景音乐挂接的实体
        /// </summary>
        private EntityCreature _bgmSlotEntity = null;

        /// <summary>
        /// 切换关卡音乐淡出时间
        /// </summary>
        public float changScenefadeTime = 1.5f;

        private MusicTriger _musicTriger = null;

        private MainCityMusic _mainCityMusic = null;

        public BgMusicManager()
            : base()
        {
            _musicTriger = new MusicTriger();
            _mainCityMusic = new MainCityMusic();
            
        }

        public BgMusicInfo GetSpareAudioSource(music musicData, bool isLightMusic)
        {
            GetGameObject();
            return BgMusicAudioSource.Instance.GetAudioSource(_bgmSlotEntity, musicData, isLightMusic);
        }

        private void GetGameObject()
        {
            if (_bgmSlotEntity == null)
            {
                //_gameObject = CameraManager.GetInstance().Camera.gameObject;
                //现在把背景音乐也移植到主角身上,解决背景音乐忽大忽小受距离影响
                _bgmSlotEntity = GameMain.Entities.PlayerAvatar.Player;
            }
        }

        public void PlayBgMusic(music musicData, Action<BgMusicInfo> callBack = null,bool isLightMusic = false)
        {
            BgMusicInfo music = GetSpareAudioSource(musicData, isLightMusic);
            if (callBack != null) callBack(music);
            music.status = MusicStatus.READY;
            ObjectPool.Instance.GetAudioClip(musicData.__path, (clip) =>
            {
                music.audioSource.clip = clip;
                music.isRecover = true;
                SoundRecoverManager.instance.GetSoundRecover(SoundRecoverManager.BGMUSIC).AddToRecordDict(music.audioSource.clip.GetInstanceID(), musicData.__path);
                var settingVolume = PlayerDataManager.Instance.SettingData.GetSettingValue(SettingType.Music)/100f;
                music.volume = musicData.__volume * settingVolume;
                music.audioSource.loop = false;
                //LoggerHelper.Error("[BgMusicManager:PlayBgMusic]=>2___musicData.__path: " + musicData.__path + ",music.volume:  " + music.volume + "ID: " + music.audioSource.GetInstanceID());
                music.status = MusicStatus.IDLE;
                music.Play();
            });
        }

        public void PlayMapBgMusic(int mapID)
        {
            music musicData = music_helper.GetInstance().GetBgMusicData(mapID);
            if (musicData == null) return;
            PlayBgMusic(musicData,(canas)=>
            {
                if (mapID == 1 || mapID == 2)
                {
                    //记录主城的音乐信息,目的是使用区域触发时可以控制主程音乐
                    //_areaTriggleMusic.cityAudioSource = canas;
                }
            },true);
            SetMusicTriger(mapID);
            //PlayAreaTriggerMusic();
            
            
        }

        private void SetMusicTriger(int mapID)
        {
            bool isCheckBossMusic = music_helper.GetInstance().GetBossMusicData(mapID) == null ? false : true;
            bool isCheckMonsterMusic = music_helper.GetInstance().GetMonsterMusicData(mapID) == null ? false : true;
            _musicTriger.SetCheckInfo(mapID, isCheckBossMusic, isCheckMonsterMusic, PlayBossBgMusic,PlayMonsterBgMusic);
        }

        public void PlayBossBgMusic(int mapID)
        {
            music musicData = music_helper.GetInstance().GetBossMusicData(mapID);
            if (musicData == null) return;
            PlayBgMusic(musicData);
        }

        public void PlayMonsterBgMusic(int mapID)
        {
            music musicData = music_helper.GetInstance().GetMonsterMusicData(mapID);
            if (musicData == null) return;
            PlayBgMusic(musicData);
        }

        public void StopAllBgMusic()
        {
            BgMusicAudioSource.Instance.StopAllBgMusic();
            
            _musicTriger.Stop();

        }

        #region 区域触发音乐
        /// <summary>
        /// 区域触发音乐
        /// 目前的音乐切换时间是读取全局表
        /// </summary>
        /// <param name="vocationMusicID"></param>
        public void PlayAreaTriggerMusic(int vocationMusicID = 0,bool loop = false)
        {
            /*
            //临时测试
            GameLoader.Utils.Timer.TimerHeap.AddTimer(3000, 0, () =>
            {
                _areaTriggleMusic.PlayAreaTriggerMusic(6);
            });
             */
            _mainCityMusic.PlayAreaTriggerMusic(vocationMusicID);
        }
        #endregion


    }
}
