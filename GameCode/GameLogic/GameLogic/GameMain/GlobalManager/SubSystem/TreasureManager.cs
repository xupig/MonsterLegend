﻿using Common.Base;
using Common.Chat;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameLoader.Utils.CustomType;
using GameMain.Entities;
using ModuleCommonUI;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.RPC;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class TreasureManager : Singleton<TreasureManager>
    {
        private const int SHARE_TYPE_TREASURE_BOX = 1;
        private const int SHARE_TYPE_BOSS = 2;
        private const int SHARE_TYPE_TREASURE_DUEL_BOX = 3;

        public void OnPlayerEnterWorld()
        {
            EventDispatcher.AddEventListener<uint>(TouchEvents.ON_TOUCH_ENTITY, OnPickUpEntity);
        }

        public void OnPlayerLeaveWorld()
        {
            EventDispatcher.RemoveEventListener<uint>(TouchEvents.ON_TOUCH_ENTITY, OnPickUpEntity);
        }

        private void OnPickUpEntity(uint id)
        {
            Entity entity = MogoWorld.GetEntity(id);
            if (entity is EntityDropItem)
            {
                PickUpDropItem(id);
            }
            else if (entity is EntityDuelChest)
            {
                OpenDuelChest(id);
            }
            else if (entity is EntityChest)
            {
                OpenChest(id);
            }
            else if (entity is EntityPortal)
            {
                PickUpPortal(id);
            }
            else if (entity is EntityCombatPortal)
            {
                PickUpCombatPortal(id);
            }
        }

        private void PickUpDropItem(uint id)
        {
            EntityDropItem dropItem = MogoWorld.GetEntity(id) as EntityDropItem;
            if (dropItem != null)
            {
                dropItem.BePicked(PlayerAvatar.Player.id);
            }
        }

        public void OpenChest(uint id)
        {
            EntityChest chest = MogoWorld.GetEntity(id) as EntityChest;
            if (chest != null)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.TreasureReward, id);
            }
        }

        public void OpenDuelChest(uint id)
        {
            EntityDuelChest chest = MogoWorld.GetEntity(id) as EntityDuelChest;
            if (chest != null)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.TreasureReward, id);
            }
        }

        public void PickUpChest(uint id)
        {
            EntityChest chest = MogoWorld.GetEntity(id) as EntityChest;
            if (chest != null)
            {
                PlayerAvatar.Player.RpcCall("pickup_chest_req", id, chest.chest_id);
            }
        }

        public void PickUpDuelChest(uint id)
        {
            EntityDuelChest chest = MogoWorld.GetEntity(id) as EntityDuelChest;
            if (chest != null)
            {
                PlayerAvatar.Player.RpcCall("pickup_duelchest_req", id, chest.chest_id);
            }
        }

        private void PickUpPortal(uint id)
        {
            EntityPortal portal = MogoWorld.GetEntity(id) as EntityPortal;
            if (portal != null)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.PortalInstance, new int[] { (int)id, (int)portal.mission_id });
            }
        }

        private void PickUpCombatPortal(uint id)
        {
            EntityCombatPortal portal = MogoWorld.GetEntity(id) as EntityCombatPortal;
            if (portal != null)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.CombatPortalInstance, new int[] { (int)id, (int)portal.inst_id });
            }
        }

        public void EnterPortalMission(uint entityId)
        {
            PlayerAvatar.Player.RpcCall("enter_portal_req", entityId);
        }

        public void EnterCombatPortalMission(uint entityId, uint instId)
        {
            PlayerAvatar.Player.RpcCall("enter_combat_portal_req", entityId, instId);
        }

        public void CheckChestIsPicked(uint entityId)
        {
            PlayerAvatar.Player.RpcCall("check_chest_ispicked", entityId);
        }

        public void CheckDuelChestIsPicked(uint entityId)
        {
            PlayerAvatar.Player.RpcCall("check_duelchest_ispicked", entityId);
        }

        public void SendSystemInfo(int systemInfoId, params object[] args)
        {
            LuaTable table = new LuaTable();
            table.Clear();
            for (int i = 0; i < args.Length; i++)
            {
                table.Add(i + 1, args[i]);
            }
            PlayerAvatar.Player.RpcCall("send_system_notice", systemInfoId, table);
        }

        public void OnSpawnTreasureBoss(uint entityId)
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.FieldBossIntroduce, entityId);
            ShareBossPositionToWorld(entityId, GameSceneManager.GetInstance().curMapID, (int)PlayerAvatar.Player.curLine, PlayerAvatar.Player.position, 2);
        }

        public void OnKillWorldBossResp(byte[] byteArr)
        {
            PbWorldBossInfo pbInfo = MogoProtoUtils.ParseProto<PbWorldBossInfo>(byteArr);
            UIManager.Instance.ShowPanel(PanelIdEnum.WorldBossResult, pbInfo);
        }

        public void OnChestCheck(uint entityId, byte isPick)
        {
            EntityChest chest = MogoWorld.GetEntity(entityId) as EntityChest;
            if (chest != null)
            {
                if (isPick == 0)
                {
                    chest.CreateModel();
                }
                else
                {
                    chest.DestroyModel();
                }
            }
        }

        public void OnDuelChestCheck(uint entityId, byte isPick)
        {
            EntityDuelChest chest = MogoWorld.GetEntity(entityId) as EntityDuelChest;
            if (chest != null)
            {
                if (isPick == 0)
                {
                    chest.CreateModel();
                }
                else
                {
                    chest.DestroyModel();
                }
            }
        }

        public void UseTreasureMap(int itemId)
        {
            if (PlayerAvatar.Player.treasure_map_count >= int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.treasureMapDayUseTimes)))
            {
               //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.TREASURE_USE_TIMES_LIMIT));
                return;
            }
            ItemData itemData = new ItemData(itemId);
            int leftCapacity = (int)PlayerAvatar.Player.cur_bag_count - PlayerDataManager.Instance.BagData.GetItemBagData().GetAllItemCount();
            if (itemData.UsageBagCapacityRequired > leftCapacity)
            {
                SystemInfoManager.Instance.Show(328, itemData.UsageBagCapacityRequired);
                return;
            }
            int instanceId = int.Parse(itemData.UseEffect.Value);
            int minLevel = instance_helper.GetMinLevel(instanceId);
            if (minLevel > PlayerAvatar.Player.level)
            {
               //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.TREASURE_USE_WILD_LIMIT, minLevel));
                return;
            }
            EventDispatcher.TriggerEvent<int, bool>(PlayerCommandEvents.FIND_TREASURE, itemData.Id, true);
        }

        public bool CheckCanUse(int itemId)
        {
            if (PlayerAvatar.Player.treasure_map_count >= int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.treasureMapDayUseTimes)))
            {
                return false;
            }
            ItemData itemData = new ItemData(itemId);
            int leftCapacity = (int)PlayerAvatar.Player.cur_bag_count - PlayerDataManager.Instance.BagData.GetItemBagData().GetAllItemCount();
            if (itemData.UsageBagCapacityRequired > leftCapacity)
            {
                return false;
            }
            int instanceId = int.Parse(itemData.UseEffect.Value);
            int minLevel = instance_helper.GetMinLevel(instanceId);
            if (minLevel > PlayerAvatar.Player.level)
            {
                return false;
            }
            return true;
        }

        public void TransferToSharePosition(int mapId, int mapLine, Vector3 position, int type = SHARE_TYPE_BOSS)
        {
            if (inst_type_operate_helper.TeamCanCall((ChapterType)GameSceneManager.GetInstance().curMapType) == false)
            {
                SystemInfoManager.Instance.Show(1707);
                return;
            }
            if (type == SHARE_TYPE_TREASURE_BOX && activity_helper.IsOpenByFunctionId(public_config.ACTIVITY_FUNC_CHEST) == false)
            {
               //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(99101));
                return;
            }
            else if (type == SHARE_TYPE_TREASURE_DUEL_BOX && activity_helper.IsOpenByFunctionId(public_config.ACTIVITY_FUNC_DUELCHEST) == false)
            {
               //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(122130));
                return;
            }
            int instId = map_helper.GetInstanceIDByMapID(mapId);
            int costItemId = GetCostItemKvp().Key;
            int costCount = GetCostItemKvp().Value;

            if (PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag).GetItemNum(costItemId) >= costCount)
            {
                SendToDestination(mapId, mapLine, position, costItemId, costCount,type);
            }
            else
            {
                var sharePosData = new SharePostionData();
                sharePosData.mapId = mapId;
                sharePosData.mapLine = mapLine;
                sharePosData.multiNeedConfirm = true;
                sharePosData.type = type;
                sharePosData.targetPosition = position;
                EventDispatcher.TriggerEvent<SharePostionData>(PlayerCommandEvents.FIND_SHARE_POSITION, sharePosData);
                UIManager.Instance.ClosePanel(PanelIdEnum.Chat);
            }
        }

        private void SendToDestination(int mapId, int mapLine, Vector3 position, int costItemId, int costCount, int type)
        {
            var sharePosData = new SharePostionData();
            sharePosData.mapId = mapId;
            sharePosData.mapLine = mapLine;
            sharePosData.multiNeedConfirm = true;
            sharePosData.type = type;
            sharePosData.targetPosition = position;
            /*
            string content = MogoLanguageUtil.GetString(LangEnum.SHARE_TREASURE_POSITION_COST_TIP, costCount, item_helper.GetName(costItemId));
      //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, () =>
            {
                DemonGateManager.Instance.RequestTransportWithLine(mapId, mapLine, (int)position.x * 100, (int)position.z * 100);
            }, () =>
            {
          //ari MessageBox.CloseMessageBox();
                EventDispatcher.TriggerEvent<SharePostionData>(PlayerCommandEvents.FIND_SHARE_POSITION, sharePosData);
                UIManager.Instance.ClosePanel(PanelIdEnum.Chat);
            });*/
        }

        private KeyValuePair<int, int> GetCostItemKvp()
        {
            foreach (KeyValuePair<int, int> kvp in MogoStringUtils.Convert2Dic(global_params_helper.GetGlobalParam(GlobalParamId.treasureTransportCostItem)))
            {
                return kvp;
            }
            return new KeyValuePair<int, int>(0, 0);
        }

        public void ShareTreasurePosition(string content,int mapId, int mapline, Vector3 position, int type)
        {
            int instId = map_helper.GetInstanceIDByMapID(mapId);
            int minLv = instance_helper.GetMinLevel(instId);
            string instName = MogoLanguageUtil.GetContent(instance_helper.GetName(instId));
            content = string.Format(content, minLv, instName);
            ChatManager.Instance.RequestInviteLinkSendChat(public_config.CHANNEL_ID_WORLD, new ChatSharePosLinkWrapper(content, mapId, mapline, position, type));
        }

        public void ShareBossPosition(uint entityId, int mapId, int mapline, Vector3 position, int type)
        {
            EntityMonsterBase entityMonster = MogoWorld.GetEntity(entityId) as EntityMonsterBase;
            if (entityMonster == null) return;
            int monsterId = (int)entityMonster.monster_id;
            int instId = map_helper.GetInstanceIDByMapID(mapId);
            string instName = MogoLanguageUtil.GetContent(instance_helper.GetName(instId));
            string monsterName = monster_helper.GetMonsterName(monsterId);
            float percent = Mathf.Clamp01((float)entityMonster.cur_hp / entityMonster.max_hp);

            Dictionary<int, object> channelLinkDataDict = new Dictionary<int, object>();
            if (entityMonster.event_id == 3)
            {
                string friendContent = MogoLanguageUtil.GetString(LangEnum.SHARE_TREASURE_BOSS_POSITION);
                friendContent = string.Format(friendContent, PlayerAvatar.Player.name, instName, monsterName, Mathf.RoundToInt(percent * 100));
                channelLinkDataDict.Add(public_config.CHANNEL_ID_FRIEND, new ChatSharePosLinkWrapper(friendContent, mapId, mapline, position, type));
                if (PlayerAvatar.Player.guild_id != 0)
                {
                    string guildContent = MogoLanguageUtil.GetString(LangEnum.SHARE_TREASURE_BOSS_POSITION_TO_GUILD);
                    guildContent = string.Format(guildContent, PlayerAvatar.Player.name, instName, monsterName, Mathf.RoundToInt(percent * 100));
                    channelLinkDataDict.Add(public_config.CHANNEL_ID_GUILD, new ChatSharePosLinkWrapper(guildContent, mapId, mapline, position, type));
                }
            }
            else
            {
                string content = MogoLanguageUtil.GetString(LangEnum.SHARE_SUMMON_STONE_BOSS_POSITION);
                content = string.Format(content, instName, monsterName, Mathf.RoundToInt(percent * 100));
                channelLinkDataDict.Add(public_config.CHANNEL_ID_FRIEND, new ChatSharePosLinkWrapper(content, mapId, mapline, position, type));
                if (PlayerAvatar.Player.guild_id != 0)
                {
                    channelLinkDataDict.Add(public_config.CHANNEL_ID_GUILD, new ChatSharePosLinkWrapper(content, mapId, mapline, position, type));
                }
            }
            ChatManager.Instance.RequestMultiChannelInviteLinkChat(channelLinkDataDict);
        }

        public void ShareBossPositionToWorld(uint entityId, int mapId, int mapline, Vector3 position, int type)
        {
            EntityMonsterBase entityMonster = MogoWorld.GetEntity(entityId) as EntityMonsterBase;
            if (entityMonster == null) return;
            int monsterId = (int)entityMonster.monster_id;
            string content = string.Empty;
            if (entityMonster.event_id == 3)
            {
                content = MogoLanguageUtil.GetString(LangEnum.SHARE_TREASURE_BOSS_POSITION_TO_WORLD);
            }
            else
            {
                content = MogoLanguageUtil.GetString(LangEnum.SHARE_SUMMON_STONE_BOSS_POSITION_TO_WORLD);
            }
            int instId = map_helper.GetInstanceIDByMapID(mapId);
            string instName = MogoLanguageUtil.GetContent(instance_helper.GetName(instId));
            string monsterName = monster_helper.GetMonsterName(monsterId);
            content = string.Format(content, PlayerAvatar.Player.name, instName, monsterName);
            ChatManager.Instance.RequestInviteLinkSendChat(public_config.CHANNEL_ID_WORLD, new ChatSharePosLinkWrapper(content, mapId, mapline, position, type));
        }

        public void SharePositionToWorld(string content, int mapId, int mapline, Vector3 position)
        {
            ChatManager.Instance.RequestInviteLinkSendChat(public_config.CHANNEL_ID_WORLD, new ChatSharePosLinkWrapper(content, mapId, mapline, position));
        }

    }
}
