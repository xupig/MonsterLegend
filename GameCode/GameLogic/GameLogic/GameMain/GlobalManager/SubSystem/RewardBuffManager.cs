﻿using Common;
using Common.Base;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;

namespace GameMain.GlobalManager.SubSystem
{
    public class RewardBuffManager : Singleton<RewardBuffManager>
    {
        public bool HaveRewardBuff = false;
        private bool NeedShowPlayerReturn = true;
        public void RequestRewardBuff()
        {
            action_config.ITEM_COMPENSATE_QUERY_INFO.Rpc("item_compensate_req", 0);
        }

        private bool NeedShowBuffTip = false;
        public void RewardBuffResp(UInt32 actionId, byte[] byteArr)
        {
            PbItemCompensateList rewardBuffList = MogoProtoUtils.ParseProto<PbItemCompensateList>(byteArr);
            if (rewardBuffList.items.Count != 0)
            {
                int openRewardBuffTipTime = GetOpenRewardBuffTipTime();
                DateTime loginDate = MogoTimeUtil.ServerTimeStamp2UtcTime(openRewardBuffTipTime);
                DateTime nowDate = MogoTimeUtil.ServerTimeStamp2UtcTime((long)PlayerDataManager.Instance.GetTimeStamp() / 1000);
                if (loginDate.Date != nowDate.Date)
                {
                    WriteOpenRewardBuffTipTime();
                    NeedShowBuffTip = true;
                }
                HaveRewardBuff = true;
                EventDispatcher.TriggerEvent(RewardBuffEvents.ShowRewardBuffIcon);
                EventDispatcher.TriggerEvent<PbItemCompensateList>(RewardBuffEvents.RefreshRewardBuffList, rewardBuffList);
            }
            else
            {
                HaveRewardBuff = false;
                EventDispatcher.TriggerEvent(RewardBuffEvents.HideRewardBuffIcon);
            }
            ShowRewardBuffTipAndOrPlayerReturnTip();
        }

        private void ShowRewardBuffTipAndOrPlayerReturnTip()
        {
            if (BaseModule.IsPanelShowing(PanelIdEnum.GuideArrow) == false && BaseModule.IsPanelShowing(PanelIdEnum.AntiAddiction) == false)
            {
                if (NeedShowBuffTip == true && PlayerReturnManager.Instance.ReturnData.HasRewardToGet() == true)
                {
                    NeedShowPlayerReturn = false;
                    UIManager.Instance.ShowPanel(PanelIdEnum.RewardBuffTip, 3);
                }
                else if (NeedShowBuffTip == true && PlayerReturnManager.Instance.ReturnData.HasRewardToGet() == false)
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.RewardBuffTip, 1);
                    NeedShowBuffTip = false;
                }
                else if (NeedShowBuffTip == false && PlayerReturnManager.Instance.ReturnData.HasRewardToGet() == true)
                {
                    NeedShowPlayerReturn = false;
                    UIManager.Instance.ShowPanel(PanelIdEnum.RewardBuffTip, 2);
                }
            }
        }

        private int GetOpenRewardBuffTipTime()
        {
            List<int> list = LocalCache.Read<List<int>>(LocalName.RewardBuffTipShowTime);
            int openRewardBuffTipTime = 0;
            if (list != null)
            {
                openRewardBuffTipTime = list[0];
            }
            return openRewardBuffTipTime;
        }

        private void WriteOpenRewardBuffTipTime()
        {
            List<int> list = new List<int>();
            list.Add((int)(PlayerDataManager.Instance.GetTimeStamp() / 1000));
            LocalCache.Write(LocalName.RewardBuffTipShowTime, list, CacheLevel.Permanent);
        }
    }
}
