﻿using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameLoader.Utils;
using GameLoader.Utils.CustomType;
using GameMain.Entities;
using Mogo.Util;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using GameData;
using ModuleCommonUI;
using Common.Base;
using Common.Utils;
using Common;
using Common.Data;

namespace GameMain.GlobalManager.SubSystem
{
    public class GuildShopManager : Singleton<GuildShopManager>
    {
        private void RPC(action_config actionId, params object[] args)
        {
            //Debug.LogError("RPC:"+actionId);
            PlayerAvatar.Player.ActionRpcCall("guild_shop_action_req", (int)actionId, args);
        }

        public void GetShopInfo()
        {
            RPC(action_config.GUILD_SHOP_GET_INFO);
        }

        public void BuyGuildShopItem(int id, int count, int itemId)
        {
            //Debug.LogError("BuyGuildShopItem:" + id + "," + count + "," + itemId);
            RPC(action_config.GUILD_SHOP_BUY, id, count, itemId);
        }

        public void PraiseGuildShopItem(int id)
        {
            RPC(action_config.GUILD_SHOP_PRAISE, id);
        }

        public void DispraiseGuildShopItem(int id)
        {
            RPC(action_config.GUILD_SHOP_DISPRAISE, id);
        }

        public void ExchangeGuildShopItem(int id, int count, int itemId)
        {
            RPC(action_config.GUILD_SHOP_EXCHANGE, id, count, itemId);
        }

        private ulong time = 0;
        public void GuildShopRequest(int id)
        {
            if(Global.serverTimeStamp - time >= 10000)
            {
                time = Global.serverTimeStamp;
                RPC(action_config.GUILD_SHOP_REQUEST, id);
            }
            else
            {
                MogoUtils.FloatTips(10006);
            }
        }

        public void OnGuildShopResp(UInt32 actionId, UInt32 errorId, byte[] byteArr)
        {
            error_code error = (error_code)errorId;
            //Debug.LogError("OnGuildShopResp:" + (action_config)actionId + "," + error);
            switch ((action_config)actionId)
            {
                case action_config.GUILD_SHOP_GET_INFO:
                    OnGetShopInfo(byteArr);
                    break;
                case action_config.GUILD_SHOP_PRAISE:
                    OnPraiseResp(true, byteArr);
                    MogoUtils.FloatTips(74991);
                    break;
                case action_config.GUILD_SHOP_DISPRAISE:
                    OnPraiseResp(false, byteArr);
                    MogoUtils.FloatTips(74992);
                    break;
                case action_config.GUILD_SHOP_BUY:
                    if (error == error_code.ERR_SUCCESSFUL)
                    {
                        OnBuyResp(byteArr);
                    }
                    break;
                case action_config.GUILD_SHOP_EXCHANGE:
                    if (error == error_code.ERR_SUCCESSFUL)
                    {
                        OnExchangeResp(byteArr);
                    }
                    break;
                case action_config.GUILD_SHOP_REQUEST:
                    OnGuildShopRequestResp(errorId);
                    break;
            }
        }

        private void OnGuildShopRequestResp(UInt32 errorId)
        {
            if((error_code)errorId == error_code.ERR_SUCCESSFUL)
            {
                MogoUtils.FloatTips(74985);//已向会长发送补货请求
            }
        }

        private void OnExchangeResp(byte[] byteArr)
        {
            PbGuildShopChangeData data = MogoProtoUtils.ParseProto<PbGuildShopChangeData>(byteArr);
            //ari PlayerDataManager.Instance.GuildShopData.ExchangeShopData(data);
            guild_shop shop = guild_shop_helper.GetGuildShop(Convert.ToInt32(data.grid_id));
            MogoUtils.FloatTips(74990,data.item_count, item_helper.GetName(shop.__item_id));
            //EventDispatcher.TriggerEvent(GuildShopEvents.Refresh_Exchange);
            //PanelIdEnum.QuickBuy.Close();
        }

        private void OnBuyResp(byte[] byteArr)
        {
            PbGuildShopChangeData data = MogoProtoUtils.ParseProto<PbGuildShopChangeData>(byteArr);
            guild_shop shop = guild_shop_helper.GetGuildShop(Convert.ToInt32(data.grid_id));
            //ari PlayerDataManager.Instance.GuildShopData.BuyShopData(data);
            MogoUtils.FloatTips(74988, data.item_count, item_helper.GetName(shop.__item_id));
            //SystemInfoManager.Instance.Show(60061);
           // PanelIdEnum.QuickBuy.Close();
        }

        private void OnPraiseResp(bool isPraise, byte[] byteArr)
        {
            PbGuildPraiseData pbInfo = MogoProtoUtils.ParseProto<PbGuildPraiseData>(byteArr);
            //ari PlayerDataManager.Instance.GuildShopData.UpdatePraise(isPraise, pbInfo);
            /*ari
            if (GuildShopView.selectedIndex == 0)
            {
                EventDispatcher.TriggerEvent(GuildShopEvents.Refresh_Praise);
            }
            else
            {
                EventDispatcher.TriggerEvent<bool, PbGuildPraiseData>(GuildShopEvents.Refresh_Praise_Count, isPraise, pbInfo);
            }*/
           
        }

        private void OnGetShopInfo(byte[] byteArr)
        {
            PbGuildShopInfo pbInfo = MogoProtoUtils.ParseProto<PbGuildShopInfo>(byteArr);
            //ari PlayerDataManager.Instance.GuildShopData.Fill(pbInfo);
            EventDispatcher.TriggerEvent(GuildShopEvents.Show_List_View);
        }


    }
}
