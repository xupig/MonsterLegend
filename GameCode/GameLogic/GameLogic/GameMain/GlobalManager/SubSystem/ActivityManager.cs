﻿#region 模块信息
/*==========================================
// 文件名：ActivityManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/20 10:27:02
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class ActivityManager : Singleton<ActivityManager> 
    {
        public void GetActivityList()
        {
            if (PlayerDataManager.Instance.ActivityData.isDataRequested == false)
            {
                PlayerDataManager.Instance.ActivityData.isDataRequested = true;
                action_config.ACTION_ACTIVITY_GET_LEFT_COUNT.Rpc("activity_req");
            }
        }

        public void OnActivityResp(UInt32 actionId,UInt32 errorId,byte[] bytes)
        {
            switch((action_config)actionId)
            {
                case action_config.ACTION_ACTIVITY_GET_LEFT_COUNT:
                    OnActivityGetLeftCountResp(bytes);
                    break;
                case action_config.ACTION_ACTIVITY_JOIN:
                    break;
                default:
                    break;
            }
        }

        private void OnActivityGetLeftCountResp(byte[] bytes)
        {
            PbActivityLeftCount leftCount = MogoProtoUtils.ParseProto<PbActivityLeftCount>(bytes);
            PlayerDataManager.Instance.ActivityData.SetActivityData(leftCount.left_counts);
            EventDispatcher.TriggerEvent(ActivityEvents.ACTIVITY_LEFT_COUNT_INIT);
        }

        public void JoinActivity(int activityId)
        {
            Rpc(action_config.ACTION_ACTIVITY_JOIN, activityId);
        }

        private void Rpc(action_config actionId,params object[] param)
        {
            PlayerAvatar.Player.ActionRpcCall("activity_req",(int)actionId,param);
        }

    }
}
