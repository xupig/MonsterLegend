﻿#region 模块信息
/*==========================================
// 文件名：TaskManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/1/21 9:12:40
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Chat;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.CustomType;
using GameMain.Entities;

using Mogo.Util;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class TaskManager : Singleton<TaskManager>
    {

        private List<int> acceptList = new List<int>();
        private List<int> finishList = new List<int>();
        private List<int> reachedList = new List<int>();
        public Dictionary<int, int> taskIntanceInfo = new Dictionary<int, int>();

        public TaskManager()
        {
            EventDispatcher.AddEventListener<int>(SceneEvents.ENTER_MAP, OnEnterMap);
        }

        private void OnEnterMap(int mapId)
        {
            int mapType = map_helper.GetMapType(mapId);
            if (mapType == public_config.MAP_TYPE_SPACETIME_RIFT)
            {
                PlayerAutoFightManager.GetInstance().Start();
            }
        }


        private void RPC(action_config actionId, UInt32 taskId)
        {
            MogoWorld.Player.RpcCall("task_req", (int)actionId, taskId);
        }

        private void TeamTaskRpc(action_config actionId, UInt64 arg1, UInt32 arg2, UInt32 arg3)
        {
            Debug.Log("客户端请求时空裂隙:" + actionId);
            MogoWorld.Player.RpcCall("spacetime_rift_req", (int)actionId, arg1, arg2, arg3);
        }


        public void AcceptTask(UInt32 taskId)
        {
            //Debug.LogError("AcceptTask:" + taskId);
            RPC(action_config.TASK_ACCEPT, taskId);
        }

        public void FinishTask(UInt32 taskId)
        {
            //Debug.Log("FinishTask:" + taskId);
            RPC(action_config.TASK_COMPLETE, taskId);
        }

        public void FinishClientTask(uint type, int taskId)
        {
            LuaTable table = new LuaTable();
            table.Add(type, taskId);
            PlayerAvatar.Player.RpcCall("client_event_req", type, table);
        }

        public void DiscardTask(UInt32 taskId)
        {
            RPC(action_config.TASK_DISCARD, taskId);
        }

        public void GetTaskList()
        {
            RPC(action_config.TASK_GETLIST, 0);
        }

        public void StartTeamTask()
        {
            TeamTaskRpc(action_config.ACTION_SPACETIME_RIFT_TASK_APPLY_REQ, 0, 0, 0);
        }

        public void ReceiveTeamTask(int taskId)
        {
            TeamTaskRpc(action_config.ACTION_SPACETIME_RIFT_TASK_RECIVE_REQ, (UInt64)taskId, 0, 0);
        }


        public void QuitTeamTask()
        {
            TeamTaskRpc(action_config.ACTION_SPACETIME_RIFT_TASK_QUIT_REQ, 0, 0, 0);
        }

        public void ReceiveTeamTaskTimeReward(int rewardId)
        {
            TeamTaskRpc(action_config.ACTION_SPACETIME_RIFT_TASK_WEEK_REQ, (UInt64)rewardId, 0, 0);
        }

        public void ReceiveTeamTaskReward()
        {
            TeamTaskRpc(action_config.ACTION_SPACETIME_RIFT_TASK_COMMIT_REQ, 0, 0, 0);
        }

        public void GetTeamTaskOnLineData()
        {
            TeamTaskRpc(action_config.ACTION_SPACETIME_RIFT_TASK_ONLINE_REQ, 0, 0, 0);
        }

        public void OnItemCommitResp(UInt32 actionId, UInt32 errorId)
        {
            if ((action_config)actionId == action_config.TASK_HAND_IN)
            {
                if (errorId == 0)
                {
                    //ari ItemCommitPanel.isCommit = false;
                    PanelIdEnum.ItemCommit.Close();
                }
            }
        }

        public void OnSpacetimeRiftResp(UInt32 actionId, UInt32 errorId, byte[] byteData)
        {
            PbSpacetimeRiftInfo info = null;
            error_code errorCode = (error_code)errorId;
            // LoggerHelper.Info("服务器返回时空裂隙协议:" + (action_config)actionId + "," + errorCode);
            switch ((action_config)actionId)
            {
                case action_config.ACTION_SPACETIME_RIFT_TASK_APPLY_REQ:
                    OnTeamTaskStartResp(errorCode, byteData);
                    break;
                case action_config.ACTION_SPACETIME_RIFT_TASK_RECIVE_REQ:
                    OnTeamTaskReceiveResp(errorCode, byteData);
                    break;
                case action_config.ACTION_SPACETIME_RIFT_TASK_ONLINE_REQ:
                    info = MogoProtoUtils.ParseProto<PbSpacetimeRiftInfo>(byteData);
                    OnTeamTaskGetOnlineResp(errorCode, info);
                    break;
                case action_config.ACTION_SPACETIME_RIFT_TASK_COMMIT_REQ:
                    info = MogoProtoUtils.ParseProto<PbSpacetimeRiftInfo>(byteData);
                    OnTeamTaskGetReward(errorCode, info);
                    break;
                case action_config.ACTION_SPACETIME_RIFT_TASK_QUIT_REQ:
                    break;
                case action_config.ACTION_SPACETIME_RIFT_TASK_WEEK_REQ:

                    OnReceiveTeamTaskTimeRewardResp(errorCode, byteData);
                    break;
            }

        }

        private void OnReceiveTeamTaskTimeRewardResp(error_code errorCode, byte[] bytes)
        {
            if (errorCode == error_code.ERR_SUCCESSFUL)
            {
                TaskData taskData = PlayerDataManager.Instance.TaskData;
                PbWeekRward reward = MogoProtoUtils.ParseProto<PbWeekRward>(bytes);
                taskData.AddWeekRewardList(reward);
            }
        }

        private void OnTeamTaskGetReward(error_code errorCode, PbSpacetimeRiftInfo info)
        {
            if (errorCode == error_code.ERR_SUCCESSFUL)
            {
                // PlayerDataManager.Instance.TaskData.isTeammateAllReady = false;

                TaskData taskData = PlayerDataManager.Instance.TaskData;
                taskData.UpdateDayRewardList(info.day_reward_list);
                taskData.remain_times = (int)info.remain_times;
                taskData.team_task_times = (int)info.week_times;
                taskData.task_place_Id = 0;
                taskData.task_place_pos = Vector3.zero;

                EventDispatcher.TriggerEvent(TaskEvent.DESTROY_TEAM_TASK_ENTERANCE);
                // LoggerHelper.Info("DESTROY_TEAM_TASK_ENTERANCE");
            }


        }

        private void OnTeamTaskGetOnlineResp(error_code errorCode, PbSpacetimeRiftInfo info)
        {
            if (errorCode == error_code.ERR_SUCCESSFUL)
            {
                TaskData taskData = PlayerDataManager.Instance.TaskData;
                taskData.offlineRecord = info.offline_list;
                taskData.offlineReward = info.offline_rewards;
                taskData.offlineTimes = (int)info.offline_times;

                taskData.task_place_Id = (int)info.task_place_Id;
                taskData.task_place_pos = new Vector3(info.rand_posi.map_x, info.rand_posi.map_y, info.rand_posi.map_z);
                // LoggerHelper.Info("rand_posi:" + info.rand_posi.map_x + "," + info.rand_posi.map_y + "," + info.rand_posi.map_z);
                taskData.task_id = (int)info.task_id;


                taskData.UpdateDayRewardList(info.day_reward_list);
                taskData.remain_times = (int)info.remain_times;

                taskData.UpdateWeekRewardList(info.week_reward_list);
                taskData.team_task_times = (int)info.week_times;

                taskData.min_level = (int)info.lowest_level;
                taskData.ring = (int)info.current_ring;
                //taskData.task_status = (int)info.task_status;

                EventDispatcher.TriggerEvent(TaskEvent.REMAIN_TIMES_CHANGE);
                EventDispatcher.TriggerEvent(TaskEvent.RING_CHANGE);

                if (taskData.ring != 0)
                {
                    EventDispatcher.TriggerEvent(TaskEvent.CREATE_TEAM_TASK_ENTERANCE, taskData);
                    //LoggerHelper.Info("CREATE_TEAM_TASK_ENTERANCE");
                }
                else
                {
                    EventDispatcher.TriggerEvent(TaskEvent.DESTROY_TEAM_TASK_ENTERANCE);
                    //LoggerHelper.Info("DESTROY_TEAM_TASK_ENTERANCE");
                }

                if (taskData.offlineRecord.Count > 0 || taskData.offlineReward.Count > 0)
                {
                    if (GameSceneManager.GetInstance().CanShowNotice())
                    {
                        PanelIdEnum.TeamTaskOfflineReward.Show();
                    }
                }
            }
        }

        private void OnTeamTaskReceiveResp(error_code errorCode, byte[] bytes)
        {
            switch (errorCode)
            {
                case error_code.ERR_SPACETIME_RIFT_FAR_CAPTAIN://{name_list}(有玩家不在队长身边)
                    PbFarRoleInfo roleInfo = MogoProtoUtils.ParseProto<PbFarRoleInfo>(bytes);
                    string nameList = string.Empty;
                    for (int i = 0; i < roleInfo.role_list.Count; i++)
                    {
                        if (i != 0)
                        {
                            nameList += "、";
                        }
                        nameList += MogoProtoUtils.ParseByteArrToString(roleInfo.role_list[i].role_name);
                    }
                    /*//ari 
              //ari MessageBox.Show(true, string.Empty, (115001).ToLanguage(nameList), delegate()
                    {
                        for (int i = 0; i < roleInfo.role_list.Count; i++)
                        {
                            TeamManager.Instance.TeamCallOne(roleInfo.role_list[i].role_dbid);
                        }
                    });*/
                    MogoUtils.FloatTips(115001, nameList);
                    //name_list
                    break;
                case error_code.ERR_SPACETIME_RIFT_SYNC_OK://（自己的任务数据和队长一致）
                    TaskData taskData = PlayerDataManager.Instance.TaskData;
                    PbSpacetimeRiftInfo info = MogoProtoUtils.ParseProto<PbSpacetimeRiftInfo>(bytes);
                    taskData.task_place_pos = new Vector3(info.rand_posi.map_x, info.rand_posi.map_y, info.rand_posi.map_z);
                    EventDispatcher.TriggerEvent(TaskEvent.CREATE_TEAM_TASK_ENTERANCE, taskData);
                    MogoUtils.FloatTips(115026);//接受任务成功！
                    if (PlayerDataManager.Instance.TeamData.CaptainId != PlayerAvatar.Player.dbid)
                    {
                        if (PlayerAutoFightManager.GetInstance().fightType == FightType.FOLLOW_FIGHT)
                        {
                            PlayerAutoFightManager.GetInstance().Stop();
                        }
                    }
                    break;
                case error_code.ERR_SUCCESSFUL://（队长收到表示所有玩家的数据同步完毕)
                    // PlayerDataManager.Instance.TaskData.isTeammateAllReady = true;
                    break;
                default:
                    break;
            }
        }

        private void OnTeamTaskStartResp(error_code errorCode, byte[] byteData)
        {
            if (errorCode == error_code.ERR_SUCCESSFUL)
            {
                taskProgressChangeList.Clear();
                PbSpacetimeRiftInfo info = MogoProtoUtils.ParseProto<PbSpacetimeRiftInfo>(byteData);
                TaskData taskData = PlayerDataManager.Instance.TaskData;
                taskData.task_place_Id = (int)info.task_place_Id;

                taskData.task_id = (int)info.task_id;

                PbTaskList list = new PbTaskList();
                PbTaskInfo taskInfo = new PbTaskInfo();
                taskInfo.task_id = taskData.task_id;
                taskInfo.status = public_config.TASK_STATE_NEW;
                list.task_info.Add(taskInfo);

                taskData.min_level = (int)info.lowest_level;
                taskData.ring = (int)info.current_ring;
                EventDispatcher.TriggerEvent(TaskEvent.RING_CHANGE);
                EventDispatcher.TriggerEvent(TaskEvent.DESTROY_TEAM_TASK_ENTERANCE);
                OnTaskListUpdate(list,taskProgressChangeList);
            }
            else
            {
                PbRoleName info = MogoProtoUtils.ParseProto<PbRoleName>(byteData);
                string name = MogoProtoUtils.ParseByteArrToString(info.role_name);
                //  LoggerHelper.Info("玩家:"+info.role_dbid+","+name);
                MogoUtils.FloatTips(115031, name);
            }

        }

        private List<int> taskProgressChangeList = new List<int>();
        public void OnTaskResp(UInt32 actionId, UInt32 errorId, byte[] byteData)
        {
            if (errorId > 0)
            {
                return;
            }
            if (actionId == (int)action_config.TASK_GETCOMPLETED)
            {
                OnTaskCompletedListUpdate(byteData);
            }
            else
            {
                PbTaskList taskList = MogoProtoUtils.ParseProto<PbTaskList>(byteData);
                taskProgressChangeList.Clear();
                for (int i = 0; i < taskList.task_info.Count;i++ )
                {
                    PbTaskInfo taskInfo = taskList.task_info[i];
                    PbTaskInfo currentTaskInfo = null;
                    if(PlayerDataManager.Instance.TaskData.mainTaskInfo == taskInfo)
                    {
                        currentTaskInfo = PlayerDataManager.Instance.TaskData.mainTaskInfo;
                        
                    }
                    else
                    {
                        if(PlayerDataManager.Instance.TaskData.branchTaskDic.ContainsKey(taskInfo.task_id))
                        {
                            currentTaskInfo = PlayerDataManager.Instance.TaskData.branchTaskDic[taskInfo.task_id];
                        }
                    }

                    if (currentTaskInfo!=null && currentTaskInfo.task_id == taskInfo.task_id && currentTaskInfo.status == taskInfo.status)
                    {
                        taskProgressChangeList.Add(taskInfo.task_id);
                    }
                }
                OnTaskListUpdate(taskList,taskProgressChangeList);
               
            }
        }

        public void OnZeroClockResp()
        {
            //ari NpcTaskHandle.OnZeroClockResp();
            //ari NpcDialogPanel.OnZeroClockResp();
        }

        private void OnTaskCompletedListUpdate(byte[] byteData)
        {
            PbTaskCompleted completedList = MogoProtoUtils.ParseProto<PbTaskCompleted>(byteData);
            PlayerDataManager.Instance.TaskData.InitCompletedTask(completedList);
            List<int> completedIdList = new List<int>();
            for (int i = 0; i < completedList.completed.Count; i++)
            {
                completedIdList.Add(completedList.completed[i].value);
            }
            if (completedIdList.Count > 0)
            {
                EventDispatcher.TriggerEvent<List<int>>(SettingEvents.INIT_TASK_COMPLETED, completedIdList);
            }
        }

        private void UpdateTaskData(PbTaskInfo info, task_data data)
        {
            Dictionary<string, string> dict = data_parse_helper.ParseMap(data.__target);
            if ((dict == null || dict.Count == 0) && info.status == public_config.TASK_STATE_ACCEPT)
            {
                info.status = public_config.TASK_STATE_ACCOMPLISH;
            }

            if (info.status == public_config.TASK_STATE_NEW && data.__npc_id_accept == 0)
            {
                info.status = public_config.TASK_STATE_ACCEPT;
            }

            if (data.__task_type == public_config.TASK_TYPE_MAIN)
            {
                PlayerDataManager.Instance.TaskData.mainTaskInfo = info;
            }
            else
            {
                if (info.status >= public_config.TASK_STATE_REWARDED)
                {
                    PlayerDataManager.Instance.TaskData.branchTaskDic.Remove(data.__id);
                }
                else if (PlayerDataManager.Instance.TaskData.branchTaskDic.ContainsKey(info.task_id))
                {
                    PlayerDataManager.Instance.TaskData.branchTaskDic[info.task_id] = info;
                }
                else
                {
                    PlayerDataManager.Instance.TaskData.branchTaskDic.Add(info.task_id, info);
                }
            }
            OnTaskChange(info.task_id, info.status);
        }

        private void OnGetTaskListResp(byte[] byteData)
        {
            PbTaskList taskList = MogoProtoUtils.ParseProto<PbTaskList>(byteData);

            for (int i = 0; i < taskList.task_info.Count; i++)
            {
                PbTaskInfo info = taskList.task_info[i];
                task_data data = task_data_helper.GetTaskData(info.task_id);
                if (data != null)
                {
                    UpdateTaskData(info, data);
                }
            }
            PlayerDataManager.Instance.TaskData.InitMainTaskAllCompleteData(PlayerDataManager.Instance.TaskData.mainTaskInfo);
        }


        private void OnAcceptTaskResp(byte[] byteData)
        {
            PbTaskList taskList = MogoProtoUtils.ParseProto<PbTaskList>(byteData);
            for (int i = 0; i < taskList.task_info.Count; i++)
            {
                PbTaskInfo info = taskList.task_info[i];
                task_data data = task_data_helper.GetTaskData(info.task_id);
                if (data != null)
                {
                    UpdateTaskData(info, data);
                    EventDispatcher.TriggerEvent(TaskEvent.TASK_ACCEPTED, data.__id);
                    EventDispatcher.TriggerEvent<int, string>(SpaceActionEvents.SpaceActionEvents_Cond, InstanceDefine.COND_TYPE_TASK_ACCEPTED, data.__id.ToString());
                }
            }
            EventDispatcher.TriggerEvent(TaskEvent.TASK_MAIN_TASK_CHANGE,true);
            CreatureDataManager.GetInstance().RefreshMonsterSet();
        }

        private void OnTaskProgressChange(byte[] byteData)
        {
            EventDispatcher.TriggerEvent(TaskEvent.TASK_MAIN_TASK_CHANGE,true);
            CreatureDataManager.GetInstance().RefreshMonsterSet();
        }

        private void OnFinishTaskResp(byte[] byteData)
        {
            PbTaskList taskList = MogoProtoUtils.ParseProto<PbTaskList>(byteData);
            for (int i = 0; i < taskList.task_info.Count; i++)
            {
                PbTaskInfo info = taskList.task_info[i];
                task_data data = task_data_helper.GetTaskData(info.task_id);
                if (data != null)
                {
                    UpdateTaskData(info, data);
                    if (info.status == public_config.TASK_STATE_REWARDED)
                    {
                        PlayerDataManager.Instance.TaskData.AddCompletedTask(data.__id);
                        EventDispatcher.TriggerEvent<int>(TaskEvent.TASK_FINISH, data.__id);
                        EventDispatcher.TriggerEvent<int, string>(SpaceActionEvents.SpaceActionEvents_Cond, InstanceDefine.COND_TYPE_TASK_FINISH, data.__id.ToString());
                        player_helper.CheckTask(data.__id);
                        OnTaskComplete();
                    }
                }
            }
            EventDispatcher.TriggerEvent(TaskEvent.TASK_MAIN_TASK_CHANGE,true);
            CreatureDataManager.GetInstance().RefreshMonsterSet();
        }

        private void OnRemoveTaskResp(byte[] byteData)
        {
            PbTaskList taskList = MogoProtoUtils.ParseProto<PbTaskList>(byteData);
            for (int i = 0; i < taskList.task_info.Count; i++)
            {
                PbTaskInfo info = taskList.task_info[i];
                task_data data = task_data_helper.GetTaskData(info.task_id);
                if (data != null)
                {
                    if (data.__task_type != public_config.TASK_TYPE_MAIN)
                    {
                        UpdateTaskData(info, data);
                        PlayerDataManager.Instance.TaskData.RemoveDialog(info.task_id);
                    }
                }
            }
        }

        private void OnTaskListUpdate(PbTaskList taskList, List<int> taskProgressChangeList)
        {
            acceptList.Clear();
            finishList.Clear();
            reachedList.Clear();
            bool isMainTaskChange = false;
            bool isBranchTaskChange = false;
            TaskData taskData = PlayerDataManager.Instance.TaskData;
            foreach (PbTaskInfo info in taskList.task_info)
            {
                task_data data = task_data_helper.GetTaskData(info.task_id);
                if (data != null)
                {
                  //  Debug.LogError("UpdateTask:" + data.__id + "," + info.status + "," + data.__task_type);
                    Dictionary<string, string> dict = data_parse_helper.ParseMap(data.__target);
                    if ((dict == null || dict.Count == 0) && info.status == public_config.TASK_STATE_ACCEPT)
                    {
                        info.status = public_config.TASK_STATE_ACCOMPLISH;
                    }
                    if (data.__task_type == public_config.TASK_TYPE_TEAM)
                    {
                        if (info.status == public_config.TASK_STATE_ACCOMPLISH)
                        {
                            taskData.task_place_Id = 0;
                            EventDispatcher.TriggerEvent(TaskEvent.DESTROY_TEAM_TASK_ENTERANCE);
                        }
                        if ((info.status == public_config.TASK_STATE_REWARDED && taskData.teamTaskInfo != null && info.task_id == taskData.teamTaskInfo.task_id) || (info.status == public_config.TASK_STATE_REMOVE))
                        {
                            if (taskData.teamTaskInfo != null)
                            {
                                taskData.branchTaskDic.Remove(taskData.teamTaskInfo.task_id);
                            }
                            taskData.teamTaskInfo = null;
                        }
                        else
                        {
                            if (taskData.teamTaskInfo != null)
                            {
                                taskData.branchTaskDic.Remove(taskData.teamTaskInfo.task_id);
                            }
                            taskData.teamTaskInfo = info;
                        }
                    }

                    if (data.__task_type == public_config.TASK_TYPE_MAIN)
                    {
                        List<int> listInt = data_parse_helper.ParseListInt(data.__next_task);
                        if (listInt == null || listInt.Count == 0 || listInt[0] == 0 || task_data_helper.GetTaskData(listInt[0]) == null)
                        {
                            if (PlayerDataManager.Instance.TaskData.mainTaskInfo == null)
                            {
                                PlayerDataManager.Instance.TaskData.InitAllCompletedMainTask(info.task_id);
                            }
                            else
                            {
                                isMainTaskChange = true;
                                AddMainTask(info, data.__task_type);
                            }
                        }
                        else
                        {
                            isMainTaskChange = true;
                            AddMainTask(info, data.__task_type);
                        }
                    }
                    else
                    {
                        isBranchTaskChange = true;
                        if (data.__task_type != public_config.TASK_TYPE_TEAM && info.status == public_config.TASK_STATE_NEW && data.__npc_id_accept == 0)
                        {
                            info.status = public_config.TASK_STATE_ACCEPT;
                        }
                        AddBranchTask(info, taskData.branchTaskDic, data.__task_type);
                    }
                }
                else
                {
                    Debug.LogError("不存在任务:" + info.task_id);
                }
            }

            foreach (int taskId in acceptList)
            {
                EventDispatcher.TriggerEvent(TaskEvent.TASK_ACCEPTED, taskId);
                EventDispatcher.TriggerEvent<int, string>(SpaceActionEvents.SpaceActionEvents_Cond, InstanceDefine.COND_TYPE_TASK_ACCEPTED, taskId.ToString());
                //Debug.Log("接受任务:"+taskId);                
            }
            foreach (int taskId in finishList)
            {
                PlayerDataManager.Instance.TaskData.AddCompletedTask(taskId);
                EventDispatcher.TriggerEvent(TaskEvent.TASK_FINISH, taskId);
                EventDispatcher.TriggerEvent<int, string>(SpaceActionEvents.SpaceActionEvents_Cond, InstanceDefine.COND_TYPE_TASK_FINISH, taskId.ToString());
                player_helper.CheckTask(taskId);
                //Debug.Log("完成任务:" + taskId);
                OnTaskComplete();
            }
            foreach (int taskId in reachedList)
            {
                EventDispatcher.TriggerEvent(TaskEvent.TASK_REACHED, taskId);
            }
            if (isBranchTaskChange)
            {
                EventDispatcher.TriggerEvent(TaskEvent.TASK_BRANCH_TASK_CHANGE, taskProgressChangeList.Count == 0);//
                CreatureDataManager.GetInstance().RefreshMonsterSet();
            }
            if (isMainTaskChange)
            {
                EventDispatcher.TriggerEvent(TaskEvent.TASK_MAIN_TASK_CHANGE, taskProgressChangeList.Count == 0);//
                CreatureDataManager.GetInstance().RefreshMonsterSet();
            }
            for(int i=0;i<taskProgressChangeList.Count;i++)
            {
                EventDispatcher.TriggerEvent(TaskEvent.TASK_PROGRESS_CHANGE, taskProgressChangeList[i]);
            }
            CheckAutoTask();
        }

        private int _autoTaskState = 0;
       
        private const int CAN_AUTO_TASK = 0;//自动任务
        private const int STOP_AUTO_TASK = 1;//自动任务状态暂停

        public void IdleAutoTask()
        {
            _autoTaskState = STOP_AUTO_TASK;
        }

        public void ContinueAutoTask()
        {
            if (PlayerAvatar.Player.canAutoTask)
            {
                _autoTaskState = CAN_AUTO_TASK;
            }
        }


        public void Print()
        {
            Debug.LogError(_autoTaskState);
            Debug.LogError(PlayerDataManager.Instance.TaskData.mainTaskInfo == null);
            Debug.LogError(GameSceneManager.GetInstance().CanShowNotice() == false);
            Debug.LogError(BaseModule.HasFullScreenPanel());
            Debug.LogError(PlayerAvatar.Player.isActiveMoving);
        }

        public void CheckAutoTask()
        {
            /*ari
            if (autoTaskStart == false || _autoTaskState != CAN_AUTO_TASK || GuidePanel.isShow || FunctionPanel.isShow || CGDialogPanel.isShow || PCFingerGuidePanel.isShow || FingerGuidePanel.isShow || PlayerDataManager.Instance.TaskData.mainTaskInfo == null || GameSceneManager.GetInstance().CanShowNotice() == false || BaseModule.HasFullScreenPanel())
            {
                return;
            }
            task_data autoTask = task_data_helper.GetTaskData(PlayerDataManager.Instance.TaskData.mainTaskInfo.task_id);
            List<int> listInt = data_parse_helper.ParseListInt(autoTask.__last_task);
            if (listInt.Count > 0)
            {
                PbTaskInfo taskInfo = PlayerDataManager.Instance.TaskData.mainTaskInfo;
                if (taskInfo.status == public_config.TASK_STATE_ACCEPT)
                {
                    Dictionary<string, string[]> taskFollow = task_data_helper.GetTaskFollow(autoTask.__id);
                    bool canAutoTask = false;
                    if (taskFollow != null)
                    {
                        foreach (KeyValuePair<string, string[]> kvp in taskFollow)
                        {
                            switch (kvp.Key)
                            {
                                case TaskFollowHandle.OPEN_MAP:
                                    Dictionary<TaskEventEnum, TaskEventData> dic = task_data_helper.GetTaskEventData(autoTask.__id);
                                    if (dic.ContainsKey(TaskEventEnum.MonsterId))
                                    {
                                        canAutoTask = true;
                                    }
                                    break;
                                case TaskFollowHandle.GOTO_MISSION:
                                    canAutoTask = true;
                                    break;
                                case TaskFollowHandle.GOTO_NPC:
                                    canAutoTask = true;
                                    break;
                            }
                        }
                    }
                    if(canAutoTask)
                    {
                        _autoTaskState = STOP_AUTO_TASK;
                       // Debug.LogError("auto task:" + autoTask.__id + "," + PlayerDataManager.Instance.TaskData.mainTaskInfo.status);
                        TaskFollowHandle.TaskFollow(autoTask,true);
                    }
                }
                else if (taskInfo.status == public_config.TASK_STATE_ACCOMPLISH || taskInfo.status == public_config.TASK_STATE_NEW)
                {
                    _autoTaskState = STOP_AUTO_TASK;
                   // Debug.LogError("auto task:" + autoTask.__id + "," + PlayerDataManager.Instance.TaskData.mainTaskInfo.status);
                    TaskFollowHandle.TaskFollow(autoTask,true);
                }
            }*/
        }

        public void Clear()
        {
            PlayerDataManager.Instance.TaskData.lastFinishMainTaskId = -1;
            PlayerDataManager.Instance.TaskData.mainTaskInfo = null;
            set.Clear();
        }

        private HashSet<int> set = new HashSet<int>();

        public bool autoTaskStart = false;
        private void AddMainTask(PbTaskInfo info, int taskType)
        {
            if(PlayerDataManager.Instance.TaskData.mainTaskInfo!=null)
            {
                autoTaskStart = true;
            }
            if (PlayerDataManager.Instance.TaskData.mainTaskInfo == null || PlayerDataManager.Instance.TaskData.mainTaskInfo.task_id != info.task_id || PlayerDataManager.Instance.TaskData.mainTaskInfo.status != info.status)
            {
                ContinueAutoTask();
            }
            PbTaskInfo preInfo = null;
            if (set.Contains(info.task_id) == false)
            {
                preInfo = PlayerDataManager.Instance.TaskData.mainTaskInfo;
            }
            else
            {
                if (PlayerDataManager.Instance.TaskData.mainTaskId == info.task_id)
                {
                    preInfo = PlayerDataManager.Instance.TaskData.mainTaskInfo;
                }
                else
                {
                    preInfo = info;
                }
            }
            if (PlayerDataManager.Instance.TaskData.mainTaskId == info.task_id)
            {
                if (info.status != public_config.TASK_STATE_REWARDED)
                {
                    PlayerDataManager.Instance.TaskData.mainTaskInfo = info;
                }
                else
                {
                    PlayerDataManager.Instance.TaskData.mainTaskInfo = null;
                }
            }
            else
            {
                if (set.Contains(info.task_id) == false)
                {
                    set.Add(info.task_id);
                    PlayerDataManager.Instance.TaskData.mainTaskInfo = info;
                }
            }
            
            if (info.status == public_config.TASK_STATE_REWARDED)
            {
                PlayerDataManager.Instance.TaskData.RemoveDialog(info.task_id);
            }
            OnTaskStateHandle(info, preInfo, taskType);
        }

        private void OnTaskStateHandle(PbTaskInfo info, PbTaskInfo preInfo, int taskType)
        {
            OnTaskChange(info.task_id, info.status);
            Dictionary<TaskEventEnum, TaskEventData> dicInfo = task_data_helper.GetTaskEventData(info.task_id);
            if (dicInfo.ContainsKey(TaskEventEnum.InstId))
            {
                int instanceId = (int)dicInfo[TaskEventEnum.InstId].value;
                taskIntanceInfo.Remove(instanceId);
                if (taskIntanceInfo.ContainsKey(instanceId) && taskType != public_config.TASK_TYPE_BRANCH)
                {
                    taskType = public_config.TASK_TYPE_MAIN;
                }
                taskIntanceInfo.Add(instanceId, info.task_id);
            }
            if (preInfo != null)
            {
                if (preInfo.status == public_config.TASK_STATE_NEW)
                {
                    acceptList.Add(info.task_id);
                }
                else if (info.status == public_config.TASK_STATE_REWARDED)
                {
                    if (dicInfo.ContainsKey(TaskEventEnum.InstId))
                    {
                        int instanceId = (int)dicInfo[TaskEventEnum.InstId].value;
                        if (taskIntanceInfo.ContainsKey(instanceId))
                        {
                            taskIntanceInfo.Remove(instanceId);
                        }
                    }
                    finishList.Add(info.task_id);
                }
                else if (info.status == public_config.TASK_STATE_ACCOMPLISH)
                {
                    reachedList.Add(info.task_id);
                }
            }
        }


        private void AddBranchTask(PbTaskInfo info, Dictionary<int, PbTaskInfo> dic, int taskType)
        {
            PbTaskInfo preInfo = null;
            if (dic.ContainsKey(info.task_id))
            {
                preInfo = dic[info.task_id];
            }
            dic.Remove(info.task_id);
            if (info.status == public_config.TASK_STATE_REMOVE)
            {
                PlayerDataManager.Instance.TaskData.RemoveDialog(info.task_id);
                return;
            }
            //if (info.status <= public_config.TASK_STATE_ACCOMPLISH)
            //{
            //    PlayerDataManager.Instance.TaskData.AddAcceptableTask(info.task_id);
            //}
            if (info.status != public_config.TASK_STATE_REWARDED)
            {
                dic.Add(info.task_id, info);
            }
            else
            {
                PlayerDataManager.Instance.TaskData.RemoveDialog(info.task_id);
            }
            OnTaskStateHandle(info, preInfo, taskType);
        }

        public Dictionary<int, int> GetTaskIntanceInfo()
        {
            return taskIntanceInfo;
        }

        private void OnTaskComplete()
        {
            //ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, "完成任务", Common.Base.PanelIdEnum.Task);
            //ari UIManager.Instance.ShowPanel(PanelIdEnum.TaskParticle, TaskParticleType.Finish);
        }


        private void OnTaskChange(int taskId, int taskState)
        {
            /*
            task_data data = task_data_helper.GetTaskData(taskId);
            if (taskState == public_config.TASK_STATE_NEW)//可接
            {
                for (int i = 0; i < data.__last_task.Count;i++ )
                {
                    task_data lastTask = task_data_helper.GetTaskData(int.Parse(data.__last_task[i]));
                    NpcTaskHandle.RemoveTaskFromNpc(lastTask.__npc_id_accept, lastTask.__id);
                    NpcTaskHandle.RemoveTaskFromNpc(lastTask.__npc_id_report, lastTask.__id);
                }
                NpcTaskHandle.RemoveTaskFromNpc(data.__npc_id_report, taskId);
                NpcTaskHandle.AddTaskToNpc(data.__npc_id_accept, taskId, public_config.TASK_STATE_NEW);
            }
            else if (taskState == public_config.TASK_STATE_ACCOMPLISH)//可完成
            {
                NpcTaskHandle.RemoveTaskFromNpc(data.__npc_id_accept, taskId);
                NpcTaskHandle.AddTaskToNpc(data.__npc_id_report, taskId, public_config.TASK_STATE_ACCOMPLISH);
            }
            else
            {
                NpcTaskHandle.RemoveTaskFromNpc(data.__npc_id_accept, taskId);
                NpcTaskHandle.RemoveTaskFromNpc(data.__npc_id_report, taskId);
            }*/
        }

    }
}
