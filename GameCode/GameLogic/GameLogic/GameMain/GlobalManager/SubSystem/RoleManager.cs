﻿using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;

namespace GameMain.GlobalManager.SubSystem
{
    public class RoleManager : Singleton<RoleManager> 
    {
        /// <summary>
        /// 返回时装数据
        /// </summary>
        /// <param name="pbFashionList"></param>
        public void RespUpdateData(byte[] byteArr)
        {
            PbFashionList pbFashionList = MogoProtoUtils.ParseProto<PbFashionList>(byteArr);
            PlayerDataManager.Instance.FashionData.Fill(pbFashionList);
            EventDispatcher.TriggerEvent(FashionEvents.UPDATE_FASHION_VIEW);
        }

        /// <summary>
        /// 时装通知
        /// </summary>
        /// <param name="noticeType">消息类型:1购买,2过期,3续期,4穿戴</param>
        /// <param name="fashionId">时装ID</param>
        /// <param name="validTime">剩余过期时间</param>
        public void Notice(byte noticeType, int fashionId, uint validTime)
        {
            //UnityEngine.Debug.LogError("noticeType : " + noticeType + " fashionId : " + fashionId + " validTime : " + validTime);
            switch (noticeType)
            {
                case 1:
                    PlayerDataManager.Instance.FashionData.FashionItemRenewal(fashionId, validTime);
                    EventDispatcher.TriggerEvent<int>(FashionEvents.CAN_PUT_ON_NEW_FASHION, fashionId);
                    break;
                case 2:
                    PlayerDataManager.Instance.FashionData.FashionItemExpire(fashionId);
                    EventDispatcher.TriggerEvent<int>(FashionEvents.PUT_ON_FASHION, 0);
                    break;
                case 3:
                    PlayerDataManager.Instance.FashionData.FashionItemRenewal(fashionId, validTime);
                    break;
                case 4:
                    PlayerDataManager.Instance.FashionData.SetCurrentFashionId(fashionId);
                    EventDispatcher.TriggerEvent<int>(FashionEvents.PUT_ON_FASHION, fashionId);
                    break;
            }
            EventDispatcher.TriggerEvent(FashionEvents.REFRESH_FASHION_LIST);
        }

        /// <summary>
        /// 请求更换时装
        /// </summary>
        /// <param name="fashionId"></param>
        public void ReqSetCurrentFashion(int fashionId)
        {
            RPC(action_config.ACTION_FASHION_SETCURRENT, (UInt32)fashionId);
        }

        /// <summary>
        /// 请求时装列表
        /// </summary>
        public void ReqGetFashionInfo()
        {
            RPC(action_config.ACTION_FASHION_GETINFO, (UInt32)0);
        }

        private void RPC(action_config actionId, UInt32 fashionId)
        {
            PlayerAvatar.Player.RpcCall("fashion_req", (UInt32)actionId, fashionId);
        }

    }
}
