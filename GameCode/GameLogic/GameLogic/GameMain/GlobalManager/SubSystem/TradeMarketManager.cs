﻿#region 模块信息
/*==========================================
// 模块名：TradeMarketManager
// 创建者：andy
// 修改者列表：
// 创建日期：2015/03/13
// 描述说明：处理交易中心相关的协议
// 其他：需要在PlayerDataManager中注册
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Text;

using GameMain.Entities;
using Common.Data;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using MogoEngine.Events;
using Common.Events;
using GameLoader.Utils.CustomType;
using UnityEngine;
using Game.UI.UIComponent;
using Common.Utils;
using GameData;


namespace GameMain.GlobalManager.SubSystem
{
    public class TradeMarketManager : Singleton<TradeMarketManager> 
    {

        //发送请求到服务器
        private void RPC(action_config actionId, params object[] args)
        {
            PlayerAvatar.Player.ActionRpcCall("auction_req", (int)(actionId), args);
        }

        public void RequestPutOutItem(int bagPos, int price, int num, int gridIndex, int time)
        {
            action_config.ACTION_AUCTION_SELL.Register();
            RPC(action_config.ACTION_AUCTION_SELL, bagPos, price, num, gridIndex, time);
        }

        public void RequestSellItems()
        {
            RPC(action_config.ACTION_AUCTION_QUERY_SELF_SELL);
        }

        public void RequestLeftMoney()
        {
            RPC(action_config.ACTION_AUCTION_LEFT_MONEY);
        }

        public void RequestExtendGrid(int count)
        {
            action_config.ACTION_AUCTION_EXTEND_GRID.Register();
            RPC(action_config.ACTION_AUCTION_EXTEND_GRID, count);
        }

        /// <summary>
        /// 请求搜索结果
        /// </summary>
        /// <param name="itemType"></param>
        /// <param name="searchInfo"></param>
        /// <param name="page"></param>
        public void RequestSearchItems(int itemType, List<FilterData> searchInfo)
        {
            PlayerDataManager.Instance.AuctionData.ResetTradeItemData(itemType);
            RequestSearchItemByPage(itemType, searchInfo, 1);
        }

        /// <summary>
        /// 请求指定条件的指定页
        /// </summary>
        /// <param name="itemType"></param>
        /// <param name="searchInfo"></param>
        /// <param name="page"></param>
        public void RequestSearchItemByPage(int itemType, List<FilterData> searchInfo, int page)
        {
            LuaTable result = FillSearchInfo(itemType, searchInfo, page);
            RPC(action_config.ACTION_AUCTION_SEARCH, result, page);
        }

        public void RequestPullItem(int gridPostion)
        {
            RPC(action_config.ACTION_AUCTION_SELL_CANCEL, gridPostion);
        }

        public void RequestTakeOutMoney()
        {
            RPC(action_config.ACTION_AUCTION_TAKEOUT_MONEY);
        }

        public void RequestBuyItem(int itemId, int count, int price)
        {
            action_config.ACTION_AUCTION_BUY.Register();
            RPC(action_config.ACTION_AUCTION_BUY, itemId, count, price);
        }

        public void RequestAuctionInfo()
        {
            RPC(action_config.ACTION_AUCTION_QUERY_GLOBAL_INFO);
        }

        public void RequestSelledCount(int itemId)
        {
            RPC(action_config.ACTION_AUCTION_ITEM_SELLED_COUNT, itemId);
        }

        public void RequestWantAvatarNum(List<int> itemIdList)
        {
            LuaTable itemIdTable = new LuaTable();
            for (int i = 0; i < itemIdList.Count; i++)
            {
                itemIdTable.Add(i + 1, itemIdList[i]);
            }
            RPC(action_config.ACTION_AUCTION_GET_ITEMS_WANT_BUY, itemIdTable);
        }

        public void RequestBuyedCount(int itemId)
        {
            RPC(action_config.ACTION_AUCTION_ITEM_BUYED_COUNT, itemId);
        }

        public void RequestRePutOutItem(int gridId, int price, int saveTime, int itemId, int count)
        {
            RPC(action_config.ACTION_AUCTION_RESELL_ITEM, gridId, price, saveTime, itemId, count);
        }

        public void RequestWantToBuyList()
        {
            RPC(action_config.ACTION_AUCTION_GET_WANT_BUY_INFO);
        }

        public void ChangeWantToBuyState(int itemId, int whetherOrNot)
        {
            RPC(action_config.ACTION_AUCTION_CHANGE_WANT_BUY, itemId, whetherOrNot);
        }

        public void RequestBuyRecord()
        {
            RPC(action_config.ACTION_AUCTION_BUY_RECORD);
        }

        public void RequestSellRecord()
        {
            RPC(action_config.ACTION_AUCTION_SELL_RECORD);
        }

        private LuaTable FillSearchInfo(int itemType, List<FilterData> searchInfo, int page)
        {
            LuaTable result = new LuaTable();
            result.Add(public_config.SEARCH_CND_TYPE, itemType);
            if (searchInfo != null)
            {
                int count = searchInfo.Count;
                for (int i = 0; i < count; i++)
                {
                    result.Add(searchInfo[i].FilterType, searchInfo[i].FilterValue);
                }
            }
            return result;
        }

        //处理服务器返回的各种操作回应
        public void AuctionResp(UInt32 actionId, UInt32 errorId, byte[] byteArr)
        {
            switch ((error_code)errorId)
            {
                case error_code.ERR_AUCTION_NOT_ENOUGH_ITEM:
                    SystemInfoManager.Instance.Show(664);
                    EventDispatcher.TriggerEvent(TradeMarketEvents.SendSearch);
                    break;
            }
            if (errorId != 0){
                return;
            }
            switch ( (action_config)actionId )
            {
                case action_config.ACTION_AUCTION_SEARCH :
                    PbMarketItems marketItems = MogoProtoUtils.ParseProto<PbMarketItems>(byteArr);
                    PlayerDataManager.Instance.AuctionData.RefreshTradeItemsInfo(marketItems);
                    EventDispatcher.TriggerEvent(TradeMarketEvents.RefreshBuyView);
                    break;
                case action_config.ACTION_AUCTION_BUY:
                    PbBuyItem buyItem = MogoProtoUtils.ParseProto<PbBuyItem>(byteArr);
                    PlayerDataManager.Instance.AuctionData.BuyTradeItemCount((int)buyItem.type_id, buyItem.count);
                    EventDispatcher.TriggerEvent<int>(TradeMarketEvents.UpdateBuyItem, (int)buyItem.type_id);
                    break;
                case action_config.ACTION_AUCTION_QUERY_SELF_SELL:
                    PbPutawayItems items = MogoProtoUtils.ParseProto<PbPutawayItems>(byteArr);
                    PlayerDataManager.Instance.PutOutData.Fill(items);
                    EventDispatcher.TriggerEvent(TradeMarketEvents.RefreshSellView);
                    break;
                case action_config.ACTION_AUCTION_SELL:
                    PbPutawayItem item = MogoProtoUtils.ParseProto<PbPutawayItem>(byteArr);
                    PlayerDataManager.Instance.PutOutData.RefreshItem(item);
                    EventDispatcher.TriggerEvent<int>(TradeMarketEvents.UpdateSellGrid, item.grid);
                    break;
                case action_config.ACTION_AUCTION_EXTEND_GRID:
                    PbExtendGrid extendGrid = MogoProtoUtils.ParseProto<PbExtendGrid>(byteArr);
                    PlayerDataManager.Instance.AuctionData.RefreshExtendGrid(extendGrid);
                    EventDispatcher.TriggerEvent(TradeMarketEvents.UpdateUnlockGrid);
                    break;
                case action_config.ACTION_AUCTION_GET_WANT_BUY_INFO:
                    PbFullWantBuy fullWantBuy = MogoProtoUtils.ParseProto<PbFullWantBuy>(byteArr);
                    PlayerDataManager.Instance.AuctionData.FillWantBuySet(fullWantBuy);
                    EventDispatcher.TriggerEvent(TradeMarketEvents.UpdateWantBuyList);
                    break;
                case action_config.ACTION_AUCTION_CHANGE_WANT_BUY:
                    PbChangeWantBuy wantBuy = MogoProtoUtils.ParseProto<PbChangeWantBuy>(byteArr);
                    if (wantBuy.flag == 0)
                    {
                        PlayerDataManager.Instance.AuctionData.DeleteWantBuyItem((int)wantBuy.type_id);
                    }
                    else
                    {
                        SystemInfoManager.Instance.Show(52967, trademarket_helper.GetWantBuyUpLimit() - PlayerAvatar.Player.auction_want_buy_times);
                        PlayerDataManager.Instance.AuctionData.AddWantBuyItem((int)wantBuy.type_id);
                    }
                    break;
                case action_config.ACTION_AUCTION_SELL_CANCEL:
                    PbCancelSellItem cancelSellItem = MogoProtoUtils.ParseProto<PbCancelSellItem>(byteArr);
                    PlayerDataManager.Instance.PutOutData.RemoveItem((int)cancelSellItem.grid);
                    EventDispatcher.TriggerEvent<int>(TradeMarketEvents.CancelSellItem,(int)cancelSellItem.grid);
                    break;

                case action_config.ACTION_AUCTION_TAKEOUT_MONEY:
                    PbLeftMoney takeOutMoney = MogoProtoUtils.ParseProto<PbLeftMoney>(byteArr);
                    PlayerDataManager.Instance.AuctionData.RefreshMoney(takeOutMoney);
                    EventDispatcher.TriggerEvent(TradeMarketEvents.WithdrawCash);
                    EventDispatcher.TriggerEvent(TradeMarketEvents.RefreshLeftMoney);
                    break;

                case action_config.ACTION_AUCTION_QUERY_GLOBAL_INFO:
                    PbGlobalInfo globalInfo = MogoProtoUtils.ParseProto<PbGlobalInfo>(byteArr);
                    PlayerDataManager.Instance.AuctionData.RefreshGlobalInfo(globalInfo);
                    EventDispatcher.TriggerEvent(TradeMarketEvents.UpdateGlobalInfo);
                    break;

                case action_config.ACTION_AUCTION_ITEM_SELLED_COUNT:
                    PbItemCount itemSellCount = MogoProtoUtils.ParseProto<PbItemCount>(byteArr);
                    EventDispatcher.TriggerEvent<PbItemCount>(TradeMarketEvents.UpSellItemInfo, itemSellCount);
                    break;
                case action_config.ACTION_AUCTION_RESELL_ITEM:
                    PbPutawayItem putOutItem = MogoProtoUtils.ParseProto<PbPutawayItem>(byteArr);
                    PlayerDataManager.Instance.PutOutData.RefreshItem(putOutItem);
                    EventDispatcher.TriggerEvent<int>(TradeMarketEvents.UpdateSellGrid, putOutItem.grid);
                    break;
                case action_config.ACTION_AUCTION_ITEM_BUYED_COUNT:
                    PbItemCount itemBuyCount = MogoProtoUtils.ParseProto<PbItemCount>(byteArr);
                    PlayerDataManager.Instance.AuctionData.RefreshTradeItemBuyCount(itemBuyCount);
                    EventDispatcher.TriggerEvent<int>(TradeMarketEvents.UpdateBuyItemInfo, (int)itemBuyCount.type_id);
                    break;
                case action_config.ACTION_AUCTION_LEFT_MONEY:
                    RequestSellItems();
                    PbLeftMoney leftMoney = MogoProtoUtils.ParseProto<PbLeftMoney>(byteArr);
                    PlayerDataManager.Instance.AuctionData.RefreshMoney(leftMoney);
                    EventDispatcher.TriggerEvent(TradeMarketEvents.RefreshLeftMoney);
                    break;
                case action_config.ACTION_AUCTION_BUY_RECORD:
                    PbItemRecords buyRecords = MogoProtoUtils.ParseProto<PbItemRecords>(byteArr);
                    PlayerDataManager.Instance.AuctionData.FillBuyRecordList(buyRecords);
                    EventDispatcher.TriggerEvent(TradeMarketEvents.RefreshBuyRecord);
                    break;
                case action_config.ACTION_AUCTION_SELL_RECORD:
                    PbItemRecords sellRecords = MogoProtoUtils.ParseProto<PbItemRecords>(byteArr);
                    PlayerDataManager.Instance.AuctionData.FillSellRecordList(sellRecords);
                    EventDispatcher.TriggerEvent(TradeMarketEvents.RefreshSellRecord);
                    break;
                case action_config.ACTION_AUCTION_GET_ITEMS_WANT_BUY:
                    PbItemsWantBuy itemsWantBuy = MogoProtoUtils.ParseProto<PbItemsWantBuy>(byteArr);
                    PlayerDataManager.Instance.AuctionData.UpdateWantBuyInfo(itemsWantBuy);
                    break;
            }
        }

    }
}
