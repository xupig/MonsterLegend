﻿#region 模块信息
/*==========================================
// 文件名：EquipTipManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/5/27 9:49:16
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.Enum;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System.Collections.Generic;

namespace GameMain.GlobalSystem
{
    public enum ItemUseType
    {
        None = 0,
        AutoUse = 1,
        EnterBagNotice = 2,     // 进入背包提示
        LevelChangeNotice = 3   //等级变动之后需要判断是否提示，包括进入背包判断
    }

    public class EquipUpgradeTipManager
    {
        private List<BaseItemInfo> _itemList;
        private HashSet<int> _notShowSet;

        public EquipUpgradeTipManager()
        {
            _itemList = new List<BaseItemInfo>();
            _notShowSet = new HashSet<int>();
            InitNotShowList();

            EventDispatcher.AddEventListener<PanelIdEnum>(PanelEvents.SHOW_PANEL, OnShow);
            EventDispatcher.AddEventListener<PanelIdEnum>(PanelEvents.ENABLE_PANEL_CANVAS, OnShow);
            EventDispatcher.AddEventListener(EquipUpgradeTipEvents.Bag_Item_Update_All, CheckItemInList);
            EventDispatcher.AddEventListener<PbGridItem, int>(EquipUpgradeTipEvents.Bag_Item_Update, OnBagUpdate);
            EventDispatcher.AddEventListener(EquipUpgradeTipEvents.Close_Or_Use_Item, RemoveFirstItem);
            EventDispatcher.AddEventListener(EquipUpgradeTipEvents.Remove_Same_Item, RemoveAllSameItem);
            EventDispatcher.AddEventListener<BaseItemInfo>(EquipUpgradeTipEvents.Add_Item_To_First, AddItemToFirst);
            EventDispatcher.AddEventListener(EquipUpgradeTipEvents.Body_Item_Update, CheckItemInList);
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.level, LevelChangeCheck);
        }

        private void InitNotShowList()
        {
            string stringList = global_params_helper.GetGlobalParam(GlobalParamId.not_show_equip_list);
            string[] arrays = stringList.Split(',');
            for (int i = 0; i < arrays.Length; i++)
            {
                _notShowSet.Add(int.Parse(arrays[i]));
            }
        }

        private void Hide(MogoUILayer layer)
        {
            if (layer == MogoUILayer.LayerUIMain)
            {
                UIManager.Instance.ClosePanel(PanelIdEnum.EquipUpgradeTips);
            }
        }

        private void OnShow(PanelIdEnum panelId)
        {
            if (panelId == PanelIdEnum.MainUIField)
            {
                Show();
            }
        }

        private void Show()
        {
            if (GameSceneManager.GetInstance().curMapType == public_config.MAP_TYPE_CITY || GameSceneManager.GetInstance().curMapType == public_config.MAP_TYPE_WILD)
            {
                if (_itemList.Count > 0)
                {
                    UIManager.Instance.ShowPanel(PanelIdEnum.EquipUpgradeTips);
                }
                else
                {
                    UIManager.Instance.ClosePanel(PanelIdEnum.EquipUpgradeTips);
                }
            }
        }

        private void RemoveFirstItem()
        {
            if (_itemList.Count > 0)
            {
                _itemList.RemoveAt(0);
            }
        }

        private void RemoveAllSameItem()
        {
            if (_itemList.Count > 0)
            {
                BaseItemInfo baseItemInfo = _itemList[0];
                for (int i = _itemList.Count - 1; i >= 0; i--)
                {
                    if (_itemList[i].Id == baseItemInfo.Id)
                    {
                        _itemList.RemoveAt(i);
                    }
                }
            }
        }

        private void LevelChangeCheck()
        {
            BagData bodyEquipBag = PlayerDataManager.Instance.BagData.GetBagData(BagType.ItemBag);
            foreach (var pair in bodyEquipBag.GetAllItemInfo())
            {
                if (pair.Value is EquipItemInfo)
                {
                    EquipItemInfo equipItem = pair.Value as EquipItemInfo;
                    if (CheckEquipCanAdd(equipItem) == true)
                    {
                        AddItemToList(equipItem);
                    }
                }
                else if (pair.Value is CommonItemInfo)
                {
                    CommonItemInfo commonItem = pair.Value as CommonItemInfo;
                    if (commonItem.BaseConfig.__auto_use == (int)ItemUseType.LevelChangeNotice)
                    {
                        CheckItemCanAdd(commonItem, commonItem.StackCount);
                    }
                }
            }
        }

        private void OnBagUpdate(PbGridItem pbItem, int num)
        {
            CheckItemInList();
            if (pbItem == null || pbItem.item == null || _notShowSet.Contains((int)pbItem.item.id))
            {
                return;
            }
            if (item_helper.GetItemType((int)pbItem.item.id) == BagItemType.Equip)
            {
                EquipItemInfo equipItem = new EquipItemInfo(pbItem);
                if (CheckEquipCanAdd(equipItem) == true)
                {
                    AddItemToList(equipItem);
                }
            }
            else
            {
                CommonItemInfo commonItem = new CommonItemInfo(pbItem);
                if(commonItem.BaseConfig.__auto_use == (int)ItemUseType.EnterBagNotice
                    || commonItem.BaseConfig.__auto_use == (int)ItemUseType.LevelChangeNotice)
                {
                    CheckItemCanAdd(commonItem, num);
                }
            }
        }

        private void CheckItemInList()
        {
            for (int i = _itemList.Count - 1; i >= 0; i--)
            {
                if (CheckAgain(_itemList[i]) == false)
                {
                    _itemList.RemoveAt(i);
                }
            }
            Show();
        }

        private void CheckItemCanAdd(CommonItemInfo commonItem, int num)
        {
            if (ReachUseCondition(commonItem) == true)
            {
                for (int i = 0; i < num; i++)
                {
                    AddItemToList(commonItem);
                }
            }
        }

        private bool ReachUseCondition(CommonItemInfo commonItem)
        {
            return ReachLevelCondition(commonItem) 
                && ReachVipLevelCondition(commonItem)
                && ReachVocationCondition(commonItem);
        }

        private bool ReachLevelCondition(CommonItemInfo commonItem)
        {
            return commonItem.UsageLevelRequired <= PlayerAvatar.Player.level;
        }

        private bool ReachVipLevelCondition(CommonItemInfo commonItem)
        {
            return commonItem.UsageVipLevelRequired <= PlayerAvatar.Player.vip_level;
        }

        private bool ReachVocationCondition(CommonItemInfo commonItem)
        {
            return commonItem.UsageVocationRequired == PlayerAvatar.Player.vocation
                || commonItem.UsageVocationRequired == Vocation.None;
        }
        private void AddItemToList(BaseItemInfo item)
        {
            _itemList.Add(item);
            if (_itemList.Count == 1)
            {
                Show();
            }
        }

        private void AddItemToFirst(BaseItemInfo item)
        {
            _itemList.Insert(0, item);
            Show();
        }

        public bool CheckEquipCanAdd(EquipItemInfo item)
        {
            return item_equipment_helper.CheckEquipFightForceBetter(item) && CompareWithEquipList(item);
        }

        // 推荐最好的装备给玩家
        private bool CompareWithEquipList(EquipItemInfo equipItem)
        {
            for (int i = _itemList.Count - 1; i >= 0; i--)
            {
                if (_itemList[i] is EquipItemInfo)
                {
                    EquipItemInfo listItem = _itemList[i] as EquipItemInfo;
                    if (equipItem.SubType == listItem.SubType && equipItem.SubType != EquipType.ring)
                    {
                        if (equipItem.FightForce < listItem.FightForce)
                        {
                            return false;
                        }
                        else
                        {
                            _itemList.RemoveAt(i);
                        }
                    }
                }
            }
            return true;
        }

        public BaseItemInfo GetCurrentItemInfo()
        {
            if (_itemList.Count > 0)
            {
                if (CheckAgain(_itemList[0]) == false)
                {
                    _itemList.RemoveAt(0);
                    GetCurrentItemInfo();
                }
                else
                {
                    return _itemList[0];
                }
            }
            return null;
        }

        private bool CheckAgain(BaseItemInfo baseItemInfo)
        {
            if (baseItemInfo is EquipItemInfo)
            {
                if (PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(baseItemInfo.Id) <= 0)
                {
                    return false;
                }
                return item_equipment_helper.CheckEquipFightForceBetter(baseItemInfo as EquipItemInfo);
            }
            else if (baseItemInfo is CommonItemInfo)
            {
                return PlayerDataManager.Instance.BagData.GetItemBagData().GetItemNum(baseItemInfo.Id) > 0;
            }
            return true;
        }
    }
}
