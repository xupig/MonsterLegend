﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using ModuleCommonUI;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class DuelManager : Singleton<DuelManager>
    {
        private PlayerAvatar _player;
        private DuelData _duelData;

        public DuelManager() : base()
        {
            _player = MogoWorld.Player as PlayerAvatar;
            _duelData = PlayerDataManager.Instance.DuelData;
        }

        public void RequestQueryInfo()
        {
            RPC(action_config.ACTION_DUEL_QUERY_INFO);
        }

        public void RequestEnter()
        {
            if (GameSceneManager.GetInstance().chapterType == ChapterType.DuelWait)
            {
                //ari//ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(122134));
                return;
            }
            RPC(action_config.ACTION_DUEL_ENTER_MAP);
        }

        public void RequestSearch()
        {
            RPC(action_config.ACTION_DUEL_GET_INVITE_LIST);
        }

        public void RequestInvite(ulong dbid)
        {
            RPC(action_config.ACTION_DUEL_INVITE, dbid);
        }

        public void RequestQueryPlayerInfo(ulong dbid)
        {
            RPC(action_config.ACTION_DUEL_QUERY_PLAYER_INFO, dbid);
        }

        public void RequestInviteResp(bool agree)
        {
            RPC(action_config.ACTION_DUEL_INVITE_RESP, agree ? 0 : 1);
        }

        public void RequestReady()
        {
            RPC(action_config.ACTION_DUEL_READY);
        }

        public void RequestGotReward()
        {
            RPC(action_config.ACTION_DUEL_GET_STAGE_REWARD);
        }

        public void RequestStakeInfo(string name, int matchId = 0)
        {
            RPC(action_config.ACTION_DUEL_GET_GAMBLE_INFO, name, matchId);
        }

        public void RequestStakePartInfo(int matchId)
        {
            RPC(action_config.ACTION_DUEL_GET_GAMBLE_STATE, matchId);
        }

        public void RequestBet(int matchId, ulong dbid)
        {
            RPC(action_config.ACTION_DUEL_BET1, matchId, dbid);
        }

        public void RequestSpecialBet(int matchId, ulong dbid)
        {
            RPC(action_config.ACTION_DUEL_BET2, matchId, dbid);
        }

        public void RequestDuelReport(ulong dbid)
        {
            RPC(action_config.ACTION_DUEL_GET_RECORD, dbid);
        }

        private void RPC(action_config actionId, params object[] args)
        {
            LoggerHelper.Info("Request:" + (action_config)actionId);
            _player.ActionRpcCall("duel_action_req", (int)(actionId), args);
        }

        public void OnDuelResp(uint id, uint errorCode, byte[] byteArray)
        {
            LoggerHelper.Info("Response:" + (action_config)id);
            if (errorCode != 0)
            {
                switch ((action_config)id)
                {
                    case action_config.ACTION_DUEL_INVITE_RESP:
                        ParseDuelInviteResponseError(errorCode);
                        break;
                    case action_config.ACTION_DUEL_READY:
                        ParseDuelReadyError(errorCode);
                        break;
                    default:
                        Debug.LogError(string.Format("actionId:{0} errorCode:{1} 没有处理", (action_config)id, errorCode));
                        break;
                }
            }
            
            DuelPanelParameter param;
            switch ((action_config)id)
            {
                case action_config.ACTION_DUEL_ENTER_MAP:
                    break;
                case action_config.ACTION_DUEL_GET_INVITE_LIST:
                    PbDuelInviteTargetList pbDuelInviteTargetList = MogoProtoUtils.ParseProto<PbDuelInviteTargetList>(byteArray);
                    ParseByteArray(pbDuelInviteTargetList);
                    param = new DuelPanelParameter(DuelData.View.search, pbDuelInviteTargetList);
                    UIManager.Instance.ShowPanel(PanelIdEnum.Duel, param);
                    break;
                case action_config.ACTION_DUEL_INVITE_RESP:
                    PbDuelInviteResp pbDuelInviteResp = MogoProtoUtils.ParseProto<PbDuelInviteResp>(byteArray);
                    bool agree = _duelData.FillInviteResponse(pbDuelInviteResp);
                    if (agree) 
                    { 
                        EventDispatcher.TriggerEvent(DuelEvents.RefreshFightView); 
                    }
                    else
                    {
                        UIManager.Instance.ClosePanel(PanelIdEnum.Duel);
                    }
                    break;
                case action_config.ACTION_DUEL_INVITE:
                    PbDuelInviteTrigger pbDuelInviteTrigger = MogoProtoUtils.ParseProto<PbDuelInviteTrigger>(byteArray);
                    ParseByteArray(pbDuelInviteTrigger);
                    _duelData.FillInvite(pbDuelInviteTrigger);
                    param = new DuelPanelParameter(DuelData.View.fight, pbDuelInviteTrigger);
                    UIManager.Instance.ShowPanel(PanelIdEnum.Duel, param);
                    break;
                case action_config.ACTION_DUEL_READY:
                    PbDuelReadyResp pbDuelReadyResp = MogoProtoUtils.ParseProto<PbDuelReadyResp>(byteArray);
                    _duelData.FillReady(pbDuelReadyResp);
                    EventDispatcher.TriggerEvent(DuelEvents.RefreshFightView);
                    break;
                case action_config.ACTION_DUEL_MOVE:
                    PbDuelMove pbDuelMove = MogoProtoUtils.ParseProto<PbDuelMove>(byteArray);
                    Vector3 MovePosition = new Vector3(pbDuelMove.x * 0.01f, PlayerAvatar.Player.position.y, pbDuelMove.y * 0.01f);
                    EventDispatcher.TriggerEvent(PlayerCommandEvents.FIND_POSITION, MovePosition);
                    break;
                case action_config.ACTION_DUEL_QUERY_INFO:
                    PbDuelQueryInfo pbDuelQueryInfo = MogoProtoUtils.ParseProto<PbDuelQueryInfo>(byteArray);
                    UIManager.Instance.ShowPanel(PanelIdEnum.DuelEntry, pbDuelQueryInfo);
                    break;
                case action_config.ACTION_DUEL_MATCH_RESULT:
                    PbDuelMatchResult pbDuelMatchResult = MogoProtoUtils.ParseProto<PbDuelMatchResult>(byteArray);
                    CopyLogicManager.Instance.ProcessResult(ChapterType.Duel, pbDuelMatchResult);
                    break;
                case action_config.ACTION_DUEL_QUERY_PLAYER_INFO:
                    PbDuelQueryPlayerInfo pbDuelQueryPlayerInfo = MogoProtoUtils.ParseProto<PbDuelQueryPlayerInfo>(byteArray);
                    ParseByteArray(pbDuelQueryPlayerInfo);
                    param = new DuelPanelParameter(DuelData.View.fightConfirm, pbDuelQueryPlayerInfo);
                    UIManager.Instance.ShowPanel(PanelIdEnum.Duel, param);
                    break;
                case action_config.ACTION_DUEL_MATCH_START:
                    PbDuelStartMatch pbDuelStartMatch = MogoProtoUtils.ParseProto<PbDuelStartMatch>(byteArray);
                    ParseByteArray(pbDuelStartMatch);
                    CopyLogicManager.Instance.FillPreviewData(ChapterType.Duel, pbDuelStartMatch);
                    break;
                case action_config.ACTION_DUEL_GET_STAGE_REWARD:
                    PbDuelQueryInfo pbDuelQueryInfo1 = MogoProtoUtils.ParseProto<PbDuelQueryInfo>(byteArray);
                    EventDispatcher.TriggerEvent<PbDuelQueryInfo>(DuelEvents.RefreshEntryView, pbDuelQueryInfo1);
                    break;
                case action_config.ACTION_DUEL_GET_GAMBLE_INFO:
                    PbDuelMatchBetInfo pbDuelMatchBetInfo = MogoProtoUtils.ParseProto<PbDuelMatchBetInfo>(byteArray);
                    ParseByteArray(pbDuelMatchBetInfo);
                    //MogoDebugUtils.Print(pbDuelMatchBetInfo.match_state);
                    _duelData.FillStakeInfo(pbDuelMatchBetInfo);
                    param = new DuelPanelParameter(DuelData.View.stake, pbDuelMatchBetInfo);
                    UIManager.Instance.ShowPanel(PanelIdEnum.Duel, param);
                    break;
                case action_config.ACTION_DUEL_GET_GAMBLE_STATE:
                    PbDuelMatchStateInfo pbDuelMatchStateInfo = MogoProtoUtils.ParseProto<PbDuelMatchStateInfo>(byteArray);
                    //MogoDebugUtils.Print(pbDuelMatchStateInfo);
                    _duelData.FillStakeState(pbDuelMatchStateInfo);
                    EventDispatcher.TriggerEvent<PbDuelMatchStateInfo>(DuelEvents.RefreshStakeView, pbDuelMatchStateInfo);
                    break;
                case action_config.ACTION_DUEL_BET1:
                case action_config.ACTION_DUEL_BET2:
                    PbDuelMatchStateInfo pbDuelMatchStateInfo0 = MogoProtoUtils.ParseProto<PbDuelMatchStateInfo>(byteArray);
                    //MogoDebugUtils.Print(pbDuelMatchStateInfo0);
                    _duelData.FillStakeState(pbDuelMatchStateInfo0);
                    EventDispatcher.TriggerEvent<PbDuelMatchStateInfo>(DuelEvents.ResponseStake, pbDuelMatchStateInfo0);
                    break;
                case action_config.ACTION_DUEL_BET_CHANGE:
                    PbDuelMatchBetChange pbDuelMatchBetChange = MogoProtoUtils.ParseProto<PbDuelMatchBetChange>(byteArray);
                    _duelData.FillDuelMatchBetChange(pbDuelMatchBetChange);
                    //MogoDebugUtils.Print(pbDuelMatchBetChange);
                    EventDispatcher.TriggerEvent<PbDuelMatchBetChange>(DuelEvents.RewardChanged, pbDuelMatchBetChange);
                    break;
                case action_config.ACTION_DUEL_GET_RECORD:
                    PbDuelReport pbDuelReport = MogoProtoUtils.ParseProto<PbDuelReport>(byteArray);
                    EventDispatcher.TriggerEvent<PbDuelReport>(DuelEvents.ResponseRecord, pbDuelReport);
                    break;
            }
        }

        private void ParseDuelInviteResponseError(uint errorCode)
        {
            string content = string.Empty;
            switch (errorCode)
            {
                case 1:
                    content = MogoLanguageUtil.GetContent(122070);
                    break;
                case 2:
                    content = MogoLanguageUtil.GetContent(122071);
                    break;
                case 3:
                    content = MogoLanguageUtil.GetContent(122072);
                    break;
                case 4:
                    content = MogoLanguageUtil.GetContent(122073);
                    break;
                case 5:
                    content = MogoLanguageUtil.GetContent(122115);
                    break;
            }
           //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, content, PanelIdEnum.Duel);
        }

        private void ParseDuelReadyError(uint errorCode)
        {
            string content = string.Empty;
            switch (errorCode)
            {
                case 1:
                    content = MogoLanguageUtil.GetContent(122074);
                    break;
                case 2:
                    content = MogoLanguageUtil.GetContent(122075);
                    break;
                case 3:
                    content = MogoLanguageUtil.GetContent(122076);
                    break;
                case 4:
                    content = MogoLanguageUtil.GetContent(122077);
                    break;
            }
           //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, content, PanelIdEnum.Duel);
        }

        private void ParseByteArray(PbDuelStartMatch pbDuelStartMatch)
        {
            ParseByteArray(pbDuelStartMatch.invitee);
            ParseByteArray(pbDuelStartMatch.sponsor);
        }

        private void ParseByteArray(PbDuelMatchBetInfo pbDuelMatchBetInfo)
        {
            ParseByteArray(pbDuelMatchBetInfo.invitee);
            ParseByteArray(pbDuelMatchBetInfo.sponsor);
        }

        private void ParseByteArray(PbDuelQueryPlayerInfo pbDuelQueryPlayerInfo)
        {
            ParseByteArray(pbDuelQueryPlayerInfo.target);
        }

        private void ParseByteArray(PbDuelInviteTargetList pbDuelInviteTargetList)
        {
            for (int i = 0; i < pbDuelInviteTargetList.targets.Count; i++)
            {
                ParseByteArray(pbDuelInviteTargetList.targets[i]);
            }
        }

        private void ParseByteArray(PbDuelInviteTrigger pbDuelInviteTrigger)
        {
            ParseByteArray(pbDuelInviteTrigger.target);
        }

        private void ParseByteArray(PbDuelTarget pbDuelTarget)
        {
            pbDuelTarget.name = MogoProtoUtils.ParseByteArrToString(pbDuelTarget.name_bytes);
            pbDuelTarget.guild_name = MogoProtoUtils.ParseByteArrToString(pbDuelTarget.guild_name_bytes);
            if (string.IsNullOrEmpty(pbDuelTarget.guild_name)) { pbDuelTarget.guild_name = MogoLanguageUtil.GetContent(122090); }
        }
    }
}
