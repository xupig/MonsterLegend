﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.CustomType;
using GameMain.Entities;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class GuildWarManager : Singleton<GuildWarManager> 
    {
        private GuildWarData _data;

        public GuildWarManager()
        {
            _data = PlayerDataManager.Instance.GuildWarData;
            EventDispatcher.AddEventListener<int>(SceneEvents.ENTER_MAP, OnEnterMap);
        }

        private void OnEnterMap(int mapId)
        {
            if (map_helper.GetMapType(mapId) == public_config.MAP_TYPE_CITY || map_helper.GetMapType(mapId) == public_config.MAP_TYPE_WILD)
            {
                CopyLogicManager.Instance.ShowGuildTryLogicPanel();
            }
        }

        private void RPC(action_config actionId, params object[] args)
        {
            PlayerAvatar.Player.ActionRpcCall("guild_action_req", (int)actionId, args);
        }

        public void GetGuildWarInfo()
        {
            RPC(action_config.GUILD_BATTLE_INFO);
        }

        public void GetGuildWarExpInfo()
        {
            RPC(action_config.GUILD_BATTLE_EXP_INFO);
        }

        public void GetGuildWarExpRank(int start, int count)
        {
            RPC(action_config.GUILD_BATTLE_EXP_RANK, start, count);
        }

        public void GetGuildWarWeekRank(int start, int count)
        {
            RPC(action_config.GUILD_BATTLE_WEEK_RANK, start, count);
        }

        public void GetGuildWarTryLog()
        {
            RPC(action_config.GUILD_BATTLE_LOG);
        }

        public void SignUpGuildWar()
        {
            RPC(action_config.GUILD_BATTLE_SIGNUP);
        }

        public void GuildWarDraw()
        {
            RPC(action_config.GUILD_BATTLE_DRAW);
        }

        /// <summary>
        /// 获取公会战抽签结果
        /// </summary>
        public void GetGuildWarDrawResult()
        {
            RPC(action_config.GUILD_BATTLE_GET_DRAW_RESULT);
        }

        public void GetGuildWarDrawInfo()
        {
            RPC(action_config.GUILD_BATTLE_GET_DRAW_INFO);
        }

        public void GetSignUpInfo(int start, int count)
        {
            RPC(action_config.GUILD_BATTLE_GET_SIGNUPED_RANK, start, count);
        }

        public void AssignedPosition(ulong dbid, int military)
        {
            RPC(action_config.GUILD_BATTLE_CHANGE_MILITARY, dbid, military);
        }

        public void EnterWaitMission()
        {
            RPC(action_config.GUILD_BATTLE_ENTER);
        }

        public void GetGroupMatchInfo(int index)
        {
            RPC(action_config.GUILD_BATTLE_GROUP_MATCH_INFO, index);
        }

        public void GetGroupMatchLog(int index)
        {
            RPC(action_config.GUILD_BATTLE_GET_GROUP_MATCH_LOG, index);
        }

        public void GetMatchCountDownInfo()
        {
            RPC(action_config.GUILD_BATTLE_MATCH_COUNTDOWN_INFO);
        }

        public void GetFinalMatchInfo(int group = -1, int index = -1)
        {
            RPC(action_config.GUILD_BATTLE_FINAL_MATCH_INFO, group, index);
        }

        public void GetGuildMilitary(uint entityId)
        {
            PlayerAvatar.Player.RpcCall("get_guild_military_req", entityId);
        }

        public void GetGuildWarLog(int start, int count)
        {
            RPC(action_config.GUILD_BATTLE_GET_LOG, start, count);
        }

        public void GetGuildWarHistoryLog(int start, int count)
        {
            RPC(action_config.GUILD_BATTLE_GET_RESULT_LOGS, start, count);
        }

        public void GetGuildWarRankList(int season)
        {
            RPC(action_config.GUILD_BATTLE_GET_RESULT_LOG, season);
        }

        public void GetGuildWarResultRecord(ulong logId, ulong guildId, int findLastLogDbid = 1)
        {
            RPC(action_config.GUILD_BATTLE_GET_PK_LOG, logId, guildId, findLastLogDbid);
        }

        public void CallPeople()
        {
            RPC(action_config.GUILD_BATTLE_CALL);
        }

        public void BeginEnterGuildPkMapResp(string guildName, string name, uint fight, byte military, byte vocation, byte level, byte round)
        {
            GuildWarEnemyData data = new GuildWarEnemyData();
            data.guildName = guildName;
            data.name = name;
            data.fight = (int)fight;
            data.military = (int)military;
            data.vocation = (int)vocation;
            data.level = (int)level;
            data.round = (int)round;
            CopyLogicManager.Instance.FillPreviewData(ChapterType.GuildWarGroup, data);
            PlayerDataManager.Instance.GuildWarData.EnemyData = data;
        }

        public void OnGuildWarResp(UInt32 actionId, UInt32 errorCode, byte[] byteArr)
        {
            switch ((action_config)actionId)
            {
                case action_config.GUILD_BATTLE_INFO:
                    OnGetGuildBattleInfo(byteArr);
                    break;
                case action_config.GUILD_BATTLE_EXP_INFO:
                    OnGetGuildBattleExpInfo(byteArr);
                    break;
                case action_config.GUILD_BATTLE_EXP_RANK:
                    OnGetGuildBattleExpRank(byteArr);
                    break;
                case action_config.GUILD_BATTLE_WEEK_RANK:
                    OnGetGuildBattleWeekRank(byteArr);
                    break;
                case action_config.GUILD_BATTLE_LOG:
                    OnGetGuildBattleTryLog(byteArr);
                    break;
                case action_config.GUILD_BATTLE_SIGNUP:
                    OnSignUpGuildWar(byteArr);
                    break;
                case action_config.GUILD_BATTLE_GET_SIGNUPED_RANK:
                    OnGetGuildBattleSignUpList(byteArr);
                    break;
                case action_config.GUILD_BATTLE_DRAW:
                    OnDrawGuildWar(byteArr);
                    break;
                case action_config.GUILD_BATTLE_GET_DRAW_RESULT:
                    OnGuildWarDrawResult(byteArr);
                    break;
                case action_config.GUILD_BATTLE_CHANGE_MILITARY:
                    OnGuildChangeMilitaryRank(byteArr);
                    break;
                case action_config.GUILD_BATTLE_GET_DRAW_INFO:
                    OnGuildWarDrawInfo(byteArr);
                    break;
                case action_config.GUILD_BATTLE_GROUP_MATCH_INFO:
                    OnGetGuildGroupMatchInfo(byteArr);
                    break;
                case action_config.GUILD_BATTLE_MATCH_COUNTDOWN_INFO:
                    OnGetGuildCountDownInfo(byteArr);
                    break;
                case action_config.GUILD_BATTLE_FINAL_MATCH_INFO:
                    OnGetGuildFinalMatchInfo(byteArr);
                    break;
                case action_config.GUILD_BATTLE_GET_GROUP_MATCH_LOG:
                    OnGetGroupMatchLogResp(byteArr);
                    break;
                case action_config.GUILD_GET_GUILD_MILITARY:
                    OnGetGuildMilitary(byteArr);
                    break;
                case action_config.GUILD_BATTLE_GET_LOG:
                    OnGetGuildBattleLog(byteArr);
                    break;
                case action_config.GUILD_BATTLE_GET_RESULT_LOGS:
                    OnGetGuildBattleHistoryLog(byteArr);
                    break;
                case action_config.GUILD_BATTLE_GET_RESULT_LOG:
                    OnGetGuildWarRankList(byteArr);
                    break;
                case action_config.GUILD_BATTLE_GET_PK_LOG:
                    OnGetGuildWarResultRecord(byteArr);
                    break;
            }
        }

        private void OnGetGroupMatchLogResp(byte[] byteArr)
        {
            PbGuildBattleGroupMatchLogList pbList = MogoProtoUtils.ParseProto<PbGuildBattleGroupMatchLogList>(byteArr);
            EventDispatcher.TriggerEvent<PbGuildBattleGroupMatchLogList>(GuildWarEvents.Refresh_Guild_Group_Match_Log, pbList);
        }

        private void OnGetGuildWarRankList(byte[] byteArr)
        {
            PbGuildBattleResultInfoList pbList = MogoProtoUtils.ParseProto<PbGuildBattleResultInfoList>(byteArr);
            EventDispatcher.TriggerEvent<PbGuildBattleResultInfoList>(GuildWarEvents.On_Get_Guild_War_Rank_List, pbList);
        }

        private void OnGetGuildBattleHistoryLog(byte[] byteArr)
        {
            PbGuildBattleResultIndexInfoList pbList = MogoProtoUtils.ParseProto<PbGuildBattleResultIndexInfoList>(byteArr);
            EventDispatcher.TriggerEvent<PbGuildBattleResultIndexInfoList>(GuildWarEvents.On_Get_Guild_War_History_Log, pbList);
        }

        private void OnGetGuildBattleLog(byte[] byteArr)
        {
            PbGuildBattleLogList pbList = MogoProtoUtils.ParseProto<PbGuildBattleLogList>(byteArr);
            EventDispatcher.TriggerEvent<PbGuildBattleLogList>(GuildWarEvents.On_Get_Guild_War_Log, pbList);
        }

        private void OnGetGuildMilitary(byte[] byteArr)
        {
            PbGuildMilitaryInfo pbInfo = MogoProtoUtils.ParseProto<PbGuildMilitaryInfo>(byteArr);
            EventDispatcher.TriggerEvent<PbGuildMilitaryInfo>(GuildWarEvents.On_Get_Guild_Military_Info, pbInfo);
        }

        private void OnGetGuildCountDownInfo(byte[] byteArr)
        {
            PbGuildBattleMatchCountdownResult pbInfo = MogoProtoUtils.ParseProto<PbGuildBattleMatchCountdownResult>(byteArr);
            EventDispatcher.TriggerEvent<PbGuildBattleMatchCountdownResult>(GuildWarEvents.Refresh_Guild_Match_Count_Down_Info, pbInfo);
        }

        private void OnGetGuildGroupMatchInfo(byte[] byteArr)
        {
            PbGuildBattleGroupMatchResultList list = MogoProtoUtils.ParseProto<PbGuildBattleGroupMatchResultList>(byteArr);
            EventDispatcher.TriggerEvent<PbGuildBattleGroupMatchResultList>(GuildWarEvents.Refresh_Guild_Group_Match_Info, list);
        }

        private void OnGetGuildBattleSignUpList(byte[] byteArr)
        {
            PbGuildBattleSignupedInfoList list = MogoProtoUtils.ParseProto<PbGuildBattleSignupedInfoList>(byteArr);
            EventDispatcher.TriggerEvent<PbGuildBattleSignupedInfoList>(GuildWarEvents.Refresh_Guild_War_Sign_List, list);
        }

        private void OnSignUpGuildWar(byte[] byteArr)
        {
           //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(111568));
            EventDispatcher.TriggerEvent(GuildWarEvents.Refresh_Guild_Sign_Up_Success);
        }

        private void OnGetGuildBattleTryLog(byte[] byteArr)
        {
            PbGuildBattleLogInfoList list = MogoProtoUtils.ParseProto<PbGuildBattleLogInfoList>(byteArr);
            EventDispatcher.TriggerEvent<PbGuildBattleLogInfoList>(GuildWarEvents.Refresh_Guild_War_Try_Log, list);
        }

        public void OnGuildWarResultResp(byte[] byteArr)
        {
            PbGuildMissionResult pbGuildMissionResult = MogoProtoUtils.ParseProto<PbGuildMissionResult>(byteArr);
            int instanceId = map_helper.GetInstanceIDByMapID((int)pbGuildMissionResult.map_id);
            LoggerHelper.Info(string.Format("OnGuildWarResultResp, instanceId = {0}.", instanceId));
            CopyLogicManager.Instance.ProcessResult(instance_helper.GetChapterType(instanceId), pbGuildMissionResult);
        }

        private static void OnGetGuildBattleInfo(byte[] byteArr)
        {
            PbGuildStage stage = MogoProtoUtils.ParseProto<PbGuildStage>(byteArr);
            PlayerDataManager.Instance.GuildWarData.Stage = stage;
            EventDispatcher.TriggerEvent<PbGuildStage>(GuildWarEvents.Refresh_Guild_War_Stage, stage);
        }

        private static void OnGetGuildBattleExpInfo(byte[] byteArr)
        {
            PbGuildExpInfo expInfo = MogoProtoUtils.ParseProto<PbGuildExpInfo>(byteArr);
            EventDispatcher.TriggerEvent<PbGuildExpInfo>(GuildWarEvents.Refresh_Guild_War_Try_Info, expInfo);
        }

        private static void OnGetGuildBattleExpRank(byte[] byteArr)
        {
            PbGuildBattleRankInfoList list = MogoProtoUtils.ParseProto<PbGuildBattleRankInfoList>(byteArr);
            EventDispatcher.TriggerEvent<PbGuildBattleRankInfoList>(GuildWarEvents.Refresh_Guild_War_Try_Person_Rank, list);
        }

        private static void OnGetGuildBattleWeekRank(byte[] byteArr)
        {
            PbGuildBattleWeekRankInfoList list = MogoProtoUtils.ParseProto<PbGuildBattleWeekRankInfoList>(byteArr);
            EventDispatcher.TriggerEvent<PbGuildBattleWeekRankInfoList>(GuildWarEvents.Refresh_Guild_War_Try_Week_Rank, list);
        }

        private void OnDrawGuildWar(byte[] byteArr)
        {
            PbGuildBattleDrawInfoList pb = MogoProtoUtils.ParseProto<PbGuildBattleDrawInfoList>(byteArr);
            for (int i = 0; i < pb.list.Count; i++)
            {
                pb.list[i].name = MogoProtoUtils.ParseByteArrToString(pb.list[i].name_bytes);
            }
            EventDispatcher.TriggerEvent<PbGuildBattleDrawInfoList>(GuildWarEvents.Draw, pb);
        }

        private void OnGuildWarDrawResult(byte[] byteArr)
        {
            PbGuildBattleDrawResultInfoList pb = MogoProtoUtils.ParseProto<PbGuildBattleDrawResultInfoList>(byteArr);
            for (int i = 0; i < pb.list.Count; i++)
            {
                PbGuildBattleDrawResultInfo item = pb.list[i];
                item.guild1_name = MogoProtoUtils.ParseByteArrToString(item.guild1_name_bytes);
                item.guild2_name = MogoProtoUtils.ParseByteArrToString(item.guild2_name_bytes);
                item.guild3_name = MogoProtoUtils.ParseByteArrToString(item.guild3_name_bytes);
                item.guild4_name = MogoProtoUtils.ParseByteArrToString(item.guild4_name_bytes);
            }
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarDraw, pb);
        }

        private void OnGuildChangeMilitaryRank(byte[] byteArr)
        {
            PbGuildChangeMilitary pb = MogoProtoUtils.ParseProto<PbGuildChangeMilitary>(byteArr);
            PlayerDataManager.Instance.GuildData.ChangeMemberMilitaryRank(pb);
            EventDispatcher.TriggerEvent<PbGuildChangeMilitary>(GuildWarEvents.Assigned_Position, pb);
        }

        private void OnGuildWarDrawInfo(byte[] byteArr)
        {
            PbGuildBattleDrawResultList pb = MogoProtoUtils.ParseProto<PbGuildBattleDrawResultList>(byteArr);
            EventDispatcher.TriggerEvent<PbGuildBattleDrawResultList>(GuildWarEvents.Response_Draw_Info, pb);
        }

        private void OnGetGuildFinalMatchInfo(byte[] byteArr)
        {
            PbGuildBattleFinalMatchResultList pb = MogoProtoUtils.ParseProto<PbGuildBattleFinalMatchResultList>(byteArr);
            EventDispatcher.TriggerEvent<PbGuildBattleFinalMatchResultList>(GuildWarEvents.Response_Final_Match_Info, pb);
        }

        private void OnGetGuildWarResultRecord(byte[] byteArr)
        {
            PbGuildBattlePkLogList pb = MogoProtoUtils.ParseProto<PbGuildBattlePkLogList>(byteArr);
            pb.self_guild_name = MogoProtoUtils.ParseByteArrToString(pb.self_guild_name_bytes);
            pb.to_guild_name = MogoProtoUtils.ParseByteArrToString(pb.to_guild_name_bytes);
            for (int i = 0; i < pb.list.Count; i++)
            {
                PbGuildBattlePkLog item = pb.list[i];
                item.self_avatar_name = MogoProtoUtils.ParseByteArrToString(item.self_avatar_name_bytes);
                item.to_avatar_name = MogoProtoUtils.ParseByteArrToString(item.to_avatar_name_bytes);
            }
            UIManager.Instance.ShowPanel(PanelIdEnum.GuildWarResultRecord, pb);
        }
    }
}
