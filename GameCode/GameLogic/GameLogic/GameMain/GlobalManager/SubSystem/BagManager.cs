﻿#region 模块信息
/*==========================================
// 文件名：BagManager
// 命名空间: GameMain.GlobalManager.SubSystem
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/19 9:36:59
// 描述说明：背包数据管理
// 其他：
//==========================================*/
using GameLoader.Utils;


#endregion

using GameMain.Entities;


using GameData;
using MogoEngine.Events;
using Common.Events;
using Common.ServerConfig;
using Common.Data;
using UnityEngine;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using Common.Utils;
using Common.Structs;
using System;

namespace GameMain.GlobalManager.SubSystem
{
    /// <summary>
    /// BagManager管理玩家身上的多种类型的背包，比如物品背包（ItemBag），身上物品背包（BodyEquipBag），符文背包（RuneBag），身上符文背包（BodyRuneBag）
    /// 玩家赎回背包（RedeemBag）
    /// </summary>
    public class BagManager : Singleton<BagManager>
    {
        private static PlayerAvatar _player;
        private BagDataManager _bagDataManager;

        public BagManager()
        {
            _player = PlayerAvatar.Player;
            _bagDataManager = PlayerDataManager.Instance.BagData;
        }

        /// <summary>
        /// 背包获取所有数据
        /// </summary>
        public void RequestGetAllData()
        {
            RequestGetBagData(BagType.BodyEquipBag);
            RequestGetBagData(BagType.BodyRuneBag);
            RequestGetBagData(BagType.RuneBag);
            RequestGetBagData(BagType.RedeemBag);
            RequestGetBagData(BagType.ItemBag);
        }
        /// <summary>
        ///  更新单个物品处理
        /// </summary>
        public void ResponseUpdateData(BagType bagType, int num, byte[] byteArr)
        {
            PbGridItem pbGridItem = MogoProtoUtils.ParseProto<PbGridItem>(byteArr);
            BaseItemInfo oldItem = _bagDataManager.GetBagData(bagType).GetItemInfo(Convert.ToInt32(pbGridItem.grid_index));
            bool isNew = _bagDataManager.GetBagData(bagType).UpdateItem(pbGridItem);
            //Debug.Log("背包数据更新： " + bagType + " GridPosition: " + pbGridItem.grid_index); //Ari:屏蔽日誌
            EventDispatcher.TriggerEvent<BagType, uint>(BagEvents.Update, bagType, pbGridItem.grid_index);
            if (isNew && bagType == BagType.ItemBag)
            {
                EventDispatcher.TriggerEvent(BagEvents.AddItem, (int)pbGridItem.item.id);
            }
            if (bagType == BagType.ItemBag && num > 0)
            {
                EventDispatcher.TriggerEvent(EquipUpgradeTipEvents.Bag_Item_Update, pbGridItem, num);
            }
            if (bagType == BagType.BodyEquipBag)
            {
                BaseItemInfo newItem = _bagDataManager.GetBagData(bagType).GetItemInfo(Convert.ToInt32(pbGridItem.grid_index));
                if(oldItem == null || newItem.Id!=oldItem.Id)
                {
                    FightTipsManager.equipChangePos = newItem.GridPosition;
                }
                EventDispatcher.TriggerEvent(EquipUpgradeTipEvents.Body_Item_Update);
            }
        }
        /// <summary>
        /// 获取背包全部物品处理
        /// </summary>
        public void ResponseGetBagData(byte[] byteArr)
        {
            PbBagList bagList = MogoProtoUtils.ParseProto<PbBagList>(byteArr);
            BagType bagType = (BagType)bagList.bag_id;
            //Debug.LogError("获取背包全部物品: " + "bagType: " + bagType + "Count: " + bagList.items.Count);
            _bagDataManager.GetBagData(bagType).Fill(bagList);
            EventDispatcher.TriggerEvent<BagType>(BagEvents.Init, bagType);
            if (bagType == BagType.ItemBag)
            {
                EventDispatcher.TriggerEvent(EquipUpgradeTipEvents.Bag_Item_Update_All);
            }
            if (bagType == BagType.BodyEquipBag)
            {
                BodySuitEffectManager.Instance.InitBodySuitEffectDic();
            }
            //if (bagType == BagType.BodyEquipBag)
            //{
            //    EventDispatcher.TriggerEvent(EquipUpgradeTipEvents.Body_Equip_Update);
            //}
        }

        public void ResponseGetReward(byte[] byteArr)
        {
            //UnityEngine.Debug.LogError("ResponseGetReward===========");
            PbRewardList rewardList = MogoProtoUtils.ParseProto<PbRewardList>(byteArr);
            //Debug.Log("获得物品奖励:  " + rewardList.item_list.Count); //Ari去掉了日志
			LoggerHelper.Info (string.Format("RespReward way:{0},item.count:{1}",rewardList.get_way,rewardList.item_list.Count));

            for (int i = 0; i < rewardList.item_list.Count; i++)
            {
                PbItemNoticeItem pb = rewardList.item_list[i];
                pb.count = pb.count + pb.addi_count;
                if (pb.id == public_config.ITEM_SPECIAL_TYPE_LEVEL_EXP)
                {
                    int count = Mathf.CeilToInt(pb.count * avatar_level_helper.GetExpStandard((int)PlayerAvatar.Player.level) * 0.0001f);
                    pb.count = (uint)count;
                }
                else if (pb.id == public_config.ITEM_SPECIAL_TYPE_LEVEL_GOLD)
                {
                    int count = Mathf.CeilToInt(pb.count * avatar_level_helper.GetGoldStandard((int)PlayerAvatar.Player.level) * 0.0001f);
                    pb.count = (uint)count;
                }
            }
            ItemObtainManager.Instance.Show(rewardList);
            ChatManager.Instance.SendGetRewardMsgToSystemChannel(rewardList);
			if (rewardList.get_way == 101) 
			{//ActiveCode Resp

			}
        }

        /// <summary>
        /// 请求获取背包数据
        /// </summary>
        public void RequestGetBagData(BagType bagType)
        {
            RPC(action_config.ITEM_GET_PKG_DATA, (byte)bagType);
        }
        /// <summary>
        /// 请求出售物品
        /// </summary>
        /// <param name="pos">背包格子位置</param>
        /// <param name="count">数量</param>
        public void RequestSellItem(BagType bagType, int pos, int count)
        {
            action_config.ITEM_SELL.Register();
            RPC(action_config.ITEM_SELL, (byte)bagType, pos, count);
        }
        /// <summary>
        /// 请求整理背包
        /// </summary>
        /// <param name="bagType">背包类型</param>
        public void RequestSortBag(BagType bagType)
        {
            if (_bagDataManager.GetBagData(bagType).NeedSort() == true)
            {
                RPC(action_config.SORT_PKG, (byte)bagType);
            }
        }
        /// <summary>
        /// 请求使用物品
        /// </summary>
        /// <param name="gridPosition">背包格子位置</param>
        /// <param name="count">数量</param>
        public void RequestUseItem(int gridPosition, int count)
        {
            //UnityEngine.Debug.LogError("gridPosition : " + gridPosition);
            action_config.ITEM_USE.Register();
            RPC(action_config.ITEM_USE, gridPosition, count);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bagPostion"></param>
        /// <param name="bodyEquipPosition"></param>
        /// <param name="isReplace"></param>
        public void RequestWearEquip(int bagPostion, int bodyEquipPosition, bool isReplace)
        {
            //Debug.Log("BagPosition: " + bagPostion + " bodyEquipPosition:  " + bodyEquipPosition + "  isReplace:  " + isReplace);
            _player.RpcCall("equip_wear_req", bagPostion, bodyEquipPosition, action_config.EQUIP_WEAR);
        }
        /// <summary>
        /// 卸下装备
        /// </summary>
        /// <param name="bodyPos">身上位置</param>
        public void RequestTakeOffEquip(int bodyPos)
        {
            _player.RpcCall("equip_take_off_req", bodyPos);
        }
        /// <summary>
        /// 解锁格子
        /// </summary>
        public void RequestUnlockGrid(int count)
        {
            if (count > 0)
            {
                RPC(action_config.EXPAND_BAG, count);
            }
        }

        private void RPC(action_config actionId, params object[] args)
        {
            _player.ActionRpcCall("item_action_req", (int)(actionId), args);
        }


        public void ItemUseResp(byte[] byteArr)
        {
            PbItemUseResp data = MogoProtoUtils.ParseProto<PbItemUseResp>(byteArr);
            if (data.result == 0)
            {
                EventDispatcher.TriggerEvent(BagEvents.UseItemSuccess);
            }
            else
            {
                EventDispatcher.TriggerEvent(BagEvents.UseItemFail);
            }
        }
    }
}
