﻿#region 模块信息
/*==========================================
// 文件名：SystemInfoServerInfoParse
// 命名空间: GameMain.GlobalManager.ActionResp
// 创建者：LYX
// 修改者列表：
// 创建日期：2015/1/14 16:29:28
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Common.ServerConfig;
using Common.Utils;
using GameMain.Entities;
using UnityEngine;

using Common.Global;
using GameData;
using Common.ClientConfig;
using Common.Data;
using Common.Chat;

namespace GameMain.GlobalManager.SubSystem
{
    public class SystemInfoUtil
    {
        private static Regex _pattern = new Regex(@"\{[^}]*(,([a-zA-Z0-9]+))?\}");   //"{1,Num}"
        private static Regex _paramPattern = new Regex(@"\{[^}]*,([a-zA-Z0-9]+)?\}");  //example: {1,ItemId}
        private static Regex _keyPattern = new Regex(@"(?<=\{[^}]*,)([a-zA-Z]+)(?=\})");

        private const string ITEM_LINK_FORMAT = "[{0},{1}]";
        private const string VIEW_LINK_TEMPLATE = "[{0},{1},{2}]";
        private const string SHARE_POS_LINK_TEMPLATE = "[{0},{1},{2},{3},{4},{5},{6},{7}]";
        private const string NUM = "num";
        private const string CONTENT = "content";
        private const string SELF_NAME = "selfname";
        private const string PLAYER_NAME = "playername";
        private const string ITEM_ID = "itemid";
        private const string QUALITY_ITEM = "qualityitem";
        private const string MONEY_TYPE = "moneytype";
        private const string CHANNEL_ID = "channelid";
        private const string INST_ID = "instid";
        private const string MAP_ID = "mapid";
        private const string NPC_ID = "npcid";
        private const string FORMULA_ID = "formulaid";
        private const string SKILL_ID = "skillid";
        private const string SKILL_PROFICIENT = "skillproficient";
        private const string GUILD_POSITION = "guildposition";
        private const string GUILD_ACHIEVEMENT_ID = "guildachievementid";
        private const string GUILD_SKILL_ID = "guildskillid";
        private const string GUILD_ACHIEVEMENT_GET = "guildachievementget";
        private const string GUILD_NAME = "guildname";
        private const string MILITARY_RANK = "militaryrank";
        private const string CHAPTER_ID = "chapterid";
        private const string ACTIVITY_ID = "activityid";
        private const string GEM_ID = "gemid";
        private const string RED_ENVELOPE_ID = "envelopeid";
        private const string TUTOR_EVENT_ID = "tutoreventid";
        private const string MONSTER_ID = "monsterid";
        private const string DUEL_RANK_ID = "duelrankid";
        private const string TASK_NAME = "taskname";
        private const string VIEW_ID = "viewid";
        private const string BUFF_ID = "buffid";
        private const string POSITION = "position";

        private static List<ChatLinkBaseWrapper> _linkWrapperList = new List<ChatLinkBaseWrapper>();

        public static string GenerateContent(int systemInfoId, params object[] param)
        {
            string content = system_info_helper.GetContent(systemInfoId);
            if (content == null)
            {
                return content;
            }
            MatchCollection matchCollection = Regex.Matches(content, _pattern.ToString(), RegexOptions.IgnoreCase);
            for (int index = 0; index < matchCollection.Count; index++)
            {
                string matchContent = matchCollection[index].ToString();
                if (param.Length > index)
                {
                    content = content.Replace(matchContent, ParseMatch(matchContent, param[index]));
                }
                else
                {
                    //TODO:根据SystemInfoId找SubSystem 再找对应的RTX
                    if (Application.isEditor)
                    {
                  //ari MessageBox.Show(false, MessageBox.DEFAULT_TITLE, "ID为" + systemInfoId + "的系统提示缺少服务器参数，请联系RTX：");
                    }
                }
            }
            return MogoStringUtils.ConvertStyle(content);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="systemInfoId"></param>
        /// <param name="isChatLink">为true时表示解析里面的相关链接</param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static string GenerateContent(int systemInfoId, bool isChatLink, params object[] param)
        {
            string content = system_info_helper.GetContent(systemInfoId);
            MatchCollection matchCollection = Regex.Matches(content, _pattern.ToString(), RegexOptions.IgnoreCase);
            int paramIndex = 0;
            _linkWrapperList.Clear();
            for (int index = 0; index < matchCollection.Count; index++)
            {
                string matchContent = matchCollection[index].ToString();
                if (isChatLink && IsParseChatLink(matchContent))
                {
                    content = content.Replace(matchContent, ParseLinkMatch(systemInfoId, matchContent, param, ref paramIndex));
                }
                else
                {
                    content = content.Replace(matchContent, ParseMatch(matchContent, param[paramIndex++]));
                }
            }
            return PlayerDataManager.Instance.ChatData.CreateLinkMsg(MogoStringUtils.ConvertStyle(content), _linkWrapperList.ToArray(),true);
        }

        public static string GenerateContent(string content, params object[] param)
        {
            if (content == null)
            {
                return content;
            }
            MatchCollection matchCollection = Regex.Matches(content, _pattern.ToString(), RegexOptions.IgnoreCase);
            for (int index = 0; index < matchCollection.Count; index++)
            {
                string matchContent = matchCollection[index].ToString();
                if (param.Length > index)
                {
                    content = content.Replace(matchContent, ParseMatch(matchContent, param[index]));
                }
            }
            return MogoStringUtils.ConvertStyle(content);
        }

        public static bool IsContentWithParam(string content)
        {
            bool result = false;
            MatchCollection matchCollection = Regex.Matches(content, _pattern.ToString(), RegexOptions.IgnoreCase);
            for (int index = 0; index < matchCollection.Count; index++)
            {
                string matchContent = matchCollection[index].ToString();
                string paramContent = _paramPattern.Match(matchContent).Value;
                if (!string.IsNullOrEmpty(paramContent))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        private static string[] SplitContentParam(string match)
        {
            int startIndex = match.IndexOf("{");
            int endIndex = match.IndexOf("}");
            if (startIndex == -1 || endIndex == -1)
            {
                return null;
            }
            else
            {
                return match.Substring(startIndex + 1, endIndex - startIndex - 1).Split(',');
            }
        }

        private static bool IsParseChatLink(string match)
        {
            string key = GetParseKey(match);
            if (key == null)
            {
                return false;
            }
            switch (key)
            {
                case ITEM_ID:
                case POSITION:
                case VIEW_ID:
                    return true;
                default:
                    return false;
            }

        }

        private static string GetParseKey(string match)
        {
            string result = _keyPattern.Match(match).ToString();
            if (result == null)
            {
                return result;
            }
            else
            {
                return result.ToLower();
            }
        }

        private static string ParseLinkMatch(int systemInfoId, string match, object[] value, ref int paramIndex)
        {
            string key = GetParseKey(match);
            if (key == null)
            {
                value[paramIndex].ToString();
            }
            switch (key)
            {
                case ITEM_ID:
                    return ParseItemLinkContent(systemInfoId, value, ref paramIndex);
                case VIEW_ID:
                    return ParseViewIdLinkContent(systemInfoId, match, value, ref paramIndex);
                case POSITION:
                    return ParsePositionLinkContent(systemInfoId, match, value, ref paramIndex);
                default:
                    return ParseLinkContent(systemInfoId, value, ref paramIndex);

            }
        }

        private static string ParseMatch(string match, object value)
        {
            int startIndex = match.IndexOf(",");
            int endIndex = match.IndexOf("}");
            if (startIndex == -1 && endIndex == -1)
            {
                return value.ToString();
            }
            string key = match.Substring(startIndex + 1, endIndex - startIndex - 1);
            return ParseContent(key.ToLower(), value);
        }

        private static string ParsePositionLinkContent(int systemInfoId, string match, object[] value, ref int paramIndex)
        {
            string[] contentParam = SplitContentParam(match);
            int mapId = int.Parse(value[paramIndex - 2].ToString());
            int line = int.Parse(value[paramIndex + 2].ToString());

            int x = Mathf.RoundToInt(float.Parse(value[paramIndex].ToString()) / 100.0f);
            int y = 0;
            int z = Mathf.RoundToInt(float.Parse(value[paramIndex + 1].ToString()) / 100.0f);
            paramIndex += 3;
            return string.Format(SHARE_POS_LINK_TEMPLATE, contentParam[0], (int)ChatLinkType.SharePosition, mapId, line, x, y, z, 0);
        }

        private static string ParseLinkContent(int systemInfoId, object[] value, ref int paramIndex)
        {
            return value[paramIndex++].ToString();
        }

        private static string ParseViewIdLinkContent(int systemInfoId, string match, object[] value, ref int paramIndex)
        {
            string[] contentParam = SplitContentParam(match);
            Dictionary<string, string[]> dataDictList = data_parse_helper.ParseMapList(system_info_helper.GetConfig(systemInfoId).__follow);
            int viewId = int.Parse(dataDictList["0"][0]);
            _linkWrapperList.Add(new ChatViewLinkWrapper(contentParam[0],viewId));
            return string.Format(ITEM_LINK_FORMAT, contentParam[0], (int)ChatLinkType.View);
        }


        private static string ParseItemLinkContent(int systemInfoId, object[] value, ref int paramIndex)
        {
            int itemId = int.Parse(value[paramIndex].ToString());
            paramIndex++;
            _linkWrapperList.Add(new ChatItemLinkWrapper(ChatItemLinkWrapper.ItemLinkType.Item, item_helper.GetName(itemId), itemId));
            return string.Format(ITEM_LINK_FORMAT, item_helper.GetName(itemId), (int)ChatLinkType.Item);
        }

        private static string ParseContent(string key, object value)
        {
            switch (key)
            {
                case NUM:
                    return ParseNum(value);
                case CONTENT:
                    return ParseContent(value);
                case SELF_NAME:
                    return ParseSelfName(value);
                case PLAYER_NAME:
                    return ParsePlayerId(value);
                case ITEM_ID:
                    return ParseItemId(value);
                case QUALITY_ITEM:
                    return ParseQualityItem(value);
                case MONEY_TYPE:
                    return ParseMoneyType(value);
                case CHANNEL_ID:
                    return ParseChannelId(value);
                case INST_ID:
                    return ParseInstId(value);
                case MAP_ID:
                    return ParseMapId(value);
                case NPC_ID:
                    return ParseNpcId(value);
                case FORMULA_ID:
                    return ParseFormulaId(value);
                case GUILD_POSITION:
                    return ParseGuildPosition(value);
                case GUILD_ACHIEVEMENT_ID:
                    return ParseGuildAchievementId(value);
                case GUILD_SKILL_ID:
                    return ParseGuildSkillId(value);
                case GUILD_ACHIEVEMENT_GET:
                    return ParseGuildAchievementGet(value);
                case GUILD_NAME:
                    return ParseGuildName(value);
                case MILITARY_RANK:
                    return ParseMilitaryRank(value);
                case SKILL_ID:
                    return ParseSkillId(value);
                case SKILL_PROFICIENT:
                    return ParseSkillProficient(value);
                case CHAPTER_ID:
                    return ParseChapterValue(value);
                case ACTIVITY_ID:
                    return ParseActivityId(value);
                case GEM_ID:
                    return ParseGemId(value);
                case RED_ENVELOPE_ID:
                    return ParseRedEnvelopeId(value);
                case TUTOR_EVENT_ID:
                    return ParseTutorEventId(value);
                case MONSTER_ID:
                    return ParseMonsterId(value);
                case DUEL_RANK_ID:
                    return ParseDuelRankId(value);
                case TASK_NAME:
                    return ParseTaskName(value);
                case BUFF_ID:
                    return ParseBuffId(value);
                default:
                    return ParseContent(value);
            }
        }

        private static string ParseBuffId(object value)
        {
            int buffId = int.Parse(value.ToString());
            if (buff_helper.CheckBuffId(buffId))
            {
                return buff_helper.GetName(buffId);
            }
            return MogoLanguageUtil.GetContent(0);
        }

        private static string ParseTaskName(object value)
        {
            int taskId = int.Parse(value.ToString());
            return task_data_helper.GetTaskName(task_data_helper.GetTaskData(taskId));
        }

        private static string ParseTutorEventId(object value)
        {
            int tutoreventid = int.Parse(value.ToString());
            return tutor_event_helper.GetEventDescription(tutoreventid);
        }

        private static string ParseRedEnvelopeId(object value)
        {
            int envelopeConfigId = int.Parse(value.ToString());
            red_envelope config = red_envelope_helper.GetConfig(envelopeConfigId);
            string envelopeName = MogoLanguageUtil.GetContent(config.__Name);
            return envelopeName;
        }

        private static string ParseGemId(object value)
        {
            int gemId = int.Parse(value.ToString());
            int color = icon_item_helper.GetIconColor(item_helper.GetIcon(gemId));
            string gemName = item_gem_helper.GetGemName(gemId);
            return ColorDefine.GetColorHtmlString(color, gemName);
        }

        private static string ParseActivityId(object value)
        {
            int activityId = int.Parse(value.ToString());
            LimitActivityData _activityData = activity_helper.GetLimitActivity(activityId);
            return _activityData.openData.__name != 0 ? MogoLanguageUtil.GetContent(_activityData.openData.__name) : MogoLanguageUtil.GetContent(_activityData.functionData.__name);
        }

        private static string ParseSkillId(object value)
        {
            int skillId = int.Parse(value.ToString());
            return spell_helper.GetName(skillId).ToLanguage();
        }

        private static string ParseSkillProficient(object value)
        {
            int skillProficient = int.Parse(value.ToString());
            return skill_helper.GetSpellProficient(PlayerAvatar.Player.vocation.ToInt(), skillProficient).__name.ToLanguage();
        }

        private static string ParseChapterValue(object value)
        {
            int chapterId = int.Parse(value.ToString());
            return chapters_helper.GetName(chapterId);
        }
        private static string ParseGuildAchievementGet(object value)
        {
            int achievementId = int.Parse(value.ToString());
            return guild_achievement_helper.GetGuildAchievementGetContent(achievementId);
        }

        private static string ParseGuildName(object value)
        {
            return value.ToString();
        }

        private static string ParseMilitaryRank(object value)
        {
            int militaryId = int.Parse(value.ToString());
            if (guild_match_helper.HaveMilitaryRank(militaryId))
            {
                return guild_match_helper.GetMilitaryRankName(militaryId);
            }
            ///111677, 未参赛人员
            return MogoLanguageUtil.GetContent(111677);
        }

        private static string ParseGuildSkillId(object value)
        {
            int skillId = int.Parse(value.ToString());
            return guild_spell_helper.GetGuildSpellName(skillId);
        }

        private static string ParseGuildAchievementId(object value)
        {
            int achievementId = int.Parse(value.ToString());
            return guild_achievement_helper.GetName(achievementId);
        }

        private static string ParseFormulaId(object value)
        {
            int formulaID = int.Parse(value.ToString());
            mfg_formula formula = mfg_formula_helper.GetFormulaCfg(formulaID);
            if (formula != null)
            {
                Dictionary<string, string> dataDict = data_parse_helper.ParseMap(formula.__product);
                return ParseItemId(int.Parse(dataDict["1"]));
            }
            return string.Empty;
        }

        public static string ParseContent(object value)
        {
            return value.ToString();
        }

        public static string ParseNum(object value)
        {
            return value.ToString();
        }

        public static string ParseSelfName(object value)
        {
            return PlayerAvatar.Player.name;
        }

        public static string ParsePlayerId(object value)
        {
            return value.ToString();
        }

        public static string ParseDuelRankId(object value)
        {
            duel_rank_reward duelRank = duel_rank_reward_helper.GetConfig(int.Parse(value.ToString()));
            return duel_rank_reward_helper.GetNameInReward(duelRank);
        }

        public static string ParseChannelId(object value)
        {
            int channelId = int.Parse(value.ToString());
            int langId = 0;
            if (channelId == public_config.CHANNEL_ID_TEAM)
            {
                //队伍
                langId = 32528;
            }
            else if (channelId == public_config.CHANNEL_ID_GUILD)
            {
                //公会
                langId = 32529;
            }
            else if (channelId == public_config.CHANNEL_ID_WORLD)
            {
                //世界
                langId = 32530;
            }
            else if (channelId == public_config.CHANNEL_ID_PRIVATE)
            {
                //密聊
                langId = 32531;
            }
            return MogoLanguageUtil.GetContent(langId);
        }

        public static string ParseItemId(object value)
        {
            int id = int.Parse(value.ToString());
            return item_helper.GetName(id);
        }

        private static string ParseQualityItem(object value)
        {
            int id = int.Parse(value.ToString());
            return ColorDefine.GetColorHtmlString(item_helper.GetQuality(id), item_helper.GetName(id));
        }

        public static string ParseNpcId(object value)
        {
            int npcId = int.Parse(value.ToString());
            return MogoStringUtils.AppendColor("#B8860B", npc_helper.GetNpcName(npcId));
        }

        public static string ParseMonsterId(object value)
        {
            int monsterId = int.Parse(value.ToString());
            return monster_helper.GetMonsterName(monsterId);
        }

        public static string ParseInstId(object value)
        {
            int instId = int.Parse(value.ToString());
            instance inst = instance_helper.GetInstanceCfg(instId);
            if (inst != null)
            {
                return MogoLanguageUtil.GetContent(inst.__inst_name);
            }
            else
            {
                return string.Empty;
            }
        }

        public static string ParseMapId(object value)
        {
            int mapId = int.Parse(value.ToString());
            int instId = map_helper.GetInstanceIDByMapID(mapId);
            instance inst = instance_helper.GetInstanceCfg(instId);
            if (inst != null)
            {
                return MogoLanguageUtil.GetContent(inst.__inst_name);
            }
            else
            {
                return string.Empty;
            }
        }


        public static string ParseMoneyType(object value)
        {
            int id = int.Parse(value.ToString());
            switch (id)
            {
                case CostDefine.MONEY_TYPE_GOLD:
                    return MogoLanguageUtil.GetContent(14);
                case CostDefine.MONEY_TYPE_DIAMOND:
                    return MogoLanguageUtil.GetContent(15);
                case CostDefine.MONEY_TYPE_BIND_DIAMOND:
                    return MogoLanguageUtil.GetContent(16);
                default:
                    return MogoLanguageUtil.GetContent(14);
            }
        }

        private static string ParseGuildPosition(object value)
        {
            int position = int.Parse(value.ToString());
            return MogoLanguageUtil.GetContent(74626 + position - 1);
        }

    }
}
