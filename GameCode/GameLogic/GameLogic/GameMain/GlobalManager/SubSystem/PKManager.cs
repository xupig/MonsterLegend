﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameMain.Entities;

using GameLoader.Utils.CustomType;
using UnityEngine.Events;
using Common.Structs;
using GameData;
using MogoEngine.Events;
using Common.Events;
using Common.ServerConfig;
using GameLoader.Utils;
using Common.Data;
using UnityEngine;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using Common.Global;
using Common.Utils;
using Common.Chat;
using Common.Base;

namespace GameMain.GlobalManager.SubSystem
{

    public class PKManager : Singleton<PKManager>
    {

        private void RPC(action_config actionId, params object[] args)
        {
            PlayerAvatar.Player.ActionRpcCall("pk_mission_action_req", (int)actionId, args);
        }

        public void InvitePK(UInt64 dbid)
        {
            RPC(action_config.ACTION_PK_MISSION_INVITE, dbid);
        }

        public void AccpetPK(UInt64 dbid, byte accept)
        {
            RPC(action_config.ACTION_PK_MISSION_ACCEPT, dbid, accept);
        }

        public void PkReceiveInvite(UInt64 dbid, string name, byte level, byte vocation, UInt32 fight, string guildName)
        {
            PbFriendAvatarInfo pbInfo = new PbFriendAvatarInfo();
            pbInfo.dbid = dbid;
            pbInfo.name = name;
            pbInfo.level = level;
            pbInfo.vocation = vocation;
            pbInfo.fight = fight;
            pbInfo.guild_name = guildName;
            PlayerDataManager.Instance.PKData.AddPKInviteData(new PKInviteData(Global.serverTimeStamp, pbInfo));
        }

        public void OnPkMissionResp(byte[] byteArr)
        {
            PbPkMissionResult pbResult = MogoProtoUtils.ParseProto<PbPkMissionResult>(byteArr);
            CopyLogicManager.Instance.ProcessResult(ChapterType.PK, pbResult);
        }
    }
}
