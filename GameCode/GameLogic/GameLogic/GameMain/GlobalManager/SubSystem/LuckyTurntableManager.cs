﻿using System;
using System.Collections.Generic;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.Entities;
using MogoEngine.Utils;
using Common.Data;
using GameLoader.Utils.CustomType;
using MogoEngine.Events;

namespace GameMain.GlobalManager.SubSystem
{
    public class LuckyTurntableManager
    {
        private static LuckyTurntableManager _instance;

        public static LuckyTurntableManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LuckyTurntableManager();
                }
                return _instance;
            }
        }

        private LuckyTurntableData _luckyTurntableData;
        private Dictionary<int, ulong> _requestWaitDict;

        protected LuckyTurntableManager()
        {
            _luckyTurntableData = PlayerDataManager.Instance.LuckyTurntableData;
            _requestWaitDict = new Dictionary<int, ulong>();
            _requestWaitDict.Add((int)action_config.ACTION_LUCKY_TURNTABLE_RUN, 0);
            _requestWaitDict.Add((int)action_config.ACTION_LUCKY_TURNTABLE_RESET, 0);
        }

        public void LuckyTurntableResp(UInt32 actionId, UInt32 errorCode, byte[] data)
        {
            switch ((action_config)actionId)
            {
                case action_config.ACTION_LUCKY_TURNTABLE_GETINFO:
                    PbLuckyTurntableInfo pbTurntableInfo = MogoProtoUtils.ParseProto<PbLuckyTurntableInfo>(data);
                    _luckyTurntableData.UpdateTurntableInfo(pbTurntableInfo.lucky_turntable_info);
                    break;
                case action_config.ACTION_LUCKY_TURNTABLE_RUN:
                    ResetWait((int)actionId);
                    PbLuckyTurntableReward pbTurntableReward = MogoProtoUtils.ParseProto<PbLuckyTurntableReward>(data);
                    _luckyTurntableData.UpdateGetTurntableReward(pbTurntableReward);
                    break;
                case action_config.ACTION_LUCKY_TURNTABLE_RESET:
                    ResetWait((int)actionId);
                    PbLuckyTurntable pbTurntable = MogoProtoUtils.ParseProto<PbLuckyTurntable>(data);
                    _luckyTurntableData.ResetTurntableInfo(pbTurntable);
                    break;
                case action_config.ACTION_LUCKY_TURNTABLE_GET_RANKLIST:
                    PbLuckyTurntableRankList pbLuckyRank = MogoProtoUtils.ParseProto<PbLuckyTurntableRankList>(data);
                    for (int i = 0; i < pbLuckyRank.items.Count; i++)
                    {
                        PbLuckyTurntableRankListItem pbLuckyRankItem = pbLuckyRank.items[i];
                        pbLuckyRankItem.name = MogoProtoUtils.ParseByteArrToString(pbLuckyRankItem.name_b);
                    }
                    int responseCount = _luckyTurntableData.GetRespRankCount((LuckyTurntableType)pbLuckyRank.turntable_type);
                    if (responseCount < 2)
                    {
                        _luckyTurntableData.IncreaseRespRankCount((LuckyTurntableType)pbLuckyRank.turntable_type);
                    }
                    _luckyTurntableData.UpdateLuckyRankInfo(pbLuckyRank);
                    break;
                default:
                    break;
            }
        }

        public void OnTurntableActionResp(UInt32 actionId, UInt32 systemInfoId)
        {
            ResetWait((int)actionId);
        }

        public void RequestGetTurntableInfo()
        {
            RPC(action_config.ACTION_LUCKY_TURNTABLE_GETINFO, (UInt32)0);
        }

        public void RequestRunTurntable(int turntableType)
        {
            RPC(action_config.ACTION_LUCKY_TURNTABLE_RUN, (UInt32)turntableType);
        }

        public void RequestResetTurntable(int turntableType)
        {
            RPC(action_config.ACTION_LUCKY_TURNTABLE_RESET, (UInt32)turntableType);
        }

        public void RequestLuckyRankList(int turntableType)
        {
            RPC(action_config.ACTION_LUCKY_TURNTABLE_GET_RANKLIST, (UInt32)turntableType);
        }

        private void RPC(action_config actionId, UInt32 turntableType)
        {
            if (CheckIsWait((int)actionId))
                return;
            PlayerAvatar.Player.RpcCall("lucky_turntable_req", (UInt32)actionId, turntableType, new LuaTable());
        }

        private bool CheckIsWait(int actionId)
        {
            if (_requestWaitDict.ContainsKey(actionId))
            {
                ulong time = Common.Global.Global.serverTimeStamp;
                if (time - _requestWaitDict[actionId] < 5000)
                {
                    EventDispatcher.TriggerEvent(RPCEvents.RPCWait);
                    return true;
                }
                _requestWaitDict[actionId] = time;
            }
            return false;
        }

        private void ResetWait(int actionId)
        {
            if (_requestWaitDict.ContainsKey(actionId))
            {
                _requestWaitDict[actionId] = 0;
            }
        }
    }
}
