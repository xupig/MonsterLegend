﻿#region 模块信息
/*==========================================
// 文件名：MisstionManager
// 命名空间: GameMain.GlobalManager.SubSystem
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/19 9:36:59
// 描述说明：副本相关数据管理
// 其他：
//==========================================*/
#endregion

using System;
using GameMain.Entities;


using GameLoader.Utils.CustomType;
using MogoEngine.Events;
using Common.Events;
using Common.ServerConfig;
using Common.Data;
using MogoEngine.Utils;
using Common.Structs.ProtoBuf;
using MogoEngine;
using Common.Utils;
using UnityEngine;
using GameData;
using ModuleCommonUI;
using Common.Base;
using System.Collections.Generic;
using GameLoader.Utils;
using Mogo.Util;
using Common.Chat;
using Common.Global;

namespace GameMain.GlobalManager.SubSystem
{
    public class MissionManager : Singleton<MissionManager>
    {
        private PlayerAvatar _player;
        private CopyData _copyData;

        public MissionManager()
            : base()
        {
            _player = MogoWorld.Player as PlayerAvatar;
            _copyData = PlayerDataManager.Instance.CopyData;
        }

        public void ResponseMission(UInt32 actionID, UInt32 errorID, byte[] byteArr)
        {
            error_code err = (error_code)errorID;
            switch ((error_code)errorID)
            {
                case error_code.ERR_MISSION_TEAM_TYPE_SINGLE:
                    EventDispatcher.TriggerEvent(CopyEvents.SHOW_CONFIRM);
                    break;
            }
            switch ((action_config)actionID)
            {
                case action_config.ACTION_UPDATE_CLIENT_MISSION_INFO:
                    CopyStatisticDataManager.Instance.GetClientStatisticData();
                    PbMissionInfoNew pbClientMissionInfo = MogoProtoUtils.ParseProto<PbMissionInfoNew>(byteArr);
                    _copyData.FillInstance((int)pbClientMissionInfo.map_id, pbClientMissionInfo);
                    GameSceneManager.GetInstance().NoticeMissionEnd();
                    break;
                case action_config.ACTION_UPDATE_SERVER_MISSION_INFO:
                    PbMissionInfoNew pbMissionInfo = MogoProtoUtils.ParseProto<PbMissionInfoNew>(byteArr);
                    _copyData.FillInstance((int)pbMissionInfo.map_id, pbMissionInfo);
                    GameSceneManager.GetInstance().NoticeMissionEnd();
                    break;
                case action_config.ACTION_MOP_UP_MISSION:
                    PbMopUpInfo PbMopUpInfo = MogoProtoUtils.ParseProto<PbMopUpInfo>(byteArr);
                    _copyData.SweepFillInstance(PbMopUpInfo);
                    EventDispatcher.TriggerEvent(MissionEvents.MISSION_SWEEP_SUCCESSFUL);
                    break;
                case action_config.ACTION_GET_CHAPTER_RECORD:
                    PbChapterRecord chapterRecord = MogoProtoUtils.ParseProto<PbChapterRecord>(byteArr);
                    _copyData.FillChapterDict(chapterRecord);
                    break;
                case action_config.ACTION_GET_MISSION_RECORD:
                    PbMissionRecord missionRecord = MogoProtoUtils.ParseProto<PbMissionRecord>(byteArr);
                    _copyData.FillInstanceList(missionRecord);
                    break;
                case action_config.ACTION_GET_MISSION_DROPS:
                    PbDropItems dropItems = MogoProtoUtils.ParseProto<PbDropItems>(byteArr);
                    ClientDropItemManager.Instance.SetDropItemList(dropItems);
                    break;
                case action_config.ACTION_GET_STAR_SPIRIT_REWARD:
                    PbChapterInfo chapterInfo = MogoProtoUtils.ParseProto<PbChapterInfo>(byteArr);
                    _copyData.FillChapterStarRewardList(chapterInfo);
                    break;
                case action_config.GET_APPOINT_CHAPTER_RECORD:
                    _copyData.FillChapterDict(MogoProtoUtils.ParseProto<PbChapterRecord>(byteArr));
                    break;
                case action_config.ACTION_MISSION_GET_LINSE_STATE:
                    PbMapLinesState mapLinesState = MogoProtoUtils.ParseProto<PbMapLinesState>(byteArr);
                    EventDispatcher.TriggerEvent<PbMapLinesState>(CopyEvents.FieldShowAllLine, mapLinesState);
                    break;
                case action_config.ACTION_MISSION_MATCHING:
                    PbMatchingMap pbMatch = MogoProtoUtils.ParseProto<PbMatchingMap>(byteArr);
                    _copyData.FillMatchingInstance(pbMatch);
                    EventDispatcher.TriggerEvent(CopyEvents.ON_MATCH_MISSION_CHANGE);
                    break;
                case action_config.ACTION_MISSION_CANCEL_MATCH:
                    PbMatchingMap pbCancelMatch = MogoProtoUtils.ParseProto<PbMatchingMap>(byteArr);
                    ShowReMatchView(pbCancelMatch);
                    _copyData.FillCancelMatchInstance(pbCancelMatch);
                    EventDispatcher.TriggerEvent(CopyEvents.ON_MATCH_MISSION_CHANGE);
                    break;
                case action_config.ACTION_UPDATE_PVP_PVE_MISSION_INFO:
                    PbPVPMissionInfo missionInfo = MogoProtoUtils.ParseProto<PbPVPMissionInfo>(byteArr);
                    _copyData.FillInstance((int)missionInfo.map_id, missionInfo);
                    GameSceneManager.GetInstance().NoticeMissionEnd();
                    break;
                case action_config.ACTION_MISSION_GET_RESET_TIMES_RECORD:
                    PbMissionResetTimesRecord resetTimes = MogoProtoUtils.ParseProto<PbMissionResetTimesRecord>(byteArr);
                    _copyData.FillResetTimesInfo(resetTimes);
                    break;
                case action_config.ACTION_FIGHT_TRANS_PORTAL_INFO:
                    PbTransPortalList transPortal = MogoProtoUtils.ParseProto<PbTransPortalList>(byteArr);
                    int mapId = GameSceneManager.GetInstance().curMapID;
                    _copyData.FillInstance(mapId, transPortal);
                    GameSceneManager.GetInstance().NoticeMissionEnd();
                    break;
                case action_config.ACTION_RESET_MISSION_TIMES:
                    PbMissionId pbMissionId = MogoProtoUtils.ParseProto<PbMissionId>(byteArr);
                    _copyData.ResetCopyDailtyTimes(pbMissionId);
                    EventDispatcher.TriggerEvent(CopyEvents.UpdatePreview);
                    break;
                case action_config.ACTION_CAN_PLAY_AGAIN:
                    CopyLogicManager.Instance.HandlePlayAgainResult(errorID, byteArr);
                    break;
                case action_config.ACTION_AGREE_PLAY_AGAIN:
                    PbPlayAgainNotify pbPlayAgainNotify = MogoProtoUtils.ParseProto<PbPlayAgainNotify>(byteArr);
                    PlayerDataManager.Instance.PlayAgainData.FillPlayAgainAgreeData(pbPlayAgainNotify);
                    break;
                case action_config.ACTION_MISSION_STASTISTIC_INFO:
                    PbMissionStatisticList missionStatisticList = MogoProtoUtils.ParseProto<PbMissionStatisticList>(byteArr);
                    CopyStatisticDataManager.Instance.AddStaticsticData(missionStatisticList);
                    break;
                

            }
        }

        private void SendWantDonateChat(PbGroupRecordItem record)
        {
            string name = MogoProtoUtils.ParseByteArrToString(record.role_name_bytes);
            string instanceName = instance_helper.GetInstanceName(Convert.ToInt32(record.mission_id));
            string itemName = item_helper.GetName(Convert.ToInt32(record.item_list[0].id));

            string time = (6016133).ToLanguage((record.expire_time - Global.serverTimeStampSecond) / 60);

            string content = (6016150).ToLanguage(name, instanceName, itemName, time);

            ChatWantDonateLinkWrapper link = new ChatWantDonateLinkWrapper(content, record.expire_time, record.record_idx);
            ChatManager.Instance.SendClientChannelSystemMsg(public_config.CHANNEL_ID_TEAM, PlayerDataManager.Instance.ChatData.CreateWantDonateLinkMsg(link), (int)ChatContentType.WantDonate);
        }

        public void OnGroupRecordInfoResp(UInt32 actionID, UInt32 errorID, byte[] byteArr)
        {
            error_code err = (error_code)errorID;
            switch((action_config)actionID)
            {
                case action_config.ACTION_CAPTAIN_TRANSFER_NEW_REQ:
                    PbGroupRecordList recordList = MogoProtoUtils.ParseProto<PbGroupRecordList>(byteArr);
                    if (recordList.record_list.Count > 0)
                    {
                        SendWantDonateChat(recordList.record_list[0]);
                    }
                    break;
                case action_config.ACTION_CAPTAIN_TRANSFER_INFO_REQ:
                    PbGroupRecordList list = MogoProtoUtils.ParseProto<PbGroupRecordList>(byteArr);
                    PlayerDataManager.Instance.TeamData.groupRecordList = list.record_list;
                    EventDispatcher.TriggerEvent(TeamEvent.TRANSFER_INFO_CHANGE);
                    break;
                case action_config.ACTION_CAPTAIN_TRANSFER_REWARD:
                    PbApplyRecord record = MogoProtoUtils.ParseProto<PbApplyRecord>(byteArr);
                    if (err == error_code.CAPTAIN_REWEAD_APPLY_SUCCESS)
                    {
                        if (PlayerDataManager.Instance.TeamData.requiredDict.ContainsKey(record.record_idx) == false)
                        {
                            PlayerDataManager.Instance.TeamData.requiredDict.Add(record.record_idx,uint.MaxValue);
                        }
                        else
                        {
                            PlayerDataManager.Instance.TeamData.requiredDict[record.record_idx] = uint.MaxValue;
                        }
                        EventDispatcher.TriggerEvent(TeamEvent.TRANSFER_APPLY, record.record_idx);
                        MogoUtils.FloatTips(6016151);
                        //PlayerDataManager.Instance.TeamData.RemoveRecord(Convert.ToInt32(record.record_idx));
                        //EventDispatcher.TriggerEvent(TeamEvent.TRANSFER_INFO_CHANGE);
                    }
                    else if (err == error_code.CAPTAIN_REWEAD_SOMEONE_APPLY)
                    {
                        string name = MogoProtoUtils.ParseByteArrToString(record.apply_name);
                        string itemName = item_helper.GetName(Convert.ToInt32(record.item_list[0].id));
                        //ChatManager.Instance.SendClientChatMsg(public_config.CHANNEL_ID_PRIVATE,(22).ToLanguage(),ChatContentType.Text,record.apply_dbid,name,record.
                        string content = (6016140).ToLanguage(name, itemName);
                        
                        ChatDonateLinkWrapper link = new ChatDonateLinkWrapper(content, name, record.apply_dbid, Convert.ToInt32(record.item_list[0].id));
                        ChatManager.Instance.SendClientChatMsg(public_config.CHANNEL_ID_PRIVATE, PlayerDataManager.Instance.ChatData.CreateDonateLinkMsg(link), (int)ChatContentType.Donate,record.apply_dbid,name,0,0);
                    }
                    else
                    {
                        SystemInfoManager.Instance.Show(err);
                        if (err != error_code.CAPTAIN_REWARD_PLAYER_OFFLINE)
                        {
                            RequestAllTransferInfo();
                        }
                    }
                    break;
            }
        }

        private void ShowReMatchView(PbMatchingMap pbCancelMatch)
        {
            uint waitTime = uint.Parse(global_params_helper.GetConfig(104).__value) * 1000;
            ulong currentTime = Common.Global.Global.serverTimeStamp;
            ulong matchTime = _copyData.GetMatchTime((int)pbCancelMatch.mission_id);
            int mapType = GameSceneManager.GetInstance().curMapType;
            if (mapType == public_config.MAP_TYPE_CITY || mapType == public_config.MAP_TYPE_WILD)
            {
                if (matchTime != 0 && waitTime <= (currentTime - matchTime))
                {
                    if (PlayerDataManager.Instance.TeamData.TeammateDic.Count == 0 || PlayerAvatar.Player.dbid == PlayerDataManager.Instance.TeamData.CaptainId)
                    {
                  //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(97026), delegate() { MessageBox.CloseMessageBox(); }, delegate() { MissionManager.Instance.RequsetEnterMission((int)pbCancelMatch.mission_id, 1); }, MogoLanguageUtil.GetContent(97028), MogoLanguageUtil.GetContent(97027));
                    }
                    else
                    {
                       //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(97026), PanelIdEnum.MainUIField);
                    }
                }
            }
        }

        public void RequsetGetChapterRecord()
        {
            RpcCallMission(action_config.ACTION_GET_CHAPTER_RECORD);
        }

        public void RequsetGetChapterRecord(int chapterId)
        {
            RpcCallMission(action_config.GET_APPOINT_CHAPTER_RECORD, chapterId);
        }

        public void RequsetGetMissionRecord()
        {
            RpcCallMission(action_config.ACTION_GET_MISSION_RECORD);
        }

        public void RequsetGetStarSpiritReward(int chapterId, int starNum)
        {
            RpcCallMission(action_config.ACTION_GET_STAR_SPIRIT_REWARD, (UInt16)chapterId, (UInt16)starNum);
        }

        public void RequsetCleanUpMission(int missionId, int cleanCount)
        {
            RpcCallMission(action_config.ACTION_MOP_UP_MISSION, missionId, cleanCount);
        }

        public void RequsetEnterMission(int missionId, int isMatch = 0, int isCheckTeam = 0)
        {
            RpcCallMission(action_config.ACTION_ENTER_MISSION, missionId, isMatch, isCheckTeam);
        }

        public void RequsetCancelMatchMission(int missionId)
        {
            RpcCallMission(action_config.ACTION_MISSION_CANCEL_MATCH, missionId);
        }

        public void RequsetBuyCopyEnterCount(ChapterType type)
        {
            action_config.ACTION_MISSION_TYPE_BUY_TIMES.Register();
            RpcCallMission(action_config.ACTION_MISSION_TYPE_BUY_TIMES, (int)type);
        }

        public void RequestResetTimes(int missionId)
        {
            action_config.ACTION_RESET_MISSION_TIMES.Register();
            RpcCallMission(action_config.ACTION_RESET_MISSION_TIMES, missionId);
        }

        public void RequestResetTimesInfo()
        {
            RpcCallMission(action_config.ACTION_MISSION_GET_RESET_TIMES_RECORD);
        }

        public void RequsetLeaveMission(ScoreRuleData battleResultData)
        {
            int mapId = GameSceneManager.GetInstance().curMapID;
            RequsetLeaveClientMisstion(mapId, battleResultData);
        }

        public void RequsetQuitMisstion()
        {
            LoggerHelper.Debug("RequsetQuitMisstion");
            action_config.ACTION_QUIT_MISSION.Register();
            RpcCallMission(action_config.ACTION_QUIT_MISSION);
        }

        private void RequsetLeaveClientMisstion(int mapId, ScoreRuleData data)
        {
            LuaTable table = new LuaTable();
            table.Add(public_config.MISSION_COMMON_ACTION_ID, data.tableNormal);
            table.Add(public_config.MISSION_SPECIAL_ACTION_ID, data.tableSpecial);
            //客户端本难度都为1
            Debug.Log("客户端发送结算:" + mapId);
            RpcCallMission(action_config.ACTION_FINISH_CLIENT_MISSION, mapId, 1, table);
        }

        private Dictionary<int, int> GetScoreRuleDict(Dictionary<string, string[]> scoreRule)
        {
            Dictionary<int, int> dict = new Dictionary<int, int>();
            foreach (string key in scoreRule.Keys)
            {
                if (int.Parse(key) >= 1000)
                {
                    dict.Add(int.Parse(scoreRule[key][0]), int.Parse(key));
                }
            }
            return dict;
        }

        public void RequestMissionFail()
        {
            RpcCallMission(action_config.ACTION_FINISH_CLIENT_MISSION_LOSE, GameSceneManager.GetInstance().curMapID);
        }

        public void RequestLineInfo(int missionId)
        {
            RpcCallMission(action_config.ACTION_MISSION_GET_LINSE_STATE, missionId);
        }

        public void RequestSwitchLine(int missionId, int lineNum)
        {
            RpcCallMission(action_config.ACTION_MISSION_SWITCH_TO_LINE, missionId, lineNum);
        }

        public void RequestCanPlayAgain()
        {
            //Debug.LogError("RequestCanPlayAgain");
            RpcCallMission(action_config.ACTION_CAN_PLAY_AGAIN);
        }

        public void RequestAgreePlayAgain(bool agree)
        {
            //Debug.LogError("ACTION_AGREE_PLAY_AGAIN:" + agree);
            RpcCallMission(action_config.ACTION_AGREE_PLAY_AGAIN, agree ? 1 : 0);
        }


        public void RequestAllTransferInfo()
        {
            RpcCallMission(action_config.ACTION_CAPTAIN_TRANSFER_INFO_REQ);
        }

        public void RequestTransfer(int id)
        {
            RpcCallMission(action_config.ACTION_CAPTAIN_TRANSFER_REWARD, id);
        }

        private void RpcCallMission(action_config action, params object[] args)
        {
            _player.ActionRpcCall("mission_action_req", (int)action, args);
        }
    }
}
