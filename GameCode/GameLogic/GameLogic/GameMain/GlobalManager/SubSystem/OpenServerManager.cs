﻿using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class OpenServerManager : Singleton<OpenServerManager>
    {
        private static PlayerAvatar _player;
        private OpenServerData _data;

        public OpenServerManager()
        {
            _player = PlayerAvatar.Player;
            _data = PlayerDataManager.Instance.OpenServerData;
        }

        public void RequestInfo()
        {
            RPC(action_config.ACTION_CREATE_ACTIVITY_QUERY_LIST);
        }

        public void RequesetGetReward(int issueId)
        {
            RPC(action_config.ACTION_CREATE_ACTIVITY_GET_REWARD, issueId);
        }

        public void RequesetGetFinalReward()
        {
            RPC(action_config.ACTION_CREATE_ACTIVITY_GET_FINAL_REWARD);
        }

        private void RPC(action_config actionId, params object[] args)
        {
            _player.ActionRpcCall("create_activity_req", (int)(actionId), args);
        }

        public void ResponseInfo(byte[] byteArray)
        {
            PbCreateActivityList pb = MogoProtoUtils.ParseProto<PbCreateActivityList>(byteArray);
            _data.FillData(pb);
        }

        public void ResponseUpdate(byte[] byteArray)
        {
            PbCreateActivityInfo pb = MogoProtoUtils.ParseProto<PbCreateActivityInfo>(byteArray);
            _data.UpdateOneData(pb);
        }

        public void ResponseComplete(byte[] byteArray)
        {
            PbCreateActivityInfo pb = MogoProtoUtils.ParseProto<PbCreateActivityInfo>(byteArray);
            _data.CompleteOneData(pb);
        }

        public void ResponseGetReward(byte[] byteArray)
        {
            PbCreateActivityInfo pb = MogoProtoUtils.ParseProto<PbCreateActivityInfo>(byteArray);
            _data.GotRewardOneData(pb);
        }

        public void ResponseGetFinalReward(byte[] byteArray)
        {
            EventDispatcher.TriggerEvent(OpenServerEvents.Update);
        }
    }
}
