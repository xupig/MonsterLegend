﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.ServerConfig;
using Common.Utils;
using GameLoader.Utils.CustomType;
using GameMain.Entities;
using GameData;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using Common.Data;
using MogoEngine.Events;
using Common.Events;

namespace GameMain.GlobalManager.SubSystem
{
    public class EnergyBuyingManager : Singleton<EnergyBuyingManager>
    {
        private EnergyBuyingData _energyBuyData;

        private static readonly LuaTable ENERGY_BUY_EMPTY_LUA_TABLE = new LuaTable();

        public EnergyBuyingManager()
        {
            _energyBuyData = PlayerDataManager.Instance.EnergyBuyingData;
        }

        public void OnPriceResp(UInt32 actionId, UInt32 errorCode, byte[] data)
        {
            if (errorCode > 0)
            {
                return;
            }
            if (actionId == (int)action_config.ACTION_GET_PRICE_BUY_COUNT)
            {
                PbPriceList pbPriceList = MogoProtoUtils.ParseProto<PbPriceList>(data);
                int priceId = (int)pbPriceList.price_info[0].price_id;
                if (priceId == (int)PriceListId.energyBuy)
                {
                    int buyCount = (int)pbPriceList.price_info[0].buy_count;
                    _energyBuyData.UpdateBuyTimes(buyCount);
                    EventDispatcher.TriggerEvent(EnergyBuyingEvents.UPDATE_BUY_TIMES);
                }
            }
        }

        public void OnBuyEnergyResp(UInt32 actionId, UInt32 errorCode)
        {
            _energyBuyData.IsBuying = false;
        }

        public void OnZeroClockResp()
        {
            _energyBuyData.ResetBuyTimes();
            EventDispatcher.TriggerEvent(EnergyBuyingEvents.UPDATE_BUY_TIMES);
        }

        public void RequestBuyOnceEnergy()
        {
            action_config.ACTION_ENERGY_BUY_ONCE.Register();
            RPC(action_config.ACTION_ENERGY_BUY_ONCE, ENERGY_BUY_EMPTY_LUA_TABLE);
        }

        public void RequestBuyAllEnergy()
        {
            action_config.ACTION_ENERGY_BUY_ALL.Register();
            RPC(action_config.ACTION_ENERGY_BUY_ALL, ENERGY_BUY_EMPTY_LUA_TABLE);
        }

        public void RequestBuyTimes()
        {
            PlayerAvatar.Player.ActionRpcCall("price_action_req", (int)action_config.ACTION_GET_PRICE_BUY_COUNT, (int)PriceListId.energyBuy);
        }

        private void RPC(action_config actionId, params object[] args)
        {
            PlayerAvatar.Player.ActionRpcCall("energy_action_req", (int)actionId, args);
        }
    }
}
