﻿using System;
using System.Collections.Generic;
using Common.Data;
using Common.ServerConfig;
using Common.Utils;
using GameMain.Entities;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using MogoEngine.Events;
using Common.Events;
using GameData;

namespace GameMain.GlobalManager.SubSystem
{
    public class RewardManager : Singleton<RewardManager>
    {
        private DailyTaskData _dailyTaskData;
        private MonthLoginData _monthLoginData;
        private DailyActiveTaskData _dailyActiveTaskData;
        private OpenServerLoginRewardData _openServerLoginRewardData;

        public RewardManager()
        {
            _dailyTaskData = PlayerDataManager.Instance.RewardData.GetDailyTaskData();
            _monthLoginData = PlayerDataManager.Instance.RewardData.GetMonthLoginData();
            _dailyActiveTaskData = PlayerDataManager.Instance.RewardData.GetDailyActiveData();
            _openServerLoginRewardData = PlayerDataManager.Instance.RewardData.GetOpenServerLoginData();
        }

        public DailyTaskData GetDailyTaskData()
        {
            return _dailyTaskData;
        }

        public void OnRewardResp(UInt32 actionId, UInt32 errorCode, byte[] data)
        {
            if (errorCode > 0)
            {
                return;
            }
            switch ((action_config)actionId)
            {
                case action_config.ACTION_AWARD_GET_DAILY_TASK_RECORD:
                    {
                        PbAwardDailyTaskRecord awardTaskRecord = MogoProtoUtils.ParseProto<PbAwardDailyTaskRecord>(data);
                        ResponseGetDailyTaskInfos(awardTaskRecord.daily_task_info);
                        EventDispatcher.TriggerEvent(RewardEvents.UPDATE_DAILY_TASK);
                    }
                    break;
                case action_config.ACTION_AWARD_PUSH_DAILY_TASK_ACCEPT:
                    {
                        PbAwardTaskRecord awardTaskRecord = MogoProtoUtils.ParseProto<PbAwardTaskRecord>(data);
                        ResponseAcceptDailyTask(awardTaskRecord.task_id);
                        EventDispatcher.TriggerEvent(RewardEvents.UPDATE_DAILY_TASK);
                    }
                    break;
                case action_config.ACTION_AWARD_PUSH_DAILY_TASK_COMPLETTED:
                    {
                        PbAwardTaskRecord awardTaskRecord = MogoProtoUtils.ParseProto<PbAwardTaskRecord>(data);
                        ResponseCompleteDailyTask(awardTaskRecord.task_id);
                        EventDispatcher.TriggerEvent(RewardEvents.UPDATE_DAILY_TASK);
                    }
                    break;
                case action_config.ACTION_AWARD_DAILY_TASK_GET_REWARD:
                    {
                        PbAward award = MogoProtoUtils.ParseProto<PbAward>(data);
                        ResponseGetDailyTaskReward(award);
                    }
                    break;
                case action_config.ACTION_AWARD_GET_MONTH_LOGIN_RECORD:
                    {
                        PbAwardMonthLoginRecord monthLoginRecord = MogoProtoUtils.ParseProto<PbAwardMonthLoginRecord>(data);
                        ResponseUpdateMonthLogin(monthLoginRecord.month_login_info);
                        EventDispatcher.TriggerEvent(RewardEvents.UPDATE_MONTH_LOGIN);
                    }
                    break;
                case action_config.ACTION_AWARD_MONTH_LOGIN_GET_REWARD:
                    {
                        PbAwardMonthLoginInfo monthLoginInfo = MogoProtoUtils.ParseProto<PbAwardMonthLoginInfo>(data);
                        ResponseGetMonthLoginReward(monthLoginInfo);
                    }
                    break;
                case action_config.ACTION_AWARD_GET_ACTIVE_TASK_RECORD:
                    {
                        PbAwardActiveTaskRecord activeTaskRecord = MogoProtoUtils.ParseProto<PbAwardActiveTaskRecord>(data);
                        ResponseGetDailyActiveInfos(activeTaskRecord.active_task_info);
                        EventDispatcher.TriggerEvent(RewardEvents.UPDATE_ACTIVE_TASK);
                    }
                    break;
                case action_config.ACTION_AWARD_PUSH_ACTIVE_TASK_ACCEPT:
                    {
                        PbAwardTaskRecord awardTaskRecord = MogoProtoUtils.ParseProto<PbAwardTaskRecord>(data);
                        ResponseAcceptDailyActive(awardTaskRecord.task_id);
                        EventDispatcher.TriggerEvent(RewardEvents.UPDATE_ACTIVE_TASK);
                    }
                    break;
                case action_config.ACTION_AWARD_PUSH_ACTIVE_TASK_COMPLETTED:
                    {
                        PbAwardTaskRecord awardTaskRecord = MogoProtoUtils.ParseProto<PbAwardTaskRecord>(data);
                        ResponseCompleteDailyActive(awardTaskRecord.task_id);
                        EventDispatcher.TriggerEvent(RewardEvents.UPDATE_ACTIVE_TASK);
                    }
                    break;
                case action_config.ACTION_AWARD_NOTIFY_ACTIVE_TASK_TRIGGER:
                    {
                        PbAwardActiveTaskProgressInfo progressInfo = MogoProtoUtils.ParseProto<PbAwardActiveTaskProgressInfo>(data);
                        ResponseUpdateDailyActiveProgress(progressInfo);
                    }
                    break;
                case action_config.ACTION_AWARD_ACTIVE_TASK_GET_REWARD:
                    {
                        PbAward award = MogoProtoUtils.ParseProto<PbAward>(data);
                        ResponseGetDailyActiveReward(award);
                    }
                    break;
                case action_config.ACTION_AWARD_GET_ACTIVE_TASK_BOX_RECORD:
                    {
                        PbAwardActiveTaskBoxRecord activeBoxRecord = MogoProtoUtils.ParseProto<PbAwardActiveTaskBoxRecord>(data);
                        ResponseGetActiveBoxInfos(activeBoxRecord.task_box_info);
                        EventDispatcher.TriggerEvent(RewardEvents.UPDATE_ACTIVE_BOX_INFO);
                    }
                    break;
                case action_config.ACTION_AWARD_GET_BOX_REWARD:
                    {
                        PbAward award = MogoProtoUtils.ParseProto<PbAward>(data);
                        ResponseGetActiveBoxReward(award);
                    }
                    break;
                case action_config.ACTION_AWARD_ACTIVE_TASK_COMPENSATE_VIP_REWARD:
                    {
                        PbAwardBoxReward boxReward = MogoProtoUtils.ParseProto<PbAwardBoxReward>(data);
                        ResponseGetBoxVipReward(boxReward.award);
                    }
                    break;
                case action_config.ACTION_AWARD_NOTIFY_ACTIVE_RETURN_STATE:
                    {
                        PbTaskReturnStateInfo pbReturnStateInfo = MogoProtoUtils.ParseProto<PbTaskReturnStateInfo>(data);
                        ResponseUpdateActiveTaskReturnStateInfo(pbReturnStateInfo);
                    }
                    break;
                default:
                    break;
            }
        }

        public void OnCardResp(UInt32 actionId, UInt32 errorCode, byte[] data)
        {
            if (errorCode > 0)
            {
                return;
            }
            switch ((action_config)actionId)
            {
                case action_config.ACTION_GET_APPOINT_CARD_INFO:
                    {
                        PbCardInfo cardInfo = MogoProtoUtils.ParseProto<PbCardInfo>(data);
                        ResponseGetAppointCardInfo(cardInfo);
                        EventDispatcher.TriggerEvent(RewardEvents.UPDATE_CARD_INFO);
                    }
                    break;
                default:
                    break;
            }
        }

        public void OnReloadDailyActiveData()
        {
            _dailyActiveTaskData.OnReloadData();
            RequestGetDailyActiveInfo();
            RequestGetDailyActiveBoxInfo();
        }

        public void OnReloadDailyTaskData()
        {
            _dailyTaskData.OnReloadData();
            RequestGetDailyTaskInfo();
        }

        #region 每日任务返回

        public void ResponseGetDailyTaskInfos(List<PbAwardTaskInfo> awardTaskInfoList)
        {
            //先重置日常任务列表，再更新日常任务数据
            Dictionary<int, PbAwardTaskInfo> taskDict = new Dictionary<int, PbAwardTaskInfo>();
            for (int i = 0; i < awardTaskInfoList.Count; i++)
            {
                PbAwardTaskInfo info = awardTaskInfoList[i];
                taskDict.Add((int)info.task_id, info);
            }
            _dailyTaskData.ResetAcceptDailyTaskList();
            _dailyTaskData.UpdateDailyTaskList(taskDict);
            ProcessRewardPoint(RewardSystemType.DailyTask);
        }

        public void ResponseAcceptDailyTask(List<PbAwardTaskId> awardTaskIdList)
        {
            List<int> taskIdList = new List<int>();
            for (int i = 0; i < awardTaskIdList.Count; i++)
            {
                taskIdList.Add((int)awardTaskIdList[i].id);
            }
            _dailyTaskData.UpdateDailyTaskListByIds(taskIdList);
        }

        public void ResponseCompleteDailyTask(List<PbAwardTaskId> awardTaskIdList)
        {
            Dictionary<int, int> taskDict = new Dictionary<int, int>();
            for (int i = 0; i < awardTaskIdList.Count; i++)
            {
                taskDict.Add((int)awardTaskIdList[i].id, (int)DailyTaskStatus.CanCommit);
            }
            _dailyTaskData.UpdateDailyTaskList(taskDict);
            ProcessRewardPoint(RewardSystemType.DailyTask);
        }

        public void ResponseGetDailyTaskReward(PbAward awardInfo)
        {
            Dictionary<int, int> taskDict = new Dictionary<int, int>();
            int taskId = (int)awardInfo.task_id;
            taskDict.Add(taskId, (int)DailyTaskStatus.Complete);
            _dailyTaskData.UpdateDailyTaskList(taskDict);
            EventDispatcher.TriggerEvent(RewardEvents.UPDATE_DAILY_TASK);
            ProcessRewardPoint(RewardSystemType.DailyTask);
        }
        #endregion

        #region 月签到返回

        public void ResponseUpdateMonthLogin(List<PbAwardMonthLoginInfo> monthLoginInfos)
        {
            _monthLoginData.UpdateMonthLoginSign(monthLoginInfos);
            ProcessRewardPoint(RewardSystemType.MonthLoginReward);
        }

        public void ResponseGetMonthLoginReward(PbAwardMonthLoginInfo loginInfo)
        {
            if (_monthLoginData.SignDayNum == (int)loginInfo.login_days)
            {
                int signDay = (int)loginInfo.login_days;
                _monthLoginData.GetSignReward(signDay);
                EventDispatcher.TriggerEvent(RewardEvents.UPDATE_MONTH_LOGIN);
                ProcessRewardPoint(RewardSystemType.MonthLoginReward);
            }
        }
        #endregion

        #region 每日活跃任务返回

        public void ResponseGetDailyActiveInfos(List<PbAwardTaskInfo> awardTaskInfoList)
        {
            Dictionary<int, PbAwardTaskInfo> taskDict = new Dictionary<int, PbAwardTaskInfo>();
            for (int i = 0; i < awardTaskInfoList.Count; i++)
            {
                PbAwardTaskInfo info = awardTaskInfoList[i];
                taskDict.Add((int)info.task_id, info);
            }
            _dailyActiveTaskData.ResetDailyActiveList();
            _dailyActiveTaskData.UpdateDailyActiveList(taskDict);
            ProcessRewardPoint(RewardSystemType.DailyActiveTask);
            EventDispatcher.TriggerEvent(TaskEvent.TASK_MAIN_TASK_CHANGE,true);
        }

        public void ResponseAcceptDailyActive(List<PbAwardTaskId> awardTaskIdList)
        {
            List<int> taskIdList = new List<int>();
            for (int i = 0; i < awardTaskIdList.Count; i++)
            {
                taskIdList.Add((int)awardTaskIdList[i].id);
            }
            _dailyActiveTaskData.UpdateDailyActiveListByIds(taskIdList);
        }

        public void ResponseCompleteDailyActive(List<PbAwardTaskId> awardTaskIdList)
        {
            Dictionary<int, int> taskDict = new Dictionary<int, int>();
            for (int i = 0; i < awardTaskIdList.Count; i++)
            {
                taskDict.Add((int)awardTaskIdList[i].id, (int)DailyActiveStatus.CanCommit);
            }
            _dailyActiveTaskData.UpdateDailyActiveListByStatus(taskDict);
            ProcessRewardPoint(RewardSystemType.DailyActiveTask);
        }

        public void ResponseUpdateDailyActiveProgress(PbAwardActiveTaskProgressInfo progressInfo)
        {
            _dailyActiveTaskData.UpdateDailyActiveProgressInfo(progressInfo);
        }

        public void ResponseGetDailyActiveReward(PbAward awardInfo)
        {
            Dictionary<int, int> taskDict = new Dictionary<int, int>();
            int taskId = (int)awardInfo.task_id;
            taskDict.Add(taskId, (int)DailyActiveStatus.Complete);
            _dailyActiveTaskData.UpdateDailyActiveListByStatus(taskDict);
            EventDispatcher.TriggerEvent(RewardEvents.UPDATE_ACTIVE_TASK);
            EventDispatcher.TriggerEvent(TaskEvent.TASK_MAIN_TASK_CHANGE,true);
            ProcessRewardPoint(RewardSystemType.DailyActiveTask);
        }

        public void ResponseGetActiveBoxInfos(List<PbAwardActiveTaskBoxInfo> activeBoxInfoList)
        {
            if (activeBoxInfoList.Count == 0)
            {
                _dailyActiveTaskData.ResetActiveBoxList();
            }
            _dailyActiveTaskData.UpdateActiveBoxList(activeBoxInfoList);
            ProcessRewardPoint(RewardSystemType.DailyActiveTask);
        }

        public void ResponseGetActiveBoxReward(PbAward awardInfo)
        {
            //更新是否已领取vip奖励
            int boxId = (int)awardInfo.task_id;
            _dailyActiveTaskData.UpdateGetBoxReward(boxId);
            EventDispatcher.TriggerEvent(RewardEvents.UPDATE_ACTIVE_TASK);
            ProcessRewardPoint(RewardSystemType.DailyActiveTask);
        }

        public void ResponseGetBoxVipReward(List<PbAward> awardInfoList)
        {
            //补领的奖励会通过邮件发放
            List<PbItem> itemList = new List<PbItem>();
            for (int i = 0; i < awardInfoList.Count; i++)
            {
                PbAward awardInfo = awardInfoList[i];
                int boxId = (int)awardInfo.task_id;
                _dailyActiveTaskData.UpdateGetBoxVipReward(boxId);
                itemList.AddRange(awardInfo.award);
            }
            EventDispatcher.TriggerEvent(RewardEvents.UPDATE_ACTIVE_TASK);
            EventDispatcher.TriggerEvent(RewardEvents.GET_ACTIVE_BOX_VIP_REWARD);
            ProcessRewardPoint(RewardSystemType.DailyActiveTask);
        }

        public void ResponseUpdateActiveTaskReturnStateInfo(PbTaskReturnStateInfo pbReturnStateInfo)
        {
            _dailyActiveTaskData.UpdateDailyActiveReturnStateInfo(pbReturnStateInfo);
            EventDispatcher.TriggerEvent(RewardEvents.UPDATE_ACTIVE_TASK);
            ProcessRewardPoint(RewardSystemType.DailyActiveTask);
        }

        public void ResponseGetAppointCardInfo(PbCardInfo cardInfo)
        {
            _dailyTaskData.UpdateCardInfo(cardInfo);
        }

        public void OnZeroClockResp()
        {
            _dailyActiveTaskData.ResetActiveBoxList();
            EventDispatcher.TriggerEvent(RewardEvents.UPDATE_ACTIVE_BOX_INFO);
            ProcessRewardPoint(RewardSystemType.DailyActiveTask);
        }
        #endregion

        #region 请求协议

        public void RequestGetAllRewardInfo()
        {
            RequestGetDailyTaskInfo();
            RequestGetMonthLoginInfo();
            RequestGetDailyActiveInfo();
            RequestGetDailyActiveBoxInfo();
            RequestGetAppointCardInfo(public_config.CARD_TYPE_MONTH);
        }

        //请求每日任务信息
        public void RequestGetDailyTaskInfo()
        {
            RPC(action_config.ACTION_AWARD_GET_DAILY_TASK_RECORD);
        }

        //请求获取每日任务奖励
        public void RequestGetDailyTaskReward(int id)
        {
            RPC(action_config.ACTION_AWARD_DAILY_TASK_GET_REWARD, id);
        }

        //请求月登陆信息
        public void RequestGetMonthLoginInfo()
        {
            if (_monthLoginData.MonthLoginList.Count == 0)
            {
                _monthLoginData.InitMonthLoginList();
            }
            RPC(action_config.ACTION_AWARD_GET_MONTH_LOGIN_RECORD);
        }

        //请求获取月登陆奖励
        public void RequestGetMonthLoginReward()
        {
            RPC(action_config.ACTION_AWARD_MONTH_LOGIN_GET_REWARD);
        }

        public void RequestGetDailyActiveInfo()
        {
            RPC(action_config.ACTION_AWARD_GET_ACTIVE_TASK_RECORD);
        }

        public void RequestGetDailyActiveReward(int id)
        {
            RPC(action_config.ACTION_AWARD_ACTIVE_TASK_GET_REWARD, id);
        }

        public void RequestGetDailyActiveBoxInfo()
        {
            RPC(action_config.ACTION_AWARD_GET_ACTIVE_TASK_BOX_RECORD);
        }

        public void RequestGetDailyActiveBoxReward(int boxId)
        {
            RPC(action_config.ACTION_AWARD_GET_BOX_REWARD, boxId);
        }

        public void RequestGetActiveBoxVipReward()
        {
            RPC(action_config.ACTION_AWARD_ACTIVE_TASK_COMPENSATE_VIP_REWARD);
        }

        public void RequestGetAppointCardInfo(int cardType)
        {
            RPCCard(action_config.ACTION_GET_APPOINT_CARD_INFO, cardType);
        }

        #endregion

        private void RPC(action_config actionId, params object[] args)
        {
            PlayerAvatar.Player.ActionRpcCall("award_action_req", (int)actionId, args);
        }

        private void RPCCard(action_config actionId, params object[] args)
        {
            PlayerAvatar.Player.ActionRpcCall("card_action_req", (int)actionId, args);
        }

        public void ProcessRewardPoint(RewardSystemType type)
        {
            RewardDataManager rewardData = PlayerDataManager.Instance.RewardData;
            rewardData.ProcessRewardTipsPoint(type);
        }

        public void UpdateOpenServerLoginRewardData()
        {
            RewardDataManager rewardData = PlayerDataManager.Instance.RewardData;
            rewardData.UpdateOpenServerLoginRewardData();
        }
    }
}
