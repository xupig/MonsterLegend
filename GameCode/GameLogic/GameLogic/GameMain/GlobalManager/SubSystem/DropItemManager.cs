﻿using Common.Utils;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine;
using MogoEngine.RPC;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/5/21 10:31:08 
 * function: 
 * *******************************************************/

namespace GameMain.GlobalManager.SubSystem
{
    public class DropItemManager : Singleton<DropItemManager> 
    {
        public void PickupItemResp(ushort ecode, uint entityId)
        {
            LoggerHelper.Debug(string.Format("PickupItemResp, ecode = {0}, entityId = {1}", ecode, entityId));
        }

        public void PickupItemNotify(uint entityId, uint pickerId)
        {
            LoggerHelper.Debug(string.Format("PickupItemNotify, entityId = {0}, pickerId = {1}", entityId, pickerId));
            Entity entity = MogoWorld.GetEntity(entityId);
            if (entity == null || !(entity is EntityDropItem))
            {
                LoggerHelper.Error(string.Format("掉落物被拾取操作失败，entityId = {0}的实体为空，或不是掉落物类型", entityId));
                return;
            }
            (entity as EntityDropItem).RecievePickResp(pickerId);
        }
    }
}
