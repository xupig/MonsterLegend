﻿#region 模块信息
/*==========================================
// 文件名：WorldBossRankManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/2/18 16:40:07
// 描述说明：
// 其他：
//==========================================*/
#endregion
using Common.Base;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameLoader.Utils.Timer;

namespace GameMain.GlobalManager.SubSystem
{
    public class WorldBossRankManager : Singleton<WorldBossRankManager>
    {
        private PbWorldBossRankInfoList _latestRankInfo;

        public bool HideTheRankingPanel = false;

        private bool firstArriveData = true;
        private bool soloDataHasArrived = false;

        private uint timerId;

        public void UpdateSoloRankInfo(PbWorldBossRankInfoList pbWorldBossRankInfoList)
        {
            if (HideTheRankingPanel == true)
            {
                UIManager.Instance.ClosePanel(PanelIdEnum.WorldBossRanking);
                return;
            }
            soloDataHasArrived = true;
            _latestRankInfo = pbWorldBossRankInfoList;
            UIManager.Instance.ShowPanel(PanelIdEnum.WorldBossRanking);
            OpenTimer();
        }

        private void OpenTimer()
        {
            if (firstArriveData == true)
            {
                timerId = TimerHeap.AddTimer(0, 2000, ResetDataArriveMark);
                firstArriveData = false;
            }
        }

        private void ResetDataArriveMark()
        {
            if (soloDataHasArrived == false)
            {
                UIManager.Instance.ClosePanel(PanelIdEnum.WorldBossRanking);
                TimerHeap.DelTimer(timerId);
                timerId = 0;
                firstArriveData = true;
                _latestRankInfo = null;
            }
            else
            {
                soloDataHasArrived = false;
            }
        }

        public PbWorldBossRankInfoList GetLastestWorldInfo()
        {
            return _latestRankInfo;
        }
    }
}