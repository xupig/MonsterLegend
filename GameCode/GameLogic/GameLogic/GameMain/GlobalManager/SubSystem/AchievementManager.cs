﻿#region 模块信息
/*==========================================
// 文件名：AchievementManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/10/29 20:21:39
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.ServerConfig;
using Common.Utils;
using GameMain.Entities;
using System;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using MogoEngine.Events;
using Common.Events;
using Common.Base;
using GameData;
using UnityEngine;
namespace GameMain.GlobalManager.SubSystem
{
    public class AchievementManager : Singleton<AchievementManager>
    {
        private AchievementData _achievementData = PlayerDataManager.Instance.AchievementData;

        #region 请求数据
        public void RequestGetAchievementRecord()
        {
            PlayerAvatar.Player.RpcCall("achievement_req", action_config.ACTION_ACHIEVEMENT_QUERY_LIST, 0);
        }

        public void RequestGetAchievementReward(int achieveId)
        {
            PlayerAvatar.Player.RpcCall("achievement_req", action_config.ACTION_ACHIEVEMENT_REWARD, achieveId);
        }

        #endregion

        #region 服务器返回
        public void AchievementResq(UInt32 actionId, UInt32 errorId, byte[] byteArr)
        {
            if (errorId != 0)
            {
                switch ((error_code)errorId)
                {
                    // 奖励不存在
                    case error_code.ERR_ACHIEVEMENT_CFG_NOT_FOUND:
                        break;
                    // 奖励字段为空
                    case error_code.ERR_ACHIEVEMENT_NULL_FIELD_REWARD:
                        break;
                }
                return;
            }
            switch ((action_config)actionId)
            {
                case action_config.ACTION_ACHIEVEMENT_QUERY_LIST:
                    PbAchievementQuery pbQuery = MogoProtoUtils.ParseProto<PbAchievementQuery>(byteArr);
                    _achievementData.Init(pbQuery);
                    break;
                case action_config.ACTION_ACHIEVEMENT_COMPLETE:
                    PbAchievementInfo pbInfo = MogoProtoUtils.ParseProto<PbAchievementInfo>(byteArr);
                    _achievementData.UpdateCompletedAchievement(pbInfo);
                    if (achievement_helper.NeedShowGetTip(pbInfo.achv_id) && function_helper.IsFunctionOpen(FunctionId.Achievement))
                    {
                        UIManager.Instance.ShowPanel(PanelIdEnum.AchievementObtainTips, pbInfo.achv_id);
                    }
                    EventDispatcher.TriggerEvent(AchievementEvents.REFRESH_ACHIEVEMENT_LIST);
                    EventDispatcher.TriggerEvent(AchievementEvents.REFRESH_ACHIEVEMENT_TOGGLE_STATE);
                    break;
                case action_config.ACTION_ACHIEVEMENT_REWARD:
                    PbAchievementInfo pbInfo2 = MogoProtoUtils.ParseProto<PbAchievementInfo>(byteArr);
                    _achievementData.UpdateRewardedAchievement(pbInfo2);
                    EventDispatcher.TriggerEvent(AchievementEvents.REFRESH_ACHIEVEMENT_LIST);
                    EventDispatcher.TriggerEvent(AchievementEvents.REFRESH_ACHIEVEMENT_TOGGLE_STATE);
                    break;
                case action_config.ACTION_ACHIEVEMENT_UPDATE:
                    PbAchievementInfo pbInfo3 = MogoProtoUtils.ParseProto<PbAchievementInfo>(byteArr);
                    _achievementData.UpdateProcessingAchievement(pbInfo3);
                    EventDispatcher.TriggerEvent(AchievementEvents.REFRESH_ACHIEVEMENT_LIST);
                    EventDispatcher.TriggerEvent(AchievementEvents.REFRESH_ACHIEVEMENT_TOGGLE_STATE);
                    break;
            }
            CheckHasCompletedAchievement();
        }

        private void CheckHasCompletedAchievement()
        {
            if (PlayerDataManager.Instance.AchievementData.HasCompletedAchievement()
                && PlayerDataManager.Instance.FunctionData.IsFunctionOpen(FunctionId.Achievement))
            {
                PlayerDataManager.Instance.FunctionData.AddFunctionPoint(FunctionId.reward);
            }
            if (PlayerDataManager.Instance.AchievementData.HasCompletedAchievement() == false
                && PlayerDataManager.Instance.RewardData.IsShowRewardSystemPoint() == false
                && PlayerDataManager.Instance.playerReturnData.HasRewardToGet() == false)
            {
                PlayerDataManager.Instance.FunctionData.RemoveFunctionPoint(FunctionId.reward);
            }
        }
        #endregion
    }

    public class AchievementTipsParam
    {
        public int achieveId;
        public Vector3 targetPos;
        public Vector2 targetSizeDelta;
    }
}