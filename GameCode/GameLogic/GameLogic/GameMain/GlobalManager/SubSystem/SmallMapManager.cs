﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameMain.Entities;

using GameLoader.Utils.CustomType;
using UnityEngine.Events;
using Common.Structs;
using GameData;
using MogoEngine.Events;
using Common.Events;
using Common.ServerConfig;
using GameLoader.Utils;
using Common.Data;
using UnityEngine;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using Common.Global;
using Common.Utils;
using Common.Base;

namespace GameMain.GlobalManager.SubSystem
{
    public class SmallMapManager : Singleton<SmallMapManager> 
    {
        public SmallMapManager()
        {
            EventDispatcher.AddEventListener<int>(SceneEvents.ENTER_MAP, OnEnterMap);
        }

        private void OnEnterMap(int mapId)
        {
            WorldBossInfo = null;
            if (MiniInfoDict!=null)
            {
                MiniInfoDict.Clear();
                EventDispatcher.TriggerEvent(MainUIEvents.MINI_MAP_GATE_CHANGE);
            }
        }

        private WorldBossInfoData _worldBossInfo;
        public WorldBossInfoData WorldBossInfo
        {
            get
            {
                return _worldBossInfo;
            }
            set
            {
                _worldBossInfo = value;
                EventDispatcher.TriggerEvent(MainUIEvents.WORLD_BOSS_CHANGE);
            }
        }

        public int nextWorldBossTimeStamp;
        public int nextGatTimeStamp;

        private Dictionary<uint,MiniMapInfoData> _miniInfoDict;
        public Dictionary<uint,MiniMapInfoData> MiniInfoDict
        {
            get
            {
                return _miniInfoDict;
            }
        }

        public void OnWorldBossNextTimeResp(UInt32 time)
        {
            nextWorldBossTimeStamp = Convert.ToInt32(time);
            EventDispatcher.TriggerEvent(MainUIEvents.WORLD_BOSS_CHANGE);
        }

        public void OnGateNextTimeResp(UInt32 time)
        {
            nextGatTimeStamp = Convert.ToInt32(time);
            EventDispatcher.TriggerEvent(MainUIEvents.MINI_MAP_GATE_CHANGE);
        }

        public void OnWorldBossPosResp(byte isLifeBoss, UInt32 entityId, UInt32 monsterId, Int16 posx, Int16 posz)
        {
           // Debug.Log("posx:" + posx + "posz:" + posz);
            Vector3 targetPos = new Vector3(posx / 100.0f, 0, posz / 100.0f);
            WorldBossInfoData worldBossInfo = new WorldBossInfoData();
            worldBossInfo.isBossLife = isLifeBoss;
            worldBossInfo.entityId = entityId;
            worldBossInfo.targetPosition = targetPos;
            worldBossInfo.monsterId = monsterId;
            worldBossInfo.name = monster_helper.GetMonsterName((int)monsterId);
            if (isLifeBoss == public_config.WORLD_BOSS_APPEAR)
            {
                WorldBossInfo = worldBossInfo;
            }
            else
            {
                WorldBossInfo = null;
            }
            
        }

        public void OnMiniMapInfoResp(byte type, UInt32 param, UInt32 entityId, byte isLife, Int16 posx, Int16 posz)
        {
            if (isLife == public_config.WORLD_BOSS_APPEAR)
            {
                Vector3 targetPos = new Vector3(posx / 100.0f, 0, posz / 100.0f);
                MiniMapInfoData miniInfo = new MiniMapInfoData();
                miniInfo.id = entityId;
                miniInfo.type = type;
                miniInfo.param = param;
                miniInfo.isLife = isLife;
                miniInfo.targetPosition = targetPos;
                miniInfo.name = transmit_portal_helper.GetPortalName((int)miniInfo.param);
                if (_miniInfoDict == null)
                {
                    _miniInfoDict = new Dictionary<uint, MiniMapInfoData>();
                }
                if (_miniInfoDict.ContainsKey(entityId) == false)
                {
                    _miniInfoDict.Add(entityId, miniInfo);
                }
                else
                {
                    _miniInfoDict[entityId] = miniInfo;
                }
            }
            else
            {
                if (_miniInfoDict != null)
                {
                    _miniInfoDict.Remove(entityId);
                }
            }
            EventDispatcher.TriggerEvent(MainUIEvents.MINI_MAP_GATE_CHANGE);
        }

    }
}
