﻿#region 模块信息
/*==========================================
// 文件名：WingManager
// 命名空间: GameMain.GlobalManager.SubSystem
// 创建者：XYM
// 修改者列表：
// 创建日期：2015/01/28
// 描述说明：翅膀数据管理
// 其他：
//==========================================*/
#endregion

using System;
using GameMain.Entities;

using MogoEngine.Events;
using Common.Events;
using Common.ServerConfig;
using Common.Data;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using Common.Utils;
using Common.Base;

namespace GameMain.GlobalManager.SubSystem
{
    public class WingManager : Singleton<WingManager> 
    {

        private void RPC(action_config actionId, params object[] args)
        {
            PlayerAvatar.Player.ActionRpcCall("wing_action_req", (int)actionId, args);
        }

        public void GetWingData()
        {
            RPC(action_config.ACTION_WING_GET);
        }

        public void UpdateWing(int dbId, WingTrainType type)
        {
            action_config.ACTION_WING_TRAIN.Register();
            RPC(action_config.ACTION_WING_TRAIN, (UInt16)dbId, type == WingTrainType.One ? 1 : 2);
        }

        public void PutOnWing(int dbId)
        {
            RPC(action_config.ACTION_WING_PUT_ON, (UInt16)dbId);
        }

        public void AddWing(int dbId)
        {
            RPC(action_config.ACTION_WING_ADD, (UInt16)dbId);
        }

        public void GetWingRank()
        {
            RPC(action_config.ACTION_WING_FIGHT_FORCE_RANK);
        }

        public void ComposeWing(int wingId)
        {
            RPC(action_config.ACTION_WING_COMPOSE, wingId);
        }

        public void OnWingResp(UInt32 action_id, UInt32 errorId, byte[] byteArr)
        {
            switch ((action_config)action_id)
            {
                case action_config.ACTION_WING_ADD:
                    PbWingBagInfo pbWingItemInfo = MogoProtoUtils.ParseProto<PbWingBagInfo>(byteArr);
                    WingItemData wingItemData = PlayerDataManager.Instance.WingData.GetItemDataById(pbWingItemInfo.wing_id);
                    if (wingItemData.isOwned == true)
                    {
                        PlayerDataManager.Instance.WingData.AddWing(pbWingItemInfo);
                        UIManager.Instance.ShowPanel(PanelIdEnum.WingGet, new int[] { pbWingItemInfo.wing_id, 1 }); //1表示重复获得
                    }
                    else
                    {
                        PlayerDataManager.Instance.WingData.AddWing(pbWingItemInfo);
                        UIManager.Instance.ShowPanel(PanelIdEnum.WingGet, new int[] { pbWingItemInfo.wing_id, 0 }); //0表示新获得翅膀
                        EventDispatcher.TriggerEvent<int>(WingEvents.NEW_WING_CAN_WEAR, pbWingItemInfo.wing_id);
                    }
                    EventDispatcher.TriggerEvent(WingEvents.UPDATE_ITEM_VIEW);
                    break;
                case action_config.ACTION_WING_EXCHANGE:
                    PbWingBagInfo pbWingInfo = MogoProtoUtils.ParseProto<PbWingBagInfo>(byteArr);
                    UIManager.Instance.ShowPanel(PanelIdEnum.WingGet, new int[]{pbWingInfo.wing_id, 1}); // 1表示重复获得
                    break;
                case action_config.ACTION_WING_FIGHT_FORCE_RANK:
                    PbWingRank pbWingRank = MogoProtoUtils.ParseProto<PbWingRank>(byteArr);
                    PlayerDataManager.Instance.WingData.Rank = pbWingRank.ranking;
                    EventDispatcher.TriggerEvent(WingEvents.REFRESH_WING_RANK);
                    break;
            }
        }

        public void WingBagResp(byte[] byteArr)
        {
            PlayerDataManager.Instance.WingData.Fill(MogoProtoUtils.ParseProto<PbWingBagList>(byteArr));
            EventDispatcher.TriggerEvent(WingEvents.UPDATE_ITEM_VIEW);
        }

        public void UpdateWingResp(byte wingId, UInt16 curExp, UInt16 addExp, byte smallCount, byte middleCount, byte largeCount)
        {
            PlayerAvatar.Player.ClearWait((int)action_config.ACTION_WING_TRAIN);
            EventDispatcher.TriggerEvent<int, int, int>(WingEvents.SHOW_TRAIN_RESULT, smallCount, middleCount, largeCount);
            PlayerDataManager.Instance.WingData.UpdateWingExp(wingId, curExp);
            EventDispatcher.TriggerEvent(WingEvents.REFRESH_TRAIN_TOOLTIP);
            EventDispatcher.TriggerEvent(WingEvents.UPDATE_ITEM_VIEW);
        }

        public void PutOnWingResp(byte wingId)
        {
            PlayerDataManager.Instance.WingData.UpdatePutOnData(wingId);
            EventDispatcher.TriggerEvent(WingEvents.UPDATE_ITEM_VIEW);
            EventDispatcher.TriggerEvent<int>(WingEvents.CHANGE_WING_MODEL, wingId);
            EventDispatcher.TriggerEvent(WingEvents.UPDATE_PUT_ON_WING);
        }

        public void AddWingResp(byte wingId)
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.WingGet, wingId);
        }


    }
}
