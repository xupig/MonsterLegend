﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameMain.Entities;

using GameLoader.Utils.CustomType;
using UnityEngine.Events;
using Common.Structs;
using GameData;
using MogoEngine.Events;
using Common.Events;
using Common.ServerConfig;
using GameLoader.Utils;
using Common.Data;
using UnityEngine;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using Common.Global;
using Common.Utils;

namespace GameMain.GlobalManager.SubSystem
{
    public class StarSoulManager : Singleton<StarSoulManager> 
    {
        private static PlayerAvatar _player;

        public StarSoulManager() : base()
        {
            _player = MogoEngine.MogoWorld.Player as PlayerAvatar;
        }

        public void RPC(action_config actionId, params object[] args)
        {
            _player.ActionRpcCall("star_spirit_action_req", (int)actionId, args);
        }

        public void LightStar(int page, int level)
        {
            RPC(action_config.ACTION_STAR_SPIRIT_LIGHT_STAR, (UInt16)page, (UInt16)level);
        }

        public void GetRecord()
        {
            RPC(action_config.ACTION_STAR_SPIRIT_GET_RECORD);
        }

        public void OnStarSpiritResp(UInt32 action_id, UInt32 errorId, byte[] byteArr)
        {
            //Debug.Log("OnStarSpiritResp: " + action_id + "," + errorId + "," + count);
            switch ((action_config)action_id)
            {
                case action_config.ACTION_STAR_SPIRIT_LIGHT_STAR:
                    PbStarSpirit pbStar = MogoProtoUtils.ParseProto<PbStarSpirit>(byteArr);
                    PlayerDataManager.Instance.StarSoulData.AddLightStar(pbStar);
                    EventDispatcher.TriggerEvent(StarSoulEvents.REFRESH_STARSOUL_VIEW);
                    break;
                case action_config.ACTION_STAR_SPIRIT_GET_RECORD:
                    PbStarSpiritCount pbCount = MogoProtoUtils.ParseProto<PbStarSpiritCount>(byteArr);
                    PlayerDataManager.Instance.StarSoulData.Fill((int)pbCount.star_spirit_count);
                    break;
            }
        }

    }
}
