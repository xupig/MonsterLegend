﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/19 13:56:58
 * 描述说明：
 * *******************************************************/


using Common.Base;
using Common.Chat;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using ModuleCommonUI;
using Mogo.Util;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;
namespace GameMain.GlobalManager.SubSystem
{
    public class TeamManager : Singleton<TeamManager> 
    {

       // private static PlayerAvatar _player;

        private static UInt64 inviteDbId = 0;
        private static int inviteType = 0;
        private ulong inviteTime = 0;
        private ulong inviteKeepTime = 2000;
          
        private static UInt64 applyDbId = 0;
       // private static int applyType = 0;
        private ulong applyTime = 0;
        private ulong applyKeepTime = 2000;


        public TeamManager()
        {
           
        }


        #region RPC调用
        private void RPC(action_config actionId,params object[] args)
        {
            MogoWorld.Player.ActionRpcCall("team_action_req", (int)actionId, args);
        }

        public void GetAutoTeamInfo()
        {
            RPC(action_config.ACTION_AUTO_TEAM_GET_PLAYER_MATCH);
            RPC(action_config.ACTION_AUTO_TEAM_GET_PLAYER_TAKE);
        }

        public void CaptainAutoMatch(int gameId,int minLevel,int maxLevel,string content,int isReview)
        {
            RPC(action_config.ACTION_AUTO_TEAM_LEADER_MATCH,gameId,minLevel,maxLevel,content,isReview);
        }

        public void StopAutoMatch()
        {
            RPC(action_config.ACTION_AUTO_TEAM_LEADER_CANCEL_MATCH);
        }

        public void ClearAutoMatch()
        {
            RPC(action_config.ACTION_AUTO_TEAM_LEADER_CLEAR_MATCH);
        }

        public void AutoMatchTeam(int gameId)
        {
            RPC(action_config.ACTION_AUTO_TEAM_PLAYER_MATCH, gameId);
        }

        public void CancelAutoMatchTeam()
        {
            RPC(action_config.ACTION_AUTO_TEAM_PLAYER_CANCEL_MATCH);
        }

        public void TakeTeam(int gameId,int minLevel,int maxLevel)
        {
            RPC(action_config.ACTION_AUTO_TEAM_PLAYER_TAKE, gameId,minLevel,maxLevel);
        }

        public void TeamCall(int gameId,int minLevel,int maxLevel,string content,int isReview)
        {
            RPC(action_config.ACTION_AUTO_TEAM_CALL, gameId, minLevel, maxLevel, content, isReview, public_config.CHANNEL_ID_WORLD);
        }

        public void CancelTakeTeam()
        {
            RPC(action_config.ACTION_AUTO_TEAM_PLAYER_CANCEL_TAKE);
        }

        public void RequestTeamList(int gameId)
        {
            RPC(action_config.ACTION_AUTO_TEAMS,gameId);
        }

        public void CreateTeam()
        {
            RPC(action_config.ACTION_TEAM_CREATE);
        }

        public void JoinTeam(UInt32 teamId)
        {
            RPC(action_config.ACTION_TEAM_JOIN,teamId);
        }

        public void LeaveTeam()
        {
            RPC(action_config.ACTION_TEAM_LEAVE);
        }

        //public void DismissTeam()
        //{
        //    RPC(action_config.ACTION_TEAM_DISMISS);
        //}

        public void KickTeammate(UInt64 dbId)
        {
            RPC(action_config.ACTION_TEAM_KICK,dbId);
        }

        public void GiveRight(UInt64 dbId,int add)
        {
            RPC(action_config.ACTION_TEAM_GIVE_RIGHT,dbId,add);
        }

        public void ChangeCaptain(UInt64 dbId)
        {
            RPC(action_config.ACTION_TEAM_CHANGE_CAPTAIN,dbId);
        }

        public void Invite(UInt64 dbId,int type, int instanceId = instance_helper.InvalidInstanceId)
        {
            if (PlayerDataManager.Instance.TeamData.inviteDict.ContainsKey(dbId) && Global.serverTimeStamp - PlayerDataManager.Instance.TeamData.inviteDict[dbId] < GlobalParams.GetInviteCd())
            {
               //ari ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.OPERATION_CD), Common.Base.PanelIdEnum.MainUIField);
            }
            else
            {
                if (Global.serverTimeStamp - inviteTime < inviteKeepTime)
                {
                   //ari ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.OPERATION_CD), Common.Base.PanelIdEnum.MainUIField);
                }
                else
                {

                    InviteCore(dbId, type, instanceId);
                }
            }            
        }
        
        public void InviteCore(UInt64 dbId,int type, int instanceId)
        {
            inviteTime = Global.serverTimeStamp;
            inviteDbId = dbId;
            inviteType = type;
            RPC(action_config.ACTION_TEAM_INVITE, dbId, instanceId);   
        }

        public void CancelEntry()
        {

        }

        //是否同意,队伍id,邀请人id
        public void AcceptInvite(int accept,UInt32 teamId,UInt64 inviter)
        {
            RPC(action_config.ACTION_TEAM_ACCEPT,accept,teamId,inviter);
        }

        //[是否同意,队伍id,邀请人id，被邀请人id]
        public void CaptainAcceptInvite(int accept,UInt32 teamId,UInt64 inviter,UInt64 invitee, int instanceId)
        {
            RPC(action_config.ACTION_TEAM_LEADER_ACCEPT, accept, teamId, inviter, invitee, instanceId);
        }

        //如果是向在队伍的好友等申请，就是好友等的dbid
	    //如果是向队伍申请，那么就是队长的dbid
        public void Apply(UInt64 dbId)
        {
            if (PlayerDataManager.Instance.TeamData.applyDict.ContainsKey(dbId) && Global.serverTimeStamp - PlayerDataManager.Instance.TeamData.applyDict[dbId] < GlobalParams.GetInviteCd())
            {
               //ari ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, (10134).ToLanguage(), Common.Base.PanelIdEnum.MainUIField);
            }
            else
            {
                if (Global.serverTimeStamp - applyTime < applyKeepTime)
                {
                   //ari ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, (10134).ToLanguage(), Common.Base.PanelIdEnum.MainUIField);
                }
                else
                {

                    ApplyCore(dbId);
                }  
            }
               
        }
      
       public void ApplyCore(UInt64 dbId)
       {
            applyTime = Global.serverTimeStamp; 
            applyDbId = dbId;
//            applyType = type;
            RPC(action_config.ACTION_TEAM_APPLY, dbId);
       }
       
        //[是否同意,队伍id,申请人id]
        public void CaptainAcceptApply(int accept,UInt32 teamId,UInt64 applicant)
        {
             RPC(action_config.ACTION_TEAM_LEADER_APPLY_ACCEPT,accept,teamId,applicant);
        }

        //副本id,是否随机进入
        public void Start(UInt32 fbId,int random)
        {
           RPC(action_config.ACTION_TEAM_START,fbId,random);
        }

        public void Agree(UInt32 reply)
        {
            RPC(action_config.ACTION_TEAM_AGREE, reply);
        }

        public void GetNearPlayerList(int currentPage)
        {
            PlayerDataManager.Instance.TeamData.hasRequestNearPlayerData = true;
            RPC(action_config.ACTION_TEAM_NEAR_PLAYERS, currentPage);
        }

        public void TeamCallOne(UInt64 dbId)
        {
            RPC(action_config.ACTION_TEAM_CALL,dbId);
        }

        public void TeamCallAll()
        {
            RPC(action_config.ACTION_TEAM_CALL_ALL);
        }

        public void ReplyTeamCall(int reply,UInt64 dbid)
        {
            RPC(action_config.ACTION_TEAM_CALL_ACCEPT,reply,dbid);
        }

        public void TeamTransmit(UInt64 dbid)
        {
            RPC(action_config.ACTION_TEAM_SEND,dbid);
        }

        public void TeamTransmitToCaptain()
        {
            TeamTransmit(PlayerDataManager.Instance.TeamData.CaptainId);
        }

        public void FollowCaptain()
        {
            string content = MogoLanguageUtil.GetContent(10133);
      //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, EnsureFollowCaptain);
        }

        private void EnsureFollowCaptain()
        {
            RPC(action_config.ACTION_TEAM_SET_FOLLOW);
        }

        public void CancelFollowCaptain()
        {
            RPC(action_config.ACTION_TEAM_UNSET_FOLLOW);
        }

        #endregion


        #region   服务器调用客户端方法
        private List<int> dataList;

        private int GetAdditionRate(int num)
        {
            if(dataList == null)
            {
                dataList = new List<int>();
                string param = global_params_helper.GetGlobalParam(GlobalParamId.team_addition);
                string[] result = param.Split(new char[] { ',' });
                for(int i=0;i<result.Length;i++)
                {
                    dataList.Add(int.Parse(result[i]));
                }
            }
            return dataList[num - 2];
        }

        public void TeamRefresh(UInt32 teamId,byte teamState,UInt64 captainId,byte[] bytes)
        {
            PbTeamMemberList list = MogoProtoUtils.ParseProto<PbTeamMemberList>(bytes);
            PlayerDataManager.Instance.TeamData.TeamId = teamId;
            PlayerDataManager.Instance.TeamData.State = teamState;
            PlayerDataManager.Instance.TeamData.CaptainId = captainId;
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            teamData.auto_team_content = MogoProtoUtils.ParseByteArrToString(list.auto_team_content_bytes);
            teamData.auto_team_game_id = list.auto_team_game_id;
            teamData.auto_team_is_start = list.auto_team_is_start;
            teamData.auto_team_max_level = list.auto_team_max_level;
            teamData.auto_team_min_level = list.auto_team_min_level;
            teamData.auto_team_time = list.auto_team_time;
            teamData.auto_review = Convert.ToBoolean(list.auto_team_is_approve);
            bool haveNewTeammate = false;
            string newTeammate = string.Empty;

            HashSet<ulong> newDict = new HashSet<ulong>();
            for (int i = 0; i < list.teamMemberList.Count;i++ )
            {
                newDict.Add(list.teamMemberList[i].dbid);
            }
            bool haveRemoveTeammate = false;
            string removeName = string.Empty;
            foreach (KeyValuePair<ulong, PbTeamMember> kvp in teamData.TeammateDic)
            {
                if(newDict.Contains(kvp.Key) == false)
                {
                    haveRemoveTeammate = true;
                    removeName = kvp.Value.name;
                }
            }
            if (haveRemoveTeammate && list.teamMemberList.Count>=2)
            {
                string content = (10129).ToLanguage(removeName, list.teamMemberList.Count, GetAdditionRate(list.teamMemberList.Count));
                ChatManager.Instance.SendClientChannelSystemMsg(public_config.CHANNEL_ID_TEAM, content, (int)ChatContentType.Text);
            }

            if (PlayerDataManager.Instance.TeamData.TeammateDic.Count > 0)
            {
                for (int i = 0; i < list.teamMemberList.Count; i++)
                {
                    if (PlayerDataManager.Instance.TeamData.TeammateDic.ContainsKey(list.teamMemberList[i].dbid) == false)
                    {
                        haveNewTeammate = true;
                        newTeammate = MogoProtoUtils.ParseByteArrToString(list.teamMemberList[i].name_bytes);
                    }
                }
            }

            if (haveNewTeammate && list.teamMemberList.Count >= 2)
            {
                string content = (10128).ToLanguage(newTeammate, list.teamMemberList.Count, GetAdditionRate(list.teamMemberList.Count));
                ChatManager.Instance.SendClientChannelSystemMsg(public_config.CHANNEL_ID_TEAM, content, (int)ChatContentType.Text);
            }
            
            if(haveNewTeammate && PlayerDataManager.Instance.TaskData.ring!=0&&PlayerAvatar.Player.dbid == PlayerDataManager.Instance.TeamData.CaptainId)
            {
                if (PlayerDataManager.Instance.TaskData.teamTaskInfo == null || PlayerDataManager.Instance.TaskData.teamTaskInfo.status != public_config.TASK_STATE_ACCOMPLISH)
                {
              //ari MessageBox.Show(true, string.Empty, (115003).ToLanguage());
                }
            }
            PlayerDataManager.Instance.TeamData.TeammateDic.Clear();
             bool needRefresh = false;
             for (int i = 0; i < list.teamMemberList.Count;i++ )
             {
                 PbTeamMember member = list.teamMemberList[i];
                 member.name = MogoProtoUtils.ParseByteArrToString(member.name_bytes);
                 PlayerDataManager.Instance.TeamData.TeammateDic.Add(member.dbid, member);
                 if (PlayerDataManager.Instance.TeamData.ContainInNearList(member.dbid))
                 {
                     needRefresh = true;
                 }
             }
             EventDispatcher.TriggerEvent(TeamEvent.TEAM_REFRESH);
            if(needRefresh)
            {
                EventDispatcher.TriggerEvent(TeamEvent.TEAM_NEAR_PLAYER_LIST_CHANGE,0);
            }
            PlayerDataManager.Instance.TeamData.ChangePlayerAutoFightType();
        }

        public void TeamRefreshOnline(UInt64 dbid, byte online)
        {
            PlayerDataManager.Instance.TeamData.UpdateMemberOnline(dbid, (uint)online);
            PlayerDataManager.Instance.TeamInstanceData.UpdateMemberOnlineByDbid(dbid, (uint)online);
            EventDispatcher.TriggerEvent<UInt64, int>(TeamEvent.TEAM_MATE_ONLINE_REFRESH, dbid, (int)online);
        }

        public void TeamHpMemberUpdate(UInt64 id, UInt32 hp, UInt32 maxHp)
        {
            //Debug.Log("TeamHpMemberUpdate");
            PlayerDataManager.Instance.TeamData.UpdateMemberHp(id, hp, maxHp);
            EventDispatcher.TriggerEvent(TeamEvent.TEAM_MATE_HP_REFRESH);
        }

        public void TeamPosUpdate(UInt64 id, UInt32 posX, UInt32 posY)
        {
            PlayerDataManager.Instance.TeamData.UpdateMemberPos(id, posX, posY);
        }

        //<Arg> UINT8 </Arg>     <!--原因:1 自己退出 2 被队长踢3掉线超过5分钟后销毁玩家时退出4队伍解散 -->
        public void TeamQuit(byte reason)
        {
            PlayerDataManager.Instance.TeamData.TeamId = 0;
            PlayerDataManager.Instance.TeamData.State = 0;
            PlayerDataManager.Instance.TeamData.CaptainId = 0;
            PlayerDataManager.Instance.TeamData.TeammateDic.Clear();
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            teamData.auto_team_content = string.Empty;
            teamData.auto_team_game_id = 0;
            teamData.auto_team_is_start = 0;
            teamData.auto_team_max_level = 0;
            teamData.auto_team_min_level = 0;
            teamData.auto_team_time = 0;
            teamData.auto_review = false;
            if (PlayerAutoFightManager.GetInstance().fightType == FightType.FOLLOW_FIGHT)
            {
                PlayerAutoFightManager.GetInstance().Stop();
            }
            teamData.inviteDic.Clear();
            teamData.captainInviteDic.Clear();
            teamData.InvitationList.Clear();
            teamData.CaptainList.Clear();
            teamData.applyDic.Clear();
            teamData.NearPlayerList.Clear();
            teamData.nearPlayerTotalPage = 1;
            teamData.nearPlayerCurrentPage = 0; //初始化为0，0表示未初始化，其他表示当前页码
            teamData.NearTeamList.Clear();
            teamData.nearTeamTotalPage = 1;
            teamData.nearTeamCurrentPage = 1;
            teamData.FriendTeamList.Clear();
            teamData.friendTeamTotalPage = 1;
            teamData.friendTeamCurrentPage = 1;
            teamData.GuildTeamList.Clear();
            teamData.guildTeamTotalPage = 1;
            teamData.guildTeamCurrentPage = 1;
            EventDispatcher.TriggerEvent(TeamEvent.TEAM_REFRESH);
            EventDispatcher.TriggerEvent(ChatEvents.QUIT_TEAM);
            EventDispatcher.TriggerEvent(TeamEvent.TEAM_RECEIVED_LIST_CHANGE);
            EventDispatcher.TriggerEvent(TeamEvent.TEAM_QUIT);
        }

        public void TeamReceiveInvite(UInt32 teamId,UInt64 inviterId,string inviterName,byte inviterLevel,Vocation inviterVocation,string captainName,Gender gender, int instanceId)
        {
            TeamReceivedInvitation invitation = PlayerDataManager.Instance.TeamData.GetInvitation(inviterId);
            invitation.teamId = teamId;
            invitation.inviterId = inviterId;
            invitation.inviterName = inviterName;
            invitation.inviterLevel = inviterLevel;
            invitation.inviterVocation = inviterVocation;
            if(captainName == string.Empty)
            {
                captainName = inviterName;
            }
            invitation.captainName = captainName;
            invitation.receivedTime = Global.serverTimeStamp;
            invitation.gender = gender;
            invitation.instanceId = instanceId;

            PlayerDataManager.Instance.TeamData.InvitationList.Insert(0,invitation);
            EventDispatcher.TriggerEvent(TeamEvent.TEAM_RECEIVED_LIST_CHANGE);
            EventDispatcher.TriggerEvent(TeamEvent.TEAM_INVITE_NOTICE);
            PanelIdEnum.TeamInvitePop.Show(invitation);

            //ToolTipsManager.Instance.ShowTip<FloatTips>(MogoLanguageUtil.GetString(LangEnum.TEAM_NEW_INVITE));
        }

        public void TeamLeaderReceiveInvite(UInt32 teamId,UInt64 inviterId,string inviterName,UInt64 inviteeId,string inviteeName,byte inviteeLevel,Vocation inviteeVocation,Gender gender, int instanceId)
        {
            TeamCaptainReceivedInvitation invitation = PlayerDataManager.Instance.TeamData.GetCapationInvitation(inviterId, inviteeId);
            invitation.teamId = teamId;
            invitation.inviterId = inviterId;
            invitation.inviterName = inviterName;
            invitation.inviteeId = inviteeId;
            invitation.inviteeName = inviteeName;
            invitation.inviteeLevel = inviteeLevel;
            invitation.inviteeVocation = inviteeVocation;
            invitation.receivedTime = Global.serverTimeStamp;
            invitation.gender = (Gender)gender;
            invitation.instanceId = instanceId;
            PlayerDataManager.Instance.TeamData.CaptainList.Insert(0,invitation);
            EventDispatcher.TriggerEvent(TeamEvent.TEAM_RECEIVED_LIST_CHANGE);
            EventDispatcher.TriggerEvent(TeamEvent.TEAM_APPLY_NOTICE);
        }

        public void TeamLeaderReceiveApply(UInt32 teamId,UInt64 applicantId,string applicantName,byte applicantLevel,Vocation applicantVocation,Gender gender)
        {
            TeamReceivedApplicant applicant = PlayerDataManager.Instance.TeamData.GetApplication(applicantId);
            applicant.teamId = teamId;
            applicant.applicantId = applicantId;
            applicant.applicantName = applicantName;
            applicant.applicantLevel = applicantLevel;
            applicant.applicantVocation = applicantVocation;
            applicant.receivedTime = Global.serverTimeStamp;
            applicant.gender = (Gender)gender;
            PlayerDataManager.Instance.TeamData.CaptainList.Insert(0, applicant);
            EventDispatcher.TriggerEvent(TeamEvent.TEAM_RECEIVED_LIST_CHANGE);
            EventDispatcher.TriggerEvent(TeamEvent.TEAM_APPLY_NOTICE);
            ////ari ToolTipsManager.Instance.ShowTip<FloatTips>(MogoLanguageUtil.GetString(LangEnum.TEAM_NEW_APPLY));
            string content = PlayerDataManager.Instance.ChatData.CreateTeamCaptainAcceptInviteTemplate(
                new ChatTeamCaptainAcceptInviteWrapper(teamId, applicantId, applicantName, applicantLevel));
            ChatManager.Instance.SendClientChatMsg(public_config.CHANNEL_ID_TEAM, content, (byte)ChatContentType.TeamCaptainAccept);
        }

        public void TeamReceiveAgree(UInt64 dbId, byte state,UInt32 instId)
        {
            if (state != 1)
            {
                string memberName = PlayerDataManager.Instance.TeamData.TeammateDic[dbId].name;
                string copyName = instance_helper.GetInstanceName((int)instId);
                if (dbId == PlayerDataManager.Instance.TeamData.CaptainId)
                {
                    if(dbId!=PlayerAvatar.Player.dbid)
                    {
                        MogoUtils.FloatTips(10123);
                    }
                }
                else
                {
                   //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(1022, copyName, memberName), PanelIdEnum.MainUIField);
                }
                EventDispatcher.TriggerEvent(MainUIEvents.HIDE_TEAM_ENTRY_VIEW);
            }
            EventDispatcher.TriggerEvent<UInt64, byte, UInt32>(TeamEvent.TEAM_MEMBER_REPLY, dbId, state, instId);
        }
        
		public void TeamNearPlayers(Int16 currentPage,Int16 totalPage,byte[] bytes)
        {
           
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            PbNearPlayerList list = MogoProtoUtils.ParseProto<PbNearPlayerList>(bytes);
            for (int i = 0; i < list.nearPlayerList.Count;i++ )
            {
                list.nearPlayerList[i].name = MogoProtoUtils.ParseByteArrToString(list.nearPlayerList[i].name_bytes);
            }
            //if (currentPage == 1)
            //{
            //    teamData.NearPlayerList = list.nearPlayerList;
            //}
            //else
            //{
            //    if (teamData.nearPlayerCurrentPage >= currentPage)
            //    {
            //        teamData.NearPlayerList.RemoveRange((currentPage - 1) * 20, 20);
            //    }
            //    teamData.NearPlayerList.AddRange(list.nearPlayerList);
            //}

            //teamData.nearPlayerTotalPage = totalPage;
            //if (teamData.nearPlayerCurrentPage != currentPage || teamData.nearPlayerCurrentPage == 0)
            //{
            //    teamData.nearPlayerCurrentPage = currentPage;
            //}

            ///取消分页动态更新，
            teamData.NearPlayerList = list.nearPlayerList;
            teamData.nearPlayerCurrentPage = 1;

            EventDispatcher.TriggerEvent(TeamEvent.TEAM_NEAR_PLAYER_LIST_CHANGE, 0);

        }

        public void TeamNearTeams(Int16 currentPage, Int16 totalPage, byte[] bytes)
        {
            PbNearTeamList list = MogoProtoUtils.ParseProto<PbNearTeamList>(bytes);
            PlayerDataManager.Instance.TeamData.nearTeamCurrentPage = currentPage;
            PlayerDataManager.Instance.TeamData.nearTeamTotalPage = totalPage;
            if (PlayerDataManager.Instance.TeamData.NearTeamList.Count>= currentPage*20)
            {
                PlayerDataManager.Instance.TeamData.NearTeamList.RemoveRange((currentPage - 1) * 20, 20);
            }
            else if (PlayerDataManager.Instance.TeamData.NearTeamList.Count > (currentPage - 1) * 20)
            {
                PlayerDataManager.Instance.TeamData.NearTeamList.RemoveRange((currentPage - 1) * 20, PlayerDataManager.Instance.TeamData.NearTeamList.Count - (currentPage - 1) * 20);
            }
            for (int i = 0; i < list.nearTeamList.Count;i++ )
            {
                PbNearTeam team = list.nearTeamList[i];
                team.captain_name = MogoProtoUtils.ParseByteArrToString(team.captain_name_bytes);
                PlayerDataManager.Instance.TeamData.NearTeamList.Insert(Math.Min(PlayerDataManager.Instance.TeamData.NearTeamList.Count, (currentPage - 1) * 20 + i), team);
            }
            
            EventDispatcher.TriggerEvent(TeamEvent.TEAM_NEAR_TEAM_LIST_CHANGE, 0);
        }

        public void TeamFriendTeams(Int16 currentPage, Int16 totalPage, byte[] bytes)
        {
            PbNearTeamList list = MogoProtoUtils.ParseProto<PbNearTeamList>(bytes);
            PlayerDataManager.Instance.TeamData.friendTeamCurrentPage = currentPage;
            PlayerDataManager.Instance.TeamData.friendTeamTotalPage = totalPage;

            if (PlayerDataManager.Instance.TeamData.FriendTeamList.Count >= currentPage * 20)
            {
                PlayerDataManager.Instance.TeamData.FriendTeamList.RemoveRange((currentPage - 1) * 20, 20);
            }
            else if (PlayerDataManager.Instance.TeamData.FriendTeamList.Count > (currentPage - 1) * 20)
            {
                PlayerDataManager.Instance.TeamData.FriendTeamList.RemoveRange((currentPage - 1) * 20, PlayerDataManager.Instance.TeamData.FriendTeamList.Count - (currentPage - 1) * 20);
            }

            for (int i = 0; i < list.nearTeamList.Count; i++)
            {
                PbNearTeam team = list.nearTeamList[i];
                team.captain_name = MogoProtoUtils.ParseByteArrToString(team.captain_name_bytes);
                PlayerDataManager.Instance.TeamData.FriendTeamList.Insert(Math.Min(PlayerDataManager.Instance.TeamData.FriendTeamList.Count, (currentPage - 1) * 20 + i), team);
            }

            EventDispatcher.TriggerEvent(TeamEvent.TEAM_NEAR_TEAM_LIST_CHANGE, 1);
        }

        public void TeamGuildTeams(Int16 currentPage, Int16 totalPage, byte[] bytes)
        {
            PbNearTeamList list = MogoProtoUtils.ParseProto<PbNearTeamList>(bytes);
            PlayerDataManager.Instance.TeamData.guildTeamCurrentPage = currentPage;
            PlayerDataManager.Instance.TeamData.guildTeamTotalPage = totalPage;
            //PlayerDataManager.Instance.TeamData.GuildTeamList.RemoveRange((currentPage - 1) * 20, 20);

            if (PlayerDataManager.Instance.TeamData.GuildTeamList.Count >= currentPage * 20)
            {
                PlayerDataManager.Instance.TeamData.GuildTeamList.RemoveRange((currentPage - 1) * 20, 20);
            }
            else if (PlayerDataManager.Instance.TeamData.GuildTeamList.Count > (currentPage - 1) * 20)
            {
                PlayerDataManager.Instance.TeamData.GuildTeamList.RemoveRange((currentPage - 1) * 20, PlayerDataManager.Instance.TeamData.GuildTeamList.Count - (currentPage - 1) * 20);
            }

            for (int i = 0; i < list.nearTeamList.Count; i++)
            {
                PbNearTeam team = list.nearTeamList[i];
                team.captain_name = MogoProtoUtils.ParseByteArrToString(team.captain_name_bytes);
                PlayerDataManager.Instance.TeamData.GuildTeamList.Insert(Math.Min(PlayerDataManager.Instance.TeamData.GuildTeamList.Count, (currentPage - 1) * 20 + i), team);
            }
            EventDispatcher.TriggerEvent(TeamEvent.TEAM_NEAR_TEAM_LIST_CHANGE, 2);
        }

        public void TeamReceiveStart(UInt32 instId, byte type)
        {
            TeamEntryDataWrapper wrapper = new TeamEntryDataWrapper(type);
            wrapper.instId = instId;
            PanelIdEnum.TeamEntry.Show(wrapper);
        }

        public void TeamReceiveCall(UInt64 dbid)
        {
            /*
            if(PlayerDataManager.Instance.TeamData.TeammateDic.ContainsKey(dbid))
            {
                if(PlayerDataManager.Instance.TaskData.teamTaskInfo!=null)
                {
              ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, (115002).ToLanguage(), delegate()
                    {
                        ReplyTeamCall(1, dbid);
                    },
                       delegate()
                       {
                           ReplyTeamCall(0, dbid);
                       }
                   );
                }
                else
                {
                    PbTeamMember info = PlayerDataManager.Instance.TeamData.TeammateDic[dbid];
                    instance inst = instance_helper.GetInstanceCfg((int)info.map_id);
                    string _content;
                    if(dbid == PlayerDataManager.Instance.TeamData.CaptainId)
                    {
                        _content = MogoLanguageUtil.GetString(LangEnum.TEAM_TEAMMATE_CALL_YOU, info.name, inst.__inst_name.ToLanguage());
                    }
                    else
                    {
                        _content = (10122).ToLanguage(info.name, inst.__inst_name.ToLanguage()); //MogoLanguageUtil.GetString(LangEnum.TEAM_TEAMMATE_CALL_YOU, info.name, inst.__inst_name.ToLanguage());
                    }
                    
              //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, _content, delegate()
                    {
                        ReplyTeamCall(1, info.dbid);
                    },
                        delegate()
                        {
                            ReplyTeamCall(0, info.dbid);
                        }
                    );
                }
               
            }*/
        }
        
        #endregion

        public void OnTeamResp(UInt32 _actionId, UInt32 _errorId, byte[] bytes)
        {
            action_config _result = (action_config)_actionId;
             switch (_result)
             {
                 case action_config.ACTION_AUTO_TEAMS:
                     PbAutoTeamInfoList list = MogoProtoUtils.ParseProto<PbAutoTeamInfoList>(bytes);
                     PlayerDataManager.Instance.TeamData.autoTeamInfoList = list.autoTeamInfoList;
                     EventDispatcher.TriggerEvent(TeamEvent.TEAM_AUTO_TEAM_LIST_REFRESH);
                     break;
                 case action_config.ACTION_AUTO_TEAM_PLAYER_TAKE:
                     PbAutoTeamPlayerTake take = MogoProtoUtils.ParseProto<PbAutoTeamPlayerTake>(bytes);
                     PlayerDataManager.Instance.TeamData.takeInfo = take;
                     EventDispatcher.TriggerEvent(TeamEvent.TEAM_AUTO_TEAM_TAKE_INFO_REFRESH);
                     if(take.game_id!=0)
                     {
                         RequestTeamList(Convert.ToInt32(take.game_id));
                     }
                     break;
                 case action_config.ACTION_AUTO_TEAM_PLAYER_CANCEL_TAKE:
                     PlayerDataManager.Instance.TeamData.takeInfo = null;
                     EventDispatcher.TriggerEvent(TeamEvent.TEAM_AUTO_TEAM_TAKE_INFO_REFRESH);
                     break;
                 case action_config.ACTION_AUTO_TEAM_PLAYER_MATCH:
                     MogoUtils.FloatTips(6016138);
                     PbAutoTeamPlayerMatch match = MogoProtoUtils.ParseProto<PbAutoTeamPlayerMatch>(bytes);
                     if (match.game_id == 0)
                     {
                         PlayerDataManager.Instance.TeamData.matchInfo = null;
                     }
                     else
                     {
                         PlayerDataManager.Instance.TeamData.matchInfo = match;
                     }
                     PlayerDataManager.Instance.TeamData.matchInfo = match;
                     EventDispatcher.TriggerEvent(TeamEvent.TEAM_AUTO_TEAM_MATCH_INFO_REFRESH);
                     break;
                 case action_config.ACTION_AUTO_TEAM_PLAYER_CANCEL_MATCH:
                     MogoUtils.FloatTips(6016139);
                     PlayerDataManager.Instance.TeamData.matchInfo = null;
                     EventDispatcher.TriggerEvent(TeamEvent.TEAM_AUTO_TEAM_MATCH_INFO_REFRESH);
                     break;
                 case action_config.ACTION_AUTO_TEAM_GET_PLAYER_TAKE:
                     PbAutoTeamPlayerTake takeInfo = MogoProtoUtils.ParseProto<PbAutoTeamPlayerTake>(bytes);
                     if (takeInfo.game_id == 0)
                     {
                         PlayerDataManager.Instance.TeamData.takeInfo = null;
                     }
                     else
                     {
                         PlayerDataManager.Instance.TeamData.takeInfo = takeInfo;
                     }
                     EventDispatcher.TriggerEvent(TeamEvent.TEAM_AUTO_TEAM_TAKE_INFO_REFRESH);
                     break;
                 case action_config.ACTION_AUTO_TEAM_GET_PLAYER_MATCH:
                      PbAutoTeamPlayerMatch matchInfo = MogoProtoUtils.ParseProto<PbAutoTeamPlayerMatch>(bytes);
                     if(matchInfo.game_id == 0 )
                     {
                         PlayerDataManager.Instance.TeamData.matchInfo = null;
                     }
                     else
                     {
                         PlayerDataManager.Instance.TeamData.matchInfo = matchInfo;
                     }
                     EventDispatcher.TriggerEvent(TeamEvent.TEAM_AUTO_TEAM_MATCH_INFO_REFRESH);
                     break;
                 case action_config.ACTION_AUTO_TEAM_LEADER_MATCH:
                     MogoUtils.FloatTips(6016138);
                     //EventDispatcher.TriggerEvent(TeamEvent.CAPTAIN_MATCH_INFO_REFRESH);
                     break;
                 case action_config.ACTION_AUTO_TEAM_LEADER_CANCEL_MATCH:
                     MogoUtils.FloatTips(6016139);
                     //EventDispatcher.TriggerEvent(TeamEvent.CAPTAIN_MATCH_INFO_REFRESH);
                     break;
                 case action_config.ACTION_AUTO_TEAM_LEADER_CLEAR_MATCH:
                     //EventDispatcher.TriggerEvent(TeamEvent.CAPTAIN_MATCH_INFO_REFRESH);
                     break;
                 case action_config.ACTION_TEAM_UPDATE_AUTO_TEAM:
                     PbTeamInfoAutoTeam autoTeam = MogoProtoUtils.ParseProto<PbTeamInfoAutoTeam>(bytes);
                     TeamData teamData = PlayerDataManager.Instance.TeamData;
                     teamData.auto_team_content = MogoProtoUtils.ParseByteArrToString(autoTeam.auto_team_content_bytes);
                     teamData.auto_team_game_id = autoTeam.auto_team_game_id;
                     teamData.auto_team_is_start = autoTeam.auto_team_is_start;
                     teamData.auto_team_max_level = autoTeam.auto_team_max_level;
                     teamData.auto_team_min_level = autoTeam.auto_team_min_level;
                     teamData.auto_team_time = autoTeam.auto_team_time;
                     teamData.auto_review = Convert.ToBoolean(autoTeam.auto_team_is_approve);
                     EventDispatcher.TriggerEvent(TeamEvent.CAPTAIN_MATCH_INFO_REFRESH);
                     break;
                 case action_config.ACTION_TEAM_WILD_TASK:
                     /*
                     List<SelectListItemData> selectDataList = new List<SelectListItemData>();
                     if (PlayerDataManager.Instance.TeamData.TeamId == 0)//個人
                     {

                         selectDataList.Add(new SelectListItemData(MogoLanguageUtil.GetContent(6018053), TakeTeam));
                         selectDataList.Add(new SelectListItemData(MogoLanguageUtil.GetContent(6018054), JoinOtherTeam));
                         selectDataList.Add(new SelectListItemData(MogoLanguageUtil.GetContent(6018055), null));
                     }
                     else
                     {
                         selectDataList.Add(new SelectListItemData(MogoLanguageUtil.GetContent(6018053), MatchTeammate));
                         selectDataList.Add(new SelectListItemData(MogoLanguageUtil.GetContent(6018055), null));
                     }*/
                     //ari UIManager.Instance.ShowPanel(PanelIdEnum.SelectList, selectDataList);
                     break;
             }
            
        }

        /// <summary>
        /// 不在隊伍中,要帶隊
        /// </summary>
        private void TakeTeam()
        {
            TeamManager.Instance.TakeTeam(200, 1, team_helper.GetAvatarMaxLevel());
        }

        //在隊伍中，要匹配其他玩家
        private void MatchTeammate()
        {
            TeamManager.Instance.CaptainAutoMatch(200, 1, team_helper.GetAvatarMaxLevel(), string.Empty, 1);
        }

        //不在隊伍中，要進別人的隊伍
        private void JoinOtherTeam()
        {
            TeamManager.Instance.AutoMatchTeam(200);
        }
        
        public void OnTeamResp(int _actionId, int _errorId, params object[] _param)
        {
            OnTeamFollowResp(_actionId, _errorId, _param);
            action_config _result = (action_config)_actionId;
            switch (_result)
            {
                case action_config.ACTION_TEAM_LEADER_APPLY_ACCEPT:
                    EventDispatcher.TriggerEvent(TeamEvent.TEAM_RECEIVED_LIST_CHANGE);
                    break;
                case action_config.ACTION_TEAM_LEADER_ACCEPT:
                    EventDispatcher.TriggerEvent(TeamEvent.TEAM_RECEIVED_LIST_CHANGE);
                    break;
                case action_config.ACTION_TEAM_ACCEPT:
                    EventDispatcher.TriggerEvent(TeamEvent.TEAM_RECEIVED_LIST_CHANGE);
                    break;
                case action_config.ACTION_TEAM_INVITE:
                    if (_errorId == 0)
                    {
                        if (PlayerDataManager.Instance.TeamData.inviteDict.ContainsKey(inviteDbId) == false)
                        {
                            PlayerDataManager.Instance.TeamData.inviteDict.Add(inviteDbId, Global.serverTimeStamp);
                        }
                        else
                        {
                            PlayerDataManager.Instance.TeamData.inviteDict[inviteDbId] = Global.serverTimeStamp;
                        }
                        EventDispatcher.TriggerEvent(TeamEvent.TEAM_INVITE_RESP, inviteDbId, inviteType);
                    }
                    else
                    {
                        EventDispatcher.TriggerEvent(TeamEvent.REQUEST_APPLY_FAIL);
                    }
                    inviteTime = 0;
                    inviteDbId = 0;
                    inviteType = 0;
                    break;
                case action_config.ACTION_TEAM_APPLY:
                    if(_errorId == 0)
                    {
                        if (PlayerDataManager.Instance.TeamData.applyDict.ContainsKey(applyDbId) == false)
                        {
                            PlayerDataManager.Instance.TeamData.applyDict.Add(applyDbId, Global.serverTimeStamp);
                        }
                        else
                        {
                            PlayerDataManager.Instance.TeamData.applyDict[applyDbId] = Global.serverTimeStamp;
                        }
                        EventDispatcher.TriggerEvent(TeamEvent.TEAM_APPLY_RESP, applyDbId);
                    }
                    else
                    {
                        EventDispatcher.TriggerEvent(TeamEvent.REQUEST_APPLY_FAIL);
                    }
                    applyTime = 0;
                    applyDbId = 0;
                    break;
                default:
                    break;
            }
        }

        public void OnTeamFollowResp(int _actionId, int _errorId, params object[] _param)
        {
            if (_errorId != 0) return;
            switch ((action_config)_actionId)
            {
                case action_config.ACTION_TEAM_SET_FOLLOW:
                    PlayerAutoFightManager.GetInstance().Start(FightType.FOLLOW_FIGHT);
                    break;
            }
        }

        public object Dictionary { get; set; }
    }
}
