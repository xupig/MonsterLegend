﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/5/12 19:01:18
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Utils;
using UnityEngine;
using ModuleCommonUI;
using GameLoader.Utils;
using GameData;

namespace GameMain.GlobalManager.SubSystem
{
    public class DreamlandManager : Singleton<DreamlandManager> 
    {
        public const int USE_DIAMOND_QUALITY_UP_PROBABILITY = 1;//用钻石提升 概率提升
        public const int USE_ITEM_QUALITY_UP = 2;//用道具提升 必定提升到某等级
        public const int USE_ITEM_QUALITY_UP_PROBABILITY = 3;//用道具提升 概率提升

        private static PlayerAvatar _player;
        private DreamlandData _dreamlandData;

        public DreamlandManager()
        {
            _player = PlayerAvatar.Player;
            _dreamlandData = PlayerDataManager.Instance.DreamlandData;
        }

        public void RequestChapterList()
        {
            RPC(action_config.ACTION_DREAMLAND_GET_ALL_INFO);
        }

        public void RequestExplore(int landId, int stationId)
        {
            RPC(action_config.ACTION_DREAMLAND_EXPLORE, landId, stationId);
        }

        public void RequestExploreFinish(int landId)
        {
            RPC(action_config.ACTION_DREAMLAND_EXPLORE_FINISH, landId);
        }

        public void RequestRob(int id, UInt64 dbid)
        {
            RPC(action_config.ACTION_DREAMLAND_LOOT, id, dbid);
        }

        public void RequestRevenge(int landId, UInt64 dbid, uint timeStamp)
        {
            RPC(action_config.ACTION_DREAMLAND_REVENGE, landId, dbid, timeStamp);
        }

        public void RequestQualityUp(int id, int qualityUpType, int quality = 0)
        {
            action_config.ACTION_DREAMLAND_PROMOTE.Register();
            RPC(action_config.ACTION_DREAMLAND_PROMOTE, id, qualityUpType, quality);
        }

        public void RequestBuyDragon(int id)
        {
            action_config.ACTION_DREAMLAND_BUY_DRAGON.Register();
            RPC(action_config.ACTION_DREAMLAND_BUY_DRAGON, id);
        }

        public void RequestAwardRetrieve(int id, int getBackType)
        {
            RPC(action_config.ACTION_DREAMLAND_AWARD_RETRIEVE, id, getBackType);
        }

        public void RequestPlayerRefresh(int id)
        {
            action_config.ACTION_DREAMLAND_FRESH.Register();
            RPC(action_config.ACTION_DREAMLAND_FRESH, id);
        }

        public void RequestExploreFinishImmediate(int id)
        {
            action_config.ACTION_DREAMLAND_CONVOY_COMPLETE.Register();
            RPC(action_config.ACTION_DREAMLAND_CONVOY_COMPLETE, id);
        }

        public void RequestExploreTimeMinus(int id)
        {
            action_config.ACTION_DREAMLAND_RATE_FIVE.Register();
            RPC(action_config.ACTION_DREAMLAND_RATE_FIVE, id);
        }

        public void RequestClearRobCooldown(int id)
        {
            action_config.ACTION_DREAMLAND_CLEAR_ATK_CD.Register();
            RPC(action_config.ACTION_DREAMLAND_CLEAR_ATK_CD, id);
        }

        public void RequestBuyRobTimes(int id)
        {
            action_config.ACTION_DREAMLAND_BUY_ATK_TIMES.Register();
            RPC(action_config.ACTION_DREAMLAND_BUY_ATK_TIMES, id);
        }

        public void RequestMissionInfo(int context)
        {
            RPC(action_config.ACTION_DREAMLAND_EVENT_RECORDER, context);
        }

        public void RequestRetrieveRewardInfo()
        {
            RPC(action_config.ACTION_DREAMLAND_BOX_INFO);
        }

        public void RequestPlayerDetailInfo(int id, UInt64 dbid)
        {
            RPC(action_config.ACTION_DREAMLAND_GET_VISIBLE_LIST, id, dbid);
        }

        public void RequestNewMission()
        {
            RPC(action_config.ACTION_DREAMLAND_EVENT_NOTICE);
        }

        public void RequestBeRobedByMeTimes(UInt64 dbid, int id, uint timeStamp)
        {
            RPC(action_config.ACTION_DREAMLAND_GET_ATK_COUNT, dbid, id, timeStamp);
        }

        private void RPC(action_config actionId, params object[] args)
        {
            if (Application.isEditor)
            {
                LoggerHelper.Info("幻境发送协议:" + actionId);
            }
            _player.ActionRpcCall("dreamland_action_req", (int) (actionId), args);
        }

        public void OnDreamlandResponse(action_config actionId, uint errorId, byte[] byteArray)
        {
            if (Application.isEditor)
            {
                LoggerHelper.Info("幻境收到协议:" + actionId);
            }
            //Debug.LogError(string.Format("actionId:{0}", actionId));
            PbDreamlandInfoList chapterList;
            switch (actionId)
            {
            case action_config.ACTION_DREAMLAND_GET_ALL_INFO:
                chapterList = MogoProtoUtils.ParseProto<PbDreamlandInfoList>(byteArray);
                _dreamlandData.ClearChapterDict();
                _dreamlandData.InitConfigChapterDict();
                _dreamlandData.FillChapterList(chapterList);
                EventDispatcher.TriggerEvent(DreamlandEvents.ReceiveDreamlandData);
                break;
            case action_config.ACTION_DREAMLAND_EXPLORE:
                chapterList = MogoProtoUtils.ParseProto<PbDreamlandInfoList>(byteArray);
                _dreamlandData.FillWhenStartExplore(chapterList);
                EventDispatcher.TriggerEvent(DreamlandEvents.ReceiveStartExplore);
                break;
            case action_config.ACTION_DREAMLAND_EXPLORE_FINISH:
                chapterList = MogoProtoUtils.ParseProto<PbDreamlandInfoList>(byteArray);
                _dreamlandData.FillWhenFinishExplore(chapterList);
                EventDispatcher.TriggerEvent(DreamlandEvents.ReceiveFinishExplore);
                break;
            case action_config.ACTION_DREAMLAND_CONVOY_COMPLETE:
                chapterList = MogoProtoUtils.ParseProto<PbDreamlandInfoList>(byteArray);
                _dreamlandData.FillWhenFinishImmediate(chapterList);
                EventDispatcher.TriggerEvent(DreamlandEvents.RefreshChapter);
                break;
            case action_config.ACTION_DREAMLAND_RATE_FIVE:
                chapterList = MogoProtoUtils.ParseProto<PbDreamlandInfoList>(byteArray);
                _dreamlandData.FillWhenMinusTime(chapterList);
                EventDispatcher.TriggerEvent(DreamlandEvents.RefreshChapter);
                break;
            case action_config.ACTION_DREAMLAND_BUY_DRAGON:
                chapterList = MogoProtoUtils.ParseProto<PbDreamlandInfoList>(byteArray);
                _dreamlandData.FillWhenDragonQualityUp(chapterList);
                EventDispatcher.TriggerEvent(DreamlandEvents.ReceiveQualityUp);
                _dreamlandData.ClearQualityUp();
                break;
            case action_config.ACTION_DREAMLAND_EVENT_RECORDER:
                PbDreamlandEventList eventList = MogoProtoUtils.ParseProto<PbDreamlandEventList>(byteArray);
                EventDispatcher.TriggerEvent(DreamlandEvents.ReceiveMissionRecord, eventList);
                _dreamlandData.HaveNewRecord = false;
                EventDispatcher.TriggerEvent(DreamlandEvents.NewRecordChanged);
                break;
            case action_config.ACTION_DREAMLAND_PROMOTE:
                chapterList = MogoProtoUtils.ParseProto<PbDreamlandInfoList>(byteArray);
                _dreamlandData.FillWhenDragonQualityUp(chapterList);
                EventDispatcher.TriggerEvent(DreamlandEvents.ReceiveQualityUp);
                _dreamlandData.ClearQualityUp();
                break;
            case action_config.ACTION_DREAMLAND_BOX_INFO:
                PbDreamlandBoxInfoList boxInfoList = MogoProtoUtils.ParseProto<PbDreamlandBoxInfoList>(byteArray);
                EventDispatcher.TriggerEvent(DreamlandEvents.ReceiveDreamlandBoxInfo, boxInfoList);
                break;
            case action_config.ACTION_DREAMLAND_AWARD_RETRIEVE:
                PbDreamlandItemList itemList = MogoProtoUtils.ParseProto<PbDreamlandItemList>(byteArray);
                _dreamlandData.ReceiveRetrieveReward();
                EventDispatcher.TriggerEvent<PbDreamlandItemList>(DreamlandEvents.ReceiveGetYestodayReward, itemList);
                break;
            case action_config.ACTION_DREAMLAND_FRESH:
                chapterList = MogoProtoUtils.ParseProto<PbDreamlandInfoList>(byteArray);
                _dreamlandData.FillWhenRefreshPlayerList(chapterList);
                //ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(37830), PanelIdEnum.Dreamland);
                EventDispatcher.TriggerEvent(DreamlandEvents.RefreshChapter);
                break;
            case action_config.ACTION_DREAMLAND_BUY_ATK_TIMES:
                chapterList = MogoProtoUtils.ParseProto<PbDreamlandInfoList>(byteArray);
                _dreamlandData.FillWhenBuyRobTimes(chapterList);
                EventDispatcher.TriggerEvent(DreamlandEvents.RefreshChapter);
                break;
            case action_config.ACTION_DREAMLAND_GET_VISIBLE_LIST:
                PbVisibleInfo pbVisibleInfo = MogoProtoUtils.ParseProto<PbVisibleInfo>(byteArray);
                DreamlandPlayerInfo playerInfo = new DreamlandPlayerInfo();
                playerInfo.FillDetailInfo(pbVisibleInfo);
                EventDispatcher.TriggerEvent<DreamlandPlayerInfo>(DreamlandEvents.ReceivePlayerInfo, playerInfo);
                break;
            case action_config.ACTION_DREAMLAND_LOOT:
            case action_config.ACTION_DREAMLAND_REVENGE:
                PbDreamlandBattleCallBack pbDreamlandBattleCallBack = MogoProtoUtils.ParseProto<PbDreamlandBattleCallBack>(byteArray);
                CopyLogicManager.Instance.ProcessResult(ChapterType.Single, pbDreamlandBattleCallBack);
                break;
            case action_config.ACTION_DREAMLAND_CLEAR_ATK_CD:
                chapterList = MogoProtoUtils.ParseProto<PbDreamlandInfoList>(byteArray);
                _dreamlandData.FillWhenClearRobCooldown(chapterList);
                EventDispatcher.TriggerEvent(DreamlandEvents.RefreshChapter);
                break;
            case action_config.ACTION_DREAMLAND_EVENT_NOTICE:
                _dreamlandData.HaveNewRecord = true;
                EventDispatcher.TriggerEvent(DreamlandEvents.NewRecordChanged);
                break;
            case action_config.ACTION_DREAMLAND_GET_ATK_COUNT:
                PbDreamlandAtkInfo atkInfo = MogoProtoUtils.ParseProto<PbDreamlandAtkInfo>(byteArray);
                EventDispatcher.TriggerEvent<PbDreamlandAtkInfo>(DreamlandEvents.ReceiveBeRobedByMeTimes, atkInfo);
                break;
            }
        }
    }
}
