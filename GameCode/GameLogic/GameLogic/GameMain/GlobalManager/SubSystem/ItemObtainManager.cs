﻿using Common.Base;
using Common.Data;
using Common.Global;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager
{
    public enum GetWayType
    {
        None = 0,
        FloatTips = 1,
        ItemObtain = 2,
        GetPet = 3,
        GetRune = 4,
        Treasure = 5,
    }

    public class ItemObtainManager : Singleton<ItemObtainManager>
    {
        public void Show(PbRewardList rewardList)
        {
            if (rewardList.item_list.Count > 0)
            {
                ShowItemObtain(rewardList);
            }
        }

        private static bool HasHighQualityDrops(PbRewardList rewardList)
        {
            if (rewardList.get_way == 11 || rewardList.get_way == 12)
            {
                for (int i = 0; i < rewardList.item_list.Count; i++)
                {
                    if (item_helper.GetQuality(Convert.ToInt32(rewardList.item_list[i].id)) >= 4)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private static void ShowItemObtain(PbRewardList rewardList)
        {
            if (channel_helper.HasConfig(rewardList.get_way) == false)
            {
                Debug.LogError(string.Format("缺少ID为{0}的物品获得渠道", rewardList.get_way));
                return;
            }
            bool haveHighQuailityItem = HasHighQualityDrops(rewardList);
            if (haveHighQuailityItem)
            {
                ShowItemObtainPanel(rewardList);
                return;
            }
            
            switch ((GetWayType)channel_helper.GetChannelType(rewardList.get_way))
            {
                case GetWayType.None:
                    break;
                case GetWayType.FloatTips:
                    ShowFloatTips(rewardList);
                    break;
                case GetWayType.ItemObtain:
                    ShowItemObtainPanel(rewardList);
                    break;
                case GetWayType.GetPet:
                    ShowPet(rewardList);
                    break;
                case GetWayType.GetRune:
                    ShowItemObtainPanel(rewardList);
                    break;
                case GetWayType.Treasure:
                    ShowTreasureReward(rewardList);
                    break;
            }
        }

        private static void ShowTreasureReward(PbRewardList rewardList)
        {
            if (rewardList.reward_id == 0) return;
            UIManager.Instance.ShowPanel(PanelIdEnum.TreasureReward, rewardList);
        }

        private static void ShowPet(PbRewardList rewardList)
        {

        }

        private static void ShowFloatTips(PbRewardList rewardList)
        {
            for (int i = 0; i < rewardList.item_list.Count; i++)
            {
                PbItemNoticeItem itemInfo = rewardList.item_list[i];
                int id = (int)itemInfo.id;
                string name = ColorDefine.GetColorHtmlString(item_helper.GetQuality(id), item_helper.GetName(id));
                string content = string.Format(channel_helper.GetChannelDesc(rewardList.get_way), name, itemInfo.count);
                if (content != string.Empty)
                {
                   //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, content, PanelIdEnum.MainUIField);
                }
                else
                {
                    Debug.LogError(string.Format("缺少ID为{0}的物品获得渠道", rewardList.get_way));
                }
            }
        }

        private static void ShowItemObtainPanel(PbRewardList rewardList)
        {
            //星魂系统获得宠物的时候不弹出奖励界面
            if (rewardList.get_way == 40 && rewardList.item_list.Count > 0 && item_helper.GetItemType((int)rewardList.item_list[0].id) == BagItemType.PetCard)
            {
                return;
            }
            UIManager.Instance.ShowPanel(PanelIdEnum.ItemObtain, rewardList);
        }

    }
}
