﻿using Common.Base;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using MogoEngine;
using GameLoader.Utils;
using Common;
using GameLoader.Config;
using GameLoader.PlatformSdk;
using System.Net;
using Common.Data;

namespace GameMain.GlobalManager.SubSystem
{
    public class ChargeManager : Singleton<ChargeManager>
    {
        static public bool IgnoreClientChargeCheck = false;
        static public int ChargeId; //-1表示没有正在处理的订单, 0 表示自定义金额充值, 其他表示正在处理的订单号
        private List<charge_info> _chargeInfoList = new List<charge_info>();
        private charge_info _currentHandleChargeInfo = null;

        public void RequestChargeInfoList()
        {
            PlayerAvatar.Player.RpcCall("charge_req", (UInt16)action_config.ACTION_CHARGE_INFO_REQ, 0, 0, 0);
            //action_config.ACTION_CHARGE_INFO_REQ.Rpc("charge_req",0,0,0);
        }

        public void CheckChargeInfo()
        {
            PlayerAvatar.Player.RpcCall("charge_req", (UInt16)action_config.ACTION_CHARGE_CHECK_REQ, 0, 0, 0);
        }

        public void WithdrawCharge(ulong order_id)
        {
            PlayerAvatar.Player.RpcCall("charge_req", (UInt16)action_config.ACTION_CHARGE_WITHDRAW_REQ, order_id, 0, 0);
        }

        //代充用于判断是否符合相应的条件
        public void CheckChargeCondition(int chargeId)
        {
            ChargeId = chargeId;
            PlayerAvatar.Player.RpcCall("charge_req", (UInt16)action_config.ACTION_CHARGE_CONDITION_CHECK, chargeId, 0, 0);
            action_config.ACTION_CHARGE_CONDITION_CHECK.Register();
        }

        public void OnChargeResp(UInt32 action_id, UInt32 error_id, byte[] bytes)
        {
            switch ((action_config)action_id)
            {
                case action_config.ACTION_CHARGE_INFO_REQ:
                    charge_list chargeList = MogoProtoUtils.ParseProto<charge_list>(bytes);
                    EventDispatcher.TriggerEvent<charge_list>(ChargeEvents.RefreshChargeInfoList, chargeList);
                    break;
                case action_config.ACTION_CHARGE_CHECK_REQ:
                    charge_info chargeInfo = MogoProtoUtils.ParseProto<charge_info>(bytes);
                    _chargeInfoList.Add(chargeInfo);
                    CheckNextOrder();
                    break;
                case action_config.ACTION_CHARGE_WITHDRAW_REQ:
                    charge_info withdrawChargeInfo = MogoProtoUtils.ParseProto<charge_info>(bytes);
                    _currentHandleChargeInfo = withdrawChargeInfo;
                    //ari//ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(111010, withdrawChargeInfo.charge_diamond), PanelIdEnum.MainUIField);
                    if (Application.platform == RuntimePlatform.IPhonePlayer)
                    {
                        ConfirmGetExtraDiamond();
                    }
                    else if (withdrawChargeInfo.extra_diamond > 0)
                    {
                        string contents = MogoLanguageUtil.GetContent(111011, withdrawChargeInfo.charge_diamond, withdrawChargeInfo.extra_diamond);
                        //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, contents, ConfirmGetExtraDiamond, null, true, false, true);
                    }
                    else
                    {
                        ConfirmGetExtraDiamond();
                    }
                    break;
                case action_config.ACTION_CHARGE_CONDITION_CHECK:
                    if (error_id == 0)
                    {
                        EventDispatcher.TriggerEvent(ChargeEvents.CheckChargeConditionOK);
                    }
                    else
                    {
                        SystemInfoManager.Instance.Show((int)error_id, bytes);
                    }
                    break;

            }
        }

        private void ConfirmWithdrawDiamond()
        {
            if (_currentHandleChargeInfo != null)
            {
                if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    ConfirmAgainWithdrawDiamond();
                }
                else
                {
                    string vocationName = XMLManager.chinese[(int)PlayerAvatar.Player.vocation].__content;
                    string content = MogoLanguageUtil.GetContent(111009, _currentHandleChargeInfo.charge_diamond, PlayerAvatar.Player.level, vocationName);
              //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, ConfirmAgainWithdrawDiamond, OnCancel, MogoLanguageUtil.GetContent(111012), MogoLanguageUtil.GetContent(111013), null, true, false, true);
                }
            }
        }

        private void ConfirmAgainWithdrawDiamond()
        {
            ChargeManager.Instance.WithdrawCharge(_currentHandleChargeInfo.order_id);
        }

        private void ConfirmGetExtraDiamond()
        {
            Remove(_currentHandleChargeInfo.order_id);
            _currentHandleChargeInfo = null;
            CheckNextOrder();
        }

        private void OnCancel()
        {
            Remove(_currentHandleChargeInfo.order_id);
            _currentHandleChargeInfo = null;
            CheckNextOrder();
        }

        private void CheckNextOrder()
        {
            if (_currentHandleChargeInfo == null && _chargeInfoList.Count > 0)
            {
                _currentHandleChargeInfo = _chargeInfoList[0];
                if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    ConfirmWithdrawDiamond();
                }
                else
                {
                    string content = MogoLanguageUtil.GetContent(111008, _currentHandleChargeInfo.charge_diamond);
              //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, ConfirmWithdrawDiamond, OnCancel, MogoLanguageUtil.GetContent(111012), MogoLanguageUtil.GetContent(111013), null, true, false, true);
                }
            }
        }

        private void Remove(ulong orderId)
        {
            int removeIndex = -1;
            for (int i = 0; i < _chargeInfoList.Count; i++)
            {
                if (_chargeInfoList[i].order_id == orderId)
                {
                    removeIndex = i;
                }
            }
            if (removeIndex != -1)
            {
                _chargeInfoList.RemoveAt(removeIndex);
            }
        }

        /// <summary>
        /// 进行充值
        /// </summary>
        /// <param name="baseData">充值商品基础信息</param>
        /// <param name="friendDbid">代充好友的dbid(当friendDbid>0:给好友充值；friendDbid<=0:给自己充值)</param>
        public void JumpToCharge(charge_item baseData, ulong friendDbid = 0)
        {
            //A-1、构造回调数据
            //代充数据结构 string.Concat(productId, "_", getServerId(friendDbid), "_", getSortId(friendDbid));
            string productId = baseData.charge_id.ToString();
            string callbackInfo = friendDbid > 0 ? string.Concat(productId, "_", getServerId(friendDbid), "_", getSortId(friendDbid)) : productId;                                                                    
            PlayerAvatar me = MogoWorld.Player as PlayerAvatar;
            
            //A-2、到AppStore充值
            #if UNITY_IPHONE && !UNITY_EDITOR
               //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(111028), PanelIdEnum.MainUIField);
                LoggerHelper.Info(string.Format("uid:{0},dbid={1},nickName={2},serverId={3},productId={4},iosProductId={5},callbackInfo={6}", LoginInfo.platformUid, me.dbid, me.name, LocalSetting.settingData.SelectedServerID, baseData.charge_id, baseData.ios_product, callbackInfo));
                IOSPlugins.BuyDiamond(LoginInfo.platformUid,
                                        LocalSetting.settingData.SelectedServerID.ToString(), 
                                        me.dbid.ToString(), 
                                        me.level.ToString(), 
                                        me.money_diamond.ToString(),
                                        baseData.ios_product,
                                        "1",
                                        me.name,
                                        6,
                                        callbackInfo,
                                        (int)baseData.diamond);        
             #endif

            //A-3、到蜂鸟SDK充值
            if (Application.platform == RuntimePlatform.Android)
            {
				if (PlatformSdkMgr.Instance.PlatformSdkMgrType == PlatformSdkType.AndroidFn)
                {
                    //string productId = baseData.charge_id.ToString();                      //产品ID，必填有些平台用到。不同的商品要有不同的id,id从1开始
                    int productCount = 1;                                                    //商品数量。会和productName一起显示给用户看，不参与price计算。如显示：100元宝
                    string price = (baseData.money * productCount).ToString();               //实际充值金额，单位：元。不管productCount和rate是多少，这个值填多少就充多少元 
                    string coinName = XMLManager.chinese[15].__content;                      //币种(钻石)
                    string productName = coinName;                                           //产品名称
                    string productDesc = string.Concat(price, coinName);                     //产品描述
                    string serverId = LocalSetting.settingData.SelectedServerID.ToString();  //服务器ID
                    PlatformSdkMgr.Instance.Pay(me.dbid.ToString(), me.name, me.level.ToString(), serverId, price, coinName, productId, productName, productDesc, productCount.ToString(), callbackInfo);
				}
                else if (PlatformSdkMgr.Instance.PlatformSdkMgrType == PlatformSdkType.AndroidKorea)
                {
                    #if UNITY_ANDROID
                    string price = baseData.money.ToString(); //实际充值金额，单位：元
                    string serverId = LocalSetting.settingData.SelectedServerID.ToString();  //服务器ID
                    callbackInfo = friendDbid > 0 ? friendDbid.ToString() : me.dbid.ToString();
                    AndroidKoreaSdkMgr koreaSdkMgr = PlatformSdkMgr.Instance as AndroidKoreaSdkMgr;
                    if (koreaSdkMgr != null)
                    {
                        koreaSdkMgr.PayByGooglePlay(productId, price, serverId, callbackInfo);
                    }
                    #endif
                }
            }

            if (Application.platform == RuntimePlatform.WindowsPlayer)
            {
                WindowsPlatformLoginData winloginData = LoginManager.Instance.GetWindowsPlatformLoginData();
                if (winloginData.fnpid == "2")
                {
              //ari MessageBox.Show(true, "", "请移步到手机端充值，谢谢！");
                    return;
                }
                string game = "qjphs";
                string server = "S" + LocalSetting.settingData.SelectedServerID.ToString();
                //string uid = Uri.EscapeDataString(PlayerAvatar.Player.dbid.ToString());
                string uid = winloginData.username;
                string gamemoney = (baseData.money).ToString();
                string device_type = winloginData.device_type;
                //string call_back = productId + "_" + 0 + "_" + 0;
                string url = "http://pay.4399.com/?gamename=" + game + "&" + "gameserver" + "=" + server + "&" + "uid" + "=" + uid + "&" + "gamemoney" + "=" + gamemoney + "&" + "call_back" + "=" + callbackInfo + "&" + "device_type" + "=" + device_type;
                //string url = "http://access.my4399.com/?gamename=" + game + "&" + "gameserver" + "=" + server + "&" + "uid" + "=" + uid + "&" + "call_back" + "=" + "1";
                LoggerHelper.Info("charge:" + url);
                Application.OpenURL(url);
                /*HttpWebRequest req = null;
                req = (HttpWebRequest)WebRequest.Create(url);
                req.Proxy = null;
                //HttpUtils.Get(url, OnTest, OnCharge);
                /*int productCount = 1;                                                    //商品数量。会和productName一起显示给用户看，不参与price计算。如显示：100元宝
                int price = (int)(_data.money * productCount);                  //实际充值金额，单位：元。不管productCount和rate是多少，这个值填多少就充多少元
                PlatfromManager.GetInstance().SendOpenPayLog(price);*/
            }

            if(Application.platform == RuntimePlatform.WindowsWebPlayer)
            {
                WindowsWebPlatformLoginData winloginData = WebLoginInfoManager.GetInstance().GetWebPlatformLoginData();
                if (winloginData.fnpid == "2")
                {
              //ari MessageBox.Show(true, "", "请移步到手机端充值，谢谢！");
                    return;
                }
                string game = "qjphs";
                string server = "S" + LocalSetting.settingData.SelectedServerID.ToString();
                //string uid = Uri.EscapeDataString(PlayerAvatar.Player.dbid.ToString());
                string uid = winloginData.username;
                string gamemoney = (baseData.money).ToString();
                string device_type = winloginData.device_type;
                string url = "http://pay.4399.com/?gamename=" + game + "&" + "gameserver" + "=" + server + "&" + "uid" + "=" + uid + "&" + "gamemoney" + "=" + gamemoney + "&" + "call_back" + "=" + callbackInfo + "&" + "device_type" + "=" + device_type;
                //string url = "http://access.my4399.com/?gamename=" + game + "&" + "gameserver" + "=" + server + "&" + "uid" + "=" + uid + "&" + "call_back" + "=" + "1";
                LoggerHelper.Info("charge:" + url);
                //string js = "window.open(" + url +"," + "\"_blank\""+ ")";
                Application.ExternalCall("showPay", url);
                //Application.OpenURL(url);
            }
        }

        private void OnTest(string result)
        {

        }

        private void OnCharge(HttpStatusCode errorCode)
        {

        }

        /// <summary>
        /// 根据dbid获取玩家所在服务器ID
        /// </summary>
        /// <param name="dbid"></param>
        /// <returns></returns>
        private int getServerId(ulong dbid)
        {
            return (int)(dbid >> 32);
        }

        /// <summary>
        /// 根据dbid获取玩家自增序号
        /// </summary>
        /// <param name="dbid"></param>
        /// <returns></returns>
        private int getSortId(ulong dbid)
        {
            return (int)(dbid & 0xFFFFFFFF);
        }

    }
}
