﻿using Common.Data;
using Common.Events;
using Common.Global;
using GameData;
using MogoEngine;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class InteractivePointManager
    {
        private bool _isHaveNewFriendInfo = false;
        private bool _isHaveNewTeamInfo = false;
        private bool _isHaveNewMailInfo = false;

        public InteractivePointManager()
        {
            EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_RECV_LIST, RefreshFriendPoint);
            EventDispatcher.AddEventListener(FriendEvents.REFRESH_FRIEND_RECEIVED_LIST, RefreshFriendPoint);
            EventDispatcher.AddEventListener(FriendEvents.FRIEND_APPLICATION_CHANGE, RefreshFriendPoint);
            EventDispatcher.AddEventListener<ulong>(FriendEvents.FRIEND_BLESS_STATE_CHANGE, RefreshBlessedPoint);

            EventDispatcher.AddEventListener(TeamEvent.TEAM_RECEIVED_LIST_CHANGE, RefreshTeamPoint);

            EventDispatcher.AddEventListener(MailEvents.MAIL_DATA_CHANGE, RefreshMailPoint);
        }

        private void RefreshFriendPoint()
        {
            int blessingLimit = GlobalParams.GetFriendBlessingLimit();
            int blessedLimit = GlobalParams.GetFriendBlessedLimit();
            if ((PlayerDataManager.Instance.FriendData.BlessedNum > 0 && PlayerDataManager.Instance.FriendData.ReceivedNum < blessedLimit && PlayerDataManager.Instance.FriendData.BlessingNum < blessingLimit)
            || PlayerDataManager.Instance.FriendData.FriendApplicationList.Count > 0)
            {
                _isHaveNewFriendInfo = true;
            }
            else
            {
                _isHaveNewFriendInfo = false;
            }

            RefreshEntryItem();
            EventDispatcher.TriggerEvent(FriendEvents.FRIEND_POINT_CHANGE);
        }

        private void RefreshBlessedPoint(ulong dbid)
        {
            RefreshFriendPoint();
        }

        private void RefreshTeamPoint()
        {
            TeamData teamData = PlayerDataManager.Instance.TeamData;
            List<TeamReceivedData> dataList = new List<TeamReceivedData>();
            if (teamData.CaptainId == 0)   //没有队伍
            {
                foreach (TeamReceivedData data in teamData.InvitationList)
                {
                    dataList.Add(data);
                }
            }
            else if (teamData.CaptainId == MogoWorld.Player.dbid)  //自己是队长
            {
                foreach (TeamReceivedData data in teamData.CaptainList)
                {
                    dataList.Add(data);
                }
            }
            _isHaveNewTeamInfo = (dataList.Count != 0);
            RefreshEntryItem();
            EventDispatcher.TriggerEvent(TeamEvent.TEAM_POINT_REFRESH);
        }

        private void RefreshMailPoint()
        {
            _isHaveNewMailInfo = (PlayerDataManager.Instance.MailData.NewMailCount != 0);
            RefreshEntryItem();
            EventDispatcher.TriggerEvent(MailEvents.MAIL_POINT_REFRESH);
        }

        private void RefreshEntryItem()
        {
            if (_isHaveNewFriendInfo == true || _isHaveNewTeamInfo == true || _isHaveNewMailInfo == true)
            {
                PlayerDataManager.Instance.FunctionData.AddFunctionPoint(FunctionId.interactive);
            }
            else
            {
                PlayerDataManager.Instance.FunctionData.RemoveFunctionPoint(FunctionId.interactive);
            }
        }

        public bool IsHaveNewFriendInfo
        {
            get
            {
                return _isHaveNewFriendInfo;
            }
        }

        public bool IsHaveNewTeamInfo
        {
            get
            {
                return _isHaveNewTeamInfo;
            }
        }

        public bool IsHaveNewMailInfo
        {
            get
            {
                return _isHaveNewMailInfo;
            }
        }
    }
}
