﻿using System;
using GameMain.Entities;

using MogoEngine.Events;
using Common.Events;
using Common.ServerConfig;
using Common.Data;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using Common.Utils;
using Common.Base;
using UnityEngine;
using GameData;

namespace GameMain.GlobalManager.SubSystem
{
    public class GuildTaskManager : Singleton<GuildTaskManager> 
    {
        public bool canReward = false;

        private void RPC(action_config actionId, params object[] args)
        {
            //Debug.LogError("rpc: " + actionId);
            PlayerAvatar.Player.ActionRpcCall("guild_action_req", (int)actionId, args);
        }

        public void GetMyGuildScore()
        {
            RPC(action_config.GUILD_QUERY_TASK_REWARD);
        }

        public void GetGuildScoreReward(int id)
        {
            RPC(action_config.GUILD_GET_TASK_REWARD, id);
        }

        public void AcceptGuildTask(int taskId)
        {
            RPC(action_config.GUILD_ACCEPT_TASK, taskId);
        }

        public void AbandonGuildTask(int taskId)
        {
            RPC(action_config.GUILD_ABANDON_TASK, taskId);
        }

        public void GetGuildTaskInfo()
        {
            RPC(action_config.GUILD_QUERY_TASK);
        }

        public void GetGuildTaskRank(int start, int count)
        {
            RPC(action_config.GUILD_TASK_RANK_REQ, start, count);
        }

        public void OnGuildResp(uint action_id, uint errorId, byte[] byteArr)
        {
            //Debug.LogError("resp: " + (action_config)action_id);
            switch ((action_config)action_id)
            {
                case action_config.GUILD_ACCEPT_TASK:
                    OnAcceptTask();
                    break;
                case action_config.GUILD_ABANDON_TASK:
                    OnAbandonTask();
                    break;
                case action_config.GUILD_QUERY_TASK:
                    OnGetGuildTaskInfoResp(byteArr);
                    break;
                case action_config.GUILD_QUERY_TASK_REWARD:
                    OnGetGuildTaskScore(byteArr);
                    break;
                case action_config.GUILD_GET_TASK_REWARD:
                    OnGetTaskReward(byteArr);
                    break;
                case action_config.GUILD_TASK_RANK_REQ:
                    OnGetGuildTaskRank(byteArr);
                    break;
            }
        }

        private void OnGetGuildTaskRank(byte[] byteArr)
        {
            PbGuildTaskPlayerScoreInfoList dataList = MogoProtoUtils.ParseProto<PbGuildTaskPlayerScoreInfoList>(byteArr);
            EventDispatcher.TriggerEvent<PbGuildTaskPlayerScoreInfoList>(GuildTaskEvents.ON_GET_GUILD_TASK_RANK, dataList);
        }

        private void OnGetTaskReward(byte[] byteArr)
        {
            GetMyGuildScore();
        }

        private void OnAbandonTask()
        {
            GetGuildTaskInfo();
        }

        private void OnAcceptTask()
        {
            GetGuildTaskInfo();
        }

        private void OnGetGuildTaskScore(byte[] byteArr)
        {
            PbGuildTaskScoreInfo scoreInfo = MogoProtoUtils.ParseProto<PbGuildTaskScoreInfo>(byteArr);
            PlayerDataManager.Instance.TaskData.GuildTaskInfo = scoreInfo;
            CheckGreenPoint();
            EventDispatcher.TriggerEvent(GuildTaskEvents.ON_GET_GUILD_TASK_SCORE);
        }

        private void OnGetGuildTaskInfoResp(byte[] byteArr)
        {
            PbGuildTaskInfoList taskInfoList = MogoProtoUtils.ParseProto<PbGuildTaskInfoList>(byteArr);
            EventDispatcher.TriggerEvent<PbGuildTaskInfoList>(GuildTaskEvents.ON_GET_GUILD_TASK_LIST, taskInfoList);
        }


        private void CheckGreenPoint()
        {
            canReward = guild_score_reward_helper.CanReward();
        }
    }
}
