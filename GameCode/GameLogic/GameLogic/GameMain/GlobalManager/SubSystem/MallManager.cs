﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/3/14 17:58:47
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameData;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Utils;
using UnityEngine;
using Common.Utils;
using ModuleCommonUI;
using Common.Base;

namespace GameMain.GlobalManager.SubSystem
{
    public class MallManager : Singleton<MallManager> 
    {
        private const int NO_USE = 0;
        private const int MY_DBID = 0;

        private static PlayerAvatar _player;
        private MallDataManager _mallDataManager;

        public MallManager()
        {
            _player = PlayerAvatar.Player;
            _mallDataManager = PlayerDataManager.Instance.MallDataManager;
        }

        private enum LimitType
        {
            dailyLimit,
            totalLimit,
            flashSaleLimit,
        }

        /// <summary>
        /// 请求所有限购信息
        /// </summary>
        public void RequestAllLimitData()
        {
            RequestDailyLimit();
            RequestTotalLimit();
            RequestFlashSaleLimit();
        }

        /// <summary>
        /// 购买商品
        /// </summary>
        public void RequestBuy(int id, int num, int couponId)
        {
            market_data marketData = mall_helper.GetMarketDataById(id);
            action_config.ACTION_MARKET_BUY.Register();
            RPC(action_config.ACTION_MARKET_BUY, marketData.__market_version, id, num,
                couponId, MY_DBID, NO_USE, NO_USE, NO_USE);
        }

        /// <summary>
        /// 赠送好友商品
        /// </summary>
        public void RequestGive(int id, int num, UInt64 dbid, int couponId)
        {
            market_data marketData = mall_helper.GetMarketDataById(id);
            action_config.ACTION_MARKET_BUY_GIVE.Register();
            RPC(action_config.ACTION_MARKET_BUY_GIVE, marketData.__market_version, id, num,
                couponId, dbid, NO_USE, NO_USE, NO_USE);
        }

        /// <summary>
        /// 请求每日限购商品信息
        /// </summary>
        public void RequestDailyLimit()
        {
            RPC(action_config.ACTION_MARKET_DAILY_REQ);
        }

        /// <summary>
        /// 请求总量限购商品信息
        /// </summary>
        public void RequestTotalLimit()
        {
            RPC(action_config.ACTION_MARKET_BUY_TOTAL_REQ);
        }

        /// <summary>
        /// 请求抢购商品信息
        /// </summary>
        public void RequestFlashSaleLimit()
        {
            RPC(action_config.ACTION_MARKET_SELL_TOTAL_REQ);
        }

        private void RPC(action_config actionId, params object[] args)
        {
            _player.ActionRpcCall("market_action_req", (int) (actionId), args);
        }

        public void ResponseDailyLimit(byte[] byteArr)
        {
            PbBuyInfoList pbBuyInfoList = MogoProtoUtils.ParseProto<PbBuyInfoList>(byteArr);
            _mallDataManager.FillDailyLimitData(pbBuyInfoList);
        }

        public void ResponseTotalLimit(byte[] byteArr)
        {
            PbBuyInfoList pbBuyInfoList = MogoProtoUtils.ParseProto<PbBuyInfoList>(byteArr);
            _mallDataManager.FillTotalLimitData(pbBuyInfoList);
        }

        public void ResponseFlashSaleLimit(byte[] byteArr)
        {
            PbBuyInfoList pbBuyInfoList = MogoProtoUtils.ParseProto<PbBuyInfoList>(byteArr);
            _mallDataManager.FillFlashSaleLimitData(pbBuyInfoList);
        }

        public void ResponseDailyLimitById(ushort id, uint num)
        {
            _mallDataManager.FillDailyLimitDataById(id, num);
            EventDispatcher.TriggerEvent<int>(MallEvent.RefreshMallItemGrid, (int) id);
        }

        public void ResponseTotalLimitById(ushort id, uint num)
        {
            _mallDataManager.FillTotalLimitDataById(id, num);
            EventDispatcher.TriggerEvent<int>(MallEvent.RefreshMallItemGrid, (int)id);
        }

        public void ResponseFlashSaleLimitById(ushort id, uint num)
        {
            _mallDataManager.FillFlashSaleLimitDataById(id, num);
            EventDispatcher.TriggerEvent<int>(MallEvent.RefreshMallItemGrid, (int)id);
        }

        public void ResponseMarketErrorCode(uint actionId, uint errorCode, byte[] byteArray)
        {
            switch ((action_config) actionId)
            {
            case action_config.ACTION_MARKET_BUY:
                ResponseMallBuy(byteArray);
                break;
            case action_config.ACTION_MARKET_BUY_GIVE:
                ResponseMallGive(byteArray);
                break;
            }
        }

        private void ResponseMallBuy(byte[] byteArray)
        {
            PbBuyRespInfo buyRespInfo = MogoProtoUtils.ParseProto<PbBuyRespInfo>(byteArray);
            string name = item_helper.GetName(mall_helper.GetConfig(buyRespInfo.grid_id).__item_id);
            string content = LangEnum.MALL_BUY_SUCCESS.ToLanguage(buyRespInfo.buy_cnt, name);
           //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, content, PanelIdEnum.Mall);
            EventDispatcher.TriggerEvent(MallEvent.BuySuccess);
        }

        private void ResponseMallGive(byte[] byteArray)
        {
            PbBuyRespInfo buyRespInfo = MogoProtoUtils.ParseProto<PbBuyRespInfo>(byteArray);
            string name = item_helper.GetName(mall_helper.GetConfig(buyRespInfo.grid_id).__item_id);
            buyRespInfo.target_name = MogoProtoUtils.ParseByteArrToString(buyRespInfo.target_name_bytes);
            string content = LangEnum.MALL_SEND_SUCCESSFUL_TIP.ToLanguage(name, buyRespInfo.target_name);
           //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, content, PanelIdEnum.Mall);
            EventDispatcher.TriggerEvent(MallEvent.BuySuccess);
        }
    }
}
