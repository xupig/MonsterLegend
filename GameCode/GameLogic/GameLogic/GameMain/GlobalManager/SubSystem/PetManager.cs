﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/4/23 17:24:51
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.Entities;
using MogoEngine.Events;
using Common.Utils;
using UnityEngine;
using GameData;
using GameLoader.Utils.CustomType;

namespace GameMain.GlobalManager.SubSystem
{
    public class PetManager : Singleton<PetManager> 
    {
        private static PlayerAvatar _player;
        private PetData _petData;
        private BattlePetData _battlePetData;

        public PetManager()
        {
            _player = PlayerAvatar.Player;
            _petData = PlayerDataManager.Instance.PetData;
            _battlePetData = PlayerDataManager.Instance.BattlePetData;
        }

        public void RequestPetList()
        {
            RPC(action_config.ACTION_PET_GET_PET_LIST);
        }

        public void RequestCallPet(int id)
        {
            RPC(action_config.ACTION_PET_CALL_PET, id);
        }

        public void RequestPetFight(int id, int pos)
        {
            RPC(action_config.ACTION_PET_COMBAT, id, pos + 1);
        }

        public void RequestPetAssist(int id, int pos)
        {
            RPC(action_config.ACTION_PET_ASSIST, id, pos + 1);
        }

        public void RequestPetIdle(int id)
        {
            RPC(action_config.ACTION_PET_IDLE, id);
        }

        public void RequestAssistPetIdle(int id)
        {
            RPC(action_config.ACTION_PET_IDLE_ASSIST, id);
        }

        public void RequestAddExp(int id, int itemId, int num)
        {
            action_config.ACTION_PET_ADD_EXP.Register();
            RPC(action_config.ACTION_PET_ADD_EXP, id, itemId, num);
        }

        public void RequestAddStar(int id)
        {
            action_config.ACTION_PET_ADD_STAR.Register();
            RPC(action_config.ACTION_PET_ADD_STAR, id);
        }

        public void RequestAddQuality(int id)
        {
            action_config.ACTION_PET_ADD_QUALITY.Register();
            RPC(action_config.ACTION_PET_ADD_QUALITY, id);
        }

        public void RequestRevive()
        {
            action_config.ACTION_PET_REVIVE.Register();
            RPC(action_config.ACTION_PET_REVIVE);
        }

        public void RequestWash(int petId, int washType, LuaTable lockHoldList)
        {
            action_config.ACTION_PET_REFRESH.Register();
            RPC(action_config.ACTION_PET_REFRESH, petId, washType, lockHoldList);
        }

        public void RequestWashConfirm(int petId, bool isConfirm)
        {
            RPC(action_config.ACTION_PET_REFRESH_CONFIRM, petId, isConfirm ? 1 : 0);
        }

        public void RequestReplacePetInBattle(int id)
        {
            RPC(action_config.ACTION_PET_ACTIVE_RE, id);
        }

        private void RPC(action_config actionId, params object[] args)
        {
            _player.ActionRpcCall("pet_action_req", (int) (actionId), args);
        }

        public void ResponsePetInfoList(PbPetInfoList petList)
        {
            ClearWait();
            _petData.FillPetList(petList);
            EventDispatcher.TriggerEvent(PetEvents.UpdatePetInfo);
        }

        public void ResponseUpdatePetInfo(PbPetInfo pbPetInfo)
        {
            ClearWait();
            _petData.UpdatePetInfo(pbPetInfo);
            EventDispatcher.TriggerEvent(PetEvents.UpdatePetInfo);
        }

        private void ClearWait()
        {
            PlayerAvatar.Player.ClearWait((int)action_config.ACTION_PET_ADD_STAR);
            PlayerAvatar.Player.ClearWait((int)action_config.ACTION_PET_ADD_QUALITY);
        }

        public void ResponsePetFightState(PbPetInfoActiveStateList petFightStateList)
        {
            _petData.UpdatePetFightState(petFightStateList);
            _battlePetData.UpdatePetFightState();
        }

        public void ResponsePetInfoAttrisList(PbPetInfoAttrisList petInfoAttrisList)
        {
            for (int i = 0; i < petInfoAttrisList.petInfoAttrisList.Count; i++)
            {
                PbPetInfoAttris pet = petInfoAttrisList.petInfoAttrisList[i];
                PrintPet(pet);
            }
        }

        private void PrintPet(PbPetInfoAttris pet)
        {
            string name = pet_helper.GetName(pet.pet_id);
            string attriList = GetAttriList(pet.petAttriList);
            Debug.LogError(string.Format("{0}\n{1}", name, attriList));
        }

        private string GetAttriList(List<PbPetAttri> list)
        {
            string attriListStr = "";
            for (int i = 0; i < list.Count; i++)
            {
                PbPetAttri attri = list[i];
                attriListStr = string.Format("{0}{1}:{2}\n", attriListStr, attri_config_helper.GetAttributeName(attri.attri_id), attri.attri_value);
            }
            return attriListStr;
        }

        public void PetDataIsReady(int mapId)
        {
            EventDispatcher.TriggerEvent<int>(BattleUIEvents.BATTLE_PET_RECEIVE_SERVER_DATA_READY, mapId);
        }

        public void ResponsePetFight(PbPetInfoCombatList pbPetInfoCombatList)
        {
            _petData.ResponsePetFight(pbPetInfoCombatList);
        }

        public void ResponsePetIdle(PbPetInfoIdleList pbPetInfoIdleList)
        {
            _petData.ResponsePetIdle(pbPetInfoIdleList);
        }

        public void ResponsePetInfoAddExp(PbPetInfoAddExp pbPetInfoAddExp)
        {
            _petData.ResponsePetInfoAddExp(pbPetInfoAddExp);
        }

        public void ResponseCallPet(PbCreatePetInfo pbCreatePetInfo)
        {
            _petData.ShowPetGetPanel(pbCreatePetInfo);
        }

        public void ResponsePetRefresh(PbPetInfoRefresh pbPetInfoRefresh)
        {
            _petData.ResponsePetRefresh(pbPetInfoRefresh);
        }

        public void ResponsePetRefreshConfirm(PbPetInfoRefreshConfirm pbPetInfoRefreshConfirm)
        {
            _petData.ResponsePetRefreshConfirm(pbPetInfoRefreshConfirm);
        }
    }
}
