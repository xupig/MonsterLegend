﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameMain.Entities;

using GameLoader.Utils.CustomType;
using UnityEngine.Events;
using Common.Structs;
using GameData;
using MogoEngine.Events;
using Common.Events;
using Common.ServerConfig;
using GameLoader.Utils;
using Common.Data;
using UnityEngine;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using Common.Global;
using Common.Utils;
using Common.Chat;
using Common.Base;
using SpaceSystem;
using Common.ClientConfig;

namespace GameMain.GlobalManager.SubSystem
{

    public class SpaceLeftTimeManager : Singleton<SpaceLeftTimeManager>
    {
        private uint _endTime = 0;
        private float _enterSpaceTime = 0;

        /// <summary>
        /// 副本结束时间（客户端本为客户端时间戳，服务器本为服务器同步的时间戳）
        /// </summary>
        public uint endTime
        {
            get
            {
                return _endTime;
            }
        }

        /// <summary>
        /// 加载完场景的时间客户端时间戳
        /// </summary>
        public float enterSpaceTime
        {
            get
            {
                return _enterSpaceTime;
            }
        }

        public SpaceLeftTimeManager()
        {

        }

        public void InitSpaceLeftTime()
        {
            _enterSpaceTime = Time.realtimeSinceStartup;
            //计算客户端本结束时间戳，服务器本由服务器同步
            if (GameSceneManager.GetInstance().IsInClientMap())
            {
                int instId = map_helper.GetInstanceIDByMapID(GameSceneManager.GetInstance().curMapID);
                _endTime = (uint)Time.realtimeSinceStartup + (uint)(instance_helper.GetTimeLeft(instId) / 1000f);
            }
        }

        public void SpaceEndTimeResp(UInt32 endTime)
        {
            _endTime = endTime;
        }

    }
}
