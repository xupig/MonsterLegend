﻿using Common.Base;
using Common.Data;
using Common.Global;
using Common.Utils;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using GameMain.GlobalManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameData
{
    public class ActivityNoticeManager : Singleton<ActivityNoticeManager> 
    {
        public bool showActivityNotice = true;

        public void CheckActivityNotice()
        {
            if (showActivityNotice == false)
            {
                return;
            }
            int noticeLevel = int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.open_notice_level));
            if (PlayerAvatar.Player.level < noticeLevel)
            {
                return;
            }
            ShowActivityNotice();
        }

        private void ShowActivityNotice()
        {
            List<LimitActivityData> resultList = activity_helper.GetLimitActivityList();
            int priority = 0;
            int index = 0;
            int noticeId = 0;
            for (int i = 0; i < resultList.Count; i++)
            {
                if (resultList[i].openData.__notice_priority != 0 && (resultList[i].openData.__notice_priority < priority || priority == 0))
                {
                    if (activity_helper.IsOpen(resultList[i].openData.__id))
                    {
                        priority = resultList[i].openData.__notice_priority;
                        index = i;
                    }
                }
            }
            if (priority != 0)
            {
                noticeId = resultList[index].openData.__id;
                UIManager.Instance.ShowPanel(PanelIdEnum.ActivityNotice, noticeId);
            }
        }
    }
}
