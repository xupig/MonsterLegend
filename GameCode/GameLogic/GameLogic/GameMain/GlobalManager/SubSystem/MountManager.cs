﻿using Common.ServerConfig;
using MogoEngine;
using System;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using Common.Events;
using MogoEngine.Events;

namespace GameMain.GlobalManager.SubSystem
{
    public class MountManager
    {
        private static MountManager _instance;

        public static MountManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MountManager();
                }
                return _instance;
            }
        }

        private MountManager()
        {
        }

        private void RPC(action_config actionId, UInt32 value = 0)
        {
            MogoWorld.Player.RpcCall("ride_req", (int)actionId, value);
        }

        //请求坐骑信息
        public void MountInfoReq()
        {
            RPC(action_config.ACTION_RIDE_GETINFO);
        }

        //请求上坐骑
        public void MountReq()
        {
            RPC(action_config.ACTION_RIDE_MOUNT);
        }

        //请求设置坐骑信息
        public void SetCurrentReq(uint rideId)
        {
            RPC(action_config.ACTION_RIDE_SETCURRENT, rideId);
        }

        /// <summary>
        /// 坐骑的相关通知
        /// </summary>
        /// <param name="noticeType">消息类型:1购买,2过期,3续期,4穿戴</param>
        /// <param name="error_code"></param>
        /// <param name="value"></param>
        public void MountNotice(byte noticeType, uint mountId, uint validTime)
        {
            switch (noticeType)
            {
                case 1:
                    PlayerDataManager.Instance.mountData.MountItemRenewal(mountId, validTime);
                    EventDispatcher.TriggerEvent(MountEvents.CAN_RIDE_NEW_MOUNT, (int)mountId);
                    break;
                case 2:
                    PlayerDataManager.Instance.mountData.MountItemExpire(mountId);
                    EventDispatcher.TriggerEvent<int>(MountEvents.RIDE_ON_MOUNT, 0);
                    break;
                case 3:
                    PlayerDataManager.Instance.mountData.MountItemRenewal(mountId, validTime);
                    break;
                case 4:
                    PlayerDataManager.Instance.mountData.SetCurrentMountId(mountId);
                    EventDispatcher.TriggerEvent<int>(MountEvents.RIDE_ON_MOUNT, (int)mountId);
                    break;
            }
            EventDispatcher.TriggerEvent(MountEvents.REFRESH_MOUNT_LIST);
        }

        public void RespUpdateMountData(byte[] byteArr)
        {
            PbRideList pbRideList = MogoProtoUtils.ParseProto<PbRideList>(byteArr);
            PlayerDataManager.Instance.mountData.Fill(pbRideList);
        }
    }
}
