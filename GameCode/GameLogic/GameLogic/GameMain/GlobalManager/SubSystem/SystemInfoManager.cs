﻿#region 模块信息
/*==========================================
// 文件名：SystemInfoManager
// 命名空间: GameMain.GlobalManager
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/19 8:33:32
// 描述说明：主要用于对服务器的返回码最初响应(已滤过错误码)
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;

using UnityEngine;

using ModuleCommonUI;
using GameMain.GlobalManager.SubSystem;
using Common.ServerConfig;
using GameData;
using Common.Base;
using Common.Data;
using GameMain.Entities;
using Common.Utils;
using Common.Chat;
using GameLoader.Utils;

namespace GameMain.GlobalManager
{

    public enum SystemInfoType
    {
        CHAT_WORLD = 1,//聊天框(世界频道)
        CHAT_NEAR = 2,//聊天框(附近频道)
        CHAT_TEAM = 3,//聊天框(队伍频道)
        CHAT_GUILD = 4,//聊天框(公会频道)
        CHAT_PRIVATE = 5,//私聊
        CHAT_HELP = 6,
        CHAT_WORLD_TIPS = 7,
        CHAT_WORLD_SYSTEM = 8,
        GET_EQUIP_TIPS = 10,    //获得装备
        FIGHTFORCE_TIPS = 11,   //战力提示
        GUILD_RECORD = 12, //公会记事
        FRIEND_INTIMATE = 13,//好友亲密值记录
        
        NOTICE = 21,// 通知栏  
        MESSAGE_BOX = 22,//对话框
        SPLOT_LIGHT = 23,//跑马灯
        FLOAT_TIPS = 24,//浮动tips（提醒）
        SPLOT_LIGHT_TOP = 25,   //跑马灯（顶部）
    }

    public class SystemInfoManager : Singleton<SystemInfoManager> 
    {
        private static int posIndex;

        public void Show(action_config actionId, int systemInfoId, object[] objArr)
        {
            if (systemInfoId == 0)
            {
                systemInfoId = actionId.ToInt32();
            }
            if (CheckIsShowChat(actionId))
            {
                ShowChat(actionId, systemInfoId, objArr);
            }
            else
            {
                ShowInfo(systemInfoId, objArr);
            }
        }

        public void Show(error_code errorCode, params object[] objArr)
        {
            ShowInfo((int)errorCode, objArr);
        }

        public void Show(int systemInfoId, params object[] objArr)
        {
            ShowInfo(systemInfoId, objArr);
        }

        public void Show(error_code errorCode,UInt16 pos)
        {
            if (errorCode == error_code.ERR_ENCHANT_REPLACE)
            {
                posIndex = (int)pos;

                string content = "是否替换？";
          //ari MessageBox.Show(false, MessageBox.DEFAULT_TITLE, content, DoRePlace, null, null, null);
            }
        }

        void DoRePlace()
        {
            PlayerAvatar.Player.ActionRpcCall("enchant_action_req", (int)action_config.ENCHANT_REPLACE, posIndex);
        }

        private void ShowInfo(int systemInfoId, params object[] param)
        {
            if(system_info_helper.HasConfig(systemInfoId) == false)
            {
                Debug.LogError(string.Format("缺少ID为{0}的系统提示", systemInfoId));
                return;
            }
            List<int> typeList = system_info_helper.GetTypeList(systemInfoId);
            if(typeList.Count == 0)
            {
                return;
            }
            for(int i = 0; i < typeList.Count; i++)
            {
                switch((SystemInfoType)typeList[i])
                {
                    case SystemInfoType.CHAT_WORLD:
                    case SystemInfoType.CHAT_NEAR:
                    case SystemInfoType.CHAT_TEAM:
                    case SystemInfoType.CHAT_GUILD:
                    case SystemInfoType.CHAT_PRIVATE:
                    case SystemInfoType.CHAT_HELP:
                    case SystemInfoType.CHAT_WORLD_TIPS:
                    case SystemInfoType.CHAT_WORLD_SYSTEM:
                        if (CanAddSystemChat(systemInfoId, typeList[i]))
                        {
                            AddSystemChat(systemInfoId, typeList[i], param);
                        }
                        break;
                    case SystemInfoType.NOTICE:
                        ShowFastNotice(systemInfoId, SystemInfoUtil.GenerateContent(systemInfoId, param));
                        break;
                    case SystemInfoType.MESSAGE_BOX:
                        ShowMessageBox(SystemInfoUtil.GenerateContent(systemInfoId, param));
                        break;
                    case SystemInfoType.SPLOT_LIGHT:
                        ShowSpotlight(SystemInfoUtil.GenerateContent(systemInfoId, param));
                        break;
                    case SystemInfoType.FLOAT_TIPS:
                        ShowFloatTips(SystemInfoUtil.GenerateContent(systemInfoId, param));
                        break;
                    case SystemInfoType.GUILD_RECORD:
                        AddGuildRecord(SystemInfoUtil.GenerateContent(systemInfoId, param));
                        break;
                    case SystemInfoType.FRIEND_INTIMATE:
                        PlayerDataManager.Instance.FriendData.FriendIntimateEventResp((error_code)systemInfoId,true, param);
                        break;
                    case SystemInfoType.SPLOT_LIGHT_TOP:
                        ShowSpotlight(SystemInfoUtil.GenerateContent(systemInfoId, param), 1);
                        break;
                    default:
                        ShowFloatTips(SystemInfoUtil.GenerateContent(systemInfoId, param));
                        break;
                }
            }
        }

        public void ShowInfo(int type, string content)
        {
            switch ((SystemInfoType)type)
            {
                case SystemInfoType.CHAT_WORLD:
                case SystemInfoType.CHAT_NEAR:
                case SystemInfoType.CHAT_TEAM:
                case SystemInfoType.CHAT_GUILD:
                case SystemInfoType.CHAT_PRIVATE:
                case SystemInfoType.CHAT_HELP:
                case SystemInfoType.CHAT_WORLD_SYSTEM:
                    AddSystemChat(content, type);
                    break;
                case SystemInfoType.NOTICE:
                    break;
                case SystemInfoType.MESSAGE_BOX:
                    ShowMessageBox(content);
                    break;
                case SystemInfoType.SPLOT_LIGHT:
                    ShowSpotlight(content);
                    break;
                case SystemInfoType.FLOAT_TIPS:
                    ShowFloatTips(content);
                    break;
                case SystemInfoType.GUILD_RECORD:
                    AddGuildRecord(content);
                    break;
                case SystemInfoType.FRIEND_INTIMATE:
                    break;
                case SystemInfoType.SPLOT_LIGHT_TOP:
                    ShowSpotlight(content, 1);
                    break;
                default:
                    ShowFloatTips(content);
                    break;
            }
        }

        private void AddGuildRecord(string content)
        {
            GuildManager.Instance.AddGuildRecord(content);
        }

        private void AddSystemChat(int systemInfoId, int channel, params object[] param)
        {
            int channelId = GetChatChannelId(channel);
            int chatMsgType = GetChatMsgType(channel);
            string content = string.Empty;
            Dictionary<string, string[]> followDict = system_info_helper.GetFollowData(systemInfoId);
            if (followDict.ContainsKey("0") && !followDict.ContainsKey("6"))
            {
                int viewId = int.Parse(followDict["0"][0]);
                content = SystemInfoUtil.GenerateContent(systemInfoId, param);
                ChatViewLinkWrapper viewLink = new ChatViewLinkWrapper(content, viewId);
                content = PlayerDataManager.Instance.ChatData.CreateViewLinkMsg(viewLink);
                chatMsgType = (int)ChatContentType.View;
            }
            else if (followDict.ContainsKey("5"))
            {
                if (systemInfoId == (int)error_code.WIDE_RANDOM_MONSTER_START_1 || systemInfoId == (int)error_code.WIDE_RANDOM_MONSTER_START_2)
                {
                    content = ProcessRandomRefreshMonsterChat(systemInfoId, followDict, param);
                    if (string.IsNullOrEmpty(content))
                    {
                        return;
                    }
                    chatMsgType = (int)ChatContentType.SharePosition;
                }
                else
                {
                    if (param.Length < 4)
                    {
                        LoggerHelper.Error(string.Format("系统提示Id={0}，坐标链接参数有误！", systemInfoId));
                        return;
                    }
                    object[] objArr = new object[param.Length - 4];
                    if (param.Length > 4)
                    {
                        param.CopyTo(objArr, 4);
                    }
                    content = SystemInfoUtil.GenerateContent(systemInfoId, objArr);
                    Vector3 pos = new Vector3(float.Parse(param[2].ToString()) / 100f, 0, float.Parse(param[3].ToString()) / 100f);
                    string followValue = followDict["5"][0];
                    ChatSharePosLinkWrapper sharePosLink = new ChatSharePosLinkWrapper(content, int.Parse(param[0].ToString()), int.Parse(param[1].ToString()), pos, int.Parse(followValue));
                    content = PlayerDataManager.Instance.ChatData.CreateSharePosLinkMsg(sharePosLink);
                    chatMsgType = (int)ChatContentType.SharePosition;
                }
            }
            else if (systemInfoId == (int)error_code.ERR_DUEL_BET2_BEGIN_NOTICE)
            {
                content = SystemInfoUtil.GenerateContent(systemInfoId, param);
                int length = param.Length;
                int matchId = int.Parse(param[length - 1].ToString());
                string name = param[length - 2].ToString();
                content = PlayerDataManager.Instance.ChatData.CreateDuelTemplate(content, new ChatDuelLinkWrapper(matchId, name));
                ChatManager.Instance.SendClientChannelSystemMsg((byte)channelId, content, (int)ChatContentType.Duel);
                return;
            }
            else
            {
                content = SystemInfoUtil.GenerateContent(systemInfoId,true, param);
            }
            
            ChatManager.Instance.SendClientChannelSystemMsg((byte)channelId, content, chatMsgType);
        }

        //处理随机刷出野外怪物链接内容
        private string ProcessRandomRefreshMonsterChat(int systemInfoId, Dictionary<string, string[]> followDict, params object[] param)
        {
            string content = string.Empty;
            if (param.Length < 4)
            {
                LoggerHelper.Error(string.Format("系统提示Id={0}，坐标链接参数有误！", systemInfoId));
                return content;
            }
            content = SystemInfoUtil.GenerateContent(systemInfoId, param);
            Vector3 pos = new Vector3(float.Parse(param[2].ToString()) / 100f, 0, float.Parse(param[3].ToString()) / 100f);
            string followValue = followDict["5"][0];
            ChatData chatData = PlayerDataManager.Instance.ChatData;
            ChatSharePosLinkWrapper link = new ChatSharePosLinkWrapper(content, int.Parse(param[0].ToString()), int.Parse(param[4].ToString()), pos, int.Parse(followValue));
            content = chatData.CreateSharePosLinkMsg(link);
            return content;
        }

        /**系统聊天**/
        private void AddSystemChat(string content, int channel)
        {
            int channelId = GetChatChannelId(channel);
            int chatMsgType = GetChatMsgType(channel);
            ChatManager.Instance.SendClientChannelSystemMsg((byte)channelId, content, chatMsgType);
        }

        public int GetChatChannelId(int systemInfoType)
        {
            int channelId = public_config.CHANNEL_ID_WORLD;
            switch ((SystemInfoType)systemInfoType)
            {
                case SystemInfoType.CHAT_WORLD:
                case SystemInfoType.CHAT_NEAR:
                case SystemInfoType.CHAT_TEAM:
                case SystemInfoType.CHAT_GUILD:
                case SystemInfoType.CHAT_PRIVATE:
                    channelId = systemInfoType;
                    break;
                case SystemInfoType.CHAT_WORLD_TIPS:
                case SystemInfoType.CHAT_WORLD_SYSTEM:
                    channelId = public_config.CHANNEL_ID_WORLD;
                    break;
                default:
                    break;
            }
            return channelId;
        }

        public int GetChatMsgType(int systemInfoType)
        {
            int chatMsgType = public_config.CHAT_MSG_TYPE_TEXT;
            switch ((SystemInfoType)systemInfoType)
            {
                case SystemInfoType.CHAT_WORLD_TIPS:
                    chatMsgType = public_config.CHAT_MSG_TYPE_TIPS;
                    break;
                case SystemInfoType.CHAT_WORLD_SYSTEM:
                    chatMsgType = public_config.CHAT_MSG_TYPE_SYSTEM;
                    break;
                default:
                    break;
            }
            return chatMsgType;
        }

        private bool CanAddSystemChat(int systemInfoId, int systemInfoType)
        {
            if (systemInfoType == (int)SystemInfoType.CHAT_WORLD_SYSTEM || systemInfoType == (int)SystemInfoType.CHAT_WORLD_TIPS)
            {
                return (system_info_helper.CanShowSystemMsgCurLv(systemInfoId) && system_info_helper.CanShowSystemMsgCurDay(systemInfoId));
            }
            return true;
        }

        private void ShowMessageBox(string content)
        {
      //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content);
        }

        private void ShowSpotlight(string content, int showType = 0)
        {
            SpotlightData data = new SpotlightData(content, showType, 1);
            UIManager.Instance.ShowPanel(PanelIdEnum.Spotlight,  data);
        }

        private void ShowFloatTips(string content)
        {
           //ari ToolTipsManager.Instance.ShowTip(Common.Base.PanelIdEnum.FloatTips, content, PanelIdEnum.MainUIField);
        }

        private void ShowFastNotice(int systemInfoId, string content)
        {
            FastNoticeData data = new FastNoticeData(content, system_info_helper.GetFollowData(systemInfoId));
            PlayerDataManager.Instance.NoticeData.AddNoticeData(data);

            //UIManager.Instance.ShowPanel(PanelIdEnum.FastNotice,  data);
        }

        public void ShowFastNotice(int systemInfoId)
        {
            FastNoticeData data = new FastNoticeData(system_info_helper.GetContent(systemInfoId), system_info_helper.GetFollowData(systemInfoId));
            PlayerDataManager.Instance.NoticeData.AddNoticeData(data);
            //UIManager.Instance.ShowPanel(PanelIdEnum.FastNotice,  data);
        }

        public void ShowChat(action_config actionId, int systemInfoId, params object[] objArr)
        {
            if (system_info_helper.HasConfig(systemInfoId) == false)
            {
                Debug.LogError(string.Format("缺少ID为{0}的系统提示", systemInfoId));
                return;
            }
            List<int> typeList = system_info_helper.GetTypeList(systemInfoId);
            if (typeList.Count == 0)
            {
                return;
            }
            string content = string.Empty;
            if (actionId == action_config.CHAT_FORGE_PRIVATE)
            {
                content = system_info_helper.GetContent(systemInfoId);
                content = content.Replace("\\n", "\n");
                content = MogoStringUtils.ConvertStyle(content);
                ulong sendDbid = ulong.Parse(objArr[0].ToString());
                string sendName = objArr[1].ToString();
                int sendLv = 1;
                int sendVocation = 2;
                if (objArr.Length >= 4)
                {
                    sendLv = int.Parse(objArr[2].ToString());
                    sendVocation = int.Parse(objArr[3].ToString());
                    //UnityEngine.Debug.LogError("send lv = " + sendLv + ", send vocation = " + sendVocation);
                }
                for (int i = 0; i < typeList.Count; i++)
                {
                    int channelId = GetChatChannelId(typeList[i]);
                    int chatMsgType = (int)ChatContentType.Text;
                    ChatManager.Instance.SendClientChatMsg(channelId, content, chatMsgType, sendDbid, sendName, sendLv, sendVocation);
                }
            }
        }

        private bool CheckIsShowChat(action_config actionId)
        {
            if (actionId == action_config.CHAT_FORGE_PRIVATE)
            {
                return true;
            }
            return false;
        }

    }
}
