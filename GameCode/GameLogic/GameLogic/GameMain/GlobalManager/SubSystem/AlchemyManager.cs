﻿#region 模块信息
/*==========================================
// 文件名：AlchemyManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/11/16 10:02:04
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameLoader.Utils.CustomType;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class AlchemyManager
    {
        private static AlchemyManager _instance;

        public static AlchemyManager Instance
        {
            get 
            {
                if(_instance == null)
                {
                    _instance = new AlchemyManager();
                }
                return _instance;
            }
        }

        private AlchemyManager()
        {

        }

        public void OnZeroClockResp()
        {
            PlayerAvatar.Player.assist_alchemy_cnt = 0;
            PlayerAvatar.Player.alchemy_cnt = 0;
        }

        public void InviteAlchemy(UInt64 dbId,int cell)
        {
            action_config.ACTION_ALCHEMY_INVITE.Register();
            RPC(action_config.ACTION_ALCHEMY_INVITE, dbId,cell);
        }

        public void RequestAlchemyAssistList()
        {
            RPC(action_config.ACTION_ALCHEMY_GET_ASSISTS);
        }

        public void ReceiveAlchemyReward()
        {
            action_config.ACTION_ALCHEMY_RECEIVE.Register();
            RPC(action_config.ACTION_ALCHEMY_RECEIVE);
        }

        public void RequestAlchemyAssitCnt()
        {
            RPC(action_config.ACTION_ALCHEMY_GET_ASSIST_CNT);
        }

        private LuaTable luaTable = new LuaTable();
        public void Alchemy(Dictionary<int,PbAlchemyAssist> dict)
        {
            luaTable.Clear();
            int count = 1;
            Dictionary<int,PbAlchemyAssist> removeList = new Dictionary<int,PbAlchemyAssist>();
           bool hasIllegalMember = false;
            foreach (KeyValuePair<int, PbAlchemyAssist> kvp in dict)
            {
                if (kvp.Value != null)
                {
                    
                    if(PlayerDataManager.Instance.FriendData.HasFriend(kvp.Value.dbid) == false && PlayerDataManager.Instance.GuildData.IsGuildMember(kvp.Value.dbid) == false)
                    {
                        hasIllegalMember = true;
                        removeList.Add(kvp.Key,kvp.Value);
                    }
                    else
                    {
                        LuaTable table = new LuaTable();
                        table.Add(1, kvp.Key);
                        table.Add(2, kvp.Value.dbid);
                        luaTable.Add(count, table);
                        count++;
                    }
                }
            }
            if (hasIllegalMember)
            {
                foreach(KeyValuePair<int,PbAlchemyAssist> kvp in removeList)
                {
                    SystemInfoManager.Instance.Show(error_code.ERR_ALCHEMY_NO_FRIEND, kvp.Value.name);
                    dict[kvp.Key] = null;
                    PlayerDataManager.Instance.AlchemyData.hadInviteAssistDict.Remove(kvp.Value.dbid);
                    PlayerDataManager.Instance.AlchemyData.inviteAlchemyAssistDict.Remove(kvp.Value.dbid);
                    EventDispatcher.TriggerEvent(AlchemyEvents.UPDATE_ALCHEMY_ASSIST_LIST);
                }
            }
            else
            {
                action_config.ACTION_ALCHEMY_ALCHEMY.Register();
                PlayerAvatar.Player.RpcCall("alchemy_action_req", (int)action_config.ACTION_ALCHEMY_ALCHEMY, luaTable);
            }
           
        }

        private bool RPC(action_config actionId, params object[] args)
        {
            PlayerAvatar.Player.ActionRpcCall("alchemy_action_req", (int)actionId, args);
            return true;
        }

        public void OnAlchemyResp(UInt32 actionId, UInt32 errorId, byte[] byteArr)
        {
            if(errorId==0)
            {
                switch ((action_config)actionId)
                {
                    case action_config.ACTION_ALCHEMY_ALCHEMY:
                        
                        break;
                    case action_config.ACTION_ALCHEMY_GET_ASSISTS:
                        PbAlchemyAssistList list = MogoProtoUtils.ParseProto<PbAlchemyAssistList>(byteArr);
                        PlayerDataManager.Instance.AlchemyData.ClearPlayer();
                        for (int i = 0; i < list.alchemyAssistList.Count; i++)
                        {
                            list.alchemyAssistList[i].name = MogoProtoUtils.ParseByteArrToString(list.alchemyAssistList[i].name_bytes);
                            PlayerDataManager.Instance.AlchemyData.alchemyAssistDict[(int)list.alchemyAssistList[i].pos] = list.alchemyAssistList[i];
                        }
                        PlayerDataManager.Instance.AlchemyData.hasReceiveAssistList = true;
                        //PlayerDataManager.Instance.AlchemyData.CheckAlchemyTimeStamp();
                        EventDispatcher.TriggerEvent(AlchemyEvents.UPDATE_ALCHEMY_ASSIST_LIST);
                        break;
                    case action_config.ACTION_ALCHEMY_GET_ASSIST_CNT:
                        PbAlchemyAssistCnt cnt = MogoProtoUtils.ParseProto<PbAlchemyAssistCnt>(byteArr);
                        PlayerAvatar.Player.assist_alchemy_cnt = cnt.cnt;
                        EventDispatcher.TriggerEvent(AlchemyEvents.UPDATE_ALCHEMY_ASSIST_CNT);
                        break;
                    case action_config.ACTION_ALCHEMY_INVITE:
                        PbAlchemyInvite assist = MogoProtoUtils.ParseProto<PbAlchemyInvite>(byteArr);
                        if(PlayerDataManager.Instance.AlchemyData.inviteAlchemyAssistDict.ContainsKey(assist.dbid))
                        {
                            if (PlayerDataManager.Instance.AlchemyData.alchemyAssistDict[(int)assist.pos]!=null)
                            {
                                PbAlchemyAssist replaceAssist = PlayerDataManager.Instance.AlchemyData.alchemyAssistDict[(int)assist.pos];
                                PlayerDataManager.Instance.AlchemyData.inviteAlchemyAssistDict.Remove(replaceAssist.dbid);
                                PlayerDataManager.Instance.AlchemyData.hadInviteAssistDict.Remove(replaceAssist.dbid);
                            }
                            PlayerDataManager.Instance.AlchemyData.alchemyAssistDict[(int)assist.pos] = PlayerDataManager.Instance.AlchemyData.inviteAlchemyAssistDict[assist.dbid];
                            if (PlayerDataManager.Instance.AlchemyData.hadInviteAssistDict.ContainsKey(assist.dbid) == false)
                            {
                                PlayerDataManager.Instance.AlchemyData.hadInviteAssistDict.Add(assist.dbid, PlayerDataManager.Instance.AlchemyData.inviteAlchemyAssistDict[assist.dbid]);
                            }
                            else
                            {
                                PlayerDataManager.Instance.AlchemyData.hadInviteAssistDict[assist.dbid] = PlayerDataManager.Instance.AlchemyData.inviteAlchemyAssistDict[assist.dbid];
                            }
                            EventDispatcher.TriggerEvent(AlchemyEvents.UPDATE_ALCHEMY_INVITE_PLAYER, assist.dbid, (int)assist.pos);
                            PanelIdEnum.AlchemyInvite.Close();
                        }
                        break;
                    case action_config.ACTION_ALCHEMY_RECEIVE:
                        
                        EventDispatcher.TriggerEvent(AlchemyEvents.UPDATE_ALCHEMY_REWARD);
                        PlayerDataManager.Instance.AlchemyData.ClearInvite();
                        PlayerDataManager.Instance.AlchemyData.ClearPlayer();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
