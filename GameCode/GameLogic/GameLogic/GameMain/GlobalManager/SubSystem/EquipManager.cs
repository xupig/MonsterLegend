﻿using Common.Base;
using Common.Chat;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameLoader.Utils.CustomType;
using GameMain.Entities;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class EquipManager : Singleton<EquipManager> 
    {
        private const string LINK_FORMAT = "[{0},{1}]";

        public void RequestStrengthen(int equipPosition, LuaTable stoneData, byte flag)
        {
            action_config.STRENGTHEN_REQ.Register();
            action_config.STRENGTHEN_REQ.Rpc("strengthen_req", equipPosition, stoneData, flag);
        }

        public void RequestOneClickStrengthen(int equipPosition)
        {
            action_config.STRENGTHEN_ONE_KEY_REQ.Register();
            action_config.STRENGTHEN_ONE_KEY_REQ.Rpc("strengthen_req", equipPosition);
        }

        public void RequestStrengthenList()
        {
            action_config.STRENGTHEN_QUERY_LIST_REQ.Rpc("strengthen_req");
        }

        public void RequestAllBodyEnchantInfo()
        {
            action_config.ENCHANT_GET_RECORD.Rpc("enchant_action_req");
        }

        public void RequestEnchant(int posId)
        {
            action_config.ENCHANT.Register();
            action_config.ENCHANT.Rpc("enchant_action_req",posId);
        }

        public void RequestReplace(int posIndex)
        {
            action_config.ENCHANT_REPLACE.Rpc("enchant_action_req",posIndex);
        }

        public void RequestDecompose(int gridPostion)
        {
            EquipItemInfo equipItemInfo = PlayerDataManager.Instance.BagData.GetItemBagData().GetItemInfo(gridPostion) as EquipItemInfo;
            if (equipItemInfo.Quality >= public_config.ITEM_QUALITY_PURPLE)
            {
                Action confirmAction = new Action(() =>
                {
                    PlayerAvatar.Player.RpcCall("equip_decompose_req", gridPostion);
                });
                Action cancelAction = new Action(() =>
                {
                    return;
                });

          //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetString(LangEnum.COMPOSITION_ARE_YOU_OK), confirmAction, cancelAction, false, true, false);
            }
            else
            {
                PlayerAvatar.Player.RpcCall("equip_decompose_req", gridPostion);
            }
        }

        /// <summary>
        /// 请求熔炼
        /// </summary>
        /// <param name="smeltId">装备熔炼流水号</param>
        public void RequestSmelt(int smeltId, int smeltNum)
        {
            PlayerAvatar.Player.RpcCall("equip_smelting_req", action_config.ACTION_EQUIP_SMELTING_SMELT, smeltId, smeltNum);
            action_config.ACTION_EQUIP_SMELTING_SMELT.Register();
        }

        public void RequesetRecast(BagType bagType, int gridPosition)
        {
            LuaTable list = new LuaTable();
            list.Add(1, (int)bagType);
            list.Add(2, gridPosition);
            PlayerAvatar.Player.RpcCall("equip_req", action_config.EQUIP_RECAST_REQ, list);
        }

        public void CheckUpgradeEquipInfo(BagType bagType, int gridPosition)
        {
            LuaTable list = new LuaTable();
            list.Add(1, (int)bagType);
            list.Add(2, gridPosition);
            PlayerAvatar.Player.RpcCall("equip_upgrade_req", action_config.ACTION_EQUIP_UPGRADE_GET_INFO_REQ, list);
        }

        public void RequestUpgradeEquip(BagType bagType, int gridPosition)
        {
            LuaTable list = new LuaTable();
            list.Add(1, (int)bagType);
            list.Add(2, gridPosition);
            PlayerAvatar.Player.RpcCall("equip_upgrade_req", action_config.ACTION_EQUIP_UPGRADE_UPGRADE_REQ, list);
        }


        public void RequesetLastRecastInfo()
        {
            action_config.EQUIP_RECAST_GET_INFO_REQ.Rpc("equip_req");
        }

        /// <summary>
        /// 确认重铸结果
        /// </summary>
        /// <param name="result">0表示取消，1表示确认</param>
        public void ConfirmRecastResult(int result)
        {
            LuaTable list = new LuaTable();
            list.Add(1, result);
            PlayerAvatar.Player.RpcCall("equip_req", action_config.EQUIP_RECAST_CONFIRM_REQ, list);
        }

        public void RequestDecomposeList(List<int> gridPosList)
        {
            LuaTable list = new LuaTable();
            int count = gridPosList.Count;
            for (int i = 0; i < count; i++)
            {
                list.Add(i + 1,gridPosList[i]);
            }
            PlayerAvatar.Player.RpcCall("equip_decompose_list_req", list);
        }

        public void StrengthenQueryListRsp(byte[] byteArr)
        {
            PbStrengthenInfoList strengthenInfoList = MogoProtoUtils.ParseProto<PbStrengthenInfoList>(byteArr);
            PlayerDataManager.Instance.EquipStrengthenData.Fill(strengthenInfoList);
            EventDispatcher.TriggerEvent(EquipStrengthenEvents.Init);
        }

        public void StrengthenQueryRsp(byte[] byteArr)
        {
            //PbStrengthenInfo strengthenInfo = MogoProtoUtils.ParseProto<PbStrengthenInfo>(byteArr);
            //PlayerDataManager.Instance.EquipStrengthenData.UpdateStrengthenInfo(strengthenInfo);
        }

        public void MakeStrengthenRsp(uint result, byte[] byteArr)
        {
            if (result == 0)
            {
                EventDispatcher.TriggerEvent(EquipStrengthenEvents.StrengthenSuccess);
            }
            else if ((error_code)result == error_code.ERR_STRENGTHEN_FAIL)///成功
            {
                EventDispatcher.TriggerEvent(EquipStrengthenEvents.StrengthenFail);
            }
        }

        public void EquipSmeltingResp(UInt32 actionId, UInt32 errorId, byte[] byteArr)
        {
            switch ((action_config)actionId)
            {
                case action_config.ACTION_EQUIP_SMELTING_SMELT:
                    PbEquipSmeltingItemList equipSmeltingItemList = MogoProtoUtils.ParseProto<PbEquipSmeltingItemList>(byteArr);
                    EventDispatcher.TriggerEvent<PbEquipSmeltingItemList>(EquipEvents.EQUIP_SMELT_RESULT, equipSmeltingItemList);
                    break;
            }
        }

        public void OnEquipResp(UInt32 actionId, UInt32 errorCode, byte[] byteArr)
        {
            switch ((action_config)actionId)
            {
                case action_config.EQUIP_RECAST_CONFIRM_REQ:
                case action_config.EQUIP_RECAST_GET_INFO_REQ:
                    PbEquipRecastResp recastResultNotice = MogoProtoUtils.ParseProto<PbEquipRecastResp>(byteArr);
                    EventDispatcher.TriggerEvent<PbEquipRecastResp>(EquipEvents.RECAST_RESULT, recastResultNotice);
                    break;
                case action_config.EQUIP_RECAST_BUFF_NOTICE:
                    PbEquipRecastResp includeBuffRecastResult = MogoProtoUtils.ParseProto<PbEquipRecastResp>(byteArr);
                    CheckBuffAndTrySendChat(includeBuffRecastResult);
                    break;
            }
        }


        public void OnEnchantResp(UInt32 actionId, UInt32 errorCode, byte[] byteArr)
        {
            switch ((action_config)actionId)
            {
                case action_config.ENCHANT:
                    PbEquipEnchantAttriInfo replaceEnchantAttri = MogoProtoUtils.ParseProto<PbEquipEnchantAttriInfo>(byteArr);
                    PlayerDataManager.Instance.EnchantData.UpdateLuckyValue(replaceEnchantAttri.pos, replaceEnchantAttri.luck_value);
                    EventDispatcher.TriggerEvent<int>(EnchantEvents.UpdateLuckyValue, (int)replaceEnchantAttri.pos);
                    EventDispatcher.TriggerEvent<PbEquipEnchantAttriInfo>(EnchantEvents.ReplaceAttribute, replaceEnchantAttri);
                    break;
                case action_config.ENCHANT_REPLACE:
                    PbEquipEnchantAttriInfo resultEnchantAttri = MogoProtoUtils.ParseProto<PbEquipEnchantAttriInfo>(byteArr);
                    PlayerDataManager.Instance.EnchantData.UpdateEnchantAttri(resultEnchantAttri);
                    EventDispatcher.TriggerEvent<int>(EnchantEvents.UpdateEnchantByBody, (int)resultEnchantAttri.pos);
                    break;
                case action_config.ENCHANT_GET_RECORD:
                    PbEquipEnchantAttriRec enchantAttriRec = MogoProtoUtils.ParseProto<PbEquipEnchantAttriRec>(byteArr);
                    PlayerDataManager.Instance.EnchantData.Fill(enchantAttriRec);
                    EventDispatcher.TriggerEvent(EnchantEvents.Init);
                    break;
            }
        }

        public void EquipUpgradeResp(UInt32 actionId, UInt32 errorId, byte[] args)
        {
            switch ((action_config)actionId)
            {
                case action_config.ACTION_EQUIP_UPGRADE_GET_INFO_REQ:
                    PbEquipUpgradeResp equipUpgrade = MogoProtoUtils.ParseProto<PbEquipUpgradeResp>(args);
                    EventDispatcher.TriggerEvent<PbEquipUpgradeResp>(EquipEvents.GET_RISING_STAR_CHECK, equipUpgrade);
                    break;
                case action_config.ACTION_EQUIP_UPGRADE_UPGRADE_REQ:
                    PbEquipUpgradeResp equipUpgradeSuccess = MogoProtoUtils.ParseProto<PbEquipUpgradeResp>(args);
                    UIManager.Instance.ShowPanel(PanelIdEnum.EquipRisingStarPop, equipUpgradeSuccess);
                    EventDispatcher.TriggerEvent(EquipEvents.RISING_STAR_SUCCESS);
                    break;
            }
        }


        private void CheckBuffAndTrySendChat(PbEquipRecastResp recastResult)
        {
            if (recastResult.recast_equip.equip_buffs.Count > 0)
            {
                int itemId = (int)recastResult.recast_equip.id;
                string equipName = item_helper.GetName(itemId);
                int quality = item_helper.GetQuality(itemId);
                List<uint> buffList = recastResult.recast_equip.equip_buffs;
                int buffCount = buffList.Count;
                string buffName = string.Empty;
                for (int i = 0; i < buffCount; i++)
                {
                    if (buff_helper.CheckBuffId((int)buffList[i]) == false)
                    {
                        return;
                    }
                    buffName += string.Format("【{0}】", buff_helper.GetName((int)buffList[i]));
                }
                string itemLink = string.Format(LINK_FORMAT, equipName, (int)ChatLinkType.Item);

                string content = MogoLanguageUtil.GetContent(114068, PlayerAvatar.Player.name, buffName, itemLink);

                ChatItemLinkWrapper itemLinkWrapper = new ChatItemLinkWrapper(equipName, (int)recastResult.pkg_type, (int)recastResult.pos, item_helper.GetQuality(itemId));
                List<ChatItemLinkWrapper> itemLinkList = new List<ChatItemLinkWrapper>();
                itemLinkList.Add(itemLinkWrapper);
                content = PlayerDataManager.Instance.ChatData.CreateLinkMsg(MogoStringUtils.ConvertStyle(content), itemLinkWrapper, true);
                ChatManager.Instance.RequestSendChat(public_config.CHANNEL_ID_WORLD, content, 0, (byte)ChatContentType.Item, itemLinkList.ToArray());
            }
        }

    }
}
