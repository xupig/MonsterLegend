﻿#region 模块信息
/*==========================================
// 文件名：FunctionManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/5/25 14:03:29
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class FunctionManager : Singleton<FunctionManager> 
    {

        public void GetFunctionOpenQuery()
        {
            PlayerAvatar.Player.RpcCall("function_open_req", (int)action_config.ACTION_FUNCTION_OPEN_QUERY);
        }

        public void OnFunctionResp(UInt32 actionId,UInt32 errorId,byte[] bytes)
        {
            switch((action_config)actionId)
            {
                case action_config.ACTION_FUNCTION_OPEN_NOTIFY:
                    FunctionOpenNotify(bytes);
                    break;
                case action_config.ACTION_FUNCTION_OPEN_QUERY:
                    FunctionOpenQuery(bytes);
                    break;
                default:
                    break;
            }
        }

        

        private void FunctionOpenNotify(byte[] bytes)
        {
            PbFunctionOpenNotify notify = MogoProtoUtils.ParseProto<PbFunctionOpenNotify>(bytes);
            if(notify.is_open == 1)
            {
                PlayerDataManager.Instance.FunctionData.AddFunction(notify.function_id);
            }
            else
            {
                PlayerDataManager.Instance.FunctionData.RemoveFunction(Convert.ToInt32(notify.function_id));
            }
        }

        private void FunctionOpenQuery(byte[] bytes)
        {
            PbFunctionOpenList list = MogoProtoUtils.ParseProto<PbFunctionOpenList>(bytes);
            PlayerDataManager.Instance.FunctionData.InitFunction(list.function_id);
            if (PlayerDataManager.Instance.FunctionData.IsFunctionOpen(FunctionId.dailyactive))
            {
                EventDispatcher.TriggerEvent(TaskEvent.TASK_MAIN_TASK_CHANGE,true);
            }
            
            //Debug.LogError("FunctionOpenQuery:"+list.function_id.Count);
            EventDispatcher.TriggerEvent(FunctionEvents.INIT);
        }

    }
}
