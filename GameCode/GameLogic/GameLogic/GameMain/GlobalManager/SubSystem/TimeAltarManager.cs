﻿using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameLoader.Utils.Timer;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class TimeAltarManager : Singleton<TimeAltarManager>
    {
        private int _functionId = -1;
        private int _date = -1;
        private int _count = -1;

        public void RequestRedemption(int functionId, int date, int count)
        {
            _functionId = functionId;
            _date = date;
            _count = count;

            action_config.ACTION_TIME_ALTAR_GET_REWARD.Rpc("time_altar_req", functionId, date, 1);
        }

        public void TimeAltarResp(UInt32 action_id, UInt32 errorId, byte[] byteArr)
        {
            switch ((action_config)action_id)
            {
                case action_config.ACTION_TIME_ALTAR_QUERY_INFO:
                    PbTimeAltarInfo timeAltarInfo = MogoProtoUtils.ParseProto<PbTimeAltarInfo>(byteArr);
                    PlayerDataManager.Instance.TimeAltarData.Fill(timeAltarInfo);
                    EventDispatcher.TriggerEvent(TimeAltarEvents.InitTimeAltar);
                    break;
                case action_config.ACTION_TIME_ALTAR_GET_REWARD:
                    PlayerDataManager.Instance.TimeAltarData.UpdateRedemptionItem(_functionId, _date, _count-1);
                    EventDispatcher.TriggerEvent<int>(TimeAltarEvents.UpdateTimeAltar, _functionId);
                    _functionId = -1;
                    _date = -1;
                    _count = -1;
                    break;
            }
        }
    }
}
