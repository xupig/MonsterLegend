﻿#region 模块信息
/*==========================================
// 文件名：AntiAddictionManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/2/26 16:11:00
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ClientConfig;
using Common.Events;
using Common.ServerConfig;
using Common.Utils;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Mgrs;
using System;
namespace GameMain.GlobalManager.SubSystem
{
    public enum AntiPanelType
    {
        Login = 1,
        WriteMessage = 2,
        KickOff = 3,
    }

    public class AntiDataWrapper
    {
        public AntiPanelType type;
        public int alreadyPlayTime;
        public int tipsId;
        public bool isKickOff = false;
    }

    public class AntiAddictionManager : Singleton<AntiAddictionManager>
    {
        public bool NeedShowMessage = false;

        private int alreadyPlayedTime = 0;

        private uint timeId;
        public void SetMainUIShowAntiMessage(bool needShowMessage)
        {
            NeedShowMessage = needShowMessage;
            EventDispatcher.TriggerEvent<bool>(MainUIEvents.NEED_SHOW_ANTIADDICTION, needShowMessage);
            if (needShowMessage == true)
            {
                if (timeId == 0)
                {
                    alreadyPlayedTime = (int)PlayerAvatar.Player.addiction_time;
                    timeId = TimerHeap.AddTimer(0, 1000, RefreshPlayTime);
                }
            }
            else
            {
                TimerHeap.DelTimer(timeId);
                timeId = 0;
            }
        }

        private void RefreshPlayTime()
        {
            EventDispatcher.TriggerEvent<int>(AntiAddictionEvents.ANTI_TIME_REFRESH, alreadyPlayedTime);
            alreadyPlayedTime++;
        }

        public void OnAntiAddictionResp(UInt32 ret_code, UInt32 arg1, UInt32 arg2)
        {
            int tipsId = (int)arg1;
            int totaltime = (int)arg2;
            AntiDataWrapper wrapper = new AntiDataWrapper();
            wrapper.tipsId = tipsId;
            wrapper.alreadyPlayTime = totaltime;
            wrapper.type = AntiPanelType.KickOff;
            switch ((error_code)ret_code)
            {
                case error_code.ERR_ANTI_ADDICTION_TIPS:
                    UIManager.Instance.ShowPanel(PanelIdEnum.AntiAddiction, wrapper);
                    break;
                case error_code.ERR_ANTI_ADDICTION_TIP_AND_QUIT:
                    wrapper.isKickOff = false;
                    UIManager.Instance.ShowPanel(PanelIdEnum.AntiAddiction, wrapper);
                    break;
                case error_code.ERR_ANTI_ADDICTION_TIP_NOT_QUIT:
                    wrapper.isKickOff = true;
                    UIManager.Instance.ShowPanel(PanelIdEnum.AntiAddiction, wrapper);
                    break;
            }
        }

        public void SendTheAntiAddictionCode(UInt16 type, UInt32 arg1)
        {
            PlayerAvatar.Player.RpcCall("anti_addiction_req", type, arg1);
        }

        public void AddListener()
        {
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.addiction_time, RefreshPlayedTime); 
        }

        private void RefreshPlayedTime()
        {
            alreadyPlayedTime = (int)PlayerAvatar.Player.addiction_time;
        }
    }
}