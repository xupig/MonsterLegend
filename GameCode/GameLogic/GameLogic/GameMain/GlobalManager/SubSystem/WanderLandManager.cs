﻿#region 模块信息
/*==========================================
// 创建者：陈铨
// 创建日期：2015/3/30 14:19:20
// 描述说明：
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Utils;
using UnityEngine;
using Common.Utils;
using GameLoader.Utils;
using ModuleCommonUI;

namespace GameMain.GlobalManager.SubSystem
{
    public class WanderLandManager : Singleton<WanderLandManager>
    {
        private static PlayerAvatar _player;
        private static WanderLandData _wanderLandData;

        public WanderLandManager()
        {
            _player = PlayerAvatar.Player;
            _wanderLandData = PlayerDataManager.Instance.WanderLandData;
        }

        public void RequestInfo()
        {
            RPC(action_config.ACTION_TRY_INFO_QUERY_REQ);
        }

        public void RequestFight(int instanceId)
        {
            RPC(action_config.ACTION_TRY_CHALLENGE_REQ, instanceId);
        }

        public void RequestSweep(int instanceId)
        {
            RPC(action_config.ACTION_TRY_MOP_UP_REQ, instanceId);
        }

        public void RequestResetTimes()
        {
            RPC(action_config.ACTION_TRY_DATA_RESET_REQ);
        }

        public void RequsetGetInstanceBoxReward(int instanceId, int starNum)
        {
            RPC(action_config.ACTION_TRY_MISSION_BOX_REQ, instanceId, 0, starNum);
        }

        public void RequsetGetAllInstanceBoxReward(int instanceId)
        {
            RPC(action_config.ACTION_TRY_MISSION_BOX_REQ, instanceId, 1, 0);
        }

        public void RequsetGetChapterBoxReward(int chapterId, int starNum)
        {
            RPC(action_config.ACTION_GET_STAR_SPIRIT_REWARD, chapterId, starNum);
        }

        private void RPC(action_config actionId, params object[] args)
        {
            LoggerHelper.Debug("试炼秘境发送协议:" + (action_config)actionId);
            _player.ActionRpcCall("try_mission_action_req", (int)(actionId), args);
        }

        public static void ResponseWanderLandInfo(uint actionId, uint errorCode, byte[] byteArr)
        {
            LoggerHelper.Debug("试炼秘境收到协议:" + (action_config)actionId);
            PbTryMissionInfo wanderLandInfo;
            switch ((action_config)actionId)
            {
                case action_config.ACTION_TRY_INFO_QUERY_REQ:
                    wanderLandInfo = MogoProtoUtils.ParseProto<PbTryMissionInfo>(byteArr);
                    _wanderLandData.FillWanderLandInfo(wanderLandInfo);
                    EventDispatcher.TriggerEvent(WanderLandEvents.RefreshWanderLandView);
                    break;
                case action_config.ACTION_TRY_DATA_RESET_REQ:
                    wanderLandInfo = MogoProtoUtils.ParseProto<PbTryMissionInfo>(byteArr);
                    _wanderLandData.FillWanderLandInfo(wanderLandInfo);
                    EventDispatcher.TriggerEvent(WanderLandEvents.Reset);
                    break;
                case action_config.ACTION_TRY_MOP_UP_REQ:
                    PbTryPassState pbTryPassState = MogoProtoUtils.ParseProto<PbTryPassState>(byteArr);
                    PlayerDataManager.Instance.CopyData.FillWanderlandInstance(pbTryPassState.mission_id, pbTryPassState);
                    //MessageBox.Show(true, MessageBox.DEFAULT_TITLE, MogoLanguageUtil.GetContent(40421));
                    EventDispatcher.TriggerEvent(WanderLandEvents.SweepSuccess);
                    break;
                case action_config.ACTION_TRY_MISSION_BOX_REQ:
                    PbMissionStarInfo pbMissionStarInfo = MogoProtoUtils.ParseProto<PbMissionStarInfo>(byteArr);
                    _wanderLandData.FillMissionBox(pbMissionStarInfo);
                    EventDispatcher.TriggerEvent(WanderLandEvents.RefreshGetBoxReward);
                    break;
            }
        }
    }
}
