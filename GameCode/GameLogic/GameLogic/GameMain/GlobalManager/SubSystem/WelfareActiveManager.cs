﻿using Common.Base;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameLoader.Utils.CustomType;
using GameMain.Entities;
using ModuleCommonUI;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Utils;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    /// <summary>
    /// 福利活动//与服务器通讯类
    /// </summary>
    public class WelfareActiveManager : Singleton<WelfareActiveManager>
    {
        /// <summary>
        /// 创建界面预输入的邀请码
        /// </summary>
        public string inputInviteCode;
        /// <summary>
        /// 玩家已有的邀请码
        /// </summary>
        public string playerInviteCode;

        /// <summary>
        /// 奖励信息列表
        /// </summary>
        public PbCompleteList rewardList { get; private set; }

        /// <summary>
        /// 手机绑定信息
        /// </summary>
        public PbBindMobileInfo pbBindMobileInfo { get; private set; }

        /// <summary>
        /// 福利活动系统--成功返回处理
        /// </summary>
        /// <param name="msgId">消息ID</param>
        /// <param name="respCode">响应码</param>
        /// <param name="data">数据</param>
        public void OnFriendInviteResp(uint msgId, uint respCode, byte[] data)
        {
            LoggerHelper.Debug(string.Format("福利活动系统推送 msgId:{0},respCode:{1},data:{2}", msgId, respCode, data));
            switch ((action_config)msgId)
            {
                case action_config.ACTION_INVITE_USE_INVITE_NUM:  
                     //邀请码使用成功,派发事件给CreateRolePanel.cs接收
                     EventDispatcher.TriggerEvent(WelfareActiveEvent.C_USE_SUCESS);
                     //ari//ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, "成功使用邀请码", PanelIdEnum.MainUIField);
                     break;    
                case action_config.ACTION_INVITE_GET_INVITE_NUM:
                case action_config.ACTION_INVITE_GENERATE_INVITE_NUM:
                     //派发事件给 WelfareActivePanel.cs 刷新页面
                     playerInviteCode = MogoProtoUtils.ParseProto<PbFriendInviteCode>(data).invite_code;
                     LoggerHelper.Info("==== 返回邀请码：" + playerInviteCode);
                     EventDispatcher.TriggerEvent<string>(WelfareActiveEvent.C_REFRESH_INVITE_CODE, playerInviteCode);
                     break;
                case action_config.ACTION_INVITE_GET_REWARD:
                     //领取奖励
                     EventDispatcher.TriggerEvent<PbCompleteId>(WelfareActiveEvent.C_REFRESH_RECEVIE, MogoProtoUtils.ParseProto<PbCompleteId>(data)); 
                     break;
                case action_config.ACTION_INVITE_CHECK_REWARD:
                case action_config.ACTION_INVITE_UPDATE_PROGRESS:
                case action_config.ACTION_INVITE_NOTIFY_NEW_REWARD: 
                     //派发事件给 WelfareActivePanel.cs 刷新页面
                     rewardList = MogoProtoUtils.ParseProto<PbCompleteList>(data);
                     EventDispatcher.TriggerEvent(WelfareActiveEvent.C_REFRESH_REWARD_LIST); 
                     break;
                case action_config.ACTION_INVITE_QUERY_LIST:
                     PbInviteRecordList recordList = MogoProtoUtils.ParseProto<PbInviteRecordList>(data);
                     LoggerHelper.Info("==== 返回邀请记录：" + recordList);
                     EventDispatcher.TriggerEvent(WelfareActiveEvent.C_REFRESH_INVITE_RECORD_LIST, recordList);
                     break;
            }
        }

        //错误返回处理(在OnActionResp()中返回错误)
        public void RespErrorHandler(ushort msgId, ushort respCode, LuaTable data)
        {
            if (respCode >= (int)error_code.ERR_FRIEND_INVITE_NUM_NOT_EXIST && respCode <= (int)error_code.ERR_FRIEND_INVITE_NUM_NOT_GENERATE)
            {
               //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, SystemInfoUtil.GenerateContent(respCode), PanelIdEnum.MainUIField);
            }
            else if (respCode >= (int)error_code.ERR_BIND_MOBILE_REPEAT_BIND && respCode <= (int)error_code.ERR_BIND_MOBILE_CODE_PAST_DUE)
            {//手机绑定错误码返回 
               //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, SystemInfoUtil.GenerateContent(respCode), PanelIdEnum.MainUIField);
            }
        }

        //===================  向服务器发起请求  =====================//
        /// <summary>
        /// 向服务器发起--生成邀请码请求
        /// </summary>
        public void CreateInviteCodeReq()
        {
            if (string.IsNullOrEmpty(playerInviteCode))
            {//没邀请码,发起邀请码创建请求
                LoggerHelper.Info("创建邀请码请求!");
                Rpc((int)action_config.ACTION_INVITE_GENERATE_INVITE_NUM);
            }
        }

        /// <summary>
        ///  向服务器发起--使用邀请码请求(别人的邀请码)
        ///  <param name="inviteCode">对方的邀请码</param>
        /// </summary>
        public void UseInviteCodeReq(string inviteCode)
        {
            Rpc((int)action_config.ACTION_INVITE_USE_INVITE_NUM, inviteCode);
        }

        /// <summary>
        /// 向服务器发起--获取邀请码请求
        /// </summary>
        public void GetInviteCodeReq()
        {
            Rpc((int)action_config.ACTION_INVITE_GET_INVITE_NUM);
        }

         /// <summary>
        /// 向服务器发起--领取奖励请求
        /// <param name="inviteId">inviteId领取ID(invite.xlsx表中id)</param>
        /// </summary>
        public void ReciveRewardReq(int inviteId)
        {
            Rpc((int)action_config.ACTION_INVITE_GET_REWARD, inviteId);
        }

        /// <summary>
        /// 向服务器请求获取邀请记录列表
        /// </summary>
        public void QueryInviteRecordList()
        {
            Rpc((int)action_config.ACTION_INVITE_QUERY_LIST);
        }

        /// <summary>
        /// 分享链接成功上送请求
        /// </summary>
        public void ShareLinkOkReq()
        {
            Rpc((int)action_config.ACTION_INVITE_MARK_SHARED);
        }

        //激活码中FCode类型码请求
        public void FCodeReq(ulong fCodeNum)
        {
            PlayerAvatar.Player.RpcCall("f_code_req", fCodeNum);
        }

        private void Rpc(int msgId, params object[] args)
        {
            //LoggerHelper.Debug("PlayerAvatar.Player:" + PlayerAvatar.Player + ",MogoWorld.Player:" + (MogoWorld.Player as PlayerAccount));
            if (PlayerAvatar.Player != null)
            {
                PlayerAvatar.Player.ActionRpcCall("friend_invite_action_req", msgId, args);
            }
            else
            {//在创号|选角界面输入邀请码

                (MogoWorld.Player as PlayerAccount).ActionRpcCall("friend_invite_action_req", msgId, args);
            }
        }

        /// <summary>
        /// 获取验证码请求
        /// </summary>
        /// <param name="mobileId">手机号码</param>
        private LuaTable dataMobile = new LuaTable();
        public void GetCheckCode(ulong mobileId)
        {
            dataMobile.Clear();
            dataMobile.Add(1, mobileId);
            PlayerAvatar.Player.RpcCall("bind_mobile_action_req", action_config.ACTION_GET_VERIFICATION_CODE, dataMobile);
        }

        /// <summary>
        /// 绑定手机请求
        /// </summary>
        /// <param name="mobileId"></param>
        public void BindMobileReq(int verCode)
        {
            dataMobile.Clear();
            dataMobile.Add(1, verCode);
            PlayerAvatar.Player.RpcCall("bind_mobile_action_req", action_config.ACTION_BIND_MOBILE, dataMobile);
        }

        /// <summary>
        /// 获取绑定信息请求
        /// </summary>
        public void GetBindMobileInfoReq()
        {
            dataMobile.Clear();
            PlayerAvatar.Player.RpcCall("bind_mobile_action_req", action_config.ACTION_GET_BIND_MOBILE_INFO, dataMobile);
        }

        /// <summary>
        /// 获取绑定奖励请求
        /// </summary>
        public void GetBindRewardReq()
        {
            dataMobile.Clear();
            PlayerAvatar.Player.RpcCall("bind_mobile_action_req", action_config.ACTION_GET_BIND_REWARD, dataMobile);
        }

        public bool MobileBindable()
        {
            bool isVerify = pbBindMobileInfo != null && 
                ((pbBindMobileInfo.last_time > 0 && pbBindMobileInfo.next_get_time <= 0) || pbBindMobileInfo.last_time <= 0) &&
                (PlayerAvatar.Player.level > int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.bind_phone_open)));
            return isVerify;
        }

        /// <summary>
        /// 绑定手机结果
        /// </summary>
        /// <param name="msgId">消息ID</param>
        /// <param name="respCode">错误码</param>
        public void OnBindMobileResp(uint msgId, uint respCode, byte[] data)
        {
            LoggerHelper.Debug(string.Format("msgId:{0},respCode:{1}，data{2}", msgId, respCode, data));
            switch ((action_config)msgId)
            {
                case action_config.ACTION_GET_VERIFICATION_CODE:
                    EventDispatcher.TriggerEvent<PbVerificationCode>(WelfareActiveEvent.C_GET_CHECK_CODE, MogoProtoUtils.ParseProto<PbVerificationCode>(data));
                    break;
                case action_config.ACTION_BIND_MOBILE:
                    EventDispatcher.TriggerEvent(WelfareActiveEvent.C_BIND_MOBILE_RESULT);
                    break;
                case action_config.ACTION_GET_BIND_MOBILE_INFO:
                    pbBindMobileInfo = MogoProtoUtils.ParseProto<PbBindMobileInfo>(data);
                    //可验证状态: 玩家达到20级 && （已绑定且剩余天数为0 或 未绑定）
                    bool isVerify = MobileBindable();
                    EventDispatcher.TriggerEvent<PbBindMobileInfo>(WelfareActiveEvent.C_GET_BIND_MOBILE_INFO, pbBindMobileInfo);
                    EventDispatcher.TriggerEvent<bool>(WelfareActiveEvent.C_REFRESH1_REWARD_TIP, isVerify);
                    LoggerHelper.Info(string.Format("[OnBindMobileResp] isVerify={0},pbBindMobileInfo={1}", isVerify, pbBindMobileInfo));
                    break;
                case action_config.ACTION_GET_BIND_REWARD:
                    EventDispatcher.TriggerEvent(WelfareActiveEvent.C_BIND_REWARD_RESULT);
                    break;
            }
        }

        public void OnFCodeResp(uint respCode, byte[] data)
        {
           //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, SystemInfoUtil.GenerateContent((int)respCode), PanelIdEnum.MainUIField);
        }
    }
}
