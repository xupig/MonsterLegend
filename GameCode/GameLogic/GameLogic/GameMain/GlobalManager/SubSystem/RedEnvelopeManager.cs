﻿using Common.ServerConfig;
using System;
using System.Collections.Generic;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using GameData;
using Common.Data;
using Common.Chat;
using Common.Utils;
using GameMain.Entities;
using GameLoader.Utils.CustomType;
using MogoEngine.Events;
using Common.Events;

namespace GameMain.GlobalManager.SubSystem
{
    public class RedEnvelopeManager
    {
        private static RedEnvelopeManager _instance;
        public static RedEnvelopeManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RedEnvelopeManager();
                }
                return _instance;
            }
        }

        private RedEnvelopeData _redEnvelopeData;

        private RedEnvelopeManager()
        {
            _redEnvelopeData = PlayerDataManager.Instance.RedEnvelopeData;
        }

        public void OnRedEnvelopeNotifyResp(UInt32 redEnvelopeConfigId, byte[] byteArray)
        {
            PbRedEnvelopeLink pbLinkInfo = MogoProtoUtils.ParseProto<PbRedEnvelopeLink>(byteArray);
            pbLinkInfo.name = MogoProtoUtils.ParseByteArrToString(pbLinkInfo.name_bytes);
            SendRedEnvelopeToChat(pbLinkInfo);
        }

        public void OnRedEnvelopeNotifyListResp(byte[] byteArray)
        {
            PbRedEnvelopeLinkList pbLinkInfoList = MogoProtoUtils.ParseProto<PbRedEnvelopeLinkList>(byteArray);
            for (int i = 0; i < pbLinkInfoList.list.Count; i++)
            {
                PbRedEnvelopeLink pbLinkInfo = pbLinkInfoList.list[i];
                pbLinkInfo.name = MogoProtoUtils.ParseByteArrToString(pbLinkInfo.name_bytes);
                SendRedEnvelopeToChat(pbLinkInfo);
            }
        }

        private void SendRedEnvelopeToChat(PbRedEnvelopeLink pbLinkInfo)
        {
            red_envelope config = red_envelope_helper.GetConfig((int)pbLinkInfo.envelope_type_id);
            List<string> dataList = data_parse_helper.ParseListString(config.__Notice);
            if (dataList.Count > 0)
            {
                int systemInfoId = int.Parse(dataList[0]);
                system_info systemInfoConfig = system_info_helper.GetConfig(systemInfoId);
                ChatData chatData = PlayerDataManager.Instance.ChatData;
                List<string> dataList2 = data_parse_helper.ParseListString(systemInfoConfig.__type);
                for (int i = 0; i < dataList2.Count; i++)
                {
                    int type = int.Parse(dataList2[i]);
                    int channelId = SystemInfoManager.Instance.GetChatChannelId(type);
                    ChatRedEnvelopeWrapper redEnvelopLink = CreateRedEnvelopeWrapper(config.__Chat_name, systemInfoId, pbLinkInfo);
                    string content = chatData.CreateLinkMsg(redEnvelopLink);
                    ChatManager.Instance.SendClientChatMsg(channelId, content, (int)ChatContentType.RedEnvelope, pbLinkInfo.dbid, pbLinkInfo.name, 0, (int)pbLinkInfo.vocation);
                    RedEnvelopeInfo envelopeInfo = new RedEnvelopeInfo(pbLinkInfo, systemInfoId, config.__Type);
                    _redEnvelopeData.AddGrabRedEnvelope((int)pbLinkInfo.envelope_id, envelopeInfo);
                }
            }
        }

        private ChatRedEnvelopeWrapper CreateRedEnvelopeWrapper(int nameId, int systemInfoId, PbRedEnvelopeLink pbLinkInfo)
        {
            string name = MogoLanguageUtil.GetContent(nameId);
            ChatRedEnvelopeWrapper redEnvelopLink = new ChatRedEnvelopeWrapper(pbLinkInfo.envelope_id, name, pbLinkInfo.name, systemInfoId);
            return redEnvelopLink;
        }

        public void OnGrabRedEnvelopeActionResp(int actionId, LuaTable redEnvelopeTable)
        {
            if (actionId != (int)action_config.ACTION_RED_ENVELOPE_GRAB)
                return;
            if (redEnvelopeTable.Count == 0)
                return;
            if (redEnvelopeTable.Count > 1)
            {
                object[] redEnvelopeValues = new object[redEnvelopeTable.Count];
                redEnvelopeTable.Values.CopyTo(redEnvelopeValues, 0);
                int envelopeId = int.Parse(redEnvelopeValues[0].ToString());

                LuaTable rewardInfoTable = redEnvelopeValues[1] as LuaTable;
                string[] rewardIds = new string[rewardInfoTable.Count];
                rewardInfoTable.Keys.CopyTo(rewardIds, 0);
                object[] rewardNums = new object[rewardInfoTable.Count];
                rewardInfoTable.Values.CopyTo(rewardNums, 0);
                List<RewardData> rewardList = new List<RewardData>();
                for (int i = 0; i < rewardIds.Length; i++)
                {
                    int rewardId = int.Parse(rewardIds[i]);
                    int num = int.Parse(rewardNums[i].ToString());
                    RewardData rewardData = new RewardData(rewardId, num);
                    rewardList.Add(rewardData);
                }
                RedEnvelopeInfo envelopeInfo = _redEnvelopeData.GetRedEnvelope(envelopeId);
                if (envelopeInfo != null)
                {
                    envelopeInfo.rewardList = rewardList;
                    EventDispatcher.TriggerEvent<RedEnvelopeInfo>(ChatEvents.GRAB_RED_ENVELOPE, envelopeInfo);
                    int grabNum = 0;
                    if (redEnvelopeValues.Length >= 3)
                    {
                        grabNum = int.Parse(redEnvelopeValues[2].ToString());
                    }
                    SendPrivateChatMsgToPlayer(envelopeInfo, grabNum);
                }
            }
            else
            {
                object[] redEnvelopeValues = new object[redEnvelopeTable.Count];
                redEnvelopeTable.Values.CopyTo(redEnvelopeValues, 0);
                int envelopeId = int.Parse(redEnvelopeValues[0].ToString());
                RedEnvelopeInfo envelopeInfo = _redEnvelopeData.GetRedEnvelope(envelopeId);
                if (envelopeInfo != null)
                {
                    envelopeInfo.rewardList = null;
                    EventDispatcher.TriggerEvent<RedEnvelopeInfo>(ChatEvents.GRAB_RED_ENVELOPE, envelopeInfo);
                }
            }
        }

        private void SendPrivateChatMsgToPlayer(RedEnvelopeInfo envelopeInfo, int grabNum)
        {
            if (grabNum == 0)
                return;
            if (envelopeInfo.rewardList != null && envelopeInfo.rewardList.Count > 0)
            {
                if (envelopeInfo.playerDbid == PlayerAvatar.Player.dbid || envelopeInfo.playerDbid == 0)
                    return;
                string content = red_envelope_helper.GetPrivateChatNotice(envelopeInfo.envelopeConfigId, grabNum);
                if (!string.IsNullOrEmpty(content))
                {
                    ChatManager.Instance.RequestSendChat(public_config.CHANNEL_ID_PRIVATE, content, envelopeInfo.playerDbid);
                }
            }
        }

        public void RequestGrabRedEnvelope(int envelopeId)
        {
            RedEnvelopeRpc(action_config.ACTION_RED_ENVELOPE_GRAB, envelopeId);
        }

        public void RequestGetRedEnvelopeList()
        {
            RedEnvelopeRpc(action_config.ACTION_RED_ENVELOPE_GET_LIST);
        }

        private void RedEnvelopeRpc(action_config actionId, params object[] args)
        {
            PlayerAvatar.Player.ActionRpcCall("red_envelope_action_req", (int)actionId, args);
        }
    }
}
