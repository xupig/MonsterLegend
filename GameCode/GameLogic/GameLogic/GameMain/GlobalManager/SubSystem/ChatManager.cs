﻿#region 模块信息
/*==========================================
// 文件名：ChatManager
// 命名空间: GameMain.GlobalManager.SubSystem
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/30 9:36:59
// 描述说明：聊天数据管理
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameMain.Entities;


using GameLoader.Utils.CustomType;
using UnityEngine.Events;
using Common.Structs;
using GameData;
using MogoEngine.Events;
using Common.Events;
using Common.ServerConfig;
using GameLoader.Utils;
using Common.Data;
using UnityEngine;
using Common.Data.Chat;
using MogoEngine.Utils;
using Common.Structs.ProtoBuf;
using Common.Global;
using MogoEngine;
using ModuleCommonUI;
using Common.Utils;
using Common.Base;
using Common.Chat;
using System.Text.RegularExpressions;

namespace GameMain.GlobalManager.SubSystem
{
    public class ChatManager : Singleton<ChatManager> 
    {
        /// <summary>
        /// 只保存最近30条记录
        /// </summary>
        public const int MAX_CHAT_ITEM_COUNT = 30;
        public const int WORLD_CHAT_LEVEL_SETTING_ID = 30;

        private static LuaTable EMPTY_LUA_TABLE = new LuaTable();
        private static Regex LINK_NAME_PATTERN = new Regex(@"(?<=\[).*?(?=\,)");
        //在系统频道中显示获得物品的信息
        private static readonly string SYSTEM_GET_REWARD_ITEM_NAME_FORMAT = "<color={0}>{1}</color>";
        private static readonly string SYSTEM_GET_REWARD_ITEM_DESC_SPLIT_SIGN = "，";
     
        private Dictionary<UInt64, PbChatAvatarInfo> _addBlackListDict;
        private ChatData _chatData;
        private int _treasurePosShareCount = 0;

        public string voiceTestText = "";
        public uint curVoiceTestId = 0;

        public ChatManager()
            : base()
        {
            _chatData = PlayerDataManager.Instance.ChatData;
            _addBlackListDict = new Dictionary<ulong, PbChatAvatarInfo>();
        }

        public void ResponseChat(UInt32 actionId, UInt32 errorId, byte[] byteArr)
        {
            if(errorId == 0)
            {
                switch ((action_config)actionId)
                {
                    case action_config.CHAT_CHANNEL:
                        PbChatInfoResp chatInfo = MogoProtoUtils.ParseProto<PbChatInfoResp>(byteArr);
                        chatInfo.send_name = MogoProtoUtils.ParseByteArrToString(chatInfo.send_name_bytes);
                        chatInfo.accept_name = MogoProtoUtils.ParseByteArrToString(chatInfo.accept_name_bytes);
                        chatInfo.msg = MogoProtoUtils.ParseByteArrToString(chatInfo.msg_bytes);
                        chatInfo.voice_id = MogoProtoUtils.ParseByteArrToString(chatInfo.voice_id_bytes);
                        ProcessChatInfo(chatInfo);
                        break;
                    case action_config.CHAT_SHIELD_LIST:
                        AddBlackList(byteArr);
                        EventDispatcher.TriggerEvent(ChatEvents.BLACK_LIST_CHANGE);
                        break;
                    case action_config.CHAT_NEAR_LIST:
                        //_chatData.ChatPrivateData.pbRecentChatInfo = MogoProtoUtils.ParseProto<PbChatInfo>(byteArr);
                        break;
                    case action_config.CHAT_SHIELD_REQ:
                        EventDispatcher.TriggerEvent(FriendEvents.ADD_BALCK_LIST, AddBlackList(byteArr));
                        EventDispatcher.TriggerEvent(ChatEvents.BLACK_LIST_CHANGE);
                        break;
                    case action_config.CHAT_CANCEL_SHIELD_REQ:
                        EventDispatcher.TriggerEvent(FriendEvents.REMOVE_BLACK_LIST, RemoveBlackList(byteArr));
                        EventDispatcher.TriggerEvent(ChatEvents.BLACK_LIST_CHANGE);
                        break;
                }
                
            }
        }

        private void ProcessChatInfo(PbChatInfoResp chatInfo)
        {
            ProcessChatInfo(chatInfo, true);
        }

        private void ProcessChatInfo(PbChatInfoResp chatInfo, bool isAddToChannelAll)
        {
            if (_chatData.isShield(chatInfo.send_dbid))
                return;
            if (!DoBeforeAddToChatData(chatInfo))
                return;
            chatInfo = DoFilterChatContent(chatInfo);
            _chatData.AddChatInfo(chatInfo, isAddToChannelAll);
            _chatData.TeamEntryChatData.AddTeamChatInfo(chatInfo);
            if (chatInfo.channel_id == public_config.CHANNEL_ID_PRIVATE)
            {
                _chatData.ChatPrivateData.AddToPrivateChatData(chatInfo);
            }
            _chatData.ChatLinkData.AddChatLinkInfo(chatInfo);
            ProcessAddNewMsgFlag(chatInfo);
            if (chatInfo.msg_type == public_config.CHAT_MSG_TYPE_VOICE)
            {
                _chatData.AddChatVoiceDurationInfo(chatInfo);
                _chatData.AddPlayedVoiceInfo(chatInfo);
                _chatData.AddAutoPlayVoice(chatInfo);
            }
            EventDispatcher.TriggerEvent<int, PbChatInfoResp>(ChatEvents.RECEIVE_CONTENT, (int)chatInfo.channel_id, chatInfo);
            if (chatInfo.channel_id == public_config.CHANNEL_ID_TEAM)
            {
                EventDispatcher.TriggerEvent(TeamEvent.TEAM_RECEIVE_CHAT_CONTENT);
            }
            ProcessSendChatInfoSucceed(chatInfo);
        }

        private bool DoBeforeAddToChatData(PbChatInfoResp chatInfo)
        {
            //当发送宝箱分享链接返回结果后，如果发送给的是自己，就不需加到聊天数据中去显示
            if (chatInfo.channel_id == public_config.CHANNEL_ID_PRIVATE && chatInfo.send_dbid == PlayerAvatar.Player.dbid && chatInfo.accept_dbid == 0)
            {
                if (chatInfo.msg_type == (uint)ChatContentType.SharePosition)
                {
                    _treasurePosShareCount = _treasurePosShareCount > 0 ? --_treasurePosShareCount : 0;
                    EventDispatcher.TriggerEvent(TreasureEvent.ON_SHARE_SUCCEED, _treasurePosShareCount);
                    return false;
                }
            }
            return true;
        }

        private void ProcessSendChatInfoSucceed(PbChatInfoResp chatInfo)
        {
            if (chatInfo.send_dbid == PlayerAvatar.Player.dbid)
            {
                if (chatInfo.msg_type == (int)ChatContentType.Guild)
                {
                    EventDispatcher.TriggerEvent(GuildEvents.On_Guild_Recuit_Succeed);
                }
                else if (chatInfo.msg_type == (int)ChatContentType.Team)
                {
                   //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(83026), PanelIdEnum.MainUIField);
                }
                else if (chatInfo.msg_type == (uint)ChatContentType.SharePosition)
                {
                    _treasurePosShareCount = _treasurePosShareCount > 0 ? --_treasurePosShareCount : 0;
                    EventDispatcher.TriggerEvent(TreasureEvent.ON_SHARE_SUCCEED, _treasurePosShareCount);
                }
            }
        }

        private PbChatInfoResp DoFilterChatContent(PbChatInfoResp chatInfo)
        {
            if (!IsChatMsgNeedFilter(chatInfo))
            {
                return chatInfo;
            }
            chatInfo.msg = FilterContent(chatInfo.msg);
            return chatInfo;
        }

        private bool IsChatMsgNeedFilter(PbChatInfoResp chatInfo)
        {
            if (IsChatLinkMsg(chatInfo) || ChatInfoHelper.isSystem(chatInfo) || chatInfo.msg_type == (uint)ChatContentType.TipsMsg)
            {
                return false;
            }
            return true;
        }

        private void ProcessAddNewMsgFlag(PbChatInfoResp chatInfo)
        {
            ulong playerDbid = chatInfo.send_dbid;
            if (playerDbid == PlayerAvatar.Player.dbid || _chatData.IsSystemChannelMsg(chatInfo))
                return;
            _chatData.ProcessAddNewMsg((int)chatInfo.channel_id, playerDbid);
        }

        private UInt64 AddBlackList(byte[] byteArr)
        {
            PbChatInfo chatInfo = MogoProtoUtils.ParseProto<PbChatInfo>(byteArr);
            List<PbChatAvatarInfo> list = chatInfo.avatars_info;
           
            UInt64 dbid = 0;
            for (int i = 0; i < list.Count; i++)
            {
                dbid = list[i].dbid;
                list[i].name = MogoProtoUtils.ParseByteArrToString(list[i].name_bytes);
            }
            _chatData.AddBlackList(list);
            return dbid;
        }

        private UInt64 RemoveBlackList(byte[] byteArr)
        {
            List<PbChatAvatarInfo> list = MogoProtoUtils.ParseProto<PbChatInfo>(byteArr).avatars_info;
            UInt64 dbid = 0;
            for (int i = 0; i < list.Count; i++)
            {
                dbid = list[i].dbid;
            }
            _chatData.RemoveBlackList(list);
            return dbid;
        }

        /// <summary>
        /// 获取最近密聊列表
        /// </summary>
        /// <param name="_actionId">804</param>
        /// <param name="_senderDbid">查询人的dbid</param>
        /// <param name="pageId">0表示不分页</param>
        public void RequestRecentPrivateChat(int pageId = 0)
        {
            RPC(action_config.CHAT_NEAR_LIST, PlayerAvatar.Player.dbid, pageId);
        }
        
        public void GetBlackList()
        {
            RPC(action_config.CHAT_SHIELD_LIST);
        }

        private void RPC(action_config actionId, params object[] args)
        {
            PlayerAvatar.Player.ActionRpcCall("chat_action_req", (int)actionId, args);
        }

        public void RemoveFromBlackList(UInt64 dbId)
        {
            RPC(action_config.CHAT_CANCEL_SHIELD_REQ, dbId);
        }

        public void AddToBlackList(PbChatAvatarInfo chatAvatarInfo)
        {
            chatAvatarInfo.name = MogoProtoUtils.ParseByteArrToString(chatAvatarInfo.name_bytes);
            if(_addBlackListDict.ContainsKey(chatAvatarInfo.dbid) == false)
            {
                _addBlackListDict.Add(chatAvatarInfo.dbid, chatAvatarInfo);
            }
            RPC(action_config.CHAT_SHIELD_REQ, chatAvatarInfo.dbid);
        }

        /// <summary>
        /// 请求发送聊天
        /// </summary>
        public void RequestSendChat(int channel, string content, UInt64 playerId, byte contentType = (byte)ChatContentType.Text, params object[] paramList)
        {
            uint chatId = _chatData.CreateChatId();
            RequestSendChat(channel, chatId, content, playerId, "", contentType, paramList);
        }

        public void RequestSendChat(int channel, string content, string playerName, byte contentType = (byte)ChatContentType.Text, params object[] paramList)
        {
            uint chatId = _chatData.CreateChatId();
            RequestSendChat(channel, chatId, content, 0, playerName, contentType, paramList);
        }

        /// <summary>
        /// 请求发送聊天
        /// 注：私聊玩家的id或名字可以任选一个作为私聊时的参数
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="content"></param>
        /// <param name="playerId">用于私聊玩家的id</param>
        /// <param name="playerName">用于私聊玩家的名字</param>
        /// <param name="contentType"></param>
        /// <param name="paramList"></param>
        public void RequestSendChat(int channel, uint chatId, string content, UInt64 playerId, string playerName, byte contentType = (byte)ChatContentType.Text, params object[] paramList)
        {
            GMManager.GetInstance().ProcessCommand(content);
            if(PlayerAvatar.Player.level >= Convert.ToInt32(global_params_helper.GetGlobalParam(WORLD_CHAT_LEVEL_SETTING_ID)))
            {
                if (contentType == (byte)ChatContentType.Text)
                {
                    PlayerAvatar.Player.RpcCall("chat_req", channel, content, playerId, playerName, contentType, EMPTY_LUA_TABLE, chatId);
                }
                else if (contentType == (byte)ChatContentType.Item)
                {
                    ChatItemLinkWrapper[] linkList = paramList as ChatItemLinkWrapper[];
                    if (linkList != null && linkList.Length > 0)
                    {
                        LuaTable luaTable = new LuaTable();
                        int normalItemIndex = 0;
                        for (int i = 0; i < linkList.Length; i++)
                        {
                            if (linkList[i].itemLinkType == (int)ChatItemLinkWrapper.ItemLinkType.Normal)
                            {
                                normalItemIndex++;
                                LuaTable item = new LuaTable();
                                item.Add(1, linkList[i].bag);
                                item.Add(2, linkList[i].grid);
                                luaTable.Add(normalItemIndex, item);
                            }
                        }
                        PlayerAvatar.Player.RpcCall("chat_req", channel, content, playerId, playerName, contentType, luaTable, chatId);
                    }
                }
                else if (contentType == (byte)ChatContentType.Voice)
                {
                    string voiceId = (string)paramList[0];
                    LuaTable luaTable = new LuaTable();
                    luaTable.Add(1, voiceId);
                    PlayerAvatar.Player.RpcCall("chat_req", channel, content, playerId, playerName, contentType, luaTable, chatId);
                }
                else if (contentType == (byte)ChatContentType.TipsMsg)
                {
                    PlayerAvatar.Player.RpcCall("chat_req", channel, content, playerId, playerName, contentType, EMPTY_LUA_TABLE, chatId);
                }
                else
                {
                    PlayerAvatar.Player.RpcCall("chat_req", channel, content, playerId, playerName, contentType, EMPTY_LUA_TABLE, chatId);
                }
            }
            else
            {
               //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(32552), PanelIdEnum.Chat);
            }
        }

        public void RequestInviteLinkSendChat(int channel, params object[] args)
        {
            if (args != null && args.Length > 0)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    if (args[i] is ChatGuildLinkWrapper)
                    {
                        ChatGuildLinkWrapper guildLink = args[i] as ChatGuildLinkWrapper;
                        RequestSendChat(channel, _chatData.CreateGuildLinkMsg(guildLink), 0, (byte)ChatContentType.Guild);
                    }
                    else if (args[i] is ChatTeamLinkWrapper)
                    {
                        ChatTeamLinkWrapper teamLink = args[i] as ChatTeamLinkWrapper;
                        RequestSendChat(channel, _chatData.CreateTeamLinkMsg(teamLink), 0, (byte)ChatContentType.Team);
                    }
                    else if (args[i] is ChatSharePosLinkWrapper)
                    {
                        ChatSharePosLinkWrapper sharePosLink = args[i] as ChatSharePosLinkWrapper;
                        RequestSendChat(channel, _chatData.CreateSharePosLinkMsg(sharePosLink), 0, (byte)ChatContentType.SharePosition);
                    }
                    else if (args[i] is ChatViewLinkWrapper)
                    {
                        ChatViewLinkWrapper viewLink = args[i] as ChatViewLinkWrapper;
                        RequestSendChat(channel, _chatData.CreateViewLinkMsg(viewLink), 0, (byte)ChatContentType.View);
                    }
                }
            }
        }

        /// <summary>
        /// 请求同时发送多频道邀请链接内容
        /// </summary>
        /// <param name="channelList">频道id的列表</param>
        /// <param name="args">要发的链接相关数据，这几个频道的数据是使用相同的</param>
        public void RequestMultiChannelInviteLinkChat(Dictionary<int, object> channelLinkDataDict)
        {
            int treasurePosCount = 0;
            foreach (var item in channelLinkDataDict)
            {
                int channel = item.Key;
                if (item.Value is ChatSharePosLinkWrapper)
                {
                    treasurePosCount++;
                    RequestInviteLinkSendChat(channel, item.Value);
                }
            }
            if (_treasurePosShareCount == 0)
            {
                _treasurePosShareCount = treasurePosCount;
            }
        }

        public void RequestChatItemDetail(string itemLink)
        {
            PlayerAvatar.Player.RpcCall("chat_get_item_req", itemLink);
        }

        //发送客户端的聊天消息
        public void SendClientChatMsg(int channel, string content, int msgType = public_config.CHAT_MSG_TYPE_TEXT)
        {
            SendClientChatMsg(channel, content, msgType, PlayerAvatar.Player.dbid, PlayerAvatar.Player.name, PlayerAvatar.Player.level, (int)PlayerAvatar.Player.vocation);
        }

        public void SendClientChatMsg(int channel, string content, int msgType, ulong sendDbid, string sendName, int sendLevel, int sendVocation)
        {
            PbChatInfoResp chatInfo = new PbChatInfoResp();
            uint chatId = _chatData.CreateChatId();
            chatInfo.seq_id = chatId;
            chatInfo.channel_id = (uint)channel;
            chatInfo.send_dbid = sendDbid;
            chatInfo.send_name = sendName;
            chatInfo.send_level = (uint)sendLevel;
            chatInfo.send_vocation = (uint)sendVocation;
            chatInfo.accept_dbid = PlayerAvatar.Player.dbid;
            chatInfo.accept_name = PlayerAvatar.Player.name;
            chatInfo.accept_level = PlayerAvatar.Player.level;
            chatInfo.accept_vocation = (uint)PlayerAvatar.Player.vocation;
            chatInfo.msg = content;
            chatInfo.msg_type = (uint)msgType;
            ProcessChatInfo(chatInfo);
        }

        //发送客户端各频道系统消息
        public void SendClientChannelSystemMsg(int channel, string content, int msgType = public_config.CHAT_MSG_TYPE_TEXT, bool isAddToChannelAll = true)
        {
            if (channel == public_config.CHANNEL_ID_GUILD && PlayerAvatar.Player.guild_id == 0)
                return;
            if (channel == public_config.CHANNEL_ID_PRIVATE)
                return;
            PbChatInfoResp chatInfo = new PbChatInfoResp();
            uint chatId = _chatData.CreateChatId();
            chatInfo.seq_id = chatId;
            chatInfo.channel_id = (uint)channel;
            chatInfo.send_dbid = 0;
            chatInfo.msg = content;
            chatInfo.msg_type = (uint)msgType;
            chatInfo.accept_dbid = 0;
            ProcessChatInfo(chatInfo, isAddToChannelAll);
        }

        //仅在综合频道显示的提示消息，即消息的标题为"提示"
        public void SendClientTipsChatMsg(int channel, string content)
        {
            SendClientChannelSystemMsg(channel, content, public_config.CHAT_MSG_TYPE_TIPS);
        }

        public void SendClientTipsChatMsg(int channel, string content, bool isAddToChannelAll)
        {
            SendClientChannelSystemMsg(channel, content, public_config.CHAT_MSG_TYPE_TIPS, isAddToChannelAll);
        }

        //系统消息，消息的标题为“系统”
        public void SendClientSystemChatMsg(int channel, string content)
        {
            SendClientChannelSystemMsg(channel, content, public_config.CHAT_MSG_TYPE_SYSTEM);
        }

        private string FilterLinkMsg(string srcLinkMsg)
        {
            string destLinkMsg = srcLinkMsg;
            string linkName = LINK_NAME_PATTERN.Match(destLinkMsg).Value;
            string filterName = FilterContent(linkName);
            destLinkMsg = destLinkMsg.Replace(linkName, filterName);
            return destLinkMsg;
        }

        public void ResponseChatItemDetail(string itemLink, byte[] byteArr)
        {
            PbItemInfo pbItemInfo = MogoProtoUtils.ParseProto<PbItemInfo>(byteArr);
            if(pbItemInfo != null)
            {
                pbItemInfo.count = 0;
               //ari ToolTipsManager.Instance.ShowItemTip(ItemInfoCreator.CreateItemInfo(pbItemInfo), PanelIdEnum.Chat);
            }
        }

        //过滤敏感词
        public string FilterContent(string msg)
        {
            return _chatData.FilterWordsInfo.FilterContent(msg);
        }

        public bool HasNewChatInfo()
        {
            if (_chatData.CheckChannelHasNewMsg(public_config.CHANNEL_ID_TEAM) || 
                _chatData.CheckChannelHasNewMsg(public_config.CHANNEL_ID_GUILD) ||
                _chatData.CheckChannelHasNewMsg(public_config.CHANNEL_ID_PRIVATE))
            {
                return true;
            }
            return false;
        }

        public bool IsChatLinkMsg(PbChatInfoResp chatInfo)
        {
            if (chatInfo.msg_type == (int)ChatContentType.Guild || chatInfo.msg_type == (int)ChatContentType.Team || chatInfo.msg_type == (int)ChatContentType.SharePosition
                || chatInfo.msg_type == (int)ChatContentType.View || chatInfo.msg_type == (int)ChatContentType.TeamCaptainAccept || chatInfo.msg_type == (int)ChatContentType.Question)
            {
                return true;
            }
            return false;
        }

        public bool IsChatMsgContainsSendTime(PbChatInfoResp chatInfo)
        {
            return IsChatLinkMsg(chatInfo);
        }

        public bool IsChatMsgContainsSendTime(int msgType)
        {
            PbChatInfoResp chatInfo = new PbChatInfoResp();
            chatInfo.msg_type = (uint)msgType;
            return IsChatMsgContainsSendTime(chatInfo);
        }

        /// <summary>
        /// 判断聊天内容中是否存在链接内容
        /// 采用正则表达式进行链接匹配
        /// </summary>
        /// <param name="chatInfo"></param>
        /// <returns></returns>
        public bool IsChatMsgContainsLink(PbChatInfoResp chatInfo)
        {
            return ChatLinkData.LINK_CONTENT_PATTERN.IsMatch(chatInfo.msg);
        }

        public bool IsSendToSystemChannel(PbChatInfoResp chatInfo)
        {
            return _chatData.IsSystemChannelMsg(chatInfo);
        }

        //是否是系统类频道消息，比如提示频道、系统频道
        public bool IsSystemTypeChannelMsg(PbChatInfoResp chatInfo)
        {
            return chatInfo.msg_type == (int)ChatContentType.TipsMsg || IsSendToSystemChannel(chatInfo);
        }

        public bool IsCancelChatVoiceFunction()
        {
            return Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.WindowsEditor;  //
        }

        public void SendGetRewardMsgToSystemChannel(PbRewardList pbRewardList)
        {
            List<string> systemDescList = channel_helper.GetChannelSystemDesc(pbRewardList.get_way);
            if (systemDescList == null)
                return;
            if (pbRewardList.item_list.Count == 0)
                return;
            string destSystemDesc = MogoLanguageUtil.GetContent(systemDescList[0]);
            string compensateDesc = string.Empty;
            string compensateDescFormat = MogoLanguageUtil.GetContent(systemDescList[2]);

            string destItemDesc = string.Empty;
            string itemInfoContentFormat = MogoLanguageUtil.GetContent(systemDescList[1]);
            for (int i = 0; i < pbRewardList.item_list.Count; i++)
            {
                PbItemNoticeItem itemInfo = pbRewardList.item_list[i];
                string colorHexToken = item_helper.GetColorHexToken((int)itemInfo.id);
                string name = item_helper.GetName((int)itemInfo.id);
                name = string.Format(SYSTEM_GET_REWARD_ITEM_NAME_FORMAT, colorHexToken, name);
                if (itemInfo.compensate_rate > 0)
                {
                    int compensateNum = Mathf.FloorToInt(itemInfo.compensate_rate * 100f);
                    compensateDesc = string.Format(compensateDescFormat, compensateNum.ToString() + "%");
                    destItemDesc += string.Format(itemInfoContentFormat, name, itemInfo.count) + compensateDesc;
                }
                else
                {
                    destItemDesc += string.Format(itemInfoContentFormat, name, itemInfo.count);
                }
                if (i < pbRewardList.item_list.Count - 1)
                {
                    destItemDesc += SYSTEM_GET_REWARD_ITEM_DESC_SPLIT_SIGN;
                }
            }
            destSystemDesc += destItemDesc;
            SendClientTipsChatMsg(public_config.CHANNEL_ID_WORLD, destSystemDesc, false);
        }

        public void OnChatQuestionNotifyResp(int questionId)
        {
            _chatData.ChatQuestionData.UpdateQuestionInfo(questionId);
            SendClientChannelSystemMsg(public_config.CHANNEL_ID_WORLD, _chatData.ChatQuestionData.CurQuestionInfo.ChatDesc, (int)ChatContentType.Question);
        }
    }
}
