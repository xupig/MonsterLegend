﻿#region 模块信息
/*==========================================
// 文件名：SingleInstanceManger
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem.SystemInfo
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/7/14 16:27:42
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs.ProtoBuf;
using Common.Utils;
using System.Collections.Generic;
using GameData;
using GameMain.Entities;
using Common.Data;
using MogoEngine;
using GameLoader.Utils.CustomType;
using Common.ServerConfig;
using MogoEngine.Utils;
using MogoEngine.Events;
using Common.Events;
using Common.Base;
using Common.Structs;

namespace GameMain.GlobalManager.SubSystem
{

    public class SingleInstanceManager : Singleton<SingleInstanceManager>
    {
        public SingleInstanceData _singleInstanceData;

        public SingleInstanceManager()
        {
            _singleInstanceData = PlayerDataManager.Instance.SingleInstanceData;
        }

        public void GetChapterInfo()
        {
            MogoWorld.Player.ActionRpcCall("single_mission_action_req", (int)action_config.ACTION_SINGLE_MISSION_INFO);
        }

        private int _instanceId;
        public void RequestEnterInstance(int instanceId)
        {
            _instanceId = instanceId;
            CopyLogicManager.Instance.RequestEnterCopy(ChapterType.PetDefence, RequestEnterCopy, instanceId);
        }

        private void RequestEnterCopy()
        {
            MogoWorld.Player.ActionRpcCall("single_mission_action_req", (int)action_config.ACTION_SINGLE_MISSION_CHALLENGE, _instanceId);
        }

        public void RequestSweepInstance(int instanceId)
        {
            MogoWorld.Player.ActionRpcCall("single_mission_action_req", (int)action_config.ACTION_SINGLE_MISSION_MOP_UP, instanceId);
        }

        public void RequestByInstanceCount(int chapterId)
        {
            MogoWorld.Player.ActionRpcCall("single_mission_action_req", (int)action_config.ACTION_SINGLE_MISSION_BUY_COUNT, chapterId);
        }

        public void RequestGetChapterInfo(int chapterId)
        {
            MogoWorld.Player.ActionRpcCall("mission_action_req", (int)action_config.GET_APPOINT_CHAPTER_RECORD, chapterId);
        }

        public void RequesetGetInstanceState(int chapterId)
        {
            MogoWorld.Player.ActionRpcCall("single_mission_action_req", (int)action_config.ACTION_SINGLE_MISSION_STATE, chapterId);
        }

        public List<RewardData> GetRewardDataListByInstanceId(int instanceId)
        {
            List<RewardData> result = new List<RewardData>();
            const int BossType = 3;
            foreach (KeyValuePair<int, monster_value> pair in XMLManager.monster_value)
            {
                monster_value value = pair.Value;
                if (value.__inst_id == instanceId && value.__monster_type == BossType)
                {
                    result.AddRange(item_reward_helper.GetRewardDataList(value.__reward_id, PlayerAvatar.Player.vocation));
                }
            }
            List<int> rewardList = instance_reward_helper.GetAllRewardIdList(instanceId);
            for (int i = 0; i < rewardList.Count; i++)
            {
                result.AddRange(item_reward_helper.GetRewardDataList(rewardList[i], PlayerAvatar.Player.vocation));
            }
            HashSet<int> hasSet = new HashSet<int>();
            for (int i = result.Count - 1; i >= 0; i--)
            {
                if (hasSet.Contains(result[i].id))
                {
                    result.RemoveAt(i);
                }
                else
                {
                    hasSet.Add(result[i].id);
                }
            }
            return result;
        }

        internal void OnSingleMissionResp(uint actionId, uint errorId, byte[] bytes)
        {
            switch ((action_config)actionId)
            {
                case action_config.ACTION_SINGLE_MISSION_INFO:
                    PbSingleMissionInfo pbSingleMissionInfo = MogoProtoUtils.ParseProto<PbSingleMissionInfo>(bytes);
                    SetChapterInfo(pbSingleMissionInfo);
                    EventDispatcher.TriggerEvent<int>(SingleInstanceEvents.BEGIN_REFRESH, 1);
                    break;
                case action_config.ACTION_SINGLE_MISSION_CHALLENGE:
                    break;
                case action_config.ACTION_SINGLE_MISSION_MOP_UP:
                    PbMopUpInfo PbMopUpInfo = MogoProtoUtils.ParseProto<PbMopUpInfo>(bytes);
                    EventDispatcher.TriggerEvent<PbMopUpInfo>(SingleInstanceEvents.OPEN_SWEEP_PANEL, PbMopUpInfo);
                    _singleInstanceData.UpdateChapterEnterCounts(PbMopUpInfo);
                    break;
                case action_config.ACTION_SINGLE_MISSION_STATE:
                    PbSingleMissionStateList pbSingleMissionStateList = MogoProtoUtils.ParseProto<PbSingleMissionStateList>(bytes);
                    _singleInstanceData.UpdateSingleMissionState(pbSingleMissionStateList);
                    EventDispatcher.TriggerEvent(SingleInstanceEvents.BEGIN_REFRESH, 2);
                    break;
                case action_config.ACTION_SINGLE_MISSION_BUY_COUNT:
                    PbSingChapterInfo pbSingChapter = MogoProtoUtils.ParseProto<PbSingChapterInfo>(bytes);
                    _singleInstanceData.UpdateChapterInfo(pbSingChapter);
                    break;
            }
        }

        private void SetChapterInfo(PbSingleMissionInfo pbSingleMissionInfo)
        {
            _singleInstanceData.UpdateAllChapterInfo(pbSingleMissionInfo);
        }

        public static void RequestGetBuyCount(int priceId)
        {
            MogoWorld.Player.ActionRpcCall("price_action_req", (int)action_config.ACTION_GET_PRICE_BUY_COUNT, priceId);
        }

        public void OnPriceResp(uint actionId, uint errorId, byte[] byteArr)
        {
            PbPriceList pbInfoList = MogoProtoUtils.ParseProto<PbPriceList>(byteArr);
            for (int i = 0; i < pbInfoList.price_info.Count; i++)
            {
                PbPriceInfo pb = pbInfoList.price_info[i];
                if (_singleInstanceData.ContainsPriceId((int)pb.price_id))
                {
                    SingleInstanceManager.Instance._singleInstanceData.UpdatePriceDict(pbInfoList.price_info[i]);
                }
            }
        }

        public void RequesetBuyChapterCount(int chapterId)
        {
            MogoWorld.Player.ActionRpcCall("single_mission_action_req", (int)action_config.ACTION_SINGLE_MISSION_BUY_COUNT, chapterId);
        }
    }
}