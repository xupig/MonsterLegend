﻿using Common.Base;
using Common.Data;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using GameMain.GlobalManager.SubSystem;
using ModuleCommonUI;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameMain.GlobalManager.SubSystem
{
    public class VipManager : Singleton<VipManager> 
    {

        private static VipDataManager vipDataManager;
        private int vipLevel;
        
        public VipManager()
        {
            vipDataManager = PlayerDataManager.Instance.VipData;
        }

        public void RequestGetRewards(int vipLevel)
        {
            this.vipLevel = vipLevel;
            action_config.ACTION_VIP_GET_REWARDS.Rpc("vip_action_req", vipLevel);
        }

        public void RequestGetRewardsRecord()
        {
            action_config.ACTION_VIP_GET_RECORD.Rpc("vip_action_req");
        }

        public void OnVIPResp(UInt32 action_id, UInt32 error_id, byte[] byteArray)
        {
            action_config actionEnum = (action_config)action_id;
            error_code errorCode = (error_code)error_id;
            switch (actionEnum)
            {
                case action_config.ACTION_VIP_GET_RECORD:
                    OnRewardsRecordResp(errorCode, byteArray);
                    break;
                case action_config.ACTION_VIP_GET_REWARDS:
                    OnGetRewardsResp(errorCode, byteArray);
                    break;
            }
        }

        private void OnRewardsRecordResp(error_code errorCode, byte[] byteArray)
        {
            switch (errorCode)
            {
                case error_code.ERR_SUCCESSFUL:
                    PbVIPRewardRecord pbVIPRewardRecord = MogoProtoUtils.ParseProto<PbVIPRewardRecord>(byteArray);
                    vipDataManager.RefleshVipData(pbVIPRewardRecord);
                    EventDispatcher.TriggerEvent(VipEvents.ON_RECEIVE_VIP_REWARD_RECORD);
                    if (pbVIPRewardRecord.vip_record.Count > 0)
                    {
                        EventDispatcher.TriggerEvent<List<PbVIPRewardInfo>>(RewardEvents.GET_VIP_GIFT, pbVIPRewardRecord.vip_record);
                    }
                    break;
                default:
                    SystemInfoManager.Instance.Show(errorCode);
                    break;
            }
        }

        private void OnGetRewardsResp(error_code errorCode, byte[] byteArray)
        {
            switch (errorCode)
            {
                case error_code.ERR_SUCCESSFUL:
                    RequestGetRewardsRecord();
                    //MessageBox.Show(false, MessageBox.DEFAULT_TITLE, "获得VIP奖励");
                    UIManager.Instance.ShowPanel(PanelIdEnum.ItemObtain, vip_helper.GetVipReward(this.vipLevel));
                    break;
                default:
                    SystemInfoManager.Instance.Show(errorCode);
                    break;
            }
        }

    }
}
