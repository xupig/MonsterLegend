﻿using System;
using GameMain.Entities;

using MogoEngine.Events;
using Common.Events;
using Common.ServerConfig;
using Common.Data;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using Common.Utils;
using Common.Base;
using UnityEngine;
using ModuleCommonUI;
using Mogo.Util;
using GameData;


namespace GameMain.GlobalManager.SubSystem
{
    public class FieldTaskManager : Singleton<FieldTaskManager> 
    {
        public FieldTaskManager()
        {
            //EventDispatcher.AddEventListener<int>(TaskEvent.TASK_FINISH, OnTaskFinsh);
            EventDispatcher.AddEventListener<int>(TaskEvent.TASK_REACHED, OnTaskReached);
        }

        private void OnTaskReached(int taskId)
        {
            if (task_data_helper.GetTaskType(taskId) != public_config.TASK_TYPE_WILD) { return; }
            if (PlayerDataManager.Instance.TaskData.IsAutoWildTask)
            {
                FinshTask();
            }
        }

        //private void OnTaskFinsh(int taskId)
        //{
        //    if (task_data_helper.GetTaskType(taskId) != public_config.TASK_TYPE_WILD) { return; }
        //    if (PlayerDataManager.Instance.TaskData.IsAutoWildTask)
        //    {
        //        waitingNextTask = true;
        //    }
        //}

        private void RPC(action_config actionId, params object[] args)
        {
            //Debug.LogError("rpc: " + (action_config)actionId);
            PlayerAvatar.Player.ActionRpcCall("wild_task_req", (int)actionId, args);
        }

        public void QueryTaskInfo()
        {
            RPC(action_config.ACTION_WILD_TASK_QUERY_INFO);
        }

        public void AcceptTask()
        {
            RPC(action_config.ACTION_WILD_TASK_ACCEPT);
        }

        public void AskAcceptTeamTask(int agree)
        {
            RPC(action_config.ACTION_WILD_TASK_ASK_ACCEPT_TEAM, agree);
        }

        public void AbandonTask()
        {
            if (PlayerDataManager.Instance.TaskData.IsAutoWildTask)
            {
                PlayerActionManager.GetInstance().StopAllAction();
                PlayerDataManager.Instance.TaskData.IsAutoWildTask = false;
                EventDispatcher.TriggerEvent(FieldTaskEvents.ON_AUTO_FIELD_TASK_STATE_CHANGE);
            }
            OnAbandonTask();
        }

        private void OnAbandonTask()
        {
            RPC(action_config.ACTION_WILD_TASK_ABANDON);
        }

        public void OnWildTaskResp(uint action_id, uint errorId, byte[] byteArr)
        {
            //Debug.LogError("resp: " + (action_config)action_id);
            switch ((action_config)action_id)
            {
                case action_config.ACTION_WILD_TASK_QUERY_INFO:
                    OnQueryWildTaskInfo(byteArr);
                    break;
                case action_config.ACTION_WILD_TASK_ACCEPT:
                    OnAcceptTask();
                    break;
                case action_config.ACTION_WILD_TASK_ASK_ACCEPT_TEAM:
                    OnAskAcceptTeam();
                    break;
                case action_config.ACTION_WILD_TASK_LEADER_REFUSE_TASK:
                    OnRefuseTeamTask();
                    break;
            }
        }

        private void OnRefuseTeamTask()
        {
            MogoUtils.FloatTips(31141);
        }

        private void OnAskAcceptTeam()
        {
            string content = MogoLanguageUtil.GetContent(31140);
      //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, AgreeAcceptTeamTask, DisagreeAcceptTeamTask);
        }

        private void AgreeAcceptTeamTask()
        {
            AskAcceptTeamTask(1);
        }

        private void DisagreeAcceptTeamTask()
        {
            AskAcceptTeamTask(0);
        }

        private void OnAcceptTask()
        {
            PanelIdEnum.Task.Close();
        }

        //private bool waitingNextTask = false;
        private void OnQueryWildTaskInfo(byte[] byteArr)
        {
            PbWildTaskInfo taskInfo = MogoProtoUtils.ParseProto<PbWildTaskInfo>(byteArr);
            taskInfo.wild_task_id = wild_task_helper.GetWildTaskIdByTaskId(taskInfo.task_id);
            PlayerDataManager.Instance.TaskData.WildTaskInfo = taskInfo;
            EventDispatcher.TriggerEvent(FieldTaskEvents.ON_GET_FIELD_TASK_INFO);
            GotoNextTask();
        }

        private void GotoNextTask()
        {
            if (PlayerDataManager.Instance.TaskData.IsAutoWildTask)
            {
                GotoWildTask();
            }
        }

        public void FinshTask()
        {
            PbWildTaskInfo wildTaskInfo = PlayerDataManager.Instance.TaskData.WildTaskInfo;
            if (wildTaskInfo != null && wildTaskInfo.task_id != 0)
            {
                TaskManager.Instance.FinishTask((uint)wildTaskInfo.task_id);
            }
        }

        public void GotoWildTask()
        {
            PbWildTaskInfo wildTaskInfo = PlayerDataManager.Instance.TaskData.WildTaskInfo;
            if (wildTaskInfo != null && wildTaskInfo.task_id != 0)
            {
                task_data taskData = task_data_helper.GetTaskData(wildTaskInfo.task_id);
                UIManager.Instance.ClosePanel(PanelIdEnum.Task);
                //ari TaskFollowHandle.TaskFollow(taskData);
            }
        }

        public void ShowAutoTaskMessageBox()
        {
            string content = MogoLanguageUtil.GetContent(31142);
            //ari MessageBox.Show(true, MessageBox.DEFAULT_TITLE, content, CancelAutoTask, AutoWildTask, MogoLanguageUtil.GetContent(31143), MogoLanguageUtil.GetContent(31144));
        }

        private static void CancelAutoTask()
        {
            PlayerDataManager.Instance.TaskData.IsAutoWildTask = false;
            FieldTaskManager.Instance.GotoWildTask();
            EventDispatcher.TriggerEvent(FieldTaskEvents.ON_AUTO_FIELD_TASK_STATE_CHANGE);
        }

        private static void AutoWildTask()
        {
            if (PlayerAutoFightManager.GetInstance().fightType == FightType.FOLLOW_FIGHT)
            {
                PlayerAutoFightManager.GetInstance().Stop();
                TeamManager.Instance.CancelFollowCaptain();
            }
            PlayerDataManager.Instance.TaskData.IsAutoWildTask = true;
            FieldTaskManager.Instance.GotoWildTask();
            EventDispatcher.TriggerEvent(FieldTaskEvents.ON_AUTO_FIELD_TASK_STATE_CHANGE);
        }
    }
}
