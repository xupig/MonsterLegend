﻿#region 模块信息
/*==========================================
// 文件名：SpellManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/4/22 14:20:26
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameLoader.Utils;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class SpellManager : Singleton<SpellManager> 
    {
        //已学习 【groupId,id】spell.xml
        //技能槽 【pos_i,id】 spell.xml

        public void SpellLearn(int spellId)
        {
            action_config.ACTION_SPELL_LEARN.Rpc("spell_action_req", ConvertToOrignal(spellId));
        }

        public void SpellUpgrade(int spellId)
        {
            action_config.ACTION_SPELL_UPGRADE.Rpc("spell_action_req", ConvertToOrignal(spellId));
        }

        public void SpellEquip(int spellId)
        {
            Debug.Log("SpellEquip:"+spellId);
            action_config.ACTION_SPELL_EQUIP.Rpc("spell_action_req", ConvertToOrignal(spellId));
        }

        public int ConvertToOrignal(int spellId)
        {
            spell spell = spell_helper.GetSkill(spellId);
            spell_sys sys = skill_helper.GetSpellSysyByGroupId(spell.__group);
            spell orignalSpell = skill_helper.GetSpell(sys.__before_proficient, spell.__sp_level);
            return orignalSpell.__id;
        }

        public void SpellProficient(int groupId)
        {
            action_config.ACTION_SPELL_PROFICIENT.Rpc("spell_action_req", groupId);
        }

        public void GetLearnedSpells()
        {
            action_config.ACTION_SPELL_GET_LEARNED_SPELLS.Rpc("spell_action_req");
        }

        public void GetSpellSlots()
        {
            action_config.ACTION_SPELL_GET_SPELL_SLOTS.Rpc("spell_action_req");
        }

        //public void GetSpellProficientList()
        //{
        //    action_config.ACTION_SPELL_GET_SPELL_PRIFICIENT.Rpc("spell_action_req");
        //}


        public void OnSpellResp(UInt32 actionId,UInt32 errorId,byte[] bytes)
        {
            
            action_config acyionConfig = (action_config)actionId;
            switch(acyionConfig)
            {
                case action_config.ACTION_SPELL_LEARN:
                    OnLearnSpellResp(bytes);
                    break;
                case action_config.ACTION_SPELL_UPGRADE:
                    OnLearnSpellResp(bytes);
                    break;
                case action_config.ACTION_SPELL_EQUIP:
                    OnEquipSkill(bytes);
                    break;
                case action_config.ACTION_SPELL_PROFICIENT:
                    OnSpellProficientChange(bytes);
                    break;
                case action_config.ACTION_SPELL_GET_LEARNED_SPELLS:
                    OnGetLearnedSpellList(bytes);
                    break;
                case action_config.ACTION_SPELL_GET_SPELL_SLOTS:
                    OnGetSpellSlotList(bytes);
                    break;
                //case action_config.ACTION_SPELL_GET_SPELL_PRIFICIENT:
                //    OnSpellProficientChange(bytes);
                //    break;
                default:
                    break;
            }
        }

        public void EntitySpellSlotsResp(byte[] bytes)
        {
            PbEntitySpellSlots slotRecord = MogoProtoUtils.ParseProto<PbEntitySpellSlots>(bytes);
            uint entityID = slotRecord.ent_id;
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(entityID);
            if (creature == null) return;
            List<int> learnedSkillList = new List<int>();
            for (int i = 0; i < slotRecord.spell_slots.Count; i++)
            {
                int spellID = (int)slotRecord.spell_slots[i];
                learnedSkillList.Add(spellID);
            }
            creature.skillManager.SetSlottedSkillList(learnedSkillList);
        }

        public void EntityLearnedSpellResp(byte[] bytes)
        {
            PbEntityLearnedSpells learnRecord = MogoProtoUtils.ParseProto<PbEntityLearnedSpells>(bytes);
            uint entityID = learnRecord.ent_id;
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(entityID);
            if (creature == null) return;
            var slottedSkillList = creature.skillManager.GetSlottedSkillList();
            List<int> learnedSkillList = new List<int>();
            for (int i = 0; i < learnRecord.learned_spells.Count; i++)
            {
                int spellID = (int)learnRecord.learned_spells[i];
                if (spell_helper.GetShowType(spellID) == 0 && !slottedSkillList.Contains(spellID)) { continue; }
                learnedSkillList.Add(spellID);
            }
            creature.skillManager.AddLearnedSkillList(learnedSkillList);
        }

        private void OnSpellProficientChange(byte[] bytes)
        {
            PbSpellSlotInfo slotInfo = MogoProtoUtils.ParseProto<PbSpellSlotInfo>(bytes);
            PlayerDataManager.Instance.SpellData.EquipSkill(slotInfo);
            EventDispatcher.TriggerEvent(SpellEvents.SPELL_SLOT_LIST_CHANGE);
            EventDispatcher.TriggerEvent(SpellEvents.SPELL_UPDATE_EQUIPED_SKILL); 
            //PbProficientIdRecord record = MogoProtoUtils.ParseProto<PbProficientIdRecord>(bytes);
           // PlayerDataManager.Instance.SpellData.OnSpellProficientChange(record.proficient_info);
        }

        private void OnGetSpellSlotList(byte[] bytes)
        {
            PbSpellSlotsRecord slotRecord = MogoProtoUtils.ParseProto<PbSpellSlotsRecord>(bytes);
            PlayerDataManager.Instance.SpellData.UpdateSlotSkills(slotRecord.spell_slot_info);
            EventDispatcher.TriggerEvent(SpellEvents.SPELL_SLOT_LIST_CHANGE);
        }

        private void OnEquipSkill(byte[] bytes)
        {
            PbSpellSlotInfo slotInfo = MogoProtoUtils.ParseProto<PbSpellSlotInfo>(bytes);
            PlayerDataManager.Instance.SpellData.EquipSkill(slotInfo);
            EventDispatcher.TriggerEvent(SpellEvents.SPELL_SLOT_LIST_CHANGE);
            EventDispatcher.TriggerEvent(SpellEvents.SPELL_UPDATE_EQUIPED_SKILL);
        }

        private void OnGetLearnedSpellList(byte[] bytes)
        {
            PbLearnedSpellsRecord spellRecord = MogoProtoUtils.ParseProto<PbLearnedSpellsRecord>(bytes);
            PlayerDataManager.Instance.SpellData.InitLeanedSkill(spellRecord.learned_spell_info);
            EventDispatcher.TriggerEvent(SpellEvents.SPELL_LIST_INIT);
        }

        private void OnLearnSpellResp(byte[] bytes)
        {
            PbLearnedSpellsInfo spellInfo = MogoProtoUtils.ParseProto<PbLearnedSpellsInfo>(bytes);
            int position = PlayerDataManager.Instance.SpellData.UpdateLearnSkill(spellInfo);
            if(position != -1)
            {
                EventDispatcher.TriggerEvent(SpellEvents.SPELL_CHANGE, position);
            }
            
        }
    }
}
