﻿using Common.Data;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameLoader.Utils.CustomType;
using GameMain.Entities;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class ChargeActivityManager : Singleton<ChargeActivityManager>
    {
        private static PlayerAvatar _player;

        public ChargeActivityManager()
        {
            _player = PlayerAvatar.Player;
        }

        public void RequesetAllInfo()
        {
            _player.RpcCall("charge_activity_req", (int)action_config.ACTION_CHARGE_ACTIVITY_QUERY_INFO_LIST, 0, new LuaTable());
        }

        public void RequesetInfo(int activityId)
        {
            _player.RpcCall("charge_activity_req", (int)action_config.ACTION_CHARGE_ACTIVITY_QUERY_INFO, activityId, new LuaTable());
        }

        public void RequestGetReward(int activityId)
        {
            _player.RpcCall("charge_activity_req", (int)action_config.ACTION_CHARGE_ACTIVITY_REWARD, activityId, new LuaTable());
        }

        public void ResponseInfo(byte[] byteArray)
        {
            PbChargeActivityTypeOne pb = MogoProtoUtils.ParseProto<PbChargeActivityTypeOne>(byteArray);
            PlayerDataManager.Instance.ChargeActivityData.UpdateSingleActivityData(pb);
        }

        public void ResponseAllInfo(byte[] byteArray)
        {
            PbChargeActivityQueryList pb = MogoProtoUtils.ParseProto<PbChargeActivityQueryList>(byteArray);
            PlayerDataManager.Instance.ChargeActivityData.UpdateActivityData(pb);
        }

     
    }
}
