﻿using System;
using GameMain.Entities;

using MogoEngine.Events;
using Common.Events;
using Common.ServerConfig;
using Common.Data;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using Common.Utils;
using Common.Base;
using GameData;
using MogoEngine.Mgrs;
using Common.ClientConfig;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class TutorManager : Singleton<TutorManager> 
    {
        public bool canGetReward;
        public bool noticeBuyTutorShopItem;

        public void Init()
        {
            CheckGreenPoint();
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.tutor_level, CheckGreenPoint);
        }

        private void CheckGreenPoint()
        {
            canGetReward = tutor_helper.CanGetReward();
            noticeBuyTutorShopItem = tutor_helper.CanBuyTutorShopItem();
            if (canGetReward || noticeBuyTutorShopItem)
            {
                PlayerDataManager.Instance.FunctionData.AddFunctionPoint(FunctionId.tutor);
            }
            else
            {
                PlayerDataManager.Instance.FunctionData.RemoveFunctionPoint(FunctionId.tutor);
            }
            EventDispatcher.TriggerEvent(TutorEvents.REFRESH_GREEN_POINT);
            RefreshTutorNotice();
        }

        private void RefreshTutorNotice()
        {
            EventDispatcher.TriggerEvent<PanelIdEnum>(FastNoticeEvents.REMOVE_FAST_NOTICE, PanelIdEnum.Token);
            if (noticeBuyTutorShopItem)
            {
                SystemInfoManager.Instance.ShowFastNotice(52966);
            }
        }

        private void RPC(action_config actionId, params object[] args)
        {
            PlayerAvatar.Player.ActionRpcCall("tutor_req", (int)actionId, args);
        }

        public void GetReward(int level)
        {
            RPC(action_config.ACTION_TUTOR_GET_REWARD, level);
        }

        public void GetLogs()
        {
            RPC(action_config.ACTION_TUTOR_GET_LOGS);
        }

        public void OnTutorResp(UInt32 action_id, UInt32 errorId, byte[] byteArr)
        {
            switch ((action_config)action_id)
            {
                case action_config.ACTION_TUTOR_GET_REWARD:
                    break;
                case action_config.ACTION_TUTOR_GET_LOGS:
                    PbTutorEventInfoList pbList = MogoProtoUtils.ParseProto<PbTutorEventInfoList>(byteArr);
                    EventDispatcher.TriggerEvent<PbTutorEventInfoList>(TutorEvents.REFRESH_TUTOR_LOGS, pbList);
                    break;
            }
        }


    }
}
