﻿using Common.Data;
using Common.Events;
using Common.Structs;
using GameData;
using GameMain.Entities;
using GameMain.GlobalManager;
using MogoEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameMain.GlobalManager.SubSystem
{
    public class EquipPointManager
    {
        private Dictionary<EquipSubTab, BaseEquipPoint> _equipPointDic;

        public EquipPointManager()
        {
            _equipPointDic = new Dictionary<EquipSubTab, BaseEquipPoint>();
            _equipPointDic.Add(EquipSubTab.strengthen, new StrengthenPoint());
            _equipPointDic.Add(EquipSubTab.enchant, new EnchantPoint());
            _equipPointDic.Add(EquipSubTab.decompose, new DecomposePoint());
            _equipPointDic.Add(EquipSubTab.Smelt, new SmeltPoint());
            _equipPointDic.Add(EquipSubTab.Recast, new RecastPoint());
            _equipPointDic.Add(EquipSubTab.Suit, new SuitPoint());
            _equipPointDic.Add(EquipSubTab.RisingStar, new RisingStarPoint());
            //_equipPointDic.Add(EquipSubTab.

            EventDispatcher.AddEventListener(EquipEvents.REFRESH_EQUIP_POINT, RefreshPointList);
        }

        private void RefreshPointList()
        {
            if (IsHintStrengthenEntry() == true || EnchantPointSet.Count != 0 || SuitPointSet.Count != 0)
            {
                PlayerDataManager.Instance.FunctionData.AddFunctionPoint(FunctionId.equip);
            }
            else
            {
                PlayerDataManager.Instance.FunctionData.RemoveFunctionPoint(FunctionId.equip);
            }
            if (CanSmelt() == true || CanRecast() == true)
            {
                PlayerDataManager.Instance.FunctionData.AddFunctionPoint(FunctionId.smelter);
            }
            else
            {
                PlayerDataManager.Instance.FunctionData.RemoveFunctionPoint(FunctionId.smelter);
            }
        }

        public HashSet<int> StrengthenPointSet
        {
            get
            {
                return (_equipPointDic[EquipSubTab.strengthen] as StrengthenPoint).Data as HashSet<int>;
            }
        }

        public HashSet<int> EnchantPointSet
        {
            get
            {
                return (_equipPointDic[EquipSubTab.enchant] as EnchantPoint).Data as HashSet<int>;
            }
        }

        public HashSet<int> SuitPointSet
        {
            get
            {
                return (_equipPointDic[EquipSubTab.Suit] as SuitPoint).Data as HashSet<int>;
            }
        }

        public HashSet<int> RisingStarSet
        {
            get
            {
                return (_equipPointDic[EquipSubTab.RisingStar] as RisingStarPoint).Data as HashSet<int>;
            }
        }

        public HashSet<int> SmeltPointSet
        {
            get
            {
                return (_equipPointDic[EquipSubTab.Smelt] as SmeltPoint).Data as HashSet<int>;
            }
        }

        public bool Decompose
        {
            get
            {
                return (bool)(_equipPointDic[EquipSubTab.decompose] as DecomposePoint).Data;
            }
        }

        public bool CanSmelt()
        {
            return (_equipPointDic[EquipSubTab.Smelt] as SmeltPoint).HaveSmeltPoint;
        }

        public bool CanRecast()
        {
            return (bool)(_equipPointDic[EquipSubTab.Recast] as RecastPoint).Data;
        }

        public void RemovePoint(EquipSubTab equipSubTab, object data = null)
        {
            _equipPointDic[equipSubTab].RemovePoint(data);
        }

        public int GetMinStrengthenLevelPosition()
        {
            List<int> strengthenPointList = StrengthenPointSet.ToList();
            int minStrengthenLevel = int.MaxValue;
            int minStrengthenPosition = -1;
            for (int i = 0; i < strengthenPointList.Count; i++)
            {
                EquipStrengthenItemInfo strengthenItemInfo = PlayerDataManager.Instance.EquipStrengthenData.GetEquipStrengthenInfo(strengthenPointList[i]);
                if (minStrengthenLevel > strengthenItemInfo.StrengthenLevel)
                {
                    minStrengthenLevel = strengthenItemInfo.StrengthenLevel;
                    minStrengthenPosition = strengthenItemInfo.EquipItemInfo.GridPosition;
                }
            }

            return minStrengthenPosition;
        }

        public void IncreaseStrengthenPointNeedStrengthenTimes()
        {
            (_equipPointDic[EquipSubTab.strengthen] as StrengthenPoint).IgnoreStrengthenPointTimes += 1;
        }

        public void ResetStrengthenPointNeedStrengthenTimes()
        {
            (_equipPointDic[EquipSubTab.strengthen] as StrengthenPoint).IgnoreStrengthenPointTimes = 0;
        }

        public bool IsHintStrengthenEntry()
        {
            int minStrengthenPosition = GetMinStrengthenLevelPosition();

            if (minStrengthenPosition != -1)
            {
                return strengthen_helper.CanStrengthenManyTimes(minStrengthenPosition, (_equipPointDic[EquipSubTab.strengthen] as StrengthenPoint).IgnoreStrengthenPointTimes * 5 + 5);
            }
            return false;
        }

        
    }

}
