﻿#region 模块信息
/*==========================================
// 文件名：FightForceChannelManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/9/8 15:43:03
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Utils;
using Common.Structs;
using Common.Base;
using MogoEngine.Events;
using Common.Data;
using Common.Events;
using GameData;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using System;
using GameMain.Entities;
using System.Collections.Generic;
using UnityEngine;
namespace GameMain.GlobalManager.SubSystem
{
    public class FightForceChannelManager : Singleton<FightForceChannelManager>
    {

        public void Init()
        {

        }

        public FightForceChannelManager()
        {
            EventDispatcher.AddEventListener<int, CopyResult>(CopyEvents.Result, OnInstanceFinished);
            EventDispatcher.AddEventListener<PanelIdEnum, object>(BaseModule.GetShowEventName(PanelIdEnum.MainUIField), ShowFightForceView);
        }

        private void ShowFightForceView(PanelIdEnum arg1, object arg2)
        {
            if (EnteringMainTownNeedShow == true && isShowFightForceView == true && PlayerDataManager.Instance.FunctionData.IsFunctionOpen(FunctionId.fightForceIncrease))
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.FightForceChannel);
                EnteringMainTownNeedShow = false;
            }
        }

        private void OnInstanceFinished(int instanceId, CopyResult copyResult)
        {
            if (copyResult == CopyResult.Lose)
            {
                EnteringMainTownNeedShow = true;
            }
        }

        private bool isShowFightForceView = true;
        private bool hasEventAdded = true;

        public bool IsShowFightForceView
        {
            get
            {
                return isShowFightForceView;
            }
            set
            {
                isShowFightForceView = value;
                if (value == false && hasEventAdded == true)
                {
                    EventDispatcher.RemoveEventListener<int, CopyResult>(CopyEvents.Result, OnInstanceFinished);
                    EventDispatcher.RemoveEventListener<PanelIdEnum, object>(BaseModule.GetShowEventName(PanelIdEnum.MainUIField), ShowFightForceView);
                    EventDispatcher.RemoveEventListener<PanelIdEnum, object>(BaseModule.GetShowEventName(PanelIdEnum.MainUIField), ShowFightForceView);
                    hasEventAdded = false;
                }
                else if(value == true && hasEventAdded == false)
                {
                    EventDispatcher.AddEventListener<int, CopyResult>(CopyEvents.Result, OnInstanceFinished);
                    EventDispatcher.AddEventListener<PanelIdEnum, object>(BaseModule.GetShowEventName(PanelIdEnum.MainUIField), ShowFightForceView);
                    EventDispatcher.AddEventListener<PanelIdEnum, object>(BaseModule.GetShowEventName(PanelIdEnum.MainUIField), ShowFightForceView);
                    hasEventAdded = true;
                }
            }
        }
        
        private bool EnteringMainTownNeedShow = false;
    }
}