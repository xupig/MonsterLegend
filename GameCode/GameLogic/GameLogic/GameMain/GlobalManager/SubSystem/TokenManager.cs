﻿#region 模块信息
/*==========================================
// 文件名：TokenManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/11 14:16:57
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameLoader.Utils.CustomType;
using ModuleCommonUI;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class TokenManager : Singleton<TokenManager> 
    {
        public void OnTokenShopResp(UInt32 actionId, UInt32 errorId, byte[] bytes)
        {
            if ((action_config)actionId == action_config.ACTION_TOKEN_SHOP_GET_ITEM_LIMIT)
            {
                if (_hasInitData == false)
                {
                    _hasInitData = true;
                    PbTokenShop shop = MogoProtoUtils.ParseProto<PbTokenShop>(bytes);
                    PlayerDataManager.Instance.TokenData.FillDaliyCount(shop);
                    EventDispatcher.TriggerEvent(TokenEvents.INIT);
                }
            }
            else if((action_config)actionId == action_config.ACTION_TOKEN_SHOP_EXCHANGE)
            {
                TokenShopExchangeResp(bytes);
            }
            else if ((action_config)actionId == action_config.ACTION_TOKEN_SHOP_GET_TOTAL_LIMIT_ALL)
            {
                PbTokenTotalInfoList totalInfoList = MogoProtoUtils.ParseProto<PbTokenTotalInfoList>(bytes);
                PlayerDataManager.Instance.TokenData.FillTotalCount(totalInfoList);
                EventDispatcher.TriggerEvent(TokenEvents.REFRESH_TOTAL_COUNT);
            }
            else if ((action_config)actionId == action_config.ACTION_TOKEN_SHOP_GET_TOTAL_LIMIT_SINGLE)
            {
                PbTokenTotalInfo totalInfo = MogoProtoUtils.ParseProto<PbTokenTotalInfo>(bytes);
                PlayerDataManager.Instance.TokenData.UpdateTotalCount(totalInfo);
                EventDispatcher.TriggerEvent(TokenEvents.REFRESH_TOTAL_COUNT);
            }
        }

        public void TokenShopExchangeResp(byte[] bytes)
        {
            PbTokenShopItem item = MogoProtoUtils.ParseProto<PbTokenShopItem>(bytes);
            PlayerDataManager.Instance.TokenData.UpdateTokenShopItem(item);
            PanelIdEnum.QuickBuy.Close();
        }

        public void TokenExchange(int shop_item_id,int count)
        {
            action_config.ACTION_TOKEN_SHOP_EXCHANGE.Register();
            RPC(action_config.ACTION_TOKEN_SHOP_EXCHANGE, shop_item_id, count);
        }

        private bool _hasInitData = false;

        public void GetTokenShopItemLimit()
        {
            if (_hasInitData == false)
            {
                RPC(action_config.ACTION_TOKEN_SHOP_GET_ITEM_LIMIT);
            }
        }

        public void GetTokenShopToTalLimit()
        {
            RPC(action_config.ACTION_TOKEN_SHOP_GET_TOTAL_LIMIT_ALL);
        }

        private void RPC(action_config actionId, params object[] args)
        {
            MogoWorld.Player.ActionRpcCall("token_shop_action_req", (int)actionId, args);
        }

        /*
        public override void ActionResponse(int _actionId, int _errorId, params object[] _param)
        {
            switch((action_config)_actionId)
            {
                case action_config.ACTION_TOKEN_SHOP_EXCHANGE:
                    LuaTable table = _param[0] as LuaTable;
                    object[] objArr = new object[table.Count];
                    table.Values.CopyTo(objArr, 0);
                    PbTokenShopItem item = new PbTokenShopItem();
                    item.item_id = (uint)objArr[0];
                    item.surplus_count = (uint)objArr[1];
                    PlayerDataManager.Instance.TokenData.UpdateTokenShopItem(item);
                    EventDispatcher.TriggerEvent(TokenEvents.UPDATE, item);
                    break;
                default:
                    break;
            }
        }
        */ 

    }
}
