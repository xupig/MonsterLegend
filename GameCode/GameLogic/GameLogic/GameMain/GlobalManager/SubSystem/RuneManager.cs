﻿/*********************************************************
 * 开发人员：巩靖
 * 创建时间：2014/12/22 10:15:47
 * 描述说明：
 * *******************************************************/

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs;
using Common.Structs.ProtoBuf;
using GameLoader.Utils;
using ModuleCommonUI;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using Common.ExtendComponent;
using Common.Utils;
using UnityEngine;
using GameData;
using GameMain.Entities;
using Common.Global;

namespace GameMain.GlobalManager.SubSystem
{

    public class RuneManager : Singleton<RuneManager> 
    {

        //public const int RUNE_MAX_NUM = 125;

        public static DateTime START_TIME = new DateTime(1970, 1, 1, 0, 0, 0);

        public Dictionary<int, PbRuneWishInfo> runeDataDic;

        //ari public static RuneUpgradeData levelUpData = new RuneUpgradeData();

        public RuneManager()
            : base()
        {
            runeDataDic = new Dictionary<int, PbRuneWishInfo>();

            EventDispatcher.AddEventListener<BagType>(BagEvents.Init, UpdateBags);
        }

        public static bool NotSelectedRune(int pos)
        {
            return true;//ari  (pos != levelUpData.pos || levelUpData.bagType != BagType.RuneBag);
        }

        public void RPC(action_config actionId, params object[] args)
        {

            MogoWorld.Player.ActionRpcCall("rune_action_req", (int)actionId, args);
        }

        public void ListRPC(action_config actionId,int[] data)
        {

            MogoWorld.Player.ListRpcCall("rune_action_req", (int)actionId, data);
        }

       

        private void UpdateBags(BagType pkg)
        {
            if (pkg == BagType.RuneBag)
            {
                EventDispatcher.TriggerEvent(RuneEvent.RUNE_BAG_ALL_CHANGE);
            }
            else if (pkg == BagType.BodyRuneBag)
            {
                EventDispatcher.TriggerEvent(RuneEvent.RUNE_BODY_BAG_ALL_CHANGE);
            }
        }



        /// <summary>
        /// 一键排序,整理
        /// </summary>
        public void OnSortButtonUp()
        {
            RPC(action_config.ACTION_RUNE_AUTO_SORT, 1);
        }

        /// <summary>
        /// 一键合成符文
        /// </summary>
        public void OnCombineButtonUp(int startPos,int endPos)
        {
            RPC(action_config.ACTION_RUNE_AUTO_COMBINE, startPos,endPos);
        }

        /// <summary>
        /// 刷新符文
        /// </summary>
        public void OnRefreshRune(int id)
        {
            action_config.ACTION_RUNE_REFRESH.Register();
            RPC(action_config.ACTION_RUNE_REFRESH, id);
        }


        /// <summary>
        /// 使用符文
        /// </summary>
        public void OnRuneUse(int pos)
        {
            RPC(action_config.ACTION_RUNE_USE, pos);
        }

        /// <summary>
        /// 穿上符文
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="body_pos"></param>
        public void OnRunePutOn(int pos ,int body_pos)
        {
            RPC(action_config.ACTION_RUNE_PUT_ON,  pos, body_pos );
        }

        /// <summary>
        /// 脱下符文
        /// </summary>
        /// <param name="body_pos"></param>
        /// <param name="pos"></param>
        public void OnRunePutDown(int body_pos,int pos)
        {
            RPC(action_config.ACTION_RUNE_PUT_DOWN,  body_pos,pos);
        }

        /// <summary>
        /// 符文锁定
        /// </summary>
        public void OnRuneLock(int pos, BagType bagType)
        {
            RPC(action_config.ACTION_RUNE_LOCK, pos,(int)bagType);
        }

        /// <summary>
        /// 符文解锁
        /// </summary>
        public void OnRuneUnlock(int pos, BagType bagType)
        {
            RPC(action_config.ACTION_RUNE_UNLOCK, pos,(int)bagType);
        }


        /// <summary>
        /// 升级背包符文
        /// </summary>
        /// <param name="posList"></param>
        public void LevelUpBagRune(int[] posList)
        {
            action_config.ACTION_RUNE_UPDATE.Register();
            ListRPC(action_config.ACTION_RUNE_UPDATE, posList);
        }

        /// <summary>
        /// 升级身体背包符文
        /// </summary>
        /// <param name="posList"></param>
        public void LevelUpBodyBagRune(int[] posList)
        {
            action_config.ACTION_RUNE_UPDATE_BODY.Register();
            ListRPC(action_config.ACTION_RUNE_UPDATE_BODY, posList);
        }

        public void GetWishInfo()
        {
            RPC(action_config.ACTION_RUNE_WISH_INFO);
        }

        #region 服务器调用客户端远程方法
        /// <summary>
        /// 购买金币
        /// </summary>
        public void OnBuyVowTimeViaGold()
        {
            LoggerHelper.Debug("OnButVowTimeViaGold");
        }

        /// <summary>
        /// 购买钻石
        /// </summary>
        public void OnBuyVowTimeViaDiamond()
        {
            LoggerHelper.Debug("OnButVowTimeViaDiamond");
        }

        /// <summary>
        /// 购买卷轴
        /// </summary>
        public void OnBuyVowTimeViaScroll()
        {
            LoggerHelper.Debug("OnButVowTimeViaScroll");
        }


        public void WishInfoRuneResp(byte[] byteArr)
        {
            PbWishInfoList bagList = MogoProtoUtils.ParseProto<PbWishInfoList>(byteArr);
            int count = bagList.rune_wish_info.Count;
            for (int i = 0; i < count;i++ )
            {
                PbWishInfo wishInfo = bagList.rune_wish_info[i];
                if (runeDataDic.ContainsKey(wishInfo.location_id))
                {
                    runeDataDic[wishInfo.location_id] = wishInfo.rune_wish_info;
                }
                else
                {
                    runeDataDic.Add(wishInfo.location_id, wishInfo.rune_wish_info);
                }
            }
            _hasInitial = true;
            EventDispatcher.TriggerEvent(RuneEvent.WISH_INFO_CHANGE);
        }

        private Dictionary<int, Dictionary<int, rune_random>> wishData;
        private int level = 0;
        private bool _hasInitial;
        public bool CheckRune()
        {
            if (_hasInitial == false)
            {
                return false;
            }
            if (wishData == null)
            {
                level = PlayerAvatar.Player.level;
                wishData = rune_helper.GetWishData();
            }
            else if (level != PlayerAvatar.Player.level)
            {
                level = PlayerAvatar.Player.level;
                wishData = rune_helper.GetWishData();
            }

            foreach (KeyValuePair<int, Dictionary<int, rune_random>> kvp in wishData)
            {
                int _location = GetLocation(kvp.Value);
                uint lastUseTime = RuneManager.Instance.GetLastUseTime(_location);
                int remainTimes = RuneManager.Instance.GetRemainTimes(_location);
                int _max = GlobalParams.GetRuneLocationDailyTimes(_location);
                int _cd = GlobalParams.GetRuneLocationCd(_location);
                if (lastUseTime != 0 && remainTimes < _max)
                {
                    long left = (long)_cd * 1000 - (long)Global.serverTimeStamp + (long)lastUseTime * 1000;
                    if (left < 0)
                    {
                        return true;
                    }
                }
                else if (remainTimes < _max)
                {
                    return true;
                }
            }
            return false;
        }

        private int GetLocation(Dictionary<int, rune_random> dic)
        {
            foreach (KeyValuePair<int, rune_random> kvp in dic)
            {
                return kvp.Value.__location;
            }
            return 0;
        }


        public void UpdateRuneLock(byte bagType,byte pos, SByte locakState)
        {
            RuneItemInfo info = PlayerDataManager.Instance.BagData.GetBagData((BagType)bagType).GetAllItemInfo()[pos] as RuneItemInfo;
            info.RuneLock = locakState;
            EventDispatcher.TriggerEvent<BagType, int, RuneItemInfo>(RuneEvent.RUNE_BAG_ITEM_CHANGE, (BagType)bagType, pos, info);
        }


        public void OnRuneResp(UInt32 actionId, UInt32 errorId, byte[] byteArr)
        {
            if (errorId != 0)
            {
                if ((error_code)errorId == error_code.ERR_RUNE_LEVEL_LIMIT)
                {
                    EventDispatcher.TriggerEvent(RuneEvent.RUNE_LEVEL_UP_VIEW_UPDATE);
                }
                return;
            }
            switch ((action_config)actionId)
            {
                case action_config.ACTION_RUNE_UPDATE:
                case action_config.ACTION_RUNE_UPDATE_BODY:
                    EventDispatcher.TriggerEvent(RuneEvent.RUNE_LEVEL_UP_VIEW_UPDATE);
                    break;
                case action_config.ACTION_RUNE_REFRESH:
                    PbRuneItemInfoList list = MogoProtoUtils.ParseProto<PbRuneItemInfoList>(byteArr);
                    OnRuneRefreshResp(list, actionId);
                    break;
                case action_config.ACTION_RUNE_PUT_ON:
                    PbRunePutOn putOn = MogoProtoUtils.ParseProto<PbRunePutOn>(byteArr);
                    UIManager.Instance.ClosePanel(PanelIdEnum.RuneToolTips);
                    EventDispatcher.TriggerEvent(RuneEvent.RUNE_PUT_ON, (int)putOn.body_pos);
                    break;
                case  action_config.ACTION_RUNE_PUT_DOWN:
                    UIManager.Instance.ClosePanel(PanelIdEnum.RuneToolTips);
                    break;
                default:
                    break;
            }
        }

        #endregion

        private void OnRuneRefreshResp(PbRuneItemInfoList list, UInt32 actionId)
        {
            UIManager.Instance.ShowPanel(PanelIdEnum.ItemObtain, list);
        }


        #region 共有方法

        

        public uint GetLastUseTime(int location)
        {
            if (runeDataDic.ContainsKey(location))
            {
                return runeDataDic[location].wish_last_time;
            }
            return 0;
        }

        public int GetRemainTimes(int location)
        {
            if (runeDataDic.ContainsKey(location))
            {
                return runeDataDic[location].wish_count;
            }
            return 0;
        }


        public void SetLastUseTime(int location,uint value)
        {
            if (runeDataDic.ContainsKey(location))
            {
                runeDataDic[location].wish_last_time = value;
            }
        }

        public void ZeroClockResp()
        {
            GetWishInfo();
        }

        #endregion

    }
}
