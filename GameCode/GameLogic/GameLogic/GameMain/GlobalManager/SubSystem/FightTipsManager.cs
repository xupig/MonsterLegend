﻿#region 模块信息
/*==========================================
// 文件名：FightTipsManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/8/27 16:44:05
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.ClientConfig;
using Common.Global;
using Common.Utils;
using GameData;
using GameLoader.Utils.Timer;
using GameMain.Entities;
using MogoEngine.Mgrs;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class FightForeceHistoryData
    {
        public DateTime time;
        public int fightForce;

        public FightForeceHistoryData(DateTime time, int fight)
        {
            this.time = time;
            fightForce = fight;
        }
    }

    public class FightTipsManager : Singleton< FightTipsManager>
    {
        public static int equipChangePos = -1;
        private int oldFightForce = 0;
        private bool delayShow = false;
        private uint _timerId;
        // 储存最近的三次战斗力变化数据，GM指令使用
        private Queue<FightForeceHistoryData> _historyData;
        private int fightTipsCdTime = 3000;

        public FightTipsManager()
        {
            fightTipsCdTime = global_params_helper.GetFightTipsCdTime();
            _historyData = new Queue<FightForeceHistoryData>();
            oldFightForce = (int)PlayerAvatar.Player.fight_force;
            EntityPropertyManager.Instance.AddEventListener(PlayerAvatar.Player, EntityPropertyDefine.fight_force, ShowFightTips);
        }

        private void ShowFightTips()
        {
            SaveDataToHistory();

            if ((Time.realtimeSinceStartup - SpaceLeftTimeManager.Instance.enterSpaceTime) * 1000 < fightTipsCdTime && (PlayerAvatar.Player.fight_force < oldFightForce))
            {
                UpdateOldFightForce();
                return;
            }
            if (oldFightForce == 0)
            {
                UpdateOldFightForce();
                return;
            }
            if (delayShow == true)
            {
                return;
            }
            else if(oldFightForce != PlayerAvatar.Player.fight_force)
            {
                ShowTips();
               // UIManager.Instance.ShowPanel(PanelIdEnum.FightTips, oldFightForce);
                UpdateOldFightForce();
            }
        }

        public string GetFightForceHistory()
        {
            StringBuilder result = new StringBuilder();
            FightForeceHistoryData[] historyArrays =  _historyData.ToArray();
            for (int i = 0; i < historyArrays.Length; i++)
            {
                result.Append(string.Format("Time : {0}   {1}\n", historyArrays[i].time, historyArrays[i].fightForce));
            }
            return result.ToString();
        }

        private void SaveDataToHistory()
        {
            if (_historyData.Count >= 3)
            {
                _historyData.Dequeue();
            }
            _historyData.Enqueue(new FightForeceHistoryData(DateTime.Now, (int)PlayerAvatar.Player.fight_force));
        }

        public void ShowTipsNow()
        {
            delayShow = false;
            if (oldFightForce != PlayerAvatar.Player.fight_force)
            {

                ShowTips();
                if (PlayerAvatar.Player.fight_force > oldFightForce)
                { //IOS平台弹并且战力提升的情况下：出五星好评
                    PlayerAvatar.Player.showAstarHaoPing(LocalName.AstarPJ_RoleLevelUp);
                }
            }
            UpdateOldFightForce();
        }

        private void ShowTips()
        {
            /*
            if(EquipFightForceTips.isShow)
            {
                equipChangePos = -1;
                return;
            }
            if(PlayerAvatar.Player.fight_force > oldFightForce && equipChangePos!=-1)
            {
                if(!(CGDialogPanel.isShow || GuidePanel.isShow || FunctionPanel.isShow || PCFingerGuidePanel.isShow || FingerGuidePanel.isShow))
                {
                    EquipFightForceTips.isShow = true;
                    PanelIdEnum.EquipFightForceTips.Show(new int[] { equipChangePos, oldFightForce });
                }
                //UIManager.Instance.ShowPanel(PanelIdEnum.FightTips, oldFightForce);
                equipChangePos = -1;
            }
            else
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.FightTips, oldFightForce);
            }*/
        }

        private void UpdateOldFightForce()
        {
            oldFightForce = (int)PlayerAvatar.Player.fight_force;
        }

        public void SetDelayShow(bool isDelayShow)
        {
            ResetTimer();
            delayShow = isDelayShow;
            oldFightForce = (int)PlayerAvatar.Player.fight_force;
            _timerId = TimerHeap.AddTimer(5000, 0, SetDelayShowFalse);
        }

        private void SetDelayShowFalse()
        {
            delayShow = false;
        }

        private void ResetTimer()
        {
            if (_timerId != 0)
            {
                TimerHeap.DelTimer(_timerId);
                _timerId = 0;
            }
        }
     }
}