﻿#region 模块信息
/*==========================================
// 文件名：FriendManager
// 命名空间: GameMain.GlobalManager.SubSystem
// 创建者：LYX
// 修改者列表：
// 创建日期：2014/12/30 9:36:59
// 描述说明：好友数据管理
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameMain.Entities;


using GameLoader.Utils.CustomType;
using UnityEngine.Events;
using Common.Structs;
using GameData;
using MogoEngine.Events;
using Common.Events;
using Common.ServerConfig;
using GameLoader.Utils;
using Common.Data;
using UnityEngine;
using Common.Data.Chat;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using Common.Global;
using Common.Base;
using ModuleCommonUI;
using Common.Utils;
using MogoEngine;
using GameLoader.Utils.Timer;

namespace GameMain.GlobalManager.SubSystem
{
    public class FriendManager : Singleton<FriendManager> 
    {
        #region 方便书写

        private FriendData m_friendData;

        private Dictionary<action_config, Action<byte[]>> delegateDict;
        private Dictionary<action_config, Action<byte[]>> errorDict;
        private Dictionary<action_config, ReRequestParams> timeOutReRequestDict;

        private HashSet<UInt64> applySet;
        //private Dictionary<UInt64, PbFriendAvatarInfo> applyDict;
        private Dictionary<UInt64, PbFriendAvatarInfo> acceptDic;

        private static uint CHECK_TIME_INTERVAL = 3000;

        #endregion

        private PlayerInfoParam _curPlayerInfoParam;

        public FriendManager( ) :base()
        {
            m_friendData = PlayerDataManager.Instance.FriendData;
            delegateDict = new Dictionary<action_config, Action<byte[]>>();
            errorDict = new Dictionary<action_config, Action<byte[]>>();
            timeOutReRequestDict = new Dictionary<action_config, ReRequestParams>();
            applySet = new HashSet<ulong>();
            acceptDic = new Dictionary<ulong, PbFriendAvatarInfo>();
            Init();
        }

        private void Init()
         {
             Register(action_config.FRIEND_ADD,FriendAddResp);
             Register(action_config.FRIEND_ADDED, GetFriendAddedResp);
             Register(action_config.FRIEND_GET_P_INFO, GetFriendInfo);
             Register(action_config.FRIEND_GET_PS_INFO, GetFriendListResp);
             Register(action_config.CHAT_NEAR_LIST, GetNearListResp);
             Register(action_config.FRIEND_SEARCH, FriendSearchResp);
             Register(action_config.FRIEND_GET_PLAYER_DETAILED_INFO, GetPlayerDetailInfo);
             Register(action_config.FRIEND_GET_SEND_LIST, FriendSendList);
             Register(action_config.FRIEND_GET_RECEIVE_LIST, FriendReceiveList);
             Register(action_config.FRIEND_GET_BLESS_LIST, FriendRewardedList);
             Register(action_config.FRIEND_ACCEPT, FriendAccept);
             Register(action_config.FRIEND_ACCEPTED, FriendAccepted);
             Register(action_config.FRIEND_REFUSE, FriendRefuse);
             Register(action_config.FRIEND_REFUSEED, FriendRefused);
             Register(action_config.FRIEND_DEL, FriendDel);
             Register(action_config.FRIEND_DELED, FriendDel);
             Register(action_config.FRIEND_GIVE_INVITE, FriendGiveInvite);
             Register(action_config.FRIEND_GIVE_ACCEPT, FriendGiveAccept);
             Register(action_config.FRIEND_GET_BLESS_REWARD, GetBlessReward);
            // Register(action_config.FRIEND_INTIMATE_EVENT, FriendIntimateEventResp);
             Register(action_config.FRIEND_GIVE_ACCEPTED, ReceiveBless);
             Register(action_config.FRIEND_GIVE_GOODS, OnGiveGoodsResp);
             Register(action_config.FRIEND_BUY_AND_GIVE_GOODS, OnGiveGoodsResp);
             Register(action_config.FRIEND_GET_ONLINE_LIST, OnGetOnlineFriendListResp);
             Register(action_config.FRIEND_STATE_NOTICE,OnFriendOnLineNotice);
             Register(action_config.FRIEND_ADDED_LIST, OnGetApplicationListResp);
             Register(action_config.FRIEND_GET_FRIEND_DEGREE, OnUpdateDegree);
             Register(action_config.FRIEND_GET_RECOMMEND, FriendRecommend);
             Error(action_config.FRIEND_ADD, OnFriendAddError);
             Error(action_config.FRIEND_ACCEPT, OnFriendAcceptError);
            // private Dictionary<UInt64, PbFriendAvatarInfo> applyDict;
         //private Dictionary<UInt64, PbFriendAvatarInfo> acceptDic;
         }

        private void Register(action_config actionId, Action<byte[]> handler)
        {
            delegateDict.Add(actionId,handler);
        }

        private void RegisterTimeOutReRequest(action_config actionId, params object[] args)
        {
            if (timeOutReRequestDict.ContainsKey(actionId) == false)
            {
                timeOutReRequestDict.Add(actionId, new ReRequestParams(3, args));
                TimerHeap.AddTimer<action_config>(CHECK_TIME_INTERVAL, 0, CheckTimeOutDic, actionId);
            }
            else
            {
                timeOutReRequestDict[actionId] = new ReRequestParams(3, args);
            }
        }

        private void UnRegisterTimeOutReRequest(action_config actionId)
        {
            if (timeOutReRequestDict.ContainsKey(actionId) == true)
            {
                timeOutReRequestDict.Remove(actionId);
            }
        }

        private void CheckTimeOutDic(action_config actionId)
        {
            if (timeOutReRequestDict.ContainsKey(actionId) == true && timeOutReRequestDict[actionId].LeftRequestTime > 0)
            {
                RPC(actionId, timeOutReRequestDict[actionId].RPCParams);
                timeOutReRequestDict[actionId].LeftRequestTime--;
                TimerHeap.AddTimer<action_config>(CHECK_TIME_INTERVAL, 0, CheckTimeOutDic, actionId);
            }
            if (timeOutReRequestDict.ContainsKey(actionId) == true && timeOutReRequestDict[actionId].LeftRequestTime <= 0)
            {
                UnRegisterTimeOutReRequest(actionId);
            }
        }


        private bool RPC(action_config actionId, params object[] args)
        {
            PlayerAvatar.Player.ActionRpcCall("friend_action_req", (int)actionId, args);
            return true;
        }

        private void Error(action_config actionId, Action<byte[]> error)
        {

        }

        public bool IsInApplying(ulong dbid)
        {
            return applySet.Contains(dbid);
        }


        #region RPC请求


        private PbFriendAvatarInfo _lastQueryPlayerInfo; 

        public void GetPlayerDetailInfo(string name, PlayerInfoParam playerInfoParam = null)
        {
            //if (_lastQueryPlayerInfo == null || _lastQueryPlayerInfo.name != name)
            //{
            //    Debug.Log("GetPlayerDetailInfo:"+name);
            _curPlayerInfoParam = playerInfoParam;
            RPC(action_config.FRIEND_GET_PLAYER_DETAILED_INFO, name);
            //}
            //else
            //{
            //    if (playerInfoParam != null)
            //    {
            //        playerInfoParam.avatarInfo = _lastQueryPlayerInfo;
            //        UIManager.Instance.ShowPanel(PanelIdEnum.PlayerInfo, playerInfoParam);
            //    }
            //    else
            //    {
            //        UIManager.Instance.ShowPanel(PanelIdEnum.PlayerInfo, _lastQueryPlayerInfo);
            //    }
            //}
        }

        public void AddFriend(PbFriendAvatarInfo avatarInfo)
        {
            if (applySet.Contains(avatarInfo.dbid))
            {
            // Debug.Log("AddFriend:" + avatarInfo.dbid);
               //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.FRIEND_APPLY_SCUESS), PanelIdEnum.MainUIField);
            }
            else
            {
                //Debug.Log("SendAddFriend:" + avatarInfo.dbid);
                RPC(action_config.FRIEND_ADD, avatarInfo.dbid);
            }
        }

        public void AddFriend(UInt64 avatarDbid)
        {
            if (applySet.Contains(avatarDbid))
            {
               //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetString(LangEnum.FRIEND_APPLY_SCUESS), PanelIdEnum.MainUIField);
            }
            else
            {
                RPC(action_config.FRIEND_ADD, avatarDbid);
            }
        }


        public void DelFriend(PbFriendAvatarInfo avatarInfo)
        {
            RPC(action_config.FRIEND_DEL, avatarInfo.dbid);
        }

        public void RequestRecomendFriend()
        {
            RPC(action_config.FRIEND_GET_RECOMMEND);
        }

        public void AcceptFriend(PbFriendAvatarInfo avatarInfo)
        {
            if (PlayerDataManager.Instance.FriendData.GetFriendAvatarInfoByDbid(avatarInfo.dbid) != null)
            {
                PlayerDataManager.Instance.FriendData.RemoveApplication(avatarInfo.dbid);
                EventDispatcher.TriggerEvent(FriendEvents.FRIEND_APPLICATION_CHANGE);
            }

            if(acceptDic.ContainsKey(avatarInfo.dbid) == false)
            {
                acceptDic.Add(avatarInfo.dbid,avatarInfo);
            }
            RPC(action_config.FRIEND_ACCEPT, avatarInfo.dbid);
        }

        public void RejectFriend(UInt64 dbId)
        {

            if(PlayerDataManager.Instance.FriendData.GetFriendAvatarInfoByDbid(dbId) != null)
            {
                PlayerDataManager.Instance.FriendData.RemoveApplication(dbId);
                EventDispatcher.TriggerEvent(FriendEvents.FRIEND_APPLICATION_CHANGE);
            }
            RPC(action_config.FRIEND_REFUSE, dbId);
        }

        public void BlessFriend(UInt64 dbId)
        {
            RPC(action_config.FRIEND_GIVE_INVITE,dbId);
        }

        public void ReplyBless(UInt64 dbId)
        {
            RPC(action_config.FRIEND_GIVE_ACCEPT,dbId);
        }

        public void ReceiveBless(UInt64 dbId)
        {
            RPC(action_config.FRIEND_GET_BLESS_REWARD,dbId);
        }

        public void ReceiveAllBless()
        {
            RPC(action_config.FRIEND_BATCH_GET_BLESS_REWARD);
        }

        public void GiveGoods(UInt64 dbId,int itemId,int num)
        {
            RPC(action_config.FRIEND_GIVE_GOODS,dbId,itemId,num);
        }

        public void BuyAndGiveGoods(UInt64 dbId, int itemId, int num)
        {
            RPC(action_config.FRIEND_BUY_AND_GIVE_GOODS, dbId, itemId, num);
        }

        private bool _hasInitBlessing;
        private bool _hasInitBlessed;
        private bool _hasInitReceived;
        //private bool _hasInitFriend;
        public void GetBlessingList()
        {  
            if(_hasInitBlessing ==false)
            {
                ///增加祝福别人列表超时重新请求
                RegisterTimeOutReRequest(action_config.FRIEND_GET_SEND_LIST);
                RPC(action_config.FRIEND_GET_SEND_LIST);
            }
        }

        public void GetBlessedList()
        {
            if(_hasInitBlessed == false)
            {
                ///增加受到祝福列表超时重新请求
                RegisterTimeOutReRequest(action_config.FRIEND_GET_RECEIVE_LIST);
                RPC(action_config.FRIEND_GET_RECEIVE_LIST);
            }
        }

        public void GetApplicationList()
        {
            RPC(action_config.FRIEND_ADDED_LIST);
        }

        private bool _hasGetIntimateList = false;
        public void GetIntimateEventList()
        {
            if (_hasGetIntimateList == false)
            {
                RPC(action_config.FRIEND_GET_INTIMATE_EVENT_LIST);
            }
        }

        public void GetReceivedList()
        {
            if(_hasInitReceived == false)
            {
                ///增加已领取祝福列表超时重新请求
                RegisterTimeOutReRequest(action_config.FRIEND_GET_BLESS_LIST);
                RPC(action_config.FRIEND_GET_BLESS_LIST);
            }
            
        }

        public bool SendGetFriendListMsg()
        {
            PlayerDataManager.Instance.FriendData.hasRequestData = true;
            ///增加好友列表请求返回超时重新请求
            RegisterTimeOutReRequest(action_config.FRIEND_GET_PS_INFO, 0, public_config.MGR_ID_AVATAR_FRIEND);
            return RPC(action_config.FRIEND_GET_PS_INFO, 0, public_config.MGR_ID_AVATAR_FRIEND);
        }

        public void SendGetFriendMsg(string _toFindName)
        {
            RPC(action_config.FRIEND_GET_P_INFO, 1, _toFindName);
        }

        public void SendSearchPlayerMsg(string _toFindName)
        {
            RPC(action_config.FRIEND_SEARCH, 1, _toFindName);
        }

        public void SendGetFriendOnlineListMsg()
        {
            RPC(action_config.FRIEND_GET_ONLINE_LIST);
        }


        #endregion


        #region  响应服务器的返回码


        public void OnFriendResp(UInt32 actionId, UInt32 errorId, byte[] byteArr)
        {
            action_config actionConfig = (action_config)actionId;
            UnRegisterTimeOutReRequest(actionConfig);
            if(errorId == 0)
            {
                if(delegateDict.ContainsKey(actionConfig))
                {
                    Action<byte[]> handler = delegateDict[actionConfig];
                    handler(byteArr);
                }
            }
            else
            {
                if(errorDict.ContainsKey(actionConfig))
                {
                    Action<byte[]> error = delegateDict[actionConfig];
                    error(byteArr);
                }
            } 
        }

        private void OnGiveGoodsResp(byte[] byteArr)
        {
            //Debug.Log("OnGiveGoodsResp");
            PbFriendIntimateItem item = MogoProtoUtils.ParseProto<PbFriendIntimateItem>(byteArr);
            if (PlayerDataManager.Instance.FriendData.SendItemDict.ContainsKey((int)item.item_id) == false)
            {
                PlayerDataManager.Instance.FriendData.SendItemDict.Add((int)item.item_id,(int)item.time);
            }
            EventDispatcher.TriggerEvent(FriendEvents.FRIEND_SEND_LIST_CHANGE,(int)item.item_id);
            EventDispatcher.TriggerEvent(FriendEvents.FRIEND_INTIMATE_CHANGE, item.recv_dbid);
        }

        private void OnGetOnlineFriendListResp(byte[] byteArr)
        {
            PbFrinedOnlineList onlineList = MogoProtoUtils.ParseProto<PbFrinedOnlineList>(byteArr);
            PlayerDataManager.Instance.FriendData.UpdateOnlineFriendList(onlineList);
            EventDispatcher.TriggerEvent(FriendEvents.FRIEND_UPDATE_ONLINE);
        }

        public void OnGetApplicationListResp(byte[] byteArr)
        {
            PbFriendAddedList list = MogoProtoUtils.ParseProto<PbFriendAddedList>(byteArr);
            m_friendData.AddApplication(list.avatars_info);
            EventDispatcher.TriggerEvent(FriendEvents.FRIEND_APPLICATION_CHANGE);
            if(list.avatars_info.Count > 0)
            {
                SystemInfoManager.Instance.ShowFastNotice(60317);
            }
            //Debug.Log("OnGetApplicationListResp");
        }

        private void FriendRecommend(byte[] byteArr)
        {
            PbFriendRecommend recommend = MogoProtoUtils.ParseProto<PbFriendRecommend>(byteArr);
            PlayerDataManager.Instance.FriendData.UpdateRecommendFriendList(recommend);
            EventDispatcher.TriggerEvent(FriendEvents.FRIEND_RECOMMEND_CHANGE);
        }

        private void OnUpdateDegree(byte[] byteArr)
        {
            PbFriendDegree friendDegree = MogoProtoUtils.ParseProto<PbFriendDegree>(byteArr);
            PlayerDataManager.Instance.FriendData.UpdateFriendDegree(friendDegree.dbid, friendDegree.degree);
        }

        private void OnFriendOnLineNotice(byte[] byteArr)
        {
            PbFrinedStateNotice notice = MogoProtoUtils.ParseProto<PbFrinedStateNotice>(byteArr);
            if(notice.state == 2)
            {
                notice.state = 0;
            }
            if(notice.state == 1)
            {
                notice.name = MogoProtoUtils.ParseByteArrToString(notice.name_bytes);
                SystemInfoManager.Instance.Show(action_config.FRIEND_STATE_NOTICE.ToInt32(), notice.name);
            }
           // Debug.Log("OnFriendOnLineNotice" + notice.dbid+","+ notice.state);
            PlayerDataManager.Instance.FriendData.UpdateFriendOnlineState(notice.dbid, notice.state);
            EventDispatcher.TriggerEvent(FriendEvents.FRIEND_ONLINE_STATE_CHANGE, notice.dbid);
        }


        private void GetNearListResp(byte[] byteArr)
        {

        }

        private void GetFriendAddedResp(byte[] byteArr)
        {
            PbFriendInfo friendInfo = MogoProtoUtils.ParseProto<PbFriendInfo>(byteArr);
            m_friendData.AddApplication(friendInfo.avatars_info);
            EventDispatcher.TriggerEvent(FriendEvents.FRIEND_APPLICATION_CHANGE);
        }

        private void FriendAddResp(byte[] byteArr)
        {
            PbFriendInfo friendInfo = MogoProtoUtils.ParseProto<PbFriendInfo>(byteArr);
            applySet.Add(friendInfo.avatars_info[0].dbid);
            EventDispatcher.TriggerEvent(FriendEvents.FRIEND_ADD_APPLY_SUCCESS,friendInfo.avatars_info[0].dbid);
        }

        private void OnFriendAddError(byte[] byteArr)
        {
            PbFriendInfo friendInfo = MogoProtoUtils.ParseProto<PbFriendInfo>(byteArr);
            applySet.Remove(friendInfo.avatars_info[0].dbid);
        }

        private void OnFriendAcceptError(byte[] byteArr)
        {
            PbFriendInfo friendInfo = MogoProtoUtils.ParseProto<PbFriendInfo>(byteArr);
            acceptDic.Remove(friendInfo.avatars_info[0].dbid);
        }


        private void GetFriendInfo(byte[] byteArr)
        {
            PbFriendInfo friendInfo = MogoProtoUtils.ParseProto<PbFriendInfo>(byteArr);
            m_friendData.UpdateFriend(friendInfo.avatars_info[0]);
            EventDispatcher.TriggerEvent(FriendEvents.REFRESH_FRIEND, friendInfo.avatars_info[0]);
        }

        private void GetFriendListResp(byte[] byteArr)
        {
            PbFriendInfo friendInfo = MogoProtoUtils.ParseProto<PbFriendInfo>(byteArr);
            m_friendData.UpdateFriendList(friendInfo);
            EventDispatcher.TriggerEvent(FriendEvents.REFRESH_FRIEND_LIST);
        }

        private void FriendSearchResp(byte[] byteArr)
        {
            PbFriendInfo friendInfo = MogoProtoUtils.ParseProto<PbFriendInfo>(byteArr);
            friendInfo.avatars_info[0].name = MogoProtoUtils.ParseByteArrToString(friendInfo.avatars_info[0].name_bytes);
            EventDispatcher.TriggerEvent(FriendEvents.GET_PLAYERINFO, friendInfo.avatars_info[0]);
        }

        private void GetPlayerDetailInfo(byte[] byteArr)
        {
            Debug.Log("GetPlayerDetailInfo");
            PbFriendInfo friendInfo = MogoProtoUtils.ParseProto<PbFriendInfo>(byteArr);
            friendInfo.avatars_info[0].name = MogoProtoUtils.ParseByteArrToString(friendInfo.avatars_info[0].name_bytes);
            _lastQueryPlayerInfo = friendInfo.avatars_info[0];
            if (_curPlayerInfoParam != null)
            {
                _curPlayerInfoParam.avatarInfo = _lastQueryPlayerInfo;
                UIManager.Instance.ShowPanel(PanelIdEnum.PlayerInfo, _curPlayerInfoParam);
            }
            else
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.PlayerInfo, _lastQueryPlayerInfo);
            }
        }

        private void FriendSendList(byte[] byteArr)
        {
            _hasInitBlessing = true;
            PbFriendList sendList = MogoProtoUtils.ParseProto<PbFriendList>(byteArr);
            m_friendData.UpdateBlessing(sendList);
            EventDispatcher.TriggerEvent(FriendEvents.REFRESH_FRIEND_SEND_LIST);
        }

        private void FriendRewardedList(byte[] byteArr)
        {
            Debug.Log("已领取祝福列表");
            _hasInitReceived = true;
            PbFriendList rewardedList = MogoProtoUtils.ParseProto<PbFriendList>(byteArr);
            m_friendData.UpdateReceivedList(rewardedList);
            EventDispatcher.TriggerEvent(FriendEvents.REFRESH_FRIEND_RECEIVED_LIST);

        }

        private void FriendReceiveList(byte[] byteArr)
        {
            Debug.Log("被祝福列表");
            _hasInitBlessed = true;
            PbFriendList receivedList = MogoProtoUtils.ParseProto<PbFriendList>(byteArr);
            m_friendData.UpdateBlessed(receivedList);
            if (m_friendData.BlessedNum > 0)
            {
                SystemInfoManager.Instance.ShowFastNotice(50004);
            }

            EventDispatcher.TriggerEvent(FriendEvents.REFRESH_FRIEND_RECV_LIST);
        }

        public void GetIntimateEventList(byte[] byteArr)
        {

        }

        public void EventRecordResp(UInt16 action_id, LuaTable luaTable)
        {
            if ((action_config)action_id == action_config.FRIEND_GET_INTIMATE_EVENT_LIST)
            {
                _hasGetIntimateList = true;
                foreach (var item in luaTable)
                {
                    if (luaTable.IsLuaTable(item.Key))
                    {
                        LuaTable table = item.Value as LuaTable;
                        int systemInfoId = Convert.ToInt32(table["1"]);
                        LuaTable sysTable = table["2"] as LuaTable;

                        object[] objArr = new object[sysTable.Count];
                        sysTable.Values.CopyTo(objArr, 0);

                        PlayerDataManager.Instance.FriendData.FriendIntimateEventResp((error_code)systemInfoId, false, objArr);
                    }
                }
                EventDispatcher.TriggerEvent(FriendEvents.INTIMATE_EVENT_LIST_CHANGE);
            }
        }

        private void FriendAccept(byte[] byteArr)
        {
            Debug.Log("接受好友");
            PbFriendInfo friendInfo = MogoProtoUtils.ParseProto<PbFriendInfo>(byteArr);
            PbFriendAvatarInfo avatarInfo = friendInfo.avatars_info[0];
            m_friendData.RemoveApplication(avatarInfo.dbid);
            if(acceptDic.ContainsKey(avatarInfo.dbid))
            {
                acceptDic[avatarInfo.dbid].is_friend = 1;
                acceptDic[avatarInfo.dbid].degree = 0;
                m_friendData.FriendAvatarList.Add(acceptDic[avatarInfo.dbid]);
                //m_friendData.FriendTotalPage = Mathf.CeilToInt(m_friendData.FriendAvatarList.Count / 20);
                EventDispatcher.TriggerEvent(FriendEvents.REFRESH_FRIEND_LIST);
                acceptDic.Remove(avatarInfo.dbid);
            }
            EventDispatcher.TriggerEvent(FriendEvents.FRIEND_APPLICATION_CHANGE);
            EventDispatcher.TriggerEvent(FriendEvents.ADD_FRIEND, avatarInfo.dbid);
        }

        private void FriendAccepted(byte[] byteArr)
        {
            Debug.Log("被接受好友");
            PbFriendInfo friendInfo = MogoProtoUtils.ParseProto<PbFriendInfo>(byteArr);
            PbFriendAvatarInfo avatarInfo = friendInfo.avatars_info[0];
            avatarInfo.name = MogoProtoUtils.ParseByteArrToString(avatarInfo.name_bytes);
            if(applySet.Contains(avatarInfo.dbid))
            {
                applySet.Remove(avatarInfo.dbid);
            }
            m_friendData.FriendAvatarList.Add(avatarInfo);
           // m_friendData.FriendTotalPage = Mathf.CeilToInt(m_friendData.FriendAvatarList.Count / 20);
            EventDispatcher.TriggerEvent(FriendEvents.REFRESH_FRIEND_LIST);
            EventDispatcher.TriggerEvent(FriendEvents.ADD_FRIEND, avatarInfo.dbid);
        }

        private void FriendRefuse(byte[] byteArr)
        {
            Debug.Log("拒绝好友");
            PbFriendInfo friendInfo = MogoProtoUtils.ParseProto<PbFriendInfo>(byteArr);
            PbFriendAvatarInfo avatarInfo = friendInfo.avatars_info[0];
            m_friendData.RemoveApplication(avatarInfo.dbid);
            EventDispatcher.TriggerEvent(FriendEvents.FRIEND_APPLICATION_CHANGE);
        }

        private void FriendRefused(byte[] byteArr)
        {
            Debug.Log("被拒绝好友");
            PbFriendInfo friendInfo = MogoProtoUtils.ParseProto<PbFriendInfo>(byteArr);
            PbFriendAvatarInfo avatarInfo = friendInfo.avatars_info[0];
            applySet.Remove(avatarInfo.dbid);
        }

        private void FriendDel(byte[] byteArr)
        {
            Debug.Log("删除好友");
            PbFriendInfo friendInfo = MogoProtoUtils.ParseProto<PbFriendInfo>(byteArr);
            PbFriendAvatarInfo avatarInfo = friendInfo.avatars_info[0];
            m_friendData.RemoveFriend(avatarInfo.dbid);
            EventDispatcher.TriggerEvent(FriendEvents.REFRESH_FRIEND_LIST);
            EventDispatcher.TriggerEvent(FriendEvents.REMOVE_FRIEND, avatarInfo.dbid);
        }

        private void FriendGiveInvite(byte[] byteArr)
        {
            Debug.Log("赠送体力");
            PbFriendInfo friendInfo = MogoProtoUtils.ParseProto<PbFriendInfo>(byteArr);
            PbFriendAvatarInfo avatarInfo = friendInfo.avatars_info[0];
            m_friendData.AddBlessing(avatarInfo.dbid);
            EventDispatcher.TriggerEvent(FriendEvents.FRIEND_BLESS_STATE_CHANGE, avatarInfo.dbid);
        }

        private void FriendGiveAccept(byte[] byteArr)
        {
            Debug.Log("FriendGiveAccept 回赠祝福");
            PbFriendInfo friendInfo = MogoProtoUtils.ParseProto<PbFriendInfo>(byteArr);
            PbFriendAvatarInfo avatarInfo = friendInfo.avatars_info[0];
            m_friendData.AddBlessing(avatarInfo.dbid);
            m_friendData.AddReceivedList(avatarInfo.dbid);
            m_friendData.RemoveBlessedByDbid(avatarInfo.dbid);
            EventDispatcher.TriggerEvent(FriendEvents.FRIEND_BLESS_STATE_CHANGE, avatarInfo.dbid);
        }

        private void GetBlessReward(byte[] byteArr)
        {
            Debug.Log("领取祝福奖励");
            PbFriendInfo friendInfo = MogoProtoUtils.ParseProto<PbFriendInfo>(byteArr);
            PbFriendAvatarInfo avatarInfo = friendInfo.avatars_info[0];
            m_friendData.AddReceivedList(avatarInfo.dbid);
            m_friendData.RemoveBlessedByDbid(avatarInfo.dbid);
            EventDispatcher.TriggerEvent(FriendEvents.FRIEND_BLESS_STATE_CHANGE, avatarInfo.dbid);
        }

        private void ReceiveBless(byte[] byteArr)
        {
            Debug.Log("被赠送体力祝福");
            PbFriendInfo friendInfo = MogoProtoUtils.ParseProto<PbFriendInfo>(byteArr);
            PbFriendAvatarInfo avatarInfo = friendInfo.avatars_info[0];
            m_friendData.AddBlessed(avatarInfo.dbid);
            EventDispatcher.TriggerEvent(FriendEvents.FRIEND_BLESS_STATE_CHANGE, avatarInfo.dbid);
        }

        

        #endregion

    }

    public class PlayerInfoParam
    {
        public Vector3 targetPos;
        public Vector2 targetSizeDelta;
        public PbFriendAvatarInfo avatarInfo;
        public AlignmentType horizontalAlignType;
        public AlignmentType verticalAlignType;

        public PlayerInfoParam(Vector3 targetPos, Vector2 targetSizeDelta, AlignmentType horizontalAlignType = AlignmentType.AlignGameObject, 
            AlignmentType verticalAlignType = AlignmentType.AlignGameObject)
        {
            this.targetPos = targetPos;
            this.targetSizeDelta = targetSizeDelta;
            this.horizontalAlignType = horizontalAlignType;
            this.verticalAlignType = verticalAlignType;
        }

        public enum AlignmentType
        {
            AlignGameObject = 0,
            ScreenCenter = 1,
            SpecifyPos = 2
        }
    }

    public class ReRequestParams
    {
        public int LeftRequestTime;
        public object[] RPCParams;

        public ReRequestParams(int leftRequestTime, object[] rpcRarams)
        {
            LeftRequestTime = leftRequestTime;
            RPCParams = rpcRarams;
        }
    }
}
