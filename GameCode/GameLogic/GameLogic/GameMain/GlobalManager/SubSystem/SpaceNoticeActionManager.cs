﻿using ACTSystem;
using Common.ClientConfig;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameLoader.Utils.CustomType;
using MogoEngine.Events;
using MogoEngine.Utils;
using SpaceSystem;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{

    public class SpaceNoticeActionManager : Singleton<SpaceNoticeActionManager>
    {
        private List<SystemNoticeData> _dataList;

        public SpaceNoticeActionManager()
        {
            _dataList = new List<SystemNoticeData>();
        }

        public void MoreSpaceActionEventResp(UInt32 mapId, byte[] clientActionData)
        {
            PbSpaceActionEventList clientActionList = MogoProtoUtils.ParseProto<PbSpaceActionEventList>(clientActionData);
            foreach (PbSpaceActionEvent pbSpaceActionEvent in clientActionList.spaceActionEventList)
            {
                ProcessSpaceActionEvent(pbSpaceActionEvent);
            }
        }

        public void SpaceActionEventResp(UInt32 mapId, byte[] clientActionData)
        {
            PbSpaceActionEvent pbSpaceActionEvent = MogoProtoUtils.ParseProto<PbSpaceActionEvent>(clientActionData);
            ProcessSpaceActionEvent(pbSpaceActionEvent);
        }

        private void ProcessSpaceActionEvent(PbSpaceActionEvent pbSpaceActionEvent)
        {
            if (pbSpaceActionEvent.event_type == public_config.SPACE_ACTION_EVENT_ACTION_TASK)
            {
                ShowCopyStarState(pbSpaceActionEvent);
                return;
            }
            EntityData entityData = GlobalManager.GameSceneManager.GetInstance().CurrActionEntityData((int)pbSpaceActionEvent.action_id);
            if (pbSpaceActionEvent.event_type == public_config.SPACE_ACTION_EVENT_TIMER_LEFT_TIME)
            {
                ShowRightUpNotice(pbSpaceActionEvent, entityData);
            }
            else if (pbSpaceActionEvent.event_type == public_config.SPACE_ACTION_EVENT_CND_PROGRESS)
            {
                ShowRightUpProgressNotice(pbSpaceActionEvent, entityData);
            }
            else if (pbSpaceActionEvent.event_type == public_config.SPACE_ACTION_EVENT_STOP_TIMER)
            {
                EventDispatcher.TriggerEvent(BattleUIEvents.HIDE_PROGRESSBAR_INFOMATION);
                return;
            }
            else if (pbSpaceActionEvent.event_type == public_config.SPACE_ACTION_EVENT_REGION_LEFT_TIME)
            {
                ShowProgressNotice(pbSpaceActionEvent, entityData);
            }
            else if (pbSpaceActionEvent.event_type == public_config.SPACE_ACTION_EVENT_MOVE_POINT)
            {
                var movePointData = entityData as EntityAiMoveActionData;
                GameMain.GlobalManager.GameSceneManager.GetInstance().SetCurrAutoMovePoint(movePointData.ai_move_point_l);
                return;
            }
            else if (pbSpaceActionEvent.event_type == public_config.SPACE_ACTION_EVENT_PLAYER_SCORE)
            {
                ShowPlayerScore(pbSpaceActionEvent, entityData);
            }
            else if (pbSpaceActionEvent.event_type == public_config.SPACE_ACTION_EVENT_CAMP_SCORE)
            {
                ShowCampScore(pbSpaceActionEvent, entityData);
            }
            else if (pbSpaceActionEvent.event_type == public_config.SPACE_ACTION_EVENT_UPDATE_COUNT)
            {
                ShowUpdateCount(pbSpaceActionEvent, entityData);
            }

        }

        private void ShowUpdateCount(PbSpaceActionEvent pbSpaceActionEvent, EntityData entityData)
        {
            var regionData = entityData as EntityCountActionData;
            SystemNoticeTopData systemNoticeData = new SystemNoticeTopData();
            systemNoticeData.type = InstanceDefine.COUNTER_NOTICE_ACTION;
            systemNoticeData.notice_type = regionData.count_notice_type_i;
            systemNoticeData.rightUpTargetId = regionData.target_type_i;
            systemNoticeData.timerCount = (int)pbSpaceActionEvent.event_param1;
            systemNoticeData.maxCount = int.Parse(regionData.count_max_l);
            AddSystemNotice(systemNoticeData);
        }

        private void ShowCopyStarState(PbSpaceActionEvent pbSpaceActionEvent)
        {
            SystemNoticeCopyStarData systemNoticeData = new SystemNoticeCopyStarData();
            systemNoticeData.type = InstanceDefine.COPY_STAR_CHAGED;
            systemNoticeData.actionId = (int)pbSpaceActionEvent.action_id;
            systemNoticeData.state = (int)pbSpaceActionEvent.event_param1;
            AddSystemNotice(systemNoticeData);
        }

        private void ShowCampScore(PbSpaceActionEvent pbSpaceActionEvent, EntityData entityData)
        {
            var regionData = entityData as EntityCountActionData;
            SystemNoticeTopData systemNoticeData = new SystemNoticeTopData();
            systemNoticeData.type = InstanceDefine.COUNTER_NOTICE_ACTION;
            systemNoticeData.notice_type = regionData.count_notice_type_i;
            systemNoticeData.rightUpTargetId = pbSpaceActionEvent.event_param1;
            systemNoticeData.timerCount = (int)pbSpaceActionEvent.event_param2;
            systemNoticeData.maxCount = int.Parse(regionData.count_max_l);
            AddSystemNotice(systemNoticeData);
        }

        private void ShowPlayerScore(PbSpaceActionEvent pbSpaceActionEvent, EntityData entityData)
        {
            var regionData = entityData as EntityCountActionData;
            SystemNoticeTopData systemNoticeData = new SystemNoticeTopData();
            systemNoticeData.type = InstanceDefine.COUNTER_NOTICE_ACTION;
            systemNoticeData.notice_type = regionData.count_notice_type_i;
            systemNoticeData.rightUpTargetId = pbSpaceActionEvent.event_param1;
            systemNoticeData.timerCount = (int)pbSpaceActionEvent.event_param2;
            systemNoticeData.maxCount = int.Parse(regionData.count_max_l);
            AddSystemNotice(systemNoticeData);
        }

        private void ShowProgressNotice(PbSpaceActionEvent pbSpaceActionEvent, EntityData entityData)
        {
            var regionData = entityData as EntityRegionTriggerActionData;
            SystemNoticeProgressData systemNoticeData = new SystemNoticeProgressData();
            systemNoticeData.type = InstanceDefine.SYSTEM_NOTICE_ACTION_PROGRESSBAR_TYPE;
            systemNoticeData.priority = regionData.show_priority_i;
            systemNoticeData.content = regionData.notice_content_id_i;
            systemNoticeData.time = (uint)(pbSpaceActionEvent.event_param1);
            systemNoticeData.progressbarPlayType = regionData.is_can_trigger_notice_i;
            AddSystemNotice(systemNoticeData);
        }

        private void ShowRightUpProgressNotice(PbSpaceActionEvent pbSpaceActionEvent, EntityData entityData)
        {
            var condData = entityData as EntityCondTriggerActionData;
            SystemNoticeRightUpData systemNoticeData = new SystemNoticeRightUpData();
            systemNoticeData.type = InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_TYPE;
            systemNoticeData.rightUpType = InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_PROGRESSBAR_TYPE;
            systemNoticeData.priority = condData.show_priority_i;
            systemNoticeData.content = condData.notice_content_id_i;
            systemNoticeData.rightUpNoticeId = condData.id_i;
            systemNoticeData.killMonsterNum = (int)pbSpaceActionEvent.event_param2;
            int totalCount = 0;
            string[] str = condData.event_trigger_num_l.Split(',');
            string[] countId = condData.event_action_s.Split(';');
            for (int i = 0; i < countId.Length; i++)
            {
                //if (System.Convert.ToInt32(countId[i].Split(',')[0]) == (int)clientAction.event_param1)
                //{
                totalCount = System.Convert.ToInt32(str[i]);
                // break;
                //}
            }
            systemNoticeData.monsterTotalNum = totalCount;
            systemNoticeData.rightUpTargetId = int.Parse(countId[0]);
            AddSystemNotice(systemNoticeData);
        }

        private void ShowRightUpNotice(PbSpaceActionEvent pbSpaceActionEvent, EntityData entityData)
        {
            var timeData = entityData as EntityTimerActionData;
            SystemNoticeRightUpData systemNoticeData = new SystemNoticeRightUpData();
            systemNoticeData.type = InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_TYPE;
            systemNoticeData.rightUpType = InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_TIME_TYPE;
            systemNoticeData.priority = timeData.show_priority_i;
            systemNoticeData.time = (uint)(pbSpaceActionEvent.event_param1);
            systemNoticeData.rightUpNoticeId = (int)pbSpaceActionEvent.action_id;
            systemNoticeData.content = timeData.notice_content_id_i;
            AddSystemNotice(systemNoticeData);
        }

        public void SpaceNoticeActionResp(UInt32 mapId, UInt16 actionId, LuaTable table)
        {

            EntityData entityData = GlobalManager.GameSceneManager.GetInstance().CurrActionEntityData((int)actionId);
            EntitySystemNoticeActionData data = entityData as EntitySystemNoticeActionData;
            object[] objArr = new object[table.Count];
            table.Values.CopyTo(objArr, 0);
            //Debug.LogError("actionId:" + actionId);
            //Debug.LogError("content:" + MogoLanguageUtil.GetContent(notioceData.notice_content_id_i, objArr));
            switch (data.notice_mode_type_i)
            {
                case InstanceDefine.SYSTEM_NOTICE_ACTION_UP_TYPE:
                case InstanceDefine.SYSTEM_NOTICE_ACTION_MIDDLE_TYPE:
                    ShowSystemNoticeFloatView(data, objArr);
                    break;
                //case InstanceDefine.SYSTEM_NOTICE_ACTION_PROGRESSBAR_TYPE:
                case InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_TYPE:
                    ShowSystemNoticeRightUpView(data, objArr);
                    break;
                default:
                    break;
            }
        }

        public void ShowCombatAttri(byte[] combatAttri)
        {
            PbCombatAttriList combatAttriList = MogoProtoUtils.ParseProto<PbCombatAttriList>(combatAttri);
            SystemCombatAttriData systemNoticeData = new SystemCombatAttriData();
            systemNoticeData.type = InstanceDefine.COUNTER_NOTICE_ACTION;
            systemNoticeData.notice_type = InstanceDefine.NOTICE_COMBAT_ATTRI;
            systemNoticeData.combat_attri = combatAttriList.combatAttriList;
            AddSystemNotice(systemNoticeData);
        }

        private void ShowSystemNoticeRightUpView(EntitySystemNoticeActionData data, object[] objArr)
        {
            SystemNoticeRightUpData systemNoticeData = new SystemNoticeRightUpData();
            systemNoticeData.type = data.notice_mode_type_i;
            systemNoticeData.time = (uint)data.notice_countdown_i * 1000;
            systemNoticeData.content = data.notice_content_id_i;
            systemNoticeData.objArr = objArr;
            systemNoticeData.priority = data.show_priority_i;
            systemNoticeData.rightUpType = InstanceDefine.SYSTEM_NOTICE_ACTION_RIGHTUP_CONTENT_TYPE;
            systemNoticeData.rightUpNoticeId = data.id_i;
            AddSystemNotice(systemNoticeData);
        }

        private void ShowSystemNoticeFloatView(EntitySystemNoticeActionData data, object[] objArr)
        {
            SystemNoticeDisplayData systemNoticeData = new SystemNoticeDisplayData();
            systemNoticeData.type = data.notice_mode_type_i;
            systemNoticeData.time = (uint)data.notice_countdown_i * 1000;
            systemNoticeData.content = data.notice_content_id_i;
            systemNoticeData.objArr = objArr;
            systemNoticeData.priority = data.show_priority_i;
            AddSystemNotice(systemNoticeData);
        }

        public void AddSystemNotice(SystemNoticeData systemNoticeData)
        {
            _dataList.Add(systemNoticeData);
            EventDispatcher.TriggerEvent<SystemNoticeData>(BattleUIEvents.SHOW_INFOMATION, systemNoticeData);
        }

        public void RemoveSystemNotice(SystemNoticeData systemNoticeData)
        {
            _dataList.Remove(systemNoticeData);
        }

        public void ShowSystemNotice()
        {
            for (int i = _dataList.Count - 1; i >= 0; i--)
            {
                EventDispatcher.TriggerEvent<SystemNoticeData>(BattleUIEvents.SHOW_INFOMATION, _dataList[i]);
            }
        }

        public void SpaceActionEventDebugResp(byte[] clientActionData)
        {
            PbSpaceActionEventDebug pbSpaceActionEvent = MogoProtoUtils.ParseProto<PbSpaceActionEventDebug>(clientActionData);
            EntityData entityData = GlobalManager.GameSceneManager.GetInstance().CurrActionEntityData((int)pbSpaceActionEvent.action_id);
            if (pbSpaceActionEvent.event_type == public_config.SPACE_ACTION_EVENT_DEBUG_CREATE)
            {
                GameObject go = GameObject.Find("ActionEntities");
                var obj = new GameObject(entityData.type + "(" + entityData.id_i + ")");
                entityData.editorObj = obj;

                obj.layer = UnityEngine.LayerMask.NameToLayer("TransparentFX");
                if (entityData.origin_point_type_i == 0)
                {
                    entityData.editorObj.transform.position
                    = new Vector3(SpaceDataConfig.ScaleToReal(entityData.pos_x_i),
                                  SpaceDataConfig.ScaleToReal(entityData.pos_y_i),
                                  SpaceDataConfig.ScaleToReal(entityData.pos_z_i));
                }
                obj.AddComponent<BoxCollider>();
                obj.transform.SetParent(go.transform);
            }
            else if (pbSpaceActionEvent.event_type == public_config.SPACE_ACTION_EVENT_DEBUG_DESTROY)
            {
                GameObject go = GameObject.Find(entityData.type + "(" + entityData.id_i + ")");
                GameObject.Destroy(go);
            }
            else if (pbSpaceActionEvent.event_type == public_config.SPACE_ACTION_EVENT_DEBUG_TRIGGER)
            {
            }
            GameSceneManager.GetInstance().AddInspectingCache((int)pbSpaceActionEvent.event_type, (int)pbSpaceActionEvent.action_id);
        }
        
    }
}
