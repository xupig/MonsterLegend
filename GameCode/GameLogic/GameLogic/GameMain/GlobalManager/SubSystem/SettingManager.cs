﻿using System;
using System.Collections.Generic;
using Common.Data;
using Common.ServerConfig;
using GameData;
using GameMain.Entities;

namespace GameMain.GlobalManager.SubSystem
{
    public class SettingManager
    {
        private static SettingManager _instance;
        public static SettingManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SettingManager();
                }
                return _instance;
            }
        }

        private SettingData _settingData;

        protected SettingManager()
        {
            _settingData = PlayerDataManager.Instance.SettingData;
        }

        public void SyncPushSettingInfo()
        {
            Dictionary<int, bool> pushSettingDict = _settingData.PushSettingDict;
            foreach (var item in pushSettingDict)
            {
                int pushId = item.Key;
                if (push_helper.IsServerPush(pushId))
                {
                    int serverPushType = push_helper.GetServerPushType(pushId);
                    bool isPush = item.Value;
                    SettingManager.Instance.RequestPushSettingChanged(serverPushType, isPush);
                }
            }
        }

        public void RequestPushSettingChanged(int serverPushType, bool isPush)
        {
            int pushTag = isPush == true ? 1 : 0;
            RPC(action_config.ACTION_SETTING_PUSH, serverPushType, pushTag);
        }

        private void RPC(action_config actionId, params object[] args)
        {
            PlayerAvatar.Player.ActionRpcCall("setting_req", (int)actionId, args);
        }
    }
}
