﻿using System;
using System.Collections.Generic;
using Common.ServerConfig;
using Common.Utils;
using GameData;
using GameMain.GlobalManager.SubSystem;
using GameLoader.Utils.Timer;
using Common.Global;

namespace GlobalManager
{
    public enum SystemChatMsgType
    {
        SystemMsg = 1,
        HelpMsg = 2
    }

    public class SystemChatMsgManager
    {
        private int _curMsgId = 0;
        private uint _showMsgTimer = 0;
        private uint _controlShowMsgTickTimerId = 0;
        private bool _isStartShowMsg = false;
        private int _showTimeInterval = 0;

        private static SystemChatMsgManager _instance;
        public static SystemChatMsgManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SystemChatMsgManager();
                }
                return _instance;
            }
        }

        private bool _isShowMsg = true;
        public bool IsShowMsg
        {
            get { return _isShowMsg; }
            set
            {
                if (_isShowMsg != value)
                {
                    _isShowMsg = value;
                    if (_isShowMsg)
                    {
                        StartGetOneMsgToShow();
                    }
                    else
                    {
                        StopShowMsg();
                    }
                }
            }
        }

        protected SystemChatMsgManager()
        {
            
        }

        public void StartShowMsg()
        {
            _controlShowMsgTickTimerId = TimerHeap.AddTimer(0, 60 * 1000, OnControlShowMsgTick);
        }

        public void Show(int systemMsgId)
        {
            ShowMsg(systemMsgId);
        }

        private void ShowMsg(int systemMsgId)
        {
            system_chat_msg info = system_chat_msg_helper.GetConfig(systemMsgId);
            if (info == null)
                return;
            _curMsgId = systemMsgId;
            if (info.__type == (int)SystemChatMsgType.SystemMsg)
            {
                AddSystemChatMsg(SystemInfoUtil.GenerateContent(info.__desc));
            }
        }

        private void AddSystemChatMsg(string content)
        {
            ChatManager.Instance.SendClientTipsChatMsg(public_config.CHANNEL_ID_WORLD, content);
        }

        private void OnControlShowMsgTick()
        {
            if (!IsShowMsg)
            {
                return;
            }
            DateTime curDateTime = MogoTimeUtil.ServerTimeStamp2UtcTime((long)(Global.serverTimeStamp / 1000));
            int timeInterval = 0;
            if (curDateTime.Hour >= 0 && curDateTime.Hour < 10)
            {
                timeInterval = GlobalParams.GetSystemPushLongTimeInterval();
            }
            else
            {
                timeInterval = GlobalParams.GetSystemPushTimeInterval();
            }
            if (_showTimeInterval != timeInterval)
            {
                int curTimeInterval = _showTimeInterval;
                _showTimeInterval = timeInterval;
                if (IsShowMsg)
                {
                    StopShowMsg();
                    StartGetOneMsgToShow();
                }
            }
        }

        private void StartGetOneMsgToShow()
        {
            if (_isStartShowMsg)
                return;
            _isStartShowMsg = true;
            _showMsgTimer = TimerHeap.AddTimer((uint)(_showTimeInterval * 1000), _showTimeInterval * 1000, GetOneSystemMsg);
        }

        private void GetOneSystemMsg()
        {
            List<int> msgIdList = new List<int>();
            foreach (KeyValuePair<int, system_chat_msg> item in XMLManager.system_chat_msg)
            {
                system_chat_msg info = item.Value;
                if (info.__id != _curMsgId && system_chat_msg_helper.CanShowSystemMsgCurLv(info.__id) && system_chat_msg_helper.CanShowSystemMsgCurDay(info.__id))
                {
                    msgIdList.Add(info.__id);
                }
            }

            //随机从列表中选取一条
            if (msgIdList.Count > 0)
            {
                int index = UnityEngine.Random.Range(0, msgIdList.Count);
                Show(msgIdList[index]);
            }
        }

        private void StopShowMsg()
        {
            ResetTimer();
            _isStartShowMsg = false;
            _curMsgId = 0;
        }

        private void ResetTimer()
        {
            if (_showMsgTimer != 0)
            {
                TimerHeap.DelTimer(_showMsgTimer);
                _showMsgTimer = 0;
            }
        }

        public void Clear()
        {
            StopShowMsg();
            if (_controlShowMsgTickTimerId != 0)
            {
                TimerHeap.DelTimer(_controlShowMsgTickTimerId);
                _controlShowMsgTickTimerId = 0;
            }
        }
    }
}
