﻿using Common.Data;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameMain.GlobalManager.SubSystem
{
    public class CopyStatisticDataManager : Singleton<CopyStatisticDataManager>
    {
        private List<CopyStatisticData> _copyStatisticDataList;

        public CopyStatisticDataManager()
        {
            _copyStatisticDataList = new List<CopyStatisticData>();
        }

        /// <summary>
        /// 搜集客户端战斗统计数据
        /// </summary>
        public void GetClientStatisticData()
        {
            _copyStatisticDataList.Clear();
            ///搜集玩家自己的
            List<int> actionIdList = mission_battle_statistics_helper.GetStatisticActionIdList();
            for(int i = 0; i < actionIdList.Count; i++)
            {
                PlayerCopyStatisticData playerCopyStatisticData = new PlayerCopyStatisticData();

                playerCopyStatisticData.actionId = actionIdList[i];
                playerCopyStatisticData.Level = PlayerAvatar.Player.level;
                playerCopyStatisticData.Name = PlayerAvatar.Player.name;
                playerCopyStatisticData.type = mission_battle_statistics_helper.OutPutOrBear(actionIdList[i]);
                playerCopyStatisticData.vocation = (int)PlayerAvatar.Player.vocation;
                playerCopyStatisticData.value = (int)PlayerAvatar.Player.GetStatisticsDamage(actionIdList[i]);
                AddStaticsticData(playerCopyStatisticData);
            }

            ///搜集宠物的
            List<PetInfo> petList = PlayerDataManager.Instance.PetData.FightingPetList;
            for (int i = 0; i < petList.Count; i++)
            {
                for (int j = 0; j < actionIdList.Count; j++)
                {
                    PetCopyStatisticData petCopyStatisticData = new PetCopyStatisticData();
                    petCopyStatisticData.petId = petList[i].Id;
                    petCopyStatisticData.actionId = actionIdList[j];
                    petCopyStatisticData.petInfo = petList[i];
                    petCopyStatisticData.type = mission_battle_statistics_helper.OutPutOrBear(actionIdList[j]);
                    petCopyStatisticData.value = (int)petList[i].GetStatisticDamage(actionIdList[j]);
                    AddStaticsticData(petCopyStatisticData);
                }
            }
            
        }

        private void AddStaticsticData(CopyStatisticData copyStatisticData)
        {
            _copyStatisticDataList.Add(copyStatisticData);
        }

        public void AddStaticsticData(PbMissionStatisticList missionStatisticList)
        {
            _copyStatisticDataList.Clear();
            for (int i = 0; i < missionStatisticList.statistic_list.Count; i++)
            {
                PbActionStatisticList actionStatisticList = missionStatisticList.statistic_list[i];
                for (int j = 0; j < actionStatisticList.action_list.Count; j++)
                {
                    PbActionStatisticInfo actionStatisticInfo = actionStatisticList.action_list[j];
                    if (actionStatisticList.entity_type == public_config.ENTITY_TYPE_PET)
                    {
                        PetCopyStatisticData statisticData = new PetCopyStatisticData();
                        statisticData.petId = (int)actionStatisticList.pet_id;
                        statisticData.type = mission_battle_statistics_helper.OutPutOrBear((int)actionStatisticInfo.action_id);
                        List<PetInfo> petList = PlayerDataManager.Instance.PetData.FightingPetList;
                        for (int k = 0; k < petList.Count; k++)
                        {
                            if ((int)actionStatisticList.pet_id == petList[k].Id)
                            {
                                statisticData.petInfo = petList[k];
                            }
                        }
                        statisticData.actionId = (int)actionStatisticInfo.action_id;
                        statisticData.value = (int)actionStatisticInfo.action_score;
                        AddStaticsticData(statisticData);
                    }
                    else
                    {
                        PlayerCopyStatisticData statisticData = new PlayerCopyStatisticData();

                        statisticData.type = mission_battle_statistics_helper.OutPutOrBear((int)actionStatisticInfo.action_id);
                        statisticData.Name = MogoProtoUtils.ParseByteArrToString(actionStatisticList.entity_name_bytes);
                        statisticData.Level = (int)actionStatisticList.entity_level;
                        statisticData.vocation = (int)actionStatisticList.entity_vocation;
                        statisticData.actionId = (int)actionStatisticInfo.action_id;
                        statisticData.value = (int)actionStatisticInfo.action_score;
                        statisticData.campId = (int)actionStatisticList.group_id;
                        AddStaticsticData(statisticData);
                    }

                }
            }
        }

        public List<CopyStatisticData> GetCopyStaticsticData(StatisticOutputOrBear type, int actionId)
        {
            List<CopyStatisticData> result = new List<CopyStatisticData>();
            for (int i = 0; i < _copyStatisticDataList.Count; i++)
            {
                if (_copyStatisticDataList[i].actionId == actionId && _copyStatisticDataList[i].type == type)
                {
                    result.Add(_copyStatisticDataList[i]);
                }

            }
            result.Sort(SortStaticsticData);
            return result;
        }

        public List<int> GetActionIdList(StatisticOutputOrBear type, int showType)
        {
            List<int> result = new List<int>();
            for (int i = 0; i < _copyStatisticDataList.Count; i++)
            {
                if (_copyStatisticDataList[i].type == type && result.Contains(_copyStatisticDataList[i].actionId) == false && mission_battle_statistics_helper.IsCanShowStatistic(_copyStatisticDataList[i].actionId, showType))
                {
                    result.Add(_copyStatisticDataList[i].actionId);
                }

            }
            result.Sort();
            return result;
        }

        private int SortStaticsticData(CopyStatisticData first, CopyStatisticData second)
        {
            return second.value - first.value;
        }
    }

    public enum StatisticCopyType
    {
        Single = 1,
        Multi = 2,
        PVP = 3,
    }
}
