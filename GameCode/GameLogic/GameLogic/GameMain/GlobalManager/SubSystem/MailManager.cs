﻿using System;
using Common.Data;
using Common.Events;
using Common.Structs.ProtoBuf;
using Common.ServerConfig;
using MogoEngine.Utils;
using MogoEngine;
using GameMain.Entities;
using MogoEngine.Events;
using Common.Utils;
using Common.Structs;
using GameLoader.Utils.CustomType;
using UnityEngine;
using System.Collections.Generic;
using Common.Base;

namespace GameMain.GlobalManager.SubSystem
{
    public class MailManager : Singleton<MailManager> 
    {
        private const int PER_PAGE_MAIL_COUNT = 5;
        private bool _isFirstRequest = true;

        public void RequestMailList(UInt32 page_index)
        {
            int curMailCount = PlayerDataManager.Instance.MailData.MailDataList.Count;
            int curMailTotal = PlayerDataManager.Instance.MailData.MailTotalNum;

            if (_isFirstRequest == true
                || (curMailCount <= page_index * PER_PAGE_MAIL_COUNT && curMailCount < curMailTotal))
            {
                action_config.MAIL_INFO_REQ.Rpc("mail_action_req", (int)page_index);
            }
            else
            {
                EventDispatcher.TriggerEvent<int>(MailEvents.REFRESH_MAIL_LIST, (int)page_index);
            }
        }

        public void RequestReadMail(UInt64 mail_id)
        {
            PbMail mail = PlayerDataManager.Instance.MailData.GetMailById(mail_id);
            if (mail.read_state == public_config.MAIL_READ)
            {
                EventDispatcher.TriggerEvent<ulong>(MailEvents.DISPLAY_MAIL_CONTENT, mail.id);
                return;
            }
            action_config.MAIL_READ_REQ.Rpc("mail_action_req", (ulong)mail_id);
        }

        public void RequestDeleleMail(UInt64 mail_id)
        {
            action_config.MAIL_DELETE_REQ.Rpc("mail_action_req", mail_id);
        }

        public void RequestDeleteList(LuaTable table)
        {
            action_config.MAIL_BATCH_DELETE_REQ.Rpc("mail_action_req", table);
        }

        public void RequestBatchDrawAttachment()
        {
            action_config.MAIL_BATCH_GET_ATTACHMENT_REQ.Rpc("mail_action_req");
        }

        public void RequestDrawAttachment(UInt64 mailId)
        {
            action_config.MAIL_GET_ATTACHMENT_REQ.Rpc("mail_action_req", mailId);
        }

        public void RequestSendMail(MailInfo _mailInfo)
        {
            RequestSendMail(_mailInfo.avatar_dbid, _mailInfo.title, _mailInfo.text, _mailInfo.from, _mailInfo.time, _mailInfo.attachment, _mailInfo.mail_type);
        }

        private void RequestSendMail(UInt64 avatar_dbid, string title, string text, string from, UInt32 time, LuaTable attachment, UInt32 mail_type)
        {
            action_config.MAIL_SEND_MAIL_REQ.Rpc("mail_action_req", avatar_dbid, title, text, from, time, attachment, mail_type);
        }

        public void MailResp(UInt32 action_id, UInt32 errorId, byte[] byteArr)
        {
            switch ((action_config)action_id)
            {
                case action_config.MAIL_NEW_MAIL_RESP:
                    PbMail newMail = MogoProtoUtils.ParseProto<PbMail>(byteArr);
                    PlayerDataManager.Instance.MailData.AddMail(newMail);
                    EventDispatcher.TriggerEvent<int>(MailEvents.REFRESH_MAIL_LIST,1);
                    break;
                case action_config.MAIL_INFO_REQ:
                    PbMailList mailList = MogoProtoUtils.ParseProto<PbMailList>(byteArr);
                    PlayerDataManager.Instance.MailData.Fill(mailList);
                    if (_isFirstRequest == true)
                    {
                        EventDispatcher.TriggerEvent<PanelIdEnum>(FastNoticeEvents.REMOVE_FAST_NOTICE, PanelIdEnum.Interactive); //删除已经显示出来的通知, 11是个邮件的节面id
                        PlayerDataManager.Instance.NoticeData.RemoveNoticeData(PanelIdEnum.Interactive); //清除将要显示出来的通知
                        for (int i = 0; i < PlayerDataManager.Instance.MailData.NewMailCount; i++)
                        {
                            SystemInfoManager.Instance.ShowFastNotice(60759);
                        }
                        _isFirstRequest = false;
                    }
                    int mailPageNum = PlayerDataManager.Instance.MailData.MailDataList.Count / 10 + 1;
                    EventDispatcher.TriggerEvent<int>(MailEvents.REFRESH_MAIL_LIST, mailPageNum);
                    break;
                case action_config.MAIL_READ_REQ:
                    PbMail readMail = MogoProtoUtils.ParseProto<PbMail>(byteArr);
                    PlayerDataManager.Instance.MailData.AddMail(readMail);
                    EventDispatcher.TriggerEvent<ulong>(MailEvents.DISPLAY_MAIL_CONTENT,readMail.id);
                    break;
                case action_config.MAIL_DELETE_REQ:
                    PbMailDbid deleteMail = MogoProtoUtils.ParseProto<PbMailDbid>(byteArr);
                    PlayerDataManager.Instance.MailData.DeleteMail(deleteMail.id);
                    EventDispatcher.TriggerEvent(MailEvents.DELETE_MAIL);
                    break;
                case action_config.MAIL_GET_ATTACHMENT_REQ:
                    PbMailDbid drawMail = MogoProtoUtils.ParseProto<PbMailDbid>(byteArr);
                    PlayerDataManager.Instance.MailData.DrawMail(drawMail.id);
                    EventDispatcher.TriggerEvent<ulong>(MailEvents.DRAW_MAIL_ATTACHMENT, drawMail.id);
                    break;
                case action_config.MAIL_ON_ADD_NEW_PUBLIC_MAIL:
                    break;
                case action_config.MAIL_BATCH_DELETE_REQ:
                    PbMailDbids deleteMails = MogoProtoUtils.ParseProto<PbMailDbids>(byteArr);
                    PlayerDataManager.Instance.MailData.DeleteMailList(deleteMails.mail_dbid);
                    EventDispatcher.TriggerEvent(MailEvents.BATCH_DELETE_MAIL);
                    break;
            }

            EventDispatcher.TriggerEvent(MailEvents.MAIL_DATA_CHANGE);
        }

        public void PulicMailsNotification(UInt32 action_id, UInt32 errorId, byte[] byteArr)
        {
            PbPublicMailNotification publicMail = MogoProtoUtils.ParseProto<PbPublicMailNotification>(byteArr);
            if (publicMail.mails_count != 0)
            {
                SystemInfoManager.Instance.ShowFastNotice(60759);
            }
        }
    }
}
