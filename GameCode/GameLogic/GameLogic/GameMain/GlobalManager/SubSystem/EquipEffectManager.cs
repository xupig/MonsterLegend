﻿#region 模块信息
/*==========================================
// 文件名：EquipEffectManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/11/18 20:41:57
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Utils;
namespace GameMain.GlobalManager.SubSystem
{
    public class EquipEffectManager : Singleton<EquipEffectManager>
    {
        private EquipEffectData _equipEffectData = PlayerDataManager.Instance.EquipEffectData;

        public void RequestGetFashionInfo()
        {
            PlayerAvatar.Player.RpcCall("facade_req", action_config.ACTION_FACADE_GETINFO, 0);
        }

        public void SetEquipEffect(int effectId)
        {
            PlayerAvatar.Player.RpcCall("facade_req", action_config.ACTION_FACADE_SETCURRENT, effectId);
        }

        public void RequestActiviteEffect(int effectId)
        {
            PlayerAvatar.Player.RpcCall("facade_req", action_config.ACTION_FACADE_ACTIVITE, effectId);
        }

        public void RequestTakeOffEffect(int effectId)
        {
            PlayerAvatar.Player.RpcCall("facade_req", action_config.ACTION_FACADE_TAKEOFF, effectId);
        }

        public void FacadeResp(uint actionId, uint errorId, byte[] bytes)
        {
            switch ((action_config)actionId)
            {
                case action_config.ACTION_FACADE_GETINFO:
                    PbFacadeInfo pbFacadeInfo = MogoProtoUtils.ParseProto<PbFacadeInfo>(bytes);
                    _equipEffectData.Init(pbFacadeInfo);
                    break;
                case action_config.ACTION_FACADE_NOTICE:
                    PbFacadeNotice pbFacadeNotice = MogoProtoUtils.ParseProto<PbFacadeNotice>(bytes);
                    _equipEffectData.SetFacadeCanActivite(pbFacadeNotice.facade_id);
                    EventDispatcher.TriggerEvent(FashionEvents.ACTIVATE_FASHION_CHANGE);
                    EventDispatcher.TriggerEvent<int>(FashionEvents.CAN_ACTIVITE_NEW_EFFECT, pbFacadeNotice.facade_id);
                    break;
                case action_config.ACTION_FACADE_SETCURRENT:
                    PbFacadeNotice pbCurrentFacade = MogoProtoUtils.ParseProto<PbFacadeNotice>(bytes);
                    _equipEffectData.SetCurrentEffect(pbCurrentFacade.facade_id);
                    break;
                case action_config.ACTION_FACADE_ACTIVITE:
                    PbFacadeNotice activiteNotice = MogoProtoUtils.ParseProto<PbFacadeNotice>(bytes);
                    _equipEffectData.SetEffectActivited(activiteNotice.facade_id);
                    EventDispatcher.TriggerEvent(FashionEvents.ACTIVATE_FASHION_CHANGE);
                    //ari EquipFacadeTipDataWrapper wrapper = new EquipFacadeTipDataWrapper();
                    //ari wrapper.type = EquipTipType.EffectType;
                    //ari wrapper.effectId = activiteNotice.facade_id;
                    //ari UIManager.Instance.ShowPanel(PanelIdEnum.EquipFacadeTip, wrapper);
                    break;
                case action_config.ACTION_FACADE_TAKEOFF:
                    PbFacadeNotice takeOffFacade = MogoProtoUtils.ParseProto<PbFacadeNotice>(bytes);
                    _equipEffectData.TakeOffEffect(takeOffFacade.facade_id);
                    break;
            }
            EventDispatcher.TriggerEvent(FashionEvents.REFRESH_EFFECT_LIST);
        }
    }
}