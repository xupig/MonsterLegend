﻿#region 模块信息
/*==========================================
// 文件名：PlayerReturnManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2016/3/19 14:17:28
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Structs.ProtoBuf;
using Common.Utils;
using GameMain.Entities;
using MogoEngine.Utils;
using Common.Data;
using Common.ServerConfig;
using MogoEngine.Events;
namespace GameMain.GlobalManager.SubSystem
{
    public class PlayerReturnManager : Singleton<PlayerReturnManager>
    {
        public PlayerReturnData ReturnData
        {
            get
            {
                return PlayerDataManager.Instance.playerReturnData;
            }
        }

        public bool IsOldPlayer
        {
            get
            {
                return PlayerAvatar.Player.old_role_status == public_config.OLD_RETURN_STATUS_YES;
            }
        }

        public bool HasRewardToGet()
        {
            return ReturnData.HasRewardToGet();
        }

        public void RequestToGetReward(int dayIndex)
        {
            PlayerAvatar.Player.RpcCall("old_return_reward_req", action_config.ACTION_OLD_ROLE_RETURN_REWARD_REQ, dayIndex);
        }

        public void OnOldReturnResp(ushort actionId, uint errorCode, byte[] byteArr)
        {
            if (byteArr.Length > 0)
            {
                PbOldReturnList returnPlayerRewardList = MogoProtoUtils.ParseProto<PbOldReturnList>(byteArr);
                ReturnData.UpdatePbOldReturn(returnPlayerRewardList);
            }
        }
    }
}