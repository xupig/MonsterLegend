﻿using Common.ServerConfig;
using GameMain.Entities;
using System;
using System.Collections.Generic;
using GameLoader.Utils.CustomType;
using System.Linq;
using System.Text;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using MogoEngine.Events;
using Common.Events;
using Common.Data;
using UnityEngine;
using Common.Utils;
using Common.Base;

namespace GameMain.GlobalManager.SubSystem
{
    public class FavorManager : Singleton<FavorManager> 
    {
        private void RPC(action_config actionId,int npcId, LuaTable table)
        {
            PlayerAvatar.Player.RpcCall("npc_reputation_req", (int)actionId, npcId, table);
        }

        public void RequestAllNpcFavor()
        {
            PlayerAvatar.Player.RpcCall("npc_reputation_req", (int)action_config.ACTION_NPC_REPUTATION_QUERY_ALL,0,0);
        }

        public void RequestNpcFavor(int npcId)
        {
            PlayerAvatar.Player.RpcCall("npc_reputation_req", (int)action_config.ACTION_NPC_REPUTATION_QUERY, npcId, new LuaTable());
        }

        public void RequestGiveItem(int npcId, ItemData itemData)
        {
            LuaTable table = new LuaTable();
            table.Add(itemData.Id, 1);
            RPC(action_config.ACTION_NPC_REPUTATION_HAND_IN, npcId,table);
        }

        public void NpcReputationResp(UInt32 actionId, UInt32 errorId, byte[] args)
        {
            PbReputationInfo favorInfo;
            switch ((action_config)actionId)
            {
                case action_config.ACTION_NPC_REPUTATION_QUERY_ALL:
                    PbReputationList reputationList = MogoProtoUtils.ParseProto<PbReputationList>(args);
                    for (int i = 0; i < reputationList.data.Count; i++)
                    {
                        PlayerDataManager.Instance.FavorData.UpdateNpcFavor(reputationList.data[i]);
                    }
                    break;
                case action_config.ACTION_NPC_REPUTATION_QUERY:
                    favorInfo = MogoProtoUtils.ParseProto<PbReputationInfo>(args);
                    FavorData favorData = PlayerDataManager.Instance.FavorData;
                    PlayerDataManager.Instance.FavorData.UpdateNpcFavor(favorInfo);
                    break;
                case action_config.ACTION_NPC_REPUTATION_HAND_IN:
                    favorInfo = MogoProtoUtils.ParseProto<PbReputationInfo>(args);
                    PlayerDataManager.Instance.FavorData.UpdateNpcFavor(favorInfo);
                    EventDispatcher.TriggerEvent<int>(FavorEvents.UpdateNpcFavor, favorInfo.npc_id);
                    break;
            }
        }
    }
}
