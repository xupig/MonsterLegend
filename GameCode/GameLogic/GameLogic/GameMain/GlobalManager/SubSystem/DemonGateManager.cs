﻿#region 模块信息
/*==========================================
// 文件名：DemonDateManager
// 命名空间: GameMain.GlobalManager.SubSystem
// 创建者：Duwilliam
// 修改者列表：
// 创建日期：2015年5月7日14:38:17
// 描述说明：恶魔之门数据管理
// 其他：
//==========================================*/
#endregion

using System;
using System.Collections.Generic;
using GameMain.Entities;


using Common.Structs;
using GameData;
using MogoEngine.Events;
using Common.Events;
using Common.ServerConfig;
using Common.Data;
using UnityEngine;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using Common.Utils;
using Common.Base;

namespace GameMain.GlobalManager.SubSystem
{
    public class DemonGateManager : Singleton<DemonGateManager>
    {
        private const int DEMONGATE_PLAY_ID = 28; //function 配置表， 恶魔之门 玩法id
        private const int DEMONGATE_DAY_RANK_INDEX = 4;
        private const int DEMONGATE_WEEK_RANK_INDEX = 5;

        private DemonGateData _demonGateData;
        private HashSet<int> _normalInstanceSet;


        public DemonGateManager()
        {
            _demonGateData = PlayerDataManager.Instance.DemonGateData;
            _demonGateData.IsInitData = false;
        }

        public bool isNormalInstance(int instanceId)
        {
            if (_normalInstanceSet == null)
            {
                _normalInstanceSet = new HashSet<int>();
                foreach (KeyValuePair<int, evil_mission> pair in XMLManager.evil_mission)
                {
                    List<string> dataList = data_parse_helper.ParseListString(pair.Value.__inst_id);
                    _normalInstanceSet.Add(int.Parse(dataList[1]));
                }
            }
            return _normalInstanceSet.Contains(instanceId);
        }

        #region 服务器返回
        public void EvilInfoResp(byte[] byteAttr)
        {
            _demonGateData.SelfRankInfo = MogoProtoUtils.ParseProto<PbEvilMissionInfo>(byteAttr);
            EventDispatcher.TriggerEvent(DemonGateEvents.ACTION_DEMONGATE_FIGHT_EVENTS);

            if (_demonGateData.SelfRankInfo.cur_score != 0)
            {
                _demonGateData.IsInitData = true;
            }

            if (_demonGateData.SelfRankInfo.rank_index_day == 0)
            {
                return;
            }

        }


        public void ShowResultPanel(byte[] byteAttr)
        {
            PbEvilMissionResult missionResult = MogoProtoUtils.ParseProto<PbEvilMissionResult>(byteAttr);
            PlayerDataManager.Instance.CopyData.FillInstance((int)missionResult.map_id, missionResult);
        }

        public void GetWorldAverageLevelResp(UInt32 worldLevel)
        {
            _demonGateData.WorldLevel = worldLevel;
            EventDispatcher.TriggerEvent(DemonGateEvents.ACTION_DEMONGATE_INFO_EVENTS);
        }

        public List<RewardData> GetRewardDataListByInstanceId(int instanceId, int worldLevel)
        {
            List<RewardData> result = new List<RewardData>();
            const int BossType = 3;
            foreach (KeyValuePair<int, monster_value> pair in XMLManager.monster_value)
            {
                monster_value value = pair.Value;
                if (value.__inst_id == instanceId && value.__difficulty == worldLevel && value.__monster_type == BossType)
                {
                    return item_reward_helper.GetRewardDataList(value.__reward_id, PlayerAvatar.Player.vocation);
                }
            }
            return result;
        }

        public List<DemonGateItemInfo> GetRankRewardList()
        {
            if (_demonGateData.WorldLevel == 0)
            {
                Debug.LogError(" GetRankRewardList worldLevel is error " + _demonGateData.WorldLevel);
                return null;
            }
            List<DemonGateItemInfo> rankRewardList = new List<DemonGateItemInfo>();
            foreach (KeyValuePair<int, evil_mission_reward> reward in GameData.XMLManager.evil_mission_reward)
            {
                List<string> dataList = data_parse_helper.ParseListString(reward.Value.__world_level);
                string worldlevelRangeMin = dataList[0];
                string worldlevelRangeMax = dataList[1];
                if (_demonGateData.WorldLevel >= int.Parse(worldlevelRangeMin) && _demonGateData.WorldLevel <= int.Parse(worldlevelRangeMax))
                {
                    item_reward itemReward = item_reward_helper.GetItemReward(reward.Value.__reward);
                    List<RewardData> rewards = item_reward_helper.GetRewardDataList(itemReward, PlayerAvatar.Player.vocation);

                    List<string> dataList2 = data_parse_helper.ParseListString(reward.Value.__rank);
                    string rankRangeMin = dataList2[0];
                    string rankRangeMax = dataList2[1];

                    string rankValue = string.Empty;
                    if (int.Parse(rankRangeMax) <= 3)
                    {
                        rankValue = rankRangeMax;
                    }
                    else
                    {
                        rankValue = rankRangeMin + "~" + rankRangeMax;
                    }
                    int averageValue = (int.Parse(rankRangeMax) + int.Parse(rankRangeMin)) / 2;
                    var itemData = new DemonGateItemInfo()
                    {
                        id = reward.Value.__id,
                        rank = rankValue,
                        rankAverageValue = averageValue,
                        rewards = rewards,
                    };
                    rankRewardList.Add(itemData);
                }
            }

            rankRewardList.Sort(DemonGateItemInfoComparison);

            return rankRewardList;
        }

        private int DemonGateItemInfoComparison(DemonGateItemInfo x, DemonGateItemInfo y)
        {
            return x.rankAverageValue - y.rankAverageValue;
        }

        public List<RewardData> GetSelfRewardDataList(int selfRank, int rankType)
        {
            int rewardID = 0;
            int playerLevel = PlayerAvatar.Player.level;
            GameDataTable<int, rank_rewards> list = GameData.XMLManager.rank_rewards;
            foreach (KeyValuePair<int, rank_rewards> reward in list)
            {
                List<string> dataList = data_parse_helper.ParseListString(reward.Value.__world_level);
                string worldlevelRangeMin = dataList[0];
                string worldlevelRangeMax = dataList[1];
                if (reward.Value.__type == rankType && playerLevel >= int.Parse(worldlevelRangeMin) && playerLevel <= int.Parse(worldlevelRangeMax))
                {
                    List<string> dataList2 = data_parse_helper.ParseListString(reward.Value.__rank);
                    string rankRangeMin = dataList2[0];
                    string rankRangeMax = dataList2[1];
                    if (selfRank >= int.Parse(rankRangeMin) && selfRank <= int.Parse(rankRangeMax))
                    {
                        rewardID = reward.Value.__rewards;
                    }
                }
            }

            item_reward itemReward = item_reward_helper.GetItemReward(rewardID);
            return item_reward_helper.GetRewardDataList(itemReward, PlayerAvatar.Player.vocation);
        }

        private int GetPlayMethodSubType()
        {
            string playMethodSubID = string.Empty;

            List<LimitActivityData> dataList = new List<LimitActivityData>();
            dataList.AddRange(activity_helper.GetLimitActivityList());
            dataList.AddRange(activity_helper.GetNormalFunctionList());

            for (int i = 0; i < dataList.Count; i++)
            {
                LimitActivityData dataItem = dataList[i];
                if (dataItem.openData.__fid == DEMONGATE_PLAY_ID)
                {
                    playMethodSubID = activity_helper.GetPlayerMethodSubID(dataItem.openData);
                }
            }

            if (string.IsNullOrEmpty(playMethodSubID))
            {
                return -1;
            }

            return int.Parse(playMethodSubID);
        }

        public int GetCopyID()
        {

            int playMethodSubID = GetPlayMethodSubType();

            int copyID = -1;
            foreach (KeyValuePair<int, evil_mission> evil_mission in XMLManager.evil_mission)
            {
                if (evil_mission.Value.__type == playMethodSubID)
                {
                    List<string> dataList = data_parse_helper.ParseListString(evil_mission.Value.__world_level);
                    string worldlevelRangeMin = dataList[0];
                    string worldlevelRangeMax = dataList[1];
                    if (_demonGateData.WorldLevel >= int.Parse(worldlevelRangeMin) && _demonGateData.WorldLevel <= int.Parse(worldlevelRangeMax))
                    {
                        List<string> dataList2 = data_parse_helper.ParseListString(evil_mission.Value.__inst_id);
                        copyID = int.Parse(dataList2[0]);
                    }
                }
            }
            return copyID;
        }

        public int GetNPCId()
        {
            int playMethodSubID = GetPlayMethodSubType();
            int npcId = -1;
            foreach (KeyValuePair<int, evil_mission> evil_mission in XMLManager.evil_mission)
            {
                if (evil_mission.Value.__type == playMethodSubID)
                {
                    List<string> dataList = data_parse_helper.ParseListString(evil_mission.Value.__world_level);
                    string worldlevelRangeMin = dataList[0];
                    string worldlevelRangeMax = dataList[1];
                    if (_demonGateData.WorldLevel >= int.Parse(worldlevelRangeMin) && _demonGateData.WorldLevel <= int.Parse(worldlevelRangeMax))
                    {
                        npcId = evil_mission.Value.__npc_id;
                    }
                }
            }
            return npcId;
        }

        #endregion
        private void RPC(action_config actionId, params object[] _args)
        {
            PlayerAvatar.Player.ActionRpcCall("evil_mission_action_req", (int)actionId, _args);
        }

        #region RPC请求

        /// <summary>
        /// 请求当前 服务器 世界等级
        /// </summary>
        public void RequestWorldLevel()
        {
            PlayerAvatar.Player.RpcCall("get_world_average_level_req");
        }

        public void RequestDemonGateInfo()
        {
            RPC(action_config.ACTION_EVIL_INFO);
        }

        private int _type;
        public void RequestDemonGateFight(int type)
        {
            _type = type;
            CopyLogicManager.Instance.RequestEnterCopy(ChapterType.DemonGate, RequestEnterCopy);
        }

        private void RequestEnterCopy()
        {
            RPC(action_config.ACTION_EVIL_CHALLENGE, _type);
        }

        public void RequestTransport(int mapId, int xPosition, int zPosition)
        {
            RPC(action_config.ACTION_EVIL_SEND, mapId, xPosition, zPosition);
        }

        public void RequestTransportWithLine(int mapId, int mapLine, int xPosition, int zPosition)
        {
            RPC(action_config.ACTION_SHARE_SEND, mapId, mapLine, xPosition, zPosition);
        }

        public void RequestDemonGateSweep()
        {
            RPC(action_config.ACTION_EVIL_MOP_UP);
        }
        #endregion

        public List<RewardData> GetNextStageRewardDataList(int rank, int type)
        {
            int playerLevel = PlayerAvatar.Player.level;
            int rangeMin;
            if (rank == 0)
            {
                rangeMin = 100;
            }
            else
            {
                rangeMin = GetRankRangeMin(rank) - 1;
            }
            int rewardID = 0;
            GameDataTable<int, rank_rewards> list = GameData.XMLManager.rank_rewards;
            foreach (KeyValuePair<int, rank_rewards> reward in list)
            {
                if (reward.Value.__type != type)
                {
                    continue;
                }
                List<string> dataList = data_parse_helper.ParseListString(reward.Value.__world_level);
                string worldlevelRangeMin = dataList[0];
                string worldlevelRangeMax = dataList[1];
                if (playerLevel >= int.Parse(worldlevelRangeMin) && playerLevel <= int.Parse(worldlevelRangeMax))
                {
                    List<string> dataList2 = data_parse_helper.ParseListString(reward.Value.__rank);
                    string rankRangeMin = dataList2[0];
                    string rankRangeMax = dataList2[1];
                    if (rangeMin >= int.Parse(rankRangeMin) && int.Parse(rankRangeMax) >= rangeMin)
                    {
                        rewardID = reward.Value.__rewards;
                        item_reward itemReward = item_reward_helper.GetItemReward(rewardID);
                        return item_reward_helper.GetRewardDataList(itemReward, PlayerAvatar.Player.vocation);
                    }
                }
            }
            return null;
        }

        private int GetRankRangeMin(int rank)
        {
            GameDataTable<int, rank_rewards> list = GameData.XMLManager.rank_rewards;
            int type = (int)RankType.demonGateDayRank;
            foreach (KeyValuePair<int, rank_rewards> reward in list)
            {
                List<string> dataList = data_parse_helper.ParseListString(reward.Value.__rank);
                int rankMin = int.Parse(dataList[0]);
                int rankMax = int.Parse(dataList[1]);
                if (reward.Value.__type == type && rankMin <= rank && rankMax >= rank)
                {
                    return rankMin;
                }
            }
            return 100;
        }
    }
}
