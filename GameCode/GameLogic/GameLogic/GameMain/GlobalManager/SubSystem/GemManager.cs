﻿using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using GameLoader.Utils;
using GameLoader.Utils.CustomType;
using GameMain.Entities;
using Mogo.Util;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using GameData;
using ModuleCommonUI;
using Common.Base;
using Common.Utils;
using Common;
using Common.Data;

namespace GameMain.GlobalManager.SubSystem
{
    public class GemManager : Singleton<GemManager>
    {
        public static int gemType;
        public GemData gemData = PlayerDataManager.Instance.gemData;

        public GemSettingData settingData = new GemSettingData();

        public static int UPGRADE_NEED_COUNT = 2;
        public static int COMPOSITE_NEEN_COUNT = 3;
        /// <summary>
        /// 服务器端装备宝石普通槽编号从1开始
        /// </summary>
        public static int SLOT_INDEX_START = 1;
        /// <summary>
        /// 服务器端装备宝石扩展槽编号从21开始
        /// </summary>
        public static int NEW_SLOT_INDEX_START = 21;
        /// <summary>
        /// 通用槽
        /// </summary>
        public static int SLOT_TYPE_COMMON = 99;

        public static int SOUL_STONE_ITEM_ID = 11000;

        //bad design
        private static int _compositeGemId;

        public void ResetSettingData()
        {
            settingData.ResetData();
        }

        public void CompositeInBag(int gemId)
        {
            action_config.GEM_COMBINE.Register();
            _compositeGemId = gemId;
            MogoWorld.Player.ActionRpcCall("gem_action_req", (int)action_config.GEM_COMBINE, gemId);
        }

        public void CompositeInBagAnyway(int gemId)
        {
            action_config.GEM_COMBINE_IN_BAG_ANYWAY.Register();
            _compositeGemId = gemId;
            MogoWorld.Player.ActionRpcCall("gem_action_req", (int)action_config.GEM_COMBINE_IN_BAG_ANYWAY, gemId);
        }

        public void Punch(int equipPosition, int slotPosition)
        {
            action_config.GEM_EQUIP_PUNCHING.Register();
            MogoWorld.Player.ActionRpcCall("gem_action_req", (int)action_config.GEM_EQUIP_PUNCHING, equipPosition, slotPosition);
        }

        public void Inlay(int equipPosition, int slotPosition, int gemPosition)
        {
            action_config.GEM_INLAY.Register();
            MogoWorld.Player.ActionRpcCall("gem_action_req", (int)action_config.GEM_INLAY_TO_SLOT, equipPosition, slotPosition, gemPosition);
        }

        public void QuickInlay(int equipPosition, int slotPosition)
        {
            action_config.GEM_INLAY_BUY_GEM.Register();
            MogoWorld.Player.ActionRpcCall("gem_action_req", (int)action_config.GEM_INLAY_BUY_GEM, equipPosition, slotPosition);
        }

        public void Outlay(int equipPosition, int slotPositon)
        {
            action_config.GEM_OUTLAY.Register();
            MogoWorld.Player.ActionRpcCall("gem_action_req", (int)action_config.GEM_OUTLAY, equipPosition, slotPositon);
        }

        public void SpriteOneTime()
        {
            action_config.GEM_SPIRIT.Register();
            MogoWorld.Player.ActionRpcCall("gem_action_req", (int)action_config.GEM_SPIRIT, 1);
        }

        public void SpiritTenTime()
        {
            action_config.GEM_SPIRIT_TEN.Register();
            MogoWorld.Player.ActionRpcCall("gem_action_req", (int)action_config.GEM_SPIRIT_TEN, 10);
        }

        /// <summary>
        /// 升级装备上镶嵌的宝石
        /// </summary>
        /// <param name="equipPosition"></param>
        /// <param name="slotPosition"></param>
        public void UpgradeInEquip(int equipPosition, int slotPosition)
        {
            action_config.GEM_COMBINE_IN_EQUIP.Register();
            MogoWorld.Player.ActionRpcCall("gem_action_req", (int)action_config.GEM_COMBINE_IN_EQUIP, equipPosition, slotPosition);
        }

        public void UpgradeInEquipAnyway(int equipPosition, int slotPosition)
        {
            action_config.GEM_COMBINE_IN_BAG_ANYWAY.Register();
            MogoWorld.Player.ActionRpcCall("gem_action_req", (int)action_config.GEM_COMBINE_IN_EQUIP_ANYWAY, equipPosition, slotPosition);
        }

        public void OnGemResp(UInt32 action_id, UInt32 _errorID, byte[] byteArr)
        {
            if (_errorID > 0)
            {
                //UnityEngine.Debug.LogError("Error ID:" + _errorID + " " + (error_code)_errorID);
                return;
            }
            gemData.UpdateGemData();
            PbGemSpiritInfo pbGemSpiritInfo = null;
            string content = string.Empty;
            switch ((action_config)action_id)
            {
                case action_config.GEM_COMBINE:
                case action_config.GEM_COMBINE_IN_BAG_ANYWAY:
                    if (_compositeGemId != 0)
                    {
                        string name = item_gem_helper.GetGemName(_compositeGemId);
                        content = MogoLanguageUtil.GetContent(34104, name);
                        _compositeGemId = 0;
                    }
                    //获得宝石
                    EventDispatcher.TriggerEvent(GemEvents.COMPOSITE_SUCCESS);
                    break;
                case action_config.GEM_COMBINE_IN_EQUIP:
                case action_config.GEM_COMBINE_IN_EQUIP_ANYWAY:
                    EventDispatcher.TriggerEvent(GemEvents.UPGRADE_SUCCESS);
                    break;
                case action_config.GEM_EQUIP_PUNCHING:
                   //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(34105), PanelIdEnum.Gem);
                   //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(34107), PanelIdEnum.Gem);
                    EventDispatcher.TriggerEvent(GemEvents.PUNCH_SUCCESS);
                    break;
                case action_config.GEM_INLAY:
                case action_config.GEM_INLAY_TO_SLOT:
                    //镶嵌成功
                    EventDispatcher.TriggerEvent(GemEvents.INLAY_SUCCESS);
                    break;
                case action_config.GEM_OUTLAY:
                    EventDispatcher.TriggerEvent(GemEvents.OUTLAY_SUCCESS);
                    break;
                case action_config.GEM_SPIRIT:
                    pbGemSpiritInfo = MogoProtoUtils.ParseProto<PbGemSpiritInfo>(byteArr);
                    int basisScore = gem_spirit_helper.GetNormalBlessScore((int)PlayerAvatar.Player.spirit_level);
                    if (pbGemSpiritInfo.little_hit_bless_num > 0)
                    {
                        //小爆击
                        content = string.Format("{0}+{1}", basisScore, ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, (pbGemSpiritInfo.all_bless - basisScore).ToString()));
                       //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(34001, content), PanelIdEnum.Gem);
                    }
                    if (pbGemSpiritInfo.big_hit_bless_num > 0)
                    {
                        //大爆击
                        content = string.Format("{0}+{1}", basisScore, ColorDefine.GetColorHtmlString(ColorDefine.COLOR_ID_GREEN, (pbGemSpiritInfo.all_bless - basisScore).ToString()));
                       //ari ToolTipsManager.Instance.ShowTip(PanelIdEnum.FloatTips, MogoLanguageUtil.GetContent(34002, content), PanelIdEnum.Gem);
                    }
                    EventDispatcher.TriggerEvent(GemEvents.SPIRIT_SUCCESS);
                    break;
                case action_config.GEM_SPIRIT_TEN:
                    pbGemSpiritInfo = MogoProtoUtils.ParseProto<PbGemSpiritInfo>(byteArr);
                    EventDispatcher.TriggerEvent(GemEvents.SPIRIT_SUCCESS);
                    EventDispatcher.TriggerEvent<PbGemSpiritInfo>(GemEvents.SHOW_SPIRIT_RESULT, pbGemSpiritInfo);
                    break;
                case action_config.GEM_INLAY_BUY_GEM:
                    if ((error_code)_errorID == error_code.ERR_SUCCESSFUL)
                    {
                        UIManager.Instance.ClosePanel(PanelIdEnum.GemPop);
                        EventDispatcher.TriggerEvent(GemEvents.INLAY_SUCCESS);
                    }
                    break;
            }
        }
    }

    public class GemSettingData
    {
        public bool ShowGemNotEnoughPop = true;
        public bool ShowSpiritTenTimePop = true;
        public bool ShowCompositeCost = true;
        public bool ShowQuickInlay = true;

        public void ResetData()
        {
            ShowGemNotEnoughPop = true;
            ShowSpiritTenTimePop = true;
            ShowCompositeCost = true;
            ShowQuickInlay = true;
        }
    }
}
