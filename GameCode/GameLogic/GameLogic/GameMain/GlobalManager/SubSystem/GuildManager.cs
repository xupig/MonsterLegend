﻿using Common.Base;
using Common.Events;
using Common.Global;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameLoader.Utils.CustomType;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class GuildManager : Singleton<GuildManager> 
    {
        private Dictionary<action_config, ulong> _lastUpdateTimeStampDict = new Dictionary<action_config, ulong>();
        private ulong _updateInterval;

        public GuildManager()
        {
            _updateInterval = ulong.Parse(global_params_helper.GetGlobalParam((int)GlobalParamId.rankingListUpdateTime));
        }

        public void GetGuildList()
        {
            PlayerAvatar.Player.ActionRpcCall("rank_action_req", (int)action_config.ACTION_GET_TNE_MIN_RANK_LIST, public_config.MGR_ID_RANK_GUILD, 0, 1);
        }

        private bool RPC(action_config actionId, params object[] args)
        {
            if (_lastUpdateTimeStampDict.ContainsKey(actionId) == true)
            {
                if (Global.serverTimeStamp > _lastUpdateTimeStampDict[actionId] + _updateInterval)
                {
                    _lastUpdateTimeStampDict[actionId] = Global.serverTimeStamp;
                    PlayerAvatar.Player.ActionRpcCall("guild_action_req", (int)actionId, args);
                    return true;
                }
            }
            else
            {
                PlayerAvatar.Player.ActionRpcCall("guild_action_req", (int)actionId, args);
                return true;
            }
            return false;
        }

        public void RegisterUpdate(action_config actionId)
        {
            if (_lastUpdateTimeStampDict.ContainsKey(actionId) == false)
            {
                _lastUpdateTimeStampDict.Add(actionId, 0);
            }
        }

        private void ClearUpdateTimeStamp()
        {
            _lastUpdateTimeStampDict.Clear();
        }

        public void GetApplyJoinList()
        {
            RPC(action_config.GUILD_GET_NEAR_JOIN_LIST);
        }

        public void GetMemberList(int orderType = public_config.GUILD_MEMBER_LIST_ORDER_TYPE_POSITION, int isAscending = public_config.GUILD_MEMBER_LIST_ORDER_ASC, int start = 1, int count = 200)
        {
            RPC(action_config.GUILD_GET_MEMBERS_LIST, start, count, orderType, isAscending);
        }

        public void GetMemberInfo(ulong id)
        {
            RPC(action_config.GUILD_GET_MEMBER_INFO, id);
        }

        public void ApplyJoinGuild(UInt64 id)
        {
            RPC(action_config.GUILD_APPLY_JOIN, id);
        }

        public void CancelJoinGuild(UInt64 id)
        {
            RPC(action_config.GUILD_CANCEL_JOIN, id);
        }

        public void LeaveGuild()
        {
            RPC(action_config.GUILD_LEAVE);
        }

        public void CreateGuild(string name, string manifesto, int iconId, int itemId)
        {
            RPC(action_config.GUILD_CREATE, name, ChatManager.Instance.FilterContent(manifesto), iconId, itemId);
        }

        public void SetManifesto(string manifesto)
        {
            RPC(action_config.GUILD_SET_MANIFESTO, ChatManager.Instance.FilterContent(manifesto));
        }

        public void GetMyGuildInformation()
        {
            if (PlayerAvatar.Player.guild_id != 0)
            {
                GetGuildInformation();
                GetSkillList();
                GetAchievementList();
            }
        }

        public void InviteJoin(ulong dbid)
        {
            RPC(action_config.GUILD_INVITE_JOIN, dbid);
        }

        public void AcceptInvite(ulong dbid)
        {
            RPC(action_config.GUILD_ACCEPT_INVITE, dbid);
        }

        public void GetOfflineNotice()
        {
            PlayerDataManager.Instance.GuildData.RefreshNotice();
        }

        public void GetGuildInformation()
        {
            RPC(action_config.GUILD_GET_MY_GUILD_INFO);
        }

        public void SearchGuild(string text)
        {
            RPC(action_config.GUILD_SEARCH, text);
        }

        public void SearchGuildSimpleInfo(UInt64 guildId)
        {
            RPC(action_config.GUILD_GET_GUILD_NAME,guildId);
        }

        public void UpgradeGuild()
        {
            RPC(action_config.GUILD_LEVEL_UP);
        }

        public void GetGuildJoinList()
        {
            RegisterUpdate(action_config.GUILD_APPLY_JOIN_LIST);
            RPC(action_config.GUILD_APPLY_JOIN_LIST);
        }

        public void AcceptApply(UInt64 id)
        {
            RPC(action_config.GUILD_ACCEPT_APPLY, id);
        }

        public void RefuseApply(UInt64 id)
        {
            RPC(action_config.GUILD_REFUSE_APPLY, id);
        }

        public void AppointPosition(ulong id, int position)
        {
            RPC(action_config.GUILD_APPOINT_POSITION, id, position);
        }

        public void TransferLeader(ulong id)
        {
            RPC(action_config.GUILD_LEADER_POSITION_TRANSFER, id);
        }

        public void FireMember(UInt64 id)
        {
            RPC(action_config.GUILD_FIRE_MEMBER, id);
        }

        public void DonateItem(int id, int value)
        {
            action_config.GUILD_CONTRIBUTION_HAND_IN.Register();
            RPC(action_config.GUILD_CONTRIBUTION_HAND_IN, id, value);
        }

        public void SendNotice(string notice)
        {
            RPC(action_config.GUILD_SEND_NOTICE, notice);
        }

        public void GetNoticeList()
        {
            RegisterUpdate(action_config.GUILD_GET_NOTICE_LIST);
            RPC(action_config.GUILD_GET_NOTICE_LIST);
        }

        public void GetSkillList()
        {
            RegisterUpdate(action_config.GUILD_GET_SKILL_LIST);
            RPC(action_config.GUILD_GET_SKILL_LIST);
        }

        public void StudyGuildSkill(int skillId)
        {
            RPC(action_config.GUILD_STUDY_SKILL, skillId);
        }

        public void GetAchievementList()
        {
            RegisterUpdate(action_config.GUILD_GET_ACHIEVEMENT_LIST);
            RPC(action_config.GUILD_GET_ACHIEVEMENT_LIST);
        }

        public void GetAchievementReward(int id)
        {
            RPC(action_config.GUILD_GET_ACHIEVEMENT_REWARD, id);
        }

        public void GetGuildRecordList()
        {
            RegisterUpdate(action_config.GUILD_GET_EVENT_RECORD_LIST);
            RPC(action_config.GUILD_GET_EVENT_RECORD_LIST);
        }

        public void GotoGuildBossScene()
        {
            RPC(action_config.GUILD_BOSS_GOTO);
        }

        public void OnZeroClockResp()
        {
            ClearUpdateTimeStamp();
            GetGuildInformation();
        }

        public void OnGuildManageResp(UInt32 actionId, UInt32 errorCode, byte[] byteArr)
        {
            switch ((action_config)actionId)
            {
                case action_config.GUILD_LEAVE:
                    PbGuildLeaveInfo pbLeave = MogoProtoUtils.ParseProto<PbGuildLeaveInfo>(byteArr);
                    if (pbLeave.dbid == PlayerAvatar.Player.dbid)
                    {
                        PlayerDataManager.Instance.GuildData.Clear();
                        ClearUpdateTimeStamp();
                        UIManager.Instance.ClosePanel(PanelIdEnum.GuildInformation);
                        EventDispatcher.TriggerEvent(ChatEvents.LEAVE_GUILD);
                    }
                    else
                    {
                        PlayerDataManager.Instance.GuildData.RemoveMemberFromList(pbLeave.dbid);
                        EventDispatcher.TriggerEvent(GuildEvents.Refresh_Member_List);
                    }
                    break;
                case action_config.GUILD_SET_MANIFESTO:
                    PbGuildManifesto pbManifesto = MogoProtoUtils.ParseProto<PbGuildManifesto>(byteArr);
                    PlayerDataManager.Instance.GuildData.MyGuildData.Manifesto = MogoProtoUtils.ParseByteArrToString(pbManifesto.content_bytes);
                    EventDispatcher.TriggerEvent(GuildEvents.Refresh_Manifesto);
                    break;
                case action_config.GUILD_GET_MEMBERS_LIST:
                    PbGuildMemberInfoList pbMemberInfoList = MogoProtoUtils.ParseProto<PbGuildMemberInfoList>(byteArr);
                    PlayerDataManager.Instance.GuildData.GuildMemberInfoList = pbMemberInfoList;
                    EventDispatcher.TriggerEvent(GuildEvents.Refresh_Member_List);
                    break;
                case action_config.GUILD_GET_MEMBER_INFO:
                    PbGuildMemberInfo pbMemberInfo = MogoProtoUtils.ParseProto<PbGuildMemberInfo>(byteArr);
                    PlayerDataManager.Instance.GuildData.AddMemberInfoToList(pbMemberInfo);
                    EventDispatcher.TriggerEvent(GuildEvents.Refresh_Member_List);
                    break;
                case action_config.GUILD_APPLY_JOIN_LIST:
                    PbGuildApplyJoinInfoList pbApplyList = MogoProtoUtils.ParseProto<PbGuildApplyJoinInfoList>(byteArr);
                    PlayerDataManager.Instance.GuildData.GuildApplyJoinInfoList = pbApplyList;
                    EventDispatcher.TriggerEvent(GuildEvents.Show_Apply_List);
                    PlayerDataManager.Instance.GuildData.RefreshNotice();
                    break;
                case action_config.GUILD_ACCEPT_APPLY:
                    PbGuildAcceptApply pbAccept = MogoProtoUtils.ParseProto<PbGuildAcceptApply>(byteArr);
                    PlayerDataManager.Instance.GuildData.RemoveApplyId(pbAccept.dbid);
                    PlayerDataManager.Instance.GuildData.AddApplyMemberIdToList(pbAccept.dbid);
                    EventDispatcher.TriggerEvent(GuildEvents.Show_Apply_List);
                    PlayerDataManager.Instance.GuildData.RefreshNotice();
                    UIManager.Instance.ClosePanel(PanelIdEnum.GuildCreatePanel);
                    if (pbAccept.dbid == PlayerAvatar.Player.dbid)
                    {
                        GetGuildInformation();
                    }
                    break;
                case action_config.GUILD_REFUSE_APPLY:
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.Id != 0)
                    {
                        PbGuildRefuseApply pbRefuse = MogoProtoUtils.ParseProto<PbGuildRefuseApply>(byteArr);
                        PlayerDataManager.Instance.GuildData.RemoveApplyId(pbRefuse.dbid);
                        EventDispatcher.TriggerEvent(GuildEvents.Show_Apply_List);
                        PlayerDataManager.Instance.GuildData.RefreshNotice();
                    }
                    break;
                case action_config.GUILD_APPLY_JOIN:
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.Id != 0)
                    {
                        PbGuildApplyJoin pbInfo = MogoProtoUtils.ParseProto<PbGuildApplyJoin>(byteArr);
                        PlayerDataManager.Instance.GuildData.AddApplyJoinInfoToList(pbInfo.apply_join_info);
                        EventDispatcher.TriggerEvent(GuildEvents.Show_Apply_List);
                        PlayerDataManager.Instance.GuildData.RefreshNotice();
                    }
                    break;
                case action_config.GUILD_CANCEL_JOIN:
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.Id != 0)
                    {
                        PbGuildCancelJoin pbCancelInfo = MogoProtoUtils.ParseProto<PbGuildCancelJoin>(byteArr);
                        PlayerDataManager.Instance.GuildData.RemoveApplyId(pbCancelInfo.dbid);
                        EventDispatcher.TriggerEvent(GuildEvents.Show_Apply_List);
                        PlayerDataManager.Instance.GuildData.RefreshNotice();
                    }
                    break;

                case action_config.GUILD_APPOINT_POSITION:
                    PbGuildAppointPosition pbAppoint = MogoProtoUtils.ParseProto<PbGuildAppointPosition>(byteArr);
                    PlayerDataManager.Instance.GuildData.ChangeMemberPosition(pbAppoint);
                    EventDispatcher.TriggerEvent(GuildEvents.Refresh_Member_List);
                    break;
                case action_config.GUILD_FIRE_MEMBER:
                    PbGuildFireMember pbFire = MogoProtoUtils.ParseProto<PbGuildFireMember>(byteArr);
                    if (pbFire.dbid == PlayerAvatar.Player.dbid)
                    {
                        PlayerDataManager.Instance.GuildData.Clear();
                        ClearUpdateTimeStamp();
                        UIManager.Instance.ClosePanel(PanelIdEnum.GuildInformation);
                    }
                    else
                    {
                        PlayerDataManager.Instance.GuildData.RemoveMemberFromList(pbFire.dbid);
                        EventDispatcher.TriggerEvent(GuildEvents.Refresh_Member_List);
                    }
                    break;
                case action_config.GUILD_SEND_NOTICE:
                    PbGuildNoticeInfo pbNoticeInfo = MogoProtoUtils.ParseProto<PbGuildNoticeInfo>(byteArr);
                    PlayerDataManager.Instance.GuildData.FillGuildNoticeInfo(pbNoticeInfo);
                    EventDispatcher.TriggerEvent(GuildEvents.Refresh_Guild_Notice_List);
                    break;
                case action_config.GUILD_GET_NOTICE_LIST:
                    PbGuildNoticeList pbNoticeList = MogoProtoUtils.ParseProto<PbGuildNoticeList>(byteArr);
                    PlayerDataManager.Instance.GuildData.FillGuildNoticeList(pbNoticeList);
                    EventDispatcher.TriggerEvent(GuildEvents.Refresh_Guild_Notice_List);
                    break;
                case action_config.GUILD_CONTRIBUTION_HAND_IN:
                    PbGuildContributeHandIn pbHandIn = MogoProtoUtils.ParseProto<PbGuildContributeHandIn>(byteArr);
                    PlayerDataManager.Instance.GuildData.UpdateMyTodayDonate(pbHandIn.cont_id, pbHandIn.count);
                    UIManager.Instance.ShowPanel(PanelIdEnum.GuildPop, 1);
                    break;
                case action_config.GUILD_GET_GUILD_NAME:
                    PbGuildSimpleInfo pbGuildSimpleInfo = MogoProtoUtils.ParseProto<PbGuildSimpleInfo>(byteArr);
                    //ari GuildDataManager.GetInstance().AddGuild(pbGuildSimpleInfo);
                    break;
                case action_config.GUILD_INVITE_JOIN:
                    PbGuildInviteJoin pbGuidlInviteJoin = MogoProtoUtils.ParseProto<PbGuildInviteJoin>(byteArr);
                    PlayerDataManager.Instance.GuildData.AddInviteInfo(pbGuidlInviteJoin);
                    break;
                case action_config.GUILD_STATE_NOTICE:
                    PbGuildStateNotice pbGuildStateNotice = MogoProtoUtils.ParseProto<PbGuildStateNotice>(byteArr);
                    PlayerDataManager.Instance.GuildData.ChangeMemberOnlineState(pbGuildStateNotice);
                    EventDispatcher.TriggerEvent(GuildEvents.Refresh_Member_List);
                    break;
            }
        }

        public void OnGuildCreateResp(UInt32 actionId, UInt32 errorCode, byte[] byteArr)
        {
            switch ((action_config)actionId)
            {
                case action_config.GUILD_CREATE:
                    GetGuildInformation();
                    break;
                case action_config.GET_GUILD_LIST:
                    PbRankGuildInfoList pbRankInfoList = MogoProtoUtils.ParseProto<PbRankGuildInfoList>(byteArr);
                    EventDispatcher.TriggerEvent<PbRankGuildInfoList>(GuildEvents.Show_Guild_List, pbRankInfoList);
                    break;
                case action_config.GUILD_SEARCH:
                    PbGuildSearchList pbSearchList = MogoProtoUtils.ParseProto<PbGuildSearchList>(byteArr);
                    EventDispatcher.TriggerEvent<PbGuildSearchList>(GuildEvents.Show_Search_List, pbSearchList);
                    break;
                case action_config.GUILD_GET_NEAR_JOIN_LIST:
                    PbGuildApplyJoinList pbList = MogoProtoUtils.ParseProto<PbGuildApplyJoinList>(byteArr);
                    PlayerDataManager.Instance.GuildData.FillGuildApplyJoin(pbList);
                    EventDispatcher.TriggerEvent(GuildEvents.Refresh_Guild_list_Item);
                    break;
                case action_config.GUILD_APPLY_JOIN:
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.Id == 0)
                    {
                        PbGuildApplyJoin pbInfo = MogoProtoUtils.ParseProto<PbGuildApplyJoin>(byteArr);
                        PlayerDataManager.Instance.GuildData.AddApplyIdToList(pbInfo.guild_id);
                        EventDispatcher.TriggerEvent(GuildEvents.Refresh_Guild_list_Item);
                    }
                    break;
                case action_config.GUILD_CANCEL_JOIN:
                    if (PlayerDataManager.Instance.GuildData.MyGuildData.Id == 0)
                    {
                        PbGuildCancelJoin pbCancelInfo = MogoProtoUtils.ParseProto<PbGuildCancelJoin>(byteArr);
                        PlayerDataManager.Instance.GuildData.CancelApplyIdToList(pbCancelInfo.guild_id);
                        EventDispatcher.TriggerEvent(GuildEvents.Refresh_Guild_list_Item);
                    }
                    break;
                case action_config.GUILD_GET_MY_GUILD_INFO:
                    PbGuildInfo pbGuildInfo = MogoProtoUtils.ParseProto<PbGuildInfo>(byteArr);
                    PlayerDataManager.Instance.GuildData.MyGuildData.Fill(pbGuildInfo);
                    EventDispatcher.TriggerEvent(GuildEvents.Show_My_Guild_Info);
                    GetMemberInfo(PlayerAvatar.Player.dbid);
                    break;
            }
        }

        public void OnGuildAchievementSkillResp(UInt32 actionId, UInt32 errorCode, byte[] byteArr)
        {
            switch ((action_config)actionId)
            {
                case action_config.GUILD_GET_SKILL_LIST:
                    PbGuildSkillList pbSkillList = MogoProtoUtils.ParseProto<PbGuildSkillList>(byteArr);
                    PlayerDataManager.Instance.GuildData.FillGuildSkillData(pbSkillList);
                    EventDispatcher.TriggerEvent(GuildEvents.Refresh_Skill_View);
                    break;
                case action_config.GUILD_GET_ACHIEVEMENT_LIST:
                    PbGuildAchievementList pbAchievementList = MogoProtoUtils.ParseProto<PbGuildAchievementList>(byteArr);
                    PlayerDataManager.Instance.GuildData.FillGuildAchievementData(pbAchievementList);
                    EventDispatcher.TriggerEvent(GuildEvents.Refresh_Achievement_View);
                    break;
            }
        }

        public void OnGuildNoticeResp(UInt32 actionId, UInt32 errorCode, byte[] byteArr)
        {
            switch ((action_config)actionId)
            {
                case action_config.GUILD_ITEM_NOTIFY:
                    PbGuildItemNotify pbItem = MogoProtoUtils.ParseProto<PbGuildItemNotify>(byteArr);
                    PlayerDataManager.Instance.GuildData.MyGuildData.UpdateGuildItem(pbItem.item_id, pbItem.item_count);
                    break;
                case action_config.GUILD_PLAYER_CONTRIBUTION_NOTIFY:
                    PbGuildContributionNotify pbContribution = MogoProtoUtils.ParseProto<PbGuildContributionNotify>(byteArr);
                    PlayerDataManager.Instance.GuildData.UpdateMyContribution(pbContribution.cont_num, pbContribution.today_cont_num, (uint)pbContribution.history_cont_num, (uint)pbContribution.yesterday_cont_num);
                    EventDispatcher.TriggerEvent(GuildEvents.Refresh_Guild_Contribute);
                    break;
                case action_config.GUILD_LEVEL_UP:
                    PlayerDataManager.Instance.GuildData.MyGuildData.Level++;
                    EventDispatcher.TriggerEvent(GuildEvents.Show_My_Guild_Info);
                    break;
                case action_config.GUILD_STUDY_SKILL:
                    PbGuildSkillStudy pbSkillStudy = MogoProtoUtils.ParseProto<PbGuildSkillStudy>(byteArr);
                    PlayerDataManager.Instance.GuildData.UpdateSkill(pbSkillStudy.skill_id);
                    EventDispatcher.TriggerEvent(GuildEvents.Refresh_Skill_View);
                    break;
                case action_config.GUILD_ACHIEVEMENT_NOTIFY:
                    PbGuildAchievementData pbAchievement = MogoProtoUtils.ParseProto<PbGuildAchievementData>(byteArr);
                    PlayerDataManager.Instance.GuildData.UpdateGuildAchievement(pbAchievement);
                    EventDispatcher.TriggerEvent(GuildEvents.Refresh_Achievement_View);
                    break;
                case action_config.GUILD_BOOMING_NOTIFY:
                    PbGuildBoomingNotify pbBooming = MogoProtoUtils.ParseProto<PbGuildBoomingNotify>(byteArr);
                    PlayerDataManager.Instance.GuildData.MyGuildData.UpdateGuildBooming(pbBooming.booming);
                    break;
            }
        }

        public void EventRecordResp(UInt16 action_id, LuaTable luaTable)
        {
            if ((action_config)action_id == action_config.GUILD_GET_EVENT_RECORD_LIST)
            {
                PlayerDataManager.Instance.GuildData.UpdateGuildRecord(luaTable);
                EventDispatcher.TriggerEvent(GuildEvents.Refresh_Guild_Record);
            }
        }

        public void AddGuildRecord(string content)
        {
            PlayerDataManager.Instance.GuildData.AddGuildRecordToList(content);
            EventDispatcher.TriggerEvent(GuildEvents.Refresh_Guild_Record);
        }
    }
}
