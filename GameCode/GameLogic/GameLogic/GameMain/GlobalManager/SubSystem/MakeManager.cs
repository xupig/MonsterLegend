﻿#region 模块信息
/*==========================================
// 文件名：MakeManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem
// 创建者：巩靖
// 修改者列表：
// 创建日期：2015/3/26 15:59:54
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Base;
using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameData;
using GameMain.Entities;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;

namespace GameMain.GlobalManager.SubSystem
{
    public class MakeManager : Singleton<MakeManager>
    {
        public MakeData makeData
        {
            get { return PlayerDataManager.Instance.MakeData; }
        }

        private void RPC(action_config actionId, params object[] args)
        {
            PlayerAvatar.Player.ActionRpcCall("mfg_action_req", (int)actionId, args);
        }

        /// <summary>
        /// 制造产品
        /// </summary>
        /// <param name="formulaId"></param>
        public void MakeFormula(int formulaId)
        {
            RPC(action_config.ACTION_MFG_PRODUCT, formulaId);
        }

        /// <summary>
        /// 学习配方
        /// </summary>
        /// <param name="formulaId"></param>
        public void LearnFormula(int formulaId)
        {
            RPC(action_config.ACTION_MFG_LEARN, formulaId);
        }

        /// <summary>
        /// 请求稀有配方背包
        /// </summary>
        public void GetFormulaBag()
        {
            if (PlayerDataManager.Instance.MakeData.hasRequestData == false)
            {
                PlayerDataManager.Instance.MakeData.hasRequestData = true;
                RPC(action_config.ACTION_MFG_FORMULA_BAG);
            }
        }

        public void GetFormulaCD()
        {
            RPC(action_config.ACTION_MFG_CD);
        }

        /// <summary>
        /// 开启天赋
        /// </summary>
        /// <param name="giftId">天赋ID</param>
        public void ActivateGift(int giftId)
        {
            RPC(action_config.ACTION_MFG_TALENT, giftId);
        }

        /// <summary>
        ///  请求CD时间
        /// </summary>
        public void RequestGetCDList()
        {
            RPC(action_config.ACTION_MFG_CD);
        }

        /// <summary>
        /// 请求重置专精
        /// </summary>
        public void RequestResetGift()
        {
            RPC(action_config.ACTION_MFG_RESET);
        }

        public void OnMakeResp(UInt32 actionId, UInt32 errorId, byte[] byteArr)
        {
            if (errorId > 0)
            {
                return;
            }
            PbMfgFormulaInfo info;
            switch ((action_config)actionId)
            {
                case action_config.ACTION_MFG_PRODUCT:
                    info = MogoProtoUtils.ParseProto<PbMfgFormulaInfo>(byteArr);
                    List<BaseItemData> itemList = mfg_formula_helper.GetItemId((int)info.formula_id);
                    MakeManager.Instance.makeData.UpdateSingleCDTime(info);
                    UIManager.Instance.ShowPanel(PanelIdEnum.ItemObtain, itemList);
                    EventDispatcher.TriggerEvent<int>(MakeEvents.FORMULA_MAKE_SUCCESS, (int)info.formula_id);
                    EventDispatcher.TriggerEvent<int>(MakeEvents.REFRESH_CONTENT_STATE, (int)info.formula_id);
                    break;
                case action_config.ACTION_MFG_LEARN:
                    info = MogoProtoUtils.ParseProto<PbMfgFormulaInfo>(byteArr);
                    PlayerDataManager.Instance.MakeData.AddLearnItem(info);
                    EventDispatcher.TriggerEvent<PbMfgFormulaInfo>(MakeEvents.FORMULA_BAG_CHANGE, info);
                    EventDispatcher.TriggerEvent<int>(MakeEvents.FORMULA_MAKE_SUCCESS, (int)info.formula_id);
                    break;
                case action_config.ACTION_MFG_FORMULA_BAG:
                    List<PbMfgFormulaInfo> list = MogoProtoUtils.ParseProto<PbMfgFormulaInfoList>(byteArr).mfgFormulaInfoList;
                    PlayerDataManager.Instance.MakeData.InitLearnList(list);
                    break;
                case action_config.ACTION_MFG_CD:
                    List<PbMfgFormulaInfo> cdList = MogoProtoUtils.ParseProto<PbMfgFormulaInfoList>(byteArr).mfgFormulaInfoList;
                    PlayerDataManager.Instance.MakeData.InitCDList(cdList);
                    break;
                default:
                    break;
            }
        }

      
    }
}
