﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameMain.Entities;

using GameLoader.Utils.CustomType;
using UnityEngine.Events;
using Common.Structs;
using GameData;
using MogoEngine.Events;
using Common.Events;
using Common.ServerConfig;
using GameLoader.Utils;
using Common.Data;
using UnityEngine;
using Common.Structs.ProtoBuf;
using MogoEngine.Utils;
using Common.Global;
using Common.Utils;
using Common.Chat;
using Common.Base;
using Mogo.Util;

namespace GameMain.GlobalManager.SubSystem
{
    public class TeamInstanceManager : Singleton<TeamInstanceManager>
    {
        public TeamInstanceManager()
        {
            EventDispatcher.AddEventListener<int>(SceneEvents.ENTER_MAP, OnEnterMap);
            EventDispatcher.AddEventListener<int, CopyResult>(CopyEvents.Result, OnInstanceFinished);
        }

        private bool _showRecommendFriend = false;
        private void OnInstanceFinished(int instanceId, CopyResult copyResult)
        {
            if (copyResult == CopyResult.Win)
            {
                _showRecommendFriend = true;
            }
        }

        private void OnEnterMap(int mapId)
        {
            if (_showRecommendFriend && map_helper.GetMapType(mapId) == public_config.MAP_TYPE_CITY || map_helper.GetMapType(mapId) == public_config.MAP_TYPE_WILD)
            {
                ShowRecommendFriendPanel();
            }
            PlayerDataManager.Instance.TeamInstanceData.HpMemberList.Clear();
            _showRecommendFriend = false;
        }

        private void ShowRecommendFriendPanel()
        {
            int limit = int.Parse(global_params_helper.GetGlobalParam(GlobalParamId.friend_recommend));
            if (PlayerDataManager.Instance.FriendData.GetMyFriendCount() >= limit) return;
            List<PbHpMember> list = GetRecommendList(PlayerDataManager.Instance.TeamInstanceData.HpMemberList);
            if (list.Count > 0)
            {
                UIManager.Instance.ShowPanel(PanelIdEnum.RecommendFriend, list);
            }
        }

        private List<PbHpMember> GetRecommendList(List<PbHpMember> dataList)
        {
            List<PbHpMember> result = new List<PbHpMember>();
            UInt64 mySeverId = PlayerAvatar.Player.dbid >> 32;
            for (int i = 0; i < dataList.Count; i++)
            {
                UInt64 avatarDbid = dataList[i].dbid;
                if (mySeverId == (avatarDbid >> 32) && PlayerDataManager.Instance.FriendData.HasFriend(avatarDbid) == false)
                {
                    result.Add(dataList[i]);
                }
            }
            return result;
        }

        private void RPC(string func, action_config actionId, params object[] args)
        {
            PlayerAvatar.Player.ActionRpcCall(func, (int)actionId, args);
        }

        public void CallPeople(int gameId, int channelId = public_config.CHANNEL_ID_WORLD)
        {
            RPC("team_call_action_req", action_config.ACTION_TEAM_CALL_CALL, gameId, channelId);
        }

        public void GetBuyCount(int id)
        {
            RPC("price_action_req", action_config.ACTION_GET_PRICE_BUY_COUNT, id);
        }

        public void ApplyJoinTeam(ulong playerDbid, uint gameId)
        {
            RPC("team_call_action_req", action_config.ACTION_TEAM_CALL_CALL_ACCEPT, playerDbid, gameId);
        }

        public void TransferItemToPlayer(ulong dbid, int position)
        {
            PlayerAvatar.Player.RpcCall("transfer_item_to_player", dbid, position);
        }

        public void CaptainItemApply(ulong dbid,byte index,byte option)
        {
            PlayerAvatar.Player.RpcCall("captain_distribute_req", action_config.CAPTAIN_ITEM_APPLY, dbid, index, option);
        }

        public void CaptainItemAssign(ulong dbid, byte index)
        {
            PlayerAvatar.Player.RpcCall("captain_distribute_req", action_config.CAPTAIN_ITEM_DISTRIBUTION, dbid, index, 0);
        }

        public void CaptainItemEnsure(ulong dbid, byte index)
        {
            PlayerAvatar.Player.RpcCall("captain_distribute_req", action_config.CAPTAIN_ITEM_FINAL, dbid, index, 0);
        }

        public void OnPriceResp(uint actionId, uint errorId, byte[] byteArr)
        {
            PbPriceList pbInfoList = MogoProtoUtils.ParseProto<PbPriceList>(byteArr);
            for (int i = 0; i < pbInfoList.price_info.Count; i++)
            {
                PbPriceInfo info = pbInfoList.price_info[i];
                if (info.price_id == inst_type_operate_helper.GetTimePriceId(ChapterType.TeamCopy))
                {
                    PlayerDataManager.Instance.TeamInstanceData.UpdateDailyTimes(info);
                }
            }
        }

        public void SendTeamCallMsg(UInt32 gameId, byte channelId,int minLevel,int maxLevel,string msg)
        {
            string content = string.Empty;
            ChatTeamLinkWrapper link;
            if (gameId == 10000000)
            {
                content = MogoLanguageUtil.GetContent(115005);
                link = new ChatTeamLinkWrapper(content, PlayerAvatar.Player.dbid, gameId, minLevel, maxLevel);
                ChatManager.Instance.RequestInviteLinkSendChat(public_config.CHANNEL_ID_WORLD, link);
            }
            else
            {
                team_detail detail = team_helper.GetTeamDetail(Convert.ToInt32(gameId));
                int instanceId = detail.__instance_id;
               
                ChapterType chapterType = instance_helper.GetChapterType(instanceId);
                switch (chapterType)
                {
                    case ChapterType.Copy:
                        task_data taskData = task_data_helper.GetMainTaskIdByInstanceId(instanceId);
                        string taskName = string.Empty;
                        if (taskData == null)
                        {
                            Debug.LogError("找不到对应的主线任务！！instanceId:" + instanceId);
                            taskName = "无";
                        }
                        else
                        {
                            taskName = task_data_helper.GetTaskName(taskData);
                        }
                        int level = instance_helper.GetMinLevel(instanceId);
                        string copyName = MogoLanguageUtil.GetContent(instance_helper.GetName(instanceId));
                        content = MogoLanguageUtil.GetContent(105627, level, taskName, copyName);
                        break;
                    //case ChapterType.Field:
                    //    {
                    //        string contents = MogoLanguageUtil.GetContent(10118, MogoLanguageUtil.GetContent(31139));
                    //        link = new ChatTeamLinkWrapper(contents, PlayerAvatar.Player.dbid, Convert.ToUInt32(instanceId), 1, team_helper.GetAvatarMaxLevel());
                    //        ChatManager.Instance.RequestInviteLinkSendChat(public_config.CHANNEL_ID_WORLD, link);
                    //    }
                    //    break;
                    default:
                        if(msg.Length==0)
                        {
                            content = MogoLanguageUtil.GetContent(83025, team_helper.GetDetailInstanceName(detail), minLevel, maxLevel);
                        }
                        else
                        {
                            content = MogoLanguageUtil.GetContent(83041, team_helper.GetDetailInstanceName(detail), minLevel, maxLevel, msg);
                        }
                       
                        break;
                }
                link = new ChatTeamLinkWrapper(content, PlayerAvatar.Player.dbid, gameId, minLevel, maxLevel);
                if (channelId == public_config.CHANNEL_ID_GUILD)
                {
                    link.guildId = PlayerAvatar.Player.guild_id;
                }
                ChatManager.Instance.RequestInviteLinkSendChat(channelId, link);
            }
        }

        public void OnTeamMissionResp(byte[] byteArr)
        {
            PbTeamMissionResultNew pbInfo = MogoProtoUtils.ParseProto<PbTeamMissionResultNew>(byteArr);
            PlayerDataManager.Instance.CopyData.FillInstance((int)pbInfo.map_id, pbInfo);
        }

        public void OnCaptainDistributionResp(UInt32 errorId, byte[] byteArr)
        {
            PbCaptainDistInfo pbInfo = MogoProtoUtils.ParseProto<PbCaptainDistInfo>(byteArr);
            PlayerDataManager.Instance.TeamInstanceData.PbCaptainDistInfo = pbInfo;
            UIManager.Instance.ShowPanel(PanelIdEnum.TeamRewardAssign);
        }

        public void HpMemberRefresh(byte[] byteArr)
        {
            PbHpMemberList pbList = MogoProtoUtils.ParseProto<PbHpMemberList>(byteArr);
            PlayerDataManager.Instance.TeamInstanceData.FillHpMember(pbList);
            PlayerDataManager.Instance.InstanceAvatarData.FillHpMember(pbList);
            EventDispatcher.TriggerEvent(TeamInstanceEvents.Teammate_Refresh);
        }

        public void HpMemberUpdate(UInt32 id, UInt32 hp, UInt32 maxHp)
        {
            PlayerDataManager.Instance.TeamInstanceData.UpdateHpMember(id, hp, maxHp);
            EventDispatcher.TriggerEvent(TeamInstanceEvents.Teammate_Hp_Refresh);
        }

        public void HpMemberDel(UInt32 id)
        {
            PlayerDataManager.Instance.TeamInstanceData.HpMemberDic.Remove(id);
            EventDispatcher.TriggerEvent(TeamInstanceEvents.Teammate_Refresh);
        }

        public void HpMemberUpdateOnline(UInt32 eid, byte online)
        {
            PlayerDataManager.Instance.TeamInstanceData.UpdateMemberOnline(eid, (uint)online);
            PlayerDataManager.Instance.TeamData.UpdateMemberOnlineByEntityId(eid, (uint)online);
            EventDispatcher.TriggerEvent<uint, int>(TeamInstanceEvents.Teammate_Online_Refresh, eid, (int)online);
        }

    }
}
