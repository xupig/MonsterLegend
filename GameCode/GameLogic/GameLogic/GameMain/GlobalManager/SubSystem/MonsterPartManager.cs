﻿using Common.Structs.ProtoBuf;
using Common.Utils;
using GameLoader.Utils;
using GameMain.Entities;


/* *******************************************************
 * author :  xu xianghan
 * email  :  xianghan0326@gmail.com  
 * history:  created by xu xianghan 2015/5/13 17:28:28 
 * function: 
 * *******************************************************/

namespace GameMain.GlobalManager.SubSystem
{
    public class MonsterPartManager : Singleton<MonsterPartManager> 
    {
        public void ResponseBodyPartList(uint entityId, PbBodyPartList bodyPartList)
        {
            LoggerHelper.Info(string.Format("ResponseBodyPartList, entityId = {0}, partCount = {1}.", entityId, bodyPartList.bodyPartList.Count));
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(entityId);
            if (creature == null) return;
            if (creature is EntityMonsterBase)
            {
                var monster = creature as EntityMonsterBase;
                monster.partManager.ResponseBodyPartList(bodyPartList);
            }
        }

        public void ResponseBodyPartUpdate(uint entityId, PbBodyPart pbBodyPart)
        {
            LoggerHelper.Debug(string.Format("ResponseBodyPartUpdate, entityId = {0}, partId = {1}.", entityId, pbBodyPart.body_part_id));
            var creature = CombatSystem.CombatSystemTools.GetCreatureByID(entityId);
            if (creature == null) return;
            if (creature is EntityMonsterBase)
            {
                var monster = creature as EntityMonsterBase;
                monster.partManager.ResponseBodyPartUpdate(pbBodyPart);
            }
        }

        public void ResponseBodyPartSelect(uint entityId, byte partId, uint ecode)
        {
            LoggerHelper.Info(string.Format("ResponseBodyPartSelect, entityId = {0}, partId = {1}, ecode = {2}", entityId, partId, ecode));
            if (ecode == 0)
            {
                var creature = CombatSystem.CombatSystemTools.GetCreatureByID(entityId);
                if (creature == null) return;
                if (creature is EntityMonsterBase)
                {
                    var monster = creature as EntityMonsterBase;
                    monster.partManager.ResponseBodyPartSelect(partId);
                }
            }
        }
    }
}
