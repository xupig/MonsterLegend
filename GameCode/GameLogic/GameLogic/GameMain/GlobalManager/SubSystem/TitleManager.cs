﻿using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameMain.GlobalManager.SubSystem
{
    public class TitleManager : Singleton<TitleManager> 
    {

        public void RequestTitleBag()
        {
            //Debug.LogError("RequestTitleBag");
            action_config.ACTION_TITLE_GET_TITLE_BAG.Rpc("title_action_req");
        }

        public void RequestUseTitle(uint titleId)
        {
            action_config.ACTION_TITLE_EQUIP_TITLE.Rpc("title_action_req", titleId);
        }

        //请求查看称号，服务器需要将其标记为已经查看
        public void RequestCheckTitle(uint titleId)
        {
            action_config.ACTION_TITLE_MARK_CHECK.Rpc("title_action_req", titleId);
        }

        public void RequestUnuseTitle()
        {
            action_config.ACTION_TITLE_UNINSTALL.Rpc("title_action_req");
        }

        public void OnTitleResp(UInt32 actionId, UInt32 error_code, byte[] byteArr)
        {
            //Debug.LogError("Return");
            switch ((action_config)actionId)
            {
                case action_config.ACTION_TITLE_GET_TITLE_BAG:
                    PbTitleBag titleBag = MogoProtoUtils.ParseProto<PbTitleBag>(byteArr);
                    //Debug.LogError("TitleBag" + titleBag.title_info.Count);

                    //ari PlayerDataManager.Instance.TitleData.Fill(titleBag);
                    EventDispatcher.TriggerEvent(TitleEvents.InitTitleData);
                    break;
                case action_config.ACTION_TITLE_GET_NEW_TITLE:
                    PbTitleInfo titleInfo = MogoProtoUtils.ParseProto<PbTitleInfo>(byteArr);
                    //Debug.LogError("TitleInfo" + titleInfo.title_id);
                    SystemInfoManager.Instance.ShowFastNotice(52652);
                    //ari PlayerDataManager.Instance.TitleData.AddTitle(titleInfo);
                    EventDispatcher.TriggerEvent<int>(TitleEvents.UpdateTitleData, (int)titleInfo.title_id);
                    break;
            }
        }
    }
}
