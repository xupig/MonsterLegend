﻿#region 模块信息
/*==========================================
// 文件名：RankingListManager
// 命名空间: GameLogic.GameLogic.GameMain.GlobalManager.SubSystem
// 创建者：蔡文超(GZ2967)
// 修改者列表：
// 创建日期：2015/4/17 10:03:34
// 描述说明：
// 其他：
//==========================================*/
#endregion

using Common.Data;
using Common.Events;
using Common.ServerConfig;
using Common.Structs.ProtoBuf;
using Common.Utils;
using GameLoader.Utils.CustomType;
using GameMain.Entities;
using MogoEngine;
using MogoEngine.Events;
using MogoEngine.Utils;
using System;
namespace GameMain.GlobalManager.SubSystem
{
    public class RankingListManager : Singleton<RankingListManager>
    {
        public RankInfo rankInfo = PlayerDataManager.Instance.rankInfo;

        // 请求排行榜玩家信息对应的ID
        public const int ROLE_GENERAL_INFO_REQUEST_ID = 1;
        public const int ROLE_FIGHT_ATTRIBUTE_ID = 2;
        public const int ROLE_EQUIP_REQUEST_ID = 3;
        public const int ROLE_FACADE_REQUEST_ID = 4;
        public const int ROLE_WING_DATA_ID = 5;
        public const int ROLE_GUILD_INFO = 6;

        public void OnRankResp(UInt32 actionId, UInt32 errorCode, byte[] byteArr)
        {
            switch ((action_config)actionId)
            {
                // 获得十分钟更新的排行榜数据
                case action_config.ACTION_GET_TNE_MIN_RANK_LIST:
                    PbRankInfoList pbRankInfoList = MogoProtoUtils.ParseProto<PbRankInfoList>(byteArr);
                    RankType rankType = (RankType)pbRankInfoList.type;
                    rankInfo.UpdateRankData((RankType)pbRankInfoList.type, pbRankInfoList);
                    rankInfo.UpdateAwardRankData((RankType)pbRankInfoList.type, pbRankInfoList);
                    if (rankType == RankType.demonGateDayRank || rankType == RankType.demonGateWeekRank)
                    {
                        EventDispatcher.TriggerEvent<RankType>(RankingListEvents.GET_DEMON_RANK_DATA, (RankType)pbRankInfoList.type);
                    }
                    else
                    {
                        EventDispatcher.TriggerEvent<RankType>(RankingListEvents.GET_RANKLIST_DATA, (RankType)pbRankInfoList.type);
                    }
                    break;
                // 获得十分钟更新的公会排行榜数据
                case action_config.ACTION_RANK_GUILD_INFO:
                    PbRankGuildInfoList pbRankGuildInfoList = MogoProtoUtils.ParseProto<PbRankGuildInfoList>(byteArr);
                    rankInfo.UpdateGuildRankData(pbRankGuildInfoList);
                    rankInfo.UpdateAwardRankData((RankType)pbRankGuildInfoList.type, pbRankGuildInfoList);
                    RankType type = (RankType)pbRankGuildInfoList.type;
                    EventDispatcher.TriggerEvent<RankType>(RankingListEvents.GET_GUILD_RANK, type);
                    if ((RankType)pbRankGuildInfoList.type == RankType.guildRank)
                    {
                        EventDispatcher.TriggerEvent<PbRankGuildInfoList>(RankingListEvents.GET_GUILD_RANK_IN_CHAT, pbRankGuildInfoList);
                    }
                    break;
                // 获得十分钟自己的排名
                case action_config.ACTION_GET_TNE_MIN_RANKING:
                    PbRankInfo pbTenRankInfo = MogoProtoUtils.ParseProto<PbRankInfo>(byteArr);
                    rankInfo.UpdateMyRankData((RankType)pbTenRankInfo.type, pbTenRankInfo);
                    EventDispatcher.TriggerEvent<PbRankInfo>(RankingListEvents.GET_MY_RANKING, pbTenRankInfo);
                    break;
                // 获得崇拜数据
                case action_config.ACTION_RANK_WORSHIP:
                    PbRankInfo pbWorshipInfo = MogoProtoUtils.ParseProto<PbRankInfo>(byteArr);
                    rankInfo.UpdateWorshipData(pbWorshipInfo);
                    EventDispatcher.TriggerEvent<PbRankInfo>(RankingListEvents.GET_RANK_WORSHIP_RESULT, pbWorshipInfo);
                    break;
                // 拿到排行榜崇拜数据
                case action_config.ACTION_RANK_GET_RANK_INFO:
                    PbRankInfoList pbHasWorshipInfo = MogoProtoUtils.ParseProto<PbRankInfoList>(byteArr);
                    rankInfo.UpdateWorshipData(pbHasWorshipInfo);
                    break;
            }
        }

        public void GetPlayerDataResp(UInt32 actionId, UInt32 errorCode, byte[] byteArr)
        {
            PbOfflineData pbOfflineData = MogoProtoUtils.ParseProto<PbOfflineData>(byteArr);
            switch (actionId)
            {
                case ROLE_GENERAL_INFO_REQUEST_ID:
                    rankInfo.FightForceCompareInfo = pbOfflineData.sys_fight_list;
                    EventDispatcher.TriggerEvent<PbOfflineData>(RankingListEvents.GET_RANK_PLAYER_GENERAL_INFO, pbOfflineData);
                    break;
                case ROLE_FIGHT_ATTRIBUTE_ID:
                    //rankInfo.UpdateRoleAttribute(pbOfflineData.battle_attries, pbOfflineData.vocation);
                    EventDispatcher.TriggerEvent<PbOfflineData>(RankingListEvents.GET_RANK_PLAYER_ATTRIBUTE_INFO, pbOfflineData);
                    break;
                case ROLE_EQUIP_REQUEST_ID:
                    PbBagList pbBodyEquipList = pbOfflineData.pkg_body_equip;
                    rankInfo.UpdateEquipInfo(pbOfflineData);
                    EventDispatcher.TriggerEvent<PbBagList>(RankingListEvents.GET_RANK_PLAYER_EQUIP_INFO, pbBodyEquipList);
                    break;
                case ROLE_WING_DATA_ID:
                    if (pbOfflineData.wing_body != null)
                    {
                        EventDispatcher.TriggerEvent<PbODWingBody>(RankingListEvents.GET_RANK_PLAYER_WING_DATA, pbOfflineData.wing_body);
                    }
                    break;
                case ROLE_FACADE_REQUEST_ID:
                    EventDispatcher.TriggerEvent<PbOfflineData>(RankingListEvents.GET_RANK_PLAYER_FACADE_INFO, pbOfflineData);
                    break;
                case ROLE_GUILD_INFO:
                    EventDispatcher.TriggerEvent<PbOfflineData>(RankingListEvents.GET_RANK_PLAYER_GUILD_INFO, pbOfflineData);
                    break;
            }
        }

        // 请求十分钟刷新的排行榜数据
        public void RequestTenMinutesRankListData(int type)
        {
            MogoWorld.Player.ActionRpcCall("rank_action_req", (int)action_config.ACTION_GET_TNE_MIN_RANK_LIST, type);
        }

        // 分页请求数据
        public void RequestTenMinutesRankListData(int type, int index)
        {
            MogoWorld.Player.ActionRpcCall("rank_action_req", (int)action_config.ACTION_GET_TNE_MIN_RANK_LIST, type, index);
        }

        public void RequestMyRanking(int type)
        {
            MogoWorld.Player.ActionRpcCall("rank_action_req", (int)action_config.ACTION_GET_TNE_MIN_RANKING, type);
        }

        public void RequestWorshipToPlayer(int type, ulong playerDbId)
        {
            MogoWorld.Player.ActionRpcCall("rank_action_req", (int)action_config.ACTION_RANK_WORSHIP, type, playerDbId);
        }

        public void RequestGetMyWorshipRecord()
        {
            var table = new LuaTable();
            PlayerAvatar.Player.RpcCall("rank_action_req", (int)action_config.ACTION_RANK_GET_RANK_INFO, table);
        }

        public void GetRankPlayerFightInfo(ulong playerDbId)
        {
            var table = new LuaTable();
            table.Add(1, (int)public_config.UM_PLAYER_FIGHT_INDEX);
            table.Add(2, (int)public_config.UM_PLAYER_FIGHT_ATTRIES);
            table.Add(3, (int)public_config.UM_PLAYER_VOCATION_INDEX);
            PlayerAvatar.Player.RpcCall("get_player_data_req", ROLE_FIGHT_ATTRIBUTE_ID, playerDbId, table);
        }

        public void GetRankPlayerEquipInfo(ulong playerDbId)
        {
            var table = new LuaTable();
            table.Add(1, (int)public_config.UM_PLAYER_ITEMS_INDEX);
            table.Add(2, (int)public_config.UM_PLAYER_BODY_RUNE);
            PlayerAvatar.Player.RpcCall("get_player_data_req", ROLE_EQUIP_REQUEST_ID, playerDbId, table);
        }

        public void GetRankPlayerRoleInfo(ulong playerdbId)
        {
            var table = new LuaTable();

            table.Add(1, (int)public_config.UM_PLAYER_VOCATION_INDEX);
            table.Add(2, (int)public_config.UM_PLAYER_NAME_INDEX);
            table.Add(3, (int)public_config.UM_PLAYER_LEVEL_INDEX);
            table.Add(4, (int)public_config.UM_PLAYER_VIP_LEVEL_INDEX);
            table.Add(5, (int)public_config.UM_PLAYER_DBID_INDEX);
            table.Add(6, (int)public_config.UM_PLAYER_FACADE);
            table.Add(7, (int)public_config.UM_PLAYER_FIGHT_INDEX);
            table.Add(8, (int)public_config.UM_PLAYER_TITLE_INDEX);

            PlayerAvatar.Player.RpcCall("get_player_data_req", ROLE_GENERAL_INFO_REQUEST_ID, playerdbId, table);
        }

        public void GetRankPlayerFacadeInfo(ulong playerdbId)
        {
            var table = new LuaTable();
            table.Add(1, (int)public_config.UM_PLAYER_VOCATION_INDEX);
            table.Add(2, (int)public_config.UM_PLAYER_FACADE);
            PlayerAvatar.Player.RpcCall("get_player_data_req", ROLE_FACADE_REQUEST_ID, playerdbId, table);
        }

        public void GetRankPlayerWingData(ulong playerdbId)
        {
            var table = new LuaTable();
            table.Add(1, (int)public_config.UM_PLAYER_WING_BODY);
            PlayerAvatar.Player.RpcCall("get_player_data_req", ROLE_WING_DATA_ID, playerdbId, table);
        }

        public void GetRankPlayerGuildData(ulong playerdbId)
        {
            var table = new LuaTable();
            table.Add(1, (int)public_config.UM_PLAYER_DBID_INDEX);
            table.Add(2, (int)public_config.UM_PLAYER_VOCATION_INDEX);
            table.Add(3, (int)public_config.UM_PLAYER_NAME_INDEX);
            table.Add(4, (int)public_config.UM_PLAYER_LEVEL_INDEX);
            table.Add(5, (int)public_config.UM_PLAYER_FIGHT_INDEX);
            table.Add(6, (int)public_config.UM_PLAYER_GUILD_ID_INDEX);
            PlayerAvatar.Player.RpcCall("get_player_data_req", ROLE_GUILD_INFO, playerdbId, table);
        }

        public void RequestPlayerPKData(ulong playerDbId)
        {
            PlayerAvatar.Player.ActionRpcCall("pk_mission_action_req", (int)action_config.ACTION_PK_GET_PK_LOGS, playerDbId);
        }

        public void RequestPlayerGuildData(ulong playerDbId)
        {
            PlayerAvatar.Player.ActionRpcCall("guild_action_req", (int)action_config.GUILD_BATTLE_GET_OTHER_PK_LOG, playerDbId);
        }

        public void OnGuildResp(uint actionId, uint errorId, byte[] args)
        {
            if ((action_config)actionId == action_config.GUILD_BATTLE_GET_OTHER_PK_LOG && (error_code)errorId == error_code.ERR_SUCCESSFUL)
            {
                PbGuildPkLogList guildPKList = MogoProtoUtils.ParseProto<PbGuildPkLogList>(args);
                EventDispatcher.TriggerEvent<PbGuildPkLogList>(RankingListEvents.GET_RANK_PLAYER_GUILD_DATA, guildPKList);
            }
        }

        public void OnPkResp(uint actionId, uint errorId, byte[] args)
        {
            if ((error_code)errorId == error_code.ERR_SUCCESSFUL)
            {
                PbPkLogList pkLogList = MogoProtoUtils.ParseProto<PbPkLogList>(args);
                EventDispatcher.TriggerEvent<PbPkLogList>(RankingListEvents.GET_RANK_PLAYER_PK_DATA, pkLogList);
            }
        }

        
    }
}